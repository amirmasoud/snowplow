<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

class Markdown
{
    public static function frontmatter(...$parameters): string
    {
        $generatedAt = Carbon::now()->toDateTimeLocalString();
        $version = '1.0.0'; // @todo we can do better than this :)

        return <<<FRONTMATTER
        ---
        title: {$parameters['title']}
        lang: {$parameters['lang']}
        generated_at: {$generatedAt}
        generated_via_version: {$version}
        sources:
            - {$parameters['source']}
        ---
        FRONTMATTER;
    }

    public static function render(...$parameters): string
    {
        $frontmatter = static::frontmatter(...$parameters);

        return <<<RENDER
        $frontmatter
        # {$parameters['title']}
        {$parameters['text']}
        RENDER;
    }
}
