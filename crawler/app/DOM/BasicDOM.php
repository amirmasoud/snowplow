<?php

namespace App\DOM;

use App\Markdown;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Exceptions\PropertyReservedException;
use DOMDocument;
use DOMXPath;
use Psr\Http\Message\ResponseInterface;
use Throwable;

#[AllowDynamicProperties]
abstract class BasicDOM
{
    public DOMXPath $xpath;

    public DOMDocument $doc;

    private array $markdownParameters = [];

    const ERROR_TEXT = '';

    const THROW_ERROR = false;

    public static function from(ResponseInterface $response)
    {
        return new static($response);
    }

    private function __construct(public ResponseInterface $response)
    {
        $this->doc = new DOMDocument();
        libxml_use_internal_errors(true);
        $this->doc->loadHTML($response->getBody());

        $this->xpath = new DOMXPath($this->doc);
    }

    /**
     * @throws PropertyReservedException|Throwable
     */
    public function set($key, $value): BasicDOM
    {
        throw_if(property_exists(static::class, $key), new PropertyReservedException());

        $content = Str::startsWith($value, '/')
            ? $this->content($value) // xpath
            : $value;

        $this->$key = $content;

        $this->markdownParameters[$key] = $content;

        return $this;
    }

    abstract protected function content(string $path): string;

    abstract public function get(string $path): string;

    abstract public function markdown(string $path): string;

    /**
     * @throws Throwable
     */
    public function put(string $path, ?string $file = null): bool
    {
        return $file
            ? Storage::put($file, $this->markdown($path))
            : Storage::put($path, Markdown::render(...$this->markdownParameters));
    }
}
