<?php

namespace App\DOM;

use Illuminate\Support\Facades\Storage;
use Throwable;

class HeaderDOM extends BasicDOM
{
    /**
     * @throws Throwable
     */
    protected function content(string $path): string
    {
        try {
            return $this->xpath->evaluate($path)[0]?->textContent;
        } catch (Throwable $e) {
            return static::THROW_ERROR
                ? throw $e
                : static::ERROR_TEXT;
        }
    }

    /**
     * @throws Throwable
     */
    public function get(string $path): string
    {
        return $this->content($path);
    }

    /**
     * @throws Throwable
     * @todo never used
     */
    public function markdown(string $path): string
    {
        return "# {$this->get($path)}";
    }
}
