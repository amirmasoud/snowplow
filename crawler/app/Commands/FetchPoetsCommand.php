<?php

namespace App\Commands;

use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use App\Json\PoetsJson;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Ausi\SlugGenerator\SlugOptions;
use Illuminate\Support\Facades\Http;
use Ausi\SlugGenerator\SlugGenerator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;
use function Termwind\{render};
use Closure;
use GuzzleHttp\Promise\Utils;
// use Illuminate\Http\Client\Pool;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\TransferException;

class FetchPoetsCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'fetch:poets {--P|poet=}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Fetch all poets';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $poets = $this->get();
    }

    public function get()
    {
        $slug = new SlugGenerator((new SlugOptions())
            ->setValidChars('آ-ی۰-۹')
            ->setLocale('fa')
            ->setDelimiter('-')
        );

        $poets = [];

        $this->send(
            ['https://api.ganjoor.net/api/ganjoor/poets'],
            after: function ($homeCollect) use ($slug, &$poets) {
                $this->putJson('index.json', $homeCollect);
                $bar = $this->output->createProgressBar(count($homeCollect));
                $bar->start();
                foreach ($homeCollect as $poet) {
                    if ($this->option('poet') && in_array($this->option('poet'), [$poet->id, $poet->name, $poet->fullUrl, $poet->nickname])) {
                        $poets[] = "https://api.ganjoor.net/api/ganjoor/poet/{$poet->id}?catPoems=true";
                    }

                    if (! $this->option('poet')) {
                        $poets[] = "https://api.ganjoor.net/api/ganjoor/poet/{$poet->id}?catPoems=true";
                    }
                }
                $bar->finish();
            }
        );

        $books = [];

        $this->send(
            $poets,
            after: function ($poetCollection) use ($slug, &$books) {
                $this->putJson("{$slug->generate($poetCollection->poet->name)}/index.json", $poetCollection);
                foreach ($poetCollection->cat->children as $book) {
                    $books[] = "https://api.ganjoor.net/api/ganjoor/cat?url={$book->fullUrl}&poems=true&mainSections=false";
                }
            }
        );

        $contents = [];

        $children = [];

        $this->send(
            $books,
            after: function ($contentCollection) use (&$children, $slug, &$contents) {
                $this->putJson("{$slug->generate($contentCollection->poet->name)}/{$slug->generate($contentCollection->cat->title)}/index.json", $contentCollection);

                foreach ($contentCollection->cat->children as $content) {
                    $children[] = "https://api.ganjoor.net/api/ganjoor/cat?url={$content->fullUrl}&poems=true&mainSections=false";
                }

                foreach ($contentCollection->cat->poems as $content) {
                    $contents[] = "https://api.ganjoor.net/api/ganjoor/poem/{$content->id}?catInfo=true&catPoems=true&rhymes=true&recitations=true&images=true&songs=true&comments=true&verseDetails=true&navigation=true&relatedpoems=true";
                }
            }
        );

        $this->send(
            $children,
            after: function ($contentCollection) use ($slug, &$contents) {
                $this->putJson("{$slug->generate($contentCollection->poet->name)}/{$slug->generate($contentCollection->cat->ancestors[1]->title)}/{$slug->generate($contentCollection->cat->title)}/index.json", $contentCollection);

                foreach ($contentCollection->cat->poems as $content) {
                    $contents[] = "https://api.ganjoor.net/api/ganjoor/poem/{$content->id}?catInfo=true&catPoems=true&rhymes=true&recitations=true&images=true&songs=true&comments=true&verseDetails=true&navigation=true&relatedpoems=true";
                }
            }
        );

        $this->send(
            array_reverse($contents),
            after: function ($contentCollection) use ($slug, &$contents) {
                $pathToContent = '';
                if (count($contentCollection->category->cat->ancestors) == 2) {
                    $pathToContent = '/'.$slug->generate($contentCollection->category->cat->ancestors[1]->title);
                }
                $this->putJson("{$slug->generate($contentCollection->category->poet->name)}$pathToContent/{$slug->generate($contentCollection->category->cat->title)}/{$slug->generate($contentCollection->title)}.json", $contentCollection);
            }
        );
    }

    public function send(array $urls, ?Closure $fullfilled = null, ?Closure $after = null, ?Closure $rejected = null, bool $asCollection = false)
    {
        if (! $fullfilled) {
            $fullfilled = function ($response) use ($after, $asCollection) {
                $body = json_decode($response->getBody());

                if ($asCollection) {
                    $body = Collection::make($body);
                }

                if ($after) {
                    $after($body);
                }
            };
        }

        if (! $rejected) {
            $rejected = function ($reason, $index) {
                // echo 'E';

                if ($reason instanceof \GuzzleHttp\Exception\ConnectException) {
                    logger($reason);
                    echo $reason->getMessage();
                    return;
                }

                if ($reason->hasResponse()){
                    if ($reason->getResponse()->getStatusCode() == '400') {
                        echo '400';
                    } elseif($reason->getResponse()->getStatusCode() == '403') {
                        echo '403';
                    } else {
                        // echo $reason->getMessage();
                    }
                } else {
                    echo 'O';
                }
            };
        }

        $client = new Client([
            'timeout' => 600, // Response timeout
            'connect_timeout' => 600, // Connection timeout
            'allow_redirect' => false,
            'read_timeout' => 30,
        ]);

        $bar = $this->output->createProgressBar(count($urls));
        $bar->start();
        foreach (array_chunk($urls, 30) as $chunk) {
            $requests = function ($urls) {
                foreach ($urls as $promise) {
                    yield new Request('GET', $promise);
                }
            };

            $pool = new Pool($client, $requests($chunk), [
                'concurrency' => 30,
                'fulfilled' => fn ($response, $index) => $fullfilled($response),
                'rejected' => fn ($reason, $index) => $rejected($reason, $index),
            ]);

            $promise = $pool->promise();

            $promise->wait(false);

            $bar->advance(30);
        }
        $bar->finish();
    }

    protected function putJson(string $path, $collect): void
    {
        Storage::put($path, json_encode($collect, JSON_UNESCAPED_UNICODE));
    }

    protected function putMarkdown(string $path, $collect)
    {
        Storage::put($path, $collect);
    }
}
