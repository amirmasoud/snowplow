<?php

namespace App\Commands;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use LaravelZero\Framework\Commands\Command;

class NormalizePoetsCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'normalize:poets {--P|poet=}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Normalize fetched poets';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Storage::allFiles('.') as $file) {
            $exploded = explode('.', $file);
            $exploded = array_reverse($exploded);
            if ($exploded[0] === 'json' && ! str_contains($file, 'index.json')) {
                $content = json_decode(Storage::get($file));
                $path = str_replace('json/', 'md/', $exploded[1]);
                Storage::put(
                    "$path.md",
                    <<<MARKDOWN
                    ---
                    title: >-
                        $content?->title
                    ---
                    # $content?->title

                    $content?->htmlText
                    MARKDOWN
                );
            }
        }
    }
}
