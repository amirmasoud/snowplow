<?php

use voku\helper\ASCII;

return [
    'language' => ASCII::PERSIAN_LANGUAGE_CODE,
    'absolute' => '',
    'pages' => [
        [
            /**
             * if `title` or `text` start with `/` we would assume that supplied string is xpath.
             */
            'title' => '/html/body/div[2]/div[4]/div/article/div[3]/h2/a',
            'text' => '/html/body/div[2]/div[4]/div/article/p[1]',
            'path' => '/حافظ/زندگی-نامه',
            'source' => 'https://ganjoor.net/hafez',
        ],
        [
            'title' => '/html/body/div[2]/div[4]/div/article/div[3]/h2/a',
            'text' => '/html/body/div[2]/div[4]/div/article/div[6]/div[3]/p',
            'path' => '/حافظ/زندگی-نامه',
            'source' => 'https://ganjoor.net/hafez',
        ],
    ],
];
