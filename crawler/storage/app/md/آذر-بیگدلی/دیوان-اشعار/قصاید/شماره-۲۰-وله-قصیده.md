---
title: >-
    شمارهٔ ۲۰ - وله قصیده
---
# شمارهٔ ۲۰ - وله قصیده

<div class="b" id="bn1"><div class="m1"><p>خوانده بر خوان فلکم، هان چکنم؟!</p></div>
<div class="m2"><p>خون دل، مایده ی خوان چکنم؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میزبان را، همه ابنای زمان</p></div>
<div class="m2"><p>بیکی خوان شده مهمان چکنم؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشته هم کاسه، سیه کاسه ی چند؛</p></div>
<div class="m2"><p>دست در کاسه ی ایشان چکنم؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چنین خلق، که هم خورده نمک</p></div>
<div class="m2"><p>هم شکستند نمکدان چکنم؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم نمک ریخته از بی نمکی</p></div>
<div class="m2"><p>هم طلب داشته تاوان چکنم؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوع خود را همه ی جانوران</p></div>
<div class="m2"><p>هم نشینند جز انسان چکنم؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که انسان شمرندم، ز ایشان</p></div>
<div class="m2"><p>بایدم بود گریزان چکنم؟!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآنکه این نوع، کش انسان خوانند؛</p></div>
<div class="m2"><p>شده بازیچه ی شیطان چکنم؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جستجو ناشده، حال همه کس</p></div>
<div class="m2"><p>آشکارا شده پنهان چکنم؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال دونان، ز بیان مستغنی است</p></div>
<div class="m2"><p>گشته اشراف چو دونان چکنم؟!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عیب پنهانی ارذال جهان</p></div>
<div class="m2"><p>چون عیان گشت، در اعیان چکنم؟!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آهن تفته، ز آتش بتر است؛</p></div>
<div class="m2"><p>بدتر از بد شده نیکان چکنم؟!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سالها شد که برون می ناید</p></div>
<div class="m2"><p>در ز بحر و، گهر از کان چکنم؟!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رنگ از رنگرز مهر ندید</p></div>
<div class="m2"><p>جامه ی لعل بدخشان چکنم؟!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز ابر نیسان، دم آبی نچشید؛</p></div>
<div class="m2"><p>صدف گوهر رخشان چکنم؟!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می و آب و زر و خاک و، گل و خار</p></div>
<div class="m2"><p>شده با هم همه یکسان چکنم؟!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گشته یکرنگ همه خلق جهان</p></div>
<div class="m2"><p>شکوه از این، گله از آن چکنم؟!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفته رفته شده ناکس، همه کس</p></div>
<div class="m2"><p>گفتگو با همه نتوان چکنم؟!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مال را، به ز جمال و ز کمال؛</p></div>
<div class="m2"><p>میشمارند حریفان چکنم؟!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیده از حسن و، دل از عشق نفور؛</p></div>
<div class="m2"><p>با چنین مردم نادان چکنم؟!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که زر در کف قبطی بیند</p></div>
<div class="m2"><p>گویدش : موسی عمران چکنم؟!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیرهن پوش، چو گرگی نگرد؛</p></div>
<div class="m2"><p>خواندش یوسف کنعان چکنم؟!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سور، در سایه ی ماتم بگریخت؛</p></div>
<div class="m2"><p>سود شد مایه ی نقصان چکنم؟!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرد ماتمکده ی خاک نشست</p></div>
<div class="m2"><p>بسیه جامه ی کیوان چکنم؟!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خرقه در نیل فرو برد از گرد</p></div>
<div class="m2"><p>نیلگون قبه ی گردان چکنم؟!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از میان برده فلک مشعل مهر</p></div>
<div class="m2"><p>تار شد، طاق نه ایوان چکنم؟!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشد از شمع کواکب روشن</p></div>
<div class="m2"><p>یک شب این تیره شبستان چکنم؟!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خانه را، کش نفروزند چراغ؛</p></div>
<div class="m2"><p>نقش خورشید بر ایوان چکنم؟!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نامه را، کش ننویسند ز مهر؛</p></div>
<div class="m2"><p>مهر جمشید بعنوان، چکنم؟!</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دور جمشید، به ضحاک رسید؛</p></div>
<div class="m2"><p>شد جم اضحوکه ی دوران چکنم؟!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دوش ضحاک فلک را ماران</p></div>
<div class="m2"><p>شد چو تنین شرر افشان چکنم؟!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زهر این مار، برآورد دمار</p></div>
<div class="m2"><p>از بد و نیک جهان، هان چکنم؟!</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جانگزا زهر جهان سوز مدام</p></div>
<div class="m2"><p>ریختش از بن دندان چکنم؟!</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بس سر جانور از مغز تهی</p></div>
<div class="m2"><p>شد، نشد چاره ی ثعبان چکنم؟!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عنقریب است، کزین سم نقیع</p></div>
<div class="m2"><p>یکتن انسان نبرد جان چکنم؟!</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عالم از انس تهی گشت و، در آن</p></div>
<div class="m2"><p>انس دارند بنی جان چکنم؟!</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دهر ویران و، در آن ویرانه</p></div>
<div class="m2"><p>دیو رونق ده دیوان چکنم؟!</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تیغها آخته دیوان بر هم</p></div>
<div class="m2"><p>بر سر تخت سلیمان چکنم؟!</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ناامید آل پیمبر ز جهان</p></div>
<div class="m2"><p>کامرانف دوده ی مروان چکنم؟!</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گشته هر پیرزنی، تیر زنی</p></div>
<div class="m2"><p>چرخش، از ناله، رجز خوان چکنم؟!</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گوی زن گشته و فرموکش گوی</p></div>
<div class="m2"><p>قامت خم شده چوگان چکنم؟!</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چرخ را کرده کمان، دوکش تیر؛</p></div>
<div class="m2"><p>سوزنش آمده پیکان چکنم؟!</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>معجزش مغفر و آن جامه که دوخت</p></div>
<div class="m2"><p>بهر خفتن، شده خفتان چکنم؟!</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر عجوزه، زده از معجزه دم</p></div>
<div class="m2"><p>هر سلیطه، شده سلطان چکنم؟!</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گشته هر ماری و هر موری میر</p></div>
<div class="m2"><p>خاین خانه ی اخوان چکنم؟!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کاه و بیجاده، بیک نرخ خرند؛</p></div>
<div class="m2"><p>فلک آویخته میزان چکنم؟!</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون زحل، آنکه بکین شد مایل؛</p></div>
<div class="m2"><p>برتری یافت ز اقران چکنم؟!</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آنکه چون پیر زنان ساخت بچرخ</p></div>
<div class="m2"><p>داده چرخش سرو سامان چکنم؟!</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>و آن لئیمان که چو مورند ضعیف</p></div>
<div class="m2"><p>دیو را گفته سلیمان چکنم؟!</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کوفت کاووس چو کوس اقبال</p></div>
<div class="m2"><p>سر برآورد بطغیان چکنم؟!</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ناکسان از طمع جیفه ی او</p></div>
<div class="m2"><p>شهره اش کرده به احسان چکنم؟!</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از دو مردار، که از تخت آویخت</p></div>
<div class="m2"><p>کرکسش برد بکیوان چکنم؟!</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قصه ی عالم ویران گفتم</p></div>
<div class="m2"><p>شرح ویرانی ایران چکنم؟!</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جای غولان شده آن دشت که بود</p></div>
<div class="m2"><p>پیش ازین بیشه ی شیران چکنم؟!</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شد ببین جای کیان، جای کیان</p></div>
<div class="m2"><p>هان بناسازی گیهان چکنم؟!</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زابل، از زابلیان مانده تهی</p></div>
<div class="m2"><p>گشته با مزبله یکسان چکنم؟!</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر طرف مینگرم، ضحاکی</p></div>
<div class="m2"><p>مهد گسترده در ایوان چکنم؟!</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بسته پیرامن او، دونی چند</p></div>
<div class="m2"><p>صف، پی بردن فرمان چکنم؟!</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر چه گوید، همه گر هذیان است</p></div>
<div class="m2"><p>کرده بر صدق وی اذعان چکنم؟!</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ظلمت ظلم، سیه کرده جهان؛</p></div>
<div class="m2"><p>روز و شب ساخته یکسان چکنم؟!</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نام تاراج، نهادند خراج؛</p></div>
<div class="m2"><p>گشته ویران دهی ایران چکنم؟!</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>باز این خارجیان گر ننهند</p></div>
<div class="m2"><p>بی خراج این ده ویران چکنم؟!</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر چه را، غیر شمارد دشوار؛</p></div>
<div class="m2"><p>غیرتم گیردش آسان چکنم؟!</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>هر چه را، خلق گران انگارند</p></div>
<div class="m2"><p>خردش همتم ارزان چکنم؟!</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ای ضعیفان، ز تکاهل بردید؛</p></div>
<div class="m2"><p>همه سرها بگریبان چکنم؟!</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چاره ی ظلم، بود آسان، ولیک</p></div>
<div class="m2"><p>ضعفتان کرده هراسان چکنم؟!</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>غیرت، ای فوج ابابیل، که شد</p></div>
<div class="m2"><p>کعبه از ابرهه ویران چکنم؟!</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آذر، این سرسبکان را از ضعف؛</p></div>
<div class="m2"><p>گوش سنگین بود، افغان چکنم؟!</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آهن سرد چه کوبم؟! نرسد</p></div>
<div class="m2"><p>پتک یک مرد بسندان چکنم؟!</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>کاوه، کز نطع برافراشت درفش؛</p></div>
<div class="m2"><p>بسته دارد در دکان چکنم؟!</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گاو، کو دایه ی افریدون بود</p></div>
<div class="m2"><p>ناورد شیر به پستان چکنم؟!</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خارزاری است عراق از ایران</p></div>
<div class="m2"><p>پیش اگر بود گلستان چکنم؟!</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>در عراق، از روش اهل نفاق؛</p></div>
<div class="m2"><p>تل خاکی است صفاهان چکنم؟!</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>رایت کاوگیان گشت نگون</p></div>
<div class="m2"><p>پیش اگر سود بکیوان چکنم؟!</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ساحت گلشن فردوس شده است</p></div>
<div class="m2"><p>منبت خار مغیلان چکنم؟!</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بلبلش مرده، گلش پژمرده</p></div>
<div class="m2"><p>سنبلش طره پریشان چکنم؟!</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زنده رودش، که نم از کوثر داشت</p></div>
<div class="m2"><p>با حمیم آمده یکسان چکنم؟!</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>باغها گشته نمودار جحیم</p></div>
<div class="m2"><p>قصرها گشته بیابان چکنم؟!</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>روزها شد که ندید آنجا کس</p></div>
<div class="m2"><p>صبح را با لب خندان چکنم؟!</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>رفت شبها که کس آنجا نشنید؛</p></div>
<div class="m2"><p>نغمه ی مرغ سحر خوان، چکنم؟!</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>پاسبان گشته در آن کشور دزد</p></div>
<div class="m2"><p>گرگ آنجا شده چوپان چکنم؟!</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر طرف بال فشان خفاشی</p></div>
<div class="m2"><p>آفتابش شده پنهان چکنم؟!</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مردمش، چون گله ی گرگ زده؛</p></div>
<div class="m2"><p>هر طرف گشته گریزان چکنم؟!</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>شده روی همه بی رنگ، دریغ؛</p></div>
<div class="m2"><p>شده جسم همه بیجان، چکنم؟!</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هیچ یک را نبود میل وطن</p></div>
<div class="m2"><p>در غریبی همه حیران چکنم؟!</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خاصه من، کز همه آزرده ترم</p></div>
<div class="m2"><p>نکنم گر ز غم افغان چکنم؟!</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>آورم یاد چو از خود، بهمه</p></div>
<div class="m2"><p>نکشم گر خط نسیان؛ چکنم؟!</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گشته گیتی بخلاف املم</p></div>
<div class="m2"><p>ز اولین روز الی الآن چکنم؟!</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>کان ما کان، اگر از کین گردد؛</p></div>
<div class="m2"><p>پس از این نیز کماکان چکنم؟!</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>چاره ی غصه، بود صبر؛ ولی</p></div>
<div class="m2"><p>صبر کم، غصه فراوان چکنم؟!</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هم فرو دوخت زمانه دستم</p></div>
<div class="m2"><p>بزه چاک گریبان چکنم؟!</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>هم برون ریخت ز تنگی صدفم</p></div>
<div class="m2"><p>سی و دو گوهر غلطان چکنم؟!</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>از ندامت، اگر اکنون خواهم</p></div>
<div class="m2"><p>گیرم انگشت بدندان چکنم؟!</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>تاجرم، لیک متاعی که مراست</p></div>
<div class="m2"><p>بودش روی بنقصان چکنم؟!</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>طمع محتسبم، نیز نداد؛</p></div>
<div class="m2"><p>رخصت بستن دکان چکنم؟!</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>من، تنک مایه و، کالا کاسد؛</p></div>
<div class="m2"><p>نفروشم اگر ارزان چکنم؟!</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بیژنم در چه توران و، ز من</p></div>
<div class="m2"><p>بیخبر خسرو ایران، چکنم؟!</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>زال چرخم، چو منیژه ندهد،</p></div>
<div class="m2"><p>جرعه ی آب و لب نان چکنم؟!</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>ور دهد، هم، چو نگیرد دستم؛</p></div>
<div class="m2"><p>خاتم رستم دستان چکنم؟!</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بر من، ابنای زمان در رشکند؛</p></div>
<div class="m2"><p>یوسفم، لیک به اخوان چکنم؟!</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>در شکست دل من پنداری</p></div>
<div class="m2"><p>بسته با هم همه پیمان چکنم؟!</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>زال گیتی، چو زلیخای من است</p></div>
<div class="m2"><p>دعوی پاکی دامان چکنم؟!</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>دامن، از لوث گناهم پاک است؛</p></div>
<div class="m2"><p>تهمتم برده بزندان چکنم؟!</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خاک غربت، شده دامنگیرم؛</p></div>
<div class="m2"><p>مصر دور است ز کنعان چکنم؟!</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>گرچه، در مصر غریبی دارم</p></div>
<div class="m2"><p>عزت از لطف عزیزان، چکنم؟!</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نیست فیض وطن اندر غربت</p></div>
<div class="m2"><p>در قفس ذوق گلستان چکنم؟!</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>حاش لله، همه جا ملک خداست؛</p></div>
<div class="m2"><p>از خیال وطن، افغان چکنم؟!</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>همچو خاقانی اگر تیره بود</p></div>
<div class="m2"><p>کوکبم، شکوه ز خاقان چکنم؟!</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>گیله، کافتاده صفاهان ز صفا</p></div>
<div class="m2"><p>یا همه شر شده شروان چکنم؟!</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>حاش لله، همه کس بنده ی اوست؛</p></div>
<div class="m2"><p>بنده ام، شکوه ز سلطان چکنم؟!</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>همه را، گوش بفرمان وی است</p></div>
<div class="m2"><p>چاره جز بردن فرمان چکنم؟!</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>قسمتم برد بمیخانه و زد</p></div>
<div class="m2"><p>زاهدم طعنه ی خذلان چکنم؟!</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>روزی خود، بجهان خورد آدم؛</p></div>
<div class="m2"><p>نامزد گشت بعصیان چکنم؟!</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>نه غلط، وسوسه ی شیطانی</p></div>
<div class="m2"><p>بردش از روضه ی رضوان چکنم؟!</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بمن دلشده، هم کآدمیم؛</p></div>
<div class="m2"><p>سلطنت یافته شیطان چکنم؟!</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>اختر دل سیهم، نور نداد؛</p></div>
<div class="m2"><p>نشد این گبر مسلمان چکنم؟!</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>سر طاعت، چو بخاکم نرسید</p></div>
<div class="m2"><p>چون عجایز، مژه گریان چکنم؟!</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>بیعمل، گر چه ندارد سودی؛</p></div>
<div class="m2"><p>تخم ناکاشته، باران چکنم؟!</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>دیر شد، وعده بمنحر چه روم؟!</p></div>
<div class="m2"><p>پیر شد اضحیه، قربان چکنم؟!</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>گاه پیری، ز جوانیم چه حظ؟!</p></div>
<div class="m2"><p>در خزان عیش بهاران چکنم؟!</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>گل جانپرور فروردینی</p></div>
<div class="m2"><p>نتوان چید در آبان چکنم؟!</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چون سکندر، اگر از آب حیوة</p></div>
<div class="m2"><p>بی نصیبم؛ مژه گریان چکنم؟!</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>قسمت این بود، که تنها خورد آب؛</p></div>
<div class="m2"><p>خضر از چشمه ی حیوان چکنم؟!</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>قسمت رزق چو شد روز ازل</p></div>
<div class="m2"><p>طلب بیش و کم آن چکنم؟!</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>توشه با خست منعم چه برم؟!</p></div>
<div class="m2"><p>خوشه با منت دهقان چکنم؟!</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>گوهر از آز بمخزن چه کشم؟!</p></div>
<div class="m2"><p>دانه از حرص، در انبان چکنم؟!</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>هدیه، از کیسه ی مفلس، چه خورم؟!</p></div>
<div class="m2"><p>گدیه، از کاسه ی سلطان چکنم؟!</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چون عسس، حلقه بهر در چه زنم؟!</p></div>
<div class="m2"><p>چون مگس سجده بهر خوان چکنم؟!</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>شب بشب، روز بروزم ز فلک</p></div>
<div class="m2"><p>می رسد مایده، کفران چکنم؟!</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>من که، با خون جگر ساخته ام؛</p></div>
<div class="m2"><p>هوس نعمت الوان چکنم؟!</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>جستن چاره، ز بیچاره خطاست؛</p></div>
<div class="m2"><p>از گدایان طمع نام چکنم؟!</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>من که آسودگی جان طلبم</p></div>
<div class="m2"><p>طلب خدمت سلطان چکنم؟!</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>گر سنمار شدستم، باشد؛</p></div>
<div class="m2"><p>زهر در نعم نعمان چکنم؟!</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>تن برهنه، شکمم گرسنه به؛</p></div>
<div class="m2"><p>که برم منت دونان چکنم؟!</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>لیک بینم اگر آزرده دلی</p></div>
<div class="m2"><p>گرسنه مانده و عریان چکنم؟!</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>کرد، با جود جبلی، فقرم</p></div>
<div class="m2"><p>دست چون کوته از احسان، چکنم؟!</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>چه غم از دست تهی، لیک ببخل</p></div>
<div class="m2"><p>زندم خصم چو بهتان چکنم؟!</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>آگه از راز سپهرم، از جهل،</p></div>
<div class="m2"><p>داندم خصم چو نادان چکنم؟!</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>گویم اسرار جهان، لیک چه سود؟!</p></div>
<div class="m2"><p>دهدم نسبت هذیان چکنم؟!</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>سخن من که رسیده است بعرش</p></div>
<div class="m2"><p>نرسد چون بسخندان چکنم؟!</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>نیست مداح کم از خاقانی</p></div>
<div class="m2"><p>نیست ممدوح چو خاقان چکنم؟!</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>نگنم گر پی آسایش دل</p></div>
<div class="m2"><p>بلبل طبع غزلخوان چکنم؟!</p></div></div>