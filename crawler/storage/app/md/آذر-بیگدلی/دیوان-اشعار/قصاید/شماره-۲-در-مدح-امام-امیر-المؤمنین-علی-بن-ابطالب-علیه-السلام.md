---
title: >-
    شمارهٔ ۲ - در مدح امام امیر المؤمنین علی بن ابطالب علیه السلام
---
# شمارهٔ ۲ - در مدح امام امیر المؤمنین علی بن ابطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای سوده بر در تو جبین مه، سر آفتاب؛</p></div>
<div class="m2"><p>ناز تو هم بماه رسد هم بر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صحن باغ سروی و، برطرف بام ماه؛</p></div>
<div class="m2"><p>در انجمن چراغی و، در منظر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان خط نشانه یی است، بهر شهر غالیه ؛</p></div>
<div class="m2"><p>زان رخ نمونه یی است، بهر کشور آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نسبت رخ تو، که ماهی است مهروش</p></div>
<div class="m2"><p>شد نوربخش ماه و ضیا گستر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرش است بر در تو رخ ماه طلعتان</p></div>
<div class="m2"><p>یا ریخته است بر سر یکدیگر آفتاب؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط نیست آنکه رسته بگرد دو رخ تو را</p></div>
<div class="m2"><p>ای بار سرو قد تو ماه و، بر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنبل ز گل دمیده و ریحان ز یاسمن</p></div>
<div class="m2"><p>در مشک، مه نهان شده، در عنبر آفتاب!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو مست حسنی و، شب و روزت دو ساقیند؛</p></div>
<div class="m2"><p>در بزم نام این مه و آن دیگر آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر شراب ناب و می صافشان بکف</p></div>
<div class="m2"><p>از سیم کاسه ماه و، ز زر ساغر آفتاب!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نیست در دلش ز تو آتش نشسته، چیست</p></div>
<div class="m2"><p>در گلخن سپهر بخاکستر آفتاب؟!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دندان تابناک و رخسار چون مهت</p></div>
<div class="m2"><p>این بر ستاره خنده زند، آن بر آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رویت که گلشنی و عذارت که دفتری است</p></div>
<div class="m2"><p>از صنع ایزد، ای چو مهت چاکر آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک برگ ضایع است از آن گلشن ارغوان</p></div>
<div class="m2"><p>یک فرد باطل است از آن دفتر آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر جا که ماه روی تو طالع شود، بود</p></div>
<div class="m2"><p>با آن همه ضیا ز سها کمتر آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو آفتاب برج جمالی و طلعتت</p></div>
<div class="m2"><p>از بسکه زد طپانچه ی غیرت بر آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبود عجب، که سرزند از چشمه ی سپهر</p></div>
<div class="m2"><p>با چهره ی کبود، چو نیلوفر آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنده قبا، فگنده کله بر سریر ناز؛</p></div>
<div class="m2"><p>بیند شبت اگر مه و، روزت گر آفتاب!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میگیردت چو هاله در آغوش خویش ماه</p></div>
<div class="m2"><p>میگرددت چو ذره بگرد سر آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روزی رخ تو دیدم و اکنون باین امید</p></div>
<div class="m2"><p>ای از شکنج زلف تو در چنبر آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گاهی بلاله میگذرم، گه بر ارغوان،</p></div>
<div class="m2"><p>گاهی بماه مینگرم، گه بر آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زینسان که از شعاع وی دیده روشن است</p></div>
<div class="m2"><p>گویا ز روی تست فروغی در آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا عکسی اوفتاده بر آیینه ی سپهر</p></div>
<div class="m2"><p>از قبه یی که یافته زان زیور آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن زرنگار قبه که تا اوفتاده است</p></div>
<div class="m2"><p>عالم فروز پرتوی از وی بر آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در قرب و بعد وی، چه عجب آید ار بچشم</p></div>
<div class="m2"><p>چون ماه گاه فربه و، گه لاغر آفتاب؟!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یعنی خجسته سقف زر اندود منظری</p></div>
<div class="m2"><p>کافتاده زیر سایه ی آن منظر آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آرامگاه فخر زمین و زمان علی</p></div>
<div class="m2"><p>کاو را بود غلام مه و چاکر آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شاهی که گفتمی بودش فرش آستان</p></div>
<div class="m2"><p>چون خشت سیم ماه و، چو خشت زر آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>میداد این محل بخود، ار احتمال ماه؛</p></div>
<div class="m2"><p>میکرد این شرف ز خود ار باور آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای چاکر سرای تو را چاکر آفتاب</p></div>
<div class="m2"><p>وی روشنی رای تو را مظهر آفتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در جستجوی خاک درت، کآب زندگی است؛</p></div>
<div class="m2"><p>هر شب رود بغرب، چو اسکندر آفتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دارد شها دو حلقه در بارگاه تو</p></div>
<div class="m2"><p>از سیم و زر، یکیش مه و دیگر آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از بهر سجده هر شب و هر روز بر درت</p></div>
<div class="m2"><p>از باختر مه آید و از خاور آفتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر صبح، آسمان و زمین را اگر کند</p></div>
<div class="m2"><p>روشن، برآورد ز افق چون سر آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر شام، از شموع قنادیل روضه ات</p></div>
<div class="m2"><p>تابد هزار اختر و، هر اختر آفتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>افتد اگر ز روزن قصرت بماه عکس</p></div>
<div class="m2"><p>از ماه کسب نور کند دیگر آفتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در روضه ی تو، مجمره گردان کف کلیم؛</p></div>
<div class="m2"><p>ریزان ولی چو اخگر از آن مجمر آفتاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در حجره ی تو، مروحه جنبان دمت مسیح</p></div>
<div class="m2"><p>تابان، ولی از آن دم جان پرور آفتاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون لاله، آفتاب فلک سایه ی تو را</p></div>
<div class="m2"><p>جوید چنانکه جوید نیلوفر آفتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بهر ادای خطبه ی مدح تو، نه فلک؛</p></div>
<div class="m2"><p>ای در فلک تو را شده مدحتگر آفتاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه پایه، منبری است ؛ که جا از ادب گرفت</p></div>
<div class="m2"><p>بر پایه ی چهارم آن منبر آفتاب!</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دارند آشیان شب و روزت ببام قصر</p></div>
<div class="m2"><p>مرغی دو، نام این مه و آن دیگر آفتاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>لیک از فروغ روزن آن قصر نوربخش</p></div>
<div class="m2"><p>سیمین جناح شد مه و زرین پر آفتاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>طالع شده ز مشرق صلب تو اختران</p></div>
<div class="m2"><p>زان اختران، شبیر مه و شبر آفتاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر روی خادمان درت را ندیده ام</p></div>
<div class="m2"><p>ای بر در سرای تو خدمتگر آفتاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شب ز ابروی بلال، سراغم دهد هلال؛</p></div>
<div class="m2"><p>روزم نشان دهد ز رح قنبر آفتاب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دارند بسکه شرم ز طاق رواق تو</p></div>
<div class="m2"><p>ای حاجب کمینه تو را بر در آفتاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از هاله و شعاع، شب و روز افگنند</p></div>
<div class="m2"><p>بر رخ نقاب ماه و، بسر معجر آفتاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گز خصم تیره روز تو پوشد بتن زره</p></div>
<div class="m2"><p>ای با شهاب تیغ تو از یک سر آفتاب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مشکینه درع شب، که بود زرکش از نجوم؛</p></div>
<div class="m2"><p>گردد بتن قبا، چو کشد خنجر آفتاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز اصحاب کالنجوم پیمبر گه جهاد</p></div>
<div class="m2"><p>سرور تویی بتیغ، چو از اختر آفتاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>آری، بروز رزم بود هر کجا بود؛</p></div>
<div class="m2"><p>لشکر ستاره، تیغ زن لشکر آفتاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رخش تو، کش بوقت عبور از پی نثار</p></div>
<div class="m2"><p>مانند ذره ریخته در معبر آفتاب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از میخ و نعل و سم زده هر یک گه خرام</p></div>
<div class="m2"><p>صد طعنه بر ستاه و بر مه بر آفتاب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شبها، بنقش نعلش و؛ روزان بنقش سم</p></div>
<div class="m2"><p>ای سوده بر رکاب تو چون مه سر آفتاب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هم راکع است، با تن کاهیده ماه نو؛</p></div>
<div class="m2"><p>هم ساجد است، با سر بی افسر آفتاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا آفتاب روی تو را، خاک شد نقاب ؛</p></div>
<div class="m2"><p>ای روی تو منیر مه و انور آفتاب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هر صبحگاه، چاک زند بر تن آسمان؛</p></div>
<div class="m2"><p>هر شامگاه، خاک کند بر سر آفتاب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>خون شد دلم ز سیر مه و آفتاب چند</p></div>
<div class="m2"><p>گاهی بماه طعنه زنم، گه بر آفتاب؟!</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سامان نداد کار مرا ای رفیق ماه</p></div>
<div class="m2"><p>روشن نکرد روز مرا آذر آفتاب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در وصف پادشاه عرب، خسرو عجم؛</p></div>
<div class="m2"><p>کش وقت بذل، سیم مه آرد زر آفتاب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گفتم قصیده یی و، نوشتم بصفحه یی؛</p></div>
<div class="m2"><p>کاو را زد از اشعه ی خود مسطر آفتاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کردم تمام قافیه اش ز آفتاب لیک</p></div>
<div class="m2"><p>بهتر ز آفتاب سپهرش هر آفتاب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا از فروغ تربیت آن، منیر ماه</p></div>
<div class="m2"><p>وز پرتو عنایت آن، انور آفتاب!</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گردد شبم چو روز و، نشینم بکام دل؛</p></div>
<div class="m2"><p>در گوشه یی که سایه ام افتد بر آفتاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا هست از دورنگی ایام شام و صبح</p></div>
<div class="m2"><p>کتان گداز ماه و، گهر پرور آفتاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>قهرت کند بدشمن و، لطفت کند بدوست</p></div>
<div class="m2"><p>کرد آنچه با کتان مه و با گوهر آفتاب</p></div></div>