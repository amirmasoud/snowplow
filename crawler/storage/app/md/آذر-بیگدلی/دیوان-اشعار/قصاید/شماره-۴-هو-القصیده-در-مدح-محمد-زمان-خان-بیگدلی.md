---
title: >-
    شمارهٔ ۴ - هو القصیده در مدح محمد زمان خان بیگدلی
---
# شمارهٔ ۴ - هو القصیده در مدح محمد زمان خان بیگدلی

<div class="b" id="bn1"><div class="m1"><p>گفتا سحر غلام که: زین کرده یار اسب</p></div>
<div class="m2"><p>اینک ز شهر راند برون شهریار اسب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتادم از پیش، که عنان گیرمش ز ناز</p></div>
<div class="m2"><p>نگذارد افگند بسر من گذار اسب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویی بباغ و راغ کشیدی دلش که صبح</p></div>
<div class="m2"><p>چیند گل و، چراند در لاله زار اسب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا سرخوش از شراب صبوحی، کباب خواست؛</p></div>
<div class="m2"><p>در زیر زین کشید بشوق شکار اسب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا بود بار خاطرش از دوستان غمی</p></div>
<div class="m2"><p>کآورد در هوای سفر زیر بار اسب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیتاب از حکایت او شد چنان دلم</p></div>
<div class="m2"><p>کز زخم تازیانه شود بیقرار اسب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریان، ز پا فتادم و، گفتم: بگیر دست</p></div>
<div class="m2"><p>لرزان ز جای جستم و، گفتم: بیار اسب!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کردم گران رکابش و، گشتم سبک عنان؛</p></div>
<div class="m2"><p>آورد چون غلامم بی انتظار اسب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی اختیار، تا در دروازه ی هر طرف</p></div>
<div class="m2"><p>نومید می دواندم و، امیدوار اسب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر چو برد جذبه ی عشقم برون ز شهر</p></div>
<div class="m2"><p>دیدم ز دور جلوه همی داد یار اسب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با یوز و باز، از پی آهو و کبک مست</p></div>
<div class="m2"><p>در پهن دشت راندی و در کوهسار اسب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر گام، دوریم شد ازو بیشتر؛ بلی</p></div>
<div class="m2"><p>او را سمین سمند و، رهی را نزار اسب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم: عنان مست که گیرد، مگر خرد</p></div>
<div class="m2"><p>در گوش گویدش مدوان در خمار اسب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا آورد بیاد ز تمکین دلبری</p></div>
<div class="m2"><p>از هر طرف نتازد بی اختیار اسب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا رفته رفته بست رهش آب دیده ام</p></div>
<div class="m2"><p>می تاختم چو با مژه ی اشکبار اسب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا سوختش ز ناله ی من دل، عنان کشید؛</p></div>
<div class="m2"><p>تا من رسانمش ز قفا بنده وار اسب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>القصه او، بناز روان بود و من بشوق؛</p></div>
<div class="m2"><p>تا هر دو را گرفت بیکجا قرار اسب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رفتم پیاده سویش و، چون ماه از آسمان</p></div>
<div class="m2"><p>آوردمش فرود از آن راهوار اسب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسته بشاخ سرو سهی یار بارگی</p></div>
<div class="m2"><p>کرده رها رهی بلب جویبار اسب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگرفت پس صراحی و جام از کف غلام</p></div>
<div class="m2"><p>کردش نگاهبان نکند تا فرار اسب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چشمی بسوی اسبش و، چشمی بسوی می؛</p></div>
<div class="m2"><p>گر چه نیاورد بنظر میگسار اسب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با هم بروی سبزه نشستیم و هر یکی</p></div>
<div class="m2"><p>آورده در میان سخنان، د رکنار اسب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لعلی قدح، ز دست بلورین گرفتمش؛</p></div>
<div class="m2"><p>گفتم که: گفت صبح برین اندر آر اسب؟!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت: از خمار دوش نخفتم، که صبحدم ؛</p></div>
<div class="m2"><p>رانم صبوح را بیکی مرغزار اسب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فصل بهار، گشت گل و سبزه را سحر</p></div>
<div class="m2"><p>بی اختیار گشته من و، بیقرار اسب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آری رود بباغ ز بوی گل آدمی</p></div>
<div class="m2"><p>آری بمرغزار رود در بهار اسب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من هم نهاده جام لبالب ز می بکف</p></div>
<div class="m2"><p>گفتم: بگیر و هیچ بخاطر میار اسب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جام از کفم گرفت و کشید و بپای خاست</p></div>
<div class="m2"><p>رقصان چو زیر زین خداوندگار اسب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بوالفارس زمانه، زمان خان که در زمین</p></div>
<div class="m2"><p>حکمش کشد سپهر چو حکم سوار اسب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای برده تا بروز شمار از طویله ات</p></div>
<div class="m2"><p>یک یک پیادگان جهان بی شمار اسب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا پا نهاد رایض عدل تو بر رکاب</p></div>
<div class="m2"><p>از هیچ جاده پا ننهد بر کنار اسب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر پا نهد بتارک موری، چو تازیش</p></div>
<div class="m2"><p>از تازیانه ی تو خورد زخم مار اسب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در گوش و دوش قیصر رومش ببین گرت</p></div>
<div class="m2"><p>افگند نعل و غاشیه در زنگبار اسب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در روز رزم، تیغ بکف چون دلاوران؛</p></div>
<div class="m2"><p>تا زند ا زدو سو بصف کارزار اسب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ابر اجل، بخاک فشاند تگرگ مرگ؛</p></div>
<div class="m2"><p>آید ز گرد مرد برون، وز غبار اسب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از خونش آب داده ز سر تیغها شوند</p></div>
<div class="m2"><p>دانه فشان که کرده زمین را شیار اسب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تازی در آتش، ار چه سیاوش نه ای ز بس</p></div>
<div class="m2"><p>در نعل و سنگ خاره جهاند شرار اسب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گیرد ز خون خصم تو، چون تیغ برکشی؛</p></div>
<div class="m2"><p>تا زیر زین قوایم خود در نگار اسب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نوح نبی نه ای و، ز طوفان موج خون؛</p></div>
<div class="m2"><p>چون کشتی از میان کشدت بر کنار اسب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا از قفا صهیل سمند تو نشنوند</p></div>
<div class="m2"><p>پی کرده دشمنان تو پیش از فرار اسب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از خیل دشمنان تو، هر کو فرار کرد</p></div>
<div class="m2"><p>با آنکه از پیش ندوانی ز عار اسب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دستت نگشته رنجه و، رمحت ندیده خم؛</p></div>
<div class="m2"><p>بردش ز رزمگاه بدار البوار اسب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وز لشکر تو، روز وغا، کز هجوم خلق</p></div>
<div class="m2"><p>راه عبور بست بمور و بمار اسب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پرداخت هر سواره ز پورپشنگ تخت</p></div>
<div class="m2"><p>بگرفت هر پیاده ز سام سوار اسب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>اکنون که بر در تو دوانند شام و صبح</p></div>
<div class="m2"><p>گردان ز هند پیل و یلان از تتار اسب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>افگنده آسمان دورنگم ز پا، کجاست</p></div>
<div class="m2"><p>پوینده تر ز ابلق لیل و نهار اسب؟!</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ناچار، چون زیارت کوی تو بایدم</p></div>
<div class="m2"><p>خواهم شود ز دور سپهرم دچار اسب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از توسنی خنگ فلک، هم شگفت نیست؛</p></div>
<div class="m2"><p>کاین دوست را رسد ز تو ای دوستدار اسب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فرزین شد، آن پیاده ی فرزانه، کش رسید</p></div>
<div class="m2"><p>چون پیل شاه، رخ، ز تو ای شهسوار اسب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ورنه، بیک دقیقه خود از استخوان پیل؛</p></div>
<div class="m2"><p>شطرنج باز شهر تو را، شد سه چهار اسب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گر نیست اسب تازیت، آن جانور فرست؛</p></div>
<div class="m2"><p>زین کرده، کش نژاد بود خر تبار اسب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>داری چو جام بر کف و افسر بسر، مبخش</p></div>
<div class="m2"><p>نه بی لجام استر و نه بی فسار اسب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چون نیست پیکری که نپوشیده خلعتت</p></div>
<div class="m2"><p>مفرست ناکشیده بزین زینهار اسب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>کردم روان یکی رمه، یک اسب خواستم؛</p></div>
<div class="m2"><p>غافل مشو که ناید ازین به بکار اسب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دادی گرم تو آن رمه و، اسب خواستی</p></div>
<div class="m2"><p>تا از منت بود بنظر یادگار اسب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>میدادمت بهای یکی اسب زان رمه</p></div>
<div class="m2"><p>گر در طویله داشتمی صد هزار اسب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کاری مکن که پیک سوارم پیاده باز</p></div>
<div class="m2"><p>آید فغان کنان که توقع مدار اسب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کاکنون بتحفه شعر تو بردم، باین امید</p></div>
<div class="m2"><p>کآرم ز خیل خان عدالت شعار اسب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>امروز هم نکرد چو دی و پریر لطف</p></div>
<div class="m2"><p>امسال هم نداد چو پیرار و پار اسب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>دانی از آن قبیله ام ای خان که میکند</p></div>
<div class="m2"><p>از نسبت سواری ما افتخار اسب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>هم آشیان بازم و، از ناخنان کج؛</p></div>
<div class="m2"><p>دارم بکف حسام و ز پر بر کنار اسب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از چنگ من، اگر بفلک رفته خصم من</p></div>
<div class="m2"><p>روزی که زین کنند پی گیرودار اسب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هم بگذرانم از سر این هفت، مرد تیغ</p></div>
<div class="m2"><p>هم بر جهانم از سر این نه حصار اسب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>لیکن ازین چه سود، که مانده است شصت سال</p></div>
<div class="m2"><p>هم در غلاف تیغم، هم در چدار اسب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون در غلاف زنگ نگیرد ز ننگ تیغ؟!</p></div>
<div class="m2"><p>چون در چدار لنگ نگردد ز عار اسب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هر سو بکف گرفته، یکی سر تراش تیغ؛</p></div>
<div class="m2"><p>هر سو بزین کشیده یکی خرسوار اسب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مقصود ازین قصیده ی رنگینم، اسب نیست؛</p></div>
<div class="m2"><p>دانی بهای نغمه نگیرد هزار اسب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دوشم ولی سواره حریفی ردیف شد</p></div>
<div class="m2"><p>گوینده او، خموش من و، ره سپار اسب!</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گفت: این قصیده گفت کمال و ز طبع شوخ</p></div>
<div class="m2"><p>کردش ردیف سر بسر آن نامدار اسب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از بستن هر اسب، چنان کو شکفته شد؛</p></div>
<div class="m2"><p>سنجر نشد شکفته ز فتح هزار اسب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>پنداشت هیچ اسب بگردش نمی رسد</p></div>
<div class="m2"><p>تنها دوانده گویی در روزگار اسب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>امروز، چون کمیت سخن را تو رایضی</p></div>
<div class="m2"><p>بر جای تا نشانیش، از جا برآر اسب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نشناختم، اگر چه چپ از راست، تاختم</p></div>
<div class="m2"><p>چندی بامتحان یمین و یسار اسب!</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آخر گذشت ز اسب کمال، اسب من به پل</p></div>
<div class="m2"><p>چون برد پیش رستم از اسفندیار اسب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تا میخورند شیر غزال و، غزال شیر؛</p></div>
<div class="m2"><p>تا میبرند اسب سوار و، سوار اسب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در کام دشمنانت، بود زهر مار شیر</p></div>
<div class="m2"><p>در دست دوستانت، بود پایدار اسب</p></div></div>