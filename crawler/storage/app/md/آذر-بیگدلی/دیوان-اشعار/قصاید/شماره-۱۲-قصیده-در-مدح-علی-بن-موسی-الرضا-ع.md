---
title: >-
    شمارهٔ ۱۲ - قصیده در مدح علی بن موسی الرضا (ع)
---
# شمارهٔ ۱۲ - قصیده در مدح علی بن موسی الرضا (ع)

<div class="b" id="bn1"><div class="m1"><p>ای باد شمالت چو گل آورده ببر بر</p></div>
<div class="m2"><p>لرزان ز نهالت دل هر برگ ببر بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غیرت دندانت و، از خجلت رویت؛</p></div>
<div class="m2"><p>لؤلؤست ببحر اندر و، لاله است ببر بر!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از درج درت، طعنه زند لعل بیاقوت؛</p></div>
<div class="m2"><p>وز برگ گلت، خنده زند گل بشکر بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد ایزدت از لطف، یکی حقه ی یاقوت؛</p></div>
<div class="m2"><p>انباشته آن حقه بسی و دو گهر بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چشم منت ماند از آن درج گهر دور؛</p></div>
<div class="m2"><p>عمدا ز دی از لعل ترش قفل بدربر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال تو، برخ، خرده ی عودی است بر آتش؛</p></div>
<div class="m2"><p>هر ذره اش آمیخته گویی بشرربر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط سیهت، خاسته دودی است؛ که بنشست</p></div>
<div class="m2"><p>از سوختن عود قماری بقمر بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلفت که سراسیمه بپای تو سرافگند</p></div>
<div class="m2"><p>خونخواری چشمان تو بودش بنظر بر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنگی بچه را ماند، کز فتنه ی ترکان</p></div>
<div class="m2"><p>سرگشته فتاده است بکوه و بکمر بر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بر حجری، بوسه زنند از حرم احرار؛</p></div>
<div class="m2"><p>هر ساله گزینند سفر را بحضر بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عشق تو بت، چون بحرم برد جنونم</p></div>
<div class="m2"><p>زان پیش که دستم زندش حلقه بدر بر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طفلانش، بمن بسته سر ره بپذیره؛</p></div>
<div class="m2"><p>از هر طرف انباشته حجرم بحجر بر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهری بمن، از دوستیت، دشمن جانند؛</p></div>
<div class="m2"><p>لیک از نسق شرع ز قتلم بحذر بر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و امروز که شد مفتی شهرم ز رقیبان</p></div>
<div class="m2"><p>دانم که دهد فتوی خونم بهدر بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>القصه، اگر عاشقی این است، عجب نیست؛</p></div>
<div class="m2"><p>هر روز گر فتاریم از بد به بتر بر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با هر که کنم ز اهل جهان شکوه چنان است</p></div>
<div class="m2"><p>کافسانه ی خود عرضه دهد گنگ بکر بر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بالجمله، چه تدبیر بناسازی گردون؟!</p></div>
<div class="m2"><p>جرم فلک از جا نتوان برد بجر بر!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمد مه آزار و، بخانه تو دل آزار؛</p></div>
<div class="m2"><p>تا کی بود آخر ز تو خاطر بخطر بر؟!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حیف است تو را پرده، چو گل، خاصه درین فصل؛</p></div>
<div class="m2"><p>کز پرده برآمد، گل و نسرین باثر بر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرخ و چمن، از انجم و ازهار، شب و روز؛</p></div>
<div class="m2"><p>گردیده مرصع بدراریث و درر بر!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گل مانده بگلزار، چه در شهر نمانده است؛</p></div>
<div class="m2"><p>یک تن که ز عشرت زندش چون تو بسر بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از مرد و زن، القصه بکاشانه کسی را</p></div>
<div class="m2"><p>مشغول ندانم نه بخواب و نه بخور بر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از شاه و گدا، پیر و جوان، هر که خردمند</p></div>
<div class="m2"><p>گلگشت چمن را، زده دامن بکمر بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیوانه هم، امروز بویرانه نماند؛</p></div>
<div class="m2"><p>در خانه چه مانیم چو عاصی بسقر بر؟!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بشتاب، که تا سال دگر گل بگلستان</p></div>
<div class="m2"><p>ناید نه بزاری، نه بزور و، نه بزر بر!</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ور زآنکه خمارت نگذارد که گذاری</p></div>
<div class="m2"><p>گامی دو درین فصل خوش از شهر بدر بر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خوشتر ز بهشت است درین کوچه یکی باغ</p></div>
<div class="m2"><p>کافتاده ز گل آتش طورش بشجر بر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر هر سر شاخ آمده مشغول مناجات</p></div>
<div class="m2"><p>مرغانش چو موسی همه شب تا بسحر بر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلبل بسر شاخ، ز داوود و سلیمانش؛</p></div>
<div class="m2"><p>آواز بمنقار برد، نامه بپر بر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر آستی مریم شاخ است، دمان باد</p></div>
<div class="m2"><p>کز میوه کشد عیسی شش ماهه ببر بر!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر رسته، ز سرتاسر هر شاخ، کنون برگ</p></div>
<div class="m2"><p>هر برگ بگل حامله، هر گل بثمر بر!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چندانش هوا معتدل و، آب گوارا؛</p></div>
<div class="m2"><p>کز لطف دهد جان بمدارا بمدر بر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر برگ ترش، عمر ابد یافته گویی؛</p></div>
<div class="m2"><p>نه جرعه ی خود ریخته خضرش بشمر بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از تربیت نامیه، هر سبزه ی نوخیز</p></div>
<div class="m2"><p>هر فاخته را آمده سروی بنظر بر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از فیض هوا، در همه آن باغ ندارند؛</p></div>
<div class="m2"><p>سرو و گل نوخاسته حاجت بمطر بر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از زمزمه ی بلبل و، از شعشعه ی گل؛</p></div>
<div class="m2"><p>شادند کر و کور، بسمع و ببصر بر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شب باز کند باد در باغ از آن پیش</p></div>
<div class="m2"><p>کز خنده شود باز لب گل بسحر بر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه روضه ی خلد است و اگر بگذری آنجا</p></div>
<div class="m2"><p>رضوان نگذارد که زنی حلقه بدر بر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اکنون تو و آن باغ، که در سایه ی سروی</p></div>
<div class="m2"><p>ریزی گل تر گاه بسر، گاه ببر بر!</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یادآوری از سوز دل خسته ی آذر</p></div>
<div class="m2"><p>هر لاله که بینی ز تو داغش بجگر بر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من بر در باغ آمده، بر خاک نهم سر</p></div>
<div class="m2"><p>وز بیم فراق تو کنم، خاک بسر بر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس خوانمت این تازه قصیده ز معزی:</p></div>
<div class="m2"><p>که «ای تازه تر از برگ گل تازه ببر بر»</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر بلبل طبعم، کند آهنگ ترنم؛</p></div>
<div class="m2"><p>گویم که: ازین نغمه بگلزار دگر بر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جایی که دهد عرض هنر میر معزی</p></div>
<div class="m2"><p>خود را چه بری عرض، باظهار هنر بر؟!</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>او را، سخن آویزه ی عرش آمد و ؛ نتوان</p></div>
<div class="m2"><p>با معجزه دم زد بخیالات و فکر بر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نازند بشیرین سخنش، اهل سمرقند؛</p></div>
<div class="m2"><p>چون نازش خوبان سپاهان به شکر بر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رام است مرا نیز کمیت قلم، اما</p></div>
<div class="m2"><p>دجال چو عیسی نزند تکیه بخر بر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بنهاد معزی رخ اگر بر در سنجر</p></div>
<div class="m2"><p>یا شاعر دیگر، بدر شاه دگر بر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من پای نهم بر سر والی ولایات</p></div>
<div class="m2"><p>گر شاه ولایت نهدم پای بسر بر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سلطان خراسان، علی موسی جعفر؛</p></div>
<div class="m2"><p>کارش بقضا جاری و حکمش بقدر بر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یعنی، ولی خالق و والی خلایق؛</p></div>
<div class="m2"><p>کامد ز ازل، همسر آبا بگهر بر!</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن سرور هشتم، زده و دو سر و سرور،</p></div>
<div class="m2"><p>کافگنده چو سروم، همگی سایه بسر بر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>راضی بقضا جانش و، صابر ببلا تن؛</p></div>
<div class="m2"><p>آغشته زبان نیز ز شکرش بشکر بر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خاک حرمش، شسته کلف ماه فلک را؛</p></div>
<div class="m2"><p>ماه علمش، بسته ره سیر بخور بر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا روح الامینش، شرف آمد بملایک؛</p></div>
<div class="m2"><p>تا بوالبشرش، فخر باصناف بشر بر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نازد بپدر هر پسری، از که و از مه؛</p></div>
<div class="m2"><p>او را پدر است آدم و، نازد به پسر بر!</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آری بزبان نام پدر آورد آدم</p></div>
<div class="m2"><p>آن را که پسر اوست، چه حاجت بپدر بر؟!</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ای چار کتاب فلکی را، تو مفسر؛</p></div>
<div class="m2"><p>بینا نه کسی جز تو بآیات و سور بر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون چار پرنده، که ز انفاس خلیلی</p></div>
<div class="m2"><p>جان یافته، آراسته تن نیز بپر بر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از روز ازل، حکم تو جاری بعناصر؛</p></div>
<div class="m2"><p>و آمیخته از حکمتشان، یک بدگر بر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>و امروز همت دست بر اوضاع موالید</p></div>
<div class="m2"><p>چون خامه ی نقاش، به اشکال و صور بر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از رفعت و شان، ماهچه ی رایت قدرت؛</p></div>
<div class="m2"><p>ماهیش بزیر اندر و ما، ماهش بزبر بر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چون خاست ز کر و فر گردون ز جهان گرد</p></div>
<div class="m2"><p>و افتاد از آن لرزه به تیر و به تبر بر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جاه تو شها، رو بهر آورد گه آورد؛</p></div>
<div class="m2"><p>از معرکه برگشت بفتح و بظفر بر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در معرکه بدخواه تو، کش روی سیه باد؛</p></div>
<div class="m2"><p>از شرم تو گر روی بپوشد بسپر بر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آهت بفلک چرخ سیاهی است که بسته است</p></div>
<div class="m2"><p>صیاد اجل نامه ی فتحش بسه پر بر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نشنیده کسی، شیر شود ضامن آهو؛</p></div>
<div class="m2"><p>غیر از تو که صیاد چو دیدیش بسر بر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ضامن شدی از رحمش و، تا رفت بخدمت؛</p></div>
<div class="m2"><p>بازآمد و، آهو بره بودش باثر بر!</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از جود تو، بر دشمن و بر دوست رسد فیض؛</p></div>
<div class="m2"><p>چون ابر ببارد، چه بخشک و چه بتر بر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نشنیده کسی لا ز زبانت مگر آن دم</p></div>
<div class="m2"><p>کاری پی تهلیل دو لب یک بدگر بر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر راحت روح آمده بنت العنب، اما</p></div>
<div class="m2"><p>عناب چو داد از عنبت تن بضرر بر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز آنگونه خود از دختر رز مهر بریدم</p></div>
<div class="m2"><p>کآبستنی تاک نخواهم بثمر بر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تا مهر، خرامد بسرای حمل از حوت؛</p></div>
<div class="m2"><p>تا ماه شتابد ز محرم بصفر بر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از مهر رخت، دوستت آرد به جنان گل</p></div>
<div class="m2"><p>وز کینه کشد دشمنت آتش به سقر بر</p></div></div>