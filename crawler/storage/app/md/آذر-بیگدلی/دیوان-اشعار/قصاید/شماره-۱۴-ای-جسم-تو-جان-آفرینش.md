---
title: >-
    شمارهٔ ۱۴ - ای جسم تو، جان آفرینش
---
# شمارهٔ ۱۴ - ای جسم تو، جان آفرینش

<div class="b" id="bn1"><div class="m1"><p>ای جسم تو، جان آفرینش؛</p></div>
<div class="m2"><p>جان تو، جهان آفرینش!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کرده بعالم آشکارا</p></div>
<div class="m2"><p>نام تو نشان آفرینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای گشته بلند در زمانه</p></div>
<div class="m2"><p>از شان تو شان آفرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیدا شد، ز آفریدن تو</p></div>
<div class="m2"><p>بس راز نهان آفرینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چون تو مکینی، از جلالت؛</p></div>
<div class="m2"><p>تنگ است مکان آفرینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مثل تو ندیده در جهان کس</p></div>
<div class="m2"><p>از بدو زمان آفرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو نشکفت و، نشکفد نیز</p></div>
<div class="m2"><p>گل از بستان آفرینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آغاز بهار باغ ایجاد</p></div>
<div class="m2"><p>تا فصل خزان آفرینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقشی ز شمایل تو خوشتر</p></div>
<div class="m2"><p>نابسته بنان آفرینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آراسته داشت روی یوسف</p></div>
<div class="m2"><p>روزی دودکان آفرینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امروز، بآفرین حسنت</p></div>
<div class="m2"><p>گویاست زبان آفرینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در شکر چراغ رویت امروز</p></div>
<div class="m2"><p>کافروخت شبان آفرینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روشن شده شمعها درین دیر</p></div>
<div class="m2"><p>از پیر مغان آفرینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از تیر قلم فگنده یی خم</p></div>
<div class="m2"><p>بر پشت کمان آفرینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سر برزده خامه ات ز یک شاخ</p></div>
<div class="m2"><p>با چوب شبان آفرینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گردت گله وار، خلق عالم؛</p></div>
<div class="m2"><p>روزی خور خوان آفرینش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در رزم تو آسمان گذارد</p></div>
<div class="m2"><p>از دست عنان آفرینش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیغت چو زند زبانه، افتد؛</p></div>
<div class="m2"><p>از کار زبان آفرینش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از سهم تو، بر فلک رساند؛</p></div>
<div class="m2"><p>خصم تو فغان آفرینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم آباد است، و هم خراب است؛</p></div>
<div class="m2"><p>ای از تو توان آفرینش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از خلق تو، بوستان ایجاد</p></div>
<div class="m2"><p>از جود تو، کان آفرینش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رای تو و بخت تو گزیدم</p></div>
<div class="m2"><p>از پیر و جوان آفرینش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا تو بمیانه آمدستی</p></div>
<div class="m2"><p>پیدا نه کران آفرینش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حمدا لله ثم حمدا</p></div>
<div class="m2"><p>بر رنج کشان آفرینش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از سودای محبت تو</p></div>
<div class="m2"><p>شد سود زیان آفرینش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای در شرح بلند قدریت</p></div>
<div class="m2"><p>کوتاه بیان آفرینش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از انوری این قصیده کش گفت</p></div>
<div class="m2"><p>بیرون ز گمان آفرینش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خواندی و، بچشم مینمودت؛</p></div>
<div class="m2"><p>دشوار بسان آفرینش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من نیز، گلی دو دسته بستم</p></div>
<div class="m2"><p>از لاله ستان آفرینش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا درنگری بچشم انصاف</p></div>
<div class="m2"><p>ای قاعده دان آفرینش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نی نی، غلط است آنچه گفتم؛</p></div>
<div class="m2"><p>فرق است میان آفرینش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>او از قند و کلیچه شیرین</p></div>
<div class="m2"><p>کرده است دهان آفرینش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من تره و نان جو نهادم</p></div>
<div class="m2"><p>بر گوشه ی خوان آفرینش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز استعداد است آنچه دادند</p></div>
<div class="m2"><p>ای مسأله خوان آفرینش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این استعداد تا که بخشند</p></div>
<div class="m2"><p>اکنون، نه در آن آفرینش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آذر کوتاه کن زبان، باش</p></div>
<div class="m2"><p>چون من حیران آفرینش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لال آمده روستایی عقل</p></div>
<div class="m2"><p>در شهرستان آفرینش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بادا تا باد، خرم این باغ؛</p></div>
<div class="m2"><p>از رطل گران آفرینش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون گل، بر روی دستانت</p></div>
<div class="m2"><p>در خنده دهان آفرینش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون ابر، بروز دشمنانت</p></div>
<div class="m2"><p>در گریه روان آفرینش</p></div></div>