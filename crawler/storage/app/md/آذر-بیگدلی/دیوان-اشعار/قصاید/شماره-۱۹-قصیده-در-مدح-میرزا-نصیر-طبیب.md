---
title: >-
    شمارهٔ ۱۹ - قصیده در مدح میرزا نصیر طبیب
---
# شمارهٔ ۱۹ - قصیده در مدح میرزا نصیر طبیب

<div class="b" id="bn1"><div class="m1"><p>من کیستم، آن درد کش صاف ضمیرم؛</p></div>
<div class="m2"><p>کز لای خم و رشحه ی خمر است خمیرم!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جرگه ی ارباب هنر، نیست مثالم؛</p></div>
<div class="m2"><p>در حلقه ی اصحاب نظر، نیست نظیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرقه ی مجنون که بود پوست، پلاسم؛</p></div>
<div class="m2"><p>در پیکر لیلی که سمن بوست، حریرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد سحرم، در چمن افتاده عبورم؛</p></div>
<div class="m2"><p>گلبن بسر، از بوی گل افشانده عبیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر کار گل افتد چو گره، باد شمالم؛</p></div>
<div class="m2"><p>گیرد چو رخ لاله غبار، ابر مطیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غالیه منت نکشم، زلف ایازم؛</p></div>
<div class="m2"><p>از ماشطه خجلت نبرم، بدر منیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برد و برد اندوه ز دل، رویم و رایم؛</p></div>
<div class="m2"><p>روزی که جوان بودم و، امروز که پیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم، مجمره ی آتش موسی است، چراغم؛</p></div>
<div class="m2"><p>هم، مروحه ی نفحه ی عیسی است، ضمیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم، هم سفر کشتی نوح است روانم؛</p></div>
<div class="m2"><p>هم، همنفس بلبل روح است صفیرم!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دیده که پاک است، در آن دیده لطیفم؛</p></div>
<div class="m2"><p>هر چشم که تنگ است، در آن چشم حقیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سامعه ی بی ادبان، شیون و شینم؛</p></div>
<div class="m2"><p>در ذائقه ی خشک لبان، شکر و شیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیواسطه ظالم کش و، مظلوم رهانم؛</p></div>
<div class="m2"><p>تیغ کف سلطان، قلم دست وزیرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون پشه ضعیفم، ولی آزاد ز پیلم؛</p></div>
<div class="m2"><p>چون مور نحیفم، ولی آسوده ز شیرم!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محتاجم و، در کشور فقرم متنعم؛</p></div>
<div class="m2"><p>درویشم و، در مملکت صبر امیرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از رنج، گشاده است بدل راه سرورم؛</p></div>
<div class="m2"><p>چون گنج، فتاده است بویرانه سریرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر سر نفتاده است، گرانی ز کلاهم؛</p></div>
<div class="m2"><p>بر تن نرسیده است، حرارت ز حریرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوشتر بود از تاج، بسر سایه ی فقرم ؛</p></div>
<div class="m2"><p>بهتر بود از تخت، بتن نقش حصیرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر دامن ظالم، نزنم دست تظلم؛</p></div>
<div class="m2"><p>او گرچه غنی باشد و، من گر چه فقیرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من باز سفیدم، چه غم از زاغ سیاهم؟!</p></div>
<div class="m2"><p>من شیر جوانم، چه غم از روبه پیرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>المنه لله، که تا دیده گشودم</p></div>
<div class="m2"><p>منت ز کسی نیست جز از حی قدیرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از طاعت و عصیان خدا، احمد مرسل</p></div>
<div class="m2"><p>بر جنت و بر نار بشیر است و نذیرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امروز ز کس نشنوم آواز مخالف</p></div>
<div class="m2"><p>در گوش بود زمزمه ی روز غدیرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>و آن روز، که در خاک نجف افتم و خسبم</p></div>
<div class="m2"><p>بیمی بدل از کس نه، چه منکر چه نکیرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از من، نگه عجز ندیده است و نبیند؛</p></div>
<div class="m2"><p>دشمن همه خود دوزد اگر دیده بتیرم!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاصه چو بود همدم دیرینه، معینم</p></div>
<div class="m2"><p>خاصه چو بود صاحب فرخنده نصیرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن کو چو شوم یاوه، چو خضر است دلیلم؛</p></div>
<div class="m2"><p>آن کو چو کنم شبهه، چو عقل است مشیرم!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن کو چو شود خسته دلم، اوست طبیبم؛</p></div>
<div class="m2"><p>آن کو چو رود پا بگلم، اوست ظهیرم!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای داده بهر نامه ز اشفاق نویدم؛</p></div>
<div class="m2"><p>وی کرده بهر نکته ز اسرار خبیرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یعقوبم، و بیت الحزنم دوش وطن بود</p></div>
<div class="m2"><p>نرگس چو شکوفه شده، لاله جو زریرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ناگاه، نسیم سحری نامه ات آورد</p></div>
<div class="m2"><p>نامه نه، گلی رایحه اش کرده بصیرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفتم که: ز مصر آمده این نکهت یوسف</p></div>
<div class="m2"><p>و آن نامه بود پیرهن و، باد بشیرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>داد از تو دگر قاصدم، آن تازه قصیده</p></div>
<div class="m2"><p>کز نظم وی آسوده دل از نظم جریرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از نام جریر است غرض قافیه، ور نه</p></div>
<div class="m2"><p>دانی بود آیینه ی ادراک، ضمیرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با شعر تو، شعر بلغا، شعر ندانم؛</p></div>
<div class="m2"><p>با نظم تو، نظم فصحا، یاد نگیرم!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون جغد ز بلبل نشناسم؟ که سمیعم!</p></div>
<div class="m2"><p>چون نقد زر از قلب ندانم؟! که بصیرم!!</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از فاتحه تا خاتمه اش خواندم و دیدم</p></div>
<div class="m2"><p>گویا ز فلک نامه نگار آمده تیرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من لایق این مدح نیم، چون تو نه صدرم؛</p></div>
<div class="m2"><p>من درخور این وصف نیم، چون تو نه میرم!</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>توصیف صفاهان، نه خلاف است ؛ ولی من</p></div>
<div class="m2"><p>از وی گذرم، چون نبود از تو گریزم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گلزار جنان بود صفاهان و، چو رفتی</p></div>
<div class="m2"><p>از آتش هجر تو، ز اصحاب سعیرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شیراز، کنون کز قدمت رشک بهشت است؛</p></div>
<div class="m2"><p>هر لحظه از آن باغ رسد بوی عبیرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو خود گل آن باغ و، منت بلبل نالان؛</p></div>
<div class="m2"><p>نالم، که بگوشت نرسد از چه صفیرم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فریاد، که در باغ نشد گوشزد گل؛</p></div>
<div class="m2"><p>آن ناله که در کنج قفس دارد اسیرم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای از بر من برده فلک، بیهده زودت</p></div>
<div class="m2"><p>باز آی، که شد وعده ی دیدار تو دیرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر پای خود و دست خود، از شوق دهم بوس؛</p></div>
<div class="m2"><p>کآیم بسر راهت و، دامان تو گیرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از نفحه ی مشک ختن، از خلق تو دورم؛</p></div>
<div class="m2"><p>از جرعه ی آب خضر، از جود تو سیرم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در کنج قفس، چون شنود بوی گل از باغ</p></div>
<div class="m2"><p>از زمزمه، خاموشی بلبل نپذیرم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عمری ز غمت، بر ورقی خط نکشیدم</p></div>
<div class="m2"><p>تا سوی تو آهنگ سفر کردم سفیرم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هم ریخت بلب، شفقت تو جام چو خضرم؛</p></div>
<div class="m2"><p>هم داد بکف مدحت تو، خامه چو تیرم!</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من نیز، یکی نامه نوشتن، هوسم شد؛</p></div>
<div class="m2"><p>کآگاه شوی از همه قطمیر و نقیرم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شوق آمد و بگشود ز لب قفلم، اگر چه</p></div>
<div class="m2"><p>میکرد ادب منع ازین شغل خطیرم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گفتم که: بمدح تو کنم نغمه سرایی</p></div>
<div class="m2"><p>کآید ز فلک زهره بزیر از بم و زیرم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گه بود هوای روش سیف و رشیدم</p></div>
<div class="m2"><p>گه شد هوس طرز معزی و ظهیرم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با خود همه شب زمزمه یی داشتم، آخر</p></div>
<div class="m2"><p>نالیدن عرفی، بلب آورد نفیرم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از برگ گل آورده، کنون، نامه ندیمم؛</p></div>
<div class="m2"><p>وز مشک تر آلوده، کنون، خامه دبیرم!</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هم لطف تو، با خشک لبی داد زبانم</p></div>
<div class="m2"><p>هم عفو تو، بر بی ادبی ؛ کرد دلیرم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جرم از من و، عفو از تو؛ که در عالم معنی</p></div>
<div class="m2"><p>خود شیخ کبیری تو و، من طفل صغیرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چون خامه، بسر سیر ره مدح تو کردم</p></div>
<div class="m2"><p>به زین نبود سیری و، این باد مسیرم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>حاشا ز دعای طلب وصل تو زین پس</p></div>
<div class="m2"><p>تقصیر کنم، گر نبود عمر قصیرم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بر هر چه بفرمایی و، از هر چه کنی منع</p></div>
<div class="m2"><p>بنگر که چه فرمان برو، چه منع پذیرم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>امید که تا هست و بود، عمر ندیمم؛</p></div>
<div class="m2"><p>امید که تا هست و بود، عقل مشیرم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بینم که بدامان تو آویخته دستم</p></div>
<div class="m2"><p>زان روی که با مهر تو آمیخته شیرم</p></div></div>