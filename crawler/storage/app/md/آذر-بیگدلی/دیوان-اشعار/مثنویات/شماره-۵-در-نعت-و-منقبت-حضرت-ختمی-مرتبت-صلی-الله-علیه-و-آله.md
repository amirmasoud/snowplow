---
title: >-
    شمارهٔ ۵ - در نعت و منقبت حضرت ختمی مرتبت صلی الله علیه و آله
---
# شمارهٔ ۵ - در نعت و منقبت حضرت ختمی مرتبت صلی الله علیه و آله

<div class="b" id="bn1"><div class="m1"><p>محمد که همتای او از نخست</p></div>
<div class="m2"><p>سهی سروی از خاک آدم نرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا را مطیع و جهان را مطاع</p></div>
<div class="m2"><p>زهی خواجه گز فقر بودش متاع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پسند آمد «الفقر فخری» از او</p></div>
<div class="m2"><p>که ملک سلیمان نکرد آرزو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچشم اشکریز و بلب خنده ناک</p></div>
<div class="m2"><p>بتن جان روشن، بجان نور پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل طاوها میوه ی یا و سین</p></div>
<div class="m2"><p>بهار نخستین، ترنج پسین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرفته بمکتب، نخوانده کتاب؛</p></div>
<div class="m2"><p>کتاب ملل را فگنده در آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هفتم زمین گیر، تا نه فلک</p></div>
<div class="m2"><p>بفرمان او انس و جن و ملک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی شهپر جبرئیلش بسر</p></div>
<div class="m2"><p>گهی پرده ی عنکبوتش بدر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خلق جهان کس باین پایه نه</p></div>
<div class="m2"><p>جهانیش در سایه و، سایه نه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود سایه هر کالبد را ولی</p></div>
<div class="m2"><p>نبد سایه آن کالبد را بلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مهرش دمید از زمین و زمان</p></div>
<div class="m2"><p>زمین سایه افگند بر آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تهی دست و، پر دست خلقش ز دست؛</p></div>
<div class="m2"><p>گرسنه، از آن سیر هر کس که هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بردست هر کشورش، زیر دست؛</p></div>
<div class="m2"><p>از او بت شکن هر کجا بت پرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هنوز آب در خاک آدم نبود</p></div>
<div class="m2"><p>نشانی ز هستی عالم نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که از نو رخود آفرید ایزدش</p></div>
<div class="m2"><p>نه نوری که اختر فرو ریزدش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد آن نور چون گوهر دلپسند</p></div>
<div class="m2"><p>به پیرایه ی خاتمی سر بلند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صدف یافت از صلب آدم نخست</p></div>
<div class="m2"><p>در آنجا بهر صلب کان راه جست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرافرازیش داد از همسران</p></div>
<div class="m2"><p>چه دین پروران و چه پیغمبران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چنین از فلک تا بخاک آمده</p></div>
<div class="m2"><p>در اصلاب ارحام پاک آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو نخلش دمید از ریاض عرب</p></div>
<div class="m2"><p>رطب یافت نخل عرب از طرب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برآمد چو خورشیدش از زیر میغ</p></div>
<div class="m2"><p>بدستیش تاج و بدستیش تیغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز غمگین غم، از سرکشان سرگرفت؛</p></div>
<div class="m2"><p>بدوریش داد از توانگر گرفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به بتخانه ها ز اختر واژگون</p></div>
<div class="m2"><p>فتادند از پا بتان سرنگون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شد از رایتش رایت کفر پست</p></div>
<div class="m2"><p>در افتاد بر طاق کسری شکست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز دریاچه ی ساوه گفتی سحاب</p></div>
<div class="m2"><p>بر آتشگه فارس افشاند آب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بملک عرب از عجم تاج رفت</p></div>
<div class="m2"><p>درفش فریدون بتاراج رفت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بشست آب زمزم، می از جام جم</p></div>
<div class="m2"><p>بمخموری افتاد شاه عجم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرش نامه پرویز بدخو درید</p></div>
<div class="m2"><p>همش تیغ فرزند، پهلو درید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو تابید آن مه ببام قریش</p></div>
<div class="m2"><p>قریش از وفا و جفا شد دو جیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو دارد جماد و نبات اختلاف</p></div>
<div class="m2"><p>عجب نیست در نوع انسان خلاف</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنان کز افق شاه انجم گروه</p></div>
<div class="m2"><p>درفش زر افشاند بر دشت و کوه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شد از خار و خارای نزهت زدا</p></div>
<div class="m2"><p>گل لعل گون، لعل گلگون جدا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نبی هم بتکمیل چون یافت نام</p></div>
<div class="m2"><p>تمامی از او یافت هر ناتمام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی سنگ، تسبیح گفتش بدست؛</p></div>
<div class="m2"><p>یکی سنگش، از درج گوهر شکست!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سرنگ از طبر زد، نحاس از ذهب؛</p></div>
<div class="m2"><p>جدا گشت چون حمزه از بولهب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رؤف، رحیم،کریم، کظیم</p></div>
<div class="m2"><p>که ایزد ستودش بخلق عظیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدیو جهان خواجه ی کائنات</p></div>
<div class="m2"><p>علیه السلام و علیه الصلوه</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو و انبیا یا نبی الوری</p></div>
<div class="m2"><p>فاین الثریا و این الثری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فرستنده ات از فرستادگان</p></div>
<div class="m2"><p>بپا داشته بر در استادگان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو را داده پی بهره آدم ز روح</p></div>
<div class="m2"><p>خدا ناخدایی کشتی نوح</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ذبیح و خلیل اند دل خوش ز تو</p></div>
<div class="m2"><p>بجان رسته از تیغ و آتش ز تو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر آراست در خاک بطحا خلیل</p></div>
<div class="m2"><p>سرایی بنام خدای جلیل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همانا نبودش مرادی جز این</p></div>
<div class="m2"><p>که سازی مقام ای رسول گزین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر نه، غنی بود حق ا زمکان</p></div>
<div class="m2"><p>نخواهد مکان صانع کن فکان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر آورد از طور، موسی قبس؛</p></div>
<div class="m2"><p>ز روی تو بود آن قبس مقتبس</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سلیمان کند، بیندار مشتری؛</p></div>
<div class="m2"><p>در انگشت سلمانت انگشتری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دهن شست عیسی بشهد و بشیر</p></div>
<div class="m2"><p>که شد از قدومت بشر را بشیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرفت ای ز پیغمبران سرفراز</p></div>
<div class="m2"><p>ز نام تو هر چار دفتر طراز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بچار آینه از تو افتاد نور</p></div>
<div class="m2"><p>به انجیل و تورات و فرقان، زبور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سر از تاج معراج بادت بلند</p></div>
<div class="m2"><p>ز تشریف رحمت تنت بهره مند</p></div></div>