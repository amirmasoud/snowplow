---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>ای عادلی که دیده رعایای این دیار</p></div>
<div class="m2"><p>عدل تو را معاینه، عفو تو را عیان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی بودم، آرمیده ز غوغای مرد و زن</p></div>
<div class="m2"><p>کآمد یکی به حجرهٔ داعی ز راعیان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کامروز گاوی از رمه رم کرده سوی شهر</p></div>
<div class="m2"><p>آمد مگر ندیده رعایت زراعیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی به ره ز مزرعه‌ای خورده خوشه‌ای</p></div>
<div class="m2"><p>وین قصه را رسانده به گوش تو ساعیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عدل و عفو تا چه پسند آیدت کنون</p></div>
<div class="m2"><p>غفلت راعیان و شفاعت ز داعیان</p></div></div>