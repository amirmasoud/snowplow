---
title: >-
    شمارهٔ ۸ -  قطعه
---
# شمارهٔ ۸ -  قطعه

<div class="b" id="bn1"><div class="m1"><p>کشت فرهاد را اگر خسرو</p></div>
<div class="m2"><p>خود بپاداش جان شیرین داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ز یک سنگ آب خوردستند</p></div>
<div class="m2"><p>تیغ شیرویه، تیشه ی فرهاد</p></div></div>