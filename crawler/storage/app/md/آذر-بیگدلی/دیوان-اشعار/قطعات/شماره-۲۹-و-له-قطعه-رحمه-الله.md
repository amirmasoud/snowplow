---
title: >-
    شمارهٔ ۲۹ -  و له قطعه رحمه الله
---
# شمارهٔ ۲۹ -  و له قطعه رحمه الله

<div class="b" id="bn1"><div class="m1"><p>ای خنک دی که از بهار افزون</p></div>
<div class="m2"><p>هست هنگام عیش وعیش شگرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشد از سیم ناب چون دیوار</p></div>
<div class="m2"><p>بر در خانه ی که و مه برف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننگری خود، ز خواجگان تا ناز؛</p></div>
<div class="m2"><p>نشنوی خود، ز ناصحان تا حرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دو کس از مهان روشندل</p></div>
<div class="m2"><p>با دو تن از بتان مشکین عرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز و در گوشه ی خرابی ده</p></div>
<div class="m2"><p>قدح باده غوطه در خم ژرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه چه باده؟ سهیل رنگین درع!</p></div>
<div class="m2"><p>وه چه باده؟ چراغ سیمین ظرف!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی در روی واضحات الثغر</p></div>
<div class="m2"><p>چشم در چشم «قاصرات اطرف»</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی ریحان دهد، دهند چو سیر</p></div>
<div class="m2"><p>طعم حلوا دهد، دهند چو ترف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور ز سرما نیاری، این کاری</p></div>
<div class="m2"><p>جام می بر کف و کنی می صرف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر شبه ریز، ریزه ی یاقوت؛</p></div>
<div class="m2"><p>سای بر مشک، سوده ی شنجرف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ و ماهی کباب ساز آنجا</p></div>
<div class="m2"><p>تا چرد بره سبزه از ته برف</p></div></div>