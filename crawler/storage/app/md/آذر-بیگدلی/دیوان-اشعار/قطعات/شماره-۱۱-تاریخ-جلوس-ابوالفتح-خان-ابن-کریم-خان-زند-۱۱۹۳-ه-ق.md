---
title: >-
    شمارهٔ ۱۱ - تاریخ جلوس ابوالفتح خان ابن کریم خان زند - (۱۱۹۳ ه.ق)
---
# شمارهٔ ۱۱ - تاریخ جلوس ابوالفتح خان ابن کریم خان زند - (۱۱۹۳ ه.ق)

<div class="b" id="bn1"><div class="m1"><p>چرا خون نگریم، چرا می ننوشم؟!</p></div>
<div class="m2"><p>که رفت از جهان کسری و خسرو آمد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدر رفت و، آمد پسر لوحش الله؛</p></div>
<div class="m2"><p>جهان از کرم خالی و مملو آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کریم اسم، شاه کردم پروری شد؛</p></div>
<div class="m2"><p>سخن سنج ماهی، سخا پرتو آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز گیتی ابوالنصر شاه کهن شد</p></div>
<div class="m2"><p>بعالم ابوالفتح شاه نو آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خلقش، جهان رشک باغ جنان شد</p></div>
<div class="m2"><p>ز جودش، گهر در شمار جو آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین ماتم و سور جانسوز دلکش</p></div>
<div class="m2"><p>که تاریخ را هر کسی پیرو آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقم کرد آذر کز ایوان شاهی</p></div>
<div class="m2"><p>برون رفت کاووس و کیخسرو آمد</p></div></div>