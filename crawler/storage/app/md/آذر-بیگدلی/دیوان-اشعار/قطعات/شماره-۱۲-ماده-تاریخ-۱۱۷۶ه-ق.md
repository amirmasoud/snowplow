---
title: >-
    شمارهٔ ۱۲ - ماده تاریخ (۱۱۷۶ه.ق)
---
# شمارهٔ ۱۲ - ماده تاریخ (۱۱۷۶ه.ق)

<div class="b" id="bn1"><div class="m1"><p>دریغا ز ناسازگاری گردون</p></div>
<div class="m2"><p>که ویران شد از وی سرای محمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمد که بگذشت عمرش سراسر</p></div>
<div class="m2"><p>بحمد خدا و ثنای محمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفردوس ازین خاکدان شد روانه</p></div>
<div class="m2"><p>که آنجا نهد سر بپای محمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در اندیشه بودند همصحبتانش</p></div>
<div class="m2"><p>بتاریخ سال فنای محمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقم کرد آذر الهی الهی</p></div>
<div class="m2"><p>بهشت برین، باد جای محمد</p></div></div>