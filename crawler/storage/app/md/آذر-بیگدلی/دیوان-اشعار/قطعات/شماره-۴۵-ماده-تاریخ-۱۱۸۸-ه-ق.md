---
title: >-
    شمارهٔ ۴۵ - ماده تاریخ (۱۱۸۸ ه.ق)
---
# شمارهٔ ۴۵ - ماده تاریخ (۱۱۸۸ ه.ق)

<div class="b" id="bn1"><div class="m1"><p>بفرمان دارای ایران که بودش</p></div>
<div class="m2"><p>بکف تیغ رستم، بسر تاج خسرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردم رسم، سلطان جمشید پایه؛</p></div>
<div class="m2"><p>کریم اسم، خاقان خورشید پرتو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه زند، کز نام او زنده ماند؛</p></div>
<div class="m2"><p>جهان کو بداد و دهش کرد مملو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون بندد اندر میان رشته ی در</p></div>
<div class="m2"><p>کسی کو نبودش بکف خوشه جو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی مسجد آراست از لطف ایزد</p></div>
<div class="m2"><p>به شیراز کافگند بر نه فلک ضو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه مسجد، که سودی، گر امروز بودی؛</p></div>
<div class="m2"><p>بخاکش لب جم، رخ کی، سرزو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو هست زینگونه مسجد بگیتی</p></div>
<div class="m2"><p>وگر دیگری گویدت هست، مشنو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پی ضبط تاریخ آن سال فرخ</p></div>
<div class="m2"><p>فتادند دانشوران در تک و دو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رقم کرد کلک گهر سلک آذر:</p></div>
<div class="m2"><p>«بشیراز وا شد در کعبهٔ نو»</p></div></div>