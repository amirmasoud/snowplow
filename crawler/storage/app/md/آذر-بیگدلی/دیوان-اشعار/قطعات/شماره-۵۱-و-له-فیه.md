---
title: >-
    شمارهٔ ۵۱ -  و له فیه
---
# شمارهٔ ۵۱ -  و له فیه

<div class="b" id="bn1"><div class="m1"><p>ایا خان عاقل، که زنجیر من</p></div>
<div class="m2"><p>به افسون و افسانه برداشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین پس نرنجی، چو رنجانمت</p></div>
<div class="m2"><p>که زنجیر دیوانه برداشتی</p></div></div>