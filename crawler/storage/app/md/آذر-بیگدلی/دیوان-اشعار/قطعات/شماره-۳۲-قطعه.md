---
title: >-
    شمارهٔ ۳۲ -  قطعه
---
# شمارهٔ ۳۲ -  قطعه

<div class="b" id="bn1"><div class="m1"><p>بر آستانه ی جانان، که روضه ی ارم است؛</p></div>
<div class="m2"><p>اگر تو را گذری افتد ای نسیم شمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگو: ز کوی تو رفتم، بشوق اینکه مرا</p></div>
<div class="m2"><p>چو دوستان قدمی چند آیی از دنبال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باین امید دگر باز آمدم سویت</p></div>
<div class="m2"><p>که بر سر رهم آیی و پرسیم احوال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی ز خانه تو در رفتن و در آمدنم</p></div>
<div class="m2"><p>برون نیامدی ای شمع حجله گاه جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تغافل تو همانا بمن بود مخصوص</p></div>
<div class="m2"><p>وگرنه غیر من رانده از حریم وصال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر هزار کس از شهر رفت و باز آمد</p></div>
<div class="m2"><p>که هم مشایعتش کردی و هم استقبال</p></div></div>