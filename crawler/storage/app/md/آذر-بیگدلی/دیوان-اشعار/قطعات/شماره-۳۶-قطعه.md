---
title: >-
    شمارهٔ ۳۶ -  قطعه
---
# شمارهٔ ۳۶ -  قطعه

<div class="b" id="bn1"><div class="m1"><p>دو نگاهی که کردمت همه عمر</p></div>
<div class="m2"><p>نرود تا قیامت از یادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه اولین، که دل بردی؛</p></div>
<div class="m2"><p>نگه آخرین، که جان دادم!</p></div></div>