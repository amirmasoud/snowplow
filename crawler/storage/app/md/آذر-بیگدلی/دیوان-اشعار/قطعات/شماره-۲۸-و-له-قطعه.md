---
title: >-
    شمارهٔ ۲۸ -  و له قطعه
---
# شمارهٔ ۲۸ -  و له قطعه

<div class="b" id="bn1"><div class="m1"><p>دوشم که نسود دیده بر هم</p></div>
<div class="m2"><p>از فرقت جان گزای هاتف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روز ستاره میشمردم</p></div>
<div class="m2"><p>در آرزوی لقای هاتف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل که ز روشنی گرو برد</p></div>
<div class="m2"><p>از روی ستاره رای هاتف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل هاتف، که کام جان بود؛</p></div>
<div class="m2"><p>میخواستم از خدای هاتف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگاه، خروس عرش برداشت؛</p></div>
<div class="m2"><p>این زمزمه چون ندای هاتف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کان شب خیزان، تنفس الصبح؛</p></div>
<div class="m2"><p>آمد وقت دعای هاتف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخاستم و، بسجده ی شکر؛</p></div>
<div class="m2"><p>جستم ز خدا، بقای هاتف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سر از سجده برگرفتم</p></div>
<div class="m2"><p>افتاد بسر هوای هاتف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در زاویه ی هوا فگندم</p></div>
<div class="m2"><p>دو چشم بره چو های هاتف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از طرف افق دمید ناگاه</p></div>
<div class="m2"><p>صبح از دم جان فزای هاتف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح شب تار من صباحی</p></div>
<div class="m2"><p>آمد بزبان ثنای هاتف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جی بکفش، همه لآلیش</p></div>
<div class="m2"><p>از گوهر بحر زای هاتف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون نافه خجسته نامه یی داشت</p></div>
<div class="m2"><p>از خامه ی مشکسای هاتف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مضمون، همه شکوه ی صباحی</p></div>
<div class="m2"><p>کو ناشده غمزدای هاتف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیگانگی اش، چنانکه گویا</p></div>
<div class="m2"><p>هرگز نشد آشنای هاتف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او نیز نوشته نامه یی نغز</p></div>
<div class="m2"><p>در عذر خود از برای هاتف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون بلبل بوی گل شنیده</p></div>
<div class="m2"><p>دیدم شده هم نوای هاتف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتم که: نه، حق بجانب اوست؛</p></div>
<div class="m2"><p>ای بیخبر از وفای هاتف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هاتف، چو غریب این دیار است</p></div>
<div class="m2"><p>باید جستن رضای هاتف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این نامه که او نوشته، چون نیست</p></div>
<div class="m2"><p>جز وصل تو مدعای هاتف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر عذر که در جواب گفتی</p></div>
<div class="m2"><p>بالله نبود سزای هاتف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عذری ز تو نشنود صباحی؛</p></div>
<div class="m2"><p>هر کس باشد بجای هاتف</p></div></div>