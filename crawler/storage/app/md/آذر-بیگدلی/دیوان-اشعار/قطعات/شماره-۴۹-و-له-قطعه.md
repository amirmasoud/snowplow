---
title: >-
    شمارهٔ ۴۹ -  و له قطعه
---
# شمارهٔ ۴۹ -  و له قطعه

<div class="b" id="bn1"><div class="m1"><p>ایا خان زمان، کز بیم خشمت؛</p></div>
<div class="m2"><p>کند بهرام خون آشام لاوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی اندود ایوان تو کیوان</p></div>
<div class="m2"><p>کشد از ماه نو بر دوش ناوه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خواند خطبه ی جاه تو برجیس</p></div>
<div class="m2"><p>علا گردد ز آغازش علاوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تند خورشید، از خط شعاعی</p></div>
<div class="m2"><p>برای بند شمشیرت کلاوه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو لیلی، هر شبت تا بر شبستان</p></div>
<div class="m2"><p>نهد ناهید ازین مشکین کجاوه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود کمتر دبیر مجلست تیر</p></div>
<div class="m2"><p>بچشمش گر ز خور نبود غشاوه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان کردم پیاده قاصدی دوش</p></div>
<div class="m2"><p>سپهرش گر نه باز آرد ز آوه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیامی چند از من سویت آورد</p></div>
<div class="m2"><p>طمع دارم که نشماریش یاوه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نهی گر نامه ام بر سر، عجب نیست؛</p></div>
<div class="m2"><p>که شد چتر فریدون نطع کاوه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوابی درخورش ده تا نگوید:</p></div>
<div class="m2"><p>معاذ الله من تلک القساوه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مبادا از خوی شرمش چو آید</p></div>
<div class="m2"><p>شود صحرای قم، دریای ساوه</p></div></div>