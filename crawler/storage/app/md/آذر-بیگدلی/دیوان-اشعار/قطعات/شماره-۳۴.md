---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دوش، از گردش فلک که مدام</p></div>
<div class="m2"><p>شاد غمگین کند، عزیز ذلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاست آواز پایی و گفتم:</p></div>
<div class="m2"><p>صور اول دمید اسرافیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاصدی دل سیاه و روی ترش</p></div>
<div class="m2"><p>دیدم آمد زرنگ با زنبیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوزه یی چند داشت زنبیلش</p></div>
<div class="m2"><p>ببهای خفیف و وزن ثقیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تار چون بخت بیکسان غریب</p></div>
<div class="m2"><p>تیره چون روی مفلسان معیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد، چون گوی کودکان نحیف</p></div>
<div class="m2"><p>تنگ، چون چشم خواجگان بخیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پوست بر سر همه چو سلاخان</p></div>
<div class="m2"><p>که هم از جیفه شان بود مندیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشمار در بهشت، ولی</p></div>
<div class="m2"><p>گویی افتاد از سقر قندیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داشت هر یک دوازده رخنه</p></div>
<div class="m2"><p>چون ز دست و عصای موسی، نیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه، چون داغ لاله از سودا</p></div>
<div class="m2"><p>دامن آلوده شان برب قلیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حاش لله، چه رب و چه کوزه؟!</p></div>
<div class="m2"><p>خم نیلی، در آن عصاره ی نیل!!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا ز دود سریشم ماهی</p></div>
<div class="m2"><p>خرده یی مانده در ته پاطیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا درین راه، پیک روی سیاه؛</p></div>
<div class="m2"><p>که چو ابلیس بوده در تضلیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ریخته آبروی خود در وی</p></div>
<div class="m2"><p>روی بی آبش، اینک است دلیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لیک از ضعف، معده ی کوزه</p></div>
<div class="m2"><p>گشته فاسد، نیافته تحلیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امتحان را، زدم در آن انگشت</p></div>
<div class="m2"><p>گویی از سرمه دان برآید میل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسته من لب ز خشم و، او میگفت؛</p></div>
<div class="m2"><p>زیر لب: این که را کنم تحویل؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتمش : کیستی تو، و اینها چیست؟!</p></div>
<div class="m2"><p>که پسندیده بر من این تحمیل؟!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از چه اقلیمی، از کدامین شهر؟!</p></div>
<div class="m2"><p>از چه او یماقی، از کدامین ایل؟!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفت: من قاصدم ز حضرت آن</p></div>
<div class="m2"><p>کش بود کف برزق خلق کفیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیدی از سلاله ی احمد</p></div>
<div class="m2"><p>نام او احمد از نژاد خلیل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اینک از قم که دار الایمان است</p></div>
<div class="m2"><p>او فرستادت این علی التعجیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باورم نامد، آنچه گفت از وی</p></div>
<div class="m2"><p>خورد چون رب، قسم برب جلیل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتمش: چیست باعث قلت؟</p></div>
<div class="m2"><p>گفت: گفتند: رب للتقلیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم: استغفر الله، ای ملعون!</p></div>
<div class="m2"><p>گفتم: استغفر الله، ای ضلیل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مشتغل من بمدح او، نسزد</p></div>
<div class="m2"><p>کز تو در کارم افگند تعطیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عجبا، کان حریف عهد گسل ؛</p></div>
<div class="m2"><p>با چنین ارمغانت کرده گسیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بوبد، آن، کش مشام نیست ضعیف</p></div>
<div class="m2"><p>جوید، آن کش نظر نگشت کلیل!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سمن از ساحت چمن، نه خسک</p></div>
<div class="m2"><p>نافه از آهوی ختن نه بسیل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نفرستاده یا وی این تحفه</p></div>
<div class="m2"><p>یا فرستاده کردیش تبدیل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گفت: نه؛ گفتم: این همه هزل است</p></div>
<div class="m2"><p>ز منش گوی، ای مرا تو وکیل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نیک هر کار می کند، نیک است؛</p></div>
<div class="m2"><p>کل فعل من الجمیل جمیل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا محلل، سیم مطلقه را</p></div>
<div class="m2"><p>از حلولی بشو کند تحلیل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم محبت برد، زر محلول</p></div>
<div class="m2"><p>هم عدویت شود سراحلیل</p></div></div>