---
title: >-
    شمارهٔ ۵۳ -  قطعه
---
# شمارهٔ ۵۳ -  قطعه

<div class="b" id="bn1"><div class="m1"><p>غریب آزار دزد ساوجی شد</p></div>
<div class="m2"><p>به ساوه شحنه، نه هوشی نه هنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همانا مانده زان دریا که شد خشک</p></div>
<div class="m2"><p>ز یمن مولد احمد نهنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رحم شهش گردن رهاند</p></div>
<div class="m2"><p>ز تیغ عدل، باری پالهنگی</p></div></div>