---
title: >-
    شمارهٔ ۵۵ -  و له فیه
---
# شمارهٔ ۵۵ -  و له فیه

<div class="b" id="bn1"><div class="m1"><p>جهان مکرمت ای میرزا حسین که کرده</p></div>
<div class="m2"><p>ز جان و دل فلکت بندگی سپهر غلامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه بوی تو آرد نسیم روضهٔ رضوان</p></div>
<div class="m2"><p>نمیکند بمن تنگدل مشام مشامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستمکشان جهان، از جفای چرخ ستمگر؛</p></div>
<div class="m2"><p>نمی شدند خلاص، ار نبود لطف تو حامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که، اسم تو بالای اسم جمله نویسم؛</p></div>
<div class="m2"><p>بدفتری که نویسند ز اهل جود اسامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمیرسد بمقیمان آستان تو دستم</p></div>
<div class="m2"><p>زهی رفیع جنابی، زهی بلند مقامی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوشته بودم ازین پیش نامه یی بتو ناگه</p></div>
<div class="m2"><p>رسید پیک مراد و، رساند نامه ی نامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه نامه؟ نافه ی مشک و، چه نامه؟ طبله ی عنبر!</p></div>
<div class="m2"><p>بخط فزون ز شفیعا بنظم به ز نظامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی چه سود؟ که قصد من از نوشتن نامه</p></div>
<div class="m2"><p>نبود غیر فرستادن صحیفه ی جامی!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کتاب را، نفرستادی ای حبیب من، اکنون؛</p></div>
<div class="m2"><p>میان خوف و رجا مانده ام، ز نامه ی سامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا محبت آن نسخه کرده طامع وسایل</p></div>
<div class="m2"><p>وگرنه شکر که هستم ز خاندان گرامی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طمع نبوده مرا هیچگه طریقه اگر چه</p></div>
<div class="m2"><p>طمع طریقه ی شاعر بود، ز عارف وعامی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میانه ی من و طامع، تفاوتی است که باشد</p></div>
<div class="m2"><p>میان بصری و کوفی، میان مصری وشامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین دو هفته غرض شاه خاوران چو نشیند</p></div>
<div class="m2"><p>بتختگاه حمل، جام زر بدست گرامی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دهد ز برگ، به اشجار خشک جامه ی اطلس؛</p></div>
<div class="m2"><p>بر آورد ثمر باغ را ز علت خامی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو هم نشینی و بنشانیم ببزم و، دلم خوش؛</p></div>
<div class="m2"><p>کنی بخلعتی، اما ز کهنه جامه ی جامی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه کهنه جامی؟ همان مندرس کتاب که هرگز</p></div>
<div class="m2"><p>ندزدش، فگنی گر برهگذار، حرامی!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سپاری آن صحف خاصم ار بخط مصنف</p></div>
<div class="m2"><p>سپارمت بغلامان خاص خط غلامی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازین کرم گذرد سال و ماه و روز و شب من</p></div>
<div class="m2"><p>به خدمت تو سراسر، به مدحت تو تمامی</p></div></div>