---
title: >-
    شمارهٔ ۲۷ -  در مدح آقا محمد هاشم زرگر
---
# شمارهٔ ۲۷ -  در مدح آقا محمد هاشم زرگر

<div class="b" id="bn1"><div class="m1"><p>ای آنکه کسی مثل تو ننوشته خط نسخ</p></div>
<div class="m2"><p>تا خامه ی قدرت رقم نون زده با کاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عرش خدا، روح الامین آمد و آورد</p></div>
<div class="m2"><p>قرآن که بود معجزهٔ سید اشراف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان عهد، هزار و صد هشتاد فزون است</p></div>
<div class="m2"><p>رفت و فصحای عربش آمده وصاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لطف ازلی خواست کنون معجز دیگر</p></div>
<div class="m2"><p>از کلک تو ظاهر کند ای مظهر الطاف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز، ز فضل احد و باطن احمد؛</p></div>
<div class="m2"><p>خط معجزه ی تست، در اطراف و در اکناف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناورده چو او،سوره کسی صافی و محکم؛</p></div>
<div class="m2"><p>ننوشته چو تو، آیه کسی روشن و شفاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یعنی که شد از خط تو، خط دگران نسخ؛</p></div>
<div class="m2"><p>غیر از تو کسی را نرسد با تو زند لاف!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کف، و رقت صفحه ی رویی است؛ که از خط</p></div>
<div class="m2"><p>شیرازه زند بر دل سی پاره ی صحاف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در نامه، مداد تو بود نافه ی مشکی</p></div>
<div class="m2"><p>کافشانده بکافور غزال ختن از ناف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دست توانای تو آن خامه کمانی است</p></div>
<div class="m2"><p>کآرند بکف روز جدل آرش و نداف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گز لک بکف غیر و، بدست تو حدید است؛</p></div>
<div class="m2"><p>کز کوره کشد پنجه ی سوزنگر و سیاف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد معجزه ی هاشمی، آن روز کلامی</p></div>
<div class="m2"><p>کامد بنی هاشمیش کاشف و کشاف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خط نیز بود معجزه ی هاشمی امروز</p></div>
<div class="m2"><p>کز دست تو ظاهر شد و شد شهره در اطراف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داند کسی این معجزه ها نیک که، خواند</p></div>
<div class="m2"><p>از بسمله ی فاتحه تا جزو لایلاف</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قرآن نه، بهشت است خوش؛ آن دم که چو غلمان</p></div>
<div class="m2"><p>بینم که در آن مردم چشمم شده طواف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای همدم صافی گهر، ای صاحب اخلاق؛</p></div>
<div class="m2"><p>ای از همه صافی گهران برترت اوصاف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زآن روز که زاده است تو را مادر گیتی</p></div>
<div class="m2"><p>اسلاف نگویند دگر ناخلف اخلاف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کم دیده ام از خلق جهان چون تو خلیقی</p></div>
<div class="m2"><p>گشتم چل و نه سال میان همه اصناف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عیب تو همین است، که از کس چو بچشمت</p></div>
<div class="m2"><p>حسن کمی آید، چه ز اعیان چه ز اجلاف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توصیف وی، از حد بری از فرط تسامح</p></div>
<div class="m2"><p>تحسین وی افزون کنی از غایت اجحاف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این گر چه بود از اثر صافی سینه</p></div>
<div class="m2"><p>وین گر چه بود از نظر صاف و دل صاف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ناگفته کسی مشک، شب افروز شبه را</p></div>
<div class="m2"><p>ناگفته کسی لیک یلک روز بخفاف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر چیز باندازه خوش است، این ز تو خوش نیست؛</p></div>
<div class="m2"><p>کز حسن خیاطت شمری بخیه ی اکاف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خنده، بزنگی بچه یی، نام دهی حور</p></div>
<div class="m2"><p>وز نشأه، بلای ته خم، اسم نهی صاف</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گویی که همه بیضه ی بیضا بمن آورد</p></div>
<div class="m2"><p>بینی که فتد مهره ی زرد از پر خطاف</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من شاعر و، در حرف تو خود این همه اغراق</p></div>
<div class="m2"><p>من بسته لب از حرف و، تو خود این همه حراف؟!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز اغراق تو افغان، ز سکوت دگری آه؛</p></div>
<div class="m2"><p>کز لب گه تحسین زندش آبله تا ناف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بالله که خاموشی او زین دو برون نیست</p></div>
<div class="m2"><p>من دانم و آن کو پدرش نامده از قاف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا ز ابلهیش نیست، بسر سایه ی دانش</p></div>
<div class="m2"><p>یا از حسدش نیست، بدل مایه ی انصاف</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر ز ابلهیش، راه سخن نیست، غمی نیست؛</p></div>
<div class="m2"><p>خورشید ندارد گله، از بینش خفاف</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ور بسته لبش را حسد، المنه بالله؛</p></div>
<div class="m2"><p>زر نیک شناسد محک اندر کف صراف</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خاموشی دانا، گه تحسین سخن چیست؟!</p></div>
<div class="m2"><p>ظلمی که بود شهره ز شاپور ذوالاکتاف</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>القصه، بهر راه میانه روی اولی؛</p></div>
<div class="m2"><p>شد خیر الامور اوسطها شیمه ی اسلاف</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نازش بدلیری است، نه جبن و نه تهور؛</p></div>
<div class="m2"><p>بالش بود از جود، نه از بخل و نه ز اسراف!</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هشدار، نگویی بگل تیره گل تر؛</p></div>
<div class="m2"><p>زنهار، نگویی بنمدمال قصب باف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا ز اختر تا بنده بود زیور افلاک</p></div>
<div class="m2"><p>تا گوهر رخشنده دهد زینت اصداف</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بادا بفلک اختر اقبال تو روشن</p></div>
<div class="m2"><p>بادا به زمین گوهر آمال تو شفاف</p></div></div>