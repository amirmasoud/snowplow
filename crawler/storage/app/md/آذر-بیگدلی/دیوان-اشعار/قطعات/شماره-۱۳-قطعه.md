---
title: >-
    شمارهٔ ۱۳ -  قطعه
---
# شمارهٔ ۱۳ -  قطعه

<div class="b" id="bn1"><div class="m1"><p>شنیدم ز قیصر ستمدیده‌ای</p></div>
<div class="m2"><p>چنین گفت چون قصر قیصر نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که: الحمدلله در روزگار</p></div>
<div class="m2"><p>ستمکش بماند و، ستمگر نماند!</p></div></div>