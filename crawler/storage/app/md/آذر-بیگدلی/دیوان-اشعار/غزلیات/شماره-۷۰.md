---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>آمد شب و، وقت یا رب آمد!</p></div>
<div class="m2"><p>یا رب چکنم؟ دگر شب آمد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همسایه، شنید یا ربم را</p></div>
<div class="m2"><p>از یا رب من، به یارب آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دوست بگو بکویت امشب</p></div>
<div class="m2"><p>دشمن بکدام مطلب آمد؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کز راه هزار بدگمانی</p></div>
<div class="m2"><p>جانم صدبار بر لب آمد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خال سیاه کنج چشمت</p></div>
<div class="m2"><p>امروز بچشم من، شب آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلقی بگمان، که پیشم این روز</p></div>
<div class="m2"><p>از گردش چشم کوکب آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردی دارد دلم که درمانش</p></div>
<div class="m2"><p>.......................ب آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان سوخت ز سوز عشقم آذر</p></div>
<div class="m2"><p>تو پنداری بتن تب آمد</p></div></div>