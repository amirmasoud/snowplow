---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>روز و شب، از رخ رخشنده ی شاه عجبی</p></div>
<div class="m2"><p>آفتاب عجبی دارم و ماه عجبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط چون سبزه اش، از چهره ی چون گل زده سر</p></div>
<div class="m2"><p>از زمین عجبی، رسته گیاه عجبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید آن مه گنه بیگنهی از من و، کشت؛</p></div>
<div class="m2"><p>بیگناه عجبی را، بگناه عجبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه عجب گرد هم از دست دل و دین؟ که بناز</p></div>
<div class="m2"><p>شوخ چشم عجبی، کرد نگاه عجبی!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم از طرف بناگوشی و، کنج دهنی؛</p></div>
<div class="m2"><p>خط سبز عجبی، خال سیاه عجبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لقب عاشق و معشوق، گر از من پرسند</p></div>
<div class="m2"><p>من گدای عجبی گویم و شاه عجبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنم از اشک گدازد، دلم از آه چو شمع</p></div>
<div class="m2"><p>آذر، اشک عجبی دارم و آه عجبی</p></div></div>