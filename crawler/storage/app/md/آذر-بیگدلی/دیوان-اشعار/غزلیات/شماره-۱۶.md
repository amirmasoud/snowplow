---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>از خنده چه آلوده شود لب بعتابت</p></div>
<div class="m2"><p>زهر از شکرت میچکد و، آتش از آبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قتل من بیگنه، ای شوخ بپرهیز</p></div>
<div class="m2"><p>کان نیست گناهی که نویسند ثوابت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاجب ندهد راهم و خواهم که نهانی</p></div>
<div class="m2"><p>گویم بتو حرفی و برآرم ز حجابت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر چه بویرانی دل اینهمه کوشی؟!</p></div>
<div class="m2"><p>جز دل نبود خانه یی، ای خانه خرابت!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خون اسیران، چو کشی جام و شوی مست؛</p></div>
<div class="m2"><p>جز مرغ دل سوختگان نیست کبابت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روز گذاریم بزانو سرو از غم</p></div>
<div class="m2"><p>خوابی نه شب هجر، که بینیم بخوابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آذر بحلت کرد، مگر چند توان گفت</p></div>
<div class="m2"><p>در روز حساب از غم بیرون ز حسابت؟!</p></div></div>