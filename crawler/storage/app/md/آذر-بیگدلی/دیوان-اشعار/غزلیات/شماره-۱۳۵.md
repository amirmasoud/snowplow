---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>جانان نشسته تا من از شوق جان فشانم</p></div>
<div class="m2"><p>من ایستاده تا او گوید: فشان، فشانم!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل کنم فراموش، از پرفشانی دام؛</p></div>
<div class="m2"><p>صد سال اگر پرو بال در آشیان فشانم!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محمل گذشت، و اشکم خاکی نهشت؛ تا من</p></div>
<div class="m2"><p>بر سر چو بازآید آن کاروان، فشانم!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برنایدم، گر از دست کاری؛ ولی توانم،</p></div>
<div class="m2"><p>از آستین غباری زان آستان فشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آذر، گرت بسر زر افشاند تا نشاندت</p></div>
<div class="m2"><p>برخیز تا بپایت، من نیز جان فشانم!</p></div></div>