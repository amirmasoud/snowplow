---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گفتی: که دلت از عشق پیوسته غمین بادا</p></div>
<div class="m2"><p>تا بوده چنین بوده، تا باد چنین بادا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناله ی من ای گل، آشفته مکن سنبل؛</p></div>
<div class="m2"><p>گو پرده درد بلبل، گل پرده نشین بادا!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صید دل از این وادی، دارد سر آزادی</p></div>
<div class="m2"><p>امید که صیادی، بازش بکمین بادا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اغیار همی پویند، تا پیش منت جویند؛</p></div>
<div class="m2"><p>حرفی بگمان گویند، ای کاش یقین بادا!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افزود دگر امشب، زخم دل من زان لب؛</p></div>
<div class="m2"><p>زان بیشترک یا رب، آن لب نمکین بادا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تیغ جفا بربست، صد کشته بهم پیوست؛</p></div>
<div class="m2"><p>در صید کشی آن دست، چابکتر ازین بادا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت که ز پی پوید، وصلت بدعا جوید؛</p></div>
<div class="m2"><p>هر چند که او گوید، گویم، نه چنین بادا!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دی کآن مه موزون رفت، دلخون شده در خون رفت!</p></div>
<div class="m2"><p>دین از پی دل چون رفت، جان پیرو دین بادا!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت این دل شورانگیز، ویران از توچون تبریز؛</p></div>
<div class="m2"><p>این ملک خرابت نیز، در زیر نگین بادا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاراج بدخشان گر، کرد آن لب جان پرور،</p></div>
<div class="m2"><p>آن زلف سیاه آذر غارتگر چین بادا</p></div></div>