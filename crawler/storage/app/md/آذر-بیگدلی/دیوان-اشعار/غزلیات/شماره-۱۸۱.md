---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>مرا بجرم وفای من از جفا کشتی</p></div>
<div class="m2"><p>جفا نگر، که چو دیدی ز من وفا کشتی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بآن گناه، که بیگانه را کسی بکشد</p></div>
<div class="m2"><p>تو بیوفا، همه یاران آشنا کشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشد ز قید تو مرغی رها، مگر مرغی</p></div>
<div class="m2"><p>که گر ز کنج قفس کردیش رها کشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آگه است خدا، روز حشر عذرت چیست؟!</p></div>
<div class="m2"><p>نهان ز خلق اگر امروزت از جفا کشتی؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان ز کشتنم، اکنون که زنده از جورت</p></div>
<div class="m2"><p>کسی نماند که گوید: مرا چرا کشتی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خیل بیگنهان، کس نماند در کویت</p></div>
<div class="m2"><p>به تیغ جور مرا بیگناه تا کشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان مردم عالم، بس است این طعنت</p></div>
<div class="m2"><p>که پادشاه جهان بودی و گدا کشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو شد شکار تو مرغ دلم، نمیدانم</p></div>
<div class="m2"><p>که داریش بقفس باز اسیر، یا کشتی؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه خواجه ای تو، که هر بنده را که دانستی</p></div>
<div class="m2"><p>نمیکند بتو دعوی خونبها کشتی؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا که درد نکشت، ای طبیب حیرانم؛</p></div>
<div class="m2"><p>چه دشمنی بمنت بود، کز دوا کشتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخاک پای تواش تا سپارم از یاری</p></div>
<div class="m2"><p>بگو که آذر بیچاره را کجا کشتی؟!</p></div></div>