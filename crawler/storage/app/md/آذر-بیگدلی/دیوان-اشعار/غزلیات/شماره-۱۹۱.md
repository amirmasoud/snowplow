---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه بسر رسیده باشی</p></div>
<div class="m2"><p>من مرده، تو آرمیده باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که چه دیده ام شب هجر</p></div>
<div class="m2"><p>گر روز فراق دیده باشی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جام رقیب، می ننوشی</p></div>
<div class="m2"><p>گر خون دلم چشیده باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاصد! نرسیده بر لبم جان</p></div>
<div class="m2"><p>ای کاش باو رسیده باشی!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با او دو بدو نشسته، گویی</p></div>
<div class="m2"><p>یک یک زمن آنچه دیده باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز آی که گوییم نهانی</p></div>
<div class="m2"><p>هر حرف کزو شنیده باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظلم است که، از قفس برانیش</p></div>
<div class="m2"><p>مرغی که پرش بریده باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>انگار آذر روی چون زان باغ</p></div>
<div class="m2"><p>از شاخ گلی نچیده باشی</p></div></div>