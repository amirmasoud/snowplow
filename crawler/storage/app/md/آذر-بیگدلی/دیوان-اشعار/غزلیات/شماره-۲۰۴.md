---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>کجایی یوسف ثانی، کجایی؟!</p></div>
<div class="m2"><p>جدا از پیر کنعانی کجایی؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدست اهرمن، حیف است خاتم</p></div>
<div class="m2"><p>تو ای دست سلیمانی کجایی؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سردت گردم، کنون از صحبت دوش؛</p></div>
<div class="m2"><p>نداری گر پشیمانی کجایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو افتد فکر معموری بخاطر</p></div>
<div class="m2"><p>همین گویم که ویرانی کجایی؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گیرد دامنم را خار امید</p></div>
<div class="m2"><p>همین نالم که عریانی کجایی؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسلمانان، نمی پرسند حالم</p></div>
<div class="m2"><p>کجایی ای مسلمانی کجایی؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو از کعبه و بتخانه آذر</p></div>
<div class="m2"><p>که می‌دانم نمی‌دانی کجایی؟!</p></div></div>