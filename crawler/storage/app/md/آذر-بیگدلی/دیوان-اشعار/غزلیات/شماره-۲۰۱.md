---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>ای به سیما، همان که می‌دانی؛</p></div>
<div class="m2"><p>تو شهی، ما همان که می‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آستان تو، عاشقان رفتند؛</p></div>
<div class="m2"><p>مانده بر جا همان که می‌دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوهکن، جان ز شوق کند که داشت</p></div>
<div class="m2"><p>کارفرما همان که می‌دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خرامان به گلشنت بیند</p></div>
<div class="m2"><p>افتد از پا همان که می‌دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح نوروز، روی روشن تو؛</p></div>
<div class="m2"><p>شام یلدا همان که می‌دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد آذر، که شاد بنشینم</p></div>
<div class="m2"><p>یک نفس با همان که می‌دانی</p></div></div>