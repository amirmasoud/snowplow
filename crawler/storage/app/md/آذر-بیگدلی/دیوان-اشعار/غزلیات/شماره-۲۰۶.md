---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>کاکل عنبرین نهان، زیر کلاه کرده‌ای</p></div>
<div class="m2"><p>روز هزار کس چو من تار و، سیاه کرده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد رخ ز ماه به، داده به زلف چون زره،</p></div>
<div class="m2"><p>تاب و زرشک خون گره، در دل ماه کرده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز کنارم از جفا، رفته‌ای ای پسر مرا</p></div>
<div class="m2"><p>گوش به در نشانده‌ای، چشم به راه کرده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتن بی‌گنه اگر، نیست گنه به مذهبت؛</p></div>
<div class="m2"><p>در همه عمر جان من، پس چه گناه کرده‌ای؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز دلم به دوستان، شکوهٔ من به دشمنان؛</p></div>
<div class="m2"><p>گفته و، آه گفته‌ای؛ کرده و، آه کرده‌ای!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیله دلبری است این، کآذر بی‌گناه را</p></div>
<div class="m2"><p>کشته و خود به تعزیت جامه سیاه کرده‌ای</p></div></div>