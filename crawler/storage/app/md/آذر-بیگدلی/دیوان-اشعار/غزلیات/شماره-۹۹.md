---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>مرا، کام دل، ازنگاهی برآید</p></div>
<div class="m2"><p>که از کنج چشم سیاهی برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند گر نه بانگ جرس رهنمایی</p></div>
<div class="m2"><p>چه از سعی کم کرده راهی برآید؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب عید، چشمم ببامی است کز وی</p></div>
<div class="m2"><p>پی دیدن ماه، ماهی برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت ماه و بهتر ز ماهی که گاهی</p></div>
<div class="m2"><p>رود در پس ابر و گاهی برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشاهان رسد ناز چون من گدایی</p></div>
<div class="m2"><p>که از خلوت چون تو شاهی برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گر کشد، ور کند زنده شاید؛</p></div>
<div class="m2"><p>کش این هر دو کار، از نگاهی برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیارم ز کوی تو رفتی چو صیدی</p></div>
<div class="m2"><p>که نتواند از صید گاهی برآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جفایش مباد از دل دردمندی</p></div>
<div class="m2"><p>شبی ناله یی، روزی آهی برآید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن رنجه، سرپنجه از بهر قتلم</p></div>
<div class="m2"><p>چه از کشتن بیگناهی برآید؟!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دعا سرکنم کز لبت کام آذر</p></div>
<div class="m2"><p>برآید الهی، الهی برآید</p></div></div>