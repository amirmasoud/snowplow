---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>تا کی به درت نالیم، هرشب من و دربان‌ها؟</p></div>
<div class="m2"><p>آنها ز فغان من، من از ستم آن‌ها؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامان توام شاید، کز سعی به دست آید؛</p></div>
<div class="m2"><p>لیک آه که می‌باید زد دست به دامان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکبار برون آور، زان چاک گریبان سر</p></div>
<div class="m2"><p>چون رفته فرو بنگر سرها به گریبان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جسم تو جان پاک، در راه تو جان‌ها خاک؛</p></div>
<div class="m2"><p>هر سو گذری چالاک، بر باد رود جان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد تیر فزون یا کم، از تو به دل چاکم؛</p></div>
<div class="m2"><p>گم‌گشته و از خاکم، پیدا شده پیکان‌ها!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نارم ز طبیبان یاد، دارم به دل ناشاد؛</p></div>
<div class="m2"><p>درد تو و نتوان داد، این درد به درمان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند دلت لرزد، زین غم که خطش سر زد؟!</p></div>
<div class="m2"><p>این سبزه تو را ارزد آذر به گلستان‌ها!</p></div></div>