---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>با مشک خطت، یاد ز عنبر نکند کس</p></div>
<div class="m2"><p>با شهد لبت، میل بشکر نکند کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد، که چندان ز وفای تو بمردم</p></div>
<div class="m2"><p>گفتم که کنون جور تو باور نکند کس!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گریه کنم گل همه شب خاک درت را</p></div>
<div class="m2"><p>تا روز ز بیداد تو بر سر نکند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکبار، گذر کن بسر خاک شهیدان</p></div>
<div class="m2"><p>تا دعوی خون در صف محشر نکند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آذر! ز وفا گشته سگش با تو برابر؛</p></div>
<div class="m2"><p>شه را بگدا گر چه برابر نکند کس</p></div></div>