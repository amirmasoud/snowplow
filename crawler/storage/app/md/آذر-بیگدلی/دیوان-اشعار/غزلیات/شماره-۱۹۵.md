---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>ماه رخش چو بنمود، از طرف بام نیمی</p></div>
<div class="m2"><p>از شرم کاست، تا شد ماه تمام نیمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم رها کنندم، مشکل رسم بجایی</p></div>
<div class="m2"><p>زین بال کش قفس ریخت، نیمی و دام نیمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از گرم خویی عشق، وز سرد مهری حسن؛</p></div>
<div class="m2"><p>سوزان کباب دل ماند، نیمی و خام نیمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم ز روی و مویت، رنجی و، تا نرنجی</p></div>
<div class="m2"><p>گویم بصبح نیمی، ناچار و، شام نیمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین نیم جان که دارم، چون نگذرم که یارم</p></div>
<div class="m2"><p>از لطف بگذراند بر لب زنام نیمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم نگه ز هر یک نبود از آن دو چشمم</p></div>
<div class="m2"><p>بس زان دو یک نگاهم، از هر کدام نیمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بیخبر نگویم حرفی ز مستی آذر</p></div>
<div class="m2"><p>ساقی دهد چو جامم، ریزد ز جام نیمی!</p></div></div>