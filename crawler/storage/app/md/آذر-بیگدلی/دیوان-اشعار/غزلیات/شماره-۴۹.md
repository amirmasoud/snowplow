---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>من از غمت، زبانی، با خلق در شکایت</p></div>
<div class="m2"><p>وز قصد من، نهانی، دل با تو در حکایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست مانده فردی، گفتا ز عشق دردی</p></div>
<div class="m2"><p>خوشتر نه کاش کردی از دل بدل سرایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر درد عشق جوییم درمان ز هم من و دل</p></div>
<div class="m2"><p>چون گمرهان که جویند از یکدگر هدایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل چنین نباید از بنده خواجه شاید</p></div>
<div class="m2"><p>بر تو غرامت آید، گر من کنم جنایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف تو را که عام است، آرام دل گواه است</p></div>
<div class="m2"><p>آری ز عدل شاه است آبادی ولایت</p></div></div>