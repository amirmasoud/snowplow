---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>ستمکشان تو، از شکوه لب چنان بستند؛</p></div>
<div class="m2"><p>که از شکایت اغیار هم زبان بستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمان بصبر رقیبان مبر، اگر بینی</p></div>
<div class="m2"><p>زبان ز شکوه دو روزی به امتحان بستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بترس ز آه شهیدان، نه ساکنان سپهر</p></div>
<div class="m2"><p>گشاده دست تو درهای آسمان بستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من مرنج، دو روزی بباغ اگر نایم</p></div>
<div class="m2"><p>پرم بکنج قفس ای هم آشیان بستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز باغ عشق، نبردم بری ز پرورشت؛</p></div>
<div class="m2"><p>نبسته دسته گلی، دست باغبان بستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شکوه سرکنم از دلبران؟ همان گیرم</p></div>
<div class="m2"><p>بوعده های دروغم دگر زبان بستند!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مصر رفت ز کنعان هزار کس آذر</p></div>
<div class="m2"><p>که گاه آمدنش، راه کاروان بستند</p></div></div>