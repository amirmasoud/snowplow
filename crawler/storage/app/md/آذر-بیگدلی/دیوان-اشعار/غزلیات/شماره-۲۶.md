---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>افغان که رفت جانم و جانانم آرزوست</p></div>
<div class="m2"><p>دردا که کشت دردم و درمانم آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من تنگدل ز کنج قفس نیستم، ولی</p></div>
<div class="m2"><p>یک ناله در میان گلستانم آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من قابل ملازمت محرمان نیم</p></div>
<div class="m2"><p>وین طرفه تر که خدمت سلطانم آرزوست؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حرف کفر و قصه ی ایمان دلم گرفت</p></div>
<div class="m2"><p>صلحی میان گبر و مسلمانم آرزوست</p></div></div>