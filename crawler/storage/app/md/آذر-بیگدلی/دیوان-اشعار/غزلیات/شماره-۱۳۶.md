---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>آمد از راه و نشد فرصت دیدن چکنم؟!</p></div>
<div class="m2"><p>رفت و باید ستم هجر کشیدن چکنم؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از همان بزم، که کس ناله ی زارم نشنید؛</p></div>
<div class="m2"><p>بایدم خنده ی اغیار شنیدن چکنم؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه در گوش کشم، گر تو بهیچم نخری</p></div>
<div class="m2"><p>چون تو را نیست سر بنده خریدن چکنم؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زد بتیغ ستم و، بست بفتراکم و ماند</p></div>
<div class="m2"><p>بدلم حسرت در خاک طپیدن چکنم؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه من، سوی سفر میرود از منزل و نیست</p></div>
<div class="m2"><p>از پی محمل او پای دویدن چکنم؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیرم ای مهر گسل، با تو گزینم پیوند</p></div>
<div class="m2"><p>چون رسد نوبت پیوند بریدن چکنم؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیرم آن آهوی وحشی شود آذر رامم</p></div>
<div class="m2"><p>چون کند بی سبب آهنگ رسیدن چکنم؟!</p></div></div>