---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>آمد سحر به پرسش من یار با رقیب</p></div>
<div class="m2"><p>یا من زرشک جان دهم امروز، یا رقیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خون من که کشته شدم، پیش از او گرفت</p></div>
<div class="m2"><p>ز آن پیشتر که کشته شود خونبها رقیب!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بیوفا، بس است جفا از خدا بترس</p></div>
<div class="m2"><p>تا چند بینم از تو جفا من، وفا رقیب!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زارم بکش برو، که ببیند چو کشته ام؛</p></div>
<div class="m2"><p>ناید ز بیم جان دگرت از قفا رقیب!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون پایمال رشکم، از این بزم رفتنم</p></div>
<div class="m2"><p>بهتر؛ ببزم تا ننهاده است پا رقیب!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیگانه، بایدم ز تو ناآشنا شدن</p></div>
<div class="m2"><p>زان پیشتر که با تو شود آشنا رقیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم ز بدگمانی دل، گیرم ای پری</p></div>
<div class="m2"><p>هم پاکدامنی تو و، هم پارسا رقیب!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کوی یار آذر اگر میروی برو</p></div>
<div class="m2"><p>آگه ز خواری تو نگشته است تا رقیب!</p></div></div>