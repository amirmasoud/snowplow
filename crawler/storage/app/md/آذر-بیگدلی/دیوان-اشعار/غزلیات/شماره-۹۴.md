---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>دوشم، به اهل بزم سر گفتگو نبود</p></div>
<div class="m2"><p>من در خمار بودم و، می در سبو نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسید: در دل تو ندانم چه آرزوست؟!</p></div>
<div class="m2"><p>غافل که در دلم بجز این آرزو نبود!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرم سگ تو نیست، گرت شب نبرد خواب</p></div>
<div class="m2"><p>او را مکش، که ناله ی من بود، ازو نبود!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش آمدم که پای تو بوسم، ز بیم غیر</p></div>
<div class="m2"><p>راهی بخلوت توام از هیچ سو نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میخوردم از فراق تو خون دوش وقت مرگ</p></div>
<div class="m2"><p>یعنی که بیتو آب خوشم در گلو نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قاصد بگو: ز دوریت آذر سپرد جان</p></div>
<div class="m2"><p>ور گوید: از منش گله یی بود، گو نبود!</p></div></div>