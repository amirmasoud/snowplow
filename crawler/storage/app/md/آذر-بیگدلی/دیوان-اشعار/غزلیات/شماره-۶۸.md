---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>خاکش، اگر ز دوری، بر باد رفته باشد</p></div>
<div class="m2"><p>آن یار نیست کش یار از یاد رفته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه کشت خود را، از عشق خسرو، اما</p></div>
<div class="m2"><p>مشکل زیاد شیرین، فرهاد رفته باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق اسیری آن مرغ داند که از پی صید</p></div>
<div class="m2"><p>روزی بآشیانش، صیاد رفته باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی جفا، روزی اندیشه کن که ما را</p></div>
<div class="m2"><p>تا آسمان ز جورت فریاد رفته باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیاد مهربانی، آذر گمان نبردم</p></div>
<div class="m2"><p>کآنجا که صیدش از پا افتاد، رفته باشد</p></div></div>