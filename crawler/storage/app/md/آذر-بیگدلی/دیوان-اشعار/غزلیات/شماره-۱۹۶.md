---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>تا می نخوری، قد رخ زرد ندانی ؛</p></div>
<div class="m2"><p>تا جان ندهی، فایده ی درد ندان ی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جان نرسد بر لبت، از حسرت پیغام</p></div>
<div class="m2"><p>آن مژده که قاصد بمن آورد ندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا صاحب محمل، دلت ازکفر نرباید</p></div>
<div class="m2"><p>بیتابی مجنون ز پی گرد ندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا سنگدلی، بر سر خاکت ننشیند</p></div>
<div class="m2"><p>صبری که دلم در غم او کرد ندانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که: کنی چاره ی درد دل آذر</p></div>
<div class="m2"><p>افسوس که خاصیت این درد ندانی</p></div></div>