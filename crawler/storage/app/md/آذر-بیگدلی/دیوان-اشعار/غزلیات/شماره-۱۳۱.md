---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه از غم دل، حرفیش گفته باشم؛</p></div>
<div class="m2"><p>حرفیش گفته باشم، حرفی شنفته باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم ز بخت بیدار، روز و شبی که با یار</p></div>
<div class="m2"><p>تا شب نشسته باشم، تا روز خفته باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون میکشیم باری، جرمم بپرس؛ شاید</p></div>
<div class="m2"><p>کاری نکرده باشم، حرفی نگفته باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سرمه ی سلیمان، آذر کشم بدیده</p></div>
<div class="m2"><p>زان آستان بمژگان گردی که رفته باشم</p></div></div>