---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>قاصد، از ننگ زمن نامه بجایی نبرد</p></div>
<div class="m2"><p>ور برد، نام چو من بیسر و پایی نبرد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال آن بنده چه باشد، که چو آزاد شود</p></div>
<div class="m2"><p>جز در خواجه ی خود، راه بجایی نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر افتد بگمان، کز پی دلجویی اوست؛</p></div>
<div class="m2"><p>چو بجورم کشد و نام خطائی نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام من برد، ندانم ز غضب یا کرم است؟!</p></div>
<div class="m2"><p>ز آنکه شاهی بعبث نام گدایی نبرد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میرود از همه کس قاصد و من میگویم</p></div>
<div class="m2"><p>که: پیامی ز منش غیر دعایی نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبرم از تو شکایت بکسی جز تو، که دوست</p></div>
<div class="m2"><p>گله ی دوست بجز دوست بجایی نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر آذر، که ز غم مرد و ازو شکوه نکرد</p></div>
<div class="m2"><p>دگر آن به که کسی نام وفائی نبرد</p></div></div>