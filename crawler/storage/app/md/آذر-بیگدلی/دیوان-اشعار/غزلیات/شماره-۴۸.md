---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>نهم به پای کسی سر، که سر نهاده به پایت</p></div>
<div class="m2"><p>کنم فدای کسی جان، که کرده جان به فدایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآ، برای خدا، همچو مه به بام و نظر کن؛</p></div>
<div class="m2"><p>ببین چگونه مرا می‌کشند زار برایت؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشسته گرد ملالم به چهره بی‌تو و، ترسم؛</p></div>
<div class="m2"><p>گمان برند که رخ سوده‌ام به خاک سرایت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن حذر کسی، گر چه از غرور جوانی؛</p></div>
<div class="m2"><p>تو غافلی ز خدا، من سپرده‌ام به خدایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل است جای تو و، بیدلان به دیر و حرم بین؛</p></div>
<div class="m2"><p>تو در کجایی و جویند غافلان ز کجایت؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشم که مردم از اهل وفا، جفای تو باور</p></div>
<div class="m2"><p>نمی‌کنند، که از من شنیده‌اند وفایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوای درد ز مردن تو را، و من متحیر؛</p></div>
<div class="m2"><p>که چیست درد تو آذر که مردن است دوایت؟!</p></div></div>