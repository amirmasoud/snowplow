---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>هر مرغ که میپرد ز بامت</p></div>
<div class="m2"><p>گویم، بمن آورد پیامت!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خونم، که چو آب شد حلالت؛</p></div>
<div class="m2"><p>گر با دگران خوری حرامت!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مپسند ستمگران بمحشر</p></div>
<div class="m2"><p>خندند بزخم ناتمامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش دانه بحیله میفشانی</p></div>
<div class="m2"><p>از صید مگر تهی است دامت؟!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خواه ببخش و، خواه بفروش؛</p></div>
<div class="m2"><p>زین کوی نمیرود غلامت!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دوست! مریز خون دشمن</p></div>
<div class="m2"><p>کز دوست کشند انتقامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر از آذر، شکایت از غیر؛</p></div>
<div class="m2"><p>تا زین دو خوش آید از کدامت؟!</p></div></div>