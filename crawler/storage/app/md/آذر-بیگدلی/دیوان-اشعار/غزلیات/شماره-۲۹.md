---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>بر آستان توام، شب چو شد، فغانی هست</p></div>
<div class="m2"><p>که شب فغان سگی در هر آستانی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم پر است، دم نزع شکوه تا نکنم؛</p></div>
<div class="m2"><p>بپرس حال مرا، تا مرا زبانی هست!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمان این بمنت نیست کز تو شکوه کنم</p></div>
<div class="m2"><p>مگر هنوز بصبر منت گمانی هست؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سگت برای چه افتاده در قفای رقیب؟!</p></div>
<div class="m2"><p>هنوز در تن من، مشت استخوانی هست!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه من، از خبر مهر من بکینم کشت</p></div>
<div class="m2"><p>ولی ازین خبرش نیست کآسمانی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر است دامن خلق از گل و تهی از من</p></div>
<div class="m2"><p>باین گمان که در این باغ باغبانی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براه عشق، همین پایه بس تو را آذر</p></div>
<div class="m2"><p>که گردی از تو بدنبال کاروانی هست!</p></div></div>