---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>دردی دارم که گفتنی نیست</p></div>
<div class="m2"><p>ور گفته شود شنفتنی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغی است دلم ز داغت، اما</p></div>
<div class="m2"><p>باغی که گلش شکفتنی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داند همه کس غمم، که عشقت</p></div>
<div class="m2"><p>گنجی است، ولی نهفتنی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسانه ی وصل تا نباشد</p></div>
<div class="m2"><p>چشمم شب هجر خفتنی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردی است در آستانت آذر</p></div>
<div class="m2"><p>اما گردی که رفتنی نیست</p></div></div>