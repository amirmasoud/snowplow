---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>گر دل جویی، هوات جویم</p></div>
<div class="m2"><p>ور جان طلبی، رضات جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد گر چه جفایت آفت جان</p></div>
<div class="m2"><p>تا جان دارم جفات جویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا روز وفات، ای جفاجو</p></div>
<div class="m2"><p>مهرت طلبم، وفات جویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس چون ندهد سراغت از رشک</p></div>
<div class="m2"><p>خون گریم و از خدات جویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو در دل و، دل تو برده رفتی</p></div>
<div class="m2"><p>اکنون چکنم کجات جویم؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیرت، بفسون اگر ز ره برد؛</p></div>
<div class="m2"><p>منهم روم از دعات جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آذر، درد تو درد عشق است</p></div>
<div class="m2"><p>رفتم ز اجل دوات جویم</p></div></div>