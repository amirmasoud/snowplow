---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>گیرم بعجز دامنت و جان سپارمت</p></div>
<div class="m2"><p>شاید که گامی از پی نعش خود آرمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کشته رشکم، امشب از آن کو نمی روم</p></div>
<div class="m2"><p>کز دل نیایدم برقیبان گذارمت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر حرف گفتمت، ز رقیبان شنفتم آه؛</p></div>
<div class="m2"><p>نامحرمی و، محرم خود میشمارمت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغم زنی و، دم نزنم؛ آه چون کنم؟!</p></div>
<div class="m2"><p>دانم که دشمن منی و دوست دارمت!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آذر ز حیله یار بدل بردن آمده است</p></div>
<div class="m2"><p>امشب بپاسبانی دل می گمارمت</p></div></div>