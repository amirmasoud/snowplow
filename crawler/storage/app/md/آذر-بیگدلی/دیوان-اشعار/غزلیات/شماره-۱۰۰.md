---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>ماه، بر روی چو ماهش نگرید</p></div>
<div class="m2"><p>فتنه در چشم سیاهش نگرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد عارض، خط شیرین بینید؛</p></div>
<div class="m2"><p>کنج لب، خال سیاهش نگرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میرود، وز پی او دلشدگان</p></div>
<div class="m2"><p>شاه بینید و سپاهش نگرید!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی گنه کشته شد آذر یاران</p></div>
<div class="m2"><p>بی گناه است، گناهش نگرید</p></div></div>