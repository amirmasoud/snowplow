---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>گرت بخاک شهیدان گذار خواهد بود</p></div>
<div class="m2"><p>هزار چون منت امیدوار خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلام بندگیش از من ای صبا برسان</p></div>
<div class="m2"><p>گرت بمنزل سلمی گذار خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه بیگنهم میکشد، باین شادم</p></div>
<div class="m2"><p>که روز حشر زمن شرمسار خواهد بود!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که روشن ازو نیست کلبه ام/ گویم</p></div>
<div class="m2"><p>که: روز واقعه ی شمع مزار خواهد بود</p></div></div>