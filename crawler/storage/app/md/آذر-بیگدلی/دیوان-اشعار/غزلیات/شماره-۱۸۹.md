---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>فغان که عمر سرآمد در انتظار کسی</p></div>
<div class="m2"><p>که همچو عمر، دمی بیش نیست یار کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وفا بوعده گر این است امید گاهان را</p></div>
<div class="m2"><p>کسی مباد بعالم امیدوار کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن دیار شدم خاک ره، که ننشیند</p></div>
<div class="m2"><p>بدامن کسی از رهروان، غبار کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آن شدم که کنم منع دل ز عشق بتان</p></div>
<div class="m2"><p>ولی بدست کسی نیست اختیار کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چه سود ازین زندگی، که تا بودم</p></div>
<div class="m2"><p>نه کس بکار من آمد، نه من بکار کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشست غیر به پهلوی او، مباد آن روز</p></div>
<div class="m2"><p>که ناکسی بودش جای در کنار کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم ز بیکسی آذر، جز اینکه میترسم</p></div>
<div class="m2"><p>کسی نیاوردم نامه از دیار کسی</p></div></div>