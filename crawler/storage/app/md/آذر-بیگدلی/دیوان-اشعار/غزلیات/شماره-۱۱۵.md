---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>مشکل که برم من جان، آسان چو تو بردی دل؛</p></div>
<div class="m2"><p>دل بردن تو آسان، جان بردن من مشکل!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صیاد ز بی رحمی، زد تیری و پنهان شد؛</p></div>
<div class="m2"><p>نگذاشت بکام دل، در خاک طپد بسمل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حشر چو برخیزم، در دامنت آویزم؛</p></div>
<div class="m2"><p>صد فتنه برانگیزم، کز من نشوی غافل!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیلی، بفراز تخت، آسوده چه غم دارد؟!</p></div>
<div class="m2"><p>افتاده بره مجنون چون گرد پی محمل!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از هجر منال آذر، کز دوری آن دلبر؛</p></div>
<div class="m2"><p>دست همه کس بر سر، پای همه کس در گل!!</p></div></div>