---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دلم، که شکوه ز بیداد دلبری دارد؛</p></div>
<div class="m2"><p>ستمکشی است که یار ستمگری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بآن درخت، زیان یا رب از خزان مرساد؛</p></div>
<div class="m2"><p>که زیر سایه ی خود، مرغ بی پری دارد!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شوق، دل چو کبوتر طپد بسینه مگر</p></div>
<div class="m2"><p>سراغ نامه ببال کبوتری دارد؟!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکایت از ستمت کی کنم؟ که بسته لبم</p></div>
<div class="m2"><p>شکایتی که ز جور تو دیگری دارد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خواجه یی تو؟ که هر بنده یی که مینگرم</p></div>
<div class="m2"><p>بغیر بنده ی تو بنده پروری دارد!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفت مهر تو تا جای در دلم، گفتم</p></div>
<div class="m2"><p>که: بشکنم صدفی را که گوهری دارد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براه عشق تو، گم گشت آذر، این راهی است</p></div>
<div class="m2"><p>که هر که گم شود، امید رهبری دارد</p></div></div>