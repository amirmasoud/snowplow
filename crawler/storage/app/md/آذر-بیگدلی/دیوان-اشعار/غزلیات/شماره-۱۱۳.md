---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>مهی، که مایهٔ شادی عالم است غمش</p></div>
<div class="m2"><p>بود شکایت بسیار من، ز لطف کمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا فراق وی آن روز کشت و، می‌ترسم</p></div>
<div class="m2"><p>که روز حشر به قتلم کنند متهمش!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منش ستمگری آموختم، ندانستم</p></div>
<div class="m2"><p>که من نخست دهم جان به خواری از ستمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فگند تیغ و یم سر بپای او، شادم</p></div>
<div class="m2"><p>که بر نداشتم آن روز هم سر از قدمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغان که روز فراقم، زمان زمان آمد؛</p></div>
<div class="m2"><p>بیاد سوی رقیبان، نگاه دمبدمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فگند عشق، به بتخانه یی مرا کز ناز</p></div>
<div class="m2"><p>ندیده گوشه ی چشمی برهمن از صنمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه مرغ نامه ام آذر برد بکوی بتی</p></div>
<div class="m2"><p>که نیست باک ز قتل کبوتر حرمش؟!</p></div></div>