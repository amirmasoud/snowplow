---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>چون صبر ز جور تو ستمگر نکند کس؟!</p></div>
<div class="m2"><p>جز صبر ز جورت چه کند گر نکند کس؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خضر ببخشد قدح آب بقا را</p></div>
<div class="m2"><p>با خاک در دوست، برابر نکند کس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل نبود آرزوی خلوت خاصم</p></div>
<div class="m2"><p>این بس که تو را منع از آن در نکند کس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افتادگی آموز، که در کوی خرابات</p></div>
<div class="m2"><p>نظارهٔ درویش و توانگر نکند کس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو مستی و از تندی خوی تو در آن بزم</p></div>
<div class="m2"><p>یاد از غم محرومی آذر نکند کس</p></div></div>