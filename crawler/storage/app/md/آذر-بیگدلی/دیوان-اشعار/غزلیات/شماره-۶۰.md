---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>چند روزی از سر کویت سفر خواهیم کرد</p></div>
<div class="m2"><p>امتحان را جای در کوی دگر خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گذرگاهی دگر، چاکی بدل خواهیم زد؛</p></div>
<div class="m2"><p>بر سر راهی دگر، خاکی بسر خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کسی آید ز پی، ما، باز خواهیم گشت؛</p></div>
<div class="m2"><p>ورنه آنجا، اشک حسرت دیده تر خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آنچه گفتی، دیگران را آگهی خواهیم داد؛</p></div>
<div class="m2"><p>ز آنچه کردی، دیگران را با خبر خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر غرور حسنت اکنون بسته بر ما راه حرف</p></div>
<div class="m2"><p>روز محشر گفتگو با یکدگر خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خانه گر خالی است، شکر پاسبان خواهیم گفت</p></div>
<div class="m2"><p>ورنه گامی چند منزل دورتر خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ببینیم از هواخواهان که نازش میکشد</p></div>
<div class="m2"><p>گاه گاه آذر بکوی او گذر خواهیم کرد</p></div></div>