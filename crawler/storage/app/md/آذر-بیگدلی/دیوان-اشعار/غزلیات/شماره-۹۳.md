---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>یاد باد آنکه ز یاری منت عار نبود</p></div>
<div class="m2"><p>یار من بودی و کس غیر منت یار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز حشرم، تو گواهی که شب هجرم کشت</p></div>
<div class="m2"><p>کان شب ای دیده کسی غیر تو بیدار نبود!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل آزاری رشک، آه کنون دانستم</p></div>
<div class="m2"><p>کآنچه زین پیش کشیدم ز تو آزار نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواریم، کار رسانده است بجایی که رقیب</p></div>
<div class="m2"><p>با توام دید بهرجا، بمنش کار نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم از تاب کمند تو، چنین شد بیتاب؛</p></div>
<div class="m2"><p>ورنه کی بود که این صید گرفتار نبود؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبلی دوش بدام آمد و در ناله ی او</p></div>
<div class="m2"><p>اثری بود که تا بود بگلزار نبود!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود امید نگه باز پسینش آذر</p></div>
<div class="m2"><p>ورنه جان دادنش از هجر تو دشوار نبود!</p></div></div>