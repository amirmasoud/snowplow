---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>جز دل نالان مرا ای گل، نه دمساز دگر</p></div>
<div class="m2"><p>غیر بلبل، نیست بلبل را هم آواز دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدلانت، آگه از راز دلم هم نیستند</p></div>
<div class="m2"><p>کز تو هر یک دیده گاه دلبری ناز دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقانت فارغ از رشکند، کز نیرنگ حسن؛</p></div>
<div class="m2"><p>در میان داری جدا با هر یکی راز دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یادم آید، کز قفایت میدویدم هر کجا</p></div>
<div class="m2"><p>کبک دیگر بینم از دنبال شهباز دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بدام از آشیان افتادمت، بشکن پرم؛</p></div>
<div class="m2"><p>تا نماند در دلم امید پرواز دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زدی تیرم، بکش، مگذار غلطانم بخاک</p></div>
<div class="m2"><p>تا ببازویت نخندد ناوک انداز دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آذر امشب از دگر شبها فزون در زاریم</p></div>
<div class="m2"><p>مینوازد مطرب مجلس، مگر ساز دگر</p></div></div>