---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>عاشقم و، عشق من زوال ندارد</p></div>
<div class="m2"><p>رفتنم از کویت احتمال ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وای بحالم، ز بیکسی که بکویت</p></div>
<div class="m2"><p>هیچکسم آگهی ز حال ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ندهم تن بدوری تو، که از پی</p></div>
<div class="m2"><p>روز فراقت، شب وصال ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام دل، از نخل قامت تو چه جویم؟!</p></div>
<div class="m2"><p>غیر بر حسرت، این نهال ندارد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکوه ی جورش کجا برم که شهیدم</p></div>
<div class="m2"><p>کرده و از کرده انفعال ندارد؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون اسیری کنون مریز که دانی</p></div>
<div class="m2"><p>در صف محشر زبان لال ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نکند ناله در شکنجه ی دامش</p></div>
<div class="m2"><p>مرغ دل آذر فراغ بال ندارد؟!</p></div></div>