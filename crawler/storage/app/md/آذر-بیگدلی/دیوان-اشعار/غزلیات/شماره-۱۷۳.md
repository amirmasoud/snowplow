---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>کو خضر راهی؟ کز خیل آن ماه</p></div>
<div class="m2"><p>وامانده ام پس، گم کرده ام راه!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل صبوری، هنگام دوری</p></div>
<div class="m2"><p>باور ندارم، والله بالله!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریزد چو جیحون، خیزد چو گردون</p></div>
<div class="m2"><p>از دیده ام اشک، از سینه ام آه!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانها فدایت، تا چیست رایت؟!</p></div>
<div class="m2"><p>جنگ تو دلکش، صلح تو دلخواه!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی از کمندم، افتد به بندم؟!</p></div>
<div class="m2"><p>آن صید وحشی، این رشته کوتاه!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افغان ز گفتن، آه از نهفتن؛</p></div>
<div class="m2"><p>مسکین گدایی، کو رنجد از شاه!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمن بدلبر، همبزم و؛ آذر</p></div>
<div class="m2"><p>جان میسپارد بیرون درگاه</p></div></div>