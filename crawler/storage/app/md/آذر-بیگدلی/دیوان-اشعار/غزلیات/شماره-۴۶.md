---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>حیات جاودان بخشد بعاشق لعل خندانت</p></div>
<div class="m2"><p>بود سرچشمه ی آب بقا چاک زنخدانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشی گر تو شمع تربت ما نیست دلسوزی؛</p></div>
<div class="m2"><p>که افروزد چراغی بر سر خاک شهیدانت!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمیدانی چرا شد چاک تا دامن گریبانم؟!</p></div>
<div class="m2"><p>مگر روزی بدست چون خودی افتد گریبانت!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن هر دم بچاک سینه ی اهل وفا خندی</p></div>
<div class="m2"><p>که تا ریزد نمک بر زخم دل‌ها از نمکدانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحشر از کشتن آذر، مکن انکار میترسم</p></div>
<div class="m2"><p>خجل گردی چو بیرون آورد از سینه پیکانت</p></div></div>