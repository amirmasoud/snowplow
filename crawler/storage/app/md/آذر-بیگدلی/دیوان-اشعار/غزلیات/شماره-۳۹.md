---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>حرف جورت، همه جا با همه کس خواهم گفت</p></div>
<div class="m2"><p>این حدیثی است که تا هست نفس خواهم گفت!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد صیادم ازین باغ و ز بیمهری گل</p></div>
<div class="m2"><p>حرفها دارم و در کنج قفس خواهم گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نماند بسر کوی تو جز من دگری</p></div>
<div class="m2"><p>قصه ی جور تو با اهل هوس خواهم گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال خود، کز پی آن قافله سرگردانم؛</p></div>
<div class="m2"><p>چون بگوشم رسد آواز جرس خواهم گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنم آذر گله از دشمن اگر بینم دوست</p></div>
<div class="m2"><p>نیست گل ورنه دل آزاری خس خواهم گفت</p></div></div>