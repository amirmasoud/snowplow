---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>شب پره کو روز آفتاب نبیند</p></div>
<div class="m2"><p>روشنی دیده جز بخواب نبیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وادی عشق است، از فریب حذر کن؛</p></div>
<div class="m2"><p>تشنه در این دشت جز سراب نبیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساکن بزم تو، قدر وصل نداند</p></div>
<div class="m2"><p>گر چه در آب است ماهی، آب نبیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محتشمان را، چه آگهی ز غم دل؟!</p></div>
<div class="m2"><p>چشم هما گنج در خراب نبیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه بداغ فراق سوخته جانش</p></div>
<div class="m2"><p>ز آتش دوزخ دگر عذاب نبیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز پی آهوی ختن نرود کس</p></div>
<div class="m2"><p>جیب و بغل پر ز مشک ناب نبیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز ادب ننگرم بروی وی آذر</p></div>
<div class="m2"><p>یار بسوی من، از حجاب نبیند!</p></div></div>