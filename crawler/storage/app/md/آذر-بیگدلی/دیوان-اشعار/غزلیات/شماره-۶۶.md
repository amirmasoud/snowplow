---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>دم مرگم، ز غم هجر غمی بیش نباشد</p></div>
<div class="m2"><p>گر تو را بینم و از عمر دمی بیش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز از حسرتم آگاه نگردی، مگر آن دم</p></div>
<div class="m2"><p>که ز پا افتی و منزل قدمی بیش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زده زیبا صنمان گرد دلم حلقه و غافل</p></div>
<div class="m2"><p>که درین بتکده جای صنمی بیش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه عادت بستم داده مرا، کاش نداند</p></div>
<div class="m2"><p>که ز ترک ستم او را ستمی بیش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم آید دمی از لطف طبیبم بسر آذر</p></div>
<div class="m2"><p>که مرا فرصت گفتار دمی بیش نباشد</p></div></div>