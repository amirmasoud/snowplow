---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>ایزد سرشته هر دل از آب و گل دگر</p></div>
<div class="m2"><p>باشد مرا دل دگر، او را دل دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم شب وصال تو، حسرت بروز هجر</p></div>
<div class="m2"><p>جز رشک نیست وصل تو را حاصل دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیلی، اگر بجای دگر رفت محملش</p></div>
<div class="m2"><p>مجنون نمیرود ز پی محمل دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش تو آورم ز غم ار مشکلی، فغان</p></div>
<div class="m2"><p>کاسان نکرده پیش نهی مشکل دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کشته آنکه روز جزا خواست خونبها</p></div>
<div class="m2"><p>نبود بغیر قاتل من، قاتل دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافل ز دام جستمت، اما ز بی پری</p></div>
<div class="m2"><p>ترسم بگیردم ز خدا غافل دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون شد دلم، که نیست یکی با زبان دلت</p></div>
<div class="m2"><p>باید تو را زبان دگر یا دل دگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با کم ز سوختن نه چو پروانه، آه اگر</p></div>
<div class="m2"><p>روشن شود ز شمع رخت، محفل دگر؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نادیدن تو مشکل و، از بیم مدعی</p></div>
<div class="m2"><p>باید ز دور دیدنت، این مشکل دگر!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشتی هزار صید و، پی جان آذری</p></div>
<div class="m2"><p>سهل است اگر بخون بطپد بسمل دگر!</p></div></div>