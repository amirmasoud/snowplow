---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>بدو زلف تو که یکسر دل مبتلا نشسته</p></div>
<div class="m2"><p>همه حیرتم که آیا دل من کجا نشسته؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهمین امید، هر شب بره تو می نشینم؛</p></div>
<div class="m2"><p>که تو بیوفا بپرسی که دگر چرا نشسته؟!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چکنم ز رشک یا رب، چو بروز حشر بینم</p></div>
<div class="m2"><p>که هزار کشته آنجا، پی خونبها نشسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب و شاهد و چغانه، من و باده ی مغانه</p></div>
<div class="m2"><p>تو ببین که نقش طالع، چه بمدعا نشسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بچمن گلی ندیدم، که جدا ز خار باشد</p></div>
<div class="m2"><p>عجب است اینکه آذر ز گلشن جدا نشسته؟!</p></div></div>