---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>دم مردن شدن دمساز چون من ناتوانی را</p></div>
<div class="m2"><p>مرا گر زنده کردی، کشتی از رشکم جهانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین گلشن بود جای من ای گل، بلبلم، بلبل؛</p></div>
<div class="m2"><p>نه جغدم کو بهر ویرانه خوش کرد آشیانی را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریغا گشت صرف مهربانی عمر و، نتوانم</p></div>
<div class="m2"><p>که با خود مهربان سازم دل نامهربانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیابان محبت را، ندانم کیست خضر امشب</p></div>
<div class="m2"><p>که ره گم کرده می بینم، ز هر سو کاروانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون راندی مرا از کوی خود ای گل، بدان ماند</p></div>
<div class="m2"><p>که فصل گل کسی راند ز باغی باغبانی را!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توانایی بازوی تو را، ای صید کش دانم؛</p></div>
<div class="m2"><p>که خواهد ورنه از تو خون صید ناتوانی را؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخواهم رفت از کوی بتان آذر، مگر بینم</p></div>
<div class="m2"><p>شبی روزی نباشد پاسبانی آستانی را</p></div></div>