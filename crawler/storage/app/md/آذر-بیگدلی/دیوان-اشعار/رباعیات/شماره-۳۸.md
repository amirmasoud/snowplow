---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>این دل، سر راهی بنگاری نگرفت</p></div>
<div class="m2"><p>این دیده، فروغی ز عذاری نگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این پا، روزی بخاک کویی نرسید</p></div>
<div class="m2"><p>این دست، شبی دامن یاری نگرفت</p></div></div>