---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>سویم بفرست گاه پیغامی و گاه</p></div>
<div class="m2"><p>باز آی که تا ببینم آن روز چو ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل مشو از حال من القصه که من</p></div>
<div class="m2"><p>هم گوش برآوازم و، هم چشم براه</p></div></div>