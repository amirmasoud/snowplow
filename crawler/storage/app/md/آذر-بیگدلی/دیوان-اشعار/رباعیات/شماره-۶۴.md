---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>گفتی: ز کسیت کینه در سینه نماند</p></div>
<div class="m2"><p>چون نقش بد و نیک در آیینه نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لوحی است عجب آینه ی سینه ی ما</p></div>
<div class="m2"><p>کش ماند نشان ز مهر و، از کینه نماند</p></div></div>