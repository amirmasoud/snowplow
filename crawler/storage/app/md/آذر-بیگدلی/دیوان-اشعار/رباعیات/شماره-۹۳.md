---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>آذر! آن شوخ را در اندیشه نگر!</p></div>
<div class="m2"><p>دلباخته دلبر جفاپیشه نگر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن رخ که چو لاله بود، چون خیری بین</p></div>
<div class="m2"><p>آن دل که چو سنگ بود، چون شیشه نگر!</p></div></div>