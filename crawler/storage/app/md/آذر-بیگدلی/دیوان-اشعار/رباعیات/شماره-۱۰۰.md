---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>در کنج خرابه بر حصیری دو سه گز</p></div>
<div class="m2"><p>با دخترکی گرم تر از دختر رز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان می میخور، ورنه دل خود میخور،</p></div>
<div class="m2"><p>هن لب میمز، ورنه لب خود میگز!</p></div></div>