---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>صحبت چه شود گرم میان من و تو</p></div>
<div class="m2"><p>آید بمیان راز نهان من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیخود شوم و، بخود چو آیم، چه شود؟!</p></div>
<div class="m2"><p>گویی که چه رفت بر زبان من و تو؟!</p></div></div>