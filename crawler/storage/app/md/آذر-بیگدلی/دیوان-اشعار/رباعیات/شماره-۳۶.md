---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>چشمم، که نه ز آن بزم خواهد خفت</p></div>
<div class="m2"><p>گفتم: شب وصل است و کنون خواهد خفت!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شوق نخفت، تا شب هجر آمد؛</p></div>
<div class="m2"><p>چون با تو نخفت، بیتو چون خواهد خفت؟!</p></div></div>