---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>تا در غم جانان، بلبلم جان نرسید؛</p></div>
<div class="m2"><p>آن درد که داشتم، بدرمان نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا که رسید با هزاران حسرت</p></div>
<div class="m2"><p>جان بر لب و لب بر لب جانان نرسید!</p></div></div>