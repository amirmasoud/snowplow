---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ای کاسلامت، بکافریها ماند؛</p></div>
<div class="m2"><p>دل باختنت، بدلبریها ماند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که ز اول ستمت، جان نبرم؛</p></div>
<div class="m2"><p>و اندر دل تو، ستمگریها ماند!</p></div></div>