---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>از کین چکند تا بدل ما دل تو؟!</p></div>
<div class="m2"><p>باشد دل ما شیشه و خارا دل تو!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیست دلت سنگ؟ چرا ز افغانم</p></div>
<div class="m2"><p>خون شد دل خوبان همه، الا دل تو؟!</p></div></div>