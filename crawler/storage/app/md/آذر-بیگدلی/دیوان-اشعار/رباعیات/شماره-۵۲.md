---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>جان رفت و، دل از تو باز دردی دارد؛</p></div>
<div class="m2"><p>اشک سرخی و رنگ زردی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل منشین، که خاک غم پرور من</p></div>
<div class="m2"><p>هر چند بباد رفته، گردی دارد</p></div></div>