---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>ز آن شب که رخت دیده، گلت چیدستم</p></div>
<div class="m2"><p>ا زدیدن روز، دیده پوشیدستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز گرفته شمع خورشید بکف</p></div>
<div class="m2"><p>گردم پی آن شب که، تو را دیدستم</p></div></div>