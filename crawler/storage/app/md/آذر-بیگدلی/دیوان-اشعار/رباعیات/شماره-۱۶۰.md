---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>گفتم: یار است پای من در گل از او!</p></div>
<div class="m2"><p>گفتم : بخت است کار من مشکل از او!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی گله نه ز یار دارم، نه ز بخت</p></div>
<div class="m2"><p>دل دشمن من بوده، و من غافل از او!</p></div></div>