---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>کوی تو، که رشک گلستان ارم است</p></div>
<div class="m2"><p>تا هست در آن مرغ دلم، محترم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون چو رود، سزاست خون ریختنش؛</p></div>
<div class="m2"><p>تا در حرم است صید، صید حرم است!!</p></div></div>