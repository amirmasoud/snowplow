---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>باد سحری، ز مرغزاری برخاست؛</p></div>
<div class="m2"><p>وز هر طرفی، بانگ هزاری برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عشق گلی، در آشیان نالیدم؛</p></div>
<div class="m2"><p>از هر قفسی ناله ی زاری برخاست!</p></div></div>