---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>ای خواجه، چه میگریزی از من مه و سال؟!</p></div>
<div class="m2"><p>نه تو بکرم شهره و نه من بسؤال!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دوست بود دشمن دشمن، مگریز!</p></div>
<div class="m2"><p>مال تو تو را دشمن و، من دشمن مال!</p></div></div>