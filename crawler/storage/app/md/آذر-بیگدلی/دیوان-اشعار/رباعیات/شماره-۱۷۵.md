---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>دارم ز فراق ماه مهر افروزی</p></div>
<div class="m2"><p>در دل سوزی، نیست دلی دل سوزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز روز و شب من که ندارد شب و روز</p></div>
<div class="m2"><p>هر روز شبی دارد و هر شب روزی</p></div></div>