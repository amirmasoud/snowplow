---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای برده ز مهر تاب، ماه علمت</p></div>
<div class="m2"><p>وی داده به خضر آب، خاک قدمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی ساخته کان خراب، نقش درمت</p></div>
<div class="m2"><p>آن از تو کریمتر که داد این کرمت</p></div></div>