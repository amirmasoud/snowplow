---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>در کوی تو، دلباخته ها می بینم</p></div>
<div class="m2"><p>چون دل بغمت ساخته ها می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که سروی که منش پروردم</p></div>
<div class="m2"><p>در هر شاخش فاخته ها می بینم</p></div></div>