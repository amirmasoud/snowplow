---
title: >-
    شمارهٔ ۱۳۳
---
# شمارهٔ ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>چون گشت بخیر عاقبت کار نعیم</p></div>
<div class="m2"><p>از باغ جهان رفت به گلزار نعیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاریخ وفات خواستم، آذر گفت:</p></div>
<div class="m2"><p>شد دار نعیم ناگهان دار نعیم</p></div></div>