---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ای فوج طبیب را بدوزخ قائد</p></div>
<div class="m2"><p>خواهی شودت قیمت مسهل عائد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر نیست،ولی هر آنچه مسهل خوردم</p></div>
<div class="m2"><p>واپس دهم اکنون مع شیء زائد</p></div></div>