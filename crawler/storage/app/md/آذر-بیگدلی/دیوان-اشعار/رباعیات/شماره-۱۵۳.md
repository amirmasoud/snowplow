---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>شد زلزله یی، که نیست شد هست زمین</p></div>
<div class="m2"><p>بس سرو روان که گشت پابست زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم بفغان آمده از دور سپهر</p></div>
<div class="m2"><p>من خاک بسر میکنم از دست زمین</p></div></div>