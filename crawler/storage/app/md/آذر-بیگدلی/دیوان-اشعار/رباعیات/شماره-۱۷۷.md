---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>وقت است ز هر سو شکفد روی گلی</p></div>
<div class="m2"><p>هر بلبل مست رو نهد سوی گلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راز من و یار، گل کند، چون شنویم</p></div>
<div class="m2"><p>آن ناله ی بلبلی و، من بوی گلی!</p></div></div>