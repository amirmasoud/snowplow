---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>ای محرم خلوت ومه انجمنم</p></div>
<div class="m2"><p>محروم چو ببینم بتو، طعنه زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی من بودم چنین، که امروز تویی</p></div>
<div class="m2"><p>فردا تو چنان شوی، که امروز منم</p></div></div>