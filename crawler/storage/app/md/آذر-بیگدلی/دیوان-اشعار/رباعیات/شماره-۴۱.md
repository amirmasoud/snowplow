---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>قاصد که ازو به من خبر هیچ نگفت</p></div>
<div class="m2"><p>گفتم که تو را یار مگر هیچ نگفت؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که چرا، گفتمش آن گفته بگو؟!</p></div>
<div class="m2"><p>آهی به لب آورد و دگر هیچ نگفت!</p></div></div>