---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای اهل هوس، عشق شماری دگر است</p></div>
<div class="m2"><p>آب و گل عشق، از دیاری دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کار که مشکل تر از آن کاری نیست</p></div>
<div class="m2"><p>کاری دگر است، و عشق کاری دگر است!</p></div></div>