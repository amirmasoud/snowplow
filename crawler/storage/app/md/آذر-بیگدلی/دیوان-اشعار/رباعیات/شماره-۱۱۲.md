---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>خوش آنکه ز وصل تو شبی بالم خوش</p></div>
<div class="m2"><p>رو بر کف پای نازکت مالم خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم تو بمن باشد و من گریم زار</p></div>
<div class="m2"><p>گوش تو بمن باشد و من نالم خوش</p></div></div>