---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>ای خون دل پیر و جوان خورد دلت!</p></div>
<div class="m2"><p>وی ز آهن و از سنگ گرو برده دلت!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار، میازار دلم، میترسم</p></div>
<div class="m2"><p>گردد ز دل آزردنم، آزرده دلت!</p></div></div>