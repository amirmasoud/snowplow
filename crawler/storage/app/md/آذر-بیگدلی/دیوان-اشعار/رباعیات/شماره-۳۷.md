---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>د رجان، از داغ عشق، سوزم بگرفت</p></div>
<div class="m2"><p>در دل، سوزی ز دلفروزم بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخندیدم به تیره روزان شب و روز</p></div>
<div class="m2"><p>تا آه کدام تیره روزم بگرفت؟!</p></div></div>