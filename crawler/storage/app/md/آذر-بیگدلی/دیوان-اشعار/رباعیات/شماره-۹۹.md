---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>هر مرغ که نامه ی من از تیهو و باز</p></div>
<div class="m2"><p>آرد سوی تو، ناید از آن کوی تو باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه عجبی فتاده ما را بمیان</p></div>
<div class="m2"><p>کز یک طرف است بسته، از یک سو باز</p></div></div>