---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست، اسیران تو یک یک رفتند؛</p></div>
<div class="m2"><p>وز دام تو طایران زیرک رفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو، که جرس ناله کنان میگوید:</p></div>
<div class="m2"><p>یک قافله دل ز کویت اینک رفتند</p></div></div>