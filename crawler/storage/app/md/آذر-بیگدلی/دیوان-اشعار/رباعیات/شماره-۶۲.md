---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>هاروت، بجزع چشم بندت ماند!</p></div>
<div class="m2"><p>یاقوت، بلعل نوشخندت ماند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشاد، بسرو سر بلندت ماند!</p></div>
<div class="m2"><p>خورشید، بماه دلپسندت ماند!</p></div></div>