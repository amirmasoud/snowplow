---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>تا کی شب و روز و روز، شب از سر سوز</p></div>
<div class="m2"><p>نالم ز فراقت ای مه مهر افروز؟!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی: شب و روزی دهمت دل، چه شود</p></div>
<div class="m2"><p>کان شب امشب باشد و آن روز امروز</p></div></div>