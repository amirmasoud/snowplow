---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>در گلشن روزگار میگردیدم</p></div>
<div class="m2"><p>از هر شاخی، تازه گلی میچیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هم نفسان رفته میکردم یاد</p></div>
<div class="m2"><p>هر جا گل دسته بسته یی میدیدم</p></div></div>