---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>رفت آن کاغیار دشمن بودند</p></div>
<div class="m2"><p>برق حاصل، آتش خرمن بودند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دشمنیت، کنون بمن دوست شدند</p></div>
<div class="m2"><p>آنان که ز دوستیت دشمن بودند</p></div></div>