---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>شب بیتو چو باشم، سحرم ناید باز</p></div>
<div class="m2"><p>ور با تو نشینم، رسدم صبح فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حیرتم از کار شب خود، کآن شب</p></div>
<div class="m2"><p>چون با تو بود کوته و، چون بی تو دراز؟!</p></div></div>