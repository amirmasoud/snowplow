---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>هر بار که سرگردان ز من یار گذشت</p></div>
<div class="m2"><p>گفتم که: چنین ز بیم اغیار گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشت و نبود غیر با او، افغان</p></div>
<div class="m2"><p>کاین بار هم از برم چو هر بار گذشت</p></div></div>