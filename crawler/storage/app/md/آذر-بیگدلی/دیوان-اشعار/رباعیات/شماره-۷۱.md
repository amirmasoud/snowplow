---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>تو شاه و شهانت ز هواخواهانند</p></div>
<div class="m2"><p>تو ماه و مهانت همه همراهانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهان جهانند گدایان درت</p></div>
<div class="m2"><p>یعنی که گدایان درت، شاهانند</p></div></div>