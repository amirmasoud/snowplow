---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>از هجر، مرا هم مژه گوهر پوش است</p></div>
<div class="m2"><p>دل از ستمت بناله، لب خاموش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشم ترم، بیتو در افشان شب و روز؛</p></div>
<div class="m2"><p>اما نه از آن در، که تو را در گوش است</p></div></div>