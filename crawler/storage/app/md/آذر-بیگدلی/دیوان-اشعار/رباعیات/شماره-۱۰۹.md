---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>یا رب مپسند کعبه گردد ناووس</p></div>
<div class="m2"><p>دهقان گیرد کاسه، ز دست کاووس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ساعد روسبی نشیند شاهین</p></div>
<div class="m2"><p>در خانه ی روستا خرامد طاووس</p></div></div>