---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>میخانه، ز کارگاه مانی خوشتر</p></div>
<div class="m2"><p>مینای می، از برد یمانی خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این آب، ز آتش جوانی خوشتر</p></div>
<div class="m2"><p>این آتش، از آب زندگانی خوشتر</p></div></div>