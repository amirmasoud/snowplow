---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>حالم ز غم هجر، چه ناخوش حال است</p></div>
<div class="m2"><p>حالی که ز گفتنش زبانم لال است!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم چشم نگه ز چشمی و چه چشم؟!</p></div>
<div class="m2"><p>چشمی که هزار چشمش از دنبال است!</p></div></div>