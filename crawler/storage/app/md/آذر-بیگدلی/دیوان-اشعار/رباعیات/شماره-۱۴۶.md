---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>دی دست نگارین بت سیمین تن من</p></div>
<div class="m2"><p>بر گردن من فگند و شد رهزن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جرم که بر گردن از آن دستش بود</p></div>
<div class="m2"><p>از گردن خود فگند بر گردن من</p></div></div>