---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>من بیتو دریده هر نفس پیرهنی</p></div>
<div class="m2"><p>بی من تو نشسته هر زمان در چمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون بمن و تو نیست کس را سخنی</p></div>
<div class="m2"><p>من از چو تویی دورم و، تو از چو منی</p></div></div>