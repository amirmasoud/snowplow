---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>خالی، که چو داغ لاله در سینه ی اوست</p></div>
<div class="m2"><p>هندوست که پاسبان گنجینه ی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه سینه بسینه ی من از مهر نهاد</p></div>
<div class="m2"><p>نه داغ دلم عکس بر آیینه ی اوست</p></div></div>