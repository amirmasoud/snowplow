---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>چل سال، براه عاشقی کردم سیر</p></div>
<div class="m2"><p>گفتم: شودم عاقبت کار بخیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که باین روز فتادم، نالم</p></div>
<div class="m2"><p>از چشم خوش تو، یا ز چشم بد غیر؟!</p></div></div>