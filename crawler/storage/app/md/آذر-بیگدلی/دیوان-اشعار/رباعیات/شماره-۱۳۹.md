---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>ای در هوس روی تو، روشن رایان!</p></div>
<div class="m2"><p>وی در طلب کوی تو، بزم آرایان!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیهات بکوی تو توانیم رسید</p></div>
<div class="m2"><p>ما بی پایان و، راه ما بی پایان!</p></div></div>