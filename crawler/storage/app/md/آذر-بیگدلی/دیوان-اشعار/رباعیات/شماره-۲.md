---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>آن مرغ، که ناله همنفس بود او را</p></div>
<div class="m2"><p>در کنج قفس، باغ هوس بود او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد نغمه سرای طرف باغ، اما کو؟</p></div>
<div class="m2"><p>آن ناله، که در کنج قفس بود او را؟!</p></div></div>