---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ز آزردن جان خسته جانی بگذر</p></div>
<div class="m2"><p>از قتل من پیر، جوانی بگذر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکرانه ی بازوی توانا، صیاد!</p></div>
<div class="m2"><p>از کشتن صید ناتوانی بگذر!</p></div></div>