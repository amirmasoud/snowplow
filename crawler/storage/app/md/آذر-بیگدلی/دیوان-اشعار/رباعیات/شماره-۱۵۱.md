---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>آن زلف، که صید مرغ دل کرده ز چین</p></div>
<div class="m2"><p>بس خون جگر بمشک پرورده ز چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی بعجب ز طره ی مشکینش</p></div>
<div class="m2"><p>کو خورده شکست و غارت آورده ز چین</p></div></div>