---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>تن ز آتش غم، چو عود می بین و مپرس</p></div>
<div class="m2"><p>آهم بلب کبود می بین و مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس را خبر از درون آتشکده نیست</p></div>
<div class="m2"><p>از روزنه هاش دود می بین و مپرس</p></div></div>