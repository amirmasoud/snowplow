---
title: >-
    شمارهٔ ۴۷ - حکایت
---
# شمارهٔ ۴۷ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم که: آزاده یی پاک زاد</p></div>
<div class="m2"><p>که والا گهر بود وعالی نژاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر کرد بر تخت گاه کیان</p></div>
<div class="m2"><p>بجای هما، بسته جغد آشیان!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آشفتگی دید در روزگار</p></div>
<div class="m2"><p>بلندی و پستیش بی اعتبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحر خسروی دید بر روی تخت</p></div>
<div class="m2"><p>شبانگه بویرانه یی برده رخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گدایی بشب حسرت قوت داشت</p></div>
<div class="m2"><p>سحر تاج بر سر ز یاقوت داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهم زشت و زیبا هم آغوش دید</p></div>
<div class="m2"><p>خسک با گل و نیش با نوش دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر دید با زهر آمیخته</p></div>
<div class="m2"><p>بدامان گل، خار آویخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تنگی گیتی دلش گشت تنگ</p></div>
<div class="m2"><p>دلش تنگ شد از جهان دو رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدرویشی از خواجگی دل نهاد</p></div>
<div class="m2"><p>بفرد امل، خط باطل نهاد!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از آن پیشتر کش ببندند رخت</p></div>
<div class="m2"><p>برون برد رخت خود آن نیکبخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرش ز افسر فقر زیور گرفت</p></div>
<div class="m2"><p>تنش نقش از بوریا برگرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بویرانه یی رفت و تنها نشست</p></div>
<div class="m2"><p>ره خویش و بیگانه، بر خویش بست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه از قصر شاهان بسر سایه اش</p></div>
<div class="m2"><p>نه از گنج عالم بکف مایه اش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی گفتش از دوستداران نغز</p></div>
<div class="m2"><p>که: بیرون کش از پوست ای دوست مغز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگو تا چه دیدی ز وضع جهان</p></div>
<div class="m2"><p>که کردی ز اهل جهان رخ نهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز عیش جهان چشم بستی، چرا؟!</p></div>
<div class="m2"><p>ز قصر شهان پا شکستی، چرا؟!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بپاسخ چنین گفتش آن نیکمرد</p></div>
<div class="m2"><p>که: بشنو اگر هستی از اهل درد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان را همی بینم آن تازه باغ</p></div>
<div class="m2"><p>که هستش گل و بلبل و خار و زاغ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بمن باغبان چون نبخشد گلش</p></div>
<div class="m2"><p>دریغ آید از نغمه ی بلبلش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه با خار او دست یازی کنم</p></div>
<div class="m2"><p>چه با زاغ آن نوحه سازی کنم؟!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان چیست؟ پیمانه یی پر می است!</p></div>
<div class="m2"><p>که هم صاف و هم درد می در وی است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز صافش، چو ساقی نپیمایدم؛</p></div>
<div class="m2"><p>ز دردش چه نوشم که درد آیدم؟!</p></div></div>