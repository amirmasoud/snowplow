---
title: >-
    شمارهٔ ۲۶ - حکایت
---
# شمارهٔ ۲۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>یکی روز رفتم بباغی ز کاخ</p></div>
<div class="m2"><p>که گل رخت بر بسته بودش ز شاخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم سوخت بر بلبل تیره بخت</p></div>
<div class="m2"><p>که نالیدی از هجر گل بر درخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندیدی چو روی گل اندر میان</p></div>
<div class="m2"><p>پریدن همی خواستی ز آشیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بآن بینوا گفتم: اینک هنوز</p></div>
<div class="m2"><p>نرفته است از رفتن گل دو روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی چون ز سر منزل دوستان</p></div>
<div class="m2"><p>نه جای گل است آخر این بوستان؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمان تا جهان نو کند عهد گل</p></div>
<div class="m2"><p>بگلبن فرود آورد مهد گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بایدت گل، مرو زینهار</p></div>
<div class="m2"><p>که گر زود یا دیر، آید بهار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ابر بهار و ز باد شمال</p></div>
<div class="m2"><p>شود ساحت باغ مینو مثال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هر شاخ، در خنده آید گلی</p></div>
<div class="m2"><p>ز هر آشیان، بر پرد بلبلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنالید آن بلبل تنگدل</p></div>
<div class="m2"><p>که زخمم مزن بر دل ای سنگدل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باین با غم آورد گل از نخست</p></div>
<div class="m2"><p>نخستم بگل گشت پیمان درست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بشوق گل این باغ شد منزلم</p></div>
<div class="m2"><p>درین منزل آسود از گل دلم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل و جانم از بوی گل گشت مست</p></div>
<div class="m2"><p>درین گلشنم داشت گل پای بست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شب و روز با گل در این بوستان</p></div>
<div class="m2"><p>مرا بود بس رازها د رمیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر شاخم از شاخ پرواز بود</p></div>
<div class="m2"><p>هزارم هم آواز دمساز بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون کآمد ایام دولت بسر</p></div>
<div class="m2"><p>ازین باغ گل بست بار سفر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روم من هم از پی، چو گل زار رفت</p></div>
<div class="m2"><p>نماند بجا دل، چو دلدار رفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگوشم ز گل داستانها بسی است</p></div>
<div class="m2"><p>بهر شاخم از گل نشانها بسی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آنها بیاد آیدم هر نشان</p></div>
<div class="m2"><p>چکد خونم از دیده ی خون فشان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پس از گل، بگلشن نشینم چرا؟!</p></div>
<div class="m2"><p>نبینم چو گل، خار بینم چرا؟!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا عیش بر خویش سازم حرام؟</p></div>
<div class="m2"><p>ز غوغای زاغان ناخوش خرام؟!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون میروم تا گل آید بباغ</p></div>
<div class="m2"><p>هم از بوی گل بلبل آید بباغ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفت این و نالیدن آغاز کرد</p></div>
<div class="m2"><p>بنالید و از شاخ پرواز کرد</p></div></div>