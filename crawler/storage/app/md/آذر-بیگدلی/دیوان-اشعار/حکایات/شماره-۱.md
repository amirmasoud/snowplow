---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>شبی میگذشتم ز ویرانه یی</p></div>
<div class="m2"><p>ز پا دیدم افتاده دیوانه یی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دیوانه، فرزانه یی حق شناس</p></div>
<div class="m2"><p>چه دیو و چه دیوانه زو در هراس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان گشته یی، خضرش از همرهان؛</p></div>
<div class="m2"><p>جهان دیده یی، بسته چشم از جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه جز ز آسمان، سایه یی بر سرش</p></div>
<div class="m2"><p>نه جز موی سر، جامه یی در برش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ره فقر پیموده با پای لنگ</p></div>
<div class="m2"><p>فراخ آستین بوده با دست تنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشیدایی آسوده خاطرز شید</p></div>
<div class="m2"><p>همه عمر آزاد از عمرو و زید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلش گنج اسرار حق را امین</p></div>
<div class="m2"><p>نه او از کسی نه کسی زو غمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخودگفتی، از خودشنفتی بسی؛</p></div>
<div class="m2"><p>مرنجان کسی را، مرنج از کسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه با آسمان کینی از وی گمان</p></div>
<div class="m2"><p>نه ز او کینه یی در دل آسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم او گردن عجز افراشته</p></div>
<div class="m2"><p>هم از وی فلک دست برداشته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرسنه، ولی سیر از ناز و نوش</p></div>
<div class="m2"><p>برهنه، ولی خلق را عیب پوش!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تهی دست و، پا بر سر گنجهاش</p></div>
<div class="m2"><p>ز فاقه دل آسوده از رنجهاش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو گز بوریا کرده بر خاک فرش</p></div>
<div class="m2"><p>زده دست کوتاه، بر ساق عرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اقامت گزین در مقام رضا</p></div>
<div class="m2"><p>رضا داده جانش بحکم قضا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرش مست عشق و، دلش هوشیار</p></div>
<div class="m2"><p>لبش خنده ریز و مژه اشکبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گهی خنده میکرد و گه میگریست!</p></div>
<div class="m2"><p>باو گفتم: این خنده و گریه چیست؟!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه دیدی بگو گرنه یی ز اهل زرق</p></div>
<div class="m2"><p>که گریان چو ابری و خندان چو برق؟!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو دیوانه افسانه ی من شنفت</p></div>
<div class="m2"><p>فشاند اشک چون شمع خندان و گفت:</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه پرسی؟! که گر گویمت سرگذشت</p></div>
<div class="m2"><p>ز عیش جهان بایدت در گذشت!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو راکام شیرین، مرا باده تلخ؛</p></div>
<div class="m2"><p>تو از غره گویی سخن من ز سلخ!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خردمند را، بی خرد یار نیست</p></div>
<div class="m2"><p>به آسوده، فرسوده را کار نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زدم بوسه بر دستش آنگه بپای</p></div>
<div class="m2"><p>که ای دانش آرای فرخنده رای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منم تشنه کام و، تو بارنده میغ؛</p></div>
<div class="m2"><p>ز تشنه چرا آب داری دریغ؟!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا از کرم باش آموزگار</p></div>
<div class="m2"><p>بگو تا چه ها دیدی از روزگار؟!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز وضع جهان هر چه دیدی بگو</p></div>
<div class="m2"><p>ز بیننده هم گر شنیدی بگو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل ازعجز نالی من سوختش</p></div>
<div class="m2"><p>چو شمع آتش من رخ افروختش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همش های های و، همش قاه قاه؛</p></div>
<div class="m2"><p>همی گفت: ای خضر گم کرده راه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگو زین سرای سیاه و سفید</p></div>
<div class="m2"><p>دو چشم و دو گوشم چه دید و شنید؟!</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر آنچه شنیدستم از روزگار</p></div>
<div class="m2"><p>شمارم، کشد تا بروز شمار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه خوش گفت پیر پسندیده گوی</p></div>
<div class="m2"><p>سخن هر چه میگویی از دیده گوی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنونم مزن طعنه ها، هوش باش</p></div>
<div class="m2"><p>چو از دیده گویم سخن، گوش باش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببین تا چه دیدم ز دور سپهر</p></div>
<div class="m2"><p>ز اندوه و شادی، ز کین و ز مهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شود تا تو را هم دل آگه ز راز</p></div>
<div class="m2"><p>نه پرسی از این خنده و گریه باز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم اینجا در اندک زمانی نه دیر</p></div>
<div class="m2"><p>که از خودپرستان شدم گوشه گیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی ژرف دریا بدیدم نخست</p></div>
<div class="m2"><p>که موج غبارش رخ ابر شست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سفاین خرامنده چون بط بسش</p></div>
<div class="m2"><p>لبالب ز در دامن هر خسش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بآن لجه ریزان بسی شد بشط</p></div>
<div class="m2"><p>درون پر زماهی، برون پر ز بط</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در اصداف رخشان در از هر طرف</p></div>
<div class="m2"><p>چو دری در این لاجوردی صدف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بغواصی الیاسی از هر کنار</p></div>
<div class="m2"><p>بر آورده بس لؤلؤ شاهوار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشیده از آن دست گوهر فروش</p></div>
<div class="m2"><p>شهان را بتاج و بتان را بگوش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پی صحبت خضر گفتی کلیم</p></div>
<div class="m2"><p>بگسترده در ساحل آن گلیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فروزنده ماهیش چون آفتاب</p></div>
<div class="m2"><p>در افتاده از دست موسی در آب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شناور سمکها به پشت وشکم</p></div>
<div class="m2"><p>زر وسیم ریزان بدامان یم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بقعرش ز ساحل زر ماهیان</p></div>
<div class="m2"><p>چو سیمین کواکب ز گردون عیان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه سرگرانی بهم داشتند</p></div>
<div class="m2"><p>مگر یونس اندر شکم داشتند؟!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بصید بط و ماهیش صبح و شام</p></div>
<div class="m2"><p>بهر گوشه صیادی افگنده دام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مگر ناخدای خدا ناشناس</p></div>
<div class="m2"><p>نپذرفت از غرقه یی التماس</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پس آن غرقه را موج هر سو کشاند</p></div>
<div class="m2"><p>در آخر دم از دیده خونی فشاند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ببین قطره خونی بدریا چه کرد؟!</p></div>
<div class="m2"><p>ز بنیاد آن چون برآورد گرد؟!</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چنان ناگه آن بحر طوفان گرفت</p></div>
<div class="m2"><p>که ماندند از آن فیلسوفان شگفت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فرو بست دم آن خروشنده یم</p></div>
<div class="m2"><p>نه نامی بجا ماند از آن یم، نه نم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هم آنجا عیان شد یکی پهن دشت</p></div>
<div class="m2"><p>پرنده پر افشان، چرنده بگشت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سفاین ز گرداب بر گل نشست</p></div>
<div class="m2"><p>شدش ناخدا غرق و لنگر شکست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روان هر شکاری بکف تیغ تیز</p></div>
<div class="m2"><p>نموده بمرغابیان رستخیز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز خون بطان بطن یم شد یمن</p></div>
<div class="m2"><p>درش چون عقیق یمن در ثمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز غوک و ز ماهی آن بحر شور</p></div>
<div class="m2"><p>شکم ساخته پر وحوش و طیور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر آمد ز تر دامنی خاک خشک</p></div>
<div class="m2"><p>ره گاو عنبر زد آهوی مشک</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فگنده در این جاده ها کاروان</p></div>
<div class="m2"><p>در آن جاده ها کاروان ها روان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گدایان مفلس ز رخشنده در</p></div>
<div class="m2"><p>صدف کرده خالی و زنبیل پر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بسا بی کله سرور عهد شد</p></div>
<div class="m2"><p>بسا پا برهنه که بر مهد شد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز مرجان و در برده چندان بدوش</p></div>
<div class="m2"><p>که هر پیله ور گشت گوهر فروش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کنون مانده ز آنجا دو روشن سمک</p></div>
<div class="m2"><p>بهفتم زمین و بهشتم فلک</p></div></div>