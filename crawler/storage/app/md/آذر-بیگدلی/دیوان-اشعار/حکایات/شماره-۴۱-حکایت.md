---
title: >-
    شمارهٔ ۴۱ - حکایت
---
# شمارهٔ ۴۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>بمن دوستی گفت از دوستان</p></div>
<div class="m2"><p>که با من همی گشت در بوستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که: از روزگارم غمی در دل است</p></div>
<div class="m2"><p>که ناگفتن و گفتنش مشکل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا هست رازی نهان در جهان</p></div>
<div class="m2"><p>کنون خواهم آن راز گویم نهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفخر زمان خان گیلان زمین</p></div>
<div class="m2"><p>کش از عدل شه گله را گرگ امین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزرگ است و دانا و آزاده مرد</p></div>
<div class="m2"><p>ز درمان نشاید نهان داشت درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانم، ولی کی توان جست راه</p></div>
<div class="m2"><p>نهان از رقیبان در آن بزمگاه؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خضر ار کنی رهنمایی مرا</p></div>
<div class="m2"><p>ازین قید بخشی رهایی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم خشک لب، اوست بارنده میغ؛</p></div>
<div class="m2"><p>چرا آب از تشنه داری دریغ؟!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این بیش مپسند ای آموزگار</p></div>
<div class="m2"><p>ستم بر ستمدیده ی روزگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منش گفتم: ای سالک ره نورد</p></div>
<div class="m2"><p>ز درد خود آوردیم دل بدرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بناید دریغم ز مقصود تو</p></div>
<div class="m2"><p>زیانی مرا نیست از سود تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خانه یی کاو بدولت گشاد</p></div>
<div class="m2"><p>ز کس نیست خالی که خالی مباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کند مجلس آن دم تهی از کرم</p></div>
<div class="m2"><p>که ریزد بدامان سائل درم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز شرم کسان بسکه شرم آیدش</p></div>
<div class="m2"><p>بوقت عطا خلوتی بایدش</p></div></div>