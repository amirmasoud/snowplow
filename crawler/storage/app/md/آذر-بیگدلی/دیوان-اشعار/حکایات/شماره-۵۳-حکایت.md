---
title: >-
    شمارهٔ ۵۳ - حکایت
---
# شمارهٔ ۵۳ - حکایت

<div class="b" id="bn1"><div class="m1"><p>بفرمان حجاج شوریده بخت</p></div>
<div class="m2"><p>که بودش زبان تلخ و گفتار سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاندند بر نطع فوجی اسیر</p></div>
<div class="m2"><p>بگفتش یکی ز آن میان کای امیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی روز در مجلس راستان</p></div>
<div class="m2"><p>همی زد ز تو هر کسی داستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی خواست نامت بزشتی برد</p></div>
<div class="m2"><p>منت پرده نگذاشتم بر درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهایی ده امروزم از زیر تیغ</p></div>
<div class="m2"><p>مفرمای در حقگزاری دریغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن بینوا بیکس بیگناه</p></div>
<div class="m2"><p>طلب کرد حجاج ظالم گواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگفتش: یکی زان اسیران مرا</p></div>
<div class="m2"><p>گواه است از آن پرس و این ماجرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نپوشید چشم از حق ن مرد نیز</p></div>
<div class="m2"><p>چنین گفت در زیر شمشیر تیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که : آری در آن روز این حق پرست</p></div>
<div class="m2"><p>ز نیکی زبان بداندیش بست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خروشید کز بیم این رستخیز</p></div>
<div class="m2"><p>نکردی چرا منع بدگو تو نیز؟!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپاسخ چنین گفت آن بیگناه</p></div>
<div class="m2"><p>که: ای از جفای تو روزم سیاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه جای عتاب است با من تو را</p></div>
<div class="m2"><p>که بودم من آن روز دشمن تو را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خوی توام چون نبود ایمنی</p></div>
<div class="m2"><p>شگفتت نیاید ز من دشمنی!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ظلمت، خدا در نظر داشتم</p></div>
<div class="m2"><p>از امروز گویا خبر داشتم!</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو این حرف بشنود از آن تنگدل</p></div>
<div class="m2"><p>بر آن هر دو بخشود آن سنگدل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بگفتا: به ایشان مدارید کار</p></div>
<div class="m2"><p>که این راست گویست و آن دوستدار!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هم از راستی رستگاری رسد</p></div>
<div class="m2"><p>هم از دوستی دوستداری رسد!!</p></div></div>