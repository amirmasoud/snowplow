---
title: >-
    شمارهٔ ۴۸ - حکایت
---
# شمارهٔ ۴۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>جهانگردی از خیل کارآگهان</p></div>
<div class="m2"><p>سیاحت همیکرد گرد جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره افتادش، از انقلاب سپهر</p></div>
<div class="m2"><p>شبانگه بدریای مغرب چو مهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر افگنان شد بدریا کنار</p></div>
<div class="m2"><p>مگر عبرتی گیرد از روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصیادی افتاد چشمش نخست</p></div>
<div class="m2"><p>که هر صید را کرده دامی درست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه ماهی تازه و نغز و پاک</p></div>
<div class="m2"><p>گرفتی ز آب و فگندی بخاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته به پهلوی آن صید گیر</p></div>
<div class="m2"><p>یکی سرو قد دختر دلپذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چوماهی عیان دختر اندر میان</p></div>
<div class="m2"><p>ز اطراف او ریخته ماهیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو گفتی که در برج حوت آفتاب</p></div>
<div class="m2"><p>کشیده است از چهره ی خودنقاب!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پدر هر چه ماهی کشیدی بدام</p></div>
<div class="m2"><p>بخاکش کشاندی بسعی تمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر باره آن دختر محترم</p></div>
<div class="m2"><p>در آبش فگندی ز راه کرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بپرسید سیاح از آن ماه چهر</p></div>
<div class="m2"><p>که ای مهر روی تو در جان مهر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن ضایع اینگونه رنج پدر</p></div>
<div class="m2"><p>بودناخلف، دزد گنج پدر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین گنج ضایع پسندی چرا؟!</p></div>
<div class="m2"><p>گره کو گشاید، تو بندی چرا؟!</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپای پدر بایدت سر نهاد</p></div>
<div class="m2"><p>که از ماهی ای ماه قوت تو داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه آخر تو را مایه از گنج اوست؟!</p></div>
<div class="m2"><p>نه آخر تو را راحت از رنج اوست؟!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پدر را که شد رنج کش زینهار</p></div>
<div class="m2"><p>مرنجان که رنجاندت روزگار!</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بپاسخ چنین گفتش آن نوش لب</p></div>
<div class="m2"><p>که: دورم مدان زینهار از ادب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیم ناخلف، نیست خونم هدر؛</p></div>
<div class="m2"><p>کزین پیشتر گفت با من پدر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که: صیدی که،غافل شد از ذکر حق</p></div>
<div class="m2"><p>بود دام صیاد را مستحق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنین صید، بر خود پسندم چرا؟!</p></div>
<div class="m2"><p>دل خود، باین قوت بندم چرا؟!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفتم که قوتی جز این نیستم</p></div>
<div class="m2"><p>ز جوع ار بمیرم، حزین نیستم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرا قوت من گردد این صیدخام؟!</p></div>
<div class="m2"><p>که از حق شود غافل، افتد بدام؟!</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پدر را گرین قوت بهر من است</p></div>
<div class="m2"><p>شکر داند او لیک زهر من است!</p></div></div>