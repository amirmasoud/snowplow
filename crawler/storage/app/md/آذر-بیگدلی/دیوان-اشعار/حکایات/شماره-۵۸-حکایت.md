---
title: >-
    شمارهٔ ۵۸ - حکایت
---
# شمارهٔ ۵۸ - حکایت

<div class="b" id="bn1"><div class="m1"><p>هست مروی در احادیث حسن</p></div>
<div class="m2"><p>از حسین بن علی، کان ممتحن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمین کربلا میشد شهید</p></div>
<div class="m2"><p>در میان خاک و خون، خوش میطپید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد از سلطان معشوقان ندا</p></div>
<div class="m2"><p>کای براه دوست کرده جان فدا!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داده در راه وفا فرزند و زن</p></div>
<div class="m2"><p>کشته ی تیغ جفا، از عشق من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرزویت چیست؟ یک یک بر شمار</p></div>
<div class="m2"><p>تا گذارم آرزویت در کنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت: میخواهم ز تو هفتاد جان</p></div>
<div class="m2"><p>تا کنم یک یک نثارت در زمان!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز آمد حضرت روح الامین</p></div>
<div class="m2"><p>این پیام آورد از عرش برین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کای جهانت در وفا داری خجل</p></div>
<div class="m2"><p>کرده این نامه بنام خود سجل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر وفا کردی بوعد از صادقی است</p></div>
<div class="m2"><p>این شهادت منتهای عاشقی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حد معشوقی است بالاتر از این</p></div>
<div class="m2"><p>را ز نتوان گفت پیداتر از این</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون شنید این حرف، شاه دین حسین؛</p></div>
<div class="m2"><p>گفت: این خون بر رخ من باد زین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داشتی چون میل با این مشت خاک</p></div>
<div class="m2"><p>دادم او را و گرفتم جان پاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حمد لله، ثم حمدلله که دوست</p></div>
<div class="m2"><p>دید کش جان دادنم در راه اوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس کن آذر، این زبان دیگر است</p></div>
<div class="m2"><p>این زبان را گوش دیگر در خور است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صبح شد، ای شمع آتش دم مسوز</p></div>
<div class="m2"><p>دم فروکش، آشکارا گشت روز!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تنگ شد دل، ناله را تأثیر ده</p></div>
<div class="m2"><p>نغمه کم کن، رنگ را تغییر ده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان هر کس، محرم این راز نیست</p></div>
<div class="m2"><p>گوش هر کس، لایق این ساز نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بویی از خون شهیدان صبح و شام</p></div>
<div class="m2"><p>میخورد در راه عشقم، بر مشام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داغ ها دارم، بدل از ماهها</p></div>
<div class="m2"><p>خیزدم از سینه هر شب آهها</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دردها دارم گمان از رشکها</p></div>
<div class="m2"><p>ریزدم از دیده هر شب اشکها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از فلک دارم بسینه سوزها</p></div>
<div class="m2"><p>آه ازین شبها، فغان زین روزها!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جانم، از این محنت و اندوه کاست؛</p></div>
<div class="m2"><p>روز امیدی، شب وصلی کجاست؟!</p></div></div>