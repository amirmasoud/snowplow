---
title: >-
    شمارهٔ ۲۷ - حکایت
---
# شمارهٔ ۲۷ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم یکی روز بر طرف دشت</p></div>
<div class="m2"><p>جوانی بدهقان پیری گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خوی ا رخ افشاندش آفتاب</p></div>
<div class="m2"><p>همی کشت نخل و همی داد آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوان را شگفت آمد از کار وی</p></div>
<div class="m2"><p>چنین گفت با پیر فرخنده پی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که: اکنون از پیری ای نیکبخت</p></div>
<div class="m2"><p>همی لرزدت تن چو برگ درخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کاری درختی که ناید بکار</p></div>
<div class="m2"><p>از آن نه شکوفه ببینی نه بار؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انصاف اگر نگذری این عمل</p></div>
<div class="m2"><p>دهد یاد از حرص و طول امل!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپاسخ چنین گفت دهقان پیر</p></div>
<div class="m2"><p>که: ای نوجوان خرده بر من مگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خوردیم ما کشته ی دیگران</p></div>
<div class="m2"><p>که بودند تخم وفا پروران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکاریم تا کشته ی ما خورند</p></div>
<div class="m2"><p>مگر نام ما را به نیکی برند</p></div></div>