---
title: >-
    شمارهٔ ۳۴ - حکایت
---
# شمارهٔ ۳۴ - حکایت

<div class="b" id="bn1"><div class="m1"><p>شنیدم ز شیبانیان غیور</p></div>
<div class="m2"><p>مگر ارقم بن کلیب از غرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشورید بر معن بن زایده</p></div>
<div class="m2"><p>زیان دید از آن کار بیفایده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیازید چون بر زبر دست دست</p></div>
<div class="m2"><p>ز دست زبر دست دستش شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز آتش کین شان بود گرم</p></div>
<div class="m2"><p>برافروخت رخسار ارقم ز شرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تنهایی آمد بدربار معن</p></div>
<div class="m2"><p>که تا جانش آساید از تیر طعن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازو معن پرسید کای ذوفنون</p></div>
<div class="m2"><p>بگو تا چه آوردت اینجا کنون؟!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آوردت اینجا دلیری و زور</p></div>
<div class="m2"><p>هم ازپای خود آمدستی بگور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و گر چاپلوسیت آورد، ریو؛</p></div>
<div class="m2"><p>فرشته نلغزد بافسون دیو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمین بوسه زد ارقمش پیش تخت</p></div>
<div class="m2"><p>که فیروزه تختی و فیروز بخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین در نیاورد دامن کشم</p></div>
<div class="m2"><p>جز امید بخشایش و بخششم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون کز گنه لب گزان آمدم</p></div>
<div class="m2"><p>بر این آستان، فرد از آن آمدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که دانی نزد سر خطائی ز کس</p></div>
<div class="m2"><p>بجز من ازین بیگناهان و بس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرت عدل گردن فرازی کند</p></div>
<div class="m2"><p>سرم بر سر نیزه بازی کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ورت عفو خندد بروی گناه</p></div>
<div class="m2"><p>بس آید بر این آستان عذر خواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رخ معن از این عذر چون گل شکفت</p></div>
<div class="m2"><p>بروی گنه کار خندید و گفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که: هان خاطر ای ارقم از غم رهان</p></div>
<div class="m2"><p>گذشتم ز جرم تو و همرهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فشاندش بسر گنج در پای رنج</p></div>
<div class="m2"><p>نشاندش چو ماران ارقم بگنج</p></div></div>