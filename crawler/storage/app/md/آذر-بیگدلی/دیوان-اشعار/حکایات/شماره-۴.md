---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>بجایش یکی باغ دیدم شگرف</p></div>
<div class="m2"><p>که فردوس از نزهتش بسته طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا، طاق سیمابی افراخته</p></div>
<div class="m2"><p>زمین، فرش زنگاری انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن باغبانان زرینه کفش</p></div>
<div class="m2"><p>بکف بیلشان کاویانی درفش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر سو خیابانی آراسته</p></div>
<div class="m2"><p>ز خار و خسش سبزه پیراسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم اشجار آن را دم جبرئیل</p></div>
<div class="m2"><p>هم انهار آن را نم سلسبیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرافراز سرو سهی قد چنار</p></div>
<div class="m2"><p>کشیده دو صف بر لب جویبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو یاران یکدل بهم پای بست</p></div>
<div class="m2"><p>در آغوش یکدیگر آورده دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درختانش از میوه قد کرده خم</p></div>
<div class="m2"><p>چه از حمل گنجینه، گنجور جم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گردن فروزان صاحب کرم</p></div>
<div class="m2"><p>سرافگنده از شرم و ریزان درم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رنگینی میوه هر شاخ بست</p></div>
<div class="m2"><p>تو گفتی زده چتر، طاووس مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه، مشک با خاکش آمیخته</p></div>
<div class="m2"><p>همه، گوهر از تاکش آویخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شعری ز شام و سهیل از یمان</p></div>
<div class="m2"><p>گل از خار و لاله ز خارا دمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر افروخته چون کلاه قباد</p></div>
<div class="m2"><p>چراغ گل و مشعل لاله باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز ریحان آن، مغز شب مشکبیز؛</p></div>
<div class="m2"><p>ز نسرین این، صبح کافور ریز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهر موسمی خاصه اردی بهشت</p></div>
<div class="m2"><p>بآن خاک سوگند خوردی بهشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نظر باز هر گوشه مرغ چمن</p></div>
<div class="m2"><p>بدوشیزگان گل و یاسمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوش آواز مرغان آن پرفشان</p></div>
<div class="m2"><p>چه طوطی ز منقار شکرفشان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گل سرخ و سرو سرافراخته</p></div>
<div class="m2"><p>ربوده دل از بلبل و فاخته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز هر سو بآن باغ و آن بوستان</p></div>
<div class="m2"><p>خرامیده با هم بسی دوستان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بساغر کشی، هر دو آزاده بخت</p></div>
<div class="m2"><p>نشستند در سایه ی یک درخت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بعشرت گرفتند ساغر ز هم</p></div>
<div class="m2"><p>تهی کرده مینا ز می، دل ز غم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نهاده سر مست در پای تاک</p></div>
<div class="m2"><p>بچشم اختران را فشاندند خاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر باغ را باغبانی سحر</p></div>
<div class="m2"><p>بروی تماشائیان بست در!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>و یا کند از باغ شاخ گلی</p></div>
<div class="m2"><p>که افتاد از آشیان بلبلی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز ابری سیه ریخت ناگه تگرگ</p></div>
<div class="m2"><p>نه بار اندر آن باغ ماند و نه برگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز سبزه چنان دامن خاک شست</p></div>
<div class="m2"><p>که گویی گیاهی در آنجا نرست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رزان را بنه کرد یغما خزان</p></div>
<div class="m2"><p>وز آن برگ نگذاشت باد وزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سراسر درختان این کند و رفت</p></div>
<div class="m2"><p>همه برگش از هم پراگند و رفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگلبن در آویخت ابری کبود</p></div>
<div class="m2"><p>تو گویی ز آتشکده خاست دود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر طره ی سنبل آشفته ماند</p></div>
<div class="m2"><p>بسا حرف سوسن که ناگفته ماند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد آشفته چون شاخ نرگس شکست</p></div>
<div class="m2"><p>چه کوری که افتد عصایش ز دست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پریدند قمری و بلبل ز باغ</p></div>
<div class="m2"><p>بحال چمن، نوحه کردند زاغ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خس و خار، پیرهن گل درید</p></div>
<div class="m2"><p>زغن آشیان بست وبلبل پرید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نگون گشت شمشاد و افتاد سرو</p></div>
<div class="m2"><p>خروشان و نالان چکاو و تذرو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرفتند مرغان از آنجا کران</p></div>
<div class="m2"><p>چه از مجلس سوک، رامشگران</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همان میگساران، همان دوستان؛</p></div>
<div class="m2"><p>که بودند با هم بیک بوستان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو گل ساغر از دست افتادشان</p></div>
<div class="m2"><p>چو بلبل نوا رفت از یادشان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همه گشته در سایه ی تاک خاک</p></div>
<div class="m2"><p>بر اندامشان شد کفن برگ تاک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نشد فاش گویند راز نهفت</p></div>
<div class="m2"><p>سخن گفتشان، در میان نیم گفت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو مانداز خرابی آن تازه باغ</p></div>
<div class="m2"><p>بدل از گلم خار واز لاله داغ</p></div></div>