---
title: >-
    شمارهٔ ۳۲ - حکایت
---
# شمارهٔ ۳۲ - حکایت

<div class="b" id="bn1"><div class="m1"><p>دو زن داشت مردی دو مو، پیش ازین</p></div>
<div class="m2"><p>سواری دو اسب آمدش زیر زین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ز آن دو پیر، آن دگر خردسال</p></div>
<div class="m2"><p>قد آن و ابروی این چون هلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی اژدهاوش، یکی مه جبین</p></div>
<div class="m2"><p>رخ آن و گیسوی این پر ز چین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر یک شبی مهد آراستی</p></div>
<div class="m2"><p>فزودیش این آنچه آن کاستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن شب که پیرش هم آغوش بود</p></div>
<div class="m2"><p>بخواب عدم رفته بیهوش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بناخن همه شب زن حیله گر</p></div>
<div class="m2"><p>ز رویش سیه موی کندی مگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بموی سفیدش چه افتد نگاه</p></div>
<div class="m2"><p>بچشم آیدش عالم از غم سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شود از زن نوجوان بدگمان</p></div>
<div class="m2"><p>که با هم نسازند تیر و کمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمد ز آن جوان، شد چو در روزگار</p></div>
<div class="m2"><p>جوان با جوان پیر با پیر یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر شب چو خفتی بمهد جوان</p></div>
<div class="m2"><p>نبودش بتن از کسالت توان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهانی ز جا خاستی آن نگار</p></div>
<div class="m2"><p>کشیدیش موی سفید از عذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که فردا چو بیند سیه موی خود</p></div>
<div class="m2"><p>بگرداند از پیرزن روی خود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سحرگه در آیینه ی آفتاب</p></div>
<div class="m2"><p>چو دیدند رخسار خود شیخ و شاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز مشاطه ی صبح عالم فروز</p></div>
<div class="m2"><p>جدا گشت زلف شب از روی روز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آیینه چون دید آن دردمند</p></div>
<div class="m2"><p>بچشم آمدش صورت ریشخند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر سو نظر کرد از هیچ سوی</p></div>
<div class="m2"><p>ندید از زنخ تا بناگوش موی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دلش خون، تنش موی شد، سینه ریش؛</p></div>
<div class="m2"><p>بخندید و بگریست بر روز خویش!</p></div></div>