---
title: >-
    بخش ۱۲ - جواب
---
# بخش ۱۲ - جواب

<div class="b" id="bn1"><div class="m1"><p>زهی پاکیزه قول و نیک تحقیق</p></div>
<div class="m2"><p>سؤالی سودمند از روی تدقیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سؤالی رسته از آلایش فرش</p></div>
<div class="m2"><p>بفرش آورده رو از باطن عرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل از گل گر بروید پاک روید</p></div>
<div class="m2"><p>چه جای آنکه از افلاک روید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سؤالاتی چو دسته غنچه گل</p></div>
<div class="m2"><p>چو باز آید دل از آن گلشن و کل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بهر هدیه ارباب تکمیل</p></div>
<div class="m2"><p>فرستد بر صراط وحی و تنزیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ثماری رسته از شاخ درایت</p></div>
<div class="m2"><p>که بالیدست از بیخ ولایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهاری روح پرور چون دم روح</p></div>
<div class="m2"><p>نجات ماسوی چون کشتی نوح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهشتی طره حورش دلاویز</p></div>
<div class="m2"><p>نظر گاه قصورش معرفت خیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلستانی ترست و سبز و خرم</p></div>
<div class="m2"><p>گلست و یاسمینش اسم اعظم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهانی از قیود ملک خالی</p></div>
<div class="m2"><p>زمین و آسمان او مثالی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمینی پادشاهش دولت دوست</p></div>
<div class="m2"><p>سپهری کافتابش طلعت اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیاری حاکم او جلوه یار</p></div>
<div class="m2"><p>که در او نیست غیر از یار دیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نباشد کس درینجا یار تنهاست</p></div>
<div class="m2"><p>چه باشد اینکه گفتم حاکم ماست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مر این گفتار بر ما حتم کردن</p></div>
<div class="m2"><p>سخن را در ولایت ختم کردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدایا ده بمن نور هدایت</p></div>
<div class="m2"><p>مرا ده غوص در بحر ولایت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ازین دریا گهر باید ربودن</p></div>
<div class="m2"><p>پس از آن گوهر تحقیق سودن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بپای این سؤال گوهر افشان</p></div>
<div class="m2"><p>فشاندن گوهر شهوار غلتان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سؤال از درج اللهیست گوهر</p></div>
<div class="m2"><p>جواب از بحر دل لولوی دیگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگنج ای در سؤالت صد معالی</p></div>
<div class="m2"><p>بنه عقد ل آلی بر ل آلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگیر ای در فقیری برده بس رنج</p></div>
<div class="m2"><p>بنه این گنج دولت بر سر گنج</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سؤال از بحر علمست و جوابش</p></div>
<div class="m2"><p>ز دریائی که لاهوتست آبش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زدی سر خدا را حلقه بر در</p></div>
<div class="m2"><p>گشودندت در اسرار مضمر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیا ای دیده در فرقت بسی گاز</p></div>
<div class="m2"><p>بگیر این گوهر گنجینه راز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آ ای کرده بر قانون ما سیر</p></div>
<div class="m2"><p>ببزم ما گشودندت در خیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیا ساقی از آن صهبای بیغش</p></div>
<div class="m2"><p>بیاور تازنم بر آب و آتش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من و دل برق سیر و باد ساریم</p></div>
<div class="m2"><p>که خضر بر و الیاس بحاریم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ازین دست و ازین می هر دو مستیم</p></div>
<div class="m2"><p>بدور چشم ساقی می پرستیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که دریای ولایت بی کنارست</p></div>
<div class="m2"><p>کسی داند که سباح بحارست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مرا زین بحر امکان گذر نیست</p></div>
<div class="m2"><p>ولیکن چاره ئی از این سفر نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می استی ناخدای قلزم روح</p></div>
<div class="m2"><p>بساغر کن که باشد کشتی نوح</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدریای دل ای یار بهشتی</p></div>
<div class="m2"><p>بغیر از ساغر می نیست کشتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو نوح من بدریا افکند فلک</p></div>
<div class="m2"><p>ب آهنگ ملک از خطه ملک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کند طی بحار عالم دل</p></div>
<div class="m2"><p>رود تا مجمع البحرین کامل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زند از ساغر لا صاف الا</p></div>
<div class="m2"><p>بگیرد کار او از نفی بالا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که مرد نیستی در دار مستی</p></div>
<div class="m2"><p>بود دائر مدار دور هستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می من عشق آن یار قلندر</p></div>
<div class="m2"><p>که ایجادش باطوارست مضمر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بود او نقطه و ادوار و اکوار</p></div>
<div class="m2"><p>باو پیوسته چونان خط پرگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا او در سرست و سینه و دل</p></div>
<div class="m2"><p>کجا باشد که او را نیست منزل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بوحی دل کند ما را هدایت</p></div>
<div class="m2"><p>بما آموزد اسرار ولایت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نهد در جیب جانم رشته در</p></div>
<div class="m2"><p>ز گوهر دامن دل را کند پر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا بنشسته بر صدر سویدا</p></div>
<div class="m2"><p>بچشم سر من چون روز پیدا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دو چشم من بروی او بود باز</p></div>
<div class="m2"><p>دو گوشم یار را باشد ب آواز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زبان اوست قیوم بیانم</p></div>
<div class="m2"><p>تو گوئی جای دارد بر زبانم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بقول قدسی انکس را که با اوست</p></div>
<div class="m2"><p>زبان و چشم و گوش و دست و پا اوست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدرس معرفت استاد تعلیم</p></div>
<div class="m2"><p>ولایت را کند بر چار تقسیم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدرک و دید بی انکار و تردید</p></div>
<div class="m2"><p>بعام و خاص و بر اطلاق و تقیید</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بود مرعام ظل اسم رحمن</p></div>
<div class="m2"><p>که عیسی راست در آن حکم و فرمان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ولی خاصست ظل اسم الله</p></div>
<div class="m2"><p>که احمد راست در او رتبه و جاه</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>و لیستی خدا مر مؤمنین را</p></div>
<div class="m2"><p>ز قرآن کرد بتوان درک این را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ولایت قرب و قرب امر اضافیست</p></div>
<div class="m2"><p>دو سو دارد مگو یک سوی کافیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ولی هم خدا و مؤمنینند</p></div>
<div class="m2"><p>خداوندان دل بر طبق اینند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بود عیسی ولی ختم مطلق</p></div>
<div class="m2"><p>بایمان نی باسم ذاتی حق</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>که این دولت بدین معراج اسعد</p></div>
<div class="m2"><p>بود معراج درویشان احمد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دثار احمد والامقامست</p></div>
<div class="m2"><p>پس از او خرقه اول امامست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رسد از دوش بر دوش این تلبس</p></div>
<div class="m2"><p>رساند فیض بر آفاق و انفس</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>فشاند نور بر ایجاد دائم</p></div>
<div class="m2"><p>بود دائم ردای دوش قائم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بود بی سر بکرو صورت عمرو</p></div>
<div class="m2"><p>طراز کشف سر صاحب الامر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو عنوان دوئی بر خاست از بین</p></div>
<div class="m2"><p>یکی ماند که باشد بود را عین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کسی کو صاحب الامرست مطلق</p></div>
<div class="m2"><p>بود در امر هستی صاحب حق</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>که عین بود باشد ذات وحدت</p></div>
<div class="m2"><p>عیان هستیست از مرآت وحدت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>وجود از وحدت خود نیست منفک</p></div>
<div class="m2"><p>خدا موجود بی همتاست بی شک</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خدا تا هست همراهست مهدی</p></div>
<div class="m2"><p>ولی اسم اللهست مهدی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بهر دور و بهر کورش سرایت</p></div>
<div class="m2"><p>که در چرخست گردون ولایت</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز شرق مهدی این شید جهانتاب</p></div>
<div class="m2"><p>دمید و شد پدید از جان اقطاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که اقطابند در تعلیم وارث</p></div>
<div class="m2"><p>قوای نفس را در بعث باعث</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ادیب عشق را شاگرد هوشند</p></div>
<div class="m2"><p>بوحی غیب استاد سروشند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شدند از فضل فیض ختم مرسل</p></div>
<div class="m2"><p>ز کل انبیا این قوم افضل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مکین بزم جمع الجمع ذاتند</p></div>
<div class="m2"><p>باسماء الهی امهاتند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>رجال بارگاه جمع جمعند</p></div>
<div class="m2"><p>تمام بود بزم اقطاب شمعند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مدام از نشاه توحید مستند</p></div>
<div class="m2"><p>سر افرازان سر مست الستند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز سر تا پای دریای وجودند</p></div>
<div class="m2"><p>چو گوهر در تک عمان جودند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بنه از خود سر مستکبری را</p></div>
<div class="m2"><p>بکن از بیخ اصل منکری را</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ز علم عشق کن در راه مرکب</p></div>
<div class="m2"><p>چه تازی مرکب جهل مرکب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مقام علم را از جهل بشناس</p></div>
<div class="m2"><p>اگر دانشوری از جهل بهراس</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو دیدی کاملی در سرزمینی</p></div>
<div class="m2"><p>بصیری کار دانی راز بینی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بخلوتخانه او خاک در باش</p></div>
<div class="m2"><p>بنه سر پیش پای او و سر باش</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ز دنبالش بپوی این هفت وادی</p></div>
<div class="m2"><p>که در این راه بینی روی هادی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدون علم در خلوت نشستن</p></div>
<div class="m2"><p>در تعلیم را بر روی بستن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بود از کوی هادی دور بودن</p></div>
<div class="m2"><p>ز دیدار حقیقت کور بودن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بودهم بزم و همزانوی مهدی</p></div>
<div class="m2"><p>ولی جاهل نبیند روی مهدی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>که او قطبست مرا قطاب حق را</p></div>
<div class="m2"><p>ازو آموخت قطبیت سبق را</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بجان هر دلی از او تجلیست</p></div>
<div class="m2"><p>دل مقصود را از او تسلیست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جمال وصف را او حسن ذاتست</p></div>
<div class="m2"><p>که ذات ختم موضوع صفاتست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>صفات ذات وصف خاتم ماست</p></div>
<div class="m2"><p>که از نقصان اسمائی مبراست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ببازوی محمد زور بازوست</p></div>
<div class="m2"><p>علی را در ولایت هم ترازوست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>محمد ختم و او ختم و علی ختم</p></div>
<div class="m2"><p>تجلی های او در کاملی ختم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>نباشد معنی ختم ای برادر</p></div>
<div class="m2"><p>که بعد از او نباشد ختم دیگر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ندارد ختم جز این معنی و بس</p></div>
<div class="m2"><p>که بر حق نیست زو نزدیکتر کس</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>که ختمستی علی بعد از پیمبر</p></div>
<div class="m2"><p>پس از او خاتمند اولاد یکسر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>که ختمند اولیای احمد پاک</p></div>
<div class="m2"><p>همه پاکند از آلایش خاک</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>که در هر دور غوشستی مهذب</p></div>
<div class="m2"><p>که از او بر خدا کس نیست اقرب</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>اگر مجموعه مجموع اسماست</p></div>
<div class="m2"><p>بود او ختم مطلق بی کم و کاست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>اگر بر جزء اسمای محمد</p></div>
<div class="m2"><p>مسمی شد بود ختم مقید</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نباشد گر وجود غوث مشهود</p></div>
<div class="m2"><p>کمال اسم اعظم نیست موجود</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شنیدستم بدرس مدرس دل</p></div>
<div class="m2"><p>که اسم اعظمست انسان کامل</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>خدا پیدا و اسم اوست پیدا</p></div>
<div class="m2"><p>باسم اوست قائم کل اشیا</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اگر انسان نباشد دل نباشد</p></div>
<div class="m2"><p>چو دل نبود ره و منزل نباشد</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>چو نبود منزل و ره ارتقا نیست</p></div>
<div class="m2"><p>که جز طی منازل در خدا نیست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خدای لم یزل در دل مقیمست</p></div>
<div class="m2"><p>که این بیت از بناهای قدیمست</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>نگنجد در زمین ها و اسمانها</p></div>
<div class="m2"><p>بدل گنجد که فردست از مکانها</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>نه قطره ست و نه جودریاست ایندل</p></div>
<div class="m2"><p>که درج گوهر یکتاست ایندل</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>دل عارف چو آید در طلاطم</p></div>
<div class="m2"><p>ازو زاید هزاران بحر قلزم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>هزاران بحر چبود کل هستی</p></div>
<div class="m2"><p>ز دل خیزد بگاه ذوق و مستی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ببام دل زند شاه ابد کوس</p></div>
<div class="m2"><p>نه بر هر بام بی بنیان منکوس</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>مقام اسم اعظم نیست جز دل</p></div>
<div class="m2"><p>نگین خاتم جم نیست جز دل</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>نباشد آنی از موجود انسان</p></div>
<div class="m2"><p>نباشد حق و در حق نیست نقصان</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بهر آنیست انسان فرد و قائم</p></div>
<div class="m2"><p>ولی الله باشد آن دائم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>نباشد آن دائم کن فکان نیست</p></div>
<div class="m2"><p>ولی نبود زمین و آسمان نیست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>زمین و آسمان را ظل دل دان</p></div>
<div class="m2"><p>ولی دل را بدلبر متصل دان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>نه آن وصلت که از ما و توئی زاد</p></div>
<div class="m2"><p>من و ما و تو از مام دوئی زاد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بعینیت رسید از این توصل</p></div>
<div class="m2"><p>خداوند بقا شد از تبدل</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>بعین ذات اسما را مدد شد</p></div>
<div class="m2"><p>مدیر و احدیت شد احد شد</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نهاد از موطن تفصیل مجمل</p></div>
<div class="m2"><p>شه آخر قدم بر صدر اول</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>فنای ذات او در عین توحید</p></div>
<div class="m2"><p>رهاند او را ز امکانات تحدید</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>چو از تحدید رست آن ظل ممتد</p></div>
<div class="m2"><p>فرو پیوست با انوار بیحد</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ز ظل عاشقی آن ماه ممشوق</p></div>
<div class="m2"><p>تسلط یافت بر خورشید معشوق</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>پس از بیگانگیها آشنا شد</p></div>
<div class="m2"><p>گذشت از عاشقی معشوق ما شد</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ولی ختم خاص سر محمود</p></div>
<div class="m2"><p>بفرق ما سوی الله ظل ممدود</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بود هم مهدی و هم هادی کل</p></div>
<div class="m2"><p>باطلاق از مقامات تبدل</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بود موجود در ادوار و اکوار</p></div>
<div class="m2"><p>نباشد غیر او در دار دیار</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>وجود مطلق از او در ترانه</p></div>
<div class="m2"><p>تو میگوئی که موجودست یا نه</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>مباد از جامه بینش کسی عور</p></div>
<div class="m2"><p>تواند ظلمت و عالم پر از نور</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>کسی کز جلوه مهدیست بی بهر</p></div>
<div class="m2"><p>بود در ده اگر شاهست در شهر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>کسی کز این تجلی کامگارست</p></div>
<div class="m2"><p>اگر در ده بود شاه دیارست</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>خدایا بینش ما را قوی کن</p></div>
<div class="m2"><p>صراط ما صراط مستوی کن</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>که انوار حقیقت در کنارست</p></div>
<div class="m2"><p>میان ما منیت پرده دارست</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>اگر این پرده را پرداختم من</p></div>
<div class="m2"><p>باقلیم حقیقت تاختم من</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر این پرده هستی بجاماند</p></div>
<div class="m2"><p>خدا رفت و هدی رفت و هوا ماند</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>تو ماندی بی خدا ای خفته بر گنج</p></div>
<div class="m2"><p>که بر گنج از گدائی میبری رنج</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>طلب رحمن عرش از قلب انسان</p></div>
<div class="m2"><p>که باشد قلب انسان عرش رحمن</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>منه پا از در دل جای دیگر</p></div>
<div class="m2"><p>بفرق ماسوی نه پای دیگر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>که چون شد دل بعزم خویش جازم</p></div>
<div class="m2"><p>ولی الله باشد بلکه خاتم</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>شه ذوالامر النصرست گو باش</p></div>
<div class="m2"><p>ولی و والی عصرست گو باش</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>ز دل بادا درود از روح تحسین</p></div>
<div class="m2"><p>بجان اولیاء احمدیین</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>که سر غیب را فصل الخطابند</p></div>
<div class="m2"><p>ظهور باطن ختمی م آبند</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>زدند از دولت ختم رسالت</p></div>
<div class="m2"><p>ببام لم یزل کوس جلالت</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>نگردد سیر احمد تا ابدگم</p></div>
<div class="m2"><p>بود این بحر دائم در تلاطم</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بود گه جام و گه می گاه ساقی</p></div>
<div class="m2"><p>ولی الله مادامست باقی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>دوئی بر کند بند و بیخ خرگاه</p></div>
<div class="m2"><p>علم زد آیت الملک لله</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>بدل شد کائنات پیچ در پیچ</p></div>
<div class="m2"><p>بیکتائی خدا ماند و دگر هیچ</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>که گر بیننده با چشم صفا دید</p></div>
<div class="m2"><p>بدیوار و بدر نور خدا دید</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>جزین تقسیم در قسطاس اکبر</p></div>
<div class="m2"><p>ولایت را بود تقسیم دیگر</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>دو قسمست این غنی الذات مفرد</p></div>
<div class="m2"><p>نخستین مطلق و ثانی مقید</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>بود وصف الهی سر مطلق</p></div>
<div class="m2"><p>مقید گردد از اقطاب بر حق</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بذات هر ولی آن مطلق الذات</p></div>
<div class="m2"><p>شود مخصوص چونان شخص و مرآت</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ولی مرآت پیش شخص منفیست</p></div>
<div class="m2"><p>عیان شخصست و بس مرآت مخفیست</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>که باشد حکم مرآت اینکه او را</p></div>
<div class="m2"><p>نبیند کس چو در او دید رو را</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>ولایت بود مطلق شد مقید</p></div>
<div class="m2"><p>بقید ذات انسان مؤید</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بعین رتبه اطلاق حی بود</p></div>
<div class="m2"><p>بتقلید آمد و بالذات وی بود</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ز اطلاق ازل آن سر سرشار</p></div>
<div class="m2"><p>بتقلید مؤید شد گرفتار</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>گرفتاران تقلید نهایت</p></div>
<div class="m2"><p>بدام افتاده بند ولایت</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>ز قید ما سوای یار رسته</p></div>
<div class="m2"><p>ز خود بگسسته و با یار بسته</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>برون زد خیمه از آفاق و انفس</p></div>
<div class="m2"><p>مبرا شد ز تشبیه و تقدس</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>شد از وارستن تقیید و اطلاق</p></div>
<div class="m2"><p>امیر انفس و سلطان آفاق</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>سر سلطان کل بیگانه اوست</p></div>
<div class="m2"><p>دل درویش دولتخانه اوست</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>شه ار بی او بود نقشیست برگاه</p></div>
<div class="m2"><p>شه برگاه نقش جان آگاه</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>که بشناسد مقام احمد و آل</p></div>
<div class="m2"><p>دهد تمییز مهدی را ز دجال</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>زند زین نفس چون دجال گردن</p></div>
<div class="m2"><p>نهد گردن بعشق هادی من</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>که باشد هادی من سر توحید</p></div>
<div class="m2"><p>ز تقییدات امکانی بتجرید</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>که در فن نظر هادیست برهان</p></div>
<div class="m2"><p>بقانون مکاشف جذبه جان</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>براهین از بدایات شهودند</p></div>
<div class="m2"><p>علوم حقه انحاء وجودند</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>مکاشف را براهین از بدایت</p></div>
<div class="m2"><p>کند در تیه حیرانی هدایت</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>چو برهان هادی این صعب وادیست</p></div>
<div class="m2"><p>بود روشن که برهان نور هادیست</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>نظر با کشف همراز قدیمند</p></div>
<div class="m2"><p>که سلطان ولایت را ندیمند</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>ندیم پادشاهند این دو کامل</p></div>
<div class="m2"><p>مکین لامکان صفه دل</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>نظر بی کشف لا حق معتبر نیست</p></div>
<div class="m2"><p>چنو کشفی که مسبوق نظر نیست</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>بدون یکدگر چون خاک خوارند</p></div>
<div class="m2"><p>ولی با هم چو گردون استوارند</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>که در تفریق عباد عبادند</p></div>
<div class="m2"><p>بجمعیت خداوند رشادند</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>اگر در فرق سرگردان و پستند</p></div>
<div class="m2"><p>بجمعیت سرافراز الستند</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>بصحرای دوئی صید نزارند</p></div>
<div class="m2"><p>بنیزار احد شیر شکارند</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>زاثنینیت خود در حجابند</p></div>
<div class="m2"><p>بچرخ واحدیت آفتابند</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>چو یکتا نیستند این هر دو هیچند</p></div>
<div class="m2"><p>دو راه خوفناک پیچ پیچند</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>بوحدت شاهراه مستقیمند</p></div>
<div class="m2"><p>صراط ربط حادث با قدیمند</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>کسی کو مجمع این هر دو دریاست</p></div>
<div class="m2"><p>اگر باشد برون از حصر یکتاست</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>بوحدت رازداری نیست جزوی</p></div>
<div class="m2"><p>نهان آشکاری نیست جزوی</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>چنینست آنکه سلطان دیارست</p></div>
<div class="m2"><p>که سر او نهان و آشکارست</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>بود پیدا ولی غیب الغیوبست</p></div>
<div class="m2"><p>که آبسکون امکان و وجوبست</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>نشسته در مقام قاب قوسین</p></div>
<div class="m2"><p>نه قابش در متی نه قوس در این</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>ز وضعست و متی و این بیرون</p></div>
<div class="m2"><p>بود در چون و باشد سر بیچون</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>ولی خاص ختم المرسلینست</p></div>
<div class="m2"><p>سزای رفع اثنینیت اینست</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>بگردون ولایت نیز گه گاه</p></div>
<div class="m2"><p>یکی خورشید باشد دیگری ماه</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بدین سیرند ارباب مکارم</p></div>
<div class="m2"><p>که این شمس و قمر راهست خاتم</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>یکی چون آفتاب بی کسوفست</p></div>
<div class="m2"><p>یکی ماه مبرا از خسوفست</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>ولایات شموسی را صف حی</p></div>
<div class="m2"><p>بود در پیش و اقماریست از پی</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>فروغ شمس شرق هوست بالذات</p></div>
<div class="m2"><p>قمر را شمس برهاند ز ظلمات</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>بدین تاء/سیس و این تحقیق لابد</p></div>
<div class="m2"><p>بود خاتم پذیرای تعدد</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>ولایت چار باشد ختم او چار</p></div>
<div class="m2"><p>یکی گردند در توحید ناچار</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>که آن یک باشد از تعداد بیرون</p></div>
<div class="m2"><p>منزه باشد از چند و چه و چون</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>عدد کمست و او وارسته از کم</p></div>
<div class="m2"><p>گرفته دامن توحید محکم</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>کشیده رخت در بنگاه تجرید</p></div>
<div class="m2"><p>که عریانیست رخت گاه تجرید</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>کند چون گردد از هر جامه عریان</p></div>
<div class="m2"><p>حقیقت را برون سر از گریبان</p></div></div>