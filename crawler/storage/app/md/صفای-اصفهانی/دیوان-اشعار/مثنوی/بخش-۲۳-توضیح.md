---
title: >-
    بخش ۲۳ - توضیح
---
# بخش ۲۳ - توضیح

<div class="b" id="bn1"><div class="m1"><p>ز من بنیوش هان اسرار دیگر</p></div>
<div class="m2"><p>بطرز دیگر و گفتار دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکم را در پی توضیح تمییز</p></div>
<div class="m2"><p>مهیا شو که رحمت گشت سرریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجود کامل ما فیض عامست</p></div>
<div class="m2"><p>بخلق وجد او فوق التمامست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزاران دور باید تا که مردی</p></div>
<div class="m2"><p>ببیند روی قطبی یا که فردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو قطبی دم فروزد در معارف</p></div>
<div class="m2"><p>ز دم پی میتوان بردن بعارف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که سلطان حقیقت بی نیازست</p></div>
<div class="m2"><p>ولی این در بروی خلق بازست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخدمت قامت همت علم کن</p></div>
<div class="m2"><p>چو کلک من سر خود را قدم کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپای اهل بینش خاک شو خاک</p></div>
<div class="m2"><p>که این پایت سری بخشد بافلاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتفسیر و بتوضیح و بتاء/ویل</p></div>
<div class="m2"><p>مهیا شو که آمد وحی جبریل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود منطوق گفتار شریعت</p></div>
<div class="m2"><p>که باشد علم رفتار طریقت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شریعت با طریقت هر دو منطوق</p></div>
<div class="m2"><p>حقیقت برترست از درک مخلوق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود مسکوت اسرار حقیقت</p></div>
<div class="m2"><p>خرد حیران شد از کار حقیقت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حقیقت برتر از حد بیانست</p></div>
<div class="m2"><p>بیان بیکار شد وقت عیانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عیانست آنکه ناپیدا و پیداست</p></div>
<div class="m2"><p>حقیقت در سر و سر سویداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بود در جمله و از جمله بیرون</p></div>
<div class="m2"><p>زهی شاء/ن و زهی اسرار بیچون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شریعت را بدان و شرح کن سهل</p></div>
<div class="m2"><p>طریقت را مگو در نزد نااهل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حقیقت را بدان نیک ار کنی فاش</p></div>
<div class="m2"><p>هدف گردد بتیر طعن اوباش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به نتوان گفت نزد عام اسرار</p></div>
<div class="m2"><p>که خاصان گفته اندی بر سر دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که من با نامه و با خامه این راز</p></div>
<div class="m2"><p>نگویم نیستند این هر دو دمساز</p></div></div>