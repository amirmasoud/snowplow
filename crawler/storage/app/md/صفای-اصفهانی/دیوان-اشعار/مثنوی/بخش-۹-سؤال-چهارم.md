---
title: >-
    بخش ۹ - سؤال چهارم
---
# بخش ۹ - سؤال چهارم

<div class="b" id="bn1"><div class="m1"><p>شهان دیدند در ره رهزن و غول</p></div>
<div class="m2"><p>نهادندی بنای رتبه بر طول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخستین رتبه علم الیقین است</p></div>
<div class="m2"><p>دوم عین الیقین مستبین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیم حق الیقین لولوی تدقیق</p></div>
<div class="m2"><p>بدین ترتیب سفتند اهل تحقیق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سرست اینکه قومی ز اهل آداب</p></div>
<div class="m2"><p>نهادندی بنای علم بر آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدون علم باشد سالها بین</p></div>
<div class="m2"><p>ز حد جهل تا سر منزل عین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنای رتبه بر طولست بی ریب</p></div>
<div class="m2"><p>بچشم جهل نتوان دید غیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نبود علم گر نور مبینست</p></div>
<div class="m2"><p>چه جای دعوی حق الیقینست</p></div></div>