---
title: >-
    بخش ۱۴ - جواب
---
# بخش ۱۴ - جواب

<div class="b" id="bn1"><div class="m1"><p>بیاور ساقی آن صاف صفا خیز</p></div>
<div class="m2"><p>که بدهد صاف را از درد تمییز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نداری صاف ده درد از خط اشک</p></div>
<div class="m2"><p>که صاف آفتاب از او برد رشک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیاور هر چه داری درد یا صاف</p></div>
<div class="m2"><p>که می صافست و درد و صاف ز اوصاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرابی ریز دور از رنگ و از بو</p></div>
<div class="m2"><p>بمینای من از میخانه هو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که این میخانه پرورد قدیمست</p></div>
<div class="m2"><p>خمش در خانه سر حکیمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بن خم هشته در بنیان جودست</p></div>
<div class="m2"><p>بنای خانه مبنای وجودست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجود می بلا نقص بسیطست</p></div>
<div class="m2"><p>تمام دور هستی را محیطست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرابم کن بکلی از می ذات</p></div>
<div class="m2"><p>مرا بر نفی موکولست اثبات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آنستم که از این می پرستی</p></div>
<div class="m2"><p>کنم در نیستی تدبیر هستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فنای ذات رهرو راست مقصود</p></div>
<div class="m2"><p>مقام نفی باشد جای محمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو پنداری که من مست و خرابم</p></div>
<div class="m2"><p>مگر مستی سپس بیند بخوابم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که من مستم ولی مست وجودم</p></div>
<div class="m2"><p>خراب غیب و آباد شهودم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو داری باده ئی در خورد مستان</p></div>
<div class="m2"><p>بجام تست جان می پرستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا کن زنده در این حال مردن</p></div>
<div class="m2"><p>که باید جان بی پایان سپردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که از یک جان سپردن لطف جانان</p></div>
<div class="m2"><p>دهد هر مرده ئی را هفتصد جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دهد یکدانه هفصد دانه حاصل</p></div>
<div class="m2"><p>بنص آیه سبع سنابل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرم باشد کنم قربان ساقی</p></div>
<div class="m2"><p>بهر دم هفتصد جان جمله باقی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که او از ما بگیرد جان فانی</p></div>
<div class="m2"><p>دهد جانی که گوئی من ر آنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر چه لن ترانی نیست باطل</p></div>
<div class="m2"><p>مباش از من رآنی نیز غافل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که باشد در تجلی های انوار</p></div>
<div class="m2"><p>ز احمد تا بموسی فرق بسیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بود موسی هنوز اندر سماوات</p></div>
<div class="m2"><p>رسول مصطفی ممسوس بالذات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بود احمد بذات الله ممسوس</p></div>
<div class="m2"><p>تجلی های ذاتی راست ماء/نوس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بجنبد تا بسنبد بال این طیر</p></div>
<div class="m2"><p>براند تا بماند رفرف از سیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شود بالای او ادنای قوسین</p></div>
<div class="m2"><p>نشیمنگاه شاه کون بی این</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهم ریزد بنه و بنگاه محمود</p></div>
<div class="m2"><p>نماند غیر وجه الله محمود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ببام حق مزن کوس سوائی</p></div>
<div class="m2"><p>نماند احمدی ماند خدائی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زند بر بام قدس ذات مطلق</p></div>
<div class="m2"><p>ندای من رآنی قدراء/ی الحق</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باورنگ کمال عز سرمد</p></div>
<div class="m2"><p>خدا بنشست چون برخاست احمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلی از جان مردان هنرمند</p></div>
<div class="m2"><p>چو برخیزند بنشیند خداوند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر حق گفت سبحانی عجب نیست</p></div>
<div class="m2"><p>کسی را غیر حق این گفت و لب نیست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو شد بر با یزید از حق تجلی</p></div>
<div class="m2"><p>باستغراق گفت این قول اعلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو باز آمد ازان غیب و ازان هول</p></div>
<div class="m2"><p>بوی گفتند سر زد از تو این قول</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز روی عجز با حق گفت در راز</p></div>
<div class="m2"><p>که ای کوینده بی شبه و انباز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنند این قول را از من روایت</p></div>
<div class="m2"><p>بمن این قوم در طی حکایت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر من گفتمی سبحانی از خود</p></div>
<div class="m2"><p>شد ستم کافر و گبرو بدودد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نمودم زین خطا گفتن ستغفار</p></div>
<div class="m2"><p>شدم مؤمن نمودم قطع زنار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون گویم که گشتم سالک راه</p></div>
<div class="m2"><p>هو الله الذی لا غیره الله</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو باز از ذات اول شد ب آخر</p></div>
<div class="m2"><p>تجلی گشت مظهر عین ظاهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سرایت کرد سر ذات بر ذات</p></div>
<div class="m2"><p>شه شطرنج علم و عین شد مات</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حقیقت شد پدید از ذوق و مستی</p></div>
<div class="m2"><p>نماند از بایزید پیر هستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درین دریا ز سر تا پای شد گم</p></div>
<div class="m2"><p>بخویش این بحر آمد در تلاطم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زنای بایزید این قول شد راست</p></div>
<div class="m2"><p>که ذات لم یزل در جبه ماست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نمود از پای خلع نعل امکان</p></div>
<div class="m2"><p>خدا پیدا شد و کونین پنهان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دوئی از احولی خیزد شکی نیست</p></div>
<div class="m2"><p>بچشم راست بین حق جز یکی نیست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو راند از این دوتائی رخش سالک</p></div>
<div class="m2"><p>بیکتائی بهر ملکست مالک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کسی کز این دوتائی رست یکتاست</p></div>
<div class="m2"><p>سوای ذات او کس نیست پیداست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بجز حق در مکان و لا مکان نیست</p></div>
<div class="m2"><p>نشان از کس بکوی بی نشان نیست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درین میخانه مستانند بیحد</p></div>
<div class="m2"><p>گروهی بیخود و قومی معربد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یکی افتاده از مستی بیک دوش</p></div>
<div class="m2"><p>نموده کل هستی را فراموش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یکی دریا کشیدست و طلبکار</p></div>
<div class="m2"><p>حریفان خفته او بنشسته بیدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بوحدت رتبه از اندازه بیشست</p></div>
<div class="m2"><p>مقام هر کسی بر حد خویشست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بحد خویش هر کس را زبانیست</p></div>
<div class="m2"><p>بوحدت هر زبانی را بیانیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بیان هر زبان از حد برونست</p></div>
<div class="m2"><p>عبارات حقیقت را شئونست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شئون هر عبارت را تجلیست</p></div>
<div class="m2"><p>که عین هر عبارت عین مولیست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عبارات وجود ماست شتی</p></div>
<div class="m2"><p>مقام جمع ما بی ند و همتا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شد از این نفی و این اثبات معلوم</p></div>
<div class="m2"><p>که در این نکته اسراریست مکتوم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که گوید بایزیدی وقت گفتار</p></div>
<div class="m2"><p>نگفتم باز گفت آرد بتکرار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نشاید گفت این نطقست واهی</p></div>
<div class="m2"><p>نه بالله نیست جز نطق آلهی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بصورت چون نشیند شاه بر تخت</p></div>
<div class="m2"><p>زند بر بام دولت نوبت بخت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر نوبت زند سلطان معنی</p></div>
<div class="m2"><p>ببام دل نکاهد جان معنی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فزاید جان معنی نوبت فقر</p></div>
<div class="m2"><p>که باشد دولت الحق دولت فقر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>الهی تاج فقرم نه بتارک</p></div>
<div class="m2"><p>ردای فقر کن بر من مبارک</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>که در کوی عنایت من فقیرم</p></div>
<div class="m2"><p>تو سلطان غیور و من حقیرم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو شاهی من گدایم رسم شاهست</p></div>
<div class="m2"><p>که بخشد جرم آن کاهل گناهست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که جای بیگناهان در نعیمست</p></div>
<div class="m2"><p>کرا بجشد که رحمن و رحیمست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نبخشی گر مرا ای وای بر من</p></div>
<div class="m2"><p>که گردد خصم عقل و رای بر من</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر رحمت کنی سلطان تختم</p></div>
<div class="m2"><p>بعین فقر دولت یار بختم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من و دل هر دو همراه طریقیم</p></div>
<div class="m2"><p>فنای فقر را در ره رفیقیم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدان امید کز حیرت رهانی</p></div>
<div class="m2"><p>دو همره را کنی در فقر فانی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>مرا بنمود غواص غریبی</p></div>
<div class="m2"><p>بغوص خویش اطوار عجیبی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>فنا را سر فرو برد او بکانون</p></div>
<div class="m2"><p>ز دریای بقا آورد بیرون</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ب آتش رفت و بیرون آمد از آب</p></div>
<div class="m2"><p>الهی ده کلید فتح این باب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مرا در نطع لوح محو کن مات</p></div>
<div class="m2"><p>چو شه بنشان بصدر لوح اثبات</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جم دل را بساط سروری ده</p></div>
<div class="m2"><p>سلیمان مرا انگشتری ده</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>که دیوانند بس ناسخته در راه</p></div>
<div class="m2"><p>گریزند ار ببینند اسم الله</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>درین ره دیوانسی هست بسیار</p></div>
<div class="m2"><p>تن او بار و کله بردار و طرار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>برند این غولهای نا ممیز</p></div>
<div class="m2"><p>کله از سر، سر از تن، تن ز حیز</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو دیو نفس کافر شد مسلمان</p></div>
<div class="m2"><p>رسد کار دل سالک بسامان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو پیچد نفس این شرک دو تو را</p></div>
<div class="m2"><p>دل از وحدت برآردهای و هو را</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چو کار دل بسامان صفا شد</p></div>
<div class="m2"><p>فنا تکمیل شد دور بقا شد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>لب عین الحیوه دل نشستی</p></div>
<div class="m2"><p>ز قید ماسوای دوست رستی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زدی یک جام زاب زندگانی</p></div>
<div class="m2"><p>جهانی زنده کن از جام ثانی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو ظلمت محو شد نور مبینست</p></div>
<div class="m2"><p>مقام صحو بعد المحو اینست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بگوش من ندای محو موهوم</p></div>
<div class="m2"><p>بود صرف صدای صحو معلوم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>خدا باشد یکی در دین وحدت</p></div>
<div class="m2"><p>دوئی کفرست در آئین وحدت</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بکفر حق گرا ای پیر جاهل</p></div>
<div class="m2"><p>که خواهی مرد در این کفر باطل</p></div></div>