---
title: >-
    در مدح رکن الدوله والی خراسان
---
# در مدح رکن الدوله والی خراسان

<div class="b" id="bn1"><div class="m1"><p>صبح عیان گشت باز خلق بخواب اندرون</p></div>
<div class="m2"><p>سر ز خمار شراب برده بجیب سکون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ صراحی ز حلق در دل بط ریخت خون</p></div>
<div class="m2"><p>از دل بط خون مرغ باید خوردن کنون</p></div></div>
<div class="b2" id="bn3"><p>قومو اضاق المجال یا ایها النائمون</p>
<p>هبوا طال الرقود یا معشرا الراقدین</p></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی تا ماه من روی نشسته ز خواب</p></div>
<div class="m2"><p>گیر بکف ماه نو ریز درو آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رخ او بر فروز شعله آتش بر آب</p></div>
<div class="m2"><p>برسم هر روزه می ریز بساغر شراب</p></div></div>
<div class="b2" id="bn6"><p>برنگ آزرم گل ببوی رشک گلاب</p>
<p>صاف چو یاقوت تر پاک چو در ثمین</p></div>
<div class="b" id="bn7"><div class="m1"><p>وقت صبوحی سبو دوش بدوش آورید</p></div>
<div class="m2"><p>آذر زردشت را ز اذرنوش آورید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون سیاوش را باز بجوش آورید</p></div>
<div class="m2"><p>می زدگان را ز می باز بهوش آورید</p></div></div>
<div class="b2" id="bn9"><p>رامش جان بر زنید جان بخروش آورید</p>
<p>تا ببرید از نشاط دل ز کف رامتین</p></div>
<div class="b" id="bn10"><div class="m1"><p>آذر ما هست می با دل خرم بیار</p></div>
<div class="m2"><p>از بط عیسی بطون طینت مریم بیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در غم خرداد ماه باده بی غم بیار</p></div>
<div class="m2"><p>خرمی ماه را رطل دمادم بیار</p></div></div>
<div class="b2" id="bn12"><p>طور دلم را بجان زلزله یم بیار</p>
<p>نور کف موسوی جلوه ده از آستین</p></div>
<div class="b" id="bn13"><div class="m1"><p>نیست اگر گل چه باک ای پسر گلعذار</p></div>
<div class="m2"><p>آرمل اندر میان کار گل اندر کنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیز و بیارا ز روی بزم چو روی بهار</p></div>
<div class="m2"><p>مل ز لب می پرست گل ز رخ لاله سار</p></div></div>
<div class="b2" id="bn15"><p>می چو یمانی عقیق لاله چو چینی نگار</p>
<p>عقیق چونان شهاب نگار چون حور عین</p></div>
<div class="b" id="bn16"><div class="m1"><p>گر ندهی می مرا دل ببرد جان ز پی</p></div>
<div class="m2"><p>دلبر من کن بجام تا خط سر شارمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روح دم از آن شراب در رگ و در خون و پی</p></div>
<div class="m2"><p>رسته کن آئین جم شسته کن آثار کی</p></div></div>
<div class="b2" id="bn18"><p>در گذر از این و آن تا کی و تا چندهی</p>
<p>گوئی از کیقباد جوئی از آبتین</p></div>
<div class="b" id="bn19"><div class="m1"><p>نقش یمانی ز جام ای پسر ساده آر</p></div>
<div class="m2"><p>یعنی یک ساتکین عقیق وش باده آر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زمردین خط بتا ز لعل بیجاده آر</p></div>
<div class="m2"><p>کرده جم آنچ از نخست بهر من آماده آر</p></div></div>
<div class="b2" id="bn21"><p>باده اگر آوری بیاد شهزاده آر</p>
<p>چو شعر من روح بخش چو گفته من متین</p></div>
<div class="b" id="bn22"><div class="m1"><p>رکن الدوله مهین شهزاده کامگار</p></div>
<div class="m2"><p>آنکه همال پدر اوست پس از شهریار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کشور ازو بر دوام لشکر ازو برقرار</p></div>
<div class="m2"><p>دولت ازو در قوام ملک ازو استوار</p></div></div>
<div class="b2" id="bn24"><p>آنکه بعدلش نمود آب ز آتش فرار</p>
<p>چنانکه در روز جنگ گرگ ز شیر عرین</p></div>
<div class="b" id="bn25"><div class="m1"><p>هم اثر آفتاب هم قدر آسمان</p></div>
<div class="m2"><p>شاه عطارد دبیر ماه زحل پاسبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بعقل و تدبیر پیر ببخت و دولت جوان</p></div>
<div class="m2"><p>عدلش سنجی اگر بعدل نوشیروان</p></div></div>
<div class="b2" id="bn27"><p>بسبک کاه ضعیف بسنگ کوه گران</p>
<p>بر شود آن بر سپهر سر نهد این بر زمین</p></div>
<div class="b" id="bn28"><div class="m1"><p>غره غرای اوست قالی بدر منیر</p></div>
<div class="m2"><p>زرای بیضا ضیاش خود بفلک مستشیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شه صفت و شه نژاد شیر دل و شیر گیر</p></div>
<div class="m2"><p>بهتر جمع کبار مهتر جم غفیر</p></div></div>
<div class="b2" id="bn30"><p>بعزم چون پور زال بحزم چون زال پیر</p>
<p>ببخت چون کیقباد بتخت چون کی نشین</p></div>
<div class="b" id="bn31"><div class="m1"><p>چو شه بر اندام اوست قبای فرماندهی</p></div>
<div class="m2"><p>چو جم در انگشت اوست خاتم فر و بهی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر زده بر بام چرخ رایت عز و مهی</p></div>
<div class="m2"><p>همت والاش را وهم کند کوتهی</p></div></div>
<div class="b2" id="bn33"><p>الحق او را سریست در خور تاج شهی</p>
<p>اینش چتر و علم آنش تاج و نگین</p></div>
<div class="b" id="bn34"><div class="m1"><p>بتیر شاهین شکار بتیغ خارا شکاف</p></div>
<div class="m2"><p>بوقر هم وزن کوه بوقع همسنگ قاف</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روبه او راست ننگ ز شیر نر در مصاف</p></div>
<div class="m2"><p>چرخ با جلال وی کر نکند اعتراف</p></div></div>
<div class="b2" id="bn36"><p>تیرویش چون شهاب سینه بدوزد بناف</p>
<p>سینه آن چون حریر ناوک این آتشین</p></div>
<div class="b" id="bn37"><div class="m1"><p>شوکت او در فکند بکوه زلزال را</p></div>
<div class="m2"><p>صولت او زنده ساخت سطوت آجال را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز تیغ پاینده داشت خمسه و خلخال را</p></div>
<div class="m2"><p>ز تیر افکند گرد خیوق و آخال را</p></div></div>
<div class="b2" id="bn39"><p>آری مهدی کند چاره دجال را</p>
<p>جان دهد آری بخاک عیسی گردون نشین</p></div>
<div class="b" id="bn40"><div class="m1"><p>شاه فرا آسمان همت والای تست</p></div>
<div class="m2"><p>بر ز برش ماه و مهر روی تو و رای تست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زینت تاج ملوک گوهر یکتای تست</p></div>
<div class="m2"><p>رشته نظم و خلل در کف ایمای تست</p></div></div>
<div class="b2" id="bn42"><p>گر نه خطا گفته ام تخت شهی جای تست</p>
<p>آری گاه مهان در خور شاه مهین</p></div>
<div class="b" id="bn43"><div class="m1"><p>بفر و تاء/یید و بخت بیمن تشریف شاه</p></div>
<div class="m2"><p>بسای کاندر خورست کلاه عزت بماه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز چرخ بر ساز تخت ز ماه بر زن کلاه</p></div>
<div class="m2"><p>ز چرخ مه بگذران حشمت این بار گاه</p></div></div>
<div class="b2" id="bn45"><p>خلعت شاهی بپوش بعون و لطف اله</p>
<p>باش همی مستدام بتخت عزت مکین</p></div>
<div class="b" id="bn46"><div class="m1"><p>شد ز ثنا کستریت شهره چنان نام من</p></div>
<div class="m2"><p>که گشته گوئی سخن ختم بایام من</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گشت بایام شاه بخت حرون رام من</p></div>
<div class="m2"><p>رخت ثنای تو دید در خور اندام من</p></div></div>
<div class="b2" id="bn48"><p>تا بخراسان کشید چرخ سرانجام من</p>
<p>بسیرت راستان بعادت راستین</p></div>
<div class="b" id="bn49"><div class="m1"><p>من ز چه تقدیر را تجاوز از خط کنم</p></div>
<div class="m2"><p>خود نه ظهیرم که چشم ز خون دل شط کنم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز دل باظهار فقر ناله چو بر بط کنم</p></div>
<div class="m2"><p>زانکه بانشاد شعر چو خامه را اقط کنم</p></div></div>
<div class="b2" id="bn51"><p>بمدح شهزاده تا رای مسمط کنم</p>
<p>روح منوچهریم همی کند آفرین</p></div>
<div class="b" id="bn52"><div class="m1"><p>صفا نه خاقانیست تا کند اظهار فضل</p></div>
<div class="m2"><p>گوهر نغزش بود در خور بازار فضل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کم بود از خردلی آری خروار فضل</p></div>
<div class="m2"><p>نقطه موهوم گشت مرکز پرگار فضل</p></div></div>
<div class="b2" id="bn54"><p>پیشکش آورده است پیش خریدار فضل</p>
<p>هستی دارای آن باش خریدار این</p></div>
<div class="b" id="bn55"><div class="m1"><p>غضائری سان همی تا که بشکر نوال</p></div>
<div class="m2"><p>ثنای شه را نهم بکتف باد شمال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ببحر دارم دوان یکی چو در لال</p></div>
<div class="m2"><p>بکوه سازم روان یکی چو آب زلال</p></div></div>
<div class="b2" id="bn57"><p>بشعر گویم مدیح ز شاه جویم منال</p>
<p>چنانکه استاد ری ز فیض شاه تکین</p></div>
<div class="b" id="bn58"><div class="m1"><p>هست بر اندام روز تا سلب عنصری</p></div>
<div class="m2"><p>تا فکند شب بدوش جامه نیلوفری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تا که بود برقرار این فلک اخضری</p></div>
<div class="m2"><p>لاله کند تا بسر مقنعه آذری</p></div></div>
<div class="b2" id="bn60"><p>تا که باطفال باغ ابر کند مادری</p>
<p>سپس کند چون جنان ز شیر پستان زمین</p></div>
<div class="b" id="bn61"><div class="m1"><p>روز نکو خواه شاه خرم و فیروز باد</p></div>
<div class="m2"><p>شام عدو بین وی شام غم اندوز باد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همچو فلک برقرار آن شب و این روز باد</p></div>
<div class="m2"><p>بزم ترا روی یار شمع شب افروز باد</p></div></div>
<div class="b2" id="bn63"><p>چون رخ اطفال باغ روز تو نوروز باد</p>
<p>شام تا یکجا چنان روز تو یک سر چنین</p></div>