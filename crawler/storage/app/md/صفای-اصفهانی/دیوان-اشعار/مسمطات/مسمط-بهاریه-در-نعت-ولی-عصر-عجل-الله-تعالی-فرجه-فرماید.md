---
title: >-
    مسمط بهاریه در نعت ولی عصر عجل الله تعالی فرجه فرماید
---
# مسمط بهاریه در نعت ولی عصر عجل الله تعالی فرجه فرماید

<div class="b" id="bn1"><div class="m1"><p>از شاخ سرو و مرغ سحرخیز زد صفیر</p></div>
<div class="m2"><p>برخیز من غلام تو ای ترک بی‌نظیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان سرخ گل زد زنگارگون سریر</p></div>
<div class="m2"><p>ای لاله تو رهزن و مشک تو دستگیر</p></div></div>
<div class="b2" id="bn3"><p>با گونه چو لاله بیاور شراب پیر</p>
<p>در پای گل که عالم فرتوت شد جوان</p></div>
<div class="b" id="bn4"><div class="m1"><p>شد روزگار تازه و خرداد ماه شد</p></div>
<div class="m2"><p>گیتی به دیده دی و بهمن سیاه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گاه سبزه خسرو گل پادشاه شد</p></div>
<div class="m2"><p>در پای گل زدن می چون لاله گاه شد</p></div></div>
<div class="b2" id="bn6"><p>ای ماه ارغوان من از رنج کاه شد</p>
<p>این کاه را به لاله توان کرد ارغوان</p></div>
<div class="b" id="bn7"><div class="m1"><p>قد تو چون صنوبر و رویت چو لاله است</p></div>
<div class="m2"><p>بر لاله تو کشته دو مشکین کلاله است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل از کلاله تو پریشان و واله است</p></div>
<div class="m2"><p>صد سالخورده بنده‌ات ای خرد ساله است</p></div></div>
<div class="b2" id="bn9"><p>خطت نرسته نوبت خط پیاله است</p>
<p>می‌ده ز خط جور که باشد خط امان</p></div>
<div class="b" id="bn10"><div class="m1"><p>از کاخ سر پس از مه اردیبهشت زن</p></div>
<div class="m2"><p>خرداد شد تو خیمه بر اطراف کشت زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاب فسرده نار کف زرد هشت زن</p></div>
<div class="m2"><p>بردار خشت خم سر گردون به خشت زن</p></div></div>
<div class="b2" id="bn12"><p>آن خاک خشک بر سر آن پیر زشت زن</p>
<p>ای خوب‌تر به گونه ز خورشید آسمان</p></div>
<div class="b" id="bn13"><div class="m1"><p>زلف تو مشک ناب فروهشته بر پرند</p></div>
<div class="m2"><p>بر پای دل ز یک سر مویت هزار بند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا شد لوای عشق تو از بام دل بلند</p></div>
<div class="m2"><p>بنیان هستی من و ما را ز بیخ کند</p></div></div>
<div class="b2" id="bn15"><p>ای طره تو فتنه دل‌های دردمند</p>
<p>ای گونه تو آفت جان‌های ناتوان</p></div>
<div class="b" id="bn16"><div class="m1"><p>دست صبا به طره شمشاد شانه زد</p></div>
<div class="m2"><p>قمری به شاخ سرو ز وحدت ترانه زد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر گل هزاردستان چنگ و چغانه زد</p></div>
<div class="m2"><p>بایدم دم سپیده شراب شبانه زد</p></div></div>
<div class="b2" id="bn18"><p>بیدار کن دو فتنه که باید نشانه زد</p>
<p>دل را به ناوک مژه و ابروی چون کمان</p></div>
<div class="b" id="bn19"><div class="m1"><p>نخلی که دست صانع کل کشت داد بر</p></div>
<div class="m2"><p>خاک سیه ز لاله و گل گشت کان زر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد پست پیش سرو چمن سرو کاشمر</p></div>
<div class="m2"><p>گلبن نهاد افسر پرویز گل به سر</p></div></div>
<div class="b2" id="bn21"><p>از شاخ ریخت بر سر گل گنج نامور</p>
<p>از خاک رست از فر گل گنج شایگان</p></div>
<div class="b" id="bn22"><div class="m1"><p>در زیر ظل رایت سلطان نوبهار</p></div>
<div class="m2"><p>بنشست خسرو گل سوری چو شهریار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نرگس نهاد بر سر دیهیم زرنگار</p></div>
<div class="m2"><p>بر خاک ریخت ابر گهرهای شاهوار</p></div></div>
<div class="b2" id="bn24"><p>در جام گوهری ز خزف ریز آب نار</p>
<p>چون آتش ترای لب لعلت چو ناردان</p></div>
<div class="b" id="bn25"><div class="m1"><p>هدهد فراخت رایت و قمری نواخت کوس</p></div>
<div class="m2"><p>سارویه در ترانه وحدت علی الرؤس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گل را هزار دستان زد بر به پای‌بوس</p></div>
<div class="m2"><p>رویی مراست بی‌تو به کردار سندروس</p></div></div>
<div class="b2" id="bn27"><p>ای گونه تو سرخ‌تر از دیده خروس</p>
<p>افکن به ساغر از دل بط خون ماکیان</p></div>
<div class="b" id="bn28"><div class="m1"><p>از پر فراشت مرغ سلیمان به سر لوی</p></div>
<div class="m2"><p>بر تارک چکاوه بود تاج خسروی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در آستین گل ید بیضای موسوی</p></div>
<div class="m2"><p>موسیجه موسیست و چمن وادی طوی</p></div></div>
<div class="b2" id="bn30"><p>قمری دری سراید و دراج پهلوی</p>
<p>طوطی فسانه گوید و طاوس داستان</p></div>
<div class="b" id="bn31"><div class="m1"><p>مرغان شد آنکه باز به وجد ابتدای کنند</p></div>
<div class="m2"><p>در صحن باغ دلشدگان را ندی کنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دل دستگیر زمزمه داودی کنند</p></div>
<div class="m2"><p>در شاعری به سبک صفا اقتدی کنند</p></div></div>
<div class="b2" id="bn33"><p>احیای شعر عنصری و عسجدی کنند</p>
<p>کز عسجدی نمانده و از عنصری نشان</p></div>
<div class="b" id="bn34"><div class="m1"><p>ساقی بیا که چون بط آهنگ شط کنیم</p></div>
<div class="m2"><p>در شط می شنا چو شتابنده بط کنیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ادراک سر جام جم از هفت خط کنیم</p></div>
<div class="m2"><p>از نای بط شهود دم بار بط کنیم</p></div></div>
<div class="b2" id="bn36"><p>جان را رهین خون گرانبار بط کنیم</p>
<p>در پیشگاه میکده صاحب الزمان</p></div>
<div class="b" id="bn37"><div class="m1"><p>ختم ولایت نبوی پادشاه عصر</p></div>
<div class="m2"><p>ذاتی که سر سر نبوت به دوست حصر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن شاه کش به بام الوهیتست قصر</p></div>
<div class="m2"><p>باب امم امام مسلم خدای نصر</p></div></div>
<div class="b2" id="bn39"><p>موجود بی‌بدایت و بی‌انتها و حصر</p>
<p>مولود در مکان پدر پیر لامکان</p></div>
<div class="b" id="bn40"><div class="m1"><p>طفلی که پیر بود و فلک بود در قماط</p></div>
<div class="m2"><p>سری که ملک را به ملک داد ارتباط</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کویش بهشت و رهگذر کوی او صراط</p></div>
<div class="m2"><p>ساریست همچو نقطه توحید از نقاط</p></div></div>
<div class="b2" id="bn42"><p>در صورت سلیمان در کسوت بساط</p>
<p>در عقل و نفس و طبع و هیولی و جسم و جان</p></div>
<div class="b" id="bn43"><div class="m1"><p>عقل نخست با همه حشمت گدای اوست</p></div>
<div class="m2"><p>خورشید آسمان برین خاک پای اوست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه آسمان مظله ظل همای اوست</p></div>
<div class="m2"><p>آن وجهه کز فناست منزه لقای اوست</p></div></div>
<div class="b2" id="bn45"><p>فانیست در خدای و بزرگی روای اوست</p>
<p>مقهور قاهرست و با شیاست قهرمان</p></div>
<div class="b" id="bn46"><div class="m1"><p>مشکوه سر اوست ولی نعمت مسیح</p></div>
<div class="m2"><p>از دولت گدای درش دولت مسیح</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در کیش اوست پیش امم دعوت مسیح</p></div>
<div class="m2"><p>از خوان اوست ریزه خوری حضرت مسیح</p></div></div>
<div class="b2" id="bn48"><p>روحی که جلوه کرد درو صورت مسیح</p>
<p>آمد برون ز خلوت و شد عیسی زمان</p></div>
<div class="b" id="bn49"><div class="m1"><p>ای دل که بنده در نفس مقیدی</p></div>
<div class="m2"><p>آزاده مؤید و حبس مؤبدی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بشکن قفس که باز سفید مؤیدی</p></div>
<div class="m2"><p>در جو خویش صاحب سلطان سوددی</p></div></div>
<div class="b2" id="bn51"><p>دارای سر قائم آل محمدی</p>
<p>کز صورت تو سر ولایت بود عیان</p></div>
<div class="b" id="bn52"><div class="m1"><p>پیداست پیش دیده بینا ولی امر</p></div>
<div class="m2"><p>سر نشست و صورت بالا ولی امر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ساریست در ضعیف و توانا ولی امر</p></div>
<div class="m2"><p>جاری بود به قطره و دریا ولی امر</p></div></div>
<div class="b2" id="bn54"><p>سرست بس که باشد پیدا ولی امر</p>
<p>پیدا و پیش دیده دجال خونهان</p></div>
<div class="b" id="bn55"><div class="m1"><p>ذاتیست کز علو تجلیست در صفات</p></div>
<div class="m2"><p>اسمای امهات مر او راست اسم ذات</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>طفلی کزو رسیده بام و باب حیات</p></div>
<div class="m2"><p>باب جماد و جانور حادث و نبات</p></div></div>
<div class="b2" id="bn57"><p>در بحر بیکران فنا کشتی نجات</p>
<p>بر گوهر ثمین بقا بحر بیکران</p></div>
<div class="b" id="bn58"><div class="m1"><p>محبوب عاشقان دل از دست داده اوست</p></div>
<div class="m2"><p>مطلوب سالکان ز پا در افتاده اوست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بری که بر فراشته این سقف ساده اوست</p></div>
<div class="m2"><p>طفلی که عقل پیرش از اندیشه زاده اوست</p></div></div>
<div class="b2" id="bn60"><p>شاهی که آسمانش بر در ستاده اوست</p>
<p>چون بنده در مجره کمر بسته بر میان</p></div>
<div class="b" id="bn61"><div class="m1"><p>ختم ولایت آیت کل خسرو وجود</p></div>
<div class="m2"><p>سلطان چار حضرت از غیب و از شهود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن جلوه کش برند بدیر و حرم سجود</p></div>
<div class="m2"><p>آن شاه کز جبلت او جلوه گرد جود</p></div></div>
<div class="b2" id="bn63"><p>قوسین را نزول نمود آن شه و صعود</p>
<p>از بی‌نشان بیامد و شد سوی بی‌نشان</p></div>
<div class="b" id="bn64"><div class="m1"><p>قومی ولایت تو به عیسی کنند ختم</p></div>
<div class="m2"><p>ختمست آیت تو به عیسی کنند ختم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>راه هدایت تو به عیسی کنند ختم</p></div>
<div class="m2"><p>قدر کفایت تو به عیسی کنند ختم</p></div></div>
<div class="b2" id="bn66"><p>خواهند رایت تو به عیسی کنند ختم</p>
<p>ای خاتم ولایت احمد مخواه هان</p></div>
<div class="b" id="bn67"><div class="m1"><p>عیسی پیاده‌ای‌ست به ظل لوای تو</p></div>
<div class="m2"><p>تو پادشاه امری و عیسی گدای تو</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>من با زبان عیسی گویم ثنای تو</p></div>
<div class="m2"><p>ای مهدی وجود که جان‌ها فدای تو</p></div></div>
<div class="b2" id="bn69"><p>دجال شرک خانه گرفتست جای تو</p>
<p>توحید کن که جای بپردازد این عوان</p></div>
<div class="b" id="bn70"><div class="m1"><p>خورشید آسمان ولایت کجا و ظل</p></div>
<div class="m2"><p>خیر البشر کجا و بشر دل کجا و گل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>روح الله آیتیست زانسان معتدل</p></div>
<div class="m2"><p>عیسی لطیفه‌ای‌ست از آن لطف متصل</p></div></div>
<div class="b2" id="bn72"><p>ای فتنه مشاهده دلبر کجا و دل</p>
<p>مهدی کجا و عیسی جانان کجا و جان</p></div>
<div class="b" id="bn73"><div class="m1"><p>مهدی ظهور جمع جمیع حقایقست</p></div>
<div class="m2"><p>بر بدو و ختم قادر و قیوم و فائقست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>اسما شقیق و مهدی باغ شقایقست</p></div>
<div class="m2"><p>هست این حدیقه‌ای که محیط حدایقست</p></div></div>
<div class="b2" id="bn75"><p>عیسی دقیقه‌ای‌ست که از آن دقایقست</p>
<p>مهدیست مظهر کل در محضر عیان</p></div>
<div class="b" id="bn76"><div class="m1"><p>مهدی فراز قصر الوهی کند کنام</p></div>
<div class="m2"><p>عیسی به چرخ چارم فرقست زین دو گام</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بسیار راه باشد از حال تا مقام</p></div>
<div class="m2"><p>سر مست خاص می‌دهد از می تمیز جام</p></div></div>
<div class="b2" id="bn78"><p>این باده نیست در خور مینای جان عام</p>
<p>اوج یقین کجا و پر طائر گمان</p></div>
<div class="b" id="bn79"><div class="m1"><p>ازاین و آن ببر که به قطبت مدار نیست</p></div>
<div class="m2"><p>قطب مدیر ما به مدار استوار نیست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ذات ولی هفت و چهار آشکار نیست</p></div>
<div class="m2"><p>یک وحدتست بسته هفت و چهار نیست</p></div></div>
<div class="b2" id="bn81"><p>رندی که بر تکاور وحدت سوار نیست</p>
<p>گو گام زن که باز نمانی از این و آن</p></div>
<div class="b" id="bn82"><div class="m1"><p>ای جامع لطیف که در هر دلیت جاست</p></div>
<div class="m2"><p>در دل نشسته‌ای تو و دل خانه خداست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یک کشور و دو سلطان در عهده خطاست</p></div>
<div class="m2"><p>حق را دویی نگنجد این مسلک صفاست</p></div></div>
<div class="b2" id="bn84"><p>توحید سر خاص سلاطین اولیاست</p>
<p>یک پادشاست بر همه عالم خدایگان</p></div>
<div class="b" id="bn85"><div class="m1"><p>یعنی تویی که نیست و رای تو جزو و کل</p></div>
<div class="m2"><p>ای مهدی ولایت و ای هادی سبل</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>فعال عقل و نفس هیولای خار و گل</p></div>
<div class="m2"><p>تا کی زنیم زیر گلیم دغا دهل</p></div></div>
<div class="b2" id="bn87"><p>هم خالق عقولی و هم رازق مثل</p>
<p>هم سر لامکانی و هم صورت مکان</p></div>
<div class="b" id="bn88"><div class="m1"><p>با آنکه بی‌نشانی در هر کرانه‌ای</p></div>
<div class="m2"><p>از تست ای ولی ولایت نشانه‌ای</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>هم در میان نه‌ای و تو و هم در میانه‌ای</p></div>
<div class="m2"><p>ای خانه خدا که خداوند خانه‌ای</p></div></div>
<div class="b2" id="bn90"><p>ای پاسبان دین که به دولت یگانه‌ای</p>
<p>بیرون بیا ز پرده که شد دزد پاسبان</p></div>