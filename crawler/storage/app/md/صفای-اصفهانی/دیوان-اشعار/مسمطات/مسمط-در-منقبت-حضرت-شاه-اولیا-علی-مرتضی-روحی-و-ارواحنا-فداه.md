---
title: >-
    مسمط در منقبت حضرت شاه اولیا علی مرتضی روحی و ارواحنا فداه
---
# مسمط در منقبت حضرت شاه اولیا علی مرتضی روحی و ارواحنا فداه

<div class="b" id="bn1"><div class="m1"><p>بریز ماه من ای آفتاب آفاقی</p></div>
<div class="m2"><p>ز خط جام جم دل شراب اشراقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیار ساقی ای فیض اقدست ساقی</p></div>
<div class="m2"><p>ازان رحیق که بخشد بزهر تریاقی</p></div></div>
<div class="b2" id="bn3"><p>مرا که فانی عشقم ز باده باقی</p>
<p>بدار باقی یعنی ز خویش کن فانی</p></div>
<div class="b" id="bn4"><div class="m1"><p>بیا که سنگ شد از سرخ گل بسان شقیق</p></div>
<div class="m2"><p>بیار باده ببوی گلاب و رنگ عقیق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه بل میی که زرنگست و بوی صاف و رحیق</p></div>
<div class="m2"><p>رحیق مانده بمینای دل ز عهد عتیق</p></div></div>
<div class="b2" id="bn6"><p>کدام دل دل عارف که باده تحقیق</p>
<p>از او کشند حریفان بزم عرفانی</p></div>
<div class="b" id="bn7"><div class="m1"><p>دوباره تازه شد از باد روزگار کهن</p></div>
<div class="m2"><p>می کهن غم نو میبرد ز خاطر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بت منا که چو لعل تو نیست سنگ یمن</p></div>
<div class="m2"><p>بریز لعل که بارد سحاب در عدن</p></div></div>
<div class="b2" id="bn9"><p>برنگ لاله و سنگ عقیق و بوی سمن</p>
<p>بروی سرخ تر از بهرمان سیلانی</p></div>
<div class="b" id="bn10"><div class="m1"><p>نگار من که سر زلف تست ظل همای</p></div>
<div class="m2"><p>بسلطنت رسد ار اوفتد بفرق گدای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که عود غالیه بیزست و دود لخلخه سای</p></div>
<div class="m2"><p>حدیث طره ات ار بگذرد بچین و ختای</p></div></div>
<div class="b2" id="bn12"><p>ختاتبه شود و چین شود چو نقش سرای</p>
<p>که بسته بی جان تصویر پنجه مانی</p></div>
<div class="b" id="bn13"><div class="m1"><p>مرا بدل غمت ای آفت چگل خوشتر</p></div>
<div class="m2"><p>بدست زلف تو ایماه معتدل خوشتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سینه ئیکه دراونیست عشق گل خوشتر</p></div>
<div class="m2"><p>سر فسرده جمادست مشتعل خوشتر</p></div></div>
<div class="b2" id="bn15"><p>هوای قد تو در بوستان دل خوشتر</p>
<p>هزار مرتبه زین سروهای بستانی</p></div>
<div class="b" id="bn16"><div class="m1"><p>میی که تا کش در لامکان دل شده کشت</p></div>
<div class="m2"><p>صنوبر دل کامل درخت باغ بهشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که خاک طوبی با آب زندگیش سرشت</p></div>
<div class="m2"><p>خم شراب حقیقت که گر بخاک و بخشت</p></div></div>
<div class="b2" id="bn18"><p>زنند و ریزند از خاک و خشت طرح کنشت</p>
<p>کنشت خندد بر قبله مسلمانی</p></div>
<div class="b" id="bn19"><div class="m1"><p>بساتکین من آن لعل گون شراب بریز</p></div>
<div class="m2"><p>بماه نو ز سهیل خم آفتاب بریز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ب آتشی که زدی بر دل من آب بریز</p></div>
<div class="m2"><p>ز طره در قدح باده مشگ ناب بریز</p></div></div>
<div class="b2" id="bn21"><p>ز لعل در می عناب گون گلاب بریز</p>
<p>وزان گلاب بخر مغز را ز حیرانی</p></div>
<div class="b" id="bn22"><div class="m1"><p>بتا عصاره تاک کف کلیم بیار</p></div>
<div class="m2"><p>بشکر دست جواد و دل کریم بیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیار مایه امید و دفع بیم بیار</p></div>
<div class="m2"><p>می جلال و جمال از خم حکیم بیار</p></div></div>
<div class="b2" id="bn24"><p>بط وجوب ز خمخانه قدیم بیار</p>
<p>که وا رهانی ما را ز قید امکانی</p></div>
<div class="b" id="bn25"><div class="m1"><p>درآمد از در من دوش با پیام سروش</p></div>
<div class="m2"><p>بتی ز غالیه بر ماه گشته مرزنگوش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نموده حلقه ز مشگ تتار و کرده بگوش</p></div>
<div class="m2"><p>فکنده در بنه کائنات جوش و خروش</p></div></div>
<div class="b2" id="bn27"><p>نمود جلوه و مارا نه عقل ماند و نه هوش</p>
<p>شدند هر دو بشمشیر عشق قربانی</p></div>
<div class="b" id="bn28"><div class="m1"><p>درآمد از در و ما را ز هوش کرد بری</p></div>
<div class="m2"><p>مهی که داشت بگلبرگ تازه مشک طری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بروی لاله خود رو بنفشه طبری</p></div>
<div class="m2"><p>بسرو ماند و رفتار او بکبک دری</p></div></div>
<div class="b2" id="bn30"><p>لطیف تر ز ملک دلربای تر ز پری</p>
<p>که چون پری ره دل میزند بپنهانی</p></div>
<div class="b" id="bn31"><div class="m1"><p>بکفر زلف مرا چاک زد بد امن کیش</p></div>
<div class="m2"><p>ز خسروان نظر افکند بر من درویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بنوشداروی جان کرد مرهم دل ریش</p></div>
<div class="m2"><p>بگفتمش به ازین هست منزلی در پیش</p></div></div>
<div class="b2" id="bn33"><p>میان جمع بتان دست زد بزلف پریش</p>
<p>اشاره کرد بسر منزل پریشانی</p></div>
<div class="b" id="bn34"><div class="m1"><p>نهاد ساتکنی پر ز باده انوار</p></div>
<div class="m2"><p>بدست من که بنوش این می تجلی یار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دلم که بود ز اندوه همچو بوتیمار</p></div>
<div class="m2"><p>کشید باده و شد باز جبرئیل شکار</p></div></div>
<div class="b2" id="bn36"><p>ز خود برون شد و منصور وار بر سر دار</p>
<p>زد از تسلط توحید کوس سبحانی</p></div>
<div class="b" id="bn37"><div class="m1"><p>سپس که گشت تنم در جناب عشق فدی</p></div>
<div class="m2"><p>بگوش جان من آمد ز عرش ذات ندی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که ای منصه انوار آفتاب هدی</p></div>
<div class="m2"><p>خدای جستن جستن بود ز خوی خودی</p></div></div>
<div class="b2" id="bn39"><p>بدوش کرد ز توحید خاص خاص ردی</p>
<p>کسی که اطلس و اکسون اوست عریانی</p></div>
<div class="b" id="bn40"><div class="m1"><p>شنید گوش دلم چون ز غیب نغمه راز</p></div>
<div class="m2"><p>چو باز از قفس اسم زد در پرواز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گشود بال بجوی که از نشیب و فراز</p></div>
<div class="m2"><p>گذشت و اسم و صفت ماند و ناز مرد و نیاز</p></div></div>
<div class="b2" id="bn42"><p>بظل رایت توحید پر فکند چو باز</p>
<p>ببام قصر جلال علی عمرانی</p></div>
<div class="b" id="bn43"><div class="m1"><p>شهی که عرش دل اوست مستوی الرحمن</p></div>
<div class="m2"><p>مکان عرش که باشد بر از زمان و مکان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو در نوردد فراش امر فرش زمان</p></div>
<div class="m2"><p>تجلی احدی کون را کند بنیان</p></div></div>
<div class="b2" id="bn45"><p>ز سمت غرب خفا آفتاب شرق عیان</p>
<p>کند طلوع و شود کائنات را بانی</p></div>
<div class="b" id="bn46"><div class="m1"><p>شهی که عقل هیولای استقامت اوست</p></div>
<div class="m2"><p>قیامت من و دل در قیام قامت اوست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قیام قامت موزون او قیامت اوست</p></div>
<div class="m2"><p>امام ملک و ملک بنده امامت اوست</p></div></div>
<div class="b2" id="bn48"><p>ز یک تجلی مولود با کرامت اوست</p>
<p>چهار و هفت اب و ام و عالی و دانی</p></div>
<div class="b" id="bn49"><div class="m1"><p>کسیکه گام نهد در قفای سالک عدل</p></div>
<div class="m2"><p>تواند آنکه برد راه در مسالک عدل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود ملیک رقاب ملوک مالک عدل</p></div>
<div class="m2"><p>بدولت علوی محو شد مهالک عدل</p></div></div>
<div class="b2" id="bn51"><p>که بندگان در خسرو ممالک عدل</p>
<p>بدست گرگ سپارند چوب چوپانی</p></div>
<div class="b" id="bn52"><div class="m1"><p>گدای سر ولی خسرویست دایه گنج</p></div>
<div class="m2"><p>بود دلی که خراب خداست مایه گنج</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نهاده بر در سلطان فقر پایه گنج</p></div>
<div class="m2"><p>فتاده بر سر درویش دوست سایه گنج</p></div></div>
<div class="b2" id="bn54"><p>ندیده وحدت جمع از هزار جایه گنج</p>
<p>دلی که نیست در او دستگاه ویرانی</p></div>
<div class="b" id="bn55"><div class="m1"><p>علیست گوهر دریای بیکرانه دل</p></div>
<div class="m2"><p>همای عشقش عنقای آشیانه دل</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ولایت او دام دلست و دانه دل</p></div>
<div class="m2"><p>ز دست خیمه درویش او بخانه دل</p></div></div>
<div class="b2" id="bn57"><p>من ار بگویم در عشق او فسانه دل</p>
<p>کفاف ندهد صد سال زاد کیوانی</p></div>
<div class="b" id="bn58"><div class="m1"><p>دلی که بسته تجرید پای بند خداست</p></div>
<div class="m2"><p>سری که پوید آزاده در کمند خداست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیوش پند من ای راهرو که پند خداست</p></div>
<div class="m2"><p>بعشق کوش که عشق اختر بلند خداست</p></div></div>
<div class="b2" id="bn60"><p>سوار عشق ولی راکب سمند خداست</p>
<p>که در نوردد هفت آسمان ب آسانی</p></div>
<div class="b" id="bn61"><div class="m1"><p>خدای امر شه اولیا علی ولی</p></div>
<div class="m2"><p>ظهور ذات ابد سر وحدت ازلی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>که وصف ذاتی او قائمی و لم یزلی</p></div>
<div class="m2"><p>ز بس کمال محلاستی ببی بدلی</p></div></div>
<div class="b2" id="bn63"><p>ز فرط علو مسمی بود باسم علی</p>
<p>که قائمست بذاتش صفات ربانی</p></div>
<div class="b" id="bn64"><div class="m1"><p>شهی که جامه خورشید در غمش چاکست</p></div>
<div class="m2"><p>مهی که ذره او آفتاب افلاکست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز شرک دور و ز شک خالی و ز غش پاکست</p></div>
<div class="m2"><p>زر وجودش کبریت احمر خاکست</p></div></div>
<div class="b2" id="bn66"><p>حقیقت او مقصود سر لولاکست</p>
<p>طریقت او قیوم راه انسانی</p></div>
<div class="b" id="bn67"><div class="m1"><p>بدین صراط من و دل دو پیر و سلفیم</p></div>
<div class="m2"><p>بعشق او پدر خویش را نکو خلفیم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شهید شاه بادراک سر من عرفیم</p></div>
<div class="m2"><p>علی معاینه دریاستی و ماش کفیم</p></div></div>
<div class="b2" id="bn69"><p>دو گوهریم وز دریای شحنه النجفیم</p>
<p>ز فیض آن کف کز اوست ابر نیسانی</p></div>
<div class="b" id="bn70"><div class="m1"><p>خدای گشت چو ظاهر بذات مصطفوی</p></div>
<div class="m2"><p>نواخت نوبت شاهی بدولت علوی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>حقیقت احدی در لباس مرتضوی</p></div>
<div class="m2"><p>بجلوه آمد و زد بر فراز عرش لوی</p></div></div>
<div class="b2" id="bn72"><p>لوای وحدت و شد ماسوی بنفی سوی</p>
<p>نماند غیر خدائی که نیستش ثانی</p></div>
<div class="b" id="bn73"><div class="m1"><p>شه منا که سیهل و سماک زنده تست</p></div>
<div class="m2"><p>تو پادشاهی و خورشید و ماه بنده تست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>توئی که گریه ابراز هوای خنده تست</p></div>
<div class="m2"><p>حجاب چهره برافکن اگر پسنده تست</p></div></div>
<div class="b2" id="bn75"><p>که آفتاب گذارد که سر فکنده تست</p>
<p>بپیش پای تو بر خاک راه پیشانی</p></div>
<div class="b" id="bn76"><div class="m1"><p>حدیث نفس مرا گفت ترک عرفان کن</p></div>
<div class="m2"><p>ببند طرف ز دولت ز فقر کتمان کن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چه گفت گفت که ترک وصال جانان کن</p></div>
<div class="m2"><p>بیار روی بتن پشت بر دل و جان کن</p></div></div>
<div class="b2" id="bn78"><p>بشوی دفتر توحید و مدح دیوان کن</p>
<p>مرا چه کار بدیوانگان دیوانی</p></div>
<div class="b" id="bn79"><div class="m1"><p>ز جان چگونه دل خویش را بتن بندم</p></div>
<div class="m2"><p>ز دوست چون دل خود را بخویشتن بندم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چرا ز یزدان خاطر باهرمن بندم</p></div>
<div class="m2"><p>که بست طرف ازین سلطنت که من بندم</p></div></div>
<div class="b2" id="bn81"><p>حدیث عشق ترا بر پر سخن بندم</p>
<p>که عرش و فرش بگیرم بعون یزدانی</p></div>
<div class="b" id="bn82"><div class="m1"><p>منم گدای تو و آسمان گدای منست</p></div>
<div class="m2"><p>چو آشنای توام دولت آشنای منست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سخن سماست ولی مزد شست پای منست</p></div>
<div class="m2"><p>ستاره آینه صیقل صفای منست</p></div></div>
<div class="b2" id="bn84"><p>بچشم او ز ثنای تو توتیای منست</p>
<p>تبارک الله ازین سرمه صفا هانی</p></div>
<div class="b" id="bn85"><div class="m1"><p>بخاک پای تو کز اوست وحدت جانم</p></div>
<div class="m2"><p>بگرد کثرت آلوده نیست دامانم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بجان سوارم و ملک دلست میدانم</p></div>
<div class="m2"><p>من ار بصورت آشفته و پریشانم</p></div></div>
<div class="b2" id="bn87"><p>گدای عشقم و بر عقل و نفس سلطانم</p>
<p>ببین شرافت این جوهر هیولانی</p></div>