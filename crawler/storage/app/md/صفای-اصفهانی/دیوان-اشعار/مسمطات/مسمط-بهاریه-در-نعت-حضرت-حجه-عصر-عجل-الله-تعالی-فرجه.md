---
title: >-
    مسمط بهاریه در نعت حضرت حجه عصر عجل الله تعالی فرجه
---
# مسمط بهاریه در نعت حضرت حجه عصر عجل الله تعالی فرجه

<div class="b" id="bn1"><div class="m1"><p>شد وقت آنکه باز بانوار یاسمین</p></div>
<div class="m2"><p>پهلو بفر سینه سینا زند زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون وادی طوی شد بستان و کرد هین</p></div>
<div class="m2"><p>موسی گل برون ید بیضا ز آستین</p></div></div>
<div class="b2" id="bn3"><p>گل را بماء قبطی ممزوج کرد طین</p>
<p>زد چون شبان سرخ سر از طور شاخسار</p></div>
<div class="b" id="bn4"><div class="m1"><p>خاک سیه شد از گل سوری شقیق رنگ</p></div>
<div class="m2"><p>چون آبگینه شد ز صفای شقیق سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سرخ گل چرد بستاک جبال رنگ</p></div>
<div class="m2"><p>بزدای ای چو ماه دو روی تو بی درنگ</p></div></div>
<div class="b2" id="bn6"><p>ز آئینه من از می چون آفتاب زنگ</p>
<p>ای آفتابت آینه ماه میگسار</p></div>
<div class="b" id="bn7"><div class="m1"><p>دارم سری گران و نژند از خمار دوش</p></div>
<div class="m2"><p>ترکا بطشت دختر رز را بریز هوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آور دوباره خون سیاوش را بجوش</p></div>
<div class="m2"><p>آن می که هست صاف تر از سیرت سروش</p></div></div>
<div class="b2" id="bn9"><p>افکن بجام خسروی ایماه میفروش</p>
<p>غم دیو و تو تهمتن و بط گرز گاو سار</p></div>
<div class="b" id="bn10"><div class="m1"><p>خرداد ماه داد ببستان بهشت را</p></div>
<div class="m2"><p>هشت افسر هما شکم خاک زشت را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>موری صفای ساغر جم داد خشت را</p></div>
<div class="m2"><p>دانا بتخت کی ندهد طرف کشت را</p></div></div>
<div class="b2" id="bn12"><p>بهمن تو باش نار کف زر دهشت را</p>
<p>در جام جم بسوز برسم سفندیار</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای ترک خلخی بمه از مشک هله کن</p></div>
<div class="m2"><p>بر گونه چو لاله ز سنبل کلاله کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپریش مشک تر خرد پیر واله کن</p></div>
<div class="m2"><p>این شنبلید زار مرا باغ لاله کن</p></div></div>
<div class="b2" id="bn15"><p>چون لاله بهار دو روی پیاله کن</p>
<p>ای گونه ات معاینه چون لاله بهار</p></div>
<div class="b" id="bn16"><div class="m1"><p>خیز ای پسر که راه غم از باده طی کنیم</p></div>
<div class="m2"><p>وز زور می بناخن اندوه نی کنیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ساغر ز کاسه سر کاوس و کی کنیم</p></div>
<div class="m2"><p>جان را سوار مرکب اقبال پی کنیم</p></div></div>
<div class="b2" id="bn18"><p>جا بر هلال توسن خورشید می کنیم</p>
<p>از می شویم توسن خورشید را سوار</p></div>
<div class="b" id="bn19"><div class="m1"><p>امسال نوبهار ز پیرار و پار به</p></div>
<div class="m2"><p>آری ز بهمن و دی خرم بهار به</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در دیده ئی که یار درو نیست خار به</p></div>
<div class="m2"><p>از هر چه آیدت بنظر روی یار به</p></div></div>
<div class="b2" id="bn21"><p>از منبری که بی دم منصور دار به</p>
<p>با این ترانه تازه تر از منبرست دار</p></div>
<div class="b" id="bn22"><div class="m1"><p>مرغان بدستگاه سلیمان زنند کوس</p></div>
<div class="m2"><p>بلبل نمود بر سر اورنگ گل جلوس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هدهد نهاد تاج تبارک علی الرؤس</p></div>
<div class="m2"><p>در پای سرو و لاله چون دیده خروس</p></div></div>
<div class="b2" id="bn24"><p>ساری بخاکساری و قمری بپای بوس</p>
<p>در رقص و در ترنم از صعوه تا هزار</p></div>
<div class="b" id="bn25"><div class="m1"><p>ما نیز خوشتر آنکه بگیریم زلف دوست</p></div>
<div class="m2"><p>آن رشته ئی که محکم از آن عهد ماست اوست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خاص اینکه با دغالیه سای و عبیر بوست</p></div>
<div class="m2"><p>گوئی بباغ رهگذرش زان شکنج موست</p></div></div>
<div class="b2" id="bn27"><p>حیفست باد در خور مغز آدمی بپوست</p>
<p>بشکاف پوست تا دهدت زلف دوست بار</p></div>
<div class="b" id="bn28"><div class="m1"><p>ما ای پسر بعشق تو از مام زاده ایم</p></div>
<div class="m2"><p>سر در کمند زلف تو از جان نهاده ایم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل را بیاد وصل تو از دست داده ایم</p></div>
<div class="m2"><p>در دور چشم مست تو سر گرم باده ایم</p></div></div>
<div class="b2" id="bn30"><p>از هر چه غیر سینه صاف تو ساده ایم</p>
<p>ای سینه تو صاف تر از عقل هوشیار</p></div>
<div class="b" id="bn31"><div class="m1"><p>شاه منی تو ماه گرفتار بندتست</p></div>
<div class="m2"><p>خورشید سر نهاده بسرو بلند تست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر جان لاله داغ لب نوشخند تست</p></div>
<div class="m2"><p>گردون عشق سایه گرد سمند تست</p></div></div>
<div class="b2" id="bn33"><p>ای پادشاه حسن که سر در کمند تست</p>
<p>امروز نیست غیرتو سلطان درین دیار</p></div>
<div class="b" id="bn34"><div class="m1"><p>بر سیم ساده غالیه تر نهاده ئی</p></div>
<div class="m2"><p>گل را بسر زغالیه افسر نهاده ئی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در خسروی تو عادت دیگر نهاده ئی</p></div>
<div class="m2"><p>بر سر و جوی خسرو خاور نهاده ئی</p></div></div>
<div class="b2" id="bn36"><p>یکپایه زافتاب فراتر نهاده ئی</p>
<p>ای آفتاب سرزده از سر و جویبار</p></div>
<div class="b" id="bn37"><div class="m1"><p>برخیز تا من و تو دم از جام جم زنیم</p></div>
<div class="m2"><p>وقت سپیده دم می چون سرخ دم زنیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ما را که گفت از قدر دوست دم زنیم</p></div>
<div class="m2"><p>در جبر و اختیار دم از بیش و کم زنیم</p></div></div>
<div class="b2" id="bn39"><p>توحید خوش دمیست بیا تا به هم زنیم</p>
<p>زین دم نظام سلسله جبر و اختیار</p></div>
<div class="b" id="bn40"><div class="m1"><p>مائیم سر راهروان طریق عشق</p></div>
<div class="m2"><p>درد یکشان مست سفال رحیق عشق</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بیگانه از جمیع جهات و رفیق عشق</p></div>
<div class="m2"><p>با آنکه سوختیم بنار حریق عشق</p></div></div>
<div class="b2" id="bn42"><p>محکم گرفته رشته عهد عتیق عشق</p>
<p>در دست دل که چرخ چو او نیست استوار</p></div>
<div class="b" id="bn43"><div class="m1"><p>ایدر بموی عهد امانت مقیدم</p></div>
<div class="m2"><p>از هر چه جز علاقه این مو مجردم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درویش خانقاهم و شاه مؤیدم</p></div>
<div class="m2"><p>در کوی فقر صاحب سلطان سوددم</p></div></div>
<div class="b2" id="bn45"><p>دائر بامر قائم آل محمدم</p>
<p>کز دور اوست دائره امر را مدار</p></div>
<div class="b" id="bn46"><div class="m1"><p>مولود مام دهر که سرمد قماط اوست</p></div>
<div class="m2"><p>آبا و امهات برقص از نشاط اوست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در جیب جان غیب و شهود ارتباط اوست</p></div>
<div class="m2"><p>جم زیر امر مور ضعیف بساط اوست</p></div></div>
<div class="b2" id="bn48"><p>این فیض منبسط اثر انبساط اوست</p>
<p>کز او ز عقل تا بهیولیست آشکار</p></div>
<div class="b" id="bn49"><div class="m1"><p>طفلی که از تجلی او زاد عقل پیر</p></div>
<div class="m2"><p>پیری که عقل طفل سبق خوان و او خبیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عقلی که شمس تابدش از مشرق ضمیر</p></div>
<div class="m2"><p>نفسی که اوست دائره چرخ را مدیر</p></div></div>
<div class="b2" id="bn51"><p>چرخی که کائنات بچوگان او اسیر</p>
<p>سر زد ز آسمان وجود آفتاب وار</p></div>
<div class="b" id="bn52"><div class="m1"><p>ای آفتاب بنده این خاک و آب باش</p></div>
<div class="m2"><p>وانگه ب آسمان ابد آفتاب باش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با گرد شهسوار قدم هم رکاب باش</p></div>
<div class="m2"><p>از ذره گان شمس ولایت م آب باش</p></div></div>
<div class="b2" id="bn54"><p>بر روشنان جان شه مالک رقاب باش</p>
<p>با تخت آبنوسی و دیهیم زرنگار</p></div>
<div class="b" id="bn55"><div class="m1"><p>شاهی که از میامن اقبال اوست بخت</p></div>
<div class="m2"><p>سست است عهد هستی و پیمان اوست سخت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در طور دل چو موسی سالک کشید رخت</p></div>
<div class="m2"><p>او کرد یک تجلی و شد کوه لخت لخت</p></div></div>
<div class="b2" id="bn57"><p>بانگی که بر بگوش کلیم آمد از درخت</p>
<p>بود از زبان مهدی بر تیغ کوهسار</p></div>
<div class="b" id="bn58"><div class="m1"><p>بر کوهسار انی انا الله ندای کیست</p></div>
<div class="m2"><p>در کثرت این ترانه وحدت صدای کیست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آواز آشناست ولی آشنای کیست</p></div>
<div class="m2"><p>از هر چه هست کرده ظهور این لقای کیست</p></div></div>
<div class="b2" id="bn60"><p>جز خاتم ولایت کل در هوای کیست</p>
<p>این محمدت که میشنوم من ز مور و مار</p></div>
<div class="b" id="bn61"><div class="m1"><p>سلطان خلق و امر خدای شهود و غیب</p></div>
<div class="m2"><p>شاه یقین که نیست دران شاه شک و ریب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مورش تجلی کف موسی کند ز جیب</p></div>
<div class="m2"><p>مارش چو مار موسی بی یاری شعیب</p></div></div>
<div class="b2" id="bn63"><p>شد اژدر کمال و فرو برد مار عیب</p>
<p>دجال شرک مرد و بمهدی کشید کار</p></div>
<div class="b" id="bn64"><div class="m1"><p>ما بنده طریقت این آستانه ایم</p></div>
<div class="m2"><p>در خانه فناش خداوند خانه ایم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چونانکه قطره غرق یم بیکرانه ایم</p></div>
<div class="m2"><p>عشق ولی چو مرغ و من و دل دو دانه ایم</p></div></div>
<div class="b2" id="bn66"><p>مارا بخوان که در غم عشقش فسانه ایم</p>
<p>ای همنفس که میطلبی درس عشق یار</p></div>
<div class="b" id="bn67"><div class="m1"><p>رندان راهرو که سه و سیصد و دهند</p></div>
<div class="m2"><p>ابدال بی بدیل که پرورده شهند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همدست و هم حقیقت و همراز و همرهند</p></div>
<div class="m2"><p>ایدل بهوش باش که ابدال آگهند</p></div></div>
<div class="b2" id="bn69"><p>از هر طرف که میگذری در گذر گهند</p>
<p>غیر از ولی مبین که نرانندت از قطار</p></div>
<div class="b" id="bn70"><div class="m1"><p>ای صاحب ولایت نه عقل پست تست</p></div>
<div class="m2"><p>شست قضا و دست قدر زیر دست تست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بر صدر بارگاه الوهی نشست تست</p></div>
<div class="m2"><p>خمخانه احد تو و کونین مست تست</p></div></div>
<div class="b2" id="bn72"><p>جز کون جامع آنچه سرایم شکست تست</p>
<p>ای پنج حضرت از تو بتحقیق برقرار</p></div>
<div class="b" id="bn73"><div class="m1"><p>ای سایه حقیقت سلطان دل توئی</p></div>
<div class="m2"><p>بهتر ز ماه تبت و ترک چگل توئی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در شهر عشق پادشه مستقل توئی</p></div>
<div class="m2"><p>بر عرش انکه برد سر آب و گل توئی</p></div></div>
<div class="b2" id="bn75"><p>اظلال را نگر که خداوند ظل توئی</p>
<p>ای سایه تو بر سر اظلال پایدار</p></div>
<div class="b" id="bn76"><div class="m1"><p>ای دل تو از زخم گل صهبای جان مجو</p></div>
<div class="m2"><p>آنکس که آسمان کند از آسمان مجو</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سلطان لامکان دلی از مکان مجو</p></div>
<div class="m2"><p>در هر چه هست هست هم از لامکان مجو</p></div></div>
<div class="b2" id="bn78"><p>جز صاحب الزمان بزمین و زمان مجو</p>
<p>بفکن حجاب این فلک پیر پرده دار</p></div>
<div class="b" id="bn79"><div class="m1"><p>این راه راز راهروان وفا طلب</p></div>
<div class="m2"><p>این می ز میکشان سبوی ولا طلب</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>با غیر کم نشین سخن از آشنا طلب</p></div>
<div class="m2"><p>ذوالامر را ز خود بدر آی از خدا طلب</p></div></div>
<div class="b2" id="bn81"><p>مرآت این لطیفه ز سر صفا طلب</p>
<p>کش صیقلیست آینه از فیض هشت و چار</p></div>
<div class="b" id="bn82"><div class="m1"><p>من هر چه یافتم ز ولی یافتم بصبر</p></div>
<div class="m2"><p>رستم بیمن وحدتش از اختیار و جبر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جستم ز جوی تن که بدی پرده دار قبر</p></div>
<div class="m2"><p>ببریدم از علاقه این نفس شوم گبر</p></div></div>
<div class="b2" id="bn84"><p>گل را بلطف تربیت بحر داد و ابر</p>
<p>رحمت باوستاد من آن ابر بحر بار</p></div>
<div class="b" id="bn85"><div class="m1"><p>دستی به تیغ داد گرای شاهزاده کن</p></div>
<div class="m2"><p>از شاخ شرک باغ دل کون ساده کن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در جام جمع از خم توحید باده کن</p></div>
<div class="m2"><p>گودال باش قافیه ای شه اراده کن</p></div></div>
<div class="b2" id="bn87"><p>ما را بجان سوار کن از تن پیاده کن</p>
<p>چون راکب براق و خداوند ذوالفقار</p></div>
<div class="b" id="bn88"><div class="m1"><p>ما را بحق خویشتن از خویش کن بری</p></div>
<div class="m2"><p>ما باب خیبریم و تو بازوی حیدری</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>تن ذره و تو خسرو خورشید خاوری</p></div>
<div class="m2"><p>ای آفتاب وحدت کن ذره پروری</p></div></div>
<div class="b2" id="bn90"><p>شد اژدهای گنج تو این جسم عنصری</p>
<p>مار تو شد بر آورش از دو دمان دمار</p></div>