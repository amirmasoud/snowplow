---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>رسید دست من از عشق دل بدولت دوست</p></div>
<div class="m2"><p>که این خرابه بی حد و وصف خانه اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بران بدم که نگنجم بپوست در غم مغز</p></div>
<div class="m2"><p>غم تو آمد و ما را نه مغز ماند و نه پوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بباغ دل بهوای طلوع طلعت یار</p></div>
<div class="m2"><p>مکار تخم ارادت که این گل خود روست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بساحلی تو چه دانی غم مرا که ز درد</p></div>
<div class="m2"><p>کنار حسرت دریا و چشم غیرت جوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکوه میکده عشق بین که مست خدای</p></div>
<div class="m2"><p>نظاره سر جمشید میکند که سبوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نضارت گل میخانه و مل مینا</p></div>
<div class="m2"><p>نظیر آب حیاتست و روضه مینوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر ز حال دل ای بی خبر ز حال مگیر</p></div>
<div class="m2"><p>که پای بند سر آن دو زلف غالیه بوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من مپرس بدان تاب زلف بین که از آن</p></div>
<div class="m2"><p>پدید حال دل دردمند موی بموست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گسسته رشته پیمان و سوزن مژه اش</p></div>
<div class="m2"><p>هزار چاک بدل می زند چه جای رفوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکایت من و او در فضای قدس فنا</p></div>
<div class="m2"><p>همان مقدمه شاهباز با تیهوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شهود و غیب و قوی و نزار و پست و بلند</p></div>
<div class="m2"><p>چو غیر جلوه او نیست هر چه هست نکوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلند و پست تمام وجود پای زدم</p></div>
<div class="m2"><p>نشان پای بتم لا اله الا هوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببین به فر صفا کش فضای کون و مکان</p></div>
<div class="m2"><p>تمام زیر پر مرغ نطق نادره گوست</p></div></div>