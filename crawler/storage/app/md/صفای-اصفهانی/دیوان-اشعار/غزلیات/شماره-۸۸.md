---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>باز آی که از غیر تو پرداخته ام دل</p></div>
<div class="m2"><p>ای سر تو را سینه سودا زده منزل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قربان تو میکردم اگر یافتمی جان</p></div>
<div class="m2"><p>بر زلف تو میبستم اگر داشتمی دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز نقش خط از روی نکویت خط هستی</p></div>
<div class="m2"><p>نقشیست که گردون زده بر آب بباطل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندانکه بود کام تو از قتل من آسان</p></div>
<div class="m2"><p>کار من دلباخته از دست تو مشکل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من روی چو زر کرده ام از عشق تو بنمای</p></div>
<div class="m2"><p>بر گردنم آن ساعد چون سیم حمایل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آئینه شود سینه پرداخته از زنگ</p></div>
<div class="m2"><p>بر عرش پرد طائر برخاسته از گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی که شوی مظهر انوار تجلی</p></div>
<div class="m2"><p>آئینه خود ساز بر آن روی مقابل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خود بین نبرد راه بخمخانه وحدت</p></div>
<div class="m2"><p>محجوب ندارد خبر از نشاء/ه کامل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غرب خفا گونه خورشید حقیقت</p></div>
<div class="m2"><p>پیداست، تو در پرده ئی ای دیده غافل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کن در طلب گوهر جان کشتی تن را</p></div>
<div class="m2"><p>مستغرق دریای دل بی تک و ساحل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از کشته شدن زنده شوی در طلب تیغ</p></div>
<div class="m2"><p>بایست زدن بوسه بسر پنجه قاتل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قومی بحرم ساجد و قومی بکلیسا</p></div>
<div class="m2"><p>روی دل من جانب آن هندوی مقبل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز ای دل سودازده قلاشی و رندی</p></div>
<div class="m2"><p>جز رنج چه بردست کس از کسب فضائل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما زنده بعشقیم که بی فاصله ما را</p></div>
<div class="m2"><p>جاریست چو خون در کبد و عرق و مفاصل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بردار ز گل ذره محجوب صفا را</p></div>
<div class="m2"><p>ای بر همگی پرتو خورشید تو شامل</p></div></div>