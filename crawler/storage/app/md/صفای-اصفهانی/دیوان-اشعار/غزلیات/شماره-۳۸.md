---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>شمس حقیقت از افق جان پدید شد</p></div>
<div class="m2"><p>جان نیز شد نهفته و جانان پدید شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دوش تا سپیده دم از جسم بی ثبات</p></div>
<div class="m2"><p>مردم هزار مرتبه تا جان پدید شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مغرب خفا رخ توحید ذات دوست</p></div>
<div class="m2"><p>از مشرق آفتاب درخشان پدید شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن آفتاب سرزده از مشرق وجوب</p></div>
<div class="m2"><p>از سینه مغارب امکان پدید شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن گوهر معالی دریای بی زوال</p></div>
<div class="m2"><p>زین نه صدف چو قطره نیسان پدید شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطان بارگاه حقیقت ز غیب ذات</p></div>
<div class="m2"><p>از جلوه ئی بصورت انسان پدید شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این صورت خداست که انسان لایزال</p></div>
<div class="m2"><p>از لم یزل بصورت رحمن پدید شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد برون ز پرده شک شاهد یقین</p></div>
<div class="m2"><p>وز جان کفر جلوه ایمان پدید شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجموع کائنات کمر بست بنده وار</p></div>
<div class="m2"><p>فرمان پذیر امر که سلطان پدید شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این اضطراب و این غلق از ملک و مال بود</p></div>
<div class="m2"><p>در ملک فقر امن فراوان پدید شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دولت سپیده دم آفتاب فقر</p></div>
<div class="m2"><p>روی سیاه دفتر دیوان پدید شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن آفتاب تن زده در مغرب خفا</p></div>
<div class="m2"><p>از مشرق سمای خراسان پدید شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابر کریم یم عظمت لجه نجات</p></div>
<div class="m2"><p>کز دست فیض بارش باران پدید شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر پایه ئی که بود صفا را بکتم غیب</p></div>
<div class="m2"><p>از دستگاه دولت قرآن پدید شد</p></div></div>