---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>کدام شه که گدای در سرای تو نیست</p></div>
<div class="m2"><p>چگونه شاه تواند شد ار گدای تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خاک پای تو گشتند سر شدند سران</p></div>
<div class="m2"><p>سری چگونه کند سر که خاکپای تو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بعرش پرد مرغ آشیان گلست</p></div>
<div class="m2"><p>دلی که با دو پر باز در هوای تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان ز غیر ندید آنکه آشنای تو شد</p></div>
<div class="m2"><p>که نیست هر که درین نشاء/ آشنای تو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشاد کار نبیند بتنگنای دو کون</p></div>
<div class="m2"><p>دلی که بسته موی گره گشای تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو تاست پشت فلک از نهیب بار فراق</p></div>
<div class="m2"><p>که زیر سلسله طره دو تای تو نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من از برای تو در آتشم چنانکه در آب</p></div>
<div class="m2"><p>برای سوختنست آنکه از برای تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سترده باد بتیغ فنا ز دوش بقا</p></div>
<div class="m2"><p>سری که در سر عهد تو و وفای تو نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل ار بقا طلبد در فنای تست از آنک</p></div>
<div class="m2"><p>فنای کون و مکان باشد و فنای تو نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سزای من نبود جز تو پای تا سر خویش</p></div>
<div class="m2"><p>بمن ببخش که غیر از کرم سزای تو نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عطای من همه رویست و موی دلبر من</p></div>
<div class="m2"><p>کدام رزق که در سفره عطای تو نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدل ز صیقل تجرید شد تجلی یار</p></div>
<div class="m2"><p>چه صفوتست که در سیرت صفای تو نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرو ز دیده ام ای در دلم گرفته وطن</p></div>
<div class="m2"><p>جفا مکن که مرا طاقت جفای تو نیست</p></div></div>