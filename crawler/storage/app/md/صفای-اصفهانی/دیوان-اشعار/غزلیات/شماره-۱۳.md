---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بغیر خاک سر کوی دل پناهی نیست</p></div>
<div class="m2"><p>بجز گدای در فقر پادشاهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مراست سلطنت فقر با کلاه نمد</p></div>
<div class="m2"><p>ازین نمد بسر پادشه کلاهی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جلال بین که سر آفتاب را زین سیر</p></div>
<div class="m2"><p>جز آستان طریقت حواله گاهی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدیده دل کامل که ثابتست چو کوه</p></div>
<div class="m2"><p>شکوه پادشه کون سر کاهی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدوست ره نبری جز بخانه دل ما</p></div>
<div class="m2"><p>ز خانه دل ما تا بدوست راهی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آب دیده توان برد پی ب آتش دل</p></div>
<div class="m2"><p>مرا بعشق تو زین خوبتر گواهی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید عفو منست از خدای جرم خودی</p></div>
<div class="m2"><p>»که در طریقت ما غیر از این گناهی نیست «</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پناه میبرم ایدل ز دست خویش بدوست</p></div>
<div class="m2"><p>بهوش باش که جز نیستی پناهی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا ز فقر بدولت مخوان که گاه ملوک</p></div>
<div class="m2"><p>بر فقیر به از کنج خانقاهی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه باک چرخ مرا ز استراق دیو نفاق</p></div>
<div class="m2"><p>شهاب ثاقب درویش غیر آهی نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قوام چرخ بود بر ستون خیمه فقر</p></div>
<div class="m2"><p>باستقامت این خیمه بارگاهی نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فریب جاه نخواهیم خورد و غبطه مال</p></div>
<div class="m2"><p>گدای فقر مقید بمال و جاهی نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل صفا ز تجلیست بوستان بهشت</p></div>
<div class="m2"><p>بجز خط تو درین بوستان گیاهی نیست</p></div></div>