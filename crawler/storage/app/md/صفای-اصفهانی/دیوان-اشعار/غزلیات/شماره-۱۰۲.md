---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>روح وقتیم و کلیم سلفیم</p></div>
<div class="m2"><p>صاحب نفحه و خورشید کفیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارش مزرعه فقر و فنا</p></div>
<div class="m2"><p>آتش خرمن آب و علفیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قمر بارغ بی ابر و غروب</p></div>
<div class="m2"><p>فارغ از نقص و بری از کلفیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طالع از شرق سمای دل پاک</p></div>
<div class="m2"><p>آفتابیم و بیت الشرفیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما عرفنا بزبانیم و بدل</p></div>
<div class="m2"><p>عارف سر سر من عرفیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لامکان را ز مکانیم درود</p></div>
<div class="m2"><p>از سماوات زمین را تحفیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علم اسمای تو در مدرس ما</p></div>
<div class="m2"><p>از پدر مانده که پور خلفیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب شیرین پسری برده ز دست</p></div>
<div class="m2"><p>دل ما را که بشور و شعفیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دف و چنگیم چو یار از بر ما</p></div>
<div class="m2"><p>رفت چنگیم چو برگشت دفیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بزن این دف نبواز این بر بط</p></div>
<div class="m2"><p>بی نوازنده لطفت تلفیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساعتی در تک این بحر گهر</p></div>
<div class="m2"><p>لحظه‌ای بر سر دریات کفیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آدم رسته ازین هفت اندام</p></div>
<div class="m2"><p>گوهر جسته ازین نه صدفیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از صفوف همه کون و مکان</p></div>
<div class="m2"><p>آمدیم از عقب و پیش صفیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوهر قلزم افلاکی روح</p></div>
<div class="m2"><p>بزمین تن خاکی خزفیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زنده دستگاه فقر صفا</p></div>
<div class="m2"><p>بنده درگه شاه نجفیم</p></div></div>