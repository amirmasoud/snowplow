---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گذشت درگه شاهی ز آسمان سرما</p></div>
<div class="m2"><p>که خاک درگه درویش تست افسر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زند کبوتر ما در هوای بام تو پر</p></div>
<div class="m2"><p>شکار نسر حقیقت کند کبوتر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند زلف ترا در خورست گردن شیر</p></div>
<div class="m2"><p>که تاب داده ئی از بهر صید لاغر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بظل رایت خورشید آسمان وجود</p></div>
<div class="m2"><p>طلوع کرد ز شرق شهود اختر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاره ایم نه بل شاهباز دست شهیم</p></div>
<div class="m2"><p>که آفتاب بود زیر سایه پر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهفته در ظلمات تنست آب حیوه</p></div>
<div class="m2"><p>بسینه است دل آئینه سکندر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدور نقطه دل چنبریم دایره وار</p></div>
<div class="m2"><p>بدان احاطه که چرخست زیر چنبر ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدیم بنده سلطان فقر و از افراد</p></div>
<div class="m2"><p>ممالک ملک وملک شد مسخر ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کتاب جمع وجودیم ما بمدرس خود</p></div>
<div class="m2"><p>که هر چه هست بود آیت مفسر ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مس نواقص امکان زر وجوب شود</p></div>
<div class="m2"><p>شود چو طرح بر او گرد کیمیاگر ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفای گوشه نشینیم و هست روشن تر</p></div>
<div class="m2"><p>ز آفتاب فلک طینت منور ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگاهبان سرو گنج و افسر و ملکیم</p></div>
<div class="m2"><p>که شاهوارتر از گوهرست گوهر ما</p></div></div>