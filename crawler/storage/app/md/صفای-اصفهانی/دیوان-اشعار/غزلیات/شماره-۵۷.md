---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>رازی که به دل دارم گر باز عیان گردد</p></div>
<div class="m2"><p>از فتنه نپرهیزد آشوب جهان گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ده زان گهر تا کی ای ساقی افلاکی</p></div>
<div class="m2"><p>تا جسم من خاکی عقل و دل و جان گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مرده روان جوید وز مرگ امان جوید</p></div>
<div class="m2"><p>از باده نشان جوید بی نام و نشان گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی ز خم باقی ده باده اشراقی</p></div>
<div class="m2"><p>کاین غم سبک از ساقی وز رطل گران گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریز آن می یزدانی در ساغر اهریمن</p></div>
<div class="m2"><p>تا پیر بدان زشتی زیبا و جوان گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بخت جوان باشد دل پیرو جان باشد</p></div>
<div class="m2"><p>با پیر مغان باشد تا پیر مغان گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان پخته که هر خامی زر یافت سرانجامی</p></div>
<div class="m2"><p>گر بنده زند جامی کاوس کیان گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باریک تر از مویم زان درد که گر بارش</p></div>
<div class="m2"><p>بر کوه نهد بنیان باریک میان گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل داد توان ما را کی بود گمان ما را</p></div>
<div class="m2"><p>کز تاب سر موئی بی تاب و توان گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من جان بیان را دل بر موی عیان بستم</p></div>
<div class="m2"><p>کاین سلسله محکم زنجیر بیان گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر عبد امین باشد سلطان یقین باشد</p></div>
<div class="m2"><p>چون کار چنین باشد درویش چنان گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درویش چو زد پائی بر کون و بچرخ آمد</p></div>
<div class="m2"><p>بالاتر و والاتر از کون و مکان گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر نفی زمان پوید دارای زمان جوید</p></div>
<div class="m2"><p>اسرار زمان گوید خود قطب زمان گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن غمزه و بالا را رمزیست که در حلش</p></div>
<div class="m2"><p>تیر فلک بالا بی کلک و بنان گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بردار نقاب از گل بگشا گره از سنبل</p></div>
<div class="m2"><p>تا شهر پر از بلبل با شور و فغان گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چشم خدا بیند در منظر ما بیند</p></div>
<div class="m2"><p>با چشم صفا بیند تا یار عیان گردد</p></div></div>