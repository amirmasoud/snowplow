---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>ساقیا جان جاودانه بیار</p></div>
<div class="m2"><p>صبحدم شد می شبانه بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ از ره رسیده ما را</p></div>
<div class="m2"><p>از می و نقل آب و دانه بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خم وحدت آن شراب کهن</p></div>
<div class="m2"><p>ای مدیر شرابخانه بیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه مستم ولی خراب نیم</p></div>
<div class="m2"><p>یکدو ساغر بدین بهانه بیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طره دوست در همست و پریش</p></div>
<div class="m2"><p>ای نسیم شمال شانه بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابا شب فراق مرا</p></div>
<div class="m2"><p>از دم صبحدم نشانه بیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاها بحضرت درویش</p></div>
<div class="m2"><p>سر طاعت بر آستانه بیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایدل آن دلفریب را دربند</p></div>
<div class="m2"><p>بفسون و دم و فسانه بیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست تثلیث را ببر بکنار</p></div>
<div class="m2"><p>پای توحید در میانه بیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مغنی بزن نوای طرب</p></div>
<div class="m2"><p>می و چنگ و دف و چغانه بیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زن دم از عشق در حیاض و ریاض</p></div>
<div class="m2"><p>ماهی و مرغ در ترانه بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر نه چرخ را بچنبر حال</p></div>
<div class="m2"><p>ای بلند اختر یگانه بیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببر از دست صعوه دل و باز</p></div>
<div class="m2"><p>باز لاهوتی آشیانه بیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند در پرده ئی درآی ببزم</p></div>
<div class="m2"><p>بچم اندر بر و چمانه بیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بجهان دل صفا جانی</p></div>
<div class="m2"><p>ای جهانداور زمانه بیار</p></div></div>