---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>جان و دل و دین و رگ و پوست عشق</p></div>
<div class="m2"><p>در همه شیا بتکاپوست عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم بدل کاشته بی حاصلان</p></div>
<div class="m2"><p>غافل ازین نکته که خودروست عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو یکی باشد یا عشق اوست</p></div>
<div class="m2"><p>در سر سودازده یا اوست عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق بود بحر خدائی گهر</p></div>
<div class="m2"><p>یا که خدا قلزم و لولوست عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا که نه لولوست نه دریای ژرف</p></div>
<div class="m2"><p>ژرف چو او بینی هر دوست عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد بنیروی دو عالم شکار</p></div>
<div class="m2"><p>برتر ازین هر دو بنیروست عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر فلک را شکند در مصاف</p></div>
<div class="m2"><p>شیر غضبناک بی آهوست عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی اگر خویش بسنجی بکار</p></div>
<div class="m2"><p>سنگ گران کن گه ترازوست عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زو دل و بازوت ندارد گریز</p></div>
<div class="m2"><p>نیروی دل قوت بازوست عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کوه گرانست میان مرا</p></div>
<div class="m2"><p>بسته بزنجیر مگر موست عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر فلک ار پشت نماید دوتاست</p></div>
<div class="m2"><p>پشت ندارد همگی روست عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خالق این پنج دریچه حواس</p></div>
<div class="m2"><p>خالق این گنبد نه توست عشق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوی بکو در عقب او متاز</p></div>
<div class="m2"><p>بر سر هر برزن و هر کوست عشق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر تو بچوگان خدائی زنی</p></div>
<div class="m2"><p>در گذر عرصه دل گوست عشق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمزمه خلوت سر صفا</p></div>
<div class="m2"><p>همهمه ساحت مینوست عشق</p></div></div>