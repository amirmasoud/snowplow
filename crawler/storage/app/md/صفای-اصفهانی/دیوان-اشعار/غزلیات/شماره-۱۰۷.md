---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>امشب سر آن دارم کز خانه برون تازم</p></div>
<div class="m2"><p>این خانه هستی را از بیخ براندازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن خانه گور آمد جان جیفه گورستان</p></div>
<div class="m2"><p>زین جیفه بپرهیزم این خانه بپردازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه ام و داند دیوانه بخود خواند</p></div>
<div class="m2"><p>او سلسله جنباند من عربده آغازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با روح قدس همراه بودیم و بماند از من</p></div>
<div class="m2"><p>من بال نیفکندم بی روح قدس تازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ششدر عشقش دل واماند در این بازی</p></div>
<div class="m2"><p>گر پاک نبازم جان با نرد غمش بازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتشم و راهی جز صبر نمیدانم</p></div>
<div class="m2"><p>هم گریم و هم خندم هم سوزم و هم سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بسته سودایم این سلسله از پایم</p></div>
<div class="m2"><p>بردار که بگریزم بگذار که بگدازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بال بیفشانم این گرد علایق را</p></div>
<div class="m2"><p>بر خاک به ننشینم بر ساعد شه بازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من آینه ذاتم این زنگ طبیعت را</p></div>
<div class="m2"><p>از آینه بزدایم این آینه بطرازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگرفته ز سر تا پا آئینه دهم صیقل</p></div>
<div class="m2"><p>تا عکس بیندازد آن دلبر طنازم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من بچه شهبازم بر دوش و سر سلطان</p></div>
<div class="m2"><p>گر ناز کنم صد ره شه باز کشد نازم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اورنگ خلافت را داود مزامیرم</p></div>
<div class="m2"><p>سر میشکند سنگم دل میبرد آوازم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من مورم و نشمارم بر باد سلیمان را</p></div>
<div class="m2"><p>در بادیه عشقش من از همه ممتازم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راز ازلی مشکل پوشید توان از دل</p></div>
<div class="m2"><p>دل خواجه این منزل من محرم این رازم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در قاف احد دارد سیمرغ صفا منزل</p></div>
<div class="m2"><p>زین شمع نمی برد پروانه پروازم</p></div></div>