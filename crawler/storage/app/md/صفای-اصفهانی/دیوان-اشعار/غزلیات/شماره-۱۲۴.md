---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>بتار موی بتی شد سلاسل دل من</p></div>
<div class="m2"><p>ببین بضعف که یکموی شد سلاسل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشیده ابروی آن ترک نیم مست کمان</p></div>
<div class="m2"><p>پی شکار دل این مرغ نیم بسمل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپرده دیدم و بی پرده در شمائل او</p></div>
<div class="m2"><p>بشکل صورت تصویر شد شمائل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه فتنه بود که رفت از مقابل من و باز</p></div>
<div class="m2"><p>نشسته است شب و روز در مقابل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزاد عشقم و پرورد و کشت و برد خاک</p></div>
<div class="m2"><p>ندانم از چه بزاد آنکه بود قاتل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسوخت ز آتش و خاکسترم سپرد بباد</p></div>
<div class="m2"><p>چه آب بود که از او سرسته شد گل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال مغز بسر دارم و نهفته بپوست</p></div>
<div class="m2"><p>بمغز خشک ببین و خیال باطل من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه در سفرم باز در مقام خودم</p></div>
<div class="m2"><p>که هم ترازوی ما هست برج محمل من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار مرحله طی کرده راه مانده هنوز</p></div>
<div class="m2"><p>ز من بپرس که گویم کجاست منزل من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پدید گشت بیک عمر جستجوی که بود</p></div>
<div class="m2"><p>من آنکه میدوم اندر قفاش در دل من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه پرده بود که روشن نبود دیده دل</p></div>
<div class="m2"><p>ز طلعتی که بود آفتاب محفل من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکیست شاهد و مشهود و آشکار و نهان</p></div>
<div class="m2"><p>شوید جمع و نمائید حل مشکل من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فنای کون و مکان باشد و بقای صفاست</p></div>
<div class="m2"><p>همانکه پیش تو دریاست هست ساحل من</p></div></div>