---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>اگر آن مرغ که رفت از بر من باز آید</p></div>
<div class="m2"><p>باز بشکسته پر روح به پرواز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ باغ ملکوتست دل من که پرید</p></div>
<div class="m2"><p>بهوائی که اگر صعوه رود باز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف او سلسله عشق بود چنگ زنم</p></div>
<div class="m2"><p>که بگوش دل از آن سلسله آواز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرم راز حقیقت در فقرست و فنا</p></div>
<div class="m2"><p>کیست جز دل که مقیم حرم راز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طنز چبود بخداوندی ما سجده برید</p></div>
<div class="m2"><p>که بخلوتگه ما آن بت طناز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجز پیش آر و نیاز ایدل سر گشته که یار</p></div>
<div class="m2"><p>نازنینیست که از دستگه ناز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمذاق من شوریده خیال لب دوست</p></div>
<div class="m2"><p>شهد آلوده تر از شکر اهواز آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هفت گردنده چالاک بگردش نرسند</p></div>
<div class="m2"><p>دل چو دردست حقیقت بتک و تاز آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر وحدت چو تجلی کند از غیب وجود</p></div>
<div class="m2"><p>آفتابیست که از مشرق اعجاز آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب روح القدست اینکه به نای دل من</p></div>
<div class="m2"><p>دم قدسی دمد و دمگه و دمساز آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق سریست که تا سر نسپاری ندهند</p></div>
<div class="m2"><p>نیست نان پاره که از دکه خباز آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمع اضداد کند خسرو توحید صفا</p></div>
<div class="m2"><p>این صدائیست که در خلوت خراز آید</p></div></div>