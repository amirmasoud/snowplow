---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>ما و دل سودا زده سرمست الستیم</p></div>
<div class="m2"><p>بر گشته ز میخانه دو آشفته مستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با افسر سلطانی کونین بلندیم</p></div>
<div class="m2"><p>با خاک در خاک نشینان تو پستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موهوم بود هستی ما سر تو موجود</p></div>
<div class="m2"><p>المنه لله کزین واهمه رستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما شیشه شکستیم و کف پای ملک را</p></div>
<div class="m2"><p>زین شیشه بشکسته درین بادیه خستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عشق تو دیوانه و با جام تو سرمست</p></div>
<div class="m2"><p>چون نیست شدیم از همه با عشق تو هستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوند مهمات ز کونین بریدیم</p></div>
<div class="m2"><p>با رشته پیمان سر زلف تو بستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما باز قوی منزلت ساعد جانیم</p></div>
<div class="m2"><p>از بام جهان با پر افراشته جستیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد تو برو مسجدی و صومعه ئی باش</p></div>
<div class="m2"><p>ما رند خرابات رو باده پرستیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهین وجودیم بحبس تن خاکی</p></div>
<div class="m2"><p>کز قوت پر این قفس تنگ شکستیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برخاسته از کنگره عرش و ب آفاق</p></div>
<div class="m2"><p>پرواز نمودیم و ببام تو نشستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صحرای ترا آهوی در بند گرفتار</p></div>
<div class="m2"><p>دریای ترا ماهی افتاده بشستیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زانکه فقیریم فقیر در شاهیم</p></div>
<div class="m2"><p>ور زانکه خرابیم از آن ساغر و دستیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ساقی مستان به صفا رطل دمادم</p></div>
<div class="m2"><p>مخمور بمگذار که ما مست الستیم</p></div></div>