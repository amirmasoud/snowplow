---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>بدل نه تاب که تا درد عشق چاره کنم</p></div>
<div class="m2"><p>بتن نه جامه که از دست درد پاره کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز استخوان گذرد ناوک محبت دوست</p></div>
<div class="m2"><p>مگر دلی که مرا هست سنگ خاره کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شماره غم دل چون کنم ز عشق مگر</p></div>
<div class="m2"><p>ستاره فلک واژگون شماره کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک ز دامن من کسب آفتاب کند</p></div>
<div class="m2"><p>من ار زدیده بدامان خود ستاره کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشسته در دلی ای سرو باغ جان برخیز</p></div>
<div class="m2"><p>که من قیامت موعود خود نظاره کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زهد خشک چه حاصل من اعتماد سپس</p></div>
<div class="m2"><p>بدامن تر رند شرابخواره کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تلخکامی فرقت بکوی باده فروش</p></div>
<div class="m2"><p>اگر قرار گرفتم بمی غراره کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گرم شد سرم از باده نه بنای بلند</p></div>
<div class="m2"><p>بیک دو عربده بی بام و برج و باره کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه غم ز پست و بلند زمین حادثه بار</p></div>
<div class="m2"><p>مرا که اطلس چرخ بلند یاره کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوم بعرصه شطرنج کائنات دلیر</p></div>
<div class="m2"><p>شهان پیاده کنم بازی سواره کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا که عمر دوباره ست بوسه از لب یار</p></div>
<div class="m2"><p>که بوسم آن دو لب و زندگی دوباره کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلم گرفت ز بیحاصلان بیهده گوی</p></div>
<div class="m2"><p>غبار مدرسه تا چند زیب شاره کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرو کنم بخراباتیان بی سر و پای</p></div>
<div class="m2"><p>دل خراب و خرابات را اجاره کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>براز اشاره بود دوست و رنه پرده ز کار</p></div>
<div class="m2"><p>برافکنم چو بابروی خود اشاره کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مراست طوق ولایت بگردن دل و جان</p></div>
<div class="m2"><p>که اعتصام بحبل دو گوشواره کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا که خانه منور ز آفتاب صفاست</p></div>
<div class="m2"><p>چه التفات بماه و بماهپاره کنم</p></div></div>