---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بجهان می ندهم آنچه مرا در سر ازوست</p></div>
<div class="m2"><p>که مرا در سر ازو آنچه جهان یکسر ازوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید گلگونه مقصود بهر روی که دید</p></div>
<div class="m2"><p>چشم بیننده که دارد دل دانشور ازوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کشم گر نکشم باده خمخانه یار</p></div>
<div class="m2"><p>خم ازو خانه ازو باده ازو ساغر ازوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دید ز آئینه خود گونه اکسیر مرا</p></div>
<div class="m2"><p>دل که نه آئینه بر شده خاکستر ازوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان پست و رواق حرم عشق بلند</p></div>
<div class="m2"><p>این بنائیست که بالای فلک چنبر ازوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمش از خاطر و سوداش ز دل می نرود</p></div>
<div class="m2"><p>دل سودا زده و خاطر غم پرور ازوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ و پیکانش اگر بر سرو بر سینه ماست</p></div>
<div class="m2"><p>چه غم ای خواجه که هم سینه ازو هم سر ازوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک شو خاک که در کوی خرابات مغان</p></div>
<div class="m2"><p>خاک راهست که بر فرق شهان افسر ازوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق اکسیر مرادست که در بوته دل</p></div>
<div class="m2"><p>دوران دارد و گلگونه عاشق زر ازوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر در میکده تا حلقه صفت بی سر و پای</p></div>
<div class="m2"><p>نشوی راه بباطین نبری کاین در ازوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جوی از خاک صفا گر طلبی آب بقا</p></div>
<div class="m2"><p>این غباریست که آئینه اسکندر ازوست</p></div></div>