---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>بشری دل من کامشب یار آید و جان بخشد</p></div>
<div class="m2"><p>آن زندگی باقی بر مرده روان بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حد ازل قائم ملک ابد دائم</p></div>
<div class="m2"><p>هر پیر غلامی را آن شاه جوان بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای جلوه ربانی زان قطره نیسانی</p></div>
<div class="m2"><p>بر گوهر انسانی بحر آرد و جان بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسکندر صاحب دل کائینه کند جان را</p></div>
<div class="m2"><p>درویش پریشان را دیهیم کیان بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکستر درویشی آئینه دولت را</p></div>
<div class="m2"><p>از زنگ بپردازد وز سنگ امان بخشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلطان گه باطن ز آبادی درویشان</p></div>
<div class="m2"><p>ویرانه ظاهر را صد گنج نهان بخشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر قابله فطرت با سابقه رحمت</p></div>
<div class="m2"><p>آن بالغه حکمت در سمع جهان بخشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نطق گهر ریزد در بارد و زر ریزد</p></div>
<div class="m2"><p>بر سنگ بدان سختی کو رطب لسان بخشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر مغرب ناسوتی در مشرق لاهوتی</p></div>
<div class="m2"><p>آن اختر هاهوتی خورشید عیان بخشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مور بپرهیزم گر روی بگرداند</p></div>
<div class="m2"><p>با شیر دراویزم گر تاب و توان بخشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ملک قدم بخشم از حدثنی ربی</p></div>
<div class="m2"><p>گر شیخ دو من ارزان از مال فلان بخشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرمایه ز چالاکی از دولت افلاکی</p></div>
<div class="m2"><p>کاین دستگه خاکی در سود زیان بخشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میری که دهد ما را تاج و کمر دولت</p></div>
<div class="m2"><p>بر مار دهد دندان بر مور میان بخشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توقیع نبوت را بر صدر شود عنوان</p></div>
<div class="m2"><p>انگشت ولایت را بر کلک زبان بخشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر خاطر دل جوید فیاض ازل ما را</p></div>
<div class="m2"><p>فیض شب قدر ما اندر رمضان بخشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر جان صفا خواهی از این دل و جان بگذر</p></div>
<div class="m2"><p>شاه آید و دل آرد یار آید و جان بخشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان سر دهد و افسر بر عامی و بر عارف</p></div>
<div class="m2"><p>دل صاف صفاپرور بر خرد و کلان بخشد</p></div></div>