---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>حیرتست این کوی یاران را صلا باید زدن</p></div>
<div class="m2"><p>گام سمت وادی فقر و فنا باید زدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست سلطان را درین وادی گذر دست نیاز</p></div>
<div class="m2"><p>دولت ار خواهی بدامان گدا باید زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موسیا از جان گذشتن روی جانان دیدنست</p></div>
<div class="m2"><p>تکیه بر حق پای بر دست و عصا باید زدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چنین میدان اگر تیغ آید از سر باک نیست</p></div>
<div class="m2"><p>بلکه بر بازوی قاتل مرحبا باید زدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستیی شرطست دید یار را نز درد جهل</p></div>
<div class="m2"><p>باده تحقیق از جام بلا باید زدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کف فقرست مفتاح در گنج وجود</p></div>
<div class="m2"><p>پای اسکتبار بر فرق غنا باید زدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرگ نبود مردن عشاق را در کیش عشق</p></div>
<div class="m2"><p>سور توحیدست این زیروستا باید زدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد دل را عشق دردانگیز یکتا در جنون</p></div>
<div class="m2"><p>دست در زنجیر آن زلف دوتا باید زدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه الا الله ار خواهی چو منصوران یار</p></div>
<div class="m2"><p>کوس سبحانی فراز دار لا باید زدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در خم چوگان کثرت بودن از ناراستیست</p></div>
<div class="m2"><p>گوی از میدان توحید خدا باید زدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بکشتن دست بدهد پای هشتن در وصال</p></div>
<div class="m2"><p>پیش روی دوست در خون دست و پا باید زدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کی رسی ای پای بند تن بسربازان یار</p></div>
<div class="m2"><p>گام در این ره ب آئین صفا باید زدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در قفای بنده معنی قدم خواهی نهاد</p></div>
<div class="m2"><p>حشمت سلطان صورت را قفا باید زدن</p></div></div>