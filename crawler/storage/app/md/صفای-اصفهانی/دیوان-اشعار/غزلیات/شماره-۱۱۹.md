---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>دل بردی از من به یغما ای ترک غارتگر من</p></div>
<div class="m2"><p>دیدی چه آوردی ای دوست از دست دل بر سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو در دل نهان شد دل زار و تن ناتوان شد</p></div>
<div class="m2"><p>رفتی چو تیر و کمان شد از بار غم پیکر من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌سوزم از اشتیاقت در آتشم از فراقت</p></div>
<div class="m2"><p>کانون من سینهٔ من سودای من آذر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من مست صهبای باقی زآن ساتکین رواقی</p></div>
<div class="m2"><p>فکر تو در بزم ساقی ذکر تو رامشگر من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مهره در ششدر عشق یک چند بودم گرفتار</p></div>
<div class="m2"><p>عشق تو چون مهره چندیست افتاده در ششدر من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل در تف عشق افروخت گردون لباس سیه دوخت</p></div>
<div class="m2"><p>از آتش و آه من سوخت در آسمان اختر من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گبر و مسلمان خجل شد دل فتنه آب و گل شد</p></div>
<div class="m2"><p>صد رخنه در ملک دل شد زَ اندیشهٔ کافر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکرانه کز عشق مستم میخواره و می پرستم</p></div>
<div class="m2"><p>آموخت درس الستم استاد دانشور من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلطان سیر و سلوکم مالک رقاب ملوکم</p></div>
<div class="m2"><p>در سورم و نیست سوگم بین نغمهٔ مزمر من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عشق سلطان بختم در باغ دولت درختم</p></div>
<div class="m2"><p>خاکستر فقر تختم خاک فنا افسر من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با خار آن یار تازی چون گل کنم عشقبازی</p></div>
<div class="m2"><p>ریحان عشق مجازی نیش من و نشتر من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل را خریدار کیشم سرگرم بازار خویشم</p></div>
<div class="m2"><p>اشک سپید و رخ زرد سیم من است و زر من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول دلم را صفا داد آیینه‌ام را جلا داد</p></div>
<div class="m2"><p>آخر به باد فنا داد عشق تو خاکستر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا چند در های و هویی ای کوس منصوری دل</p></div>
<div class="m2"><p>ترسم که ریزند بر خاک خون تو در محضر من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بار غم عشق او را گردون ندارد تحمل</p></div>
<div class="m2"><p>کی می‌تواند کشیدن این پیکر لاغر من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل دم ز سر صفا زد کوس تو بر بام ما زد</p></div>
<div class="m2"><p>سلطان دولت لوا زد از فقر در کشور من</p></div></div>