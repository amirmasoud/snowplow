---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ساقی درد کشان دی در میخانه گشود</p></div>
<div class="m2"><p>آشنائی نظر لطف به بیگانه گشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برد در پای خم و بر دل پیمان شکنم</p></div>
<div class="m2"><p>عقده‌ها بود بسی باز به پیمانه گشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل شد آزاد چو او از شکن زلف بخال</p></div>
<div class="m2"><p>گره و سلسله و خم بسر شانه گشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دام بادانه بهر مرغ زیان بود و مرا</p></div>
<div class="m2"><p>کار مرغ دل ازین دام که با دانه گشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد دیوانه ام از عشق و برین گونه زرد</p></div>
<div class="m2"><p>چشم وا کرد و شفاخانه دیوانه گشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من پی گنج غم عشق تو ویرانه شدم</p></div>
<div class="m2"><p>مثلست اینکه در گنج بویرانه گشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار با من سخنی گفت و ز هر عضو مرا</p></div>
<div class="m2"><p>بنوانطق چو از استن حنانه گشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل هر عقده بکار سر آنزلف فکند</p></div>
<div class="m2"><p>شانه عشق قوی دست بدندانه گشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سما جستم و در سینه من داشت وطن</p></div>
<div class="m2"><p>آنچه از شهر بنگشود ز کاشانه گشود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در شب تیره دلم بود چو پروانه ببند</p></div>
<div class="m2"><p>شمع روشن شد و بال پر پروانه گشود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیض بردم ز هزاران در دل باز بجد</p></div>
<div class="m2"><p>چون طلب کرد در فیض جداگانه گشود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیده بودم که بود جایگهش ساعد شاه</p></div>
<div class="m2"><p>بال آن روز که شهباز من از لانه گشود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قفل غم بود صفا را بدل از دست دوئی</p></div>
<div class="m2"><p>نفس پیر هم از همت مردانه گشود</p></div></div>