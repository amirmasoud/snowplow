---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>باز دل زیر غم عشق چنانست که بود</p></div>
<div class="m2"><p>بار این خسته همان کوه گرانست که بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالها بود صلاح دل من صحبت عشق</p></div>
<div class="m2"><p>بازهم مصلحت وقت همانست که بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بارها آمده بر سینه ام آن ناوک و باز</p></div>
<div class="m2"><p>بکمین دلم آن سخت کمانست که بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد و کشت مرا جان دگر داد و گذشت</p></div>
<div class="m2"><p>باز می آمد و آن آفت جانست که بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک گهر سفت و دو دریا شد و آن در یتیم</p></div>
<div class="m2"><p>لب لعلش بهمان طرز و بیانست که بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم مزن آه مکش سر غمش فاش مکن</p></div>
<div class="m2"><p>این همان آتش جانسوز نهانست که بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سوار قدر انداز مکن سخت رکاب</p></div>
<div class="m2"><p>توسن عشق همان سست عنانست که بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیر گشتم بخوانی ز غم عشق و هنوز</p></div>
<div class="m2"><p>خاطرم خسته آن تازه جوانست که بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیرت و سان دلم بود بطفلی غم دوست</p></div>
<div class="m2"><p>پیرم و دل بهمان سیرت و سانست که بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود حیرانیم از فرقت و وصل آمد باز</p></div>
<div class="m2"><p>در سر و سینه من آن هیجانست که بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما با قصای یقین تاخته با دامن تر</p></div>
<div class="m2"><p>زاهد خشک بدان وهم و گمانست که بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوه نبود بثبات من آشفته مست</p></div>
<div class="m2"><p>در دل شیخ هنوز آن خفقانست که بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در صفای من و در صوفی دکان دغل</p></div>
<div class="m2"><p>تا صف حشر همان سود و زیانست که بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سود من بردم و صوفی بزیان آمد و شیخ</p></div>
<div class="m2"><p>عمرش آخر شد و بیچاره همانست که بود</p></div></div>