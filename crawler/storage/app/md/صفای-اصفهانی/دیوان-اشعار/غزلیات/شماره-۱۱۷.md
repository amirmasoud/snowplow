---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>شاهد ماهست مخفی در ظهور خویشتن</p></div>
<div class="m2"><p>آفتاب ماست در جلباب نور خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احمد ما بست احرام از در دیر طلب</p></div>
<div class="m2"><p>تا مشرف شد بمعراج حضور خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موسی جان را بصیرت داد و از شاخ درخت</p></div>
<div class="m2"><p>نوبت انی انا الله زد بطور خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد دل را نغمه داودی و بی حرف و صوت</p></div>
<div class="m2"><p>خواند در کهسار و در وادی زبور خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر پر بگرفت بدو و ختم را چون جلوه کرد</p></div>
<div class="m2"><p>آن سلیمان حقیقت در ظهور خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیسی ما را بشارت داد بر نور وجود</p></div>
<div class="m2"><p>آفتاب روح با اندام عور خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کامل ما نقطه شد در تحت بای اسم ذات</p></div>
<div class="m2"><p>گشت ساری در حروف و در سطور خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یار بر کون و مکان بگذشت و جان تازه داد</p></div>
<div class="m2"><p>هر دل و هر جان که دید اندر عبور خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از کمال ذات آمد تا هیولای نخست</p></div>
<div class="m2"><p>کامل مطلق که نپسندد قصور خویشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کنار جوی خود رویاند سرو قد خویش</p></div>
<div class="m2"><p>در بهشت خود خرامان کرد حور خویشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من بخاک افتاده بودم کرد بر رویم نگاه</p></div>
<div class="m2"><p>چشم نگشاید بکس یار ار غرور خویشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیرتش خاکستر بود صفا بر باد داد</p></div>
<div class="m2"><p>سوخت ما را یار با عشق غیور خویشتن</p></div></div>