---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دو چشم او که ندانم فرشته یا که پریست</p></div>
<div class="m2"><p>بخواب رفت و مرا در بدن تب سهریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستاره کس به ندیدست و آفتاب بهم</p></div>
<div class="m2"><p>بر آفتاب رخش لب ستاره سحریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ستاره نبیند که گونه مه من</p></div>
<div class="m2"><p>ز آفتاب بود خوبتر ز بی بصریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زوال شمس پدیدست و شمس طلعت یار</p></div>
<div class="m2"><p>منزهست ز تغییر و از زوال بریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیان ماست خبرهای غیب بی خبران</p></div>
<div class="m2"><p>بران سرند که پایان کار بی خبریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکار شاه نمودم درین قفس زنهار</p></div>
<div class="m2"><p>گمان بد نبرد کس که باز من هنریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدست و خط بتم سوری و سپر غم خلد</p></div>
<div class="m2"><p>چه جای لاله باغ بنفشه طبریست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فراز قامت بالنده روی دلبر ماست</p></div>
<div class="m2"><p>چو آفتاب که بالای سرو غاتفریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود چو باز شکاری بوقت بردن دل</p></div>
<div class="m2"><p>که در خرامش او شیوه های کبک دریست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمر کن از سر آن زلف و حکمران بدوام</p></div>
<div class="m2"><p>که بی ثباتی این خسروان ز بی کمریست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزار نکته بکارست شاه را که تمام</p></div>
<div class="m2"><p>سوای مملکت آرائیست و تاجوریست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپیش تیغ فنا ای سوار مرکب دل</p></div>
<div class="m2"><p>ز عشق دوست سپر کن که آسمان سپریست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز سر قدم کن و طی کن طریق عشق صفا</p></div>
<div class="m2"><p>فروتر از قدم آن سر که در هوای سریست</p></div></div>