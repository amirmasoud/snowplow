---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دوش در فقر ما چتر و لوا بخشیدند</p></div>
<div class="m2"><p>افسر سلطنت ملک بقا بخشیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مالک ملک بقا گشتم و سلطان غنا</p></div>
<div class="m2"><p>این تسلط بمن از فقر و فنا بخشیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده پیر مغانم که گدایان درش</p></div>
<div class="m2"><p>سلطنت را بمن بی سر و پا بخشیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد بود این دل دیوانه سودا زده را</p></div>
<div class="m2"><p>آشنایان ره عشق دوا بخشیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بودم آواره گم کرده ره سوخته ئی</p></div>
<div class="m2"><p>همت و پای و ره و راهنما بخشیدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک کونین گرفتند و فقیرم کردند</p></div>
<div class="m2"><p>علم الله که در فقر غنا بخشیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان جسمانی بیدانش و بی دید مرا</p></div>
<div class="m2"><p>زنده کردند و بتن روح لقا بخشیدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خود و دیده و دل پاک ربودند و سپس</p></div>
<div class="m2"><p>بر دل و دیده من نور خدا بخشیدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس ذات و قمر و انجم اسما و صفات</p></div>
<div class="m2"><p>تو چه دانی که باین ذره چها بخشیدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فقر کامل شد و سلطان غنا کرد ظهور</p></div>
<div class="m2"><p>خود کلید در این گنج بما بخشیدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگرفتند سر و سینه پر باد و هوا</p></div>
<div class="m2"><p>دل بی کینه بی کبر و ریا بخشیدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود من بود خطائی که ز حد بود برون</p></div>
<div class="m2"><p>شاه بودند و بمن بنده خطا بخشیدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من صفا بودم و آئینه ام آلوده زنگ</p></div>
<div class="m2"><p>زنگ زائینه زدودند و صفا بخشیدند</p></div></div>