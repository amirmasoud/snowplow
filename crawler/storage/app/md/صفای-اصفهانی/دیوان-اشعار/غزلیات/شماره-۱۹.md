---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>رویت همه آتشست و آبست</p></div>
<div class="m2"><p>مویت همه حلقه است و تابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل گل و وقت صبح برخیز</p></div>
<div class="m2"><p>ای چشم دلم چه وقت خوابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگشای ز هم هلال ابروی</p></div>
<div class="m2"><p>در جام میی چو آفتابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنشسته ببارگاه گلبن</p></div>
<div class="m2"><p>گل خسرو مالک الرقابست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر که درین سراست بلبل</p></div>
<div class="m2"><p>از اول صبح در خطابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کای تشنه خفته در بیابان</p></div>
<div class="m2"><p>برخیز که هر چه هست آبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آبست و سراب نیست غافل</p></div>
<div class="m2"><p>لب تشنه خفته در سرابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل ز جناب عشق مگریز</p></div>
<div class="m2"><p>جبریل مقیم آن جنابست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر پرده برافکنند از کار</p></div>
<div class="m2"><p>بینند که یار بی نقابست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورشید بدین تجلی و تاب</p></div>
<div class="m2"><p>در دیده بسته در حجابست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر دیده که باز شد بتوحید</p></div>
<div class="m2"><p>از گونه وصل نور یابست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن شاه بود بخانه فقر</p></div>
<div class="m2"><p>گنجینه بمنزل خرابست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکحرف ز درس آشنائی</p></div>
<div class="m2"><p>بهتر ز هزار من کتابست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر دفتر عشق را بخوانی</p></div>
<div class="m2"><p>یک نقطه او هزار بابست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیرست صفا بمسلک عشق</p></div>
<div class="m2"><p>با آنکه هنوز در شبابست</p></div></div>