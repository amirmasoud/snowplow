---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>گاه دی است و نوبت فصل بهار من</p></div>
<div class="m2"><p>بنشسته است یار چو گل در کنار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر گنج خسروی ندهم کنج خانقاه</p></div>
<div class="m2"><p>امروز دور دور من و یار یار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان یافتم ز دولت دل در حضور یار</p></div>
<div class="m2"><p>فرخنده است روز چنین روزگار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جبریل را ز بال فکند و هنوز نیست</p></div>
<div class="m2"><p>در اوج خویش باز حقیقت شکار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی دلم بسمت دیاری بود که اوست</p></div>
<div class="m2"><p>ابروی دوست قبله شرع دیار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش و نگار را بزدودم ز لوح دل</p></div>
<div class="m2"><p>تا گشت جای جلوه نقش نگار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جسم و جان امید بریدم هزار بار</p></div>
<div class="m2"><p>تا دید روی او دل امیدوار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیدم که عشق اوست خداوند کائنات</p></div>
<div class="m2"><p>روزی که شد بکوی حقیقت گذار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بردم بپای عشق بسر سجده نیاز</p></div>
<div class="m2"><p>یک قبله گشت و یکدل و یکروی کار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دادم زمام مملکت دل بدست دوست</p></div>
<div class="m2"><p>باقی نماند در کف من اختیار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبحست و یار ساقی و من در خمار دوش</p></div>
<div class="m2"><p>یارب پذیر عذر لب میگسار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز صاف غم که صیقل آئینه صفاست</p></div>
<div class="m2"><p>کو آب رحمتی که نشاند غبار من</p></div></div>