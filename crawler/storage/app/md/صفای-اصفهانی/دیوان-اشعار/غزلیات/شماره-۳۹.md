---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>قومی بگرد کوی فنا راهبر شدند</p></div>
<div class="m2"><p>بر چشم دل کشیده و صاحب نظر شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحب نظر شدند که از دار اقتدار</p></div>
<div class="m2"><p>در کوی فقر آمده و خاک در شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قومی بر آستان حقیقت نهاده سر</p></div>
<div class="m2"><p>کاین راهرا بپای طلب پی سپر شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی برهنه پا و سر از یمن فقر پای</p></div>
<div class="m2"><p>بنهاده بر سر خودی و تاجور شدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دست دل گرفته ز سر تا بپای تن</p></div>
<div class="m2"><p>در عشق و پیش تیر ملامت سپر شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردند جای درخم چوگان زلف یار</p></div>
<div class="m2"><p>چون گوی آن گروه که بی پا و سر شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وارسته از تعین ظلمت سرای خاک</p></div>
<div class="m2"><p>خورشید را معاینه نور بصر شدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خاک و گل رسیده باسرار جان و دل</p></div>
<div class="m2"><p>هم سیر آفتاب و رفیق قمر شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این طوطیان قند شکر رسته زین قفس</p></div>
<div class="m2"><p>با کام تلخ همدم کان شکر شدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر قلب همچو مس زده اکسیر انقیاد</p></div>
<div class="m2"><p>در بوته وداد شدند آب و زر شدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این مناخ تنگ ندیدند جای امن</p></div>
<div class="m2"><p>تصمیم عزم داده بکاخ دگر شدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در کوی دل رسیده فکندند بار خویش</p></div>
<div class="m2"><p>آسوده از مهالک سیر و سفر شدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با آنکه بود در دل عشاقشان مقام</p></div>
<div class="m2"><p>مانند سر عشق بعالم سمر شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وصل آورد افاقه و در دور اتصال</p></div>
<div class="m2"><p>دیوانگان عشق تو دیوانه تر شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای شیر حق ز پرده برون آی کز خفات</p></div>
<div class="m2"><p>موران ماده همسر شیران نر شدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از دیدن و شنیدنت ای مهدی صفا</p></div>
<div class="m2"><p>دجال سیرتان کدر و کور و کر شدند</p></div></div>