---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>دردیست ز عشق او به جانم</p></div>
<div class="m2"><p>پیداست ز جسم ناتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این سوز ز جان رسید بر پوست</p></div>
<div class="m2"><p>از پوست به مغز استخوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نام و نشان خود گذشتم</p></div>
<div class="m2"><p>من بنده شاه بی‌نشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برهان جلالت من اینست</p></div>
<div class="m2"><p>پیرم به تجلی و جوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آنکه جوانم آسمان را</p></div>
<div class="m2"><p>چون تیر گذشته از کمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون قاصد کعبه حضورم</p></div>
<div class="m2"><p>مقصود زمین و آسمانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تابنده آستان فقرم</p></div>
<div class="m2"><p>چرخست گدای آستانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با آنکه تنم ز عشق موئیست</p></div>
<div class="m2"><p>در پهلوی نفس پهلوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در وادی ایمنم چو موسی</p></div>
<div class="m2"><p>بر گله خویشتن شبانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای آنکه کنی به بحر و کان روی</p></div>
<div class="m2"><p>من گوهر بحر و زرکانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگذشته ازین و آن و چون روح</p></div>
<div class="m2"><p>در صورت این و سر آنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من باز سپیدم و مهیاست</p></div>
<div class="m2"><p>بر ساعد شاه آشیانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرورده نعمت حکیمم</p></div>
<div class="m2"><p>بر خوان وجود میهمانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از کوزه عیسی است آبم</p></div>
<div class="m2"><p>از سفره احمدست نانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با اینهمه قدر و جاه، فانی</p></div>
<div class="m2"><p>در مهدی صاحب‌الزمانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من نیستم اوست کیستم من</p></div>
<div class="m2"><p>پیداست صفای اصفهانم</p></div></div>