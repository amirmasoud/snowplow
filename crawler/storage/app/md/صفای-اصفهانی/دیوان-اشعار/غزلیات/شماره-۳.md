---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>بنشین به پس زانو در مصطبه جان‌ها</p></div>
<div class="m2"><p>تا چند همی گردی بر گرد بیابان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذار سر ای سالک بر پای گدای دل</p></div>
<div class="m2"><p>تا تاج نهند از سر در پای تو سلطان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مزرعه دنیی حاصل نتوان بردن</p></div>
<div class="m2"><p>در مزرعه گر بارد از چشم تو باران‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کوه اگر گویم این راز ز هم ریزد</p></div>
<div class="m2"><p>گویی دل سنگینت زد پتک به سندان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشکافت به طنازی بشکست به طراری</p></div>
<div class="m2"><p>تیرش همه جوشن‌ها، زلفش همه پیمان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ماه همی‌تابد، آن سرو همی‌روید</p></div>
<div class="m2"><p>در زاویه دل‌ها از باغچه جان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چرخ چرا جویی کز تست پریشان‌تر</p></div>
<div class="m2"><p>سرّی که بود پنهان در سینه انسان‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاهی که بود درویش سلطان دلست ار نه</p></div>
<div class="m2"><p>بر تخت همی‌ماند بر صورت ایوان‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهنشه فقرستی شایسته سلطانی</p></div>
<div class="m2"><p>مردست که خواهد برد این گوی ز میدان‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلطان که بود آدم از دیو نپرهیزد</p></div>
<div class="m2"><p>شمشیر یداللهی برد سر شیطان‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با دوست نیندیشم در این دی و این بهمن</p></div>
<div class="m2"><p>آن طرفه بهار خوش با آن گل و ریحان‌ها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابروی نگار من ابطال کشد در خون</p></div>
<div class="m2"><p>زین طرفه کمان آمد بر سینه چه پیکان‌ها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این سر نتوان گفتن جز بر سر دار ای دل</p></div>
<div class="m2"><p>اسرار صفا یکسر ثبتست به دیوان‌ها</p></div></div>