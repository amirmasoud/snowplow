---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>آنانکه در صراط صعود ولایتند</p></div>
<div class="m2"><p>از حق نزول کرده و بر خلق آیتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایت زدند بر ز بر بام امر و خلق</p></div>
<div class="m2"><p>در خطه امارتشان زیر رایتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کونین در تغیر و مستان جام عشق</p></div>
<div class="m2"><p>در کوی میفروش بظل حمایتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مفتی کند روایت و در راه کوی فقر</p></div>
<div class="m2"><p>واماندگان قافله اهل درایتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پیشگاه عشق گدایان ره نشین</p></div>
<div class="m2"><p>صاحب سریر دولت بدوند و غایتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بحر موج خیز فنا کشتی نجات</p></div>
<div class="m2"><p>بر آسمان فقر نجوم هدایتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معشوق در نهایت حسنست و در خفاست</p></div>
<div class="m2"><p>با آنکه عاشقان رخش بی نهایتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر غیر روی یار ببینند در وجود</p></div>
<div class="m2"><p>اهل شهود در خور جرم و خیانتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قومی که رسته اند ز وهم و خیال نفس</p></div>
<div class="m2"><p>در بارگاه عقل امیر کفایتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذشته از تقید جانند و قید جسم</p></div>
<div class="m2"><p>وارسته از تعین شکر و شکایتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای دل ز اهل مدرسه بگریز کاین گروه</p></div>
<div class="m2"><p>مخدوم بنده اند ولی بی عنایتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آستان میکده فقر خاک باش</p></div>
<div class="m2"><p>ای تشنه لب که دردکشان در سقایتند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما محرم معاینه و همرهان هنوز</p></div>
<div class="m2"><p>در پرده حدیث و حجاب روایتند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این سبطیان سر زده از موسی کمال</p></div>
<div class="m2"><p>از نیل رسته اند و بتیه غوایتند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جمعی که خوانده درس دل از مدرس صفا</p></div>
<div class="m2"><p>در شارع حقیقت شرع ولایتند</p></div></div>