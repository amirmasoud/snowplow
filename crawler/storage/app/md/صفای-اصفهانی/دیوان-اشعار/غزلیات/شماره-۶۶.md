---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>زین سپس دل را به رسوایی نشان خواهیم کرد</p></div>
<div class="m2"><p>با غم عشق تواش همداستان خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین خراب آباد وحشت خیمه برخواهیم کند</p></div>
<div class="m2"><p>خانه در کوی خرابات مغان خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده از بالای چون تیر تو بر خواهیم داشت</p></div>
<div class="m2"><p>پشت تیر چرخ زین بالا، کمان خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی زمین و آسمان آب بقا خواهیم خورد</p></div>
<div class="m2"><p>خاک بر فرق زمین و آسمان خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر و آب زندگی و ما و کف خاک فنا</p></div>
<div class="m2"><p>تا کدامین زین دو عمر جاودان خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک را و خشت را از دولت اکسیر فقر</p></div>
<div class="m2"><p>گنج باد آورد و گنج شایگان خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهباز دل چو بال افراخت در معراج عشق</p></div>
<div class="m2"><p>شهپر روح القدس را امتحان خواهیم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نان این بیدولتان خاکست و خون ما خویش را</p></div>
<div class="m2"><p>بر سر خوان حقیقت میهمان خواهیم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میهمان خواهیم شد در خلوت فقر و فنا</p></div>
<div class="m2"><p>وندران خلوت خدا را میزبان خواهیم کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهر خدمت رشته جان بر میان خواهیم بست</p></div>
<div class="m2"><p>زین گران جانان سنگین دل، کران خواهیم کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان و دل خواهیم داد اندر سر سودای عشق</p></div>
<div class="m2"><p>عقل پندارد درین سودا زیان خواهیم کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملکتی جوئیم بیرون از قیاس واز قران</p></div>
<div class="m2"><p>وندران درویش را صاحبقران خواهیم کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این مکان عاریت کی در خور درویش ماست</p></div>
<div class="m2"><p>این گدا را پادشاه لامکان خواهیم کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ما روان گنج کونینیم و سلطان وجود</p></div>
<div class="m2"><p>چون تجلی کرد ایثار روان خواهیم کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تیغ اگر آید بپیش تیغ سر خواهیم داشت</p></div>
<div class="m2"><p>تیرا گر بارد نشان تیر جان خواهیم کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچه جز بردار نتوان گفت آن خواهیم گفت</p></div>
<div class="m2"><p>آنچه جز با قتل نتوان کرد آن خواهیم کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر خورشید حقیقت را که در غرب خفاست</p></div>
<div class="m2"><p>جلوه گر از جانب شرق عیان خواهیم کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما و سر دل دو خورشیدیم بر گردون امر</p></div>
<div class="m2"><p>با تراب صاحب الامر اقتران خواهیم کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این قفس کی در خور پرواز سیمرغ صفاست</p></div>
<div class="m2"><p>صعوه را ما باز قدسی آشیان خواهیم کرد</p></div></div>