---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>برفت هر که در اینخانه بود و یار بماند</p></div>
<div class="m2"><p>هزار نقش زدودیم تا نگار بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مرا بکنار اختیار کرد و بچرخ</p></div>
<div class="m2"><p>نماند و آرزوی چرخ در کنار بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت هر چه ز هر خار زخم دید گلم</p></div>
<div class="m2"><p>گلی بود که منزه ز زخم خار بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیر باغ وجود آمد آن بهار و گذشت</p></div>
<div class="m2"><p>چه نقشها که درین باغ از آن بهار بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طربسرای مرا بود سروی از قد یار</p></div>
<div class="m2"><p>بلند و دلکش و سرسبز و پایدار بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدار عشق چه منصورهاست بر سر دار</p></div>
<div class="m2"><p>گمان مبر تو که منصور رفت و دار بماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار پرده بهر راز داشتم من و عشق</p></div>
<div class="m2"><p>درید و راز درون من آشکار بماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببرد سیل سرشکم هزار کوه ز جای</p></div>
<div class="m2"><p>غم تو بود که چون کوه استوار بماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ثبات کوه و قرار زمین و دور سپهر</p></div>
<div class="m2"><p>نماند و میکده عشق بر قرار بماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نماند شعری و چندین هزار شعر بلند</p></div>
<div class="m2"><p>ز عشق روی تو از من به یادگار بماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز عقل پیر ضیا و ز نفس نور بدهر؟</p></div>
<div class="m2"><p>ز طبع من سخن نغز آبدار بماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به کوی یار پریشان بسی رسید و گذشت</p></div>
<div class="m2"><p>به جز حکایت من کاندرین دیار بماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه غم که دولت دنیی نماند بهر صفا</p></div>
<div class="m2"><p>خزائن گهر پاک شاهوار بماند</p></div></div>