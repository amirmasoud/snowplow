---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>خوش آن گروه که شوریده شراب شدند</p></div>
<div class="m2"><p>شدند در پی آبادی و خراب شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فدای همت دُردی‌کشان که هستی خویش</p></div>
<div class="m2"><p>تمام داده کشیدند درد و ناب شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشید دردی جام طلب بطفلی و پیر</p></div>
<div class="m2"><p>بکوی میکده در عالم شباب شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مخواه قدر فلک ذره باش بر در دوست</p></div>
<div class="m2"><p>که ذرکان در دوست آفتاب شدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رموز عشق ز ام الکتاب سینه خویش</p></div>
<div class="m2"><p>فرا گرفته و مستغنی از کتاب شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خراب عشق نمودند خانه دل خود</p></div>
<div class="m2"><p>امین گوهر آن گنج دیر یاب شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم بهمرهی عشق رفت تا بر یار</p></div>
<div class="m2"><p>دو رهنورد درین ورطه همرکاب شدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بغیر عشق کسی طی این طریق نکرد</p></div>
<div class="m2"><p>که گم شدند اگر باد یا سحاب شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا گمان که دگر پای بند می نشوند</p></div>
<div class="m2"><p>ز نه حجاب فلک رسته بی حجاب شدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه جستند از صد هزار بند ولیک</p></div>
<div class="m2"><p>اسیر در خم آن طره بتاب شدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدست شاه دو باز سپید برپابند</p></div>
<div class="m2"><p>ببند تیره تر از شهپر عقاب شدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمال دوست عیان دیده زلف اوست عیان</p></div>
<div class="m2"><p>زهر سراب گذشتند و عین آب شدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپیده دم شد و شد آفتاب یار پدید</p></div>
<div class="m2"><p>کسان ندیده که مردند یا بخواب شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو حرف یافته آنان که در کتاب صفا</p></div>
<div class="m2"><p>کتاب عشق شدند و هزار باب شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار باب بهر باب صد هزار بیان</p></div>
<div class="m2"><p>ز هر بیان همه کونین کامیاب شدند</p></div></div>