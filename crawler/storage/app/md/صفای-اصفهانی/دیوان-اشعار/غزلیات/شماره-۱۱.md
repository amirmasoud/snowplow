---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>پس دیوار تن بر شده ماهیست عجب</p></div>
<div class="m2"><p>بمنش با نظر لطف نگاهیست عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر پادشه دولت پاینده فقر</p></div>
<div class="m2"><p>از ره عشق مرا برد که راهیست عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کف مرگ توان جست بهمدستی عشق</p></div>
<div class="m2"><p>عشق در حادثه مرگ پناهیست عجب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاعت عشق صوابست که مقبول خداست</p></div>
<div class="m2"><p>سر بی عشق بتن بار گناهیست عجب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب از یوسف دل نیست که افتاد بچاه</p></div>
<div class="m2"><p>کنده زیر رسن زلف تو چاهیست عجب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز بر بسته پر و صعوه پرد با پر باز</p></div>
<div class="m2"><p>عرصه کون و مکان شعبده گاهیست عجب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دعوی عشق مرا حسن دلیلست قوی</p></div>
<div class="m2"><p>شاهد حسن ترا عشق گواهیست عجب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایکه محبوب جهانی تو ببستان بهشت</p></div>
<div class="m2"><p>رسته از باغ رخت مهر گیاهست عجب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمان پست و تو سلطان بلند اختر حسن</p></div>
<div class="m2"><p>بنشین بر دل وارسته که گاهیست عجب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پادشه بنده فقرست که از سایه دوست</p></div>
<div class="m2"><p>بر سر پادشه فقر کلاهیست عجب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل ما دستگه سلطنت شاه صفاست</p></div>
<div class="m2"><p>بنده شاه صفائیم که شاهیست عجب</p></div></div>