---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>یار از پرده برون آمد و جان پیدا شد</p></div>
<div class="m2"><p>برقع از روی برافکند و جهان پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم زلف مسلسل بسر شانه گشود</p></div>
<div class="m2"><p>گره و سلسله و کون و مکان پیدا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینکه پیداست نهان بود پس پرده غیب</p></div>
<div class="m2"><p>گشت بی پرده و پیدا و نهان پیدا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتماشای گل و سرو روان گشت بباغ</p></div>
<div class="m2"><p>گل نوخاسته و سرو روان پیدا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برد از باغ دلم کونه آن طرفه بهار</p></div>
<div class="m2"><p>آن کدورت که بایام خزان پیدا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر گنج حقیقت که ب آبادی دین</p></div>
<div class="m2"><p>گم شد از من بخرابات مغان پیدا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حشمت سلطنت خاک نشین در فقر</p></div>
<div class="m2"><p>کی توان گفت که از تخت کیان پیدا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز گمان و ز یقین رستم و از دانش و دید</p></div>
<div class="m2"><p>تا یقینی که مرا بود گمان پیدا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدل شیفته رازی که نهان بود ز غیب</p></div>
<div class="m2"><p>گشت پیدا و چگویم که چسان پیدا شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم بگرفتم از آن یار که دارم بکنار</p></div>
<div class="m2"><p>تا که در چشم من آن موی میان پیدا شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مژه یا ناوک دلدوز بود ماه مرا</p></div>
<div class="m2"><p>آنچه از خانه ابروی کمان پیدا شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل که گم شد بجوانی ز صفا در غم پیر</p></div>
<div class="m2"><p>در خم طره آن تازه جوان پیدا شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوست دل را به سویدا و به سر و به خفا</p></div>
<div class="m2"><p>یار ما را به سر و چشم و زبان پیدا شد</p></div></div>