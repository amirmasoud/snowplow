---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>کفر آئین منست ار عشق را تمکین کنم</p></div>
<div class="m2"><p>کافر عشقم اگر من پشت بر آئین کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر باطن را گذارم بر فراز عرش پای</p></div>
<div class="m2"><p>خاک خذلان بر سر معراج ظاهر بین کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوای دوست می پرند با هم کبک و باز</p></div>
<div class="m2"><p>کبک را فرخنده خوانم باز را تحسین کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر دهم گر صعوه را از عشق عنقای قدم</p></div>
<div class="m2"><p>قاف را تا قاف پر سیمرغ و پر شاهین کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی گذارم طائر تقدیس را آلوده بال</p></div>
<div class="m2"><p>من که مرغ خانه را شهباز علیین کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر عشق دوست را گر سیر انسانی کند</p></div>
<div class="m2"><p>در مقام قلب بر روح القدس تلقین کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذرم از هفت خوان تن گر از تن بگذرم</p></div>
<div class="m2"><p>ور نه بر جان کی رسم گر جسم را روئین کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هواهای تن این حیوان اصطبل و علف</p></div>
<div class="m2"><p>جان نکاهم رفرف معراج دل را زین کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده امکان فرو گیرم ز رخسار وجوب</p></div>
<div class="m2"><p>کون را یکباره بی امکان و بی تکوین کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی وحدت را کنم بی پرده چونان آفتاب</p></div>
<div class="m2"><p>خاکیان را بی نیاز از ماه و از پروین کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عرصه توحید را پردازم از صف نفاق</p></div>
<div class="m2"><p>دست حق در ذوالفقار صفدر صفین کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنگ خودبینی کند مرآت دل را بی صفا</p></div>
<div class="m2"><p>من صفایم نیستی را پیشوای دین کنم</p></div></div>