---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>دی گفت به من بگریز از ناوک خون‌ریزم</p></div>
<div class="m2"><p>گفتم که ز دستانت کو پای که بگریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بازم و گر شیرم با صولت آهویت</p></div>
<div class="m2"><p>نه بال که برپرم نه بال که بستیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با سوز غم عشقت در کوره حدادم</p></div>
<div class="m2"><p>با کار سر زلفت در فتنه چنگیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از موی گره واکن صد سلسله شیدا کن</p></div>
<div class="m2"><p>تا من دل سودائی در سلسله آویزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستان رخت بر من آموخت بسی دستان</p></div>
<div class="m2"><p>دستان زن این بستان چون مرغ سحرخیزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان صاف روان پرور لبریز کن آن ساغر</p></div>
<div class="m2"><p>چندان که بیالائی این خرقه پرهیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با باده فرو آور از توسن تن جان را</p></div>
<div class="m2"><p>تا تارک کیوان را ساید سم شبدیزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آتشم از خویت ای یار پس از مردن</p></div>
<div class="m2"><p>بنشین به سر خاکم کز بوی تو برخیزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن حلقه که از زلفت در گردن جان دارم</p></div>
<div class="m2"><p>بگشایم اگر روزی صد فتنه برانگیزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آمیخت غمت خونم با خاک که نگذارد</p></div>
<div class="m2"><p>خونی که به رخ مالم خاکی که به سر ریزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از درد تو مخمورم زان صاف صفا پرور</p></div>
<div class="m2"><p>وقتست که پیمائی جامی دو سه لبریزم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زین شعر صفاهانی آشوب خراسانم</p></div>
<div class="m2"><p>هم فتنه شیرازم هم آفت تبریزم</p></div></div>