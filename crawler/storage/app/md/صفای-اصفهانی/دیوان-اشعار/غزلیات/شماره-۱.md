---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>عشق رخت براه حقیقت سمند ما</p></div>
<div class="m2"><p>خاک درت دوای دل دردمند ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودائیان عشق توایم و در آتشیم</p></div>
<div class="m2"><p>در سوز دائمیم و نباشد گزند ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد بدست کوته ما تاب زلف دوست</p></div>
<div class="m2"><p>بیدار بود اختر بخت بلند ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر پسند پست و بلندیم در کمال</p></div>
<div class="m2"><p>ای جلوه جمال تو خاطر پسند ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شکر تو شهد مذاق دل امید</p></div>
<div class="m2"><p>تلخست بی شرنگ غمت کام قند ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما خاک تیره و رخ خوب تو آفتاب</p></div>
<div class="m2"><p>ما صید لاغر و سر زلفت کمند ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پندم دهد که عاشق دیوانه ئی و هست</p></div>
<div class="m2"><p>دیوانه آنکه میدهد از عشق پند ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جستیم چون تو آمدی از جا سپندوار</p></div>
<div class="m2"><p>بی آتش وصال تو چبود سپند ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای فارس ترا فرس امر زیر ران</p></div>
<div class="m2"><p>بجهاندی از علائق امکان نوند ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی رائض عنایتت از اولین قدم</p></div>
<div class="m2"><p>می نگذرد نهایت سیر سمند ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در سینه است و در دل ما سر عشق و هست</p></div>
<div class="m2"><p>غافل ز سر ما سر ناهوشمند ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگذشت بر سبیل حکایت مدار عمر</p></div>
<div class="m2"><p>شد گریه های ما همگی ریشخند ما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برق براق نیستی و رفرف فناست</p></div>
<div class="m2"><p>در راه فقر دوست کبود و کرند ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای خواجه تا بچونی و در چند نیستی</p></div>
<div class="m2"><p>هستیست در خور دل بیچون و چند ما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ما عرش وحدتیم و پر مرغ عقل شیخ</p></div>
<div class="m2"><p>بر بام خانه می نرسد از خرند ما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پند از صفا دریغ نباشد ولیک حیف</p></div>
<div class="m2"><p>شد پند ما بمدرک محجوب بند ما</p></div></div>