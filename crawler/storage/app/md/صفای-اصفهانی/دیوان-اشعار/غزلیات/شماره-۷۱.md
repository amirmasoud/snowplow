---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>دوش از خاک در فقر کلاهم دادند</p></div>
<div class="m2"><p>افسر سلطنت ماهی و ماهم دادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جستم از دشمن بیگانه پناه از در دوست</p></div>
<div class="m2"><p>آشنایان در دوست پناهم دادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمقامی که ره فقر بسلطان ندهند</p></div>
<div class="m2"><p>من درویش صفت رفتم و راهم دادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر من سود شبی پای گدای در عشق</p></div>
<div class="m2"><p>صبحدم دستگه و افسر شاهم دادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر خط زندگی و ملک بقای ابدی</p></div>
<div class="m2"><p>زان خط سبز و سر زلف سیاهم دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه من بیهده نبود که ره سیر سما</p></div>
<div class="m2"><p>ساکنان ملکوت از خط آهم دادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز چه محبوب جهانم من بیگانه ز خویش</p></div>
<div class="m2"><p>از خط دوست مگر مهر گیاهم دادند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف جاه بدم پیشتر از خلقت جان</p></div>
<div class="m2"><p>خلق کردند تن و راه بچاهم دادند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مالک ملک ازل نیز بچاهم نگذاشت</p></div>
<div class="m2"><p>والی مصر ابد کرده و جاهم دادند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپر و چتر و علم سلطنتم کس به نداد</p></div>
<div class="m2"><p>فر سلطان حقیقت بنگاهم دادند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طاعت ار بود مرا بود بسنگ پر کاه</p></div>
<div class="m2"><p>کوه رحمت بشکوه پر کاهم دادند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رحمت خاص که از بی گنهانست بری</p></div>
<div class="m2"><p>بارش مزرع من شد که گناهم دادند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فقر و درماندگی و بندگی و عجز و نیاز</p></div>
<div class="m2"><p>جلواتیست که از فیض الهم دادند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه و بیگاه در دوست زدم خاصان راه</p></div>
<div class="m2"><p>بدرون گاه ندادندم و گاهم دادند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا نمودند مرا محرم اسرار صفا</p></div>
<div class="m2"><p>بار دادند بخلوتگه و گاهم دادند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز تباهی به نرستند سلاطین سلوک</p></div>
<div class="m2"><p>هر چه دادند بدین حال تباهم دادند</p></div></div>