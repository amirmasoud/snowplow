---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>من دوش رخ صدق و صفا را دیدم</p></div>
<div class="m2"><p>عنوان محبت و وفا را دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادم بصفا صیقل آئینه دل</p></div>
<div class="m2"><p>در صیقل آئینه خدا را دیدم</p></div></div>