---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای چشم دل خواب نئی بیداری</p></div>
<div class="m2"><p>ای مغز سرم مست نئی هشیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیری سری علامتی آثاری</p></div>
<div class="m2"><p>فتحی کشفی قیامتی دیداری</p></div></div>