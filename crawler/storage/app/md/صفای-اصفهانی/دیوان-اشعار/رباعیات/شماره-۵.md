---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>درجان مقیدان جمال مطلق</p></div>
<div class="m2"><p>ساری شد و جلوه کرد و برگشت ورق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق خلق شد و خلق حق و هر دو یکست</p></div>
<div class="m2"><p>خلقست بجای خلق حقست بحق</p></div></div>