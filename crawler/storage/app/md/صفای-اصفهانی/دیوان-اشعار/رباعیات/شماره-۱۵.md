---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>پرسی از من که مست یا هشیاری</p></div>
<div class="m2"><p>آری مستم باده پرستم آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست که مست و محو و حیران توام</p></div>
<div class="m2"><p>من روی ترا سیر ندیدم باری</p></div></div>