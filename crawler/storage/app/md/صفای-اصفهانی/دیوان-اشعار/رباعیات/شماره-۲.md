---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>من بنده تو پادشاه پاینده بذات</p></div>
<div class="m2"><p>ای ذات مقدس تو سلطان صفات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدلی و جواد و قادر و قائل کل</p></div>
<div class="m2"><p>یا همهمه اراده و علم و حیات</p></div></div>