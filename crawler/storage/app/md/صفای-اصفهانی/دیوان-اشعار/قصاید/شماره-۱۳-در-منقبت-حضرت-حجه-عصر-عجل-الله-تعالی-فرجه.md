---
title: >-
    شمارهٔ  ۱۳ - در منقبت حضرت حجه عصر عجل الله تعالی فرجه
---
# شمارهٔ  ۱۳ - در منقبت حضرت حجه عصر عجل الله تعالی فرجه

<div class="b" id="bn1"><div class="m1"><p>آن زلف باز دولت خورشید زیر بالش</p></div>
<div class="m2"><p>هندوی سایه پرور در زیر زلف و خالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی آفتاب گویم روئی که بر نتابد</p></div>
<div class="m2"><p>خورشید آسمانی با ابروی هلالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فرط خوبروئی زد راه عقل پیرم</p></div>
<div class="m2"><p>طفلی که نیست بیرون از هفت و هشت سالش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میمست غنچه او جان پای بند میمش</p></div>
<div class="m2"><p>دالست طره او دل دستگیر دالش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدی مرا و گفتی آشفته حالی آری</p></div>
<div class="m2"><p>سودائی غم عشق آشفته است حالش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افکند تیر عشقش اسفندیار روئین</p></div>
<div class="m2"><p>آری تهمتنست این پرورده است زالش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل پیر عقل داند من را و دوش دیدم</p></div>
<div class="m2"><p>طفلی که بر نیایم امروز با خیالش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان و دلیست ما را این هر دو در کف او</p></div>
<div class="m2"><p>جان خسته کمندش دل بسته دوالش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جود همچو ساقی طبعش ملال گیرد</p></div>
<div class="m2"><p>من پیش او دهم جان تا ننگرم ملالش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مور میگریزم زین ضعف چون ستیزم</p></div>
<div class="m2"><p>با آنکه میگریزد شیر نر از غزالش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رندان می پرستند مست می الستش</p></div>
<div class="m2"><p>دردیکشان مستند آلوده زلالش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این صید را نگیرد شیری که نیست چنگش</p></div>
<div class="m2"><p>این بام را نپرد مرغی که نیست بالش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشقست این میفتید در حبس و دام و بندش</p></div>
<div class="m2"><p>شیرست این مخارید چنگال و دم و یالش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن خواست تا نهد سر از دل بپای دلبر</p></div>
<div class="m2"><p>بین آرزوی ابتر و اندیشه محالش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در سینه اینکه داری سنگ و گلست و جانان</p></div>
<div class="m2"><p>جان و دلست مفریب از سنگ و از سفالش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بتخانه هوی را مجلای دوست دانی</p></div>
<div class="m2"><p>وائینه ات مکدر بی جلوه جمالش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من ز اشتغال رستم با عشق دوست بستم</p></div>
<div class="m2"><p>خوشا دلی که باشد با دوست اشتغالش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بندش سلاسل دل تیغش حمائل جان</p></div>
<div class="m2"><p>گر میکشد مباحش ور میکشد حلالش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در زخم سینه ره کرد تیر زره شکافش</p></div>
<div class="m2"><p>وان زخم را تبه کرد مشگ زره مثالش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرغ ار شوم اسیرم در چنگل عقابش</p></div>
<div class="m2"><p>روی ار شوم خمیرم در پنجه جلالش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگرفتم آنکه گشتم جبریل چون نمانم</p></div>
<div class="m2"><p>از مرکب بلوغش وز رفرف کمالش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این سیرداند آنکو داند م آل انسان</p></div>
<div class="m2"><p>انسان شدن نداند تا داندی م آلش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بافرق چون بگویم اسرار جمع جمعش</p></div>
<div class="m2"><p>این نغمه را نوازم در پرده وصالش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رخش جدل برانگیخت جان بنده جدالش</p></div>
<div class="m2"><p>آواز النشورش فریاد القتالش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سلطان وحدت آمد با آنکه اوست یکتا</p></div>
<div class="m2"><p>لاهوت از یمینش ناسوت از شمالش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شنگرف ریز دازدم زنگار گون حسامش</p></div>
<div class="m2"><p>خورشید سوزد از تف سیماب گون نصالش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون آتش وجوبی تفتد بسوز امکان</p></div>
<div class="m2"><p>این پنیه زار چبود با برق اشتعالش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پتک فنای مطلق کوبد بفرق گیتی</p></div>
<div class="m2"><p>ویران کند قفارش وارون کند جبالش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آب زبان تیزش زین نه کمان بشوید</p></div>
<div class="m2"><p>مریخ و تیغ کندش تیر و زبان لالش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر چشم شرک تازد پیکان شرک سوزش</p></div>
<div class="m2"><p>با فرق کفر سازد خایسک کفر مالش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>من پیش ازان دهم جان کان شاه جنگ جوید</p></div>
<div class="m2"><p>ترسم که تنگ گردد از قتل من مجالش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن قالبی که قلبش از عرش اعظمستی</p></div>
<div class="m2"><p>گر اوفتد نپاید عرش عظیم هالش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قلبش که صور صبحش صبح قیامتستی</p></div>
<div class="m2"><p>پوشیده حی قیوم تشریف لا یزالش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر پیشتر بمیرم از موت زنده گردم</p></div>
<div class="m2"><p>نقلست موت عارف نقدست انتقالش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قد قیامت دل هرگز دوتا نگردد</p></div>
<div class="m2"><p>از قامت اولوالامر پیداست اعتدالش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قطب مدیر کامل غوث محیط اعظم</p></div>
<div class="m2"><p>سلطان سر که امرست بر ملک و بر مثالش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از شهر شاه خوبان عزم شکار دارد</p></div>
<div class="m2"><p>امروز صید صحرا فرخنده است فالش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قوس ازل کمانش بالای دوست تیرش</p></div>
<div class="m2"><p>جسم فلک گوزنش جان ملک مرالش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با آنکه غیر عشقش موجود نیست آوخ</p></div>
<div class="m2"><p>از قلب زود رنجش در بود بد سگالش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بشری که بد سگالان دارند قلب منکوس</p></div>
<div class="m2"><p>من کوس مینوازم در بام و جد و حالش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آمد شه حقایق در کف کمند توحید</p></div>
<div class="m2"><p>گردن نهید گردن در بند امتثالش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با آنکه عرش اعظم هست از جهات بیرون</p></div>
<div class="m2"><p>از هر جهت که بینی فرشست از طلالش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با آنکه هر چه دارند خاقان و قیصر از اوست</p></div>
<div class="m2"><p>خاقان دهد خراجش قیصر دهد منالش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر صدر پاسبانی گر بنگری برین در</p></div>
<div class="m2"><p>خورشید را توان دید گرد صف نعالش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>درویش بی سر و پاش گر سلطنت سگالد</p></div>
<div class="m2"><p>افسر دهد طغانش ملکت دهد ینالش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر کوه را بینی بی موی دوست بینی</p></div>
<div class="m2"><p>از مویه همچو مویش از ناله همچو نالش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در پیشگاه عشقش عقل ار چه پای پوید</p></div>
<div class="m2"><p>با آنکه حیلت او نگذشته از سبالش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>عشق آتشیست مضمر نه آسمانش مجمر</p></div>
<div class="m2"><p>خورشید و ماه و اختر افروخته ذگالش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بشکست حقه چرخ وا کرد عقده دل</p></div>
<div class="m2"><p>دست قضا شکوهش شست قدر فعالش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دجال چون گریزد از کارزار مهدی</p></div>
<div class="m2"><p>شیر عرین چو غرد قربان شود شگالش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گاوست خویش پرور از بهر عید قربان</p></div>
<div class="m2"><p>دجال گاو مهدی عیدست در قتالش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دل شهر بند وحدت گنج جلال سلطان</p></div>
<div class="m2"><p>کوپال فقر بر کف عشقست کوتوالش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پیداست روی جانان اما بپیش چشمی</p></div>
<div class="m2"><p>کز توتیای ما زاغ دادند اکتحالش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نخلیست آسمانی خرماش لا مکانی</p></div>
<div class="m2"><p>طوبی لک ار نشانی در باغ دل نهالش</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>واصل مشو که واصل در سیر نیست کامل</p></div>
<div class="m2"><p>یعنی بوصل زن چنگ در زلف اتصالش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بی جسم و جان و دل شو با دوست متصل شو</p></div>
<div class="m2"><p>فانیست قطره تا هست از بحر انفصالش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تو جان جان جانی از مرگ جسم مگریز</p></div>
<div class="m2"><p>جان تو نیست فانی مندیش زارتحالش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رمل و رماد باشد دنیی ز هر دو بگذر</p></div>
<div class="m2"><p>بر باده ده رمادش بر آب زن رمالش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دیوی کریه منظر هم کفر و هم جنونش</p></div>
<div class="m2"><p>زالی سیاه پستان هم عطسه هم سعالش</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جان باش تا نبینی هرگز شکنجه تن</p></div>
<div class="m2"><p>روح القدس نباشد اندیشه نکالش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>باز یقین زند پر در جو قاف عنقا</p></div>
<div class="m2"><p>شک است زاغ زن سنگ بر بال احتیالش</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>باز سپید شه را از این قفس رها کن</p></div>
<div class="m2"><p>کز طبل باز سلطان باز آیدی تعالش</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شب ها ز دولت خویش بی طعمه کی پسندد</p></div>
<div class="m2"><p>کز عقل تا هیولاست پرورده نوالش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چرخ دل صفا را از ابر کرد صافی</p></div>
<div class="m2"><p>زان روست مطلع الشمس مرآت مه صقالش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بیضای دست موسی سر زد ز آستینش</p></div>
<div class="m2"><p>عشق آتش مثالیست دل طور بی مثالش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>در چشم نیست مویش با جسم نیست خویش</p></div>
<div class="m2"><p>نه نفس او عدویش نه عقل او عقالش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>برهان اوست روشن توحید اوست پیدا</p></div>
<div class="m2"><p>پیداست سر وحدت حق نیستی همالش</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دل مرکزست و جانش پرگار مرکز دل</p></div>
<div class="m2"><p>نه پای در دواد و نه دست در سؤالش</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون نیستان شکر از مغز خویش قوتش</p></div>
<div class="m2"><p>جسمی نزار و جانی از شهد مال مالش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تابیده آفتابش از مشرق تجلی</p></div>
<div class="m2"><p>نه آفت هبوطش نه فتنه وبالش</p></div></div>