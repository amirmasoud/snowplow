---
title: >-
    شمارهٔ  ۱۹ - فی کمالات النفسانیه و مراتب الانسانیه
---
# شمارهٔ  ۱۹ - فی کمالات النفسانیه و مراتب الانسانیه

<div class="b" id="bn1"><div class="m1"><p>وحدت جمعم نه لامکان نه مکانم</p></div>
<div class="m2"><p>برتر ازین هر دوام نه این و نه آنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسته ام از این مکان و کون و مرکب</p></div>
<div class="m2"><p>فرد بسیطم محیط کون و مکانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی نهم اندر قفای کام جهان گام</p></div>
<div class="m2"><p>منکه سرا پای صد هزار جهانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیشتر از آنکه طور زاید و موسی</p></div>
<div class="m2"><p>بر گله عقل و نفس و وهم شبانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نخورد جز که بر نشانه توحید</p></div>
<div class="m2"><p>تیر شهود ار جهد زشست و کمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن بری از حدود نقطه سیال</p></div>
<div class="m2"><p>دائره و مرکز و مدیر زمانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسکه بلندم نکرده باز ز هم بال</p></div>
<div class="m2"><p>می نرسد دست آسمان بمیانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمسم و ذراتم این ثوابت و سیار</p></div>
<div class="m2"><p>ماهم و این آفتاب و ماه کتانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قطبم از آن ثابتم بمرکز تجرید</p></div>
<div class="m2"><p>روحم از ان در مجردست روانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فانیم و باقیم بماء/من سرمد</p></div>
<div class="m2"><p>دهر و زمان در پناه امن و امانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صرف وجودم نه صورتم نه هیولی</p></div>
<div class="m2"><p>وحدت بی صورتم نه جسم و نه جانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ره عشق امتیاز پیر و جوان نیست</p></div>
<div class="m2"><p>تا چه کند عقل پیر و بخت جوانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک سرو چندین هزار سر ربوبی</p></div>
<div class="m2"><p>یک تن و دریا و گوهر و زر و کانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فارس فحلم چنو که قائد توفیق</p></div>
<div class="m2"><p>تا در صاحب زمان کشیده عنانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رستم وقتم نبرد دیو هوی را</p></div>
<div class="m2"><p>حکمت بر گستوان و ببر بیانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نور احد کرده از جهات تجلی</p></div>
<div class="m2"><p>بر من و زان جلوه از جهات جهانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از یمن دل و زیر رایحه الله</p></div>
<div class="m2"><p>بجهاند از کوه تن چو برق یمانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بود بطفلی دلم بزرگتر از عرش</p></div>
<div class="m2"><p>نور تجلی بزرگ کرد و کلانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایدون عرش عظیم و مشرق بیضاش</p></div>
<div class="m2"><p>هست سهامن بدل چو چرخ کیانم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باغ نهال هدایت سلف از کلک</p></div>
<div class="m2"><p>رشد خلف میوه درخت بنانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من نه بخود زنده ام هویت ساریست</p></div>
<div class="m2"><p>ساری در روح و سر و نطق و بیانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باز شهم بال میزنم بهوایش</p></div>
<div class="m2"><p>یا شهم و همچو باز در طیرانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می پرم از بدو تا نهایت بیحد</p></div>
<div class="m2"><p>طائر بیحد و بدو و ختم و کرانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اول و آخر یکیست اول و آخر</p></div>
<div class="m2"><p>خواهی پیدای من ببین و نهانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من نه بخسرو مقیدم نه به درویش</p></div>
<div class="m2"><p>خسرو و درویش هر دو در همیانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گنج احد غیب و در شهادت مطلق</p></div>
<div class="m2"><p>هست مفاتیح غیب زیر زبانم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این نه زبان منست و زمزمه من</p></div>
<div class="m2"><p>حرف تو همصحبت لبست و دهانم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سامع و گوینده اوست من همه هیچم</p></div>
<div class="m2"><p>آمد و برد از میانه نام و نشانم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اوست من از فیض بخت سرمد آن ذات</p></div>
<div class="m2"><p>سرمدم و دهرم و زمانم و آنم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنم از آنم بعین نقطه سیال</p></div>
<div class="m2"><p>در ازل و لایزال پاک روانم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زاده ام از لامکان بصورت و در سیر</p></div>
<div class="m2"><p>من پدر پیر لامکان و مکانم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کرده ز شش سوی روی دوست تجلی</p></div>
<div class="m2"><p>بر دل و جانم نه بل بخان و بمانم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر و عیانم بعین آینه اوست</p></div>
<div class="m2"><p>آینه چبود خود اوست سر و عیانم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زنده بامرم نه بلکه آمر ساری</p></div>
<div class="m2"><p>خلق نه بل امر زنده از سریانم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سیرت و سانم بود بمسلک توحید</p></div>
<div class="m2"><p>صرف وجودست سر سیرت و سانم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نافه ناف غزال چین تجلی</p></div>
<div class="m2"><p>عطر مشام اللهم نه مشک و نه بانم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ملک من از نفخه صعق هله فانیست</p></div>
<div class="m2"><p>مملکت کل من علیها فانم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صاف نشاط دل من از خم اسماست</p></div>
<div class="m2"><p>ساقی باقیست ذات پیر مغانم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در زده چندین هزار جام و ز اول</p></div>
<div class="m2"><p>تشنه ترم خشک مانده است لبانم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>می نپسندد ببر باری عطشان</p></div>
<div class="m2"><p>شان ولی الله علی الشانم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر به نبینم بچشم دل رخ مقصود</p></div>
<div class="m2"><p>نیستم انسان بی بدل حیوانم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کر به نبوسد لبان من لب مطلوب</p></div>
<div class="m2"><p>طفلم و از ثدی غفلتست لبانم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خاک بدم آتش ودادم بگداخت</p></div>
<div class="m2"><p>آب روانم کنون و باد بزانم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>باد بزانم ولی بگلشن توحید</p></div>
<div class="m2"><p>آب روانم و دل بجوی جنانم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قافیه تکرار شد مرا طلب ای چرخ</p></div>
<div class="m2"><p>آنکه تو میگردی از قفاش من آنم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>والی مصر دلم که هست طبایع</p></div>
<div class="m2"><p>سبع عجاف و عقول سبع سمانم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سبع سمانم بعکس رؤیت ریان</p></div>
<div class="m2"><p>خورد عجاف خیال و وهم و گمانم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دولت کامل رسید و ساحت دل را</p></div>
<div class="m2"><p>ملک خدا کرد و کرد ملکت بانم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفتی شو نفی تا زنی در اثبات</p></div>
<div class="m2"><p>گشتم چونان و مدتیست چنانم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دست دلم زد در ولایت شمسی</p></div>
<div class="m2"><p>شمس ولایت درآمد از در جانم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>یکران کز آسمان بخاک نهد ناف</p></div>
<div class="m2"><p>در تک توحید از مهابت رانم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رانم چونانکه جبرئیل بماند</p></div>
<div class="m2"><p>کو بزمینست و من بکاهکشانم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سدره فرو دست زانکه منبر صدرش</p></div>
<div class="m2"><p>بر سر طینست و من بر از سرطانم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>صرف صفای جریده ره جانان</p></div>
<div class="m2"><p>نیست تعلق برای و روی بجانم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شمس کمالم نه آفت و نه افولم</p></div>
<div class="m2"><p>باغ بهشتم نه بهمن و نه خزانم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>صعوه نیم شاهباز سدره نشینم</p></div>
<div class="m2"><p>بنده نیم پادشاه ملک ستانم</p></div></div>