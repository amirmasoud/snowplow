---
title: >-
    شمارهٔ  ۸ - وله ایضا
---
# شمارهٔ  ۸ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>شب قدر ما آنزلف چنو شام سیاست</p></div>
<div class="m2"><p>روز را گر بودی قدر ز قدر شب ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمانست زمینی که نظر گاه منست</p></div>
<div class="m2"><p>که بهر ذره که میبینم خورشید سماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار در خلوت من هر سر شب تا دم صبح</p></div>
<div class="m2"><p>هر دم صبح بمشکویم تا وقت مساست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه بر گونه ام آنروی چنو روز سپید</p></div>
<div class="m2"><p>گاه در دستم آنزلف چنو شام سیاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم من دل شد و دل چشم بیکتائی خواست</p></div>
<div class="m2"><p>دل و چشم من یکدیده و یکدل دو گواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهدی بهتر از این نیست که در دست منست</p></div>
<div class="m2"><p>که به یکتائی او شاهد آنزلف دوتاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دل ما طلب آن قبله که هر روی بر اوست</p></div>
<div class="m2"><p>طلعت دوست بود قبله و دل قبله نماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دعوت یار مکن گر کنی ای طالب یار</p></div>
<div class="m2"><p>مگذر از دل بیدار که محراب دعاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار پیداست همی هی چه دوی سوی بسوی</p></div>
<div class="m2"><p>اوست بی سوی و ز هر سوی که بینی پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طفل وحدت به نزادست خطامام وجود</p></div>
<div class="m2"><p>مادر انکه نزادست موحد بخطاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نیست جز دوست اگر هست ببالا و بپست</p></div>
<div class="m2"><p>پست اگر بیند بینای حقیقت بالاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سست منگر بگل و سنگ و سفال و در و کوی</p></div>
<div class="m2"><p>که گل و سنگ و سفال و در و کو نیست خداست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه بهر چشم عیانست بما خورده مگیر</p></div>
<div class="m2"><p>روشنست اینکه نه هر دیده که بینی بیناست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زرفانی که نه در صره سلطان و وزیر</p></div>
<div class="m2"><p>گنج باقیست که در سلسله فقر گداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه گدائی که بود دستخوش سیم ملوک</p></div>
<div class="m2"><p>آنکه خاک کف پای او اکسیر طلاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه طلائی که بود دستکش قید خلاص</p></div>
<div class="m2"><p>زر بی غش که خلوصش دل مرد داناست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قطره و دریا پیش دل داناست یکی</p></div>
<div class="m2"><p>قطره ئی نیست اگر باشد عین دریاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عین دریاست که بگرفته سراپای وجود</p></div>
<div class="m2"><p>یک وجودست سراپای اگر سر یا پاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شرط این غوص بود جستن از جوی دوئی</p></div>
<div class="m2"><p>گوهر وحدت موجود بدریای جزاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی کم وکاست وجودست بهر ذره که هست</p></div>
<div class="m2"><p>غیر او نیست همینست سخن بی کم وکاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو خدا نیست بخیر و شرشرنیست وجود</p></div>
<div class="m2"><p>خیر محضست که در وحدت هستی یکتاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر تن کامل او صاف خدا دوخته اند</p></div>
<div class="m2"><p>شمسع نعلین اگر باشد یابند قباست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تار و پود ردی عارف ذات احدیست</p></div>
<div class="m2"><p>جامه عامی پود هوس وتار هویست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تن که از تار هوی رسته و از پود هوس</p></div>
<div class="m2"><p>درع او اسم حق و راکب و مرکوب هواست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عاد را کرد تلف مهلکه باد دبور</p></div>
<div class="m2"><p>نصرت احمد معراجی از باد صباست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آب اثبات خودی منبع او چشمه نفی</p></div>
<div class="m2"><p>نان الا طلبی معدن او سفره لاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زن در نیستی ای طالب هستی که عدم</p></div>
<div class="m2"><p>ظلماتیست که در عالم او آب بقاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همچو ما باش که بعد از سیران و طیران</p></div>
<div class="m2"><p>سفر اندر وطن و زاویه بال عنقاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیکرم دائره دور و دلم نقطه عشق</p></div>
<div class="m2"><p>که بود مرکز این دائره و پا برجاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر دو زانوی من شیفته محبوب منست</p></div>
<div class="m2"><p>کاین چنین تنگ گرفتم ببغل از چپ و راست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اینکه چل سال نسا را متمتع نشدم</p></div>
<div class="m2"><p>در طواف حرم کعبه دل حج نساست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در منی رمی جمار من اوصاف خودیست</p></div>
<div class="m2"><p>عرفات من بیدای دل بی مبداست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حجرالاسود موجود سویدای منست</p></div>
<div class="m2"><p>سعی من از طرف مروه کثرت بصفاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>محرم خلوت سریم ز میقات وجود</p></div>
<div class="m2"><p>کعبه اهل حقیقت بحقیقت اینجاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دل داناست حریم حرم خاص الخاص</p></div>
<div class="m2"><p>که لطیفست و خبیرست نه صخره نه صماست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>صخره صما باشد دل نادان که درش</p></div>
<div class="m2"><p>باشد از حقد و حسد بامش از کبر و ریاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نکند منزل در تیه ضلالت دل پیر</p></div>
<div class="m2"><p>جسته از مصر هوی موسی با دست و عصاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باستین نور خدا دارد این طرفه کلیم</p></div>
<div class="m2"><p>چون عصا بر کف آن دست که شرق بیضاست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ید بیضای کلیمست که دارد ببغل</p></div>
<div class="m2"><p>دل وارسته که در سینه چونان سیناست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز ایمن دل که برو مضغه سمعست اسیر</p></div>
<div class="m2"><p>شجر طور و طوی بالا کز حق بصداست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دل خردست سزاوار و ساده احدی</p></div>
<div class="m2"><p>که بپرداخته از فرش خودی عرش خداست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فرش این خانه ز دیبای بساتین بهشت</p></div>
<div class="m2"><p>که سمیعست و بصیرست و بهی تر دیباست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خوش بنائیست برافراشته معمار قدم</p></div>
<div class="m2"><p>قصر دل عرش ستایشگر این طرفه بناست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر چه ایوان و غرف دارد بنیان وجود</p></div>
<div class="m2"><p>این بنا راست که دست احدیت بناست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دل من با همه آثار معالی که در اوست</p></div>
<div class="m2"><p>خاک گردیست که بنشسته بایوان رضاست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حضرت پنجم آن هشتم اولاد نذیر</p></div>
<div class="m2"><p>که بود جد سه مولود و اء/ب هفت آباست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قادر مطلق و در کتفش شاهین قدر</p></div>
<div class="m2"><p>قاضی بر حق و بر دستش میزان قضاست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پسر هشتم و بر چار پسر باب نخست</p></div>
<div class="m2"><p>که ز پشت پدران آمده و جد نیاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر ز آباش نگارند بهی تر پدرست</p></div>
<div class="m2"><p>ور ز ابناش شمارند نکوتر ابناست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کیست سلطان سرای احدیت دل غوث</p></div>
<div class="m2"><p>دم عیسی کف موسی که درین بام و سراست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای خداوند سلاطین گه دولت فقر</p></div>
<div class="m2"><p>فقر من بنده بپایان شد هنگام عطاست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر چه هستیست کجا فر و بهای تو بود</p></div>
<div class="m2"><p>همه سرگرم لقای تو و آن فر و بهاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر چه موجود کجا نور و ضیای تو دمد</p></div>
<div class="m2"><p>همگی ذره اشراقی آن نور و ضیاست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر چه در حیز امکانست آثار وجوب</p></div>
<div class="m2"><p>همه در بندگی این حرم و این مولاست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بخراسان تو این مرد عراقیست غریب</p></div>
<div class="m2"><p>ایکه هم نشو من از لطف تو و هم منشاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آن نهالم که مرا دست تو در باغ وجود</p></div>
<div class="m2"><p>کشت و پرورد بتائید تو در نشو و نماست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دست دادی که بدان زد دل من باب طلب</p></div>
<div class="m2"><p>تا بایدون که نشیمنگه دل فقر و فناست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>راهبر عشق تو مقصود تو برهان وصول</p></div>
<div class="m2"><p>سر توحید که آورده مرا از ره راست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نکند چون و چرا کس که تن پیر مراد</p></div>
<div class="m2"><p>جای حقست و دلش بیرون از چون و چراست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بنده فانیست در او آری من نیستم اوست</p></div>
<div class="m2"><p>بنده جائی نبود سلطان خود در همه جاست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بحر دانش متلاطم شد و بر اوست مدیر</p></div>
<div class="m2"><p>چرخ بینش که بر او گونه توحید و ذکاست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فلک بینش چرخیست که بر منطقه اش</p></div>
<div class="m2"><p>بیحد و حصر چو خورشید فلک اخترهاست</p></div></div>