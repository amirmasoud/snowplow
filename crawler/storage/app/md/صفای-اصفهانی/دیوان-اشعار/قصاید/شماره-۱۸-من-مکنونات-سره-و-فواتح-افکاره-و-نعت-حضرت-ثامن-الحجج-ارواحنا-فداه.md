---
title: >-
    شمارهٔ  ۱۸ - من مکنونات سره و فواتح افکاره و نعت حضرت ثامن الحجج ارواحنا فداه
---
# شمارهٔ  ۱۸ - من مکنونات سره و فواتح افکاره و نعت حضرت ثامن الحجج ارواحنا فداه

<div class="b" id="bn1"><div class="m1"><p>ای چرخ گرد گرد مکش زارم</p></div>
<div class="m2"><p>خیره مگرد در پی آزارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار آسیات کند گردش</p></div>
<div class="m2"><p>کم سوده کن ز گردش بسیارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثابت نئی بسیرت خود کمتر</p></div>
<div class="m2"><p>تهدید کن ز ثابت و سیارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من مرکز زمین نیم و جورت</p></div>
<div class="m2"><p>گردد بدور چون خط پرگارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاسد مکن که تاجر تجریدم</p></div>
<div class="m2"><p>ای مشتریت مفلس بازارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فاسد مکن که قافله چینم</p></div>
<div class="m2"><p>مشک ترست تعبیه در بارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیزار کردیم تو ز خود آوخ</p></div>
<div class="m2"><p>کز هستی تو و خود بیزارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طومار وار پیچم و کردارت</p></div>
<div class="m2"><p>تبتست در مطاوی طومارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پندارم از تو کین کشم و غافل</p></div>
<div class="m2"><p>کاین لقمه نیست در خور پندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دائم بر آن سری که بیوباری</p></div>
<div class="m2"><p>ای اژدهای مردم او بارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مجبور کردیم بگرفتاری</p></div>
<div class="m2"><p>پنداشتم که فاعل مختارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مختار بودم از دل و از قالب</p></div>
<div class="m2"><p>بر صد هزار درد گرفتارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بند چار عنصر ظلمانی</p></div>
<div class="m2"><p>از این مزاج مختلف آثارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ظلمت نیم تجلی نورم من</p></div>
<div class="m2"><p>ظلمت گرفته دامن انوارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آبم ولی نه دستکش خاکم</p></div>
<div class="m2"><p>نورم ولی نه دستخوش نارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاک بسیط مرکز توحیدم</p></div>
<div class="m2"><p>باد بزان گلشن اسرارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نار نزاده زاهن و از سنگم</p></div>
<div class="m2"><p>ورد نرسته از گل و از خارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من پر کاه بودم و غم صرصر</p></div>
<div class="m2"><p>سنجیده بود چرخ بمعیارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایدون بسنگ کوه گران سنگم</p></div>
<div class="m2"><p>بل کوه را بکوبد پیکارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز و شگال چرخ نرنجاند</p></div>
<div class="m2"><p>با چون منی که ضیغم ناهارم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من شیر مرغزار الوهیت</p></div>
<div class="m2"><p>آهوی قدس طعمه و ادرارم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>برقاب هر دو قوس کنم جولان</p></div>
<div class="m2"><p>عشق دلست رفرف رهوارم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کی میرسند قافله گردون</p></div>
<div class="m2"><p>بر گرد من که قافله سالارم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چندین هزار دور ربوبی من</p></div>
<div class="m2"><p>پیشم ز چرخ و آخر ادوارم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اطوار را بدائره ام ساری</p></div>
<div class="m2"><p>در نقطه نهایت اطوارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سیر جماد کرده شدم نامی</p></div>
<div class="m2"><p>حیوان چرید یاسمن و خارم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حیوان شدم نه خار و نه گل بودم</p></div>
<div class="m2"><p>ز آدم شکفت نوگل گلزارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>انسان شدم بکار طلب رفتم</p></div>
<div class="m2"><p>مطلوب گشته باز طبکارم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سلاک راست چار سفر من خود</p></div>
<div class="m2"><p>عمریست در کشاکش اسفارم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سیر منازل سفر ثانی</p></div>
<div class="m2"><p>بیرون بود ز حیز گفتارم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بیرون بود ز خواب و خور و غفلت</p></div>
<div class="m2"><p>سیر عوالم دل بیدارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من بنده دلم که درین ظلمت</p></div>
<div class="m2"><p>بنمود راه روشن هموارم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طی کرد بر عالم ناسوتی</p></div>
<div class="m2"><p>تا بار داد در حرم یارم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بار خودی ز دوش بیفکندم</p></div>
<div class="m2"><p>بر شکر آنکه محرم این بارم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگذشته از زمان هله فانی در</p></div>
<div class="m2"><p>دیهور و دهر و سرمد و دیهارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر ملک و بر ملک شده ام قاهر</p></div>
<div class="m2"><p>مقهور عشق قاهر قهارم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از خاک ین دو دار نیالودم</p></div>
<div class="m2"><p>شهپر که باز ساعد دادارم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از بلبلان گلشن لاهوتم</p></div>
<div class="m2"><p>برگ ولایتست بمنقارم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن ناوکم که بر هدف توحید</p></div>
<div class="m2"><p>از سر نشسته تا بن سوفارم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آئینه شهودم و میتابد</p></div>
<div class="m2"><p>خورشید یار از درو دیوارم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صد ره درین مشاهده روشن تر</p></div>
<div class="m2"><p>از آفتاب آینه کردارم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بالا ترم ز پستی و از سستی</p></div>
<div class="m2"><p>در ماء/منی بلندم و ستوارم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دریای پر ز خون بودی وحدت</p></div>
<div class="m2"><p>بحر محیط او من نهمارم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خون تمام هستی ازین دریا</p></div>
<div class="m2"><p>باشد بگردن دل خونخوارم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بر آفتاب و ماه فلک سلطان</p></div>
<div class="m2"><p>درویش فقر حیدر کرارم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مست می ولایت موجودم</p></div>
<div class="m2"><p>این خمر را بخانه خمارم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با یازده خلیفه پس از حیدر</p></div>
<div class="m2"><p>یارم چنانکه دشمن اغیارم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اغیار کیست مقدرت مهدی</p></div>
<div class="m2"><p>دجالها فشرده بمنشارم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کشتم جنود نفس بهیمی را</p></div>
<div class="m2"><p>در ملک خویش قاتل کفارم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آتش زدم بمملکت شرکت</p></div>
<div class="m2"><p>شر نیستم شراره اشرارم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در شاعری مقنن قانونم</p></div>
<div class="m2"><p>بینی چو ژرف بینی اشعارم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دریای بی نهایت و بی قعرم</p></div>
<div class="m2"><p>پیداست از تشعب انهارم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هان غوص کن گهر بر سلطان بر</p></div>
<div class="m2"><p>من بحر پر ز گوهر شهوارم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بحرم محققست ز امواجم</p></div>
<div class="m2"><p>ابرم معینست ز مدرارم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سر رشته خدات بدست آید</p></div>
<div class="m2"><p>گر سر نهی برشته گفتارم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ایمن شوی ز سنگ سبکساران</p></div>
<div class="m2"><p>گر نشمری بسنگ سکبارم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>طاوس نیستم که تنم بر پر</p></div>
<div class="m2"><p>من کی بفکر درهم و دینارم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>رهزن نیم بسبک دغل بازان</p></div>
<div class="m2"><p>اما بهوش باش که طرارم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>داود وادیم که جبل گیرد</p></div>
<div class="m2"><p>رقص جمل ز نغمه مزمارم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در زیر بار عشقم چون اشتر</p></div>
<div class="m2"><p>بر دست یار باشد ماهارم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زالایش دوئیست دل صافی</p></div>
<div class="m2"><p>مکنون سر عترت اطهارم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>مشهور دهرم ازدم منصوری</p></div>
<div class="m2"><p>منصور وار بر ز بر دارم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>من کاه نیستم که اگر خیزد</p></div>
<div class="m2"><p>باد از ختن برد زی بلغارم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نز هیبت بخار چنو کوهم</p></div>
<div class="m2"><p>کافتد ز لرزه کیک بشلوارم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>از چرخ و کوه و بحر و برم برتر</p></div>
<div class="m2"><p>بیرون ز هر چهارم و هر چارم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چونان نیم بدست و دم نائی</p></div>
<div class="m2"><p>درهای و هوی وحدت ناچارم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جان کیست جسم چیست کزین ساغر</p></div>
<div class="m2"><p>نه سر بجای ماند و نه دستارم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دائر بدور خویشم و چابک تر</p></div>
<div class="m2"><p>از آفتاب گنبد دوارم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دنیی است جیفه طالب دنیی سگ</p></div>
<div class="m2"><p>سگ نیستم چه کار بمردارم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اشرار را ز رشته رقیت</p></div>
<div class="m2"><p>حرم صفای باطن احرارم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>خورشید آسمان صفا هانم</p></div>
<div class="m2"><p>نور و ضیاست حکمت و کردارم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از بندگان شاه خراسانی</p></div>
<div class="m2"><p>شمس هدی رضا سر ابرارم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر روح کفر آژده سوهانم</p></div>
<div class="m2"><p>بر چشم شرک تافته مسمارم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آنرا که نیست مور در سلطان</p></div>
<div class="m2"><p>در آستین دیده و دل مارم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در بند عشق سلسله طه</p></div>
<div class="m2"><p>گر نیستم مقید زنارم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دارم زمام ملک و ملک بر کف</p></div>
<div class="m2"><p>در کار هر دو کونم و بیکارم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>قدرم بپای فرق فلک ساید</p></div>
<div class="m2"><p>غم نیست گر نداند مقدارم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>پا تا بسر خرابم ازین کثرت</p></div>
<div class="m2"><p>در شهر بند وحدت معمارم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>نو کیسه نیستم زر دولت را</p></div>
<div class="m2"><p>گنجور گنج و کان کهن بارم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از ذرگان شمس شموسم من</p></div>
<div class="m2"><p>روشنگر شموسم و اقمارم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>در باغ عزتم گل بینائی</p></div>
<div class="m2"><p>خارت بدیده گر نگری خوارم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زنهار خوار نیستم ای رهرو</p></div>
<div class="m2"><p>مشکن اگر درستی زنهارم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>منگر بدینکه خواند خری ناقص</p></div>
<div class="m2"><p>یا منحرف مزاجی بیمارم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بنگر بدینکه مکرمت باری</p></div>
<div class="m2"><p>پرداخت چل صباح بتیمارم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>در من نماند گل که نکشت آن شه</p></div>
<div class="m2"><p>و اب حیات داد بتکرارم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تکرار چیست جلوه وحدانی</p></div>
<div class="m2"><p>بیخست و شاخ و برگ و گل و بارم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>چون شمع روز مرده و شب روشن</p></div>
<div class="m2"><p>بین روز روشنست شب تارم</p></div></div>