---
title: >-
    شمارهٔ  ۲۷ - در حکم و معارف و نعت قطب اولیاء حضرت علی بن ابیطالب علیه الصلوه و السلام
---
# شمارهٔ  ۲۷ - در حکم و معارف و نعت قطب اولیاء حضرت علی بن ابیطالب علیه الصلوه و السلام

<div class="b" id="bn1"><div class="m1"><p>ایدل ار خواهی بسر آهنگ افسر داشتن</p></div>
<div class="m2"><p>کشور تجرید را باید مسخر داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طریق اهل معنی سلطنت را شرط نیست</p></div>
<div class="m2"><p>با وجود کشور تجرید کشور داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننگ زیور دار ایدل زیور ار خواهی که هست</p></div>
<div class="m2"><p>زیور اهل حقیقت ننگ زیور داشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نا قلندر باشد آن رهرو که در طی طریق</p></div>
<div class="m2"><p>پای بند او شود موی قلندر داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتن شهوت پر روحست و در معراج عشق</p></div>
<div class="m2"><p>شاهباز عشقرا شرطست شهپر داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز روحت چون کبوتر گشت و میگفتم رواست</p></div>
<div class="m2"><p>در پایم دوست میباید کبوتر داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز بال این کبوتر را مکن شهوت مران</p></div>
<div class="m2"><p>راندن شهوت کبوتر راست ابتر داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای در شهر بقا بنهادن و فرماندهیست</p></div>
<div class="m2"><p>خاک اقلیم فنا را افسر سر داشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنچه بی رنگی کند در سیرسی رنگ وجود</p></div>
<div class="m2"><p>رنگ ارژنگی نگردد سنگ آزر داشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایکه خواهی داوری از خط داور سر مپیچ</p></div>
<div class="m2"><p>داوری باشد سر اندر خط داور داشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایکه از خر داشتن نازی بعزم دار مرگ</p></div>
<div class="m2"><p>گر مسیحی رخت تن بایست بر خر داشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیش اهل درد بهر سرفرازی خوشترست</p></div>
<div class="m2"><p>نالش سر داشتن از بالش پر داشتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اعتدال دوست را ننگست چشم از عاشقان</p></div>
<div class="m2"><p>زلف چون شمشاد و قد چون صنوبر داشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلکه دارد چشم آن کز عشق آن ابروی کج</p></div>
<div class="m2"><p>راستی مانند ابرو پشت چنبر داشتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کم نگر بر اختران بر آسمان دل برآی</p></div>
<div class="m2"><p>چیست کوری چشم بینائی ز اختر داشتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اختر توحید اگر تابید بر چرخ وجود</p></div>
<div class="m2"><p>اختر از هفت آسمان بایست برتر داشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر آنکشور که خورشید حقیقت طالعست</p></div>
<div class="m2"><p>کاریکذره ست صد خورشید خاور داشتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نفس اگر لشکر کشد با عقل نفس دیگرست</p></div>
<div class="m2"><p>عقل اگر دارد بسر سودای لشکر داشتن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جهاد نفس من کردستم این کارای پسر</p></div>
<div class="m2"><p>ترک سر کردن سبق گیرد ز مغفر داشتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیستت فرزندی این چار مام و هفت باب</p></div>
<div class="m2"><p>ای برادر خواهی ار قدر وسه خواهر داشتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میتوانی گر گذشتی زین شش و پنج و چهار</p></div>
<div class="m2"><p>هفت گیسودار گردونی بچادر داشتن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دل نبندد هر که بر زلف شئون مرآت جان</p></div>
<div class="m2"><p>میتواند با رخ جانان برابر داشتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رسم دلبر داشتن را آزمودم سالهاست</p></div>
<div class="m2"><p>باید از این آب و از این خاک دل بر داشتن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کافر عشقست آنکش پیش آن بت روست پاک</p></div>
<div class="m2"><p>از مسلمانی گذشتن عشق کافر داشتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سر آن خط مشوش را کسی داند که دید</p></div>
<div class="m2"><p>بایدش مجموع درس عشق از بر داشتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایدل ار بر گرد مردان حقیقت بین رسی</p></div>
<div class="m2"><p>دیده تحقیق را باید منور داشتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فتنه صورت مشو مرد خدا بین را توان</p></div>
<div class="m2"><p>دید رو از کام خشک و دیده تر داشتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ایکه چشمت از انانیت حصاری به ندید</p></div>
<div class="m2"><p>همتت ایمن نشست از حصن خیبر داشتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کی توان کندن پی تسخیر این نفس یهود</p></div>
<div class="m2"><p>این در خیبر مگر بازوی حیدر داشتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مظهر اسم الهی راز دار عقل کل</p></div>
<div class="m2"><p>آری آری اسم را رسمست مظهر داشتن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قصدمن زین اسم اعظم حرف ولفظوصوت نیست</p></div>
<div class="m2"><p>این مسمای معانی را مصور داشتن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بل بود اسمی که مشتقست ز اوصاف قدیم</p></div>
<div class="m2"><p>اسم مشتق چیست دانی وصف مصدر داشتن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیست فرق مظهر از ظاهر بحکم اتحاد</p></div>
<div class="m2"><p>میتوان دیدن ولی با راء/ی انور داشتن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که خواهد ایمنی از فتنه یاجوج نفس</p></div>
<div class="m2"><p>بایدش عقلی چنو سد سکندر داشتن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سد اسکندر چه باشد روی دارای وجود</p></div>
<div class="m2"><p>دیدن و آئینه جان را منور داشتن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کیست دارای وجود کامل کافی علیست</p></div>
<div class="m2"><p>کانچه گویم در حقش بایست باور داشتن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر که خود را پیش پای حضرت او داشت خاک</p></div>
<div class="m2"><p>ننگ دارد بالله از دیهیم قیصر داشتن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پای کو ننمود کف خویش از این رهگذر</p></div>
<div class="m2"><p>عار دارد استوا بر تخت سنجر داشتن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>راست ناید بر صحیفه هیکلی سطر وجود</p></div>
<div class="m2"><p>جز تنی مسطر رگی چون خط مسطر داشتن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در کتاب دل نظر کن نقش روی نفس کل</p></div>
<div class="m2"><p>تا توانی سر این صورت بمنظر داشتن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هر که بو جهلست گو بر معجزاتش بین که نیست</p></div>
<div class="m2"><p>کار سلمان چشم اعجاز از پیمبر داشتن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با خلوص و صدق شو تا بوذر و سلمان شوی</p></div>
<div class="m2"><p>سهل نبود قدر سلمان و ابوذر داشتن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در ظلال پرده قدرش ندیدن عرش راست</p></div>
<div class="m2"><p>عقل را در پرده غفلت مستر داشتن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پرده غفلت چه باشد از در انعام او</p></div>
<div class="m2"><p>چشم بستن چشم از این درب آن در داشتن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از طبیعی خواستن سر الهیات راست</p></div>
<div class="m2"><p>از مزاج کاسنی امید شکر داشتن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باوجود خم ز ساغر داشتن چشم شراب</p></div>
<div class="m2"><p>باوجود بحر روی خود بفرغر داشتن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فخر امکانیست بر سلطانی و سلطانی است</p></div>
<div class="m2"><p>بندگی بر درگه سلطان قنبر داشتن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای که اهل آذری بر آذر عشق آر روی</p></div>
<div class="m2"><p>بهر آذر خوی کن طبع سمندر داشتن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قدرت درویش او گر وقر بدهد کاه را</p></div>
<div class="m2"><p>میتواند کوه را چون کاه لاغر داشتن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر برو به روی بدهد تا نماید ضیغمی</p></div>
<div class="m2"><p>ننگ روباهست رو بر ضیغم نر داشتن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کوه را گو پیش حلم او ز دعوی رسته باش</p></div>
<div class="m2"><p>علم را در علم باید سنگ دیگر داشتن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برگزیدن این و آن را بر با و باشد شبیه</p></div>
<div class="m2"><p>بر شبه آوردن و همسنگ گوهر داشتن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کس شبه همسنگ گوهر کی کند پیش حکیم</p></div>
<div class="m2"><p>کی عرض را میتوان در حد جوهر داشتن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گر نباشد قطب گردون وجود اولیاء</p></div>
<div class="m2"><p>آسمان کی میتواند قطب و محور داشتن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اولیا را گر نباشد پادشاهی چون علی</p></div>
<div class="m2"><p>چون توانند از مه و خورشید چاکر داشتن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گلشن توحید را باید گل صدق و صفا</p></div>
<div class="m2"><p>تا تواند شیر را در بیشه مضطر داشتن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گل همان بهتر که بدهد بر غضنفر چشم زخم</p></div>
<div class="m2"><p>نی رخی تابنده چون چشم غضنفر داشتن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>این گل صدق و صفا در گلشن توحید ماست</p></div>
<div class="m2"><p>از خلوص جان باین روح مطهر داشتن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نقطه ئی از علم توحیدش دبیر چرخ پیر</p></div>
<div class="m2"><p>شرح نتواند دهد با هفت دفتر داشتن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هفت دفتر چیست این آن نقطه باشد کاندرو</p></div>
<div class="m2"><p>دائره ایجاد را یارند مضمر داشتن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>فاقد کل میتواند در ظلال عقل او</p></div>
<div class="m2"><p>نفس را در عین بی برگی توانگر داشتن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گفت احمر بین جنبیکم لکم اعدا عدو</p></div>
<div class="m2"><p>این کلام الله را نتوان محقر داشتن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>اژدر نفس ای برادر خفته بر بالای گنج</p></div>
<div class="m2"><p>گنج خواهی باید اول وضع اژدر داشتن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گنج پر گو گرد احمد اژدر آنرا پاسبان</p></div>
<div class="m2"><p>دفع اژدر کردن و گوگرد احمر داشتن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اهل نفسی از تو شش وادیست تا اخفای دوست</p></div>
<div class="m2"><p>خویش را چون مهره می نتوان بششدر داشتن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اسب تن پی ساز و با رفرف سواران شو رفیق</p></div>
<div class="m2"><p>طی نگردد این طریق از اسب و استر داشتن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جان من نه پای در باب تولای ولی</p></div>
<div class="m2"><p>تا توانی این عدو را در پس در داشتن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>این ولی عصر این اکسیر اعظم این امام</p></div>
<div class="m2"><p>خاک شو تا زر شوی این کشتن آن بر داشتن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نفس نتواند شمار خویش از خیل عقول</p></div>
<div class="m2"><p>جز که از همت ره انجامی مشمر داشتن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای ره انجام وجودت عقل در سیر صعود</p></div>
<div class="m2"><p>ناطقستی نفس ما بر عشق رهبر داشتن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از پی اثبات ذات خویشتن ذات ترا</p></div>
<div class="m2"><p>کرد ظاهر خواست حق برهان اظهر داشتن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خطبه خواندن مر خطیبان را بنام تست پای</p></div>
<div class="m2"><p>برفراز بام این نه پله منبر داشتن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>با فروغ آفتاب همتت ما را خطاست</p></div>
<div class="m2"><p>تکیه بر خورشید گردون مدور داشتن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عام کی داند ترا کش در نظر پایه ولیست</p></div>
<div class="m2"><p>دست بر عمرو و توانائی بعنتر داشتن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>از نصیری پرس سر این که گوید آن خدای</p></div>
<div class="m2"><p>عرش را یارد فرود و فرش را برداشتن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ای ولی الله اعظم صدر عرش کبریا</p></div>
<div class="m2"><p>گر ترا بیند ترا خواهد مصدر داشتن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ای وجودت حشرا کبر هر که حشر اندر تو یافت</p></div>
<div class="m2"><p>فارغست از انتظار حشر اکبر داشتن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ای قیامت فانی اندر قامتت با این قیام</p></div>
<div class="m2"><p>نیست قائم حجت حشر مکرر داشتن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ای بهشت عدن روحانی که اشعار صفا</p></div>
<div class="m2"><p>در مدیحت بر بهشت آموخت کوثر داشتن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>آسمانم باز با سلطان فقرا فکند کار</p></div>
<div class="m2"><p>بعد چندین سال با صد حشمت و فر داشتن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>حشمت من را درین دانست اینجا آمدن</p></div>
<div class="m2"><p>صدق آوردن دل و جان ثنا گر داشتن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>گفت نتوان گوهر توحید آوردن بدست</p></div>
<div class="m2"><p>جز دلی در بحر عرفان آشناور داشتن</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>از شهود و غیب مطلق در مظاف آن و این</p></div>
<div class="m2"><p>با وجود کون جامع مخزن زر داشتن</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مخزن زر داشتن نبود بود در خاک شور</p></div>
<div class="m2"><p>از عطش جان دادن و دریای اخضر داشتن</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ای ظفرهای ترا در پنج حضرت امتداد</p></div>
<div class="m2"><p>میتوان ما را بامدادی مظفر داشتن</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ایکه خواهی شعر گفتن شعر ناگفتن بخواه</p></div>
<div class="m2"><p>شعر گفتن نیست چون رزق مقدر داشتن</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کسب کردن باید و دانستن سر علوم</p></div>
<div class="m2"><p>صحو معلوم و قوانین مقرر داشتن</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>تا توان گفتن دو بیتی را که گر بیند خبیر</p></div>
<div class="m2"><p>صورت او را تواند سر مخبر داشتن</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>نه دو زحف از چار بحر آوردن و از ابلهی</p></div>
<div class="m2"><p>ابتری گفتن ملقب احذ مضمر داشتن</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گر نداند نیز نام اخذ و قبض آنهم رواست</p></div>
<div class="m2"><p>نقطه را ناخوانده لاف خط پر گر داشتن</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>تا سپهر لاجوردی رنگ با کف الخضیب</p></div>
<div class="m2"><p>ثابتست اندر سر تیر دو پیگر داشتن</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>جسم احباب تو چونان صورت سعدالسعود</p></div>
<div class="m2"><p>باد تقویمش بفال سعد اکبر داشتن</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>فرق اعدایت بزیر تیغ مریخ ار توان</p></div>
<div class="m2"><p>نحس اکبر را نشیب نحس اصغر داشتن</p></div></div>