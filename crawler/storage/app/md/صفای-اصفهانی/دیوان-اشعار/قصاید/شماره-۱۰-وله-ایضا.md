---
title: >-
    شمارهٔ  ۱۰ - وله ایضا
---
# شمارهٔ  ۱۰ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>بگل سوری ماند رخ آن ترک پسر</p></div>
<div class="m2"><p>که سپارند بدو غالیه لاله سپر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپر لاله کند غالیه آن ترک و خطاست</p></div>
<div class="m2"><p>من ندیدستم از غالیه بر لاله سپر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گونه اش خرمنی از لاله خود روی بزیر</p></div>
<div class="m2"><p>طره اش دامنی از نافه آهو بزبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنبل از مشک سیه کاشته بر سیم سپید</p></div>
<div class="m2"><p>نرگس از جزع یمان ریخته بر لالهتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو سیه خال دو هندوبچه ماه سوار</p></div>
<div class="m2"><p>دو سر زلف دو جراره بیژاده شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه را زلف گرهگیر دلارام و مراست</p></div>
<div class="m2"><p>بر دل و بر جان از زلف دلارام خطر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه ب آب افتد و در آتش و در آتش و آب</p></div>
<div class="m2"><p>نرود تا نرود جان بقفا دل باثر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خم زلف و قد بر رفته بچوگان و بتیر</p></div>
<div class="m2"><p>لب لعل و زنخ ساده بیاقوت و گهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب او دارد آمیخته با شکر و شیر</p></div>
<div class="m2"><p>وین شگفتست که نگذاردش از شیر شکر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمری دارد چو نموی و از انموی غمیست</p></div>
<div class="m2"><p>بر دلم بار که کوه افتد از آنغم ز کمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهنی دارد چون ذره و در سینه مراست</p></div>
<div class="m2"><p>دل تنگی که ازان ذره خورد خون جگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه گویند بخورشید همی ماند و من</p></div>
<div class="m2"><p>در شگفت از نظر مردم کوتاه نظر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کی شنیدستی خورشید که از زلف سیاه</p></div>
<div class="m2"><p>بنهد بر سر گلبرگ طری مشک تتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا بیفروزد از شاخ شجر آتش طور</p></div>
<div class="m2"><p>رخ و قد آتش افروخته و شاخ شجر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باز گویند بمه ماند و زین گفت پریش</p></div>
<div class="m2"><p>من خورم چون شکن طره او یک بدگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ماه کی دیدی چنبر نهد از قیر بشیر</p></div>
<div class="m2"><p>ماه کی دیدی افسر زند از مشک بسر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا چو ترک من سرگرم شود از می ناب</p></div>
<div class="m2"><p>خواند از گفته من نغمه توحید از بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوید ای ذات تو سر صفت و فعل و اثر</p></div>
<div class="m2"><p>ای هیولای تو آراسته کل صور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای جناب جبروتی که بناسوتی و باز</p></div>
<div class="m2"><p>از تو در بر ملکوتست و بلاهوت اثر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ذات بیرنگی و هر رنگ که هست از تو پدید</p></div>
<div class="m2"><p>شخص یکتائی و هر جمع که هست از تو سمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر تو پنهان شوی این کون و مکان هست عیان</p></div>
<div class="m2"><p>چون تو پیدا شوی از کون و مکان نیست خبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حضرت جامع ذات احد و عین کثیر</p></div>
<div class="m2"><p>سر علم تو قضا صورت علم تو قدر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ظاهر و باطن باطن همه عقل و دل پاک</p></div>
<div class="m2"><p>پدر و مادر این نه صدف و چار گهر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عرش انعام تو هر سینه که در اوست فؤاد</p></div>
<div class="m2"><p>فرش اقدام تو هر دیده که در اوست بصر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسکه نزدیکی پنهانی و این نیست شگفت</p></div>
<div class="m2"><p>که بنزدیک بصر می ننماید مبصر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همچو ماهی که ب آبستی جوینده آب</p></div>
<div class="m2"><p>یا سمندر که ب آذر بنداند آذر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو همانشخصی کت ملک و ملک ظل دوپای</p></div>
<div class="m2"><p>تو همان بازی کت کون و مکان زیر دو پر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو همان شاهی کت عقل و هیولای وجود</p></div>
<div class="m2"><p>دو غلامند زهی زین دو مبارک جوهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اکتناه تو بود بیرون از درک ملک</p></div>
<div class="m2"><p>انکشاف تو بود بالا از عقل بشر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بشر آنجا که توئی گر رسد از خویش رود</p></div>
<div class="m2"><p>آری از خویش رود پشه چو آید صرصر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هست لاهوت ترا پای بفرق جبروت</p></div>
<div class="m2"><p>که محیطست باسمای تو تاپای ز سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حضرت جمع وجودی که مفاهیم صفات</p></div>
<div class="m2"><p>هست در او همه ممتاز چو عود از عنبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>واحد اول اقلیم ازل ملک اله</p></div>
<div class="m2"><p>که بشر راست درو راه اگر کرد سفر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سفر ثانی در سیر من الله الیه</p></div>
<div class="m2"><p>که ولایت را تکمیل صعودست و سیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سیر سالک همه در اسم صفت باشد و ذات</p></div>
<div class="m2"><p>غیر آن اسم که بر ذات بود مستاء/ثر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اسم مستاء/ثر ذاتی که بجز ذات خدای</p></div>
<div class="m2"><p>نبرد راه کسی گر چه بود پیغمبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زین فرا ترا حدیت که تجلیست بذات</p></div>
<div class="m2"><p>ذاتر اللذات این جای وجوبست و حذر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حذر ای عارف از نفس خدا گفت خدا</p></div>
<div class="m2"><p>در نبی عقل نبی یافت بدین نکته ظفر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ظفر از عقل نبی بود و کمالات ولی</p></div>
<div class="m2"><p>که بمجهول کسی راه نیابد بفکر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رفرف خواجه درین سیر شود بی پرواز</p></div>
<div class="m2"><p>کشتی نوح درین بحر شود بی لنگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آن هویت که بود ساری در غیب و شهود</p></div>
<div class="m2"><p>برتر از اینهمه آنی تو و از هر دو بدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم برون از دل و هم در دل اصحاب قلوب</p></div>
<div class="m2"><p>هم نهان از سر و هم در سر ارباب هنر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر چه هستی تو و بالذات از اینجمله بری</p></div>
<div class="m2"><p>غیر در پرده نهانست و تو از پرده بدر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همه نقش رخ زیبای تو از غیب و شهود</p></div>
<div class="m2"><p>خودتوئی نقش چه ایفرد برون از حد و مر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خلو داری تو بذات از همه ای کرده بذات</p></div>
<div class="m2"><p>کسوت کثرت از غایت توحید ببر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ظاهری در همه ای باطن این چار ایوان</p></div>
<div class="m2"><p>باطنی از همه ای ظاهر این نه منظر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>باطنی در چه ز بس ظاهر در عین ظهور</p></div>
<div class="m2"><p>ظاهری برکه که هم ظاهری و هم مظهر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خودتوئی غیر تو در دیده من نقش براب</p></div>
<div class="m2"><p>غیر ذات توهبا غیر صفات تو هدر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیست جز عارف توحید و تو زیبنده تاج</p></div>
<div class="m2"><p>نیست جز بنده سر تو سزاوار کمر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سر درویش ترا تاج لقد کرمناست</p></div>
<div class="m2"><p>که نهد پایش بر تارک خورشید افسر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رسته از پست و ز بالاست بلی مرد خداست</p></div>
<div class="m2"><p>کز جهت جسته به بی سو نه فرو دست و نه بر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پسر آدم خاکی و نه خاکست و نه باد</p></div>
<div class="m2"><p>نیست از آب و برونست ز حد آذر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>لامکانست و مکان چون عرض او جوهر پاک</p></div>
<div class="m2"><p>آسمانست و زمین چون شجر و اوست ثمر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نوبر هستی هستی همه یکباغ کهن</p></div>
<div class="m2"><p>پسر انسان آن باغ کهن را نوبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در زمین نیست ولی هست زمین را مبنی</p></div>
<div class="m2"><p>در سما نیست ولی هست سما را محور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در زمان نیست ولی هست زمان را دائر</p></div>
<div class="m2"><p>در مکان نیست ولی هست مکان را داور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>داور امکان مجموعه ملک و ملکوت</p></div>
<div class="m2"><p>که بلاهوت مقامستش و ناسوت مقر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نه ببحرست ولی حکمش جاریست ببحر</p></div>
<div class="m2"><p>نه ببرست ولی امرش ساریست ببر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه بتلوینش تمکن نه به تمکینش مقام</p></div>
<div class="m2"><p>شمر و دریا آزاده نه دریا نه شمر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پسر آدم نفس فلک و عقل ملک</p></div>
<div class="m2"><p>هر دو پستند و بود بالا این طرفه پسر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>پسر احمد شاهنشه اقلیم وجود</p></div>
<div class="m2"><p>که بود خسرو اسماء الهی لشکر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کارفرمای قضا حضرت انسان که بذات</p></div>
<div class="m2"><p>هست او اکبر و انسان کبیرست اصغر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ولی مرشد سلطان صفا قبله کل</p></div>
<div class="m2"><p>شمس هشتم که بود ذات نخستش خاور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>قطب عالم شه جان مرشد توحید رضا</p></div>
<div class="m2"><p>که سلاطین را باشد بطریقت رهبر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در تک ذره شمسش سپر افکنده براب</p></div>
<div class="m2"><p>آفتاب فلک از عجز چنو نیلوفر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سک او در هنر اردست دهد با روباه</p></div>
<div class="m2"><p>روبه ماده شکست آرد بر ضیغم نر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر کجا ذره او در سر شیدست دوار</p></div>
<div class="m2"><p>هر کجا روبه او در دل شیرست خطر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نظر لطفش بر خاک فرو بارد جان</p></div>
<div class="m2"><p>فره قهرش از چرخ فرو آرد فر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ای کماندار کمان ازل و قوس ابد</p></div>
<div class="m2"><p>قسی نه فلک از قوس کمال تو وتر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وتر قوس تو حاوی به محدد ز عظم</p></div>
<div class="m2"><p>صبی شیر تو بر عقل معلم ز کبر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>صعوه شیر تو همبازی باز ملکوت</p></div>
<div class="m2"><p>بنده سفل تو همبازی نیروی قدر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>پیشگاه تو قوی مایه تر از ملک مثال</p></div>
<div class="m2"><p>حشم و مملکتش بی عدد و پهناور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بر خلیل تو از آن فیض مقدس که تر است</p></div>
<div class="m2"><p>از دل آتش سوزنده دمد سیسنبر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>پیشتر ز آنکه تو بر تخت شهی پای نهی</p></div>
<div class="m2"><p>سر بخاک تو نهاد از عظمت اسکندر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گر نیاورد ز ظلمات بدست آب حیات</p></div>
<div class="m2"><p>کف خاک تواش آورد ز ظلمات بدر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>میزبانی تو و من بی خبر از راه دراز</p></div>
<div class="m2"><p>میهمان آمده تو پادشه و من مضطر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از جبالیکه بدی ریخته چون نیش گراز</p></div>
<div class="m2"><p>در هوائی که بدی تفته چو کام اژدر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ریگهایش همه فتاک چو حد پیکان</p></div>
<div class="m2"><p>خارهایش همه سفاک چو نیش نشتر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>غیر ذی ذرع بیابانی منزلگه دیو</p></div>
<div class="m2"><p>بی سر و بی بن صحرائی آبشخور شر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بامیدی که مگر از طرق فقر و فنا</p></div>
<div class="m2"><p>ز غنا و ز بقای تو کنم آبشخور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>آب حیوان دهم و زنده کنم هیکل خاک</p></div>
<div class="m2"><p>کسوت روح بپوشم بتن خاکستر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>سر آن وحدت اطلاقی کز قید بریست</p></div>
<div class="m2"><p>فاش گویم که یکی هست و جزین نیست مفر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مظهر او توئی ای مظهر و ظاهر همه او</p></div>
<div class="m2"><p>غیر او نیست اگر هست قل الله فذر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ظاهرت را پی تولید نمودند قیام</p></div>
<div class="m2"><p>هفت علوی پدر و چار خشیجی مادر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>باطنت ای تو بباطن پسر سر ظهور</p></div>
<div class="m2"><p>مادر وحدت ذاتست و بنه عقل پدر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ای سحاب کرم و جود بگردون وجود</p></div>
<div class="m2"><p>از یم رحمت برکشت صفاریز مطر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>تن زنم من تو تجلی کن تا جلوه کند</p></div>
<div class="m2"><p>سر توحید چو خورشید سما وقت سحر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بهمه خلق تو بنمای رخ و قامت یار</p></div>
<div class="m2"><p>وانسر زلف که هست از دل و از جان بهتر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>زینهار ای پسر سر من این نغز نشید</p></div>
<div class="m2"><p>بمخوان جز ببر معتقد دانشور</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بمگو سر مرا جز بر جویای خدا</p></div>
<div class="m2"><p>که تو در خوابی و سیر این اثر جوع و سهر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>که تو در پست همی غلتی و این نکته بلند</p></div>
<div class="m2"><p>که تو با پای همی پوئی و این جلوه بپر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>که تو وابسته عاداتی و ما رسته ز قید</p></div>
<div class="m2"><p>ما بسر منزل فقریم و تو در کبر و بطر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>یا نبی ارکب معنی بود این کشتی نوح</p></div>
<div class="m2"><p>تا کنی بر قدم نوح ازین بحر گذر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>پسر نوح نئی تکیه مکن بر فن خویش</p></div>
<div class="m2"><p>تا نمانی بدل مشرک و جان کافر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بصفا بنگر و اسرار معارف بنیوش</p></div>
<div class="m2"><p>گر نه از باصره ئی اعمی وز سامعه کر</p></div></div>