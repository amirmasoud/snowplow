---
title: >-
    شمارهٔ  ۱ - قصیده
---
# شمارهٔ  ۱ - قصیده

<div class="b" id="bn1"><div class="m1"><p>دیماه دم سپیده و سرما</p></div>
<div class="m2"><p>ای ترک بیار آتش مینا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن آتش مشگبوی کن روشن</p></div>
<div class="m2"><p>تا دیده عقل را کنی بینا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن شعله همچو لاله مینو</p></div>
<div class="m2"><p>افروز بزیر حقه مینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شنگرف بسای در دل مشکو</p></div>
<div class="m2"><p>سیماب زدند کوه را سیما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردست بگیر ساتکین می</p></div>
<div class="m2"><p>بنمای چو موسی آن ید بیضا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خلق برند پی بدین برهان</p></div>
<div class="m2"><p>بر آتش طور و سینه سینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گرمی می برودت بهمن</p></div>
<div class="m2"><p>تبدیل کنند مردم دانا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گردش ساغری چو ماه نو</p></div>
<div class="m2"><p>از پنجه ترکی آفتاب آسا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو نشاهدمن که گونه خورشید</p></div>
<div class="m2"><p>میتابد و نیست چو نرخش رخشا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بت روی منا تو شاهد چینی</p></div>
<div class="m2"><p>یا شاه بتان خلخ و یغما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای گونه لعل و خط زنگارت</p></div>
<div class="m2"><p>جام سحر و صحیفه خضرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با قد تو قد سرو ناموزون</p></div>
<div class="m2"><p>باروی تو روی ماه نازیبا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قد تو که آفتاب گردونی</p></div>
<div class="m2"><p>چون تیر بود بترکش جوزا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیریکه مهش کمان بود پنهان</p></div>
<div class="m2"><p>مشتاب چو تیر تا شود پیدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گویند که مشک تر همی زاید</p></div>
<div class="m2"><p>در تاتر و ناف آهوی صحرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مشکوی مراست تا تری ترکی</p></div>
<div class="m2"><p>کش مشگ دمد ز لاله حمرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشگفت که مشگ سوزد از آتش</p></div>
<div class="m2"><p>میسوزم من ب آتش سودا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کت مشک سیاه می نیفروزد</p></div>
<div class="m2"><p>با آنکه ب آتش افکنی عمدا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اندام و دلت بنرمی و سختی</p></div>
<div class="m2"><p>دارند چنو که خاره و خارا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در سینه یکی حکایت از سندان</p></div>
<div class="m2"><p>در جامه یکی شکایت از دیبا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر سرو دمیده بینمت سنبل</p></div>
<div class="m2"><p>برسیم سپید عنبر سارا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در عنبر تست لاله نعمان</p></div>
<div class="m2"><p>در سنبل تست عبهر شهلا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با آنکه نشسته در دل و جانی</p></div>
<div class="m2"><p>ای جان و دلم بدیدنت دروا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عشق تو نشسته بر سر برزن</p></div>
<div class="m2"><p>وصل تو نهفته در پر عنقا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من دست زنم درین فرا پستی</p></div>
<div class="m2"><p>بر دامن شاه عالم بالا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از بحر نخست گوهر هشتم</p></div>
<div class="m2"><p>دریای چهار لؤلؤی لالا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گردون چهار اختر خاتم</p></div>
<div class="m2"><p>کاین چار چو گوهرند و او دریا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دارای نه آسمان تو در تو</p></div>
<div class="m2"><p>دارنده هفت ارض تا بر تا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سلطان سمای روح وارض تن</p></div>
<div class="m2"><p>قیوم چهار ام و هفت آبا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>او جان و جمیع ما سوی پیکر</p></div>
<div class="m2"><p>او کل و تمام کائنات اجزا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگذشته از آن که علم الانسان</p></div>
<div class="m2"><p>مالم یعلم ستایمش زان سا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اسمای خدا بذات او قائم</p></div>
<div class="m2"><p>قیوم و قدیر و حی و بی همتا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دائر بوی است بی وی این اوصاف</p></div>
<div class="m2"><p>قائم بوی است بی وی این اسما</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>او نیست خداست قل هوالواحد</p></div>
<div class="m2"><p>کاین پست ز خویش رست و شد والا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وارست ز طبع و نفس و عقل و جان</p></div>
<div class="m2"><p>بر سر خفا رسید بل اخفی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>انسان شد و این خزانه عرشی</p></div>
<div class="m2"><p>الاست چو یافت نقد گنج لا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از خویش و ز غیر خویش شد فانی</p></div>
<div class="m2"><p>باقیست باو فلیس هو الا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سلطان گه ولایت مطلق</p></div>
<div class="m2"><p>کو هست مدیر کن فکان تنها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میر ملکوتیان روشن دل</p></div>
<div class="m2"><p>پیر جبروتیان جان پیرا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مجموع وجود پیشگاهستش</p></div>
<div class="m2"><p>من ملک ندیده ام بدین پهنا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>او شخص وجود و هیکل موجود</p></div>
<div class="m2"><p>عرش و فلک و ملک همه اعضا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در نقطه خاک مرکز هستی</p></div>
<div class="m2"><p>پیدا شد و شد چو نقطه پا برجا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نه دائره سپهر از آن دائم</p></div>
<div class="m2"><p>گردند بگرد مرکز غبرا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن نقطه رضاست کز سر کلکش</p></div>
<div class="m2"><p>بر لوح قضا قدر کند انشا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>افشا کند از قضای اجمالی</p></div>
<div class="m2"><p>سر قدر از قضا شود افشا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>احیا کند این نفوس انسانی</p></div>
<div class="m2"><p>انفاس شه از اماته احیا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر نفس باختیار مرد از خود</p></div>
<div class="m2"><p>در پای ولی ولیش کرد احیا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این قلعه کفر را کند بنیان</p></div>
<div class="m2"><p>وز شهر الوهی آورد بنا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خشت آوردش ز قالب وحدت</p></div>
<div class="m2"><p>سنگ آوردش ز کوه استغنا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>آبش همه آب صفوت آدم</p></div>
<div class="m2"><p>خاکش همه خاک طینت یحیی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سقف و در و بام او ز هم ریزد</p></div>
<div class="m2"><p>سقف و در و بام تازه آرد تا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شهریش کند مکان کروبی</p></div>
<div class="m2"><p>خلوتگه یار و خالی از اعدا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مانند خلیل خانه ئی سازد</p></div>
<div class="m2"><p>سر حلقه دین و قبله دنیا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>آراسته تر ز نیر اعظم</p></div>
<div class="m2"><p>پیراسته تر ز ذروه اعلی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای پادشهی که هست درویشت</p></div>
<div class="m2"><p>دارای گه سکندر و دارا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زان سنگ که سود سم شهپایت</p></div>
<div class="m2"><p>ترصیع کنند افسر کسری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>معلول نخست بود عقل کل</p></div>
<div class="m2"><p>از خاک در تو کرد استشفا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نفس اول بود طفل ابجد خوان</p></div>
<div class="m2"><p>عشق تو معارفش نمود القا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همواره تمام حظ لاهوتی</p></div>
<div class="m2"><p>از دفتر عشق کرد استیفا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فرمان وجود تست گر هستی</p></div>
<div class="m2"><p>سلطان شهود باشدش طغرا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>سر عطسه آدم صفی عیسی</p></div>
<div class="m2"><p>سر خیل مجردان تن فرسا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>واماند ز بندگان اسرارت</p></div>
<div class="m2"><p>در سیر ببند چادر ترسا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خورشید ترا سماست قیومی</p></div>
<div class="m2"><p>عیسی است بر آستانه خورشا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گر خاتم خاص احمدش خواند</p></div>
<div class="m2"><p>بترائی از فطانت بترا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فرقست میان عیسی و احمد</p></div>
<div class="m2"><p>وز اشیا تا بمنتهی الاشیا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بر زمره اولیای ختمیین</p></div>
<div class="m2"><p>تو خاتم و هفت باب و چار ابنا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بر پیکر جمله خلعت لولاک</p></div>
<div class="m2"><p>بر تارک جمله تاج کرمنا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بر دست جمیع از ابد ساغر</p></div>
<div class="m2"><p>در ساغر جمله از ازل صهبا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بالای تمام هیکل وحدت</p></div>
<div class="m2"><p>کز صرف وجودشان بود بالا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>این چارده نور پاک یک نورند</p></div>
<div class="m2"><p>از مختمشان گرفته تا مبدا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>در وحدت عین آخرست اول</p></div>
<div class="m2"><p>از چشم صفا ببین که بینی ها</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از دیده دوست میتوان دیدن</p></div>
<div class="m2"><p>حسن رخ دوست بی من و بی ما</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>دجال نبرد راه بر مهدی</p></div>
<div class="m2"><p>خورشید ندید کور مادرزا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>جان سالک و راه دور و شب تاریک</p></div>
<div class="m2"><p>ای برق بجه ز جانب صنعا</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تا بگذرد این تجلی برقی</p></div>
<div class="m2"><p>سالک بسر سلوک بنهد پا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ای شمس حقیقت رضا سر زن</p></div>
<div class="m2"><p>از غرب سمای سر جابلسا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کز نور تو آفتاب جان گردد</p></div>
<div class="m2"><p>ذرات زمین جسم جابلقا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>این ساخته سرمه صفاهانی</p></div>
<div class="m2"><p>ای عقل بکش بدیده حورا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تا دیده حور آشنا گردد</p></div>
<div class="m2"><p>با خاک قدوم عروه الوثقی</p></div></div>