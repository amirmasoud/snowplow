---
title: >-
    ترکیب بند من واردات القلبیه فی معرفه الالهیه
---
# ترکیب بند من واردات القلبیه فی معرفه الالهیه

<div class="b" id="bn1"><div class="m1"><p>ای موسی طور قلب آگاه</p></div>
<div class="m2"><p>لاتحزن اننی اناالله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماراست طفیل ظل خورشید</p></div>
<div class="m2"><p>بالاتر از آفتاب تا ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک و ملکوتمان مشابه</p></div>
<div class="m2"><p>با آن که منزهیم ز اشباه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مجمع این دو بحر در سیر</p></div>
<div class="m2"><p>با موسی و خضر هر دو همراه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالاتر ازین دو قطب گردون</p></div>
<div class="m2"><p>گردون مقربان درگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن سوتر از این مهابط سر</p></div>
<div class="m2"><p>سریست که غیر نیست آگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما روشن و آفتاب تاریک</p></div>
<div class="m2"><p>ما مرتفع و ستاره کوتاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان مطلع اننی اناالحق</p></div>
<div class="m2"><p>دل مرجع لااله الاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با ضیغم غاب غوث اعظم</p></div>
<div class="m2"><p>شیر فلک البروج روباه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورشید به نور ماست روشن</p></div>
<div class="m2"><p>از گاه سپیده تا شبانگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما خسرو لامکان توحید</p></div>
<div class="m2"><p>خورشید سوار عرش خرگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در مزرع خاکسار عشقست</p></div>
<div class="m2"><p>نه خرمن آسمان کم از کاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما بنده پادشاه فقریم</p></div>
<div class="m2"><p>با این همه عز و رتبه و جاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برقیم به خرمن بداندیش</p></div>
<div class="m2"><p>ابریم به مزرع نکوخواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عبدیم و به فقر شاه مطلق</p></div>
<div class="m2"><p>شاهیم به عشق عبد اواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاهیم که هست پای درویش</p></div>
<div class="m2"><p>در فقر طراز افسر شاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عبدیم که از صفای بر حق</p></div>
<div class="m2"><p>آموخته ایم راه از چاه</p></div></div>
<div class="b2" id="bn18"><p>تا راه بریم بر دقایق</p>
<p>در حل حقیقه الحقایق</p></div>
<div class="b" id="bn19"><div class="m1"><p>سلطان سریر عشق ماییم</p></div>
<div class="m2"><p>هم پادشهیم و هم گداییم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر خسرو گاه افسر سر</p></div>
<div class="m2"><p>بر سالک راه خاک پاییم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر دست سکندر ولایت</p></div>
<div class="m2"><p>آیینه قطب حق نماییم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ما مالک ملک و گنج فقریم</p></div>
<div class="m2"><p>ما صاحب افسر فناییم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دریای وجود را ل آلی</p></div>
<div class="m2"><p>در بحر عدم نهنگ لاییم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با وحدت دل به نفی کثرت</p></div>
<div class="m2"><p>شمشیر نه تیر نه بلاییم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در فلک نجات ناخدا کیست</p></div>
<div class="m2"><p>ما بر سر ناخدا خداییم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در کشتی دل ببحر توحید</p></div>
<div class="m2"><p>بر صدر نشسته ناخداییم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ما بنده مصطفای مطلق</p></div>
<div class="m2"><p>سلطان سریر اصطفاییم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر جسم شکسته مومیایی</p></div>
<div class="m2"><p>در چشم ضریر توتیاییم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عشقست که ماورای عقلست</p></div>
<div class="m2"><p>ما نیز ورای ماوراییم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل خانه و خلوت خداوند</p></div>
<div class="m2"><p>ما خواجه خلوت و سراییم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از یک سر موی گر فروشند</p></div>
<div class="m2"><p>مجموع دو کون را بهاییم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از کسوت کائنات عوریم</p></div>
<div class="m2"><p>پوشیده ردای کبریاییم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در دیده ما بجز خدا نیست</p></div>
<div class="m2"><p>آسوده ز قید ماسواییم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جمشید جمال را سریریم</p></div>
<div class="m2"><p>خورشید کمال را سماییم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیشیم ز آسمان بمعنی</p></div>
<div class="m2"><p>باانکه به صورت از قفاییم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بالاتر نه بنای بالا</p></div>
<div class="m2"><p>با آنکه فروتر بناییم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>طی ظلمات کرده ایدون</p></div>
<div class="m2"><p>خضر سرچشمه بقاییم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دارای وجود را سراپا</p></div>
<div class="m2"><p>بالای شهود را قباییم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بیگانه ز غیر و غیر چون نیست</p></div>
<div class="m2"><p>با هرچه که هست آشناییم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>میخواره و رند و خانه بر دوش</p></div>
<div class="m2"><p>بی کینه و کبر و بی ریاییم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>صافی شده از کدورت سر</p></div>
<div class="m2"><p>صاحبدل صفه صفاییم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن همزه که اوست فوق واحد</p></div>
<div class="m2"><p>آن نقطه که هست تحت باییم</p></div></div>
<div class="b2" id="bn43"><p>ما یافته ایم در معارف</p>
<p>این نقطه بنفی ذات عارف</p></div>
<div class="b" id="bn44"><div class="m1"><p>افراد که همدم جلیلند</p></div>
<div class="m2"><p>پیران مراد را دلیلند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هم صاحب نفخه سرافیل</p></div>
<div class="m2"><p>هم محرم راز جبرییلند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بر گوهر جود بحر عمان</p></div>
<div class="m2"><p>بر کشت وجود رود نیلند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از گوهر پاک گنج پنهان</p></div>
<div class="m2"><p>از مشرب صاف سلسبیلند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خارج همه از اداره قطب</p></div>
<div class="m2"><p>با قطب برادر سبیلند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هم مالک ملکت سلیمان</p></div>
<div class="m2"><p>هم صاحب ثروت خلیلند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دارند بحق هزار برهان</p></div>
<div class="m2"><p>خاموش ولی ز قال و قیلند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در مملکت وجود باقی</p></div>
<div class="m2"><p>بعد از اقطاب بی بدیلند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در مصر ولایتند والی</p></div>
<div class="m2"><p>یوسف رخ و دلبر و جمیلند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اکسیر سعادتمند افراد</p></div>
<div class="m2"><p>پرقیمت و قابل و قلیلند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>از خلق نه از عروق واعصاب</p></div>
<div class="m2"><p>بر خاتم انبیا سلیلند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>داود زبور خوان توحید</p></div>
<div class="m2"><p>با کوه به نغمه هم رسیلند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنانکه لباس جاه پوشند</p></div>
<div class="m2"><p>در فقر برهنه و ذلیلند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بینند حجاره های سجیل</p></div>
<div class="m2"><p>کاین قوم ضلال قوم پیلند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نابرده به کعبه فنا پی</p></div>
<div class="m2"><p>بر نفی بقای خود دخیلند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آن فرقه که زنده اند دایم</p></div>
<div class="m2"><p>در مسلخ عشق او قتیلند</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خلاق معانیند و صورت</p></div>
<div class="m2"><p>امرند که خلق را کفیلند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>قوت دل اولیاست تهلیل</p></div>
<div class="m2"><p>با خاتم انبیا اکیلند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر مسند حق خلیفه الله</p></div>
<div class="m2"><p>غوثند و خدای را وکیلند</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از اسم گذشته در یم ذات</p></div>
<div class="m2"><p>مستغرق بلکه مستحیلند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ایجاد عیال جود افراد</p></div>
<div class="m2"><p>هم لم یلدند و هم معیلند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بحرند که حاوی ل آلی</p></div>
<div class="m2"><p>ابرند که راوی غلیلند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هستیست ز وجودشان و ایشان</p></div>
<div class="m2"><p>در معرض امتحان بخیلند</p></div></div>
<div class="b2" id="bn67"><p>قومی همه رند و لاابالی</p>
<p>بیرون ز تصور خیالی</p></div>
<div class="b" id="bn68"><div class="m1"><p>ما گاه فراز آفتابیم</p></div>
<div class="m2"><p>گه معتکلف تراب و آبیم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گاهی شه کون و گاه درویش</p></div>
<div class="m2"><p>آباد گهی و گه خرابیم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گه تیره و گاه صاف بی غش</p></div>
<div class="m2"><p>گه دردی و گه ناب نابیم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر سایه ما ز نور گوید</p></div>
<div class="m2"><p>بنیوش که ظل آفتابیم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خود گوی ز ما متاب گردن</p></div>
<div class="m2"><p>ما خسرو مالک الرقابیم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>با آب وصال دوست شاداب</p></div>
<div class="m2"><p>با آتش عشق او کبابیم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آبی که ز سر گذشت دریاست</p></div>
<div class="m2"><p>ما تشنه مانده در سرابیم</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ما خفته میان بحر عطشان</p></div>
<div class="m2"><p>وین طرفه که تشنه ایم و خوابیم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>موجود بجز خدای نبود</p></div>
<div class="m2"><p>ما مانده ز خویش در حجابیم</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>یک حرف وفا نخوانده با آنک</p></div>
<div class="m2"><p>دیباچه نغز نه کتابیم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از ام و ابیم زاده اما</p></div>
<div class="m2"><p>ما جد قدیم ام و بابیم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>سر صحف دلیم لیکن</p></div>
<div class="m2"><p>معلوم نشد که از چه بابیم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در دست حبیب عروه الله</p></div>
<div class="m2"><p>مر گردن خصم را طنابیم</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بر دوست خط کتاب رحمت</p></div>
<div class="m2"><p>بر دشمن آیت عذابیم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>پیر پدر ستاره پیر</p></div>
<div class="m2"><p>در اول نوبت شبابیم</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ما خسرو اعظمیم و درویش</p></div>
<div class="m2"><p>ما شیخ مکرمیم و شابیم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>خورشید تکاورست ما را</p></div>
<div class="m2"><p>با عیسی چرخ همرکابیم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شاهست که عارفست و معروف</p></div>
<div class="m2"><p>ما بنده معرفت م آبیم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>خمار و شرابخوار و ساقی</p></div>
<div class="m2"><p>خمخانه و ساغر و شرابیم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>بر چرخ رویم بی تحرک</p></div>
<div class="m2"><p>هم سیر دعای مستجابیم</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>در رزم هوای نفس چون گرگ</p></div>
<div class="m2"><p>با پنجه شیر شرزه غابیم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>دنییست چو جیفه گر پرستیم</p></div>
<div class="m2"><p>این جیفه بسیرت کلابیم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>کم جوی سفال و سنگ دنیی</p></div>
<div class="m2"><p>ما گوهر گنج دیریابیم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>مقهور حضور و نور انوار</p></div>
<div class="m2"><p>وارسته ز ظلمت غیابیم</p></div></div>
<div class="b2" id="bn92"><p>با جسم بکعبه حضوریم</p>
<p>در ظلمت محض عین نوریم</p></div>
<div class="b" id="bn93"><div class="m1"><p>ای راز مرا طلیعه ناز</p></div>
<div class="m2"><p>بگشای در دریچه راز</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ناز تو بلای نازنینان</p></div>
<div class="m2"><p>کشتی همه را چه میکنی ناز</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>بردی دل ما به شوخی و طنز</p></div>
<div class="m2"><p>ای دلبر نغز و شوخ طناز</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>بگشای در خزانه عرش</p></div>
<div class="m2"><p>زین درج دررکه میکنی باز</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ای مطرب عشق کن بتوحید</p></div>
<div class="m2"><p>در پرده ترانه دگر ساز</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>این توسن وحدت تو تازان</p></div>
<div class="m2"><p>در عرصه انتها و آغاز</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بر وحدت آفتاب ذاتت</p></div>
<div class="m2"><p>ذرات وجود من هم آواز</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>باز دلت از زمین آثار</p></div>
<div class="m2"><p>دارد بسمای ذات پرواز</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>تا بال گشوده یی بدین فر</p></div>
<div class="m2"><p>بر ساعد شه ندیده کس باز</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ای ذات ولی امر مطلق</p></div>
<div class="m2"><p>ای از همه کائنات ممتاز</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ای قطب مکان لامکان سیر</p></div>
<div class="m2"><p>خورشید سوار آسمان تاز</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>در مملکت کمال سرمد</p></div>
<div class="m2"><p>شاهی تو و بی شریک و انباز</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>عشق تو شراره ییست جانسوز</p></div>
<div class="m2"><p>جویای تو عاشقیست جانباز</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>سر دل بایزید و منصور</p></div>
<div class="m2"><p>سودای سر جنید و خراز</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>در عشق نشان شدیم و جز اشک</p></div>
<div class="m2"><p>در خانه ما نبود غماز</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>از آن لب لعل کی کند دل</p></div>
<div class="m2"><p>دندان من ار کنند با گاز</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ای مطرب دل ز تار وحدت</p></div>
<div class="m2"><p>زنگ دل ما بزخمه پرداز</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ای ساقی جان بساغر افکن</p></div>
<div class="m2"><p>آن آتش خان و مان برانداز</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>در بی کز و بازی ار رسیدی؟</p></div>
<div class="m2"><p>بر دوست رسی نه از کزو باز؟</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>از دست خدا خرند جان را</p></div>
<div class="m2"><p>نان پاره نه کز دکان خباز</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>از خود بگذر خدای یک موی</p></div>
<div class="m2"><p>از تارک ما ندارد افراز</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بنشست به عرض وحدت دل</p></div>
<div class="m2"><p>سلطان بدو صد هزار اعزاز</p></div></div>
<div class="b2" id="bn115"><p>ما عرض حقیقت خداییم</p>
<p>شک نیست که هرچه هست ماییم</p></div>
<div class="b" id="bn116"><div class="m1"><p>ماییم ظهور نور انوار</p></div>
<div class="m2"><p>جز ما نبود بدار دیار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>جایی که منم صدای جبریل</p></div>
<div class="m2"><p>میاید و کس نمیدهد بار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>فیض احدیتیم و حق را</p></div>
<div class="m2"><p>در فیض وجود نیست تکرار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>ما مظهر واجب الوجودیم</p></div>
<div class="m2"><p>در ذات صفات و فعل و آثار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>اسرار وجود در تجلیست</p></div>
<div class="m2"><p>ما آینه وجود اسرار</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>یارست که کرده جلوه از سر</p></div>
<div class="m2"><p>تا پای ز پای تا سر یار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>عشقیست که محو کرد و حیران</p></div>
<div class="m2"><p>جان و دل دردمند دیدار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>در دست که کرده از گرانی</p></div>
<div class="m2"><p>سنگ دل کوه را سبکسار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>با روی تو ای مراد هر دل</p></div>
<div class="m2"><p>جان و دل دیده است بیکار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بی درد تو ای طبیب هر درد</p></div>
<div class="m2"><p>جسمست نحیف و روح بیمار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بیمار تراست نفح عیسی</p></div>
<div class="m2"><p>مست غم عشق تست هشیار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>خوابست که نیست همدم عشق</p></div>
<div class="m2"><p>عشقست رفیق بخت بیدار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بی شاخ شکوفه قد دوست</p></div>
<div class="m2"><p>بی نرگس مست چشم دلدار</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>چون نرگس مستمی گران سر</p></div>
<div class="m2"><p>چون شاخ شکوفه سرنگونسار</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بیروی تو لاله نیست در بر</p></div>
<div class="m2"><p>بی موی تو مشک نیست در بار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>چشمی که سمن ندید و شکر</p></div>
<div class="m2"><p>آمیخته گوییا که ناچار</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>زین روی سمن بری بخرمن</p></div>
<div class="m2"><p>زین لعل شکرخوری بخروار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>ای قطب مدیر دار هستی</p></div>
<div class="m2"><p>زین دایره تا بچرخ دوار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>اقلیم دل مرا بتحقیق</p></div>
<div class="m2"><p>سلطانی تخت را سزاوار</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بر تست مدار امر چونانک</p></div>
<div class="m2"><p>بر نقطه مدار خط پرگار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>من تاجرم و متاع من عشق</p></div>
<div class="m2"><p>بازار دلست و حق خریدار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>گنجینه لایزال بر دست</p></div>
<div class="m2"><p>بنشسته بچارسوق بازار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>چشم دل من بیار روشن</p></div>
<div class="m2"><p>خورشید سپهر و دیده تار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ماراست غذای جان و دل دوست</p></div>
<div class="m2"><p>عالم هم کاسه لیس پندار</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>بی قوت لب تو ماسوی را</p></div>
<div class="m2"><p>دل خورده و بازمانده ناهار</p></div></div>
<div class="b2" id="bn141"><p>زین مغز اگر بیفکنی پوست</p>
<p>اعصاب و عروق و جسم و جان اوست</p></div>
<div class="b" id="bn142"><div class="m1"><p>چشمی که ندیده روی ما را</p></div>
<div class="m2"><p>بیند بکدام رو خدا را</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ای آنکه ندیده ییش در عرش</p></div>
<div class="m2"><p>کن سجده جناب قدس ما را</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>در خانه ماست زود زن دست</p></div>
<div class="m2"><p>در زلف بت گریزپا را</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>یکتاست بخانه آنکه دیدست</p></div>
<div class="m2"><p>آنگونه و طره دوتا را</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>ای آنکه ندیدی آن دو سوسن</p></div>
<div class="m2"><p>وان سنبلکان مشک سا را</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بر دست بگیر جان شیرین</p></div>
<div class="m2"><p>پیش آی و بعجز گوی یارا</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بیگانه مشو که جان سپارند</p></div>
<div class="m2"><p>یاران حرکات آشنا را</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>این حرف بگوی و بذل جان کن</p></div>
<div class="m2"><p>زین بذل پذیره شو بقا را</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>چندان که سرای دوست ماند</p></div>
<div class="m2"><p>ماند که زد این در سرا را</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>چندان که جناب عشق باقیست</p></div>
<div class="m2"><p>باقیست که چنگ زد فنا را</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>دل خانه ماست صیقلی کن</p></div>
<div class="m2"><p>آیینه قطب حق نما را</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>این سینه سرای سر عشقست</p></div>
<div class="m2"><p>پرداخته کن ز غیر جا را</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>سلطان ازل رسید تنها</p></div>
<div class="m2"><p>هم ارض گرفت و هم سما را</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>ماهی که دل از سپهر میجست</p></div>
<div class="m2"><p>از دل به سپهر زد لوا را</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>آن دل که مقید هوی بود</p></div>
<div class="m2"><p>زین بست و سوار شد هوی را</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>از غیر ردای فقر بگذشت</p></div>
<div class="m2"><p>بگزید ردای کبریا را</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>از جاه گذشت و از تکبر</p></div>
<div class="m2"><p>هم کبر نهاد و هم ریا را</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>در راه رضای دوست بگزید</p></div>
<div class="m2"><p>بر راحت خویشتن بلا را</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>بگذشت ز حرف دفتر جور</p></div>
<div class="m2"><p>خواند آیت مصحف وفا را</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>دل در پی سلطنت گدا شد</p></div>
<div class="m2"><p>تا دید بساط پادشا را</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>دریافت که شاه مینشاند</p></div>
<div class="m2"><p>بر دامن خویشتن گدا را</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>در ظل حقیقت صفا دید</p></div>
<div class="m2"><p>چون دید حقیقت صفا را</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>قومی که به تاج و گنج سلطان</p></div>
<div class="m2"><p>انعام کنند بینوا را</p></div></div>
<div class="b2" id="bn165"><p>بگذاشت کدورت و صفا شد</p>
<p>بگذشت ز اهرمن خدا شد</p></div>
<div class="b" id="bn166"><div class="m1"><p>ای بنده ز بود خویش لا شو</p></div>
<div class="m2"><p>بگذار ز سر منی و ما شو</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>بیگانه ز پادشاه کثرت</p></div>
<div class="m2"><p>با بنده وحدت آشنا شو</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>حق وحدت باقی است و فانی</p></div>
<div class="m2"><p>در وحدت باقی خدا شو</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>بر غیب و شهود شاه مطلق</p></div>
<div class="m2"><p>سلطان وجود را گدا شو</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>با وحدت ذات خویش مشغول</p></div>
<div class="m2"><p>وارسته ز قید ماسوی شو</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>بی وضع و متی و این فارغ</p></div>
<div class="m2"><p>از چون و چگونه و چرا شو</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>این ارض و سماست پرده ای دل</p></div>
<div class="m2"><p>از ارض منزه و سما شو</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>از ملک و ملک علاقه بگسل</p></div>
<div class="m2"><p>یکتای بری ز هر دو تا شو</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>یار آمده و گه نثارست</p></div>
<div class="m2"><p>ایجان عزیز من فدا شو</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>طالب ز فنا رسید بر دوست</p></div>
<div class="m2"><p>گر طالب دوستی فنا شو</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>مردانه ز هرچه هست بگذر</p></div>
<div class="m2"><p>رندانه بیا و بی ریا شو</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>در دست خودی دوا او نفی</p></div>
<div class="m2"><p>از درد بحضرت دوا شو</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>خواهی رسی ار بسر اطلاق</p></div>
<div class="m2"><p>از بند خود ای پسر رها شو</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>مستغرق قلزم خدایی</p></div>
<div class="m2"><p>بر کشتی کون ناخدا شو</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>تا بار دهندت آشنایان</p></div>
<div class="m2"><p>بیگانه از این منی و ما شو</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>ای دل به طریق عشقبازی</p></div>
<div class="m2"><p>چندی به فراق مبتلا شو</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>تا قدر وصال را بدانی</p></div>
<div class="m2"><p>ای بسته بند هجر وا شو</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>معشوق تویی و عشق و عاشق</p></div>
<div class="m2"><p>گو راز نهفته برملا شو</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>بر دوست گرای و یک حقیقت</p></div>
<div class="m2"><p>بر بام دل آی و یک هوا شو</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>یا کن ظلمات خویشتن طی</p></div>
<div class="m2"><p>چون خضر و به چشمه بقا شو</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>یا گیر بدست دامن پیر</p></div>
<div class="m2"><p>کی خضر مراد رهنما شو</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>ای موسی ما بخضر مگرای</p></div>
<div class="m2"><p>ای آتش طور خضر ما شو</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>ماراست حبال سحر اوهام</p></div>
<div class="m2"><p>ای عقل مجرد اژدها شو</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>قلبست زر وجود ناقص</p></div>
<div class="m2"><p>ای گرد کمال کیمیا شو</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>کن قلب تمام را زر پاک</p></div>
<div class="m2"><p>ای سالک اگر مسی طلا شو</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>بگذار ستبرق سلاطین</p></div>
<div class="m2"><p>سلطان سریر بوریا شو</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>از هرچه کدورتست شو صاف</p></div>
<div class="m2"><p>هم مسلک سیرت صفا شو</p></div></div>
<div class="b2" id="bn193"><p>از خویش بجه ز بند هستی</p>
<p>خود را به مبین و بس که رستی</p></div>