---
title: >-
    بخش ۳۷
---
# بخش ۳۷

<div class="n" id="bn1"><p>در بیان تجلی آن ولی اکبر به قابلیت و استعداد فرزند دلبند خود علی اصغر و با دست مبارکش به میدان بردن و بدرجه‌ی رفیعه‌ی شهادت رسانیدن و مختصری از مراتب و شئونات آن امام زاده‌ی بزرگوار علیه السلام:</p></div>
<div class="b" id="bn2"><div class="m1"><p>بازم اندر مهد دل طفل جنون</p></div>
<div class="m2"><p>دست از قنداقه می‌آرد برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مادر طبع مرا از روی ذوق</p></div>
<div class="m2"><p>خوش درآرد شیر، در پستان شوق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله اطفال قلوب از انبساط</p></div>
<div class="m2"><p>وقت شد کآیند بیرون از قماط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشرتی از آن هوای نو کنند</p></div>
<div class="m2"><p>از طرب، نشو و نمای نو کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واگذارند امهات طبع را</p></div>
<div class="m2"><p>باز آباء کرام سبع را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز وقت کیسه پردازی بود</p></div>
<div class="m2"><p>ای حریف این آخرین بازی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شش جهت در نرد عشق آن پری</p></div>
<div class="m2"><p>می‌کند با مهره دل، ششدری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همتی می‌دارم از ساقی مراد</p></div>
<div class="m2"><p>وز در میخانه می‌جویم گشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچنین از کعبتین عشق داو</p></div>
<div class="m2"><p>تا درین بازی نمایم کنجکاو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بازیی تا اندرین دفتر کنم</p></div>
<div class="m2"><p>شرح شاه پاکبازان، سر کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لاجرم چون آن حریف پاک باز</p></div>
<div class="m2"><p>در قمار عاشقی شد پاکباز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد برون با کیسه‌ی پرداخته</p></div>
<div class="m2"><p>مایه‌یی از جزو و از کل باخته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رقص رقصان، از نشاط باختن</p></div>
<div class="m2"><p>منبسط، از کیسه را پرداختن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انقباضی دید در خود اندکی</p></div>
<div class="m2"><p>در دل حق الیقین آمد شکی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کاین کسالت بعد حالت از چه زاد</p></div>
<div class="m2"><p>حالت کل را کسالت از چه زاد؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس ز روی پاکبازی، جهد کرد</p></div>
<div class="m2"><p>تا فشاند هست اگر در کیسه گرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون فشاند آن پاکبازان را، امیر</p></div>
<div class="m2"><p>گوهری افتاد در دستش، صغیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درة التاج گرامی گوهران</p></div>
<div class="m2"><p>آن سبک در وزن و در قیمت گران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ارفع المقدار من کل الرفیع</p></div>
<div class="m2"><p>الشفیع بن الشفیع بن الشفیع</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرمی آتش، هوای خاک ازو</p></div>
<div class="m2"><p>آب کار انجم و افلاک ازو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کودکی در دامن مهرش بخواب</p></div>
<div class="m2"><p>سه ولد با چارمام و هفت باب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مایه‌ی ایجاد، کز پر مایگی</p></div>
<div class="m2"><p>کرده مهرش، طفل دین را دایگی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وه چه طفلی! ممکنات او راطفیل</p></div>
<div class="m2"><p>دست یکسر کاینات او را به ذیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشته ارشاد از ره صدق و صفا</p></div>
<div class="m2"><p>زیر دامان ولایش، اولیا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شمه‌یی، خلد از رخ زیبنده‌اش</p></div>
<div class="m2"><p>آیتی، کوثر ز شکر خنده‌اش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اشرف اولاد آدم را، پسر</p></div>
<div class="m2"><p>لیکن اندر رتبه آدم را پدر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از علی اکبر بصورت اصغرست</p></div>
<div class="m2"><p>لیک در معنی علی اکبرست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ظاهراً از تشنگی بیتاب بود</p></div>
<div class="m2"><p>باطناً سر چشمه‌ی هر آب بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یافت کاندر بزم آن سلطان ناز</p></div>
<div class="m2"><p>نیست لایق تر ازین گوهر، نیاز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خوش ره آوردی بداندر وقت برد</p></div>
<div class="m2"><p>بر سر دستش به پیش شاه برد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کای شه این گوهر به استسقای تست</p></div>
<div class="m2"><p>خواهش آبش ز خاک پای تست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لطف بر این گوهر نایاب کن</p></div>
<div class="m2"><p>از قبول حضرتش سیراب کن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>این گهر از جزع های تابناک</p></div>
<div class="m2"><p>ای بسا گوهر فروریزد به خاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>این گهر از اشک‌های پر ز خون</p></div>
<div class="m2"><p>می‌کند الماس ها را، لعلگون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آبی ای لب تشنه باز آری بجو</p></div>
<div class="m2"><p>بو که آب رفته باز آری بجو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شرط این آبت، بزاری جستن‌ست</p></div>
<div class="m2"><p>ورنداری، دست از وی شستن‌ست</p></div></div>