---
title: >-
    بخش ۱۱
---
# بخش ۱۱

<div class="n" id="bn1"><p>در استشفای عارفانه در طریق اهل وجد گوید.</p></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا جام دگر لبریز کن</p></div>
<div class="m2"><p>آتش ما را ز آبی، تیز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خرد، ثابت بود بر جای خویش</p></div>
<div class="m2"><p>مدعا را پرده می‌گیرد به پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرخوشم کن ز آن به جان پرورده‌ها</p></div>
<div class="m2"><p>تا تو هم را بسوزم، پرده‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست گردم، رشته‌ای آرم به دست</p></div>
<div class="m2"><p>قصۀ مستان که گوید غیر مست؟</p></div></div>