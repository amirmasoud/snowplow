---
title: >-
    بخش ۳۶
---
# بخش ۳۶

<div class="n" id="bn1"><p>در بیان توصیه‌ی آن مقتدای انام و سید و سرور خاص و عام، خواهر خود را از تیمار بیمار خود، اعنی گرامی فرزند و والاامام السید السجاد، زین العابدین(ع) و تفویض بعضی ودایع که بآن حضرت برساند:</p></div>
<div class="b" id="bn2"><div class="m1"><p>باز دل را نوبت بیماری ست</p></div>
<div class="m2"><p>ای پرستاران زمان یاری‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جستجویی از گرفتاران کنید</p></div>
<div class="m2"><p>پرسشی از حال بیماران کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«عاشقی پیداست از زاری دل</p></div>
<div class="m2"><p>نیست بیماری چو بیماری دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای تا فرقش گرفتار تب است</p></div>
<div class="m2"><p>سرگران از ذکر یارب یارب ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگش از صفرای سودا، زرد شد</p></div>
<div class="m2"><p>پای تا سر مبتلای درد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بیماران که تان، فرهماست</p></div>
<div class="m2"><p>اندر اینجا روی صحبت با شماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکه را اینجا دلی بیمار هست</p></div>
<div class="m2"><p>با خبر ز آن ناله‌های زار هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌دهد یاد از زمانی، کآن امام</p></div>
<div class="m2"><p>سرور دین، مقتدای خاص و عام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهرش را بر سر زانو نشاند</p></div>
<div class="m2"><p>پس گلاب از اشک بر رویش فشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ای خواهر چو برگشتی ز راه</p></div>
<div class="m2"><p>هست بیماری مرا در خیمه گاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان بقربان تن بیمار او</p></div>
<div class="m2"><p>دل فدای ناله‌های زار او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسته‌ی بند غمش، جسم نزار</p></div>
<div class="m2"><p>بسته‌ی بند ولایش، صد هزار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دل شب گر ز دل آهی کند</p></div>
<div class="m2"><p>ناله‌یی گر در سحرگاهی کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن مؤسس، این مقرنس طاق راست</p></div>
<div class="m2"><p>ز آن مروج، انفس و آفاق راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جانفشانی را فتاده محتضر</p></div>
<div class="m2"><p>جان ستانی را ستاده منتظر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پرسشی کن حال بیمار مرا</p></div>
<div class="m2"><p>جستجویی کن، گرفتار مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز آستین اشکش ز چشمان پاک کن</p></div>
<div class="m2"><p>دور از آن رخساره گرد و خاک کن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با تفقد بر گشابند دلش</p></div>
<div class="m2"><p>عقده‌یی گر هست در دل، بگسلش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر بود بیهوش، باز آرش بهوش</p></div>
<div class="m2"><p>در وحدت اندر آویزش بگوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنچه از لوح ضمیرت جلوه کرد</p></div>
<div class="m2"><p>جلوه ده بر لوح آن سلطان فرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرچه نقش صفحه‌ی خاطر مراست</p></div>
<div class="m2"><p>و آنچه ثبت سینه‌ی عاطر مراست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جمله را بر سینه‌اش، افشانده‌ام</p></div>
<div class="m2"><p>از الف تا یا، بگوشش خوانده‌ام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این ودیعت را پس از من حامل اوست</p></div>
<div class="m2"><p>بعد من در راه وحدت، کامل اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اتحاد ماندارد حد و حصر</p></div>
<div class="m2"><p>او حسین عهد و من سجاد عصر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من کیم؟ خورشید، او کی؟ آفتاب</p></div>
<div class="m2"><p>در میان بیماری او شد حجاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>واسطه اندر میان ما، تویی</p></div>
<div class="m2"><p>بزم وحدت را نمی‌گنجد دویی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عین هم هستیم مابی کم و کاست</p></div>
<div class="m2"><p>در حقیقت واسطه هم عین ماست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قطب باید، گردش افلاک را</p></div>
<div class="m2"><p>محوری باید سکون خاک را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چشم بر میدان گمار ای هوشمند</p></div>
<div class="m2"><p>چون من افتادم، تو او را کن بلند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کن خبر آن محیی اموات را</p></div>
<div class="m2"><p>ده قیام آن قائم بالذات را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس وداع خواهر غمدیده کرد</p></div>
<div class="m2"><p>شد روان و خون روان از دیده کرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ذوالجناح عشقش اندر زیر ران</p></div>
<div class="m2"><p>در روش، گامی بدل گامی بجان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر بظاهر، گامزن در فرش بود</p></div>
<div class="m2"><p>لیک در باطن، روان در عرش بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در زمین ار چند بودی، ره نورد</p></div>
<div class="m2"><p>لیک سرمه چشم کروبیش کرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>داد جولان و سخن کوتاه شد</p></div>
<div class="m2"><p>دوست را، وارد به قربانگاه شد</p></div></div>