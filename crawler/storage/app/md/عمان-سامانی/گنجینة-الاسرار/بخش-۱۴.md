---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="n" id="bn1"><p>در انتقال از عالم وجد و شوق دورجوع به مطلب بر مشرب اهل ذوق گوید</p></div>
<div class="b" id="bn2"><div class="m1"><p>باز آن گوینده گفتن ساز کرد</p></div>
<div class="m2"><p>وز زبان من حدیث آغاز کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هل زمانی تا شوم دمساز خویش</p></div>
<div class="m2"><p>بشنوم با گوش خویش آواز خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ببینم اینکه گوید راز، کیست؟</p></div>
<div class="m2"><p>از زبان من سخن پرداز کیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این منم یا رب چنین دستانسرا</p></div>
<div class="m2"><p>یا دگر کس می‌کند تلقین مرا؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این منم یارب بدین گفتار نغز</p></div>
<div class="m2"><p>یا که من چون پوستم گوینده، مغز؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوخ شیرین مشرب من، کیستی؟</p></div>
<div class="m2"><p>ای سخنگوی از لب من، کیستی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصه‌یی مطلوب می‌گویی، بگو</p></div>
<div class="m2"><p>نکته‌یی مرغوب می‌گویی، بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زود باشد کاین می پر مشعله</p></div>
<div class="m2"><p>عارفان را جمله سوزد، مشغله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهروان زین باده مستی‌ها کنند</p></div>
<div class="m2"><p>خودپرستان، حق‌پرستی‌ها کنند</p></div></div>