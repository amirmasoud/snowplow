---
title: >-
    بخش ۴۰
---
# بخش ۴۰

<div class="n" id="bn1"><p>در بیان خطابه‌ی آن امام مهربان و موعظت مخالفان با حقیقت، گو زبان، از راه رحمت و از در شفقت و هدایت بر سبیل اجمال گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>مطرب ای مجموعه‌ی فصل الخطاب</p></div>
<div class="m2"><p>باغ وحدت را، لب لعل تو آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نوایت داده با قدسی نفس</p></div>
<div class="m2"><p>مرغ جان را، جای در خاکی قفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوش خاصان، مستمع بر ساز تو</p></div>
<div class="m2"><p>جان پاکان، گوش بر آواز تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفان حق شنو را، چون سروش</p></div>
<div class="m2"><p>نغمه‌ی وحدت، رسانیده بگوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای زده با آن نوای دلپسند</p></div>
<div class="m2"><p>همچو نی مان، آتش اندر بند بند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان برقص از ناله‌ی شبهای توست</p></div>
<div class="m2"><p>نیشکر ریزیش، از آن لبهای توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده‌یی با بهترین قانون بزن</p></div>
<div class="m2"><p>آتش اندر سینه چون کانون بزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بکی آخر نشابوری نوا</p></div>
<div class="m2"><p>راست کن در نی، نوای نینوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که، جان دیگر نوائی سر کند</p></div>
<div class="m2"><p>نایی طبعم نوائی سر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سازد آگه مستمع را ز آن نوا</p></div>
<div class="m2"><p>از نوای شه بدشت نینوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن زمان کان شاه بر جای ایستاد</p></div>
<div class="m2"><p>بانوای خطبه بر نی تکیه داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرنمود آفاق را ز آوای حق</p></div>
<div class="m2"><p>شد نوای حق بلند از نای حق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتشان کای دشمنان خانگی</p></div>
<div class="m2"><p>آشنایم من، چرا بیگانگی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوش بر آن نغمه‌ی موزون کنید</p></div>
<div class="m2"><p>پنبه را از گوش خود بیرون کنید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کی رسد بی آشنایی با سروش</p></div>
<div class="m2"><p>این نوای آشنائیتان بگوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گوش می‌خواهد ندای آشنا</p></div>
<div class="m2"><p>آشنا داند صدای آشنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نوشتانم من، شما ترسان زنیش</p></div>
<div class="m2"><p>خویشتانم من، شما غافل ز خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من خدا چهرم شما ابلیس چهر</p></div>
<div class="m2"><p>من همه مهرم شما غافل ز مهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رحمت من در مثل همچون هماست</p></div>
<div class="m2"><p>سایه‌اش گسترده بر فرق شماست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون کنم چون؟ نفس کافر مایه‌تان</p></div>
<div class="m2"><p>می‌کند محروم از این سایه‌تان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غیر کافر کس ز من محروم نیست</p></div>
<div class="m2"><p>از هما محروم غیر از بوم نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>موش کورید و من آن تابنده نور</p></div>
<div class="m2"><p>خویش را از نور کردستید، دور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من همه حق و شما باطل همه</p></div>
<div class="m2"><p>از تجلی من شده، عاطل همه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من خداوند و شما شیطان پرست</p></div>
<div class="m2"><p>من ز رحمان و شما ز ابلیس، هست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آنچه فرمود او به آن قوم از صواب</p></div>
<div class="m2"><p>غیر تیر از هیچ سو نامد جواب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیغ ها بر قتل او شد آخته</p></div>
<div class="m2"><p>نیزه‌ها بر قصد او افراخته</p></div></div>