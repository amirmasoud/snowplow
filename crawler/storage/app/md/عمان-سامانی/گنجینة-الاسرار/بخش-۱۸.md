---
title: >-
    بخش ۱۸
---
# بخش ۱۸

<div class="n" id="bn1"><p>در مراتب وجد عارفانه و شور عاشقانه و اشاره به حال خود در انتساب سلوک به حضرت پیرو مرشد صافی ضمیر خود کثر اللّه افاضاته گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>باز وقت آمد که مستی سرکنم</p></div>
<div class="m2"><p>وز هیاهو گوش گردون، کر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از در مجلس در آیم، سرگران</p></div>
<div class="m2"><p>بر زمین، افتان و بر بالا، پران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه رقصان در میان؛ گه در کنار</p></div>
<div class="m2"><p>جام می دستی و دستی زلف یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخ بخ ای صهبای جان پرورد ما</p></div>
<div class="m2"><p>مرهم زخم و دوای درد ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخ بخ صهبای جان افروز ما</p></div>
<div class="m2"><p>عشرت شب، انبساط روز ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خدا دوران، خدا دورت کند</p></div>
<div class="m2"><p>فارغ از سرهای بی شورت کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوی از ما آن ملامت گوی را</p></div>
<div class="m2"><p>آن ترش کرده به مستان، روی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می سزد سنگ ارزنی ما را بجام</p></div>
<div class="m2"><p>چون نخوردت بوی این می بر مشام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شور مجنون گر همی خواهی هله</p></div>
<div class="m2"><p>زلف لیلی را بجنبان سلسله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای سرا پا عقل خالص، روح پاک</p></div>
<div class="m2"><p>از چه جسمی زاده‌یی؟ روحی فداک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای وجودت در صفا، مرآت حق</p></div>
<div class="m2"><p>بهره‌مند از هر صفت، جز ذات حق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای ز شبهت، مادر گیتی، عقیم</p></div>
<div class="m2"><p>ای بحق ما را صراط المستقیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای شب جهال را؛ تابنده ماه</p></div>
<div class="m2"><p>ای بره گم کرد گان؛ هادی راه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از تو آمد مقصد عارف پدید</p></div>
<div class="m2"><p>چشم حق بینان خدا را درتو دید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مدتی شد هستم ای صدر کبار</p></div>
<div class="m2"><p>این بساط کبریایی را غبار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندک اندک طاقتم را کاهش‌ست</p></div>
<div class="m2"><p>از تو ای ساقی، مرا این خواهش‌ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بازمان ز آن باده در ساغر کنی</p></div>
<div class="m2"><p>حالت ما را پریشانتر کنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بگویم بی کم و بی کاستی</p></div>
<div class="m2"><p>آری ‌آری مستی است و راستی:</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شرح آن سرحلقهٔ عشّاق را</p></div>
<div class="m2"><p>پُر کنم، مجموعهٔ اوراق را</p></div></div>