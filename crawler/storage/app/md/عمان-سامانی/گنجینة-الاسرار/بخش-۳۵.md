---
title: >-
    بخش ۳۵
---
# بخش ۳۵

<div class="n" id="bn1"><p>در بیان تجلی کردن جمال بی‌مثال حسینی از روی معنی در آینهٔ وجود زینب خاتون سلام اللّه علیه و علیها از راه شهود به طور اجمال گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>قابل اسرار دید آن سینه را</p></div>
<div class="m2"><p>مستعد جلوه، آن آیینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک هستی منهدم یکباره کرد</p></div>
<div class="m2"><p>پرده‌ی پندار او را پاره کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی اندر لوح صورت، نقش بست</p></div>
<div class="m2"><p>آنچه از جان خاست اندر دل نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیمه زد در ملک جانش شاه غیب</p></div>
<div class="m2"><p>شسته شد ز آب یقینش زنگ ریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی خود را بچشم خویش دید</p></div>
<div class="m2"><p>صورت آینده راه از پیش دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتابی کرد در زینب ظهور</p></div>
<div class="m2"><p>ذره‌یی ز آن، آتش وادی طور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد عیان در طور جانش رایتی</p></div>
<div class="m2"><p>خر موسی صعقا، ز آن آیتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عین زینب دید زینب را بعین</p></div>
<div class="m2"><p>بلکه با عین حسین عین حسین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طلعت جان را به چشم جسم دید</p></div>
<div class="m2"><p>در سراپای مسمی اسم دید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غیب بین گردید با چشم شهود</p></div>
<div class="m2"><p>خواند بر لوح وفا، نقش عهود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دید تابی در خود و بیتاب شد</p></div>
<div class="m2"><p>دیده‌ی خورشید بین پر آب شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صورت حالش پریشانی گرفت</p></div>
<div class="m2"><p>دست بیتابی به پیشانی گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواست تابر خرمن جنس زنان</p></div>
<div class="m2"><p>آتش اندازد «انا الاعلی» زنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دید شه لب را بدندان می‌گزد</p></div>
<div class="m2"><p>کز تو اینجا پرده داری می‌سزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رخ ز بیتابی، نمی‌تابی چرا؟</p></div>
<div class="m2"><p>در حضور دوست، بیتابی چرا؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرد خود داری ولی تابش نبود</p></div>
<div class="m2"><p>ظرفیت در خورد آن آبش نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تجلی‌های آن سرو سهی</p></div>
<div class="m2"><p>خواست تا زینب کند قالب تهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سایه سان بر پای آن پاک اوفتاد</p></div>
<div class="m2"><p>صیحه زن غش کرد و بر خاک اوفتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از رکاب ای شهسوار حق پرست</p></div>
<div class="m2"><p>پای خالی کن که زینب شد ز دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد پیاده، بر زمین زانو نهاد</p></div>
<div class="m2"><p>بر سر زانو سر بانو نهاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس در آغوشش نشانید و نشست</p></div>
<div class="m2"><p>دست بر دل زد، دل آوردش بدست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتگو کردند با هم متصل</p></div>
<div class="m2"><p>این به آن و آن به این، از راه دل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیگر اینجا گفتگو را راه نیست</p></div>
<div class="m2"><p>پرده افکندند و کس را راه نیست</p></div></div>