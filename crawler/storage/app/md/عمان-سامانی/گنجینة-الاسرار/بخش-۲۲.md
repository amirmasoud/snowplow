---
title: >-
    بخش ۲۲
---
# بخش ۲۲

<div class="n" id="bn1"><p>در ازدیاد وجد و اشتداد شوق بر مشرب اهل عرفان و ذوق و اشارت به مراتب عالیه‌ی زبده و برگزیده‌ی ناس حضرت ابوالفضل العباس سلام اللّه علیه بر سبیل اجمال گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>باز لیلی زد به گیسو شانه را</p></div>
<div class="m2"><p>سلسله جنبان شد این دیوانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنگ بر دارید ای فرزانگان</p></div>
<div class="m2"><p>ای هجوم آرنده بر دیوانگان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چه بر دیوانه‌تان، آهنگ نیست</p></div>
<div class="m2"><p>او مهیا شد، شما را سنگ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل را با عشق، تاب جنگ کو؟</p></div>
<div class="m2"><p>اندرین جا سنگ باید، سنگ کو؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز دل افراشت از مستی علم</p></div>
<div class="m2"><p>شد سپهدار علم، جف القلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشته با شور حسینی، نغمه گر</p></div>
<div class="m2"><p>کسوت عباسیان کرده به بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانب اصحاب، تازان با خروش</p></div>
<div class="m2"><p>مشکی از آن حقیقت پر، به دوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرده از شط یقین، آن مشک پر</p></div>
<div class="m2"><p>مست و عطشان همچو آب آورشتر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشنه‌ی آبش، حریفان سر بسر</p></div>
<div class="m2"><p>خود ز مجموع حریفان، تشنه‌تر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرخ زاستسقای آبش در طپش</p></div>
<div class="m2"><p>برده او بر چرخ بانگ العطش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای ز شط سوی محیط آورده آب</p></div>
<div class="m2"><p>آب خود را ریختی، واپس شتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب آری سوی بحر موج خیز!</p></div>
<div class="m2"><p>بیش ازین آبت مریز آبت بریز</p></div></div>