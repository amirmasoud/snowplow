---
title: >-
    بخش ۱۷
---
# بخش ۱۷

<div class="n" id="bn1"><p>در بیان عارف شدن به مراتب جانبازان راه حقیقت از در ارادت به شیخ طریقت از ر اه مراقبه گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>باز هستی، طاقتم را طاق کرد</p></div>
<div class="m2"><p>دفتر صبر مرا؛ اوراق کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یادم آمد؛ خلوتی خالی ز غیر</p></div>
<div class="m2"><p>پیری اندر صدر آن، یادش بخیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خم صفت، صافی دل و روشن ضمیر</p></div>
<div class="m2"><p>خضروش، گمگشتگان را دستگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مر مرا از حال خویش افزوده حال</p></div>
<div class="m2"><p>خواب بود این می ندانم یا خیال؟!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هشت بر زانو، سر تسلیم من</p></div>
<div class="m2"><p>خواست تا سری ...</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس لب گوهر فشان آورد پیش</p></div>
<div class="m2"><p>پیشتر بردم دو گوش هوش خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دم آن مقبل صاحب نظر</p></div>
<div class="m2"><p>گشتم از شور شهیدان، با خبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عالمی دیدم ازین عالم، برون</p></div>
<div class="m2"><p>عاشقانی، سرخ رو یکسر ز خون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست بر دامان واجب، بر زده</p></div>
<div class="m2"><p>خود ز امکان خیمه بالاتر زده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد آن شمع هدی از هر کنار</p></div>
<div class="m2"><p>پرزنان و پرفشان، پروانه‌وار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترسم از این بیشتر، شرحی دهم</p></div>
<div class="m2"><p>تار تن را، نطق بشکافد ز هم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز آنکه در گوش من آن والانژاد</p></div>
<div class="m2"><p>گفت، اما رخصت گفتن نداد</p></div></div>