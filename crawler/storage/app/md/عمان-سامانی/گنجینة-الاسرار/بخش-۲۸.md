---
title: >-
    بخش ۲۸
---
# بخش ۲۸

<div class="n" id="bn1"><p>در بیان جواب دادن آن ولی اکبر با توجهات و تفقدات مر نوردیده‌ی خود، علی اکبر را برمصداق اینکه، بهرچه از دوست و امانی، چه کفر آن حرف و چه ایمان» بر مذاق اهل عرفان گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>در جواب ار تنک شکر قند ریخت</p></div>
<div class="m2"><p>شکر از لب های شکر خند ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت: کای فرزند مقبل آمدی</p></div>
<div class="m2"><p>آفت جان، رهزن دل آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده‌یی از حق؛ تجلی ای پسر</p></div>
<div class="m2"><p>زین تجلی، فتنه‌ها داری بسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست بهر فتنه، قامت کرده‌یی</p></div>
<div class="m2"><p>وه کزین قامت، قیامت کرده‌یی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرگست بالاله در طنازیست</p></div>
<div class="m2"><p>سنبلت با ارغوان در بازیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رخت مست غرورم می‌کنی</p></div>
<div class="m2"><p>از مراد خویش دورم می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه دلم پیش تو گاهی پیش اوست</p></div>
<div class="m2"><p>رو که در یک دل نمی‌گنجد دو دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیش ازین بابا! دلم را خون مکن</p></div>
<div class="m2"><p>زاده‌ی لیلی؛ مرا مجنون مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پشت پا، بر ساغر حالم مزن</p></div>
<div class="m2"><p>نیش بر دل؛ سنگ بر بالم مزن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک غم بر فرق بخت دل مریز</p></div>
<div class="m2"><p>بس نمک بر لخت لخت دل مریز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو چشم خود به قلب دل متاز</p></div>
<div class="m2"><p>همچو زلف خود، پریشانم مساز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حایل ره، مانع مقصد مشو</p></div>
<div class="m2"><p>بر سر راه محبت، سد مشو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لن تنالوا البر حتی تنفقوا</p></div>
<div class="m2"><p>بعد از آن؛ مما تحبون گوید او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست اندر بزم آن والا نگار</p></div>
<div class="m2"><p>از تو بهتر گوهری، بهر نثار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرچه غیر از اوست، سد راه من</p></div>
<div class="m2"><p>آن بت ست و غیرت من، بت شکن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان رهین و دل اسیر چهر تست</p></div>
<div class="m2"><p>مانع راه محبت، مهر تست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن حجاب از پیش چون دورافکنی</p></div>
<div class="m2"><p>من تو هستم در حقیقت، تو منی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ترا او خواهد از من رو نما</p></div>
<div class="m2"><p>رو نما شو، جانب او، رو، نما</p></div></div>