---
title: >-
    بخش ۳۳
---
# بخش ۳۳

<div class="n" id="bn1"><p>در بیان تعرض آن شهسوار میدان حقیقت از جهان تجرد بعالم تقید و توجه و تفقد به خواهر خود بر مذاق عارفان گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>پس زجان بر خواهر استقبال کرد</p></div>
<div class="m2"><p>تا رخش بوسد، الف را دال کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو جان خود در آغوشش کشید</p></div>
<div class="m2"><p>این سخن آهسته بر گوشش کشید:</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کای عنان گیر من آیا زینبی؟</p></div>
<div class="m2"><p>یا که آه دردمندان در شبی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش پای شوق زنجیری مکن</p></div>
<div class="m2"><p>راه عشق‌ست این عنانگیری مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با تو هستم جان خواهر، همسفر</p></div>
<div class="m2"><p>تو بپا این راه کوبی من بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خانه سوزان را تو صاحبخانه باش</p></div>
<div class="m2"><p>با زنان در همرهی مردانه باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان خواهر در غمم زاری مکن</p></div>
<div class="m2"><p>با صدا بهرم عزاداری مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معجراز سر، پرده از رخ، وامکن</p></div>
<div class="m2"><p>آفتاب و ماه را رسوا مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست بر من ناگوار و ناپسند</p></div>
<div class="m2"><p>از تو زینب گر صدا گردد بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرچه باشد تو علی را دختری</p></div>
<div class="m2"><p>ماده شیرا کی کم از شیر نری؟!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با زبان زینبی شاه آنچه گفت</p></div>
<div class="m2"><p>با حسینی گوش، زینب می شنفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با حسینی لب هر آنچاو گفت راز</p></div>
<div class="m2"><p>شه بگوش زینبی بشنید باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوش عشق، آری زبان خواهد زعشق</p></div>
<div class="m2"><p>فهم عشق آری بیان خواهد ز عشق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با زبان دیگر این آواز نیست</p></div>
<div class="m2"><p>گوش دیگر، محرم این راز نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای سخنگو، لحظه‌یی خاموش باش</p></div>
<div class="m2"><p>ای زبان، از پای تا سر گوش باش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا ببینم از سر صدق و صواب</p></div>
<div class="m2"><p>شاه را، زینب چه می‌گو‌ید جواب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت زینب در جواب آن شاه را:</p></div>
<div class="m2"><p>کای فروزان کرده مهر و ماه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عشق را، از یک مشیمه ازاده‌ایم</p></div>
<div class="m2"><p>لب به یک پستان غم بنهاده‌ایم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تربیت بوده‌ست بر یک دوشمان</p></div>
<div class="m2"><p>پرورش در جیب یک آغوشمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا کنیم این راه را مستانه طی</p></div>
<div class="m2"><p>هر دو از یک جام خوردستیم می</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر دو در انجام طاعت کاملیم</p></div>
<div class="m2"><p>هر یکی امر دگر را حاملیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو شهادت جستی ای سبط رسول</p></div>
<div class="m2"><p>من اسیری را به جان کردم قبول</p></div></div>