---
title: >-
    بخش ۲۷
---
# بخش ۲۷

<div class="n" id="bn1"><p>در بیان اینکه طالبان راه و عاشقان لقاءاللّه را، از خلع تعینات و قلع تعلقات که هر یک مقصد را، سد راهند و حجابی همت کاه گریزی نیست چه عارف را حذر از آفات و موحد را، اسقاط اضافات واجبند لله در قائله:</p></div>
<div class="b" id="bn2"><div class="m1"><p>چو ممکن گرد هستی برنشاند</p></div>
<div class="m2"><p>بجز واجب دگر چیزی نماند</p></div></div>
<div class="n" id="bn3"><p>و اشارت به آن موحد بی نیاز و مجاهد، خانه برانداز که گرد تعلقات را به باران مجاهده فرو نشانید و نقود تعینات را بهوای مشاهده بر فشانید و شرذمه‌یی از حالات جناب علی اکبر سلام اللّه علیه، که در مرتبه‌ی والاترین تعینات و در منزله‌ی بالاترین تعلقات بود، گوید:</p></div>
<div class="b" id="bn4"><div class="m1"><p>بازم اندر هر قدم، در ذکر شاه</p></div>
<div class="m2"><p>از تعلق گردی آید سد راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش مطلب، سد بابی می‌شود</p></div>
<div class="m2"><p>چهر مقصد را، حجابی می‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی ای منظور جان افروز من</p></div>
<div class="m2"><p>ای تو آن پیر تعلق سوز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ده آن صهبای جان پرورد را</p></div>
<div class="m2"><p>خوش به آبی بر نشان، این گرد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که ذکر شاه جانبازان کنم</p></div>
<div class="m2"><p>روی در، با خانه پردازان کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن برتبت، موجد لوح و قلم</p></div>
<div class="m2"><p>و آن بجانبازی، ز جانبازان علم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر هدف، تیر مراد خود نشاند</p></div>
<div class="m2"><p>گرد هستی را، بکلی برفشاند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد ایثار آنچه گرد، آورده بود</p></div>
<div class="m2"><p>سوخت هرچ آن آرزو را پرده بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از تعلق، پرده‌یی دیگر نماند</p></div>
<div class="m2"><p>سد راهی؛ جز علی اکبر نماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اجتهادی داشت از اندازه بیش</p></div>
<div class="m2"><p>کان یکی را نیز بردارد ز پیش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که اکبر با رخ افروخته</p></div>
<div class="m2"><p>خرمن آزادگان را، سوخته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ماه رویش، کرده از غیرت، عرق</p></div>
<div class="m2"><p>همچو شبنم، صبحدم بر گل ورق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر رخ افشان کرده زلف پر گره</p></div>
<div class="m2"><p>لاله را پوشیده از سنبل، زره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نرگسش سرمست در غارتگری</p></div>
<div class="m2"><p>سوده مشک تر، به گلبرگ تری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آمد وافتاد از ره، باشتاب</p></div>
<div class="m2"><p>همچو طفل اشک، بر دامان باب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کای پدر جان! همرهان بستند بار</p></div>
<div class="m2"><p>ماند بار افتاده اندر رهگذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر یک از احباب سرخوش در قصور</p></div>
<div class="m2"><p>وز طرب پیچان، سر زلفین حور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گامزن، در سایه‌ی طوبی همه</p></div>
<div class="m2"><p>جامزن، با یار کروبی همه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قاسم و عبداللّه و عباس و عون</p></div>
<div class="m2"><p>آستین افشان ز رفعت؛ برد و کون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از سپهرم، غایت دلتنگی‌ست</p></div>
<div class="m2"><p>کاسب اکبر را چه وقت لنگی‌ست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیر شد هنگام رفتن ای پدر</p></div>
<div class="m2"><p>رخصتی گر هست باری زودتر</p></div></div>