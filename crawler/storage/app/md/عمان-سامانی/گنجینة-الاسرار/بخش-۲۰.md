---
title: >-
    بخش ۲۰
---
# بخش ۲۰

<div class="n" id="bn1"><p>در بیان اشتداد وجد و حال و انقلاب حالت آن سید بی همال که مباداقدایی آید یا بدائی رخ نماید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>ز آن نمی‌آرم بر آوردن خروش</p></div>
<div class="m2"><p>ترسم او را آن خروش آید بگوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باروش آید که ما را تاب نیست</p></div>
<div class="m2"><p>تاب کتان در بر مهتاب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمت آرد بر دل افکار ما</p></div>
<div class="m2"><p>بخشد او بر ناله‌های زار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندک اندک دست بر دارد ز جور</p></div>
<div class="m2"><p>ناقص آید، بر من، این فرخنده دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرخوشم، کان شهریار مهوشان</p></div>
<div class="m2"><p>کی به مقتل پا نهد دامن کشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان خویش بیند سرخ رو</p></div>
<div class="m2"><p>خون روان از چشمشان مانند جو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرق خون افتاده در بالای خاک</p></div>
<div class="m2"><p>سوده بر خاک مذلت، روی پاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان بکف بگرفته از بهر نیاز</p></div>
<div class="m2"><p>چشمشان بر اشتیاق دوست، باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر غریبیشان کند خوش خوش نگاه</p></div>
<div class="m2"><p>بر ضعیفیشان بخندد، قاه قاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لب چو بربست آن شه دلدادگان</p></div>
<div class="m2"><p>حرز جا جست، آن سر آزادگان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت: کای صورتگر ارض و سما</p></div>
<div class="m2"><p>ای دلت، آینه‌ی ایزد نما</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اول این آیینه از من یافت زنگ</p></div>
<div class="m2"><p>من نخست انداختم بر جام سنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باید اول از پی دفع گله</p></div>
<div class="m2"><p>من بجنبانم سر این سلسله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شورش اندر مغزمستان آورم</p></div>
<div class="m2"><p>می بیاد می پرستان آورم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پاسخش را از دو مرجان ریخت، در</p></div>
<div class="m2"><p>گفت احسنت انت فی الدارین حر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قصد جانان کرد و جان بر باد داد</p></div>
<div class="m2"><p>رسم آزادی به مردان، یاد داد</p></div></div>