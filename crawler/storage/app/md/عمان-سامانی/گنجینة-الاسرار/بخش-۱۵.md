---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="n" id="bn1"><p>رجوع به مطلب و بیان حال آن طالب و مطلوب حضرت رب اعنی شیرازه‌ی دفتر توحید و دروازه‌ی کشور تجرید و تفرید سراندازان را رئیس و سالار، پاکبازان را انیس و غمخوار، سید جن و بشر، سر حلقه‌ی اولیائی حشر: مولی الموالی سیدالکونین ابی عبدالله الحسین صلوات اللّه علیه و اصحابه و ورود آن حضرت به صحرای کربلا و هجوم و ازدحام کرب وبلا:</p></div>
<div class="b" id="bn2"><div class="m1"><p>گوید او چون باده خواران الست</p></div>
<div class="m2"><p>هر یک اندر وقت خود گشتند مست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز انبیا و اولیا، از خاص و عام</p></div>
<div class="m2"><p>عهد هر یک شد به عهد خود تمام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوبت ساقی سرمستان رسید</p></div>
<div class="m2"><p>آنکه بدپا تا بسرمست، آن رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه بد منظور ساقی هست شد</p></div>
<div class="m2"><p>و آنکه گل از دست برد، از دست شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم شد بازار عشق ذوفنون</p></div>
<div class="m2"><p>بوالعجب عشقی، جنون اندر جنوب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیره شد تقوی و زیبایی بهم</p></div>
<div class="m2"><p>پنجه زد درد و شکیبایی بهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوختن با ساختن آمد قرین</p></div>
<div class="m2"><p>گشت محنت با تحمل، همنشین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زجر و سازش متحد شد، درد و صبر</p></div>
<div class="m2"><p>نور و ظلمت متفق شد، ماه و ابر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیش و غم مدغم شد و تریاق و زهر</p></div>
<div class="m2"><p>مهر و کین توأم شد و اشفاق و قهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناز معشوق و نیاز عاشقی</p></div>
<div class="m2"><p>جور عذرا و رضای وامقی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق، ملک قابلیت دید صاف</p></div>
<div class="m2"><p>نزهت از قافش گرفته تا بقاف</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بساط آن، فضایش بیشتر</p></div>
<div class="m2"><p>جای دارد هرچه آید، پیشتر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت اینک آمدم من ای کیا</p></div>
<div class="m2"><p>گفت از جان آرزومندم بیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت بنگر، بر ز دستم آستین</p></div>
<div class="m2"><p>گفت منهم برزدم دامان، ببین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاجرم زد خیمه عشق بی قرین</p></div>
<div class="m2"><p>در فضای ملک آن عشق آفرین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بی قرینی با قرین شد، همقران</p></div>
<div class="m2"><p>لامکانی را، مکان شد لامکان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرد بروی باز، درهای بلا</p></div>
<div class="m2"><p>تا کشانیدش بدشت کربلا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داد مستان شقاوت را خبر</p></div>
<div class="m2"><p>کاینک آمد آن حریف در بدر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نک نمایید آید آنچ از دستتان</p></div>
<div class="m2"><p>میرود فرصت، بنازم شستتان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سرکشید از چار جانب فوج فوج</p></div>
<div class="m2"><p>لشکر غم، همچنان کز بحر، موج</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یافت چون سرخیل مخموران خبر</p></div>
<div class="m2"><p>کز خمار باده آید درد سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خواند یکسر همرهان خویش را</p></div>
<div class="m2"><p>خواست هم بیگانه و هم خویش را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفتشان ای مردم دنیا طلب</p></div>
<div class="m2"><p>اهل مصر و کوفه و شام و حلب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مغزتان را شور شهوت غالبست</p></div>
<div class="m2"><p>نفستان، جاه و ریاست طالبست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای اسیران قضا؛ در این سفر</p></div>
<div class="m2"><p>غیر تسلیم و رضا، این المفر؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همره مارا هوای خانه نیست</p></div>
<div class="m2"><p>هرکه جست از سوختن، پروانه نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیست در این راه غیر از تیر و تیغ</p></div>
<div class="m2"><p>گو میا، هرکس ز جان دارد دریغ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جای پا باید بسر بشتافتن</p></div>
<div class="m2"><p>نیست شرط راه، رو برتافتن</p></div></div>