---
title: >-
    بخش ۱
---
# بخش ۱

<div class="n" id="bn1"><p>در بیان اینکه صاحب جمال را خودنمائی موافق حکمت شرط‌ست و اشاره به تجلی اول بر وجه اتم و اکمل و پوشیدن اعیان ثابته کسوت تعین را و طلوع عشق از مطلع لاهوتی و تجلی به عالم ملکوتی و ناسوتی- نعم ماقال:</p></div>
<div class="b" id="bn2"><div class="m1"><p>در ازل پرتو حسنت ز تجلی دم زد</p></div>
<div class="m2"><p>عشق پیدا شد و آتش به همه عالم زد (حافظ)</p></div></div>
<div class="n" id="bn3"><p>بر مصداق حدیث کنت کنزاً مخفیا فأحببت ان اعرف بر مذاق اهل توحید گوید:</p></div>
<div class="b" id="bn4"><div class="m1"><p>کیست این پنهان مرادر جان و تن</p></div>
<div class="m2"><p>کز زبان من همی گوید سخن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این که گوید از لب من راز کیست</p></div>
<div class="m2"><p>بنگرید این صاحب آواز کیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در من اینسان خود نمایی می‌کند</p></div>
<div class="m2"><p>ادعای آشنایی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست این گویا و شنوا در تنم؟</p></div>
<div class="m2"><p>باورم یارب نیاید کاین منم!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>متصلتر با همه دوری به من</p></div>
<div class="m2"><p>ازنگه با چشم و از لب با سخن!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش پریشان با منش گفتارهاست</p></div>
<div class="m2"><p>در پریشان گوئیش اسرارهاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوید او چون شاهدی صاحب جمال</p></div>
<div class="m2"><p>حسن خود بیند بسرحد کمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برای خودنمایی صبح و شام</p></div>
<div class="m2"><p>سر برآرد گه ز روزن گه ز بام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با خدنگ غمزه صیددل کند</p></div>
<div class="m2"><p>دید هرجا طایری بسمل کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گردنی هر جا درآرد در کمند</p></div>
<div class="m2"><p>تا نگوید کس اسیرانش کمند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لاجرم آن شاهد بالا و پست</p></div>
<div class="m2"><p>با کمال دلربایی در الست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جلوه‌اش گرمی بازاری نداشت</p></div>
<div class="m2"><p>یوسف حسنش خریداری نداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غمزه‌اش را قابل تیری نبود</p></div>
<div class="m2"><p>لایق پیکانش نخجیری نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عشوه‌اش هرجا کمند انداز گشت</p></div>
<div class="m2"><p>گردنی لایق نیامد، بازگشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ما سوی آیینۀ آن رو شدند</p></div>
<div class="m2"><p>مظهر آن طلعت دلجو شدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پس جمال خویش در آیینه دید</p></div>
<div class="m2"><p>روی زیبا دید و عشق آمد پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدتی آن عشق بی نام و نشان</p></div>
<div class="m2"><p>بد معلق در فضای بیکران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلنشین خویش مأوائی نداشت</p></div>
<div class="m2"><p>تا درو منزل کند جایی نداشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بهر منزل بیقراری ساز کرد</p></div>
<div class="m2"><p>طالبان خویش را آواز کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چونکه یکسر طالبان را جمع ساخت</p></div>
<div class="m2"><p>جمله را پروانه خود را شمع ساخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جلوه‌ای کرد از یمین و از یسار</p></div>
<div class="m2"><p>دوزخی و جنتی کرد آشکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جنتی خاطرنواز و دل‌فروز</p></div>
<div class="m2"><p>دوزخی دشمن‌گداز و غیر سوز</p></div></div>