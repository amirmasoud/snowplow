---
title: >-
    بخش ۱ - توحید
---
# بخش ۱ - توحید

<div class="b" id="bn1"><div class="m1"><p>ای که از هستی ترا آثار نیست</p></div>
<div class="m2"><p>عاقل از هستیت در انکار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست محروم از ظهور یک گیاه</p></div>
<div class="m2"><p>خالی از انوار تو یک پر کاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود آنکه اندر پرده ای</p></div>
<div class="m2"><p>سر ز هر پرده برون آورده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوهها پاشید از توحید تو</p></div>
<div class="m2"><p>زهره ها، بدرید از تمجید تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالبانت «رب ارنی » گو همه</p></div>
<div class="m2"><p>«لن ترانی » پاسخ از هر سو همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه وجودت مخفی از فرط ظهور</p></div>
<div class="m2"><p>بر مثال آفتاب از فرط نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان سبب تمثیل کردم آفتاب</p></div>
<div class="m2"><p>تا بیابد نکته فهم دیر یاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ورنه بیرونی تو از ادراک ما</p></div>
<div class="m2"><p>کی بروید این گیاه از خاک ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر اینجا شاهدی گیرم قوی</p></div>
<div class="m2"><p>از کتاب مستطاب مثنوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«ای برون از فهم قال و قیل من</p></div>
<div class="m2"><p>خاک بر فرق من و تمثیل من »</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارفانت در ثنا، مبهوت و مات</p></div>
<div class="m2"><p>وز ثنایت جمله عاجز در صفات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمله محتاجند و ذات تو غنی</p></div>
<div class="m2"><p>بر تو زیبد کبریایی و منی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مایه مجموع هستی ها تویی</p></div>
<div class="m2"><p>خالق بالا و پستی ها تویی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ظاهر از بود تو بوده کاینات</p></div>
<div class="m2"><p>ما به تو قائم تو چه قائم به ذات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیستی و کس نداند چیستی</p></div>
<div class="m2"><p>هستی اما در ردای نیستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم و گوشی ده که بی گفت و شنفت</p></div>
<div class="m2"><p>یا زبانی ده که نتوانیم گفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ورنه بیرونی تو از فهم عقول</p></div>
<div class="m2"><p>ای منزه ذات تو عمایقول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا بگیر این لغوهای خام ما</p></div>
<div class="m2"><p>یا زبان خود بنه در کام ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر که تنزیهت کنم آن فهم نیست</p></div>
<div class="m2"><p>گر که تشبیهت کنم از کافریست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وانکه وصفت بر مذاق خاص و عام</p></div>
<div class="m2"><p>این همه گفتند و مطلب ناتمام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>معذرت را پیش ارباب کظیم</p></div>
<div class="m2"><p>خوش بگو «واستغفر الله العظیم »</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کی دهد صورت ز صورتگر خبر</p></div>
<div class="m2"><p>آن بود مبهوت صورتگر صور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وان که باشد عارف صاحبنظر</p></div>
<div class="m2"><p>بیندت در هر گیاهی جلوه گر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مور را فر سلیمانی دهد</p></div>
<div class="m2"><p>خاک را تشریف سلطانی دهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خوش نباشد وصف سیمرغ از مگس</p></div>
<div class="m2"><p>ای تو کمتر از مگس این حرف بس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ملک تو باشد قدیم و تازه نیست</p></div>
<div class="m2"><p>کبریایی ترا اندازه نیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از خیال تو برون در هیچ حال</p></div>
<div class="m2"><p>ما نباشیم ای تو بیرون از خیال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از دل و جانت بیارای کارساز</p></div>
<div class="m2"><p>هر شب آریم ای تو از ما بی نیاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ز اسمت مانده اندر پرده در</p></div>
<div class="m2"><p>پرده ما روسیاهان را مدر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کافران عشق تو ای بی قرین</p></div>
<div class="m2"><p>جمله را نسبت خداوندان دین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>می کشی عشاق هیچت نیست غم</p></div>
<div class="m2"><p>نیستت اندر ترازو سنگ کم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در ره عشق تو سرها ریخته</p></div>
<div class="m2"><p>خونشان با خاک ره آمیخته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه به تعریفت حدی نه غایت است</p></div>
<div class="m2"><p>«ماعرفناک » رسولت آیت است</p></div></div>