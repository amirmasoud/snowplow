---
title: >-
    بخش ۳ - در فضیلت مولای متقیان حضرت امیرالمؤمنین(ع)
---
# بخش ۳ - در فضیلت مولای متقیان حضرت امیرالمؤمنین(ع)

<div class="b" id="bn1"><div class="m1"><p>گفت آن بینای ما زاغ البصر</p></div>
<div class="m2"><p>این عبارت را به طرزی مختصر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاندر آن شب چون دلم شد منجلی</p></div>
<div class="m2"><p>هر کجا دیدم علی دیدم علی(ع)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خدا در لامکان سرگرم راز</p></div>
<div class="m2"><p>با ملک در آسمان اندر نماز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در یکی صف در رکوع و در سجود</p></div>
<div class="m2"><p>در دگر صف در قیام و در قعود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زمین اصحاب را مهمان به خوان</p></div>
<div class="m2"><p>مر مرا در عرش اعظم میزبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر فراز عرش با من با شتاب</p></div>
<div class="m2"><p>بر زمین بربسته زهرا را به خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جا گرفته بر سر هر محتضر</p></div>
<div class="m2"><p>گشته هر مولود را حاضر به سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر فلک می دیدمش موجود بس</p></div>
<div class="m2"><p>در زمین موجد به هر صاحب نفس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی رضای او عبادتهاست خام</p></div>
<div class="m2"><p>بی ولای او اطاعت ناتمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه گه آوازی به گوش انباز بود</p></div>
<div class="m2"><p>ابن عمم صاحب آواز بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>احمد(ص) آن دیباچه دیوان کل</p></div>
<div class="m2"><p>احمد(ص) آن سر دفتر ختم رسل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مطلع التوحید سلطان ازل</p></div>
<div class="m2"><p>مقطع العنوان حی لم یزل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اولین نقش تجلیات ذات</p></div>
<div class="m2"><p>آخرین نقش تمام کاینات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صادر اول ز مصدر در الست</p></div>
<div class="m2"><p>اولین مصدر به هر صادر که هست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اولین صبح از بروز و از ظهور</p></div>
<div class="m2"><p>آخرین صبح از تجلیات نور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اولین جمله اشیا به اسم</p></div>
<div class="m2"><p>آخرین جمله اشیا به جسم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن خرامان سرو باغ «فاستقم »</p></div>
<div class="m2"><p>منزل وحی و ندای قم اقم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه عالم را شفای علت است</p></div>
<div class="m2"><p>آنکه آدم را شفیع ذلت است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صفحه ایجاد را بنوشت و خواند</p></div>
<div class="m2"><p>نی بخواند و نی قلم بر صفحه راند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حامل مهر نبوت پشت او</p></div>
<div class="m2"><p>گشته است ماه سما انگشت او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مصطفی(ص) آن مظهر ذات اله</p></div>
<div class="m2"><p>از طفیلش خلق ماهی تا به ماه</p></div></div>