---
title: >-
    شمارهٔ ۵: امشب در این بساط به ما عشرت شهی است
---
# شمارهٔ ۵: امشب در این بساط به ما عشرت شهی است

<div class="b" id="bn1"><div class="m1"><p>امشب در این بساط به ما عشرت شهی است</p></div>
<div class="m2"><p>کاقبالمان موافق و توفیق همرهی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعیم دوستان و حکایاتمان دراز</p></div>
<div class="m2"><p>این شب عدوی ماست که میلش به کوتهی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نغمه های چنگ شبانان به گوش ماست</p></div>
<div class="m2"><p>کی اعتنا به بانگ خروس سحرگهی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دهرمان موافقت، از کوکب اتفاق</p></div>
<div class="m2"><p>از چرخمان موافقت، از بخت همرهی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عاقلی به حلقه دیوانگان درآی</p></div>
<div class="m2"><p>کاین حلقه را نهادن و رفتن ز آگهی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را اگر چه بی خبری عادت آمده است</p></div>
<div class="m2"><p>لیکن به معنی همه اسرار آگهی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشی به صحبت همه از اشتیاق صبح</p></div>
<div class="m2"><p>گوش دگر همی به اذان سحرگهی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیر، آن کسی بود که سگ نفس را کشد</p></div>
<div class="m2"><p>ورنه اسیر نفس شدن عین روبهی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحر است طبع من «عمان » طبع ها چو جوی</p></div>
<div class="m2"><p>هر جوی عاقبت به سوی بحر منتهی است</p></div></div>