---
title: >-
    شمارهٔ ۷ - در مدح مولی الموالی حضرت اسدالله الغالب علی بن ابیطالب علیه السلام
---
# شمارهٔ ۷ - در مدح مولی الموالی حضرت اسدالله الغالب علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>دو هفته ماه من ای لعبت بهشتی رو</p></div>
<div class="m2"><p>دگر چه شد که ز من کرده ای تهی پهلو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سرو نازی و بر چشم منت باید جای</p></div>
<div class="m2"><p>که جای سرو بسی خوشتر است بر لب جو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تراست نازش کبک و چمیدن طاووس</p></div>
<div class="m2"><p>تراست صولت شیر و رمیدن آهو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زلف پیچان بنهاده ای دو صد نیرنگ</p></div>
<div class="m2"><p>به چشم فتان بنهفته ای دو صد جادو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی سراغ کنی از دلم گهی از تن</p></div>
<div class="m2"><p>به جان خود که تو واقف ترستی از هر دو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مراست یک تن و آن هم هلاک آن رخسار</p></div>
<div class="m2"><p>مراست یک دل و آن هم اسیر آن گیسو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو در خرامش و نازی و من ز فرقت تو</p></div>
<div class="m2"><p>ز ناله همچون نالم ز مویه همچون مو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن که آیی مخمور چشم و تافته زلف</p></div>
<div class="m2"><p>به ناز پرده برافکنده ز آن رخ نیکو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برای دلها زنجیر هشته از طره</p></div>
<div class="m2"><p>به قصد جان ها خنجر کشیده از ابرو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنان به تازی بر من که شیر بر نخجیر</p></div>
<div class="m2"><p>چنان بگیری بر دل که باز بر تیهو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز در و گوهر مملو کنی مرا کلبه</p></div>
<div class="m2"><p>ز مشک و عنبر مشحون کنی مرا مشکو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی به بالی بر خود به تابش رخسار</p></div>
<div class="m2"><p>همی به نازی بر من به پیچش گیسو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گهی بگویی، کولاله را بدینسان رنگ</p></div>
<div class="m2"><p>گهی بگویی، کو مشک را بدینسان بو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا بگویی گر منصفی بیا و ببین</p></div>
<div class="m2"><p>مرا به گویی گر منکری بگیر و به بو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی بگویی جامی شراب ناب بیار</p></div>
<div class="m2"><p>گهی بگویی مدحی ز بوتراب (ع) بگو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>علی(ع) امیر عرب پادشاه کشور دین</p></div>
<div class="m2"><p>که هست در خم چوگان او فلک چون گو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مروتش را زین نغزتر کجا برهان؟</p></div>
<div class="m2"><p>فتوتش را زین خوبتر دلیلی کو؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که داده در ره حق، گاه جوع، نان به فقیر؟</p></div>
<div class="m2"><p>که داد در سر دین روز فتح سر به عدو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرفت کشور دین را به ضربت شمشیر</p></div>
<div class="m2"><p>شکست پشت عدو را به قوت بازو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دست قدرت در بر گرفت از خیبر</p></div>
<div class="m2"><p>چنین بباید دست خدای را نیرو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به او اعادی گر کینه ور شدند چه غم</p></div>
<div class="m2"><p>کجا ز بانگ سگان شیر را رسد آهو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غلام درگه او گر غلام و گر خواجه</p></div>
<div class="m2"><p>کنیز مطبخ او گر کنیز و گر بانو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی به رأفت و الطاف بی کسان را یار</p></div>
<div class="m2"><p>خهی به رحمت و انصاف بیوگان را شو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز روی مدح تو امروز پرده برگیرم</p></div>
<div class="m2"><p>اگر چه نسبت کفرم دهند از هر سو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو آن عدیم عدیلی که بهر معرفتت</p></div>
<div class="m2"><p>هنوز آدم را سر به حیرت است فرو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکیت خواند از صدق اولین مخلوق</p></div>
<div class="m2"><p>یکیت گوید نی لا اله الا هو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خدات خوانده ولی مصطفات گفته وصی</p></div>
<div class="m2"><p>تو هم گزیده اویی و هم خلیفه او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هوا نبارد، گر گوییش به خشم نبار</p></div>
<div class="m2"><p>زمین نروید، گر گوییش به قهر نرو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من و مدیح تو وین عقل بینوا حاشا</p></div>
<div class="m2"><p>ز وضع خانه چه گوید که نیست ره در کو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز مهر جانب «عمان » ببین و شعر ترش</p></div>
<div class="m2"><p>که طعنه ها زده بر شعر خواجه و خواجو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ثنا و مدح ترا حد و حصر نیست ولیک</p></div>
<div class="m2"><p>ندید قافیه زین پیش طبع قافیه جو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همیشه تا که به سنگ و سبو زنند مثل</p></div>
<div class="m2"><p>هماره تا ز نفاق و وفاق آید بو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>موافقان تو دایم گرانبها چون سنگ</p></div>
<div class="m2"><p>منافقان تو دایم شکسته دل چو سبو</p></div></div>