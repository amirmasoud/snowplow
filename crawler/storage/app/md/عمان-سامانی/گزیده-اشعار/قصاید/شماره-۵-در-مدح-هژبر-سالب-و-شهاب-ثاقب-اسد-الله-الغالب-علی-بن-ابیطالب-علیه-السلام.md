---
title: >-
    شمارهٔ ۵ - در مدح هژبر سالب و شهاب ثاقب اسد الله الغالب علی بن ابیطالب علیه السلام
---
# شمارهٔ ۵ - در مدح هژبر سالب و شهاب ثاقب اسد الله الغالب علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>زندگانی چیست دانی؟ جان منور داشتن</p></div>
<div class="m2"><p>بوستان معرفت را تازه و تر داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عرش فرش پای کوب تست همت کن بلند</p></div>
<div class="m2"><p>تا کی از این خاکدان بالین و بستر داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگسل این دام هوس ای مرغ قدسی آشیان</p></div>
<div class="m2"><p>گر دو عالم بایدت در زیر شهپر داشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر دیناری کش از خاکست پذیرفتن وجود</p></div>
<div class="m2"><p>بهر دیبایی کش از کرمست گوهر داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مسلمان تا به کی خون مسلمان ریختن</p></div>
<div class="m2"><p>ای برادر تا به کی کین برادر داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه خالی کن ز کبر و آز و شهوت تا به کی</p></div>
<div class="m2"><p>خانه پر گندم نمودن کیسه پر زر داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به چند این نخوت و ناز و غرور و عجب و کبر</p></div>
<div class="m2"><p>از غلام و باغ و راغ و اسب و استر داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این سر غدار را تا کی نهفتن در کلاه</p></div>
<div class="m2"><p>وین تن مردار را تا کی به زیور داشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی کلاهانند اندر ساحت اقلیم عشق</p></div>
<div class="m2"><p>پای تا سر ننگ از خورشید افسر داشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرده هر نقشی و بی زحمت فکندن بر قلم</p></div>
<div class="m2"><p>خوانده هر درسی و بی منت ز دفتر داشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاشف راز درون از مژه آوردن به هم</p></div>
<div class="m2"><p>واقف سر ضمیر از لب ز هم برداشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کارشان بر روی نطع عاشقی پا کوفتن</p></div>
<div class="m2"><p>شغلشان در زیر تیغ دوستی سر داشتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی در و بامند، اما آسمان را آرزوست</p></div>
<div class="m2"><p>بر مثال حاجبانشان جای بر در داشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لب خموش اما نشایدشان سر هر موی را</p></div>
<div class="m2"><p>لحظه یی غافل ز ذکر نام حیدر(ع) داشتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شیر یزدان داور امکان خدیو دین علی(ع)</p></div>
<div class="m2"><p>کز وجود اوست دین را زینت و فر داشتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باید آن کس را که مهر او نباشد ای پدر</p></div>
<div class="m2"><p>اعتقاد او به ناپاکی مادر داشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شخص قدرش در تمام عالم کون و فساد</p></div>
<div class="m2"><p>سخت دلتنگست از جای محقر داشتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوش در معراج توصیفش براق فکر را</p></div>
<div class="m2"><p>کش بود در پویه ننگ از نام صرصر داشتن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوش همی راندم به تعجیلی که جبریل خرد</p></div>
<div class="m2"><p>ماند اندر نیم ره با آن همه پر داشتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا میان ممکن و واجب که دریاییست ژرف</p></div>
<div class="m2"><p>واجب آمد فلک جرئت را به لنگر داشتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عشق گفتا، رفرفم من بر نشین برتر خرام</p></div>
<div class="m2"><p>تا کی آخر رخت بر این کندرو، خر داشتن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حاجب وهمم گریبان سبکرایی گرفت</p></div>
<div class="m2"><p>گفت گستاخانه نتوان رو بر این در داشتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جز پس این پرده هر جا دست دست مرتضی است</p></div>
<div class="m2"><p>لیک بالاتر نشاید پا از این در داشتن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عشق گفتا ای گرانجان سبکسر لب ببند</p></div>
<div class="m2"><p>من توانم از میان این پرده را بر داشتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از پس این پرده دست او مگر نامد برون</p></div>
<div class="m2"><p>خواست چون در سفره شرکت با پیمبر داشتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زان زمان در حیرتستم کاین عجایب مظهریست</p></div>
<div class="m2"><p>تا کی آخر حیرت این پاک مظهر داشتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ممکن و در لامکان، جهل است کردن اعتقاد</p></div>
<div class="m2"><p>واجب و در خاکدان، کفر است باور داشتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عشق گوید هرچه می خواهی بیان کن باک نیست</p></div>
<div class="m2"><p>خوش نباشد سر ایزد را مستر داشتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عقل گوید حد نگهدار ای مسلمان زینهار</p></div>
<div class="m2"><p>می نیندیشی ز ننگ نام کافر داشتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فتنه خیزد، دست اگر خواهی بیاوردن فرود</p></div>
<div class="m2"><p>خون بریزد پای اگر خواهی فراتر داشتن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عشق گوید غایت کفر است با صدق مقال</p></div>
<div class="m2"><p>عاشقان را باکی از شمشیر و خنجر داشتن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عقل گوید تا به کی زین فکرت آشوب خیز</p></div>
<div class="m2"><p>دل مشوش ساختن خاطر مکدر داشتن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در بر اغیار سر حق مگو زشت است زشت</p></div>
<div class="m2"><p>پیش چشم کور، آیینه سکندر داشتن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای علی(ع) ای معدن جود و جلال و فضل و علم</p></div>
<div class="m2"><p>جز تو کس را کی رسد تیغ دو پیکر داشتن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جز تو کس را کی رسد در کعبه ای دست خدا</p></div>
<div class="m2"><p>بی محابا پای بر دوش پیمبر داشتن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فاش می خواندم خدایت در میان خاص و عام</p></div>
<div class="m2"><p>گر نبودی کفر مطلق شرک داور داشتن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من نمی گویم خدایی لیک بی توفیق تو</p></div>
<div class="m2"><p>باد برگی را نیارد از زمین برداشتن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من نمی گویم خدایی لیک بی امداد تو</p></div>
<div class="m2"><p>نطفه را صورت نبندد شکل جانور داشتن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من نمی گویم خدایی لیک می گردد پسر</p></div>
<div class="m2"><p>در رحم، زن را کنی گر منع دختر داشتن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من نمی گویم خدایی لیک باید خلق را</p></div>
<div class="m2"><p>بر کف تو چشم روزی را مقدر داشتن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>منکران را هم سر و کار اوفتد آخر به تو</p></div>
<div class="m2"><p>ناگزیر آمد رسن از ره به چنبر داشتن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نوح را کشتی به گرداب فنا بودی هنوز</p></div>
<div class="m2"><p>گر نه او را بودی از لطف تو لنگر داشتن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>طبع من از ریزش دست تو آرد شعر نغز</p></div>
<div class="m2"><p>زانکه «عمان » را ز باران است گوهر داشتن</p></div></div>