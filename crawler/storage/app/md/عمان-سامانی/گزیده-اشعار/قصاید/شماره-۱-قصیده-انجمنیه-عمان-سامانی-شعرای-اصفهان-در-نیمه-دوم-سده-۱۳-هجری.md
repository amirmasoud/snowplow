---
title: >-
    شمارهٔ ۱ - قصیده انجمنیه عمان سامانی شعرای اصفهان در نیمه دوم سده ۱۳ هجری
---
# شمارهٔ ۱ - قصیده انجمنیه عمان سامانی شعرای اصفهان در نیمه دوم سده ۱۳ هجری

<div class="b" id="bn1"><div class="m1"><p>دیگران را شور بستان بر سر و شوق چمن</p></div>
<div class="m2"><p>ما و رندان غزلخوان و فضای انجمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما هنر را اخترانیم انجمن ما را سپهر</p></div>
<div class="m2"><p>ما سخن را بلبلانیم انجمن ما را چمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندرآ، تا حوزه یی بینی پر از عقل و روان</p></div>
<div class="m2"><p>اندرآ، تا روضه یی بینی پر از سرو و سمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محفلی آماده در وی هم محبت هم صفا</p></div>
<div class="m2"><p>مجمعی آواره از وی هم تکلف هم محن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک طرف گرم غزلخوانی ظریفان جوان</p></div>
<div class="m2"><p>یک طرف مست سخنرانی حریفان کهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله دانایان حکمت جمله دارایان فضل</p></div>
<div class="m2"><p>جمله اربابان دانش جمله استادان فن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمله ماه ذوق و این فرخنده محفلشان سپهر</p></div>
<div class="m2"><p>جمع شمع ذوق و این زیبنده مجلشان لگن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاحب این انجمن پیری که از بس روشنی</p></div>
<div class="m2"><p>آفتاب اندر بر رایش نیارد دم زدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارفی از پای تا سر حلم و تسلیم و رضا</p></div>
<div class="m2"><p>فاضلی از فرق تا پا دانش و فضل و فطن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان فقیران را پدر خوانندش ارباب هنر</p></div>
<div class="m2"><p>که پرستار فقیران است در سر و علن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندر این ملکش به عرفان نیست همتا، هم مگر</p></div>
<div class="m2"><p>بوالوفا از کرد خیزد یا اویس اندر قرن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او به صدر انجمن مانند مه بر آسمان</p></div>
<div class="m2"><p>دیگران بر گرد او صف بسته چون عقد پرن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن یکی مسکین و در گنجینه طبعش نهان</p></div>
<div class="m2"><p>هر چه لعل اندر بدخشان است و در اندر عدن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عارفی فرخنده کار است و اریبی هوشمند</p></div>
<div class="m2"><p>فاضلی کامل عیار است و ادیبی ممتحن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وان دگر پرتو که خورشیدیست با فر و شکوه</p></div>
<div class="m2"><p>ز آسمان مردمی در انجمن پرتو فکن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیبد این شعر منوچهری به وصف حضرتش</p></div>
<div class="m2"><p>گرچه او خود گفته در مدح ابوالقاسم حسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>«شعر او فردوس را ماند که اندر شعر اوست</p></div>
<div class="m2"><p>هرچه در فردوس ما را وعده داده ذوالمنن »</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>او چو مسکین در هنر مسکین چو او در مردمی</p></div>
<div class="m2"><p>این چو آن یک هوشیار و آن چو این یک مهر تن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر دو پنداری که یک جانند پنهان در دو جسم</p></div>
<div class="m2"><p>یا یکی جسمند ظاهر گشته در دو پیرهن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وان دگر افسر که می شاید به بازار سخن</p></div>
<div class="m2"><p>رشته اشعار او را گوهر جانها ثمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاطرش را هر چه اندر روضه باغ ارم</p></div>
<div class="m2"><p>خامه اش را هر چه اندر ناف آهوی ختن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وان دگر باشد بقا کز نظم چون آب بقا</p></div>
<div class="m2"><p>غم همی بزداید از دل جان همی بخشد به تن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هی عبیر آرد، به دفتر چونکه برگیرد قلم</p></div>
<div class="m2"><p>هی گهر ریزد، به محفل چونکه بگشاید دهن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وان دگر پور هما عنقا که در دانشوری</p></div>
<div class="m2"><p>هست زیر شهپرش از قاف تا قاف سخن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اندر آن گلشن که او شد بلبل دستانسرای</p></div>
<div class="m2"><p>حیف باشد استماع نغمه زاغ و زغن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>وان دگر سرگشته آن بسحاق دانشور که هست</p></div>
<div class="m2"><p>شعر روح افزاش حلوای مذاق مرد و زن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دکه قناد را ابیات او مقدار کاه</p></div>
<div class="m2"><p>کلبه طباخ را اشعار او رونق شکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وان دگر آشفته کز اشعار نغز دلنشین</p></div>
<div class="m2"><p>می برد آشفتگی بیرون ز طبع انجمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وان دگر فرخ که از فرخندگی ماند، به عید</p></div>
<div class="m2"><p>وان دگر ساغر که از صاحبدلی ماند، به دن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وان دگر پروین که چون اشعار شیرین سرکند</p></div>
<div class="m2"><p>در مذاق طبع آمیزد، به هم شهد و لبن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وان دگر دهقان که اندر مزرع دفتر ز نظم</p></div>
<div class="m2"><p>پرورد پیوسته نسرین و شقیق و یاسمن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>وان دگر شعری و شعرش با شرافت توأمان</p></div>
<div class="m2"><p>وان دگر جوزا و طبعش با سعادت مقترن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گوید آن یک، چشم شوخ من همه غنج و فریب</p></div>
<div class="m2"><p>گوید این یک، شاه من دارد قدی چون نارون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گوید این یک ماه من دارد لبی چون ناردان</p></div>
<div class="m2"><p>گوید این یک شاه من دارد قدی چون نارون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>من همی خوانم مدیح صاحب کیهان مدار</p></div>
<div class="m2"><p>خواجه فرخ «امین الدوله » میر مؤتمن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بنده «عمانم » سلیل قطره آن استاد نظم</p></div>
<div class="m2"><p>شیوه ام مدح تو سامان صفاهانم وطن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا کنم مدح تو کلکم هست ابری سیل بار</p></div>
<div class="m2"><p>تا کنم وصف تو طبعم هست بحری موج زن</p></div></div>