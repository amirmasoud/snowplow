---
title: >-
    شمارهٔ ۸ - در ستایش دو علت ایجاد و دو مایه خلقت بنی آدم النبی الاکرم و الولی الافخم صلوات الله علیهما
---
# شمارهٔ ۸ - در ستایش دو علت ایجاد و دو مایه خلقت بنی آدم النبی الاکرم و الولی الافخم صلوات الله علیهما

<div class="b" id="bn1"><div class="m1"><p>بزرگ مایه ایجاد قادر ازلی</p></div>
<div class="m2"><p>ز نور پاک جمال محمد است و علی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نور پاک جمال محمد(ص) است و علی(ع)</p></div>
<div class="m2"><p>بزرگ مایه ایجاد قادر ازلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو دست کار کنند این دو دستیار وجود</p></div>
<div class="m2"><p>از این دو دست قوی دستگاه لم یزلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صورتند دو لیکن به معنی اند یکی</p></div>
<div class="m2"><p>مبینشان دو که باشد دو بینی از حولی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوب حلقه طاعت در مدینه علم</p></div>
<div class="m2"><p>کننده در خیبر به بازوان یلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در گشوده شد آنگه به شهر یابی راه</p></div>
<div class="m2"><p>بلی، بری به نبی راه با ولای ولی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبی کند ز ولی قصه چون گلاب ز گل</p></div>
<div class="m2"><p>ببو به صدق و رها کن طبیعت جعلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانه گر چه سر ابتذال دین دارد</p></div>
<div class="m2"><p>چگونه غیرت حق تن دهد به مبتذلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفتم آنکه شود در زمانه منکر نور</p></div>
<div class="m2"><p>عنان دل سوی ظلمت کشاند از دغلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آفتاب فروزان ز شرق کرد طلوع</p></div>
<div class="m2"><p>شود چه عاید خفاش غیر منفعلی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود محال کزین بادها فرو میرد</p></div>
<div class="m2"><p>چراغ طلعت حق، با کمال مشتعلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدیو آیین یعسوب دین که چرخ برین</p></div>
<div class="m2"><p>بر رهیش ز خورشید دوخته حللی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نسیم تربیت او بود که در مه و سال</p></div>
<div class="m2"><p>کند به باغ گهی عقربی گهی حملی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شراب تقویت او بود که در شب و روز</p></div>
<div class="m2"><p>کند به کام گهی حنظلی گهی عسلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بندگی طلبید از فلک دو دست قبول</p></div>
<div class="m2"><p>به سینه زد که لک الحکم والا طاعة لی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنند هر دو ز یاقوتی ادعا لیکن</p></div>
<div class="m2"><p>چه مایه فرق که از اصلی است تا بدلی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو کوکبند فروزنده لیک چندین فرق</p></div>
<div class="m2"><p>میان عالم برجیسی است با زحلی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به جز ولایت او قصد حق نبد ز الست</p></div>
<div class="m2"><p>به کاینات که گفتند در جواب بلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شها مدیح تو واجب شده است «عمان » را</p></div>
<div class="m2"><p>ز جان و دل نه به ذکر خفی و بانگ جلی</p></div></div>