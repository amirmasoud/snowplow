---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تاری از طره تارت چو به تاتار افتاد</p></div>
<div class="m2"><p>روز بر مشک فروشان خطا تار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر پنهان ترا فاش نسازم زین رو</p></div>
<div class="m2"><p>شایدم بار دگر با تو سر و کار افتاد</p></div></div>