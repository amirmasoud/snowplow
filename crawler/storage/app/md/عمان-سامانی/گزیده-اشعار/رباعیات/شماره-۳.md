---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا به کف تاری از آن طره طرار افتاد</p></div>
<div class="m2"><p>طالع بخت مرا همچو شب تار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه رخسار تو زد آتش حرمان بر دل</p></div>
<div class="m2"><p>قسمت ما ز گلستان رخت خار افتاد</p></div></div>