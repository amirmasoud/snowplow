---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>مرد چون قلب نام خویش اندوخت</p></div>
<div class="m2"><p>اندر این دور محتشم گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از این رمز گشت خالی مرد</p></div>
<div class="m2"><p>فتح نامش ز غصه ضم گردد</p></div></div>