---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>دلبری دارم و هر کس که بود همنامش</p></div>
<div class="m2"><p>دولتی گردد و از بخت برآید کامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنج حرف است و شمارش صد و پنجاه بود</p></div>
<div class="m2"><p>به حساب جمل ار جمع کنی ارقامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چار از آن هست یکی جامه دیبای نفیس</p></div>
<div class="m2"><p>که مهین همه افلاک بود همنامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلب گردان و بر او ثلث ز اصلش بفزای</p></div>
<div class="m2"><p>نه در آغاز ولی در وسط و انجامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ندانست کسی ماند ز وصلش محروم</p></div>
<div class="m2"><p>وانکه دانست فتد مرغ هوا در دامش</p></div></div>