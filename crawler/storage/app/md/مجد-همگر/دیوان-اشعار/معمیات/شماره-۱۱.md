---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>فرموده اقتراحی صاحب علاءالدین</p></div>
<div class="m2"><p>آنکو به کلک کار جهان را دهد نظام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نیک رای و رسم که رایش بود رهی</p></div>
<div class="m2"><p>وان نیک بخت خواجه که بختش بود غلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکش جهان جافی و ایام شد مطیع</p></div>
<div class="m2"><p>وانکش سپهر سرکش و اجرام گشت رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طبع تند و تیز گرفتم به ارتحال</p></div>
<div class="m2"><p>از روی لطف همچو هوا قطره از غمام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن چار حرف چیست که عقد مبارکش</p></div>
<div class="m2"><p>باشد به حصر چون مور مصحف کلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف نخست نیمه حرف چهارم است</p></div>
<div class="m2"><p>وز حرف چارمش سومش ده یکی تمام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی نی ز وصف او نبود چاره طبع را</p></div>
<div class="m2"><p>زانسان که نیست چاره ازو در همه مقام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه صبی و گاه نمویست خوش لقا</p></div>
<div class="m2"><p>روز شباب و روز مشیب است نیکنام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چند هست خرد بزرگ است و بابها</p></div>
<div class="m2"><p>محبوب اهل و جاهل و مقبول خاص و عام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ابر رجم یابد و از آفتاب مهر</p></div>
<div class="m2"><p>ز آهن درود بیند و از سنگ انتقام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فی الجمله دایه ئیست مربی بوالبشر</p></div>
<div class="m2"><p>فی القصه دانه ئیست که ابلیس راست دام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ماراست زین مطاع گرانمایه مبلغی</p></div>
<div class="m2"><p>در ذمت وکیل وزیر کریم وام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر داد گشت گردنش آزاد و کرد داد</p></div>
<div class="m2"><p>ورنه به روز عرض مظالم بود غرام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قاضی بود خدای و رسولش بود وکیل</p></div>
<div class="m2"><p>زندان معین است و عدو را بود مقام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>باشد خدای ناصر ایام مستقیم</p></div>
<div class="m2"><p>خلقش ز خلق شاکر و اقبال مستدام</p></div></div>