---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>نام یارم کزو دلم درواست</p></div>
<div class="m2"><p>آنکه چون سرو راست قامت خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چار حرف است و در حساب جمل</p></div>
<div class="m2"><p>آمد از شصت و پنج بی کم و کاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه از یازده برانداز آنگه</p></div>
<div class="m2"><p>نه و مقلوب باشد و دوراست</p></div></div>