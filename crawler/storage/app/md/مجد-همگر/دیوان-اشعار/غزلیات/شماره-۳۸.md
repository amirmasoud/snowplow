---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>سر آن ندارد این دل که ز عشق سر ندارد</p></div>
<div class="m2"><p>سر عشق می نگیرد به خودم نمی گذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تنوره ئیست ز آتش تن من ز گرمی دل</p></div>
<div class="m2"><p>خنک آن تنی ست باری که دلی چنین ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از سر هوائی نفسی زنم به شادی</p></div>
<div class="m2"><p>دل غم پرست بر من همه شادئی سرآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و مجلس غم اکنون که ز بزم شادمانی</p></div>
<div class="m2"><p>نه دلم همی گشاید نه میم همی گوارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر عاشقی ندارم به خدا ولیکن این دل</p></div>
<div class="m2"><p>همه راه عشق پوید همه تخم مهر کارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جگرم گداخت وآمد ز ره دو دیده بیرون</p></div>
<div class="m2"><p>چکند دو دیده اکنون که سرشک خون نبارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چکنم که راز عشقت ز کسی نهفته دارم</p></div>
<div class="m2"><p>که سرشک خون به سرخی همه بر رخم نگارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفسی که می شمارم ز شمار زندگی نیست</p></div>
<div class="m2"><p>به چنین صفت مرا خود که ز زندگان شمارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه امید در تو بندد تن و جان و من چو چشمت</p></div>
<div class="m2"><p>لطفی نمی نماید نظری نمی گمارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز تو چشم حقگذاری دل بنده خود ندارد</p></div>
<div class="m2"><p>که تو خود دلی نداری که حق کسی گذارد</p></div></div>