---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>دیدی که مهر ما به چسان خوار داشتی</p></div>
<div class="m2"><p>عهد مرا چگونه سبکبار داشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدی که تا زمهر تو تیمار داشتم</p></div>
<div class="m2"><p>ما را چگونه در غم و تیمار داشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا داشتم چو جان و دلت داشتم عزیز</p></div>
<div class="m2"><p>تا داشتی چو خاک رهم خوار داشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس روز تا شبم ز خور و خواب بستدی</p></div>
<div class="m2"><p>بس شب که تا به روزم بیدار داشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاول که لاف دوستی و مهر ما زدی</p></div>
<div class="m2"><p>با ما نه زین صفت سرآزار داشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نقطه وفای تو درسینه داشتم</p></div>
<div class="m2"><p>سرگشته ام به صورت پرگار داشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکچند داشتی دل و پس کشتیش به جور</p></div>
<div class="m2"><p>چندین زمانش از پی این کار داشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امسال با که داری کز ما بریده ای</p></div>
<div class="m2"><p>آن رغبتی که در حق ما پار داشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خاطرت نیامد کآخر به عمر خویش</p></div>
<div class="m2"><p>بیچاره ای شکسته دلی یار داشتی</p></div></div>