---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>در عشق هیچ درد چو درد فراق نیست</p></div>
<div class="m2"><p>بر دل غمی بتر ز غم اشتیاق نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من مخواه صبر و مفرمای دوریم</p></div>
<div class="m2"><p>کم طاقت صبوری و برگ فراق نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عشق طاق ابروی آن جفت نرگست</p></div>
<div class="m2"><p>یک دل به من نمای که از صبر طاق نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که وصل ما و ترا اتفاق هست</p></div>
<div class="m2"><p>ما متفق شدیم و ترا اتفاق نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری چو حلقه بر در وصل تو سر زدیم</p></div>
<div class="m2"><p>عشقت جواب داد که کس در وثاق نیست</p></div></div>