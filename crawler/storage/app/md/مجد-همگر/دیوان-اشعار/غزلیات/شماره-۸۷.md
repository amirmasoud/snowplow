---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>این چه ننگ است که بر روی چو ماه آوردی</p></div>
<div class="m2"><p>وین چه رنگ است که از خط سیاه آوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیرگه بود که از مشک رخت خالی داشت</p></div>
<div class="m2"><p>لیکن این عنبر گلپوش به گاه آوردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گل عارض تو مهر دلم خود بس نیست</p></div>
<div class="m2"><p>که شفیعی دگر از مهر گیاه آوردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی چون توبه و ایمانت جهان روشن داشت</p></div>
<div class="m2"><p>گردش از بوالعجبی کفر و گناه آوردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دعوی حسن تو ثابت شد و محتاج نبود</p></div>
<div class="m2"><p>کز پی شاهد بس خط گواه آوردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شب زلف تو بیچاره دلم گمره بود</p></div>
<div class="m2"><p>از رخ همچو مهش باز به راه آوردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیشکر را کمر از چنبر زرین بستی</p></div>
<div class="m2"><p>خرمن سیم مه از بند کلاه آوردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخ تو باغ بهار است که نوباوه او</p></div>
<div class="m2"><p>پیش بزم ملک ملک پناه آوردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گل و سنبل و شمشاد و بنفشه به صبوح</p></div>
<div class="m2"><p>دسته بستی و سوی مجلس شاه آوردی</p></div></div>