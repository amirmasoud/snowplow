---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>اگر به صبر مرا با تو چاره باید کرد</p></div>
<div class="m2"><p>دلم صبورتر از سنگ خاره باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و گر ز جور کند جامه پاره مظلومی</p></div>
<div class="m2"><p>مرا ز جور تو صدجان نثاره باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو جورهای نهان می کنی و ترسم از آن</p></div>
<div class="m2"><p>که راز عشق توام آشکاره باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به ترک تو گفتن ز دل اجازت نیست</p></div>
<div class="m2"><p>نخست با دل ریش استخاره باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روامدار که با اینهمه امید مرا</p></div>
<div class="m2"><p>ز دور در تو به حسرت نظاره باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که بحر غمت را کرانه نیست پدید</p></div>
<div class="m2"><p>ز غرقه گاه هلاکم کناره باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یادگار رخت قبله ای به دست آرم</p></div>
<div class="m2"><p>گرم پرستش ماه و ستاره باید کرد</p></div></div>