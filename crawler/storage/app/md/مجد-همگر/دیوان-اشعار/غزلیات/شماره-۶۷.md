---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>امید از وصل تو نتوان بریدن</p></div>
<div class="m2"><p>کمان عشق تو نتوان کشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا روزی شود وصل تو لیکن</p></div>
<div class="m2"><p>ندانم کی به من خواهد رسیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یقینم کز چنان شیرین زبانی</p></div>
<div class="m2"><p>جواب تلخ می باید شنیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روا نبود ز من جستن جدائی</p></div>
<div class="m2"><p>خطا باشد ز تو دوری گزیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین سربازتر عاشق نیابی</p></div>
<div class="m2"><p>ازین بیچاره سر وز تو بریدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی بوسه بده گر می توانی</p></div>
<div class="m2"><p>تن و جانی به یک بوسه خریدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وفای تو زمن می نگسلد مهر</p></div>
<div class="m2"><p>وصال تو زمن تا کی رمیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دوری راضیم از تو که چشمم</p></div>
<div class="m2"><p>ندارد طاقت روی تو دیدن</p></div></div>