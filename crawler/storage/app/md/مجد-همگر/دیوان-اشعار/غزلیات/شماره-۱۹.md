---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>یا ترک من بی دل غمخوار بگوئید</p></div>
<div class="m2"><p>یا حال من دلشده با یار بگوئید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا از من و از غصه من یاد نیارید</p></div>
<div class="m2"><p>یا قصه در دم بر دلدار بگوئید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تنگ دهانی که لبش داروی جانهاست</p></div>
<div class="m2"><p>حال مرض این دل بیمار بگوئید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرح دل محروم ستم یافته من</p></div>
<div class="m2"><p>با آن دل بی حم ستمکار بگوئید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بار بگفتید و نیاورد ترحم</p></div>
<div class="m2"><p>تا بو که کند رحم دگر بار بگوئید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد که دوای دل رنجور بیابم</p></div>
<div class="m2"><p>حال من و او بر سر بازار بگوئید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خلق بدانند جفا کاری یارم</p></div>
<div class="m2"><p>در مجمع یاران وفادار بگوئید</p></div></div>