---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ای ز بوس تو نقل من بسیار</p></div>
<div class="m2"><p>وزکنار تو کار من چو نگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کنارم نشین و بوسه بده</p></div>
<div class="m2"><p>که ز تو قانعم به بوس و کنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خط دوستی مشو بیرون</p></div>
<div class="m2"><p>طوف کن گرد خویش دایره وار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان نوازی به لعل شکرریز</p></div>
<div class="m2"><p>دلربائی به زلف عنبر بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رهت گر روی چو باد آید</p></div>
<div class="m2"><p>برتوان چید عنبر بسیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست من برسر است و بر سر من</p></div>
<div class="m2"><p>دامنی می کشی نه بر هنجار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش من بر در است و دست جفات</p></div>
<div class="m2"><p>حلقه وارم نشانده بر در زار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب آن بخت باشدم که شبی</p></div>
<div class="m2"><p>دامن تو به دستم افتد خوار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا بود دولتم که آن خم زلف</p></div>
<div class="m2"><p>حلقه گوش من شود یکبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا من آوازه در جهان فکنم</p></div>
<div class="m2"><p>از سر عشرتی سنائی وار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین سپس دست ما و دامن دوست</p></div>
<div class="m2"><p>پس ازین گوش ما و حلقه یار</p></div></div>