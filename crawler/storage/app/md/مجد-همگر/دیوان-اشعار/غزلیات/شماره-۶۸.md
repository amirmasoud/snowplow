---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>جانا به صبوح خرمی کن</p></div>
<div class="m2"><p>با ما به نشاط همدمی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایام بهار خرم آمد</p></div>
<div class="m2"><p>ای باغ بهار خرمی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک لحظه فراغت دل خویش</p></div>
<div class="m2"><p>در کار دل من غمی کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پرده دری چو صبح تا چند</p></div>
<div class="m2"><p>یکچند چو شام محرمی کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خاک رهم تو آفتابی</p></div>
<div class="m2"><p>آخر نظری سوی زمی کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا چون پری از نظر نهان شو</p></div>
<div class="m2"><p>یا میل به خوی آدمی کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای داروی درد دردمندان</p></div>
<div class="m2"><p>با این دل ریش مرهمی کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه مردمی از جهان برافتاد</p></div>
<div class="m2"><p>ای مردم دیده مردمی کن</p></div></div>