---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>کسی که بر لب لعل تو کامرانی یافت</p></div>
<div class="m2"><p>چو خضر تا به ابد عمر جاودانی یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنکه دید رخت جان آشکارا دید</p></div>
<div class="m2"><p>هر آنکه یافت لبت آب زندگانی یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آنکه رسته دندان چون در تو گزید</p></div>
<div class="m2"><p>زجزع دامن پر لعل های کانی یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که بر گل رخسار تو فکند نظر</p></div>
<div class="m2"><p>کنار خویش پر از اشک ارغوانی یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا مگرد پی وصل ملک ناممکن</p></div>
<div class="m2"><p>که رایگان نتوان گنج شایگانی یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب از هوای لبت باد سرد و حسرت دید</p></div>
<div class="m2"><p>دل از لقای رخت داغ لن ترانی یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لبش مجوی که کام سکندر است و چو زو</p></div>
<div class="m2"><p>نیافت کام سکندر تو چون توانی یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وصل خویشم برنا و چشم روشن کن</p></div>
<div class="m2"><p>کزین توانی اقبال آسمانی یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زندگانی باقی رسان مرا به شبی</p></div>
<div class="m2"><p>کزین توانم کام از جهان فانی یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بوی جامه نه پیری ضریر برنا شد</p></div>
<div class="m2"><p>شب وصال نه زالی ز سر جوانی یافت</p></div></div>