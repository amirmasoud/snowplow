---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>کجا از دوزخ اندیشد تنی کز مهر تو سوزد</p></div>
<div class="m2"><p>چرا یاد بهشت آرد دلی کز مهرت افروزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر افلاطون شود زنده شود شیدا و چون بنده</p></div>
<div class="m2"><p>ز عشق آن لب و خنده زلفظش ابجد آموزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روا باشد به جان تو که در دور زمان تو</p></div>
<div class="m2"><p>دل نامهربان تو ز جانم وام کین تو زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل آزارا جگر سوزا بسا شبها بسا روزا</p></div>
<div class="m2"><p>دلم با عشق جان سوزا به راهت دیده بر دوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وصلت گر شوم خرم مگر یکدم ز نم بی غم</p></div>
<div class="m2"><p>بسا شادی کزان یکدم دل پر دردم اندوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم کز رنج بیداری به روز آرم شب تاری</p></div>
<div class="m2"><p>بدین خواری بدین زاری دلت بر من نمی‌سوزد</p></div></div>