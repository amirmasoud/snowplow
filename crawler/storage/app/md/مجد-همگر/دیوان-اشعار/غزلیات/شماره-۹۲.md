---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>پیداست خود که نیست ترا رای آشتی</p></div>
<div class="m2"><p>زیرا که گم شده ست سروپای آشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تافتی ز مهر و وفا روی دل چنانک</p></div>
<div class="m2"><p>نه روی صلح داری و نه رای آشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ما دلت به کینه چنان مشتغل شده ست</p></div>
<div class="m2"><p>کو نیز خود ندارد پروای آشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردوختی به کینه وری چشم مردمی</p></div>
<div class="m2"><p>نگذاشتی به حیله گری جای آشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با اینهمه جفا که تو کردی به جان من</p></div>
<div class="m2"><p>دل می کند هنوز تمنای آشتی</p></div></div>