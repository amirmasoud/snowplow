---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>آخر شبی ز لطف سلامی به ما فرست</p></div>
<div class="m2"><p>روزی به دست باد پیامی به ما فرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تشنگی وصل تو جانم به لب رسید</p></div>
<div class="m2"><p>از لعل آب دار تو جامی به ما فرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در روزه فراق تو شد شام صبح من</p></div>
<div class="m2"><p>از خوان وصل لقمه شامی به ما فرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن مرغ نادرم که غمت دانه من است</p></div>
<div class="m2"><p>چون دانه‌ام نمودی دامی به ما فرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وان هندویم که کنیت خاصم غلام تست</p></div>
<div class="m2"><p>ای ترک شوخ نام غلامی به ما فرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر رد شدم ز بند غم آزاد کن مرا</p></div>
<div class="m2"><p>ور کرده‌ای قبولم نامی به ما فرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر توانگری ز وصال و جمال خویش</p></div>
<div class="m2"><p>درویشم و نگه کن و وامی به ما فرست</p></div></div>