---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>آن روی چون بهارت رشک نگار چینی</p></div>
<div class="m2"><p>جانم ز مهر رویت شد لحظه درد چینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رضوان گرت ببیند آراسته بدینسان</p></div>
<div class="m2"><p>در تو به تهمت افتد گوید که حور عینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ دلنوازی شمشاد تازه روئی</p></div>
<div class="m2"><p>بر چرخ خوبروئی خورشید مه جبینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بام چون خرامی سروی که بر سپهری</p></div>
<div class="m2"><p>چون در چمن نشینی ماهی که بر زمینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم جهانت خوانم دل گفت نی که جان است</p></div>
<div class="m2"><p>چون نیک می ببینم هم آنی و هم اینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من سر فدات کردم از بس نیازمندی</p></div>
<div class="m2"><p>تو سر فرو نیاری از بس که نازنینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاده ام به مهرت دستم چرا نگیری</p></div>
<div class="m2"><p>جان داده ام به بویت با من چرا به کینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من وصل جویم از تو تو صبر جوئی از من</p></div>
<div class="m2"><p>من آن ز تو نیابم تو این ز من نبینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دی با دلم غمت گفت کز وی اثر نیابی</p></div>
<div class="m2"><p>گر بر امید وصلش عمری دگر نشینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او با تو در نسازد تو در غمش چه سوزی</p></div>
<div class="m2"><p>فرسوده گردی از غم گر کوه آهنینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من زین جهان گزیدم کنجی به نامراد</p></div>
<div class="m2"><p>تا در جهان بر آمد نامم به به گزینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با اینهمه ندارد کس در جهان به جز من</p></div>
<div class="m2"><p>طبعی بدین لطیفی شعری بدین متینی</p></div></div>