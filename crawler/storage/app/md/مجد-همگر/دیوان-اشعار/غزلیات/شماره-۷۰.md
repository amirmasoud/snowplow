---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>مردمان گوش کنید انده تنهائی من</p></div>
<div class="m2"><p>رحمت آرید دمی بر دل شیدائی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ازین داشتم آسوده دلی گوشه نشین</p></div>
<div class="m2"><p>که شب و روز بدی مونس تنهائی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دلی خرم و آراسته کاندر همه عمر</p></div>
<div class="m2"><p>بود ازو خرم و آراسته برنائی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان عشق چنان تاختنی کرد بر او</p></div>
<div class="m2"><p>که به تاراج بشد جمله شکیبائی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایه افکند بر او مهر مهی کزوی شد</p></div>
<div class="m2"><p>زیر پی سایه صفت دولت بالائی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسفی تختگه مصر دلم را بگرفت</p></div>
<div class="m2"><p>که ندارد خبر از درد زلیخائی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاره کار چنین عشق ندانم چکنم</p></div>
<div class="m2"><p>سر این کار برون است زدانائی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نصیحت ز سرم دور نگردد سودا</p></div>
<div class="m2"><p>که نصیحت نپذیرد دل سودائی من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بسی عاشق رسوا به جهان در دیدم</p></div>
<div class="m2"><p>کس نبوده ست در این شیوه به رسوائی من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم من جز به رخ یار نبیند عالم</p></div>
<div class="m2"><p>از رخ اوست مگر مایه بینائی من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از بس آمد شدنم مردم کویش شده اند</p></div>
<div class="m2"><p>سرگران بر من مسکین ز سبک پائی من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رای من خود همه آنست که گیرم کم دل</p></div>
<div class="m2"><p>تا چه آرد به سرم عادت خودرائی من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درد تنهائی اثر بر رخ من پیدا کرد</p></div>
<div class="m2"><p>آه ازین محنت تنهائی و پیدائی من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناتوان کرد دلم را غم او چتوان کرد</p></div>
<div class="m2"><p>که نه درخورد غم اوست توانائی من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همچو ابریشم یکتاست مرا ناله حزین</p></div>
<div class="m2"><p>پشت من کرد دوتا ناله یکتائی من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قصه زاری این عشق به هر جا برسید</p></div>
<div class="m2"><p>وه که چون شهره شد این قصه هر جائی من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تلخی هجر چو جانم به لب آورد چه سود</p></div>
<div class="m2"><p>شور شیرین سخنی لاف شکرخائی من</p></div></div>