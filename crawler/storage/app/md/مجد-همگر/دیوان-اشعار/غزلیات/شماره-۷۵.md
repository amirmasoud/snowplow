---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>روح محضی بدان طربناکی</p></div>
<div class="m2"><p>قطره ژاله ای بدان پاکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ببیند طراوت تو بهار</p></div>
<div class="m2"><p>نکند دعوی طربناکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چمن گر قد تو جلوه کند</p></div>
<div class="m2"><p>نزند سرو لاف چالاکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ملاحت برون ز اوهامی</p></div>
<div class="m2"><p>به حلاوت فزون ز ادراکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جفا پایمرد ایامی</p></div>
<div class="m2"><p>در ستم دستیار افلاکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه خونخواری و شکستن عهد</p></div>
<div class="m2"><p>بس دلیری و سخت ناپاکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مار زلف تو زخم زد بر دل</p></div>
<div class="m2"><p>که کند جز لب تو تریاکی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک پای توام چه باشد اگر</p></div>
<div class="m2"><p>سایه ای افکنی بر این خاکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به درازی کشید فرقت ما</p></div>
<div class="m2"><p>طال شوقی الی محیا کی</p></div></div>