---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>غم از دلم به جدائی جدا نمی گردد</p></div>
<div class="m2"><p>دلم ز بند ملامت رها نمی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مرا ز غم عشق سرزنش مکنید</p></div>
<div class="m2"><p>که دل به سرزنش از عشق وا نمی گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ذره ئیست دل من هوا پرست از مهر</p></div>
<div class="m2"><p>که ذره ای ز هوائی جدا نمی گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه من به تن خویش پارسا گردم</p></div>
<div class="m2"><p>که پادشاه تنم پارسا نمی گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به گرد بلاگشت و مبتلا شد او</p></div>
<div class="m2"><p>چه خوش دلی که به گرد بلا نمی گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر که بر غم او تنگ شد جهان فراخ</p></div>
<div class="m2"><p>که جز به گرد دل تنگ ما نمی گردد</p></div></div>