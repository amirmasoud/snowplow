---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>عید آمد ای نگارین بردار جام باده</p></div>
<div class="m2"><p>و زبند غم برون شو تا دل شود گشاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهد صبوح نوکن جام می کهن ده</p></div>
<div class="m2"><p>کم کن به عیش شیرین تلخی جام باده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوئی شبی ببینم من شادمان نشسته</p></div>
<div class="m2"><p>تو همچو شمع رخشان در مجلس ایستاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو قصد رقص کرده من عزم پای بوست</p></div>
<div class="m2"><p>تو دست من گرفته من در پی ات فتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو مست عیش گشته من مست شور عشقت</p></div>
<div class="m2"><p>تو کام خویش رانده من داد وصل داده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو نرم کرده دل را من دل دلیر کرده</p></div>
<div class="m2"><p>زلف کجت گرفته لب بر لبت نهاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آیا بود که روزی در دست نرد وصلت</p></div>
<div class="m2"><p>نقشی چنین برافتد با آن نگار ساده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای چنگی نواگر برگوی این غزل را</p></div>
<div class="m2"><p>بهر صبوح عیدی در بزم شاهزاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاهی که چون رخ آرد در کارزار چون پیل</p></div>
<div class="m2"><p>در پای اسبش افتد خورشید چون پیاده</p></div></div>