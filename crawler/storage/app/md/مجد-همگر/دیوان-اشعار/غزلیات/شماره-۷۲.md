---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>بیا کز جان و دل به در خوری تو</p></div>
<div class="m2"><p>بیا کز زندگانی خوشتری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قد سروی به بر نسرین به رخ گل</p></div>
<div class="m2"><p>مگر باغ و بهاری دیگری تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نوری چرا از دیده دوری</p></div>
<div class="m2"><p>وگرناری چرا جان پروری تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا جانی که در جسمم نهانی</p></div>
<div class="m2"><p>مرا چشمی که در من ننگری تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من سوزانم از هجر و گدازان</p></div>
<div class="m2"><p>چه سودم زانکه شمع و شکری تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خاکی نیم درخورد تو لیک</p></div>
<div class="m2"><p>مرا چون جان شیرین درخوری تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهشتت کو اگر حور بهشتی</p></div>
<div class="m2"><p>مقامت کو اگر هستی پری تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خواهندت کجا جویم نشانت</p></div>
<div class="m2"><p>نگوئی کز کدامین کشوری تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندانم کیستی زینها که گفتم</p></div>
<div class="m2"><p>مگر معشوق مجد همگری تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطا گفتم نئی او را ولیکن</p></div>
<div class="m2"><p>ندیم بزم شاه صفدری تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اتابک آنکه گردون قدر او را</p></div>
<div class="m2"><p>همی گوید منم پای و سری تو</p></div></div>