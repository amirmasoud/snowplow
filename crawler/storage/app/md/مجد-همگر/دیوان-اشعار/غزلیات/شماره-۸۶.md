---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>گر با تو زبانی شودم هر سر موئی</p></div>
<div class="m2"><p>وز جور تو نالم به تو بر هر سر کوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناز و جفا هم نکنی یک سرمو کم</p></div>
<div class="m2"><p>وز ناله من در تو نگیرد سرموئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسن تمامی و دل افروز و منم نیز</p></div>
<div class="m2"><p>تن کاسته و انگشت نما از همه روئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یعنی تو مه چاردهی از همه وجهی</p></div>
<div class="m2"><p>من بی تو مه یک شبه ام از همه سوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عشق تو مردی نکند یاد ز جفتی</p></div>
<div class="m2"><p>وز حسن تو یک زن نبرد طاعت شوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا آب جمال تو روان گشت نبرده ست</p></div>
<div class="m2"><p>یک تشنه درست از لب جوی تو سبوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چشمه نوش تو عیان شد نرسیده ست</p></div>
<div class="m2"><p>یک جرعه کام از لب آزی به گلوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اشک و جگر سوخته در عشق گرفتند</p></div>
<div class="m2"><p>با لعل لب و مشک سر زلف تو خویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وامروز به اقبال لب و زلف تو هر یک</p></div>
<div class="m2"><p>از رنگ به رنگی شد و از بوی به بوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با جور تو کس پای ندارد به جز از من</p></div>
<div class="m2"><p>ناید مه دی کار چناری ز کدوئی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر رغم مرا سفله و ناکس چه گزینی</p></div>
<div class="m2"><p>آخر چو منی را چه کشی بهر چنوئی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناجنسی ازین جنس نیاید ز حریفی</p></div>
<div class="m2"><p>زشتی بدینسان نکند هیچ نکوئی</p></div></div>