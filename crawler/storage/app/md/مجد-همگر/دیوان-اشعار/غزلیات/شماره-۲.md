---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گر تو پنداری که عشقم هر دم افزون نیست هست</p></div>
<div class="m2"><p>یا دلم در دوری روی تو پر خون نسیت هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور ترا شبهت بود کاندر فراقت بردلم</p></div>
<div class="m2"><p>هر شب از خیل عنا وغم شبیخون نیست هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور تو صورت بسته ای کز عکس دندان ولبت</p></div>
<div class="m2"><p>چشم من پر لعل ناب و در مکنون نیست هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بری ظن کاندرین شبهای تیره بردرت</p></div>
<div class="m2"><p>از سرشک دیدگانم خاک معجون نیست هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو پنداری که چون لیلی نئی از نیکویی</p></div>
<div class="m2"><p>یا رهی در عشق تو افزون ز مجنون نیست هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته بودی درپیامی فرصتم بر وصل نیست</p></div>
<div class="m2"><p>ورتوگویی ترس بد گویانت اکنون نیست هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بهانه ست ارنه شب تیره ست و خلوت بی رقیب</p></div>
<div class="m2"><p>اینت زیبا اتفاقی فرصتت چون نیست هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر بهار و باع مفتونند خلقی وین زمان</p></div>
<div class="m2"><p>گر توگویی کاین پریشان برتو مفتون نیست هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم هر کس روشن است از گلستان گر ظن بری</p></div>
<div class="m2"><p>کآب چشمم بی رخ گلگونت گلگون نیست هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به کنج خاطرت افتد که در نظم غزل</p></div>
<div class="m2"><p>لطف او چون حسن تو زاندازه بیرون نیست هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور تو پنداری که بر تخت سلیمان دوم</p></div>
<div class="m2"><p>بنده از آصف به جاه وحشمت افزون نیست هست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وربگردد در دل ضحاک سیرت دشمنش</p></div>
<div class="m2"><p>کآن شهنشاه مظفر فر فریدون نیست هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور کسی گوید نظیرش زیر گردون هست نیست</p></div>
<div class="m2"><p>ور گمان آید که قدرش بر ز گردون نیست هست</p></div></div>