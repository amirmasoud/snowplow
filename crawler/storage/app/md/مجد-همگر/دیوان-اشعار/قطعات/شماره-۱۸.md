---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>سراج الدین غصنی دام فضله</p></div>
<div class="m2"><p>چراغی نیست بل نور الهیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مه تا ماهی او را مستفیدند</p></div>
<div class="m2"><p>که صیت فضلش از مه تا به ماهیست</p></div></div>