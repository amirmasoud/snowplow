---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>ای لفظ تو آب زندگانی</p></div>
<div class="m2"><p>وی کلک تو اصل شادمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط تو که جان ازوست واله</p></div>
<div class="m2"><p>جانیست ز غایت روانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیخ قلم تو شاخ دولت</p></div>
<div class="m2"><p>بارش همه عز جاودانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نامه فرخت بدیدم</p></div>
<div class="m2"><p>گفتم که زهی مسیح ثانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بفزود دلم خطاب عالیت</p></div>
<div class="m2"><p>چون دل که فروزد از جوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شادم کردی که شاد بادی</p></div>
<div class="m2"><p>زندم ماندی که زنده مانی</p></div></div>