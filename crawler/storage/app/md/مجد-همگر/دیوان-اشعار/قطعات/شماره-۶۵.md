---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>خورشید چو از حوت سوی خانه بهرام</p></div>
<div class="m2"><p>بخرام از خانه سوی گلزار گه شام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش که آغاز کند خوی تو خامی</p></div>
<div class="m2"><p>برخیز به گه ساقی و پیش آر می خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو یار شو ای یار اگر دهر نشد یار</p></div>
<div class="m2"><p>تو رام شو ای دوست اگر دور نشد رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چرخ به کام تو نگردد تو بگردان</p></div>
<div class="m2"><p>دوری دو به کام دل من جام غم انجام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بر رخ تو نوش کنم مایه شادی</p></div>
<div class="m2"><p>بر یاد وزیر الوزراء خواجه اسلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارای جهان گیر جهان دار جهان بخش</p></div>
<div class="m2"><p>دستور خطاپوش عطاپاش عطانام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن خواجه که شد جای خلافت به وی آباد</p></div>
<div class="m2"><p>وان صدر که بگرفت زمانه به وی آرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلمش سبب گردش تابنده افلاک</p></div>
<div class="m2"><p>امرش علل جنبش بی فترت اجرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عهد همایون وی آرامگه شیر</p></div>
<div class="m2"><p>خفتنگه آهو بره شد در طرف شام</p></div></div>