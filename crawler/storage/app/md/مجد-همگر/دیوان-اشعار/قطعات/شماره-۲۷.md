---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>دور دور خر است و کره خران</p></div>
<div class="m2"><p>گر ترا باور آید ار ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عزیز نبی شود زنده</p></div>
<div class="m2"><p>جز خرش هیچ در نظر ناید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور فرود آید از فلک عیسی</p></div>
<div class="m2"><p>جز به خر می سوار برناید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اربگردد همه جهان دجال</p></div>
<div class="m2"><p>خر به پشت خر از مقر ناید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور سر از خاک برزند قارون</p></div>
<div class="m2"><p>جز خریدار کره خر ناید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نسیم خرد تبسم کن</p></div>
<div class="m2"><p>کز دم خر به جز به خر ناید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجد با خر مگوی راز خران</p></div>
<div class="m2"><p>که ز خر بر خرد ضرر ناید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خری خرخر و تو عشوه مخر</p></div>
<div class="m2"><p>که به جز خرخری ز خر ناید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور به روی تو برکشند خری</p></div>
<div class="m2"><p>از من رو کشیده زر ناید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور به خر داده اند شغل ترا</p></div>
<div class="m2"><p>جز به آنش به کار برناید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز خر نامه خوان ولیک بدان</p></div>
<div class="m2"><p>که ز خر نامه خوان هنر ناید</p></div></div>