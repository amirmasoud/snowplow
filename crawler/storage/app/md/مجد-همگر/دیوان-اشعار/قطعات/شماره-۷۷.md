---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>خسروا داشت سخایی تو مرا پار چنانک</p></div>
<div class="m2"><p>کآن نیارست زدن لاف ز هستی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان با همه تعظیم و بلندی کوراست</p></div>
<div class="m2"><p>می زد از روی تواضع دم پستی با من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو برداشتی اکنون ز سرم دست کرم</p></div>
<div class="m2"><p>می زند از سر کین تیغ دودستی با من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یادمی آر از آن شب که رهی را گفتی</p></div>
<div class="m2"><p>عمر باقی بنشین خوش چو نشستی با من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وین شب آن بود که در سر هوس نردت بود</p></div>
<div class="m2"><p>نرد من بردم و عمدا تو شکستی با من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الف مصر سحرگاه رسید از کرمت</p></div>
<div class="m2"><p>هم بدان وعده و میعاد که بستی با من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غایت مکرمت این بود و حقیقت که نکرد</p></div>
<div class="m2"><p>کرم رایج تو عشوه پرستی با من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب امسال چه تدبیر کنم گر چون پار</p></div>
<div class="m2"><p>شه نبازد ندبی نرد به مستی با من</p></div></div>