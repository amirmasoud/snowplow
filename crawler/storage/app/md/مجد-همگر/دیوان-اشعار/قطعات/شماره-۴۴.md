---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>شرف الدین محمد حسنی</p></div>
<div class="m2"><p>فخرآل رسول و تاج تبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه بد نور دیده زهرا</p></div>
<div class="m2"><p>وانکه بد پشت حیدر کرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین جهان غرور و دار فنا</p></div>
<div class="m2"><p>رفت سوی سرای امن و قرار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز یکشنبه سلخ ذیقعده</p></div>
<div class="m2"><p>سال بر ششصد و دو چل با چار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سرای امارت بغداد</p></div>
<div class="m2"><p>در جوار جواد امام کبار</p></div></div>