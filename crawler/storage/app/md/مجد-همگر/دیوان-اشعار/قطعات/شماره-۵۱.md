---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>در طوس دوش گفتم خرم نیم چرا</p></div>
<div class="m2"><p>یاریم گفت ساده دلا خرمی و طوس؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم کز آدمی اثر اینجا نیافتم</p></div>
<div class="m2"><p>گفتا تو نیز گاو شدی آدمی و طوس؟</p></div></div>