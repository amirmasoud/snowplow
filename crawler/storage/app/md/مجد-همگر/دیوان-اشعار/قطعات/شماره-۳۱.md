---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای خسروی که سایس امر تو از نفاذ</p></div>
<div class="m2"><p>بر پای هفت توسن گردون نهاد قید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مجلس مربی عدل تو توبه کرد</p></div>
<div class="m2"><p>دهر دنی ز مکر و سپهر دغا ز کید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تهذیب خلق خوب تو در عنفوان عمر</p></div>
<div class="m2"><p>منسوخ کرد قصه بسطامی و جنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدخواهت ار ز درد دمی سرد برکشید</p></div>
<div class="m2"><p>ابر تموز برف دهد در هوای فید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حالیست بنده را که گر آنها کند به شرح</p></div>
<div class="m2"><p>برخاطر تو جلوه کند در لباس شید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنکه دامن تو گرفتم ز خاص و عام</p></div>
<div class="m2"><p>ایمن نیم ز چوبک عمر و ز قصد زید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کعبه پناه خودم جای امن ده</p></div>
<div class="m2"><p>کاندر حریم کعبه حق آمن است صید</p></div></div>