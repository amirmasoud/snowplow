---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>افتخار جهان ظهیر الدین</p></div>
<div class="m2"><p>ای جهان را به جان تو سوگند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که در مهد عهد نادیده ست</p></div>
<div class="m2"><p>دیده آسمان چو تو فرزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور آئینه گون سپهر ترا</p></div>
<div class="m2"><p>نانموده چو آینه مانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفع عین الکمال قدر ترا</p></div>
<div class="m2"><p>چرخ و سیاره مجمرند و سپند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدمت باد با خضر همراه</p></div>
<div class="m2"><p>نفست با مسیح خویشاوند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم پاکت به معجزی که توراست</p></div>
<div class="m2"><p>رسم بیماری از جهان برکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق تو حقگذار و خلق نواز</p></div>
<div class="m2"><p>فضل توشه پسند و شهرپسند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نپرسی زماجرای رهی</p></div>
<div class="m2"><p>که چه دید از جفا و خصمش چند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناگزیرم بود که شرح دهم</p></div>
<div class="m2"><p>حال جسم ضعیف و حال نژند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیر قصد آنچنان زدند مرا</p></div>
<div class="m2"><p>که به جان و دلم رسید گزند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زآنچه بستند بر بروت رهی</p></div>
<div class="m2"><p>شهر پر شد ز طنز و سبلت خند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جگرم را نه آن طپش دادند</p></div>
<div class="m2"><p>که علاجش کنی به سرکه و قند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در مزاج دلم نه آن خلل است</p></div>
<div class="m2"><p>که شفی یاب گردد از ریوند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غبنم این خود نه بس که می بینم</p></div>
<div class="m2"><p>از جفای جهان بد پیوند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خرکناس در جل اطلس</p></div>
<div class="m2"><p>رخش رستم به زیر پشم آکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون کبوتر نشسته ام قانع</p></div>
<div class="m2"><p>به هوای دیار خود خرسند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون ازین آشیان کنم پرواز</p></div>
<div class="m2"><p>که مرا مهر خانه بال بکند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاصه از خاک پارس کاندر وی</p></div>
<div class="m2"><p>آب طوقم شد و هوا پابند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پند دل می دهم به دوری لیک</p></div>
<div class="m2"><p>چکنم دل نمی پذیرد پند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قصه یک حاجتم روا گردان</p></div>
<div class="m2"><p>که شدستم عظیم حاجتمند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرضه کن حال دردناک مرا</p></div>
<div class="m2"><p>پیش رای شهی و تخت بلند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گو مکن ضایعم که دور فلک</p></div>
<div class="m2"><p>ناورد چون منی دگر به کمند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر توان کرد کار من بگذار</p></div>
<div class="m2"><p>ورنه مردانه همتی دربند</p></div></div>