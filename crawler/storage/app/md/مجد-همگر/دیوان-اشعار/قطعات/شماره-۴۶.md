---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>دلم دیوانه گشت از تاب زنجیر</p></div>
<div class="m2"><p>تنم بگداخت زین زندان دلگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه شب مه بینم و نه روز خورشید</p></div>
<div class="m2"><p>نه بر من می وزد بادی به شبگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبونم کرد ایام تبه کار</p></div>
<div class="m2"><p>تباهم کرد گردون زبون گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرم شد در بهار عمر پر برف</p></div>
<div class="m2"><p>دل من در جوانی زود شد پیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درونم شد ز دست دهر پر درد</p></div>
<div class="m2"><p>روانم شد ز شست چرخ پرتیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بی تدبیرم اکنونم چه چاره</p></div>
<div class="m2"><p>چو بیچاره شدم اکنون چه تدبیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا در حبس عیشی دست داده ست</p></div>
<div class="m2"><p>ز یار و رود و جام و نغمه زیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حریفم گریه آمد جام می اشک</p></div>
<div class="m2"><p>سرودم ناله رود آواز زنجیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نجستم با کسی در کینه پیشی</p></div>
<div class="m2"><p>نکردم با کسی در مهر تقصیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به جای شیر مهرم داد دایه</p></div>
<div class="m2"><p>که با من مهر شد چون با شکر شیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به قدر خود وفا با هر که کردم</p></div>
<div class="m2"><p>مکافاتم جفا آمد ز تقدیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه افتاد ای رفیقان مر شما را</p></div>
<div class="m2"><p>که شد یکبارتان یاد من از ویر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هم مشفق ترند از آدمیزاد</p></div>
<div class="m2"><p>به دریا ماهی و در کوه نخجیر</p></div></div>