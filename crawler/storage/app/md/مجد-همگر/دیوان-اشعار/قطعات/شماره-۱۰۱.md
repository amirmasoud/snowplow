---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>گر چه پیش از تو بود حاتم طی</p></div>
<div class="m2"><p>تو ز حاتم به منزلت پیشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جهانداری و به نسبت جود</p></div>
<div class="m2"><p>همچنان تنگدست و درویشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما توانگرتریم از تو از آنک</p></div>
<div class="m2"><p>ما تو داریم کز جهان بیشی</p></div></div>