---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ز غبن هدهد میمون که شد متابع زاغ</p></div>
<div class="m2"><p>ز رشک ملک سلیمان که شد مسخر دیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گوش معنی بشنو که هر دمی صد بار</p></div>
<div class="m2"><p>شهان سلغری از خاک برکشند غریو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجاست آصف تا نوحه گر شود بر ملک</p></div>
<div class="m2"><p>که بر و بحرش بی کد خدای ماند و خدیو</p></div></div>