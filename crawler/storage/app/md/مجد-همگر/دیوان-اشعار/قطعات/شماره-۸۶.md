---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>شاها ز فر سایه معمار عدل تو</p></div>
<div class="m2"><p>همسایه عقاب گرفت آشیان چکاو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنگال شیر شرزه به پشتی پاس تو</p></div>
<div class="m2"><p>کوتاه شد ز گردن گور و سرین گاو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک خاصیت ز لطف تو در کوه اگر نهد</p></div>
<div class="m2"><p>ظاهر شود ز خوف دل سنگ خاره تاو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خاک درگه تو چو پهلو زد آفتاب</p></div>
<div class="m2"><p>چرخش به طعنه گفت نیی مرد او مکاو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو هر آنکه نرد دغا باخت روزگار</p></div>
<div class="m2"><p>نقش بدش نمود و نیارست خواست داو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل شود محصل مال و ضیاع تو</p></div>
<div class="m2"><p>از خوار جو خراج و ز ساری و ساوه ساو</p></div></div>