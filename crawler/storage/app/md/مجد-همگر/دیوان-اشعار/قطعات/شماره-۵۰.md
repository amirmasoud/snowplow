---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>زندگانی خسرو نقبا</p></div>
<div class="m2"><p>باد چون مدت زمانه دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظام امور و رفعت قدر</p></div>
<div class="m2"><p>با حصول مراد و نعمت و ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر آن کار کآورد رخ و رای</p></div>
<div class="m2"><p>باد یارش خدای بی انباز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رای عالیش را کنم معلوم</p></div>
<div class="m2"><p>حال من چاکر از طریق جواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دین پناها دلم به حضرت تو</p></div>
<div class="m2"><p>بیش از آن دارد اشتیاق و نیاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که به سعی قلم به صد طومار</p></div>
<div class="m2"><p>نتوان شمه ای نمودن باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزکی چند شد که دور از تو</p></div>
<div class="m2"><p>در شادی نشد به رویم باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با من آن می کند عنای فراق</p></div>
<div class="m2"><p>که به مهره کند مشعبده باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود چه تاب آورد تذرو ضعیف</p></div>
<div class="m2"><p>در کف انتقام چنگل باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به خدائی که دار ضرب فلک</p></div>
<div class="m2"><p>از طلای نجوم کرد به ساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صنع او هر سحر سبیکه خور</p></div>
<div class="m2"><p>آرد از کوره افق به فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درم ماه را دو نیمه کند</p></div>
<div class="m2"><p>هر به یکماه بی میانجی گاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که عیار دلم به بوی خلاص</p></div>
<div class="m2"><p>در فراق تو هست جفت گداز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه تحریر شوق و درد فراق</p></div>
<div class="m2"><p>با قلم راست چون بگویم راز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کلک سرگشته در موافقتم</p></div>
<div class="m2"><p>ناله و گریه می کند آغاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر دلم عیش تلخ می دارد</p></div>
<div class="m2"><p>فرقت دوستان غم پرداز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روضه مشهد ارچه روحانیست</p></div>
<div class="m2"><p>حبذا بام مسجد شیراز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زه زهی کعبه ای که مرغ دلم</p></div>
<div class="m2"><p>در فضای تو می کند پرواز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر بیابد نسیم جان بخشت</p></div>
<div class="m2"><p>جسد مرده روح یابد باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هان و هان ای برید باد سحر</p></div>
<div class="m2"><p>رنجه شو یکزمان و نیک بتاز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاک درگاه آن جماعت بوس</p></div>
<div class="m2"><p>که فلکشان برد به طوع نماز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک به یکشان دعای من برسان</p></div>
<div class="m2"><p>از در دل نه از ره آواز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خاصه مولا قوام ملت و دین</p></div>
<div class="m2"><p>آن مکان مکارم و اعزاز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باز امام امم عزیزالدین</p></div>
<div class="m2"><p>آنکه بر کسوت مدیست طراز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سرور دین عماد ملت و دین</p></div>
<div class="m2"><p>به هنر خلق را نمود اعجاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افضل دین و دولت اضل عصر</p></div>
<div class="m2"><p>آن به فضل از جهانیان ممتاز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تاج سادات و تاج دین جعفر</p></div>
<div class="m2"><p>آن رفیق شفیق دوست نواز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه خوانمش جعفر طیار</p></div>
<div class="m2"><p>از سر صدق نه ز روی مجاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیک شد نقطه ای ز بی سوری</p></div>
<div class="m2"><p>شد به تصحیف جعفر طناز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این لطیفه ازان لطیف تر است</p></div>
<div class="m2"><p>که بیاید تصرف غماز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد گسسته طویله سخنم</p></div>
<div class="m2"><p>از کششهای محنت ره آز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مثل میل دو دولت درشان</p></div>
<div class="m2"><p>عشق محمود باد و حسن ایاز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نرسد حادثه به حضرتشان</p></div>
<div class="m2"><p>کایمن است آسمان ز تیرانداز</p></div></div>