---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>به گاه بزرگی بریدند حلقت</p></div>
<div class="m2"><p>غلامان ز مظلومی و عجز و خردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو روئین تنی با بزرگان و خردان</p></div>
<div class="m2"><p>که از گرز و تیغ همه جان ببردی</p></div></div>