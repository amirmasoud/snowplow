---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای به حق شاهی که قدرت از علو مرتبت</p></div>
<div class="m2"><p>بر سر شاه فلک دیده ست پای تخت خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز خاک پارس چون برداشتم رخت امید</p></div>
<div class="m2"><p>گفتم آرم سوی خاک آستانت رخت خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پارسالت دور از اکنون خود نبد پردخت من</p></div>
<div class="m2"><p>کز وقایع هم نبودت یکزمان پردخت خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین زمان گفتم کنی مافات را یک ده قضا</p></div>
<div class="m2"><p>وین ندیدم جز طبع ساده یک لخت خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زانکه گشتم از جنابت چند باری ناامید</p></div>
<div class="m2"><p>سخت دلتنگم ز رای سست و روی سخت خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از که بینم رنج این حرمان کزین حضرت مراست</p></div>
<div class="m2"><p>از زمانه یا ز تقصیر تو یا از بخت خویش</p></div></div>