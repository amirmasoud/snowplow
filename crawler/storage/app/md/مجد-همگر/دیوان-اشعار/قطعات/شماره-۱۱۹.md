---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>ای آن زمین وقار که بر آسمان فضل</p></div>
<div class="m2"><p>ماه خجسته منظر و خورشید منظری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قومی ز ناقدان سخن گفته ظهیر</p></div>
<div class="m2"><p>ترجیح می نهند بر اشعار انوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمعی دگر بر این سخن انکار می کنند</p></div>
<div class="m2"><p>فی الجمله در محل نزاعند وداوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترجیح یکطرف تو بدیشان نما که هست</p></div>
<div class="m2"><p>زیر نگین طبع تو ملک سخنوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را در این مجادله فریاد رس تو باش</p></div>
<div class="m2"><p>نه پادشاه ملک سخن مجدهمگری</p></div></div>