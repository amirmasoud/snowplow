---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>ای به تاثیر عدل معتدلت</p></div>
<div class="m2"><p>جاه تو قوت گزاف شکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعتدال بهار دولت تو</p></div>
<div class="m2"><p>نام و ناموس اختلاف شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حلاوت رحیق لفظ خوشت</p></div>
<div class="m2"><p>سوزش و تلخی سلاف شکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن خصمت ز صرصر حدثان</p></div>
<div class="m2"><p>آورد چون نی و قلاف شکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرد از قاف قوت قهرت</p></div>
<div class="m2"><p>الف قامتش چو کاف شکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لجه بحر زاخری به کرم</p></div>
<div class="m2"><p>تشنگی جان به اعتراف شکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صاحبا ذمیم به زنهارت</p></div>
<div class="m2"><p>وعده را کم ده از خلاف شکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرض خواه گران چون کوهم</p></div>
<div class="m2"><p>که به پیشانی است قاف شکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رویم ا خشم او شکن گیرد</p></div>
<div class="m2"><p>چن سمن کآورد به ناف شکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز شنبه چو معتکف گردم</p></div>
<div class="m2"><p>زودم آرد در اعتکاف شکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم به پشتی دولتت روزی</p></div>
<div class="m2"><p>ای به انصاف پشت لاف شکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صف فاقه که پشت من بشکست</p></div>
<div class="m2"><p>بشکند سیف دین مصاف شکن</p></div></div>