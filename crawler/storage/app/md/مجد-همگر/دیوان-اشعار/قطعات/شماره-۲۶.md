---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>فرخ همای دولت و سعد سپهر ملک</p></div>
<div class="m2"><p>ای آنکه سایه ات به جهان فربهی دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سایه مبارک شاه جهان ترا</p></div>
<div class="m2"><p>اقبال و بخت و دولت و تاج و مهی دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا که موکب تو شبیخون برد ظفر</p></div>
<div class="m2"><p>با باد مرکبان ترا همرهی دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور سپهر هر چه تو رغبت کنی کند</p></div>
<div class="m2"><p>دست زمانه آنچه تو فرمان دهی دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر پرتوی ز رای تو بر جرم مه فتد</p></div>
<div class="m2"><p>حالی هلال یکشبه را فربهی دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هول آتش سر تیغ تو سیر چرخ</p></div>
<div class="m2"><p>تن در گریز و شعبده روبهی دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر حامله که نور سنانت فتد بر او</p></div>
<div class="m2"><p>ناگه به چشم های جنینش اکمهی دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید خنجرت ز سر سمت معرکه</p></div>
<div class="m2"><p>شبهای عمر خصم ترا کوتهی دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاثیر ماه رایت تو روی خصم را</p></div>
<div class="m2"><p>در بوستان معرکه رنگ بهی دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشمن گر از دریغ دمی سرد برکشد</p></div>
<div class="m2"><p>فصل تموز را صفت دی مهی دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها امید هست مراکز قبول تو</p></div>
<div class="m2"><p>اقبال طالع بد ما را بهی دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارند چشم آن دل و گوشم که لطف تو</p></div>
<div class="m2"><p>یک لحظه گوش و دل به حدیث رهی دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بزم عشرت این فلک آبگینه رنگ</p></div>
<div class="m2"><p>دورم همه ز شیشه و جام تهی دهد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دانی که بنده پرده دریده ست همچو گل</p></div>
<div class="m2"><p>از بسکه دل به قد چو سرو سهی دهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندوه عشق خیمه زند بر در دلش</p></div>
<div class="m2"><p>هر کس که دل به عشق بت خرگهی دهد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقل از طریق عشق به صد مرحله ست دور</p></div>
<div class="m2"><p>هر تن که دل به عشق دهد ز ابلهی دهد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پای پیل حادثه گر دست گیریم</p></div>
<div class="m2"><p>بر عرصه مراد سپهرم شهی دهد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جز باد صبح کیست کسی کو به شرح و بسط</p></div>
<div class="m2"><p>شه زاده را ز قصه من آگهی دهد</p></div></div>