---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>من هجاء چون کنم مطرزک را</p></div>
<div class="m2"><p>که هجاء کردن است کژ خوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علت آینه که می گویند</p></div>
<div class="m2"><p>دورقی داندی و شروانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نگویم که نیستش دانش</p></div>
<div class="m2"><p>کآن طریقی بود ز نادانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقل انکار حس چگونه کند</p></div>
<div class="m2"><p>خور نگردد به میغ ظلمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه من دیده ام ز سیرت او</p></div>
<div class="m2"><p>نه ز مهداری و هجاخوانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی تکلف بگویم و نکنم</p></div>
<div class="m2"><p>سخن آرائی و سخندانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کافه خلق راست او بدخواه</p></div>
<div class="m2"><p>خویش و بیگانه قاضی و دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنچه او از زبان شوم کند</p></div>
<div class="m2"><p>نکند صد خدنگ ماکانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصد و غمز و نفاق و خبث کند</p></div>
<div class="m2"><p>سر به سر منطقی و برهانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز تکبر چو سر بگرداند</p></div>
<div class="m2"><p>آید اندر کلام نفسانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر کسی استراق سمع کند</p></div>
<div class="m2"><p>بشنود صد فسون شیطانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به ظرافت چو گه خورد حاشا</p></div>
<div class="m2"><p>وآرد آن خنده زمستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیر هر خنده ای چو زهر بود</p></div>
<div class="m2"><p>صد هلاهل ز حقد پنهانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز اباطیل پیچ پیچ چو کاف</p></div>
<div class="m2"><p>قاف را بشکند به پیشانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مردکی کوهی شبانکاره</p></div>
<div class="m2"><p>بغتتاً چون شود خراسانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لاجرم زین نمط بود فن او</p></div>
<div class="m2"><p>که همی بینی و همی خوانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوه پرورده پلنگ نهاد</p></div>
<div class="m2"><p>دور از آئین و رسم انسانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تازی اش هست و پارسی گه گاه</p></div>
<div class="m2"><p>می درآید به صد پریشانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم شوخش ندیده در همه عمر</p></div>
<div class="m2"><p>حشمت و نعمت و تن آسانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شره و حرص جاه و مالش داد</p></div>
<div class="m2"><p>چشم پوشیدگی و عریانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عورت خویش را نمی بیند</p></div>
<div class="m2"><p>از غرور و نشاط شهوانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شرم بادش ز نور و شمس و عبید</p></div>
<div class="m2"><p>وز کمال و عماد زاکانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شمس کیشی ز جور آن بدکیش</p></div>
<div class="m2"><p>اشک دارد چو در عمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در براق کمال او نرسد</p></div>
<div class="m2"><p>صد از آن ژاژ خای کهدانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کیست عبداله ابی سلول</p></div>
<div class="m2"><p>حاسد اختصاص سلمانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>علم باقی همی فروشد شیخ</p></div>
<div class="m2"><p>به حطام مزخرف فانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نخورد بر زمال و جاه چنین</p></div>
<div class="m2"><p>گو شود فیلسوف یونانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خانه ای کش بنا ز ظلم بود</p></div>
<div class="m2"><p>زود روی آورد به ویرانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر توئی اهل علم و فتوی و درس</p></div>
<div class="m2"><p>... انی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه که علم از تو سفله بیزار است</p></div>
<div class="m2"><p>هم بدین شیوه هستی ارزانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نی نی از فتنه جوئی و شر و شور</p></div>
<div class="m2"><p>وز هوس های شغل دیوانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دردهد تن به ننگ سرهنگی</p></div>
<div class="m2"><p>خوش کند دل به عار دربانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با چنین سیرتی که می بینی</p></div>
<div class="m2"><p>با چنین خصلتی که می دانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر او عالم و مسلمان است</p></div>
<div class="m2"><p>وای بر علم و بر مسلمانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بد شدم من ز صحبت بد او</p></div>
<div class="m2"><p>نیک گفت آن حکیم روحانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که نکوکار بد شود ز بدان</p></div>
<div class="m2"><p>خاصه چون جور بیند از جانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به جوانی ز هجو و غیبت و خبث</p></div>
<div class="m2"><p>توبتم داد لطف رحمانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کرد هیچی مرا بدین سر زال</p></div>
<div class="m2"><p>این شغاد لعین دستانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>توبه من شکست و بشکندش</p></div>
<div class="m2"><p>گردن از قهر عدل یزدانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از همه گفته ها پشیمانم</p></div>
<div class="m2"><p>گرچه کردم بسی درافشانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>وندر این قطعه از چنین گفتار</p></div>
<div class="m2"><p>کافرم گر برم پشیمانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یارب از غیب رحمتی بفرست</p></div>
<div class="m2"><p>بهتر از ابرهای نیسانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سقطه نخجوانیش نو کن</p></div>
<div class="m2"><p>تا جهان را ز فتنه برهانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر چنین دیو و دد دریغ دریغ</p></div>
<div class="m2"><p>نظر لطف آصف ثانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نفس دیو مردمان مرساد</p></div>
<div class="m2"><p>در چنین سیرت سلیمانی</p></div></div>