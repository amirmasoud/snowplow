---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>گفتم که مدح صاحب اعظم کنم ادا</p></div>
<div class="m2"><p>آسیمه گشت عقلم و بر رخ فکند چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا کدام صاحب اعظم مدار ملک</p></div>
<div class="m2"><p>دارای دین و دنیی و مخدوم فخر دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون لطف اوست خلق جهان را شفیع و یار</p></div>
<div class="m2"><p>بادش خدای عز و جل حافظ و معین</p></div></div>