---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>خدایگانا در ملک شرع معجز تو</p></div>
<div class="m2"><p>شکست بند طلسم زمانه جادو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبیم پاس تو در مرغزار ملک جهان</p></div>
<div class="m2"><p>که شیر محترز است از چراگه آهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همای معدلتت سایه آنچنان افکند</p></div>
<div class="m2"><p>که باز برحذر است از تعرض تیهو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریف عدل تو صد بار باز مالیده ست</p></div>
<div class="m2"><p>ز کعبتین دغا نقش عالم شش تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شده ست خادم لفظ تو لولو از بن گوش</p></div>
<div class="m2"><p>ازان همیشه به لالاست نسبت لولو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیافت مثل تو دور جهان صورتگر</p></div>
<div class="m2"><p>ندید شبه تو چشم زمانه جادو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان چو یافت ترا روح محض سر تا پای</p></div>
<div class="m2"><p>در آن فتاد به شرکی که نیست آن معفو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو باز دیدت در طینت نهاد بشر</p></div>
<div class="m2"><p>چه گفت گفت جهان لااله الاهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عروس ملک جهان بر تو شد چنان عاشق</p></div>
<div class="m2"><p>که جاودان نکند آرزوی دیگر شو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رشک رای تو هر شام نور چشم فلک</p></div>
<div class="m2"><p>فرو شود به گل تیره پرگره ابرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دست صبح گریبان خویشتن بدرد</p></div>
<div class="m2"><p>ز عشق سنجق فتحت شب سیه گیسو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرکب است سر سنجقت به فتح و ظفر</p></div>
<div class="m2"><p>چنانکه چرم ز ترکیب زاج با مازو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آنکه شربتی از کوزه خلاف تو خورد</p></div>
<div class="m2"><p>شودش کاسه سر جفت کاسه زانو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسیکه چون سرطان با تو یک زمان کژ رفت</p></div>
<div class="m2"><p>ز ثور چرخ لگد خورد و زحملش سرو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منم که تا سر من سایه قبول تو یافت</p></div>
<div class="m2"><p>شد آفتاب ختائی نسب مرا هندو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو داغ و طوق تو دارد سرین و گردن من</p></div>
<div class="m2"><p>به پشت گرمی تو با فلک زنم پهلو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مثال سعی من و خصم بدسگال عجول</p></div>
<div class="m2"><p>حدیث کودک و مار است و زاهد و راسو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان سفله چنان کژ نهاد و کور دل است</p></div>
<div class="m2"><p>که باز می نشناسد صدیق را ز عدو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بس است خاطر شه را یکی اشارت من</p></div>
<div class="m2"><p>چه حاجت است که یک نکته را کنم یا دو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دمی بر آورم از راه نعمت المصدور</p></div>
<div class="m2"><p>اگر چه قافیه مر طبع را گرفت گلو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در این زمان که فضای سپهر و صحن زمین</p></div>
<div class="m2"><p>ز سردی نفس زمهریر شد مملو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز سردی دم دی آرزوست روبه را</p></div>
<div class="m2"><p>که باژگونه کند پوستین به دیگر سو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به آرزو و هوس مرغ در هوا گوید</p></div>
<div class="m2"><p>کجاست بابزن و آب گرم و آتش کو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در این چهله یقینم که زاهد چله دار</p></div>
<div class="m2"><p>نماز صبح کند چاشتگه ز بیم وضو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در این هواست سرایم که زمهریز بر او</p></div>
<div class="m2"><p>نوشته است دو صد عبده و خادمهو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در این سه ماه شود امرد ایمن از دباب</p></div>
<div class="m2"><p>از آنکه در کف لوطی شود فسرده خپو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا که شارع سرماست روز و شب وطنم</p></div>
<div class="m2"><p>برفت مغز زبس سردی هوا چو کدو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درید کار مرا روزگار گرگ نهاد</p></div>
<div class="m2"><p>که بر نکابت این روزگار باد تفو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر آنچه گرگ درد معجز تو بتواند</p></div>
<div class="m2"><p>به موی رو به کردن ز روی لطف رفو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دگر مواهب شاهانه را که دارم چشم</p></div>
<div class="m2"><p>امید هست که محصول گردد آن مرجو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنانکه موی شکاف است بنده در مدحت</p></div>
<div class="m2"><p>مگر دریغ ندارد عنایتت یک مو</p></div></div>