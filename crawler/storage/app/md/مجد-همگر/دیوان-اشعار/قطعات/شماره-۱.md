---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>روز فطرت چو دست قدرت ساخت</p></div>
<div class="m2"><p>از منی قرطه پرند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حبس صلبم قرارگاه آمد</p></div>
<div class="m2"><p>پس رحم کرد شهربند مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان مضایق چو لطف مبدا خلق</p></div>
<div class="m2"><p>بگذرانید بی گزند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دایه مهربان به مهد اندر</p></div>
<div class="m2"><p>داشت دربند روز چند مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس از آن از ادیب علم آموز</p></div>
<div class="m2"><p>ادب و زجر بود و پند مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنس او گرچه بود تلخ مذاق</p></div>
<div class="m2"><p>کام دل داشت همچو قند مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شب و صبح چرخ مجمره شکل</p></div>
<div class="m2"><p>ز اختران سوختی سپند مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده دل شدم مرید مراد</p></div>
<div class="m2"><p>تا هوی کرد دردمند مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف شیرازیان آهو چشم</p></div>
<div class="m2"><p>پای دام آمد و کمند مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معجر دلبران یغما کرد</p></div>
<div class="m2"><p>در خم طره پای بند مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از به هر چشم شوخ لعبت باز</p></div>
<div class="m2"><p>لعبتان طراز و چند مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون از آن بیدلی شدم آزاد</p></div>
<div class="m2"><p>حرص در بندگی فکند مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سهوی افتاد بر جهان و آورد</p></div>
<div class="m2"><p>نظر شاه در پسند مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غلطی رفت بر سپهر و فزود</p></div>
<div class="m2"><p>روزکی چند بادکند مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پست گشتم ز بندگی و نکرد</p></div>
<div class="m2"><p>پستی همت بلند مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنده وارم گرفت اسیر و نداد</p></div>
<div class="m2"><p>یارئیی طالع نژند مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برگ ترتیب جاه و سود و زیان</p></div>
<div class="m2"><p>بیخ شادی ز دل بکند مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زیر پالان کشید همچو خران</p></div>
<div class="m2"><p>انده اشهب و سمند مرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همچو انعام ضال و حیران کرد</p></div>
<div class="m2"><p>طلب گاو و گوسپند مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوستان کم شدند و بفزودند</p></div>
<div class="m2"><p>دشمنان از هزار و اند مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>غیرت دوستان حزینم کرد</p></div>
<div class="m2"><p>حسد دشمنان نژند مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نظر حاسدان چو دید که کرد</p></div>
<div class="m2"><p>نظر خسرو ارجمند مرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بند فرزین مکرشان بگشاد</p></div>
<div class="m2"><p>زآهنین بند مستمند مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چکنم گوئی آفرید خدای</p></div>
<div class="m2"><p>از پی بیدلی و بند مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه در بند بر زیان آمد</p></div>
<div class="m2"><p>مایه عمر سودمند مرا</p></div></div>