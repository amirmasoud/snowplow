---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>می فرستد هزار حمد و ثنا</p></div>
<div class="m2"><p>مختصر بی تکلف اطناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرفه کنکاجگیم روی نمود</p></div>
<div class="m2"><p>این زمان چون درآمدیم از خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد معلوم را عالیشان</p></div>
<div class="m2"><p>که رهی با یکی دو از اعقاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو سه روز است تا همی سوزیم</p></div>
<div class="m2"><p>نفس را همچو مشرکان به عذاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مضطرب چون سمندریم و چو حوت</p></div>
<div class="m2"><p>گاه در آفتاب و گه در آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که پذیره شویم یا نشویم</p></div>
<div class="m2"><p>پیش بنهاده ایم اصطرلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل می گویدم شدن اولی</p></div>
<div class="m2"><p>نه به آهستگی و نه به شتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماند یک چاشت روز و این فردا</p></div>
<div class="m2"><p>می رود یک سئوال از استصواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بامدادان رویم یا امشب</p></div>
<div class="m2"><p>اندرین چیست مذهب اصحاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر امشب رویم بر صحرا</p></div>
<div class="m2"><p>دست بر هم کجا دهد اسباب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به چه مرد دستور کرد توان</p></div>
<div class="m2"><p>نقل نقل و طعام و جام شراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور بمانیم تا سحر خیزیم</p></div>
<div class="m2"><p>درفتادیم همچو خر به خلاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چاشتگه کآفتاب گردد گرم</p></div>
<div class="m2"><p>چون کند راه مرد مست و خراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نعل سوزان کند سم مرکب</p></div>
<div class="m2"><p>پای راکب کند رکاب کباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>متردد بمانده است رهی</p></div>
<div class="m2"><p>می رود هر دم انتظار جواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رای عالی در این چه فرماید</p></div>
<div class="m2"><p>مصلحت در کدام و چیست صواب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صدر فاضل رشید دولت را</p></div>
<div class="m2"><p>که مرا اندر این عنا دریاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر آواش دق کند در نظم</p></div>
<div class="m2"><p>حاکم است اندر این و در هر باب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به کم از ساعتی به نظم آمد</p></div>
<div class="m2"><p>اینچنین سمط پر ز در خوشاب</p></div></div>