---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای زاحسانت آز آواره</p></div>
<div class="m2"><p>وی زانعامت آرزو زاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بانگ یوزت به ببر بیم دهد</p></div>
<div class="m2"><p>پاست از بیخ برکند باره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو سرور سوار سروآسا</p></div>
<div class="m2"><p>سر سور سپهر و سیاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهره شهر شیر شرزه شکار</p></div>
<div class="m2"><p>شرف شحنه شبانکاره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک گردد ز خشمت اختر خصم</p></div>
<div class="m2"><p>خور و خیزت ز خنجر خاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشه با پیل اگر زند پهلو</p></div>
<div class="m2"><p>پشه را پوستین شود پاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوئیا کز تو گشت گوینده</p></div>
<div class="m2"><p>گاه مردیم گواه گهواره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به من بی نظیر کن نظری</p></div>
<div class="m2"><p>تا جهانی شوند نظاره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم و غبنم کشند اگر شودم</p></div>
<div class="m2"><p>غیر تو غمگسار و غمخواره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه خرم چون نه چاره مانده نه چیز</p></div>
<div class="m2"><p>چکنم چون نجوئیم چاره</p></div></div>