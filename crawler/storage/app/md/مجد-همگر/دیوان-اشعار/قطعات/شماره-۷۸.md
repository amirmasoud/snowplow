---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>به خدائی که آشنائی داد</p></div>
<div class="m2"><p>وحی او با دل پیامبران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رسولی که روشنائی داد</p></div>
<div class="m2"><p>نور عقلش به چشم راهبران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کز دل پاک و سینه صافی</p></div>
<div class="m2"><p>بازگشتم به راه رهسپران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندرین پرده گرچه محجوب است</p></div>
<div class="m2"><p>صورت من ز چشم بی بصران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر راست بین نکو بیند</p></div>
<div class="m2"><p>کاندرین ره نیم ز کژنظران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرده ای صوفیانه خواهم گفت</p></div>
<div class="m2"><p>گر نگیرند خرده مختصران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو شاهی و من ترا محکوم</p></div>
<div class="m2"><p>چیست بر من تحکم دگران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سر تو که نور چشم من است</p></div>
<div class="m2"><p>خاک پایت که باد تاج سران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من در این پایه ام که شاید بود</p></div>
<div class="m2"><p>کم ملایک شوند سجده گران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک نفس من روا داری</p></div>
<div class="m2"><p>که برد سجده سگان و خران</p></div></div>