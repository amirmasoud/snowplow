---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>حیات بخشا چرخم ز غبن خواهد کشت</p></div>
<div class="m2"><p>به تیغ کین من از چرخ کینه خواه بخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیه دلی دل شه بر رهی گران کرده ست</p></div>
<div class="m2"><p>بیان جرم من از خصم دل سیاه بخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صد دلیل شود روشنت که بی جرمم</p></div>
<div class="m2"><p>ز من دلیل بجو یا ازو گواه بخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حوالت گنه ار برمن است هم سهل است</p></div>
<div class="m2"><p>شفیع بنده توئی عذر آن گناه بخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه مراد تو جوید شه جهان ز خدا</p></div>
<div class="m2"><p>تو از برای خدا بنده را ز شاه بخواه</p></div></div>