---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>صاحبا بنده همه ساله چنین می خواهد</p></div>
<div class="m2"><p>که زحال من و اتباع تو باشی آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن از بهر صداعی که مبادت هرگز</p></div>
<div class="m2"><p>زحمت بارگهت می ندهد جز گه گاه</p></div></div>