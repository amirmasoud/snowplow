---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>مردکی بود طامع و شیاد</p></div>
<div class="m2"><p>نام آن غرم حیدر آبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کودکان را ز بهر شفتالو</p></div>
<div class="m2"><p>سیب وامرود دادی و آبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه کدیه برای کسب حطام</p></div>
<div class="m2"><p>دیده کردی چو چرخ دولابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب به بزم معاشرت رفتی</p></div>
<div class="m2"><p>خفتی آنجا برای دبابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تایکی مست خفته را گادی</p></div>
<div class="m2"><p>جان بدادی ز رنج بی خوابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو همان حیدری ولی بی آب</p></div>
<div class="m2"><p>می کن ای دون سفله بی آبی</p></div></div>