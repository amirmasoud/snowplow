---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>قدوم ماه ربیع و خروج ماه صفر</p></div>
<div class="m2"><p>خجسته باد بدین مقصد و پناه بشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدایگان جهانبان بهای دولت و دین</p></div>
<div class="m2"><p>که از مدیح وی افزوده گشت جاه هنر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خجسته صاحب دیوان مشرق و مغرب</p></div>
<div class="m2"><p>که هست رایت رایات او سپاه ظفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قد قدش کوته بود قبای سپهر</p></div>
<div class="m2"><p>به فرق جاهش کوچک بود کلاه قمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان پناها در من نظر به رحمت کن</p></div>
<div class="m2"><p>که دور باد ز راه تو اشتباه نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ز سایه درگاه خود مکن نومید</p></div>
<div class="m2"><p>که نیست در دو جهانم امید گاه دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ثنات خوانم از بام تا به گاه شفق</p></div>
<div class="m2"><p>دعات گویم از شام تابه گاه سحر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدای در همه حالت رفیق باد رفیق</p></div>
<div class="m2"><p>اگر برای حضر باشد ار به راه سفر</p></div></div>