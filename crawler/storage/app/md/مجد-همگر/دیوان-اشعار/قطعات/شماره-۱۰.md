---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای خسروی که فتنه نشان آب تیغ تو</p></div>
<div class="m2"><p>روی زمانه را ز غبار فتن بشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر طرف باغ کام لب جوی آرزو</p></div>
<div class="m2"><p>شادابتر ز بخت تو یک سرو بن نرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاصیتی که طبع مرا هست در وفا</p></div>
<div class="m2"><p>دلجویئی نمود که معهود عهد تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دفع فتنه ایکه قضا زان کرانه کرد</p></div>
<div class="m2"><p>حزم تو پیش رفت و میان را ببست چست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بسته بد دری به مواسات برگشاد</p></div>
<div class="m2"><p>گر خسته بد دلی به مراعات بازجست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صیت تو سایر است خصوص از مدیح من</p></div>
<div class="m2"><p>از مکه تا به خلخ و از مصر تا به بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گشت تب معارض عرضت خدای داد</p></div>
<div class="m2"><p>توفیق خیر و بوی شفا ساعت نخست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیرنگ نقش جود نباشد به تب تباه</p></div>
<div class="m2"><p>بنیاد ذات خیر نگردد به رنج سست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدرود باد هر که نباشدت نیکخواه</p></div>
<div class="m2"><p>رنجور باد آنکه نخواهدت تن درست</p></div></div>