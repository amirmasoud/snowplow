---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ای بحر براعت که ضمیر تو جهان را</p></div>
<div class="m2"><p>دائم به عطا لولو منثور فرستد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلکت در ناسفته به اقطار رساند</p></div>
<div class="m2"><p>طبعت زر ناسخته به جمهور فرستد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظمی که فرستادیم از روی تفضل</p></div>
<div class="m2"><p>زانهاست که خورشید به گنجور فرستد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آری چه شود کم اگر آن گلبن دولت</p></div>
<div class="m2"><p>بوئی به دل خسته رنجور فرستد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان خلق معطر چه کم آید که نسیمی</p></div>
<div class="m2"><p>نزدیک یکی عاشق مهجور فرستد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حوصله این دل مهموم نگنجد</p></div>
<div class="m2"><p>نوباوه کز آن خاطر مسرور فرستد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس هسته شهباز به دراج نماید</p></div>
<div class="m2"><p>یا طعمه سیمرغ به عصفور فرستد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خواست که ترتیب دعائی کند از صدق</p></div>
<div class="m2"><p>وانگه سوی آن ساحت معمور فرستد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقلش ز سر طعنه قفائی به سزا داد</p></div>
<div class="m2"><p>یعنی که کس آن خدره سوی طور فرستد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر ماه چه منت بود ار ابر به نوروز</p></div>
<div class="m2"><p>از هاله ورا خرمن کافور فرستد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در علم عطارد چه فزاید گرش از جهل</p></div>
<div class="m2"><p>استاد لغت تخته مسطور فرستد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باشد ز در خنده چو چوبک زن لوری</p></div>
<div class="m2"><p>خاتون فلک را دف و طنبور فرستد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سایه سیه رو بود آن کز مه نخشب</p></div>
<div class="m2"><p>خورشید جهان را مدد از نور فرستد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کیوان نشود مفتخر از رای به یاریش</p></div>
<div class="m2"><p>آنکس که به تو رقعه ناجور فرستد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن مستی غفلت بود آنکو به تقرب</p></div>
<div class="m2"><p>نزدیک خضر جرعه مخمور فرستد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از ساده دلی باشد کز ماک ده شوخ</p></div>
<div class="m2"><p>لختی جگر سوخته زی حور فرستد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شینی بود از خرمگسی قطره شوری</p></div>
<div class="m2"><p>زی خانه شش گوشه زنبور فرستد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کی بر طبق بید بر خوشه پروین</p></div>
<div class="m2"><p>دهقان خرف خوشه انگور فرستد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیروزی از آنکس نتوان داشت طمع کو</p></div>
<div class="m2"><p>پیروزه کرمان به نشابور فرستد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کس نسخه نقش در گرمابه به تحفه</p></div>
<div class="m2"><p>بر دیبه چینی سوی فغفور فرستد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنجا همه از عقل شنیده دل و این نظم</p></div>
<div class="m2"><p>مر شاه سخن را به چه دستور فرستد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جان علی و جسم حسین و دل زهرا</p></div>
<div class="m2"><p>کز روضه رسولش سوی جان نور فرستد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن خسرو سادات که چرخش به سعادت</p></div>
<div class="m2"><p>اتمام سیادت را همه منشور فرستد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای بحر گهرزای که موج تو عدو را</p></div>
<div class="m2"><p>از نکبت نکبا سوی در دور فرستد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>قانع شو ازین خاک به یکذره ز اخلاص</p></div>
<div class="m2"><p>کز جان به مهر آمده میسور فرستد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تقصیر مدان گر رهی از کثرت اشغال</p></div>
<div class="m2"><p>نزدیک تو این قصه مسطور فرستد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>استاد سخندانی و کی عیب نماید</p></div>
<div class="m2"><p>با قطعه اگر جایزه مزدور فرستد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از خوان وی این ماحضری سرد نماید</p></div>
<div class="m2"><p>خاصه چو زمستان ز ره دور فرستد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو گرم مزاج کرمی وز ره حکمت</p></div>
<div class="m2"><p>شاید که به وارد سوی محرور فرستد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ترسد که دلش سرد شود کز دل تنگش</p></div>
<div class="m2"><p>مر صدر ترا تفته مصدور فرستد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن به که ثناهای ترا خفیه سراید</p></div>
<div class="m2"><p>و اوراد دعاهای تو مستور فرستد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا شام ابد بخت تو بیدار بماناد</p></div>
<div class="m2"><p>تا چرخ ز خور سایه به دیجور فرستد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برکام جهان باش تو منصور و مظفر</p></div>
<div class="m2"><p>تا خور به جهان رایت منصور فرستد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خصمت شده در خواب شبی تا دم صبحی</p></div>
<div class="m2"><p>کایام به بالینش دم صور فرستد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از قدر قدر روزی تو جام طرب باد</p></div>
<div class="m2"><p>تا واهب جان روزی مقدور فرستد</p></div></div>