---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>خسروا راز هفت پرده چرخ</p></div>
<div class="m2"><p>پیش رای تو می شود ظاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به هیبت در آسمان نگری</p></div>
<div class="m2"><p>چرخ در دور تو شود فاتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدرت آنجا رسید کز رتبت</p></div>
<div class="m2"><p>می نگردد نظر بر او قادر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذات تو جوهریست کز تعظیم</p></div>
<div class="m2"><p>شد زبان از بیان آن قاصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنه تو عالمی که می نشود</p></div>
<div class="m2"><p>مرغ وهم اندر آن هوا طایر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدر دیوان تو ز قدر و شرف</p></div>
<div class="m2"><p>فلک است وندر او ملک حاضر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجگانی به دور او فرشته صفت</p></div>
<div class="m2"><p>همه در کار مملکت ماهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وزارت مشرف الدین است</p></div>
<div class="m2"><p>صد چو آصف به حکمت وافر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک را خامه عمادالدین</p></div>
<div class="m2"><p>در خور آمد چو دیده را باصر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشرف الملک در متانت رای</p></div>
<div class="m2"><p>آفتابیست در زمین زاهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راستی را مکان مستوفی</p></div>
<div class="m2"><p>مملکت راست حامی و ناصر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاطرش بحر لطف و کان ذکاست</p></div>
<div class="m2"><p>آفرین باد بر چنین خاطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نایبانی نشسته هم برشان</p></div>
<div class="m2"><p>در صناعات کاتبی فاخر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم بد دور کاین گره هستند</p></div>
<div class="m2"><p>همه عین صواب جز ناظر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی گناه از میان این حلقه</p></div>
<div class="m2"><p>عزلت بنده از چه بود آخر</p></div></div>