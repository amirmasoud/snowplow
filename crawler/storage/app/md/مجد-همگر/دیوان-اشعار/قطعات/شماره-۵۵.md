---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>زمین با فلک گفت دوش از سر عجز</p></div>
<div class="m2"><p>و فی الله من کل حطب جنابک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا رونق و رسم و آئین نمانده ست</p></div>
<div class="m2"><p>ز ملکی که راند اردشیر بن بابک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک گفت بینی و دانی و پرسی</p></div>
<div class="m2"><p>دریغا اتابک دریغا اتابک</p></div></div>