---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>کریم پارس و اصیل عراق شمس الدین</p></div>
<div class="m2"><p>زهی که ملک ز نام تو یافته اعزاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توئی که فکر تو گر بر فلک گذار کند</p></div>
<div class="m2"><p>زلوح چرخ بخواند همه دقایق راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>براق قدر تو گر سرکشد ز عالم کون</p></div>
<div class="m2"><p>به گرد او نرسد آسمان بیهده تاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ابر دست تو گر بر سر جهان بارد</p></div>
<div class="m2"><p>زمانه باز رهد جاودان ز رنج نیاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدست طاعت تو بر جهانیان مفروض</p></div>
<div class="m2"><p>زبهر آنکه توئی از جهانیان ممتاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولای حضرت تو واجب است چون روزه</p></div>
<div class="m2"><p>دعای دولت تو لازم است همچو نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر با همه رفعت بدان شود راضی</p></div>
<div class="m2"><p>که با تو با بد و نیک جهان بود انباز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی مباد که گردد شریک با چو توئی</p></div>
<div class="m2"><p>سپهر سفله افسوس پیشه طناز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدای داد ترا از پی مصالح خلق</p></div>
<div class="m2"><p>دلی رحیم و نهادی کریم و خلق نواز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به حق آنکه دلت را مکان رحمت کرد</p></div>
<div class="m2"><p>که یکزمان دل خود را به حال من پرداز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان نمای ضمیرت مگر نموده بود</p></div>
<div class="m2"><p>که چون نشیب فرو رفت کار من ز فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهانیان همه حیران شدند از آن صنعت</p></div>
<div class="m2"><p>که کرد با من بیچاره چرخ شعبده باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبوری من مسکین به غایتی برسید</p></div>
<div class="m2"><p>که جان همی دهم و هم نمی دهم آواز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز کین دشمن گرگین نهاد سگ سیرت</p></div>
<div class="m2"><p>شدم چو بیژن در کر و فر چنگ گراز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ندیده روی منیژه فتادم اندر چاه</p></div>
<div class="m2"><p>ز مکر دشمن بدگوی و حاسد و غماز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حقیقت است که کیخسرو زمانه توئی</p></div>
<div class="m2"><p>دل تو جام و کفت رستم کمندانداز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به بوی مرغ کز انگشتری دهد خبرم</p></div>
<div class="m2"><p>بمانده ام چو یکی کبک زیر چنگل باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بماند از من و تو یادگار این قصه</p></div>
<div class="m2"><p>اگر ز چاه بلا آوری مرا به فراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا بر اهل هنر منتی بود وافر</p></div>
<div class="m2"><p>اگر ز چنگ عنا جان من رهانی باز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه شمع و عودم و در بزم عیش خود شب و روز</p></div>
<div class="m2"><p>چو عود همدم سوزم چو شمع جفت گداز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همیشه بر سر پایم به خون دل گریان</p></div>
<div class="m2"><p>زبیم آنکه سرم را دهند در دم گاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کجا توانم ازین بوم و بر گرفتن دل</p></div>
<div class="m2"><p>نهاده خلق در این بود سر ز شام و حجاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل شکسته پر و بال من نمی خواهد</p></div>
<div class="m2"><p>که در هوای دگر مملکت کند پرواز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز شهر خویش دو منزل اگر شوم بیرون</p></div>
<div class="m2"><p>هزار مفسده گونه گون کنند آغاز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا گدائی شاه جهان بسی به ازین</p></div>
<div class="m2"><p>که خواندم ملک روم به خسرو انجاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زبان ز مدحت این دولت ار کنم کوتاه</p></div>
<div class="m2"><p>شود زبان جهانی به نقص بنده دراز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به نیم روز و خراسان خبر رسد گر من</p></div>
<div class="m2"><p>به نیم شب بگریزم ز خطه شیراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حدیث بنده و این افترا که ساخته اند</p></div>
<div class="m2"><p>ز بس خجالت گفتن نمی توانم باز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو سیر خورده همان به که کم زنم دم از ان</p></div>
<div class="m2"><p>که توبتو بودش گنده تر بسان پیاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو کارساز جهانی و کارساز تو حق</p></div>
<div class="m2"><p>دراز می نکنم قصه کار من تو بساز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هزار کام بران در جهان به دولت شاه</p></div>
<div class="m2"><p>هزار سال بمان در نشاط و نعمت و ناز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روز نو مه روزه ست خیز و طاعت کن</p></div>
<div class="m2"><p>چو روز عید رسد باده نوش و عشرت ساز</p></div></div>