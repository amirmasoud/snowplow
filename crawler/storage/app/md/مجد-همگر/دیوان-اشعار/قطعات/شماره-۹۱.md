---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>زهی شهریاری که خورشید چرخ</p></div>
<div class="m2"><p>بود پیش رای تو چون شب پره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قضا در مهمات ملک جهان</p></div>
<div class="m2"><p>ز رای تو خواهد همی مشوره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن قلب کاندر تو آورد روی</p></div>
<div class="m2"><p>بدیهه شود میمنه ش میسره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تیغت که بر دشمنان بگذرد</p></div>
<div class="m2"><p>شود روی هامون چو که پر دره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار آمد و پیک شادی رسید</p></div>
<div class="m2"><p>در این روز شادی بود می سره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهر از پی بارگاه تو ساخت</p></div>
<div class="m2"><p>به ترتیب نوروزی نادره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نثار درت کرد سیاره را</p></div>
<div class="m2"><p>ازین هفت در قصر بی کنگره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحرگاه خورشید را با حمل</p></div>
<div class="m2"><p>برآورد ازین برشده منظره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رسم خدم پیش خدمت کشید</p></div>
<div class="m2"><p>غلامی ختائی و یک سر بره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زبزم تو خالی مباد این چهار</p></div>
<div class="m2"><p>ندیم و می و مطرب و مسخره</p></div></div>