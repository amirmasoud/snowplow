---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>تا لعل تو از تنگ شکر بار نگیرد</p></div>
<div class="m2"><p>دل از غم آن لعل شکر بار نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جام لبان چاشنی از قند فکندی</p></div>
<div class="m2"><p>تا لعل لبت تلخی گفتار نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بر سمن و سنبل تو زار نگردم</p></div>
<div class="m2"><p>گر سنبل تو طرف سمنزار نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آتش دل گرد رخت راه ببندم</p></div>
<div class="m2"><p>تا گرد گلستان رخت خار نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دیدن تو زاهد صد ساله شگفت است</p></div>
<div class="m2"><p>گر خرقه نیندازد و زنار نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گریم و گوئی به اشارت که مریزاشک</p></div>
<div class="m2"><p>تا دشمنت از چشم و دل اقرار نگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر لعل در آن لولو شهوار نگیری</p></div>
<div class="m2"><p>روی از مژه ام لولو شهوار نگیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من دل به هوای لب و دندان تو دادم</p></div>
<div class="m2"><p>مانا که بدین جرمم دادار نگیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا دل نشود عاشق هستی نپذیرد</p></div>
<div class="m2"><p>تا زر نشود خالص مقدار نگیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی دو سه دستی به طرب با تو برآرم</p></div>
<div class="m2"><p>گر پای دلم در گل تیمار نگیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>انکار مدار از من ار انکار نداری</p></div>
<div class="m2"><p>تا لوح دلت صورت انکار نگیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از یار کم آزار خود آزار چه گیری</p></div>
<div class="m2"><p>کز یار کم آزار کس آزار نگیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برق نفس گرم من آفاق گرفته ست</p></div>
<div class="m2"><p>وندر دل تو شوخ ستمکار نگیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آهم عجب ار در دل خارا ننشیند</p></div>
<div class="m2"><p>سوزم عجب ار در در و دیوار نگیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خود بر دل تو مهر به مسمار که بندد</p></div>
<div class="m2"><p>کآن سنگ سیاه است که مسمار نگیرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر فاش شود راز جفاهای تو بر من</p></div>
<div class="m2"><p>کس را هوس یار دگربار نگیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زینسان که تو در یاری من راه سپردی</p></div>
<div class="m2"><p>زین پس به جهان هیچ کسی یار نگیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زر رخ و دردانه غلطان سر شکم</p></div>
<div class="m2"><p>نقدیست که در پیش تو بازار نگیرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آهم همه دودیست که بر کس ننشیند</p></div>
<div class="m2"><p>اشکم همه آبیست که بر کار نگیرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زین پس نکنم گریه ننالم نزنم آه</p></div>
<div class="m2"><p>تا آینه روی تو زنگار نگیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتی که دلت را به نصیحت ادبی کن</p></div>
<div class="m2"><p>تا کار سر زلف مرا خوار نگیرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یا در خم این زلف چو زنجیر نپیچد</p></div>
<div class="m2"><p>یا جای در این طره طرار نگیرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یا پای به خود دارد و خاموش نشیند</p></div>
<div class="m2"><p>یا دست بر این زلف زره دار نگیرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در عرض یکی تار کزآن زلف کم آید</p></div>
<div class="m2"><p>صد قافله از تبت و تاتار نگیرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>او زنگی مست است سبکبار و سرانداز</p></div>
<div class="m2"><p>خون صد از آن دل به یکی تار نگیرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای دوست بنه عذر دلم کز همه روئی</p></div>
<div class="m2"><p>کس نکته بر آن سوخته زار نگیرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر دل سر زلف تو یکی بار گرفته ست</p></div>
<div class="m2"><p>گوید که دلت از دل من بار نگیرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کو شیفته رای است و به زنجیر گرفتار</p></div>
<div class="m2"><p>تا خرده بر آن شیفته بسیار نگیرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زنهار مخور با دل من کز همه جرمی</p></div>
<div class="m2"><p>کس را به گنه سخت چو زنهار نگیرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این شعر چو زر نقد روان است وزین روی</p></div>
<div class="m2"><p>از مجد کسی صره دینار نگیرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بس پیرهن کاغذی از دست پوشم</p></div>
<div class="m2"><p>گر دست تو زو کاغذ اشعار نگیرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من صاحب دیوان شوم ار صاحب دیوان</p></div>
<div class="m2"><p>از نسبت همنامی من عار نگیرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن خواجه که بی واسطه منت خوانش</p></div>
<div class="m2"><p>چرخ از مه و خور قرصه ادرار نگیرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن شمس که بی رهبری رایت رایش</p></div>
<div class="m2"><p>خور مملکت گنبد دوار نگیرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بی رقعه پروانه او منشی گردون</p></div>
<div class="m2"><p>در کف و بنان خامه و طومار نگیرد</p></div></div>