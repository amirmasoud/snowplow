---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>زهی رویت مه خوبان آفاق</p></div>
<div class="m2"><p>جمالت عذر خواه درد عشاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیده مثل خلقت چشم مخلوق</p></div>
<div class="m2"><p>نیاورده چو خلقت صنع خلاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت در چار حد زد پنج نوبت</p></div>
<div class="m2"><p>دراین شش سو رواق هفت اطباق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدم چون ذره پنهان در هوایت</p></div>
<div class="m2"><p>ز مهرت گشته ام مشهور آفاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان فتنه کردم عاقبت جای</p></div>
<div class="m2"><p>نهادم عافیت بر گوشه طاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهی را خواندی اندر مهر عاصی</p></div>
<div class="m2"><p>مر او را گفتی اندر عشق زراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مهر اندر بود تغییر احوال</p></div>
<div class="m2"><p>به عشق اندر بود تبدیل اخلاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدی داهی و عاقل چون دلش بود</p></div>
<div class="m2"><p>کنون چون دل بشد آهی شد و عاق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جانت می خورم در عشق سوگند</p></div>
<div class="m2"><p>به مهرت می کنم در عهد میثاق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دشنامت که گوشم راست مژده</p></div>
<div class="m2"><p>به پیغامت که هوشم راست تریاق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مشکین سنبلت بالای لاله</p></div>
<div class="m2"><p>به سیمین سینه ات زیر بغل طاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خفته نرگست در سحر بیدار</p></div>
<div class="m2"><p>به جفت ابرویت در دلبری طاق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به مژگانت که دل را گشت مخلب</p></div>
<div class="m2"><p>به زلفینت که جان را هست معلاق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به سیل اشک من کآبی است خونرنگ</p></div>
<div class="m2"><p>به دود آه من کابری است براق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به چابک خیزی آن بیستون کوه</p></div>
<div class="m2"><p>به نازک طبعی آن سیمگون ساق</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان دو طره طرار سرباز</p></div>
<div class="m2"><p>بدان دو غمزه غماز ایقاق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گلزار رخت نزهتگه دل</p></div>
<div class="m2"><p>کزو گلرنگ و گلچین گردد احداق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به گفتارت که گشتم نیک محتاج</p></div>
<div class="m2"><p>به دیدارت که هستم سخت مشتاق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هجر دلگداز صبر سوزت</p></div>
<div class="m2"><p>کزو دوزخ پذیرد وام احراق</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به روی جانفزای دلفروزت</p></div>
<div class="m2"><p>کز و خورشید گیرد نام اشراق</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خاک سم اسب خسرو عصر</p></div>
<div class="m2"><p>که باشد خسروان را کحل آماق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به نعل اشهب مریخ میخش</p></div>
<div class="m2"><p>که شد گردنکشان را طوق اعناق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به شست او که شد خیاط اجسام</p></div>
<div class="m2"><p>به دست او که شد قسام ارزاق</p></div></div>