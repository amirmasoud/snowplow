---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>سپیده دم چو دمیدن گرفت بوی چمن</p></div>
<div class="m2"><p>هوا ز ژاله گهر بست بر عذار سمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت سمن بر سیماب سینه سرو آسا</p></div>
<div class="m2"><p>به کف چمانه درآمد چمان چمان به چمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چکان چکان خویش از گل زناز بر قرطه</p></div>
<div class="m2"><p>کشان کشان سر زلف دراز در دامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نماز برد بر قامتش چو راهب سرو</p></div>
<div class="m2"><p>سجود کرد بر عارضش سمن چو شمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربود خوب ز نرگس به نرگس پر خواب</p></div>
<div class="m2"><p>شکست پشت بنفشه به زلف پرز شکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشست و ناله ز مرغان صبحخیز بخاست</p></div>
<div class="m2"><p>گشاد چهره و گل پاره کرد پیراهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب را و رهی را چو حلقه بر در ماند</p></div>
<div class="m2"><p>درآمد از در شادی و آنگهی با من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز روی لطف بپیوست همچو می با جام</p></div>
<div class="m2"><p>ز راه مهر بر آمیخت همچو جان با تن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا ز شادی آن آهوی ختن از دل</p></div>
<div class="m2"><p>دمی به کام بر آمد چو بوی مشک ختن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار گوهر شهوار چشم گوهر بار</p></div>
<div class="m2"><p>فشاند در قدم آن نگار سیم ذقن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو بوسه داد مرا از پی سه جام شراب</p></div>
<div class="m2"><p>یکی امید فزای و دوم خمار شکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و گرچه داد مرا خوش بشارتی که شدم</p></div>
<div class="m2"><p>به جان و دل رهی آن زبان وکام و دهن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بشارتی به امید و امان اهل زمان</p></div>
<div class="m2"><p>به یمن موکب و فرقدوم صدر ز من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خجسته سایه و خورشید پایه شمس الدین</p></div>
<div class="m2"><p>که آفتاب زمین است وسایه ذوالمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گزیده سامان آن خواجه حمیده سیر</p></div>
<div class="m2"><p>فریضه فرمان آن صاحب ستوده سنن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به نفس پاک ولی و به جود عام علی</p></div>
<div class="m2"><p>به نام شهره حسین و به خلق خوب حسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نور رای چو افکند سایه بر ملکت</p></div>
<div class="m2"><p>زمانه گفت زهی آفتاب سایه فکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کفش صحایف آمال را زند ترقین</p></div>
<div class="m2"><p>دلش وظایف ارزاق را کند روشن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایا شبیه تو نادیده دهر صافی فهم</p></div>
<div class="m2"><p>و یا نظیر تو نازاده چرخ صائب ظن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر تجللی نور دلت فتد بر طور</p></div>
<div class="m2"><p>اساس طور شود همچو سرمه در هاون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز نظم ملک فلک ذهنت ار براندیشد</p></div>
<div class="m2"><p>نجوم نقش شود مجتمع چو نقش پرن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و گر ز تفرقه و رنج خاطری که مباد</p></div>
<div class="m2"><p>نظر کنی سوی این خنگ سرکش توسن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بیم فکرت تو دسته گل پروین</p></div>
<div class="m2"><p>بسان نعش ز هم بگسلد در این گلشن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو ابر دست تو باران جود درگیرد</p></div>
<div class="m2"><p>بسا که گرید و نالد سحاب در بهمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدنگ غیرت کف تو چون روان گردد</p></div>
<div class="m2"><p>محیط ژرف شود چون قدیر در جوشن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به جرم کیوان زان نسبت است آهن را</p></div>
<div class="m2"><p>که کرد وقف سراپای دشمنت آهن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به کلک فتوی ازان دست می برد بر جیس</p></div>
<div class="m2"><p>که حکم سفک کند تا نماندت دشمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به قصد خصم تو بهرام چون کمین گیرد</p></div>
<div class="m2"><p>کجا برآرد سر بدسگالت از مکمن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر نه پیروی ذات تو کند خورشید</p></div>
<div class="m2"><p>چراغ چرخ شود بی فتیله و روغن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مغنی سومین طارم ارنه بر کامت</p></div>
<div class="m2"><p>دمی زند شود آواز مزمرش شیون</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگرنه تیر کمان قد شود به خدمت تو</p></div>
<div class="m2"><p>قدر بدوزد کلک و کفش به تیر محن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مه ار جوی ز هوای تو کم کند در دل</p></div>
<div class="m2"><p>قضا به آتش نکبت بسوزدش خرمن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهان پناها آب لطافت سخنت</p></div>
<div class="m2"><p>ز روی لوح دل من بشست گرد حزن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو تر و تازه به پرسش درآمدی تر شد</p></div>
<div class="m2"><p>زبان بنده به آزادی تو چون سوسن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به فرق قدر تو بر فکر من به قدر نثار</p></div>
<div class="m2"><p>هزار در ثمین ریخت بی قبول ثمن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدان خدای که صباغ صنعش از دل خاک</p></div>
<div class="m2"><p>به رنگ مختلف آرد نتایج معدن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که یک لطیفه ز درج درت به لفظ قبول</p></div>
<div class="m2"><p>مرا به آید از صد خزانه در عدن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بس که دیدم رنج و عنا ز جور لئام</p></div>
<div class="m2"><p>ز بس که خوردم جام جفا ز دست فتن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سرم ملول شد از جستن دنا ودنی</p></div>
<div class="m2"><p>دلم نفور شد از دیدن دیار و سکن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از آن ز شاهی مرغان ملول شد سیمرغ</p></div>
<div class="m2"><p>که یافت فرق خروس لئیم با گرزن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گذاشت طوطی و طاووس و باز را و همای</p></div>
<div class="m2"><p>ز ننگ صبحت خفاش و بوم و زاغ وزغن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هزار جوهر کان پیش نهمتم یک جو</p></div>
<div class="m2"><p>هزار جان بر سیمرغ همتم ارزن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرشته ئیست مرا در دماغ صائب فکر</p></div>
<div class="m2"><p>که روح پاک همی بخشدم به جای سخن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نزول آن به دل وجان تیره ممکن نیست</p></div>
<div class="m2"><p>چه مرد اهلی جبریل باشد اهریمن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کجا به نفس بهیمی در آید این معنی</p></div>
<div class="m2"><p>که نفس ناطقه در شرح آن بود الکن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کجا به راستی این سخن رسد کژدان</p></div>
<div class="m2"><p>کجا معارضی این نمط کند کودن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مسافریست لطیف و غریب گفته من</p></div>
<div class="m2"><p>ولی به چاه عنا در چو یوسف و بیژن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سخن سخیف و رکیک آن بود که در پستی</p></div>
<div class="m2"><p>وطن به دامن صاحب سخن کند موطن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چهار ربع زمین نظم ونثر من دارد</p></div>
<div class="m2"><p>ز مصر تا به ختا و ز روم تابه ختن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حکیم جوهر باقی رسد معانی را</p></div>
<div class="m2"><p>ز پارس جوهر من تحفه بر سوی مسکن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شهان سلغری از عشق طرز من در خاک</p></div>
<div class="m2"><p>به دست واقعه بر خود همی درند کفن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بقای ذات تو جاوید باد تا باشی</p></div>
<div class="m2"><p>هزار نسل مرا چون پدر به پاداشن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سر حبیب ترا تاج فخر بر تارک</p></div>
<div class="m2"><p>تن عدوی ترا تیغ قهر بر گردن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سرای جاه ترا از شرف ستون سما</p></div>
<div class="m2"><p>نهال عمر ترا از بقا غصون و غصن</p></div></div>