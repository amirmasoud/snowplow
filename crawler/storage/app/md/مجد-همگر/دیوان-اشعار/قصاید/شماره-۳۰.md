---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>بر من زمانه کرد هنرها همه وبال</p></div>
<div class="m2"><p>وز غم بریخت خون جوانیم چرخ زال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تنگنای حلقه این اژدهای پیر</p></div>
<div class="m2"><p>شد چون لعاب افعی در حلق من زلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلکم ز دست بستد تیر حسود طبع</p></div>
<div class="m2"><p>بر من کمین گشاد سپهر کمان مثال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افعی ست با من این تتق سبز زرنگار</p></div>
<div class="m2"><p>ما در خیال آنکه عروسی ست با جمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس زود بگسلد ز هم این ششدر کهن</p></div>
<div class="m2"><p>ایمن شویم ز آفت این هفت کوتوال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک صبحدم ز باد دم سرد برکشیم</p></div>
<div class="m2"><p>از روی این عماری زنگارگون هلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون زلف یار کرد مرا چرخ خیره سر</p></div>
<div class="m2"><p>چون خال دوست کرد مرا دهر تیره حال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخا چه خواهی از من عور برهنه پای</p></div>
<div class="m2"><p>دهرا چه جوئی از من زار شکسته بال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای روزگار سفله علی رغم بخت من</p></div>
<div class="m2"><p>گوهر به سنگ بشکن و در تاج نه سفال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیسی زنده را به دوسیم سیه مخر</p></div>
<div class="m2"><p>وز زربساز سم خر مرده را نعال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از چشم باز توخته کن لقمه های بوم</p></div>
<div class="m2"><p>وز ران شیر ساخته کن طعمه شغال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای چشم بخت خفته شو و زین سپس مبین</p></div>
<div class="m2"><p>وی شاخ کام خشک شو و زین سپس مبال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای دل هزار جور دمادم کش و مجوش</p></div>
<div class="m2"><p>وی تن هزار زخم پیاپی خور و منال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای پای پیل فتنه مرا خردتر بکوب</p></div>
<div class="m2"><p>وی دست چرخ سفله مرا سخت تر بمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از مالشی که یافت دلم روشنی گرفت</p></div>
<div class="m2"><p>روشن شود هر آینه آئینه از صقال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از زخم تو چو طبل ننالم به هیچ روی</p></div>
<div class="m2"><p>ورخود ز پشت من به مثل برکشی دوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درشست حادثات چو ماهی بمانده ام</p></div>
<div class="m2"><p>نه روی استقامت و نه رای ارتحال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گردون چو دام ساخت چه درمان جز انقیاد</p></div>
<div class="m2"><p>ایزد چو حکم کرد چه چاره جز امتثال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرسوده گشتم از کف هر کوب چون نمک</p></div>
<div class="m2"><p>آلوده گشتم از دهن خلق چون خلال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کارم تمام گشته و با نور همچو بدر</p></div>
<div class="m2"><p>نقصان گرفت و تیره شد از غایت کمال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مخفی شدم ز تهمت بدگوی چون قمر</p></div>
<div class="m2"><p>نابوده هیچ با رخ خورشیدم اتصال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ترسم چو از محاق تواری برون شوم</p></div>
<div class="m2"><p>در من کشند مرد وزن انگشت چون هلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در کنج انزوا ز نهیب عری خصم</p></div>
<div class="m2"><p>فارغ نیم دمی چو شه رقعه ز انتقال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وقتی چنین که شاخ گل از خاک بردمید</p></div>
<div class="m2"><p>طالع نگرکه بخت مرا خشک شد نهال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شش ماه شدکه می نشناسم ز روز شب</p></div>
<div class="m2"><p>ترسم که اخترم به سر آید دراین وبال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر من نتافت روز زمستان فروغ مهر</p></div>
<div class="m2"><p>بر من نجست وقت بهاران دم شمال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راضی شدم به فرصت دشمن در این عنا</p></div>
<div class="m2"><p>سیر آمدم ز جان و جوانی دراین ملال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عیبم همین که نیستم از نطفه حرام</p></div>
<div class="m2"><p>جرمم همین که زاده ام از نسبتی حلال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هستم ز نسل ساسان نز تخمه تکین</p></div>
<div class="m2"><p>هستم ز صلب کسری نز دوده نیال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دارم به قدر خویش هنر ریزه وز آن</p></div>
<div class="m2"><p>دارد زمانه بامن مسکین سر جدال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شعری به خوش مذاقی چون چاشنی وصل</p></div>
<div class="m2"><p>کلکی به نقشبندی چون صورت خیال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه رنگ همتم ببرد زنگ بغض و بخل</p></div>
<div class="m2"><p>نه پای همتم بخلد خار جاه و مال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زفتی ندیده چشم کس از من به وقت جود</p></div>
<div class="m2"><p>لا ناشنیده گوش کس از من گه سئوال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جز با هنر نبوده دلم را نشست و خاست</p></div>
<div class="m2"><p>جز با کتب نبوده مرا هیچ قیل و قال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تشویر رد کس نبرد صدق این سخن</p></div>
<div class="m2"><p>گر بر محک عقل زنندم بدین خصال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عمرم ز سی گذشت و نگشتم به عمر شاد</p></div>
<div class="m2"><p>جان در فراق رفت و ندیدم رخ وصال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فصل ربیع عمر چو سی سال بود رفت</p></div>
<div class="m2"><p>زان باقی ام چه سود اگر هست شصت سال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل را نشاط لهو نباشد پس از شباب</p></div>
<div class="m2"><p>خورشید را فروغ کم آید گه زوال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر سنگ خاره گوش کند ماجرای من</p></div>
<div class="m2"><p>رحمت کند بر این تن بیچاره لامحال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای مرغ صبح خوان به نوا این سخن بخوان</p></div>
<div class="m2"><p>در بارگاه شاه جهان گر بود مجال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دانم که شه ز بهر سخن باز جویدم</p></div>
<div class="m2"><p>داند اگر که نیست دراین فن مرا همال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هم در حمایت آوردم عفو شهریار</p></div>
<div class="m2"><p>هم در پناه گیردم الطاف ذوالجلال</p></div></div>