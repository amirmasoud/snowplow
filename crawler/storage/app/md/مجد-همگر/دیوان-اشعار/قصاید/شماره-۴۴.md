---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>زهی انا مل و کلکت گره گشای جهان</p></div>
<div class="m2"><p>جهان نمای ضمیر تو رهنمای جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پایه کنف تست انتهای سپهر</p></div>
<div class="m2"><p>به سایه شرف تست التجای جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه به حسبت وداد تو افتخار وجود</p></div>
<div class="m2"><p>همه به رسم و نهاد تو اقتدای جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دستیاری تقدیر مثل تو ننهاد</p></div>
<div class="m2"><p>ز حد کتم عدم پای در فضای جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نپرورید شبیه ترا جهان خدای</p></div>
<div class="m2"><p>نیافرید نظیر ترا خدای جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم همت وجود توای جهان سخا</p></div>
<div class="m2"><p>به نیم ذره نسنجد همه غنای جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه پاس تو بانگی زدی بر این جافی</p></div>
<div class="m2"><p>نماندی اثری از همه جفای جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پای و هم به گرد جهان دویدم و نیست</p></div>
<div class="m2"><p>کسی نظیر تو و نیست خود ورای جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکرد قدر تو بر هیچ وهم جلوه از آنک</p></div>
<div class="m2"><p>زیادت آمد قدر تو از ازای جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فروگرفت ز سر قدر تو کلاه ارنی</p></div>
<div class="m2"><p>به جنب قدر تو تنگ آمدی قبای جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به صد دل است جهان بر کمال توعاشق</p></div>
<div class="m2"><p>که دلنواز و جودی و دلربای جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضیعف رای حسود تو آن گمان دارد</p></div>
<div class="m2"><p>که در ولای تو فاتر شود قوای جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محمد اسما بر تو لقب چه بندم از آنک</p></div>
<div class="m2"><p>تو هم جهان بهائی و همبهای جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رتبت است و بهی روی تو جهان بها</p></div>
<div class="m2"><p>به قیمت است یکی موی تو بهای جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مسیح معجزتا قدرتی نمای که شد</p></div>
<div class="m2"><p>ز بس عفونت و فتنه پی هوای جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به جز معالج رایت که شربتیش دهد</p></div>
<div class="m2"><p>کزان امید توان بست در شفای جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به جای ماند جهان را به یک نظر پاست</p></div>
<div class="m2"><p>اگرنه پاس تو ماندی به جای وای جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان به جای تو غیری کجا قبول کند</p></div>
<div class="m2"><p>بدین لطیفه که تو کرده ای به جای جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون به قوت عدلت حشاشه ای مانده است</p></div>
<div class="m2"><p>دریغ و درد که گر کم کنی دوای جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان پناها در سایه تو آن خاکم</p></div>
<div class="m2"><p>که سایه نفکنم از ناز بر همای جهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز کبر و عجب نه حاشا که من نه آن بازم</p></div>
<div class="m2"><p>که بنگرم به کرشمه به کبریای جهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حریص صید حضیضی نیم بر آن رایم</p></div>
<div class="m2"><p>که گیرم اوج حقیقی ز تنگنای جهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دمی مباد ز جان عزیز بر خوردار</p></div>
<div class="m2"><p>کسی که نفس به خواری دهد برای جهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به حق فقر و توانگر دلی که در خور نیست</p></div>
<div class="m2"><p>همه غنای جهانم به یک عنای جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ایا بهست از ین خوان که کشتمان آبا</p></div>
<div class="m2"><p>از آنکه جیفه مسموم شد ابای جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو عندلیب به مدحت هزار دستانم</p></div>
<div class="m2"><p>که خرم است بدین داستان سرای جهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به هر سرای ز نظم من است سوری از آنک</p></div>
<div class="m2"><p>سخنسرای شهانم سخنسرای جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان به روز جوانیم رنگ پیری داد</p></div>
<div class="m2"><p>دهاد ایزد داور به حق سزای جهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نگار سبز سیه دل ز من گریزد از آنک</p></div>
<div class="m2"><p>سرم سپید شد از گرد آسیای جهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا تو گوئی در راه آسیا دیده ست</p></div>
<div class="m2"><p>چنان ستمگر دوران و بیوفای جهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو دیرمان که نکوکاری و وفاداری</p></div>
<div class="m2"><p>که ناگزیر همین باشد اقتضای جهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو خیر خواه جهانی به روز و شب بادا</p></div>
<div class="m2"><p>به خیر در پی تو سال و مه دعای جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نفاذ امر تو سر بسته باقضای قضا</p></div>
<div class="m2"><p>بقای عمر تو پیوسته با بقای جهان</p></div></div>