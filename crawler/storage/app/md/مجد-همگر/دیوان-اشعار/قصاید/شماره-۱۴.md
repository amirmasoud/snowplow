---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>نهادم از بن هر موی بر کشد فریاد</p></div>
<div class="m2"><p>ز دوستان که زمن شان همی نیاید یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خروش برکشم از دل چو کبک در دم باز</p></div>
<div class="m2"><p>بنالم از همه رگها چو چنگ در ره باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر زمانه چنین بد نهاد شد با من</p></div>
<div class="m2"><p>کجا شدند مرا دوستان نیک نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلی نهاد زمانه چو بد شود ز قضا</p></div>
<div class="m2"><p>زمانه رنگ شود هر که از زمانه بزاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دراین زمانه خود کام از که جویم کام</p></div>
<div class="m2"><p>در این کشاکش بیداد از که خواهم داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک به کینه احرار تا کمر دربست</p></div>
<div class="m2"><p>به جز کمان نکشید و به جز کمین نگشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزای مشتری و خور چنان حزینم کرد</p></div>
<div class="m2"><p>که لحن زهره نگرداندم دگر دلشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروش کوس شهانم چو یاوری ننمود</p></div>
<div class="m2"><p>صریر چرخ زمانم کجا رسد فریاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بر قبول سلاطین نبود بنیادی</p></div>
<div class="m2"><p>مرا قبول شیاطین کجا نهد بنیاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلا مجوی سلامت ز آشیان وجود</p></div>
<div class="m2"><p>که بر ندامت و حسرت نهاده اندش لاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی که خاک تو بسرشت بی عنانسرشت</p></div>
<div class="m2"><p>هر آنکه اصل تو بنهاد بی بلا ننهاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دراین زمان که خرد را نماند هیچ مجال</p></div>
<div class="m2"><p>در این مکان که هنر را نماند هیچ ملاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نماند جهان خواجه جهان مانده ست</p></div>
<div class="m2"><p>وگر بمرد ملک قطب ملک باقی باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایگان وزیران شرق شمس الدین</p></div>
<div class="m2"><p>که هست خاک درش غیرت کلاه قباد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به کف کریم و به چهره بهی به سیرت خوب</p></div>
<div class="m2"><p>به تن حلیم و به دل صابر و به شیمت راد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عروس ملک جهان شد بر او چنان عاشق</p></div>
<div class="m2"><p>که تا به حشر نبیند رخ دگر داماد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گرد عالم ملک آمد آن بنان و قلم</p></div>
<div class="m2"><p>که قصر ملک به تاییدش استوار استاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر تو نیستی آن نایب نبی بحق</p></div>
<div class="m2"><p>به سعی تو نشدی خانه هدی آباد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به عهد تو نشدی ملت از خلل خالی</p></div>
<div class="m2"><p>به بذل تو نشدی امت از زلل آزاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لطیفه ای ز حساب جمل مراست چنان</p></div>
<div class="m2"><p>کز این دو لفظ بر آید صد و دو با هفتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلفظ صاحب دیوان همین بر آید عقد</p></div>
<div class="m2"><p>دراین تساوی انصاف بنده باید داد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مراست حق دعائی بر اهل این دولت</p></div>
<div class="m2"><p>چو فر صاحب مغفور بر رهی افتاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زکلک چون صدف و از بنان همچو خلج</p></div>
<div class="m2"><p>چه در که صاحب ماضی به بنده نفرستاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ربیع بختا در بوستان دولت تو</p></div>
<div class="m2"><p>مرا بهار و دی آزاد یافت چون شمشاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه حاجتیم به پیوند ساغر نوشین</p></div>
<div class="m2"><p>نه رغبتیم به دلبند کشور نوشاد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز من حکایت پارین مپرس و آن اکرام</p></div>
<div class="m2"><p>ز من شکایت امسال بین و این بیداد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان بدم ز توفر که کس چو بنده نبود</p></div>
<div class="m2"><p>چنان شدم ز تحیر که کس چو بنده مباد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز خاک پارس زلال چنین سخن مطلب</p></div>
<div class="m2"><p>که ناید آب ز سندان و روغن از پولاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لقای گلبن خوشبوی را مجوی از خار</p></div>
<div class="m2"><p>نوای بلبل خوشگوی را مجوی از خاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>متاع کرج نخیزد ز رشته تنکت</p></div>
<div class="m2"><p>قماش هند نخیزد ز تربت بغداد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عمارت کف فرهاد ناید از شیرین</p></div>
<div class="m2"><p>عبارت لب شرین نیاید از فرهاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به چشم رحم نگه کن مرا ز روی کرم</p></div>
<div class="m2"><p>که روی جاه ترا ز خم چشم بدمرساد</p></div></div>