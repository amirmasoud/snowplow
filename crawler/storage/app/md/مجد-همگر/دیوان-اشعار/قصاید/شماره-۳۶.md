---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>که می برد ز من خسته دل به یار پیام</p></div>
<div class="m2"><p>که می رساندش از لفظ من درود و سلام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرامجال بود کز ملال خاطر او</p></div>
<div class="m2"><p>در افکند سخن من علی الخصوص پیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کراست زهره که با آن نگار زهره جبین</p></div>
<div class="m2"><p>حدیث من کند آغاز از سر اکرام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ماجرای من او را که می کند آگاه</p></div>
<div class="m2"><p>ز واقعات من او را که می کند اعلام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گوش او که رساند فغان و ناله من</p></div>
<div class="m2"><p>که بوئی آورد از زلف او مرا به مشام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که می رود که بگوید که در فراق رخت</p></div>
<div class="m2"><p>جداشد از دل من صبر وز تنم آرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که می رود که مرا پیش یار یاد کند</p></div>
<div class="m2"><p>که می رود که مرا نزد او برد پیغام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که می رود که بگوید که خون مات حلال</p></div>
<div class="m2"><p>ولیک بی منت این عیش و کام باد حرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که می دهد خبر آن نگار مهر گسل</p></div>
<div class="m2"><p>که نیم مرده عشقت تمام گشت تمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حال زار من او را خبر دهید کسی</p></div>
<div class="m2"><p>که سوختم ز غم آخر چه می خوری می خام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا دلیست به صد پاره بی تو صبر چه سود</p></div>
<div class="m2"><p>که هیچ می نپذیرد به صبر و جهد انجام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به لب رسید مرا جان در آرزوی لبت</p></div>
<div class="m2"><p>چه وقت آنکه تو برلب نهاده ای لب جام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه روز آنکه تو در صبحدم خوری باده</p></div>
<div class="m2"><p>که روز عمرم من خسته دل رسید به شام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من از غم تو خود و دوستان نشسته به غم</p></div>
<div class="m2"><p>تو پیش دشمن و بدگوی من نشسته به کام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من از فراق تو سرگشته ام به کوه و کمر</p></div>
<div class="m2"><p>تو همچو کوه کمر بسته ای به کینه مدام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بس که از غم هجرت فسرده گشت دلم</p></div>
<div class="m2"><p>گهی به کوه کنم جای و گه به باغ مقام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شدند شیفته از آه من وحوش و طیور</p></div>
<div class="m2"><p>بسوخت بر من مسکین دل سوام و هوام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز شوق روی تو در صبحدم به یاری من</p></div>
<div class="m2"><p>ادا کنند نواساری و چکاو و حمام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به خواب در سحری این غزل ز پرده راست</p></div>
<div class="m2"><p>سماع کرده ام از بلبلی فصیح کلام</p></div></div>