---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>جاءالشتاء و مل الدجی ظل</p></div>
<div class="m2"><p>بالحب و الراح این التوسل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خز به خرگه بامنقل و مل</p></div>
<div class="m2"><p>این نکته یاد آر کالبرد یقبل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برف است ریزان در پای گلبن</p></div>
<div class="m2"><p>زاغ است تازان بر جای بلبل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درحلق نخجیر آب است زنجیر</p></div>
<div class="m2"><p>درگردن واک موج است چون غل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز سپید است بر شاخساران</p></div>
<div class="m2"><p>کز سیم دارد منقار و چنگل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طرف بستان از لحن ودستان</p></div>
<div class="m2"><p>وز شور مستان گرنیست غلغل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می نوش و بشنو هر یک دم ازنو</p></div>
<div class="m2"><p>از بیشه غلغل وز شیشه قلقل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بردار کامی از عمر باقی</p></div>
<div class="m2"><p>تا کی تهاون تا کی تغافل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مشنو که گردون راد است یا زفت</p></div>
<div class="m2"><p>منگر که گیتی خار است یا گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در زیر گردون ناید مسلم</p></div>
<div class="m2"><p>جاه از تغیر مال از تبدل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر گشت بی بر باغ از زمستان</p></div>
<div class="m2"><p>برساز باغی با هر تجمل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از چهره لاله و ز غمزه نرگس</p></div>
<div class="m2"><p>وز خط بنفشه وز زلف سنبل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر گل پدید آر زان روی تشویر</p></div>
<div class="m2"><p>بر سرو بشکن زان قد تمایل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای چشم مستت عین تعدی</p></div>
<div class="m2"><p>زلف چو شستت اصل تطاول</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بربوی وصلت تا کی صبوری</p></div>
<div class="m2"><p>با بار هجرت تا کی تحمل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فارحم سقامی یاذالترحم</p></div>
<div class="m2"><p>وشف غرامی یا ذالتفضل</p></div></div>