---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه چو باد ناتوانی</p></div>
<div class="m2"><p>با باد به بوی همعنانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچ از تو به جان خرند عشاق</p></div>
<div class="m2"><p>بر باد دهی به رایگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهونئی و نسیم مشکین</p></div>
<div class="m2"><p>باد از تو برد به ارمغانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون باد سبکسری و گه گاه</p></div>
<div class="m2"><p>در بند زری و سر گرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنجیر باد سبکسری و گه گاه</p></div>
<div class="m2"><p>در بند زری و سر گرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنجیر هزار حلقه ای زان</p></div>
<div class="m2"><p>بندی داری هزار گانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صیاد نیی و دلشکاری</p></div>
<div class="m2"><p>شمشیر نئی و سرفشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه در پی سرو پایمالی</p></div>
<div class="m2"><p>گه بر سر لاله سایبانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه معجز صاحب زبوری</p></div>
<div class="m2"><p>گه مار پیمبر شبانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه چنبر گردن نسیمی</p></div>
<div class="m2"><p>گه حلقه گوش ارغوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه پیش افتی و در کناری</p></div>
<div class="m2"><p>گه با کمری و در میانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زنگی بچه دلستان نباشد</p></div>
<div class="m2"><p>تو زنگی شوخ دلستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر دل دزدی چرا به صورت</p></div>
<div class="m2"><p>هندوی سیاه پاسبانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابروی مسلسل وسیاهی</p></div>
<div class="m2"><p>یا بر سر آتشی دخانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ظلمات سکندری و یابند</p></div>
<div class="m2"><p>در سایه ات آب زندگانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر چند که تیره و درازی</p></div>
<div class="m2"><p>همچون شب وصل دل نشانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قلب روزی به لفظ تازی</p></div>
<div class="m2"><p>وز چهره به رنگ قلب کانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شش بر سر لفظ قلب کل نه</p></div>
<div class="m2"><p>کاندر لغت دری تو آنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آشفته و تیره ای و دلگیر</p></div>
<div class="m2"><p>چون خط نجیب دامغانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن بحر مکارم و معالی</p></div>
<div class="m2"><p>وان کان لطافت ومعانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن کز قلم است ابن مقله</p></div>
<div class="m2"><p>وان کز کلم است ابن هانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای قهر تو انتهای پیری</p></div>
<div class="m2"><p>وی لطف تو مبدا جوانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با بزم تو لیل یوم عیش است</p></div>
<div class="m2"><p>باقی باشد جهانی فانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای لعل تو آب زندگانی</p></div>
<div class="m2"><p>وی وصل تو عمر جاودانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ماهی و چو مه نه دلنوازی</p></div>
<div class="m2"><p>مهری و چنو نه مهربانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چشم ار برود تو نور چشمی</p></div>
<div class="m2"><p>جان ار ببری به جای جانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مجنون توام به جانسپاری</p></div>
<div class="m2"><p>لیلی منی به دلستانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از عشق منم فسانه شهر</p></div>
<div class="m2"><p>وز حسن تو فتنه جهانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرهاد توام به تلخ عیشی</p></div>
<div class="m2"><p>شیرین منی به خوش زبانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر رحم کنی تو در خورم من</p></div>
<div class="m2"><p>ور جان خواهی سزای آنی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ترسم که دلت بماند از من</p></div>
<div class="m2"><p>گویم که به حور و ماه مانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چه حور که خوش تر از بهشتی</p></div>
<div class="m2"><p>چه ماه که مهر آسمانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از شنگی فتنه زمینی</p></div>
<div class="m2"><p>وز شوخی شورش زمانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صد جیب ز مشک پرکنددل</p></div>
<div class="m2"><p>گردامن زلف برفشانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر بگشائی دهان خورد جان</p></div>
<div class="m2"><p>صد تنگ شکر به رایگانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از جان خواهم که در وثاقت</p></div>
<div class="m2"><p>باشم شب وروز ایرمانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گاهی به درت به خاکروبی</p></div>
<div class="m2"><p>گه بر بامت به پاسبانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رنجم منما که ناتوانم</p></div>
<div class="m2"><p>دریاب مرا که می توانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خطی که بداده ای به وصلم</p></div>
<div class="m2"><p>ای وصل تو آب زندگانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر روز هزار ره ببوسم</p></div>
<div class="m2"><p>بر دیده و دل نهم نهانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زان روز شمار کار من هست</p></div>
<div class="m2"><p>کاغذ بوسی و رقعه خوانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یا محو شود سیاهی او</p></div>
<div class="m2"><p>زین اشک روان ارغوانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا چند به یاد وعده کژ</p></div>
<div class="m2"><p>بر آتش حسرتم نشانی</p></div></div>