---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ای جمال تو رونق گلزار</p></div>
<div class="m2"><p>بنده زلف تو نسیم بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف مشکین به گرد روی نکوت</p></div>
<div class="m2"><p>چون بر اطراف آفتاب غبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او کوثر و بهشت ندید</p></div>
<div class="m2"><p>گو ببین اینک آن لب و رخسار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب و رخسار تو ز چشم و دلم</p></div>
<div class="m2"><p>بسکه بر بوده اند خواب و قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلستان جان زغمزه تو</p></div>
<div class="m2"><p>آهوانند جمله شیر شکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود ندانم چرا کند شب و روز</p></div>
<div class="m2"><p>چشم مستت مرا اسیر خمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه چنان مستم از می عشقت</p></div>
<div class="m2"><p>که شوم تا به سالها هشیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسن روی تو زیور خوبیست</p></div>
<div class="m2"><p>نیستش حاجتی به رنگ و نگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه خوبی حمال مهوش تست</p></div>
<div class="m2"><p>چتر او چیست زلف عنبر بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به رخساره تو نسبت کرد</p></div>
<div class="m2"><p>گرم شد آفتاب را بازار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه شود گر فلک ترا با من</p></div>
<div class="m2"><p>در میان آورد به بوس و کنار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا کند طبع من در آن حالت</p></div>
<div class="m2"><p>مدح فرمانده جهان تکرار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صاحب اعظم آنکه عالم را</p></div>
<div class="m2"><p>روی او هست عالم الاسرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه فیض نوال رافت او</p></div>
<div class="m2"><p>محو کرده ست ظلم را آثار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای محیط جهان قدر تورا</p></div>
<div class="m2"><p>آسمان شکل نقطه پرگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درو گردون به صد قران دیگر</p></div>
<div class="m2"><p>ناورد چون توئی به هشت و چهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زانکه شمشیر آبدار تو هست</p></div>
<div class="m2"><p>بازوی شرع احمد مختار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صاحبابنده کمینه که هست</p></div>
<div class="m2"><p>طاعتت را به جان پذیرفتار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به یسارت چو او همیشه یمین</p></div>
<div class="m2"><p>که یمینی تو قبله هست یسار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه هر دم هزار شکر کند</p></div>
<div class="m2"><p>در حقیقت یکی بود ز هزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور دهد شرح آرزومندی</p></div>
<div class="m2"><p>از یکی شمه پرشود طومار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور اجازت دهد مکارم تو</p></div>
<div class="m2"><p>کنم احوال خویشتن اظهار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوید آنکس منم که خوانندم</p></div>
<div class="m2"><p>همگنان بحر جود و کوه وقار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون برآرم حسام را زنیام</p></div>
<div class="m2"><p>روز روشن برآرم از شب تار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزه دارند مژگنان از من</p></div>
<div class="m2"><p>که به خون جگر کنند افطار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کرده ام با جهود و نصرانی</p></div>
<div class="m2"><p>آنچه کرده ست حیدر کرار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زین دو ملت به خطه موصل</p></div>
<div class="m2"><p>هر که را بینی از صغار و کبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا بود در برش علامت زرد</p></div>
<div class="m2"><p>یا بود بسته در میان زنار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بوستانی بساختم دردین</p></div>
<div class="m2"><p>که همه زنده رغبت آرد بار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با چنین شوکت و توانائی</p></div>
<div class="m2"><p>با چنین سروری و استظهار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با سگ اندر جوال چون باشم</p></div>
<div class="m2"><p>من که با شیر کرده ام پیکار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بندگان تو آفتاب محل</p></div>
<div class="m2"><p>آستان تو آسمان مقدار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کرده حکمت بر آسمان میدان</p></div>
<div class="m2"><p>گشته رایت بر آفتاب سوار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کیمیائیست رای صائب تو</p></div>
<div class="m2"><p>که کندعقل را تمام عیار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>لاجرم طبع فضل پرور تو</p></div>
<div class="m2"><p>دارد از ملک هر دو عالم عار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شیر قدر تو آهنین مخلب</p></div>
<div class="m2"><p>مرغ امر تو آتشین منقار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لطف و قهر تو اصل شادی و غم</p></div>
<div class="m2"><p>مهروکین تو عین منبرودار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بی وجود سحاب دولت تو</p></div>
<div class="m2"><p>دوحه سلطنت نیارد بار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بی نسیم رضای خدمت تو</p></div>
<div class="m2"><p>گلشن مملکت نریزد خار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>افتخارت کمینه فرمانبر</p></div>
<div class="m2"><p>روزگارت کمینه خدمتکار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آستانت مهذب فضلا</p></div>
<div class="m2"><p>بارگاه تو منزل اخیار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کلک تو درنظام ملت و ملک</p></div>
<div class="m2"><p>برده از دیده قدر مقدار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رای تو در امور دولت و دین</p></div>
<div class="m2"><p>کرده بر چهره رضا رفتار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو وزیری و بندگان درت</p></div>
<div class="m2"><p>سلطنت می کنند در اقطار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می کنم بر طریقه شعرا</p></div>
<div class="m2"><p>بیتی از شعر بوالفرج امضار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زانکه نظمش به نزد اهل هنر</p></div>
<div class="m2"><p>بفزاید طراوت گفتار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>« چرخ پست است و همت تو بلند</p></div>
<div class="m2"><p>دهر مست است ورای تو هشیار»</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نیست دردی چو خست شرکا</p></div>
<div class="m2"><p>که کند سنگ خاره را بیمار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>آنچه می بینم از جلال الدین</p></div>
<div class="m2"><p>کس ندید از زمانه غدار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>صبح پیری طلوع کرد و هنوز</p></div>
<div class="m2"><p>نشد از خواب کودکی بیدار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>غره مال گشت و بی خبر است</p></div>
<div class="m2"><p>از غرور جهان مردمخوار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خود نداند که شهریاری نیست</p></div>
<div class="m2"><p>جز به مردی و دانش و ایثار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خواجه شاعران سنائی را</p></div>
<div class="m2"><p>هست بیتی عظیم ولایق کار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>«هر که از چوب مرکبی سازد</p></div>
<div class="m2"><p>مرکب آسوده است و غره سوار»</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بندگان تو گرچه بسیارند</p></div>
<div class="m2"><p>تو مرا در حسابشان مشمار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زانکه من شیر بیشه ظفرم</p></div>
<div class="m2"><p>دیگران نقش شیر بر دیوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا که دولت ملازمت بودم</p></div>
<div class="m2"><p>بودم از دولت تو دولتیار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همه کس را به من وسیلت بود</p></div>
<div class="m2"><p>این رسائل نوشته آن اشعار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>این زمان کز خودم جدا کردی</p></div>
<div class="m2"><p>شد دلم یار غصه و تیمار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون توام برگرفته ای اول</p></div>
<div class="m2"><p>آخرم بیش ازین فرو مگذار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا بود چار طبع و پنج حواس</p></div>
<div class="m2"><p>تا بود هفت گنبد دوار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بادت اندر جهان چو دولت بخت</p></div>
<div class="m2"><p>نصرت و فتح بر یمین و یسار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خاک پایت چو این قصیده من</p></div>
<div class="m2"><p>ریخته آب لولو شهوار</p></div></div>