---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>دوش چو کرد آسمان افسر زر ز سر یله</p></div>
<div class="m2"><p>ساخت ز ماه و اختران یاره و عقد و مرسله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکل فلک خراش شد مهر چو دانه آس شد</p></div>
<div class="m2"><p>عقده راس داس شد از پی کشت سنبله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طرف جیبن نمود ماه از طرف بساط شاه</p></div>
<div class="m2"><p>آمده با قبول و جاه از قبل مقابله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهره چو شیر خشمگین کرده به مکمنی کمین</p></div>
<div class="m2"><p>بر دم تیغ آهنین داده صقال مصقله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه فلک ز بارگه کرده نشاط خوابگه</p></div>
<div class="m2"><p>بر دربارگه سپه ساخته شمع و مشعله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیر سپهر پنجمین شیر سپهر کرده زین</p></div>
<div class="m2"><p>چهره چو شیر تابه کین با که کند مجادله</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی فال مشتری انجم سعد مشتری</p></div>
<div class="m2"><p>او ز شراع ششدری با همه در مقابله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نرگس نرگس آسمان سفته به تیر غمزگان</p></div>
<div class="m2"><p>سنبل هندویش جهان رفته به سایه کله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن زمیان انس و جان برده هزار کاروان</p></div>
<div class="m2"><p>وین ز نشاط انس و جان رفته هزار قافله</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست طراز یاسمین لاله لولو آفرین</p></div>
<div class="m2"><p>کرده لبش چو انگبین تعبیه در شکر وله</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سر زلف خود شکن وز گهر سرشک من</p></div>
<div class="m2"><p>بافته جیب و پیرهن ساخته گوی انگله</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من ز غمش چو بی هشان بر رخم از هوان نشان</p></div>
<div class="m2"><p>تن ز دو چشم خونفشان غرقه در آب و آبله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او چو پری ز دلبری کرده مرا ز دل بری</p></div>
<div class="m2"><p>خسته دل من آن پری بسته به بند و سلسله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بت خلخ چگل از تو بت تبت خجل</p></div>
<div class="m2"><p>نزد تو وزن جان و دل یک جو و نیم خردله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مشعله بر فروختی رخت خرد بسوختی</p></div>
<div class="m2"><p>بر فلکی فروختی شهر نشور و مشغله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرده به عالمی روان حسن نو تو کاروان</p></div>
<div class="m2"><p>وز در خسرو جهان یافته زاد وراحله</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مالک مملکت ستان بارگهش در امان</p></div>
<div class="m2"><p>حکم به عدل تو امان کرده چه خوش معامله</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای گه گیر رخش تو خنجر نور بخش تو</p></div>
<div class="m2"><p>گشته بگام رخش تو سقف زمین و مرحله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا به مذاق انس و جان بدهد وناورد جهان</p></div>
<div class="m2"><p>نکهت گل به گلستان لذت مل بر آمله</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملک بقا گشاده ای خوان عطا نهاده ای</p></div>
<div class="m2"><p>طعم طمع تو داده ای بیش ز قدر حوصله</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طبع تو پادشاه خور مل به کفت به جام زر</p></div>
<div class="m2"><p>دلبر گلرخت به بر بی غم و رنج و غائله</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خیل تو از حد خزر تا به حدود کاشغر</p></div>
<div class="m2"><p>ملک تو از در شعر تا به در مباهله</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دخل مر کبت عیان در حد مصر و قیروان</p></div>
<div class="m2"><p>شغل او امرت روان تا به برون و داخله</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چار فلک ز شش کران هفت مدار آسمان</p></div>
<div class="m2"><p>حکم ترا بداده جان قدرت فکر فاعله</p></div></div>