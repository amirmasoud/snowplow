---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>شب وداع چو برداشتن طریق صواب</p></div>
<div class="m2"><p>به عزم بندگی صاحب سپهر جناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آفتاب سپهرم نبود خالی چشم</p></div>
<div class="m2"><p>وز آفتاب زمینم بماند دیده پر آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو روی شاه نقاب خضاب بر گون بست</p></div>
<div class="m2"><p>نگار صبح رخ از چهره بر گشاد نقاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرشک چون در بر روی روشنش ریزان</p></div>
<div class="m2"><p>چنانکه بر رخ آبگینه بر چکد سیماب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آن لب چو عقیقش بماند باقی اشک</p></div>
<div class="m2"><p>چو قطره قطره شبنم نشتسه بر عناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کباب شد دلم از آب چشم او والحق</p></div>
<div class="m2"><p>کسی ندید دلی را کز آب گشت کباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیل آن که دلم شد کباب در سینه</p></div>
<div class="m2"><p>بخار و دود نفس برده اشک چون خوناب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشست و گفت حکایات دوری از هر جای</p></div>
<div class="m2"><p>گرست و خواند شکایات فرقت از هر باب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روانه کرد از آن لعل همچو می در جام</p></div>
<div class="m2"><p>عتاب تلخ خوش جانفزای همچو شراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخواند این غزل تر میان گریه زار</p></div>
<div class="m2"><p>چنانکه خاک رهم شد ز آب دیده خلاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیقت لیله بلوی بقرفه الاحباب</p></div>
<div class="m2"><p>بقیت منفرداً منک فی اشدعذاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا هوای تو در سر ترا هوای دگر</p></div>
<div class="m2"><p>خلاف داب پسندیده نیست در آداب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دلم بتفت چو برتافتی عنان ز وطن</p></div>
<div class="m2"><p>سرم بگشت چو برگاشتی رخ از احباب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا به روی تو امید و رای تو به سفر</p></div>
<div class="m2"><p>مرا به صحبت تو میل و میل تو به ذهاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدیل گلشن و طارم مکن جبال و سهول</p></div>
<div class="m2"><p>عدیل مجلس و خلوت مکن کهوف و شعاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن زمان که مرا جای داده ای در دل</p></div>
<div class="m2"><p>گمان برم که مرا در فکنده ای به خراب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل خراب بر آتش مرا زدوری تو</p></div>
<div class="m2"><p>چو گنج ساکن لیکن ز تف او در تاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگو هر آنچه تو دانی مگو حدیث سفر</p></div>
<div class="m2"><p>بکن هر آنچه تو خواهی مکن به هجر خطاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه دست ساید در امن و خوف با تو عنان</p></div>
<div class="m2"><p>چه پای دارد در گرم و سرد با تو رکاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جواب دادم کز عزم این سفر با من</p></div>
<div class="m2"><p>مکن عتاب که از تو صواب نیست عتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدیع نیست ز احباب رنج راه و سفر</p></div>
<div class="m2"><p>غریب نیست ز عشاق قطع سهل و عقاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شنیده ای ز حکایات و دیده ای ز سمر</p></div>
<div class="m2"><p>رسیده ای به روایات و خوانده ای به کتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وفای لیلی و مجنون هوای زینت و زید</p></div>
<div class="m2"><p>بلای وامق و عذرا عنای دعد و رباب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپرده اند بسی راه های بی پایان</p></div>
<div class="m2"><p>بدیده اند بسی بحرهای بی پایاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو این مبین که به کامم دراست تلخی هجر</p></div>
<div class="m2"><p>تو آن نگر که کدامم دراست حسن مآب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دری که هست بر او پیر آسمان حارس</p></div>
<div class="m2"><p>دری که هست ورا پیک اختران تواب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شوم زظلمت این آستان ظلم نمای</p></div>
<div class="m2"><p>به بارگاه یکی آفتاب عالمتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز دل بنالم چون بیدلان در آن کعبه</p></div>
<div class="m2"><p>به خون بگریم چون مجرمان در آن محراب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به قول صاحب دعوت به امر خالق عرش</p></div>
<div class="m2"><p>میان دعوت مظلوم و عرش نیست حجاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برم ظلومه به دیوان صاحب و شنوم</p></div>
<div class="m2"><p>ز لفظ صاحب دیوان شرق و غرب جواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خشکسال حوادث بنالم و یابم</p></div>
<div class="m2"><p>ز کلک ابرنوال وزیر فتح الباب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ستوده آصف و دستور عالم عادل</p></div>
<div class="m2"><p>که در کمال و عدالت شد آفتاب نصاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سپهر حشمت و دریای جود شمس الدین</p></div>
<div class="m2"><p>مشیر مملکت و مالک رئوس و رقاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بهشت بزمی کز لطف و قهراو بدل است</p></div>
<div class="m2"><p>جزای اهل ثواب و سزای اهل عقاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به رزم و بزم کف زرفشان سر پاشش</p></div>
<div class="m2"><p>گهی ضراب نماید گهی شود ضراب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به حکم قاطع و تدبیر خوب و عزم درست</p></div>
<div class="m2"><p>به امر نافذ و خلق کریم و رای صواب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مصون گذارد ذرات خاک را از باد</p></div>
<div class="m2"><p>نگاهدارد اجزاء آتش اندر آب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر سحاب نبارد به امر او قطره</p></div>
<div class="m2"><p>شرار نار ببارد خلاف او ز سحاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر از حکایت او آب جوشنی پوشد</p></div>
<div class="m2"><p>خدنگ نار جهد در هوای خود حباب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به عقل شر غریزی برون برد ز سباع</p></div>
<div class="m2"><p>به علم جهل طبیعی جدا کند ز دواب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ایا خلاصه مخلوق و خاصه خالق</p></div>
<div class="m2"><p>و یا نقاده انسان و زبده انساب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیافت مثل تو دور سپهر جز در وهم</p></div>
<div class="m2"><p>ندید شبه تو چشم زمانه جز در خواب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رودبه مدح تو از خامه جان، نوا ز الفاظ</p></div>
<div class="m2"><p>شود ز نام تو در نامه سرفراز القاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از آن قبل که به چنگال دامنت دارد</p></div>
<div class="m2"><p>حروف اکثر قلال خیزد از قلاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به روزگار تو کژی رخ از جهان بر تافت</p></div>
<div class="m2"><p>مگر که زلف بتان را که کم نشد خم و تاب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز بیم عدل تو ناراستی به جان آید</p></div>
<div class="m2"><p>شود چو سوزن دوزنده ناخنان ذیاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به یمن عهد تو مشهور شد غراب البین</p></div>
<div class="m2"><p>به مژده بردن وصلت به جمله احزاب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چگونه شاد نباشد جهان بر‌ آن دوری</p></div>
<div class="m2"><p>که کرکس آید حراز و پیک وصل غراب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ضمیر پاکت ار ازکاد رون براندیشد</p></div>
<div class="m2"><p>کند به رسته او در رفوگری مهتاب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>و گر ز آتش کینت محیط اثر یابد</p></div>
<div class="m2"><p>چو لعل گردد در قعر بحر در خوشاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خلاف خاصیت طبع را ارادت تو</p></div>
<div class="m2"><p>برون دماند مردم گیا زبیخ سداب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر آنکه آب رخ از خاک درگه تو نجست</p></div>
<div class="m2"><p>نخواند از ره یالیت نص کنت تراب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>موسم است به داغ تو بچه در ارحام</p></div>
<div class="m2"><p>مقید است ز برق تو نطفه در اصلاب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان پناها ناگفته حال و قصه من</p></div>
<div class="m2"><p>به نور نفس بخوان بی میانجی اطناب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>که گر بگویم زحمت نما بود مکثار</p></div>
<div class="m2"><p>وگر نویسم وحشت فزا بود اسهاب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به سمع عالی اگر بگذرد عجب مانی</p></div>
<div class="m2"><p>ز قصه های عجیب و فسانه های عجاب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا ز حادثه پارس سال چار از پنج</p></div>
<div class="m2"><p>مباح بود سر و مال برنهیب و نهاب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اگر چه پارس پر آب است و در کنار محیط</p></div>
<div class="m2"><p>چو قلب خویش مرا داد تشنگی چو سراب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صفیر زد فلک از روی حیرت و غیرت</p></div>
<div class="m2"><p>که بر سواد مسلط چرا شدند کلاب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فقال فاعتبرو امنه یا اولی الابصار</p></div>
<div class="m2"><p>فسار فانتبهو امنه یا اولی الالباب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چومهره بازئی دیدم که دمبدم نبود</p></div>
<div class="m2"><p>ز زیر حقه مینا زمانه لعاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به قصد اهل هنر بر گشاد و بیرون کرد</p></div>
<div class="m2"><p>پلنگ حادثه چنگال و شیر نایبه ناب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هنر که زاد زمن شد و بال هستی من</p></div>
<div class="m2"><p>بلی و بال عقاب آمده ست پر عقاب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به لطف و رحمتم از ناب شیر و فتنه بپای</p></div>
<div class="m2"><p>که ذات تست همه لطف محض و رحمت ناب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به رهمنومی دولت رسیده ام به درت</p></div>
<div class="m2"><p>نه از نظر زیج و تحت اصطرلاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به استخارت اقبال بود و فتوی عقل</p></div>
<div class="m2"><p>که بنده کرد سوی درگه تو شتاب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به استشارت بختم هنروران گفتند</p></div>
<div class="m2"><p>کز اوست ملتمس خاطر ترا ایجاب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به خلد ننگرم ار پیش آیدم رضوان</p></div>
<div class="m2"><p>به راه حج نروم گر نخواندم بواب</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به عز عرش مجید و به حق مصحف مجد</p></div>
<div class="m2"><p>که مجد را نبود جز درت محل و مآب</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>نکرده ام به فرومایه استعانت هیچ</p></div>
<div class="m2"><p>نه در زمان مشیب و نه در اوان شباب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زبنده گر به کس این موهبت رسید به عمر</p></div>
<div class="m2"><p>بری بود هبته الله از ایزد وهاب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مرا به عالم اسباب در حصول غرض</p></div>
<div class="m2"><p>توئی بهین سببی از مسبب الاسباب</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اگر گشایش هر در ز درگه صمدیست</p></div>
<div class="m2"><p>توئی گشاده دری از متفح الابواب</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>مرا بخر به قبولی که گنج ها یابی</p></div>
<div class="m2"><p>دراین جهان زثنا و در آن جهان ز ثواب</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چو من نبینی قرنی دگر تو در من بین</p></div>
<div class="m2"><p>چو من نیابی دوری دگر مرا دریاب</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>نهاده خلق جهان گوش و چشم برره من</p></div>
<div class="m2"><p>که تا چگونه بود زین درم به خانه ایاب</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تو نام جوی ز دولت که تا ابد باشند</p></div>
<div class="m2"><p>ملوک از در تو نام جوی و دولت یاب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زجیش فتح تو هنگام کین به صف مصاف</p></div>
<div class="m2"><p>ولیت باد مصیب و عدوت باد مصاب</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به عز جاه تو نازان در آن جهان اسلاف</p></div>
<div class="m2"><p>به روی ورای تو شادان در این جهان اعقاب</p></div></div>