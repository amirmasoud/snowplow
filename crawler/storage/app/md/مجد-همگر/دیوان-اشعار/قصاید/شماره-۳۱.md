---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>خجسته بادا فصل ربیع و گردش سال</p></div>
<div class="m2"><p>بر این خجسته لقا پادشاه فرخ فال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ و چشم سلاطین و نور دیده ملک</p></div>
<div class="m2"><p>فرشته خو عضدالدین شه ستوده خصال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر مقدرت و قدر سعد بوبکر آن</p></div>
<div class="m2"><p>که آفتاب جمال است و آسمان جلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ورای آدمی و آدم است وبه ز ملک</p></div>
<div class="m2"><p>بدین حدیث گواه است ایزد متعال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهی به وقت ادا کرده جود وقت سجود</p></div>
<div class="m2"><p>خهی به دست سخا داده مال همچو رمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توئی که حضرت تو هست کعبه حاجات</p></div>
<div class="m2"><p>توئی که درگه تو هست قبله آمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رای روشن تو خورده مهر و مه تشویر</p></div>
<div class="m2"><p>ز دست باذل تو کرده کان وبحر سئوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روز بزم چو دستت کند گهر باری</p></div>
<div class="m2"><p>روان حاتم طی جوید از کف تو نوال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نماند در دل دریا و کان زر و گوهر</p></div>
<div class="m2"><p>ز بسکه دشمن مال است شاه دشمن مال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه عطا دل و دستت دو خاصیت دارند</p></div>
<div class="m2"><p>به وقت آنکه گذاری وظایف آمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از این بجوشد خون در دل خزاین و کان</p></div>
<div class="m2"><p>وز آن برآید جان از تن دفاین و مال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به گاه رزم چو در برکشی تو جوشن کین</p></div>
<div class="m2"><p>ز هیبت تو بلرزد روان رستم زال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز تاب رمح تو گردد هوا پر از شعله</p></div>
<div class="m2"><p>ز کوب گرز تو گردد زمین پر از زلزال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو مرکب تو زند شیهه در صف هیجا</p></div>
<div class="m2"><p>چو آب گردد خون مبارزان قتال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر خروش ز تن بگسلد دل دشمن</p></div>
<div class="m2"><p>چنانکه بر شکم کوس می زنند دوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کبوتریست مسافر خدنگ چار پرت</p></div>
<div class="m2"><p>گرفته درسر منقار نامه آجال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>معاشریست معربد حسام خونخوارت</p></div>
<div class="m2"><p>که جرعه دانش بود بحر خون مالامال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخن به کنه کمالت نمی رسد ورنه</p></div>
<div class="m2"><p>به دولت تو مرا خاطری ست بس به کمال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زبان بنده ثنای تو کی تواند گفت</p></div>
<div class="m2"><p>که مدحت تو برون است از بیان مقال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز حرص مدح وثنای تو شد فراموشم</p></div>
<div class="m2"><p>حدیث باغ و بهار و حکایت خط و خال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر چه قافیه شد خرج و تنگ شد میدان</p></div>
<div class="m2"><p>ز عشق یاد کنم چند بیت وصف الحال</p></div></div>