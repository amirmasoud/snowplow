---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>الا یا مشعبد شمال معنبر</p></div>
<div class="m2"><p>بخاری بخوری و یا گرد عنبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه روحی ولیکن چو روحی مصفا</p></div>
<div class="m2"><p>نه نوری ولیکن چو نوری منور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفسهای فردوسیانی به خلقت</p></div>
<div class="m2"><p>روانهای روحانیانی به گوهر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خلقی که نه جسم داری و نه جان</p></div>
<div class="m2"><p>چه مرغی که نه پای داری و نه پر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی پوئی و پای تو در تو پنهان</p></div>
<div class="m2"><p>همی پری و پر تو در تو مضمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسول بهشتی ز عالم به عالم</p></div>
<div class="m2"><p>برید بهاری ز کشور به کشور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسیم تو نافه گشاید به صحرا</p></div>
<div class="m2"><p>صریر تو دستان زند بر صنوبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خاک اندرت صد هزاران مطرا</p></div>
<div class="m2"><p>به آب اندرت صد هزاران زره در</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اشکال تو روی دریا منقش</p></div>
<div class="m2"><p>ز آثار تو روی صحرا مصور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الا یا خجسته براق سلیمان</p></div>
<div class="m2"><p>یکی بر سر کوی معشوق بگذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی صورت انگیز بر خاکش از خون</p></div>
<div class="m2"><p>نزار و جگر خسته و زرد لاغر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خروشان و جوشان و بریان و گریان</p></div>
<div class="m2"><p>بری گشته از خواب و بیزار از خور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گذشته بناگوشش از گوشه دل</p></div>
<div class="m2"><p>رسیده دو زانوش بر تارک سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه پیش و پیرامن او مخطط</p></div>
<div class="m2"><p>همه چاک پیراهن او معصفر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روان گشته رنجورش از درد هجران</p></div>
<div class="m2"><p>زبان گشته مجروحش از یاد دلبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز داغ درونش جوارح جراحت</p></div>
<div class="m2"><p>ز پیکان هجرانش افکار پیکر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به حالی که گر بر صفت بگذرانی</p></div>
<div class="m2"><p>شرر بارد از کلک و طوفان ز دفتر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>الا باد مشکین چو این نقش کردی</p></div>
<div class="m2"><p>در آویز در دامن آن ستمگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگویش که برخون این سوخته دل</p></div>
<div class="m2"><p>چه عذر آوری پیش دادار داور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر شرط مهر آزمائی توانی</p></div>
<div class="m2"><p>بکن پرسشی باری از حال چاکر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیا ای صنم بر سر راه باری</p></div>
<div class="m2"><p>یکی بر سر راه بگری و بنگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به تن بین ره صید مجروح از آهم</p></div>
<div class="m2"><p>منقط ز بس قطره های مقطر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرازش ز خونم چو کوه طبر خون</p></div>
<div class="m2"><p>نشیبش ز اشکم چو دریا ز گوهر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همه خاک و خاره چو لعل بدخشی</p></div>
<div class="m2"><p>همه سنگ ریزه چو یاقوت احمر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان ای نگارین که بردندم از تو</p></div>
<div class="m2"><p>بدانسان که آرند اسیران کافر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو بیمار بر پشت حمال نالان</p></div>
<div class="m2"><p>دو لب از نفس خشک و دو آستین تر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمانی ستاده چو بر طور موسی</p></div>
<div class="m2"><p>زمانی نشسته چو دجال بر خر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خری بد شراری خری بد طبیعت</p></div>
<div class="m2"><p>خری خفته بالای مفرنج و منظر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو دستش چنان چون دو چوگان گل کش</p></div>
<div class="m2"><p>دو پایش چو دوخر کمان کمانگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخفتی گر از باد پالانش بودی</p></div>
<div class="m2"><p>بماندی گر از سایه بودیش افسر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به هر موی او دیده ای رسته گریان</p></div>
<div class="m2"><p>به هر دیده ای نوحه کردی بر آن خر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمانی فتادی چو مصروع بیخود</p></div>
<div class="m2"><p>زمانی معلق زدی چون کبوتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دو بیطاقت و دو ضعیف و دو بیدل</p></div>
<div class="m2"><p>دو بیچاره و دو حزین و دو مضطر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی ره بریدیم چون بار بستیم</p></div>
<div class="m2"><p>دراین هر دو ره بر عجب مانده رهبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شنیدم که عیسی چو بر آسمان شد</p></div>
<div class="m2"><p>پیاده شد و ماند خر را هم ایدر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا با چنین خر به معراج عیسی</p></div>
<div class="m2"><p>ببردند تا جای پاکان برابر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به دشتی رسیدم به مانند دریا</p></div>
<div class="m2"><p>که کس جز ملایک ندیدیش معبر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه خورشید کردی بروجش سیاحت</p></div>
<div class="m2"><p>نه تقدیر کردی حدوش مقرر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گیاش از درشتی چو دندان افعی</p></div>
<div class="m2"><p>هواش از عیون همچو کام غضنفر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز آبش اجل رسته وز باد پیکان</p></div>
<div class="m2"><p>ز خاکش خسک رسته وز خار خنجر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه جز دیو در ساحتش کس مسافر</p></div>
<div class="m2"><p>نه جز وحش در وحشتش جین ماذر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همی رفتمی در چنان حال لرزان</p></div>
<div class="m2"><p>چو کهف یتیمان عریان به آذر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حضاری پدید آمد از دور گفتی</p></div>
<div class="m2"><p>سپهریست رسته ز پولاد و مرمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نشیبش ز الماس گسترده مفرش</p></div>
<div class="m2"><p>فرازش ز کافور پوشیده چادر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زبالاش اطلاس پوشیده انجم</p></div>
<div class="m2"><p>به دامانش پنهان شده چادر خور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یکی صورتی چون جهانی مهیا</p></div>
<div class="m2"><p>بر آورده پیکر به فرق دو پیکر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز وادیش عالم پر از تف دوزخ</p></div>
<div class="m2"><p>زبادش دو دیده پر از نیش نشتر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هوائی پر از آسمانهای سیمین</p></div>
<div class="m2"><p>زمینی پر از بوستانها به زیور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در این آسمان خاره و خار گلبن</p></div>
<div class="m2"><p>در آن آستان چشم نخجیر اختر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>طریقت بر این آسمان چون صراطی</p></div>
<div class="m2"><p>چو موی سر زلف خوبان کشمر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به جائی مسلسل به هنجار باران</p></div>
<div class="m2"><p>به جائی شده راست چون خط محور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رهی چون شهابی به پهنای گردون</p></div>
<div class="m2"><p>رهی چو طنابی فرو هشته از جر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رهی هم به کردار زنار راهب</p></div>
<div class="m2"><p>برآویخت از طرف محراب و منبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گهی دوخته پای او پشت ماهی</p></div>
<div class="m2"><p>گهی برده سر بر رخ نجم ازهر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عدیل و رفیق من من اندر چنین ره</p></div>
<div class="m2"><p>یکی اژدهای خروشان چو تندر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو بر روی خرافه برکرم پیله</p></div>
<div class="m2"><p>همی رفتمی من بر آن راه منکر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به قوت چو گردون به صورت چو دریا</p></div>
<div class="m2"><p>به تندی چو طوفان به تیزی چو صرصر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنان اژدهائی که از سهم و بیمش</p></div>
<div class="m2"><p>فسرده شدی بحر و بگداختی بر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>من اندر کنارش پشیمان و حیران</p></div>
<div class="m2"><p>همی رفتمی همچو عاصی به محشر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدینسان شدم تا یکی سنگلاخی</p></div>
<div class="m2"><p>چو قعر جهنم مهول مقعر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>یکی وادیئی چون یکی کنج دوزخ</p></div>
<div class="m2"><p>در آکنده مشتی خسیس محقر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گروهی چو یکمشت عفریت حیران</p></div>
<div class="m2"><p>به کنجی چو گور جهودان خیبر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو دیوان به مطمورهای سلیمان</p></div>
<div class="m2"><p>چو رهبان به کنج ستودان قیصر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سلب سایه و سنگ فرش و غذا غم</p></div>
<div class="m2"><p>هنر فتنه و فخر شور و شرف شر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چو نسناس ناکس چو نخجیر خیره</p></div>
<div class="m2"><p>چو یاجوج بی حد و ماجوج بی مر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه غافل از حکم دین و شریعت</p></div>
<div class="m2"><p>همه بی خبر از خدا و پیمبر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نه هرگز کسی دیده هنجار قبله</p></div>
<div class="m2"><p>نه هرگز شنیده کس الله اکبر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو دیوان هندی همه پیر و برنا</p></div>
<div class="m2"><p>چو غولان دشتی همه ماده و نر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>گروهی کریهان سگ طبع خس خو</p></div>
<div class="m2"><p>گروهی خسیسان خس خوار خس بر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به یکپاره نان آن کند دیده زن</p></div>
<div class="m2"><p>به یک استخوان این خورد خون مادر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همه دیو چهران و دیوانه طبعان</p></div>
<div class="m2"><p>همه سگ پرستان و گوساله پرور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به هر زیر سنگی گروهی بهیمه</p></div>
<div class="m2"><p>خزیده به یکدیگر اندر سراسر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به یک روزه نان جمله درویش لیکن</p></div>
<div class="m2"><p>رز بدبختی و بدسگالی توانگر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چه دارند این قوم قدی سلیمان</p></div>
<div class="m2"><p>اگر نیستی سهم شاه مظفر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ملک ناصر حق و سلطان مشرق</p></div>
<div class="m2"><p>که جمشید ملک است و خورشید لشکر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدانجا رسیده که گوینده گوید</p></div>
<div class="m2"><p>نه خالق و لیکن ز مخلوق برتر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چه عز است کآن مر ورا نیست آئین</p></div>
<div class="m2"><p>چه جاهست کآن مر ورا نیست درخور</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جهان را به دو گوهر ناموافق</p></div>
<div class="m2"><p>به توفیق ابر و به کردار صرصر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>یکی کلک روشن تن تیره صورت</p></div>
<div class="m2"><p>یکی تیغ خونخوار یاقوت پیکر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>دو صورت که هر دو منافی نیابند</p></div>
<div class="m2"><p>یکی خاک میدان یکی مشک اذفر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>یکی دولت افشاند از تاج محنت</p></div>
<div class="m2"><p>یکی آتش انگیزد از آب کوثر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ایا پادشاهی که از دولت تو</p></div>
<div class="m2"><p>جوان گشت باز این جهان معمر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>فلک زان شرف تا شود خاک پایش</p></div>
<div class="m2"><p>شودهر شبی بر بساط مدبر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به روزی که بخت آزمایند مردم</p></div>
<div class="m2"><p>برد هر کس از کشته خویش کیفر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>زمین گردد از نعل اسبان معزبل</p></div>
<div class="m2"><p>هوا گردد از گرد میدان معنبر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جهان گردد از خون گران چو دریا</p></div>
<div class="m2"><p>تو چون موج کشتی به ساحل برآور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>گهی همچو خورشید بر روی گردون</p></div>
<div class="m2"><p>گهی چون فرامرز بر پشت اشقر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>به نوک سنان شمری موت دشمن</p></div>
<div class="m2"><p>به گرز گردان بشکنی ترک و مغفر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سرکینه جویان به تن در گریزد</p></div>
<div class="m2"><p>ز ره بر کتف گردد از هم اجاعر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>بدانگه که حمله بری بر معادی</p></div>
<div class="m2"><p>چو ثعبان موسی چو شیر دلاور</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>ایا پادشاهی که از سهم تیغت</p></div>
<div class="m2"><p>مونث شود در رحم ها مذکر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>زمین ار چو دوزخ شود ور چو دریا</p></div>
<div class="m2"><p>زمان ار چو حنظل شود ورچو شکر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>منم از زبان و دل خویش ایمن</p></div>
<div class="m2"><p>ز رتبت مصفا ز تهمت مطهر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>ز گفتار بدگوی چون گرگ یوسف</p></div>
<div class="m2"><p>ز تلبیس بدخواه چون شیر مادر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>میان من و دشمن من شریعت</p></div>
<div class="m2"><p>طریقی نهاده ست سهل و مشهر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>اگر گشت راضی به احکام ایزد</p></div>
<div class="m2"><p>وگر سر بتابد زدین پیمبر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>به حکم نیاکان او بازگردم</p></div>
<div class="m2"><p>سیاوخش وار اندر ایم به آذر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همی تا موافق نگشت آب و آتش</p></div>
<div class="m2"><p>همی تا مساعد نشد نفع با ضر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>همی تا جهان گردد از نور و ظلمت</p></div>
<div class="m2"><p>زمانی مصفا زمانی مکدر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بقا بادت ای شاه در عز و دولت</p></div>
<div class="m2"><p>سر چتر تو گشته با چرخ همسر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>همیشه دو چشمت به ترک پری‌رخ</p></div>
<div class="m2"><p>همیشه دو دستت به زلف معنبر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>رخ بدسگال تو از آب دریا</p></div>
<div class="m2"><p>دل دشمن تو پر آذر چو مجمر</p></div></div>