---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>تا سوی تگنای دلم یافت راه دوست</p></div>
<div class="m2"><p>آن دل که توبه دوست بدی شد گناه دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکشب نرفت بر سر کویم به رسم یاد</p></div>
<div class="m2"><p>روزی نکرد در دل ریشم نگاه دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در دلم نشست چرا ننگرد در او</p></div>
<div class="m2"><p>آن بی حفاظ دلبر و آن دل سیاه دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل نیم پشیمان ایکاش سازدی</p></div>
<div class="m2"><p>از نازنین دو دیده من تکیه گاه دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاریست بوالعجب چو زمانه به خوی و طبع</p></div>
<div class="m2"><p>دشمن نواز و دوت کش و کینه خواه دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردنده و دو رنگ و مخالف چو روز و شب</p></div>
<div class="m2"><p>گه گه به سال دشمن و گه گه به ماه دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه دشمنم به غمزه دردپرده گاه اشک</p></div>
<div class="m2"><p>گاهی دلم به مهر کند خسته گاه دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریا کنار چشم من و مردمی در او</p></div>
<div class="m2"><p>تر دامن وجود من در یاشناه دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه نعره ای زنم ز تحسر که وای دل</p></div>
<div class="m2"><p>گه ناله ای کنم ز تحیر که آه دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مهر سبزه خط دلجوی او دلم</p></div>
<div class="m2"><p>با آنکه داشت مهر گیا شد گیاه دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بر نخورد چشم من از سبزه خطش</p></div>
<div class="m2"><p>یارب که برخوراد زروی چو ماه دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورشید در نظاره کند رجعت از غروب</p></div>
<div class="m2"><p>بر بام اگر برآید هر شامگاه دوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواهم که این سخن به نوا خوش کند ادا</p></div>
<div class="m2"><p>روزی که راه یابد در بارگاه دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن بارگه که مجمع شیران لشکر است</p></div>
<div class="m2"><p>دشمن شکار یکسر و یکرویه شاه دوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن شه که عاشقند مر او را کلاه و تخت</p></div>
<div class="m2"><p>دیگر ملوک تخت پرست و کلاه دوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای دوستکام شه که سیه روز دشمنت</p></div>
<div class="m2"><p>هاروت وار گشته نگونسار و چاه دوست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای جود تو ز لذت بخشش سئوال جوی</p></div>
<div class="m2"><p>هم عفو تو ز غایت رحمت گناه دوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فضل تو یار من بس اگر دشمن منند</p></div>
<div class="m2"><p>یکمشت فضل دشمن مغرور جاه دوست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیم و امید بنده ز رد و قبول تست</p></div>
<div class="m2"><p>یک شهر خواه دشمن من گرد و خواه دوست</p></div></div>