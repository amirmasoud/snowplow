---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>هر آه که من به صبحگاهی زده‌ام</p></div>
<div class="m2"><p>زان آتشی اندر دل ماهی زده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خواب اگر دلت بترسد تو بدان</p></div>
<div class="m2"><p>کآن لحظه من از سوز دل آهی زده‌ام</p></div></div>