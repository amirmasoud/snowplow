---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>شاها کرمت از پی آن برخیزد</p></div>
<div class="m2"><p>تا رسم تکبر زمیان برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن تو به نفس خود جهان دگری</p></div>
<div class="m2"><p>عاقل نپسندد که جهان برخیزد</p></div></div>