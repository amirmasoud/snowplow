---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>یکچند به آوازه بدم مفتونت</p></div>
<div class="m2"><p>نادیده به آواز شدم مجنونت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دیدمت آوازه و آواز چه بود</p></div>
<div class="m2"><p>از حور بهشت یافتم افزونت</p></div></div>