---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>یاری که بدوست سربلندی ما را</p></div>
<div class="m2"><p>وز دوری اوست مستمندی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می گفت چو در دل خرابم بنشست</p></div>
<div class="m2"><p>کآخر به خراب درفکندی ما را</p></div></div>