---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>ای همچو سلیمانت سر و تاج بلند</p></div>
<div class="m2"><p>بر مور ضعیف بار ماران مپسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پائی که نیازردی ازو مور رواست</p></div>
<div class="m2"><p>مجروح ز مار آهنین از در بند</p></div></div>