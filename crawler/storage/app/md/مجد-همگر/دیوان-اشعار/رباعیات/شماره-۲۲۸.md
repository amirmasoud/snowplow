---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>خاکی ز زمین که عطف دامانت برفت</p></div>
<div class="m2"><p>در دیده کشم به آشکار و به نهفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این عذر که آمدی کجا خواهم خواست</p></div>
<div class="m2"><p>وین لطف که کرده ای کجا دانم گفت</p></div></div>