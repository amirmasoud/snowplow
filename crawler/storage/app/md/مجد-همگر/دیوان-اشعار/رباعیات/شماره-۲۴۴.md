---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>چشمت کم ما گرفت بیگانه صفت</p></div>
<div class="m2"><p>عشق تو مرا بسوخت پروانه صفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد رس ای پری رخ از بهر خدا</p></div>
<div class="m2"><p>رحم آر بر این عاشق دیوانه صفت</p></div></div>