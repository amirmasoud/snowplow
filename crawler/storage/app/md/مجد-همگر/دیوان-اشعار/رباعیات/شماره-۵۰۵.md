---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>ای جور تو از صبر من غمگین بیش</p></div>
<div class="m2"><p>هجر تو ز طاقت من مسکین بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که سزای خویش بینی دیدم</p></div>
<div class="m2"><p>این است سزای من و صد چندین بیش</p></div></div>