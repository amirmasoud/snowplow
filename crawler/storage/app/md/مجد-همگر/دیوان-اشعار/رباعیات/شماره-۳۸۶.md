---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>زان قصه دردم که یکی یار نخواند</p></div>
<div class="m2"><p>کم بد که کسی بر سر بازار نخواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسوس که سر دفتر راز دل من</p></div>
<div class="m2"><p>شهری زن و مرد خواند ولی یار نخواند</p></div></div>