---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>از مرگ تو مهر و ماه و انجم بگریست</p></div>
<div class="m2"><p>حور و ملک و پری و مردم بگریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ساز بنام تو سرودی گفتم</p></div>
<div class="m2"><p>طنبور بنالید و بریشم بگریست</p></div></div>