---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>در ماتم شمس از شفق خون بچکید</p></div>
<div class="m2"><p>مه چهره بکند و زهره گیسو ببرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب جامه سیه کرد ازین ماتم و صبح</p></div>
<div class="m2"><p>برزد نفس سرد و گریبان بدرید</p></div></div>