---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>شاها رخ و رای دلگشایت چون است</p></div>
<div class="m2"><p>نازک قدم سپهر سایت چون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی باز دلم به دست فکر است اسیر</p></div>
<div class="m2"><p>ای تاج سر زمانه پایت چون است</p></div></div>