---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>ای دل اگر آغاز به می خواهی کرد</p></div>
<div class="m2"><p>و آغاز به نای و نوش و نی خواهی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تدبیر نشاط کن در این موسم گل</p></div>
<div class="m2"><p>اکنون نکنی نشاط کی خواهی کرد</p></div></div>