---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>آن روز که من بدیدم ای شاه ترا</p></div>
<div class="m2"><p>نه مهر بدیده بود و نه ماه ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غبن و دریغ برکنم دیده خویش</p></div>
<div class="m2"><p>اکنون که بدید دیده بدخواه ترا</p></div></div>