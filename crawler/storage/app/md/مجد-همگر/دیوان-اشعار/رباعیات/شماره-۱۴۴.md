---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>دی گفت مرا دلت چرا غمگین است</p></div>
<div class="m2"><p>دربند کدام دلبر شیرین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادم به کفش آینه گفتم بنگر</p></div>
<div class="m2"><p>کآنکس که دل مرا ربوده ست این است</p></div></div>