---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>ای لعل تو پرنکته شیرین غریب</p></div>
<div class="m2"><p>مازار دل خسته غمگین غریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانروی که نه غریب و مسکین چو منی</p></div>
<div class="m2"><p>بخشای بر این عاشق مسکین غریب</p></div></div>