---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>سودای تو آتش دلم افزون کرد</p></div>
<div class="m2"><p>نادیدن رویت آب چشمم خون کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر در که لبت در صدف گوشم ریخت</p></div>
<div class="m2"><p>هجران توام ز دیدگان بیرون کرد</p></div></div>