---
title: >-
    شمارهٔ ۴۸۶
---
# شمارهٔ ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>ای دوست ز دوست تا توانی مگریز</p></div>
<div class="m2"><p>آهسته که قدر ما بدانی مگریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با افعی زهردار هم کاسه مشو</p></div>
<div class="m2"><p>وز صحبت آب زندگانی مگریز</p></div></div>