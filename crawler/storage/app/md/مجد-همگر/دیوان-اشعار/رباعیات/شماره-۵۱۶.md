---
title: >-
    شمارهٔ ۵۱۶
---
# شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>هرگز ز تو نگلسم محال است محال</p></div>
<div class="m2"><p>یا عهد توبشکنم خیال است خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تو نکنم دعوی خون دل خویش</p></div>
<div class="m2"><p>خون دل من بر تو حلال است حلال</p></div></div>