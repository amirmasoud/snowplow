---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای باد چو بگذری به موصل ما را</p></div>
<div class="m2"><p>یاد آر و سلام کن رضی بابا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی تو جهودم ار فراموش کنم</p></div>
<div class="m2"><p>یاد تو مسلمان و بت ترسا را</p></div></div>