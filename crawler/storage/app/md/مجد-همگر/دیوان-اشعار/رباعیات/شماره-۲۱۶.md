---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>دی بگذشتم چو بیهشان بردر و دشت</p></div>
<div class="m2"><p>وز بوی گلاب و گل دماغم پر گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چه حالت است گفتند این دم</p></div>
<div class="m2"><p>آن گلرخ سروقامت آنجا بگذشت</p></div></div>