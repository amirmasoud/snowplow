---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>در نامه تو قلم چو گردن بفراشت</p></div>
<div class="m2"><p>گفتم بنویسم و سرشکم نگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال دل مشتاق نه آن صورت داشت</p></div>
<div class="m2"><p>کآنرا به سر کلک توانست نگاشت</p></div></div>