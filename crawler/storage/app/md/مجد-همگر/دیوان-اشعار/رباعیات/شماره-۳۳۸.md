---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>هر شب سپه غم تو راهم گیرد</p></div>
<div class="m2"><p>حلق دل تنگ بی گناهم گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچم غم خود نیست ولی ترسم از آنک</p></div>
<div class="m2"><p>در آینه رخ تو آهم گیرد</p></div></div>