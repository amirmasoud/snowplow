---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>بی بزم تو چشم جام می خون ریزد</p></div>
<div class="m2"><p>بی چهره تو باغ چه رنگ آمیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی عارض تو لاله چرا برروید</p></div>
<div class="m2"><p>بی قامت تو سرو چرا برخیزد</p></div></div>