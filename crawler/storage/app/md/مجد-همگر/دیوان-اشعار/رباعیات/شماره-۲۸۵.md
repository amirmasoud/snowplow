---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>گر یک نظرت بر من حیران افتد</p></div>
<div class="m2"><p>بیچاره دلم در سر صد جان افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاک کف پات فتم بوسه زنان</p></div>
<div class="m2"><p>چون تشنه که در چشمه حیوان افتد</p></div></div>