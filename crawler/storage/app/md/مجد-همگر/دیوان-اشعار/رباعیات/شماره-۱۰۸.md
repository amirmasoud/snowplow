---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>آن تازه صنوبرش که خوشرفتار است</p></div>
<div class="m2"><p>بس دل که به او بسته و از غم زار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست از غم او هزار بارم بر دل</p></div>
<div class="m2"><p>زیرا که بدو هزار دل پربار است</p></div></div>