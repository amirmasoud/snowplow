---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>پستم کردی به محنت ای چرخ بلند</p></div>
<div class="m2"><p>تا چند به بند و حبسم آخر تا چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین طبعم ولی نباشم نی قند</p></div>
<div class="m2"><p>کز بند به تنگ افتم و از تنگ به بند</p></div></div>