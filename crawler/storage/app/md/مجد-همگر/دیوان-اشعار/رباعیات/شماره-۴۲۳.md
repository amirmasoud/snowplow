---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>هرگز ز تو میل و آرزویم نشود</p></div>
<div class="m2"><p>پیوند تو از هر سر مویم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من رفتم و تا خاک نگردد رویم</p></div>
<div class="m2"><p>از خاک درت نشان رویم نشود</p></div></div>