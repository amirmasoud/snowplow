---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>گر کسری و دارا شوی از دولت و بخت</p></div>
<div class="m2"><p>ور افسر و خاقان شوی از افسر و تخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وریوسف و قارون شوی از مال و جمال</p></div>
<div class="m2"><p>زین با تو نیارامد بی حرزه سخت</p></div></div>