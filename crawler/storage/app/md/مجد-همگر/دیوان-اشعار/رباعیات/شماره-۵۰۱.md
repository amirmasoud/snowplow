---
title: >-
    شمارهٔ ۵۰۱
---
# شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>ای چون لب شیرین تو دشنام تو خوش</p></div>
<div class="m2"><p>وی چون دهن تنگ تو پیغام تو خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چهره حور روی دلخواه تو خوب</p></div>
<div class="m2"><p>چون بوی بهشت بوی اندام تو خوش</p></div></div>