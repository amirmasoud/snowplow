---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>شمعی که ازوست عیش می خوران خوش</p></div>
<div class="m2"><p>وز سوز وی است وقت بیداران خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریان گریان تا به سحرگه می گفت</p></div>
<div class="m2"><p>بگذشت مرا روز شب یاران خوش</p></div></div>