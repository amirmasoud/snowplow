---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>خورشید نخواهم که ببیند رویت</p></div>
<div class="m2"><p>نه مه که به شب روی رود در کویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شانه شود دلم به صد شاخ ز رشک</p></div>
<div class="m2"><p>گر شانه زند دست به شاخ مویت</p></div></div>