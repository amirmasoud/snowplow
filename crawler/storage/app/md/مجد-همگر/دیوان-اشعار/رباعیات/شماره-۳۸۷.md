---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>اشک و دل خونین شده با هم یارند</p></div>
<div class="m2"><p>وز روزن دیده سر برون می آرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکدست شدند و دامنم می گیرند</p></div>
<div class="m2"><p>واکنون دو سرند و یک گریبان دارند</p></div></div>