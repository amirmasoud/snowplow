---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>نز روزه فرض و مستحب می ترسم</p></div>
<div class="m2"><p>نز سنت با رنج و تعب می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گرسنگی روزه نمی ترسم لیک</p></div>
<div class="m2"><p>از خوردن و خفتن به شب می ترسم</p></div></div>