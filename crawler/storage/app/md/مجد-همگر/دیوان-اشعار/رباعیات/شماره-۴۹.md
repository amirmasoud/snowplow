---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>افکند دلم به کوی دلداری رخت</p></div>
<div class="m2"><p>و آورد به رویم ز غمش کاری سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بار ازین یار مرا بختی نیست</p></div>
<div class="m2"><p>ای کاش مرا یار بدی یاری بخت</p></div></div>