---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>یکدم نشوی با من مسکین دمساز</p></div>
<div class="m2"><p>کز حادثه صد در نشود بر من باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بوسه ز لعل تو و صد خون جگر</p></div>
<div class="m2"><p>یک غمزه ز چشم تو و شهری غماز</p></div></div>