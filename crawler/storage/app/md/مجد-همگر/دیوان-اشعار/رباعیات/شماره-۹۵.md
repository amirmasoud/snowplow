---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>اشکم نه غم زمانه خونین کرده است</p></div>
<div class="m2"><p>رویم نه ز درد بینوائی زرد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد روز شباب و یار دیرینه ز دست</p></div>
<div class="m2"><p>ای دوست غم این غم است و درد این درد است</p></div></div>