---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>گر صدر دل است شاهراه غم تست</p></div>
<div class="m2"><p>ور سینه خسته تکیه گاه غم تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور بیدق اشک است بر این عرصه چهر</p></div>
<div class="m2"><p>هم پیشرو مرکب شاه غم تست</p></div></div>