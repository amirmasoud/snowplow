---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>نه دل ز غمت ذوق جوانی دارد</p></div>
<div class="m2"><p>نه برگ نشاط و شادمانی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هجر تو گر یک دو نفس هست مرا</p></div>
<div class="m2"><p>می کیست که نام زندگانی دارد</p></div></div>