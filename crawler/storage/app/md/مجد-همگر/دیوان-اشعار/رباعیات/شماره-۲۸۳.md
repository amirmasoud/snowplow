---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>ترسا بچه ایکه باج از ارمن بستد</p></div>
<div class="m2"><p>بر من بگذشت و جانم از تن بستد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشست و به یک کرشمه صبرم بربود</p></div>
<div class="m2"><p>برخاست به یک سخن دل از من بستد</p></div></div>