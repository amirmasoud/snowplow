---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>آئینه جانم آن رخ فرخ تست</p></div>
<div class="m2"><p>قوت دلم آن لب شکر پاسخ تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ماه شب چاردهم خوبتری</p></div>
<div class="m2"><p>وز سیزده مه ماه مبارک رخ تست</p></div></div>