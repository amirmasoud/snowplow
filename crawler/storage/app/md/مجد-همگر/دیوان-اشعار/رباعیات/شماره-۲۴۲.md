---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>دی بر سر ره چو دلستانم می رفت</p></div>
<div class="m2"><p>هوش از تن و طاقت از روانم می رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او می شد و چشمم ز پی اش می نگریست</p></div>
<div class="m2"><p>دیدم به دو چشم خود که جانم می رفت</p></div></div>