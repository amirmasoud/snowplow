---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>در عشق تو گر شود مرا خاک نقاب</p></div>
<div class="m2"><p>بس دل که شود ز داغ جور تو کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس روز که بر دریغ من درد خوری</p></div>
<div class="m2"><p>بس شب که خیال من نبینی در خواب</p></div></div>