---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>گیرم که مرا خدمت تو شاهی داد</p></div>
<div class="m2"><p>ملکی به سزا ز ماه تا ماهی داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه عمر عزیز با تو ضایع کردم</p></div>
<div class="m2"><p>تو عمر گذشته را عوض خواهی داد</p></div></div>