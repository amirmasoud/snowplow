---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>در باغ شدی و گل به دست تو در است</p></div>
<div class="m2"><p>می بوئی و از نوش لبت بهره ور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر شاخ ندیم خار بودی واکنون</p></div>
<div class="m2"><p>چون با لبت آمیخته شد گلشکراست</p></div></div>