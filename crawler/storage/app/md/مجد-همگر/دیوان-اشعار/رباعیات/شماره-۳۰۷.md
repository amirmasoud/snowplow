---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>زلف تو که خون ریختن آئین دارد</p></div>
<div class="m2"><p>کفریست که رونق دوصد دین دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاقان به جهان بیش زیک چینش نیست</p></div>
<div class="m2"><p>آن هندوی زلف تست و صد چین دارد</p></div></div>