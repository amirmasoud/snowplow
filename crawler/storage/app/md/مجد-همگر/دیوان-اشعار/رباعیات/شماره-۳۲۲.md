---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>جانا غم دوری تو با ما آن کرد</p></div>
<div class="m2"><p>کآن را به همه عمر بیان نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رویم اثرهای غمت پیدا شد</p></div>
<div class="m2"><p>تا حادثه روی تو ز ما پنهان کرد</p></div></div>