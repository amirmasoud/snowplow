---
title: >-
    شمارهٔ ۴۹۰
---
# شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>ای صبح رخ از سوز شب تار بترس</p></div>
<div class="m2"><p>وی خفته ز آه من بیدار بترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که شبی در تو رسد سوز دلم</p></div>
<div class="m2"><p>از سوز دل سوخته زنهار بترس</p></div></div>