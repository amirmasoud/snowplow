---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>هر روز زمانه را ز نو بازاریست</p></div>
<div class="m2"><p>هر لحظه ز نو حادثه در پیکاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای یار کهن ز رنج نو دل مشکن</p></div>
<div class="m2"><p>کاین چرخ کهن نو به نو اندر کاریست</p></div></div>