---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>تا عشق تو همنشین دل ماست</p></div>
<div class="m2"><p>داغ غم عشق بر جبین دل ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ابر دو دیده هردمش آب دهم</p></div>
<div class="m2"><p>تا کشت غم تو بر زمین دل ماست</p></div></div>