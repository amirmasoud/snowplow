---
title: >-
    شمارهٔ ۳۹۶
---
# شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>گفتم که تو را ماه فلک می خوانند</p></div>
<div class="m2"><p>خاک قدمت تاج ملک می خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی دانستم که با چنان مشکین زلف</p></div>
<div class="m2"><p>این کور دلان تو را کلک می خوانند</p></div></div>