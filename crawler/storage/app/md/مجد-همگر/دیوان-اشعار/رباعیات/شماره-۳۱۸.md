---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>گر چاره این تنگدلی باید کرد</p></div>
<div class="m2"><p>ما را چو تو ده رنگ دلی باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نرم تر از موم دلی دارم لیک</p></div>
<div class="m2"><p>با سنگدلان سنگدلی باید کرد</p></div></div>