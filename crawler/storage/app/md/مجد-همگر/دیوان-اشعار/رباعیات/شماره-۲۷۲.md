---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>دل بی هوس تو ای دل افروز مباد</p></div>
<div class="m2"><p>بر هیچ مراد بی تو پیروز مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که بود هجر تو تا شب مرساد</p></div>
<div class="m2"><p>وان شب که رسم به وصل تو روز مباد</p></div></div>