---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>زلف تو که دل در شکنش زندانیست</p></div>
<div class="m2"><p>دزد است و به آویختگی ارزانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل برد و هزار کس گواهند بر این</p></div>
<div class="m2"><p>وان شوخ هنوز برسر پیشانیست</p></div></div>