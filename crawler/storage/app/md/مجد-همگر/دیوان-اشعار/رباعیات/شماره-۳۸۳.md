---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>خورشید بماناد اگر سایه نماند</p></div>
<div class="m2"><p>تن باقی ماند اگرچه پیرایه نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید که خسرو سپهر آمد گفت</p></div>
<div class="m2"><p>شیرین بزیا دیر اگر دایه نماند</p></div></div>