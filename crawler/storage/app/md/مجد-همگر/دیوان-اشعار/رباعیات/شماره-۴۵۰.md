---
title: >-
    شمارهٔ ۴۵۰
---
# شمارهٔ ۴۵۰

<div class="b" id="bn1"><div class="m1"><p>از قطره آب رخنه گردد مرمر</p></div>
<div class="m2"><p>چون موم شود گوهر سنگ از آذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیلاب سرشک و آتش سینه من</p></div>
<div class="m2"><p>در سنگ دلت نمی کند هیچ اثر</p></div></div>