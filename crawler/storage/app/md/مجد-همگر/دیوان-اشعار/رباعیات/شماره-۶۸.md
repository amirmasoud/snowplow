---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چون عهد شکستنت مرا گشت درست</p></div>
<div class="m2"><p>بر دل بستم سنگ شکیبائی چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مهر تو کم شود نگویم با ماست</p></div>
<div class="m2"><p>ور جان ببری چو دل نگویم با تست</p></div></div>