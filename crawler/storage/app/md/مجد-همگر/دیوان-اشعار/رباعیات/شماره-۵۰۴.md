---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>ای روی تو آراسته بی آرایش</p></div>
<div class="m2"><p>دیدار تو داده روح را آسایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخشود نیم گرت سر بخشش هست</p></div>
<div class="m2"><p>کز بهر چنین روز بود بخشایش</p></div></div>