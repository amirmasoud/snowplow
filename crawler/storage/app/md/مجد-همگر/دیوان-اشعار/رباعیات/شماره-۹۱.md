---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>آن یار که راحت روان است کجاست</p></div>
<div class="m2"><p>شد دور ز چشم من نهان است کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جائی نبرم گمان که پرسم خبرش</p></div>
<div class="m2"><p>حوراست. ملک. پریست جان است کجاست</p></div></div>