---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ای شمع نه عاشقی رخت زرد چراست</p></div>
<div class="m2"><p>وین گریه ز روی مهر یا درد چراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سوز منت نیست رخت زرد ز چیست</p></div>
<div class="m2"><p>ور درد منت نیست دمت سرد چراست</p></div></div>