---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>هستت خبر ای جان که خریدار تو کیست</p></div>
<div class="m2"><p>وانده خر و جانفروش بازار تو کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود دانم که کشته درد کی ام</p></div>
<div class="m2"><p>تو کی دانی که عاشق زار تو کیست</p></div></div>