---
title: >-
    شمارهٔ ۴۹۳
---
# شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>ای تن ز شب دراز دلسوز مباش</p></div>
<div class="m2"><p>وی دل ز پی صبح غم اندوز مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو همدم شمع گرد و گو صبح مدم</p></div>
<div class="m2"><p>شو مونس رود باش و گو روز مباش</p></div></div>