---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>خون کرد دلم را چو دل لاله فراق</p></div>
<div class="m2"><p>بر من بگماشت گریه و ناله فراق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از عمری چو یار دیدار نمود</p></div>
<div class="m2"><p>یک ساعته وصل بود و یکساله فراق</p></div></div>