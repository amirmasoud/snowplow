---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>قصاب که در گردن جان چنبر اوست</p></div>
<div class="m2"><p>از پشتی حسن سنیه کرده ست و نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چرخ زند پهلو و دارد در پوست</p></div>
<div class="m2"><p>دلداری دشمن و جگرخواری دوست</p></div></div>