---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>یک یک هنرم بین و گنه ده ده بخش</p></div>
<div class="m2"><p>جرمی که نرفت حسبته لله بخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازباد دروغ آتش خشمت مفروز</p></div>
<div class="m2"><p>و آب رخ من به خاک سلغر شه بخش</p></div></div>