---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>تا دست قضا بر سرمن آتش بیخت</p></div>
<div class="m2"><p>آب مژه از دیده من سیل انگیخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که شود ریخته در خاک تنم</p></div>
<div class="m2"><p>کآن تازه گل از باد اجل زود بریخت</p></div></div>