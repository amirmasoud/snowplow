---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>دل بین که مرا غم که مهمان آورد</p></div>
<div class="m2"><p>وز عشق که بر سرم چه طوفان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آتش پارسی روانسوزتر است</p></div>
<div class="m2"><p>این سیل که از راه خراسان آورد</p></div></div>