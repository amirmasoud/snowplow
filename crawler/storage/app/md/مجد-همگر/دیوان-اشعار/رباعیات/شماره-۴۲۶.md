---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>با هر که دلم به مهر پیوسته شود</p></div>
<div class="m2"><p>چون چرخ به کین من کمر بسته شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور در عمری دست برم سوی گلی</p></div>
<div class="m2"><p>خاری گردد وزاو دلم خسته شود</p></div></div>