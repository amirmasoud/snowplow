---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>گفتم که به اندیشه و با رای درست</p></div>
<div class="m2"><p>خود را به دراندازم ازین واقعه چست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز مذهب این قوم ملالم بگرفت</p></div>
<div class="m2"><p>هر یک زده دست عجز در شاخی سست</p></div></div>