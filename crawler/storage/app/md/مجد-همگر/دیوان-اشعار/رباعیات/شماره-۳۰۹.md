---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>گر چه دلم از جور تو داغی دارد</p></div>
<div class="m2"><p>از پشتی صبر خود دماغی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو به جز از عشق ندارد هوسی</p></div>
<div class="m2"><p>وز وصل و فراق تو فراغی دارد</p></div></div>