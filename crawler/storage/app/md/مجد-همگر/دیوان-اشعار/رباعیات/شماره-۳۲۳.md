---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>با هجر چنان حریف خو نتوان کرد</p></div>
<div class="m2"><p>وز زشتی ایام نکو نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این شادی آنکه بی رخش غمگینم</p></div>
<div class="m2"><p>وین یاد کسی که یاد او نتوان کرد</p></div></div>