---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>هر روز گل وفات پژمرده تر است</p></div>
<div class="m2"><p>هر لحظه دل از جور تو آزرده تر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که در عشق تو دلگرم ترم</p></div>
<div class="m2"><p>در مهر دل سخت تو افسرده تر است</p></div></div>