---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>از جور تو هیچ خسته نفرین مکناد</p></div>
<div class="m2"><p>دردم دل خرم تو غمگین مکناد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایزد به جزای من مسکین غریب</p></div>
<div class="m2"><p>مانند منت غریب و مسکین مکناد</p></div></div>