---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>چشمت ز زمانه فتنه انگیزتر است</p></div>
<div class="m2"><p>مژگان تو از تیر اجل تیزتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی کمانکش تو در خیره کشی</p></div>
<div class="m2"><p>از چرخ ستیزه کار خونریزتر است</p></div></div>