---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ای باد که جان فدای پیغام تو باد</p></div>
<div class="m2"><p>آن لحظه که بگذری بر آن حور نژاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو بر سر ره دلشده ای را دیدم</p></div>
<div class="m2"><p>کز آرزوی تو جان شیرین می داد</p></div></div>