---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>تا کی عمرت به خودپرستی گذرد</p></div>
<div class="m2"><p>یا در غم نیستی و هستی گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن عمر که مرگ باشد اندر پی او</p></div>
<div class="m2"><p>آن به که به خواب یا به مستی گذرد</p></div></div>