---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>با دل سخن خویش بگفتم به نهفت</p></div>
<div class="m2"><p>کاین چشم من از عشق فلان دوش نخفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بودم و دل پس این سخن فاش که کرد</p></div>
<div class="m2"><p>با دل سخن خویش نمی یارم گفت</p></div></div>