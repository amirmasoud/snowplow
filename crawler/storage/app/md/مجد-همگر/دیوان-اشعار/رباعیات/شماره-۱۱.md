---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>آن شد که به زر عوام جویند ترا</p></div>
<div class="m2"><p>خاصان دل و جان و دیده گویند ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که به جوی خوبی ات آب نماند</p></div>
<div class="m2"><p>گر نان گردی سگان نبویند ترا</p></div></div>