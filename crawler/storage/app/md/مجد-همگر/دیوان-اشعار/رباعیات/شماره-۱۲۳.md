---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>از حسرت چهره ایکه باغ دلم است</p></div>
<div class="m2"><p>از باغ و بهار و گل فراغ دلم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر رخ دوست به باغ آمدمی</p></div>
<div class="m2"><p>چون بی رخ اوست باغ داغ دلم است</p></div></div>