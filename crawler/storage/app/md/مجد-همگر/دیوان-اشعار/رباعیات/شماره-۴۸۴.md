---
title: >-
    شمارهٔ ۴۸۴
---
# شمارهٔ ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>در چشم من است آن رخ رخشنده هنوز</p></div>
<div class="m2"><p>بر یاد من است آن لب پرخنده هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سرو جوان فتاده در پای اجل</p></div>
<div class="m2"><p>من پیر به ماتم تو در زنده هنوز</p></div></div>