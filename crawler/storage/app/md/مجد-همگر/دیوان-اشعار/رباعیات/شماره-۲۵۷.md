---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>افکند مرا گردش دهر ازکویت</p></div>
<div class="m2"><p>جائی که صبا نیارد آنجا بویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه روی تو دیدنم میسر باشد</p></div>
<div class="m2"><p>نه روی کسی که دیده باشد رویت</p></div></div>