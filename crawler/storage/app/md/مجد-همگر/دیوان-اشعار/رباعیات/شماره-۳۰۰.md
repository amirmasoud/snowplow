---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>نه پایم ازین پس ره کویت سپرد</p></div>
<div class="m2"><p>نه بر دل تنگم آرزویت گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دیده باز چشم من دوخته باد</p></div>
<div class="m2"><p>گر دیده من باز به رویت نگرد</p></div></div>