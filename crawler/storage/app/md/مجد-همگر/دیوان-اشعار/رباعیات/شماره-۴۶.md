---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای وصل تو سرمایه اسباب حیات</p></div>
<div class="m2"><p>در بحر غم تو نیست پایاب حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب تشنه خضر پیش لبت جان می داد</p></div>
<div class="m2"><p>می گفت که خاک بر سرآب حیات</p></div></div>