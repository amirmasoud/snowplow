---
title: >-
    شمارهٔ ۵۲۲
---
# شمارهٔ ۵۲۲

<div class="b" id="bn1"><div class="m1"><p>برخیز که غنچه در قماط است ای دل</p></div>
<div class="m2"><p>در هر چمن از لاله بساط است ای دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنرا بنشان پیش که من دانم و تو</p></div>
<div class="m2"><p>امروز که موسم نشاط است ای دل</p></div></div>