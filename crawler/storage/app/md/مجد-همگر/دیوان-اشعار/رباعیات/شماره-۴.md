---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>گر طعنه همی زنی من شیدا را</p></div>
<div class="m2"><p>در آینه بنگر آن رخ زیبا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شیفته و سغبه تر از من گردی</p></div>
<div class="m2"><p>وانگه نکنی بیش ملامت ما را</p></div></div>