---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>ای نام من از عشق تو دیوانه شهر</p></div>
<div class="m2"><p>وی خصم من آشنا و بیگانه شهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منسوخ شده است ویس و رامین امروز</p></div>
<div class="m2"><p>عشق من و حسن تست افسانه شهر</p></div></div>