---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>ای خصم مدان رفتنش ا زنفرینت</p></div>
<div class="m2"><p>بد مهری دنیا مشمار از کینت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ماتم او ز چشم خون بار چو سیل</p></div>
<div class="m2"><p>گر دوخته نیست چشم عبرت بینت</p></div></div>