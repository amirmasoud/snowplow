---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>بی وصل تو جان نخواهم ای زیبا یار</p></div>
<div class="m2"><p>در هجر تو شد دیده و دل در سر کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جان و دل و دیده شدم زاری زار</p></div>
<div class="m2"><p>ای جان و دل و دیده چنینم مگذار</p></div></div>