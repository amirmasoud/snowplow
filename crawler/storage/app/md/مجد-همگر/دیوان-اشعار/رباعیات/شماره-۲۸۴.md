---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>هر دل که در آن زلف چو شستت افتد</p></div>
<div class="m2"><p>در دام بلای چشم مستت افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل است ز دست تو بجستن لیکن</p></div>
<div class="m2"><p>چون من دگری کجا به دستت افتد</p></div></div>