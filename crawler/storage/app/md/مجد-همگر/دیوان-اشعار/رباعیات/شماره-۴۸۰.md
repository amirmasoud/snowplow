---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>آغاز غم تو ماجرائیست دراز</p></div>
<div class="m2"><p>و آهنگ فراق تو نوائیست دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن طره کوته تو خوب است ولیک</p></div>
<div class="m2"><p>بالای بلند تو بلائیست دراز</p></div></div>