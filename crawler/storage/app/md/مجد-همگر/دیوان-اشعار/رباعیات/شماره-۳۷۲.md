---
title: >-
    شمارهٔ ۳۷۲
---
# شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>در جعبه روزگار یک تیر نماند</p></div>
<div class="m2"><p>کاندر دل من به کینه تا پر ننشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک آیت غم بر ورق گردون نیست</p></div>
<div class="m2"><p>کاین دل به نظر ز بام تا شام نخواند</p></div></div>