---
title: >-
    شمارهٔ ۳۳۲
---
# شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>آن دد که چو دمنه در کمینگاه بمرد</p></div>
<div class="m2"><p>از هیبت شیرنر چو روباه بمرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانروی که بدخواه همه نیکان بود</p></div>
<div class="m2"><p>در کام اجل به کام بدخواه بمرد</p></div></div>