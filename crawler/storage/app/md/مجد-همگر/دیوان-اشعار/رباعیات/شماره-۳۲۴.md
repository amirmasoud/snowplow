---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>کم نال دلا اگر شهت یاد نکرد</p></div>
<div class="m2"><p>وز بند پس از شش مه ات آزاد نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه گوهر بحر سعد سلمان سی سال</p></div>
<div class="m2"><p>در قلعه نای ماند و فریاد نکرد</p></div></div>