---
title: >-
    شمارهٔ ۵۱۱
---
# شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>از نظم تباه دل نمی داری تنگ</p></div>
<div class="m2"><p>وز دامن من باز نمی داری چنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ننگ ز شعر نیک خود می دارم</p></div>
<div class="m2"><p>وز شعر بد خود تو نمی داری ننگ</p></div></div>