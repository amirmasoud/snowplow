---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>در پارس به رایگان چو صد شهر تراست</p></div>
<div class="m2"><p>از نیم ده منت چرا غیرت خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملکی که سلیمان به دعا از حق خواست</p></div>
<div class="m2"><p>بی برزنگان بخور که در شرع رواست</p></div></div>