---
title: >-
    شمارهٔ ۵۴۸
---
# شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>نه کارگر است چاره سازی با تو</p></div>
<div class="m2"><p>نه درگیرد زبان درازی با تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه رام شوی به دلنوازی با من</p></div>
<div class="m2"><p>مشکل کاریست عشق بازی با تو</p></div></div>