---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>شاها چو تو در سپهر دین ماهی نیست</p></div>
<div class="m2"><p>دل را به جز از مهر تو دلخواهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بخت به خدمت تو می آیم لیک</p></div>
<div class="m2"><p>چون حادثه نزدیک توام راهی نیست</p></div></div>