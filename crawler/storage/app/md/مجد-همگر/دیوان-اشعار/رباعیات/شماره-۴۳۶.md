---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>در نامه ز بس غم که به هر می آید</p></div>
<div class="m2"><p>در چشم قلم ز غصه نم می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر حال دلم دل قلم سوخت و زآن</p></div>
<div class="m2"><p>خون سیه از چشم قلم می آید</p></div></div>