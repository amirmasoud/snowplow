---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>با خوت که آدمی سرشتی دارد</p></div>
<div class="m2"><p>با بوت که صد گل بهشتی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خوبی و نیک عهدی و آزادی</p></div>
<div class="m2"><p>با بنده چو بدکنی نه زشتی دارد</p></div></div>