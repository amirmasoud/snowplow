---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>با آنکه به روی خوب پشتی ما را</p></div>
<div class="m2"><p>نرمی همگان را و درشتی ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم گوئی به دم ترا زنده کنم</p></div>
<div class="m2"><p>من دم نخرم برو که کشتی ما را</p></div></div>