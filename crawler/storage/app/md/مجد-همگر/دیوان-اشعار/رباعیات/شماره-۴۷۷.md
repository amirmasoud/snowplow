---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>نومید بدم ز دیدنت عمر دراز</p></div>
<div class="m2"><p>عمری شدم از عشوه تو در تک و تاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون باد بدان عمر من و عهد تو نیز</p></div>
<div class="m2"><p>هم با سر نومیدی خود رفتم باز</p></div></div>