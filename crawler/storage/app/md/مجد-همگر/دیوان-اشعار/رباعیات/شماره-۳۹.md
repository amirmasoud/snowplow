---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>گفتم چو بد آمده ست فرجام شراب</p></div>
<div class="m2"><p>در تو به گریزم نبرم نام شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون با دل تنگم آشنا شد غم تو</p></div>
<div class="m2"><p>زین پس من و یاد رویت و جام شراب</p></div></div>