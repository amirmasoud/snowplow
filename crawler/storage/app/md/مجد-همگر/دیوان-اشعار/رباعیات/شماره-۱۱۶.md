---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>پیوسته ز دیده در کنارم اشک است</p></div>
<div class="m2"><p>در پای غم یار نثارم اشک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دید عیان راز نهانم از اشک</p></div>
<div class="m2"><p>پیداست که خصم آشکارم اشک است</p></div></div>