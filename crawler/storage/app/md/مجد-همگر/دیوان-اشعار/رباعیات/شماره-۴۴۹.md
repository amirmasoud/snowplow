---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>از جور تو دلبر ز دلم نیست خبر</p></div>
<div class="m2"><p>وز خوی تو جانان نه ز جان دارم اثر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زینسان بود آن جان که تو باشی جانش</p></div>
<div class="m2"><p>زینگونه بود دل که تو باشی دلبر</p></div></div>