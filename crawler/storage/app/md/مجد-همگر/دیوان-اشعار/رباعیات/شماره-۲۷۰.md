---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>ای دل چو تو کس فتنه و شردوست مباد</p></div>
<div class="m2"><p>مانند تو کس شیفته بر دوست مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی دیده جماش توئی دشمن جان</p></div>
<div class="m2"><p>کس چون تو هوس باز و نظردوست مباد</p></div></div>