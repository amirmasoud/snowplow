---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>گفتم شبه ار سفته بود به باشد</p></div>
<div class="m2"><p>وان نرگس اگر خفته بود به باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا ز دو فتنه عالمی برخیزد</p></div>
<div class="m2"><p>باری چو یکی خفته بود به باشد</p></div></div>