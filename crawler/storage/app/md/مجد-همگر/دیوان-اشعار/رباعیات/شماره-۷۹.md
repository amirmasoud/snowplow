---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>از دیدن روی منکلی مهرم خاست</p></div>
<div class="m2"><p>بی دیدن روی منکلی عمرم کاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نا دیدن روی منکلی صبر کراست</p></div>
<div class="m2"><p>نادیدن روی منکلی عین خطاست</p></div></div>