---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>جانا دل من که گنج اسرار خداست</p></div>
<div class="m2"><p>مازار که آزردنش آزار خداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در رد و قبول من که نیکم یابد</p></div>
<div class="m2"><p>زنهار مکن حکم که آن کار خداست</p></div></div>