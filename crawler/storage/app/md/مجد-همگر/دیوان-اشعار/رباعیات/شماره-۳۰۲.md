---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>چون دست اجل تخته برهان بسترد</p></div>
<div class="m2"><p>بس بار مظالم که بدان عالم برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مدت عمرش که ز بد بود بتر</p></div>
<div class="m2"><p>هرگز به ازین نکرد کاری که بمرد</p></div></div>