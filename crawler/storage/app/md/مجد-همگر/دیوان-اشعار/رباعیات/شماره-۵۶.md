---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>خواهم که به دیده و سرآیم به درت</p></div>
<div class="m2"><p>صد قصه ز غصه دل آرم به برت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند که زحمتی ست از دردسرش</p></div>
<div class="m2"><p>از دردسرت نمی دهم درد سرت</p></div></div>