---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>چشمم به جنازه تو چون درنگریست</p></div>
<div class="m2"><p>خون ریخت که بی رخ تو چون خواهم زیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار ز چشم شوخ آن کز چو توئی</p></div>
<div class="m2"><p>جان بستد و در جوانی تو نگریست</p></div></div>