---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>ای دوست تو غم با من غمساز گذار</p></div>
<div class="m2"><p>خود عمر به عیش و طرب و ناز گذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیش شب و روز نیک خود را دریاب</p></div>
<div class="m2"><p>روز بد و تیره شب به من باز گذار</p></div></div>