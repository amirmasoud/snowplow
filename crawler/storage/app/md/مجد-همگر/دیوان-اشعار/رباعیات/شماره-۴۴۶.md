---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>یار از پی کار مشکلم می گرید</p></div>
<div class="m2"><p>بر سعی و امید باطلم می گرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم می دهد او مرا و چون هیزم تر</p></div>
<div class="m2"><p>می سوزم و بر دود دلم می گرید</p></div></div>