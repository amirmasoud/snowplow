---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>در عشق دلم فراق بهر افتاده ست</p></div>
<div class="m2"><p>وز صاف لطف به درد قهر افتاده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مهر مهی نهفته در پرده راز</p></div>
<div class="m2"><p>با شور و شری شهره شهر افتاده ست</p></div></div>