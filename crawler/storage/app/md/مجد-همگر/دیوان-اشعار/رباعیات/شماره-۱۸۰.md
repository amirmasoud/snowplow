---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>گر کوتهی عمر ز بیدادگریست</p></div>
<div class="m2"><p>با ظلم تو این عمر دراز تو ز چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیغمبر حق به سال شصت و سه گذشت</p></div>
<div class="m2"><p>تو ظالم ضال تا به صد خواهی زیست</p></div></div>