---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>شمعم که ز دوری تو ای مایه ناز</p></div>
<div class="m2"><p>کارم همه شب گریه و سوز است و گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوتاهی عمر خویشتن می خواهم</p></div>
<div class="m2"><p>تا باز رهم از غم شبهای دراز</p></div></div>