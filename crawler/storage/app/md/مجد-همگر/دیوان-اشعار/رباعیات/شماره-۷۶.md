---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>محرومی وصل تو دل و جانم کاست</p></div>
<div class="m2"><p>شد طبع کژت به رغم من با همه راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که امید از تو به یکره ببرم</p></div>
<div class="m2"><p>عذرم به هزار سال نتوانی خواست</p></div></div>