---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>در عشق توام نه صبر برخاست نه دل</p></div>
<div class="m2"><p>بی روی توام نه عقل پیداست نه دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این غم که مراست کوه قاف است نه غم</p></div>
<div class="m2"><p>وان دل که تراست سنگ خاراست نه دل</p></div></div>