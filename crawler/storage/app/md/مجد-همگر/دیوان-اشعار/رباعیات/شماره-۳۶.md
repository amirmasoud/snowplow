---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای چرخ بیار هر چه داری ز عذاب</p></div>
<div class="m2"><p>زنهار مده مرا و در بد بشتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوفان بلا بیار و مندیش از من</p></div>
<div class="m2"><p>زیرا که تو بارانی و من خشت در آب</p></div></div>