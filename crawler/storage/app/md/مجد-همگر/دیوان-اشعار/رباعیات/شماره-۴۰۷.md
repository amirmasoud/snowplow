---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>گفتم به یکی کله که خاکش فرسود</p></div>
<div class="m2"><p>چونی؟ که بدی؟ جفای چرخت چه نمود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا چو تو بودم و چنین گشتم، زود</p></div>
<div class="m2"><p>تو نیز نه بس دیر چو من خواهی بود</p></div></div>