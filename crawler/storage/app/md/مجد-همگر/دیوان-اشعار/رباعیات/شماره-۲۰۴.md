---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>غم کشت مرا و غمگسار آگه نیست</p></div>
<div class="m2"><p>دل خون شد و دلدار ز کار آگه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این با که توان گفت که عمرم بگذشت</p></div>
<div class="m2"><p>در حسرت روی یار و یار آگه نیست</p></div></div>