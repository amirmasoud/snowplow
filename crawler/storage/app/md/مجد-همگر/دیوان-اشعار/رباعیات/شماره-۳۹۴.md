---
title: >-
    شمارهٔ ۳۹۴
---
# شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>والله که گر دل مرا خوش نکند</p></div>
<div class="m2"><p>یا چاره این جان بلاکش نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آینه عارضش آن کار کند</p></div>
<div class="m2"><p>دود دل من که هیچ آتش نکند</p></div></div>