---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>دیدم شب و روز رنج و بیداری‌ها</p></div>
<div class="m2"><p>تا در تو رسیده‌ام به دشواری‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ذات پسندیده تو حق عزیز</p></div>
<div class="m2"><p>مپسند مرا بدین چنین خواری‌ها</p></div></div>