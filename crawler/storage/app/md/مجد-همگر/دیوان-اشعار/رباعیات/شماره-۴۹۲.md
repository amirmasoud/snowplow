---
title: >-
    شمارهٔ ۴۹۲
---
# شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>درمان چو نجوئیم به دل درد مباش</p></div>
<div class="m2"><p>گرم است دلم با تو به دل سرد مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا برخوری از جان و جوانی و جمال</p></div>
<div class="m2"><p>باعاشق پیر ناجوانمرد مباش</p></div></div>