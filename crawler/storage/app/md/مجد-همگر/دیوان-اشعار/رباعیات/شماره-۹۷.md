---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>یاری دارم که عبرت ماه و خور است</p></div>
<div class="m2"><p>سر تا پایش ز یکدگر خوبتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با خرد و علم و هنر عاشق او</p></div>
<div class="m2"><p>و او عاشق و رند و جاهل و بی هنر است</p></div></div>