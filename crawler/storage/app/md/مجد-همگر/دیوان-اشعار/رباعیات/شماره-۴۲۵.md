---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>خون گر ز لقب تاشی ات آگاه شود</p></div>
<div class="m2"><p>از ننگ تو همچو سایه در چاه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود عجب از غایت کژ رفتن تو</p></div>
<div class="m2"><p>گر سایه ز صحبت تو گمراه شود</p></div></div>