---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>در بزم شهان چو آب من روشن نیست</p></div>
<div class="m2"><p>جز بر سر خاک تیره ام مسکن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شد که نبد رخصت بیرون شدنم</p></div>
<div class="m2"><p>اکنونم اجازه درون رفتن نیست</p></div></div>