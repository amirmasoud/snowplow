---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ای قد خوش تو سرو شاداب حیات</p></div>
<div class="m2"><p>لعل لب تو چشمه نایاب حیات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قد تو باد در کف سرو چمن</p></div>
<div class="m2"><p>با لعل تو خاک بر سر آب حیات</p></div></div>