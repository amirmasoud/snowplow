---
title: >-
    شمارهٔ ۵۱۸
---
# شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>گشتم ز جفای فلک و گردش سال</p></div>
<div class="m2"><p>بد حال و نخواهم که کسم داند حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا گریم و دوستم بگوید مگری</p></div>
<div class="m2"><p>یا نالم و دشمنم بگوید که منال</p></div></div>