---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>گر برسر آتشم نشانی چو سپند</p></div>
<div class="m2"><p>ور جمله تنم جداکنی بند از بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرسند شوم به این و آن و نشوم</p></div>
<div class="m2"><p>هرگز به جدائی تو یکدم خرسند</p></div></div>