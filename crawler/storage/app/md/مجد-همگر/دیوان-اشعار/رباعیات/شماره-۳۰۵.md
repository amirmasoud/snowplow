---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>هرگز نکنی میل و دلت نگذارد</p></div>
<div class="m2"><p>کاندر عمری دمی دلت یاد آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان یار که در دل شب تیره تو را</p></div>
<div class="m2"><p>می آرد یاد و خون دل می بارد</p></div></div>