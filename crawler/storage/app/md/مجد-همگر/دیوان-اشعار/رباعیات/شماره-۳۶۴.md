---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>بر یاد رخ و قد تو ای سرو بلند</p></div>
<div class="m2"><p>روی گل و پای سرو بوسم یکچند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل چون تو برنجد و بگرداند روی</p></div>
<div class="m2"><p>از جا برود سرو و ببرد پیوند</p></div></div>