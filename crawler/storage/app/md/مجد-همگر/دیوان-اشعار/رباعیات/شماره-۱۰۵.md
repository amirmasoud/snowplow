---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>بی روی توام ز لاله زار آزار است</p></div>
<div class="m2"><p>بی چشم تو نرگس بر چشمم خوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی قد تو سرو نیست برکارم راست</p></div>
<div class="m2"><p>بی رنگ رخ تو گل به چشمم خار است</p></div></div>