---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>از خاک چو رخت من بر افلاک نهند</p></div>
<div class="m2"><p>داغ اجلم بر دل غمناک نهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد کنم کفن دران از دستت</p></div>
<div class="m2"><p>حالی چو هزار دست در خاک نهند</p></div></div>