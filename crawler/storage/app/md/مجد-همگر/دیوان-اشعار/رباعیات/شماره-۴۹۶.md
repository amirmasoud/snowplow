---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>سبحان لله قد و خط و رخسارش</p></div>
<div class="m2"><p>وان ابرو و چشم و لب شکر بارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که به التماس من صورت کرد</p></div>
<div class="m2"><p>نقاش قضا به مستطر و پرگارش</p></div></div>