---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>خاکی که به فرمان تو از ره برخاست</p></div>
<div class="m2"><p>طاقی شد و جفت بارگاهش جوزاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چرخ به فرمان تو سر درنارد</p></div>
<div class="m2"><p>با خاک رهش کند تف قهر تو راست</p></div></div>