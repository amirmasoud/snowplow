---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>جانا سر زلفت ز پس و پشت مپیچ</p></div>
<div class="m2"><p>پیچ و خم زلف تو مرا کشت مپیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیش حدیثی که میان من و تست</p></div>
<div class="m2"><p>چون زلف دراز خود بر انگشت مپیچ</p></div></div>