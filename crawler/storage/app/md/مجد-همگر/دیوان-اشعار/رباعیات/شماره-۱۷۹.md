---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>در خواب غرور خلق بیداری نیست</p></div>
<div class="m2"><p>وز مستی جهل دهر هشیاری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احوال غم فراق آن عهد شکن</p></div>
<div class="m2"><p>در حال چو با قلم بگفتم بگریست</p></div></div>