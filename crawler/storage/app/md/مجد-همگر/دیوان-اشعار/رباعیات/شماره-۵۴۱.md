---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>هجر تو تباه کرد حالم چکنم</p></div>
<div class="m2"><p>بگرفت ز جان بی تو ملالم چکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی مگری منال در فرقت من</p></div>
<div class="m2"><p>چون بی تو نگریم و ننالم چکنم</p></div></div>