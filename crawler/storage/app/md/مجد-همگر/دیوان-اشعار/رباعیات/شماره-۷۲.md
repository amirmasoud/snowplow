---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>چشم تو مرا به تیر مژگان خسته ست</p></div>
<div class="m2"><p>زلف تو مرا به تار موئی بسته ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب که این سر سبکی آشفته ست</p></div>
<div class="m2"><p>زنهار که آن معربدی بدمست ست</p></div></div>