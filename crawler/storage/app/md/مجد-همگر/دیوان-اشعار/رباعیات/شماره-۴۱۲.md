---
title: >-
    شمارهٔ ۴۱۲
---
# شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>می جز ز کف تو نوش لب خوش نبود</p></div>
<div class="m2"><p>بی روی تو خوش عیش و طرب خوش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب خوش مکن ای دوست که بر دلشدگان</p></div>
<div class="m2"><p>بی عارض چون روز تو شب خوش نبود</p></div></div>