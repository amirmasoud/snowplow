---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای چرخ عنانم از سفر هیچ متاب</p></div>
<div class="m2"><p>نانم ز سراندیب ده آبم زسراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شام ز بامیان دهم قرصی نان</p></div>
<div class="m2"><p>هر بام ز شام ده مرا شربتی آب</p></div></div>