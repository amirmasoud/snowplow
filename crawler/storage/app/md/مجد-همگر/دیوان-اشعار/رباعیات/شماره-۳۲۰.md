---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>محرم بدم و جور تو محرومم کرد</p></div>
<div class="m2"><p>حاکم بدم و حکم تو محکومم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خارا بدم و هجر تو چون آبم کرد</p></div>
<div class="m2"><p>آهن بدم و عشق تو چون مومم کرد</p></div></div>