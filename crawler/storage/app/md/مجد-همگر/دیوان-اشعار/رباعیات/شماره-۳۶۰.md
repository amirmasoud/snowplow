---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>یارم به تفرج چمن بیرون شد</p></div>
<div class="m2"><p>بر باره چو مه سوار برگردون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بست نقاب تا بپوشد رخ خوب</p></div>
<div class="m2"><p>خوبیش ز بستن نقاب افزون شد</p></div></div>