---
title: >-
    شمارهٔ ۳۵۹
---
# شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>در دل نفسی یاد تو گردیده نشد</p></div>
<div class="m2"><p>کآن لحظه دو جوی آبم از دیده نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر رفت و هوای توام از سر نه برفت</p></div>
<div class="m2"><p>دیده بشد و وصال تو دیده نشد</p></div></div>