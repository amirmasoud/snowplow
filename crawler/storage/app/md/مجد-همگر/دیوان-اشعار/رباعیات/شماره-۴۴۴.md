---
title: >-
    شمارهٔ ۴۴۴
---
# شمارهٔ ۴۴۴

<div class="b" id="bn1"><div class="m1"><p>دیدمش خوی از گل ترش می بارید</p></div>
<div class="m2"><p>مشک از دو کمند عنبرش می بارید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سرو چمن در چمن باغ چمان</p></div>
<div class="m2"><p>وز باد شکوفه بر سرش می بارید</p></div></div>