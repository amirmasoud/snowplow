---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>چون تنگ دل آکنده شد از بس گله هات</p></div>
<div class="m2"><p>معذورم اگر برم به هر کس گله هات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز تنگدلی چو شرح جور تو دهم</p></div>
<div class="m2"><p>اول نفسم گریه بود پس گله هات</p></div></div>