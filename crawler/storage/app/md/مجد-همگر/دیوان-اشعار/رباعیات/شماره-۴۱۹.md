---
title: >-
    شمارهٔ ۴۱۹
---
# شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>ماهی که به مهرش دل خور گرم شود</p></div>
<div class="m2"><p>کی با تو دلش بر سر آزرم شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخی که به غمزه سیل خون می راند</p></div>
<div class="m2"><p>از قطره گریه تو کی نرم شود</p></div></div>