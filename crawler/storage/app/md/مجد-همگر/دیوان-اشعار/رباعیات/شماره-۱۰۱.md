---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>کارم همه جانسپاری و ترک سر است</p></div>
<div class="m2"><p>خوردم همه درد دل و خون جگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین تنگدلی و تنگ عیشی که مراست</p></div>
<div class="m2"><p>ضعف دل و تنگی نفس صعبتر است</p></div></div>