---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>مگذار که کار دل قراری گیرد</p></div>
<div class="m2"><p>یا همنفسی و غمگساری گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی یار چنین فتاده بگذار او را</p></div>
<div class="m2"><p>تا بو که بود چون تو یاری گیرد</p></div></div>