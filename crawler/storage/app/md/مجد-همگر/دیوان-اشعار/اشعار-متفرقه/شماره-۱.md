---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>نه در فراق توام طاقت شکیبائیست</p></div>
<div class="m2"><p>نه در وصال توام ایمنی ز شیدائیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در وصال تو شادم نه در فراق صبور</p></div>
<div class="m2"><p>چه طالع است مرا یارب این چه رسوائیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا به عادت هر یار رنج دل منما</p></div>
<div class="m2"><p>اگر چه عادت خوبان همیشه خودرائیست</p></div></div>