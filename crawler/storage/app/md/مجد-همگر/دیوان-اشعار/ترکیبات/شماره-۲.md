---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>از رخت لاله در نظر روید</p></div>
<div class="m2"><p>وز غمت خار در جگر روید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدت ار سایه بر زمین فکند</p></div>
<div class="m2"><p>خشک و تر سرو غاتفر روید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور فتد آب لعل تو بر خاک</p></div>
<div class="m2"><p>بوم و بر جمله نیشکر روید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لفظ چون شکرت اگر شنود</p></div>
<div class="m2"><p>صخره حالی نبات بر روید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نیی را به خدمت لب تو</p></div>
<div class="m2"><p>بر میان هم ز تن کمر روید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط تو تربیت ز اشکم یافت</p></div>
<div class="m2"><p>زانکه سبزه هم از مطر روید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس که گریم ز خط دمیدن تو</p></div>
<div class="m2"><p>که ز نم سبزه بیشتر روید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشقان بر دری چه سر پاشند</p></div>
<div class="m2"><p>که ازو چون گیاه سر روید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دری بر رهت فشانم زر</p></div>
<div class="m2"><p>که ز خاک فضاش زر روید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مخدوم صاحب دیوان</p></div>
<div class="m2"><p>که ازو شاخ فخر و فر روید</p></div></div>
<div class="b2" id="bn11"><p>صاحب عصر و پیشوای جهان</p>
<p>آن جهان بها بهای جهان</p></div>
<div class="b" id="bn12"><div class="m1"><p>شکرش در سخن گهر ریزد</p></div>
<div class="m2"><p>پسته اش خندد و شکر ریزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاشقش همچو شمع در شب وصل</p></div>
<div class="m2"><p>پیش رویش نخست سر ریزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر سموم عتاب او بوزد</p></div>
<div class="m2"><p>از درخت حیات بر ریزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور نسیم عنایتش بجهد</p></div>
<div class="m2"><p>گلبن خشک برگ تر ریزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابر بر بام او به جای سرشک</p></div>
<div class="m2"><p>همه خونابه جگر ریزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چشم آهووشش ز مخموری</p></div>
<div class="m2"><p>جرعه از خون شیر نر ریزد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یار هر کس به زر درآرد سر</p></div>
<div class="m2"><p>یار من همچو ریگ زر ریزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کیمیای نکوست خاک درش</p></div>
<div class="m2"><p>که سبیکه ز قرص خور ریزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مجد آن درهمی زند که بر او</p></div>
<div class="m2"><p>گر پرد جبرئیل پر ریزد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با همه فقر گنج و کان ثنا</p></div>
<div class="m2"><p>پیش مخدوم دادگر ریزد</p></div></div>
<div class="b2" id="bn22"><p>صاحب عصر و مقتدای جهان</p>
<p>آن جهان بها بهای جهان</p></div>
<div class="b" id="bn23"><div class="m1"><p>هر که از وصل او نشان خواهد</p></div>
<div class="m2"><p>پر سیمرغ و آشیان خواهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وانکه نور جمال او طلبد</p></div>
<div class="m2"><p>خسرو چارم آسمان خواهد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنکه از وصل او زیان بیند</p></div>
<div class="m2"><p>اجل از عمر او زیان خواهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پری از دام زلف او برمد</p></div>
<div class="m2"><p>فتنه از چشم او امان خواهد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عشق او هر که اختیار کند</p></div>
<div class="m2"><p>عافیت زانسوی جهان خواهد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دام زلفینش حلق دل گیرد</p></div>
<div class="m2"><p>تیر چشمش هلاک جان خواهد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر رخ از دیده جوی خون راند</p></div>
<div class="m2"><p>هر که دلجوی آنچنان خواهد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در جهان هر کجا که داد دهیست</p></div>
<div class="m2"><p>داد از آن چشم دلستان خواهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خون جانم ز چشم خونخوارش</p></div>
<div class="m2"><p>تیغ عدل خدایگان خواهد</p></div></div>
<div class="b2" id="bn32"><p>صاحب عصر و کدخدای جهان</p>
<p>آن جهان بها بهای جهان</p></div>