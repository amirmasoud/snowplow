---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دلم نهان شد و شد عافیت نهان از دل</p></div>
<div class="m2"><p>هلاک جان من آمد دلم فغان از دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چین زلف تو در مدتیست تا گم شد</p></div>
<div class="m2"><p>دهان تنگ توام می دهد نشان از دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروشد از پی تو دم به درد و رفت به غم</p></div>
<div class="m2"><p>برآمد از غم تو دل ز جان و جان از دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه طعنه بود که بر دل نیامد آن از من</p></div>
<div class="m2"><p>چه جور ماند که بر من نیامد آن از دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبانه گشت زبان در دهان ز سوز دلم</p></div>
<div class="m2"><p>بلی همیشه حکایت کند زبان از دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغ کز دهن دشمن آشکارا شد</p></div>
<div class="m2"><p>حدیث دوست که می داشتم نهان از دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شرح حال دل خود چه حاجت است مرا</p></div>
<div class="m2"><p>که خون دیده خبر می دهد عیان از دل</p></div></div>
<div class="b2" id="bn8"><p>صبوری من بیچاره اختیاری نیست</p>
<p>چه چاره چون زغم عشق رستگاری نیست</p></div>
<div class="b" id="bn9"><div class="m1"><p>براندم از غم هجر تو خون ناب از چشم</p></div>
<div class="m2"><p>مرا ز فرقت رویت برفت خواب از چشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم بر آتش هجرت کباب گشت و کنون</p></div>
<div class="m2"><p>همی برآید خونابه کباب از چشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مپوش چهره و بر بنده روزتیره مکن</p></div>
<div class="m2"><p>که روز تیره شود چون شد آفتاب از چشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چشم من چو در آمد عقیق تو پس ازان</p></div>
<div class="m2"><p>حقیقتم که بیفتاد لعل ناب از چشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه زآب چشم من آید خلل در آتش دل</p></div>
<div class="m2"><p>نه نیزم آتش دل بازدارد آب از چشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر به سیل بناها همی خراب شود</p></div>
<div class="m2"><p>بنای کالبد من شود خراب از چشم</p></div></div>
<div class="b2" id="bn15"><p>بشد ز کرده چشم و دلم قرار از جان</p>
<p>برآردم غم این هر دوان دمار از جان</p></div>
<div class="b" id="bn16"><div class="m1"><p>بشد مرا به امید تو روزگار از دست</p></div>
<div class="m2"><p>بدادم از پی وصل تو کار و بار از دست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>منم فتاده پای غم تو دستم گیر</p></div>
<div class="m2"><p>مکن ستیزه و این سرکشی بدار از دست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز دست من به جوانی مشو نگارینا</p></div>
<div class="m2"><p>که بی جوانی ناید نکو نگار از دست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز سیل دیده تنم را گذشت آب از سر</p></div>
<div class="m2"><p>ز سوز سینه دلم را برفت کار از دست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا زمانه هم این خار برکشد از پای</p></div>
<div class="m2"><p>گرم عنان نکشد عمر پایدار از دست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز دست ظلم تو فردا نگر که نگذارم</p></div>
<div class="m2"><p>عنان صفدر و سردار روزگار از دست</p></div></div>
<div class="b2" id="bn22"><p>معین ملک و شهنشاه و شهریار جهان</p>
<p>که اختیار خدای است و افتخار جهان</p></div>
<div class="b" id="bn23"><div class="m1"><p>زمانه دور جوانی گرفت باز از سر</p></div>
<div class="m2"><p>زمین دگر کند آغاز کشف راز از سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>فغان بلبل سرمست زان ز حد بگذشت</p></div>
<div class="m2"><p>که غنچه می ننهد سرکشی و ناز از سر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرفت باد صبا رسم معدلت بر دست</p></div>
<div class="m2"><p>نهاد لشکر دی شور و شر و تاز ازسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کشید باغ قبای زمردی در بر</p></div>
<div class="m2"><p>چو که کلاه حواصل نهاد باز از سر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فکند در چمن آوازه قامت بت من</p></div>
<div class="m2"><p>گرفت قامت سرو سهی نماز از سر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو آب شعر مرا دوش در چمن بلبل</p></div>
<div class="m2"><p>بخواند مدح سپهدار سرفراز سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>علو همت او چرخ را حقیقت شد</p></div>
<div class="m2"><p>نهاد لاجرم آن نخوت و مجاز از سر</p></div></div>
<div class="b2" id="bn30"><p>خدیو ملک کرم شهریار کشور جود</p>
<p>که هست نام همایونش سکه زر جود</p></div>
<div class="b" id="bn31"><div class="m1"><p>زهی خدای ترا عمر جاودان داده</p></div>
<div class="m2"><p>سپهر پیر ترا دولتی جوان داده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به روز بزم کف چون سحاب تو به سخا</p></div>
<div class="m2"><p>قفای بحر زده گوشمال کان داده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به گاه رزم لب تیغ آبدار تو خصم</p></div>
<div class="m2"><p>هزار بار ببوسیده خاک و جان داده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لطافت تو به اعجاز خلق گاه سخن</p></div>
<div class="m2"><p>چو عیسی از دم خود مرده را روان داده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سیاست تو به هنگام کین ز کله خصم</p></div>
<div class="m2"><p>همای را به سر نیزه استخوان داده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به جنگ خنجر نیلوفریت را نصرت</p></div>
<div class="m2"><p>ز خون گرم عدو رنگ ارغوان داده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دبیر چرخ ز دیوان عمر روز قضا</p></div>
<div class="m2"><p>برات عمر حسود تو زان جهان داده</p></div></div>
<div class="b2" id="bn38"><p>زهی زمانه گرفته بها و زیب از تو</p>
<p>چو بخت و دولت و اقبال ناشکیب از تو</p></div>
<div class="b" id="bn39"><div class="m1"><p>توئی که بخت تو در دهر کامکاری کرد</p></div>
<div class="m2"><p>توئی که در همه کارت خدای یاری کرد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سپهر در پی امر تو ره به سر پیمود</p></div>
<div class="m2"><p>زمانه بر در حکم تو جانسپاری کرد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به خاکبوس درت رغبتی نمود ملک</p></div>
<div class="m2"><p>قضاش گفت تو آن پردلی نیاری کرد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تواضع تو بدین آرزوش رخصت داد</p></div>
<div class="m2"><p>که تا بداند باری که بردباری کرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>پلنگ را اثر عدل تو بر آن بگماشت</p></div>
<div class="m2"><p>که شیر در دهن غرم مرغزاری کرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به هر طرف پی احسان تو چنان ره برد</p></div>
<div class="m2"><p>که باز دایگی کبک کوهساری کرد</p></div></div>
<div class="b2" id="bn45"><p>زهی به فر تو آراسته زمان و زمین</p>
<p>تراست دست تحکم بر آسمان و زمین</p></div>
<div class="b" id="bn46"><div class="m1"><p>کسی که در کنف سایه خدا باشد</p></div>
<div class="m2"><p>همیشه بر همه مقصود پادشا باشد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تراست این همه اقبال و باشد آن کس را</p></div>
<div class="m2"><p>که برکشیده و بگزیده خدا باشد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>درت چو بر رخ خواهندگان گشاده شود</p></div>
<div class="m2"><p>صریرش از کرم اهلا و مرحبا باشد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به مجلسی که ز خلق خوشت سخن رانند</p></div>
<div class="m2"><p>حدیث مشک ختا گر رود خطا باشد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ملوک را ز تو تشریف هاست خود چه شود</p></div>
<div class="m2"><p>که وقت نوبت تشریف این گدا باشد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بدین مطایبه مجرم نیم که با چو توئی</p></div>
<div class="m2"><p>به مذهب ظرفا اینقدر روا باشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ربیع عمر تو بادا چنانکه تا صد سال</p></div>
<div class="m2"><p>درخت دولت و عمر تو در نما باشد</p></div></div>