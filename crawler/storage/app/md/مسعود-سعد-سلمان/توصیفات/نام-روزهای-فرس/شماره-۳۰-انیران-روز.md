---
title: >-
    شمارهٔ ۳۰ - انیران روز
---
# شمارهٔ ۳۰ - انیران روز

<div class="b" id="bn1"><div class="m1"><p>انیران ز پیران شنیدم چنان</p></div>
<div class="m2"><p>که می خورد باید به رطل گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیارای نگار آن می مشکبوی</p></div>
<div class="m2"><p>کزو نافه مشک یابی دهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل اندر کم و بیش گیتی مبند</p></div>
<div class="m2"><p>همی دار جان را همی شادمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که شادست و زو مملکت شاد باد</p></div>
<div class="m2"><p>شهنشاه گیتی ملک ارسلان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دولت جهان را جوان دارد او</p></div>
<div class="m2"><p>که بختش جوان باد و ملکش جوان</p></div></div>