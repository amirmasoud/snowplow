---
title: >-
    شمارهٔ ۵ - سپندارمذ روز
---
# شمارهٔ ۵ - سپندارمذ روز

<div class="b" id="bn1"><div class="m1"><p>سپندارمذ روز خیز ای نگار</p></div>
<div class="m2"><p>سپندار آر ما را و جام می آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می آر از پی آنکه بی می نشد</p></div>
<div class="m2"><p>دلی شادمان و تنی شادخوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپند آر پی آنکه چشم بدان</p></div>
<div class="m2"><p>بگرداند ایزد ازین روزگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از عدل سلطان ملک ارسلان</p></div>
<div class="m2"><p>خزان گشت خرم تر از روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قوی باد ملکش که از ملک او</p></div>
<div class="m2"><p>شد اندر جهان عدل و جود آشکار</p></div></div>