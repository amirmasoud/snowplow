---
title: >-
    شمارهٔ ۲۵ - اردروز
---
# شمارهٔ ۲۵ - اردروز

<div class="b" id="bn1"><div class="m1"><p>ارد روزست فرخ و میمون</p></div>
<div class="m2"><p>با همه لهو و خرمی مقرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دلارای یار گلگون رخ</p></div>
<div class="m2"><p>خیز و پیش آر باده گلگون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به یاد خدایگان زمین</p></div>
<div class="m2"><p>شاد باشیم و می خوریم اکنون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه ملک ارسلان که او دارد</p></div>
<div class="m2"><p>تاج جمشید و تخت افریدون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد عدلش همیشه دهر آرای</p></div>
<div class="m2"><p>باد ملکش همیشه روز افزون</p></div></div>