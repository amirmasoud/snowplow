---
title: >-
    شمارهٔ ۱۷ - سروش روز
---
# شمارهٔ ۱۷ - سروش روز

<div class="b" id="bn1"><div class="m1"><p>روز سروش است که گوید سروش</p></div>
<div class="m2"><p>باده خور و نغمه مطرب نیوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبز شد از سبزه همه بوستان</p></div>
<div class="m2"><p>لعل می آر ای صنم سبزپوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه جهاندار ملک ارسلان</p></div>
<div class="m2"><p>می ز کف نوش لبی کرد نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه دهد یاری جاهش فلک</p></div>
<div class="m2"><p>وآنکه کند قوت ملکش سروش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به ابد دولت و اقبال را</p></div>
<div class="m2"><p>باد گشاده سوی فرمانش گوش</p></div></div>