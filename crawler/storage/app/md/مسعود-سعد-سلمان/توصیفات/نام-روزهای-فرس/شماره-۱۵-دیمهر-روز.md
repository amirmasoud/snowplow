---
title: >-
    شمارهٔ ۱۵ - دیمهر روز
---
# شمارهٔ ۱۵ - دیمهر روز

<div class="b" id="bn1"><div class="m1"><p>ای مرا همچو جان و از جان به</p></div>
<div class="m2"><p>بامدادان نشاط کن، برجه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی به مهرست مهربانی کن</p></div>
<div class="m2"><p>کز همه چیز مهربانی به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن از عز ملک سلطان گوی</p></div>
<div class="m2"><p>باده بر یاد ملک سلطان ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه ملک ارسلان که عالم را</p></div>
<div class="m2"><p>غرقه کردست در عطای فره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مایه جود او ز دریا بیش</p></div>
<div class="m2"><p>پایه جاه او ز گردون به</p></div></div>