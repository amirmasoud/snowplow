---
title: >-
    شمارهٔ ۳ - اردیبهشت روز
---
# شمارهٔ ۳ - اردیبهشت روز

<div class="b" id="bn1"><div class="m1"><p>اردیبهشت روزست ای ماه دلستان</p></div>
<div class="m2"><p>امروز چون بهشت برینست بوستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن باده ای که خرم ازو گشت عیش و عمر</p></div>
<div class="m2"><p>زآن باده ای که گردد ازو تازه طبع و جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیرا رسیده ایم به دولت به کام خویش</p></div>
<div class="m2"><p>در ملک و دولت ملک و شاه کامران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان ابوالملوک ملک ارسلان که یافت</p></div>
<div class="m2"><p>از ملک او زمین شرف از اوج آسمان</p></div></div>