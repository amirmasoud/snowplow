---
title: >-
    شمارهٔ ۲۹ - مار اسپند روز
---
# شمارهٔ ۲۹ - مار اسپند روز

<div class="b" id="bn1"><div class="m1"><p>ای دلارام روز مار اسپند</p></div>
<div class="m2"><p>دست بی جام لعل می مپسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمی در جهان خرم بین</p></div>
<div class="m2"><p>شادمانی کن و بناز بخند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آنکه عدل خدایگان جهان</p></div>
<div class="m2"><p>بیخ جور و نیاز را برکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه ملک ارسلان بن مسعود</p></div>
<div class="m2"><p>شاه گیتی گشای دشمن بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک او را سپند سوز ای دوست</p></div>
<div class="m2"><p>کاین بود رسم روز مار اسپند</p></div></div>