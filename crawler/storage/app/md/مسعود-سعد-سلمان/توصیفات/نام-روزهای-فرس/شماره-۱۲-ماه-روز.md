---
title: >-
    شمارهٔ ۱۲ - ماه روز
---
# شمارهٔ ۱۲ - ماه روز

<div class="b" id="bn1"><div class="m1"><p>ماه روز ای به روی خوب چو ماه</p></div>
<div class="m2"><p>باده لعل مشکبوی بخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت روشن چو ماه بزم که گشت</p></div>
<div class="m2"><p>نام این روز ماه و روی تو ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاد گردان به باده ما را خیز</p></div>
<div class="m2"><p>که جهان شاد شد به دولت شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شه ملک ارسلان بن مسعود</p></div>
<div class="m2"><p>خسرو جود و رز داد پناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بود گاه و افسر آلت ملک</p></div>
<div class="m2"><p>باد ازو افتخار افسر و گاه</p></div></div>