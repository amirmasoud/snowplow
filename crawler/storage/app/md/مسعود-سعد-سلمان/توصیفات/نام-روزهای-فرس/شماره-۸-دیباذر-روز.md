---
title: >-
    شمارهٔ ۸ - دیباذر روز
---
# شمارهٔ ۸ - دیباذر روز

<div class="b" id="bn1"><div class="m1"><p>روز دی است خیز و بیار ای نگار می</p></div>
<div class="m2"><p>ای ترک می بیار که ترکی گرفت دی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ده به رطل و جام که در بزم خسروی</p></div>
<div class="m2"><p>بنشست شاه شاد ملک ارسلان به می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهی که کرد چرخ و فلک را به زیر پای</p></div>
<div class="m2"><p>تا کرد فرش شاهی و دولت به زیر پی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ملک را به نام وی اسناد کرد چرخ</p></div>
<div class="m2"><p>کرد از زمانه نام ملوک زمانه طی</p></div></div>