---
title: >-
    شمارهٔ ۲۴ - دین روز
---
# شمارهٔ ۲۴ - دین روز

<div class="b" id="bn1"><div class="m1"><p>دین روز ای روی تو آگفت دین</p></div>
<div class="m2"><p>می خور و شادی کن و خرم نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با می و می خوردن دین را چه کار</p></div>
<div class="m2"><p>می خور و می نوش و قوی دار دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گنهی کز می حاصل شود</p></div>
<div class="m2"><p>محو کند خدمت شاه زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه جهانگیر ملک ارسلان</p></div>
<div class="m2"><p>آنکه کند ملک بر او آفرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به نگین نازد ملک جهان</p></div>
<div class="m2"><p>ملک جهان بادش زیر نگین</p></div></div>