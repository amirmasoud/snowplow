---
title: >-
    شمارهٔ ۲۷ - آسمان روز
---
# شمارهٔ ۲۷ - آسمان روز

<div class="b" id="bn1"><div class="m1"><p>آسمان روز ای چو ماه آسمان</p></div>
<div class="m2"><p>باده نوش و دار دل را شادمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ز باده شاد کن زیرا که عقل</p></div>
<div class="m2"><p>باده را بیند همی شادی جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان باده خور ای تازه چو گل</p></div>
<div class="m2"><p>تازه کن شادی به باده هر زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر جوی از جود خورشید ملوک</p></div>
<div class="m2"><p>مدح خوان در صدر سلطان جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو را گردد جهانی شکر گوی</p></div>
<div class="m2"><p>تا تو را باشد جهانی مدح خوان</p></div></div>