---
title: >-
    شمارهٔ ۲ - اردی بهشت ماه
---
# شمارهٔ ۲ - اردی بهشت ماه

<div class="b" id="bn1"><div class="m1"><p>بهشت است گیتی ز اردیبهشت</p></div>
<div class="m2"><p>حلال آمد ای مه می اندر بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شادی نشین هین و می خواه می</p></div>
<div class="m2"><p>که بی می نشستنت زشتست زشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راغ و به باغ و به کوه و به دشت</p></div>
<div class="m2"><p>ز فر گرانمایه اردی بهشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخندید گلزار و بگریست ابر</p></div>
<div class="m2"><p>بنالید مرغ و ببالید کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی کله یابی که رضوانش بافت</p></div>
<div class="m2"><p>بسی حله بینی که حوراش رشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گویی که ملک ملک ارسلان</p></div>
<div class="m2"><p>گل و عنبر و مشک درهم سرشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهاندار شاهی که چرخ بلند</p></div>
<div class="m2"><p>به ملکش یکی عهد محکم نبشت</p></div></div>