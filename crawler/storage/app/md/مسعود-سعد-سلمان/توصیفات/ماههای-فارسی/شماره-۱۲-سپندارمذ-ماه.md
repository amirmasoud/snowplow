---
title: >-
    شمارهٔ ۱۲ - سپندارمذ ماه
---
# شمارهٔ ۱۲ - سپندارمذ ماه

<div class="b" id="bn1"><div class="m1"><p>سپندارمذماه آخر ز سال</p></div>
<div class="m2"><p>که گشت آخرین ماه هر بدسگال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی مژده دارد که تا چند روز</p></div>
<div class="m2"><p>پذیرد چمن حسن و زیب و جمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر مرغزاری بتازد تذرو</p></div>
<div class="m2"><p>به هر بوستانی ببالد نهال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشد ابر بر سایه فرش بهار</p></div>
<div class="m2"><p>دمد مشک بر کوه باد شمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سلطان گیتی ملک ارسلان</p></div>
<div class="m2"><p>شود طالع سال فرخنده فال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهاندار شاها تویی از ملوک</p></div>
<div class="m2"><p>که گردون محلی و دریا نوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو مهر مضی تاب و بر خلق تاب</p></div>
<div class="m2"><p>چو سرو سهی بال و در ملک بال</p></div></div>