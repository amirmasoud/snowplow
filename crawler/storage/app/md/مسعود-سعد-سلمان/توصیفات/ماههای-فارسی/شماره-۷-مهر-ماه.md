---
title: >-
    شمارهٔ ۷ - مهر ماه
---
# شمارهٔ ۷ - مهر ماه

<div class="b" id="bn1"><div class="m1"><p>ای مه مه مهر و مهر ماه است</p></div>
<div class="m2"><p>بی باده نشستن از گناه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و رخ دوستان سپید است</p></div>
<div class="m2"><p>روی و دل دشمنان سیاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطان ملک ارسلان مسعود</p></div>
<div class="m2"><p>در ملک به کام نیکخواه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهان همه بندگان اویند</p></div>
<div class="m2"><p>امروز چو او کدام شاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبه است عزیز و پیشگاهش</p></div>
<div class="m2"><p>یارب چه خجسته پیشگاه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکتاست به بندگیش گردون</p></div>
<div class="m2"><p>گر چند به خدمتش دو تاه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایوانش نه پیشگاه ایوانش</p></div>
<div class="m2"><p>سرمایه عز و اصل جاه است</p></div></div>