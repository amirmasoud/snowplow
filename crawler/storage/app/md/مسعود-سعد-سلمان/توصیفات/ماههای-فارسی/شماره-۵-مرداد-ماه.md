---
title: >-
    شمارهٔ ۵ - مرداد ماه
---
# شمارهٔ ۵ - مرداد ماه

<div class="b" id="bn1"><div class="m1"><p>مردان مهست سخت خرم</p></div>
<div class="m2"><p>می نوش پیاپی و دمادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گردون طبع خاک پر تف</p></div>
<div class="m2"><p>وز باران چشم ابر پر نم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دشت لباسهای رو نیست</p></div>
<div class="m2"><p>بر کوه لباسهای میرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنشین و طرب فزای و می خواه</p></div>
<div class="m2"><p>در دولت شهریار اعظم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطان ملک ارسلان مسعود</p></div>
<div class="m2"><p>تاج سر خسروان عالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تاج به تو شده مزین</p></div>
<div class="m2"><p>وی تخت تو را شده مسلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شاد نشین که دشمن تو</p></div>
<div class="m2"><p>از هول تو جان داد در غم</p></div></div>