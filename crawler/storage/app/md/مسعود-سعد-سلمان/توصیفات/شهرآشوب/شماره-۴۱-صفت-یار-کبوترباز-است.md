---
title: >-
    شمارهٔ ۴۱ - صفت یار کبوترباز است
---
# شمارهٔ ۴۱ - صفت یار کبوترباز است

<div class="b" id="bn1"><div class="m1"><p>انس تو با کبوترست همه</p></div>
<div class="m2"><p>ننگری از هوس به چاکر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم به ساعت بر تو باز آید</p></div>
<div class="m2"><p>هر کبوتر که رانی از بر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتن و آمدن به نزد رهی</p></div>
<div class="m2"><p>چون نیاموزی از کبوتر خویش</p></div></div>