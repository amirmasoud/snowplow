---
title: >-
    شمارهٔ ۲۴ - در حق دلبر نوخط گفته
---
# شمارهٔ ۲۴ - در حق دلبر نوخط گفته

<div class="b" id="bn1"><div class="m1"><p>نیکوتری به چشم من از دولت</p></div>
<div class="m2"><p>وز نعمت جوانی شیرین تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی و نور داده تو را ایزد</p></div>
<div class="m2"><p>سروی و آب داده تو را کوثر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرگار حسن بر رخ تو گشته</p></div>
<div class="m2"><p>صد دایره فکنده بر آن رخ بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر مشک زده دایره را از آن</p></div>
<div class="m2"><p>افتاده نقطه ای به میان اندر</p></div></div>