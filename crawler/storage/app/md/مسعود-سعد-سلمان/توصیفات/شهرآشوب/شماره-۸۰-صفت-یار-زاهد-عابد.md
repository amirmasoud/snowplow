---
title: >-
    شمارهٔ ۸۰ - صفت یار زاهد عابد
---
# شمارهٔ ۸۰ - صفت یار زاهد عابد

<div class="b" id="bn1"><div class="m1"><p>تو زاهدی و دو زلف تو آفتاب پرست</p></div>
<div class="m2"><p>به سجده اید شما هر دو درگه و بیگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا دو چشم تو دیبای لعل پوشیدست</p></div>
<div class="m2"><p>اگر نپوشند ای دوست زاهدان دیباه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز راه گمشده را زاهدان به راه آرند</p></div>
<div class="m2"><p>تو باز مردم با راه را کنی بیراه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو زاهدی ز چه رویست چشمک تو دژم</p></div>
<div class="m2"><p>تو عابدی ز چه معین است زلفک تو دو تاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمانه تیغ غمان ای نگار بر دارد</p></div>
<div class="m2"><p>ز هر کسی که به چنگ آرد آن دو زلف سیاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر که هست بتا حلقه‌های زلفینت</p></div>
<div class="m2"><p>حروف اشهد ان لا اله الا الله</p></div></div>