---
title: >-
    شمارهٔ ۹ - صفت دلبر خباز کند
---
# شمارهٔ ۹ - صفت دلبر خباز کند

<div class="b" id="bn1"><div class="m1"><p>اندر تنور روی چو سوسن فرو بری</p></div>
<div class="m2"><p>چون شمع و گل برآری بازار تنور راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر سر تنوری می ترسم از تو ز انک</p></div>
<div class="m2"><p>طوفان نوح گاه نخست از تنور خاست</p></div></div>