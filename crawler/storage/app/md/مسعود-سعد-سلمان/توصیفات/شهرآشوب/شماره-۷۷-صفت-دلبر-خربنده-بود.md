---
title: >-
    شمارهٔ ۷۷ - صفت دلبر خربنده بود
---
# شمارهٔ ۷۷ - صفت دلبر خربنده بود

<div class="b" id="bn1"><div class="m1"><p>آهنین پوش ندیدم چو تو سرو</p></div>
<div class="m2"><p>نمدین خود ندیدم چو تو ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو را هرگز خربنده که دید</p></div>
<div class="m2"><p>ماه را دید کس از پشم کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ره راست بیفتاده ست آنک</p></div>
<div class="m2"><p>او تو را از پی خر دارد راه</p></div></div>