---
title: >-
    شمارهٔ ۴۷ - در حق دلبر احول گفته
---
# شمارهٔ ۴۷ - در حق دلبر احول گفته

<div class="b" id="bn1"><div class="m1"><p>ای دو زلفت چو ماه در آخر</p></div>
<div class="m2"><p>وی رخانت چو مشک در اول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احول اکحلی و متفقند</p></div>
<div class="m2"><p>خلق در حسن احول اکحل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شده بار دگر کسی هم جفت</p></div>
<div class="m2"><p>کرده با دیگری مرا تو بدل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر مرا نیستی امید وصال</p></div>
<div class="m2"><p>نیمی جفت یار دان به مثل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دو بینی همی و این نه شگفت</p></div>
<div class="m2"><p>یک دو بیند همی به چشم احول</p></div></div>