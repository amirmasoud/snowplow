---
title: >-
    شمارهٔ ۹۰ - صفت دلبر دیباباف است
---
# شمارهٔ ۹۰ - صفت دلبر دیباباف است

<div class="b" id="bn1"><div class="m1"><p>دیبا بافی ای بت دیبا رخ</p></div>
<div class="m2"><p>هر پیشه را به دو رخ برهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیبا بافی از همه جنسی تو</p></div>
<div class="m2"><p>چون روی خویش بافت نمی دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیبای روم کس نخرد هرگز</p></div>
<div class="m2"><p>گر نقش روی خویش بگردانی</p></div></div>