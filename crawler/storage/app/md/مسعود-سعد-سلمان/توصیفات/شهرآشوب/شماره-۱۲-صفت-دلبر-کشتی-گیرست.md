---
title: >-
    شمارهٔ ۱۲ - صفت دلبر کشتی گیرست
---
# شمارهٔ ۱۲ - صفت دلبر کشتی گیرست

<div class="b" id="bn1"><div class="m1"><p>ای دلارام یار کشتی گیر</p></div>
<div class="m2"><p>سینه تو ز سنگ آکنده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تنی کش برت زده ست آسیب</p></div>
<div class="m2"><p>همچو مارش ز هم پراکنده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تواندت بر زمین افکند</p></div>
<div class="m2"><p>ماه را بر زمین که افکنده ست</p></div></div>