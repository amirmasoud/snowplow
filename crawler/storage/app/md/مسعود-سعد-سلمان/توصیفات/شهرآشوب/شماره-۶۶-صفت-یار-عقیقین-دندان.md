---
title: >-
    شمارهٔ ۶۶ - صفت یار عقیقین دندان
---
# شمارهٔ ۶۶ - صفت یار عقیقین دندان

<div class="b" id="bn1"><div class="m1"><p>زرد کردی رخم به انده و غم</p></div>
<div class="m2"><p>لعل کردی دهان تنبول تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دندانت تا عقیق شدست</p></div>
<div class="m2"><p>لعل گشته ست جزع دیده من</p></div></div>