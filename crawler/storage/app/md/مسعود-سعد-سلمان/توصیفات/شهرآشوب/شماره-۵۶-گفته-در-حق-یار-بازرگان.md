---
title: >-
    شمارهٔ ۵۶ - گفته در حق یار بازرگان
---
# شمارهٔ ۵۶ - گفته در حق یار بازرگان

<div class="b" id="bn1"><div class="m1"><p>ای دلارام یار بازرگان</p></div>
<div class="m2"><p>ماه نقطه دهان موی میان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و جانم به بوسه بخری</p></div>
<div class="m2"><p>اینت کالا خریدن ارزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سود جست اندر آن که کرد آری</p></div>
<div class="m2"><p>سود جوید همیشه بازرگان</p></div></div>