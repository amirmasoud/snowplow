---
title: >-
    شمارهٔ ۳۸ - صفت دلبر صیاد بود
---
# شمارهٔ ۳۸ - صفت دلبر صیاد بود

<div class="b" id="bn1"><div class="m1"><p>تو را ای چو آهو به چشم و به تگ</p></div>
<div class="m2"><p>سگانند در تگ چو مرغی به پر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا با تو سازند کاهو و سگ</p></div>
<div class="m2"><p>نسازند پیوسته با یکدیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهی تو که هرگز نترسی ز شب</p></div>
<div class="m2"><p>گلی تو که تازه شوی از مطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نیلوفر انس تو با حوض آب</p></div>
<div class="m2"><p>چو لاله همی جای تو در خضر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا هر شبی ای دلارام یار</p></div>
<div class="m2"><p>چرا هر زمان ای نگارین پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دشتی دگر بینمت خوابگاه</p></div>
<div class="m2"><p>ز حوضی دگر بینمت آبخور</p></div></div>