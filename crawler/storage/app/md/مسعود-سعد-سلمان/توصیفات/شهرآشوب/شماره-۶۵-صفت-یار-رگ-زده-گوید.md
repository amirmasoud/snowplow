---
title: >-
    شمارهٔ ۶۵ - صفت یار رگ زده گوید
---
# شمارهٔ ۶۵ - صفت یار رگ زده گوید

<div class="b" id="bn1"><div class="m1"><p>خود را چرا رگ زدی بی علتی</p></div>
<div class="m2"><p>ای آنکه هست خون رگت جان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانسته ای که خون تو جان منست</p></div>
<div class="m2"><p>زین روی را بریخته ای خون ز تن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا از برای آن زده ای تا شوی</p></div>
<div class="m2"><p>بر رگ زدن دلیر چو من در سخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگ گلست دست تو آری بتا</p></div>
<div class="m2"><p>بر برگ گل درست شود رگ زدن</p></div></div>