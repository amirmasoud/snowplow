---
title: >-
    شمارهٔ ۶۰ - صفت یار روزه دار بود
---
# شمارهٔ ۶۰ - صفت یار روزه دار بود

<div class="b" id="bn1"><div class="m1"><p>ای بت شکر لب شیرین دهان</p></div>
<div class="m2"><p>خوبتر از عمری و خوشتر ز جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزه همی داری مردم کشی</p></div>
<div class="m2"><p>راست نیایند به هم این و آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه تو را دارد از روزه سود</p></div>
<div class="m2"><p>داردت از کشتن عاشق زیان</p></div></div>