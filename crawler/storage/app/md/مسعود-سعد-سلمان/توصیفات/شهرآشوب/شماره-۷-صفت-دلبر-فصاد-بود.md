---
title: >-
    شمارهٔ ۷ - صفت دلبر فصاد بود
---
# شمارهٔ ۷ - صفت دلبر فصاد بود

<div class="b" id="bn1"><div class="m1"><p>آمد آن حور و دست من بربست</p></div>
<div class="m2"><p>زدم استادوار دست به شست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نخ او به دست بگرفتم</p></div>
<div class="m2"><p>چون رگ دست من به شست بخست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت هشیار باش و آهسته</p></div>
<div class="m2"><p>دست هر جا مزن چو مردم مست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ار من به دست بگرفتم</p></div>
<div class="m2"><p>ز نخ ساده تو عذرم هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآنکه هنگام رگ زدن رسم است</p></div>
<div class="m2"><p>سیب سیمین گرفتن اندر دست</p></div></div>