---
title: >-
    شمارهٔ ۷۲ - دل دلدار چو مغناطیس است
---
# شمارهٔ ۷۲ - دل دلدار چو مغناطیس است

<div class="b" id="bn1"><div class="m1"><p>ای خجسته بر چو سیم تو را</p></div>
<div class="m2"><p>تیغ بدریده غیبه جوشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه شمشیر زد همی گه جنگ</p></div>
<div class="m2"><p>قصد زحمت نکرد گاه زدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل تو هست سنگ مغناطیس</p></div>
<div class="m2"><p>به سوی خویش می کشد آهن</p></div></div>