---
title: >-
    شمارهٔ ۳۳ - وصف دلدار و درد دیده او
---
# شمارهٔ ۳۳ - وصف دلدار و درد دیده او

<div class="b" id="bn1"><div class="m1"><p>خواهی که درد ناید بر چشمت</p></div>
<div class="m2"><p>آنجا که ناصواب بود منگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون گمان برم که ز چشم بد</p></div>
<div class="m2"><p>آسیب یافت چشم تو ای دلبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا نیست سرخ چشم تو از علت</p></div>
<div class="m2"><p>عکس رخت فتاده به چشم اندر</p></div></div>