---
title: >-
    شمارهٔ ۲۹ - صفت دلبر فقیه بود
---
# شمارهٔ ۲۹ - صفت دلبر فقیه بود

<div class="b" id="bn1"><div class="m1"><p>ز روی خواهش گفتم بدان نگار که من</p></div>
<div class="m2"><p>ز شادمانی درویشم ای بت دلبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا نصیب زکوة لبان یاقوتین</p></div>
<div class="m2"><p>بده که نیست ز من هیچ کس بدان حق تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جواب داد که من فقه خوانده ام دانم</p></div>
<div class="m2"><p>ز فقه واجب ناید زکوة بر گوهر</p></div></div>