---
title: >-
    شمارهٔ ۳۶ - صفات دلبر زرین کمرست
---
# شمارهٔ ۳۶ - صفات دلبر زرین کمرست

<div class="b" id="bn1"><div class="m1"><p>ای ماهروی لعبت جوزا کمر</p></div>
<div class="m2"><p>سیم است و زر به ماه و به جوزابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز روز لهو و نشاط است خیز</p></div>
<div class="m2"><p>پیش من آر باده و اندوه بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیرا چو مه به جوزا باشد بتا</p></div>
<div class="m2"><p>روز نشاط باشد و لهو و بطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور خوش نیایدت کمر زر همی</p></div>
<div class="m2"><p>دل ز آن مدار تنگ و غم آن مخور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کز پشت و روی اشک همی سازم</p></div>
<div class="m2"><p>به ز آنکه هست پشتش طرف کمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز بود که گرد میانت یکی</p></div>
<div class="m2"><p>آرم دو دست خود چو کمر سر به سر</p></div></div>