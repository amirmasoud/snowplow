---
title: >-
    شمارهٔ ۷۵ - وله
---
# شمارهٔ ۷۵ - وله

<div class="b" id="bn1"><div class="m1"><p>جانا ز حسن گشت رخ تو چو جان من</p></div>
<div class="m2"><p>وندر جمال خویش عیان شد گمان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جستی ز لشکری که کند لاش حسن تو</p></div>
<div class="m2"><p>رستی ز آفتی که بپوشد رخان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از انده بنفشه بتا ارغوانت رست</p></div>
<div class="m2"><p>در خار باز رست گل ارغوان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بازم رهان ز ظلمت هجران ز بهر آنک</p></div>
<div class="m2"><p>ایمن شد از تباهی ظلمت رخان تو</p></div></div>