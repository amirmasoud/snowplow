---
title: >-
    شمارهٔ ۲۳ - صفت دلبر صوفی مذهب
---
# شمارهٔ ۲۳ - صفت دلبر صوفی مذهب

<div class="b" id="bn1"><div class="m1"><p>گفتم چرا نسازی با من تو</p></div>
<div class="m2"><p>تا کی تنم ز بهر تو بگدازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا تو بت پرستی و من صوفی</p></div>
<div class="m2"><p>با بت پرست صوفی کی سازد</p></div></div>