---
title: >-
    شمارهٔ ۱۴ - در حق دلبر خباز بگفت
---
# شمارهٔ ۱۴ - در حق دلبر خباز بگفت

<div class="b" id="bn1"><div class="m1"><p>آنکه او بر دکان ز بس خوبی</p></div>
<div class="m2"><p>همچو خورشید بر سپهر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد فراز تنور چون دل من</p></div>
<div class="m2"><p>باد و مه رفت و باد و مهر آمد</p></div></div>