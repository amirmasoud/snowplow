---
title: >-
    شمارهٔ ۸۴ - صفت یار آشناگر گفت
---
# شمارهٔ ۸۴ - صفت یار آشناگر گفت

<div class="b" id="bn1"><div class="m1"><p>نگارینا نرستی ز آب و در آب</p></div>
<div class="m2"><p>سبک رفتاری و نیکو شناهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی تو ماهی سیمی و هرگز</p></div>
<div class="m2"><p>نترسد در میان آب ماهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنارم آبگیری هست و در وی</p></div>
<div class="m2"><p>توانی آشنا کرد ار بخواهی</p></div></div>