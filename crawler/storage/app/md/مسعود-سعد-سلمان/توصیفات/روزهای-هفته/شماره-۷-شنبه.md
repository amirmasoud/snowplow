---
title: >-
    شمارهٔ ۷ - شنبه
---
# شمارهٔ ۷ - شنبه

<div class="b" id="bn1"><div class="m1"><p>زحل والی شنبه است ای نگار</p></div>
<div class="m2"><p>مرا این چنین روز بی می مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زحل تیره رأیست و تاریک جرم</p></div>
<div class="m2"><p>تو خیز و می لعل روشن بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که امروز گیتی همه روشن است</p></div>
<div class="m2"><p>ز اقبال و عدل شه کامگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک ارسلان پادشاهی که او</p></div>
<div class="m2"><p>زمانه فروزست و گیتی نگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهار و خزان باد روز و شبش</p></div>
<div class="m2"><p>شبش به روز باد و خزانش بهار</p></div></div>