---
title: >-
    شمارهٔ ۱۷۹ - مدح سیف الدوله محمود و تهنیت فتح اگره
---
# شمارهٔ ۱۷۹ - مدح سیف الدوله محمود و تهنیت فتح اگره

<div class="b" id="bn1"><div class="m1"><p>دو سعادت به یکی وقت فراز آمد تنگ</p></div>
<div class="m2"><p>یکی از گردش سال و یکی از شورش جنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما از این هر دو به شکر و به ثنا قصد کنیم</p></div>
<div class="m2"><p>زانکه انده شد و شادی سوی ما کرد آهنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه نوروز دگر بار به ما روی نمود</p></div>
<div class="m2"><p>قلعه اگره درآورد ملک زاده به چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشوری بود نه قلعه همه پر مرد دلیر</p></div>
<div class="m2"><p>بر هوا بر شده و ساخته از آهن و سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی او رفته در آنجا که قرار ماهی</p></div>
<div class="m2"><p>سر او بر شده آنجا که بنات و خرچنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد او بیشه و کوه گشن و سبز چنانک</p></div>
<div class="m2"><p>گذر باد و ره مار درو ناخوش و تنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چنین قلعه محمود جهاندار گرفت</p></div>
<div class="m2"><p>به دلیری و شجاعت نه به مکر و نیرنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پشته ها کرد ز بس کشته در و پنجه جای</p></div>
<div class="m2"><p>جوی خون کرد به هر پشته روان صد فرسنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برده زنجیر به زنجیر از آن قلعه قطار</p></div>
<div class="m2"><p>همچنانست که بر روی هوا صف کلنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای امیری که برون آرد بیم و فزعت</p></div>
<div class="m2"><p>طعمه از پنجه شیر و خوره از کام نهنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باد را هیچ نباشد گه خشم تو شتاب</p></div>
<div class="m2"><p>کوه را هیچ نباشد گه حلم تو درنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای تو را فر فریدون و نهاد جمشید</p></div>
<div class="m2"><p>وی تو را سیرت کیخسرو و رای هوشنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای به صدر اندر بایسته تر از نوشروان</p></div>
<div class="m2"><p>وی به حرب اندر شایسته تر از پور پشنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چرخ گردنده با پایه او رنگ تو پست</p></div>
<div class="m2"><p>باد پوینده بر مرکب رهوار تو لنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیر پای ولی و در دو کف ناصح تو</p></div>
<div class="m2"><p>خاک چون عنبر سارا شود و بید خدنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر تن حاسد و بد خواه تو و کام عدو</p></div>
<div class="m2"><p>خز چون خار مغیلان شود و شهد شرنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زود باشد که ازین فتح خبر کرده شود</p></div>
<div class="m2"><p>به خراسان و عراق و حبش و بربر و زنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این گلی بود ز بستان فتوحت خوشبو</p></div>
<div class="m2"><p>شاخکی بود ز ریحان مرادت خوش رنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین سپس نامه فتح تو سوی حضرت شاه</p></div>
<div class="m2"><p>دم دم آید همی از معبر چین و لب گنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میل بعضی ملکا سوی نشاطست و طرب</p></div>
<div class="m2"><p>اندرین فصل و سوی خوردن بگماز چو زنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زانکه بستان شده از حسن بسان مشکوی</p></div>
<div class="m2"><p>زانکه صحرا شده از نقش بسان ارتنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرغزار و کهسار از سپر غم و خیری</p></div>
<div class="m2"><p>راست چون سینه طاوس شد و پشت پلنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اختیار تو درین وقت سوی عزم سفر</p></div>
<div class="m2"><p>از پی قوت دین و قبل حمیت و ننگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حرب کفار گزیده بدل مجلس بزم</p></div>
<div class="m2"><p>بانگ تکبیر شنوده بدل نغمه چنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا همی تازد بر مفرش دشت آهوی غرم</p></div>
<div class="m2"><p>تا همی یازد بر دامن که بچه رنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو بمان دایم وز فر تو آراسته باد</p></div>
<div class="m2"><p>تاج و تخت شهی و افسر ملک و اورنگ</p></div></div>