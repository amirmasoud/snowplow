---
title: >-
    شمارهٔ ۱۸۱ - ستایش سیف الدوله محمود
---
# شمارهٔ ۱۸۱ - ستایش سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>ولایت مه شعبان به روزه شد تحویل</p></div>
<div class="m2"><p>بدل شد این مه با آز و اینت نیک بدیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به امر باری شیطان شدست بسته به بند</p></div>
<div class="m2"><p>زبان خلق گشاده شدست بر تهلیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نار در دل کفار و نور در مسجد</p></div>
<div class="m2"><p>چو نور در دل ابرار و نار در قندیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون برآید بانگ مذکران به نشاط</p></div>
<div class="m2"><p>کنون بخیزد آواز مقربان ز رسیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجسته بادا بر شهریار سیف دول</p></div>
<div class="m2"><p>مه مبارک ماه صیام بر تفضیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدایگانی کز خسروان ببرد سبق</p></div>
<div class="m2"><p>برای و روی منور به خلق و خلق جمیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پناه شاهی محمود شاه کو دارد</p></div>
<div class="m2"><p>ز پادشاهی تخت وز خسروی اکلیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حسام او را اندر سر عدوست مقام</p></div>
<div class="m2"><p>سنان او را اندر دل حسود مقیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکسته گردن گردنکشان به گرز گران</p></div>
<div class="m2"><p>زدوده آینه ملک را به تیغ صقیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو از غلاف برآورد نیلگون صمصام</p></div>
<div class="m2"><p>زند مخالف او جامه خود اندر نیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خجسته درگه او سوی هر جلال سبب</p></div>
<div class="m2"><p>خجسته خدمت او سوی هر کمال دلیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عزیز خلق بود آنکه او کندش عزیز</p></div>
<div class="m2"><p>ذلیل دهر شود هر که او کندش ذلیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون که قصد سفر کرد رای عالی او</p></div>
<div class="m2"><p>ز شر و فتنه تهی شه همه طریق و سبیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به شیر گردد خالی ز دام و دد بیشه</p></div>
<div class="m2"><p>به سیل گردد صافی ز گرد و خاک مسیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خجسته بادا بر شاه قصد حضرت شاه</p></div>
<div class="m2"><p>دلیل باد ورا جبرئیل و میکائیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدایگانا فرخنده بادت این مه نو</p></div>
<div class="m2"><p>ز کردگارت بادا جزا ثواب جزیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه بادی از هر چه آرزوست به کام</p></div>
<div class="m2"><p>همیشه بادی از هر مراد با تحصیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مخالفانت گرفتار این چهار بلا</p></div>
<div class="m2"><p>که داد خواهم هر یک جدا جدا تفصیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی به تیغ گران و یکی به تیر سبک</p></div>
<div class="m2"><p>یکی به پنجه شیر و یکی به خرطم پیل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیشه باد تو را خسروی به ملک ضمان</p></div>
<div class="m2"><p>همیشه باد تو را مملکت به تخت کفیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جلالت ابدی با تو چون شجاعت جفت</p></div>
<div class="m2"><p>سعادت ازلی با تو چون سخات عدیل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>غلام گشته جهان پیش تو صغار و کبار</p></div>
<div class="m2"><p>نصیبت آمده از مملکت کثیر و قلیل</p></div></div>