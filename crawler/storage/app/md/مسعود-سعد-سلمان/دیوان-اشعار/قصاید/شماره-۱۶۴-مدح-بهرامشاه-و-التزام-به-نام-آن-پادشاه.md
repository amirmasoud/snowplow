---
title: >-
    شمارهٔ ۱۶۴ - مدح بهرامشاه و التزام به نام آن پادشاه
---
# شمارهٔ ۱۶۴ - مدح بهرامشاه و التزام به نام آن پادشاه

<div class="b" id="bn1"><div class="m1"><p>تا برآمد ز آتش شمشیر بهرامی شرار</p></div>
<div class="m2"><p>داد گیتی را فلک بر ملک بهرامی قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد بهرام افتخار از ملک شه بهرام شاه</p></div>
<div class="m2"><p>در همه معنی که برتر دیده از این افتخار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت ملک و عدل او آباد تا ملکست و عدل</p></div>
<div class="m2"><p>ملک بهرامی لباس و عدل بهرامی نگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش بهرام زمین بهرام گردون بنده شد</p></div>
<div class="m2"><p>در زمانه بندگی ملک او کرد افتخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر فلک بهرام گوید دولت بهرام شاه</p></div>
<div class="m2"><p>هر چه مقصودست گیتی را نهاد اندر کنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآسمان روح الامین گویان به صد شادی که هست</p></div>
<div class="m2"><p>با ملک بهرام شه بهرام گردون جانسپار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوخت شمشیر و جان بدسگالان روز رزم</p></div>
<div class="m2"><p>زانکه ببرامست شمشیر تو را آموزگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برتر آمد مرتبه بهرام را از مهر و ماه</p></div>
<div class="m2"><p>تا ز نامی نام تو اندر جهان شد نامدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در همه معنی چو احمد بود بهرامی مضا</p></div>
<div class="m2"><p>از پی صدر وزارت کرد او را اختیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کف کافی او زان خامه بهرام سیر</p></div>
<div class="m2"><p>سعد و نحس دوستان و دشمنان شد آشکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این وزارت را که بهرامی است تیغ طبع او</p></div>
<div class="m2"><p>از نشاط خدمت تو گشت خرم روزگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به عون ملک و دین باشند پیش تخت تو</p></div>
<div class="m2"><p>همچو بهرام از مضا هنگام رأی و وقت کار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راویا تو مدح های ملک بهرامی بخوان</p></div>
<div class="m2"><p>ساقیا تو جام های بزم بهرامی بیار</p></div></div>