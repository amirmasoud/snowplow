---
title: >-
    شمارهٔ ۳۰۲ - مدح یکی از آل شیبان
---
# شمارهٔ ۳۰۲ - مدح یکی از آل شیبان

<div class="b" id="bn1"><div class="m1"><p>ای خداوند عید روزه گشای</p></div>
<div class="m2"><p>بر تو فرخنده شد چو فر همای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژده ها داردت ز نصرت و فتح</p></div>
<div class="m2"><p>شاد باش و به عز و ناز گرای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بر اطراف مملکت برده</p></div>
<div class="m2"><p>پاسبان خنجر عدو پیرای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گه جود حاتمی تو به حق</p></div>
<div class="m2"><p>به گه جنگ رستمی تو به جای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون درآید دو فوج رویاروی</p></div>
<div class="m2"><p>چون برآید به حمله ها یاهای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ با رخش تو ندارد تاب</p></div>
<div class="m2"><p>کوه با زخم تو ندارد پای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سخا کار راد بزم افروز</p></div>
<div class="m2"><p>وی هما پیشه گرد رزم آرای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بده انصاف آنچه می بینی</p></div>
<div class="m2"><p>من بگفتم تو را به قلعه نای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواندمت شعرهای طبع آویز</p></div>
<div class="m2"><p>گفتمت مدحهای گوش سرای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مژده ها دادمت به قوت دل</p></div>
<div class="m2"><p>وعده ها کردمت به صحت رای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فال هایی که من زدم دیدی</p></div>
<div class="m2"><p>که چگونه تمام کرد خدای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنچه کردست وانچه خواهد کرد</p></div>
<div class="m2"><p>ده یکی نیست یک دو ماه بپای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا نبینی که بخت روز افزون</p></div>
<div class="m2"><p>چه طرازد ز جاه گردون سای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هم بدین حشمت زمانه نورد</p></div>
<div class="m2"><p>هم بدین همت فلک پیمای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هم بدین تیغ های آتشبار</p></div>
<div class="m2"><p>هم بدین سرکشان آهن خای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رتبت بو حلیمیان برکش</p></div>
<div class="m2"><p>افتخار زریریان بفزای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دولتی را ز بن دگر پی نه</p></div>
<div class="m2"><p>عالمی را دگر ز سر بگشای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به حسام ز دوده روشن</p></div>
<div class="m2"><p>تیره زنگار شرک را بزدای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خانه گمرهی به آتش ده</p></div>
<div class="m2"><p>چهره کافری به خون اندای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طاغیان را به یک زمان افکند</p></div>
<div class="m2"><p>ناله کوس تو به ناله وای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو بدین بیرهان غره شده</p></div>
<div class="m2"><p>اثر فتح ایزدی بنمای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون قلم پیشت ار بسر بروند</p></div>
<div class="m2"><p>سرشان چون قلم ز تن بربای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مغزهاشان چو مغز مار بکوب</p></div>
<div class="m2"><p>نیز افسایشان چو مار افسای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تیغ زهر آبداده پازهرست</p></div>
<div class="m2"><p>بگزایدت زهر زود گزای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فال گیر این ستایشی کآرد</p></div>
<div class="m2"><p>بر تو سید ملوک ستای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>رو که نصرت تراست یاری گر</p></div>
<div class="m2"><p>رو که ایزد تراست راهنمای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با مراد همه جهان بخرام</p></div>
<div class="m2"><p>با فتوح همه جهان بازآی</p></div></div>