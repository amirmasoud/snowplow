---
title: >-
    شمارهٔ ۲۰۶ - هم در ستایش او
---
# شمارهٔ ۲۰۶ - هم در ستایش او

<div class="b" id="bn1"><div class="m1"><p>گر یک وفا کنی صنما صد وفا کنم</p></div>
<div class="m2"><p>ور تو جفا کنی همه من کی جفا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو نرد عشق بازی و با من دغا کنی</p></div>
<div class="m2"><p>من جان ببازم و نه همانا دغا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آب دیده تیره کند دیده مرا</p></div>
<div class="m2"><p>این دیده را ز خاک درت توتیا کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل عارضی و لاله رخی ای نگار من</p></div>
<div class="m2"><p>در مرغزار آن گل و لاله چرا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار و گیا چو دایه لاله ست و اصل گل</p></div>
<div class="m2"><p>از بهر هر دو خدمت آب و گیا کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و دل منی و دل و جان دریغ نیست</p></div>
<div class="m2"><p>گر من تو را که هم دل و جانی عطا کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بر کنم دل از تو بردارم از تو مهر</p></div>
<div class="m2"><p>آن مهر بر که افکنم آن دل کجا کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان بیم کاشنایی و بیگانگی کنی</p></div>
<div class="m2"><p>دل را همیشه با همه رنج آشنا کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای چون هوا لطیف ز رنج هوای تو</p></div>
<div class="m2"><p>شبها دو دست خویش همی بر هوا کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این هر چه بر تنست همه دل کند همی</p></div>
<div class="m2"><p>کی راست باشد اینکه گله از هوا کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جور و جفا مکن که ز جور و جفای تو</p></div>
<div class="m2"><p>باشد که بر تو از دل خسته دعا کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با تو به بد دعا نکنم گر تو بد کنی</p></div>
<div class="m2"><p>در رنج و درد گر کنم ای بت خطا کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر هیچ چاره کرد ندانم غم تو را</p></div>
<div class="m2"><p>این دل که آفتست پس تو رها کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرگز جدایی از تو نجویم که تو مرا</p></div>
<div class="m2"><p>جانی ز جان خویش جدایی چرا کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جانم ز تن جدا باد ار من به هیچ وقت</p></div>
<div class="m2"><p>یک لحظه جان ز مهر تو ای جان جدا کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر شب که مه برآید من ز آرزوی تو</p></div>
<div class="m2"><p>تا وقت صبح روی به ماه سما کنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر ناله و گریستن زار زار خویش</p></div>
<div class="m2"><p>ای ماه و زهره زهره و مه را گوا کنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وصفت نمی کنم به زبانی که هم بدان</p></div>
<div class="m2"><p>بر شاه شرق و غرب همیدون ثنا کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مسعود پادشاهی کز چرخ قدر من</p></div>
<div class="m2"><p>برتر شود که مدح چنین پادشا کنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوید همی حسامش نصرت روان شود</p></div>
<div class="m2"><p>اندر وغا که روی به سوی وغا کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روی مرا ندید و نبیند عدوی تو</p></div>
<div class="m2"><p>زیرا به رزم روی عدو را قفا کنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بأسش همی چگوید من وقت کار زار</p></div>
<div class="m2"><p>نیزه به دست شاه جهان اژدها کنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وانگاه نیزه گوید من سحرهای کفر</p></div>
<div class="m2"><p>همچون عصای موسی عمران هبا کنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اقبال شاه گوید من کیمیاگرم</p></div>
<div class="m2"><p>کز خاک و گل به دولت او کیمیا کنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گوید همی طبیعت در دهر خلق را</p></div>
<div class="m2"><p>از عدل شاه مایه نشو و نم کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر روز بامدادان از عفو و خشم او</p></div>
<div class="m2"><p>مر خلق را دو صورت خوف و رجا کنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوید همی زمانه که از کین و مهر شاه</p></div>
<div class="m2"><p>در عالم اصل شدت و عین رخا کنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گوید جهان که روز نبیند عدوی شاه</p></div>
<div class="m2"><p>زیرا که هر صباح که بیند مسا کنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چونان که شب نبیند هرگز ولی او</p></div>
<div class="m2"><p>زیرا که ظلمتی که ببینم ضیا کنم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گوید همی جلالت کعبه ست قصر شاه</p></div>
<div class="m2"><p>هر حاجتم که باشد در وی روا کنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بوسم همیشه گوید تخت مبارکش</p></div>
<div class="m2"><p>زان تخت گاه مروه کنم گه صفا کنم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بیتی که گفته بودم تضمین کنم همی</p></div>
<div class="m2"><p>چون هست گفته من بگذار تا کنم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من ناشنیده گویم از خویشتن چو ابر</p></div>
<div class="m2"><p>چون کوه نه که هر چه شنیدم صدا کنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اقبال شاه چون ز علا و سنا شدست</p></div>
<div class="m2"><p>من جمله آفرین علا و سنا کنم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آراسته ست دولت و ملت به این و آن</p></div>
<div class="m2"><p>پس آفرین هر دو به حق و سزا کنم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون من برشته کردم یاقوت مدح شاه</p></div>
<div class="m2"><p>یاقوت را به ارز کم از کهربا کنم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دانش به من مفوض کردست کار نظم</p></div>
<div class="m2"><p>زان نوع هر چه خواهد از من وفا کنم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون کرد کدخدایی آن را به رسم من</p></div>
<div class="m2"><p>یا کرده ام چنانکه ببایست یا کنم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر هیچ گونه درگذرد مدحتی ز وقت</p></div>
<div class="m2"><p>ناچار چون نماز فریضه قضا کنم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من شرح مدح شاه دهم در سخن همی</p></div>
<div class="m2"><p>نه کار کرد خویش همی بر هبا کنم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دولت حقوق من به تمامی ادا کند</p></div>
<div class="m2"><p>هرگه که پیش شاه مدیحی ادا کنم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>انعام شاه را که مرا داد خانمان</p></div>
<div class="m2"><p>بسیار شد به شکر چگونه جزا کنم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر روز من ثنا کنمش بر ملا به نظم</p></div>
<div class="m2"><p>در شب همی به نثر دعا در خلا کنم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در باغ وصف شاه چو بلبل زنم نوا</p></div>
<div class="m2"><p>دلهای خلق بسته آن خوش نوا کنم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وانگه چو گوییم که توانی سزای شاه</p></div>
<div class="m2"><p>پرداخت یک مدیح جواب تولا کنم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گوید ملک مرا که عنایت به باب تو</p></div>
<div class="m2"><p>چندان کنم که جان عدو با عنا کنم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون تو رضای شاه بجویی به مدح نیک</p></div>
<div class="m2"><p>من سوی تو نگاه به چشم رضا کنم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شاها زمانه گوید من مقتدی شدم</p></div>
<div class="m2"><p>در بیش و کم به دولت تو اقتدا کنم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گوید همی قضا که من اندر جهان ملک</p></div>
<div class="m2"><p>حکم بقای شاه خلود و بقا کنم</p></div></div>