---
title: >-
    شمارهٔ ۲۰۱ - گله از خلف وعده خواجه بوطاهر
---
# شمارهٔ ۲۰۱ - گله از خلف وعده خواجه بوطاهر

<div class="b" id="bn1"><div class="m1"><p>من که مسعود سعد سلمانم</p></div>
<div class="m2"><p>زانچه گفتم همه پشیمانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زانکه خواجه مرا خداوندست</p></div>
<div class="m2"><p>خویشتن را غلام او دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به همه وقت شکر او گویم</p></div>
<div class="m2"><p>به همه جای مدح او خوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر ثنائی که گفتم او را من</p></div>
<div class="m2"><p>سجلست او به صدر دیوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست معلوم او که در خدمت</p></div>
<div class="m2"><p>من ز کس هیچ مزد نستانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواستم شغلکی که شغلی هست</p></div>
<div class="m2"><p>هست از آنسان که من همی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی آن شغل را به قوت این</p></div>
<div class="m2"><p>ز سر امروز تازه گردانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بگفتندش اهتزاز نمود</p></div>
<div class="m2"><p>نیکویی گفت بس فراوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه کس بگفتم این قصه</p></div>
<div class="m2"><p>که من از نایبان دیوانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کردم از همت و مروت او</p></div>
<div class="m2"><p>شکرهایی چنانکه من دانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواستم تا قباله بنویسم</p></div>
<div class="m2"><p>نایبی را به شغل بنشانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون به منشور نامه آمد کار</p></div>
<div class="m2"><p>رفت چیزی که گفت نتوانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم آخر که بیش صبر نماند</p></div>
<div class="m2"><p>در دل این غصه را بپیچانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تیز در ریش و کفل درگه شد</p></div>
<div class="m2"><p>خنده ها رفت بر بروتانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرد شد گرم گشته امیدم</p></div>
<div class="m2"><p>کند شد تیز گشته دندانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه کنم قصه زرد شد رویم</p></div>
<div class="m2"><p>چه دهم شرح رنجه شد جانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خجل و تیره ام ز دشمن و دوست</p></div>
<div class="m2"><p>نیک رنجور و سخت حیرانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون ز مهتر آمد اجنبیی</p></div>
<div class="m2"><p>خیره اکنون زنخ چه جنبانم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خواجه طاهر تو طبع من دانی</p></div>
<div class="m2"><p>که نه جنس فلان و بهمانم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر کریمی مرا به جان بخرد</p></div>
<div class="m2"><p>تو چنان دان که من بس ارزانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر چه هستم چو لاله سوخته دل</p></div>
<div class="m2"><p>چون گل نوشکفته خندانم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کار کن تر بسی ز خایسکم</p></div>
<div class="m2"><p>رنج بردارتر ز سندانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خسته زخم های گردونم</p></div>
<div class="m2"><p>بسته حملهای کیوانم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر من آن گفت بس اثر نکند</p></div>
<div class="m2"><p>که به تن آشنای حرمانم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در غم چیز دل نیاویزم</p></div>
<div class="m2"><p>به دم حرص تن نرنجانم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تن سپرده به حکم دادارم</p></div>
<div class="m2"><p>دل نهاده به فضل یزدانم</p></div></div>