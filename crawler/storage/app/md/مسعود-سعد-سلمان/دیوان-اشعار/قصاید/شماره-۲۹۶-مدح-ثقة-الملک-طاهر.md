---
title: >-
    شمارهٔ ۲۹۶ - مدح ثقة الملک طاهر
---
# شمارهٔ ۲۹۶ - مدح ثقة الملک طاهر

<div class="b" id="bn1"><div class="m1"><p>در کف دو زبانیست مرا بسته دهانی</p></div>
<div class="m2"><p>گوید چو فصیحان صفت بیت زمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کودک عمری که بود کوژ چو پیری</p></div>
<div class="m2"><p>و آواز برآورده چو آواز جوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترکیب بدیعش ز جماد و حیوانست</p></div>
<div class="m2"><p>شخصش ز جمادی و زبان از حیوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زرین را نیست ازو ساخته کفی</p></div>
<div class="m2"><p>تکیه زده بر ران و کف سیمین رانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان را ز همه شادی دادست نصیبی</p></div>
<div class="m2"><p>دل را ز همه رامش کردست ضمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزم خداوند سراید غزل و مدح</p></div>
<div class="m2"><p>صد گونه سخن گوید بی هیچ زبانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طاهر ثقت الملک سهری که ز رأیش</p></div>
<div class="m2"><p>در ملک بیفزاید هر روز جهانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید که هر روز سر از ملک برآرد</p></div>
<div class="m2"><p>گوید به بیانی که چنان نیست بیانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه چون ثقت الملک بود ملک فروزی</p></div>
<div class="m2"><p>نه نیز چو مسعود ملک ملک ستانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای جسم تو جانی که سرشتست ز نوری</p></div>
<div class="m2"><p>هرگز نبود پاکتر از جسم تو جانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در طبع تو از چرخ نگشتست هراسی</p></div>
<div class="m2"><p>بر عقل تو از دهر نمانده ست نهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>افروخته رای تو همی ملک فروزد</p></div>
<div class="m2"><p>ای رای تو تیغی که چنان نیست فسانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حزمت چو بیارامد و عزمت چو بجنبد</p></div>
<div class="m2"><p>آن کوه رکابی بود این باد عنانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اقبال تو و هیبت تو نوری و ناری</p></div>
<div class="m2"><p>مهر تو و کین تو بهاری و دخانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از خامه تو ملک به خوبی و به نغزی</p></div>
<div class="m2"><p>چون لعبت آذر شد و چون صورت مانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگز نکشد پی به گمان تو یقینی</p></div>
<div class="m2"><p>هرگز نبرد پی به یقین تو گمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کام تو به هر وقتی آراسته بزمی</p></div>
<div class="m2"><p>جود تو به هر وقتی پرداخته کانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مال تو خریدار ثنا گشته و هر روز</p></div>
<div class="m2"><p>داری ز ثنا سودی و از مال زیانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای رای تو آن سخت کمانی که ندیدست</p></div>
<div class="m2"><p>این سخت کمان چرخ چو او سخت کمانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این طالع بختم سرطانست همیشه</p></div>
<div class="m2"><p>زان کج رود این بخت بدم چون سرطانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امروز خداوندا در حبس تنم را</p></div>
<div class="m2"><p>جان در غلیانست و تن اندر خفقانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون مردم بیمار که در بحران باشد</p></div>
<div class="m2"><p>پیوسته همی گویم با خود هذیانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر گویم و گر نه غم دل در دل چون نار</p></div>
<div class="m2"><p>می بترکد این دل اگر گویم یانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از رنج روانم را رفته همه قوت</p></div>
<div class="m2"><p>زیرا که تنی دارم چون رفته روانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیوسته درین حبس گرفتارم و مأخوذ</p></div>
<div class="m2"><p>هر روز به جلویزی و هر شب به عوانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا دوزخی نبود درمانده نگردد</p></div>
<div class="m2"><p>در دست چنین دوزخی زندان بانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من بسته بدخواهم غبنا که بدینسان</p></div>
<div class="m2"><p>گردد چو منی بسته تلبیس چنانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این هست همه سهل جز این نیست که امروز</p></div>
<div class="m2"><p>در دل زندم دوری روی تو سنانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جانم که بترسیده ست از چرخ ستمگر</p></div>
<div class="m2"><p>از رای کریم تو همی خواهد امانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ور من بمرم فضل فرو گرید و گوید</p></div>
<div class="m2"><p>والله که ازین پس بنبینیم چو فلانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دردا و دریغا که شود ضایع و باطل</p></div>
<div class="m2"><p>زین نوع بنانی و ازین جنس بیانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه نه که به حسن نظر دولت سامیت</p></div>
<div class="m2"><p>آخر بکنم روزی با بخت قرانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>امروز من از رای بلند تو بدیدم</p></div>
<div class="m2"><p>از دولت و اقبال دلیلی و نشانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>والله که بخواهم دید ارزنده بمانم</p></div>
<div class="m2"><p>بر تن ز تو تشریفی و بر سر برکانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوش چیز از آنست سبک خیزی تازی</p></div>
<div class="m2"><p>از ساز به زریال و به رخشش چو گرانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وین حال عیانست مرا ز آنکه بر عقل</p></div>
<div class="m2"><p>احوال جهان نیست نهانی چو عیانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا هیچ تهی نیست مکانی ز مکینی</p></div>
<div class="m2"><p>چونان که جدا نیست مکینی ز مکانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یک لحظه و یک ساعت قصر تو مبادا</p></div>
<div class="m2"><p>بی صدری و دیوانی بی بزمی و خوانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سر سبزتر از مورد و فزاینده تر از سرو</p></div>
<div class="m2"><p>دلشاد ز هر سرو قدی مورد نشانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون لاله شده جام تو از باده و گشته</p></div>
<div class="m2"><p>از روی بتان بزم تو چون لاله ستانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>می خواسته از غالیه خطی که دهانش</p></div>
<div class="m2"><p>باشد چو درآید به سخن غالیه دانی</p></div></div>