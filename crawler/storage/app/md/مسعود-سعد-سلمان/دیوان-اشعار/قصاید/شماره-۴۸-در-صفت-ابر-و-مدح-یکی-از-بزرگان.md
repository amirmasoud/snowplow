---
title: >-
    شمارهٔ ۴۸ - در صفت ابر و مدح یکی از بزرگان
---
# شمارهٔ ۴۸ - در صفت ابر و مدح یکی از بزرگان

<div class="b" id="bn1"><div class="m1"><p>زهی هوا را طواف و چرخ را مساح</p></div>
<div class="m2"><p>که جسم تو ز بخارست و پرتو ز ریاح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر به صورت و ترکیب هستی از اجسام</p></div>
<div class="m2"><p>چرا به بالا تازی ز پست چون ارواح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دوستی که تو داری همی پریدن را</p></div>
<div class="m2"><p>به حرص و طبع همه تن ترا شدست جناح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو کشتی یی که ز رعد و ز برق و باد تو را</p></div>
<div class="m2"><p>چو بنگریم شراع است و لنگر و ملاح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تویی که لشکر بحر و سپاه جیحونی</p></div>
<div class="m2"><p>ز برق و رعدت کوس و علم به قلب و جناح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی ز گریه تو زرد دیده نرگس</p></div>
<div class="m2"><p>گهی ز خنده تو سرخ چهره تفاح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو چشم عاشق داری به اشک روی هوا</p></div>
<div class="m2"><p>چو روی دلبر داری به نقش روی بطاح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توراست اکنون بر کوه پیچش تنین</p></div>
<div class="m2"><p>چنانکه بودت در بحر سازش تمساح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه در بحار قرارت نه در جبال سکون</p></div>
<div class="m2"><p>نه تیز رحلت پیکی چو زود رو سیاح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر این بلندی جز مر تو را اجازت نیست</p></div>
<div class="m2"><p>که باری آید نزدیک این غداة و رواح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هنر سوار بزرگی است که دست جاهش کرد</p></div>
<div class="m2"><p>به تازیانه حشمت زمانه را اصلاح</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ربود و برد کف را دو رای عالی او</p></div>
<div class="m2"><p>ز جور و طبع جهان و فلک حرون و جماح</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه قعر حلمش دریافت فکرت غواص</p></div>
<div class="m2"><p>نه غور حزمش بنمود نهمت مساح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزرگ بار خدایا تو ملک و دولت را</p></div>
<div class="m2"><p>چو عقل مایه عونی چو بخت اصل نجاح</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه وقار و گه جود دست و طبع توراست</p></div>
<div class="m2"><p>ثبات تند جبال و مضاء تیز ریاح</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز رای و عزم تو گردون و دهر از آن ترسد</p></div>
<div class="m2"><p>که این کشیده سیوفست و آن زدوده رماح</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر همیدون بحر مکارمی نه عجب</p></div>
<div class="m2"><p>که خط های کف تست جویهای سماح</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به روزگار تو شادم اگر چه محرومم</p></div>
<div class="m2"><p>از آن بزرگی طنان و طلعت وضاح</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپیدرویم چون روز تا به مدحت تو</p></div>
<div class="m2"><p>سیاه کردم چون شب دفاتر و الواح</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به طبع و خاطرم اندر مدیح و وصف تو را</p></div>
<div class="m2"><p>گشاد و بست کمال و هنر نقاب و وشاح</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ثنا و شکر تو گویم همی به جان و به دل</p></div>
<div class="m2"><p>که نیست شکر و ثنا جز تو را حلال و مباح</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو تا چو خورشید از چشم من جدا شده ای</p></div>
<div class="m2"><p>همی سیاه مسا گرددم سپید صباح</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو روز بود مرا آفتاب من بودی</p></div>
<div class="m2"><p>چو شب درآید دائم تو باشیم مصباح</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز سعی و فضل تو داروی و مرهمم باید</p></div>
<div class="m2"><p>که تن رهین سقام است و دل اسیر جراح</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چگونه بسته شوم هر زمان به بند گران</p></div>
<div class="m2"><p>که هست رأی تو قفل زمانه را مفتاح</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لزمت سجنا و الباب مغلق دونی</p></div>
<div class="m2"><p>ولیس یفتح دون المهیمن الفتاح</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا تو دانی و دانی که هیچ وقت نبود</p></div>
<div class="m2"><p>دردنائت را خود بر دل من استفتاح</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تفاوت است میان من و عدو چونانک</p></div>
<div class="m2"><p>تفاوت است به اقسام در میان قداح</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر چه هر دو به آواز و بانک معروفند</p></div>
<div class="m2"><p>زئیر شیر شناسد مردمان زنباح</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو را به محنت مسعود سعد عمر گذشت</p></div>
<div class="m2"><p>بدار ماتم دولت که نیست جای مزاح</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فلک به حرب تو آنگه دلیر شد که تو را</p></div>
<div class="m2"><p>نیافت پای مجال و نداشت دست صلاح</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز عقل ساز حسام و ز دست ساز سپر</p></div>
<div class="m2"><p>که با زمانه و چرخی تو در جدال و نطاح</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برو چو طوطی و بلبل به قول و لحن مباش</p></div>
<div class="m2"><p>که دام های بلا را قوی شود ملواح</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز پیش خویش بینداز عمدة الکتاب</p></div>
<div class="m2"><p>به دست خویش فرو شو مسائل ایضاح</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همی گذار جهان را به کل محترفه</p></div>
<div class="m2"><p>ستور وار همی زی ولا علیک جناح</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه تا بود افلاک مرکز انجم</p></div>
<div class="m2"><p>همیشه تا بود ارواح قوت اشباح</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تن عدوی تو با ناله باد چون تن زیر</p></div>
<div class="m2"><p>لب ولی تو پر خنده چون لب اقداح</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تنت چو طبعت صافی و طبع چون تن راست</p></div>
<div class="m2"><p>دلت ز جانت مسرور و جان ز دل مرتاح</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به چشمت اندر حسن و به طبعت اندر لهو</p></div>
<div class="m2"><p>به گوشت اندر لحن و به دستت اندر راح</p></div></div>