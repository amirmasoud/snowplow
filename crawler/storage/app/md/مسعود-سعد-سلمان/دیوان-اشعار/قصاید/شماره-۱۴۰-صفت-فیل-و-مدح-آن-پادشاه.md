---
title: >-
    شمارهٔ ۱۴۰ - صفت فیل و مدح آن پادشاه
---
# شمارهٔ ۱۴۰ - صفت فیل و مدح آن پادشاه

<div class="b" id="bn1"><div class="m1"><p>همی گذشت به میدان شاه کشور</p></div>
<div class="m2"><p>عظیم شخصی قلعه ستان و صفدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسان گردون رفتار و رنگ و فعلش</p></div>
<div class="m2"><p>چو ماه بر روی آئینه منور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو چرخ و عقدش تابان بسان انجم</p></div>
<div class="m2"><p>چو ابر و برقش غران به جای تندر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه باد لیکن در جنگ باد صولت</p></div>
<div class="m2"><p>نه کوه لیکن در حمله کوه پیکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسان مرکز بر مرکز معلق</p></div>
<div class="m2"><p>به زیر گنبد چون گنبد مدور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پای گرد برآرد ز کوه بابل</p></div>
<div class="m2"><p>به یشک خاک برآرد ز حصن خیبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گاه رفتن ماننده سماری</p></div>
<div class="m2"><p>چهار پایش مانند چار لنگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه دویدن مانند اسب تازی</p></div>
<div class="m2"><p>رونده اسبی از نیکویی مصور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمین نوردی زین خنگ زیور اسبی</p></div>
<div class="m2"><p>که هست زیور اسبان خنگ زیور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرین و گردن و پشت و برش مسمن</p></div>
<div class="m2"><p>میان گرده و پای و رخش مضمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گاه جستن مانند برق لامع</p></div>
<div class="m2"><p>گه دویدن مانند باد صرصر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شکل چنبر ناوردگاه سازد</p></div>
<div class="m2"><p>وگر بخواهی بیرون جهد ز چنبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو چرخ محور گردد به گاه جولان</p></div>
<div class="m2"><p>چنانکه گردد زو خیره چرخ محور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه از مؤخر پیدا ورا مقدم</p></div>
<div class="m2"><p>نه از مقدم پیدا ورا مؤخر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زوهم پیش شود او گه دویدن</p></div>
<div class="m2"><p>اگر کنندش با وهم هیچ همبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان دود چو دوانی برابر او را</p></div>
<div class="m2"><p>که پای بیرون بنهد ز خط مسطر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز هیچ چیز نترسد بسان نیزه</p></div>
<div class="m2"><p>ز هیچ باک ندارد بسان خنجر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چگونه خنجری آن خنجری که وصفش</p></div>
<div class="m2"><p>همی نگنجد کس را به خاطر اندر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهر صورت تیغی که از صحیفه ش</p></div>
<div class="m2"><p>به جای زهره و تیر و نجوم دو پیکر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هزار کوکب مریخ گشته پیدا</p></div>
<div class="m2"><p>که حکمشان همه نحسست بر عدو بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو وهم لابد اندر شود به هر دل</p></div>
<div class="m2"><p>چو عقل ناچار اندر شود به هر سر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز گونه گونه عرضهاست پر جواهر</p></div>
<div class="m2"><p>ولی جواهر او را عرض چو جوهر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چنین شنیدم از مردمان دانا</p></div>
<div class="m2"><p>که می بسنبد الماس گوهرآور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دروست گوهر و الماس طبع تیغش</p></div>
<div class="m2"><p>چرا نسنبد الماس وار گوهر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو چرخ و نورش مانند نور کوکب</p></div>
<div class="m2"><p>چو آب و فعلش مانند فعل آذر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز نور او شده روز حسود مظلم</p></div>
<div class="m2"><p>ز صفوتش شده عیش عدو مکدر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو وصل شاه جهان یافت او ز شادی</p></div>
<div class="m2"><p>عروس وار بیاراست تن به زیور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو نوعروسان زین روی دایم اکنون</p></div>
<div class="m2"><p>گهی لباسش احمر بود گه اخضر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر آن تنی که بدین تیغ گشت بی جان</p></div>
<div class="m2"><p>نباشد او را هول نکیر و منکر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غذای او همه مغز عدوی بی دین</p></div>
<div class="m2"><p>لباس او همه از خون مرد کافر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو آتشست و بسوزد دل مخالف</p></div>
<div class="m2"><p>وز آب گردد افزون فروغ اخگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر آنکه روزی در دهر گشت کشته</p></div>
<div class="m2"><p>ازو طلب کند او جان به روز محشر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر نداری باور همی حدیثم</p></div>
<div class="m2"><p>ازو بری به گه کارزار کیفر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همیشه باشد ازو مملکت به رونق</p></div>
<div class="m2"><p>چو کلک باشد با او همیشه یاور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چگونه کلکی کلکی کزو بزاید</p></div>
<div class="m2"><p>هزار معنی چون زاید ز مادر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو یار دلبر معشوق و سرو قامت</p></div>
<div class="m2"><p>چو مرد بیدل گریان و زرد و لاغر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو کار گیتی بسته گره ز گیتی</p></div>
<div class="m2"><p>چو رنگ خورشید رنگش ز تابش خور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسان ماه و چو پیدا شد از سپهرش</p></div>
<div class="m2"><p>به نور معنی گردد سپهرش انور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو از سپهر فرو شد چو ماه روشن</p></div>
<div class="m2"><p>شود سپهرش تاری و تیره یک سر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به رنگ زر شده بیماروار و او را</p></div>
<div class="m2"><p>ز مشک بالین و ز سیم ناب بستر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر ز بالین تیره شود سر او را</p></div>
<div class="m2"><p>ولیک تنش به بستر همه منور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بیم آنکه سر او چو تنش گردد</p></div>
<div class="m2"><p>همی خضاب کند سر به مشک اذفر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسان مستان از ره رود به یک سو</p></div>
<div class="m2"><p>ز باده گویی خورده ست یک دو ساغر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از آنکه در خم مانند رنگ و بویش</p></div>
<div class="m2"><p>به رنگ لعل بدخشی و بوی عنبر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به جامی از وی گردد غمی نشاطی</p></div>
<div class="m2"><p>به جرعه از وی گردد جبان دلاور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به جام زرین همچون گل موجه</p></div>
<div class="m2"><p>درونش احمر باشد برونش اصفر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گهی چو مرد معمر ولیکن از او</p></div>
<div class="m2"><p>شود به طبع جوان مردم معمر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>معین من به گه مدح شاه عالم</p></div>
<div class="m2"><p>که هست بر همه شاهان دهر سرور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>امیر غازی محمود سیف دولت</p></div>
<div class="m2"><p>خدایگان جهان شاه دادگستر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>شهی که دارد ظاهر چو پاک باطن</p></div>
<div class="m2"><p>شهی که دارد مخبر چو خوب منظر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مراد او را گشته قضا متابع</p></div>
<div class="m2"><p>هوای او را گشته قدر مسخر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زمین ز پایه تختش فزود رتبت</p></div>
<div class="m2"><p>فلک ز عالی قدرش گرفت مفخر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شده ز سهمش تاری هزار خانه</p></div>
<div class="m2"><p>شده ز نامش روشن هزار منبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سپید گشته به مدحش هزار خاطر</p></div>
<div class="m2"><p>سیاه گشته ز شکرش هزار دفتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>به گاه بخشش مانند حاتم طی</p></div>
<div class="m2"><p>به گاه کوشش مانند رستم زر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نه با سنانش جوشن بود چو جوشن</p></div>
<div class="m2"><p>نه با حسامش مغفر بود چو مغفر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به خواب دید غضنفر حسام او زآن</p></div>
<div class="m2"><p>ز تب نباشد خالی تن غضنفر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز بس که شاهان بوسند فرش او را</p></div>
<div class="m2"><p>شدست فرشش ز آثار لب مجدر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به پیش خاطر او آفتاب تاری</p></div>
<div class="m2"><p>به نزد همت او آسمان محقر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شها ز عدل تو چونان شدست گیتی</p></div>
<div class="m2"><p>که باز جفت شد از بیم با کبوتر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شده نگون ز نهیب تو تاج کسری</p></div>
<div class="m2"><p>شده خراب ز بیم تو قصر قیصر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>منور است به رأی تو هفت گردون</p></div>
<div class="m2"><p>مزین است به روی تو هفت کشور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فراخته ست برای تو چتر و رایت</p></div>
<div class="m2"><p>فروخته ست به فر تو تخت و افسر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ز نور روی تو عالم شدست روشن</p></div>
<div class="m2"><p>ز بوی خلق تو گیتی شده معطر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همی سعود بود حکم نجم زهره</p></div>
<div class="m2"><p>چو گشت رای تو شاها برو مجاور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بلند گردون با همتت زمین است</p></div>
<div class="m2"><p>بزرگ دریا با فک تست فرغر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز ذوالفقار تو آن دیده اند شاهان</p></div>
<div class="m2"><p>که خلق دیدند از ذوالفقار حیدر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به نزد خلق ظفر زآن ستوده باشد</p></div>
<div class="m2"><p>که مر حسام و سنان تراست رهبر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر چه شعر رهی نیست شهریارا</p></div>
<div class="m2"><p>به لفظ و معنی با شعرها برابر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز دق مسلم باشد ز عیب خالی</p></div>
<div class="m2"><p>نباشد از سخن هیچ کس مزور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو بنده پیش تو مدحت کند روایت</p></div>
<div class="m2"><p>دهان بنده به مدحت شود معنبر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر آن مدیح که خالی بود ز نامت</p></div>
<div class="m2"><p>بودش معنی منحول و لفظ ابتر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سخن به مدح تو نازد خدایگانا</p></div>
<div class="m2"><p>چنانکه اخبار از هاشمی پیمبر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نکرد شاها بنده هیچ وصف نادر</p></div>
<div class="m2"><p>که در صفات معانی نشد مکرر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تمام کرد یکی مدحتی چو بستان</p></div>
<div class="m2"><p>ز وزن و معنی لاله ز لفظ عبهر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چنانکه راشدی استاد این صناعت</p></div>
<div class="m2"><p>کند فضایل آن پیش شه مفسر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بدیهه گفته ست اندر کتابخانه</p></div>
<div class="m2"><p>به فر دولت شاهنشه مظفر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدان طریق بنا کردم این که گوید</p></div>
<div class="m2"><p>حکیم راشدی آن فاضل سخنور</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>رونده شخصی قلعه گشای و صفدر</p></div>
<div class="m2"><p>پناه عسکر و آرایش معسکر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مفاعلن فعلاتن مفاعلن فع</p></div>
<div class="m2"><p>ز وزن مجتث باشد به وزن کمتر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خدایگانا امروز راشدی را</p></div>
<div class="m2"><p>به فر دولت سلطان ابوالمظفر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>رسید شعر به شعری و شد به گیتی</p></div>
<div class="m2"><p>چو جود کف تو اشعار او مشهر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز شعر اوست همه شعرهای عالم</p></div>
<div class="m2"><p>چنانکه هست همه فعل ها ز مصدر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چو نثر او نبود نثر پر معانی</p></div>
<div class="m2"><p>چو نظم او نبود نظم روح پرور</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>اگر نباشد پیشت رهی مصدق</p></div>
<div class="m2"><p>وگر نداری مر بنده را تو باور</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>حدیث کردن بی حشو او نگه کن</p></div>
<div class="m2"><p>بدین قصیده که امروز خوانده بنگر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دهند بی شک افاضل بدان گواهی</p></div>
<div class="m2"><p>اگر به فضلش سازد رهیت محضر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هر آنکه یارش اقبال شاه باشد</p></div>
<div class="m2"><p>طریق شعر بود نزد او میسر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خدایگانا می خور به شاد کامی</p></div>
<div class="m2"><p>به لحن چنگ و به آرامی نای و مزمر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>به روی حوری رویش چو نقش مانی</p></div>
<div class="m2"><p>ز دست ترکی قدش چو سرو کشمر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>به روی ماه تمام و به چشم نرگس</p></div>
<div class="m2"><p>به زلف عنبر ناب و به قد صنوبر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>به آب رویش نور جمال پیدا</p></div>
<div class="m2"><p>به خم چشمش سحر حلال مضمر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>زیاد بادت از بخت هر زمان عز</p></div>
<div class="m2"><p>فزونت بادا در ملک هر زمان فر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>همیشه تا ز زمین بردمد بنفشه</p></div>
<div class="m2"><p>همیشه تا ز فلک می بتابد اختر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به فر و شادی و لهو و نشاط بنشین</p></div>
<div class="m2"><p>ز عمر و دولت و شادی ملک بر خور</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>همیشه دولت تو یاور و مساعد</p></div>
<div class="m2"><p>همیشه ناصر تو ایزد کروگر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>زمانه رای تو را گشته همچو بنده</p></div>
<div class="m2"><p>سپهر قدر بلند تو را چو چاکر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>همیشه چتر تو را یمن و فتح همره</p></div>
<div class="m2"><p>همیشه تیغ تو را نصر و سعد همبر</p></div></div>