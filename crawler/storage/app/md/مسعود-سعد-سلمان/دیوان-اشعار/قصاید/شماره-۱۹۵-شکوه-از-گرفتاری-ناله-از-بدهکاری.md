---
title: >-
    شمارهٔ ۱۹۵ - شکوه از گرفتاری، ناله از بدهکاری
---
# شمارهٔ ۱۹۵ - شکوه از گرفتاری، ناله از بدهکاری

<div class="b" id="bn1"><div class="m1"><p>روز تا شب ز غم دل افگارم</p></div>
<div class="m2"><p>همه شب تا به روز بیدارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دل شخص جان همی کاهم</p></div>
<div class="m2"><p>به دل اشک خون همی بارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب یک زمان قرارم نیست</p></div>
<div class="m2"><p>راست گویی بر آتش و خارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دو دیده دو جوی بگشادم</p></div>
<div class="m2"><p>بر دو رخ زعفران همی کارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه همسایگان همی شنوند</p></div>
<div class="m2"><p>گریه سخت و ناله زارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته این سپهر رزاقم</p></div>
<div class="m2"><p>خسته این جهان غدارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاین سیه می کند به غم روزم</p></div>
<div class="m2"><p>وین تبه می کند به بدکارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بدان غمگنم که محبوسم</p></div>
<div class="m2"><p>نه بدان رنجه ام که بیمارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخت بیمار بوده ام غمگین</p></div>
<div class="m2"><p>حبس بودست نیز بسیارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست از حمله اجل باکم</p></div>
<div class="m2"><p>نیست از بند پادشه عارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تقاضای قرض خواهانست</p></div>
<div class="m2"><p>همه اندوه و رنج و تیمارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر زمانی سبک شود دل من</p></div>
<div class="m2"><p>کز غم وام ها گرانبارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عاجزم سخت و حقتعالی را</p></div>
<div class="m2"><p>به تو مهتر شفیع می آرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه در کدیه همی کوبم</p></div>
<div class="m2"><p>نه دم عشوه ای همی دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روزی نیم خورده می طلبم</p></div>
<div class="m2"><p>که بدو وام کرده بگذارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر تو سعیی کنی برون آیم</p></div>
<div class="m2"><p>از غمی کاندرو گرفتارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور نیابی به کار من توفیق</p></div>
<div class="m2"><p>به خدای ار من از تو آزارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که من از چرخ سرنگون همه سال</p></div>
<div class="m2"><p>بسته اختر نگونسارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در چنین رنج ها به حق خدای</p></div>
<div class="m2"><p>که به جان مرگ را خریدارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وین سخن گر نه راست می گویم</p></div>
<div class="m2"><p>کافرم وز خدای بیزارم</p></div></div>