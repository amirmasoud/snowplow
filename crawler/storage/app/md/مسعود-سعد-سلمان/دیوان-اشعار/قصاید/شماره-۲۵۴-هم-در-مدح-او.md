---
title: >-
    شمارهٔ ۲۵۴ - هم در مدح او
---
# شمارهٔ ۲۵۴ - هم در مدح او

<div class="b" id="bn1"><div class="m1"><p>به نام ایزد بی چون به قصد حضرت سلطان</p></div>
<div class="m2"><p>ز هندستان برون آمد امیر و شاه هندستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک محمود ابراهیم امیر عالم عادل</p></div>
<div class="m2"><p>که سیف دولت و دین است و عز ملت و ایمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر شاهنشه غازی پناه ملک ابوالقاسم</p></div>
<div class="m2"><p>که خورشید جلالست و سپهرش حضرت سلطان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی راند او سوی حضرت به فیروزی و بهروزی</p></div>
<div class="m2"><p>کشیده رایت عالیش سر بر تارک کیوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خجسته طلعتش تابان میان کوکبه لشکر</p></div>
<div class="m2"><p>چنان کاندر کواکب ماه افروزنده تابان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خوشید درخشنده نهاد او روی در مغرب</p></div>
<div class="m2"><p>شده فیروزه گون گردون پسان دیبه کمسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپهر نیلگون گردی لباس نیلگون توزی</p></div>
<div class="m2"><p>زمین کهرباگون را شدی رخ قیرگون یکسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جنگ روز تاری شب سپاه آوردی از ظلمت</p></div>
<div class="m2"><p>درخشان روز از گیتی شدی از بیم او پنهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب تاری به جنگ اندر کمان را تیز بگشادی</p></div>
<div class="m2"><p>زدی بر ساج گون جوشن هزاران عاج گون پیکان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشست آن خسرو غازی به فرخ مرکبی بر کوست</p></div>
<div class="m2"><p>به موکب شمسه موکب به میدان زینت میدان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سماری سیر و کوه اندام و کوکب چشم و رعد آوا</p></div>
<div class="m2"><p>جهان هیئت زمین طاقت قمر جبهت فلک جولان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رونده مرکبی تازی که پیماید جهان یک شب</p></div>
<div class="m2"><p>تو گویی با فلک دارد به گاه تاختن پیمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بشستی دست هر گه کوب زین پای اندر آوردی</p></div>
<div class="m2"><p>ز رایت رای هندستان ز خانه خان ترکستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شمالی باد هر ساعت شتابش را همی دادی</p></div>
<div class="m2"><p>ز پویه بوی خلق او نسیم روضه رضوان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو گویی جامه ظلمست از عدلش شده معلم</p></div>
<div class="m2"><p>تو گویی نامه کفرست بروی از هدی عنوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو صبح کاذب از مشرق نمودی روی گفتی تو</p></div>
<div class="m2"><p>عمود سیم شاهستی ابر سیمابگون خفتان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو روی از کله بنمودی به گیتی روز افکندی</p></div>
<div class="m2"><p>به روی کوه و صحرا بر به نور مهر شادروان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک زاده شه غازی به رامش کردی آرامش</p></div>
<div class="m2"><p>نه گشته لشکرش مانده نه گشته مرکبش پژمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسان تیره شب تاری بسان تیره شب روشن</p></div>
<div class="m2"><p>چو زلف و دیده حورا چو طبع و خاطر شیطان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز نور طلعت خسرو بسان روز روشن شد</p></div>
<div class="m2"><p>که حاجت نامد اندر وی به نور مشعل سوزان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بگذشتی بدی چونان که عقل از وی شدی عاجز</p></div>
<div class="m2"><p>ز وصفش وهم ها خیره ز نعتش فهم ها حیران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیابانی شده پیدا که بودی اندر او بی شک</p></div>
<div class="m2"><p>هزاران جان شده بی تن هزاران تن شده بی جان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>و زنده باد و تابان مهر در وی راه گم کردی</p></div>
<div class="m2"><p>جز این دو نه درو چیزی ز سیر این و تف آن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به حوض اندر شده آبش چو قرطه دلبران پرچین</p></div>
<div class="m2"><p>به دشت اندر شده تیغش چو زلف دلبران پیچان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه جز خار خسک بستر نه جز سنگ سیه بالین</p></div>
<div class="m2"><p>نه جز باد وزان رهبر نه جز شیر سیه رهبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه گفتم چیز جز یارب نه جستم چیز جز رستن</p></div>
<div class="m2"><p>نه راندم اسب جز پویه نه دیدم خلق جز افغان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بگذشتی بدی چونین که کردم وصف او پیدا</p></div>
<div class="m2"><p>چو زینگونه بیابانی گذاره کرد او زینسان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پدیدار آمدی کوهی چو رایش محکم و عالی</p></div>
<div class="m2"><p>بنش بگذشته از ماهی سرش بگذشته از سرطان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز راوه . . .</p></div>
<div class="m2"><p>گذشتی چون ز نیل مصر بر موسی بن عمران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه کاری توان کردن چو باشد یاورت نصرت</p></div>
<div class="m2"><p>به هر راهی توان رفتن چو باشد رهبرت یزدان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زهر آبی که بگذشتی به هر دشتی که پیوستی</p></div>
<div class="m2"><p>شدی سنگ اندر او لؤلؤ شدی ریگ اندر آن مرجان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شه غازی ملک محمود ازین راهی بدین صعبی</p></div>
<div class="m2"><p>به فیروزی برون آمد به نام حضرت سبحان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهنشاهی که او داده سریر ملک را رتبت</p></div>
<div class="m2"><p>خداوندی کز او گشته قوی مر ملک را بنیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بدو عالی شده دولت بدو صافی شده نیت</p></div>
<div class="m2"><p>بدو پیراسته موکب بدو آراسته ایوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شود ملکش همی افزون دهد بختش همی یاری</p></div>
<div class="m2"><p>کند دهرش همی خدمت برد چرخش همی فرمان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همی بسیاری دریا به نزد کف او اندک</p></div>
<div class="m2"><p>همه دشواری عالم به پیش تیغ او آسان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صنیع خویشتن خواند امیرالمؤمنین او را</p></div>
<div class="m2"><p>شده امکان او افزون که بادش بر فزون امکان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همایون باد و فرخنده بر او این عز و جاه او</p></div>
<div class="m2"><p>همیشه عزوجاه او چو نامش باد جاویدان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رسیده باد حلم او چو سهم او به هر موضع</p></div>
<div class="m2"><p>بر افزون باد تمکینش ز امیرالمؤمنین هزمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خداوندا تو آن شاهی که پیش تو هبا باشد</p></div>
<div class="m2"><p>سخای حاتم طائی و زور رستم دستان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز رای خویشتن شاها به یک لحظه نهی چرخی</p></div>
<div class="m2"><p>اگر جز بر مراد تو کند چرخ فلک دوران</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر ناگه حسود تو کند عصیان تو پیدا</p></div>
<div class="m2"><p>شود اندر دلش آتش به ساعت بی گمان عصیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همی تا منتظم دارد زمین را دور هفت انجم</p></div>
<div class="m2"><p>همی تا تربیت یابد جهان از طبع چار ارکان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همیشه شاد زی شاها به روی زاده خاتون</p></div>
<div class="m2"><p>می مشکین ستان دایم ز دست بچه خاقان</p></div></div>