---
title: >-
    شمارهٔ ۲۴۳ - به دوستی خوشدل نام فرستاده
---
# شمارهٔ ۲۴۳ - به دوستی خوشدل نام فرستاده

<div class="b" id="bn1"><div class="m1"><p>ای خوشدل ای عزیز گرانمایه یار من</p></div>
<div class="m2"><p>ای نیکخواه یار من و دوستدار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی و هیچ گونه نیابی ز غم قرار</p></div>
<div class="m2"><p>با خویشتن ببردی مانا قرار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهجورم و به روز فراق تو جفت من</p></div>
<div class="m2"><p>رنجورم و به شب غم تو غمگسار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوردم به وصلت تو بسی باده نشاط</p></div>
<div class="m2"><p>در فرقت تو پیدا آمد خمار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانم که نیک دانی در فضل دست من</p></div>
<div class="m2"><p>واندر سخن شناخته ای اختیار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد روزگار گشت فرو ماند و خیره شد</p></div>
<div class="m2"><p>بدخواه روزگار من از روزگار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاینجا به حضرت اندر دهقان دشمنم</p></div>
<div class="m2"><p>پیدا همی نیارد در ده هزار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریان شدست و نالان چو ابر نوبهار</p></div>
<div class="m2"><p>نادیده یک شکوفه هنوز از بهار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بحر گردد او نبود تا به کعب من</p></div>
<div class="m2"><p>ور باد گردد او نرسد در غبار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن گوهرم که گردد گوهر مرا صدف</p></div>
<div class="m2"><p>وان آتشم که آتش گردد شرار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وان شیرم از قیاس که چون من کنم زئیر</p></div>
<div class="m2"><p>روبه شوند شیران در مرغزار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر دهر هست بوته هر تجربت چرا</p></div>
<div class="m2"><p>گردون همی گرفت نداند عیار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر روزگار فاضل باشد مرا بسی</p></div>
<div class="m2"><p>گر او کند به راستی و حق شمار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای یادگار مانده جهان را ز اهل فضل</p></div>
<div class="m2"><p>بس باشد این قصیده تو را یادگار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرگز نبود همت من در خور یسار</p></div>
<div class="m2"><p>هرگز نبود در خور همت یسار من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای همچو آشکار من وهم نهان من</p></div>
<div class="m2"><p>دانسته ای نهان من و آشکار من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک ره بیا بر من و کوتاه کن غمم</p></div>
<div class="m2"><p>وز بهر خود دراز مدار انتظار من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای بحر راد مهری از بهر من بگیر</p></div>
<div class="m2"><p>این شعرهای چون گهر شاهوار من</p></div></div>