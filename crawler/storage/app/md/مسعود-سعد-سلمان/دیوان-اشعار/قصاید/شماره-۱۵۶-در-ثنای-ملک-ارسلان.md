---
title: >-
    شمارهٔ ۱۵۶ - در ثنای ملک ارسلان
---
# شمارهٔ ۱۵۶ - در ثنای ملک ارسلان

<div class="b" id="bn1"><div class="m1"><p>با روی تازه و لب پر خنده نوبهار</p></div>
<div class="m2"><p>آمد به خدمت ملک و شاه کامگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان ابولملوک ملک ارسلان که ملک</p></div>
<div class="m2"><p>ذات عزیز او را پرورد در کنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردون دادگستر و مهر جهان فروز</p></div>
<div class="m2"><p>سلطان تاجدار و جهاندار بردبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای اختیار مملکت و افتخار عصر</p></div>
<div class="m2"><p>شایسته اختیاری و بایسته افتخار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دست هر نبرده فرو ماند از نبرد</p></div>
<div class="m2"><p>چون کارزار گردد بر مرد کارزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر حمله ای که آری شاها ثنا کند</p></div>
<div class="m2"><p>بر تو روان رستم و جان سفندیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاری که جست رای تو آمد تو را به سر</p></div>
<div class="m2"><p>تخمی که کشت بخت تو آمد تو را به بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه نه نگویم آنکه چه دیدی هنوز تو</p></div>
<div class="m2"><p>از نوع بختیاری ای شاه بختیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست ابتدای دولت و خواهد شدن هنوز</p></div>
<div class="m2"><p>فغفور پرده دارت و کسری رکابدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صاحبقران شوی و بگیری همه جهان</p></div>
<div class="m2"><p>وایزد بدین سبب ز جهان کردت اختیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردند خسروان زمانه فدای تو</p></div>
<div class="m2"><p>وز خسروان تو مانی در ملک یادگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاهی به هند تازی و گاهی به قیروان</p></div>
<div class="m2"><p>گاهی به روم و گاه به چین گاه زنگبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آری ز ترک خانان بسته به بند پای</p></div>
<div class="m2"><p>رایان ز هند و پیلان کرده ز تنکه بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دانی که با خدای جهان چند نذر کرد</p></div>
<div class="m2"><p>آن اعتقاد روشن تو در شبان تار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اقبال پایدار تو را استوار کرد</p></div>
<div class="m2"><p>زان عهد پایدار تو و نذر استوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در انتظار رحمت و فضل تو مانده ام</p></div>
<div class="m2"><p>ای کرده روزگار تو را دولت انتظار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>داند خدای عرش که گیتی قرار داد</p></div>
<div class="m2"><p>کز رنج دل نیابم شبها همی قرار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من بنده سال سیزده محبوس مانده ام</p></div>
<div class="m2"><p>جان کنده ام ز محنت در حبس و در حصار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین زینهار خوار فلک جان من گریخت</p></div>
<div class="m2"><p>در زینهارت این ملک زینهار دار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در سمج های تنگ و خشن مانده مستمند</p></div>
<div class="m2"><p>در بندهای سخت بتر مانده سوگوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دارم هزار دشمن و یک جان و نیم تن</p></div>
<div class="m2"><p>لیکن گذشته وام من از هشتصد هزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی برگ و بی نوا شده و جمع گرد من</p></div>
<div class="m2"><p>عورات بی نهایت و اطفال بی شمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسیار امیدوار ز تو یافته نصیب</p></div>
<div class="m2"><p>من بی نصیب گشته و مانده امیدوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاها به حق آنکه به کام تو کرده است</p></div>
<div class="m2"><p>کار جهان خدای جهاندار کردگار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پیر ضعیف حالم و درویش عاجزم</p></div>
<div class="m2"><p>بر پیری و ضعیفی من بنده رحمت آر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گیرم گناهکارم و والله که نیستم</p></div>
<div class="m2"><p>نه عفو کرده ای گنه هر گناهکار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا شاد بگذرانم ارم روزگار هست</p></div>
<div class="m2"><p>در مدح و در ثنای تو این مانده روزگار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گیرم به مدح و شکر و ثنای تو هر زمان</p></div>
<div class="m2"><p>هر پایه ای ز تخت تو در در شاهوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این گفتم و ندانم تا چند مانده است</p></div>
<div class="m2"><p>این روح مستحیل درین عمر مستعار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ور من رهی بمانم گنج بماندت</p></div>
<div class="m2"><p>زین طبع حق گزار و زبان سخن گذار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عمری دراز باید تابنده ای چو من</p></div>
<div class="m2"><p>گردد به مدح چون تو جهاندار نامدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا سایه ور درختی گردد نهالکی</p></div>
<div class="m2"><p>بنگر که چند آب درآید به جویبار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاها فراخ سالست این سال ملک تو</p></div>
<div class="m2"><p>وین بس بزرگ فالست اندیشه بر گمار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لؤلؤ ز بحر برده سحاب از پس سحاب</p></div>
<div class="m2"><p>بر ملک تو فشانده نثار از پس نثار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکرویه گشت ملک هلا روی ملک بین</p></div>
<div class="m2"><p>دستت گرفت عدل هلا تخم عدل کار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نو عز و نو بزرگی و نو لهو و نو طرب</p></div>
<div class="m2"><p>نو ملک و نو سعادت و نو روز و نو بهار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شد لعل روی عشرت و شد روی عیش سرخ</p></div>
<div class="m2"><p>ساقی بیار جام می لعل خوشگوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فارغ دل و مرفه بنشین به تخت ملک</p></div>
<div class="m2"><p>انصاف پیشکار تو و عدل دستیار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دشمنت اگر به کینه برآرد چو مار سر</p></div>
<div class="m2"><p>شمشیر تو دمار برآرد ز مغز مار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ناشاد شد عدو سپردش قضا به خاک</p></div>
<div class="m2"><p>تو شادزی و دل به نشاط و طرب سپار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جز در رضای تو نبود چرخ را مسیر</p></div>
<div class="m2"><p>جز بر مراد تو نبود بخت را مدار</p></div></div>