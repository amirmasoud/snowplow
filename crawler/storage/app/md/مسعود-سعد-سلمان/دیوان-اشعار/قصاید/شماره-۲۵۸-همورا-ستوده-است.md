---
title: >-
    شمارهٔ ۲۵۸ - همورا ستوده است
---
# شمارهٔ ۲۵۸ - همورا ستوده است

<div class="b" id="bn1"><div class="m1"><p>تهنیت عید را چو سرو خرامان</p></div>
<div class="m2"><p>از در خرپشته اندر آمد جانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بو یا زلفش به بوی عنبر سارا</p></div>
<div class="m2"><p>رنگین رویش به رنگ لاله نعمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده به شانه دو تاه سیصد حلقه</p></div>
<div class="m2"><p>کرده به تنبول لعل سی و دو مرجان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشک سیاهش به زیر حلقه مغفر</p></div>
<div class="m2"><p>سیم سپیدش به زیر عیبه خفتان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاله خود روی زیر جعد مسلسل</p></div>
<div class="m2"><p>سوسن آزاد زیر زلف پریشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماندم حیران ز روی خوب وی آری</p></div>
<div class="m2"><p>هر که ببیند پری بماند حیران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گریان گریان نگاه کردم در وی</p></div>
<div class="m2"><p>دیده من کرد پاک خندان خندان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تهنیتم کرد و گفت عید مبارک</p></div>
<div class="m2"><p>گفت چو من روز عید خواهی مهمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر رخ او بر زدم گلاب تو گفتی</p></div>
<div class="m2"><p>هست گل سرخ زیر قطره باران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش امروز نزد چاکر بنشین</p></div>
<div class="m2"><p>و آتش هجران من زمانی بنشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتا برخیز و سوی خدمت بشتاب</p></div>
<div class="m2"><p>تهنیت عید بر شهنشه بر خوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خسرو محمود شهریار جهانگیر</p></div>
<div class="m2"><p>خسرو محمود شهریار جهانبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آتش سوزان زده حسامش در هند</p></div>
<div class="m2"><p>دو دو شرارش رسیده در همه گیهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای گه بخشش بسان عیسی مریم</p></div>
<div class="m2"><p>وی گه کوشش بسان موسی عمران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت تو آن کرد کو نکرد به دعوت</p></div>
<div class="m2"><p>تیغ توآن کرد کو نکرد به ثعبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو به لهاور و هول تو به سراندیب</p></div>
<div class="m2"><p>تو به بلا رام و سهم تو به خراسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسته ایام را به ظل تو راحت</p></div>
<div class="m2"><p>خسته افلاس را سخای تو درمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مال فراوان به نزد جود تو اندک</p></div>
<div class="m2"><p>خدمت اندک به مجلس تو فراوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کار جلالت ز ملکت تو به رونق</p></div>
<div class="m2"><p>شغل بزرگی به دولت تو به سامان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاهان دعوی کنند و برهانشان نیست</p></div>
<div class="m2"><p>تو نکنی دعوی و نمایی برهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سست شود دست و پای شاهان چون تو</p></div>
<div class="m2"><p>سخت کنی تنگ روز جنگ به یکران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای چو سلیمان به جاه و حشمت و رتبت</p></div>
<div class="m2"><p>باره شبدیز تو چو تخت سلیمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رفت مه صوم و عید میمون آمد</p></div>
<div class="m2"><p>هست مبشر به فتح های فراوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عیدت فرخنده باد و طاعت مقبول</p></div>
<div class="m2"><p>باد دل و عمر تو ز دولت شادان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باد به کردار عمر نوح تو را عمر</p></div>
<div class="m2"><p>باد حسام تو بر عدوی تو طوفان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چرخ تو را دولت سمایی رهبر</p></div>
<div class="m2"><p>تیغ تو را نصرت خدایی افسان</p></div></div>