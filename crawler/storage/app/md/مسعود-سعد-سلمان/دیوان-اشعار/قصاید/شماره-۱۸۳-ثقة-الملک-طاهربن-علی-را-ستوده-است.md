---
title: >-
    شمارهٔ ۱۸۳ - ثقة الملک طاهربن علی را ستوده است
---
# شمارهٔ ۱۸۳ - ثقة الملک طاهربن علی را ستوده است

<div class="b" id="bn1"><div class="m1"><p>به طاهر علی آباد شد جهان کمال</p></div>
<div class="m2"><p>گرفت عدل نظام و فزود ملک کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود به حکم وی اندر فلک مدار و مسیر</p></div>
<div class="m2"><p>وزد به امر وی اندر هوا جنوب و شمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مهر مملکت از صدر او فروخته روی</p></div>
<div class="m2"><p>چو چرخ مفخرت از قدر او فراخته یال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر ساوش زاید ز خاک زر عیار</p></div>
<div class="m2"><p>ز بهر جودش روید ز سنگ سیم حلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط طبع جز از بزم او ندید پناه</p></div>
<div class="m2"><p>امید روح جز از جود او نیافت منال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هژبر هیبت او بر عدو گذارد چنگ</p></div>
<div class="m2"><p>همای دولت او بر ولی گشاید بال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روز بخشش دستش به مال داد جواب</p></div>
<div class="m2"><p>همای دولت او بر ولی گشاید بال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به روز بخشش دستش به مال داد جواب</p></div>
<div class="m2"><p>هر آن کسی که مر او را به مدح کرد سؤال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهی بزرگی کت هست بر سپهر محل</p></div>
<div class="m2"><p>زهی کریمی کت نیست در زمانه همال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر چه رای تو بی شک به قدر کیوانست</p></div>
<div class="m2"><p>به نام ایزد بر ملک مشتریست به فال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو آن کریم خصالی که چشم چرخ بلند</p></div>
<div class="m2"><p>درین زمانه نبیند چو تو کریم خصال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به حشمت تو چنان شد جهان که بیش زباد</p></div>
<div class="m2"><p>نه زرد گردد برگ و نه چفته گردد نال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عدو ز بار غم ار چه خمیده چوگانست</p></div>
<div class="m2"><p>همی چو گوی نیابد ز زخم سهم تو هال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زوال دشمن دین در کمال دولت تست</p></div>
<div class="m2"><p>کمال دولت شاهیت را مباد زوال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار رحمت بر سال و ماه و روز تو باد</p></div>
<div class="m2"><p>که روز بخت تو ماه است و ماه عمر تو سال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بزرگوار خدایا به حال من بنگر</p></div>
<div class="m2"><p>که چون بگشت و همی گردد از جهان احوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وداع کرد مرا دولت نکرده سلام</p></div>
<div class="m2"><p>فراق جست ز من پیش از آنکه بود وصال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو باد دی دم من سرد و دم نیارم زد</p></div>
<div class="m2"><p>که دل به تنگی میم است و تن به کوژی دال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درین حصار و در آن سمج تاریم که همی</p></div>
<div class="m2"><p>نیارد آمد نزدیک من ز دوست خیال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز رنج لرزان چون برگ یافته آسیب</p></div>
<div class="m2"><p>به درد پیچان چو مار کوفته دنبال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهی ز رنج بپیچم گه از بلا بطپم</p></div>
<div class="m2"><p>چو شیر خسته به تیر و چو مرغ بسته به بال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دلم ز محنت خون گشت و خون همی گریم</p></div>
<div class="m2"><p>همه شب از غم عورات و انده اطفال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه تنگ روزی مردم که چرخ هر ساعت</p></div>
<div class="m2"><p>در افکند به ترازوی روزیم مثقال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تنم هنوز نگشته ست هم به پیری پیر</p></div>
<div class="m2"><p>ولیک رویی دارم چو روی زالی زال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدان درست که در حبس و بند بنده تو</p></div>
<div class="m2"><p>عقاب بی پر گشته ست و شیر بی چنگال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز پیش آنکه زادرار تو بگشتم حال</p></div>
<div class="m2"><p>نشسته بودم با مرگ در جدال و قتال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به فرش و جامه توانگر شدم همی پس از آنک</p></div>
<div class="m2"><p>به حبس جامه من شال بود و فرش بلال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نگاه کن که چگونه زید کسی در حبس</p></div>
<div class="m2"><p>که فرش و جامه او از بلال باشد و شال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غلامکی که جوالیست آنچه او دارد</p></div>
<div class="m2"><p>ز بیم سرما هر شب فرو شدی به جوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من و غلام و کنیزک بدان شده قانع</p></div>
<div class="m2"><p>که هر سه روز همی یافتیم یک من کال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو من ندیدم رویینه و برنجینه</p></div>
<div class="m2"><p>ز بس ضرورت قانع شدم همی به سفال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سخن نگفتم چون نرم آن سفال نبود</p></div>
<div class="m2"><p>سفال که دهد چون نیست خود به قدر سفال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بساختی همه اسباب من خداوندا</p></div>
<div class="m2"><p>شدم ز بخشش تو نیک روز و نیکو فال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو نوعروسان دادی مرا جهاز که هست</p></div>
<div class="m2"><p>چو نوعروسان پایم ز بند در خلخال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ثنای من شنو و از فساد من مشنو</p></div>
<div class="m2"><p>حدیث حاسد مکار و دشمن محتال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خدای بیچون داند که هر چه دشمن گفت</p></div>
<div class="m2"><p>دروغ گفتم دروغ و محال گفت محال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز رنج و غم نبود هیچ ترس و باک ولی</p></div>
<div class="m2"><p>مرا بخواهد کشتن شماتت جهال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رهی جاه توام لازمست نان رهی</p></div>
<div class="m2"><p>عیال جود توام واجبست حق عیال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز کس ننالم جمله من از هنر نالم</p></div>
<div class="m2"><p>از آنکه بر تن من جز هنر نگشت وبال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شود به آب گشوده گلو و حیلت چیست</p></div>
<div class="m2"><p>که در گلوی من آویخته است آب زلال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>درآمدم پس دشمن چو چرغ وقت شکار</p></div>
<div class="m2"><p>چو چرز برزد ناگه بریش من پیخال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر او ازین پس گوریش خواندم شاید</p></div>
<div class="m2"><p>وزین حدیث نباید مرا نمود ملال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو تیغ کند و سیه شد به حبس خاطر من</p></div>
<div class="m2"><p>سپید و بران گردد به یک فسان و صقال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>درخت من که همی سایه بر جهان گسترد</p></div>
<div class="m2"><p>نیافت آب و همه خشک شد به استیصال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کنون ز شاخ من ار بار مدح خواهی جست</p></div>
<div class="m2"><p>به دست خویش کن ای دوست مرمرا ز نهال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مرا بدان تو که در پارسی و در تازی</p></div>
<div class="m2"><p>به نظم و نثر ندارد چو من کس استقلال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>زبانم ار بنگردد به هر بیان گردد</p></div>
<div class="m2"><p>بیان حکمت سست و زبان دانش لال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گواست بر من ایزد که هر امید که هست</p></div>
<div class="m2"><p>به فضل تست پس از فضل ایزد متعال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بکند چرخت مسعود سعد ریش مکن</p></div>
<div class="m2"><p>چو نال گشتی از رنج و ناله بیش منال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مجوی رزم که بازوت را بشد نیرو</p></div>
<div class="m2"><p>مدار یاره که بازوت را نماند مجال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کریم طبعا رادا به خرمی بنشین</p></div>
<div class="m2"><p>نشاط جوی و کرم کن به طبع نیک سگال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو سبز گشت چمن لعل می ستان ز بتی</p></div>
<div class="m2"><p>که بر سپیدی رویش بود سیاهی خال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>همیشه تا بر دانش به حق گشاده بود</p></div>
<div class="m2"><p>در ثواب و عقاب از ره حرام و حلال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به جشن و بزم تو مدحت ستان و خواسته ده</p></div>
<div class="m2"><p>به مهر و کینه تو ناصح نواز و حاسد مال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو مهر تابان تاب و چو چرخ گردان گرد</p></div>
<div class="m2"><p>چو ابر باران بار و چو سرو بالان بال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گشاده چشم به دیدار ساقی و معشوق</p></div>
<div class="m2"><p>کشیده گوش به آواز مطرب و قوال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همیشه باد بقای تو در کمال شرف</p></div>
<div class="m2"><p>وزان کمال و شرف دور باد چشم زوال</p></div></div>