---
title: >-
    شمارهٔ ۱۴۹ - ستایش ظهیرالدوله ابراهیم
---
# شمارهٔ ۱۴۹ - ستایش ظهیرالدوله ابراهیم

<div class="b" id="bn1"><div class="m1"><p>ز عز و مملکت و بخت باد برخوردار</p></div>
<div class="m2"><p>سر ملوک جهان خسرو ملوک شکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظهیر ملت حق بوالمظفر ابراهیم</p></div>
<div class="m2"><p>نصیر دولت و دین پادشاه گیتی دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه عزم و قضا قوت و قدر قدرت</p></div>
<div class="m2"><p>ستاره زیور و خورشید رای و چرخ آثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین توان و هوا صفوت و اثیر نهیب</p></div>
<div class="m2"><p>جهان مکانت و دریا نوال و کوه وقار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز رأی طبع و کف راد و پهن عالی او</p></div>
<div class="m2"><p>فلک زمین شد و دریا سراب و ابر غبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تبارک الله از آن ابر آفتاب فروغ</p></div>
<div class="m2"><p>که برفروزد ازو بخت آسمان کردار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ماه و مهر کند عدل را فراز و نشیب</p></div>
<div class="m2"><p>ز فر و زیب دهد ملک را شعار و دثار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عفوش از تف آتش همی بروید گل</p></div>
<div class="m2"><p>به خشمش از گل تازه همی بروید خار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هیچ گردون چون روی او نتافت نجوم</p></div>
<div class="m2"><p>ز هیچ دریا چون گفت او نخاست بخار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ستارگان مگر از حزم و عزم او زادند</p></div>
<div class="m2"><p>که در جبلت این ثابتست و آن سیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان پناها شاها جهان شاهی را</p></div>
<div class="m2"><p>نبود بی تو دل و دیده روشن و بیدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سحاب جود تو آباد کرد هر ویران</p></div>
<div class="m2"><p>نسیم عدل تو گلزار کرد هر گلزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نه آتش بأست به رزم گشتی تیز</p></div>
<div class="m2"><p>کجا ز گوهر ملک آمدی پدید عیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به کارزار دگر کرده ای نهاد جهان</p></div>
<div class="m2"><p>مگر که قسمت او بوده بود ناهموار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به حد و خنجر لعل تکاوران کردی</p></div>
<div class="m2"><p>زمین هامون دریا و کوه آخته غار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان گشادی بی مرز گر ز سندان کوب</p></div>
<div class="m2"><p>ملوک کشتی بی حد به تیغ خاره گذار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز گرد رخش تو چون چرخ تیره بیند روی</p></div>
<div class="m2"><p>ز آب خنجر ملک تو نصرت آرد بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهشت و دوزخ باشد ضیا و ظلمت را</p></div>
<div class="m2"><p>به کیش مانوی آن مدعی چهره نگار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آنکه نیک همانند نسبتی دارند</p></div>
<div class="m2"><p>به مهر و کینه تو روز روشن و شب تار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شراب عدل تو گرمست کرد عالم را</p></div>
<div class="m2"><p>نهیب تو ببرد از سر زمانه خمار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>محیط گیتی گشته ست همت تو از آنک</p></div>
<div class="m2"><p>همی نماید گیتیش نقطه پرگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو روی و پشت عدوی تو زرد و مجروحست</p></div>
<div class="m2"><p>ز زخم سطوت جود تو چهره دینار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگر مخالف و بدخواه ملک و دولت تست</p></div>
<div class="m2"><p>ز آب و آتش خیل حباب و فوج شرار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از آن حباب چو سر برکند شود ناچیز</p></div>
<div class="m2"><p>وز آن شرار چو سر برزند بمیرد زار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نماند در همه روی زمین خداوندی</p></div>
<div class="m2"><p>که او به بندگی تو نمی کند اقرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بزرگوار خدایا چو قرب ده سالست</p></div>
<div class="m2"><p>که می بکاهد جان من از غم و تیمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رخم ز ناخن خسته برم ز دست کبود</p></div>
<div class="m2"><p>دلم ز آتش سوزان تنم چو موی نزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بس که تف بلاچپ و راست بر من زد</p></div>
<div class="m2"><p>ز من بجست چو سیماب بی قرار قرار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدین تغیر هایل به نعمت عالی</p></div>
<div class="m2"><p>که طعم عیشم زهرست و رنگ روزم تار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان بلرزم کاندر هوا نلرزد مرغ</p></div>
<div class="m2"><p>چنان بپیچم کاندر زمین نپیچد مار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تنم هژبری دارد شکسته اندر چنگ</p></div>
<div class="m2"><p>دلم عقابی دارد گرفته در منقار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو کلک و نیزه اگر راست نیستم دل و تن</p></div>
<div class="m2"><p>چو کلک و نیزه مرا هست بر میان زنار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چرا ز دولت عالی تو پیچم روی</p></div>
<div class="m2"><p>که بنده زاده این دولتم به هفت تبار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه سعد سلمان پنجاه سال خدمت کرد</p></div>
<div class="m2"><p>به دست کرد برنج این همه ضیاع و عقار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به من سپرد و ز من بستدند فرعونان</p></div>
<div class="m2"><p>شدم به عجز و ضرورت ز خانمان آوار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به حضرت آمدم انصاف خواه و داد طلب</p></div>
<div class="m2"><p>خبر نداشتم از حکم ایزد دادار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه روشنایی و باران ز مهر و ابر بود</p></div>
<div class="m2"><p>نه جست باید روزی ز کف تو ناچار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرا امید به هنجار مقصدی بنمود</p></div>
<div class="m2"><p>دلم برد که به مقصد بیاردم هنجار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همی ندانم خود را گناهی و جرمی</p></div>
<div class="m2"><p>مگر سعایت و تلبیس دشمن مکار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز من بترسد ای شاه خصم ناقص من</p></div>
<div class="m2"><p>که کار مدح به من بازگردد آخر کار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز شال پیدا آرند دیبه رومی</p></div>
<div class="m2"><p>ز جزع باز شناسند لؤلؤ شهوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز پارگین بشناسند بحر در آگین</p></div>
<div class="m2"><p>ز تار میغ بدانند ابر گوهر بار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپر فکند و ندیده به دست من شمشیر</p></div>
<div class="m2"><p>بداد پشت و نبوده میان ما پیکار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در آن هزیمت تیری گشاد در دیده</p></div>
<div class="m2"><p>مرا بخست چو من داشتم گشادش خوار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خدای داند و هر کو خدای را به دروغ</p></div>
<div class="m2"><p>گواه خوانده باشد ز جمله کفار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که قصد من همه آن بود تا به خدمت شاه</p></div>
<div class="m2"><p>چو بندگان دگر تیز گرددم بازار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هزار دیوان سازم ز نظم و در هر یک</p></div>
<div class="m2"><p>هزار مدح طرازم چو صد هزار نگار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مشاطه وار عروسان پردگی ضمیر</p></div>
<div class="m2"><p>به پیش تخت کنم جلوه و به مجلس بار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به صیقل صفت و مدح نیک بزدایم</p></div>
<div class="m2"><p>ز تیغ آتش و آیینه هنر زنگار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به اختران خرد بخت را کنم گردون</p></div>
<div class="m2"><p>به لعبتان سخن بزم را کنم فرخار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو عندلیب سرایم ثنای مدحت تو</p></div>
<div class="m2"><p>چرا ببندم چون باز بسته بر کهسار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یکی به رحمت بر جان و بر تنم بخشای</p></div>
<div class="m2"><p>که من نه در خور بندم شهانه اهل حصار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نگاه کن که چه نیرنگ ها و شعبده ها</p></div>
<div class="m2"><p>به مدحت تو برآرم ز جان و دل هر بار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه من کفایت عرضه همی کنم به سخن</p></div>
<div class="m2"><p>توان ستود فلک را به رتبت و مقدار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تکلفی نشود در مثل به حلم جبال</p></div>
<div class="m2"><p>تعذری نبود در سمر به جود بحار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چه رنج فکرت باید کشید اگر گویم</p></div>
<div class="m2"><p>که آفتاب منیرست و آسمان دوار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گزیده تر ز همه دولتست دولت تو</p></div>
<div class="m2"><p>گزیده تر ز همه فصل هاست فصل بهار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به پایه ای ز محلت نمی رسد گردون</p></div>
<div class="m2"><p>پدید باشد کآخر کجا رسد گفتار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>اگر سزای تو باید همی مدیح و ثنا</p></div>
<div class="m2"><p>مگر گشاده شود بر همه ملوک اشعار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همیشه تا زبر گوی بی مدار سپهر</p></div>
<div class="m2"><p>نجوم و چرخ نیاساید از مسیر و مدار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خدایگانا چون آفتاب ملک افروز</p></div>
<div class="m2"><p>زمانه دارا چون آسمان زمانه گذار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نظاره گاه تو بر تختگاه باد و چمن</p></div>
<div class="m2"><p>نشستگاه تو از ملک فرق باد و کنار</p></div></div>