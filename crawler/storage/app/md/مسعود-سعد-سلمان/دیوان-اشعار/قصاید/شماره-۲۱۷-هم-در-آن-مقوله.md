---
title: >-
    شمارهٔ ۲۱۷ - هم در آن مقوله
---
# شمارهٔ ۲۱۷ - هم در آن مقوله

<div class="b" id="bn1"><div class="m1"><p>چون مشرفست همت بر رازم</p></div>
<div class="m2"><p>نفسم غمی نگردد از آزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در به زیر پاره الماسم</p></div>
<div class="m2"><p>چون زر پخته در دهن گازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته دو پای و دوخته دو دیده</p></div>
<div class="m2"><p>تا کی بوم صبور که نه بازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با هر چه آدمیست همی گویی</p></div>
<div class="m2"><p>در هر غمی کش افتد انبازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من گوهرم ز آتش دل ترسم</p></div>
<div class="m2"><p>ناگاهی آشکاره شود رازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه نه که گر فلک بودم بوته</p></div>
<div class="m2"><p>وآتش بود اثر بنگدازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی سفر نبینم و از دانش</p></div>
<div class="m2"><p>گه در حجاز و گاه در اهوازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ابرم که در و لؤلؤ بفشانم</p></div>
<div class="m2"><p>چون رعد در جهان بود آوازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از راستی چو تیره بود بیتم</p></div>
<div class="m2"><p>دشمن کشم از آن چو بیندازم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان شعر کایچ خامه نپردازد</p></div>
<div class="m2"><p>کانرا به یک نشست نپردازم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بادم به نظم و نثر و نه نمامم</p></div>
<div class="m2"><p>مشکم به خلق و جود و نه غمازم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مقصود می نیابم و می جویم</p></div>
<div class="m2"><p>مقصد همی نبینم و می تازم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر عمر و بر جوانی می گریم</p></div>
<div class="m2"><p>کانچم ستد فلک ندهد بازم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با چرخ در قمارم و می مانم</p></div>
<div class="m2"><p>وین دست چون نگر که همی بازم</p></div></div>