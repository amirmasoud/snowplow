---
title: >-
    شمارهٔ ۲ - در ستایش محمود شاه
---
# شمارهٔ ۲ - در ستایش محمود شاه

<div class="b" id="bn1"><div class="m1"><p>شاهان جهان شاهی و شاه جهانیا</p></div>
<div class="m2"><p>در چشم جور و عدل پدید و نهانیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بایسته تر به خسروی اندر ز دیده ای</p></div>
<div class="m2"><p>شایسته تر به مملکت اندر زجانیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل و روان به لطف نیابد همی تو را</p></div>
<div class="m2"><p>گویی که عقل دیگر و دیگر روانیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن به توست سنت و آیین خسروی</p></div>
<div class="m2"><p>تازه به توست رسم و ره پهلوانیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مذهب تناسخ اثبات گرددی</p></div>
<div class="m2"><p>من گویمی تو بی شک نوشیروانیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویم مگر که صورت عقلی عیان شده</p></div>
<div class="m2"><p>چون بنگرم به عقل و حقیقت همانیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویی صفات ایزدی اندر صفات توست</p></div>
<div class="m2"><p>کایدون فزون ز وهم و برون از گمانیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنده نیازی گویی که دولتی</p></div>
<div class="m2"><p>دارنده زمینی گویی زمانیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با هر کسی چو با تن مهجور وصلتی</p></div>
<div class="m2"><p>در هر دلی چو در دل مجرم امانیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاها نظام یابد هندوستان کنون</p></div>
<div class="m2"><p>زان خنجر زدوده هندوستانیا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صاحبقران تو باشی و اینک خدایگان</p></div>
<div class="m2"><p>دادت به دست خاتم صاحبقرانیا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا مملکت بماند تو جاودان بمان</p></div>
<div class="m2"><p>اندر میان مملکت جاودانیا</p></div></div>