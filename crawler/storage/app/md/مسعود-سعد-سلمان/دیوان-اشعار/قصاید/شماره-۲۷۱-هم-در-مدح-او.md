---
title: >-
    شمارهٔ ۲۷۱ - هم در مدح او
---
# شمارهٔ ۲۷۱ - هم در مدح او

<div class="b" id="bn1"><div class="m1"><p>روز مهر و ماه مهر و جشن فرخ مهرگان</p></div>
<div class="m2"><p>مهر بفزای ای نگار مهرجوی مهربان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو روی عاشقان بینم به زردی روی باغ</p></div>
<div class="m2"><p>باده باید بر صبوحی همچو روی دوستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاجهاشان بود بر سر از عقیق و لاجورد</p></div>
<div class="m2"><p>قرطه هاشان بود در بر از پرند و پرنیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کله ها زد باد نیسان از ملون جامه ها</p></div>
<div class="m2"><p>پرده ها بست ابر آزار از منقش بهرمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشک بودی بی حد و کافور بودی بی قیاس</p></div>
<div class="m2"><p>در بودی بی مر و یاقوت بودی بی کران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حمل بویا مشک بودی تنگها بر تنگها</p></div>
<div class="m2"><p>بار مروارید بودی کاروان در کاروان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خزانی باد سوی بوستان لشکر کشید</p></div>
<div class="m2"><p>زینتش گشتست روی ارغوان چون زعفران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا کاکنون به سوی باغ و بستان بگذری</p></div>
<div class="m2"><p>دیبه زربفت بینی زین کران تا آن کران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غبار باد دیناری شده برگ درخت</p></div>
<div class="m2"><p>وز صفای آب زنگاری شده جوی روان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد چو روی بدسگال مملکت برگ درخت</p></div>
<div class="m2"><p>باشد آب جوی همچون تیغ شاه کامران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیف دولت شاه محمود بن ابراهیم آنک</p></div>
<div class="m2"><p>جان شاهی را تنست و شخص شاهی را روان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خسرو خسرو نژاد و پهلو پهلو نسب</p></div>
<div class="m2"><p>شهریار بر و بحر و پادشاه انس و جان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش او حلم زمین همچون هوا باشد سبک</p></div>
<div class="m2"><p>پیش طبع او هوا هم چون زمین باشد گران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از نهیب گرز او در چرخ گردنده اثر</p></div>
<div class="m2"><p>وز سر شمشیر او بر ماه دو هفته نشان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای گه بخشش فریدون گاه کوشش کیقباد</p></div>
<div class="m2"><p>ای به همت اردشیر و ای به حشمت اردوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ور فریدون قباد و اردوان و اردشیر</p></div>
<div class="m2"><p>زنده اندی پیش رخشت بنده بودندی دوان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کوه و بحر و آفتاب و آسمان خوانم تو را</p></div>
<div class="m2"><p>کوه و بحر و آفتاب و آسمانی بی گمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو به گاه حلم کوهی و به گاه علم بحر</p></div>
<div class="m2"><p>گاه رفعت آفتابی گاه قدرت آسمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ تو چون برفروزد در میان کارزار</p></div>
<div class="m2"><p>مغز بدخواهت بجوشد در میان استخوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جشن فرخ مهرگان آمد به خدمت مر تو را</p></div>
<div class="m2"><p>خسروانی جام بستان بر نهاد خسروان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جوشن و برگستوان از خز باید ساختن</p></div>
<div class="m2"><p>کامد اینک با لباس لشکری باد خزان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرخ و فرخنده بادت مهرگان و روز مهر</p></div>
<div class="m2"><p>باد دولت با تو کرده صد قران در یک قران</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک از تو با نشاط و تو ز ملکت با نشاط</p></div>
<div class="m2"><p>دولت از تو شادمان و تو ز دولت شادمان</p></div></div>