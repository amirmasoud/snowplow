---
title: >-
    شمارهٔ ۲۶۴ - درود بر خواجه احمد بن حسن
---
# شمارهٔ ۲۶۴ - درود بر خواجه احمد بن حسن

<div class="b" id="bn1"><div class="m1"><p>شاد باش ای زمانه ریمن</p></div>
<div class="m2"><p>بکن آنچ آید از تو در هر فن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن اگر روی گرددم بگداز</p></div>
<div class="m2"><p>پشت اگر سنگ گرددم بشکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بنایی برآیدم بشکوب</p></div>
<div class="m2"><p>ور نهالی ببالدم بر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که افتاد برکشش در وقت</p></div>
<div class="m2"><p>من چو برخاستم مرا بفکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازم اندر بلایی افکندی</p></div>
<div class="m2"><p>که کشیدن نمی تواند تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر آن خانه ام که از تنگی</p></div>
<div class="m2"><p>نجهدم باد هیچ پیرامن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ز تنگی اگر شوم دلتنگ</p></div>
<div class="m2"><p>نتوانم درید پیراهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور مهتاب و آفتاب همی</p></div>
<div class="m2"><p>به شب و روز بینم از روزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترسم از بس که دید تاریکی</p></div>
<div class="m2"><p>اندرین حبس چشم روشن من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دید نتوانم ار خلاص بود</p></div>
<div class="m2"><p>همچو خفاش چشمه روشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بند من گشت از آنچه نسبت کرد</p></div>
<div class="m2"><p>از دل دلربای من آهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان کنون همچو بچگان عزیز</p></div>
<div class="m2"><p>دارمش زیر سایه دامن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر از من به حیله ببریدند</p></div>
<div class="m2"><p>این همه دوستان عهد شکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه سبب را فرو گذاشت مرا</p></div>
<div class="m2"><p>خواجه سید رئیس ابن حسن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه از نوبهاری رادی او</p></div>
<div class="m2"><p>به خزان رست در جهان سوسن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه دانش بدو نموده هنر</p></div>
<div class="m2"><p>وانکه دانا ازو گشاده سخن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بزرگی و فضل را ماوی</p></div>
<div class="m2"><p>وی کریمی و جود را مسکن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه چو لفظ تو در دریا بار</p></div>
<div class="m2"><p>نه چو کف تو ابر در بهمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر جوادی به نزد تو سفله</p></div>
<div class="m2"><p>هر فصیحی به نزد او الکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا همی مهر بردمد به فلک</p></div>
<div class="m2"><p>تا همی سرو بر جهد ز چمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جهان دوستکام بادی تو</p></div>
<div class="m2"><p>که شدم من به کامه دشمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر تو نالم همی معونت کن</p></div>
<div class="m2"><p>مر مرا از زمانه ریمن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد جفت تو دولت میمون</p></div>
<div class="m2"><p>باد یار تو ایزد ذوالمن</p></div></div>