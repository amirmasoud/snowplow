---
title: >-
    شمارهٔ ۲۰۲ - مدح ابوالفرج نصر بن رستم
---
# شمارهٔ ۲۰۲ - مدح ابوالفرج نصر بن رستم

<div class="b" id="bn1"><div class="m1"><p>افتخار اهل تیغ ای صاحب اهل قلم</p></div>
<div class="m2"><p>شمع سادات عرب خورشید احرار عجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای امین شاه غازی صاحب دیوان هند</p></div>
<div class="m2"><p>روشن از رای تو بینم کار تاریک حشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عمید ملک سلطان بوالفرج اهل فرج</p></div>
<div class="m2"><p>ناصر دین و دیانت خواجه نصر روستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج دانش دایم از بحر دلت پر گوهر است</p></div>
<div class="m2"><p>باغ طبع اهل فضلت گشت چون باغ ارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاکر کلک تو گشته بنده رایت شده</p></div>
<div class="m2"><p>هر که هست اندر همه عالم ز اعیان محتشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جاودان بشکفته بستان گل اقبال تو</p></div>
<div class="m2"><p>زانکه دارد باغ ایران ز ابر تو همواره نم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جاه تو بر اوج کیوان سر برآورد از زمین</p></div>
<div class="m2"><p>جود تو بر فرق فرقد بر نهاد ایدون قدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب مهر دوستانت خورده زان خوش گشت عود</p></div>
<div class="m2"><p>خون بدخواهانت خورده گشت از آن رنگین بقم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناصحان پیوسته از فر تو شاد و بی غمند</p></div>
<div class="m2"><p>حاسدان همواره ز اقبال تو در تیمار و غم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تو در عالم نیامد صاحبی با داد و دین</p></div>
<div class="m2"><p>گشته ای از داد و دین اندر همه عالم علم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دلت شد بحر معنی لفظ تو در و گهر</p></div>
<div class="m2"><p>خوار شد پیش دل و دستت همه زر و درم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا تو را دادار داد انصاف و داد اندر جهان</p></div>
<div class="m2"><p>گشت چون سیمرغ پنهان از جهان جور و ستم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامه ای شد فتح و دولت جود تو بر وی خطاب</p></div>
<div class="m2"><p>دفتری شد عز و ملت جاهت اندر وی رقم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسرو خسروشکن در مملکت همچون جم است</p></div>
<div class="m2"><p>باز چون آصف تویی روز و شب اندر فضل جم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیست همچون شاه عالم محتشم شاه ملوک</p></div>
<div class="m2"><p>نیست از ارکان دولت همچو تو کس محتشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سید اقران خویشی در کفایت روز فضل</p></div>
<div class="m2"><p>همچنان چون صاحب گردان بهیجا روستم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گردش گردون نیارد همچو تو نیکو سیر</p></div>
<div class="m2"><p>دیده گردون نبیند همچو تو عالی همم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از یم طبع تو خیزد گوهر عقل و خرد</p></div>
<div class="m2"><p>گوهر عقل و خرد نیکوترست از در یم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پسته و فندق ز مهر و کین تو آگه شدند</p></div>
<div class="m2"><p>این فم از مدحت گشاد و آن ز بیمت بست فم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که در راه خلاف و خشم تو بنهاد پای</p></div>
<div class="m2"><p>رفتنش چون مار بر پشت زمین گشت از شکم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایزد از خلق تو آرد در جهان پیدا بهار</p></div>
<div class="m2"><p>زان چونیسان اندر آمد زآن شود گیتی خرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچو تو مخدوم ناید فضل را هرگز پدید</p></div>
<div class="m2"><p>زین قبل گشتند افاضل مر تو را یکسر خدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای همایون طبع تو پیرایه جود و هنر</p></div>
<div class="m2"><p>وی مبارک خاطر تو مایه فضل و کرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از تو زیباتر نیاید در جهان صاحب بلی</p></div>
<div class="m2"><p>از تو والاتر نباشد در زمین مهتر نعم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ظلمت این شعر رای روشن تو نور کرد</p></div>
<div class="m2"><p>هر کجا آثار نور آمد شود روشن ظلم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنده بر تو گشتم حلقه در گوش ای عمید</p></div>
<div class="m2"><p>زانکه برناید ز من جز آفرینت هیچ دم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بس فراوان بینوا از فر تو گشته غنی</p></div>
<div class="m2"><p>من هم از فر تو گشتم فارغ از رنج و الم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از تو در هندوستان یافتم من نام جود</p></div>
<div class="m2"><p>قد بختم راست از تو شد کجا بد پر ز خم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در حوالی طوف خواهی کرد بر کام ولی</p></div>
<div class="m2"><p>تا کنی بدخواه شاه از دولت سلطان دژم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بود بی قدر دایم در مسلمانی شمن</p></div>
<div class="m2"><p>تا بود در پیش ایزد خار جاویدان صنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر بساط سرورانی جاودان دایم بمان</p></div>
<div class="m2"><p>در بهشت ناحیت دلشاد جاویدان بچم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باد میمون و مبارک بر تو این عید جلیل</p></div>
<div class="m2"><p>دشمنان را کن بسان گوسپند و گاو کم</p></div></div>