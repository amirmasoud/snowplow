---
title: >-
    شمارهٔ ۲۷ - در مدیح
---
# شمارهٔ ۲۷ - در مدیح

<div class="b" id="bn1"><div class="m1"><p>دل از دولت همیشه شاد بادت</p></div>
<div class="m2"><p>که ما شادیم تا بینیم شادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آنی کز خرد چیزی نماندست</p></div>
<div class="m2"><p>درین گیتی که آن ایزد ندادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستوده سیرت و پاکیزه طبعت</p></div>
<div class="m2"><p>گزیده فعلت و نیکو نهادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو چرخ عالی از رتبت محلت</p></div>
<div class="m2"><p>چو آب صافی از پاکی نژادت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین پیراسته است از تیغ تیزت</p></div>
<div class="m2"><p>جهان آراسته است از دست رادت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان بندگی اقبال بستت</p></div>
<div class="m2"><p>زبان محمدت دولت گشادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خدمت بخت هم زانو نشستت</p></div>
<div class="m2"><p>به حرمت فتح در پیش ایستادت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی تازه شود عالم به نامت</p></div>
<div class="m2"><p>همی باده خورد دولت به یادت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هنرمندی ز تو نادر نباشد</p></div>
<div class="m2"><p>چو ملک شاه باشد اوستادت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همایون باد بر تو عید و هر روز</p></div>
<div class="m2"><p>که از گردون بر آید عید بادت</p></div></div>