---
title: >-
    شمارهٔ ۲۶۹ - مدح عمیدالملک ابوالقاسم
---
# شمارهٔ ۲۶۹ - مدح عمیدالملک ابوالقاسم

<div class="b" id="bn1"><div class="m1"><p>روز نوروز و ماه فروردین</p></div>
<div class="m2"><p>آمدند ای عجب ز خلد برین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاجها ساخت گلبنان را آن</p></div>
<div class="m2"><p>حله ها بافت باغ ها را این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باد فرخنده بر عمید اجل</p></div>
<div class="m2"><p>خاصه پادشاه روی زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمده دین و ملک ابوالقاسم</p></div>
<div class="m2"><p>که بیاراست روی ملک به دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن بزرگی که رایت همت</p></div>
<div class="m2"><p>بگذرانید از اوج علیین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ذکا کرد ملک را ثابت</p></div>
<div class="m2"><p>به دها داد فتنه را تسکین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنر از رای او برد تعظیم</p></div>
<div class="m2"><p>خرد از طبع او کند تلقین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزم او را مضای بادبزان</p></div>
<div class="m2"><p>حرم او را ثبات کرده متین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این یکی را زمانه زیر رکاب</p></div>
<div class="m2"><p>وان یکی را سپهر زیر نگین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور و ظلمت بود به عفو و به خشم</p></div>
<div class="m2"><p>آب و آتش بود به مهر و به کین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه عجب گر ز داد او زین پس</p></div>
<div class="m2"><p>خویش گردد تذرو را شاهین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاد باش ای جهان به روی تو شاد</p></div>
<div class="m2"><p>غم نصیب عدوست شاد نشین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه چو تو گاه بزم ابر بهار</p></div>
<div class="m2"><p>نه چو تو وقت رزم شیر عرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>راست گویی ز بهر تیغ و قلم</p></div>
<div class="m2"><p>آفریده شد آن خجسته یمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بنده خویش را معونت کن</p></div>
<div class="m2"><p>ای جهان را شده به عدل معین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که خواهد همیشه شادی تو</p></div>
<div class="m2"><p>نبود در همه جهان غمگین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شب نخسبم همی ز رنج و عنا</p></div>
<div class="m2"><p>نیست حاجت به بستر و بالین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر به تو نیستی قوی دل من</p></div>
<div class="m2"><p>چکدی زهره من مسکین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از تو بودی همه تعهد من</p></div>
<div class="m2"><p>گاه محنت به حصن های حصین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جان تو دادی مرا پس از ایزد</p></div>
<div class="m2"><p>اندرین حبس و بند باز پسین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به خدایی که صنع و حکمت او</p></div>
<div class="m2"><p>ماند از گردش شهور و سنین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که به باقی عمر یک لحظه</p></div>
<div class="m2"><p>رو نتابم زخدمتت پس ازین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سازم از جود تو ضیاع و عقار</p></div>
<div class="m2"><p>گیرم از مدح تو رفیق و قرین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ببرد چون به روی تو نگرم</p></div>
<div class="m2"><p>شادی تو ز روی بختم چین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فخرم آن بس بود که هر روزی</p></div>
<div class="m2"><p>بر بساطت نهم به عجز جبین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا بود بر فلک طلوع و غروب</p></div>
<div class="m2"><p>تا بود در زمان مکان و مکین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>باد چرخ محل و رتبت تو</p></div>
<div class="m2"><p>روشن از ماه و زهره و پروین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد باغ نشاط و نزهت تو</p></div>
<div class="m2"><p>خرم از لاله و گل و نسرین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من مبارک زبان و نیک پیم</p></div>
<div class="m2"><p>هم چنین باد و هم چنین آمین</p></div></div>