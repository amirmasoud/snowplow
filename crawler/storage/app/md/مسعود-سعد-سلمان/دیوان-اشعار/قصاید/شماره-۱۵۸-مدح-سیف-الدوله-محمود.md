---
title: >-
    شمارهٔ ۱۵۸ - مدح سیف الدوله محمود
---
# شمارهٔ ۱۵۸ - مدح سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>چو روز روشن بنمود چهره از شب تار</p></div>
<div class="m2"><p>زدود مهر ز آیینه فلک زنگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان که نور ز رأی خدایگان جهان</p></div>
<div class="m2"><p>بتافت مهر منیر از سپهر دایره وار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی گذشت به من بر چو روی اهریمن</p></div>
<div class="m2"><p>چو خط مرکز در خط دایره پرگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم چو گردون از عشق ناشکیب شده</p></div>
<div class="m2"><p>پدید کرد همه رازش آن دو زلف چو قار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبست زلفش و گردون دل من و نه عجب</p></div>
<div class="m2"><p>که راز گردون آید پدید در شب تار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم چو دریا در موج کرده پیدا سر</p></div>
<div class="m2"><p>به گاه موج ز دریا شود پدید شرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا ز دیده روان خون و خواب رفته از آن</p></div>
<div class="m2"><p>بلی ز رفتن خونست علت بیدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جدا شده من از آن ماه خویش و گم کرده</p></div>
<div class="m2"><p>ز من دلی به بیابان عاشقی هنجار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنم به تیر غمان کرده عشق او خسته</p></div>
<div class="m2"><p>دلم به تیغ هوا کرده هجر او افگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عیاروار دل من ربود دلبر من</p></div>
<div class="m2"><p>بلی ربودن باشد همیشه کر عیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا خوشست وگر چند ناخوشست مدام</p></div>
<div class="m2"><p>ز درد هجران عیش من ای ملامت کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن ملامت و بر سوخته نمک مفکن</p></div>
<div class="m2"><p>ز جنگ دست بدار و مرا عذاب مدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز چوب خشک چرا بود بایدم کمتر</p></div>
<div class="m2"><p>که ناله گیرد چون او جدا شود از یار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه کمترم به وفا داشتن من از قمری</p></div>
<div class="m2"><p>که از فراق به گاه سحر بموید زار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو زیر چنگ همه روز مدح او گویم</p></div>
<div class="m2"><p>اگر چه گشتم چون زیر چنگ زار و نزار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه جویم همچون شراب شادی او</p></div>
<div class="m2"><p>وگرچه دارد چون جرعه شرابم خوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر ببارد ابر رضای او بر من</p></div>
<div class="m2"><p>خزان هجرش بر من شود ز وصل بهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر برین دل من مهر مهر او تابد</p></div>
<div class="m2"><p>درخت شادی و لهو و نشاط آرد بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی چه نالم چندین ز هجر آن دلبر</p></div>
<div class="m2"><p>چو زود ناله کند دیر به شود بیمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هزار شکرت امروز مر مرا ز فراق</p></div>
<div class="m2"><p>هزار شکر بگویم نه بل هزار هزار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که از فراق دلارام شد مرا حاصل</p></div>
<div class="m2"><p>وصال درگه معمور شاه گیتی دار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شه مظفر و منصور شاه دولت و داد</p></div>
<div class="m2"><p>خدایگان فلک همت ملک دیدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امیر غازی محمود سیف دولت و دین</p></div>
<div class="m2"><p>بنام و سیرت و کنیت چو احمد مختار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خجسته نامش زیبنده بر کمینه ملک</p></div>
<div class="m2"><p>چو نقش بر دیبا و چو مهر در دینار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهنشهی که به شاهنشهی او دولت</p></div>
<div class="m2"><p>به طوع و رغبت اقرار کرد بی اجبار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شهی که هست کف و تیغ او به رزم و به بزم</p></div>
<div class="m2"><p>چو بحر گوهر موج و چو ابر صاعقه بار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی گشاید کشور همی ستاند ملک</p></div>
<div class="m2"><p>به تیغ جان انجام و به گرز عمر اوبار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به بندگیش بزرگی همی شود راضی</p></div>
<div class="m2"><p>به چاکریش زمانه همی دهد اقرار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهان و گنبد دوار چون بدیدندش</p></div>
<div class="m2"><p>به گاه آن که همی کرد با عدو پیکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهان ز روز و شب ساخت جوشن و خفتان</p></div>
<div class="m2"><p>ز مهر و ماه سپر کرد گنبد دوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمانه کرد همی مستی از شراب ستم</p></div>
<div class="m2"><p>ببرد خنجر او از سر زمانه خمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی به روزی صدره سر قلم بزند</p></div>
<div class="m2"><p>از آن که هست قلم بسته بر میان زنار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه مر فضایل او را جهان دهد تفصیل</p></div>
<div class="m2"><p>نه مر مناقب او را کند سپهر شمار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خدایگانا مهر تو فکر توست مگر</p></div>
<div class="m2"><p>کزو نباشد خالی دل صغار و کبار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر نکردی قدر تو بر فلک مسکن</p></div>
<div class="m2"><p>فلک نبودی زینسان که هست با مقدار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر نگشتی نام تو در جهان سایر</p></div>
<div class="m2"><p>جهان نبودی چونین که هست پر انوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>رکاب و پای تو جوینده عنان و کفت</p></div>
<div class="m2"><p>به کارزار عدو در سوار گرد سوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شود ز هیبت تیغت رکاب او خلخال</p></div>
<div class="m2"><p>شود ز بیم سنان تو ساعدش افگار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه باشد نام ملوک زنده به شعر</p></div>
<div class="m2"><p>ولیک زند به نام تو بازگشت اشعار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شگفت نیست که مدحت همی بلند آید</p></div>
<div class="m2"><p>به دولت تو رهی را بلند شد گفتار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سخن به وزن درست آید و به نظم قوی</p></div>
<div class="m2"><p>چو باشدش هنر مرد پر خرد معیار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همیشه تا ملکا بردمد چو خاطر تو</p></div>
<div class="m2"><p>به حکم ایزد خورشید روشن از شب تار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به کامگاری جز فرش خرمی مسپر</p></div>
<div class="m2"><p>به شادمانی جز دل به خرمی مسپار</p></div></div>