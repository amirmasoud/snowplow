---
title: >-
    شمارهٔ ۳۳ - شکایت از اوضاع و مدح عمید حسن
---
# شمارهٔ ۳۳ - شکایت از اوضاع و مدح عمید حسن

<div class="b" id="bn1"><div class="m1"><p>هیچ کس را غم ولایت نیست</p></div>
<div class="m2"><p>کار اسلام را رعایت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست یک تن درین همه اطراف</p></div>
<div class="m2"><p>که درو وهن را سرایت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارهای فساد را امروز</p></div>
<div class="m2"><p>حد و اندازه ای غایت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کنند این و هیچ مفسد را</p></div>
<div class="m2"><p>بر چنین کارها نکایت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست انصاف را مجال توان</p></div>
<div class="m2"><p>عدل را قوت حمایت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین قوی دست مفسدان ما را</p></div>
<div class="m2"><p>دست و تمکین یک جنایت نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر ای خواجه عمید حسن</p></div>
<div class="m2"><p>از تو این خلق را عنایت نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از همه کارها که در گیتی است</p></div>
<div class="m2"><p>هیچ کس را چو تو هدایت نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شد آخر نماند مرد و سلاح</p></div>
<div class="m2"><p>علم و طبل نی و رایت نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لشکری نیست کار دیده به جنگ</p></div>
<div class="m2"><p>کار فرمای با کفایت نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این همه هست شکر ایزد را</p></div>
<div class="m2"><p>از چنین کارها شکایت نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه کنم من که مر شما را بیش</p></div>
<div class="m2"><p>هیچ اندیشه ولایت نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چنین عیب های عمر گذار</p></div>
<div class="m2"><p>غم و رنج مرا نهایت نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان شیرین خوشست و چون بشود</p></div>
<div class="m2"><p>از پس جان به جز حکایت نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همه قصه من همی گویم</p></div>
<div class="m2"><p>از زبان کسی روایت نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وین معونت که من همی خواهم</p></div>
<div class="m2"><p>دانم از جمله جنایت نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد ولایت صریح تر گفتم</p></div>
<div class="m2"><p>ظاهر است این سخن کنایت نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آیتی آمده درین به شما</p></div>
<div class="m2"><p>گر چه امروز وقت آیت نیست</p></div></div>