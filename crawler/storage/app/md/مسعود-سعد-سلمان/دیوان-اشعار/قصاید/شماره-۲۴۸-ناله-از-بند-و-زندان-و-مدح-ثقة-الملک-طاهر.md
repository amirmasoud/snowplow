---
title: >-
    شمارهٔ ۲۴۸ - ناله از بند و زندان و مدح ثقة الملک طاهر
---
# شمارهٔ ۲۴۸ - ناله از بند و زندان و مدح ثقة الملک طاهر

<div class="b" id="bn1"><div class="m1"><p>مقصور شد مصالح کار جهانیان</p></div>
<div class="m2"><p>بر حبس و بند این تن رنجور ناتوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حبس و بند نیز ندارندم استوار</p></div>
<div class="m2"><p>تا گرد من نباشد ده تن نگاهبان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر ده نشسته بر در و بر بام سمج من</p></div>
<div class="m2"><p>با یکدیگر دمادم گویند هر زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیزید و بنگرید مبادا به جادویی</p></div>
<div class="m2"><p>او از شکاف روزن پرد بر آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هین بر جهید زود که حیلت گریست این</p></div>
<div class="m2"><p>کز آفتاب پل کند از سایه نردبان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>البته هیچ کس به نیندیشد این سخن</p></div>
<div class="m2"><p>کاین شاعر مخنث خود کیست در جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون بگذرد ز روزن و چون بر پرد ز سمج</p></div>
<div class="m2"><p>نه مرغ و موش گشتست این خام قلبتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با این دل شکسته و با دیده ضعیف</p></div>
<div class="m2"><p>سمجی چنین نهفته و بندی چنین گران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از من همی هراسند آنان که سالها</p></div>
<div class="m2"><p>زایشان همی هراسد در کار جنگوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گیرم که ساخته شوم از بهر کارزار</p></div>
<div class="m2"><p>بیرون شوم ز گوشه این سمج ناگهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باچند کس برآیم در قلعه گرچه من</p></div>
<div class="m2"><p>شیری شوم دژآگه و پیلی شوم دمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس بی سلاح جنگ چگونه کنم مگر</p></div>
<div class="m2"><p>مر سینه را سپر کنم و پشت را کمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زیرا که سخت گشته ست از رنج انده این</p></div>
<div class="m2"><p>چونان که چفته گشته ست از بار محنت آن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دانم که کس نگردد از بیم گرد من</p></div>
<div class="m2"><p>زینگونه شیرمردی من چون شود عیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جانم ز رنج و محنتشان در شکنجه است</p></div>
<div class="m2"><p>یارب ز رنج و محنت بازم رهان به جان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در حال خوب گردد حال من ار شود</p></div>
<div class="m2"><p>بر حال من دل ثقت الملک مهربان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خورشید سرکشان جهان طاهر علی</p></div>
<div class="m2"><p>آن چرخ با جلالت و آن بحر بی کران</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای آن جوان که چون تو ندیدست چرخ پیر</p></div>
<div class="m2"><p>یارست رای پیر تو را دولت جوان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم کوفسون مهر تو بر خویشتن دمد</p></div>
<div class="m2"><p>ز آهنش ضیمران دمد از خار ارغوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با جوش حشمت تو چه صحرا چه کوهسار</p></div>
<div class="m2"><p>با زخم خنجر تو چه سندان چه پرنیان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دارد سپهر خوانده مهر تو را بناز</p></div>
<div class="m2"><p>ندهد زمانه رانده کین تو را امان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بالای رتبت تو گذشته ز هر فلک</p></div>
<div class="m2"><p>پهنای بسطت تو رسیده به هر مکان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یک ماهه دولت تو نگشته ست هیچ چرخ</p></div>
<div class="m2"><p>یک روزه بخشش تو ندیدست هیچ کان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرید همی نیاز جهان بر عطای تو</p></div>
<div class="m2"><p>خندد همی عطای تو بر گنج شایگان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه چرخ را خلاف تو کاری همی رود</p></div>
<div class="m2"><p>نه ملک را ز رأی تو رازی بود نهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیوسته طیره و خجل است ابر و آفتاب</p></div>
<div class="m2"><p>زان لفظ درفشان تو و دست زرفشان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جاه تو را سعادت چون روز را ضیا</p></div>
<div class="m2"><p>عزم تو را کفایت چون تیغ را فسان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر نه ز بهر نعمت بودی بدان درست</p></div>
<div class="m2"><p>از فصل های سال نبودی تو را خزان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از بهر دیده و دل بد خواه تو فلک</p></div>
<div class="m2"><p>سازد همی حسام و فرازد همی سنان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بیمت چو تیغ سر بزند دشمن تو را</p></div>
<div class="m2"><p>گر چون قلم نبندد پیشت میان به جان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از تو قرین نصرت و اقبال و دولتست</p></div>
<div class="m2"><p>ملک علای دولت و دین صاحب قران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>والله که چشم چرخ جهاندیده هیچ وقت</p></div>
<div class="m2"><p>نه چون تو بنده دید و نه چون او خدایگان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای بر هوات خلق همه سود کرده من</p></div>
<div class="m2"><p>بر مایه هوات چرا کرده ام زیان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اندر ولوع خدمت خویش اعتقاد من</p></div>
<div class="m2"><p>دانی همه و داند یزدان غیب دان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون بلبلان نوای ثناهای تو زدم</p></div>
<div class="m2"><p>تا کرد روزگار مرا اندر آشیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن روی و قد بوده چو گلنار و ناردان</p></div>
<div class="m2"><p>با رنگ زعفران شده با ضعف خیزران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اندر تنم ز سرما بفسرده خون تن</p></div>
<div class="m2"><p>بگداخت بازم آتش دل مغز استخوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آگنده دل چو نار ز تیمار و هر دو رخ</p></div>
<div class="m2"><p>گشته چو نار کفته و اشکم چو ناردان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا مر مرا دو حلقه بندست بر دو پای</p></div>
<div class="m2"><p>هستم دو دیده گویی از خون دو ناودان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بندم همی چه باید کامروز مر مرا</p></div>
<div class="m2"><p>بسته شود دو پای به یک تار ریسمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون تار پرنیان تنم از لاغری و من</p></div>
<div class="m2"><p>مانم همی به صورت بی جان پرنیان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چندان دروغ گفت نشاید که شکر هست</p></div>
<div class="m2"><p>از روی مهربانی نز روی سوزیان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در هیچ وقت بی شفقت نیست گوتوال</p></div>
<div class="m2"><p>هر شب کند زیادت بر من دو پاسبان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گوید نگاهبانم گر بر شوی به بام</p></div>
<div class="m2"><p>در چشم کاهت افتد از راه کهکشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در سمج من دکانی چون یک بدست نیست</p></div>
<div class="m2"><p>نگذاردم که هیچ نشینم بر آن دکان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این حق بگو چگونه توانم گزاردن</p></div>
<div class="m2"><p>کاین خدمتم کنند همیدون به رایگان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غبنا و اندها که مرا چرخ دزدوار</p></div>
<div class="m2"><p>بی آلت سلاح بزد راه کاروان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چون دولتی نمود مرا محنتی فزود</p></div>
<div class="m2"><p>بی گردن ای شگفت نبوده ست گردران</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من راست خود بگویم چون راست هیچ نیست</p></div>
<div class="m2"><p>خود راستی نهفتن هرگز کجا توان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بودم چنانکه سخت به اندام کارها</p></div>
<div class="m2"><p>راندم همی به دولت سلطان کامران</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر کوه رزم کردم و در بیشه صف درید</p></div>
<div class="m2"><p>در حمله بر نتافتم از هیچ کس عنان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر هفت روز کردم جنگی به هفت جای</p></div>
<div class="m2"><p>در قصها نخواندم جز جنگ هفتخوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اقبال شاه بود و جوانی و بخت نیک</p></div>
<div class="m2"><p>امروز هر چه بود همه شد خلاف آن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>در روزگار جستم تا پیش من بجست</p></div>
<div class="m2"><p>در روزگار جستن کاریست کالامان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گردون هزار کان ستد از من به جور و قهر</p></div>
<div class="m2"><p>هرچ آن به زور یافته بودم یکان یکان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>اکنون درین مرنجم در سمج بسته در</p></div>
<div class="m2"><p>بر بند خود نشسته چو بر بیضه ماکیان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رفتن مرا ز بند به زانوست یا به دست</p></div>
<div class="m2"><p>خفتن چه حلقه هاش نگونست یا سنان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در یکدرم ز زندان با آهنی سه من</p></div>
<div class="m2"><p>هر شام و چاشت باشم در یوبه دونان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سکباجم آرزو کند و نیست آتشی</p></div>
<div class="m2"><p>جز چهره به زردی مانند زعفران</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه نه نه راست گفتم کز بر وجود تو</p></div>
<div class="m2"><p>در سبز مرغزارم و در تازه بوستان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خواهم همی که دانم با تو به هیچ وقت</p></div>
<div class="m2"><p>گویی همی دریغ که باطل شود فلان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آری به دل که همچو دگر بندگان نیک</p></div>
<div class="m2"><p>مسعود سعد خدمت من کرد سالیان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>این گنبد کیان که بدینگونه بی گناه</p></div>
<div class="m2"><p>بر کند و بر کشفت مرا بیخ و خانمان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>معذور دارمش که شکایت مرا ز تست</p></div>
<div class="m2"><p>نه بود و هست بنده تو گنبد کیان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ور روزگار کرد نه او هم غلام تست</p></div>
<div class="m2"><p>از بهر من بگوی مر او را که هان و هان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مسعود سعد بنده سی ساله منست</p></div>
<div class="m2"><p>تو نیز بنده منی این قدر را بدان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کان کس که بندگی کندم کی رضا دهم</p></div>
<div class="m2"><p>کو را به عمر محنتی افتد به هیچ سان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای داده جاه تو به همه دولتی نوید</p></div>
<div class="m2"><p>ای کرده جود تو به همه نهمتی ضمان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>در پارسی و تازی در نظم و نثر کس</p></div>
<div class="m2"><p>چون من نشان نیارد گویا و ترجمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>پر گنج و پر خزینه دانش ندیده اند</p></div>
<div class="m2"><p>چون طبع و خاطر من گنجور و قهرمان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>آنک که بانگ من چو به گوش سخن رسد</p></div>
<div class="m2"><p>اندر تن فصاحت گردد روان روان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>من در شب سیاهم نام من آفتاب</p></div>
<div class="m2"><p>من در مرنجم و سخن من به قیروان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>جز من که گفت خواهد در خورد تو ثنا</p></div>
<div class="m2"><p>جز تو که را رسد به بزرگی من گمان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>آرایشی بود به ستایشگری چو من</p></div>
<div class="m2"><p>در بزم و مجلس تو به نوروز و مهرگان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای آفتاب روشن تابان روزگار</p></div>
<div class="m2"><p>کردست روزگار مرا دایم امتحان</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گر چه ز هیچ جنس ندیدم من این عنا</p></div>
<div class="m2"><p>نه هیچ وقت خوانده ام از هیچ داستان</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>معزول نیست طبع من از نظم گر چه هست</p></div>
<div class="m2"><p>معزولم از نبشتن این گفتها بنان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خود نیست بر قلمدان دست مرا سبیل</p></div>
<div class="m2"><p>باری مرا اجازت باشد به دوکدان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تا دولتست و بخت که دلها از آن و این</p></div>
<div class="m2"><p>همواره تازه باشد و پیوسته شادمان</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>هر ساعتی ز دولت شمعی دگر فروز</p></div>
<div class="m2"><p>هر لحظه ای ز بخت نهالی دگر نشان</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تا فرخی بپاید در فرخی بپای</p></div>
<div class="m2"><p>تا خرمی بماند در خرمی بمان</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>از هر چه خواستند به دادی تو داد خلق</p></div>
<div class="m2"><p>اکنون تو داد خلق ز دولت همی ستان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بنیوش قصه من و آنگه کریم وار</p></div>
<div class="m2"><p>بخشایش آر بر من بدبخت گم نشان</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تا شکر گویمت ز دماغی همه خرد</p></div>
<div class="m2"><p>تا مدح خوانمت به زبانی همه بیان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چون شکر من تو نشنوی از هیچ شکر گو</p></div>
<div class="m2"><p>چون مدح من تو نشنوی از هیچ مدح خوان</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>تا در دهان زبان بودم در زبان مرا</p></div>
<div class="m2"><p>آرم زبان به شکر و ثنای تو در دهان</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>وانگه که بی ثنای تو باشد زبان من</p></div>
<div class="m2"><p>اندر دهان چه فایده دارد مرا زبان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ای باد نوبهاری وی مشکبوی باد</p></div>
<div class="m2"><p>این مدح من بگیر و بدان پیشگه رسان</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بوالفتح راوی آنکه چو او نیست این مدیح</p></div>
<div class="m2"><p>یا در سراش خواند یا نه به وقت خوان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>دانم که چون بخواند احسنت ها کنند</p></div>
<div class="m2"><p>قاضی خوش حکایت و لؤلؤی ساربان</p></div></div>