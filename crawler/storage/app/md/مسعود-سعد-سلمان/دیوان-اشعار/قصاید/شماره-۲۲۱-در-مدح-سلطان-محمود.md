---
title: >-
    شمارهٔ ۲۲۱ - در مدح سلطان محمود
---
# شمارهٔ ۲۲۱ - در مدح سلطان محمود

<div class="b" id="bn1"><div class="m1"><p>شب دراز و ره دور و غربت و احزان</p></div>
<div class="m2"><p>چگونه ماند تن یا چگونه ماند جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسان مردم بی هوش گشته زار و نزار</p></div>
<div class="m2"><p>دلم ز درد غریبی تن از غم بهتان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا دو دیده به سیر ستارگان مانده</p></div>
<div class="m2"><p>که کی برآید مه کی فرو شود سرطان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنات نعش بگیرد ز هفت کوکب بیم</p></div>
<div class="m2"><p>که باشد از سپری لاجوردگون تابان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهی دراز و درو جای جای یخ بسته</p></div>
<div class="m2"><p>درین دو خاک به کردار را کاهکشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ز سودا دل در هزار گونه هوس</p></div>
<div class="m2"><p>به کار خویش فرومانده عاجز و حیران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز روی گنبد خضرا نهان شده پروین</p></div>
<div class="m2"><p>مه چهارده تابان شده ز چرخ کیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو روی خسرو محمود سیف دولت و دین</p></div>
<div class="m2"><p>که افتخار زمین است و اختیار زمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مظفری ملکی خسروی خداوندی</p></div>
<div class="m2"><p>که جاه و قدرش بگذشته است از کیوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهی که هند شد از فر او بسان بهشت</p></div>
<div class="m2"><p>چو روی داد ز غزنین به سوی هندستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایگانا دانی که بنده تو چه کرد</p></div>
<div class="m2"><p>به شهر غزنین با شاعران چیره زبان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر آن قصیده که گفتیش را شدی یک ماه</p></div>
<div class="m2"><p>جواب گفتم زان بر بدیهه هم بزمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نه بیم تو بودی شها به حق خدای</p></div>
<div class="m2"><p>که راشدی را بفکندمی ز نام و نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر دو تن را جنگ اوفتادی اندر شعر</p></div>
<div class="m2"><p>ز شعر بنده بدیشان شواهد و برهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی به دیگر گفتی که این درست نبود</p></div>
<div class="m2"><p>اگر بگوید مسعود سعد بن سلمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو پایگاهم دیدند نزد شاهنشه</p></div>
<div class="m2"><p>که داشتم بر او جاه و رتبت و امکان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به پیش شاه نهادند مر مرا تهمت</p></div>
<div class="m2"><p>به صد هزاران نیرنگ و حیلت و دستان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگر ز پایگه خود بیفکنند مرا</p></div>
<div class="m2"><p>به پیش همه شه سود مرا کنند زیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو من جریده اشعار خویش عرضه کنم</p></div>
<div class="m2"><p>نخست یابم نام تو بر سر دیوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سزد که نام من این نامدار ثبت کنی؟</p></div>
<div class="m2"><p>به ملک غفلت در متن دفتر نیسان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا مدار به طبع و هنر گران و سبک</p></div>
<div class="m2"><p>که من به مایه سبک نیستم به طبع گران</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همیشه تا به جهان سالی و تهی نبود</p></div>
<div class="m2"><p>جواهر از اعراض و عناصر از الوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو حال نیک و بد آید همی ز سمت ملک</p></div>
<div class="m2"><p>بهفت کوکب و از پنج و حس چار ارکان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو سرو و لاله بناز و چو صبح و باغ بخند</p></div>
<div class="m2"><p>چو ماه و مهر بتاب و چو عقل و روح بمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خجسته دولت و فرخنده بخت تو هر سال</p></div>
<div class="m2"><p>چو آفتاب منیر و چو نوبهار جوان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخر مرا و نکویم بدار زیرا من</p></div>
<div class="m2"><p>بهر نکویی حقم به هر بها ارزان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه بادی در ملک بی کرانه عزیز</p></div>
<div class="m2"><p>همیشه بادی از بخت جاودان شادان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نشاط کن ملکا بر سماع نای علی</p></div>
<div class="m2"><p>نبید رنگین خور بر کنار آب روان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنانکه چرخ بپاید تو همچو چرخ بپای</p></div>
<div class="m2"><p>چنانکه کوه بماند تو همچو کوه بمان</p></div></div>