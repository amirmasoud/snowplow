---
title: >-
    شمارهٔ ۵۷ - از وزیر بهروز بن احمد یاری خواهد
---
# شمارهٔ ۵۷ - از وزیر بهروز بن احمد یاری خواهد

<div class="b" id="bn1"><div class="m1"><p>بهروزبن احمد که وزیر الوزرا شد</p></div>
<div class="m2"><p>بشکفت وزارت که سزا جفت سزا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رای چو خورشیدش بر ملک و ملک تافت</p></div>
<div class="m2"><p>هر رای که بر روی زمین بود هبا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چون فلک عالی بر صحن جهان گشت</p></div>
<div class="m2"><p>آفاق جلالت همه پر نور و ضیا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با رتبت او پایه افلاک زمین گشت</p></div>
<div class="m2"><p>با همت او چشمه خورشید سها شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقبال و سعادت را آن مجلس و آن دست</p></div>
<div class="m2"><p>روینده زمین آمد و بارنده سما شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قافله زایر آن درگه سامیش</p></div>
<div class="m2"><p>کعبه ست که مأوای مناجات و دعا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا گشت خریدار هنر رای بلندش</p></div>
<div class="m2"><p>بازار هنرمندان یکباره روا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه ره تقدیر و قضا هرگز نسپرد</p></div>
<div class="m2"><p>تا فکرت او پرده تقدیر و قضا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون بنده شدش دولت و اقرار همی کرد</p></div>
<div class="m2"><p>آخر خرد روشن و بشنید و گوا شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشمنش که بگریخت ز چنگال نهیبش</p></div>
<div class="m2"><p>صد شکر همی کرد که در دام بلا شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای آنکه به اقبال تو در باغ وزارت</p></div>
<div class="m2"><p>هر شاخ که سر برزد با نشو و نما شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا رحمت و انصاف تو در دولت پیوست</p></div>
<div class="m2"><p>گیتی همه از صاعقه ظلم جدا شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ایام تو در شاهی تاریخ هنر گشت</p></div>
<div class="m2"><p>آثار تو در دانش فهرست دعا شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس عاجز و درمانده و بس کوفته چون من</p></div>
<div class="m2"><p>کز چنگ بلا زود به فر تو رها شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دانند که در خدمت سلطان جهاندار</p></div>
<div class="m2"><p>تا گشت زبانم به ثنا وقف ثنا شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زانجای که از آن تاخته بودیم به تعجیل</p></div>
<div class="m2"><p>زیرا که همه حاجب زین جای روا شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ظنی که بیاراسته بودیم تبه گشت</p></div>
<div class="m2"><p>تیری که بینداخته بودیم خطا شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر دیده من جست همی تابش خورشید</p></div>
<div class="m2"><p>روزم چو شب تاری تاریک چرا شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گیرم که گنه کردم والله که نکردم</p></div>
<div class="m2"><p>عفوی که خداوندان کردند کجا شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دارم به تو امید و وفا گرددم آخر</p></div>
<div class="m2"><p>کامید همه خلق جهان از تو روا شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر راست رود تیر امیدم نه شگفت است</p></div>
<div class="m2"><p>زیرا چو کمان قامتم از رنج دو تا شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مدحی چو شکوفه نه شکفت است ز طبعم</p></div>
<div class="m2"><p>اکنون که تن از خواری همجنس گیا شد</p></div></div>