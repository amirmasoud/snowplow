---
title: >-
    شمارهٔ ۲۸۸ - مدیح سلطان ابراهیم بن مسعود
---
# شمارهٔ ۲۸۸ - مدیح سلطان ابراهیم بن مسعود

<div class="b" id="bn1"><div class="m1"><p>ز فردوس با زینت آمد بهاری</p></div>
<div class="m2"><p>چو زیبا عروسی و تازه نگاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگسترده بر کوه و بر دشت فرشی</p></div>
<div class="m2"><p>کش از سبزه پو دست وز لاله تاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوهر بپیراست هر بوستانی</p></div>
<div class="m2"><p>به دیبا بیاراست هر مرغزاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتی کرد هر گلبنی را و شاید</p></div>
<div class="m2"><p>که هر گلستانیست چون قندهاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برافکند بر دوش این طیلسانی</p></div>
<div class="m2"><p>در آویخت در گوش آن گوشواری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میی خواه بویا چو رنگین عقیقی</p></div>
<div class="m2"><p>بتی خواه زیبا چو خرم بهاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه کارها را نیامیز بر هم</p></div>
<div class="m2"><p>ز هر پیشکاری همی خواه کاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز مطرب نوایی ز ساقی نبیدی</p></div>
<div class="m2"><p>ز معشوق بوسی ز دلبر کناری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمینی است چون صورت دلفروزی</p></div>
<div class="m2"><p>هواییست چون سیرت بردباری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز روی تذروان زمین را بساطی</p></div>
<div class="m2"><p>ز پشت کلنگان هوا را بخاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چرخ دارد ز هر گونه چیزی</p></div>
<div class="m2"><p>که شاید نمودن بدان افتخاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز شاهان گیتی به گیتی ندارد</p></div>
<div class="m2"><p>چو خسرو براهیم مسعود باری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهان شهریاری که در شهریاری</p></div>
<div class="m2"><p>زمانه ندارد چنو شهریاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو او کامگاری که از کامگاران</p></div>
<div class="m2"><p>نشد چیره بر کام او کامگاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر جود او آب دریا سرابی</p></div>
<div class="m2"><p>بر قدر او چرخ گردان غباری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ثواب و عقابش به میدان و ایوان</p></div>
<div class="m2"><p>فروزنده نوری و سوزنده ناری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان آتشین تیغ در هر نبردی</p></div>
<div class="m2"><p>گرفته ست هر خسروی را عیاری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به شمشیر داده قوی گوشمالی</p></div>
<div class="m2"><p>شهان جهان را به هر کار زاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برآورده گردی ز هر تند کوهی</p></div>
<div class="m2"><p>فرو رانده سیلی به هر ژرف غاری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه با رای او اختران را فروغی</p></div>
<div class="m2"><p>نه با گنج او کوهها را یساری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهاندار شاها جهان را به شاهی</p></div>
<div class="m2"><p>نکردست گردون چو تو اختیاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نبودست چون امر و نهی تو هرگز</p></div>
<div class="m2"><p>زمانه نوردی و گیتی گذاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ندادت گلی چرخ هرگز فراکف</p></div>
<div class="m2"><p>که نه در دل دشمنت خست خاری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ازینسان برآید همه کام نهمت</p></div>
<div class="m2"><p>کرا بود چون دولت آموزگاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شه روزگاری و چون روزگارت</p></div>
<div class="m2"><p>ندیدست کس ملک را روزگاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر ملک را یادگاری بباید</p></div>
<div class="m2"><p>بیابد هم از ملک تو یادگاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی تا بود کوکبی را شعاعی</p></div>
<div class="m2"><p>همی تا بود آتشی را شراری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همی دیده ای بر گشاید گیایی</p></div>
<div class="m2"><p>همی پنجه ای برفرازد چناری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روان باد حکم تو بر هر سپهری</p></div>
<div class="m2"><p>رسان باد امر تو در هر دیاری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گهت گوش بر نغمه رود سازی</p></div>
<div class="m2"><p>گهت چشم بر صورت میگساری</p></div></div>