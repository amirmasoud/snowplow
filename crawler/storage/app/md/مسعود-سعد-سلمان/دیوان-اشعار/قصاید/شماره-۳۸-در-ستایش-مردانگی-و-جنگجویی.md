---
title: >-
    شمارهٔ ۳۸ - در ستایش مردانگی و جنگجویی
---
# شمارهٔ ۳۸ - در ستایش مردانگی و جنگجویی

<div class="b" id="bn1"><div class="m1"><p>تا توانی مکش ز مردی دست</p></div>
<div class="m2"><p>که به سستی کسی ز مرگ نجست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی ار شست نگسلد در آب</p></div>
<div class="m2"><p>بسته او را به خشکی آرد شست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او را بلند مردی کرد</p></div>
<div class="m2"><p>تا به روز اجل نگردد پست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی ننمود خوب در مجلس</p></div>
<div class="m2"><p>تا ندیدند در مصافش شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که با جان نایستاد به رزم</p></div>
<div class="m2"><p>دان که در پیشگه به حق ننشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر فرازد چو نیزه هر مردی</p></div>
<div class="m2"><p>که میان جنگ را چو نیزه ببست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بسا رزمگاه چون دوزخ</p></div>
<div class="m2"><p>که قضا اندر او درست نرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل مردان ز ترس چون دل طفل</p></div>
<div class="m2"><p>سر گردان ز حمله چون سرمست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرخ گردان ز گرد کان چو شبه</p></div>
<div class="m2"><p>تیغ بران ز خون چو شاخ کبست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیزه چون حمله خواستم بردن</p></div>
<div class="m2"><p>گشت پیچان مرا چو مار به دست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم این شاخ مرگ راست گرای</p></div>
<div class="m2"><p>که بسی دل به خواهم خست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنی ار احتراز وقتش نیست</p></div>
<div class="m2"><p>ور کنی اضطراب جایش هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا بجنبی همی ز شادی خون</p></div>
<div class="m2"><p>یا بلرزی همی ز بیم شکست</p></div></div>