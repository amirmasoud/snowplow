---
title: >-
    شمارهٔ ۳۱۰ - مدح منصور بن سعید
---
# شمارهٔ ۳۱۰ - مدح منصور بن سعید

<div class="b" id="bn1"><div class="m1"><p>ای ابر گه بگریی و گه خندی</p></div>
<div class="m2"><p>کس داندت چگونه ای و چندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که قطره ای ز تو بچکد گاهی</p></div>
<div class="m2"><p>باران شوی چه نادره آوندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنداخت بحر آنچه تو برچیدی</p></div>
<div class="m2"><p>بگزید خاک آنچه تو بفکندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کوهی و به گونه دریایی</p></div>
<div class="m2"><p>بر بحری و به شکل دماوندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی به بانگ رعد همی نالم</p></div>
<div class="m2"><p>گاهی به نور برق همی خندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چشم و دیده لؤلؤ بگشایی</p></div>
<div class="m2"><p>بر دست و پای گلبن بر بندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از در همه کنار تهی کردی</p></div>
<div class="m2"><p>تا خوشه را به دانه بیاکندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخشیدن از تو نیست عجب ایرا</p></div>
<div class="m2"><p>دریای بی کران را فرزندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار چون به غزنین بگذشتی</p></div>
<div class="m2"><p>لؤلؤ بدان دیار پراکندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیغام می دهمت بگو زنهار</p></div>
<div class="m2"><p>از این حزین تنگدل بندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با تاج سروران همه حضرت</p></div>
<div class="m2"><p>خواجه عمید حضرت میمندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منصور بن سعید خداوندی</p></div>
<div class="m2"><p>کز فر اوست تازه خداوندی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای چون خرد تنت به خرد ورزی</p></div>
<div class="m2"><p>وی چون هنر دلت به هنرمندی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>افلاک را به رتبت هم جنسی</p></div>
<div class="m2"><p>اقبال را به رادی مانندی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برد از نیاز همت تو قوت</p></div>
<div class="m2"><p>برد از کبست جود تو خرسندی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از هر هنر جهان را تمثالی</p></div>
<div class="m2"><p>وز هر مهم فلک را سوگندی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاخ سخا و رادی بنشاندی</p></div>
<div class="m2"><p>بیخ نیاز و زفتی برکندی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو حاتم زمانه و من چونین</p></div>
<div class="m2"><p>درمانده نیاز تو نپسندی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کارم ببست چونکه نبگشایی</p></div>
<div class="m2"><p>جانم گسست چونکه نپیوندی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گویم ببین همی که غنی گردی</p></div>
<div class="m2"><p>بپذیر پند اگر ز در پندی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زانچ از دو دیده بر رخ بفشاندی</p></div>
<div class="m2"><p>وانچ از دو رخ ز دیده فرو راندی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فردا مگر ز من بنیابی تو</p></div>
<div class="m2"><p>امروز آنچه یافتی از من دی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای آنکه از سمامه و خورشیدی</p></div>
<div class="m2"><p>از جود و خلق شکری و قندی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلشاد زی بدانکه بود او را</p></div>
<div class="m2"><p>لب قند و روی سیب سمرقندی</p></div></div>