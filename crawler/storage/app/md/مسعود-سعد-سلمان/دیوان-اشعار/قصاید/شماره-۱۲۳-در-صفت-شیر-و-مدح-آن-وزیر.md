---
title: >-
    شمارهٔ ۱۲۳ - در صفت شیر و مدح آن وزیر
---
# شمارهٔ ۱۲۳ - در صفت شیر و مدح آن وزیر

<div class="b" id="bn1"><div class="m1"><p>بگشاد خون ز چشم من آن یار سیم بر</p></div>
<div class="m2"><p>چون بر بسیج رفتن بستم همی کمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او آفتاب و همچو مطر اشکش و مرا</p></div>
<div class="m2"><p>در آفتاب نادره آمد همی مطر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه روی تافت گاه ببوسید روی من</p></div>
<div class="m2"><p>گه بر بکند و گاه گرفت او مرا به بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه گفت اگر توانی ایدر مقام کن</p></div>
<div class="m2"><p>گه گفت اگر توانی با خود مرا ببر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که حاجتم به تو افزون کنون از آنک</p></div>
<div class="m2"><p>حاجت فزون بود به مه ای ماه در سفر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه نوگلی و شکر دانم که چاره نیست</p></div>
<div class="m2"><p>از آفتاب و باران کس را به راه در</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترسم ز آفتاب فرو پژمری چو گل</p></div>
<div class="m2"><p>بگدازی ای نگار ز باران تو چون شکر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و ایدر مقام کردن دانی که چاره نیست</p></div>
<div class="m2"><p>چون داد روی سوی سفر نازش بشر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدرود کردم او را وز وی جدا شدم</p></div>
<div class="m2"><p>در پیش برگرفتم راهی پر از عبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بیشه ای فتادم کاندر زمین او</p></div>
<div class="m2"><p>مالیده خون جانوران و بریده سر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه ز انبهی تواند آمد به گوش بانگ</p></div>
<div class="m2"><p>نه ز دیدگان تواند رفتن برون نظر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون سرگذشت مجنون پر فتنه و بلا</p></div>
<div class="m2"><p>چون داستان وامق پرآفت و خطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان آمدم شگفت که از بس بلا و شور</p></div>
<div class="m2"><p>در وی چگونه یارد رستن همی شجر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد بسته مرکبان را دم از برای آن</p></div>
<div class="m2"><p>کامد به گوش ایشان آواز شیر نر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آمد برون ز بیشه یکی زرد و سرخ چشم</p></div>
<div class="m2"><p>لاغر میان و اندک دنبال و پهن سر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رویش چراست زرد نترسیده او ز کس</p></div>
<div class="m2"><p>چشمش چراست سرخ نبودش شبی سهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می جست همچو تیر و دو چشمش همی نمود</p></div>
<div class="m2"><p>مانند کوکب سپر از روی چون سپر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مانند آفتاب همی رفت و بر زمین</p></div>
<div class="m2"><p>همچون مجره پیدا از پنجه هاش اثر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از سهم روی و بانگ نخیز و گریز او</p></div>
<div class="m2"><p>هر زنده چشم و گوش همی داشت کور و کر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنجا که قصد کرد بسان قضاش دید</p></div>
<div class="m2"><p>وانچش مراد بود بیامدش چون قدر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آتش نهاد و خیره بود در میان آب</p></div>
<div class="m2"><p>خورشید رنگ و تیره از او جان جانور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ماننده خور است همیشه به طبع گرم</p></div>
<div class="m2"><p>آری شگفت می نبود گرم طبع خور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از بهر چیست تارک جوشان و ترش روی</p></div>
<div class="m2"><p>چون یافته است دانم بر جانور ظفر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در سهم و جنگ داند رفتن همی چو تیر</p></div>
<div class="m2"><p>وز بد چو تیغ کرد نداند همی حذر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هست او قوی دل و جگرآور ز بهر آنک</p></div>
<div class="m2"><p>باشد طعام او همه ساله دل و جگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گشت او دلیر و نامور از بهر آنکه او</p></div>
<div class="m2"><p>بسیار برد جان دلیران نامور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خورشید رنگ و فعل شهابست بهر آنک</p></div>
<div class="m2"><p>در مرغزار چون فلک او را بود ممر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفتم که یارب او را بگمار و چیره کن</p></div>
<div class="m2"><p>بر دشمنان صاحب کافی پر هنر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>منصوربن سعید بن احمد که در جهان</p></div>
<div class="m2"><p>چون فضل نامور شد و چون جود مشتهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر طول و عرض همت او را داردی سپهر</p></div>
<div class="m2"><p>خورشید کی رسیدی هرگز به باختر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ور آفتاب بودی چون مهر او به فعل</p></div>
<div class="m2"><p>جز جانور نبودی در سنگ ها گهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ای مدحتت به دانش چون طبع رهنمای</p></div>
<div class="m2"><p>وی خدمتت به دولت چون بخت راهبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جز خدمت تو خدمت کردن بود ریا</p></div>
<div class="m2"><p>جز مدحت تو مدحت گفتن بود هدر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جودت به خاص و عام رسیده چو آفتاب</p></div>
<div class="m2"><p>فضلت چو روزگار گرفته ست بحر و بر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چرخی و از تو باشد چون چرخ نیک و بد</p></div>
<div class="m2"><p>بحری و از تو خیزد چون بحر نفع و ضر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>با رتبت تو گردون بی قدر چون زمین</p></div>
<div class="m2"><p>با هیبت تو آتش بی تاب چون شرر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در جسم ها هوای بقای تو چون روان</p></div>
<div class="m2"><p>در چشم ها جمال لقای تو چون بصر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من مدحت تو گفت ندانم همی تمام</p></div>
<div class="m2"><p>مانند تو تویی و سخن گشت مختصر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>معشوق تا چو زر ز کف من جدا شده ست</p></div>
<div class="m2"><p>او را همی بجویم در خاک همچو زر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از فضل خویش دایم رنجور مانده ام</p></div>
<div class="m2"><p>شاخ درخت رنج بود دایم از ثمر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یک همت تو حاصل گرداندم همم</p></div>
<div class="m2"><p>یک فکرت تو زایل گرداندم فکر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از آتش فراق دل آتشکده شده ست</p></div>
<div class="m2"><p>وز آب این دو دیده کنارم همی شمر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از بس سمر که گفته ام اندر فراق دوست</p></div>
<div class="m2"><p>همچون فراق گشته ام اندر جهان سمر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چون مهرباد روز بقای تو بی ظلام</p></div>
<div class="m2"><p>چون چرخ باد ساعت عمر تو بی عبر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ماه تو با جلالت و عز تو با ثبات</p></div>
<div class="m2"><p>عمر تو با سعادت و عیش تو با بطر</p></div></div>