---
title: >-
    شمارهٔ ۲۱۵ - ستایش پادشاه
---
# شمارهٔ ۲۱۵ - ستایش پادشاه

<div class="b" id="bn1"><div class="m1"><p>تو را بشارت باد ای خدایگان عجم</p></div>
<div class="m2"><p>به جاه کسری و ملک قباد و دولت جم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیام داد مرا دولت خجسته به تو</p></div>
<div class="m2"><p>که ای دو دیده و جان شهنشه اعظم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را بشارت دادم به ملک هفت اقلیم</p></div>
<div class="m2"><p>که تیغ تیز تو خواهد گشادن این عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چین کنند به مدح تو خطبه بر منبر</p></div>
<div class="m2"><p>به مصر و بصره به نامت زنند زر و درم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شهر مکه به امرت روند سوی غزا</p></div>
<div class="m2"><p>به روم و زنگ به نامت کنند جامه عمل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان آدم شادان شد از تو شاه از آنک</p></div>
<div class="m2"><p>به چرخ بردی از قدر گوهر آدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چون تو شاه به آیین شدست کار جهان</p></div>
<div class="m2"><p>به چون تو خسرو روشن شدست چشم حشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرای ملکت محکم به تو شده عالی</p></div>
<div class="m2"><p>بنای دولت عالی به تو شده محکم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برنده تیغ تو آسان کننده دشوار</p></div>
<div class="m2"><p>رونده کلک تو پیدا کننده مبهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برد سنان تو از روی پادشاهی چین</p></div>
<div class="m2"><p>دهد حسام تو مر پشت کافری را خم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زده است بازوی تو در عنان دولت چنگ</p></div>
<div class="m2"><p>نهاده پای تو اندر رکاب ملک قدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شهریار تو باشی و پادشاه جهان</p></div>
<div class="m2"><p>ندید خواهد چشم زمانه روی ستم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان هند ببندی روان ز خون جیحون</p></div>
<div class="m2"><p>کنون که گردد تیغت میان هند حکم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شد فروزان خورشید روشن از مشرق</p></div>
<div class="m2"><p>کجا برآید از جایگاه تیره ظلم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تهی شود همه بیشه ز آهو و خرگوش</p></div>
<div class="m2"><p>چو از نشیب که از خود برون شود ضیغم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین ز خون عدو گردد احمر و اشقر</p></div>
<div class="m2"><p>چو کار زار تو گردد بر اشهب و ادهم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو تیز ناوک تو با کمان بپیوندد</p></div>
<div class="m2"><p>تن و روان مخالف جدا شوند از هم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آفتاب حسامت در آید از در هند</p></div>
<div class="m2"><p>ز خون نماند اندر تن عدوی تونم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون که تیغ تو مانند ابر خون بارد</p></div>
<div class="m2"><p>جهان سراسر گردد چون بوستان ارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر کجا که نهد روی رایت عالیت</p></div>
<div class="m2"><p>به دولت تو نیاید فتوح و دولت کم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شوند از آمد و رفتن مبارزان مانده</p></div>
<div class="m2"><p>ز فتحنامه نوشتن شود ستوه قلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خنجر ای ملک اکنون تو خسته ای دل کفر</p></div>
<div class="m2"><p>که کرده ای تو چه بسیار خسته را مرهم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جود باطل کردی سخاوت حاتم</p></div>
<div class="m2"><p>به تیغ باطل کردی شجاعت رستم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر آنکه جز رقم بندگی کشد بر خود</p></div>
<div class="m2"><p>برو کشد ز فنا دست روزگار رقم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان فلک را بر تارکش فرود آرد</p></div>
<div class="m2"><p>اگر برآرد جز بر مراد رای تو دم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه تا به جهان اندرون غم و شادیست</p></div>
<div class="m2"><p>تو شاد بادی و وانکو به تو نه شاد به غم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو پادشاه جهان و جهان به تو یاور</p></div>
<div class="m2"><p>ملوک عصر تو را بنده تو ولی نعم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه قدر تو عالی و بخت تو پیروز</p></div>
<div class="m2"><p>همیشه عمر تو افزون و جاه تو خرم</p></div></div>