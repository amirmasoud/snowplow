---
title: >-
    شمارهٔ ۵۶ - مدیح کمال الدوله سلطان شیرزاد
---
# شمارهٔ ۵۶ - مدیح کمال الدوله سلطان شیرزاد

<div class="b" id="bn1"><div class="m1"><p>ز بار نامه دولت بزرگی آمد سود</p></div>
<div class="m2"><p>بدین بشارت فرخنده شاد باید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمونه ای ز جلالت به دهر پیدا شد</p></div>
<div class="m2"><p>ستاره ای ز سعادت به خلق روی نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به باغ دولت و اقبال شاخ شادی رست</p></div>
<div class="m2"><p>که مملکت را زو بار و سایه بینی زود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی به رمز چه گویم صریح خواهم گفت</p></div>
<div class="m2"><p>جهان ملک ملکی در جهان ملک افزود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر این سعادت لوهور خلعتی پوشید</p></div>
<div class="m2"><p>ز کامرانی تا روز شادمانی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بس نشاط که در طبع مردمان آویخت</p></div>
<div class="m2"><p>بدین دو هفته به شبها یک آدمی نغنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دوستکامی این باده ای بدان آورد</p></div>
<div class="m2"><p>به شادمانی آن دسته ای ازین بربود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشست شاه به سور و همیشه سورش باد</p></div>
<div class="m2"><p>بر مراد دل از کشته عزیز درود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد مصاف شکن شیرزاد شیر شکر</p></div>
<div class="m2"><p>که جان کفر به پولاد هندوی پالود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی به مرکب پوینده قعر بحر شکافت</p></div>
<div class="m2"><p>گهی به رایت بر رفته اوج چرخ بسود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر زمین که درآمد چو آب لشکر او</p></div>
<div class="m2"><p>ز تاب آتش شمشیر او برآمد دود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمود خون عدو بر کشیده خنجر او</p></div>
<div class="m2"><p>به گونه شفق سرخ بر سپهر کبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عریض جاهش پهنای هر دیار گرفت</p></div>
<div class="m2"><p>بلند قدرش بالای هر فلک پیمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدین نهاد که شوید همی جهان از کفر</p></div>
<div class="m2"><p>نماند خواهد بومی ز هند کفرآلود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شد سخاوت او بر زمانه مستولی</p></div>
<div class="m2"><p>نیاز کرد جهان را به درد دل بدرود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بر خزانه نبخشود و مالها بخشید</p></div>
<div class="m2"><p>نماند کس که بر آن کس ببایدش بخشود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بزرگ بارخدایا تو آن شهی که جهان</p></div>
<div class="m2"><p>جز آن نکرد که شاهانه همتت فرمود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک شناس نداند به راستیت شناخت</p></div>
<div class="m2"><p>ملک ستای نداند به واجبیت ستود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه چشم گردون چون کرده تو صورت دید</p></div>
<div class="m2"><p>نه گوش گیتی چون گفته تو لفظ شنود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل رعیت و چشم حشم به دولت تو</p></div>
<div class="m2"><p>به بزم و رزم تو بر شادی و نشاط آسود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز سور فرخ تو روی خرمی افروخت</p></div>
<div class="m2"><p>ز فتح شامل تو جان کافری فرسود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به رزمگاه تو بارنده ابر لؤلؤ ریخت</p></div>
<div class="m2"><p>به بزمگاه تو پوینده باد عنبر سود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به باغ لهو تو رامش چو ارغوان خندید</p></div>
<div class="m2"><p>ز شاخ مدح تو دولت چو عندلیب سرود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا شود از باغ دشت مشک آگین</p></div>
<div class="m2"><p>همیشه تا شود از مهر کوه زر اندود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بقات باد که امروز مایه دولت</p></div>
<div class="m2"><p>ز روزگار بقای تو را شناسد سود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمانه و فلکت رهنمای و یاری گر</p></div>
<div class="m2"><p>خدایگان و خدای از تو راضی و خشنود</p></div></div>