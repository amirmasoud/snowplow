---
title: >-
    شمارهٔ ۱۳۰ - مدح جمال الملک رشید
---
# شمارهٔ ۱۳۰ - مدح جمال الملک رشید

<div class="b" id="bn1"><div class="m1"><p>چون ببستم کمر به عزم سفر</p></div>
<div class="m2"><p>آگهی یافت سرو سیمین بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنجه و تافته به رسم وداع</p></div>
<div class="m2"><p>اندر آمد چو سرو و ماه از در</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه به فندق همی شخود سمن</p></div>
<div class="m2"><p>گه به لؤلؤ همی گزید شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مر مرا گفت ای عزیز رفیق</p></div>
<div class="m2"><p>همه با رنج و محنتی تو مگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تو بازیچه ای عجب کرده ست</p></div>
<div class="m2"><p>گردش این سپهر بازیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه سنگت کند همی بر کوه</p></div>
<div class="m2"><p>گاه بادت کند به صحرا بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه با دیو داردت هم رخت</p></div>
<div class="m2"><p>گاه با شیر داردت همبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه در حبس ها بداری پای</p></div>
<div class="m2"><p>گاه در دشت ها برآری پر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه یکایک به طبع بربندی</p></div>
<div class="m2"><p>از پی رزم همچو نیزه کمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه بجوشد بر تو در جوشن</p></div>
<div class="m2"><p>گه بتفسد سر تو در مغفر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای عجب لااله الاالله</p></div>
<div class="m2"><p>بخت باشد از این مخالف تر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیرم از من به عجز بشکیبی</p></div>
<div class="m2"><p>یا ندارد بر تو عشق خطر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خدمت مجلس جمال الملک</p></div>
<div class="m2"><p>چون توانی گذاشت نیک نگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مفخر و زینت زمانه رشید</p></div>
<div class="m2"><p>که نیارد چنو زمانه دگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه او را خدای عزوجل</p></div>
<div class="m2"><p>داد علم علی و عدل عمر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه آثار همتش بسته ست</p></div>
<div class="m2"><p>گردن دین و ملک را زیور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه با خلق او ندارد بوی</p></div>
<div class="m2"><p>نافه مشک و بیضه عنبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خرم از جود او بهار عطا</p></div>
<div class="m2"><p>روشن از عدل او جهان هنر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رای او را سها بود خورشید</p></div>
<div class="m2"><p>خشم او را شرر بود آذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر ندارد سخای کفش را</p></div>
<div class="m2"><p>بحر پر در و کان پر گوهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر نتابد نهیب بأسش را</p></div>
<div class="m2"><p>مرکز خاک و چنبر محور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مهر او کرد شکر از حنظل</p></div>
<div class="m2"><p>کین او ساخت حنظل از شکر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دهر با عزم او ندارد زور</p></div>
<div class="m2"><p>مهر با رای او ندارد فر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قدر او چرخ گشت و چرخ زمین</p></div>
<div class="m2"><p>طبع او بحر گشت و بحر شمر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به کمالش همی ببالد ملک</p></div>
<div class="m2"><p>تاب جودش همی بکاهد زر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جان او پیش جان خلق جهان</p></div>
<div class="m2"><p>گشته از تیر روزگار سپر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عدل شافی او به هر بقعه</p></div>
<div class="m2"><p>رای کافی او به هر کشور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هیبت او چو شیر وقت نخیز</p></div>
<div class="m2"><p>بسته بر نائبات راه گذر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ظلم را همچو باز دوخته چشم</p></div>
<div class="m2"><p>فتنه را همچو مار کوفته سر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای جهان را به مکرمت ضامن</p></div>
<div class="m2"><p>وی خرد را به راستی داور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باز گردون گوژپشت سپرد</p></div>
<div class="m2"><p>دل و جانم به انده بی مر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از قضا پیش من نهاد رهی</p></div>
<div class="m2"><p>که در او وهم کور گردد و کر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آب حوضش به طعم چون زقّوم</p></div>
<div class="m2"><p>برگ شاخش به شکل چون نشتر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من درین ره نهاده تن به قضا</p></div>
<div class="m2"><p>وز توکل سپرده دل به قدر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به سم باره باز خواهم کرد</p></div>
<div class="m2"><p>هر زمانی صحیفه های عبر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه شب بر ستاره خواهم بست</p></div>
<div class="m2"><p>به طلوع و غروب وهم و نظر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>راست مانند ابر و باد مرا</p></div>
<div class="m2"><p>رفت باید همی به بحر و به بر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از فراق هوای مجلس تو</p></div>
<div class="m2"><p>با لب خشک و با دو دیده تر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>رویم از گریه همچو روی زریر</p></div>
<div class="m2"><p>دلم از سوز چون دل مجمر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ژاله گشته سرشک من ز عنا</p></div>
<div class="m2"><p>لاله گشته دو چشم من ز سهر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از پی نور در شبان سیاه</p></div>
<div class="m2"><p>آرزومند طلعت تو بصر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مدح های تو حرز جان و تنم</p></div>
<div class="m2"><p>در بیابان و بیشه و کردر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ساخت خواهم ز نام تو تیغی</p></div>
<div class="m2"><p>از پی جنگ شیر شرزه نر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>راند خواهم ز گفته هات مثل</p></div>
<div class="m2"><p>گفت خواهم ز کردهات سمر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا ببینمت آفتاب نهاد</p></div>
<div class="m2"><p>اندر آن صدر آسمان پیکر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بود خواهم ز هجر تو همه روز</p></div>
<div class="m2"><p>بی قرار و نوان چو نیلوفر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دیده بی تو نخواهدم نعمت</p></div>
<div class="m2"><p>دست بی تو نگیردم ساغر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر من از فرقتت حرام بود</p></div>
<div class="m2"><p>ناله نای و نغمه مزمر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دوری بزم تو بخواهد بر</p></div>
<div class="m2"><p>زآتش طبع من فروغ و شرر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زنگ خواهد زد از جدایی تو</p></div>
<div class="m2"><p>خاطر آبدار چون خنجر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عز من بی تو بود خواهد ذل</p></div>
<div class="m2"><p>نفع من بی تو گشت خواهد ضر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بی توام شادیی نخواهد بود</p></div>
<div class="m2"><p>ای شگفتی که داردم باور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا همی باشدم به مدح و به شکر</p></div>
<div class="m2"><p>طبع و خاطر قوی و کاریگر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مدح های تو بارم از خامه</p></div>
<div class="m2"><p>شکرهای تو خوانم از دفتر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گر بدانجا کشد زمانه مرا</p></div>
<div class="m2"><p>که برو سودمند نیست حذر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>والله ار در جهان چو من یابی</p></div>
<div class="m2"><p>هیچ مداح و بنده و چاکر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا بتابد ز آسمان پروین</p></div>
<div class="m2"><p>تا بروید به بوستان عرعر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به جلالت عنان دولت گیر</p></div>
<div class="m2"><p>به سعادت بساط فخر سپر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دورها جشن های دولت بین</p></div>
<div class="m2"><p>قرن ها سالهای عمر شمر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بر تن تو ز خرمی کسوت</p></div>
<div class="m2"><p>بر سر تو ز فرخی افسر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گشته گردون به حلم تو گردان</p></div>
<div class="m2"><p>داده گردن به امر تو اختر</p></div></div>