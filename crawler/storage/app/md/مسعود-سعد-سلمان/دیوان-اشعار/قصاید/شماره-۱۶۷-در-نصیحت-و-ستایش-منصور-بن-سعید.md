---
title: >-
    شمارهٔ ۱۶۷ - در نصیحت و ستایش منصور بن سعید
---
# شمارهٔ ۱۶۷ - در نصیحت و ستایش منصور بن سعید

<div class="b" id="bn1"><div class="m1"><p>چند گویی که نشنوندت راز</p></div>
<div class="m2"><p>چند جویی که می نیابی باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد مکن خو که طبع گیرد خو</p></div>
<div class="m2"><p>ناز کم کن که آز گردد ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از فراز آمدی سبک به نشیب</p></div>
<div class="m2"><p>رنج بینی که بر شوی به فراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیشتر کن عزیمت چون برق</p></div>
<div class="m2"><p>در زمانه فکن چو رعد آواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمتر از شمع نیستی بفروز</p></div>
<div class="m2"><p>گر سرت را جدا کنند بگاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راست کن لفظ و استوار بگو</p></div>
<div class="m2"><p>سره کن راه و پس دلیر بتاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک صرفی به قعر مرکز رو</p></div>
<div class="m2"><p>نور محضی به اوج گردون تاز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نیابی مراد خویش بکوش</p></div>
<div class="m2"><p>تا نسازد زمانه با تو بساز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر عقابی مگیر عادت جغد</p></div>
<div class="m2"><p>ور پلنگی مگیر خوی گراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به کم از قدر خود مشو راضی</p></div>
<div class="m2"><p>بین که گنجشک می نگیرد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر زمین فراخ ده ناورد</p></div>
<div class="m2"><p>بر هوای بلند کن پرواز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر تو سنگی بلای سختی کش</p></div>
<div class="m2"><p>ور نه ای سنگ بشکن و بگداز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چند باشی به این و آن مشغول</p></div>
<div class="m2"><p>شرم دار و به خویشتن پرداز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دل و سر مساز سنگ و گهر</p></div>
<div class="m2"><p>هر چه داری ز دل برون انداز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیز منویس نامه های امید</p></div>
<div class="m2"><p>بیش مفرست رقعه های نیاز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جز بر صاحب اجل منصور</p></div>
<div class="m2"><p>آنکه مهرش برد ز چرخ نماز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در صفت مدح او چو گرد آید</p></div>
<div class="m2"><p>لشکری کش ز عقل باشد ساز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرکب شکر او چو رعد بکوب</p></div>
<div class="m2"><p>علم وصف او چه مه به فراز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حمله ها بر به طبع تیغ گذار</p></div>
<div class="m2"><p>رزم ها کن به وهم تیرانداز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو بهی قرعه امید بزن</p></div>
<div class="m2"><p>تو بری مهره مراد بباز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ور نوای مدیح خواهی زد</p></div>
<div class="m2"><p>رود کردار طبع را بنواز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حرز جان تو بس بود ز بلا</p></div>
<div class="m2"><p>مدحت شهریار بنده نواز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پادشاه بوالمظفر ابراهیم</p></div>
<div class="m2"><p>آن زمانه نهاد گردون ساز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنکه از عدل و جود او به جهان</p></div>
<div class="m2"><p>رنج کوتاه گشت و عمر دراز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای به هر حال چون عصای کلیم</p></div>
<div class="m2"><p>تیغ برانت مایه اعجاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهر مجدی بر آسمان شرف</p></div>
<div class="m2"><p>روز از تو بتافت زیب و براز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نام تو بر نگین دولت نقش</p></div>
<div class="m2"><p>جاه تو بر لباس ملک طراز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شرف دودمان آدم را</p></div>
<div class="m2"><p>به حقیقت تویی و خلق مجاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صدفم من که در شود به ثبات</p></div>
<div class="m2"><p>هر چه آید مرا به طبع فراز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>داریم همچو مشرکان به عذاب</p></div>
<div class="m2"><p>ورچه هرگز نخواندمت انباز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شده از من موافقان رنجور</p></div>
<div class="m2"><p>شده بر من مخالفان طناز</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه غم مدح تو ازین دل کم</p></div>
<div class="m2"><p>نه در سعی تو بر این تن باز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خواستم کز ولایت مهرت</p></div>
<div class="m2"><p>بروم جان مرا نداد جواز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کردم این گفته ها همه موجز</p></div>
<div class="m2"><p>که ستودست در سخن ایجاز</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روز عیشم نداد خواهد نور</p></div>
<div class="m2"><p>تا نبینم چو آفتابت باز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا بود صبح واشی و نمام</p></div>
<div class="m2"><p>تا بود ساعی و غماز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زین شود باغ طبله عطار</p></div>
<div class="m2"><p>زان شود راغ کلبه بزاز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر چمن ورد و سرو ماند راست</p></div>
<div class="m2"><p>به رخ و قد لعبتان طراز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همچو ورد طری بتاب و بخند</p></div>
<div class="m2"><p>همچو سرو سهی ببال و بناز</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با علو سپهر بادت امر</p></div>
<div class="m2"><p>با سعود زمانه بادت راز</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همه فردای تو به از امروز</p></div>
<div class="m2"><p>همه فرجام تو به از آغاز</p></div></div>