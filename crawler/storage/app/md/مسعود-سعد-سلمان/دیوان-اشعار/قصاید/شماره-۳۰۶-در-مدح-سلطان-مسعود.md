---
title: >-
    شمارهٔ ۳۰۶ - در مدح سلطان مسعود
---
# شمارهٔ ۳۰۶ - در مدح سلطان مسعود

<div class="b" id="bn1"><div class="m1"><p>گفتی که وفا کنم جفا کردی</p></div>
<div class="m2"><p>وز خود همه ظن من خطا کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن پس که بر آنچه گفته بودی تو</p></div>
<div class="m2"><p>صد بار خدای را گوا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آب دو دیده آشنا کردم</p></div>
<div class="m2"><p>تا با غم خویشم آشنا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمت ناید ز خویشتن کز من</p></div>
<div class="m2"><p>برگشتی و یار ناسزا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردی تو مرا به کام بدگویان</p></div>
<div class="m2"><p>ای بی معنی چنین چرا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چون دل خود به تو رها کردم</p></div>
<div class="m2"><p>ای دوست چرا مرا رها کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن دل که ز من به قهر بربودی</p></div>
<div class="m2"><p>از بهر خدای را کجا کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از من دل خویش بستدی ترسم</p></div>
<div class="m2"><p>آن را به دگر کسی عطا کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای عاشق خسته دل جفادیدی</p></div>
<div class="m2"><p>زآن کش به دل و به جان وفا کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاید که ز عشق دل بپردازی</p></div>
<div class="m2"><p>چون قصد ثنای پادشا کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسعود که نام او چو برگفتی</p></div>
<div class="m2"><p>والله که بر او همه ثنا کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهی که ز خدمت همایونش</p></div>
<div class="m2"><p>هر کام که داشتی روا کردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهی که ز خاک صحن میدانش</p></div>
<div class="m2"><p>اندر کف بخت کیمیا کردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاهی که غبار مرکب او را</p></div>
<div class="m2"><p>در دیده عمر توتیا کردی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چرخی که ز مدح او همه گیتی</p></div>
<div class="m2"><p>مانند اثیر پر ضیا کردی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مهری که چو وصف ذات او گفتی</p></div>
<div class="m2"><p>از فخر نشست بر سما کردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بحری که چو غور طبع او جستی</p></div>
<div class="m2"><p>در موج جلال آشنا کردی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر جان مخالفان به مدح او</p></div>
<div class="m2"><p>هر بیتی تیری از بلا کردی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از شه به رضای خود ثنا دیدی</p></div>
<div class="m2"><p>جان زود فدای آن رضا کردی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وآنگاه عروس مدح خوبش را</p></div>
<div class="m2"><p>پیرایه ز دره پر بها کردی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرد از گردون فریشته آمین</p></div>
<div class="m2"><p>چون ملک و بقاش را دعا کردی</p></div></div>