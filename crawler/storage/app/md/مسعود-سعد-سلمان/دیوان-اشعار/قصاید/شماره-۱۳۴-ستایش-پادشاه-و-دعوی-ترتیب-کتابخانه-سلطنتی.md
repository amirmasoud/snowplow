---
title: >-
    شمارهٔ ۱۳۴ - ستایش پادشاه و دعوی ترتیب کتابخانه سلطنتی
---
# شمارهٔ ۱۳۴ - ستایش پادشاه و دعوی ترتیب کتابخانه سلطنتی

<div class="b" id="bn1"><div class="m1"><p>جهان دارا به کام جهان دار</p></div>
<div class="m2"><p>جهان جز بر سریر ملک مگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نام تست بخت تو همیشه</p></div>
<div class="m2"><p>که هستش جفت سعد چرخ دوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوندا زبان بنده تو</p></div>
<div class="m2"><p>به شکر تو چو ابری شد شکربار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه کن تا عروسان ثنا را</p></div>
<div class="m2"><p>چگونه تیز خواهد کرد بازار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خوبی بوستان مدحت تو</p></div>
<div class="m2"><p>همه قصر تو خواهد کرد فرخار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار آوای بزمت بود خواهد</p></div>
<div class="m2"><p>که خواهد کرد بزمت را چو گلزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جان خواهد ستودت زانکه جانش</p></div>
<div class="m2"><p>تو دادی از پس یزدان دادار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جان درمانده بود و کرده بر وی</p></div>
<div class="m2"><p>زمانه روز روشن را شب تار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن او ز انده و تیمار بی جان</p></div>
<div class="m2"><p>چو مار گرزه اندر آهنین غار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یک فرمان که فرمانت روان باد</p></div>
<div class="m2"><p>رهانیدش از آن اندوه و تیمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی گردد همی در حضرت امروز</p></div>
<div class="m2"><p>عزیز و سرفراز و نام بردار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همش هر جشن جاه و خلقت شاه</p></div>
<div class="m2"><p>همش هر روز عز خدمت بار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همش توقیع سیم و غله بوده</p></div>
<div class="m2"><p>بیاسوده دلش زاندوه پیکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه زن گوید که بر تن نیست جامه</p></div>
<div class="m2"><p>نه گوید بچه بر سر نیست دستار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دعای شاه چون تسبیح گویند</p></div>
<div class="m2"><p>عیال بی حد و اطفال بسیار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون این وام ها ماند و نماند</p></div>
<div class="m2"><p>چو بر نقدی روانش کرد ادرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که بگذارد بچاره یک یک این وام</p></div>
<div class="m2"><p>برون آرد ز پایش یک یک این خار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیاراید کنون دارالکتب را</p></div>
<div class="m2"><p>به توفیق خدای فرد جبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز هر دارالکتب کاندر جهانست</p></div>
<div class="m2"><p>چنان سازد که بیش آید به مقدار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شادی برجهد هر بامدادی</p></div>
<div class="m2"><p>بروبد خاک هر حجره به رخسار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به جان آن را عمارت پیش گیرد</p></div>
<div class="m2"><p>که چون بنده نباشد هیچ معمار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دهد هر علم را نظمی که هر کس</p></div>
<div class="m2"><p>بود از علم نوعی را خریدار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کند مشحون همه طاق و رف آن</p></div>
<div class="m2"><p>به تفسیر و به اخبار و به اشعار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر این گفتار او باور نیاید</p></div>
<div class="m2"><p>تو را ظاهر شود زین پس به کردار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه مردست آنکه همچون هم نباشد</p></div>
<div class="m2"><p>مر او را در جهان گفتار و کردار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قوی دل گردد آنکه کاندرین باب</p></div>
<div class="m2"><p>بود توقیع سلطان جهاندار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا ز دور چرخ گردان</p></div>
<div class="m2"><p>به گیتی شاهی و شادی بود یار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز شاهی شاد بادی زانکه امروز</p></div>
<div class="m2"><p>تویی شاهی و شادی را سزاوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو بر تخت جلالت شاد و شاهان</p></div>
<div class="m2"><p>میان بسته به پیشت بنده کردار</p></div></div>