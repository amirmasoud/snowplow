---
title: >-
    شمارهٔ ۲۲۲ - مدح علاء الدوله سلطان مسعود
---
# شمارهٔ ۲۲۲ - مدح علاء الدوله سلطان مسعود

<div class="b" id="bn1"><div class="m1"><p>دولت جوان و ملک جوان و ملک جوان</p></div>
<div class="m2"><p>ملک جهان گرفتن و دادن نکو توان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ترک باد جنگ برون کنی یکی ز سر</p></div>
<div class="m2"><p>برخیز و باده در ده بر فتح جنگوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنمود خسروان جهان را نموده نی</p></div>
<div class="m2"><p>تیغ علاء دولت و دین خسرو جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسعود پادشاهی کز فر ملک او</p></div>
<div class="m2"><p>آرایش بهار ستد صورت خزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهی که رخش او را دولت بود دلیل</p></div>
<div class="m2"><p>شاهی که تیغ او را نصرت بود فسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر پی گمانش پی بگسلد یقین</p></div>
<div class="m2"><p>واندر دم یقینش بی بفکند گمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا جود او به راه امل گشته بدرقه</p></div>
<div class="m2"><p>نگسست کاروان مکارم ز کاروان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درماندگان کم درمی را سخای او</p></div>
<div class="m2"><p>از دل همی به حاصل هستی کند ضمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترسیدگان بی نظیری را امید او</p></div>
<div class="m2"><p>بر درج اعتماد نویسد همی امان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاها زمین ز قوت اقبال ملک تو</p></div>
<div class="m2"><p>ممکن بود که دست برآرد به آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاخ گل از نشاط دل افروز بزم تو</p></div>
<div class="m2"><p>واجب بود که جانور آید به بوستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امنست در حوالی ملک تو کار بند</p></div>
<div class="m2"><p>عدلست در حوالی ملک تو قهرمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دستت همی زمین را مفلس کند به زر</p></div>
<div class="m2"><p>تیغت همی هوا را قارون کند ز جان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موجود شد ز کوشش تو در شاهوار</p></div>
<div class="m2"><p>معلوم شد ز بخشش تو گنج شایگان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک تو عدل را پسری سخت نیک بخت</p></div>
<div class="m2"><p>عدل تو ملک را پدری نیک مهربان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از دست تو ندیده مگر تیغ تو بلای</p></div>
<div class="m2"><p>بر کار تو نکرده مگر گنج تو زیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گیتی ز کارکرد تو گوید همی خبر</p></div>
<div class="m2"><p>زیرا که دستبرد تو بیند همی عنان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیند جلالت تو و گوید ثنای تو</p></div>
<div class="m2"><p>گردون و روزگار تو بی چشم و بی دهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از زخم گام باره تو در صمیم دی</p></div>
<div class="m2"><p>بر کوه لاله رسته و بر دشت ضیمران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو سوی شیر تاخته از حرص صید شیر</p></div>
<div class="m2"><p>بر سخته زور و قوت بازو به امتحان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برده دو زخم حربه به یک خاستن به کار</p></div>
<div class="m2"><p>کرده دو شیر شرزه به یک حمله بی روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگشادشان دو روزن جانکاه بر دو یال</p></div>
<div class="m2"><p>ریزان از آن دو روزن از خون دو ناودان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آغاز کرده خاک زمین را ز خون این</p></div>
<div class="m2"><p>آهار داده سنگ سیه را ز مغز آن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این را نبوده کاری دندان عمر خوار</p></div>
<div class="m2"><p>وان را نداده یاری چنگال جانستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این سست پنجه گشته از آن بازوی قوی</p></div>
<div class="m2"><p>وان کندیشک مانده از آن خنجریمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حفظ خدای و تقویت چرخ و سعی بخت</p></div>
<div class="m2"><p>بوده تو را پناه و معین و نگاهبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا فتح جنگوان تو در داستان فرود</p></div>
<div class="m2"><p>گم شد حدیث رستم دستان ز داستان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اسباب غزو ساخته چون جد و چون پدر</p></div>
<div class="m2"><p>چون جد و چون پدر کمر فتح بر میان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ره پیش برگرفتی و ناگاه پیش تو</p></div>
<div class="m2"><p>مردان کار دیده و گردان کاردان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر باره زمانه گذار و زمین نورد</p></div>
<div class="m2"><p>تندر صهیل و اختر سیر و قضا توان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در لعب کر و فر تو گردان چو گردباد</p></div>
<div class="m2"><p>بر عطف طعن و ضرب تو پیچان چو خیزران</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خوش بگسلد چو خیزد زنجیر آهنین</p></div>
<div class="m2"><p>باز ایستد به جای به یک تار پرنیان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حزم تو را ز فرق گذشته لب سپر</p></div>
<div class="m2"><p>عزم تو را به گوش رسیده زه کمان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>راندی چنانکه خاک نشورید بر زمین</p></div>
<div class="m2"><p>رفتی چنانکه مرغ نجنبید ز آشیان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نادیده راههای تو را روزها اثر</p></div>
<div class="m2"><p>نا داده گرزهای تو را بادها نشان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گه کوه زیر پای تو گه ابر زیردست</p></div>
<div class="m2"><p>گه چرخ همرکاب تو گه وهم همعنان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن کوه را که خاصه تو را جنگ جای بود</p></div>
<div class="m2"><p>در پیش سجده کرد همی گنبد کیان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پرداختی طریقی مشکل به هفت روز</p></div>
<div class="m2"><p>بر کوفتی ثغوری هایل چو هفت خوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر کشوری زدی که درو کیش کافری</p></div>
<div class="m2"><p>سالی هزار بوده به تاریخ باستان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خلقی نه مردم آسا نه آدمی سرشت</p></div>
<div class="m2"><p>با دیو هم سجیت و با غول همزبان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنجا شراب تیغ چشیدند ناشتا</p></div>
<div class="m2"><p>آنجا غریو کوس شنیدند ناگهان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بسته کمر ز هیبت و ز بیم تیغ تو</p></div>
<div class="m2"><p>جز تیغ آفتاب نیفکنده زیر ران</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون بنگریستند به دستی نبود بیش</p></div>
<div class="m2"><p>از راه کهکشانش تا راه کهکشان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یک خرده یادم آمد و این نیک خرده ایست</p></div>
<div class="m2"><p>شاید که در سخن کنم این خرده را بیان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نمرود ساخت کرکس و آگه نبود از آنک</p></div>
<div class="m2"><p>دارد سپهر گردون زانگونه نردبان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شمشیر آبدار تو در چین فکند زود</p></div>
<div class="m2"><p>فرشی و سایبانی از آتش و دخان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از خون تازه یافت زمین لعل مقنعه</p></div>
<div class="m2"><p>وز گرد تیره یافت هوا مشک طیلسان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گشتی چو شرزه شیر سپاهی به یک نفس</p></div>
<div class="m2"><p>شستی ز کفر و شرک جهانی به یک زمان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نیلوفری حسام تو کشت آن گروه را</p></div>
<div class="m2"><p>بر پشت و سینه لاله و بر چهره زعفران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در هر تنی پراکند آن پرنیان پرند</p></div>
<div class="m2"><p>خاکی کزو نروید جز دار پرنیان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شد غورغار ژرف یک آهنگ رود خون</p></div>
<div class="m2"><p>شد صحن دشت پهن همه کوه استخوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سعی قوی نمود بیک بیک ضعیف</p></div>
<div class="m2"><p>زخم سبک گزارد همی خنجر گران</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خسته ز پیش تیغ تو و نعل رخش تو</p></div>
<div class="m2"><p>خونش به نهروان شد و گردش به قیروان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خاکستری شد آن کوه از آتش نبرد</p></div>
<div class="m2"><p>دودی سیه برآمد زان تیره دودمان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>روح الامین فریشتگان را چه گفت گفت</p></div>
<div class="m2"><p>خشنود گشت بار خدای از خدایگان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>این چاشنیست شربت تیغ تو هند را</p></div>
<div class="m2"><p>باقی دهد که باقی بادی تو جاودان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بخت جوان یکی شد با رای پیر تو</p></div>
<div class="m2"><p>ای کرده باز پیر جهان را ز سر جوان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>اکنون یکی به پیشگه عدل برنشین</p></div>
<div class="m2"><p>یک هفته حرص جنگ ز خاطر فرونشان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بستان چو ناردان و چو گلنار باده ای</p></div>
<div class="m2"><p>زان کش رخ و لبست چو گلنار و ناردان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شهزاده میزبان و تو مهمان روزگار</p></div>
<div class="m2"><p>بسته میان به خدمت مهمان و میزبان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا دایمست جنبش گردون و آفتاب</p></div>
<div class="m2"><p>تا واجبست گردش نوروز و مهرگان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از چرخ حل و عقد زمانست بر زمین</p></div>
<div class="m2"><p>وز دهر امر و نهی مکین است بر مکان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از بخت هر مراد که خواهی همی بیاب</p></div>
<div class="m2"><p>وز دهر هر نشاط که داری همی بران</p></div></div>