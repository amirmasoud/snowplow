---
title: >-
    شمارهٔ ۱۵۷ - در صفت پیلان و مدح آن سلطان
---
# شمارهٔ ۱۵۷ - در صفت پیلان و مدح آن سلطان

<div class="b" id="bn1"><div class="m1"><p>سوی میدان شهریار گذر</p></div>
<div class="m2"><p>قدرت و صنع کردگار نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایستاده نگاه کن چپ و راست</p></div>
<div class="m2"><p>کوههای بلند و جاناور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر یکی با یک اژدهای دمان</p></div>
<div class="m2"><p>اژدها نه و اژدها پیکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو ستون در دهان هر یک از آن</p></div>
<div class="m2"><p>اندر آهن گرفته سر تا سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دژ آهنین ویشک قویش</p></div>
<div class="m2"><p>در دژ آهنین گشاید در</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمنی را اگر بخسبانند</p></div>
<div class="m2"><p>از گل و خاک و خون بود بستر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشی را اگر بر افروزند</p></div>
<div class="m2"><p>گردد آن را نجوم چرخ شرر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه نعمت ژنده پیلانست</p></div>
<div class="m2"><p>که سر نصرتند و روی ظفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه مستند و اهتزاز کنند</p></div>
<div class="m2"><p>به سرود و سماع بازیگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه دیوان روز پیکارند</p></div>
<div class="m2"><p>برده دیوان ز زخمشان کیفر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد زده زان چهار صد عفریت</p></div>
<div class="m2"><p>که گه تک شوند مرغ به پر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این شگفتی کدام خسرو راست</p></div>
<div class="m2"><p>یک جهان دیو گشته فرمانبر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سلیمان نشسته کامروا</p></div>
<div class="m2"><p>ملک داد و رز دین پرور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شه ملک ارسلان بن مسعود</p></div>
<div class="m2"><p>شادی تخت و نازش افسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه از نام همچو خورشیدش</p></div>
<div class="m2"><p>آسمان شد ز بس شرف منبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داده در دست از زمانه زمام</p></div>
<div class="m2"><p>بسته در خدمتش سپهر کمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک را کرده عدل او یاری</p></div>
<div class="m2"><p>ملک را بسته عدل او زیور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به فغان آمده ز تیغش کفر</p></div>
<div class="m2"><p>به خروش آمده ز دستش زر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای بر رفعت تو چرخ زمین</p></div>
<div class="m2"><p>وی بر بخشش تو بحر شمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملکی و به ملک هفت اقلیم</p></div>
<div class="m2"><p>نیست اندر جهان ز تو حق تر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من زدم فال و فال گشت نهال</p></div>
<div class="m2"><p>آن نهالی که دولت آرد بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لشکری دولت تو تعبیه کرد</p></div>
<div class="m2"><p>کاندرو وهم کس نیافت گذر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ژنده پیلان تو چو پیلانند</p></div>
<div class="m2"><p>از پس و پیش آن قوی لشکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش هر پیل فوجی از ترکان</p></div>
<div class="m2"><p>رزمجویان چو شیر شرزه نر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر کرا پیل و شیر بازیگر</p></div>
<div class="m2"><p>دشمنان را به نزد او چه خطر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این همه هست هست و بود و بود</p></div>
<div class="m2"><p>کردگار جهان تو را یاور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پیش چشم آیدم همی فتحی</p></div>
<div class="m2"><p>که شود ناگهان به دهر سمر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من از آن فتح چون براندیشم</p></div>
<div class="m2"><p>یادم آید همی ز فتح کتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که در ایام جد جد تو را</p></div>
<div class="m2"><p>کرد روزی کروکر داور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پادشاها به فرخی بنشین</p></div>
<div class="m2"><p>شهریارا به خرمی می خور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون به بزم تو در کف تو شود</p></div>
<div class="m2"><p>باده آب حیات در ساغر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه عجب گر فلک شود مجلس</p></div>
<div class="m2"><p>ماه و ساقی و زهره خنیاگر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا ز گردون و اختر اندر دهر</p></div>
<div class="m2"><p>هر چه مضمر بود شود مظهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باد گردان برای تو گردون</p></div>
<div class="m2"><p>باد تابان به حکم تو اختر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هفت کشور تو را به زیر نگین</p></div>
<div class="m2"><p>وز تو آباد و شاد هر کشور</p></div></div>