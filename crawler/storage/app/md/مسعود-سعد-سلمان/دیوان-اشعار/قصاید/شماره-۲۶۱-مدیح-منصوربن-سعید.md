---
title: >-
    شمارهٔ ۲۶۱ - مدیح منصوربن سعید
---
# شمارهٔ ۲۶۱ - مدیح منصوربن سعید

<div class="b" id="bn1"><div class="m1"><p>دوش گفتی ز تیرگی شب من</p></div>
<div class="m2"><p>زلف حورست و رای اهریمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زشت چون ظلم و بیکرانه چو حرص</p></div>
<div class="m2"><p>تیره چون محنت و سیه چو حزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانده شد مهر گویی از رفتار</p></div>
<div class="m2"><p>سیر شد چرخ گویی از گشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو زنگار خورده آینه ای</p></div>
<div class="m2"><p>می نمود از فراز من روزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که زرنگش نمی توانستم</p></div>
<div class="m2"><p>اندرو روی صبح را دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ مانند گرزنی که بود</p></div>
<div class="m2"><p>اندرو در و گوهر گرزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش اندر دلم بسوخته صبر</p></div>
<div class="m2"><p>آب ازین دیدگان ببرده وسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر چون آتشی فرو شد و زو</p></div>
<div class="m2"><p>پر ز دود سیاه شد روزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نه دود سیاه بود چرا</p></div>
<div class="m2"><p>زو روان گشت آب دیده من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سیاهیش چشم من اعمی</p></div>
<div class="m2"><p>وز نهیبش زبان من الکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دلم ترجمان شده کلکی</p></div>
<div class="m2"><p>چون زبانم همی گشاده سخن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دلم چون شب سیاه آورد</p></div>
<div class="m2"><p>از معانی کواکب روشن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نه آبستن است از چه سبب</p></div>
<div class="m2"><p>ناشکیبا بود گه زادن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کس نداند که او چه خواهد زاد</p></div>
<div class="m2"><p>این چنین باشد آری آبستن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به سرش رفتن و کشان از پس</p></div>
<div class="m2"><p>گیسوی عنبرینش چون دامن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیز رفتار گردد و چیره</p></div>
<div class="m2"><p>چونکه مجروح گردد از آهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دشمن اوست آهن و که شنید</p></div>
<div class="m2"><p>کس که باشد صلاحش از دشمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نوبهاری همی برآرد زود</p></div>
<div class="m2"><p>که ازو عقل را بود گلشن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زآن سیاهیش چون دل لاله</p></div>
<div class="m2"><p>بر سپیدیش همچو روی سمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بست زنار و شد نگار پرست</p></div>
<div class="m2"><p>صاحب از بهر آن زدش گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خواجه منصوربن سعید که کرد</p></div>
<div class="m2"><p>زنده آثار احمدبن حسن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای سخای تو در جهان سایر</p></div>
<div class="m2"><p>وانکه گرداردی سخات بدن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به جهان در نماندی خالی</p></div>
<div class="m2"><p>از هوا جای یک سر سوزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وعده تو ندید هرگز بطل</p></div>
<div class="m2"><p>بخشش تو نداشت هرگز من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نیست پاداشنی سخای تو را</p></div>
<div class="m2"><p>نه سخای تو هست پاداشن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو حسامی به گوهر و به هنر</p></div>
<div class="m2"><p>باز پیش حسام فقر مجن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وین عجب تر که تیغ دانش را</p></div>
<div class="m2"><p>هم تو صیقل شدی و هم توسن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به گه آفرینش از حشمت</p></div>
<div class="m2"><p>باقیی ماند گشت اصل فتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ز بهر وزارت آورده</p></div>
<div class="m2"><p>مر تو را سروری چو در عدن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دری و در نظم و نثر تو را</p></div>
<div class="m2"><p>کس نداند درین زمانه ثمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از دل و جان رهی خاص توام</p></div>
<div class="m2"><p>تا مرا جان و دل بود در تن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در هوای توام ببسته میان</p></div>
<div class="m2"><p>در ثنای توام گشاده دهن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من بیفتاده ام مرا بردار</p></div>
<div class="m2"><p>بار اندوه از تنم بفکن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خز کوفی مدار همچو پلاس</p></div>
<div class="m2"><p>گل سوری مبوی چون راسن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای شکسته منازعان را پشت</p></div>
<div class="m2"><p>پشت اندیشه را به من بشکن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رخ برافروز همچو مهر سپهر</p></div>
<div class="m2"><p>سر برافراز همچو سرو چمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باده گیر از کف دلارایی</p></div>
<div class="m2"><p>لعبتی ماهروی زهره ذقن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر نماندست سوسن و گل هست</p></div>
<div class="m2"><p>عارض و روی چون گل و سوسن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مجلست چرخ باد و تو خورشید</p></div>
<div class="m2"><p>ساغرت ماه و می سهیل یمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باد دستار نیکخواهت تاج</p></div>
<div class="m2"><p>باد پیراهن عدوت کفن</p></div></div>