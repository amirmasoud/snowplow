---
title: >-
    شمارهٔ ۸۵ - در مدح سلطان ظهیرالدوله ابراهیم
---
# شمارهٔ ۸۵ - در مدح سلطان ظهیرالدوله ابراهیم

<div class="b" id="bn1"><div class="m1"><p>شهریارا کردگارت یار باد</p></div>
<div class="m2"><p>بنده تو گبند دوار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز جاهت را سعادت نور باد</p></div>
<div class="m2"><p>شاخ ملکت را جلالت بار باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عزم جزم تو به حل و عقد ملک</p></div>
<div class="m2"><p>چون ستاره ثابت و سیار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبع و عقلت بحر لؤلؤ موج باد</p></div>
<div class="m2"><p>دست جودت ابر گوهربار باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقطه ای باد آسمان گرد درت</p></div>
<div class="m2"><p>رای تو بر گرد او پرگار باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولتت را سعی بی تقصیر باد</p></div>
<div class="m2"><p>نصرتت را تیغ بی زنگار باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زار وقت شادی تو زیر باد</p></div>
<div class="m2"><p>خار وقت جود تو دینار باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزهای روشن گیتی همه</p></div>
<div class="m2"><p>بر عدوی تو شبان تار باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغز بدخواه تو اندر خاک خفت</p></div>
<div class="m2"><p>دیده اقبال تو بیدار باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ را با حاسدت آویز باد</p></div>
<div class="m2"><p>بخت را با دشمنت پیکار باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تارک این زیر چنگ شیر باد</p></div>
<div class="m2"><p>سینه آن پیش نیش مار باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ و تیرت را به روز کارزار</p></div>
<div class="m2"><p>فتح و نصرت قبضه و سوفار باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جهان بر هر جهانگیری ز تو</p></div>
<div class="m2"><p>هر مثالی لشکری جرار باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صدرت از مه منظران باد آسمان</p></div>
<div class="m2"><p>بزمت از بت پیکران فرخار باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست و بازوی تو را در کارزار</p></div>
<div class="m2"><p>فر و زور حیدر کرار باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رای تو تابنده چون خورشید باد</p></div>
<div class="m2"><p>ملک تو پاینده چون کهسار باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر که از شادیت چون گل تازه نیست</p></div>
<div class="m2"><p>همچو شاخ گل دلش پر خار باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دولتت هر سو که تازی جفت باد</p></div>
<div class="m2"><p>ایزدت هر جا که باشی یار باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو عجب داری که من گویم همی</p></div>
<div class="m2"><p>کز جلالت شاه برخوردار باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کز فلک هر ساعتی گوید ملک</p></div>
<div class="m2"><p>خسرو ابراهیم گیتی دار باد</p></div></div>