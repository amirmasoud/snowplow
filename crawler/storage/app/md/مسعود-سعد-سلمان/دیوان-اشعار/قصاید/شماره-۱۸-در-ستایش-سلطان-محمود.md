---
title: >-
    شمارهٔ ۱۸ - در ستایش سلطان محمود
---
# شمارهٔ ۱۸ - در ستایش سلطان محمود

<div class="b" id="bn1"><div class="m1"><p>هوای روشن بگرفت تیره رنگ سحاب</p></div>
<div class="m2"><p>جهان گشته خرف بازگشت از سرشاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان چو یافت شباب ای شگفت گرم و ترست</p></div>
<div class="m2"><p>مزاج گرم وتر آری بود مزاج شباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روان شده است هوا را خوی و چنان باشد</p></div>
<div class="m2"><p>چو وقت گرما پوشد حواصل و سنجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شگفت نیست که شنگرف خیزد از سیماب</p></div>
<div class="m2"><p>از آنکه مایه شنگرف باشد از سیماب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسان کوره شنگرف شد گل از گل سرخ</p></div>
<div class="m2"><p>برو چو روشن سیماب ریخت قطره سحاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین شده همه چون چشم کبک و روی تذرو</p></div>
<div class="m2"><p>هوا شده همه چون دم باز و پر عقاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس که ابر هوا همچو بیدلان بگریست</p></div>
<div class="m2"><p>چو دلفریبان بگشاد گل ز روی نقاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز کوهسار سحرگه چو صبح صادق تافت</p></div>
<div class="m2"><p>گل مورد بگشاد چشم خویش از خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بهر آن که ببیند سپاه خسرو را</p></div>
<div class="m2"><p>به راغ لاله پدید آمد از میان حجاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بوستان کمر زر ببست گلبن زرد</p></div>
<div class="m2"><p>ز بهر خدمت شاه زمانه چون حجاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایگان جهان تاج خسروان محمود</p></div>
<div class="m2"><p>شه همه عجم و خسرو همه اعراب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به گاه ضرب همی زر و سیم بوسه زند</p></div>
<div class="m2"><p>ز عز نامش بر روی سکه ضراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر خواست که بوسه زند رکابش را</p></div>
<div class="m2"><p>رسید می نتواند بدان بلند جناب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امید خلق به درگاه او روا گردد</p></div>
<div class="m2"><p>که خسروی را قبله است و ملک را محراب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تیره ابر و به روشن اثیر در حرکت</p></div>
<div class="m2"><p>ز تیغ و تیرش آموختند برق سحاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که برق وار جهد از نیام او خنجر</p></div>
<div class="m2"><p>شهاب وار رود از کمان به شتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی نسوزد جز جان دیو روز نبرد</p></div>
<div class="m2"><p>یکی نبارد جز گرد مرگ روز ضراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو روی داری شاها به سوی هندوستان</p></div>
<div class="m2"><p>به نام ایزد و عزم درست و رای صواب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به دولت تو ز بهر سپاه و لشکر تو</p></div>
<div class="m2"><p>به دشت آب روان گشت هر چه بود سراب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خیال تیغ تو در دیده ملوک بماند</p></div>
<div class="m2"><p>چنان که تیغ تو بینند روز و شب در خواب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بیم تو تنشان زخم خورده چون نیزه است</p></div>
<div class="m2"><p>ز سهم تو دلشان همچو گوی در طبطاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بیشه هایی آری سپاه را که زمینش</p></div>
<div class="m2"><p>نتافته است بر او آفتاب و نه مهتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز رودهایی لشکر همی گذاره کنی</p></div>
<div class="m2"><p>که دیو هرگز در وی نیافتی پایاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون ملوک به بستان و باغ مشغولند</p></div>
<div class="m2"><p>همی ستانند انصاف شادی از احباب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نشانده مطرب زیبا فکنده لاله لعل</p></div>
<div class="m2"><p>به پای ساقی گلرخ به دست باده ناب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز آب گلها حوض و ز سایبان ایوان</p></div>
<div class="m2"><p>ز چوب بتکده عود و ز آب ابر گلاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو را نشاط بدان تا کدام شهر زنی</p></div>
<div class="m2"><p>کدام بتکده سازی ز بوم هند خراب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ستاده مرکب غران به جای بربط و چنگ</p></div>
<div class="m2"><p>گرفته خنجر بران به جای جام و شراب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو هر زمان ملکا نو بهاری آرایی</p></div>
<div class="m2"><p>که عاجز آید ازو خاطر اولوالالباب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببارد ابر و جهد برق تا پدید آرد</p></div>
<div class="m2"><p>ز خون دشمن بر خاک لاله سیراب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به رزم آتش افروخته است خنجر تو</p></div>
<div class="m2"><p>به پیش آتش افروخته که دارد تاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کدام کشور کش نه ز دست تست امیر</p></div>
<div class="m2"><p>کدام خسرو کش نه ز دست تست مآب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بس امان که نبشتند از تو شاهان را</p></div>
<div class="m2"><p>ز کار ماند شها دست و خامه کتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنین طریق ز شاهان کرا بود که تراست</p></div>
<div class="m2"><p>به حلم و عفو درنگ و به جنگ و جود شتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو سیف دولتی و عز ملتی که تو را</p></div>
<div class="m2"><p>صنیع خویش به نامه خلیفه کرد خطاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نصیب دولت و ملت ز خویشتن داری</p></div>
<div class="m2"><p>درست کردی بر خویشتن همه القاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شهی که ایزد صاحبقرانش خواهد کرد</p></div>
<div class="m2"><p>چنین که ساخت ز اول بسازدش اسباب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کنون همی دمد ای شاه صبح نصرت و فتح</p></div>
<div class="m2"><p>هنوز اول صبح است خسروا مشتاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا فلک آبگون همی گردد</p></div>
<div class="m2"><p>گهی بسان رحا گه حمایل و دولاب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به دولت اندر ملک تو را مباد کران</p></div>
<div class="m2"><p>به شادی اندر عمر تو را مباد حساب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به بوستان سعادت چو زاد سرو ببال</p></div>
<div class="m2"><p>ز آسمان جلالت چو آفتاب بتاب</p></div></div>