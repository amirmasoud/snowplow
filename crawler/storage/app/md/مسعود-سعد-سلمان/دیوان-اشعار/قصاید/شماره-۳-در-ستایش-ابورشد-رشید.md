---
title: >-
    شمارهٔ ۳ - در ستایش ابورشد رشید
---
# شمارهٔ ۳ - در ستایش ابورشد رشید

<div class="b" id="bn1"><div class="m1"><p>ای رفیقان من ای عمر و منصور و عطا</p></div>
<div class="m2"><p>که شما هر سه سمائید و هوائید و صبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده بیچاره مرا جوع به ماه رمضان</p></div>
<div class="m2"><p>خبری هست ز شوال به نزدیک شما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به مغرب ننموده است مرا چهره هلال</p></div>
<div class="m2"><p>من چنان گشتم از ضعف که در شرق سها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عید گویی که همی آید از سنگ برون</p></div>
<div class="m2"><p>یا مه روزه مرا می دهد از سنگ حیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی طعمه شامی شده ام چون خفاش</p></div>
<div class="m2"><p>وز پی دیدن خورشید شدم چون حربا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کنم قصه بیهوده ز خمر و ز خمار</p></div>
<div class="m2"><p>چون نمی یارم گفتن سخن ماه سما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به قندیل فتاده است مرا کار به شب</p></div>
<div class="m2"><p>همچو شمعم که زیم امشب و میرم فردا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندرین روزه همه رنج من است از من آز آنک</p></div>
<div class="m2"><p>سفری کرد نیارستم من سرد بغا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مرا هیچ حلاوت نبود اندر روز</p></div>
<div class="m2"><p>چه کنم پس تو اگر سازی شب را حلوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حاش لله که مرا نیست بدین ره مذهب</p></div>
<div class="m2"><p>جز که هزلی است که رفته است میان شعرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرض یزدان را بگزارد هر کس که کند</p></div>
<div class="m2"><p>خدمت خاصه سلطان به خلا و به ملا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تحفه دولت ابورشد رشید آنکه فلک</p></div>
<div class="m2"><p>خواهدی تا کند او را از پی جود ثنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا جهان بادا در خدمت سلطان بادا</p></div>
<div class="m2"><p>اندرین ز ایزد تقدیر و ز من بنده دعا</p></div></div>