---
title: >-
    شمارهٔ ۷۰ - شکوه از کجروی زمانه
---
# شمارهٔ ۷۰ - شکوه از کجروی زمانه

<div class="b" id="bn1"><div class="m1"><p>چون منی را فلک بیازارد</p></div>
<div class="m2"><p>خردش بی خرد نینگارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمانی چو ریگ تشنه ترم</p></div>
<div class="m2"><p>گر چه بر من چو ابر غم بارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بیفسایدم چو مار غمی</p></div>
<div class="m2"><p>بر دل من چو مار بگمارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تنم خاک محنتی نشود</p></div>
<div class="m2"><p>به دگر محنتیش بسپارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر آن تنگیم که وحشت او</p></div>
<div class="m2"><p>جان و دل را همی بیفشارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راضیم گر چه هول دیدارش</p></div>
<div class="m2"><p>دیده من به خار می خارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کز نهیبش همی قضا و بلا</p></div>
<div class="m2"><p>بر در او گذشت کم یارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سقف این سمج من سیاه شبی است</p></div>
<div class="m2"><p>که دو دیده به دوده انبارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز هر کس که روزنش بیند</p></div>
<div class="m2"><p>اختری سخت خرد پندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر دو قطره به هم بود باران</p></div>
<div class="m2"><p>جز یکی را به زیر نگذارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم ازو نگسلم که در تنگی</p></div>
<div class="m2"><p>به دلم نیک نسبتی دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شعر گویم همی و انده دل</p></div>
<div class="m2"><p>خاطرم جز به شعر نگسارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این جهان را به نظم شاخ زند</p></div>
<div class="m2"><p>هر چه در باغ طبع من کارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از فلک تنگدل مشو مسعود</p></div>
<div class="m2"><p>گر فراوان تو را بیازارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بد میندیش سر چو سرو برآر</p></div>
<div class="m2"><p>گر جهان بر سرت فرود آرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حق نخفته ست بنگری روزی</p></div>
<div class="m2"><p>که حق تو تمام بگزارد</p></div></div>