---
title: >-
    شمارهٔ ۱۹۱ - ابراز خلوص نسبت به یکی از اکابر
---
# شمارهٔ ۱۹۱ - ابراز خلوص نسبت به یکی از اکابر

<div class="b" id="bn1"><div class="m1"><p>ای آن که چون ز جاه تو بر تو ثنا کنم</p></div>
<div class="m2"><p>گیتی ز نور خاطر خود پر ضیا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گه که گفت خواهم مدح تو نظم خویش</p></div>
<div class="m2"><p>چون باد از نفاذ و چو آب از صفا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحرم که هر چه یابد طبعم گهر کند</p></div>
<div class="m2"><p>چون کوه که هر چه شنیدم صدا کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک بار من به سال درون چون گیا و خار</p></div>
<div class="m2"><p>از باغ خود تو را گل و لاله عطا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزدیک تو ز خار و گیا کمترم از آنک</p></div>
<div class="m2"><p>در سال خدمت تو چو خار و گیا کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی نی نه راست گفت کی دل دهد مرا</p></div>
<div class="m2"><p>کز خدمتت زمانی خود را جدا کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر خدمتی که در وی تقصیر کرده ام</p></div>
<div class="m2"><p>ماننده نماز فریضه قضا کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بحرم شگفت نیست که گاهی تهی بوم</p></div>
<div class="m2"><p>تیغم عجب مدار که گاهی خطا کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیزارم از خدا و فرستاده خدا</p></div>
<div class="m2"><p>گر جز هوای تو به دل اندر هوا کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیگانه ام ز مردمی گر من به هیچ وقت</p></div>
<div class="m2"><p>جز با رضای تو دل خود آشنا کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از مدح و خدمتت نشوم هیچ منزوی</p></div>
<div class="m2"><p>ور چه همی ز مدح ملوک انزوا کنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورشید روی گردم هر گه که پیش تو</p></div>
<div class="m2"><p>چون چرخ پشت خویش به خدمت دو تا کنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خواندن مدیح توام چشم روشنست</p></div>
<div class="m2"><p>گویی که در دوات همی توتیا کنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون روز و شب مدیح تو گویم به سر و جهر</p></div>
<div class="m2"><p>خورشید و ماه را به فلک بر گوا کنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر دیگران به خدمتت از سیم زر کنند</p></div>
<div class="m2"><p>از خاک من به دولت تو کیمیا کنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آید به من سعادت کآیم به نزد تو</p></div>
<div class="m2"><p>بر من ثنا کنند چو بر تو ثنا کنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وقت دعاست آخر شعر و تو را خدای</p></div>
<div class="m2"><p>داد آنچه بایدت به چه معنی دعا کنم</p></div></div>