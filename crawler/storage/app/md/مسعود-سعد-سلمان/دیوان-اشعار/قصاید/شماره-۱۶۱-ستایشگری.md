---
title: >-
    شمارهٔ ۱۶۱ - ستایشگری
---
# شمارهٔ ۱۶۱ - ستایشگری

<div class="b" id="bn1"><div class="m1"><p>خسروا چون تو که دیدست افتخار و اختیار</p></div>
<div class="m2"><p>خسروان را اختیاری خسروی را افتخار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی و شیری و هر شاهی و هر شیری که هست</p></div>
<div class="m2"><p>مانده از هول تو اندر اضطراب و اضطرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات جاهت را نشانده کامگاری بر کتف</p></div>
<div class="m2"><p>عدل ملکت را گرفته بختیاری در کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عدل و حق را سعی و عون تو یسارست و یمین</p></div>
<div class="m2"><p>ملک و دین را امر و نهی تو شعارست و دثار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی گاه بزم و آسمانی گاه رزم</p></div>
<div class="m2"><p>خسروی روز شکار و کیقبادی روز بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر ارواح با کین تو بگذارد عرض</p></div>
<div class="m2"><p>عنصر اجسام بی مهر تو نپذیرد نگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجلس و درگاه تو اندر جهان گشتست و باد</p></div>
<div class="m2"><p>کعبه فریاد خواه و قبله امیدوار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر خواندم همتت را مهر از آن بفزود فخر</p></div>
<div class="m2"><p>چرخ گفتم رتبتت را رتبتت را کرد عار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پادشاه دادورز و شهریار گنج بخش</p></div>
<div class="m2"><p>دیر زی ای پادشاه و شاه زی ای شهریار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزگار پادشاهی از تو شاد و خرم است</p></div>
<div class="m2"><p>اینت عالی پادشاهی اینت خرم روزگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پایدار و استوارست از تو دین و مملکت</p></div>
<div class="m2"><p>پایداری پایدار و استواری استوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یادگار حیدر و رستم تویی اندر نبرد</p></div>
<div class="m2"><p>رستمی با گاوسار و حیدری با ذوالفقار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی گمان از آب انعام تو کوثر یک حباب</p></div>
<div class="m2"><p>بی خلاف از آتش خشم تو دوزخ یک شرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه بهار از بخشش تو گشته هنگام خزان</p></div>
<div class="m2"><p>گه خزان از مجلس تو گشته هم طبع بهار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دانش اندر حل و عقد آموزگار ملک تست</p></div>
<div class="m2"><p>به ز دانش ملک را هرگز که دید آموزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیده های بیکران چهره چرخ کبود</p></div>
<div class="m2"><p>شد سپید ایرا که ملکت را بسی کرد انتظار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ و رخشت آبدار و تابدارست و ظفر</p></div>
<div class="m2"><p>در سر آن آبدار و در تن این تابدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوی مغز و رنگ دل تیر و سنان تو نیافت</p></div>
<div class="m2"><p>وجه نام این و آن شد مغز جوی و دل گذار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه دارد مغز پیش تو نیاید در مصاف</p></div>
<div class="m2"><p>وآنکه آمد پیش تو بی دل شود در کارزار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر چه بر شیری نباشد هیچ گاوی را ظفر</p></div>
<div class="m2"><p>گردن شیران شکستی تو به گرز گاوسار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ژنده پیلان تو گردانند چون حمله برند</p></div>
<div class="m2"><p>غارها را کوه کوه و کوهها را غارغار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همچو خاک اندر درنگ و همچو آب اندر شتاب</p></div>
<div class="m2"><p>همچو آتش در نهیب و همچو باد اندر نهاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عمر و جان از هر یکی ترسان و لرزانست از آنک</p></div>
<div class="m2"><p>هر یکی چون اژدهایی جان شکار و عمر خوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون حصاری از بلندی و ز تن سنگین او</p></div>
<div class="m2"><p>پست گشته بر زمین چون خاک بر سنگین حصار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرز خارا و ز آهن خاست اهل تیغ تو</p></div>
<div class="m2"><p>پس چرا زخمش برآرد زآهن و خارا دمار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شد ز مور و مار پنداری مرکب زآنکه هست</p></div>
<div class="m2"><p>روی او بر چشم مور و حد او با زخم مار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جان بدخواهان تو در قبضه ترکان تست</p></div>
<div class="m2"><p>یک تن تنها از ایشان و ز بدخواهان هزار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کیفر از شمشیرشان برده نهنگ تیزچنگ</p></div>
<div class="m2"><p>چاشنی تیرشان خورده هژبر مرغزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این دلیران و یلان و گردنان و سرکشان</p></div>
<div class="m2"><p>نوذرند و بیژنند و رستم و اسفندیار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پادشاها هفت کشور در مقام دار و گیر</p></div>
<div class="m2"><p>هم بدین ترکان بگیر و هم بدین ترکان سپار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای گزین کردگار از گردش چرخ بلند</p></div>
<div class="m2"><p>صورت عالم دگرگون شد به صنع کردگار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بار کافور ترست از شاخ خشک بیدمشک</p></div>
<div class="m2"><p>کابر لؤلؤ بار بوده باز شد کافور بار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آب چون می بوده روشن گشته شد همچون بلور</p></div>
<div class="m2"><p>در قدح های بلورین می گسار ای میگسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پر سمن شد باغ همچون لاله گردان جام می</p></div>
<div class="m2"><p>گر چه نه وقت سمن زارست و وقت لاله زار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر رهی کآن خوشتر و هر باده ای کآن تلختر</p></div>
<div class="m2"><p>مطربا آن ره سرای و ساقیا آن باده آر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر چه بینی توده برف اندر میان بوستان</p></div>
<div class="m2"><p>نقشبند بوستان پر نقش های قندهار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زود خواهد کرد باغ و راغ و دشت و کوه را</p></div>
<div class="m2"><p>گوهر آگین همچو تاج شهریار تاجدار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نوبهاری روی بنماید چو روی دوستان</p></div>
<div class="m2"><p>گر چه یابی آب بسته بر کران رودبار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باز ابر آرد ز دریا در و لؤلؤ روز و شب</p></div>
<div class="m2"><p>تا کند بر کنگره ایوان سلطانی نثار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شهریارا ماهی آمد بس عزیز و محترم</p></div>
<div class="m2"><p>با مبارک عهد و مهر ایزد پروردگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>می به رغبت نوش و سنگ انداز کن با دوستان</p></div>
<div class="m2"><p>زانکه گردون کرد جان دشمنان را سنگسار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باده و شادی و رادی هر سه یکجا زاده اند</p></div>
<div class="m2"><p>این مر آن را پشتوان و آن مر اینرا دستیار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رای رادی خیزدت بر دست جام باده نه</p></div>
<div class="m2"><p>بار شادی بایدت در طبع تخم باده کار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای چو مهر و ابر دایم نورمند و سودمند</p></div>
<div class="m2"><p>نور این بس بی قیاس و سود آن بس بی شمار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا بتابد مهر بر عالم بسان مهر تاب</p></div>
<div class="m2"><p>تا ببارد ابر بر گیتی بسان ابر بار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کام جوی و کام یاب و کام خواه و کام ران</p></div>
<div class="m2"><p>شادکام و شاد طبع و شادمان و شاد خوار</p></div></div>