---
title: >-
    شمارهٔ ۶۰ - شکوه از حبس و زندان
---
# شمارهٔ ۶۰ - شکوه از حبس و زندان

<div class="b" id="bn1"><div class="m1"><p>چو مردمان شب دیرنده عزم خواب کنند</p></div>
<div class="m2"><p>همه خزانه اسرار من خراب کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاب شرم چو لاله ز روی بردارند</p></div>
<div class="m2"><p>چو ماه و مهر سر و روی در نقاب کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخم ز چشمم هم چهره تذرو شود</p></div>
<div class="m2"><p>چو تیره شب را هم گونه غراب کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنم به تیر قضا طعمه هژبر نهند</p></div>
<div class="m2"><p>دلم به تیر عنا مسته عقاب کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل مورد گشته است چشم من ز سهر</p></div>
<div class="m2"><p>ز آتش دلم از گل همی گلاب کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اشک چشمم چون فانه کور میخ کشند</p></div>
<div class="m2"><p>چو غنچه هیچم باشد که سیر خواب کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز صبر و خواب چه بهره بود مرا که مرا</p></div>
<div class="m2"><p>به درد و رنج دل و مغز خون و آب کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من آن غریبم و بی کس که تا به روز سپید</p></div>
<div class="m2"><p>ستارگان ز برای من اضطراب کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنالم ایرا با من فلک همی کند آنک</p></div>
<div class="m2"><p>به زخم زخمه بر ابریشم رباب کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس که بر من باران غم زنند مرا</p></div>
<div class="m2"><p>سرشک دیده صدف وار در ناب کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر آنچه هست بر این تن زنند بر دریا</p></div>
<div class="m2"><p>به رنج در دهان صدف لعاب کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک آفتم را هر روز صد طریق نهند</p></div>
<div class="m2"><p>یک اندهم را هر شب هزار باب کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تن مرا ز بلا آتشی برافروزند</p></div>
<div class="m2"><p>دلم برآرند از بر برو کباب کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز درد و وصلت یاران من آن کنم به جزع</p></div>
<div class="m2"><p>که جان پژوهان بر فرقت شباب کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همی گذارم هر شب چنان کسی کورا</p></div>
<div class="m2"><p>ز بهر روز به شب وعده عقاب کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روان شوند سبک بچگان دیده من</p></div>
<div class="m2"><p>به زیر زانوی من خاک را خلاب کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طناب بافته باشد بدان امید که باز</p></div>
<div class="m2"><p>ز صبح خیمه شب را مگر طناب کنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر این حصار ز دیوانگی چنان شده ام</p></div>
<div class="m2"><p>که اختران همه دیوم همی خطاب کنند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو من به صورت دیوان شدم چرا جوشم</p></div>
<div class="m2"><p>چو هر زمانم هم حمله شهاب کنند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر بساط زمین مفرشم کنند سزد</p></div>
<div class="m2"><p>چو سایبان من از پرده سحاب کنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به گردم اندر چندین حوادث آمد جمع</p></div>
<div class="m2"><p>که از حوادث دیگر مرا حجاب کنند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شگفت نیست که بر من همی شراب خورند</p></div>
<div class="m2"><p>چو خون دیده لبم را همی شراب کنند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به طبع طبعم چون نقره تابدار شدست</p></div>
<div class="m2"><p>که هر زمانش در بوته تیز تاب کنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا سؤال کنم خلق را که در هر حال</p></div>
<div class="m2"><p>جواب من همه ناکردن جواب کنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روا بود که زمن دشمنان براندیشند</p></div>
<div class="m2"><p>حذر ز آتش تر بهر التهاب کنند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سزای جنگند اینها که آشتی کردند</p></div>
<div class="m2"><p>نگر که اکنون با من همی عتاب کنند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خطا شمارند ار چند من خطا نکنم</p></div>
<div class="m2"><p>صواب گیرند ار چند ناصواب کنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چگونه روزی دارم نکو نگر که مرا</p></div>
<div class="m2"><p>همی ز آتش سوزنده آفتاب کنند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپید مویم بر سر بدیده اند مگر</p></div>
<div class="m2"><p>از آن به دود سیاهش همی خضاب کنند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چگونه باشد حالم چو هست راحت من</p></div>
<div class="m2"><p>بدانچه دوزخیان را همی عذاب کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر به دست خسانم چه شد نه شیران را</p></div>
<div class="m2"><p>پس از گرفتن هم خانه با کلاب کنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا درنگ نماندست از درنگ بلا</p></div>
<div class="m2"><p>بکشتنم ز چه معنی چنین شتاب کنند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو هیچ دعوت من در جهان نمی شنوند</p></div>
<div class="m2"><p>امید تا کی دارم که مستجاب کنند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به کار کرد مرا با زمانه دفترهاست</p></div>
<div class="m2"><p>چه فضل ها بودم گر به حق حساب کنند</p></div></div>