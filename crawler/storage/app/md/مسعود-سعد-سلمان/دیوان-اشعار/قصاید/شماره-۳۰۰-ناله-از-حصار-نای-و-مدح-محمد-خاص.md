---
title: >-
    شمارهٔ ۳۰۰ - ناله از حصار نای و مدح محمد خاص
---
# شمارهٔ ۳۰۰ - ناله از حصار نای و مدح محمد خاص

<div class="b" id="bn1"><div class="m1"><p>نوا گوی بلبل که بس خوش نوایی</p></div>
<div class="m2"><p>مبادا تو را زین نوا بینوایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نواهای مرغان دو سه نوع باشد</p></div>
<div class="m2"><p>تو هر دم زنی با نوایی نوایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر از عشق گویا شدستی تو چون من</p></div>
<div class="m2"><p>مبادات از رنج و انده رهایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی مرغ دیدم به دیدار نیکو</p></div>
<div class="m2"><p>ندانند ایشان به جز ژاژ خایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه جو فروشان گندم نمایند</p></div>
<div class="m2"><p>تو گندم فروشی و ارزن نمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی زند باف آفرین باد بر تو</p></div>
<div class="m2"><p>که بس طرفه مرغی و بس خوشنوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخسبند مرغان و تو شب نخسبی</p></div>
<div class="m2"><p>مگر همچو من بسته در حصن نایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگویی تو ای رنج با من چه باشی</p></div>
<div class="m2"><p>تو ای بی غمی نزد من چون نیایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به من بر بلا از فراق تو آمد</p></div>
<div class="m2"><p>نهنگ فراقی تو یا اژدهایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه دو چشمم پر از آب داری</p></div>
<div class="m2"><p>به چشم من اندر تو چون توتیایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو ای چشم من چشم داود گشتی</p></div>
<div class="m2"><p>تو ای دامنم دامن اوریایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ببر صبحت از من فراق تو یک ره</p></div>
<div class="m2"><p>که داده ست با من تو را آشنایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگرنه بنالم که طاقت ندارم</p></div>
<div class="m2"><p>چگونه کنم صبر با مبتلایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به پیش ولی نعمتم باز گویم</p></div>
<div class="m2"><p>که دارد کفش بر سخا پادشایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که او خاص شاهست و من خاص دولت</p></div>
<div class="m2"><p>بر او دولت و بخت داد این گوایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>الا این کریمی که اندر غمانم</p></div>
<div class="m2"><p>بلا را نجاتی و غم را دوایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مثل زد نباید ز نعمان و حاتم</p></div>
<div class="m2"><p>که نعمان نبردی و حاتم سخایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>محمد خصالی و آدم کمالی</p></div>
<div class="m2"><p>براهیم خلقی و یوسف لقایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر مدح و حمد و ثنا راست معدن</p></div>
<div class="m2"><p>تویی معدن حمد و قطب ثنایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیا کند باید بدر آن دهانی</p></div>
<div class="m2"><p>که از نطق او چون تویی راستایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به تو حاجتی دارم ای خاص سلطان</p></div>
<div class="m2"><p>که تو مرکز جود و کان عطایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازین شاعرانی که آیند زی تو</p></div>
<div class="m2"><p>ولیکن به علم و خرد روستایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیایند این قوم زی تو همیشه</p></div>
<div class="m2"><p>ز بهر گدایی و کالا ربایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز من بنده بر دل تو یادی نیاری</p></div>
<div class="m2"><p>نپرسی نگویی که روزی کجایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چراغیست افروخته طبع شاعر</p></div>
<div class="m2"><p>ضو آنکه فزاید که روغن فزایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو کم گشت روغنش تاریک سوزد</p></div>
<div class="m2"><p>به مقدار روغن دهد روشنایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بمیرد چو روغن ازو بازگیری</p></div>
<div class="m2"><p>چگونه بود چون فتیله فزایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا پشت بشکست گردون گردان</p></div>
<div class="m2"><p>فرو ماندم از ورزش کدخدایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نکو گردد این پشت بشکسته آنگه</p></div>
<div class="m2"><p>که از جود تو باشدش مومیایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>الا تا سکونست دایم زمین را</p></div>
<div class="m2"><p>بود پیشه باد خاک آزمایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنان باد رای جهان زی تو سرور</p></div>
<div class="m2"><p>که تا او بپاید تو با او بپایی</p></div></div>