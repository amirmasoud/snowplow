---
title: >-
    شمارهٔ ۲۴۶ - ای برادر نکو نگر به وجود
---
# شمارهٔ ۲۴۶ - ای برادر نکو نگر به وجود

<div class="b" id="bn1"><div class="m1"><p>خویش را در جهان علم کردن</p></div>
<div class="m2"><p>هست بر خویشتن ستم کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن به تیمار در هوس بستن</p></div>
<div class="m2"><p>دل به اندیشه جای غم کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشمگین بودن وز خشم خدای</p></div>
<div class="m2"><p>بر تن بی خرد رقم کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستان را و زیر دستان را</p></div>
<div class="m2"><p>به دل آورد و متهم کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست با راستی زدن در کار</p></div>
<div class="m2"><p>قامت راستی به خم کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و جان را همه طعام و شراب</p></div>
<div class="m2"><p>نغمه و لحن زیر و بم کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حرام و حلال جاهل وار</p></div>
<div class="m2"><p>روز و شب خواسته به هم کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاد ناکردن از سؤال و شمار</p></div>
<div class="m2"><p>خانه پر زر و پر درم کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لقمه لقمه ز آتش دوزخ</p></div>
<div class="m2"><p>اندین مردری شکم کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر ناپایدار چون شمنان</p></div>
<div class="m2"><p>در پرستیدن صنم کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای برادر نکونگر به وجود</p></div>
<div class="m2"><p>سازد اندیشه عدم کردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن و جان در خصومتند و سزد</p></div>
<div class="m2"><p>عقل را در میان حکم کردن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوش بر لابنه به عجز چو نیست</p></div>
<div class="m2"><p>مذهب مردمان نعم کردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرم از هیچ کس مجوی که نیست</p></div>
<div class="m2"><p>عادت هیچ کس کرم کردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با نصیبی که داری از روزی</p></div>
<div class="m2"><p>ممکنت نیست هیچ ضم کردن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست از عقل گر بیندیشی</p></div>
<div class="m2"><p>تکیه بر تیغ و بر قلم کردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه چاره کنی و نتوانی</p></div>
<div class="m2"><p>چاره این شمرده دم کردن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست مسعود سعد باب خرد</p></div>
<div class="m2"><p>دل ز کار جهان دژم کردن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رنج بر دل منه که گردون را</p></div>
<div class="m2"><p>پیشه افزونی است و کم کردن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر چه دانی بگوی از آنکه زبانت</p></div>
<div class="m2"><p>خشک باشد به وقت نم کردن</p></div></div>