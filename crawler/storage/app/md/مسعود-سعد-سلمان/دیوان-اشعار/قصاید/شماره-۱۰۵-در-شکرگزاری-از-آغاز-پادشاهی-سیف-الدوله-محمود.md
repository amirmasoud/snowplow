---
title: >-
    شمارهٔ ۱۰۵ - در شکرگزاری از آغاز پادشاهی سیف الدوله محمود
---
# شمارهٔ ۱۰۵ - در شکرگزاری از آغاز پادشاهی سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>ساقیا چون گشت پیدا نور صبح از کوهسار</p></div>
<div class="m2"><p>بر صبوحی خیز و بنشین جام محمودی بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان گشت از شعاع آفتاب آراسته</p></div>
<div class="m2"><p>همچو شخص من به خلعت های خاص شهریار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر یکی خورشید باشد بر سپهر آبگون</p></div>
<div class="m2"><p>هست بر خلعت مرا خورشید تابنده هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بود بر چرخ گردنده همیشه سعد نحس</p></div>
<div class="m2"><p>خلعتم سعدیست کانرا هیچ نحسی نیست یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاها شکر تو پیش که دانم گفت من</p></div>
<div class="m2"><p>جز به پیش کردگار ذوالجلال کامگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب گویم الهی شاه سیف الدوله را</p></div>
<div class="m2"><p>در ثبات ملک شاهی و جهانداری بدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می ده ای ساقی که روزی سخت خوب و خرم است</p></div>
<div class="m2"><p>ساتکینی جفتکان بر هر ندیمی برگمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور کسی گوید که مستم کی توانم خورد می</p></div>
<div class="m2"><p>کن به نوک موزه ترکانه او را هوشیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گو مشو مست و به پیش شاه ما هشیار باش</p></div>
<div class="m2"><p>زانکه باشد پیش او هشیار مردم نامدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کو خداوندیست عالم در همه انواع علم</p></div>
<div class="m2"><p>یادگار از خسروان کو باد دایم یادگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پادشاهی را جمال و شهریاری را شرف</p></div>
<div class="m2"><p>سروری را اختیار و خسروی را افتخار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از سنان او همی باشد نهیب اندر نهیب</p></div>
<div class="m2"><p>زینهار از تیغ او خواهد به جمله زینهار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون برافروزد حسامش در میان معرکه</p></div>
<div class="m2"><p>بدسگالش در دماغ خویشتن بیند شرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسروا تا پادشاهی در جهان موجود گشت</p></div>
<div class="m2"><p>روزگارت را همی کرد از زمانه اختیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون به تخت پادشاهی برنشستی در زمان</p></div>
<div class="m2"><p>پادشاهی پیش تو بندد میان را بنده وار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نوبهار بدسگالان شهریارا شد خزان</p></div>
<div class="m2"><p>تا رهی را خلعتی دادی بهار اندر بهار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا همی یابد زمین از دایره دایم سکون</p></div>
<div class="m2"><p>تا کند پیوسته مهر از بهر این مرکز مدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کامران و دیرزی و شاه بند و شهر گیر</p></div>
<div class="m2"><p>سیم بخش و زر ده و دشمن کش و خنجر گذار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همچنین مر بندگان خویش را گردان بزرگ</p></div>
<div class="m2"><p>گه به خلعت های فاخر گه به زر با عیار</p></div></div>