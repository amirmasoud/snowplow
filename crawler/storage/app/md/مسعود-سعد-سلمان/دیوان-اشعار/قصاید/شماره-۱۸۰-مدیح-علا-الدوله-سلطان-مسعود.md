---
title: >-
    شمارهٔ ۱۸۰ - مدیح علاء الدوله سلطان مسعود
---
# شمارهٔ ۱۸۰ - مدیح علاء الدوله سلطان مسعود

<div class="b" id="bn1"><div class="m1"><p>همیشه دشمن مالست شاه دشمن مال</p></div>
<div class="m2"><p>یکیست او را در بزم و رزم دشمن و مال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علاء دولت سلطان تاجور مسعود</p></div>
<div class="m2"><p>که تافت از فلک ملکش آفتاب کمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پناه دولت و دینست و دین و دولت ازو</p></div>
<div class="m2"><p>گرفته عز و بزرگی و دیده عز و جلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهاده بر فلک مفخرت به قدر قدم</p></div>
<div class="m2"><p>نشانده در چمن مملکت به عدل نهال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همای رامش در بزم او برآرد پر</p></div>
<div class="m2"><p>هژبر فتنه به رزمش بیفکند چنگال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهاده روی به هندوستان ز دارالملک</p></div>
<div class="m2"><p>به فرخ اختر و پیروز روز و میمون فال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشید لشکر جرار تا به مرکز غزو</p></div>
<div class="m2"><p>ره فراخ فرو بست بر جنوب و شمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز تیغ دستان بر کوهها گرفته طریق</p></div>
<div class="m2"><p>ز باد پایان در دشت ها نمانده مجال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جبال جنگی در موکبش روان که به زخم</p></div>
<div class="m2"><p>به روز معرکه از بیخ بر کنند جبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پی شکسته همی ماهی زمین را پشت</p></div>
<div class="m2"><p>به یشک خسته همه شیر آسمان را یال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کدام شاهست اندر همه جهان یکسر</p></div>
<div class="m2"><p>که از نهیبش گیرد قرار و یابد حال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدایگانا یک نکته باز خواهم راند</p></div>
<div class="m2"><p>که هست درگه عالی تو محط رحال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خزاین تو گشاده ست بر همه شعرا</p></div>
<div class="m2"><p>جواهر تو بدیشان رسیده از هر حال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منم که تشنه همی مانم و دگر طبقه</p></div>
<div class="m2"><p>رسیده اند ز انعام تو به آب زلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یمین دولت سلطان ماضی از غزنین</p></div>
<div class="m2"><p>به مدح گویان بر وقف داشتی اموال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غضایری که اگر زنده باشدی امروز</p></div>
<div class="m2"><p>به شعر من کندی فخر در همه احوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هر قصیده که از شهر ری فرستادی</p></div>
<div class="m2"><p>هزار دینار او بستدی ز زر حلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگویدی که به من تا به حشر فخر کند</p></div>
<div class="m2"><p>«هر آن که بر سر یک بیت من نویسد قال »</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همی چه گوید بنگر در آن قصیده شکر</p></div>
<div class="m2"><p>که می نماید از آن زر بیکرانه ملال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>«بس ای ملک که نه لؤلؤ فروختم به سلم »</p></div>
<div class="m2"><p>بس ای ملک که نه گوهر فروختم به جوال »</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدای داند کاندر پناه شاه جهان</p></div>
<div class="m2"><p>غضایری را می نشمرم به شعر همال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من آن کسم که گه نظم هیچ گوینده</p></div>
<div class="m2"><p>به لفظ و معنی چون من ندارد استقلال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گهی به نثر فشانم و لفظ در ثمین</p></div>
<div class="m2"><p>گهی به نظم نمایم ز طبع سحر حلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو یافتم شرف مجلس شهنشاهی</p></div>
<div class="m2"><p>گذشت از اوج سر همتم ز کبر و دلال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به گوشم آمد فرخنده دعوت دولت</p></div>
<div class="m2"><p>به چشمم آمد تابنده صورت اقبال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ولیک بخت به رغبت نمی دهد یاری</p></div>
<div class="m2"><p>جهان شوخ همی دارد آخرم دنبال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که روز جشن مرا جود شاه یاد نکرد</p></div>
<div class="m2"><p>اگر ز بخت بنالم که گویدم که منال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که گاه مدحت بودم ز جمله شعرا</p></div>
<div class="m2"><p>به وقت خدمت بودم ز زمره عمال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه پایگاه من از حشمتی فزود شرف</p></div>
<div class="m2"><p>نه دستگاه من از خلعتی گرفت جمال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه گویم آخر با مردمان لوهاور</p></div>
<div class="m2"><p>چو باز گردم و از حال من کنند سؤال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز ابر و مهر چو باران و روشنی طلبم</p></div>
<div class="m2"><p>نه التماس کجست و نه آرزوی محال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شها ملوک همه ناز شاعران بکشند</p></div>
<div class="m2"><p>تو آفتاب ملوکی بتاب تا صد سال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جهان پناهی و برگ و نوای خلق جهان</p></div>
<div class="m2"><p>سخای تست پس از فضل ایزد متعال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همیشه تا ندهد جرم ماه تابش خور</p></div>
<div class="m2"><p>همیشه تا نشود قد سرو قامت نال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو مهر بر فلک مفخرت به فخر بگرد</p></div>
<div class="m2"><p>چو سرو بر چمن مملکت به ناز ببال</p></div></div>