---
title: >-
    شمارهٔ ۱۹ - هم در مدح سلطان محمود
---
# شمارهٔ ۱۹ - هم در مدح سلطان محمود

<div class="b" id="bn1"><div class="m1"><p>چیست آن کاتشش زدوده چو آب</p></div>
<div class="m2"><p>چو گهر روشن و چو لؤلؤ ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست سیماب و آب و هست درو</p></div>
<div class="m2"><p>صفوت آب و گونه سیماب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه سطرلاب و خوبی و زشتی</p></div>
<div class="m2"><p>بنماید تو را چو اسطرلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه زمانه ست و چون زمانه همی</p></div>
<div class="m2"><p>شیب پیدا کند همی ز شباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست محراب و بامداد کنند</p></div>
<div class="m2"><p>سوی او روی چون سوی محراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست نقاش و شبه بنگارد</p></div>
<div class="m2"><p>صورت هر چه بیند از هر باب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو مشاطگان کند بر چشم</p></div>
<div class="m2"><p>جلوه روی خوب و زلف بتا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صافی آبست و تیره رنگ شود</p></div>
<div class="m2"><p>گر بدو هیچ راه یابد آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه شکل و چو تافت مهر بر او</p></div>
<div class="m2"><p>آید از نور عکس او مهتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون هوا روشن و به اندک دم</p></div>
<div class="m2"><p>پر شود روی او ز تیره سحاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روشن و راست گو گویی نیست</p></div>
<div class="m2"><p>جز دل و خاطر اولوالالباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو رای ملک پدید آرد</p></div>
<div class="m2"><p>کژی از راستی خطا ز صواب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نام او باژگونه آن لفظی است</p></div>
<div class="m2"><p>که بگویند چون خورند شراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاه محمود سیف دولت و دین</p></div>
<div class="m2"><p>که نبیند چو او زمانه به خواب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه اندر جهان نماند دیو</p></div>
<div class="m2"><p>گر شود خشم او به جای شهاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خسروان پیش او کمر بندند</p></div>
<div class="m2"><p>همچو در پیش خسروان حجاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون زمین و فلک به بزم و به رزم</p></div>
<div class="m2"><p>نشناسد مگر درنگ و شتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست معجب به جود خویش و جهان</p></div>
<div class="m2"><p>می نماید به جود او اعجاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای شهنشاه خسروی که شده ست</p></div>
<div class="m2"><p>زیر امر تو گردش دولاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه عجب گر ز بنده محجوبی</p></div>
<div class="m2"><p>سازد از ابر آفتاب حجاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه اعدای من زمن گیرند</p></div>
<div class="m2"><p>آنچه سازند با من از هر باب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از عقاب است پر آن تیری</p></div>
<div class="m2"><p>که بدو می بیفکنند عقاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دستهایم به رشته ای بستست</p></div>
<div class="m2"><p>کش ندادست جز دو دستم تاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در سکون برترم ز کوه که من</p></div>
<div class="m2"><p>در جواب عدو نگیرم تاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر چه گویند مر مرا بی شک</p></div>
<div class="m2"><p>زو نیابند خوب و زشت جواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هست بنده نبیره آدم</p></div>
<div class="m2"><p>در همه چیز اثر کند انساب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفته بدسگال چون ابلیس</p></div>
<div class="m2"><p>دور کردم از آن چو خلد جناب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شهریارا مبین تو دوری من</p></div>
<div class="m2"><p>مدح من بین چو لولؤ خوشاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در صافی نزاد هیچ صدف</p></div>
<div class="m2"><p>زر ساده نزاد هیچ تراب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا من از خدمت تو گشتم دور</p></div>
<div class="m2"><p>کم شد از محتسب مرا ایجاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همچو حرفی شدم نحیف و بلا</p></div>
<div class="m2"><p>گرد من همچو گرد حرف اعراب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می فرو باردم چو باران اشک</p></div>
<div class="m2"><p>می برآید دمم بسان سحاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیستم چون ذباب شوخ چرا</p></div>
<div class="m2"><p>دلم از ضعف شد چو پر ذباب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون غرابم ز دور بینی از آن</p></div>
<div class="m2"><p>تیره شد روز من چو پر غراب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کافری نعمتت نبوده مرا</p></div>
<div class="m2"><p>دوزخ خشمت از چه کرد عذاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر بد و نیک از تو در همه سال</p></div>
<div class="m2"><p>خلق عالم معاقبند و مثاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنکه بی خدمتی ثواب دهیش</p></div>
<div class="m2"><p>دید بایدش بی گناه عقاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من از آن بندگانم ای خسرو</p></div>
<div class="m2"><p>که نبندند طمع در اسباب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زیست دانند باستام و کمر</p></div>
<div class="m2"><p>رفت دانند با عصا و جراب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر کمانم کند فلک نجهد</p></div>
<div class="m2"><p>سخنم جز به راستی نشاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در شوم گر مرا بفرمایی</p></div>
<div class="m2"><p>در دهان هژبر تیز انیاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بنهم از برای نام تو را</p></div>
<div class="m2"><p>دیدگان زیر سکه ضراب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خسروا بر رهیت تیز مشو</p></div>
<div class="m2"><p>سیفی اندر بریدنم مشتاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این نهال نشانده را مشکن</p></div>
<div class="m2"><p>مکن آباد کرد خویش خراب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا بپوشد زمین ز سبزه لباس</p></div>
<div class="m2"><p>تا ببندد هوا ز ابر نقاب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عزی و همچو عز محبب باش</p></div>
<div class="m2"><p>سیفی و همچو سیف نصرت یاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر تو فرخنده باد ماه صیام</p></div>
<div class="m2"><p>خلد بادت ز کردگار ثواب</p></div></div>