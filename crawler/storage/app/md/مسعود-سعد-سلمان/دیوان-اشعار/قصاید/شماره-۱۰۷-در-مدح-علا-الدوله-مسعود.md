---
title: >-
    شمارهٔ ۱۰۷ - در مدح علاء الدوله مسعود
---
# شمارهٔ ۱۰۷ - در مدح علاء الدوله مسعود

<div class="b" id="bn1"><div class="m1"><p>ز غزو باز خرامید شاد و برخوردار</p></div>
<div class="m2"><p>علاء دولت مسعود شاه شیر شکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدای ناصر و نصرت رفیق و بخت قرین</p></div>
<div class="m2"><p>ظفر دلیل و زمانه مطیع و دولت یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپه به غزو فروبرده و درآورده</p></div>
<div class="m2"><p>به آتش سر خنجر ز شرک دود و دمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شیر رایت همواره پیشه کرده هوا</p></div>
<div class="m2"><p>ز شیر شرزه تهی کرده بیشه ها هموار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان فروخته زان رای آفتاب نهاد</p></div>
<div class="m2"><p>به زیر سایه آن چتر آسمان کردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به باد مرکب کرده بهار شرک خزان</p></div>
<div class="m2"><p>به ابر دولت کرده خزان عصر بهار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکنده زلزله سخت بر مسام زمین</p></div>
<div class="m2"><p>نهاده ولوله صعب بر سر کهسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حد تیغ زمین را بساط کرده ز خون</p></div>
<div class="m2"><p>به گرد رخش هوا را مظله زد ز غبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدایگانا آن خسروی که گرودن بست</p></div>
<div class="m2"><p>به خدمت تو میان بنده وار چاکروار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به طوع و طبع کند ناصر تو را یاری</p></div>
<div class="m2"><p>به جان و تن ندهد حاسدتر از نهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز رای تست خرد را دلیل و یاری مگر</p></div>
<div class="m2"><p>ز دست تست سخا را منال و دست گزار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به غزو روی نهادی و روی روز به گرد</p></div>
<div class="m2"><p>کبود کرده چو نیل و سیاه کرده چو قار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز کوه صحرا کردی همی ز صحرا کوه</p></div>
<div class="m2"><p>به آن تناور صحرانورد کوه گذار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حصار شکل هیونی که چون برانگیزیش</p></div>
<div class="m2"><p>به زخم یشک سبک برکند ز بیخ حصار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه بازداردش از گشتن آتشین میدان</p></div>
<div class="m2"><p>نه راه گیردش از رفتن آهنین دیوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز آب خنجر تو آتشی فروخت چنان</p></div>
<div class="m2"><p>کز آن سپهر و ستاره دخان نمود و شرار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان شکفت ز خون مرغزار کوشش تو</p></div>
<div class="m2"><p>که نصرت و ظفر آورد شاخ بأس تو بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آب و آتش و بادی به تیغ و نیزه و تیر</p></div>
<div class="m2"><p>به وقت حمله و هنگام رزم و ساعت کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز پشت پیل تو بر مغز شیر باری خشت</p></div>
<div class="m2"><p>که پیل شیر شکاری و شیر پیل سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کدام خسرو دانی که نه به خدمت تو</p></div>
<div class="m2"><p>گرفت آرزوی خویش را به مهر کنار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کدام رای شناسی که نه ز هیبت تو</p></div>
<div class="m2"><p>کمند تافته شد بر میان او زنار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدوی تو که گرفتار کینه تو شود</p></div>
<div class="m2"><p>شکوه نایدش از شرزه شیر و افعی و مار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه جست ز آتش و خار نهیب تو نشگفت</p></div>
<div class="m2"><p>که سر دو کند نمایدش پیش آتش و خار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو رزم را ستد و داد نام نیک یلان</p></div>
<div class="m2"><p>دو صف کشند دو سو خون دو رسته بازار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز جان فروشان در رسته ها ز خوف و رجا</p></div>
<div class="m2"><p>خروش خیزد پیش و پس و یمین و یسار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مبارزت را بر مایه سود باشد نیک</p></div>
<div class="m2"><p>بلی و بد دلی آن جا زیان کند بازار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نبرده گردان بینند چون تو را بینند</p></div>
<div class="m2"><p>چو آب و آتش در شور عرصه پیکار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به حمله رخش برون داده رستم داستان</p></div>
<div class="m2"><p>به ذوالفقار زده چنگ حیدر کرار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به سوی دشمن تو تیر تو چنان بپرد</p></div>
<div class="m2"><p>که از قریحت و از دیده فکرت و دیدار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز بند شست تو اندر گشاد چون بجهد</p></div>
<div class="m2"><p>عجب مکن که ز سکانش بگذرد سوفار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان نگر ملکا تا چگونه شعبده کرد</p></div>
<div class="m2"><p>به اعتدال شب و روز را نهاده قرار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگارگر فلک جادوی بهار آرای</p></div>
<div class="m2"><p>بهاری آورد اینک چو صد هزار نگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هوای گریان لؤلؤ فشاند بر صحرا</p></div>
<div class="m2"><p>صبای پویان شنگرف ریخت بر کهسار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شد از نشاط بهار جمال طلعت تو</p></div>
<div class="m2"><p>شکوفه ها را از خواب دیده ها بیدار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بانک موکب رعد و ز تاب خنجر برق</p></div>
<div class="m2"><p>سیاه کرد هوا را سپاه دریا بار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز سایه ابر بگسترد فرش بوقلمون</p></div>
<div class="m2"><p>ز شاخ بلبل بگشاد لحن موسیقار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو باد گشت به جوی اندر آب و لاله نگر</p></div>
<div class="m2"><p>چه مست گشت کز آن باده خورد بر ناهار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نبود باید می خواره را کم از لاله</p></div>
<div class="m2"><p>که هیچ لحظه نگردد همی ز می هشیار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به تازه تازه همی بوستان بخندد خوش</p></div>
<div class="m2"><p>به نوع نوع همی آسمان بگرید زار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نشاط جوی و فلک را به کام خود یله کن</p></div>
<div class="m2"><p>نبید خواه و جهان را به کام دل بگذار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همیشه تا به جهان زیر این دوازده برج</p></div>
<div class="m2"><p>بود جهت شش و اقلیم هفت و طبع چهار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زمانه خورده زمین را به طبع در یک سال</p></div>
<div class="m2"><p>جوان و پیر کند دور آفتاب دو بار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو را بدانچه کنی رای پیر و بخت جوان</p></div>
<div class="m2"><p>به حل و عقد ممالک مشیر باد و مشار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سر و دل فرحت را مباد رنج و ملال</p></div>
<div class="m2"><p>گل و مل طربت را مباد خار و خمار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به نور و تابش بادی همیشه چون خورشید</p></div>
<div class="m2"><p>به قدر و رتبت بادی چو گنبدوار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به فخر و محمدت و شکر و مدح مستظهر</p></div>
<div class="m2"><p>ز عمر و مملکت و عز و بخت برخوردار</p></div></div>