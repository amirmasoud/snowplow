---
title: >-
    شمارهٔ ۱۸۶ - تهنیت جلوس ملک ارسلان
---
# شمارهٔ ۱۸۶ - تهنیت جلوس ملک ارسلان

<div class="b" id="bn1"><div class="m1"><p>به عون ایزد روز رفته از شوال</p></div>
<div class="m2"><p>برآمد ز فلک دولت آفتاب کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشته پانصد و نه سال تازی از هجرت</p></div>
<div class="m2"><p>زهی مبارک ماه و زهی مبارک سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان به عدل بیاراست آن بزرگ ملک</p></div>
<div class="m2"><p>که دین و دولت ازو یافته ست فر و جمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابوالملوک ملک ارسلان بن مسعود</p></div>
<div class="m2"><p>که بحر کوه وقارست و کوه بحر نوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هفت چرخ فلک او بیافت هفت اقلیم</p></div>
<div class="m2"><p>که یافت ز تایید ایزد متعال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه روز بود که پیش از زوال چشمه مهر</p></div>
<div class="m2"><p>مخالفان را شد عمر و جان و جاه زوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهارشنبه بود و چهار گوشه تخت</p></div>
<div class="m2"><p>گرفت نصرت و تایید و دولت و اقبال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی ولیت بهم کرد زر و گوهر و در</p></div>
<div class="m2"><p>همی عدوت بخایید ریگ و سنگ و سفال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را به حیلت حاجت نه و خدای معین</p></div>
<div class="m2"><p>شده هبا و هدر جمله حیلت محتال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدایگانا تا تو به ملک بنشستی</p></div>
<div class="m2"><p>به فرخ اختر و پیروز روز و میمون فال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همای نصرت زی دولت تو گشت روان</p></div>
<div class="m2"><p>عقاب خذلان در دشمن تو زد چنگال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه ایستاده به میدان هنوز خصم توراست</p></div>
<div class="m2"><p>تو گوی ملک به یک زخم سخت کردی هال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو کوه قاف قوی شد ز فر رای تو ملک</p></div>
<div class="m2"><p>چو رود دجله روان شد ز جود دست تو مال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه بود ملک پس از سال پانصد از هجرت</p></div>
<div class="m2"><p>بدان که پانصد دیگر چنین بود در حال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بقای دولت عالی که در جهان شرف</p></div>
<div class="m2"><p>به باغ ملک چو خسرو ملک نشاند نهال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هلال ملک است این پادشاه زاده و بال</p></div>
<div class="m2"><p>بر اوج شاهی ایمن ز هر خسوف و زوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به هفت کشور گیتی بگستراند نور</p></div>
<div class="m2"><p>چو بدر گردد پیش تو این خجسته هلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو ابر گاهی در بزم بر گشاید دست</p></div>
<div class="m2"><p>چو شیر وقتی در رزم بر فرازد یال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدای عزوجل چشم بد بگرداناد</p></div>
<div class="m2"><p>ز ملکت ای ملک مال بخش اعدا مال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان درآید در قبضه تو ملک جهان</p></div>
<div class="m2"><p>چنان که قیصر و کسری شوند از عمال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر برانی شاها به قصد بصره و روم</p></div>
<div class="m2"><p>کند به پیش سپاه تو رهبری اقبال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امید هر که جز از تو امید داشت به ملک</p></div>
<div class="m2"><p>دروغ بود دروغ و محال بود محال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه بر کف تو واجبست روزی خلق</p></div>
<div class="m2"><p>از آنکه کلف تو روزی دهست و خلق عیال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سبب تویی که دهی خلق را همی روزی</p></div>
<div class="m2"><p>مسبب است بدان روزی ایزد متعال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرادهای تو شاها خدای حاصل کرد</p></div>
<div class="m2"><p>که روز روز امیدست و وقت وقت سؤال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه تا به چمن سرو نازد و بالد</p></div>
<div class="m2"><p>چو سرو در چمن مملکت بناز و ببال</p></div></div>