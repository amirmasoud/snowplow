---
title: >-
    شمارهٔ ۲۱۶ - تفاخر به دانش و گوهر خویش
---
# شمارهٔ ۲۱۶ - تفاخر به دانش و گوهر خویش

<div class="b" id="bn1"><div class="m1"><p>هر آن جواهر کز روزگار بستانم</p></div>
<div class="m2"><p>چرا دهم به خس و خار ار نه بستانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست چپ بدهم آن گهر که در یک سال</p></div>
<div class="m2"><p>بهای صد گهر از دست راست بستانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تیر هر جا ناخوانده گر همی نروم</p></div>
<div class="m2"><p>چرا که دایم سر کوفته چو پیکانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدان جهت همه کس را چو خویشتن خواهم</p></div>
<div class="m2"><p>که من به دست و دل و تیغ گوهر افشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن نتیجه جانست جان چرا کاهم</p></div>
<div class="m2"><p>گمان مبر که چو پروانه دشمن جانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر جهان خرد خوانیم رواست که من</p></div>
<div class="m2"><p>هم آخشیجم و هم مرکزم هم ارکانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلی به فرمان گویم اگر هجا گویم</p></div>
<div class="m2"><p>از آنکه قول خداوند را به فرمانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخوان ز قرآن بر از یحب و ما یظلم</p></div>
<div class="m2"><p>بدان طریق روم زانکه اهل قرآنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی که خانه و خوانش ندیده ام هرگز</p></div>
<div class="m2"><p>به مدح او سخن چرب و خوش چرا رانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گاه خدمت بر دستها چو بوسه دهم</p></div>
<div class="m2"><p>چنان بگریم گویی که ابر نیسانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چهار گوهر و هفت اخر و دوازده برج</p></div>
<div class="m2"><p>هر آنچه بینی من صد هزار چندانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من از دوازده و هفت و چار بگذشتم</p></div>
<div class="m2"><p>چه گر به صورت با خلق عصر یکسانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علوم عالم دانم ولیکن اندر عصر</p></div>
<div class="m2"><p>اگر دو مردم دانم بدان که نادانم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرد پشیمان نبود ز مدح گفتن من</p></div>
<div class="m2"><p>ز مدح گفتن این مهتران پشیمانم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سزد که فخر کند روزگار بر سخنم</p></div>
<div class="m2"><p>از آنکه در سخن از نادران گیهانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدای داند کز شعر نام جویم و بس</p></div>
<div class="m2"><p>وگرنه جز به شهادت زبان نگردانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفتم این وز من سر به سر سماع کنند</p></div>
<div class="m2"><p>درست و راست که مسعود سعد سلمانم</p></div></div>