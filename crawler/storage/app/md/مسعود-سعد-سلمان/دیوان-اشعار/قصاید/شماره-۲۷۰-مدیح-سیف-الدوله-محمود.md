---
title: >-
    شمارهٔ ۲۷۰ - مدیح سیف الدوله محمود
---
# شمارهٔ ۲۷۰ - مدیح سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>گر نه شاگرد کف شاه جهان شد مهرگان</p></div>
<div class="m2"><p>چون کف شاه جهان پر زر چرا دارد جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نشد باد خزان را رهگذر بر تیغ او</p></div>
<div class="m2"><p>پس چرا شد بوستان دیناری از باد بزان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راست گویی منهزم گشت از خزان باد بهار</p></div>
<div class="m2"><p>چون سپاه اندر هزیمت ریخت زر بیکران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر گریان شد طلایه نوبهار اندر هوا</p></div>
<div class="m2"><p>گشت ناپیدا چو آمد نوبت باد خزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست گویی بود بلبل مدح خوان نوبهار</p></div>
<div class="m2"><p>چون خزان آمد شد از بیم خزان بسته دهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زعفران اصلی بود مر خنده را هست این درست</p></div>
<div class="m2"><p>هر که او خندان نباشد خنده ش آرد زعفران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون خزان مر بوستان را زعفران داد ای شگفت</p></div>
<div class="m2"><p>پس چرا باز ایستاد از خنده خندان بوستان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا ز بسیاری که دادش بازگشتست او به عکس</p></div>
<div class="m2"><p>هر چه از حد بگذرد ناچار گردد ضد آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز نقصان گیرد اکنون همچو عمر بدسگال</p></div>
<div class="m2"><p>شب بیفزاید کنون چون بخت شاه کامران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب روشن گشت و صافی چون سنان و تیغ او</p></div>
<div class="m2"><p>شاخ زرد و چفته شد چون پشت و روی بندگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطب ملت سیف دولت شهریار ملک گیر</p></div>
<div class="m2"><p>تاج شاهی عز دولت خسرو گیتی ستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه ابوالقاسم ملک محمود آن کز هیبتش</p></div>
<div class="m2"><p>لرزه گیرد گاه رزم او زمین و آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیغ او چون برفروزد آتش اندر کارزار</p></div>
<div class="m2"><p>جان بدخواهان برآید زو به کردار دخان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه از بیمش بریزد ناخن ببر هژبر</p></div>
<div class="m2"><p>وانکه از هولش بدرد زهره شیر ژیان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه وصف او نگنجد هیچ کس را در یقین</p></div>
<div class="m2"><p>وانکه نعت او نیاید هیچ کس را در گمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فر خجسته رای او بر جامه شاهی علم</p></div>
<div class="m2"><p>گستریده نام او بر نامه دولت نشان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر چه او بیند بود دیدار او عین صواب</p></div>
<div class="m2"><p>هر چه او گوید بود گفتار او سحر بیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مشتری و زهره را هرگز نبودی حکم سعد</p></div>
<div class="m2"><p>گر نبودی قدر او با هر دوان کرده قران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر نبودی از برای ساز او را نامدی</p></div>
<div class="m2"><p>در ناسفته ز دریا زر پاکیزه ز کان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طرفهای ساز بگشادند در مدحش دهن</p></div>
<div class="m2"><p>کرد گردون هر یکی را گوهری اندر دهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای جلال پادشاهی وی جمال خسروی</p></div>
<div class="m2"><p>هستی اندر جاه و رتبت اردشیر و اردوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون به گوش آمد صریر کلک تو بدخواه را</p></div>
<div class="m2"><p>بشنود هم در زمان از تن صفیر استخوان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر نه قطب دولت و بخت جوان شد تخت تو</p></div>
<div class="m2"><p>پس چرا گردند گردش دولت و بخت جوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مهرگان آمد به خدمت شهریارا نزد تو</p></div>
<div class="m2"><p>در میان بوستان بگشاد گنج شایگان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باده چون زنگ خواه اندر نوای نای و چنگ</p></div>
<div class="m2"><p>نوش کن از دست حوری دلبر نوشین روان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای به تو میمون و فرخ روزگار خسروی</p></div>
<div class="m2"><p>بر تو فرخ باد و میمون خلعت شاه جهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همچنین یادی همیشه نزد شاهنشه عزیز</p></div>
<div class="m2"><p>همچنین باد از تو دایم شاه شاهان شادمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا همی دولت بود در دولت عالی به ناز</p></div>
<div class="m2"><p>تا همی نعمت بود در نعمت باقی بمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مملکت افزون و همچون مملکت بفروز کار</p></div>
<div class="m2"><p>روزگارت فرخ و چون روزگارت مهرگان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>التجای تو به بخت آمد و نعم الملتجاء</p></div>
<div class="m2"><p>ایزدت دایم معین والله خیرالمستعان</p></div></div>