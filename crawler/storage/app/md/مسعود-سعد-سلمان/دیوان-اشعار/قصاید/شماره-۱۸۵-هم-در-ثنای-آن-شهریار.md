---
title: >-
    شمارهٔ ۱۸۵ - هم در ثنای آن شهریار
---
# شمارهٔ ۱۸۵ - هم در ثنای آن شهریار

<div class="b" id="bn1"><div class="m1"><p>ای اختیار ایزد دادار ذوالجلال</p></div>
<div class="m2"><p>تاج از تو با شرف شد و تخت از تو با جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسعود شهریاری کز فر عدل تو</p></div>
<div class="m2"><p>بر ملک روزگار چو نام تو شد به فال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده نهال جاه تو را دست مملکت</p></div>
<div class="m2"><p>آورده بار عدل و سخا شاخ این نهال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوید تو را زمانه و خواند تو را فلک</p></div>
<div class="m2"><p>برجیس با سعادت و خورشید بی همال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غران هزبر بر کند از حشمت تو چنگ</p></div>
<div class="m2"><p>پران عقاب بفکند از هیبت تو بال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبع سبع گذشت که جان عدوت خورد</p></div>
<div class="m2"><p>زان پس که بود بر تن و بر جان او وبال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آورد چند مژده شمال امان تو را</p></div>
<div class="m2"><p>از ملک بی کرانه و از عمر بی زوال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاها به حال بنده مادح نگاه کن</p></div>
<div class="m2"><p>کز روزگار بروی شوریده گشت حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کرده چرخ موکب دولت ز من تهی</p></div>
<div class="m2"><p>نالم همی ز انده چون مرکب از دوال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شصت و دو سالگی ز تن من ببرد زور</p></div>
<div class="m2"><p>زان پس که بود در همه میدان مرا مجال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندک شدست صبرم و بسیار گشته غم</p></div>
<div class="m2"><p>از اندکی دخل و ز بسیاری عیال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آرام و خور به روز و شب از من جدا شدست</p></div>
<div class="m2"><p>از هول مرگ دشمن و از بیم قیل و قال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ورچه تنم به ضعف شد از رنج هر زمان</p></div>
<div class="m2"><p>آید همی قویترم این شعر با کمال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شیر مصاف رزمم و پر دل ترم ز شیر</p></div>
<div class="m2"><p>وز بیم یاوه گویان بد دل تر از شکال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از چند گونه بطلان بر من نهند و من</p></div>
<div class="m2"><p>زان بیگنه که باد زبان حسود لال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من خود ز وام ها که درو غرقه گشته تن</p></div>
<div class="m2"><p>با دهر در نبردم و با چرخ در جدال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاها اگر بخواهد رای بلند تو</p></div>
<div class="m2"><p>از کار این رهی بشود وهن و اختلال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از نان و جامه چاره نباشد همی مرا</p></div>
<div class="m2"><p>این هر دو می بباید گر نیست جاه و مال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آرزوی آنم کز ملک وضعیتی</p></div>
<div class="m2"><p>آرد به ربع برزگرم ده قفیز گال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کدیه نبود خصلت بنده به هیچ وقت</p></div>
<div class="m2"><p>هر چند شاعران را کدیه بود خصال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرگز نبود و نیز نباشد که باشدم</p></div>
<div class="m2"><p>از منعمی درآمد و از مکرمی منال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جز در مدایح تو نخیزد مرا سخن</p></div>
<div class="m2"><p>جز بر مواهب تو نباشد مرا سؤال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر ز ابر آب خواهم و از آفتاب نور</p></div>
<div class="m2"><p>چون بنگرم نباشد نزد خرد محال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چون دیگران توانگر گردم به یک نظر</p></div>
<div class="m2"><p>از آن دهن مرفه گردم به یک مثال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزی خلق گیتی اندر نوال تست</p></div>
<div class="m2"><p>پاینده باد شاها در گیتی این نوال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا مهر و سرو باشد و باشد درین جهان</p></div>
<div class="m2"><p>زین بر هوا شعاع و از آن بر زمین ضلال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دیدار تو چهر مهر منیر از نجوم چرخ</p></div>
<div class="m2"><p>ایام تو چو فصل بهار از فصول سال</p></div></div>