---
title: >-
    شمارهٔ ۵۵ - هم در مدح او
---
# شمارهٔ ۵۵ - هم در مدح او

<div class="b" id="bn1"><div class="m1"><p>امیر غازی محمود رای میدان کرد</p></div>
<div class="m2"><p>نشاط مرکب میمون و گوی و چوگان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین میدان بر اوج چرخ فخر آورد</p></div>
<div class="m2"><p>چو شاه گیتی رای نشاط میدان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک ز ترس فراموش کرد دوران را</p></div>
<div class="m2"><p>چو اسب شاه در آوردگاه دوران کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بیم آنکه رسد گوی شاه بر خورشید</p></div>
<div class="m2"><p>به گرد تاری خورشید روی پنهان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دید گردون دوران شاه در میدان</p></div>
<div class="m2"><p>همی نیارد آن روز هیچ دوران کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو هاله گاه شهنشاه اوج گردون بود</p></div>
<div class="m2"><p>گذار گوی ز چوگان بر اوج کیوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سم مرکب روی سپهر تاری کرد</p></div>
<div class="m2"><p>به زخم چوگان چشم ستاره حیران کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو دید چوگان مر شاه را چو غران شیر</p></div>
<div class="m2"><p>به دستش اندر خود را چو مار پیچان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو دید شاه چو پیچنده مار چوگان را</p></div>
<div class="m2"><p>نشاط و رامش و شادی هزار چندان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر نه مرکب میمونش هست بادبزان</p></div>
<div class="m2"><p>چرا به رفتن با باد عهد و پیمان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر نگین سلیمان به دست خسرو ماست</p></div>
<div class="m2"><p>که چون سلیمان مرباد را به فرمان کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا سلیمان خود نام مهر سیفی داشت</p></div>
<div class="m2"><p>که باد چونان فرمانبری سلیمان کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسا کها که بر آن کوه شاه چوگان زد</p></div>
<div class="m2"><p>به سم مرکب که پیکرش بیابان کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسا شها که به گشت او ز دوستی ملک</p></div>
<div class="m2"><p>بسا امیر که با رأی شاه عصیان کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تیر شاه مر این را چو تیر بی پر کرد</p></div>
<div class="m2"><p>به تیغ باز مر آن را چو تیغ بی جان کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عجب مدار که محمود سیف دولت و دین</p></div>
<div class="m2"><p>به بخت و دولت عالی چنین فراوان کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آنچه جست همه خشندی سلطان جست</p></div>
<div class="m2"><p>هر آنچه کرد ز بهر رضای یزدان کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ایا شهی که جهان را کف تو داد نسق</p></div>
<div class="m2"><p>چنانکه رای تو مر ملک را به سامان کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر آن کسی که همی کینه جست با تو به دل</p></div>
<div class="m2"><p>نه دیر زود که بخت بدش پشیمان کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو آن جوادی شاها که آز گیتی را</p></div>
<div class="m2"><p>سخاوت تو بدست فنا گروگان کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همیشه جایگهت بوستان دولت باد</p></div>
<div class="m2"><p>که دولت تو جهان را بسان بستان کرد</p></div></div>