---
title: >-
    شمارهٔ ۲۴۴ - نکوهش بروج دوازده گانه
---
# شمارهٔ ۲۴۴ - نکوهش بروج دوازده گانه

<div class="b" id="bn1"><div class="m1"><p>ازین دوازده برجم رسید کار به جان</p></div>
<div class="m2"><p>که رنج دیدم از هر یکی به دیگر سان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حمل سرود نوا شد به من همی شب و روز</p></div>
<div class="m2"><p>چنانکه بختم ازو گشت رنجه و پژمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بداد ثور بسی شیر اول و آخر</p></div>
<div class="m2"><p>به یک لگد که برو زد بریخت ناگاهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شخص جوزا هر دو شدند جفت به هم</p></div>
<div class="m2"><p>نخست کرت زادند بهر من احزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه سرطان با من به هر کجا که روم</p></div>
<div class="m2"><p>همی رود کژ و ناچار کژ رود سرطان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسد بسان اسد سهمگین و خشم آلود</p></div>
<div class="m2"><p>همی بخاید بر من ز کین من دندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سنبله همه داس آمدست قسمت من</p></div>
<div class="m2"><p>اگر چه دانه او هست قسمت دگران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب ز میزان دارم از آن که روزی من</p></div>
<div class="m2"><p>به گاه دادن بر سخته می دهد میزان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چو عقرب عقرب همی زند سر نیش</p></div>
<div class="m2"><p>که درد آن نشود به ز دارو و درمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه قوس به من بر بسان قوس بزه</p></div>
<div class="m2"><p>همی زند به دلم بر زاند هان پیکان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز جدی هست فزون رنج من از آنکه به دل</p></div>
<div class="m2"><p>چریده سبزه لهوم ز روضه امکان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجب ز دلو همی آیدم که نوبت من</p></div>
<div class="m2"><p>تهی برآید از چاه و من چنین عطشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز حوت خاری جسته ست مر مرا در حلق</p></div>
<div class="m2"><p>که هر زمان کنم از درد او هزار افغان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنین دوازده دشمن که مر مراست کراست</p></div>
<div class="m2"><p>که با همه ز یکی خویشتن نداشت توان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به حکمشان کم و بیش توانگر و درویش</p></div>
<div class="m2"><p>ز امرشان بد و نیک رعیت و سلطان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدین دوازده دشمن بگو چگونه زید</p></div>
<div class="m2"><p>اسیر دل شده مسعود سعدبن سلمان</p></div></div>