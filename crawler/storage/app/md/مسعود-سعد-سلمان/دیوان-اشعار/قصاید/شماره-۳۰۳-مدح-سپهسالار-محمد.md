---
title: >-
    شمارهٔ ۳۰۳ - مدح سپهسالار محمد
---
# شمارهٔ ۳۰۳ - مدح سپهسالار محمد

<div class="b" id="bn1"><div class="m1"><p>جهان را نباشد چنین روزگاری</p></div>
<div class="m2"><p>که آراید او را چنان نامداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر سر کشان زمانه محمد</p></div>
<div class="m2"><p>که دولت ندارد چو او یادگاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صف آرای پیلی کمربند شیری</p></div>
<div class="m2"><p>جهانگیر گردی سپه کش سواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عفو و ز خشمش ولی و عدو را</p></div>
<div class="m2"><p>فروزنده نوری و سوزنده ناری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه بی مادحش در جهان بزمگاهی</p></div>
<div class="m2"><p>نه بی سایلش بر زمین رهگذاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه با فکرتش اختری را شعاعی</p></div>
<div class="m2"><p>نه با هیبتش آتشی را شراری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه آثار مردی او را کرانی</p></div>
<div class="m2"><p>نه آیات رادی او را شماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب کین او را نیابی صباحی</p></div>
<div class="m2"><p>می مهر او را ندانی خماری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شده شرک را هول او پای بندی</p></div>
<div class="m2"><p>بده ملک را رای او دستیاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شده بحر با طبع او چون سرابی</p></div>
<div class="m2"><p>بود ابر با دست او چون غباری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شکسته سپاهی به هر رزمگاهی</p></div>
<div class="m2"><p>دریده مصافی به هر کارزاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برآورده گردی ز هر تند کوهی</p></div>
<div class="m2"><p>فرورانده سیلی بهر ژرف غاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو از خون گردان بجوشد فراتی</p></div>
<div class="m2"><p>چو از جان مردان برآید بخاری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زمین بر دلیران شود چون تنوری</p></div>
<div class="m2"><p>هوا بر سواران شود چون حصاری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نباشدش ترس از چنان صعب حالی</p></div>
<div class="m2"><p>نباشدش باک از چنان هول کاری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نوردد زمین و گذارد زمانه</p></div>
<div class="m2"><p>به هامون نوردی و دریا گذاری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زیر اندرش باره غرنده شیری</p></div>
<div class="m2"><p>به دست اندرش نیزه پیچنده ماری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شگفتی از آن خنجر مرگ سطوت</p></div>
<div class="m2"><p>که جز جان شیران نجوید شکاری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به خون هزبران خونخواره ویحک</p></div>
<div class="m2"><p>چرا تشنه باشد چنان آبداری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی آنکه جز کوششت نیست رایی</p></div>
<div class="m2"><p>زهی آنکه جز بخششت نیست کاری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین باشد و جز بدینسان نباشد</p></div>
<div class="m2"><p>کرا بود چون دولت آموزگاری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فلک بافدت هر زمانی لباسی</p></div>
<div class="m2"><p>ز تأیید پودی ز اقبال تاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ازین پیش بی حرز مدح تو بودم</p></div>
<div class="m2"><p>چو آسیمه هوشی و دیوانه ساری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنون گشته ام در ثنا عندلیبی</p></div>
<div class="m2"><p>چون من یافتم در پناهت بهاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو شاه یلانی و بنمایمت من</p></div>
<div class="m2"><p>عروسی ز مدحت به زینت نگاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی تا برآید به هر کشتمندی</p></div>
<div class="m2"><p>همی تا بروید به هر مرغزاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز هر تخم بیخی ز هر بیخ تردی</p></div>
<div class="m2"><p>ز هر ترد شاخی ز هر شاخ باری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روان باد حکم تو بر هر سپهری</p></div>
<div class="m2"><p>رسان باد نام تو بر هر دیاری</p></div></div>