---
title: >-
    شمارهٔ ۳۷ - در آغاز گرفتاری ساخته است
---
# شمارهٔ ۳۷ - در آغاز گرفتاری ساخته است

<div class="b" id="bn1"><div class="m1"><p>تا مرا بود بر ولایت دست</p></div>
<div class="m2"><p>بودم ایزد پرست و شاه پرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امر شه را و حکم الله را</p></div>
<div class="m2"><p>نبدادم به هیچ وقت از دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به غزو و به شغل داشتمی</p></div>
<div class="m2"><p>دشمنان را از آن همی دل خست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به کفار می نهادم روی</p></div>
<div class="m2"><p>بس کس از تیغ من همی به نرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یکی حمله من افتادی</p></div>
<div class="m2"><p>خیل دشمن ز ششهزار نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر از زخم تیغ من آهن</p></div>
<div class="m2"><p>حلقه گشت و ز زخم تیغ بجست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد اکنون دو پای من بگرفت</p></div>
<div class="m2"><p>خویشتن در حمایتم پیوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من کنون از برای راحت او</p></div>
<div class="m2"><p>به گه خفتن و بخاست و نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست در دست برده چون مصروع</p></div>
<div class="m2"><p>پای در پای می کشم چون مست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس که گویند از حمایت اگر</p></div>
<div class="m2"><p>بکشی دست و رسم آن آئین هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز به فرمان شهریار جهان</p></div>
<div class="m2"><p>باز کی دارم از حمایت دست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نگوید کسی که از سر جهل</p></div>
<div class="m2"><p>بنده مسعود امان خود بشکست</p></div></div>