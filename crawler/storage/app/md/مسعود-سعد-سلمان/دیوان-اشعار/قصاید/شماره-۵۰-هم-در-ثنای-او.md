---
title: >-
    شمارهٔ ۵۰ - هم در ثنای او
---
# شمارهٔ ۵۰ - هم در ثنای او

<div class="b" id="bn1"><div class="m1"><p>تا جهانست ملک سلطان باد</p></div>
<div class="m2"><p>بر جهانش به ملک فرمان باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه مسعود کاختر مسعود</p></div>
<div class="m2"><p>در مرادش درست پیمان باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه دعوی طالع میمونش</p></div>
<div class="m2"><p>در معانی بدیع برهان باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن همت سرافرازش</p></div>
<div class="m2"><p>گردن چرخ را گریبان باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کفش بر مثال های نفاذ</p></div>
<div class="m2"><p>عز توقیع و حسن عنوان باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رای او را بدانچه روی نهد</p></div>
<div class="m2"><p>همه دشوار گیتی آسان باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عزم او را بدانچه قصد کند</p></div>
<div class="m2"><p>کم و بیش زمانه یکسان باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسوت فخر و فرش جاهش را</p></div>
<div class="m2"><p>رنگ انواع و نقش الوان باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانه و شاخ و باغ مجلس او</p></div>
<div class="m2"><p>دانه در و شاخ و مرجان باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در طربناک میزبانی بخت</p></div>
<div class="m2"><p>نهمت او عزیز مهمان باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در زمین های خشک سال نیاز</p></div>
<div class="m2"><p>جود او سودمند باران باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کانچه خواهند گنج او گشتست</p></div>
<div class="m2"><p>که فزاینده گنج اوکان باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شیر چرخ ار عدوش را نخورد</p></div>
<div class="m2"><p>کند چنگ و شکسته دندان باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زیر خایسک رنج مغز عدو</p></div>
<div class="m2"><p>تارک زخم خوار سندان باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دم و چشم مخالف از تف و نم</p></div>
<div class="m2"><p>باد ایلول و ابر نیسان باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر که بی غم نخواهدش همه عمر</p></div>
<div class="m2"><p>غمش افزون و عمر نقصان باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیر فرمانش بر نشانه و قصد</p></div>
<div class="m2"><p>سخت سوفار و تیز پیکان باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باس او در مصاف کوشش حق</p></div>
<div class="m2"><p>چیره دست و فراخ میدان باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر غلامیش روز جنگ و نبرد</p></div>
<div class="m2"><p>رستم زال زر و دستان باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نصرت و فتح او به هندستان</p></div>
<div class="m2"><p>سخت بسیار و بس فراوان باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بانگ آهنگ او به نصرت و فتح</p></div>
<div class="m2"><p>در عراقین و در خراسان باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ظفر خاتم سلیمانیش</p></div>
<div class="m2"><p>اثر خاتم سلیمان باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وقت پیکار نقش خانه فتح</p></div>
<div class="m2"><p>نفس آن حله پوش عریان باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه ز الماس او چو عقد گهر</p></div>
<div class="m2"><p>نظم دولت همه به سامان باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه ز پروینش چون بنات النعش</p></div>
<div class="m2"><p>جمع دشمن همه پریشان باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز بازار قدرت او را</p></div>
<div class="m2"><p>عمر و جان بی بها و ارزان باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>معجزاتش ز دست سلطانست</p></div>
<div class="m2"><p>که فلک زیر پای سلطان باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در کف او به زخم فرعونان</p></div>
<div class="m2"><p>نیزه سرگزای ثعبان باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حفظ و عون خدای عزوجل</p></div>
<div class="m2"><p>بر سر و تنش خود و خفتان باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دست با رحم و تیغ بی رحمش</p></div>
<div class="m2"><p>گه زرافشان و گه سرافشان باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر زمین و هوای دولت او</p></div>
<div class="m2"><p>باد اقبال و ابر احسان باد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>باد نو جامه بخت او و ازو</p></div>
<div class="m2"><p>جامه دشمنانش خلقان باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حشمتش را مضای بهرام است</p></div>
<div class="m2"><p>رتبتش را علو کیوان باد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عقل او حزم عالم عقل است</p></div>
<div class="m2"><p>جان او ذات عالم جان باد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عدلش از عزم و حزم اوقاتست</p></div>
<div class="m2"><p>ملکش از چرخ ثابت ارکان باد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پشت شاهان به پیش ایوانش</p></div>
<div class="m2"><p>خم گرفته چو طاق ایوان باد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر چه در سر نباشدش آن نیست</p></div>
<div class="m2"><p>هر چه در دل بگرددش آن باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مدحتش را هزار نظام است</p></div>
<div class="m2"><p>هر یکی را هزار دیوان باد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر سر دفتر مدایح او</p></div>
<div class="m2"><p>شعر مسعود سعد سلمان باد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>صد ثناخوان که یک تن است چو او</p></div>
<div class="m2"><p>بزم او را دو صد ثناخوان باد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این زمستان بهار دولت اوست</p></div>
<div class="m2"><p>آفرین بر چنین زمستان باد</p></div></div>