---
title: >-
    شمارهٔ ۲۹ - در مدح ابونصر پارسی و شرح گرفتاری
---
# شمارهٔ ۲۹ - در مدح ابونصر پارسی و شرح گرفتاری

<div class="b" id="bn1"><div class="m1"><p>از پس من غمست و پیش غم است</p></div>
<div class="m2"><p>ز بر من نمست و زیر نم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دل بسته خسته درد است</p></div>
<div class="m2"><p>وین تن خسته بسته الم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجبا هر چه بیش می نالم</p></div>
<div class="m2"><p>مرمرا رنج بیش و صبر کم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی شمار انده است بر من جمع</p></div>
<div class="m2"><p>این بلا بین کزین شمرده دم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش طبع و دود نیاز</p></div>
<div class="m2"><p>همه از پخت دوزخ شکم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فزازنده سپهر بلند</p></div>
<div class="m2"><p>وین شگفت این بزرگتر قسم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که همه وجه بر من مسکین</p></div>
<div class="m2"><p>از همه کس تعدی و ستم است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه توان کرد کانچه بود و بود</p></div>
<div class="m2"><p>بوده حکم و رفته قلم است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصه خویش چند پردازم</p></div>
<div class="m2"><p>به کریمی که صورت کرم است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه بونصر پارسی که چو مهر</p></div>
<div class="m2"><p>به همه فضل در جهان علم است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در هنر تاج گوهر عربست</p></div>
<div class="m2"><p>در نسب فخر دوده عجم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کف کافیش بحری از جود است</p></div>
<div class="m2"><p>طبع صافیش گنجی از حکم است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در جهانش به مکرمت دست است</p></div>
<div class="m2"><p>بر سپهرش ز مرتبت قدم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رزمش افروخته تر از سقر است</p></div>
<div class="m2"><p>بزمش آراسته تر از ارم است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از بد روزگار معصوم است</p></div>
<div class="m2"><p>به بر شهریار محترم است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پاسخ من چرا همه لا کرد</p></div>
<div class="m2"><p>چون جواب همه کسش نعم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دل بدان خوش همی کنم کآخر</p></div>
<div class="m2"><p>به حقیقت وجود را عدم است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد اقبال در پرستش او</p></div>
<div class="m2"><p>تاشمن در پرستش صنم است</p></div></div>