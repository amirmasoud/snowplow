---
title: >-
    شمارهٔ ۱۰۸ - ستایش ثقة الملک
---
# شمارهٔ ۱۰۸ - ستایش ثقة الملک

<div class="b" id="bn1"><div class="m1"><p>ای که در پیش تخت هیچ ملک</p></div>
<div class="m2"><p>هیچ سرکش چو تو نبست کمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شده رزق را به کف ضامن</p></div>
<div class="m2"><p>وی شده ملک را به حق داور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عدل دیده ز رای تو قوت</p></div>
<div class="m2"><p>جور برده ز عدل تو کفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزم تو اصل سایه طوبی</p></div>
<div class="m2"><p>جود تو یمن چشمه کوثر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد جود تو عدل را کسوت</p></div>
<div class="m2"><p>بست رأی تو ملک را زیور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع تو بر طرب گشاید راه</p></div>
<div class="m2"><p>رای تو در شرف نماید در</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زمانه ز ابر دو کف تو</p></div>
<div class="m2"><p>نه عرض قایم است نه جوهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چاکران تو اند نعمت و ناز</p></div>
<div class="m2"><p>بندگان تو اند فتح و ظفر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کینه تو به آب دریا جست</p></div>
<div class="m2"><p>از همه روی او بخاست شرر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دم به آتش فکند مهرت باز</p></div>
<div class="m2"><p>ز گل سرخ رست نیلوفر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>و آتش خشمت ار زبانه دهد</p></div>
<div class="m2"><p>بفسرد زو زبانه آذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عزم تو گر نبرد جوید هیچ</p></div>
<div class="m2"><p>کند از حزم جوشن و مغفر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شودش تیغ صبح در کف تیغ</p></div>
<div class="m2"><p>شودش قرص آفتاب سپر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خیره ماند از عطای تو دریا</p></div>
<div class="m2"><p>لنگ شد با مضای تو صرصر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاطب دولت تو نیست شگفت</p></div>
<div class="m2"><p>گر بر اوج فلک نهد منبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کارسازان کام های تو اند</p></div>
<div class="m2"><p>بر خم هفت چرخ هفت اختر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیده و عمر روز را کیوان</p></div>
<div class="m2"><p>تیره دارد به بدسگال تو بر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر سعادت که مشتری دارد</p></div>
<div class="m2"><p>بر تو باشد ز گنبد اخضر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست بهرام جنگی خون ریز</p></div>
<div class="m2"><p>زد به مغفر عدوت بر خنجر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گشت روشن ز فر طلعت تو</p></div>
<div class="m2"><p>چشم خورشید روشنی گستر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وز برای نشاط مجلس تو</p></div>
<div class="m2"><p>زهره بر چرخ گشت خنیاگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گه و بیگه عطارد جادو</p></div>
<div class="m2"><p>شده با نوک کلک تو همسر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ماه بی نور بوده در خلقت</p></div>
<div class="m2"><p>از برای شب تو گشت انور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای به هر همتی جهان افروز</p></div>
<div class="m2"><p>وی به هر دانشی هنر پرور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشته مدح من و سخاوت تو</p></div>
<div class="m2"><p>خرم و شادمان ز یکدیگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به ز من نیست هیچ مدحتگوی</p></div>
<div class="m2"><p>به ز تو نیست هیچ مدحت خر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر منت نعمت است ده گونه</p></div>
<div class="m2"><p>وز منت مدحت است ده دفتر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر من آن کرده ای در این زندان</p></div>
<div class="m2"><p>که شد اندر میان خلق سمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مر مرا از عطای تو این جا</p></div>
<div class="m2"><p>هست هر گونه نعمتی بی مر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تنگ بر تنگ جامه دارم و فرش</p></div>
<div class="m2"><p>بدره بر بدره سیم دارم و زر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لیکن از درد و رنج و بیماری</p></div>
<div class="m2"><p>جانم افتاده در نهیب و خطر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به خدای ار همی شود ممکن</p></div>
<div class="m2"><p>که بگردم ز ضعف بر بستر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل من خون شده ز خون شکم</p></div>
<div class="m2"><p>اشک من خون شده ز خون جگر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تنم از رنج تافته چو رسن</p></div>
<div class="m2"><p>پشتم از باد درد چون چنبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گشته غرقه ز اشک چون کشتی</p></div>
<div class="m2"><p>مانده ساکن ز بند چون لنگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>متردد چو ناروان خامه</p></div>
<div class="m2"><p>متحیر چو بی روان پیکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دل بریان من پر اندیشه</p></div>
<div class="m2"><p>دیده را بسته بر بلای سهر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زان که من داشتم همه محفوظ</p></div>
<div class="m2"><p>جز ثنای توام نماند از بر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دهن من طعم زهر شدست</p></div>
<div class="m2"><p>وندر او مدح تو به ذوق شکر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کرده خوشبوی روزگار مرا</p></div>
<div class="m2"><p>آتش دل چو آتش مجمر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این همه هست و تن ز بیماری</p></div>
<div class="m2"><p>مانده اندر عقوبتی منکر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون همه حال خود چنین بینم</p></div>
<div class="m2"><p>زنده بودن نیایدم باور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون مرا در نوشت گردش چرخ</p></div>
<div class="m2"><p>شخص من شد به زیر خاک اندر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>والله ار چون منی دگر بینی</p></div>
<div class="m2"><p>به همه نوع در کمال و هنر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شکرهای تو در نوشته به جان</p></div>
<div class="m2"><p>می برم پیش ایزد داور</p></div></div>