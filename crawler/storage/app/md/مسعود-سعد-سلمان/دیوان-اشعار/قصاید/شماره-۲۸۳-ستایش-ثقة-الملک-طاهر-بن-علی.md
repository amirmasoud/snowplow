---
title: >-
    شمارهٔ ۲۸۳ - ستایش ثقة الملک طاهر بن علی
---
# شمارهٔ ۲۸۳ - ستایش ثقة الملک طاهر بن علی

<div class="b" id="bn1"><div class="m1"><p>ای ملک ملک چون نگار کرده</p></div>
<div class="m2"><p>در عصر خزانها بهار کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شغل همه دولت قرار داده</p></div>
<div class="m2"><p>در مرکز دولت قرار کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عدل بسی قاعده نهاده</p></div>
<div class="m2"><p>بر کلک تکاور سوار کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلکی که بسی خورده قارو گیتی</p></div>
<div class="m2"><p>در چشم معادی چو قار کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوید همه روزه بلند گردون</p></div>
<div class="m2"><p>کوهست به ما بر مدار کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این ملک به حق طاهر علی را</p></div>
<div class="m2"><p>هست از همه خلق اختیار کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو صدر جهانی صدر حشمت</p></div>
<div class="m2"><p>از حشمت تو افتخار کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اقبال تو مانند گل شکفته</p></div>
<div class="m2"><p>در دیده بدخواه خار کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای هیبت تو چون هزبر حربی</p></div>
<div class="m2"><p>جان و دل دشمن شکار کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کام ملک کامگار عادل</p></div>
<div class="m2"><p>بر کام تو را کامگار کرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسعود که پیش سپهر والا</p></div>
<div class="m2"><p>بر تاج سعادت نثار کرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شهرگشایی که مر تو را شه</p></div>
<div class="m2"><p>بر کل جهان شهریار کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرورده به حق عدل را و تکیه</p></div>
<div class="m2"><p>بر یاری پروردگار کرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای از پدر خویش کار دیده</p></div>
<div class="m2"><p>بهتر ز پدر باز کار کرده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیور زده دولت و به حشمت</p></div>
<div class="m2"><p>از جاه تو دولت شعار کرده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اقبال تو را روزگار شاهی</p></div>
<div class="m2"><p>تاج و شرف روزگار کرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای روز بزرگیت را سعادت</p></div>
<div class="m2"><p>در دهر بسی انتظار کرده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای حیدر مردی و مردی تو</p></div>
<div class="m2"><p>بر ملک تو را ذوالفقار کرده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای عالم رادی و رادی تو</p></div>
<div class="m2"><p>مر سایل را با یسار کرده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دریاب تنم را که دست محنت</p></div>
<div class="m2"><p>در حبس تنم را بشار کرده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست این تن من در حصار انده</p></div>
<div class="m2"><p>جان را ز تنم در حصار کرده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>من دی به بر تو عزیز بودم</p></div>
<div class="m2"><p>وامروز مرا حبس خوار کرده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بی رنگم و چو رنگ روزگارم</p></div>
<div class="m2"><p>بر تارک این کوهسار کرده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این گیتی پر نور و نار زینسان</p></div>
<div class="m2"><p>نور دل من پاک نار کرده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با منش بسی کارزار بوده</p></div>
<div class="m2"><p>بر من ز بلا کارزار کرده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این آهن در کوره مانده بوده</p></div>
<div class="m2"><p>بر پای منش چرخ مار کرده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون دانه نارم سرشک اندوه</p></div>
<div class="m2"><p>آکنده دلم را چو نار کرده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این دیده پر خون زمین زندان</p></div>
<div class="m2"><p>در فصل خزان لاله زار کرده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیماری و پیری و ناتوانی</p></div>
<div class="m2"><p>دربند مرا زرد و زار کرده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این چرخ نهال سعادتم را</p></div>
<div class="m2"><p>برکنده و بی بیخ و بار کرده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نی نی که مزور شدم ز رنجی</p></div>
<div class="m2"><p>کو بود تنم را نزار کرده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین پیش به زندان نشسته بودم</p></div>
<div class="m2"><p>بیمار دلم را فگار کرده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آتش دل محنت زمانه</p></div>
<div class="m2"><p>چون دود تنم پر شرار کرده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اندر غم و تیمار بی شمارم</p></div>
<div class="m2"><p>پیداست همان را شمار کرده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>امروز منم با هزار نعمت</p></div>
<div class="m2"><p>صد آرزو اندر کنار کرده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زین دولت ناسازگار بوده</p></div>
<div class="m2"><p>با بخت مرا سازگار کرده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از بخشش تو شادمانه گشته</p></div>
<div class="m2"><p>اقبال توام بختیار کرده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باریده دو کفت چو ابر بر من</p></div>
<div class="m2"><p>ایام مرا بی غبار کرده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نعمت رسدم هر زمان دمادم</p></div>
<div class="m2"><p>بر پشت ستوران بار کرده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو با فلک تند کار زاری</p></div>
<div class="m2"><p>از بهر مرا کارزار کرده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از رغم مخالف پناه جانم</p></div>
<div class="m2"><p>اندر کنف زینهار کرده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من بنده از صدر دور مانده</p></div>
<div class="m2"><p>بر مدح و دعا اختصار کرده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از دوری نادیدن جمالت</p></div>
<div class="m2"><p>نهمار سرم را خمار کرده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا چهره گردون بود و به شب ها</p></div>
<div class="m2"><p>از اختر تابان نگار کرده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در ملک شهنشاه باد و یزدان</p></div>
<div class="m2"><p>اقبال تو را پایدار کرده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو پیش شه تاجدار و گردون</p></div>
<div class="m2"><p>بد خواه تو را تاج دار کرده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در دولت سالی هزار مانده</p></div>
<div class="m2"><p>یک عز تو گردون هزار کرده</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر یاد تو خورده جهان و دایم</p></div>
<div class="m2"><p>از خلق تو را یادگار کرده</p></div></div>