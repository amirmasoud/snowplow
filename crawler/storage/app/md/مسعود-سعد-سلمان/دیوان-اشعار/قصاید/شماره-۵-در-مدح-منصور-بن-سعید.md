---
title: >-
    شمارهٔ ۵ - در مدح منصور بن سعید
---
# شمارهٔ ۵ - در مدح منصور بن سعید

<div class="b" id="bn1"><div class="m1"><p>شب آمد و غم من گشت یک دو تا فردا</p></div>
<div class="m2"><p>چگونه ده صد خواهد شد این عنا و بلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا خورم غم فردا وزآن چه اندیشم</p></div>
<div class="m2"><p>که نیست یک شب جان مرا امید بقا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع زارم و سوزان و هر شبی گویم</p></div>
<div class="m2"><p>نماند خواهم چون شمع زنده تا فردا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی بنالم چون چنگ و خلق را از من</p></div>
<div class="m2"><p>همی به کار نیاید جز این بلند نوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی کند سرطان وار باژگونه به طبع</p></div>
<div class="m2"><p>مسیر نجم مرا باژگونه چرخ دو تا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز ماه وز خورشید دیدگان سازم</p></div>
<div class="m2"><p>به راه راست درآیم به سر چو نابینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضعیف گشته در این کوهسار بی فریاد</p></div>
<div class="m2"><p>غریب مانده برین آسمان بی پهنا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آنچه هست بر این تن نهند بر کهسار</p></div>
<div class="m2"><p>ور آنچه هست درین دل زنند بر دریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تابش آب شود در در میان صدف</p></div>
<div class="m2"><p>ز رنج خون شودی لعل در دل خارا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا چون تیغ دهد آب آبگون گرودن</p></div>
<div class="m2"><p>هر آنگهی که بنالم به پیش او ز ظما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو تیغ نیک بتفساندم ز آتش دل</p></div>
<div class="m2"><p>در آب دیده کند غرق تا به فرق مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قضا به من نرسد زآنکه نیست از من دور</p></div>
<div class="m2"><p>نشسته با من هم زانوی منست این جا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هر سپیده دمی و به هر شبانگاهی</p></div>
<div class="m2"><p>ز نزد من به زمین بر پراکنند قضا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز تاب و تف دمم سنگ خاره خاک شدست</p></div>
<div class="m2"><p>در آب چشمم از آن خاک بردمید گیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نبشتنی را خاکستر است دفتر من</p></div>
<div class="m2"><p>چون خامه نقش وی انگشت من کند پیدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بماند خواهد جاوید کز بلندی جای</p></div>
<div class="m2"><p>نه ممکن است که بروی جهد شمال و صبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکن شگفت ز گفتار من که نیست شگفت</p></div>
<div class="m2"><p>از این که گفتم اندیشه کن شگفت چرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عمید مطلق منصور بن سعید که چرخ</p></div>
<div class="m2"><p>ز آستانه درگاه او ستد بالا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جواد کفی عادل دلی که در قسمت</p></div>
<div class="m2"><p>ز بخل و ظلم نیامد نصیب او الا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که جام باده به ساقی دهد به دست تهی</p></div>
<div class="m2"><p>به تیغ سر بزند کلک را نکرده خطا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به مکرمات تو دعوی اگر کند گردون</p></div>
<div class="m2"><p>بسنده باشد او را دو کف تو دو گوا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امام عالم و مطلق تو را شناختمی</p></div>
<div class="m2"><p>اگر شناختمی طبع جهل و اصل جفا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نهادمی همه گل را به خلق تو نسبت</p></div>
<div class="m2"><p>اگر ز گلها درنامدی گل رعنا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بهاری ابر به کف تو نیک مانستی</p></div>
<div class="m2"><p>به رعد اگر نزدی در زمانه طبل سخا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شبی به اصل خود از خار و از صدف گل و در</p></div>
<div class="m2"><p>ز روزگار بهاری و ز آفتاب ضیا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز چرخ گردون مهری ز کوه ثابت زر</p></div>
<div class="m2"><p>ز چشم ابر سرشکی ز حد تیغ مضا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درست و راست صفات تو گویم و نه شگفت</p></div>
<div class="m2"><p>درست و راست شنیدن ز مردم شیدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شگفت از آنکه همه مغز من محبت توست</p></div>
<div class="m2"><p>ار آنکه کوه رسیل است مرمرا به صدا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون من به سنت در اطاعت تو دارم تن</p></div>
<div class="m2"><p>فضایل تو به من بر فریضه کرد ثنا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دلیروار همی وصف تو نیارم گفت</p></div>
<div class="m2"><p>ز کفر ترسم زیرا که نیستت همتا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه روز باشد کانجاه سازدت گردون</p></div>
<div class="m2"><p>که من درآیم و گویم تو را ثنا به سزا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا نگویی از اینگونه چند خواهم دید</p></div>
<div class="m2"><p>سپید و چنگ ز روز و ز شب زمین ز هوا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فلک به دوران گه آسیا و گه دولاب</p></div>
<div class="m2"><p>زمین ز گردون گه کهربا گه مینا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همی چه گویم و دانم همی کجا بینم</p></div>
<div class="m2"><p>من آنچه گویم اینست عادت شعرا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دعای من ز دو لب راست تر همی نشود</p></div>
<div class="m2"><p>بدان سبب که رسیدم به جایگاه دعا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بس بلندی ظل زمین به من نرسد</p></div>
<div class="m2"><p>نه ام سپید صباح است و نه سیاه مسا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مدار چرخ کند آگهم ز لیل و نهار</p></div>
<div class="m2"><p>مسیر چرخ خبر گویدم ز صیف و شتا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نگر به دیده چگونه نمایدم خورشید</p></div>
<div class="m2"><p>چو آفتاب نماید مرا به دیده سها</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر استعانت و راحت جز از تو خواستمی</p></div>
<div class="m2"><p>دو چنگ را زدمی در کمرگه جوزا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه بادی بر جای تا همیشه بود</p></div>
<div class="m2"><p>به جای مرکز غبرا و گنبد خضرا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو چرخ مرکز جاه تو را شتاب و سکون</p></div>
<div class="m2"><p>چو طبع آتش رأی تو را سنا و ضیا</p></div></div>