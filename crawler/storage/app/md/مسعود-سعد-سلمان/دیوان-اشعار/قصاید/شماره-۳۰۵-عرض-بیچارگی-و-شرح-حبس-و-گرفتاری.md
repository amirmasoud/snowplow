---
title: >-
    شمارهٔ ۳۰۵ - عرض بیچارگی و شرح حبس و گرفتاری
---
# شمارهٔ ۳۰۵ - عرض بیچارگی و شرح حبس و گرفتاری

<div class="b" id="bn1"><div class="m1"><p>نه بر خلاص حبس ز بختم عنایتی</p></div>
<div class="m2"><p>نه در صلاح کار ز چرخم هدایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیشم نهد زمانه ز تیمار سورتی</p></div>
<div class="m2"><p>هر گه که بخوانم ز اندوه آیتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حبس من به هر شهر اکنون مصیبتی</p></div>
<div class="m2"><p>وز حال من به هر جا اکنون روایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی خورم به تلخی تا کی کشم به رنج</p></div>
<div class="m2"><p>از دوست طعنه ای وز دشمن سعایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کیستم چه دارم چندم کیم چیم</p></div>
<div class="m2"><p>کم هر زمان رساند گردون نکایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه نعمتی مرا که ببخشم خزینه ای</p></div>
<div class="m2"><p>نه عدتی مرا که بگیرم ولایتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه روی محفلی ام و نه پشت لشکری</p></div>
<div class="m2"><p>نه مستحق و در خور صدر و ولایتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیوسته بوده ام ز قضا در عقیله ای</p></div>
<div class="m2"><p>همواره کرده ام ز زمانه شکایتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بهر جامه کهن و نان خشک من</p></div>
<div class="m2"><p>زینجا کدیه ایست وز آنجا رعایتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای روزگار عمر به رشوت همی دهم</p></div>
<div class="m2"><p>پس چون نگه نداریم اندر حمایتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر آمدی جنایتی از من چه کردیی</p></div>
<div class="m2"><p>کاین می کنی نیامده از من جنایتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چونان که در نهاد تو را نیست آخری</p></div>
<div class="m2"><p>رنج مرا نهاد نخواهی نهایتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه از تو هیچ وقتم در دل مسرتی</p></div>
<div class="m2"><p>نه از تو هیچ روزم در تن وقایتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر جا رسد کند به من آکفت نسبتی</p></div>
<div class="m2"><p>هر چون بود کند به من انده کنایتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دارم ز جنس جنس غم و نوع نوع درد</p></div>
<div class="m2"><p>تألیف کرده هر نفسی را حکایتی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آخر رسید خواهد از این دو برون مدان</p></div>
<div class="m2"><p>یا عمر من به قطعی یا غم به غایتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای کم تعهدان ببریدم تعهدی</p></div>
<div class="m2"><p>ای کم عنایتان بکنیدم عنایتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باری دعا کنید و ز بهر دعا کنید</p></div>
<div class="m2"><p>زهاد مستجاب دعا را وصایتی</p></div></div>