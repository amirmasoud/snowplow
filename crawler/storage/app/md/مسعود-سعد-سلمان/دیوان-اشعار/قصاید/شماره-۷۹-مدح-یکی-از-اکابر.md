---
title: >-
    شمارهٔ ۷۹ - مدح یکی از اکابر
---
# شمارهٔ ۷۹ - مدح یکی از اکابر

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که دین و دولت را</p></div>
<div class="m2"><p>همه آثار تو به کار شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان شادتر شود آن کس</p></div>
<div class="m2"><p>که به نامت به کارزار شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته و کرده تو در عالم</p></div>
<div class="m2"><p>همه تاریخ روزگار شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشتوان کمال چون باید</p></div>
<div class="m2"><p>میخ حزم تو استوار شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذره ای کان ز حلم تو بجهد</p></div>
<div class="m2"><p>بیخی از تند کوهسار شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره کان ز جود بچکد</p></div>
<div class="m2"><p>سیلی از ابر تندبار شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تابود مرغزار جود تو سبز</p></div>
<div class="m2"><p>امل خلق کی نزار شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موقف بزم تو شکارگهیست</p></div>
<div class="m2"><p>که در او شکرها شکار شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس یسار و یمین که زی تو رسد</p></div>
<div class="m2"><p>از یمین تو با یسار شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب رنج ولیت روز شود</p></div>
<div class="m2"><p>گل به دست عدوت خار شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که نزد تو نیک نیست عزیز</p></div>
<div class="m2"><p>زود بینی که نیک خوار شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وانکه راه خلاف تو سپرد</p></div>
<div class="m2"><p>اگر آبست خاکسار شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرد گردن زه گریبانش</p></div>
<div class="m2"><p>آتشین طوق و گر زه مار شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر که اندر هوای تو نبود</p></div>
<div class="m2"><p>بر تن او هوا حصار شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دل بدخواهت ار ز سنگ بود</p></div>
<div class="m2"><p>پیش خشم تو چون غبار شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هیبت تو چو آتش افروزد</p></div>
<div class="m2"><p>اختر آسمان سرار شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاطر اندر مصاف مدحت تو</p></div>
<div class="m2"><p>همچو برنده ذوالفقار شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>طبع در گرد وهم تو نرسد</p></div>
<div class="m2"><p>گر همه بر قضا سوار شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون تو اندر خزان به باغ آیی</p></div>
<div class="m2"><p>آن خزان باغ را بهار شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه اطراف بی نگار چمن</p></div>
<div class="m2"><p>همچو طبع تو پرنگار شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وز تو این باغ نصرت آبادان</p></div>
<div class="m2"><p>به شگفتی چو قندهار شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاخ ها را ز لفظ تو روزی</p></div>
<div class="m2"><p>گوهر شب چراغ تار شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هست ممکن که قوت و حرکت</p></div>
<div class="m2"><p>عرض پنجه چنار شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بزم فرخنده تو را ساقی</p></div>
<div class="m2"><p>قامت سرو جویبار شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در فراق تو هر زمان تن من</p></div>
<div class="m2"><p>از بس اندیشه بی قرار شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر میم کآبگون سپهر دهد</p></div>
<div class="m2"><p>مغز عیش مرا خمار شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اشک من ناردانه شد نه عجب</p></div>
<div class="m2"><p>گو دل من کفیده نار شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چند باشم در انتظار و هوس</p></div>
<div class="m2"><p>که مگر بخت سازگار شود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>این بتر باشدم که راحت عمر</p></div>
<div class="m2"><p>در سر رنج انتظار شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پار مقصود من نشد حاصل</p></div>
<div class="m2"><p>ترسم امسال همچو پار شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای فلک همتی که هر چه کنی</p></div>
<div class="m2"><p>مایه عز و افتخار شود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یادگار جهان شدی و مباد</p></div>
<div class="m2"><p>که جهان از تو یادگار شود</p></div></div>