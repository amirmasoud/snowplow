---
title: >-
    شمارهٔ ۹۴ - مدیح عمید ابوالفرج نصر ابن رستم
---
# شمارهٔ ۹۴ - مدیح عمید ابوالفرج نصر ابن رستم

<div class="b" id="bn1"><div class="m1"><p>ای اصل سخا و رادی و داد</p></div>
<div class="m2"><p>بخل از تو خراب و جود آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خواجه عمید نصر رستم</p></div>
<div class="m2"><p>حساد به رنج و ناصحت شاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون باز تویی بلند همت</p></div>
<div class="m2"><p>مردار خورد عدوت چون خاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید سخای تو برآورد</p></div>
<div class="m2"><p>آن را که به چاه محنت افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رستم نبود به پیش تو مرد</p></div>
<div class="m2"><p>حاتم نبود به پیش تو راد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو شاد نشسته ای به لوهور</p></div>
<div class="m2"><p>نام تو به سیستان و نوزاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در قصر شجاعت و سخاوت</p></div>
<div class="m2"><p>از رای رفیع تست بنیاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاگرد دل تو گشت دریا</p></div>
<div class="m2"><p>برابر کف تو گشت استاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشته است زمانه بنده تو</p></div>
<div class="m2"><p>احرار شدند ز انده آزاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درویش ز فر تو برآسود</p></div>
<div class="m2"><p>بگذاشت خروش و بانگ و فریاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از رای تو کس نشد فراموش</p></div>
<div class="m2"><p>گیتی همه هست بر دلت یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خدمت تو فلک میان بست</p></div>
<div class="m2"><p>احسان تو طبع دهر بگشاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عدل تو ز خلق رنگ برداشت</p></div>
<div class="m2"><p>وز جود تو خلق مال بنهاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو خسرو روزگار خویشی</p></div>
<div class="m2"><p>در بند تو حاسد تو فرهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فر تو نشانده فتنه از دهر</p></div>
<div class="m2"><p>دولت چو رهی به پیشت استاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اقبال تو داد داد مظلوم</p></div>
<div class="m2"><p>هرگز ز تو کس ندیده بیداد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون موم شدم به دست تو نرم</p></div>
<div class="m2"><p>وز بهر عدو به دست فولاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید بخیل گشت پیشت</p></div>
<div class="m2"><p>تا مادر جود مر تو را زاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بادات بقا و عز و دولت</p></div>
<div class="m2"><p>وین عید خلیل فرخت زاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شادی و سلامتی و رادی</p></div>
<div class="m2"><p>با تو همه ساله رایگان باد</p></div></div>