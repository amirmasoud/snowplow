---
title: >-
    شمارهٔ ۱۵۴ - مدح عمید علی سالار
---
# شمارهٔ ۱۵۴ - مدح عمید علی سالار

<div class="b" id="bn1"><div class="m1"><p>ای باد بروب راه را یکسر</p></div>
<div class="m2"><p>وی ابر ببار بر زمین گوهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خاک عبیر گرد بر صحرا</p></div>
<div class="m2"><p>وی ابر گلاب کرد در فرغر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای رعد منال کامل آن مرکب</p></div>
<div class="m2"><p>کز نعره او سپهر گردد کر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وی برق مجه که خنجری بینی</p></div>
<div class="m2"><p>کز هیبت آن بیفسرد آذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای چرخ سپهر محمدت بشنو</p></div>
<div class="m2"><p>وی چشمه مهر مرتبت بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای گرسنه شیر در کمین منشین</p></div>
<div class="m2"><p>وی جره عقاب در هوا مگذر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر باره نشست فتنه شیران</p></div>
<div class="m2"><p>هان ای شیران ز راه یکسوتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کامد سپهی که کرد یک ساعت</p></div>
<div class="m2"><p>صحرا را کوه و کوه را کردر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پیش سپه مبارزی کورا</p></div>
<div class="m2"><p>مانند نگفته اند جز حیدر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سالار عمید خاصه خسرو</p></div>
<div class="m2"><p>آن داده بدین و ملک و دولت فر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرزانه علی که در همه گیتی</p></div>
<div class="m2"><p>یک مرد چنان نژاد از مادر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن همه گردنان سرنامه</p></div>
<div class="m2"><p>وان از همه سرکشان سردفتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در چشم کمال عقل او دیده</p></div>
<div class="m2"><p>بر گردن ملک رای او زیور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مردی سو دست و طبع او مایه</p></div>
<div class="m2"><p>رادی عرضست و دست او جوهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای بزمگه تو صورت فردوس</p></div>
<div class="m2"><p>وی رزمگه تو آیت محشر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خردست چو مکرمت کنی دریا</p></div>
<div class="m2"><p>لنگست چو حمله آوری صرصر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنی که به گاه حمله افکندن</p></div>
<div class="m2"><p>بر شخص تو جبرئیل پوشد پر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مومست به زیر تیغ تو جوشن</p></div>
<div class="m2"><p>گردست به زیر گرز تو مغفر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ تو بود به حمله در دستت</p></div>
<div class="m2"><p>همگونه شکل و برگ نیلوفر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ماننده برگ لاله گردانی</p></div>
<div class="m2"><p>چون بردی حمله بر صف کافر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امسال تو را چو وقت غزو آمد</p></div>
<div class="m2"><p>از عون خدای و نصرت اختر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از راه بخاست نعره و شیهه</p></div>
<div class="m2"><p>چونان که در ابر قیرگون تندر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر که بچکید زهره تنین</p></div>
<div class="m2"><p>در بیشه بکاست جان شیر نر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از خاک برست عنبر سارا</p></div>
<div class="m2"><p>وز کوه گشاد چشمه کوثر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر آرزوی جمال دیدارت</p></div>
<div class="m2"><p>بگشاد به باغ دیدگان عبهر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر جا که روی و خیزی و باشی</p></div>
<div class="m2"><p>اقبال و ظفر تو را بود رهبر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گویی نگرم همی در آن ساعت</p></div>
<div class="m2"><p>کآواز ظفر بخیزد از لشکر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وز خنجر تو به دولت عالی</p></div>
<div class="m2"><p>گردد ستده ولایتی دیگر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از گرد سپه هوا شود تاری</p></div>
<div class="m2"><p>وز خون عدو زمین شود احمر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برداشته فتحنامه ها پیکان</p></div>
<div class="m2"><p>زی حضرت پادشاه دین پرور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>او خرم و شاد گشته از فتحت</p></div>
<div class="m2"><p>و آگاهی داده زآن بهر کشور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرموده جواب و گفته سر نه</p></div>
<div class="m2"><p>هر جا که بباید اندر آن کشور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وان خطبه به نام تست ارزانی</p></div>
<div class="m2"><p>تا خدمت تو بداده باشد بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر نام تو خطبه ای کنم انشا</p></div>
<div class="m2"><p>تا برخوانند بر سر منبر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چونانکه ز بس فصاحت و معنی</p></div>
<div class="m2"><p>در صنعت آن فرو چکانم زر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خدمت پس خدمتیست از بنده</p></div>
<div class="m2"><p>گر نیستمی فتاده بر بستر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لیکن چه کنم که مانده ام اینجا</p></div>
<div class="m2"><p>بیمار و ضعیف و عاجز و مضطر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از جور فلک سری پر از انده</p></div>
<div class="m2"><p>وز آتش غم دلی پر از اخگر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یک ذره نماند آتش قوت</p></div>
<div class="m2"><p>بر جای بمانده ام چو خاکستر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چون موی شده تن من از زاری</p></div>
<div class="m2"><p>چون نامه شده ز غم دلم در بر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه طبع معین من گه انشا</p></div>
<div class="m2"><p>نه دستم در بیاض یاریگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قصه چه کنم ز درد بیماری</p></div>
<div class="m2"><p>شیرین جانم رسیده با غرغر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دل بسته به حسن رأی میمونت</p></div>
<div class="m2"><p>امید به فضل ایزد داور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور بگذرم از جهان ز غم رستم</p></div>
<div class="m2"><p>تو باقی مان و از جهان مگذر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جز بر سر فخر و مرتبت منشین</p></div>
<div class="m2"><p>جز دیده عز و خرمی مسپر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در حکم تو باد گردش گیتی</p></div>
<div class="m2"><p>در امر تو باد گنبد اخضر</p></div></div>