---
title: >-
    شمارهٔ ۲۵۹ - باز در مدح آن شهریار
---
# شمارهٔ ۲۵۹ - باز در مدح آن شهریار

<div class="b" id="bn1"><div class="m1"><p>به سوی هند خرامید بهر جستن کین</p></div>
<div class="m2"><p>رکاب خسرو محمود سیف دولت و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشاده چتر همایون چو آسمان بلند</p></div>
<div class="m2"><p>کشیده رایت عالی بر اوج علیین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرار برده ز برنده خنجر هندی</p></div>
<div class="m2"><p>ز بهر آنکه دهد بوم هند را تسکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عکس خنجر او آفتاب خیره شده</p></div>
<div class="m2"><p>ز سم مرکب او زلزله گرفته زمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه تاب دارد نخجیر و آهو و روباه</p></div>
<div class="m2"><p>چو سوی صید خرامد ز بیشه شیر عرین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدایگانا این داستان معروف است</p></div>
<div class="m2"><p>که کرد بنده به شعر خود اندرون تضمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار بنده ندارد دل خداوندی</p></div>
<div class="m2"><p>هزار کبک ندارد دل یکی شاهین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هزار سرکش هر روز بامداد پگاه</p></div>
<div class="m2"><p>به پیش فرش تو بر خاک می نهند جبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه غلام تو اند با که کرد خواهی رزم</p></div>
<div class="m2"><p>همه رهی تو اند از که جست خواهی کین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر ز بهر تماشا به راه و رسم شکار</p></div>
<div class="m2"><p>یکی خرامی ناگه ز راه هند به چین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگرد شاها اندر جهان که گشتن تو</p></div>
<div class="m2"><p>دهد جهان را ترتیب و ملک را تزیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو آسمان برینی و بی گمان باشد</p></div>
<div class="m2"><p>ثبات گیتی از گشت آسمان برین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به کار نایدت از بهر رزم تیغ و عمود</p></div>
<div class="m2"><p>نه نیز حاجت باشد به خنجر و زوبین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهان بگیری بی آنکه هیچ رنج بری</p></div>
<div class="m2"><p>به حزم صادق و عزم درست و رای رزین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی موفق و مسعود پادشاه بزرگ</p></div>
<div class="m2"><p>زهی مظفر و منصور شهریار زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هزار بحری هنگام بزم در یک صدر</p></div>
<div class="m2"><p>هزار شیری هنگام رزم در یک زین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو را بیژن و گرگین صفت چگونه کنم</p></div>
<div class="m2"><p>که هر غلام تو صد بیژنست و صد گرگین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بر فروختی از تیغ آتش اندر هند</p></div>
<div class="m2"><p>به شهر فارس فرو مرد آتش برزین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به هر چه قصد کنی مر تو را چه باک بود</p></div>
<div class="m2"><p>چو هست ایزد در کارها دلیل و معین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر کجا که نهی روی باشدت بی شک</p></div>
<div class="m2"><p>فتوح و نصرت پیوسته بر یسار و یمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همیشه بادی تابنده تر ز بدر منیر</p></div>
<div class="m2"><p>همیشه بادی پاینده تر ز کوه متین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به هر رهی که روی رهبر تو فتح بود</p></div>
<div class="m2"><p>کراست در همه آفاق رهبری به ازین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه دیر باشد شاها که کلک هفت اقلیم</p></div>
<div class="m2"><p>چنانکه هند شود مر تو را به زیر نگین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هزار شهر گشایی ز شهرهای بزرگ</p></div>
<div class="m2"><p>هزار نامه فتحت رود سوی غزنین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>محل رتبت تو بر شده به مهر سپهر</p></div>
<div class="m2"><p>ثبات ملک تو پیوسته بر شهور و سنین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مباد هرگز عمر تو را فنا یا رب</p></div>
<div class="m2"><p>مباد هرگز ملک تو را زوال آمین</p></div></div>