---
title: >-
    شمارهٔ ۱۴۸ - هم در ثنای او
---
# شمارهٔ ۱۴۸ - هم در ثنای او

<div class="b" id="bn1"><div class="m1"><p>پادشاه بزرگ دین پرور</p></div>
<div class="m2"><p>شهریار کریم حق گستر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو کامگار مسعودست</p></div>
<div class="m2"><p>کش زمانه ست بنده و چاکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه شاهان علاء دولت و دین</p></div>
<div class="m2"><p>آن فلک منظر ملک مخبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاجداری که رفعت نامش</p></div>
<div class="m2"><p>بر فلک برد پایه منبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کامگاری که بسطت دستش</p></div>
<div class="m2"><p>بر زمین ریخت مایه کوثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحن ملکش به دهر هفت اقلیم</p></div>
<div class="m2"><p>خیل بختش ز چرخ هفت اختر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راعی امن او به شرق و به غرب</p></div>
<div class="m2"><p>داعی جود او به بحر و به بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تارک رتبت بلندش را</p></div>
<div class="m2"><p>زیبد اکلیل آسمان افسر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردن همت بزرگش را</p></div>
<div class="m2"><p>عقد گردون سزا بود زیور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر در امر او به روز و به شب</p></div>
<div class="m2"><p>بسته دارد فلک چو کوه کمر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در صف کین او ز چپ و ز راست</p></div>
<div class="m2"><p>کند باشد درخش را خنجر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در برکه ز حرص افسر او</p></div>
<div class="m2"><p>همچو لاله ست چهره گوهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در دل کان ز بیم بخشش او</p></div>
<div class="m2"><p>چون زریرست باز گونه زر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون برانگیخت عزم نافذ او</p></div>
<div class="m2"><p>زیبدش صبح و مهر تیغ و سپر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون فرو داشت عزم ثابت او</p></div>
<div class="m2"><p>برنداردش عاصف و صرصر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عدل او بانگ زد چنان بر ظلم</p></div>
<div class="m2"><p>که ز گوگرد بازجست آذر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر او بار لطف چندان کرد</p></div>
<div class="m2"><p>که بر آذر شکوفه گشت شرر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>داد پر پر امیدواران را</p></div>
<div class="m2"><p>ساقی جود او شراب بطر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برد خوش خوش ضعیف حالان را</p></div>
<div class="m2"><p>ساقی داد او خمار ز سر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حمله ای کرد سطوتش چونانک</p></div>
<div class="m2"><p>فتنه را شد مصاف زیر و زبر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در سر و در شکم ز شور و بلا</p></div>
<div class="m2"><p>آب و خون شد ز هول مغز و جگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای جهان از کمال تو پیدا</p></div>
<div class="m2"><p>وی فلک در خصال تو مضمر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مملکت را مناقب تو مثل</p></div>
<div class="m2"><p>مفخرت را مکارم تو سمر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از پی سازهای تاج تو را</p></div>
<div class="m2"><p>قطره در می شود به بحر اندر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وز پی رودهای بزم تو را</p></div>
<div class="m2"><p>سر به گردون همی کشد عرعر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر لب نیک خواه دولت تو</p></div>
<div class="m2"><p>آب حیوان شود می ساغر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در کف بدسگال دولت تو</p></div>
<div class="m2"><p>بوی نفط سیه دهد عنبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر نباشد به طبع همت تو</p></div>
<div class="m2"><p>چنگ بگذارد از عرض جوهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر بگردد ز حال فکرت تو</p></div>
<div class="m2"><p>چرخ بگشاید از فلک چنبر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو ولی گویی و به هیچ مهم</p></div>
<div class="m2"><p>لفظ تدبیر تو نبوده مگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جزم فرمانی و به هیچ مثال</p></div>
<div class="m2"><p>شرط فرمان تو نبوده اگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه شادی شهی نهاد کزو</p></div>
<div class="m2"><p>شد شکفته بهار دولت و فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون تف کارزار برزد جوش</p></div>
<div class="m2"><p>قرص خورشید شد چو خاکستر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چهره را خاک بیخت گونه پوست</p></div>
<div class="m2"><p>دیده را خار زاد نور بصر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تیره دیدند رنگ های امید</p></div>
<div class="m2"><p>تیز دیدند چنگ های خطر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گردها کرده چشم گیتی کور</p></div>
<div class="m2"><p>کوس ها کرده گوش گردون کر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تیغ چون مورد گشت چون لاله</p></div>
<div class="m2"><p>روی چون لاله شد چو نیلوفر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سینه چون کوره تفته در جوشن</p></div>
<div class="m2"><p>مغز چون کفته غنچه در مغفر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر بساط بسیط خوف و رجا</p></div>
<div class="m2"><p>برکشیده قضا حشر به حشر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در طریق مضیق عمر و فنا</p></div>
<div class="m2"><p>برفکنده بلا نفر به نفر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در مصاف و مجال هر سردار</p></div>
<div class="m2"><p>در شتاب و درنگ هر صفدر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آتش و آب و باد و خاک شده</p></div>
<div class="m2"><p>ابرش و خنگ و بور و جم زیور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون سر سنگ پشت و روی امل</p></div>
<div class="m2"><p>گشته پنهان ز بیم تیغ و تبر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خارپشتی شده ز نیزه و تیر</p></div>
<div class="m2"><p>اجل جان شکار عمر شکر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آن زمان لا اله الا الله</p></div>
<div class="m2"><p>وهم نادرست کرد بر تو گذر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>موی بشکافتی به طعن و به ضرب</p></div>
<div class="m2"><p>کوه برداشتی به کر و به فر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نور شد حربه تو از بس خون</p></div>
<div class="m2"><p>که زدش بر برخش و پهلو و بر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بازوی عون تو گرفته قضا</p></div>
<div class="m2"><p>خنجر فتح تو کشیده قدر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در خوی و خون شده زران و کفت</p></div>
<div class="m2"><p>باره نصرت و عنان و ظفر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وان همه صاعقه به یک ذره</p></div>
<div class="m2"><p>در دل بأس تو نکرده اثر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ملک جویان سهم کام روا</p></div>
<div class="m2"><p>دهر گیران گرد نام آور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه از هول گرز مسعودی</p></div>
<div class="m2"><p>بر سرافکنده چون زنان معجر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یکی افتاده در میانه شور</p></div>
<div class="m2"><p>دیگری خسته بر کرانه شر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>این رها کرده همچو ماران پوست</p></div>
<div class="m2"><p>وان برآورده همچو موران پر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یک جهان را به بازوی معروف</p></div>
<div class="m2"><p>بر کشفتی به حمله منکر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بازگشتی به قطب شاهی شاد</p></div>
<div class="m2"><p>عون یزدان و سعی چرخ نگر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تارک تاج را به صد دامن</p></div>
<div class="m2"><p>پایه تخت را به صد زیور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در بپاشید بخت نیک چو ابر</p></div>
<div class="m2"><p>زرد پراکند نجم سعد چو خور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر سویی زان ظفر به هر ساعت</p></div>
<div class="m2"><p>برسانید جبرئیل خبر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>آفرینش مزاج کرد بدل</p></div>
<div class="m2"><p>زود از آن مژده در جهان یکسر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گشت از اقبال آن عبیر گلاب</p></div>
<div class="m2"><p>خاک در دشت و آب در فرغر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شب تاری نمود گونه روز</p></div>
<div class="m2"><p>زهر قاتل گرفت طعم شکر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>داشت روز نشستن تو به ملک</p></div>
<div class="m2"><p>فضل آن شب که داشت پیغمبر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بهر آتشکده که در گیتی است</p></div>
<div class="m2"><p>راست چون یخ فسرده شد اخگر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>شد سیه روی صورت مانی</p></div>
<div class="m2"><p>شد نگون فرق لعبت آذر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شادباش ای ملوک را مخدوم</p></div>
<div class="m2"><p>دیر زی ای زمانه را داور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ملک در جمله آن مراد بیافت</p></div>
<div class="m2"><p>که همی بودش از فلک برتر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نه عجب گر ز فر دولت تو</p></div>
<div class="m2"><p>جان پذیرد همی نبات و حجر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>حرکت گیرد و بصر یابد</p></div>
<div class="m2"><p>پنجه سرو و دیده عبهر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>داند ایزد که زود خواهی دید</p></div>
<div class="m2"><p>باختر زان خویش چون خاور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>هفت کشور گرفته و بسزا</p></div>
<div class="m2"><p>بنده ای را سپرده هر کشور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تو در آن هفته چون مه و خورشید</p></div>
<div class="m2"><p>کرده و ساخته مسیر و ممر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گفت احوال تو فلک پیمای</p></div>
<div class="m2"><p>کرد احکام تو ستاره شمر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا ابد خسروی تو خواهی کرد</p></div>
<div class="m2"><p>از چنین ملک خسروا برخور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ملکا حال خویش خواهم گفت</p></div>
<div class="m2"><p>نیک دانم که آیدت باور</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در جهان هیچ گوی نشنیدست</p></div>
<div class="m2"><p>آنچه دیدست چشم من ز عبر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سالها بوده ام چنان که بود</p></div>
<div class="m2"><p>بچه شیر خواره بی مادر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گه بزاری نشسته ام گریان</p></div>
<div class="m2"><p>خان های ز سمج مظلم تر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گه به سختی کشیده ام نالان</p></div>
<div class="m2"><p>بندهای گرانتر از لنگر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گهی آن کرد بر دلم تیمار</p></div>
<div class="m2"><p>که کند زخم زخمه بر مزمر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>خاطرم گاهی از عنا آن دید</p></div>
<div class="m2"><p>گه به تف عود بیند از مجمر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چه حکایت کنم که می بودم</p></div>
<div class="m2"><p>زآتش و خاک بالش و بستر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>غرقه روی و رنج راحت و خشک</p></div>
<div class="m2"><p>تشنه کور و چشم انده تر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بر سر کوههای بی فریاد</p></div>
<div class="m2"><p>شد جوانی من هبا و هدر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شعر من باده شد به هر محفل</p></div>
<div class="m2"><p>ذکر من تازه شد به هر محضر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>عفو سلطان نامدار رضی</p></div>
<div class="m2"><p>بر شب من فکند نور قمر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>التفات عنایتش برداشت</p></div>
<div class="m2"><p>بار رنج از تن من مضطر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>اصطباع رعایتش دریافت</p></div>
<div class="m2"><p>روزگار مرا به حسن نظر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>داد نان پاره ای که هست کفاف</p></div>
<div class="m2"><p>مر مرا با عشیرتی بی مر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سوی مولد کشید هوش مرا</p></div>
<div class="m2"><p>بویه دختر و هوای پسر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چون به هندوستان شدم ساکن</p></div>
<div class="m2"><p>بر ضیاع عقار پیر پدر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>بنده بونصر برگماشت مرا</p></div>
<div class="m2"><p>به عمل همچو نایبان دگر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>نایبی نیستم چنانکه مرا</p></div>
<div class="m2"><p>سازی و آلتی بود در خور</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>مردکی چند هست بس لتره</p></div>
<div class="m2"><p>اسبکی چند هست بس لاغر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>گاه طبلی زنم به زیر گلیم</p></div>
<div class="m2"><p>گه تیغی کشم به زیر سپر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>گه جهم همچو رنگ بر کهسار</p></div>
<div class="m2"><p>گه خزم همچو مار در کردر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>این همه هست و شغل های عمل</p></div>
<div class="m2"><p>سخت با نظم و رونق است اندر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>حشمت عالی علایی تو</p></div>
<div class="m2"><p>در جهان خود همی کشد لشکر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>کبک و شاهین همی پرد همبال</p></div>
<div class="m2"><p>شیر و آهو همی رود همبر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>سرکشان را کجاست آن یارا</p></div>
<div class="m2"><p>که برآرند بر خلاف تو سر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>گردنان را کجاست زهره آنک</p></div>
<div class="m2"><p>پای عصیان برون نهند از در</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>گر ز مدح تو حال و جاه مرا</p></div>
<div class="m2"><p>مستزادی بود عجب مشمر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>ور وجیهی شوم ز خدمت تو</p></div>
<div class="m2"><p>راست باشد ز مقتضای هنر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>من شنیدم که میر ماضی را</p></div>
<div class="m2"><p>بنده بود والی لوکر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بس شگفتی نباشد ار باشد</p></div>
<div class="m2"><p>مادحت قهرمان چالندر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تا رساند به جشن هر نظمی</p></div>
<div class="m2"><p>نقش کرده ز مدح یک دفتر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>سازد از طبع درج های ثنا</p></div>
<div class="m2"><p>قیمتی تر ز درج های درر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>لیکن از بس که دید شعبدها</p></div>
<div class="m2"><p>گام ننهد همی مگر به حذر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ترسد از عاقبت که دانستست</p></div>
<div class="m2"><p>عادت عرف گنبد اخضر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>دشمنان دارد و عجب نبود</p></div>
<div class="m2"><p>دشمن آمد تمام را ابتر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>باز چون نیک تر در اندیشه</p></div>
<div class="m2"><p>نهراسد ز هیچ نوع ضرر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>که دل و طبع تو ز رحمت و عفو</p></div>
<div class="m2"><p>آفریدست خالق اکبر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>تا هیولی است اصل هر عنصر</p></div>
<div class="m2"><p>تا بود عنصر اصل هر پیکر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>اصل ملک تو باد ثابت فرع</p></div>
<div class="m2"><p>فرع اصل تو باد نافع بر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>امرهای زمانه وصف تو را</p></div>
<div class="m2"><p>مهر همراه و مشتری همبر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بزم های سپهر نعت تو را</p></div>
<div class="m2"><p>ماه ساقی و زهره خنیاگر</p></div></div>