---
title: >-
    شمارهٔ ۶۷ - در مدح ابوالفرج و گله از او
---
# شمارهٔ ۶۷ - در مدح ابوالفرج و گله از او

<div class="b" id="bn1"><div class="m1"><p>بوالفرج ای خواجه آزادمرد</p></div>
<div class="m2"><p>هجر و وصال تو مرا خیره کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید ز سختی تن و جان آنچه دید</p></div>
<div class="m2"><p>خورد ز تلخی دل و جان آنچه خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخت بدردم ز دل سخت گرم</p></div>
<div class="m2"><p>نیک برنجم ز دم نیک سرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیر شدن در دم دولت همی</p></div>
<div class="m2"><p>محنت ناگاه به من باز خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه به صد دیده به جیحون درم</p></div>
<div class="m2"><p>از سرم این چرخ برآورد گرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسته یکی شیرم گویی به جای</p></div>
<div class="m2"><p>دیده ز خون سرخ و رخ از هول زرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نکشم تیغ زبان چون کنم</p></div>
<div class="m2"><p>با فلک گردان تنها نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز و شب اینجا به قمار اندرم</p></div>
<div class="m2"><p>هست حریفم فلک لاجورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهره او سی سیه و سی سپید</p></div>
<div class="m2"><p>گردش او زیر یکی تخت نرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر همی بازم و بازم همی</p></div>
<div class="m2"><p>داو ز من می برد این گرد گرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای به بلندی سخن شاعران</p></div>
<div class="m2"><p>هرگز مانند تو نابوده مرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرشی گستردمت از دوستی</p></div>
<div class="m2"><p>باز که فرمودت کاندر نورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روی توام از همه چیز آرزوست</p></div>
<div class="m2"><p>خسته همی جوید درمان درد</p></div></div>