---
title: >-
    شمارهٔ ۱۵۹ - هم در ستایش او
---
# شمارهٔ ۱۵۹ - هم در ستایش او

<div class="b" id="bn1"><div class="m1"><p>رسید عید و من از روی حور دلبر دور</p></div>
<div class="m2"><p>چگونه باشم بی روی آن بهشتی حور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که گوید کای دوست عید فرخ باد</p></div>
<div class="m2"><p>نگار من به لهاور و من به نیشابور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره دراز و غریبی و فرقت جانان</p></div>
<div class="m2"><p>اگر بنالم دارید مر مرا معذور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یار یاد همی آیدم که هر عیدی</p></div>
<div class="m2"><p>درآمدی ز در من بسان حور قصور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار شاخ ز سنبل نهاده بر لاله</p></div>
<div class="m2"><p>هزار حلقه ز عنبر فکنده بر کافور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن چو سیم برآراسته به جامه عید</p></div>
<div class="m2"><p>نهاده بر دو کف خویشتن گلاب و بخور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببردی از دل من تاب زآن دو زلف متاب</p></div>
<div class="m2"><p>خمار عشق فزودی به چشمک مخمور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که دور بود از چنین شگرف نگار</p></div>
<div class="m2"><p>چگونه باشد بر هجرش ای نگار صبور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا نباشم با عزم و حزم مردانه</p></div>
<div class="m2"><p>چرا ندارم هر چه بود به دل مستور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو یاد شهر لهاور و یار خویش کنم</p></div>
<div class="m2"><p>نبود کس که شد از شهر و یار خویش نفور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا به است به هر حالی و به هر وجهی</p></div>
<div class="m2"><p>جمال حضرت عزنین ز شهر لوهاور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلی به است به از وصل آن نگار مرا</p></div>
<div class="m2"><p>جلال خدمت درگاه خسرو منصور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امیر غازی محمود ابن ابراهیم</p></div>
<div class="m2"><p>خدایگانی کش هست عادلی دستور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شهی که مردی بر لشکرش شده سالار</p></div>
<div class="m2"><p>شهی که رادی بر گنج او شده گنجور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گاه هیبت سام و به گاه حشمت جم</p></div>
<div class="m2"><p>به گاه کوشش نار و به گاه بخشش نور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مثال حلمش یابی چو بنگری به جبال</p></div>
<div class="m2"><p>قیاس علمش بینی چو بنگری به حور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همی نجوید تیرش به جز دل قیصر</p></div>
<div class="m2"><p>همی نخواهد تیغش مگر سر فغفور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بترسد از سر گرزش به روز هیجا مرگ</p></div>
<div class="m2"><p>حذر کند ز حسامش به رزمگاه خدور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بهر دولت محمودیان جهان ایزد</p></div>
<div class="m2"><p>بیافرید و بدان داد تا ابد منشور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرا کنند طلب ناکسان ز گیتی مال</p></div>
<div class="m2"><p>چرا شوند به بیهوده جاهلان مغرور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یقین بدان که بلاشک ندامت آرد بار</p></div>
<div class="m2"><p>هر آنکه کارد اندر زمین جهل غرور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدایگانا راهی گذاشتی که همی</p></div>
<div class="m2"><p>برید باد ازو نگذرد به جز رنجور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز پنج سیحون بگذشته ای بنامیزد</p></div>
<div class="m2"><p>که باد چشم بد از تخت و روزگار تو دور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رسید عید همایون شها به خدمت تو</p></div>
<div class="m2"><p>نهاده پیش تو هدیه نشاط لهو و سرور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به رسم عید شها باده مروق نوش</p></div>
<div class="m2"><p>به لحن بربط و چنگ و چغانه و طنبور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خجسته بادت عید و خجسته بادت ماه</p></div>
<div class="m2"><p>خجسته بادت رفتن به درگه معمور</p></div></div>