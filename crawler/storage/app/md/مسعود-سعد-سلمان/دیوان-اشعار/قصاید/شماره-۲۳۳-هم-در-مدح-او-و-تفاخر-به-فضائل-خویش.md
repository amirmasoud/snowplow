---
title: >-
    شمارهٔ ۲۳۳ - هم در مدح او و تفاخر به فضائل خویش
---
# شمارهٔ ۲۳۳ - هم در مدح او و تفاخر به فضائل خویش

<div class="b" id="bn1"><div class="m1"><p>دوش تا صبحدم همه شب من</p></div>
<div class="m2"><p>عرضه می کرده ام سپاه سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیشتر زان سپاه را دیدم</p></div>
<div class="m2"><p>از لباس هنر برهنه بدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امرای سخن بسی بودند</p></div>
<div class="m2"><p>این تفحص نکرده بد یکتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین سپس کار هر یکی به سزا</p></div>
<div class="m2"><p>سازم ار خواهد ایزد ذوالمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنخفتم چو شمع تا بنشست</p></div>
<div class="m2"><p>زرد شمع اندرین سپید لگن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شب زین دو چشم تیره چو شب</p></div>
<div class="m2"><p>پر کواکب مرا شده دامن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عجب بر سر بنات النعش</p></div>
<div class="m2"><p>جمع گشته بسان نجم پرن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم من همچو باد در آذر</p></div>
<div class="m2"><p>چشم من همچو ابر در بهمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرگس و گل شدم که نگشایم</p></div>
<div class="m2"><p>جز به باد و به آب چشم و دهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخنم نیست بر زمانه روان</p></div>
<div class="m2"><p>همچو بر روی سنگ سخت ارزن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناروایی سخن همی ترسم</p></div>
<div class="m2"><p>که زبان مرا کند الکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خط موهوم شد ز باریکی</p></div>
<div class="m2"><p>اندرین حبس فکرت روشن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا ز مرمر شدست اندیشه</p></div>
<div class="m2"><p>در دل همچو چشمه سوزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بس شگفتی نباشد ار باشد</p></div>
<div class="m2"><p>رنج و تیمار من ز دانش من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخت من زیر فضل شد ناچیز</p></div>
<div class="m2"><p>زانکه بسیار گشت در هر فن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خیزد از آهن آتشی که چو آب</p></div>
<div class="m2"><p>می شود زو گداخته آهن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آهنم بی خلاف زانکه همی</p></div>
<div class="m2"><p>در دل خویش پرورم دشمن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به حقیقت چراغ را بکشد</p></div>
<div class="m2"><p>اگر از حد برون رود روغن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشوم خاضع عدو هرگز</p></div>
<div class="m2"><p>گر چه بر آسمان کند مسکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز گنجشک را برد فرمان</p></div>
<div class="m2"><p>شیر روباه را نهد گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>راست گردد سپهر کژ رفتار</p></div>
<div class="m2"><p>رام گردد زمانه توسن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بکنم کار و کار فرمایم</p></div>
<div class="m2"><p>هستم اندر دو جای تیغ و مسن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جوشنم گر شود منازع تیغ</p></div>
<div class="m2"><p>تیغ گردم چو او شود جوشن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان تن من بود همی به عنا</p></div>
<div class="m2"><p>زان دل من بود همی به حزن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کاندر افتاد همی به طبع ملال</p></div>
<div class="m2"><p>کاندر آید همی به عمر شکن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر بخواهد خدایگان زمین</p></div>
<div class="m2"><p>شاه محمود شهریار زمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پادشاهی که زیبدش گاه بار</p></div>
<div class="m2"><p>ماه و خورشید یاره و گرزن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نوبهارست کز سخاوت او</p></div>
<div class="m2"><p>هست بر نیکخواه او گلشن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سایل بزم او سزد حاتم</p></div>
<div class="m2"><p>کشته رزم او سزد بهمن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چون یلان در وغا برانگیزد</p></div>
<div class="m2"><p>آتش رزمگاه روز فتن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای به هنگام حلم صد احنف</p></div>
<div class="m2"><p>وی به هنگام حرب صد بیژن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زیر آلای تست حزم خرد</p></div>
<div class="m2"><p>دون اوصاف تست غایت ظن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باطن دشمنم چو ظاهر زشت</p></div>
<div class="m2"><p>باطن من چو ظاهرم احسن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عود و چندن نه هر دو خوشبویند</p></div>
<div class="m2"><p>بر زمین هر دو را یکیست وطن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون به آتش رسند هر دو به هم</p></div>
<div class="m2"><p>نبود فعل عود چون چندن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>راستم همچو سرو در هر باب</p></div>
<div class="m2"><p>زان برم نیست همچو سرو چمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آتش شغل من نجسته هنوز</p></div>
<div class="m2"><p>دود عزلم برآمد از روزن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا چو باران رضای تو بچکد</p></div>
<div class="m2"><p>بر من و تازه داردم چو سمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به خدایی که آکند صنعش</p></div>
<div class="m2"><p>مشک در ناف آهوان ختن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>که اگر من شوم به دانش پیر</p></div>
<div class="m2"><p>همچنان چون صدف به در عدن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون صدف در همه جهان نکنم</p></div>
<div class="m2"><p>جز به دریای مدح تو معدن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که جز از تو به هیچ خدمت و مدح</p></div>
<div class="m2"><p>طمع دارم ز خلق پاداشن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر وفات حفاظ و سوک خرد</p></div>
<div class="m2"><p>پاره ام باد جیب و پیراهن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ور نباشد به معصیت راضی</p></div>
<div class="m2"><p>به برم زانکه روبه است سمن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ای چو کعبه وحوش را همه امن</p></div>
<div class="m2"><p>خلق را قصر و درگهت مأمن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیت کعبه کرده بنده تو</p></div>
<div class="m2"><p>بنده را زین مراد باز مزن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا بخواهد ز ایزد آمرزش</p></div>
<div class="m2"><p>پیش از آن کش شود لباس کفن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بندد اندر رضای یزدان دل</p></div>
<div class="m2"><p>تن گشاید ز بند اهریمن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا فروزند در مجوس آذر</p></div>
<div class="m2"><p>تا پرستند در هنود وثن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چرخ ملک تو باد با خورشید</p></div>
<div class="m2"><p>باغ لهو تو باد پر سوسن</p></div></div>