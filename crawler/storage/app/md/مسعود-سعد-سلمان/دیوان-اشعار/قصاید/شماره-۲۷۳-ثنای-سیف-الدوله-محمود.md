---
title: >-
    شمارهٔ ۲۷۳ - ثنای سیف الدوله محمود
---
# شمارهٔ ۲۷۳ - ثنای سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>بر من بتافت یار و بتابم ز تاب او</p></div>
<div class="m2"><p>طاقت نماند پیش مرا با عتاب او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این روی پر ز دره و در خوشاب گشت</p></div>
<div class="m2"><p>از آرزوی دره و در خوشاب او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از رشگ آن نقاب که بر روی او رسد</p></div>
<div class="m2"><p>گشت این تن ضعیف چو تار نقاب او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نوشم آید ارچه چو زهرم دهد جواب</p></div>
<div class="m2"><p>زیرا که هست بر لب راه جواب او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بربود خواب از من و آنگه بخفت خوش</p></div>
<div class="m2"><p>پیوسته گشت گویی خوابم به خواب او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوردم شراب عشقش یک ساغر و هنوز</p></div>
<div class="m2"><p>اندر سر منست خمار شراب او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنگ عقاب زلفش و پرتذرو روی</p></div>
<div class="m2"><p>ایمن رخ تذرو ز چنگ عقاب او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز سپید روی و غراب سیاه زلف</p></div>
<div class="m2"><p>وز بیم باز او شده لرزان غراب او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داند که هست بسته زلفین او دلم</p></div>
<div class="m2"><p>هر ساعتی فزون کند از پیچ و تاب او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون زر پخته شد رخ چون سیم خام من</p></div>
<div class="m2"><p>زان آفتاب تابان وز مشک ناب او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر زر ز آفتاب زیادت شود همی</p></div>
<div class="m2"><p>نقصان چرا شود زرم از آفتاب او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر عاشق ای نگارین رحمت کن و مسوز</p></div>
<div class="m2"><p>بر آتش فراق دل چون کباب او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاید که آب او بر توبه شود که هست</p></div>
<div class="m2"><p>زان مجلس شهنشه گیتی مآب او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محمود سیف دولت شاهی که در جهان</p></div>
<div class="m2"><p>شاهنشه ست از همه شاهان خطاب او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر ملک را اگر چه فراوان بود زمان</p></div>
<div class="m2"><p>محمود شاه باشد مالک رقاب او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شخصش سپهر و خلقش در وی نجوم او</p></div>
<div class="m2"><p>خشمش اثیر و تیرش در وی شهاب او</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کفش سحاب و تازه ازو بوستان ملک</p></div>
<div class="m2"><p>زحمت ندید و صاعقه اندر سحاب او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یابد فلک درنگ به وقت درنگ او</p></div>
<div class="m2"><p>گیرد زمین شتاب به گاه شتاب او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باشد هوا گران چو سبک شد عنان او</p></div>
<div class="m2"><p>گردد زمین سبک چو گران شد رکاب او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صافی شدست آب جلالت ز آتشش</p></div>
<div class="m2"><p>افروخته ست آتش هیبت ز آب او</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آبست و آتشست حسامش به رزمگاه</p></div>
<div class="m2"><p>روی زمین و چرخ پر از موج و تاب او</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دیده مخالف ملکست سیل او</p></div>
<div class="m2"><p>واندر دل معادی دین التهاب او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر بقعه ای که مرکب او بسپرد زمینش</p></div>
<div class="m2"><p>گردد گلاب و عنبر آب و تراب او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روید به جای خار شقایق ز عنبرش</p></div>
<div class="m2"><p>باشد به جای سنگ گهر در گلاب او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آثار مهر اوست در آباد این زمین</p></div>
<div class="m2"><p>تأثیر کین اوست چنین در خراب او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کم باد بدسگال وی و باد بر فزون</p></div>
<div class="m2"><p>اقبال و ملک و دولت و عمر و شباب او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون باغ باد مجلسش آراسته مدام</p></div>
<div class="m2"><p>چون عندلیب و بلبل چنگ و رباب او</p></div></div>