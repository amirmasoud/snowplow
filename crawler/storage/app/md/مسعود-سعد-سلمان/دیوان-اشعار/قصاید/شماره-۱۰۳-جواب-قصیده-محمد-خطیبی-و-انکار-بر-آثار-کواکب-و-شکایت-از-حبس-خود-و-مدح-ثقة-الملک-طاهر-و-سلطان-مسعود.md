---
title: >-
    شمارهٔ ۱۰۳ - جواب قصیده محمد خطیبی و انکار بر آثار کواکب و شکایت از حبس خود و مدح ثقة الملک طاهر و سلطان مسعود
---
# شمارهٔ ۱۰۳ - جواب قصیده محمد خطیبی و انکار بر آثار کواکب و شکایت از حبس خود و مدح ثقة الملک طاهر و سلطان مسعود

<div class="b" id="bn1"><div class="m1"><p>محمد ای به جهان عین فضل و ذات هنر</p></div>
<div class="m2"><p>تویی اگر بود از فضل و از هنر پیکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را خطیبی خوانند شاید و زیبد</p></div>
<div class="m2"><p>که تو فصیح خطیبی به نظم و نثر اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر این لقب را بر خود درست خواهی کرد</p></div>
<div class="m2"><p>به وقت خطبه دانش ز عود کن منبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لطف و سرعت آبست و باد خاطر تو</p></div>
<div class="m2"><p>به تاب و قوت عقلت چه خاک و چه آذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تو قرین و رفیق و چو تو برادر و دوست</p></div>
<div class="m2"><p>که داشته است و که دارد بدین جهان اندر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز حسب حال چو زهر تو زهره ام خون شد</p></div>
<div class="m2"><p>که نظم کرده ای آن را به گفته چو شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد فراوان داری همی چرا نالی</p></div>
<div class="m2"><p>ازین دوازده برج نگون و هفت اختر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا تو از بره و گاو در فغان باشی</p></div>
<div class="m2"><p>که بی سروست یکی زین و بی لگد دیگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو از دو پیکر و خرچنگ چون خروش کنی</p></div>
<div class="m2"><p>چو بد کنند به تو چون نه اندر جاناور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه بیم داری از شیر کو ندارد چنگ</p></div>
<div class="m2"><p>چه خیر جویی از خوشه کو ندارد بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو را چه نقصان کرد این ترازوی خسران</p></div>
<div class="m2"><p>که پله هاش فروتر نباشد و برتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز کژدم و ز کمان این هراس و بیم چراست</p></div>
<div class="m2"><p>نه دم این را نیش و نه تیر آن را پر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ازین بزیچه بسته دهان چرا ترسی</p></div>
<div class="m2"><p>که هرگزش نه چرا گه بد و نه آبشخور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه جویی آب ز دلوی که آب نیست درو</p></div>
<div class="m2"><p>چگونه تر شود ار نیستش بر آب گذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز ماهیی که درو خار نیست این گله چیست</p></div>
<div class="m2"><p>بلی ز ماهی پر خار دیده اند ضرر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه پیر خوانی ویحک همی تو کیوان را</p></div>
<div class="m2"><p>خرف شدست ازو هیچ نیک و بد مشمر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر اورمزد توانا و کامران بودی</p></div>
<div class="m2"><p>نه در وبالش بودی نه در هبوط مقر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه خواند باید بهرام را همی خونی</p></div>
<div class="m2"><p>به دستش اندر هرگز که دید تیغ و تبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آفتاب اگر تاب و قوتی بودی</p></div>
<div class="m2"><p>سیاه روی نگشتی ز جرم قرص قمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سماع ناهید آخر ز مردمان که شنید</p></div>
<div class="m2"><p>که خواند او را اخترشناس خنیاگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه جادوییست نگویی مرا تو اندر تیر</p></div>
<div class="m2"><p>که هر دو مه شود از آفتاب خاکستر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چه بد تواند کردن مهی که گوی زمین</p></div>
<div class="m2"><p>کندش تیره از آن پس که باشد او انور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز اختران که همه سرنگون کنند غروب</p></div>
<div class="m2"><p>چه سعد باشد و نحس و چه نفع باشد و ضر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو ای برادر خود را میفکن از ره راست</p></div>
<div class="m2"><p>ز چرخ و اختر هرگز نه خیر دان نه شر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه قضا و قدر کردگار عالم راست</p></div>
<div class="m2"><p>مدان تو دولت و محنت جز از قضا و قدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمانه نادره بازیچه ها برون آورد</p></div>
<div class="m2"><p>ز بازی فلک مهره باز بازیگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان یقین که بدین گونه آفرید فلک</p></div>
<div class="m2"><p>به حکمت آنکه بر این گونه ساختش چنبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بهر شیون زینسان کبود پوشش کرد</p></div>
<div class="m2"><p>ز بهر سورش بست از ستارگان زیور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بدید باید عبرت نبود باید کور</p></div>
<div class="m2"><p>شنید باید پند و نگشت باید کر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهانت عبرت و پندست رفته و مانده</p></div>
<div class="m2"><p>تو مانده بازشناس و تو رفته بازنگر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر ز مانده نداری خبر عجب نبود</p></div>
<div class="m2"><p>ز رفته باری داری چنانکه بود خبر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بنگریم همیدون پس از قضای خدا</p></div>
<div class="m2"><p>بلای ما همه قزدار بود و چالندر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من و تو هر دو فضولی شدیم و چرخ از بیخ</p></div>
<div class="m2"><p>بکندمان و سزاوار بود و اندر خور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز ترس بر تن ما تیز و تازه افتادی</p></div>
<div class="m2"><p>بدان زمان که رگ ما بجستی از نشتر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو اهل کوشش بودیم و بابت پیکار</p></div>
<div class="m2"><p>همی چه بستیم از بهر کارزار کمر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه دست راست گرفتی به رسم قبضه تیغ</p></div>
<div class="m2"><p>نه دست چپ را بودی توان بند سپر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدانکه ما را در نظم دست نیک افتاد</p></div>
<div class="m2"><p>ز خود به جنگ چرا ساختیم رستم زر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه هر که باشد چیره براندن خامه</p></div>
<div class="m2"><p>دلیر باشد بر کار بستن خنجر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کسی که خنجر پولاد کار خواهد بست</p></div>
<div class="m2"><p>دلش چو آهن و پولاد باید اندر بر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تنی چو خارا باید سری چو سوهان سخت</p></div>
<div class="m2"><p>که پای دارد با دار و گیر حمله مگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در آن زمان که شود زیر گرد لبها خشک</p></div>
<div class="m2"><p>بدان مکان که شود زیر خود سرها تر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همه ز آهن بینند زیور مردان</p></div>
<div class="m2"><p>چو خاست گرد کمیت و سمند و جم زیور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دلاوران را دل گردد از هراس دو نیم</p></div>
<div class="m2"><p>مبارزان را خون گردد از نهیب جگر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو لاله گردد پشت زمین به طعن و به ضرب</p></div>
<div class="m2"><p>شود چو خیری روی هوا به کر و به فر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خروش رزم چو آواز زیر و بم نبود</p></div>
<div class="m2"><p>حدیث کلک دگردان و کار تیغ دگر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نبود باید گوریش تا به آخر عمر</p></div>
<div class="m2"><p>که مردمان به چنین ضحک ها شوند سمر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حدیث خویش همی گویم ای برادر من</p></div>
<div class="m2"><p>تو زینهار گمان دگر مدار و مبر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تو را نباید کاید ز من کراهیتی</p></div>
<div class="m2"><p>بدین که گفته شد ای نیک رای و ای مهتر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کنون از آنچه خوش آید تو را بخواهم گفت</p></div>
<div class="m2"><p>که هست از این پس این دولتی تو را بی مر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گرت چو سرو مسطح همی بپیرایند</p></div>
<div class="m2"><p>بدان که زود چو سرو سهی برآری سر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز صبر جوشن پوش و نبرد مردان کن</p></div>
<div class="m2"><p>ز بأس مرکب ساز و مصاف گردان در</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو گرد گبند خضرا برآی و شغل طلب</p></div>
<div class="m2"><p>که من هزیمت گشتم ز گنبد اخضر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا اگر پس ازین دولتی دهد یاری</p></div>
<div class="m2"><p>من و ثنای خداوند و خامه و دفتر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به مدحت ثقت الملک ازین چو دریا دل</p></div>
<div class="m2"><p>به غوص طبع برآرم طویله های گهر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عمید مطلق طاهر که سروران هرگز</p></div>
<div class="m2"><p>ندیده اند چو او در زمانه یک سرور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بزرگواری دریادلی که در بخشش</p></div>
<div class="m2"><p>به پیش جودش دریا کم آید از فرغر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بلند قدرش کرده ست وصف چرخ زمین</p></div>
<div class="m2"><p>گشاده طبعش کرده ست نعمت بحر شمر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز ابر رادی وز مرغزار نعمت او</p></div>
<div class="m2"><p>نه آز گردد تشنه نه مکرمت لاغر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>قلق نگشته است از قرب او مرگ خامه</p></div>
<div class="m2"><p>تهی نرفته است از دست او مگر ساغر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ندیده اند ز ایوان جاه او کنگر</p></div>
<div class="m2"><p>نجسته اند ز دریای فضل او معبر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز اوج همت او چرخ ها شود تیره</p></div>
<div class="m2"><p>ز موج بخشش او گنج ها برد کیفر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به هیچ وقت نبودست بی سخا دستش</p></div>
<div class="m2"><p>چنانکه هیچ نبودست بی عرض جوهر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو بحر مادر طبع سخاش بود رواست</p></div>
<div class="m2"><p>که هست خوی خوش او برادر عنبر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به دوست گردان اقبال دین و ملک آری</p></div>
<div class="m2"><p>نگردد اختر بی چرخ و چرخ بی محور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>برستم از همه غم کو به چشم بخشایش</p></div>
<div class="m2"><p>ز صدر جاه به من بنده تیز کرد نظر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>خدای داند کامروز اندرین زندان</p></div>
<div class="m2"><p>زجود و بخشش او نعمتست بس بی مر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همی ز رحمت او باشدم درین دوزخ</p></div>
<div class="m2"><p>نسیم سایه طوبی و چشمه کوثر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نه من ببینم در هر شرف چو او مخدوم</p></div>
<div class="m2"><p>نه او بیابد در هر هنر چو من چاکر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر خلاصی باشد مرا و خواهد او</p></div>
<div class="m2"><p>نباشدم هوس لشکر و هوای سفر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من آستانه درگاه او کنم بالین</p></div>
<div class="m2"><p>بخسبم آنجا و ایمن شوم ز رنج سهر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>برون کنم ز سرم کبر و باد بی خردی</p></div>
<div class="m2"><p>ز علم لشکر سازم ز اهل علم حشر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>شوم به نانی قانع به جامه ای راضی</p></div>
<div class="m2"><p>به خط عقل تبرا کنم ز عجب و بطر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همه به خشتک شلوار برنشینم و بس</p></div>
<div class="m2"><p>نه اسب تازی باید مرا نه ساز بزر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چه سود ازین سخن چون نگار و شعر چو در</p></div>
<div class="m2"><p>چو ما به محنت گشتیم هر دو زیر و زبر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دو اهل فضل و دو آزاده و دو ممتحنیم</p></div>
<div class="m2"><p>دو خیره رای و دو خیره سر و دو خیره بصر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>دعای ماست به هر مسجد و به هر مجلس</p></div>
<div class="m2"><p>دریغ ماست به هر محفل و به هر محضر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تو نو گرفتی در حبس و بند معذوری</p></div>
<div class="m2"><p>اگر بترسی ازین بند و بشگهی ز خطر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>منم که عشری از عمر شوم من نگذشت</p></div>
<div class="m2"><p>مگر به محنت و در محنتم هنوز ایدر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>به جای مانده ام از بندهای سخت گران</p></div>
<div class="m2"><p>ضعیف گشته ام از رنج های بس منکر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نوان و سست شده رویم از طپانچه کبود</p></div>
<div class="m2"><p>در آب دیده نمانم مگر به نیلوفر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>شده بر آب دو دیده سبک تر از کشتی</p></div>
<div class="m2"><p>اگر چه بندی دارم گران تر از لنگر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بلا و محنت و اندوه و رنج و محنت و غم</p></div>
<div class="m2"><p>دمادمند به من بر چو قطره های مطر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز بس که گویم امروز این بلا بودست</p></div>
<div class="m2"><p>تمام نام بلاها مرا شدست از بر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز ضعف پیری گشته ست چون گلیم کهن</p></div>
<div class="m2"><p>به حبس رویم و بوده چو دیبه ششتر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>ز بی حمیتی ای دوست چو غلیواجم</p></div>
<div class="m2"><p>نه ماده خود را دانم کنون همی و نه نر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>علاج را گزر پخته می خورم زیرا</p></div>
<div class="m2"><p>که آن چو سخت گزر سست شد چو برگ گزر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دریغ شخص که از بند شد نحیف و دوتا</p></div>
<div class="m2"><p>دریغ عمر که در حبس شد هبا و هدر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همی به شعر کنم ساحری از آن باشد</p></div>
<div class="m2"><p>همیشه حالم چون حال ساحران به سحر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بسان آذر و مانی بتگر و نقاش</p></div>
<div class="m2"><p>بلا و محنت بینم همی به زندان در</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>از آن که می به پرستند گفت های مرا</p></div>
<div class="m2"><p>بسان صورت مانی و لعبت آذر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>زمانه را پسری در هنر زمن به نبست</p></div>
<div class="m2"><p>چرا نهان کندم همچو بد هنر دختر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چرا به عمر چو کفار بسته دارندم</p></div>
<div class="m2"><p>اگر یکی ام از امتان پیغمبر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>بدین همانا زین امتم نمی شمرند</p></div>
<div class="m2"><p>که می برون نگذارندم از عذاب سقر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>همی سخن ها گرم آیدم کز آتش دل</p></div>
<div class="m2"><p>دهان چو کوره شد و شد زبان درو اخگر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>توزان که لختی محنت کشیده ای در حبس</p></div>
<div class="m2"><p>بدین که گفتم دانم که داریم باور</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>یقین بدان که نه مردست خصم دانش من</p></div>
<div class="m2"><p>اگر چه پوشد در جنگ جوشن و مغفر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بلی ولیک قلمدان ز دوکدان بگریخت</p></div>
<div class="m2"><p>به عاقبت بتر آمد عمامه از معجر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بکوفتم دری از خم قلتبان باز</p></div>
<div class="m2"><p>بگو بروتی باز ایدر آمدم زان در</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>خرم و نیم خرم و ابله و مخنث من</p></div>
<div class="m2"><p>خرد ندارم و دیوانه زادم از مادر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>وز آنکه نادان بودم چو گرد کردم ریش</p></div>
<div class="m2"><p>مرا به نام همه ریش گاو خواند پدر</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چو حال فضل بدیدم که چیست بگزیدم</p></div>
<div class="m2"><p>ز کار پیشه جولاهگی ز بهر پسر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بدو نوشتم و پیغام دادم و گفتم</p></div>
<div class="m2"><p>که ای سعادت در فضل هیچ رنج مبر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>اگر سعادت خواهی چو نام خویش همی</p></div>
<div class="m2"><p>به سوی نقص گرای و طریق جهل سپر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>مترس و بانگ یکایک چو سگ همی کن عف</p></div>
<div class="m2"><p>بخیز و نیز دمادم چو خر همی زن عر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>که بردرند سگان هر کرا نگردد سگ</p></div>
<div class="m2"><p>لگد زنند خران هر کرا نباشد خر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>عناست فضل نه از فضل بود عود بود</p></div>
<div class="m2"><p>که زار زار بسوزد بر آتش مجمر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نصیحت پدرانه ز من نکو بشنو</p></div>
<div class="m2"><p>مگر گرد هنر هیچ کآفتست هنر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>ز فصل نعمت مزمر بود که در مجلس</p></div>
<div class="m2"><p>ز زخم زخمه بنالد زمان زمان مزمر</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>مکار اگر که ز کشته دریغ می دروی</p></div>
<div class="m2"><p>دریغ می درود هر کسی که کارد اگر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>ز اضطراب نمودن چه فایده ما را</p></div>
<div class="m2"><p>اگر چه هستیم امروز عاجزو مضطر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نخوانده ایم که نتوان ز گیتی ایمن بود</p></div>
<div class="m2"><p>ندیده ام که نتوان ز چرخ کرد حذر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>کزین زمانه بسی چنگ و پربیفکندست</p></div>
<div class="m2"><p>هژبر آهن چنگ و عقاب آتش پر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بدان حقیقت کاین شغل و این عمل دارند</p></div>
<div class="m2"><p>سپهر عمر شکار و جهان عمر شکر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به ذات خویش مؤثر نیند و مجبورند</p></div>
<div class="m2"><p>درین همه که تو می بینی ایزدیست اثر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نخواست ماندن اگر گنج شایگان بودی</p></div>
<div class="m2"><p>بماند این سخن جانفزای تا محشر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>چو ذکر مردم عمری دگر بود پس از آن</p></div>
<div class="m2"><p>که ثابتست همه ساله منظر از مخبر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>بریده نیست امید خلاص و راحت من</p></div>
<div class="m2"><p>در این زمانه که تازه شده ست عدل عمر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>ز کدخدای جهان و شهریار ملک افروز</p></div>
<div class="m2"><p>خدایگان زمین پادشاه دین پرور</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>سپهر همت و خورشید رای و دریا دل</p></div>
<div class="m2"><p>زمانه دار و زمین خسرو و جهان داور</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>علاء دولت مسعود کامکار که ملک</p></div>
<div class="m2"><p>به دست فخر نهد بر سرش همی افسر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>نهاده مسند میمونش بر سپهر شرف</p></div>
<div class="m2"><p>نبشته نام همایونش بر نگین ظفر</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>چو از ثری علم قدر اوست تا عیوق</p></div>
<div class="m2"><p>ز باختر سپه جاه اوست تا خاور</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>گذشت رایت اقبال او ز هر گردون</p></div>
<div class="m2"><p>رسید آیت انصاف او به هر کشور</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>مضای حشمت او ابر شد به شرق و به غرب</p></div>
<div class="m2"><p>نفاذ دولت او باد شد به بحر و به بر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چو شیر شرزه و چون مار گرزه بر سر و دست</p></div>
<div class="m2"><p>ز هولش افسر فغفور و یاره قیصر</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>سپهرها را بر امر او مدار و مجال</p></div>
<div class="m2"><p>ستارگان را در حکم او مسیر و ممر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>گر او نخواهد هر سال خوش نخندد باغ</p></div>
<div class="m2"><p>ور او نگوید هر روز بر نیاید خور</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>برازدم که چون من نیست هیچ مدحت گوی</p></div>
<div class="m2"><p>برازدش که چنو نیست هیچ مدحت خر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>وزیده باد در آفاق باد دولت او</p></div>
<div class="m2"><p>که بر ولیش نسیم است و بر عدو صرصر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>گر این قصیده نیامد چنانکه در خور بود</p></div>
<div class="m2"><p>ز آنکه هستش معنی رکیک و لفظ ابتر</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>مرا به فضل تو معذور دار کاین سر و تن</p></div>
<div class="m2"><p>ز ناتوانی بر بالش است و بر بستر</p></div></div>