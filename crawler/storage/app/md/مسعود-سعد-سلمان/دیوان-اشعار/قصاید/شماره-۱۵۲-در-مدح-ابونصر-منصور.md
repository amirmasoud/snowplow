---
title: >-
    شمارهٔ ۱۵۲ - در مدح ابونصر منصور
---
# شمارهٔ ۱۵۲ - در مدح ابونصر منصور

<div class="b" id="bn1"><div class="m1"><p>مملکت را به نصرت منصور</p></div>
<div class="m2"><p>روزگاری پدید شد مشهور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارض ملک پادشا که ازوست</p></div>
<div class="m2"><p>رایت او چو نام او منصور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور عدلش زمانه را سایه ست</p></div>
<div class="m2"><p>سایه دولتش جهان را نور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزم او باد را نگفته عجول</p></div>
<div class="m2"><p>حزم او کوه را نخوانده صبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای به ترجیح فخر نامعجب</p></div>
<div class="m2"><p>وی به عز کمال نامغرور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک را از تو دولتی عالی</p></div>
<div class="m2"><p>عدل را از تو عالمی معمور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بدان بی غم از هراس خلل</p></div>
<div class="m2"><p>وان بدین ایمن از نهیب فتور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارگاه تو کارگاه وجود</p></div>
<div class="m2"><p>پایگاه تو پیشگاه صدور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با عطای تو زار گرید زر</p></div>
<div class="m2"><p>با ثنای تو زور گیرد زور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر تو بر تن وضیع و شریف</p></div>
<div class="m2"><p>مهر تو در دل اناث و ذکور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غرض از مدت بقای تو بود</p></div>
<div class="m2"><p>رفته و مانده سنین و شهور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سبب عزت و سخای تو گشت</p></div>
<div class="m2"><p>زاده و داده جبال و بحور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بپاشی به یک سخا گنجی</p></div>
<div class="m2"><p>نبوی نزد خویشتن معذور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور برآری به کینه زآب آتش</p></div>
<div class="m2"><p>نشمری بدسگال را مقهور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک عدل تا به تخت نشست</p></div>
<div class="m2"><p>به ز رای تو نامدش دستور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>باعث لهو را ندید مزید</p></div>
<div class="m2"><p>خوشتر از حسن تو نبودش سور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نرسد بی مؤونت به ذلت</p></div>
<div class="m2"><p>طمعه و دانه وحوش و طیور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نبود بی طراوت بزمت</p></div>
<div class="m2"><p>سیری و مستی نشاط و سرور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تشنگان امید فضل تو را</p></div>
<div class="m2"><p>ننماید جهان سراب غرور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خفتگان فریب کین تو را</p></div>
<div class="m2"><p>بر نیانگیزد از زمین دم صور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جز کف راد تو امید که کرد</p></div>
<div class="m2"><p>غرقه موج آز را به عبور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جز دم داد تو نوید که داد</p></div>
<div class="m2"><p>کشته تیغ ظلم را به نشور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پست اعراض تو نگشت بلند</p></div>
<div class="m2"><p>مست انعام تو نشد مخمور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حشمتت را نخیز باز حریص</p></div>
<div class="m2"><p>دشمنت را گریز زاغ حذور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدسگال تو و تجمل او</p></div>
<div class="m2"><p>شبهی دارد از سگ و ساجور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیستش ترس کایمنش کردست</p></div>
<div class="m2"><p>از تو عفو حمول و حلم وفور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طعمه شیر کی شود راسو</p></div>
<div class="m2"><p>مسته چرخ کی شود عصفور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باره تو تبارک الله چیست</p></div>
<div class="m2"><p>گهی آسوده و گهی رنجور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیک آسان بودش بس دشوار</p></div>
<div class="m2"><p>سخت نزدیک باشدش بس دور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تازش او به حرص چون صرصر</p></div>
<div class="m2"><p>گردش او به طبع چون در دور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تگ او اگر کند عجب نبود</p></div>
<div class="m2"><p>وهم را در صمیم دل محصور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>و آتش نعل او بدی نه شگفت</p></div>
<div class="m2"><p>گر مزاج هوا کند محرور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وان بریده پی شکافته سر</p></div>
<div class="m2"><p>در کفت ساحریست چون مسحور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سخت نالان چو ناقه معلول</p></div>
<div class="m2"><p>زار و گریان چو عاشق مهجور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نکته ها گیرد از هنر مرموز</p></div>
<div class="m2"><p>حرفها گیرد از خرد مستور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گل کفاند بخار در میدان</p></div>
<div class="m2"><p>در چکاند ز مشک بر کافور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دیده بی دیدگان برای العین</p></div>
<div class="m2"><p>شکل مقسوم و صورت مقدور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای به هر فضل ذات تو ممدوح</p></div>
<div class="m2"><p>وی به هر خیر سعی تو مشکور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حله طبع باف وصف تو را</p></div>
<div class="m2"><p>بوده انفاس صدق من مزدور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گوهر گنج سای مدح تو را</p></div>
<div class="m2"><p>گشته غواص ذهن من گنجور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خاطر بدپسند من شاهیست</p></div>
<div class="m2"><p>بر عروسان مدحت تو غیور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جمع کرده ز بهر زیورشان</p></div>
<div class="m2"><p>در منظوم و لؤلؤ منثور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>لعبتانی که کرده انفاسش</p></div>
<div class="m2"><p>سر فرازند بر نجوم و بدور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زلفشان از فکنده آهو</p></div>
<div class="m2"><p>لبشان از نهاده زنبور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همگان را به ناز پرورده</p></div>
<div class="m2"><p>دایه رنج در ستور و خدور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نقش کرده به حسن برغیشان</p></div>
<div class="m2"><p>تاج کسری و یاره فغفور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لیکن از رنج برده طبعم هست</p></div>
<div class="m2"><p>راحتی دون نقثت المصدور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فوز نایافته شدم مانده</p></div>
<div class="m2"><p>نجح نایافته شدم مغمور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چون شکایت کنم که فایده نیست</p></div>
<div class="m2"><p>من ضمان علی الکریم یجور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دهر بی منفعت خریست پلید</p></div>
<div class="m2"><p>چرخ بی عافیت سگیست عقور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بوم چالندرست مرتع من</p></div>
<div class="m2"><p>مار و رنگم درین ثقاب و ثغور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کوههایی ست رزمگاه مرا</p></div>
<div class="m2"><p>خواهر جودی و برادر طور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر بلندی که لنگ و لوک شدست</p></div>
<div class="m2"><p>از پس و پیش آن قبول و دبور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گل سختش به سختی سندان</p></div>
<div class="m2"><p>شخ تندش به تیزی ساطور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>میزبانان من سیوف و رماح</p></div>
<div class="m2"><p>میهمانان من کلاب و نمور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>غو کوس و غریو بوق مرا</p></div>
<div class="m2"><p>لحن نایست و نغمه طنبور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آرزو باشدم که هر سالی</p></div>
<div class="m2"><p>باشم اندر دو بقعه طنبور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بدو فضل اندرین دو فضل جلیل</p></div>
<div class="m2"><p>غیبت من بدل شود به حضور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>که مرا خوشتر از گلاب و عبیر</p></div>
<div class="m2"><p>آب غزنین و خاک لوهاور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نیست روزی دگر چه اندیشه</p></div>
<div class="m2"><p>بر به آمد شد از هوا مقصور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در قدر تا کجا رسد پیداست</p></div>
<div class="m2"><p>قوت آفریده مجبور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>کعبه جاه تو ملی و وفیست</p></div>
<div class="m2"><p>به قضای حوائج جمهور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پس چرا اندرو مرا نبود</p></div>
<div class="m2"><p>حج مقبول و عمره مبرور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نه مرا حاجتی ازو مقضی</p></div>
<div class="m2"><p>نه مرا طاعتی ازو مأجور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خود نکردم گنه و گه کردم</p></div>
<div class="m2"><p>هست اندر کرم گنه مغفور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>خیره خلق الوف تو بی جرم</p></div>
<div class="m2"><p>به چه معنی ز من شدست نفور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که نسیم صبای لطف تو شد</p></div>
<div class="m2"><p>شب و روز مرا سموم خدور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ویحک ای آسمان سال نورد</p></div>
<div class="m2"><p>کی رهیم از حریق این باحور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آخر ای آفتاب روز افزون</p></div>
<div class="m2"><p>کی دمد صبح این شب دیجور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>تا بود باغ و راغ را هر سال</p></div>
<div class="m2"><p>به ربیع و خریف زینت و حور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زلف شاه اسپرغم و روی سمن</p></div>
<div class="m2"><p>چشم بادام و دیده انگور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>باد عیشت به خرمی موصوف</p></div>
<div class="m2"><p>باد روزت به فرخی مذکور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>روزگارت رهی و بخت غلام</p></div>
<div class="m2"><p>فلکت بنده و جهان مأمور</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>از ازل دولت تو را توقیع</p></div>
<div class="m2"><p>به ابد نعمت تو را منشور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تر و تازه خزان تو چو بهار</p></div>
<div class="m2"><p>خوی و خرم روان تو چو سحور</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ناله صدرت از سرور و سریر</p></div>
<div class="m2"><p>ظلمت بزمت از بخار بخور</p></div></div>