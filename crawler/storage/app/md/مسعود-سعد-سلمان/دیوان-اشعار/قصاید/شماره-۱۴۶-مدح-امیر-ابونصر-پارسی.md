---
title: >-
    شمارهٔ ۱۴۶ - مدح امیر ابونصر پارسی
---
# شمارهٔ ۱۴۶ - مدح امیر ابونصر پارسی

<div class="b" id="bn1"><div class="m1"><p>بونصر پارسی سر احرار روزگار</p></div>
<div class="m2"><p>هست از یلان و رادان امروز یادگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبیست از لطافت و بادیست از صفا</p></div>
<div class="m2"><p>بحریست از مروت و کوهیست از وقار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همت به روی و رایش بفراخت چون قمر</p></div>
<div class="m2"><p>فضل از نصیب خلقش بشکفت چون بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایوان به وقت بزم نبیند چنو سخی</p></div>
<div class="m2"><p>میدان به گاه رزم نبیند چنو سوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنفش همی بر آب روان افکند گره</p></div>
<div class="m2"><p>لطفش همی بر آتش سوزان کند نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خشم و عنف او دو نشانست روز و شب</p></div>
<div class="m2"><p>از مهر و کین او دو نمونست نور و نار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دشمنان بگشت به قهر آسمان نهاد</p></div>
<div class="m2"><p>بر دوستان بتافت به جود آفتاب وار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا در میان باغ بخندد همی سمن</p></div>
<div class="m2"><p>تا در کنار جوی ببالد همی چنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خندیده باد نزهت او را لب طرب</p></div>
<div class="m2"><p>بالیده باد نعمت او را تن یسار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون اوج چرخ دولت عالیش مهروار</p></div>
<div class="m2"><p>چون بیخ کوه حشمت باقیش پایدار</p></div></div>