---
title: >-
    شمارهٔ ۲۸۰ - مدحتگری
---
# شمارهٔ ۲۸۰ - مدحتگری

<div class="b" id="bn1"><div class="m1"><p>ای نصرت و فتح پیش بر کرده</p></div>
<div class="m2"><p>تن پیش سپاه دین سپر کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دست نهاده عمر شیرین</p></div>
<div class="m2"><p>جان گردمیان خود کمر کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ملتان تا به حضرت غزنین</p></div>
<div class="m2"><p>بر مایه نصرت و ظفر کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه لشکر بیکران بهم خوانده</p></div>
<div class="m2"><p>نه مردم بی عدد حشر کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لشکر ترک و هند و افغانان</p></div>
<div class="m2"><p>بر باره هزار شیر نر کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز بهر شکار بد سگالان را</p></div>
<div class="m2"><p>چون گرسنه شیر پر خطر کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگرفته عنان دولت سلطان</p></div>
<div class="m2"><p>توفیق خدای راهبر کرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دشت زمرد جنگ سد بسته</p></div>
<div class="m2"><p>در کوه به تیغ تیز در کرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر دامن کوه کوفته موکب</p></div>
<div class="m2"><p>گوش ملک سپهر کر کرده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وین روشن دیده مهر تابان را</p></div>
<div class="m2"><p>از گرد سپاه بی بصر کرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد ساله زمین خشک را از خون</p></div>
<div class="m2"><p>تا ماهی و پشت گاو تر کرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صحرای فراخ و غار بی بن را</p></div>
<div class="m2"><p>از خون مخالفان شمر کرده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کفار ز بیم تیغ برانت</p></div>
<div class="m2"><p>بر کوه چو رنگ مستقر کرده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر کشور جنگوان زده ناگاه</p></div>
<div class="m2"><p>هر زیر که یافته زبر کرده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>افروخته تیغت آتش سوزان</p></div>
<div class="m2"><p>مغز و دل کفر پر شرر کرده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>انگیخته روز معرکه ابری</p></div>
<div class="m2"><p>بارانش ز ناچخ و تبر کرده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر دشمن کسوتی بپوشیده</p></div>
<div class="m2"><p>وان کسوت تازه را عبر کرده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خاک درشت ابره را داده</p></div>
<div class="m2"><p>وز خون سیاهش آستر کرده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مر عالم روح را به یک ساعت</p></div>
<div class="m2"><p>چون بتکده ها پر از صور کرده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این ساعت عالم دگر بوده</p></div>
<div class="m2"><p>آن ساعت تیغ تو دگر کرده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کاری که به ده سفر نکردی کس</p></div>
<div class="m2"><p>آسان آسان به یک سفر کرده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنجا زده ای که اهل آن دلها</p></div>
<div class="m2"><p>بودند ز کفر چون حجر کرده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه بوی رسیده در وی از ایمان</p></div>
<div class="m2"><p>نه باد هدی برو گذر کرده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر پیر پدر که از جهان رفته</p></div>
<div class="m2"><p>ده عهد به کفر با پسر کرده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواهم دهن مبشرانت را</p></div>
<div class="m2"><p>مانند صدف پر از درر کرده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای همت و عادت تو را ایزد</p></div>
<div class="m2"><p>فهرست بزرگی و هنر کرده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غزوی نکنی که ناردت ایزد</p></div>
<div class="m2"><p>از نصرت و فتح بهره ور کرده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گیری پسران بی پدر بوده</p></div>
<div class="m2"><p>آری پسران بی پدر کرده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن چیست که خسروت بفرماید</p></div>
<div class="m2"><p>کش ناری پیش همچو زر کرده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نو روز خدمتت همی آید</p></div>
<div class="m2"><p>گیتی همه پر ز بار و بر کرده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بس رود و زمین و کوه را یابی</p></div>
<div class="m2"><p>چون دیبه روم و شوشتر کرده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از کوه شکفته لاله ها بینی</p></div>
<div class="m2"><p>سرها ز میان سنگ بر کرده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آیند به باغ بلبل و قمری</p></div>
<div class="m2"><p>این قصه فتح تو زبر کرده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آواز به مدحت تو بگشاده</p></div>
<div class="m2"><p>سرها ز نشاط پر بطر کرده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو ساخته مجلسی و از خوبان</p></div>
<div class="m2"><p>پر زهره روشن و قمر کرده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در صدر نشسته و می نصرت</p></div>
<div class="m2"><p>در روی و دماغ تو اثر کرده</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر اول می که گیری اندر کف</p></div>
<div class="m2"><p>یاد شه راد دادگر کرده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>واندر دل مهربانت افتاده</p></div>
<div class="m2"><p>در زاری کار من نظر کرده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>امروز منم ثنا و شکر تو</p></div>
<div class="m2"><p>داروی تن و دل و جگر کرده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روزان و شبان ز بهر مدح تو</p></div>
<div class="m2"><p>دارم قلمی به دست سر کرده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بس زود کتابخانه را یابی</p></div>
<div class="m2"><p>از گفته من پر از گهر کرده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کی باشی باز گشته زان جانب</p></div>
<div class="m2"><p>نه راه به جانب دگر کرده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وین نصرت و فتح را من اندر خور</p></div>
<div class="m2"><p>بسیار دعای ما حضر کرده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دزدیده ز دور دیده دیدارت</p></div>
<div class="m2"><p>وز بیم پیادگان حذر کرده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا مهر ز خاور فلک باشد</p></div>
<div class="m2"><p>آهنگ به سوی باختر کرده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از خاور تا به باختر بادا</p></div>
<div class="m2"><p>رای تو به هر هنر سمر کرده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هر ساعت عز و دولت عالی</p></div>
<div class="m2"><p>باغ طرب تو تازه تر کرده</p></div></div>