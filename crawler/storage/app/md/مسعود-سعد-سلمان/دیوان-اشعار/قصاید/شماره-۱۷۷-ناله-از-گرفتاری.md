---
title: >-
    شمارهٔ ۱۷۷ - ناله از گرفتاری
---
# شمارهٔ ۱۷۷ - ناله از گرفتاری

<div class="b" id="bn1"><div class="m1"><p>چو گوگرد زد محنتم آذرنگ</p></div>
<div class="m2"><p>که در خاکم افکند چو بادرنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی هر زمان اژدهای سپهر</p></div>
<div class="m2"><p>ز دورم بدم درکشد چون نهنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآورد بازم بر آن کوهسار</p></div>
<div class="m2"><p>که بگرفت چنگم ز خرچنگ چنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی گوید ای طالع سرنگون</p></div>
<div class="m2"><p>چرایی همه ساله با من به جنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خداوند تو بادپایست و من</p></div>
<div class="m2"><p>ازو مانده زینگونه ام پای لنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین اختران او شتابنده تر</p></div>
<div class="m2"><p>تنم را چرا داد چندین درنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد از ظلمت خانه ام چشم کور</p></div>
<div class="m2"><p>شد از پستی پوششم پشت تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین سمج هرگز نگنجیدمی</p></div>
<div class="m2"><p>به صد چاره و جهد و نیرنگ و رنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرم تن نگشتی ازینسان نزار</p></div>
<div class="m2"><p>ورم دل نبودی ازینگونه تنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه کردم من ای چرخ کز بهر من</p></div>
<div class="m2"><p>کشی اسب کین را همی تنگ تنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه همخانه آهوان بوده ام</p></div>
<div class="m2"><p>که همخوابه ام کرده ای با پلنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همی تا کیم کرد باید نگاه</p></div>
<div class="m2"><p>به پشت و بدخش غیلواژ و رنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز عمرم چه لذت شناسی که هست</p></div>
<div class="m2"><p>طعامم کبست و شرابم شرنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو گونه نوا باشدم روز و شب</p></div>
<div class="m2"><p>ز آواز زاغ و ز بانگ کلنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه مایه طرب خیزد آن را ز دل</p></div>
<div class="m2"><p>که او را ازینسان بود نای و چنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بترسم همی کز نم دیدگان</p></div>
<div class="m2"><p>زند روی آیینه طبع زنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرا ناسپاسی کنم زین حصار</p></div>
<div class="m2"><p>چو در من بیفزود فرهنگ و هنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی شاه بندم کند هست فخر</p></div>
<div class="m2"><p>همی روزگارم زند نیست ننگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هنرهای طبعی پدیدار شد</p></div>
<div class="m2"><p>تنم را ازین انده و آذرنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز زخم و تراشیدن آید پدید</p></div>
<div class="m2"><p>بلی گوهر تیغ و نقش خدنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشد سنگ من موم ازین حادثه</p></div>
<div class="m2"><p>نه آب من از گرد شد تیره رنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ازیرا که بر من بلا و عنا</p></div>
<div class="m2"><p>چو آبست و چون گرد بر موم و سنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یقین دان تو مسعود کاین شعر تو</p></div>
<div class="m2"><p>یکی سنگ شد در ترازوی سنگ</p></div></div>