---
title: >-
    شمارهٔ ۸۹ - باز در مدح او
---
# شمارهٔ ۸۹ - باز در مدح او

<div class="b" id="bn1"><div class="m1"><p>از جور زمانه را جدا کرد</p></div>
<div class="m2"><p>با عدل به لطفش آشنا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شاه که تخت مملکت را</p></div>
<div class="m2"><p>چون چشمه مهر پرضیا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عادل ملکی که ایزد او را</p></div>
<div class="m2"><p>بر جمع ملوک پیشوا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاری کردش خدای بر ملک</p></div>
<div class="m2"><p>کو یاری دین مصطفی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ده شیر به رزم یک زمان کشت</p></div>
<div class="m2"><p>ده گنج به بزم یک عطا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شاه تو را خدا بی چون</p></div>
<div class="m2"><p>بر خلق زمانه پادشاه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر لوح نوشت نام ملکت</p></div>
<div class="m2"><p>بر ملک تو لوح را گوا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی همه خسروان تو را دید</p></div>
<div class="m2"><p>تاج همه خسروان تو را کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید ملوکی و شکوهت</p></div>
<div class="m2"><p>عمر همه خسروان هبا کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تأیید تو خاک درگه تو</p></div>
<div class="m2"><p>در گیتی اصل کیمیا کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اقبال تو گرد موکب تو</p></div>
<div class="m2"><p>در دیده ملک توتیا کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کین توز آب آتش افروخت</p></div>
<div class="m2"><p>مهر تو سموم را صبا کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون گردون گشت با تو یکتا</p></div>
<div class="m2"><p>در پیش تو پشت را دوتا کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر طبع که بود کم توانست</p></div>
<div class="m2"><p>اوصاف تو در خور سزا کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر وهم که هست کی تواند</p></div>
<div class="m2"><p>در بحر مدیحت آشنا کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای شاه جهان فلک ندانست</p></div>
<div class="m2"><p>آنگاه که بر تنم جفا کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون دید مرا به خدمت تو</p></div>
<div class="m2"><p>دانست که آن جفا خطا کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنست رهی که از دل و جان</p></div>
<div class="m2"><p>گاهیت دعا و گه ثنا کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همواره ثنات بر ملا گفت</p></div>
<div class="m2"><p>همواره دعات در خلا کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک مجلس اگر نگفت مدحت</p></div>
<div class="m2"><p>در مجلس دیگرش قضا کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لفظ تو چو نام بندگان برد</p></div>
<div class="m2"><p>نام رهی از میان رها کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرحوم تر از همه مرا دید</p></div>
<div class="m2"><p>محروم تر از همه مرا کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندیشه مرا به حق ایزد</p></div>
<div class="m2"><p>کز لذت خواب و خور جدا کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر بنده که از تو حاجتی خواست</p></div>
<div class="m2"><p>آن حاجت رای تو روا کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس رای تو بنده را فراموش</p></div>
<div class="m2"><p>از بهر خدای را چرا کرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باقی بادی که عدل را چرخ</p></div>
<div class="m2"><p>در ملک تو سایه بقا کرد</p></div></div>