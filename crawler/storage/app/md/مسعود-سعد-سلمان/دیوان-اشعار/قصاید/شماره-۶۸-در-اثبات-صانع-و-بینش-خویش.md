---
title: >-
    شمارهٔ ۶۸ - در اثبات صانع و بینش خویش
---
# شمارهٔ ۶۸ - در اثبات صانع و بینش خویش

<div class="b" id="bn1"><div class="m1"><p>جهان را عقل راه کاروان دید</p></div>
<div class="m2"><p>بضاعتهاش خوان استخوان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه ترکیب عمرش در فنا یافت</p></div>
<div class="m2"><p>همه بنیاد سودش بر زیان دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد خیره شد آنجا کز جهالت</p></div>
<div class="m2"><p>گروهی را ز صانع بر گمان دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا شد منکر صانع نگویی</p></div>
<div class="m2"><p>کسی کو کالبد را عقل و جان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان چون بینی اندر آئینه روی</p></div>
<div class="m2"><p>بد و نیک جهان چشمم چنان دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسی چشم سرم دید آشکارا</p></div>
<div class="m2"><p>دو چندان چشم سر اندر نهان دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تاریکی و محنت آن ندیدم</p></div>
<div class="m2"><p>که بتوانند مردان جهان دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به بینم از هر کس عجب نیست</p></div>
<div class="m2"><p>به تاریکی فراوان به توان دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سر من از آن دشمن خبر یافت</p></div>
<div class="m2"><p>که بر رویم ز خون دل نشان دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل زردم به رخ بر غم از آن کاشت</p></div>
<div class="m2"><p>که از چشمم دو جوی آب روان دید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبک در بوته زد مسکین تنم دست</p></div>
<div class="m2"><p>که بر گردن گنه بار گران دید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز ناشایست کردن شرمش آمد</p></div>
<div class="m2"><p>که بر دو کتف خود بار گران دید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فراوان بی خرد کاندر جهان او</p></div>
<div class="m2"><p>غم و شادی ز لعل این و آن دید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرد آن داشت کو نیک و بد خویش</p></div>
<div class="m2"><p>ز ایزد دید نه از آسمان دید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل بی خار اندر گلشن دهر</p></div>
<div class="m2"><p>به چشم تیز بین کی می توان دید</p></div></div>