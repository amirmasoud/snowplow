---
title: >-
    شمارهٔ ۱۹۸ - مدح سیف الدوله محمود
---
# شمارهٔ ۱۹۸ - مدح سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>چو روی چرخ شد از صبح چون صحیفه سیم</p></div>
<div class="m2"><p>ز قصر شاه مرا مژده داد باد نسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که عز ملت محمود سیف دولت را</p></div>
<div class="m2"><p>ابوالمظفر سلطان عادل ابراهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فزود حشمت و رتبت به دولت عالی</p></div>
<div class="m2"><p>چو کرد مملکت هند را بدو تسلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نام فرخ او خطبه کرد در همه هند</p></div>
<div class="m2"><p>نهاد بر سر اقبالش از شرف دیهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی ستام مرصع به گوهر الوان</p></div>
<div class="m2"><p>علی جواد کالنجم صبح لیس بهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سم و دیده سیاه و به دست و پای سپید</p></div>
<div class="m2"><p>میان و ساقش لاغر بروسرینش جسیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آب همچون کشتی و بر هوا چون باد</p></div>
<div class="m2"><p>به کوه همچو گوزن و به دشت همچو ظلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گاه گشتن جولان کند به حلقه نون</p></div>
<div class="m2"><p>به کوه همچو گوزن و به دشت همچو ظلیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گاه گشتن جولان کند به حلقه نون</p></div>
<div class="m2"><p>به گاه جستن بیرون جهد ز چشمه میم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خجسته بادا بر شاه خلعت سلطان</p></div>
<div class="m2"><p>به کامگاری بر تخت و ملک باد مقیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منجمان همه گفتند کاین دلیل کند</p></div>
<div class="m2"><p>به حکم زیج بتانی که هست در تقویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه دیر زود خطیبان کنند بر منبر</p></div>
<div class="m2"><p>به نام سیف دول خطبه های هفت اقلیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به سال پنجه ازین پیش گفت بوریحان</p></div>
<div class="m2"><p>در آن کتاب که کردست نام او تفهیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که پادشاهی صاحبقران شود به جهان</p></div>
<div class="m2"><p>چو سال هجرت بگذشت تی و سین و سه جیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هزار شکر به هر ساعتی خدا را</p></div>
<div class="m2"><p>که داد ما را شاهی بزرگوار و کریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مبارزی که به هیجا ز تیغ و نیزه او</p></div>
<div class="m2"><p>بترس باشد ترس و به بیم باشد بیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر دو آید پیشش کند به نیزه یکی</p></div>
<div class="m2"><p>وگر یک آید نزدش کند به تیغ دونیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز تیغ همچو شهابش همان رسد به عدو</p></div>
<div class="m2"><p>کجا رسد ز شهاب فلک به دیو رجیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خدایگانا آن رانده ز تیغ به هند</p></div>
<div class="m2"><p>که آن نراند کلاب و عدی به تیم و تمیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شده ز بس خون بیجاده سم گوزن به کوه</p></div>
<div class="m2"><p>شده به بحر عقیقین پشیزه ماهی سیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون به دولت تو ملک را فزاید فر</p></div>
<div class="m2"><p>کنون به فر تو هندوستان شود چو نعیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به باغهاش نروید مگر که اغچه زر</p></div>
<div class="m2"><p>به روز ابر نبارد مگر که در یتیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همیشه تا سر زلفین نیکوان بتان</p></div>
<div class="m2"><p>چوخی و جیم شود هر دو بر صحیفه سیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز نجم سعدت بادا زمان زمان الهام</p></div>
<div class="m2"><p>ز بخت نیکت بادا زمان زمان تعلیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زمین ز عدل تو مانند باغ تو چو بهشت</p></div>
<div class="m2"><p>جهان ز عدل تو مانند قصر تو چو حریم</p></div></div>