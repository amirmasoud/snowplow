---
title: >-
    شمارهٔ ۱۶۶ - صفت اراده خویش و آرزوی سفر خراسان
---
# شمارهٔ ۱۶۶ - صفت اراده خویش و آرزوی سفر خراسان

<div class="b" id="bn1"><div class="m1"><p>چو عزم کاری کردم مرا که دارد باز</p></div>
<div class="m2"><p>رسد به فرجام آن کار کش کنم آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شبی که آز برآرد کنم به همت روز</p></div>
<div class="m2"><p>دری که چرخ ببندد کنم به دانش باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ندارم گردون نگویدم که بدار</p></div>
<div class="m2"><p>وگر نتازم گردون نگویدم که بتاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه خیره گردد چشم من از شب تاری</p></div>
<div class="m2"><p>نه سست گردد پای من از طریق دراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هیچ حالی هرگز دو تا نشد پشتم</p></div>
<div class="m2"><p>مگر به بارگه شهریار وقت نماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در و گوهر در سنگ و در صدف دایم</p></div>
<div class="m2"><p>ز طبع و خاطر از نظم و نثر دارم راز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بی تمیزی این هر دو تا چو بندیشم</p></div>
<div class="m2"><p>چو بی زبانان هرگز به کس نگویم راز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی گذارد خسرو ز پیش خویش مرا</p></div>
<div class="m2"><p>که در هوای خراسان یکی کنم پرواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه از پی عزست پای باز به بند</p></div>
<div class="m2"><p>چو نام بندست آن عز همی نخواهد باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا بکش همه رنج و مجوی آسانی</p></div>
<div class="m2"><p>که کار گیتی بی رنج می نگیرد ساز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فزونت رنج رسد چون به برتری کوشی</p></div>
<div class="m2"><p>که مانده تر شوی آنگه که بر شوی به فراز</p></div></div>