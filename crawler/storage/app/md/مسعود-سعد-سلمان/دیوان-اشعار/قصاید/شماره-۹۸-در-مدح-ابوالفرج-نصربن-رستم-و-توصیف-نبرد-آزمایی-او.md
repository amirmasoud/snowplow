---
title: >-
    شمارهٔ ۹۸ - در مدح ابوالفرج نصربن رستم و توصیف نبرد آزمایی او
---
# شمارهٔ ۹۸ - در مدح ابوالفرج نصربن رستم و توصیف نبرد آزمایی او

<div class="b" id="bn1"><div class="m1"><p>آن ترجمان غیب و نماینده هنر</p></div>
<div class="m2"><p>آن کز گمان خلق مر او را بود خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن زرد چهره که کند روی دوست سرخ</p></div>
<div class="m2"><p>شخصی نه جانور برود همچو جانور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غواص پیشه ای که به دریا فرو شود</p></div>
<div class="m2"><p>از قعر بحر تیره به آرد بسی درر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن شمع برفروخته بر تخته چو سیم</p></div>
<div class="m2"><p>گر دود شمع زیر بود روشنی ز بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوینده ای که هست سخنهاش و جانش نیست</p></div>
<div class="m2"><p>پرنده ای که هست پریدنش و نیست پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغان اگر به پای روند به پر پرند</p></div>
<div class="m2"><p>او کار پای و پر بکند هر زمان به سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او را دو شاخ نکنی پیوسته هر یکی</p></div>
<div class="m2"><p>یک شاخ با قضا و دگر شاخ با قدر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی شاخ بر ولی و دگر شاخ بر عدو</p></div>
<div class="m2"><p>زین بر ولی سعادت و آن بر عدو ضرر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان یافت کلک مرتبت صد هزار تیغ</p></div>
<div class="m2"><p>کو کرد بر بنان عمید اجل گذر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آزاده بوالفرج فرج ما ز هر غمی</p></div>
<div class="m2"><p>نصر بن روستم به وغا رستم دگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کز بوالفرج رسید جهان را زهر بدی</p></div>
<div class="m2"><p>فتح و فراغت و فرح و نصرت و ظفر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رستم به کارزار یکی دیو خیره کشت</p></div>
<div class="m2"><p>این اند سال کرد به مازندران گذر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیکار نصر رستم با صد هزار دیو</p></div>
<div class="m2"><p>هر روز تا شبست و ز هر شام تا سحر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن دیو بد سپید و سیاهند این همه</p></div>
<div class="m2"><p>هست این زمین هند ز مازندران بتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نصرست نام خواجه فرامرز خوانمش</p></div>
<div class="m2"><p>زیرا که رستم است فرامرز را پدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن سایه خدا و عمید خدایگان</p></div>
<div class="m2"><p>کش از خدایگان ظفرست از خدای فر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او خود به مملکت ز عمیدان مملکت</p></div>
<div class="m2"><p>پیداترست از آنکه از انجم بود قمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن مهتر خطیر نکو خاطر و ضمیر</p></div>
<div class="m2"><p>هرگز نبوده خواسته را پیش او خطر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از گل سرشت کالبد ما همه خدای</p></div>
<div class="m2"><p>او را ز جاه وجود سرشت و نکو سیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خورده جهان بسی و نخورده چو او کسی</p></div>
<div class="m2"><p>اندر فنون دانش و هر فضل بهره ور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در خدمت ملوک سپرده تن عزیز</p></div>
<div class="m2"><p>استاده پیش شغل جهاندار چو سپر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای مهتری که خلق تو خلق پیمبرست</p></div>
<div class="m2"><p>برهان تست فضل و سخایت بود هنر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بودی از خدای جهان را پیمبری</p></div>
<div class="m2"><p>بعد از نبی محمد بر خلق بحر و بر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این خلق را پیمبر دیگر تو میبدی</p></div>
<div class="m2"><p>کت هست علم آن و سخن گشت مختصر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر کوترا سوار به بیند معاینه</p></div>
<div class="m2"><p>روح الامین شناسد و نشناسد از بشر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گویند کاین فریشته آنست کامدی</p></div>
<div class="m2"><p>گه گه به میر مکه ز یزدان کامگر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایدون بتابد از تو کمال و جمال تو</p></div>
<div class="m2"><p>چونان که نور شمس بتابد ز باختر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای باغ جود از تو سراسر فروخته</p></div>
<div class="m2"><p>بر تو زمانه باد بقا را گشاده در</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دریا اگر چه در یتیم اندرو بود</p></div>
<div class="m2"><p>با کف تو حقیرترست از یکی شمر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آتش ز تف آتش خشمت نهان شدست</p></div>
<div class="m2"><p>حصنی گرفته ز آهن و پولاد و از حجر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای چشم جود را بصر و عقل را روان</p></div>
<div class="m2"><p>گر عقل را روان بدی و جود را بصر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چونان که کان گوهر در کوه مضمرست</p></div>
<div class="m2"><p>کوهیست در تو حلم و درو فضل تو گهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نامی ز تو شدند سراسر تبار تو</p></div>
<div class="m2"><p>گر چه به اصل و فضل بزرگند و نامور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آزادگی بگشت به گرد جهان بسی</p></div>
<div class="m2"><p>آخر در اصل دولت تو گشت مستقر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زان پیش کز عدم به وجود آمدی خدای</p></div>
<div class="m2"><p>موجود کرده بود هنر در تو سر به سر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر زایران تویی به سخا کیس های سیم</p></div>
<div class="m2"><p>بر شاعران تویی به عطا بدرهای زر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر نظم و نثر و فضل تویی شاعر و سوار</p></div>
<div class="m2"><p>خوش طبع و خوش نوایی و خوش لفظ چون شکر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاعر نواز و شعرشناسی و شعر خواه</p></div>
<div class="m2"><p>آری چنین بوند بزرگان مشتهر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من مرده زنده گشتم و اکنون شدم جوان</p></div>
<div class="m2"><p>یک ذره گر ز جود تو بر من کند اثر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این روز و روزگار تو بر من خجسته باد</p></div>
<div class="m2"><p>از هم گسسته باد دل دشمن و جگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>سر سبز و دل قوی و تن آباد و شادزی</p></div>
<div class="m2"><p>وانکس که او نه شاد حزین باد و کور و کر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چندانکه هست بر فلک استاره را شمار</p></div>
<div class="m2"><p>تو شاد زی و مدت عمرت همی شمر</p></div></div>