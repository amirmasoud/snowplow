---
title: >-
    شمارهٔ ۲۶۸ - قصیده دیگر در مدح ملک ارسلان
---
# شمارهٔ ۲۶۸ - قصیده دیگر در مدح ملک ارسلان

<div class="b" id="bn1"><div class="m1"><p>ملک ملک ارسلان</p></div>
<div class="m2"><p>ساکن روض الجنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه زمانه فروز</p></div>
<div class="m2"><p>خسرو صاحبقران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رایت و رایش بلند</p></div>
<div class="m2"><p>دولت و بختش جوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت او آفتاب</p></div>
<div class="m2"><p>رتبت او آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب راهی بزن</p></div>
<div class="m2"><p>راوی بیتی بخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فی ملک عدله</p></div>
<div class="m2"><p>یخدمها النیران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بدل اردشیر</p></div>
<div class="m2"><p>وی عوض اردوان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنده امرت سپهر</p></div>
<div class="m2"><p>بسته حکمت جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ملک کامران</p></div>
<div class="m2"><p>خسرو صاحبقران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش به خواب اندرون</p></div>
<div class="m2"><p>وقت سپیده دمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد نزد رهی</p></div>
<div class="m2"><p>روان نوشیروان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت که مسعود سعد</p></div>
<div class="m2"><p>شاعر چیره زبان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دیدی عدلی که خلق</p></div>
<div class="m2"><p>یاد ندارد چنان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دید کآباد کرد</p></div>
<div class="m2"><p>جمله زمین و زمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عدل ملک بوالملوک</p></div>
<div class="m2"><p>شاه ملک ارسلان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در صفت عدل او</p></div>
<div class="m2"><p>مدح به گردون رسان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ورچه امروز هست</p></div>
<div class="m2"><p>تنت چنین ناتوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو گرددت تن درست</p></div>
<div class="m2"><p>و ایمن گردی به جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو وصف این عدل کن</p></div>
<div class="m2"><p>به وصف نیکو بیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درین معانی به شعر</p></div>
<div class="m2"><p>بساز ده داستان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای ملک مال ده</p></div>
<div class="m2"><p>خسرو گیتی ستان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سیاست ملک را</p></div>
<div class="m2"><p>پیش تو در یک زمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جمع شد از هر سویی</p></div>
<div class="m2"><p>دویست کوه روان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمله بر آن هر یکی</p></div>
<div class="m2"><p>یک اژدهای دمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر سر هر پیل مست</p></div>
<div class="m2"><p>نشسته یک پیلبان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برین سیاست که رفت</p></div>
<div class="m2"><p>ای ملک کامران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قحط چو باران نشاند</p></div>
<div class="m2"><p>رحمت تو از جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>احسنت ای پادشاه</p></div>
<div class="m2"><p>شاد به گیتی بمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>داشتن ملک و دین</p></div>
<div class="m2"><p>جز که چنین کی توان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خلق جهان را همه</p></div>
<div class="m2"><p>کودک و پیر و جوان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به جود کردی غنی</p></div>
<div class="m2"><p>به عدل دادی امان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زایل کردی شها</p></div>
<div class="m2"><p>ز خلق نرخ گران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جانشان دادی همه</p></div>
<div class="m2"><p>که اصل جانست نان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خلق به گیتی ندید</p></div>
<div class="m2"><p>چون تو شهی مهربان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زین پس دزدان شوند</p></div>
<div class="m2"><p>بدرقه کاروان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیش نترسد ز گرگ</p></div>
<div class="m2"><p>بر رمه مرد شبان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز جود خالی نه ای</p></div>
<div class="m2"><p>حظی داری از آن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عدل تو بر ملک و دین</p></div>
<div class="m2"><p>جود تو بر گنج و کان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون تو نبودست و نیست</p></div>
<div class="m2"><p>خسرو فرمان روان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عادلی و عدل تو</p></div>
<div class="m2"><p>رسید در هر مکان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>شاها با عدل و ملک</p></div>
<div class="m2"><p>زنده بمان جاودان</p></div></div>