---
title: >-
    شمارهٔ ۸۱ - گفتگو از روشنان فلکی و سیاهکاری آنان
---
# شمارهٔ ۸۱ - گفتگو از روشنان فلکی و سیاهکاری آنان

<div class="b" id="bn1"><div class="m1"><p>چو سوده دوده به روی هوا برافشانند</p></div>
<div class="m2"><p>فروغ آتش روشن ز دود بنشانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر گردان بس چشم ها گشاید باز</p></div>
<div class="m2"><p>که چشم های جهان را همه بخسبانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن سبیکه زر کافتاب گویندش</p></div>
<div class="m2"><p>زند ستامی کانرا ستارگان خوانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان گمان بودم کآسیای گردون را</p></div>
<div class="m2"><p>همی به تیزی بر فرق من بگردانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب دیده گریان چو تیغم آب دهند</p></div>
<div class="m2"><p>کز آتش دل سوزان مرا بتفسانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنند رویم همرنگ برگ رز به خزان</p></div>
<div class="m2"><p>چو شوشه رزم اندر بلا بپیچانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتم انس به غم ها و اندهان گر چند</p></div>
<div class="m2"><p>منازغان چو دل و زندگانی و جانند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمادمند و نیابند بر تنم پیدا</p></div>
<div class="m2"><p>به ریگ تافته بر قطره های بارانند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین فروخته رویان نگه کنم که همی</p></div>
<div class="m2"><p>به نور طبعی روی زمین فروزانند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سپهبدان برآشفته لشکری گشتند</p></div>
<div class="m2"><p>چنانکه خواهند از هر سویی همی رانند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گمان مبر که مگر طبع های مختلفند</p></div>
<div class="m2"><p>گمان مبر که همه طبع ها برنجانند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مسافران نواحی هفت گردونند</p></div>
<div class="m2"><p>مؤثران مزاج چهار ارکانند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هلاک و عیش و بد و نیک و شدت و فرجند</p></div>
<div class="m2"><p>غم و سرور و کم و بیش و درد و درمانند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به شکل همجنس از با بهانه همجنسند</p></div>
<div class="m2"><p>به نور همسان و ز فعل ها نه همسانند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر قدم حکم روزگار و گردونند</p></div>
<div class="m2"><p>به هر نظر سبب آشکار و پنهانند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه بلند برآرند پس فرو فکنند</p></div>
<div class="m2"><p>همی فراوان بدهند و باز بستانند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه بلند برآرند پس فرو فکنند</p></div>
<div class="m2"><p>همی فراوان بدهند و باز بستانند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کجا توانم جستن که تیز پایانند</p></div>
<div class="m2"><p>چه چاره دانم کردن که چیره دستانند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روندگان سپهرند و لنگشان خواهم</p></div>
<div class="m2"><p>ز بهر آنکه مرا رهبران زندانند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر خلندم در دیده نیست هیچ شگفت</p></div>
<div class="m2"><p>که تیره شب را بر فرق قوس پیکانند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روا بود که ازین اختران گله نکنم</p></div>
<div class="m2"><p>که بی گمان همه فرمانبران یزدانند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز اهل عصر چه خواهم که اهل عصر همه</p></div>
<div class="m2"><p>به خوی و طبع ستوران ماده را مانند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر به رحمت ایشان فریفته نشوی</p></div>
<div class="m2"><p>نکو نگر که همه اندک و فراوانند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مخواه تابش از ایشان اگر همه مهرند</p></div>
<div class="m2"><p>مجوی گوهر از ایشان اگر همه کانند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به جان خرند قصاید ز من خردمندان</p></div>
<div class="m2"><p>اگر چه طبع مرا زان کلام ارزانند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز چرخ عقلم زادند وز جمال و بقا</p></div>
<div class="m2"><p>ستارگان را مانند و جاودان مانند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمانه گفته من حفظ کرد و نزدیکست</p></div>
<div class="m2"><p>که اخترانش بر آفتاب و مه خوانند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان که بیضه عنبر به بوی دریابند</p></div>
<div class="m2"><p>مرا بدانند آنها که شعر من خوانند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>محل این سخن سرفراز بشناسند</p></div>
<div class="m2"><p>کسان که سغبه مسعود سعد سلمانند</p></div></div>