---
title: >-
    شمارهٔ ۲۰ - وصف بهار و ستایش سیف الدوله محمود
---
# شمارهٔ ۲۰ - وصف بهار و ستایش سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>مگر مشاطه بستان شدند با دو سحاب</p></div>
<div class="m2"><p>که این ببستش پیرایه وان گشاد نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به در و گوهر آراسته پدید آمد</p></div>
<div class="m2"><p>چو نوعروسی در کله از میان حجاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآمد ابر به کردار عاشق رعنا</p></div>
<div class="m2"><p>کشیده دامن و افراشته سر از اعجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی لآلی پاشد همی و گه کافور</p></div>
<div class="m2"><p>گهی حواصل پوشد همی و گه سنجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چرخ گردان دولاب وار آب روان</p></div>
<div class="m2"><p>به گاه و بی گاه آری چنین بود دولاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زیر قطره شکوفه چنان نماید راست</p></div>
<div class="m2"><p>که از بلور نمایند صورت لبلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل مورد خندان و دیده بگشاده</p></div>
<div class="m2"><p>دو طبع مختلفش داده فعل با دو سحاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسان دوست که یابد وصال یار عزیز</p></div>
<div class="m2"><p>پس از فراق دراز و پس از عنا و عذاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز لهو آمده رنج و ز وصل دیده فراق</p></div>
<div class="m2"><p>لبان خویش کند پر ز خنده دیده پر آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بوی نافه آهوست سنبل بویا</p></div>
<div class="m2"><p>به روی رنگ تذروست لاله سیراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن خجسته و شاه اسپرغم هر دو شدند</p></div>
<div class="m2"><p>یکی چو دیده چرخ و یکی چو چنگ عقاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز شاخ خویش سمن تافت چو ستاره روز</p></div>
<div class="m2"><p>ز باغ همچو شب از روز شد رمنده غراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار دستان با فاخته گمان بردند</p></div>
<div class="m2"><p>که گشت باران در جام لاله باده ناب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به رسم رفته چو رامشگران خوش دستان</p></div>
<div class="m2"><p>یکی بساخت کمانچه یکی نواخت رباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو گفت بلبل بانگ نماز غنچه گل</p></div>
<div class="m2"><p>بسان مستان بگشاد چشم خویش از خواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به پیش لاله بنفشه سجود کرد چو دید</p></div>
<div class="m2"><p>که هر دو برگی از لاله شد یکی محراب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگر که بود دم جبرئیل باد صبا</p></div>
<div class="m2"><p>که همچو عیسی مریم بزاد گل ز تراب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون مگر دم عیسی است بوی گل به سحر</p></div>
<div class="m2"><p>که زنده گشت ازو خاطر اولوالالباب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دهان گل را کرده است ابر پر لؤلؤ</p></div>
<div class="m2"><p>به مژده ای که ازو باز یافتست شباب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه مژده گفت که امروز شاه خواهد کرد</p></div>
<div class="m2"><p>به شادمانی و رامش نشاط جام و شراب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدایگان جهان سیف داد و دولت و دین</p></div>
<div class="m2"><p>به شادمانی و رامش میان باغ و سراب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک به اصل و به آدم رساند نسبت ملک</p></div>
<div class="m2"><p>کراست از ملکان در جهان چنین انساب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه سائلست حسامش که چون سؤال کند</p></div>
<div class="m2"><p>نباشد او را جز حال بدسگال جواب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز برق و آبست الماس وین شگفت نگر</p></div>
<div class="m2"><p>کز آب و الماسش برق خاست روز حراب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بتافتند بر آتش سنان و حربه او</p></div>
<div class="m2"><p>گرفت آتش از آن روز باز نیرو و تاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چگونه خاست ز پیکان همچو سیمابش</p></div>
<div class="m2"><p>شهاب از آنکه ز سیماب نیست اصل شهاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو آن مظفر شاهی که باز تو شد گه رزم</p></div>
<div class="m2"><p>قضا عدیل عنان و قدر رفیق رکاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو بازگردی از حمله باشی آهسته</p></div>
<div class="m2"><p>به گاه حمله که حمله بری شوی پرتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلی تو سیفی و سیف این چنین بود دایم</p></div>
<div class="m2"><p>که بازگردد به درنگ و در رود به شتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خدای را چو به کاری ارادتی باشد</p></div>
<div class="m2"><p>به صنع و حکمت خویشش بسازدش اسباب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو کرد خطبه به نامت خطیب بر منبر</p></div>
<div class="m2"><p>گشاده کرد به رحمت بر آسمان ابواب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر نه همت تو داشتی گرفته هوا</p></div>
<div class="m2"><p>بر آسمان شدی این خطبه و خطیب و خطاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خجسته بادت نوروز و این چنین نوروز</p></div>
<div class="m2"><p>هزار جفت شده با مه رجب دریاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بسان عرعر در بوستان ملک ببال</p></div>
<div class="m2"><p>بسان خورشید از آسمان عمر بتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به طوع و رغبت داده تو را زمانه زمان</p></div>
<div class="m2"><p>به امر و نهی نهاده تو را ملوک رقاب</p></div></div>