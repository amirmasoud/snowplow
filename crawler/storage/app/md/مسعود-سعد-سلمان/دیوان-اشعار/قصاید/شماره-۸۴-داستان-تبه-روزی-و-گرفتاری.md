---
title: >-
    شمارهٔ ۸۴ - داستان تبه روزی و گرفتاری
---
# شمارهٔ ۸۴ - داستان تبه روزی و گرفتاری

<div class="b" id="bn1"><div class="m1"><p>بیچاره تن من که ز غم جانش برآمد</p></div>
<div class="m2"><p>از دست بشد کارش و از پای درآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز به جهان دید کسی غم چو غم من</p></div>
<div class="m2"><p>کز سر شودم تازه چو گویم به سر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن داد مرا گردش گردون که ز سختی</p></div>
<div class="m2"><p>من زهر بخوردم به دهانم شکر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان آتش سوزنده مرا گشت که دوزخ</p></div>
<div class="m2"><p>در خواب بدیدم به دو چشمم شرر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز بر تن من نیست گذر راه بلا را</p></div>
<div class="m2"><p>گویی که بلا را تن من رهگذر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با لشکر تیمار حشر خواستم از تن</p></div>
<div class="m2"><p>از آب دو چشمم به دو رخ بر حشر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانم بشدی گر نبدی دل که دل من</p></div>
<div class="m2"><p>از تیر بلا پیش من اندر سپر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر تیر که گردون به سوی جان من انداخت</p></div>
<div class="m2"><p>دل گشت سپر بر دل بیچاره برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون پاره شد از تیر بلا این دل مسکین</p></div>
<div class="m2"><p>هر تیر که آمد پس از آن بر جگر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بس زود برآمد ز فلک کوکب سعدم</p></div>
<div class="m2"><p>چه سود که در وقت فرو شد چو برآمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن شب که دگر روز مرا عزم سفر بود</p></div>
<div class="m2"><p>ناگاه ز اطراف نسیم سحر آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بوی تبتی مشک و گل زرد همی زد</p></div>
<div class="m2"><p>وان ترک من از حجره چو خورشید برآمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان دیده چون نرگس چون دیده نرگس</p></div>
<div class="m2"><p>در دیده تاریک پر آبم سهر آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یک حلقه کوتاه ز زلفش بکشیدم</p></div>
<div class="m2"><p>زان حلقه مر او را به میان بر کمر آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زان زلفک پرتاب و از آن دیده پر خواب</p></div>
<div class="m2"><p>یک آستی و دامن مشک و گهر آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفتم که مرا توشه ده از دو لب نوشین</p></div>
<div class="m2"><p>کاهنگ سفر کردم و وقت سفر آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از خط وفا سرمکش و دل مبر از من</p></div>
<div class="m2"><p>کاین عشق همه رنج و درد سر آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتا چه کنم من که ازین عشق جهانسوز</p></div>
<div class="m2"><p>دل در سر اندوه شد و جان در خطر آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک هجر به سر نامده هجری دگر افتاد</p></div>
<div class="m2"><p>یک غم سپری ناشده غمی دگر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون ابر ز غم دیده من باران بارید</p></div>
<div class="m2"><p>تا شاخ فراق امروز دیگر به بر آمد</p></div></div>