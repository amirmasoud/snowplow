---
title: >-
    شمارهٔ ۱۴۴ - باز در ستایش او
---
# شمارهٔ ۱۴۴ - باز در ستایش او

<div class="b" id="bn1"><div class="m1"><p>شاه محمود سیف دولت و دین</p></div>
<div class="m2"><p>هر کجا باشد او به بحر و به بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفت بادش سر و رو دولت و بخت</p></div>
<div class="m2"><p>رهبرش فتح و یمن و نصر و ظفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه پیروز بخت فرخ پی</p></div>
<div class="m2"><p>ملک عادل فرشته سیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه آراستست مجلس ازو</p></div>
<div class="m2"><p>وانکه پیراستست ازو لشکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک دولت گرفته زو رونق</p></div>
<div class="m2"><p>پادشاهی بدو شده انور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب جهانش خوانم از آنک</p></div>
<div class="m2"><p>هست پر نور از آن همه کشور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رای او جسم فضل را چون جان</p></div>
<div class="m2"><p>رسم او چشم عقل را چو بصر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مثل پای گر نهد بر سنگ</p></div>
<div class="m2"><p>سنگ گردد به پیش پایش زر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پادشاهی که سهم او گه صید</p></div>
<div class="m2"><p>جان ستاند ز شیر شرزه نر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به مصاف اندرون به وقت نبرد</p></div>
<div class="m2"><p>در سر سرکشان کشد معجر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بند محکم همه گشاده شود</p></div>
<div class="m2"><p>چون ملک بر میان ببست کمر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر رهی کو گذر کند نکنند</p></div>
<div class="m2"><p>شرزه شیران بدان حدود گذر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قبضه تیغ او شده ست قضا</p></div>
<div class="m2"><p>تا که پیکان او شده ست قدر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این رود همچو فکرت اندر دل</p></div>
<div class="m2"><p>وان بود همچو دانش اندر سر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گه جنگ در میان مصاف</p></div>
<div class="m2"><p>چون برد حمله شاه را بنگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به برگرد افکنست و شیر شکار</p></div>
<div class="m2"><p>شیر مرد اوژنست و ببر شکر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کافران پیش او چنان باشند</p></div>
<div class="m2"><p>که نی و چوب خشک بر آذر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای سنان تو را رفیق فتوح</p></div>
<div class="m2"><p>وی حسام تو را ظفر رهبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای ز گرزت همیشه ترسان ترس</p></div>
<div class="m2"><p>وی ز شمشیر تو حذر به حذر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آفرین گوی ملک تو شده اند</p></div>
<div class="m2"><p>به گه حمله در مصاف اندر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرز و زوبین و خشت و نیزه و تیر</p></div>
<div class="m2"><p>سپر و تیغ و ناچخ و خنجر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون که امسال رای غزو افتاد</p></div>
<div class="m2"><p>به سعادت شدی به سوی سفر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کاشکی چشم من زمین بودی</p></div>
<div class="m2"><p>تا بر آن داشتی مقام و ممر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنده گر در سفر به خدمت نیست</p></div>
<div class="m2"><p>به نپرداخت از دعا به حضر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برو ای شه که یار تست خدای</p></div>
<div class="m2"><p>در همه کارت اوست یاریگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جان به پیشت نثار کرد و سبیل</p></div>
<div class="m2"><p>یله گاوان فربه و منکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این دلیلست کت ظفر باشد</p></div>
<div class="m2"><p>بر عدوی خدای و پیغمبر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زود باز آی ای ملک به مراد</p></div>
<div class="m2"><p>با دل شاد و نصرت بی مر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگشایی به دوستاران بر</p></div>
<div class="m2"><p>چون بیایی به لهو و شادی در</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاد بادی ز بخت و دولت خویش</p></div>
<div class="m2"><p>ای به تو شاد دوستان یکسر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>باش باقی تو تا جهان باقیست</p></div>
<div class="m2"><p>از جوانی و مملکت بر خور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سر تو سبز و تاج بر سر تو</p></div>
<div class="m2"><p>دشمنت را برید سر ز تبر</p></div></div>