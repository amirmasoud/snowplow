---
title: >-
    شمارهٔ ۱۲۰ - گل و می بهتر
---
# شمارهٔ ۱۲۰ - گل و می بهتر

<div class="b" id="bn1"><div class="m1"><p>یک شب از نوبهار وقت سحر</p></div>
<div class="m2"><p>باد بر باغ کرد راهگذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه گل پیام داد به می</p></div>
<div class="m2"><p>گفت من آمدم به باغ اندر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیمه ها ساختیم ز بیرم چین</p></div>
<div class="m2"><p>فرش کردم ز دیبه ششتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نز عماری من آمدم بیرون</p></div>
<div class="m2"><p>نه بدیدست روی من مادر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگشادم نقاب سبز از روی</p></div>
<div class="m2"><p>نه نمودم به کس رخ احمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد بر من دمید مشک و عبیر</p></div>
<div class="m2"><p>ابر بر من فشاند در و گهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منتظر بوده ام ز بهر تو را</p></div>
<div class="m2"><p>کرده ام در میان باغ مقر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر در این هفته نزد من نایی</p></div>
<div class="m2"><p>در نیابیم تا به سال دگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد چون باده را بگفت پیام</p></div>
<div class="m2"><p>لرزه بر وی فتاد در ساغر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شادمان گشت و اهتزاز نمود</p></div>
<div class="m2"><p>روی او سرخ شد ز لهو و بطر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باد را گفت اینت خوش پیغام</p></div>
<div class="m2"><p>مرحبا اینت خوب و نغز خبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز گرد و بگو جواب پیام</p></div>
<div class="m2"><p>بازگو آنچه گویمت یکسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گو تو هستی مخالف و بد عهد</p></div>
<div class="m2"><p>کس ندیدم ز تو مخالف تر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سال تا سال منتظر باشیم</p></div>
<div class="m2"><p>تا ببینیم چهره تو مگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بیایی نپایی ایدر دیر</p></div>
<div class="m2"><p>باربندی و بر شوی زایدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوب رویی و خوبرویان را</p></div>
<div class="m2"><p>عهد با روی کی بود در خور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چند گه بازداشت بودم من</p></div>
<div class="m2"><p>نه شنیدم نوای خنیاگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اینک از دولت و سعادت تو</p></div>
<div class="m2"><p>من ز حبس آمدم سوی منظر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسوت من شدست جام بلور</p></div>
<div class="m2"><p>مرکبم دست ترک سیمین بر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زود بشتاب تا به فرخ بزم</p></div>
<div class="m2"><p>یابی از جود شهریار نظر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه با زر تو را برآمیزد</p></div>
<div class="m2"><p>بر فشاند به دوستاران بر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باد از بوی باده مست شده</p></div>
<div class="m2"><p>بازگشت و به باغ کرد گذر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر چه پیش آمدش همی بربود</p></div>
<div class="m2"><p>هر چه بسپرد کرد زیر و زبر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در گل آویخت اندر او و چنانک</p></div>
<div class="m2"><p>سبز حله ش دریده شد در بر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روی گل ناگهان پدید آمد</p></div>
<div class="m2"><p>از میان زمردین چادر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون نگه کرد گل برابر دید</p></div>
<div class="m2"><p>روی مه را ز گنبد اخضر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد ز تشویر ماه رویش سرخ</p></div>
<div class="m2"><p>در غم جامه گشت چشمش تر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شادمان شد همه شب و همه روز</p></div>
<div class="m2"><p>شعرها می سراید از هر در</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همچو خنیاگران شاه جهان</p></div>
<div class="m2"><p>هر زمانی زنده ره دیگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاه محمود سیف دولت و دین</p></div>
<div class="m2"><p>میر صف دار و خسرو صفدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پادشاه ستوده سیرت و رسم</p></div>
<div class="m2"><p>شهریار خجسته طالع و فر</p></div></div>