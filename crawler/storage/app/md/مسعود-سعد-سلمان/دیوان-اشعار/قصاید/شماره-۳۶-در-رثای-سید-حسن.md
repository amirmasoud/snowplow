---
title: >-
    شمارهٔ ۳۶ - در رثای سید حسن
---
# شمارهٔ ۳۶ - در رثای سید حسن

<div class="b" id="bn1"><div class="m1"><p>بر تو سید حسن دلم سوزد</p></div>
<div class="m2"><p>که چو تو هیچ غمگسار نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن من زار بر تو می نالد</p></div>
<div class="m2"><p>که تنم هیچ چون تو یار نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان تو را خاک در کنار گرفت</p></div>
<div class="m2"><p>که چو تو شاه در کنار نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان اجل اختیار جان تو کرد</p></div>
<div class="m2"><p>که به از جانت اختیار نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان بکشتت قضا که بر سر تو</p></div>
<div class="m2"><p>دست جد تو ذوالفقار نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم به مرگی فگار باد تنی</p></div>
<div class="m2"><p>که دلش مرگ تو فگار نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای غریبی کجا مصیبت تو</p></div>
<div class="m2"><p>هیچ دانا غریب وار نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عزیزی که در همه احوال</p></div>
<div class="m2"><p>جان من دوستیت خوار نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ مردانگیت زنگ نزد</p></div>
<div class="m2"><p>گل آزادگیت خار نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آب مهر تو را خلاب نبود</p></div>
<div class="m2"><p>آتش خشم تو شرار نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ میدان فضل و مرکب عقل</p></div>
<div class="m2"><p>در کفایت چو تو سوار نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من شناسم که چرخ خاک نگار</p></div>
<div class="m2"><p>چون سخن های تو نگار نداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خطا خاطرت کژی نگرفت</p></div>
<div class="m2"><p>از جفا طبع تو غبار نداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگرفت عیار اثیر فلک</p></div>
<div class="m2"><p>که مگر بوته عیار نداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سی نشد سال عمر تو ویحک</p></div>
<div class="m2"><p>سال زاد تو را شمار نداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این قدر داد چون تویی را عمر</p></div>
<div class="m2"><p>شرم بادش که شرم و عار نداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باره عمر تو بجست ایراک</p></div>
<div class="m2"><p>چون که در تک شد او قرار نداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون بناگوش تو عذار ندید</p></div>
<div class="m2"><p>کو ز مشک سیه عذار نداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدنیارست کرد با تو فلک</p></div>
<div class="m2"><p>تا مرا اندرین حصار نداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تن من چون جدا شد از بر تو</p></div>
<div class="m2"><p>عاجز آمد که دستیار نداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلم از مرگت اعتبار گرفت</p></div>
<div class="m2"><p>که ازین محنت اعتبار نداشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هیچ روزی به شب نشد که مرا</p></div>
<div class="m2"><p>نامه تو در انتظار نداشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گوشم اول که این خبر بشنود</p></div>
<div class="m2"><p>به روانت که استوار نداشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زار مسعود از آن همی گرید</p></div>
<div class="m2"><p>که به حق ماتم تو زار نداشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ماتم روزگار داشته ام</p></div>
<div class="m2"><p>که دگر چون تو روزگار نداشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>باره دولتت ز زین برمید</p></div>
<div class="m2"><p>بختی بخت تو مهار نداشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همچنین است عادت گردون</p></div>
<div class="m2"><p>هر چه من گفتمش به کار نداشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دل بدان خوش کنم که هیچ کسی</p></div>
<div class="m2"><p>در جهان عمر پایدار نداشت</p></div></div>