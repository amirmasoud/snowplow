---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>چون دانش بود مهربان دایه من</p></div>
<div class="m2"><p>از فخر و شرف زد همه پیرایه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مایه من بلند شد پایه من</p></div>
<div class="m2"><p>من دریا ام کم نشود مایه من</p></div></div>