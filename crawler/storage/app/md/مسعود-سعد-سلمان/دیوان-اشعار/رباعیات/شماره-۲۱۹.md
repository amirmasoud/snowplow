---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>من در عدم از جود تو موجود شدم</p></div>
<div class="m2"><p>در دولت تو بر سر مقصود شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسعود نبودم از تو مسعود شدم</p></div>
<div class="m2"><p>در حبس چنان شدم که محسود شدم</p></div></div>