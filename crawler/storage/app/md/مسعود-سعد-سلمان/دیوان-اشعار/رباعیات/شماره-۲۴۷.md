---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>کوهی که بر او بلا ببارند منم</p></div>
<div class="m2"><p>تیغی که به دست غم سپارند منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیری که برون نمی گذارند منم</p></div>
<div class="m2"><p>خواری که نکو نگاه دارند منم</p></div></div>