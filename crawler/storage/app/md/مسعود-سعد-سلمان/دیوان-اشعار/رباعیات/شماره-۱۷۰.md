---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>پیوست فلک با من پیکار دگر</p></div>
<div class="m2"><p>از یک غارم کشید در غار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بر طاعت ز خلق در کار دگر</p></div>
<div class="m2"><p>بنمای مرا جهان به یک بار دگر</p></div></div>