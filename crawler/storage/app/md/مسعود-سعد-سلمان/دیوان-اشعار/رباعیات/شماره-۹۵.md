---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>چون بند تو بنده را همی پند بود</p></div>
<div class="m2"><p>دربند تو بنده تو خرسند بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن پایش چه در خور بند بود</p></div>
<div class="m2"><p>ور نیز بود غایت آن چند بود</p></div></div>