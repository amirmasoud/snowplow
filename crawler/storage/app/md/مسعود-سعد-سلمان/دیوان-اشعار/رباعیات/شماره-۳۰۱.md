---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>چنگم به چهار شاخ زد پیراهن</p></div>
<div class="m2"><p>چنگست مگر چهار شاخ از آهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در اشک چهار شاخ آن شاخ سمن</p></div>
<div class="m2"><p>شد باز چهار شاخ کفته رخ من</p></div></div>