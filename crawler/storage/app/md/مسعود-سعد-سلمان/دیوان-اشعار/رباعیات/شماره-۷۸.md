---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>با من چو زمانه تیر در شست گرفت</p></div>
<div class="m2"><p>از بالا بخت من ره پست گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غفلت چون فلک مرا مست گرفت</p></div>
<div class="m2"><p>جای ملک الموت مرا دست گرفت</p></div></div>