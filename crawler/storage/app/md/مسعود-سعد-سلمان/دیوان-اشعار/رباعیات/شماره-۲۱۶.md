---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>در دولت شاه چون قوی شد رایم</p></div>
<div class="m2"><p>گفتم که رکاب را ز زر فرمایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر گفت مرا که من تو را کی شایم</p></div>
<div class="m2"><p>آمد آهن گرفت هر دو پایم</p></div></div>