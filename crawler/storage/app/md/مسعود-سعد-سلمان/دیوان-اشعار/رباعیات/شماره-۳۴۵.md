---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>خوردم همه زهر عشق تو شکر کو</p></div>
<div class="m2"><p>دیدم بتر هوای تو بهتر کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شاخ هوای تو نرفتم بر کو</p></div>
<div class="m2"><p>در تاریکی سکندرم گوهر کو</p></div></div>