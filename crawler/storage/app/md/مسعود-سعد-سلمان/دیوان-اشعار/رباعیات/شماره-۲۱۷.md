---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>غمهای تو از راندن خونها کارم</p></div>
<div class="m2"><p>خود نیست چرا راندن خونها کارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده من از مرگ تو خونها دارم</p></div>
<div class="m2"><p>بر مرگ تو با به مرگ خونها بارم</p></div></div>