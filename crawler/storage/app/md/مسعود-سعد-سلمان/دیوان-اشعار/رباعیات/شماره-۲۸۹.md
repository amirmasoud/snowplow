---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>من بستر برف و بالش یخ دارم</p></div>
<div class="m2"><p>خاکستر و یخ پیشگه و بخ دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون زاغ همه نشست بر شخ دارم</p></div>
<div class="m2"><p>در یکدو گز آب ریزو مطبخ دارم</p></div></div>