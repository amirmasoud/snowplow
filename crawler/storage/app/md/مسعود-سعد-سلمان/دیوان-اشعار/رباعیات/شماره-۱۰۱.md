---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>کارم همه جز مهر تو دلجوی نبود</p></div>
<div class="m2"><p>واندر دل من ز مهر تو بوی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در خور میدان توام گور نبود</p></div>
<div class="m2"><p>جز جستن من ز پیش روی تو نبود</p></div></div>