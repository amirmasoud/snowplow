---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>آن بت که دل مرا فرا چنگ آورد</p></div>
<div class="m2"><p>شد مست و به سوی رفتن آهنگ آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم مستی مرو سیه جنگ آورد</p></div>
<div class="m2"><p>چون گل بدرید جامه و رنگ آورد</p></div></div>