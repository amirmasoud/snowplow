---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>سلطان ملک اقبال عنان داد به تو</p></div>
<div class="m2"><p>درهای نشاط شاه بگشاد به تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته ست زمانه نیک دلشاد به تو</p></div>
<div class="m2"><p>تا حشر زمانه همچنین باد به تو</p></div></div>