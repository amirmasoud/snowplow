---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>تا کی غم یار و درد فرزند کشم</p></div>
<div class="m2"><p>بیمار فراق خویش و پیوند کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چشم گشاده ام همی بند کشم</p></div>
<div class="m2"><p>ای چرخ فلک محنت تو چند کشم</p></div></div>