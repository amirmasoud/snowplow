---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>جستم از توبه بی زبانی جستم</p></div>
<div class="m2"><p>جستم ز غمت چو خیزرانی جستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پیش فراق تو به جانی جستم</p></div>
<div class="m2"><p>الحق ز تو چون برایگانی جستم</p></div></div>