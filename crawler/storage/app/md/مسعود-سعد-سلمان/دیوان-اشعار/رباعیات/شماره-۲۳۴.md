---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>سروی خواهم ز چرخ داری زندم</p></div>
<div class="m2"><p>گر گویم کاین مراست آری زندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهم که گلی چینم خاری زندم</p></div>
<div class="m2"><p>از آهن مار کرده باری زندم</p></div></div>