---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>ای دشمن و دوست مر تو را یک عالم</p></div>
<div class="m2"><p>خاری و گلی با من و با یک عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بسته به تو مهر و وفا یک عالم</p></div>
<div class="m2"><p>مانده ز تو در خوف و رجا یک عالم</p></div></div>