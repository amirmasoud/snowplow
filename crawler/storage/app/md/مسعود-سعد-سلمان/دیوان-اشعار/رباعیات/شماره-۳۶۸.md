---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>ای گل نه ز گل ز دل همی بر رویی</p></div>
<div class="m2"><p>دل را ز همه غمان فرو می شویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل تو عقیق رنگ و مشکین مویی</p></div>
<div class="m2"><p>بر آب روان زیاده استی گویی</p></div></div>