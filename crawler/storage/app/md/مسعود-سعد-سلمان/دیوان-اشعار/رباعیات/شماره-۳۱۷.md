---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>انده چه خورم چراست اندر خوردن</p></div>
<div class="m2"><p>گر هست ز کرباس مرا پیراهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز نیش خسک دارم در زندان من</p></div>
<div class="m2"><p>پوشیده به بهرمان همه جامه و تن</p></div></div>