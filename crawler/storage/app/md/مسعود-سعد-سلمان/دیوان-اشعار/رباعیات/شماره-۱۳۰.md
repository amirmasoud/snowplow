---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>خاک از رخم ار برو نهم زرد شود</p></div>
<div class="m2"><p>آتش ز دمم گر بدمم سرد شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز من اگر ز مرگ پر گرد شود</p></div>
<div class="m2"><p>والله که جهان فضل بی مرد شود</p></div></div>