---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>در تاریکی ز بس که می بنشینم</p></div>
<div class="m2"><p>در روز چو شب پرک همی بد بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد چو شب ار خوابگهی بگزینم</p></div>
<div class="m2"><p>از پهلو و دست بستر و بالینم</p></div></div>