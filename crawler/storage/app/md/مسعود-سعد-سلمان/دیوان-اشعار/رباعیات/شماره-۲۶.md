---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>تن در غم هجر داده بودم همه شب</p></div>
<div class="m2"><p>و از انده تو فتاده بودم همه شب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بر زانو نهاده بودم همه شب</p></div>
<div class="m2"><p>گویی که ز سنگ زاده بودم همه شب</p></div></div>