---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>خویش از پی من همی گریزد ملکا</p></div>
<div class="m2"><p>دشمن بر من همی ستیزد ملکا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آتش من شرر نخیزد ملکا</p></div>
<div class="m2"><p>از حبس چو من کسی چه خیزد ملکا</p></div></div>