---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>اشکم که زمین از نم او آغشتست</p></div>
<div class="m2"><p>دریست غواص فراوان گشتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته چنانکه گویی اندر شستست</p></div>
<div class="m2"><p>ریزان گویی ز رشته بیرون گشتست</p></div></div>