---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>از هر چه بگفته اند پندی دارم</p></div>
<div class="m2"><p>وز هر چه بگفته ام گزندی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه بر گردن چو سگ کلندی دارم</p></div>
<div class="m2"><p>بر پای گهی چو پیل بندی دارم</p></div></div>