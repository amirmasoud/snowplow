---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>از دل بدم آتشی برانگیخته‌ام</p></div>
<div class="m2"><p>وز دیده به جای آب خون ریخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشق تو جان و دل درآمیخته‌ام</p></div>
<div class="m2"><p>نتوان جستن که محکم آویخته‌ام</p></div></div>