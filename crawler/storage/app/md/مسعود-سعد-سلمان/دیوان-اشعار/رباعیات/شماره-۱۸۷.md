---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>عشقت کشتم که غم درودم شب و روز</p></div>
<div class="m2"><p>جان کاستم و رنج فزودم شب و روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را به هوا بیازمودم شب و روز</p></div>
<div class="m2"><p>بی دل بودم که بی تو بودم شب و روز</p></div></div>