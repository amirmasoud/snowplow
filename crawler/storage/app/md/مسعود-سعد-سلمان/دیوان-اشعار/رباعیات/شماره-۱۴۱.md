---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>ترسم ما را ستارگان چشم کنند</p></div>
<div class="m2"><p>تا زود رسد ز دور در وصل گزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی تو که روز ناید ای سرو بلند</p></div>
<div class="m2"><p>زلف سیه دراز در شب پیوند</p></div></div>