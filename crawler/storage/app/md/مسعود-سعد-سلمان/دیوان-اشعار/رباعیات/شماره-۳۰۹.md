---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>از کفر کشد زریر شیبانی کین</p></div>
<div class="m2"><p>آباد کند زریر شیبانی دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چرخ نهد زریر شیبانی زین</p></div>
<div class="m2"><p>این مرتبت زریر شیبانی بین</p></div></div>