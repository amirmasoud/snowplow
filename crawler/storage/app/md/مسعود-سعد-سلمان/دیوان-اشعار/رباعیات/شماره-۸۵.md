---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>آرام ز خویشتن جدا خواهم کرد</p></div>
<div class="m2"><p>جان از قبل تو در فنا خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو پنداری تو را رها خواهم کرد</p></div>
<div class="m2"><p>تا جان دارم تو را وفا خواهم کرد</p></div></div>