---
title: >-
    شمارهٔ ۳۲۷
---
# شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>امروز منم تفته دل و رفته روان</p></div>
<div class="m2"><p>تلخم شده زندگانی اندر زندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآنچ انده کرد مر مرا بر دل و جان</p></div>
<div class="m2"><p>بر شیران کرد ضرب سلطان جهان</p></div></div>