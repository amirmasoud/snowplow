---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>عشوه دهیم همی سرابی گویی</p></div>
<div class="m2"><p>بر من گذری همی شهابی گویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریان شوم از تو آفتابی گویی</p></div>
<div class="m2"><p>نتوانم بی تو زیست آبی گویی</p></div></div>