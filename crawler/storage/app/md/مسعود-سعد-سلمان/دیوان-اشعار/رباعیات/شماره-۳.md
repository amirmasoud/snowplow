---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گر بند کند رای بلند تو مرا</p></div>
<div class="m2"><p>در جمله پسنده است پسند تو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تهذیب تمام کرد پند تو مرا</p></div>
<div class="m2"><p>تاج سر فخر گشت بند تو مرا</p></div></div>