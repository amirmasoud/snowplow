---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بر عارض نو مشک همی افزاید</p></div>
<div class="m2"><p>وآن روی چو ماه تو همی آراید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مشک ز عارض تو زاید شاید</p></div>
<div class="m2"><p>تو آهویی و مشک ز آهو زاید</p></div></div>