---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>گویی که هوا به زیر گردست امروز</p></div>
<div class="m2"><p>با سرما خلق را نبردست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست من و پای من به دردست امروز</p></div>
<div class="m2"><p>بفروز آتش که سخت سردست امروز</p></div></div>