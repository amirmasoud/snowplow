---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>از حصن بلند دوزخ سرد مراست</p></div>
<div class="m2"><p>با خون دو دیده چهره زرد مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد یار عزیز ناجوانمرد مراست</p></div>
<div class="m2"><p>کس را چه غمست کاین همه درد مراست</p></div></div>