---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>روی و بر من تا بشدم از بر تو</p></div>
<div class="m2"><p>زردست و کبودست به جان و سر تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیرا که در آرزوی روی و بر تو</p></div>
<div class="m2"><p>این پیرهن تو گشت و آن معجر تو</p></div></div>