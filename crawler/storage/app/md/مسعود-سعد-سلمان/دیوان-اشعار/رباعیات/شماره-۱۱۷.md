---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ای شاه جهان جهان شد از داد تو شاد</p></div>
<div class="m2"><p>تو داد جهان ده که جهان داد تو داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شاه پسندیده جهان ملک تو باد</p></div>
<div class="m2"><p>سقای تو ابر باد و فراش تو باد</p></div></div>