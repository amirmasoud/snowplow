---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>تا خسته دل مرا بریده ست ز تن</p></div>
<div class="m2"><p>دارم گله هاش را چو شمشیر سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن چکنم گفت نمی یارم من</p></div>
<div class="m2"><p>کان پسته دهن کرد مرا بسته دهن</p></div></div>