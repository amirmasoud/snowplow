---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>آنی که زمان زمان مرا عشق تو بوست</p></div>
<div class="m2"><p>بی روی نکوی تو نکویی نه نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشرت و در نشاط امروز ای دوست</p></div>
<div class="m2"><p>بیرون آیی همی چو بادام از پوست</p></div></div>