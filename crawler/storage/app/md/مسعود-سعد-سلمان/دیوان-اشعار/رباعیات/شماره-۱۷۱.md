---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>این ابر چراست روز وشب چشم تو تر</p></div>
<div class="m2"><p>وی فاخته زار چند نالی به سحر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای لاله چرا جامه دریدی در بر</p></div>
<div class="m2"><p>از یار جدایید چو مسعود مگر</p></div></div>