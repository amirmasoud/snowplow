---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>آخر نگذاردم فلک چون زاری</p></div>
<div class="m2"><p>آخر بجهد فضل مرا بازاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر بد ماندم جهان گلزاری</p></div>
<div class="m2"><p>عذری خواهد ز من بهر آزاری</p></div></div>