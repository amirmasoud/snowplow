---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>ای روی تو آفتاب و من نیلوفر</p></div>
<div class="m2"><p>چون نیلوفر در آبم از دیده تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو نتابی چو آفتاب ای دلبر</p></div>
<div class="m2"><p>نگشایم دیدگان و برنارم سر</p></div></div>