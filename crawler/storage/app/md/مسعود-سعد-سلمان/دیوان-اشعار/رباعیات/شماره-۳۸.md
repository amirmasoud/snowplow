---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>بر روی دو زلفین بتابم زد دوست</p></div>
<div class="m2"><p>ز آن زلف به عنبر و گلابم زد دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش افروخته آبم زد دوست</p></div>
<div class="m2"><p>بشتافت و بوسه با شتابم زد دوست</p></div></div>