---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>با کس غم تو بیش نخواهم گفتم</p></div>
<div class="m2"><p>وین در دو دیده هم نخواهم سفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر تو ز دل پاک بخواهم رفتن</p></div>
<div class="m2"><p>بر بستر صبر خوش بخواهم خفتن</p></div></div>