---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>ز اندیشه هجران و ز نادیدن یار</p></div>
<div class="m2"><p>دل خون شد و دیده خون همی گرید زار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویم ز غم فراق روزی صد بار</p></div>
<div class="m2"><p>کاین عشق چه آفت است یارب زنهار</p></div></div>