---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>این طالع من یارب و این اختر چیست</p></div>
<div class="m2"><p>کاین دل ز بلای دهر همواره غمیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من زو نرهم یقینم و غمگین کیست</p></div>
<div class="m2"><p>آن کس که بر این طالع من خواهد زیست</p></div></div>