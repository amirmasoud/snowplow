---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>من دوش که از هجر تو در تاب شدم</p></div>
<div class="m2"><p>جان تو که گر چو شمع در خواب شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده و دل در آتش و آب شدم</p></div>
<div class="m2"><p>بر جام چو بر آینه سیماب شدم</p></div></div>