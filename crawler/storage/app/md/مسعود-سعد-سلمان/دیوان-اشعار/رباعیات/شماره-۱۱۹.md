---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>تا چرخ مرا به چنگ عشق تو سپرد</p></div>
<div class="m2"><p>شمع طربم ز باد اندوه بمرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گردن رامش مرا کوفته خورد</p></div>
<div class="m2"><p>در حسرت تو عمر به سر خواهم برد</p></div></div>