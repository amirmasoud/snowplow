---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>گفتم که تو بی وفایی ای نامردم</p></div>
<div class="m2"><p>من مردم و تو کجایی ای نامردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خس دوست چو کهربایی ای نامردم</p></div>
<div class="m2"><p>زان با چو منی نپایی ای نامردم</p></div></div>