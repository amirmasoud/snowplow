---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>عشق تو بلند و صبر من پست چرا</p></div>
<div class="m2"><p>روی تو نکو و خوی تو کست چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خواره منم دو چشم تو مست چرا</p></div>
<div class="m2"><p>پیش تو لبم بوس تو بر دست چرا</p></div></div>