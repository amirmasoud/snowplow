---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>مهمان من آمد آن بت و کرد طرب</p></div>
<div class="m2"><p>شوخی که در او همی بماندم به عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نرگس و گل نبست نه روز نه شب</p></div>
<div class="m2"><p>از نظاره دو چشم و از خنده دو لب</p></div></div>