---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>در هند کمال جود موجود آمد</p></div>
<div class="m2"><p>صد کوکبه شجاعت و جود آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چرخ ستاره ای که مسعود آمد</p></div>
<div class="m2"><p>در طالع شیرزاد مسعود آمد</p></div></div>