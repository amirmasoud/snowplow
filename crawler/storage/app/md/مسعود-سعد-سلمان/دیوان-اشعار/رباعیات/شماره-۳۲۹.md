---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>مشک از سر زلفین تو بویم پس ازین</p></div>
<div class="m2"><p>گرد در تو بدیده پویم پس از این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته رضای تو بجویم پس ازین</p></div>
<div class="m2"><p>جز با تو حدیث کس نگویم پس ازین</p></div></div>