---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>هر چند که بر کوهم در شب ز اندوه</p></div>
<div class="m2"><p>گریان باشم تا به گه بانگ خروه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همقامت تو چو سرو بینم بر کوه</p></div>
<div class="m2"><p>هرگز نشوم ز دیدن کوه ستوه</p></div></div>