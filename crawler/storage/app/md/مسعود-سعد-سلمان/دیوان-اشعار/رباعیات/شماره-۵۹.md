---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>در ماه چه روشنی که در روی تو نیست</p></div>
<div class="m2"><p>ور خلد چه خرمی که در کوی تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشک ختنی چو زلف خوشبوی تو نیست</p></div>
<div class="m2"><p>یکسر هنری عیب تو جز خوی تو نیست</p></div></div>