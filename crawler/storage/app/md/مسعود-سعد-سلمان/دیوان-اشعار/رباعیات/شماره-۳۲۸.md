---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>بگشای چو گل به وعده راست دهن</p></div>
<div class="m2"><p>ور نه ز تو چون لاله کنم پیراهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دعوی دلست با توام بند مزن</p></div>
<div class="m2"><p>وآنک در حکم عشق و اینک تو و من</p></div></div>