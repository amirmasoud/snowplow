---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>ای نای ندیده ام دلی شاد از تو</p></div>
<div class="m2"><p>نایی تو ولیکن نرهد باد از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز ناله مرا چو نای نگشاد از تو</p></div>
<div class="m2"><p>ای نای مرا چو نای فریاد از تو</p></div></div>