---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>سر کردمت ای نگار چون تو سر من</p></div>
<div class="m2"><p>گه گه به سخن چرب کنی بی روغن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین نیست عجب ای صنم پسته دهن</p></div>
<div class="m2"><p>گر پسته دهن بود همه چرب سخن</p></div></div>