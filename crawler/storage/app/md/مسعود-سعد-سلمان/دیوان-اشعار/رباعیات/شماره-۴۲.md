---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>در نعمت و مال اگر زبر دستی نیست</p></div>
<div class="m2"><p>شکر ایزد را که رای را پستی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبسته آز نیست گر هستی نیست</p></div>
<div class="m2"><p>زر مست کند چه باشد از مستی نیست</p></div></div>