---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>گر خسته شوم ز تیر پیکار تو من</p></div>
<div class="m2"><p>آهی نکنم ز بیم آزار تو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بیم سر غمزه چون خار تو من</p></div>
<div class="m2"><p>خندان میرم چو گل به دیدار تو من</p></div></div>