---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>عمری بدو کف دو رخ نگارا خستم</p></div>
<div class="m2"><p>نومیدی جان به درد دل در بستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون ز نشاط وصل تو برجستم</p></div>
<div class="m2"><p>از پای درافتم ار نگیری دستم</p></div></div>