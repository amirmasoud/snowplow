---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>تا دعوت دولت تو در گوشم شد</p></div>
<div class="m2"><p>هر زهر که داد بخت بدنوشم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روز که گفته تو در گوشم شد</p></div>
<div class="m2"><p>از نغمت پاک خود فراموشم شد</p></div></div>