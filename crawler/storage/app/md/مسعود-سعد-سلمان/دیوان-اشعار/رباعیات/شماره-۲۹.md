---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>صالح تر و خشک شد ز تو دیده و لب</p></div>
<div class="m2"><p>چه بد روزم چه شور بختم یارب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با درد هزار بار کوشم همه شب</p></div>
<div class="m2"><p>تو مردی و من بزیستم اینت عجب</p></div></div>