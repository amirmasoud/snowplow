---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>چون روی هوا دوش به قیر اندودند</p></div>
<div class="m2"><p>تا روز همه تپان و لرزان بودند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تارک من ستارگان نغنودند</p></div>
<div class="m2"><p>گویی که همه بر تن من بخشودند</p></div></div>