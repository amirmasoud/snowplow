---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>در بند تو ای شاه ملکشه باید</p></div>
<div class="m2"><p>تا بند تو پای تاجداری ساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که ز پشت سعد سلمان آید</p></div>
<div class="m2"><p>گر زهر شود ملک تو را نگزاید</p></div></div>