---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>این چرخ بسی بدل کند نوها را</p></div>
<div class="m2"><p>بدخوست از آن بدل کند خوها را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم زشت کند به طبع نیکوها را</p></div>
<div class="m2"><p>هم ضعف دهد به قهر نیروها را</p></div></div>