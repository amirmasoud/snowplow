---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>چون موج سیاه روی هامون گیرد</p></div>
<div class="m2"><p>از خنجر تو روی زمین خون گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس شیر نگر که شیر پرخون گیرد</p></div>
<div class="m2"><p>شیر علم تو شیر گردون گیرد</p></div></div>