---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>خوی تو چو رخسار نکوی تو نکوست</p></div>
<div class="m2"><p>بی روی نکوی تو نکویی نه نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نار همی پاره کنم بر تن پوست</p></div>
<div class="m2"><p>از انده هجران تو ای دلبر دوست</p></div></div>