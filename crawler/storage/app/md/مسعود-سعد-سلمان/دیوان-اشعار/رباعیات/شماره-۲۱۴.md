---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>خامش نشود همی ز غلغل بلبل</p></div>
<div class="m2"><p>بشنو که خوش آیدت ز بلبل غلغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دو لب تو گل و دو رخسار تو گل</p></div>
<div class="m2"><p>مل ده بر گل که خوش بود بر گل مل</p></div></div>