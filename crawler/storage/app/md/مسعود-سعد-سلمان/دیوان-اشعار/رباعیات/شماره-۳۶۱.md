---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>ای غم سختی تو ای دل از غم نرمی</p></div>
<div class="m2"><p>ای دم سردی تو ای دل از دم گرمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عشق خمش باش که بس بی شرمی</p></div>
<div class="m2"><p>ای هجر برو که سخت بی آزرمی</p></div></div>