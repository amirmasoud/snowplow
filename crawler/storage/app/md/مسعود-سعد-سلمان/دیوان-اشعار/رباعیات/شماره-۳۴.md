---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>اول ز پی وصال روح افزایت</p></div>
<div class="m2"><p>بگرفته بدم پای بلور آسایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که خبر شنیدم از هر جایت</p></div>
<div class="m2"><p>گردست رسد مرا ببوسم پایت</p></div></div>