---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>شاها ملکا جهان به فرمان تو باد</p></div>
<div class="m2"><p>ملک تو شکفته باغ و بستان تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشیر تو در دست تو برهان تو باد</p></div>
<div class="m2"><p>رحمت همه بر دل و تن و جان تو باد</p></div></div>