---
title: >-
    شمارهٔ ۳۹۱
---
# شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>از غنچه ناشکفته مستورتری</p></div>
<div class="m2"><p>وز نرگس نیم خفته مخمورتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خوبی از آفتاب مشهورتری</p></div>
<div class="m2"><p>ای مه ز مه دو هفته پرنورتری</p></div></div>