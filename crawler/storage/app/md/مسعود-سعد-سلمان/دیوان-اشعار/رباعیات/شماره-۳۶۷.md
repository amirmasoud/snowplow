---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>با هر تاری ساخته چون پود شوی</p></div>
<div class="m2"><p>با جمله همه زیان بی سود شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیده عهد دوست چون دود شوی</p></div>
<div class="m2"><p>زینگونه به کام دشمنان زود شوی</p></div></div>