---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>ای نای تو را نقل و می روشن کو</p></div>
<div class="m2"><p>با تو طرب طبع و نشاط تن کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو نایی لحن خوشت با من کو</p></div>
<div class="m2"><p>چون نای تو را دریچه و روزن کو</p></div></div>