---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>دانی تو که با بند گرانم یارب</p></div>
<div class="m2"><p>دانی که ضعیف و ناتوانم یارب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد در غم لوهور روانم یارب</p></div>
<div class="m2"><p>یارب که در آرزوی آنم یارب</p></div></div>