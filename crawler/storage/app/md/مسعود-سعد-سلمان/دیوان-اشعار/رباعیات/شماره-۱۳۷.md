---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>رویی که چو او چرخ فلک ننگارد</p></div>
<div class="m2"><p>قدی که چو او زمانه بیرون نارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه داد سخت اندک دارد</p></div>
<div class="m2"><p>خوی گردد اگر چشم برین بگذارد</p></div></div>