---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>چون آتش و آب از بدی پاکم و ناب</p></div>
<div class="m2"><p>چون آب صفا دارم و چون آتش تاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتش و آبم کند ار چرخ عذاب</p></div>
<div class="m2"><p>بیرون آیم چو زر و در زآتش و آب</p></div></div>