---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>چون قمری زار زار می نالم من</p></div>
<div class="m2"><p>چون بلبل آلوده به خون پیراهن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون طوطی بر وصف تو بگشاده دهن</p></div>
<div class="m2"><p>چون فاخته طوق عشقت اندر گردن</p></div></div>