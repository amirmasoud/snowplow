---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>هر گه که تو را به رهگذاری بینم</p></div>
<div class="m2"><p>از سایه ت بر زمین نگاری بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رشگ دلم چو کفته ناری بینم</p></div>
<div class="m2"><p>گر با تو جز از سایه ت یاری بینم</p></div></div>