---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>گوشم ز تو نشنود بتا جز همه سرد</p></div>
<div class="m2"><p>دل بهره نیافت از تو جز محنت و درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه اندوه نمی باید خورد</p></div>
<div class="m2"><p>چو خورد و چه پوشید کجا رفت و چه کرد</p></div></div>