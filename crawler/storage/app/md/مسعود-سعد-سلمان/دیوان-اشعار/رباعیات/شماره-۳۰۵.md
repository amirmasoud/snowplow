---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>این دیبه دو روی به کلک دو زبان</p></div>
<div class="m2"><p>پرداخته شد به قوت خاطر و جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستانش به نام ایزد ای باد وزان</p></div>
<div class="m2"><p>لوهور به نزد خواجه بونصر رسان</p></div></div>