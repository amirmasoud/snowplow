---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>روزان و شبان در آن غم و تیمارم</p></div>
<div class="m2"><p>کاسرار تو را چگونه پنهان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خون شد و خون ز دیدگان می بارم</p></div>
<div class="m2"><p>بینند ز خون دل همه اسرارم</p></div></div>