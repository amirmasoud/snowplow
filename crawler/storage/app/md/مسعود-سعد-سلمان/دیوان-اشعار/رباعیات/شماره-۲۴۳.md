---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>وصف لب رنگین تو از دل جویم</p></div>
<div class="m2"><p>در آرزوی زلف تو سنبل بویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پر خون شد ز دیده چون گل رویم</p></div>
<div class="m2"><p>وصف تو همه روز به بلبل گویم</p></div></div>