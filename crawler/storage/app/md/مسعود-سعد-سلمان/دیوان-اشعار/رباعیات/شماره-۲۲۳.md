---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>گر من برم از مردم بد سازم برم</p></div>
<div class="m2"><p>فرجام ببینم و به آغاز برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که به من دژم دژم پیوندد</p></div>
<div class="m2"><p>بنگر که چه پاره پاره زو باز برم</p></div></div>