---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>ای حورا زاده لعبت نوشادی</p></div>
<div class="m2"><p>از باغ بهشت کی برون افتادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندیش که پیرایه به تن بنهادی</p></div>
<div class="m2"><p>ای حسن تو پیرایه مادرزادی</p></div></div>