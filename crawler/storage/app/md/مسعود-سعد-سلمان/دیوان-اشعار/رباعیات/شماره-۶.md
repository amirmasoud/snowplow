---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>روزی بر من همی نیایی صنما</p></div>
<div class="m2"><p>چون آیی یک زمان نپایی صنما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر تومرا وفا نمایی صنما</p></div>
<div class="m2"><p>چون نیک مرا بیازمایی صنما</p></div></div>