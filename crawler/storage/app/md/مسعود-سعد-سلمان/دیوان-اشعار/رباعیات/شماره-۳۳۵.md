---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>هر جان که بود برتر از آن باشی تو</p></div>
<div class="m2"><p>بخریده امت به جان گران باشی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جای مرا به جای جان باشی تو</p></div>
<div class="m2"><p>ای دوست به جان نه رایگان باشی تو</p></div></div>