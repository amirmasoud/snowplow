---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>در حبس مرنج با چنین آهن‌ها</p></div>
<div class="m2"><p>صالح بی‌تو چگونه باشم تنها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه خون گریم به مرگ تو دامن‌ها</p></div>
<div class="m2"><p>گه پاره کنم ز درد پیراهن‌ها</p></div></div>