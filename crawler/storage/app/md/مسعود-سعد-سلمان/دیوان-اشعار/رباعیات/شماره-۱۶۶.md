---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>آمد به وداعم آن نگار دلبر</p></div>
<div class="m2"><p>گریان و زنان دو دست بر یکدیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر خون رخش از زخم و رخ از گریه چو زر</p></div>
<div class="m2"><p>بر لاله کامگار و بر لؤلؤی تر</p></div></div>