---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ای زرین نام لعبت سیم اندام</p></div>
<div class="m2"><p>زر تو و سیم تو نه پخته ست و نه خام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کس منگر به بی نیازی بخرام</p></div>
<div class="m2"><p>زیرا که توانگری به اندام و به نام</p></div></div>