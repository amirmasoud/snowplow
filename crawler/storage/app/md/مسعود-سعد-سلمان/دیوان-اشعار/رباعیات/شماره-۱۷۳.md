---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>مشکین کله تو گر شبست ای دلدار</p></div>
<div class="m2"><p>خورشید در او چرا گرفته ست قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیره ست در آن کله خرد را دیدار</p></div>
<div class="m2"><p>دیدار بلی خیره بود در شب تار</p></div></div>