---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>در محنت شو خوش و مکن نعمت یاد</p></div>
<div class="m2"><p>شو در ده تن که داد کس چرخ نداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بار بلایی که قضا بر تو نهاد</p></div>
<div class="m2"><p>تن دار چو کوه باش و بی باک چو باد</p></div></div>