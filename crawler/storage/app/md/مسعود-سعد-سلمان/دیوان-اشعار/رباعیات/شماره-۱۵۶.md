---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>دیدار تو از نعمت دو جهان خوشتر</p></div>
<div class="m2"><p>وز عمر وصال تو فراوان خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من عشق تو ای عشق تو از جان خوشتر</p></div>
<div class="m2"><p>پنهان دارم که عشق پنهان خوشتر</p></div></div>