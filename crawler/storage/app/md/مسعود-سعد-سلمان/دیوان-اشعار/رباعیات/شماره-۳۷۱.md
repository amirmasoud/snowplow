---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>شوخی صنمی خوشی کشی خندانی</p></div>
<div class="m2"><p>طوطی سخنی و عندلیب الحانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برده دلم به لابه و دستانی</p></div>
<div class="m2"><p>لابد پس دل روم چو سرگردانی</p></div></div>