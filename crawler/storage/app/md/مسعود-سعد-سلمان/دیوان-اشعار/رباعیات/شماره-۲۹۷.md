---
title: >-
    شمارهٔ ۲۹۷
---
# شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>در سمجی چون توانم آرامیدن</p></div>
<div class="m2"><p>کز تنگی آن نمی توان خسبیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب که همی به چشم خواهم دیدن</p></div>
<div class="m2"><p>جایی که در او فراخ بتوان دیدن</p></div></div>