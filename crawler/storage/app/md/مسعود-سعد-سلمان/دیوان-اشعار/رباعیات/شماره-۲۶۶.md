---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>شب ز انده تو همی نیاید خوابم</p></div>
<div class="m2"><p>بر جامه ز غم چو گوی در طبطابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من گاه در آتش و گه اندر آبم</p></div>
<div class="m2"><p>سنگم که به من هر چه رسد در یابم</p></div></div>