---
title: >-
    شمارهٔ ۲۲۷
---
# شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>جان هر ساعت ز کار زاری دهدم</p></div>
<div class="m2"><p>هر روز زمانه بیش کاری دهدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بخت گلی خواهم و خاری دهدم</p></div>
<div class="m2"><p>باشد روزی که روزگاری دهدم</p></div></div>