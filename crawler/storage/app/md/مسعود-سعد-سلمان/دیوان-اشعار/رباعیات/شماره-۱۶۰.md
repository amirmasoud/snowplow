---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>خورشید رخ تو تافت بر سایه عمر</p></div>
<div class="m2"><p>آمد به کفم گمشده پیرایه عمر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای اول وصلت آخرین مایه عمر</p></div>
<div class="m2"><p>در جستن سود وصل شد مایه عمر</p></div></div>