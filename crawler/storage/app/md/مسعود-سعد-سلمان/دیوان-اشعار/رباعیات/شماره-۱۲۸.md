---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>آن کوه گذار آهوی دشت نورد</p></div>
<div class="m2"><p>اندر تگ گرم شد به تگ بهر تو سرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیری که همیشه جگر شیران خورد</p></div>
<div class="m2"><p>آلوده به آهویی چرا باید کرد</p></div></div>