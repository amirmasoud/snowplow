---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>با خود گفتم که من عیال تو شدم</p></div>
<div class="m2"><p>او گفت که من ضامن مال تو شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آن که ثناگوی کمال تو شدم</p></div>
<div class="m2"><p>بیشم نکنند چون نهال تو شدم</p></div></div>