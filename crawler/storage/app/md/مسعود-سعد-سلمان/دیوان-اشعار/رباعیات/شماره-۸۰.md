---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>در بأس چو طاهر علی آهن نیست</p></div>
<div class="m2"><p>بی منت طاهر علی گردن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز منت طاهر علی بر من نیست</p></div>
<div class="m2"><p>والله که چو طاهر علی یک تن نیست</p></div></div>