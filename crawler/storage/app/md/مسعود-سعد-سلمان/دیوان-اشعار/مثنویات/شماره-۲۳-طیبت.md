---
title: >-
    شمارهٔ ۲۳ - طیبت
---
# شمارهٔ ۲۳ - طیبت

<div class="b" id="bn1"><div class="m1"><p>طیبتی می کنم معاذالله</p></div>
<div class="m2"><p>از پی خرمی مجلس شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاعر آری چنین بود گستاخ</p></div>
<div class="m2"><p>که بگوید سخن به نظم فراخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون از آن مجلس بهشت آیین</p></div>
<div class="m2"><p>دورم افکند روزگار چنین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من دگر چاره ای ندانم کرد</p></div>
<div class="m2"><p>دل ازین نوع خوش توانم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا فلک را همی مدار بود</p></div>
<div class="m2"><p>خاک را اندرو قرار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولت شاه باد پاینده</p></div>
<div class="m2"><p>نعمتش هر زمان فزاینده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرکب جاه زیر رانش باد</p></div>
<div class="m2"><p>جان دشمن فدای جانش باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزگارش شده مسخر باد</p></div>
<div class="m2"><p>دولتش بنده باد و چاکر باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باد سلطان و پادشاه زمن</p></div>
<div class="m2"><p>از لقایش به دیدگان روشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا به دل در نشاط و شادی باشد</p></div>
<div class="m2"><p>دولت و ملک شیرزادی باد</p></div></div>