---
title: >-
    شمارهٔ ۲ - ثنای عضدالدوله شیرزاد
---
# شمارهٔ ۲ - ثنای عضدالدوله شیرزاد

<div class="b" id="bn1"><div class="m1"><p>گرچه خرم شده ست لوهاوور</p></div>
<div class="m2"><p>باشد آن کس که می خورد معذور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منظر شاه خلد را ماند</p></div>
<div class="m2"><p>که بر او ابر گوهر افشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل افروز مجلس عضدی</p></div>
<div class="m2"><p>از همه نوع نعمت ابدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه بر تخت و جام باده به دست</p></div>
<div class="m2"><p>روزگار از نشاط او سرمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عضدالدوله آنکه دولت حق</p></div>
<div class="m2"><p>دست او کرده بر جهان مطلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ ملت که ملت تازی</p></div>
<div class="m2"><p>کند از تیغ او سرافرازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیرزاد آنکه شیر در بیشه</p></div>
<div class="m2"><p>باشد از بیم او در اندیشه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به هندوستان بماند شیر</p></div>
<div class="m2"><p>او نگردد ز شیر کشتن سیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من غلط می کنم که کس به جهان</p></div>
<div class="m2"><p>ندهد نیز هیچ شیر نشان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خشت او بس که کرد شیران کم</p></div>
<div class="m2"><p>شیر گردون بماند و شیر علم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منقطع کرد نسل شیران را</p></div>
<div class="m2"><p>اعتباریست این دلیران را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه فرمانبرانش را مانند</p></div>
<div class="m2"><p>خدمتش را سزا و شایانند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیشه کردند بندگی کردن</p></div>
<div class="m2"><p>کس نپیچد ز امر او گردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ور بپیچد زود بیند سر</p></div>
<div class="m2"><p>چون سر شیر نر به کنگره بر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سخن جمله گفت خواهم من</p></div>
<div class="m2"><p>در بزرگی شاه نیست سخن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمانیست جاه او به مثل</p></div>
<div class="m2"><p>آفتابیست رای او به محل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خلق را قصه ایست آثارش</p></div>
<div class="m2"><p>هند را عبره ایست پیکارش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخشش او بلات کان گشتست</p></div>
<div class="m2"><p>سخن او غذای جان گشتست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جود را ملجا است همت او</p></div>
<div class="m2"><p>جاه را مرکزست حشمت او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حله پوش برهنه خنجر اوست</p></div>
<div class="m2"><p>گوهری کاب او ز آذر اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان ستانیست پاک همچون جان</p></div>
<div class="m2"><p>پیکر و حد او یقین و گمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مار زخمی که همچو مهره مار</p></div>
<div class="m2"><p>ملک را هست بی خلاف به کار</p></div></div>