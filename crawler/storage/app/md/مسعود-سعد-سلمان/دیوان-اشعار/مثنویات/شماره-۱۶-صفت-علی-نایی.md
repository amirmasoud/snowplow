---
title: >-
    شمارهٔ ۱۶ - صفت علی نایی
---
# شمارهٔ ۱۶ - صفت علی نایی

<div class="b" id="bn1"><div class="m1"><p>از دگر سو علی به نغمه نای</p></div>
<div class="m2"><p>دل برانگیزد ای شگفت ز جای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد از جنس جنس دمدمه ها</p></div>
<div class="m2"><p>آرد از نوع نوع زمزمه ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می زند نای و تنگ می جوشد</p></div>
<div class="m2"><p>به هوا روی عقل می پوشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دل خویشتن همی گوید</p></div>
<div class="m2"><p>که غم از جان من چه می جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق و رنج محمد نایی</p></div>
<div class="m2"><p>مر مرا گشت اینت رسوایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه زند آخر او را که من نزنم</p></div>
<div class="m2"><p>اگر او هست مرد من نه زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل چرا بیهده دژم دارم</p></div>
<div class="m2"><p>نه ز کس دستگاه کم دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به خانه چرا نه بنشینم</p></div>
<div class="m2"><p>توبه با صلاح بگزینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار بی ورز و بی وبال کنم</p></div>
<div class="m2"><p>کسب خویش از ره حلال کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که اگر سیم ها به سود دهم</p></div>
<div class="m2"><p>نعمتی زین طریق زود نهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باطن این گوید و به ظاهر باز</p></div>
<div class="m2"><p>صد تضرع فزون کند ز آغاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنکه در حکم او بود شب و روز</p></div>
<div class="m2"><p>برفشاند به روی گنبد گوز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آب بی روی وی نیارد خورد</p></div>
<div class="m2"><p>پیش او هیچ از این نیارد کرد</p></div></div>