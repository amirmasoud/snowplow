---
title: >-
    شمارهٔ ۱۸ - صفت کودک جعبه زن
---
# شمارهٔ ۱۸ - صفت کودک جعبه زن

<div class="b" id="bn1"><div class="m1"><p>جعبه کودک خویش دلکش</p></div>
<div class="m2"><p>راه اشکر همی سراید خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون فرو راند زخمه بر جعبه</p></div>
<div class="m2"><p>هر که بشنید گرددش سغبه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک زمانی سماع گرم کند</p></div>
<div class="m2"><p>دل سخت از نشاط نرم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس بگیرد دلش ز انبوهی</p></div>
<div class="m2"><p>فکند در میان دو کوهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیره با خویشتن همی گوید</p></div>
<div class="m2"><p>چون ببیند رهی فرو موید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر ببندد بهانه ها سازد</p></div>
<div class="m2"><p>سوی کردانه ناگهان تازد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیمکی کهنه بنهد اندر پیش</p></div>
<div class="m2"><p>شرم نایدش زآن دو گیسوی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کف آرد نبیند کاسی را</p></div>
<div class="m2"><p>بدهد او به دور طاسی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کار و باری چنین فرو سازد</p></div>
<div class="m2"><p>پیش معشوق جعبه بنوازد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او نشسته میان قلاشان</p></div>
<div class="m2"><p>که درآیند زود فراشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اول آشفته را برون آرند</p></div>
<div class="m2"><p>شکرش با گرفته خون آرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز گشته به روسپی خانه</p></div>
<div class="m2"><p>کرده خون را ز بیم دیوانه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عین عین کرده چشم را به دروغ</p></div>
<div class="m2"><p>راست مانند گاو جسته زیوغ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون بپیش شه اندر آرندش</p></div>
<div class="m2"><p>اندر آن پایگه بدارندش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی از آژنگ همچو طفطفه ای</p></div>
<div class="m2"><p>بر خود افکنده کرم هفهفه ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شه ترنجی زند به رویش بر</p></div>
<div class="m2"><p>کند از خون روی مویش تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون بدان زخم بشکند بینیش</p></div>
<div class="m2"><p>بوالعجب گشت صورتی بینیش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روی پر گرد و بینی اندر خون</p></div>
<div class="m2"><p>بر خزیده دو دیده ملعون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آبش از دیده آمدن گیرد</p></div>
<div class="m2"><p>جعبه برگیرد و زدن گیرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عذرها خواهدش سبک عثمان</p></div>
<div class="m2"><p>درد او را کند سبک درمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل او خوش کند به یاری لک</p></div>
<div class="m2"><p>تا شود نرم و راست گردد رگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بکنند این همه ندارد سود</p></div>
<div class="m2"><p>روز دیگر همان بخواهد بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشنود باز آنچه عادت اوست</p></div>
<div class="m2"><p>ار شود باز از آن سعادت اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنچه او را دهد به زودی شاه</p></div>
<div class="m2"><p>هیچ خاطر بدان نیابد راه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر چه از جود شه به کف کند او</p></div>
<div class="m2"><p>در خرابات ها تلف کند او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز کوریش هیچ کم نشود</p></div>
<div class="m2"><p>نشد او نیکبخت وهم نشود</p></div></div>