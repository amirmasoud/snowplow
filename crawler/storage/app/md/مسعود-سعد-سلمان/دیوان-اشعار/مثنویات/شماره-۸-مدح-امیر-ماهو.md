---
title: >-
    شمارهٔ ۸ - مدح امیر ماهو
---
# شمارهٔ ۸ - مدح امیر ماهو

<div class="b" id="bn1"><div class="m1"><p>ماهو آن سید ستوده خصال</p></div>
<div class="m2"><p>باشد آهسته طبع در همه حال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مایه دانش است پنداری</p></div>
<div class="m2"><p>هست مستی او چو هشیاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات دانا و طبع برنا نیست</p></div>
<div class="m2"><p>مثل او هیچ تیز و دانا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در همه کارها کند انجاح</p></div>
<div class="m2"><p>نبود مثل او به هزل و مزاح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه چو از حال او خبر دارد</p></div>
<div class="m2"><p>هر زمانش عزیز تر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنهد بد سگال را گردن</p></div>
<div class="m2"><p>گر چه خو دارد او فرو خوردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کند نرم نرم کوشش خویش</p></div>
<div class="m2"><p>می کند آشکاره جوشش خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلش ار گه گهی گران گردد</p></div>
<div class="m2"><p>در سر او همیشه آن گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که بود جاهش از دگر کس بیش</p></div>
<div class="m2"><p>داردش شه عزیز و خاصه خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برتر از دست خود نخواهد کس</p></div>
<div class="m2"><p>عیب او این توان نهادن و بس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از همه چیز جاه دارد دوست</p></div>
<div class="m2"><p>این ز اصل بزرگ و همت اوست</p></div></div>