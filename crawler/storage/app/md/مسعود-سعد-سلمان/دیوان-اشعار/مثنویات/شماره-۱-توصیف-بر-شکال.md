---
title: >-
    شمارهٔ ۱ - توصیف بر شکال
---
# شمارهٔ ۱ - توصیف بر شکال

<div class="b" id="bn1"><div class="m1"><p>برشکال ای بهار هندستان</p></div>
<div class="m2"><p>ای نجات از بلای تابستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دادی از تیر مه بشارت ها</p></div>
<div class="m2"><p>باز رستیم از آن حرارت ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سو از ابر لشکری داری</p></div>
<div class="m2"><p>در امارت مگر سری داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بادهای تو تو میغ ها دارند</p></div>
<div class="m2"><p>میغ های تو تیغ ها دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رعدهای تو کوس هاکوبند</p></div>
<div class="m2"><p>چرخ گویی همی که بکشوبند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبع و حال هوا دگر کردی</p></div>
<div class="m2"><p>دشت ها را همه شمر کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبزه ها را طراوتی دادی</p></div>
<div class="m2"><p>عمرها را حلاوتی دادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راغ را گل زمردین کردی</p></div>
<div class="m2"><p>باغ را شاخ بسدین کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شگفتی نکونگار گری</p></div>
<div class="m2"><p>رنگ طبعی نکو به کار بری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو بدین حمله ای که افکندی</p></div>
<div class="m2"><p>بیخ خشکی ز خاک برکندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تیر بگذشت ناگهان بر ما</p></div>
<div class="m2"><p>منهزم گشت لشکر گرما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن ما زیر جامه های تنگ</p></div>
<div class="m2"><p>گشت تازه ز بادهای خنک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اینت راحت که رنج گرما نیست</p></div>
<div class="m2"><p>پس ازین جز امید سرما نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حبذا ابرهای پر نم تو</p></div>
<div class="m2"><p>حبذا سبزه های خرم تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عیش و عشرت کنون توان کردن</p></div>
<div class="m2"><p>می شادی کنون توان خوردن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که ز گرمی خبر نگردد جان</p></div>
<div class="m2"><p>نشود همچو چوب خشک دهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جام باده بجوشد اندر کف</p></div>
<div class="m2"><p>چون سر دیگ بر نیارد کف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه دور اوفتد ز چشم ترم</p></div>
<div class="m2"><p>من به وهم اندرو همی نگرم</p></div></div>