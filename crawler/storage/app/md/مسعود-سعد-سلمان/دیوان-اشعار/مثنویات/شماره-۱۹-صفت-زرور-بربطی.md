---
title: >-
    شمارهٔ ۱۹ - صفت زرور بربطی
---
# شمارهٔ ۱۹ - صفت زرور بربطی

<div class="b" id="bn1"><div class="m1"><p>زرور از بربط بدیع نوا</p></div>
<div class="m2"><p>برکند لحظه ای به لحن هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باربد زخم و سرکش آوازست</p></div>
<div class="m2"><p>شادی افزای و رنج پردازست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان نواها که او تواند زد</p></div>
<div class="m2"><p>هیچ خنیاگری نداند زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ مطرب به گرد او نرسد</p></div>
<div class="m2"><p>که کس اندر نبرد او نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد از کودکی نکو بودست</p></div>
<div class="m2"><p>خوش عنان و لطیف خو بودست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نبودم او فراز رسید</p></div>
<div class="m2"><p>الحق از لطف دلنواز رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق را صورتش نگاری شد</p></div>
<div class="m2"><p>لهو را از رخش بهاری شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با سماع غریب دلجویش</p></div>
<div class="m2"><p>بر رخ لاله رنگ گل بویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردمان باده ها همی خوردند</p></div>
<div class="m2"><p>مهتران عیش ها بسی کردند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم به خانه نثار کردندش</p></div>
<div class="m2"><p>به همه خانه ها ببردندش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر کف دست همچو آبله ای</p></div>
<div class="m2"><p>کس نکردی ز بار او گله ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عامل سرسنی ازو بر خورد</p></div>
<div class="m2"><p>که شبی ناگهان بدو برخورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون می و شیر یافت اندامی</p></div>
<div class="m2"><p>راند هر ساعتی بر او کامی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنشستی و پیش بنشاندی</p></div>
<div class="m2"><p>همه وقتیش نوش لب خواندی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وآنچه خورشید کرد کس نکند</p></div>
<div class="m2"><p>دست خفاش پشت پس نکند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اندرو گفته بود بیچاره</p></div>
<div class="m2"><p>چون شد از درد عشق دل پاره</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن دو بینی که نام بهروزیست</p></div>
<div class="m2"><p>آخرش روشنی و پیروزیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای دریغا که برنخوردم من</p></div>
<div class="m2"><p>زان رخ چون گل و تن چو سمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زآن نکویی گذشته یافتمش</p></div>
<div class="m2"><p>تو بره ریش گشته یافتمش</p></div></div>