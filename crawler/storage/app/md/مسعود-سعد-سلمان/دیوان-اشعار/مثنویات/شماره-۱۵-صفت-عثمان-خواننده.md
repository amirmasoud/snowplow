---
title: >-
    شمارهٔ ۱۵ - صفت عثمان خواننده
---
# شمارهٔ ۱۵ - صفت عثمان خواننده

<div class="b" id="bn1"><div class="m1"><p>باز عثمان عندلیب آواز</p></div>
<div class="m2"><p>کرده از قول جادویی آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست زد چون به خفچه ایقاع</p></div>
<div class="m2"><p>بگذراند ز اوج چرخ سماع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بانگ ناگه چو بر سرود زند</p></div>
<div class="m2"><p>آتش اندر دماغ عود زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه ناگه چو در سماع آید</p></div>
<div class="m2"><p>عشرت و خرمی بیفزاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساتگینی بزرگتر خواهد</p></div>
<div class="m2"><p>زو سرود و سماع درخواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهد از وی زمان زمان بازی</p></div>
<div class="m2"><p>گاهگاهش کند هم آوازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبودی گریز پای و دنس</p></div>
<div class="m2"><p>بزم ها را چو او نبودی کس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطربان را به هم بر آغالد</p></div>
<div class="m2"><p>از میانه سبک برون کالد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کند گنده ای درشت به کف</p></div>
<div class="m2"><p>راست با هره ای چو چنبر دف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بخسبد به کنجی اندر مست</p></div>
<div class="m2"><p>با یکی قحبه کلنده کست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرگز آن شوخ دیده بی شرم</p></div>
<div class="m2"><p>زلت خادمان نگردد نرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن کسانی که دشمن اویند</p></div>
<div class="m2"><p>بیهده چیزکی نمی گویند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچه گویند من چرا گویم</p></div>
<div class="m2"><p>عیب آن بی هنر چرا جویم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او نبوده ست کودک نیکو</p></div>
<div class="m2"><p>خوش نبوده ست لحن و نغمت او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به سرای کجک نرفته است او</p></div>
<div class="m2"><p>مست هرگز به شب نخفته است او</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرد بازار و کوی گم گشتست</p></div>
<div class="m2"><p>به سر مرغزار بگذشته ست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من سخن گر همی نگردانم</p></div>
<div class="m2"><p>وز طریقی دگر همی رانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حلقه گوش او همی گوید</p></div>
<div class="m2"><p>که زبان زین سخن چه می جوید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک اشارت کفایتست او را</p></div>
<div class="m2"><p>بنده را درخورست زخم عصا</p></div></div>