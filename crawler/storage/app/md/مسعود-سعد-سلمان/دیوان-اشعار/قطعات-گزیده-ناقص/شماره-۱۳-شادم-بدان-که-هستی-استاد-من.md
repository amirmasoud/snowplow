---
title: >-
    شمارهٔ ۱۳ - شادم بدان که هستی استاد من
---
# شمارهٔ ۱۳ - شادم بدان که هستی استاد من

<div class="b" id="bn1"><div class="m1"><p>ای خواجه بوالفرج نکنی یاد من</p></div>
<div class="m2"><p>تا شاد گردد این دل ناشاد من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی که هست بنده و آزاد تو</p></div>
<div class="m2"><p>هرکس که هست بنده و آزاد من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازم بدان که هستم شاگرد تو</p></div>
<div class="m2"><p>شادم بدان که هستی استاد من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای رونی‌یی که طرفهٔ بغداد، تو</p></div>
<div class="m2"><p>دارد نشستگاه تو بغداد من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانا نه آگهی تو که باران اشک</p></div>
<div class="m2"><p>از بن همی بشوید بنیاد من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوره‌ای ز آتش غم تافته است</p></div>
<div class="m2"><p>نرم آهن است گویی پولاد من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نزدیک و دور و بیگه و گه خاص و عام</p></div>
<div class="m2"><p>فریاد برگرفته ز فریاد من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنجاه و پنج سال شد و زین عدد</p></div>
<div class="m2"><p>گر هیچ گونه برگذرد داد من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنشاند روزگارم و اندر نشاند</p></div>
<div class="m2"><p>در عاج شفشه، شفشه به شمشاد من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ران هزبر لقمه کند رنگ من</p></div>
<div class="m2"><p>مغز عقاب طعمه کند خاد من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون باد و آب در که و دشت اوفتد</p></div>
<div class="m2"><p>تیغ چو آب و بارهٔ چون باد من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با گیتی استوار کنم کار خویش</p></div>
<div class="m2"><p>گر بخت استوار کند لاد من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از روزگار باز نخواهم شدن</p></div>
<div class="m2"><p>تا روزگار می بدهد داد من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیچم مکن فرامش از یاد خویش</p></div>
<div class="m2"><p>زیرا که نه فرامشی از یاد من</p></div></div>