---
title: >-
    شمارهٔ ۱۲ - نداند حقیقت که من کیستم
---
# شمارهٔ ۱۲ - نداند حقیقت که من کیستم

<div class="b" id="bn1"><div class="m1"><p>چه کین است با من فلک را به دل؟</p></div>
<div class="m2"><p>که هر روز یک غم کند بیستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این زیستن هیچ سودم نبود</p></div>
<div class="m2"><p>هوایی همی بیهده زیستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر مهربانی بپرسد مرا</p></div>
<div class="m2"><p>چه گویم از این عمر بر چیستم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن طیره گشتم که بخت بدم</p></div>
<div class="m2"><p>بخندد بر من چو بگریستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان حمل کردم که گردون همی</p></div>
<div class="m2"><p>نداند حقیقت که من کیستم</p></div></div>