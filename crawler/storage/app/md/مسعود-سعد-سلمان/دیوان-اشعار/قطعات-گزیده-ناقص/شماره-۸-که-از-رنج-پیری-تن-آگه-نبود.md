---
title: >-
    شمارهٔ ۸ - که از رنج پیری تن آگه نبود
---
# شمارهٔ ۸ - که از رنج پیری تن آگه نبود

<div class="b" id="bn1"><div class="m1"><p>دریغا جوانی و آن روزگار</p></div>
<div class="m2"><p>که از رنج پیری تن آگه نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاط من از عیش کمتر نشد</p></div>
<div class="m2"><p>امید من از عمر کوته نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سستی مرا آن پدید آمده است</p></div>
<div class="m2"><p>در این مه که هرگز در آن مه نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبک خشک شد چشمهٔ بخت من</p></div>
<div class="m2"><p>مگر آب آن چشمه را زه نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن چاهم افکند گردون دون</p></div>
<div class="m2"><p>که از ژرفی آن چاه را ته نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهشتم همی عرضه کرد و مرا</p></div>
<div class="m2"><p>حقیقت که دوزخ جز آن چه نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسا شب که در حبس بر من گذشت</p></div>
<div class="m2"><p>که بینای آن شب جز اکمه نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیاهی سیاه و درازی دراز</p></div>
<div class="m2"><p>که آن را امید سحرگه نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی بودم و داند ایزد همی</p></div>
<div class="m2"><p>که بر من موکل کم از ده نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گوش اندرم جز کس و بس نشد</p></div>
<div class="m2"><p>به لفظ اندرم جز اه و وه نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدم ناامید و زبان مرا</p></div>
<div class="m2"><p>همه گفته جز حسبی‌الله نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به شاه ار مرا دشمن اندر سپرد</p></div>
<div class="m2"><p>نکو دید خود را و ابله نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که او آب و باد مرا در جهان</p></div>
<div class="m2"><p>همه ساله جز خاک و جز که نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موجه شمرد او حدیث مرا</p></div>
<div class="m2"><p>به ایزد که هرگز موجه نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شطرنج بازان دغایی بکرد</p></div>
<div class="m2"><p>مرا گفت هین شه کن و شه نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر این قصه او ساخت معلوم شد</p></div>
<div class="m2"><p>که جز قصه شیر و روبه نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر من منزه نبودم ز عیب</p></div>
<div class="m2"><p>کس از عیب هرگز منزه نبود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرم نعمتی بود کاکنون نماند</p></div>
<div class="m2"><p>کنون دانشی هست کانگه نبود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو من دستگه داشتم هیچ وقت</p></div>
<div class="m2"><p>زبان مرا عادت نه نبود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر گفته از پر هنر عاقلان</p></div>
<div class="m2"><p>جوابم جز احسنت و جز خه نبود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تنم شد مرفه ز رنج عمل</p></div>
<div class="m2"><p>که آنگه ز دشمن مرفه نبود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در این مدت آسایشی یافتم</p></div>
<div class="m2"><p>که گه بودم آسایش و گه نبود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جدا گشتم از درگه پادشاه</p></div>
<div class="m2"><p>بدان درگهم بیش از این ره نبود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرفتم کنون درگه ایزدی</p></div>
<div class="m2"><p>کزین به مرا هیچ درگه نبود</p></div></div>