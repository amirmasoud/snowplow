---
title: >-
    شمارهٔ ۶ - چندین هزار بیت بدیع بلند ماند
---
# شمارهٔ ۶ - چندین هزار بیت بدیع بلند ماند

<div class="b" id="bn1"><div class="m1"><p>پنجاه و هفت رفت ز تاریخ عمر من</p></div>
<div class="m2"><p>شد سودمند مدت و نا سودمند ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وامروز بر یقین و گمانم ز عمر خویش</p></div>
<div class="m2"><p>دانم که چند رفت و ندانم که چند ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فهرست حال من همه با رنج و بند بود</p></div>
<div class="m2"><p>از حبس ماند عبرت و از بند پند ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از قصد بدسگالان و ز غمز حاسدان</p></div>
<div class="m2"><p>جان در بلا فتاد و تن اندر گزند ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چوگان بنه که گوی تو اندر چه اوفتاد</p></div>
<div class="m2"><p>خیره متپ که کرهٔ تو در کمند ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن به شکر کوش که از طبع پاک تو</p></div>
<div class="m2"><p>چندین هزار بیت بدیع بلند ماند</p></div></div>