---
title: >-
    شمارهٔ ۹ - زشت باشد که شعر گوید کس
---
# شمارهٔ ۹ - زشت باشد که شعر گوید کس

<div class="b" id="bn1"><div class="m1"><p>در وفات محمد علوی</p></div>
<div class="m2"><p>خواستم زد به نظم یک دو نفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز گفتم که در جهان پس از او</p></div>
<div class="m2"><p>زشت باشد که شعر گوید کس</p></div></div>