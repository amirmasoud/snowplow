---
title: >-
    شمارهٔ ۱۵ - وصف خروس
---
# شمارهٔ ۱۵ - وصف خروس

<div class="b" id="bn1"><div class="m1"><p>ناگه خروس روزی در باغ جست</p></div>
<div class="m2"><p>در زیر شاخ گل شد و ساکن نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن برگ گل که دارد بر سر بکند</p></div>
<div class="m2"><p>اندر دو ساق پایش دو خار جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن از پی جمالی بر سر بداشت</p></div>
<div class="m2"><p>وآن از پی سلاحی بر پای بست</p></div></div>