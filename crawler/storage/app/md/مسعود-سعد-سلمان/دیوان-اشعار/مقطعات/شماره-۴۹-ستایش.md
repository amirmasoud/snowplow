---
title: >-
    شمارهٔ ۴۹ - ستایش
---
# شمارهٔ ۴۹ - ستایش

<div class="b" id="bn1"><div class="m1"><p>عجب آمد مرا ز آدمیی</p></div>
<div class="m2"><p>که تو را بیند و نگه نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی اگر زمانه تو را</p></div>
<div class="m2"><p>ناگهان از حسد سیه نکند</p></div></div>