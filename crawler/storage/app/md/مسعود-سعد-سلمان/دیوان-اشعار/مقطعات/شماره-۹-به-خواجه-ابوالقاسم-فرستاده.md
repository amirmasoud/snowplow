---
title: >-
    شمارهٔ ۹ - به خواجه ابوالقاسم فرستاده
---
# شمارهٔ ۹ - به خواجه ابوالقاسم فرستاده

<div class="b" id="bn1"><div class="m1"><p>خواجه ابوالقاسم ای بزرگ اصیل</p></div>
<div class="m2"><p>غم معشوقه هیچ کمتر هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی آگه ز حال کآن خاتون</p></div>
<div class="m2"><p>جز تو آنجاش یار دیگر هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وفای تو گر خورد سوگند</p></div>
<div class="m2"><p>که نخورده ست . . . باور هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی وصل او که خواهی یافت</p></div>
<div class="m2"><p>با غم هجر او برابر هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راههایی که او زند بر چنگ</p></div>
<div class="m2"><p>یاد داری و هیچت از بر هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد خواهیش هیچ راه آورد</p></div>
<div class="m2"><p>زین معانیت هیچ در سر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمدن در خورت نبود اینجا</p></div>
<div class="m2"><p>بازگشتنت هیچ در خور هست</p></div></div>