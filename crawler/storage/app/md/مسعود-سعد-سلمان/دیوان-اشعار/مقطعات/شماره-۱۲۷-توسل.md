---
title: >-
    شمارهٔ ۱۲۷ - توسل
---
# شمارهٔ ۱۲۷ - توسل

<div class="b" id="bn1"><div class="m1"><p>ای به تو برپای شهریاری</p></div>
<div class="m2"><p>وی به تو بر جای پادشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این ز پی کدیه می نگویم</p></div>
<div class="m2"><p>نیست مرا عادت گدایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و دل اندر ثنات بستم</p></div>
<div class="m2"><p>تا فرجم را دری گشایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زآنکه تو در هر چه رای کردی</p></div>
<div class="m2"><p>با فلک سخت سر برآیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوب خصالی گزیده فعلی</p></div>
<div class="m2"><p>میمون لفظی خجسته رایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جاه تو آرد همی بلندی</p></div>
<div class="m2"><p>کار تو دارد همی روایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان روان را همی بکوشم</p></div>
<div class="m2"><p>تا دهدم روز روشنایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بندگی خویش کرد باید</p></div>
<div class="m2"><p>زانکه نکردست کس خدایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خلق جهان را فرا نمایم</p></div>
<div class="m2"><p>گر تو عنایت فرا نمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ارجو تا آسمان بپاید</p></div>
<div class="m2"><p>روشن و عالی چو او بپایی</p></div></div>