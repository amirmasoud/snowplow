---
title: >-
    شمارهٔ ۶۴ - مدح
---
# شمارهٔ ۶۴ - مدح

<div class="b" id="bn1"><div class="m1"><p>سرفرازا ز خدمت تا شدم دور</p></div>
<div class="m2"><p>بباشد دیدگانم هر زمان تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان گریم که بی معشوق عاشق</p></div>
<div class="m2"><p>چنان نالم که بی فرزند مادر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر آتش زنی اندر دل من</p></div>
<div class="m2"><p>همان گیری که مغز از دود مجمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر پر زهر گردانی دهانم</p></div>
<div class="m2"><p>زبانم گویدت شکری چو شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا در هیچ بزم و هیچ مجلس</p></div>
<div class="m2"><p>مرا در هیچ درج و هیچ دفتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخواهد جز به نامت رفت خامه</p></div>
<div class="m2"><p>نخواهد جز به یادت گشت ساغر</p></div></div>