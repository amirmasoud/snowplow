---
title: >-
    شمارهٔ ۱۳۴ - خطاب به روزن زندان
---
# شمارهٔ ۱۳۴ - خطاب به روزن زندان

<div class="b" id="bn1"><div class="m1"><p>ای دلارای روزن زندان</p></div>
<div class="m2"><p>دیدگان را نعیم جاویدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی محاق و کسوف بادی زآنک</p></div>
<div class="m2"><p>شب مرا ماه و روز خورشیدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه سعدم تویی از آنکه مرا</p></div>
<div class="m2"><p>فلک مشتری و ناهیدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور همی دیو بینم از تو رواست</p></div>
<div class="m2"><p>که گذرگاه تخت جمشیدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امید تو زنده ام گر نه</p></div>
<div class="m2"><p>مر مرا کشته بود نومیدی</p></div></div>