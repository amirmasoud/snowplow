---
title: >-
    شمارهٔ ۲۰ - مدح ثقة الملک طاهر
---
# شمارهٔ ۲۰ - مدح ثقة الملک طاهر

<div class="b" id="bn1"><div class="m1"><p>ثقت الملک تا به صدر نشست</p></div>
<div class="m2"><p>دهر پیشش میان به طوع ببست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا همایون دوات پیش نهاد</p></div>
<div class="m2"><p>الفش را فلک به تا پیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد دشمن شدست و داروی دوست</p></div>
<div class="m2"><p>تاش بسپرد آن مبارک دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنگر اکنون به تازگی عجبا</p></div>
<div class="m2"><p>کاندر آن لفظ درد و دارویست</p></div></div>