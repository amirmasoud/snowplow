---
title: >-
    شمارهٔ ۹۵ - از زبان ملک ارسلان گوید
---
# شمارهٔ ۹۵ - از زبان ملک ارسلان گوید

<div class="b" id="bn1"><div class="m1"><p>من مایه عدل و مایه جودم</p></div>
<div class="m2"><p>سلطان ملک ارسلان مسعودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید جهان فروز شد رأیم</p></div>
<div class="m2"><p>باران زمین نگار شد جودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محمود خصال و رسم و ره رانم</p></div>
<div class="m2"><p>زیرا شرف نژاد محمودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قوت و قدرت سلیمانم</p></div>
<div class="m2"><p>زیرا از اصل و نسل داودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید ملوک هفت اقلیمم</p></div>
<div class="m2"><p>تا سایه کردگار معبودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایزد داند که جز رضای او</p></div>
<div class="m2"><p>از ملک نبود و نیست مقصودم</p></div></div>