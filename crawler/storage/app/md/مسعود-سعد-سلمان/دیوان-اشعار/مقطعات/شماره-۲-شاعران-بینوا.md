---
title: >-
    شمارهٔ ۲ - شاعران بینوا
---
# شمارهٔ ۲ - شاعران بینوا

<div class="b" id="bn1"><div class="m1"><p>شاعران بینوا خوانند شعر با نوا</p></div>
<div class="m2"><p>وز نوای شعرشان افزون نمی گردد نوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوطیانه گفت و نتوانند جز آموخته</p></div>
<div class="m2"><p>عندلیبم من که هر ساعت دگر سازم نوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندران معنی که گوید بدهم انصاف سخن</p></div>
<div class="m2"><p>پادشاهم بر سخن جایز نباشد پادشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باطلی گر حق کنم عالم مرا گردد مقر</p></div>
<div class="m2"><p>ور حقی باطل کنم منکر نگردد کس مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر اردر زیر پا آرم کنم سنگ سیاه</p></div>
<div class="m2"><p>خاک اگر در دست گیرم سازم از وی کیمیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر هجا گویم رمد از پیش من دیو سپید</p></div>
<div class="m2"><p>ور غزل خوانم مرا منقاد گردد اژدها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس مرا نشناسد و بیگانه رویم نزد خلق</p></div>
<div class="m2"><p>زآنکه در گیتی ز بی جنسی ندارم آشنا</p></div></div>