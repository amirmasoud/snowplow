---
title: >-
    شمارهٔ ۵ - به خواجه ناصر
---
# شمارهٔ ۵ - به خواجه ناصر

<div class="b" id="bn1"><div class="m1"><p>خواجه ناصر خدای داند و بس</p></div>
<div class="m2"><p>کآروزی تو تا کجاست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چو رفتم تو هیچ کردی یاد</p></div>
<div class="m2"><p>صحبت من بگوی راست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار چونست مر تو را کامروز</p></div>
<div class="m2"><p>کار با برگ و بانواست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزد بونصر پارسی گویم</p></div>
<div class="m2"><p>روز بازار تیز خاست مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه کام و هوا به دولت او</p></div>
<div class="m2"><p>از فلک رایج و رواست مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچنان داردم که پنداری</p></div>
<div class="m2"><p>به دعا از خدای خواست مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرفرازی که گرد موکب او</p></div>
<div class="m2"><p>همه در چشم توتیاست مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نامداری که خاک درگه او</p></div>
<div class="m2"><p>همه در دست کیمیاست مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیکن اندر میان شغلی ام</p></div>
<div class="m2"><p>که در او شدت و رخاست مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عملی می کنم که از بد و نیک</p></div>
<div class="m2"><p>گاه خوفست و گه رجاست مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه اندر میان صدری ام</p></div>
<div class="m2"><p>کز همه دوستان ثناست مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز آفتاب سعادت تابانش</p></div>
<div class="m2"><p>روز اقبال پرضیاست مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین همه نیکویی مرا حظ است</p></div>
<div class="m2"><p>با همه شادی التقاست مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز گه بر کران دشتی ام</p></div>
<div class="m2"><p>که درو بیم صد بلاست مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمترین رهبری مرا غول است</p></div>
<div class="m2"><p>بهترین همرهی صباست مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نرمتر بالشی مرا سنگ است</p></div>
<div class="m2"><p>گرمتر بستری گیاست مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عز با دردسر که دارد من؟</p></div>
<div class="m2"><p>جاه با رنج دل کراست مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در فروغ دل چنین مخدوم</p></div>
<div class="m2"><p>آن همه رنج ها رواست مرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای رفیقان فراق روی شما</p></div>
<div class="m2"><p>در دل و جان و غم و عناست مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل و جانم همه شاد دارید</p></div>
<div class="m2"><p>وین شگفتی بدین رضاست مرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کس نگوید که زنده چون مانم</p></div>
<div class="m2"><p>چون دل و جان ز تن جداست مرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس چو بیجان دو دل همی باشم</p></div>
<div class="m2"><p>بی شما زیستن خطاست مرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه کنم قصه کآرزوی شما</p></div>
<div class="m2"><p>داند ایزد که جان بکاست مرا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ورنه این دوستی ز جان و دلست</p></div>
<div class="m2"><p>به شما این شغب چراست مرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نکنم عشرتی به طبع و همه</p></div>
<div class="m2"><p>هوس عشرت شماست مرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواجه با توام کزین گفتار</p></div>
<div class="m2"><p>از سر سمعه و ریاست مرا</p></div></div>