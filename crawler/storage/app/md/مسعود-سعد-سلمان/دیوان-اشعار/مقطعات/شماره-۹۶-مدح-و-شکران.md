---
title: >-
    شمارهٔ ۹۶ - مدح و شکران
---
# شمارهٔ ۹۶ - مدح و شکران

<div class="b" id="bn1"><div class="m1"><p>چه خدمت کرد شاها بنده تو</p></div>
<div class="m2"><p>که با توست این چنین اعزاز و اکرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولیکن خسروا تو آفتابی</p></div>
<div class="m2"><p>که هست این گیتی از تو گشته پدرام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو دریایی و از دریا همه کس</p></div>
<div class="m2"><p>لالی و درر یابد به اقسام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی بارنده ابر و ابر دایم</p></div>
<div class="m2"><p>ببارد یکسره بر خاص و بر عام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه دانم گفت شاها من ز شکرت</p></div>
<div class="m2"><p>کنم شکرت به طاقت تا سرانجام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خداوند جهان پاداش بدهد</p></div>
<div class="m2"><p>تو را ای شه بدین انعام و اکرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببند شکر پای بنده بستی</p></div>
<div class="m2"><p>به منت بنده را کردی تو احکام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه یار بادت چرخ گردون</p></div>
<div class="m2"><p>نگهدار تو باد ای شاه قسام</p></div></div>