---
title: >-
    شمارهٔ ۴۳ - اسیر خوبان
---
# شمارهٔ ۴۳ - اسیر خوبان

<div class="b" id="bn1"><div class="m1"><p>اگر اسیر کسی ام که میر خوبان شد</p></div>
<div class="m2"><p>نه من نخست کسی ام کاسیر خوبان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکیب کردن نادلپذیر دان ز دلی</p></div>
<div class="m2"><p>که بسته سخن دلپذیر خوبان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نباشد ایمن گر کوه را سپر سازد</p></div>
<div class="m2"><p>تنی که او هدف زخم تیر خوبان باشد</p></div></div>