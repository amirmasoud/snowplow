---
title: >-
    شمارهٔ ۷۴ - نبشتن ز گفتن مهمتر شناس
---
# شمارهٔ ۷۴ - نبشتن ز گفتن مهمتر شناس

<div class="b" id="bn1"><div class="m1"><p>نبشتن ز گفتن مهمتر شناس</p></div>
<div class="m2"><p>به گاه نوشتن بجا آر هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن با قلم چون قلم راست دار</p></div>
<div class="m2"><p>به نیک و به بد در سخن نیک کوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو نوک قلم را مدان جز دو چیز</p></div>
<div class="m2"><p>یکی صرف زهر و یکی محض نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو از نوش او زندگانی ستان</p></div>
<div class="m2"><p>ز زهرش مکن جان شیرین به جوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گفتن تو را گر خطایی فتد</p></div>
<div class="m2"><p>ز بربط فزونت بمالند گوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وگر در نبشتن خطایی کنی</p></div>
<div class="m2"><p>سرت چون قلم دور ماند ز دوش</p></div></div>