---
title: >-
    شمارهٔ ۵۰ - صفت گل رعنا
---
# شمارهٔ ۵۰ - صفت گل رعنا

<div class="b" id="bn1"><div class="m1"><p>دور وی چنین بود که رعناست</p></div>
<div class="m2"><p>طیره شده و روان پر درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک روی ز شرم دوستان سرخ</p></div>
<div class="m2"><p>یک روی ز بیم دشمنان زرد</p></div></div>