---
title: >-
    شمارهٔ ۱۳۰ - فرامش گشت رسم شادمانی
---
# شمارهٔ ۱۳۰ - فرامش گشت رسم شادمانی

<div class="b" id="bn1"><div class="m1"><p>بر آن افراخته کوهم که گویی</p></div>
<div class="m2"><p>مرا فرمود گردون دیده بانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدی بی غم ز ظل و خط مقیاس</p></div>
<div class="m2"><p>اگر جایی چنین دیدی بیانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همانا باز نشناسی چو بینی</p></div>
<div class="m2"><p>مرا روزی ز زاری و نوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمانی گشته قد من ز سروی</p></div>
<div class="m2"><p>زریری گشته چهر ارغوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زده را هم قضا و اوفتاده</p></div>
<div class="m2"><p>زیان مالی و جاهی و نانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بیم لشگر پیری به زندان</p></div>
<div class="m2"><p>منقص گشته بر من زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر پیری بماندی جاودانه</p></div>
<div class="m2"><p>چه انده بودی از هجر جوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کم آید حاصل رنجم تو گویی</p></div>
<div class="m2"><p>ثوالث ضرب کردم در ثوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا بیکار خوانم خویشتن را</p></div>
<div class="m2"><p>که دارم بر بلاها قهرمانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرم فانی نگشتی گوهر اشک</p></div>
<div class="m2"><p>یکی گنجی شدستی شایگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا اینجا ز بس انده که خوردم</p></div>
<div class="m2"><p>فرامش گشت رسم شادمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غم آمد سود من بر مایه عمر</p></div>
<div class="m2"><p>که کردست این چنین بازارگانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرم شد این جهانی عمرضایع</p></div>
<div class="m2"><p>نشد ضایع ثواب آن جهانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو ای از هر بدی چون جان منزه</p></div>
<div class="m2"><p>بکن نیکی به هر کس تا توانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهاد نیک و بد دانی که دانم</p></div>
<div class="m2"><p>نهاد بیش و کم دانم که دانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندارد سود درمان زمینی</p></div>
<div class="m2"><p>کرا دریافت درد آسمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا زین حادثه بس هول نبود</p></div>
<div class="m2"><p>که در دل بود ازین عالم گمانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همی دیدم که کیوان روی دادست</p></div>
<div class="m2"><p>به طالع بیش ازین باشد نشانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>درآمد بازگشت و اندر آمد</p></div>
<div class="m2"><p>چه خواهی کرد این بار از زبانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چرا نالم چرا باشم هراسان</p></div>
<div class="m2"><p>ز محنت چون ز دزدان کاروانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سزد گر فخر جویم آشکارا</p></div>
<div class="m2"><p>بر آن کو مفخرت جوید نهانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>منم کاندر عجم و اندر عرب کس</p></div>
<div class="m2"><p>نبیند چون من از چیره زبانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر افتد مشکلی در نظم و در نثر</p></div>
<div class="m2"><p>ز من خواهد زمانه ترجمانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدین هر دو زبان در هر دو میدان</p></div>
<div class="m2"><p>به گردونم رسیده کامرانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سجود آرد به پیش خاطر من</p></div>
<div class="m2"><p>روان رودکی و ابن هانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>معاذالله مرا چه افتاد زنهار</p></div>
<div class="m2"><p>نباید کاین به طیبت بربخوانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنانم کرد محنت کانچه گویم</p></div>
<div class="m2"><p>نمی دانم من از تیره روانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان دارم امید از لطف یزدان</p></div>
<div class="m2"><p>که زایل گردد از من ناتوانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیابم همت خویش ار به یکبار</p></div>
<div class="m2"><p>نخواند بخت بر من لن ترانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برون آیم ز بند و حبس روزی</p></div>
<div class="m2"><p>چو در بحری و چون زرکانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو پیش آیم مرا خوشتر نوازی</p></div>
<div class="m2"><p>چو بنشینم مرا بهتر نشانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو فرشی گستری تازه ز حرمت</p></div>
<div class="m2"><p>چو بنوشتم بساط سوزیانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنین باشد چو دانستی که از من</p></div>
<div class="m2"><p>نباشد جز به آمد شد گرانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نبودم جز چنین الحمدالله</p></div>
<div class="m2"><p>به حق حرمت سبع المثانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>منش دارم که گر گردد مجسم</p></div>
<div class="m2"><p>تو در بالای او خیره بمانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من از شادی روی فرخ تو</p></div>
<div class="m2"><p>کنم چو لاله روی زعفرانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو اندر دولتی افزون زبوده</p></div>
<div class="m2"><p>به گیتی بیش ازین مانده بمانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شود قدرت چو گردون از بلندی</p></div>
<div class="m2"><p>برد امرت چو جیحون از روانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مروت کرده باشی گر بزودی</p></div>
<div class="m2"><p>جواب این به نزد من رسانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برین خوانم ز یزدان استعانت</p></div>
<div class="m2"><p>فان الله اکرم مستعانی</p></div></div>