---
title: >-
    شمارهٔ ۷۵ - یک زمان در بهشت
---
# شمارهٔ ۷۵ - یک زمان در بهشت

<div class="b" id="bn1"><div class="m1"><p>یک زمان در بهشت بودم دوش</p></div>
<div class="m2"><p>نوش کردم ز گفته های تو نوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نبودم برسم معذورم</p></div>
<div class="m2"><p>در جمال تو بسته بودم هوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه بودم به مدحتت گویا</p></div>
<div class="m2"><p>گاه بودم ز حشمتت خاموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه چون بحر طبعم اندر موج</p></div>
<div class="m2"><p>گه چو خورشید ذاتم اندر جوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای فلک رأی مهتری که تو را</p></div>
<div class="m2"><p>نام پیغمبر است و طبع سروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه اقبال بدهدت بستان</p></div>
<div class="m2"><p>وآنچه دولت بگویدت بنیوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمدی دی تو از پی کاری</p></div>
<div class="m2"><p>بنده ام گشته حلقه اندر گوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدم من همی ببوسد فخر</p></div>
<div class="m2"><p>تا گرفتی مرا تو در آغوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من نیابم چو تو یقین گشتم</p></div>
<div class="m2"><p>تو نیابی چو من مرا مفروش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوش دیدم سلامت و شادی</p></div>
<div class="m2"><p>این همه شادی و سلامت دوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا همی لاله باشد و باده</p></div>
<div class="m2"><p>روی باده ببین و باده بنوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو باده به طبع لهوانگیز</p></div>
<div class="m2"><p>همچو لاله لباس شادی پوش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رأی عالی رضای تو جسته ست</p></div>
<div class="m2"><p>تو به جان در رضای عالی کوش</p></div></div>