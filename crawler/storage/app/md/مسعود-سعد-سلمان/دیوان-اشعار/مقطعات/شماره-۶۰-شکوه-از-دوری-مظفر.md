---
title: >-
    شمارهٔ ۶۰ - شکوه از دوری مظفر
---
# شمارهٔ ۶۰ - شکوه از دوری مظفر

<div class="b" id="bn1"><div class="m1"><p>ای مظفر فراق یافت ظفر</p></div>
<div class="m2"><p>بر تن من نکرده هیچ نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنجری ناکشیده در حمله</p></div>
<div class="m2"><p>باره ای نافکنده در ناورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرقت خیره روی روبا روی</p></div>
<div class="m2"><p>از منت در ربود مردا مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک هجر خوی سفله مرا</p></div>
<div class="m2"><p>فرد کرد از من ای بدانش فرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل تابنده را فرو شد روز</p></div>
<div class="m2"><p>هجر تاریک را بر آمد گرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل بر توست و با تو خواهد بود</p></div>
<div class="m2"><p>من بی دل چگونه خواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود خواهم ولیک سخت به رنج</p></div>
<div class="m2"><p>زیست خواهم و لیک نیک به درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر تن سست کوفته غم سخت</p></div>
<div class="m2"><p>وز دل گرم خاسته دم سرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم من آب روی خواهد برد</p></div>
<div class="m2"><p>روی من آب چشم خواهد خورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقش کار فراق پیدا شد</p></div>
<div class="m2"><p>اینک از اشک لعل و چهره زرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دهر بی شرم چون بخواست نوشت</p></div>
<div class="m2"><p>فرش شادی ما چرا گسترد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرخ بی رحم چون بخواست برید</p></div>
<div class="m2"><p>شاخ امید من چرا پرورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای هنر سنج مهتری که فلک</p></div>
<div class="m2"><p>در فنون فلک چو تو ناورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل سپردم تو را به غزنین بر</p></div>
<div class="m2"><p>بر آن دوستان به راه آورد</p></div></div>