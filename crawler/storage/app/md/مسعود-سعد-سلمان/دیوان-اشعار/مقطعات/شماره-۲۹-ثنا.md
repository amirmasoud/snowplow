---
title: >-
    شمارهٔ ۲۹ - ثنا
---
# شمارهٔ ۲۹ - ثنا

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که رای صایب تو</p></div>
<div class="m2"><p>کارهای عمل به سامان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار کرد هنر کفایت تو</p></div>
<div class="m2"><p>بر کفاة زمانه تاوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه تاریک دید روشن ساخت</p></div>
<div class="m2"><p>هر چه دشوار دید آسان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شفقت های راستت بر من</p></div>
<div class="m2"><p>مکرمت های بس فراوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عادتم کرده ای به خلعت خویش</p></div>
<div class="m2"><p>عادت کرده باز نتوان کرد</p></div></div>