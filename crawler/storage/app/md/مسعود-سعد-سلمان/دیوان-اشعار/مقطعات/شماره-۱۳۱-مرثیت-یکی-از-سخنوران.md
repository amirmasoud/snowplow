---
title: >-
    شمارهٔ ۱۳۱ - مرثیت یکی از سخنوران
---
# شمارهٔ ۱۳۱ - مرثیت یکی از سخنوران

<div class="b" id="bn1"><div class="m1"><p>گفتم تو مرا مرثیت کنی</p></div>
<div class="m2"><p>خویشان مرا تعزیت کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرزند مرا چون برادران</p></div>
<div class="m2"><p>در هر هنری تربیت کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یابی به جهان عمر تا که قاف</p></div>
<div class="m2"><p>تا قاف پر از قافیت کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهان جهان را به مدح ها</p></div>
<div class="m2"><p>هر جنس بسی تهنیت کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمال خرد را ز طبع و دل</p></div>
<div class="m2"><p>ترتیب نهی تمشیت کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان را و روان را به فضل و عقل</p></div>
<div class="m2"><p>تیمارکش تقویت کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میدان سخن را به نظم و نثر</p></div>
<div class="m2"><p>بر باره نیکو شیت کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عالم دانش به سعی فهم</p></div>
<div class="m2"><p>طاعت همه بی معصیت کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی بود گمانم کز این جهان</p></div>
<div class="m2"><p>بی زاد به رفتن نیت کنی</p></div></div>