---
title: >-
    شمارهٔ ۱۰۴ - مطایبه
---
# شمارهٔ ۱۰۴ - مطایبه

<div class="b" id="bn1"><div class="m1"><p>این چنین روز مر حریفان را</p></div>
<div class="m2"><p>پای باید کشید در دامن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میزبان نیز کعبتین خزان</p></div>
<div class="m2"><p>سیم آسا ز خانه روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چه گوید که هفت بخشیده</p></div>
<div class="m2"><p>وآن دگر گویدش بزن بر من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویدش میز نصر آزاده</p></div>
<div class="m2"><p>می نبینی سبک مترس و بزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز سرهنگ ابوالحسن گوید</p></div>
<div class="m2"><p>بزن وگرنه کعبتین بفکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این و آن را به دم علی ناییست</p></div>
<div class="m2"><p>کند انکار ده و به زرق و به فن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوسو اندر میان نشسته چو شیر</p></div>
<div class="m2"><p>با یکی دوست با یکی دشمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دستها را برهنه کرده تمام</p></div>
<div class="m2"><p>راست چون دست های بابیزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن از هفتم آسمان گوید</p></div>
<div class="m2"><p>پنج شش جای پاره پیراهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دعوی ده کند که در خانش</p></div>
<div class="m2"><p>به خدای ار علف بود یک من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زخم های برهنه کرده بره</p></div>
<div class="m2"><p>دست از دست باشدش بشکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان حلال و حرام باغ و زرع</p></div>
<div class="m2"><p>می خورد همچو شکر و روغن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باز نور زیاده قمره زده</p></div>
<div class="m2"><p>مانده بر بسته همچو چوب دهن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه گوید ز درد دل یارب</p></div>
<div class="m2"><p>گاه خارد ز زخم بد گردن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پسران نجیب ایزد یار</p></div>
<div class="m2"><p>کرد بیرون نهاده با دو سه تن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر ببردند برجهند که بیش</p></div>
<div class="m2"><p>نتوان بست پایشان به رسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ور نمانند هیچ آن گویند</p></div>
<div class="m2"><p>که بود راست بابت گلخن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دانی آنگاه تا چگونه رود</p></div>
<div class="m2"><p>از ثناهای خوب و مادر و زن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وآن مجاهز شماره های جهیز</p></div>
<div class="m2"><p>کرده و تازه گشته همچو سمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای برادر به گرد سیم برآی</p></div>
<div class="m2"><p>بر نیاید جهیز تو به سخن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بخواهی که تخم جمع شود</p></div>
<div class="m2"><p>بیش خویشش تریز چون خرمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مایه باید که سود بربندی</p></div>
<div class="m2"><p>ورنه برخیز و خیره ریش مکن</p></div></div>