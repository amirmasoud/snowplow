---
title: >-
    شمارهٔ ۱۲۲ - مدح سیف الدوله محمود
---
# شمارهٔ ۱۲۲ - مدح سیف الدوله محمود

<div class="b" id="bn1"><div class="m1"><p>رسید نامه فتح و ظفر ز شاهنشاه</p></div>
<div class="m2"><p>به سیف دولت شاه بلند حشمت و جاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که برد حاجب نعمان سپه سوی مکران</p></div>
<div class="m2"><p>به بخت و دولت سلطان به فر و عون اله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تیغ روز نکو خواه ملک کرد سپید</p></div>
<div class="m2"><p>به گرز روز بداندیش شاه کرد سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببست کفر و ضلال و مخالفی را در</p></div>
<div class="m2"><p>گشاد سنت و اسلام و ایمنی را راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنون که حاجب نعمان بکرد این خدمت</p></div>
<div class="m2"><p>بیافت بی شک تصحیف نام خویش از شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایا گداخته بد خواه را به تیغ گران</p></div>
<div class="m2"><p>ایا گذاشته از اوج چرخ پر کلاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز حمله تو بلرزد به آب در ماهی</p></div>
<div class="m2"><p>ز صولت تو به رزم اندرون بترسد ماه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتوح خواهد بودن ازین سپس هر روز</p></div>
<div class="m2"><p>به دولت تو و تأیید و فر شاهنشاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همیشه باد ز فتح و ظفر سوی تو نفر</p></div>
<div class="m2"><p>همیشه کار بادا به کام نیکو خواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عماد ملک و شریعت همیشه بادا راست</p></div>
<div class="m2"><p>همیشه پشت بداندیش ملک باد دو تاه</p></div></div>