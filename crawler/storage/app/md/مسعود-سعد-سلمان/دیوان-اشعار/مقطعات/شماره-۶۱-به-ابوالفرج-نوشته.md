---
title: >-
    شمارهٔ ۶۱ - به ابوالفرج نوشته
---
# شمارهٔ ۶۱ - به ابوالفرج نوشته

<div class="b" id="bn1"><div class="m1"><p>بوالفرج ای خواجهٔ آزاد مرد</p></div>
<div class="m2"><p>هجر وصال تو مرا خیره کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید ز سختی تن و جان آنچه دید</p></div>
<div class="m2"><p>خورد ز تلخی دل و جان آنچه خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای به بلندی سخن شاعران</p></div>
<div class="m2"><p>هرگز مانند تو نادیده مرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی توام از همه چیز آرزوست</p></div>
<div class="m2"><p>خسته همی جوید درمان درد</p></div></div>