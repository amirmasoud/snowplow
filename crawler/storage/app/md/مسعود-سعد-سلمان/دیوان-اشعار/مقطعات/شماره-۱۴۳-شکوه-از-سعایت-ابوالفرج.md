---
title: >-
    شمارهٔ ۱۴۳ - شکوه از سعایت ابوالفرج
---
# شمارهٔ ۱۴۳ - شکوه از سعایت ابوالفرج

<div class="b" id="bn1"><div class="m1"><p>بوالفرج شرم نامدت که بجهد</p></div>
<div class="m2"><p>به چنین حبس و بندم افکندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا من اکنون ز غم همی گریم</p></div>
<div class="m2"><p>تو به شادی ز دور می خندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد فراموش کز برای تو باز</p></div>
<div class="m2"><p>من چه کردم ز نیک پیوندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مر تو را هیچ باک نامد از آنک</p></div>
<div class="m2"><p>نوزده سال بوده ام بندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآن خداوند من که از همه نوع</p></div>
<div class="m2"><p>داشت بر تو بسی خداوندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشته او را یقین که تو شده ای</p></div>
<div class="m2"><p>با همه دشمنانش سوگندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نهالیت بر چمن بنشاند</p></div>
<div class="m2"><p>تا تو او را ز بیخ برکندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وین چنین قوتی تو راست که تو</p></div>
<div class="m2"><p>پارسی را کنی شکاوندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وآنچه کردی تو اندرین معنی</p></div>
<div class="m2"><p>نکند ساحر دماوندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو چه گویی چنین روا باشد</p></div>
<div class="m2"><p>در مسلمانی و خردمندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که کسی با تو در همه گیتی</p></div>
<div class="m2"><p>گر یکی زین کند تو نپسندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه در تو کنند گنده کنی</p></div>
<div class="m2"><p>ای شگفتی نکو خداوندی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به قضایی که رفت خرسندم</p></div>
<div class="m2"><p>نیست اندر جهان چو خرسندی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرده های تو ناپسندیده ست</p></div>
<div class="m2"><p>تا تو زین کرده ها چه بربندی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زود خواهی درود بی شبهت</p></div>
<div class="m2"><p>بر تخمی که خود پراکندی</p></div></div>