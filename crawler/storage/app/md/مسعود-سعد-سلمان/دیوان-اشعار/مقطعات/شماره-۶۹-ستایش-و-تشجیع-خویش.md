---
title: >-
    شمارهٔ ۶۹ - ستایش و تشجیع خویش
---
# شمارهٔ ۶۹ - ستایش و تشجیع خویش

<div class="b" id="bn1"><div class="m1"><p>تو ای تن برامش میا و مرو</p></div>
<div class="m2"><p>تو ای سر به شادی مخسب و مخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ای دل دژم باش و هموار باش</p></div>
<div class="m2"><p>تو ای دیده خون ریز و پیوسته ریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبینید پیری که جان مرا</p></div>
<div class="m2"><p>نشسته ست چون شیری اندر نخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بناگوش من پر ز شمشیر کرد</p></div>
<div class="m2"><p>ز موی سپید اینت کین و ستیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب می کند زان بناگوش من</p></div>
<div class="m2"><p>که هرگز ندیده ست شمشیر تیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن رو که با تیغ تیز آشنا</p></div>
<div class="m2"><p>مر او را نبوده ست در رستخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شناسد مرا تیغ بران که کس</p></div>
<div class="m2"><p>ندیده ست پشت مرا در گریز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نیزه روم در اجل بند بند</p></div>
<div class="m2"><p>اگر همچو جوشن شوم ریز ریز</p></div></div>