---
title: >-
    شمارهٔ ۱۶ - پیری و جوانی
---
# شمارهٔ ۱۶ - پیری و جوانی

<div class="b" id="bn1"><div class="m1"><p>آدمی سر به سر همه عیب است</p></div>
<div class="m2"><p>پرده عیبهاش برناییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر این پرده چون برون آید</p></div>
<div class="m2"><p>همه بیچارگی و رسواییست</p></div></div>