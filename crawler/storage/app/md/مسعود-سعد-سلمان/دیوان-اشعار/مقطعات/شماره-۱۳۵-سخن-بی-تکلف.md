---
title: >-
    شمارهٔ ۱۳۵ - سخن بی تکلف
---
# شمارهٔ ۱۳۵ - سخن بی تکلف

<div class="b" id="bn1"><div class="m1"><p>ای بد از نیک فرق کرده بسی</p></div>
<div class="m2"><p>قدر دعوی شناخته ز خسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده انصاف حق که هست امروز</p></div>
<div class="m2"><p>دانشت را تمام دست رسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تکلف چنین سخن خیزد</p></div>
<div class="m2"><p>در ثنای کسی ز طبع کسی؟</p></div></div>