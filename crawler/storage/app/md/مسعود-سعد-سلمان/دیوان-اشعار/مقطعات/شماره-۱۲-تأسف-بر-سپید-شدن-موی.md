---
title: >-
    شمارهٔ ۱۲ - تأسف بر سپید شدن موی
---
# شمارهٔ ۱۲ - تأسف بر سپید شدن موی

<div class="b" id="bn1"><div class="m1"><p>مویم آخر جز از سپید نگشت</p></div>
<div class="m2"><p>گر چه اول جز از سیاه نرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ آن سرخ هم نشد گر چند</p></div>
<div class="m2"><p>مردم آن را به خون دیده بشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرد را چون سپید گردد موی</p></div>
<div class="m2"><p>تن چو موی سپید گردد سست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نادرستی بودش رنگ دوم</p></div>
<div class="m2"><p>چون درستیش بود رنگ نخست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن بنه مرگ را حرص خلود</p></div>
<div class="m2"><p>از دل خویشتن برون کن چست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موی چون نادرست گشت بدان</p></div>
<div class="m2"><p>که نمانده است جای موی درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوزخ جاودانه جست آن کس</p></div>
<div class="m2"><p>کز جهانی عمر جاودانی جست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پند این مستمند بشنو نیک</p></div>
<div class="m2"><p>دل بر آن نه که آن سعادت توست</p></div></div>