---
title: >-
    شمارهٔ ۳۲ - دیده نرگس
---
# شمارهٔ ۳۲ - دیده نرگس

<div class="b" id="bn1"><div class="m1"><p>آن شب که دگر روز مرا عزم سفر بود</p></div>
<div class="m2"><p>ناگاه ز اطراف نسیم سحر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی تبتی مشک و گل سرخ همی زد</p></div>
<div class="m2"><p>وآن ترک من از حجره چو خورشید برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن دیده چون نرگس چون دیده نرگس</p></div>
<div class="m2"><p>در دیده تاریک به وقت سحر آمد</p></div></div>