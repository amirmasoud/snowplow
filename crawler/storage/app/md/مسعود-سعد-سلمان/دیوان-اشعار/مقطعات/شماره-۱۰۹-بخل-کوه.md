---
title: >-
    شمارهٔ ۱۰۹ - بخل کوه
---
# شمارهٔ ۱۰۹ - بخل کوه

<div class="b" id="bn1"><div class="m1"><p>گر چه پیوسته همه از زر و سیم</p></div>
<div class="m2"><p>گنجها پر کند این کوه کلان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرف های کمرش برف و یخست</p></div>
<div class="m2"><p>بخل از این بیش نباشد به جهان</p></div></div>