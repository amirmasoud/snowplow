---
title: >-
    شمارهٔ ۹۲ - شکوه از موی
---
# شمارهٔ ۹۲ - شکوه از موی

<div class="b" id="bn1"><div class="m1"><p>پیوسته من از سپید مویی</p></div>
<div class="m2"><p>حجام بروت کنده باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا می بکنم سپید مویی</p></div>
<div class="m2"><p>ده موی سیاه کنده باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ریش چنین که من برآرم</p></div>
<div class="m2"><p>سخت از در ریش خنده باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با موی خودم چو برنیایم</p></div>
<div class="m2"><p>با چرخ کجا بسنده باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وین قصه به دوستان رسانم</p></div>
<div class="m2"><p>گر بگذارند زنده باشم</p></div></div>