---
title: >-
    شمارهٔ ۳۴ - مدح صاحب دیوان مولتان
---
# شمارهٔ ۳۴ - مدح صاحب دیوان مولتان

<div class="b" id="bn1"><div class="m1"><p>خواجه عمید صاحب دیوان مولتان</p></div>
<div class="m2"><p>فرزانه ایست کافی و آزاده ایست راد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم عطیت معطی چو او نبود</p></div>
<div class="m2"><p>وز مادر کفایت کافی چو او نزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ابر بر بساط سخا راد کف نشست</p></div>
<div class="m2"><p>چون کوه در مصاف هنر پر دل ایستاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راهی که او سپرد به همت نکو سپرد</p></div>
<div class="m2"><p>رسمی که او نهاد به حشمت نکو نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز به هیچ مکرمت از خود عجب نکرد</p></div>
<div class="m2"><p>روزی به هیچ تربیت از ره نیوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه چون تنگ دلان بفزایش نمود فخر</p></div>
<div class="m2"><p>نه چون سبکسران به ستایش گرفت باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شد گشاده ما را یک در به صحبتش</p></div>
<div class="m2"><p>بر ما ز شادمانی صد در فزون گشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چونین که در فراقش بودیم بس غمین</p></div>
<div class="m2"><p>والله که از وصالش هستیم سخت شاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیوسته شاد باد که شادیم ازو همه</p></div>
<div class="m2"><p>زو خرمیم سخت که در خرمی زیاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست او چنانکه باید و چون او ز خلق نیست</p></div>
<div class="m2"><p>بادا چنانکه خواهدو بدخواه او مباد</p></div></div>