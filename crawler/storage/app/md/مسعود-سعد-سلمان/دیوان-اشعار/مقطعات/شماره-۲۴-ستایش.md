---
title: >-
    شمارهٔ ۲۴ - ستایش
---
# شمارهٔ ۲۴ - ستایش

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که باغ رادی را</p></div>
<div class="m2"><p>شاخ بأس تو فتح بار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ تیز تو در مصاف عدو</p></div>
<div class="m2"><p>شرک را تا به حشر کار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیدری صولتی و خنجر تو</p></div>
<div class="m2"><p>عادت و رسم ذوالفقار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کف بارنده مبارک تو</p></div>
<div class="m2"><p>جود را موسم بهار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده مسعود سلمان را</p></div>
<div class="m2"><p>نزد تو بخت پایدار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نبودش ز نام خود نیمی</p></div>
<div class="m2"><p>نیمی از نام خود نثار آورد</p></div></div>