---
title: >-
    شمارهٔ ۱۳ - مدیح
---
# شمارهٔ ۱۳ - مدیح

<div class="b" id="bn1"><div class="m1"><p>ای بزرگی که حسن رای تو را</p></div>
<div class="m2"><p>هر زمان بر من اصطناعی نوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر کف تو تند و پر گهرست</p></div>
<div class="m2"><p>بحر فضل تو ژرف و پر لؤلؤست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل شادت چو عقل بی زللست</p></div>
<div class="m2"><p>کف رادت چو علم بی آهوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز تو از مهتران خطاب که کرد</p></div>
<div class="m2"><p>بنده خویش را برادر و دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم رگ و پوست خواندیم شاید</p></div>
<div class="m2"><p>وین تمثل ز روی عقل نکوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآنکه چون خون و استخوان شد طبع</p></div>
<div class="m2"><p>مر مرا خدمت تو در رگ و پوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مرا جان و دل ز خدمت تو</p></div>
<div class="m2"><p>سال و مه با صفا و با نیروست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تخلف کنم ز خدمت تو</p></div>
<div class="m2"><p>که مرا اصل زندگانی اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاد پشتم ز بار رنج دو تاه</p></div>
<div class="m2"><p>گرنه در مهر تو دلم یکتوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تربیت کردیم به نظم و تو را</p></div>
<div class="m2"><p>تربیت کردن چو من کس خوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن قصیده به جنب این قطعه</p></div>
<div class="m2"><p>راست گویی که نامه مانوست</p></div></div>