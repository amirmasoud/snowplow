---
title: >-
    شمارهٔ ۱۰۶ - ثناگری
---
# شمارهٔ ۱۰۶ - ثناگری

<div class="b" id="bn1"><div class="m1"><p>به خدمت آمد فرخنده فصل فروردین</p></div>
<div class="m2"><p>مهی که تازه ازو گشت عز و دولت و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خجسته باد بدان شاه سرفراز کز او</p></div>
<div class="m2"><p>رسید رایت شاهی به اوج علیین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابوالملوک ملک ارسلان بن مسعود</p></div>
<div class="m2"><p>که شهریار زمانست و پادشاه زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایگانی شاهنشهی جهان گیری</p></div>
<div class="m2"><p>که چرخ زیر قدم کرد و ملک زیر نگین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو شاهی دلشاد زی خداوندا</p></div>
<div class="m2"><p>که بندگان تواند اختران چرخ برین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین دوازده برج سپهر و هفت اختر</p></div>
<div class="m2"><p>همه جلالت یاب و همه سعادت بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کامگاری بر دیده زمانه خرام</p></div>
<div class="m2"><p>به بختیاری بر تارک سپهر نشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان به کام و زمانه غلام و دولت رام</p></div>
<div class="m2"><p>قضا معین و سعادت قرین و بخت رهین</p></div></div>