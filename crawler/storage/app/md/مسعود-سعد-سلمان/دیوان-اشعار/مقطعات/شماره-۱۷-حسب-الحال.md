---
title: >-
    شمارهٔ ۱۷ - حسب الحال
---
# شمارهٔ ۱۷ - حسب الحال

<div class="b" id="bn1"><div class="m1"><p>مرا بس ز دیوان مرا ز خدمت</p></div>
<div class="m2"><p>خوشا روز بیکاری و وقت عطلت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر این تیغ کوه گل انبار گویی</p></div>
<div class="m2"><p>چو فغفور بر تختم و فور برکت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دولت مهیا بود مر کسی را</p></div>
<div class="m2"><p>اگر او نجوید بجویدش دولت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امامی که بر روزگارست ما را</p></div>
<div class="m2"><p>اگر او ندارد بدادمش مهلت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر دولت آید و گر نکبت آید</p></div>
<div class="m2"><p>به نزدیک من هر دو را هست آلت</p></div></div>