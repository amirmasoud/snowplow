---
title: >-
    شمارهٔ ۲۵ - ناله از حصار مرنج
---
# شمارهٔ ۲۵ - ناله از حصار مرنج

<div class="b" id="bn1"><div class="m1"><p>ای حصن مرنج وای آنکس</p></div>
<div class="m2"><p>کو چون من بر سر تو باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دیو در آن جهان که بجهد</p></div>
<div class="m2"><p>از خانه خود بر تو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور پنهان خانه ای کند مرگ</p></div>
<div class="m2"><p>در پیشگهش در تو باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مادر دوزخی بگو راست</p></div>
<div class="m2"><p>یا دوزخ مادر تو باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه نه که نه اینی و نه آنی</p></div>
<div class="m2"><p>دوزخ چو برابر تو باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مهتر مهتری مر او را</p></div>
<div class="m2"><p>او کهتر کهتر تو باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آتش تو ورا بسوزد</p></div>
<div class="m2"><p>والله که فراخور تو باشد</p></div></div>