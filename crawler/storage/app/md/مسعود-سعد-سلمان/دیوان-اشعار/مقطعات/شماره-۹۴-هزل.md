---
title: >-
    شمارهٔ ۹۴ - هزل
---
# شمارهٔ ۹۴ - هزل

<div class="b" id="bn1"><div class="m1"><p>بتی یافتم دوش گفتم به حرص</p></div>
<div class="m2"><p>که امشب جماعی فراوان کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگ من بخسبید و خفته بماند</p></div>
<div class="m2"><p>ندانستمش تا چه درمان کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتم ار چاره آن کنی</p></div>
<div class="m2"><p>که این لت شود تا در انبان کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقیقت تو را آنچه باید ز من</p></div>
<div class="m2"><p>به جای تو از مردمی آن کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفت اگر زآنکه موسی شوم</p></div>
<div class="m2"><p>عصای تو در دست ثعبان کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خواهی ز من من نه عیسی شوم</p></div>
<div class="m2"><p>که اندر چنین مرده ای جان کنم</p></div></div>