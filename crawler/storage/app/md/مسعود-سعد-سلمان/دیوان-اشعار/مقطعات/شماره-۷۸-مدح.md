---
title: >-
    شمارهٔ ۷۸ - مدح
---
# شمارهٔ ۷۸ - مدح

<div class="b" id="bn1"><div class="m1"><p>سخا زریست کز همت زند رای تو بر سنگش</p></div>
<div class="m2"><p>سخن نظمی است کز معنی دهد رای تو سامانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین اندک هنر خاطر همی امید بگسستم</p></div>
<div class="m2"><p>چو در مدح تو پیوستم هنر دیدم فراوانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا دانی که آن باید که هر کو نیک شعر آید</p></div>
<div class="m2"><p>نباشد جز به نام تو همه فهرست دیوانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حلمی کز توانایی ستاند کوه البرزش</p></div>
<div class="m2"><p>به طبعی کز قوی حالی پرستد بحر عمانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گردون خادمی داری بناز تن همی دارش</p></div>
<div class="m2"><p>چو دولت مرکبی داری به کام دل همی رانش</p></div></div>