---
title: >-
    شمارهٔ ۴ - مرثیه برای رشیدالدین
---
# شمارهٔ ۴ - مرثیه برای رشیدالدین

<div class="b" id="bn1"><div class="m1"><p>پرده از روی صفه برگیرید</p></div>
<div class="m2"><p>نوحه زار زار در گیرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن به تیمار و اندهان بدهید</p></div>
<div class="m2"><p>دل ز شادی و لهو برگیرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان نوحه نو آغازید</p></div>
<div class="m2"><p>چو به پایان رسد ز سر گیرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر عزیز مرا قیاس کنید</p></div>
<div class="m2"><p>از مه نو و شاخ برگیرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون فرو شد ستاره سحری</p></div>
<div class="m2"><p>کار ماتم هم از سحر گیرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر گذرگه اجل کمین دارد</p></div>
<div class="m2"><p>گر توان رهگذر دگر گیرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ستیز قضا بهش باشید</p></div>
<div class="m2"><p>وز گشاد بلا حذر گیرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار گردون همه هبا شمرید</p></div>
<div class="m2"><p>حال گردون همه هدر گیرید</p></div></div>
<div class="b2" id="bn9"><p>***</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای مه نو اگر تمام شدی</p></div>
<div class="m2"><p>سخت زود آفتاب بام شدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گیتی او را به جان رهین گشتی</p></div>
<div class="m2"><p>دولت او را به طوع رام شدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمده کار مرد و زن بودی</p></div>
<div class="m2"><p>عدت شغل خاص و عام شدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فضل او در جهان بگستردی</p></div>
<div class="m2"><p>جهل بر مردمان حرام شدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مایه فخر و محمدت جستی</p></div>
<div class="m2"><p>مایه جاه و احترام شدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون زدوده یکی سنان گشتی</p></div>
<div class="m2"><p>چون کشیده یکی حسام شدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به همه حکمتی یگانه شدی</p></div>
<div class="m2"><p>در همه دانشی تمام شدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناتمامت فلک ز ما بربود</p></div>
<div class="m2"><p>ای دریغا اگر تمام شدی</p></div></div>
<div class="b2" id="bn18"><p>***</p></div>
<div class="b" id="bn19"><div class="m1"><p>گر زمانه بر او دگر گشتی</p></div>
<div class="m2"><p>مایه معنی و هنر گشتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به همه مکرمت مثل بودی</p></div>
<div class="m2"><p>در همه مفخرت سمر گشتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شب فرزانگان چو روز شدی</p></div>
<div class="m2"><p>زهر آزادگان شکر گشتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شد فدای پدر که در هر حال</p></div>
<div class="m2"><p>همه گرد دل پدر گشتی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور نگشتی سر اجل به قضا</p></div>
<div class="m2"><p>پدر او را به طبع سرگشتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخت نیکو نیک خوش بودی</p></div>
<div class="m2"><p>که سر آنچنان پسر گشتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه گفتیش عمر بخشیدی</p></div>
<div class="m2"><p>اگرش عمر بیشتر گشتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک جهان حمله حمله آوردی</p></div>
<div class="m2"><p>گر اجل زو به جنگ برگشتی</p></div></div>
<div class="b2" id="bn27"><p>***</p></div>
<div class="b" id="bn28"><div class="m1"><p>ای رشید ای عزیز و شاه پدر</p></div>
<div class="m2"><p>روز و شب آفتاب و ماه پدر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای ادیب پدر دبیر پدر</p></div>
<div class="m2"><p>اعتماد پدر پناه پدر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به تو نازنده بود جان پدر</p></div>
<div class="m2"><p>از تو بالنده بود جاه پدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا نشسته پدر بر آتش توست</p></div>
<div class="m2"><p>پاره دودی شدست آه پدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ره نمای پدر رهت زده شد</p></div>
<div class="m2"><p>که نماند از پس تو راه پدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بی گناه پدر تو خواهی خواست</p></div>
<div class="m2"><p>عذر این بی عدد گناه پدر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از برای چه زیر تخته شدی</p></div>
<div class="m2"><p>وقت تخت تو بود شاه پدر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرگ اگر بستدی فدای تو بود</p></div>
<div class="m2"><p>نعمت عمر و دستگاه پدر</p></div></div>
<div class="b2" id="bn36"><p>***</p></div>
<div class="b" id="bn37"><div class="m1"><p>ای دگرگون شده به تو رایم</p></div>
<div class="m2"><p>برگذشت از نهم فلک وایم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسر آیم به سوی تربت تو</p></div>
<div class="m2"><p>زین سبب رشک می برد پایم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جز روان تو کی بود جفتم</p></div>
<div class="m2"><p>جز سر گور کی بود جایم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تخت شاهان چگونه آرایند</p></div>
<div class="m2"><p>گو تو همچنان بیارایم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به روان تو گر سر گورت</p></div>
<div class="m2"><p>جز به خون دو دیده اندایم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر زمان ماتمی بیاغازم</p></div>
<div class="m2"><p>هر نفس نوحه ای بیفزایم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به تو آسوده بودم از همه غم</p></div>
<div class="m2"><p>تو به مردی و من نیاسایم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو به زیر زمین بفرسایی</p></div>
<div class="m2"><p>من ز تیمار تو بفرسایم</p></div></div>
<div class="b2" id="bn45"><p>***</p></div>
<div class="b" id="bn46"><div class="m1"><p>ای گرامی تو را کجا جویم</p></div>
<div class="m2"><p>درد و تیمار تو کرا گویم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شدی از چشم چون مه و خورشید</p></div>
<div class="m2"><p>تیره شد بی تو خانه و کویم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر وفات تو روز و شب نالم</p></div>
<div class="m2"><p>از هلاک تو سال و مه مویم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دل به کف دو دست می مالم</p></div>
<div class="m2"><p>رخ به خون دو دیده می شویم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر چه گل همچو بوی و روی تو بود</p></div>
<div class="m2"><p>دل همی ندهدم که گل بویم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همه در آتش جگر غلطم</p></div>
<div class="m2"><p>همه در آب دیدگان پویم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>لاله لعل شد ز خون چشمم</p></div>
<div class="m2"><p>خیری خشک شد ز کف رویم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>خون بگریم ز مرگ چون تو پسر</p></div>
<div class="m2"><p>چون ببینم سپیدی مویم</p></div></div>
<div class="b2" id="bn54"><p>***</p></div>
<div class="b" id="bn55"><div class="m1"><p>تا ز پیش پدر روان کردی</p></div>
<div class="m2"><p>خو دل بر رخم روان کردی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بر رخان پدر ز خون دو چشم</p></div>
<div class="m2"><p>زعفران زیر ارغوان کردی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>همه روز پدر سیه کردی</p></div>
<div class="m2"><p>همه سود پدر زیان کردی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا به تیراجل بخستت جان</p></div>
<div class="m2"><p>تیر قد پدر کمان کردی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صورت مرگ زشت صوت را</p></div>
<div class="m2"><p>پیش چشم پدر عیان کردی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خاک بر هر سری پراکندی</p></div>
<div class="m2"><p>خون ز هر دیده ای روان کردی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کاروانی که گفته بود روان</p></div>
<div class="m2"><p>که تو آهنگ کاروان کردی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نور بودی مگر چو نور لطیف</p></div>
<div class="m2"><p>قصد خورشید آسمان کردی</p></div></div>
<div class="b2" id="bn63"><p>***</p></div>
<div class="b" id="bn64"><div class="m1"><p>مرده فرزند مادرت زارست</p></div>
<div class="m2"><p>مرگ ناگاه را خریدارست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر چه بر تو چو برگ لرزان بود</p></div>
<div class="m2"><p>چون گل اکنون ز درد بیدارست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه شب زیر پهلو و سر او</p></div>
<div class="m2"><p>بستر و بالش آتش و خارست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر ز دیده بر تو خون بارد</p></div>
<div class="m2"><p>چون تو فرزند را سزاوارست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هیچ بیکار نیست یک ساعت</p></div>
<div class="m2"><p>ماتم تو فریضه تر کارست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>باد خوشرو بر او دم مرگست</p></div>
<div class="m2"><p>روز روشن به او شب تارست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>خسته آسمان کینه کش است</p></div>
<div class="m2"><p>بسته روزگار غدارست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>گر نه از جان و عمر سیر شده ست</p></div>
<div class="m2"><p>از روان تو شاه بیزارست</p></div></div>
<div class="b2" id="bn72"><p>***</p></div>
<div class="b" id="bn73"><div class="m1"><p>هیچ دانی که حال ما چون شد</p></div>
<div class="m2"><p>تا ز قالب روانت بیرون شد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تا چو گل در چمن بپژمردی</p></div>
<div class="m2"><p>رویش از خون دیده گلگون شد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>زندگانی و جان و کار همه</p></div>
<div class="m2"><p>بر عزیزان تو دگرگون شد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>هر که بود از نشاط مفلس گشت</p></div>
<div class="m2"><p>گر چه از آب دیده قارون شد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>مغزها از وفات تو بگداخت</p></div>
<div class="m2"><p>دیده ها در غم تو جیحون شد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>حسرتا کان تن سرشته ز جان</p></div>
<div class="m2"><p>صید گردون ناکس دون شد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ای دریغا که آن روان لطیف</p></div>
<div class="m2"><p>طعمه روزگار وارون شد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وای و دردا که آن دل روشن</p></div>
<div class="m2"><p>خون شد و دیده ها پر از خون شد</p></div></div>
<div class="b2" id="bn81"><p>***</p></div>
<div class="b" id="bn82"><div class="m1"><p>بندگان تو زار و گریانند</p></div>
<div class="m2"><p>زار هر ساعتی تو را خوانند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چفته بالا و خسته رخسارند</p></div>
<div class="m2"><p>کوفته مغز و سوخته جانند</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>تا شبیخون زده ست بر تو اجل</p></div>
<div class="m2"><p>همه از دیده خون همی رانند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هر زمان از برای خرسندی</p></div>
<div class="m2"><p>خاک گور تو بر سر افشانند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>زانکه عمر تو بیشتر دیدند</p></div>
<div class="m2"><p>همه از عمرها پشیمانند</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>از دل اندر میان صاعقه اند</p></div>
<div class="m2"><p>وز دو دیده میان طوفانند</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هر زمانی به رسم منصب خویش</p></div>
<div class="m2"><p>زی تو آیند و دید نتوانند</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>راست گویی که در مصیبت تو</p></div>
<div class="m2"><p>همه مسعود سعد سلمانند</p></div></div>
<div class="b2" id="bn90"><p>***</p></div>
<div class="b" id="bn91"><div class="m1"><p>غم تو بر دلم مگر نیش است</p></div>
<div class="m2"><p>که همه ساله در عنا ریش است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>غم تو من کشم که مسعودم</p></div>
<div class="m2"><p>که به جان غم کشیدنم کیش است</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>موی بر فرق گوئیم تیغست</p></div>
<div class="m2"><p>مژه بر دیده گوئیم نیش است</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>گر همی خون رود ز دیده من</p></div>
<div class="m2"><p>نه شگفت است زانکه دل ریش است</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>از سیاهی و تیرگی روزم</p></div>
<div class="m2"><p>همچو اندیشه بداندیش است</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>این تن و جان زار پژمرده</p></div>
<div class="m2"><p>تن بیمار و جان درویش است</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>من بدینگونه ام که خویش نیم</p></div>
<div class="m2"><p>چه بود آنکه او تو را خویش است</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>مکنید این همه خروش و نفیر</p></div>
<div class="m2"><p>که همه خلق را همین پیش است</p></div></div>
<div class="b2" id="bn99"><p>***</p></div>
<div class="b" id="bn100"><div class="m1"><p>ای فلک سخت نابسامانی</p></div>
<div class="m2"><p>کژ رو و باژگونه دورانی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>محنت عقل و شدت صبری</p></div>
<div class="m2"><p>فتنه جسم و آفت جانی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>مار نیشی و شیر چنگالی</p></div>
<div class="m2"><p>خیره چشمی و تیز دندانی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بدهی و آنگهی نیارامی</p></div>
<div class="m2"><p>تا همه داده باز نستانی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>زود بیند ز تو دل آزاری</p></div>
<div class="m2"><p>هر که یابد ز تو تن آسانی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بشکنی زود هر چه راست کنی</p></div>
<div class="m2"><p>بر کنی باز هر چه بنشانی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>هر چه کردی همه تباه کنی</p></div>
<div class="m2"><p>مگر از کرده ها پشیمانی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نکنم سرزنش که مجبوری</p></div>
<div class="m2"><p>بسته حکم و امر یزدانی</p></div></div>
<div class="b2" id="bn108"><p>***</p></div>
<div class="b" id="bn109"><div class="m1"><p>تو رشید ای سرخداوندان</p></div>
<div class="m2"><p>اصل نیکان و نیک پیوندان</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>آن کشیدی ز غم کجا هرگز</p></div>
<div class="m2"><p>نکشیدی ز خاره و سندان</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ره جز این نیست عاقبت گر ما</p></div>
<div class="m2"><p>بندگانیم یا خداوندان</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>آسمانیست آتشین چنگال</p></div>
<div class="m2"><p>روزگاریست آهنین دندان</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>گر چه هست آن عزیز اندک عمر</p></div>
<div class="m2"><p>به حقیقت سزای صد چندان</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بر گذشته چنین جزع کردن</p></div>
<div class="m2"><p>نشمردند از خرد خردمندان</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>در رضا و ثواب ایزد کوش</p></div>
<div class="m2"><p>گر چه صعب است درد فرزندان</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>مهر من نیستی اگر نه امی</p></div>
<div class="m2"><p>خسته بند و بسته زندان</p></div></div>