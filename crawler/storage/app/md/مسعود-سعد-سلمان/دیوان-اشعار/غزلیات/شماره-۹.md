---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>ای گشته دل من به هوای تو گرفتار</p></div>
<div class="m2"><p>دل بر تو زیان کرد چه سودست ز گفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غم دل جوشان مرا بار گران کرد</p></div>
<div class="m2"><p>آن عنبر پر جوش بر آن اشهب پر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نرگس بیمار تو بر خواب چو نرگس</p></div>
<div class="m2"><p>چشمم همه شب در غم بیمار تو بیدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو سخت جفاکاری و من نیک وفاجو</p></div>
<div class="m2"><p>من سخت کم آزارم و تو نیک دل آزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند که من بیش کنم پیش تو زاری</p></div>
<div class="m2"><p>تو بیش رمی از من دلسوخته زار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منمای مرا رنج و مکن بر تن من جور</p></div>
<div class="m2"><p>کز جور تو و رنج تو تن گشت گرانبار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد که من از جور تو در پیش شهنشه</p></div>
<div class="m2"><p>جامه بدرم روز مظالم به گه بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاج ملکان خسرو مسعود براهیم</p></div>
<div class="m2"><p>سلطان جهانبخش جهانگیر جهاندار</p></div></div>