---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>در دل چو خیره خیره کند عشق خار خار</p></div>
<div class="m2"><p>با رنج دیر دیر کند صبر دار دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تن خزد ز بویه وصل تو مور مور</p></div>
<div class="m2"><p>در من جهد ز انده هجر تو مار مار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر در کشم به جامه در از شرم زیر زیر</p></div>
<div class="m2"><p>گریم ز فرقت تو دل آزار زار زار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دیده ام چو اشک زند یار تیر تیر</p></div>
<div class="m2"><p>پیچان شوم چنانکه کنم جامه تار تار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آویزدم نظر نظر اندر مژه مژه</p></div>
<div class="m2"><p>از دانه دانه لؤلؤ دیده چو هار هار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی برآزماییم ای دوست نیک نیک</p></div>
<div class="m2"><p>تا چند برگراییم ای یار بار بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل گل فتاده بر دو رخ من رده رده</p></div>
<div class="m2"><p>تا تازه تازه در جگرم خست خار خار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم کم خورم که هست زیانکار خیر خیر</p></div>
<div class="m2"><p>دل خوش کنم که هست جفاکار یار یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از راهها که هست مخوفست راه راه</p></div>
<div class="m2"><p>وز کارها که هست نه خوبست کار کار</p></div></div>