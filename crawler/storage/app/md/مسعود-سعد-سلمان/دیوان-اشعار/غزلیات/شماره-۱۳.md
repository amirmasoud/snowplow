---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>در بزم پادشا نگر این کاروبار گل</p></div>
<div class="m2"><p>وین باده بین شده به طرب دستیار گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل چند ماه منتظر بزم شاه بود</p></div>
<div class="m2"><p>وز بهر آن دراز کشید انتظار گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدار گل شده ست همه اختیار خلق</p></div>
<div class="m2"><p>تا بزم شاه ساخت همه اختیار گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلبن ملونست چو دیبای هفت رنگ</p></div>
<div class="m2"><p>تا لعل سبز گشت شعار و دثار گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا با می کهن گل نو سازوار شد</p></div>
<div class="m2"><p>گل پیشوای می شد و می پیشکار گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزم تو گل است در آمیخته به هم</p></div>
<div class="m2"><p>با هم نثار زر بود و هم نثار گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیزد گل از نشاط که پر زر ساده شد</p></div>
<div class="m2"><p>همچون کنار سایل خسرو کنار گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فخر و شرف نبینی جز در شمار شاه</p></div>
<div class="m2"><p>لهو و طرب نبینی جز در شمار گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاها همه ز شادی بزم رفیع توست</p></div>
<div class="m2"><p>این سرخ رویی گل و این افتخار گل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از روزگار گل دل و جان شاد و خرمست</p></div>
<div class="m2"><p>یارب چه روزگارست این روزگار گل</p></div></div>