---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>آمد آهسته با کرشمه و ناز</p></div>
<div class="m2"><p>دوش نزد من آن نگار طراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف پرپیچ بر شکست به گل</p></div>
<div class="m2"><p>چشم پر خواب سرمه کرده به ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نهاده بر ابروان چوگان</p></div>
<div class="m2"><p>تیر غمزه به چشم تیر انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش چون روی به نومیدی</p></div>
<div class="m2"><p>چنگ مانند ناز کرد آغاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نیازی مرا نیاز به توست</p></div>
<div class="m2"><p>ورچه دارد به من زمانه نیاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چو پرداختم به مهر تو دل</p></div>
<div class="m2"><p>تو زمانی به وصل من پرداز</p></div></div>