---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دیده گر در فراق خون بارد</p></div>
<div class="m2"><p>حق او هم تمام نگزارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غمش هیچ بر نیارم دم</p></div>
<div class="m2"><p>گر جهان بر سرم فرود آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وفا داشتنش جان بدهم</p></div>
<div class="m2"><p>تا مرا بی وفا نپندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزر و مانی ار شود زنده</p></div>
<div class="m2"><p>هر یکی خواهدش که بنگارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این به رنده چو او نپردازد</p></div>
<div class="m2"><p>وآن به خامه چو او نبگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی او همچو گل همی خندد</p></div>
<div class="m2"><p>چشم من همچو ابر می بارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشمرد نیم ذره جرم رهی</p></div>
<div class="m2"><p>چونکه روز فراق نشمارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا دل او مرا نمی خواهد</p></div>
<div class="m2"><p>یا به من آمدن نمی یارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفت و ترسم که او به نادانی</p></div>
<div class="m2"><p>به کسی دل به مهر بسپارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه شب در هوس همی باشم</p></div>
<div class="m2"><p>که نباید که عهد بگذرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در همه گر کبوتری بینم</p></div>
<div class="m2"><p>گویم از دوست نامه ای آرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باد اگر گرد بام من بوزد</p></div>
<div class="m2"><p>گویم از یار مژده ای دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر کجا هست شاد باد بدانک</p></div>
<div class="m2"><p>از من دلشده به یاد آرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا در غم فرقتت ای پسر</p></div>
<div class="m2"><p>دو دیده چو ابرست و دامن شمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وزین دل برافروخته ست آتشی</p></div>
<div class="m2"><p>کش از درد و رنجست دود و شرر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو چشمم بمانده به هنجار راه</p></div>
<div class="m2"><p>دو گوشم بمانده به آواز در</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امید وصال ار نبودی مرا</p></div>
<div class="m2"><p>که روزی درآیی ز در ای پسر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پر از گرد جعد و برآشفته زلف</p></div>
<div class="m2"><p>گشاده خوی از روی و بسته کمر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر آوردمی جان شیرین ز تن</p></div>
<div class="m2"><p>بیالودمی چشم روشن ز سر</p></div></div>