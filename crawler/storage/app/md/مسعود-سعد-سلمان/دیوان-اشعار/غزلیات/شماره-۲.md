---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گفتم که چند صبر کنم ای نگار؟ گفت:</p></div>
<div class="m2"><p>تا هست عمر، گفتم: رنجه مدار، گفت:</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی رنجْ عشق نبوَد، گفتم: نیم به رنج،</p></div>
<div class="m2"><p>فرسوده چند باشد ازین ای نگار؟ گفت:</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز انتظار روی ندارد تو را همی</p></div>
<div class="m2"><p>گفتم: شدم هلاک من از انتظار، گفت:</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این روزگار با تو بد است، این ازو شناس</p></div>
<div class="m2"><p>گفتم که نیک کِی شودم روزگار؟ گفت:</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون گشت زایل این سَخَطِ شهریار راد</p></div>
<div class="m2"><p>گفتم که کِی شود سَخَطِ شهریار؟ گفت:</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بخت رام گردد تا تو رسی به کام</p></div>
<div class="m2"><p>گفتم که بخت کِی شودم جفت و یار؟ گفت:</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمرزشی بخواه شود عفوْ جرمِ تو</p></div>
<div class="m2"><p>این گفت در کریمِ نبی کردگار گفت</p></div></div>