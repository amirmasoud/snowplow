---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>بدرود همی کرد مرا آن صنم من</p></div>
<div class="m2"><p>گریان و درآورده مرادست به گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زخم دو کف همچو دلش کردم سینه</p></div>
<div class="m2"><p>ور آب دو دیده چو برش کردم دامن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنجور شد از بهر من و روی دژم کرد</p></div>
<div class="m2"><p>کز حسرت آن روی دم سرد زدم من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رویش اثر کرده دم سرد من امروز</p></div>
<div class="m2"><p>چونان که دم گرم در آیینه روشن</p></div></div>