---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>طعنه زنی که یار کنم دیگر</p></div>
<div class="m2"><p>طعنه مزن که من نکنم باور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جان و دل ز بهر مرا خواهی</p></div>
<div class="m2"><p>من از دل تو آگهم ای دلبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان و جهان من به تو خوش باشد</p></div>
<div class="m2"><p>ای روی تو ز جان و جهان خوشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طیره گشته از رخ تو لاله</p></div>
<div class="m2"><p>وی شرم خورده از لب تو شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاد آن زمان شوم که تو را بینم</p></div>
<div class="m2"><p>تابان چو ماه و نازان چون عرعر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگشایی آن دو بسد پر لؤلؤ</p></div>
<div class="m2"><p>بفشانی آن دو چنبر پر عنبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاهی ربایم از لب تو بوسه</p></div>
<div class="m2"><p>گاهی ستانم از کف تو ساغر</p></div></div>