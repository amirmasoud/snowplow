---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای نگارین چون تو از خوبان کجاست</p></div>
<div class="m2"><p>نیست کس را آنچه از گیتی تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد و روی و زلف سرو و ماه مشک</p></div>
<div class="m2"><p>مشک پیچان ماه تابان سرو راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مرا مهر تو اندر دل نشست</p></div>
<div class="m2"><p>از دل من بیش مهر کس نخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای نگار از طاعت تو چاره نیست</p></div>
<div class="m2"><p>راست گویی خدمت خسرو علاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه مسعود آفتاب داد و دین</p></div>
<div class="m2"><p>آنکه بر شاهان گیتی پادشاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نهیبش ماه با رخسار زرد</p></div>
<div class="m2"><p>وز شکوهش چرخ با پشت دوتاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسروان را آب حوضش زمزم است</p></div>
<div class="m2"><p>سرکشان را خاک قصرش کیمیاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه گردون همت گردون محل</p></div>
<div class="m2"><p>خسرو دریا دل دریا عطاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بقا و عز و دولت شاد باد</p></div>
<div class="m2"><p>تا به گیتی دولت و عز و بقاست</p></div></div>