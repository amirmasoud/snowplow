---
title: >-
    قصیدهٔ شمارهٔ ۲۵
---
# قصیدهٔ شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>بر عمر خویش گریم یا بر وفات تو؟</p></div>
<div class="m2"><p>واکنون صفات خویش کنم یا صفات تو؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی و هست بر جا از تو ثنای خوب</p></div>
<div class="m2"><p>مردی و زنده ماند ز تو مکرمات تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدی قضای مرگ و برون رفتی از جهان</p></div>
<div class="m2"><p>نادیده چهرهٔ تو بنین و بنات تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلقی یتیم گشت و جهانی اسیر شد</p></div>
<div class="m2"><p>زین در میان حسرت و غربت ممات تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بسته بود بر تو در خانهٔ تو بود</p></div>
<div class="m2"><p>بر هر کسی گشاده طریق صلات تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو ناامید گشتی از عمر خویشتن</p></div>
<div class="m2"><p>نومید شد به هرجا از تو عفات تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالد همی به زاری و گرید همی به درد</p></div>
<div class="m2"><p>آن کس که یافتی صدقات و زکات تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر هیچکس نماند که رحمت نکرده‌ای</p></div>
<div class="m2"><p>کز رحمت آفرید خداوند ذات تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مانا که پیش خواست ترا کردگار از آنک</p></div>
<div class="m2"><p>شادی نبود هیچ ترا از حیات تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون جگر ز دیده برون افکند همی</p></div>
<div class="m2"><p>مسکین برادر تو سعید از وفات تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوید که با که گویم اکنون غمان دل</p></div>
<div class="m2"><p>وز که کنون همی شنوم من نکات تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندوه من به روی تو بودی گسارده</p></div>
<div class="m2"><p>و آرام یافتی دل من از عظات تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان همچو خون دیده ز دیده براندمی</p></div>
<div class="m2"><p>گر هیچ سود کردی و بودی نجات تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از مرگ تو به شهر خبر چون کنم که نیست</p></div>
<div class="m2"><p>دشمن‌ترین خلق جهان جز نعات تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایزد عطا دهادت دیدار خویشتن</p></div>
<div class="m2"><p>یکسر کناد عفو همه سیئات تو</p></div></div>