---
title: >-
    قصیدهٔ شمارهٔ ۳
---
# قصیدهٔ شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>به نظم و نثر کسی را گر افتخار سزاست</p></div>
<div class="m2"><p>مرا سزاست که امروز نظم و نثر مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هیچ وقت مرا نظم و نثر کم نشود</p></div>
<div class="m2"><p>که نظم و نثرم در است و طبع من دریاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لطف آ ب روان است طبع من لیکن</p></div>
<div class="m2"><p>به گاه قوت و کثرت چو آتش است و هواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چه همچو گیا نزد هر کسی خوارم</p></div>
<div class="m2"><p>وگرچه همچو صدف غرق گشته تن بی‌کاست،</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب مدار زمن نظم خوب و نثر بدیع</p></div>
<div class="m2"><p>نه لل از صدف است و نه انگبین ز گیاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نزد خصمان گر فضل من نهان باشد</p></div>
<div class="m2"><p>زیان ندارد، نزدیک عاقلان پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شگفت نیست اگر شعر من نمی‌دانند</p></div>
<div class="m2"><p>که طبع ایشان پست است و شعر من والاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چشم جد و حقیقت مرا نمی‌بینند؟</p></div>
<div class="m2"><p>که نزد عقل مرا رتبت و شرف به کجاست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر چه چشمهٔ خورشید روشن است و بلند</p></div>
<div class="m2"><p>چگونه بیند آن کش دو چشم نابیناست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هیچ وجه گناهی دگر نمی‌دانند</p></div>
<div class="m2"><p>جز آن که ما را زین شهر مولد و منشاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر برایشان سحر حلال بر خوانم</p></div>
<div class="m2"><p>جز این نگویند آخر که کودک و برناست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز کودکی و ز پیری چه عار و فخر آید</p></div>
<div class="m2"><p>چنین نگوید آن کس که عاقل و داناست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار پیر شناسم که منکر و گبر است</p></div>
<div class="m2"><p>هزار کودک دانم که زاهد الزهداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر عمید نیم یا عمیدزاده نیم</p></div>
<div class="m2"><p>ستوده نسبت و اصلم ز دودهٔ فضلاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر به زهد بنازد کسی روا باشد</p></div>
<div class="m2"><p>ور افتخار کند فاضلی به فضل سزاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به اصل تنها کس را مفاخرت نرسد</p></div>
<div class="m2"><p>که نسبت همه از آدم است و از حواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا به نیستی ای سیدی چه طعنه زنی</p></div>
<div class="m2"><p>چو هست دانشم، از زر و سیم نیست رواست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خطاست گویی در نیستی سخا کردن</p></div>
<div class="m2"><p>ملامت تو چه سودم کند که طبع، سخاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بخل و جود کم و بیش کی شود روزی</p></div>
<div class="m2"><p>خطا گرفتن بر من بدین طریق خطاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر به نیک و بد من میان ببندد خلق</p></div>
<div class="m2"><p>جز آن نباشد بر من که از خدای قضاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بس بلا که بدیدم چنان شدم به مثل</p></div>
<div class="m2"><p>که گر سعادت بینم گمان برم که بلاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو حال و قصهٔ من دان که حال و قصهٔ من</p></div>
<div class="m2"><p>بسی شگفت‌تر از حال وامق و عذراست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگرچه بر سرم آتش ببارد از گردون</p></div>
<div class="m2"><p>ز حال خود نشوم، اعتقاد دارم راست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهر بر آن کس پاشم که در خور گهر است</p></div>
<div class="m2"><p>ثنا مر او را گویم که او سزای ثناست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امیر غازی محمود سیف دولت و دین</p></div>
<div class="m2"><p>که پادشاه بزرگ است و خسرو والاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خجسته نامش بر شعرهای نادر من</p></div>
<div class="m2"><p>چو مهر بر درم است و چو نقش بر دیباست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدین قصیده که گفتم من اقتدا کردم</p></div>
<div class="m2"><p>به اوستاد لبیبی که سیدالشعراست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر آن طریق بنا کردم این قصیده که گفت:</p></div>
<div class="m2"><p>« سخن که نظم دهند آن درست باید وراست»</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قصیده خرد ولیکن به قدر و فضل بزرگ</p></div>
<div class="m2"><p>به لفظ موجز و معنیش باز مستوفاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر آن که داند داند یقین که هر بیتی</p></div>
<div class="m2"><p>از این قصیدهٔ من یک قصیدهٔ غراست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنین قصیده ز مسعود سعد سلمان خواه</p></div>
<div class="m2"><p>چنین قصاید مسعود سعد سلمان راست</p></div></div>