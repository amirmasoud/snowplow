---
title: >-
    قصیدهٔ شمارهٔ ۱۵ - دوشم شبی گذشت چه گویم چگونه بود؟
---
# قصیدهٔ شمارهٔ ۱۵ - دوشم شبی گذشت چه گویم چگونه بود؟

<div class="b" id="bn1"><div class="m1"><p>عمرم همی قصیر کند این شب طویل</p></div>
<div class="m2"><p>وز انده کثیر شد این عمر من قلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشم شبی گذشت چه گویم چگونه بود؟</p></div>
<div class="m2"><p>همچون نیاز تیره و همچون امل طویل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کف‌الخضیب داشت فلک ورنه گفتمی</p></div>
<div class="m2"><p>بر سوک مهر جامه فرو زد مگر به نیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ساکنی چرخ و سیاهی شب مرا</p></div>
<div class="m2"><p>طبع از شگفت خیره و چشم از نظر کلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم زمین ندارد اعراض مختلف</p></div>
<div class="m2"><p>گفتم هوا ندارد ارکان مستحیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمم مسیل بود ز اشکم شب دراز</p></div>
<div class="m2"><p>مردم در او نخفت و نحسبند در مسیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این دیده گر به لؤلؤ رادست در جهان</p></div>
<div class="m2"><p>با او چرا به خوابی باشد فلک بخیل؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز از وصال هجر درآبم بود مقام</p></div>
<div class="m2"><p>شب از فراق وصل در آتش کنم مقیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون مور و پشه‌ام به ضعیفی چرا کشد</p></div>
<div class="m2"><p>گردون به سلسله در، پایم چو شیر و پیل؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنده خیال دوست همی داردم چنین</p></div>
<div class="m2"><p>کاید همی به من شب تار از دویست میل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه بگذرد ز آب دو چشمم کلیم‌وار</p></div>
<div class="m2"><p>گه در شود در آتش دل راست چون خلیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه سوخته در آتش و نه غرقه اندر آب</p></div>
<div class="m2"><p>گویی که هست بر تن او پر جبرئیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زردست و سرخ دو رخ و دیده مرا به عشق</p></div>
<div class="m2"><p>زان دو رخ منقش وزان دیدهٔ کحیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون نوحه‌ای برآرم یا ناله‌ای کنم</p></div>
<div class="m2"><p>داودوار کوه بود مر مرا رسیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او را شناسم از همه خوبان اگر فلک</p></div>
<div class="m2"><p>در آتشم نهد که نیارم بر او بدیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا کی دلم ز تیر حوادث شود جریح</p></div>
<div class="m2"><p>تا کی تنم ز رنج زمانه بود علیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هرگز چو من نگیرد چنگ قضا شکار</p></div>
<div class="m2"><p>هرگز چو من نیابد تیغ بلا قتیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یک چشم در سعادت نگشاد بخت من</p></div>
<div class="m2"><p>کش در زمان نه دست قضا درکشید میل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه نه به محنت اندرم آن حال تازه شد</p></div>
<div class="m2"><p>کان سوی هر سعادت و دولت بود دلیل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پدرام و رام کرد مرا روزگار و بخت</p></div>
<div class="m2"><p>خواجه رئیس سید ابوالفتح بی‌عدیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن در هنر یگانه و آن در خرد تمام</p></div>
<div class="m2"><p>آن در سخا مقدم و آن در نسب اصیل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>افعال او گزیده و آثار او بلند</p></div>
<div class="m2"><p>اخلاق او مهدب و اقوال او جمیل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای درگه تو قبله خواهندگان شده</p></div>
<div class="m2"><p>کرد ایزدت به روزی خلقان مگر کفیل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هرگز نگشت خواهی روزی ز مکرمت</p></div>
<div class="m2"><p>زیرا که تو به مکرمت اندرنیی بخیل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>محکم‌ترست حزم تو از کوه بیستون</p></div>
<div class="m2"><p>صافی‌ترست عزم تو از خنجر صقیل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طبع تو در زمستان باغی بود خرم</p></div>
<div class="m2"><p>فر تو در حزیران ظلی بود ظلیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جز بهر خدمت تو نبندم میان به جهد</p></div>
<div class="m2"><p>روزی اگر گشاده شود پیش من سبیل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر مرکب هوای تو در راه اشتیاق</p></div>
<div class="m2"><p>سوی تو بر دو دیدهٔ روشن کنم رحیل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنم که دست دهر نیابد مرا ضعیف</p></div>
<div class="m2"><p>آنم که چشم چرخ نبیند مرا ذلیل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هرگز به چشم خفت در من مکن نگاه</p></div>
<div class="m2"><p>ور چند بر دو پایم بندی است بس ثقیل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گوشم بدان بود که سلامم کنی به مهر</p></div>
<div class="m2"><p>چشمم بدان بود که عطایم دهی جزیل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا دیدگان و تا دل و جان است مر مرا</p></div>
<div class="m2"><p>باشم ترا به جان و دل و دیدگان خلیل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا چرخ را مدار بود خاک را قرار</p></div>
<div class="m2"><p>تا کلک را صریر بود تیغ را صلیل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بادت بزرگیی به همه نعمتی مضاف</p></div>
<div class="m2"><p>بادت سعادتی به همه دولتی کفیل</p></div></div>