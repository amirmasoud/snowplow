---
title: >-
    قصیدهٔ شمارهٔ ۲ - آن راست‌گو خروس مجرب
---
# قصیدهٔ شمارهٔ ۲ - آن راست‌گو خروس مجرب

<div class="b" id="bn1"><div class="m1"><p>شد مشک شب چو عنبر اشهب</p></div>
<div class="m2"><p>شد در شبه عقیق مرکب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان بیم کافتاب زند تیغ</p></div>
<div class="m2"><p>لرزان شده به گردون کوکب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را به صبح مژده همی داد</p></div>
<div class="m2"><p>آن راست گو خروس مجرب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌زد دو بال خود را برهم</p></div>
<div class="m2"><p>از چیست آن؟ ندانم یارب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست از نشاط آمدن روز</p></div>
<div class="m2"><p>یا از تاسف شدن شب؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ماهروی سلسله زلفین</p></div>
<div class="m2"><p>و ای نوش لب سیمین غبغب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش من آر باده از آن روی</p></div>
<div class="m2"><p>نزد من آر بوی از آن لب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل را نکرد باید معذور</p></div>
<div class="m2"><p>تن را نداشت باید متعب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دولت و سعادت صاحب</p></div>
<div class="m2"><p>که آداب از او شده است مهذب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منصور بن سعید بن احمد</p></div>
<div class="m2"><p>کش بنده‌اند حران اغلب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن کو عمید رفت ز خانه</p></div>
<div class="m2"><p>و آن کو ادیب رفت به مکتب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در فضل بی‌نظیر و نه مغرور</p></div>
<div class="m2"><p>در اصل بی‌قرین و نه معجب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از رای اوست چشمهٔ خورشید</p></div>
<div class="m2"><p>وز خلق اوست عنبر اشهب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نزدیک کردگار، مکرم</p></div>
<div class="m2"><p>در پیش شهریار، مقرب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در هر زبان به دانش ممدوح</p></div>
<div class="m2"><p>در هر دلی به جود محبب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای در اصول فضل مقدم</p></div>
<div class="m2"><p>و ای در فنون علم مدرب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تقصیر اگر فتاد به خدمت</p></div>
<div class="m2"><p>من بنده را مدار معاتب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که آمد همی رهی را یک چند</p></div>
<div class="m2"><p>دور از جمال ملجس تو تب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا بر زمین بروید نسرین</p></div>
<div class="m2"><p>تا بر فلک برآید عقرب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جاه تو باد میمون طالع</p></div>
<div class="m2"><p>جان تو باد عالی مرقب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مجلست ز نزهت، مفرش</p></div>
<div class="m2"><p>بر آخورت ز دولت، مرکب</p></div></div>