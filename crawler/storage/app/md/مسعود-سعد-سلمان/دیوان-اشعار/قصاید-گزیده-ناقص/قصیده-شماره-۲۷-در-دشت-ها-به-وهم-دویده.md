---
title: >-
    قصیدهٔ شمارهٔ ۲۷ - در دشت‌ها به وهم دویده
---
# قصیدهٔ شمارهٔ ۲۷ - در دشت‌ها به وهم دویده

<div class="b" id="bn1"><div class="m1"><p>ای سرد و گرم چرخ کشیده</p></div>
<div class="m2"><p>شیرین و تلخ دهر چشیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر هزار بادیه گشته</p></div>
<div class="m2"><p>بر تو هزار باد وزیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌حد بنای آز کآشفته</p></div>
<div class="m2"><p>بی‌مر لباس صبر دریده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چند کارزار فتاده</p></div>
<div class="m2"><p>در چند مرغزار چریده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اقلیم‌ها به نام سپرده</p></div>
<div class="m2"><p>در دشت‌ها به وهم دویده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بحرها چو ابر گذشته</p></div>
<div class="m2"><p>در دشت‌ها چو باد تنیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سمج‌های حبس نشسته</p></div>
<div class="m2"><p>با حلقه‌های بند خمیده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌بیم در حوادث جسته</p></div>
<div class="m2"><p>بی‌باک با سپهر چخیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندوه، بوتهٔ تو نهاده</p></div>
<div class="m2"><p>اندیشه، آتش تو دمیده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردون ترا عیار گرفته</p></div>
<div class="m2"><p>یک ذره بر تو بار ندیده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اعجاز گفتهٔ تو ستوده</p></div>
<div class="m2"><p>انصاف کردهٔ تو گزیده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سحر آمده به رغبت و اشعار</p></div>
<div class="m2"><p>از تو به گوش حرص شنیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باغی است خاطر تو شکفته</p></div>
<div class="m2"><p>شاخی است فکرت تو دمیده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر کس بری ز شاخ تو برده</p></div>
<div class="m2"><p>هر کس گلی ز باغ تو چیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وین سر بریده خامهٔ بی حبر</p></div>
<div class="m2"><p>رزق تو از تو بازبریده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>افزون نمی‌کند ز لباده</p></div>
<div class="m2"><p>برتر نمی‌شود ز ولیده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وان کسوتی که بختت رشته است</p></div>
<div class="m2"><p>نابافته است و نیم تنیده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا چند بود خواهی بی‌جرم</p></div>
<div class="m2"><p>در کنج این خراب خزیده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چهره ز زخم درد شکسته</p></div>
<div class="m2"><p>قامت ز رنج بار خمیده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لرزان به تن چو دیو گرفته</p></div>
<div class="m2"><p>پیچان به جان چو مار گزیده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جان از تن تو چست گسسته</p></div>
<div class="m2"><p>هوش از دل تو پاک رمیده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چشمت ز گریه جوی گشاده</p></div>
<div class="m2"><p>جسمت به گونه زر کشیده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ادبار در دم تو نشسته</p></div>
<div class="m2"><p>افلاس بر سر تو رسیده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه پی به گام راست نهاده</p></div>
<div class="m2"><p>نه می به کام خویش مزیده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اشک دو دیده روی تو کرده</p></div>
<div class="m2"><p>نار چهار شاخ کفیده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گویی که دانه دانهٔ لعل است</p></div>
<div class="m2"><p>زو قطره قطره خون چکیده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در چشم تو امید گلی را</p></div>
<div class="m2"><p>صد خار انتظار خلیده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از بهر خوشه‌ای را بسیار</p></div>
<div class="m2"><p>بر خویشتن چو نال نویده</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شمشیر سطوت تو زده زنگ</p></div>
<div class="m2"><p>شیر عزیمت تو شمیده</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پر طراوت تو شکسته</p></div>
<div class="m2"><p>روز جوانی تو پریده</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر مایه سود کرد چه داری؟</p></div>
<div class="m2"><p>ای تجربت به عمر خزیده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حق تو می‌نبیند بینی</p></div>
<div class="m2"><p>این سرنگون به چندین دیده؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حال تو بی‌حلاوت و بیرنگ</p></div>
<div class="m2"><p>مانند میوه‌ای است مکیده</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هم روزی آخرش برساند</p></div>
<div class="m2"><p>ایزد بدانچه هست سزیده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مسعود سعد چند لیی ژاژ</p></div>
<div class="m2"><p>چه فایده ز ژاژ لییده</p></div></div>