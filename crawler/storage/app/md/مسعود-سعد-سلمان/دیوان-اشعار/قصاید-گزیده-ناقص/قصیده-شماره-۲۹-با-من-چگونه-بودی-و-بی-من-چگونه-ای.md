---
title: >-
    قصیدهٔ شمارهٔ ۲۹ - با من چگونه بودی و بی من چگونه‌ای؟
---
# قصیدهٔ شمارهٔ ۲۹ - با من چگونه بودی و بی من چگونه‌ای؟

<div class="b" id="bn1"><div class="m1"><p>ای لاوهور ویحک بی من چگونه‌ای</p></div>
<div class="m2"><p>بی‌آفتاب روشن، روشن چگونه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باغ طبع نظم من آراسته ترا</p></div>
<div class="m2"><p>بی‌لاله و بنفشه و سوسن چگونه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناگه عزیز فرزند از تو جدا شده است</p></div>
<div class="m2"><p>با درد او به نوحه و شیون چگونه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر پای من دو بند گران است چون تنی</p></div>
<div class="m2"><p>بیجان شده، تو اکنون بی‌تن چگونه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفرستیم پیام و نگویی به حسن عهد:</p></div>
<div class="m2"><p>«کاندر حصار بسته چو بیژن چگونه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر در حضیض برکشدت باژگونه بخت</p></div>
<div class="m2"><p>از اوج برفراخته گردن چگونه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای تیغ اگر نیام به حیلت نخواستی</p></div>
<div class="m2"><p>در درکه‌ای برهنه چو سوزن چگونه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هیچ حمله هرگز نفکنده‌ای سپر</p></div>
<div class="m2"><p>با حملهٔ زمانهٔ توسن چگونه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد ترا ز دوست یکایک تهی کنار</p></div>
<div class="m2"><p>با دشمن نهفته به دامن چگونه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از زهر مار و تیزی آهن بود هلاک</p></div>
<div class="m2"><p>با مار حلقه گشته ز آهن چگونه‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دوستان ناصح مشفق جدا شدی</p></div>
<div class="m2"><p>با دشمنان ناکس ریمن چگونه‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در باغ نوشکفته نرفتی همی به گرد</p></div>
<div class="m2"><p>در نیم رفته دمگه گلخن چگونه‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آباد جای نعمت نامد ترا به چشم</p></div>
<div class="m2"><p>محنت زده به ویران معدن چگونه‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای بوده بام و روزن تو چرخ و آفتاب</p></div>
<div class="m2"><p>در سمج تنگ بی‌در و روزن چگونه‌ای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای جره باز دشت گذار شکار دوست</p></div>
<div class="m2"><p>بسته میان تنگ نشیمن چگونه‌ای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با ناز دوست هرگز طاقت نداشتی</p></div>
<div class="m2"><p>امروز با شماتت دشمن چگونه‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای دم گرفته زندان گشته مقام تو</p></div>
<div class="m2"><p>بی‌دل گشاده طارم و گلشن چگونه‌ای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من مرغزار بودم و تو شیر مرغزار</p></div>
<div class="m2"><p>با من چگونه بودی و بی‌من چگونه‌ای»</p></div></div>