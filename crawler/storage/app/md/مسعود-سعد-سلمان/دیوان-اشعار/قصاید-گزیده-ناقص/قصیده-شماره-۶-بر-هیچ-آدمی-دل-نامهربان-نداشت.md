---
title: >-
    قصیدهٔ شمارهٔ ۶ - بر هیچ آدمی دل نامهربان نداشت
---
# قصیدهٔ شمارهٔ ۶ - بر هیچ آدمی دل نامهربان نداشت

<div class="b" id="bn1"><div class="m1"><p>این عقل در یقین زمانه گمان نداشت</p></div>
<div class="m2"><p>کز عقل راز خویش زمانه نهان نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گیتی‌ای شگفت کران داشت هرچه داشت</p></div>
<div class="m2"><p>چون بنگرم عجایب گیتی کران نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگونه چیز داشت جهان تا بنای داشت</p></div>
<div class="m2"><p>ملکی قوی چو ملک ملک ارسلان نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاینده باد ملکش و ملکی است ملک او</p></div>
<div class="m2"><p>که ایام نوبهار چنان بوستان نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت آن زمان که ملکش موجود شد جهان</p></div>
<div class="m2"><p>دلشاد و هیچ شادی تا آن زمان نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن جود و عدل دارد سلطان که پیش از این</p></div>
<div class="m2"><p>آن جود و عدل، حاتم و نوشیروان نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنگام کر و فر وغا تاب زخم او</p></div>
<div class="m2"><p>شیر ژیان ندارد و پیل دمان نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای پادشاه عادل و سلطان گنج بخش</p></div>
<div class="m2"><p>هرگز جهان ملک چو تو قهرمان نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امروز یاد خواهم کردن ز حسب حال</p></div>
<div class="m2"><p>یک داستان که دهر چنان داستان نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بونصر پارسی ملکا جان به تو سپرد</p></div>
<div class="m2"><p>زیرا سزای مجلس عالی جز آن نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان داد در هوات که باقیت باد جان</p></div>
<div class="m2"><p>اندر خور ثنا جز آن پاک جان نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان‌های بندگان همه پیوند جان توست</p></div>
<div class="m2"><p>هر بنده جز برای تو جان و روان نداشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن شهم کاردان مبارز که مثل او</p></div>
<div class="m2"><p>این دهر یک مبارز و یک کاردان نداشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرد هنر سوار که یک باره از هنر</p></div>
<div class="m2"><p>اندر جهان نماند که او زیر ران نداشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کس چون زبان او به فصاحت زبان ندید</p></div>
<div class="m2"><p>کس چون بیان او به لطافت بیان نداشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او یافت صد کرامت اگر مدتی نیافت</p></div>
<div class="m2"><p>او داشت صد کفایت اگر سو زیان نداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندیشهٔ مصالح ملک تو داشتش</p></div>
<div class="m2"><p>و اندوه سو زیان و غم خانمان نداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در هرچه اوفتاد بد و نیک و بیش و کم</p></div>
<div class="m2"><p>او تاب داشت تاب سپهر کیان نداشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شصت و سه بود عمرش چون عمر مصطفی</p></div>
<div class="m2"><p>افزون از این مقامی اندر جهان نداشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن ساعت وفات که پاینده باد شاه</p></div>
<div class="m2"><p>روی نیاز جز به سوی آسمان نداشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مدح خدایگان و ثنای خدای عرش</p></div>
<div class="m2"><p>جز بر زبان نراند و جز اندر دهان نداشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن بندگی که بودش در دل، نکرد از آنک</p></div>
<div class="m2"><p>یک هفته داشت چرخش و جز ناتوان نداشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این مدح خوان دعا کندش زان که در جهان</p></div>
<div class="m2"><p>کم بود نعمتی که بر این مدح خوان نداشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر بنده مهر داشت چهل سال و هرگز او</p></div>
<div class="m2"><p>بر هیچ آدمی دل نامهربان نداشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صاحب قران تو بادی تا هست مملکت</p></div>
<div class="m2"><p>زیرا که مملکت چو تو صاحب قران نداشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرزندکانش را پس مرگش عزیزدار</p></div>
<div class="m2"><p>کاو خود به عمر جز غم فرزندکان نداشت</p></div></div>