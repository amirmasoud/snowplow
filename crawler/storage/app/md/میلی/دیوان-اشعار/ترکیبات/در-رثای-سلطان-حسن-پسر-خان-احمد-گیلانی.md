---
title: >-
    در رثای سلطان حسن، پسر خان احمد گیلانی
---
# در رثای سلطان حسن، پسر خان احمد گیلانی

<div class="b" id="bn1"><div class="m1"><p>باز این خرابه را سپه غم گرفته است</p></div>
<div class="m2"><p>افلاک، رنگ حلقه ماتم گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خرمن زمانه نسوزد ز برق آه</p></div>
<div class="m2"><p>کز گریه آسمان و زمین نم گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم سیاه گشته، همانا که صبح را</p></div>
<div class="m2"><p>آیینه زنگ ز آه دمادم گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بی‌دلان، سراچه عالم درین بلا</p></div>
<div class="m2"><p>تنگ است چون دلی که ز عالم گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندان نگین ملک ازین غصّه کنده روی</p></div>
<div class="m2"><p>کش با دو دست حلقه ماتم گرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ملال ماه صفر این قَدَر نبود</p></div>
<div class="m2"><p>گویا که قسمتی ز محرّم گرفته است</p></div></div>
<div class="b2" id="bn7"><p>زیر و زبر زمانه اگر ازین عزاست</p>
<p>در زیر خاک، خسرو روی زمین چراست؟</p></div>
<div class="b" id="bn8"><div class="m1"><p>دردا که شمع خانه دولت تباه شد</p></div>
<div class="m2"><p>عالم سیاه از اثر دود آه شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد مهر چاشتگاه چو شمع سحر تباه</p></div>
<div class="m2"><p>روز هزار سوخته کوکب سیاه شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این است خلق را سبب زندگی (که) مرگ</p></div>
<div class="m2"><p>رفت از میان زبس که خجل زین گناه شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ظلّ همای کز سر شهزاده پا کشید</p></div>
<div class="m2"><p>هرجا افتاد، بار دل و خاک راه شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وقت جهانگشایی او بود، از غرور</p></div>
<div class="m2"><p>مانند آفتاب جدا از سپاه شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او از هزار تفرقه آسود و سود کرد</p></div>
<div class="m2"><p>امّا درین معامله، نقصان شاه شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین پس به داد کس که رسد، چون به روزگار</p></div>
<div class="m2"><p>بیداد دیده، دادرس و دادخواه شد</p></div></div>
<div class="b2" id="bn15"><p>آتش به عالمی زد و آسود جان او</p>
<p>بس دور بود این ز دل مهربان او</p></div>
<div class="b" id="bn16"><div class="m1"><p>خلقی جنازه با دل غمناک می‌برند</p></div>
<div class="m2"><p>آب حیات را به سوی خاک می‌برند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا آنکه تنگ بود جهان از شکوه او</p></div>
<div class="m2"><p>او را مسیح‌وار بر افلاک می‌برند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نی‌نی پی علاج به مأوای دیگرش</p></div>
<div class="m2"><p>از بیم این هوای خطرناک می‌برند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آلودگی نیافته از گرد معصیت</p></div>
<div class="m2"><p>آورده‌اند پاک (و) همان پاک می‌برند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن تازیان برق عنان را کشان کشان</p></div>
<div class="m2"><p>چون آهوان بسته به فتراک می‌برند</p></div></div>
<div class="b2" id="bn21"><p>جانها ز بیخودی سر پا بر بدن زنند</p>
<p>سر بر سر جنازه سلطان حسن زنند</p></div>
<div class="b" id="bn22"><div class="m1"><p>فکری برای درد دل زار او کنید</p></div>
<div class="m2"><p>زانجا قیاس مردن دشوار او کنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر گوشه جنازه او افکنید چشم</p></div>
<div class="m2"><p>نظّاره گلِ سرِ دستار او کنید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در پای آن جنازه که مستانه می‌رود</p></div>
<div class="m2"><p>یاد شمایل (و) قد و رفتار او کنید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در خاک یافت با دل پر آرزو قرار</p></div>
<div class="m2"><p>اندیشه تحمّل بسیار او کنید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از دل هنوز بر نتواند گرفت دست</p></div>
<div class="m2"><p>آیید و چاره دل افگار او کنید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>او را به جان خرید و ازو بهره‌ای ندید</p></div>
<div class="m2"><p>اندیشه زیان خریدار او کنید</p></div></div>
<div class="b2" id="bn28"><p>آه از دم وداع که در اضطراب بود</p>
<p>با شاه کامیاب، دلش در خطاب بود</p></div>
<div class="b" id="bn29"><div class="m1"><p>کای عمر برگذشته، وفا را گذاشتی</p></div>
<div class="m2"><p>آخر ز ما گذشتی و ما را گذاشتی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رفتی که جرعه‌خوار می عافیت شوی</p></div>
<div class="m2"><p>پیمانه‌نوش زهر بلا را گذاشتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با من که در فراق تو بودم چراغ صبح</p></div>
<div class="m2"><p>طغیان تندباد فنا را گذاشتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رفتیّ و جذبهٔ اجلم سوی خود کشید</p></div>
<div class="m2"><p>با برگ کاه، کاهربا را گذاشتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با من خدا عذاب فراقت روا نداشت</p></div>
<div class="m2"><p>تو داشتّی و راه خدا را گذاشتی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>امروز دست من ز عنان تو کوته است</p></div>
<div class="m2"><p>چون ماجرای روز جزا را گذاشتی؟</p></div></div>
<div class="b2" id="bn35"><p>ای آنکه چون تو دادرسی در جهان نبود</p>
<p>بیداد این‌چنین ز تو کس را گمان نبود</p></div>
<div class="b" id="bn36"><div class="m1"><p>معذور دار اگر نیّم اندر رکاب تو</p></div>
<div class="m2"><p>کان قوّتم نماند که آیم به خواب تو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جان می‌دهم به سختی ازین غم که مردنم</p></div>
<div class="m2"><p>ترسم شود وسیلهٔ چشم پرآب تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترسم ز حسرت دل خود گر خبر دهم</p></div>
<div class="m2"><p>چون بشنوی، شود سبب اضطراب تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بیرون نخواهم آمدن ای آّب زندگی</p></div>
<div class="m2"><p>از خاک تا به روز حساب از حجاب تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کاین جان که در عنان اجل می‌رود، چرا</p></div>
<div class="m2"><p>بر باد همچو گرد نشد در رکاب تو؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>افغان ز بخت خانه برانداز من که هست</p></div>
<div class="m2"><p>آباد عالمی ز تو و من خراب تو</p></div></div>
<div class="b2" id="bn42"><p>بود این حکایتش به زبان تا خموش شد</p>
<p>خاموشیی که عالم ازو پر خروش شد</p></div>
<div class="b" id="bn43"><div class="m1"><p>امّا ز گوهری که به دُرج عدم شود</p></div>
<div class="m2"><p>از بحر، غیر قطرهٔ آبی چه کم شود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسیار باشد اینکه فزاید هوای باغ</p></div>
<div class="m2"><p>نخلی اگر بریده به تیغ ستم شود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در باغ، سرفراز ز آزادگی‌ست سرو</p></div>
<div class="m2"><p>از بار میوه باشد اگر نخل خم شود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ایّام مهربان و شهنشاه نوجوان</p></div>
<div class="m2"><p>این ماجرا عجب که سبب‌ساز غم شود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>یارب که سهل بگذرد این ماجرای صعب</p></div>
<div class="m2"><p>چندان‌که خصم را سبب صد الم شود</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صبری دهد خدای درین غصّه شاه را</p></div>
<div class="m2"><p>کز خوشدلی به کین ستم متّهم شود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>افزون شود ز مردن فرزند، دولتش</p></div>
<div class="m2"><p>چندان‌که مرگ شهره به یُمن قدم شود</p></div></div>
<div class="b2" id="bn50"><p>یارب فتور عافیت سرمدی مباد</p>
<p>یعنی زوال دولت خان‌احمدی مباد</p></div>