---
title: >-
    شمارهٔ ۹ - در مدح نورنگ خان
---
# شمارهٔ ۹ - در مدح نورنگ خان

<div class="b" id="bn1"><div class="m1"><p>عجب عجب که شب غم به صبحگاه رسید</p></div>
<div class="m2"><p>نسیم وصل سواری ز گرد راه رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فرق سوختهٔ غم در آفتاب ستم</p></div>
<div class="m2"><p>ز سایبان حجاب کرم، پناه رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رساند باد به بلبل خبر که گل آمد</p></div>
<div class="m2"><p>رسید مژده به گوش گدا که شاه رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خرّمی به سماع آمدند چون مستان</p></div>
<div class="m2"><p>چو این نوید به پیران خانقاه رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار قد شده خم چون کمان به سجدهٔ شکر</p></div>
<div class="m2"><p>که بر نشانهٔ مقصود، تیر آه رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث کوته و افسانه مختصر، ز سفر</p></div>
<div class="m2"><p>مه ستاره حشم، خان جم سپاه رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سحاب همّت خورشید مکرمت، نورنگ</p></div>
<div class="m2"><p>که فیض نعمت عامش به ما سواه رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلاوری که دم کین او به گوش جهان</p></div>
<div class="m2"><p>ز آسمان و زمین وامصیبتاه رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پای توسن او تا چو نعل سود جبین</p></div>
<div class="m2"><p>به آسمان مه نو را پر کلاه رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتاد کشتی آز از کفش به گردابی</p></div>
<div class="m2"><p>که تا کران نتواند به صد شناه رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زهی رسیده به جایی ترا سریر جلال</p></div>
<div class="m2"><p>که با سپهر به سرحدّ اشتباه رسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به پای بوس تو آمد فلک، وگرنه چرا</p></div>
<div class="m2"><p>بر آستان تو با قامت دو تاه رسید؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز آستان تو خورشید با هزار کمند</p></div>
<div class="m2"><p>به جای شمسه بر ایوان بارگاه رسید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو سایه مهر نهد رو بر آن زمین همه روز</p></div>
<div class="m2"><p>که پای چتر تو با این علّو جاه رسید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمود مهر دگر از فروغ آن به سپهر</p></div>
<div class="m2"><p>اگر زرای تو پرتو به قعر چاه رسید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز اشتیاق، شتابنده شد به استقبال</p></div>
<div class="m2"><p>به گوش عفو تو چون مژدهٔ گناه رسید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فلک به خوان تو نسبت اگر نکرد درست</p></div>
<div class="m2"><p>چرا شکست پیاپی به قرص ماه رسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به دور عدل تو از کهربا عجب دارم</p></div>
<div class="m2"><p>که دست او به گریبان برگ کاه رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ترا ز نالهٔ مظلوم دل به درد آمد</p></div>
<div class="m2"><p>به غایتی که خجالت به دادخواه رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز عیش دور تو یاد از سرور مستی داد</p></div>
<div class="m2"><p>به مست خفته اگر قامت‌الصّلوه رسید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو آفتابی و بهر ثبوت این دعوی</p></div>
<div class="m2"><p>مرا چو صبح گواه از پی گواه رسید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ماهیان ید بیضا توان مشاهده کرد</p></div>
<div class="m2"><p>دمی که پرتو رای تو بر میاه رسید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کمند مهر ز جا ذرّه را نجنبانید</p></div>
<div class="m2"><p>ز ابر حلم تو گرنم به خاک راه رسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عروج خاک سزد همچو آتش از جایی</p></div>
<div class="m2"><p>که بندگان ترا بر زمین جباه رسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسی که سوختهٔ آتش عتاب تو شد</p></div>
<div class="m2"><p>به حشر، نامهٔ اعمال او سیاه رسید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دمید سبزه چو مژگان یار، نیزه گذار</p></div>
<div class="m2"><p>به هر زمین که ترا گردی از سپاه رسید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان سپاه تو برداشت خیل دشمن را</p></div>
<div class="m2"><p>که تند سیل، تو گویی به مشت کاه رسید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به رنگ صاعقه، روز نبرد، گلگونت</p></div>
<div class="m2"><p>به خصم تیزتر از ناوک نگاه رسید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>میان معرکه دشمن چنان نمود ترا</p></div>
<div class="m2"><p>که خون گرفته شکاری به صیدگاه رسید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عدو که تافت عنان از اطاعت تو مرنج</p></div>
<div class="m2"><p>چو عاقبت به رکاب تو عذرخواه رسید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که هر که سر به اطاعت نهاد یزدان را</p></div>
<div class="m2"><p>در اوّلش به زبان ذکر لا‌اله رسید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپهر منزلتا! بی‌تو ز آتش هجران</p></div>
<div class="m2"><p>چه داغها که به جان و دل تباه رسید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نفس که سلسله جنبان زندگانی بود</p></div>
<div class="m2"><p>به لب ز تیرگی دل چو دود آه رسید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زلال خضر و دم عیسوی چو زهر و سموم</p></div>
<div class="m2"><p>به ناتوان تو جانسوز و عمرکاه رسید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رسید جان به لبم بارها و تافت عنان</p></div>
<div class="m2"><p>ز استماع نویدی که گاه‌گاه رسید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به خاک پای تو کاین مژده‌ام نمود چنان</p></div>
<div class="m2"><p>که قطره‌ای به لب تشنهٔ گیاه رسید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا به دیده کنون خواب عافیت خوش باد</p></div>
<div class="m2"><p>که چشم بخت مرا وقت انتباه رسید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآر دست دعا بهر مدّعا میلی</p></div>
<div class="m2"><p>به شکر آنکه شب غم به صبحگاه رسید</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا به زبان بگذرد که صاحب جاه</p></div>
<div class="m2"><p>به سرفرازی دیهیم و فرّگاه رسید</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به فرّ جاه تو دیهیم و گاه، عالی باد</p></div>
<div class="m2"><p>که دست از تو به صد افسر و کلاه رسید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به کُنه مدّت جاهت زمانه نتواناد</p></div>
<div class="m2"><p>به ماه و سال ز تکرار سال و ماه رسید</p></div></div>