---
title: >-
    شمارهٔ ۱۶ - در مدح نورنگ‌خان
---
# شمارهٔ ۱۶ - در مدح نورنگ‌خان

<div class="b" id="bn1"><div class="m1"><p>در گلو بینم گر از تیغ شهادت شربتی</p></div>
<div class="m2"><p>یک دم از عمر به تلخی رفته یابم لذّتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مرغ نیم بسمل در میان خاک و خون</p></div>
<div class="m2"><p>نیم جانی دارم و از وی ندارم راحتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به این آسودگی در عمر خود کم بوده‌اند</p></div>
<div class="m2"><p>کشتگان تیغ او دارند هر یک حسرتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاه قتلم از حسد بگرفت دشمن دست دوست</p></div>
<div class="m2"><p>باز در صد محنتم افکند و دارد منّتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خویش را هر لحظه بینم در محبّت گرمتر</p></div>
<div class="m2"><p>با وجود آنکه هر دم پیشم آید محنتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس ندارد جرئت پابوس او از بیم جان</p></div>
<div class="m2"><p>دست می‌شویم ز جان و می‌نمایم جرئتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شکایت در عتاب آوردمش، بینم کنون</p></div>
<div class="m2"><p>من ازو شرمنده، او هم دارد از من خجلتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارم از دست ستمهایش دل آزرده‌ای</p></div>
<div class="m2"><p>کاش بهر شکوه در بزمش بیابم فرصتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از وفا عمری که سر بر آستانش داشتم</p></div>
<div class="m2"><p>هر زمانم با سگان کوی او بود الفتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این زمان کز کوی او محروم گشتم، هر زمان</p></div>
<div class="m2"><p>بر دلم از یاد هر الفت فزاید کلفتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با کدامین دل روم سویش، همان گیرم که باز</p></div>
<div class="m2"><p>با رقیبش دیدم و در دل گره شد حسرتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون ز بیداد تو می‌نالم، مرا معذور دار</p></div>
<div class="m2"><p>گر سگ کوی ترا از ناله دادم زحمتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر یک دیدار دیگر بود، نه از بیم جان</p></div>
<div class="m2"><p>وقت کشتن خواستم گر از تو یک دم مهلتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>استماع نالهٔ نی حال می‌بخشد، ولی</p></div>
<div class="m2"><p>دارد آواز نی تیر تو دیگر حالتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه در بزم تو بر وصلم حسد بردی کجاست</p></div>
<div class="m2"><p>تا بگیرد از من و محرومی من عبرتی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خسته‌ام دیدیّ و بر ریشم نبستی مرهمی</p></div>
<div class="m2"><p>زحمتم دادیّ و بر حالم نکردی رحمتی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن ستمها کز تو دیدم کی ز دل بیرون رود</p></div>
<div class="m2"><p>گر نبینم روی خورشید همایون طلعتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهر کیوان منزلت، نورنگ دریادل که چرخ</p></div>
<div class="m2"><p>با علوّ آستان او ندارد رفعتی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن‌قدر قدرت که با سر پنجهٔ انصاف او</p></div>
<div class="m2"><p>حلقهٔ بازوی گردون را نباشد قدرتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وان ولی نعمت که بر خوان سخایش حرص را</p></div>
<div class="m2"><p>هیچ در خاطر نماند آرزوی نعمتی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کجا انعام عامش گسترد خوان عطا</p></div>
<div class="m2"><p>می‌برد در خورد استعداد، هر کس قسمتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بس که در ایّام او دست تطاول کوته است</p></div>
<div class="m2"><p>زلف خوبان را به دل بردن نباشد رغبتی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از ستم در دور عدلش وحش و طیر آسوده‌اند</p></div>
<div class="m2"><p>همچو بخت عاشقان هریک به خواب غفلتی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در زمان همّتش گشتند چون گوهر عزیز</p></div>
<div class="m2"><p>پیش ازین می‌بود اگر اهل طمع را ذلّتی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عام شد انعام تا حدّی که حیرت می‌کنند</p></div>
<div class="m2"><p>در وجود آید گر از طبع خسیسان خسّتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در گلوی دشمنان، کار دم خنجر کند</p></div>
<div class="m2"><p>گر در آب تیغ او مضمر شود خاصیّتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بس که در عهدش دد و دامند ایمن از گزند</p></div>
<div class="m2"><p>در کمند، آهوی وحشی را نباشد وحشتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای که در دور تو بر دلهای محزون، از نشاط</p></div>
<div class="m2"><p>گوشهٔ بیت‌الحزن گردید بیت‌العشرتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هیچ‌کس را نگسلد تیغ اجل تار حیات</p></div>
<div class="m2"><p>تا نه از تیغ جهانسوز تو گیرد رخصتی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حاصل دریا و کان باشد ترا یک‌روزه خرج</p></div>
<div class="m2"><p>بلکه آن یک روز هم برنگذرد بی‌عسرتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شد چنان دلها به عهدت از گزند ایمن که مار</p></div>
<div class="m2"><p>گر شود همخوابه، در خاطر نیفتد دهشتی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>داورا! بر حسب فرمان از خراسان سوی هند</p></div>
<div class="m2"><p>آمدم، وین قصهٔ در هر شهر دارد شهرتی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از در ارباب دولت پا کشیدم، چون زدم</p></div>
<div class="m2"><p>دست در دامان جاه چون تو صاحب‌دولتی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رو به هر سویی نهادی، در قدم بودم ترا</p></div>
<div class="m2"><p>گر سزاوار تو از دستم نیامد خدمتی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در خلا و در ملا، غایب نبودم لحظه‌ای</p></div>
<div class="m2"><p>وز دعا و از ثنا فارغ نبودم ساعتی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>این زمان کز آستانت با دل امّیدوار</p></div>
<div class="m2"><p>کرده‌ام عزم دیار خویش بعد از مدّتی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بی‌تکلّف، بود امّیدم که از درگاه تو</p></div>
<div class="m2"><p>گر پریشان آمدم، با خود برم جمعیّتی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جز تو ممنون کسی دیگر نباشم در جهان</p></div>
<div class="m2"><p>با تو در جمعیّتم کس را نباشد شرکتی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در به روی خلق بندم، پا به دامان در کشم</p></div>
<div class="m2"><p>با دل آسوده بنشینم به کنج عزلتی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دم به دم در شرح اوصافت کنم اندیشه‌ای</p></div>
<div class="m2"><p>هر زمان در وصف اخلاقت نمایم فکرتی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در تصوّر کی گذر می‌کرد این معنی مرا</p></div>
<div class="m2"><p>کز تو آخر کار من این رنگ یابد صورتی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از تو احسانی که من می‌خواستم نسبت به خویش</p></div>
<div class="m2"><p>با لقایی کرده‌ای نسبت به هر بی‌نسبتی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تیرگی از تیره‌بختیهای من کردی به من</p></div>
<div class="m2"><p>همچو آن آیینه کز زنگی پذیرد ظلمتی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چشم آن می‌داشتم کز فتح باب دست تو</p></div>
<div class="m2"><p>زین درم سوی در دیگر نیفتد حاجتی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون ترا دیدم در همّت به رویم بستهای</p></div>
<div class="m2"><p>همّتی ورزیدم و رفتم ازین در، همّتی!</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گوهری بودم، مرا از دست دادی رایگان</p></div>
<div class="m2"><p>گرچه گوهر این زمان پیش تو دارد عزّتی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با وجود آنکه جای کلفت و آزار هست</p></div>
<div class="m2"><p>نی به خاطر دارم آزاری، نه در دل کلفتی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر صد این مقدار هم بینم در احسانت فتور</p></div>
<div class="m2"><p>کسی ازان در اعتقادم راه یابد فترتی؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>یارب اندر عمر خود هرگز نباشم با حضور</p></div>
<div class="m2"><p>از تو هرگز بر زبانم بگذرد گر غیبتی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همچو شام این ماجرا دارد کدورت، وقت شد</p></div>
<div class="m2"><p>کز دعا، چون صبح، میلی بر فرازی رایتی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا غریبانی که دور افتاده‌اند از خان و مان</p></div>
<div class="m2"><p>جمله را سوی وطن باشد خیال رجعتی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرجع اقبال و دولت باد تا روز قیام</p></div>
<div class="m2"><p>آستان بارگاه چون تو عالی حضرتی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>دشمن جاه تو دشمنکام تا هنگام مرگ</p></div>
<div class="m2"><p>از وطن آواره هریک در بلای غربتی</p></div></div>