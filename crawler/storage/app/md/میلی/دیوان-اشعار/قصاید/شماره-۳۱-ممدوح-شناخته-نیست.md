---
title: >-
    شمارهٔ ۳۱ - ممدوح شناخته نیست
---
# شمارهٔ ۳۱ - ممدوح شناخته نیست

<div class="b" id="bn1"><div class="m1"><p>بر صفحه زمین، الف قدّ پُر دلان</p></div>
<div class="m2"><p>از زخم تیغ، لا شود از ضرب گرز، دال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز وغا که در کف از سرگذشتگان</p></div>
<div class="m2"><p>گیرند تیغها ز اجل رخصت قتال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آفتاب و جای تو بر اوج آسمان</p></div>
<div class="m2"><p>فرند ارجمند تو، ماه خجسته فال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهد که خدمت از بن دندان کند ترا</p></div>
<div class="m2"><p>زین آرزو ست‌گر مه نو گشته چون خلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دل که از تطاول زلف تو سر کشد</p></div>
<div class="m2"><p>چون شیشه شکسته نمی‌یابد اتّصال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از نبی، چو نقش نگین جا گرفته است</p></div>
<div class="m2"><p>در خاطرش محبّت اصحاب و مهر آل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مجلسی که صدر نشین است قدر تو</p></div>
<div class="m2"><p>ننشیند آفتاب مگر در صف نعال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید که در مشیمه ارحام، طفل را</p></div>
<div class="m2"><p>گویا ز یُمن مدح تو گردد زبال لال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون حمله آوریّ و کنی عزم رزم جزم</p></div>
<div class="m2"><p>آنجا زخصم، تاب نشستن چه احتمال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر از نهیب، بانگ به شیر ژیان کنی</p></div>
<div class="m2"><p>بگریزد آن چنان که ز شیر ژیان شغال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر چرخ را اراده بر هم زدن کنی</p></div>
<div class="m2"><p>یابد زهم چو قطره سیماب انفصال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پاس عدل او نتواند که سرزده</p></div>
<div class="m2"><p>بر آستان خانه دل، پا نهد خیال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر بگذرد سپاه سلیمان ز ملک او</p></div>
<div class="m2"><p>از پاس عدل او نشود مور پایمال</p></div></div>