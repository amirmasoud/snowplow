---
title: >-
    شمارهٔ ۱۸ - در مدح ابراهیم میرزا
---
# شمارهٔ ۱۸ - در مدح ابراهیم میرزا

<div class="b" id="bn1"><div class="m1"><p>عشق چنین بی‌نصیب، حسن چنین بی‌وفا</p></div>
<div class="m2"><p>دل که و آرام چه، وصل کجا من کجا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچهٔ او تنگخو، عشوهٔ او پرفریب</p></div>
<div class="m2"><p>غمزهٔ او جنگجو، وعدهٔ او بی‌وفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مایهٔ نومیدی‌ام، باعث محرومی‌ام</p></div>
<div class="m2"><p>گه ز غلوی غرور، گه ز هجوم حیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌رمد آن مرغ رام، پیش من از مدّعی</p></div>
<div class="m2"><p>تا نکنم اهتمام، در طلب مدّعا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوزد دل بدگمان، تا زند آتش به جان</p></div>
<div class="m2"><p>کرده به بیگانگان، صد نگه آشنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد هزار اضطراب، چون به رهش می‌روم</p></div>
<div class="m2"><p>می‌گذرد با رقیب، تا نروم از قفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانب او تا کسی، ننگرد از بیم جان</p></div>
<div class="m2"><p>غمزهٔ او گو بریز، خون مرا بر ملا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که ز کشتن چنین گشته‌ام آسوده‌دل</p></div>
<div class="m2"><p>از طرف قاتل است، دعوی روز جزا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشته جهانی ور از بیگنهی دم زند</p></div>
<div class="m2"><p>خوبی او می‌کند، مدّعیان را گوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز به تقریب شرم، می‌فکند سر به پیش</p></div>
<div class="m2"><p>بس که به زنجیر زلف، بسته دل مبتلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طعنهٔ مردم بسی، می‌شنود بهر من</p></div>
<div class="m2"><p>گرچه نگوید ز شرم، یافته‌ام از ادا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌رسی ای تیر یار در دلم از راه دور</p></div>
<div class="m2"><p>غیر درین خانه نیست، در بگشا و درآ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای که به زنجیر زلف، حسن جهانگیر تو</p></div>
<div class="m2"><p>آهوی سر در کمند ساخته خورشید را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال دل پر ملال، می‌کنی از من سوال</p></div>
<div class="m2"><p>تا نکشی انفعال، بگذر ازین ماجرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچکسی چون مرا، گر بکشی بی‌گناه</p></div>
<div class="m2"><p>کس نتواند ز شرم، دم زدن از خونبها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ناوک بیداد تو، بس که فتد کارگر</p></div>
<div class="m2"><p>صید ترا کمتر است، لذّت زخم جفا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون به تکلّف نهی گوش به درد دلم</p></div>
<div class="m2"><p>بس که شوم مضطرب، فوت شود مدّعا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میل کُشش گر کنی، شوق دم تیغ تو</p></div>
<div class="m2"><p>شاید اگر جان دهد، مردهٔ صدساله را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حسن ترا همچو شمع، آتش و آب است جمع</p></div>
<div class="m2"><p>شد سبب این، مگر معدلت میرزا؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه ز بس گسترد، خوان خلیل‌اللّهی</p></div>
<div class="m2"><p>کرده برایش نزول، اسم خلیل از سما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عافیت شهر ازو، همچو دل از خرّمی</p></div>
<div class="m2"><p>تربیت دهر ازو، همچو مس از کیمیا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دولت او چون نسیم، گر گذرد بر جحیم</p></div>
<div class="m2"><p>بر سر آتش شود دود چو ظلّ هما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر ز کمندش فتد، سایه برین صیدگاه</p></div>
<div class="m2"><p>باد شود در کشش، چون نفس اژدها</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پرتو اگر افکند مهر عتابش، سزد</p></div>
<div class="m2"><p>گر بدمد از زمین، شعله به جای گیا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ذکر سراپرده‌اش، باد گر آرد به گوش</p></div>
<div class="m2"><p>در حرم استماع، راه نیابد صدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هشته اگر خامه را، بر ورق دفترش</p></div>
<div class="m2"><p>گشته مثال نهال، قابل نشو و نما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همّت او پرتوی گر فکند بر جهان</p></div>
<div class="m2"><p>باز رهد برگ کاه، از کشش کهربا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مطرب اگر ناگهان، یاد عتابش کند</p></div>
<div class="m2"><p>سر زند از نی چو شمع، شعله به جای نوا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کرده نهیبش گذار، چون به سوی کارزار</p></div>
<div class="m2"><p>گشته چو برگ چنار، پنجهٔ زورآزما</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای که جهان را اگر، قهر تو خواهد سراب</p></div>
<div class="m2"><p>سر نزند بعد از این، خوی ز جبین حیا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مهر ضمیر ترا، گر گذر افتد به دل</p></div>
<div class="m2"><p>نیست عجب گر ز آه، آینه یابد جلا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر نظر کبریا، سوی نجوم افکنی</p></div>
<div class="m2"><p>صورت افلاک را، آینه گردد سها</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون به زبان بگذرد، وصف دم تیغ تو</p></div>
<div class="m2"><p>بگسلد از یکدگر، سلک حروف هجا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طبع تو چون می‌نهد، قاعدهٔ راستی</p></div>
<div class="m2"><p>طوق جنون، می‌شود پیر خرد را عصا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر گذرد سوی بحر، صاعقهٔ قهر تو</p></div>
<div class="m2"><p>شعله به موج افکند، زودتر از بوریا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یک دم اگر در زمین، رای تو باشد دفین</p></div>
<div class="m2"><p>خاک دهد بعد ازین، خاصیت توتیا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر به گریبان آب، چون گهر آرد حباب</p></div>
<div class="m2"><p>حلم تو گر چون سحاب، سایه کند بر هوا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طرح تواند فکند، حفظ تو از موج آب</p></div>
<div class="m2"><p>در ره مرغابیان، دامگه ابتلا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون به کلید سخا، گنج‌گشایی کنی</p></div>
<div class="m2"><p>لاف اقامت زند، در دل خوبان وفا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جذبهٔ قدر تو برد، خضر فرومانده را</p></div>
<div class="m2"><p>از سر چاه فنا، بر در دار بقا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باشد اگر فی‌المثل، جود تو روزی‌رسان</p></div>
<div class="m2"><p>معدهٔ آتش کند از خس و خار امتلا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از چمن لطف تو، برگ برو گر نهند</p></div>
<div class="m2"><p>می بچکد چون حیا، آب ز رنگ حنا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر سر میدان کین، تیغ برآر و ببین</p></div>
<div class="m2"><p>از دل سنگین خصم، جذبهٔ آهن‌ربا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیده اگر پرتوی یافته از رای تو</p></div>
<div class="m2"><p>ز آینهٔ آفتاب، دیده خیال سها</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر درِ اندیشه را، فتح تو گردد کلید</p></div>
<div class="m2"><p>باز شود شخص را، عقده ز بند قبا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>قهر تو همچون سموم، گر گذرد بر جهان</p></div>
<div class="m2"><p>شاید اگر همچو دود در نظر آید ضیا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تنگ شود بر زمین، پیرهن آسمان</p></div>
<div class="m2"><p>سوی زمین افکنی، گر نظر کبریا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای که به دامان توست دست امیدم قوی</p></div>
<div class="m2"><p>وی که به دوران توست پای مرادم روا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>من به زبان‌آوری شهره و در حیرتم</p></div>
<div class="m2"><p>تا به کدامین زبان، شکر تو آرم به جا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بادهٔ عشرت به جام، صحبت ساقی به کام</p></div>
<div class="m2"><p>طایر مقصود رام، وحشی وصل آشنا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جام می خوشگوار، لعل می‌آلود یار</p></div>
<div class="m2"><p>رنج تنم را علاج، درد دلم را دوا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مطرب غم داده است، تار تنم را به چنگ</p></div>
<div class="m2"><p>طفل ستم کرده است، مرغ دلم را رها</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در گذر آرزو، چشم امید مرا</p></div>
<div class="m2"><p>داده غبار ستم، فایدهٔ توتیا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اختر بختم که بود، یوسف چاه وبال</p></div>
<div class="m2"><p>چون علم فتح تو، گشته کنون عرش‌سا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از چه تراوش کند، خون ز سر انگشت مهر؟</p></div>
<div class="m2"><p>گرنه به بختم گرفت، پنجهٔ زورآزما</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من که چنین گشته‌ام، در قدمت سربلند</p></div>
<div class="m2"><p>رو به کجا آورم، گر ز تو گردم جدا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>روز قیامت ز خاک، سر به چه رو برکنم</p></div>
<div class="m2"><p>گر نکنم این زمان، پیش تو جان را فدا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نیست عجب گر کنم، عمر به مدح تو صرف</p></div>
<div class="m2"><p>غیر تو امروز کیست، قابل مدح و ثنا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>تا شده‌ای مشتری،‌ گوهر نظم مرا</p></div>
<div class="m2"><p>همچو صدف گشته پر، گوش جهان زین صدا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون ز خواص و عوام، بر همه کس ظاهر است</p></div>
<div class="m2"><p>با چو تو شاهنشهی، بندگی این گدا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>دست مدار از دلم، تا نرود دل ز دست</p></div>
<div class="m2"><p>پای مکش از سرم، تا که نیفتم ز پا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وقت عنایت مبین مرتبهٔ پست من</p></div>
<div class="m2"><p>تو همه محض کرم، من همه عین خطا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از تو مرا چشم آن هست که با من کنی</p></div>
<div class="m2"><p>همّت عالیّ تو آنچه کند اقتضا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>میلی ازین درگذر، تا ز سر اعتقاد</p></div>
<div class="m2"><p>لوح دعا را کنم ساده ز حرف ریا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا که به لوح جهان، عقل نیابد نشان</p></div>
<div class="m2"><p>از رقم انتها، بی‌قلم ابتدا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دولت پاینده و عقل سلیم تو باد</p></div>
<div class="m2"><p>آخر بی‌ابتدا، اوّل بی‌انتها</p></div></div>