---
title: >-
    شمارهٔ ۱۴ - در مدح نورنگ
---
# شمارهٔ ۱۴ - در مدح نورنگ

<div class="b" id="bn1"><div class="m1"><p>ای شراب خوشدلی از جام دوران یافته</p></div>
<div class="m2"><p>کام دل بی‌منّت گیتی ز یزدان یافته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان ترا نورنگ نام از عالم بالا رسید</p></div>
<div class="m2"><p>کز تو باغ دهر از نو، رنگ احسان یافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن تویی کز نوبهار خلق و ابر جود تو</p></div>
<div class="m2"><p>بحر عنبر در کنار و گل به دامان یافته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان تویی کز عطف دامان تو خیّاط ازل</p></div>
<div class="m2"><p>مرقبای آفرینش را گریبان یافته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خیال فسحت جاهت که عالم عالم است</p></div>
<div class="m2"><p>مور در دل وسعت ملک سلیمان یافته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشته از روی تو مجلس گلشن و دل از نشاط</p></div>
<div class="m2"><p>عندلیب باغ معنی را غزلخوان یافته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل پس از عمری که جا در بزم جانان یافته</p></div>
<div class="m2"><p>از تغافلهای او خود را پشیمان یافته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصل آن غیر آشنا با دست رشک الماس ریخت</p></div>
<div class="m2"><p>بر جراحتها که دل از تیغ هجران یافته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل میان آتش و آب است از بیم و امید</p></div>
<div class="m2"><p>کز عتاب آشکارش لطف پنهان یافته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان به عزم رحلت و من شاد ازین معنی که دل</p></div>
<div class="m2"><p>درد چندین ساله ار امید درمان یافته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشتگان تیغ جانان را ز انفاس مسیح</p></div>
<div class="m2"><p>دیده دل زنگ بر آیینه جان یافته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس که از کیفیت چشم تو سرمستند خلق</p></div>
<div class="m2"><p>توبه کردن می پرست از باده آسان یافته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوش کی بر منع شیخ پاکدامان می‌نهد</p></div>
<div class="m2"><p>آنکه ذوق مستی و چاک گریبان یافته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مبتلای صد بلا میلی به جرم دوستی</p></div>
<div class="m2"><p>خویش را چون بندگان حضرت خان یافته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای فلک قدری که بر درگاه عالی جاه تو</p></div>
<div class="m2"><p>تاج سرداران شکست از چوب دربان یافته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صرصر قهرت اگر افکنده بر گردون گذار</p></div>
<div class="m2"><p>چون بنات النّعش پروین را پریشان یافته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آسمان بنموده ماه نو که اندر خدمتت</p></div>
<div class="m2"><p>گردنش فرسودگی از طوق فرمان یافته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بارها از دهشت پیکان زهرآلود تو</p></div>
<div class="m2"><p>آسمان خورشید را در ذره پنهان یافته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کودک صلب بداندیش از نهیب تیغ تو</p></div>
<div class="m2"><p>از عدم ناآمده، خود راپشیمان یافته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر کجا خورشید رایت گشته تابان، روزگار</p></div>
<div class="m2"><p>مهر را بی‌نور تر از چشم حیران یافته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آسمان خورشید را صیدگاه قهر تو</p></div>
<div class="m2"><p>همچو صید زخم دار افتان و خیزان یافته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>میزبانی کرده تا روز قیامت خلق را</p></div>
<div class="m2"><p>هر که را یک بار احسان تو مهمان یافته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس که توفیق است در عهد تو مردم را رفیق</p></div>
<div class="m2"><p>بت‌پرست از سجده بت، بوی ایمان یافته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تیرمار مرگ کز زخمش خلاصی کس ندید</p></div>
<div class="m2"><p>ظاهرا از نوک پیکان تو دندان یافته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزگار افلاک را در رستخیز قهر تو</p></div>
<div class="m2"><p>کشتی سرگشته اندر موج توفان یافته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آسمان از رشک آن دست و دل گوهرفشان</p></div>
<div class="m2"><p>آب در چشم یم و خون در دل کان یافته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنکه کان را جُسته از بهر نثار بزم تو</p></div>
<div class="m2"><p>لعل را از خرّمی چون غنچه خندان یافته</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یوسف رایت چو در مرآت مهر افکند عکس</p></div>
<div class="m2"><p>خضر عقل اندر سیاهی آب حیوان یافته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بحر اندر عهد احسان تو می‌زد لاف جود</p></div>
<div class="m2"><p>زین گناه از ابر نیسان تیر باران یافته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر که از اوج کمالت کرد بر گیتی نگاه</p></div>
<div class="m2"><p>با زمین خورشید را چون سایه یکسان یافته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مهر را از برق خرمن سوز قهرت با شعاع</p></div>
<div class="m2"><p>آسمان همچون سواد چشم و مژگان یافته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از همجوم خاکبوسان در رهت چو ماه نو</p></div>
<div class="m2"><p>کاسه سرها شکست از نعل یکران یافته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وه چه یکرانی که هرگه جسته از جا یک دوگام</p></div>
<div class="m2"><p>پیک وهم آن را برون زین هفت میدان یافته</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تر نگشته از سبکروحیّ و چالاکی سمش</p></div>
<div class="m2"><p>گر نسیم آسا به روی آب جولان یافته</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بارها ز آهسته رفتاری به میدان سپهر</p></div>
<div class="m2"><p>گوی مه در دست و پایش زخم چوگان یافته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای پی آرایش دیوان قدرت آسمان</p></div>
<div class="m2"><p>لاجوردی صفحه‌ای از نقره افشان یافته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بس که عالی شد اساس کاخ رنگ‌آمیز تو</p></div>
<div class="m2"><p>چرخ رنگ لاجورد از طاق ایوان یافته</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همّتت با آنکه داده عالمی در هر سوال</p></div>
<div class="m2"><p>انفعال از سایلان هنگام احسان یافته</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بی‌تکلّف آن تویی کامروز در بازار دهر</p></div>
<div class="m2"><p>نظم از طبع سخن سنج تو میزان یافته</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر چه جز مدح تو بر اوراق خاطر بسته نقش</p></div>
<div class="m2"><p>عقل آن را مستحق خط بطلان یافته</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر چه نتوان برد نام نظم من، اما خوشم</p></div>
<div class="m2"><p>کاین همایون نامه از نام تو عنوان یافته</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیستم لایق به احسان تو، اما دور نیست</p></div>
<div class="m2"><p>ذره‌ای صد فیض از خورشید تابان یافته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شرمسارم کز تو با این طبع ناقص،دیده‌ام</p></div>
<div class="m2"><p>آن عنایتها که خاقانی ز خاقان یافته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا دهد باد بهاری خاک را آب حیات</p></div>
<div class="m2"><p>تا شود جان از سموم و زهر نقصان یافته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خصمت از باد بهاری دیده آسیب سموم</p></div>
<div class="m2"><p>نیکخواه از زهر نفع آب حیوان یافته</p></div></div>