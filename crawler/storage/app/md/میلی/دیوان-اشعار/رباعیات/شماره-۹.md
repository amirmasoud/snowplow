---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>پیکان نگار را دل ما هدف است</p></div>
<div class="m2"><p>صد گوهر عافیت مرا در صدف است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الحق به کسی که دایمش بینی شاد</p></div>
<div class="m2"><p>گر رحم نیاوری، حقت بر طرف است</p></div></div>