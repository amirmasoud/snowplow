---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ابروی بلندت که کشند افتاده‌ست</p></div>
<div class="m2"><p>دل را همه در پی گزند افتاده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون دلم از شکستگی صد پاره</p></div>
<div class="m2"><p>چون شیشه از طاق بلند افتاده‌ست</p></div></div>