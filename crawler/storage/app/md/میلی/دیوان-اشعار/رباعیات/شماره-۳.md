---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ناکامی من همه ز خودکامی توست</p></div>
<div class="m2"><p>این سوختنم تمام از خامی توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار که از عشق تو رسوا گردم</p></div>
<div class="m2"><p>رسوایی من موجب بدنامی توست</p></div></div>