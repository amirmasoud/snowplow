---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>اکنون که دلم به دست عشق است گرو</p></div>
<div class="m2"><p>نی رای سفر مانده، نه پای تک و دو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگر مکش انتظار من ای همره</p></div>
<div class="m2"><p>بگذار مرا و راه خود گیر و برو</p></div></div>