---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>از بهر شرمساری من، ترک مست من</p></div>
<div class="m2"><p>چیزی طلب کند که نیاید ز دست من</p></div></div>