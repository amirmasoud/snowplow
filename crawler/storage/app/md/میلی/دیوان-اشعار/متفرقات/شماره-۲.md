---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تو بدگمان و مرا با تو نیست راه سخن</p></div>
<div class="m2"><p>چرا رقیب نسازد سخن میانهٔ ما</p></div></div>