---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>به روی یار نیفتد مرا نظر هرگز</p></div>
<div class="m2"><p>که تازه آرزویی در دلم گذر نکند</p></div></div>