---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>آثار جنون، عقل ز تاثیر تو دارد</p></div>
<div class="m2"><p>جان، تار رضا در کف تدبیر تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سلسله از زلف گرهگیر تو دارد</p></div>
<div class="m2"><p>دیوانه ما پای به زنجیر تو دارد</p></div></div>