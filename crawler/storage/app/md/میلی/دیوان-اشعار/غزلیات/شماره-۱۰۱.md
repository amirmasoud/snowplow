---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>خوبان در آزمودن ما صد جفا کنند</p></div>
<div class="m2"><p>با ما به اعتماد وفا تا چها کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر هزار وعده خلافیّ دیگر است</p></div>
<div class="m2"><p>گر از هزار وعده یکی را وفا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده‌زنان، کرشمه کنان، دست‌افکنان</p></div>
<div class="m2"><p>با این وآن روند و تغافل به ما کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عقل پرده‌پوش و زتقوای خود فروش</p></div>
<div class="m2"><p>بیگانه‌ام به یک نگه آشنا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل صید لاغری‌ست که صیّاد پیشگان</p></div>
<div class="m2"><p>صد بارش آورند به دام و رها کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آغاز گف‌وگو به بتان کرده‌ام ز شوق</p></div>
<div class="m2"><p>ای وای اگر ز من طلب مدّعا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنها که دل دهند چو میلی به دلبران</p></div>
<div class="m2"><p>خود را به صد هزار بلا مبتلا کنند</p></div></div>