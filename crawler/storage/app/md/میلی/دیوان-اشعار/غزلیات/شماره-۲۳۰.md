---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>ترا به کام رقیبان شنوده آمده‌ام</p></div>
<div class="m2"><p>سر هزار شکایت گشوده آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر ز کف ندهم دامن وصال ترا</p></div>
<div class="m2"><p>که خویش را به فراق آزموده آمده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان ستیزه‌گریها که دیدم و رفتم</p></div>
<div class="m2"><p>کنون ز شوق تغافل نموده آمده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هجر و وصل فزاید زمان زمان غم دل</p></div>
<div class="m2"><p>به جان ز دست دل غم فزوده آمده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جرم هجر ضروری، کم التفات مباش</p></div>
<div class="m2"><p>که عذرخواه گناه نبوده آمده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم که در سر میدان عشق چون میلی</p></div>
<div class="m2"><p>ز خصم، گوی محبت ربوده آمده‌ام</p></div></div>