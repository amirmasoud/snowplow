---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>امّید وعد‌ه‌های تو نو می‌کند مرا</p></div>
<div class="m2"><p>در دست انتظار، گرو می‌کند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاصد که هر زمان ز تو آید به سوی من</p></div>
<div class="m2"><p>شرمنده خود از تک و دو می‌کند مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیرت، مثال صورت آیینه پیش یار</p></div>
<div class="m2"><p>غافل ز شوق گفت‌و‌شنو می‌کند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میلی خیال ابروی خنجر‌گذار او</p></div>
<div class="m2"><p>رسوای شهر چون مه نو می‌کند مرا</p></div></div>