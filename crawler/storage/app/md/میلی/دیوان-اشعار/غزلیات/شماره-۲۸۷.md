---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن کز انتظارم گر خبر می‌داشتی</p></div>
<div class="m2"><p>هر زمان سویم به تقریبی گذر می‌داشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مرا در بزم می‌دیدی، پی نارفتنم</p></div>
<div class="m2"><p>هر زمان مشغولم از حرف دگر می‌داشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش مستغنی ازان بودی، که از بی‌تابی‌ام</p></div>
<div class="m2"><p>می‌شدی شرمنده، گر سویم نظر می‌داشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نماید بر تو دشمن اعتماد دوستی</p></div>
<div class="m2"><p>هر دم از راز نهانم پرده بر می‌داشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد آن کز بس که می‌دیدی سوی میلی، اگر</p></div>
<div class="m2"><p>از نگاهت بی‌خبر می‌شد، خبر می‌داشتی</p></div></div>