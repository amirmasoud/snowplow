---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>درین غمم که مباد از نگاه دم بدمش</p></div>
<div class="m2"><p>به آشنایی پنهان کنند متّهمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نامه حالت عاشق نمی‌توان دانست</p></div>
<div class="m2"><p>که غیر نام تو بیرون نیاید از قلمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دردمندی من، غیر شاد و من خوشدل</p></div>
<div class="m2"><p>که در نیافته بی درد لذت المش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسیده خواری‌ام آنجا که بی تو هم از رشک</p></div>
<div class="m2"><p>به پیش غیر توانم فتاد در قدمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیاردم به نظر از غرور و پندارد</p></div>
<div class="m2"><p>که می‌روم به سر راه انتظار کمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به این غرض ز ستم ناله می‌کند میلی</p></div>
<div class="m2"><p>که یار بشنود و یاد آرد از ستمش</p></div></div>