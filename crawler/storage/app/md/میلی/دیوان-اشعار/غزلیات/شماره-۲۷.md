---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ساقی به جلوه آر، می همچو لاله را</p></div>
<div class="m2"><p>چون لاله برفروز جمال پیاله را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر تا به پا چو نافه پر از مشک چین شود</p></div>
<div class="m2"><p>گر با کمند زلف بگیرد غزاله را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را کنی ز تیغ جفا گر ورق ورق</p></div>
<div class="m2"><p>بینی پر از حدیث وفا آن رساله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خوان غم چو میلیام از صبر تلخکام</p></div>
<div class="m2"><p>وز بیم جان فرو نبرم این نواله را</p></div></div>