---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>آن جفا پیشه که آیین وفا نشناسد</p></div>
<div class="m2"><p>تا به سویم نگرد، کاش مرا نشناسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده از زخم خدنگ تو ببستم که ترا</p></div>
<div class="m2"><p>از پی دعوی خون، روز جزا نشناسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غایت ناکسی‌ام بین، که به این رسوایی</p></div>
<div class="m2"><p>اگر از یار بپرسند، مرا نشناسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم اندیشه بسیار ز بدخویی غیر</p></div>
<div class="m2"><p>گرچه آن طفل هنوزم ز حیا نشناسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت بدبین که به میلی نکند غیر جفا</p></div>
<div class="m2"><p>خردسالی که جفا را ز وفا نشناسد</p></div></div>