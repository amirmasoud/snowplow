---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>میا به پرستش من، جون امید صحت نیست</p></div>
<div class="m2"><p>به حال مرگ مرا دیدن از محبت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غایتی هوس گفت‌وگوست با تو مرا</p></div>
<div class="m2"><p>که تاب خامُشی‌ام با وجود حیرت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون که جان به لب آمد مرا، دمی بنشین</p></div>
<div class="m2"><p>مرو، که وقت چنین رفتن از مروّت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خطت نقاب حیا برفکنده و ز حجاب</p></div>
<div class="m2"><p>هنوز با تو مرا آرزوی صحبت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بی وفایی خود، گرچه شرمسار منی</p></div>
<div class="m2"><p>هنوز پیش توام جرئت شکایت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو با رقیبی و میلی تغافلی دارد</p></div>
<div class="m2"><p>تغافلی که کم از صد نگاه حسرت نیست</p></div></div>