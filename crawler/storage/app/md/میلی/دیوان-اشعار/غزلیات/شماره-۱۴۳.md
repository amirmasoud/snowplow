---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>از نخل او امید نوا داشتم، نشد</p></div>
<div class="m2"><p>بر عهد او گمان وفا داشتم، نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیم عقوبتی ز بلا داشتم، رسید</p></div>
<div class="m2"><p>چشم اجابتی ز دعا داشتم، نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای اشک بی‌سرایت و ای آه بی‌‌اثر</p></div>
<div class="m2"><p>امیدواریی به شما داشتم، نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دم که چشمش از مژه خنجر کشیده بود</p></div>
<div class="m2"><p>چشم رعایتی ز حیا داشتم، نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم اجل ز هجر خلاصم کند،‌ نکرد</p></div>
<div class="m2"><p>این درد را امید دوا داشتم، نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی چو مرغ دام، امید فراغ بال</p></div>
<div class="m2"><p>در قید آن دو زلف دوتا داشتم، نشد</p></div></div>