---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>باز دل چشم هوس در پی داغی دارد</p></div>
<div class="m2"><p>باز پروانه ما رو به چراغی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌رود بی‌سر و پا، سر به هوا، ناپروا</p></div>
<div class="m2"><p>باز شوریده‌دل، آشفته دماغی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمرها پای طلب داشت به دامان شکیب</p></div>
<div class="m2"><p>باز افتاده به راهیّ و سراغی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب ز سودای تو با داغ جنون دلگیرم</p></div>
<div class="m2"><p>که درین خانه تاریک، چراغی دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که گردیده به طرف سر کویی خرسند</p></div>
<div class="m2"><p>نه سر سروو نه اندیشه باغی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داده میلی ز جنون دامن ناموس ز دست</p></div>
<div class="m2"><p>زده بر عالم عرفان و فراغی دارد</p></div></div>