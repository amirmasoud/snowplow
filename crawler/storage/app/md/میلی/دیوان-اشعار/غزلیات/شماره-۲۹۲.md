---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>می‌رسی قاصد و دل را به تپیدن داری</p></div>
<div class="m2"><p>باز گویا خبر نامه دریدن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کند غیر سخن، بهر فریب دل من</p></div>
<div class="m2"><p>رو بگردانی و خود را به شنیدن داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشکارا طلبی باده و نتوان پرسید</p></div>
<div class="m2"><p>که نهان با که سر باده کشیدن داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پی‌اش ای دل مشتاق ز پا افتادی</p></div>
<div class="m2"><p>وز هجوم طلب امید رسیدن داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر انگشت که چون غنچه سوسن کردی</p></div>
<div class="m2"><p>از پشیمانی خون که گزیدن داری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان شکوه ز افسردگی بزم کنی</p></div>
<div class="m2"><p>تا که را باز خیال طلبیدن داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار با غیر ازین رهگذر آید میلی</p></div>
<div class="m2"><p>یک زمان باش، اگر طاقت دیدن داری</p></div></div>