---
title: >-
    شمارهٔ ۲۶۹
---
# شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>من بی‌خبر از خویش و دل از کار فتاده</p></div>
<div class="m2"><p>گویا که به سویم نظر یار فتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورده‌ام او را بر خود از کشش دل</p></div>
<div class="m2"><p>در بزم اگر پهلوی اغیار فتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون قاصد دلدار نظر کرده به سویم</p></div>
<div class="m2"><p>دل در طمع وعده دیدار فتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشاق ز دست تو به فریاد و فغانند</p></div>
<div class="m2"><p>با آنکه زبان همه از کار فتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در پی او باز دل کیست، که امروز</p></div>
<div class="m2"><p>خورشید من از گرمی رفتار فتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی تو افسانه میلی به زبانها</p></div>
<div class="m2"><p>از آمدن و رفتن بسیار فتاده</p></div></div>