---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>هر طرف از گرد حی با آه و واویلی نشد</p></div>
<div class="m2"><p>آگه از جان دادن مجنون سگ لیلی نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار صحرای بلا از رهگذارش برنخاست</p></div>
<div class="m2"><p>تا روان از چشم مجنون هر طرف سیلی نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه که لیلی را سوی مجنون، ز استغنای حسن</p></div>
<div class="m2"><p>با وجود جذبه عشقی چنان، میلی نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه کز تاثیر استغنای عشق پرغرور</p></div>
<div class="m2"><p>رام شد آهو به مجنون و سگ لیلی نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ چون بر سینه زد میلی، سپاه غم رسید</p></div>
<div class="m2"><p>تا نزد شه کوس‌رزمی، صاحب خیلی نشد</p></div></div>