---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>انگشت اگر زنی به لبم چون پیاله‌ها</p></div>
<div class="m2"><p>از حسرت لب تو درآیم به ناله‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ودایی که خون شهیدان عشق ریخت</p></div>
<div class="m2"><p>خونابه جگر چکد از برگ لاله‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان می‌سپرد عاشق و چشم تو می‌گریست</p></div>
<div class="m2"><p>می‌داشتند ماتم مجنون، غزاله‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میلی جراحت دل ما تازه می‌شود</p></div>
<div class="m2"><p>در حلقه‌های کاکل مشکین کلاله‌ها</p></div></div>