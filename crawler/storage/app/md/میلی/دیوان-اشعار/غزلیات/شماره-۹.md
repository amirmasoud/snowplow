---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>رفت سوی خانه چون بنمود روی خویش را</p></div>
<div class="m2"><p>تا نماید بر غریبان راه کوی خویش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نیابم لذتی از همزبانیهای یار</p></div>
<div class="m2"><p>بس که می یابم پریشان، گفت وگوی خویش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پری از من گریزان است و من از انفعال</p></div>
<div class="m2"><p>می کنم پنهان ز مردم جست وجوی خویش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که ورزیدم به او بیگانگی، نزدیک شد</p></div>
<div class="m2"><p>کآشنای خود کنم، بیگانه خوی خویش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با وجود وصل، در دل حسرت دیدار ماند</p></div>
<div class="m2"><p>بس که یار از ناز برمی تافت روی خویش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی شد همچو میلی آهوی سر در کمند</p></div>
<div class="m2"><p>چون گشوده از هم کمند مشکبوی خویش را</p></div></div>