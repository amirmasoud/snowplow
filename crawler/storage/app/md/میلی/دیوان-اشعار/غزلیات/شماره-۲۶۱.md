---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>ای عالمی در خاک و خون، از غمزه خونریز تو</p></div>
<div class="m2"><p>خونبار چون مژگان ما، فتراک صیدآویز تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد چشمه خون از دلم، تیر تو بگشود و نشد</p></div>
<div class="m2"><p>سیراب ازین خونابها، نخل بلاانگیز تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون می‌شود هر صلح تو، سرمایه جنگ دگر</p></div>
<div class="m2"><p>خرسند چون گردد دلم، از صلح جنگ‌آمیز تو؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول ز گلبرگ ترت، حاصل نشد جز داغ دل</p></div>
<div class="m2"><p>آخر چه گلها بشکفد، از سبزه نو خیز تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با غمزه مردم شکار از قتلگاه کشتگان</p></div>
<div class="m2"><p>می‌آیی و خون می‌چکد، از هر نگاه تیز تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شیخ، مغروری بسی بر زهد بی‌بنیاد خود</p></div>
<div class="m2"><p>کو عشق تا بر هم زند، هنگامه پرهیز تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد میلی دیوانه را، زنجیرجنبان جنون</p></div>
<div class="m2"><p>بگذشت چون باد صبا، بر زلف عنبربیز تو</p></div></div>