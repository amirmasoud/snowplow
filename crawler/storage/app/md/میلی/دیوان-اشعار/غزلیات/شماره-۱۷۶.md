---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>چندان شب غم آه دل تنگ برآورد</p></div>
<div class="m2"><p>کآیینه صبح از نفسش زنگ برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر‌گرم به رسوایی عشقیم که ما را</p></div>
<div class="m2"><p>ز اندیشه ناموس و غم ننگ برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرمن صبر من دیوانه زد آتش</p></div>
<div class="m2"><p>هر آه که دور از تو دل تنگ برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله پی بگرفتن دامان تو دستی‌ست</p></div>
<div class="m2"><p>کز خاک، شهید تو به این رنگ برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد بر رخ میلی در غم بسته که آن شوخ</p></div>
<div class="m2"><p>از آب و گل صلح،‌ در جنگ برآورد</p></div></div>