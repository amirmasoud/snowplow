---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>شب به مستی گله چندان ز عتابش کردم</p></div>
<div class="m2"><p>که بر افروخته از جام حجابش کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منفعل گشتم ازو، گرچه نمی‌گفت جواب</p></div>
<div class="m2"><p>بس که از پرسش بسیار، غذابش کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دلم رفت برون رشک سوال دگران</p></div>
<div class="m2"><p>هر گه اندیشه ز تلخیّ جوابش کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش همخانه شد از ناله زارم بیدار</p></div>
<div class="m2"><p>گرچه صد بار ز افسانه به خوابش کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانه صبر چنان سست بنا شد میلی</p></div>
<div class="m2"><p>که به یک دم ز نَمِ گریه خرابش کردم</p></div></div>