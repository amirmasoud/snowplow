---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>... اهی چنین باشد</p></div>
<div class="m2"><p>... اهی چنین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریب خویش دادن غایتی دارد، مرا تا کی</p></div>
<div class="m2"><p>دگر تاب نشستن بر سر راهی چنین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تکلف بر طرف، تا کی توان دیدن که بر رغمم</p></div>
<div class="m2"><p>نکورویی چنان،‌ همراه بدراهی چنین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و آوارگی،‌ گر زندگی باشد، معاذاللّه</p></div>
<div class="m2"><p>ز شهری کاندرو بیدادگر شاهی چنین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو ناگاه از تو بیدادی رسد، خود را دهم تسکین</p></div>
<div class="m2"><p>که بی‌رحمی چنان، سهل است اگر گاهی چنین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز میلی غافل ای افسرده دم منشین که در یک دم</p></div>
<div class="m2"><p>زند آتش به عالم هر که را آهی چنین باشد</p></div></div>