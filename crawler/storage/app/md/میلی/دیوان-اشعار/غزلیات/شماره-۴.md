---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دل که زیاده می کند، قاعده نیاز را</p></div>
<div class="m2"><p>مایه ناز می شود، خوی بهانه ساز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون کدام بی گنه ریخته بر زمین،که تو</p></div>
<div class="m2"><p>بر زده ای چو شاخ گل، دامن سروناز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تو نیم جان خود بازم، اگر ز مردمی</p></div>
<div class="m2"><p>بازکنی به سوی من، نرگس نیم باز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به درون بزم خویش از سر ناز خوانی ام</p></div>
<div class="m2"><p>آیم و از برون در، عرض کنم نیاز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وعده خلاف کرده ای با من وسازی ام خجل</p></div>
<div class="m2"><p>رنجه به فرض اگر کنی، لعل فسانه ساز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی خسته، بگسلد رشته عمر کوتهت</p></div>
<div class="m2"><p>رخصت سرکشی دهد، گر مژه دراز را</p></div></div>