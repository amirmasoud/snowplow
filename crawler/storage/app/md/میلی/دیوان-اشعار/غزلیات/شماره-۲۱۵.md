---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>بس که هر لحظه فریبی به زبان دگرم</p></div>
<div class="m2"><p>هر چه گویی، فکند دل به گمان دگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وه که هر چند مرا برد غم از حال به حال</p></div>
<div class="m2"><p>کرد سودای تو رسوا به نشان دگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس آمدنش برده قرار از من زار</p></div>
<div class="m2"><p>هر زمان وعده نماید به زمان دگرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا نهد هجر چنان بر سر خاکم که مگر</p></div>
<div class="m2"><p>هر زمان دسترسی هست به جان دگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جهان با کفن غرقه به خون خواهم رفت</p></div>
<div class="m2"><p>تا کند عشق تو رسوای جهان دگرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر خرسندی میلیّ و نرنجیدن غیر</p></div>
<div class="m2"><p>سخنی گفت نگاهش به زبان دگرم</p></div></div>