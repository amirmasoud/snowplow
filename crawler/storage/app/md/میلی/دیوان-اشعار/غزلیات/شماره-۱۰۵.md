---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>دعای عمر به این صید مبتلا چه کند</p></div>
<div class="m2"><p>به نیم کشته تیغ اجل، دعا چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اختلاط منش می‌شود حیا مانع</p></div>
<div class="m2"><p>ولی به جذبه عشقی چنین، حیا چه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نا امید دل من، که آزمود ترا</p></div>
<div class="m2"><p>فریب وعده وصل تو بی‌وفا چه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستمگری که فزاید زمان زمان نازش</p></div>
<div class="m2"><p>به او امید دل آرزو فزا چه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصال هم نزد آبی بر آتش میلی</p></div>
<div class="m2"><p>تو خود بگو که به این درد بی‌دوا چه کند</p></div></div>