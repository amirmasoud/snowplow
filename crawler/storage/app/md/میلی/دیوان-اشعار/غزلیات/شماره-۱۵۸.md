---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>دل چشم به راه یار ننهاد</p></div>
<div class="m2"><p>بر وعده او مدار ننهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز خیال او هم از ناز</p></div>
<div class="m2"><p>پا در دل بی‌قرار ننهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست جفای او، کسی دل</p></div>
<div class="m2"><p>بر بودن این دیار ننهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پای تو در میان نیامد</p></div>
<div class="m2"><p>جان رخت به یک کنار ننهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خون نگرفته بود دل را</p></div>
<div class="m2"><p>سر در پی آن سوار ننهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایام، بنای صبر ما را</p></div>
<div class="m2"><p>چون عهد تو استوار ننهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا با تو فتاد کار میلی</p></div>
<div class="m2"><p>سر در سر کارو بار ننهاد</p></div></div>