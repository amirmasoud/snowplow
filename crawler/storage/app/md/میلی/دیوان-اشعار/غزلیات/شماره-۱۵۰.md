---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>نشان من چو بتان از ستیز می‌جویند</p></div>
<div class="m2"><p>مرا نیافته شمشیر تیز می‌جویند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طرف دشت مگر گرد آن سوار نمود</p></div>
<div class="m2"><p>که آهوان همه راه گریز می‌جویند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو در کنار رقیبیّ و پاره‌های دلم</p></div>
<div class="m2"><p>ترا به دیده خونابه‌ریز می‌جویند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به راحتند شهیدان ز قتل خود، که ترا</p></div>
<div class="m2"><p>بدین بهانه دم رستخیر می‌جویند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنانکه مرغ زند پا به تیغ، ساده‌دلان</p></div>
<div class="m2"><p>نجات ازان مژه پرستیز می‌جویند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برآر حاجت آزادگان که چون میلی</p></div>
<div class="m2"><p>به گردن آن رسن مشک بیز می‌جویند</p></div></div>