---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>زبس که سوخته داغ جدایی تو مرا</p></div>
<div class="m2"><p>فسرده ساخته در آشنایی تو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که گرم وفای توام، عجب دانم</p></div>
<div class="m2"><p>که ناامید کند بی وفایی تو مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتادی ای دل وحشی به دست سنگدلی</p></div>
<div class="m2"><p>برو که نیست امید رهایی تو مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا در آتش هجران به حال من رحمی</p></div>
<div class="m2"><p>که سوخت جان ز محبت فزایی تو مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا به عجز چو در دست و پای او افتی</p></div>
<div class="m2"><p>خجالت است ز بی دست و پایی تو مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا به رندی و مستی شناختم میلی</p></div>
<div class="m2"><p>کجا فریب دهد پارسایی تو مرا</p></div></div>