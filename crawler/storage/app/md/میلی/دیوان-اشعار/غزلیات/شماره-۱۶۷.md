---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>سر ره چشم او بر دل نگیرد</p></div>
<div class="m2"><p>که صیاد آهوی بسمل نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان مغرور آن مشکین کمند است</p></div>
<div class="m2"><p>که صید خویش را غافل نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به این زاری که دل سر در پی اوست</p></div>
<div class="m2"><p>جرس دنباله محمل نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کو لذت آوارگی یافت</p></div>
<div class="m2"><p>به کوی عافیت منزل نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیا راه طلب بر من چنان بست</p></div>
<div class="m2"><p>که خونم راه بر قاتل نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگیر ای پندگو بر ما و میلی</p></div>
<div class="m2"><p>که بر دیوانگان عاقل نگیرد</p></div></div>