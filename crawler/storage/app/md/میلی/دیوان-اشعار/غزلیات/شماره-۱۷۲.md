---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>خدنگ تو چون ره به خون می‌برد</p></div>
<div class="m2"><p>مرا بر سرره جنون می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روز شکار تو تیر خدنگ</p></div>
<div class="m2"><p>بشارت به صید زبون می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنون بین که از بزم او همنشین</p></div>
<div class="m2"><p>به صد انفعالم برون می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جوش آید از رشک، خون دلم</p></div>
<div class="m2"><p>چو بر لب می لاله‌گون می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مستی خوشم،‌ تا نیابم خبر</p></div>
<div class="m2"><p>که بختم ز بزم تو چون می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فرمان جلاد مژگان او</p></div>
<div class="m2"><p>مرا خون گرفته‌ست و خون می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از ناامیدی چو میلی مرا</p></div>
<div class="m2"><p>ز کوی تو با صد فسون می‌برد</p></div></div>