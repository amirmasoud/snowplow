---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>آن‌چنان بدگمان که بود، نماند</p></div>
<div class="m2"><p>در پی امتحان که بود، نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وعده‌اش بی‌وفا نماند، که بود</p></div>
<div class="m2"><p>غمزه نامهربان که بود، نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف او سرکشی که کرد،‌ گذشت</p></div>
<div class="m2"><p>چشم او سرگران که بود، نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نزاعی که غیر با ما داشت</p></div>
<div class="m2"><p>پای او در میان که بود، نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهراً با رقیب، یار مرا</p></div>
<div class="m2"><p>التفات نهان که بود، نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر را هم ز ناشناسی حق</p></div>
<div class="m2"><p>رنجشی بر زبان که بود، نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر وفایش اگرچه ما را هم</p></div>
<div class="m2"><p>احتمالی چنانکه بود، نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر ما گر فتاد از فتراک</p></div>
<div class="m2"><p>غیر هم در عنان که بود، نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اگر از عنان او ماندم</p></div>
<div class="m2"><p>با من آن نیم جان که بود، نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمرها بر شکست میلی بود</p></div>
<div class="m2"><p>شکرللّه هر آن که بود، نماند</p></div></div>