---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>نیم بسمل شدم از غمزه خودکامی چند</p></div>
<div class="m2"><p>در دل آرام ندارم ز دلارامی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل، بسیار به هشیاری خود مغرور است</p></div>
<div class="m2"><p>ساقیا خیز و بده ازپی هم جامی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه بی‌گنهی خوشدلم از بسمل خویش</p></div>
<div class="m2"><p>گر به سوی من افتاده، نهی گامی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهد را پاس مدار و به سر وعده میا</p></div>
<div class="m2"><p>که تسلّی‌ست دلم با طمع خامی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قانعم شوق به این ساخته کز بهر فریب</p></div>
<div class="m2"><p>آورد غیر به سویم ز تو پیغامی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که در عربده بدمست مرا دید به خویش</p></div>
<div class="m2"><p>صد دعا کرد به شکرانه دشنامی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قیاس دل خود یافته میلی که ز تو</p></div>
<div class="m2"><p>چه رود بر دل شوریده سرانجامی چند</p></div></div>