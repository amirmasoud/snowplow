---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>رازم فسانه از نگه عاشقانه شد</p></div>
<div class="m2"><p>بیهوده رازدار خجل در میانه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسوایی‌ام ببین که ز شرم پیام من</p></div>
<div class="m2"><p>قاصد به سوی او نتواند روانه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بادهٔ خیال توام دوش دست داد</p></div>
<div class="m2"><p>کیفیتی،‌ که نالهٔ زارم ترانه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با غیر وعده داد و مرا چون ز دور دید</p></div>
<div class="m2"><p>برخاست از فریب و روان سوی خانه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوقم ببین که با همه غیرت، به بزم تو</p></div>
<div class="m2"><p>پیغام غیر، آمدنم را بهانه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی نیافت لذتی از بزم وصل تو</p></div>
<div class="m2"><p>از بس خجل ز آمدن بیخودانه شد</p></div></div>