---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>چند یارب غم دل در پی جان خواهد بود</p></div>
<div class="m2"><p>توسن شوق چنین سخت عنان خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کنندم به قیامت ز بد و نیک سوال</p></div>
<div class="m2"><p>حیرت روی توام بند زبان خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان یار و رقیبان سوی من می‌نگرند</p></div>
<div class="m2"><p>حرف خون ریختن من به میان خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این غمم کشت که در حشر خجل خواهی شد</p></div>
<div class="m2"><p>کز تو هر گوشه شهیدی به فغان خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی امشب که غم او شده همصحبت من</p></div>
<div class="m2"><p>ساغرم دیده خونابه‌فشان خواهد بود</p></div></div>