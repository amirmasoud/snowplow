---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>هر دشمنیی که بخت بد کرد</p></div>
<div class="m2"><p>عشق تو هم آمد و مدد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانگی دلم یقین شد</p></div>
<div class="m2"><p>زان روز که دعوی خرد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدخواه، متاع دوستی را</p></div>
<div class="m2"><p>سرمایهٔ کینه وحسد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر بخرید بنده‌ای را</p></div>
<div class="m2"><p>کاوّل به هزار عیب، رد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مرگ برو که غمزهٔ او</p></div>
<div class="m2"><p>تا آمدن تو، کار خود کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خواب ندیده بود میلی</p></div>
<div class="m2"><p>آسودگیی که در لحد کرد</p></div></div>