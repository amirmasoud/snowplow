---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>عشّاق که ترک ره دلدار گرفتند</p></div>
<div class="m2"><p>از غیرت همراهی اغیار گرفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا با خبر از صحبت اغیار نباشم</p></div>
<div class="m2"><p>در پیش من،‌ از هم، خبر یار گرفتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادم که نخواهد سوی اغیار نظر کرد</p></div>
<div class="m2"><p>در بزمش اگر جای من زار گرفتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امّید حمایت ز کسانی که مرا بود</p></div>
<div class="m2"><p>از ناکسی‌ام جانب اغیار گرفتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس که به عاشق‌طلبی نام برآورد</p></div>
<div class="m2"><p>خلقی سر راهش پی اظهار گرفتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی به سر راه تو جمعند رقیبان</p></div>
<div class="m2"><p>از یار مگر رخصت آزار گرفتند؟</p></div></div>