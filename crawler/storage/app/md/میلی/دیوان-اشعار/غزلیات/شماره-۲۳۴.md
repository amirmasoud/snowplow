---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>به بزم دوش چنان بود همزبان با من</p></div>
<div class="m2"><p>که غیبت دگران داشت در میان با من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان ز عشق نهانم رقیب بی‌خبر است</p></div>
<div class="m2"><p>که می‌کند سخن از صحبت نهان با من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز من رمیده، همانا شنیده از جایی</p></div>
<div class="m2"><p>حکایتی که نهان داشت در میان با من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاطرت نرسد امتحان من هرگز</p></div>
<div class="m2"><p>ز بس که غیر ترا کرده بدگمان با من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بدگمانی خود شرمسار خواهی شد</p></div>
<div class="m2"><p>مباش این‌همه در بند امتحان با من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان ز خوی بد آن بهانه‌جو، میلی</p></div>
<div class="m2"><p>که دوش عربده‌ای داشت هر زمان با من</p></div></div>