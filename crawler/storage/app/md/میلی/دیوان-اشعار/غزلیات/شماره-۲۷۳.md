---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ز من ای غیر در رشکی، دلت شاد است پنداری</p></div>
<div class="m2"><p>به استغنای او کارت نیفتاده‌ست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزمش رفته‌ام ناخوانده و بینم هراسانش</p></div>
<div class="m2"><p>نهان از من پی غیری فرستاده‌ست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غیر آن تندخو رنجیده و ظاهر نمی‌سازد</p></div>
<div class="m2"><p>بنای رنجش او سست بنیاد است پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وقت آشتی هر دم گناهم بر زبان آرد</p></div>
<div class="m2"><p>هنوز آن جنگجو در بند بیداد است پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز من بگذشته و دست رقیب از دست نگذارد</p></div>
<div class="m2"><p>هنوز او را ز چون من ناکسی یاد است پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان در هر تماشا حیرتم بر حیرت افزاید</p></div>
<div class="m2"><p>که چشمم بر رخش هرگز نیفتاده‌ست پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کف مرغ دل میلی به سوی نخل بالایش</p></div>
<div class="m2"><p>شتابان می‌رود، مرغ نوآزاد است پنداری</p></div></div>