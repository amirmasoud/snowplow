---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>ز عشوه بس که مرا بی‌قرار خود کردی</p></div>
<div class="m2"><p>خجل شدیّ و مرا شرمسار خود کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآمدن مگر امروز با رقیبانی؟</p></div>
<div class="m2"><p>که وعده‌ام به سر رهگذر خود کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حیله تو فزون بود ناامیدی من</p></div>
<div class="m2"><p>مرا ز سادگی امیدوار خود کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیب را که به کوی تو دیر می‌آید</p></div>
<div class="m2"><p>ز التفات مگر شرمسار خود کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببین رقیب، تفاوت میانه من و خویش</p></div>
<div class="m2"><p>که خواری‌ام سبب اعتبار خود کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای بزم که داری، که بازم از وعده</p></div>
<div class="m2"><p>اسیر سلسله انتظار خود کردی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شد که می‌گذری وحشیانه از میلی</p></div>
<div class="m2"><p>مگر به تازه کسی را شکار خود کردی؟</p></div></div>