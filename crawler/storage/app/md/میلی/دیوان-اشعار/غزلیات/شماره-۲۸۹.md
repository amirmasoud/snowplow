---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>بس که از ما بهر غیر ای بی‌وفا رنجیده‌ای</p></div>
<div class="m2"><p>پیش ما شرمنده‌ای، از غیر تا رنجیده‌ای؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست من! امشب نداری گوش بر درد دلم</p></div>
<div class="m2"><p>از شکایتهای دوشم ظاهرا رنجیده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که داری تلخکام از جام استغنا مرا</p></div>
<div class="m2"><p>نیستم آگه که داری صلح یا رنجیده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غایت غیرآشناییها همین باشد که تو</p></div>
<div class="m2"><p>بهر یک بیگانه از صد آشنا رنجیده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌سبب رنجیده‌ای، ترسم که گردی شرمسار</p></div>
<div class="m2"><p>گر کسی پرسد که از میلی چرا رنجیده‌ای</p></div></div>