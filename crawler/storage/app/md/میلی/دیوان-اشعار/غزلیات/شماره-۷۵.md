---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>پروانه گر ز خلق نهان درد و داغ داشت</p></div>
<div class="m2"><p>دست زمانه عاقبتش بر چراغ داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اظهار سرّ خویش مگر کرده‌ای به غیر</p></div>
<div class="m2"><p>کامروز پیشتر ز تو آهنگ باغ داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب در گرفت پنبه داغ دلم ز آه</p></div>
<div class="m2"><p>بیمار عشق بر سر بالین چراغ داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر جست‌وجوی من ز چه رو خنده زد رقیب</p></div>
<div class="m2"><p>آن مست را اگرنه به جایی سراغ داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی ز بوی مشک،‌ شب از هوش رفته بود</p></div>
<div class="m2"><p>سودای زلف یار مگر در دماغ داشت؟</p></div></div>