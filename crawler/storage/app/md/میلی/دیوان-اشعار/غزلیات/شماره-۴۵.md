---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>گر نیست فکر قتل منت، قهر بهر چیست</p></div>
<div class="m2"><p>بادام تلخ چشم تو پر زهر بهر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیداد غمزه تو هلاک مرا بس است</p></div>
<div class="m2"><p>جور زمانه و ستم دهر بهر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با صد ستم کنون که مرا آزموده‌ای</p></div>
<div class="m2"><p>هر دم غضب برای چه و قهر بهر چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او در پی هلاک من و خلق در عجب</p></div>
<div class="m2"><p>کاین صید خون گرفته درین شهر بهر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بهر تلخکامی میلی نیامدی</p></div>
<div class="m2"><p>در دست غمزه‌ات، قدح زهر بهر چیست</p></div></div>