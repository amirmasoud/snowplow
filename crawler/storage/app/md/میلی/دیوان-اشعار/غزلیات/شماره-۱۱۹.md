---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>سرگران دوش گذشتن زمن زار چه بود</p></div>
<div class="m2"><p>در پی بوالهوسان گرمی بازار چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود دیروز مگر وعده دیدار تو عام؟</p></div>
<div class="m2"><p>ورنه در کوی تو جمعیّت اغیار چه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرا ترا بود سر آنکه رسانی به وفا</p></div>
<div class="m2"><p>پیش اغیار به من وعده دیدار چه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جنون من و ناسازی او ظاهر نیست</p></div>
<div class="m2"><p>که میان من و او، مایه آزار چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه از جای دگر داشتی آزار ز من</p></div>
<div class="m2"><p>بهر اندک گنهی، رنجش بسیار چه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر اظهار نیازی که ز میلی می‌دید</p></div>
<div class="m2"><p>ناز او را سبب گرمی بازار چه بود</p></div></div>