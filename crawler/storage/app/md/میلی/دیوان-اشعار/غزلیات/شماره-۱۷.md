---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>به غضب تلخ مکن عیش من مسکین را</p></div>
<div class="m2"><p>سخن تلخ میاموز، لب شیرین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر زبان آر گناهی که نداریم و به ما</p></div>
<div class="m2"><p>کینه اندوز مکن خاطر مهر آیین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون ما بر تو حلال است، بکش تیغ و بریز</p></div>
<div class="m2"><p>باری از کینه تهی ساز دل پرکین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز دستم صنمی برد و چنین در پی او</p></div>
<div class="m2"><p>که منم، بر سر دل می نهم آخر دین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیچ از خانه دل می ننهد پای برون</p></div>
<div class="m2"><p>طفل شوخی ز که آموخته این تمکین را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خشم و بیداد ترا لطف نهان نام کنم</p></div>
<div class="m2"><p>به فریب از تو کنم شاد، دل غمگین را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید این طرفه غزل واسطه گردد میلی</p></div>
<div class="m2"><p>که به خاطر گذری شاه جمال الدین را</p></div></div>