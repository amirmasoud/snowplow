---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>از مستی شب، زلف تو بی‌تاب نماید</p></div>
<div class="m2"><p>از آتش می، لعل تو بی‌آب نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن تو ز آسیب نگاه هوس‌آلود</p></div>
<div class="m2"><p>چون مجلس بر هم زده اسباب نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چشم زدن، آهوی ناخفته شب تو</p></div>
<div class="m2"><p>اظهار خمار و هوس خواب نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان تو در پیش سر افکنده ازین شرم</p></div>
<div class="m2"><p>کر چشم تو آثار می‌ناب نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیفیت امشب گذرا بوده که امروز</p></div>
<div class="m2"><p>از رنج خمار این‌همه بی‌تاب نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون اشک من آن خانه‌نشین پرده‌دری کرد</p></div>
<div class="m2"><p>از شرم،‌ کنون چون در نایاب نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میلی شده پابسته آن زلف و به چشمش</p></div>
<div class="m2"><p>مژگان تو چون خنجر قصاب نماید</p></div></div>