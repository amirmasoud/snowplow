---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>چنین به اهل وفا خشمگین چرا شده‌ای</p></div>
<div class="m2"><p>سگ توایم،‌ به ما این چنین چرا شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث مدعیان گر نکرده‌ای باور</p></div>
<div class="m2"><p>به تازه بر سر بیداد و کین چرا شده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر وفا چو نداری، چرا نمی‌گویی</p></div>
<div class="m2"><p>که آفت دل و آشوب دین چرا شده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بر سر غضبی با من و درین فکرم</p></div>
<div class="m2"><p>که رنجه از من اندوهگین چرا شده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نه میل فراغت بود ترا میلی</p></div>
<div class="m2"><p>به کنج غم، به اجل همنشین چرا شده‌ای</p></div></div>