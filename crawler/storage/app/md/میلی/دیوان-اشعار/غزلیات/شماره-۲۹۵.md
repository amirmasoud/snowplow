---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>زان مژه غیر خدنگ بلا ندهی</p></div>
<div class="m2"><p>دامن غمزه به دست وفا ندهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وه که نیایی و وعده آمدنی</p></div>
<div class="m2"><p>بهر تسلی خاطر ما ندهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاف مصاحبت تو زنم بر غیر</p></div>
<div class="m2"><p>گرچه جواب سلام مرا ندهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتن اهل محبت اگر گنه است</p></div>
<div class="m2"><p>ترک گنه ز برای خدا ندهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست من ای غم و دامن تو که ز دست</p></div>
<div class="m2"><p>دامن میلی بی‌سر و پا ندهی</p></div></div>