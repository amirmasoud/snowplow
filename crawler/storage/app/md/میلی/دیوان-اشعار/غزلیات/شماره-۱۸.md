---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>گر بیخودی مجال دهد اضطراب را</p></div>
<div class="m2"><p>بنیاد برکند دل و جان خراب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عشق می گدازم و می سوزم و خوشم</p></div>
<div class="m2"><p>با آتش است صحبت گرمی کباب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت میان ما نشود گرم از حجاب</p></div>
<div class="m2"><p>کو محرمی که رفع کند این حجاب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میدان حرف داد سوال توام، ولی</p></div>
<div class="m2"><p>برتافت دست شرم، عنان جواب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خاطرت به سختی جان کندنم خوش است</p></div>
<div class="m2"><p>منت نهم به جان و کشم این عذاب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خوابگاه دیده بختم قرار یافت</p></div>
<div class="m2"><p>از چشم من چو گریه برون کرد خواب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میلی خوش آنکه مست رسد آن سوار و من</p></div>
<div class="m2"><p>گاهی عنان ببوسم و گاهی رکاب را</p></div></div>