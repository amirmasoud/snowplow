---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>می دهد ساقی می نابی که می سوزد مرا</p></div>
<div class="m2"><p>می زند بر آتشم آبی که می سوزد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رود آن مست، هشیارانه از پیشم، ولی</p></div>
<div class="m2"><p>نخل قدش می خورد تابی که می سوزد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز من افسانه غم نشنود شبهای وصل</p></div>
<div class="m2"><p>می شود از حیله در خوابی که می سوزد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خجل از تنگدستی سازدم در بزم خویش</p></div>
<div class="m2"><p>می نماید غیر، اسبابی که می سوزد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز چون میلی درین افسردگیها دیده ام</p></div>
<div class="m2"><p>روی خورشید جهانتابی که می سوزد مرا</p></div></div>