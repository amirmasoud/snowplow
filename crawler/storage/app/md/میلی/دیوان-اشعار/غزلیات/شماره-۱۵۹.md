---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ز مجلس دوست رفت و دشمن آمد</p></div>
<div class="m2"><p>دلا منشین که وقت رفتن آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جا دشوار خیزم، بس که بی او</p></div>
<div class="m2"><p>ز مژگان خون دل در دامن آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غمناکی، به گوشم صوت مطرب</p></div>
<div class="m2"><p>ملال‌انگیزتر از شیون آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زد آتش آه دل در پنبه داغ</p></div>
<div class="m2"><p>مرا برق بلا در خرمن آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گردن بینمت خون جهانی</p></div>
<div class="m2"><p>ترا دستی مگر در گردن آمد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا آن غمزه تیری بر جگر زد</p></div>
<div class="m2"><p>کزان صد چاک در پیراهن آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیامد، گر کسی رفت از پی دوست</p></div>
<div class="m2"><p>وگر آمد، به کام دشمن آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من نشنیده میلی وز پی‌اش رفت</p></div>
<div class="m2"><p>به آخر بر سر حرف من آمد</p></div></div>