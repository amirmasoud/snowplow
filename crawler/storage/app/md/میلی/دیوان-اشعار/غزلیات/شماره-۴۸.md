---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>آنکه رشک بت چین است، این است</p></div>
<div class="m2"><p>وانکه غارتگر دین است، این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرکشی، زارکُشی، بدکیشی</p></div>
<div class="m2"><p>که به ما بر سر کین است، این است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه از بهر هلاکم، او را</p></div>
<div class="m2"><p>خنجر از چنین جبین است، این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌رود یار و من از پی نالان</p></div>
<div class="m2"><p>که بلای دلم این است، این است!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی آن شوخ که در خانه زین</p></div>
<div class="m2"><p>فتنه روی زمین است، این است</p></div></div>