---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>کو بخت کز برآمدن کام دل ازو</p></div>
<div class="m2"><p>گردم ز ناامیدی خود منفعل ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاصد ز انفعال پیامم نمی‌برد</p></div>
<div class="m2"><p>بیند ز بس که در گله‌ام متصل ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با او چو همرهی کنم، از طعنه رقیب</p></div>
<div class="m2"><p>او منفعل ز من شود و من خجل ازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل دعوی محبت او بس که می‌کند</p></div>
<div class="m2"><p>شرم آیدم که شکوه کنم پیش دل ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد به رنگ میلی خونین کفن مرا</p></div>
<div class="m2"><p>دستی به سر چو لاله و پایی به گل ازو</p></div></div>