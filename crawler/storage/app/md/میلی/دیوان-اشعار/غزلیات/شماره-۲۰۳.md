---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>مردم و جان به غم یار نهانی مشتاق</p></div>
<div class="m2"><p>دل ز جان بیش به آن همدم جانی مشتاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای اجل، منت ناآمدن خویش منه</p></div>
<div class="m2"><p>که کسی نیست درین عالم فانی مشتاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاصدا بی‌خبر از دیدن او گشتی و من</p></div>
<div class="m2"><p>به امیدی که پیامی برسانی مشتاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم خونریز، شهیدان مژده بر هم نزنند</p></div>
<div class="m2"><p>بس که هستند به آن نخل جوانی مشتاق(؟)</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده صد لطف نمایان ز تو غیر از نزدیک</p></div>
<div class="m2"><p>میلی از دور به یک لطف نهانی مشتاق</p></div></div>