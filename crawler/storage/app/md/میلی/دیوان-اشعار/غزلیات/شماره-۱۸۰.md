---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>صف شکن ترک مرا بر سر مرکب نگرید</p></div>
<div class="m2"><p>صف مژگان به خونریز مرتّب نگرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای غریبان، به فریب نگهش دل مدهید</p></div>
<div class="m2"><p>خلق شهری همه در ناله یارب نگرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز آزادی آن طفل رساند خبری</p></div>
<div class="m2"><p>قاصد اشک مرا در ره مکتب نگرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من به خوناب جگر خوردن و در بزم رقیب</p></div>
<div class="m2"><p>دم‌به‌دم بر لب او جام لبالب نگرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز بختم ز شب هجر بسی تیره‌تر است</p></div>
<div class="m2"><p>با چنین تیرگی روز سیه، شب نگرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که آسوده ز بیهوشی دوشین بودم</p></div>
<div class="m2"><p>بی‌قرار امشبم از بیخودی تب نگرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میلی از کفر سر زلب بتی می‌لافد</p></div>
<div class="m2"><p>با همه مشرب ازو دعوی مذهب نگرید</p></div></div>