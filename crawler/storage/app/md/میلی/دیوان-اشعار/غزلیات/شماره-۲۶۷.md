---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>دل و دیده را نباشد،‌ چه نهان چه آشکاره</p></div>
<div class="m2"><p>نه تحمل صبوری، نه تهوّر نظاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تو بس که ناامیدم،‌ به گمان خود نیفتم</p></div>
<div class="m2"><p>اگرم به جانب خود طلبی به صد اشاره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروید چاره‌جویان، پی کار خود، که دیگر</p></div>
<div class="m2"><p>به رمیده آهوی جان، نرسد کمند چاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کجاست بخت آنم، که به مجمع رقیبان</p></div>
<div class="m2"><p>چو مرا ز دور بیند،‌ کند از میان کناره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود شرار آهم دم واپسین، که با جان</p></div>
<div class="m2"><p>به مشایعت برآید،‌ جگر هزار پاره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تب جانگداز میلی، ز عرق نیافت تسکین</p></div>
<div class="m2"><p>چه خلل پذیرد آتش، ز ترددّ شراره</p></div></div>