---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چشم مستی باز رهزن شد دل دیوانه را</p></div>
<div class="m2"><p>کز نگاهی آشناه زد راه صد بیگانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گمان کز غیر ناگه پیشتر بیخود شوم</p></div>
<div class="m2"><p>خون شود دل در برم، چون پر دهی پیمانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم او کز اول آویزد به مردم، از فریب</p></div>
<div class="m2"><p>همچو صیادی به روی دام پاشد دانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی بزمش می روم ناخوانده و شادم ازین</p></div>
<div class="m2"><p>گرچه از شادی نخواهم یافت راه خانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شراب عاشقی، کیفیتی دارد مگر؟</p></div>
<div class="m2"><p>شعله کز یک جرعه بیخود می کند پروانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که شد ناخوانده میلی سوی یار، از خشم او</p></div>
<div class="m2"><p>بست بر همصحبتان راه و در کاشانه را</p></div></div>