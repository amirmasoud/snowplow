---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>کاروان رفته و تنها من بی‌دل مانده</p></div>
<div class="m2"><p>بی‌خداوند سگی در ته منزل مانده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار آهنگ سفر کرده و نالان‌نالان</p></div>
<div class="m2"><p>دل چاکم چو جرس در پی محمل مانده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به دنباله محمل نروم، معذورم</p></div>
<div class="m2"><p>که مرا پای ز سیل مژه در گل مانده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعل با نعل نپیوسته مرا سر تا پای</p></div>
<div class="m2"><p>که مرا عشق تو در قید سلاسل مانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی آن شه که تغافل به گدایان نزند</p></div>
<div class="m2"><p>بخت بد بین که ز احوال تو غافل مانده</p></div></div>