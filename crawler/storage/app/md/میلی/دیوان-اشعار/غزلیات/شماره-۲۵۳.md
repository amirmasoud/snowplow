---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>نمی‌بیند به سویم چون روم تنها به راه او</p></div>
<div class="m2"><p>نمی‌خواهد که خاص چون منی باشد نگاه او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین رشکم که گویا مدعای غیر شد حاصل</p></div>
<div class="m2"><p>که بوی آرزومندی نمی‌آید ز آه او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا بر حسرت امیدواری گریه می‌آید</p></div>
<div class="m2"><p>که باشد چون تویی از سادگی امیدگاه او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو بیند غیر، مخصوصانه در دستم عنانش را</p></div>
<div class="m2"><p>پی رفع گمان، خود را نمایم دادخواه او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به این تقریر بی‌تابانه، میلی شکوه کمتر کن</p></div>
<div class="m2"><p>مبادا در گریبان تو آویزد گناه او</p></div></div>