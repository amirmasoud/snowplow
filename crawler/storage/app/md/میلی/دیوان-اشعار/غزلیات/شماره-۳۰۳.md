---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>دی شدی مست می ناب و خرابم کردی</p></div>
<div class="m2"><p>داغ بر دست نهادیّ و کبابم کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناصح از من بگذر دیگر و بگذار مرا</p></div>
<div class="m2"><p>چه شدم، این همه کز پند عذابم کردی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در خانه غارت‌زده چشمم باز است</p></div>
<div class="m2"><p>تا سپاه مژه را رهزن خوابم کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود ایمن ز خلل، عافیت‌آباد دلم</p></div>
<div class="m2"><p>تو به یک چشم زدن خانه‌خرابم کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که می‌خواندم ازین پیش به طاعت همه را</p></div>
<div class="m2"><p>ناطلب رفته هر بزم شرابم کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سوال تو کند حیرت میلی افزون</p></div>
<div class="m2"><p>به فسون، بسته زبان وقت جوابم کردی</p></div></div>