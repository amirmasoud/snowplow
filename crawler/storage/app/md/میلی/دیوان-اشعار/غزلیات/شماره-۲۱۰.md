---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>به وقت صلح چو در شکوه پیش یار شدم</p></div>
<div class="m2"><p>گنه ز جانب من بود، شرمسار شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون ازو به کدام آبرو وفا طلبم؟</p></div>
<div class="m2"><p>که ساختم به جفا آنقدر که خوار شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هلاک آن نگه آشنا شوم که ازان</p></div>
<div class="m2"><p>به همزبانی پنهان امیدوار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دور برتنم ای جان رفته، حسرت خور</p></div>
<div class="m2"><p>که بسته خم فتراک آن سوار شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبر دهید به آزادگان که چون میلی</p></div>
<div class="m2"><p>شکار غمزه آن آدمی شکار شدم</p></div></div>