---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>نه شبنم بر زمین از یاسمین ریخت</p></div>
<div class="m2"><p>که پیشت آبرویش بر زمین ریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهادم آستین بر دامن چشم</p></div>
<div class="m2"><p>مرا چون شیشه خون از آستین ریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قصد کشتنم، در ساغر چشم</p></div>
<div class="m2"><p>نگاه خشمگینش زهر کین ریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو زلفت آستین بر عنبر افشاند</p></div>
<div class="m2"><p>رخت در دامن گل، مشک چین ریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بزم آرزو، چون شمع، میلی</p></div>
<div class="m2"><p>سرشک گرم زآه آتشین ریخت</p></div></div>