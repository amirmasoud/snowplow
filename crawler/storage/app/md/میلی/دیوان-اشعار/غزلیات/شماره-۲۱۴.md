---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>ز من غافل چو گردد از غرور حسن، بد خویم</p></div>
<div class="m2"><p>به تقریبی کنم هر دم سخن، تا بنگرد سویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کوی دوست رفتم از جفای دشمنان، یا رب</p></div>
<div class="m2"><p>چه او را بگذرد در دل نبیند چون در آن کویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجابش تا نگردد مانع دشنام، هر ساعت</p></div>
<div class="m2"><p>به بزم او حکایتهای گستاخانه می‌گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بختم کند یاری که تنها بینمت جایی</p></div>
<div class="m2"><p>حجاب حسن نگذارد ترا تا بنگری سویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین بس حاصل دیوانگی میلی که هر ساعت</p></div>
<div class="m2"><p>به سویم سنگ در کف می‌دود طفل جفا جویم</p></div></div>