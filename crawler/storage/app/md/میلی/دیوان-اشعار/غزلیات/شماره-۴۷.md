---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>شرمنده‌ام که روی دلی چون نمود و رفت</p></div>
<div class="m2"><p>صد سرزنیش ز ناکسی من شنود و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر فریب ساده‌ دلان، مست ناز من</p></div>
<div class="m2"><p>از بزم غیر آمد و خود را نمود و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد قحط آرزو به دل زود قانعم</p></div>
<div class="m2"><p>با آنکه دیر آمد و یک دم نبود و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بار دیگرش نتواند فریب داد</p></div>
<div class="m2"><p>او را گذاشت غیر که برخاست زود و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی خیال یار به دل ناگهان گذشت</p></div>
<div class="m2"><p>بازم به دل محبّت دیگر فزود و رفت</p></div></div>