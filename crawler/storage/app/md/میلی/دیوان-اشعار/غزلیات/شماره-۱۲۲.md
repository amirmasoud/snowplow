---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>تا دمی دامان وصلش دست شوقم سر دهد</p></div>
<div class="m2"><p>هر زمان بهر فریبم وعده دیگر دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل زعشقم جمع کرده و راندم از کوی خویش</p></div>
<div class="m2"><p>همچو صیّادی که صید نیم بسمل سر دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاب چون آرم، که یاد مهربانیهای او</p></div>
<div class="m2"><p>هر زمان دلداری شوق هجوم آور دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خلاف وعده‌ام شد منعفل، وز اضطراب</p></div>
<div class="m2"><p>رفت از یادش که بازم وعده دیگر دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال میلی با شدش خاطرنشان، از اعتماد</p></div>
<div class="m2"><p>چون خورد می‌با رقیبان، باده‌اش کمتر دهد</p></div></div>