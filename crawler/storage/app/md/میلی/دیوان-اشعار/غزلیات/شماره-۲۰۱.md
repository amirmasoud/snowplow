---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>هر لحظه مرا ذوق محبّت برد از هوش</p></div>
<div class="m2"><p>شبها که کنم با غم او دست در آغوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دم نتوانم به خیال تو به سر برد</p></div>
<div class="m2"><p>زان رو که چو یاد تو کنم، می‌روم از هوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم به رهت چشم، اجل زودتر امشب</p></div>
<div class="m2"><p>خواهم نکنی از من دلخسته فراموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس راز شهیدان تو نشنید، که بودند</p></div>
<div class="m2"><p>از حیرت نظّاره دیدار تو خاموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلسوزی میلی نکند کس دم مردن</p></div>
<div class="m2"><p>جز داغ غمش کز غم او گشته سیه‌پوش</p></div></div>