---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>از جا دلم به جلوه حیرت‌فزا مبر</p></div>
<div class="m2"><p>جا در دلم بگیر و دلم را ز جا مبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بشنود حدیث ملال‌آور ترا</p></div>
<div class="m2"><p>قاصد، پیام ما ببر و نام ما مبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیگانه کردم از تو به صد حیله خویش را</p></div>
<div class="m2"><p>بازم ز ره به یک نگه آشنا مبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد هوای بزم رقیب، آفتاب من</p></div>
<div class="m2"><p>ای شوق، همچو سایه مرا از قفا مبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دم دلا مگو که مرا درد او دواست</p></div>
<div class="m2"><p>نادردمند! این هم نام دوا مبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی چنین که بر جا بر او کرده مدعی</p></div>
<div class="m2"><p>بیهوده رنج در طلب مدعا مبر</p></div></div>