---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>چون دیدی‌ام، نظر به زمین دوختن چه بود</p></div>
<div class="m2"><p>در پیش سرفکندن و افروختن چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که کار عشق من از امتحان گذشت</p></div>
<div class="m2"><p>آن غمزه را ستمگری آموختن چه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خشنودی رقیب، غرض گر نداشتی</p></div>
<div class="m2"><p>بی موجبم به داغ جفا سوختن چه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعت ز آشنایی من گر نکرده غیر</p></div>
<div class="m2"><p>در پیش من، ز دیدنش افروختن چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی به یک نظاره چو از دست رفته‌ای</p></div>
<div class="m2"><p>عمری غرض ز عافیت اندوختن چه بود</p></div></div>