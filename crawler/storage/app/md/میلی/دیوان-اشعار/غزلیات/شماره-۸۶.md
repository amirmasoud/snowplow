---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>حرفی که از کسی نشنیدی، پیام ماست</p></div>
<div class="m2"><p>در نامهٔ تو آنچه نگنجید، نام ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه بست آن گل خود رو لب از جواب</p></div>
<div class="m2"><p>شرمندهٔ رقیب ز ننگ سلام ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل ز شکوه بهر خدا رنجه‌اش مساز</p></div>
<div class="m2"><p>آن آهوی رمیده زمانی که رام ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را ز بس که یار فراموش کرده است</p></div>
<div class="m2"><p>قاصد در انفعال ز عرض پیام ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی ز بس که ما و تو بدنام گشته‌ایم</p></div>
<div class="m2"><p>هر بد که می‌کنند رقیبان، به نام ماست</p></div></div>