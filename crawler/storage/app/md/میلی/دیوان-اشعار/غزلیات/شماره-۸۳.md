---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>گر یار به سوی دیگران رفت</p></div>
<div class="m2"><p>سوی دگر نمی‌توان رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فریاد که مدعی ز پیشم</p></div>
<div class="m2"><p>با قاصد یار،‌ همزمان رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشناخت ز مستی‌اش همانا</p></div>
<div class="m2"><p>کزپیش رقیب سرگران رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا درد دلم نگیرد آرام</p></div>
<div class="m2"><p>از بزم تو مدعی نهان رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی نتوان ازو خبر یافت</p></div>
<div class="m2"><p>از بس که به بزم این وآن رفت</p></div></div>