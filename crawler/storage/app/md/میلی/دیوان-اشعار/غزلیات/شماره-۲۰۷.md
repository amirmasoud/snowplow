---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>حرفی به تو از بیم سخنساز نگویم</p></div>
<div class="m2"><p>صد بار برآرم نفس و باز نگویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نسرسانم سخنی از تو به انجام</p></div>
<div class="m2"><p>کز شوق، دگر بار زآغاز نگویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اغیار چنان محرم رازند که از بیم</p></div>
<div class="m2"><p>یک حرف به آن پرده برانداز نگویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غایت غیرت، من دیوانه به خود هم</p></div>
<div class="m2"><p>حرفی که ازو می‌شنوم، باز نگویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند که دارم گله از غیر چو میلی</p></div>
<div class="m2"><p>بهتر که به آن غمزه غمّاز نگویم</p></div></div>