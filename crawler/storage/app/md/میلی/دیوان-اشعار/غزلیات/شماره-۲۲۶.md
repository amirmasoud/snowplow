---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>از بس که بزم مهر و وفا گرم کرده‌ام</p></div>
<div class="m2"><p>بازار التفات ترا گرم کرده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایام، چون فتیله داغم تمام سوخت</p></div>
<div class="m2"><p>تا همچو شمع، پیش تو جا گرم کرده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من ز اعتماد وفا بر سر جفاست</p></div>
<div class="m2"><p>هنگامه جفا، به وفا گرم کرده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن طفل را ز گرمی اظهار عاشقی</p></div>
<div class="m2"><p>در عشوه با وجود حیا گرم کرده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوبان، ترحمی! که چو میلی در آتشم</p></div>
<div class="m2"><p>تا اختلاط را به شما گرم کرده‌ام</p></div></div>