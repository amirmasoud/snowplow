---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>افغان که مرا همنفسان وانگذارند</p></div>
<div class="m2"><p>یک دم به سر راه تو تنها نگذارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویا سر آزار نداری،‌ که رقیبان</p></div>
<div class="m2"><p>بازم به سر راه تمنا نگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بزم تو بیرون ننهم پا که مبادا</p></div>
<div class="m2"><p>تا آمدنم اهل حسد جا نگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را ز بس آزار کنی پیش رقیبان</p></div>
<div class="m2"><p>خیزیم ز بزم تو و ما را نگذارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میلی چه روی سر زده هر روز به بزمش</p></div>
<div class="m2"><p>اندیشه نداری که مبادا نگذارند؟</p></div></div>