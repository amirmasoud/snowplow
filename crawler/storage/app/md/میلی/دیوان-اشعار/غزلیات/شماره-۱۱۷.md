---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>چو همرهی به من آن سرو خوشخرام کند</p></div>
<div class="m2"><p>ز بیم طعنه، به هرکس رسد سلام کند!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال وصل تو در خاطر است خلقی را</p></div>
<div class="m2"><p>کسی ملاحظه خاطر کدام کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دیدن تو دلم یافت لذّتی که فلک</p></div>
<div class="m2"><p>نعوذباللّه اگر فکر انتقام کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسانده مردن دل به آنکه گر خواهد</p></div>
<div class="m2"><p>به یک نگاه دگر، کار خود تمام کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آشنا و نه بیگانه‌ای، نمی‌دانم</p></div>
<div class="m2"><p>که اختلاط چنین را کسی به نام کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آن رسیده که میلی ز تلخکامی هجر</p></div>
<div class="m2"><p>می وصال تو بر خویشتن حرام کند</p></div></div>