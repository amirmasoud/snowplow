---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>با آنکه هر زمان شوم از غصه زارتر</p></div>
<div class="m2"><p>گردم زمان زمان به تو امّیدوارتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بیندم زگریه مستانه شرمسار</p></div>
<div class="m2"><p>در خنده می‌شود که شوم شرمسارتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنجاندم ز وعده خلافی، ولی چه سود</p></div>
<div class="m2"><p>از رنجشی ز وعده او بی‌مدارتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شوخیی چنین، که نگیری دمی قرار</p></div>
<div class="m2"><p>در دل گذر مکن که شود بی‌قرارتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ذوق خواری از دگران بیشتر برم</p></div>
<div class="m2"><p>از من کسی مباد به کوی تو خوارتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمنامه‌ام به غیر نمایی به این غرض</p></div>
<div class="m2"><p>کز ناامیدی‌ام شود امیدوارتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌اعتبار پیش تو خلقی به جرم عشق</p></div>
<div class="m2"><p>بیچاره میلی از همه بی‌اعتبارتر</p></div></div>