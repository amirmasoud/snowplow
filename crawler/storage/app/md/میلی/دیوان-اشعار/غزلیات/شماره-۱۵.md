---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>کدام بت شده رهزن دل چو سنگ ترا</p></div>
<div class="m2"><p>که آفتاب محبت، شکسته رنگ ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد از عتاب تو افزون، امیدواری غیر</p></div>
<div class="m2"><p>زبس که مصلحت آمیز دید جنگ ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآمدی و ندارم چو باد گستاخی</p></div>
<div class="m2"><p>که همچو گل بگشایم قبای تنگ ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ننگ غیر، دلم جان سپرد و نام نبرد</p></div>
<div class="m2"><p>زبس ملاحظه می کرد نام و ننگ ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرشمه های تو از بس که هست نازآمیز</p></div>
<div class="m2"><p>نه آشتی تو داند کسی، نه جنگ ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم ز زخم تو آسوده است و می نالم</p></div>
<div class="m2"><p>که غیر پی نبرد لذت خدنگ ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس که میلی امیدوار، ساده دل است</p></div>
<div class="m2"><p>خیال مهر و وفا کرده ریو ورنگ ترا</p></div></div>