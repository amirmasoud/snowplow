---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>جفای او من بی‌تاب را به جا نگذاشت</p></div>
<div class="m2"><p>برآن شدم که شکایت کنم، وفا نگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا تو حال چه دانی، که بیخودی هرگز</p></div>
<div class="m2"><p>ز ابتدا سخنم را به انتها نگذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه را، شه من، حق به جانب است درین</p></div>
<div class="m2"><p>که دامن تو به دست من گدا نگذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشم که مدّعیان را به گاه خواهش کام</p></div>
<div class="m2"><p>خجالت تو به اظهار مدّعا نگذاشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم به پنجه عشق تو از توتیای شوق، مرا</p></div>
<div class="m2"><p>گل ملاحظه در دیده حیا نگذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب وصل تو از توتیای شوق، مرا</p></div>
<div class="m2"><p>گل ملاحظه‌ای در دیدهٔ حیا نگذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال آن بت بیگانه، در دل میلی</p></div>
<div class="m2"><p>هوای صحبت یاران آشنا نگذاشت</p></div></div>