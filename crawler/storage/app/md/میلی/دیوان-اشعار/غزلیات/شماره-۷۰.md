---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>ترا با کسی هوای همدمی نیست</p></div>
<div class="m2"><p>پری را الفتی با آدمی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا سرگرم باش از ساغر غم</p></div>
<div class="m2"><p>که مستی در شراب بی‌غمی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر اعجاز دارد ساغر عشق</p></div>
<div class="m2"><p>که از دریا‌کشان می را کمی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگو ای دل غم خود با خیالش</p></div>
<div class="m2"><p>که بر کس اعتماد محرمی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلی کز تربت میلی زند سر</p></div>
<div class="m2"><p>درو چون غنچه دل، خرمی نیست</p></div></div>