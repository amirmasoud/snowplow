---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>دل به جان شب همه شب ز آه و فغانم دارد</p></div>
<div class="m2"><p>این سیه روز ندانم چه به جانم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق پنهان کنم و هر که به سویم نگرد</p></div>
<div class="m2"><p>دل تپد، کاین خبر از سوز نهانم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چنین پرده برانداز که او را دیدم</p></div>
<div class="m2"><p>عنقریب است که رسوای جهانم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نیاید به زبان، آنچه به دل دارم ازو</p></div>
<div class="m2"><p>چشم افسونگر او بسته زبانم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دلخسته که لب تشنه شمشیر توام</p></div>
<div class="m2"><p>گر همه آب حیات است، زیانم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که بی‌تابی‌ام از عشق خود افزون بیند</p></div>
<div class="m2"><p>تهمت آلود به عشق دگرانم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو میلی کندم شهره تقاضای جنون</p></div>
<div class="m2"><p>عشق هرچند که بی نام و نشانم دارد</p></div></div>