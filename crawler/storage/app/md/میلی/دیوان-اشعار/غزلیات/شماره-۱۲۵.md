---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>زان زهر چشم، بس که دلم تلخکام شد</p></div>
<div class="m2"><p>برکام او شراب تمنّا حرام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردم سلام و فایده این یافتم که دل</p></div>
<div class="m2"><p>نومید بعد ازین ز جواب سلام شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیون نرفت لذّت وصل تو از دلم</p></div>
<div class="m2"><p>هرچند هجر در صدد انتقام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل با صد اهتمام ندید از لب تو کام</p></div>
<div class="m2"><p>نومید آخر از تو به سعی تمام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کین زمانه را شده شرمایه فریب</p></div>
<div class="m2"><p>شرین لبی که جانم ازو تلخکام شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی به لطف او ننهی دل، که پیش ازین</p></div>
<div class="m2"><p>مخصوص بنده بود نگاهی که عام شد</p></div></div>