---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>دیده از دیدن تو بس نکند</p></div>
<div class="m2"><p>با لبت دل به جان هوس نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه عیسی دمی ولی طالع</p></div>
<div class="m2"><p>یک دمم با تو هم نفس نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه با ما رقیب کرد ز جور</p></div>
<div class="m2"><p>غیر سگ با غریب کس نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بی باک ما در آن خم زلف</p></div>
<div class="m2"><p>تیز تر رفت و رو به پس نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه با شاهدی بکرد  فراق</p></div>
<div class="m2"><p>سوز آتش به خار و خس نکند</p></div></div>