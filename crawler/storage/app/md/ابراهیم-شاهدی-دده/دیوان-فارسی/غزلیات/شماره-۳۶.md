---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>آنکو دل خود به خار ننهاد</p></div>
<div class="m2"><p>گل گل شد و در کنار ننهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکی که به خاک ره نیامیخت</p></div>
<div class="m2"><p>سر در قدم نگار ننهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقلی که به زیر بار نفس است</p></div>
<div class="m2"><p>از کار نماند بار ننهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کار جهان چو دل بپرداخت</p></div>
<div class="m2"><p>پا در ره کار بار ننهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قسام ازل چو کرد قسمت</p></div>
<div class="m2"><p>اندر دل ما قرار ننهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شاهدی از غم تو برشد</p></div>
<div class="m2"><p>دل بر غم روزگار ننهاد</p></div></div>