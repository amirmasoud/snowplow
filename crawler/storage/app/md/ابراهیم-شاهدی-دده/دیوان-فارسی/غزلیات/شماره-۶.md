---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>به عاشقان خود بنما جمال عالم آرا را</p></div>
<div class="m2"><p>که دل پر خون شد از شوق تو مشتاقان شیدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توهم ای عقل نامحرم برون شو از سرای دل</p></div>
<div class="m2"><p>که از بهر خیال دوست خالی می کنم جا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا جانا اگر خواهی تماشای لب آبی</p></div>
<div class="m2"><p>نشین بر گوشۀ چشم و ببین این موج دریا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضای دلگشا دادند از فیض رخت لیکن</p></div>
<div class="m2"><p>نشین بر دیده گه گاه و ببین سرچشمه ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ کویش به تنگ آمد ز آه و ناله‌ات هرشب</p></div>
<div class="m2"><p>برو ای شاهدی یک شب ببر زین کوچه غوغا را</p></div></div>