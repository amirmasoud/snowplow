---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>تا مردمک دیده من حسن تو دیده</p></div>
<div class="m2"><p>دیگر رقمی از اثر خواب ندیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نور رخت مجلس ما ساز منور</p></div>
<div class="m2"><p>ای چشم و چراغ من و ای نور دو دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از باغ جمال تو خجل گشته ریاحین</p></div>
<div class="m2"><p>تا خط تو بر گرد گل و لاله دمیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیا برسد بر اثر نعل سمندت</p></div>
<div class="m2"><p>این خیل سرشکم که همه عمر دویده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر عجبم زاهد آن چشم که شیران</p></div>
<div class="m2"><p>از سهم خدنگ مژه‌اش جمله رمیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکر مشو ای شیخ ز افغان دل چنگ</p></div>
<div class="m2"><p>هست از رگ جان ناله آن پیر خمیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شاهدی از نظم تو جانها به سماعند</p></div>
<div class="m2"><p>زیرا که بود میوه خوش طعم رسیده</p></div></div>