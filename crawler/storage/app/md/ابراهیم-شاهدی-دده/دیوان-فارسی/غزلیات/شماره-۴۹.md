---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>تیرت ار پیکان خونین از جگر بیرون برد</p></div>
<div class="m2"><p>جان شیرین تیره رخت از هر گذر بیرون برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خیالت جان من در تن بسی دل خوش بود</p></div>
<div class="m2"><p>لیک ترسم سیل اشکش از نظر بیرون برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می پزم سودای زهد و توبه و آمد بهار</p></div>
<div class="m2"><p>کو می صافی که این سودا ز سر بیرون برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرخطر راهست راه عشق و ما بی زاد و آب</p></div>
<div class="m2"><p>سیل اشک ما مگر هم زین خطر بیرون برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرخ رو گردد به نزد عاشقانت شاهدی</p></div>
<div class="b" id="bn6"><div class="m1"><p>خون دل را دم به دم از دیده گر بیرون برد</p></div>