---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>قدم به پرسش من رنجه کن به رسم عیادت</p></div>
<div class="m2"><p>که جان نثار قدومت کنم ز روی ارادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهاده‌ام سر تسلیم بر ارادت محبوب</p></div>
<div class="m2"><p>که بنده را نبود رسم و راه غیر عبادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا بکوش که خود را کنی نشانه تیرش</p></div>
<div class="m2"><p>که این بود به حقیقت نشان گلیم سعادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسوزش دل و خون جگر گواه چه حاجت</p></div>
<div class="m2"><p>سرشک سرخ و رخ زرد بس بود به شهادت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخون وصل تو آمد چو شاهدی بگدایی</p></div>
<div class="m2"><p>به بوسه ای بنوازش که این بس است زیادت</p></div></div>