---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>کاری ندارم در جهان جز گریه در کردار خود</p></div>
<div class="m2"><p>چون من مبادا هیچ کس درمانده ا ندر کار خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرو قد سیم تن وی گل رخ نازک بدن</p></div>
<div class="m2"><p>از دوستان بشنو سخن خواری مکن با یار خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لعل لبت با جان قرین زیر لبت در ثمین</p></div>
<div class="m2"><p>با حسن لطف این چنین ضایع مکن بازار خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با چهره بهتر از مهی قدت به از سرو سهی</p></div>
<div class="m2"><p>بوسی به جانی می دهی بس خود بکن بازار خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد شاهدی چون در فنون در قید زلف تو زبون</p></div>
<div class="m2"><p>بگذار کز سوز درون گرید به حال زار خود</p></div></div>