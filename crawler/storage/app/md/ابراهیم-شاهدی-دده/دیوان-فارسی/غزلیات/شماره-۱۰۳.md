---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>مرا گر هجر دل بر آتش است و دیده دریا هم</p></div>
<div class="m2"><p>کجا دل می کشد با باغ و با گل گشت و صحرا هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سگانت را به روزعرض هر یک را نهی کامی</p></div>
<div class="m2"><p>چه شد گر یاد آری در میان مردم از ما هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا یک روز خالی نیست از دردت دلم یکدم</p></div>
<div class="m2"><p>نمی خواهم که بی درد تو باشم روز فردا هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تشریف شهادت چون رسانی جان مشتاقان</p></div>
<div class="m2"><p>بدین دولت مزین ساز از تیغ تو ما ر ا هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو تا چون ننالد شاهدی تا صبح دم هر شب</p></div>
<div class="m2"><p>که نبود مونسش غیر از غم و درد و بلا با هم</p></div></div>