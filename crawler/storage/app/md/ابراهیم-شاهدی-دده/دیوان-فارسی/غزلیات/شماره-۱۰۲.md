---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>به عشق آن میان شد مدتی با من سمر بندم</p></div>
<div class="m2"><p>همی خواهم که من با کاکلش سودا به سر بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زنجیر سر زلفش دلم در قید محکم بود</p></div>
<div class="m2"><p>فروزان کاکلش بر سر بهر سو بند بر بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بگرفت در غربت کجایی ساربان آخر</p></div>
<div class="m2"><p>کزین دهر خراب آباد رخت خویش بربندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو از خلق جهانم می رسد محنت به هر جایی</p></div>
<div class="m2"><p>به کنج عزلتی بنشینم و بر خلق در بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از روز ازل با یار ما ر ا عهد محکم بود</p></div>
<div class="m2"><p>بگو ای شاهدی من دل به دلدار دگر بر بندم</p></div></div>