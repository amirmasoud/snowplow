---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>خواهم که با تو قصه خود در میان نهم</p></div>
<div class="m2"><p>چون بینمت ز شوق گره بر زبان نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لوح جان نماند گمان و خیال و وهم</p></div>
<div class="m2"><p>از بس که داغ درد تو بر لوح جان نهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم هوای آنکه شوم خاک پای تو</p></div>
<div class="m2"><p>من کز شرف قدم به سر فرقدان نهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش دهان تنگ تو چون آیدم به دل</p></div>
<div class="m2"><p>مهر نگین ختم سلیمان بدان نهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر پسته دم زند ز دهان تو باک نیست</p></div>
<div class="m2"><p>مغزش کنم ز غیرت و اندر دهان نهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانا بگو که شاهدی از خادمان ماست</p></div>
<div class="m2"><p>کآیم رخ نیاز بر آن خاندان نهم</p></div></div>