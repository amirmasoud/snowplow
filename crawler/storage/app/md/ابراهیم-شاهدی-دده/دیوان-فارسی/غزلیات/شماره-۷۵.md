---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>مرا گر در نظر گلزار باشد</p></div>
<div class="m2"><p>دلم بر روی آن گل زار باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و انکار می در موسم گل</p></div>
<div class="m2"><p>درین کارم بسی انکار باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غیر از صبر نبود چاره او</p></div>
<div class="m2"><p>که از عشقش دل بیمار باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جگر باید سپر کردن به تیرش</p></div>
<div class="m2"><p>که عاشق باید و دلدار باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد گر شاهدی مرد از غم عشق</p></div>
<div class="m2"><p>همیشه عشق را این کار باشد</p></div></div>