---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>جان ز تن رفت و دل اندر عقد گیسویت بماند</p></div>
<div class="m2"><p>شد تنم هم خاک دروی تا ابد بویت بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک ره گشتم ولی شادم که بعد از سالها</p></div>
<div class="m2"><p>گردی از خاک وجودم بر سر کویت بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل پر درد خود دیدم که در هرگوشه ای</p></div>
<div class="m2"><p>بس نشان ها از خدنگ چشم جادویت بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالها شستم به خوناب جگر لیکن نرفت</p></div>
<div class="m2"><p>آن غباری را که بر روی دل از خویت بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش عالم را یکایک شستم از لوح خرد</p></div>
<div class="m2"><p>همچنان در وی خیال قد دلجویت بماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شدی دور از من اما شاهدی را در نظر</p></div>
<div class="m2"><p>سجده گاهی از خیال طاق ابرویت بماند</p></div></div>