---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>تا دل نظری به حال خود کرد</p></div>
<div class="m2"><p>جز درد و غم نگار خود کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خاک سری فدای تو شد</p></div>
<div class="m2"><p>بگذر ز گناهش ار چه بد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر لعل تو را گزید جانم</p></div>
<div class="m2"><p>بد کرد ولی به جان خود کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن هم ز جنون عاشقی بود</p></div>
<div class="m2"><p>کین دل شده دعوی خرد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسانه شیخ شاهدی را</p></div>
<div class="m2"><p>در عشق فزودنش مدد کرد</p></div></div>