---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>بنما طلعت موجه خویش</p></div>
<div class="m2"><p>تا بگیرند دلبران ره خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر تو غیر جان نثارم نیست</p></div>
<div class="m2"><p>چون کنم من زدست کوته خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب هجران من شود روشن</p></div>
<div class="m2"><p>گر نمایی تو رو چون مه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ما را ز خرمن حسنت</p></div>
<div class="m2"><p>حاصلی غیر روی چون که خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهدی را بناوک هجران (مژگان)</p></div>
<div class="m2"><p>بنوازش ز لطف گه گه خویش</p></div></div>