---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>دل و جگر به غم تو کباب شد هر دو</p></div>
<div class="m2"><p>سرای دیده و دل هم خراب شد هر دو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب فراق تو شد دیده‌ام چو دریایی</p></div>
<div class="m2"><p>به رود و مردم چشمم حباب شد هر دو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علی الصباح به رویت چو دیده کردم باز</p></div>
<div class="m2"><p>سواد دیده من آفتاب شد هر دو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستون خیمه تن عشق گشت گیسویت</p></div>
<div class="m2"><p>بگرد خیمه ز هر سو طناب شد هر دو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قتل شاهدی آن غمزه بس تعلل کرد</p></div>
<div class="m2"><p>مگر که نرگس مستت به خواب شد هر دو</p></div></div>