---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>دل ز کار و بار عالم سر به سر برکنده‌ام</p></div>
<div class="m2"><p>می‌کشم بار غمت از جان و دل تا زنده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه می گردد صراحی دم به دم بر جان من</p></div>
<div class="m2"><p>چون قدح خونم خورد آن لعل من در خنده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی شکر اشکسته شد آنگه ز لعلت کام یافت</p></div>
<div class="m2"><p>زین سبب اِشکَستِگان را از دل و جان بنده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو را نسبت به قدش کرده‌ام از راستی</p></div>
<div class="m2"><p>از قدش با همت کوتاه خود شرمنده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خواب‌آلود از سر گفته‌ام نرگس ولیک</p></div>
<div class="m2"><p>همچو او من هم ز خجلت سر به پیش افکنده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شعاع آفتاب طلعتش بر من بتافت</p></div>
<div class="m2"><p>در [ میان ] از تاب خورشید رخش تابنده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاهدی تا واصله وصل تو بر جان وصل کرد</p></div>
<div class="m2"><p>خلعت شاهان ندارد قدر پیش جنده‌ام</p></div></div>