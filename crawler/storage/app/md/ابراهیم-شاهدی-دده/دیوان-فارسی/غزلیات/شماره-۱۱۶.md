---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>اگر از دهان تو زد لاف پسته</p></div>
<div class="m2"><p>نرنجی که آید به خدمت شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و گر دم زد از جعد زلفت بنفشه</p></div>
<div class="m2"><p>بیارند پیش تواش دست بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رقص ایم از شادمانی در آن دم</p></div>
<div class="m2"><p>که بینم خدنگ تو در دل نشسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن تیر کآمد ز شست تو بر دل</p></div>
<div class="m2"><p>درون دلم سرو نازیست رسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زیر قد همچو نخل است بس دل</p></div>
<div class="m2"><p>ز شوق رطب زان لب تو نشسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زلف تو دارد سر قید دلها</p></div>
<div class="m2"><p>نیابی دلی را از آن قید رسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن شاهدی دعوی شعر دیگر</p></div>
<div class="m2"><p>که نظمت چو بیتیست اشکسته بسته</p></div></div>