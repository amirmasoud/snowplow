---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>با لعل جان فزای تو آب زلال چیست</p></div>
<div class="m2"><p>با آفتاب روی تو مه در کمال چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تنگ گشته‌ام ز دهانت که هست نیست</p></div>
<div class="m2"><p>ور نیست باز گو که خیال محال چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسن بی مثال تو حیران شدند خلق</p></div>
<div class="m2"><p>معلوم کس نشد که رخت را مثال چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم امید وصل ولیکن ز بخت خویش</p></div>
<div class="m2"><p>در حیرتم که عاقبت این مآل چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نوبهار عمر ندارد بقا بسی</p></div>
<div class="m2"><p>با گل بگو که این همه غنج و دلال چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد شاهدی چو مو بخیال میان تو</p></div>
<div class="m2"><p>معلوم هم نشد که خیال محال چیست</p></div></div>