---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دو زلف سرکش دلبند داری</p></div>
<div class="m2"><p>که در هر یک بسی دل بند داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم دیوانه در سودای زلفت</p></div>
<div class="m2"><p>در ین قیدم بگو تا چند داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد گر زان لب لعل شکر بار</p></div>
<div class="m2"><p>به دشنامی مرا خرسند داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر چشم بد بر آتش رخ</p></div>
<div class="m2"><p>ز انواع گهر هر چند داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نصح شاهدی بگذر تو ای شیخ</p></div>
<div class="m2"><p>به خود پندی بده گر پند داری</p></div></div>