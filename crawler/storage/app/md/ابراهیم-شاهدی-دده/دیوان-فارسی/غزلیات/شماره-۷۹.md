---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>جانا ز دل خدنگ جفا را تو باز دار</p></div>
<div class="m2"><p>بر روی عاشقان در الطاف باز دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب خیال آن شه خوبان ندیم ماست</p></div>
<div class="m2"><p>ای دل در سرور رقیبان فراز دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمع پیش روی تو کردم وجود خویش</p></div>
<div class="m2"><p>یک ره نظر به سوی دل جان گداز دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند صوفیا ز هیاهوی بی اصول</p></div>
<div class="m2"><p>یک چند نیز گوش به قانون ساز دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شاهدی ز نازش دلبر مشو ملول</p></div>
<div class="m2"><p>گر ناز می کند حبیب تو رو به نیاز دار</p></div></div>