---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>دلا گر عشق مهروئی نداری</p></div>
<div class="m2"><p>برو کز معرفت بوئی نداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا ای سرو و بر چشمم بکن جا</p></div>
<div class="m2"><p>کزین بهتر لب جویی نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برو ای ماه با رویش مزن لاف</p></div>
<div class="m2"><p>که خال و چشم و ابرویی نداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو ای شاهدی و گوشه ای گیر</p></div>
<div class="m2"><p>که چون او چشم جادویی نداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکش جانا به تندی شاهدی را</p></div>
<div class="m2"><p>که چون او یار خوش خویی نداری</p></div></div>