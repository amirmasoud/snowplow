---
title: >-
    غزل آغازین - شمارهٔ ۱
---
# غزل آغازین - شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>شد خاک راه این سر سودا نوشت ما</p></div>
<div class="m2"><p>این شد مگر ز روز ازل سرنوشت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی دوست ما سوی جنت چرا رویم</p></div>
<div class="m2"><p>رضوان حسد برد ز نعیم و بهشت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تخم  اَمَل که دل به هوای تو کشته بود</p></div>
<div class="m2"><p>جز دانه‌های اشک نیاید ز کشت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور  برده چون محرک جمع صور یکی است</p></div>
<div class="m2"><p>بنگر محرک و منگر خوب و زشت ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقصود شاهدی چو تویی هر کجا که هست</p></div>
<div class="m2"><p>دیگر نگفت مسجد ما یا کنشت ما</p></div></div>