---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>امشب دلم به فکر دهان تو تنگ بود</p></div>
<div class="m2"><p>وز صبح با خیال خودم تیر جنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناله‌ام نوا به دل عاشقان رسید</p></div>
<div class="m2"><p>قانون عشق چون دل ما را به چنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جان و دل چو لشکر حسن تو تاختند</p></div>
<div class="m2"><p>اول کسی که بر دل ما زد خدنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سوز نغمه رشتهٔ جان مرا بسوخت</p></div>
<div class="m2"><p>پیر سخن که ورا نام جنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که جام ز جاجی به جرعه ای</p></div>
<div class="m2"><p>در هم شکست توبه ما کو چو سنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد شاهدی ز خون جگر سرخ روئیی</p></div>
<div class="m2"><p>عاشق همیشه از جگر خود به رنگ بود</p></div></div>