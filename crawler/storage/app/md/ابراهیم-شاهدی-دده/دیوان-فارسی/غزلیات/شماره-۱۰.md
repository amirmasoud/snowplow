---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>هر لحظه سیل دیده به خون می‌کشد مرا</p></div>
<div class="m2"><p>سودای گیسویت به  جنون می‌کشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر  به وعده‌های دل خلافت کشد رواست</p></div>
<div class="m2"><p>سوی سراب سوز درون می‌کشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا عشق در درون دل من قرار یافت</p></div>
<div class="m2"><p>از کارگاه عقل برون می‌کشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از کمند زلف تو‌ام بر حذر روان</p></div>
<div class="m2"><p>چشمت به ساحری و فسون می‌کشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شاهدی گذر ز فسون خرد که یار</p></div>
<div class="m2"><p>در حلقه جنون به فنون می‌کشد مرا</p></div></div>