---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>باز نما به مطربان نغمۀ جان گداز را</p></div>
<div class="m2"><p>تا بدرند از طرب پردۀ اهل راز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بنشانیم شبی شمع صفت برابرت</p></div>
<div class="m2"><p>پیش رخیت بنگری سوز دل گداز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود ای سرور جان ناز تو و نیاز من</p></div>
<div class="m2"><p>ناز بکن تو هر زمان تا نگری نیاز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بنمایی ای صنم سجده گه دو ابرویت</p></div>
<div class="m2"><p>جانب کعبه کی برند اهل نظر نیاز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهدی از هوای جان خاک ره نگار شد</p></div>
<div class="m2"><p>تا که رسد بپای بوس آن شه سرفراز را</p></div></div>