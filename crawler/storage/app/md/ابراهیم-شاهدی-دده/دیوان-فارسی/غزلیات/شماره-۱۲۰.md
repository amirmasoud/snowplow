---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>بگو به یار که کاشانهٔ که می‌پرسی</p></div>
<div class="m2"><p>منم خراب تو ویرانهٔ که می‌پرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث زلف تو امشب حکایتی است دراز</p></div>
<div class="m2"><p>تو خوابناک ز افسانهٔ که می‌پرسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم که خلوت دل کردم از غم‌آبادت</p></div>
<div class="m2"><p>تو از غم که و غمخانهٔ که می‌پرسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جام عشق تو خلق جهان همه مستند</p></div>
<div class="m2"><p>تو از تواضع مستانهٔ که می‌پرسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گدای کوی تو شد شاهدی و شاهی یافت</p></div>
<div class="m2"><p>بگو دگر در شاهانهٔ که می‌پرسی</p></div></div>