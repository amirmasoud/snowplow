---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>یار بر من دود دل‌ها می‌کشد</p></div>
<div class="m2"><p>لشکری از فتنه بر ما می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده است از زلف بس قلاب‌ها</p></div>
<div class="m2"><p>تا دل خلقی به هرجا می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوش دارویی ز لعلش خواست دل</p></div>
<div class="m2"><p>زانکه زلف او به سودا می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا سرها که سازد پایمال</p></div>
<div class="m2"><p>طرهٔ مشکین که در پا می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست قدرت کرد ماه عارضش</p></div>
<div class="m2"><p>خط مشکین را چه زیبا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهدی چون یاد می‌آرد لبش</p></div>
<div class="m2"><p>میل جان او به صهبا می‌کشد</p></div></div>