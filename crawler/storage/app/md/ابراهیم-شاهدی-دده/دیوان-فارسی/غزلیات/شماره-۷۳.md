---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>دل دید قدت ز قامت افتاد</p></div>
<div class="m2"><p>در واقعۀ قیامت افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان رفت که دل رهاند از عشق</p></div>
<div class="m2"><p>خود نیز در این ملامت افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کو به سلام عشق آمد</p></div>
<div class="m2"><p>آنجا ز رهی سلامت افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وان کو نکشید جام عشقت</p></div>
<div class="m2"><p>آخر به رهی ندامت افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس که بداد دین به دنیا</p></div>
<div class="m2"><p>سودش همگی غرامت افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق تو شاهدی در افتاد</p></div>
<div class="m2"><p>یارب که بدین ملامت افتاد</p></div></div>