---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>چو عقد سنبل تو عقده بر جبین انداخت</p></div>
<div class="m2"><p>چه عقده ها که از او در دل حزین انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد از محبت تو خاک سجده گاه ملک</p></div>
<div class="m2"><p>چو سرو قامت تو سایه بر زمین انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ تو گاه بگویند ماه و گه خورشید</p></div>
<div class="m2"><p>مرا به مهر رخت شوق آن و این انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببرد دانش و هوش و خرد ز من زلفت</p></div>
<div class="m2"><p>کنون کرشمه چشمت نظر برین انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفت دل ز هوا و نشاند در جگرم</p></div>
<div class="m2"><p>کمان ابروی تو هر چه از کمین انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن ملامت احوال شاهدی ای شیخ</p></div>
<div class="m2"><p>فراق یار و غم رویش اندرین انداخت</p></div></div>