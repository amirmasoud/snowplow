---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>دلبرا در کشتن ما خود بگویی سود چیست</p></div>
<div class="m2"><p>ور نخواهی کشتن از جور و ستم مقصود چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس نمیپرسد ز احوال درون درد من</p></div>
<div class="m2"><p>هم نمیپرسد یکی کین آه درد آلود چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهدا تا چند بر افعال ما منکر شوی</p></div>
<div class="m2"><p>چون نمی دانی که اندر کار ما بهبود چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه من میبینی و از سوز دل واقف نه ای</p></div>
<div class="m2"><p>آتشی گر نیست پنهان خود بگو این دود چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان همی کردم نثار اما همی ترسم که یار</p></div>
<div class="m2"><p>گوید ای بیمایه رو این جان غم فرسود چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی شد تا ز مژگان خون فشانم دمبدم</p></div>
<div class="m2"><p>او نگفت ای شاهدی زین گریه‌ات مقصود چیست</p></div></div>