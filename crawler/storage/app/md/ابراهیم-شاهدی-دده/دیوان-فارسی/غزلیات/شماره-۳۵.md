---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>هر سر که بوی سنبل تو در دماغ داشت</p></div>
<div class="m2"><p>از مشک و از شمامه عنبر فراغ داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تن نماند یک سر مو بی نشان تو</p></div>
<div class="m2"><p>هر جا که دیدمش ز خدنگ تو داغ داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند بود لاله جگر خون ز درد عشق</p></div>
<div class="m2"><p>لیکن به یاد لعل تو بر کف نفاغ داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوشینه شمع روشنی خود به باد داد</p></div>
<div class="m2"><p>کز روی یار مجلس رندان چراغ داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شاهدی به باغ نشد زین عجب مدار</p></div>
<div class="m2"><p>کو با خیال حسن تو بس باغ و راغ داشت</p></div></div>