---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>گویا بگذشتی ز چمن با رخ گلگون</p></div>
<div class="m2"><p>کز گونه تو لاله خجل گشت و دگرگون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنجیر بود چاره دیوانه ولیکن</p></div>
<div class="m2"><p>ماییم که گشتیم به زنجیر تو مجنون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطت سپه زنگ چو می برد سوی روم</p></div>
<div class="m2"><p>بر جان من سوخته آورد شبیخون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که بپوشم مگر این سوز درون را</p></div>
<div class="m2"><p>با آه چه گویم که زند شعله به بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این زرد رخ شاهدی داغ دل ما بود</p></div>
<div class="m2"><p>هم سرخ شد ای شاهدی از دیده پرخون</p></div></div>