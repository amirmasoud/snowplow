---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>کسی که عشق تو ورزید با فراغ نرفت</p></div>
<div class="m2"><p>دلش چو لاله پر از خون و جز بداغ نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نافه ها که در آن زلف عنبر افشان نیست</p></div>
<div class="m2"><p>که خاک شد تن و بوی تو از دماغ نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نور روی تو بگذشت دل از آن خم زلف</p></div>
<div class="m2"><p>چرا که کس به شب تار بی چراغ نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن دمی که لبت بوسه داد بر لب جام</p></div>
<div class="m2"><p>صفای لذت آن از دل نفاغ نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شاهدی گل رخسار و خط قد تو دید</p></div>
<div class="m2"><p>ز خاک کوی تو دیگر بسوی باغ نرفت</p></div></div>