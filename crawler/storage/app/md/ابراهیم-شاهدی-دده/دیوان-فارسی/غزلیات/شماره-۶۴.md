---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>ز بهر تیر تو میلِ (میلم) بهر مغاک برد</p></div>
<div class="m2"><p>شدیم خاک که میلش مگر به خاک برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بهر دوختن چاک سینه مژگانت</p></div>
<div class="m2"><p>گذر ز سینۀ مجروح چاک چاک برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لعل نوش تو دل خواست شربت عنّاب</p></div>
<div class="m2"><p>گر  ردِ می بر این جان دردناک برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای شستن دل از غم جهان یک سر</p></div>
<div class="m2"><p>کجاست می که به یک جرعه ایش پاک برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشست شاهدی از دل غم خمار به آب</p></div>
<div class="m2"><p>نبرد گر ببرد نیز آب ناک برد</p></div></div>