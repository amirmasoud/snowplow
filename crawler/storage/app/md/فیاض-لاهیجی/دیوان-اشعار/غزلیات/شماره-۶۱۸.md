---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>از فکر خال و طرّة جانانه بگذریم</p></div>
<div class="m2"><p>دامست به که از سر این دانه بگذریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان پیش ترکه عمر به افسانه بگذرد</p></div>
<div class="m2"><p>یک دم بیا به جانب میخانه بگذریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنّار عشق بر کمر ما زند چو تیغ</p></div>
<div class="m2"><p>چون رشته گر به سبحة صد دانه بگذریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جا در دل زمانه نکردیم تا به کی</p></div>
<div class="m2"><p>از گوش روزگار چو افسانه بگذریم!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خون کنیم تا دگری تر کند دماغ</p></div>
<div class="m2"><p>زین بزم همچو ساغر و پیمانه بگذریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شمع یاد سوختگان حجّ اکبرست</p></div>
<div class="m2"><p>یک شب بیا به تربت پروانه بگذریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خانه بگذریم که آید چو سیلِ مرگ</p></div>
<div class="m2"><p>چندان امان کجاست که از خانه بگذریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز عشق ره به کوی حقیقت نمی‌برد</p></div>
<div class="m2"><p>از قیل و قال بحثِ ‌حکیمانه بگذریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رندانه نیست صحبت فیّاض عیب جو</p></div>
<div class="m2"><p>زین آرزو بهست که رندانه بگذاریم</p></div></div>