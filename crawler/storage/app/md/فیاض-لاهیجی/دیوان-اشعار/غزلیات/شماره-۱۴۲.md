---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>عشق را پیغمبرم داغ جنون تاج منست</p></div>
<div class="m2"><p>این غزل‌های بلند تازه معراج منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده تسخیر دو عالم آهم از اقبال عشق</p></div>
<div class="m2"><p>گردن گردون به زیر منّت تاج منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلسم را منّت شمع مه و خورشید نیست</p></div>
<div class="m2"><p>طلعت من روشنی‌بخش شب داج منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل معنی خوشه‌چین خرمن فکر منند</p></div>
<div class="m2"><p>زلف خوبان معانی لفظ کج واج منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برده بودم هر دو عالم را ولی در باختم</p></div>
<div class="m2"><p>چون کنم در نرد طاقت! عشق لیلاج منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پناه عشق استغنا به گردون می‌زنم</p></div>
<div class="m2"><p>بر سر تیر تغافل، چرخ آماج منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت فیّاض آنکه دشمن می‌زد استغنا به من</p></div>
<div class="m2"><p>این زمان چرخش به یمن عشق محتاج منست</p></div></div>