---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>گر نسیم صبحگاهی گلستان می‌پرورد</p></div>
<div class="m2"><p>بوی زلف یار را نازم که جان می‌پرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبی آن گل خدادادست نه کار بهار</p></div>
<div class="m2"><p>این چمن را آبِ دست باغبان می‌پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله‌ام را گر کند شاخ گل حسرت رواست</p></div>
<div class="m2"><p>آنکه آب جلو‌ه‌اش سرو روان می‌پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم رحمت دارم از ابری که کمتر قطره‌اش</p></div>
<div class="m2"><p>تا قیامت سبزه‌زار آسمان می‌پرورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلستانی را که آبش اشک خون‌‌آلوده است</p></div>
<div class="m2"><p>باغبان از هر نهالش ارغوان می‌پرورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مهر مادرزاد دارد طفل روح ما به جسم</p></div>
<div class="m2"><p>این هما در بیضة ما آشیان می‌پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان گل عیشی نمی‌جستم که دایم آسمان</p></div>
<div class="m2"><p>نوبهارم را در آغوش خزان می‌پرورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هوای جلوه‌اش چون بیستون نالیده‌ام</p></div>
<div class="m2"><p>گرچه ما را حسرت موی میان می‌پرورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست عاشق را بهار آفتی هر نوع هست</p></div>
<div class="m2"><p>خضر را آسیب عمر جاودان می‌پرورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هیچ عضو از فیض بیداد توام بی‌بهره نیست</p></div>
<div class="m2"><p>حسرت تیغ تو مغز استخوان می‌پرورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ریشه‌ها در خاک قم کردیم فیّاض ار چه لیک</p></div>
<div class="m2"><p>شوق ما را در هوای اصفهان می‌پرورد</p></div></div>