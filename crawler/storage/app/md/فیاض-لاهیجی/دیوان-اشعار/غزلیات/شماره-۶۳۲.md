---
title: >-
    شمارهٔ ۶۳۲
---
# شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>لبی تر یک دم از جام طرب، کم می‌توان کردن</p></div>
<div class="m2"><p>ولی چندان که خواهی مستی از غم می‌توان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوا نامحرم دردست و مرهم خصم ناسورست</p></div>
<div class="m2"><p>بلی الماس را با داغ محرم می‌توان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لطف ظاهری خشم نهانی کم نمی‌گردد</p></div>
<div class="m2"><p>هلاهل داخل اجزای مرهم می‌توان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دیدارت سجل کردیم خون دین و دنیا را</p></div>
<div class="m2"><p>به نازی قتل عام هر دو عالم می‌توان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همین بس تا قیامت سرخ رویی‌های امیدم</p></div>
<div class="m2"><p>که از خونم حنای عشرتی نم می‌توان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان گرد کدورت شست از دل آتش عشقم</p></div>
<div class="m2"><p>که از خاکسترم آیینة جم می‌توان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مدد بخشد اگر عشق آفتابِ‌ قطره دشمن را</p></div>
<div class="m2"><p>چو مادر، مهربانی طفل شبنم می‌توان کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک گر لخت دل را هم به ما بسیار می‌داند</p></div>
<div class="m2"><p>پشیمانی ندارد، پاره‌ای کم می‌توان کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کسی تا چند رام این هوس پروردگان باشد</p></div>
<div class="m2"><p>گهی از چشم مردم چون حیا رم می‌توان کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدل برداشت باری را که گردون برنمی‌تابد</p></div>
<div class="m2"><p>طواف جرئت فرزند آدم می‌توان کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دست افتد دمی گر یار، زنگ از دل بری فیّاض</p></div>
<div class="m2"><p>تلافی‌های غم از صحبت هم می‌توان کردن</p></div></div>