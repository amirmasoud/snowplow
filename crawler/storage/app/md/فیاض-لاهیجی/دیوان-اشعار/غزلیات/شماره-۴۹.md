---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>گر کشم بر رخ دریا مژة پرخون را</p></div>
<div class="m2"><p>غوطه‌ها در عرق شرم دهم جیحون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب از جاذبة عشق که از یکرنگی</p></div>
<div class="m2"><p>طوق لیلی نکند سلسلة مجنون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رتبة کوهکن این بس که کشیدست بدوش</p></div>
<div class="m2"><p>در وفا یک دو قدم غاشیة گلگون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلد راه جنون نقش پی مجنون است</p></div>
<div class="m2"><p>خضر از ره نبرد گمشدة هامون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خونبها یافت کسی کاب دم تیغ تو خورد</p></div>
<div class="m2"><p>این هوس تشنه به خون کرده دل پر خون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو پروانه به گرد سر خود گرداند</p></div>
<div class="m2"><p>شعلة آه دل سوخته‌ام گردون را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظاهرم آینه صورت باطن شده است</p></div>
<div class="m2"><p>شاهد حال درون ساخته‌ام بیرون را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طرفه نعم‌البدلی یافته خم جا دارد</p></div>
<div class="m2"><p>که فراموش کند صحبت افلاطون را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این همه نکتة سر بسته که در نامة تست</p></div>
<div class="m2"><p>هم تو فیّاض مگر فهم کنی مضمون را</p></div></div>