---
title: >-
    شمارهٔ ۶۷۸
---
# شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>آنکه من دارم ندارد همچو او دلبر کسی</p></div>
<div class="m2"><p>بیوفا پرور کسی، ظالم کسی، کافر کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تواند سوختن داغ جنون بر سر کسی</p></div>
<div class="m2"><p>نیست عاقل گر کشد درد سر افسر کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساده لوحم هر که آید در دلم جا می‌کند</p></div>
<div class="m2"><p>خانة آیینه را هرگز نبندد در کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد و کبر و نماز و عاشق و عجز و نیاز</p></div>
<div class="m2"><p>دل به درگاهش به چیزی می‌کند خوش هر کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فلک دل خوش به چندین ناخوشی‌ها کرده‌ام</p></div>
<div class="m2"><p>دلخوشی جز من کجا دیدست از چنبر کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامرادم کرد و آخر بر مراد خود نشاند</p></div>
<div class="m2"><p>از فلک این مردمی‌ها کی کند باور کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان دهانم بوسه نه، پیغام نه، دشنام نه</p></div>
<div class="m2"><p>تنگ هم نتوان گرفتن اینقدرها بر کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منع زاهد کردمت فیّاض صد بار و نشد</p></div>
<div class="m2"><p>خود تأمل کن چه گوید با تو خود دیگر کسی</p></div></div>