---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>هر خس به باغ ما گل و هر زاغ بلبل است</p></div>
<div class="m2"><p>آشفتگی گلی است که مخصوص سنبل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناز بهار چند کشم از برای گل</p></div>
<div class="m2"><p>فصل خزان خوشست که هر برگ او گل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخصوص ماست از نگه کنج چشم یار</p></div>
<div class="m2"><p>نازی که دست پرور چندین تغافل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرهم‌پذیر کی شود از ترّهات زاغ</p></div>
<div class="m2"><p>داغ دلم که تازه ز آواز بلبل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشتیم بر حواشی خط دقتی نداشت</p></div>
<div class="m2"><p>پیچیدگی نتیجة زلفست و کاکل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسیِّ ما تمنّی دیدار می‌کند</p></div>
<div class="m2"><p>آماده باش طور که وقت تزلزل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشبیه دل به مشت گل کعبه کی رواست</p></div>
<div class="m2"><p>فیّاض هان خموش که جای تأمّل است</p></div></div>