---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>غیر زلف او که دوشادوش آن رو می‌رود</p></div>
<div class="m2"><p>سایه با خورشید کی پهلو به پهلو می‌رود!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون توانم رفت در کویی که هر روز آفتاب!</p></div>
<div class="m2"><p>جچون رود رو در قفا در کوچة او می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه عشق است این که در وی پای کس گستاخ نیست</p></div>
<div class="m2"><p>آفتاب اینجا به سر، گردون به پهلو می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود سرسبز گلزار محبّت دور نیست</p></div>
<div class="m2"><p>زخم را از تیغش امروز آب در جو می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای رحم است ای وفاداران که با چندین امید</p></div>
<div class="m2"><p>می‌رود فیّاض و خوش نومید از آن کو می‌رود</p></div></div>