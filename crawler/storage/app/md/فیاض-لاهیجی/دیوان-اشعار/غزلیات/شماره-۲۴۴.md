---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>هر آه که درد از دل ناشاد برآرد</p></div>
<div class="m2"><p>نورسته نهالی است که فریاد برآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناید به کمند کسی آن آهوی وحشی</p></div>
<div class="m2"><p>این صید دمار از دل صیّاد برآرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم سیهی دیده‌ام امروز که نازش</p></div>
<div class="m2"><p>صد فتنه ز شاگردی استاد برآرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قید تعلق نتوان عشق هوس کرد</p></div>
<div class="m2"><p>آزاده سر از قید غم آزاد برآرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل به چمن گوش بر آواز نشسته است</p></div>
<div class="m2"><p>تا نالة زارم شنود داد برآرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این با که توان گفت که در خلوت خسرو</p></div>
<div class="m2"><p>شمعی است که دود از دل فرهاد برآرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض به ناکامی جاوید بنه دل</p></div>
<div class="m2"><p>کس نیست که کام دل ناشاد برآرد</p></div></div>