---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>مشّاطه چو آرایش آن زلف علم کرد</p></div>
<div class="m2"><p>آن خم که در آن بود دلم باز به خم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تار سر زلف تو ماوای دلی بود</p></div>
<div class="m2"><p>مشّاطه برین سلسله بسیار ستم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قربانی مژگان تو گردم که به یک تاز</p></div>
<div class="m2"><p>تسخیر جهان بی‌مدد تیغ و علم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا لذّت تیغ تو چشیدست، دلم را</p></div>
<div class="m2"><p>رحمست بر آن صید که از دام تو رم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازی که نگاه تو به فیّاض حزین داشت</p></div>
<div class="m2"><p>یارب چه گنه دید که بی‌واسطه کم کرد!</p></div></div>