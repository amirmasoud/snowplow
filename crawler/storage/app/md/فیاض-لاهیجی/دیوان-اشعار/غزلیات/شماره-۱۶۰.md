---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>گر ز زلف آزاد گشتم دام کاکل در پی است</p></div>
<div class="m2"><p>گر نگه رد شد ز من تیر تغافل در پی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهره‌ای گلچین ازین گل‌ها که چیدی کی بری</p></div>
<div class="m2"><p>هر یکی را صد هزاران چشم بلبل در پی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا سر زلف تو، یا بیداد گردون، یا رقیب</p></div>
<div class="m2"><p>هر کجا رفتم مرا دست تطاول در پی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار می‌آید خرامان و رقیبش پیش پیش</p></div>
<div class="m2"><p>جای رنجش نیست یاران خار را گل در پی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم مخور فیّاض اگر از بزم او گشتی جدا</p></div>
<div class="m2"><p>سهل باشد هر ترقّی را تنزّل در پی است</p></div></div>