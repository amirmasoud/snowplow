---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>عمریست که در کوی بلا خانه نداریم</p></div>
<div class="m2"><p>گنجیم ولیکن دل ویرانه نداریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننگست دلا سوختن از آتش دیگر</p></div>
<div class="m2"><p>در عشق سر منصب پروانه نداریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر دم واعظ بکشد آتش ما را</p></div>
<div class="m2"><p>خوبست دگر گوش به افسانه نداریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما بی‌کس و کویان خرابات الستیم</p></div>
<div class="m2"><p>جایی به جز از گوشة میخانه نداریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس به جهان راهبری داشته از عقل</p></div>
<div class="m2"><p>ماییم که غیر از دل دیوانه نداریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دنیاطلبان در گرو خانه و مالند</p></div>
<div class="m2"><p>ما مال نیندوخته و خانه نداریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از لعل بتان کام دل ما نشِکیبد</p></div>
<div class="m2"><p>جز حسرت لعل لب پیمانه نداریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر جا که بود دانه بود دام به راهش</p></div>
<div class="m2"><p>ما دام نهادیم ولی دانه نداریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این نیست که در ما نبود مایة نازش</p></div>
<div class="m2"><p>شمعیم ولیکن سر پروانه نداریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فریادرسان گوش ندزدید که امشب</p></div>
<div class="m2"><p>در ترکش دل نالة مستانه نداریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شب نیست که در خلوت این سینة تاریک</p></div>
<div class="m2"><p>با یاد رخ دوست پری‌‌خانه نداریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما با نفس سوخته در ذکر حبیبیم</p></div>
<div class="m2"><p>این هست که ما سبحة صد دانه نداریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در قفل فرو بستة غم‌های دل خویش</p></div>
<div class="m2"><p>آن کهنه کلیدیم که دندانه نداریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فیّاض متاع سفر آخرت خویش</p></div>
<div class="m2"><p>چیزی به جز از مشرب رندانه نداریم</p></div></div>