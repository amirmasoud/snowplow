---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>چه سازم دست دردی دامن جانم نمی‌گیرد</p></div>
<div class="m2"><p>که امید دوا در یاد درمانم نمی‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به راه کوی او یک دم ز ضعف پا نمی‌افتم</p></div>
<div class="m2"><p>که بوی گل در آغوش گلستانم نمی‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه لازم دل رهین منّت باد صبا کردن</p></div>
<div class="m2"><p>چرا گل نسخة چاک از گریبانم نمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پر عنقا به سر از صیدگاه وصل می‌آیم</p></div>
<div class="m2"><p>دگر گرد شکاری طرف دامانم نمی‌گیرد</p></div></div>