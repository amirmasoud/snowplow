---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>هرزه کمر نبسته‌ام کینة روزگار را</p></div>
<div class="m2"><p>یاور خویش کرده‌ام صاحب ذوالفقار را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیم خزان چه می‌کند در چمن امید من</p></div>
<div class="m2"><p>من که به اشک آتشین آب دهم بهار را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و امید سجدة خاک دری که تا ابد</p></div>
<div class="m2"><p>سرمة وعده می‌دهد دیدة انتظار را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سینه به داغ دل دهم، در غم عشق تا به کی</p></div>
<div class="m2"><p>در شکنم به کام دل حسرت لاله‌زار را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر نکشد ز خاک غم دانة آرزوی من</p></div>
<div class="m2"><p>چند شفیع آورم دیدة اشکبار را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جیبِ نفس چه می‌درد نالة دلکشی که من</p></div>
<div class="m2"><p>در کف عشق داده‌ام دامن اختیار را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند چو فیّاض کند دیدة من قطار اشک</p></div>
<div class="m2"><p>گریه برون نمی‌برد از دل من غبار را</p></div></div>