---
title: >-
    شمارهٔ ۶۲۰
---
# شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>آهی از دل از پیِ دفعِ‌گزندی می‌کشیم</p></div>
<div class="m2"><p>چشم بد را سرمه از دود سپندی می‌کشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شبی سر و قد او سایه بر ما افکند</p></div>
<div class="m2"><p>ما و دل هر صبح یاهوی بلندی می‌کشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناتوانی بین که در عشق تو بار عالمی</p></div>
<div class="m2"><p>با تن زاری و جان دردمندی می‌کشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آستانت از وجود ما گرانی می‌کند</p></div>
<div class="m2"><p>پای رغبت از سر کوی تو چندی می‌کشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نفس فیّاض با این تلخکامی‌های خویش</p></div>
<div class="m2"><p>کاسة زهری ز لعل نوشخندی می‌کشیم</p></div></div>