---
title: >-
    شمارهٔ ۴۶۱
---
# شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>عاشق یاریم اما نام یار ما مپرس</p></div>
<div class="m2"><p>بی‌نصیبان دیاریم از دیار ما مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر مالامال دردیم و ز ساحل بی‌نصیب</p></div>
<div class="m2"><p>ما که پا تا سر میانیم از کنار ما مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق ما را خاک کرد و خاک ما بر باد داد</p></div>
<div class="m2"><p>بر مزار ما بیا اما مزار ما مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز تاب افتاده‌ایم از آب و تاب ما مگو</p></div>
<div class="m2"><p>ما ز کار افتاده‌ایم از کار و بار ما مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تنزّل زیر بار سایة خود مانده‌ایم</p></div>
<div class="m2"><p>اعتبار ما ببین و ز اعتبار ما مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایه پروردان زلف شاهد بخت خودیم</p></div>
<div class="m2"><p>روز ما را دیده‌ای از روزگار ما مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرها فیّاض گرد کوی جانان بوده‌ایم</p></div>
<div class="m2"><p>خاک ما را سرمه ساز و از غبار ما مپرس</p></div></div>