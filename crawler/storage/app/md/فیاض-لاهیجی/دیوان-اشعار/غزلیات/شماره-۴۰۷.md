---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>کم کنم شیون نمی‌خواهم که نقص غم شود</p></div>
<div class="m2"><p>پر نگریم خون دل ترسم که دردی کم شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سال‌ها در چشم ما جا داشتی سودی نداشت</p></div>
<div class="m2"><p>گر پری با مردم این الفت کند آدم شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شور بختی بین که بر داغ دل بی‌طاقتم</p></div>
<div class="m2"><p>سودة الماس ریزد حسرت و مرهم شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر از برگ گل نازک‌تری داری مباد</p></div>
<div class="m2"><p>گر وزد بر وی نسیم شکوه‌ای در هم شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که را درد تو دامن‌گیر شد فیّاض‌وار</p></div>
<div class="m2"><p>دامن بیگانه گیرد دشمن و محرم شود</p></div></div>