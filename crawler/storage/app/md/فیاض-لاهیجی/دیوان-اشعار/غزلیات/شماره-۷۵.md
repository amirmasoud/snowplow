---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>باز دارد عشق در آغوش بیهوشی مرا</p></div>
<div class="m2"><p>غم مهیّا می‌کند اسباب مدهوشی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زبان بی‌زبانی می‌کنم تقریر شوق</p></div>
<div class="m2"><p>کرده ذوق گفتگو سرگرم خاموشی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدّتی شد تا ز معراج قبول افکنده است</p></div>
<div class="m2"><p>طرّة او در سیه چاه فراموشی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلعت سر تا به پایی دوختم از داغ عشق</p></div>
<div class="m2"><p>چشم مستش کرد تکلیف سیه‌پوشی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده‌ام تا حلقه‌های زلف چین بر چین او</p></div>
<div class="m2"><p>می‌شود فیّاض ذوق حلقه در گوشی مرا</p></div></div>