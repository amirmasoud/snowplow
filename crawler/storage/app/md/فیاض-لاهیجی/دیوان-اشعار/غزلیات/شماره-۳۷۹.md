---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>دل نظر چون بر رخ آن بی‌ترحّم می‌کند</p></div>
<div class="m2"><p>همچو زلف او ز حیرت دست و پا گم می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرزه چشمی‌های چشمم دایم از دخل دلست</p></div>
<div class="m2"><p>ساغر این دریا دلی از پهلوی خم می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق درد از ساغر می یاد می‌باید گرفت</p></div>
<div class="m2"><p>دل پر از خونست و در ظاهر تبسّم می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افعی زلف تو از جادووشی چون روزگار</p></div>
<div class="m2"><p>آنکه جو را در کف اغیار گندم می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیابان محبت رهنمایی مشکل است</p></div>
<div class="m2"><p>خضر اینجا گام اول خویش را گم می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون روم فیّاض در راهی که رهرو از خطر</p></div>
<div class="m2"><p>هر دم از آواز پای خود توهم می‌کند!</p></div></div>