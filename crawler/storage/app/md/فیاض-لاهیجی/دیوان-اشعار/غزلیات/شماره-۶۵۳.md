---
title: >-
    شمارهٔ ۶۵۳
---
# شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>در خواب خمار آن چشم دایم ز شراب او</p></div>
<div class="m2"><p>چشم همه شب تا روز بیدار ز خواب او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ تو و ما هر دو از تشنه لبی مردیم</p></div>
<div class="m2"><p>او تشنه به خون ما، ما تشنه به آب او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ساغر وصل او لب تر نتوان کردن</p></div>
<div class="m2"><p>اندیشه به چرخ افتد از بوی شراب او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بزم جگرخواری جرأت نتوان کردن</p></div>
<div class="m2"><p>خمیازه نفس دزدد از بوی کباب او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندیشه نرنجانی از تربیتم فیّاض</p></div>
<div class="m2"><p>ممکن نبود هرگز تعمیر خراب او</p></div></div>