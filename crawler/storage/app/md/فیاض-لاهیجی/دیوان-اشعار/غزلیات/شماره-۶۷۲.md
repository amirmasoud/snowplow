---
title: >-
    شمارهٔ ۶۷۲
---
# شمارهٔ ۶۷۲

<div class="b" id="bn1"><div class="m1"><p>دوش کردی پرسش گرمی که جانم سوختی</p></div>
<div class="m2"><p>آشکارا لطف کردی و نهانم سوختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج تبخال از دلم تا ساحل لب می‌رسد</p></div>
<div class="m2"><p>بس که مغز آرزو در استخوانم سوختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش با سبّابة مژگان گرفتی نبض دل</p></div>
<div class="m2"><p>خون طاقت در رگ تاب و توانم سوختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌زدی آبی بر آتش از برون پرده لیک</p></div>
<div class="m2"><p>آتشی افروختی در دل که جانم سوختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش افکندی که پرسی حال و از شرم سخن</p></div>
<div class="m2"><p>حسرت صد شکوه در کام زبانم سوختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ غم دیدی که از خاکسترم بیرون نرفت</p></div>
<div class="m2"><p>ای که صد بار از برای امتحانم سوختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعلة برق نگاهی سر به جان دادی کز آن</p></div>
<div class="m2"><p>در درون سینه صد راز نهانم سوختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتشی افروختی ای ناله در جان حزین</p></div>
<div class="m2"><p>خود برون جستی و غافل در میانم سوختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز دل فیّاض در آتش گرو داری که دوش</p></div>
<div class="m2"><p>ناله‌ای کردی که جان ناتوانم سوختی</p></div></div>