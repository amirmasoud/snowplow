---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>درنخواهد داد تن بیماری ما در علاج</p></div>
<div class="m2"><p>گو دماغ خود مسوز اینجا مسیحا در علاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فراق خویش ما را اندک اندک خوی ده</p></div>
<div class="m2"><p>درد عشق است این و می‌باید مدارا در علاج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مداوای طبیبان جانم آسایش نیافت</p></div>
<div class="m2"><p>زانکه پنهان جمله در زخمند و پیدا در علاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق چون در تب نشاند مغز را در استخوان</p></div>
<div class="m2"><p>عقل را حاصل نگردد غیر سودا در علاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنگان جرعة وصلیم و عاجز مانده‌ایم</p></div>
<div class="m2"><p>پیش این لب‌تشنگی‌ها هفت دریا در علاج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیره‌بختی‌های ما درمان نگیرد چون کلیم</p></div>
<div class="m2"><p>می‌نماید گر ید بیضا مسیحا در علاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش درد ما که عالم در علاجش مانده‌اند</p></div>
<div class="m2"><p>گر تو ایمایی کنی کافیست تنها در علاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمرها شد کز تردّد نگسلد از رغم هم</p></div>
<div class="m2"><p>شوق در افزونی درد و تمنّا در علاج</p></div></div>