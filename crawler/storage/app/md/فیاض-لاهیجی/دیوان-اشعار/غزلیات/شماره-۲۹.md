---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>یک جهان بر هم زدم کز جمله بگزیدم ترا</p></div>
<div class="m2"><p>من چه می‌کردم به عالم گر نمی‌دیدم ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه مشکل‌پسندی‌های طبع نازکم</p></div>
<div class="m2"><p>حیرتی دارم که چون آسان پسندیدم ترا!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک بساط دهر شد زیر و زبر در انتخاب</p></div>
<div class="m2"><p>زین جواهر تا به طبع خویش برچیدم ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز خود گم می‌شدم چون می‌شنیدم نام تو</p></div>
<div class="m2"><p>خویش را گم کرده‌تر می‌خواستم دیدم ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی قبول من شدی فیّاض در ردّ و قبول</p></div>
<div class="m2"><p>تا به میزان «رهی» صد ره نسنجیدم ترا</p></div></div>