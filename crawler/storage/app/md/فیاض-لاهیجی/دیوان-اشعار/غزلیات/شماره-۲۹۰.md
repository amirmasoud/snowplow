---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>عشق ظاهر نمی‌توانم کرد</p></div>
<div class="m2"><p>کشف این سرّ نمی‌توانم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه دهی توبه‌ام دگر زاهد</p></div>
<div class="m2"><p>من که آخر نمی‌توانم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم از حیرت و ترا در عشق</p></div>
<div class="m2"><p>متحیّر نمی‌توانم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کنم تا تو فهم عشق کنی</p></div>
<div class="m2"><p>سحر ساحر نمی‌توانم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم عاشقی اگر نکنم</p></div>
<div class="m2"><p>چون تو کافر نمی‌توانم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساده دل‌تر از آب و آینه‌ام</p></div>
<div class="m2"><p>حفظ ظاهر نمی‌توانم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه فیّاض دانشم هِر را</p></div>
<div class="m2"><p>فرق از بِر نمی‌توانم کرد</p></div></div>