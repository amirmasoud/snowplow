---
title: >-
    شمارهٔ ۶۹۲
---
# شمارهٔ ۶۹۲

<div class="b" id="bn1"><div class="m1"><p>برین مباش که قانون تازه ساز کنی</p></div>
<div class="m2"><p>به قول بلهوس از عاشق احتراز کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمیز عاشق و اهل هوس نمی‌داند</p></div>
<div class="m2"><p>به جان خویش که خاطر نشان ناز کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان سوسن، بی‌گفتگو نمی‌ماند</p></div>
<div class="m2"><p>اگر تو گوش به گفتار اهل راز کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی به روی تو درهای بسته بگشایند</p></div>
<div class="m2"><p>که گوشه‌ای بنشینی و در فراز کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نیم ناز که بر آرزو کنی ای دل</p></div>
<div class="m2"><p>توانی آنکه مرا نیز بی‌نیاز کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دمی به دو عالم نظر فروبندی</p></div>
<div class="m2"><p>دگر دلت نگذارد که دیده باز کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان بلبل و فیّاض فرق بسیارست</p></div>
<div class="m2"><p>اگر دمی بنشینی و امتیاز کنی</p></div></div>