---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>امشب دگر نگاه کجت جاودانه است</p></div>
<div class="m2"><p>کج مج زبانی سر زلفت بهانه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخشت که زیر پا فلکش برقرار نیست</p></div>
<div class="m2"><p>سرگرم کرده نگهت، تازیانه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرسبز باد قامت نخل بلند تو</p></div>
<div class="m2"><p>کاندر میان سبزقبایان یگانه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ نماز مژدة سیماب می‌دهد</p></div>
<div class="m2"><p>در گوش من که حلقه به گوش ترانه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض جان فدایش اگر می‌کنی سزاست</p></div>
<div class="m2"><p>معشوق من که هر سر مو عاشقانه است</p></div></div>