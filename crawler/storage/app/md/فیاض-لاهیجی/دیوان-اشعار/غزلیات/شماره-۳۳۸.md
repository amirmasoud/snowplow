---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>بیوفا و بد و بیدادگرت ساخته‌اند</p></div>
<div class="m2"><p>خوب بودی و دگر خوبترت ساخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحم اگر بر دل زارم نکنی جرم تو نیست</p></div>
<div class="m2"><p>که ز حال دل من بی‌خبرت ساخته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی به از هر به و امروز ز هر بد بترم</p></div>
<div class="m2"><p>چه توان کرد چنین در نظرت ساخته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چه رنگ از تو شکبید دل بی‌طاقت من</p></div>
<div class="m2"><p>تو که هر لحظه به رنگ دگرت ساخته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ز غیرت نگدازیم که این بلهوسان</p></div>
<div class="m2"><p>راه چون مور به تنگ شکرت ساخته‌اند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور از آغوش رقیبان نشوی پنداری</p></div>
<div class="m2"><p>دست این طایفه را در کمرت ساخته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راز من هم ز زبان تو به من می‌گویند</p></div>
<div class="m2"><p>این حریفان که چنین پرده درت ساخته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سموم نفس بلهوسان می‌ترسم</p></div>
<div class="m2"><p>که سراپای، چو گلبرگ ترت ساخته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این لطافت که تو داری و صفایی که تراست</p></div>
<div class="m2"><p>از نسیم گل و آب گهرت ساخته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناز بر تخت کی و مملکت جم دارند</p></div>
<div class="m2"><p>بیدلان تو که با خاک درت ساخته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خبر از خویش نداری به چه کاری فیّاض</p></div>
<div class="m2"><p>چه دمیدند که بی پا و سرت ساخته‌اند!</p></div></div>