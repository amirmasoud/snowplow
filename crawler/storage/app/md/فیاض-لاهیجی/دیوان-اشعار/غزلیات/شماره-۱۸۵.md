---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>بی‌لب او نشئه‌ای در ساغر و پیمانه نیست</p></div>
<div class="m2"><p>شیشة می را دماغ جلوة مستانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیرت معشوق از سیمای عاشق ظاهرست</p></div>
<div class="m2"><p>سرگذشت شمع جز در دفتر پروانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شبم در سینه آشوبی است از پهلوی دل</p></div>
<div class="m2"><p>آفتی در خانة ما جز متاع خانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشنایی ترک آدابست در قانون عشق</p></div>
<div class="m2"><p>هر که این بیگانگی‌ها می‌کند بیگانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیض‌ خواهی کعبه بگذار و ره دل پیش‌گیر</p></div>
<div class="m2"><p>گنج در ویرانه است اما به هر ویرانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جز یک مشت گل کو گه سر و گه ساغرست</p></div>
<div class="m2"><p>سرنوشت کاسة سر جز خط پیمانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی رود فیّاض از کوی تو هرگز در بهشت</p></div>
<div class="m2"><p>ترک دین از بهر دنیا چون کند دیوانه نیست!</p></div></div>