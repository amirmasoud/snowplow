---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>به جفا چون بتان قرار کنند</p></div>
<div class="m2"><p>فکر عشّاق بیقرار کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیره‌تر تا کنند عاشق را</p></div>
<div class="m2"><p>دستة زلف، تار تار کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم از جلوه گر دلی نبرند</p></div>
<div class="m2"><p>بنشینند و پس چه کار کنند!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منّت مرهمش به جان دارند</p></div>
<div class="m2"><p>هر که را خاطری فگار کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید دشمن شدند ماهوشان</p></div>
<div class="m2"><p>دوستان را چنین شکار کنند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه چه جادوگرند گلرویان</p></div>
<div class="m2"><p>برگ گل را بنفشه زار کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمگساری کنند نام ولی</p></div>
<div class="m2"><p>غم دل را یکی هزار کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بر دست اگر زنند رواست</p></div>
<div class="m2"><p>که خزان حنا بهار کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر خط می‌کشیم زحمت زلف</p></div>
<div class="m2"><p>مشق ثلث از پی غبار کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عزّت هر که بیش، خواری بیش</p></div>
<div class="m2"><p>سر منصور را به دار کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لطف پنهان کنند با فیّاض</p></div>
<div class="m2"><p>صد جفا گرچه آشکار کنند</p></div></div>