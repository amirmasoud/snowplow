---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>چون غنچه‌ام دلی است ولی کار بسته‌تر</p></div>
<div class="m2"><p>خون گشته تر ز غنچه و زنگار بسته‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرازه بگسلم چو گل از ننگ تا به کی!</p></div>
<div class="m2"><p>چون غنچه دل به صحبت هر خار بسته‌تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیم گشایشم گرة خاطرست آه</p></div>
<div class="m2"><p>چون غنچه هر نفس شودم کار، بسته‌تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را تعلّقات به مستی فزوده است</p></div>
<div class="m2"><p>زنگار بسته‌ای شده ز نگار بسته‌تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آنکه لاف عشق زنی با هزار کار</p></div>
<div class="m2"><p>دل بسته‌ای به یار و به اغیار بسته‌تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام خداست بر لب و دل بستة هوا</p></div>
<div class="m2"><p>گر بت شکسته شد شده زنّار بسته‌تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح عدم که طبل رحیل فنا زنند</p></div>
<div class="m2"><p>کس از برهنگان نبود بار بسته‌تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دامن تو روز جزا رنگ خون ما</p></div>
<div class="m2"><p>باشد زرنگ چهرة گلنار بسته‌تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیّاض جان نثار «رهی» کن که گفته است</p></div>
<div class="m2"><p>«مفت گشادگی چو شود کار بسته‌تر»</p></div></div>