---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>بر گردِ‌رخت سبزه و گل سر زده در هم</p></div>
<div class="m2"><p>دارد چمنت برگ گل و سبزة تر هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرینی و شوری ز شکر خنده و دشمنام</p></div>
<div class="m2"><p>در حلقة لعل تو نمک هست و شکر هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوته نکند دست تطاول ز اسیران</p></div>
<div class="m2"><p>زلف تو که از دوش گذشتست و کمر هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر صلح نخواهی به من، از جنگ چه مانع</p></div>
<div class="m2"><p>برخیز که ما تیغ نهادیم و سپر هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغانِ چمن، بال به پرواز شکستند</p></div>
<div class="m2"><p>ما را نبود قوّت افشاندن پر هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هول شب گور نترسیم که ما را</p></div>
<div class="m2"><p>بسیار شب این طور گذشتست و بتر هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض برد دردِ سر از کوی تو فردا</p></div>
<div class="m2"><p>سهلست مدارای تو یک روز دگر هم</p></div></div>