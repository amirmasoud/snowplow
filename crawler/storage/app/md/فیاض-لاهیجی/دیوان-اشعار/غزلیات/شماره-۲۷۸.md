---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>کس جان ز زخم خنجر مژگان نمی‌برد</p></div>
<div class="m2"><p>تا زهر چشم یار به درمان نمی‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب نیست کز چکیدة مژگانم آسمان</p></div>
<div class="m2"><p>از دامنم ستاره به دامان نمی‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز خضر خطّ یار که سیراب لعل اوست</p></div>
<div class="m2"><p>یک تشنه ره به چشمة حیوان نمی‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو بخت آنکه گوشة دامن کند شکار</p></div>
<div class="m2"><p>دستی که ره به سوی گریبان نمی‌برد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل بیقرار نالة پرواز بسته‌ایست</p></div>
<div class="m2"><p>داغم که کس قفس به گلستان نمی‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دادِ دل ز جلوة دیوانگی دهد</p></div>
<div class="m2"><p>مجنون من که ره به بیابان نمی‌برد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا صبح خاطر سر زلفش مشوّش است</p></div>
<div class="m2"><p>یک شب مرا که خواب پریشان نمی‌برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من چون کنم که بر سر بازار وصل دوست</p></div>
<div class="m2"><p>کس دین نمی‌ستاند و ایمان نمی‌برد!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در هر دم است صد خطرم در کمین دین</p></div>
<div class="m2"><p>ایمان زاهدست که شیطان نمی‌برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>داند زبان مور سلیمان من ولی</p></div>
<div class="m2"><p>این مور ره به بزم سلیمان نمی‌برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیّاض التفات عزیزان چه شد که هم</p></div>
<div class="m2"><p>یک جذبه از قمم به صفاهان نمی‌برد</p></div></div>