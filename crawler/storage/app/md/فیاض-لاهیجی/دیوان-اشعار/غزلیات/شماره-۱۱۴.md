---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>دلم پای‌بند نسیم بهارست</p></div>
<div class="m2"><p>جنون بر سر پای در انتظارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراشیده رخسارِ کاهیّ عاشق</p></div>
<div class="m2"><p>به بازار خوبان زر سکه‌دارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از مهر زلف و رخش برنگیرم</p></div>
<div class="m2"><p>که از کفر و ایمان مرا یادگارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا سوخت هجر تو صد ره ولیکن</p></div>
<div class="m2"><p>همان دل به وصل تو امّیدوارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد یار فیّاض اگر با رقیب است</p></div>
<div class="m2"><p>به کام توهم می‌شود، روزگارست</p></div></div>