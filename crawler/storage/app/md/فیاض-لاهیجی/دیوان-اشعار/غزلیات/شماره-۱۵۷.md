---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>مگو ز عقل که دام فریب خودرایی‌ست</p></div>
<div class="m2"><p>مبین به علم که آیینة خودآرایی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که بادة تحقیق خورده می‌داند</p></div>
<div class="m2"><p>که اعتراف به جهل از کمال دانایی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان ز حیرت حسن تو نقش دیوارست</p></div>
<div class="m2"><p>فضای دهر به عهد تو کُنج تنهایی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمام دهر زآزاده‌ای نشان ندهد</p></div>
<div class="m2"><p>که سرو هم به چمن زیر بار رعنایی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست وصل دل پاره‌پاره‌ای دارم</p></div>
<div class="m2"><p>که خون تپیده‌تر از حسرت تماشایی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه عقل جنون‌پرور و جنون خودروست</p></div>
<div class="m2"><p>به گل چه باج دهد لاله‌ای که صحرایی‌ست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تصرّفی که دلم از جمال لیلی دید</p></div>
<div class="m2"><p>هنوز مجنون سرگرم دشت‌پیمایی‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظاره‌ام ز تو گل چید و جای رنجش نیست</p></div>
<div class="m2"><p>به باغبان نکند عیب کس، که یغمایی‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر چه می‌نگرم روی اوست در نظرم</p></div>
<div class="m2"><p>که گفته است طلب‌کار یار، هر جایی‌ست؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظارگان تو سر در کف خطر دارند</p></div>
<div class="m2"><p>که هر نگاه تو خونیّ صد تماشایی‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بی‌مضایقگی‌های عشق دانستم</p></div>
<div class="m2"><p>که بر جنون نزدن نقص در شکیبایی‌ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زلاف محرمی کوی دوست شد معلوم</p></div>
<div class="m2"><p>که عقل با همه تمکین هنوز سودایی‌ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین که از تو گل و لاله می‌فریبندم</p></div>
<div class="m2"><p>سزد که طعنه زند دشمنم که هر جایی‌ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا دلیست که چون قطره لجّه‌آشامست</p></div>
<div class="m2"><p>چه نقص کشتی گرداب را که دریایی‌ست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به یمن همّت بدنامی از خطر رستم</p></div>
<div class="m2"><p>که پرده‌پوشی عشق است هر چه رسوایی‌ست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به ذوق گوشه‌نشینی مبند دل زنهار</p></div>
<div class="m2"><p>که سعی گمشدگی‌ها تلاش پیدایی‌ست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به محفلی که هنر عیب‌پوش شد فیّاض</p></div>
<div class="m2"><p>ندیدن هنر خویش عین بینایی‌ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بست دوست ز دنیا و آخرت فیّاض</p></div>
<div class="m2"><p>سخن یکی‌ست دگرها عبارت‌آرایی‌ست</p></div></div>