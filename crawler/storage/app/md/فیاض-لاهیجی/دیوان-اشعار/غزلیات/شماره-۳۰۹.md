---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>به باغ سبزه چو بیند خطت به سر خیزد</p></div>
<div class="m2"><p>پی تواضع قد تو سرو برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار خط تو اول ز پشت لب سر زد</p></div>
<div class="m2"><p>که سبزه از طرف چشمه بیشتر خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال آن مژه هر گه درآیدم به ضمیر</p></div>
<div class="m2"><p>به جای ناله ز دل نوک نیشتر خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترشّح مژه آبی نزد بر آتش من</p></div>
<div class="m2"><p>مشخّص است چه سیرابی از شرر خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اثر در آن دل سنگین نمی‌کند هر چند</p></div>
<div class="m2"><p>هزار نالة سر تیزم از جگر خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نقش پای درافتادگی برم غیرت</p></div>
<div class="m2"><p>که چون فتد نتواند ز جای برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه نظاره ز طغیان خون دل فیّاض</p></div>
<div class="m2"><p>گمان مبر که نگاهم ز چشم تر خیزد</p></div></div>