---
title: >-
    شمارهٔ ۵۷۳
---
# شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>کی بود دل ز می وصل تو سرشار کنم</p></div>
<div class="m2"><p>غصه را خون کنم و در دل اغیار کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طفل مکتب شوم و پیش ادیب نگهت</p></div>
<div class="m2"><p>سبق شکوة هجران تو تکرار کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ته بته گریة خون گشته که در دل گر هست</p></div>
<div class="m2"><p>گریه بگشاید و من پیش تو اظهار کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای تا سر همه از شوق تماشای رخت</p></div>
<div class="m2"><p>نگهی گردم و دریوزة دیدار کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر حوصله کز بادة بیداد تهیست</p></div>
<div class="m2"><p>بازش از تلخ می ناز تو سرشار کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو نهی لب به لب خنده و من از سر شوق</p></div>
<div class="m2"><p>گریه را بر رخ شادی مژه افشار کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنده را تا به لب لعل تو بینم گستاخ</p></div>
<div class="m2"><p>صد هوس را گرة خاطر افگار کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر کنی ناز تو و کم نکنم من ز نیاز</p></div>
<div class="m2"><p>کم کنی گوش تو من گله بسیار کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی بود همره آن شوخ چو بلبل فیّاض</p></div>
<div class="m2"><p>به کف این تازه غزل روی به گلزار کنم</p></div></div>