---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>همین نه مرهم دل‌های خسته است شراب</p></div>
<div class="m2"><p>که مومیایی رنگ شکسته است شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل شکفتگی از جام باده سیرابست</p></div>
<div class="m2"><p>کلیدِ فتحِ در عیشِ بسته است شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلسم توبة ما را که زاهدان بستند</p></div>
<div class="m2"><p>به یک تبسّم ساغر شکسته است شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صد فسون ز دلم مهر باده نتوان برد</p></div>
<div class="m2"><p>چو حرف راست به طبعم نشسته است شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خواه خبث زن و خواه مدح کن فیّاض</p></div>
<div class="m2"><p>ز حدّ ما و تو عمریست رسته است شراب</p></div></div>