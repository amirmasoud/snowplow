---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>اگرچه شعلة حسنت تمام عالم سوخت</p></div>
<div class="m2"><p>به مهربانی من در جهان کسی کم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نسبت تو چنان ذوق سوختن عام است</p></div>
<div class="m2"><p>که در کنار گل و لاله طفل شبنم سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی که آتش بیداد او زبانه کشید</p></div>
<div class="m2"><p>چه نکته بود که بیگانه جَست و محرم سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ذوق سودة الماس کرد زخم مرا</p></div>
<div class="m2"><p>تبسّمی که در اندیشه یاد مرهم سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نیم قطره که در کار مشت گل کردند</p></div>
<div class="m2"><p>چه آتش است که تا آب و خاک آدم سوخت!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان که عاشق از اهل هوس نشد ممتاز</p></div>
<div class="m2"><p>که عشوة تو بد و نیک جمله درهم سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوای وصل تو در جانم آتشی افکند</p></div>
<div class="m2"><p>که گریه را نم خونین و ناله را دم سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر ز سختی هجران نسوختم سهل است</p></div>
<div class="m2"><p>به یک ملایمت روز وصل خواهم سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمانده بود کس از اهل درد جز فیّاض</p></div>
<div class="m2"><p>چنان ز طیش برافروختی که او هم سوخت</p></div></div>