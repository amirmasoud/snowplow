---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>خویش را بر آب و بر آیینه تا اظهار کرد</p></div>
<div class="m2"><p>آب را آتش زد و آینه را گلزار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژده چشمِ دل براهِ مصرِ خواهش را که باز</p></div>
<div class="m2"><p>اینک آن آشوب کنعان روی در بازار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمرها آسوده بودم در شکر خواب عدم</p></div>
<div class="m2"><p>فتنة چشم تو زین خواب خوشم بیدار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تل خاکستری کاید به پیش راه سیل</p></div>
<div class="m2"><p>گریة من آسمان را با زمین هموار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب که زخم ناوکش پی در پیم دل می‌نواخت</p></div>
<div class="m2"><p>ز آن میان تیری که رد شد سخت بر من کار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناشکیبایی چه بر جانم غم آسان کرده بود</p></div>
<div class="m2"><p>صبر بر من عاقبت این کار را دشوار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌ثباتی‌های نازت آه را از پا فکند</p></div>
<div class="m2"><p>سرگرانی‌های چشمت ناله را بیمار کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یارب آسان کن به گوشش ناله‌های تیشه را</p></div>
<div class="m2"><p>آنکه کوه بیستون را بر دل من بار کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دشمنی‌های کم فیّاض سدّ ره نبود</p></div>
<div class="m2"><p>هر چه با من کرد آخر یاری آن یار کرد</p></div></div>