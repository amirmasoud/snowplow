---
title: >-
    شمارهٔ ۶۹۹
---
# شمارهٔ ۶۹۹

<div class="b" id="bn1"><div class="m1"><p>در جامة خوبی چه بلا حور سرشتی</p></div>
<div class="m2"><p>چون بند قبا باز کنی طرفه بهشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس که کند عیب کسی عیب سرشت است</p></div>
<div class="m2"><p>جز زشت به آیینه که گفتست که زشتی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی بهلم، روزی بوسی لب لعلم</p></div>
<div class="m2"><p>این وعده مرا جان بلب آورد و نهشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهری که نداری به کسی چشم چه داری</p></div>
<div class="m2"><p>در مزرعه آن دانه طمع دار که کشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض دریغ از تو که خامی، چه توان کرد!</p></div>
<div class="m2"><p>در آتش غم سوختی اما نبرشتی</p></div></div>