---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>من مست محبّتم چه سازم</p></div>
<div class="m2"><p>سرشار ملامتم چه سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جا در دل بیغمان ندارم</p></div>
<div class="m2"><p>من سوز محبّتم چه سازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیوسته به کام دشمنانم</p></div>
<div class="m2"><p>من بادة عشرتم چه سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مشرب خویش خوشگوارم</p></div>
<div class="m2"><p>خونابة حسرتم چه سازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راحت سر صحبتم ندارد</p></div>
<div class="m2"><p>شایستة محنتم چه سازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند ببر ز مهر اطفال</p></div>
<div class="m2"><p>من طفلْ طبیعتم چه سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض به عزلتم چه خوانی</p></div>
<div class="m2"><p>من عاشق صحبتم چه سازم</p></div></div>