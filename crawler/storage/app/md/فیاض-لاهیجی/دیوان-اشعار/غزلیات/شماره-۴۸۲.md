---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>ای جهانی به عبودیّت خاصت مختصّ</p></div>
<div class="m2"><p>پیش لطف تو مساوی چه اعمّ و چه اخصّ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل کلّ تا ابدت حصر فضایل نکند</p></div>
<div class="m2"><p>کاین حسابی‌ست که هرگز نشود مستخلص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج تنزیل که شد مجمع اخلاص و کمال</p></div>
<div class="m2"><p>صفت خُلق ترا راوی اخبار و قصص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفت جود تو جنسی است که دارد ز عموم</p></div>
<div class="m2"><p>در همه نوع سرایت چو در افراد خِصص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جبر نقصان تو فیّاض تمامیّت اوست</p></div>
<div class="m2"><p>دراتّم درج بود هر چه کم آرد انقص</p></div></div>