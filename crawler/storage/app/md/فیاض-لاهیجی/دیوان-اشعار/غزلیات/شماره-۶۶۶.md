---
title: >-
    شمارهٔ ۶۶۶
---
# شمارهٔ ۶۶۶

<div class="b" id="bn1"><div class="m1"><p>یک شب ترا بغل نگرفتم چه فایده</p></div>
<div class="m2"><p>کام از تو بی‌بدل نگرفتم چه فایده!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کامم تویی تو تا به ابد، لیک از تو من</p></div>
<div class="m2"><p>کام دل از ازل نگرفتم چه فایده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من دمی که گرم جدل بودی، از لبت</p></div>
<div class="m2"><p>بوسی به صد جدل نگرفتم چه فایده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آغوش حسرتم چه فراخی نمی‌کند</p></div>
<div class="m2"><p>کش تنگ در بغل نگرفتم چه فایده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض شعر تست که عالم گرفته است</p></div>
<div class="m2"><p>من از تو یک غزل نگرفتم چه فایده</p></div></div>