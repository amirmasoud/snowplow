---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>نگشتم ایمن ازین چرخ کینه‌خواه هنوز</p></div>
<div class="m2"><p>دو اسبه بر سر کین‌اند مهر و ماه هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار مرحله از خویشتن سفر کردم</p></div>
<div class="m2"><p>به این نشان که نیفتاده‌ام به راه هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل امید برآمد ز شاخ خشک و مرا</p></div>
<div class="m2"><p>به ناز بالش یأس است تکیه‌گاه هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه وقت آنکه نگارم به سینه داغ وداع!</p></div>
<div class="m2"><p>نکرده بر رخ او شرم من نگاه هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ثبوت دعوی مهرم به مُهر یأس رسید</p></div>
<div class="m2"><p>کرشمه می‌طلبد از دلم گواه هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار طول امل کرد صرف قامت و نیست</p></div>
<div class="m2"><p>رسا به دامنِ تأثیر دست آه هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیم ز حاصل خود آگه این قدر دانم</p></div>
<div class="m2"><p>که چشم در ره برقست، این گیاه هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امید عافیت دوست این نویدم داد</p></div>
<div class="m2"><p>که نیست در خود رحمت ترا گناه هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گدائی است به زور این شهنشهی فیّاض</p></div>
<div class="m2"><p>ولی مباد رسانی به گوش شاه هنوز</p></div></div>