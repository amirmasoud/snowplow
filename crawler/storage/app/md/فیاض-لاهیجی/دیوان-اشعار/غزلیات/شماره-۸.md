---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گفته‌ای: بیدار باید عاشق دیدار ما</p></div>
<div class="m2"><p>پاس این حرف تو دارد دیدهٔ بیدار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رتبة افتادگی را خوش به بالا برده‌ایم</p></div>
<div class="m2"><p>سایه بر بالای خود می‌افکند دیوار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوستان مرهم‌گذار و دشمنان الماس‌ریز</p></div>
<div class="m2"><p>کس نمی‌داند علاج سینة افگار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نسیمی کز چمن برگ گلی آرد برون</p></div>
<div class="m2"><p>ناله لخت دل برون کی آرد از گلزار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر یک از پود نفس در دستِ تارِ ناله‌ایست</p></div>
<div class="m2"><p>دیگر ای حسرت چه می‌خواهی ز جان زار ما!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهد زاهد شعبه‌ای از دودمان کفر ماست</p></div>
<div class="m2"><p>خویشی نزدیک دارد سبحه با زنّار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه در آلوده دامانی مثل گشتیم لیک</p></div>
<div class="m2"><p>آب رحمت می‌رود در جوی استغفار ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کدامین فتنه بازش بر سر ناز آورد</p></div>
<div class="m2"><p>بوی خون می‌آید امروز از در و دیوار ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما ز اوج آسمان بر آستان افتاده‌ایم</p></div>
<div class="m2"><p>از مروّت نیست فیّاض این قدر آزار ما</p></div></div>