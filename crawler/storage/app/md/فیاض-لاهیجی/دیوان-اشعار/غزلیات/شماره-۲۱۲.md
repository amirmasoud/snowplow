---
title: >-
    شمارهٔ ۲۱۲
---
# شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>غنچه را دور از لب لعلت دل از گلشن گرفت</p></div>
<div class="m2"><p>بی‌رخت گل، گونه از رخسار زرد من گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاشکی یک لحظه سودای مرا کردی علاج</p></div>
<div class="m2"><p>آنکه از بادام چشمم سال‌ها روغن گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهره در خون شست او هم گرچه خون من بریخت</p></div>
<div class="m2"><p>تیغ بیداد ترا دیدی که خون من گرفت؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌نهادم سر به صحرا موج اشکم پا ببست</p></div>
<div class="m2"><p>می‌شدم بیرون ز عالم گریه‌ام دامن گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابرُوَش از کشتنت فیّاض شکّی طرفه داشت</p></div>
<div class="m2"><p>عاقبت خون ترا تیغ که در گردن گرفت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا طبع باده گرمی آن تندخو گرفت</p></div>
<div class="m2"><p>نتوان ز بیم آبله دست سبو گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دام هزار سلسله می‌‌خواست روزگار</p></div>
<div class="m2"><p>زلف کجت به عهدة یک تار مو گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد اگر ز دست تو گیرد پیاله‌ای</p></div>
<div class="m2"><p>نتوان پیاله را دگر از دست او گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کم ناله زان شدم که ز طغیان خون دل</p></div>
<div class="m2"><p>چون شیشة پرم نفس اندر گلو گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فیض خط پیاله کم از خط یار نیست</p></div>
<div class="m2"><p>فیّاض می مگر زلش رنگ و بو گرفت!</p></div></div>