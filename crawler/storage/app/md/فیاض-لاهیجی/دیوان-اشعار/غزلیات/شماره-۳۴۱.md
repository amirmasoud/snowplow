---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>آنانکه پی به راه توکّل فشرده‌اند</p></div>
<div class="m2"><p>صاف رضا ز درد تحمّل فشرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل مشو ز ساغر سرشار التفات</p></div>
<div class="m2"><p>کاین جرعه را ز تیغ تغافل فشرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای مغزکاوِ دماغ دل مرا</p></div>
<div class="m2"><p>از پیچ و تاب طرّة کاکل فشرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازک ترست شکوه‌ام از بوی گل، مگر</p></div>
<div class="m2"><p>این ناله را ز نالة بلبل فشرده‌اند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا داده‌اند طبع مرا آب و رنگ فکر</p></div>
<div class="m2"><p>خون از رگ هزار تأمّل فشرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سودائیان فکر به تشبیه زلف یار</p></div>
<div class="m2"><p>خوناب حسرت از دل سنبل فشرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چهره‌اش به آب نزاکت سرشته شد</p></div>
<div class="m2"><p>از لاله رنگ و نازکی از گل فشرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عهد زلفش از رگ اندیشه اهل فکر</p></div>
<div class="m2"><p>سودای امتناع تسلسل فشرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلدادگان عشق ز شریان آرزو</p></div>
<div class="m2"><p>خون فساد عرض تجمّل فشرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مردان عشق در گذر سیل حادثات</p></div>
<div class="m2"><p>پای ثبات سخت‌تر از پل فشرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیّاض آبروی دو عالم مجرّدان</p></div>
<div class="m2"><p>از گوشة ردای توکّل فشرده‌اند</p></div></div>