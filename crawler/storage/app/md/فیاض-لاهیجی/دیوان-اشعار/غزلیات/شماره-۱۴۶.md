---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>جهان ز عکس رخت پرگل است و یاسمن است</p></div>
<div class="m2"><p>نهال قد تو سرو بلند این چمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو رحم اگر نکنی بر دلم کسی چه کند؟</p></div>
<div class="m2"><p>تنت به ناز برآورده‌ام گناه من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز یار شکوه ندارم خدای می‌داند</p></div>
<div class="m2"><p>که شکوه‌ام ز دل بی‌قرار خویشتن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حیرتم که به گرد چه انجمن کردم</p></div>
<div class="m2"><p>که یاد روی تو شمع هزار انجمن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکفته‌رویی گل از شکفته‌رویی تست</p></div>
<div class="m2"><p>اگرچه تنگ‌دلی‌های غنچه زان دهن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلسم بند قبا را شکسته‌ایم ولی</p></div>
<div class="m2"><p>هزار عقده هوس را ز بند پیرهن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که دم نزد از دشمنی ما فیّاض</p></div>
<div class="m2"><p>همین گمان به تو داریم و در تو هم سخن است</p></div></div>