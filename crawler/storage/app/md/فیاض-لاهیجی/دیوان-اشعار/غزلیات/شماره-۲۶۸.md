---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ز جور تو دل امتناعی ندارد</p></div>
<div class="m2"><p>به وصلت سر انتفاعی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن گوش می‌گیرم از قول مطرب</p></div>
<div class="m2"><p>که دیوانه تاب سماعی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی جز زلیخای کاسد محبّت</p></div>
<div class="m2"><p>به بازار یوسف متاعی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هم دورتر افکند دوستان را</p></div>
<div class="m2"><p>فلک به از ین اختراعی ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برید ار ز یاران چه یاریم کردن</p></div>
<div class="m2"><p>نمی‌خواست کس را، نزاعی ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرنج ار وداع تو ناکرده رفتم</p></div>
<div class="m2"><p>که از خویش رفتن وداعی ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال تو وبخت فیّاضِ بیدل</p></div>
<div class="m2"><p>بلی ممکن است، امتناعی ندارد</p></div></div>