---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>هر کاروان که راه به کوی تو ساختند</p></div>
<div class="m2"><p>بازارها به مصر ز بوی تو ساختند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بند دین نماند کس از کفر زلف تو</p></div>
<div class="m2"><p>زنجیرها گسست چو موی تو ساختند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشک آیدم، چه چاره کنم! عاشقی بلاست</p></div>
<div class="m2"><p>زان محرمان که راه به کوی تو ساختند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ و ستاره با دل ما بر نیامدند</p></div>
<div class="m2"><p>تا اتفاق کرده به خوی تو ساختند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل بویی از تو دارد و باغ از تو نکهتی</p></div>
<div class="m2"><p>بیچاره بلبلان که به بوی تو ساختند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غافل بنوش باده که گیتی به جم نماند</p></div>
<div class="m2"><p>صد جام خاک شد که سبوی تو ساختند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهری ز غمزة تو چو فیّاض بیدلند</p></div>
<div class="m2"><p>تنها مرا نه عاشق روی تو ساختند</p></div></div>