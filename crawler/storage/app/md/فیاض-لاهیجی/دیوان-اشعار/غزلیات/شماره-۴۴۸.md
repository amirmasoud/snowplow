---
title: >-
    شمارهٔ ۴۴۸
---
# شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>ای در سر از داغ توام هر لحظه سودای دگر</p></div>
<div class="m2"><p>در دل ز سودای توام هر دم سویدای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم مگر اندوه دل کم گردد از سودای تو</p></div>
<div class="m2"><p>انگیخت هر سودای تو در سینه سودای دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من این سویدای کثیف از دل به ناخن برکنم</p></div>
<div class="m2"><p>کز بهر مهر آن لطیف آرم سویدای دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با این تن خاکی چه سان در خلوت جان جا کنم!</p></div>
<div class="m2"><p>تبدیل اعضا بایدم کردن به اعضای دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نشکنی این دست و پا در عشق دست و پا مزن</p></div>
<div class="m2"><p>در کار هست این کار را دستِ دگر پای دگر</p></div></div>