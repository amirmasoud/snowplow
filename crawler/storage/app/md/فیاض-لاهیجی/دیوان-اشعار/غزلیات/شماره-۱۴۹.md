---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>تنها نه دیده‌ام به رخ نازنین تست</p></div>
<div class="m2"><p>هر جا که می‌روی نگهی در کمین تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنگامه گرمی ید بیضا زیاد رفت</p></div>
<div class="m2"><p>امروز دست معجزه در آستین تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احیای رسم معجز جان‌‌بخشی مسیح</p></div>
<div class="m2"><p>موقوف یک تبسّم سحرآفرین تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید در شکنجة فتراک داشتن</p></div>
<div class="m2"><p>در بند زلف خم به خم چین به چین تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض کام جو ز پری‌چهرگان فکر</p></div>
<div class="m2"><p>ملک خیال یک‌سره زیر نگین تست</p></div></div>