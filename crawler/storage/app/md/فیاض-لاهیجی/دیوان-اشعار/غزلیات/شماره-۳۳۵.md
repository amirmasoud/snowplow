---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>رسم سرایت نفس ناتوان نماند</p></div>
<div class="m2"><p>تأثیر در قلمرو آه و فغان نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست نام اهل وفا کس نمی‌برد</p></div>
<div class="m2"><p>دردا که آتشی هم ازین کاروان نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرض نفس چه می‌بری ای عندلیب زار</p></div>
<div class="m2"><p>گوشی که ناله‌ای شنود در جهان نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دودی برآور از خس و خاشاکم ای اجل</p></div>
<div class="m2"><p>کاین مرغ را علاقه در این آشیان نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تشنه آب عزّت خود بر زمین مریز</p></div>
<div class="m2"><p>کآب گهر درین صدف آسمان نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ عمر حاصل نخل امید ما</p></div>
<div class="m2"><p>چیزی جز آبله به کف باغبان نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض پیش اهل وفا یادگار ما</p></div>
<div class="m2"><p>جز نام مهربانی ما بر زبان نماند</p></div></div>