---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>یک شب که نه در وصال باشد</p></div>
<div class="m2"><p>هر لحظه هزار سال باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنرا که تو در خیال باشی</p></div>
<div class="m2"><p>تا حشر شب وصال باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قتل همه کن حرام بر خویش</p></div>
<div class="m2"><p>تا خون منت حلال باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خون منست این دیت چیست</p></div>
<div class="m2"><p>بگذار که پایمال باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ممکن نبود گذشتن از وصل</p></div>
<div class="m2"><p>هر چند امرِ محال باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد دل شکوه می‌توان داد</p></div>
<div class="m2"><p>گر یک نفسم مجال باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلکش منقار بلبلانست</p></div>
<div class="m2"><p>فیّاض چه غم که لال باشد</p></div></div>