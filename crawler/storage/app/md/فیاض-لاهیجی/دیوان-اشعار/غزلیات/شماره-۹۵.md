---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>دل بد مکن ز همدمی غم که آشناست</p></div>
<div class="m2"><p>وز کاو کاو درد مزن دم که آشناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل ز من ترانة بیگانگی شنید</p></div>
<div class="m2"><p>رم می‌کند ز سایة محرم که آشناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیگانه است شادی و بیگانه دوست عیش</p></div>
<div class="m2"><p>دستی زنم به حلقة ماتم که آشناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغ مرا که زخمی بیگانگی اوست</p></div>
<div class="m2"><p>دلچسب نیست صحبت مرهم که آشناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبرم هزار کشتی بیگانگی شکست</p></div>
<div class="m2"><p>در موج‌خیز اشکِ دمادم که آشناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از صحبت نشاط گریزم که دشمن است</p></div>
<div class="m2"><p>در سایة ملال درآیم که آشناست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض پیش یار مریز اشک آتشین</p></div>
<div class="m2"><p>گل تر شود ز گریة شبنم که آشناست</p></div></div>