---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>دارم دلی به مهر بتان عهد بسته‌ای</p></div>
<div class="m2"><p>چون رنگ عاشقان به نگاهی شکسته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خود طمع بریده‌تر از رنگ رفته‌ای</p></div>
<div class="m2"><p>دندان به خون فشرده‌تر از زخمِ‌ بسته‌‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده‌ای ز گریه چو زخم فسرده‌ای</p></div>
<div class="m2"><p>وامانده‌ای ز ناله چو تار گسسته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانی به لب چو شمع سحرگه رسیده‌ای</p></div>
<div class="m2"><p>عمری چو برق جسته، ز خود دست شسته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون طفل غنچه خون ز لب دل مکیده‌ای</p></div>
<div class="m2"><p>چون داغ لاله بر سر آتش نشسته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجنونی از قلمرو عادت رمیده‌ای</p></div>
<div class="m2"><p>دیوانه‌ای طلسم تکلّف شکسته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از لاله‌زار حسرت جاوید غنچه‌ای</p></div>
<div class="m2"><p>وز گلستان گلبن امّید دسته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاراج کرده تر ز حصار گرفته‌ای</p></div>
<div class="m2"><p>بر باد رفته‌تر ز طلسم شکسته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش بی‌تکلف از سر عالم گذشته‌ای</p></div>
<div class="m2"><p>آسوده‌ای ز بیم و ز امّید رسته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیداست تا چه خیزد از جان رفته‌ای</p></div>
<div class="m2"><p>معلوم تا چه آید از جسم خسته‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیّاض و دیده‌ای ز غم هجر گلرخان</p></div>
<div class="m2"><p>دامن به خون فشانده چو زخم نبسته‌ای</p></div></div>