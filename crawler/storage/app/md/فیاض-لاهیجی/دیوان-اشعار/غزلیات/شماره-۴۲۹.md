---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>عمدا اگر به یاد تو مشکل توان رسید</p></div>
<div class="m2"><p>گاهی امید هست که غافل توان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گم‌گشتگی کرشمة رهبر نمی‌کشد</p></div>
<div class="m2"><p>گر بگذری ز جاده به منزل توان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندین مچین به خویش که گر بگذری ز خویش</p></div>
<div class="m2"><p>یک مشت خون به دامن قاتل توان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عصیان اگر کنی ز خدا بی‌خبر مباش</p></div>
<div class="m2"><p>گاهی به حق ز وادی باطل توان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تن هوای صحبت پاکان صواب نیست</p></div>
<div class="m2"><p>گر بشکتنی سفینه به ساحل توان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانه از کجا و حریم ادب کجا</p></div>
<div class="m2"><p>آنجا به پای مردم عاقل توان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز گمان مبر که به عشرتگه قبول</p></div>
<div class="m2"><p>بی‌رنج راه و طیّ منازل توان رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زحمت مکش که صحبت دیدار و دیده نیست</p></div>
<div class="m2"><p>آنجا عجب اگر به دلایل توان رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زخمی دوست کم نبود از شهید غیر</p></div>
<div class="m2"><p>گر نه بکشتة تو به بسمل توان رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بوسة کفش ندهد دست، چون قلم</p></div>
<div class="m2"><p>گاهی به دست بوس انامل توان رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیّاض اگر عنایت برقی رسا بود</p></div>
<div class="m2"><p>از سبزة امید به ساحل توان رسید</p></div></div>