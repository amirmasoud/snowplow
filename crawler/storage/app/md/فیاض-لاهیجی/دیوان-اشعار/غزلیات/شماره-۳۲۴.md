---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>کسی به دعوی مهرش چو صبح صادق شد</p></div>
<div class="m2"><p>که چون جرس دل او با زبان موافق شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که چشم به روی تو دوخت پنداری</p></div>
<div class="m2"><p>که عکس آینه پیش رخ تو عاشق شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لباس جلوه رسایی نمود، تا آخر</p></div>
<div class="m2"><p>به سروِ قامتِ رعنای او موافق شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که صبر به خواری نمود همچو هلال</p></div>
<div class="m2"><p>به یک دو هفته بر اقران خویش فایق شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار شکر که فیّاض بی زبان آخر</p></div>
<div class="m2"><p>چو ابروی تو زبان‌آور دقایق شد</p></div></div>