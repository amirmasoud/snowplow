---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>بیا ساقیّ و آتش در زن این زهد ریائی را</p></div>
<div class="m2"><p>چو زاهد تا به کی سازم بت خود پارسایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کاه عشق کوهی برنیاید، زور بازو بین</p></div>
<div class="m2"><p>که چون با ناتوانی می‌کند خیبر گشایی را!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سبک پروازتر در اوج همّت هر که فارغ‌تر</p></div>
<div class="m2"><p>به جای بال و پر دارند مردان بی‌نوایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لاف دانش از کشف حقایق عقل می‌نازد</p></div>
<div class="m2"><p>گلی بر سر به است از صد گلستان روستایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به استغنا ز مردم دست‌مزد عشوه می‌خواهد</p></div>
<div class="m2"><p>به نام پادشاهی می‌کند زاهد گدایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نجات عشق در سرگشتگی باشد نمی‌داند</p></div>
<div class="m2"><p>درین دریا به جز باد مخالف ناخدایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تن بی‌جاست نام آدمیّت،‌کار جان دارد</p></div>
<div class="m2"><p>زنی آوازه و باشد نَفَس در رنج نایی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترا گر شیوه غیر از بندگی باشد خدا داند</p></div>
<div class="m2"><p>که بر ذات تو تهمت می‌توان کردن خدایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزاکت چون شکر فیّاض جوشد از نی کلکم</p></div>
<div class="m2"><p>رسانیدم به جای نازکی نازک‌ادایی را</p></div></div>