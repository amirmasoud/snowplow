---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>معاندان که سخن ناشنوده می‌گویند</p></div>
<div class="m2"><p>نگفته می‌شنوند و نبوده می‌گویند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دروغْ لافیِ بیدار طالعان چه بلاست</p></div>
<div class="m2"><p>که فارغند و ز بخت غنوده می‌گویند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لاف مهر خجل نیستند بلهوسان</p></div>
<div class="m2"><p>نکشته تخم، حدیث دروده می‌گویند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حرف اهل دل انگشت رد منه زنهار</p></div>
<div class="m2"><p>که این گروه سخن آزموده می‌گویند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببزم فخر ز عرض هنر تهی‌دستان</p></div>
<div class="m2"><p>حدیث سلسله وحرف دوده می‌گویند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببزم سینه‌ام این خوش تبسّمانِ نگاه</p></div>
<div class="m2"><p>حدیث جوهرِالماسِ سوده می‌گویند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز صاف آینگی طوطیان هندِ خطت</p></div>
<div class="m2"><p>سخن ز زنگ تکلّف ز دوده می‌گویند</p></div></div>