---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>آنکه کارش به اسیران همه بیدادی بود</p></div>
<div class="m2"><p>دل اغیار ازو جلوه‌گه شادی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط برآوردی و عشاق پراکنده شدند</p></div>
<div class="m2"><p>این خط سبز تو گویی خط آزادی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منصب دلبریش پیش نرفت از خسرو</p></div>
<div class="m2"><p>زور شیرین همه بر بازوی فرهادی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سفر راه محبّت چه فراغت بودست</p></div>
<div class="m2"><p>هر قدر رنج که دل خواست درین وادی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما ندیدیم دل شاد درین عالم تنگ</p></div>
<div class="m2"><p>خبری هست که وقتی به جهان شادی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک متاع است همه رفته و نارفتة عمر</p></div>
<div class="m2"><p>چون رسیدیم به منزلگه فردا دی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت صید سبکروحی ما شد فیّاض</p></div>
<div class="m2"><p>آنکه با ما همه جا در پی صیّادی بود</p></div></div>