---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ز من منّت بود سرو و سمن را</p></div>
<div class="m2"><p>به خون دیده پروردم چمن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جرم دوستی از دولت دل</p></div>
<div class="m2"><p>چه‌ها بر سر نیامد کوهکن را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم کاو کاو ناخن غم</p></div>
<div class="m2"><p>لباس نو کنم داغ کهن را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زکات نیکویی ضبط نگاه است</p></div>
<div class="m2"><p>به یاد از من نگهدار این سخن را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرامت باد نام عشق فیّاض</p></div>
<div class="m2"><p>به هفت آب ار نمی‌شویی دهن را</p></div></div>