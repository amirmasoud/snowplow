---
title: >-
    شمارهٔ ۶۲۸
---
# شمارهٔ ۶۲۸

<div class="b" id="bn1"><div class="m1"><p>چو گرد چند به دنبال کاروان گشتن</p></div>
<div class="m2"><p>توان به بال و پر مصرعی جهان گشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مراد جلوة نازست از سهی قدّان</p></div>
<div class="m2"><p>وگرنه گِرد سرِ سرو می‌توان گشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترحّمی که به من کرده‌ای گناه تو نیست</p></div>
<div class="m2"><p>که از تصرّف عشق است مهربان گشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیریم چه غم اکنون که ممکن است مرا</p></div>
<div class="m2"><p>رخ تو دیدن و بار دگر جوان گشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خوش نماست از آن تندخو مرا فیّاض</p></div>
<div class="m2"><p>به گریه دیدن و خندیدن و روان گشتن</p></div></div>