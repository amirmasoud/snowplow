---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>کثرت غم در دلم بر یاد او جا تنگ کرد</p></div>
<div class="m2"><p>کار بر خود تنگ کرد آن کو دل ما تنگ کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم زما فرهاد درر شکست و هم مجنون به تاب</p></div>
<div class="m2"><p>نالة ما بر عزیزان کوه و صحرا تنگ کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با شکوه دل فلک گنجایش جولان نیافت</p></div>
<div class="m2"><p>آخر این یک قطره، جا بر هفت دریا تنگ کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به قدر درد دل در ناله پیچم خویش را</p></div>
<div class="m2"><p>می‌توانم آسمان را بر مسیحا تنگ کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گزند نشتر غم پهلوی آسایشم</p></div>
<div class="m2"><p>جا به روی بسترم بر نقش دیبا تنگ کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت با زاهدم ذوق مدارا صلح داد</p></div>
<div class="m2"><p>وسعت مشرب دگر خوش کار بر ما تنگ کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکر آسایش غلط باشد چو دل بر جای نیست</p></div>
<div class="m2"><p>بیدلی فیّاض بر من عیش دنیا تنگ کرد</p></div></div>