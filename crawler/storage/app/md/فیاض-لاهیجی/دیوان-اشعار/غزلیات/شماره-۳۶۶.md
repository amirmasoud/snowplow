---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>ز قال رفته‌ام از دست، حال تا چه کند</p></div>
<div class="m2"><p>خیال برد ز کارم وصال تا چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاط عیش جدا می‌کُشد ملال جدا</p></div>
<div class="m2"><p>کنون شهید نشاطم ملال تا چه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار حسرت ممکن شکسته در دل ماند</p></div>
<div class="m2"><p>به جان خسته خیال محال تا چه کند!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلوع صبح جمال تو عالمی همه سوخت</p></div>
<div class="m2"><p>فروغ مهر تو وقت زوال تا چه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمیدن سمن از زیر زلف خونم ریخت</p></div>
<div class="m2"><p>بنفشه سر زدن از روی خال تا چه کند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته رنگی ما را بهارِ حسرت کرد</p></div>
<div class="m2"><p>طلوع باده به آن رنگ آل تا چه کند!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میی به بزم ازل ریخت عشق در کامم</p></div>
<div class="m2"><p>عروج نشئة آن لایزال تا چه کند!</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرور ساغر زر کرد آنچه کرد هنوز</p></div>
<div class="m2"><p>تکلّفی که ندارد سفال تا چه کند!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو یک نفس که گریبان چو غنچه کردی باز</p></div>
<div class="m2"><p>صبا چه‌ها که نکرد و شمال تا چه کند!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهال عشق تو در دانه بود و خون می‌خورد</p></div>
<div class="m2"><p>کنون که ریشه دواند این نهال تا چه کند!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به استمالتم افکند عشق در دوزخ</p></div>
<div class="m2"><p>چنین اگر دهدم گوشمال تا چه کند!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نکردنی همه کردم درین جهان فیّاض</p></div>
<div class="m2"><p>در آن جهان کرم ذوالجلال تا چه کند!</p></div></div>