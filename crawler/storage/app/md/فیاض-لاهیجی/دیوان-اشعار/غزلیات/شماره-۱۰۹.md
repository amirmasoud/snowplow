---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ز زهر ناوک او دل چو شهد خرسندست</p></div>
<div class="m2"><p>اگر غلط نکنم تیرش از نی قندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره ز طرَة خود باز اگر کنی چه شود</p></div>
<div class="m2"><p>گره‌گشایی ما عمرهاست در بندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی به دوست رسد کز جهان تواند رست</p></div>
<div class="m2"><p>بریدن از همه عالم نشان پیوندست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم به حرف تو ناصح نمی‌کشد چه کنم؟</p></div>
<div class="m2"><p>که ناوکم به جگر دل‌نشین‌تر از پندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بی‌وفای جهانی که در زمانة تو</p></div>
<div class="m2"><p>قسم به عهد تو خوردن شکست سوگندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس است نکته همین کوری زلیخا را</p></div>
<div class="m2"><p>‍... ندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>...</p></div>
<div class="m2"><p>که مهر غیر برابر به مهر فرزندست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برای قتل تو شمشیر کی کشد، فیّاض</p></div>
<div class="m2"><p>هلاک صد چو تو در بند یک شکرخندست</p></div></div>