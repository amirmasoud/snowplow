---
title: >-
    شمارهٔ ۶۱۶
---
# شمارهٔ ۶۱۶

<div class="b" id="bn1"><div class="m1"><p>هر جا که نام درد دل مبتلا بریم</p></div>
<div class="m2"><p>رنگ اثر ز چهرة سعی دوا بریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نبض خسته می‌جهد این جا دل مسیح</p></div>
<div class="m2"><p>بیمار عشق را چه به دارالشّفا بریم!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اظهار درد بیشتر از درد می‌کشد</p></div>
<div class="m2"><p>صد درد می‌کشیم که نام دوا بریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد ره به ما مطالب کونین عرض کرد</p></div>
<div class="m2"><p>همّت نهشت دست به سوی دعا بریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکلیف پادشاهی دنیا به ما مکن</p></div>
<div class="m2"><p>درد سری که هیچ ندارد کجا بریم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیگانگان به درد دل ما نمی‌رسند</p></div>
<div class="m2"><p>این تحفه به که بر در آن آشنا بریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض حرف عقل چه لازم به بزم عشق</p></div>
<div class="m2"><p>در پیش پادشاه چه نام گدا بریم!</p></div></div>