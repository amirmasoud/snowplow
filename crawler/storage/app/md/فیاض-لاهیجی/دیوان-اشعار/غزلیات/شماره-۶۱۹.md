---
title: >-
    شمارهٔ ۶۱۹
---
# شمارهٔ ۶۱۹

<div class="b" id="bn1"><div class="m1"><p>هر دم ز گرمخویی خود برفروزیَم</p></div>
<div class="m2"><p>پُر گرم هم مباش که ترسم بسوزیَم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشد دهان تنگ توام روزی از ازل</p></div>
<div class="m2"><p>زانست کز ازل به جهان تنگ روزیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان شب که دل به زلف سیاه تو بند شد</p></div>
<div class="m2"><p>روشن نمود بر همه کس تیره‌روزیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوس آنکه این لباس عناصر بیفکنم</p></div>
<div class="m2"><p>تا چند بر بدن ز غذا پینه دوزیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض ما به عیش ابد دل نهاده‌ایم</p></div>
<div class="m2"><p>دل خوش نمی‌شود ز نشاط دو روزیم</p></div></div>