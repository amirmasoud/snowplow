---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>جلوة قد تو از چاک دل ما پیداست</p></div>
<div class="m2"><p>این شکافی‌ست که تا عالم بالا پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه از ناز ز هر دیده نهان می‌گردی</p></div>
<div class="m2"><p>عکس روی تو در آیینة دل‌ها پیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن لیلی مگر از پرده برآمد، که دگر</p></div>
<div class="m2"><p>پی دیوانگی از دامن صحرا پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتی ما که سلامت رویش در خطرست</p></div>
<div class="m2"><p>عاقبت خیریش از سوزش دل‌ها پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم حیران خبر از جلوة پنهان دارد</p></div>
<div class="m2"><p>معنی این سخن از صورت دیبا پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظرف پر نیست میّسر که تراوش نکند</p></div>
<div class="m2"><p>قطره هم‌چشمی دریاش ز سیما پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ‌کس آتش پنهان نتوانست افروخت</p></div>
<div class="m2"><p>عشق پوشیده مدارید که پیدا پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قتل پنهانی من نرگس مخمور ترا</p></div>
<div class="m2"><p>هست از هر مژه پیدا و چه رسوا پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهربانی طبیب آینة حال نماست</p></div>
<div class="m2"><p>شدّت این مرض از رنگ مسیحا پیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بر آیینة امروز نظر بگشایی</p></div>
<div class="m2"><p>بی‌کم و بیش درو صورت فردا پیداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زود رفتی ز در میکده بیرون فیّاض</p></div>
<div class="m2"><p>از تو در مجلس این دردکشان جا پیداست</p></div></div>