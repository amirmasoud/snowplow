---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>ترحّم از نظر افتادگان چشم مخمورش</p></div>
<div class="m2"><p>تبسّم از نمک پروردگان لعل پر شورش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه اهمالست ساقی را نمی‌داند،‌ نمی‌بیند</p></div>
<div class="m2"><p>که مستی در خمار افتاده است از چشم مخمورش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه رسم احتیاط است این نگهبانان نازش را</p></div>
<div class="m2"><p>که در صد پرده از رنگ حیا دارند مستورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس پیشش نیاز عالمی بر خاک می‌غلتد</p></div>
<div class="m2"><p>چرا بی‌باک و بی‌پروا نباشد ناز مغرورش؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دل‌ها آن گل نورسته پر نزدیک می‌گردد</p></div>
<div class="m2"><p>خدایا در دل نیکان تودار از چشم بد دورش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه قانونیست یارب مطرب بزم محبّت را</p></div>
<div class="m2"><p>که خون آغشته خیزد نغمة شادی ز طنبورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزاران فیض در رندیست مردان مجرّد را</p></div>
<div class="m2"><p>اگر زاهد نفهمیدست باید داشت معذورش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فروغ بی‌زوال آفتاب عشق را نازم</p></div>
<div class="m2"><p>که دایم در لباس سایه جولان می‌کند نورش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا گر نیست تاب صحبت فیّاض شوریده</p></div>
<div class="m2"><p>به من بگذار این دیوانه را، من دانم و شورش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چمن گر نسخه خواهد می‌کند آن چهره تحریرش</p></div>
<div class="m2"><p>بهار ار گم شود زان خط توان برداشت تصویرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا حکم قضا نافذ نباشد در جگر کاوی</p></div>
<div class="m2"><p>به بال ناوک مژگان خوبان می‌پرد تیرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دل کاوی چه شوخی‌هاست پیکان‌های نازش را</p></div>
<div class="m2"><p>که می‌گرید به حال زخم داران زخم زهگیرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهان با من خیال کنج چشمش حرف‌ها دارد</p></div>
<div class="m2"><p>که بوی خون صد اندیشه می‌آید ز تقریرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرابی‌های عشقم آن قدر سرمایه بخشیدست</p></div>
<div class="m2"><p>که گر عالم شود ویران توانم کرد تعمیرش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مزاج عشوه کام خویش می‌خواهد چه غم دارد</p></div>
<div class="m2"><p>که خون کوهکن می‌جوشد از سرچشمة شیرش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به غفلت خفتگان عزّت از خواری خطر دارند</p></div>
<div class="m2"><p>فراموش ار شود خواب عدم مرگست تعبیرش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زندان خانة زلفش مگر روشن تواند شد</p></div>
<div class="m2"><p>به خورشید قیامت چشم روزن‌های زنجیرش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کباب مصرع صائب توان فیّاض گردیدن</p></div>
<div class="m2"><p>که از بوی کباب افتد به فکر زخم نخجیرش</p></div></div>