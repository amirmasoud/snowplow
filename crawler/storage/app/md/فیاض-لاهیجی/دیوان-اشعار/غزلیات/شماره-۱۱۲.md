---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>خاطر ما به تو صد جا بندست</p></div>
<div class="m2"><p>آن دل تست که بی‌پیوندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گره از زلف تو کس نگشاید</p></div>
<div class="m2"><p>گر چه این عقده به مویی بندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرگرانیّ سر زلف تو چیست؟</p></div>
<div class="m2"><p>چون به بوی دل ما خرسندست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد یعقوب به دردم نرسد</p></div>
<div class="m2"><p>غم دلبر، نه غم فرزندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که گفتی دل فیّاض کجاست؟</p></div>
<div class="m2"><p>در سر زلف کسی در بند است</p></div></div>