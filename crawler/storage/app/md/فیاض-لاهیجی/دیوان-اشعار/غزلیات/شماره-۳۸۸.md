---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>امشب که در چمن ز قدومش نوید بود</p></div>
<div class="m2"><p>خلوت‌سرای غنچه گلستان عید بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدیم در چمن عجب آیین اتّحاد</p></div>
<div class="m2"><p>گل داشت ز خم کاری و بلبل شهید بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک عمر در میانة ما و نگاه دوست</p></div>
<div class="m2"><p>بی‌زحمت مجادله گفت و شنید بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواندیم نامة عمل هر کسی به حشر</p></div>
<div class="m2"><p>چون روی دوست نامة عاشق سفید بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردیم سر فرو به گریبان شام هجر</p></div>
<div class="m2"><p>شکر خدا که روز قیامت پدید بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را سبب ز نَیل مسبّب حجاب شد</p></div>
<div class="m2"><p>چیزی که بست بر رخ ما در، کلید بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض چون نظر به سراپای دل فکند</p></div>
<div class="m2"><p>هر جا که دید جلوة «میرزا سعید» بود</p></div></div>