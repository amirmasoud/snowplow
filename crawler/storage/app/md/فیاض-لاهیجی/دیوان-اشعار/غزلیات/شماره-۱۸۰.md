---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>دل که بی‌زخم تو باشد به جهان خرّم نیست</p></div>
<div class="m2"><p>زخم را از تو رواجی‌ست که با مرهم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم بیهوده مرا از سروسامان انداخت</p></div>
<div class="m2"><p>گر بدانم که غم کیست که دارم، غم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گل نغمه که خوناب دل از وی نچکد</p></div>
<div class="m2"><p>در سراپردة ساز غم ما محرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سر زلف تو گاهی شکن طرّه بخست</p></div>
<div class="m2"><p>تیره‌روزی دلم را ز تو باعث کم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام دنیا نگشاید دل ما را فیّاض</p></div>
<div class="m2"><p>داغ ما چشم سیه کردة این مرهم نیست</p></div></div>