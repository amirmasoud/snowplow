---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>کار دلم در شکنج زلف تو تنگ است</p></div>
<div class="m2"><p>همچو مسلمان که در دیار فرنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عشقت ز طعنه باک ندارد</p></div>
<div class="m2"><p>این دل چون شیشه آزمودة سنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طرف کام نیست غیر ندامت</p></div>
<div class="m2"><p>چیدم و دیدم گل امید دو رنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال دل ساده‌لوح با تو بگویم</p></div>
<div class="m2"><p>چهرة آیینه را ببین که چه رنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلح دو عالم چه می‌کنیم چو با ما</p></div>
<div class="m2"><p>گوشة ابروی یار بر سر جنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادة شیرین عمر خضر چه‌ریزی</p></div>
<div class="m2"><p>در گلوی تلخ ما که شهد شرنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر دواسپه گذشت این چه شتابست</p></div>
<div class="m2"><p>نام مجو از کسی که در غم ننگ است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض کمال خجند یافته فیّاض</p></div>
<div class="m2"><p>حیف مجال سخن که قافیه تنگ است</p></div></div>