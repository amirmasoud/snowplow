---
title: >-
    شمارهٔ ۶۱۷
---
# شمارهٔ ۶۱۷

<div class="b" id="bn1"><div class="m1"><p>ما به زیر آسمان مشتی فروزان گوهریم</p></div>
<div class="m2"><p>آتشیم آتش، ولیکن در ته خاکستریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دتر مزاج لاله و در طبع گل آبیم آب</p></div>
<div class="m2"><p>لیک بر خار و خس این دشت باد صرصریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از متاع رنگ و بو رنگین بساطی چیده‌ایم</p></div>
<div class="m2"><p>حیف کش بر جای بگذاریم و غافل بگذریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته در زنگ طبیعت همچو شمشیریم لیک</p></div>
<div class="m2"><p>چون برآییم از غلاف تن، سراپا جوهریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه‌گاه ما ورای چرخ و انجم کرده‌اند</p></div>
<div class="m2"><p>این جهان دیگرست و ما جهان دیگریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه که ما را زردرویی خوش رواجی داده بود</p></div>
<div class="m2"><p>آسمان پنداشت یک چندی که ما مشت زریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جزر و مدّست اینکه گاهی بحر و گاهی قطره‌ایم</p></div>
<div class="m2"><p>قبض و بسط است اینکه گاهی شعله گاهی اخگریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوهر شرعیم و در صندوقِ دیوِ رهزنیم</p></div>
<div class="m2"><p>گوهر عقلیم و در دریایِ‌نفس کافریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعله از خود می‌کشیم و موج در خود می‌زنیم</p></div>
<div class="m2"><p>آتش یاقوتِ شادابیم و آب گوهریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هفت دریا گر بجوشد ما چو گوهر در تهیم</p></div>
<div class="m2"><p>نُه فلک گر آب گردد ما چو روغن بر سریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رنگ صد اندیشه ریزیم و فرو ریزیم باز</p></div>
<div class="m2"><p>در دیار آرزو هم بت‌شکن هم بتگریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر زمان ما را به دست دیگری می‌پرورند</p></div>
<div class="m2"><p>خاک را شاخ گلیم و آب را نیلوفریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشرت از ما می‌کشد ما هر چه از غم می‌کشیم</p></div>
<div class="m2"><p>خنده را فرماندهیم و گریه را فرمانبریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشق را دامان پاکیم و وفا را خاک راه</p></div>
<div class="m2"><p>حسن را آیینه و آیینه را خاکستریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با دلی یک پیرهن از شیشه نازک‌تر که هست</p></div>
<div class="m2"><p>مبتلای برگ گل از خاطری نازکتریم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قبله‌ای داریم غیر از کعبة اسلامیان</p></div>
<div class="m2"><p>کز درش یک لحظه برداریم اگر سر، کافریم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بلندی‌های همّت همچو فیّاض ارنه‌ایم</p></div>
<div class="m2"><p>لیک در کوتاه‌دستی‌ها ازو واپس‌تریم</p></div></div>