---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>درمانده دل به کار من و من به کار دوست</p></div>
<div class="m2"><p>دل شرمسار من شد و من شرمسار دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گریه اختیار ندارم که داده است</p></div>
<div class="m2"><p>عشقم زمام دل به کف اختیار دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بیقرار لطفم و دل بیقرار ناز</p></div>
<div class="m2"><p>تا در هلاک ما به چه باشد قرار دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نگذری ز خویش نیابی نسیم وصل</p></div>
<div class="m2"><p>برخیز از میان و نشین در کنار دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض هستی تو گرانی ز حد فزود</p></div>
<div class="m2"><p>شرمی که بیش ازین نتوان بود بار دوست</p></div></div>