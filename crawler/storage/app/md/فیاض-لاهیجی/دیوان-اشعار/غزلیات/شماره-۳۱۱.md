---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>از دعا گویا اثر بازم به مطلب می‌رسد</p></div>
<div class="m2"><p>کز لبم تا عرش امشب جوش یارب می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله آشامیم و دوزخ در ته مینای ماست</p></div>
<div class="m2"><p>باده‌نوشان را به ما کی لاف مشرب می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه‌ای کی می‌تواند کرد در گوش اثر!</p></div>
<div class="m2"><p>می‌کَنَد جان، تا کمند ناله بر لب می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیم سرشارِ فیضِ بادة عشرت، چه شد</p></div>
<div class="m2"><p>ساغر حسرت ازین بزمم لبالب می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ما روشن، که گرد کاروان صبحگاه</p></div>
<div class="m2"><p>دامن افشان از قفای لشکر شب می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمت ما بیدلان زین گِرد خوان هر صبح و شام</p></div>
<div class="m2"><p>نعمت الوان خون دل مرتب می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزرع تبخاله محتاج وجود ابر نیست</p></div>
<div class="m2"><p>این چمن را رشحه از سرچشمة تب می‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الفتی دل را مگر با کام پیدا شد که باز</p></div>
<div class="m2"><p>ناله از دل دست در آغوش مطلب می‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو سر خود گیر فیّاض ار دل و دینیت هست</p></div>
<div class="m2"><p>اینک از ره آن بلای دین و مذهب می‌رسد</p></div></div>