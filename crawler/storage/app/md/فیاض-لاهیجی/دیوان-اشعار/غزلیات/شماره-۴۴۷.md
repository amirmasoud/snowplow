---
title: >-
    شمارهٔ ۴۴۷
---
# شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>خجل شد از سرشکم خاطر افسردة اخگر</p></div>
<div class="m2"><p>گل اشکم کجا و غنچة پژمردة اخگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آن دل زندة عشقم که با این تیره روزی‌ها</p></div>
<div class="m2"><p>کند خاکسترم روشن چراغ مردة اخگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر جوید ز آه سر من برچیدة آتش</p></div>
<div class="m2"><p>گرو بازد به اشک گرم من افشردة اخگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لباس خودنمایی شعله از بالای خس دارد</p></div>
<div class="m2"><p>نمی‌پوشد کفن جز از تن خود مردة اخگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل فیّاض زا آسان تسلّی می‌توان دادن</p></div>
<div class="m2"><p>به خاکستر شود خوش خاطر آزردة اخگر</p></div></div>