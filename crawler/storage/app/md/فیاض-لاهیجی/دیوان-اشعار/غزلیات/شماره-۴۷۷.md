---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>دلی در سینه دارم مست و مدهوش</p></div>
<div class="m2"><p>به جز یاد تو از یادش فراموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر زاهد بمیرد من نگیرم</p></div>
<div class="m2"><p>به جز حرف خط پیمانه در گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زند همچون رگ نشتر گشوده</p></div>
<div class="m2"><p>ز هر تار مژه خون دلم جوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم چند باشد تار آن زلف</p></div>
<div class="m2"><p>حساب عمر خود کردم فراموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا با این دل پر هست فیّاض</p></div>
<div class="m2"><p>لبی همچون لب پیمانه خاموش</p></div></div>