---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>ازین غیرت مرا آه از دل ناشاد می‌روید</p></div>
<div class="m2"><p>که در گلشن به یاد سرو او شمشاد می‌روید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا در بیستون و صورت شیرین تماشا کن</p></div>
<div class="m2"><p>که حسن آنجا ز آب تیشة فرهاد می‌روید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا بوم و بر کوی محبّت کز زمین آنجا</p></div>
<div class="m2"><p>همه جان حزین و خاطر ناشاد می‌روید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجان سختی دل از چنگ غمش نتوان به در بردن</p></div>
<div class="m2"><p>گیاه مهر او چون جوهر از فوولاد می‌روید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری از طوق قمری تا برون کردم عجب دارم</p></div>
<div class="m2"><p>که در گلشن چرا سرو از زمین آزاد می‌روید!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل هر گه خیال ناوک مژگان او کردم</p></div>
<div class="m2"><p>به هر مو از تن من خنجر جلّاد می‌روید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال یار خواهی نازکی از سر بنه فیّاض</p></div>
<div class="m2"><p>برو کاین سبزه از بوم و بر بیداد می‌روید</p></div></div>