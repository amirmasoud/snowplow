---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>تا بوی زلف یار در آبادی منست</p></div>
<div class="m2"><p>هر لب که خنده‌ای کند از شادی منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالم وداع جلوة پرواز می‌کند</p></div>
<div class="m2"><p>یارب دگر که در پی صیّادی منست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم سراغِ جلوة سیمرغ و کیمیا</p></div>
<div class="m2"><p>هر نقش پی که گم شده در وادی منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ناله رخنه در جگر سنگ می‌کنم</p></div>
<div class="m2"><p>کو بیستون؟ که نوبت فرهادی منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض هر چه در صف زلف گفته‌ام</p></div>
<div class="m2"><p>در شعر کارنامة استادی منست</p></div></div>