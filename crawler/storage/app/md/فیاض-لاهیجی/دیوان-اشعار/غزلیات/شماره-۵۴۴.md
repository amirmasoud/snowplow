---
title: >-
    شمارهٔ ۵۴۴
---
# شمارهٔ ۵۴۴

<div class="b" id="bn1"><div class="m1"><p>غلط کردم دلت را با ترحّم آشنا کردم</p></div>
<div class="m2"><p>ستم کردم به ناکامی، به محرومی جفا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران شیوه در جور و جفا درج است خوبی را</p></div>
<div class="m2"><p>چه بد کردم ترا با خویش سرگرم وفا کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه نا کردنش فرصت به دستم داده بود امشب</p></div>
<div class="m2"><p>تغافل گوش تا می‌کرد عرض مدّعا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن کوتاهیی می‌کرد در تقریر مطلب‌ها</p></div>
<div class="m2"><p>شکستم ناله را در سینه، صد مطلب ادا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبم در گریة بی‌طاقتی صد بار در خاطر</p></div>
<div class="m2"><p>گذشت از پیش من نفرین‌کنان و من دعا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کام شوقِ بیطاقت نهادم سر به پای او</p></div>
<div class="m2"><p>نمی‌دانم چه‌ها کرد اضطراب و، من چه‌ها کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فشاندم دامن آهی به شمع خلوتم امشب</p></div>
<div class="m2"><p>به ذوق بیکسی‌ها سایه را از خود جدا کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سبک‌تر گشتم از خود هر قدر افتاده‌تر گشتم</p></div>
<div class="m2"><p>درین افتادگی‌ها سیرِ معراج فنا کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به معراج فنا آن همّتم رو داده بود امشب</p></div>
<div class="m2"><p>که عنقا صد رهم افتاد در دام و رها کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلید خلد می‌بوسد سر انگشت نیازم را</p></div>
<div class="m2"><p>نمی‌دانم گره از ابروی ناز که وا کردم!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خضر آب بقا گوید مسیحا از هوا جوید</p></div>
<div class="m2"><p>چرا فیّاض دردش را به درمان مبتلا کردم</p></div></div>