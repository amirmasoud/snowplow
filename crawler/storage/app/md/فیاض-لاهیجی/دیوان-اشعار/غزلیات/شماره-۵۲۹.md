---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>لبی پرشکوه از یاران بی‌مهر و وفا دارم</p></div>
<div class="m2"><p>دلی صد پاره از زخم زبان آشنا دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به حال چون منی کافر به کافر رحم می‌آرد</p></div>
<div class="m2"><p>چه دارید ای مسلمانان به من، من هم خدا دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرو با چاره می‌بازم به درد خویش می‌سازم</p></div>
<div class="m2"><p>ازین بی‌غم طبیبان تا به کی چشم دوا دارم!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیده دامن از دستم، پریده تیر از شستم</p></div>
<div class="m2"><p>به چندین ناامیدی‌ها هنوز امیدها دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گام عمر کوته چون توان طی کرد حیرانم</p></div>
<div class="m2"><p>ره دور و درازی را که من در زیر پا دارم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم ساحل درین دریا چو موجم مضطرب دارد</p></div>
<div class="m2"><p>دل جمعی همین از اضطراب ناخدا دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که عرض حال من پیش تویی پروا تواند کرد؟</p></div>
<div class="m2"><p>نه اشک درد دل پرداز، نه آه رسا دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به اقبال شهادت چون امید من نیفزاید!</p></div>
<div class="m2"><p>که شمشیر تو بر سر سایة بال هما دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دیداری که می‌بینم تسلّی چون کنم دل را!</p></div>
<div class="m2"><p>که دل ذوق تماشایت جدا و من جدا دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر بیگانه‌ام زین آشنایان، نیست جرم از من</p></div>
<div class="m2"><p>که تعلیم رمیدن زان نگاه آشنا دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی‌دانم که در برمت چها گفتم چها کردم!</p></div>
<div class="m2"><p>ازین کردن و زین گفتن چها بردم چها دارم!</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهر بادی غباز خاطر من چون نیفزاید</p></div>
<div class="m2"><p>که در بازار بی‌چشمان دکان توتیا دارم!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبان تا بسته‌ام با کایناتم همزبانی‌هاست</p></div>
<div class="m2"><p>تو هم گر گوش بر بندی به گوشت حرف‌ها دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دماغ درد دل نشنیدنی زان غمزه می‌خواهم</p></div>
<div class="m2"><p>که از وی درد دل‌ها دارم و آنگه بجا دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مکن فیّاض پر طعنم شدم گر کشتة مهرش</p></div>
<div class="m2"><p>که بر دامان قاتل مشت خونی خونبها دارم</p></div></div>