---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>سایه‌ای ساقی به جرم توبه از ما وامگیر</p></div>
<div class="m2"><p>تکیه بر لطف تو دارم جرم ما بر ما مگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انتقام توبه محتاج شفاعت کردن است</p></div>
<div class="m2"><p>تا به پای خم نمی‌افتیم دست ما مگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جنون بی‌بهره‌ای بر گرد هامون پر مگرد</p></div>
<div class="m2"><p>پی به اصلی تا نباشد خانه در صحرا مگیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدر ناموس خود و عرض شریعت می‌برد</p></div>
<div class="m2"><p>محتسب گو دست ما در گردن مینا مگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تر دماغی نیست با بوی گل داغ جنون</p></div>
<div class="m2"><p>این گلاب هوش‌پرور از گل سودا مگیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج طوفان بلا راهی به ساحل می‌برد</p></div>
<div class="m2"><p>گو خطر، بر کشتی ما ره درین دریا مگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خویش را نادان گرفتن مایة آسودگی‌ست</p></div>
<div class="m2"><p>گر زنادانان نباشی خویش را دانا مگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوستان با هم نشینند و غیار از جا شوند</p></div>
<div class="m2"><p>گر تو این فرصت نداری در دل ما جا مگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لذّت دنیا همین فیّاض امیدش خوش است</p></div>
<div class="m2"><p>از فلک کام دل خود می طلب، اما مگیر</p></div></div>