---
title: >-
    شمارهٔ ۶۸۵
---
# شمارهٔ ۶۸۵

<div class="b" id="bn1"><div class="m1"><p>چون گل به چمن خنده دمادم نفروشی</p></div>
<div class="m2"><p>یک غنچه تبسم به دو عالم نفروشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوچة ما جنس دوا سخت کسادست</p></div>
<div class="m2"><p>با داغ‌دلان جلوة مرهم نفروشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمایة غم سخت عزیزست نگه‌دار</p></div>
<div class="m2"><p>هر چند که بسیار خری کم نفروشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مشرب غم تشنه لبی مایة ذوقست</p></div>
<div class="m2"><p>این زهر گلوسوز به زمزم نفروشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض به جانان نکنی درد دل اظهار</p></div>
<div class="m2"><p>در کوچة راحت‌طلبان غم نفروشی</p></div></div>