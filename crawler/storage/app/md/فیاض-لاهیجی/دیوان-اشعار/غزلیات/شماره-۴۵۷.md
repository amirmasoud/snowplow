---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>ویران دل و تو در دل ویرانه‌ای هنوز</p></div>
<div class="m2"><p>این خانه شد خراب و تو در خانه‌ای هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به آشنائیت امید طرفه بود</p></div>
<div class="m2"><p>بیگانه هم شدیم و تو بیگانه‌ای هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر گمان صبر ز ما از دلت نرفت</p></div>
<div class="m2"><p>معلوم می‌شود که چه جانانه‌ای هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک سوختن جزای محبّت نمی‌شود</p></div>
<div class="m2"><p>ای شعله در تلافی پروانه‌ای هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غارت نظاره چه از حسن کم شود</p></div>
<div class="m2"><p>عالم به جرعه مست و تو خمخانه‌ای هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سبزة دیار محبت چه آفت است</p></div>
<div class="m2"><p>عالم تمام سبز و تو دردانه‌ای هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض حرف وصل دلیرانه می‌زنی</p></div>
<div class="m2"><p>معلوم می‌شود که تو دیوانه‌ای هنوز</p></div></div>