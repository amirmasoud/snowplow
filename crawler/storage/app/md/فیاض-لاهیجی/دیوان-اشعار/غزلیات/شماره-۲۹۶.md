---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>یار در دلداری ما هیچ خودداری نکرد</p></div>
<div class="m2"><p>یا چنین تمکین بما تقصیر در یاری نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدمش در خواب و گردیدم به گردش تا سحر</p></div>
<div class="m2"><p>کرد خواب آخر به من کاری که بیداری نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی را بی تو از شیون به تنگ آورده‌ام</p></div>
<div class="m2"><p>زاری من کس ندید امشب که بیزاری نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدمت عشقم برهمن کرد و یک بارم به سهو</p></div>
<div class="m2"><p>تار زلفی بر میان انداز زناری نکرد</p></div></div>