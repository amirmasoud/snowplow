---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>نظر به روی تو دارد نگاه بی‌ادبست</p></div>
<div class="m2"><p>سری به گوش تو دارد کلاه بی‌ادبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی به روی تو دستی زند گهی بر دوش</p></div>
<div class="m2"><p>در اختلاط تو زلف سیاه بی‌ادبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بی‌نقاب و من از انفعال می‌لرزم</p></div>
<div class="m2"><p>که طفل آرزویم را نگاه بی‌ادبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آتشیم چو خالت ازینکه بر لب جو</p></div>
<div class="m2"><p>به طرز خطّ تو روید گیاه بی‌ادبست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دور روی تو شرمنده نیستند افسوس</p></div>
<div class="m2"><p>که مهر خیره‌سر افتاد و ماه بی‌ادبست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاده پرده ز کار دلم چه چاره کنم</p></div>
<div class="m2"><p>که گریه طفلْ مر جست و آه بی‌ادبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس ببند و ترحم به خرج کن فیّاض</p></div>
<div class="m2"><p>که آهِ پرده درِ صبح‌گاه بی‌ادبست</p></div></div>