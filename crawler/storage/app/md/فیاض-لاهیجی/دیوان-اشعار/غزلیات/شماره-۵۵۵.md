---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>ندید کشتِ‌ امل قطره‌ای ز جوی کسم</p></div>
<div class="m2"><p>به آب آینه رو شست چهرة هوسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم بوی گلی تازه بر مشامم زد</p></div>
<div class="m2"><p>به احتیاط بگیرید رخنة قفسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که شیونم آخر به گوش کس نرسید</p></div>
<div class="m2"><p>میان قافله گم گشت نالة جرسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست کوتهم اندیشة بلندی هست</p></div>
<div class="m2"><p>هوای جلوة عنقاست در پر مگسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار مطلب سر بسته در دلم گر هست</p></div>
<div class="m2"><p>ولی ز شرم طلب تنگ می‌شود نفسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلاه گوشة فقرم به فرق ارزانی</p></div>
<div class="m2"><p>کزو به دولت جاید هست دسترسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشت تیغ وی از ننگ خون من فیّاض</p></div>
<div class="m2"><p>قبول شعله نگردید مشت خار و خسم</p></div></div>