---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>کسی چون خرد دستگاهی نداشت</p></div>
<div class="m2"><p>به سرّ قَدَر لیک راهی نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنه می‌پسندند از آدمی</p></div>
<div class="m2"><p>وگرنه فرشته گناهی نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمال ازل پیش از ایجاد عشق</p></div>
<div class="m2"><p>شهی بود اما سپاهی نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گناه اسیران به دیوان حشر</p></div>
<div class="m2"><p>چو زلف بتان عذرخواهی نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردید تا آه عاشق بلند</p></div>
<div class="m2"><p>سپهر برین تکیه‌گاهی نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز فرماندهان غیر سلطان عشق</p></div>
<div class="m2"><p>کسی همچو دل بارگاهی نداشت</p></div></div>