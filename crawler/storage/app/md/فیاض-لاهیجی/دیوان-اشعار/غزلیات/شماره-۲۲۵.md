---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>خواهم که شبی سرزده آیم به در صبح</p></div>
<div class="m2"><p>تا جرعة فیضی کشم از جام زر صبح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در فیض سحر درج بود دولت جاوید</p></div>
<div class="m2"><p>اقبال دهد باج به دریوزه‌گر صبح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفاق به نور گهر خویش بگیرم</p></div>
<div class="m2"><p>چون مهر اگر گام زنم بر اثر صبح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرواز کند با نفسم طایر معنی</p></div>
<div class="m2"><p>خورشید تواند که شود همسفر صبح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دربسته به ما صبح درآ از در یاری</p></div>
<div class="m2"><p>تا باز گشاییم به هم قفل زر صبح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگشای گریبان و سحر کن شب ما را</p></div>
<div class="m2"><p>تا چند توان گشت چنین دربدر صبح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اکنون که نوا بر لب فیّاض گره شد</p></div>
<div class="m2"><p>با مرغ سحرخیز که گوید خبر صبح</p></div></div>