---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>صیت حسنم که به آفاق به جنگ آمده‌ام</p></div>
<div class="m2"><p>پی تسخیر دو عالم ز فرنگ آمده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چهرة حسن غیورم که ز سرحدّ غرور</p></div>
<div class="m2"><p>تا به اقلیم حیا رنگ برنگ آمده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرمن خندة گل می‌دهم امشب بر باد</p></div>
<div class="m2"><p>غنچة شوخم و از شرم به تنگ آمده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک تسلیم شدن گوشة امنی دارد</p></div>
<div class="m2"><p>به امیدیست که در کام نهنگ آمده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخل گمنامیم آخر ثمر شهرت داد</p></div>
<div class="m2"><p>رفته بودم پی نام و همه ننگ آمده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژده ای دوست که دیگر نتوان بست مرا</p></div>
<div class="m2"><p>شیشة نازکم و سخت به سنگ آمده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو گفتم سپر عجز به سرکش فیّاض</p></div>
<div class="m2"><p>تیغ افکنده‌ام و با تو به جنگ آمده‌ام</p></div></div>