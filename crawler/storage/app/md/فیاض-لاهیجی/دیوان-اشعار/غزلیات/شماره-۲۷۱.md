---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>گر خود ز لطف گامی در راه ما گذارد</p></div>
<div class="m2"><p>ما دیده فرش سازیم تا یار پا گذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب که شمع مجلس با آن پری سپردست</p></div>
<div class="m2"><p>پروانه منصب خود باید به ما گذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر برخورد به زلفش باد صبا به گلشن</p></div>
<div class="m2"><p>از شرم بوی گل را در دم به جا گذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر صید دیگری دام انداختن شگون نیست</p></div>
<div class="m2"><p>با کوهکن بگویید این کار واگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانه عاجز آمد از دشمنی فیّاض</p></div>
<div class="m2"><p>این شکوه به که یک چند با آشنا گذارد</p></div></div>