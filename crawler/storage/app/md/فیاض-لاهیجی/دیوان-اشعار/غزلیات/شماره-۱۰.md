---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>کرد جا داغ جنون باز ز نو بر سر ما</p></div>
<div class="m2"><p>سایه افکند دگر بر سر ما اختر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما فلک سوختگان عیش و طرب نشناسیم</p></div>
<div class="m2"><p>عید، ماتم شود آید چو سوی کشور ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر ما سوختگان کم نشود بعد هلاک</p></div>
<div class="m2"><p>دیده روشن کند آیینه ز خاکستر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیره روزیم ولی عشق چو پرتو فکند</p></div>
<div class="m2"><p>جامه چون خلعت فانوش شود در بر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پهلوی راحت ما را نبود آرامی</p></div>
<div class="m2"><p>همچو جوهر مگر از تیغ کنی بستر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایة بال هما درد سر آرد چون موی</p></div>
<div class="m2"><p>داغ عشق تو اگر سایه کند بر سر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زردی چهرة ما قدر پر کاهش نیست</p></div>
<div class="m2"><p>سیلی عشق مگر سکّه زند بر زر ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از غنا نیست که ما ناز بر افلاک کنیم</p></div>
<div class="m2"><p>اطلس چرخِ برین تنگ بود در بر ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح ما از افق شیشه چو طالع گردد</p></div>
<div class="m2"><p>در نظر جلوة خورشید کند ساغر ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رند و تر دامن و مستیم و، تو زاهد فیّاض</p></div>
<div class="m2"><p>ما دماغ تو نداریم، برو از سر ما</p></div></div>