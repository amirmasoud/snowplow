---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بیجا نبود سعی فلک در هلاک ما</p></div>
<div class="m2"><p>خورشید گرده می‌کند از خاک پاک ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تیغ یار، حسرتِ عاشق به خون تپید</p></div>
<div class="m2"><p>دامان تست و دست امید هلاک ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در انتظار شور قیامت نشسته‌ایم</p></div>
<div class="m2"><p>باری به امتحان گذری کن به خاک ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز بهار مشرب ما را خزان نبود</p></div>
<div class="m2"><p>بدمستی افکند به زمین برگ تاک ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با های های گریه برابر نشسته است</p></div>
<div class="m2"><p>آواز خندة جگر چاک چاک ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیدردی زمانه به خود بسته‌ایم و باز</p></div>
<div class="m2"><p>خون گریه می‌کند نفس خنده‌ناک ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بوده‌ایم رند و سرانداز بوده‌ایم</p></div>
<div class="m2"><p>از کس نبوده است چو فیّاض باک ما</p></div></div>