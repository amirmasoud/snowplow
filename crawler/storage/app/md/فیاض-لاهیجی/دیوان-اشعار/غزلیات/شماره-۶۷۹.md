---
title: >-
    شمارهٔ ۶۷۹
---
# شمارهٔ ۶۷۹

<div class="b" id="bn1"><div class="m1"><p>در دلم نیست به جز عهد تو پیمان کسی</p></div>
<div class="m2"><p>چند بیداد کنی بر دلم ای جان کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر را سلسله جنبان مشو از بهر خدا</p></div>
<div class="m2"><p>زلف بر هم چه زنی آفت ایمان کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بمیرم نکشم ناز طبیبان در عشق</p></div>
<div class="m2"><p>درد او را نتوان داد به درمان کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکنم عزم سفر گر به بهشتم طلبند</p></div>
<div class="m2"><p>تا توان رفت درین شهر به فرمان کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منع دل چون کنم از دیدن رویش فیّاض</p></div>
<div class="m2"><p>نبرد اینِ دل سودازده فرمان کسی</p></div></div>