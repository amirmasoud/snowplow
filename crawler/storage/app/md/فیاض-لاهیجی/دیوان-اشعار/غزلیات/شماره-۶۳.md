---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>من و کویی که کس را نیست جز عشرت به یاد آنجا</p></div>
<div class="m2"><p>توان در خاکبازی یافت اکسیر مراد آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشاد فیض خواهی چشم عبرت در چمن بگشا</p></div>
<div class="m2"><p>که خط سبزه بلبل را کند صاحب سواد آنجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بزم عقل افکندم به کوی عشق رخت آخر</p></div>
<div class="m2"><p>هر آن خرمن که اینجا داشتم دادم به باد آنجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی از سیر گلزاری چه طرف آرزو بندد</p></div>
<div class="m2"><p>که گل هر چند می‌چینی شود حسرت ز یاد آنجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رقیب دیو سیرت را نگهبان کرده بر گنجی</p></div>
<div class="m2"><p>که نتوان کرد یک دم بر ملک هم اعتماد آنجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بیگانه در بیرون نمود آن حسن در پرده</p></div>
<div class="m2"><p>ندیدم چون درون رفتم به غیر از اتّحاد آنجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه می‌پرسی سراغ بزم عشرت از من ای فیّاض</p></div>
<div class="m2"><p>گذشت آن هم اگر گاهی گذاری می‌فتاد آنجا</p></div></div>