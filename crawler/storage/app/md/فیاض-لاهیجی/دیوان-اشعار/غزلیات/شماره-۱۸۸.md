---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>امشب که ذوق جلوه رخش بی‌نقاب داشت</p></div>
<div class="m2"><p>بر رخ هزار پرده به رنگ حجاب داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست داشت بهر تماشای حسن خویش</p></div>
<div class="m2"><p>آیینه‌ای که حوصلة آفتاب داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آیینه شد زعکس رخش آفتاب‌پوش</p></div>
<div class="m2"><p>با آنکه رخ هنوز درون نقاب داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید در شکنجة تاب از رخ تو بود</p></div>
<div class="m2"><p>روزی که التفات تو با ما عتاب داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر مطلع بلند که می‌خواند آفتاب</p></div>
<div class="m2"><p>روی تو در بدیهه هزارش جواب داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امشب که روشناس اثر بود آه ما</p></div>
<div class="m2"><p>بیدار بود بخت ولی دیده خواب داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کاروان فیض متاع زیان نبود</p></div>
<div class="m2"><p>فیّاض صبح ما ز چه در شیر آب داشت!</p></div></div>