---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>قدم از بار محنت برنخیزد</p></div>
<div class="m2"><p>محبّت هم ازین کمتر نخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دل تخم هوش کشتم نشد سبز</p></div>
<div class="m2"><p>بلی دانه ز خاکستر نخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دامنگیری خاکش در آن کو</p></div>
<div class="m2"><p>چو سایه هر که افتد برنخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی بر بستر حسرت نیفتم</p></div>
<div class="m2"><p>که سیلابم ز چشم تر نخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان افتاده‌ام فیّاض از پای</p></div>
<div class="m2"><p>که آه از دل به نشتر برنخیزد</p></div></div>