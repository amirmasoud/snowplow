---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>چشم دلم به عالم بالا گشاده‌اند</p></div>
<div class="m2"><p>در خلوتم دریچه به صحرا گشاده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طول امل فراخور عرض جمال تست</p></div>
<div class="m2"><p>آغوش موج در خور دریا گشاده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلگشت شهر و کوی نیاید ز پای عشق</p></div>
<div class="m2"><p>این گام را به دامن صحرا گشاده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ماه من برآ که به راهت ستارگان</p></div>
<div class="m2"><p>چشم امید بهر تماشا گشاده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاندم که جلوه قسمت نخل بلندتست</p></div>
<div class="m2"><p>خمیازه را بغل به تمنّا گشاده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر ما گذر به خاطر پاک تو بسته است</p></div>
<div class="m2"><p>راهی که از دلت به دل ما گشاده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آسیب فتنه بی‌خبران را فراغت است</p></div>
<div class="m2"><p>سیلی است این که بر دل دانا گشاده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طفل دیار عشق نداند بلوغ چیست</p></div>
<div class="m2"><p>در کودکی زبان مسیحا گشاده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بهر صید عصمت یوسف کنند دام</p></div>
<div class="m2"><p>صد حلقه از کمند زلیخا گشاده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اینجا گشاد قفل به دست کلید نیست</p></div>
<div class="m2"><p>بر اهل دل دری به مدارا گشاده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فیّاض جا بر اهل طرب تنگ شد بس است</p></div>
<div class="m2"><p>در بزم غم برای تو صد جا گشاده‌اند</p></div></div>