---
title: >-
    شمارهٔ ۶۹۶
---
# شمارهٔ ۶۹۶

<div class="b" id="bn1"><div class="m1"><p>خوش بی‌خبر ز حال دل زار گشته‌ای</p></div>
<div class="m2"><p>معلوم می‌وشد که خبردار گشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیداری شکسته‌دلان ضعف طالعست</p></div>
<div class="m2"><p>پیداست بخت خفته که بیدار گشته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قطره اشکم آینة جلوه‌های تست</p></div>
<div class="m2"><p>ظالم بیا ببین که چه گلزار گشته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمخوارگی علامت غمخوار دوستی است</p></div>
<div class="m2"><p>دل داده‌ای ز دست که دلدار گشته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بوی درد می‌شنوم از تو، دور نیست</p></div>
<div class="m2"><p>در لاله‌زارِ سینة افگار گشته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با قید خویشتن نتوان صید عشق شد</p></div>
<div class="m2"><p>آزاد گشته‌ای که گرفتار گشته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیدرد درد کس نتواند علاج کرد</p></div>
<div class="m2"><p>دانستم ای مسیح که بیمار گشته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای درد بر نیایم اگر با تو، دور نیست</p></div>
<div class="m2"><p>کم گشته است صبر و تو بسیار گشته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیّاض از درشتی ایّام شکوه چند</p></div>
<div class="m2"><p>آخر غنیمت است که هموار گشته‌ای</p></div></div>