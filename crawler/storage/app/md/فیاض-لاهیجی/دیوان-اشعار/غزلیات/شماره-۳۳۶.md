---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>هر کس به عارض تو خط مشک فام خواند</p></div>
<div class="m2"><p>مضمون تیره بختی ما را تمام خواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من طفل مشربم ز فنون هنر مرا</p></div>
<div class="m2"><p>چندان سواد بس که توان خطّ جام خواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در داد جمله را به سر گِرد خوانِ غم</p></div>
<div class="m2"><p>گردون صلای عامی و ما را به نام خواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسمت به روزنامة احوال من به سهو</p></div>
<div class="m2"><p>صبحی نوشته بود ولی بخت، شام خواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض تا زمن سبق خامشی گرفت</p></div>
<div class="m2"><p>مشکل دگر تواند درس کلام خواند</p></div></div>