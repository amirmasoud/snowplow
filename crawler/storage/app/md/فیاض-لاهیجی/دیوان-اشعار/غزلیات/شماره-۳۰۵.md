---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>شب از هجر رخت صد غم در غمخانة ما زد</p></div>
<div class="m2"><p>نوای جغد آتش بی‌تو در ویرانة ما زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان در قتل ما بازار رشک دلبران شد گرم</p></div>
<div class="m2"><p>که خود را شعله بی‌تابانه بر پروانة ما زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از یاد لب لعلش به خون شعله می‌غلتد</p></div>
<div class="m2"><p>چه می‌بود اینکه آتش در دل پیمانة ما زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود از نوبهار گریة ما هیچ تقصیری</p></div>
<div class="m2"><p>که برق آفتی پیدا شد و بر دانة ما زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو عرض درد دل کردیم فیّاض از حیا پیشش</p></div>
<div class="m2"><p>لبش صد خنده بر تقریر بی‌تابانة ما زد</p></div></div>