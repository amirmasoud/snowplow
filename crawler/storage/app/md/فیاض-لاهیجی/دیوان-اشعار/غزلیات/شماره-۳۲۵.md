---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و خانه به زندان شریک شد</p></div>
<div class="m2"><p>چاک جگر به چاک گریبان شریک شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تار جامه با تن نازکدلان عشق</p></div>
<div class="m2"><p>در دشمنی به خار مغیلان شریک شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش وقت عندلیب، که تا بوی گل رسید</p></div>
<div class="m2"><p>بفروخت آشیان، به گلستان شریک شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها حریف کینة ما آسمان نبود</p></div>
<div class="m2"><p>در قتل ما به غمزة خوبان شریک شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض بگذر از سر هم چشمی فلک</p></div>
<div class="m2"><p>با کس درین معامله نتوان شریک شد</p></div></div>