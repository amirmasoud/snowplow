---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>رسید موسم نوروز و روزگار شکفت</p></div>
<div class="m2"><p>ز خندة گل شادی دل بهار شکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باده ساقی نیسان به جام گلشن ریخت!</p></div>
<div class="m2"><p>که گل به روی گلش همچو روی یار شکفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تبسّم که به داغ جگر نمک‌ریز است؟</p></div>
<div class="m2"><p>کزین امید گل لاله داغدار شکفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه حاجتست به باغم که نازنین مرا</p></div>
<div class="m2"><p>دمیده سبزة خطّ و گل عذار شکفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی به کشت بهارم بیا کنون که مرا</p></div>
<div class="m2"><p>هزار رنگ گل اشک در کنار شکفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خاکِ کُشتة او سر کشیده شعلة شوق</p></div>
<div class="m2"><p>ترا خیال که شمع سر مزار شکفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکفته ریخت ز کلک من این غزل فیّاض</p></div>
<div class="m2"><p>ز بسکه خاطرم از التفات یار شکفت</p></div></div>