---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>جسم خاکی گر بمیرد آتش جان زنده است</p></div>
<div class="m2"><p>گرد میدان گر نباشد مرد میدان زنده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر خامی‌ها جفای روزگاران کیمیاست</p></div>
<div class="m2"><p>آتش افسردگان دایم به دامان زنده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ننالد کس چه فرق از زندگی تا مردگی</p></div>
<div class="m2"><p>گر نه بلبل، کس چه داند در گلستان زنده است!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق باقی شد که فانی در بقای حسن شد</p></div>
<div class="m2"><p>هر که یوسف دید داند پیر کنعان زنده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک، جان می‌کاهد امّا عمر افزون می‌کند</p></div>
<div class="m2"><p>تا ابد از نسبت لعلش بدخشان زنده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با بزرگی شیوة کوچک‌دلی‌ها پیشه کن</p></div>
<div class="m2"><p>تا ابد زین شیوه‌‌ها نام بزرگان زنده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرزوی طوف مشهد مرده را جان می‌دهد</p></div>
<div class="m2"><p>تا ابد فیّاض بر یاد خراسان زنده است</p></div></div>