---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>ذوق دیدارست کامی کز جهان دل می‌برد</p></div>
<div class="m2"><p>زاهد از دنیا نمی‌دانم چه حاصل می‌برد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به دریا داده را ز آسیب طوفان باک نیست</p></div>
<div class="m2"><p>موج آخر کشتی خود را به ساحل می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابلهان را درک ذوق عشوة دنیا کجاست</p></div>
<div class="m2"><p>وه که این زن دل ز دست مرد عاقل می‌برد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه بسیارست ره در وادی حیرت ولی</p></div>
<div class="m2"><p>جادة گمگشتگی راهی به منزل می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن، آب و رنگ نبود، عشق، پیچ و تاب نیست</p></div>
<div class="m2"><p>از مجرّد هم مجرّددان اگر دل می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز هر شکل و شمایل گیردش فیّاض‌وار</p></div>
<div class="m2"><p>هر که را دل از کف این شکل و شمایل می‌برد</p></div></div>