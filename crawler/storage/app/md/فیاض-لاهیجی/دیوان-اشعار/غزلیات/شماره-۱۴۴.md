---
title: >-
    شمارهٔ ۱۴۴
---
# شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>بی‌روی تو تا چشم صراحی نگرانست</p></div>
<div class="m2"><p>در شیشة ما باده یکی راز نهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جامة صبرم نشود پاره! که امشب</p></div>
<div class="m2"><p>در پرتو دیدار تو مهتاب کتانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ممتاز بود داغ دل از داغ سراپا</p></div>
<div class="m2"><p>این لاله درین باغ گل دست نشانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رنگ تنک ظرف توان یافت ضمیرش</p></div>
<div class="m2"><p>ناگفته به کس راز دل شیشه عیانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض ازین ورطه به ساحل نتوان رفت</p></div>
<div class="m2"><p>کز اشک تو هر جا که کناریست میانست</p></div></div>