---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>حسن محجوب ز نظّاره خطرها دارد</p></div>
<div class="m2"><p>پردة شرم کتانست و نظر مهتاب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب از قوّت بازوی که پرتو دارد؟</p></div>
<div class="m2"><p>برق این تیشه که در کوه و کمر مهتاب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر امّید زیان سود ندارد فیّاض</p></div>
<div class="m2"><p>سفر ما که در او بیم خطر مهتاب است</p></div></div>