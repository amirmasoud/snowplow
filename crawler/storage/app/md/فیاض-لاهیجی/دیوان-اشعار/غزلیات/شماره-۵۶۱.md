---
title: >-
    شمارهٔ ۵۶۱
---
# شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>ز داغ لاله چشم آمد به داغم</p></div>
<div class="m2"><p>ز بوی گل پریشان شد دماغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل امید از بالندگی‌ها</p></div>
<div class="m2"><p>نگنجد تنگ در آغوش باغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خصمی بود بادردم دوا را</p></div>
<div class="m2"><p>که مرهم شد سَبَل در چشم داغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین گمگشتگی ترسم که گیرند</p></div>
<div class="m2"><p>ز آواز پر عنقا سراغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبم پروانه بلبل بود در بزم</p></div>
<div class="m2"><p>که کرد این روغن گل در چراغم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو دیر آیی و ترسم بادة عمر</p></div>
<div class="m2"><p>نمی‌باقی نماند در ایاغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه منت از هما فیّاض کز بخت</p></div>
<div class="m2"><p>به سر بس سایبان پرِّ زاغم</p></div></div>