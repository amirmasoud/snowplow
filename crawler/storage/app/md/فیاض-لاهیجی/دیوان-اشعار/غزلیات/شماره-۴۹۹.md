---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>از سر گذشتگان را تاج و کمر مبارک</p></div>
<div class="m2"><p>بر هر که سر ندارد این درد سر مبارک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر قفس نهادن آزاد کرد ما را</p></div>
<div class="m2"><p>فتح شکستگی‌ها بر بال و پر مبارک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از ره اوفتادم خلق از پیم فتادند</p></div>
<div class="m2"><p>توفیق گمشدن کرد بر من سفر مبارک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقف خرام خسرو بوم و بر شکر شد</p></div>
<div class="m2"><p>بر جلوه‌های شیرین کوه و کمر مبارک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت هنروران را جز تیرگی شگون نیست</p></div>
<div class="m2"><p>اقبال بخت و طالع بر بی‌هنر مبارک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسودگان منزل امن و امان نشستند</p></div>
<div class="m2"><p>بر ما که رهروانیم عید خطر مبارک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پاره کردن جیب بی‌طاقتی خجل شد</p></div>
<div class="m2"><p>این رخنه‌های بیداد هم بر جگر مبارک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا هستی تو باقیست محرومی از وصالش</p></div>
<div class="m2"><p>در شیر غوطه خوردن هم بر شکر مبارک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیّاض را چه دانند قدر این مذاق تلخان</p></div>
<div class="m2"><p>در هند طوطیان را این نیشکر مبارک</p></div></div>