---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>به جان افزایم و خاک بدن در خاکدان ریزم</p></div>
<div class="m2"><p>بنای جسم را ویران کنم تا طرح جان ریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنای گلستان عشق را آن تازه معمارم</p></div>
<div class="m2"><p>که خون صد بهار افشانم و رنگ خزان ریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بلبل تا در آموزم طریق عشقبازی را</p></div>
<div class="m2"><p>کف خاکستر پروانه را در گلستان ریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغم آید ارنه تا ببیند آنچه من دیدم</p></div>
<div class="m2"><p>ز کویش مشت خاشاکی به چشم بلبلان ریزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تجلّی گل کند از هر سر خاری درین گلشن</p></div>
<div class="m2"><p>اگر عکس رخش از دیده در آب روان ریزم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خاک این چمن تا حشر گل‌ها آتشین روید</p></div>
<div class="m2"><p>اگر رشحی به ابر از اشک چشم خونفشان ریزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا از ناز حیف آید که خارم در ره افشانی</p></div>
<div class="m2"><p>مرا خود در نظر ناید که در پای تو جان ریزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خجلت‌ها که از عشق تو دارد جسم بی‌جانم</p></div>
<div class="m2"><p>به پیش این هما تا چند مشت استخوان ریزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نصیب من نشد فیّاض پروازی به کام دل</p></div>
<div class="m2"><p>از آن ترسم که آخر بال و پر د‌ر آشیان ریزم</p></div></div>