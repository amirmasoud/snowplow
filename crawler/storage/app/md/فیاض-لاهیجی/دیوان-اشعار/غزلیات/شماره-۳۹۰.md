---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>امشب که دست نالة زارم بساز بود</p></div>
<div class="m2"><p>در بزم دل، مدار به سوز و گداز بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم سفید گشته گرفتم به لخت دل</p></div>
<div class="m2"><p>این بود بر رخم در صبحی که باز بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک دم که تُرک چشم تو غافل ز ما گشت</p></div>
<div class="m2"><p>یک عمر در ولایت ما ترکتاز بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که اهل دل نفس گرم می‌زدند</p></div>
<div class="m2"><p>آهم به یاد نخل قدت سرفراز بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا از گل تو بوی حقیقت شنیده‌ام</p></div>
<div class="m2"><p>کارم مدام تربیت این مجاز بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتیم پیرو بخت جوانی نشد نصیب</p></div>
<div class="m2"><p>این عمرِ بی‌نصیبی ما خوش دراز بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض نازها که کشد از نیاز ما؟</p></div>
<div class="m2"><p>نازی که از نیاز جهان بی‌نیاز بود</p></div></div>