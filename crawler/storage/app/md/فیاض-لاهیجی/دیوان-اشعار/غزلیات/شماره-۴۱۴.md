---
title: >-
    شمارهٔ ۴۱۴
---
# شمارهٔ ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>نه همین دل در برم چون مرغ بسمل می‌جهد</p></div>
<div class="m2"><p>هر سر مو زاضطرابم چون رگ دل می‌جهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچنان آمادة زخمم که هر گه در خیال</p></div>
<div class="m2"><p>یاد آن مژگان کنم، خون از رگ دل می‌جهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه می‌پیچد غبار خاطرم بر دود آه</p></div>
<div class="m2"><p>گردباد از شرم من منزل به منزل می‌جهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌نهد عمدا به قصد سینة من در کمان</p></div>
<div class="m2"><p>هر خدنگی کز کمان غمزه غافل می‌جهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قتل عاشق دهشتی دارد که از تأثیر آن</p></div>
<div class="m2"><p>تا ابد دل در بر شمشیر قاتل می‌جهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد بیمار تب غم را مداوا مشکل است</p></div>
<div class="m2"><p>ای طبیب اینجا مرا نبض و ترا دل می‌جهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌جهد از بزم ما پیوسته فیّاض از هراس</p></div>
<div class="m2"><p>آن چنان کز صحبت دیوانه عاقل می‌جهد</p></div></div>