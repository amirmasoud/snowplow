---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>به آن قد سرفرازی می‌توان کرد</p></div>
<div class="m2"><p>به آن رخ عشقبازی می‌توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آن نازی که بر خود چید حسنت</p></div>
<div class="m2"><p>به عالم بی‌نیازی می‌توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به رویت هر که زلفت دید دانست</p></div>
<div class="m2"><p>که با خورشید بازی می‌توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دل بردی ز دست بیقراران</p></div>
<div class="m2"><p>گهی هم دلنوازی می‌توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تظلّم می‌کند بیچارگی‌ها</p></div>
<div class="m2"><p>که گاهی چاره سازی می‌توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جفا بس ای فلک کز یک شرر آه</p></div>
<div class="m2"><p>هزار انجم گدازی می‌توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه لازم کوتهی ای بختِ فیّاض</p></div>
<div class="m2"><p>چو زلف غم درازی می‌توان کرد</p></div></div>