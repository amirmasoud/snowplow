---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>با وجود ضعف کی ما را کس از جا برده است</p></div>
<div class="m2"><p>جادوی‌ها کرده زلفش تا دل ما برده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشت عمری از لگدکوب جنون آسوده بود</p></div>
<div class="m2"><p>عشق مجنونِ دگر اینک به صحرا برده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهش آغوشِ موجِ فتنه بی‌تابانه باز</p></div>
<div class="m2"><p>کشتی بی‌طاقت ما را به دریا برده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به من در حرفی ای ساقی من اینجا نیستم</p></div>
<div class="m2"><p>مدّتی شد تا مرا ذوق تماشا برده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتنة بالابلندان بودی اکنون وترا</p></div>
<div class="m2"><p>عشق بالادست ما یکباره بالا برده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محو دیداریم واعظ از سر ما دور شو</p></div>
<div class="m2"><p>ذوق امروز از دل ما بیم فردا برده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که سیر بیستون داری هوس، زحمت مکش</p></div>
<div class="m2"><p>موج آب تیشة فرهادش از جا برده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تصرّف‌های حسن شوخ او در حیرتم</p></div>
<div class="m2"><p>خویش را ننموده دل را از کف ما برده است!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌کند دل آرزوی صحبت فیّاض، از آن</p></div>
<div class="m2"><p>کز دم او پی به اعجاز مسیحا برده است</p></div></div>