---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>امشب ز فرقت تو دلم چون چراغ سوخت</p></div>
<div class="m2"><p>در خون نشست گریه ازین درد و داغ سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام و سبو ز هجر رخت دل شکسته‌اند</p></div>
<div class="m2"><p>چون داغ لاله در کف ساقی ایاغ سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محض از برای خاطر پروانه‌ها به بزم</p></div>
<div class="m2"><p>شب تا صباح شمع نشست و دماغ سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از شرم قطره‌های عرق بر عذار تو</p></div>
<div class="m2"><p>شبنم چو خال بر رخ گل‌های باغ سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض عاشقی تو و ما داغِ بی‌غمی</p></div>
<div class="m2"><p>خواهد ترا محبت و ما را فراغ سوخت</p></div></div>