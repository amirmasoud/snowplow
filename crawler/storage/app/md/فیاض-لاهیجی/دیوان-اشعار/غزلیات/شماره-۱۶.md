---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>گر مستی‌یی ز لعل بتان می‌کنیم ما</p></div>
<div class="m2"><p>در مستی شراب نهان می‌کنیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسواترست نالة لب بستگانِ بیم</p></div>
<div class="m2"><p>ما را خمش مکن که فغان می‌کنیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طفل سرشک بر مژه فریاد می‌کند</p></div>
<div class="m2"><p>ورنه ز شکوه منع زبان می‌کنیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از یک نگاهِ لطف به تاراج رفته‌ایم</p></div>
<div class="m2"><p>سودی که می‌کنیم زیان می‌کنیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دم ز موج نالة گم گشته در سراغ</p></div>
<div class="m2"><p>غم‌نامه‌ها به دوست روان می‌کنیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز خلاف عجز و تنزّل نکرده‌ایم</p></div>
<div class="m2"><p>کاری که کرده‌ایم همان می‌کنیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض می‌شویم هلاک جفای دوست</p></div>
<div class="m2"><p>آخر وفای خویش عیان می‌کنیم ما</p></div></div>