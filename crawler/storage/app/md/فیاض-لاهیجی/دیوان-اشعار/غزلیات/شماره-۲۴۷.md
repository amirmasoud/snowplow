---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>شکستن رنگ از جانم برآورد</p></div>
<div class="m2"><p>جگر از زیر دندانم برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه حسرت بود یارب اینکه امشب</p></div>
<div class="m2"><p>دمار ناله از جانم برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمش کردم نهان ناگاه طاقت</p></div>
<div class="m2"><p>سر از چاک گریبانم برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یک بی‌طاقتی آه سیه‌رو</p></div>
<div class="m2"><p>ز دل صد راز پنهانم برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد برداشت زنجیرم ز گردن</p></div>
<div class="m2"><p>جنون گِرد بیابانم برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپرس از ناله‌های بی‌ترنّم</p></div>
<div class="m2"><p>خموشی شور از افغانم برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مهر خود کنون امّیدوارم</p></div>
<div class="m2"><p>که رنگ کفر از ایمانم برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین فیّاض صبر بی‌مروّت</p></div>
<div class="m2"><p>به دشواری چه آسانم برآورد!</p></div></div>