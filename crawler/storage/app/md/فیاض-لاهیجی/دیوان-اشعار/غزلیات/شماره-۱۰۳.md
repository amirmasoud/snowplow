---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>در عهد نگاه تو که صیاد شکیب است</p></div>
<div class="m2"><p>در حلقة زلف تو کمین‌گاه فریب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیدة عشّاق تو طفلان نگه را</p></div>
<div class="m2"><p>در مشق حیا گوشة چشم تو ادیب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نیست ز بیماری آشفته دماغان</p></div>
<div class="m2"><p>سودای تو در کشور اندیشه طبیب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کشور خوبی همه فرمانبر اویند</p></div>
<div class="m2"><p>خط تو که در سلسله حسن نجیب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل تو باندازه تدبیر نباشد</p></div>
<div class="m2"><p>بر فرق که تا سایه کند بخت نصیب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دست که نالیم که در میکدة رشک</p></div>
<div class="m2"><p>با مست تماشای تو نظاره رقیب است</p></div></div>