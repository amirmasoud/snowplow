---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>گریه بی‌خون دلم رنگی ندارد در بساط</p></div>
<div class="m2"><p>بی من آه و ناله با هم کی نمایند اختلاط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سبکروح غم عشقم که دایم می‌کند</p></div>
<div class="m2"><p>گریه بی من انقباض و ناله با من انبساط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دل درد آشنای من نبودی در میان</p></div>
<div class="m2"><p>عشق را و حسن را با هم نبودی ارتباط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده‌ام تا بوده‌ام در انقلاب روزگار</p></div>
<div class="m2"><p>دوستی‌ها با ملال و دشمنی‌ها با نشاط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من مهیّا می‌کنم اسباب حسرت ورنه نیست</p></div>
<div class="m2"><p>سینه را بی‌سعی من سامان آهی در بساط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من جنونم، می‌زنم خود را به دریا بی‌درنگ</p></div>
<div class="m2"><p>عقل گو بنشین که تا کشتی بسازد احتیاط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هول فردای قیامت گر ندارم دور نیست</p></div>
<div class="m2"><p>کرده‌ام با تار زلفش عمرها مشق صراط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه بهر دیدن روی تو باشد کی کند</p></div>
<div class="m2"><p>اشک خونین با سر مژگان عاشق اختلاط!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم مخور فیّاض دنیا قابل تعمیر نیست</p></div>
<div class="m2"><p>در ره سیل فنا کردند طرح این رباط</p></div></div>