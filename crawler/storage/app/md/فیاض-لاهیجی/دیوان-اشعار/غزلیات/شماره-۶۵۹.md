---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>ای فتنه یک دم آی ز بالای زین فرو</p></div>
<div class="m2"><p>شورِ زمانه خاسته یک دم نشین فرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قامتی چنین چو به گلشن گذر کنی</p></div>
<div class="m2"><p>سرو از خجالت تو رود در زمین فرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل‌ها چو نافه در شکنش بس که خون شدند</p></div>
<div class="m2"><p>ناید ز ناز زلف ترا سر به چین فرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آئین کفرو سجدة بت نیز عالمی است</p></div>
<div class="m2"><p>زاهد چه رفته‌ای همه در فکر دین فرو!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض حاصلی ندهد بهر این حیات</p></div>
<div class="m2"><p>رفتن به بحر غصّه و غم این چنین فرو</p></div></div>