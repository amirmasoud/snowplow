---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>تا نکتة رنگینِ ادا جوش برآورد</p></div>
<div class="m2"><p>خون در رگ اندیشة ما جوش برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناپختگیی داشت جنون پیشتر از ما</p></div>
<div class="m2"><p>این باده به خمخانة ما جوش برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر موی جدا بر تنم آراسته باغی است</p></div>
<div class="m2"><p>یاد تو ز هر موی جدا جوش برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیغام تو هر موی من از داغ برآراست</p></div>
<div class="m2"><p>شاخ گلم از باد صبا جوش برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آنکه ز کافرمنشی زخم نخوردم</p></div>
<div class="m2"><p>خونم ز مزار شهدا جوش برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تابم و آتش به ته پای ندارم</p></div>
<div class="m2"><p>این دیگ ندانم ز کجا جوش برآورد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه نخوردم ز کسی نیش، چو فیّاض</p></div>
<div class="m2"><p>خون گله‌ام از مژه‌ها جوش برآورد</p></div></div>