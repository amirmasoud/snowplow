---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>در ازل سوز محبّت در دل ما جا گرفت</p></div>
<div class="m2"><p>از دل ما بود هر جا آتشی بالا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشتیم امشب حدیث روی جانان بر زبان</p></div>
<div class="m2"><p>در میان برجست شمع و از زبان ما گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ازین هرگز متاع جلوه این قیمت نداشت</p></div>
<div class="m2"><p>نخل بالادست او این نرخ را بالا گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخته‌ای بعد از شکستن هم نیامد بر کنار</p></div>
<div class="m2"><p>کشتی ما عاقبت کام دل از دریا گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن مقصود اگر در کف نباشد گو مباش</p></div>
<div class="m2"><p>گردبادم می‌توانم دامن صحرا گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چنین کز رفتن ما می‌شود خوشحال دوست</p></div>
<div class="m2"><p>رفته رفته می‌توانم در دل او جا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ازل سر در پی ما تیره‌بختان کرده‌ بود</p></div>
<div class="m2"><p>ما برون رفتیم غم فیّاض را تنها گرفت</p></div></div>