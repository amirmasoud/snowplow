---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>به زلف او دل خود را به ابرام آشنا کردم</p></div>
<div class="m2"><p>عجب رم کرده مرغی باز با دام آشنا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بی‌طاقتی خون باد کز بی محرمی آخر</p></div>
<div class="m2"><p>صبا را با سر زلفش به پیغام آشنا کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر مویم دو صد فوّارة الماس می‌جوشد</p></div>
<div class="m2"><p>به تلخی‌های هجران تو تا کام آشنا کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزاکت غوطه‌ها در شهد و شکر خورد تا آخر</p></div>
<div class="m2"><p>به صد تلخی لب او را به دشنام آشنا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر گام از ره مطلب دو صد منزل پس افتادم</p></div>
<div class="m2"><p>به راه عقل نافرجام تا کام آشنا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کامم ناگوارا بود خون بادة عشرت</p></div>
<div class="m2"><p>به صد خون دل آخر لب بدین جام آشنا کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به راه عشق فیّاض آفتی چون نیکنامی نیست</p></div>
<div class="m2"><p>غلط کردم، به نیکویی چرا نام آشنا کردم!</p></div></div>