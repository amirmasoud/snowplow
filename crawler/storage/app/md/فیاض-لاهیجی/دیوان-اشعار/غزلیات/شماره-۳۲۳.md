---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>اقبال رو نمود و به ما یار یار شد</p></div>
<div class="m2"><p>وین روزگار تیرة ما روزگار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیمان ناشکستة ما با تو تازه گشت</p></div>
<div class="m2"><p>عهد نبستة تو به ما استوار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دانه‌های جوهر تیغ تو دل خوش است</p></div>
<div class="m2"><p>این آب و دانه مرغ مرا سازگار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلگونه‌ای برای عروس خزان نماند</p></div>
<div class="m2"><p>رنگی که داشت گل همه صرف بهار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض را نوید وصال تو زنده کرد</p></div>
<div class="m2"><p>چشمی که هم نداشت برای تو چار شد</p></div></div>