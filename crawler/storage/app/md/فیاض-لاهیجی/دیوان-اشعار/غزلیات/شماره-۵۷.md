---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>جدا از دوستان در مرگ می‌بینم رهائی را</p></div>
<div class="m2"><p>براندازد خدا بنیاد ایّام جدایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین کشور رواج س‌ست عهدی از تو پیدا شد</p></div>
<div class="m2"><p>به نام خویش کردی سکّه نقد بیوفایی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم سرد غرض‌گویان ز صرصر باج می‌گیرد</p></div>
<div class="m2"><p>خدا روشن نگهدارد چراغ آشنایی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو خود پامال کردی لیک ترسم تا ابد ماند</p></div>
<div class="m2"><p>ز خونم تهمتی در گردن آن دست حنایی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آسانی نیاید شاهد عصمت در آغوشم</p></div>
<div class="m2"><p>دو عالم داده‌ام کابین عروس پارسایی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلا باشد بلا شهرت گرت در دیده جا سازند</p></div>
<div class="m2"><p>از آن بر توتیایی برگزیدم خاک پایی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین زودی عجب گر وصل او گردد نصیب من</p></div>
<div class="m2"><p>که من از راه دوری دیده‌ام این آشنایی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمی‌سازی به من تنها نیاید خود وفا از تو</p></div>
<div class="m2"><p>نگهدار از برای دیگران هم بی‌وفایی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدا روزی کند فیّاض چندی صحبت صائب</p></div>
<div class="m2"><p>که بستانیم از هم داد ایّام جدایی را</p></div></div>