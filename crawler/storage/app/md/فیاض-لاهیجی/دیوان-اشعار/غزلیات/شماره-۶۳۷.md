---
title: >-
    شمارهٔ ۶۳۷
---
# شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>برافکن پرده و از عکس آن رونوبهارم کن</p></div>
<div class="m2"><p>درآ در جلوه و از داغ حسرت لاله‌زارم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرم چون کشتة ناز تو بهر خونبهای من</p></div>
<div class="m2"><p>زمین جلوه‌گاه خویش را وقف مزارم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز لطف آشکارت قدر من پوشیده می‌ماند</p></div>
<div class="m2"><p>به پنهانی بیا در پیش مردم آشکارم کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا تا از نظر انداختی با خاک یکسانم</p></div>
<div class="m2"><p>بیا بر رغم گردون یک کف خاک اعتبارم کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قرار صبر با خود داده‌ام اما پشیمانم</p></div>
<div class="m2"><p>بگو فیّاض ازو حرفیّ و دیگر بیقرارم کن</p></div></div>