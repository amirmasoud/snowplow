---
title: >-
    شمارهٔ ۵۶۷
---
# شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>ز دانش مرا بس، که نام تو دانم</p></div>
<div class="m2"><p>سوادم همین بس که نام تو خوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا نالم از ضعف، آن قوّتم بس</p></div>
<div class="m2"><p>که آهی به عمری به پایان رسانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز آه شرربار حسرت ثمر کو!</p></div>
<div class="m2"><p>نهالی که در عرصة دل نشانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراد دو عالم اگر در کف آید</p></div>
<div class="m2"><p>کف خاک نبود که بر سر فشانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آن بی‌نشان کوی کس ره نبردست</p></div>
<div class="m2"><p>عبث قاصد اشک را می‌دوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گَرد سواران نخواهم رسیدن</p></div>
<div class="m2"><p>درین دشت گلگون چه بر می‌جهانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه پرسی ز من حال فیّاض بیدل</p></div>
<div class="m2"><p>تو دادی به صحرا سرش، من چه دانم؟</p></div></div>