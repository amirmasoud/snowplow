---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>گفتگوی چشم جادویی مرا دیوانه کرد</p></div>
<div class="m2"><p>همزبانی‌های ابرویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیسوی زنجیر، عاقل می‌کند دیوانه را</p></div>
<div class="m2"><p>حلقة زنجیر گیسویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رام با دیوانه می‌شد پیش ازین آهو ولی</p></div>
<div class="m2"><p>رم نمودن‌های آهویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاش از یک تار مو تدبیر زنجیرم کند</p></div>
<div class="m2"><p>آنکه از هر یک سر مویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کجا و طاقت این های های گریه‌ها</p></div>
<div class="m2"><p>وه که از بی‌طاقتی هویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با گل وصل بتان چشم آشنایان دیگرند</p></div>
<div class="m2"><p>زین می هوش آزما بویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌کشش کوشش طریق رهروان کعبه نیست</p></div>
<div class="m2"><p>اندرین وادی تکاپویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیستون عشق کندن پیشة هر تیشه نیست</p></div>
<div class="m2"><p>امتحان دست و بازویی مرا دیوانه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کو به کو فیّاض گشتم گر چه پر همراه عقل</p></div>
<div class="m2"><p>عشق آخر در سر کویی مرا دیوانه کرد</p></div></div>