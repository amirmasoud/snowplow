---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>دانشم حاشا که ابرِ آفتاب من شود</p></div>
<div class="m2"><p>من از آن عارف‌ترم کاین بت حجاب من شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقِ‌ کافر بین، که می‌گوید عجب دارم اگر</p></div>
<div class="m2"><p>چار دفتر شرح یک حرف از کتاب من شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل باقی می‌توانم تا ابد بیدار دید</p></div>
<div class="m2"><p>گر فنای ذاتیم یک لحظه خواب من شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من به این سوزی که در دل دارم از شرم گناه</p></div>
<div class="m2"><p>گر به یاد من فتد دوزخ کباب من شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شب وصل تو طول روز محشر باشدش</p></div>
<div class="m2"><p>آن قدر نبود که صرف اضطراب من شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستیم خمخانه خالی کرد و شورش برنخاست</p></div>
<div class="m2"><p>گردش چشمی مگر جام شراب من شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با دل بی‌عشق اگر فیّاض از دنیا روم</p></div>
<div class="m2"><p>راحت فردوس در عقبی عذاب من شود</p></div></div>