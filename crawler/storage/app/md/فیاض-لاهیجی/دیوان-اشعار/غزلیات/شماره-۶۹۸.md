---
title: >-
    شمارهٔ ۶۹۸
---
# شمارهٔ ۶۹۸

<div class="b" id="bn1"><div class="m1"><p>سخت بی‌مهر و جفا پیشه و پر فن شده‌ای</p></div>
<div class="m2"><p>جان من خوب به کام دل دشمن شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستم داغ که بیگانه شدی با من لیک</p></div>
<div class="m2"><p>داغ ازینم که فرمودة دشمن شده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون طلا دست فشارِ دم گرمم بودی</p></div>
<div class="m2"><p>که دمید این نفس سرد که آهن شده‌ای؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب پر از خندة گل، چهره پر از لالة رنگ</p></div>
<div class="m2"><p>دگر از بهر تماشای که گلشن شده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش خانة من بودی و کافیت نبود</p></div>
<div class="m2"><p>برقِ هر جا که یکی سوخته خرمن شده‌ای!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرم من چیست گرم آتش سوداست بلند</p></div>
<div class="m2"><p>که برین شعله تو عمریست که دامن شده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرود یاد توام یک نفس از پیش نظر</p></div>
<div class="m2"><p>من نیم بی‌تو دمی گر چه تو بی من شده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این زمان تیره شود خاطرت از من، چه عجب</p></div>
<div class="m2"><p>که ز خاکسترم ای آینه روشن شده‌ای!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار چون با تو ندارد سر یاری فیّاض</p></div>
<div class="m2"><p>تو چه در دعوی مهرش رگ گردن شده‌ای؟</p></div></div>