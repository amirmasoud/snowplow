---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>عندلیب گلشنم گلخن نصیبم کرده‌اند</p></div>
<div class="m2"><p>بخت بد بنگر! چنان بودم چنینم کرده‌آند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ازل چون طرح دریا ریختند از اشک من</p></div>
<div class="m2"><p>موج این دریا ز چین آستینم کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بر دستم نکویان پرورش‌ها داده‌اند</p></div>
<div class="m2"><p>تا چو داغ عشق خوبان دلنشینم کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شکنج زلفم و دل سوی خطّم می‌کشد</p></div>
<div class="m2"><p>کاردانان محبّت پیش‌بینم کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چین ابرو وا خرید از اختلاط مردمم</p></div>
<div class="m2"><p>این گره دانسته در کار جبینم کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا شکار عشق خواهم گشت آخر یا جنون</p></div>
<div class="m2"><p>این دو شیرافکن دگر خوش در کمینم کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گلستان قمم فیّاض فارغ از بهشت</p></div>
<div class="m2"><p>گل‌فروشان بلبل این سرزمینم کرده‌اند</p></div></div>