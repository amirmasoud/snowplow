---
title: >-
    شمارهٔ ۶۵۰
---
# شمارهٔ ۶۵۰

<div class="b" id="bn1"><div class="m1"><p>از دل هوای وصل تو کی می‌رود برون</p></div>
<div class="m2"><p>کی نشئه از طبیعت می می‌رود برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب ز شرم نالة زارم نفس نزد</p></div>
<div class="m2"><p>این عقده کی ز خاطر نی می‌رود برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر باد دستی مژه‌ام بشنود به خواب</p></div>
<div class="m2"><p>دود از نهاد حاتم طی می‌رود برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر صبحدم ز خشکی افسردگان زهد</p></div>
<div class="m2"><p>خون از دماغ شیشة می می‌رود برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض را وداع‌کنان دید یار و گفت</p></div>
<div class="m2"><p>مجنون این قبیله ز حی می‌رود برون</p></div></div>