---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>به جیب چاک و به دل داغ هجر یار ندارم</p></div>
<div class="m2"><p>چه سان به سیر روم روی لاله‌زار ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب هوس چه گشایم به آب چشمة حیوان!</p></div>
<div class="m2"><p>اجازت از دم شمشیر آبدار ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسید دست و گریبان، بهار و سبزه برآمد</p></div>
<div class="m2"><p>خوشم که خاطر جمعی درین بهار ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس ز ناله تهی گشت و دیده پر ز سرشکم</p></div>
<div class="m2"><p>پیاده‌ای بدوانم اگر سوار ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجاست سایة سنگی، کجاست بوتة خاری</p></div>
<div class="m2"><p>که من ز خونِ دل و دیده یادگار ندارم!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید لطف و تمنّای رحم و چشم مروّت</p></div>
<div class="m2"><p>به کی حسرتت ای بیوفا چه کار ندارم!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفت گرد سوار غمم جهان و چه حاصل</p></div>
<div class="m2"><p>که کارْ دیده سواری درین غبار ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر به هیچ ندارم، حذر ز هیچ نگیرم</p></div>
<div class="m2"><p>چه اعتبار ازین به که اعتبار ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکار آهوی فرصت غنیمت است چه سازم</p></div>
<div class="m2"><p>که من تهیّة انداز این شکار ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به سر اگر گل دولت نزد رسایی بختم</p></div>
<div class="m2"><p>همین بس است که در سینه خار خار ندارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین ستمکده فیّاض از چه خوش ننشینم</p></div>
<div class="m2"><p>چه بی‌تعلقی‌یی من درین دیار ندارم؟</p></div></div>