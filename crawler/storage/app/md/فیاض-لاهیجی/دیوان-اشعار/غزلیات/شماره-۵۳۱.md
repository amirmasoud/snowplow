---
title: >-
    شمارهٔ ۵۳۱
---
# شمارهٔ ۵۳۱

<div class="b" id="bn1"><div class="m1"><p>همین نه لخت جگر در دهان غم دارم</p></div>
<div class="m2"><p>هزار نعمت الوان به خوان غم دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناز بالش عشرت فرو نمی‌آید</p></div>
<div class="m2"><p>سری که بهر تو بر آستان غم دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا رسد که کنم نازها به شاهد عیش</p></div>
<div class="m2"><p>که دست در کمر شاهدان غم دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عهدة صفت حسن برنمی‌آید</p></div>
<div class="m2"><p>زبان عشق که من در دهان غم دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چه‌گونه کند عیش صید خویش که من</p></div>
<div class="m2"><p>هزار زخم نمایان نشان غم دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چه‌گونه کند عیش صید خویش که من</p></div>
<div class="m2"><p>هزار زخم نمایان نشان غم دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمی ز عشرت سرگشتگی نیاساید</p></div>
<div class="m2"><p>چه کوکب است که بر آسمان غم دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دمی که دیده نه بر جلوة قدت بازست</p></div>
<div class="m2"><p>هزار قافله حسرت زیان غم دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو که فارغم از عیش در غمت هیهات</p></div>
<div class="m2"><p>چه مغز عیش که در استخوان غم دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عیش عالم اگر پشت پا زنم سهلست</p></div>
<div class="m2"><p>کنون که دست طرب در میان غم دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین به چشم کمم گو مبین زمانه که من</p></div>
<div class="m2"><p>عجیب سلطنتی در جهان غم دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همین نه بلبل و پروانه ریزه‌خوار منند</p></div>
<div class="m2"><p>هزار سوخته جان میهمان غم دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پرند نالة شب، پرنیان آه سحر</p></div>
<div class="m2"><p>چه جنس‌هاست که من در دکان غم دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قطار اشک ز غمنامه‌ام پرست و هنوز</p></div>
<div class="m2"><p>به زیر هر مژه صد داستان غم دارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه غم ز گرمی خورشید عشرتست مرا</p></div>
<div class="m2"><p>که چتر آه به سر سایبان غم دارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خون دیدة غلتیده در شکایت هجر</p></div>
<div class="m2"><p>به هر دیار روان کاروان غم دارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبان زمزمة عیش گر چه نیست مرا</p></div>
<div class="m2"><p>ولی به هر سر مویی زبان غم دارم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشانه‌اش دل بیدرد آسمان حیف است</p></div>
<div class="m2"><p>خدنگ ناله که من در کمان غم دارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه شد که عیش ز نامهربانیم داغست</p></div>
<div class="m2"><p>ولی درون و برون مهربان غم دارم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز عیش دوستی بیوفا دلت فیّاض</p></div>
<div class="m2"><p>چه شکوه‌هاست که خاطرنشان غم دارم</p></div></div>