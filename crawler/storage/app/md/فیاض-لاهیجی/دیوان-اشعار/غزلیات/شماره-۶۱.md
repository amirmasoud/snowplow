---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>این قدر آب هوس بستن به جوی دل چرا</p></div>
<div class="m2"><p>می‌کنی ای دانه استعداد را باطل چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ازین منزل به جای زاد بردار و برو</p></div>
<div class="m2"><p>کار آسانست بر خود می‌کنی مشکل چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دلم خون کرد منع گریه از یاری نبود</p></div>
<div class="m2"><p>سر بریدن جای خود بستن رگ بسمل چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل ابروی تو با شمشیر بازی می‌کند</p></div>
<div class="m2"><p>تهمت خون کس نهد بر دامن قاتل چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنگ با او کن که ننگ از وی نیابی در گریز</p></div>
<div class="m2"><p>با من دیوانه می‌کاوی تو ای عاقل چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک من کردی چه گویم یارِ دشمن هم شدی؟</p></div>
<div class="m2"><p>قدر خود را هم نمی‌دانی تو ای جاهل چرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدق اگر داری ز کذب مردمان آسوده باش</p></div>
<div class="m2"><p>گر نمی‌رنجی ز حق، رنجیدن از باطل چرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پاکی دامن کجا و تهمت آلودگی</p></div>
<div class="m2"><p>آفتاب اندودن ای دشمن به مشت گل چرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دامن دریا نگردد تهمت‌آلود غبار</p></div>
<div class="m2"><p>می‌دهی بر باد گرد دامن ساحل چرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطره گر در خود نگنجد جای استبعاد نیست</p></div>
<div class="m2"><p>این قدر کم‌ظرفی از یاران دریادل چرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاقلان را هوش اگر در سر نباشد دور نیست</p></div>
<div class="m2"><p>این‌قدر بیهوشی از رندان لایعقل چرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اندر آن وادی که مجنونست این‌آوازه نیست</p></div>
<div class="m2"><p>می‌فزایی ای جرس بار دل محمل چرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کعبه می‌خواهی درین وادی بیابان مرگ شو</p></div>
<div class="m2"><p>تا توان فیّاض در ره مرد در منزل چرا</p></div></div>