---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>تا صبا طرف نقاب از روی رخشانی شکست</p></div>
<div class="m2"><p>از خجالت هر طرف رنگ گلستانی شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاری از زلف کجش زنّار یک عالم دلست</p></div>
<div class="m2"><p>از شکست هر سو مو کافرستانی شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطرم بر هر چه می‌آید جراحت می‌شود</p></div>
<div class="m2"><p>تا کجا سنگ جفایی شیشة جانی شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهد با زنّار زلفی بسته‌ام کز موج کفر</p></div>
<div class="m2"><p>هر طرف در جلوه آمد فوج ایمانی شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون من یارب چه خاصیّت دهد کز هر طرف</p></div>
<div class="m2"><p>بر میان هر کس به قتلم طرف دامانی شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به محرومی نهادم این کشاکش تا به کی</p></div>
<div class="m2"><p>من همان گیرم که عهدی بست و پیمانی شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوسه‌ای کردم هوس چین بر لب خندان فکند</p></div>
<div class="m2"><p>بشکند تا خاطر ما شکرستانی شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پر ز حسرت‌ریزه شد دامن به جای لخت دل</p></div>
<div class="m2"><p>بسکه در دل حسرتم از لعل خندانی شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی تو خون دیده بود و لخت دل فیّاض را</p></div>
<div class="m2"><p>گر دم آبی گرفت و گر لب نانی شکست</p></div></div>