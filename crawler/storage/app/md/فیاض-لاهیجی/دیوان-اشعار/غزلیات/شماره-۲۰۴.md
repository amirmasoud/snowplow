---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>شور جنونم از سر این بخت شوم رفت</p></div>
<div class="m2"><p>فرّ همای عشق به تاراج بوم رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کشت ما که آبخورش آتش دلست</p></div>
<div class="m2"><p>سرگرم شد سموم و ستم بر سموم رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فال خرابی غم او می‌زند دلم</p></div>
<div class="m2"><p>آسودگی ز طالع این مرزوبوم رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افکند سایه بار غمت بر وجود ما</p></div>
<div class="m2"><p>آتش دگر به تربیت شمع و موم رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض تا چه فتنه دهد روزِ خطِّ یار</p></div>
<div class="m2"><p>اینک سپاه زنگ به تاراج روم رفت</p></div></div>