---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>نماز شام چنان نشئة میش گل کرد</p></div>
<div class="m2"><p>که آفتاب ز بدمستیش تنزل کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم زلف تو زد بر دماغ او هر گاه</p></div>
<div class="m2"><p>صبا به عهد تو میل شمیم سنبل کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر به باغ تو بودی که امشب از بلبل</p></div>
<div class="m2"><p>گل دریده دهن صد سخن تحمل کرد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری ز سرّ دهانش برون نبرد آخر</p></div>
<div class="m2"><p>دلم چو غنچه درین نکته بس تأمّل کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریب زلف نخوردی ولی ببین فیّاض</p></div>
<div class="m2"><p>که چون شکار تو آخر کمند کاکل کرد!</p></div></div>