---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>بستم ز چارگوشة عالم نگاه را</p></div>
<div class="m2"><p>تا دیدم آن دو گوشة چشم سیاه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرقی میان روز و شب خود نکرده‌ایم</p></div>
<div class="m2"><p>تا فرق کرده‌ایم سپید و سیاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچیده دود در جگر ای گریه مهلتی</p></div>
<div class="m2"><p>چندان که از شکنجه برآریم آه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را رواست دعوی اعجاز چون کلیم</p></div>
<div class="m2"><p>کز آستین خود به در آرد گواه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض عمرهاست که در چشم خونفشان</p></div>
<div class="m2"><p>دارم ذخیره سرمة آن خاک راه را</p></div></div>