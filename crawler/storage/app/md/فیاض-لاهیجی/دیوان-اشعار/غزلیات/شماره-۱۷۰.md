---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>گر تو پنداری بتان را بی‌وفایی نیست هست</p></div>
<div class="m2"><p>وربگویی در میان رسم جدایی نیست هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گمان داری که خوبان را غم دل نیست هست</p></div>
<div class="m2"><p>ور بگویی هم که کافر ماجرایی نیست هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر گمان داری که هجران کمتر از مرگست نیست</p></div>
<div class="m2"><p>ور بگویی کز اجل بدتر جدایی نیست هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو گویی عشق را از عقل پروا هست نیست</p></div>
<div class="m2"><p>وربگویی عقل اینجا روستایی نیست هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر گمان داری که عشق از پارسا دورست نیست</p></div>
<div class="m2"><p>وربگویی عشق مرگ پارسایی نیست هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو پنداری نگاهش آشنای ماست نیست</p></div>
<div class="m2"><p>ور بگویی در نگاهش آشنایی نیست هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که هرگز نالة زار مرا نشنیده‌ای</p></div>
<div class="m2"><p>گر گمان داری که جرم نارسایی نیست هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر گمان داری که هر چند آفتاب انوری</p></div>
<div class="m2"><p>بی‌جمالت چشم ما بی‌روشنایی نیست هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کسی را این گمان باشد که گمراه ترا</p></div>
<div class="m2"><p>با وجود گمرهی صد رهنمایی نیست هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بگویم من که اشکم را روایی هست نیست</p></div>
<div class="m2"><p>ور بگویم ناروایی را روایی نیست هست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای که گفتی بهترست از دیگران فیّاض ما</p></div>
<div class="m2"><p>ور گمان داری که کمتر از سنایی نیست هست</p></div></div>