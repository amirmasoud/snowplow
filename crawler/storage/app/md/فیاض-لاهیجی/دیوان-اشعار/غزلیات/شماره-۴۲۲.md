---
title: >-
    شمارهٔ ۴۲۲
---
# شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>چو بینم کبک، یادم جلوة دلدار می‌آید</p></div>
<div class="m2"><p>که هر گه در خرام آید بدین رفتار می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاهم جیب و دامن پر گل از رخسار او برگشت</p></div>
<div class="m2"><p>به آیینی که پنداری کس از گلزار می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالت هر شب آید بر سر بالین و ننشیند</p></div>
<div class="m2"><p>مگر شرمش ز پاس دیدة بیدار می‌آید!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا زهرِ نگاه او غنیمت دان که این مرهم</p></div>
<div class="m2"><p>برای زخم بندی‌ها ترا در کار می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی در سایة دیوار او فیّاض عشرت کن</p></div>
<div class="m2"><p>که روزی آفتابت بر سر دیوار می‌آید</p></div></div>