---
title: >-
    شمارهٔ ۵۸۸
---
# شمارهٔ ۵۸۸

<div class="b" id="bn1"><div class="m1"><p>چند از گل سخن عرض تجمّل شنوم</p></div>
<div class="m2"><p>لاف آشفتگی از طرّة سنبل شنوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی گل آمد و رفت از کف من صبر و قرار</p></div>
<div class="m2"><p>چه کنم آه اگر نالة بلبل شنوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده ‌بردار که غیرت به جنون خواهد زد</p></div>
<div class="m2"><p>تا کی از باد صبا وصف رخ گل شنوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه در گوش تو آهسته رقیبان گویند</p></div>
<div class="m2"><p>من به آواز بلندش ز تغافل شنوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشت فیّاض ز غیرت دل بیتاب مرا</p></div>
<div class="m2"><p>تا کی از وی سخن تاب و تحمّل شنوم</p></div></div>