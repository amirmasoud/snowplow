---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>چمن به خرّمی و گل به بار نزدیک است</p></div>
<div class="m2"><p>جنون تهیّه نکرد و بهار نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشان ساحل این بحر ای که می‌پرسی</p></div>
<div class="m2"><p>اگر به موج سواری کنار نزدیک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غیر یأس ابد در میانه فاصله نیست</p></div>
<div class="m2"><p>بیا که وعدة دیدار یار نزدیک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خنده دست ندارم ولی به دولت عشق</p></div>
<div class="m2"><p>رهم به گریة بی‌اختیار نزدیک است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گواه دعوی منصور می‌تواند شد</p></div>
<div class="m2"><p>کسی که یک سر و گردن به دار نزدیک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر از دریچة گرداب کن برون و ببین</p></div>
<div class="m2"><p>که چون محیط فنا را کنار نزدیک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدم به وادی حرمان به راه نه فیّاض</p></div>
<div class="m2"><p>که راه وصل ازین رهگذار، نزدیک است</p></div></div>