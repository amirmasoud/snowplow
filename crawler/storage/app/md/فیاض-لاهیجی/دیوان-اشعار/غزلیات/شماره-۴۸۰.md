---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>به این شوخی که دارد پی بهار جلوه رنگینش</p></div>
<div class="m2"><p>متانت کو که بی‌تابانه گردد گرد تمکینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به داغ بیکسی هرگز نمی‌سوزد کسی را دل</p></div>
<div class="m2"><p>به بیماری که یاد دوست باشد شمع بالینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جرم لاغری فتراکش از من سر نمی‌پیچد</p></div>
<div class="m2"><p>هنوز از مشت خونی می‌توانم کرد رنگینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه دارد مهربانی‌ها بجز نامهربانی‌ها</p></div>
<div class="m2"><p>دعاگوی ویم آخر که می‌ترسم زنفرینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شوخی‌های فهم است اینکه چون بر وی غزل خوانم</p></div>
<div class="m2"><p>به مدح گوشة ابرو کند نشنیده تحسینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه پروای شکار چون منی آن چین ابرو را</p></div>
<div class="m2"><p>پری در دام دارد موج‌های زلف پرچینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه می‌خواهد ز جان من سر زلف سمن سایش</p></div>
<div class="m2"><p>چه می‌گوید به خون من کف دست نگارینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبارم در کمین اضطرابی خفته می‌خواهم</p></div>
<div class="m2"><p>که شوخی‌ها کند تکلیف دولتخانة زینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«رهی» را بنده شد فیّاض از بس فیض خدمت‌ها</p></div>
<div class="m2"><p>در اندک مدّتی گردید خدمتگار دیرینش</p></div></div>