---
title: >-
    شمارهٔ ۵۶۸
---
# شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>جا در دل پاک تو نمودن نتوانم</p></div>
<div class="m2"><p>چون گرد بر آن آینه بودن نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژگان شکند خار به چشمم شب دوری</p></div>
<div class="m2"><p>گر بخت شوم بی‌تو غنودن نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چهره اگر گرد ملالی ننشیند</p></div>
<div class="m2"><p>رخساره به آیینه نمودن نتوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد خاک سرم در ره بیداد تو ای وای</p></div>
<div class="m2"><p>از خرمن حسن تو ربودن نتوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کاهربا گشته‌ام، اما پر کاهی</p></div>
<div class="m2"><p>از خرمن حسن تو ربودن نتوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذوق سفرم گرم چنان کرده که دیگر</p></div>
<div class="m2"><p>گر یار کند وعده که بودن، نتوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض اگر صیقف دیدار نباشد</p></div>
<div class="m2"><p>زآیینة دل زنگ زدودن نتوانم</p></div></div>