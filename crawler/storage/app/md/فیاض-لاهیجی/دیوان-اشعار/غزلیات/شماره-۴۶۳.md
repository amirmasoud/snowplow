---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>بلهوس گر نیستی دور از کنار یار باش</p></div>
<div class="m2"><p>گر نه گلچینی برو خار سر دیوار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نظر چون خفتگان آیند بیداران عشق</p></div>
<div class="m2"><p>مستی‌یی گر می‌کنی در بزم ما هشیار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه باشی آن چنان کن کز تو آسایش برند</p></div>
<div class="m2"><p>در گلستان گل نباشی رخنهٔ دیوار باش</p></div></div>