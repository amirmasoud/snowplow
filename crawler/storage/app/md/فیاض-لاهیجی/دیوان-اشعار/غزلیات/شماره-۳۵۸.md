---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>گر آتش تو چو شمع استخوانم آب کند</p></div>
<div class="m2"><p>عجب که رشتة عمر مرا به تاب کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه جوهر تیغ تو همچو مرغابی</p></div>
<div class="m2"><p>برای صید دلم دانه‌ای در آب کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حیله چشم تو خود را به خواب می‌دارد</p></div>
<div class="m2"><p>بدین فسانه مگر صید را به خواب کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک به نسخة تقدیر سال‌ها گردید</p></div>
<div class="m2"><p>که بهر من همه روز بد انتخاب کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دور خود رخِ آن مه ز مشک تر فیّاض</p></div>
<div class="m2"><p>خطی کشیده که تسخیر آفتاب کند</p></div></div>