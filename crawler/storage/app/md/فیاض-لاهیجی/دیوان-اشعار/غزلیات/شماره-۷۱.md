---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>چو کرد خاک ره یار روزگار مرا</p></div>
<div class="m2"><p>به چشم عالمیان داد اعتبار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغ بوی گل و برگ گلستانم نیست</p></div>
<div class="m2"><p>مگر به باغ برد نالة هزار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کف نه جام میی، در نظر نه روی مهی</p></div>
<div class="m2"><p>گلی شکفته نگردید ازین بهار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز طرز دیدن پنهانت این چنین پیداست</p></div>
<div class="m2"><p>که راز دل زتو خواهد شد آشکار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا ز گردش چشم تو حال می‌گردد</p></div>
<div class="m2"><p>به گردش مه و مهر فلک چه کار مرا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز نارسایی اقبالم ای فلک خوش باش</p></div>
<div class="m2"><p>به دامنی نرسم گر کنی غبار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که زار و ضعیفم ز هجر او فیّاض</p></div>
<div class="m2"><p>مگر صبا برساند به کوی یار مرا</p></div></div>