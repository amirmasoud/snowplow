---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>یک نفس خود را ز غم آزاد می‌باید گرفت</p></div>
<div class="m2"><p>صرفه‌ای از عمر بی‌بنیاد می‌باید گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقة فتراک را در گوش می‌باید کشید</p></div>
<div class="m2"><p>سرمه از گرد ره صیّاد می‌باید گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق خندیدن گرت انگشت بر لب می‌زند</p></div>
<div class="m2"><p>خویشتن را همچو گل بر باد می‌باید گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محبّت با دلی از شیشه نازک‌تر که هست</p></div>
<div class="m2"><p>جان آهن سینة فولاد می‌باید گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای تدبیر محبَّت می‌رسد آخر به سنگ</p></div>
<div class="m2"><p>غیرتی از تیشة فرهاد می‌باید گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فن جانبازی عشّاق هم تعلیم‌هاست</p></div>
<div class="m2"><p>سر خط این مشق از استاد می‌باید گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبری را شیوه‌ها جز حسن مادرزاد هست</p></div>
<div class="m2"><p>شمّه‌ای گفتم دگرها یاد می‌باید گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساغر پر تا خط بغداد بر لب بی‌غمی است</p></div>
<div class="m2"><p>پادشه را خطة بغداد می‌باید گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترا چو خط طَرَفِ روی لاله رنگ گرفت</p></div>
<div class="m2"><p>ز رشک آینة آفتاب زنگ گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکست قیمت لعل آن لب و به خنده شکست</p></div>
<div class="m2"><p>گرفت ملک دل آن غمزه و به جنگ گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شکوه گرم زبان‌آوری شدم افسوس</p></div>
<div class="m2"><p>که آن دهن سر راهم گرفت و تنگ گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به دوستی تو گر شهره‌ام عجب نبود</p></div>
<div class="m2"><p>مرا که گوهر اشک از رخ تو رنگ گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه اعتراض دلش سخت اگر بود، فیاض</p></div>
<div class="m2"><p>نکرده است ز سختی کسی به سنگ، گرفت</p></div></div>