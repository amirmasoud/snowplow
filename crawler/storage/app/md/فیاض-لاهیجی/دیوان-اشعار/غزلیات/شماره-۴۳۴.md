---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>حرف عقبا را دل آگاهم از دنیا شنید</p></div>
<div class="m2"><p>از لب امروز گوشم نغمة فردا شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبه تنهایی چنان کردم که در شب‌های غم</p></div>
<div class="m2"><p>می‌توان از آشیانم نالة عنقا شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم پر حرف تو امشب گفت در گوش دلم</p></div>
<div class="m2"><p>هر چه خواهد از لب خاموش من فردا شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به یاد ناله آرم عالمی پر می‌شود</p></div>
<div class="m2"><p>آنچه گوش ناشنو زان لعل ناگویا شنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاسبان شد مضطرب امشب چو در زندان غم</p></div>
<div class="m2"><p>نالة زنجیر من از دامن صحرا شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشنا با حرف عاشق نیست غیر از گوش یار</p></div>
<div class="m2"><p>درد دل در پیش مردم کردم او تنها شنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رمز عشق و عاشقی فیّاض جز با کس نگفت</p></div>
<div class="m2"><p>در جهان هر کس سرود این نغمه را از ما شنید</p></div></div>