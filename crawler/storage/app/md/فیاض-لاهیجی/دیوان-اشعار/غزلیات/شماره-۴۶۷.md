---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>به گوش درد دلم گاه گاه می‌رسدش</p></div>
<div class="m2"><p>پیام ناله و تقریر آه می‌رسدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکرده فرق ز می خون عاشقان طفلی است</p></div>
<div class="m2"><p>فرشته گر ننویسد گناه، می‌رسدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترانه‌ریزی لعلش به باده جا دارد</p></div>
<div class="m2"><p>کنایه سنجی عارض به ماه می‌رسدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آنکه درد دلی خواند از رسالة عشق</p></div>
<div class="m2"><p>به هر چه حکم کند بی‌گواه می‌رسدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تغافلی که به فیّاض می‌کند نگهش</p></div>
<div class="m2"><p>به سوی ما نکند گر نگاه می‌رسدش</p></div></div>