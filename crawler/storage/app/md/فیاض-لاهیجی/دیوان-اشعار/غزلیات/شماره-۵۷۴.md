---
title: >-
    شمارهٔ ۵۷۴
---
# شمارهٔ ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>خواهم ز داغ عشق لباسی به بر کنم</p></div>
<div class="m2"><p>الماس کو که ابرة این آستر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ناله بی‌رفیق به جنگ اثر متاز</p></div>
<div class="m2"><p>صبری که آه سوخته را هم خبر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر اوج شعله جلوة پروازم آرزوست</p></div>
<div class="m2"><p>کو آتشی که تربیت بال و پر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی گریه پرتوی ندهد صبح طالعم</p></div>
<div class="m2"><p>کو خون که روغنی به چراغ سحر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معشوق مبتذل شود از یک نگاه گرم</p></div>
<div class="m2"><p>نگذاشت غیرتم که در آن دل اثر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فیّاض نامه‌ای که نویسم به نزد یار</p></div>
<div class="m2"><p>از شوق سر نکرده قلم گریه سر کنم</p></div></div>