---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>که می‌تواند از پیش یار برخیزد؟</p></div>
<div class="m2"><p>نشسته‌ایم که از ما غبار برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اضطرار سپردیم خویش را در عشق</p></div>
<div class="m2"><p>بگو ز مجلس ما اختیار برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی که سرمة خطّش نمی‌کشم در چشم</p></div>
<div class="m2"><p>ز دیده جای نگاهم غبار برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هلاک تربیت مجلسی شوم که در آن</p></div>
<div class="m2"><p>خزان اگر بنشیند بهار برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصیب زورق فیّاض باد طوفانی</p></div>
<div class="m2"><p>که موج صد خطرش از کنار برخیزد</p></div></div>