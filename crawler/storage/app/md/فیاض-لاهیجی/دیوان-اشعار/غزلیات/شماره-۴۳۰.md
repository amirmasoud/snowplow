---
title: >-
    شمارهٔ ۴۳۰
---
# شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>به دلم تیرنگاهی ز تو غافل نرسید</p></div>
<div class="m2"><p>سر نیشی به رگ آبلة دل نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره سودای سر زلف تو بی‌پایانست</p></div>
<div class="m2"><p>هیچ اندیشه درین راه به منزل نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کِشتة طالع ما را چه خطر پیش آمد</p></div>
<div class="m2"><p>که برون کرد سر از خاک و به حاصل نرسید!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر عشق است و در او موج خطر بسیارست</p></div>
<div class="m2"><p>کس درین لجّه به جز موج به ساحل نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موجة خون شهیدان ز سر چرخ گذشت</p></div>
<div class="m2"><p>این قدر بود که بر دامن قاتل نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست و تیغ تو ز بس وقف تماشایی بود</p></div>
<div class="m2"><p>نوبت فرصت نظّاره به بسمل نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر غلط بخشیت ای چرخ همین نکته بس است</p></div>
<div class="m2"><p>که عطاهای تو یک بار به قابل نرسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این کهن جامة دنیا که طرازش ز فناست</p></div>
<div class="m2"><p>تا که دیوانه نیفکند به عاقل نرسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورق خاطر فیّاض غلط در غلط است</p></div>
<div class="m2"><p>نظر مرد برین صفحة باطل نرسید</p></div></div>