---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>ز طرز غنچه پی بردم که شرم روی او دارد</p></div>
<div class="m2"><p>ز رنگ شعله دانستم که بیم خوی او دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمان داری که آزادند نزدیکان او؟ نه نه</p></div>
<div class="m2"><p>گره‌بند قبا پیوسته در پهلوی او دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه در دیده می‌دزدم که دارد عکس او در بر</p></div>
<div class="m2"><p>نفس در سینه می‌پیچم که بوی موی او دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌بیند ز شرم عکس در آیینه هم گاهی</p></div>
<div class="m2"><p>دل عاشق مگر آیینه‌ای بر روی او دارد!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا قفل گره در زنگ دارد خاطر فیّاض؟</p></div>
<div class="m2"><p>کلید یک جهان دل گوشة ابروی او دارد</p></div></div>