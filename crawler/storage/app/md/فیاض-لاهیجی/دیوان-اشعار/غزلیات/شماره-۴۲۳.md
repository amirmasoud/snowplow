---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>چنانم بزم عشرت بی‌لبش دلگیر می‌آید</p></div>
<div class="m2"><p>که موج باده در چشمم دم شمشیر می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم بر زبان حرف که دارد کلک تقریرم</p></div>
<div class="m2"><p>ولی دانم که بوی خون ازین تقریر می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جرم از طاعتم امّیدواری بیشتر باشد</p></div>
<div class="m2"><p>که از سعی آنچه ناید از تو ای تقصیر می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به این تلخی به امیّد تو عمر جاودان خواهم</p></div>
<div class="m2"><p>که می‌گوید که هرگز عاشق از جان سیر می‌آید!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرنگ ناله آثار سرایت می‌توان دانست</p></div>
<div class="m2"><p>تو زودآ ای نفس بر لب که قاصد دیر می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شد امروز اگر غیر از گریبان نیست در دستم</p></div>
<div class="m2"><p>که فردا دست جیب آموزِ دامنگیر می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو احوال دل دیوانه در تحریر می‌آرم</p></div>
<div class="m2"><p>صریر خامه‌ام چون نالة زنجیر می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوانی کرده ضایع کی به پیری می‌رسد جایی</p></div>
<div class="m2"><p>که بی‌ایوار کمتر کاری از شبگیر می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خون خوردن بدل شد میل شیر آن طفل را لیکن</p></div>
<div class="m2"><p>هنوزش از لب خونخواره بوی شیر می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چنانم روز روشن بی‌رخ او دشمن جان شد</p></div>
<div class="m2"><p>که می‌پندارم از روزن به چشمم تیر می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فتراک نگاهش موج خون همنشینان بین</p></div>
<div class="m2"><p>به مهمان رفته پنداری که از نخجیر می‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توان با بی‌نیازی پوست از گردون بر آوردن</p></div>
<div class="m2"><p>در آن بیشه که این آهوست کمتر شیر می‌آید</p></div></div>