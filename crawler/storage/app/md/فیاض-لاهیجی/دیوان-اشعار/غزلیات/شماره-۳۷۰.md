---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>هر که این شیرین لبان محو شکر خندش کنند</p></div>
<div class="m2"><p>شیرة زهر هلاهل شربت قندش کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس درین بیت‌الحزن بی‌بهره از آزار نیست</p></div>
<div class="m2"><p>درد معشوق ار نباشد داغِ فرزندش کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست منع ناله ممکن ناتوان عشق را</p></div>
<div class="m2"><p>گر به تیغ طعنه چون نی بند از بندش کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نهالی را که گیرد ریشه در گلزار عشق</p></div>
<div class="m2"><p>برکنند اول زبیخ آنگه برومندش کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شورش دیوانه از هم بگسلد زنجیر را</p></div>
<div class="m2"><p>مفت مجنونی که بی‌زنجیر در بندش کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جسم اگر خاکست و جان پاکست استبعاد نیست</p></div>
<div class="m2"><p>بوتة خاری بود کز گل برومندش کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو فیّاضی ندارند این وفا پروردگان</p></div>
<div class="m2"><p>تا به انواع جفا دانسته خرسندش کنند</p></div></div>