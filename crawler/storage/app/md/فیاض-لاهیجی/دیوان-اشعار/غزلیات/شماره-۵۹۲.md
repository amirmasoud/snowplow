---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>دیده را از پرتو روی تو تابی می‌دهم</p></div>
<div class="m2"><p>گلشن نظّاره را از شعله آبی می‌دهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لب لعلت حدیثی بر زبان می‌آورم</p></div>
<div class="m2"><p>گفتگو را غوطه در موج شرابی می‌دهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیچ و تاب عطسه در مغز جهان می‌افکنم</p></div>
<div class="m2"><p>چون ز بزم دل برون بوی کبابی می‌دهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کنم آغوش مژگانی به سیل گریه باز</p></div>
<div class="m2"><p>موج را در روی دریا اضطرابی می‌دهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتنه‌ای از هر طرف بیدار می‌گردد ز خواب</p></div>
<div class="m2"><p>چون به یاد چشم مستش تن به خوابی می‌دهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ آسایش برون از دامن هستی نرفت</p></div>
<div class="m2"><p>این کتان را شست و شو در ماهتابی می‌دهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج معنی هر طرف فیّاض می‌گردد روان</p></div>
<div class="m2"><p>چون عنان گفتگو را پیچ و تابی می‌دهم</p></div></div>