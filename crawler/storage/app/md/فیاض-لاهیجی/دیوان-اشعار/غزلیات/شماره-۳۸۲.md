---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>غنچه را دل در بر آن لعل سخنگو بشکند</p></div>
<div class="m2"><p>شعله را از بیمِ خُویش، رنگ بر رو بشکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر برافشاند به تحریک نسیمی طرّه را</p></div>
<div class="m2"><p>یک جهان دل در شکنج هر سر مو بشکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کند در سرمه زیبی میل همچشمی به او</p></div>
<div class="m2"><p>عشق، میل سرمه را در چشم آهو بشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من گرفتم شیشه نه سنگ است و سنگ خاره است</p></div>
<div class="m2"><p>دل که افتاد از خم آن طاق ابرو بشکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قبا بگشاید آن گل پیرهن در صحن باغ</p></div>
<div class="m2"><p>در مشام بلبلان ترسم که گل بو بشکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب در بستر غم تکیه‌ام بر بوریاست</p></div>
<div class="m2"><p>بسکه چون نی استخوانم زیر پهلو بشکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرق عصیان آنقدر گشتم که در بازار حشر</p></div>
<div class="m2"><p>گر به وزن آرند جرم من ترازو بشکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجز خسرو پنجه ز آهن کرد، ترسم عاقبت</p></div>
<div class="m2"><p>کوهکن را زور این سرپنجه بازو بشکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست گویم نیستی فیّاض مرد راه عشق</p></div>
<div class="m2"><p>گر دلت از گفتة من بشکند گو بشکند</p></div></div>