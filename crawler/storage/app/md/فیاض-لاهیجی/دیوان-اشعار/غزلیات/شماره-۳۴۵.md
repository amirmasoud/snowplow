---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>چون خوی دلبران ز عتابم سرشته‌اند</p></div>
<div class="m2"><p>همچون تبسّم از شکرابم سرشته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیخم ولیک شوخی طفلانه می‌کنم</p></div>
<div class="m2"><p>کز رنگ و بوی عهد شبابم سرشته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نالة حزین مرا گوش کرده‌اند</p></div>
<div class="m2"><p>مرغان چراغ زمزمه خاموش کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کبکان ز نازکی سبق جلوة ترا</p></div>
<div class="m2"><p>صد بار خوانده‌اند و فراموش کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اطفال گل چو گوهر شبنم ز نازکی</p></div>
<div class="m2"><p>حرف ترا خریده و در گوش کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبان که گِرد چهره برآورده‌اند خط</p></div>
<div class="m2"><p>این شعله را خوشند که خس‌پوش کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض از بتان نتوان چشم خیر داشت</p></div>
<div class="m2"><p>آیینه را ببین که چه مدهوش کرده‌اند</p></div></div>