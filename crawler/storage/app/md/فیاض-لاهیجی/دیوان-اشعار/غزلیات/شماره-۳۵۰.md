---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>یک بار نکردیم در آن دل اثری چند</p></div>
<div class="m2"><p>شرمندة آزردن آه سحری چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دامن پاکت نبود روز قیامت</p></div>
<div class="m2"><p>چون عذر توان خواست ز دامان تری چند!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسباب جهانگیری عشق است مهیّا</p></div>
<div class="m2"><p>از آتش سودای تو در دل شرری چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونین گله‌ام زان مژه‌ها جوش برآورد</p></div>
<div class="m2"><p>چند از رگ طاقت هوس نیشتری چند!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ننمود اثر چهره و زنگار برآورد</p></div>
<div class="m2"><p>آیینة دل در کف آه سحری چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دل به سوی دیده شد از دیده به دامن</p></div>
<div class="m2"><p>پروردگی‌یی یافت سرشک از سفری چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیّاض به خون مژه افسانة غم را</p></div>
<div class="m2"><p>رفتیم و نوشتیم به دیوار و دری چند</p></div></div>