---
title: >-
    شمارهٔ ۶۸۷
---
# شمارهٔ ۶۸۷

<div class="b" id="bn1"><div class="m1"><p>گمنام گرد و باش فراموش عالمی</p></div>
<div class="m2"><p>بردار بارِ صیت خود از دوش عالمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو نیک و بد همه در دام خود کشید</p></div>
<div class="m2"><p>خوش حلقه کرد زلف تو در گوش عالمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من لب ز شکوة تو فرو بسته‌ام ولی</p></div>
<div class="m2"><p>فریاد می‌کند لب خاموش عالمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالیده‌ای ز حسن به نوعی که تا ابد</p></div>
<div class="m2"><p>تنگست بر امید تو آغوش عالمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم تمام آینه‌دار جمال تست</p></div>
<div class="m2"><p>گشتیم در خیال تو مدهوش عالمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بردیم در هوای تو خود را زیاد خلق</p></div>
<div class="m2"><p>گشتیم در غم تو فراموش عالمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمریست کز خیال لب نازنین تو</p></div>
<div class="m2"><p>نیش است در مذاق دلمن نوش عالمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبی بر آتش همه کس زد سرشک ما</p></div>
<div class="m2"><p>آخر نشاند گریة ما جوش عالمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیّاض فیض خانه بدوشی بس اینکه ما</p></div>
<div class="m2"><p>برداشتیم بار خود از دوش عالمی</p></div></div>