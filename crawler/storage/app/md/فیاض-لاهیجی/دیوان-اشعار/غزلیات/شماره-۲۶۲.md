---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>از بس که هوای دهن تنگ تو دارد</p></div>
<div class="m2"><p>دل در تپش بیخودی آهنگ تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکرنگی من با تو همین منصب دل نیست</p></div>
<div class="m2"><p>هر قطرة خون در تن من رنگ تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سخت‌دلی‌های تو مأیوس نشد دل</p></div>
<div class="m2"><p>این شیشه امید دگر از سنگ تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چشم کسی جا نکنم گر تو برانی</p></div>
<div class="m2"><p>کی صلح کسی چاشنی جنگ تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا حشر دگر مفلسی درد نبیند</p></div>
<div class="m2"><p>فیّاض ز دردی که دل تنگ تو دارد</p></div></div>