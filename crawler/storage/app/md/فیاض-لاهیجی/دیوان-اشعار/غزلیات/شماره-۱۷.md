---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>آتش چکد چو آب ز طرز بیان ما</p></div>
<div class="m2"><p>گویی که شعله‌ایست زبان در دهان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عکس چهره هر دو قدم در دیار عشق</p></div>
<div class="m2"><p>طرح بهار ریخته رنگ خزان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب که داشتیم حدیث رخت نبود</p></div>
<div class="m2"><p>دلسوزتر ز شمع کسی همزبان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خامة شکسته‌نویسان به وصف زلف</p></div>
<div class="m2"><p>حرف درست سر نزند از زبان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز کسی ز ضعف به ما ره نمی‌برد</p></div>
<div class="m2"><p>گاهی ز ناله پرس چو خواهی نشان ما</p></div></div>