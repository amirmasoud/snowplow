---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>سایه زلفت به سر شمشاد سازد شانه را</p></div>
<div class="m2"><p>سبز در آتش کند اقبال خالت دانه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستیم چون بوی گل پنهان نمی‌ماند به کس</p></div>
<div class="m2"><p>من که بر سر همچو شاخ گل زدم پیمانه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال ما از ما چه می‌پرسی که پامال توییم</p></div>
<div class="m2"><p>سیل داند سرگذشتی هست اگر ویرانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وضع دنیا گرنه با انجام باشد غم مدار</p></div>
<div class="m2"><p>خاصه از بهر خرابی ساختند این خانه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چارة غم در محبّت تن به غم دردانست</p></div>
<div class="m2"><p>سوختن آبی بر آتش می‌زند پروانه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به راه افتادم از بیراه شوقم مانده شد</p></div>
<div class="m2"><p>جاده کی زنجیر بر پا می‌نهد دیوانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیش دنیا عاقلان را سخت غافل کرده است</p></div>
<div class="m2"><p>از برای خواب پیدا کرده‌اند افسانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گم نخواهد گشت در خاک این گرامی تخم پاک</p></div>
<div class="m2"><p>سبز خواهد کرد دهقان عاقبت این دانه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در میان کفر و دین بیگانگی فیّاض چیست؟</p></div>
<div class="m2"><p>صلح باید داد با هم کعبه و بتخانه را</p></div></div>