---
title: >-
    شمارهٔ ۶۹۰
---
# شمارهٔ ۶۹۰

<div class="b" id="bn1"><div class="m1"><p>تا به کی چون آتش ای گل خانمانسوزی کنی</p></div>
<div class="m2"><p>چشم نیکو را به خونریزی بدآموزی کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت آنم کو که چون شب با غمت خلوت کنم</p></div>
<div class="m2"><p>همچو شمع آیی به بزم و مجلس افروزی کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم آن دارم که نابینا چو گردم در غمت</p></div>
<div class="m2"><p>سرمة بینائیم ز آن خاکِ در روزی کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک زمان با آهوی چشمت سفارش کن بگو</p></div>
<div class="m2"><p>با ضعیفان شکاری تا به کی یوزی کنی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از وفایت آن طمع دارم که بعد از سوختن</p></div>
<div class="m2"><p>همچو شمعم بر مزار آیی و دلسوزی کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پس از عمری توانم شد به بزم او سفید</p></div>
<div class="m2"><p>ترسم ای طالع در آن ساعت سیه‌روزی کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خوشا فیّاض اقبالی که از یاری بخت</p></div>
<div class="m2"><p>خاک گردی بر در جانان و فیروزی کنی</p></div></div>