---
title: >-
    شمارهٔ ۳۸۱
---
# شمارهٔ ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>مستی ز گرد تفرقه پاکم نمی‌کند</p></div>
<div class="m2"><p>تا غنچه خُسبِ سایة تاکم نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نالة گداخته سر تا به پا پرم</p></div>
<div class="m2"><p>مرهم علاج سینة چاکم نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هفت جوش صبر وجودم سرشته‌اند</p></div>
<div class="m2"><p>خوی زمانه عربده‌ناکم نمی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جرأت نگر که شوکت خصمی چو آسمان</p></div>
<div class="m2"><p>دست آزمای تهمت باکم نمی‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که طالع هندوی من چرا</p></div>
<div class="m2"><p>گوش آشنای نغمة را کم نمی‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>السماسْ‌سوده سودة الماس می‌شود</p></div>
<div class="m2"><p>زان کُشت آسمانم و خاکم نمی‌‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاکستر ار شوم که نگهدار آتشم</p></div>
<div class="m2"><p>دانسته‌ام که عشق هلاکم نمی‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آلایش محیط در امکان عقل نیست</p></div>
<div class="m2"><p>کس این گمان به دامن پاکم نمی‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیّاض مهر زلف بتان سرنوشت ماست</p></div>
<div class="m2"><p>این بخت سایه از سر ما کم نمی‌کند</p></div></div>