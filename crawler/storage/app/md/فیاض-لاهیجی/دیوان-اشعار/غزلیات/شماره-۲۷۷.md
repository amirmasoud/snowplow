---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>امشب که از نم مژه آبم نمی‌برد</p></div>
<div class="m2"><p>در دل خیال کیست که خوابم نمی‌برد!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری است پای در گلم از گریه، چون کنم!</p></div>
<div class="m2"><p>این سیل تندِ خانه خرابم نمی‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ضعف نیست قوّت از خویش رفتنم</p></div>
<div class="m2"><p>وامانده‌ام چنان که شرابم نمی‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راضی شدم به صلح ولی شهد آشتی</p></div>
<div class="m2"><p>از کام تلخی شکرابم نمی‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وز نالة شبانه جگر سوختم چه سود</p></div>
<div class="m2"><p>کان مست ره به بوی کبابم نمی‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لطفم خراب کرده به نوعی که تا ابد</p></div>
<div class="m2"><p>از جا فریب ناز و عتابم نمی‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خضرم که می‌شود! که درین وادی خطر</p></div>
<div class="m2"><p>همراهی درنگ و شتابم نمی‌برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفتة علاقة دستارم آن چنان</p></div>
<div class="m2"><p>کز ره فریب طرف نقابم نمی‌برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیّاض همدمان ز بس از من رمیده‌اند</p></div>
<div class="m2"><p>در آب اگر دهند گم آبم نمی‌برد</p></div></div>