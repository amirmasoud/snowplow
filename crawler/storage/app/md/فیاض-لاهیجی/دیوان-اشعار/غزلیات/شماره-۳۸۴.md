---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>خوبرویان چون نظر بر رخ هم بگشایند</p></div>
<div class="m2"><p>بیم آنست که آیینه ز هم بربایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمندان وی از درد، غمش می‌طلبند</p></div>
<div class="m2"><p>بلبلانش همه از نالة هم میزایند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نو غزالان که به صیّادی خود مغرورند</p></div>
<div class="m2"><p>همه رم کرده به نخجیر گهش می‌آیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطلب کعبه روان کی طلب من باشد</p></div>
<div class="m2"><p>راه دورست که این طایفه می‌پیمایند</p></div></div>