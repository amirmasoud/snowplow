---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>جدا از کوی او شوقم گل و گلشن نمی‌داند</p></div>
<div class="m2"><p>دلم ذوق تپیدن، دیده‌ام دیدن نمی‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیارم گفت حال خویش پنداری درین کشور</p></div>
<div class="m2"><p>کسی درد دل ناگفته فهمیدن نمی‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی در دیده جا دارد، گهی در سینة تنگم</p></div>
<div class="m2"><p>ولی آن خرمن گل جای در دامن نمی‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ای شاخ گل ایمن باش اگر در دامنم باشی</p></div>
<div class="m2"><p>که دستِ خوبه حسرت کرده، گل چیدن نمی‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ادای کنج چشم از من کسی بهتر نمی‌فهمد</p></div>
<div class="m2"><p>زبان گوشة ابرو کسی چون من نمی‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آب دیده خواهد مرد یا در آتش سینه</p></div>
<div class="m2"><p>دل عاشق به مرگ خویشتن مردن نمی‌داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بویی قانعم فیّاض از گلزار وصل او</p></div>
<div class="m2"><p>که این مور از ضعیفی دانه از خرمن نمی‌داند</p></div></div>