---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>چمن بی‌تو فیض هوایی ندارد</p></div>
<div class="m2"><p>دماغ گلستان صفایی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تبسّم ندارد چرا غنچه بر لب</p></div>
<div class="m2"><p>چرا بلبل امشب نوایی ندارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوفه اگر بر کشیدست خود را</p></div>
<div class="m2"><p>که در چشم ما بی‌ تو جایی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شاهی چه لذّت برد پادشاهی</p></div>
<div class="m2"><p>که روی دلی با گدایی ندارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه حظّی توان کرد فیّاض هرگز</p></div>
<div class="m2"><p>ز شعری که حسن ادایی ندارد</p></div></div>