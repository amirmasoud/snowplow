---
title: >-
    شمارهٔ ۴۱۶
---
# شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>مویش وظیفة شب دیجور می‌دهد</p></div>
<div class="m2"><p>رویش چراغ آینه را نور می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخمور را خیال لبش مست می‌کند</p></div>
<div class="m2"><p>عنّاب او نتیجة انگور می‌دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داغم که زخمی نمک آن تبسّم است</p></div>
<div class="m2"><p>طرح نمک به سینة ناسور می‌دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دار سیاست سر میدان عشق تست</p></div>
<div class="m2"><p>نخلی که میوة سر منصور می‌دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فیّاض من ز سودة الماس دیده‌ام</p></div>
<div class="m2"><p>خاصیّتی که مرهم کافور می‌دهد</p></div></div>