---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>یک لحظه که در پیش من آن شوخ نشست</p></div>
<div class="m2"><p>ننشسته، به من فتنه‌گری در پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهری که نداشت در دل از من برداشت</p></div>
<div class="m2"><p>عهدی که نبسته بود با من بشکست</p></div></div>