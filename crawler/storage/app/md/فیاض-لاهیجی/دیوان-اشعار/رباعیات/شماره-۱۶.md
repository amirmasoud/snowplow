---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>فیّاض بیا که عشق بارت دادست</p></div>
<div class="m2"><p>وز فتنة عقل زینهارت دادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردانه بیا از سر هستی بگذر</p></div>
<div class="m2"><p>کاین دجلة پر زور گذارت دادست</p></div></div>