---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>عمری شده تا ز خیل مهجورانیم</p></div>
<div class="m2"><p>نزدیک نشسته‌ایم و از دورانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیننده ز بس کمست، مانند عصا</p></div>
<div class="m2"><p>محتاج به دستگیری کورانیم</p></div></div>