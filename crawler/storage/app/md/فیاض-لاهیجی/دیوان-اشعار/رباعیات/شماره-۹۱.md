---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>ای گل شده بی‌رخ نکویت بدنام</p></div>
<div class="m2"><p>وز شرم لب تو باده گردیده حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی ترا هلال گفتم، مه نو</p></div>
<div class="m2"><p>بالید به خود چنانکه شد ماه تمام</p></div></div>