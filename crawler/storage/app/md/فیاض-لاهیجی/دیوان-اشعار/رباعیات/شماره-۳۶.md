---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>آن مه که به لب چشمة کوثر دارد</p></div>
<div class="m2"><p>وز بادة ناز نشئه در سر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی زمانه پر ز چین می‌گردد</p></div>
<div class="m2"><p>لب از لب خنده‌گر دمی بردارد</p></div></div>