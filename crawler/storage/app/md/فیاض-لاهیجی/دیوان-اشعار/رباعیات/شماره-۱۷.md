---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای شعله ز دست خوی بیدادگرت</p></div>
<div class="m2"><p>می‌سوزم و هیچ نیست از من خبرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چو پروانه چه گردی گردم</p></div>
<div class="m2"><p>می‌گردم قربان سرت گرد سرت</p></div></div>