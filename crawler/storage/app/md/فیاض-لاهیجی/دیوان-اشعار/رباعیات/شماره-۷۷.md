---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>در کشور فضل کرده یزدانت صدر</p></div>
<div class="m2"><p>خورشید برِ تو گه هلال و گه بدر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرتاسر آفاق ترا بنده سزد</p></div>
<div class="m2"><p>افسوس که مانده‌ای تو مجهول‌القدر</p></div></div>