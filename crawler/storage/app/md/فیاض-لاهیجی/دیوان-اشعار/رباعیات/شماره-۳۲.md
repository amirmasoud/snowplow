---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>تا زلف به روی تو پریشان شده است</p></div>
<div class="m2"><p>بر همزن جمعیّت ایمان شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال رخ تو مگر که ابراهیم است</p></div>
<div class="m2"><p>کاتش ز برای او گلستان شده است</p></div></div>