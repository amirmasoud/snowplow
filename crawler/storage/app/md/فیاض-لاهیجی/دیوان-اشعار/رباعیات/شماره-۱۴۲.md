---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>یارب نفس گرم ثنا سنجم ده</p></div>
<div class="m2"><p>وز درد طلب راحت هر رنجم ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خویش تهی کن وز خویشم پر کن</p></div>
<div class="m2"><p>ویرانه کن و در خور آن گنجم ده</p></div></div>