---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>آنانکه رهی به عالم دل دارند</p></div>
<div class="m2"><p>از صحبت جسم پای در گل دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌جاده گر افتند به ره عیب مکن</p></div>
<div class="m2"><p>در گمشدگی رهی به منزل دارند</p></div></div>