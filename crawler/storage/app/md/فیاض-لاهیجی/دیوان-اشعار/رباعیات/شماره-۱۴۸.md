---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>ای آنکه دو لب به نشئة مل داری</p></div>
<div class="m2"><p>شیرین دهنی چون غنچة گل داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوقی و عاشقانه می‌خوانی شعر</p></div>
<div class="m2"><p>با آنکه گلی، زبان بلبل داری</p></div></div>