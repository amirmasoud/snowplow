---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>لطف تو به ما نه این چنین می‌بایست</p></div>
<div class="m2"><p>دشنام تو شیرین‌تر ازین می‌بایست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با روی ترش تبسّمی هم جا داشت</p></div>
<div class="m2"><p>بیمار ترا سکنجبین می‌بایست</p></div></div>