---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>امشب ز غمت روح و روانم می‌سوخت</p></div>
<div class="m2"><p>وز تاب تب جسم تو جانم می‌سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان تب که شب دوش ترا داشت به رنج</p></div>
<div class="m2"><p>مغزی که نداشت استخوانم می‌سوخت</p></div></div>