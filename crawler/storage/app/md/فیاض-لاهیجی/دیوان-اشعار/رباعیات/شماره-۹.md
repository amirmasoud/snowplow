---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>هر چند که دل لبالب از نور خداست</p></div>
<div class="m2"><p>لیکن به مثل قطره کجا بحر کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داند نظری کو به حقیقت بیناست</p></div>
<div class="m2"><p>کابینه اناالعکس اگر گفت؟ خطاست</p></div></div>