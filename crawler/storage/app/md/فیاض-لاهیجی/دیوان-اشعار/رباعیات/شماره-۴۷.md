---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دنیا آن به که خواجه پاکش بخورد</p></div>
<div class="m2"><p>ورنه به فسون دیوِ هلاکش بخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانه بنما گر نخورد خاک زمین</p></div>
<div class="m2"><p>روزی دو سه نگذرد که خاکش بخورد</p></div></div>