---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>در عهد تو حسن را زکاتی نبود</p></div>
<div class="m2"><p>پیمان و وفای را ثباتی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهلست اگر روی ز من گردانی</p></div>
<div class="m2"><p>این هم خالی ز التفاتی نبود</p></div></div>