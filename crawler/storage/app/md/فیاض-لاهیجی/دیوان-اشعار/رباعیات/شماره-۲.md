---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>در وادی عشق پر مکش منّت پا</p></div>
<div class="m2"><p>بی‌گام درین مرحله شو ره پیما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم گویند پای بردار و برو</p></div>
<div class="m2"><p>من می‌گویم که پای بگذار و بیا</p></div></div>