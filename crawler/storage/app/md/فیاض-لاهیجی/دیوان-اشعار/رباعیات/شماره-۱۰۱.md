---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>بینم چو وفا ز بی‌وفایی ترسم</p></div>
<div class="m2"><p>در روز وصاف از جدایی ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم همه از روز جدایی ترسند</p></div>
<div class="m2"><p>جز من که ز روز آشنایی ترسم</p></div></div>