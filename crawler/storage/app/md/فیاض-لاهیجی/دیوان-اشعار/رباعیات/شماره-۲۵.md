---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>آهم ز دل زبانه فرسود نشست</p></div>
<div class="m2"><p>از بوتة خارِ هستیم دود نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانندة خار خشک در گلخن عشق</p></div>
<div class="m2"><p>زودم آتش گرفت و هم زود نشست</p></div></div>