---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>در فکر شبم تا به سحر خواب نبرد</p></div>
<div class="m2"><p>از بی‌خبری که ره به اسباب نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید سببی گر چه سبب‌ساز خداست</p></div>
<div class="m2"><p>بی‌دلو ورسن ز چاه کس آب نبرد</p></div></div>