---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>صد شکر که آن دُر به عدن باز آمد</p></div>
<div class="m2"><p>وان ماه سفر کردة من باز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز مگر روز قیامت برخاست!</p></div>
<div class="m2"><p>کان جان ز تن رفته به تن باز آمد</p></div></div>