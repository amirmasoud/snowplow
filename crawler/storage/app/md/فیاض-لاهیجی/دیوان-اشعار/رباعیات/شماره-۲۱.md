---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>آنم که ز خرّمی دلم را عارست</p></div>
<div class="m2"><p>وز عادت من خوی طرب بیزارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌حاصل از آن شدم که بختم پرورد</p></div>
<div class="m2"><p>نخلی که به سایه پرورد بی‌بارست</p></div></div>