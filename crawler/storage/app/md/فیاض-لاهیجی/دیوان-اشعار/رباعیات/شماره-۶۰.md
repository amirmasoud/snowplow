---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>با خلق جهان غیر نزاعی نبود</p></div>
<div class="m2"><p>روزی نبود که اختراعی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیّاض بساط مهر برچین کامروز</p></div>
<div class="m2"><p>کاسدتر ازین جنس متاعی نبود</p></div></div>