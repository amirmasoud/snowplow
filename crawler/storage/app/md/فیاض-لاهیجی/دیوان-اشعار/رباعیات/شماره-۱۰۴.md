---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>چشم سیه یار نپرسی حالم؟</p></div>
<div class="m2"><p>بیمارم و یک بار نپرسی حالم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیماران حال یکدگر می‌پرسند</p></div>
<div class="m2"><p>ای نرگس بیمار نپرسی حالم؟</p></div></div>