---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>هر کس که چو من سری به دردش دارد</p></div>
<div class="m2"><p>در نالة گرم و آه سردش دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عارضه نیست زردی رنگ رخش</p></div>
<div class="m2"><p>همچشمی آفتاب زردش دارد</p></div></div>