---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>دور افکندن نشانة خواستن است</p></div>
<div class="m2"><p>ویران کردن برای آراستن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پستت کردند تا بلندی طلبی</p></div>
<div class="m2"><p>افتادن دانه بهر برخاستن است</p></div></div>