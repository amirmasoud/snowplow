---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>گر مشکل حشر بر تو مفتوح شود</p></div>
<div class="m2"><p>تو نوح و تن تو کشتی نوح شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز چنین که روح تو تن شده است</p></div>
<div class="m2"><p>فردا چه عجب تن تو گر روح شود!</p></div></div>