---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>هر لحظه دُری نهد در آغوشم چشم</p></div>
<div class="m2"><p>از خون جگر دهد می نوشم چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌نوشد و چشم خون مرا می‌بیند</p></div>
<div class="m2"><p>یعنی که زبد همیشه می‌پوشم چشم</p></div></div>