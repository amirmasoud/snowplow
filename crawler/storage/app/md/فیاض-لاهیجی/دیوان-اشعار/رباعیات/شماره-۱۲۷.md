---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>آن شاخ گل ارچه هست پنهان ز چمن</p></div>
<div class="m2"><p>از فیض وجود اوست گیتی گلشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید اگر چه هست در ابر نهان</p></div>
<div class="m2"><p>از نورویست باز عالم روشن</p></div></div>