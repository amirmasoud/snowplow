---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ای راحت جان غصّه پرورد بیا</p></div>
<div class="m2"><p>وی صیقل خاطر پر از گرد بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد دل خسته‌دلان بی‌خبری</p></div>
<div class="m2"><p>مردیم ز دوری تو بیدرد، بیا</p></div></div>