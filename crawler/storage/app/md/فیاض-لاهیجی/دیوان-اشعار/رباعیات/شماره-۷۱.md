---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>دیروز که آن شکر لب از ما رنجید</p></div>
<div class="m2"><p>می‌کرد زبان عتاب و لب می‌خندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سینة مجروح اسیران بلا</p></div>
<div class="m2"><p>آن می‌زد زخم و این نمک می‌پاشید</p></div></div>