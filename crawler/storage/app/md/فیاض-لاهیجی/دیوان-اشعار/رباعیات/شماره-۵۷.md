---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>آنانکه خدا را به نظر می‌دانند</p></div>
<div class="m2"><p>راهی به مؤثر از اثر می‌دانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعی که قیاس گل گرفتند ز خار</p></div>
<div class="m2"><p>معلوم که از گل چه قدر می‌دانند</p></div></div>