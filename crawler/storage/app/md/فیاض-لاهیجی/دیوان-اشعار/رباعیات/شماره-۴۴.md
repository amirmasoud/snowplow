---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>هر گاه که آن زهره جبین می‌رقصد</p></div>
<div class="m2"><p>از بسکه لطیف و دلنشین می‌رقصد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر نثار سروِ قدش همه را</p></div>
<div class="m2"><p>دل در بر و جان در آستین می‌رقصد</p></div></div>