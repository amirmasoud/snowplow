---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ما خاک وجود خویش را زر نکنیم</p></div>
<div class="m2"><p>خود را با خاک تا برابر نکنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به در دوست وجودی ننهند</p></div>
<div class="m2"><p>تا سر ز گریبان عدم بر نکنیم</p></div></div>