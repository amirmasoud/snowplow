---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>هم گریة من ز چشم مست دگریست</p></div>
<div class="m2"><p>هم خنده ز لعل می‌پرست دگریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>القصّه مرا چو صورت آیینه</p></div>
<div class="m2"><p>هم گریه و هم خنده به دست دگریست</p></div></div>