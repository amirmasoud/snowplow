---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>هر چند نیارم آمدن در بر تو</p></div>
<div class="m2"><p>از ضعف فتاده‌ام به خاک در تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادم که به کام دل توانم گردید</p></div>
<div class="m2"><p>در زیر لب آهسته به گرد سر تو</p></div></div>