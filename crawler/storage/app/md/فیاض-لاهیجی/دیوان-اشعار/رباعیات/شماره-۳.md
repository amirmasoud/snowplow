---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>اصحاب پیمبر ارچه نورند و هُدا</p></div>
<div class="m2"><p>هر یک سوی آخرت رهی‌اند جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیکن شه مردان علی عالیقدر</p></div>
<div class="m2"><p>راهیست که راست می‌رود تا به خدا</p></div></div>