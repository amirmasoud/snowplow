---
title: >-
    شمارهٔ ۲۰ - در منقبت سیدالشهداء امام حسین(ع)
---
# شمارهٔ ۲۰ - در منقبت سیدالشهداء امام حسین(ع)

<div class="b" id="bn1"><div class="m1"><p>به یار نامه نوشتم به خون صبر و سکون</p></div>
<div class="m2"><p>چو غنچه نامه پیچیده ته به ته پر خون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نامه, نامه آراسته بدین تقریر</p></div>
<div class="m2"><p>چه نامه, نامه پیراسته بدین مضمون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که ای ز هجر توام جان اشتیاق به لب</p></div>
<div class="m2"><p>که ای جدا ز توام چشم آرزو پرخون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم که بی‌تو ندارم به جان ثبات و شکیب</p></div>
<div class="m2"><p>منم که بی‌تو ندارم به دل قرار و سکون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تویی که بی‌منی آسوده ظاهر و باطن</p></div>
<div class="m2"><p>تویی که بی‌منی آراسته درون و برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گویمت که چه‌ها می‌کشم ز دست فراق</p></div>
<div class="m2"><p>که کس مباد به دست عدوی خویش زبون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو شاد زی به فراغت که دور از تو مرا</p></div>
<div class="m2"><p>نصیب عشق به صحراکشیده رخت جنون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پی خلاصی من زین طلسم دم بدمم</p></div>
<div class="m2"><p>خیال نرگس چشم تو می‌دمد افسون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان به آب حیات لب تو تشنه لبم</p></div>
<div class="m2"><p>اگرچه سرزده از دیده‌ام دو صد جیحون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خبر ز روز و شبم نیست این قدر دانم</p></div>
<div class="m2"><p>که گاه‌گاه غمم می‌شود ز حد افزون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمند زلف تو در گردن دلست همان</p></div>
<div class="m2"><p>اگر ز پرده هفت آسمان شوم بیرون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گزند ناخن حسرت که کیست لیلی او؟</p></div>
<div class="m2"><p>مرا به هم‌چو نمایند کاین بود مجنون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غم تو زد رهم از آشنا و بیگانه</p></div>
<div class="m2"><p>ز خاک کوی توام نیز راند ذوق جنون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عریضه را به سیاهی نوشته‌ام یعنی</p></div>
<div class="m2"><p>ز دوری تو به سرکار دل, ندارم خون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز خون شکوه چو شد نامه سر بسر لبریز</p></div>
<div class="m2"><p>به یادم آمد ناگه دل وفا افزون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که مانده بود در آن طره دور از بر من</p></div>
<div class="m2"><p>نشسته در خم آن زلف تا کمر در خون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به مقتضای مروت بدین روش کردم</p></div>
<div class="m2"><p>کنار نامه به احوال پرسیش مشحون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که این فریفته دام طره مفتون</p></div>
<div class="m2"><p>بگو در آن خم زلف سیاه چونی چون؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا به خواب نصیب است روی او دیدن</p></div>
<div class="m2"><p>ولی به خواب نیم دست‌رس ز بخت زبون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو چون به حلقه آن زلف دست‌رس داری</p></div>
<div class="m2"><p>ترا که در خم آن زلفی از ازل مفتون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان‌که معنی پیچیده در شکنجه حرف</p></div>
<div class="m2"><p>چنان‌که در شکن لفظ نارسا مضمون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به یاد چشم به خون جگر تپیده من</p></div>
<div class="m2"><p>روا بود که ببینی به آن رخ گلگون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو تیره غمزه خوری یاد کن ز سینه من</p></div>
<div class="m2"><p>که جز به یاد تو من هم نمی‌خورم دم خون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگرچه جای من آنجا پرست از اغیار</p></div>
<div class="m2"><p>ولیک جای تو خالی به سینه محزون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به دست باد صبا گشت نامه چون مرسل</p></div>
<div class="m2"><p>چو خاک‌بوس درش کرد ایستاد برون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن به پیش درش در برون توقف کرد</p></div>
<div class="m2"><p>که باد را به حریمش نبود راه درون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرفت دست ادب نامه را به پیش آورد</p></div>
<div class="m2"><p>ستد ز دست وی و کرد لطف را ممنون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به دست ناز چو بگشود نامه را از هم</p></div>
<div class="m2"><p>چه دید نامه درهم, چو طره مفتون</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو زخم تازه‌شکن بر شکن ز خون لبریز</p></div>
<div class="m2"><p>ز حرف حرف چو دریای موج‌زن از خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به طنز گفت به خط کسی نمی‌ماند</p></div>
<div class="m2"><p>مگر که کاتب او طفل بوده یا مجنون!</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>معانیش همه بی‌ربط‌تر ز حرف وفا</p></div>
<div class="m2"><p>عبارتش همه درهم‌تر از حدیث جنون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه روشناس نظر نه به حرف دل نزدیک</p></div>
<div class="m2"><p>نه لفظ اوست به طبع آشنا و نه مضمون</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مگر مسوده زلف چین به چین بتی است!</p></div>
<div class="m2"><p>مگر به حال دل بی‌دلی است این مشحون!</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به پیش زلف خود افکند و گفت از سر ناز</p></div>
<div class="m2"><p>سواد این چو تو داری تو فهم کن مضمون</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو زلف دید خط آشنا به خود پیچید</p></div>
<div class="m2"><p>فکند عقده سررشته را به دست ظنون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پس از تأمل بسیار گفت این ز کسی است</p></div>
<div class="m2"><p>که رفته از بر ما چون ز خویش صبر و سکون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کرا دماغ که بنویسد از طریق وفا</p></div>
<div class="m2"><p>جواب و, پرسد کش حال چیست, واقعه چون؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پیام داد زبانی ولی نهفته ز ناز</p></div>
<div class="m2"><p>که ای ز دوری ما همچو بخت خویش زبون</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چرا به دست جدایی چنین زبون شده‌ای؟</p></div>
<div class="m2"><p>نگفتمت که ندارد فراق یار شگون؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز خاک درگه ما دوریت مناسب نیست</p></div>
<div class="m2"><p>به سعی کوش که از چنگ غم شوی بیرون</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر نباشدت آسان خلاصی از هجران</p></div>
<div class="m2"><p>مدد طلب ز شهنشاه عرصه مسکون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شهنشهی که به حصر مدایحش نرسد</p></div>
<div class="m2"><p>خرد اگر متفنن شود به جمع فنون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شهی که از شرف او رنگ پادشاهی او</p></div>
<div class="m2"><p>ز پای پای نهادست بر سر گردون</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شهی که قامت جاهش خمیده درناید</p></div>
<div class="m2"><p>به سعی تا به ابد زیر این رواق نگون</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>امام مفترض‌الطاعه شاهزاده حسین</p></div>
<div class="m2"><p>به ناز داشتهٔ لطف ایزد بی‌چون</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عجب که گوشه دامن به دست حصر دهد</p></div>
<div class="m2"><p>فضایلش که ز قید شمار جسته برون</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>جهات ست همه دریا و ذات او گوهر</p></div>
<div class="m2"><p>نه آسمان همه طومار و ذات او مضمون</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگرنه رابط ایجاد او شدی بودی</p></div>
<div class="m2"><p>ازل خزینه کاف و ابد طویله نون</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>برای مولد او کرد آسمان حرکت</p></div>
<div class="m2"><p>زمین برای مقر وی اقتضای سکون</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بود چو روزنه دیده پیش قصر جلال</p></div>
<div class="m2"><p>به پیش خلوت قدسش سراچه گردون</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به فرض ا گر کره نه سپهر پهن کنند</p></div>
<div class="m2"><p>به پیش ساحت قدرش پلی است برهامون</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اگرنه رایت او سرفراشتی به فلک</p></div>
<div class="m2"><p>که داشتی نگه این کهنه سقف را چو ستون؟</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به خون نشست به اندک مخالفت ز شفق</p></div>
<div class="m2"><p>نبود بر فلک دون خلاف او میمون</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>غلط سرودم این نکته را خطا کردم</p></div>
<div class="m2"><p>سزای او نبود گفت‌وگو برین قانون</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زمانه کیست که با او درافتدی به گزاف؟</p></div>
<div class="m2"><p>سپهر کیست که با او درآیدی به فسون؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خلافش ار گذرد آفتاب را به ضمیر</p></div>
<div class="m2"><p>چو داغ لاله سیه‌رو نشیند اندر خون</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خلاف او نرود دهر در شهور و سنین</p></div>
<div class="m2"><p>جز امر او نکند چرخ در دهور و قرون</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نبود رغبتی او را بدین دو روزه حیات</p></div>
<div class="m2"><p>نبود الفتی او را بدین سراچه دون</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وگرنه گرد ازل از ابد برآوردی</p></div>
<div class="m2"><p>رضای او به جهانگیری ار شدی مقرون</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ارادتش سر پرگار اگر بجنباند</p></div>
<div class="m2"><p>جهان به حیطه درآرد چو نون و نقطه نون</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز کلک امرش اگر نقطه‌ای فرو ریزد</p></div>
<div class="m2"><p>سپهر غرقه شود همچو قطره در جیحون</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گره به گوشه ابروی نهی اگر فکند</p></div>
<div class="m2"><p>سپهر را حرکت منتقل شود به سکون</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ابد کند سر خود را به در ز جیب ازل</p></div>
<div class="m2"><p>پی نظاره جاهش که خیمه برده برون</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>برای قصر جلالش کشد مصالح کار</p></div>
<div class="m2"><p>قضا که خشت مه و مهر بسته بر گردون</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>در آستانه او پست, آسمان بلند</p></div>
<div class="m2"><p>به ساکنان درش تنگ, عرصه مسکون</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به دهر رایت اقبال او نمی‌گنجد</p></div>
<div class="m2"><p>چنان که در دل تنگ آه عاشق محزون</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شکوه جاه و جلاش کسی چه سان بیند؟</p></div>
<div class="m2"><p>که در شکنجه این شش جهت بود مسجون</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>فکنده سایه به ماضی و حال و استقبال</p></div>
<div class="m2"><p>رسانده پایه به من کان و کاین و سیکون</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ز حارسان شکوه وی است اسکندر</p></div>
<div class="m2"><p>ز راویان علوم وی است افلاطون</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به کفه خردش عقل عاقلان جهان</p></div>
<div class="m2"><p>چو پیش عاقله کم سنگ ترهات جنون</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نسیم لطف خوشش را طباع آب بقا</p></div>
<div class="m2"><p>زبانه غضبش را طبیعت طاعون</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>به لاتناهی اعداد نیز نتوان کرد</p></div>
<div class="m2"><p>شمار فضلش کز لاتناهی است فزون</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عدد نگشت کمال محیطش ارچه نگشت</p></div>
<div class="m2"><p>نهایتش چو کمالات او به پیرامون</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>که لاتناهی اعداد هست بالقوه</p></div>
<div class="m2"><p>ز ننگ قوه بود لاتناهی‌اش بیرون</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>طلوع مطلع مدحش چه دلگشاست کنون</p></div>
<div class="m2"><p>هزار خنده به خورشید می‌زند گردون</p></div></div>