---
title: >-
    شمارهٔ ۲۷ - قصیده موسوم به معجزه‌الشوق در منقبت امام رضا(ع)
---
# شمارهٔ ۲۷ - قصیده موسوم به معجزه‌الشوق در منقبت امام رضا(ع)

<div class="b" id="bn1"><div class="m1"><p>محیط عشق که ما مرکزیم و غم پرگار</p></div>
<div class="m2"><p>درین میانه ز عالم گرفته‌ایم کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنون عشق برآراست خوش به سامانم</p></div>
<div class="m2"><p>کجاست عقل که گل چیند اندرین گلزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتاده خوش به سر هم متاع رسوایی</p></div>
<div class="m2"><p>خرد کجاست که سودا کند درین بازار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داغ عشق برآرای پای تا سر خویش</p></div>
<div class="m2"><p>که زیب سکه کند نقد را تمام عیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریب عشوه دنیا مخور که آینه را</p></div>
<div class="m2"><p>به‌رنگ سبز کند جلوه در نظر زنگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زهد غره بود زاهد و نمی‌داند</p></div>
<div class="m2"><p>که تار سبحه به تدریج می‌شود زنار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تن لباس درم ز آرزوی عریانی</p></div>
<div class="m2"><p>که بند پاست درین راه بر سرم دستار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه راز بپوشم که همچو غنچه گل</p></div>
<div class="m2"><p>نقاب خود به خود افتد مرا ز چهره کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان حکایت من تشنه شنیدن‌هاست</p></div>
<div class="m2"><p>که باز می‌شود از شوق خود به خود طومار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب ولایت امنی است ملک رسوایی</p></div>
<div class="m2"><p>که هیچ‌گونه کسی با کسی ندارد کار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو حاضری و به روی تو دیده نگشایم</p></div>
<div class="m2"><p>که بی‌رخ تو فراموش کرده‌ام دیدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر به منع من ای عقل دردسر کم کش</p></div>
<div class="m2"><p>که من مجادله با خویش کرده‌ام بسیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کنون که ذوق جنون ریشه کرد در دل من</p></div>
<div class="m2"><p>دگر نمی‌شود این نخل کنده از بن و بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون که دامن من پر ز سنگ طفلانست</p></div>
<div class="m2"><p>خرد به راهم از آیینه می‌کشد دیوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون که کرده مجردترم ز نور نظر</p></div>
<div class="m2"><p>فلک کشیده به گردم ز آبگینه حصار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر چه دفع ملامت کند نهفتن عشق</p></div>
<div class="m2"><p>مرا کنون که برافتاده پرده از رخ کار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه پردگی کندم تار عنکبوت آخر</p></div>
<div class="m2"><p>خرد چه هرزه به من می‌تند عناکب‌وار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا کنون که نمودند راه عالم غیب</p></div>
<div class="m2"><p>درین ستمکده دیگر نه ممکن است قرار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه‌سان نگاه تواند به دیده کرد درنگ!</p></div>
<div class="m2"><p>دمی که پرده برافتد ز پیش چهره یار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو عشق جای کند در طبیعتی هرگز</p></div>
<div class="m2"><p>به حیله‌ها نتوان کرد منعش از اظهار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چراغ تا که نیفروختند در فانوس</p></div>
<div class="m2"><p>ز خلق چهره تواند نهفت در شب تار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ظهور عشق به اظهار نیست حاجتمند</p></div>
<div class="m2"><p>که ظاهر است وجود مؤثر از آثار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به عهد عشق مجویید اثر ز هستی من</p></div>
<div class="m2"><p>که آفتاب درآید به سایه دیوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بهار شد چمنم از نسیم گلشن عشق</p></div>
<div class="m2"><p>چو گل شکفته‌ام اکنون درین خجسته بهار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کنون که هم گل و هم بلبلم درین گلشن</p></div>
<div class="m2"><p>کنم به وصف بهار خود این غزل تکرار</p></div></div>