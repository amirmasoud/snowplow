---
title: >-
    شمارهٔ ۱۷ - در منقبت حضرت فاطمه زهرا(س)
---
# شمارهٔ ۱۷ - در منقبت حضرت فاطمه زهرا(س)

<div class="b" id="bn1"><div class="m1"><p>چنان به صحن چمن شد نسیم روح‌افزار</p></div>
<div class="m2"><p>که دم ز معجز عیسی زند نسیم صبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رطوبتی است چمن را چنان ز سبزه و گل</p></div>
<div class="m2"><p>که گر بیفشریش آب می‌چکد ز هوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس هوا طرب‌انگیز شد به صحن چمن</p></div>
<div class="m2"><p>ز ذوق غنچه نمی‌گنجد اندرون قبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان که نامیه را فیض عام شد شاید</p></div>
<div class="m2"><p>که آرزو به مطالب رسد به نشو و نما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نشو سبزه زمان گر نمو کند شاید</p></div>
<div class="m2"><p>که بی‌میانجی امروز دی شود فردا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سعی نشو و نما پرعجب مدان که شود</p></div>
<div class="m2"><p>نهال حسرت عاشق به میوه کام‌روا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فیض بخشی نشو و نما عجب نبود</p></div>
<div class="m2"><p>رسد به بار اجابت اگر نهال دعا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین که قامت خوبان نمو کند در حسن</p></div>
<div class="m2"><p>بلند چون نشود نخل حسرت دل‌ها؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوای قامت شمشادقامتان دارد</p></div>
<div class="m2"><p>نهال سرو که در باغ می‌شود رعنا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهال سرو چمن سر به ابر می‌ساید</p></div>
<div class="m2"><p>ز بس گرفته ز فیض بهار نشو و نما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شود در آب سخن سبز همچو نی در آب</p></div>
<div class="m2"><p>چو سر کنم قلم از بهر وصف آب و هوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس که عیش فزا گشته موج‌های نسیم</p></div>
<div class="m2"><p>کند به کشتی غم کار موجه بر دریا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صبا کند دهن غنچه پر زر از تحسین</p></div>
<div class="m2"><p>چو گردد از پی وصف هوا نفس پیرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به شاخ تا دم بادی وزیده گشته ز ذوق</p></div>
<div class="m2"><p>به وصف آب و هوا برگ برگ نغمه‌سرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه‌گونه مرغ نشیند خمش که فیض نسیم</p></div>
<div class="m2"><p>زبان سوسن خاموش را کند گویا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه‌سان ز جلوه پرواز بلبل استد باز</p></div>
<div class="m2"><p>کنون که صورت دیبا پرد به بال صبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>توان ز فیض سبکروحی نسیم چمن</p></div>
<div class="m2"><p>پرید بی‌مدد بال و پر چو رنگ حنا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هوا ز بس که رطوبت گرفته نیست عجب</p></div>
<div class="m2"><p>که کار آب کند با صحیفه موج هوا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو موج بحر بر آبست موجه سوهان</p></div>
<div class="m2"><p>ز بس که آب گرفتند از هوا اشیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز فیض عام طراوت چنان تری شده عام</p></div>
<div class="m2"><p>که زهد نیز نماندست خشک در دنیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میان سبزه تواند نهان شدن آتش</p></div>
<div class="m2"><p>ز اعتدال طبیعت چو باده در مینا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون که سبزه برآمد ز سنگ هست امید</p></div>
<div class="m2"><p>که سبز در دل خوبان کنیم تخم وفا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیا ببین که در احیای مردگان نبات</p></div>
<div class="m2"><p>نیابت دم عیسی کند نسیم صبا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عموم یافت ز بس اعتدال ممکن گشت</p></div>
<div class="m2"><p>دم ریا شود ار معتدل ولی به ریا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>میان ابر سیه آفتاب پنهانست</p></div>
<div class="m2"><p>چو زیر طره شبرنگ چهره زیبا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز ازدحام سحاب فضای عالم کون</p></div>
<div class="m2"><p>ز بس که راه نیابد به روی ارض ضیا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ره نزول کند گم ز تیرگی باران</p></div>
<div class="m2"><p>از آن فروزد هردم چراغ برق هوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپاه ابر به روی هواست چون ظلمات</p></div>
<div class="m2"><p>درو نهان شده باران به‌سان آب بقا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز بس که متصل آید ز قطره رسم شود</p></div>
<div class="m2"><p>هزار دایره بر سطح آب در یک جا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شدست قوس قزح چون کمان حلاجی</p></div>
<div class="m2"><p>که پنبه می‌زند از ابر و می دهد به هوا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به باغ شاخ گل امروز نایب موسی است</p></div>
<div class="m2"><p>کز آستین خود آرد برون ید بیضا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به وصف آب و هوا چون شوم صحیفه‌نگار</p></div>
<div class="m2"><p>هزار غنچه معنی شود شکفته مرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رسید تا به زبانم شکفته می‌گردد</p></div>
<div class="m2"><p>به آب و تاب کنم چون حدیث غنچه ادا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به سینه غنچه پیکان شکفته جا گیرد</p></div>
<div class="m2"><p>درین هوا چو خدنگی شود ز شست رها</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دهر غنچه نشکفته غنچه دل ماست</p></div>
<div class="m2"><p>وگرنه نامی ماندی ز غنچه چون عنقا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شهاب نیست به شب کز وفور فیض به ارض</p></div>
<div class="m2"><p>ستاره از فلک آید برای کسب هوا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان که روح‌فزا گشته است پنداری</p></div>
<div class="m2"><p>هوا شمیم گرفته ز تربت زهرا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه تربتی که بود آبروی گوهر دین</p></div>
<div class="m2"><p>چه تربتی که بود نور چشم نور و ضیا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه تربتی که رسد گر غبار او به فلک</p></div>
<div class="m2"><p>هزار جان گرامی کند به نقد فدا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه تربتی که بود ننگش از گران‌قدری</p></div>
<div class="m2"><p>عبیر جیب و بغل گر نمایدش حورا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خجسته تربت پاکی که گوهر عصمت</p></div>
<div class="m2"><p>درو گرفته چو دُر در دل صدف مأوا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نهال گلشن عصمت گل حدیقه دین</p></div>
<div class="m2"><p>سرور سینه بی‌کینه رسول خدا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گرانبها صدف گوهر حسین و حسن</p></div>
<div class="m2"><p>قیاس منتج قدر ائمه والا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نتیجه‌ای که ز انتاج قدر او زادند</p></div>
<div class="m2"><p>نتایج کرم و علم و عدل و جود و سخا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پی نتایج احدی عشر ز روی شرف</p></div>
<div class="m2"><p>علی مقدمه کبریست و او صغرا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زهی جلالت قدری که زاده نسبش</p></div>
<div class="m2"><p>بزرگ ملت و دین است تا به روز جزا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سیادت از شرف اوست نور چشم نسب</p></div>
<div class="m2"><p>شرافت از نسب اوست تاج عز و علا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز بندگان وفایش چه ساره چه هاجر</p></div>
<div class="m2"><p>ز دایگان سرایش چه مریم و حوا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فتان به خاک درش صدهزار حوراوش</p></div>
<div class="m2"><p>دوان به گرد سرش صدهزار آسیه‌سا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کنیزی حرمش آرزوی بانوی مصر</p></div>
<div class="m2"><p>گدایی درش امید پادشاه سبا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>که بود جز وی بنت‌الرسول والبضعه</p></div>
<div class="m2"><p>کرا جز اوست لقب‌البتول والعذرا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هنوز طینت حوا نگشته بود خمیر</p></div>
<div class="m2"><p>که بود نامزدش گشته سروری نسا</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بود رعایت عصمت به ذات پاکش ختم</p></div>
<div class="m2"><p>چنان که ختم نبوت به خواجه دو سرا</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به خود سپهر چه مقدار ازین هوس بالید</p></div>
<div class="m2"><p>که گردد از پی جاهش کمینه پرده‌سرا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ولیک غافل ازین در طریق عقل و قیاس</p></div>
<div class="m2"><p>که در حساب چه مقدار گنجد از دریا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مقرنس فلکش پایه‌ای ز قصر جلال</p></div>
<div class="m2"><p>مسدس جهتش عرصه‌ای ز صحن سرا</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فضای عالم قدرش اگر بپیمایند</p></div>
<div class="m2"><p>مساحتش نکند وهم لامکان پیدا</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>عروس کنه جلالش نقاب نگشاید</p></div>
<div class="m2"><p>مگر به حجله علم خدای بی‌همتا</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به وهم عرصه قدرش نمی‌توان پیمود</p></div>
<div class="m2"><p>محیط را نتوان کرد طی به زور شنا</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کنند طول زمان حلقه حلقه گر چو کمند</p></div>
<div class="m2"><p>به اوج قصر جلالش هنوز نیست رسا</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>محیط عرصه قدرش نمی‌تواند شد</p></div>
<div class="m2"><p>زمان اگر سر خود را گره کند برپا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درین سخن سر مویی نه جای اغراقست</p></div>
<div class="m2"><p>مجردات برونند از دی و فردا</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هم این زمان طویل و هم این مکان عریض</p></div>
<div class="m2"><p>نظر به عالم قدس است ذره در صحرا</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به چشم ظاهر، قدرش نمی‌توان دیدن</p></div>
<div class="m2"><p>نگاه ظاهریان از کجا و او ز کجا</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به چشم ظاهر اگر هم نظر کنی بینی</p></div>
<div class="m2"><p>که رفته قدرش از هرچه هست بر بالا</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طهارت نسب او را سلامی از آدم</p></div>
<div class="m2"><p>جلالت حسب او را پیامی از حوا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شرافتش به ازل بوده همعنان قدم</p></div>
<div class="m2"><p>جلالتش به ابد رفته هم رکاب بقا</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به شیر پرورشش دایگی نموده قدر</p></div>
<div class="m2"><p>به حجر تربیتش یاوری نموده قضا</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>اگر به حکم خود اینجاش غصب حق ظالم</p></div>
<div class="m2"><p>کند, چه می‌کند آنجا که حاکم است خدا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگرچه ایذی او سهل داشت زین چه کند</p></div>
<div class="m2"><p>که کرده است خدا و رسول را ایذا</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بزرگوارا آنی که وصف رتبه تو</p></div>
<div class="m2"><p>به جبرئیل و خدا و فرشته است سزا</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>مرا چه حد که کنم وصف رتبه شأنت</p></div>
<div class="m2"><p>مرا چه حد که شوم درخور تو مدح‌سرا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تویی که مدح تو کرده خدای عزوجل</p></div>
<div class="m2"><p>تویی که وصف ترا کرده خواجه دو سرا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نقاب قدر تو بگشوده «بضعه منی»</p></div>
<div class="m2"><p>علوشان تو بنموده از «من اذاها»</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چه حاجتست به تعریف رتبه‌ات که بسی است</p></div>
<div class="m2"><p>علوشان ترا رتبه ائمه گوا</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ز خدمت تو بود جبرائیل منت‌دار</p></div>
<div class="m2"><p>به دایگی تو گرم شتاب لطف خدا</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به چاکری درت آسمان مرادطلب</p></div>
<div class="m2"><p>به خاک‌روبی تو آفتاب کام‌روا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>فلک به راه وفاق تو می‌رود شب و روز</p></div>
<div class="m2"><p>از آن پرآبله باشد همیشه‌‌اش کف پا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>غبار درگهت آرایش نسیم بهار</p></div>
<div class="m2"><p>ز گرد بارگهت آب‌روی باد صبا</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>به رتبه تو تواند فلک تشبه کرد</p></div>
<div class="m2"><p>پرد به بال و پر آفتاب اگر حربا</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>من و مناسبت خدمتت, زهی امید!</p></div>
<div class="m2"><p>من و موافقت طاعتت خجسته رجا!</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>مرا توقع لطفت بس است حسن عمل</p></div>
<div class="m2"><p>مرا توجه فضلت بس است خیر جزا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زمن نه درخور شأن تو خدمتی لایق</p></div>
<div class="m2"><p>زمن نه در حق جاهت ستایشی بسزا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>من و ستایش فضل تو دعویی است خلاف</p></div>
<div class="m2"><p>من و سرودن مدحت مظنه‌ایست خطا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مرا ز دعوی مدحت نه غیر ازین مطلب</p></div>
<div class="m2"><p>مرا ز لاف مدیحت نه هیچ کام الا</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>که معترف به جلال توأم زهی توفیق</p></div>
<div class="m2"><p>که معرفت به توام حاصل است شکر خدا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>همیشه تا که ز لطف و ز قهر در عالم</p></div>
<div class="m2"><p>معززند احبا مذللند اعدا</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>عزیز لطف تو بادا چو دوستان فیاض</p></div>
<div class="m2"><p>ذیل قهر تو اعدا همیشه در همه‌جا</p></div></div>