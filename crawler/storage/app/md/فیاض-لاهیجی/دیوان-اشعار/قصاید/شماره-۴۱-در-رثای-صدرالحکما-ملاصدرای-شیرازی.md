---
title: >-
    شمارهٔ ۴۱ - در رثای صدرالحکما ملاصدرای شیرازی
---
# شمارهٔ ۴۱ - در رثای صدرالحکما ملاصدرای شیرازی

<div class="b" id="bn1"><div class="m1"><p>زین هفت‌خوان که پایه او بر سر فناست</p></div>
<div class="m2"><p>در شش جهت به هر چه نظر می‌کنی خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره آن دلی که کند تکیه بر سپهر</p></div>
<div class="m2"><p>سرگشته آن سری که به بالین آسیاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظن ثبات دعوی راحت درین جهان</p></div>
<div class="m2"><p>تفسیر هر دو آیه سیمرغ و کیمیاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بر لباس عاریت زندگی منه</p></div>
<div class="m2"><p>کاین جامه تارش از عدم و پودش از فناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقلی ز کاسه سر فغفور می‌کند</p></div>
<div class="m2"><p>گوشت اگر ز کاسه چینی پر از صداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌علتی نبوده جهان هیچ‌گه ولی</p></div>
<div class="m2"><p>زین پیش، پرده داشته امروز برملاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز چون به‌دی و پریوش حسد بود</p></div>
<div class="m2"><p>فردا چه‌گونه باشد، کش روی برقفاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمعیت است ساخته اضداد را به هم</p></div>
<div class="m2"><p>بر اختلاط ساخته، دلبستگی خطاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیرم منافقانه بهم گرم الفتند</p></div>
<div class="m2"><p>چون دست یافتند بهم، خونشان هباست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان در چهار بالش امکان چه خفته‌ای؟</p></div>
<div class="m2"><p>برخیز کز برای تو افلاک متکاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هماشیان زاغ و زغن چون شود به طبع!</p></div>
<div class="m2"><p>آن همتی که بر پر عنقاش نکته‌هاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیمرغ قاف را نکشد دل به سنگلاخ</p></div>
<div class="m2"><p>کی استخوان زاغ و زغن طعمه هماست!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواهی بمیر تا که شوی زنده ابد</p></div>
<div class="m2"><p>آب بقا برای تو در کاسه فناست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ابر، تیره روز تو و روزگار تو</p></div>
<div class="m2"><p>باران گریه سرکن اگر میلت انجلاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون برق در مشیمه این تیره‌فام ابر</p></div>
<div class="m2"><p>عمرت به خنده می‌رود و حاصلت بکاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در سینه دل مقید صید مگس مکن</p></div>
<div class="m2"><p>عنقا هم آشیانه درین آشیان تراست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فرخنده طایریست دلت، طعمه معرفت</p></div>
<div class="m2"><p>گر استخوان جهل کنی طعمه‌ات، خطاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برخاربست تن به حقارت نظر مکن</p></div>
<div class="m2"><p>کاین بوته خزان‌زده مستوکر هماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همچون زنان فریفته رسم و عادتی</p></div>
<div class="m2"><p>ای چادر رسوم به سر، مردیت کجاست؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درخورد لاف همت مردانه نیستی</p></div>
<div class="m2"><p>طفلی هنوز و این لبن عادتت غذاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برخویش فرض روزه مریم نکرده چون</p></div>
<div class="m2"><p>دعوی کنی که بکردم من مسیح‌زاست!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوتاه می‌کنند چو دست تو بی‌گمان</p></div>
<div class="m2"><p>خود دست اگر ز مایده برداشتی رواست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>موت ارادتی است ترا آب زندگی</p></div>
<div class="m2"><p>مت قبل ان تموت باین چشمه رهنماست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مت بالاراده جان من امروز ازین امل</p></div>
<div class="m2"><p>گرتحیی بالطبیعه ترا مایه رجاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مردن درین سراچه فانی به کام دل</p></div>
<div class="m2"><p>تاریخ مولد تو به عشرتگه بقاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر این لباس عاریت از تن برون کنی</p></div>
<div class="m2"><p>در زیر آن برای تو آماده حله‌هاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در دوزخ طبیعت اگر سوختی کنون</p></div>
<div class="m2"><p>فردا بهشت نقد ترا در کف رضاست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ور زانکه مر عوارض طبعت بود بهشت</p></div>
<div class="m2"><p>در دوزخی چو بر عرضت دست نارساست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون نفس را گزیر نباشد ز دوزخی</p></div>
<div class="m2"><p>اینجا کن اختیار که هم زودش انقضاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بی‌زحمت گداز طلا را خلاص نیست</p></div>
<div class="m2"><p>جور طبیعت آتش و نفس تو چون طلاست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دانسته مرد دین ستم چرخ می‌کشد</p></div>
<div class="m2"><p>هرچند حکم او به سر آسمان رواست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دنیاست پشت آینه عقباست روی آن</p></div>
<div class="m2"><p>این را همه کدورت و آن را همه صفاست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آیینه گر به عمد کند تیره پشت وی</p></div>
<div class="m2"><p>کاین تیرگی پشت بر او مایه جلاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواهی اگر به فرض که پشتش چو رو کنی</p></div>
<div class="m2"><p>آن پشت رو نگردد و آن روی خود قفاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دنیا و آخرت ز چه کس را نمی‌دهند</p></div>
<div class="m2"><p>گر وهم کرده‌ای که ز بخل است، این خطاست!</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شب را به روز جمع نکردست هیچ‌کس</p></div>
<div class="m2"><p>جمع ظلام و نور نیاید به فعل راست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>معقول را تصور محسوس کرده‌ای</p></div>
<div class="m2"><p>این پنبه‌ات به گوش دل از غایت شفاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دل‌داده‌ای به ارض طبیعت، خطاست این</p></div>
<div class="m2"><p>اجسام اخروی همه از جوهر سماست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>راضی شدم به جور طبیعت به زور عقل</p></div>
<div class="m2"><p>کاین باعث نفور ارادت ازین دغاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مارت اگر به جان نرساند گزند نیش</p></div>
<div class="m2"><p>لون مصورش نه سزاوار احتماست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون خوب در روی فرحش حاصل غمست</p></div>
<div class="m2"><p>چون نیک بنگری سقمش مایه شقاست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا تنگ‌تر کند به دلم تنگنای دهر</p></div>
<div class="m2"><p>جور زمانه را به نظر قدر کیمیاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر زخم دل گسل ز سنان ستاره‌ام</p></div>
<div class="m2"><p>در سینه همچو روزن امید دلگشاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قطعی به غیر قطع تعلق نمی‌کند</p></div>
<div class="m2"><p>تیغم ز آفتاب به سر شهپر هماست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا دیده بر حقارت دنیا شود بصیر</p></div>
<div class="m2"><p>در چشم من کدورت ایام توتیاست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سنگی به تازه دست سپهرم به شیشه زد</p></div>
<div class="m2"><p>کز وی تمام روی زمین شیشه پاره‌هاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دل بارها شکست مرا از فلک ولی</p></div>
<div class="m2"><p>این دل شکستنی‌ست که سربار بارهاست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برجانم از مصیبت استاد من رسید</p></div>
<div class="m2"><p>دردی که بر دل علی از فقد مصطفاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خالی نبودم ارچه دمی از مصیبتی</p></div>
<div class="m2"><p>اینها جدا و این غم دندان‌شکن جداست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>استاد من که هم اب و هم رب معنوی است</p></div>
<div class="m2"><p>تا حشر اگر پرستش خاکش کنم رواست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>استاد کیمیاگر من آنکه تا ابد</p></div>
<div class="m2"><p>بر صنع کیمیاگریش طبع من گواست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>طبعم که خاک تیره جهل و غرور بود</p></div>
<div class="m2"><p>از صنع کیمیاگریش این زمان طلاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از چاه ذل رساند به معراج عزتم</p></div>
<div class="m2"><p>اقبال او که بر سر من سایه هماست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>صدر جهان و عالم احسان و عقل کل</p></div>
<div class="m2"><p>کز وی کمال را شرف و فضل را بهاست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مشکات عقل را به هنر فکر اوست زیب</p></div>
<div class="m2"><p>مصباح شرع را به مثل علم او ضیاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ارباب فکر را نظر مستقیم او</p></div>
<div class="m2"><p>تا روز حشر در کف اندیشه‌ها عصاست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آیینه‌های باطن اهل شهود را</p></div>
<div class="m2"><p>در مشهد تجلی او صیقل جلاست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در خلوت مشاهده افلاطن بهوش</p></div>
<div class="m2"><p>در مجمع مناظره رسطوی تیزراست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مشاییان پیاده و او در میان سوار</p></div>
<div class="m2"><p>اشراقیان فتاده و او در میان به‌پاست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خرمن‌کشان فلسفه گردند گرد او</p></div>
<div class="m2"><p>زیرا که ذات او به مثل قطب این رحاست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بی‌او درین زمانه چنانم که فی‌المثل</p></div>
<div class="m2"><p>کام نهنگ بر دلم این نیلگون فضاست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آتش همی فشاندم این آسمان به سر</p></div>
<div class="m2"><p>چشم ستاره بر سر من چشم اژدهاست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جسم شکسته را دم شمشیر بستر است</p></div>
<div class="m2"><p>پهلوی خسته را ز دم شیر متکاست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تا رخت بسته است ازین تیره گون‌سرا</p></div>
<div class="m2"><p>تا آن سراش تکیه‌گه پهلوی بقاست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>طفل رضیع میل به پستان چه سان کند</p></div>
<div class="m2"><p>میلم هزار مرتبه افزون بدان سراست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>او بود جان و من به مثل قالبی ازو</p></div>
<div class="m2"><p>رفتست جان و قالب بی‌جان همی بجاست</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آری هما چو می‌شود از آشیان جدا</p></div>
<div class="m2"><p>کی آشیانه را حد پرواز با هماست!</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شاید به جذبه‌ای کشدم سوی خویشتن</p></div>
<div class="m2"><p>هرچند جسم و جان را مرکز ز هم جداست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جایی که جان پاک نبی می‌کند عروج</p></div>
<div class="m2"><p>گر جسم من مشایعت جان کند رواست</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ای کرده جسم پاک تو جان در تن زمین</p></div>
<div class="m2"><p>شد جانور زمین ز نوال تو و سزاست</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چون خاک تیره، جانور از فیض عام تست</p></div>
<div class="m2"><p>جانم که خاک تست ز جان پس چرا جداست!</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>در راه کعبه رفتنت ای من فدای تو</p></div>
<div class="m2"><p>دل را به سوی نکته باریک رهنماست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>این خود مقررست که ارباب هوش را</p></div>
<div class="m2"><p>قطع طریق عشق نه تنها همین به‌پاست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>تن چون به سوی کعبه تن‌ها روان شود</p></div>
<div class="m2"><p>جان را به سوی کعبه جان‌ها شتاب‌هاست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>دانی که چیست کعبه جان، جان کعبه اوست</p></div>
<div class="m2"><p>قطع طریق وادی آن طی ماسواست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تکلیف تن به کعبه به نزدیک هوشمند</p></div>
<div class="m2"><p>جان را به خوان نعمت قرب خدا صلاست</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بی‌آنکه وصل کعبه شود جسم را نصیب</p></div>
<div class="m2"><p>جان را به رب کعبه همه کام‌ها رواست</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شهباز جان پاک تو همراه جبرئیل</p></div>
<div class="m2"><p>کش دست و لب به مایده قرب آشناست</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>زان پیشتر که جسم ره کعبه طی کند</p></div>
<div class="m2"><p>شوقش به وصل کعبه جان‌ها رساند راست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>در راه کعبه مرده و آسوده در نجف</p></div>
<div class="m2"><p>ای من فدای خاک تو این مرتبت کراست!</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از راه کعبه‌ت نجف آورد سوی خویش</p></div>
<div class="m2"><p>این جذبه کار قوت بازوی مرتضی است</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>این هم اشارهٔیست مبرا ز شک و ریب</p></div>
<div class="m2"><p>ان را که دل به کعبه تحقیق آشناست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یعنی میانه نجف و کعبه فرق نیست</p></div>
<div class="m2"><p>آسوده باش ما ز خدا و خدا ز ماست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>فیاض رشته سخن اینجا گسسته به</p></div>
<div class="m2"><p>کاین انتها به نزد خرد خیرالانتهاست</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>من بعد حسرت تو و خاک در نجف</p></div>
<div class="m2"><p>کانجا مراد هر دو جهانت به زیرپاست</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آیی اگر به آتش دوزخ توان زدن</p></div>
<div class="m2"><p>جز خاک آستان نجف در جهان کجاست!</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دست دعا برآر به درگاه کردگار</p></div>
<div class="m2"><p>زین خاک پرامید که آب رخ دعاست</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>پرنور باد مرقد پاک خدایگان</p></div>
<div class="m2"><p>تا آرزوی خلق به خاک نجف رواست</p></div></div>