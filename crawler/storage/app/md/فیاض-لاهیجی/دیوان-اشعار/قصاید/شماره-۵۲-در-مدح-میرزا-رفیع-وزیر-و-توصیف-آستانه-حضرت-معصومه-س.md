---
title: >-
    شمارهٔ ۵۲ - در مدح میرزا رفیع وزیر و توصیف آستانه حضرت معصومه (س)
---
# شمارهٔ ۵۲ - در مدح میرزا رفیع وزیر و توصیف آستانه حضرت معصومه (س)

<div class="b" id="bn1"><div class="m1"><p>به خانه‌ای که تو کردی دمی درو مسکن</p></div>
<div class="m2"><p>نرفت تا ابدش آفتاب از روزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز رشک آینه سوزم از آنکه می‌دانم</p></div>
<div class="m2"><p>که عکس روی تو گاهی در آن کند مسکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شمع روی تو هر خانه‌ای که نور گرفت</p></div>
<div class="m2"><p>دوید پرتو خور همچو دود از روزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای طینت حس تو دست صنع بسی</p></div>
<div class="m2"><p>ببیخت ریزة خورشید را به پرویزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو هر کجا که کمندیست از بلا و ستم</p></div>
<div class="m2"><p>ز نارسائی بختم مراست در گردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمند زلف تو کو دام محنت است و بلا</p></div>
<div class="m2"><p>ندانم از چه سبب نیست هم به گردن من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مراست غمکدة سینه دایما تاریک</p></div>
<div class="m2"><p>اگر چه دارد از امدادِ چاک صد روزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ضعف قوّت رحلت نمانده ورنه مرا</p></div>
<div class="m2"><p>نفس برون شده بودی هزار بار از تن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>امید جغد چنانم نشسته در پس مرگ</p></div>
<div class="m2"><p>که هست گویی میراث‌ خوار کلبة من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک ز کینه گذشت و زمانه مهر گزید</p></div>
<div class="m2"><p>تو همچنان به جفا ایستاده عهد شکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا چو گردش چشم تو حال گردانست</p></div>
<div class="m2"><p>چرا کنم گله از گردش سپهر کهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ملامتم ز غم از حد گذشت و می‌ترسم</p></div>
<div class="m2"><p>که تیره گرددم آیینة دل روشن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه همدمی، نه رفیقی که از لطافت مهر</p></div>
<div class="m2"><p>تواند از دل زارم زدود گرد محن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زآشنایی بیگانگان ملول شدم</p></div>
<div class="m2"><p>خوشا فراغت بیگانگیّ و کنج حزن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر آن سرم که نشیمن کنم به بزم کسی</p></div>
<div class="m2"><p>که وارهم به پناهش زدهر جادو فن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به بزمگاه ولی‌نعمتی که در کنفش</p></div>
<div class="m2"><p>ز شرّ حادثه آسوده‌اند صد چون من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رفیع ملّت و دین آفتاب شرع مبین</p></div>
<div class="m2"><p>بلندرتبه پناه زمان و صدر زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر نسب گویی متصل به خیر رسل</p></div>
<div class="m2"><p>اگر حسب پرسی متصف به خلق حسن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زهی به حسن شیم فایق از همه اقران</p></div>
<div class="m2"><p>چنان که شاخ گل از نورسیدگان چمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به بزم قدس چراغ ضمیر پاک ترا</p></div>
<div class="m2"><p>کزوست تیره شب فضل و مردمی روشن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان ز پنبة مه می‌کند فتیله مدام</p></div>
<div class="m2"><p>فلک ز شیرة خورشید می‌دهد روغن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز کلک مشک سواد تو هر رقم باشد</p></div>
<div class="m2"><p>شبی ز نور معانی به روز آبستن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>طلوع پرتو مهر تو هر کجا باشد</p></div>
<div class="m2"><p>به آفتاب رسد جلوة سها کردن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر به نور ضمیر تو ره رود گردون</p></div>
<div class="m2"><p>به کجروی نشود شهرة زمین و زمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر ز تیزی طبعت سخن کنم شاید</p></div>
<div class="m2"><p>که ذوالفقار نماید مرا زبان به دهن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چنان ز نور ضمیر تو دیده درگیرد</p></div>
<div class="m2"><p>که رشته خطّ نظر شد به دیدة سوزن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو در عراقی و مردم نموده چشم سفید</p></div>
<div class="m2"><p>به خاک تیرة لاهور و آب شور دکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز بیم عدل سیاستگر تو ممکن نیست</p></div>
<div class="m2"><p>نسیم را گُلِ بویی ز گلستان چیدن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز لطف طبع تو اشیا چنان لطیف شدند</p></div>
<div class="m2"><p>که همچو عکس توان غوطه خورد در آهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان به عهد تو برخاست رسم شکوه ز دهر</p></div>
<div class="m2"><p>که عندلیب فراموش کرده نالیدن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به غیر من ز تو محروم در جهان کس نیست</p></div>
<div class="m2"><p>ولی ز پستی طالع ز تیره‌بختی من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر به سایه نیفتد ز منع شخص فروغ</p></div>
<div class="m2"><p>ز آفتاب شکایت نمی‌توان کردن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هزار بار شنیدم که گفته‌ای فیّاض</p></div>
<div class="m2"><p>که هست شمع هنر در زمنه زو روشن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چرا چنین شده خلوت‌نشین بزم خمول؟</p></div>
<div class="m2"><p>چرا چنین شده عزلت گزین کنج محن؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه یوسفست و ندارد خلاصی از زندان؟</p></div>
<div class="m2"><p>نه بیژنست و فرو رفته در چه بیژن؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چرا به سایة ما درنمی خزد که شود</p></div>
<div class="m2"><p>چو آفتاب فلک شمع طالعش روشن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدایگانا، این لطف را جوابی هست</p></div>
<div class="m2"><p>ولی خدا دهدم جرأت بیان کردن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هزار بار به دل نقش بسته‌ام که کنم</p></div>
<div class="m2"><p>در آن محیط رجال هنروری مأمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولی چه چاره دکنم ره نمی‌توانم رفت</p></div>
<div class="m2"><p>که دست حادثه پایم شکسته در دامن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به درگهت نرسم زانکه بی‌تهیّة زاد</p></div>
<div class="m2"><p>نمی‌نماید احرام کعبه مستحسن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز دور درد دلی می‌کنم که در همه وقت</p></div>
<div class="m2"><p>ز قرب و بعد بود آفتاب نورافکن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سخن طراز چه غم گر نباشدت نزدیک؟</p></div>
<div class="m2"><p>که گوش لطف تو از دور می‌رسد به سخن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>توان شناختن احوال از قرینة حال</p></div>
<div class="m2"><p>که هست پیش ضمیر تو نیک و بد روشن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ادا چه‌گونه کنم خود که گشته است از شرم</p></div>
<div class="m2"><p>زبان ناطقه در مدّعای خود الکن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لسان قالم اگر بسته شد چه غم دارم</p></div>
<div class="m2"><p>زبان حال نخواهد مؤونت گفتن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز شرح حال پریشانی دلم با تو</p></div>
<div class="m2"><p>فتاد سلسلة نظم را شکن به شکن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز گفتگو نگشاید گره ولی شاید</p></div>
<div class="m2"><p>هزار نکته به یک خامشی ادا کردن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز پاک گوهری از دست چرخ خاتم شکل</p></div>
<div class="m2"><p>به خون دیده زدم غوطه چون عقیق یمن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به خار خشک قناعت کنم درین گلزار</p></div>
<div class="m2"><p>به برگ کاه تسلّی شوم از این خرمن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به مال وقت مرا کرده آسمان محتاج</p></div>
<div class="m2"><p>کنون که ملک هنر جمله وقف گشته به من</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فلک کنون به تو افکنده است کار مرا</p></div>
<div class="m2"><p>گرت ز دست برآید به دیگری مفکن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مخوان به جانب خویشم اگر چه زین طلبم</p></div>
<div class="m2"><p>رسیده تا به درت پا، گذشته سر ز پرن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گرفتم اینکه منم لؤلؤ از توجّه تو</p></div>
<div class="m2"><p>کسی برای چه لؤلؤ طلب کندن به عدن؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شرر اگر چه شب تیره پرتوی دارد</p></div>
<div class="m2"><p>خجل بود به بر آفتاب نورافکن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو از حوادث دوران پناه داد مرا</p></div>
<div class="m2"><p>به آستانة معصومه حضرت ذوالمن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>روا مدار کزین روضه دور مانم دور</p></div>
<div class="m2"><p>که از غبار درش گشته دیده‌ام روشن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چه آستانه بهشتی که بیند از رضوان</p></div>
<div class="m2"><p>چنان غبار در او بگیردش دامن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>که با هزار فسون و فسانه نتواند</p></div>
<div class="m2"><p>به نیم ذرّه دل از خاک روبیش کندن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز پوست نافه برون آید و دهد انصاف</p></div>
<div class="m2"><p>کنند نسبت خاکش اگر به مشک ختن</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>سرشت آدم ازین خاک اگر شدی، ابلیس</p></div>
<div class="m2"><p>نهادی از سر رغبت به سجده‌اش گردن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مثال روضة او ناشنیده پیر خرد</p></div>
<div class="m2"><p>به شکل مدرسة او ندیده چرخ کهن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بعینه حجراتش صوامع ملکوت</p></div>
<div class="m2"><p>در آن به صورت انسان مَلّک گرفته وطن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز شرم چشمة حیوان فرو رود به زمین</p></div>
<div class="m2"><p>ز حوض مدرسه پیشش اگر کنند سخن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چه حاجت است که لب تر کند ازو تشنه</p></div>
<div class="m2"><p>همین بس است که نام وی آیدش به دهن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز جرم ماه کند محو تیرگی آسان</p></div>
<div class="m2"><p>در آن تواند اگر همچو عکس غوطه زدن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به نوربخشی گردد چو آفتاب مثل</p></div>
<div class="m2"><p>به فرض بخت من اینجا بشوید ار سر و تن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فکنده کاهکشان عکس اندران گویی</p></div>
<div class="m2"><p>ز بسکه ریگ ته جو بود فروغ‌افکن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به سنگریزة آن جوهری برد گر پی</p></div>
<div class="m2"><p>برای لؤلؤ دیگر نمی‌رود به عدن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بدین امید که آسودة درش گردد</p></div>
<div class="m2"><p>سپهر پیر همی آرزو کند مردن</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ز فیض‌بخشی خاکش چه شهر قم چه بهشت</p></div>
<div class="m2"><p>ز عطر او چه زمین فرج چه دشت ختن</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بسان آنکه بروبد کسی ز خانه غبار</p></div>
<div class="m2"><p>در آستانة آن فیض می‌توان رُفتن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از آن همیشه دهد نور آفتاب که کرد</p></div>
<div class="m2"><p>ز شمع بارگه او چراغ خود روشن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نهال شمع که دارد گل تجلّی بار</p></div>
<div class="m2"><p>بود بعینه چون نخل وادی ایمن</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در آستانة او کز وفور مایة فیض</p></div>
<div class="m2"><p>به چشم مردم دانا خوش است چون گلشن</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گشوده مصحف خوانا ز هر طرف بینی</p></div>
<div class="m2"><p>چنانکه دفتر گل وا شود به روی چمن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در آن میانه به الحان جانفزا حفّاظ</p></div>
<div class="m2"><p>چو بلبلان چمن نغمه‌سنج و دستان‌زن</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>به دور قبّه قنادیل مغفرت بینی</p></div>
<div class="m2"><p>به فرق زوّار از عکس نور سایه‌فکن</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ز خطّ وهمی ترکیب‌بند شکل بروج</p></div>
<div class="m2"><p>قیاس رشتة قندیل‌ها توان کردن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>غبار فرش درش آبروی مهر منیر</p></div>
<div class="m2"><p>خط کتابة او سرنوشت چرخ کهن</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>همیشه تا بود این آستانه مشرق نور</p></div>
<div class="m2"><p>همیشه تا بود این خاک فیض را معدن</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>تو با صدارت کل باشیش نسق‌فرما</p></div>
<div class="m2"><p>به حسن سعی تو بادا رواج این مأمن</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تو همچو شاخ گل آیین‌فزای این گلزار</p></div>
<div class="m2"><p>چو عندلیب من آوازه‌سنج این گلشن</p></div></div>