---
title: >-
    شمارهٔ ۴۴ - مطلع سوم
---
# شمارهٔ ۴۴ - مطلع سوم

<div class="b" id="bn1"><div class="m1"><p>بر سریر پادشاهی پادشاه کامیاب</p></div>
<div class="m2"><p>جلوه‌گر گردید چون بر تخت گردون آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کودکی از سر گرفت این پیر بازیگوش چرخ</p></div>
<div class="m2"><p>زال گیتی را مبدل گشت پیری با شباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیرگی برخاست از روی زمانه آن‌چنان</p></div>
<div class="m2"><p>کز فروغ مهر برخیزد بخار از روی آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو زلیخای جهان از سر جوانی تازه کن</p></div>
<div class="m2"><p>مصر گیتی را فزود از نور یوسف آب‌وتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پایه بالا رفت تخت سلطنت را تا به عرش</p></div>
<div class="m2"><p>سر به گردون سود تاج خسروی چون آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طعنه بر شکر ز شیرینی زند شیرین ملک</p></div>
<div class="m2"><p>دست مهر خسرو عهدست او را طره‌تاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو اقلیم‌آرا داور آفاق‌گیر</p></div>
<div class="m2"><p>صفدر خورشد مغفر فارس گردون رکاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه دریادل صفی شاهنشه گردون که هست</p></div>
<div class="m2"><p>آفتاب از تیغ عالمگیر او در اضطراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده‌ای از خیمه‌گاه حشمت او آسمان</p></div>
<div class="m2"><p>ذره‌ای از جلوه‌گاه مرکب او آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبزه‌پرور در ریاض دهر جودش چون مطر</p></div>
<div class="m2"><p>سایه‌گستر بر سر آفاق عدلش چون سحاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفتاب از هیبت شمشیر قهرش لرزه‌زن</p></div>
<div class="m2"><p>وز کمند دیوبندش آسمان در پیچ‌وتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آسمان درگاه او را می‌تواند شد محیط</p></div>
<div class="m2"><p>گر تواند بر سر دریا زدن خرگه حباب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون شکار انداز دل گردد به شاهین نگاه</p></div>
<div class="m2"><p>در نهاد مرغ دل آرام گردد اضطراب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرغ فارغ‌بال را ذوق گرفتاری کند</p></div>
<div class="m2"><p>دلنشین‌تر زآشیان خویش چنگال عقاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیش گردد آب خوش با زهر قهرش در مذاق</p></div>
<div class="m2"><p>نوش گردد زهرمار از التفاتش همچو آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه فلک در کشتی اقبال او یک بادبان</p></div>
<div class="m2"><p>وز کف دریا نهادش هفت دریا یک حباب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک شرر از شعله قهرش جحیم هفت در</p></div>
<div class="m2"><p>یک چمن از گلشن لطفش بهشت هشت باب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آب تیغ برق‌آسایش ز جوی ذوالفقار</p></div>
<div class="m2"><p>زور بازوی توانایش ز صلب بوتراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رایت نصر من‌الله قصر قدرش را ستون</p></div>
<div class="m2"><p>خیمه اجلالش از حبل‌المتین دارد طناب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چوب دربانش صداع چرخ را صندل‌فروش</p></div>
<div class="m2"><p>بار احسانش جیاد خلق را مالک رقاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه‌بیت قدرش از ترکیب گردون منتخب</p></div>
<div class="m2"><p>مطلع اقبالش از دیوان خورشید انتخاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفتاب از سایه او نور می‌گیرد به وام</p></div>
<div class="m2"><p>آسمان از پایه او می‌کند قدر اکتساب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در نهاد کوه، سهم او درآرد زلزله</p></div>
<div class="m2"><p>وز دل سیماب لطف او برآرد اضطراب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پرتو مهرش دلفروزست چون برق امید</p></div>
<div class="m2"><p>شعله قهرش عدوسوزست چون تیر شهاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>او نبود اول که شاهان جهان را نام بود</p></div>
<div class="m2"><p>جلوه انجم بود پیش از طلوع آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابر لطفش گر ببارد قطره‌ای بر گلستان</p></div>
<div class="m2"><p>تا ابد دیگر نیابی تلخکامی در گلاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا ابد بی‌خان‌ومانی شد نصیب جغد و بس</p></div>
<div class="m2"><p>بسکه عدلش در جهان نگذاشت جایی را خراب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عقل پیرش داده ایزد در سر و بخت جوان</p></div>
<div class="m2"><p>دولتش در عهده خود کرده کار شیخ و شاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گشته از بیم سیاست‌های ضبط دولتش</p></div>
<div class="m2"><p>تا کتان ظلم را گردیده عدلش ماهتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سرکشی‌ها پای‌بند زلف محبوبان چو چین</p></div>
<div class="m2"><p>گوشه‌گیر چشم خوبان فتنه‌ها مانند خواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تیغ در دست جهانگیرش چودر دریاست موج</p></div>
<div class="m2"><p>جلوه بر بالای رهوارش چو بر چرخ آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون سمند نیلگون در زین کشد پس درخورست</p></div>
<div class="m2"><p>کهکشانش جای تنگ و ماه نو جای رکاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حبذا رخشی که از نرمی چو آید در خرام</p></div>
<div class="m2"><p>می‌تواند همچو موج آید روان بر روی آب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شوخ‌وش چابک‌روش لیلی‌منش عذرانظر</p></div>
<div class="m2"><p>کاکل‌افشان موپریشان کم‌درنگ و پرشتاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چرخ‌پیکر، مهرمنظر، ماهرو، دریاخروش</p></div>
<div class="m2"><p>آسمان‌جنبش، ستاره‌گردش، آتش اضطراب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آب را ماند که از آتش نمی‌یابد نهیب</p></div>
<div class="m2"><p>باد را ماند که از دریا نمی‌گیرد حساب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زلف یال از دلفریبی گیسوی پرتاب حور</p></div>
<div class="m2"><p>چتر دم در جانفزایی دسته سنبل به تاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در لباس جلوه رنگین همچو طاووس خیال</p></div>
<div class="m2"><p>در یراق گوهر آیان چون عروس بی‌نقاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شوخیش دردست و پا چون شعله در دست نسیم</p></div>
<div class="m2"><p>تندیش در رگ چو زور نشئه در موج شراب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>می‌دود هموارتر از رنگ می بر روی یار</p></div>
<div class="m2"><p>می‌جهد آسان‌تر از مژگان عاشق مثل خواب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زین، عیان بر پشت او چون کبک بر بالای کوه</p></div>
<div class="m2"><p>بر کتف زلف عنان چون طره پرپیچ و تاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شاه چون یوسف عزیز مصر زین، وز هر طرف</p></div>
<div class="m2"><p>حلقه چشم زلیخا حلقه چشم رکاب</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون نهد پا در رکاب و چون به کف گیرد عنان</p></div>
<div class="m2"><p>در عنان کیخسرو افتد در رکاب افراسیاب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ای مدار دهر را بایست‌تر از آسمان</p></div>
<div class="m2"><p>ای جمال روز را درکارتر از آفتاب</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عهد ملکت گلشن ایام را فصل بهار</p></div>
<div class="m2"><p>دور گردون را بهار دولتت عهد شباب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روز عرض لشکرت تعبیر او خواهد شدن</p></div>
<div class="m2"><p>گر شبی بیند فلک روز قیامت را به خواب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کی زر خورشید هرگز رایج افتادی چنین</p></div>
<div class="m2"><p>گرنه از نام تو کردی سکه نور اکتساب</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خطبه را کی می‌شدی آوازه بر چرخ بلند</p></div>
<div class="m2"><p>گرنه با آوازه نامت نمودی انتساب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در سرش افتاده پنداری هوای دست تو</p></div>
<div class="m2"><p>لاجرم خاطر تهی کردست از دریا حباب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدر گردون تا مقابل دیده ماه پرچمت</p></div>
<div class="m2"><p>رفته‌رفته می‌کند پهلو تهی از آفتاب</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون ز جوهر چین فتد بر ابروی شمشیر تو</p></div>
<div class="m2"><p>در درون سنگ آتش گردد از بیم تو آب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پادشاها حاجتی دارم به خاک درگهت</p></div>
<div class="m2"><p>حاجتی کز سرمه دارد دیده ناکرده خواب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>داد را کامی به دل دارم که می‌گویم به رمز</p></div>
<div class="m2"><p>زانکه شاهی چون تو باید کامبخش ورمزیاب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>لیک لطفت لذتی دارد که ترسم یاد آن</p></div>
<div class="m2"><p>محو سازد از دل من وعده یوم‌الحساب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>من به لذت‌‌های دنیا چشم اندازم ز دور</p></div>
<div class="m2"><p>زانکه چون نزدیک گردی خاک بنماید سراب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من که حرف سرنوشتم بوده از روز ازل</p></div>
<div class="m2"><p>انتساب این حریم و التجای این جناب</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من که داده آب و تاب گوهر من بی‌گزاف</p></div>
<div class="m2"><p>گردش این آسمان و تابش این آفتاب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>من که تا بسته است بر من آب رحمت ز آسمان</p></div>
<div class="m2"><p>کشته امید خود را داده‌ام زین چشمه آب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>این‌که رنجورم سراسر دیده بودم این دوا</p></div>
<div class="m2"><p>این‌که مخمورم لبالب خورده بودم این شراب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تشنه گر آبی خورد از چشمه حیوان خوش است</p></div>
<div class="m2"><p>ورنه آب جوی مردم می‌برد از روی آب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از سواد نسخه شرح پریشانی پرست</p></div>
<div class="m2"><p>فردفردم همچو دفتر جزوجزوم چون کتاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خون دل خوردم بسی، نه بلکه دل خوردم بسی</p></div>
<div class="m2"><p>داشتم از پهلوی دل هم شراب و هم کباب</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گربه درد من رسد کس آن تو خواهی بود و بس</p></div>
<div class="m2"><p>ورنه می‌دانم ندارد کس سؤال را جواب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زمزم لطف ترا آن تشنه‌ام کز بیخودی</p></div>
<div class="m2"><p>در میان دجله جان می‌داد و خوش می‌گفت آب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>عرض حاجت کردم اکنون می‌روم کز بهر شاه</p></div>
<div class="m2"><p>پر کنم هفت آسمان را از دعای مستجاب</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا سپهر از انجم آرد از برای شه سپاه</p></div>
<div class="m2"><p>تا فلک از ماه نو دارد سمندش را رکاب</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دولت شه روزافزون باد چون نور هلال</p></div>
<div class="m2"><p>لشکرش چون نور کوکب برتر آید از حساب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>آسمان در زیر بار منت این آستان</p></div>
<div class="m2"><p>نیر اعظم به مژگان خاکروب این جناب</p></div></div>