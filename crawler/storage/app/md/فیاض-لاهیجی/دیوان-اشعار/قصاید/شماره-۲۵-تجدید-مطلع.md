---
title: >-
    شمارهٔ ۲۵ - تجدید مطلع
---
# شمارهٔ ۲۵ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>ندانم از نظر من چسان بود مستور</p></div>
<div class="m2"><p>رخی که یک نفس از خاطرم نگردد دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاه بد نتوان بر جمال او دیدن</p></div>
<div class="m2"><p>دعا کنیم کز آن روی چشم آینه دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رشگ پیک نظر را نمی‌توانم دید</p></div>
<div class="m2"><p>که در حوالی کویش کند به سهو مرور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مباد عرصه آفاق بوی او گیرد</p></div>
<div class="m2"><p>نسیم را به سر کوی او مباد عبور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تاب نور نشاید جمال او دیدن</p></div>
<div class="m2"><p>خوشا رخی که بود در شعاع خود مستور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم ذره نظرکن که تا شود روشن</p></div>
<div class="m2"><p>که آفتاب به هر ذره کرده است ظهور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین به خویش نگیرد ز ننگ، اگر عشاق</p></div>
<div class="m2"><p>به غیر حسرت دیدار او برند به گور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان دل به دو مصرع گرفت ابرویش</p></div>
<div class="m2"><p>که صاحب سخن از مطلعی شود مشهور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیال دوست ز هر تار موی من جوشد</p></div>
<div class="m2"><p>چو نقش صورت شیرین ز خامه شاپور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم ز جلوه عکسش چنین خراب چراست؟</p></div>
<div class="m2"><p>رخی که خانه آیینه شد ازو معمور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس از فراق توان قدر وصل دانستن</p></div>
<div class="m2"><p>مرا ز کوی تو دوری ضرور بود ضرور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از مشاهده کوهکن یقینم شد</p></div>
<div class="m2"><p>که وصل دوست میسر نبوده است به زور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من آن نیم که خلاصی ز درد و غم جویم</p></div>
<div class="m2"><p>هزار چاره نمودم که داغ شد ناسور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عشق روی تو صبرم فرار کرده ز دل</p></div>
<div class="m2"><p>به یاد چشم تو خوابم ز چشم کرده نفور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز سینه سوز تو کمتر نمی‌شود هرچند</p></div>
<div class="m2"><p>نهد به داغ دلم پنبه مرهم کافور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جدا ز کوی تو راهی به هیچ سو نبرم</p></div>
<div class="m2"><p>که روز هجر تو بر من شب است و من شب‌کور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به ذکر نام خوشت تشنه می‌رود سیراب</p></div>
<div class="m2"><p>به یاد لعل لبت مست می‌شود مخمور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز رشگ روی تو داغست زلف را مگشای</p></div>
<div class="m2"><p>که داغ تازه خورشید می‌شود ناسور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلم ز لعل تو بی‌نیش غیرنوش نخورد</p></div>
<div class="m2"><p>رقیب بر سر شهدت نشسته چون زنبور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگاه مست تو هرگه به طرف باغ افتاد</p></div>
<div class="m2"><p>به تاک قطره می گشت دانه انگور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کنون که دور ز کویت اسیر هجرانم</p></div>
<div class="m2"><p>چو در شکنجه شهباز، ناتوان عصفور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هوای فر سلیمانی است در سر من</p></div>
<div class="m2"><p>که کرده تنگ جهان را به من چو دیده مور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز ذوق خاک‌نشینی درگهی که بود</p></div>
<div class="m2"><p>قضاش نادره معمار و آسمان مزدور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه درگه آنکه بود آستان سبع شداد</p></div>
<div class="m2"><p>به پیش محکمیش معترف به عجز و قصور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه درگه آنکه نگردید دست قدرت را</p></div>
<div class="m2"><p>بلندپایه‌تر از وی عمارتی مقدور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه‌درگه آنکه به چندین هزار نقش و نگار</p></div>
<div class="m2"><p>سپهر را نتوان گفت پیش او معمور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه‌درگه آنکه به سطحش گر آفتاب افتد</p></div>
<div class="m2"><p>گمان بری که به سطحش نشسته گرد فتور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هزار سال گذشت و بسا که هم گذرد</p></div>
<div class="m2"><p>که بیم کهنگیش نیست از مرور دهور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلند درگه سلطان شرع و ملت و دین</p></div>
<div class="m2"><p>که صیت سلطنتش هست تا به نفخه صور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امام جعفر صادق که صبح صادق رای</p></div>
<div class="m2"><p>به مهر خاک درش دم همی زند از نور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهی که دایره عدل او دو عالم را</p></div>
<div class="m2"><p>کشیده است در آغوش چون بلدراسور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به پیش عرصه ملک ابد نهایت او</p></div>
<div class="m2"><p>نمود ملک سلیمانی است چون پی مور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سکون و جنبش رایات نصرتش تا هست</p></div>
<div class="m2"><p>زمانه را حرکات سپهر نیست ضرور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز بیم، تا به ابد متصل شود به عدم</p></div>
<div class="m2"><p>صلابتش به ازل گر کند تغافل دور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنین که داروی هر درد ازوست حیرانم</p></div>
<div class="m2"><p>که آفتاب چرا مانده این چنین رنجور!</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به هر طرف که کند میل، در قدم باشد</p></div>
<div class="m2"><p>به پیش ابد چو صبا بر پسش ازل چو دبور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر ز چهره رایش نقاب بردارند</p></div>
<div class="m2"><p>چو آفتاب شود کاینات غرقه نور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به گلشنی که چو خورشید پرتو اندازد</p></div>
<div class="m2"><p>ز شاخ خشک برآید گل تجلی نور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به شکر نعمت او آسمان به یک سر پاست</p></div>
<div class="m2"><p>که گر دمی بنشیند ز پای نیست شکور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>محیط علمش اگر موج‌ور شود گردد</p></div>
<div class="m2"><p>به نیم رشحه او حل مشکلات امور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به گرد خاطر او سیر فوج فوج اسرار</p></div>
<div class="m2"><p>چنان‌که در چمن باغ خلد جلوه حور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بود ز دفترش افلاک آن ورق که بود</p></div>
<div class="m2"><p>به نیم صفحه‌اش انجیل ضبط و نیم زبور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دو سایه از سخط و عفو او جحیم و نعیم</p></div>
<div class="m2"><p>دو پرتو از غضب و لطف اوست ماتم و سور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر زبانه قهر خدا عیان خواهی</p></div>
<div class="m2"><p>ببین به گوشه ابروی قهر او از دور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>وگر گشاده در فیض را ندیدستی</p></div>
<div class="m2"><p>ببین به جبهه صبح آیتش تبسم نور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جراحت دل صد خسته را شود مرهم</p></div>
<div class="m2"><p>تبسمش به لب لعل چون شود پرشور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز فیض معدلت عام او عجب نبود</p></div>
<div class="m2"><p>خرابه دل عاشق اگر شود معمور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو رنگ نهی برآید به چهره نگهش</p></div>
<div class="m2"><p>ز بیم آب شود زهره در دل انگور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سیاستش به غضب چهره چون برافروزد</p></div>
<div class="m2"><p>گره شود نفس نغمه در رگ طنبور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>رسد به سبع شداد ار مهابتش فکند</p></div>
<div class="m2"><p>در او به سعی تزلزل هزارگونه فتور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر نداند قدرش چه غم که رفته به خواب</p></div>
<div class="m2"><p>رگ شعور حسودش که هست خصم شعور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زبان دشمن او نیش می‌زند بر دل</p></div>
<div class="m2"><p>به نوش غوطه زند گر چو نشتر زنبور</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چنان گرفته رگ حلق دشمنش را بخل</p></div>
<div class="m2"><p>که آشنا به گلویش نشد به جز ساطور</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به او عداوت بدخواهش اختیاری نیست</p></div>
<div class="m2"><p>بود جبلی خفاش دشمنی با نور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>وجود خصم برای ظهور اسبابست</p></div>
<div class="m2"><p>که آفتاب به تقریب سایه شد مشهور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز خلق اوست فلک مجمری که هست از وی</p></div>
<div class="m2"><p>مشام عالم بالا پر از بخار و بخور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عذوبت سخنش در مذاق تشنه عقل</p></div>
<div class="m2"><p>نشسته است چو می در طبیعت مخمور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به گاه نظم چو گردد سخن به وصف کفش</p></div>
<div class="m2"><p>به موج بحر برآید مقطعات بحور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زهی به ذات و صفات از جهانیان ممتاز</p></div>
<div class="m2"><p>چو در میانه انوار نور آتش طور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هلاک نظم علوم تو نظم عقد پرن</p></div>
<div class="m2"><p>اسیر نثر کلام تو لؤلؤ منثور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>زواهر حکمت آسمان دین را نجم</p></div>
<div class="m2"><p>جواهر کلمت ملک شرع را دستور</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز خاک پای تو هر شب به دیده‌بانی دهر</p></div>
<div class="m2"><p>کشد به دیده انجم سپهر سرمه نور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به کارخانه تقدیر مستمد از تست</p></div>
<div class="m2"><p>به حل و عقل مقاصد مدبرات امور</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بود اوامر «کن» را به خطة تقدیر</p></div>
<div class="m2"><p>به خاطر تو ورود و ز سینة تو صدور</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>غبار راه تو بر تن برای حفظ شرف</p></div>
<div class="m2"><p>هزاربار نکوتر از اطلس و سیفور</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>دمی به سایه دیوار کویت آسودن</p></div>
<div class="m2"><p>مرا ز سایه طوبی به است و حور و قصور</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چه خار و خس ز حریمت چه بالهای ملک</p></div>
<div class="m2"><p>که رفته‌اند به جاروب طره، زمره حور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به منزلی که بود خاکروب بال ملک</p></div>
<div class="m2"><p>چه عیش‌ها که توان کرد چشم دشمن کور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کند به شاکله هرچند میل باکی نیست</p></div>
<div class="m2"><p>گزیده‌اند اگر بر تو خصم را جمهور</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>اگر به راه تو کمتر روند خلق چه باک</p></div>
<div class="m2"><p>پل صراطی و سخت است بر صراط عبور</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>متابع تو اگر کم بود چه غم که شود</p></div>
<div class="m2"><p>هزار قطره یکی گوهر، آنگهی به مرور</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گهر ز طینت پاکست آنچنان کم‌یاب</p></div>
<div class="m2"><p>خزف ز خست ذاتست آنقدر موفور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>بود به مرتبه از کاینات بیش انسان</p></div>
<div class="m2"><p>ولی بود به مراتب کم از شماره مور</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همان به جنس بشر کن نظر که از کثرت</p></div>
<div class="m2"><p>میان آدم و نا آدم است نسبت دور</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هزار نطفه بباید که تا یکی گردد</p></div>
<div class="m2"><p>به قابلیت اطوار متصف به وفور</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو گشت قابل اطوار قرن‌ها باید</p></div>
<div class="m2"><p>که تا یکی به بر آید به صد مشقت و زور</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>رسید چون به بر از صدهزار کم افتد</p></div>
<div class="m2"><p>یکی چنان که برد دیده نور و سینه سرور</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>خدایگانا خورشید عالم جانا</p></div>
<div class="m2"><p>تویی که سر نبوت شد از تو محو ظهور</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>تویی خلاصه عترت تویی نقاوه نسل</p></div>
<div class="m2"><p>که هست روح رسول از تو تا ابد مسرور</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تویی تو آنکه پس پرده قضا عمری</p></div>
<div class="m2"><p>ازل به روی تو افکنده بود چشم از دور</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>دوام دولت تست آنکه چشم بر ره اوست</p></div>
<div class="m2"><p>ابد که در تتق غیب کرده رخ مستور</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>چه حاجت است به تعریف عقل ذات ترا</p></div>
<div class="m2"><p>که بی‌نقاب درآید رخت به دیده کور</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>منم یکی ز غلامان درگهت که مدام</p></div>
<div class="m2"><p>به مدحت تو زبانم بود زبانه نور</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>منم که هست ز فکر مدیح حضرت تو</p></div>
<div class="m2"><p>سرادقات ضمیرم سرای پرده حور</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>هزار صورت شیرین سیه‌قلم دارم</p></div>
<div class="m2"><p>سفیدروی‌تر از نقش خامه شاپور</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>درین قصیده تو کردی مدد روان مرا</p></div>
<div class="m2"><p>وگرنه مانده به بند «ظهیر» بود این زور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کنون امید من اینست در دو عالم و بس</p></div>
<div class="m2"><p>که با سگان درت سازدم خدا محشور</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>چو آفتاب کنم چرخ خاک را روشن</p></div>
<div class="m2"><p>چراغ مهر ترا چون برم به خلوت گور</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اگرچه زلت فیاض بیش در بیش است</p></div>
<div class="m2"><p>گناه او به تو بخشد یقین خدای غفور</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>همیشه تا که گریبان عقل کل باشد</p></div>
<div class="m2"><p>چو جیب خامه‌ام از مدحت تو مشرق نور</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>بود به دامن لطف تو متصل دستم</p></div>
<div class="m2"><p>چو دست حسرت زاهد به طرف دامن حور</p></div></div>