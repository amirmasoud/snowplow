---
title: >-
    شمارهٔ ۴۹ - مطلع دوم
---
# شمارهٔ ۴۹ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>ای روزگار را به وجود تو خرّمی</p></div>
<div class="m2"><p>بر خویشتن ببال که یکتای عالمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون آسمان به عرصة آفاق سروری</p></div>
<div class="m2"><p>چون آفتاب در همه کشور مسلّمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سایة تو سجده برد نور آفتاب</p></div>
<div class="m2"><p>کاندر نسب زاشرف اولاد آدمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدر ترا پرستش ایام شد جلال</p></div>
<div class="m2"><p>کز رتبه بر تمامت عالم مقدّمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ذات تست نازش دین و دول که تو</p></div>
<div class="m2"><p>سلطان علم بودی و دستور عالمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جنب لاتناهی ابعاد جاه خویش</p></div>
<div class="m2"><p>برهم زن قضیّة برهان سلّمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حصر فضایل تو نصیب شماره نیست</p></div>
<div class="m2"><p>این جایگه بود که فزونی کند کمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاک درت به دیدة افلاک توتیاست</p></div>
<div class="m2"><p>اینجا بلند چون نشود قدر آدمی!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلزار دولت تو ازل‌پرور آدمست</p></div>
<div class="m2"><p>زان با ابد درست کند عهد خرّمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر از کجا و تربیت این چمن کجا</p></div>
<div class="m2"><p>دریا درین محیط کند مشق شبنمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنیان شوکت تو که همسایة قضاست</p></div>
<div class="m2"><p>باج از سپهر پیر ستاند ز محکمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا غم به دولت تو ز دل‌ها کناره کرد</p></div>
<div class="m2"><p>عشرت گرفته است برات مسلّمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>داغ خود آفتاب چرا به نمی‌کند</p></div>
<div class="m2"><p>اکنون که هست لطف ترا کار مرهمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر آفتاب دم زند از نور جبهه‌ات</p></div>
<div class="m2"><p>در چارفصل کم نشود فیض خرّمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سیمای جبهة تو دم از نور می‌زند</p></div>
<div class="m2"><p>معلوم می‌شود که به خورشید توأمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در مجمع مشاهده جسم مروّحی</p></div>
<div class="m2"><p>در محفل مناظره روح مجسّمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در مجلس تو سامعه از هوش می‌رود</p></div>
<div class="m2"><p>با عقل همزبانی و با روح همدمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر رتبه‌ات که مرتبه‌سنج مراتبست</p></div>
<div class="m2"><p>کس را نداده‌اند تلاش مقدّمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر تست اقتدای خلایق که در کمال</p></div>
<div class="m2"><p>چون ذات عقل بر همه عالم مقدّمی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در جاه و در بزرگی و در شان و در شکوه</p></div>
<div class="m2"><p>چون آفتاب در همه عالم مسلّمی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم در نسب سیادت و هم در حسب کمال</p></div>
<div class="m2"><p>چشم بد از تو دور که ممتاز عالمی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در نزد حسن خلق حریف تو عاجزیست</p></div>
<div class="m2"><p>با سرکشان زیاد و زافتادگان کمی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون مرحمت به نزد خلایق معزّزی</p></div>
<div class="m2"><p>چون معدلت به پیش شهنشه مکرّمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر امتیاز شاه بنازند ماه و مهر</p></div>
<div class="m2"><p>کش در جهان تو صاحب دیوان اعظمی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اقبال شه بلند کش آیین ملک را</p></div>
<div class="m2"><p>دستور اعظمیّ و وزیر معظّمی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر شغلِ پشت پا زده‌ات خواند پادشاه</p></div>
<div class="m2"><p>دانست چون به دولت و اقبال توأمی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>محتاب بود بخت جوانش به عقل پیر</p></div>
<div class="m2"><p>شاهست آفتاب و تواش صبح همدمی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پاینده‌تر ز قائمة عرش می‌سزد</p></div>
<div class="m2"><p>بنیان دولتی که تواش رکن محکمی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از صر صر خزان نکشد زردروییی</p></div>
<div class="m2"><p>گلزار شوکتی که تواش تازه شبنمی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از دیو حادثات جهان را دگر چه بیم</p></div>
<div class="m2"><p>اکنون که پادشاه سلیمان تو خاتمی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اقبال شاه را ز تو هرگز گزیر نیست</p></div>
<div class="m2"><p>گر پادشه جسمت تو هم جام این جمی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر چند کز تقدّس ذات فرشته‌خوی</p></div>
<div class="m2"><p>دانم کزین معامله چو غنچه درهمی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیکن عموم مصلحت خلق عذر خواست</p></div>
<div class="m2"><p>در گلشن وجود عجب ابر پرنمی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غافل مشو که واسطة فیض اقدسی</p></div>
<div class="m2"><p>دل بد مکن که باعث خیر دمادمی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ختم سخن کنم به دعای تو تا ابد</p></div>
<div class="m2"><p>کاین مدّعا بهست ز هر بیشی و کمی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا مهر و ماه هست تو باشیّ و پادشاه</p></div>
<div class="m2"><p>چندان که کسب می‌کند از مهر مه همی</p></div></div>