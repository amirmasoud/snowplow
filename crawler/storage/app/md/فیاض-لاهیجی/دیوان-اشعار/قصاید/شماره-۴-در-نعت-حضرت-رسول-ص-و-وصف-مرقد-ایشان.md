---
title: >-
    شمارهٔ ۴ - در نعت حضرت رسول (ص) و وصف مرقد ایشان
---
# شمارهٔ ۴ - در نعت حضرت رسول (ص) و وصف مرقد ایشان

<div class="b" id="bn1"><div class="m1"><p>دلا تا چند خود را فرش این نُه سایبان بینی</p></div>
<div class="m2"><p>یکی بر سطح این کرسی برآ تا عرش جان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهرت آشیان آمد تو بر روی زمین تا کی</p></div>
<div class="m2"><p>چو مرغ بال و پر برکنده از دور آشیان بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کنعان تعلق همچو یعقوب از نظر بگذر</p></div>
<div class="m2"><p>که بی یوسف ستم باشد که روی این و آن بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر بربندتا از هر بن مو دیده‌ور گردی</p></div>
<div class="m2"><p>زبان بربند تا خود هر سر مو را زبان بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو قوت در نهاد تست ادراکی و تحریکی</p></div>
<div class="m2"><p>که از یک دانش آموزی و از دیگر توان بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو بال خویشتن کن این دو قوت را درین زندان</p></div>
<div class="m2"><p>به پرواز اندرآ تا آشیان خود عیان بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو پرکم دیده خود را درون این قفس یک ره</p></div>
<div class="m2"><p>به پرواز آی تا خود را همای پرفشان بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مرغ آشیان گم کرده از خود در هراسستی</p></div>
<div class="m2"><p>ببینی فرّ خود چون خویش را در آشیان بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو کوته‌بین عجایب‌های خلقت را ندیدستی</p></div>
<div class="m2"><p>فلک را این فلک دانی جهان را این جهان بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن وادی که مردان الهی راه پیمایند</p></div>
<div class="m2"><p>فلک رااندر آن وادی چو گرد کاروان بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درآ در عالم عقلی که آنجا این دو عالم را</p></div>
<div class="m2"><p>نبینی هیچ و گر بینی دو پایه نردبان بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطوط شش جهت را چون نقط بر یک طرف یابی</p></div>
<div class="m2"><p>کرات نه فلک را همچو مرکز در میان بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جهانی کاندر آن خود را ز هر علت بری یابی</p></div>
<div class="m2"><p>جهانی کاندر آن خود را ز هر تهمت امان بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهانی کاندر آن گر سنگ یابی لعل و زر یابی</p></div>
<div class="m2"><p>جهانی کاندر آن گر خار بینی گلستان بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به هر وادی که بخرامی سراسر کام دل یابی</p></div>
<div class="m2"><p>به هر موضع که بنشینی همه آرام جان بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه یک کس را در آن کشور غمی برگرد دل یابی</p></div>
<div class="m2"><p>نه یک دل را در آن وادی جز از شادی تپان بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه منت بر کسی باشد نه ممنون کسی باشی</p></div>
<div class="m2"><p>در آن هر دل که بینی چون دل خود شادمان بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گهی قد و سیانت میزبان کام دل باشند</p></div>
<div class="m2"><p>گهی بر خان خود روحانیان را میهمان بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم ارواح مقدس را به عشرت میزبان باشی</p></div>
<div class="m2"><p>هم اشخاص مجرد را به عزت میزبان بینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو یادآری از آن منزل که بودی روز چندانی</p></div>
<div class="m2"><p>به رحم آید دلت از هر که شاه و شهربان بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وزین مهمانی و مهمان نوازی‌های این مردم</p></div>
<div class="m2"><p>چو یاد آری ز خجلت چون عرق خود را روان بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به مهمانخانه خاص الهی گر شرف یابی</p></div>
<div class="m2"><p>ز بس غیرت نمی‌خواهی که خود را در میان بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نیابی در میان خود را ولی کام ابد یابی</p></div>
<div class="m2"><p>نبینی خویش را لیکن بقای جاودان بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رهی داری در آن عالم نه بس دور و دراز آنجا</p></div>
<div class="m2"><p>نمی‌خواهی در این عالم که سود خود زیان بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی پا بر سر تن نه که راه جان پدید آید</p></div>
<div class="m2"><p>به زیر پی درآور نفس تا رخسار جان بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به یک گامی که برداری به نزل می‌رسی لیکن</p></div>
<div class="m2"><p>ضرورست اینکه پیش از راه از منزل نشان بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشان شرعست و دیدن علم، رفتن ترک خود کردن</p></div>
<div class="m2"><p>چو کردی ترک خود خود را در آن عالم نشان بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدون شرع و دانش گر بدین ره پی سپر گردی</p></div>
<div class="m2"><p>همه دزدان دین یابی همه غولان جان بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عمل بی علم نبود جز به کام اژدها رفتن</p></div>
<div class="m2"><p>چو بی دانش بود اعمال اژدرها دمان بینی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ولیکن علم را هم بی عمل کامی روا نبود</p></div>
<div class="m2"><p>چه سان بینی شجاعی را که بی تیغ و سنان بینی؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چه تیراندازیی آید از آن پر لاف تیرافکن</p></div>
<div class="m2"><p>که لافش در میان اما نه تیرش در کمان بینی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به قدر آنچه می‌دانی عمل کن دانه می‌افشان</p></div>
<div class="m2"><p>که کشت این جهانی حاصلش را آن جهان بینی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ملک از گفته می‌زاید بهشت از کرده می‌خیزد</p></div>
<div class="m2"><p>قصور و حور و غلمان کشته دست و زبان بینی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شراب ارغوانی دانش و آثار آن یابی</p></div>
<div class="m2"><p>طعام جاودانی نیت و اخلاص آن بینی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عمل تنها نمازو روزه نبود مرد معنی را</p></div>
<div class="m2"><p>که این را حرز جان دانی و آنرا حفظ نان بینی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز حج آوازه خواهی وز غزا نام ونشان جویی</p></div>
<div class="m2"><p>زکاتی گردهی یک عمر منت را ضمان بینی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عمل باشد تقرب جستن و فرمانبری کردن</p></div>
<div class="m2"><p>به هر کاری که از دانش رضای حق در آن بینی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو با دست آنچه می‌کاری به چشمش آبیاری کن</p></div>
<div class="m2"><p>که بی‌آب ار فشانی دانه کشت خود زیان بینی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تواضع کن به مردم با کسان افتادگی پیش آر</p></div>
<div class="m2"><p>که این افتادگی ها را به گردون نردبان بینی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نماز از بهر آن معراج مومن شد که هر ساعت</p></div>
<div class="m2"><p>نهی سر بر زمین و خویش را در آسمان بینی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>برون کن از ولایات دل خود کبر و نخوت را</p></div>
<div class="m2"><p>که با نخوت ملک را همچو دیوی در میان بینی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بران از ملک تن فرماندهان خشم و شهوت را</p></div>
<div class="m2"><p>کز ایشان شعله این نور قدسی را دخان بینی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برانگیزد بخارات هوس چون لجه شهوت</p></div>
<div class="m2"><p>رخ خورشید جان در گرد ظلمت‌ها نهان بینی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو دریای غضب گیرد تلاطم, کشتی دل را</p></div>
<div class="m2"><p>به گرداب تحیر چون دل دزوخ تپان بینی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز اوباش طبیعت آید او از فتنه آشوبی</p></div>
<div class="m2"><p>که ملک سینه ویران‌تر ز حال عاشقان بینی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>وزین صحرانشینان هیولی آید آن جرئت</p></div>
<div class="m2"><p>که در شهر یقین آشوب ترکان گمان بینی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بیا از مادت بگسل تعلق‌های خواهش را</p></div>
<div class="m2"><p>که در بازار محشر جمله صورت در دکان بینی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز ظاهر پی به باطن می‌توان بردن اگر مردی</p></div>
<div class="m2"><p>که برهان را چو دریابی ز قرآن ترجمان بینی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر خواهی ازین یک پله هم برتر توانی شد</p></div>
<div class="m2"><p>که ظاهر در حقیقت عین باطن بی‌گمان بینی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر هم در گذشتی زین منازل منزلی داری</p></div>
<div class="m2"><p>که عنقا گشته طاووس بقا، در آشیان بینی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بهشتی داری و در دوزخی آسوده حیرانم</p></div>
<div class="m2"><p>که زر در خانه در خاک و تو گرد کاروان بینی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بهشتی در حقیقت خویش را دوزخ نمودستی</p></div>
<div class="m2"><p>درین اندیشه بیجا که این یابی و آن بینی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تعلق بگسل از خواهش که از دوزخ امان یابی</p></div>
<div class="m2"><p>نقاب از خود برافکن تا بهشت جاودان بینی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به بوی گل درین دیرینه خارستان چه می‌پویی</p></div>
<div class="m2"><p>سری در جیب بر تا گلستان در گلستان بینی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ازین مطموره فردا و دی گر پا نهی بیرون</p></div>
<div class="m2"><p>ازل را تا ابد یک جا دو طفل توامان بینی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گه آهوی ازل را در چراگاه ابد یابی</p></div>
<div class="m2"><p>گهی گاو زمین در کشتزار آسمان بینی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ازین صحرای وحشت روی نه درکوی جمعیت</p></div>
<div class="m2"><p>که دورافتادگان خویش را یک‌جا ستان بینی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یکی زین ملک خود بینی به ملک بیخودی بگذر</p></div>
<div class="m2"><p>که اینجا هر چه گم کردی در آن وادی نشان بینی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خلیل آسا درآ در آتش عشق و تماشا کن</p></div>
<div class="m2"><p>که خود را هر سر مو همچو شاخ ارغوان بینی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>فریبت داده رنگینی ظاهر چشم دل بگشا</p></div>
<div class="m2"><p>که بر آیینه جان رنگ این ظلمت عیان بینی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مشو مغرور آرایش جلا ده چشم معنی را</p></div>
<div class="m2"><p>که رنگینی ظاهر را نهنگ جان ستان بینی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو ظاهر بین باین شکل و شمایل مانده در حیرت</p></div>
<div class="m2"><p>چه خواهی کرد اگر روزی جمال جاودان بینی!</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>درین بیغوله هستی چه حس از آب و گل زاید</p></div>
<div class="m2"><p>که خود را در هوایش هر زمان آتش فشان بینی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>گذر در مصر معنی کن که در هر کوچه از غیبش</p></div>
<div class="m2"><p>متاع حسن یوسف کاروان در کاروان بینی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز حسن معنوی دان پر توی افتاده بر ظاهر</p></div>
<div class="m2"><p>که چندین حسن آب و رنگ و بو در خاکیان بینی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به رنگ لاله و گل در صفای لعل و گوهر بین</p></div>
<div class="m2"><p>که آب دست استاد طبیعت را روان بینی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز رنگ آمیزی حسن گلستان طبیعت دان</p></div>
<div class="m2"><p>که رنگی بر عذار زاده دریا و کان بینی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>جمال ذاتی نفس نباتی بین و حیوانی</p></div>
<div class="m2"><p>اگر طاووس و طوطی گر گل و گر ضیمران بینی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>جمال نفس نطقی جلوه‌گر می‌بین و حیران شو</p></div>
<div class="m2"><p>چو ناز و عشوه و غنج و دلال دلبران بینی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>وزین یک پرده برتر شو ز حسن نفس کلی دان</p></div>
<div class="m2"><p>که گردش بر فلک یابی و نور از اختران بینی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جمال عقل کلی بر تو ظاهر می‌تواند شد</p></div>
<div class="m2"><p>به چشم عشق اگر در وجد و شوق آسمان بینی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>تامل کن به چشم سر جمال لایزالی را</p></div>
<div class="m2"><p>که عقل کل در آن واله چو عقل مردمان بینی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>همه از پرتو انوار حسن لایزالی دان</p></div>
<div class="m2"><p>اگر در خار گل یابی اگر در جسم جان بینی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نداری چشم معنی بین که در طومار هر خاری</p></div>
<div class="m2"><p>حدیث حسن آن گل داستان در داستان بینی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>تو نتوانی شکستن این طلسم رنگ ظاهر را</p></div>
<div class="m2"><p>مگر در خود ز زور عشق روحانی توان بینی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به نام عشق می‌باشد غریو کوس فتح اینجا</p></div>
<div class="m2"><p>اگر نه مرد عشق آیی درین میدان هوان بینی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو افریدون عشق آید به میدان، هر سر مو را</p></div>
<div class="m2"><p>که بر تن داری از مردی درفش کاویان بینی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مجردوار پا در کارزار نفس نه کانجا</p></div>
<div class="m2"><p>ز عریانی بر اسب غازیان بر گستوان بینی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فروتن شو به قصد سربلندی‌ها که از عزت</p></div>
<div class="m2"><p>درین میدان سر افتادگی بر آسمان بینی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تقدم جو مشو ور اتفاق افتد چنان می‌کن</p></div>
<div class="m2"><p>که گر در صدر باشی خویش را در آستان بینی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سر گردنکشی در خاک می‌کن کاندرین مجلس</p></div>
<div class="m2"><p>همیشه دست رد بر سینة گردنکشان بینی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کدورت‌های دوران را جلای زنگ دل می‌دان</p></div>
<div class="m2"><p>که صاف آیینه از خاکستر روشنگران بینی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زر ناقص عیاری از گدازی نیستت چاره</p></div>
<div class="m2"><p>چه لازم کاین گداز از انفعال امتحان بینی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>غش هستی ببر از نقد خویش امروز اگر خواهی</p></div>
<div class="m2"><p>که فردا چون طلای ده‌دهی خود را روان بینی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>وجود خود چنین کاندر مکان بستی عجب دارم</p></div>
<div class="m2"><p>که در خود بال پرواز فضای لامکان بینی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>پر افشانی نیاری در هوای لازمان کردن</p></div>
<div class="m2"><p>که خود را بال و پر بر بسته در دام زمان بینی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>همای اوج لاهوتند مردان خدا تا کی</p></div>
<div class="m2"><p>تو در ناسوت، نحس چرخ و سعد اختران بینی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>همای عقل بر سر سایه گستر بین چه حالست این</p></div>
<div class="m2"><p>که بهر جغد سودا کاسه سر آشیان بینی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ترا لذات عقلی بهتر از لذات جسمانی</p></div>
<div class="m2"><p>که این را در تغیر یابی آن را جاودان بینی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز راحت‌ها همان بهتر که بی‌رنج و تعب یابی</p></div>
<div class="m2"><p>ز نعمت‌ها همان بهتر که بی‌کام و دهان بینی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>وجود استخوان دایم به کار از بهر مغز آید</p></div>
<div class="m2"><p>چه بدبختی تو بی دانش که از مغز استخوان بینی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ترا در طاعت حق گر نظر بر حور و غلمانست</p></div>
<div class="m2"><p>عجب دارم که حسن آفتاب از فرقدان بینی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>برای قرب شاهانست روی پاسبان دیدن</p></div>
<div class="m2"><p>تو خورسندی ز قرب‌شه که روی پاسبان بینی!</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>به جنت وعده فرمودن ز نقص همت ما دان</p></div>
<div class="m2"><p>که گلخن گلخنی را به زباغ و بوستان بینی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>به قدر همت خود هر کسی اجر عمل یابد</p></div>
<div class="m2"><p>بهشت وحور عین را تا چسان دانی چنان بینی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>ز جنت هر کسی چیزی تصور می‌تواند کرد</p></div>
<div class="m2"><p>یکی قرب و لقا بیند تو لحم طیر و نان بینی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یکی از حور انس و از فواکه بهره دانش</p></div>
<div class="m2"><p>تو آنها جمله را سرمایه فرج ودهان بینی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>قیاس آن ز معراج پیمبر می‌توان کردن</p></div>
<div class="m2"><p>که تو عمامه و ریش و ردا و طیلسان بینی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>نبینی یک حقیقت را هزاران اسم می‌باشد؟</p></div>
<div class="m2"><p>که در مفهوم هر یک اختلافی در میان بینی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>به قدر فهم ها شد اختلاف نام ها زانست</p></div>
<div class="m2"><p>که در قران گهی بر آسمان نام دخان بینی</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>به ما بر منت جانست ایزد زا زنان دادن</p></div>
<div class="m2"><p>حکیمش روح حیوانی و تو بر سفره نان بینی</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به نزد حکمت اندیشان معنی در لسان شرع</p></div>
<div class="m2"><p>تجرد را به عریانی محشر ترجمان بینی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>سبکروحی بود در کار پرواز تجرد را</p></div>
<div class="m2"><p>ترا مشکل که بار خود ازین ارکان گران بینی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>اگر بال خود از گرد چهار ارکان بیفشانی</p></div>
<div class="m2"><p>به شاخ سدره خود را چون ملایک آشیان بینی</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چرا دامن ز ارکان و ز آثارش نیفشانی</p></div>
<div class="m2"><p>کز ارکان است اغلالی که هم بر کافران بینی</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ترا فیاض، پرآشفته دیدم دل غمین گشتم</p></div>
<div class="m2"><p>ازین مشتی نصیحت گونه کز دل بر زبان بینی</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>نه و عظم بود مطلب نه نصیحت زین زین نواسنجی</p></div>
<div class="m2"><p>ولی در دل چو دردی هست بر لب هم فغان بینی</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>تو این الفاظ را دودی شناس از آتش معنی</p></div>
<div class="m2"><p>چو اتش در سرا پنهان بود دودش عیان بینی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>ترا خود نیست آن حالت که جز محسوس دریابی</p></div>
<div class="m2"><p>وگرنه دایم این خون از بن هر مو روان بینی</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>مگو واعظ، که واعظ را نفس افسرده می‌باشد</p></div>
<div class="m2"><p>کسی کاغشته در خونش سراپا از بیان بینی</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>مخوانش ناصح، آن از دفتر دل نکته پردازی</p></div>
<div class="m2"><p>که نوک خامه‌اش چون تارمژگان خون فشان بینی</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به تحریک حکیم غزنوی این ناله‌ها کردم</p></div>
<div class="m2"><p>به آهنگی که مرغی را به مرغی همزبان بینی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>خموشی این زمان فرض است دانشمند معنی را</p></div>
<div class="m2"><p>که گر چون جان سبک گوید تو چون جسمش گران بینی</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>مرا این گفتگو با مردگان زنده دل باشد</p></div>
<div class="m2"><p>وزین مرده دلان زنده مهرم بر دهان بینی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>به بی دردی قیاس دردمندی‌ها چنان باشد</p></div>
<div class="m2"><p>که خواهی حال طوفان دیدگان را در کران بینی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>قیاس می‌کنی با حال خود احوال مردم را</p></div>
<div class="m2"><p>تو در آیینه می‌‌بینی که عالم گلستان بینی</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>تو خود کاسوده‌ای آشوب عالم را چه می‌دانی</p></div>
<div class="m2"><p>به حال من ببین تا فتنه آخر زمان بینی</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>غزل خواهی دو مصرع از دو بیت خود کنم مطلع</p></div>
<div class="m2"><p>که روح بلبل شیراز را هم شادمان بینی</p></div></div>