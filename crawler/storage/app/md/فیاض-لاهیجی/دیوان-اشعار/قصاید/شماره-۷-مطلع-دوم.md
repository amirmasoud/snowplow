---
title: >-
    شمارهٔ ۷ - مطلع دوم
---
# شمارهٔ ۷ - مطلع دوم

<div class="b" id="bn1"><div class="m1"><p>پیش قصر قدرت افکنده ز ناخوش منظری</p></div>
<div class="m2"><p>چون بنفشه سر به پیش این گنبد نیلوفری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این مقرنس طاق والا پشت از ان خم کرده است</p></div>
<div class="m2"><p>تا ببوسد استانت را به رسم چاکری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مسدس کلبه این شش جهت جای تو نیست</p></div>
<div class="m2"><p>می کند ذات تو در دریای دیگر گوهری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان سوراخ سوراخ است دایم چون دلم</p></div>
<div class="m2"><p>محفل قدر ترا در آرزوی مجمری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت بودی گر نبودی ذات پاکت در میان</p></div>
<div class="m2"><p>این که مبدا مبدایی کردی و مصدر مصدری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ریگ دشتت آب می بخشد به لولوی عدن</p></div>
<div class="m2"><p>خار راهت باج می گیرد ز گلبرگ طری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش دستت بحر چون گرداب می دزدد نفس</p></div>
<div class="m2"><p>پیش جودت می زند ابر از خجالت برتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه پیدا زان نباشد جسم پاکت را که هست</p></div>
<div class="m2"><p>سایه ات پر نورتر از نور شمع خاوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا قدم بر تارک افلاک سودی می کند</p></div>
<div class="m2"><p>خاک پایت تا ابد بر فرق گردون افسری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کار یک انگشت اعجازت بود شق القمر</p></div>
<div class="m2"><p>شمه ای از کار معراجت بود گردون دری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرق گردون ممتنع داند اگر نادان چه باک</p></div>
<div class="m2"><p>ممتنع باید که یابد کار معجز برتری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من به برهان می کنم اثبات این مطلب درست</p></div>
<div class="m2"><p>تا نپندارد کسی کاین هست محض شاعری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست ظن امتناع ذاتی اینجا جهل وبس</p></div>
<div class="m2"><p>خرق از اعدام آسان تر بود چون بنگری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منع عادت هست و معجزعین خرق عادتست</p></div>
<div class="m2"><p>کی توان بی خرق عادت دعوی پیغمبری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هست در حکمت بلی خرق محدد ممتنع</p></div>
<div class="m2"><p>لیک غیر از سطح اطلس را محدد نشمری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در شب معراج از اطلس فزون تر کس نگفت</p></div>
<div class="m2"><p>از احادیث شب معراج دانم مخبری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا رسول الله خیرالمرسلین ختم الرسل</p></div>
<div class="m2"><p>ای که در وصف تو حیران می شود عقل حری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من به قدر فهم خود وصف جلالت می کنم</p></div>
<div class="m2"><p>ورنه می دانم به قدر از عرض دانش برتری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من یکی از بندگان خدمت دور توام</p></div>
<div class="m2"><p>گرچه تقصیرات دارم من درین خدمت گری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه جرم بی حسابم هست لیکن در حساب</p></div>
<div class="m2"><p>پیش عفوت برگ کاهست و نسیم صرصری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تاب دوری بیش ازینم نیست از درگاه تو</p></div>
<div class="m2"><p>بی لیاقت گر به نزدیکم رسانی قادری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دوستدار اهل بیت و عترت پاک توام</p></div>
<div class="m2"><p>دیگری لایق ندانم در سری و سروری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عترت پاکت مرا تا بر سرند و سرورند</p></div>
<div class="m2"><p>حاش لله گر پسندم دیگری در چاکری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که او بی مهر عترت لاف ایمان می زند</p></div>
<div class="m2"><p>پیش من فرقی ندارد از جهود خیبری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لاف عشق و عاشقی فیاض و پس صبر و شکیب</p></div>
<div class="m2"><p>عشق اهل بیت اگرداری ز حسرت خون گری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عاشقی و دوری از معشوق بی تابی کجاست</p></div>
<div class="m2"><p>عاشقان را صبر از معشوق باشد کافری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تالب خشک است عاشق را نصیب و چشم تر</p></div>
<div class="m2"><p>دست عاشق تا ندارد صبر پیراهن دری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دست بی تابی مبادا کوته از جیب دلم</p></div>
<div class="m2"><p>در محبت کم مبادا یکدم از چشمم تری</p></div></div>