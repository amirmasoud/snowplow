---
title: >-
    شمارهٔ ۵ - ترکیب‌بند در عشق عرفانی
---
# شمارهٔ ۵ - ترکیب‌بند در عشق عرفانی

<div class="b" id="bn1"><div class="m1"><p>ای دل بیا که دست ردی بر جهان زنیم</p></div>
<div class="m2"><p>سنگ از زمین کنیم و به فرق زمان زنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن نگشت از ورق خور سواد کس</p></div>
<div class="m2"><p>این لوح ساده را به سر آسمان زنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم مست ساقی جامی طلب کنیم</p></div>
<div class="m2"><p>چون شیشه خنده بر می چون ارغوان زنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین بار هستیی که گرانی کند به ما</p></div>
<div class="m2"><p>خود را سبک کنیم و به رطل‌ گران زنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر ز دور گردون چون پیر می‌شویم</p></div>
<div class="m2"><p>حیف است پشت پای به بخت جوان زنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک سر سری به بحر تفکّر فرو بریم</p></div>
<div class="m2"><p>و آنگه زنخ به مایة دریا و کان زنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آریم تازه تازه برون گوهر سخن</p></div>
<div class="m2"><p>هر چند بی‌بهاست، در رایگان زنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اول به عشوه از دو جهان دلبری کنیم</p></div>
<div class="m2"><p>وآنگه چو چشم یار به صف‌های جان زنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از یمن ضرب ناخن تأثیر فکرها</p></div>
<div class="m2"><p>بس سکّة خراش به نقد روان زنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با بخت بد ستیزه اگر رو دهد دلیر</p></div>
<div class="m2"><p>خود را به قلب لشکر هندوستان زنیم</p></div></div>
<div class="b2" id="bn11"><p>از آستین همّت گردون‌نورد خویش</p>
<p>دستی برون کنیم و بجوییم مرد خویش</p></div>
<div class="b" id="bn12"><div class="m1"><p>جانم ز غصّه‌های جهان دردپرورست</p></div>
<div class="m2"><p>دل از غبار کینة دوران مکدّرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کو روی تازه‌ای که برآید به کام دل</p></div>
<div class="m2"><p>کاین آفتاب هرزه در ابر مکرّرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاید گر التفات کند لطف دلبری</p></div>
<div class="m2"><p>کش آفتاب، عاشقِ از ذرّه کمترست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شوخی که ظاهرست ز لعلش معاینه</p></div>
<div class="m2"><p>کان نمک که تعبیه در تنگ شکّرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مستی که در ستیزه‌گه تُرکِ غمزه‌اش</p></div>
<div class="m2"><p>صد فتنه در شکنجة زلف معنبرست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون تیغ ناز برکشدش غمزه از نیام</p></div>
<div class="m2"><p>بر هر طرف نگاه کنی جلوة سرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در قتل عاشقانش کجا فکرِ خونبهاست</p></div>
<div class="m2"><p>خاک درش به خون شهیدان برابرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در جلوه‌گاه غمزة او از خدنگ ناز</p></div>
<div class="m2"><p>هر آرزو که می‌طلبد دل، میسّرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با یاد زمزم لب حسرت‌فزای تو</p></div>
<div class="m2"><p>تا حشر آب در دهن حوض کوثرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در عرض گاه جلوة حسن آفتاب را</p></div>
<div class="m2"><p>بر رخ خراش ناخن او سکّة زرست</p></div></div>
<div class="b2" id="bn22"><p>طفل است و خاطری نتواند نگاه داشت</p>
<p>دل می‌برد ولیک نداند نگاه داشت</p></div>
<div class="b" id="bn23"><div class="m1"><p>ماهی که آفتاب ندیدست روی او</p></div>
<div class="m2"><p>چون غنچه باد هم نشنیدست بوی او</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از مثل او سخن مکن ای دل که این سخن</p></div>
<div class="m2"><p>آیینه هم نیارد گفتن به روی او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رویی چنانکه گویی مشّاطه می‌کند</p></div>
<div class="m2"><p>هر صبحدم به چشمه خور شستشوی او</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خورشید را به او نرسد لاف همسری</p></div>
<div class="m2"><p>کاین شیشه‌ایست پر شده هم از سبوی او</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گل پاره پاره شد ز غم رشگِ تازه، باز</p></div>
<div class="m2"><p>از بلبلان شنیده مگر گفت‌وگوی او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر در جهان به هم نرسد مثل او دگر</p></div>
<div class="m2"><p>این آفتاب هرزه کند جستجوی او</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وصلش به آروز به کسی کی رسد که هست</p></div>
<div class="m2"><p>صد خون دل به گردن هر آرزوی او</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای برهمن به کیش بت خود چنان مناز</p></div>
<div class="m2"><p>زنّار بسته است مگر پیش موی او</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرغ فگار، زحمت بال و پرت مده</p></div>
<div class="m2"><p>بر گرد او نمی‌رسی از بیم خوی او</p></div></div>
<div class="b2" id="bn32"><p>از نشئه دم زند چو لب او شراب چیست!</p>
<p>وز رخ چو پرده برفکند آفتاب چیست!</p></div>
<div class="b" id="bn33"><div class="m1"><p>ای ملک دل مسخّر روی چو ماه تو</p></div>
<div class="m2"><p>خورشید سایه‌پرور زلف سیاه تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صف‌های دل شکستی و این طرفه‌تر که هست</p></div>
<div class="m2"><p>حسن ترا ظفر ز شکستِ کلاه تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خورشید عاشقی کندش تا به روز حشر</p></div>
<div class="m2"><p>هر ذرّه کو بلند شد از جلوه‌گاه تو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر گه به عزم جلوه برون تازی آفتاب</p></div>
<div class="m2"><p>خود را ضعیف سازد کافتد به راه تو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دست از ستم مدار که فردا به روز حشر</p></div>
<div class="m2"><p>در گردن شهید تو باشد گناه تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طفلیّ و صرفه‌ای نبرد از تو آفتاب</p></div>
<div class="m2"><p>چون چارده شوی که برآید به ماه تو</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دامن نگیردت به جزا خون هیچ‌کس</p></div>
<div class="m2"><p>گر این نگاه گرم بود عذرخواه تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>خورشید تیغ بسته ز یک گوشه سر زند</p></div>
<div class="m2"><p>حسن تو چون دهد به تو عرض سپاه تو</p></div></div>
<div class="b2" id="bn41"><p>همتای تو به عالم بالا و پست نیست</p>
<p>مثل تو در بهشت ندانم که هست؟ نیست</p></div>
<div class="b" id="bn42"><div class="m1"><p>خورشید اگر بود رخ بی‌شرم بی‌صفاست</p></div>
<div class="m2"><p>شبنم گل‌عذار بتان را خوی حیاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای غنچه آبروی حیا را مده به باد</p></div>
<div class="m2"><p>رنگ گل نکویی از آب رخ حیاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در گلستان روی تو ای نوبهار حسن</p></div>
<div class="m2"><p>چیزی که آب و رنگ ندارد گل وفاست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ما را نه دل به جا و نه دین از تو و همان</p></div>
<div class="m2"><p>نازت به جا کرشمه به جا سرکشی به‌جاست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>من می‌کشم جفای تو تا زنده‌ام ولیک</p></div>
<div class="m2"><p>در مردنم خلاصی ازین درد و غم کجاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو شیرخواره بودی و من بودم آشنات</p></div>
<div class="m2"><p>خونخواره چون شدی؟ همه بیگانگی چراست؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خون می‌خورم زدست تو ای طفل شیرخوار</p></div>
<div class="m2"><p>شیرت به خون بدل چو شود قسمتم چهاست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تیری است از تو دربن هر موی و شکوه نیست</p></div>
<div class="m2"><p>خود گو که تاب جور تو زین بیشتر کراست؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر روز از تو آب رخی می‌رود به باد</p></div>
<div class="m2"><p>اینها سزای آن که به شبها غم تو خواست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پا از طلب نمی‌کشم اما نمی‌شود</p></div>
<div class="m2"><p>کار دل شکسته ز زلف کج تو راست</p></div></div>
<div class="b2" id="bn52"><p>این زخم خون‌چکان که دلم تازه خورده است</p>
<p>چون آب روشن است که تیغ تو کرده است</p></div>
<div class="b" id="bn53"><div class="m1"><p>هر شامگه که جلوه نماید جمال تو</p></div>
<div class="m2"><p>خور در زمین فرو رود از انفعال تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چشم از تو برنداشته‌ام در تمام عمر</p></div>
<div class="m2"><p>یا خود تو بوده‌ای به نظر، یا خیال تو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در حشر سرخ‌رویی اهل گنه ازوست</p></div>
<div class="m2"><p>هر خون عاشقی که شود پایمال تو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>با غیر باده می‌ خوری ای مه چه می‌کنی!</p></div>
<div class="m2"><p>خونی که خورده‌ای نکنم گر حلال تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>طفلی هنوز صرفة نازی نگاه‌دار</p></div>
<div class="m2"><p>بگذار تا که بدر برآید هلال تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>دیروز ماه بودی و امروز آفتاب</p></div>
<div class="m2"><p>بهتر ز یکدگر گذرد ماه و سال تو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بارش گل تجلّی و بر آتش کلیم</p></div>
<div class="m2"><p>نسبت به نخل طور رساند نهال تو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حسن از تو دست باز ندارد که دیده است</p></div>
<div class="m2"><p>فال سعادت از رخ فرخنده فال تو</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در عهد خوبرویی تو آفتاب و ماه</p></div>
<div class="m2"><p>سوگند می‌خورند به جاه و جلال تو</p></div></div>
<div class="b2" id="bn62"><p>جانا ستیزة تو ندارد نهایتی</p>
<p>جور و جفا خوش است ولی تا به غایتی</p></div>
<div class="b" id="bn63"><div class="m1"><p>روز سیاه بنگر و شب‌های تار من</p></div>
<div class="m2"><p>اینست بی‌تو روز من و روزگار من</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از سینه‌ام چو شعلة فانوس روشن است</p></div>
<div class="m2"><p>سوز نهان من ز دل بی‌قرار من</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون آستان فتادة این درگهم که هست</p></div>
<div class="m2"><p>خاک در تو آبِ رخ اعتبار من</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>یک‌ دم نشین به ناز به پیشم که تا نهد</p></div>
<div class="m2"><p>دوران جزای صبر مرا در کنار من</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>بگشا به خنده آن لب شیرین چه می‌شود</p></div>
<div class="m2"><p>گر غنچه‌ای شکفته شود از بهار من؟</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای خوبتر ز پارِ تو امسال، از چه روست</p></div>
<div class="m2"><p>امسال من ز عشق تو بدتر ز پار من؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از خاک درگه تو چو دورم کند به زور</p></div>
<div class="m2"><p>بخت زبون و طالع ناسازگار من</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شادم ازین که بر در و دیوار کوی تو</p></div>
<div class="m2"><p>مانَد ز خون دیدة من یادگار من</p></div></div>
<div class="b2" id="bn71"><p>سیر از جفا نشد نگه عشوه‌ساز تو</p>
<p>از جوی زخم آب خورد تیغ ناز تو</p></div>
<div class="b" id="bn72"><div class="m1"><p>کی یوسف این طراوت و روی چو ماه داشت</p></div>
<div class="m2"><p>این شیوة تبسّم و طرز نگاه داشت</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>این ناز و این کرشمه و این چشم و این نگاه</p></div>
<div class="m2"><p>در عهد تو چه‌گونه توان دل نگاه داشت!</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>من کوه کندم از مژه در عاشقیّ و تو</p></div>
<div class="m2"><p>لیکن کجا به پیش تو مقدار کاه داشت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شد کور دیده بی‌تو مرا، یاد آن به‌خیر</p></div>
<div class="m2"><p>کاین چشم توتیائی از آن خاک راه داشت</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عشّاق بوده‌اند ولی کس چو من کجا</p></div>
<div class="m2"><p>این روزگار تیره و بخت سیاه داشت؟</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>صد بار سوختیّ و تسلّی نمی‌شوی</p></div>
<div class="m2"><p>مسکین دلم به کیش تو چندین گناه داشت</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>هرگز کسی ندیده رخت بی‌نقاب زلف</p></div>
<div class="m2"><p>خورشید من همیشه به سایه پناه داشت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چشمت به غمزه ریخت اگر خون عاشقان</p></div>
<div class="m2"><p>از هر نگاه گرم تو صد عذرخواه داشت</p></div></div>
<div class="b2" id="bn80"><p>ناید به هم ز ذوق لب خون‌چکان زخم</p>
<p>تا خنجر تو کرد زبان در دهان زخم</p></div>
<div class="b" id="bn81"><div class="m1"><p>ای آفتاب را ز درت چشمِ توتیا</p></div>
<div class="m2"><p>در کوچة تو سرمه‌فروشی کند صبا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر ذرّه آفتابِ دگر زآستان تو</p></div>
<div class="m2"><p>صد آفتاب بر سر کوی تو خاک پا</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کفر از شکست زلف تو اسلام را شکست</p></div>
<div class="m2"><p>نوعی که سبحه از غم زنّار شد دوتا</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز آشوب جادوی سر زلف ترا بود</p></div>
<div class="m2"><p>صد فتنه دست بسته به هر حلقه مبتلا</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از آرزوی دیدن روی تو آسمان</p></div>
<div class="m2"><p>هر صبح آورد زر خورشید رونما</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>دل را به بند خانة تاریک زلف تو</p></div>
<div class="m2"><p>هر حلقه‌ای ازوست یک روزن بلا</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دور و درازی ره زلف توام گداخت</p></div>
<div class="m2"><p>صد عمر طی شد و نپذیرفت انتها</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گرمی مکن به خار و خس شعله بیش ازین</p></div>
<div class="m2"><p>ترسم که چون سپند جهانی ز خود مرا</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>دستم به دامنت نرسد لیک خون من</p></div>
<div class="m2"><p>در حشر هم عجب که کند دامنت رها</p></div></div>
<div class="b2" id="bn90"><p>چشم تو از نگه چو مرا منفعل کند</p>
<p>خون مرا چو آب به تیغت بحل کند</p></div>
<div class="b" id="bn91"><div class="m1"><p>پنهان چه‌سان کنم که دلم مهربان تست</p></div>
<div class="m2"><p>این پیچشم تمام ز موی میان تست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>یک دل به سینه دارم و چندین هزار کام</p></div>
<div class="m2"><p>این‌ها همه به عهدة لطف نهان تست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کس ناز را به خوبی آن ابروان نکرد</p></div>
<div class="m2"><p>تیری است این‌که در خور زور کمان تست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>من تشنه‌لب چه‌گونه نمیرم که لعل ناب</p></div>
<div class="m2"><p>سیراب حسرت لب گوهرفشان تست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>تیر قضا که بر سر کس بی‌گمان رسد</p></div>
<div class="m2"><p>تفسیر ناوک نگه ناگهان تست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>از صد یکی به جای نیاری یقین ماست</p></div>
<div class="m2"><p>آن وعده‌ای که با دل ما در گمان تست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یک بلبلی به باغ تو نگذارد از ستم</p></div>
<div class="m2"><p>این ناز تندخوی تو گر باغبان تست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>کار گشایش دل تنگ حزین من</p></div>
<div class="m2"><p>در بند یک گشادگی ابروان تست</p></div></div>
<div class="b2" id="bn99"><p>از شکوه بس کنم که دل یار نازکست</p>
<p>خوی کرشمه نازک و بسیار نازکست</p></div>