---
title: >-
    شمارهٔ ۶ - قصیدهٔ ترجیع در عشق
---
# شمارهٔ ۶ - قصیدهٔ ترجیع در عشق

<div class="b" id="bn1"><div class="m1"><p>بازم سر زلفِ چون کمندی</p></div>
<div class="m2"><p>از هر سویی نهاد بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد کاسة زهر در گلو ریخت</p></div>
<div class="m2"><p>بازم لب لعل نوشخندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آتشِ آهِ سرکشم سوخت</p></div>
<div class="m2"><p>اقبال ستاره‌ام سپندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشوبِ نگاهِ جادوی تاخت</p></div>
<div class="m2"><p>بر عرصة طاقتم سمندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پیچش تار زلفی آمد</p></div>
<div class="m2"><p>بر مهرة دل مرا گزندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم‌سنگ‌ترم نموده کاهش</p></div>
<div class="m2"><p>از پیکر صورت پرندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیروزترم گرفته خواهش</p></div>
<div class="m2"><p>از پشه به چنگ فیل بندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون قافیه تنگ گشت کارم</p></div>
<div class="m2"><p>از مصرع قامت بلندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با دستگهی که ناز دارد</p></div>
<div class="m2"><p>چون صبر کند نیازمندی!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیوانگیم بهانه‌جو شد</p></div>
<div class="m2"><p>ای ناصح هرزه‌گوی پندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوشیده به عشق برنیاید</p></div>
<div class="m2"><p>زو از دل خسته صبر کَندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون دسترسم به کام دل نیست</p></div>
<div class="m2"><p>من نیز بر آن سرم که چندی</p></div></div>
<div class="b2" id="bn13"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn14"><div class="m1"><p>می‌کوشم و کوششم به‌جا نیست</p></div>
<div class="m2"><p>می‌گریم و گریه‌ام روا نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیگانة روزگار خویشم</p></div>
<div class="m2"><p>در هیچ دیارم آشنا نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تأثیر چه‌سان کند در آن دل</p></div>
<div class="m2"><p>این خاصیتی که با دعا نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر یار آمد کجا نشیند</p></div>
<div class="m2"><p>کز عشق ویم به سینه جا نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آسان آسان کجا توان یافت</p></div>
<div class="m2"><p>این جنس وفاست، کیمیا نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>من بی‌او یک نفس نبودم</p></div>
<div class="m2"><p>او با من یک نفس چرا نیست!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ظاهر دوریم لیک پنهان</p></div>
<div class="m2"><p>او از دل و دل ازو جدا نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خوبان دل ما به زور بردند</p></div>
<div class="m2"><p>در دل دادن گناه ما نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لوح از خط آرزوی شستیم</p></div>
<div class="m2"><p>در دل حرفی ز مدّعا نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفتی در دل ز من چه داری؟</p></div>
<div class="m2"><p>در دل ز توام بگو چه‌ها نیست؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در دل زتوام هزار کام است</p></div>
<div class="m2"><p>لیکن چو یکی از آن روا نیست</p></div></div>
<div class="b2" id="bn25"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn26"><div class="m1"><p>بازم غم عشق در سر افتاد</p></div>
<div class="m2"><p>دل با هوس غمی درافتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از سرزنشم گذشت بالین</p></div>
<div class="m2"><p>خار و خسکم به بستر افتاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غم در دل آتشی برافروخت</p></div>
<div class="m2"><p>کاتش به دل سمندر افتاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کردند وداع هم دل و دین</p></div>
<div class="m2"><p>چشمم به کدام کافر افتاد؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زنّارم دست در کمر کرد</p></div>
<div class="m2"><p>تسبیح به دست و پا درافتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آزادی را که صید ما بود</p></div>
<div class="m2"><p>دیدار به روز محشر افتاد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بی‌تابی را که مرغ رامست</p></div>
<div class="m2"><p>هم بال شکست و هم پر افتاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آسایش من چو وعدة یار</p></div>
<div class="m2"><p>هر روز به روز دیگر افتاد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خواب خوشم از مژه هراسد</p></div>
<div class="m2"><p>آری گذرش به نشتر افتاد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پوشیدن غم چه سود دارد؟</p></div>
<div class="m2"><p>چون پرده ز کار من برافتاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کامی نشود به سعی حاصل</p></div>
<div class="m2"><p>این تجربه خود مکرّر افتاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گردون به مراد کس نزد گام</p></div>
<div class="m2"><p>با او چو نمی‌توان درافتاد</p></div></div>
<div class="b2" id="bn38"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn39"><div class="m1"><p>عشق از سر زلفت ای دلارام</p></div>
<div class="m2"><p>بر هر طرفم فکنده صد دام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از نشئة زهر چشمت ای شوخ</p></div>
<div class="m2"><p>تلخست همیشه کام بادام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عیشم تلخ است از آنکه هرگز</p></div>
<div class="m2"><p>شیرین نکنی لبی به دشنام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با لعل تو غنچه را چه یارا</p></div>
<div class="m2"><p>با قد تو سرو را چه اندام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در پیرهن تو برگ گل خار</p></div>
<div class="m2"><p>در انجمن تو شمع بدنام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نتواند کرد در روش کبک</p></div>
<div class="m2"><p>همراهی جلوة تو یک گام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در عشق تو دل چون مرغ بسمل</p></div>
<div class="m2"><p>تا جان ندهد نگیرد آرام</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز آغاز محبّت تو پیداست</p></div>
<div class="m2"><p>کاین شغل نمی‌رسد به انجام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گل غنچه کند دهن که خواهد</p></div>
<div class="m2"><p>بوسد دهن ترا به پیغام</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پر وا نکنی به صید و ترسم</p></div>
<div class="m2"><p>بر مرغ دل آشیان شود دام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گفتم کنم از تو کام حاصل</p></div>
<div class="m2"><p>یا در سر ننگِ دل کنم نام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون کام نشد میّسر از تو</p></div>
<div class="m2"><p>من‌بعد بر آن سرم که ناکام</p></div></div>
<div class="b2" id="bn51"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn52"><div class="m1"><p>ای بزم طرب حرام بی‌تو</p></div>
<div class="m2"><p>عیشم همه ناتمام بی‌تو</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با گشتِ چمن چه کار ما را</p></div>
<div class="m2"><p>گل را نبریم نام بی‌تو</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تنها نه دلست بی‌سرانجام</p></div>
<div class="m2"><p>هر پختة ماست خام بی‌تو</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>با یاد تو نشئه‌ایست امّا</p></div>
<div class="m2"><p>آن نشئه به ما حرام بی‌تو</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر عیش که با تو کرده‌ام خرج</p></div>
<div class="m2"><p>از من کشد انتقام بی‌تو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در جلوة کبک نشئه‌ای نیست</p></div>
<div class="m2"><p>رفت از یادش خرام بی‌تو</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گل بوی نکرد در گلستان</p></div>
<div class="m2"><p>بلبل دارد زکام بی‌تو</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چشمی دارد لباب از خون</p></div>
<div class="m2"><p>در مجلس عیش، جام بی‌تو</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چون کشتیِ سر به باد داده</p></div>
<div class="m2"><p>یک‌جا نکنم مقام بی‌تو</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گفتم ز تو کام دل برآید</p></div>
<div class="m2"><p>حاصل چو نگشت کام بی‌تو</p></div></div>
<div class="b2" id="bn62"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn63"><div class="m1"><p>ما را نسبی است تا به آدم</p></div>
<div class="m2"><p>هر نطفه خمیرمایة غم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بی‌عشق نرفته‌ایم یک گام</p></div>
<div class="m2"><p>بی‌درد نبوده‌ایم یک‌ دم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا چند فرو خورم غم دل</p></div>
<div class="m2"><p>کو آه که سر نهد به عالم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بی‌گریه چو طفل کم کنم خواب</p></div>
<div class="m2"><p>بی‌ناله چو شیشه کم زنم دم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>آه است دوای عاشق تو</p></div>
<div class="m2"><p>باد است علاج آتش کم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>عشق از دو دلست آتش‌افروز</p></div>
<div class="m2"><p>گل از غم بلبل است درهم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پروانه و شمع هر دو سوزند</p></div>
<div class="m2"><p>پروانه تمام، شمع کم‌کم</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بر روی تو خوی عقیق‌فام است</p></div>
<div class="m2"><p>چون برگل و لاله قطرة نم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شادابی گل نگر که رنگش</p></div>
<div class="m2"><p>آتش شده در نهادِ شبنم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>لطف تو فزاید آتش دل</p></div>
<div class="m2"><p>غمخواری تست مایة غم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>می‌خواست که زود به نگردد</p></div>
<div class="m2"><p>دلسوزی داغ کرد مرهم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>کام از تو گرفتن است مشکل</p></div>
<div class="m2"><p>تو کام نمی‌دهی و من هم</p></div></div>
<div class="b2" id="bn75"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn76"><div class="m1"><p>زان موی میان و زلف تاریک</p></div>
<div class="m2"><p>از غصّه شدم چو موی باریک</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کس رازِ میانِ او نداند</p></div>
<div class="m2"><p>زآنروی که نکته‌ایست باریک</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زلفین ترا به دل ربودن</p></div>
<div class="m2"><p>پیوسته کند نسیم تحریک</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>با عشق تو قرب و بعد یکسان</p></div>
<div class="m2"><p>تابد خورشید دور و نزدیک</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>چشمان تو با دلم نسازند</p></div>
<div class="m2"><p>جنگ است میان ترک و تاجیک</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>با آن که در آبِ دیده‌ام غرق</p></div>
<div class="m2"><p>دل بر سر آتش است چون دیگ</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>داغی که تو سوختی به جانم</p></div>
<div class="m2"><p>از مرهم کس نمی‌شود نیک</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>مهرم که یقینِ تست دانم</p></div>
<div class="m2"><p>هرگز نکند قبولِ تشکیک</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>زلف تو به روز من نشسته است</p></div>
<div class="m2"><p>در پهلوی آفتاب تاریک</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>در حبل متین زلفِ اوزن</p></div>
<div class="m2"><p>ای دل، دستی که سوف یهدیک</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>جمعی با من شریکِ کامند</p></div>
<div class="m2"><p>افزون‌تر از شمارة ریگ</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>خواهم که به گوشه‌ای ازین پس</p></div>
<div class="m2"><p>بی‌یاد شریک و بیم تشریک</p></div></div>
<div class="b2" id="bn88"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn89"><div class="m1"><p>تا با آیینه روی با روست</p></div>
<div class="m2"><p>او آیینه است و آینه اوست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>از پشتی رخ خطش زند لاف</p></div>
<div class="m2"><p>طوطی پس آینه سخنگوست</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>با عشق به عالمم فراغ است</p></div>
<div class="m2"><p>این سنگم بس که در ترازوست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>جا در سر زلف یار دارم</p></div>
<div class="m2"><p>از من تا دوست یک سر موست</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>کی وا شود این گره ز کارم؟</p></div>
<div class="m2"><p>تا نازِ ترا گره بر ابروست</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>پیدا باشد چو گل نهانم</p></div>
<div class="m2"><p>چون غنچه نیم که توی بر توست</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>آبم که ز پاک طینتی نیست</p></div>
<div class="m2"><p>چون شیشه حجاب مغز من پوست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>عجزست که آفتی ندارد</p></div>
<div class="m2"><p>فرهاد شهید زور بازوست</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>هر درد دلی که از تو باشد</p></div>
<div class="m2"><p>عیش است که عیش‌ها غم اوست</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>این بازی‌ها که با سرم کرد</p></div>
<div class="m2"><p>من‌بعد سرم به‌راه زانوست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>من دوست برای کام جستم</p></div>
<div class="m2"><p>چون کام دلم نمی‌دهد دوست</p></div></div>
<div class="b2" id="bn100"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn101"><div class="m1"><p>خورشیدم و تیره روزگارم</p></div>
<div class="m2"><p>آیینه‌ام و غبار دارم</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>چشم تو سیاه کرد روزم</p></div>
<div class="m2"><p>زلف تو گره فکند کارم</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>نه شب دانم نه روز بی‌تو</p></div>
<div class="m2"><p>شرمندة روز و روزگارم</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>تا دست به گردنم نیاری</p></div>
<div class="m2"><p>من دست ز دامنت ندارم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>وامانده‌ترین کاروانم</p></div>
<div class="m2"><p>هر چند سبک‌تر است بارم</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>تا بیند سوی من خزانم</p></div>
<div class="m2"><p>تا بینم سوی او بهارم</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>تا چشم گشوده‌ام سرشکم</p></div>
<div class="m2"><p>تا دست فشانده‌ام غبارم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>از سرکشی و کشاکش تو</p></div>
<div class="m2"><p>ناامّیدم امیدوارم</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>در مجلس عیش نیست جایم</p></div>
<div class="m2"><p>گویا شمع سر مزارم</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>از همدیم دمی نیاسود</p></div>
<div class="m2"><p>از سایة خویش شرمسارم</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>با آنکه فزون‌ترم ز خورشید</p></div>
<div class="m2"><p>یک ذرّه نکردی اعتبارم</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>کام دل من ندادی آخر</p></div>
<div class="m2"><p>من هم چو تو چاره‌ای ندارم</p></div></div>
<div class="b2" id="bn113"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn114"><div class="m1"><p>روی تو ز زلف در نقاب است</p></div>
<div class="m2"><p>شب پردة روی آفتاب است</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>با عکس تو دیدة ترم را</p></div>
<div class="m2"><p>هم باران و هم، آفتاب است</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>ما را با یاد آن لب لعل</p></div>
<div class="m2"><p>دل در بر شیشة شراب است</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>در دل جستن ترا درنگ است</p></div>
<div class="m2"><p>در جان دادن مرا شتاب است</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>آنجا که تو رخش جلوه تازی</p></div>
<div class="m2"><p>خورشید به ذرّه‌ای حساب است</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>گر نافه ز شرم بوی زلفت</p></div>
<div class="m2"><p>آهنگ خطا کند صواب است</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>جانم به هوای کام لعلت</p></div>
<div class="m2"><p>لب تشنة چشمة سراب است</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>از بیم نگاه ترک تازت</p></div>
<div class="m2"><p>جانم گرداب اضطراب است</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>در خواب جمال او ببینی</p></div>
<div class="m2"><p>ای دل اگرت خیال خواب است</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>چون کام دل حزینم از تو</p></div>
<div class="m2"><p>در بند بهانة جواب است</p></div></div>
<div class="b2" id="bn124"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn125"><div class="m1"><p>ای ماندة جست‌وجوی برخیز</p></div>
<div class="m2"><p>وی کشتة آرزوی برخیز</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>برخیز که رفت فرصت از دست</p></div>
<div class="m2"><p>هان از سر گفت‌وگوی برخیز</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بنشین که نسیم صبح برخاست</p></div>
<div class="m2"><p>ای تشنة آب‌روی برخیز</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>چون میوه کام خام بستست</p></div>
<div class="m2"><p>از کام سخن مگوی برخیز</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>این صورت معنی‌یی ندارد</p></div>
<div class="m2"><p>زین گلشن رنگ و بوی برخیز</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>چوگان حوادث از پی تست</p></div>
<div class="m2"><p>هان زین میدان چو گوی برخیز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>گل با تو سر وفا ندارد</p></div>
<div class="m2"><p>ای بلبل هرزه‌گوی برخیز</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>این باغ برِ وفا ندارد</p></div>
<div class="m2"><p>از روی گلش چو بوی برخیز</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>لب تشنة چشمة سبوییم</p></div>
<div class="m2"><p>ای ساقی ماه‌روی برخیز</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>در کشتن من سبب بسی هست</p></div>
<div class="m2"><p>ای طفل بهانه‌جوی برخیز</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>پروانه ز پا نمی‌نشیند</p></div>
<div class="m2"><p>ای شعلة تندخوی برخیز</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>زین پیش که گوییم به ناکام</p></div>
<div class="m2"><p>کز سر بنه آرزوی، برخیز</p></div></div>
<div class="b2" id="bn137"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>
<div class="b" id="bn138"><div class="m1"><p>روزی که کمان کشی ز قربان</p></div>
<div class="m2"><p>بر ما عیدست و عید قربان</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>حاجت نبود به باغ رفتن</p></div>
<div class="m2"><p>در آینه سیر کن گلستان</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>تا خاطر ما نمی‌شود جمع</p></div>
<div class="m2"><p>زلف تو نمی‌شود پریشان</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>آنم که ز کوتهیّ اقبال</p></div>
<div class="m2"><p>دستم نرسد به هیچ دامان</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>در ملک ریاضت است جایم</p></div>
<div class="m2"><p>اکسیر قناعتم دهد جان</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>یکروزة پوست تختة فقر</p></div>
<div class="m2"><p>هرگز ندهم به تخت ایران</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>درویشی را نتیجه دارم</p></div>
<div class="m2"><p>از نسبت خاک ملک گیلان</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>از خام فرج قمم دهد آب</p></div>
<div class="m2"><p>آتش زندم هوای کاشان</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>چشمم یارب مباد هرگز</p></div>
<div class="m2"><p>محتاج به سرمة صفاهان</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>خون می‌کشدم به خاک شیراز</p></div>
<div class="m2"><p>کاصل گهر منست آن کان</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>در حسرت دوستان تبریز</p></div>
<div class="m2"><p>سرخاب کنم روان ز مژگان</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>خواهم که دهد به وجه دلخواه</p></div>
<div class="m2"><p>کام دل من شه خراسان</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>ور زانکه بدادن چنین کام</p></div>
<div class="m2"><p>در آخرت من است نقصان</p></div></div>
<div class="b2" id="bn151"><p>بنشینم و ترک کام گیرم</p>
<p>شاید که به کام دل بمیرم</p></div>