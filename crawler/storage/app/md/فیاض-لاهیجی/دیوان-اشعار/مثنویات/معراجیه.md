---
title: >-
    معراجیه
---
# معراجیه

<div class="b" id="bn1"><div class="m1"><p>یکی از دوستان راست مزه</p></div>
<div class="m2"><p>که ازو گرد فقر راست مزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چاک دل دوخته به رشتة فقر</p></div>
<div class="m2"><p>همچو دل سوخته به رشتة فقر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست در دامن فنا زده‌ای</p></div>
<div class="m2"><p>بر ره و رسم پشت پا زده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یافته خرقه رسم و راه از او</p></div>
<div class="m2"><p>فقر را پشم در کلاه از او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک تجرید مایة عملش</p></div>
<div class="m2"><p>پوست پوشی سفینة غزلش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سال‌ها بوده خاک راه نجف</p></div>
<div class="m2"><p>از فلک جَسته در پناه نجف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر آن بارگاه عزّ و سری</p></div>
<div class="m2"><p>زده بازارِ گَرمِ خاکِ دری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آگهی ترجمان اوصافش</p></div>
<div class="m2"><p>همچو درّ نجف دل صافش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی از روزهای روزبهی</p></div>
<div class="m2"><p>دل ز شادی پر و ز غصّه تهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش جمعی ز دوستان سره</p></div>
<div class="m2"><p>همه نخل حیات را ثمره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد نقل حکایتی رنگین</p></div>
<div class="m2"><p>که ازو تلخِ عمر شد شیرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت کاین هوش بخشِ گوش‌طلب</p></div>
<div class="m2"><p>هست مشهور در عراق عرب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبع‌ها زین بهار چون بشکفت</p></div>
<div class="m2"><p>خرّمی دست‌ها به هم زد و گفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حیف کاین کهنه‌پوش دیرینی</p></div>
<div class="m2"><p>در خورستش لباس رنگینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کسی در جواب چون تن زد</p></div>
<div class="m2"><p>هوس انگشت بر لب من زد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست طبع هوس چو عذرپذیر</p></div>
<div class="m2"><p>یک زمان گوش کن بدین تقریر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نیک مردی ز تاجران عرب</p></div>
<div class="m2"><p>دامن او گرفته دست طلب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نام او در زمانه حاجی نجم</p></div>
<div class="m2"><p>کرده شیطان هر هوس را رجم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بست احرام طوف رکن و مقام</p></div>
<div class="m2"><p>از نجف کرد سوی کعبه خرام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من ندانم چرا به دیدة دید</p></div>
<div class="m2"><p>کعبه را در نجف به طوف ندید!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کرده از راه مصر عزم حجاز</p></div>
<div class="m2"><p>که حقیقت طلب شود ز مجاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گشت با کاروان حج همراه</p></div>
<div class="m2"><p>گه به پا رَه بُرید و گه به نگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سفر پا به کار دل ناید</p></div>
<div class="m2"><p>نظر هوش در سفر باید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شتران کف‌زنان در آن وادی</p></div>
<div class="m2"><p>همه را هوش رفته از شادی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در پی ناقه رهروان عرب</p></div>
<div class="m2"><p>گه حدی گوی و گه خدای طلب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نظر افتاد نجم را ناگاه</p></div>
<div class="m2"><p>محملی دید سر کشیده به ماه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دامن پرده باد را در کف</p></div>
<div class="m2"><p>دیده را در نظاره حق به طرف</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دختری دید اندر آن خرگاه</p></div>
<div class="m2"><p>محمل از حسن وی چو خرگه ماه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در کمال جمال و فیروزی</p></div>
<div class="m2"><p>همه چیزش ز نیکویی روزی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پای تا سر همه به کام نگاه</p></div>
<div class="m2"><p>لیک مویش سفید چون شب ماه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر دمیده سفیدی از شبِ مو</p></div>
<div class="m2"><p>آفتابی مه نوش ابرو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گشت اندیشه زین عجب درهم</p></div>
<div class="m2"><p>حیرتش می فزود بر سر هم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دید مه چون نظر فکند به نجم</p></div>
<div class="m2"><p>نسخة پر ز معنی کم حجم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گفت کای ناشناس حرمت حج</p></div>
<div class="m2"><p>رفته در راه دین به دیدة کج</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>محرمان حریم این درگاه</p></div>
<div class="m2"><p>کی به نامحرمان کنند نگاه؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دیده بر بند از سیاه و سفید</p></div>
<div class="m2"><p>چشم معنی گشا و دیدة دید</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرد شد منفعل ز گفتة زن</p></div>
<div class="m2"><p>گفت خامش که اِنّ بعض‌الظّن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حیرتم کرد مضطرب احوال</p></div>
<div class="m2"><p>که به هم دیدم آفتاب و هلال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>موی دیدم سفید بر سر ماه</p></div>
<div class="m2"><p>چشم کردم برین سفید سیاه</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مه چو دریافت بی‌دروغ و فنش</p></div>
<div class="m2"><p>بوی صدق نهفته در سخنش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گفت این قصه هست دور و دراز</p></div>
<div class="m2"><p>با تو گویم چو می‌رسی به حجاز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>قصة من دراز و ره کوتاه</p></div>
<div class="m2"><p>تار این نغمه نیست رشتة راه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون رسیدند کاروان به طواف</p></div>
<div class="m2"><p>چهره پرگرد راه و آینه صاف</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بعد سعی طواف رکن و مقام</p></div>
<div class="m2"><p>شد حلال آنچه گشته بود حرام</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نجم مشتاق دیدن مه بود</p></div>
<div class="m2"><p>دل به پای نگاه در ره بود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا که روزی دچار هم گشتند</p></div>
<div class="m2"><p>قصّه کوتاه یار هم گشتند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نجم را برد مه به خانة خویش</p></div>
<div class="m2"><p>سفره گسترد و نان نهاد به پیش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دید آنجا نشسته پیرزنی</p></div>
<div class="m2"><p>جسته از دست صد خزان چمنی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دست شستند از طعام و شراب</p></div>
<div class="m2"><p>یافت ره در میان سؤال و جواب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قصّه سر کرد ماه نوش لبان</p></div>
<div class="m2"><p>چهره ‌ای همچو مه به نیم شبان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کاین سفیدی مو ز پیری نیست</p></div>
<div class="m2"><p>که هنوزم ز عمر باشد بیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>عجبم من حکایتم عجب است</p></div>
<div class="m2"><p>بلعجب حال من ازین سبب است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پدرم هست مهتری ز عرب</p></div>
<div class="m2"><p>در قبیله سرآمدی به نسب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پدر و مادرم اباعن جد</p></div>
<div class="m2"><p>عمّ و خال و برادران بی‌حد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه ممتاز در میان عرب</p></div>
<div class="m2"><p>همه را مال و جاه و عزّ و نسب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>لیک در دین و مذهب و ملّت</p></div>
<div class="m2"><p>همه اهل جماعت و سنّت</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دین سنّی میانشان شایع</p></div>
<div class="m2"><p>مذهب بوحنیفه را تابع</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بود ما را برادری زین پیش</p></div>
<div class="m2"><p>در جوانی ز هر چه گویی بیش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>مهر او در دلم چو نقش نگین</p></div>
<div class="m2"><p>روز و شب خدمت ویم آیین</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نه ز هم یک نفس جدا بودیم</p></div>
<div class="m2"><p>نه به غیر هم آشنا بودیم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>جست ناگاه تند باد اجل</p></div>
<div class="m2"><p>کرد سروش به سایه جای بدل</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چون سپردند قامتش در خاک</p></div>
<div class="m2"><p>گشت یک باره جیب صبرم چاک</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تن چون برف را لحد شد ظرف</p></div>
<div class="m2"><p>رفت چون حرف مدغم اندر حرف</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من که بی او نبود آرامم</p></div>
<div class="m2"><p>گشت لبریزِ بیخودی جامم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گفتم این نازنین برادر من</p></div>
<div class="m2"><p>که چو جان بود در برابر من</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نازنین و عزیز پرورده</p></div>
<div class="m2"><p>خو به ناز و به نازکی کرده</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>حجرة گور تنگ و تیره و تار</p></div>
<div class="m2"><p>همنشینی نه خفته نه بیدار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>که کند تا ز خواب بیدارش؟</p></div>
<div class="m2"><p>که گشاید قبا و دستارش؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>کرده خوابی که نیست بس شدنش</p></div>
<div class="m2"><p>رفته راهی که نیست آمدنش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>سخن اینجا رسید و شد باریک</p></div>
<div class="m2"><p>سفری دور و ره بسی نزدیک</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>من همان به که همرهش باشم</p></div>
<div class="m2"><p>غمخور گاه و بی‌گهش باشم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>همرهش با رخ چو باغ شدم</p></div>
<div class="m2"><p>خلوت گور را چراغ شدم</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کردم این عزم و دل ز جان کندم</p></div>
<div class="m2"><p>تن به سردابه‌اش درافکندم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همه قوم و قبیله بر سر من</p></div>
<div class="m2"><p>خون دل ریختند در بر من</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به ملامت سرشت مشت گلم</p></div>
<div class="m2"><p>نه نصیحت شنید گوش دلم</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>همه بگذاشتندم و رفتند</p></div>
<div class="m2"><p>مرده انگاشتندم و رفتند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>من در آن گور تنگ و تیره و تار</p></div>
<div class="m2"><p>دل ز جان برگرفته دست از کار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>داده با خود قرار مردن خویش</p></div>
<div class="m2"><p>خون خود کرده خود به گردن خویش</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ناگهان گوشه‌ای شکافته شد</p></div>
<div class="m2"><p>پرتوی همچو صبح تافته شد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>شخص نورانیی درون آمد</p></div>
<div class="m2"><p>که ز وحشت دلم برون آمد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از فروغ جمال آن خورشید</p></div>
<div class="m2"><p>شد شب تیره همچو روز سفید</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر سر مرده‌ام به صد تمکین</p></div>
<div class="m2"><p>رفت و بنشست بر سر بالین</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بعد یکدم ز جانب دیگر</p></div>
<div class="m2"><p>دو کس دیگر آمدند به در</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>این یکی سخت هولناک و مهیب</p></div>
<div class="m2"><p>وآن یکی را ز اعتدال نصیب</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>آن یکی دست راست کرد طلب</p></div>
<div class="m2"><p>این یکی راست رفت جانب چپ</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>در کف او عمودی از آتش</p></div>
<div class="m2"><p>همه خوش‌ها زدیدنش ناخوش</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پیش تابوت مردة مسکین</p></div>
<div class="m2"><p>زد عمودی چو آسمان به زمین</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>از نهیب صدا در آن شب تار</p></div>
<div class="m2"><p>مرده از خواب مرگ شد بیدار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>من ز دهشت ز خویشتن رفتم</p></div>
<div class="m2"><p>ماند قالب به جا و من رفتم</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>گشت از صورت آن نوازنده</p></div>
<div class="m2"><p>زنده‌ام مرده مرده‌ام زنده</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چو ز بیهوشی آمدم با هوش</p></div>
<div class="m2"><p>موی دیدم سفید بر سر دوش</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>تیره شد روز در دل تنگم</p></div>
<div class="m2"><p>شد سفید این شب سیه‌رنگم</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چون نظر بر برادر افکندم</p></div>
<div class="m2"><p>خبر از خود نماند یک چندم</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دیدم از خواب مرگ بر جسته</p></div>
<div class="m2"><p>به تنش جانِ رفته پیوسته</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>دید خود را به حالت منکر</p></div>
<div class="m2"><p>نه پدر پیش چشم و نه مادر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>کفنش جامه گشته حسرت قوت</p></div>
<div class="m2"><p>چار دیوار، تختة تابوت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>بسترش خاک و خشت بالینش</p></div>
<div class="m2"><p>بی‌کسی، همنشین دیرینش</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ره‌آمد شدن نه پیش و نه پس</p></div>
<div class="m2"><p>نفس بسته همزبانش و بس</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>سر زد از دیده اشک و از دل آه</p></div>
<div class="m2"><p>بانگ زد هر طرف که وا ابتاه</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>از پدر چون ندید روی جواب</p></div>
<div class="m2"><p>سوی مادر دواند پیک خطاب</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>یأس مادر چو حلقه بر در زد</p></div>
<div class="m2"><p>قرعة بانگ بر برادر زد</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>از برادر چو نیز امید برید</p></div>
<div class="m2"><p>بر زبان نام عمّ و خال دوید</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چون دل از عم و خال هم شد سرد</p></div>
<div class="m2"><p>بر زبان گرم نام من آورد</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>خواستم دم برآورم به جواب</p></div>
<div class="m2"><p>نفسم بر گلو فکند طناب</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>چون نیامد جوابی از کس باز</p></div>
<div class="m2"><p>دست بر سر زدن گرفت آغاز</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>کرد بنیاد گریه و شیون</p></div>
<div class="m2"><p>پود صد ناله تارتار کفن</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>دست گیری نه در حضور و نه غیب</p></div>
<div class="m2"><p>گاه دامن درید و گاهی جیب</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>سر دیوانگی ز دل بر زد</p></div>
<div class="m2"><p>چوب تابوت کند و بر سر زد</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>یافت آن دم که این شب گورست</p></div>
<div class="m2"><p>دامن زندگی ز کف دورست</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>چشمش آن دم ز خواب شد بیدار</p></div>
<div class="m2"><p>که فرو بسته دید چارة کار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>وه که بیداری ابد را سود</p></div>
<div class="m2"><p>نیست، چون دست و پای چاره غنود</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>داد می‌کرد و دادرس کس نه</p></div>
<div class="m2"><p>راه را پیش و پای را پس نه</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>نفس از ناله سینه‌گیر افتاد</p></div>
<div class="m2"><p>چشم بر منکر و نکیر افتاد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>رفت یک باره دست و دل از کار</p></div>
<div class="m2"><p>دیدنی کم، ندیدنی بسیار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>منکر آمد به پیش بهر سؤال</p></div>
<div class="m2"><p>کردش اول زبان دهشت لال</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>باز پرسیدش از خدای، نخست</p></div>
<div class="m2"><p>کس ندانسته را ازو می‌جست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>محو دهشت زبان خاموشش</p></div>
<div class="m2"><p>آنچه دانسته هم فراموشش</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>مرد نورانی از سر بالین</p></div>
<div class="m2"><p>کرد بر وی جواب را تلقین</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>گفت تا مرده را کند آگاه</p></div>
<div class="m2"><p>خود به خود لااله‌الا‌الله</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بعد از آن از نبی سؤالش کرد</p></div>
<div class="m2"><p>امتحان زبان لالش کرد</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>در جوابش زبان به بند افتاد</p></div>
<div class="m2"><p>باز حلّال مشکلات گشاد</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>گفت آنگه بگو امام تو کیست؟</p></div>
<div class="m2"><p>مایة فخر و احترام تو کیست</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>آنکه بی او نماز نیست درست</p></div>
<div class="m2"><p>عاشقان را نیاز نیست درست</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>چون نبود از امامت آگاهیش</p></div>
<div class="m2"><p>کندتر زبان به همراهیش</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>چون امامت نبود در دینش</p></div>
<div class="m2"><p>زان ملّقن نکرد تلقینش</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>چون نبود از امام دین خبرش</p></div>
<div class="m2"><p>زد همان گرز آتشین به سرش</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>زد عمودی که آتش از وی جست</p></div>
<div class="m2"><p>مو به مویش به شعله در پیوست</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>کفنش پنبه و عمود آتش</p></div>
<div class="m2"><p>شعله از تار تار او سرکش</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>گفتی از بس که شعله گرم دوید</p></div>
<div class="m2"><p>کفنش بر تنست نفت سفید</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بار دیگر ز هوش رفتم باز</p></div>
<div class="m2"><p>نه به سر هوش و نه به لب آواز</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>چون به هوش آمدم ز بی‌هوشی</p></div>
<div class="m2"><p>گوشزد شد نوای خاموشی</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>دیدم از سینه خوف را رانده</p></div>
<div class="m2"><p>آن دو کس رفته این یکی مانده</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>آن مجرّد سرشت نورانی</p></div>
<div class="m2"><p>تن مجسّم ولیک روحانی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>دست آویختم به دامن او</p></div>
<div class="m2"><p>مور گشتم به گرد خرمن او</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>آب شرم از دو دیده بگشادم</p></div>
<div class="m2"><p>خاک گشتم به پایش افتادم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>گفتم ای عین نور و نورالعین</p></div>
<div class="m2"><p>بر سری و بزرگواری زین</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>به حق آن بزرگوار اله</p></div>
<div class="m2"><p>که ترا داد این بزرگی و جاه</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>که به من بازگو کیی چه کسی؟</p></div>
<div class="m2"><p>که کس بی‌کسی و دادرسی</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>ملکی بس مقرّبی به عمل</p></div>
<div class="m2"><p>یا که هستی پیمبر مرسل</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>من ندانم کیی به عزّت و جاه</p></div>
<div class="m2"><p>که به فرمان تست ماهی و ماه؟</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>چون شکر خنده با لبش شد جفت</p></div>
<div class="m2"><p>نفس عنبرین گشاد و چه گفت</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>منم آن عارف خدای به حق</p></div>
<div class="m2"><p>خازن مخزن قضا مطلق</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>وارث شرع احمد مرسل</p></div>
<div class="m2"><p>عالم علم آخر و اوّل</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>منم آن کس که بی‌محبّت من</p></div>
<div class="m2"><p>نه فرایض قبول شد نه سنن</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>هر که را با منش شناخت نبود</p></div>
<div class="m2"><p>از شناسایی خداش چه سود</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>هست دانستنم خدادانی</p></div>
<div class="m2"><p>مهر من مایة مسلمانی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بغض من موجب نکوهش زشت</p></div>
<div class="m2"><p>در کف مهر من کلید بهشت</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بی‌شناسائیم برادر تو</p></div>
<div class="m2"><p>شد چنین خوار در برابر تو</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>داشتی گر ز مهر من مایه</p></div>
<div class="m2"><p>برگذشتی به انجمش پایه</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>سر عزّت به آسمان سودی</p></div>
<div class="m2"><p>در نعیم ابد بیاسودی</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>نام من چون نداشت ورد زبان</p></div>
<div class="m2"><p>آنچه هم داشت نامدش به زبان</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>مهر من چون نبود در بارش</p></div>
<div class="m2"><p>زان کسادی گرفت بازارش</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>گفتم ای نور پاک یزدانی</p></div>
<div class="m2"><p>وی ز تو عالمی به نادانی</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>نام خود باز گو به من که که‌ای</p></div>
<div class="m2"><p>وز چه جنسی، چه عالمی و چه‌ای</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>که ندانم کسی بدین اوصاف</p></div>
<div class="m2"><p>نشنیدم چنین کس از اسلاف</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>گفت نامم علی ابی‌طالب</p></div>
<div class="m2"><p>در همه چیز بر همه غالب</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>منم آن آفتاب عالمتاب</p></div>
<div class="m2"><p>که ندیدست روی پوش سحاب</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>پرده سوزست پرتو چهرم</p></div>
<div class="m2"><p>نیست یک ذرّه خالی از مهرم</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>چون به گوشم رسید نام علی</p></div>
<div class="m2"><p>مهر او گشت در دلم ازلی</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>گفتم ای من فدای نام خوشت</p></div>
<div class="m2"><p>دین و دل عقل و هوش پیشکشت</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>گرچه هست این سؤال ترک ادب</p></div>
<div class="m2"><p>حیرتم کرد پایمال عجب</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>کز چه وقت سؤال از آن مسکین</p></div>
<div class="m2"><p>نام خود داشتی دریغ چنین</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>از تو دیدش دو عقده روی‌ِ گشاد</p></div>
<div class="m2"><p>در سوم از چه رو دریغ افتاد</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>گفت چون در جواب آن دو سؤال</p></div>
<div class="m2"><p>بود معلوم او حقیقت حال</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>لیک از هول گور و بیم گزند</p></div>
<div class="m2"><p>بود افتاده بر زبانش بند</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>فرض شد بر مروّتم ارشد</p></div>
<div class="m2"><p>که ز من یافت آن دو عقده گشاد</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>چون به فضل من اعتقاد نداشت</p></div>
<div class="m2"><p>نام من جز به سهو یاد نداشت</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>این گره بر زبانش محکم بود</p></div>
<div class="m2"><p>لاجرم لایق جهنّم بود</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>نام من دادی ار کسیش به یاد</p></div>
<div class="m2"><p>بر زبانش نیامدی ز عناد</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>این چنین است حکم بار اله</p></div>
<div class="m2"><p>که کسی بی‌بصر نبیند راه</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>گفتم آوخ که خاک بر سر من</p></div>
<div class="m2"><p>بر پدر لعن باد و مادر من</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>جمله قوم و قبیله‌ام یک سر</p></div>
<div class="m2"><p>پی بوبکر رفته‌اند و عمر</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>باد در حشرشان زبان کج مج</p></div>
<div class="m2"><p>کز ره راست رفته‌اند به کج</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>همه حق را نهفته‌اند به زور</p></div>
<div class="m2"><p>راه نزدیک رفته‌اند به دور</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>چاره چون بود ره نرفتم راست</p></div>
<div class="m2"><p>چه کنم چاره از میان برخاست</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>کرده‌ام در حیات چون تقصیر</p></div>
<div class="m2"><p>کی به گورم شوند عذرپذیر</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>ره نرفتم چو راه روشن بود</p></div>
<div class="m2"><p>عذر تاریکیم ندارد سود</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>نیست چون چاره دیگرم چه کنم؟</p></div>
<div class="m2"><p>چه کنم خاک بر سرم چه کنم</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>گفت چون گوش کرد زاری من</p></div>
<div class="m2"><p>دید فریاد و بی‌قراری من</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>که هنوزت ز عمر باقی هست</p></div>
<div class="m2"><p>بزم را باده هست و ساقی هست</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>خود به مرگ خود ار شتافته‌ای</p></div>
<div class="m2"><p>لیک عمر دوباره یافته‌ای</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>چون شوی زین مضیق تیره خلاص</p></div>
<div class="m2"><p>پی ما گیر و باش بندة خاص</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>بعد ازین راه راست گیر به پیش</p></div>
<div class="m2"><p>هر چه خواهی شنو ز عمة خویش</p></div></div>
<div class="b" id="bn184"><div class="m1"><p>که درست اعتقاد و نیک زن است</p></div>
<div class="m2"><p>یکی از شیعیان خاص من است</p></div></div>
<div class="b" id="bn185"><div class="m1"><p>در قبیله به دین و دانش فرد</p></div>
<div class="m2"><p>یک چنین زن به از هزاران مرد</p></div></div>
<div class="b" id="bn186"><div class="m1"><p>این بگفت و ز دیده گشت نهان</p></div>
<div class="m2"><p>ماندم از سینه رفته تاب و توان</p></div></div>
<div class="b" id="bn187"><div class="m1"><p>پدرم در کمین من به قرار</p></div>
<div class="m2"><p>بود جمعی گذاشته بیدار</p></div></div>
<div class="b" id="bn188"><div class="m1"><p>که چو آواز زار من شنوند</p></div>
<div class="m2"><p>نغمه‌ ریزی تار من شنوند</p></div></div>
<div class="b" id="bn189"><div class="m1"><p>بر سرم بی‌درنگ بشتابند</p></div>
<div class="m2"><p>نیمه جانم ز مرگ دریابند</p></div></div>
<div class="b" id="bn190"><div class="m1"><p>چو ازین مژده جان من بشکفت</p></div>
<div class="m2"><p>دل ز غم فرد شد به شادی جفت</p></div></div>
<div class="b" id="bn191"><div class="m1"><p>لیک چون ره نیافتم بیرون</p></div>
<div class="m2"><p>دل ز بیم هلاک شد پر خون</p></div></div>
<div class="b" id="bn192"><div class="m1"><p>گاه جان می‌شد از الم خسته</p></div>
<div class="m2"><p>گه به الطاف شاه دل بسته</p></div></div>
<div class="b" id="bn193"><div class="m1"><p>دل به نومیدیم عنان چو سپرد</p></div>
<div class="m2"><p>نالة من خبر به یاران برد</p></div></div>
<div class="b" id="bn194"><div class="m1"><p>ناگهان روزنی پدید افتاد</p></div>
<div class="m2"><p>در سردابه چون دلم بگشاد</p></div></div>
<div class="b" id="bn195"><div class="m1"><p>بر سرم ریختند خرد و بزرگ</p></div>
<div class="m2"><p>یوسفم برد جان ز چنگل گرگ</p></div></div>
<div class="b" id="bn196"><div class="m1"><p>حال خود را نهفتم از کم و بیش</p></div>
<div class="m2"><p>گفتم احوال خود به عمة خویش</p></div></div>
<div class="b" id="bn197"><div class="m1"><p>عمه‌ام راه حل به من بنمود</p></div>
<div class="m2"><p>کرد تعلیم آنچه لازم بود</p></div></div>
<div class="b" id="bn198"><div class="m1"><p>گفتم احوال خویش بی کم و بیش</p></div>
<div class="m2"><p>قصة عمًه هم شنو از خویش</p></div></div>