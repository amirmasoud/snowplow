---
title: >-
    شمارهٔ ۱۲ - شاید خطاب به قاضی سعید باشد
---
# شمارهٔ ۱۲ - شاید خطاب به قاضی سعید باشد

<div class="b" id="bn1"><div class="m1"><p>ای آنکه هر دم از نگه دلنواز خویش</p></div>
<div class="m2"><p>جان دگر به قالب حسرت روان کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب چو نوبهار تبسّم کنی سبیل</p></div>
<div class="m2"><p>رخسار آز را چو رخ گلستان کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهرنما کند چو غضب تیغ ابروت</p></div>
<div class="m2"><p>گلزار رنگ چهرة گل را خزان کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با دشمنان درآیی چون بوی گل به خار</p></div>
<div class="m2"><p>وز دوستان چو باد صبا سر گران کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خود نگویم اینکه در اطوار دوستی</p></div>
<div class="m2"><p>با غیر چون نشینی و با من چه‌سان کنی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیکن دو بیت بر تو ز نظم یگانه‌ای</p></div>
<div class="m2"><p>خوانم بآن امید که شاید روان کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارم وصیّتی به تو ای دشمن دلم</p></div>
<div class="m2"><p>خواهم غلط کنی تو و گوشی به آن کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مفروش دوست بر سر بازار دشمنان</p></div>
<div class="m2"><p>ترسم درین معامله آخر زیان کنی</p></div></div>