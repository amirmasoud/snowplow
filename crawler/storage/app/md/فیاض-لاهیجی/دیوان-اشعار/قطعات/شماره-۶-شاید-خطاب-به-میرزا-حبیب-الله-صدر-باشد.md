---
title: >-
    شمارهٔ ۶ - شاید خطاب به میرزا حبیب‌الله صدر باشد
---
# شمارهٔ ۶ - شاید خطاب به میرزا حبیب‌الله صدر باشد

<div class="b" id="bn1"><div class="m1"><p>صدر جهان و عالم جان و سپهر فضل</p></div>
<div class="m2"><p>ای آنکه آسمانت به جان چاکری کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اطفال فضل را به جهان بهر تربیت</p></div>
<div class="m2"><p>شد وقت آنکه طبع خوشت مادری کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید اگر طبیعت معجزنمای تو</p></div>
<div class="m2"><p>در ملک شرع دعوی پیغمبری کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طومار نه فلک ز قضا این امید داشت</p></div>
<div class="m2"><p>کانشای فکر بکر ترا دفتری کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افشانی کتاب کمال ترا ز شوق</p></div>
<div class="m2"><p>خورشید در پیالة گردون زری کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در لجّة تلاطم امواج فکرتت</p></div>
<div class="m2"><p>کوه متانت تو مگر لنگری کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خطّی به استقامت طبع خوشت کجاست</p></div>
<div class="m2"><p>تا آسمانِ فکر ترا محوری کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خطبة جلال تو خوانند قدسیان</p></div>
<div class="m2"><p>نه آسمان خطیب ترا منبری کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کج‌سلیقگی مه نو از پی شرف</p></div>
<div class="m2"><p>در مدح‌سنجیت هوس شاعری کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر پرتوی ز عکس جمالت به وی فتد</p></div>
<div class="m2"><p>مه فربهیّ و مهرِ فلک لاغری کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم چشم چرخ شد زَمی اکنون که ریگ دشت</p></div>
<div class="m2"><p>از پرتو ضمیر خوشت اختری کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پیرایة جمال عروس خیال تو</p></div>
<div class="m2"><p>بر دست و پای شاهد دین زیوری کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>معراج فطرت تو بر اوج سمای قدس</p></div>
<div class="m2"><p>بر پیش طاق چرخ نهم برتری کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کان سنگ‌ریزه‌ای بود و بحر قطره‌ای</p></div>
<div class="m2"><p>آنجا که همّت تو سخاگستری کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بند زبان ناطقه گردد نفس ز شرم</p></div>
<div class="m2"><p>جایی که فطرت تو سخن‌آوری کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کلک تو در خرام چو انشا کنی کلام</p></div>
<div class="m2"><p>خون جگر به کاسة کبک دری کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر شعلة طبیعتت ار بشکند نقاب</p></div>
<div class="m2"><p>آتش هوای طینت خاکستری کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید آسمان به سهائی ملقّب است</p></div>
<div class="m2"><p>در کشوری که طبع خوشت اختری کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهر شمیم مجلس انس تو از شرف</p></div>
<div class="m2"><p>خورشید عنبریّ و فلک مجمری کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شاها ز بیم آنکه ز لطف عمیم تو</p></div>
<div class="m2"><p>این بنده برتری به مه و مشتری کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دورم فکند از تو به صد حیله آسمان</p></div>
<div class="m2"><p>این ظلم را مگر کرمت داوری کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دیده دور از تو و بر تن جدا ز تو</p></div>
<div class="m2"><p>مژگان من سنانی و مو خنجری کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نزدیک شد که محنت هجران دل مرا</p></div>
<div class="m2"><p>از زندگی ملول و ز هستی بری کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حرمان بلاست ورنه ز مردن چه غم مرا</p></div>
<div class="m2"><p>مفت من اینکه تا عدمم رهبری کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا دیو هجر برد ز ره خاطر مرا</p></div>
<div class="m2"><p>رفت آنکه دیده‌ام نگهی با پری کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آیینة امید من از هجر تیره گشت</p></div>
<div class="m2"><p>کو صیقل وصال که روشنگری کند؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نادیده کام وصل به هجرم فکند چرخ</p></div>
<div class="m2"><p>کز وی مباد پایة من برتری کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر من وبال کرد مسلمانی مرا</p></div>
<div class="m2"><p>مشکل که در فرنگ کس این کافری کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باری روا مدار علی‌رغم آسمان</p></div>
<div class="m2"><p>کاین خسته خاک گردد و خاکستری کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لطفی نما که شاید ازین ورطه وارهد</p></div>
<div class="m2"><p>در خدمت تو شاد زید چاکری کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا آسمان خمیده کند از درت گذر</p></div>
<div class="m2"><p>تا آفتاب شاهدی و دلبری کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پیوسته باد شاهد بختت جوان و شاد</p></div>
<div class="m2"><p>پشت عدوت همچو فلک چنبری کند</p></div></div>