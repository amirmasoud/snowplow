---
title: >-
    شمارهٔ ۳ - ماده تاریخ سیل قم
---
# شمارهٔ ۳ - ماده تاریخ سیل قم

<div class="b" id="bn1"><div class="m1"><p>داد از دست سیل حادثه، داد</p></div>
<div class="m2"><p>که ازو شد گُل بلا سیراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیلی از کوه غم فرود آمد</p></div>
<div class="m2"><p>که ازو چشم فتنه شد بی‌خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه چه سیل؛ آسمان سیّالی</p></div>
<div class="m2"><p>برده از عمرها گرو ز شتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته بر دوش کوه‌های گران</p></div>
<div class="m2"><p>کرده سیراب موج‌های سراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیر از سر بدر روی چو خمار</p></div>
<div class="m2"><p>زود از پا درافکنی، چو شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخِ میدان فراخِ پهن آغوش</p></div>
<div class="m2"><p>بر سرش چرخ‌زن چو قصرِ حباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتنه‌اش چنگ بر زده به عنان</p></div>
<div class="m2"><p>اجلس دست بر زده به رکاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این جهان درشت ازو هموار</p></div>
<div class="m2"><p>فلک بی‌حساب ازو به حساب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهر قم کابروی عالم بود</p></div>
<div class="m2"><p>شد ازو خشک لب چو موج سراب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در روانی و بی‌ثباتی زد</p></div>
<div class="m2"><p>در دروازه تخته بر سر آب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خانه‌ها از شکستگی‌ها کرد</p></div>
<div class="m2"><p>خاک دیوار بر سر اسباب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مدرسه غسل ارتماسی کرد</p></div>
<div class="m2"><p>رفت در سجده مسجد و محراب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حرف دیوار، سست در هر جا</p></div>
<div class="m2"><p>سخن در، شکسته در هر باب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کشتی عمر را ز موج بلا</p></div>
<div class="m2"><p>جای امنی نبود جز گرداب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شهر قم را که رشک عالم بود</p></div>
<div class="m2"><p>کرد سیلاب همچو نقش بر آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اشک عشّاق بود شورانگیز</p></div>
<div class="m2"><p>بر دمیده ز کورة سیماب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با که دست قضا به آتش قهر</p></div>
<div class="m2"><p>از گُلِ این زمین گرفت گلاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من چه گویم چه کرد با قم سیل؟</p></div>
<div class="m2"><p>قم کتان بود و سیل چون مهتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر لب بام اگر زنی انگشت</p></div>
<div class="m2"><p>با تو گوید حکایت سیلاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بهر تاریخ فکر می‌کردم</p></div>
<div class="m2"><p>جمعی از دوستان برای صواب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوستی آه آتشین زد و گفت</p></div>
<div class="m2"><p>خاک قم را به باد داد این آب</p></div></div>