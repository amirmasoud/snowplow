---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>اگر به بند زمانه کسی شود محبوس</p></div>
<div class="m2"><p>ز حال او بنمایند مردم استفسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم که گر به غلط چون کسی برد نامم</p></div>
<div class="m2"><p>هزار بار بگوید ز ترس استغفار</p></div></div>