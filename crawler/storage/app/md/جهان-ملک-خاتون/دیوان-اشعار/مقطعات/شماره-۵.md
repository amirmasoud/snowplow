---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>اگرچه یار تواند به سوی خسته نظر</p></div>
<div class="m2"><p>ز روی لطف فکندن ولی نمی فکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا همیشه دل دشمنان کند خرم</p></div>
<div class="m2"><p>چرا مدام دل دوستان خود شکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن مکن که ز باغ دل وفاداران</p></div>
<div class="m2"><p>کسی درخت محبّت ز بیخ برنکند</p></div></div>