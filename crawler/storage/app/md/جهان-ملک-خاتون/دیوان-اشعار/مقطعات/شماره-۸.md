---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>به کنج مدرسه ای کز دلم خراب ترست</p></div>
<div class="m2"><p>نشسته ام من مسکین بی کس درویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز از سخن خلق رستگار نیم</p></div>
<div class="m2"><p>به بحر فکر فرو رفته ام ز طالع خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم همیشه از آن روی پر ز خونابست</p></div>
<div class="m2"><p>که می رسد نمک جور بر جراحت ریش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا نه رغبت جاه و نه حرص مال و منال</p></div>
<div class="m2"><p>گرفته ام به ارادت قناعتی در پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندانم از من خسته جگر چه می خواهند</p></div>
<div class="m2"><p>چو نیست با بد و نیکم حکایت از کم و بیش</p></div></div>