---
title: >-
    شمارهٔ ۱۱۵۰
---
# شمارهٔ ۱۱۵۰

<div class="b" id="bn1"><div class="m1"><p>دل ضعیف مرا در دو زلف خود جا کن</p></div>
<div class="m2"><p>نشیمنیش خدا را به زلف شیدا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیب گشت ملول از من ضعیف نحیف</p></div>
<div class="m2"><p>بیا و یک دمش از وصل خود مداوا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم به دست جفا دادی و نبخشودی</p></div>
<div class="m2"><p>به عذرهای گذشته یکی مدارا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وصال ببستی ز روی ما ز چه روی</p></div>
<div class="m2"><p>چو حلقه سر به درت می زنیم در وا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدار نور دریغ از دلم چو آئینه</p></div>
<div class="m2"><p>بیا و رخ به رخ دلبر مه آسا کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرابخانه امید دل خراب از غم</p></div>
<div class="m2"><p>ز دولت شب وصلت بیا و احیا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا که دور ز رویت جهان نمی بینم</p></div>
<div class="m2"><p>به ماه روی خودم هر دو دیده بینا کن</p></div></div>