---
title: >-
    شمارهٔ ۸۴۷
---
# شمارهٔ ۸۴۷

<div class="b" id="bn1"><div class="m1"><p>ای به رخت نیاز من از حد و اندازه ی بیش</p></div>
<div class="m2"><p>بر دل ریش ما بگو چند زنی ز غمزه نیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریش غم تو بر دلم هست ز تیغ روز هجر</p></div>
<div class="m2"><p>دور مدار دلبرا مرهم وصل خود ز ریش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرهم وصل چون نداد لطف تو ای طبیب من</p></div>
<div class="m2"><p>بر سر ریش خاطرم بیش نمک مزن به ریش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیر جفای ترکشت بیش مزن به جان ما</p></div>
<div class="m2"><p>زآنکه فضای کوی تو قبله بود مرا و کیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جور کشیدنم ز تو بس عجب اوفتاده است</p></div>
<div class="m2"><p>خویش منی و این ستم کس بکند به جای خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز وداع مشکلست از رخ خوب دلبران</p></div>
<div class="m2"><p>بین که چه حالتی بود دل ز پس و رهم ز پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک نظری ز لطف خود سوی جهان فکن که من</p></div>
<div class="m2"><p>دم به دمم به روی تو صبر کمست و عشق بیش</p></div></div>