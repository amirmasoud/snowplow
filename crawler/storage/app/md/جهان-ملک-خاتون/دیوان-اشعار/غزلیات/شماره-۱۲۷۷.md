---
title: >-
    شمارهٔ ۱۲۷۷
---
# شمارهٔ ۱۲۷۷

<div class="b" id="bn1"><div class="m1"><p>تا کی ای دیده دل اندر رخ جانان بندی</p></div>
<div class="m2"><p>مدّتی با غم زلف مهی اندر بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس جفا می رود اندر غم هجران بر من</p></div>
<div class="m2"><p>تا کی ای جان ستم و جور به ما بپسندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش رویت نرود هرگزم از دیده جان</p></div>
<div class="m2"><p>تو مگر مهر رخت در دل ما آکندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر ای دیده ی مهجور ستمدیده چرا</p></div>
<div class="m2"><p>در غمش باز سپر بر سر آب افکندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل خسته تو تا چند ز سودای رخش</p></div>
<div class="m2"><p>در سر زلف دلارام چنین پابندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما سهی سرو ندیدیم بدین شیوه گری</p></div>
<div class="m2"><p>ما دگر ماه ندیدیم بدین دلبندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دل و جان و جهان هر سه فدایت کردم</p></div>
<div class="m2"><p>تو چرا یکسره دل را ز وفا برکندی</p></div></div>