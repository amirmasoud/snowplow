---
title: >-
    شمارهٔ ۷۰۹
---
# شمارهٔ ۷۰۹

<div class="b" id="bn1"><div class="m1"><p>مرا زین بیش درد دل نباید</p></div>
<div class="m2"><p>وگر دردم دهی دیگر نشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشاید جور با یاران یکدل</p></div>
<div class="m2"><p>اگرچه دوستی در دل فزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو سروش بر دو چشم خود نشانم</p></div>
<div class="m2"><p>به سوی ما اگر یک لحظه آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تخم جفا کارد به جانم</p></div>
<div class="m2"><p>به دم مهر گیاهِ او برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردم از وفایش تا توانم</p></div>
<div class="m2"><p>به عهدش گر جهان بر من سرآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جورش گویم ای دل ترک او کن</p></div>
<div class="m2"><p>بر اینم جان من گر دلبر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا در راه عشقش سر فدا کن</p></div>
<div class="m2"><p>به پیش ما دمی کان دلبر آید</p></div></div>