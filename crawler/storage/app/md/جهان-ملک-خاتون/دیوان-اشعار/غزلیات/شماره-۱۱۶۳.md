---
title: >-
    شمارهٔ ۱۱۶۳
---
# شمارهٔ ۱۱۶۳

<div class="b" id="bn1"><div class="m1"><p>تا به کی این در زنم در باز کن</p></div>
<div class="m2"><p>با وصالت یک دمم دمساز کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک زمان بنوازم ای جان از وصال</p></div>
<div class="m2"><p>بعد ازین چندانکه خواهی ناز کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ببست او راه وصلش را به ما</p></div>
<div class="m2"><p>ای دل مسکین ز او خو باز کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه چون دف هر دمم دستی زنی</p></div>
<div class="m2"><p>چنگ جانم را دمی درساز کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر توانی گفت حالت با صبا</p></div>
<div class="m2"><p>یک زمانش محرم این راز کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دو چشم بخت من اندر جهان</p></div>
<div class="m2"><p>گر نه شبکوری دو دیده باز کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ببینی بر سر راهش دمی</p></div>
<div class="m2"><p>قصّه عشق مرا آغاز کن</p></div></div>