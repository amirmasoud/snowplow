---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>خوش باشد ار آن دلبر جانانهٔ ما باشد</p></div>
<div class="m2"><p>در بحر غم عشقش دردانهٔ ما باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رغم بداندیشان آخر چه شود کز لطف</p></div>
<div class="m2"><p>آن جان جهان یک شب در خانهٔ ما باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادی نبود ما را جز با شب وصل تو</p></div>
<div class="m2"><p>گویی که غم عشقش هم‌خانهٔ ما باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانه شدم از خویش تا با تو شدم پیوند</p></div>
<div class="m2"><p>زآن رو که به جز عشقت بیگانهٔ ما باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاید که ز جور تو ای نور دو چشم ما</p></div>
<div class="m2"><p>اندر سر هر کویی افسانهٔ ما باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای باد صبا مویی بگشای بیاور تا</p></div>
<div class="m2"><p>تاری ز سر زلفش در شانهٔ ما باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر هر دو جهان بخشند ما را به نظر ناید</p></div>
<div class="m2"><p>ای دوست سر کویت کاشانهٔ ما باشد</p></div></div>