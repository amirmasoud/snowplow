---
title: >-
    شمارهٔ ۶۴۶
---
# شمارهٔ ۶۴۶

<div class="b" id="bn1"><div class="m1"><p>بیشتر خلق جهان در پی جاه و درمند</p></div>
<div class="m2"><p>ز وفا دور و جفاجوی و نه اهل کرمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزگاریست پرآشوب که از خلق جهان</p></div>
<div class="m2"><p>حذر اولیست که یاران وفادار کمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحشتی از همه دارند ندانم که چرا</p></div>
<div class="m2"><p>همچو آهوی چرا گشته ز مردم برمند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر یک نان که مبادا به جهانش سفله</p></div>
<div class="m2"><p>هست امکان که تهیگاه هم از هم بدرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردش دور فلک را چو بقا نیست چرا</p></div>
<div class="m2"><p>آخر از کار جهان جمله چنین بی خبرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده ی قدّ تو گشتند ز جان تا دانی</p></div>
<div class="m2"><p>سرو نازی که به بالای تو اندر چمنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل من آهوی وحشیست که در کوه و درت</p></div>
<div class="m2"><p>گشت سرگشته و عمری که نیامد به کمند</p></div></div>