---
title: >-
    شمارهٔ ۱۲۵۵
---
# شمارهٔ ۱۲۵۵

<div class="b" id="bn1"><div class="m1"><p>عاشقانت را دگر در جست‌و‌جو آورده‌ای</p></div>
<div class="m2"><p>طالبان وصل را در گفت‌و‌گو آورده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکهت عنبر همی‌یابد دماغ جان مگر</p></div>
<div class="m2"><p>ای صبا از زلف دلدارم تو بو آورده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تار زلفت ساختی چوگان به میدان جفا</p></div>
<div class="m2"><p>بی‌دلان را دل به نزد خود چو گوی آورده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکسی دارند عشق چشمه نوشت ولی</p></div>
<div class="m2"><p>بادپیمایان چو خاک ره به کو آورده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چنین محروم و محزون در فراق یار و تو</p></div>
<div class="m2"><p>ای دل آخر با نگارم رو به رو آورده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمنانم را به هجرم شاد و خرّم کرده‌ای</p></div>
<div class="m2"><p>دوستانت را ز دیده خون به جو آورده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نسیم صبحدم آمد ز کویش گفتمش</p></div>
<div class="m2"><p>قصّه درد جهان را مو به مو آورده‌ای</p></div></div>