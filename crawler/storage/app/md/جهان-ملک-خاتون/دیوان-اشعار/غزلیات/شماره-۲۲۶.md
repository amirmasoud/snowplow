---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>که می گوید که چشمش نرگسینست</p></div>
<div class="m2"><p>که می گوید که زلفش عنبرینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیاهی را چه نسبت با دو چشمش</p></div>
<div class="m2"><p>توان کردن دلم زان رو حزینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو ابرویش هلال عید خوانند</p></div>
<div class="m2"><p>رخش را چون بگویم یاسمینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه نسبت زلف او با مشک و عنبر</p></div>
<div class="m2"><p>که بوی او مرا دنیی و دینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو زلفش همچو شب بر روی خورشید</p></div>
<div class="m2"><p>ز سر تا پا هزارش آفرنیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وصلم گر نوازد بهتر آنست</p></div>
<div class="m2"><p>صلاح کار ما باری در اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا گویند ترک عشق او گوی</p></div>
<div class="m2"><p>نمی یارم که یارم نازنینست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دل بیرون نیارم کرد مهرش</p></div>
<div class="m2"><p>که عشقش در دلم نقش نگینست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان گشتم بسی در عشق رویش</p></div>
<div class="m2"><p>مرا مهر رخ او دل گزینست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جفا بر من بسی کرد آن ستمگر</p></div>
<div class="m2"><p>همانا گردش گردون برینست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی سوزد دلش بر حال زارم</p></div>
<div class="m2"><p>چه چاره چونکه آن دل آهنینست</p></div></div>