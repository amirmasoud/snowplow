---
title: >-
    شمارهٔ ۱۳۱۰
---
# شمارهٔ ۱۳۱۰

<div class="b" id="bn1"><div class="m1"><p>الهی تو بگشا به لطفت دری</p></div>
<div class="m2"><p>که منّت نمی خواهم از دیگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه ره راست کج رفته ام</p></div>
<div class="m2"><p>نماید به ما هم رهی رهبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بر سرماست او را سخن</p></div>
<div class="m2"><p>فدای سر یار دارم سری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی من ای خالق ذوالجلال</p></div>
<div class="m2"><p>تو بی منّت خلق بگشا دری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی داوری رفت بر من ز دور</p></div>
<div class="m2"><p>بجز لطف تو نیستم داوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که یارد که حمد و ثنایت کند</p></div>
<div class="m2"><p>اگر چند باشد زبان آوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو گر دوست خواهی چه غم باشدم</p></div>
<div class="m2"><p>اگر خصم باشد مرا کشوری</p></div></div>