---
title: >-
    شمارهٔ ۱۴۰۹
---
# شمارهٔ ۱۴۰۹

<div class="b" id="bn1"><div class="m1"><p>ای روان گشته ز چشمم ز فراقت جویی</p></div>
<div class="m2"><p>ز چه از خون جگر در طلب مه رویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب دیجور به امّید سحر بیدارم</p></div>
<div class="m2"><p>بو که از زلف تو آرد به دماغم بویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به گرد رخ تو زلف چو چوگان دیدم</p></div>
<div class="m2"><p>در سر کوی تو سرگشته منم چون گویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زند ناوک دلدوزم از آن غمزه مست</p></div>
<div class="m2"><p>چه تفاوت کند از دست کمان ابرویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی ندیدست به عالم چو نگارم شوخی</p></div>
<div class="m2"><p>دلبری لاله رخی سنگ دلی مه رویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکل اینست که دل برد ز دستم ناگاه</p></div>
<div class="m2"><p>نیست جز لطف ویم در دو جهان دلجویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه برگشت چو بخت از من بیچاره هنوز</p></div>
<div class="m2"><p>نفروشم به همه ملک جهانش مویی</p></div></div>