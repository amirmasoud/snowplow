---
title: >-
    شمارهٔ ۹۳۱
---
# شمارهٔ ۹۳۱

<div class="b" id="bn1"><div class="m1"><p>دیده بگشادم و دل در سر زلفت بستم</p></div>
<div class="m2"><p>در تو بستم دل و از هر که جهان وارستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای در سنگ فراقت زده ام مسکین من</p></div>
<div class="m2"><p>آه اگر لطف تو ای دوست نگیرد دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مست تو گهی مست و گهی مخمورست</p></div>
<div class="m2"><p>جادویی مست و خرابست و من از وی مستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقم صبر که بر لوح دلم بودی نقش</p></div>
<div class="m2"><p>من به سیلاب سرشک از دل غمگین شستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کمان خم شده پیوسته چو ابروی توام</p></div>
<div class="m2"><p>تا دل شیفته را در خم زلفت بستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عهد بستم که دگر عهد نبندم با کس</p></div>
<div class="m2"><p>وآنچه بستم چو سر زلف خودش بشکستم</p></div></div>