---
title: >-
    شمارهٔ ۴۴۸
---
# شمارهٔ ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>دوای درد دوری صبر دارد</p></div>
<div class="m2"><p>کسی کاو عشق ورزد صبر کارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه عشق و صبر از هم بود دور</p></div>
<div class="m2"><p>ز دیده عاشقی گر خون ببارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بادی کز سر کوی تو خیزد</p></div>
<div class="m2"><p>دلم در خاک راهش جان سپارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پای آن کنم جان را که هرگز</p></div>
<div class="m2"><p>سرش سودای عشق ما ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جانش بنده ام جانی ولیکن</p></div>
<div class="m2"><p>مرا از بندگان کی می شمارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز یاد او دمی خالی نباشم</p></div>
<div class="m2"><p>که در سالی دمی یادم نیارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان و جان فدای دوست کردم</p></div>
<div class="m2"><p>به جز من این دلیری خود که یارد</p></div></div>