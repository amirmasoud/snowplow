---
title: >-
    شمارهٔ ۴۳۵
---
# شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>سر سرگشته سودای تو دارد</p></div>
<div class="m2"><p>هوای قد و بالای تو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری دارد به عالم پر ز سودا</p></div>
<div class="m2"><p>که این سر نیز در پای تو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رای تو بر خون دل ماست</p></div>
<div class="m2"><p>دل بیچاره ام رای تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نور دیده ی مایی نگارا</p></div>
<div class="m2"><p>دلم آخر که بر جای تو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهی سروا تو دانی دیده ی ما</p></div>
<div class="m2"><p>نظر بر قد رعنای تو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد گل به بستان رنگ و بویی</p></div>
<div class="m2"><p>حیا از روی زیبای تو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن کوته کنم فی الجمله امروز</p></div>
<div class="m2"><p>جهانی باز یغمای تو دارد</p></div></div>