---
title: >-
    شمارهٔ ۱۱۰۴
---
# شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>دل را به بوی وصل تو دادیم و می رویم</p></div>
<div class="m2"><p>داغ غمت به سینه نهادیم و می رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل در شکنج زلف تو بستیم و در غمت</p></div>
<div class="m2"><p>خون دل از دو دیده گشادیم و می رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در غم فراق تو جان می رسد به لب</p></div>
<div class="m2"><p>ما بر امید وصل تو شادیم و می رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آبی بر آتش دل ما زن که در غمش</p></div>
<div class="m2"><p>بر خاک راه دوست چو بادیم و می رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبی بر آتش دل ما زن که در غمش</p></div>
<div class="m2"><p>بر خاک راه دوست چو خاکیم و می رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا صیت عشق روی تو در عالم اوفتاد</p></div>
<div class="m2"><p>جان و جهان به یاد تو دادیم و می رویم</p></div></div>