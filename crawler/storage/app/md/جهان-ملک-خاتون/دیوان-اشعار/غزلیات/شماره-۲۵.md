---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>دلبرا تا کی زنی بر جان ما تیغ جفا</p></div>
<div class="m2"><p>بگذر از این رسم جانا مگذر از راه وفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک چشم مست ما چندم جفا بر جان کند</p></div>
<div class="m2"><p>من بلا تا کی کشم از دست آن ترک خطا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال زار من خدا را بازگویی یک به یک</p></div>
<div class="m2"><p>گر گذاری می کنی سوی نگارم ای صبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم از درد فراق روی تو آمد به لب</p></div>
<div class="m2"><p>تا کی ای نامهربان داری مرا از خود جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز هجران حال من آشفته شد چون موی تو</p></div>
<div class="m2"><p>کوری چشم حسودان یک رهم از در درآ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ جان خسته ام گفتا ز درد اشتیاق</p></div>
<div class="m2"><p>یک شب از روی هوس من کردم آهنگ هوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ببینم روی شهرآرای آن زیبا صنم</p></div>
<div class="m2"><p>وصل جان بخشش مگر باشد دل ما را دوا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآنکه گر دردی بود از بار هجران بر دلی</p></div>
<div class="m2"><p>هم شب وصل دلارامش بود چون توتیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوسه ای ده از زکات جسن کاین رنجور عشق</p></div>
<div class="m2"><p>از لب جان پرورت دانم که می یابد شفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون بدیدم روی او را دیده ی جان خیره شد</p></div>
<div class="m2"><p>زآنکه در خورشید دیدم از کجا بد تا کجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه روی از من بگردانید دلبر در جهان</p></div>
<div class="m2"><p>دیدن دیدار او خواهم به زاری از خدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به پابوس تو دست دل نمی یارد رسید</p></div>
<div class="m2"><p>چون ندارد چاره ای از دور می گوید دعا</p></div></div>