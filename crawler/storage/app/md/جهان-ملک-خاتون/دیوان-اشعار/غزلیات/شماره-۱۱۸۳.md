---
title: >-
    شمارهٔ ۱۱۸۳
---
# شمارهٔ ۱۱۸۳

<div class="b" id="bn1"><div class="m1"><p>ای بار غم تو بر دل من</p></div>
<div class="m2"><p>مهر تو سرشته در گل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر چه شود به لطف آسان</p></div>
<div class="m2"><p>از وصل کنی تو مشکل من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ز تو کی شوم شبی دور</p></div>
<div class="m2"><p>بنگر تو خیال باطل من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق رخت نبود جز غم</p></div>
<div class="m2"><p>ای نور دو دیده حاصل من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او سرو سهی و من چو خاکم</p></div>
<div class="m2"><p>آخر ز چه نیست مایل من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم نکند ز ما صبوری</p></div>
<div class="m2"><p>مسکین دل تنگ غافل من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دوست مدام ایستادست</p></div>
<div class="m2"><p>نقش رخ تو مقابل من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خاک شوم مگر که مهرت</p></div>
<div class="m2"><p>بیرون رود از مفاصل من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جمله جهان شوند حوری</p></div>
<div class="m2"><p>جز مهر تو نیست در دل من</p></div></div>