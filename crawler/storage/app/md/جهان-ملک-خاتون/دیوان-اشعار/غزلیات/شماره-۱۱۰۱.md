---
title: >-
    شمارهٔ ۱۱۰۱
---
# شمارهٔ ۱۱۰۱

<div class="b" id="bn1"><div class="m1"><p>چون تو طبیب دردی درد تو با که گویم</p></div>
<div class="m2"><p>درمان درد دوری ای دلبر از که جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبم ببرد عشقت بر باد داد عمرم</p></div>
<div class="m2"><p>از آتش فراقت کمتر ز خاک کویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تاب زلف پرچین چوگان مزن خدا را</p></div>
<div class="m2"><p>کاندر فراق رویت سرگشته تر ز گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بوستان وصلت ای جان من چو بلبل</p></div>
<div class="m2"><p>بر روی چون گل تو آشفته همچو مویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ترک عشق گویم تا روز حشر جانا</p></div>
<div class="m2"><p>ور کوزه گر بسازد از خاک من سبویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانم ز پا درآمد در جست و جوی وصلت</p></div>
<div class="m2"><p>در کوی هجرت آخر تا کی به سر بپویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه ز ما فراغت دارد نگار لیکن</p></div>
<div class="m2"><p>من در جهان به بویش دایم به جست و جویم</p></div></div>