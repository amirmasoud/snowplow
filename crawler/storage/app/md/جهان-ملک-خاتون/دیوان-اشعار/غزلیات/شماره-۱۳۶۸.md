---
title: >-
    شمارهٔ ۱۳۶۸
---
# شمارهٔ ۱۳۶۸

<div class="b" id="bn1"><div class="m1"><p>جانا چه باشد ار نظری سوی ما کنی</p></div>
<div class="m2"><p>امّید خسته ای ز وصالت دوا کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر شب وصال تو امّید بسته ام</p></div>
<div class="m2"><p>جانا تو میل هجر بگو تا چرا کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هستی تو پادشاه ملاحت چه کم شود</p></div>
<div class="m2"><p>از روی لطف گر نظری بر گدا کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیار وعده ای به وفا کرده ای کنون</p></div>
<div class="m2"><p>واجب بود که وعده خود را وفا کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دم به وصل خویش دو دست دلم بگیر</p></div>
<div class="m2"><p>کز پا درآمدیم تو تا کی جفا کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانا طبیب درد دل عاشقان تویی</p></div>
<div class="m2"><p>از لعل لب مگر دل ما را دوا کنی</p></div></div>