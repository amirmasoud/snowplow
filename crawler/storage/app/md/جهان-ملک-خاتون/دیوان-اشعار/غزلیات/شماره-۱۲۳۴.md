---
title: >-
    شمارهٔ ۱۲۳۴
---
# شمارهٔ ۱۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ماهتابی هست و ابرش دست پیرامن زده</p></div>
<div class="m2"><p>در فراقش دست دل هرکس به پیراهن زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نمی تابد مه رویش به ما تدبیر چیست</p></div>
<div class="m2"><p>آتش دل شعله های هجر در خرمن زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه طوفان فراق دوست کاری مشکلست</p></div>
<div class="m2"><p>امّت نوح پیمبر دست بر دامن زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرم می گردد به آتش آهن ای دلبر مگر</p></div>
<div class="m2"><p>آتش آن دل همانا سنگ بر آهن زده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم آه آتشینم هم کند در وی اثر</p></div>
<div class="m2"><p>لعل روح افزای جانان آتشی در من زده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسم نوروز در بستان ز باد صبحدم</p></div>
<div class="m2"><p>غنچه از غوغای بلبل چاک پیراهن زده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل شوریده غوغا در جهان انداخته</p></div>
<div class="m2"><p>گل به حسن خویش مغرورست باری تن زده</p></div></div>