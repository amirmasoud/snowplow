---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>ندانستم که اهلیت گناهست</p></div>
<div class="m2"><p>و یا این ره که می پویم چه راهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جور روزگار و طعن دشمن</p></div>
<div class="m2"><p>جهان پیش جهان بینم سیاهست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه هر مردی تواند کرد مردی</p></div>
<div class="m2"><p>سواری شیر دل پشت سپاهست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسان را بر در هرکس پناهست</p></div>
<div class="m2"><p>مرا بر درگه لطفش پناهست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر آهی کشم در هم کشد روی</p></div>
<div class="m2"><p>مگر آیینه را تندی ز آهست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال آن بت خورشید پیکر</p></div>
<div class="m2"><p>جهان پیما و شب رو همچو ماهست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چون در خلوت وصلی چه دانی</p></div>
<div class="m2"><p>که مسکینی ز هجرت دادخواهست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگار ماه رویم را ز خوبی</p></div>
<div class="m2"><p>هزاران یوسف مصری به چاهست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا رحمت نیارد بر گدایان</p></div>
<div class="m2"><p>چو دایم بر جهان او پادشاهست</p></div></div>