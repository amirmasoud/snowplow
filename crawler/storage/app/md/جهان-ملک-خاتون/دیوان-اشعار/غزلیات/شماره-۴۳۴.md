---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>نگار من دلی چون سنگ دارد</p></div>
<div class="m2"><p>ز نام عاشقانش ننگ دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بر آتش هجران او سوخت</p></div>
<div class="m2"><p>کنون باد از غمش در چنگ دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا با او سر صلحست و یاری</p></div>
<div class="m2"><p>چرا با ما همیشه جنگ دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست روز هجرانش خدا را</p></div>
<div class="m2"><p>دلم را چون دهانش تنگ دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهی سرویست در باغ دل ما</p></div>
<div class="m2"><p>به دستان حیله و نیرنگ دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا بی جرمی آن یار ستمگر</p></div>
<div class="m2"><p>به خون جان ما آهنگ دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشا حال دلی کاندر جهان او</p></div>
<div class="m2"><p>دو گوش و هوش و رو در چنگ دارد</p></div></div>