---
title: >-
    شمارهٔ ۹۶۰
---
# شمارهٔ ۹۶۰

<div class="b" id="bn1"><div class="m1"><p>من به لطفت امید می دارم</p></div>
<div class="m2"><p>مگر از غم کنی سبکبارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ندارم بجز تو کس به جهان</p></div>
<div class="m2"><p>ضایع از لطف خویش مگذارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه من جلوه داد در چشمم</p></div>
<div class="m2"><p>اوست یا خود خیال پندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به جان آمد ای مسلمانان</p></div>
<div class="m2"><p>از جفای بت ستمکارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به امّید روی چون خورشید</p></div>
<div class="m2"><p>شب همه تا به صبح بیدارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چه میلی ندارد او سوی ما</p></div>
<div class="m2"><p>تخم مهرش میان جان کارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ندارم تمتّعی ز وصال</p></div>
<div class="m2"><p>تا نگویی که من جهان دارم</p></div></div>