---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>راست گویم مُرْوَت از عالم برفت</p></div>
<div class="m2"><p>شرم از چشم بنی آدم برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم کم و بیشی وفا در خلق بود</p></div>
<div class="m2"><p>آن نشد بیش این زمان و کم برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدمم غم بود اندر هجر تو</p></div>
<div class="m2"><p>شکر کز وصل توام همدم برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا دردی که بودم از غمت</p></div>
<div class="m2"><p>یافتم وصل ترا دردم برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد امّا پیش ما ننشست دوست</p></div>
<div class="m2"><p>پرسشی فرمود و هم دردم برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از فراقش باز بر خاکم نشاند</p></div>
<div class="m2"><p>در غمش از دیده ی ما دم برفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتاب روز وصلش باز شد</p></div>
<div class="m2"><p>بار دیگر از جهان شبنم برفت</p></div></div>