---
title: >-
    شمارهٔ ۱۰۷۰
---
# شمارهٔ ۱۰۷۰

<div class="b" id="bn1"><div class="m1"><p>شبت خوش باد همچون روز خرّم</p></div>
<div class="m2"><p>چو حال من مبادت کار در هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دست غم به جان آمد دل من</p></div>
<div class="m2"><p>مبادا غم کسی را یار و عمدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الا ای خوش نسیم صبحگاهی</p></div>
<div class="m2"><p>تویی مر عاشقان را یار و محرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو با آن نگار سست پیمان</p></div>
<div class="m2"><p>چرا پشت دلم را کرده ای خم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی خوش نگذرد بر ما ز هجران</p></div>
<div class="m2"><p>ز بالایت بلا بینیم هردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سوی ما خرام ای سرو آزاد</p></div>
<div class="m2"><p>مبادا از سر ما سایه ات کم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا از وصل خود شادم نداری</p></div>
<div class="m2"><p>که بگرفتم ملال از صحبت غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرنجانم به هجرانت از این بیش</p></div>
<div class="m2"><p>ز وصل خود مرا بنواز یک دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار آمد بیا و تازه دل باش</p></div>
<div class="m2"><p>جهان از باد نوروزیست خرّم</p></div></div>