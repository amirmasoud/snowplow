---
title: >-
    شمارهٔ ۱۳۵۴
---
# شمارهٔ ۱۳۵۴

<div class="b" id="bn1"><div class="m1"><p>نگارا چون قلم ما را به سر تا کی بگردانی</p></div>
<div class="m2"><p>مگر حال من مسکین سرگردان نمی دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زلف خویشتن ما را مکن سودازده جانا</p></div>
<div class="m2"><p>که در هجرت به جان آمد جهانی از پریشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا دردیست در عشقت که تا جانم به تن باشد</p></div>
<div class="m2"><p>طبیب من تویی آخر چرا فارغ ز درمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و جان و قرار و هوش و صبر و عقل</p></div>
<div class="m2"><p>به باد عشق بردادم تمام از روی نادانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بر باد بردادی و آتش در من افکندی</p></div>
<div class="m2"><p>نه گویی تا به کی ما را چو خاک از دامن افشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن بر من ستم زین پس که کس این ظلم نپسندد</p></div>
<div class="m2"><p>کنون ترسم که همچون من به درد دل فرو مانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بیچاره می دانم که باری تا غم هجران</p></div>
<div class="m2"><p>نهادی بر دلم داغی که آن داغیست سلطانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بلبل در قفس دایم چرا داری دلم در بند</p></div>
<div class="m2"><p>قفس را بشکند روزی ز بار غصّه زندانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهانی سر به سر اندوه و بار غصّه می بینم</p></div>
<div class="m2"><p>چرا این نازنین آخر ملولی از جهانبانی</p></div></div>