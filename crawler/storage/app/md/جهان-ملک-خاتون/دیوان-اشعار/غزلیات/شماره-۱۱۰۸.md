---
title: >-
    شمارهٔ ۱۱۰۸
---
# شمارهٔ ۱۱۰۸

<div class="b" id="bn1"><div class="m1"><p>از وصل دری گشا به رویم</p></div>
<div class="m2"><p>کاشفته به روی تو چو مویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر لطف و کرم کنی توانی</p></div>
<div class="m2"><p>ور جور و جفا کنی چه گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از باده ی عشق مست گردد</p></div>
<div class="m2"><p>گر کوزه گری کند سبویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک در تو ز آتش عشق</p></div>
<div class="m2"><p>رخساره به آب دیده شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دست جفا و جورت ای جان</p></div>
<div class="m2"><p>سرگشته ز هجر تو چو گویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بوی تو باد صبح مستست</p></div>
<div class="m2"><p>من زنده از آن حیات بویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایم چو جهان به جست و جویت</p></div>
<div class="m2"><p>باشد همه روز گفت و گویم</p></div></div>