---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>تو بگو که چونم از تو دمکی گزیر باشد</p></div>
<div class="m2"><p>ز رخی که همچو خورشید و مهی منیر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دین و جسم و جانم چو تویی بگو که ما را</p></div>
<div class="m2"><p>بجز از خیال روی تو چه در ضمیر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سر ار چو گو بگردم ز در تو برنگردم</p></div>
<div class="m2"><p>اگر از جفات بر ما همه تیغ و تیر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدوان سمند هجران به شکستگان بی دل</p></div>
<div class="m2"><p>سبب آنکه ناله زار لگام گیر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر از سر عنایت سوی ما عنان گرایی</p></div>
<div class="m2"><p>به فدای خاک پایت سر و جان حقیر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کرا کند سر و جان به فدای خاک پایت</p></div>
<div class="m2"><p>به جهان مگر دعایی ز من فقیر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جهان تو می پسندم نه چنان نیازمندم</p></div>
<div class="m2"><p>به رخ چو مهرت ای جان که صفت پذیر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دم صبح باری امروز نسیم پیرهن داشت</p></div>
<div class="m2"><p>به غلط اگر نیفتم نفس بشیر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز غمش خبر ندارم به فراق آن دلارام</p></div>
<div class="m2"><p>حجرم به زیر پهلو همه چون حریر باشد</p></div></div>