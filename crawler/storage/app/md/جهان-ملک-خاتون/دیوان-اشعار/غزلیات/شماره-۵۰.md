---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>قد تو سر کشد از جمله سرو بستان‌ها</p></div>
<div class="m2"><p>رخ تو طعنه زند بر گل گلستان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشید سر ز من خسته‌دل چو سرو روان</p></div>
<div class="m2"><p>ببرد دل ز برم آن صنم به دستان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبوح روی تو خورشید عالم‌آرای است</p></div>
<div class="m2"><p>رخ چو ماه تو شمع همه شبستان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی چون گل خود صبحدم نمی‌شنوی</p></div>
<div class="m2"><p>خروش بلبل و بانگ هزار دستان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزید باد بهاری جهان منوّر شد</p></div>
<div class="m2"><p>نمی‌کشد دلم الّا به سوی بستان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهار و لاله و گل چون دمید در بستان</p></div>
<div class="m2"><p>جمال طلعت زیبای تو شکست آن‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا که سیب زنخدان تو علاج دلست</p></div>
<div class="m2"><p>کجا برم به جهان جمله بار بستان‌ها</p></div></div>