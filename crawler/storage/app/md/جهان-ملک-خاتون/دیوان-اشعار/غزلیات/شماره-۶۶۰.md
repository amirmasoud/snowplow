---
title: >-
    شمارهٔ ۶۶۰
---
# شمارهٔ ۶۶۰

<div class="b" id="bn1"><div class="m1"><p>بی رخ تو نمی توانم بود</p></div>
<div class="m2"><p>زآنکه وصل تو چون روانم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدن روی تو به جان جستم</p></div>
<div class="m2"><p>تا مرا طاقت و توانم بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ببردی و ترک ما گفتی</p></div>
<div class="m2"><p>بر تو ای جان کی آن گمانم بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن قد همچو سرو و آن لب لعل</p></div>
<div class="m2"><p>مونس جان ناتوانم بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی هرکه بگذشتم</p></div>
<div class="m2"><p>از غم عشق داستانم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سر و جان تو که با غم عشق</p></div>
<div class="m2"><p>خاطری فارغ از جهانم بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فاش شد در جهان تو تا دانی</p></div>
<div class="m2"><p>با تو سرّی که در نهانم بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر من خسته دل کناره گرفت</p></div>
<div class="m2"><p>دست سروی که در میانم بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو ز من فارغ و من از غم تو</p></div>
<div class="m2"><p>همه شب سر بر آستانم بود</p></div></div>