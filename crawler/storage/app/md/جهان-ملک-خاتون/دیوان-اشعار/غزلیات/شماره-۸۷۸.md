---
title: >-
    شمارهٔ ۸۷۸
---
# شمارهٔ ۸۷۸

<div class="b" id="bn1"><div class="m1"><p>جهان خرّم و ما چنین تنگ دل</p></div>
<div class="m2"><p>ز جور بتی مهوش سنگ دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر هست چشم خوش او خمار</p></div>
<div class="m2"><p>مرا او گرفتست در چنگ دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا میل بر صلح و در وصل دوست</p></div>
<div class="m2"><p>ورا صلح بر هجر و بر جنگ دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مطرب زند راه وصلش ز جان</p></div>
<div class="m2"><p>به صوتم دو گوش و به آهنگ دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی را که بهرام باشد نوند</p></div>
<div class="m2"><p>چگونه دهد او بجز جنگ دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بخت یاری دهد دلبرا</p></div>
<div class="m2"><p>بشویم به فضل تو از زنگ دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان در سر کار او شد خراب</p></div>
<div class="m2"><p>از آنم چنین خسته و تنگ دل</p></div></div>