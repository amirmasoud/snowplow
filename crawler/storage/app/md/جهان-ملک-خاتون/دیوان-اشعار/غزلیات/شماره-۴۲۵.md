---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>بگو کجا برم از دست هجر تو فریاد</p></div>
<div class="m2"><p>که کند خانه صبرم ز بیخ و از بنیاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان و داد که پیچید دست طاقت من</p></div>
<div class="m2"><p>به جان رسید دل خسته ی من از بیداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه در زمانه وفا و نه بر سپهر امید</p></div>
<div class="m2"><p>فلک جفای تو تا کی کشم که شرمت باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که یک نفس از یاد تو نیاساید</p></div>
<div class="m2"><p>روا بود که تو او را گذاشتی از یاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سرو گوشه گرفتم که از جفا برهم</p></div>
<div class="m2"><p>ولی ز غصّه دوران نمی شوم آزاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر غصّه ندارم قرین ز کار جهان</p></div>
<div class="m2"><p>نمی شوم ز زمانه زمانکی دلشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز غصّه جان عزیزم به لب رسید به غم</p></div>
<div class="m2"><p>کنون اگر نرسی خود کیم رسد فریاد</p></div></div>