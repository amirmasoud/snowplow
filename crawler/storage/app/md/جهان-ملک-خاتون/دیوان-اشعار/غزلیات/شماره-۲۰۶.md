---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>دیگر دلم از دست تو فریاد زنانست</p></div>
<div class="m2"><p>دل بردی و بیچاره کنون در غم جانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بردی و جان در صدف هجر نهادی</p></div>
<div class="m2"><p>مشکل که تو را میل به سوی دگرانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازآی و به سرچشمه ی جانم گذری کن</p></div>
<div class="m2"><p>در دیده ی ما جای چنان سرو روانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرخ زر و گوهر شده ارزان ز غم هجر</p></div>
<div class="m2"><p>بر روی چو زر اشک چو سیماب روانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من قالب بی روحم و جان از بر ما دور</p></div>
<div class="m2"><p>زیرا که مرا جان و دل آن روح و روانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دست که در گردن دلدار حمایل</p></div>
<div class="m2"><p>بودیم کنون بر دلم از غصه زنانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر مرد که او گشت گرفتار بلایی</p></div>
<div class="m2"><p>زنهار یقین دان که ز کردار زنانست</p></div></div>