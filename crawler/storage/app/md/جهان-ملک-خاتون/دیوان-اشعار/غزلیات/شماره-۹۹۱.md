---
title: >-
    شمارهٔ ۹۹۱
---
# شمارهٔ ۹۹۱

<div class="b" id="bn1"><div class="m1"><p>بگفتی هم شبی جانا نظر بر حالت اندازم</p></div>
<div class="m2"><p>ز روی مردمی یک دم به حال دوست پردازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا دست دلم گیر و شبی از وصل ای دلبر</p></div>
<div class="m2"><p>میان جان من بنشین که سر در پات اندازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای کوی وصل او بلند افتاده است ای دل</p></div>
<div class="m2"><p>اگرچه زین هوس دایم گرفته همچو شهبازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین امّید عمرم شد به باد و یاد می نارد</p></div>
<div class="m2"><p>که ما را بود مسکینی زهی دلدار طنّازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگ و روی چون گلنار خواب چشم ما بردی</p></div>
<div class="m2"><p>ز زلف خویشتن نعلی در آتش کرده ای بازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دام زلف تو جانم مقید گشت تا دانی</p></div>
<div class="m2"><p>سرافکندست زلف تو ولی من زان سرافرازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو دف تا کی مرا در دست هر ناجنس بگذاری</p></div>
<div class="m2"><p>چو چنگم گر زنی باری زمانی نیز بنوازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عودم بر سر آتش ز عشق رویت ای دلبر</p></div>
<div class="m2"><p>اگرچه همچو نی فاش است از آوازه ات رازم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر جان خواهی ای دلبر و گر سر خواهی ای سرور</p></div>
<div class="m2"><p>نتابم سر، جهان و جان به فرمان تو در بازم</p></div></div>