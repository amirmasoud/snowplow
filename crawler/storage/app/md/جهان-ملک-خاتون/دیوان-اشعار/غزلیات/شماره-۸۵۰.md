---
title: >-
    شمارهٔ ۸۵۰
---
# شمارهٔ ۸۵۰

<div class="b" id="bn1"><div class="m1"><p>دغدغه ی وصل تو چون برود از دماغ</p></div>
<div class="m2"><p>کیست که از عشق تو بر دل او نیست داغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ غم عشق تو بر همه دلها بود</p></div>
<div class="m2"><p>لیک مزاج تو را هست ز عالم فراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل چو رخت کی شکفت در چمن و گلستان</p></div>
<div class="m2"><p>سرو چو قدت نرست در همه بستان و باغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ جهان بی توأم هست چو زندان ولی</p></div>
<div class="m2"><p>با رخ گلرنگ تو فارغم از باغ و راغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر رخت دلبرا هیچ تو دانی که چیست</p></div>
<div class="m2"><p>در تن من چون روان چشم مرا چون چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکهت زلفت فرست سوی من از صبحدم</p></div>
<div class="m2"><p>تا که بیاسایدم از سر زلفت دماغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبر دلخواه را این دو جهت عالیست</p></div>
<div class="m2"><p>روی وفا در زوال حسن گرفته بلاغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل بشد از بوستان خار جفا بردمید</p></div>
<div class="m2"><p>داد کنون بوستان شحنگی خود به زاغ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل و کبک و تذرو از در بستان برفت</p></div>
<div class="m2"><p>بین که جهان چون گرفت قمری و زاغ و کلاغ</p></div></div>