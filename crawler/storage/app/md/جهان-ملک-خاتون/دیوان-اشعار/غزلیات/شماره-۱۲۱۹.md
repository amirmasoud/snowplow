---
title: >-
    شمارهٔ ۱۲۱۹
---
# شمارهٔ ۱۲۱۹

<div class="b" id="bn1"><div class="m1"><p>ای صبا گرزان نگار ما خبر داری بگو</p></div>
<div class="m2"><p>بر در آن یار سنگین دل گذر داری بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز حال من بپرسد آن نگار سنگ دل</p></div>
<div class="m2"><p>چون تو حال زار زار من زبر داری بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بگوید حال آن بیچاره مسکین چه شد</p></div>
<div class="m2"><p>گو به حال و کار زارش گر نظر داری بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به حال ما نظر خواهی فکندن بعد ازین</p></div>
<div class="m2"><p>ور نمی اندازی و یاری دگر داری بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل محزون نظر کن ناوک دلدوز را</p></div>
<div class="m2"><p>پیش تیغ کافرش گر جان سپر داری بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان باری ندارم جز تو محبوبی دگر</p></div>
<div class="m2"><p>دلبرا با ما سر یاری اگر داری بگو</p></div></div>