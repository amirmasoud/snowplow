---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>مرا دلبند و مونس غیر غم نیست</p></div>
<div class="m2"><p>ترا سرمایه جز جور و ستم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا چون تو نباشد در جهان یار</p></div>
<div class="m2"><p>ترا بهتر ز من دلدار کم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر روی ترا در خواب بینم</p></div>
<div class="m2"><p>ز بخت سرکش خود باورم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان آمد دل من از جفایت</p></div>
<div class="m2"><p>که بر جای تو جز غم در برم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلال عید اگر چه خوش هلالیست</p></div>
<div class="m2"><p>چو طاق ابروی دلدار خم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خبرداری نگارینا به جانت</p></div>
<div class="m2"><p>که در هجران تو خواب و خورم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درم خواهد ز من درویش گفتم</p></div>
<div class="m2"><p>بگویم نی گرم هست و گرم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کریمان را کرم باقیست لیکن</p></div>
<div class="m2"><p>چه چاره چون به دست اندر درم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تهی دستم چو سرو آزاد آری</p></div>
<div class="m2"><p>درم جایی بود کانجا کرم نیست</p></div></div>