---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>سرو از قد و قامت تو پستست</p></div>
<div class="m2"><p>بر خاک ره از غمت نشستست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که گل مرا ز بنیاد</p></div>
<div class="m2"><p>از آب و هوای تو سرشتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر بگذشت آب چشمم</p></div>
<div class="m2"><p>اینم به فراق سرگذشتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این عادت و خوی و بوی کاوراست</p></div>
<div class="m2"><p>ز آدم نبود که او فرشتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تخم غم مهر خویش گویی</p></div>
<div class="m2"><p>در جان رهی به عشق کشتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی به جفا جهانی آخر</p></div>
<div class="m2"><p>یک روز نگفته ای که زشتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حال دل خویش چون بگویم</p></div>
<div class="m2"><p>گویی تو که اینش سرنبشتست</p></div></div>