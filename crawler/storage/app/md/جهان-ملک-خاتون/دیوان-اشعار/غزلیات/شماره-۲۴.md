---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>این جهان بی وفا با کس نکردست او وفا</p></div>
<div class="m2"><p>درد از او بسیار باشد زو نمی آید دوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر درختی کاو کشد سر بر فلک از بار و برگ</p></div>
<div class="m2"><p>هم بریزاند به قهرش عاقبت باد فنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صدف پنهان شد اندر بحر بی پایان چه غم</p></div>
<div class="m2"><p>گوهر ذات شریفت را همی خواهم بقا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهری شهوار از دریای لطف آمد برفت</p></div>
<div class="m2"><p>کاندر این عالم نمی داند کسی او را بها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یارب آن درّ معانی را ز لطف بی دریغ</p></div>
<div class="m2"><p>در میان درج اقبالش نگه دارد خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر معبودی ز جان گویم که بنّای ازل</p></div>
<div class="m2"><p>گلشن اقبال عمرت را کنون کرده بنا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز هیجا در نبردت رستم و اسفندیار</p></div>
<div class="m2"><p>گرد نعل مرکبت کردند باری توتیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز دعای دولتت ورد زبانم هیچ نیست</p></div>
<div class="m2"><p>آخر از دست فقیران خود چه خیزد جز دعا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد از این تصدیع و گستاخی که کردم در جهان</p></div>
<div class="m2"><p>من دعای دولتت گویم بجز مدح و ثنا</p></div></div>