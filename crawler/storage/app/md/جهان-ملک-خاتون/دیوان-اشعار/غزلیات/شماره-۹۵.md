---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>ای سرو چمن چو قامتت راست</p></div>
<div class="m2"><p>با ما تو بگو که آن چه بالاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میلت سوی ما بدی همیشه</p></div>
<div class="m2"><p>این سرکشی تو از چه برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنهار تو سرمکش ز ما زانک</p></div>
<div class="m2"><p>سرسبزی سرو نیز از ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتا که مکن خیال باطل</p></div>
<div class="m2"><p>کاین عشق و هوس تو را نه تنهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمی چو نکرد او بر آن دل</p></div>
<div class="m2"><p>گفتم نه دلست سنگ خاراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دود دلم بُتا حذر کن</p></div>
<div class="m2"><p>کش میل همیشه سوی بالاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با قد خوش تو سرو بستان</p></div>
<div class="m2"><p>دعوی نکند ورا چه یاراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای ماه دو هفته با همه حسن</p></div>
<div class="m2"><p>از شرم رخ تو در کم و کاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دوست در آرزوی رویت</p></div>
<div class="m2"><p>دانم که نخفت دیده شبهاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیچاره دل ضعیف ما را</p></div>
<div class="m2"><p>از لعل تو بوسه ای تمنّاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتا به جهان تو یار بودیم</p></div>
<div class="m2"><p>گفتم تو بگو که از چه پیداست</p></div></div>