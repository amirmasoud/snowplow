---
title: >-
    شمارهٔ ۷۹۰
---
# شمارهٔ ۷۹۰

<div class="b" id="bn1"><div class="m1"><p>به دست دل نیفتاده چو تو حوری‌وشی هرگز</p></div>
<div class="m2"><p>سهی سرو دلارامی نگاری سرکشی هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لعل آن لب شیرین به رخسار چو خورشیدت</p></div>
<div class="m2"><p>نیفتادست در جانم بدینسان آتشی هرگز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروناید دلم باری به صورتهای بی معنی</p></div>
<div class="m2"><p>به صورت کی بود چون تو به معنی مهوشی هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مخموری آن چشمت نباشد در جهان نرگس</p></div>
<div class="m2"><p>که دیده مست و مخموری چو چشمت سرخوشی هرگز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه دُرد درد تو بسی نوشیده ام لیکن</p></div>
<div class="m2"><p>ندیدم در شراب آن دو لعل لب غشی هرگز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه جز جفا از دل نیاید بر من مسکین</p></div>
<div class="m2"><p>ز سر بیرون نکردم من هوای دلکشی هرگز</p></div></div>