---
title: >-
    شمارهٔ ۸۶۱
---
# شمارهٔ ۸۶۱

<div class="b" id="bn1"><div class="m1"><p>بر لب آمد جان ما از اشتیاق</p></div>
<div class="m2"><p>شد چو صبرم تلخ از هجران مذاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دو چشم پر غمم خون می چکد</p></div>
<div class="m2"><p>یک زمان از شوق و یک دم از فراق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فراق روی خوبت ای صنم</p></div>
<div class="m2"><p>با غمت جفتست دل و ز صبر طاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوبرویان را وفا کمتر بود</p></div>
<div class="m2"><p>با وفا حسنش نباشد اتّفاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اتّفاقی باید اندر دوستی</p></div>
<div class="m2"><p>تا به کی باشد میان ما نفاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه یارم فارغست از مخلصان</p></div>
<div class="m2"><p>ما به روی دوست داریم اشتیاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو کامم برنیاری در جهان</p></div>
<div class="m2"><p>دلبرا حکمی بود مالا یطاق</p></div></div>