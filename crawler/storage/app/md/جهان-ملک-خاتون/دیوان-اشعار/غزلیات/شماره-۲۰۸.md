---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>دیده ام در رخ جان پرور تو حیرانست</p></div>
<div class="m2"><p>زآنکه حسن رخت امروز دو صد چندانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسته ی روز فراقت شده ام مسکین من</p></div>
<div class="m2"><p>که بیا لعل شکرخای تواش درمانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح وصل تو ندیدیم و بشد عمر در آن</p></div>
<div class="m2"><p>مگر ای دوست شب هجر تو بی پایانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ی بخت من غمزده ی شوریده</p></div>
<div class="m2"><p>سالها تا ز غم عشق رخت گریانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر رخسار چو خورشید تو اندر دل ما</p></div>
<div class="m2"><p>به سرو جان تو سوگند که صد چندانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدّتی تا به هوای قد آن سرو بلند</p></div>
<div class="m2"><p>مرغ جانم به سر کوی تو در طیرانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادم امروز بده از شب وصلت زیراک</p></div>
<div class="m2"><p>خانه ی عمر من از جور جهان ویرانست</p></div></div>