---
title: >-
    شمارهٔ ۴۷۵
---
# شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>بیش از این با من بیچاره جفا نتوان کرد</p></div>
<div class="m2"><p>با وجود ستمش ترک وفا نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون طبیب من دلخسته تو باشی چه کنم</p></div>
<div class="m2"><p>درد خود را ز تو ای دوست نهان نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فراق رخ چون ماه تو ای نور دو چشم</p></div>
<div class="m2"><p>غیر خون جگر از دیده روان نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ما برد رخ و لعل تو ای دوست ولی</p></div>
<div class="m2"><p>تکیه بر آتش و بر آب روان نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشتیاقی که مرا هست به دیدار رخت</p></div>
<div class="m2"><p>شرح آن ای دل و دینم به زبان نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایمردی کن و دریاب که از درد فراق</p></div>
<div class="m2"><p>بیش از این بر سر کوی تو فغان نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم به فریاد من خسته ی بیچاره برس</p></div>
<div class="m2"><p>دل چو بردی ز برم قصد به جان نتوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان و دل هر دو زیانست مرا در غم تو</p></div>
<div class="m2"><p>لیکن اندیشه ی این سود و زیان نتوان کرد</p></div></div>