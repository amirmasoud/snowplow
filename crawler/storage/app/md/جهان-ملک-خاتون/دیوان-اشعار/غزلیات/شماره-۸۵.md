---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>بیا که پیک نظر می دوانم از چپ و راست</p></div>
<div class="m2"><p>که تا کجا چو تو سروی به باغ جان برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و یا گلی که به روی تو باشدش نسبت</p></div>
<div class="m2"><p>چگونه صحن چمن را به حسن خویش آراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگشت گرد جهان و ندید چون رخ تو</p></div>
<div class="m2"><p>گلی و نیز چو قدّ تو هیچ سرو نخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر کنی گذری سوی ما نباشد عیب</p></div>
<div class="m2"><p>تو سرو جانی و دایم ترا گذر بر ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببرده ای دلم از دست و باز پس ندهی</p></div>
<div class="m2"><p>چه اوفتاد چه شد در جهان مگر یغماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا بیا و غنیمت شمر یکی امروز</p></div>
<div class="m2"><p>که را امید بقا ای عزیز بر فرداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلال وصل زمانی بر آتش دل زن</p></div>
<div class="m2"><p>ز شور عشق تو جانا به عالمی غوغاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل از فراق تو بر آتش بلا سوزان</p></div>
<div class="m2"><p>دو دیده از غم هجر تو دایماً دریاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلت نسوخت به حال من ضعیف نحیف</p></div>
<div class="m2"><p>نه دل بود نه دل آن دل مگر که آن خاراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فراق جمله رفیقان زمانه می طلبد</p></div>
<div class="m2"><p>بدید عاقبت الامر آنچه دشمن خواست</p></div></div>