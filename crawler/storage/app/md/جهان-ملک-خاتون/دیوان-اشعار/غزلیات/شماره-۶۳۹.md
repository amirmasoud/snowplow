---
title: >-
    شمارهٔ ۶۳۹
---
# شمارهٔ ۶۳۹

<div class="b" id="bn1"><div class="m1"><p>هر دم که جان وصال تو را یاد می کند</p></div>
<div class="m2"><p>از غصّه جهان دلم آزاد می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت بریخت خون دل مردمان به زجر</p></div>
<div class="m2"><p>آن شوخ دیده بین که چه بیداد می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو قدت چو بگذرد اندر میان باغ</p></div>
<div class="m2"><p>بالاش ناز با قد شمشاد می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صبحدم به سوی گلستان گذار کن</p></div>
<div class="m2"><p>بلبل ز گل شنو که چه فریاد می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسکین دل ضعیف نحیفم به هر نفس</p></div>
<div class="m2"><p>بر وعده وصال تو دل شاد می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بد که گوید از من دلخسته ام چه سود</p></div>
<div class="m2"><p>مشنو تو زینهار که افساد می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل ز بهر گل بکشد جور بر هزار</p></div>
<div class="m2"><p>خسرو ببین چه ظلم به فرهاد می کند</p></div></div>