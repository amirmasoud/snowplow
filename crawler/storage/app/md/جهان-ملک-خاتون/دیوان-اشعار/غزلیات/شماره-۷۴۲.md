---
title: >-
    شمارهٔ ۷۴۲
---
# شمارهٔ ۷۴۲

<div class="b" id="bn1"><div class="m1"><p>بی تکلّف خوشست بوی بهار</p></div>
<div class="m2"><p>ناله ی بلبلان و بانگ هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه و جویبار و طرف چمن</p></div>
<div class="m2"><p>در صبوحی چه خوش بود با یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی در روی دوستان کرده</p></div>
<div class="m2"><p>وز جفاهای دشمنان به کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل رویش به صبح می خواهم</p></div>
<div class="m2"><p>از دل و جان به غیر صحبت خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قامتی همچو سرو در بستان</p></div>
<div class="m2"><p>دلبری خوش حضور شیرین کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار ما تند و سرکشست و دلم</p></div>
<div class="m2"><p>از جفا گشت از جهان بیزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بهشت برین نمی خواهم</p></div>
<div class="m2"><p>گر نباشد مرا درو دیدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته بودم بگو مرا باری</p></div>
<div class="m2"><p>حاصل من نبود جز پندار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خطت سر نهاده ام چو قلم</p></div>
<div class="m2"><p>سر دوانم مکن تو چون پرگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جهانم امید بر در تست</p></div>
<div class="m2"><p>ناامیدم مکن ز خود زنهار</p></div></div>