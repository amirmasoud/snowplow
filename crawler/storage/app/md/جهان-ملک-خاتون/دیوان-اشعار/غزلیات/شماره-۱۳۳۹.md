---
title: >-
    شمارهٔ ۱۳۳۹
---
# شمارهٔ ۱۳۳۹

<div class="b" id="bn1"><div class="m1"><p>مرا خود نباشد به عالم دلی</p></div>
<div class="m2"><p>کزو باشدم یک زمان حاصلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی در سرابوستان گل بود</p></div>
<div class="m2"><p>ولی همچو من کی بود بلبلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل آزاری خلق ازین پس مجوی</p></div>
<div class="m2"><p>به دست آر اگر می توانی دلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آن بلبل شوخ دیده ببین</p></div>
<div class="m2"><p>که چون می رباید ز بستان گلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو سرو سهی بین بدین سرکشی</p></div>
<div class="m2"><p>ز حسرت فرو برده پا در گلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غریقم به دریای هجران تو</p></div>
<div class="m2"><p>چه غم باشدت بر لب ساحلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چرخ بلا درفتادی جهان</p></div>
<div class="m2"><p>چه حاصل ز اندیشه باطلی</p></div></div>