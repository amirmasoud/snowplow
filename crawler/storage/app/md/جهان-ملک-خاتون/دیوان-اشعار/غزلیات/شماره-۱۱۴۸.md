---
title: >-
    شمارهٔ ۱۱۴۸
---
# شمارهٔ ۱۱۴۸

<div class="b" id="bn1"><div class="m1"><p>بیا ای سرو جان من کنار چشم ما جا کن</p></div>
<div class="m2"><p>وگر در چشمه ننشینی درون جان تو مأوا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حد بردی جفا بر من نمی پرسی شبی حالم</p></div>
<div class="m2"><p>که گفتت این چنین جانا جفا چندین تو بر ما کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو تا کی بسته ای بر ما در شادی بگو جانا</p></div>
<div class="m2"><p>بیا وز وصل جان پرور به روی ما دری وا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رفتن ای دل و جانم چنین مشتاب از پیشم</p></div>
<div class="m2"><p>ز روی مردمی آخر دمی با ما مدارا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی گویم به دلبندی به وصلم می رسان هر شب</p></div>
<div class="m2"><p>همی گویم که گه گاهی نظر بر ما خدا را کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدش چون سرو بستانی بدید افکند سر در پیش</p></div>
<div class="m2"><p>خجل شد گفتمش سروا زمانی سر به بالا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سرو ناز می گویم اگر دلدار من روزی</p></div>
<div class="m2"><p>خرامد در چمن خود را فدای قد رعنا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به گل گفتم اگر بینی رخ گلرنگ دلدارم</p></div>
<div class="m2"><p>چو بلبل هر نفس تحسین آن رخسار زیبا کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرافکندست نرگس در میان باغ و می گویم</p></div>
<div class="m2"><p>برآور سر مشو محزون نظر در چشم شهلا کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلا تا کی تو سرگردان چنین گرد جهان گردی</p></div>
<div class="m2"><p>وطن در شست زلفین بتی دلخواه پیدا کن</p></div></div>