---
title: >-
    شمارهٔ ۷۰۴
---
# شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>اگر نقاب بت من ز چهره بگشاید</p></div>
<div class="m2"><p>بسا دلی که به شوخی به غمزه برباید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لوح خاطر من یاد دوست نتوان برد</p></div>
<div class="m2"><p>ولی اگر بکند یاد ما دمی شاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا به گردش و دور زمانه باید ساخت</p></div>
<div class="m2"><p>نه آنچنان که بباید چنانکه می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا بیار ز زلف نگار ما بویی</p></div>
<div class="m2"><p>که تا دماغ دل من از آن بیاساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان نگار جفاجوی ما بگو تا کی</p></div>
<div class="m2"><p>فراق خون دلم را ز دیده پالاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا به خون من خسته دل کمر بستست</p></div>
<div class="m2"><p>نیرزد آنکه دو دستش به خون بیالاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هلال طاق دو ابروی او عظیم خوشست</p></div>
<div class="m2"><p>چه حاجتست که آن را به وسمه آراید</p></div></div>