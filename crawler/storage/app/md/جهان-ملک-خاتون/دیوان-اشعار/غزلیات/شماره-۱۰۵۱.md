---
title: >-
    شمارهٔ ۱۰۵۱
---
# شمارهٔ ۱۰۵۱

<div class="b" id="bn1"><div class="m1"><p>بی رخت صبر از این بیش ندارم چه کنم</p></div>
<div class="m2"><p>تا به کی عمر در این غصّه گذارم چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین خسته ز ایام فراقت که منم</p></div>
<div class="m2"><p>خون دل در غمت از دیده نبارم چه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو را هست به جای من دلخسته کسی</p></div>
<div class="m2"><p>من بجز لطف تو ای دوست ندارم چه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ز هجران تو ای دوست ز پای افتادم</p></div>
<div class="m2"><p>دست از خون دل خود ننگارم چه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز گل وصل تو بویی به مشامم نرسید</p></div>
<div class="m2"><p>دایم از هجر تو مجروح ز خارم چه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب و روز و گه و بی گه ز غمش گریانم</p></div>
<div class="m2"><p>فارغ از حال من خسته نگارم چه کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که بی جرم بشد خاطرت آزرده ز من</p></div>
<div class="m2"><p>زاریم شد ز حد و سخت نزارم چه کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جهان چون نبود دولت وصلت یک دم</p></div>
<div class="m2"><p>به غمت روز و شبی گر نگذارم چه کنم</p></div></div>