---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>جز بوی سر زلف تو با باد صبا نیست</p></div>
<div class="m2"><p>...........................................ا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون شد دل مسکین من اندر غم رویت</p></div>
<div class="m2"><p>اندر دل خوبان جفاپیشه وفا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست ربایند دل خلق جهانی</p></div>
<div class="m2"><p>وانگاه به سر بار بجز جور و جفا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز مهر و وفا در دل ما نیست همانا</p></div>
<div class="m2"><p>جز جور و ستم قاعده شهر شما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیداد مکن بر دل عشاق از این بیش</p></div>
<div class="m2"><p>زیرا که ز حد بردن بیداد روا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالات بلاییست نه بالاست خدا را</p></div>
<div class="m2"><p>کس نیست به عهدت که گرفتار بلا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خاطرت ای دوست فراموش نشاید</p></div>
<div class="m2"><p>آن بنده که از یاد تو یک لحظه جدا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل برد ز دستم صنم و قصد جفا کرد</p></div>
<div class="m2"><p>رحمش به من خسته و شرمش ز خدا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون خاک رهت گشته ام ای سرو گل اندام</p></div>
<div class="m2"><p>میلت سوی ما از چه سبب چون و چرا نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند ترا برگ و هوای دگران است</p></div>
<div class="m2"><p>از خان وصال تو مرا برگ و نوا نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلطان جهانی و به خیل تو گداییم</p></div>
<div class="m2"><p>آخر ز چه رویت نظری سوی گدا نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون روی بپیچید ز من هاتف جان گفت</p></div>
<div class="m2"><p>احوال جهان پیش تو بی روی و ریا نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای سرو روان راست بگو تا ز چه معنی</p></div>
<div class="m2"><p>ما را به وصال تو نیازست و ترا نیست</p></div></div>