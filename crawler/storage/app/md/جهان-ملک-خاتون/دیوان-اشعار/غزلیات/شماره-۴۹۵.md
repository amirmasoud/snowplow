---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>باد بویی ز سر زلف پریشان آورد</p></div>
<div class="m2"><p>باد جانش به فدا کز بر جانان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش عشق تو می سوخت درون دل ما</p></div>
<div class="m2"><p>خاک کوی تو مگر باد به درمان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داده بودم سر و سامان ز غم عشق به باد</p></div>
<div class="m2"><p>سر سرگشته ما باز به سامان آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز وجودم رقمی بیش نبودی باقی</p></div>
<div class="m2"><p>نکهت زلف تو از نو به تنم جان آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم بختم که بدی تیره کنون روشن شد</p></div>
<div class="m2"><p>که بشیر آمد و بویی ز گریبان آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه آن روی چو خورشید تو را روزی دید</p></div>
<div class="m2"><p>چون شبی بی رخت ای ماه به پایان آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را خلوت وصل تو شبی دست نداد</p></div>
<div class="m2"><p>همچو مجنون ز غمت رو به بیابان آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ دانی شب هجران تو را نیست سحر</p></div>
<div class="m2"><p>که جهانی ز غم عشق به افغان آورد</p></div></div>