---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>مرا به درد فراق تو هیچ درمان نیست</p></div>
<div class="m2"><p>به غیر آتش عشق رخ تو در جان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو جانی و ز برم دور می شوی چه کنم</p></div>
<div class="m2"><p>ز جان مفارقت ای نور دیده آسان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا نیاز به روز وصال بسیارست</p></div>
<div class="m2"><p>شب فراق ترا گوییا که پایان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان صحن چمن صبحدم گذر کردم</p></div>
<div class="m2"><p>به قد و قامت تو هیچ سرو بستان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر به روی گلم اوفتاد تا دانی</p></div>
<div class="m2"><p>که چون رخ تو گلی در همه گلستان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا به دوست چرا حال ما نمی گویی</p></div>
<div class="m2"><p>مگر ترا ره رفتن به کوی جانان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو که بی تو به جان آمدم چرا آخر</p></div>
<div class="m2"><p>نصیب ما ز وصال تو غیر حرمان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعیدم از رخ چون ماهت ای پری پیکر</p></div>
<div class="m2"><p>کدام جان که به عید رخ تو قربان نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دوری از برم ای جان مکوش چندینی</p></div>
<div class="m2"><p>که در جهان بتر از درد روز هجران نیست</p></div></div>