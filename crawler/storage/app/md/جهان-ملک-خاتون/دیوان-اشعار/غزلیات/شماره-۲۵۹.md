---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>جهان را باز ایام جوانیست</p></div>
<div class="m2"><p>سر سودای عیش و کامرانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا بگذر شبی در کوی یارم</p></div>
<div class="m2"><p>بگویش با توأم رازی نهانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خضرم طالب سرچشمه ی نوش</p></div>
<div class="m2"><p>لب جان بخشت آب زندگانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا در روی تو حیران دو دیده</p></div>
<div class="m2"><p>مرا با قامتت پیوند جانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخش زیبنده تر از ماه و خورشید</p></div>
<div class="m2"><p>قدش مانند سرو بوستانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گویم نازش آن سرو آزاد</p></div>
<div class="m2"><p>که دایم شیوه ی او دلستانیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر داری نگارا در فراقت</p></div>
<div class="m2"><p>که کار دیده ی ما پاسبانیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جان آمد دل ما از غم تو</p></div>
<div class="m2"><p>اگرچه در غمم صد شادمانیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر کامم در این عالم ندادی</p></div>
<div class="m2"><p>مرا مقصود کام آن جهانیست</p></div></div>