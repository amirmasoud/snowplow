---
title: >-
    شمارهٔ ۱۲۷۹
---
# شمارهٔ ۱۲۷۹

<div class="b" id="bn1"><div class="m1"><p>دلا خود را به مه رویی چه بندی</p></div>
<div class="m2"><p>مرا حیران و سرگردان پسندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین زار و نزارم در فراقش</p></div>
<div class="m2"><p>بگو تا کی به عشقش کار بندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یقین تو بار نابی از در دوست</p></div>
<div class="m2"><p>که همچون آهوی سر در کمندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا از ما بگو با آن ستمگر</p></div>
<div class="m2"><p>نگویی در پی آزار چندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا مهر از من مسکین بریدی</p></div>
<div class="m2"><p>چرا شاخ امید از بیخ کندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو من چند گریم در فراقت</p></div>
<div class="m2"><p>به سان ابر تو چون گل بخندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانا تا به کی چون مرغ زیرک</p></div>
<div class="m2"><p>به دام زلف دلبر پای بندی</p></div></div>