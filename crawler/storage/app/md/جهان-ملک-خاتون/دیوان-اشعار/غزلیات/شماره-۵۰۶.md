---
title: >-
    شمارهٔ ۵۰۶
---
# شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>ای دیدهٔ نم دیده بی روی تو خون ریزد</p></div>
<div class="m2"><p>طوفان ز غم عشقت هر لحظه برانگیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر باد که برخیزد در صبحدم از کویش</p></div>
<div class="m2"><p>مهر رخ آن مه را با خاک من آمیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر حلقه که بگشایی از زلف پریشانت</p></div>
<div class="m2"><p>بینی تو بسی دلها کز حلقه درآویزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با لطف گل سوری در گلشن جان افروز</p></div>
<div class="m2"><p>چون روی تو را بیند در حال فرو ریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالع نکند یاری کاو در بر ما آید</p></div>
<div class="m2"><p>بیچاره دل حیران با بخت چه بستیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نقد دل خود را در خاک درت گم کرد</p></div>
<div class="m2"><p>خاک سر هر کویی از بهر چه می بیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کوی وفاداری شد جان جهان ساکن</p></div>
<div class="m2"><p>گر سر برود او را از کوی تو نگریزد</p></div></div>