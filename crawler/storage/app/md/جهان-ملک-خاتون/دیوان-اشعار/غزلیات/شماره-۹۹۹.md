---
title: >-
    شمارهٔ ۹۹۹
---
# شمارهٔ ۹۹۹

<div class="b" id="bn1"><div class="m1"><p>اگر با دولتت پاینده باشم</p></div>
<div class="m2"><p>به پیش بندگانت بنده باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنازم پیش جانت تا بمیرم</p></div>
<div class="m2"><p>بمیرم پیش تو تا زنده باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم از دولت وصلت به کامی</p></div>
<div class="m2"><p>اگر با طالع فرخنده باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سر خشنود باشم آن زمانی</p></div>
<div class="m2"><p>که پیش پای تو افکنده باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم در سایه ی مهرت بود جای</p></div>
<div class="m2"><p>چو خورشید جهان تابنده باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو سلطانی ز حکمت سر نپیچم</p></div>
<div class="m2"><p>درین ملک جهان تا بنده باشم</p></div></div>