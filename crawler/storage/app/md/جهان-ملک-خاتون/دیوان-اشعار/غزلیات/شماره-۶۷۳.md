---
title: >-
    شمارهٔ ۶۷۳
---
# شمارهٔ ۶۷۳

<div class="b" id="bn1"><div class="m1"><p>تا دلم را کرده ای مأوای خود</p></div>
<div class="m2"><p>من ندارم در غمت پروای خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منّتی باشد به عید روی تو</p></div>
<div class="m2"><p>گر کنی قربان مرا در پای خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما بین تا به خود عاشق شوی</p></div>
<div class="m2"><p>سرو جانم بر قد و بالای خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم و روی و زلف و خال و رنگ و بوی</p></div>
<div class="m2"><p>خوب داری هر یکی بر جای خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنبر و کافور و عود و مشک ناب</p></div>
<div class="m2"><p>کرده ای هرچار را لالای خود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی شیدا و حیران کرده ای</p></div>
<div class="m2"><p>دلبرا بر روی شهرآرای خود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو چون قدّ تو را در باغ دید</p></div>
<div class="m2"><p>شرمش آمد از قد زیبای خود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگذشتم دوش بی روی تو بود</p></div>
<div class="m2"><p>زآب چشم شوخ خون پالای خود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بجویی در همه ملک جهان</p></div>
<div class="m2"><p>کی توانی یافتن همتای خود</p></div></div>