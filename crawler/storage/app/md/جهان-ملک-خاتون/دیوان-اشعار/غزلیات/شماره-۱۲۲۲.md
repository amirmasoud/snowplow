---
title: >-
    شمارهٔ ۱۲۲۲
---
# شمارهٔ ۱۲۲۲

<div class="b" id="bn1"><div class="m1"><p>ای سودا دیده ام روی چو ماه</p></div>
<div class="m2"><p>دل گرفته در سر زلفت پناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رخ چون ماه و قد همچو سرو</p></div>
<div class="m2"><p>وز دو ابروی کج و چشم سیاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قد چون یاسمین و لعل لب</p></div>
<div class="m2"><p>یک جهت کردم بدان زلف دوتاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه مکر و فسون کرد او به چشم</p></div>
<div class="m2"><p>تا به افسونم ببرد آخر ز راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابروی فتّان و چشم مست تو</p></div>
<div class="m2"><p>خون ما آخر بریزد بیگناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو سیم اندام را در بوستان</p></div>
<div class="m2"><p>گو ز روی لطف کن در ما نگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شود چون کیمیا خاکی تنم</p></div>
<div class="m2"><p>زان نظر جانا ز کام نیک خواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر خاکم گذر کن تا ز مهر</p></div>
<div class="m2"><p>رسته بینی یک جهان مهر گیاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک نظر بر ما فکن از روی لطف</p></div>
<div class="m2"><p>چون تویی بر هر دو عالم پادشاه</p></div></div>