---
title: >-
    شمارهٔ ۹۵۱
---
# شمارهٔ ۹۵۱

<div class="b" id="bn1"><div class="m1"><p>از آن زمان که من آن روی چون قمر دیدم</p></div>
<div class="m2"><p>دل ضعیف خود از عشق بی خبر دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلاوتی که در آن چهره نگارین است</p></div>
<div class="m2"><p>قسم به جان تو در ماه و خور اگر دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خون دل رقمی از سرشک چون سیماب</p></div>
<div class="m2"><p>ز درد عشق تو جانا به روی زر دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرار نیست مرا چون دو زلف از رخ تو</p></div>
<div class="m2"><p>به جان دوست از آن دم که یک نظر دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی ندید مگر مرگ خویشتن به دو چشم</p></div>
<div class="m2"><p>بیا که روز فراقت به چشم و سر دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کوه و دشت همی گشتم از فراق رخت</p></div>
<div class="m2"><p>ز اشک دیده ی ما آب تا کمر دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر ز کوی تو ما را سفر نخواهد بود</p></div>
<div class="m2"><p>از آن بلا که ز بالات در سفر دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان و هرچه در او هست سر به سر دانی</p></div>
<div class="m2"><p>نبود همّتم ای دوست مختصر دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از این سبب که نظر کردم از سر تحقیق</p></div>
<div class="m2"><p>جهان و کار جهان جمله در گذر دیدم</p></div></div>