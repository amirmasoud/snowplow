---
title: >-
    شمارهٔ ۱۲۷۵
---
# شمارهٔ ۱۲۷۵

<div class="b" id="bn1"><div class="m1"><p>گر مرا با درد عشقت چاره بودی خوش بدی</p></div>
<div class="m2"><p>ور شب وصل بتان همواره بودی خوش بدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستگیرم گر بدی وصل رخت در پیش ما</p></div>
<div class="m2"><p>گر شب هجران تو آواره بودی خوش بدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو بستان را بود میلی خرامان سوی ما</p></div>
<div class="m2"><p>میل تو گر سوی این بیچاره بودی خوش بدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاد می دارد خیال وصل او ما را ولی</p></div>
<div class="m2"><p>بر غم دلدار اگر غمخواره بودی خوش بدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر شب وصلت بدیدی صبح بر بستی نقاب</p></div>
<div class="m2"><p>پرده هجران تو گر پاره بودی خوش بدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهره گفتن ندارم راز خود را در جهان</p></div>
<div class="m2"><p>ای دریغا گر مرا این چاره بودی خوش بدی</p></div></div>