---
title: >-
    شمارهٔ ۱۳۷۲
---
# شمارهٔ ۱۳۷۲

<div class="b" id="bn1"><div class="m1"><p>دلبرا با من جفا تا کی کنی</p></div>
<div class="m2"><p>بگذرد کار جهان تا هی کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بخوانی دفتر غمهای من</p></div>
<div class="m2"><p>نامه جور و جفا را طی کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل سرگشته بر باد هواست</p></div>
<div class="m2"><p>هرچه از راه وفا با وی کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود عیشم به خوبی چون بهار</p></div>
<div class="m2"><p>از بلاجویی بهارم دی کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چو نی می نالم و تو چشم و گوش</p></div>
<div class="m2"><p>سوی عود و چنگ و نای و نی کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون میسّر نیست امکان وصال</p></div>
<div class="m2"><p>آخر ای دل چندش اندر پی کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به کی از غمزه های نیم مست</p></div>
<div class="m2"><p>خون ما ریزی و میل می کنی</p></div></div>