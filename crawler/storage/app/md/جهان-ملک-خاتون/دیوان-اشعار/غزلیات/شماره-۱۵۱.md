---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>چو شوق روی تو بر بودم اختیار از دست</p></div>
<div class="m2"><p>برفت چون سر زلف توأم قرار از دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست بود مرا دامن نگار دریغ</p></div>
<div class="m2"><p>که برد چرخ جفا پیشه ام نگار از دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت رسد به گریبان دوستان دستی</p></div>
<div class="m2"><p>بگیر دامن یاران خود مدار از دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به دست تو افتد شبی سر زلفش</p></div>
<div class="m2"><p>مده دو زلف پریشانش زینهار از دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال قامت دلخواه ما چو سرو سهیست</p></div>
<div class="m2"><p>برفت از نظر ما و رفت یار از دست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جان رسید دل من ز دست جور فراق</p></div>
<div class="m2"><p>جفا و جور ز بهر خدا بدار از دست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو جان رسید به لب، دلبرم نظر فرمود</p></div>
<div class="m2"><p>چو حاصلم بود اکنون که رفت کار از دست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه روزگار وفا کرد با من و نه نگار</p></div>
<div class="m2"><p>ز دست رفت جهان را چو روزگار از دست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قرار و خواب و شکیبایی و جفا بردن</p></div>
<div class="m2"><p>برفت از غم عشق تو هر چهار از دست</p></div></div>