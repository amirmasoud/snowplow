---
title: >-
    شمارهٔ ۱۳۶۹
---
# شمارهٔ ۱۳۶۹

<div class="b" id="bn1"><div class="m1"><p>چو شود گر ز من ای جان دمکی یاد کنی</p></div>
<div class="m2"><p>خاطر خسته ما را شبکی شاد کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شدم از دل و جان بنده تو پس چه شود</p></div>
<div class="m2"><p>گر ز بند غم و هجر خودم آزاد کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک زمانم ز سر لطف خدا را بنواز</p></div>
<div class="m2"><p>تا به کی بر دل من این همه بیداد کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وعده وصل بدادی و به جایی نرسید</p></div>
<div class="m2"><p>تا چه باشد که چنین وعده به میعاد کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فراق رخ همچون گل جانان به چمن</p></div>
<div class="m2"><p>بلبلا تا به کی این ناله و فریاد کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدّعی چند میان من و جانان آخر</p></div>
<div class="m2"><p>از سر حقد و حسد این همه افساد کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل خسته به قلماش رقیبان نتوان</p></div>
<div class="m2"><p>که تو ترک رخ آن حور پری زاد کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با وجود قد و بالای جهان آرایش</p></div>
<div class="m2"><p>نیست واجب که نظر بر قد شمشاد کنی</p></div></div>