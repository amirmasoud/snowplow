---
title: >-
    شمارهٔ ۱۲۲۵
---
# شمارهٔ ۱۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ای غمت در جان ما جا ساخته</p></div>
<div class="m2"><p>خانه ی دل را به تو پرداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بت مه روی من عشقت مرا</p></div>
<div class="m2"><p>در زبان خاص و عام انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چمن سرو روان را دیده ام</p></div>
<div class="m2"><p>قدّ تو بر سرو سرافراخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شبی چو شمع در طشت فراق</p></div>
<div class="m2"><p>در غمت بنشسته و سر باخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالهت ما را به درد هجر کشت</p></div>
<div class="m2"><p>یک شبم از وصل خود ننواخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قلب جان این دل سودازده</p></div>
<div class="m2"><p>زآتش هجران تو بگداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کوی فراقت ای صنم</p></div>
<div class="m2"><p>چند سرگردان شوم چون فاخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان ما در ششدر عشقت بماند</p></div>
<div class="m2"><p>با تو تا نرد طرب را باخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دل بیچاره پر درد من</p></div>
<div class="m2"><p>در جهان غیر از تو کس نشناخته</p></div></div>