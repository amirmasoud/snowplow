---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>دلم دیده به رویی خوب بستست</p></div>
<div class="m2"><p>ز جستجوی هرکس باز رستست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زلف سرکشت آشفته حالیست</p></div>
<div class="m2"><p>چو لعل می پرستت می پرستست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر زلف سیاهت را وطن ساخت</p></div>
<div class="m2"><p>جزاک الله به نامی نیک جستست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو زلف تو پریشان حال چون من</p></div>
<div class="m2"><p>مدامت نرگس مخمور مستست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخت بنمای تا روشن شود چشم</p></div>
<div class="m2"><p>که دیدار رخت بر ما خجستست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سان بندگان بس سرو آزاد</p></div>
<div class="m2"><p>به خاک راه از آن بالا نشستست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه جامه ی مهرت نگارا</p></div>
<div class="m2"><p>به بالای دل ما نیک چستست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل سنگین من در آرزویت</p></div>
<div class="m2"><p>پریشان حال و سرگردان و خستست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به باغ جان بسی سرو سمن بوی</p></div>
<div class="m2"><p>که پیش قامت رعناش پستست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل رویش به دست دیگران است</p></div>
<div class="m2"><p>دل و جان جهان از خار خستست</p></div></div>