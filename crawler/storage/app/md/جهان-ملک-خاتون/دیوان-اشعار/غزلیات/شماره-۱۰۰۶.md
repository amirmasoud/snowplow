---
title: >-
    شمارهٔ ۱۰۰۶
---
# شمارهٔ ۱۰۰۶

<div class="b" id="bn1"><div class="m1"><p>ز زلفت بو نمی یابد دماغم</p></div>
<div class="m2"><p>به خون دیده می سوزد چراغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا در دل بُد این پیکان هجران</p></div>
<div class="m2"><p>نهادی از جفا بر سینه داغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شادی از وصالش نیست ما را</p></div>
<div class="m2"><p>چه تدبیرم بباید ساخت با غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو امکان نیست یک گل چیدن از وصل</p></div>
<div class="m2"><p>چه حاصل ای جهان از باغ و راغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان بی روی گلرنگش نخواهم</p></div>
<div class="m2"><p>که از جنّت بود با او فراغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا آورد از زلفش نسیمی</p></div>
<div class="m2"><p>ز بوی آن معطّر شد دماغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو گل از باغ بیرون رفت اکنون</p></div>
<div class="m2"><p>حوالت کرد هجرانش به داغم</p></div></div>