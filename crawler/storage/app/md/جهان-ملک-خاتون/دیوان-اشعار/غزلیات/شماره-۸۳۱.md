---
title: >-
    شمارهٔ ۸۳۱
---
# شمارهٔ ۸۳۱

<div class="b" id="bn1"><div class="m1"><p>ای دل ما را به دردت حال خوش</p></div>
<div class="m2"><p>کرده ای کار مرا پامال خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال من زارست در هجران تو</p></div>
<div class="m2"><p>کی تو پرسیدی مرا احوال خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وعده ی وصلم دهی امّا چه سود</p></div>
<div class="m2"><p>می کنی در وعده ام اهمال خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون الف بودم قدی در وصل تو</p></div>
<div class="m2"><p>کرده ای پشت دلم چون دال خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعره های شوق بر گل می زنم</p></div>
<div class="m2"><p>بلبل بستان نباشد لال خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقلان در بند نام و ننگ خویش</p></div>
<div class="m2"><p>فارغ از هر دو جهان ابدال خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه احوال جهان آشفته گشت</p></div>
<div class="m2"><p>بو که از لطفم بود آمال خوش</p></div></div>