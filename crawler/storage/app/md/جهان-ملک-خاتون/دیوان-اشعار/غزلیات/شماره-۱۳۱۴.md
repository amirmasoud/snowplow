---
title: >-
    شمارهٔ ۱۳۱۴
---
# شمارهٔ ۱۳۱۴

<div class="b" id="bn1"><div class="m1"><p>سهی سروا چرا با ما نسازی</p></div>
<div class="m2"><p>به خون ما بگو تا چند بازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاز عاشقان بشنو خدا را</p></div>
<div class="m2"><p>که بازی نیست کار عشق بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنازم پیش قدّت ای دلارام</p></div>
<div class="m2"><p>اگرچه از چو من صد بی نیازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو را زیبد به باغ ای سرو آزاد</p></div>
<div class="m2"><p>اگر دعوی کنی در سرفرازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو اندر بوته هجران به زاری</p></div>
<div class="m2"><p>بگو قلب مرا تا کی گدازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای کوی عشقت بس بلندست</p></div>
<div class="m2"><p>ز گنجشکی نیاید شاهبازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم بیچاره در هجران چه باشد</p></div>
<div class="m2"><p>به وصل ار چاره کارم بسازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبیب ما ز لطف بی دریغت</p></div>
<div class="m2"><p>چرا درد مرا درمان نسازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به محراب دو ابرویت که داریم</p></div>
<div class="m2"><p>ز دیده جامهٔ جان را نمازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حقیقت شد جهان را درد عشقت</p></div>
<div class="m2"><p>نه چون بلبل به گل عشق مجازی</p></div></div>