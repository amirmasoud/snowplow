---
title: >-
    شمارهٔ ۴۱۳
---
# شمارهٔ ۴۱۳

<div class="b" id="bn1"><div class="m1"><p>مرا با قامت آن سرو آزاد</p></div>
<div class="m2"><p>مسلمانان ز جانم بس خوش افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال قامتش سرو بلندست</p></div>
<div class="m2"><p>که بیخ صبر برکندم ز بنیاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالش از دو چشمم کی شود دور</p></div>
<div class="m2"><p>که در سالی نیارد یک دمش یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز غم گشتم مسلمانان چو مویی</p></div>
<div class="m2"><p>نگویی از وصالت کی شدم شاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارم خواب و خور در درد هجران</p></div>
<div class="m2"><p>شبی از وصل خویشم رس به فریاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنم خاکی و باد عشق در سر</p></div>
<div class="m2"><p>چه دارد آشنایی خاک با باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم برد و تنم چون خاک ره کرد</p></div>
<div class="m2"><p>هزارش آفرین بر جان و تن باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآمد آتش عشقت تنم سوخت</p></div>
<div class="m2"><p>از آن رو خاک ما بر باد برداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بگشود از سر زلفش گره باز</p></div>
<div class="m2"><p>جهان از بوی زلفش گشت آزاد</p></div></div>