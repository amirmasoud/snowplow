---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>دلم ز درد غم عشق جان نخواهد برد</p></div>
<div class="m2"><p>گمان هستی این ناتوان نخواهد برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ز جور تو جانم به لب رسد جانا</p></div>
<div class="m2"><p>بجز در تو به جایی فغان نخواهد برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا که وقت گلست و به بوستان برمت</p></div>
<div class="m2"><p>که کس گلی به در بوستان نخواهد برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم به قامت شمشاد تو چنان مایل</p></div>
<div class="m2"><p>که نام قامت سرو روان نخواهد برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تویی گلی به گلستان و بلبل سرمست</p></div>
<div class="m2"><p>بجز ثنای رخت بر زبان نخواهد برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و کلبه احزان ما مشرّف کن</p></div>
<div class="m2"><p>دمی به لطف که کس این گمان نخواهد برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو چشم مست توأم ناتوان و هیچکسی</p></div>
<div class="m2"><p>به پیشت اسم من ناتوان نخواهد برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش دشمن بدخو ز لطف دلبر ما</p></div>
<div class="m2"><p>یقین که آب رخ دوستان نخواهد برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بده زکات جوانی و حسن و زیبایی</p></div>
<div class="m2"><p>که جز ثواب کسی از جهان نخواهد برد</p></div></div>