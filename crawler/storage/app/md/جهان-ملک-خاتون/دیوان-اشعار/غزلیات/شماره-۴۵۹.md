---
title: >-
    شمارهٔ ۴۵۹
---
# شمارهٔ ۴۵۹

<div class="b" id="bn1"><div class="m1"><p>در دلم بود که دلدار وفا خواهد کرد</p></div>
<div class="m2"><p>کامم از لعل لب خویش روا خواهد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دلم درد بسی بود ز ایام فراق</p></div>
<div class="m2"><p>درد ما را مگر از لطف دوا خواهد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی گمان برد دل خسته سرگردانم</p></div>
<div class="m2"><p>که بت سرکش ما باز جفا خواهد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون کسی نیست که پیش تو بگوید حالم</p></div>
<div class="m2"><p>محرم راز دلم باد صبا خواهد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز هجران تو دانی که من خسته جگر</p></div>
<div class="m2"><p>جان شیرین ز تنم باز جدا خواهد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ جان من دلخسته به تو مشتاقست</p></div>
<div class="m2"><p>ز هوس بر سر کوی تو هوا خواهد کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاه غم عشقش چو چنین مغرورست</p></div>
<div class="m2"><p>نظری کی ز وفا سوی گدا خواهد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جهان گر صد از این جور نمایی چه کند</p></div>
<div class="m2"><p>به سر و جان تو او غیر دعا خواهد کرد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قامتت را چو ببیند به چمن سرو روان</p></div>
<div class="m2"><p>به هوای قد تو نشو و نما خواهد کرد</p></div></div>