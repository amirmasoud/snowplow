---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>بتی که خاطر او لازم جفا باشد</p></div>
<div class="m2"><p>چه لازمست که با او مرا وفا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا تو جرم کنی و خطا نهی بر ما</p></div>
<div class="m2"><p>مکن خطا که بد از نیکوان خطا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو پادشاه جهانی و من گدای درت</p></div>
<div class="m2"><p>صلاح کار گدایان ز پادشا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرآنکه همچو من از عافیت نپرهیزد</p></div>
<div class="m2"><p>چه جای اینکه از اینش بتر جزا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که نشنود از دوستان مخلص پند</p></div>
<div class="m2"><p>بخرّمی دل دشمنان سزا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا مگوی نگارا که عهد بشکستی</p></div>
<div class="m2"><p>طریق عهد شکستن نه زان ما باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من آن نیم که به جور از تو روی برتابم</p></div>
<div class="m2"><p>که نقض عهد هم از عادت شما باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه دشمنی که نکردی به دوستی با من</p></div>
<div class="m2"><p>ز دوستان صفت دشمنان روا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر تو طعنه زنی بر جهان که بد مهرست</p></div>
<div class="m2"><p>امید مهر و وفا در جهان که را باشد</p></div></div>