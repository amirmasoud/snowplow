---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>مرا جز درگه لطف تو می دانی پناهی نیست</p></div>
<div class="m2"><p>اگرچه بر من مسکین محزونت نگاهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیده خون همی بارم ز شوق روی آن دلبر</p></div>
<div class="m2"><p>به غیر از مردم دیده در این عالم گواهی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو حلقه بر درم دایم ز هرکس سرزنش دیده</p></div>
<div class="m2"><p>چه چاره چون مرا در خلوت وصل تو راهی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک کویت ای دلبر ز جانم معتکف دایم</p></div>
<div class="m2"><p>چرا آخر تو را روزی به سوی ما نگاهی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم در عشق بیچاره نمی سازی مرا چاره</p></div>
<div class="m2"><p>به درد عشقت ای دلبر بجز سوزی و آهی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدیدم مشک تتّاری بسی با عنبر سارا</p></div>
<div class="m2"><p>به غیر از زلف شبرنگش چو آن مشک سیاهی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو حال من نمی دانی و دایم در جهان باری</p></div>
<div class="m2"><p>گدایی چون من مسکین و مانند تو شاهی نیست</p></div></div>