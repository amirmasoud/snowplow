---
title: >-
    شمارهٔ ۹۷۴
---
# شمارهٔ ۹۷۴

<div class="b" id="bn1"><div class="m1"><p>به عالم غیر تو دلبر ندارم</p></div>
<div class="m2"><p>بجز لطفت کسی دیگر ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم چرخ فلک هم دست باشد</p></div>
<div class="m2"><p>من از خاک درت سر بر ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز بوی سر زلفت نگارا</p></div>
<div class="m2"><p>در این شبهای غم رهبر ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر عالم همه خورشید رویند</p></div>
<div class="m2"><p>بجز مهر رخت در خور ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا تا بوی زلفت در دماغست</p></div>
<div class="m2"><p>چنان میلی سوی عنبر ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم رانی ورم بنوازی ای دوست</p></div>
<div class="m2"><p>من از مهر رخت دل بر ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به درگاه تو غیر جان سپاری</p></div>
<div class="m2"><p>به جان تو که من در سر ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبی گر بازم آیی از در ای جان</p></div>
<div class="m2"><p>ز بخت خویشتن باور ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجز ورد دعایش من شب و روز</p></div>
<div class="m2"><p>جهانا آیتی از بر ندارم</p></div></div>