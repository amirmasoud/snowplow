---
title: >-
    شمارهٔ ۹۵۶
---
# شمارهٔ ۹۵۶

<div class="b" id="bn1"><div class="m1"><p>نگارا از فراقت سخت زارم</p></div>
<div class="m2"><p>به سختی روزگاری می گذارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خور دارم نه خواب از عشق رویت</p></div>
<div class="m2"><p>چو زلف تو پریشان روزگارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو ابروی توأم پیوسته در خم</p></div>
<div class="m2"><p>چو چشمان تو در عین خمارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاک کوی هجران آخر ای جان</p></div>
<div class="m2"><p>نشانی تا به کی در دل غبارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بوی آنکه بر من رحمت آری</p></div>
<div class="m2"><p>نشسته منتظر بر ره گذارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکردی هیچ رحمت بر دل من</p></div>
<div class="m2"><p>از آن بی رحمتی بس شرمسارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببردی آب روی من به یکبار</p></div>
<div class="m2"><p>چرا کردی چو خاک راه خوارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن زین بیش بر من جور و خواری</p></div>
<div class="m2"><p>مبر یکباره جانا اعتبارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میان وادی جان رمیده</p></div>
<div class="m2"><p>بجز تخم غم مهرت چه کارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان را اختیاری نیست یارا</p></div>
<div class="m2"><p>به دست باد دادم اختیارم</p></div></div>