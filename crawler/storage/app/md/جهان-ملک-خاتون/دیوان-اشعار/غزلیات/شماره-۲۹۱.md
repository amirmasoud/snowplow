---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>جانا دلم ز روی تو یک دم صبور نیست</p></div>
<div class="m2"><p>بی روی جان فزای تو ما را حضور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرو برمگیر ز ما سایه ی قدت</p></div>
<div class="m2"><p>زیرا که آفتاب تو از سایه دور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شمع جمع ما که جهان از تو روشن است</p></div>
<div class="m2"><p>بازآ که بی جمال تو در دیده نور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویند در بهشت برین حور زا بسیست</p></div>
<div class="m2"><p>دیدم بهشت را و یکی چون تو حور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن فرّ و زیب و حسن و ملاحت که در وی است</p></div>
<div class="m2"><p>در حور عین نباشد و اندر قصور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازآ که نور دیده ی مایی و در غمت</p></div>
<div class="m2"><p>در دیده ای و در دل تنگم سرور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبرم ز روی خویش مفرما که بیش از این</p></div>
<div class="m2"><p>جانا دلم به درد فراقت صبور نیست</p></div></div>