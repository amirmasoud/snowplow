---
title: >-
    شمارهٔ ۱۳۲۰
---
# شمارهٔ ۱۳۲۰

<div class="b" id="bn1"><div class="m1"><p>چرا تو درد دلم را دوا نمی سازی</p></div>
<div class="m2"><p>نظر به عین عنایت بما نیندازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا به زلف نگارم اگر رسی زنهار</p></div>
<div class="m2"><p>مباز با سر زلفش مکن تو جانبازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوش یار رسان نرمکی ز من پیغام</p></div>
<div class="m2"><p>که عاشقان جهانرا تو محرم رازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو چرا چه سبب از چه رو بتا با من</p></div>
<div class="m2"><p>به هیچ گونه نداری طریق دمسازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گفته ایم ز دستت بجز شکایت هجر</p></div>
<div class="m2"><p>چه کرده ایم به پایت بجز سر اندازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو چاره ساز جهانی تویی به لطف امروز</p></div>
<div class="m2"><p>چرا به چاره بیچارگان نپردازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرچه خوار شدم بر درت چو خاک هنوز</p></div>
<div class="m2"><p>میان خلق جهانم بود سرافرازی</p></div></div>