---
title: >-
    شمارهٔ ۴۷۲
---
# شمارهٔ ۴۷۲

<div class="b" id="bn1"><div class="m1"><p>دل برد زلف شوخت و آنگاه قصد جان کرد</p></div>
<div class="m2"><p>انصاف ده نگارا با دوست این توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نور هر دو دیده این مردم دو دیده</p></div>
<div class="m2"><p>خون دل از دو دیده در حسرتت روان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مدح تو چو سوسن کردم زبان درازی</p></div>
<div class="m2"><p>گر می کشی تو دانی مدح رخت ز جان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی چه کرد با من از روی بی وفایی</p></div>
<div class="m2"><p>دل برد آن ستمگر رویش ز ما نهان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زلف چون بنفشه و از خال عنبرینش</p></div>
<div class="m2"><p>ما را چو نرگس خود بیمار و ناتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا قد همچو سروش در پیش ما روان شد</p></div>
<div class="m2"><p>جانم روان روان را در پیش او روان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گفت با دو دیده افتاد کار ما را</p></div>
<div class="m2"><p>فی الحال مردمک را در جست و جو دوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه جهان ندارد با دلبران وفایی</p></div>
<div class="m2"><p>دیدی که کس جفایی زین گونه با جهان کرد</p></div></div>