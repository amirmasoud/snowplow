---
title: >-
    شمارهٔ ۳۷۸
---
# شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>در عشق تو تا چند کشم بار ملامت</p></div>
<div class="m2"><p>اندیشه نداری مگر از روز قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کوی ملامت دل من رخت به در برد</p></div>
<div class="m2"><p>تا کرد وطن در سر کویت به سلامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بحر غم عشق تو بیچاره دلم را</p></div>
<div class="m2"><p>نه راه گریزست و نه یارای اقامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون پند رفیقان موافق نشنیدی</p></div>
<div class="m2"><p>ای دل ندهد فایده امروز ندامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنواز به تشریف وصالت دل ما را</p></div>
<div class="m2"><p>گر بنده نوازی ز سر لطف و کرامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ملک دلم شد ز قدوم تو مشرّف</p></div>
<div class="m2"><p>جان خود به چه ارزد که فرستم به اقامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمان تو در گوشه ی محراب دو ابرو</p></div>
<div class="m2"><p>مستند و نشاید که کند مست امامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر جان جهان در عوض وصل بخواهی</p></div>
<div class="m2"><p>ترکت نتوان کرد و کنم ترک تمامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مردمک دیده ز شوخی ننشستی</p></div>
<div class="m2"><p>تا شد دل تنگم هدف تیر ملامت</p></div></div>