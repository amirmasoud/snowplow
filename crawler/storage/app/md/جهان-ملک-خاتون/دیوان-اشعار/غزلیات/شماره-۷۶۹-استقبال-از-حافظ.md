---
title: >-
    شمارهٔ ۷۶۹ - استقبال از حافظ
---
# شمارهٔ ۷۶۹ - استقبال از حافظ

<div class="b" id="bn1"><div class="m1"><p>ای دل ار سرگشته‌ای از جور دوران غم مخور</p></div>
<div class="m2"><p>باشد احوال جهان افتان و خیزان غم مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تندباد چرخ چون در آتش عشقت فکند</p></div>
<div class="m2"><p>آبرویت گر شود با خاک یکسان غم مخور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه چون یعقوب گشتی ساکن بیت الحزن</p></div>
<div class="m2"><p>یوسف گمگشته باز آید به کنعان غم مخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در طلب باش و مباش از لطف یزدان ناامید</p></div>
<div class="m2"><p>هم به امّیدی رسند امیدواران غم مخور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبه مقصود خواهی رو متاب از بادیه</p></div>
<div class="m2"><p>دل بنه بر درد از خار مغیلان غم مخور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد او بهتر ز درمانست بنشین صبر کن</p></div>
<div class="m2"><p>درد دل را گر نیابی هیچ درمان غم مخور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اعتمادی نیست بر کار جهان خرسند باش</p></div>
<div class="m2"><p>آب باز آید به جوی رفته ای جان غم مخور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باغبانا صبر کن با زحمت زاغان بساز</p></div>
<div class="m2"><p>بلبل شوریده باز آید به بستان غم مخور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای جهان تا کی دل از کار جهان داری ملول</p></div>
<div class="m2"><p>روزگارت عاقبت گردد به سامان غم مخور</p></div></div>