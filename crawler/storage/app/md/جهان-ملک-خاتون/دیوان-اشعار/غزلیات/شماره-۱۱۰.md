---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>مانی قدرت او در دو جهان نقّاش است</p></div>
<div class="m2"><p>او به صورت گری نقش جهانی فاش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوطی طبع من خسته ز دریای وجود</p></div>
<div class="m2"><p>به ثنای در توحید تو بس در پاش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متنفّس نشود از در لطفش نومید</p></div>
<div class="m2"><p>کمترین جانوری بر در او خفّاش است</p></div></div>