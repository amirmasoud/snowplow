---
title: >-
    شمارهٔ ۱۰۲۵
---
# شمارهٔ ۱۰۲۵

<div class="b" id="bn1"><div class="m1"><p>بلبل صفت از عشق تو فریاد زنانم</p></div>
<div class="m2"><p>ای دوست بجز مدح و ثنای تو ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دم نزنم بی تو و یاد تو از آن روی</p></div>
<div class="m2"><p>در همت عالی همه نیکست زبانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سوسن آزاد که او جمله زبانست</p></div>
<div class="m2"><p>بر یاد تو سرتاسر دل جمله زبانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساکن شده ام بر سر کویش به امیدی</p></div>
<div class="m2"><p>باشد که سرآید ز جهان سرو روانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریادرس ای دوست که فریادرسم نیست</p></div>
<div class="m2"><p>کز چرخ گذشتست ز جور تو فغانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وصف دهان و لب و دندان که تو داری</p></div>
<div class="m2"><p>رای تو اگر راه دهد دُر بچکانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زنهار عنایت ز من خسته مگردان</p></div>
<div class="m2"><p>چون بر کرم تست امید دو جهانم</p></div></div>