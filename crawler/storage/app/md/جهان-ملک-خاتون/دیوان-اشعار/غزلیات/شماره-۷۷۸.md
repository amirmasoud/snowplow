---
title: >-
    شمارهٔ ۷۷۸
---
# شمارهٔ ۷۷۸

<div class="b" id="bn1"><div class="m1"><p>آخر نظری کن به من ای سرو روان باز</p></div>
<div class="m2"><p>هر چند که آید همه از سرو روان ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرگشته چو ماییم خروشان ز فراقت</p></div>
<div class="m2"><p>در گوش تو خواهیم که گوییم همه راز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدّ تو بلندست و مرا دست رسی نیست</p></div>
<div class="m2"><p>گویی تو که با ناله و با گریه همی ساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید جهانتابی و من ذرّه مهرت</p></div>
<div class="m2"><p>از بهر خدا سایه ی لطفی به من انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی که ز هجران دل ما در چه ملالست</p></div>
<div class="m2"><p>چون شمع که باشد سر او در دهن گاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکین دل من همچو کبوتر بچه وحشیست</p></div>
<div class="m2"><p>چون پنجه تواند که کند با چو تو شهباز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند که صبرست جهان چاره ی کارت</p></div>
<div class="m2"><p>از روی ضرورت شده با هجر تو دمساز</p></div></div>