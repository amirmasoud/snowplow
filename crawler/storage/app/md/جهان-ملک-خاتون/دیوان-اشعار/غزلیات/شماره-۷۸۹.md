---
title: >-
    شمارهٔ ۷۸۹
---
# شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>فغان و داد ازین روزگار سفله نواز</p></div>
<div class="m2"><p>که دارد اهل مروّت بدین صفت به نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آهن و مس و رویست و قلع عالم پر</p></div>
<div class="m2"><p>طلا و نقره ازین جور می رود بگداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چراغ بزر ز روغن همیشه می سوزد</p></div>
<div class="m2"><p>جفانگر که سر شمع می برند به گاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شهر کبک و کبوتر به دانه می دارند</p></div>
<div class="m2"><p>به دشت و کوه و بیابان به طعمه گردد باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جور چرخ جفاجوی دونِ دون پرور</p></div>
<div class="m2"><p>نکرد مرغ دل من درین هوا پرواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکایت ستم چرخ با که بتوان گفت</p></div>
<div class="m2"><p>به غیر باد صبا کاو مراست محرم راز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر به گوش فلک از جهان دهد پیغام</p></div>
<div class="m2"><p>که بیش ازین سر ناجنس را به خود مفراز</p></div></div>