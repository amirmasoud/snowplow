---
title: >-
    شمارهٔ ۷۲۰
---
# شمارهٔ ۷۲۰

<div class="b" id="bn1"><div class="m1"><p>آنچه دل حزین من از جور او کشید</p></div>
<div class="m2"><p>نه دیده دیده باشد و نه گوش کس شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست جور بی حد و اندوه گریه گفت</p></div>
<div class="m2"><p>از حد بشد تحمّل و جانم به لب رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر حال زار من دل سنگین بسوزدت</p></div>
<div class="m2"><p>گر شرح آن دهم که دل خسته ام چه دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرح غمش چگونه دهم شمّه ای از آن</p></div>
<div class="m2"><p>کاو رفت و دیگری به من خسته دل گزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد دلم ببین تو که آن دلستان شوخ</p></div>
<div class="m2"><p>مهر از من شکسته به یکبارگی برید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ دل ضعیف من اندر هوای دوست</p></div>
<div class="m2"><p>از روی شوق از قفس سینه بر پرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیچاره دل برفت به بازار عشق دوست</p></div>
<div class="m2"><p>جان را به غم فروخت و غم عشق او خرید</p></div></div>