---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>گر مرا میل تو باشد بی تکلّف دور نیست</p></div>
<div class="m2"><p>زآنکه در فردوس اعلی مثل تو یک حور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس شهلا اگرچه مست و شوخ و سرکشست</p></div>
<div class="m2"><p>در سرابستان جان چون چشم تو مخمور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه عاشق هست بسیارت ولی چون من یکی</p></div>
<div class="m2"><p>خسته ی دل بسته ی سرگشته ی مهجور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طبیب من چو دردم را تو درمانی بگو</p></div>
<div class="m2"><p>کز چه رو آخر تو را پروای این رنجور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جدا گشتی ز چشمم ای دو چشم جان من</p></div>
<div class="m2"><p>بی رخ عاشق فریبت چشم دل را نور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به تیغم می زنی یا جان ستانی حاکمی</p></div>
<div class="m2"><p>کیست کاو در عشق تو عاشق تر از منصور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه گشتم در جهان بسیار خوبان دیده ام</p></div>
<div class="m2"><p>در جهان خوبرویی به ز تو منظور نیست</p></div></div>