---
title: >-
    شمارهٔ ۱۰۰۱
---
# شمارهٔ ۱۰۰۱

<div class="b" id="bn1"><div class="m1"><p>سرو قد تو رسته روان بر کنار چشم</p></div>
<div class="m2"><p>گه بر سرش نشانم و گه در کنار چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روی تو نظر نتوانیم بعد از این</p></div>
<div class="m2"><p>تا بر رخت ز ما ننشیند غبار چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو قدت به خون جگر پروریده ام</p></div>
<div class="m2"><p>زان رو کش آب داده ام از جویبار چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزار مردم این همه خوش نیست دلبرا</p></div>
<div class="m2"><p>نازک بود دو دیده ما کار و بار چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک جرعه می ز لعل لب خویش نوش کن</p></div>
<div class="m2"><p>تا بشکند به معجز لعلت خمار چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غمزه در فراق تو برهم زنم بتا</p></div>
<div class="m2"><p>خون می رود به دامنم از رهگذار چشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی روی تو جهان همه تیره ست پیش ما</p></div>
<div class="m2"><p>زیرا سیه شدست مرا روزگار چشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گلشن وصال تو خواهیم رنگ و بوی</p></div>
<div class="m2"><p>تا دشمن تو را شوم ای دوست خار چشم</p></div></div>