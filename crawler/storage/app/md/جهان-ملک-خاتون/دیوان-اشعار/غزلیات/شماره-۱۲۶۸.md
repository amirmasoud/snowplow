---
title: >-
    شمارهٔ ۱۲۶۸
---
# شمارهٔ ۱۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ای مسلمانان خدا را همّتی</p></div>
<div class="m2"><p>تا کند دلدار با من رحمتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وصالش هر زمان بنوازدم</p></div>
<div class="m2"><p>کز فراق افتاده ام در زحمتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غم وصل تو بودم عقل گفت</p></div>
<div class="m2"><p>کی گدایی یافت زین سان نعمتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده نگشایم ز غیرت بر کسی</p></div>
<div class="m2"><p>در فراقت تا نباشد تهمتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون خیالت بگذرد بر چشم من</p></div>
<div class="m2"><p>می نهد بر دیده ی جان منّتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دهد دست آنکه بوسم پای تو</p></div>
<div class="m2"><p>در جهان زین به نباشد دولتی</p></div></div>