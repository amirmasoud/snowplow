---
title: >-
    شمارهٔ ۶۶۲
---
# شمارهٔ ۶۶۲

<div class="b" id="bn1"><div class="m1"><p>گر کسی را درد بی درمان بود</p></div>
<div class="m2"><p>از لبت درمان او آسان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره ای زان چشمه ام بر لب چکان</p></div>
<div class="m2"><p>اعتمادی نیست تا بر جان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده جان بربدوزم از رخت</p></div>
<div class="m2"><p>گر جهانی سر به سر پیکان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو همه جانی که دل بردی ز ما</p></div>
<div class="m2"><p>با تو ما را چون سخن در جان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دمک ای دیده خون دل مریز</p></div>
<div class="m2"><p>مردمی کن مردمم مهمان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زنان زنهار بدعهدی مکن</p></div>
<div class="m2"><p>عشق بازی عادت مردان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شراب عشق خویشم مست کن</p></div>
<div class="m2"><p>زآنکه مستی عادت رندان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این دل بیچاره ام در هجر تو</p></div>
<div class="m2"><p>تا به کی در عشق سرگردان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست می پرسی چو سرو قامتت</p></div>
<div class="m2"><p>بی رخت بر ما جهان زندان بود</p></div></div>