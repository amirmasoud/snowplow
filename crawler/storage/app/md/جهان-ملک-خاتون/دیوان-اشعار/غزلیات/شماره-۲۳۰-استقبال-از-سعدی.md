---
title: >-
    شمارهٔ ۲۳۰ - استقبال از سعدی
---
# شمارهٔ ۲۳۰ - استقبال از سعدی

<div class="b" id="bn1"><div class="m1"><p>فراغتیست مرا از جهان و هرچه در اوست</p></div>
<div class="m2"><p>چه باک دارم از اندیشه‌های دشمن و دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا اگر چو سخن خلق در دهان گیرند</p></div>
<div class="m2"><p>غریب نیست صدف دایماً پر از لولوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که از بد و نیک زمانه دست بشست</p></div>
<div class="m2"><p>معینست که فارغ ز مادح و بدگوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو پاک دامنی آفتاب مشهورست</p></div>
<div class="m2"><p>چه باک اگر شب تاریک در مقابل اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رسم تضمین این بیت دلکش آوردم</p></div>
<div class="m2"><p>ز شعر شیخ که جانم به طبع دارد دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«ز دست دشمنم ای دوستان شکایت نیست</p></div>
<div class="m2"><p>شکایتم همه از دوستان دشمن خوست»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسیم گلشن طبعم حسود تر دامن</p></div>
<div class="m2"><p>شنیده است که چون باد خلد عنبربوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سان غنچه که بر وی وزد صبا هر دم</p></div>
<div class="m2"><p>از آن معاینه بر خویشتن بدرّد پوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان خوشست چو سرو از چمن مرو بیرون</p></div>
<div class="m2"><p>که جای مردم روشن روان کنون لب جوست</p></div></div>