---
title: >-
    شمارهٔ ۸۴۲
---
# شمارهٔ ۸۴۲

<div class="b" id="bn1"><div class="m1"><p>تو جوری می کنی بر من ز حد بیش</p></div>
<div class="m2"><p>مکن زین بیش و از آهم بیندیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ریشست دل از جور ایام</p></div>
<div class="m2"><p>تو هم بر ریش من جانا مزن نیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمک از لب مزن بر ریش جانم</p></div>
<div class="m2"><p>نمک مشکل توان زد بر سر ریش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا ای عید روی ماهرویان</p></div>
<div class="m2"><p>که تا قربان شوم هر دم درین کیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جفا تا کی برم در درد عشقت</p></div>
<div class="m2"><p>من بیچاره از بیگانه و خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو سلطانی و من درویش مسکین</p></div>
<div class="m2"><p>ز بهر حق نظر می کن به درویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دل گفتم برو گر بینیش روی</p></div>
<div class="m2"><p>فدا کن در جهان جان و میندیش</p></div></div>