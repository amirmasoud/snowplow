---
title: >-
    شمارهٔ ۸۳۷
---
# شمارهٔ ۸۳۷

<div class="b" id="bn1"><div class="m1"><p>به خوابش دیده ام زلف تو را دوش</p></div>
<div class="m2"><p>شدم زان بو بدین سان مست و مدهوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باشم از در من گر درآیی</p></div>
<div class="m2"><p>که بر راه تو دارم چشم و هم گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم از یاد تو خالی زمانی</p></div>
<div class="m2"><p>چرا کردی به یکبارم فراموش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من امشب با فراقت چون بسازم</p></div>
<div class="m2"><p>چو یاد آرم زمانی از شب دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان زنهار چون اهل دلی تو</p></div>
<div class="m2"><p>ز دست جور هر نااهل مخروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگارم فارغست از ما نگویی</p></div>
<div class="m2"><p>تو چون دیگی به غم تا کی کنی جوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مپوشان رخ ز چشمم ای پری زاد</p></div>
<div class="m2"><p>نشاید کرد آتش زیر سرپوش</p></div></div>