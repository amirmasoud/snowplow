---
title: >-
    شمارهٔ ۱۱۸۷
---
# شمارهٔ ۱۱۸۷

<div class="b" id="bn1"><div class="m1"><p>بگشای چشم مرحمت و حال ما ببین</p></div>
<div class="m2"><p>بر جان من ز جور فراقت جفا ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالم عظیم ناخوش و دردم ز غم به دل</p></div>
<div class="m2"><p>هستی طبیب دل تو به دردم دوا ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی نداند آنکه کند عیب در غمم</p></div>
<div class="m2"><p>ای پادشاه صورت حال گدا ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردی ز حد جفا صنما هم به سوی ما</p></div>
<div class="m2"><p>از روی لطف خویش به چشم وفا ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذر چو سرو ناز و نظر کن ز روی لطف</p></div>
<div class="m2"><p>بر حال ما تعدّی هر ناسزا ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیگانه وار تا به کی آخر ستم کنی</p></div>
<div class="m2"><p>چشم وفا گشای و در این آشنا ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر در جهان به خاک منت اوفتد گذر</p></div>
<div class="m2"><p>از خاک ما دمیده تو مهر گیا ببین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردی که در هوا رود از خاک پای تو</p></div>
<div class="m2"><p>در چشم ما عوض توتیا ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خیر کوش و بیش میازار خلق را</p></div>
<div class="m2"><p>آری جهان سفله ندارد بقا ببین</p></div></div>