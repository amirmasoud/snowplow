---
title: >-
    شمارهٔ ۶۷۸
---
# شمارهٔ ۶۷۸

<div class="b" id="bn1"><div class="m1"><p>از حال من نگار مرا گر خبر شود</p></div>
<div class="m2"><p>چون زلف خاطرش همه در یکدگر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آه دل بجز لب خشکم چه حاصلست</p></div>
<div class="m2"><p>از خون دیده دامن من گرچه تر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز گمان مبر که خیال رخت دمی</p></div>
<div class="m2"><p>از دیده دور گردد و از دل بدر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیچاره دل گناه ندارد ولی چه سود</p></div>
<div class="m2"><p>دانم یقین که در سر کار نظر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دیده تا به چند بریزی تو خون من</p></div>
<div class="m2"><p>کز دیده حال زار دلم زارتر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیک نظر فرست مگر تا ببیندش</p></div>
<div class="m2"><p>ورنه غذای روح ز خون جگر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل گفت با دو دیده که مردم گواه من</p></div>
<div class="m2"><p>کان دیده را ببیند و حالش بتر شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دل صبور باش به هجران و دم مزن</p></div>
<div class="m2"><p>نومید هم مباش چه دانی مگر شود</p></div></div>