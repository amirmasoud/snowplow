---
title: >-
    شمارهٔ ۶۳۷
---
# شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>این دیدهٔ جان مرا روی تو بینا می‌کند</p></div>
<div class="m2"><p>مدحت زبان روح را گویی که گویا می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری‌ست تا جان می‌دهم بر وعدهٔ روز وصال</p></div>
<div class="m2"><p>تا کی بت سنگین‌دلم امروز و فردا می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب ز هجرت مردم چشم من سودازده</p></div>
<div class="m2"><p>هردم چو ملّاحان شنا در آب دریا می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوید به هجران صبر کن تا کام یابی از جهان</p></div>
<div class="m2"><p>سودای عشقش چون کنم در سینه غوغا می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از صبر کامم تلخ شد حاصل نشد کام دلم</p></div>
<div class="m2"><p>آخر بگو تا کی بتم این جور بر ما می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد دلی دارم ز تو گل قند فرمودم طبیب</p></div>
<div class="m2"><p>زان روی و لب کن چاره‌ای کان دفع صفرا می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چشم فتّانش بگو تا کی خورد خون دلم</p></div>
<div class="m2"><p>با ما چرا چون زلف خود هر لحظه سودا می‌کند</p></div></div>