---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>عیدیست مبارک که کس آن عید ندیدست</p></div>
<div class="m2"><p>وز باد صبا هیچ کس آن بو نشنیدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوش دل آمد سحر از هاتف غیبم</p></div>
<div class="m2"><p>کین عید به بخت شه شه زاده سعیدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایام خزانست ولیک از نظر لطف</p></div>
<div class="m2"><p>در باغ سعادات همه سبزه دمیدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از گلبن امید برآمد گل دولت</p></div>
<div class="m2"><p>ورنه به چنین فصل گل از باغ که چیدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جویان هلال شب عیدند خلایق</p></div>
<div class="m2"><p>خورشید درخشنده در این عید رسیدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عید صیامت طرب و خرمی اولیست</p></div>
<div class="m2"><p>زیرا که قدوم شه شه زاده دو عیدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آهوی تتاری که در او نافه چینست</p></div>
<div class="m2"><p>بویی مگر از گلشن لطف تو شنیدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا سرو روان، قدّ تو را دید به بستان</p></div>
<div class="m2"><p>از شرم چنان قامت رعنات خمیدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان روز که محروم ز الطاف عمیمم</p></div>
<div class="m2"><p>خونم ز دل و دیده ی غمدیده چکیدست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از جان چو دعاگوی و ثناخوان به جهانم</p></div>
<div class="m2"><p>از عین عنایت ز چه رو بنده بعیدست</p></div></div>