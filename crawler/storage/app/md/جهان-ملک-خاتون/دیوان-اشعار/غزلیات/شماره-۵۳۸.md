---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>تا مرا طاقت هجران و توانم باشد</p></div>
<div class="m2"><p>نکنم ترک غمت تا دل و جانم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شدی دور مرا از نظر ای نور دو چشم</p></div>
<div class="m2"><p>دایماً خون دل از دیده روانم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوبی و نارون از پای درآیند ز رشک</p></div>
<div class="m2"><p>در لب جوی که آن سرو روانم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفته بودی که شبی داد ز وصلت بدهم</p></div>
<div class="m2"><p>گر دهی نیز کجا طالع آنم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر نوازش که کنی بنده دلسوخته را</p></div>
<div class="m2"><p>بجز از دولت وصلت نه چنانم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مار شیدای فراقت به دلم نیشی زد</p></div>
<div class="m2"><p>غیر تریاک وصال تو زیانم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر شبی بنده نوازی ز سر لطف یقین</p></div>
<div class="m2"><p>چه سعادت به از این در دو جهانم باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با همه جور که از دست تو می یابد دل</p></div>
<div class="m2"><p>ذکر اوصاف رخت ورد زبانم باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا مراد من دلخسته ز وصلت ندهی</p></div>
<div class="m2"><p>همه شب بر سر کوی تو فغانم باشد</p></div></div>