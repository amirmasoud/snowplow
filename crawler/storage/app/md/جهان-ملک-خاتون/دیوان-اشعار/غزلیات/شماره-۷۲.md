---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>دل رفت از بر من و زلفش مقام ساخت</p></div>
<div class="m2"><p>چون موم از آتش رخ او بین که چون گداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرّاد ده هزار درین عرصه که منم</p></div>
<div class="m2"><p>دیدی که عشق روی تو با من چه مهره باخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسکین دل ضعیف من اندر پناه تست</p></div>
<div class="m2"><p>نشناخت هیچکس ز جهان چون تو را شناخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم سفر کنم ز دیارت ز دست عشق</p></div>
<div class="m2"><p>خیل خیال تو به سر من دو اسبه تاخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند لطف آن صنمم بی نهایتست</p></div>
<div class="m2"><p>هرگز شبی ز وصل من خسته را شناخت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم هوای عشق بلندست چون کنم</p></div>
<div class="m2"><p>مرغ دلم برفت و در آن زلف خانه ساخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا عشق تو شناخت دل من در این جهان</p></div>
<div class="m2"><p>گشتم به چشم خلق جهان باز ناشناخت</p></div></div>