---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>من دلداده ندارم به غم عشق دوا</p></div>
<div class="m2"><p>چاره ی درد من خسته بجو بهر خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من سودازده در عشق تو سرگردانم</p></div>
<div class="m2"><p>همچو زلف تو به گرد رخ تو، بی سر و پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآنکه آمد ز غم عشق تو جانم بر لب</p></div>
<div class="m2"><p>قصّه حال دل خویش بگفتم به صبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش از من دلخسته به دلدار بگوی</p></div>
<div class="m2"><p>یک شبم از سر لطف از در کاشانه درآ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من چنین واله و سرگشته و مشتاق به تو</p></div>
<div class="m2"><p>تو گریزان زمن خسته نگویی که چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظری کن به دو چشمم توبه حالم صنما</p></div>
<div class="m2"><p>که ز هجران تو چون زلف تو گشتم شیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون به خاک در تو تشنه به جانم چه کنم</p></div>
<div class="m2"><p>از سر لطف و کرامت نظری کن سوی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جفا تا به کی آخر دل ما بخراشی</p></div>
<div class="m2"><p>می نیابم ز سر کوی تو بویی ز وفا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه در کار جهان نیست وفا می دانم</p></div>
<div class="m2"><p>لیکن از یار بگو چند توان برد جفا</p></div></div>