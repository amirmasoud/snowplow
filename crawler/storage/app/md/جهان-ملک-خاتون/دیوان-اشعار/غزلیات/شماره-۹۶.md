---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>ز هوای سر زلف تو دلم پر سوداست</p></div>
<div class="m2"><p>وز خیال رخ تو دیده ی جان خون پیماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوس وصل تو دارد دل سرگشته ی من</p></div>
<div class="m2"><p>سر در این سر شود ای دوست که اندیشه ی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفت از باد هوا زود پریشان گردد</p></div>
<div class="m2"><p>سر سبک دارد از آن روی چنین بی سر و پاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خار هجر تو دل خسته ی ما بخراشید</p></div>
<div class="m2"><p>گل روی تو نبینیم، چنین ظلم رواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو رخسار تو نشکفت گلی در بستان</p></div>
<div class="m2"><p>در چمن چون قد زیبات کجا سروی خاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور بود نیز نه چون قامت رعنات بود</p></div>
<div class="m2"><p>من بگویم سخنی چون قد و بالای تو راست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسبت روی تو با ماه چنین می کردم</p></div>
<div class="m2"><p>نیک دیدیم نگارا ز کجا تا به کجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظرم بر مه و خورشید نیفتاد دگر</p></div>
<div class="m2"><p>تا سواد رخت از دیده ی غمدیده جداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواری و جور و جفا بر من مسکین تا چند</p></div>
<div class="m2"><p>مکن ای جان عزیزم مکن این عین خطاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من مسکین دو جهان در سر و کارت کردم</p></div>
<div class="m2"><p>آخر این جور و جفا بر من بیچاره چراست</p></div></div>