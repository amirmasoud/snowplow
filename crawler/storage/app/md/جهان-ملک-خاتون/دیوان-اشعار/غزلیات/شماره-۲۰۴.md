---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>بنای خاطر ما از غم تو ویرانست</p></div>
<div class="m2"><p>ز سوز عشق رخت آتشیم در جانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ز من طلبد جان از او دریغم نیست</p></div>
<div class="m2"><p>هزار جان عزیزم فدای جانانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عید روی تو گفتم به دل چه چاره کنم</p></div>
<div class="m2"><p>جواب داد که جز جان تو را چه قربانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا که در شب وصل تو جان شیرین را</p></div>
<div class="m2"><p>نهاده بر کف دستم که تا چه فرمانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شده ملول طبیبان عالم از دردم</p></div>
<div class="m2"><p>از آن که درد مرا روز وصل درمانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز وصل خویش تو مجموع کن دل ما را</p></div>
<div class="m2"><p>که خاطرم چو سر زلف تو پریشانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدم ز سر کنم و راه شوق بسپارم</p></div>
<div class="m2"><p>که راه کعبه ی مقصودم از بیابانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کدام بلبل خوش گو چو من بود در باغ</p></div>
<div class="m2"><p>کدام گل چو رخ دوست در گلستانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سزد که گر نرود پای نارون که دگر</p></div>
<div class="m2"><p>خجل ز قامت رعناش سرو بستانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا ز یاد غمش گو که بر گل رویش</p></div>
<div class="m2"><p>شنیده ای که جهانی هزار دستانست</p></div></div>