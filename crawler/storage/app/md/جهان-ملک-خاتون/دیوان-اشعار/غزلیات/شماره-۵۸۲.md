---
title: >-
    شمارهٔ ۵۸۲
---
# شمارهٔ ۵۸۲

<div class="b" id="bn1"><div class="m1"><p>رمید دل ز من خسته پیش دلبر شد</p></div>
<div class="m2"><p>دماغ جان ز سر زلف او معطّر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو روی آن بت رعنا بدید از سر شوق</p></div>
<div class="m2"><p>ز جان غلام رخ یار ماه پیکر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه زلف سیاهت به شب رهش گم کرد</p></div>
<div class="m2"><p>به بوی عنبر ساراش باز رهبر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد دوش کشیدم به خواب طرّه یار</p></div>
<div class="m2"><p>دو دستم از سر زلفین او معنبر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآمد از درم آن ماه روی سیم اندام</p></div>
<div class="m2"><p>ز روش کلبه احزان ما منوّر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه کرّه بی باک چرخ، توسن بود</p></div>
<div class="m2"><p>به تازیانه وصلش دگر مسخّر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصال دوست به زاری زار می جستم</p></div>
<div class="m2"><p>هزار شکر که آن دولتم میسّر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو روی تو نبود نقش در جهان باری</p></div>
<div class="m2"><p>به کارگاه خیالم چنین مصوّر شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا تو دست برآورده ای به غارت دل</p></div>
<div class="m2"><p>مگر به دور جمالت جهان مسخّر شد</p></div></div>