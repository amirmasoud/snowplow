---
title: >-
    شمارهٔ ۱۱۵۳
---
# شمارهٔ ۱۱۵۳

<div class="b" id="bn1"><div class="m1"><p>بیا دردم به وصل خود دوا کن</p></div>
<div class="m2"><p>ز لعلت کام جان ما روا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصلم وعده ی بسیار دادی</p></div>
<div class="m2"><p>یکی زان وعده ها آخر وفا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلاف بی وفایی کز تو دیدم</p></div>
<div class="m2"><p>وفا داری کن و ترک جفا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن بیگانگی با ما ازین بیش</p></div>
<div class="m2"><p>مرا با خود زمانی آشنا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که گفتت ای نگار شوخ دلبر</p></div>
<div class="m2"><p>چو چشم بد مرا از خود جدا کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا از وصل خود بنواز یک شب</p></div>
<div class="m2"><p>نظر ای دوست آخر بر خدا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صلح آخر شبی از در درآیم</p></div>
<div class="m2"><p>اگر مردی به ترک ماجرا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو سروناز بستانی حقیقت</p></div>
<div class="m2"><p>شدم خاکت گذر بر سوی ما کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبیب من تویی از روی احسان</p></div>
<div class="m2"><p>جهانی را ز وصل خود دوا کن</p></div></div>