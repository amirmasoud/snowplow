---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>هر چند دلارام مرا مهر و وفا نیست</p></div>
<div class="m2"><p>یک لحظه خیالش ز من خسته جدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یار جفاپیشه اگر ترک وفا کرد</p></div>
<div class="m2"><p>میلش سوی یاران وفادار چرا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که برد نزد دلارام پیامی</p></div>
<div class="m2"><p>کس محرم عشّاق بجز باد صبا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پیک سحر از من مهجور بگویش</p></div>
<div class="m2"><p>زین بیش جفا بر من دلخسته روا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازآی که رنجور غم از درد جدایی</p></div>
<div class="m2"><p>می سوزد و جز وصل توأش هیچ دوانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی به علی رغم بداندیش وفا کن</p></div>
<div class="m2"><p>حیفست که با ما نظرت جز به جفا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیهات که مهر از تو توان داشت توقّع</p></div>
<div class="m2"><p>کابین وفا قاعده ی شهر شما نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که غم عشق توأم مونس جانست</p></div>
<div class="m2"><p>گفتا غم ما در خور هر بی سر و پا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل غم احوال جهان بیش میندیش</p></div>
<div class="m2"><p>کاین حادثه ی چرخ در اندیشه ی ما نیست</p></div></div>