---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>تا تو از لب کرده ای درمان ما</p></div>
<div class="m2"><p>آتشی افکنده ای در جان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چشم ما تو مردم زاده ای</p></div>
<div class="m2"><p>یک شبی از لطف شو مهمان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نیاید چشم بیدارش به خواب</p></div>
<div class="m2"><p>بر سر هر کو رسد افغان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم از دستش برون آریم دل</p></div>
<div class="m2"><p>چون کنم دل نیست در فرمان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک نظر گفتم کند در ما ولی</p></div>
<div class="m2"><p>هست فارغ از گدا سلطان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن از جست و جوی قامتت</p></div>
<div class="m2"><p>شد پراکنده سر و سامان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تکلّف راست گویم در جهان</p></div>
<div class="m2"><p>نیست قدّی چون قد جانان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس عجب دیدم که آن سرو سهی</p></div>
<div class="m2"><p>چون برون رفت از سر پیمان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عید رویش گفت با من کای جهان</p></div>
<div class="m2"><p>جان چه باشد تا کنی قربان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند گردد بر سر کویت مدام</p></div>
<div class="m2"><p>این دل مسکین سرگردان ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل ببرد از ما و رفت آن بی وفا</p></div>
<div class="m2"><p>کشت تخم قهر خود در جان ما</p></div></div>