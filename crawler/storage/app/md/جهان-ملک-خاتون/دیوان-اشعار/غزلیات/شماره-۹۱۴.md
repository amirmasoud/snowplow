---
title: >-
    شمارهٔ ۹۱۴
---
# شمارهٔ ۹۱۴

<div class="b" id="bn1"><div class="m1"><p>تا سر کوی تو مأوا کرده ام</p></div>
<div class="m2"><p>دولت وصلت تمنّا کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی زلفت می دهد باد صبا</p></div>
<div class="m2"><p>زان سبب آهنگ صحرا کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل همی خواهد که جوید مسکنی</p></div>
<div class="m2"><p>من سر زلف تو پیدا کرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام خود را در جهان از عشق تو</p></div>
<div class="m2"><p>عاشق و بدنام و رسوا کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان بی قدّ او در پیش سرو</p></div>
<div class="m2"><p>ناکسم گر سر به بالا کرده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست در زلفین او خواهم زدن</p></div>
<div class="m2"><p>باز بنگر تا چه سودا کرده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک کویت را به جای توتیا</p></div>
<div class="m2"><p>در جهان بین روشنی زا کرده ام</p></div></div>