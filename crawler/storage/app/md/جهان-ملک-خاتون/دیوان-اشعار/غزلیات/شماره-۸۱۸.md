---
title: >-
    شمارهٔ ۸۱۸
---
# شمارهٔ ۸۱۸

<div class="b" id="bn1"><div class="m1"><p>دلم بر آتشست از لعل نوشش</p></div>
<div class="m2"><p>بدان تا که بیابم پای بوسش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من بربود صبر و هوش و آرام</p></div>
<div class="m2"><p>مسلمانان به چشم می فروشش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخورد او خون جانم را به غمزه</p></div>
<div class="m2"><p>که همچون شیر مادر باد نوشش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قسم دارم بر آن چشمان مخمور</p></div>
<div class="m2"><p>بر آن لعل لبان باده نوشش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آن روی چو ماه و زلف شبرنگ</p></div>
<div class="m2"><p>بدان بالا و برز حلّه پوشش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که خواب و خور ندارم در غم او</p></div>
<div class="m2"><p>جهان را بر فلک برشد خروشش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه باشد ای صبا کز روی یاری</p></div>
<div class="m2"><p>رسانی حال زار ما به گوشش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگو مشکن دلم را ای دلارام</p></div>
<div class="m2"><p>به یکباره ببردی صبر و هوشش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را گر یوسف مصری به دستت</p></div>
<div class="m2"><p>به قلبی گر خریدت می فروشش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه قلبی کاو بود بر دل ورا نام</p></div>
<div class="m2"><p>به گوش دل چنین گوید سروشش</p></div></div>