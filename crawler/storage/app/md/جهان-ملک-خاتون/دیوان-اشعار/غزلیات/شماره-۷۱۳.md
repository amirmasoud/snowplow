---
title: >-
    شمارهٔ ۷۱۳
---
# شمارهٔ ۷۱۳

<div class="b" id="bn1"><div class="m1"><p>بیا غم از بر آن یار غمگسار رسید</p></div>
<div class="m2"><p>دعا بسی و ثناهای بی شمار رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نامه کرد مشرف مرا نگار به لطف</p></div>
<div class="m2"><p>جهان ز نامه و نامش به اعتبار رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن نوازش و الطاف بنده پرور او</p></div>
<div class="m2"><p>چه خرّمی به دل تنگ سوگوار رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا ببر تو پیامی ز من به سوی نگار</p></div>
<div class="m2"><p>که بس جفا به من خسته دل ز یار رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و چاره ی درد دلم کن از سر لطف</p></div>
<div class="m2"><p>که جان به لب ز غم رویت ای نگار رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل وصال تو را خلق گل فشان کردند</p></div>
<div class="m2"><p>مرا ز گلشن وصلت نصیب خار رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبر به بلبل شوریده ده صبا زنهار</p></div>
<div class="m2"><p>که وقت بانگ تو و ناله هزار رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جواب دادم و گفتم به گل بگو باری</p></div>
<div class="m2"><p>که جان غمزده بر لب ز انتظار رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز روزگار بگو تا که طرف بربندد</p></div>
<div class="m2"><p>بسی ملال که ما را ز روزگار رسید</p></div></div>