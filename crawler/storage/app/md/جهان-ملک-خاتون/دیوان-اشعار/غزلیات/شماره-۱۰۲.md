---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>درد دل دارم همانا وصل او درمان ماست</p></div>
<div class="m2"><p>وز دو لعل آبدارش آتشی در جان ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه بر حالم نسوزد آن دل بی رحم تست</p></div>
<div class="m2"><p>وآنکه با سر می نیاید محنت هجران ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با طبیب درد ما گوی ای نسیم صبحدم</p></div>
<div class="m2"><p>آنکه نپذیرد علاجی درد بی درمان ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو بیا بر دیده ی من جای کن تا گویمت</p></div>
<div class="m2"><p>این قد چون نارون سرو سرابستان ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه سرو ناز بستانی ز ما سر می کشد</p></div>
<div class="m2"><p>چون رسد اینش چو او پیوسته در فرمان ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که در کنج قناعت نیم نانی می خورم</p></div>
<div class="m2"><p>در مقام فقر صد جمشید و کی دربان ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جهان چون تکیه بر درگاه لطفش کرده ام</p></div>
<div class="m2"><p>از عطا و بخشش او هر دو عالم زان ماست</p></div></div>