---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>شوقم به وصل دوست نهایت پذیر نیست</p></div>
<div class="m2"><p>ای دوست از وصال تو ما را گزیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبان روزگار بدیدم به چشم خویش</p></div>
<div class="m2"><p>آن بی نظیر در دو جهانش نظیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که در ضمیر نمی آوری مرا</p></div>
<div class="m2"><p>ما را بجز خیال رخت در ضمیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند آفتاب جهانتاب روشنست</p></div>
<div class="m2"><p>لیکن چو ماه طلعت تو مستنیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ترکتاز حسن تو جانا دلی که دید</p></div>
<div class="m2"><p>کاو در کمند زلف سیاهت اسیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهان به حال فقیران نظر کنند</p></div>
<div class="m2"><p>تو شاه روزگاری و چون من فقیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پا درآمدم ز سر لطف دست گیر</p></div>
<div class="m2"><p>چون جز امید وصل توأم دستگیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمی که در جمال تو حیران نمی شود</p></div>
<div class="m2"><p>حقّا که پیش اهل بصارت بصیر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر خاک آستان تو سر می نهد جهان</p></div>
<div class="m2"><p>زآنش نظر به جانب تاج و سریر نیست</p></div></div>