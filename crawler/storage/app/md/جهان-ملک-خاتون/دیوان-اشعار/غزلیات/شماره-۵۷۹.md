---
title: >-
    شمارهٔ ۵۷۹
---
# شمارهٔ ۵۷۹

<div class="b" id="bn1"><div class="m1"><p>چرا ز وصل تو کامم روا نمی باشد</p></div>
<div class="m2"><p>چرا به بخت منت جز جفا نمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیام من که رساند به یار مهرگسل</p></div>
<div class="m2"><p>رسول من چکنم جز صبا نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو به هجر توأم خون دل ز دیده برفت</p></div>
<div class="m2"><p>به غیر مردمکم کس گوا نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوای درد من ای جان نمی کنی چکنم</p></div>
<div class="m2"><p>به دست بنده به غیر از دعا نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نوش داروی لعلت دوای رنجورست</p></div>
<div class="m2"><p>بده که جز شب وصلت دوا نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بترس از آه دل زار دردمندانت</p></div>
<div class="m2"><p>که تیر آه سحرگه خطا نمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برون مبر ز حد ای جان جفا که در دو جهان</p></div>
<div class="m2"><p>ستم به خسته دلان هم روا نمی باشد</p></div></div>