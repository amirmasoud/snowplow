---
title: >-
    شمارهٔ ۱۳۲۶
---
# شمارهٔ ۱۳۲۶

<div class="b" id="bn1"><div class="m1"><p>بیا که غمزه سرمست تو به دلدوزی</p></div>
<div class="m2"><p>فراق روی تو را می کند بدآموزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بخت یار نباشد بگو چه چاره کنم</p></div>
<div class="m2"><p>که دولت شب وصلت مرا شود روزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم به آتش عشقت بسوخت در سر لطف</p></div>
<div class="m2"><p>تو را به حال من خسته نیست دلسوزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به غمزه گوی کز این پس مریز خون دلم</p></div>
<div class="m2"><p>چرا که نیست به جز شیوه ات جگرسوزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا چو موم گدازان ز تاب هجرانش</p></div>
<div class="m2"><p>تو شمع مجلس انسی بدین دلفروزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و سر مکش از ما چو سرو ناز که من</p></div>
<div class="m2"><p>به روت عاشق دیرینه ام نه امروزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا تو گوشه انسی بگیر از همه خلق</p></div>
<div class="m2"><p>که غیر بار غمش در جهان نیندوزی</p></div></div>