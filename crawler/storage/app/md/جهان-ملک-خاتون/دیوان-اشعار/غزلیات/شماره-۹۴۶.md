---
title: >-
    شمارهٔ ۹۴۶
---
# شمارهٔ ۹۴۶

<div class="b" id="bn1"><div class="m1"><p>از سر سوز جگر بر در آن یار شدم</p></div>
<div class="m2"><p>با دلی پر ز غم و دیده ی خونبار شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم از روی ترحّم جگر ریش مرا</p></div>
<div class="m2"><p>مرهمی نه که به کام دل اغیار شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالها خون جگر خوردم و از دست غمش</p></div>
<div class="m2"><p>هم اسیر ستم دشمن خوانخوار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ملامتگر ازین بیش میازار مرا</p></div>
<div class="m2"><p>که من از دوست جدا از سر نار چار شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از غصّه ی دلدار بپردازم دل</p></div>
<div class="m2"><p>باز فریادکنان بر در دلدار شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک اندیشه ی بیهوده نکردی ای دل</p></div>
<div class="m2"><p>عاقبت تا به بلای تو گرفتار شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش در خواب شدم دولت وصلش دیدم</p></div>
<div class="m2"><p>وز خیالش اثری نیست چو بیدار شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنم از تنگی دل خسته چنان شد حّقا</p></div>
<div class="m2"><p>که من از جان و جهان یکسره بیزار شدم</p></div></div>