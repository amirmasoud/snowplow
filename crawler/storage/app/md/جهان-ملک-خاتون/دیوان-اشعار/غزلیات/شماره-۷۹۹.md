---
title: >-
    شمارهٔ ۷۹۹
---
# شمارهٔ ۷۹۹

<div class="b" id="bn1"><div class="m1"><p>مریز خون دل خلق را نگارا بس</p></div>
<div class="m2"><p>مکن برین دل مسکین جفا خدا را بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر به جانب ما کن چو سرو سیمینی</p></div>
<div class="m2"><p>که یک نظر ز تو ای سرو ناز ما را بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وفا نمای تو باری خلاف رای از چه</p></div>
<div class="m2"><p>نمی شود ز جفا یک زمان شما را بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه خون دلم می خورد نهان چشمت</p></div>
<div class="m2"><p>مریز خون دل خلق آشکارا بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حد بشد به من خسته دل جفای تو زان</p></div>
<div class="m2"><p>ز کار عشق تو ای دلربا وفا را بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گرد جور برآورده ای ز جان جهان</p></div>
<div class="m2"><p>میار پیش تو این باره ی بلا را بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو طاقتم بشد از دست و پای صبر نماند</p></div>
<div class="m2"><p>مزن تو بر دل من ناوک جفا را بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نظر کند آن سایه بر من دلتنگ</p></div>
<div class="m2"><p>بدان که یک نظر پادشا گدا را بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر جهان همه شادی و خرّمی گیرد</p></div>
<div class="m2"><p>کنون مرا غم روی تو غمگسارا بس</p></div></div>