---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>پیش روی تو دلم از سر جان برخیزد</p></div>
<div class="m2"><p>جان چه باشد ز سر هر دو جهان برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق سوخته گر بر سر خاکش گذری</p></div>
<div class="m2"><p>از لحد نعره زنان رقص کنان برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان من و تو پیرهنی مانده حجاب</p></div>
<div class="m2"><p>با کنار آی که آن هم ز میان برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند در خواب رود بخت من شوریده</p></div>
<div class="m2"><p>وقت آنست که از خواب گران برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستم هجر تو زین روی که عالم بگرفت</p></div>
<div class="m2"><p>ترسم آشوب از این دور زمان برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پای شمشاد ز شرم تو بماند در گل</p></div>
<div class="m2"><p>در چمن گر قد سرو تو چمان برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترک وصلت نکنم تا بودم جان در تن</p></div>
<div class="m2"><p>ور به یکباره ام امید ز جان برخیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتنه برخیزد ار آن گلبن نو بنشیند</p></div>
<div class="m2"><p>سرو بنشیند ار آن سرو روان برخیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمّه ای گر ز غم حال جهان برخیزد</p></div>
<div class="m2"><p>ای بسا نعره که از پیر و جوان برخیزد</p></div></div>