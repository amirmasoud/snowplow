---
title: >-
    شمارهٔ ۱۲۶۲
---
# شمارهٔ ۱۲۶۲

<div class="b" id="bn1"><div class="m1"><p>دلا این عشق بازی تا به کی بی</p></div>
<div class="m2"><p>شب هجرش درازی تا به کی بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهی سروا مکن جز راستی هیچ</p></div>
<div class="m2"><p>بهل بازی که بازی تا به کی بی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان با ما دلت با دیگرانست</p></div>
<div class="m2"><p>بگو این حقّه بازی تا به کی بی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم در بوته هجران به زاری</p></div>
<div class="m2"><p>نگویی جان گدازی تا به کی بی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کبوتروار شد در مهربانی</p></div>
<div class="m2"><p>مجو دوری که بازی تا به کی بی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن مر دوستان را خوار و غمخوار</p></div>
<div class="m2"><p>چنین دشمن نوازی تا به کی بی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیاز ما به وصلت هست بسیار</p></div>
<div class="m2"><p>تو را این بی نیازی تا به کی بی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرافکندست نرگس پیش چشمت</p></div>
<div class="m2"><p>ترا این سرفرازی تا به کی بی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا خرقه به خون دیده پالود</p></div>
<div class="m2"><p>به خون جامه نمازی تا به کی بی</p></div></div>