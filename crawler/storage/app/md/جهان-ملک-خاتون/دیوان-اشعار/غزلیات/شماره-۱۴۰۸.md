---
title: >-
    شمارهٔ ۱۴۰۸
---
# شمارهٔ ۱۴۰۸

<div class="b" id="bn1"><div class="m1"><p>ای مرا پیوند جان جانم تویی</p></div>
<div class="m2"><p>چان چه ارزد جان و جانانم تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بسا دردی که دارم از فراق</p></div>
<div class="m2"><p>یک زمان بازآ که درمانم تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی وصالت [نیست] سامانی مرا</p></div>
<div class="m2"><p>هم سری ما را و سامانم تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر حیاتی هست ما را ز آن لبست</p></div>
<div class="m2"><p>جان به تو زنده ست و جانانم تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغ جان را نیست رونق بی قدت</p></div>
<div class="m2"><p>واپس آ چون سرو بستانم تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من شده پروانه ی سوزان تو</p></div>
<div class="m2"><p>در نظر شمع شبستانم تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی رخت در چشم جانم نور نیست</p></div>
<div class="m2"><p>در جهان بین ماه تابانم تویی</p></div></div>