---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>دو دیده از رخ چون آفتاب آب گرفت</p></div>
<div class="m2"><p>ترا چو بخت من ای دوست از چه خواب گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا تو روی خود از چشم ما بپوشانی</p></div>
<div class="m2"><p>که دیده و که شنیده که مه نقاب گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به لابه گفتم کامم بده از آن لب لعل</p></div>
<div class="m2"><p>ز روی لطف مرا ساغری شراب گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال قامت آن سرو چون نگار ببین</p></div>
<div class="m2"><p>درون دیده ی ما آمد و سراب گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو زلف سرکش شوخت ز جیب تابنده</p></div>
<div class="m2"><p>مگر ز آتش رخسار یار تاب گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آهم ار بنشیند بر آینه گردی</p></div>
<div class="m2"><p>فغان ز خلق برآید که ماهتاب گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نوبهار کسی را که نیست عقل معاش</p></div>
<div class="m2"><p>به گوشه ی چمنی شد کنار آب گرفت</p></div></div>