---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>بسیار بگفتم دل دیوانه ی خود را</p></div>
<div class="m2"><p>پندم نکند گوش زهی خیره ی خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو همی خواهم و خوبست مرا رأی</p></div>
<div class="m2"><p>تدبیر ندارم چکنم طالع بد را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد ز دست دل خودرأی بلاکش</p></div>
<div class="m2"><p>الحق که به جانم شده دشمن دل خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست است مدام از قدح شوق تو جانم</p></div>
<div class="m2"><p>بر مست ملامت نرسد اهل خرد را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه صفت پیش تو ای شمع جهان سوز</p></div>
<div class="m2"><p>خواهم که بسوزم همگی جسم و جسد را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل صفتم ناله هزارست ز شوقت</p></div>
<div class="m2"><p>ای خار چو من کرده گل روی تو صد را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دیده چنان سیل محبّت بگشایم</p></div>
<div class="m2"><p>کز سینه ی دشمن ببرم زنگ حسد را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن روز که در خاک تنم را بسپارند</p></div>
<div class="m2"><p>بر یاد تو چون روضه کنم خاک لحد را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر جان جهان داغم غم هجر تو تا کی</p></div>
<div class="m2"><p>کم سوز دل سوخته، دارای صمد را</p></div></div>