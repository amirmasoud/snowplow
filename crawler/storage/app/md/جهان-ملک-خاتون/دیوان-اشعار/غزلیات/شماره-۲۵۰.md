---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>ما را به درد عشق تو جز صبر چاره چیست</p></div>
<div class="m2"><p>وز دور در جمال رخت جز نظاره چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمی نمی کنی به من خسته ی غریب</p></div>
<div class="m2"><p>آخر بگو که آن دل چون سنگ خاره چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ریش بود از سر تیغ جفای تو</p></div>
<div class="m2"><p>بر ریش بیش جور و جفا بی شماره چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروی و در میان دو چشمم نشسته ای</p></div>
<div class="m2"><p>مقصود قد و قامتت از ما کناره چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستم نمی رسد به گریبان وصل تو</p></div>
<div class="m2"><p>پس دامن دلم ز فراق تو پاره چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه کیست تا که دعوی خوبی کند برت</p></div>
<div class="m2"><p>با روی دلفریب تو نور ستاره چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل ز انقلاب جهان تنگ دل مشو</p></div>
<div class="m2"><p>احوال روزگار چنین است چاره چیست</p></div></div>