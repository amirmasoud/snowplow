---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>وصالت دوای دل دردمند</p></div>
<div class="m2"><p>در وصل از این بیش بر ما مبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن گریه چون ابر بر جان من</p></div>
<div class="m2"><p>چو گل بر من و حال زارم مخند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسوزان مرا از فراق رخت</p></div>
<div class="m2"><p>که او آتش است و دل ما سپند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دارد بتی مهوش همچو من</p></div>
<div class="m2"><p>دو ابرو کمان و دو گیسو کمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخواهم به هر دو جهان جز تو کس</p></div>
<div class="m2"><p>اگر روز حشرم مخیر کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگیرد دلم انس با هیچکس</p></div>
<div class="m2"><p>گرم بی رخ تو به جنّت برند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو تیر جفایت ببارد ز ابر</p></div>
<div class="m2"><p>نشاید که مژگان به هم برزنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر در غمت ناله ای بشنوی</p></div>
<div class="m2"><p>ز مردان نه مردند ایشان زنند</p></div></div>