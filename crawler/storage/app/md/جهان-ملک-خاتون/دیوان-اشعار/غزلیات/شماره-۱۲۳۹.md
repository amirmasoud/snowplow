---
title: >-
    شمارهٔ ۱۲۳۹
---
# شمارهٔ ۱۲۳۹

<div class="b" id="bn1"><div class="m1"><p>تو را لبیست نگارا چو غنچه پرخنده</p></div>
<div class="m2"><p>مرا سریست چو نرگس به پیش افکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقاب از رخ خود برگشا که تا خورشید</p></div>
<div class="m2"><p>شود ز تاب رخ روشن تو شرمنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنت ز درد مصون باد و دل ز غم آزاد</p></div>
<div class="m2"><p>که نیست ذات شریفت به این دو ارزنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت ز جان شده‌ام بنده و خداوندان</p></div>
<div class="m2"><p>نظر ز روی عنایت کنند بر بنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آنکه روی تو را صبح و شام می‌بیند</p></div>
<div class="m2"><p>یقین شدم که ورا دولتیست پاینده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر به جانب ما کن ز روی لطف دمی</p></div>
<div class="m2"><p>که سال و ماه و شب و روز تست فرخنده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو همدمم دم عیسی‌دم است گو یکدم</p></div>
<div class="m2"><p>بدم که تا دو جهان گردد از دمش زنده</p></div></div>