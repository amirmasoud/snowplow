---
title: >-
    شمارهٔ ۱۱۲۰
---
# شمارهٔ ۱۱۲۰

<div class="b" id="bn1"><div class="m1"><p>صبا برو تو پیامی ز من به یار رسان</p></div>
<div class="m2"><p>غم فراق چو دانی به غمگسار رسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلام و پرسش بی حد به اشتیاق تمام</p></div>
<div class="m2"><p>ازین کمینه خاکی بدان نگار رسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو شرح حال من خسته دل نکو دانی</p></div>
<div class="m2"><p>ز روی لطف خدا را بدان دیار رسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قرار نیست چو من در دو زلف سرکش دوست</p></div>
<div class="m2"><p>بیا ز نکهت زلفش به بیقرار رسان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو فراق رسانید جان ما بر لب</p></div>
<div class="m2"><p>بیا و منتظری را به انتظار رسان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو جان به لعل لبت تشنه ام نگارینا</p></div>
<div class="m2"><p>بیا و تشنه وصلت به چشمه سار رسان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نگار شبی حال زار ما پرسد</p></div>
<div class="m2"><p>بگو بیا و جهان را به اعتبار رسان</p></div></div>