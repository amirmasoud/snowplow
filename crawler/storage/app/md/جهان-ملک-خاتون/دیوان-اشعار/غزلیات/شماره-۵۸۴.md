---
title: >-
    شمارهٔ ۵۸۴
---
# شمارهٔ ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>دیده ای کاو به سر کوی وفا رهبر شد</p></div>
<div class="m2"><p>بی تکلّف به همه ملک جهان سرور شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام بی تو نمی دید جهان را گویی</p></div>
<div class="m2"><p>توتیای شب وصل تو ورا رهبر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان همی داد دلم در هوس دیدارت</p></div>
<div class="m2"><p>شکر یزدان که به مهر رخ تو بیمر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فصل ایام بهارست و لب جوی خوشست</p></div>
<div class="m2"><p>لیک با زیور حسن تو کنون بهتر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوسن از جمله ریاحین چو به بو کمتر بود</p></div>
<div class="m2"><p>بوی خلقت بشنید او و زبان آور شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار من دور شدی باز ندانم خود را</p></div>
<div class="m2"><p>چه کنم با تو مرا دیده و دل خوگر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش عشق تو هر لحظه فزونست مرا</p></div>
<div class="m2"><p>مهر ما روز به روز از دل تو کمتر شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان بدادم به امید شب وصلت باری</p></div>
<div class="m2"><p>روزی ما نشد و زان کسی دیگر شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پراکندگی حال جهان بی خبری</p></div>
<div class="m2"><p>زلف گمراه تو زان روی چنین کافر شد</p></div></div>