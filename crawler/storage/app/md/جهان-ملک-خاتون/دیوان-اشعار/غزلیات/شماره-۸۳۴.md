---
title: >-
    شمارهٔ ۸۳۴
---
# شمارهٔ ۸۳۴

<div class="b" id="bn1"><div class="m1"><p>بیا که دیده ی من دید دوش خوابی خوش</p></div>
<div class="m2"><p>برآمده به شب تیره آفتابی خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به روی او نظرم خیره شد چنان دیدم</p></div>
<div class="m2"><p>که بست بر رخ چون ماه خود نقابی خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سؤال کردم و گفتم نقاب بر مه چیست؟</p></div>
<div class="m2"><p>که هست میل دل من به ماهتابی خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبند بر رخ چون آفتاب خویش نقاب</p></div>
<div class="m2"><p>که هست در سر آن هر دو زلف تابی خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتمش ز در بخت ما درآ یک شب</p></div>
<div class="m2"><p>نگشت با من مسکین به هیچ بابی خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جان دوست که شبهاست تا ز درد فراق</p></div>
<div class="m2"><p>دو چشم بخت بد من نکرد خوابی خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز درد روز فراقش به غیر خون جگر</p></div>
<div class="m2"><p>به جان دوست که هرگز نخوردم آبی خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا شراب ز خون دلست و از دیده</p></div>
<div class="m2"><p>بجز جگر نبود خوردنم کبابی خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز درد دل چو نبشتم شکایتی به طبیب</p></div>
<div class="m2"><p>به غیر جور نفرمود او جوابی خوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخت گلست و مرا دل ز عشقت آتش محض</p></div>
<div class="m2"><p>از آن زند به رخم دیده ها گلابی خوش</p></div></div>