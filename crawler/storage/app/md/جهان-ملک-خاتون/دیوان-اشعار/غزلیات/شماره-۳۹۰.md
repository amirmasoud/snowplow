---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>چرا به کار من ای جان وفا نکردی هیچ</p></div>
<div class="m2"><p>به حال خسته دلان جز جفا نکردی هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا ز لعل لب آبدار خود کامم</p></div>
<div class="m2"><p>شبی ز روی ارادت روا نکردی هیچ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیب درد منی راست گو که از چه سبب</p></div>
<div class="m2"><p>ز روز وصل دلم را دوا نکردی هیچ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لطف با همه کس در میان و بس شادان</p></div>
<div class="m2"><p>به بخت ما بجز از ماجرا نکردی هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی خطاب کشیدم ز روز هجرانت</p></div>
<div class="m2"><p>به وصل ما تو به غیر از خطا نکردی هیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سرو ناز خرامیده ای میان چمن</p></div>
<div class="m2"><p>نظر ز روی عنایت به ما نکردی هیچ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو پادشاه جهانی و من گدای غریب</p></div>
<div class="m2"><p>ترحمی ز چه رو بر گدا نکردی هیچ</p></div></div>