---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>آن دل نگویمش من آن سنگ خاره باشد</p></div>
<div class="m2"><p>از دست جور هجرت صد جامه پاره باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقع ز روی برکن ای ماه دلفروزم</p></div>
<div class="m2"><p>چون وصل نیست باری یک دم نظاره باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای قدّ همچو سروت در غایت بلندی</p></div>
<div class="m2"><p>سر می کشد قد تو از ما چه چاره باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشّاق روی خوبت بسیار در جهانند</p></div>
<div class="m2"><p>چون من هزار عاشق کی در شماره باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بلبل غزلخوان بر روی چون گل تو</p></div>
<div class="m2"><p>آخر بگو نگارا دستان چه کاره باشد</p></div></div>