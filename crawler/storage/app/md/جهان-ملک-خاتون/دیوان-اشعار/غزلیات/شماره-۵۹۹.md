---
title: >-
    شمارهٔ ۵۹۹
---
# شمارهٔ ۵۹۹

<div class="b" id="bn1"><div class="m1"><p>دل من در طلبکاری وصل تو به جان آمد</p></div>
<div class="m2"><p>ز دست جورت ای دلبر جهانی در فغان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جورت گفتم ای دل ترک مهرش کن مکش خواری</p></div>
<div class="m2"><p>جوابم داد و گفت آری به دل گر بر توان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو چشم مست خون خوارش خطا کرد از جفا بر من</p></div>
<div class="m2"><p>اشارت کرد بر ابرو و دردم در کمان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن تیری که بگشود او ز شست و غمزه و ابرو</p></div>
<div class="m2"><p>چو دیدم ناگه از هر سو به جان ناتوان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز یادت در همه عمرم نگشتم یک زمان خالی</p></div>
<div class="m2"><p>زمانی مهربانی کن که از غم دل به جان آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان را جز غم رویت نباشد در جهان کاری</p></div>
<div class="m2"><p>تو گویی از برای مهر رویت در جهان آمد</p></div></div>