---
title: >-
    شمارهٔ ۱۲۹۲
---
# شمارهٔ ۱۲۹۲

<div class="b" id="bn1"><div class="m1"><p>چه باشد ار ز من خسته دل تو یاد آری</p></div>
<div class="m2"><p>ز روزگار وصال و ز عهد دلداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن تو عهد فراموش و مگسل آن پیمان</p></div>
<div class="m2"><p>مکش چو سرو سر از ما اگر وفاداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جور و غصه بیازرده ای دل ما را</p></div>
<div class="m2"><p>چه باشد ار ز وصالم به لطف باز آری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندادیم شبکی کام دل ز لعل لبم</p></div>
<div class="m2"><p>ببردی از من بیچاره دل به عیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عزیز مصر دل خلق عالمی بودم</p></div>
<div class="m2"><p>مکن چنین به عزیزان کسی کند خواری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خواب دیده ام آن روی همچو ماهوشش</p></div>
<div class="m2"><p>چه باشد ار بنماید دمی به بیداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به چشم مست و سر زلف دل ز ما بربود</p></div>
<div class="m2"><p>نداد کام دل ما زهی سیه کاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهاد بار جهان بر دل من آن دلبر</p></div>
<div class="m2"><p>وفا نکرد و جفا می کند به سر باری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم تو را ز جهان بنده ای بدار مرا</p></div>
<div class="m2"><p>که آن زمان به تو زیبا بود جهانداری</p></div></div>