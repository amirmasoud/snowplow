---
title: >-
    شمارهٔ ۱۱۰۲
---
# شمارهٔ ۱۱۰۲

<div class="b" id="bn1"><div class="m1"><p>درد دل خویش با که گویم</p></div>
<div class="m2"><p>درمان دل خود از که جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو اوفتان و خیزان</p></div>
<div class="m2"><p>سرگشته ی زلف تو چو گویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم به لب آمد از فراقت</p></div>
<div class="m2"><p>در جستن وصل چند پویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر حال دلم دهد گواهی</p></div>
<div class="m2"><p>خوناب دو چشم و رنگ رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شوق میان همچو مویت</p></div>
<div class="m2"><p>از غصّه گداخته چو مویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خاک وجود ناشکیبا</p></div>
<div class="m2"><p>گر کوزه گری کند سبویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر شادی دوست و رغم دشمن</p></div>
<div class="m2"><p>من ترک وصال تو نگویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند تحمّل فراقت</p></div>
<div class="m2"><p>آخر نه ز آهن و نه رویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نور دو دیده چهره از غم</p></div>
<div class="m2"><p>تا چند به آب دیده شویم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر یار ز عشق خویش صد داغ</p></div>
<div class="m2"><p>بر جان و دلم نهد چه گویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از جمله مخلصان چه باشد</p></div>
<div class="m2"><p>من بنده ی بندگان اویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیر از تو کسی دگر ندارم</p></div>
<div class="m2"><p>ای از دو جهان تو آرزویم</p></div></div>