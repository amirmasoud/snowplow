---
title: >-
    شمارهٔ ۱۱۵۸
---
# شمارهٔ ۱۱۵۸

<div class="b" id="bn1"><div class="m1"><p>نگارا بر من مسکین نظر کن</p></div>
<div class="m2"><p>ز آب چشم مظلومان حذر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا ای باد صبح ار می توانی</p></div>
<div class="m2"><p>نگارم را ز حال ما خبر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو ای سرو ناز بوستانی</p></div>
<div class="m2"><p>ز لطفت یک زمان بر ما گذر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا در دام عشق او اسیری</p></div>
<div class="m2"><p>مرادت بر نمی آید سفر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفر کردن دوای درد عشقست</p></div>
<div class="m2"><p>برو یا عشق او از سر بدر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سنان غمزه اش خونریزتر گشت</p></div>
<div class="m2"><p>توانی جان و دل پیشش سپر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم هجرانش چون استاد عشقست</p></div>
<div class="m2"><p>بیا دل قصّه عشقش ز بر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو تا کی در جهان سرگشته گردی</p></div>
<div class="m2"><p>برو دستی در آن آر و کمر کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز سودا زود در زلفش درآویز</p></div>
<div class="m2"><p>شکنج طره اش زیر و زبر کن</p></div></div>