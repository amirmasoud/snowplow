---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>درد ما را ز وصال تو دوا کی باشد</p></div>
<div class="m2"><p>کام جانم ز دهان تو روا کی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وفا وعده همی کرد که یارت باشم</p></div>
<div class="m2"><p>در دل ماه رخان مهر و وفا کی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودی غم کارت بخورم صبری کن</p></div>
<div class="m2"><p>صبرم از روی نگارین تو تا کی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه جان را به غمت باخت و نشد شاد به وصل</p></div>
<div class="m2"><p>به غم و اندُهت ای دوست سزا کی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیش از این جور و جفا بر من مسکین مپسند</p></div>
<div class="m2"><p>که مرا طاقت این جور و جفا کی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از جهانم شده یکتا به غمت خرسندم</p></div>
<div class="m2"><p>که به یک دل صنما قبله ی دوتا کی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو سلطان جهانی نظری دار به ما</p></div>
<div class="m2"><p>گرچه اندیشه سلطان و گدا کی باشد</p></div></div>