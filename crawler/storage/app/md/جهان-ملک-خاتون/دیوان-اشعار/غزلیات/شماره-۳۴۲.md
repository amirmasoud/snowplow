---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>هوای کوی جانان خوش هواییست</p></div>
<div class="m2"><p>مگر آرام جان مبتلاییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بالای تو عاشق گشتم ای مه</p></div>
<div class="m2"><p>بلای عشق جانان خوش بلاییست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم صبح عنبر بو از آنست</p></div>
<div class="m2"><p>که بویش بوی زلف آشناییست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیالت هست دایم در دو دیده</p></div>
<div class="m2"><p>بر آن سر مردم چشمم گواییست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا جانا دمی کز دست هجران</p></div>
<div class="m2"><p>میان دیده و دل ماجراییست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه ساکن شده بر خاک کویت</p></div>
<div class="m2"><p>اگر باشد گدا ور پادشاییست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بخرامی اندر باغ جانم</p></div>
<div class="m2"><p>سراب چشم من دانی چه جاییست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیفکن سایه بر سر یک زمانم</p></div>
<div class="m2"><p>که تا گویم جهان را خوش هماییست</p></div></div>