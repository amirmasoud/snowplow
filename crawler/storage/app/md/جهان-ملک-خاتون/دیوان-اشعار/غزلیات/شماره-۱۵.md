---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>گرچه کردی تو به یک بار فراموش مرا</p></div>
<div class="m2"><p>نرود یاد تو از خاطر مدهوش مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خدا دولت وصلت به دعا می خواهم</p></div>
<div class="m2"><p>تا کشی رغم بداندیش در آغوش مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهر و تریاک و گل و خار به هم بنهادند</p></div>
<div class="m2"><p>چند گویی که برو نیش تو را نوش مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مرا بخت وصال تو نباشد اولیست</p></div>
<div class="m2"><p>بار هجر تو کشیدن به سر و دوش مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از درد و غم عشق فغان بردارم</p></div>
<div class="m2"><p>می کند وعده ی دیدار تو خاموش مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوش سودای غم عشق تو در سر دارم</p></div>
<div class="m2"><p>سر همانا که رود بر سر این جوش مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد عشق تو گر از خلق نهان داشتمی</p></div>
<div class="m2"><p>برگرفت از دو جهان عشق تو سرپوش مرا</p></div></div>