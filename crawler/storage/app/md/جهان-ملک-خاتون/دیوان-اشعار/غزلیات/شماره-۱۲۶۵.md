---
title: >-
    شمارهٔ ۱۲۶۵
---
# شمارهٔ ۱۲۶۵

<div class="b" id="bn1"><div class="m1"><p>تا که مهر مهرم از درج وفا برداشتی</p></div>
<div class="m2"><p>بی خطایی از چه رو راه خطا برداشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بت سیمین بر نامهربان سنگ دل</p></div>
<div class="m2"><p>شرم بادت بی گناهی دل ز ما برداشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نارسیده از گلستان رخت بویی به ما</p></div>
<div class="m2"><p>از چه معنی خنجر خار جفا برداشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل آشفته ی شیدای سرگردان بگو</p></div>
<div class="m2"><p>کز میان مهربانان خود که را برداشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه از جان و جهانت در طلب سرگشته بود</p></div>
<div class="m2"><p>چون تو را گشتست از او خاطر چرا برداشتی</p></div></div>