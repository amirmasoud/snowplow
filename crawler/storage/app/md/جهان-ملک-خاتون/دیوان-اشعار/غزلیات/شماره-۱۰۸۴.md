---
title: >-
    شمارهٔ ۱۰۸۴
---
# شمارهٔ ۱۰۸۴

<div class="b" id="bn1"><div class="m1"><p>بسیار در فراق تو زنهارها زدیم</p></div>
<div class="m2"><p>فریاد عاشقانه به بازارها زدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون گل رخت بنظر در نیامدم</p></div>
<div class="m2"><p>بسیارها تفرّج گلزارها زدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستم به قدّ سرو روانت نمی رسد</p></div>
<div class="m2"><p>بسیار دست بر سر دیوارها زدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تدبیر راه وصل تو کردن نمی توان</p></div>
<div class="m2"><p>عمری در این معامله آن جارها زدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبهای تار در غم هجرانت ای صنم</p></div>
<div class="m2"><p>بر مردمک ز شوق تو مسمارها زدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون چنگ در خروشم و چون نی ز ناله زار</p></div>
<div class="m2"><p>در راه عشق روی تو اسرارها زدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر روی ما دری نگشادی ز راه وصل</p></div>
<div class="m2"><p>سر همچو حلقه بر در تو بارها زدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در راه وصل اگرچه مغیلان به راه بود</p></div>
<div class="m2"><p>ما آتشی ز آه در آن خارها زدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسکین دلم ز بار غمت در جهان بسوخت</p></div>
<div class="m2"><p>خونابها زدیده بدان بارها زدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم که نار دل بنشانم به آب اشک</p></div>
<div class="m2"><p>ننشست و از فراق تو زنهارها زدیم</p></div></div>