---
title: >-
    شمارهٔ ۱۱۰۰
---
# شمارهٔ ۱۱۰۰

<div class="b" id="bn1"><div class="m1"><p>چه دردست این که درمانش ندانیم</p></div>
<div class="m2"><p>چه بحرست این که پایانش ندانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال سروی اندر چشم ما رست</p></div>
<div class="m2"><p>که قطعاً ره به بستانش ندانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیبانم دوایی تلخ گفتند</p></div>
<div class="m2"><p>که ما درمان هجرانش ندانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا روی دل اندر کعبه ی وصل</p></div>
<div class="m2"><p>ولی حدّ بیابانش ندانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عید روی چون خورشید و ماهش</p></div>
<div class="m2"><p>بجز جان هیچ قربانش ندانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به قول خود وفا ننمود باری</p></div>
<div class="m2"><p>به غیر از نقض پیمانش ندانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان خوش شد در این موسم خدا را</p></div>
<div class="m2"><p>بلای هجر آسانش ندانیم</p></div></div>