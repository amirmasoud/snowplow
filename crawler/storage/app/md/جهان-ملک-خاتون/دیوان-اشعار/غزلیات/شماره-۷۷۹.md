---
title: >-
    شمارهٔ ۷۷۹
---
# شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>هست چون زلف بتانم هوس عمر دراز</p></div>
<div class="m2"><p>تا دمی درد دل خویش بگویم به تو باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر به گوش تو نهم حال جهان عرضه دهم</p></div>
<div class="m2"><p>تا نظر بر من بیچاره کنی از سر ناز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرّ عشق تو نخواهم که بگویم با کس</p></div>
<div class="m2"><p>خاصه با باد صبا کاو نبود محرم راز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور وصل تو چو عمرست شتابان چه کنم</p></div>
<div class="m2"><p>چند نالم ز غم عشق تو شبهای دراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند پیش رخ مه پیکر تو جان جهان</p></div>
<div class="m2"><p>همچو شمعی بود از هجر تو در سوز و گداز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویم به لب آمد ز غمم جان عزیز</p></div>
<div class="m2"><p>چند گویی به من خسته که با درد بساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل بیچاره ی من با غم عشقت چه کند</p></div>
<div class="m2"><p>چون کبوتر نتواند که کند حمله به باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغ جان من مسکین به هواداری تو</p></div>
<div class="m2"><p>آن تواند که کند بر سر کویت پرواز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وا پس آ جان گرامی به تنم تا گویند</p></div>
<div class="m2"><p>عمر بگذشت ولی جان به جهان آمد باز</p></div></div>