---
title: >-
    شمارهٔ ۱۳۷۰
---
# شمارهٔ ۱۳۷۰

<div class="b" id="bn1"><div class="m1"><p>ای ماه اگر به گوشه گلشن نظر کنی</p></div>
<div class="m2"><p>گل را ز حال بلبل بیدل خبر کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که جمله چشم امیدم به راه تست</p></div>
<div class="m2"><p>شاید گرم به گوشه چشمی نظر کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما بینوا و مفلس و تو کیمیافروش</p></div>
<div class="m2"><p>آخر چه باشد ار مس ما را به زر کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما همچو خاک راه فتاده به کوی دوست</p></div>
<div class="m2"><p>واجب کند که بر سر خاکی گذر کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ساقی مراد چه باشد اگر شبی</p></div>
<div class="m2"><p>از جام وصل خویش مرا بی خبر کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکین دلا تو تا به کی آخر ز جور یار</p></div>
<div class="m2"><p>جان را به پیش تیر فراقش سپر کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا سر بر آستانه مهرش بنه ز جان</p></div>
<div class="m2"><p>یا آنکه این هوا ز سر دل به در کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>واجب کند اگر نکند بعد ازین وفا</p></div>
<div class="m2"><p>زان غمزه های شوخ جفاجو حذر کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل اگر مراد همی خواهی از جهان</p></div>
<div class="m2"><p>در کوی دوست دیده و دل جان سپر کنی</p></div></div>