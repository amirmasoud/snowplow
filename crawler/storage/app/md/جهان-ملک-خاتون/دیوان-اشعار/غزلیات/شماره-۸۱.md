---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>سرو بستان خجل ز رفتارت</p></div>
<div class="m2"><p>شرم دارد شکر ز گفتارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به چند ای نگار شهرآشوب</p></div>
<div class="m2"><p>می کُشم نفس و می کشم بارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای طبیب دلم چه شد آخر</p></div>
<div class="m2"><p>که نپرسی ز حال بیمارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرمسارم ز مردم دیده</p></div>
<div class="m2"><p>چند جویم به اشک آزارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل وصل تو با دگر یاری</p></div>
<div class="m2"><p>من دلخسته چون خورم خارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به تیغم زنی از آن بازو</p></div>
<div class="m2"><p>شوم از جان فدا دگر بارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک سر مو اگر فروشندت</p></div>
<div class="m2"><p>به جهانی منم خریدارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآن دو زلف و دو چشم پر فتنه</p></div>
<div class="m2"><p>از دل و جان شدم گرفتارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم نگاهی به سوی ما می کن</p></div>
<div class="m2"><p>تا نگهبان بود نگه دارت</p></div></div>