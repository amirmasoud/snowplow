---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>گرم مهمان شوی یک دم چه باشد</p></div>
<div class="m2"><p>ورم یک دم شوی همدم چه باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور از وصلت بیاساید غریبی</p></div>
<div class="m2"><p>ز حسن رویت آخر کم چه باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مجروح بی درمان ما را</p></div>
<div class="m2"><p>گر از وصلش کنی مرهم چه باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روز تلخ هجرانت نگارا</p></div>
<div class="m2"><p>مرا مونس به غیر از غم چه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی بینم به هر عمری وصالت</p></div>
<div class="m2"><p>از این مشکلترم ماتم چه باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم خونست در هجران نگارا</p></div>
<div class="m2"><p>ببین حال دلم دردم چه باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز من پرسی که حالت چیست با درد</p></div>
<div class="m2"><p>بجز هجران تو دردم چه باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان از آب دیده شد چو دریا</p></div>
<div class="m2"><p>گل خیسیده را در نم چه باشد</p></div></div>