---
title: >-
    شمارهٔ ۷۵۶
---
# شمارهٔ ۷۵۶

<div class="b" id="bn1"><div class="m1"><p>جانا گرت به جانب ما اوفتد گذر</p></div>
<div class="m2"><p>بینی ز مهر خود که جهانیست بی خبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حد گذشت شرح غم حال این جهان</p></div>
<div class="m2"><p>بر حال زار من تو کنی رحمتی مگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از روز هجر تو دل تنگم به جان رسید</p></div>
<div class="m2"><p>باشد شبی که با تو کنم دست در کمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تیغ می کشی تو و گر تیر می زنی</p></div>
<div class="m2"><p>جز جان به راه عشق نداریم ما سپر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور و فروغ طلعت زیباش در جهان</p></div>
<div class="m2"><p>ای دل بغایتیست که حیران شود بصر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردیست در دلم ز غم اشتیاق تو</p></div>
<div class="m2"><p>کان درد را دوا نبود غیر گلشکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن گل ز عارض تو و شکّر ز لعل تو</p></div>
<div class="m2"><p>در هم سرشته اند و نهفتند در گهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا چند عجب و ناز و تکبّر کنی مکن</p></div>
<div class="m2"><p>یک دم ز لطف سوی دوستان نگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر یک نظر کنی سوی ما کیمیا شویم</p></div>
<div class="m2"><p>خاک رهیم در تو تمامست یک نظر</p></div></div>