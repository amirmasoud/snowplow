---
title: >-
    شمارهٔ ۳۰۹
---
# شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>درد هجران ز تو نهانم نیست</p></div>
<div class="m2"><p>بیش ازین طاقت و توانم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهرت از دل نمی شود خالی</p></div>
<div class="m2"><p>غیر یاد تو بر زبانم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فراق رخت شبان دراز</p></div>
<div class="m2"><p>جز خیال تو میزبانم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال خود خواهمت که عرضه دهم</p></div>
<div class="m2"><p>محرمی راز آن چنانم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ پربسته ام به کوی مراد</p></div>
<div class="m2"><p>چه کنم میل آشیانم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس بلاها ز دشمنان دارم</p></div>
<div class="m2"><p>هیچ لطفی ز دوستانم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنوازم بتا که هیچ کسی</p></div>
<div class="m2"><p>غیر لطف تو در جهانم نیست</p></div></div>