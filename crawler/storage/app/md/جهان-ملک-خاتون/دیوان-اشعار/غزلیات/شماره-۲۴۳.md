---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>مقصود جهان از دو جهان وصل نگاریست</p></div>
<div class="m2"><p>ورنی به جهانم بجز این کار چه کاریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازآی که روشن شودم دیده به رویت</p></div>
<div class="m2"><p>کز هجر تو بر مردمک دیده غباریست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دوست مپندار که ما را شب هجران</p></div>
<div class="m2"><p>بی روی دلارای تو خوابی و قراریست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی مار میسّر نشود گنج و ز گلزار</p></div>
<div class="m2"><p>دیدی تو گلی تازه که بی صحبت خاریست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین بیش میازار دل خسته ی ما را</p></div>
<div class="m2"><p>ای دوست که آزار دل خسته نه کاریست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منصور انا الحق زد و بر دار زدندش</p></div>
<div class="m2"><p>ای دیده در او بنگر و بنگر که چه داریست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق تو بسیار و هوادار تو بی حد</p></div>
<div class="m2"><p>در زمره ی عشاق، جهان در چه شماریست</p></div></div>