---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>گر مرا به ماه رویت مهر باشد دور نیست</p></div>
<div class="m2"><p>زآنکه کس را در جهان مانند تو منظور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کی صبرم ز وصل خویش فرمایی بگو</p></div>
<div class="m2"><p>بیش از این صبرم ز روی خوب تو مقدور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان روی تو هستند بسیاری ولیک</p></div>
<div class="m2"><p>هیچ کس را حالتی چون حالت منصور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فراقت جان به لب آمد مرا در انتظار</p></div>
<div class="m2"><p>گر بپرسد حال این بیچاره چندان دور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به کی عذر آورد در دادن کام دلم</p></div>
<div class="m2"><p>گر کند تقصیر یارم بعد از این معذور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به دوری گر گرفتارم بگو با این طبیب</p></div>
<div class="m2"><p>رحمتش آخر چرا بر جان این رنجور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نیم تنها به عشق دوست مشهور جهان</p></div>
<div class="m2"><p>در جهان آن کیست کاندر عشق او مشهور نیست</p></div></div>