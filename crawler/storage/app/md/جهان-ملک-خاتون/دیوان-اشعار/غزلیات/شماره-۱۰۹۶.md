---
title: >-
    شمارهٔ ۱۰۹۶
---
# شمارهٔ ۱۰۹۶

<div class="b" id="bn1"><div class="m1"><p>گر نسیم کوی او دارد مسلمانان نسیم</p></div>
<div class="m2"><p>جان همی باید فدا کردن نسیمش را نه سیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزگارم بس مشوّش کرده ای چون زلف خود</p></div>
<div class="m2"><p>بی تو در تن می نخواهم جان شیرین حق علیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بود با دوست دوزخ هست فردوس برین</p></div>
<div class="m2"><p>ورنه بی او کی توانم دید جنّات نعیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وعده ای دادی که کامت می دهم یک شب بده</p></div>
<div class="m2"><p>وعده خود را وفا کن عادتست این از کریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خراج زلف و لعلت ملک هم باشد رواست</p></div>
<div class="m2"><p>با دو زلف همچو جیم و با دهان همچو میم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان خویت عزیز من دگرگون می شود</p></div>
<div class="m2"><p>ای دریغا گر تو را بودی مزاجی مستقیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز غمت در دل نیاید هر زمان در عشق تو</p></div>
<div class="m2"><p>جز خیالت نیست ما را مونس و یار و ندیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهنین پولاد را آه دل من نرم کرد</p></div>
<div class="m2"><p>واین دل چون سنگ او بر ما نمی گردد رحیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سال‌ها بگذشت تا یادم نیارد در جهان</p></div>
<div class="m2"><p>خود نمی گویی که هرگز بنده ای دارم قدیم</p></div></div>