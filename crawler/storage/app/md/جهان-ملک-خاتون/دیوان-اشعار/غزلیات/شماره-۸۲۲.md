---
title: >-
    شمارهٔ ۸۲۲
---
# شمارهٔ ۸۲۲

<div class="b" id="bn1"><div class="m1"><p>چند ز دیده خون دل بر رخ جان چکانمش</p></div>
<div class="m2"><p>چند فغان ز عشق تو تا به فلک رسانمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش اشتیاق تو شعله زند درون جان</p></div>
<div class="m2"><p>هردم از آب دیدگان باز فرونشانمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای تو چو سرو جان ما در چمن جهان خرام</p></div>
<div class="m2"><p>تا سر و جان به پای تو همچو درم فشانمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راست بگو که چون قلم در صفت جمال تو</p></div>
<div class="m2"><p>مردم دیده در جهان چند به سر دوانمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بستد ز دست ما برد به پای غم فکند</p></div>
<div class="m2"><p>هم به توأم امید آن هست که واستانمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدر وصال دوستان چون نشناختم چه سود</p></div>
<div class="m2"><p>گر پس ازین شبی دگر یابم قدر دانمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بو که شبی به دست من دامن وصلش اوفتد</p></div>
<div class="m2"><p>تاز غم جهان مگر یکسره وارهانمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توسن عشق دوست را زین مراد کرده ام</p></div>
<div class="m2"><p>عرصه وصل عرض کن تا به میان چمانمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ندهد درین جهان کام من رمیده دل</p></div>
<div class="m2"><p>روز جزا در آن جهان دست منست و دامنش</p></div></div>