---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>ای باد صبحدم خبر آن نگار چیست</p></div>
<div class="m2"><p>بویت خوش است حال سر زلف یار چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوئیست بس عزیز عجیب حیات بخش</p></div>
<div class="m2"><p>با بوی زلفش عنبر و مشک تتار چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چشم نیم مست تو نرگس چه دسته ایست</p></div>
<div class="m2"><p>با قد دلفریب تو سرو و چنار چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زلف بی قرار پریشان آن نگار</p></div>
<div class="m2"><p>حال دل رمیده این بی قرار چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلهای روز وصل تو در دامن رقیب</p></div>
<div class="m2"><p>بر جان بی دلان غمت نوک خار چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی اگر به سوی غریبان گذر کنی</p></div>
<div class="m2"><p>جز جان نازنین به جهانم نثار چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد بهار و روی دلارام و بوستان</p></div>
<div class="m2"><p>خواهد دلم که بهتر از این در بهار چیست</p></div></div>