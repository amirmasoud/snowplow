---
title: >-
    شمارهٔ ۷۳۸
---
# شمارهٔ ۷۳۸

<div class="b" id="bn1"><div class="m1"><p>نهاد ملک دلم بر غم رخ تو مدار</p></div>
<div class="m2"><p>چو زلف خویش دلم بیش ازین شکسته مدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ما ز سحر دو چشم خوش تو مست شدیم</p></div>
<div class="m2"><p>بیا که از می لعل تو بشکنیم خمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا گرت گذر افتد به کوی یار بگوی</p></div>
<div class="m2"><p>که در فراق تو تا کی کشد دل من بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که بر من بیچاره ات نباشد مهر</p></div>
<div class="m2"><p>منم که بی تو ندارم به هیچ گونه قرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لب رسید مرا جان ز دست هجرانت</p></div>
<div class="m2"><p>مکن جفا که چنین کس نمی کند با یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خاک بر سر راهت فتاده ام جانا</p></div>
<div class="m2"><p>مرا ز خاک مذلّت به لطف خود بردار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نیست طاقت صبرم چو هست درد فراق</p></div>
<div class="m2"><p>غم زمانه ازین بیش بر دلم مگمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خداست مطّلع حال من که در شب و روز</p></div>
<div class="m2"><p>دعای دولتت از جان همی کنم تکرار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مراد من بده ای دوست ورنه می دانم</p></div>
<div class="m2"><p>جزای این بدهد ایزدت به روز شمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شدم به کام دل دشمنان و هجر ای دوست</p></div>
<div class="m2"><p>مرا به کام دل دشمنان چنین مگذار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جان تو که من خسته در فراق رخت</p></div>
<div class="m2"><p>شدم ز جان و جهان و جهانیان بیزار</p></div></div>