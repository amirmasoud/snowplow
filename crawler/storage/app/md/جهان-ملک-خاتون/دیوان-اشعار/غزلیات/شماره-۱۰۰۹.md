---
title: >-
    شمارهٔ ۱۰۰۹
---
# شمارهٔ ۱۰۰۹

<div class="b" id="bn1"><div class="m1"><p>همی پرسی که چونی در فراقم</p></div>
<div class="m2"><p>چه می پرسی ز خورد و خواب، طاقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و هوش او چنان بربود یکسر</p></div>
<div class="m2"><p>به چشم شوخ و ابروهای طاقم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وصلم به رو یکباره در بست</p></div>
<div class="m2"><p>به جان آورد دل از اشتیاقم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به صبرم وعده ای داد آن نگارین</p></div>
<div class="m2"><p>نرفته تلخی آن از مذاقم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردانم ز تو روی از جفا من</p></div>
<div class="m2"><p>مترسان ای عزیز از طمطراقم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من بی خواب بی خور شب همه شب</p></div>
<div class="m2"><p>زحسرت هر دو دیده بر رواقم</p></div></div>