---
title: >-
    شمارهٔ ۱۳۵۸
---
# شمارهٔ ۱۳۵۸

<div class="b" id="bn1"><div class="m1"><p>جهان بگرفت باز از سر جوانی</p></div>
<div class="m2"><p>رسید ایام عیش و کامرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار و نرگس و بید و بنفشه</p></div>
<div class="m2"><p>کنار جوی و روز شادمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکفته ارغوان و سوسن آزاد</p></div>
<div class="m2"><p>چمن سبز و شراب ارغوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنونت ای عزیزا قدر بشناس</p></div>
<div class="m2"><p>مده بر باد غم عمر و جوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رخ اندر چمن همچون گل نو</p></div>
<div class="m2"><p>به قد در باغ سرو بوستانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جان آمد دلم در درد عشقت</p></div>
<div class="m2"><p>مکن زین بیش با ما دلستانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا در سر به جای نور چشمی</p></div>
<div class="m2"><p>مرا در تن تو چون روح و روانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقیب بی خرد چندم دهی پند</p></div>
<div class="m2"><p>تو قدر روز وصل او چه دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تاب هجر جانان مشکن ای دل</p></div>
<div class="m2"><p>که بهر روز وصلش در جهانی</p></div></div>