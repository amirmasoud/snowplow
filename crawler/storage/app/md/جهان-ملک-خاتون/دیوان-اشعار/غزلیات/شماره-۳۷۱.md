---
title: >-
    شمارهٔ ۳۷۱
---
# شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>منم که نقش توأم هرگز از خیال نرفت</p></div>
<div class="m2"><p>دو چشم بختم از آن چهره و جمال نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جان ملول شدم در فراق یار و هنوز</p></div>
<div class="m2"><p>نگار مهوش من از سر ملال نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان رسید مرا دل ز دست هجرانش</p></div>
<div class="m2"><p>به کوی دوست مرا جاده ی وصال نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان مجمع رندان و عاشقان رخش</p></div>
<div class="m2"><p>بجز حکایت آن خط و زلف و خال نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به پیش لعل تو کانست مایه ای ز حیات</p></div>
<div class="m2"><p>حدیث باده و سرچشمه ی زلال نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آنکه موی میانت بدید و قدّ چو سرو</p></div>
<div class="m2"><p>نبود آنکه شب و روز در خیال نرفت</p></div></div>