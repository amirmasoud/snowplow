---
title: >-
    شمارهٔ ۶۹۱
---
# شمارهٔ ۶۹۱

<div class="b" id="bn1"><div class="m1"><p>مگر روزی شب هجران سرآید</p></div>
<div class="m2"><p>درخت وصل جانان در برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر سرو قد دلدارم از در</p></div>
<div class="m2"><p>شبی از روی غمخواری درآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتم ترک عشق او کنم لیک</p></div>
<div class="m2"><p>کنم ترک غمش گر دل برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب هجران چو زلفش تیره رنگست</p></div>
<div class="m2"><p>چو عمر دشمنت هم آخر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مریض عشق جانانم چه باشد</p></div>
<div class="m2"><p>گرم دلبر به پرسش بر سر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز جانی ندارم در وفایش</p></div>
<div class="m2"><p>به پایش افکنم گر دلبر آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان را کس نماند بی خداوند</p></div>
<div class="m2"><p>چو خصمی رفت خصمی دیگر آید</p></div></div>