---
title: >-
    شمارهٔ ۱۲۰۶
---
# شمارهٔ ۱۲۰۶

<div class="b" id="bn1"><div class="m1"><p>جانم به لب رسید ز جور و جفای تو</p></div>
<div class="m2"><p>تا کی کشم ملامت هرکس برای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه می کشم ز جفای تو جورها</p></div>
<div class="m2"><p>از دل به در نمی رود ای جان وفای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانا به خون جان منت گر رضا بود</p></div>
<div class="m2"><p>جان را چه وقع ست مقدّم رضای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به دل که ای دل بیچاره چاره چیست</p></div>
<div class="m2"><p>بالای همچو سرو روان شد بلای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی کشم ملامت و تا کی برم عنا</p></div>
<div class="m2"><p>کردم جهان و جان به سر ماجرای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل رفت از بر من و جان نیز در خطر</p></div>
<div class="m2"><p>وآنگه بگو که می دهدم خونبهای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیگانه شد ز جان و جهان ای صنم چرا</p></div>
<div class="m2"><p>هرکس که شد ز جمله جهان آشنای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر تو کیستی و من ذره چیستم</p></div>
<div class="m2"><p>تو پادشاه هر دو جهان من گدای تو</p></div></div>