---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>آن دل که به زلفین تو پابند نباشد</p></div>
<div class="m2"><p>پیش دل من آنکه خردمند نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دل ببری از من و گر جان بستانی</p></div>
<div class="m2"><p>امری بجز از امر خداوند نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دل که ز درد غم عشق تو خلل یافت</p></div>
<div class="m2"><p>تحقیق که در وی اثر پند نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گم شد دل مسکین من خسته عجب نیست</p></div>
<div class="m2"><p>گر در سر زلفین تو در بند نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سرو سهی هم گذری سوی جهان کن</p></div>
<div class="m2"><p>میلت سوی ما تا کی و تا چند نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او را نتوان گفت که از اهل دلانست</p></div>
<div class="m2"><p>آنکس که دلش باشد و دلبند نباشد</p></div></div>