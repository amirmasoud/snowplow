---
title: >-
    شمارهٔ ۹۳۹
---
# شمارهٔ ۹۳۹

<div class="b" id="bn1"><div class="m1"><p>چرا گشتی چنین غافل ز دردم</p></div>
<div class="m2"><p>نکردی رحمتی بر روی زردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکردی جز جفا بر من نگارا</p></div>
<div class="m2"><p>بگو غیر از وفاداری چه کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندادی کام ما یکدم ز وصلت</p></div>
<div class="m2"><p>بسی خون جگر در عشق خوردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا از دیده و دل حاصلی نیست</p></div>
<div class="m2"><p>به غیر از آب گرم و آه سردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفرما صبر با درد فراقم</p></div>
<div class="m2"><p>چگونه صبر بتوانم ز دردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب تاریک هجرانم بفرسود</p></div>
<div class="m2"><p>صبا درد دلم را بین و دردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان گر در سر عشقت کنم من</p></div>
<div class="m2"><p>به جان تو که از عهدت نگردم</p></div></div>