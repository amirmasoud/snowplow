---
title: >-
    شمارهٔ ۱۲۶۴
---
# شمارهٔ ۱۲۶۴

<div class="b" id="bn1"><div class="m1"><p>ای دل ز شراب عشق مستی</p></div>
<div class="m2"><p>در زلف نگار پای بستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند نظر ز دور کردن</p></div>
<div class="m2"><p>بیزار شدم ز بت پرستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای جان و جهان تو خاطر ما</p></div>
<div class="m2"><p>بسیار به خار جور خستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که چو زلف عهد یاران</p></div>
<div class="m2"><p>بشکستی و زود باز رستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با مدّعیان بد سرانجام</p></div>
<div class="m2"><p>بر رغم من ای پسر نشستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنجور غم فراق گشتم</p></div>
<div class="m2"><p>تا درد مرا دوا فرستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندیش که در جهان چه خواهد</p></div>
<div class="m2"><p>رنجور به غیر تندرستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد و فغان که نیست ما را</p></div>
<div class="m2"><p>با عشق رخ تو نام هستی</p></div></div>