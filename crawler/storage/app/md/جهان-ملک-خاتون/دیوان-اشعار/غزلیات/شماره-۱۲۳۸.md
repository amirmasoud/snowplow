---
title: >-
    شمارهٔ ۱۲۳۸
---
# شمارهٔ ۱۲۳۸

<div class="b" id="bn1"><div class="m1"><p>بنفشه چون سر زلف بتان سر افکنده</p></div>
<div class="m2"><p>به پیش زلف تو نرگس ز جان شده بنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیازمند وصال توأم مرا بنواز</p></div>
<div class="m2"><p>که نیست بنده مسکین به هجرت ارزنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان تو که ازین بیشتر به درد فراق</p></div>
<div class="m2"><p>دلم مکن چو سر زلف خود پراکنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرم شبی بنوازی به وصل جان پرور</p></div>
<div class="m2"><p>لب جهان کنم از وصل دوست پر خنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصال عید و شب شادیست و فصل بهار</p></div>
<div class="m2"><p>به روزگار همایون و بخت فرخنده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دعای جان تو گویم ز جان که بر دو جهان</p></div>
<div class="m2"><p>مدام سایه لطف تو باد پاینده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرآنکه هست ترا دوست در جهان خرّم</p></div>
<div class="m2"><p>حسود جاه تو را هر دو دیده برکنده</p></div></div>