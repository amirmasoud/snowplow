---
title: >-
    شمارهٔ ۱۰۶۹
---
# شمارهٔ ۱۰۶۹

<div class="b" id="bn1"><div class="m1"><p>اگر نه وصل تو باشد جهان نمی خواهم</p></div>
<div class="m2"><p>چه جای هر دو جهانست جان نمی خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه روی تو بینم چه حاصل از دیده</p></div>
<div class="m2"><p>وگر نه ذکر تو گویم زبان نمی خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نه سرو روان قد تو را بینم</p></div>
<div class="m2"><p>به جان دوست که در تن روان نمی خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر قدت ببر آید مرا شبی صنما</p></div>
<div class="m2"><p>به بوستان گل و سرو روان نمی خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صبحدم چو گل وصل چینم از گلزار</p></div>
<div class="m2"><p>صداع ناخوش هر باغبان نمی خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به کوی عشق تو خواهم که رخ نهم بر خاک</p></div>
<div class="m2"><p>ولیک زحمت آن آستان نمی خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توانم آنکه فغان دارم از تو در کویت</p></div>
<div class="m2"><p>ولیک دردسر دوستان نمی خواهم</p></div></div>