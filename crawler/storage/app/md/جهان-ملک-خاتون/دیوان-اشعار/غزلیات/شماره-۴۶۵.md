---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>مسکین دلم به کوی غمت تا گذار کرد</p></div>
<div class="m2"><p>بسیار با خیال رخت کارزار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دیده دید ماه جمال تو هر شبی</p></div>
<div class="m2"><p>از دست جور عشق تو دستم نگار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا با گل رخ تو گرفتست انس دل</p></div>
<div class="m2"><p>بس ناله در فراق رخت چون هزار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حور و قصور بر دل ما عرضه کرده اند</p></div>
<div class="m2"><p>از آن میانه کوی تو را اختیار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شدّت فراق تو ای نور دیده ام</p></div>
<div class="m2"><p>در دیده بین که روی جهان لاله زار کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگشود هیچ کار من از انتظار دوست</p></div>
<div class="m2"><p>چون خاک راه دوست مرا خاکسار کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاف از وفا و عهد تو بسیار می زنم</p></div>
<div class="m2"><p>پیش رقیب باز مرا شرمسار کرد</p></div></div>