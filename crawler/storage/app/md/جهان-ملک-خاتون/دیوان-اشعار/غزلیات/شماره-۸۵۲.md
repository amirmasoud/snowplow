---
title: >-
    شمارهٔ ۸۵۲
---
# شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>من نمی‌خواهم به جز روی تو باغ</p></div>
<div class="m2"><p>با رخت دارم ز بستان‌ها فراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌رخت عالم نمی‌بینم بیا</p></div>
<div class="m2"><p>در دو چشم ما تویی همچون چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ و بستانم تویی اندر جهان</p></div>
<div class="m2"><p>بی‌تو من بس فارغم از باغ و راغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بهار و گل کجا سازد مقام</p></div>
<div class="m2"><p>بلبل شوریده دانی غیر باغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیش ازین صبرم ز جان ممکن نبود</p></div>
<div class="m2"><p>تا به کی بر دل نهیم از عشق داغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل برفت و خارور شد بوستان</p></div>
<div class="m2"><p>جای بلبل بین که چون بگرفت زاغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نغمه دستان کنون خلق جهان</p></div>
<div class="m2"><p>باز نشناسند از بانگ کلاغ</p></div></div>