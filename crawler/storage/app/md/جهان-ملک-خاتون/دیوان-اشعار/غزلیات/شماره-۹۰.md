---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>خوشا بادی که از کوی تو برخاست</p></div>
<div class="m2"><p>کز او بوی سر زلف تو پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سواد رنگ رخسار تو آورد</p></div>
<div class="m2"><p>ز رنگ و بو چمن دیگر بیاراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهی سرو چمن از پای بنشست</p></div>
<div class="m2"><p>چو شمشاد قدش بر پای برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو ماه چارده روی تو را دید</p></div>
<div class="m2"><p>ز رشک روی تو هر لحظه می کاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترحم نیست قطعاً در دل تو</p></div>
<div class="m2"><p>نه دل باشد تو گویی سنگ خاراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نماند در چمن رنگی و بویی</p></div>
<div class="m2"><p>بجز سرو سهی کاو پای برجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگاری چابک عیار دارم</p></div>
<div class="m2"><p>نه رویش خوش که خویش نیز زیباست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتار غم رویت جهانیست</p></div>
<div class="m2"><p>نه در عشقت به عالم بنده تنهاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر گویم که قدّت سرو جانست</p></div>
<div class="m2"><p>به پیشت راست گفتن را که یاراست</p></div></div>