---
title: >-
    شمارهٔ ۵۱۸
---
# شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>فکرم به منتهای جمالت نمی رسد</p></div>
<div class="m2"><p>دست امید من به وصالت نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون سکندر ار به جهان در طلب دوم</p></div>
<div class="m2"><p>جز حسرتم ز آب زلالت نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان می دهم به بوی وصال تو و هنوز</p></div>
<div class="m2"><p>اندیشه ام به خیل خیالت نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فریاد بی دلان ز غمت بر فلک رسید</p></div>
<div class="m2"><p>بر خاطر شریف ملالت نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدّت نهال روضه خلدست و مشکل آن</p></div>
<div class="m2"><p>دست ضعیف دل به نهالت نمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ دلم هوای سر کوی او گرفت</p></div>
<div class="m2"><p>بیچاره گشت و در پر و بالت نمی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اخلاص ما به روی و ریا نیست با رخت</p></div>
<div class="m2"><p>زان روی چشم در خط و خالت نمی رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند ماه نو که به عیدند شاد از او</p></div>
<div class="m2"><p>لیکن به ابروی چو هلالت نمی رسد</p></div></div>