---
title: >-
    شمارهٔ ۱۱۳۱
---
# شمارهٔ ۱۱۳۱

<div class="b" id="bn1"><div class="m1"><p>چو زلف دوست برآشفت روزگار جهان</p></div>
<div class="m2"><p>از آن برفت چنین سر به سر قرار جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه نیست وفا در مزاج او لیکن</p></div>
<div class="m2"><p>کجا کسی که حذر می کند ز کار جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان نکرد وفا با کسی و هم نکند</p></div>
<div class="m2"><p>نهاده اند بدینسان مگر قرار جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بی وفایی او تنگ دل مشو زنهار</p></div>
<div class="m2"><p>چه چاره چون که چنینست کار و بار جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شرح غصّه دوران دهم که بر دل من</p></div>
<div class="m2"><p>هزار بار نشسته ز رهگذار جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان سفله برآورد هم به تیغ جفا</p></div>
<div class="m2"><p>دمار ظلم و تعدّی ز روزگار جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام درد بگویم که از جفا چه نکرد</p></div>
<div class="m2"><p>به حال زار دلم جور بی شمار جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکرد راست ترازوی مهر صرّافش</p></div>
<div class="m2"><p>از آن سبب نگرفتست کس عیار جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر جهان همه گلزار بود از ستمش</p></div>
<div class="m2"><p>نرفت در دل ریشم به غیر خار جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عروس چهره او رنگ و بو بسی دارد</p></div>
<div class="m2"><p>ولی نماند به دست کسی نگار جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نرست شاخ امیدی به گلشن وصلش</p></div>
<div class="m2"><p>نچید یک گل رنگین کسی ز بار جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که خورد جرعه آبی ز چشمه نوشش</p></div>
<div class="m2"><p>که دید سبزه خرّم به مرغزار جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که چید یک گل مهر از درخت قامت او</p></div>
<div class="m2"><p>که عاقبت بشد از جان و سر نثار جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فریب و عشوه دهد او بسی ولی عاقل</p></div>
<div class="m2"><p>کجا نهد دل و جان را به نوبهار جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گرد باغ وصالش بسی بگردیدم</p></div>
<div class="m2"><p>نبوده است بجز خون دل ثمار جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هلاک سروقدان و زوال ماه رخان</p></div>
<div class="m2"><p>جزین چه بود به عالم دگر شکار جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شراب مستی دور زمانه گرچه خوشست</p></div>
<div class="m2"><p>به جان تو که نیرزد دمی خمار جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ببرد اوّل بارم ز دست هوش و خرد</p></div>
<div class="m2"><p>نکرد یک نظر آخر به حال زار جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قرار اگر نبود در جهان جهانبان را</p></div>
<div class="m2"><p>بگو قرار که بودست در کنار جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غم جهان نتوان خورد بیش ازین ای دل</p></div>
<div class="m2"><p>که پیش اهل خرد نیست اعتبار جهان</p></div></div>