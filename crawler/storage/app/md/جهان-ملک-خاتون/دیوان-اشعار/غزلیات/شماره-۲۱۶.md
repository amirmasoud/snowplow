---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>می دهم جانی به عشقش تا مرا جان در تنست</p></div>
<div class="m2"><p>دیده ی جانم خیال روی او را مسکنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده روشن شد مرا تا نکهت زلفت شنید</p></div>
<div class="m2"><p>ای عزیز من مگر بویی از آن پیراهنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی شادند بر وصل دلارایش ولی</p></div>
<div class="m2"><p>چون کنم در عشق او روزی مرا غم خوردنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که لافی زد به عشق یار و سربازی نکرد</p></div>
<div class="m2"><p>مرد نتوان گفت او را بلکه کمتر از زنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوست می دارم به بستان وصل دلداران مدام</p></div>
<div class="m2"><p>صبحدم بر بانگ چنگ این عیب باری در منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنگ روی من چو کاه و جو به جو گشتم به عشق</p></div>
<div class="m2"><p>سعی می دانی چه باشد دُرّ معنی سفتنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به هجرانم کشد ور می نوازد هم به لطف</p></div>
<div class="m2"><p>هست مشهور این حکایت گرد ران با گردنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بخندم خاطر مسکین برنجاند به زجر</p></div>
<div class="m2"><p>هر که جان کردت فدا جای عقوبت کردنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داستان بیژن و گیوار شنیدستی بخوان</p></div>
<div class="m2"><p>گوش بر قول رقیب از کرده به ناکردنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آنکه گرگینش به خصمی در بن چاهی فکند</p></div>
<div class="m2"><p>ای مسلمانان چه گویم این گناه بیژنست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان ز بهر روز وصل یار خواهم در بدن</p></div>
<div class="m2"><p>تا نپنداری که جان در خوردن و در خفتنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زبان داری به ذکر دوست جاری کن مدام</p></div>
<div class="m2"><p>در دهن دانی نه از بهر حکایت گفتنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گاو پرواری خورد آب و علف بس بی قیاس</p></div>
<div class="m2"><p>آدمی را خود هنر کم خوردن و کم گفتنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو بوتیمار تا کی در غم دل مانده ای</p></div>
<div class="m2"><p>ذوق بلبل هیچ دانی در جهان گردیدنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر مرا دشمن به نار ناامیدی دل بسوخت</p></div>
<div class="m2"><p>مشکلم از دوستان بار خجالت بردنست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا به کی ای نفس امّاره مرا در خون دهی</p></div>
<div class="m2"><p>ترک بدبختی بکن چون وقت عذر آوردنست</p></div></div>