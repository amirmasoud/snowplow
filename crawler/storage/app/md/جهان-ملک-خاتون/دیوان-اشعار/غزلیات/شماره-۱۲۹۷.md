---
title: >-
    شمارهٔ ۱۲۹۷
---
# شمارهٔ ۱۲۹۷

<div class="b" id="bn1"><div class="m1"><p>ای دل به درد عشقش دانم دوا نداری</p></div>
<div class="m2"><p>یک دم خیال رویش از خود جدا نداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل به درد هجران تا کی صبور باشم</p></div>
<div class="m2"><p>ای نور هر دو دیده ترس از خدا نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارت جفاست بر من تا کی توان کشیدن</p></div>
<div class="m2"><p>مشکل که یک سر مو در دل وفا نداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این درد با که گویم مسکین دل جفاکش</p></div>
<div class="m2"><p>چون نزد او رسولی بیش از صبا نداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا گویدش که جانم بر لب رسید از غم</p></div>
<div class="m2"><p>زین بیش جور بر من دانم روا نداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من جز وفا گناهی بر خود نمی شناسم</p></div>
<div class="m2"><p>لیکن تو بر ضعیفان غیر از جفا نداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطانی جهان شد بر تو مقرّر ای دل</p></div>
<div class="m2"><p>لیکن چه سود چون تو میل گدا نداری</p></div></div>