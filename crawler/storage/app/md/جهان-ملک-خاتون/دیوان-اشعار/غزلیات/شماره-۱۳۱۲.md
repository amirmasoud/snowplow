---
title: >-
    شمارهٔ ۱۳۱۲
---
# شمارهٔ ۱۳۱۲

<div class="b" id="bn1"><div class="m1"><p>فدای جان منست آن نگار چون حوری</p></div>
<div class="m2"><p>بگو چگونه توان کرد از رخش دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان رسید دل من ز درد روز فراق</p></div>
<div class="m2"><p>از آنکه هست دلم را دوای مهجوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیب درد دلم را دوا نکرد و برفت</p></div>
<div class="m2"><p>که نیست جز شب وصلش دوای رنجوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شهد لب چو کنم نوش می بدادم نیش</p></div>
<div class="m2"><p>چه چاره چون که ترا نیست خوی دل جوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهشت و جنّت و حور و قصور بی رخ تو</p></div>
<div class="m2"><p>چه گونه در نظر آید مرا تو منظوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا هوای بلندست از جهان ما را</p></div>
<div class="m2"><p>ز شاهباز نیاید مزاج عصفوری</p></div></div>