---
title: >-
    شمارهٔ ۵۹۱
---
# شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>تا چند ز هجرت دلم ای یار بنالد</p></div>
<div class="m2"><p>بی روی تو شب تا به سحر زار بنالد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حسرت لعل شکرین تو چو طوطی</p></div>
<div class="m2"><p>در بند قفس گشته گرفتار بنالد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شب به سر کوی تو از درد جدایی</p></div>
<div class="m2"><p>فریادکنان از غم دلدار بنالد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باور نتوان داشت از او لاف محبّت</p></div>
<div class="m2"><p>گر عاشق گل در چمن از خار بنالد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی که قدم رنجه کنی بر سر بیمار</p></div>
<div class="m2"><p>معذور همی دار چو بیمار بنالد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان ز غم عشق تو ای دوست بنالم</p></div>
<div class="m2"><p>کز سوز دل من در و دیوار بنالد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر شب ز غم هجر تو تا صبح بنالم</p></div>
<div class="m2"><p>چون مرغ سحر در غم گلزار بنالد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون کار جهان بی ستمی نیست همی ساز</p></div>
<div class="m2"><p>کان یار نخوانند که از یار بنالد</p></div></div>