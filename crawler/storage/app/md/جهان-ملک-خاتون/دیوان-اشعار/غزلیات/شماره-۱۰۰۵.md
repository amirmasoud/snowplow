---
title: >-
    شمارهٔ ۱۰۰۵
---
# شمارهٔ ۱۰۰۵

<div class="b" id="bn1"><div class="m1"><p>تا چند زنی تیغ جفا بر دل ریشم</p></div>
<div class="m2"><p>زین بیش نماندست مرا طاقت نیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمی بکن و بر من دل خسته ببخشای</p></div>
<div class="m2"><p>وز دل نمک جور از این بیش مریشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را به غم عشق رخت دادم و عمریست</p></div>
<div class="m2"><p>تا از غم دیدار تو بیگانه ز خویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهرم به دلت کم شد و عشقت به دلم بیش</p></div>
<div class="m2"><p>غافل مشو ای دوست چنین از کم و بیشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>استاد غم عشق تو را نیست جز این کار</p></div>
<div class="m2"><p>کاو نقش خیال تو نهادست به پیشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی تو مرا قبله و ابروی تو محراب</p></div>
<div class="m2"><p>اینست همه دینم و آنست همه کیشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملجای جهان نیست بجز درگه لطفت</p></div>
<div class="m2"><p>زنهار به خواری تو مران از در خویشم</p></div></div>