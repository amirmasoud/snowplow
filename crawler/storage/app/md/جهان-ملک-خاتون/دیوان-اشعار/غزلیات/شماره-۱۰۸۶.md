---
title: >-
    شمارهٔ ۱۰۸۶
---
# شمارهٔ ۱۰۸۶

<div class="b" id="bn1"><div class="m1"><p>یاد باد آنکه عزیزان همه با هم بودیم</p></div>
<div class="m2"><p>همه دم با غم و شادی همه همدم بودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیش زنبورصفت بر دل هرکس نزدیم</p></div>
<div class="m2"><p>به وفا عهد سپر کرده و محکم بودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زکات و صدقات و به بناهای بزرگ</p></div>
<div class="m2"><p>شکر معبود که مشهور به عالم بودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سالها در چمن ذوق به بستان طرب</p></div>
<div class="m2"><p>همچو گل خنده زنان خوش دل و خرّم بودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با همه خلق جهان خلق و تواضع کردیم</p></div>
<div class="m2"><p>چون صبا با همه کس یکدل و محرم بودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که خورشیدصفت بر همه کس تافته ایم</p></div>
<div class="m2"><p>در سر مهر جهان دافع شبنم بودیم</p></div></div>