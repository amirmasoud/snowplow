---
title: >-
    شمارهٔ ۱۲۵۸
---
# شمارهٔ ۱۲۵۸

<div class="b" id="bn1"><div class="m1"><p>مرغ دلم به زلف تو تا ساخت خانه‌ای</p></div>
<div class="m2"><p>یکباره کرد از من مسکین کرانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ از برای دانه فتد در کمند شوق</p></div>
<div class="m2"><p>زلفش کمند دل شد و آن خال دانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بستد از دو دستم و در پای غم فکند</p></div>
<div class="m2"><p>می‌خواست گوییا بت بدخو بهانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دوستان فراق بت گلعذار من</p></div>
<div class="m2"><p>کردم ز خون دل به رخ جان نشانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه سنگ بوسدش کف پا گاه شانه زلف</p></div>
<div class="m2"><p>مسکین منم که کمترم از سنگ و شانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصف جمال و قامت او نیست حدّ ما</p></div>
<div class="m2"><p>هست او میان مجمع خوبان یگانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرگس‌صفت دو چشم تو مخمور خوش بود</p></div>
<div class="m2"><p>در پا فتاد عیش چو تیر نشانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آتشی که از رخ خوب تو در دلست</p></div>
<div class="m2"><p>هردم زنند غایت شوقت زبانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشقی که با رخ تو مرا هست در جهان</p></div>
<div class="m2"><p>باشد حدیث خسرو و شیرین فسانه‌ای</p></div></div>