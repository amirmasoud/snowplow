---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>جهانی سر به سر چون نوبهارست</p></div>
<div class="m2"><p>به باغستان جان گلها به بارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین همچون زمرّد سبز گشته</p></div>
<div class="m2"><p>همه صحرا ز گل نقش و نگارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه بستان پر از گلهای رنگین</p></div>
<div class="m2"><p>هزاران بلبل اندر شاخسارست‌</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب جو سر به سر خیری و سوسن</p></div>
<div class="m2"><p>درخت ارغوان بس بیشمارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشق گل میان بوستانها</p></div>
<div class="m2"><p>فغان بلبل و بانگ هزارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا یک دم که با هم خوش برآییم</p></div>
<div class="m2"><p>چو می دانی که عالم در گذارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا از بوستان وصلت ای جان</p></div>
<div class="m2"><p>نصیب خاطر ما جمله خارست</p></div></div>