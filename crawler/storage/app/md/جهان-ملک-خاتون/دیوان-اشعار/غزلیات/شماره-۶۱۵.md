---
title: >-
    شمارهٔ ۶۱۵
---
# شمارهٔ ۶۱۵

<div class="b" id="bn1"><div class="m1"><p>دلم در شست زلفت گشت پابند</p></div>
<div class="m2"><p>شده در بند زلفت نیک خرسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دلخسته در هجرانت گویم</p></div>
<div class="m2"><p>جفا بر عاشق دلداده تا چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره بگشای از زلف زره پوش</p></div>
<div class="m2"><p>ندارم بیش از این ای دوست در بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز صبرم تلخ شد کام دل ریش</p></div>
<div class="m2"><p>بکن درمانم از لبهای چون قند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگارینا مرنجانم از این بیش</p></div>
<div class="m2"><p>ستم بر بنده ی بیچاره مپسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرانم چون ز جانت بنده گشتم</p></div>
<div class="m2"><p>که نتوان رفتن از پیش خداوند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کردم کان جهان بینم به یکبار</p></div>
<div class="m2"><p>درخت دوستی از بیخ برکند</p></div></div>