---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>آزاد سرو بستان از جان شدیم بندت</p></div>
<div class="m2"><p>از چشم زخم دوران یارب مبا گزندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل به بند زلفت بستم ز روی اخلاص</p></div>
<div class="m2"><p>من چون خلاص خواهم ای نازنین ز بندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفت کمند دلها من آهوی گرفتار</p></div>
<div class="m2"><p>سر چون کشم نگویی ای دوست از کمندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حق مگیر ای دل در عالم حقیقت</p></div>
<div class="m2"><p>منصور وار خود را بردار کن ز بندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسکین دل بلاکش دلدار سرو بالا</p></div>
<div class="m2"><p>ننشست آخر الامر تا در بلا فکندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دشمنان توان زیست با دوستان به عزّت</p></div>
<div class="m2"><p>دشمن نه ای ولیکن بر دوست کی نهندت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبر از لب چو نوشت تلخست نیک دانی</p></div>
<div class="m2"><p>تا کی توان صبوری از لعل همچو قندت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کم رسد به رویت از چشم بد گزندی</p></div>
<div class="m2"><p>بر روی همچو آتش خالیست چون سپندت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بد نمی پسندم بر روی دلپذیرت</p></div>
<div class="m2"><p>گر خود دل تو باری بد بر جهان پسندت</p></div></div>