---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>بیش از این سر ز من بی دل و بیهوش متاب</p></div>
<div class="m2"><p>بی دلی را ز ره لطف و کرامت دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب در دیده ی بی خواب خوشم می آید</p></div>
<div class="m2"><p>به خیالی که توان دید خیال تو به خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم دیده ی ما غرقه به آب غم تست</p></div>
<div class="m2"><p>چند گیرد غم عشقت سر مردم در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شبی خیل خیال تو بود مهمانم</p></div>
<div class="m2"><p>رهر دم از دیده شراب آرم و از سینه کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم از روی محّبت که زمانی بنشین</p></div>
<div class="m2"><p>مرو ای نور دو چشمم چو سر زلف به تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل نو دیدم و گفتم که مگر عارض تست</p></div>
<div class="m2"><p>حمل بر آب کند تشنه ی بیچاره سراب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلبرا گر لب لعلت به لب جام نهی</p></div>
<div class="m2"><p>از حیا آب شود پیش لبت لعل مذاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل، رخ خوب تو را دید و فرو ریخت ز شرم</p></div>
<div class="m2"><p>راست ماننده ی کتّان ز فروغ مهتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم جادوی تو دیدم دلم از دست برفت</p></div>
<div class="m2"><p>آه از آن نرگس مستت که جهان کرد خراب</p></div></div>