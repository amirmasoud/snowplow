---
title: >-
    شمارهٔ ۷۴۹
---
# شمارهٔ ۷۴۹

<div class="b" id="bn1"><div class="m1"><p>تا چند به ما جفا کند یار</p></div>
<div class="m2"><p>وز خویشتنم جدا کند یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان در سر کار عشق کردیم</p></div>
<div class="m2"><p>گفتم که مگر وفا کند یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وین خسته دل حزین ما را</p></div>
<div class="m2"><p>از وصل شبی دوا کند یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز لعل لب شکر فروشش</p></div>
<div class="m2"><p>کام دل ما روا کند یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که مگر چو سرو بستان</p></div>
<div class="m2"><p>از جان همه میل ما کند یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ترک خطا بریخت خونم</p></div>
<div class="m2"><p>تا چند چنین خطا کند یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امّید من شکسته خاطر</p></div>
<div class="m2"><p>از وصل چرا هبا کند یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آخر ز چه روی بی گناهی</p></div>
<div class="m2"><p>بر ما ستم و جفا کند یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندین ستم و جفا نگویید</p></div>
<div class="m2"><p>بر جان جهان چرا کند یار</p></div></div>