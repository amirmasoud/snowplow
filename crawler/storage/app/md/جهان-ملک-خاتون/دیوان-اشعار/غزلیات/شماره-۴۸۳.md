---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>ناتوان چشم تو تا میل به مخموری کرد</p></div>
<div class="m2"><p>دل سرگشته ی من باز ز تن دوری کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خجل گشته ز روی تو زبان طبعم</p></div>
<div class="m2"><p>که چرا نسبت رویت به گل سوری کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سر زلف تو چین یافت خطا کرد بسی</p></div>
<div class="m2"><p>که به ملک دل من دعوی فغفوری کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دل من شود از شهد لبش شیرین کام</p></div>
<div class="m2"><p>مدّتی بر در بستان تو زنبوری کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا مگر درد دل او به سلیمان برد</p></div>
<div class="m2"><p>سالها در کف پای غم تو موری کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش نرگس مست تو چرا رنجورست</p></div>
<div class="m2"><p>گفت مخمور بسی روی به رنجوری کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت آخر ز چه گشتی تو چنین زار و نزار</p></div>
<div class="m2"><p>گفتم ای جان و جهان بر همه مهجوری کرد</p></div></div>