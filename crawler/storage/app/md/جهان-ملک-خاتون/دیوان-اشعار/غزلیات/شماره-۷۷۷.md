---
title: >-
    شمارهٔ ۷۷۷
---
# شمارهٔ ۷۷۷

<div class="b" id="bn1"><div class="m1"><p>دلم از درد فراق تو به جان آمد باز</p></div>
<div class="m2"><p>جانم از شوق وصالت به فغان آمد باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژده ی وصل تو ناگه به جهان دردادند</p></div>
<div class="m2"><p>جان پژمرده ز مهرت به جهان آمد باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال دلدار دلم خواست که معلوم کند</p></div>
<div class="m2"><p>آشکارا ز برم رفت و نهان آمد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ای دل چو برفتی ز برم حال بگوی</p></div>
<div class="m2"><p>گفت خاموش که آن روح و روان آمد باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژده سوی چمن و آب روان باید برد</p></div>
<div class="m2"><p>که دگرباره ز در سرو روان آمد باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان به شکرانه بدادیم چو دیدیم به چشم</p></div>
<div class="m2"><p>سرو بستان ملاحت که چمان آمد باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر معبود که طاووس دل محزونم</p></div>
<div class="m2"><p>از در گلشن جان جلوه کنان آمد باز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل من لعل لبش دید و به تحسین می گفت</p></div>
<div class="m2"><p>لعل جان بخش نگر جان که به کان آمد باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفته بودی که تو روزی ز غمم باز آیی</p></div>
<div class="m2"><p>از غم روی تو ای جان بتوان آمد باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه بودی به تو دلشاد و تو بروی دلشاد</p></div>
<div class="m2"><p>در کناری بد و آخر به میان آمد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای جهان گرچه شدی پیر ز ایام فراق</p></div>
<div class="m2"><p>مونس جان و دل پیر و جوان آمد باز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو لاله دلم از درد فراقت می سوخت</p></div>
<div class="m2"><p>دیده سوی تو چو نرگس نگران آمد باز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلبل جان من از خار فراقت شده لال</p></div>
<div class="m2"><p>از گل روی تو آخر به زبان آمد باز</p></div></div>