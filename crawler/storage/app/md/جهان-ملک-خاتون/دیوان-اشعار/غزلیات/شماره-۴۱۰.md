---
title: >-
    شمارهٔ ۴۱۰
---
# شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>مرا به صبحدمی در چمن گذار افتاد</p></div>
<div class="m2"><p>ز بوی گل به مشامم خیال یار افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت یک دو سه بیتی به خاطرم به هوس</p></div>
<div class="m2"><p>چو از هوا نظرم سوی آن نگار افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه کردم و دیدم گرفته آشوبی</p></div>
<div class="m2"><p>چنانکه چرخ ازان ناله بیقرار افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سؤال کردم و گفتم چه غلغلست به باغ</p></div>
<div class="m2"><p>هزار ناله و فریاد در هزار افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جواب داد که سروی درآمد از در باغ</p></div>
<div class="m2"><p>ز رشک قامت او لرزه بر چنار افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل از خجالت رویش میان صحن چمن</p></div>
<div class="m2"><p>بریخت دردم و در دست و پای خار افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنفشه چون سر زلفش بدید در خم شد</p></div>
<div class="m2"><p>ز رشک و در قدم او به ره گذار افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوفه و گل سوری و سوسن آزاد</p></div>
<div class="m2"><p>به اسم بندگیت جمله در شمار افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوای زلف و رخت کرد بلبل دل من</p></div>
<div class="m2"><p>گلی نچیده ز غم در دهان خار افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگرچه نیست تو را صبر در فراق رخش</p></div>
<div class="m2"><p>تحمّلی بکن ای دل که باز کار افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراق روی تو کردست حال زار مرا</p></div>
<div class="m2"><p>میان ما و غمت باز کارزار افتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی فراق بیفتد میان دلداران</p></div>
<div class="m2"><p>میان ما و تو ای دوست چند بار افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی نبود چنین هیچ بار بر دل من</p></div>
<div class="m2"><p>بیا که بی تو جهانی ز اعتبار افتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو بخت یار نبودم جدا شدی ز برم</p></div>
<div class="m2"><p>تو را فراق من ای جان به اختیار افتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگرچه سکّه رویت به قلب دل زده اند</p></div>
<div class="m2"><p>به دار ضرب وصال تو کم عیار افتاد</p></div></div>