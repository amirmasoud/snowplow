---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>آتشی کز غم هجران تو بر جان منست</p></div>
<div class="m2"><p>ز آن شرر در دو جهان ناله و افغان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردم ار هست ز هجر تو نگارا دانم</p></div>
<div class="m2"><p>لب جان بخش بتم مایه ی درمان منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بدیدم سر زلفین تو گفتم ای دل</p></div>
<div class="m2"><p>حال او بین بتر از حال پریشان منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاره صبرست مرا در غم هجران چه کنم</p></div>
<div class="m2"><p>دل سرگشته خدا را نه به فرمان منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش همچون مه ده چار برآمد بر بام</p></div>
<div class="m2"><p>گفتم ای دیده ببین آن رخ جانان منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم از عید رخت چند بعیدم داری</p></div>
<div class="m2"><p>گفت این لاشه بسی عید که قربان منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش تا به کیم در غم هجران داری</p></div>
<div class="m2"><p>گفت بسیار کسی بی سر و سامان منست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش هم نظری کن تو بر احوال جهان</p></div>
<div class="m2"><p>گفت در کوی غمم او ز گدایان منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جهان داریت ار نیست ملالی صنما</p></div>
<div class="m2"><p>چه شود گر تو بگویی که جهان زان منست</p></div></div>