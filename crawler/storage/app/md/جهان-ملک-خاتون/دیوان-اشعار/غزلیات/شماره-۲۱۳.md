---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>آن روی نگویند مگر صورت جانست</p></div>
<div class="m2"><p>و آن چشم نگویند مگر قوت روانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و آن زلف نباشد مگر آن عنبر سار است</p></div>
<div class="m2"><p>و آن لب نتوان گفت که از دیده نهانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشبیه به مه روی تو را چون بتوان کرد</p></div>
<div class="m2"><p>و آن قد دلارای مگر سرو روانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنی تو سراسر که همه مایه ی لطفی</p></div>
<div class="m2"><p>در جان من این آتش عشق تو از آنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمریست که در حسرت یک لحظه وصالت</p></div>
<div class="m2"><p>خون دلم از دیده ی غمدیده روانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ترک غم عشق تو ای دوست بگویم</p></div>
<div class="m2"><p>تا جان بودم در تن و تا سعی و توانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشکل همه اینست که آن دلبر بی مهر</p></div>
<div class="m2"><p>با ما به زبانست و دلش با دگرانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دوست چو بوسیدن پایت نتوانم</p></div>
<div class="m2"><p>از دور دعای منت آخر چه زیانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بوی سر زلفین توأم مایه روحست</p></div>
<div class="m2"><p>لعل لب شیرین توأم قوّت جانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل غم ایام بگو چند توان خورد</p></div>
<div class="m2"><p>بسیار مخور غصّه که عالم گذرانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نومید نشاید که کنی بنده ی خود را</p></div>
<div class="m2"><p>بر لطف تو امّید مرا هر دو جهانست</p></div></div>