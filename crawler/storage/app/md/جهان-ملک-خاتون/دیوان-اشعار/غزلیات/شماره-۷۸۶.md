---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>صبا مرا به جهان نیست جز تو محرم راز</p></div>
<div class="m2"><p>مرا به روی نگارین خویش هست نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو حال زار من خسته دل تو می دانی</p></div>
<div class="m2"><p>که جان رسید مرا بر لب از فراقش باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گوش او نرسیده حکایت دردم</p></div>
<div class="m2"><p>که مونس دل ما بود یار بنده نواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر ز روی ترحّم نظر کند بر ما</p></div>
<div class="m2"><p>درآید از در بختم شبی چو سروی ناز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دست طعنه ی دشمن به جان رسید دلم</p></div>
<div class="m2"><p>که همچو شمع زبانش بریده باد به گاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم ز شدّت هجر رخ تو بیچاره</p></div>
<div class="m2"><p>ز لطف چاره ی کار من غریب بساز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوای کوی غمت بس بلند افتادست</p></div>
<div class="m2"><p>کبوتریست دلم چون کند درو پرواز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخم نمود به اسب جفا بزد فرزین</p></div>
<div class="m2"><p>درین میانه ببین مات می شود سرباز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرچه می رسدت ناز ای سهی سروم</p></div>
<div class="m2"><p>دگر به بخت جهان ناز کرده ای آغاز</p></div></div>