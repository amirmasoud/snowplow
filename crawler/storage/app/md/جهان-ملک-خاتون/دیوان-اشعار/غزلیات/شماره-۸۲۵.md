---
title: >-
    شمارهٔ ۸۲۵
---
# شمارهٔ ۸۲۵

<div class="b" id="bn1"><div class="m1"><p>مراست درد فراقی که نیست درمانش</p></div>
<div class="m2"><p>شبیست شدّت هجران که نیست پایانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا سریست و فدای تو کرده ام چه کنم</p></div>
<div class="m2"><p>که از بلای فراق تو نیست سامانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عظیم دور فتادست کعبه ی مقصود</p></div>
<div class="m2"><p>که نیست در همه عالم حد بیابانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببین که مشکل ما حل نمی شود باری</p></div>
<div class="m2"><p>از آن سبب شب هجران ما شد آسانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم ز دست غم و درد تو به جان آمد</p></div>
<div class="m2"><p>بگو که با که بگوییم درد پنهانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب درد دلم را دوا نمی سازد</p></div>
<div class="m2"><p>چرا که نیست امیدی چنان بدین جانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر به جان رسدت دست ای جهان زنهار</p></div>
<div class="m2"><p>مکن دریغ و به جانان خود برافشانش</p></div></div>