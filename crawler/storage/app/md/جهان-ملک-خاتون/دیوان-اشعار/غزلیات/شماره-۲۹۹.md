---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>به درد ما بجز از وصل تو دوا خوش نیست</p></div>
<div class="m2"><p>بیا که جانی و جانم ز تن جدا خوش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغ ماه رخت را اگر وفا بودی</p></div>
<div class="m2"><p>چرا که دلبر مه روی بی وفا خوش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان رسید دل من ز جور هجرانت</p></div>
<div class="m2"><p>جفا مکن صنما کاین همه جفا خوش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جفا اگر چه ز خوبان طریقه ایست قدیم</p></div>
<div class="m2"><p>نگار من مکن آن را که گوییا خوش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنند جاهل و نادان جفا به خلق ولی</p></div>
<div class="m2"><p>ستم ز پادشه لطف بر گدا خوش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه باغ و بهارست و سبزه خرّم</p></div>
<div class="m2"><p>به جان دوست که این جمله بی شما خوش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دیده گرچه شدی دور در دو دیده من</p></div>
<div class="m2"><p>به غیر خاک کف پات توتیا خوش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر که نیست خوشی در بهار و طوف چمن</p></div>
<div class="m2"><p>اگر خوش است خدا را مرا چرا خوش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا که بی تو جهان ناخوش است بر دل من</p></div>
<div class="m2"><p>اگر خوش است ترا بی جهان مرا خوش نیست</p></div></div>