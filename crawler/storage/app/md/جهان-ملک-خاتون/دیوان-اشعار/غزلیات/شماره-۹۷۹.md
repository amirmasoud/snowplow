---
title: >-
    شمارهٔ ۹۷۹
---
# شمارهٔ ۹۷۹

<div class="b" id="bn1"><div class="m1"><p>منم کاندر جهان یاری ندارم</p></div>
<div class="m2"><p>بجز هجران تو کاری ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم هجران مرا یاریست محرم</p></div>
<div class="m2"><p>که غیر از لطف او یاری ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوی او سگان را هست باری</p></div>
<div class="m2"><p>من دلسوخته باری ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمت چون کوه و مسکین تن چو کاهست</p></div>
<div class="m2"><p>ولی مشکل که غمخواری ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا با عشق تو رازیست پنهان</p></div>
<div class="m2"><p>که با نامحرمان کاری ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم گم گشت در کویت از آن روی</p></div>
<div class="m2"><p>شدم بی دل که دلداری ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان حالت چرا زین سان خرابست</p></div>
<div class="m2"><p>چنین باشد جهانداری ندارم</p></div></div>