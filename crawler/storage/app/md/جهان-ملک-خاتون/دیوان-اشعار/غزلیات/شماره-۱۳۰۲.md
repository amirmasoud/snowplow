---
title: >-
    شمارهٔ ۱۳۰۲
---
# شمارهٔ ۱۳۰۲

<div class="b" id="bn1"><div class="m1"><p>که دارد در دو عالم چون تو یاری</p></div>
<div class="m2"><p>به قد سرو سهی رخ چون نگاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وفا در دل نداری یک سر موی</p></div>
<div class="m2"><p>به میدان جفا چابک سواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم فرسود در بند فراقش</p></div>
<div class="m2"><p>ندارم بر وصالش اختیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مسکین ما از پا درآمد</p></div>
<div class="m2"><p>نمی افتد به دست ما نگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه بار دارم بر دل از تو</p></div>
<div class="m2"><p>بجز عشقت ندارم هیچ کاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چشمت مستم ای دلدار و دارم</p></div>
<div class="m2"><p>ز لعل دلکشت در سر خماری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشستم بر سر خاک رهت خوش</p></div>
<div class="m2"><p>چو سرو ناز کن بر ما گذاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جان آمد دلم از بردباری</p></div>
<div class="m2"><p>نظر سوی جهان انداز باری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظر بر من نکرد آن سرو آزاد</p></div>
<div class="m2"><p>جهان را نیست پیشش اعتباری</p></div></div>