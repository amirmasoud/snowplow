---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>گفتم به چمن قامت آن سرو روانست</p></div>
<div class="m2"><p>گفتا که روانش نتوان گفت که جانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار مپندار که در شدّت هجران</p></div>
<div class="m2"><p>یک لحظه مرا بی رخ تو صبر و توانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک من دلداده به باد غم او شد</p></div>
<div class="m2"><p>کاتش به دل ما ز لب دوست نهانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرتاسر عالم همه اینست چو دیدم</p></div>
<div class="m2"><p>وآن شوخ جفا جوی همانست همانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر غم هجرش بگذشت از سپر جان</p></div>
<div class="m2"><p>در عهد بسی سُست ولی سخت کمانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتیم گدای سر کویش به حقیقت</p></div>
<div class="m2"><p>زان روی که او پادشه هر دو جهانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما جان به غم عشق سپردیم ولیکن</p></div>
<div class="m2"><p>جانا چه توان کرد دلت با دگرانست</p></div></div>