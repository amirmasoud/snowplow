---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>آن بنده که جز تو کس ندارد</p></div>
<div class="m2"><p>جز بندگیت هوس ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنجور فراقت ای دلارام</p></div>
<div class="m2"><p>جز یک نفس از نفس ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه تو شاه باز عشقست</p></div>
<div class="m2"><p>اندیشه ز خرمگس ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن بلبل دل که پای بندست</p></div>
<div class="m2"><p>چون بانگ در این قفس ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیچاره کسی که غرق دریاست</p></div>
<div class="m2"><p>کاو راه ز پیش و پس ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فریاد غم دل جهان رس</p></div>
<div class="m2"><p>کاین بنده بجز تو کس ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسکین دل من محیط عشقست</p></div>
<div class="m2"><p>واندیشه ز بار خس ندارد</p></div></div>