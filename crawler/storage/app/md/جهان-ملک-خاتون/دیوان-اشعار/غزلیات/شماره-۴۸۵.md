---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>دلبر به هر چه گفت به قولش وفا نکرد</p></div>
<div class="m2"><p>با این دل رمیده به غیر از جفا نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره دل به درد غمش شد اسیر و او</p></div>
<div class="m2"><p>از لطف خویش درد دلم را دوا نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهدی ببست با من بیچاره پیش ازین</p></div>
<div class="m2"><p>دل برد آن نگار و به عهدش وفا نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل برد و تن به دست بلای فراق داد</p></div>
<div class="m2"><p>آن بی حفاظ با من مسکین چه ها نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم به خاک کوی وفایش نشسته ام</p></div>
<div class="m2"><p>بگذشت آن نگار و نظر بر گدا نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتا مراد تو بدهم تنگ دل مشو</p></div>
<div class="m2"><p>لیکن مرادم از لب لعلش روا نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه جز جفا ننمودی به حال من</p></div>
<div class="m2"><p>دانی که در جهانی چو جهان کس وفا نکرد</p></div></div>