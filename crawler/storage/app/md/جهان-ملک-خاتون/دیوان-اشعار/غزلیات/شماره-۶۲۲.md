---
title: >-
    شمارهٔ ۶۲۲
---
# شمارهٔ ۶۲۲

<div class="b" id="bn1"><div class="m1"><p>تا چند با دل من مسکین جفا کند</p></div>
<div class="m2"><p>آن بی وفا نگار به ترک وفا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست او طبیب این دل محزون ناتوان</p></div>
<div class="m2"><p>واجب کند که درد دلم را دوا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کامم ز شربت شب هجران شدست تلخ</p></div>
<div class="m2"><p>کامم چه باشد ار ز لب خود دوا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او پادشاه حسن و ملاحت از آن شدست</p></div>
<div class="m2"><p>تا گوش و هوش و دیده به سوی گدا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سرو نازنین چه شود در میان باغ</p></div>
<div class="m2"><p>گر پشت بر جفای خود و رو به ما کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل برد از دو دستم و در خون جان شدم</p></div>
<div class="m2"><p>با دوستان بپرس چرا ماجرا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حال من غریب که گوید به پیش دوست</p></div>
<div class="m2"><p>آری مگر که باد صبا این ادا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با دلبرم بگوی که بیگانه خو چراست</p></div>
<div class="m2"><p>وقتست کاو نظر به سوی آشنا کند</p></div></div>