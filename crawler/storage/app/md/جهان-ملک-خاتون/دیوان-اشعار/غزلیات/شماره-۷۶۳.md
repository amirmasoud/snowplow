---
title: >-
    شمارهٔ ۷۶۳
---
# شمارهٔ ۷۶۳

<div class="b" id="bn1"><div class="m1"><p>چون زلف سودایی شدم بر روی بغدادی پسر</p></div>
<div class="m2"><p>آشفته و سرگشته ام در کوی بغدادی پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بوی خط و عارضش جان را کنم ایثار اگر</p></div>
<div class="m2"><p>آرد نسیمی صبحدم از سوی بغدادی پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو چون برگ سمن قد تو چون سرو چمن</p></div>
<div class="m2"><p>محراب هر دو چشم من ابروی بغدادی پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بویی که آرد صبحدم از زلف یارم دم به دم</p></div>
<div class="m2"><p>صد جامه ی جان بر درم بر بوی بغدادی پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدخویی دارم چو خور کز وی شود خیره بصر</p></div>
<div class="m2"><p>از خود ندارم من خبر از خوی بغدادی پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر روی چون ماه چگل بر قامت رعنا مهل</p></div>
<div class="m2"><p>کاشفته گردد همچو دل گیسوی بغدادی پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارم همه کام جهان دانی چه خواهم این زمان</p></div>
<div class="m2"><p>دارد دلم بس آرزو بر روی بغدادی پسر</p></div></div>