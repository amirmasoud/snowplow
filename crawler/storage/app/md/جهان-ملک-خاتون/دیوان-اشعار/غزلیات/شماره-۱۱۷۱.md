---
title: >-
    شمارهٔ ۱۱۷۱
---
# شمارهٔ ۱۱۷۱

<div class="b" id="bn1"><div class="m1"><p>زین بیشتر چون زلف خود خاطر پریشانم مکن</p></div>
<div class="m2"><p>واندر فراق ای عمر من شیدا و حیرانم مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردی ز تو دارم ولی درمان نمی یابم چرا</p></div>
<div class="m2"><p>آخر که گفتت درد ده وز لطف درمانم مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست هجران جان من آمد به لب از وصل خود</p></div>
<div class="m2"><p>دادم بده زین بیشتر بیداد بر جانم مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیمان تو با من داده ای در عهد حسن خویشتن</p></div>
<div class="m2"><p>ای نور دیده رخنه ای در عهد و پیمانم مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ذرّه ناچیز و تو سلطان انجم خود تویی</p></div>
<div class="m2"><p>خورشید وارم رخ نما چون ابر گریانم مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند من مستغرقم از آب چشم خویشتن</p></div>
<div class="m2"><p>بر آتش هجران خود زین بیش بریانم مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای جان و ای جانان من فرماندهی بر جان من</p></div>
<div class="m2"><p>هر جور می خواهی بکن در بند هجرانم مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون از رخ جان پرورت هستم بعید اندر جهان</p></div>
<div class="m2"><p>من لاشه ی بی حاصلم در عید قربانم مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تاب چوگان دو زلف اندر سر میدان عشق</p></div>
<div class="m2"><p>مانند گوی اندر غمت افتان و خیزانم مکن</p></div></div>