---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>اگر دمی ز تو بویی به من رساند باد</p></div>
<div class="m2"><p>هزار جان و جهانم فدای آن دم باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی که یاد من خسته سالها نکنی</p></div>
<div class="m2"><p>منم که با غم رویت نشسته ام دلشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شد چرا چه سبب حال من نمی پرسی</p></div>
<div class="m2"><p>که هر دمم به فلک می رسد ز غم فریاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از ملامت دشمن به عشق نگریزم</p></div>
<div class="m2"><p>بیا که دل به تو دادیم و هرچه باداباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا سریست ز عهدت به باد خواهد شد</p></div>
<div class="m2"><p>که در طریقت عشقست عهد بر بنیاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بخت یار نباشد ستیز نتوان کرد</p></div>
<div class="m2"><p>دلا ز کار جهان همچو سرو باش آزاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رقیب بی خرد آخر نصیحتم کم کن</p></div>
<div class="m2"><p>چرا که با تو چنین حادثه بسی افتاد</p></div></div>