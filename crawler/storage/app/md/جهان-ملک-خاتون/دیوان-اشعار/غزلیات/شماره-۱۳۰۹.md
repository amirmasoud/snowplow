---
title: >-
    شمارهٔ ۱۳۰۹
---
# شمارهٔ ۱۳۰۹

<div class="b" id="bn1"><div class="m1"><p>چه شود گر فکند بر من مسکین نظری</p></div>
<div class="m2"><p>یا بپرسد ز دل سوخته خرمن خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز من تیره شد از جور فراقت صنما</p></div>
<div class="m2"><p>شب هجر تو همانا که ندارد سحری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر توانی که بجویی دلم امروز بجوی</p></div>
<div class="m2"><p>ورنه بسیار بجویی و نیابی اثری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قیامت ز لحد نعره زنان برخیزم</p></div>
<div class="m2"><p>چو سر خاک من ای دوست گذاری گذری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به جان من بی دل دگری هست تو را</p></div>
<div class="m2"><p>من بیچاره به جای تو ندارم دگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان ز من خواسته بودی صنما شرمت باد</p></div>
<div class="m2"><p>چون فرستم بر جانانه چنین مختصری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جهان ماه ندیدم که نهادست کلاه</p></div>
<div class="m2"><p>سرو هرگز نشنیدم که ببندد کمری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدمی بر سر بیمار نه ای جان و جهان</p></div>
<div class="m2"><p>تا به هر گام به پای تو فشانیم سری</p></div></div>