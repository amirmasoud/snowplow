---
title: >-
    شمارهٔ ۱۱۱۶
---
# شمارهٔ ۱۱۱۶

<div class="b" id="bn1"><div class="m1"><p>به خدایی که جز او نیست خداوند جهان</p></div>
<div class="m2"><p>که مرا عشق تو شد در همه دم همدم جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سحر بهر وصالت به دعا می گویم</p></div>
<div class="m2"><p>که الهی تو مرا زود به مقصود رسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدمی نیست بجز غصّه مرا روز فراق</p></div>
<div class="m2"><p>چاره ای نیست بجز ناله و زاری و فغان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار دیگر ز خدا دولت وصلت خواهم</p></div>
<div class="m2"><p>می دهم جان به امید ار دهدم عمر امان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روانم قد او بود روان شد ز برم</p></div>
<div class="m2"><p>جان پژمرده روان شد ز پی سرور روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی رسد کام از آن لب به دل خسته ی من</p></div>
<div class="m2"><p>که رسانید فراق تو مرا جان به لبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وصالت که دمی با من بی دل بنشین</p></div>
<div class="m2"><p>بیش ازینم به سر آتش هجران منشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قوّت جان منی دور مباش از بر من</p></div>
<div class="m2"><p>نور چشمی مشو از دیده غمدیده نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه یادم بشد از یاد تو ای یار عزیز</p></div>
<div class="m2"><p>یکدم از یاد تو غافل نبود جان جهان</p></div></div>