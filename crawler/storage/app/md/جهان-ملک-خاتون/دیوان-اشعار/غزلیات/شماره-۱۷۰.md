---
title: >-
    شمارهٔ ۱۷۰
---
# شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>از زلف خودم بویی بر دست صبا بفرست</p></div>
<div class="m2"><p>کشتی به جفا ما را بویی ز وفا بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روز و شبم ساکن در کوی غمت جانا</p></div>
<div class="m2"><p>از کوی وصال خود هم بخش گدا بفرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانا چو زکات حسن بر مستحقست واجب</p></div>
<div class="m2"><p>من مستحقم باری بی روی و ریا بفرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به وصال خود دریاب دمی ما را</p></div>
<div class="m2"><p>گفتا که جهان و جان زودم به نوا بفرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنجور غم عشقم هستی تو طبیب دل</p></div>
<div class="m2"><p>در درد گرفتارم دریاب و دوا بفرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خیل خیال تو آورد شبیخونی</p></div>
<div class="m2"><p>بردند به یغما جان از وصل صفا بفرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتا به جهان ما را سودای کسی در سر</p></div>
<div class="m2"><p>چون نیست برو عاشق از دور دعا بفرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم اگرم باشد رخصت که دهم جان را</p></div>
<div class="m2"><p>در پای تو گفتا نه بنشین تو ثنا بفرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل هوس وصلش داری نشود ممکن</p></div>
<div class="m2"><p>مرغ دل سرگردان از روی هوا بفرست</p></div></div>