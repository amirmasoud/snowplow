---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای خجل از شرم رویت آفتاب</p></div>
<div class="m2"><p>وی ز تاب هجر تو دلها کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش دل را دوا می خواستم</p></div>
<div class="m2"><p>از طبیب و صبر فرمودم جواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر فرمودی مرا در عاشقی</p></div>
<div class="m2"><p>عشق مشکل صبر گیرد در حساب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غمت هر چند زاری کرده ام</p></div>
<div class="m2"><p>زان دهن نشنیده ام هرگز جواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون میسّر نیست وصلت دلبرا</p></div>
<div class="m2"><p>دولتی باشد گرت بینم به خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان ملجاء من درگاه تست</p></div>
<div class="m2"><p>بیش ازین روی از من مسکین متاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیر گشت از غصّه دوران دلم</p></div>
<div class="m2"><p>یاد باد آن دولت وقت شباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون جهان را جز تو دارایی نبود</p></div>
<div class="m2"><p>از چه رو کردی جهان یک سر خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رحم کن بر من که در ایام حسن</p></div>
<div class="m2"><p>رحم بر بیچارگان باشد ثواب</p></div></div>