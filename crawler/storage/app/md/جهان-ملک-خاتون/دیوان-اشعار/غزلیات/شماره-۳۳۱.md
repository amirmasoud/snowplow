---
title: >-
    شمارهٔ ۳۳۱
---
# شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>مرا در عشق جز درد دلی نیست</p></div>
<div class="m2"><p>چو از وصل نگارم حاصلی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر پیش تو آسانست جانا</p></div>
<div class="m2"><p>مرا چون روز هجران مشکلی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز مهرت نمی ورزیم کاری</p></div>
<div class="m2"><p>بجز کوی تو ما را منزلی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گویند دل دادی تو بر باد</p></div>
<div class="m2"><p>فدا بادا تو را غیر از دلی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجوی از ورطه ی عشقش خلاصی</p></div>
<div class="m2"><p>که بحر عشق او را ساحلی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم دیوانه از زنجیر زلفش</p></div>
<div class="m2"><p>وزین سودا به عالم عاقلی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبا از من پیامی نزد دلدار</p></div>
<div class="m2"><p>رسان چون جز تو ما را حاملی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل از من بستد و در دیگری بست</p></div>
<div class="m2"><p>چو می بینم چو شخصم غافلی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو دلبر در جهان جان جهانی</p></div>
<div class="m2"><p>جهان را بی تو خود جان و دلی نیست</p></div></div>