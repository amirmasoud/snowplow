---
title: >-
    شمارهٔ ۱۲۷۰
---
# شمارهٔ ۱۲۷۰

<div class="b" id="bn1"><div class="m1"><p>گفتم ز جور دوست بگویم حکایتی</p></div>
<div class="m2"><p>نگذاردم وفا که نویسم شکایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردم نمی رسد ز فراقش به آخری</p></div>
<div class="m2"><p>شوقم چه جور دوست ندارد نهایتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ملک دل که بود خراب از جفای چرخ</p></div>
<div class="m2"><p>سلطان عشق باز برافراشت رایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من منتظر نشسته چه باشد که بنگری</p></div>
<div class="m2"><p>در حال این شکسته به چشم عنایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانم به لب رسید ز دست جفای تو</p></div>
<div class="m2"><p>وقتست اگر کنی دل ما را رعایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار جهان خراب شد از جور روزگار</p></div>
<div class="m2"><p>معلوم کرده ای و نکردی حمایتی</p></div></div>