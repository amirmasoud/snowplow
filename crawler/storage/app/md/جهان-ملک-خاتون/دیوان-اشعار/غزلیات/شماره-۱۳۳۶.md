---
title: >-
    شمارهٔ ۱۳۳۶
---
# شمارهٔ ۱۳۳۶

<div class="b" id="bn1"><div class="m1"><p>ماییم و غم عشقت و خوابی و خیالی</p></div>
<div class="m2"><p>وز ماه رخت گشته تنم همچو هلالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با محنت هجر تو شب و روز قرینم</p></div>
<div class="m2"><p>تا با تو کجا دست دهد روز وصالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خیل خیال تو بود انس دلم را</p></div>
<div class="m2"><p>گر خاطر محزون کندم دفع ملالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال دل من عرضه دهی پیش نگارم</p></div>
<div class="m2"><p>ای باد صبا گر بود آنجات مجالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گفت گر او حال من خسته بپرسد</p></div>
<div class="m2"><p>گو از غم هجران تو گشتست هلالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس به جهان منصب و مالی طلبیدند</p></div>
<div class="m2"><p>ما را غم عشق تو به از منصب و مالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حقّا که نخواهم نه به دنیی نه به عقبی</p></div>
<div class="m2"><p>جز خاک سر کوی تو مالی و منالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم به جهان آرزوی وصل تو دارم</p></div>
<div class="m2"><p>گفتا چه کنی باز تمنّای وصالی</p></div></div>