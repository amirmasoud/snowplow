---
title: >-
    شمارهٔ ۷۲۳
---
# شمارهٔ ۷۲۳

<div class="b" id="bn1"><div class="m1"><p>چه چاره چونکه دل از وی به کام دل نرسید</p></div>
<div class="m2"><p>زلال لعل لب او به کام دل نچکید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کام دل نرسیدم شبی به دولت وصل</p></div>
<div class="m2"><p>ز درد روز فراق تو جان به لب برسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منه تو بار گران بیش از این به شخص ضعیف</p></div>
<div class="m2"><p>ستمگرا که از آن پشت طاقتم بخمید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حال زار غریبی نزار رحمت کن</p></div>
<div class="m2"><p>که جان بداد به هجران و روی دوست ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکرد مرغ دلم در هوای جان پرواز</p></div>
<div class="m2"><p>که تا شراب مودّت ز جام عشق چشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ضعیف ستمدیده بلاکش من</p></div>
<div class="m2"><p>بیا که از دو جهان مهر روی تو بگزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا و کلبه احزان ما منوّر کن</p></div>
<div class="m2"><p>ز حد گذشت میان من و تو گفت و شنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شد چه بود که آن ماه روی مشکین بوی</p></div>
<div class="m2"><p>به سان آهوی وحشی ز پیش ما برمید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشان که داد چو من بنده در جهان یکدل</p></div>
<div class="m2"><p>بتی ستمگر شوخ ای نگار چون تو که دید</p></div></div>