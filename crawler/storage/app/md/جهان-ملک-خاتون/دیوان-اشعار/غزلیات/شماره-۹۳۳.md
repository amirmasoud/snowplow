---
title: >-
    شمارهٔ ۹۳۳
---
# شمارهٔ ۹۳۳

<div class="b" id="bn1"><div class="m1"><p>به بوی زلف تو دیوانه گشتم</p></div>
<div class="m2"><p>ز خویش و آشنا بیگانه گشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شادی دور گشتم در فراقت</p></div>
<div class="m2"><p>به درد عشق تو همخانه گشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمع جمع جانی در شبستان</p></div>
<div class="m2"><p>از آن رو بر رخت پروانه گشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسی در بحر بی پایان عشقش</p></div>
<div class="m2"><p>به جست و جوی آن دردانه گشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو مرغ زیرکم افتاده در دام</p></div>
<div class="m2"><p>اسیر بند زلف و دانه گشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیدم یک زمان کام دل از یار</p></div>
<div class="m2"><p>به عشقش در جهان افسانه گشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراب آباد دنیا را وفا نیست</p></div>
<div class="m2"><p>بسی در گرد این ویرانه گشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بوی زلف عنبرساش عمریست</p></div>
<div class="m2"><p>به گرد کوی آن جانانه گشتم</p></div></div>