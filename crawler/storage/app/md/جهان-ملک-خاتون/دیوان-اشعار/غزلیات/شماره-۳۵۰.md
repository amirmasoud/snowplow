---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>صبحدم ذوقی ندارد بی تو در بستان گذشت</p></div>
<div class="m2"><p>درد دل دارم ز تو نتوانم از درمان گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ مشکلتر ز هجر جان گداز یار نیست</p></div>
<div class="m2"><p>چون بگویم شدّت هجران من آسان گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی سرویست قدّش در سرابستان جان</p></div>
<div class="m2"><p>چون رسیدی پیش او از راستی نتوان گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دهی فرمان که بگذر از سر جان و جهان</p></div>
<div class="m2"><p>من نیارم دلبرا از حکم و از فرمان گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق گویندم سر و سامان به باد عشق داد</p></div>
<div class="m2"><p>عاشقان آسان توانند از سر و سامان گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر چه ارزد در غمش سامان چه باشد در جهان</p></div>
<div class="m2"><p>عاشق آن باشد که بتواند روان از جان گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل می گوید به من بگذر ز کار و بار عشق</p></div>
<div class="m2"><p>بگذرم از جان ولی نتوانم از جانان گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بعیدم از رخ چون عید فرّخ فال او</p></div>
<div class="m2"><p>لاشه ی شخص ضعیفم بین که از فرمان گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت راز عشق ننهفتی ز یاران گفتمش</p></div>
<div class="m2"><p>قصّهٔ درد دل بیچارهٔ ما زان گذشت</p></div></div>