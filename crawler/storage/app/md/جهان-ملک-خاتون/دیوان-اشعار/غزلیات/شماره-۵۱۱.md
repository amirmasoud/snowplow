---
title: >-
    شمارهٔ ۵۱۱
---
# شمارهٔ ۵۱۱

<div class="b" id="bn1"><div class="m1"><p>عاقبت این درد دل را هم شبی درمان رسد</p></div>
<div class="m2"><p>واین سر سرگشته‌ام از وصل با سامان رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رخش گرچه بعیدم هم به عیدم هست امید</p></div>
<div class="m2"><p>کز برای جان او این لاشه در قربان رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل امّید از وصال یار برنتوان گرفت</p></div>
<div class="m2"><p>بو که شب‌های دراز هجر با پایان رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه‌ای از لعل او کردم تمنّا گفت جان</p></div>
<div class="m2"><p>در عوض خواهم فدا بادت اگر فرمان رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فراق او مرا جان گریبان چاک شد</p></div>
<div class="m2"><p>دست کوتاهم کیم دستی بدان دامان رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال دل را بازگفتن در طریق عشق نیست</p></div>
<div class="m2"><p>خاصه آن ساعت که یک دم جان بر جانان رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دو عالم را به کار عشق کردم در غمت</p></div>
<div class="m2"><p>ای عزیز من جهان را کی سخن در جان رسد</p></div></div>