---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>مرا تا در تنم پیوند جانست</p></div>
<div class="m2"><p>غم عشقش میان جان نهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد هجر آن سرو سمن بوی</p></div>
<div class="m2"><p>سرشک دیده ام بر رخ روانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بربود و بر خاک ره انداخت</p></div>
<div class="m2"><p>نمی دارد نگاهش مشکل آنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذاری گر فتد بر بوستانم</p></div>
<div class="m2"><p>دو چشمم سوی آن سرو روانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا بنشین زمانی دل نشانم</p></div>
<div class="m2"><p>که از مهر توأم در دل نشانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه اندازم به پایت جز سری نیست</p></div>
<div class="m2"><p>فدای جان تو روح و روانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فدا کردم به پایت جان ولیکن</p></div>
<div class="m2"><p>دل بی مهر او با دیگرانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلی چون رویش ای بلبل نگویی</p></div>
<div class="m2"><p>که تا خود در کدامین بوستانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل مسکین من عمریست کز غم</p></div>
<div class="m2"><p>چنین سرگشته از کار جهانست</p></div></div>