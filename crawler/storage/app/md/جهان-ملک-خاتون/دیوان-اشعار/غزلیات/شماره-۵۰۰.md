---
title: >-
    شمارهٔ ۵۰۰
---
# شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>مرغ جان من دلخسته هوا می گیرد</p></div>
<div class="m2"><p>بوی زلف تو هم از باد صبا می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه و خورشید جهان را به یقین می دانم</p></div>
<div class="m2"><p>کز رخ روشن تو نور و ضیا می گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طوبی و نارون اندر چمن باغ بهشت</p></div>
<div class="m2"><p>از قد و قامت تو نشو و نما می گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طبیب دل پردرد نگویی با من</p></div>
<div class="m2"><p>کز من خسته ملال تو چرا می گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه من گر همه آتش شود ای جان جهان</p></div>
<div class="m2"><p>آب حیوان منی در تو کجا می گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازِ مهر من مهجور کبوتر بچه ای</p></div>
<div class="m2"><p>در هوای شب وصلت به بلا می گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودم ز جفایت بنهم سر به جهان</p></div>
<div class="m2"><p>دامن مهر تو ای دوست وفا می گیرد</p></div></div>