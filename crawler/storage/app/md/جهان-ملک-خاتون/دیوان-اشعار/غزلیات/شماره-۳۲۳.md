---
title: >-
    شمارهٔ ۳۲۳
---
# شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>بر من خسته ز هجران چه جفاهاست که نیست</p></div>
<div class="m2"><p>بر دل من ز فراقت چه بلاهاست که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم سرمست تو ترک است و خطایی باشد</p></div>
<div class="m2"><p>با من خسته اش ای جان چه خطاهاست که نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه وفاها که نکردم به غم عشق و ز تو</p></div>
<div class="m2"><p>به من دلشده آخر چه جفاهاست که نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صبوح رخ و در شام سر زلف بتان</p></div>
<div class="m2"><p>وز دهان من مسکین چه دعاهاست که نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بگویم که در اوصاف گل رخسارت</p></div>
<div class="m2"><p>بلبلان را به زبان در، چه ثناهاست که نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جفاهاست که بر من نرسید از غم تو</p></div>
<div class="m2"><p>در دل غم زده ی ما چه وفاهاست که نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هوایت ز هوس گرد جهان می گردم</p></div>
<div class="m2"><p>در سر من ز وصالت چه هواهاست که نیست</p></div></div>