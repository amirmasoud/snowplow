---
title: >-
    شمارهٔ ۱۲۶۷
---
# شمارهٔ ۱۲۶۷

<div class="b" id="bn1"><div class="m1"><p>چه کردم تا زمن دل برگرفتی</p></div>
<div class="m2"><p>به جایم دیگری دلبر گرفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه دیدی از من ای دلدار آخر</p></div>
<div class="m2"><p>که رفتی همدمی دیگر گرفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا ای نور چشم از خون دیده</p></div>
<div class="m2"><p>رخ چون لاله ام در زر گرفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جفا کردی و آنگه بی وفایی</p></div>
<div class="m2"><p>نگارینا دگر با سر گرفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلت چون داد ای دلدار آخر</p></div>
<div class="m2"><p>که جز من دیگری در بر گرفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی دانم چرا از هر دو رخسار</p></div>
<div class="m2"><p>جهانی را تو در آذر گرفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گناهی جز وفاداری ندارم</p></div>
<div class="m2"><p>چرا جانا دل از ما برگرفتی</p></div></div>