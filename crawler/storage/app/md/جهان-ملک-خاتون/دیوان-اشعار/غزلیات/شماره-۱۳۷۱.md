---
title: >-
    شمارهٔ ۱۳۷۱
---
# شمارهٔ ۱۳۷۱

<div class="b" id="bn1"><div class="m1"><p>تا به کی جانا دلم پر خون کنی</p></div>
<div class="m2"><p>وز میان دیده ام بیرون کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی از بار فراقت دلبرا</p></div>
<div class="m2"><p>پشت امّید مرا چون نون کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی از هجر خود ای لیلی عهد</p></div>
<div class="m2"><p>در غم عشق خودم مجنون کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در فراق روی خوبت تا بچند</p></div>
<div class="m2"><p>جویبار دیده ام جیحون کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دل پردرد بی درمان من</p></div>
<div class="m2"><p>از غم هجران خود محزون کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کم کنی هر روز از ما دوستی</p></div>
<div class="m2"><p>در جهان با دشمنان افزون کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل مسکین بساز و دم مزن</p></div>
<div class="m2"><p>ور نسازی با غم او چون کنی</p></div></div>