---
title: >-
    شمارهٔ ۵۳۰
---
# شمارهٔ ۵۳۰

<div class="b" id="bn1"><div class="m1"><p>دل عاشق ز غم پردرد باشد</p></div>
<div class="m2"><p>رخش از درد دوری زرد باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شبهای فراق روی دلبر</p></div>
<div class="m2"><p>ز خورد و خواب دایم فرد باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه در خورد منست این آرزو لیک</p></div>
<div class="m2"><p>شب وصلت مرا در خورد باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم خاک سر کویت مبادا</p></div>
<div class="m2"><p>ز ما بر خاطر تو گرد باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی آگه شود بر دردم ای جان</p></div>
<div class="m2"><p>به عشق او که صاحب درد باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو می دانی مرا در هجر رویت</p></div>
<div class="m2"><p>دلی بس گرم و آهی سرد باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی کاو را بود صبر از نگاری</p></div>
<div class="m2"><p>به عشق او که صاحب درد باشد</p></div></div>