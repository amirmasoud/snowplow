---
title: >-
    شمارهٔ ۱۳۲۸
---
# شمارهٔ ۱۳۲۸

<div class="b" id="bn1"><div class="m1"><p>من تو را دارم و جز لطف توأم نیست کسی</p></div>
<div class="m2"><p>در جهانم نبود غیر تو فریادرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفسی بی تو نیارم زدن ای جان گرچه</p></div>
<div class="m2"><p>نکنی یاد من خسته به عمری نفسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکسی راست هوایی و خیالی در سر</p></div>
<div class="m2"><p>من بجز فکر و خیال تو ندارم هوسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیش از اینم چو مگس از شکر خویش مران</p></div>
<div class="m2"><p>که تفاوت نکند در شکرستان مگسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر من دلشده هر چند گزیدی دگری</p></div>
<div class="m2"><p>به وصالت که به جای تو مرا نیست کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرق دریای غم عشقم و از خون جگر</p></div>
<div class="m2"><p>می رود بی رخت از چشمه چشمم ارسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل جان من از شوق گلستان رخت</p></div>
<div class="m2"><p>تا به کی صبر کند نعره زنان در قفسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می درآید چو جرس دشمن بیهوده درای</p></div>
<div class="m2"><p>نتوان ترک غمم گفت به بانگ جرسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طالب وصل تو ای خسرو خوبان جهان</p></div>
<div class="m2"><p>نه من دلشده ام بس که چو من هست بسی</p></div></div>