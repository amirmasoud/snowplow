---
title: >-
    شمارهٔ ۱۳۰۵
---
# شمارهٔ ۱۳۰۵

<div class="b" id="bn1"><div class="m1"><p>به دردی دل گرفتارست باری</p></div>
<div class="m2"><p>که درمان نیستش جز وصل یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیبانم دوا گفتند از آن لب</p></div>
<div class="m2"><p>که هست او نازنینی گلعذاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر درد دلم زان لب شود خوش</p></div>
<div class="m2"><p>که گل با شکّرش بودست کاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسلمانان چه تدبیرم که نگرفت</p></div>
<div class="m2"><p>دو دست بخت من یک شب نگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه باشد گر چو سرو ناز روزی</p></div>
<div class="m2"><p>ز لطف خود کند بر ما گذاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر بر دیده مالم خاک پایت</p></div>
<div class="m2"><p>که بر دل دارم از هجران غباری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به فریادم رس ای دلبر کزین بیش</p></div>
<div class="m2"><p>ندارم طاقت هجر تو باری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوابم داد کای بیچاره هیهات</p></div>
<div class="m2"><p>به وصلم تا که باشد بختیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر بختم دهد یاری به وصلت</p></div>
<div class="m2"><p>جهان را نیز باشد بخت یاری</p></div></div>