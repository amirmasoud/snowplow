---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>برگشت نگار و دل ز ما برد</p></div>
<div class="m2"><p>ما را به غم فراق بسپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دل که ز ما ستد به دستان</p></div>
<div class="m2"><p>بستد ز من آن بگو کجا برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگشت ز ما و خسته جانم</p></div>
<div class="m2"><p>از تیغ فراق خود بیازرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دست فراق او دل من</p></div>
<div class="m2"><p>خون جگر از دو دیده بفشرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد که هجر سوزناکت</p></div>
<div class="m2"><p>گردِ ستم از جهان برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آوخ چه کنم که باد بویی</p></div>
<div class="m2"><p>سوی من خسته دل نیاورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می دان به یقین که برنیاید</p></div>
<div class="m2"><p>کاری که بزرگ باشد از خرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این باده فروش هم غلط کرد</p></div>
<div class="m2"><p>نشناخت شراب صافی از درد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آتش عشق گرم بودیم</p></div>
<div class="m2"><p>آخر تو بگو که از چه بفسرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چند که در جهان وفا نیست</p></div>
<div class="m2"><p>در درد غمت وفا به سر برد</p></div></div>