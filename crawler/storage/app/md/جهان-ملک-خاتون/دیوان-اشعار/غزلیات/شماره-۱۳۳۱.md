---
title: >-
    شمارهٔ ۱۳۳۱
---
# شمارهٔ ۱۳۳۱

<div class="b" id="bn1"><div class="m1"><p>چرا جانا به دردم شاد باشی</p></div>
<div class="m2"><p>چرا راضی بدین بیداد باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگارا چون ز جانت بنده گشتم</p></div>
<div class="m2"><p>چو سرو از ما چرا آزاد باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنفشه وار در پایت فتادم</p></div>
<div class="m2"><p>که تا در باغ چون شمشاد باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلا تا کی ز جور آن ستمگر</p></div>
<div class="m2"><p>چنین با ناله و فریاد باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر عمری گرم یادم نیاری</p></div>
<div class="m2"><p>مرا همچون نفس در یاد باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل گفتم رسم در دوست گفتا</p></div>
<div class="m2"><p>کجا در وی رسی گر باد باشی</p></div></div>