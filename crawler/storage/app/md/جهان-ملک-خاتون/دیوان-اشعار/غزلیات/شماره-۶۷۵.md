---
title: >-
    شمارهٔ ۶۷۵
---
# شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>دلبرا نقش خیالت ز دل ما نرود</p></div>
<div class="m2"><p>مُهر مهرت نفسی زین دل شیدا نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگذرد بر من سودا زده روزی به غلط</p></div>
<div class="m2"><p>که دلم از سر زلف تو به سودا نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لحظه ای در همه اوقات میسّر نشود</p></div>
<div class="m2"><p>که ز هجرت ستمی بر من شیدا نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق خسته چو بر خاک درت ساخت مقام</p></div>
<div class="m2"><p>گر به تیغش بزند حاسد از آنجا نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشبم وعده فردا چه دهی می ترسم</p></div>
<div class="m2"><p>که مرا کاری از این وعده فردا نرود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی منع من از صحبت جانان چه کنی</p></div>
<div class="m2"><p>که به بادی مگس از صحبت حلوا نرود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل مژگان من خسته جهان کرد خراب</p></div>
<div class="m2"><p>بس عجب دارم ار این سیل به دریا نرود</p></div></div>