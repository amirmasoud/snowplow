---
title: >-
    شمارهٔ ۱۱۹۸
---
# شمارهٔ ۱۱۹۸

<div class="b" id="bn1"><div class="m1"><p>صبا باز آ که در مان دارم از تو</p></div>
<div class="m2"><p>به دردم منّت جان دارم از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیب من تویی مشکل توانم</p></div>
<div class="m2"><p>که درد خویش پنهان دارم از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا و بوی زلفینش بیاور</p></div>
<div class="m2"><p>بگو اشکی چو باران دارم از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگویی تا به کی ای بی وفا یار</p></div>
<div class="m2"><p>دو چشم بخت گریان دارم از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی مشکل که در را هم نهادی</p></div>
<div class="m2"><p>من بیچاره آسان دارم از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بده کام دلم از وصل ورنه</p></div>
<div class="m2"><p>به هر کویی من افغان دارم از تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشد خالی ز من خیل خیالت</p></div>
<div class="m2"><p>درون دیده مهمان دارم از تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهر از دیده و دینارم از رخ</p></div>
<div class="m2"><p>به هجران نیک ارزان دارم از تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر جور جهان آید به رویم</p></div>
<div class="m2"><p>نگردم زان که پیمان دارم از تو</p></div></div>