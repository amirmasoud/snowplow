---
title: >-
    شمارهٔ ۱۰۸۰
---
# شمارهٔ ۱۰۸۰

<div class="b" id="bn1"><div class="m1"><p>تا دیده و دل در سر زلفین تو بستیم</p></div>
<div class="m2"><p>واندر طلب وصل تو جان بر کف دستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زلف پریشان تو مجموع گرفتار</p></div>
<div class="m2"><p>وز نرگس شهلات نه مخمور و نه مستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما وصل تو خواهیم که آیی بر آغوش</p></div>
<div class="m2"><p>دیدار نمایی نه که خورشید پرستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برخاسته ای از سر مهرم چه توان کرد</p></div>
<div class="m2"><p>با آنکه ز جان در غم روی تو نشستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند نداری سر وصل من مسکین</p></div>
<div class="m2"><p>ما از دل و جان بنده و مشتاق تو هستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنمای تو خورشید جمال رخ خود را</p></div>
<div class="m2"><p>در صبحدمی تا صلواتی بفرستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما آب رخ خود به جهانی نفروشیم</p></div>
<div class="m2"><p>چون خاک ره آخر چه سبب پیش تو پستیم</p></div></div>