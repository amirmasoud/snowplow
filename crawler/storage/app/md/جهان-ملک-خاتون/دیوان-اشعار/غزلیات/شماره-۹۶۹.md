---
title: >-
    شمارهٔ ۹۶۹
---
# شمارهٔ ۹۶۹

<div class="b" id="bn1"><div class="m1"><p>باز در سر هوس روی نگاری دارم</p></div>
<div class="m2"><p>نه به شب خواب و نه در روز قرای دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارها با تو دلم گفت که عمریست که من</p></div>
<div class="m2"><p>بر دل از حسرت دیدار تو باری دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده لعل لبت کرد مرا مست و هنوز</p></div>
<div class="m2"><p>در سر از غمزه مست تو خماری دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالمی را به تو دلشاد و من خسته جگر</p></div>
<div class="m2"><p>از همه ملک جهان دامن خاری دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکسی را ز میانیست کنار یاری</p></div>
<div class="m2"><p>زین میانه من سرگشته کناری دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش در دو جهان جز تو مرا یاری نیست</p></div>
<div class="m2"><p>شرم نامد ز منش گفت که آری دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل رویت همه در دست رقیبست چرا</p></div>
<div class="m2"><p>من ز گلزار شب وصل تو خاری دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش گر تو دو صد جور کنی بر دل ریش</p></div>
<div class="m2"><p>به جهان غیر غم عشق تو کاری دارم</p></div></div>