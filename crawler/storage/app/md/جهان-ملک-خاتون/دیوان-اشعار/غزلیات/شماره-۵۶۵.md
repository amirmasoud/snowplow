---
title: >-
    شمارهٔ ۵۶۵
---
# شمارهٔ ۵۶۵

<div class="b" id="bn1"><div class="m1"><p>شب هجران که پایانش نباشد</p></div>
<div class="m2"><p>بود دردی که درمانش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری کاو از هوای عشق خالیست</p></div>
<div class="m2"><p>یقین دانم که سامانش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مباد آن کس که در شبهای هجران</p></div>
<div class="m2"><p>که بر دل هیچ فرمانش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا یابی کسی بر درد هجران</p></div>
<div class="m2"><p>که دستی بر گریبانش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن کاو یافت مشکل روز وصلش</p></div>
<div class="m2"><p>بلای هجر آسانش نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبر بیهوده رنجی در پی او</p></div>
<div class="m2"><p>که قول و عهد و پیمانش نباشد</p></div></div>