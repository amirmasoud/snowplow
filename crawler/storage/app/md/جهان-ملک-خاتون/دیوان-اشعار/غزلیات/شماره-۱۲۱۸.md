---
title: >-
    شمارهٔ ۱۲۱۸
---
# شمارهٔ ۱۲۱۸

<div class="b" id="bn1"><div class="m1"><p>ای صبا قصّه ی دردم بر دلدار بگو</p></div>
<div class="m2"><p>حال این خسته هجران بر آن یار بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوزش سینه مجروح پریشان مرا</p></div>
<div class="m2"><p>بنشین پیشش و درد دل افگار بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظری سوی جهان بفکن و نیکو بنگر</p></div>
<div class="m2"><p>حالت مردمک دیده ی خونبار بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز حال من سرگشته هجران پرسد</p></div>
<div class="m2"><p>گشتم ای دوست به کان دل اغیار بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرزنش یافته چون حلقه ی در روز و شبم</p></div>
<div class="m2"><p>که نداریم به خلوتگه تو بار بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه سلامی نه پیامی نه گذاری بر ما</p></div>
<div class="m2"><p>کرد ترک من دلداده به یکبار بگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم بسیار ز دست شب هجران خوردم</p></div>
<div class="m2"><p>رس به فریاد من خسته ی غمخوار بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از وصالت دمکی خسته ی هجران بنواز</p></div>
<div class="m2"><p>که شدم روز ز هجران چو شب تار بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گلستان وصال رخت ای جان جهان</p></div>
<div class="m2"><p>از چه رو قسمت ما نیست بجز خار بگو</p></div></div>