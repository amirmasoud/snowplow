---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>دلبر دلم ببرد و غم حال ما نداشت</p></div>
<div class="m2"><p>وز جور شحنه ای چو غمش بر دلم گماشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگزید دلبری و بپیچید سر ز ما</p></div>
<div class="m2"><p>شرم و حیا ز روی من خسته دل نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقّاش روزگار همه صورت رخت</p></div>
<div class="m2"><p>گویی درون دیده ی مهجور ما نگاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در باغ چون رخ تو بگو کی گلی شکفت</p></div>
<div class="m2"><p>با قدّ تو کدام سهی سرو سرفراشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس چو دید قد بلندت ز سیم و زر</p></div>
<div class="m2"><p>کرد او نثار مقدم وصل تو هرچه داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذشت و حال زار جهان دید و همچنان</p></div>
<div class="m2"><p>ما را میان بحر غم و غصّه واگذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشقش برون نمی رودم از دل و دماغ</p></div>
<div class="m2"><p>گویی که تخم مهر خود اندر جهان بکاشت</p></div></div>