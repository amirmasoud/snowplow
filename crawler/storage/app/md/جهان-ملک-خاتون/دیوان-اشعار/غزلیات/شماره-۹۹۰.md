---
title: >-
    شمارهٔ ۹۹۰
---
# شمارهٔ ۹۹۰

<div class="b" id="bn1"><div class="m1"><p>بیا ای سرو ناز من روان تا در برت گیرم</p></div>
<div class="m2"><p>گهی دست تو را گیرم گهی در پای تو میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر کامم به ناکامی رسانی از شب وصلم</p></div>
<div class="m2"><p>به روز حشر برخیزم به حسرت دامنت گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیب من چو دردم دید در دم گفت بیچاره</p></div>
<div class="m2"><p>دوای تو نمی دانم چو رفت از دست تدبیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوابش ای دل دانا من نادان چنین دانم</p></div>
<div class="m2"><p>که در پای دل شیدا ز زلف اوست زنجیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمان ابروانت چون مرا خم داد پشت دل</p></div>
<div class="m2"><p>مزن باری تو از غمزه به ناوکهای چون تیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نور تجلّی را نمایی ور نهان داری</p></div>
<div class="m2"><p>مرا باری به نام تست بام و شام تکبیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم دانی که می داند بد و نیک جهان بسیار</p></div>
<div class="m2"><p>تو آخر چند بفریبی به تقریر و به تحریرم</p></div></div>