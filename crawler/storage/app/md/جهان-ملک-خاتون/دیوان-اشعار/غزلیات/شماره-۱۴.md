---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای به روی تو دیده باز مرا</p></div>
<div class="m2"><p>چاره ای از وصال ساز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش از اینم نماند طاقت و صبر</p></div>
<div class="m2"><p>بیم دیوانگیست باز مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باشد ای نور چشم و راحت جان</p></div>
<div class="m2"><p>بر رخ خوب تو نیاز مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو قندم به بوته ی هجران</p></div>
<div class="m2"><p>چند داری تو در گداز مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون که محراب ابرویش دیدم</p></div>
<div class="m2"><p>واجب آمد در او نماز مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر کوی دلبران آورد</p></div>
<div class="m2"><p>به یکی شکل و شیوه باز مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با غم عشقت آفرید از ازل</p></div>
<div class="m2"><p>مگر ای دوست بی نیاز مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در چمن دوش گفتم ای بلبل</p></div>
<div class="m2"><p>گل از آن تو، سرو ناز مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در جهان خود دگر نمی باید</p></div>
<div class="m2"><p>بی وصال تو برگ و ساز مرا</p></div></div>