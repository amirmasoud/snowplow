---
title: >-
    شمارهٔ ۵۷۰
---
# شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>به دردت داروی دردم نباشد</p></div>
<div class="m2"><p>ز دردت جز رخی زردم نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی لطف خود دریاب ما را</p></div>
<div class="m2"><p>که گر جویی دگر گردم نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به میدان وفا و عشق بازی</p></div>
<div class="m2"><p>کسی دیگر هماوردم نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فراق روی تو ای نور دیده</p></div>
<div class="m2"><p>به جان تو که در خوردم نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بگرفت دم در درد هجران</p></div>
<div class="m2"><p>تحمّل بیش از این دردم نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر از وصل روح افزایت ای جان</p></div>
<div class="m2"><p>تو دانی داروی دردم نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بده کام دلم یک دم ز وصلت</p></div>
<div class="m2"><p>که تا درد سرت هر دم نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جگر گر هست ما را در غم عشق</p></div>
<div class="m2"><p>بگو تا چون دم سردم نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مسلمانان مرا جز سینه ی ریش</p></div>
<div class="m2"><p>از آن ماه جهان گردم نباشد</p></div></div>