---
title: >-
    شمارهٔ ۶۵۱
---
# شمارهٔ ۶۵۱

<div class="b" id="bn1"><div class="m1"><p>خاطرم از هر دو کون آزاد بود</p></div>
<div class="m2"><p>با خیالش روز و شب دلشاد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خیالم قدّ آن دلبر گذشت</p></div>
<div class="m2"><p>چون بدیدم قامت شمشاد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش قدّش بنده گشتم رایگان</p></div>
<div class="m2"><p>گرچه یار ما چو سرو آزاد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد من یک لحظه از وصلش نداد</p></div>
<div class="m2"><p>وآنچه کرد او بر من از بیداد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی جفایش از دلم</p></div>
<div class="m2"><p>شب همه شب ناله و فریاد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قول و عهدی بود ما را در میان</p></div>
<div class="m2"><p>عهد بشکستی و قولت باد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون وفایی نیست در عهد جهان</p></div>
<div class="m2"><p>زان سبب عهد تو بی بنیاد بود</p></div></div>