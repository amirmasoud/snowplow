---
title: >-
    شمارهٔ ۱۰۵۰
---
# شمارهٔ ۱۰۵۰

<div class="b" id="bn1"><div class="m1"><p>شب وصال میسّر نمی شود چه کنم</p></div>
<div class="m2"><p>سعادتیست چو باور نمی شود چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز خیال که او نور دیده ی بصرست</p></div>
<div class="m2"><p>به پیش دیده مصوّر نمی شود چه کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببین که توسن ایام تند سفله نواز</p></div>
<div class="m2"><p>به هیچ گونه مسخّر نمی شود چه کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب فراق که تاریکتر ز روز منست</p></div>
<div class="m2"><p>به شمع وصل منوّر نمی شود چه کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر زری که ز رخ دارم وز دیده گهر</p></div>
<div class="m2"><p>گدای عشق توانگر نمی شود چه کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار حیله و دستان به وصل او کردم</p></div>
<div class="m2"><p>به حیله کار میسّر نمی شود چه کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن چو قند مکرّر بود مرا به جهان</p></div>
<div class="m2"><p>چو ذکر دوست مکرّر نمی شود چه کنم</p></div></div>