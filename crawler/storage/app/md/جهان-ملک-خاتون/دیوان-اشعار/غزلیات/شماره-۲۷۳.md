---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>ای که پنداری که ما را جز تو یاری هست نیست</p></div>
<div class="m2"><p>یا مرا غیر از غم عشق تو کاری هست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستم از غم گیر ای دلبر که افتادم ز پای</p></div>
<div class="m2"><p>زآنکه ما را در جهان جز تو نگاری هست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چو چنگم می زنی ور می نوازی همچو نی</p></div>
<div class="m2"><p>ای که خواهی گفت ما را از تو عاری هست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بار بسیارست بر جان من مسکین ز غم</p></div>
<div class="m2"><p>هیچ باری چون غم هجرانت باری هست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم مستش برد خواب از چشم بیداران ولیک</p></div>
<div class="m2"><p>همچو زلف سرکش او بی قراری هست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو گویی بر دلم از تو جفایی نیست هست</p></div>
<div class="m2"><p>در بلای عشق چون من بردباری هست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندگان بسیار داری در جهان بهتر ز من</p></div>
<div class="m2"><p>بنده ی بیچاره باری در شماری هست نیست</p></div></div>