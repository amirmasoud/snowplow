---
title: >-
    شمارهٔ ۱۲۹۹
---
# شمارهٔ ۱۲۹۹

<div class="b" id="bn1"><div class="m1"><p>گرم بودی به وصلت اختیاری</p></div>
<div class="m2"><p>نبودی بر دلم از هجر باری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم بر آتش رخسارت ای جان</p></div>
<div class="m2"><p>چو زلف تو نمی گیرد قراری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه یاری داد بختم در فراقت</p></div>
<div class="m2"><p>که تا گردم به وصلت بختیاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باری بر نگیری از دل من</p></div>
<div class="m2"><p>نظر کن بر من دلخسته باری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جفا چندین مکن بر حال زارم</p></div>
<div class="m2"><p>ببخشا بر دل زاری نزاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ربود از من به دستان دل نگارم</p></div>
<div class="m2"><p>ندیده کس بدین دستان نگاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نپرسی از من دلخسته دانم</p></div>
<div class="m2"><p>جهان را نیست پیشت اعتباری</p></div></div>