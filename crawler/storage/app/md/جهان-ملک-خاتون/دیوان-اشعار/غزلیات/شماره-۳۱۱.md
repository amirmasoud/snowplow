---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>الهی شکر و فضلت را کران نیست</p></div>
<div class="m2"><p>که را حمد و ثنایت در زبان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توکل کرده ی دریای حق را</p></div>
<div class="m2"><p>به گلّه هیچ محتاج شبان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گنج درّ معنی کم تو دادی</p></div>
<div class="m2"><p>مرا چندان شعف بر پاسبان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی برگ درختی کو نه ذکرت</p></div>
<div class="m2"><p>کند گویا که آن در بوستان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلی کان نشکفاند باد لطفت</p></div>
<div class="m2"><p>یقین دانم که اندر گلستان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه پرسی حال زار ناتوانان</p></div>
<div class="m2"><p>که کس را یک نظر بر ناتوان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سروش جای دادی بر دل خاک</p></div>
<div class="m2"><p>از آن قدی چنان در بوستان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز درگاه لطفت ای جهاندار</p></div>
<div class="m2"><p>تو می دانی مرا جا و مکان نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز هجر مدعی ما را به گیتی</p></div>
<div class="m2"><p>به جز درگاه تو دارالامان نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترا باشد به عالم بنده بسیار</p></div>
<div class="m2"><p>ولی چون من غریبی در جهان نیست</p></div></div>