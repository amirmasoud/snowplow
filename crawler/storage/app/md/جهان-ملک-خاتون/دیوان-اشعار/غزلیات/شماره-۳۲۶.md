---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>چه کنم چون ز بخت یاری نیست</p></div>
<div class="m2"><p>با منش رای سازگاری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل خسته با فراق توأم</p></div>
<div class="m2"><p>چاره جز صبر و سازگاری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه زاری به عشق تن در ده</p></div>
<div class="m2"><p>کار عاشق به غیر زاری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار ما بی وفا و بدمهر است</p></div>
<div class="m2"><p>در دل او وفا و یاری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کیت میل بر جفا باشد</p></div>
<div class="m2"><p>مکن این شرط دوستداری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عزیزم مکن جفا زین بیش</p></div>
<div class="m2"><p>که تحمّل مرا به خواری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم فزودی و ترک ما گفتی</p></div>
<div class="m2"><p>آخرت رسم غمگساری نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته ای از برم چرا دوری</p></div>
<div class="m2"><p>دوری از دوست اختیاری نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه جان و جهان به تیغ فراق</p></div>
<div class="m2"><p>خسته ای، غیر جان سپاری نیست</p></div></div>