---
title: >-
    شمارهٔ ۶۱۴
---
# شمارهٔ ۶۱۴

<div class="b" id="bn1"><div class="m1"><p>چون تو چشم مرحمت بر حال ما خواهی فکند</p></div>
<div class="m2"><p>بیش از این جور و جفا از تو نمی دارم پسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور مرا از آتش هجران بخواهی سوختن</p></div>
<div class="m2"><p>خاک راهت گشته ام بیداد و خواری تا به چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بت نامهربان حدی بود هر چیز را</p></div>
<div class="m2"><p>مرغ جانم را تو تا کی داری اندر قید و بند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا ز بندش ده خلاصی یا بکش تا وارهد</p></div>
<div class="m2"><p>پند من بشنو ازین بینش به زلف خود مبند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کشی و می کشی ما را بدام زلف خود</p></div>
<div class="m2"><p>چون کشد خود را ز شستت آهوی سر در کمند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با قد چون سرو و با این عارض همچون سمن</p></div>
<div class="m2"><p>با رخ همچون گل و لاله به لعل همچو قند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ربود از دستم آن دلدار شهرآشوب باز</p></div>
<div class="m2"><p>با قد چون سرو و چشم شوخ و زلف چون کمند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ربودی دل ز دستم رفتم از دل هوش و صبر</p></div>
<div class="m2"><p>بیش از این مپسند بر ما از غم هجران گزند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون منم اندر جهان از عشق سرگردان چرا</p></div>
<div class="m2"><p>آن بت مه روی از دل بیخ مهر ما بکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم ار آیی شبی مهمان ما لطفی بود</p></div>
<div class="m2"><p>گفت رو هرزه مگو زآنجا برو بر خود مخند</p></div></div>