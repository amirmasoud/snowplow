---
title: >-
    شمارهٔ ۱۰۷۵
---
# شمارهٔ ۱۰۷۵

<div class="b" id="bn1"><div class="m1"><p>دور از تو دلبرا چه جفاها کشیده ایم</p></div>
<div class="m2"><p>وز طعن دشمنان چه سخنها شنیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که دیده از غمت ای نور دیده دید</p></div>
<div class="m2"><p>نشنیده ایم از کس و هرگز ندیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دور چرخ سفله ز دست حریف هجر</p></div>
<div class="m2"><p>بس جرعه ی جفا که به یادت کشیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی که چیست حاصل عمرت ز عشق ما</p></div>
<div class="m2"><p>دل رفت در پیش طمع از جان بریده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از اشتیاق آن دهن همچو میم تو</p></div>
<div class="m2"><p>چون نون ز بار محنت هجران خمیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین بیش خون مردمک دیده ام مریز</p></div>
<div class="m2"><p>کاو را به نار و خون جگر پروریده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما ذرّه وار در سر بازار مهر یار</p></div>
<div class="m2"><p>شادی به باد داده و غم را خریده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دوست یاد ما نکند ما ز مهر دل</p></div>
<div class="m2"><p>بر یاد دوست جامه جان را دریده ایم</p></div></div>