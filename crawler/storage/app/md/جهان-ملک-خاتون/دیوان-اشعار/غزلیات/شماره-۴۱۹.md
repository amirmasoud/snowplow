---
title: >-
    شمارهٔ ۴۱۹
---
# شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>تا چند کنم جانا از دست غمت فریاد</p></div>
<div class="m2"><p>زین بیش نمی آرم من طاقت این بیداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با غم هجرانت تا کی گذرانم روز</p></div>
<div class="m2"><p>شاید که کنی یک شب از وصل خودم دلشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم قد رعنایت گشتم ز میان جان</p></div>
<div class="m2"><p>من بنده آن قامت هستی تو چو سرو آزاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخرام میان باغ تا قامت تو بیند</p></div>
<div class="m2"><p>افتد ز قدم در دم هم طوبی و هم شمشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآتش هجرانت ای دوست خبر داری</p></div>
<div class="m2"><p>کاین خانه ی صبر من برکند غم از بنیاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال دل مسکینم آخر که تواند گفت</p></div>
<div class="m2"><p>ای نور دو چشم من در گوش تو غیر از باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریاد جهان سوزم افتاده به کوه و دشت</p></div>
<div class="m2"><p>تا سوز غم عشقت در هر دو جهان افتاد</p></div></div>