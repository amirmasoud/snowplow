---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>تو را از حال مسکینان خبر نیست</p></div>
<div class="m2"><p>بر آب چشم ما زانت گذر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زاری زارم از هجران رویت</p></div>
<div class="m2"><p>چرا او را به سوی ما نظر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب تاریک هجرانم بفرسود</p></div>
<div class="m2"><p>در آن شب گوییا هرگز قمر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب دیجور بی پایان چو زلفش</p></div>
<div class="m2"><p>ز صبح روی جانانم اثر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتی سنگین دلی خوشی جفاجوست</p></div>
<div class="m2"><p>بر این مسکین دلم رحمش مگر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه صبرم هست از رویش نه آرام</p></div>
<div class="m2"><p>شب و روزم ز عشقش خواب و خور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تیغ هجرم از خود چند رانی</p></div>
<div class="m2"><p>مکن جانا که ما را این سپر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرم صد ره برانی، این دل من</p></div>
<div class="m2"><p>بجز بر بوی زلفت راهبر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلا گرد در او چند گردی</p></div>
<div class="m2"><p>ز کوی عشق جانان ره به در نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هجران رخت جانا جهان را</p></div>
<div class="m2"><p>غذای دل بجز خون جگر نیست</p></div></div>