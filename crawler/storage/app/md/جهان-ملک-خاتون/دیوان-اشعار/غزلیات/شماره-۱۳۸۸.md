---
title: >-
    شمارهٔ ۱۳۸۸
---
# شمارهٔ ۱۳۸۸

<div class="b" id="bn1"><div class="m1"><p>چه ساعتی بود آیا که آن چنان ماهی</p></div>
<div class="m2"><p>درآید از در من بی رقیب ناگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طناب خیمه غم بگسلد ز منزل هجر</p></div>
<div class="m2"><p>زند به گلشن جانم ز وصل خرگاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امور ملک چه نقصان پذیرد ار شاهان</p></div>
<div class="m2"><p>نظر به حال گدایان کنند گه گاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان دوست که گر جان به لب رسد در دل</p></div>
<div class="m2"><p>نباشدم بجز از وصل دوست دلخواهی</p></div></div>