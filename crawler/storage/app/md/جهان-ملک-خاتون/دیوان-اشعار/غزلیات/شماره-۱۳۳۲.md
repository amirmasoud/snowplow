---
title: >-
    شمارهٔ ۱۳۳۲
---
# شمارهٔ ۱۳۳۲

<div class="b" id="bn1"><div class="m1"><p>دلا تو تا به غم عشق در جهان باشی</p></div>
<div class="m2"><p>میان خلق جهان بی سخن چو جان باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نرگسم شده بیمار تا به کی با ما</p></div>
<div class="m2"><p>چو سوسن ای بت مهروی ده زبان باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز نسل بنی آدمی بگو آخر</p></div>
<div class="m2"><p>چرا ز دیده ی ما چون پری نهان باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حد گذشت مرا شرح حال عشق ای دل</p></div>
<div class="m2"><p>قلم صفت تو ز غم چند سر دوان باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگار خوش به سر ناز بالش امید</p></div>
<div class="m2"><p>به خواب خوش تو چرا سر بر آستان باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراغتیست تو را از جهان بگو تا چند</p></div>
<div class="m2"><p>مدام بر سر بازار داستان باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار جان به فدایت کنم من از سر شوق</p></div>
<div class="m2"><p>میان باغ دل من تو چون روان باشی</p></div></div>