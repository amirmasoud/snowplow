---
title: >-
    شمارهٔ ۹۶۲
---
# شمارهٔ ۹۶۲

<div class="b" id="bn1"><div class="m1"><p>دلبرا آرزوی دیدن رویت دارم</p></div>
<div class="m2"><p>روز و شب فکر هوای سر کویت دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه از شوق کنارت به خیال تو مدام</p></div>
<div class="m2"><p>دل به مهرت گرو و دیده به سویت دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کی کار من خسته پریشان داری</p></div>
<div class="m2"><p>رحم کن رحم که دل در خم مویت دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی زلفت به من آورد نسیم سحری</p></div>
<div class="m2"><p>زان فرح جان و جهان زنده به بویت دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه از خوی تو جز غصّه ندارم حاصل</p></div>
<div class="m2"><p>هر چه دارم من بیچاره ز خویت دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به چوگان نزنی بر سر میدان جهان</p></div>
<div class="m2"><p>سر به دست از هوس بودن گویت دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر قدم رنجه کنی روزی و تشریف دهی</p></div>
<div class="m2"><p>در جهان چون دل و چون دیده به کویت دارم</p></div></div>