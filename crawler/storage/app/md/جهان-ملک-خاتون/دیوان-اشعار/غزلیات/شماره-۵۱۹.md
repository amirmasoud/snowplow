---
title: >-
    شمارهٔ ۵۱۹
---
# شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>فریاد کاین طبیب به دردم نمی رسد</p></div>
<div class="m2"><p>دستم به دور وصل تو هر دم نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجروح شد دلم به سر نیش اشتیاق</p></div>
<div class="m2"><p>مشکل که از وصال تو مرهم نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راضی شدم به نکهت زلفین دلکشت</p></div>
<div class="m2"><p>فریاد و الغیاث که آن هم نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلدار اگرچه همدم یاران محرمست</p></div>
<div class="m2"><p>ما را به غیر غم ز تو همدم نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیش فراق روی تو دانی که هر نفس</p></div>
<div class="m2"><p>بر جان خستگانت ز صد کم نمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرّاح هجر روی تو بس نیش می زند</p></div>
<div class="m2"><p>بر دل ولی چه سود که بر دم نمی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندانکه دیده بر در شادی نهاده ام</p></div>
<div class="m2"><p>بس حلقه بر در دلم از غم نمی رسد</p></div></div>