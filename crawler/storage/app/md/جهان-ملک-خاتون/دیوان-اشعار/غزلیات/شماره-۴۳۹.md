---
title: >-
    شمارهٔ ۴۳۹
---
# شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>دلبر غم حال ما ندارد</p></div>
<div class="m2"><p>یک ذرّه به دل وفا ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاطر او مگر وفا نیست</p></div>
<div class="m2"><p>یا خود سر و برگ ما ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حد بگذشت جور بر ما</p></div>
<div class="m2"><p>باشد که چنین روا ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او جان منست بی تکلّف</p></div>
<div class="m2"><p>جان از تن ما جدا ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردیست مرا که جز وصالش</p></div>
<div class="m2"><p>در هر دو جهان دوا ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با بخت من آن نگار باری</p></div>
<div class="m2"><p>غیر از ستم و جفا ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داریم هوای کوی دلبر</p></div>
<div class="m2"><p>این بنده جز این خطا ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نیست ورا نظر به سویم</p></div>
<div class="m2"><p>او دست ز ما چرا ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلطان جهان ز روی رحمت</p></div>
<div class="m2"><p>رحمی به دل گدا ندارد</p></div></div>