---
title: >-
    شمارهٔ ۶۵۵
---
# شمارهٔ ۶۵۵

<div class="b" id="bn1"><div class="m1"><p>فروغ حسن تو تا در کمال خواهد بود</p></div>
<div class="m2"><p>زبان فکر من از مدح لال خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرام باد بر آنکس همیشه خواب و قرار</p></div>
<div class="m2"><p>که گفت خون دل ما حلال خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جان رسید دل ما ز هجر تو تا کی</p></div>
<div class="m2"><p>تو را ز صحبت ما این ملال خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حسن تکیه مکن بیش از این که هم روزی</p></div>
<div class="m2"><p>کمال حسن تو را هم زوال خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چو عود نوازی به وصلم از هجران</p></div>
<div class="m2"><p>یقین که عاقبتم گوشمال خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر جهان همه حوری شوند پیوسته</p></div>
<div class="m2"><p>دو دیده ام نگران جمال خواهد بود</p></div></div>