---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>یار ما شبهای تاری بر تو همچون روز باد</p></div>
<div class="m2"><p>سال و ماه و هفته و روزت به از نوروز باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لشکر منصور رایات همایون تو گشت</p></div>
<div class="m2"><p>دایم از فتح و ظفر بر دشمنان فیروز باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بدخواهان جاهت ای دو دیده همچو شمع</p></div>
<div class="m2"><p>در سر بالین عیشت روز و شب در سوز باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دشمنانت همچو روباهند جاها در کمین</p></div>
<div class="m2"><p>شیر چرخ هفتمین پیش تو دست آموز باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ قهر جان ستانت ای ز جان خوشتر مرا</p></div>
<div class="m2"><p>در دو چشم دشمنانت ناوک دلدوز باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به نخجیر سعادت میل فرمایی ز جان</p></div>
<div class="m2"><p>بس پلنگ سفله دوران تو را چون یوز باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع جمع خاطر محزون مایی راستی</p></div>
<div class="m2"><p>در شبستان وفا دایم جهان افروز باد</p></div></div>