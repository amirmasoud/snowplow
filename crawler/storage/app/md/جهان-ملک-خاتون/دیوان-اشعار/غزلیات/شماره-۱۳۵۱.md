---
title: >-
    شمارهٔ ۱۳۵۱
---
# شمارهٔ ۱۳۵۱

<div class="b" id="bn1"><div class="m1"><p>دل من به دست آر اگر می توانی</p></div>
<div class="m2"><p>که جز لطف تو کس ندارم تو دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز درد غم عشق بس ناتوانم</p></div>
<div class="m2"><p>به فریاد من رس اگر می توانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیم صبا از من ناتوان [تو]</p></div>
<div class="m2"><p>به گوش نگارم پیامی رسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگویش ز من بیوفایی مکن</p></div>
<div class="m2"><p>چه بد عهد یاری چه نامهربانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان بنده ای شد ز درگاه تو</p></div>
<div class="m2"><p>اگر جان ببخشی اگر دلستانی</p></div></div>