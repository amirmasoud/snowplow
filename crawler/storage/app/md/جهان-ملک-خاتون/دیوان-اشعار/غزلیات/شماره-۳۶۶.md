---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>از بوی گلم دماغ بگرفت</p></div>
<div class="m2"><p>زان روی دلم به باغ بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشکست دماغ من ز سودا</p></div>
<div class="m2"><p>بی یار ز باغ و راغ بگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ظلمت هجرتم گرفتار</p></div>
<div class="m2"><p>وصل تو شبی چراغ بگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق تو چو بر دلم فزون شد</p></div>
<div class="m2"><p>حسن تو چنین به داغ بگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بوی گل از چمن برون شد</p></div>
<div class="m2"><p>سرتاسر باغ و راغ بگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانی به جهان که سینه ی جان</p></div>
<div class="m2"><p>از دست فراق داغ بگرفت</p></div></div>