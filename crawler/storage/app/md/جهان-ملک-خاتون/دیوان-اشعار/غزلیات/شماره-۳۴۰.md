---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>دل من در فراق شیداییست</p></div>
<div class="m2"><p>خسته از درد ناشکیباییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پریشان شدست معذورست</p></div>
<div class="m2"><p>دایماً زان دو زلف سوداییست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق او را چگونه شرح دهم</p></div>
<div class="m2"><p>بی سخن در کمال زیباییست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قامتش را چه نسبت است به سرو</p></div>
<div class="m2"><p>اعتدالی به حدّ رعناییست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم وصف نرگس رعناش</p></div>
<div class="m2"><p>گرچه در عین مجلس آراییست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دو دیده که روز و شب حیران</p></div>
<div class="m2"><p>در رخت دیده ی تماشاییست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا دوتا زلف تو به دست آمد</p></div>
<div class="m2"><p>جامه ی دل ز شوق یکتاییست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست افتادگان بگیر از غم</p></div>
<div class="m2"><p>چون تو را قدرت تواناییست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دل خسته را به رغم جهان</p></div>
<div class="m2"><p>سر دیوانگی و شیداییست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظر کردگار می دانی</p></div>
<div class="m2"><p>که نه بر جاهلی و داناییست</p></div></div>