---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>به رویت اشتیاقم بی نهایت</p></div>
<div class="m2"><p>بود گر سویم اندازی عنایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جفا کردی بسی بر من نگارا</p></div>
<div class="m2"><p>نپیچیدم سر از مهر و وفایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که جان من ز هجران بر لب آمد</p></div>
<div class="m2"><p>بگو جانا اگر اینست رایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو سلطان جهانداری خدا را</p></div>
<div class="m2"><p>نظر کن یک زمان سوی گدایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی از دشمن و از دوست دیدم</p></div>
<div class="m2"><p>جفا و جور و خواری از برایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو هم گر زانکه گردانی چو پرگار</p></div>
<div class="m2"><p>نگردانم سر از فرمان و رایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آب زندگانی بخشدم خضر</p></div>
<div class="m2"><p>نخواهم آن و خواهم خاک پایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر بر من نداری تا دگربار</p></div>
<div class="m2"><p>چه کردند از من مسکین روایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندارم صبر دل کز حد ببردی</p></div>
<div class="m2"><p>جفا و جور را هم هست غایت</p></div></div>