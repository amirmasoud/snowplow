---
title: >-
    شمارهٔ ۶۸۲
---
# شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>شب نیست کز غمت جگرم خون نمی‌شود</p></div>
<div class="m2"><p>وز راه دیده‌ام همه بیرون نمی‌شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگم چو کهرباست ولی از سرشک چشم</p></div>
<div class="m2"><p>آن نیست کز فراق تو گلگون نمی‌شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرشب مرا به وعدهٔ وصلش دهد امید</p></div>
<div class="m2"><p>وآن نیز هم به طالع وارون نمی‌شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم قدی به سان الف در فراق تو</p></div>
<div class="m2"><p>باور مکن که چون صفت نون نمی‌شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن زلف کافرش که چو افعیست پیچ پیچ</p></div>
<div class="m2"><p>مشکل در آن که رام به افسون نمی‌شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم میسّرم شود ای دوست روز وصل</p></div>
<div class="m2"><p>تدبیر و چاره چیست کنون چون نمی‌شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل غم جهان تو از این بیشتر مخور</p></div>
<div class="m2"><p>چون اقتضای دور دگرگون نمی‌شود</p></div></div>