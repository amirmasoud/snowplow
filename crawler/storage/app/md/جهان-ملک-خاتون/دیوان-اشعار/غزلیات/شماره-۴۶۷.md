---
title: >-
    شمارهٔ ۴۶۷
---
# شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>در جهان ما دلبری خواهیم کرد</p></div>
<div class="m2"><p>وز جهانی دلبری خواهیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای دل در بند غم خواهیم داد</p></div>
<div class="m2"><p>ما نه کاری سرسری خواهیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه چون مور ضعیفم ناتوان</p></div>
<div class="m2"><p>با پلنگان همسری خواهیم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ندارم بی وصالت خواب و خور</p></div>
<div class="m2"><p>با خیالت داوری خواهیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در فراقت صبحدم در کوهسار</p></div>
<div class="m2"><p>ناله چون کلک دری خواهیم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر رقیبان حمله ای همچون خلیل</p></div>
<div class="m2"><p>بر بتان آزری خواهیم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندگی کردیم اگر درخور فتد</p></div>
<div class="m2"><p>بعد از این ما چاکری خواهیم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خیالت در جهان بین آمدم</p></div>
<div class="m2"><p>جان و دل را یاوری خواهیم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گریه در شوق رخی همچون نگار</p></div>
<div class="m2"><p>همچو ابر آذری خواهیم کرد</p></div></div>