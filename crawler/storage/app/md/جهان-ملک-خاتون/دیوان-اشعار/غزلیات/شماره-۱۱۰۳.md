---
title: >-
    شمارهٔ ۱۱۰۳
---
# شمارهٔ ۱۱۰۳

<div class="b" id="bn1"><div class="m1"><p>در راه عشق روی تو ما بی خبر رویم</p></div>
<div class="m2"><p>مجنون صفت همیشه به کوه و کمر رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راهیست پیچ پیچ چو زلف بتان دراز</p></div>
<div class="m2"><p>آن به بود که با رخ او در قمر رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بویی ز وصل او به مشامم نمی رسد</p></div>
<div class="m2"><p>ای دل بیا که تا قدمی پیشتر رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی خدنگ غمزه دلدار قاتلست</p></div>
<div class="m2"><p>در پیش ناوک غم او جان سپر رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمش بلای خلق جهانست چون کنم</p></div>
<div class="m2"><p>شاید که از بلا قدری دورتر رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون در دیار خویش نداریم رونقی</p></div>
<div class="m2"><p>ای دل بیا بیا که به شهری دگر رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنمای روی مهوشت ای عمر نازنین</p></div>
<div class="m2"><p>تا از شعاع روی تو از خود بدر رویم</p></div></div>