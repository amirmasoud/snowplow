---
title: >-
    شمارهٔ ۱۳۶۱
---
# شمارهٔ ۱۳۶۱

<div class="b" id="bn1"><div class="m1"><p>دلی ز دست بدادم ز روی نادانی</p></div>
<div class="m2"><p>ز دست جور تو خوردم بسی پشیمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریختی به ستم خون دل ز دیده ما</p></div>
<div class="m2"><p>کنون به گردن تو خون ماست تا دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه دادن جان مشکلست در هجران</p></div>
<div class="m2"><p>تو رخ نمای که تا جان دهم به آسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر کشند به چین صورت نگار به دست</p></div>
<div class="m2"><p>بگو که صورت جان کی کشد چنین مانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال دوست درآمد به دیده می گفتم</p></div>
<div class="m2"><p>اگرچه هست خیالم به دوست می مانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ز روی تو زین بیش صبر و طاقت نیست</p></div>
<div class="m2"><p>بیا که بر دل پردرد من تو درمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا و چاره کار جهان بجوی به لطف</p></div>
<div class="m2"><p>وگرنه هم به غم حال خویش درمانی</p></div></div>