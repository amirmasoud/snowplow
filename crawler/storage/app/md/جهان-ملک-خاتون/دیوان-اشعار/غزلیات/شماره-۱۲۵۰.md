---
title: >-
    شمارهٔ ۱۲۵۰
---
# شمارهٔ ۱۲۵۰

<div class="b" id="bn1"><div class="m1"><p>آن چه زلفست آن که باز از قهر تابش داده‌ای</p></div>
<div class="m2"><p>وآن چه نرگس‌های مستست آن که خوابش داده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه پیکانست بر جانم بگو زان غمزه‌ها</p></div>
<div class="m2"><p>گوییا از بی‌وفایی زهر نابش داده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بگشادم که تا بینم جمال آفتاب</p></div>
<div class="m2"><p>ای دو چشم من چرا بر رخ نقابش داده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان لب چو نوشدارو هر دو چشم پرخمار</p></div>
<div class="m2"><p>از دل مجروح من گویی کبابش داده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خون خوارت بسی خوردست خوناب جگر</p></div>
<div class="m2"><p>نیک سرمستست دل زان لب شرابش داده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن نهال قامتش را بین چو سرو بوستان</p></div>
<div class="m2"><p>ای دل مسکین مگر از دیده آبش داده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به چشم جان سؤال از روز وصلت کرده بود</p></div>
<div class="m2"><p>گفته ای لالا و از ابرو جوابش داده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای طبیب من ز لعلت بوسه‌ای می‌خواست دل</p></div>
<div class="m2"><p>در جهان وز خون دل دردم جوابش داده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهد وصلت چون شود بیرون به کام دل که تو</p></div>
<div class="m2"><p>شربتی شیرین تو از لعل مذابش داده‌ای</p></div></div>