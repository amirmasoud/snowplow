---
title: >-
    شمارهٔ ۴۲۷
---
# شمارهٔ ۴۲۷

<div class="b" id="bn1"><div class="m1"><p>دلم هجر تو یارا برنتابد</p></div>
<div class="m2"><p>فراقت سنگ خارا برنتابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن بر وعده ام زین بیش دلشاد</p></div>
<div class="m2"><p>کزین پس دل مدارا برنتابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزن زین بیش بر دل تیغ هجرم</p></div>
<div class="m2"><p>که از دستت نگارا برنتابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان آمد دلم از جور زین بیش</p></div>
<div class="m2"><p>ستم از تو خدا را برنتابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو جوری می کنی بر من ز حد بیش</p></div>
<div class="m2"><p>دلم زین بیش یارا برنتابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهان می کن دلا اسرار عشقش</p></div>
<div class="m2"><p>که رازش آشکارا برنتابد</p></div></div>