---
title: >-
    شمارهٔ ۱۱۲۳
---
# شمارهٔ ۱۱۲۳

<div class="b" id="bn1"><div class="m1"><p>ای سخت گمان سست پیمان</p></div>
<div class="m2"><p>تا چند زنی مرا به پیکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تیر جفا دلم بخستی</p></div>
<div class="m2"><p>جز مرهم وصل نیست درمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سرو روان و مونس دل</p></div>
<div class="m2"><p>بازآ ز درم دمی خرامان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیراهن صبر را کنم چاک</p></div>
<div class="m2"><p>هر شب ز غم تو تا گریبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست دل زار زار تنگم</p></div>
<div class="m2"><p>ای دوست کجا رسد به دامان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بازآی که عاشقان رویت</p></div>
<div class="m2"><p>در هجر تو بی سرند و سامان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشکل همه آنکه دولت وصل</p></div>
<div class="m2"><p>یک روز نگشت بر من آسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر وصل تو دست من گرفتی</p></div>
<div class="m2"><p>در پای تو کردمی دل و جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر جان جهان ستم بتا رفت</p></div>
<div class="m2"><p>از جور و جفای تو فراوان</p></div></div>