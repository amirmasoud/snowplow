---
title: >-
    شمارهٔ ۵۲۵
---
# شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>مرا دردی بود در دل که از وصلش دوا باشد</p></div>
<div class="m2"><p>دوای درد دوری را مگر لطف شما باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا یاریست بی همتا ندارد در جهان مانند</p></div>
<div class="m2"><p>چنین یاری نمی دانم که در عالم که را باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دولت خانه ی وصلت فتادم در شب هجران</p></div>
<div class="m2"><p>نگارینا چنین ظلمی بر این مسکین چرا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان مجمع رندان همی خواهم که بنشیند</p></div>
<div class="m2"><p>به شرطی کان بت مه رو به تنها زان ما باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به درد دل گرفتارم من سرگشته بی وصلش</p></div>
<div class="m2"><p>بود با دیگری شاد او مسلمانی کجا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا چون جان بود در تن ملول از ما چرا گردد</p></div>
<div class="m2"><p>نگویی یار سنگین دل جدا از ما چرا باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به صبح و شام می گویم دعای دولتت دایم</p></div>
<div class="m2"><p>دعای صادقان در شأن یاران بی ریا باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نظر فرما به محتاجان ز روی صورت و معنی</p></div>
<div class="m2"><p>خصوصاً بر دلی محزون که از غم مبتلا باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شیرش در شده خوبی مگر با جان برون آید</p></div>
<div class="m2"><p>گدا گر خود شود سلطان گدا را خو گدا باشد</p></div></div>