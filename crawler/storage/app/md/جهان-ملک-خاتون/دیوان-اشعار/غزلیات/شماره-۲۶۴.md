---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>جز غم به جهان نصیب ما نیست</p></div>
<div class="m2"><p>جز غصّه کسی قریب ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستیم محبّ خاک کویش</p></div>
<div class="m2"><p>گویی به جهان حبیب ما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از درد به لب رسید جانم</p></div>
<div class="m2"><p>رحمی به دل طبیب ما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد طوطی خوش کلام اگر هست</p></div>
<div class="m2"><p>خوبیش چو عندلیب ما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق رخ تو ای نگارین</p></div>
<div class="m2"><p>کس نیست که او رقیب ما نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکین دل من غریب و عاشق</p></div>
<div class="m2"><p>کس نیست که او رقیب ما نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خان وصال او بسی کس</p></div>
<div class="m2"><p>هستند ولی نصیب ما نیست</p></div></div>