---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>مستغرق بحر غم عشقیم نگارا</p></div>
<div class="m2"><p>خود حال نپرسی که چه شد غرقه ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست به فریاد دل خستهٔ ما رس</p></div>
<div class="m2"><p>بفرست نوایی من بی برگ و نوا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نور دو چشمم به غلط وعده وفا کن</p></div>
<div class="m2"><p>تا چند تحمّل بتوان کرد جفا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی تو که از دفتر ایام بشستند</p></div>
<div class="m2"><p>در عهد تو ای جان و جهان نام وفا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ز آنکه تو را میل من خسته نباشد</p></div>
<div class="m2"><p>از لطف نظر کن به من خسته خدا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالای تو بالا نتوان گفت بلاییست</p></div>
<div class="m2"><p>یارب تو بگردان ز دل خلق بلا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطان جهانی و جهانست به کامت</p></div>
<div class="m2"><p>بنواز زمانی ز سر لطف گدا را</p></div></div>