---
title: >-
    شمارهٔ ۱۳۸۲
---
# شمارهٔ ۱۳۸۲

<div class="b" id="bn1"><div class="m1"><p>ای صبا آخر چرا افتان و خیزان می‌روی</p></div>
<div class="m2"><p>زود بشتاب ار به کاری سوی جانان می‌روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال یعقوب ستمکش پیش یوسف بازگوی</p></div>
<div class="m2"><p>نیک می‌دانی تو حالم چون ز کنعان می‌روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد بی‌درمان ما را گر توانی هم به لطف</p></div>
<div class="m2"><p>چاره‌ای کن چاره‌ای چون پیش درمان می‌روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصّه سوز درون بلبل شوریده‌دل</p></div>
<div class="m2"><p>لطف کن با گل بگو چون سوی بستان می‌روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حالت حزن دل تنگم ز تو پوشیده نیست</p></div>
<div class="m2"><p>یک به یک با او بگو کز بیت احزان می‌روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دل سرگشته‌ام گو تا به کی در کوی دوست</p></div>
<div class="m2"><p>با دل پرآتش و با چشم گریان می‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت حال خرابی جهان را عرضه دار</p></div>
<div class="m2"><p>چون به نزد مالک ملک سلیمان می‌روی</p></div></div>