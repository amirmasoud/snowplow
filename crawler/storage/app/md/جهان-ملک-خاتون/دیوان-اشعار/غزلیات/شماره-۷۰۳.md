---
title: >-
    شمارهٔ ۷۰۳
---
# شمارهٔ ۷۰۳

<div class="b" id="bn1"><div class="m1"><p>تو را به کون و مکان سر فرو نمی آید</p></div>
<div class="m2"><p>مرا دلی که صبوری از او نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر جهان همه حور بهشت خواهد بود</p></div>
<div class="m2"><p>چه چاره چون به خیالم جز او نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر به روضه خلدم قصور عرضه کنند</p></div>
<div class="m2"><p>به جان تو که به چشمم نکو نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی نمی گذرد بر من از غم هجران</p></div>
<div class="m2"><p>که خون دیده به رویم فرو نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از این بلای دل و دیده جفاکش من</p></div>
<div class="m2"><p>عجب که مردم چشمم برو نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آب دیده توان دید خون چشمم را</p></div>
<div class="m2"><p>به قطره قطره ی خون کاو فرو نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر جهان همه پر چشمه حیات شود</p></div>
<div class="m2"><p>به غیر خون دل ما به جو نمی آید</p></div></div>