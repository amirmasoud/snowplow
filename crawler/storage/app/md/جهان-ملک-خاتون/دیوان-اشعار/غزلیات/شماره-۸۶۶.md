---
title: >-
    شمارهٔ ۸۶۶
---
# شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>به جان رسید دل من ز گردش افلاک</p></div>
<div class="m2"><p>شدست جامه صبرم ز دست هجران چاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نوشداروی وصلم به جان رسان ورنی</p></div>
<div class="m2"><p>چو جان رسید به لب حاصلم چه از تریاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه ترک من خسته دل نگار بگفت</p></div>
<div class="m2"><p>به ترک دوست نگویم به جان او حاشاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر کنم گذری بر دلش عجب نبود</p></div>
<div class="m2"><p>چرا که در دل دریا گذر کند خاشاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلال وصل توأم در دهان جان باشد</p></div>
<div class="m2"><p>هزار سال چو خفتم هنوز در دل خاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به وعده وصلت امید خواهد بود</p></div>
<div class="m2"><p>مرا ز طعنه بدگوی و از رقیب چه باک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکار تیغ فراقت مگر منم به جهان</p></div>
<div class="m2"><p>چو کُشتیم به جفا هم ببند بر فتراک</p></div></div>