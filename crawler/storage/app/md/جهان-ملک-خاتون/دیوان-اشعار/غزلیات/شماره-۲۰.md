---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>به دوش برمفکن آن دو زلف مشکین را</p></div>
<div class="m2"><p>مکش به تیغ جفا عاشقان مسکین را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باشد ار به شب وصل شاد گردانی</p></div>
<div class="m2"><p>ز لطف خاطر، بیچارگان غمگین را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز غبن عنبر سارا چو موم بگدازد</p></div>
<div class="m2"><p>اگر تو باز گشایی دو زلف پرچین را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رنگ و بوی چنین گر به بوستان گذری</p></div>
<div class="m2"><p>خجل کنی به چمن ارغوان و نسرین را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشام جان و جهانی یقین برآساید</p></div>
<div class="m2"><p>اگر به شانه زنی زلف عنبرآگین را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کرده ام گنهی در جهان بگو با تو</p></div>
<div class="m2"><p>بجز وفا که کمر بسته ای چنین کین را</p></div></div>