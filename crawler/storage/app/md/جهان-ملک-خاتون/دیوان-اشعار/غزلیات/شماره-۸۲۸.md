---
title: >-
    شمارهٔ ۸۲۸
---
# شمارهٔ ۸۲۸

<div class="b" id="bn1"><div class="m1"><p>گر زنم آهی بسوزم خرمنش</p></div>
<div class="m2"><p>ای مسلمانان بگویید از منش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به حالم رحمتی آرد کنون</p></div>
<div class="m2"><p>ورنه در عشقش بگیرم دامنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بدیدم حسن رویش را به دل</p></div>
<div class="m2"><p>گفتم الحمدی بدم پیرامنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صبا گر سوی کنعان بگذری</p></div>
<div class="m2"><p>نزدم آور بویی از پیراهنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم عنایت بودمی ای کاجکی</p></div>
<div class="m2"><p>تا به دیده رفتمی من مأمنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست دل نگذارم از زلف دوتا</p></div>
<div class="m2"><p>ای صبا من، تا نگردی ضامنش</p></div></div>