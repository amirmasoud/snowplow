---
title: >-
    شمارهٔ ۸۹۲
---
# شمارهٔ ۸۹۲

<div class="b" id="bn1"><div class="m1"><p>من بی دل چکنم پیش که گویم غم دل</p></div>
<div class="m2"><p>که ز عشق تو بجز غصّه ندارم حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای صبا حال دل من بر دلدار بگوی</p></div>
<div class="m2"><p>که جهانی ز غم عشق تو شد لایعقل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غافل از یاد تو یک لحظه نیم تا دانی</p></div>
<div class="m2"><p>زینهار از من دلخسته نباشی غافل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طمع دانه کند مرغ که در دام افتد</p></div>
<div class="m2"><p>ورنه در دام غم و غصّه نیاید عاقل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق را میل به حوران بهشتی باشد</p></div>
<div class="m2"><p>چه کنم نیست مرا جز به تو خاطر مایل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وصال تو بس امّید وفا بود مرا</p></div>
<div class="m2"><p>آه کاندیشه غلط بود و تصوّر باطل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قیامت برد از عشق جهان حسرتها</p></div>
<div class="m2"><p>که به تشریف وصال تو نگردد واصل</p></div></div>