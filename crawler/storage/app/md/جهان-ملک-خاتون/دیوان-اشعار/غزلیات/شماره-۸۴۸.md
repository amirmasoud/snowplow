---
title: >-
    شمارهٔ ۸۴۸
---
# شمارهٔ ۸۴۸

<div class="b" id="bn1"><div class="m1"><p>به غیر سوز و گدازیم چاره نیست چو شمع</p></div>
<div class="m2"><p>نزار و زارم و گریان ز غم میانه جمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فراغتی ز من و حال زار من داری</p></div>
<div class="m2"><p>مگر نمی رسدت حال زار بنده به سمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه کرد غم هجر دوست قلع مرا</p></div>
<div class="m2"><p>نمی کند غم عشقش دل من از جان قمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم مدام به بالین دوست تا دم صبح</p></div>
<div class="m2"><p>ز درد هجر عزیزان نزار و زار چو شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو چرخ سفله ببین کاو چگونه قلاّشست</p></div>
<div class="m2"><p>که فرق می نکند نقره را کنون از قلع</p></div></div>