---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>دلم ز غمزه ی شوخش حذر نخواهد کرد</p></div>
<div class="m2"><p>هوای زلف وی از سر بدر نخواهد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه می گذرد عمر در غمش لیکن</p></div>
<div class="m2"><p>دل من از سر کویش سفر نخواهد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببست عهد بسی با من ضعیف نحیف</p></div>
<div class="m2"><p>وفا به عهد همانا دگر نخواهد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جان دوست که بی روی دوست دیده ی من</p></div>
<div class="m2"><p>نظر ز مهر به مهر و قمر نخواهد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقین ز پای درآمد دلم بنومیدی</p></div>
<div class="m2"><p>اگر دو دست مرادم کمر نخواهد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو شکّر شیرین به لطف بگشایی</p></div>
<div class="m2"><p>دل التفات دگر بر شکر نخواهد کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هیچ روی نظر بر من شکسته نکرد</p></div>
<div class="m2"><p>مگر که دود دل ما اثر نخواهد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرش چو سگ تو برانی دو صد ره از در خویش</p></div>
<div class="m2"><p>دل التفات به جای دگر نخواهد کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرش ز ناوک دلدوز تو به هم دوزی</p></div>
<div class="m2"><p>به غیر جان و جهان را سپر نخواهد کرد</p></div></div>