---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>بتا تا مهر روی تو دلم با مهربانی جست</p></div>
<div class="m2"><p>درخت قامتت گویی میان دیده ما رست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نو زین حالت خبر داری که یار مهربان من</p></div>
<div class="m2"><p>نداند بنده پروردن ولی در دلربایی چست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تاب روز هجرانت ببین کاین مردم دیده</p></div>
<div class="m2"><p>به جان آمد ز غمخواری به خون دیده رخ را شست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درخت قامت سروش کشیده سر به رعنایی</p></div>
<div class="m2"><p>ولی خاک تن مسکین به قدش مهربانی جست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسلمانان به جان آمد دل من از جفای یار</p></div>
<div class="m2"><p>که ننمود او وفا با ما و جور و دلبربایی جست</p></div></div>