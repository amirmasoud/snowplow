---
title: >-
    شمارهٔ ۱۰۴۶
---
# شمارهٔ ۱۰۴۶

<div class="b" id="bn1"><div class="m1"><p>دل ز تنهایی به جان آمد ندانم چون کنم</p></div>
<div class="m2"><p>هر زمان از آتش دل دیده را پر خون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دو زلف کافرت خود سر فرو نارد به ما</p></div>
<div class="m2"><p>ای نگار ماه رخ گر صد هزار افزون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شب هجران ز روی چون زر و سیماب اشک</p></div>
<div class="m2"><p>گر تو می خواهی جهانی را از آن قارون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من جهان بین را ز بهر دیدنت خواهم ولی</p></div>
<div class="m2"><p>گر تو فرمایی ز راه حسرتش بیرون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کنم از بی وفا دلبر شکایت با کسی</p></div>
<div class="m2"><p>گر کنم هم شکوه ای از طالع وارون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ببارم اشک خونین از جفایش دور نیست</p></div>
<div class="m2"><p>لیک از آن ترسم که ناگه عالمی جیحون کنم</p></div></div>