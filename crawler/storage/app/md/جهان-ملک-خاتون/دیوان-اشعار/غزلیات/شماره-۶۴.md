---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>چون غنیمت بود شب مهتاب</p></div>
<div class="m2"><p>وصل ما را ز لطف خود دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو به خواب خوشی بگو ز چه روی</p></div>
<div class="m2"><p>بر دو چشمم ببسته ای ره خواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند نالم ز درد عشق رخت</p></div>
<div class="m2"><p>چند ریزم ز دیدگان خوناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرط نبود که در مسلمانی</p></div>
<div class="m2"><p>من خورم خون و دیگران می ناب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سر آب خوش نشسته به عیش</p></div>
<div class="m2"><p>دلبر بی وفا و ما به سراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر ز پا پا ز سر نمی دانم</p></div>
<div class="m2"><p>شده در آب دیدگان غرقاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد دل با طبیب خود گفتم</p></div>
<div class="m2"><p>خود ندادم به هیچ گونه جواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل به درد فراق بنهادم</p></div>
<div class="m2"><p>گفتم این دیده ای مگر تو ثواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مرا خون دل به دیده کنی</p></div>
<div class="m2"><p>وز دو لعلم نمی دهی عنّاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرو نازا بناز در بستان</p></div>
<div class="m2"><p>بیش از این از جهان تو روی متاب</p></div></div>