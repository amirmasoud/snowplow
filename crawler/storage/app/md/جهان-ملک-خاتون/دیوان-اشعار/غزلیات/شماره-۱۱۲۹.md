---
title: >-
    شمارهٔ ۱۱۲۹
---
# شمارهٔ ۱۱۲۹

<div class="b" id="bn1"><div class="m1"><p>در چمن تا قد آن سرو روانست روان</p></div>
<div class="m2"><p>خونم از دیده غمدیده روانست روان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه آید سوی ما هم به نثار قدمش</p></div>
<div class="m2"><p>پیش او جان و تن و روح روانست روان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دل و جان و تن و روح نباشد درخور</p></div>
<div class="m2"><p>نقد این جمله بگوئیم روانست روان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زندگی بی تو نخواهیم که این جان عزیز</p></div>
<div class="m2"><p>در تنم بی رخ تو بار گرانست گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بودم اندیشه که او ترک جفا خواهد کرد</p></div>
<div class="m2"><p>آن جفاپیشه چو دیدیم همانست همان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میل او جمله سوی ما به جفا بود و ستم</p></div>
<div class="m2"><p>گرچه کردیم وفا باز برآنست بر آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میل ما گرچه نداری نکنم قطع طمع</p></div>
<div class="m2"><p>زآنکه در باغ جهان سرو چمانست چمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غایب از چشم من دلشده زنهار مشو</p></div>
<div class="m2"><p>که تو جانی و جهان زنده به جانست به جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل و جان دادم و مهرت بخریدم آخر</p></div>
<div class="m2"><p>سر به سر سود من خسته زیانست زیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه از کار جهان چشم وفا نتوان داشت</p></div>
<div class="m2"><p>در جهان یار وفادار جهانست جهان</p></div></div>