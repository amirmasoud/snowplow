---
title: >-
    شمارهٔ ۹۸۶
---
# شمارهٔ ۹۸۶

<div class="b" id="bn1"><div class="m1"><p>بیا جانا که جانت را بمیرم</p></div>
<div class="m2"><p>وگر میرم به جان منّت پذیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بر خاکم افتد سایه تو</p></div>
<div class="m2"><p>برآرم دست و دامانت بگیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از هجران به جان آمد که از جان</p></div>
<div class="m2"><p>گزیرم هست و از تو ناگزیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلاص من مجویید ای رفیقان</p></div>
<div class="m2"><p>که من در قید مهر او اسیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظر گفتند داری با فقیران</p></div>
<div class="m2"><p>من مسکین شیدا هم فقیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی آید به کویت ناله من</p></div>
<div class="m2"><p>که گوش چرخ کر گشت از نفیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر یک شب در آغوش من آیی</p></div>
<div class="m2"><p>بمیرم پیشت و هرگز نمیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مردی پای دارم چون نشانه</p></div>
<div class="m2"><p>وگر خواهد زدن هردم به تیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همی ترسم جهان بر من سرآید</p></div>
<div class="m2"><p>به درد هجر و در حسرت بمیرم</p></div></div>