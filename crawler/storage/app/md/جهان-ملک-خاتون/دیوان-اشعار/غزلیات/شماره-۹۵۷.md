---
title: >-
    شمارهٔ ۹۵۷
---
# شمارهٔ ۹۵۷

<div class="b" id="bn1"><div class="m1"><p>تو می دانی که جز تو کس ندارم</p></div>
<div class="m2"><p>بدین امید روزی می گذارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی دانم چرا چون زلف خوبان</p></div>
<div class="m2"><p>مسلمانان پریشان روزگارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از دستم برفت اندر فراقش</p></div>
<div class="m2"><p>شبی نگرفت دست دل نگارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بر دل ز هجران بار بسیار</p></div>
<div class="m2"><p>بده باری مرا در وصل بارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که در هجران رویت ای دلارام</p></div>
<div class="m2"><p>همیشه خون دل از دیده بارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه عاشقان بسیار دارد</p></div>
<div class="m2"><p>بگو آخر که من کی در شمارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بستان جهان گر خود گلی هست</p></div>
<div class="m2"><p>منش از آب دیده باز بارم</p></div></div>