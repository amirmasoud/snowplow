---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>میان دو دیده مرا جای تست</p></div>
<div class="m2"><p>درون دل خسته مأوای تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خسته با عشق دارم سری</p></div>
<div class="m2"><p>از آن سر همه پر ز سودای تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب به زاری زارم ز هجر</p></div>
<div class="m2"><p>همه روز در شهر غوغای تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بر شب وصلت ار دست نیست</p></div>
<div class="m2"><p>سری دارم ای دوست در پای تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر جان ستانی وگر دل دهی</p></div>
<div class="m2"><p>چه یارا مرا رای من رای تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بستان جان ای بت گلعذار</p></div>
<div class="m2"><p>چه سروی بود کاو به بالای تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضرورت کشم جور کاندر جهان</p></div>
<div class="m2"><p>نبینم کسی را که همتای تست</p></div></div>