---
title: >-
    شمارهٔ ۱۰۰۰
---
# شمارهٔ ۱۰۰۰

<div class="b" id="bn1"><div class="m1"><p>تو شمعی و منت پروانه باشم</p></div>
<div class="m2"><p>به عشقت در جهان افسانه باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسیر غم به یاد آشنایی</p></div>
<div class="m2"><p>ز وصلت تا به کی بیگانه باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسلسل مانده در زنجیر زلفش</p></div>
<div class="m2"><p>به زاری تا به کی دیوانه باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو بر طرف چمن در شادی و من</p></div>
<div class="m2"><p>بدین سان با غمت همخانه باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن خال سیاه و زلف چون دام</p></div>
<div class="m2"><p>چو مرغی در هوای دانه باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانم تا به کی جان در کف دست</p></div>
<div class="m2"><p>خروشان در پی جانانه باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین دریا که موجش خون دلهاست</p></div>
<div class="m2"><p>به جست و جوی آن دردانه باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان ویران شد از آشوب هجرت</p></div>
<div class="m2"><p>چرا من خود درین ویرانه باشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم بوسی ز لعلت خواست گفتم</p></div>
<div class="m2"><p>اگر فرمان دهد پروانه باشم</p></div></div>