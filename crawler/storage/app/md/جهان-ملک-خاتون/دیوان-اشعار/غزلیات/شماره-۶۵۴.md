---
title: >-
    شمارهٔ ۶۵۴
---
# شمارهٔ ۶۵۴

<div class="b" id="bn1"><div class="m1"><p>مرا خیال تو تا در ضمیر خواهد بود</p></div>
<div class="m2"><p>گمان مبر که ز عشقم گزیر خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای مهر دل من ز جان و دل شب و روز</p></div>
<div class="m2"><p>مدام در پی ماه منیر خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از جفای تو روی از درت نگردانم</p></div>
<div class="m2"><p>که هر چه دوست کند دلپذیر خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا به ماه و ستاره نظر کجا باشد</p></div>
<div class="m2"><p>چو در خیال من آن بی نظیر خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رو به کعبه ی جان کرده ام به پای دلم</p></div>
<div class="m2"><p>تمام خار مغیلان حریر خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزیز من دل من در چه زنخدانت</p></div>
<div class="m2"><p>به سان یوسف مصری اسیر خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر ز روی جوانان جهان نخواهد داشت</p></div>
<div class="m2"><p>گرش هزار غم از چرخ پیر خواهد بود</p></div></div>