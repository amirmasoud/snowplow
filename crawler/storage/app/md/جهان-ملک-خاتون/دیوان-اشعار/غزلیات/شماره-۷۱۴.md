---
title: >-
    شمارهٔ ۷۱۴
---
# شمارهٔ ۷۱۴

<div class="b" id="bn1"><div class="m1"><p>جان من شکسته دل از غم تو به جان رسید</p></div>
<div class="m2"><p>وز غم عشقت ای صنم کارد به استخوان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز غمت شکسته شد جان به هوات بسته شد</p></div>
<div class="m2"><p>ای دل و جان ز دست تو جان به لب جهان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان برسد به لب مرا تا برسم به وصل تو</p></div>
<div class="m2"><p>چون کنم ای نگار من در تو نمی توان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر بدی مرا سپر در غم روی تو ولی</p></div>
<div class="m2"><p>جست ز شست تو روان بر دل ناتوان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناله ی بی شمار من در تو اثر نمی کند</p></div>
<div class="m2"><p>گرچه ز جور عشق صد ناله بر آسمان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه تو چو ابروان راست چو شد به سوی من</p></div>
<div class="m2"><p>سینه سپر کنم روان تیر چو از کمان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سر خوان حسن تو بهره گرفته هرکسی</p></div>
<div class="m2"><p>قسمت من ز عشق تو درد دل و فغان رسید</p></div></div>