---
title: >-
    شمارهٔ ۸۸۴
---
# شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>ای خیال روی تو سلطان دل</p></div>
<div class="m2"><p>خود چه جوری می کند بر جان دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجر رویت خون گشاد از چشم جان</p></div>
<div class="m2"><p>دیده ی بیچاره شد مهمان دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردم از حد رفت ای دلبر مگر</p></div>
<div class="m2"><p>لعل جان بخشت کند درمان دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرض و نام و ننگ من بر باد رفت</p></div>
<div class="m2"><p>عشق تو بردم سر و سامان دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر عید رویت ای زیبا نگار</p></div>
<div class="m2"><p>گشت جان نازنین قربان دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد بر جانم ستم آن دل بسی</p></div>
<div class="m2"><p>چون کنم تا کی برم فرمان دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به جان آمد ز دست دیده ام</p></div>
<div class="m2"><p>گفت چند ایذا کنی درشان دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده او را در بلا می افکند</p></div>
<div class="m2"><p>پس چه باشد این همه تاوان دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل به دریای غمت مستغرقست</p></div>
<div class="m2"><p>عاقبت تا چون شود پایان دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دل و دیده نباشد در جهان</p></div>
<div class="m2"><p>دیده را جز روی تو بستان دل</p></div></div>