---
title: >-
    شمارهٔ ۱۱۹۵
---
# شمارهٔ ۱۱۹۵

<div class="b" id="bn1"><div class="m1"><p>گر کشم بار کسی هم بار تو</p></div>
<div class="m2"><p>ور خورم خاری هم از گلزار تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو را از من فراغت حاصلست</p></div>
<div class="m2"><p>چون فراغت باشدم از کار تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک نظر از لطف گر بر ما کنی</p></div>
<div class="m2"><p>سرچه باشد جان کنم ایثار تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست بوسم زود و در پایت فتم</p></div>
<div class="m2"><p>همچو دامن کوری اغیار تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای گل رنگین منم در صبحدم</p></div>
<div class="m2"><p>از دل و جان بلبل بازار تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به کی در بند دلداران بود</p></div>
<div class="m2"><p>ای دل من جانم از پندار تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ز من بارست بر خاطر ترا</p></div>
<div class="m2"><p>ای عزیز من نخواهم بار تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه شادی بر غم حالم ولی</p></div>
<div class="m2"><p>من شدم از جان و دل غمخوار تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه آزردی مرا از داغ هجر</p></div>
<div class="m2"><p>من نخواهم در جهان آزار تو</p></div></div>