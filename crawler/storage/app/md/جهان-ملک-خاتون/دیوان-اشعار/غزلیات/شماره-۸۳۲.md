---
title: >-
    شمارهٔ ۸۳۲
---
# شمارهٔ ۸۳۲

<div class="b" id="bn1"><div class="m1"><p>بیا تا در برت گیرم چو جان خوش</p></div>
<div class="m2"><p>دهم در پای تو جانا روان خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چوگان دو زلفم زن که چون گوی</p></div>
<div class="m2"><p>دوم در پای اسبت سر دوان خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا در گلستان تا گوشه گیریم</p></div>
<div class="m2"><p>که باشد موسم گل بوستان خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سوی ما خرام ای جان که دانم</p></div>
<div class="m2"><p>بود در پای سرو آب روان خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بانگ بلبل و قمری سحرگاه</p></div>
<div class="m2"><p>نگارا هست طرف گلستان خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای چنگ و عود و ناله نای</p></div>
<div class="m2"><p>نباشد هیچ بی آن دلستان خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر باشد وصال دوست باری</p></div>
<div class="m2"><p>که باشد آن همه با دوستان خوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرو ریزد ز درد روز هجران</p></div>
<div class="m2"><p>ز دیده اشک من چون ناودان خوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان در وقت گل خوش گشت لیکن</p></div>
<div class="m2"><p>مبادا بی تو کس را در جهان خوش</p></div></div>