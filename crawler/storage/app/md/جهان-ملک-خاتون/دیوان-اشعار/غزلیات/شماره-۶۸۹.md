---
title: >-
    شمارهٔ ۶۸۹
---
# شمارهٔ ۶۸۹

<div class="b" id="bn1"><div class="m1"><p>هر دل که نه پردردست آن دل به چه کار آید</p></div>
<div class="m2"><p>وآنکس که نشد عاشق خود چون به شمار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسکین دل تنگ من مشکن به ستم یارا</p></div>
<div class="m2"><p>زنهار نگه دارش روزیت به کار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی اگر از دستم آید که به پاش افتم</p></div>
<div class="m2"><p>یارب چه شبی باشد آن شب که نگار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به دل محزون خونست تو را روزی</p></div>
<div class="m2"><p>زنهار تحمّل کن جوری که ز یار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نومید مشو ای دل شاید که نشاید بود</p></div>
<div class="m2"><p>کاین اختر بخت من روزی به گذار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم تو جهان داری گفتا به ولا ولله</p></div>
<div class="m2"><p>آخر تو بپرس از وی از ماش چه عار آید</p></div></div>