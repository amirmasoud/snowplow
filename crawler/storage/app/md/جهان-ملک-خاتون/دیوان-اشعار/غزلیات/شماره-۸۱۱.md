---
title: >-
    شمارهٔ ۸۱۱
---
# شمارهٔ ۸۱۱

<div class="b" id="bn1"><div class="m1"><p>اگر به خلق نماید رخ جهان آراش</p></div>
<div class="m2"><p>هزار جان گرامی کنند اندر پاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رغم دشمن بیهوده گوی رخ بنمای</p></div>
<div class="m2"><p>که آفتاب نخواهد دو دیده ی خفّاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببرد هوش ز من آن دو نرگس جادو</p></div>
<div class="m2"><p>بریخت خون دل من به غمزه جمّاش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که کرد سنبل تر را به روی گل پرچین</p></div>
<div class="m2"><p>که دید غنچه ی سیراب و لعل گوهرپاش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گل بدید رخش در عرق نشست ز شرم</p></div>
<div class="m2"><p>چو سرو دید قدش درد چید از آن بالاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو قد دوست نروید به بوستان سروی</p></div>
<div class="m2"><p>چو روی او نکشد صورتی دگر نقّاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بوی دوست خرابم چه چشم او مستم</p></div>
<div class="m2"><p>از آن سبب شده ام لاابالی و قلّاش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگوی مطرب خوش گو بیار ساقی می</p></div>
<div class="m2"><p>که ترک زهد بگفتم چو مردم اوباش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غصّه خوردن ما هیچ برنیاید کار</p></div>
<div class="m2"><p>غم جهان مخور ای دل زمانکی خوش باش</p></div></div>