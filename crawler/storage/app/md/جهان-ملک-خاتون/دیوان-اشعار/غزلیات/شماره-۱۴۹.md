---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>تا مرا دیده بر آن زهره جبین افتادست</p></div>
<div class="m2"><p>دلم از دوری او سخت حزین افتادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام حسن رخش دید و در او حیران شد</p></div>
<div class="m2"><p>راستی بر همه آفاق گزین افتادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من خسته دل آخر چه سبب بی جرمی</p></div>
<div class="m2"><p>خشم کردست و بر ابروش به چین افتادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام دل هست جدا دایم از این بی سر و پا</p></div>
<div class="m2"><p>عادت بخت من اینست و چنین افتادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لب لعل دهد شب همه شب کام رقیب</p></div>
<div class="m2"><p>از چه رو با من بیچاره به کین افتادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه ای خواهم از او در عوضش جان خواهد</p></div>
<div class="m2"><p>ای عزیزان چه کنم قصّه بدین افتادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر تو چون رود آخر ز دل و جان جهان</p></div>
<div class="m2"><p>عشق تو با دل من نقش نگین افتادست</p></div></div>