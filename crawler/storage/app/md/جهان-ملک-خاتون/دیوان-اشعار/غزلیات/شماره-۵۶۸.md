---
title: >-
    شمارهٔ ۵۶۸
---
# شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>بر خسته دلان جور از این بیش نباشد</p></div>
<div class="m2"><p>نیش ستم آخر به سر ریش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجروح دل خسته ام از تیغ فراقش</p></div>
<div class="m2"><p>در نوش لبت بهره بجز نیش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکس خورد آخر غم احوال دل خویش</p></div>
<div class="m2"><p>ما را به غم عشق غم خویش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانه به حال من دلداده ببخشود</p></div>
<div class="m2"><p>مشکل که ترحّم به دل خویش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان در تن مهجور من ای نور دو دیده</p></div>
<div class="m2"><p>بی صحبت شیرین تو کاریش نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که کنم جان و جهان در سر کارش</p></div>
<div class="m2"><p>قول من بیچاره کمابیش نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیچاره دلم را ز چه روی ای بت مه روی</p></div>
<div class="m2"><p>در بارگه وصل تو باریش نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بار غم هجران تو مشکل بود امّا</p></div>
<div class="m2"><p>از جور و جفاهای تو یاریش نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سایه انصاف بدارم که جهان را</p></div>
<div class="m2"><p>جز درگه الطاف تو جاییش نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما منتظر لطف تو مگذار که گویند</p></div>
<div class="m2"><p>سلطان جهان را غم درویش نباشد</p></div></div>