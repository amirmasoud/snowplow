---
title: >-
    شمارهٔ ۶۰۹
---
# شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>چو زلف خویش چرا عهد یار بشکستند</p></div>
<div class="m2"><p>چرا به تیغ جفا جان خستگان خستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز محنت شب هجران و اشتیاق وصال</p></div>
<div class="m2"><p>به چشم حسرت ما راه خواب دربستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قسم به روی چو خورشید تو که هشیاران</p></div>
<div class="m2"><p>به بوی زلف تو جانا هنوز سرمستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم دوست که یاران خشم رفته ی ما</p></div>
<div class="m2"><p>به شکل ابروی دلدار باز پیوستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرم برفت ز سودای عاشقان رخش</p></div>
<div class="m2"><p>همیشه داغ غم عشق دوست بربستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فدای روی تو کردند ای صنم دل و جان</p></div>
<div class="m2"><p>از آن جهت ز غم روزگار وارستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمی رود ز خیالم دمی که مردم چشم</p></div>
<div class="m2"><p>مدام دیده ی جان در جمال او بستند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آتش غم عشقت که در جهان افتاد</p></div>
<div class="m2"><p>کنون ز باد هوایت چو خاک ره پستند</p></div></div>