---
title: >-
    شمارهٔ ۱۳۰۴
---
# شمارهٔ ۱۳۰۴

<div class="b" id="bn1"><div class="m1"><p>حاش لله که مرا جز تو بود دلداری</p></div>
<div class="m2"><p>یا دلم غیر غم عشق گزیند یاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم حال من بی دل بخور امروز که نیست</p></div>
<div class="m2"><p>به جهانم بجز از عشق رخت غمخواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زارم از عشق رخ خوب تو دریاب مرا</p></div>
<div class="m2"><p>مکن آزرده خدا را به جفا بازاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش قصد دل و دین من آخر چه سبب</p></div>
<div class="m2"><p>کرده ای، ای دل و دینم تو بگو گفت آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم ای جان ز گلستان وصالت هرگز</p></div>
<div class="m2"><p>چون ندیدم من دلخسته مگر جز خاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چه رو این همه بیداد پسندی بر من</p></div>
<div class="m2"><p>گر نوازیم همانا که نباشد عاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه از حال من خسته جگر بی خبری</p></div>
<div class="m2"><p>جان شیرین به سر عشق تو کردم باری</p></div></div>