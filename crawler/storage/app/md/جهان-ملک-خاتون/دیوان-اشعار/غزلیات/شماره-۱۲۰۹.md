---
title: >-
    شمارهٔ ۱۲۰۹
---
# شمارهٔ ۱۲۰۹

<div class="b" id="bn1"><div class="m1"><p>ای دیده ی بخت جهان در آرزوی روی تو</p></div>
<div class="m2"><p>وی قبله ی جان جهان طاق خم ابروی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نور چشم من صبا آورد بویت سوی ما</p></div>
<div class="m2"><p>عمریست تا جان می دهد مسکین دلم بر بوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند چوگان جفا جانا زنی بر جان ما</p></div>
<div class="m2"><p>دانی که من سرگشته ام مانند گو در کوی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سر و سیم اندام ما زنهار از ما سرمکش</p></div>
<div class="m2"><p>تا بنگرم در قامتت ای دیده ی ما سوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمان تو ترک خطا زلف تو از مشک ختن</p></div>
<div class="m2"><p>دانی که عمری تا شدم از جان و دل هندوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر ز روی مرحمت روزی ز حال من بپرس</p></div>
<div class="m2"><p>باشد که رحمی آیدت ای روی عالم سوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حد بشد بر ما جفا میلی کن آخر سوی ما</p></div>
<div class="m2"><p>مسکین دل بیچاره ام آمد به جان از خوی تو</p></div></div>