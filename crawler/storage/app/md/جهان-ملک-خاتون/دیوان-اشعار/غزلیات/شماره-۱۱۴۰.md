---
title: >-
    شمارهٔ ۱۱۴۰
---
# شمارهٔ ۱۱۴۰

<div class="b" id="bn1"><div class="m1"><p>روی او را کجا توان دیدن</p></div>
<div class="m2"><p>یا گلی از وصال او چیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کی چون قلم توان جانا</p></div>
<div class="m2"><p>گرد کویت به فرق گردیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نگنجد بتا به مذهب عشق</p></div>
<div class="m2"><p>از عزیزان همه جفا دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه ابر خوش بود به چمن</p></div>
<div class="m2"><p>صبحدم همچو غنچه خندیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام دل از جهان خوشست ولیک</p></div>
<div class="m2"><p>نبود هیچ چون جهان دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل یارست از جهان مقصود</p></div>
<div class="m2"><p>نه چو کفّار بت پرستیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در لب جوی و پای گل چه خوشست</p></div>
<div class="m2"><p>با نگاری به سبزه غلطیدن</p></div></div>