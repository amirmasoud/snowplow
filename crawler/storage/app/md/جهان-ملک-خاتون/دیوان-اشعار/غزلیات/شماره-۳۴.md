---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>مرا که فرض شد از جان و دل دعای شما</p></div>
<div class="m2"><p>بگو چگونه بگویم دمی ثنای شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پادشاهی ایران زمینش ننگ آید</p></div>
<div class="m2"><p>کسی که شد به سر کوی غم زدای شما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه رای تو بر درد و غصّه ی دل ماست</p></div>
<div class="m2"><p>به غیر صبر چه تدبیر رای رای شما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر رضای تو بر خون دل بود سهلست</p></div>
<div class="m2"><p>مقدمّست به خون دلم رضای شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان دوست که مرغ دل من غمگین</p></div>
<div class="m2"><p>به هیچ روی ندارد بجز هوای شما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به کلبه ی احزان ما دهی تشریف</p></div>
<div class="m2"><p>میان دیده ی روشن کنیم جای شما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جای جان سر و زر کرده ام فدا لیکن</p></div>
<div class="m2"><p>محقّریست شدم شرمسار پای شما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتمش که جهانی به تیغ هجر مکش</p></div>
<div class="m2"><p>جواب داد که سهلست خون بهای شما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به درد هجر بمردیم آه اگر روزی</p></div>
<div class="m2"><p>به درد ما نرسد نوبت دوای شما</p></div></div>