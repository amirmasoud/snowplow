---
title: >-
    شمارهٔ ۸۴۳
---
# شمارهٔ ۸۴۳

<div class="b" id="bn1"><div class="m1"><p>نگارینا مکن بر من ستم بیش</p></div>
<div class="m2"><p>مزن زین بیش تو بر ریش من نیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز وصلم کام دل می ده خدا را</p></div>
<div class="m2"><p>مجو زین بیشتر کام بداندیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو خویشی بیش ازین ما را میازار</p></div>
<div class="m2"><p>که فرقی باشد از بیگانه تا خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ترکش چون توان کز تیر مژگانش</p></div>
<div class="m2"><p>اگر قربان شوم بهتر درین کیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا داغ فراقت هست بر جان</p></div>
<div class="m2"><p>نمک واجب نباشد بر سر ریش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه گویم مدّعی بد سرانجام</p></div>
<div class="m2"><p>چه با من در میان بودش ازین پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو سلطان جهانداری خدا را</p></div>
<div class="m2"><p>مشو غافل دمی از حال درویش</p></div></div>