---
title: >-
    شمارهٔ ۵۶۷
---
# شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>مبا دردی که درمانش نباشد</p></div>
<div class="m2"><p>فراقی را که پایانش نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرامش باد آن دل ای دلارام</p></div>
<div class="m2"><p>اگر عشق تو در جانش نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرو در راه عشقی ای دل ریش</p></div>
<div class="m2"><p>که آن حدّ بیابانش نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری کاو از غم تو پر ز سوداست</p></div>
<div class="m2"><p>یقین دانی که سامانش نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کاو روی مه رویش را ببیند</p></div>
<div class="m2"><p>چرا در عید قربانش نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی کز روز وصل یار برخورد</p></div>
<div class="m2"><p>فراق دوست آسانش نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانی در فراقت مبتلا شد</p></div>
<div class="m2"><p>بجز وصل تو درمانش نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل از دستش برون بردی چه چاره</p></div>
<div class="m2"><p>چو بر دل حکم و فرمانش نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر نانش دهد چرخ کهن سال</p></div>
<div class="m2"><p>چه حاصل چونکه دندانش نباشد</p></div></div>