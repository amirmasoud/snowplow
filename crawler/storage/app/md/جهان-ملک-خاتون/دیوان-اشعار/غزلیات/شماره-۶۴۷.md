---
title: >-
    شمارهٔ ۶۴۷
---
# شمارهٔ ۶۴۷

<div class="b" id="bn1"><div class="m1"><p>بی تو چشمم جهان نمی بیند</p></div>
<div class="m2"><p>گل وصلت دلم نمی چیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز سر زلف دلکشت صنما</p></div>
<div class="m2"><p>جای دیگر دمی نمی شیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه هستت به جای من دگری</p></div>
<div class="m2"><p>دل من جز غم تو نگزیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ی بختم از خدا خواهد</p></div>
<div class="m2"><p>که شب و روز در رخت بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ببیند قد تو سرو روان</p></div>
<div class="m2"><p>از خجالت به خاک بنشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وآنگه از قد سرکشت به نیاز</p></div>
<div class="m2"><p>ای دلارام درد برچیند</p></div></div>