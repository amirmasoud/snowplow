---
title: >-
    شمارهٔ ۱۲۴۹
---
# شمارهٔ ۱۲۴۹

<div class="b" id="bn1"><div class="m1"><p>دو دیده بر سر راهت نهاده ام بازآی</p></div>
<div class="m2"><p>بلا و غصّه ازین بیش بر جهان مفزای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگیر دست ضعیفم به وصل خویش شبی</p></div>
<div class="m2"><p>مبر چو گیسوی پرتاب خویشتن در پای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه عهد ببستی ولی یقینم بود</p></div>
<div class="m2"><p>که هر سخن که بگویی نیاوری بر جای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا و یک نظر از روی مهر با ما کن</p></div>
<div class="m2"><p>که هست روی چو خورشید تو جهان آرای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قسم به چشم چو آهوی تو که سرمستم</p></div>
<div class="m2"><p>به بوی آن دو سر زلف شوخ غالیه سای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روی و موی تو آنگه به طاق ابرویت</p></div>
<div class="m2"><p>به بوس آن دو لب لعل و شهد شکرخای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر از فراق تو جانم به لب رسید از غم</p></div>
<div class="m2"><p>ز روی لطف ترحّم کن و شبی بازآی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که تا به دیده تحقیق بنگری در ما</p></div>
<div class="m2"><p>که بی تو چون بگدازم به هجر جان فرسای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دست روز فراقت چو چنگ بخروشم</p></div>
<div class="m2"><p>به حسرت شب وصلت ز ناله ام چون نای</p></div></div>