---
title: >-
    شمارهٔ ۷۵۲
---
# شمارهٔ ۷۵۲

<div class="b" id="bn1"><div class="m1"><p>صبا برو ز بر من به سوی آن دلدار</p></div>
<div class="m2"><p>بپرسش از من مسکین خسته دل بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مپیچ در سر زلفین عنبرآسایش</p></div>
<div class="m2"><p>نسیم آن ز سر زلف خود به باد بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو که بر من آشفته حال رحمت کن</p></div>
<div class="m2"><p>که شد به تیغ فراق تو خاطرم افگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خورد و خواب برآمد دلم به هجرانت</p></div>
<div class="m2"><p>ترحّمی بکن آخر به دیده ی خونبار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن که عادت تو نیست مردم آزاری</p></div>
<div class="m2"><p>تو نور دیده ی مایی نخواهمت آزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو فارغی ز من خسته ی پریشان حال</p></div>
<div class="m2"><p>که خوش به خواب صبوحی و من ز غم بیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا به حال جهان التفات می نکنی</p></div>
<div class="m2"><p>به حرمت هر دو جهان را به لطف دوست بدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو حال زار دل ما مگر نمی دانی</p></div>
<div class="m2"><p>که شد دلم به فراق تو از جهان بیزار</p></div></div>