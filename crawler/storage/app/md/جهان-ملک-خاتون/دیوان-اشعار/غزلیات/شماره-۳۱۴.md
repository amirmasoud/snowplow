---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>کدام دل که به داغ فراق بریان نیست</p></div>
<div class="m2"><p>کدام دیده ی جان بی رخ تو گریان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان رسید مرا دل ز روز درد فراق</p></div>
<div class="m2"><p>شب فراق مرا گوییا که پایان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر تو تلخ بگویی ورم برنجانی</p></div>
<div class="m2"><p>به جان تو که مرا تلخ تر ز هجران نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگرچه گشته بعیدم ز روی چون ماهت</p></div>
<div class="m2"><p>کدام جان که به عید رخ تو قربان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو که تا به کیم وعده می دهی بر صبر</p></div>
<div class="m2"><p>عزیز من چه کنم چون دلم به فرمان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سوز عشق تو دردیست بر دلم جانا</p></div>
<div class="m2"><p>که در جهانش بجز وصل دوست درمان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هر طریق که باشد میان مجلس انس</p></div>
<div class="m2"><p>یقین که صعب تر از صحبت گرانجان نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه لاله و گل در جهان بود بسیار</p></div>
<div class="m2"><p>ولی چو بلبل طبعم به شاخساران نیست</p></div></div>