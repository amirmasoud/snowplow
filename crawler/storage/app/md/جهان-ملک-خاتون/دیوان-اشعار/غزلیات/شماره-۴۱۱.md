---
title: >-
    شمارهٔ ۴۱۱
---
# شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>تا دیده ی من بر رخ همچون قمر افتاد</p></div>
<div class="m2"><p>راز دلم از پرده محنت بدر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگر نکند چشم به خورشید جهانتاب</p></div>
<div class="m2"><p>آن را که بدان طلعت چون مه نظر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بوی گذاری که کند بر سر او دوست</p></div>
<div class="m2"><p>چون خاک دل شیفته در ره گذر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوی فراقت صنما عاشق مسکین</p></div>
<div class="m2"><p>دلداده به جان از غم جان بی خبر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن طرّه هندو که به بالات حسد برد</p></div>
<div class="m2"><p>آشفته و سرگشته به کوه و کمر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که به پات افکنم این سر چو بدیدم</p></div>
<div class="m2"><p>پیش قدمت جان و جهان مختصر افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حال دل مجروح من خسته چه پرسی</p></div>
<div class="m2"><p>عمریست که از کار جهان بی خبر افتاد</p></div></div>