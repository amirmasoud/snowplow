---
title: >-
    شمارهٔ ۶۶۳
---
# شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>چون دیده ام به هجر رخت پر ز خون بود</p></div>
<div class="m2"><p>ما را اگر دمی بنوازی تو چون بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسین دل ضعیف من ای نور دیدگان</p></div>
<div class="m2"><p>با درد اشتیاق تو عضوی زبون بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم مگر شبی بنوازی مرا به لطف</p></div>
<div class="m2"><p>نگذاردم که بخت بدم رهنمون بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشتم یقین و طالع خویش آزموده ام</p></div>
<div class="m2"><p>بختم همیشه چون سر زلفت نگون بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مویی شد از خیال رخت تن ز روز هجر</p></div>
<div class="m2"><p>کاهی شود اگرچه کُه بیستون بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذار کاین دلم ز فراقت حزین شود</p></div>
<div class="m2"><p>چون از زبان من به تو صد آفرین بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون قامتت بگو سخنی راست، کج مگو</p></div>
<div class="m2"><p>تا کی قدم ز بار فراقت چو نون بود</p></div></div>