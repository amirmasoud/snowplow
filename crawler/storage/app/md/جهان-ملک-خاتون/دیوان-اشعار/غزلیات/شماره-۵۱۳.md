---
title: >-
    شمارهٔ ۵۱۳
---
# شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>آخر این دردم به درمان کی رسد</p></div>
<div class="m2"><p>نوبت دیدار جانان کی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دل سرگشته ی سودازده</p></div>
<div class="m2"><p>از وصال او به سامان کی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آدم آشفته دل در انتظار</p></div>
<div class="m2"><p>مانده تا پیغام رضوان کی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل چو بلبل زار و نالان در فراق</p></div>
<div class="m2"><p>تا گل رویش به بستان کی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده ی یعقوب بر راه امید</p></div>
<div class="m2"><p>تا دگر یوسف به کنعان کی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردمند عشق را از باغ وصل</p></div>
<div class="m2"><p>غنچه ای بی خار هجران کی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصّه جور غم مور ضعیف</p></div>
<div class="m2"><p>تا به درگاه سلیمان کی رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در وصلش جهانی منتظر</p></div>
<div class="m2"><p>تا غم هجرش به پایان کی رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل فدای عشق او کردم کنون</p></div>
<div class="m2"><p>منتظر تا نوبت جان کی رسد</p></div></div>