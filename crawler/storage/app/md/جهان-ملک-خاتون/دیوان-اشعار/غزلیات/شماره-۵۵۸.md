---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>دل عاشق چرا شیدا نباشد</p></div>
<div class="m2"><p>به عشق اندر جهان رسوا نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگویی تا بکی ای شوخ دلبند</p></div>
<div class="m2"><p>تو را پروای وصل ما نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بستان ملاحت سرو باشد</p></div>
<div class="m2"><p>ولی چون قد او رعنا نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدامین دیده در وی نیست حیران</p></div>
<div class="m2"><p>مگر چشمی که او بینا نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوقش در جهان یکتا شدم من</p></div>
<div class="m2"><p>ولی با ما دلش یکتا نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه دل باشد که باشد غافل از یار</p></div>
<div class="m2"><p>نه سر باشد که پر سودا نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نوعی از جهان دل در تو بستم</p></div>
<div class="m2"><p>که با غیر توأم پروا نباشد</p></div></div>