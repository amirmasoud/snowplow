---
title: >-
    شمارهٔ ۷۳۳
---
# شمارهٔ ۷۳۳

<div class="b" id="bn1"><div class="m1"><p>کس ندیدست سرو در رفتار</p></div>
<div class="m2"><p>نشنیدیم ماه در گفتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس رنگ و بو نشان ندهد</p></div>
<div class="m2"><p>همچو زلف تو مشک در تاتار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه بوی بهار خوش باشد</p></div>
<div class="m2"><p>نیست هرگز چو بوی صحبت یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو به خوابی و فارغ از حالم</p></div>
<div class="m2"><p>چشم جانم چو بخت تو بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای عزیز دلم بگو ز چه روی</p></div>
<div class="m2"><p>گشتی از عاشقان چنین بیزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از وصالت گلی نمی چینم</p></div>
<div class="m2"><p>تا به کی داریم به خار فگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچنان از زمانه در رنجم</p></div>
<div class="m2"><p>که خوشا پار و مرحبا پیرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ دانی دلم چه می خواهد</p></div>
<div class="m2"><p>در چنین موسمی به فصل بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب کشت و کنار جو دو سه روز</p></div>
<div class="m2"><p>در چمن دست ما و دست نگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بانگ رود و سرود و نغمه چنگ</p></div>
<div class="m2"><p>ناله بلبلان خوش گفتار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه کس را درین جهان باشد</p></div>
<div class="m2"><p>آرزوی دل این چنین بسیار</p></div></div>