---
title: >-
    شمارهٔ ۸۷۹
---
# شمارهٔ ۸۷۹

<div class="b" id="bn1"><div class="m1"><p>مراست از دو جهان مهر دوست حاصل دل</p></div>
<div class="m2"><p>جزین چه هست مرا در جهان مداخل دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که گر همه عالم شوند دشمن جان</p></div>
<div class="m2"><p>به دوستی که نشستی تو در مقابل دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال بود مرا کز تو برخورم شبها</p></div>
<div class="m2"><p>زنیم خنده گهی بر خیال باطل دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دست دل بجز از خون نمی خورم چه کنم</p></div>
<div class="m2"><p>ز عشق دوست جزین نیست حاصل دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا که راه وصال تو نیست آسانش</p></div>
<div class="m2"><p>به لطف خویش نظر کن به کار مشکل دل</p></div></div>