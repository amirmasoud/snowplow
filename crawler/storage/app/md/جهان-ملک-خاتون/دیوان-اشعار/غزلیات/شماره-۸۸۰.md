---
title: >-
    شمارهٔ ۸۸۰
---
# شمارهٔ ۸۸۰

<div class="b" id="bn1"><div class="m1"><p>چون ندادم آن ستمگر کام دل</p></div>
<div class="m2"><p>تا به کی باشم چنین در دام دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی بنما با همه جور و جفا</p></div>
<div class="m2"><p>تا شود یک لحظه ام آرام دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای صبا از روی دلداری ببر</p></div>
<div class="m2"><p>پیش آن جان و جهان پیغام دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو من از دست فراقت سوختم</p></div>
<div class="m2"><p>چون درین سودا بپختم خام دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی چون خورشید تو صبح جهان</p></div>
<div class="m2"><p>زلف شبرنگ تو باشد شام دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسته لعل تو باشد عید روح</p></div>
<div class="m2"><p>چشم مخمور جهان بادام دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز وصلت باز محرومم چرا</p></div>
<div class="m2"><p>دشمنم باشد چنین ناکام دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شراب وصل خویشم مست کن</p></div>
<div class="m2"><p>گر ز لعل دوست باشد جام دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست از آغاز عشقت آتشی</p></div>
<div class="m2"><p>در جهان تا چون شود فرجام دل</p></div></div>