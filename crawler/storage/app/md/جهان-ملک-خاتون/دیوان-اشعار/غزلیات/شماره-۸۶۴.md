---
title: >-
    شمارهٔ ۸۶۴
---
# شمارهٔ ۸۶۴

<div class="b" id="bn1"><div class="m1"><p>ای گل رویت بهارستان عشق</p></div>
<div class="m2"><p>وای دل من بلبل بستان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی بر سپهر دلبری</p></div>
<div class="m2"><p>وز رخت پرنور شد ایوان عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لشکر اندیشه بر هم می زند</p></div>
<div class="m2"><p>چشم سرمستت به ترکستان عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردمندانیم در بازار غم</p></div>
<div class="m2"><p>ای لب جان پرورت درمان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاره بیچارگان خسته کن</p></div>
<div class="m2"><p>ای طبیب از لطف در دکّان عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما مگس واریم و آمد در ازل</p></div>
<div class="m2"><p>آیت شهد لبت درمان عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما گدایانیم در خیلت مقیم</p></div>
<div class="m2"><p>پادشاه حسنی و سلطان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاکمی بر ملک اهل عشق خوش</p></div>
<div class="m2"><p>ما همه محکوم و در فرمان عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا به ملک عشق حسنت چاره داد</p></div>
<div class="m2"><p>جان ما شد در جهان حیران عشق</p></div></div>