---
title: >-
    شمارهٔ ۴۴۷
---
# شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>کسی که تخم غمت در میان جان کارد</p></div>
<div class="m2"><p>روا بود که جهان را ز یاد بگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن ستم تو از این بیش نور دیده ی من</p></div>
<div class="m2"><p>که دیده ام ز فراق رخ تو خون بارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طریق دلبر من دلبریست باکی نیست</p></div>
<div class="m2"><p>ولی چو برد دلم را بگو نگه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ببرد و به غم داد و قصد دینم کرد</p></div>
<div class="m2"><p>به غیر دلبر عیار من که آن دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر به رهگذرم بیند آن جفاپیشه</p></div>
<div class="m2"><p>ز ره بگردد و ما را ندیده انگارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر شبی به وصالم نوازد او چه شود</p></div>
<div class="m2"><p>ز جان من ستم روز هجر بردارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تفاوتی نکند گر ز روی لطف و کرم</p></div>
<div class="m2"><p>دمی ز صحبت و عهد قدیم یاد آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به خاطرش آید که بگذرد بر ما</p></div>
<div class="m2"><p>یقین ز طالع خویشم که بخت نگذارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر ستم که کند بر دلم که دامن دوست</p></div>
<div class="m2"><p>ز دست ما نگذاریم او چه پندارد</p></div></div>