---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ای دیده گشته در غم هجر تو چون شراب</p></div>
<div class="m2"><p>وای دل بر آتش غم عشقت شده کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سرو راستی که قدی خوش کشیده ای</p></div>
<div class="m2"><p>در بوستان عشق سر از سوی ما متاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آتش رخت جگر ما کباب شد</p></div>
<div class="m2"><p>وز خون دیده رفت به جام دلم شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشا نقاب از رخ خورشید پیکرت</p></div>
<div class="m2"><p>زان رو که نیست عادت خورشید در نقاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر رخت چو بر من خسته جگر فتاد</p></div>
<div class="m2"><p>سرگشته همچو ذرّه شدم پیش آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشتاب سوی خسته ی هجران خود ببین</p></div>
<div class="m2"><p>کاین عمر بیوفا به چه سان می کند شتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر عزیز در غم ایام صرف شد</p></div>
<div class="m2"><p>خوش عهد کودکی و خوشا موسم شباب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانا به دیده ام تو نگویی که از چه روی</p></div>
<div class="m2"><p>ترکان چشم مست تو بستند راه خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با من خیال یار درآمد به گفت و گوی</p></div>
<div class="m2"><p>گفتا بگو حکایت و بشنو ز من جواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جایی که هست مسکن خیل خیال دوست</p></div>
<div class="m2"><p>آنجا مجال خواب نباشد به هیچ باب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهیم دست بوس تو دریافت چو عنان</p></div>
<div class="m2"><p>باشد که پای بوس تو یابیم چون رکاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر دی به لعل دلکشت ای دوست دل ز ما</p></div>
<div class="m2"><p>کردی به تیر غمزه ی فتان جهان خراب</p></div></div>