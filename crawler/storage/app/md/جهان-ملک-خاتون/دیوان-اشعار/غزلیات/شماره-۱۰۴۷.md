---
title: >-
    شمارهٔ ۱۰۴۷
---
# شمارهٔ ۱۰۴۷

<div class="b" id="bn1"><div class="m1"><p>تا به چند این دیده را در هجر تو جیحون کنم</p></div>
<div class="m2"><p>واین دل بیچاره را در عشق تو پر خون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی ای لیلی صفت در آرزوی روی تو</p></div>
<div class="m2"><p>این دل پر درد را هر ساعتی مجنون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به کی رخ را ز درد هجرت ای زیبانگار</p></div>
<div class="m2"><p>از سرشک دیده ی مهجور خود گلگون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر هزار افسانه خوانم در غم عشق تو من</p></div>
<div class="m2"><p>در نمی گیرد به گوشت ور هزار افسون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدّ بختم چون الف بود از وصالت دلبرا</p></div>
<div class="m2"><p>مدّتی شد تا ز هجر روی تو چون نون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته ای در هجر رویم غیر صبرت چاره نیست</p></div>
<div class="m2"><p>رفت پای طاقت از دستم صبوری چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه آن دلدار را با ما عنایت کمترست</p></div>
<div class="m2"><p>من دعای دولت او هر زمان افزون کنم</p></div></div>