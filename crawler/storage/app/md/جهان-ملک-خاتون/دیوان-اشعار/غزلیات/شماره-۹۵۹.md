---
title: >-
    شمارهٔ ۹۵۹
---
# شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>در حسرت روی آن نگارم</p></div>
<div class="m2"><p>خون جگر از دو دیده بارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایم به غم زمانه در بند</p></div>
<div class="m2"><p>از دست برفت کار و بارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست جفای چرخ باری</p></div>
<div class="m2"><p>آشفته چو زلف آن نگارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم به لب آمد از فراقش</p></div>
<div class="m2"><p>روزی ز غمش هزار بارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون یاد کنم ز روزگاران</p></div>
<div class="m2"><p>کو روز و کجاست روزگارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بودیم عزیز جان و دلها</p></div>
<div class="m2"><p>و امروز چو خاک راه خوارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بردار مرا ز خاک راهت</p></div>
<div class="m2"><p>زنهار چنین روا مدارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کردیم خطا و جرم بسیار</p></div>
<div class="m2"><p>ای دوست به عفو درگذارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دامن لطف دست امّید</p></div>
<div class="m2"><p>آخر تو بگو که چون بدارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسیار کست به جای من هست</p></div>
<div class="m2"><p>من جز تو در این جهان ندارم</p></div></div>