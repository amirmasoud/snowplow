---
title: >-
    شمارهٔ ۹۷۵
---
# شمارهٔ ۹۷۵

<div class="b" id="bn1"><div class="m1"><p>به دوران وصالت غم ندارم</p></div>
<div class="m2"><p>به ریش هجر تو مرهم ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسلمانان به درد روز هجران</p></div>
<div class="m2"><p>گرفتارم ولی همدم ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که گوید حال زارم با تو هم باد</p></div>
<div class="m2"><p>که در عالم جز او محرم ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان مستغرق دریای عشقم</p></div>
<div class="m2"><p>که من پروای خود یک دم ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آن سودا که بود اندر سر ما</p></div>
<div class="m2"><p>به جان تو که آن هم کم ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دستم گر فتد خاکی ز کویت</p></div>
<div class="m2"><p>به دل یادی ز جام جم ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عشقش گر جهان بر من سرآید</p></div>
<div class="m2"><p>ندانی که غمی زان هم ندارم</p></div></div>