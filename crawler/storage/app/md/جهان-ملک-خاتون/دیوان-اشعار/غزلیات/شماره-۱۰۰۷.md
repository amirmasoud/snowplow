---
title: >-
    شمارهٔ ۱۰۰۷
---
# شمارهٔ ۱۰۰۷

<div class="b" id="bn1"><div class="m1"><p>ز گل با روی تو باشد فراغم</p></div>
<div class="m2"><p>چرا با روی تو باشد مرا غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبینم غیر رویت در جهان روی</p></div>
<div class="m2"><p>تویی در عالم ای چشم و چراغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی داغ فراقت در دلم بود</p></div>
<div class="m2"><p>مجدّد کرده ای بر سینه داغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا با روی و قدّت ای دلارام</p></div>
<div class="m2"><p>فراغت باشد از بستان و باغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به وصلت بلبلی بودم ثناخوان</p></div>
<div class="m2"><p>کنون از داغ هجران همچو زاغم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرنه سرو قدّت دربر آید</p></div>
<div class="m2"><p>به دیده در نیاید باغ و راغم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسیم صبح بوی زلفش آورد</p></div>
<div class="m2"><p>ز عنبر تازه شد باری دماغم</p></div></div>