---
title: >-
    شمارهٔ ۳۹۶
---
# شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>ای مردمک دیده تا کی کنی این بیداد</p></div>
<div class="m2"><p>خون جگرم ریزی از دیده که شرمت باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران تو جانم را آورد به لب باری</p></div>
<div class="m2"><p>وز دولت وصل تو یک لحظه نگشتم شاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیرین لب تو هرگز کی داد شبی کامم</p></div>
<div class="m2"><p>وز حسرت روی تو جان داد چنین فرهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن روز که می بستی بر عهد و وفا بندی</p></div>
<div class="m2"><p>با من خردم می گفت عهدیست نه بر بنیاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دادم بده از وصلت ای دوست شبی آخر</p></div>
<div class="m2"><p>ورنی بر دادارم از جور تو خواهم داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریاب دل ما را ورنه دو جهان باری</p></div>
<div class="m2"><p>از دست جفای تو بر باد نخواهم داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستت دل چون پولاد رحمی نبود در وی</p></div>
<div class="m2"><p>تا چند زنم آخر فریاد ز تو فریاد</p></div></div>