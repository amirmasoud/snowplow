---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>تا به کی در دل من درد تو پنهان باشد</p></div>
<div class="m2"><p>تا کیم آتش سودای تو در جان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد ما به نکند هیچ مداوای طبیب</p></div>
<div class="m2"><p>زآنکه او را لب جان بخش تو درمان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشکل اینست که بی روی تو نتوانم زیست</p></div>
<div class="m2"><p>چاره ی درد دلم پیش تو آسان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بیچاره ندارم به جهان جز تو کسی</p></div>
<div class="m2"><p>لیک چون بنده تو را بنده فراوان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گل و لاله نظر کی کند این دیده ی شوخ</p></div>
<div class="m2"><p>هر کجا قامت آن سرو خرامان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به عهدش بکنم جان و جهان جمله فدا</p></div>
<div class="m2"><p>اگر آن عهدشکن با سر پیمان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان و دل را چه محل نام جهان یعنی چه</p></div>
<div class="m2"><p>همه عالم جهت صحبت جانان باشد</p></div></div>