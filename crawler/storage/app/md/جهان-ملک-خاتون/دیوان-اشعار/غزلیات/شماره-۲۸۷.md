---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>مرا به غیر هوای تو، هیچ در سر نیست</p></div>
<div class="m2"><p>بجز وصال رخ تو خیال دیگر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نکهت شب زلفت دماغ ما تر کن</p></div>
<div class="m2"><p>که همچو بوی دو زلف تو هیچ عنبر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبی دراز و چو زلف سیاه و بی سر و پای</p></div>
<div class="m2"><p>مرا بتر که در این شب نگار در بر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بوستان وصالت شدم که گل چینم</p></div>
<div class="m2"><p>به غیر خار فراق تو هیچ در بر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی همچو زرم سکه ای به سیم زدی</p></div>
<div class="m2"><p>زآب دیده همانا که بهتر از زر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مکتب غم عشقم نشانده ای و مرا</p></div>
<div class="m2"><p>به غیر آیت مهر رخ تو از بر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خواب شمع جمال تو دیده ام باری</p></div>
<div class="m2"><p>به مه ندیده ام آن روشنی و در خور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دولت شب وصلت جهان شبی بنواز</p></div>
<div class="m2"><p>مرا فراق تو ای دوست بیش در خور نیست</p></div></div>