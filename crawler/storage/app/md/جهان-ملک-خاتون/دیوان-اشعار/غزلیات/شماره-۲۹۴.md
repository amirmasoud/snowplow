---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>یک دم مرا ز صحبت جانان گزیر نیست</p></div>
<div class="m2"><p>غیر از خیال قامت او در ضمیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پا درآمدم ز فراقت ستمگرا</p></div>
<div class="m2"><p>لیکن چه چاره چونکه غمت دستگیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پادشاه حسن و لطافت بگو چرا</p></div>
<div class="m2"><p>هیچت نظر ز لطف به حال فقیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل که جان و سر بنهادیم در غمت</p></div>
<div class="m2"><p>وز من ببین نگار که منت پذیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند رو به ترک بت بی وفا بگوی</p></div>
<div class="m2"><p>گفتم نمی توان که بتم را نظیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هستند دلبران به جهان بس ولی مرا</p></div>
<div class="m2"><p>در چشم جان چو حسن رخش دلپذیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم جهان و جان کنمش پیش کش ز شوق</p></div>
<div class="m2"><p>لیکن ورا نظر به متاعی حقیر نیست</p></div></div>