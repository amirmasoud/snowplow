---
title: >-
    شمارهٔ ۱۲۹۶
---
# شمارهٔ ۱۲۹۶

<div class="b" id="bn1"><div class="m1"><p>ای دل به سر کویش رو گر هوسی داری</p></div>
<div class="m2"><p>جان در قدمش انداز گر خود نفسی داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا کام دلم از لب یک لحظه بده جانا</p></div>
<div class="m2"><p>یا جان بستان از من تو بنده بسی داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پای شهید عشق چون کشته شوم آخر</p></div>
<div class="m2"><p>دریاب دل ما را گر دست رسی داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بلبل بستانم محزون نه روا باشد</p></div>
<div class="m2"><p>پر بسته مرا تا کی اندر قفسی داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرفی ز فراق تو کردیم بیان دانی</p></div>
<div class="m2"><p>ای نور دو چشمم گر در خانه کسی داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خاطر تو گوهر دریای محیطی تو</p></div>
<div class="m2"><p>در گرد جهان می گرد باری ز خسی داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خال لب شیرینت دانی به چه می ماند</p></div>
<div class="m2"><p>گویی شکرستان را شحنه مگسی داری</p></div></div>