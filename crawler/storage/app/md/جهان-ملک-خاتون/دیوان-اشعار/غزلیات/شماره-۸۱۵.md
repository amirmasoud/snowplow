---
title: >-
    شمارهٔ ۸۱۵
---
# شمارهٔ ۸۱۵

<div class="b" id="bn1"><div class="m1"><p>دل ربود از من و در دست بلا افکندش</p></div>
<div class="m2"><p>در غم هجر خدا را که چنین مپسندش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه ز قیدش برهاند نه مرادش بدهد</p></div>
<div class="m2"><p>حیف باشد دل بیچاره چنین در بندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان فروشد دل بیچاره به کوی غم دوست</p></div>
<div class="m2"><p>که نباشد به جهان هیچکسی مانندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویم ای دل بنشین گرد بلا بیش مگرد</p></div>
<div class="m2"><p>دل دیوانه ی ما سود ندارد پندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام و ننگ و تن و جان در سر کارش کردم</p></div>
<div class="m2"><p>گشت بدنام ملامت بکنم تا چندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک عشقت نکند این دل سرگشته مگر</p></div>
<div class="m2"><p>مهر رویت ز ازل در دل و جان آکندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل همی خواست که سیری بکند گرد جهان</p></div>
<div class="m2"><p>لیک زنجیر سر زلف تو شد پابندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از دو عالم بجز از نام تو یادش نبود</p></div>
<div class="m2"><p>بار دیگر ز غمت گر به جهان آرندش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خبرت هست نگارا که جهان از غم تو</p></div>
<div class="m2"><p>آتشی از رخ تو در دل و جان افکندش</p></div></div>