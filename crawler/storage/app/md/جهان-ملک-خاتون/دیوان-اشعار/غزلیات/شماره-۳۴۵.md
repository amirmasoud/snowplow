---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>دیدی که آن نگار سرو برگ ما نداشت</p></div>
<div class="m2"><p>دل بستد از فلان و به دست غمش گذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بس نبود کاو بستد دل ز دست ما</p></div>
<div class="m2"><p>وآنگاه شحنه ای ز جفا بر دلم گماشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم مگر که ریش مرا مرهمی بود</p></div>
<div class="m2"><p>مسکین دل ضعیف بر او این طمع نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاو ریش اندرون مرا این نمک زند</p></div>
<div class="m2"><p>یارب چه بود فکرش و با ما سرچه داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک دم وفا نکرد به قولش چو دوستان</p></div>
<div class="m2"><p>تخم جفا به وادی خاطر چرا بکاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه وفا نگیرد و دایم جفا کند</p></div>
<div class="m2"><p>ما را بر او نبود بر این گونه چشم داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم جهان گشاده به راه امید اوست</p></div>
<div class="m2"><p>از نیمروز تا به شب از صبح تا به چاشت</p></div></div>