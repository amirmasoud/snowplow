---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>دل من در خم چوگان دو زلفش چون گوست</p></div>
<div class="m2"><p>که کند چاره ی درد دل ما را جز دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود خون می رود از دیده ی من در غم او</p></div>
<div class="m2"><p>دل سنگین نگارم مگر از آهن و روست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نام و ننگ و دل و دین در سر کارت کردم</p></div>
<div class="m2"><p>دوستان عیب کنندم که فلانی بدخوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنشین عمر گرانمایه مکن صرف غمش</p></div>
<div class="m2"><p>جه کنم جان من و عشق فلان سنگ و سبوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی مقصود چو در کعبه ی رویت دارم</p></div>
<div class="m2"><p>این همه جور و جفا بر من مسکین ز چه روست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلبرا خوی بد از روی نکو حیف بود</p></div>
<div class="m2"><p>خوی زشت از رخ خوب ای بت زیبا نه نکوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جهان فاش شد و نیست ز عالم خبرش</p></div>
<div class="m2"><p>که فلان شاه جهانست و جهان بنده ی اوست</p></div></div>