---
title: >-
    شمارهٔ ۱۰۵۵
---
# شمارهٔ ۱۰۵۵

<div class="b" id="bn1"><div class="m1"><p>بیا که بی رخ خوبت نظر به کس نکنم</p></div>
<div class="m2"><p>به غیر کوی تو جایی دگر هوس نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا سریست به درگاه تو نهاده به خاک</p></div>
<div class="m2"><p>که التجا بجز از تو به هیچ کس نکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به روز وصل به پای تو گر رسد دستم</p></div>
<div class="m2"><p>گرم به تیغ زنی روی باز پس نکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو حواله من تا به کی به صبر کنی</p></div>
<div class="m2"><p>به جان دوست که من غیر یک نفس نکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبا بگوی به گل از زبان بلبل مست</p></div>
<div class="m2"><p>که من تحمّل ازین بیش در قفس نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلا مرا به جهان تا که جان بود در تن</p></div>
<div class="m2"><p>ز عشق سیر نگردم ز عیش بس نکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوای کعبه مقصود در دماغ منست</p></div>
<div class="m2"><p>به راه بادیه من گوش بر جرس نکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم چو بحر جهانست اگر رقیب خسیست</p></div>
<div class="m2"><p>یقین بدان که ز عالم نظر به خس نکنم</p></div></div>