---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>دردمندم از لب لعلت بده درمان ما</p></div>
<div class="m2"><p>کز رخ چون خورفکندی آتشی در جان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دلم دردیست درمانش نمی دانم ز وصل</p></div>
<div class="m2"><p>خود نمی آید به سر این درد بی درمان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق گویندم تو را بودی سر و سامان چه شد</p></div>
<div class="m2"><p>سر ز سودا پر شد و از دست رفت سامان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر از روی کرم بازآ که در هجران تو</p></div>
<div class="m2"><p>ز آب چشم خون فشانم تر شده دامان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدّتی تا در جهان سرگشته می گردم به غم</p></div>
<div class="m2"><p>خود نمی گویی که چون شد زار سرگردان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالها تا همچو سرو از عشق رویت سر کشید</p></div>
<div class="m2"><p>این دل سرگشته ی مهجور نافرمان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عید رویش را فدا کردم جهان و جان چه گفت</p></div>
<div class="m2"><p>خود چه ارزد لاشه ای تا می کنی قربان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس چو من در عاشقی جان و جهانی در نباخت</p></div>
<div class="m2"><p>عشق روی دوست آمد آیتی در شان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من گدای کوی او گشتم که تا بر من نظر</p></div>
<div class="m2"><p>افکند، دارد فراغت از جهان سلطان ما</p></div></div>