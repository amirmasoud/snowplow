---
title: >-
    شمارهٔ ۱۰۷۷
---
# شمارهٔ ۱۰۷۷

<div class="b" id="bn1"><div class="m1"><p>دیده دیدن رویت ز خدا می طلبیم</p></div>
<div class="m2"><p>در رخ نور وشت نور و صفا می طلبیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدّتی تا ز غمت دل به جفا بنهادیم</p></div>
<div class="m2"><p>لیکن از کوی صفا بوی وفا می طلبیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نداریم گناهی، چو تو می گویی نیست</p></div>
<div class="m2"><p>عفو آن از کرم و لطف شما می طلبیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دلت گرچه ز ما بود کدورت حالی</p></div>
<div class="m2"><p>از درون دل تو ذوق و صفا می طلبیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیب ما خود همه اینست که مشتاق توایم</p></div>
<div class="m2"><p>لب یار و لب جام و لب ما می طلبیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ جانیم در این قالب تن روزی چند</p></div>
<div class="m2"><p>لاجرم از دو جهان روی هوا می طلبیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو بالای تو ار سایه بینداخت به ما</p></div>
<div class="m2"><p>ما ز بالای تو ای دوست بلا می طلبیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرتو نور رخت گرچه جهانگیر شدست</p></div>
<div class="m2"><p>ما زکاتی ز تو از بهر گدا می طلبیم</p></div></div>