---
title: >-
    شمارهٔ ۱۳۷۷
---
# شمارهٔ ۱۳۷۷

<div class="b" id="bn1"><div class="m1"><p>من دردمند چاره ی دردم چه می کنی</p></div>
<div class="m2"><p>تدبیر حال دیده پر نم چه می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داریم ما دلی چو دو زلف تو پر ز شور</p></div>
<div class="m2"><p>بازش ز درد دوری درهم چه می کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجروح شد دلم ز جفاهایت ای صنم</p></div>
<div class="m2"><p>بر ریش ما بگوی که مرهم چه می کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردیست در دلم که دمم زان گرفته شد</p></div>
<div class="m2"><p>ای جان من بگوی که دردم چه می کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پشت امید من چو الف بود در غمت</p></div>
<div class="m2"><p>بازش چو طاق ابروی خود خم چه می کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر می کشی به غصّه مرا یکسره بکش</p></div>
<div class="m2"><p>خون در دلم ز هجر تو هردم چه می کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر قول دشمنان جفاجوی بی وفا</p></div>
<div class="m2"><p>هردم عنایتت تو ز ما کم چه می کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شادی و خرّمی ز جهان رفت گوئیا</p></div>
<div class="m2"><p>مسکین دل ضعیف تو با غم چه می کنی</p></div></div>