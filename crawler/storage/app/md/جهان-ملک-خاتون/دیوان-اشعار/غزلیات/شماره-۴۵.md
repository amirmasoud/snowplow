---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>عشقبازی‌ست کنون با رخ تو پیشهٔ ما</p></div>
<div class="m2"><p>از سر لطف نگارا بکن اندیشهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهروان ره عشقیم و بیابان فراق</p></div>
<div class="m2"><p>از لب لعل خدا را تو بده توشهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه شبگرد من آن جان جهان پیمایم</p></div>
<div class="m2"><p>بانگ در داد که زنهار مچین خوشهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو جانی تو بیا بر سر و جانم بنشین</p></div>
<div class="m2"><p>چون سرایی‌ست یقین خوش بود این گوشهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلبن وصل تو با شحنهٔ هجران می‌گفت</p></div>
<div class="m2"><p>لطف فرما و مکن از دو جهان ریشهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم اندیشه وصلی بکن آخر گفتا</p></div>
<div class="m2"><p>بازگردد ز حیا شیر نر از بیشهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشدم سرو صفت راست ثبات قدمی</p></div>
<div class="m2"><p>دلبرا در دو جهان نیست جز این پیشهٔ ما</p></div></div>