---
title: >-
    شمارهٔ ۶۰۱
---
# شمارهٔ ۶۰۱

<div class="b" id="bn1"><div class="m1"><p>آنان که مهر روی چو ماه تو دیده‌اند</p></div>
<div class="m2"><p>مهرت به جان و دل ز جهانی خریده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرهادوار عاشق و زارند خسروان</p></div>
<div class="m2"><p>تا یک حدیث از آن لب شیرین شنیده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیل محبّت تو چو دانی که از ازل</p></div>
<div class="m2"><p>ای نور دیده بر رخ جان‌ها کشیده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ما جدا مشو که دل طالبان دوست</p></div>
<div class="m2"><p>عشق رخ تو از همه عالم گزیده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از راه مردمی همه آیند مردمان</p></div>
<div class="m2"><p>کاندر هوای روی تو ای نور دیده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر هوای کعبهٔ وصل تو عاشقان</p></div>
<div class="m2"><p>جان داده‌اند و راه بیابان گزیده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنان که رو به قبلهٔ امّید داشتند</p></div>
<div class="m2"><p>هم عاقبت به کعبهٔ وصلش رسیده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد صبا نمود که سرو چمن تمام</p></div>
<div class="m2"><p>از شرم قامتت به قیامش خمیده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنان که عاقلان جهانند بی‌ریا</p></div>
<div class="m2"><p>بر خاک آستانت چو ماهی تپیده‌اند</p></div></div>