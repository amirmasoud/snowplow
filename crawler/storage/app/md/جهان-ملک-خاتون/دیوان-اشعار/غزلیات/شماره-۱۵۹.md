---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>مرغ جانم به سر کوی بتی در بندست</p></div>
<div class="m2"><p>به نسیمی ز سر کوی وفا خرسندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کمانی که بود راست چو ابروی کجش</p></div>
<div class="m2"><p>تیر مژگانش تو گویی ز هواش افکندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد دل هست بسی زان لب و رخ بر دل من</p></div>
<div class="m2"><p>لاجرم چاره ی درد دل ما گل قندست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ عشق رخ دلدار به منقار قضا</p></div>
<div class="m2"><p>گوییا مهر رخش در دل ما آکندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبر گویند ضرورت بکن از صحبت یار</p></div>
<div class="m2"><p>کس چه داند شب ایام فراقش چندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل گفتا برو ای دل به جهان سیری کن</p></div>
<div class="m2"><p>گفت نتوان که به زلفش دو جهان پابندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه جور و جفا بر من مسکین ز چه روست</p></div>
<div class="m2"><p>چو تو دانی که جهان از دل و جانت بندست</p></div></div>