---
title: >-
    شمارهٔ ۸۳۰
---
# شمارهٔ ۸۳۰

<div class="b" id="bn1"><div class="m1"><p>در فراقت من نکردم خواب خوش</p></div>
<div class="m2"><p>بی لبت هرگز نخوردم آب خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرقه گشتم در غمت دستم بگیر</p></div>
<div class="m2"><p>بحر غم را کی بود پایاب خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در فراقت جان شیرین می دهم</p></div>
<div class="m2"><p>بنده بیچاره را دریاب خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی خوبت کعبه صاحب دلان</p></div>
<div class="m2"><p>زآنکه باشد ابرویت محراب خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تابکی در تاب باشی ای نگار</p></div>
<div class="m2"><p>گرچه باشد در دو زلفت تاب خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز رخ عاشق فریبت دلبرا</p></div>
<div class="m2"><p>من ندیدم در جهان مهتاب خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار من عشقست و بار از غم به دل</p></div>
<div class="m2"><p>باشدم در عشق او اسباب خوش</p></div></div>