---
title: >-
    شمارهٔ ۱۱۶۹
---
# شمارهٔ ۱۱۶۹

<div class="b" id="bn1"><div class="m1"><p>خلاف عادت معهود را وفایی کن</p></div>
<div class="m2"><p>بیا و درد من از وصل خود دوایی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو پادشاهی و من بنده ای ضعیف نحیف</p></div>
<div class="m2"><p>نظر ز روی عنایت سوی گدایی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا شدی تو ز ما ای نگار بیگانه</p></div>
<div class="m2"><p>که گفت پشت وفا را به آشنایی کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوای خسته دلان وصل تست تا دانی</p></div>
<div class="m2"><p>ز لطف چاره احوال بی نوایی کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بگذری چو سهی سرو در سرا بستان</p></div>
<div class="m2"><p>به سوی خسته دلان نیز مرحبایی کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر کند نظری بر جهان ز لطف کنون</p></div>
<div class="m2"><p>بیا و میل سوی بوستان سرایی کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به لطف گفتمش امشب دمی مرا بنواز</p></div>
<div class="m2"><p>که گفت با تو که آن باز ماجرایی کن</p></div></div>