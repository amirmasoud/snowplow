---
title: >-
    شمارهٔ ۱۳۷۶
---
# شمارهٔ ۱۳۷۶

<div class="b" id="bn1"><div class="m1"><p>دلبرا با ما تو لطف بی‌نهایت می‌کنی</p></div>
<div class="m2"><p>از درم بازآ اگر با ما عنایت می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای امید دوستان هجر تو ما را دشمن است</p></div>
<div class="m2"><p>دشمن ما را چرا آخر حمایت می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ستانی غم دهی بر ما ستم داری روا</p></div>
<div class="m2"><p>بعد از آن از من به صد دستان شکایت می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفته ای کاو گفت ترک عشق ما، بالله که من</p></div>
<div class="m2"><p>بی‌خبر ز آنم بتا کز من روایت می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل سرگشته در هجران روی آن نگار</p></div>
<div class="m2"><p>جان ز غم دادی و پنداری کفایت می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای صبا جان جهان یک سر معطّر می‌شود</p></div>
<div class="m2"><p>چون ز زلف مشک‌بوی او حکایت می‌کنی</p></div></div>