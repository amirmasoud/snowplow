---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>فریاد که جز لطف تو فریادرسم نیست</p></div>
<div class="m2"><p>واندر دو جهان غیر غمت هیچ کسم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل همه اینست که از پای فتادم</p></div>
<div class="m2"><p>از هجر و به وصل رخ تو دست رسم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه نفسی یاد من خسته نکردی</p></div>
<div class="m2"><p>صبر از رخ همچون قمرت یک نفسم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بلبل شوریده ام از عشق رخ تو</p></div>
<div class="m2"><p>اندر قفسی بسته ولی همنفسم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قدّ خود ار راست بپرسی ز من ای جان</p></div>
<div class="m2"><p>زین سخت تر امروز به جان تو قسم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جنّت فردوس دهندم به حقیقت</p></div>
<div class="m2"><p>جز خاک سر کوی تو باری هوسم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه به سر آمد به زبان دشمن بدخواه</p></div>
<div class="m2"><p>من بحر محیطم که زیانی ز خسم نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من چون شتر بارکش و خار جفاخور</p></div>
<div class="m2"><p>در گردن ظالم چه کنم چون جرسم نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در صبر و مدارا به جهان چاره ندارم</p></div>
<div class="m2"><p>در درد فراق تو چو فریادرسم نیست</p></div></div>