---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>گفتم آن ترک جفا جو نکند هیچ وفا</p></div>
<div class="m2"><p>نکند با دل سرگشته ی ما غیر جفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون همه کار جهان، بنده به کامش خواهد</p></div>
<div class="m2"><p>از چه رو می نکند کام دل خسته روا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نهادیم سری در قدمت سرو روان</p></div>
<div class="m2"><p>راست گو از چه کشی ای دل و دین سر تو ز ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذشتی و زدی آتش مهری به دلم</p></div>
<div class="m2"><p>آن نه بالاست که هست او به دل خسته بلا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاهی به جهان از کرمت کم نشود</p></div>
<div class="m2"><p>گر کنی از سر لطفت نظری سوی گدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو را با من بیچاره ی مسکین جنگست</p></div>
<div class="m2"><p>چه کنم با تو که ما سر صلحست و صفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نام مشک ختنی برد زبانم گویی</p></div>
<div class="m2"><p>بوی زلف تو شنیدم همه از باد صبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوش در ماه رخ او نظری می کردم</p></div>
<div class="m2"><p>چون بدیدم به مه ای دوست کجا تا به کجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من به ظلمات شب هجر تو تا کی باشم</p></div>
<div class="m2"><p>بی رخ دوست نباشد به جهان نور و ضیا</p></div></div>