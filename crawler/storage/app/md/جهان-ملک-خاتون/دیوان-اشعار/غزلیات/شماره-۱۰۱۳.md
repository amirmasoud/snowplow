---
title: >-
    شمارهٔ ۱۰۱۳
---
# شمارهٔ ۱۰۱۳

<div class="b" id="bn1"><div class="m1"><p>بگرفت ز دست غم ملالم</p></div>
<div class="m2"><p>باشد که نظر کنی به حالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بلبل مست در گلستان</p></div>
<div class="m2"><p>از شوق رخش چرا ننالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حسرت آن هلال ابرو</p></div>
<div class="m2"><p>ای دوست ببین که چون هلالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشفته به عشق آن پری رو</p></div>
<div class="m2"><p>مشتاق بدان دو زلف و خالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وصل تو چون الف قدم بود</p></div>
<div class="m2"><p>وامروز ز هجر همچو دالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنکه مرا نمی کنی یاد</p></div>
<div class="m2"><p>یکدم نروی تو از خیالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بند فراق تو گرفتار</p></div>
<div class="m2"><p>محروم به یک ره از وصالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرگشته چو خضر بر لب جوی</p></div>
<div class="m2"><p>بر لب بچکان از آن زلالم</p></div></div>