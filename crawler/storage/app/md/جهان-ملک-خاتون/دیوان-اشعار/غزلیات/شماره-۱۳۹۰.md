---
title: >-
    شمارهٔ ۱۳۹۰
---
# شمارهٔ ۱۳۹۰

<div class="b" id="bn1"><div class="m1"><p>الهی یا الهی یا الهی</p></div>
<div class="m2"><p>به فضلت چون جهانی را پناهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلش را تازه دار از نور ایمان</p></div>
<div class="m2"><p>تنش را دور گردان از تباهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوندی تو و ما بندگانیم</p></div>
<div class="m2"><p>نکو باشد به بنده هرچه خواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شر ظالمانش دور گردان</p></div>
<div class="m2"><p>برون آور سپیدیش از سیاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به لطفت گرچه بس امیدوارست</p></div>
<div class="m2"><p>ولی شرمنده است از هر گناهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشو نومید از وصلش تو زنهار</p></div>
<div class="m2"><p>دلا می ساز اگر تو مرد راهی</p></div></div>