---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>سهی سرو مرا بالا بلندست</p></div>
<div class="m2"><p>لب جان بخش او خوشتر ز قندست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم در عشق او نالان همانا</p></div>
<div class="m2"><p>نمی داند شب هجران که چندست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی داند درازی شب هجر</p></div>
<div class="m2"><p>که در زلف دلارامی به بندست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل مسکین من در بام و در شام</p></div>
<div class="m2"><p>به نار هر دو رخسارت سپندست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا آن دلبر نامهربانم</p></div>
<div class="m2"><p>درخت مهر ما از بیخ کندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندارد با من دلخسته یاری</p></div>
<div class="m2"><p>به غایت سرکش و بدخو و تندست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بدخویی نباشد در جهان هیچ</p></div>
<div class="m2"><p>مکن جانا که کاری ناپسندست</p></div></div>