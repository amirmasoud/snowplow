---
title: >-
    شمارهٔ ۵۰۴
---
# شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>خوشا مشکی که از زلف تو ریزد</p></div>
<div class="m2"><p>خوشا بادا که از کوی تو خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت را در چمن گر گل ببیند</p></div>
<div class="m2"><p>ز شرم روی تو دردم بریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشم شوخت آهوی تتاری</p></div>
<div class="m2"><p>خورد زنهار و از پیشت گریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم در کوی تو جان کرده ضایع</p></div>
<div class="m2"><p>به سر خاک رهت تا چند بیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وصلم شربتی سازنده ندهد</p></div>
<div class="m2"><p>به هجران همچنین با من ستیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن گویم ز من گر راست پرسی</p></div>
<div class="m2"><p>چو قدّت سرو در بستان نخیزد</p></div></div>