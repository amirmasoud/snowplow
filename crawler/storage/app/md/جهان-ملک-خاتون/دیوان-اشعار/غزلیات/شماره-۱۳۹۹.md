---
title: >-
    شمارهٔ ۱۳۹۹
---
# شمارهٔ ۱۳۹۹

<div class="b" id="bn1"><div class="m1"><p>بیا جانا که دردم را دوایی</p></div>
<div class="m2"><p>ز جان مشتاقتم آخر کجایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی بینم جهان را بی جمالت</p></div>
<div class="m2"><p>تو می دانی که نور چشم مایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم بی تو یک دم صبر و آرام</p></div>
<div class="m2"><p>بگو تا کی کنی از ما جدایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو جان رفته ای از بر خدا را</p></div>
<div class="m2"><p>نه وقت آمد که بازم با تن آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر یک شب درآیی از درم شاد</p></div>
<div class="m2"><p>بود در شأن من لطف خدایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیامد وقت آن جانا که از لطف</p></div>
<div class="m2"><p>عنان دوستی سویم گرایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا بیگانه وش گشتی به یکبار</p></div>
<div class="m2"><p>که برچیدی بساط آشنایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکردی رحمتی بر حال زارم</p></div>
<div class="m2"><p>نگارا پیشه کردی بی وفایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سلطان جهانی من گدایت</p></div>
<div class="m2"><p>کنم از جانب وصلت گدایی</p></div></div>