---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>دل از دوران گیتی شاد بادت</p></div>
<div class="m2"><p>ز غمهای جهان آزاد بادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبادا یادم از یادت فراموش</p></div>
<div class="m2"><p>همیشه عهد و پیمان یاد بادت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه در سرابستان جانم</p></div>
<div class="m2"><p>قد و بالای چون شمشاد بادت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه دلبرا از پیر معنی</p></div>
<div class="m2"><p>وفا و مهر ما ارشاد بادت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگو تا کی کنی این جور و بیداد</p></div>
<div class="m2"><p>پشیمانی ازین بیداد بادت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خداوندا سرای مهربانی</p></div>
<div class="m2"><p>همیشه در جهان آباد بادت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غذای روح ما اندر مصلّی</p></div>
<div class="m2"><p>ز آب سرو رکناباد بادت</p></div></div>