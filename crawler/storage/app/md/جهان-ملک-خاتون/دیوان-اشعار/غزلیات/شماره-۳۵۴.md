---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>دلبر چه زود از سر پیمان ما برفت</p></div>
<div class="m2"><p>از رفتنش چه سوز که بر جان ما برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردم چو عشق دوست که حالش پدید نیست</p></div>
<div class="m2"><p>هست و طبیب از سر درمان ما برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشو و نما نکرد دگر شاخ نارون</p></div>
<div class="m2"><p>تا آن قد چو سرو ز بستان ما برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگر نخواند بلبل خوشخوان به صبحدم</p></div>
<div class="m2"><p>تا آن رخ چو گل ز گلستان ما برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانا به چشم تو که نشد جمع خاطرم</p></div>
<div class="m2"><p>تا از بر آن دو زلف پریشان ما برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی برفروخت کاخ دل ما به نور وصل</p></div>
<div class="m2"><p>تا از میانه شمع شبستان ما برفت</p></div></div>