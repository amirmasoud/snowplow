---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>ما را به درد عشق تو جز صبر چاره نیست</p></div>
<div class="m2"><p>شب نیست کز فراق تو صد جامه پاره نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان ز درد عشق تو خونم ز دیده رفت</p></div>
<div class="m2"><p>کز اشک دیده بر سر کویم گذاره نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانا به حال زار منت کی شود نظر</p></div>
<div class="m2"><p>چون عاشقان روی ترا خود شماره نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستغرق محیط فراقم ستمگرا</p></div>
<div class="m2"><p>دریای بی کران غمت را کناره نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیچاره ام تو چاره کارم نمی کنی</p></div>
<div class="m2"><p>زین بیش در غم تو صبوریم چاره نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکدم به دولت شب وصلت نمی رسم</p></div>
<div class="m2"><p>از دورم از جمال تو غیر از نظاره نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتی به درد هجر جهانی به انتظار</p></div>
<div class="m2"><p>مشکل که با تو گفت و شنیدیم یاره نیست</p></div></div>