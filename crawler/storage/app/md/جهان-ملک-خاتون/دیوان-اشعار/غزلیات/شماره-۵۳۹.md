---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>اگرچه بر دلم از هجر صد ستم باشد</p></div>
<div class="m2"><p>ولی امید وصال ار بود چه غم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگرچه خسته و زاری دلا مباد آن روز</p></div>
<div class="m2"><p>که سایه غم او از سر تو کم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدم به پرسش بیمار نه که خواهم کرد</p></div>
<div class="m2"><p>هزار جان گرامی فدا گرم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز روز فراقت به جان رسید کنون</p></div>
<div class="m2"><p>گرش به وصل نوازی شبی کرم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا به حال گدایان نظر کنی شاها</p></div>
<div class="m2"><p>تو را که ملک سلیمان و جام جم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه پشت مرادم به زیر بار فراق</p></div>
<div class="m2"><p>هلال وار چو ابروی دوست خم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که بر در وصل تو جان دهد چو جهان</p></div>
<div class="m2"><p>میان حلقه ی عشّاق محترم باشد</p></div></div>