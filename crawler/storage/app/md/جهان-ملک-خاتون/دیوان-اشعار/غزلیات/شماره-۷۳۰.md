---
title: >-
    شمارهٔ ۷۳۰
---
# شمارهٔ ۷۳۰

<div class="b" id="bn1"><div class="m1"><p>در حسرت آن چهره و روییم دگر بار</p></div>
<div class="m2"><p>آشفته روی تو چو موییم دگر بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بوی امیدی که تو بر ما گذر آری</p></div>
<div class="m2"><p>از لعبتکان سر کوییم دگر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تاب سر زلف چو چوگان تو یارا</p></div>
<div class="m2"><p>در کوی تو سرگشته چو گوییم دگر بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خاک کف پای تو در دست من افتد</p></div>
<div class="m2"><p>در گرد جهان در تک و پوییم دگر بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانند سکندر منم و چشمه حیوان</p></div>
<div class="m2"><p>در ظلمت گیسوی تو جوییم دگر بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تشبیه قدش کردم با سرو گل اندام</p></div>
<div class="m2"><p>رنجید ز ما راست نگوییم دگر بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از صبح نسیم سر زلف تو شنیدیم</p></div>
<div class="m2"><p>در حسرت آن نکهت و بوییم دگر بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر دامن وصل تو به دست دلم افتد</p></div>
<div class="m2"><p>از لعل تو جز کام نجوییم دگر بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق تو چو سنگست و دل ما جو سبوییست</p></div>
<div class="m2"><p>در شوق تو چون سنگ و سبوییم دگر بار</p></div></div>