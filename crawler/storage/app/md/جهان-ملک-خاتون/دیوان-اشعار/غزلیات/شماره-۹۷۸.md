---
title: >-
    شمارهٔ ۹۷۸
---
# شمارهٔ ۹۷۸

<div class="b" id="bn1"><div class="m1"><p>به عالم غیر تو یاری ندارم</p></div>
<div class="m2"><p>بجز عشق رخت کاری ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را گر هست بر جایم بسی یار</p></div>
<div class="m2"><p>به جانت من کسی باری ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کوی تو سگان را هست باری</p></div>
<div class="m2"><p>چرا ای دوست من باری ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خداوند منی من بنده فرمان</p></div>
<div class="m2"><p>به جان تو کز این عاری ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه برگرفت آزرم از پیش</p></div>
<div class="m2"><p>من از دلدار آزاری ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جان تو که در عالم نگارا</p></div>
<div class="m2"><p>به جز لطفت جهانداری ندارم</p></div></div>