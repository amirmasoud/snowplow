---
title: >-
    شمارهٔ ۱۰۴۵
---
# شمارهٔ ۱۰۴۵

<div class="b" id="bn1"><div class="m1"><p>درد ما را با غمت چون نیست درمان چون کنم</p></div>
<div class="m2"><p>وین سر سرگشته ام را نیست سامان چون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ضعیفست ای مسلمانان به فریادم رسید</p></div>
<div class="m2"><p>چون ندارد طاقت آن بار هجران چون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در امید صبح دیدارش به جان آمد دلم</p></div>
<div class="m2"><p>وین شب هجران نمی آید به پایان چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوای کعبه وصلش تکاپویی زدم</p></div>
<div class="m2"><p>چون نمی بیند دلم حدّ بیابان چون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به عید روی چون خورشید تابانش بگو</p></div>
<div class="m2"><p>این محقّر جان به پیش دوست قربان چون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدن دیدار رویت نازنینا مشکلست</p></div>
<div class="m2"><p>بار هجران رخت را بر خود آسان چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوستان گویند طوفی در چمن کردن خوشست</p></div>
<div class="m2"><p>با قد یارم نظر بر سرو بستان چون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای صبا حال دلم با بلبل بستان بگو</p></div>
<div class="m2"><p>با رخ همچون گلش میل گلستان چون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان ز ما درخواست آن نور دو دیده در جهان</p></div>
<div class="m2"><p>من دریغ از جان شیرین رای جانان چون کنم</p></div></div>