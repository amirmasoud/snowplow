---
title: >-
    شمارهٔ ۸۶۰
---
# شمارهٔ ۸۶۰

<div class="b" id="bn1"><div class="m1"><p>ما را ز حد برفت ز درد تو اشتیاق</p></div>
<div class="m2"><p>تا کی کشیم زهر فراق تو در مذاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمی به حال زار من مستمند کن</p></div>
<div class="m2"><p>زان رو که نیستم پس ازین طاقت فراق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طاق و جفت من که ببازید عشق تو</p></div>
<div class="m2"><p>با درد عشق جفت شدیم وز صبر طاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان می دهیم بر سر کویش ز گفت و گوی</p></div>
<div class="m2"><p>یک شب وصال دوست مگر افتد اتّفاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آرزوی روی تو و لعل دلکشت</p></div>
<div class="m2"><p>در نار محرقست تن و دل در احتراق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نور شمع طلعت تو مهر در کسوف</p></div>
<div class="m2"><p>وز تاب آفتاب رخت ماه در محاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح از فروغ نور جمال تو مشتقست</p></div>
<div class="m2"><p>شب را ز شام ظلمت زلف تو اشتقاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خاکبوس خاک درت ممکنم شود</p></div>
<div class="m2"><p>بالای هفت منظر گردون زنم رواق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی کشیم جان و جهان از میان جان</p></div>
<div class="m2"><p>بار جفا و جور تو و درد اشتیاق</p></div></div>