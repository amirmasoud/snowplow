---
title: >-
    شمارهٔ ۴۰۹
---
# شمارهٔ ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>ما را نمی رود نفسی یاد او ز یاد</p></div>
<div class="m2"><p>یارب که یاد او ز دل خسته کم مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند اعتقاد تو با ما درست نیست</p></div>
<div class="m2"><p>هردم زیادتست مرا با تو اعتقاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من جز به یاد تو نزنم یک دم و تو را</p></div>
<div class="m2"><p>یک دم نیاید از من آشفته حال یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوسته شادی تو اگر در غم منست</p></div>
<div class="m2"><p>هرگز دلم بجز غم عشقت مباد شاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیداد می کشم ز غمت بر امید آنک</p></div>
<div class="m2"><p>روزی به خوشدلی بستانم ز وصل داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از درد هجر اگرچه گرفتار محنتم</p></div>
<div class="m2"><p>هرگز ز روزگار تو را محنتی مباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتی به تیغ هجر من مستمند را</p></div>
<div class="m2"><p>این رخصتت به خون دل عاشقان که داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در لطف و دلبری چو تو فرزند خوبروی</p></div>
<div class="m2"><p>از مادر زمانه به نیک اختری نزاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانم ز اشتیاق فلانی به لب رسید</p></div>
<div class="m2"><p>یارب بلای عشق که اندر جهان نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر روز وصل دوست نداریم دست رس</p></div>
<div class="m2"><p>یک شب ز روی لطف خدا روزیم کناد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حال جهان چو خال تو یکباره تیره گشت</p></div>
<div class="m2"><p>تا شور زلف دلکش تو در جهان فتاد</p></div></div>