---
title: >-
    شمارهٔ ۸۳۸
---
# شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>گر شبی سوی من خسته دل افتد رایش</p></div>
<div class="m2"><p>چه تفاوت کند از رای جهان آرایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عنانی ز عنایت سوی ما گرداند</p></div>
<div class="m2"><p>جان فشانیم به جای سرو زر در پایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خرامد قد چون سرو روانت در باغ</p></div>
<div class="m2"><p>هیچ شک نیست که در دیده ی ما شد جایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه شب تا به سحر در غم تو بخروشد</p></div>
<div class="m2"><p>فارغی ای صنم از دیده ی خون پالایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فارغ از جنّت فردوس بود خاطر او</p></div>
<div class="m2"><p>که شبی بر سر کوی تو بود مأوایش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلق گویند برو ترک غمش گو چکنم</p></div>
<div class="m2"><p>نیست در زیر کبودی فلک همتایش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه روزی ز سر زلف تو بویی بشنید</p></div>
<div class="m2"><p>نبود در دو جهان با دگری پروایش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خلقی به جهان خون شده از قامت او</p></div>
<div class="m2"><p>این بلا بین که دلم می کشد از بالایش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش خاک رهم ز آتش دل آبم ده</p></div>
<div class="m2"><p>گفت وصلم همه بادست تو می پیمایش</p></div></div>