---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>دردم او دادست و درمانم از اوست</p></div>
<div class="m2"><p>چاره ی دردم که جوید غیر دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کند با من جفا آن بی وفا</p></div>
<div class="m2"><p>بد نباشد هر چه زو آید نکوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا توانایی بود جورش به جان</p></div>
<div class="m2"><p>می کشم زو گرچه یاری تندخوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال جان پرسیدم از دل عقل گفت</p></div>
<div class="m2"><p>از که می پرسی که سرگردان چه گوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاب چوگان دو زلفش می برم</p></div>
<div class="m2"><p>لاجرم افتان و خیزان کو به کوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو برو چشم از همه عالم بدوز</p></div>
<div class="m2"><p>هر که میلش سوی یاری خوب روست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفتد بر مشک چین چشمش خطاست</p></div>
<div class="m2"><p>هر که را در دست، زلفی مشک بوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر می ورزم به ماهی در زمین</p></div>
<div class="m2"><p>کافتاب آسمانش مهرجوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من به دست یار دادم اختیار</p></div>
<div class="m2"><p>اعتمادی در جهان ما را بدوست</p></div></div>