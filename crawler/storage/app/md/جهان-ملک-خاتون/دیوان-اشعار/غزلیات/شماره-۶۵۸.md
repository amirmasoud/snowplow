---
title: >-
    شمارهٔ ۶۵۸
---
# شمارهٔ ۶۵۸

<div class="b" id="bn1"><div class="m1"><p>درد ما را دوا نخواهد بود</p></div>
<div class="m2"><p>کامم از لب روا نخواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازنینا خیال طلعت تو</p></div>
<div class="m2"><p>از دو چشمم جدا نخواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال ما پیش تو که عرضه دهد</p></div>
<div class="m2"><p>محرمم جز صبا نخواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جور تا کی کشم بگو ز غمت</p></div>
<div class="m2"><p>ظلم بر ما روا نخواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چه رو دلبرا میانه ما</p></div>
<div class="m2"><p>بجز از ماجرا نخواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جور از این بیشتر مکن که مرا</p></div>
<div class="m2"><p>طاقت این جفا نخواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ز روز نخست دانستم</p></div>
<div class="m2"><p>یار ما را وفا نخواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با همه جور لطف جان بخشت</p></div>
<div class="m2"><p>فارغ از حال ما نخواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر و تدبیر و رایهای صواب</p></div>
<div class="m2"><p>مانع هر قضا نخواهد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شک ندارم که پادشاه جهان</p></div>
<div class="m2"><p>نظرش بر خطا نخواهد بود</p></div></div>