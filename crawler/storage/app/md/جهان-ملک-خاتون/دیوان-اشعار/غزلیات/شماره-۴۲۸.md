---
title: >-
    شمارهٔ ۴۲۸
---
# شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>لب لعلت ز جهانی دل و جان می طلبد</p></div>
<div class="m2"><p>دل و جان را چه محل هر دو جهان می طلبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قد سرو روانت بخرامد به چمن</p></div>
<div class="m2"><p>در نثار قدمش روح و روان می طلبد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل جان من از شوق گل رخسارت</p></div>
<div class="m2"><p>گل به بستان جهان نعره زنان می طلبد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جفای تو نگارا دلم از جان بگرفت</p></div>
<div class="m2"><p>همچنان دولت وصل تو به جان می طلبد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه بیرون بود از حسن تو داری به جهان</p></div>
<div class="m2"><p>دل سرگشته ی من در لبت آن می طلبد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من در طلب قامت رعنای تو بود</p></div>
<div class="m2"><p>نه که در باغ جهان سرو روان می طلبد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو را بود عنایت به سوی خسته دلان</p></div>
<div class="m2"><p>دل بیچاره ی ما از تو همان می طلبد</p></div></div>