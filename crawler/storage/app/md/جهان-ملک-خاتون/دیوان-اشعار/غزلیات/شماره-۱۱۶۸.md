---
title: >-
    شمارهٔ ۱۱۶۸
---
# شمارهٔ ۱۱۶۸

<div class="b" id="bn1"><div class="m1"><p>نشین به تخت دل ما و پادشاهی کن</p></div>
<div class="m2"><p>بده تو داد دل ما و هرچه خواهی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گواه خون دل ماست مردم دیده</p></div>
<div class="m2"><p>تو چشم سوی من و گوش بر گواهی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که خون دل به فراقت ز دیده می بارد</p></div>
<div class="m2"><p>ز وصل خویشتنش زود عذرخواهی کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر ز مردم چشمم نمی کنی باور</p></div>
<div class="m2"><p>نظر به اشک چو مرجان و رنگ کاهی کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم گدای سر کوی تو دریغ مدار</p></div>
<div class="m2"><p>نظر ز حال فروماندگان و شاهی کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین جهان اگرت وصل دوست می باید</p></div>
<div class="m2"><p>بیا و از دل و جان آه صبحگاهی کن</p></div></div>