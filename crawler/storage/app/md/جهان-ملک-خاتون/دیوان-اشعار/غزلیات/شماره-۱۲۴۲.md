---
title: >-
    شمارهٔ ۱۲۴۲
---
# شمارهٔ ۱۲۴۲

<div class="b" id="bn1"><div class="m1"><p>آه از این روزگار گردیده</p></div>
<div class="m2"><p>آه از این کار ناپسندیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان رسیدم به لب بگو چه کنم</p></div>
<div class="m2"><p>از جفای تو ای دل و دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جفاهای چرخ بی قانون</p></div>
<div class="m2"><p>خون فشانیم دایم از دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا خار کز غمت خوردیم</p></div>
<div class="m2"><p>یک گل از باغ وصل ناچیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ز وصف جمال او گشتم</p></div>
<div class="m2"><p>عاشق روی دوست نادیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه چشمی جمال تو طلبند</p></div>
<div class="m2"><p>خوش بود مردم جهان دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ دانی بتا که آن بالا</p></div>
<div class="m2"><p>بر دل ما بلاست از دیده</p></div></div>