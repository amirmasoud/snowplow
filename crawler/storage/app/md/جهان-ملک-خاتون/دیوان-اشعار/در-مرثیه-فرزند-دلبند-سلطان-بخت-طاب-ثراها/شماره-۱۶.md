---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>گرچه به ستم چرخ به من دست گشود</p></div>
<div class="m2"><p>وین تازه گل از باغ دل من بربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم به دو چشم خویش کز لطف دری</p></div>
<div class="m2"><p>بر روی وی از روضه رضوان بگشود</p></div></div>