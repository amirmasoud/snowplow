---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>بازم ز غم زمانه جان می‌سوزد</p></div>
<div class="m2"><p>جان را چه محل جمله جهان می‌سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سوختن جانش نهان می‌باشد</p></div>
<div class="m2"><p>دیدم جگر خود که عیان می‌سوزد</p></div></div>