---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>از دنیی دون اگر دری بر وی بست</p></div>
<div class="m2"><p>یا خار جفای دهر بر پاش شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم به یقین نه در گمانم دیدم</p></div>
<div class="m2"><p>در جنّت فردوس که با حور نشست</p></div></div>