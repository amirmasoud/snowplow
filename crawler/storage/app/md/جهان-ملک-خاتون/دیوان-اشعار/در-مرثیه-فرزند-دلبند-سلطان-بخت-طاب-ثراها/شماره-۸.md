---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>به حسرت ببردش ز دنیای دون</p></div>
<div class="m2"><p>به عقبی بود نیکیش رهنمون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مریزا گل رویش اندر مغاک</p></div>
<div class="m2"><p>به فردوس اعلی برش جان پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراموش گردان جهان بر دلش</p></div>
<div class="m2"><p>پر از عنبر و مشک گردان دلش</p></div></div>