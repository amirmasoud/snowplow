---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گل فرو ریخت و رخ از باغ جهان پنهان کرد</p></div>
<div class="m2"><p>بلبل دلشده را خسته دل و نالان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل نخندید ز بستان امیدم به ستم</p></div>
<div class="m2"><p>خار هجران تو ای جان اثرم در جان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخت برگشت ز من تا تو شدی از بر من</p></div>
<div class="m2"><p>روز هجران توأم بی سر و بی سامان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز وصل تو نشد روزی من زآنکه مرا</p></div>
<div class="m2"><p>بخت وارونه حوالت به شب هجران کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس عجب واقعه ای بد که مثل را گویند</p></div>
<div class="m2"><p>رخ خورشید به گل کی بتوان پنهان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاری من به فلک بر شد لیکن چه کنم</p></div>
<div class="m2"><p>بجز از صبر و تحمّل تو بگو چتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد هجر تو چنانست که طبیبان جهان</p></div>
<div class="m2"><p>نتوانند یکی درد مرا درمان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درد بسیار کشیدم ز فلک لیک عجب</p></div>
<div class="m2"><p>آن همه درد و بلا بر دل من آسان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیر هجران عزیزان به دلم بود بسی</p></div>
<div class="m2"><p>لیک پیکان فراق تو اثر در جان کرد</p></div></div>