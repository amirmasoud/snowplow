---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>تا چند فلک جامه غم می‌دوزد</p></div>
<div class="m2"><p>بر قامت جان بین که دلم می‌سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شرح توان داد که مسکین دل من</p></div>
<div class="m2"><p>خون از ستم زمانه می‌اندوزد</p></div></div>