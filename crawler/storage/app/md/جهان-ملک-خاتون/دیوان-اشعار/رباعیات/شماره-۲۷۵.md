---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>تا کی بود از غمت بلا بر دل من</p></div>
<div class="m2"><p>تا چند نهی داغ جفا بر دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین بیش منه که طاقتم طاق شدست</p></div>
<div class="m2"><p>بار غم و غصّه دلبرا بر دل من</p></div></div>