---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>مشکل که به درد عشق درمانم نیست</p></div>
<div class="m2"><p>جز آتش هجران تو در جانم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که مگر به هجر صبرم باشد</p></div>
<div class="m2"><p>روزی ز تو ای نگار و امکانم نیست</p></div></div>