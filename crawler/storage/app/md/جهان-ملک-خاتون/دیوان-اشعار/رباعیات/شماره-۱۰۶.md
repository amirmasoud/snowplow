---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>آن دوست که آرام دل ما باشد</p></div>
<div class="m2"><p>گویند که زشتست بهل تا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید که به چشم کس نه زیبا باشد</p></div>
<div class="m2"><p>تا باری از آن من تنها باشد</p></div></div>