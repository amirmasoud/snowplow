---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>رخ را ز من خسته نهان می دارد</p></div>
<div class="m2"><p>صحبت همه شب با دگران می دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ترک غم عشق تو نتوانم گفت</p></div>
<div class="m2"><p>زان رو که تعلّقم به جان می دارد</p></div></div>