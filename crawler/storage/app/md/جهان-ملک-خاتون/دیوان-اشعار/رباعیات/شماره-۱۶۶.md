---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>دل بسته آن پسته دهانست هنوز</p></div>
<div class="m2"><p>جان در پی آن سرو روانست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت دیدار تو ای جان و جهان</p></div>
<div class="m2"><p>خون جگر از دیده روانست هنوز</p></div></div>