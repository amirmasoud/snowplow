---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ما را ز جهان وصل نگاری کامست</p></div>
<div class="m2"><p>وز کام جدا گشتنم از ناکامست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشّاق تو در جهان بسی هست ولی</p></div>
<div class="m2"><p>بیچاره جهان که در جهان بدنامست</p></div></div>