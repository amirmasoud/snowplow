---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>خورشید رخا من به کمند تو درم</p></div>
<div class="m2"><p>بارت بکشم به جان و جورت ببرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سیم و زرم خواهی ور جان و سرم</p></div>
<div class="m2"><p>خود را بفروشم و مرادت بخرم</p></div></div>