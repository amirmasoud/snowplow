---
title: >-
    شمارهٔ ۳۱۹
---
# شمارهٔ ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>در خاک درت آب رخم ریخته ای</p></div>
<div class="m2"><p>وز حلقه زلفت دلم آویخته ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد دل من ز لعل جان پرور تست</p></div>
<div class="m2"><p>زان روی تو گل با شکر آمیخته ای</p></div></div>