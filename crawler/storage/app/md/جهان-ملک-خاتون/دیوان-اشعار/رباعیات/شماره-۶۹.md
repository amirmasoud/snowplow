---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>زین بیش روا مدار بر من ستمت</p></div>
<div class="m2"><p>دل شاد کنم دمی که مردم ز غمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم ز غمت دمی دم ای عیسی دم</p></div>
<div class="m2"><p>باشد که شوم زنده به فرخنده غمت</p></div></div>