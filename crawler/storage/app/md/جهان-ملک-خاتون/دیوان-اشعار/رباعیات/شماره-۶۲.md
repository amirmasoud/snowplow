---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>در بندگیت طاقم و با درد تو جفت</p></div>
<div class="m2"><p>ز الماس مژه درّ زمین خواهم سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست جفای چرخ و از جور رقیب</p></div>
<div class="m2"><p>در بستر ایمنی نمی یارم خفت</p></div></div>