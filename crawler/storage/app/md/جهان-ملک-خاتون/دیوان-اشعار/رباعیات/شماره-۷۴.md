---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>دیدم صنمی به قد چو سرو آزاد</p></div>
<div class="m2"><p>چون نرگس تازه گل رخی حور نژاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد خار جفا دمیده پیرامن او</p></div>
<div class="m2"><p>همسایه بد خدای کس را مدهاد</p></div></div>