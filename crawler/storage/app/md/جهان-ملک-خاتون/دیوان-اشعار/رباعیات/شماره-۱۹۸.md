---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>آمد گه آن که خوش بود دامن گل</p></div>
<div class="m2"><p>آمد به فغان هزار پیرامن گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صبحدمی نسیم کویش بدمید</p></div>
<div class="m2"><p>زان روی چنان درید پیراهن گل</p></div></div>