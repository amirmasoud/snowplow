---
title: >-
    شمارهٔ ۲۵۵
---
# شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>دایم به خم زلف بتان در بندیم</p></div>
<div class="m2"><p>تا چند دل و دیده به ایشان بندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست شب فراقت ای دیده من</p></div>
<div class="m2"><p>ما باز سپر به روی آب افکندیم</p></div></div>