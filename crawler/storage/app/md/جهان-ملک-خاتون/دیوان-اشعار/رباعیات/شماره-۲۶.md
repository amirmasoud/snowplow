---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ما را غم عشق تو به جان آوردست</p></div>
<div class="m2"><p>و اوصاف جمالت به زبان آوردست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورنه ز من خاکی سرگشته چرا</p></div>
<div class="m2"><p>خون دلم از دیده روان آوردست</p></div></div>