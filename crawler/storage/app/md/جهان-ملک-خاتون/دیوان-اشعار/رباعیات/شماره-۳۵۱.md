---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>گفتم بر ما خرام ای سرو سهی</p></div>
<div class="m2"><p>کز هجر تو بر خاک نشستست رهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت آنکه شبی رسد به وصلت صنما</p></div>
<div class="m2"><p>او را نبود هیچ بجز روز بهی</p></div></div>