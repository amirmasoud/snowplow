---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>از دست غمت به جان رسیدست دلم</p></div>
<div class="m2"><p>صد جامه جان ز غم دریدست دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس خار ز غم خورده و یک روز گلی</p></div>
<div class="m2"><p>از گلبن وصل تو نچیدست دلم</p></div></div>