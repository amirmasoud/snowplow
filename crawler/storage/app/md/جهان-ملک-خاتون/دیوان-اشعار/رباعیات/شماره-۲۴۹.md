---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>می‌آیی و لطف و کرمت می‌بینم</p></div>
<div class="m2"><p>آسایش جان در قدمت می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن وقت که غایبی همت می‌بینم</p></div>
<div class="m2"><p>هرجا که نگه می‌کنمت می‌بینم</p></div></div>