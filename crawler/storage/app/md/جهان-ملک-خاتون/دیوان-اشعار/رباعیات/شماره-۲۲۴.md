---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>بر آتش عشق تو کبابی ارزم</p></div>
<div class="m2"><p>وز لعل لبت جام شرابی ارزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تصدیع ز حد گذشت ما را با تو</p></div>
<div class="m2"><p>ای جان و جهان مگر جوابی ارزم</p></div></div>