---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>ای دل دم عشق یار زین بیش مزن</p></div>
<div class="m2"><p>بیگانه شدی تو لاف زین بیش مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پای درآمدی تو بیهوده دو دست</p></div>
<div class="m2"><p>از درد فراق بر سر خویش مزن</p></div></div>