---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>با خصم بگو چند کنی مکر و فسون</p></div>
<div class="m2"><p>تا چند کشم جفای هر سفله دون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما معتکف پرده اسرار شدیم</p></div>
<div class="m2"><p>تا خود فلک از پرده چه آرد بیرون</p></div></div>