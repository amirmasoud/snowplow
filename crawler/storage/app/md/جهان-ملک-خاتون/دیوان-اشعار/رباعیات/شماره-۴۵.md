---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست دل از دست فراقت خونست</p></div>
<div class="m2"><p>در خون دلم روی جان گلگونست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیلی صفتا ببین که دیوانه دلم</p></div>
<div class="m2"><p>از روز فراقت ای صنم مجنونست</p></div></div>