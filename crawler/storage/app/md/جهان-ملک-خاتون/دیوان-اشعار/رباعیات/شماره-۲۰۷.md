---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>بیمارم و کس نیست که آبی دهدم</p></div>
<div class="m2"><p>ور ناله زنم دمی جوابی دهدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچم به جهان نیست مگر لطف طبیب</p></div>
<div class="m2"><p>کز خون جگر مگر شرابی دهدم</p></div></div>