---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>در عشق رخت ز من رمیدست دلم</p></div>
<div class="m2"><p>غمهای تو را به جان خریدست دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دید به بستان جهان سرو روان</p></div>
<div class="m2"><p>صد درد ز بالای تو چیدست دلم</p></div></div>