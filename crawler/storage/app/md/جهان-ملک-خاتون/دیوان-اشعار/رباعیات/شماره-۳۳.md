---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>از درد بمردیم دوایی بفرست</p></div>
<div class="m2"><p>بی برگان را برگ و نوایی بفرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری گرهی به کار خلق افتاده</p></div>
<div class="m2"><p>یارب ز کرم گره گشایی بفرست</p></div></div>