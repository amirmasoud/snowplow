---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>گفتم که دگر چشم به دلبر نکنم</p></div>
<div class="m2"><p>صوفی شوم و گوش به منکر نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم که خلاف طبع موزون منست</p></div>
<div class="m2"><p>توبت کردم که توبه دیگر نکنم</p></div></div>