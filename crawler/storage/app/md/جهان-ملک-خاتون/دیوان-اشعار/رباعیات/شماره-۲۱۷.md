---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>تا کی غم این جهان فرسوده خورم</p></div>
<div class="m2"><p>تا چند جفای مردم سفله برم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خال بتان حال دلم گشت تباه</p></div>
<div class="m2"><p>در چرخ کبودجامه خود نیست کرم</p></div></div>