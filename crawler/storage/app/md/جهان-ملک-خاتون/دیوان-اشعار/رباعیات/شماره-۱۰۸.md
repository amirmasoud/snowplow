---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>تا کی دلم از هجر تو پر خون باشد</p></div>
<div class="m2"><p>وز خانه اختیار بیرون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشتاق به وصلت من و از من تو ملول</p></div>
<div class="m2"><p>و این نیز هم از طالع وارون باشد</p></div></div>