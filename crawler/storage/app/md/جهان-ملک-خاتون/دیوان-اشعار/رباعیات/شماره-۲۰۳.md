---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>در باغ رخ تو گل به بار است مدام</p></div>
<div class="m2"><p>و آن نرگس مست پر خمارست مدام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این تازه گل طبع من از جور فلک</p></div>
<div class="m2"><p>دریاب که هم صحبت خارست مدام</p></div></div>