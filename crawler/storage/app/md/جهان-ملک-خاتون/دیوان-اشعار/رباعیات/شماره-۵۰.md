---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>بر ذکر تو دایماً زبانم جاریست</p></div>
<div class="m2"><p>همراه تو دل به خواب و در بیداریست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی کرم نظر به حالم فرمای</p></div>
<div class="m2"><p>کاین دل به غم عشق تو بس بازاریست</p></div></div>