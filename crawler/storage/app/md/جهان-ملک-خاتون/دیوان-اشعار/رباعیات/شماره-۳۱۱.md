---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>جانا به کمال حسن مغرور مشو</p></div>
<div class="m2"><p>وز راه وفا و مردمی دور مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل چو طبیب ما ندارد رحمی</p></div>
<div class="m2"><p>از درد فراق دوست رنجور مشو</p></div></div>