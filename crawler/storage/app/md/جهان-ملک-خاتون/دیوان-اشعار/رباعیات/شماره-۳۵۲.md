---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>مانند فلک نیست جهان فرسایی</p></div>
<div class="m2"><p>هردم کند اندیشه و هر شب رایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چرخ فلک دل منه ای یار از آنک</p></div>
<div class="m2"><p>چون دلبر بی وفاست هر شب جایی</p></div></div>