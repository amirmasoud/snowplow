---
title: >-
    شمارهٔ ۱۴۸
---
# شمارهٔ ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>آمد گه بلبل و گل و فصل بهار</p></div>
<div class="m2"><p>خواهم لب جوی و خلوت و بوس و کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرّم تن آنکه بخت یارش باشد</p></div>
<div class="m2"><p>مسکین دل آنکه دور باشد از یار</p></div></div>