---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>از رنگ رخ تو گل به رنگ آوردیم</p></div>
<div class="m2"><p>وز قد تو سرو را به تنگ آوردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که دریدیم گریبان فراق</p></div>
<div class="m2"><p>تا دامن وصل تو به چنگ آوردیم</p></div></div>