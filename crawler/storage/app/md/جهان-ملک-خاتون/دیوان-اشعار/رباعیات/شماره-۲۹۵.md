---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>ای بی رخ دوست رفته خواب از دیده</p></div>
<div class="m2"><p>خوناب روان گشته چو آب از دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت آن دو لعل شکّر خایش</p></div>
<div class="m2"><p>دایم چکدم دُرّ خوشاب از دیده</p></div></div>