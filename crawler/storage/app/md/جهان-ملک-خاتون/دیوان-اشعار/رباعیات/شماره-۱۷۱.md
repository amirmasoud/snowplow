---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>من دل به تو داده ام نکو می دارش</p></div>
<div class="m2"><p>زنهار که بیش از این مکن آزارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دلشده رحم کن تو ای دوست چو نیست</p></div>
<div class="m2"><p>جز لطف تو ای دوست کسی غمخوارش</p></div></div>