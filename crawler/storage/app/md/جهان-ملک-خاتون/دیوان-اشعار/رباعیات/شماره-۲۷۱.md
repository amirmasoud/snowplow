---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>ببرید ز من نگار مسکین دل من</p></div>
<div class="m2"><p>مهجور شد از وصال مسکین دل من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننواخت مرا شبی به وصلش چه کنم</p></div>
<div class="m2"><p>زان روی چنین شدست مسکین دل من</p></div></div>