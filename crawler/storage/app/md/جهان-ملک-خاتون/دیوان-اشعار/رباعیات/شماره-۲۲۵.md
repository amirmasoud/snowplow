---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>چون شمع ز سر تا به قدم می سوزم</p></div>
<div class="m2"><p>من سوخته ام دگر به غم می سوزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت شیرین لب آن یار عزیز</p></div>
<div class="m2"><p>یک دم نه که تا به صبحدم می سوزم</p></div></div>