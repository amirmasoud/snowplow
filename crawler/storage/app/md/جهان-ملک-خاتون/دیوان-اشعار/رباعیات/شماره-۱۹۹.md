---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>ای بر رخ زیبات شده مجنون گل</p></div>
<div class="m2"><p>بی مهر و وفا مباش تو همچون گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیلی صفتا ببست گل بار سفر</p></div>
<div class="m2"><p>از باغ خدای را مبر بیرون گل</p></div></div>