---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم که با تو رازی دارم</p></div>
<div class="m2"><p>با حضرت دوست من نیازی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیچاره چرا چنین شوی سرگردان</p></div>
<div class="m2"><p>چون هست یقین که چاره سازی دارم</p></div></div>