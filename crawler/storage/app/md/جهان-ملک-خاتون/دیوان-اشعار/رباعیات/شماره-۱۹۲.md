---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>در صبحدمی که خوش سراید بلبل</p></div>
<div class="m2"><p>در باغ فتد ز عشق رویش غلغل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا چه کنم ناله ز جان گر نکنم</p></div>
<div class="m2"><p>در عشق رخ گلی و گفتم مل مل</p></div></div>