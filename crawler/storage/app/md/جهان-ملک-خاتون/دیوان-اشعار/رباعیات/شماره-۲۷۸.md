---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>از غمزه مست دوست در خوابم من</p></div>
<div class="m2"><p>وز تاب شب هجر تو در تابم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوچه وصل تو منم در تک و پوی</p></div>
<div class="m2"><p>وز تاب شب هجر تو در تابم من</p></div></div>