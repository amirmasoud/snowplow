---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>از جور زمانه بر دلم بار بسیست</p></div>
<div class="m2"><p>یارم نه به دست لیکن اغیار بسیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجریست دلم ولیک با این همه سوز</p></div>
<div class="m2"><p>از جور فلک ملول از دست خسیست</p></div></div>