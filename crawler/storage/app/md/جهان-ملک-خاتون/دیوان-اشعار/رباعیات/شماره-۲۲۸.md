---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>ای عشق رخت برده مرا خواب از چشم</p></div>
<div class="m2"><p>سودای توأم گشاده خوناب از چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که یاقوت درخشان باشد</p></div>
<div class="m2"><p>با رنگ رخت ببرده ای آب از چشم</p></div></div>