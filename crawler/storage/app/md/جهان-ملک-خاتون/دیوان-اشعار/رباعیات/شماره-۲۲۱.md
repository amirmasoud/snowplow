---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>من پیش دو چشم نیمه مستت میرم</p></div>
<div class="m2"><p>و آنگه به لبان می پرستت میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی به سر خسته هجرانت نه</p></div>
<div class="m2"><p>ای دیده من که پیش دستت میرم</p></div></div>