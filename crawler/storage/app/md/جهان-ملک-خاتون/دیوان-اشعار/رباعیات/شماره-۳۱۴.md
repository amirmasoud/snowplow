---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>با عید وصال خویش آخر باز آی</p></div>
<div class="m2"><p>مردم ز خیال دوست آخر باز آی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر رمضان ز هجر تا کی باشم</p></div>
<div class="m2"><p>فرخنده هلال عید آخر باز آی</p></div></div>