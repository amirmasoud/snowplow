---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>تا چند کنی به عالمم سرگردان</p></div>
<div class="m2"><p>یارب که که گفت که ز ما سرگردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زلف توأم چو گو به چوگان بزند</p></div>
<div class="m2"><p>چون گوی کنم در قدمت سر، گردان</p></div></div>