---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>یارب به درت شب نیازست امشب</p></div>
<div class="m2"><p>با دوست مرا نوبت رازست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قلبست و خلاص خواهم از دل زان روی</p></div>
<div class="m2"><p>در بوته عشق در گدازست امشب</p></div></div>