---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>ابروی تو پیوسته به خم می بینم</p></div>
<div class="m2"><p>وز هجر تو چشم خود به نم می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر به که گویم غم جان و دل خود</p></div>
<div class="m2"><p>غمخوار درین زمانه کم می بینم</p></div></div>