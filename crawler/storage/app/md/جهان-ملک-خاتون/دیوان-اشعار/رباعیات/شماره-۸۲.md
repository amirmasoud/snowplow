---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>زلفت چو بنفشه پیچ و تابی دارد</p></div>
<div class="m2"><p>لعل تو چو آتشست و آبی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که چو چشم تو نباشد مستی</p></div>
<div class="m2"><p>گفتا که به هر گوشه خرابی دارد</p></div></div>