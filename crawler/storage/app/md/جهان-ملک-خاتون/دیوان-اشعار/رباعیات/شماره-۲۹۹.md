---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>آه از دل بیقرار و آه از دیده</p></div>
<div class="m2"><p>خون دل ما که کرد راه از دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دیده ندید دل بدو میل نکرد</p></div>
<div class="m2"><p>گاهی گله از دلست و گاه از دیده</p></div></div>