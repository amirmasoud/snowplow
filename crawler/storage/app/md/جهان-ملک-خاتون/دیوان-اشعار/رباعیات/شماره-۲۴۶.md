---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>با درد تو ای نگار درمان چه کنم</p></div>
<div class="m2"><p>با عشق رخ تو سر و سامان چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیدست مرا روی دلارات بگو</p></div>
<div class="m2"><p>در پای خیال دوست قربان چه کنم</p></div></div>