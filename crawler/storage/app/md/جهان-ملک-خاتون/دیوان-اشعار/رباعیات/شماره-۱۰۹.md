---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بیچاره دلم زبون گردون باشد</p></div>
<div class="m2"><p>دایم ز غم زمانه پر خون باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب از اندوه فلک می گرید</p></div>
<div class="m2"><p>تا عاقبت کار جهان چون باشد</p></div></div>