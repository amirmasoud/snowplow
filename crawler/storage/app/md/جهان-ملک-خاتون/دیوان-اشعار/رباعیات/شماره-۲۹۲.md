---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>لعلت نمکی در دل ریش افکنده</p></div>
<div class="m2"><p>فریاد به بیگانه و خویش افکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون چشم تو دید نرگس اندر بستان</p></div>
<div class="m2"><p>از خجلت و شرم سر به پیش افکنده</p></div></div>