---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>یارب نظری کن به جهان از کرمت</p></div>
<div class="m2"><p>زیرا که به پای بندگی بر درمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر یوسف مصری بفروشی به درم</p></div>
<div class="m2"><p>من یوسف مصرم و به جان می خرمت</p></div></div>