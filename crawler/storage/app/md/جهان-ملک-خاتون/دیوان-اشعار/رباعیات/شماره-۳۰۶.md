---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>ما بنده ز جانیم و تو فرمان می ده</p></div>
<div class="m2"><p>دردیست مرا از تو و درمان می ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل اگرت به دست افتد وصلش</p></div>
<div class="m2"><p>جان پیش رخ نگار آسان می ده</p></div></div>