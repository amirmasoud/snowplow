---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>دل تنگ مشو که جان به جانان برسد</p></div>
<div class="m2"><p>و این درد مرا نوبت درمان برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید ز روز وصل بر نتوان داشت</p></div>
<div class="m2"><p>باشد که شب هجر به پایان برسد</p></div></div>