---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>پروانه صفت پیش تو در پروازم</p></div>
<div class="m2"><p>در پای تو از عشق تو سر می بازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شمع در اشتیاق رویت صنما</p></div>
<div class="m2"><p>می سوزم و می گدازم و می سازم</p></div></div>