---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>گل با رخ دوست بی نسق افتاده</p></div>
<div class="m2"><p>در مجلس رندان به ورق افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روی تو را بدید نیکوش ببین</p></div>
<div class="m2"><p>کز شرم رخ تو در عرق افتاده</p></div></div>