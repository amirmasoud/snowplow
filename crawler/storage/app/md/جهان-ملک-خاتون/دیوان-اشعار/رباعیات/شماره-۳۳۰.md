---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>افتاده مرا با سر زلفت هوسی</p></div>
<div class="m2"><p>با دل نبود عقل مرا دست رسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرّ غم تو نهفته در سینه شدم</p></div>
<div class="m2"><p>واقف نبود جز دل من هیچکسی</p></div></div>