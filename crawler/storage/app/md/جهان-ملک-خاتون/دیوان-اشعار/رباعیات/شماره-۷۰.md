---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>جز وصل توأم هیچ نمی باید هیچ</p></div>
<div class="m2"><p>جز یاد توأم هیچ نمی باید هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچست دهان تنگت ای جان و دلم</p></div>
<div class="m2"><p>زان لب بجز از هیچ نمی خواهد هیچ</p></div></div>