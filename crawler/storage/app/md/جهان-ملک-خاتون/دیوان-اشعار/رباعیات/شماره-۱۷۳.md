---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>گل کیست به نزد عارض چون سمنش</p></div>
<div class="m2"><p>گر لاف زند برون کنم از چمنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پیش لبش غنچه دهن بگشاید</p></div>
<div class="m2"><p>چون باد بیایم و بدرّم دهنش</p></div></div>