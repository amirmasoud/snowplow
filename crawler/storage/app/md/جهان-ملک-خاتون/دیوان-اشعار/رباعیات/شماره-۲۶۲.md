---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>زنهار که دل منه تو بر حال جهان</p></div>
<div class="m2"><p>تا خود چه توان برد ز احوال جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو همدم خویش باش در گوشه ی صبر</p></div>
<div class="m2"><p>تا خود به کجا می رسد احوال جهان</p></div></div>