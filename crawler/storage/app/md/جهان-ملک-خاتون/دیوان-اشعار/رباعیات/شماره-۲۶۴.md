---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>ذوقی دگرست وقت گل می خوردن</p></div>
<div class="m2"><p>همچون درمش ورق به گرد آوردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وآنگه به سر ماه رخی سرو قدی</p></div>
<div class="m2"><p>از روی طرب نثار بر وی کردن</p></div></div>