---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>ما داد دل دوست ازین پس ندهیم</p></div>
<div class="m2"><p>دل را دگر از دست به هرکس ندهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق رخ تو بحر محیطست و دلم</p></div>
<div class="m2"><p>درّیست گرانمایه به هر خس ندهیم</p></div></div>