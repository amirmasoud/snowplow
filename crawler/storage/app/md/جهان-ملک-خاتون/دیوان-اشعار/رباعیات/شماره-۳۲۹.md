---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>یاد تو ز دل دور نباشد نفسی</p></div>
<div class="m2"><p>غیر از سر کوی تو ندارم هوسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستغرق بحر غصّه گشتم یارب</p></div>
<div class="m2"><p>فریاد رسم که نیست فریادرسی</p></div></div>