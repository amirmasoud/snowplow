---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>صبحی به چمن سوسن آزاد بخاست</p></div>
<div class="m2"><p>وز قامت خود صحن چمن می آراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخسار چو لاله اش بدیدم گفتم</p></div>
<div class="m2"><p>در سوخته خرمن زدن آتش نه رواست</p></div></div>