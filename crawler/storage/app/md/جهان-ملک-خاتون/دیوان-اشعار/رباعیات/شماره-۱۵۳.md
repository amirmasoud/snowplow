---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>رنگ رخ تو گلست و لعل تو شکر</p></div>
<div class="m2"><p>نقل لب تو نبات و نقل تو شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قند و شکر و نبات فرع لب تست</p></div>
<div class="m2"><p>حقّا که تو خود قندی و اصل تو شکر</p></div></div>