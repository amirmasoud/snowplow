---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>از عکس رخ تو گل و گلزار خجل</p></div>
<div class="m2"><p>از رنگ لب لعل تو گلنار خجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پیش قد و قامت تو سرو سهی</p></div>
<div class="m2"><p>یکباره نگویم که دو صد باره خجل</p></div></div>