---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>گر من ز غمت جامه دل چاک زنم</p></div>
<div class="m2"><p>آتش ز سر سوز بر افلاک زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آه من دلشده ای ماه بترس</p></div>
<div class="m2"><p>زیرا که من آه از دل غمناک زنم</p></div></div>