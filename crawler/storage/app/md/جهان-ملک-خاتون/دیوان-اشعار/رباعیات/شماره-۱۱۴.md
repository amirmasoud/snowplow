---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>خورشید رخ تو را ز جان خواسته شد</p></div>
<div class="m2"><p>از شرم رخت تمام مه کاسته شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیرامن عارض تو خطّی بدمید</p></div>
<div class="m2"><p>گل بود به سبزه نیز آراسته شد</p></div></div>