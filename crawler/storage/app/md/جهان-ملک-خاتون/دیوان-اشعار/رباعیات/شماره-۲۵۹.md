---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>در هجر رخت دلم ملولست ز جان</p></div>
<div class="m2"><p>زین بیش مرا به جان شیرین مرسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبرم به غم روی تو امکان نبود</p></div>
<div class="m2"><p>دریاب مرا به وصلت ای جان و جهان</p></div></div>