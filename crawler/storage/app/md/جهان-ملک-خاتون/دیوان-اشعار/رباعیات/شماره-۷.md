---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>عشّاق به درگهت اسیرند بیا</p></div>
<div class="m2"><p>بدخویی تو بر تو نگیرند بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جور و جفا که کرده ای معذوری</p></div>
<div class="m2"><p>زان پیش که عذرت نپذیرند بیا</p></div></div>