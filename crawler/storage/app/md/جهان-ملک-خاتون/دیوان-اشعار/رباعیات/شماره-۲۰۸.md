---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>بر یاد لبت شبی به روز آوردم</p></div>
<div class="m2"><p>چون شمع همه گریه و سوز آوردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوقی به دلم رسید و دل زنده شدم</p></div>
<div class="m2"><p>چون یاد رخ جهان فروز آوردم</p></div></div>