---
title: >-
    شمارهٔ ۲۰۰
---
# شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>تخت چمن جهان که آراست چو گل</p></div>
<div class="m2"><p>در جمله جهان مگو که زیباست چو گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در صف خوبان جهان بنشینی</p></div>
<div class="m2"><p>رخسار دلارای تو پیداست چو گل</p></div></div>