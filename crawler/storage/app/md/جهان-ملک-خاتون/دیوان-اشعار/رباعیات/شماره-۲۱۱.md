---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>ای بر دل و جان ز هجر او صد بارم</p></div>
<div class="m2"><p>مردم ز غمش نخورد غم یکبارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم باری وصال جان دریابم</p></div>
<div class="m2"><p>مقبول نشد نداد دلبر بارم</p></div></div>