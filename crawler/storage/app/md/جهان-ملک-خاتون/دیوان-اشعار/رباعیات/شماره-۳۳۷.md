---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>مجنون نفسی بزد که لیلی لیلی</p></div>
<div class="m2"><p>آخر ز چه نیستت به سویم میلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیلی به زبان حال گفتا مجنون</p></div>
<div class="m2"><p>در دیده ما شد از خیالت خیلی</p></div></div>