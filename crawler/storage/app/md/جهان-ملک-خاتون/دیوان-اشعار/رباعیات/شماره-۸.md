---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>عمریست که تا از تو جداییم بیا</p></div>
<div class="m2"><p>در درد به امّید وفاییم بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخاسته از سر جهان بنشستیم</p></div>
<div class="m2"><p>بر خاک در تو بی نواییم بیا</p></div></div>