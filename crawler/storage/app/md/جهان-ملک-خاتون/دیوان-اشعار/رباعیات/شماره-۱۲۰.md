---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>دل داد به دست باد پیغامی چند</p></div>
<div class="m2"><p>کز لعل توکی نوش کنم جامی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چشم تو یا فتنه دور قمرست</p></div>
<div class="m2"><p>وین زلف مسلسلت یا دامی چند</p></div></div>