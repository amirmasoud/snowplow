---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>تا آرزوی روی نگارم باشد</p></div>
<div class="m2"><p>در دیده جان ز غم غبارم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که به دل آب صبوری می زن</p></div>
<div class="m2"><p>بر آتش عشق چون قرارم باشد</p></div></div>