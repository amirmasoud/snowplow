---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>در پای گل و سرو دلم کرد مقام</p></div>
<div class="m2"><p>آوخ چه شود اگر دهد دست مدام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عمر شود تمام در حضرت دوست</p></div>
<div class="m2"><p>شکرانه دهم به وصلت ای ماه تمام</p></div></div>