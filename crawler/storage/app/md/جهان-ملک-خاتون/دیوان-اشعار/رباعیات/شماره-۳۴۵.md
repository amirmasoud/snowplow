---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>ای دوست چه شد چه شد که یادم نکنی</p></div>
<div class="m2"><p>یک لحظه به وصل خویش شادم نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقصود من از جهان وصال تو بود</p></div>
<div class="m2"><p>آخر ز چه رو دمی مرادم نکنی</p></div></div>