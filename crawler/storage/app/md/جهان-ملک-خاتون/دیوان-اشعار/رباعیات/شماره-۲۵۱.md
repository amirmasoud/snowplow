---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>تا چند ز هجر تو زبون بنشینم</p></div>
<div class="m2"><p>در دیده خود میان خون بنشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آینه بین جمال و انصاف بده</p></div>
<div class="m2"><p>بی روی تو ای نگار چون بنشینم</p></div></div>