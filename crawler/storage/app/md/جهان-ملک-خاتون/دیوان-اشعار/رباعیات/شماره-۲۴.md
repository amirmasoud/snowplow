---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>اندر خور همّتم جهانی هیچست</p></div>
<div class="m2"><p>آن را چه محل بود که جانی هیچست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در گذر آید آن سهی سرو روان</p></div>
<div class="m2"><p>جان گر بفشانیم روانی هیچست</p></div></div>