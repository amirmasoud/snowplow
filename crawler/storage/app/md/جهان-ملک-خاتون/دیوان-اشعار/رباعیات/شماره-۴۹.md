---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>فریاد ز جور دشمن و فرقت دوست</p></div>
<div class="m2"><p>کاین هر دو بلا به نزد و امّا نه نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند جفا بسی نه بر حق الحق</p></div>
<div class="m2"><p>«از شیشه همان برون تراود که دروست»</p></div></div>