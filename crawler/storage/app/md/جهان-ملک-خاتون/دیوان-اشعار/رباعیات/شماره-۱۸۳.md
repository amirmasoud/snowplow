---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>گفتم که چو ابرویت که دیدست هلال</p></div>
<div class="m2"><p>گفتا که چنین در شب عیدست هلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم دو هلال در مهی بس عجبست</p></div>
<div class="m2"><p>گفتا به دو هفته مه بعیدست هلال</p></div></div>