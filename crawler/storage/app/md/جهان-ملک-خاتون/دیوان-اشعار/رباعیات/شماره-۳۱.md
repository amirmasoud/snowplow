---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>در دیده دلم خیال رویش می بست</p></div>
<div class="m2"><p>دلبر به کرشمه گفت زان نرگس مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش باش که کام تو برآرم روزی</p></div>
<div class="m2"><p>بخشیدن مست اگر بود دست به دست</p></div></div>