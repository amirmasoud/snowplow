---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>گفتم که جهان مگر وفا خواهد کرد</p></div>
<div class="m2"><p>دردیست مرا و او دوا خواهد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چرخ فلک نگر که در هفته و ماه</p></div>
<div class="m2"><p>ای دیده چه ها کرد و چه ها خواهد کرد</p></div></div>