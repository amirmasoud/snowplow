---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>از بس که بیازرد دل دشمن و دوست</p></div>
<div class="m2"><p>گویی به گناه هیچ کندندش پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقتی غم او بر همه دلها بودی</p></div>
<div class="m2"><p>اکنون همه غمهای جهان بر دل اوست</p></div></div>