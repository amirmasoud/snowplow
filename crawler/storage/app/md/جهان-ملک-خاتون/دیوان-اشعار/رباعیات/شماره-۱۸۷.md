---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>در صبح رخ دوست نیاز آرد دل</p></div>
<div class="m2"><p>در طاق دو ابروش نماز آرد دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنوازدم از دولت وصلش شبکی</p></div>
<div class="m2"><p>زین بیش بگوی تا نیازارد دل</p></div></div>