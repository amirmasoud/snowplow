---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>اسرار تو در سینه نهانست مرا</p></div>
<div class="m2"><p>وز دیده سرشک خون روانست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سر به فدای راه عشقت کردم</p></div>
<div class="m2"><p>ای همنفسان چه جای جانست مرا</p></div></div>