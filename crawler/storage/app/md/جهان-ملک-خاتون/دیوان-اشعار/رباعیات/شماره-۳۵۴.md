---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>چون رای بلند تو نباشد رایی</p></div>
<div class="m2"><p>مثل رخ خوب تو جهان آرایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز درگه تو نیست مرا ملجایی</p></div>
<div class="m2"><p>چون بنده تو را بسیست در هر جایی</p></div></div>