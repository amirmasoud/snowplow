---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>جانا دل من بر سر عهدست هنوز</p></div>
<div class="m2"><p>عهدی که ببست با تو نشکست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجروح شد از خار جفایت دل و نیست</p></div>
<div class="m2"><p>در باغ وصال بر گلش دست هنوز</p></div></div>