---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای دل گل روی یار دیدن چه خوش است</p></div>
<div class="m2"><p>وز لعل لبانش بوسه چیدن چه خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک لحظه وصال او به بازار غمش</p></div>
<div class="m2"><p>دل کرده فدا به جان خریدن چه خوش است</p></div></div>