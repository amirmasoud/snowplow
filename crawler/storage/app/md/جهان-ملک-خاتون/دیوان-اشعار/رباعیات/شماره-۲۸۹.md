---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>بیداد به من ز حد بشد دادم ده</p></div>
<div class="m2"><p>تا چند خورم غم، تو دلی شادم ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر نفسی که می زنم در شب و روز</p></div>
<div class="m2"><p>نامی ز هزار و یک یکی یادم ده</p></div></div>