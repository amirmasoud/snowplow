---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>یارب تو روا مدار دل تنگ مرا</p></div>
<div class="m2"><p>مپسند در این روز بدین ننگ مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای چرخ کبود حرفه اینت بترست</p></div>
<div class="m2"><p>اسب همه رهوار و خر لنگ مرا</p></div></div>