---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>افتاده مرا دیده به یاری نازک</p></div>
<div class="m2"><p>دل بسته به روی گلعذاری نازک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل که کشی بار نگاری نازک</p></div>
<div class="m2"><p>یک روی جهان دشمن و کاری نازک</p></div></div>