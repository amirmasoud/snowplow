---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>در آتش روی دوست بگداخت دلم</p></div>
<div class="m2"><p>جان و سر و عمر خویش در باخت دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند چرا نساختی با غم او</p></div>
<div class="m2"><p>گفتم چه کنم چون که نمی ساخت دلم</p></div></div>