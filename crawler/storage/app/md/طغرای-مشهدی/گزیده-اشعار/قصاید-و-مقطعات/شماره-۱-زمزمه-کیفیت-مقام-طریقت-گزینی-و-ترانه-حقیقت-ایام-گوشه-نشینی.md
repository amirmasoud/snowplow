---
title: >-
    شمارهٔ ۱: زمزمهٔ کیفیت مقام طریقت گزینی و ترانهٔ حقیقت ایام گوشه نشینی
---
# شمارهٔ ۱: زمزمهٔ کیفیت مقام طریقت گزینی و ترانهٔ حقیقت ایام گوشه نشینی

<div class="b" id="bn1"><div class="m1"><p>ز قناعت نخورم غیر هوا در کشمیر</p></div>
<div class="m2"><p>به چنین رزق، کنم شکر خدا در کشمیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاسمین بهر چه آرد طبق خویش برم</p></div>
<div class="m2"><p>که نیم شیر طلب، صبح و مسا در کشمیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله پیشم چو نهد کاسه آش جشی</p></div>
<div class="m2"><p>چمچه ای زان نخورم همچو صبا در کشمیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل زرد آوردم گر ز مزعفر طبقی</p></div>
<div class="m2"><p>به مشامم ندهد بوی غذا در کشمیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه داند که کبابش نبود منظورم</p></div>
<div class="m2"><p>گرچه بی رزقی ام افکند ز پا در کشمیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرص خبازی اگر میده گوهر باشد</p></div>
<div class="m2"><p>نستانم ز کفش همچو گدا در کشمیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب من چون لب جو، نان کسی را نچشد</p></div>
<div class="m2"><p>موج آب از چه شود سفره گشا در کشمیر؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موی سر، گشته چو طاووس، کلاهم در فقر</p></div>
<div class="m2"><p>چه رباید ز سرم باد صبا در کشمیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرد غربت زدگی خرقه من گشته چو کوه</p></div>
<div class="m2"><p>لیک از نیش خسان، بخیه نما در کشمیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پای من چون کتل از مشت خسی یافته کفش</p></div>
<div class="m2"><p>نعل وارون چه زند کفش ربا در کشمیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شانه تابم ز مقید شدن لاله چو باد</p></div>
<div class="m2"><p>گر به دوشم کند از برگ، ردا در کشمیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سوی نرگس نکنم باز، کف همت خویش</p></div>
<div class="m2"><p>چون به دستم دهد از شاخ، عصا در کشمیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پا به خس خانه بلبل نگذارم از ننگ</p></div>
<div class="m2"><p>موسم گرم شدنهای هوا در کشمیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پنجه بر آتش گل، باز نخواهم کردن</p></div>
<div class="m2"><p>سردی ام گر بکشد فصل شتا در کشمیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکوه را چون ندهم رنگ ز نقاش بهار؟</p></div>
<div class="m2"><p>که به نامم شده طراح جفا در کشمیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نشود یک درمش رایج بازار چمن</p></div>
<div class="m2"><p>چون خزان گر بودم کان طلا در کشمیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همچو گل گر به دکان لعل بدخشان آرم</p></div>
<div class="m2"><p>کس نیاید به سرش غیر هوا در کشمیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر بود سبزه صفت، معدن فیروزه مرا</p></div>
<div class="m2"><p>به تعدی بردش باد فنا در کشمیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سوسن از تیرگی انگشت نما گردیده</p></div>
<div class="m2"><p>چه رساند به دلم برگ صفا در کشمیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جام نرگس که تهی بودن او مشهور است</p></div>
<div class="m2"><p>گشته لبریز دغایم همه جا در کشمیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رو به من طعن دمیدی ز لب نافرمان</p></div>
<div class="m2"><p>گر نمی بود زبانش به قفا در کشمیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گل خیری ز شرارت به سرم چون ندود؟</p></div>
<div class="m2"><p>که ز خیریت خود کرده ابا در کشمیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بوی کلفت نشنیدم ز گل تاج خروس</p></div>
<div class="m2"><p>تا نگردید قزلباش نما در کشمیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قد کشیده ست بنفشه ز سر دیوارم</p></div>
<div class="m2"><p>که نگونسار کند کاخ مرا در کشمیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کف نیلوفر اگر نایب مصقل گردد</p></div>
<div class="m2"><p>نشود از دل من زنگ زدا در کشمیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا نماید خفه از دود خودم غنچه صفت</p></div>
<div class="m2"><p>مشعل لاله نینگیخت ضیا در کشمیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گل ز سرپنجه گریبان مرا نگذارد</p></div>
<div class="m2"><p>دامنم را چو کند خار رها در کشمیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غیر سوزش نرسد نفع به چشم تر من</p></div>
<div class="m2"><p>گر زر جعفری افتد به جلا در کشمیر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مغز ما چشمه سودا شده از سنبل تر</p></div>
<div class="m2"><p>که دهد روغن بادام به ما در کشمیر؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کس نفهمید که ریحان چه کدورت دارد</p></div>
<div class="m2"><p>ز من عاجز بی برگ و نوا در کشمیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طفل شبنم همه جا بنگی مادرزاد است</p></div>
<div class="m2"><p>کار ازو چون طلبم همچو صبا در کشمیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تردماغی ز گل کوزه چه سان خواهم یافت</p></div>
<div class="m2"><p>که ز خشکی نکند نشو و نما در کشمیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گل عباسی ازان رو که به شه منسوب است</p></div>
<div class="m2"><p>می کند از من درویش ابا در کشمیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دل من لاله صفت چون نشود داغ فریب؟</p></div>
<div class="m2"><p>که شقایق دمد از تخم دغا در کشمیر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پیش ازان دم که رسد آتش گلنار به دود</p></div>
<div class="m2"><p>شعله افکند به مشت خس ما در کشمیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خارج آهنگ به گوش دلم آید صوتش</p></div>
<div class="m2"><p>چون شود مرغ سحر، نغمه سرا در کشمیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گربه بید، ره سگ صفتی پیش گرفت</p></div>
<div class="m2"><p>پاچه ام را نکند زخم چرا در کشمیر؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز سفیدار، سیاه است تنم چون سوسن</p></div>
<div class="m2"><p>بس که انداخت به من، برگ جفا در کشمیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا ربوده ست چنار از دل من سوز درون</p></div>
<div class="m2"><p>گرم نفرین شومش صبح و مسا در کشمیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گوش من چون نشود کر، که به تحریک نسیم</p></div>
<div class="m2"><p>عرعر افکند به هر سو عللا در کشمیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همچو ریحان ندهد شام مرا یک جو نور</p></div>
<div class="m2"><p>ز ارغوان گر شفق افتد به هوا در کشمیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تاک را شیوه بدمستی ازان خرم ساخت</p></div>
<div class="m2"><p>که پی لت شودم دست گشا در کشمیر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چشم من چون نخورد ترس، که هر نارونی</p></div>
<div class="m2"><p>کفچه ماری ست قوی ساخته پا در کشمیر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>افکند پنجه شمشاد به کار دل من</p></div>
<div class="m2"><p>عقده ای گر کند از طره جدا در کشمیر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دست جراح بهاری نبرد پی به رفو</p></div>
<div class="m2"><p>تیغ بید ار زندم زخم رسا در کشمیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به صنوبر چه کنم یکدلی خویش اظهار؟</p></div>
<div class="m2"><p>که دو سویم کشد از شاخ بلا در کشمیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بال مرغ نگهم زخم بود همچو تذرو</p></div>
<div class="m2"><p>نیزه سرو شده تا به هوا درکشمیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا توانم ز شط گریه خود بیرون شد</p></div>
<div class="m2"><p>می کنم بر لب «دل» مشق شنا در کشمیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سبزه «باغ نشاطم» به کدورت افکند</p></div>
<div class="m2"><p>سوی نخلش چه روم همچو صبا در کشمیر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>«فیض بخش» از اثر پست و بلند چمنش</p></div>
<div class="m2"><p>هر قدم گشت مرا رنج فزا در کشمیر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>«شاله مار» از دم عقرب خس و خار انگیزد</p></div>
<div class="m2"><p>تا کنندم ز گل نیش، فنا در کشمیر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تخم ابلیس، زمین دار «الهی باغ» است</p></div>
<div class="m2"><p>سیرگاهش نکنم چون صلحا در کشمیر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>«حسن آباد» قبیح از چه نباشد بر من؟</p></div>
<div class="m2"><p>که شد این غمکده از ظلم بنا در کشمیر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>«نور باغ» از ره ظلمت شده تاریک نشین</p></div>
<div class="m2"><p>گل او چون دهدم بوی ضیا در کشمیر؟</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>«ظفرآباد» که از فوج خزان دید شکست</p></div>
<div class="m2"><p>چه گل قدر برآرد بر ما در کشمیر؟</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هر که گردید دلیلم به ره «باغ نسیم»</p></div>
<div class="m2"><p>کرد سرگشته مرا همچو صبا در کشمیر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>«زعفران زار» که یک سیرگه مشهور است</p></div>
<div class="m2"><p>سیر او ریخت به من رنج و عنا در کشمیر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تا شود رهزن عقلم شجر «کاکاپور»</p></div>
<div class="m2"><p>ریشه بنگ دواند همه جا در کشمیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کلفت از چشمه «اچول» نه چنان گشت روان</p></div>
<div class="m2"><p>که به سویم ندود جوی فنا در کشمیر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>شتر عرعری آب که در «ورناک» است</p></div>
<div class="m2"><p>بهر ترسم فکند کف به هوا در کشمیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز «صفاپور» دلم چون به کدورت نرسد؟</p></div>
<div class="m2"><p>که شط او نزند موج صفا در کشمیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بخت برگشته، ز هر کوه به من کرد نصیب</p></div>
<div class="m2"><p>عوض مهرگیا، کینه گیا در کشمیر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>«پیر پنجال» که شد صومعه دار خنکی</p></div>
<div class="m2"><p>به مریدی چه کند گرم، مرا در کشمیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>«کوه ماران» زخس دامن زهرآلودش</p></div>
<div class="m2"><p>ریخت افعی به من بی سر و پا در کشمیر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پشته «تخت سلیمان» که ز گل یافت پری</p></div>
<div class="m2"><p>سایه اش دیو بود بر سر ما در کشمیر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>«کوه لار» از کمر خویش برافراخته تیغ</p></div>
<div class="m2"><p>چون نتازد به سرم بهر وغا در کشمیر؟</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>خاک کشمیر برانداخته تخم گل ذوق</p></div>
<div class="m2"><p>زان سبب قحط شده برگ حنا در کشمیر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>همچو نرگس نبود رنج مرا درمانی</p></div>
<div class="m2"><p>گر ببارد ز رگ ابر، دوا در کشمیر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون بنفشه ز نگونساری کاشانه تن</p></div>
<div class="m2"><p>نکنم فرق عروسی ز عزا در کشمیر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شبنم از چشم ترم آب در آرد به تغار</p></div>
<div class="m2"><p>گر بشویند گل و لاله، قبا در کشمیر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شهر و ده، کوه و بیابان، همه جا خار غم است</p></div>
<div class="m2"><p>گل شادی طلبم تا به کجا در کشمیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دختر رز که نپوشیده رخ از بی برگان</p></div>
<div class="m2"><p>خواهد از من زر گل، روی نما در کشمیر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>اشتها نیست که یک آب خورم تا «دهلی»</p></div>
<div class="m2"><p>بس که خوردم جگر تشنه، دغا در کشمیر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به نگهبانی چندال که دزد چمن است</p></div>
<div class="m2"><p>خضر را گم شده نعلین و عصا در کشمیر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>باد کز روی ادب، تخت سلیمان می برد</p></div>
<div class="m2"><p>زده بر «تخت سلیمان» سرپا در کشمیر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>عرق شرم توان یافت ز رخسار گلی</p></div>
<div class="m2"><p>اگر از هند بود تخم حیا در کشمیر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چه عجب گر گل بی عصمتی افتد به سرش</p></div>
<div class="m2"><p>مرغ حقگو چو کند پا به هوا در کشمیر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>صوفیان را نبود باب سر دوش صلاح</p></div>
<div class="m2"><p>صافی باده اگر نیست ردا در کشمیر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>باد از گل نتواند زر بی سود گرفت</p></div>
<div class="m2"><p>بس که رایج شده سود از علما در کشمیر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مفتی از شیخ، گواهی به چه سان گیرد وام</p></div>
<div class="m2"><p>وام شرعی نبود گر به ربا در کشمیر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>رو به بتخانه کند هیات محرابی را</p></div>
<div class="m2"><p>حاجی آرد چو به کف قبله نما در کشمیر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بوریایی که بود معبد زاهد را فرش</p></div>
<div class="m2"><p>می دهد بوی ریا تا همه جا در کشمیر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>سخن از طوطی و مینای دکن می دزدد</p></div>
<div class="m2"><p>نظم خوانی که کند نشو و نما در کشمیر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چون تنک مایه مضمون نشوم، کز غارت</p></div>
<div class="m2"><p>نقد و جنس سخنم گشت فنا در کشمیر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>جای آن هست که دستان زن نطقم زهراس</p></div>
<div class="m2"><p>همچو بلبل نکند صوت و صدا در کشمیر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مزد این شیوه که آیینه صفت یکرویم</p></div>
<div class="m2"><p>پشتم از بار ستم گشت دو تا در کشمیر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>دل بخت سیه از راحت من می ترکید</p></div>
<div class="m2"><p>گر نمی برد ز لاهور مرا در کشمیر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گشته پیدا ز نم چشم ترم عمانی</p></div>
<div class="m2"><p>چون جزیره نکند کار مخا(؟) در کشمیر؟</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>گر شهید است کسی کو به جفا می میرد</p></div>
<div class="m2"><p>می توان خواند مرا از شهدا در کشمیر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>زان جماعت که سر خویش دهم در رهشان</p></div>
<div class="m2"><p>نشنیده احدی نام وفا در کشمیر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>اختلاط که دهد برگ نشاطم، که مرا</p></div>
<div class="m2"><p>دشمنی چند بود دوست نما در کشمیر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>عکسشان تا زده بر جوی چمن، تیره دلی</p></div>
<div class="m2"><p>نیست با آینه آب، صفا در کشمیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>تخم این فرقه برافتد، که شد از کثرتشان</p></div>
<div class="m2"><p>باغ بر من چو قفس تنگ فضا در کشمیر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>کم نگشتند ز همسایگی ام اهل نفاق</p></div>
<div class="m2"><p>مگر آن سال که افتاد وبا در کشمیر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>طعنه مشک ختایی، شده مادر به ختن</p></div>
<div class="m2"><p>بس که پیدا شده مادر بخطا در کشمیر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>عیب طغرا نکنی بر سر این تندمقال</p></div>
<div class="m2"><p>گر بدانی که کشیده ست چها در کشمیر</p></div></div>