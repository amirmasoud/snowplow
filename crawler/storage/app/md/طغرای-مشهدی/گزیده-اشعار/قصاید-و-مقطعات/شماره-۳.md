---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>طغرا، به تیغ نطق، جهان را گرفته ای</p></div>
<div class="m2"><p>کم نیستی تو هم ز جهانگیر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیرافکنان رای تو صد ره فکنده اند</p></div>
<div class="m2"><p>پشت کمان به ترکش پر تیر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کشوری که تیغ خیالت علم شود</p></div>
<div class="m2"><p>روید غلاف شرم ز شمشیر آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آنکه می شود ز ره پرتوافکنی</p></div>
<div class="m2"><p>نظم تو دستمایه تسخیر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که بهر چه بی قدر مانده است</p></div>
<div class="m2"><p>بر صفحه وجود، چو تصویر آفتاب</p></div></div>