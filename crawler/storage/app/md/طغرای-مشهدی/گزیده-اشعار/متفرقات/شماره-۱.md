---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>برخورد چون به گل روی تو نظاره ما</p></div>
<div class="m2"><p>گل صد برگ نماید دل صد پاره ما</p></div></div>