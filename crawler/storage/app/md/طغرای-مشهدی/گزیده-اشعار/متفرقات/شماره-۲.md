---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>پایی به دیدن من غمناک برنداشت</p></div>
<div class="m2"><p>هرگز به رنگ سایه ام از خاک برنداشت</p></div></div>