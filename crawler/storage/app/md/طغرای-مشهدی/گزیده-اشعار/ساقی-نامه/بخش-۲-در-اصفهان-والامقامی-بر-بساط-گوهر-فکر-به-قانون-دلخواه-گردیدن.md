---
title: >-
    بخش ۲ - در اصفهان والامقامی بر بساط گوهر فکر به قانون دلخواه گردیدن
---
# بخش ۲ - در اصفهان والامقامی بر بساط گوهر فکر به قانون دلخواه گردیدن

<div class="b" id="bn1"><div class="m1"><p>چو مطرب کند ساز و برگ نوا</p></div>
<div class="m2"><p>رسد مرغ را خرمی در هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز باغ رخش کز طرب شد پدید</p></div>
<div class="m2"><p>توان دسته دسته گل ذوق چید...</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز صافی در آواز آن خوش سرود</p></div>
<div class="m2"><p>نمایان شده عکس آهنگ رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چنگ آرد آن زینت محفلم</p></div>
<div class="m2"><p>ز تکرار نغمه مکرر دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گردد سراینده پیشرو</p></div>
<div class="m2"><p>ز «تن تن» به مستان دهد جان نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب از نغمه گویی کرامات کرد</p></div>
<div class="m2"><p>که نارفته سیر مقامات کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوایی که در پرده غیب بود</p></div>
<div class="m2"><p>ز سرخانه نقش سلمک نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نغمه سنجی کند در عراق</p></div>
<div class="m2"><p>شود صبر اهل نشابور، طاق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عرب می دود از مقام حشم</p></div>
<div class="m2"><p>کزو بشنود نغمه ای در عجم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به یک گوشه گیری، قد چارگاه</p></div>
<div class="m2"><p>شده از فراق سه گاهش دو تاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز غم گر دل راستان درهم است</p></div>
<div class="m2"><p>به عزال معزول ساز غم است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به زابل چو صوتش کند ترکتاز</p></div>
<div class="m2"><p>بیاتش دود در جلو ز اهتزاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز آواز او پنجگاه از گوشت</p></div>
<div class="m2"><p>شش آوازه را هفتمین صوت گشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو کوچک ازو نی سواری کند</p></div>
<div class="m2"><p>بزرگ فغان را حصاری کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بس حسن صوتش بود دلربا</p></div>
<div class="m2"><p>مقام فدایش گرفته صبا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز نوروزخوانی درین کهنه دیر</p></div>
<div class="m2"><p>بهار گل نغمه شد وقت سیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو خارا ز ابریشم ساز بافت</p></div>
<div class="m2"><p>حسینی به وجه حسن پرده یافت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بت نغمه اش چون مبرقع شده</p></div>
<div class="m2"><p>نواخوان ماه مقنع شده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نماید چو از پرده، بسته نگار</p></div>
<div class="m2"><p>کند ساز تحسین حنابند یار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توان یافت از شعبه تازه اش</p></div>
<div class="m2"><p>که تا اصفهان رفته آوازه اش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به تصنیف ساز ره بوسلیک</p></div>
<div class="m2"><p>ز مغلوب خوانی ست غالب شریک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن تا ز نیریز وحدت نگفت</p></div>
<div class="m2"><p>نشد آشکارا سرود نهفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شهش چون به بزم خود آواز کرد</p></div>
<div class="m2"><p>ز والامقامی به شه ناز کرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نوا ریخت بر دوستان حجاز</p></div>
<div class="m2"><p>چه سان خواندش کس مخالف نواز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شترغو زده پشت پا بر جرس</p></div>
<div class="m2"><p>که زنگوله او بود دسترس</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سازش چو عشاق پرداختند</p></div>
<div class="m2"><p>ز صندوق دل، ارغنون ساختند ...</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نگارا ندانم چرا سرکشی</p></div>
<div class="m2"><p>به گلدستگی شعله آتشی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بی تو بینم صراحی به پیش</p></div>
<div class="m2"><p>نهم چون سبو دست بر حلق خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز تن چون مسافر شود هوش من</p></div>
<div class="m2"><p>برآواز پایت بود گوش من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز شوق تو پیچیده در نامه ام</p></div>
<div class="m2"><p>صدای غزلخوانی خامه ام</p></div></div>