---
title: >-
    بخش ۷ - غزلسرایی
---
# بخش ۷ - غزلسرایی

<div class="b" id="bn1"><div class="m1"><p>سخن چون سپاه بیان را گرفت</p></div>
<div class="m2"><p>به این فوج، تخت زبان را گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسلم بود خسروی بر سخن</p></div>
<div class="m2"><p>که دربسته، ملک دهان را گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بر بادپای نفس شد سوار</p></div>
<div class="m2"><p>دو صد قلعه گوش جان را گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صف آرای ابیات رنگین چو گشت</p></div>
<div class="m2"><p>اقالیم روح و روان را گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از سطر، لشکرکشی ساز کرد</p></div>
<div class="m2"><p>به یک نی سواری، جهان را گرفت</p></div></div>