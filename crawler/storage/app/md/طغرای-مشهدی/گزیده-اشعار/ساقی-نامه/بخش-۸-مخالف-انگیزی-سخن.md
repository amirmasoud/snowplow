---
title: >-
    بخش ۸ - مخالف انگیزی سخن
---
# بخش ۸ - مخالف انگیزی سخن

<div class="b" id="bn1"><div class="m1"><p>درین عصر کز نطق جز رنج نیست</p></div>
<div class="m2"><p>سخن فهم هست و سخن سنج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فیض سخن قسمت من شده</p></div>
<div class="m2"><p>به من دوست از رشک، دشمن شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال سخن بهتر از وصل اوست</p></div>
<div class="m2"><p>که باب حسد، معنی فصل اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن چون درآید به کاخ ورق</p></div>
<div class="m2"><p>ره حرف گیران پذیرد نسق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی گوید این قافیه گشته لنگ</p></div>
<div class="m2"><p>چه سان همرهی کرده با نظم شنگ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی از ردیفش بود نکته گیر</p></div>
<div class="m2"><p>که این لفظ اولی چرا شد اخیر؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن را ازین شعر نافهم چند</p></div>
<div class="m2"><p>تو گویی که معنی فتاده به بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر بخت یار است با نکته دان</p></div>
<div class="m2"><p>کلامش بخوبی دود بر زبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گردیده بخت سیه، دشمنم</p></div>
<div class="m2"><p>چه سان رو دهد فیض خوش گفتنم؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا از سخن حل نشد مشکلی</p></div>
<div class="m2"><p>چو خامه ندیدم ازو حاصلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درین شغل کز وی مراکام نیست</p></div>
<div class="m2"><p>سرانجام باید، سرانجام نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شود چون رقم کاری شعر، فرض</p></div>
<div class="m2"><p>دوات و قلم می ستانم به قرض ...</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چه یاری که از من ندیده سخن</p></div>
<div class="m2"><p>ولیکن نکرده تلافی به من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیفشرده پایی به همراهی ام</p></div>
<div class="m2"><p>نبرده به سر منزل شاهی ام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سخن عزت شهریارم نداد</p></div>
<div class="m2"><p>به گفتار خود اعتبارم نداد ...</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخن مایه درد سر بوده است</p></div>
<div class="m2"><p>ز سوداش چندین ضرر بوده است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر از توارد شود عیب دار</p></div>
<div class="m2"><p>به دزدی دهد عیبجویش قرار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وگر بی توارد درآید به فرد</p></div>
<div class="m2"><p>ازو بیسوادان برآرند گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سخن گر چو مصحف بود در نظام</p></div>
<div class="m2"><p>برون ناید از دست کاتب تمام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قلم چون به تحریر او سر کند</p></div>
<div class="m2"><p>به خاک سیاهش برابر کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به لب از نسیم دل آزردگی</p></div>
<div class="m2"><p>بیانم بود گرم افسردگی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن نیست کآورده ام تازه پیش</p></div>
<div class="m2"><p>تراشیده ام دشمنی بهر خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنم عمر خود گر به تصحیح صرف</p></div>
<div class="m2"><p>نگردد دلم ایمن از سهو حرف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شب و روز درمانده ام چون قلم</p></div>
<div class="m2"><p>به دست سیه کافران رقم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه نظم و چه نثر، آنچه آید به لب</p></div>
<div class="m2"><p>بود وقت تحریر، محنت طلب ...</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مقرر چنین است در وقت فکر</p></div>
<div class="m2"><p>که ناظم شود خوش ز مضمون بکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو مضمون بکری به من رو نمود</p></div>
<div class="m2"><p>خورم غصه کاین را که خواهد ربود؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گل باغ طبعم ز بیم درو</p></div>
<div class="m2"><p>بود پیش فصل خموشی گرو</p></div></div>