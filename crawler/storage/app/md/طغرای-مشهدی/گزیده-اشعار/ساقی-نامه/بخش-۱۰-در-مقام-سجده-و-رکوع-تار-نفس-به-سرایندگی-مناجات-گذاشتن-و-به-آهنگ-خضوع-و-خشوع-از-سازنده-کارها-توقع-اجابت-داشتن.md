---
title: >-
    بخش ۱۰ - در مقام سجده و رکوع، تار نفس به سرایندگی مناجات گذاشتن و به آهنگ خضوع و خشوع، از سازنده کارها توقع اجابت داشتن
---
# بخش ۱۰ - در مقام سجده و رکوع، تار نفس به سرایندگی مناجات گذاشتن و به آهنگ خضوع و خشوع، از سازنده کارها توقع اجابت داشتن

<div class="b" id="bn1"><div class="m1"><p>الهی به اعزاز پیر مغان</p></div>
<div class="m2"><p>که جبریل شد بهر او زندخوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تعظیم خلوت نشینان دیر</p></div>
<div class="m2"><p>به قرب صنمهای لاهوت سیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بتخانه و حق پرستان او</p></div>
<div class="m2"><p>به کوی خرابات و مستان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خمهای سبز شراب طهور</p></div>
<div class="m2"><p>که روشن ضمیرند چون نخل طور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رنگینی باده لعل پوش</p></div>
<div class="m2"><p>به گل کاری جامه می فروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به ساغر که نگزیده اندیشه را</p></div>
<div class="m2"><p>به یک چشم دیده خم و شیشه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جامی که افتاده از روی دست</p></div>
<div class="m2"><p>به اشکی که غلتیده از چشم مست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مستان سرگرم جوش و خروش</p></div>
<div class="m2"><p>به گلبانگ پی در پی نوش نوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عقلی که در می پرستی بود</p></div>
<div class="m2"><p>به هوشی که در عین مستی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گل کاری نغمه های دو تار</p></div>
<div class="m2"><p>به آبی که جاری ست در جوی تار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به صندوق پرنغمه ارغنون</p></div>
<div class="m2"><p>که خالی نگردد چو افتد نگون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به قفل زبان دف پوست پوش</p></div>
<div class="m2"><p>که در گوش خود بر لب آرد خروش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ذوق جلاجل کز آواز دف</p></div>
<div class="m2"><p>زند چون سرایندگان کف به کف</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خونگرمی جامهای شراب</p></div>
<div class="m2"><p>به دلسوزی مرغهای کباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به مطرب که دستش بود همچو یشم</p></div>
<div class="m2"><p>نهد نی سرانگشت او را به چشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به دریای میخانه شوق تو</p></div>
<div class="m2"><p>به گرداب پیمانه ذوق تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به صهبای وحدت که داری به پیش</p></div>
<div class="m2"><p>به مستت که هرگز نیاید به خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به سوزی که در جان خورشید ماند</p></div>
<div class="m2"><p>به ذوقی که در طبع ناهید ماند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به آن گل که از تخم آتش دمید</p></div>
<div class="m2"><p>به آن مرغ کز شاخ معجز پرید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به شامی که در کلبه لاله است</p></div>
<div class="m2"><p>به صبحی که در خلوت ژاله است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به آن قطره کز بحر دارد خبر</p></div>
<div class="m2"><p>به آن ذره کز مهر یابد نظر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به تخت سعادت، به تاج شرف</p></div>
<div class="m2"><p>به فوج ولایت، به شاه نجف</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که از کافرستان هندم برآر</p></div>
<div class="m2"><p>به اسلام زار عراقم درآر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تنم را به دولتسرا راه ده</p></div>
<div class="m2"><p>سرم را سجود در شاه ده</p></div></div>