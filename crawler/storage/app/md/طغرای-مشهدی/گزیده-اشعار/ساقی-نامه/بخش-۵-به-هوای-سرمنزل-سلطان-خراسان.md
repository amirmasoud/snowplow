---
title: >-
    بخش ۵ - به هوای سرمنزل سلطان خراسان
---
# بخش ۵ - به هوای سرمنزل سلطان خراسان

<div class="b" id="bn1"><div class="m1"><p>سپهری ست مشهد سعادت نشان</p></div>
<div class="m2"><p>خیابان او نایب کهکشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک در خسرو هشتمین</p></div>
<div class="m2"><p>نهم آسمان گشته آن سرزمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگشتی چنین سدره رفعت مآب</p></div>
<div class="m2"><p>نخوردی گر از جوی آن شهر، آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو صد بحر عمان چوگردد روان</p></div>
<div class="m2"><p>نباشد به مقدار یک نهر آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خاکش مگو حرف مشک تتار</p></div>
<div class="m2"><p>که بر خاطرش می نشیند غبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن بوستان خوش آب و هوا</p></div>
<div class="m2"><p>ز گل می تراود چو بلبل نوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمستان او چون بهاری کند</p></div>
<div class="m2"><p>خزان هر طرف لاله کاری کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیم از چمن بس که گل گل شده</p></div>
<div class="m2"><p>به طاووسی دشت سنبل شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه بازار و کوچه، چه دیوار و در</p></div>
<div class="m2"><p>مصفا چو دکان آیینه گر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز یک خانه آن دیار طرب</p></div>
<div class="m2"><p>توان یافت آرایش صد حلب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر جا که نقاش مانی نگار</p></div>
<div class="m2"><p>رقم کرده خنیاگری بر جدار،</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کشیده چنان تار برساز او</p></div>
<div class="m2"><p>که درخانه پیچیده آواز او!</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بازار او کوچه باغ ارم</p></div>
<div class="m2"><p>متاع طرب می خرد دم بدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دکانها به آرایش گنجفه</p></div>
<div class="m2"><p>ز انبوه صنعت، کسادی خفه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مدارس ز درگاه حکمت گزین</p></div>
<div class="m2"><p>زده تخته بر فرق یونان زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هر جانب او دو صد مدرسه</p></div>
<div class="m2"><p>ز علم لدنی پر از وسوسه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گلچینی درگه بی نیاز</p></div>
<div class="m2"><p>چمن کرده در مسجد او نماز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آن تازه حمام رونق سرشت</p></div>
<div class="m2"><p>کند غسل جمعه، نسیم بهشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صبا چون به تهلیل پرداخته</p></div>
<div class="m2"><p>به آب خیابان وضو ساخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زآبش صدف گر شود بهره ور</p></div>
<div class="m2"><p>ناستد ز موج روانی گهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز جویش نهال است آب حیات</p></div>
<div class="m2"><p>ز خاکش بود سبز، شاخ نبات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روان گر شود هفت دریا قطار</p></div>
<div class="m2"><p>نخیزد صدا همچو یک آبشار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به حوضش نخوردی اگر غوطه ای</p></div>
<div class="m2"><p>نبستی فلک آبگون فوطه ای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو فواره برجسته از جای خویش</p></div>
<div class="m2"><p>به نسرین اختر زده پای خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر از سردی آب شد بی قرار</p></div>
<div class="m2"><p>خورد سبزه سیماب شبنم نهار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبا از حریر گل و یاسمین</p></div>
<div class="m2"><p>مهین حله ای بافت بهر زمین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بس تنگ شد جا ز ترتیب سرو</p></div>
<div class="m2"><p>نهد پای بر چشم نرگس، تذرو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درختان به دمسازی آب رود</p></div>
<div class="m2"><p>ز برگ طرب در مقام سرود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زمرد لباسان سرو سهی</p></div>
<div class="m2"><p>نکرده به رقص طرب کوتهی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رود از خیابان به هر خانه آب</p></div>
<div class="m2"><p>به رنگ ضو از چشمه آفتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در آن صحن از قاف تا قاف دهر</p></div>
<div class="m2"><p>کشیده ست چون جدول صبح نهر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به آلودگی چون نباشد روان؟</p></div>
<div class="m2"><p>کزو شسته شد نامه مجرمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خضر گر نمی بود سقای شاه</p></div>
<div class="m2"><p>به آب حیاتش که می داد راه؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو شد دیگ سرکار حضرت قطار</p></div>
<div class="m2"><p>به هر سو جهانی شود آش خوار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فلک از هلال آورد ظرف پیش</p></div>
<div class="m2"><p>که یابد ازان حصه ای بهر خویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز نان پزکه تفتان او گشته مهر</p></div>
<div class="m2"><p>به یک گرده مه رسیده سپهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نوازنده نوبت شاه دین</p></div>
<div class="m2"><p>دمامه نوازد ز چرخ برین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نقاره به سرکار چون شد نواخت</p></div>
<div class="m2"><p>مقام خوش آوازی خود شناخت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به آهنگ سرنا و صوت نفیر</p></div>
<div class="m2"><p>جوانانه رقصان شود چرخ پیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دم کرنایش برآرد چو گرد</p></div>
<div class="m2"><p>ز ایوان گردون طلا لاجورد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به هر جانب روضه آن امام</p></div>
<div class="m2"><p>کند سجده صد فیض بیت الحرام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شدی شسته گر لاجورد سپهر</p></div>
<div class="m2"><p>گرفتی زرافشان نمایش ز مهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شفق را نمودی به لعلی حساب</p></div>
<div class="m2"><p>سفیداب صبح ار شدی نیز باب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قلم را ز انداز طرح جدار</p></div>
<div class="m2"><p>به هر مو درآویخته صد بهار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نگون لاله زاری بود سقف او</p></div>
<div class="m2"><p>که سرچشمه فیض شد وقف او</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زرین گنبدش در صفاپروری</p></div>
<div class="m2"><p>بود خرمنی از گل جعفری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بر اوج پیمای این هفت پل</p></div>
<div class="m2"><p>مه و مهر از خرمن او دوگل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دل عندلیبان مقری خطاب</p></div>
<div class="m2"><p>مقید به گلدسته اش همچو آب</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه قالی ست فرش زمین حرم</p></div>
<div class="m2"><p>شده فرش از خوش هوایی ارم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در کبریا نصب شد در رهش</p></div>
<div class="m2"><p>بود پرده غیب از درگهش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز قندیلهای پسر از آب و تاب</p></div>
<div class="m2"><p>نمایان ز هر گوشه صد آفتاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درین آستان گشته هر شامگاه</p></div>
<div class="m2"><p>سپهر برین مشعل افروز ماه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>پی نذر درگاه او آسمان</p></div>
<div class="m2"><p>کند نقره صبح را شمعدان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>شب آید بدین روضه از راه نور</p></div>
<div class="m2"><p>به پروانه حق، چراغان طور</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دعاها رسیده ست بی نطق و سمع</p></div>
<div class="m2"><p>ز نسرین انجم به گلهای شمع</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو فانوس گلگون قبا شد قطار</p></div>
<div class="m2"><p>بود رقص پروانه در لاله زار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به مقراض خادم فتاده ز مهر</p></div>
<div class="m2"><p>سر شمع در طاس چارم سپهر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز تاکید عودآزمای سرور</p></div>
<div class="m2"><p>هوای حرم تانیفتد ز نور،</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو آتش ز مجمر کند رو به عود</p></div>
<div class="m2"><p>دهد بوی خوش بی ملاقات دود</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کبوتر کند چون به آن روضه میل</p></div>
<div class="m2"><p>شود بیضه در آشیانش سهیل</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشاط محجر یکی صد شده</p></div>
<div class="m2"><p>که از چارسو رو به مرقد شده</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نگردم چرا من به گرد ضریح؟</p></div>
<div class="m2"><p>که او گشته برگرد مرقد صریح</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>شود گم در آن بارگاه عظیم</p></div>
<div class="m2"><p>ردای خضر با عصای کلیم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دل از دار حفاظ جوید نشاط</p></div>
<div class="m2"><p>که دارد ز مصحف گل انبساط</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>صف رحلها از دو سو جلوه گر</p></div>
<div class="m2"><p>یکی از زمرد، یکی از گهر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو حافظ به قرآن شده ترصدا</p></div>
<div class="m2"><p>شنیده به تحسین ز ایزد ندا</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نماندی چنین مصحف گل سقیم</p></div>
<div class="m2"><p>رساندی به حفاظ او گر نسیم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز دارالسیاده علم گشته نور</p></div>
<div class="m2"><p>شده چلچراغش به از نخل طور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به ذوقی که جاروب آن در شود</p></div>
<div class="m2"><p>مه از هاله گلدسته تر شود</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>به مشهد ز خلد آمده سلسبیل</p></div>
<div class="m2"><p>که گردد ز سقای حضرت سبیل</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مرصع نشد تا در آن جناب</p></div>
<div class="m2"><p>جواهر نگردید عزت مآب</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>درو برده استاد عرض اقتدار</p></div>
<div class="m2"><p>ز نه آسمان یک زبرجد به کار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شمارد دل شب نشین حرم</p></div>
<div class="m2"><p>ز الماس او شام را صبحدم</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نبودی اگر تخته اش در سخت</p></div>
<div class="m2"><p>به آب زمرد شدی چون درخت</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>محرک نمی جوید آن در زکس</p></div>
<div class="m2"><p>به موج گهر می رود پیش و پس</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>برآورده گردون به فیروزه نام</p></div>
<div class="m2"><p>که چون لعل یابد بر آن در، مقام</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو یاقوت را گشت آن در، پناه</p></div>
<div class="m2"><p>نکرد آتش از بیم بر وی نگاه</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>کبابم که سیلانی اشک من</p></div>
<div class="m2"><p>نشد باب درگاه شاه زمن</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چه سان باشدم رنگی از مرتبه؟</p></div>
<div class="m2"><p>که مقبول آن در نیم چون شبه</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تنم گر شود کان فرخندگی</p></div>
<div class="m2"><p>نبیند ز من جوهر بندگی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>ز من کرد چون آستانش ابا</p></div>
<div class="m2"><p>شد از زردرویی تنم کهربا</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>خزف طینتم، از ره احترام</p></div>
<div class="m2"><p>چه سان جا کنم در لآلی مقام</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بده ساقی آن آب مرجان نمود</p></div>
<div class="m2"><p>که خاکم عقیقی کند در وجود</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>مگر زین صفا، پاسبان درش</p></div>
<div class="m2"><p>مکانم دهد ز آستان زرش</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو مینای صبحم بود سر بلند</p></div>
<div class="m2"><p>که بینم بر آن بزم انجم سپند</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز نزدیک آن بزم قدسی سرور</p></div>
<div class="m2"><p>مرا چرخ مینایی افکند دور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>سزد گر به مستی ز کوه فغان</p></div>
<div class="m2"><p>زنم سنگ بر شیشه آسمان</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>مغنی مخوان نغمه طرز عراق</p></div>
<div class="m2"><p>که آید مخالف مرا در مذاق</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>بود در خور نغمه سنجان رند</p></div>
<div class="m2"><p>سرود خراسان و محبوب هند</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ز ناسازی چرخ پرریو و رنگ</p></div>
<div class="m2"><p>به خود، سرفرو برده ام همچو چنگ</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>نشد راست این بربط کج نهاد</p></div>
<div class="m2"><p>ز نه پرده، یک صوت عیشم نداد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>همان به کزین طعنه مضراب وار</p></div>
<div class="m2"><p>به قانون نمایم دلش را فگار</p></div></div>