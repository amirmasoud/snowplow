---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>در فصل چنین که شد گلستان آباد</p></div>
<div class="m2"><p>سرزد گل عیش بلبل از شاخ مراد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که تهی ز ساغر می باشد</p></div>
<div class="m2"><p>چون دست چنار، بایدش داد به باد!</p></div></div>