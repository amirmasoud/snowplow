---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>داری دولبی که از ازل هم نمکند</p></div>
<div class="m2"><p>در بزمگه شراب حسنت گزکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارند جدایی به گه خنده ز هم</p></div>
<div class="m2"><p>چون نوبت بوسه می شود، هر دو یکند</p></div></div>