---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>عشق تو چو من دردکشی می خواهد</p></div>
<div class="m2"><p>سودای تو دیوانه وشی می خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین شعله فروز معصیت، روی متاب</p></div>
<div class="m2"><p>دیگ کرم تو آتشی می خواهد</p></div></div>