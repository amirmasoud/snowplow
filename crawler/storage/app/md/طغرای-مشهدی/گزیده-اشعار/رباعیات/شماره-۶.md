---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>در وادی عشق، بی خبر باید رفت</p></div>
<div class="m2"><p>آماده صدگونه خطر باید رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دار، رسن به گوش منصور کشید</p></div>
<div class="m2"><p>کاین جا ره پا نیست، به سر باید رفت</p></div></div>