---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ابر آمده شب که ماهتابت ببرد</p></div>
<div class="m2"><p>در خانه ز کوچه با شتابت ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بارش باران اگر آبت ببرد</p></div>
<div class="m2"><p>نفعش به ازان است که خوابت ببرد</p></div></div>