---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>دل بهر معاش، چند زحمت بکشد</p></div>
<div class="m2"><p>وز بهر دو لقمه صد مشقت بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند که رزق می رسد بی منت</p></div>
<div class="m2"><p>با رزق چنین، کسی چه منت بکشد؟</p></div></div>