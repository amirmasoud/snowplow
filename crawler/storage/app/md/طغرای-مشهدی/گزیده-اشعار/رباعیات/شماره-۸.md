---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>اشک آمد و حسرت ز دل ریش نبرد</p></div>
<div class="m2"><p>از وادی بی طاقتی ام پیش نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سیل که آید به سر ریگ روان</p></div>
<div class="m2"><p>برداشت ز جا و همره خویش نبرد</p></div></div>