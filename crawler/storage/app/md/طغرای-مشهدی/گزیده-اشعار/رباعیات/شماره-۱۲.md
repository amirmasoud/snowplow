---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>مشکل باشد ز دام او جان بردن</p></div>
<div class="m2"><p>مشکل تر از این، دل به کفش نسپردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس همه چیزش به نظر شیرین است</p></div>
<div class="m2"><p>چون حب نبات می توانش خوردن!</p></div></div>