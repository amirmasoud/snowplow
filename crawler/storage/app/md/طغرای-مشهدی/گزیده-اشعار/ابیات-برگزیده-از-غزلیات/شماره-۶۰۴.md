---
title: >-
    شمارهٔ  ۶۰۴
---
# شمارهٔ  ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>تا نخندد بر تو صد گوهرشناس</p></div>
<div class="m2"><p>ای خزف، همچشمی گوهر مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی غلط، حرفی نیاید بر لبم</p></div>
<div class="m2"><p>آنچه از من بشنوی، باور مکن</p></div></div>