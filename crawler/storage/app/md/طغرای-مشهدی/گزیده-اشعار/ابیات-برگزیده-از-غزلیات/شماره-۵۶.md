---
title: >-
    شمارهٔ  ۵۶
---
# شمارهٔ  ۵۶

<div class="b" id="bn1"><div class="m1"><p>بس که از جوی صفا، شاداب می بینم ترا</p></div>
<div class="m2"><p>همچو عکس شاخ گل در آب می بینم ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شب تاریک برداری نقاب از روی خویش</p></div>
<div class="m2"><p>از فروغ چهره در مهتاب می بینم ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شبنم بر برگ سوسن خفته می آید به یاد</p></div>
<div class="m2"><p>تکیه چون بر بستر سنجاب می بینم ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساده لوحی بین که دارم از تو چشم دوستی</p></div>
<div class="m2"><p>من که دشمن پرور احباب می بینم ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناز کمتر کن که من در کامجویی قانعم</p></div>
<div class="m2"><p>بس بود، یک بار اگر درخواب می بینم ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه می آید مرا بر حال طغرا همچو شمع</p></div>
<div class="m2"><p>هر کجا با خوی آتش تاب می بینم ترا</p></div></div>