---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>زمین در سایه خم می زند گلبانگ جوش اینجا</p></div>
<div class="m2"><p>سبوی می به زور باده می آید به دوش اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دویی برخاست امشب از در میخانه و مسجد</p></div>
<div class="m2"><p>اگر خواهی می وحدت، بگیر آنجا، بنوش اینجا</p></div></div>