---
title: >-
    شمارهٔ  ۴۷۴
---
# شمارهٔ  ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>نیست یک دانه نصیب تن ما لذت رزق</p></div>
<div class="m2"><p>ما ز تقدیر نداریم جوی منت رزق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل غم خوردن خود نیست مرا یک سر مو</p></div>
<div class="m2"><p>خورده ام بس که پیاپی ز جهان حسرت رزق</p></div></div>