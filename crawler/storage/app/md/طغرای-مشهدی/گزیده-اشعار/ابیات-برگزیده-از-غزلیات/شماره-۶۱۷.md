---
title: >-
    شمارهٔ  ۶۱۷
---
# شمارهٔ  ۶۱۷

<div class="b" id="bn1"><div class="m1"><p>می رود از گردش گردون، چه زیبا و چه زشت</p></div>
<div class="m2"><p>چون بغلتد شیشه، از وی درد و صاف آید برون</p></div></div>