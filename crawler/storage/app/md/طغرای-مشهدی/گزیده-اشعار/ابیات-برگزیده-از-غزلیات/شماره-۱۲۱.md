---
title: >-
    شمارهٔ  ۱۲۱
---
# شمارهٔ  ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>موج را گشت صبا سلسله جنبان در آب</p></div>
<div class="m2"><p>که شود با خس من دست و گریبان در آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم چشم ترم را ز خیال تو چه سود؟</p></div>
<div class="m2"><p>گل نیاید به کف از عکس گلستان در آب</p></div></div>