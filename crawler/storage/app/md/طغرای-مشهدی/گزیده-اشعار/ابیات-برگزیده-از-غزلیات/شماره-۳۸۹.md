---
title: >-
    شمارهٔ  ۳۸۹
---
# شمارهٔ  ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>عمرم همه صرف گلرخان شد</p></div>
<div class="m2"><p>من پیر شدم، هوس جوان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایام بهار این گلستان</p></div>
<div class="m2"><p>از سستی طالعم خزان شد</p></div></div>