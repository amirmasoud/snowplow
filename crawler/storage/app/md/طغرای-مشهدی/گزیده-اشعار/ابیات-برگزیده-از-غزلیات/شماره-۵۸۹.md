---
title: >-
    شمارهٔ  ۵۸۹
---
# شمارهٔ  ۵۸۹

<div class="b" id="bn1"><div class="m1"><p>دو بوس از لب لعل جانان گرفتن</p></div>
<div class="m2"><p>بود بهتر از صد بدخشان گرفتن</p></div></div>