---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>تا نام تو سردفتر معنی ست رقم را</p></div>
<div class="m2"><p>برفرد بیان، سجده ضرور است قلم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر وسعت خلقت به نبی ضابطه می داد</p></div>
<div class="m2"><p>از خانه ات اخراج نمی کرد صنم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق تو به گل کاری اشکم چو زند دست</p></div>
<div class="m2"><p>آرایش دستار دهد باغ ارم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان پیش که بنیاد شود کلبه شادی</p></div>
<div class="m2"><p>در کوچه ما ساخت قضا، خانه غم را</p></div></div>