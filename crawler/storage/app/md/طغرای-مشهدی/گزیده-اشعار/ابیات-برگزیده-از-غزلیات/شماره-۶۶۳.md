---
title: >-
    شمارهٔ  ۶۶۳
---
# شمارهٔ  ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>دوران به آب می کرد، طرح ایاغ لاله</p></div>
<div class="m2"><p>بهر چه تر نباشد، دایم دماغ لاله؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که بی تو گشتند، خوبان باغ درهم</p></div>
<div class="m2"><p>آیینه رخ گل، گردید داغ لاله</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم که چون سوخت، آن روی شعله ناکم</p></div>
<div class="m2"><p>پروانه سوز خود نیست، هرگز چراغ لاله</p></div></div>