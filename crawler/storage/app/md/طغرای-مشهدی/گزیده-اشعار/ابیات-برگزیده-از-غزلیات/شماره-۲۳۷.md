---
title: >-
    شمارهٔ  ۲۳۷
---
# شمارهٔ  ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>هما ز سایه دهد گرچه دولتی به شهان</p></div>
<div class="m2"><p>ولی چو سایه، خودش از قفای درویش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام یافته پوست تخت، می داند</p></div>
<div class="m2"><p>که تخت پادشهان، نصف جای درویش است</p></div></div>