---
title: >-
    شمارهٔ  ۶۲۶
---
# شمارهٔ  ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>بیهوده گشتیم، مهمان گردون</p></div>
<div class="m2"><p>رزقی ندیدیم، بر خوان گردون</p></div></div>