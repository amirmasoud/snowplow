---
title: >-
    شمارهٔ  ۵۱
---
# شمارهٔ  ۵۱

<div class="b" id="bn1"><div class="m1"><p>شوق به روی تیغ دویدن به پای خود</p></div>
<div class="m2"><p>آرد به صیدگاه تو رنگ پریده را</p></div></div>