---
title: >-
    شمارهٔ  ۶۱۱
---
# شمارهٔ  ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>خنده طرب از تو، گریه و فغان از من</p></div>
<div class="m2"><p>از تو قهقه شادی، آه خونچکان از من</p></div></div>