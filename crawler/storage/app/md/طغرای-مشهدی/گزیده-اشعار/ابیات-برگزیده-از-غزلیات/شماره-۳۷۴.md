---
title: >-
    شمارهٔ  ۳۷۴
---
# شمارهٔ  ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>در ساز فغانم ره آواز غلط شد</p></div>
<div class="m2"><p>از زمزمه ام نغمه دمساز غلط شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیچید صدا در چمن از نغمه بلبل</p></div>
<div class="m2"><p>چندان که رگ گل به رگ ساز غلط شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باغ تمنا چو گشودیم پرو بال</p></div>
<div class="m2"><p>از هر طرف اندازه پرواز غلط شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درمانده اصلاح غلط زار سلوکم</p></div>
<div class="m2"><p>هرجا پی اصلاح شدم، باز غلط شد</p></div></div>