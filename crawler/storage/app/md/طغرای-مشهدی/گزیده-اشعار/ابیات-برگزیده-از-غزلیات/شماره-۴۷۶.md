---
title: >-
    شمارهٔ  ۴۷۶
---
# شمارهٔ  ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>در کف امید ما شد ساغر سرشار خشک</p></div>
<div class="m2"><p>شد گلوی شیشه ما چون لب بیمار خشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت تردامن و زاهد نمی چسبد به هم</p></div>
<div class="m2"><p>گل نمی گیرد به خود، هرگه بود دیوار خشک</p></div></div>