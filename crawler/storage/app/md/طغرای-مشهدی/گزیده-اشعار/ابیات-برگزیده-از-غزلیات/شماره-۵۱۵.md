---
title: >-
    شمارهٔ  ۵۱۵
---
# شمارهٔ  ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>چشم و دل پاس خیالش را به نوبت داشتند</p></div>
<div class="m2"><p>دل سحرخیزانه چون بیدار شد، خوابید چشم</p></div></div>