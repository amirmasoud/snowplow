---
title: >-
    شمارهٔ  ۱۷۷
---
# شمارهٔ  ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>خواهی به دیر زمزمه کن،خواه در حرم</p></div>
<div class="m2"><p>هرجا که دم ز عشق زند کس، غنیمت است</p></div></div>