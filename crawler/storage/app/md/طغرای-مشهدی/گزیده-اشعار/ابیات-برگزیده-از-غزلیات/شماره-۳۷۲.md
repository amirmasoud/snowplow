---
title: >-
    شمارهٔ  ۳۷۲
---
# شمارهٔ  ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>به امیدی که چو طفل نگهم بازآیی</p></div>
<div class="m2"><p>تا دم صبح، در خانه چشمم وا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرفه راهی ست ره مرگ که با اینهمه خوف</p></div>
<div class="m2"><p>هرکه دیدیم، درو بی خبر و تنها بود</p></div></div>