---
title: >-
    شمارهٔ  ۶۱۳
---
# شمارهٔ  ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>قدرشناس شاخ گل، قامت یار من ببین</p></div>
<div class="m2"><p>پنجه لاله دیده ای، دست نگار من ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو گل از فراق او، تاخته بر سرم خزان</p></div>
<div class="m2"><p>بوی پیادگی شنو، رنگ سوار من ببین</p></div></div>