---
title: >-
    شمارهٔ  ۳۰۴
---
# شمارهٔ  ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>مشو ملول که ساغر ز دست خواهد شد</p></div>
<div class="m2"><p>نصیب و قسمت ما آنچه هست خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قصد صنع خدا چون به خود نظاره کند</p></div>
<div class="m2"><p>خداپرست نشد، خودپرست خواهد شد</p></div></div>