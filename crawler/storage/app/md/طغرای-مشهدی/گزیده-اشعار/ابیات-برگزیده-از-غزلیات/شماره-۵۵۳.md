---
title: >-
    شمارهٔ  ۵۵۳
---
# شمارهٔ  ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>گل ز دستم رفته و من خار پیدا می کنم</p></div>
<div class="m2"><p>یار باید جست و من اغیار پیدا می کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ حقگویم، نیم منصور وش ممنون کس</p></div>
<div class="m2"><p>خود ز هر شاخ درختی، دار پیدا می کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دهد زاهد به دستم سبحه صد دانه را</p></div>
<div class="m2"><p>در میان دانه ها، زنار پیدا می کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر توانم کرد عمری مشق دل سختی چو طور</p></div>
<div class="m2"><p>من هم اندک طاقت دیدار پیدا می کنم</p></div></div>