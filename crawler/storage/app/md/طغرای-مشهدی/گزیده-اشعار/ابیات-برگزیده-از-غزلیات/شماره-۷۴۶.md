---
title: >-
    شمارهٔ  ۷۴۶
---
# شمارهٔ  ۷۴۶

<div class="b" id="bn1"><div class="m1"><p>به برگ عیش رسم، گر چو نوبهار آیی</p></div>
<div class="m2"><p>میان باغ شوم، چون تو در کنار آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو آن گلی که بهار از رخ تو می بارد</p></div>
<div class="m2"><p>چرا نهال نگردد چو پیش خار آیی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کند سفیدی چشمم نیابت دم صبح</p></div>
<div class="m2"><p>شبی که سوی من از راه انتظار آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار خاک نشین، آه می کشد ز پی ات</p></div>
<div class="m2"><p>نمی شود که به این کوچه بی غبار آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا ز رشته زلفش اگر بداری دست</p></div>
<div class="m2"><p>عجب که یک سر سوزن مرا به کار آیی</p></div></div>