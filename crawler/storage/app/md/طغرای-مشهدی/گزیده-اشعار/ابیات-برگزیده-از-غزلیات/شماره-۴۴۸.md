---
title: >-
    شمارهٔ  ۴۴۸
---
# شمارهٔ  ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>یار میان باز کرد، رو به کناری نشین</p></div>
<div class="m2"><p>سیرت حیرت بیار، صورت دیوار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه گر روزگار، گشته سحرخیز جنگ</p></div>
<div class="m2"><p>شب چو رسد، همچو تیغ، خفته بیدار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به سان مهره شطرنج خانه گرد مباش</p></div>
<div class="m2"><p>بتاب رخ ز کسان، در پی نبرد مباش</p></div></div>