---
title: >-
    شمارهٔ  ۷۵۳
---
# شمارهٔ  ۷۵۳

<div class="b" id="bn1"><div class="m1"><p>بس که گشته شیوه اسلامیان حق دشمنی</p></div>
<div class="m2"><p>حق گریزد از مسلمان در پناه ارمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دختر رز، گر نسازد با هوسناکان جام</p></div>
<div class="m2"><p>می شود در مذهب مفتی، سیاست کردنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاضی ما بس که شد در رشوه خواریها دلیر</p></div>
<div class="m2"><p>می شمارد تیغ را از رشوه های خوردنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشعل زرین توقع دارد از من میر شب</p></div>
<div class="m2"><p>گر بود از شمع آهی کلبه ام را روشنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بماند سنگ درکشمیر بربالای سنگ؟</p></div>
<div class="m2"><p>خاک این ملک از ستم شد همچو آتش رفتنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر مسلمانی که نعمت پرور این باغ بود</p></div>
<div class="m2"><p>شد ز دست هندوی حاکم، فقیر گلخنی</p></div></div>