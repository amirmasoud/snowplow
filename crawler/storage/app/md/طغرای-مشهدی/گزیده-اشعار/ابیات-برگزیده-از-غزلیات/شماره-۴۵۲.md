---
title: >-
    شمارهٔ  ۴۵۲
---
# شمارهٔ  ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>نگیرم چون سر زلف سیاهش؟</p></div>
<div class="m2"><p>که خود پرورده ام از دود آهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محبت داد ما را سرزمینی</p></div>
<div class="m2"><p>که آب دیده می خواهد گیاهش</p></div></div>