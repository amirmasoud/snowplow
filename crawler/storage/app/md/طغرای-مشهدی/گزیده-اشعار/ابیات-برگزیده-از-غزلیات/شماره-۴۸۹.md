---
title: >-
    شمارهٔ  ۴۸۹
---
# شمارهٔ  ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>هوس پرورده زلف بتانم</p></div>
<div class="m2"><p>نمی روید ز خاکم غیر سنبل</p></div></div>