---
title: >-
    شمارهٔ  ۳۱۱
---
# شمارهٔ  ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>تن ز بسیاری تردامنی ام خاک نشد</p></div>
<div class="m2"><p>خاک ناپاک شد و طینت من پاک نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که آتش به سرم ریخت خیال تو چو شمع</p></div>
<div class="m2"><p>اشکها ریخت ز چشمم، مژه نمناک نشد</p></div></div>