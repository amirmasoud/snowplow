---
title: >-
    شمارهٔ  ۷۰۳
---
# شمارهٔ  ۷۰۳

<div class="b" id="bn1"><div class="m1"><p>گر به مهر همه چون صبح برآرم نفسی</p></div>
<div class="m2"><p>ندهد چرخ ازین فرقه به من همنفسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب گرفتم به برش، از من مسکین رخ تافت</p></div>
<div class="m2"><p>شعله را کی بود آرام در آغوش خسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشیان و پر پرواز و گل و دانه و آب</p></div>
<div class="m2"><p>همه را داد به ما چرخ، ولی در قفسی</p></div></div>