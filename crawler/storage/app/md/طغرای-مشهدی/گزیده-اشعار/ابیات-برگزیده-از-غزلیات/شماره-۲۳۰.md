---
title: >-
    شمارهٔ  ۲۳۰
---
# شمارهٔ  ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>دهد گردون عوض یک دسته خارم</p></div>
<div class="m2"><p>اگر گلدسته ای از دست من رفت</p></div></div>