---
title: >-
    شمارهٔ  ۶۳۶
---
# شمارهٔ  ۶۳۶

<div class="b" id="bn1"><div class="m1"><p>بدن غنچه نباشد به صفای تن او</p></div>
<div class="m2"><p>نیست با جامه گل، نکهت پیراهن او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در خانه ما دختر رز پرده نشین</p></div>
<div class="m2"><p>مرد اگر مرد بود، پرده نخواهد زن او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می گذارم به گریبان ز پی چاک زدن</p></div>
<div class="m2"><p>دست بردارم اگر یک نفس از دامن او</p></div></div>