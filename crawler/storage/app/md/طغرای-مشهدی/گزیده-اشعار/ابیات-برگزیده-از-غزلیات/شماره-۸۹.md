---
title: >-
    شمارهٔ  ۸۹
---
# شمارهٔ  ۸۹

<div class="b" id="bn1"><div class="m1"><p>مشت خسم، ولی چو نشینم به آن بهار</p></div>
<div class="m2"><p>گلدسته می کند اثر رنگ و بو مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد ره گر از خمار بیفتم به پای خم</p></div>
<div class="m2"><p>یک دستگیر نیست به غیر از سبو مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی شمع پی به گریه من می برد، نه ابر</p></div>
<div class="m2"><p>کزدیده، جای قطره چکد آرزو مرا</p></div></div>