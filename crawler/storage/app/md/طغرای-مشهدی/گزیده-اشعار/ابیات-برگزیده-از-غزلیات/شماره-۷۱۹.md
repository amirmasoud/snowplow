---
title: >-
    شمارهٔ  ۷۱۹
---
# شمارهٔ  ۷۱۹

<div class="b" id="bn1"><div class="m1"><p>بس که برگرد تو گشتم، سر من می گردد</p></div>
<div class="m2"><p>باید از شوق چنین گشت به قربان کسی</p></div></div>