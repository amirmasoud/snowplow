---
title: >-
    شمارهٔ  ۱۶۲
---
# شمارهٔ  ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>چون نریزد غم شراب تلخکامی در قدح؟</p></div>
<div class="m2"><p>باده شیرین، پسند طبع این می خواره نیست</p></div></div>