---
title: >-
    شمارهٔ  ۱۶۱
---
# شمارهٔ  ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>ای گل، همه تن لب شده در وصف بهاری</p></div>
<div class="m2"><p>یک بار نگفتی که درین باغ خزان هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مانده هوس نان به دل آدم آبی</p></div>
<div class="m2"><p>جایی که غم آب نباشد، غم نان هست</p></div></div>