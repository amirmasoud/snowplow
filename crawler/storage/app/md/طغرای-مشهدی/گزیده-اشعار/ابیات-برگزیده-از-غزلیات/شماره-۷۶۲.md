---
title: >-
    شمارهٔ  ۷۶۲
---
# شمارهٔ  ۷۶۲

<div class="b" id="bn1"><div class="m1"><p>ندارد دیده ما طالع نظاره رویی</p></div>
<div class="m2"><p>چو پیشانی نمی بینیم غیر از چین ابرویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو دام از خود نیم در صیدگاه آرزو آگه</p></div>
<div class="m2"><p>ز بس در حیرتم افکنده نقش پای آهویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گل در یک قبایم، لیک از بسیاری غفلت</p></div>
<div class="m2"><p>به رنگ بلبل دیبا، ازو نشنیده ام بویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنم گردیده خاک و در شکنج سایه دامم</p></div>
<div class="m2"><p>نمی دانم که بر یادم پریشان کرده گیسویی؟</p></div></div>