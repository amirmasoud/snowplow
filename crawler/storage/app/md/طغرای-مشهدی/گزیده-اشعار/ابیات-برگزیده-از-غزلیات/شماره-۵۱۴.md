---
title: >-
    شمارهٔ  ۵۱۴
---
# شمارهٔ  ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>دایم غم نخوردن یک بوس می خورم</p></div>
<div class="m2"><p>بوسی نخورده ام ز تو، افسوس می خورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی غیرتی نگر، که ز بتخانه گرد عشق</p></div>
<div class="m2"><p>سیلی برای ناله چو ناقوس می خورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرچشمه دار العطشم تا به کربلا</p></div>
<div class="m2"><p>آبی اگر ز غمکده طوس می خورم</p></div></div>