---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>به عجز پای منه، عرض مرد می رود اینجا</p></div>
<div class="m2"><p>پی تو سایه به قصد نبرد می رود اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ست در دل آزادگان صفا و کدورت</p></div>
<div class="m2"><p>جلای آینه، همراه گرد می رود اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بندر غم او، اشک سرخ نیست معطل</p></div>
<div class="m2"><p>هزار قافله رنگ زرد می رود اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدام سرو قد افکند سایه برسر خاکم؟</p></div>
<div class="m2"><p>که آبروی زمرد، به گرد می رود اینجا</p></div></div>