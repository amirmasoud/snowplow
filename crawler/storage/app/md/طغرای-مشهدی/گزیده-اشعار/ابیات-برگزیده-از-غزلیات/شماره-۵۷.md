---
title: >-
    شمارهٔ  ۵۷
---
# شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>ندید مادر ایام، روی شوکت ما</p></div>
<div class="m2"><p>چو طفل اشک، بزرگی نبود قسمت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه کام دل ما برآید از گردون</p></div>
<div class="m2"><p>که خرمنش پر کاهی ست پیش همت ما</p></div></div>