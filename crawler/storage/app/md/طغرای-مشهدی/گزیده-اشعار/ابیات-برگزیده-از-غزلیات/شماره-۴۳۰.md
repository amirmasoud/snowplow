---
title: >-
    شمارهٔ  ۴۳۰
---
# شمارهٔ  ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>دور طرب نگردد، هرگز به کام نرگس</p></div>
<div class="m2"><p>نام تو گر نباشد بر دور جام نرگس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردد چو غنچه بیدار، از باد نوبهاری</p></div>
<div class="m2"><p>پیش از سلام گلها، گوید سلام نرگس</p></div></div>