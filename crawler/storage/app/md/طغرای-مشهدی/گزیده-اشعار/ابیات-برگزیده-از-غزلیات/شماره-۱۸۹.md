---
title: >-
    شمارهٔ  ۱۸۹
---
# شمارهٔ  ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>طغرا در آخر غزل آورد مطلعی</p></div>
<div class="m2"><p>کاین گوشوار طرز سخندانی من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معمار عشق، زنده به ویرانی من است</p></div>
<div class="m2"><p>غم، تازه رو ز دیدن پیشانی من است</p></div></div>