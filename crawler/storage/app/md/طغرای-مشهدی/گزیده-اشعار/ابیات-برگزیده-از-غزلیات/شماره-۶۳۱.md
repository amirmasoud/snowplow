---
title: >-
    شمارهٔ  ۶۳۱
---
# شمارهٔ  ۶۳۱

<div class="b" id="bn1"><div class="m1"><p>دختری کز سایه رز می رمید</p></div>
<div class="m2"><p>گشته رام شیشه و جام و سبو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده غسل از بهر پاکی صد حلال</p></div>
<div class="m2"><p>در حرام شیشه و جام و سبو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن به قانون ده، مرو بی چنگ و نی</p></div>
<div class="m2"><p>در مقام شیشه و جام و سبو</p></div></div>