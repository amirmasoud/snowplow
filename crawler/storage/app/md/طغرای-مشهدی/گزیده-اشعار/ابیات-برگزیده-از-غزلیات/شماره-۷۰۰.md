---
title: >-
    شمارهٔ  ۷۰۰
---
# شمارهٔ  ۷۰۰

<div class="b" id="bn1"><div class="m1"><p>ای که داری گله از دست فراموشی یار</p></div>
<div class="m2"><p>ادب آن است که در خاطر او جا نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدف از خانه او قطره آبی نچشید</p></div>
<div class="m2"><p>تکیه بر همت کم کاسه دریا نکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای قدح، قلقل بسیار، کم از عربده نیست</p></div>
<div class="m2"><p>بهتر آن است که سر در سر مینا نکنی</p></div></div>