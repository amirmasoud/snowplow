---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>منت خضر بر سرم نیست، چو شمع انجمن</p></div>
<div class="m2"><p>آب حیات می دهد، آتش سوختن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوهرچوب دار را ساخته ام طلسم خود</p></div>
<div class="m2"><p>تا نبرد به هر طرف، کشمکش رسن مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازکی شمیم گل، طرح درشتی افکند</p></div>
<div class="m2"><p>بی قد سرو او بود گر هوس چمن مرا</p></div></div>