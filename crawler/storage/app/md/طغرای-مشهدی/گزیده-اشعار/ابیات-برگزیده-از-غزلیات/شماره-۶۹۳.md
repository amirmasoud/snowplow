---
title: >-
    شمارهٔ  ۶۹۳
---
# شمارهٔ  ۶۹۳

<div class="b" id="bn1"><div class="m1"><p>گفتی که در دل تو، نگشود خاطر من</p></div>
<div class="m2"><p>چون خاطرت گشاید؟ در تنگنا نشستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون گل تو خوش نشینی، کو حد آنکه گویم</p></div>
<div class="m2"><p>کاین جا چرا نیایی، وانجا چرا نشستی</p></div></div>