---
title: >-
    شمارهٔ  ۵۹۳
---
# شمارهٔ  ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>باشد اگر امید اثر با گریستن</p></div>
<div class="m2"><p>کی دل کشد به خنده، بود تا گریستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شمع،این قدر به تو لذت نمی رسد</p></div>
<div class="m2"><p>سوز و گداز از تو و از ما گریستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه خصلتیم، ز ما پر بعید نیست</p></div>
<div class="m2"><p>بیهوده خنده کردن و بیجا گریستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باید چو برق، خنده زنان از جهان گذشت</p></div>
<div class="m2"><p>نتوان چو ابر بر سر دنیا گریستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طغرا چه طالع است که امروز بایدت</p></div>
<div class="m2"><p>بر حال خود ز محنت فردا گریستن</p></div></div>