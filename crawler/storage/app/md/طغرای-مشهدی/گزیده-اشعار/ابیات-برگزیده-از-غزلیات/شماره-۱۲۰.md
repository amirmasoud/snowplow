---
title: >-
    شمارهٔ  ۱۲۰
---
# شمارهٔ  ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ما را گرفته بی او، غم در میانه امشب</p></div>
<div class="m2"><p>می نالد از غم ما، چنگ و چغانه امشب</p></div></div>