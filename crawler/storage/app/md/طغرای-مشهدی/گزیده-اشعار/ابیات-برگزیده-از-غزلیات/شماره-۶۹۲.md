---
title: >-
    شمارهٔ  ۶۹۲
---
# شمارهٔ  ۶۹۲

<div class="b" id="bn1"><div class="m1"><p>ما به جای قطره، دریا ریختیم از چشم تر</p></div>
<div class="m2"><p>دیده ما را ببین وز ابر بارانی مگوی</p></div></div>