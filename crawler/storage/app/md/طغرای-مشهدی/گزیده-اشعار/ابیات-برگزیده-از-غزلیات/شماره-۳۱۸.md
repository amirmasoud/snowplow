---
title: >-
    شمارهٔ  ۳۱۸
---
# شمارهٔ  ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>گر دهدم باغبان، یک گل ازین نه چمن</p></div>
<div class="m2"><p>رنگ جدا، بو جدا، برگ جدا می دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فیض خداداد نظم، گشت به طغرا نصیب</p></div>
<div class="m2"><p>کس نتواند گرفت، آنچه خدا می دهد</p></div></div>