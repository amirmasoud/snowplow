---
title: >-
    شمارهٔ  ۱۸۵
---
# شمارهٔ  ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>غزل عاشقانه باید گفت</p></div>
<div class="m2"><p>بیت باب ترانه باید گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر نازش خطا نمی داند</p></div>
<div class="m2"><p>همه جا را نشانه باید گفت</p></div></div>