---
title: >-
    شمارهٔ  ۷۳۱
---
# شمارهٔ  ۷۳۱

<div class="b" id="bn1"><div class="m1"><p>ای روی تو سردفتر گلهای بهاری</p></div>
<div class="m2"><p>بهر تو کند آب چمن آینه داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بلبل صبح از پی وصلت نزند بال؟</p></div>
<div class="m2"><p>خورشید ندارد گل رویی که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گیرم چو به زور از تو گل بوسه، مکن عیب</p></div>
<div class="m2"><p>بسیاری شوقم ندهد فرصت زاری</p></div></div>