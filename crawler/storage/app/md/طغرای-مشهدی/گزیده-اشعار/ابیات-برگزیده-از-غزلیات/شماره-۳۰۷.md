---
title: >-
    شمارهٔ  ۳۰۷
---
# شمارهٔ  ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>آرام نگیرد نفسی بر مژه ام اشک</p></div>
<div class="m2"><p>کودک چو رود بر سر دیوار، بترسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گوشه این کلبه، خرابی شده پنهان</p></div>
<div class="m2"><p>روزی که برآید، در و دیوار بترسد</p></div></div>