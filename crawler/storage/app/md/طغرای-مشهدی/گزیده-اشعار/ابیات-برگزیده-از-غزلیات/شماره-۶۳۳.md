---
title: >-
    شمارهٔ  ۶۳۳
---
# شمارهٔ  ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>ناله برگشت ز لب از فرح دیدن تو</p></div>
<div class="m2"><p>گریه در دیده گره گشت ز خندیدن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می کند شبنم گل از سر شب تا دم صبح</p></div>
<div class="m2"><p>مشق شیرینی در میکده غلتیدن تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برهمن گرم پرستیدن سنگ سیه است</p></div>
<div class="m2"><p>دل چه سان بگذرد از شغل پرستیدن تو</p></div></div>