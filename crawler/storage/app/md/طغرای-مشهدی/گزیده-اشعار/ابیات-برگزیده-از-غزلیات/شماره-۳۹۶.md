---
title: >-
    شمارهٔ  ۳۹۶
---
# شمارهٔ  ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>خاک غربت شود اندوه وطن را درمان</p></div>
<div class="m2"><p>آشنا زخم چو زد، مرهم بیگانه ببند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبرو می رود از دست، به آمد شد غیر</p></div>
<div class="m2"><p>چون حباب از همه جانب ره کاشانه ببند</p></div></div>