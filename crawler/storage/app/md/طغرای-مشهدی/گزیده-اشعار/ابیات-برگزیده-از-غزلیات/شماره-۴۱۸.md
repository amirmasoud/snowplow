---
title: >-
    شمارهٔ  ۴۱۸
---
# شمارهٔ  ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>ز تنگدستی اگر با زمین شوم یکسان</p></div>
<div class="m2"><p>چو نقش پا ننشینم بر آستانه غیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سرگذشت دو زلفت فسانه هاست مرا</p></div>
<div class="m2"><p>به وقت خواب مکن گوش بر فسانه غیر</p></div></div>