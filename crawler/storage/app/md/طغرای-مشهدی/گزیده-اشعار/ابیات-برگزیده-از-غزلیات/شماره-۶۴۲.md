---
title: >-
    شمارهٔ  ۶۴۲
---
# شمارهٔ  ۶۴۲

<div class="b" id="bn1"><div class="m1"><p>چرخ نامرد است، اگر مردی ازو یاری مخواه</p></div>
<div class="m2"><p>خویش را در زیر بار یک جهان خواری مخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا توان از تلخ و شیرین جهان پرهیز کرد</p></div>
<div class="m2"><p>همچو چشم خوبرویان، غیر بیماری مخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در لباس خرمی صد عقده می روید ز دل</p></div>
<div class="m2"><p>سرو قامت باش، لیکن رخت زنگاری مخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شب زلفش مده چون شانه هرگز تن به خواب</p></div>
<div class="m2"><p>گر سراپا چشم گردی، غیر بیداری مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زورق از یمن سبکباری به ساحل می رسد</p></div>
<div class="m2"><p>جان اگر خواهی، درین دریا گرانباری مخواه</p></div></div>