---
title: >-
    شمارهٔ  ۲۱۲
---
# شمارهٔ  ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>از تبسم لب ببند ای غنچه، حال گل ببین</p></div>
<div class="m2"><p>کز گلابش گریه دایم در قفای خنده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقبت اندیشگان را در نشاط آباد بزم</p></div>
<div class="m2"><p>گریه می آید بر آن لب کآشنای خنده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس آرا کیست امشب کز هجوم دلخوشی</p></div>
<div class="m2"><p>صورت دیوار، لبریز صدای خنده است</p></div></div>