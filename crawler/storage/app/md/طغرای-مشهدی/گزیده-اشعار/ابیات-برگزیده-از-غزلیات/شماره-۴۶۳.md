---
title: >-
    شمارهٔ  ۴۶۳
---
# شمارهٔ  ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>نمی توان به هم آغوشی ات قناعت کرد</p></div>
<div class="m2"><p>به حیرتم که چه سان می شود قبا قانع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب وصال، ادب تا نگشت مانع شوق</p></div>
<div class="m2"><p>به پای بوس نگشتیم چون حنا قانع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار بادیه سرگشتگی ست در هر گام</p></div>
<div class="m2"><p>مشو به یک دو نشانی ز رهنما قانع</p></div></div>