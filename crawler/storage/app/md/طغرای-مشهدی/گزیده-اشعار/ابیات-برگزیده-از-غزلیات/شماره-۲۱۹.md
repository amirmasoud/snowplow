---
title: >-
    شمارهٔ  ۲۱۹
---
# شمارهٔ  ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>از مقام ریشه و پیوند می باید گریخت</p></div>
<div class="m2"><p>چون صدای نی ز چندین بند می باید گریخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خویشها دارند چون زنبور، دایم نیشها</p></div>
<div class="m2"><p>خویش را خواهی، ز خویشاوند می باید گریخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نداری ای چمن، تاب سواران خزان</p></div>
<div class="m2"><p>تا به آن جایی که می تازند، می باید گریخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما غریبان را زنی چون دختر رز لایق است</p></div>
<div class="m2"><p>از زنی کورا شود فرزند، می بایدگریخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به کی طغرا به غارت می دهی مضمون نو؟</p></div>
<div class="m2"><p>زین کهن دزدان معنی بند می باید گریخت</p></div></div>