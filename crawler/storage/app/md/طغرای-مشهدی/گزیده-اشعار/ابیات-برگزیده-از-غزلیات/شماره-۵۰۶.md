---
title: >-
    شمارهٔ  ۵۰۶
---
# شمارهٔ  ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>آنچه حاصل شد ز کشت همتم</p></div>
<div class="m2"><p>در کنار خوشه چین انداختم</p></div></div>