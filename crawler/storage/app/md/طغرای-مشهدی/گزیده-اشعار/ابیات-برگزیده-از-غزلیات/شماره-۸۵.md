---
title: >-
    شمارهٔ  ۸۵
---
# شمارهٔ  ۸۵

<div class="b" id="bn1"><div class="m1"><p>در هنرمندی ز مردن نیست غم استاد را</p></div>
<div class="m2"><p>زنده دارد کار شیرین، تا ابد فرهاد را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ابد آدم نخوردی نیم گندم از بهشت</p></div>
<div class="m2"><p>در ازل خوردی اگر یک جو غم اولاد را</p></div></div>