---
title: >-
    شمارهٔ  ۵۸۴
---
# شمارهٔ  ۵۸۴

<div class="b" id="bn1"><div class="m1"><p>ندارد انتها باران اشکم</p></div>
<div class="m2"><p>بترس از سیل بی پایان اشکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا شد بارش ابر بهاری؟</p></div>
<div class="m2"><p>که گردد قطره اش حیران اشکم</p></div></div>