---
title: >-
    شمارهٔ  ۳۲۲
---
# شمارهٔ  ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>عمری ست که همخوابه میناست درین بزم</p></div>
<div class="m2"><p>جا دارد اگر دختر رز حامله باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار نماز از پی هم کرد صراحی</p></div>
<div class="m2"><p>واجب نتوان خواند، مگر نافله باشد</p></div></div>