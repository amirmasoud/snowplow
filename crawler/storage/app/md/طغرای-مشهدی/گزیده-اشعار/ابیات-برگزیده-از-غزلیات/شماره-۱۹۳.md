---
title: >-
    شمارهٔ  ۱۹۳
---
# شمارهٔ  ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>بی فرات کرشمه ات در باغ</p></div>
<div class="m2"><p>ماتم افروزی شهید گل است</p></div></div>