---
title: >-
    شمارهٔ  ۳۰۲
---
# شمارهٔ  ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>گل ما را به آب غم سرشتند</p></div>
<div class="m2"><p>مجو شادی ز خاک این غم آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی دانم چه دید از سرنوشتم</p></div>
<div class="m2"><p>که می گرید قلم در دست استاد</p></div></div>