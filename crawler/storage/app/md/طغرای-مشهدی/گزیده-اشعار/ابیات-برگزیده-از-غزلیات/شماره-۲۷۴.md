---
title: >-
    شمارهٔ  ۲۷۴
---
# شمارهٔ  ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>گفتی که نعش ما را، کس پیش و پس نباشد</p></div>
<div class="m2"><p>روزی که ما نباشیم، گو هیچ کس نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خانه زاد دامیم، باید ز بعد مردن</p></div>
<div class="m2"><p>تابوت ما اسیران، غیر از قفس نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از یمن شانه کم شد، آشوب چین زلفت</p></div>
<div class="m2"><p>بازار فتنه گرم است، هر جا عسس نباشد</p></div></div>