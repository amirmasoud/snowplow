---
title: >-
    شمارهٔ  ۵۹
---
# شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>بس که زان گل در دل ما خار نومیدی شکست</p></div>
<div class="m2"><p>سبزه نالان می دمد از خاک دردآلود ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بقا رنگی ندارد بر سر آتش سپند</p></div>
<div class="m2"><p>در محبت نیست فرق از بود تا نابود ما</p></div></div>