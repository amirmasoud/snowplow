---
title: >-
    شمارهٔ  ۲۴
---
# شمارهٔ  ۲۴

<div class="b" id="bn1"><div class="m1"><p>می گزد همچو نغمه خارج</p></div>
<div class="m2"><p>بی قدح، سیر ماهتاب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تمنای آتشین رویی</p></div>
<div class="m2"><p>می دهد گریه، سر به آب مرا</p></div></div>