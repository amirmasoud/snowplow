---
title: >-
    شمارهٔ  ۲۸۷
---
# شمارهٔ  ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>کارم ز نارسیدن کشتی به جان رسید</p></div>
<div class="m2"><p>تا ناخدا رسد، به خدا می توان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلسرد این چمن نشود از چه تیره بخت؟</p></div>
<div class="m2"><p>تا داغ لاله گرم کند جا، خزان رسید</p></div></div>