---
title: >-
    شمارهٔ  ۵۵۴
---
# شمارهٔ  ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>تردامنی به کف داد، سرخانه حبابم</p></div>
<div class="m2"><p>هرجا که می نهم پا، تا نصف تن در آبم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که تنگ ظرفی، گل کرد در وجودم</p></div>
<div class="m2"><p>ساقی به مستی افکند، از وعده شرابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مهد هستی ام دل چون تن دهد به آرام؟</p></div>
<div class="m2"><p>زاییده مادر دهر، بر خشت اضطرابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل بود تکلم، آشفته خاطران را</p></div>
<div class="m2"><p>حرفی چو پرسم از گل، بلبل دهد جوابم</p></div></div>