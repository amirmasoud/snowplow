---
title: >-
    شمارهٔ  ۵۶۷
---
# شمارهٔ  ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>از بس ز پا فتاده ایام پیری ام</p></div>
<div class="m2"><p>ناید ز صد جوان، سر مو دستگیری ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزاده ام مخوان، که ز ملک عدم، قضا</p></div>
<div class="m2"><p>آورده در وجود به طرز اسیری ام</p></div></div>