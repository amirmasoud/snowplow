---
title: >-
    شمارهٔ  ۲۵۹
---
# شمارهٔ  ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>دل برون آرم ازان سینه که غم نشناسد</p></div>
<div class="m2"><p>دست بردارم ازان تن که الم نشناسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالک پاکرو آن است که چون ریگ روان</p></div>
<div class="m2"><p>دشت پیما شود و نقش قدم نشناسد</p></div></div>