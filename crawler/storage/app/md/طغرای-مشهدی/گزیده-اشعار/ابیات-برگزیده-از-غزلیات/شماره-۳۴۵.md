---
title: >-
    شمارهٔ  ۳۴۵
---
# شمارهٔ  ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>دل به امید عشق او، تن به هوس نمی دهد</p></div>
<div class="m2"><p>هر که به شعله یار شد، باج به خس نمی دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسته نقاب شرم او، بوسه کجا و من کجا</p></div>
<div class="m2"><p>در نگشوده، باغبان میوه به کس نمی دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آهن و شیشه را نبود اینهمه بهره از صفا</p></div>
<div class="m2"><p>عکس جمال کیست این، کآینه پس نمی دهد؟</p></div></div>