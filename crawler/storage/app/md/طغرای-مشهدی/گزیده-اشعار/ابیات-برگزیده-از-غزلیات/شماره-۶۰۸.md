---
title: >-
    شمارهٔ  ۶۰۸
---
# شمارهٔ  ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>جان من رفتی، چه سان خواهم ز هجران زیستن</p></div>
<div class="m2"><p>چون مسافر گشت جان، یک لحظه نتوان زیستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در(سه) فصل عمر باید سر به جیب غم کشید</p></div>
<div class="m2"><p>تا توانی همچو گل یک فصل خندان زیستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همزبان ناموافق، کم ز عزرائیل نیست</p></div>
<div class="m2"><p>مرگ دانایان بود با جمع نادان زیستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام را از کف مده، گر زندگی داری هوس</p></div>
<div class="m2"><p>بی قدح، یک دم درین غمخانه نتوان زیستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نماید چون وطن، اقلیم عقبی دلنشین</p></div>
<div class="m2"><p>در جهان پیوسته باید چون غریبان زیستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خرد گفتم که مشکلتر ز مردن چیست، گفت</p></div>
<div class="m2"><p>صحبت جمعی که نتوان همچو ایشان زیستن</p></div></div>