---
title: >-
    شمارهٔ  ۶۸۶
---
# شمارهٔ  ۶۸۶

<div class="b" id="bn1"><div class="m1"><p>چشم مرا آب داد، تازگی روی می</p></div>
<div class="m2"><p>رنگ شکفتن گرفت، طبع من از بوی می</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تازه دلی پیشه باد، سرخوشی از ریشه باد</p></div>
<div class="m2"><p>سبزتر از شیشه باد، نخل من از جوی می</p></div></div>