---
title: >-
    شمارهٔ  ۱۲۷
---
# شمارهٔ  ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>امشب من و چشم قدح و دیدن مهتاب</p></div>
<div class="m2"><p>مینا و سر گریه ز خندیدن مهتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تردستی ساقی نرسیده ست به جایی</p></div>
<div class="m2"><p>کامشب نکند خواهش گل چیدن مهتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی تو پاکان غم شبگرد ندارند</p></div>
<div class="m2"><p>چیزی نتوان یافت ز کاویدن مهتاب</p></div></div>