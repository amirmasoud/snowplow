---
title: >-
    شمارهٔ  ۵۳۷
---
# شمارهٔ  ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>دست مطرب را زآب جویبار تار ساز</p></div>
<div class="m2"><p>سبزتر از برگهای تاک می خواهد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند بردوش افکنم صدجامه بی چاک را</p></div>
<div class="m2"><p>همچو گل، یک جامه صدچاک می خواهد دلم</p></div></div>