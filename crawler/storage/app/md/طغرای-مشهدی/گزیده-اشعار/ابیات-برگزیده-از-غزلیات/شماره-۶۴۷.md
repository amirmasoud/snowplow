---
title: >-
    شمارهٔ  ۶۴۷
---
# شمارهٔ  ۶۴۷

<div class="b" id="bn1"><div class="m1"><p>پژمرده بود حسن بت دست رسیده</p></div>
<div class="m2"><p>رنگ گل ناچیده ندارد گل چیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مردمک از گریه به صد آب بشوید</p></div>
<div class="m2"><p>بیرون نرود نقش تو از پرده دیده</p></div></div>