---
title: >-
    شمارهٔ  ۴۱۶
---
# شمارهٔ  ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>ساقی به ته شیشه می ناب نگه دار</p></div>
<div class="m2"><p>خواهیم دگر تشنه شدن، آب نگه دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطرب من کم حوصله را سوختی آخر</p></div>
<div class="m2"><p>گفتم به تو در پرده که مضراب نگه دار</p></div></div>