---
title: >-
    شمارهٔ  ۲۹۷
---
# شمارهٔ  ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>صد جامه پوشد از شرم، سرو خجسته قامت</p></div>
<div class="m2"><p>جایی که آن سهی قد، از پیرهن برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میدان شناس داغم، مردآزمای شورش</p></div>
<div class="m2"><p>از عهده دل من، کی سوختن برآید؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیش وطن دلم را نوش است در غریبی</p></div>
<div class="m2"><p>گل قدر خار داند، چون از چمن برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین گونه ام که غربت دامن گرفته، ترسم</p></div>
<div class="m2"><p>کز آتش فراقم، دود از وطن برآید</p></div></div>