---
title: >-
    شمارهٔ  ۱۳۱
---
# شمارهٔ  ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ناله ها در بحر و بر دارد غریب</p></div>
<div class="m2"><p>کام خشک و چشم تر دارد غریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کند پرواز، رو بر شهر خود</p></div>
<div class="m2"><p>آرزوی بال و پر دارد غریب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر غربت، خوار و زارش می کند</p></div>
<div class="m2"><p>همچو گل، هرچند زر دارد غریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نباشد صندل یاد وطن</p></div>
<div class="m2"><p>تا قیامت درد سر دارد غریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در غریبی خاطر ایمن مجوی</p></div>
<div class="m2"><p>هر قدم، چندین خطر دارد غریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی به شاهین ذوق دارد، نی به باز</p></div>
<div class="m2"><p>ذوق مرغ نامه بر دارد غریب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار نگشاید ز دستش یکقلم</p></div>
<div class="m2"><p>گر چو طغرا صد هنر دارد غریب</p></div></div>