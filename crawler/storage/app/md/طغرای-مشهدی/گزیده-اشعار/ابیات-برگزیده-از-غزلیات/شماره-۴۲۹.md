---
title: >-
    شمارهٔ  ۴۲۹
---
# شمارهٔ  ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>کنم چون یاد ایام وصالش</p></div>
<div class="m2"><p>نیاید بر زبانم غیر افسوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر آن بت ز میخواری کشد دست</p></div>
<div class="m2"><p>قدح فریاد بردارد چو ناقوس</p></div></div>