---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>در محبت سست بنیادیم چون قصر حباب</p></div>
<div class="m2"><p>از پر کاهی به هر سو می پرد دیوار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عقوبتگاه غفلت، دست و پا گم کرده ایم</p></div>
<div class="m2"><p>می چکد رنگ قبول از چهره انکار ما</p></div></div>