---
title: >-
    شمارهٔ  ۱۸۷
---
# شمارهٔ  ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>چین پیشانی ارباب غم از شادی ماست</p></div>
<div class="m2"><p>صد گره در دل زنجیر ز آزادی ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چو گل، دست در آغوش خرابی داریم</p></div>
<div class="m2"><p>خارخار عبثی در دل آبادی ماست</p></div></div>