---
title: >-
    شمارهٔ  ۵۵
---
# شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>کمان طالع ما تیر برنمی تابد</p></div>
<div class="m2"><p>مگر ز غیب خورد تیر بر نشانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین قفس، من و طغرا و بلبلیم اسیر</p></div>
<div class="m2"><p>که مانده در عدم آباد، آب و دانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قابل سبز شدن نیست، میفشان برخاک</p></div>
<div class="m2"><p>ترسم از شرم زمین، آب شود دانه ما</p></div></div>