---
title: >-
    شمارهٔ  ۱۸۰
---
# شمارهٔ  ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>امشب قدح از شراب خالی ست</p></div>
<div class="m2"><p>جای گل ماهتاب خالی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی چنگ هزار [تار] زلفت</p></div>
<div class="m2"><p>از تار نوا،رباب خالی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بازآ که گلابی دو چشمم</p></div>
<div class="m2"><p>بی روی تو از گلاب خالی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آینه رو ندید امروز</p></div>
<div class="m2"><p>پیمانه آفتاب خالی ست</p></div></div>