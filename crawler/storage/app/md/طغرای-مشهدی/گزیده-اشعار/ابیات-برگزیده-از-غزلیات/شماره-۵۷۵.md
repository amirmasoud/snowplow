---
title: >-
    شمارهٔ  ۵۷۵
---
# شمارهٔ  ۵۷۵

<div class="b" id="bn1"><div class="m1"><p>کار اگر چنین باشد، مزد کار ما معلوم</p></div>
<div class="m2"><p>نزد کارفرمایان، اعتبار ما معلوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت ما نمی داند، غیر واژگون سازی</p></div>
<div class="m2"><p>با چنین مددکاری، کار و بار ما معلوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز داغ حرمان شد باغ پرگل تازه</p></div>
<div class="m2"><p>باغ ما که این باشد، نوبهار ما معلوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخل خشک این باغیم، بی نصیب سرسبزی</p></div>
<div class="m2"><p>گر جهان شود خرم، برگ و بار ما معلوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می زند عبث طغرا، دم ز شعرپردازی</p></div>
<div class="m2"><p>قدر و قیمت شاعر در دیار ما معلوم</p></div></div>