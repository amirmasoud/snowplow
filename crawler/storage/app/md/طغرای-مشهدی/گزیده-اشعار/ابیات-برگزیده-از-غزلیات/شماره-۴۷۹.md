---
title: >-
    شمارهٔ  ۴۷۹
---
# شمارهٔ  ۴۷۹

<div class="b" id="bn1"><div class="m1"><p>دماغم خشک و لب [خشک] و دهان خشک</p></div>
<div class="m2"><p>دو چشمم تر، گلو خشک و زبان خشک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گردد کشتی ما گرم رفتار</p></div>
<div class="m2"><p>کنار بحر تر گردد، میان خشک</p></div></div>