---
title: >-
    شمارهٔ  ۲۷۸
---
# شمارهٔ  ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>نچسبید بر دامنت طفل اشکم</p></div>
<div class="m2"><p>چه گویم به او، دست و پایی ندارد</p></div></div>