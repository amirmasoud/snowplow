---
title: >-
    شمارهٔ  ۱۷۸
---
# شمارهٔ  ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>بوسه در آغوش ازان خودکام نتوانم گرفت</p></div>
<div class="m2"><p>صید خود را درمیان دام نتوانم گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که بی آرام گشتم از رمیدنهای او</p></div>
<div class="m2"><p>رام چون گردد به من، آرام نتوانم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چنین خواهم به دام برگ ریز هجر ماند</p></div>
<div class="m2"><p>از کف قاصد، گل پیغام نتوانم گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند بر ساقی کند شوقم گرفت بیش و کم؟</p></div>
<div class="m2"><p>مستیی خواهم که دیگر جام نتوانم گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که دورم همچو مغرب زان بت مشرق جبین</p></div>
<div class="m2"><p>صبح اگر ساغر دهد، تا شام نتوانم گرفت</p></div></div>