---
title: >-
    شمارهٔ  ۲۶۳
---
# شمارهٔ  ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>لفظ پاکیزه به دستم ز صفای تو رسد</p></div>
<div class="m2"><p>به کفم معنی رنگین زحنای تو رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجه اش همچو شقایق ز حنا گیرد رنگ</p></div>
<div class="m2"><p>دست عاشق چو به خاک کف پای تو رسد</p></div></div>