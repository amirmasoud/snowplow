---
title: >-
    شمارهٔ  ۱۰۵
---
# شمارهٔ  ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>آن مرغ بینواییم، کز آشیانه ما</p></div>
<div class="m2"><p>صد کوچه باغ راه است، تا آب و دانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما شعله ایم، لیکن پیش کسان ز تمکین</p></div>
<div class="m2"><p>ساکن چو آتش می، باشد زبانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با آنکه هر فسانه، خواب آورد به دیده</p></div>
<div class="m2"><p>از دیده می برد خواب، دایم فسانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابنای جنس ما را، چون مهره های شطرنج</p></div>
<div class="m2"><p>از بهر جنگ آرد، دوران به خانه ما</p></div></div>