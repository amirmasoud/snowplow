---
title: >-
    شمارهٔ  ۴۰۳
---
# شمارهٔ  ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>در شبستان دلم، از سرگذشت زلف او</p></div>
<div class="m2"><p>گر هوس خوابید، چندین آرزو بیدار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین تکاپو حاصلم چون آسیا بیگار شد</p></div>
<div class="m2"><p>مزد سرگردانی ام در کیسه اغیار شد</p></div></div>