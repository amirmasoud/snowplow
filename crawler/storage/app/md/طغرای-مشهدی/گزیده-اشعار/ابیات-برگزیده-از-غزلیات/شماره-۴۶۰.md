---
title: >-
    شمارهٔ  ۴۶۰
---
# شمارهٔ  ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>حباب وقت خودم در شط تنک ظرفی</p></div>
<div class="m2"><p>چه لب به خنده گشایم، که نیست تاب نشاط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اجل نیامده، بالین مرگ می سازم</p></div>
<div class="m2"><p>بدین بهانه مگر سر نهم به خواب نشاط</p></div></div>