---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>چو نیست در سخن خویش فتح باب مرا</p></div>
<div class="m2"><p>هزار حرف به دل مانده چون کتاب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که چرخ، سحرخیز سردمهری شد</p></div>
<div class="m2"><p>عجب که گرم شود تن به آفتاب مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جوی باغ فتادم، ولی ز بی قدری</p></div>
<div class="m2"><p>چو عکس شاخ درختان نبرد آب مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد او چو کنم گریه در سرمستی</p></div>
<div class="m2"><p>چکد به جامه بی طاقتی گلاب مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شمع، بود ز بیداری ام صبا گلچین</p></div>
<div class="m2"><p>نسیم نرگس او کرد مست خواب مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آبروی که خاکم قبول خواهد کرد؟</p></div>
<div class="m2"><p>اگر کسی ندهد غسل در شراب مرا</p></div></div>