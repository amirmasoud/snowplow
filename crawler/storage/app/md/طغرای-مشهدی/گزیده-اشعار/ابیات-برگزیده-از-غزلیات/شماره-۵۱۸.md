---
title: >-
    شمارهٔ  ۵۱۸
---
# شمارهٔ  ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>شد فراموش ز تکرار خموشی سخنم</p></div>
<div class="m2"><p>تخته مشق سکوت است زبان در دهنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که بی روی تو دارم سر خونریزی خود</p></div>
<div class="m2"><p>می شوم کشته، اگر تیغ کشد موی تنم!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب غربت به سر راه نشاطم گل شد</p></div>
<div class="m2"><p>هر غباری که به دل بود ز خاک وطنم</p></div></div>