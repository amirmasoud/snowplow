---
title: >-
    شمارهٔ  ۲۹۹
---
# شمارهٔ  ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>ثبات خانه تن را حباب می داند</p></div>
<div class="m2"><p>بنای جلوه گری را سراب می داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدح که هیچ نمی داند از مراتب بزم</p></div>
<div class="m2"><p>شراب را ز غم او کباب می داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نیم جو نخرد ذوق آرمیدن را</p></div>
<div class="m2"><p>دلی که چاشنی اضطراب می داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو سیل اشک روان کن، که خود ره جو را</p></div>
<div class="m2"><p>اگر چپ است وگر راست، آب می داند</p></div></div>