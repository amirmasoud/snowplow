---
title: >-
    شمارهٔ  ۲۳۳
---
# شمارهٔ  ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>چون نقش قدم، غیر زمین، بستر ما نیست</p></div>
<div class="m2"><p>از پای فتادیم و کسی بر سر ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما امت عشقیم، ز ما صبر مجویید</p></div>
<div class="m2"><p>کاین طرز عمل، گفته پیغمبر ما نیست</p></div></div>