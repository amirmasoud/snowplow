---
title: >-
    شمارهٔ  ۴۹۳
---
# شمارهٔ  ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>معنی مقصود را از لفظ یارب یافتم</p></div>
<div class="m2"><p>آنچه از من روز گم شد، دردل شب یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم دل در بزم گیتی باز کردم چون قدح</p></div>
<div class="m2"><p>شیشه افلاک را از خون لبالب یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی ز هفتاد و دو ملت می توانی یافتن</p></div>
<div class="m2"><p>اختلافی را که من در چار مذهب یافتم</p></div></div>