---
title: >-
    شمارهٔ  ۷۶۰
---
# شمارهٔ  ۷۶۰

<div class="b" id="bn1"><div class="m1"><p>چشم من سوی تو، چشم تو به سوی دگری</p></div>
<div class="m2"><p>من به روی تو ببینم، تو به روی دگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چو طوطی ز نم جوی طبیعت سبزیم</p></div>
<div class="m2"><p>سبزه ما نخورد آب ز جوی دگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاکی دل به تو در سجده تسلیم چه سود</p></div>
<div class="m2"><p>نتوان کرد عبادت به وضوی دگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سبوی خودم از دست رود بر سر آب</p></div>
<div class="m2"><p>به که چون دسته بچسبم به سبوی دگری</p></div></div>