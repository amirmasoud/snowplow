---
title: >-
    شمارهٔ  ۳۹۵
---
# شمارهٔ  ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>در بزمگاه وصل چو نوبت به ما رسد</p></div>
<div class="m2"><p>مینا جدا، پیاله جدا، می جدا رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل سر به نازبالش بلبل نمی نهد</p></div>
<div class="m2"><p>در گلشنی که پای نسیم حیا رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون طفل غنچه، از گل کم قسمتی مرا</p></div>
<div class="m2"><p>تا پیرهن ز کف نرود، کی قبا رسد</p></div></div>