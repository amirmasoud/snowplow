---
title: >-
    شمارهٔ  ۳۲۷
---
# شمارهٔ  ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>به پنجه مژه از باغ، ژاله باید چید</p></div>
<div class="m2"><p>به جای گل ز چمن داغ لاله باید چید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب تا نشود خانه، زود بر لب طاق</p></div>
<div class="m2"><p>قرابه های می دیرساله باید چید</p></div></div>