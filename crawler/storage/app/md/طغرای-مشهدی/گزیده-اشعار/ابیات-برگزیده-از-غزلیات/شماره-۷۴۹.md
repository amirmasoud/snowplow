---
title: >-
    شمارهٔ  ۷۴۹
---
# شمارهٔ  ۷۴۹

<div class="b" id="bn1"><div class="m1"><p>من نه ابرم، نه تو گل، بهر چه در باغ وصال</p></div>
<div class="m2"><p>چون زمین تر شود از گریه من، خنده کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روی پیشتر از موسم گل رو به چمن</p></div>
<div class="m2"><p>بی نیازش ز گل موسم آینده کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و آن لعل روان بخش که از یک سخنش</p></div>
<div class="m2"><p>دل اگر مرده صدساله بود، زنده کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که خوب است سراپای تنت چون خورشید</p></div>
<div class="m2"><p>بدنما گر بود آیینه، خوشاینده کنی</p></div></div>