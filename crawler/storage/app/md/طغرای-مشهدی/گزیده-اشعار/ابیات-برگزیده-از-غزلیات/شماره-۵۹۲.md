---
title: >-
    شمارهٔ  ۵۹۲
---
# شمارهٔ  ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>خدایا آن صنم را مایل حسن آزمایی کن</p></div>
<div class="m2"><p>چراغم را زرویش دودمان روشنایی کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک قتلگاه عافیت بر من فشان گردی</p></div>
<div class="m2"><p>لباس عیدی ام را سرخ چون دست حنایی کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگشتم از ازل در هیچ کاری قابل تحسین</p></div>
<div class="m2"><p>اگر گویم مسلمانم، تو کافرماجرایی کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوس بر من شد از زلف بتان هر سو کمند افکن</p></div>
<div class="m2"><p>تو این صید گرانجان را سبکپای رهایی کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم از خویش غافل، می شناسم خونی خود را</p></div>
<div class="m2"><p>به قتلم در لباس هر که خواهی خودنمایی کن</p></div></div>