---
title: >-
    شمارهٔ  ۲۰۵
---
# شمارهٔ  ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>زلف با قامت او، تا به کمر همراه است</p></div>
<div class="m2"><p>هر کجا روز بلند است، شبش کوتاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی وصیت دلم از خود نرود شام فراق</p></div>
<div class="m2"><p>این چراغی ست که از رفتن خود آگاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من دیوانه چه سان بگذرم از وادی عشق</p></div>
<div class="m2"><p>جاده چون کوچه زنجیر، سراسر چاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل زخمی که ز پیکان غمت در دل ماند</p></div>
<div class="m2"><p>همچو نقش قدم خضر، زیارتگاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به گوشت نرسد ناله طغرا چه عجب</p></div>
<div class="m2"><p>بس که از عشق زبون گشته، فغانش آه است</p></div></div>