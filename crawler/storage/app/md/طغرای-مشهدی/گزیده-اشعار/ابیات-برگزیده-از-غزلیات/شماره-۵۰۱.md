---
title: >-
    شمارهٔ  ۵۰۱
---
# شمارهٔ  ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>نهان شد صافی ام ز آمیزش ناصاف این مجلس</p></div>
<div class="m2"><p>به چشم میکشان صهبای دردآلود را مانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهد پا بر بساط مشربم ترسا وگر مومن</p></div>
<div class="m2"><p>تفاوت نیست پیشم، درگه معبود را مانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جور آسمان، گفتار و کردارم دگرگون شد</p></div>
<div class="m2"><p>به احمد پیروم، لیک امت محمود را مانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عصیان می گریزم وز عبادت می کشم تاوان</p></div>
<div class="m2"><p>نه دنیایی، نه دینی، زاهد مردود را مانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجز نقصان ندیدم از جنون عاشقی طغرا</p></div>
<div class="m2"><p>به سودا دشمنم، سوداگر بی سود را مانم</p></div></div>