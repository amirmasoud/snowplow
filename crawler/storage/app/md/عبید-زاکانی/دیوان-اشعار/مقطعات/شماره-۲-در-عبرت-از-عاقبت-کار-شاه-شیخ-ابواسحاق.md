---
title: >-
    شمارهٔ ۲ - در عبرت از عاقبت کار شاه شیخ ابواسحاق
---
# شمارهٔ ۲ - در عبرت از عاقبت کار شاه شیخ ابواسحاق

<div class="b" id="bn1"><div class="m1"><p>سلطان تاج بخش جهاندار امیر شیخ</p></div>
<div class="m2"><p>کاوازهٔ سعادت جودش جهان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی چو کیقباد و چو افراسیاب گرد</p></div>
<div class="m2"><p>کشور چو شاه سنجر و شاه اردوان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پشتی دین به قوت تدبیر پیر کرد</p></div>
<div class="m2"><p>روی زمین به بازوی بخت جوان گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عیش ساز و عادت خسرو بنا نهاد</p></div>
<div class="m2"><p>در رسم و عدل شیوهٔ نوشیروان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایوان و قصر و جنت و فردوس برفراشت</p></div>
<div class="m2"><p>در وی نشست شاد و قدح شادمان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بنده‌ای که بر در او جایگاه یافت</p></div>
<div class="m2"><p>خود را امیر خسرو صاحبقران گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنگر که روزگار چه بازی پدید کرد</p></div>
<div class="m2"><p>نکبت چگونه دولت او را عنان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوشی بزد محیط بلائی به ناگهان</p></div>
<div class="m2"><p>ملک و خزانه و پسرش در میان گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا سوز و گریه‌ای که بهم برزد آن بنا</p></div>
<div class="m2"><p>یا دود ناله‌ای که در آن دودمان گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کان بوستان سرای که آئین و رنگ و بوی</p></div>
<div class="m2"><p>خلد برین ز رونق آن بوستان گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اکنون بدان رسید که بر جای عندلیب</p></div>
<div class="m2"><p>زاغ سیه دل آمد و در او مکان گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قصری که برد فرخی از فر او همای</p></div>
<div class="m2"><p>سگ بچه کرد در وی و جغد آشیان گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در کار روزگار و ثبات جهان عبید</p></div>
<div class="m2"><p>عبرت هزار بار از این می‌توان گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بیچاره آدمی چو ندارد به هیچ حال</p></div>
<div class="m2"><p>نه بر ستاره داد و نه بر آسمان گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوشوقت مقبلی که دل اندر جهان نبست</p></div>
<div class="m2"><p>واسوده خاطریکه ز دنیا کران گرفت</p></div></div>