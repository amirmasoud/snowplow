---
title: >-
    شمارهٔ ۱۸ - در وصف معشوقه گوید
---
# شمارهٔ ۱۸ - در وصف معشوقه گوید

<div class="b" id="bn1"><div class="m1"><p>زهی لعل لب نازک میانت</p></div>
<div class="m2"><p>مراد دیدهٔ باریک بینان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم عشقت به هشیاری و مستی</p></div>
<div class="m2"><p>مراد دیدهٔ خلوت نشینان</p></div></div>