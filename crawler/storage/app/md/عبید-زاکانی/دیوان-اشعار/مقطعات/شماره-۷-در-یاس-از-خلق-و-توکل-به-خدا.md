---
title: >-
    شمارهٔ ۷ - در یاس از خلق و توکل به خدا
---
# شمارهٔ ۷ - در یاس از خلق و توکل به خدا

<div class="b" id="bn1"><div class="m1"><p>نماند هیچ کریمی که پای خاطر من</p></div>
<div class="m2"><p>ز بند حادثهٔ روزگار بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال بود مرا کان غرض که مقصود است</p></div>
<div class="m2"><p>حصول آن غرض از شهریار بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان هوس بر سلطان کامران رفتم</p></div>
<div class="m2"><p>که از عطای ویم کار و بار بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیش شاه و وزیرم دری گشاده نشد</p></div>
<div class="m2"><p>مگر ز غیب دری کدر کار بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبید حاجت از آن درطلب که رحمت او</p></div>
<div class="m2"><p>اگر ببندد یک در هزار بگشاید</p></div></div>