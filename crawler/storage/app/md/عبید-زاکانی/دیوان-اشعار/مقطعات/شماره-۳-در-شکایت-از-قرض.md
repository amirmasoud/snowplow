---
title: >-
    شمارهٔ ۳ - در شکایت از قرض
---
# شمارهٔ ۳ - در شکایت از قرض

<div class="b" id="bn1"><div class="m1"><p>مرا قرض هست و دگر هیچ نیست</p></div>
<div class="m2"><p>فراوان مرا خرج و زر هیچ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان گو همه عیش و عشرت بگیر</p></div>
<div class="m2"><p>مرا زین حکایت خبر هیچ نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنر خود ندانم و گر نیز هست</p></div>
<div class="m2"><p>چو طالع نباشد هنر هیچ نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنان ارادت چو از دست رفت</p></div>
<div class="m2"><p>غم و فکر برگ و دگر هیچ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به درگاه او التجا کن عبید</p></div>
<div class="m2"><p>که این رفتن در به در هیچ نیست</p></div></div>