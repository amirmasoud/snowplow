---
title: >-
    شمارهٔ ۱۶ - در شکایت از قرض گوید
---
# شمارهٔ ۱۶ - در شکایت از قرض گوید

<div class="b" id="bn1"><div class="m1"><p>مردم به عیش و شادی و من در بلای قرض</p></div>
<div class="m2"><p>هریک به کار و باری و من مبتلای قرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرض خدا و قرض خلایق به گردنم</p></div>
<div class="m2"><p>آیا ادای فرض کنم یا ادای قرض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرجم فزون ز غایت و قرضم برون ز حد</p></div>
<div class="m2"><p>فکر از برای خرج کنم یا برای قرض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هیچ خط نتابم غیر از سجل دین</p></div>
<div class="m2"><p>وز هیچکس ننالم غیر از گوای قرض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شهر قرض دارم واندر محله قرض</p></div>
<div class="m2"><p>در کوچه قرض دارم واندر سرای قرض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از صبح تا به شام در اندیشه مانده‌ام</p></div>
<div class="m2"><p>تا خود کجا بیابم ناگه رجای قرض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردم ز دست قرض گریزان و من به صدق</p></div>
<div class="m2"><p>خواهم پس از نماز و دعا از خدای قرض</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرضم چو آبروی گدایان به باد رفت</p></div>
<div class="m2"><p>از بس که خواستم ز در هر گدای قرض</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خواجه تربیت نکند نزد پادشا</p></div>
<div class="m2"><p>مسکین عبید چون کند آخر دوای قرض</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه علاء دولت و دین آن که جز کفش</p></div>
<div class="m2"><p>هرگز کسی ندید به گیتی سزای قرض</p></div></div>