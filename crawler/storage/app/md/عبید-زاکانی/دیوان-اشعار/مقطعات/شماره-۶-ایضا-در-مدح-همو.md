---
title: >-
    شمارهٔ ۶ - ایضا در مدح همو
---
# شمارهٔ ۶ - ایضا در مدح همو

<div class="b" id="bn1"><div class="m1"><p>ای جوانبخت وزیری که کند افسر سر</p></div>
<div class="m2"><p>خاک پایت چو بدین گنبد خضرا برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان هر خسته ز لطف تو دوا کسب کند</p></div>
<div class="m2"><p>دل هرکس ز عطایت به تمنی برسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک را چون تو عمیدی چو خدا روزی کرد</p></div>
<div class="m2"><p>رکن اسلام ز نام تو به اعلی برسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسروا بنده عبید از کرمت دارد چشم</p></div>
<div class="m2"><p>کش ز یمن نظرت کار به بالا برسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بود مصلحت احوال دعاگو با شاه</p></div>
<div class="m2"><p>عرضه فرمای چو رایات به بیضا برسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیتکی چند ز اشعار کسان داردم یاد</p></div>
<div class="m2"><p>یک دو زان شاید اگر زانکه به آنها برسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«آنکه او هست در این دور به نانی خرسند</p></div>
<div class="m2"><p>حرص گیرد چو بدین حضرت والا برسد»</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آری از چاه به جز آب تمنی نکند</p></div>
<div class="m2"><p>بار گوهر طلبد هرکه به دریا برسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا ابد کامروا باشد که خصمت گر خود</p></div>
<div class="m2"><p>نظری باز کند مرگ مفاجا برسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مدت عمر تو چندانکه پیاپی صد بار</p></div>
<div class="m2"><p>جرم خورشید جهانگرد به جوزا برسد</p></div></div>