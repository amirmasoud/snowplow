---
title: >-
    شمارهٔ ۲۴ - در وصف قلعهٔ دارالامان کرمان گوید
---
# شمارهٔ ۲۴ - در وصف قلعهٔ دارالامان کرمان گوید

<div class="b" id="bn1"><div class="m1"><p>حریم قلعهٔ دارالامان که در عالم</p></div>
<div class="m2"><p>چو آسمان به بلندیش نیست همتائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نسبت من و با استری که من دارم</p></div>
<div class="m2"><p>به راستی که بلائی است این نه بلائی</p></div></div>