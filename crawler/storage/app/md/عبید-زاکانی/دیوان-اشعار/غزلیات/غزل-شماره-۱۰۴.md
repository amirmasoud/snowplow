---
title: >-
    غزل شمارهٔ ۱۰۴
---
# غزل شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>عزم کجا کرده‌ای باز که برخاستی</p></div>
<div class="m2"><p>موی به شانه زدی زلف بیاراستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه چو روی تو دید گفت زهی نیکوی</p></div>
<div class="m2"><p>سرو که قد تو دید گفت زهی راستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش غوغای عشق چون بنشستی نشست</p></div>
<div class="m2"><p>فتنهٔ آخر زمان خاست چو برخاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش در آن سرخوشی هوش ز ما میر بود</p></div>
<div class="m2"><p>کاسه که میداشتی عذر که میخواستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش عبید آمدی مرده دلش زنده شد</p></div>
<div class="m2"><p>باز چو بیرون شدی جان و تنش کاستی</p></div></div>