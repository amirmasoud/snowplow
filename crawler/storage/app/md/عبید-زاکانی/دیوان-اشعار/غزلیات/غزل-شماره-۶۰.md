---
title: >-
    غزل شمارهٔ ۶۰
---
# غزل شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>یار پیمان شکنم با سر پیمان آمد</p></div>
<div class="m2"><p>دل پر درد مرا نوبت درمان آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چه ماهیست که کاشانهٔ ما روشن کرد</p></div>
<div class="m2"><p>وین چه شمعیست که بازم به شبستان آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخت باز آمد و طالع در دولت بگشاد</p></div>
<div class="m2"><p>مدعی رفت و مرا کار به سامان آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می بیارید که ایام طرب روی نمود</p></div>
<div class="m2"><p>گل بریزید که آن سرو خرامان آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر لطف ببخشود بر احوال عبید</p></div>
<div class="m2"><p>مگرش رحم بدین دیدهٔ گریان آمد</p></div></div>