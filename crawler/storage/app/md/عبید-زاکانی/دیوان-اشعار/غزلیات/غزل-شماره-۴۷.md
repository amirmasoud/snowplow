---
title: >-
    غزل شمارهٔ ۴۷
---
# غزل شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>باد صبا جیب سمن برگشاد</p></div>
<div class="m2"><p>غلغل بلبل به چمن در فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنده کند مردهٔ صد ساله را</p></div>
<div class="m2"><p>باد چو بر گل گذرد بامداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمزم مرغان سخندان شنو</p></div>
<div class="m2"><p>تا نکنی نغمهٔ داود یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موسم عیشست غنیمت شمار</p></div>
<div class="m2"><p>هرزه مده عمر و جوانی به باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت به افسوس نشاید گذاشت</p></div>
<div class="m2"><p>جام می از دست نباید نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بتوان خاطر خود شاددار</p></div>
<div class="m2"><p>نیست بدین یک دو نفس اعتماد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک همانست که بر باد داد</p></div>
<div class="m2"><p>تخت سلیمان و سریر قباد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرخ همانست که بر خاک ریخت</p></div>
<div class="m2"><p>خون سیاووش و سر کیقباد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انده دنیا بگذار ای عبید</p></div>
<div class="m2"><p>تا بتوان زیست یکی لحظه شاد</p></div></div>