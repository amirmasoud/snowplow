---
title: >-
    غزل شمارهٔ ۱۹
---
# غزل شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ز سنبلی که عذارت بر ارغوان انداخت</p></div>
<div class="m2"><p>مرا به بیخودی آوازه در جهان انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرح زلف تو موئی هنوز نا گفته</p></div>
<div class="m2"><p>دلم هزار گره در سر زبان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان تو صفتی از ضعیفیم میگفت</p></div>
<div class="m2"><p>مرا ز هستی خود نیک در گمان انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمان ابروی پیوسته میکشی تا گوش</p></div>
<div class="m2"><p>بدان امید که صیدی کجا توان انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دلفریبی مویت سخن دراز کشید</p></div>
<div class="m2"><p>لب تو نکتهٔ باریک در میان انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب مدار که در دور روی و ابرویت</p></div>
<div class="m2"><p>سپر فکند مه از عجز تا کمان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سر عشق هر آنچ از عبید پنهان بود</p></div>
<div class="m2"><p>سرشگ جمله در افواه مردمان انداخت</p></div></div>