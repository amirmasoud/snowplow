---
title: >-
    غزل شمارهٔ ۱۴
---
# غزل شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>دلداده را ز تیر ملامت گزند نیست</p></div>
<div class="m2"><p>دیوانه را طریقهٔ عاقل پسند نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درد ما چه فکر وز احوال ما چه باک</p></div>
<div class="m2"><p>آنرا که دل مقید و پا در کمند نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرهاد را که با دل شیرین تعلقست</p></div>
<div class="m2"><p>رغبت به نوشدارو و حاجت به قند نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرجا که آتش غم دلدار شعله زد</p></div>
<div class="m2"><p>جان برفشان به ذوق که جای سپند نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس کن عبید با دل شوریده داوری</p></div>
<div class="m2"><p>بیچاره را نصیحت ما سودمند نیست</p></div></div>