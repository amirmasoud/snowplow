---
title: >-
    غزل شمارهٔ ۳۵
---
# غزل شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ساقیا باز خرابیم بده جامی چند</p></div>
<div class="m2"><p>پخته‌ای چند فرو ریز به ما خامی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی و گوشهٔ محراب و نکونامی و زرق</p></div>
<div class="m2"><p>ما و میخانه و دردی کش و بدنامی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده پیش آر که بر طرف چمن خوش باشد</p></div>
<div class="m2"><p>مطربی چند و گلی چند و گل اندامی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم و لب پیش من آور چو رسد باده به من</p></div>
<div class="m2"><p>تا بود نقل مرا شکر و بادامی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده در خانه اگر نیست برای دل ما</p></div>
<div class="m2"><p>رنجه شو تا در میخانه بنه گامی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بهای می گلگون اگرت زر نبود</p></div>
<div class="m2"><p>خرقهٔ ما به گرو کن بستان جامی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذکر سجاده و تسبیح رها کن چو عبید</p></div>
<div class="m2"><p>نشوی صید بدین دانه بنه دامی چند</p></div></div>