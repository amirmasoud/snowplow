---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>دلی که بستهٔ زنجیر زلف یاری نیست</p></div>
<div class="m2"><p>به پیش اهل نظر هیچش اعتباری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری که نیست در او کارگاه سودائی</p></div>
<div class="m2"><p>به کارخانهٔ عیشش سری و کاری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عقل برشکن و ذوق بیخودی دریاب</p></div>
<div class="m2"><p>که پیش زنده دلان عقل در شماری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملامت من مسکین مکن که در ره عشق</p></div>
<div class="m2"><p>به دست عاشق بیچاره اختیاری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر مگوی که هر بحر را کناری هست</p></div>
<div class="m2"><p>از آنکه به جز غم عشق را کناری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شوق زلف بتان بیقرار و سرگردان</p></div>
<div class="m2"><p>منم که مثل من آشفته روزگاری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ز مستی و رندی عبید را عاریست</p></div>
<div class="m2"><p>مرا از این دو صفت هیچ عیب و عاری نیست</p></div></div>