---
title: >-
    غزل شمارهٔ ۹۰
---
# غزل شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>دلا باز آشفته کاری مکن</p></div>
<div class="m2"><p>چو دیوانگان بیقراری مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرت نیست دردی، غنیمت شمار</p></div>
<div class="m2"><p>ورت هست فریاد و زاری مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کارت ز عشقست و بارت ز عشق</p></div>
<div class="m2"><p>شکایت ز بی کار و باری مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگارا نگارا جدائی ز ما</p></div>
<div class="m2"><p>خدا را اگر دوست داری مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چشم سرمست اودیده‌ای</p></div>
<div class="m2"><p>دگر دعوی هوشیاری مکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جور و جفا هرچه ممکن بود</p></div>
<div class="m2"><p>بکن ترک پیمان و یاری مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبید ار سر عشق داری بیا</p></div>
<div class="m2"><p>در این راه جز جانسپاری مکن</p></div></div>