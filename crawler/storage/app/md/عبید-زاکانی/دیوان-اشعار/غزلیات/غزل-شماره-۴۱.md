---
title: >-
    غزل شمارهٔ ۴۱
---
# غزل شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>دوش لعلت نفسی خاطر ما خوش می‌کرد</p></div>
<div class="m2"><p>دیده می‌دید جمال تو و دل غش می‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی زیبای تو با ماه یکایک می‌زد</p></div>
<div class="m2"><p>سر گیسوی تو با باد کشاکش می‌کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سنبل زلف تو هرلحظه پریشان می‌شد</p></div>
<div class="m2"><p>خاطر خستهٔ عشاق مشوش می‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زو هر آن حلقه بر گوشهٔ مه می‌افتاد</p></div>
<div class="m2"><p>دل مسکین مرا نعل در آتش می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر بر سینه‌ام آن غمزهٔ فتان می‌زد</p></div>
<div class="m2"><p>قصد خون دلم آن عارض مهوش می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خط و خال و بناگوش و لب و چشم و رخت</p></div>
<div class="m2"><p>هرکه یک بوسه طمع داشت غلط شش می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش نقش رخ تو دیدهٔ خونریز عبید</p></div>
<div class="m2"><p>صفحهٔ چهره به خونابه منقش می‌کرد</p></div></div>