---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>رفتم از خطهٔ شیراز و به جان در خطرم</p></div>
<div class="m2"><p>وه کزین رفتن ناچار چه خونین جگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میروم دست زنان بر سر و پای اندر گل</p></div>
<div class="m2"><p>زین سفر تا چه شود حال و چه آید به سرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه چون بلبل شوریده درآیم به خروش</p></div>
<div class="m2"><p>گاه چون غنچهٔ دلتنگ گریبان بدرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از این شهر اگر برشکنم در شکنم</p></div>
<div class="m2"><p>من از این کوی اگر برگذرم درگذرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌خود و بی‌دل و بی‌یار برون از شیراز</p></div>
<div class="m2"><p>«میروم وز سر حسرت به قفا مینگرم»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قوت دست ندارم چو عنان میگیرم</p></div>
<div class="m2"><p>«خبر از پای ندارم که زمین می‌سپرم»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چنین زار که امروز منم در غم عشق</p></div>
<div class="m2"><p>قول ناصح نکند چاره و پند پدرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عبید این سفری نیست که من میخواهم</p></div>
<div class="m2"><p>میکشد دهر به زنجیر قضا و قدرم</p></div></div>