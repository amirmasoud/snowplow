---
title: >-
    غزل شمارهٔ ۶۹
---
# غزل شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>چمن دل بردن آیین میکند باز</p></div>
<div class="m2"><p>جهان را لاله رنگین میکند باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم خوش نفس با غنچه هر دم</p></div>
<div class="m2"><p>حدیث نافهٔ چین میکند باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوفه هر زری کاورد بر دست</p></div>
<div class="m2"><p>نثار پای نسرین میکند باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشاده چشم خواب آلود نرگس</p></div>
<div class="m2"><p>تماشای ریاحین میکند باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین از ابر احسان می‌پذیرد</p></div>
<div class="m2"><p>هوا را سبزه تحسین میکند باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عبید از دولت خسرو در این فصل</p></div>
<div class="m2"><p>بنای عیش شیرین میکند باز</p></div></div>