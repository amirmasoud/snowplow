---
title: >-
    غزل شمارهٔ ۱۷
---
# غزل شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>رمید صبر و دل از من چو دلنواز برفت</p></div>
<div class="m2"><p>چه چاره سازم از این پس چو چاره‌ساز برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوار گشته و عمدا گرفته باز به دست</p></div>
<div class="m2"><p>نموده روی به بیچارگان و باز برفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گریه چشمهٔ چشم بریخت چندان خون</p></div>
<div class="m2"><p>که کهنه خرقهٔ سالوسم از نماز برفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز از خیال قد و زلف یار و قصهٔ شوق</p></div>
<div class="m2"><p>دگر ز خاطرم اندیشهٔ دراز برفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز منع خلق از این بیش محترز بودم</p></div>
<div class="m2"><p>کنون حدیث من از حد احتراز برفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغ و درد که در هجر یار و غصهٔ دهر</p></div>
<div class="m2"><p>برفت عمر و حقیقت که بر مجاز برفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبید چون جرست ناله سود می‌نکند</p></div>
<div class="m2"><p>چو کاروان جرس جمله بیجواز برفت</p></div></div>