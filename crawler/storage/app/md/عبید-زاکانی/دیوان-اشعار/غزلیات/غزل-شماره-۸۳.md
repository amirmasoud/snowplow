---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>قصد آن زلفین سرکش کرده‌ام</p></div>
<div class="m2"><p>خاطر از سودا مشوش کرده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عشقش میان جان و دل</p></div>
<div class="m2"><p>منزل اندر آب و آتش کرده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از وصالش تا طمع ببریده‌ام</p></div>
<div class="m2"><p>با خیالش وقت خود خوش کرده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نسیم گلستان تا شمه‌ای</p></div>
<div class="m2"><p>بوی او بشنیده‌ام غش کرده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیش او بگرفته قربان گشته‌ام</p></div>
<div class="m2"><p>تا نپنداری که ترکش کرده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دو لعل و از دو ابرو و دو زلف</p></div>
<div class="m2"><p>گر امان یابم غلط شش کرده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل طلب کردم ز زلفش بانک زد</p></div>
<div class="m2"><p>کای عبید آنجا فروکش کرده‌ام</p></div></div>