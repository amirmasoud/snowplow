---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>گرم عنایت او در بروی بگشاید</p></div>
<div class="m2"><p>هزار دولتم از غیب روی بنماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظر به گلشن روحانیون نیندازم</p></div>
<div class="m2"><p>سرم به پایهٔ کروبیان فرو ناید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر به حال پریشان ما کند نظری</p></div>
<div class="m2"><p>ز روی لطف بر احوال ما ببخشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش خاطرم ار کاینات عرضه کنند</p></div>
<div class="m2"><p>ز کبر دامن همت بدان نیالاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توان در آینهٔ آن جمال جان دیدن</p></div>
<div class="m2"><p>گرش به صیقل توفیق زنگ بزداید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ورم ز پیش براند به جور حکم اوراست</p></div>
<div class="m2"><p>پسند دوست بود هرچه دوست فرماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبید را کرمش تا نوازشی نکند</p></div>
<div class="m2"><p>دلش ز غم نرهد خاطرش نیاساید</p></div></div>