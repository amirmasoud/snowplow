---
title: >-
    غزل شمارهٔ ۲۲
---
# غزل شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>جانا بیا که بی تو دلم را قرار نیست</p></div>
<div class="m2"><p>بیشم مجال صبر و سر انتظار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوانه این چنین که منم در بلای عشق</p></div>
<div class="m2"><p>دل عاقبت نخواهد و عقلم به کار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خواندنت مراد و گر راندن آرزوست</p></div>
<div class="m2"><p>آن کن که رای تست مرا اختیار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را همین بسست که داریم درد عشق</p></div>
<div class="m2"><p>مقصود ما ز وصل تو بوس و کنار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل همیشه عاشق و همواره مست باش</p></div>
<div class="m2"><p>کان کس که مست عشق نشد هوشیار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عشق همنشین شو و از عقل برشکن</p></div>
<div class="m2"><p>کو را به پیش اهل نظر اعتبار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قوم را طریقی و راهی و قبله‌ایست</p></div>
<div class="m2"><p>پیش عبید قبله به جز کوی یار نیست</p></div></div>