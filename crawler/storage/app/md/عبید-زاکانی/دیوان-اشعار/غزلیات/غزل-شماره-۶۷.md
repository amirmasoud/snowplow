---
title: >-
    غزل شمارهٔ ۶۷
---
# غزل شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>میپزد باز سرم بیهده سودای دگر</p></div>
<div class="m2"><p>میکند خاطر شوریده تمنای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوس سروقدی گرد دلم میگردد</p></div>
<div class="m2"><p>که ندارد به جهان همسر و همتای دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش در کوی خودم نعره زنان دیده ز دور</p></div>
<div class="m2"><p>گشته رسوای جهان با دو سه شیدای دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت کاین شیفته را باز چه حال افتاد است</p></div>
<div class="m2"><p>نیست جز مسکنت و عجز مداوای دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاره صبر است ز سعدی بشنو پند عبید</p></div>
<div class="m2"><p>«سعدی امروز تحمل کن و فردای دگر»</p></div></div>