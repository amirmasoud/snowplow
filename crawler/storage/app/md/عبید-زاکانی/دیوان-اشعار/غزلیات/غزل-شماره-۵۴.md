---
title: >-
    غزل شمارهٔ ۵۴
---
# غزل شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>وداع کعبهٔ جان چون توان کرد</p></div>
<div class="m2"><p>فراقش بر دل آسان چون توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیبم میرود من درد خود را</p></div>
<div class="m2"><p>نمیدانم که درمان چون توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا عهدیست کاندر پاش میرم</p></div>
<div class="m2"><p>خلاف عهد و پیمان چون توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کفر زلفش ایمان هرکه آورد</p></div>
<div class="m2"><p>دگر بارش مسلمان چون توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گویند پنهان دار رازش</p></div>
<div class="m2"><p>غم عشقست پنهان چون توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم راز دل بتوان نهفتن</p></div>
<div class="m2"><p>دوای چشم گریان چون توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبید از عشق اگر دیوانه گردد</p></div>
<div class="m2"><p>بدین جرمش به زندان چون توان کرد</p></div></div>