---
title: >-
    غزل شمارهٔ ۹۲
---
# غزل شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>منگر به حدیث خرقه‌پوشان</p></div>
<div class="m2"><p>آن سخت‌دلان سست‌کوشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آویخته سبحه‌شان به گردن</p></div>
<div class="m2"><p>همچون جرس از درازگوشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دور چو کشتگان ببینی</p></div>
<div class="m2"><p>از راه بگرد و رو بپوشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بند ریا و زرق برخیز</p></div>
<div class="m2"><p>با ساده‌نشین و باده‌نوشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مفروش به ملک هردو عالم</p></div>
<div class="m2"><p>خاک سر کوی میْ‌فروشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ چه خوش بود سحرگاه</p></div>
<div class="m2"><p>ما سرخوش و بلبلان خروشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب غزل عبید برخوان</p></div>
<div class="m2"><p>دل برده ز دست تیزهوشان</p></div></div>