---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>در خانه تا قرابهٔ ما پر شراب نیست</p></div>
<div class="m2"><p>ما را قرار و راحت و آرام و خواب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خلوتی که باده و ساقی و شاهد است</p></div>
<div class="m2"><p>حاجت به چنگ و بربط و نای و رباب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش کن به باده وقت حریفان که پیش ما</p></div>
<div class="m2"><p>عمری که خوش نمیگذرد در حساب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک شراب اگر هوست میکند وضو</p></div>
<div class="m2"><p>در آفتابه کن که در این خانه آب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را که ملک فقر و قناعت مسلم است</p></div>
<div class="m2"><p>حاجت به جود خسرو مالک رقاب نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون عبید خانهٔ هستی خراب کن</p></div>
<div class="m2"><p>زیرا که جای گنج به جز در خراب نیست</p></div></div>