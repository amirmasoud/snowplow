---
title: >-
    غزل شمارهٔ ۷۲
---
# غزل شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>بی‌یار دل شکسته و دور از دیار خویش</p></div>
<div class="m2"><p>درمانده‌ایم عاجز و حیران به کار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روزگار هیچ مرادی نیافتیم</p></div>
<div class="m2"><p>آزرده‌ایم لاجرم از روزگار خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه کار دل به کام و نه دلدار سازگار</p></div>
<div class="m2"><p>خونین دلم ز طالع ناسازگار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکدم قرار نیست دلم را ز تاب عشق</p></div>
<div class="m2"><p>در آتشم ز دست دل بی‌قرار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر آنکه میزند آبی بر آتشم</p></div>
<div class="m2"><p>منت پذیرم از مژهٔ سیل‌بار خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوانه دل به عشق سپارد عبیدوار</p></div>
<div class="m2"><p>عاقل به دست دل ندهد اختیار خویش</p></div></div>