---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>میکند سلسلهٔ زلف تو دیوانه مرا</p></div>
<div class="m2"><p>میکشد نرگس مست تو به میخانه مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متحیر شده‌ام تا غم عشقت ناگاه</p></div>
<div class="m2"><p>از کجا یافت در این گوشهٔ ویرانه مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس در بناگوش تو دارد دل من</p></div>
<div class="m2"><p>قطرهٔ اشگ از آنست چو دردانه مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولتی یابم اگر در نظر شمع رخت</p></div>
<div class="m2"><p>کشته و سوخته یابند چو پروانه مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد سر میدهد این واعظ و میپندارد</p></div>
<div class="m2"><p>کالتفاتست بدان بیهده افسانه مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاره آنست که دیوانگیی پیش آرم</p></div>
<div class="m2"><p>تا فراموش کند واعظ فرزانه مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از می مهر تو تا مست شدم همچو عبید</p></div>
<div class="m2"><p>نیست دیگر هوس ساغر و پیمانه مرا</p></div></div>