---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>هرگه که شبی خود را در میکده اندازیم</p></div>
<div class="m2"><p>صد فتنه برانگیزیم صد کیسه بپردازیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سر که بود در می وان راز که گویدنی</p></div>
<div class="m2"><p>ما مونس آن سریم ما محرم آن رازیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نغمه که پیش آرند ما با همه در شوریم</p></div>
<div class="m2"><p>هر ساز که بنوازند ما با همه در سازیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین پیش کسی بودیم و امروز در این کشور</p></div>
<div class="m2"><p>ما جمری بغدادیم ما بکروی شیرازیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر حکم کند سلطان کین باده براندازند</p></div>
<div class="m2"><p>او باده براندازد ما بنک براندازیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنروز که در محشر مردم همه گرد آیند</p></div>
<div class="m2"><p>ما با تو در آن غوغا دزدیده نظر بازیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر یاد تو هر ساعت مانند عبید اکنون</p></div>
<div class="m2"><p>بزمی دگر افروزیم عیشی دگر آغازیم</p></div></div>