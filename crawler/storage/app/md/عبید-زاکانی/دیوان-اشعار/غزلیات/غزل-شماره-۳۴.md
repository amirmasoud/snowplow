---
title: >-
    غزل شمارهٔ ۳۴
---
# غزل شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>دلم ز عشق تبرا نمی‌تواند کرد</p></div>
<div class="m2"><p>صبوری از رخ زیبا نمی‌تواند کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم از درون دل من برون نمی‌آید</p></div>
<div class="m2"><p>که ترک مسکن و ماوی نمی‌تواند کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروی خوب مرا دیده روشنست ولی</p></div>
<div class="m2"><p>به هیچ وجه مهیا نمی‌تواند کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برفت دوش خیالش ز چشم من چه کند</p></div>
<div class="m2"><p>مقام بر لب دریا نمی‌تواند کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صبر کام توان یافتن ولیک چه سود</p></div>
<div class="m2"><p>چو صبر در دل ما جا نمی‌تواند کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عبید گه گهی از بهر مصلحت میگفت</p></div>
<div class="m2"><p>که توبه میکند اما نمی‌تواند کرد</p></div></div>