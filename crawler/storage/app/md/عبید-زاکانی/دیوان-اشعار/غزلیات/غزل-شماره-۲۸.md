---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>سر نخوانیم که سودا زدهٔ موئی نیست</p></div>
<div class="m2"><p>آدمی نیست که مجنون پری‌روئی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز از بند و غم آزاد نگردد آن دل</p></div>
<div class="m2"><p>که گرفتار کمند سر گیسوئی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبله‌ام روی بتانست و وطن کوی مغان</p></div>
<div class="m2"><p>به از این قبله‌ام و خوشتر از این کوئی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس مرا از دل سرگشته نشانی ندهد</p></div>
<div class="m2"><p>عجب از معتکف گوشهٔ ابروئی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میتوان دامن وصلت به کف آورد ولی</p></div>
<div class="m2"><p>ای دریغا که مرا قوت بازوئی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر مرض دارو و هر درد علاجی دارد</p></div>
<div class="m2"><p>زخم تیر مژه را مرهم و داروئی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر موئی نتوان یافت بر اعضای عبید</p></div>
<div class="m2"><p>که در او ناوکی از غمزهٔ جادوئی نیست</p></div></div>