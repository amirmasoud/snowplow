---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>سپیده‌دم به صبوحی شراب خوش باشد</p></div>
<div class="m2"><p>نوا و نغمهٔ چنگ و رباب خوش باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتی که مست و خرابی ز چشم فتانش</p></div>
<div class="m2"><p>نشسته پیش تو مست و خراب خوش باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحرگهان چو ز خواب خمار برخیزی</p></div>
<div class="m2"><p>خیال بنگ و نشاط شراب خوش باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان باغ چو وصل نگار دست دهد</p></div>
<div class="m2"><p>کنار آب و شب ماهتاب خوش باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمایل خوش جانان به خواب دیدم دوش</p></div>
<div class="m2"><p>امید هست که تعبیر خواب خوش باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عبید این دو سه بیتک به یک زمان گفته است</p></div>
<div class="m2"><p>گرش تو گفت توانی جواب خوش باشد</p></div></div>