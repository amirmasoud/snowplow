---
title: >-
    غزل شمارهٔ ۳۱
---
# غزل شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ز من مپرس که بر من چه حال می‌گذرد</p></div>
<div class="m2"><p>چو روز وصل توام در خیال می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان برابر چشمم سیاه می‌گردد</p></div>
<div class="m2"><p>چو در ضمیر من آن زلف و خال می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر هلاک خودم آرزوست منع مکن</p></div>
<div class="m2"><p>مرا که عمر چنین در ملال می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال مهر تو در چشم هر سهی سرویست</p></div>
<div class="m2"><p>که در حوالیش آب زلال می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بوی زلف توام روح تازه می‌گردد</p></div>
<div class="m2"><p>سپیده‌دم که نسیم شمال می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و وصال تو آن فکر و آرزو هیهات</p></div>
<div class="m2"><p>که بر دماغ چه فکر محال می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلام و چاکر روی چو ماه توست عبید</p></div>
<div class="m2"><p>وزین حدیث بسی ماه و سال می‌گذرد</p></div></div>