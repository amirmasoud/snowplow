---
title: >-
    غزل شمارهٔ ۸۵
---
# غزل شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>از حد گذشت درد و به درمان نمی‌رسیم</p></div>
<div class="m2"><p>بر لب رسید جان و به جانان نمی‌رسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رهروان به کعبهٔ مقصود می‌رسند</p></div>
<div class="m2"><p>ما جز به خارهای مغیلان نمی‌رسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنانکه راه عشق سپردند پیش از این</p></div>
<div class="m2"><p>شبگیر کرده‌اند به ایشان نمی‌رسیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایشان مقیم در حرم وصل مانده‌اند</p></div>
<div class="m2"><p>ما سعی می‌کنیم و به دربان نمی‌رسیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بویی ز عود می‌شنود جان ما ولی</p></div>
<div class="m2"><p>در کُنْهِ کار مجمره‌گردان نمی‌رسیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون صبح در صفا نفس صدق می‌زنیم</p></div>
<div class="m2"><p>لیکن به آفتاب درخشان نمی‌رسیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مسکنت چو پیرو سلمان نمی‌شویم</p></div>
<div class="m2"><p>در سلطنت به جاه سلیمان نمی‌رسیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچون عبید واله و حیران بمانده‌ایم</p></div>
<div class="m2"><p>در سر کارخانهٔ یزدان نمی‌رسیم</p></div></div>