---
title: >-
    غزل شمارهٔ ۵
---
# غزل شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>ای خط و خال خوشت مایهٔ سودای ما</p></div>
<div class="m2"><p>ای نفسی وصل تو اصل تمنای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چونکه قدم مینهد شوق تو در ملک جان</p></div>
<div class="m2"><p>صبر برون میجهد از دل شیدای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چتر همایون عشق سایه چو بر ما فکند</p></div>
<div class="m2"><p>راه خرابات پرس گر طلبی جای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رخ زیبای تو قبله‌گه عام را</p></div>
<div class="m2"><p>کعبهٔ دیگر نباد دلبر ترسای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم لولی وشیم ما که وسجده کدام</p></div>
<div class="m2"><p>رای هزیمت گرفت عقل سبک رای ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفی افسرده را زحمت ما گو مده</p></div>
<div class="m2"><p>رو تو و محراب زهد ما و چلیپای ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رطل گرانرا ز دست تا ننهی ای عبید</p></div>
<div class="m2"><p>زانکه روان میبرد عمر سبک پای ما</p></div></div>