---
title: >-
    غزل شمارهٔ ۶۵
---
# غزل شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>دوش سلطان خیالش باز غوغا کرده بود</p></div>
<div class="m2"><p>ملک جان تاراج و رخت صبر یغما کرده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق شوقش از دهانم شعله میزد هر زمان</p></div>
<div class="m2"><p>و آتش سودای او قصد سویدا کرده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده‌ام دریای خونست و من اندر حیرتم</p></div>
<div class="m2"><p>تا خیالش چون گذر بر راه دریا کرده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر چه میزد یار ما لاف وفاداری دل</p></div>
<div class="m2"><p>عاقبت بشکست پیمانی که با ما کرده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان ز من میخواست لعلش در بهای بوسه‌ای</p></div>
<div class="m2"><p>بی‌تکلف مختصر چیزی تمنا کرده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردها چون دیر شد نومید روی از ما بتافت</p></div>
<div class="m2"><p>هر که روزی دردمندی را مداوا کرده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر عبید از عشق دم زد پیش از این معذور دار</p></div>
<div class="m2"><p>کین گناهی نیست کان بیچاره تنها کرده بود</p></div></div>