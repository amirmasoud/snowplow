---
title: >-
    ترجیع بند
---
# ترجیع بند

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که کار دریابیم</p></div>
<div class="m2"><p>در شتاب است عمر بشتابیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدهٔ حرص و آز بر دوزیم</p></div>
<div class="m2"><p>پنجهٔ زهد و زرق برتابیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما گدایان کوی میکده‌ایم</p></div>
<div class="m2"><p>نه مقیمان کنج محرابیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه ز جور زمانه در خشمیم</p></div>
<div class="m2"><p>نز جفای سپهر در تابیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه اسیران نام و ناموسیم</p></div>
<div class="m2"><p>نه گرفتار ملک و اسبابیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندهٔ یکروان یک رنگیم</p></div>
<div class="m2"><p>دشمن شیخکان قلابیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد کوی مغان همیگردیم</p></div>
<div class="m2"><p>مترصد که فرصتی یابیم</p></div></div>
<div class="b2" id="bn8"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>
<div class="b" id="bn9"><div class="m1"><p>هر که او آه عاشقانه زند</p></div>
<div class="m2"><p>آتش از آه او زبانه زند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق شمعی از آن برافروزد</p></div>
<div class="m2"><p>شعله چون بر شرابخانه زند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می درآید به جوش و هر قطره</p></div>
<div class="m2"><p>عکس دیگر بر آستانه زند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که زان باده جرعه‌ای بچشید</p></div>
<div class="m2"><p>لاف مستی جاودانه زند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بندهٔ آن دمم که با ساقی</p></div>
<div class="m2"><p>شاهد ما دم از چمانه زند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با حریفی سه چار کز مستی</p></div>
<div class="m2"><p>این کند رقص و آن چغانه زند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیز تا پیش از آنکه مرغ سحر</p></div>
<div class="m2"><p>بال زرین بر آشیانه زند</p></div></div>
<div class="b2" id="bn16"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>
<div class="b" id="bn17"><div class="m1"><p>عقل با روح خودستائی کرد</p></div>
<div class="m2"><p>عشق با هر دو پادشائی کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از پس پرده حسن با صد ناز</p></div>
<div class="m2"><p>چهره بنمود و دلربائی کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ناگهان التفات عشق بدید</p></div>
<div class="m2"><p>غره شد دعوی خدائی کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کار دریافت رند فرزانه</p></div>
<div class="m2"><p>رفت و با عشق آشنائی کرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صوفی افزوده بود مایهٔ خویش</p></div>
<div class="m2"><p>در سر زهد و پارسائی کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هجر بر ما در طرب در بست</p></div>
<div class="m2"><p>وصلش آمد گره گشائی کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خیز تا چون ارادتش ما را</p></div>
<div class="m2"><p>سوی میخانه ره نمائی کرد</p></div></div>
<div class="b2" id="bn24"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>
<div class="b" id="bn25"><div class="m1"><p>عشق گنجیست دل چو ویرانه</p></div>
<div class="m2"><p>عشق شمعیست روح پروانه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در بیابان عشق میگردد</p></div>
<div class="m2"><p>روح مدهوش و عقل دیوانه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دست تا در نزد به دامن عشق</p></div>
<div class="m2"><p>ره به منزل نبرد فرزانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خرم آن عارفان که دنیا را</p></div>
<div class="m2"><p>پشت پائی زدند مردانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آدم از دانه اوفتاده به دام</p></div>
<div class="m2"><p>آه از این دام وای از آن دانه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عمر در باختیم تا اکنون</p></div>
<div class="m2"><p>گه به افسون و گه به افسانه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بعد از امروز گر به دست آریم</p></div>
<div class="m2"><p>دامن یار و کنج میخانه</p></div></div>
<div class="b2" id="bn32"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>
<div class="b" id="bn33"><div class="m1"><p>عقل را دانشی و رائی نیست</p></div>
<div class="m2"><p>بهتر از عشق رهنمائی نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>طلب عشق و وصل ورزیدن</p></div>
<div class="m2"><p>کار هر مفلس و گدائی نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نام جنت مبر که عاشق را</p></div>
<div class="m2"><p>خوشتر از کوی یار جائی نیست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پای در کوی زهد و زرق منه</p></div>
<div class="m2"><p>کاندر آن کوی آشنائی نیست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر در خانقه مرو که در او</p></div>
<div class="m2"><p>جز ریائی و بوریائی نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پیش ما مجلس شراب خوشست</p></div>
<div class="m2"><p>مجلس وعظ را صفائی نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>راه میخانه گیر تا شب و روز</p></div>
<div class="m2"><p>چون در اسلامیان وفائی نیست</p></div></div>
<div class="b2" id="bn40"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>
<div class="b" id="bn41"><div class="m1"><p>آه از این صوفیان ازرق پوش</p></div>
<div class="m2"><p>که ندارند عقل و دانش و هوش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>رقص را همچو نی کمر بسته</p></div>
<div class="m2"><p>لوت را همچو سفره حلقه بگوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از پی صید در پس زانو</p></div>
<div class="m2"><p>مترصد چو گربهٔ خاموش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شکر آنرا که نیستی صوفی</p></div>
<div class="m2"><p>عیش میران و باده میکن نوش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خیز تا پیش آنکه ناگاهی</p></div>
<div class="m2"><p>برکشد صبحدم خروس خروش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با صبوحی کنان درد آشام</p></div>
<div class="m2"><p>با خراباتیان عشوه فروش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رو به میخانهٔ مغان آریم</p></div>
<div class="m2"><p>باده در جام و چنگ در آغوش</p></div></div>
<div class="b2" id="bn48"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>
<div class="b" id="bn49"><div class="m1"><p>خیز جانا چمانه برداریم</p></div>
<div class="m2"><p>باده‌های مغانه برداریم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اسب شادی به زیر ران آریم</p></div>
<div class="m2"><p>و ز قدح تازیانه برداریم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیش از این غصهٔ جهان نخوریم</p></div>
<div class="m2"><p>دل ز کام زمانه برداریم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زهد و تسبیح دام و دانهٔ ماست</p></div>
<div class="m2"><p>از ره این دام و دانه برداریم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شاهد و نقل و باده برگیریم</p></div>
<div class="m2"><p>دف و چنگ و چغانه برداریم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیشتر زآنکه ناگهان روزی</p></div>
<div class="m2"><p>رخت از این آشیانه برداریم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>یک زمان چون عبید زاکانی</p></div>
<div class="m2"><p>راه خمارخانه برداریم</p></div></div>
<div class="b2" id="bn56"><p>با مغان بادهٔ مغانه خوریم</p>
<p>تا به کی غصهٔ زمانه خوریم</p></div>