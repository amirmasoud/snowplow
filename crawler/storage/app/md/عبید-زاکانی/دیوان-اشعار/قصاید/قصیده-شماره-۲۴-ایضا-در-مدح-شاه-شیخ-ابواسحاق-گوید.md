---
title: >-
    قصیدهٔ شمارهٔ ۲۴ - ایضا در مدح شاه شیخ ابواسحاق گوید
---
# قصیدهٔ شمارهٔ ۲۴ - ایضا در مدح شاه شیخ ابواسحاق گوید

<div class="b" id="bn1"><div class="m1"><p>میرسد نوروز عید و میدهد بوی بهار</p></div>
<div class="m2"><p>باد فرخ بر جناب شاه گردون اقتدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قهرمان چار عنصر پادشاه شش جهت</p></div>
<div class="m2"><p>آفتاب هفت کشور سایهٔ پروردگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیخ ابوسحاق سلطان جهان دارای دهر</p></div>
<div class="m2"><p>خسرو گیتی ستان جمشید افریدون شعار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پادشاهی کاورد زخم سنانش روز رزم</p></div>
<div class="m2"><p>دهر را در اضطراب و چرخ را در زینهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برق خشمش بیقرار و موج قهرش بی امان</p></div>
<div class="m2"><p>فیض جودش بی قیاس و بحر لطفش بی کنار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصف او بیرون ز هر معنی که آری در سخن</p></div>
<div class="m2"><p>جود او افزون ز هر صورت که آید در شمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه بر درگاه امرش مسرعی فرمان پذیر</p></div>
<div class="m2"><p>آفتاب از حسن جاهش بندهٔ خنجر گذار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشاها دیدهٔ اهل جهان روشن به تست</p></div>
<div class="m2"><p>این جهان را بزمت از کیخسرو و جم یادگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میزند خورشید از رای جهانگیر تو لاف</p></div>
<div class="m2"><p>میکند گردون به خاک آستانت افتخار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چاکرانت را ملازم بخت و دولت بر یمین</p></div>
<div class="m2"><p>بندگانت را مقارن فتح و نصرت بر یسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک میبخشی و میبودند شاهان ملک‌گیر</p></div>
<div class="m2"><p>تاج میبخشی و میبودند شاهان تاجدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اطلس نه توی این چرخ مقرنس شکل را</p></div>
<div class="m2"><p>کرده‌اند از بهر عالی بارگاهت برکنار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز رزم از بانگ رعد کوس و برق تیغ تیز</p></div>
<div class="m2"><p>کوه را در جنبش آرد بحر را در اضطرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قامت گردون شود چون قد چوگان خم پذیر</p></div>
<div class="m2"><p>کلهٔ شیرافکنان چون گوی گردان خاکسار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی صحرا گردد از زخم سم اسبان ستوه</p></div>
<div class="m2"><p>تل و هامون گردد از خون دلیران لاله‌زار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیزه برباید تن مردان جنگی را ز تن</p></div>
<div class="m2"><p>حدت پیکان کند از جوشن جانها گذار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خنجر تیز تو هامونرا کند دریای خون</p></div>
<div class="m2"><p>آتش قهر تو از دریا برانگیزد غبار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>باد عمرت بی قیاس و باد عیشت بر دوام</p></div>
<div class="m2"><p>باد بختت کامران و باد جاهت پایدار</p></div></div>