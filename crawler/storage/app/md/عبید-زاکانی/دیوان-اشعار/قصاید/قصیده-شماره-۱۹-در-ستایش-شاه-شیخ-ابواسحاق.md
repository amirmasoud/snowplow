---
title: >-
    قصیدهٔ شمارهٔ ۱۹ - در ستایش شاه شیخ ابواسحاق
---
# قصیدهٔ شمارهٔ ۱۹ - در ستایش شاه شیخ ابواسحاق

<div class="b" id="bn1"><div class="m1"><p>همیشه تا سپر مهر زرفشان باشد</p></div>
<div class="m2"><p>غلام سایهٔ چتر خدایگان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهانگشای جوانبخت شیخ ابواسحاق</p></div>
<div class="m2"><p>که پادشاه جهانست تا جهان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سزد که سر به فلک در نیاورد ز علو</p></div>
<div class="m2"><p>کسی که بندهٔ این شاه کامران باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایگانا گردون پیر میخواهد</p></div>
<div class="m2"><p>که در حمایت آن دولت جوان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمینه بنده‌ای از چاکران این درگاه</p></div>
<div class="m2"><p>هزار چون جم و دارا و اردوان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بهر سائل و زایر خجسته خامهٔ تو</p></div>
<div class="m2"><p>گره گشای در گنج شایگان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براق سیر سمند جهان بورت را</p></div>
<div class="m2"><p>ظفر ملازم و اقبال همعنان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه نبرد ز دشمن کشان به لشگرگاه</p></div>
<div class="m2"><p>کسی که پشت نماید مگر گمان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زخم گرز گران خورد کن سر اعدا</p></div>
<div class="m2"><p>چنانکه عادت شاهان خرده‌دان باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو زلف و چشم بتان هرکه فتنه انگیزد</p></div>
<div class="m2"><p>ز عدل شاه پریشان و ناتوان باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به روز رزم ببین پهلوانی خسرو</p></div>
<div class="m2"><p>که پادشاه کم افتد که پهلوان باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فدای خاک در کبریات خواهد بود</p></div>
<div class="m2"><p>عبید را نه یکی گر هزار جان باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بقای عمر تو بادا که خوشتر از همه چیز</p></div>
<div class="m2"><p>بقای سرمد و اقبال جاودان باشد</p></div></div>