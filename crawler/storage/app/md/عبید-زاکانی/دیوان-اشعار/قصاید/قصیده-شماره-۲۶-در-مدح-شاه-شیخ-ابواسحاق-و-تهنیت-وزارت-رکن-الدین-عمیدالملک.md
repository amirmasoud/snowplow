---
title: >-
    قصیدهٔ شمارهٔ ۲۶ - در مدح شاه شیخ ابواسحاق و تهنیت وزارت رکن‌الدین عمیدالملک
---
# قصیدهٔ شمارهٔ ۲۶ - در مدح شاه شیخ ابواسحاق و تهنیت وزارت رکن‌الدین عمیدالملک

<div class="b" id="bn1"><div class="m1"><p>شد ملک فارس باز به تایید کردگار</p></div>
<div class="m2"><p>خوشتر ز صحن جنت و خرمتر از بهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت فکند سایه بر اطراف این مقام</p></div>
<div class="m2"><p>اقبال کرد باز بر این مملکت گذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیمرغ ز آشیان عنایت ز اوج قدس</p></div>
<div class="m2"><p>بگشاد شاهبال سعادت بر این دیار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز آمد از نسایم و الطاف ایزدی</p></div>
<div class="m2"><p>در بوستان دهر گل خرمی به بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانهای غم‌پرست کنون گشت شادمان</p></div>
<div class="m2"><p>دلهای ناامید کنون شد امیدوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز سایهٔ عنایت سلطان تاج‌بخش</p></div>
<div class="m2"><p>شاه عدو شکار جهانگیر کامکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمشید عهد خسرو گیتی جمال دین</p></div>
<div class="m2"><p>«آن بیش ز آفرینش و کم ز آفریدگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تشریف یافت صدر وزارت به فال سعد</p></div>
<div class="m2"><p>از سایهٔ مبارک مخدوم نامدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید آسمان وزارت عمید ملک</p></div>
<div class="m2"><p>آن تا هزار جد و پدر شاه و شهریار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مشتری عطیت و ناهید خاصیت</p></div>
<div class="m2"><p>وی آسمان مهابت خورشید اقتدار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای عنصر تو زبدهٔ محصول کاینات</p></div>
<div class="m2"><p>وی ذات تو نتیجهٔ الطاف کردگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابری به گاه بخشش و کانی به گاه جود</p></div>
<div class="m2"><p>بحری به گاه کوشش و کوهی گه وقار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کلک تو مسرعیست که هردم هزار بار</p></div>
<div class="m2"><p>تا ملک چین بتازد و تا حد زنگبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این ابر را که فیض به هر کس همی رسد</p></div>
<div class="m2"><p>اسمیست او ز بحر بنان تو مستعار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه ابر را کجا و بنان تو از کجا</p></div>
<div class="m2"><p>کان قطره‌ای دو بخشد و این در شاهوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاها در این میان غزلی درج میکنم</p></div>
<div class="m2"><p>تا باشد این طریقه ز داعیت یادگار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل باز جلوه کرد بر اطراف جویبار</p></div>
<div class="m2"><p>ای ترک نازنین من ای رشگ نوبهار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خانه دور شو که کنون خانه دوزخست</p></div>
<div class="m2"><p>خرگاه ساز کن که بهشتست مرغزار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با غنچه شو مصاحب و با یاسمن نشین</p></div>
<div class="m2"><p>با ارغوان طرب کن و با لاله می گسار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گل ریز و مطربان بنشان انجمن بساز</p></div>
<div class="m2"><p>یا توق خواه شیره بنه چرغتو بیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طرف کلاه کج کن و بند کمر ببند</p></div>
<div class="m2"><p>پائی بکوب و دست بزن کاسه‌ای بدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نازان به ترکتاز فرو ریز خون می</p></div>
<div class="m2"><p>شادان ز روی عربده بشکن سر خمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر خیل دل ز طرهٔ هندو گشا کمین</p></div>
<div class="m2"><p>در ملک جان به غمزهٔ جادو فکن شکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صوفی و کنج مسجد و سالوسی نهان</p></div>
<div class="m2"><p>ما و شراب و شاهد و رندی آشکار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرگز خیال روی تو از جان نمیرود</p></div>
<div class="m2"><p>امشب نیم ز روی خیال تو شرمسار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر خستگان جفا و ستم بیش از این مکن</p></div>
<div class="m2"><p>آخر نگاه کن به جفاهای روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دامن ز صحبت من بیچاره در مکش</p></div>
<div class="m2"><p>دست عبید و دامن لطف تو زینهار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا از فلک بتابد اجرام مستنیر</p></div>
<div class="m2"><p>تا چرخ تیز گرد کند بر مدر مدار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بادا وجود قدسیت ایمن ز حادثات</p></div>
<div class="m2"><p>« ای کاینات را بوجود تو افتخار»</p></div></div>