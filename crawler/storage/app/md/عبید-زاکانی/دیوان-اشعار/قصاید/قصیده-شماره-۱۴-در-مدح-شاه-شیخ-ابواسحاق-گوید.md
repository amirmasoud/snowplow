---
title: >-
    قصیدهٔ شمارهٔ ۱۴ - در مدح شاه شیخ ابواسحاق گوید
---
# قصیدهٔ شمارهٔ ۱۴ - در مدح شاه شیخ ابواسحاق گوید

<div class="b" id="bn1"><div class="m1"><p>جهان خوشست و چمن خرمست و بلبل شاد</p></div>
<div class="m2"><p>ببار بادهٔ گلرنگ هرچه بادا باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شش جهت چو از این هفت چرخ بوقلمون</p></div>
<div class="m2"><p>از آنچه هست مقدر نه کم شود نه زیاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نای و نی نفسی وقت خویشتن خوش دار</p></div>
<div class="m2"><p>چو نای و نی چه دهی عمر خویشتن بر باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگیر دست بتی وز زمانه دست بدار</p></div>
<div class="m2"><p>غلام سرو قدی باش و از جهان آزاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین که بود زتاثیر زمهریر خراب</p></div>
<div class="m2"><p>ز یمن مقدم نوروز می‌شود آباد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شاهدان چمن صد هزار لخلخه حور</p></div>
<div class="m2"><p>به دست پیک نسیم بهار بفرستاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نقشبند ریاحین قبای غنچه ببست</p></div>
<div class="m2"><p>صبا به لطف سر نافهٔ ختن بگشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان سبزه و گل رقص میکند لاله</p></div>
<div class="m2"><p>به پیش آب روان جلوه میکند شمشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درم فشانی بر فرق سبزه‌ها کاریست</p></div>
<div class="m2"><p>که باز لطف نسیم بهار را افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رنگ و بوی چمن جنتیست پنداری</p></div>
<div class="m2"><p>که هست درگه اعلای شاه شاه نژاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهانگشای جوانبخت شیخ ابواسحاق</p></div>
<div class="m2"><p>که چرخ پیر جوانی چو او ندارد یاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمینه بندهٔ او صد چو رستم دستان</p></div>
<div class="m2"><p>کهینه چاکر او صد چو کیقباد و قباد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مهابتیست سر تیغ آبدارش را</p></div>
<div class="m2"><p>که از صلابت او آب میشود فولاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایگانا تا روز حشر لطف خدای</p></div>
<div class="m2"><p>زمام دولت و حکمت به دست حکم تو داد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو شمع هر که کند سرکشی در این حضرت</p></div>
<div class="m2"><p>عجب مدار گرش آتش اوفتد به نهاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سمند باد مسیر تو، با صبا هم تک</p></div>
<div class="m2"><p>سنان صاعقه بار تو با قدر همزاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه شیر فلک آرزوی آن دارد</p></div>
<div class="m2"><p>که با سگان درت دوستی کند بنیاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به روز معرکه صد خصم را به هم بر دوخت</p></div>
<div class="m2"><p>هر آن خدنگ که از بازوی تو یافت گشاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مراد خلق ز جود تو میشود حاصل</p></div>
<div class="m2"><p>ز روی لطف مراد دلت خدا بدهاد</p></div></div>