---
title: >-
    قصیدهٔ شمارهٔ ۲۵ - در ستایش شاه شیخ ابواسحاق
---
# قصیدهٔ شمارهٔ ۲۵ - در ستایش شاه شیخ ابواسحاق

<div class="b" id="bn1"><div class="m1"><p>گذشت روزه و سرما رسید عید و بهار</p></div>
<div class="m2"><p>کجاست ساقی ما گو بیا و باده بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صباح عید بده ساغریکه در رمضان</p></div>
<div class="m2"><p>بسوختیم ز تسبیح و زهد و استغفار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمیکه بی می و معشوق و نای میگذرد</p></div>
<div class="m2"><p>محاسب خردش در نیاورد به شمار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنیمت است غنیمت شمار و فرصت دان</p></div>
<div class="m2"><p>«توانگری و جوانی و عشق و بوی بهار»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و بزم طرب ساز کن که خوش باشد</p></div>
<div class="m2"><p>«شراب و سبزه و آب روان و روی نگار»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر قدح که دهی پر ز باده از سر صدق</p></div>
<div class="m2"><p>دعای دولت شاه جهان کنی تکرار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمال دنیی و دین شاه شیخ ابواسحاق</p></div>
<div class="m2"><p>خدایگان جهان پادشاه گیتی دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستاره جیش قضا حمله و قدر قدرت</p></div>
<div class="m2"><p>سپهر بخشش دریا نوال کوه وقار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدبری که جهان را به تیغ اوست نظام</p></div>
<div class="m2"><p>شهنشهی که فلک را ز عدل اوست مدار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صدای صیت شکوهش به کوه داد سکون</p></div>
<div class="m2"><p>شهاب عزم سریعش به باد داده قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم از مؤثر رمحش ستاره در لرزه</p></div>
<div class="m2"><p>هم از منافع کلکش جهان پر از ایثار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو رای ثابت او سایه بر فلک انداخت</p></div>
<div class="m2"><p>درست مغربی مهر شد تمام عیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرم پناه جوادیکه هست در جنبش</p></div>
<div class="m2"><p>جهان و هرچه در او هست خوار و بی‌مقدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایگانا آنی که با معالی تو</p></div>
<div class="m2"><p>خطاب چرخ بود: «لیس غیره دیار»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمینه پیک جناب تو ماه حلقه به گوش</p></div>
<div class="m2"><p>کهینه بندهٔ امرت سماک نیزه گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه تا که بود ماه و مهر و کیوان را</p></div>
<div class="m2"><p>در این حدیقهٔ زنگارگون مسیر و مدار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به کامرانی و اقبال باش تا باید</p></div>
<div class="m2"><p>ز عمر و جاه و جوانی و بخت برخوردار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عدو به دام و ولی شادکام و بخت جوان</p></div>
<div class="m2"><p>فلک مطیع و جهانت غلام و دولت یار</p></div></div>