---
title: >-
    قصیدهٔ شمارهٔ ۳ - در مدح جلال‌الدین شاه شجاع مظفری و فتح اصفهان
---
# قصیدهٔ شمارهٔ ۳ - در مدح جلال‌الدین شاه شجاع مظفری و فتح اصفهان

<div class="b" id="bn1"><div class="m1"><p>صباح عید و رخ یار و روزگار شباب</p></div>
<div class="m2"><p>خروش چنگ و لب زنده رود و جام شراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای دلبر و غوغای عشق و آتش شوق</p></div>
<div class="m2"><p>نوای بربط و آواز عود و بانک رباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوید فتح صفاهان و مژدهٔ اقبال</p></div>
<div class="m2"><p>نشان بخت بلند و امید فتح‌الباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ باده گساران ز خرمی در جوش</p></div>
<div class="m2"><p>درون مهر پرستان ز عاشقی در تاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط در دل و می در کف و طرب در جان</p></div>
<div class="m2"><p>نگار سرخوش و ما بیخود و ندیم خراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی نمونهٔ دولت زهی نشانهٔ بخت</p></div>
<div class="m2"><p>دگر چه باشد ازین بیش عیش را اسباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنیمتست غنیمت شمار فرصت عیش</p></div>
<div class="m2"><p>ز باده دست مدار و ز عیش روی متاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش خود بنشان شاهدان شیرین کار</p></div>
<div class="m2"><p>که با شکردهنان خوش بود سؤال و جواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنوش جام می‌ای جان نازنین عبید</p></div>
<div class="m2"><p>شتاب میکند این عمر نازنین دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بزم شاه جهان عیش ران و شادی کن</p></div>
<div class="m2"><p>خدایگان جهان آفتاب عالمتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جلال دولت و دین تاج‌بخش تخت نشین</p></div>
<div class="m2"><p>سپهر مهر و سخا پادشاه عرش جناب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سریر بخش ممالک سنان کشور گیر</p></div>
<div class="m2"><p>جهانگشای جوان دولت سعادت یاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به نوک نیزه برآرد ز قعر نیل نهنگ</p></div>
<div class="m2"><p>به زخم تیر در آرد ز اوج ابر عقاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شدست فتنه در ایام پادشاهی او</p></div>
<div class="m2"><p>چو چشم بخت بداندیش جاه او در خواب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان پناها بر آستان دولت تو</p></div>
<div class="m2"><p>سپهر حاجب بارست و مشتری بواب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ببسته خدمت صدر ترا صدور میان</p></div>
<div class="m2"><p>نهاده طاعت امر ترا ملوک رقاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>علو قدر تو جائیست از معارج جاه</p></div>
<div class="m2"><p>که وهم تیز قدم در نیایدش پایاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به پیش بحر سخای تو بحر جود محیط</p></div>
<div class="m2"><p>چو پیش بحر محیطست لعمه‌های سراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مثال روی تو و آفتاب چنانک</p></div>
<div class="m2"><p>حدیث نور تجلی و پرتو مهتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فلک زفر تو اندوخته شکوه و جلال</p></div>
<div class="m2"><p>خرد ز رای تو آموخته صلاح و صواب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم از مهابت خشم تو کوه در لرزه</p></div>
<div class="m2"><p>هم از خجالت دست تو بحر در غر قاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چکان ز تیغ تو خون عدوست پنداری</p></div>
<div class="m2"><p>مگر که قطرهٔ خون میچکد ز قطر سحاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدایگانا از پرتو عنایت تو</p></div>
<div class="m2"><p>که باد سایهٔ او مستدام بر احباب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر آسمان تو گشتم مقیم و دولت گفت :</p></div>
<div class="m2"><p>«نزلت خیر مقام وجدت خیر مآب»</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تا فکند دست صبح وقت سحر</p></div>
<div class="m2"><p>ز تاب شعلهٔ خورشید بر سپهر طناب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طناب عمر ترا امتداد چندان باد</p></div>
<div class="m2"><p>که حصر آن نکند فهم تا به روز حساب</p></div></div>