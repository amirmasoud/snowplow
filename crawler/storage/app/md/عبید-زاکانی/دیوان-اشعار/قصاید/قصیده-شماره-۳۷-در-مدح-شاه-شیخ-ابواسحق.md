---
title: >-
    قصیدهٔ شمارهٔ ۳۷ - در مدح شاه شیخ ابواسحق
---
# قصیدهٔ شمارهٔ ۳۷ - در مدح شاه شیخ ابواسحق

<div class="b" id="bn1"><div class="m1"><p>ای دوش چرخ غاشیه گردان جاه تو</p></div>
<div class="m2"><p>خورشید در حمایت پر کلاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه جهان سکندر ثانی جمال دین</p></div>
<div class="m2"><p>ای برتر از شهان جهان دستگاه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشم دشمنان شود از بیم او سفید</p></div>
<div class="m2"><p>سر بر فراخت پرچم گیسو سیاه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دعوی سعادت دنیا و آخرت</p></div>
<div class="m2"><p>نزدیک عقل داد و کرم بس گواه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در معرضی که جیش تو بر خصم چیره شد</p></div>
<div class="m2"><p>خورشید تیره گشت ز گرد سپاه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو جان عالمی و علی سهل جان جان</p></div>
<div class="m2"><p>تو در پناه خالق و او در پناه تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا در پی همند شب و روز و ماه و سال</p></div>
<div class="m2"><p>بادا خجسته روز و شب و سال و ماه تو</p></div></div>