---
title: >-
    قصیدهٔ شمارهٔ ۱۸ - در مدح خواجه رکن‌الدین عمیدالملک
---
# قصیدهٔ شمارهٔ ۱۸ - در مدح خواجه رکن‌الدین عمیدالملک

<div class="b" id="bn1"><div class="m1"><p>باز گل جلوه‌کنان روی به صحرا دارد</p></div>
<div class="m2"><p>نوجوان است سر عیش و تماشا دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خار در پهلو و پا در گل و خوش میخندد</p></div>
<div class="m2"><p>لطف بین کین گل نورستهٔ رعنا دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب هر لحظه چو داود زره میسازد</p></div>
<div class="m2"><p>باد خاصیت انفاس مسیحا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله بر طرف چمن رقص کنان پنداری</p></div>
<div class="m2"><p>نو عروسیست که پیراهن والا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قصهٔ سرو دراز است نمیشاید گفت</p></div>
<div class="m2"><p>کان حدیثیست که آن سر به ثریا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینچنین زار که بلبل به چمن می‌نالد</p></div>
<div class="m2"><p>نسبتی با من دلدادهٔ شیدا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوستان را همه اسباب مهیاست ولی</p></div>
<div class="m2"><p>خرم آن کو همه اسباب مهیا دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد امروز غنیمت شمر از دست مده</p></div>
<div class="m2"><p>کور بختست که اندیشهٔ فردا دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بت من جلوه‌کنان گر به چمن درگذرد</p></div>
<div class="m2"><p>با رخش سوی گل و لاله که پروا دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن چه حسن است که آن شکل و شمایل را هست</p></div>
<div class="m2"><p>وان چه لطفست که آن قامت و بالا دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتمش زلف تو دارد دل من از سرطنز</p></div>
<div class="m2"><p>گفت کین بی سر و پا بین که چه سودا دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قطرهٔ اشگ من خسته جگر در غم او</p></div>
<div class="m2"><p>هست خونی که تعلق به سویدا دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عالمی بندهٔ اوگشته واو از سر صدق</p></div>
<div class="m2"><p>هوس بندگی صاحب دانا دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رکن دین خواجهٔ مه چاکر خورشید غلام</p></div>
<div class="m2"><p>که دل و مرتبهٔ حاتم و دارا دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در جهان همسر و همتاش نه بودست و نه هست</p></div>
<div class="m2"><p>به خدائی که نه انباز و نه همتا دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دشمن از برق سنانش بگدازد ور خود</p></div>
<div class="m2"><p>تن ز پولاد و دل از صخرهٔ صما دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صاحبا شاهد شد سرمهٔ چشم افلاک</p></div>
<div class="m2"><p>خاک پای تو که در دیدهٔ ما جا دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خرد پیر ترا دولت برنا یار است</p></div>
<div class="m2"><p>خنک این پیر که آن دولت برنا دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دست دریاش گهر بخش تو هنگام عطا</p></div>
<div class="m2"><p>همچو ابریست که خاصیت دریا دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیش رای تو کجا لاف ضیا باید زد</p></div>
<div class="m2"><p>کیست خورشید که این زهره و یارا دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حلقهٔ چاکری تست که دارد مه نو</p></div>
<div class="m2"><p>کمر بندگی تست که جوزا دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راستی خواجه در این عهد ترا شاید گفت</p></div>
<div class="m2"><p>که زجودت همه کس عیش مهنا دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گه گهی تربیتی از سر اشفاق و کرم</p></div>
<div class="m2"><p>بنده از خدمت مخدوم تمنی دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>می‌نواز از سر انعام دعاگویان را</p></div>
<div class="m2"><p>که دعاهای به اخلاص اثرها دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا ابد در دو جهان نام نکو کسب کند</p></div>
<div class="m2"><p>هر مربی که چو من بنده مربی دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دایما کامروا باش و به شادی گذران</p></div>
<div class="m2"><p>که جهانی به جناب تو تولی دارد</p></div></div>