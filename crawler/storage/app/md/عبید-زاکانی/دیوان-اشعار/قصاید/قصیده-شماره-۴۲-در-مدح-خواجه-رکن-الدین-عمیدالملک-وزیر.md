---
title: >-
    قصیدهٔ شمارهٔ ۴۲ - در مدح خواجه رکن‌الدین عمیدالملک وزیر
---
# قصیدهٔ شمارهٔ ۴۲ - در مدح خواجه رکن‌الدین عمیدالملک وزیر

<div class="b" id="bn1"><div class="m1"><p>در این مقام فرح‌بخش و جای روحفزای</p></div>
<div class="m2"><p>بخواه باده و بر دل در طرب بگشای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عیش کوش و حیات دو روزه فرصت دان</p></div>
<div class="m2"><p>چو برق میگذرد عمر، کاهلی منمای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دستگیری ساغر خلاص شاید یافت</p></div>
<div class="m2"><p>ز جور وهم زمین گرد آسمان پیمای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پایمردی گلگونه میتوان رستن</p></div>
<div class="m2"><p>ز دست حادثهٔ روزگار محنت زای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طریق زهد نه راهست گرد هرزه مگرد</p></div>
<div class="m2"><p>حدیث توبه نه کار است زان سخن بازآی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب خوار، به بزم خدایگان جهان</p></div>
<div class="m2"><p>به کام عیش و طرب راه عمر میپیمای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان فیض و کرم رکن دین عمیدالمللک</p></div>
<div class="m2"><p>که باد تا به ابد در پناه لطف خدای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فروغ رایش چون آفتاب عالمگیر</p></div>
<div class="m2"><p>اساس جاهش مانند قطب پا بر جای</p></div></div>