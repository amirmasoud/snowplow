---
title: >-
    قصیدهٔ شمارهٔ ۱۶ - ایضا در مدح همو
---
# قصیدهٔ شمارهٔ ۱۶ - ایضا در مدح همو

<div class="b" id="bn1"><div class="m1"><p>خوش آن نسیم که بوئی ز زلف یار آرد</p></div>
<div class="m2"><p>به عاشقی خبر یار غمگسار آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سوی بلبل بیدل برد بشارت گل</p></div>
<div class="m2"><p>به باغ مژدهٔ ایام نوبهار آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا کسی که سلامی بدان دیار برد</p></div>
<div class="m2"><p>وز آن دیار پیامی بدین دیار آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نه پیک نسیم بهار رنجه شود</p></div>
<div class="m2"><p>عنایتی به سر عاشقان زار آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که حال من به سر کوی یار عرضه کند</p></div>
<div class="m2"><p>که یادش از من مهجور دلفکار آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اختیار نکردم جدائی از بر یار</p></div>
<div class="m2"><p>بلا که بر سر خاطر به اختیار آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غریب شهر کسانم که در شمار آیم</p></div>
<div class="m2"><p>غریب بی سر و پا را که در شمار آرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عبید را به از آن نیست در چنین سختی</p></div>
<div class="m2"><p>که روی عجز به درگاه کردگار آرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر که بخت بلندش ز خواب برخیزد</p></div>
<div class="m2"><p>تهوری کند و دولتی به کار آرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که آن غریب پریشان خسته کشتی عمر</p></div>
<div class="m2"><p>ز موج لجهٔ ایام برکنار آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو بخت دولت اقبال و فتح و نصرت روی</p></div>
<div class="m2"><p>به سوی بارگه شاه کامکار آرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جمال دنیی ودین خسرویکه روز نبرد</p></div>
<div class="m2"><p>به زخم تیر فلک را به زینهار آرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز ترس کوه بلرزد کمر بیندازد</p></div>
<div class="m2"><p>سموم قهرش اگر رو به کوهسار آرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گاه لطف دم خلق عنبر افشانش</p></div>
<div class="m2"><p>شکست در نفس آهوی تتار آرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جهان پناها آنی که گرد موکب تو</p></div>
<div class="m2"><p>برای چرخ نهم تاج افتخار آرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همای چتر تو چون سایه بر جهان افکند</p></div>
<div class="m2"><p>قضا ز فتح و ظفر بر سرش نثار آرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر آرزو که ز بخت امتحان کنی در حال</p></div>
<div class="m2"><p>به پیش رفت تو بی‌دفع و انتظار آرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز جور چرخ جفا پیشه در امان باشد</p></div>
<div class="m2"><p>عنایت تو کسی را که در حصار آرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حسود جاه ترا تخت و تاج باید لیک</p></div>
<div class="m2"><p>زمانه از پی او ریسمان و دار آرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عدو نشاند نهالی و بهر کشتن او</p></div>
<div class="m2"><p>کمان ونیزه و شمشیر و تیربار آرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خجسته کلک تو دایم ز بحر جود و کرم</p></div>
<div class="m2"><p>برای گوش امل در شاهوار آرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همه ثنای تو گویند هر زمان کامروز</p></div>
<div class="m2"><p>متاع شعر به بازار روزگار آرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دعا به پیش تو آرم که هرکسی تحفه</p></div>
<div class="m2"><p>به قدر طاقت و امکان و اقتدار آرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو پایدار بمان تا ابد که بخت ترا</p></div>
<div class="m2"><p>زمانه مژدهٔ اقبال پایدار آرد</p></div></div>