---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای مقصد خورشید پرستان رویت</p></div>
<div class="m2"><p>محراب جهانیان خم ابرویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرمایهٔ عیش تنگدستان دهنت</p></div>
<div class="m2"><p>سر رشتهٔ دلهای پریشان مویت</p></div></div>