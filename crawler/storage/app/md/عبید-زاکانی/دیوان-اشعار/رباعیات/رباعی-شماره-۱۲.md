---
title: >-
    رباعی شمارهٔ ۱۲
---
# رباعی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>هر چند بهشت صد کرامت دارد</p></div>
<div class="m2"><p>مرغ و می و حور سرو قامت دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی بده این بادهٔ گلرنگ به نقد</p></div>
<div class="m2"><p>کان نسیهٔ او سر به قیامت دارد</p></div></div>