---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>هرکس که سر زلف تو آورد بدست</p></div>
<div class="m2"><p>از غالیه فارغ شد و از مشگ برست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاقل نکند نسبت زلفت با مشگ</p></div>
<div class="m2"><p>داند که میان این و آن فرقی هست</p></div></div>