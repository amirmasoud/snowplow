---
title: >-
    رباعی شمارهٔ ۴۰
---
# رباعی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>بیم است که در بیخودی افسانه شوم</p></div>
<div class="m2"><p>وانگشت نمای خویش و بیگانه شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عقل فضول میدهد زحمت من</p></div>
<div class="m2"><p>ناگاه ز دست عقل دیوانه شوم</p></div></div>