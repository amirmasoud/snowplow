---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>گفتم عقلم گفت که حیران منست</p></div>
<div class="m2"><p>گفتم جانم گفت که قربان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که دلم گفت که آن دیوانه</p></div>
<div class="m2"><p>در سلسلهٔ زلف پریشان منست</p></div></div>