---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>از شوق توام هست بر آتش خاطر</p></div>
<div class="m2"><p>بی‌وصل توام نمیشود خوش خاطر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حسرت ابرو و سر زلف خوشت</p></div>
<div class="m2"><p>پیوسته نشسته‌ام مشوش خاطر</p></div></div>