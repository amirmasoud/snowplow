---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>از شدت دست تنگی و محنت برد</p></div>
<div class="m2"><p>در خیمه ما نه خواب یابی و نه خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تابه و صحن و کاسه و کوزهٔ ما</p></div>
<div class="m2"><p>نه چرب و نه شیرین و نه گرم است و نه سرد</p></div></div>