---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دل با رخ دلبری صفائی دارد</p></div>
<div class="m2"><p>کو هر نفسی میل به جائی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرح شب هجران و پریشانی ما</p></div>
<div class="m2"><p>چون زلف بتان دراز نائی دارد</p></div></div>