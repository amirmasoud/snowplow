---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>تا ساخته شخص من و پرداخته‌اند</p></div>
<div class="m2"><p>در زیر لگد کوب غم انداخته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی من زرد روی دلسوخته را</p></div>
<div class="m2"><p>چون شمع برای سوختن ساخته‌اند</p></div></div>