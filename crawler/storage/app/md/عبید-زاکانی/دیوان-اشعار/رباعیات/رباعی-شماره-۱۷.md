---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>درویش که می خورد به میری برسد</p></div>
<div class="m2"><p>ور روبهکی خورد به شیری برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر پیر خورد جوانی از سر گیرد</p></div>
<div class="m2"><p>ور زانکه جوان خورد به پیری برسد</p></div></div>