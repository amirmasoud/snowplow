---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای لعل لبت به دلنوازی مشهور</p></div>
<div class="m2"><p>وی روی خوشت به ترکتازی مشهور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با زلف تو قصه‌ایست ما را مشکل</p></div>
<div class="m2"><p>همچون شب یلدا به درازی مشهور</p></div></div>