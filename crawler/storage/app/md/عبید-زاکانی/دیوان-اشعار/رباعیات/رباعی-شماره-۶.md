---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>دنیا نه مقام ماست نه جای نشست</p></div>
<div class="m2"><p>فرزانه در او خراب اولیتر و مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش غم ز باده آبی میزن</p></div>
<div class="m2"><p>زان پیش که در خاک روی باد بدست</p></div></div>