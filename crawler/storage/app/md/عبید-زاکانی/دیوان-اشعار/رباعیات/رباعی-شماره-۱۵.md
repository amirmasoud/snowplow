---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>فکری که بر آن طبع روان می‌گذرد</p></div>
<div class="m2"><p>شرحش ز معانی و بیان می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعر تو چرا نازک و شیرین نبود</p></div>
<div class="m2"><p>آخر نه بدان لب و دهان می‌گذرد</p></div></div>