---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تا مهر توام در دل شوریده نشست</p></div>
<div class="m2"><p>وافتاد مرا چشم بدان نرگس مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این غم ز دلم نمی‌نهد پای برون</p></div>
<div class="m2"><p>وین اشگ ز دامنم نمیدارد دست</p></div></div>