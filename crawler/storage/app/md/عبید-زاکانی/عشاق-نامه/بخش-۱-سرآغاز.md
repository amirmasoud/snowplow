---
title: >-
    بخش ۱ - سرآغاز
---
# بخش ۱ - سرآغاز

<div class="b" id="bn1"><div class="m1"><p>خدایا تا از این فیروزه ایوان</p></div>
<div class="m2"><p>فروزد ماه و مهر و تیر و کیوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه خاور جهان آرای باشد</p></div>
<div class="m2"><p>زمان باقی زمین بر جای باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر این نیلوفری کاخ کیانی</p></div>
<div class="m2"><p>کند خورشید تابان قهرمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانرا چار عنصر مایه باشد</p></div>
<div class="m2"><p>مکانرا از جهت شش پایه باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جوهر تا عرض راهست تاری</p></div>
<div class="m2"><p>هیولا تا کند صورت نگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه تا فراز فرش غبرا</p></div>
<div class="m2"><p>معلق باشد این نه سقف مینا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان محکوم سلطان جهان باد</p></div>
<div class="m2"><p>فلک مامور شاه کامران باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخستین دم که خاطر خامه دربست</p></div>
<div class="m2"><p>بر این دیبای ششتر نقش بربست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو استاد طبیعت داد سازش</p></div>
<div class="m2"><p>نوشتم نام خسرو بر طرازش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهنشاه جهان دارای عالم</p></div>
<div class="m2"><p>چراغ دودمان نسل آدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همایون گوهر دریای شاهی</p></div>
<div class="m2"><p>وجودش آیت لطف الهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضمیرش نقطهٔ پرگار معنی</p></div>
<div class="m2"><p>درونش مهبط انوار معنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جم ثانی جمال دنیی و دین</p></div>
<div class="m2"><p>ابواسحاق سلطان السلاطین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خجسته پادشاه دادگستر</p></div>
<div class="m2"><p>جهانگیر آفتاب هفت کشور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غلام بارگاهش تاجداران</p></div>
<div class="m2"><p>جنابش سجده‌گاه شهریاران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زخیلش هر سوی صاحب کلاهی</p></div>
<div class="m2"><p>سپاهش هریکی میری و شاهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بروز بزم چون برگاه جمشید</p></div>
<div class="m2"><p>بگاه رزم چون تابنده خورشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سریرش پایه بر گردون کشیده</p></div>
<div class="m2"><p>قدم بر جای افریدون کشیده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرافکنده برش هر سر فرازی</p></div>
<div class="m2"><p>ز باغش هر تذوری شاهبازی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدو بادا فلک را سربلندی</p></div>
<div class="m2"><p>مبادا دشمنش را زورمندی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در او قبلهٔ اقبال بادا</p></div>
<div class="m2"><p>حریمش کعبهٔ آمال بادا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرم اقبال روزی یار گردد</p></div>
<div class="m2"><p>غنوده بخت من بیدار گردد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر آن درگاه خواهم داد از این دل</p></div>
<div class="m2"><p>مسلمانان مرا فریاد از این دل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلی دارم دل از جان برگرفته</p></div>
<div class="m2"><p>امید از کفر و ایمان برگرفته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دل ریشی غم اندوزی بلائی</p></div>
<div class="m2"><p>به دام عشق خوبان مبتلائی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلی شوریده شکلی بیقراری</p></div>
<div class="m2"><p>دلی دیوانه‌ای آشفته کاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلی دارم غم دوری کشیده</p></div>
<div class="m2"><p>ز چشم یار رنجوری کشیده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلی کو از خدا شرمی ندارد</p></div>
<div class="m2"><p>ز روی خلق آزرمی ندارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مشقت خانهٔ عشق آشیانی</p></div>
<div class="m2"><p>محلت دیدهٔ بی دودمانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخون آغشته ای سودا مزاجی</p></div>
<div class="m2"><p>کهن بیمار عشق بی علاجی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو چشم شاهدان پیوسته مستی</p></div>
<div class="m2"><p>مغی کافر نهادی بت پرستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو زلف کافران آشفته کاری</p></div>
<div class="m2"><p>سیه روئی پریشان روزگاری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همیشه بر بلای عشق مفتون</p></div>
<div class="m2"><p>سراپای وجودش قطرهٔ خون</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نباشد در پی مالی و جاهی</p></div>
<div class="m2"><p>نباشد هرگزش روئی به راهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز غم هردم به صد دستان برآید</p></div>
<div class="m2"><p>ز بهر خط و خالش جان برآید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز شیدائی و خود رائی نترسد</p></div>
<div class="m2"><p>چو نادانان ز رسوائی نترسد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شود حیران هر شوخی و شنگی</p></div>
<div class="m2"><p>نباشد هرگزش نامی و ننگی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هرانکو داردش چون دیده در تاب</p></div>
<div class="m2"><p>نهانش را به خون دل دهد آب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درون خویش دائم ریش خواهد</p></div>
<div class="m2"><p>بلا چندانکه بیند بیش خواهد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه سوگواری پیشه دارد</p></div>
<div class="m2"><p>همیشه عاشقی اندیشه دارد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز دور ار سرو بالائی ببیند</p></div>
<div class="m2"><p>به پایش در فتد دردش بچیند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو دست نار پستانی بگیرد</p></div>
<div class="m2"><p>به پیش نار بستانش بمیرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز بهر خوبرویان جان ببازد</p></div>
<div class="m2"><p>به کفر زلفشان ایمان ببازد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو گوئی عادت پروانه دارد</p></div>
<div class="m2"><p>به جان خویشتن پروا ندارد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>من از افکار او پیوسته افگار</p></div>
<div class="m2"><p>من از تیمار او پیوسته بیمار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به نور چشم بیند هر کسی راه</p></div>
<div class="m2"><p>دل مسکین ز چشم افتاده در چاه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا دل کشت فریاد از که خواهم</p></div>
<div class="m2"><p>اسیر دل شدم داد از که خواهم؟</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز دست این دل دیوانه مستم</p></div>
<div class="m2"><p>درون سینه دشمن میپرستم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ندیده دانه‌ای از وصف دلدار</p></div>
<div class="m2"><p>به دام دل گرفتارم گرفتار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بدینسان خسته کسرا دل مبادا</p></div>
<div class="m2"><p>کسی را کار دل مشکل مبادا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز دست دل شدم با غصه دمساز</p></div>
<div class="m2"><p>خدایا این دلم را چاره‌ای ساز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا دل در غم دلداری افکند</p></div>
<div class="m2"><p>به دام عشق گل رخساری افکند</p></div></div>