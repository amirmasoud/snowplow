---
title: >-
    بخش ۸ - پیغام فرستادن عاشق به معشوق
---
# بخش ۸ - پیغام فرستادن عاشق به معشوق

<div class="b" id="bn1"><div class="m1"><p>پس از عمری که دل خونابه میخورد</p></div>
<div class="m2"><p>خرد بیرون شد و دل کار میکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بر دل شد ز غم راه نفس تنگ</p></div>
<div class="m2"><p>به صد افسون و صد دستان و نیرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقابی تیز پر را رام کردم</p></div>
<div class="m2"><p>به سوی آن صنم پیغام کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای هم جان و هم جانانهٔ دل</p></div>
<div class="m2"><p>غمت سلطان خلوت خانهٔ دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمالت چشم جان را چشمهٔ نور</p></div>
<div class="m2"><p>ز رخسار تو بادا چشم بد دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم آن بیدلی کز بیقراری</p></div>
<div class="m2"><p>کنم بر درگهت فریاد و زاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلاف رای تو رایی ندارم</p></div>
<div class="m2"><p>بغیر از کوی تو جایی ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم دائم تمنای تو ورزد</p></div>
<div class="m2"><p>درونم مهر و سودای تو ورزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا جادوی چشمت برده از راه</p></div>
<div class="m2"><p>زنخدان توام افکنده در چاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسیر زلف مشگین تو گشتم</p></div>
<div class="m2"><p>ترحم کن چو مسکین تو گشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم پر جوش و تن پرتاب تا کی</p></div>
<div class="m2"><p>ز حسرت دیده پر خوناب تا کی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین مدهوش و رسوا چند گردم</p></div>
<div class="m2"><p>چو گردون بی سر و پا چند گردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر این مجروح سرگردان ببخشای</p></div>
<div class="m2"><p>بر این محزون بی‌سامان ببخشای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو زلف خویش بی‌سامانیم بین</p></div>
<div class="m2"><p>پریشانی و سرگردانیم بین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جز از الطاف تو غمخواریم نیست</p></div>
<div class="m2"><p>ز چشمت بهره جز بیماریم نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمانی گر ز روی آشنائی</p></div>
<div class="m2"><p>دهد شمع جمالت روشنائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شوم پروانه در پای تو میرم</p></div>
<div class="m2"><p>به پیش قد و بالای تو میرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا از آفتابت ذره‌ای بس</p></div>
<div class="m2"><p>وز آن باغ ارم گل تره‌ای بس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگویم یک زمان پیشت نشینم</p></div>
<div class="m2"><p>شوم خرسند کز دورت ببینم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو احوالم سراسر عرضه داری</p></div>
<div class="m2"><p>یکایک قصهٔ من برشماری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز اشعار همام این نظم دلسوز</p></div>
<div class="m2"><p>ادا کن پیش آن ماه دلفروز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو اینجا هست این ابیات در کار</p></div>
<div class="m2"><p>ز استادان نباشد عاریت عار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگو میگوید آن بیخواب و آرام</p></div>
<div class="m2"><p>از آن ساعت که ناگاه از سر بام</p></div></div>