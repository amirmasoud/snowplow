---
title: >-
    بخش ۲۵ - در صفت حال
---
# بخش ۲۵ - در صفت حال

<div class="b" id="bn1"><div class="m1"><p>دلا تا چند از این صورت پرستی</p></div>
<div class="m2"><p>قدم بر فرق هستی زن که رستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم هر بوده و نابوده تا چند</p></div>
<div class="m2"><p>حکایت گفتن بیهوده تا چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو رندان خیز و چابک دستیی کن</p></div>
<div class="m2"><p>ز جام نیستی سر مستیی کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رها کن عقل و رو دیوانه میگرد</p></div>
<div class="m2"><p>چو مستان بر در میخانه میگرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که از میخانه یابی روشنائی</p></div>
<div class="m2"><p>کنی با پاکبازان آشنائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم از غم زن اگر شادیت باید</p></div>
<div class="m2"><p>خرابی جو گر آبادیت باید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مزن چون نار در خون جگر جوش</p></div>
<div class="m2"><p>بهی خواهی چو به پشمینه میپوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگر خواهی ز محنت رستگاری</p></div>
<div class="m2"><p>بکمتر زان قناعت کن که داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سریر سلطنت بی داوری نیست</p></div>
<div class="m2"><p>غم صاحب کلاهی سرسری نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو چشم هوس را میل درکش</p></div>
<div class="m2"><p>پس آنگه خرقه را در نیل درکش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طمع گستاخ شد بانگی بر او زن</p></div>
<div class="m2"><p>هوس را نیز سنگی در سبو زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن ترسم که چون میبایدت مرد</p></div>
<div class="m2"><p>تو آری گرد و دیگر کس کند خورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر روحت ز آلایش سلیم است</p></div>
<div class="m2"><p>رسیدن در صراط المستقیم است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر در چاه نفس افتی به خواری</p></div>
<div class="m2"><p>تو معذوری که بینائی نداری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در این منزل که هم راهست و هم چاه</p></div>
<div class="m2"><p>علایق هر یکی غولی است بگریز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو مردان بارهٔ دولت برانگیز</p></div>
<div class="m2"><p>به افسون خواندن از این غول بگریز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو طاووس سرابستان جانی</p></div>
<div class="m2"><p>چو باز آشیان لامکانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از این بیغولهٔ غولان چه خواهی</p></div>
<div class="m2"><p>نه جغدی خانه در ویران چه خواهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در این کشتی که نامش زندگانیست</p></div>
<div class="m2"><p>نفس را پیشه در وی بادبانیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشاید خفت فارغ در شکر خواب</p></div>
<div class="m2"><p>فتاده کشتی از ساحل به گرداب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در این گرداب نتوان آرمیدن</p></div>
<div class="m2"><p>بباید رخت بر هامون کشیدن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از این دریا مشو یک لحظه ایمن</p></div>
<div class="m2"><p>منت خود این همی گویم ولیکن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدین ملاحی و این ناخدائی</p></div>
<div class="m2"><p>از این گرداب کی خواهی رهائی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به بادی بشکند بازار دنیا</p></div>
<div class="m2"><p>به کاری می‌نیاید کار دنیا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه جای تست زین دل گوشه بردار</p></div>
<div class="m2"><p>رهت پیشست رو ره توشه بردار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ترا جای دگر آرامگاهی است</p></div>
<div class="m2"><p>وز این سازنده‌تر آب و گیاهی است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در آنجا بینوایانرا بود کار</p></div>
<div class="m2"><p>در آن کشور گدایانرا بود کار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در او درمان فروشان درد خواهند</p></div>
<div class="m2"><p>تنی باریک و روئی زرد خواهند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ندارد سرکشی آنجا روائی</p></div>
<div class="m2"><p>به کاری ناید آنجا پادشائی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر این عرصه مشو کژرو چو فرزین</p></div>
<div class="m2"><p>دغا باز است گردون مهره برچین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ادای بد مکن با قول کج بار</p></div>
<div class="m2"><p>که آرد بدادائی مفلسی بار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اگر خوش عیشی و گر مستمندی</p></div>
<div class="m2"><p>در این ده روزه کاینجا پای بندی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو عنقا گوشهٔ عزلت نگهدار</p></div>
<div class="m2"><p>مرو بر سفرهٔ مردم مگس وار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تردد در میان خلق کم کن</p></div>
<div class="m2"><p>چو مردان روی بر دیوار غم کن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نمی‌بینی کمان چون گوشه گیر است</p></div>
<div class="m2"><p>بر او آوازهٔ زه ناگزیر است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مجرد باش و بر ریش جهان خند</p></div>
<div class="m2"><p>ز مردم بگسل و بر مردمان خند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مکن زن هر زمان جنگی میندوز</p></div>
<div class="m2"><p>ز بهر شهوتی ننگی میندوز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که از بی‌غیرتی به پارسائی</p></div>
<div class="m2"><p>بدیوثی نیرزد کدخدائی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>علائق بر سر خاکت نشاند</p></div>
<div class="m2"><p>مجرد شو که تجریدت رهاند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>غنیمت مرد را بی‌آب و رنگی است</p></div>
<div class="m2"><p>خوشی در عالم بی‌نام و ننگی است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خراب آباد دنیا غم نیرزد</p></div>
<div class="m2"><p>همه سورش بیک ماتم نیرزد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در این صحرای بی‌پایان چه پوئی</p></div>
<div class="m2"><p>غنیمت زین ره ویران چه جوئی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از این منزل که ما در پیش داریم</p></div>
<div class="m2"><p>دلی خسته روانی ریش داریم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیابانی است کو سامان ندارد</p></div>
<div class="m2"><p>رهی دارد که آن پایان ندارد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بدین ره رفتنت کاری است مشکل</p></div>
<div class="m2"><p>نه مقصودت نه مقصد هست حاصل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در این ویرانه گر صد گنج داری</p></div>
<div class="m2"><p>وزین کاشانه گر صد رنج داری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرت کیخسرو جمشید نامست</p></div>
<div class="m2"><p>ورت خلق جهان یکسر غلامست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به وقت کوچ همراهی نیابی</p></div>
<div class="m2"><p>ز کوهی پرهٔ کاهی نیابی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چه خوش میگوید این معنی نظامی</p></div>
<div class="m2"><p>به رغبت بشنو ای جان گرامی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>« که مال و ملک و فرزند و زن و زور</p></div>
<div class="m2"><p>همه هستند با تو تا لب گور »</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>» روند این همرهان چالاک با تو</p></div>
<div class="m2"><p>نیاید هیچکس در خاک با تو »</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>کجا آن کو از این ماتم نگرید</p></div>
<div class="m2"><p>کدامین سنگدل زین غم نگرید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در این بستان گل و نرگس که بوئی</p></div>
<div class="m2"><p>همان سرو و همان سنبل که جوئی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دلم میگردد از گفتن پریشان</p></div>
<div class="m2"><p>ولی چون بنگری هریک از ایشان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رخ خوبی و چشم دلستانیست</p></div>
<div class="m2"><p>قد شوخی و زلف نوجوانیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>از این منزل هرآنکو بر نشیند</p></div>
<div class="m2"><p>کسش دیگر در این منزل نبیند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به وقت خود چو مردان کار دریاب</p></div>
<div class="m2"><p>مشو غافل که این گردنده دولاب</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ندارد کار جز نیرنگ سازی</p></div>
<div class="m2"><p>فغان زین حقه و زین حقه بازی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>یکی از مؤبدی پرسید در راز</p></div>
<div class="m2"><p>ز جور چرخ و از انجام و آغاز</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جوابش داد از احوال این دیر</p></div>
<div class="m2"><p>که دایم میکند گرد زمین سیر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>حقیقت کس نشانی باز ندهد</p></div>
<div class="m2"><p>کسی نیز از فلک آواز ندهد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>اگر چه سست مهری زود سیر است</p></div>
<div class="m2"><p>چنین در دور تا دیده است دیر است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در این پرده خرد را نیست راهی</p></div>
<div class="m2"><p>ندارد دانش آنجا دستگاهی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بدین چشمه که نورت میفزاید</p></div>
<div class="m2"><p>بدین ایوان که دورت مینماید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>به پای جسم چون شاید رسیدن</p></div>
<div class="m2"><p>به بال روح می‌باید پریدن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طلسمی این چنین از دور دیدن</p></div>
<div class="m2"><p>کجا شاید در احکامش رسیدن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از او جز دور سامانی نبینی</p></div>
<div class="m2"><p>تر آن به که خاموشی گزینی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>نصیحت گر ز مؤبد گوش داریم</p></div>
<div class="m2"><p>همان بهتر که لب خاموش داریم</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بجز توفیق یاری نیست اینجا</p></div>
<div class="m2"><p>بجز تسلیم کاری نیست اینجا</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>جهانرا بی‌ثباتی رسم و دین است</p></div>
<div class="m2"><p>همیشه عادت دنیا چنین است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>کسی آغاز و انجامش نداند</p></div>
<div class="m2"><p>همان بهتر که کس نامش نداند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خود این احوال ما گر گوش داری</p></div>
<div class="m2"><p>نبینی روی کس گر هوش داری</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>نیازی عشق و دل در کس نبندی</p></div>
<div class="m2"><p>دگر چون ابلهان بر خود نخندی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>عبید از کار دنیا دل بپرداز</p></div>
<div class="m2"><p>دگر ره بر سر افسانه شو باز</p></div></div>