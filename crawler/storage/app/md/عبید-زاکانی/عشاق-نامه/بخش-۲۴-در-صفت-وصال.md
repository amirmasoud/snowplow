---
title: >-
    بخش ۲۴ - در صفت وصال
---
# بخش ۲۴ - در صفت وصال

<div class="b" id="bn1"><div class="m1"><p>چنین زیبا نگاری دلستانی</p></div>
<div class="m2"><p>به رعنائی و خوبی داستانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان بر عاشق خود مهربان بود</p></div>
<div class="m2"><p>که گوئی عاشق جان و جهان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبودی با منش جز مهربانی</p></div>
<div class="m2"><p>ندیدیم جز از او شیرین زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدامم خرمی دمساز بودی</p></div>
<div class="m2"><p>به رویش چشم جانم باز بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دل گفتم که ای مدهوش بیمار</p></div>
<div class="m2"><p>غمش را در میان جان نگهدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کزین خوشتر کسی دلبر نیابد</p></div>
<div class="m2"><p>به خوبی کس از این بهتر نیابد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهم خوش بود ما را روزگاری</p></div>
<div class="m2"><p>به وصلش داشتم خوش کار و باری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سعادت یار و بختم همنشین بود</p></div>
<div class="m2"><p>زمان در حکم و اقبالم قرین بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز طالع خرم و دلشاد بودم</p></div>
<div class="m2"><p>ز بند هر غمی آزاد بودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان محکوم و دولت یاورم بود</p></div>
<div class="m2"><p>فلک مامور و اختر چاکرم بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون زان عیش جز خون در دلم نیست</p></div>
<div class="m2"><p>در آن شادی به جز غم حاصلم نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تنی خسته دلی غمناک دارم</p></div>
<div class="m2"><p>به دستی باد و دستی خاک دارم</p></div></div>