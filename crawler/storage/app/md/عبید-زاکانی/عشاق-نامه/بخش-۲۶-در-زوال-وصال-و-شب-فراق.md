---
title: >-
    بخش ۲۶ - در زوال وصال و شب فراق
---
# بخش ۲۶ - در زوال وصال و شب فراق

<div class="b" id="bn1"><div class="m1"><p>من اندر عیش و بختم در کمین بود</p></div>
<div class="m2"><p>چه شاید کرد چون طالع چنین بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زناگه بخت وارون بر سرم تاخت</p></div>
<div class="m2"><p>از آن خوش زندگانی دورم انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز هر سو دشمنانم را خبر شد</p></div>
<div class="m2"><p>حدیث ما به هر جائی سمر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانی را از آن آگاه کردند</p></div>
<div class="m2"><p>ز وصلش دست ما کوتاه کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو خصمان را از این معنی خبر شد</p></div>
<div class="m2"><p>حکایت بعد از این نوع دگر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در این معنی بسی تقریر کردند</p></div>
<div class="m2"><p>به آخر دست این تدبیر کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که اینجا بودنش کاری است دشوار</p></div>
<div class="m2"><p>بباید رفتنش زین ملک ناچار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر این اندیشه یکسر دل نهادند</p></div>
<div class="m2"><p>بر او زین قصه رمزی برگشادند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو بشنید این سخن خورشید خوبان</p></div>
<div class="m2"><p>ز رفتن شد تنش چون بید لرزان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل اندامم درون پردهٔ راز</p></div>
<div class="m2"><p>چو غنچه تنگ خوئی کرده آغاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفیر و ناله و شیون برآورد</p></div>
<div class="m2"><p>خروش از جان مرد و زن برآورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فغان بر گنبد گردان رسانید</p></div>
<div class="m2"><p>صدای ناله بر کیوان رسانید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز هر نوعی بسی در رفع کوشید</p></div>
<div class="m2"><p>غریمش هر سخن کو گفت نشنید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کز اینجا طاقت دوری ندارم</p></div>
<div class="m2"><p>چنین از عقل دستوری ندارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به پشت بادپائی بر نشاندش</p></div>
<div class="m2"><p>ز آب دیده در آذر نشاندش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>براهش با پری همداستان کرد</p></div>
<div class="m2"><p>پریوارش ز چشم من نهان کرد</p></div></div>