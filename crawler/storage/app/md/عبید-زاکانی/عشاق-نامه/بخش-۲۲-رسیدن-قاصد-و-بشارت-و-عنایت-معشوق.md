---
title: >-
    بخش ۲۲ - رسیدن قاصد و بشارت و عنایت معشوق
---
# بخش ۲۲ - رسیدن قاصد و بشارت و عنایت معشوق

<div class="b" id="bn1"><div class="m1"><p>در این اندیشه شب را روز کردم</p></div>
<div class="m2"><p>فراوان نالهٔ دلسوز کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو از حد افق هنگام شبگیر</p></div>
<div class="m2"><p>علم بفراشت خورشید جهانگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مشرق بر شفق زر می‌فشاندند</p></div>
<div class="m2"><p>به صنعت لعل در زر می‌نشاندند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ طالع شب تیره می‌شد</p></div>
<div class="m2"><p>سپاه روز بر شب چیره می‌شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن ساعت سخن نوعی دگر شد</p></div>
<div class="m2"><p>دعای صبحگاهم کارگر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ناگه پیک دولت می‌دوانید</p></div>
<div class="m2"><p>به من پیغام دلبر می‌رسانید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که دل خوش دار اینک یارت آمد</p></div>
<div class="m2"><p>دگر آبی بروی کارت آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه مدتی رنجی کشیدی</p></div>
<div class="m2"><p>برآخر دست در گنجی کشیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غمی خوردی و غمخواری گرفتی</p></div>
<div class="m2"><p>دلی دادی و دلداری گرفتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز همت دانه‌ای در دام کردی</p></div>
<div class="m2"><p>بدین افسون پری را رام کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشست آن مشفق دیرینه پیشم</p></div>
<div class="m2"><p>دوای درد و مرهم ساز ریشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمن پیغام دلبر باز میگفت</p></div>
<div class="m2"><p>حکایت های غم پرداز میگفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبان چون در پیام یار بگشود</p></div>
<div class="m2"><p>دلم خرم شد و جانم بیاسود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قدح از دست در بستان فکندم</p></div>
<div class="m2"><p>کلاه از عیش بر ایوان فکندم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رمیده بخت من سامان پذیرفت</p></div>
<div class="m2"><p>کهن بیماریم درمان پذیرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گل عیشم به باغ عمر بشکفت</p></div>
<div class="m2"><p>نگارم میرسید و بخت میگفت:</p></div></div>