---
title: >-
    بخش ۳ - غزل
---
# بخش ۳ - غزل

<div class="b" id="bn1"><div class="m1"><p>خم ابروی او در جان فزائی</p></div>
<div class="m2"><p>طراز آستین دلربائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدا از لطف محضش آفریده</p></div>
<div class="m2"><p>به نام ایزد زهی لطف خدائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به غمزه چشم مستش کرده پیدا</p></div>
<div class="m2"><p>رسوم مستی و سحر آزمائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز کوی او غباری کاورد باد</p></div>
<div class="m2"><p>کند در چشم جانها توتیائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بنماید رخ چون ماه تابان</p></div>
<div class="m2"><p>برو پیشش گدائی کن گدائی</p></div></div>