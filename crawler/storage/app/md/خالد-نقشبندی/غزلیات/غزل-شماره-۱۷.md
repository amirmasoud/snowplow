---
title: >-
    غزل شماره ۱۷
---
# غزل شماره ۱۷

<div class="b" id="bn1"><div class="m1"><p>رو به محراب دو ابرویت عبث کردم عبث</p></div>
<div class="m2"><p>سجده سوی کعبه کویت عبث کردم عبث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نئی رحمی به حال داد خواهان آیدت</p></div>
<div class="m2"><p>دست در زنجیر گیسویت عبث کردم عبث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر راهت چو خاک افتادنت بی سود بود</p></div>
<div class="m2"><p>ناله شبگیر در کویت عبث کردم عبث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاکلت را مشک چین گفتم خطا گفتم خطا</p></div>
<div class="m2"><p>نسبت خورشید با رویت عبث کردم عبث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخدا ترس و جفا آئینی و مردم فریب</p></div>
<div class="m2"><p>میل دل روز ازل سویت عبث کردم عبث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به فتراک نگاهت بستنم بد بود بد</p></div>
<div class="m2"><p>جان فدای چشم جادویت عبث کردم عبث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خویت ار خون ریزدم رویت دهد صد خون بها</p></div>
<div class="m2"><p>خالد آسا شکوه از خویت عبث کردم عبث</p></div></div>