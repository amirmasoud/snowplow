---
title: >-
    غزل شماره ۵۵
---
# غزل شماره ۵۵

<div class="b" id="bn1"><div class="m1"><p>عزیز اگر ز روی غمگساری</p></div>
<div class="m2"><p>خیال دوستان در خاطر آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هجران آب بحرین دو دیده</p></div>
<div class="m2"><p>ابد بر بنده روم است جاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گاه گریه ام صد خنده آید</p></div>
<div class="m2"><p>به اشک و آه ابر نوبهاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میازار ار نمردم از فراقت</p></div>
<div class="m2"><p>لعمر الله ما فیه اختیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از داغت چنان سوزد، نسوزد</p></div>
<div class="m2"><p>به بزم خسروان عود قماری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از مردن نترسم، لیک ترسم</p></div>
<div class="m2"><p>گهی برتربتم تشریف ناری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز هجر دوست چندین شکوه خالد</p></div>
<div class="m2"><p>بعید است از طریق جان‌سپاری</p></div></div>