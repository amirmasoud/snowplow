---
title: >-
    غزل شماره ۵۱
---
# غزل شماره ۵۱

<div class="b" id="bn1"><div class="m1"><p>خسروی دارم که کرده درگه مهمیز او</p></div>
<div class="m2"><p>لشگر جانها لگد کوب سم شبدیز او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نهد بر هم لب نازک، توان دیدن چو در</p></div>
<div class="m2"><p>عقد دندانها عیان از لعل قند آمیز او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کشد بر برگ گل مانی ز مشک تر رقم</p></div>
<div class="m2"><p>کی کشد تصویر روی و خط عنبر بیز او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پس آئینه بتوان دید رویش را ز بس</p></div>
<div class="m2"><p>رخنه ها افتد در او از غمزه خونریز او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه خار قاقمش با برگ نسرین می کند</p></div>
<div class="m2"><p>دل نخواهد دید هرگز از خدنگ تیز او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زدی خالد به شیرین عکس روی خسروم</p></div>
<div class="m2"><p>تنگ شکر می‌شدی بی شک دل پرویز او</p></div></div>