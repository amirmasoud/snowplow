---
title: >-
    غرل شماره ۲
---
# غرل شماره ۲

<div class="b" id="bn1"><div class="m1"><p>وام بگرفتم به صد جان گرد نعلین ترا</p></div>
<div class="m2"><p>هست جانی آنهم از تو چون دهم دین ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی توام چندان مطول شد شب تاریک هجر</p></div>
<div class="m2"><p>مختصر خوانم تطاولهای زلفین ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه نو بر مهر ثابت عقرب و پروین روان</p></div>
<div class="m2"><p>وه چه زیبد هیئت اشکال بی شین ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر رسد بندان نگردد کشف راز ار ننگرند</p></div>
<div class="m2"><p>گرد روی خون چکان، چوگان زلفین ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفی جزء و حصر فرد شمس و استلزام او</p></div>
<div class="m2"><p>بس منافی شد دهان و زلف و خدین ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم بیمارت دهد در هر اشارت صد شفا</p></div>
<div class="m2"><p>بوعلی مشکل که داند حکمت العین ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهره ات ز آب دلارائی هوا را داده نم</p></div>
<div class="m2"><p>تاب رخسارت هویدا کرد قوسین ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خالد از ابروی مشکینت اگر گوید سخن</p></div>
<div class="m2"><p>چون کشد آخر کمان قاب قوسین ترا</p></div></div>