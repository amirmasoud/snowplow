---
title: >-
    غزل شماره ۳۵
---
# غزل شماره ۳۵

<div class="b" id="bn1"><div class="m1"><p>ز شوقت شمع چون پروانه رقاص</p></div>
<div class="m2"><p>نه تنها شمع، بل کاشانه رقاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی تابی عشقش منع دل چند؟</p></div>
<div class="m2"><p>کز آتش چون نگردد دانه رقاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر عشقت به کوه آرد شبیخون</p></div>
<div class="m2"><p>جهر از جای چون دیوانه رقاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو در دل، دل به زلفت در کشاکش</p></div>
<div class="m2"><p>چو جان از عشق خود جانانه رقاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تمکین شیشه دل تیره گردد</p></div>
<div class="m2"><p>مودب باش و شو طفلانه رقاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز سوز عشق خالد چون نرقصد؟</p></div>
<div class="m2"><p>کزو چون خویش شد بیگانه رقاص</p></div></div>