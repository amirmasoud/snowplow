---
title: >-
    غزل شماره ۸
---
# غزل شماره ۸

<div class="b" id="bn1"><div class="m1"><p>الهی تا به کی مرغ دل اندر دام کاکل‌ها</p></div>
<div class="m2"><p>بود درمانده و پا بسته، ای حلال مشکل‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه خامه مانی ز فیضت رشحه‌ریز آمد</p></div>
<div class="m2"><p>کجا یک قطره شبنم ریختی بر چهره گل‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگرنه بر گلستان پرتو حسنت زدی عکسی</p></div>
<div class="m2"><p>که در وی می‌شنیدی بانگ واویلای بلبل‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تقدیر ار نبودی دست تقدیر جهان‌آرا</p></div>
<div class="m2"><p>که را در خود بدی مشاطگی زلف سنبل‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک جلوه ز روی ماه کنعنانی درافکندی</p></div>
<div class="m2"><p>ز شهرستان مغرب تا به مصر آواز غلغل‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمالی را که نه آرایش از عکس رخت گیرد</p></div>
<div class="m2"><p>چه سود از خط و خال و غازه و زیب و تجمل‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به داد خالد بیچاره درمانده رس یا رب</p></div>
<div class="m2"><p>که دارد قلزم جودت بسی چون او به ساحل‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یک جنبش ز برق لامع نور قدیم جود</p></div>
<div class="m2"><p>به لطفش وارهان از گردش دور تسلسل‌ها</p></div></div>