---
title: >-
    غزل شماره ۳
---
# غزل شماره ۳

<div class="b" id="bn1"><div class="m1"><p>به معمار غمت نو ساختم ویرانه خود را</p></div>
<div class="m2"><p>به یادت کعبه کردم عاقبت بتخانه خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرو ماندند اطبای جهان از چاره‌ام آخر</p></div>
<div class="m2"><p>به دردی یافتم درمان، دل دیوانه خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سودایت چنان بد نام گشتم در همه عالم</p></div>
<div class="m2"><p>به گوش خود شنیدم هر طرف افسانه خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گرد شمع رویت بس که گشتم ماندم از پرواز</p></div>
<div class="m2"><p>سرت گردم چه زیبا سوختی پروانه خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ادیب من جلیس من شود در حلقه رندان</p></div>
<div class="m2"><p>به گوشش گر رسانم ناله مستانه خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در اقلیم محبت از خرابی‌هاست معموری</p></div>
<div class="m2"><p>به سیل اشک باید کند اساس خانه خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراپا نعمتم با این همه درماندگی خالد</p></div>
<div class="m2"><p>نمی‌دانم چه سان آرم به جا شکرانه خود را</p></div></div>