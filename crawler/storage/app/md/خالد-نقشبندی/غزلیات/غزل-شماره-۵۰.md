---
title: >-
    غزل شماره ۵۰
---
# غزل شماره ۵۰

<div class="b" id="bn1"><div class="m1"><p>نشتر فولاد یا مژگان دلدار است این</p></div>
<div class="m2"><p>نشات می یا نگاه چشم بیمار است این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ژاله بر گل یا خوی خجلت ز شرم روی دوست</p></div>
<div class="m2"><p>یا عرق بر جبهه شوخ ستمکار است این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلک مانی ریخت بر برگ سمن مشک ختن</p></div>
<div class="m2"><p>یا خط نو سرزده از روی دلدار است این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمس خاور بر سر سروسهی بگرفت جای</p></div>
<div class="m2"><p>یا بلا یا خود همین بالا و رخسار است این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست این یاقوت کان، یاقوت جان بیدلان</p></div>
<div class="m2"><p>آب حیوان یا لب لعل شکر بار است این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره آب بقا یا رشحه چاه زنخ</p></div>
<div class="m2"><p>سیب بستان ارم یا غبغب یار است این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیست این کز نغمه جانکاه او دل می رود</p></div>
<div class="m2"><p>خالد دلسوخته یا بلبل زار است این</p></div></div>