---
title: >-
    غزل شماره ۳۲
---
# غزل شماره ۳۲

<div class="b" id="bn1"><div class="m1"><p>بازم افتاد به دل داغ نگاری که مپرس</p></div>
<div class="m2"><p>لاله زاری است پر از لاله عذاری که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشته جان صید بت تازه شکاری که مگوی</p></div>
<div class="m2"><p>شده دل بسته فتراک سواری که مپرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا غبار فتن انگیخته در دور قمر</p></div>
<div class="m2"><p>از خطش ره به دل آورده غباری که مپرس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا برون شد به سفر می کشد از قطره اشک</p></div>
<div class="m2"><p>خون دل دم به دم از از دیده قطاری که مپرس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو دگر میکده را در نگشاید خمار</p></div>
<div class="m2"><p>که مرا هست از آن دیده خماری که مپرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسم تیر کنم گریه بحال بلبل</p></div>
<div class="m2"><p>دارم از هجرت گل ناله زاری که مپرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شد از خنده گل صحن گلستان خالی</p></div>
<div class="m2"><p>سر فرو برده به دل چنگل خاری که مپرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نظم گهر اشک جدایی خالد</p></div>
<div class="m2"><p>به هم آورده به امید نثاری که مپرس</p></div></div>