---
title: >-
    غزل شماره ۴۶
---
# غزل شماره ۴۶

<div class="b" id="bn1"><div class="m1"><p>عاشق و مست و خراب کیستم؟</p></div>
<div class="m2"><p>بیخود از جام شراب کیستم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مذاق آب حیاتم تلخ شد</p></div>
<div class="m2"><p>تشنه لعل مذاب کیستم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیم بسمل غرقه اندر خون و خاک</p></div>
<div class="m2"><p>صید چشم نیم خواب کیستم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در به در مانند قیس عامری</p></div>
<div class="m2"><p>واله شوق جناب کیستم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخت بر بست از دلم صبر و قرار</p></div>
<div class="m2"><p>اینچنین در اضطراب کیستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آتش دل سوختم سر تا به پای</p></div>
<div class="m2"><p>ای دریغا من کباب کیستم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خالد اندر رقص و حالت ذره سان</p></div>
<div class="m2"><p>در هوای آفتاب کیستم؟</p></div></div>