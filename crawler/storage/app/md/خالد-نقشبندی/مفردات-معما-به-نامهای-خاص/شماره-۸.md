---
title: >-
    شماره ۸
---
# شماره ۸

<div class="b" id="bn1"><div class="m1"><p>خالدا کرد فلک قد ترا خم، یعنی</p></div>
<div class="m2"><p>که ازین در به یقین وقت بردن رفتن تست</p></div></div>