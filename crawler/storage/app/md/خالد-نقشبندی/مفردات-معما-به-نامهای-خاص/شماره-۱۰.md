---
title: >-
    شماره ۱۰
---
# شماره ۱۰

<div class="b" id="bn1"><div class="m1"><p>ور از نفس کسی چراغ افسرد</p></div>
<div class="m2"><p>آن شمع شبستان شرف روشن باد</p></div></div>