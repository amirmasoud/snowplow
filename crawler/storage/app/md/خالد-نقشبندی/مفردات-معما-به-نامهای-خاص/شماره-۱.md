---
title: >-
    شماره ۱
---
# شماره ۱

<div class="b" id="bn1"><div class="m1"><p>تا به خالت شد سر زلف آشنا</p></div>
<div class="m2"><p>عالمی را عام شد درد و بلا</p></div></div>