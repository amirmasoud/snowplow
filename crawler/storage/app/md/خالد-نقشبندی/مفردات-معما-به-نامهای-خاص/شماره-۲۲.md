---
title: >-
    شماره ۲۲
---
# شماره ۲۲

<div class="b" id="bn1"><div class="m1"><p>شد چو ماه از نسل زهرا منجلی</p></div>
<div class="m2"><p>اختر برج شرف، سید علی</p></div></div>