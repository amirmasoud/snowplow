---
title: >-
    شماره ۲۰
---
# شماره ۲۰

<div class="b" id="bn1"><div class="m1"><p>روی زمین جمله زبرجد شده</p></div>
<div class="m2"><p>زاغ و زغن آخر ابجد شده</p></div></div>