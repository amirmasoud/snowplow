---
title: >-
    شماره ۲۳
---
# شماره ۲۳

<div class="b" id="bn1"><div class="m1"><p>جدل با هزبر دمان می‌کنی</p></div>
<div class="m2"><p>چنین می‌شوی، چون چنان می‌کنی</p></div></div>