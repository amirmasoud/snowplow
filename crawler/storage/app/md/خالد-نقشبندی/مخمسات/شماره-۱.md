---
title: >-
    شماره ۱
---
# شماره ۱

<div class="b" id="bn1"><div class="m1"><p>ای وصل تو اعظم امانی</p></div>
<div class="m2"><p>سرمایه و عیش و کامرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بی تو به عمر جاودانی</p></div>
<div class="m2"><p>یک لحظه زیم به شادمانی</p></div></div>
<div class="b2" id="bn3"><p>یارب نخورم بر از جوانی</p></div>
<div class="b" id="bn4"><div class="m1"><p>در بحر غمم فتاده مشکل</p></div>
<div class="m2"><p>کشتی رسدم دمی به ساحل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر باد شدیم ز آتش دل</p></div>
<div class="m2"><p>شد آب دو چشمم خاک تن گل</p></div></div>
<div class="b2" id="bn6"><p>افسوس تو حال ما ندانی</p></div>
<div class="b" id="bn7"><div class="m1"><p>بی مهر رخ تو شام هجران</p></div>
<div class="m2"><p>ریزم چو سپهر خون به دامان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد چاک کنم ز غم گریبان</p></div>
<div class="m2"><p>لیکن چو نمی رسد به سامان</p></div></div>
<div class="b2" id="bn9"><p>مقصود چه سود خون فشانی؟</p></div>
<div class="b" id="bn10"><div class="m1"><p>پیوسته چون غنچه می خورم خون</p></div>
<div class="m2"><p>هر گوشه رود ز دیده جیحون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامن شده ز اشک سرخ گلگون</p></div>
<div class="m2"><p>بی قد تو لیک سرو موزون</p></div></div>
<div class="b2" id="bn12"><p>ما را چه هوای گلستانی</p></div>
<div class="b" id="bn13"><div class="m1"><p>گر زیسته ایم بی وصالت</p></div>
<div class="m2"><p>غرق عرقیم از خجالت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اما به دو ابروی هلالت</p></div>
<div class="m2"><p>این نیست حیات بی جمالت</p></div></div>
<div class="b2" id="bn15"><p>مرگی است به نام زندگانی</p></div>
<div class="b" id="bn16"><div class="m1"><p>بی صبر و شکیب داشت یک چند</p></div>
<div class="m2"><p>خون دل ریش آرزومند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با هجر تو تا گرفت پیوند</p></div>
<div class="m2"><p>ای من به خیال از تو خرسند</p></div></div>
<div class="b2" id="bn18"><p>ببرید ز دوستان جانی</p></div>
<div class="b" id="bn19"><div class="m1"><p>خالد ز دو دیده خون سارا</p></div>
<div class="m2"><p>می بار نهان و آشکارا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زان ابر نهال مدعا را</p></div>
<div class="m2"><p>شاداب همی نما خدا را</p></div></div>
<div class="b2" id="bn21"><p>تا بار دهد همان که دانی</p></div>
<div class="b" id="bn22"><div class="m1"><p>یارب به مجردان افلاک</p></div>
<div class="m2"><p>یا رب به شه سریر لولاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از غیر تو رستگان بی باک</p></div>
<div class="m2"><p>پیش تو شفیع آورم تاک</p></div></div>
<div class="b2" id="bn24"><p>بار دگرم بدو رسانی</p></div>