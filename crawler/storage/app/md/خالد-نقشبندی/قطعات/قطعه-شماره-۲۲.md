---
title: >-
    قطعه شماره ۲۲
---
# قطعه شماره ۲۲

<div class="b" id="bn1"><div class="m1"><p>ای شده در دهر به دانش علم</p></div>
<div class="m2"><p>وی زده بر مهر ز عنبر رقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نامه اندوه زدایت رسید</p></div>
<div class="m2"><p>شکوه کنان از من و رنج و الم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلسله اش مرغ روان را چو دام</p></div>
<div class="m2"><p>رایحه اش اخگر دل را چو دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حق تو نیست قصوری مرا</p></div>
<div class="m2"><p>لیک، به آن جان عزیزت قسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوش نبد در دم باز آمدن</p></div>
<div class="m2"><p>رفت ز یادم که به خدمت رسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست بسی کار به بی اختیار</p></div>
<div class="m2"><p>نیست نهان نکته جف القلم</p></div></div>