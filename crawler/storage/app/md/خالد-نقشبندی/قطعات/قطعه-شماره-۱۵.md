---
title: >-
    قطعه شماره ۱۵
---
# قطعه شماره ۱۵

<div class="b" id="bn1"><div class="m1"><p>اندر ره عشق خسته جانی بهتر</p></div>
<div class="m2"><p>وز شرح غم تو بی زبانی بهتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیمارئی کاو موجب دیدار تو بود</p></div>
<div class="m2"><p>صد بار ز صحت جوانی بهتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وصل توام ز شربت مرگ چه باک</p></div>
<div class="m2"><p>وصلت ز زلال زندگانی بهتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آزرده مشو عزیز من ز آزارم</p></div>
<div class="m2"><p>صد چون من اگر مرد، تو مانی بهتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنجورم وز آزردگیت می میرم</p></div>
<div class="m2"><p>بر من ز گل ار شکر فشانی بهتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان می کنم و طاقت فریادم نیست</p></div>
<div class="m2"><p>جان کندن عشاق نهانی بهتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خالد اگرت هست به کف جوهر جان</p></div>
<div class="m2"><p>از بهر نثار یار جانی بهتر</p></div></div>