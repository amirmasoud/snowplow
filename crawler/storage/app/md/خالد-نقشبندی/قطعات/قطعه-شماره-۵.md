---
title: >-
    قطعه شماره ۵
---
# قطعه شماره ۵

<div class="b" id="bn1"><div class="m1"><p>آنکه صد فضل بر روان دارد</p></div>
<div class="m2"><p>هر که سودای نام آن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام نامی او به بیت اخیر</p></div>
<div class="m2"><p>همچو در در صدف مکان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج فضل است و معدن عرفان</p></div>
<div class="m2"><p>زیبد ار خالدش نهان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچنان جای کرده در دل تنگ</p></div>
<div class="m2"><p>تو مپندار جای جان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خامه در وصف آدمیت او</p></div>
<div class="m2"><p>اخرس است ارچه صد زبان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلف سربسته از دل عشاق</p></div>
<div class="m2"><p>مرغ پا بسته در میان دارد</p></div></div>