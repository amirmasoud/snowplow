---
title: >-
    قطعه شماره ۴۴
---
# قطعه شماره ۴۴

<div class="b" id="bn1"><div class="m1"><p>واله شوق جمال دوستان</p></div>
<div class="m2"><p>بی نصیب از گشت باغ و بوستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده وامانده از وصل حبیب</p></div>
<div class="m2"><p>خالد درمانده در هندوستان</p></div></div>