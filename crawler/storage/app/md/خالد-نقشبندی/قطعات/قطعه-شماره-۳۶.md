---
title: >-
    قطعه شماره ۳۶
---
# قطعه شماره ۳۶

<div class="b" id="bn1"><div class="m1"><p>ای از مژه ات غرقه به خون استادم</p></div>
<div class="m2"><p>وز خط تو در قید جنون استادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلداده دیده خمار آلودت</p></div>
<div class="m2"><p>عالم همه وز جمله فزون استادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیدای دو آهوی شکار اندازت</p></div>
<div class="m2"><p>صد چون من و صد هزار چون استادم</p></div></div>