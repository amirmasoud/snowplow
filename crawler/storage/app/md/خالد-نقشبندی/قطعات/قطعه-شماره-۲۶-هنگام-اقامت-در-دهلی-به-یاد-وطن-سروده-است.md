---
title: >-
    قطعه شماره ۲۶ (هنگام اقامت در دهلی به یاد وطن سروده است)
---
# قطعه شماره ۲۶ (هنگام اقامت در دهلی به یاد وطن سروده است)

<div class="b" id="bn1"><div class="m1"><p>خون شد دلم، نسیم صبا غمگسار شد</p></div>
<div class="m2"><p>بر دشت شهرزور، دمی رهگذار شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت آنکه ما به عیش در آن بوم بگذریم</p></div>
<div class="m2"><p>زینهار تو وکیل من دل فگار شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می بوس خاک آن چمن و بعد از آن روان</p></div>
<div class="m2"><p>نزدیک بارگاه بت پرده دار شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واکن به صد هزار ادب بند برقعش</p></div>
<div class="m2"><p>حیران نقش خامه پروردگار شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشا چو غنچه کوی گریبان کرته اش</p></div>
<div class="m2"><p>محو صفای سینه آن گلعذار شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاری ز چین طره اش از لطف باز کن</p></div>
<div class="m2"><p>گو سرچنار را که تورشک تتار شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم بر دلم نشست چو گردون ز داغ هجر</p></div>
<div class="m2"><p>ای چشمه سار چشم تو هم سر چنار شو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی کاری است کار جهان و جهانیان</p></div>
<div class="m2"><p>بگریز خالد از همه و مردکار شو</p></div></div>