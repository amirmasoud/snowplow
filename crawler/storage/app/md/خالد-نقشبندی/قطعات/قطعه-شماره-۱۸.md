---
title: >-
    قطعه شماره ۱۸
---
# قطعه شماره ۱۸

<div class="b" id="bn1"><div class="m1"><p>ای گشته من فگار بی تو</p></div>
<div class="m2"><p>نومید ز زندگانی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زارم چو کشی به درد هجران</p></div>
<div class="m2"><p>می ترس از نوجوانی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند فراشم گذاری</p></div>
<div class="m2"><p>یاد آر ز مهربانی خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بی تو نزیستم، نکردم</p></div>
<div class="m2"><p>اقرار به سخت جانی خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود گوی که با که گویم آخر؟</p></div>
<div class="m2"><p>شرح الم نهانی خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز آی که بهر تو گذشتم</p></div>
<div class="m2"><p>از مطلب دو جهانی خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چرخ تو را ز من بریده است</p></div>
<div class="m2"><p>شاد است به نکته دانی خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعقوب به کنج غم گرفتار</p></div>
<div class="m2"><p>یوسف به جهان ستانی خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جانا به سعادتی که داری</p></div>
<div class="m2"><p>رحم آر به یار جانی خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دریاب که بی تو گشت خالد</p></div>
<div class="m2"><p>بیزار ز زندگانی خویش</p></div></div>