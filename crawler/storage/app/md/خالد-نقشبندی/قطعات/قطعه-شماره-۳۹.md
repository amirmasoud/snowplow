---
title: >-
    قطعه شماره ۳۹
---
# قطعه شماره ۳۹

<div class="b" id="bn1"><div class="m1"><p>ما هر علم و حاکم لولاک</p></div>
<div class="m2"><p>اوحدی ممالک ادراک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داور دهر و حامی اسلام</p></div>
<div class="m2"><p>اعلم و اعمل و همه ادراک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالع سعد، احمد مرسل</p></div>
<div class="m2"><p>سحر را محو کرده در املاک</p></div></div>