---
title: >-
    قطعه شماره ۱۰ (در زیارت مرقد بایزید بسطامی)
---
# قطعه شماره ۱۰ (در زیارت مرقد بایزید بسطامی)

<div class="b" id="bn1"><div class="m1"><p>یارب به حق تربت سلطان بایزید</p></div>
<div class="m2"><p>یارب به قاطعیت برهان بایزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب به آشیانه شهباز لامکان</p></div>
<div class="m2"><p>یارب به قرب و منزلت جان بایزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب به حق وسعت آن مشرب کریم</p></div>
<div class="m2"><p>یارب به تشنگی فراوان بایزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب به سوز سینه آن پیر نیک بخت</p></div>
<div class="m2"><p>یارب به نور مشعل ایمان بایزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا رب به هر دو سلسله حضرت رسول</p></div>
<div class="m2"><p>تا جعفر از اعاظم پیران بایزید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حضرت غلامعلی تا به بوالحسن</p></div>
<div class="m2"><p>یک یک به حق جمله مریدان بایزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خالد شکسته بیچاره غریب</p></div>
<div class="m2"><p>بگشا دری ز مخزن عرفان بایزید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لب تشنه زلال هدایت بود، ورا</p></div>
<div class="m2"><p>سیراب کن ز قلزم احسان بایزید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او را به خود رسان وز خود بینیش رهان</p></div>
<div class="m2"><p>او هم یکی شود ز غلامان بایزید</p></div></div>