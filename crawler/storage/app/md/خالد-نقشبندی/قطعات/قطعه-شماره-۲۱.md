---
title: >-
    قطعه شماره ۲۱
---
# قطعه شماره ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای خون فشرده در دل یاقوتت از رقم</p></div>
<div class="m2"><p>تیر فلک چو قوس ز رشک شده است خم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این بارهاست کز پی یک نامه سیاه</p></div>
<div class="m2"><p>آدم روانه گشت، نه لا بود و نه نعم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو سوار شاهی و بیمی زمات نیست</p></div>
<div class="m2"><p>آن به صریح گویی و رخ ناوری به هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این بار می فرست وگرنه ز دست تو</p></div>
<div class="m2"><p>خواهیم برد شکوه به شاه فلک همم</p></div></div>