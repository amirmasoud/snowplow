---
title: >-
    شماره ۵
---
# شماره ۵

<div class="b" id="bn1"><div class="m1"><p>قبیله م فیراقت، قبیله م فیراقت</p></div>
<div class="m2"><p>ئه رامم سه نده ن سه وادی فیراقت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل قه قنه س ئاسان جه ئیشتیاقت</p></div>
<div class="m2"><p>طاقه ت تاق بیه ن په ی ئه بروی طاقت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوور جه قامتت قیامه ن خیزان</p></div>
<div class="m2"><p>هیجرت شه راره ی جه هه نم بیزان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاری پیم که رده ن مه حروومیی رازت</p></div>
<div class="m2"><p>نه که رده ن وه دل نیم نگای نازت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قه در عافیت وه صلت نه زانام</p></div>
<div class="m2"><p>شوکرانه ی شه که ررازت نه وانام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغه م کوی شادیم بادوه باد شانو</p></div>
<div class="m2"><p>ته مام ئینتیقام ده صلت جیم سانو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاص خاص جه شیدده ت نائیره ی دووری</p></div>
<div class="m2"><p>وه کوی نووره که رد سه ر تا پای نووری</p></div></div>