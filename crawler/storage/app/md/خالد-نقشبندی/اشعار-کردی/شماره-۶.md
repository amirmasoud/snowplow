---
title: >-
    شماره ۶
---
# شماره ۶

<div class="b" id="bn1"><div class="m1"><p>هامسه ران نه سه ب... هامسه ران نه سه ب</p></div>
<div class="m2"><p>مه حبو و بیم هه ن شای عالی نه سه ب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ئیسمش موحه ممه د، قوره یشی و عه ره ب</p></div>
<div class="m2"><p>ئه داش ئامینه کناچه ی وه هه ب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ته مامی عومرش شه صت و سی ساله ن</p></div>
<div class="m2"><p>باب و با پیرش به ی طه رز و حاله ن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عه بدوللا، عه بدولموططه لیب، هاشم</p></div>
<div class="m2"><p>عه بدولمه نافه ن، بزانه ش لازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه کوتا و نه به رز، سپی و گه ندم گوون</p></div>
<div class="m2"><p>نه که وتن سایه اش نه رووی دنیای دوون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به وه له دبی یه ن جه مه ککه ی ئه نوه ر</p></div>
<div class="m2"><p>چل سال چاگه مه ند بی پی به پیغه مبه ر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وه سیزده ی هه نی ئه و خورجه مینه</p></div>
<div class="m2"><p>راهی بی، کوچ که ردشی وه مه دینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چوگه موقیم بی تامودده ی ده سال</p></div>
<div class="m2"><p>مه رگش په ی ئاماجه لای بی زه وال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به رشی جه دنیای فانیی بی بنیاد</p></div>
<div class="m2"><p>ئه بله هر که سیوه ن دل پیش که روشاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد دو بیست و چارهه زار پیغه مبر</p></div>
<div class="m2"><p>سیصه دو سیزده ره سول ره هبه ر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ئیبراهیم و نووح، مووسی ئولوولعه زم</p></div>
<div class="m2"><p>عیسی و موحه ممه د بزانه ش وه جه زم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سه بیدولکه و نه ین، خه تمو لمورسه لین</p></div>
<div class="m2"><p>جه ماسیوای حه ق بیته ر جه گردین</p></div></div>