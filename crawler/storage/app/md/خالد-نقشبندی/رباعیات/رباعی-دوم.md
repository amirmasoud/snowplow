---
title: >-
    رباعی دوم
---
# رباعی دوم

<div class="b" id="bn1"><div class="m1"><p>امروز که منزلم نصیبین گردید</p></div>
<div class="m2"><p>از داغ غمت دلم نصیبین گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوری ز سر کوی تو از من دور است</p></div>
<div class="m2"><p>اما چه توان کرد نصیبب این گردد</p></div></div>