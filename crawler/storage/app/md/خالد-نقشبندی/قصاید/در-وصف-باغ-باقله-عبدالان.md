---
title: >-
    در وصف باغ باقله عبدالان
---
# در وصف باغ باقله عبدالان

<div class="b" id="bn1"><div class="m1"><p>گوش باید کرد ازین سرگشته اندوهگین</p></div>
<div class="m2"><p>شمه ای از صنعت خلاق گیتی آفرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند تن روزی ز همزادان ز جام عیش مست</p></div>
<div class="m2"><p>بهر گشت گلستان گشتیم با هم همقرین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ده به ده، صحرا به صحرا، تا به گلزار ارم</p></div>
<div class="m2"><p>یعنی باغ عبدلان آن معدن ارباب دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان هاتف ز هر سو بانگ زد کای بیدلان</p></div>
<div class="m2"><p>هذه جنات عدن فادخلوها خالدین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون فرو بردیم سر بهر تماشای چمن</p></div>
<div class="m2"><p>از دل ما محو شد سودای فردوس برین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو و شمشاد و صنوبر، بید مشک و نارون</p></div>
<div class="m2"><p>ایستاده صف به صف چون دلبران نازنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عر عر از سودای گل دیوانه خواهدشد مگر</p></div>
<div class="m2"><p>زان به پا قید جنونش گشته زلف یاسمین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوئیا با قد جانان لاف رعنایی زده</p></div>
<div class="m2"><p>بید مجنون، زان کند روی خجالت بر زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوطی و دراج و ساری، تیهو و کبک دری</p></div>
<div class="m2"><p>داده بر باد از نوا اندوه عشاق حزین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چهچه بلبل، صدای قمری و بانگ تذرو</p></div>
<div class="m2"><p>کرده جا الحانش در گوش سپهر هشتمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویی از چاه زنخدان عزیزان آب خورد</p></div>
<div class="m2"><p>می چکد از آبیش آب نزاکت اینچنین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوج و زرد آلود، انار و پسته، انجیر و عنب</p></div>
<div class="m2"><p>هر یکی گوید که ای طالب بیا از من بچین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از لطافت در میان سیب و امرود است جنگ</p></div>
<div class="m2"><p>مشت از آن مالند بر فرق دگر از روی کین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می توان مدهوش بود از بوی خاکش تا ابد</p></div>
<div class="m2"><p>بسکه می ریزد ز شاخ تاک خشکش بر زمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی طفلان بستان یعنی گنجشکان او</p></div>
<div class="m2"><p>شیره می بارد بجای شیر از پستان تین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند انواع ریاحین برکنار جویبار</p></div>
<div class="m2"><p>سوسن و لاله، بنفشه، نرگس دیده نمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل شقایق، زلف عروس، تاج خروس و پیل گوش</p></div>
<div class="m2"><p>هر یکی گوید منم بهتر، بسوی من ببین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از نوای نغمه سنجان گوش گردون گشته کر</p></div>
<div class="m2"><p>از تواضع زهره هر دم بر زمین ساید جبین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می خورد هر دم سمندر غوطه ها در جوی آب</p></div>
<div class="m2"><p>گوئیا آتش شده است از سایه گل آتشین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از نزاکت می برد آب زلالش بر کمر</p></div>
<div class="m2"><p>بیدلان را صبر و آرام و شکیب و عقل و دین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون فروریزد ز کوه باقله با صد طرب</p></div>
<div class="m2"><p>گردد از عکس هوا هر قطره اش دری ثمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یارب این آبست ازین کوه بلند آید به زیر</p></div>
<div class="m2"><p>یا فلک از رشک ریزد اشک حسرت بر زمین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از صدای دلبرای صافیش گردد خجل</p></div>
<div class="m2"><p>ناله بربط، بیاض گردن خوبان چین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وز نسیم جانفزاش اندک اندک بر کمر</p></div>
<div class="m2"><p>می شود سنبل پریشان همچو زلف حور عین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کردگارا، شهسوار عرصه روز جزا</p></div>
<div class="m2"><p>آورم پیشت شفیع و حضرت روح الامین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خالد از فرط گنه شرمنده درگاه تست</p></div>
<div class="m2"><p>فاعف عنه کل ذنب، انت خیر الراحمین</p></div></div>