---
title: >-
    شماره ۱ (ابیات منتخبه من قصیدیه العربیه من بحر الکامل فی مدح شیخه اانشاها لیله دخوله بلده جهان آباد)
---
# شماره ۱ (ابیات منتخبه من قصیدیه العربیه من بحر الکامل فی مدح شیخه اانشاها لیله دخوله بلده جهان آباد)

<div class="b" id="bn1"><div class="m1"><p>کملت مسافه کعبه الآمال</p></div>
<div class="m2"><p>حمدا لمن قد من بالاکمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>و اراح مرکبی الطلیح من السری</p></div>
<div class="m2"><p>و من اعتوارالحط و الترحال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نجانی من قید الاقارب و الوطن</p></div>
<div class="m2"><p>و علاقه الاحباب و الاموال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و هموم امهتی و حسره اخوتی</p></div>
<div class="m2"><p>و غموم عمی اوخیال الخال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>و مواعظ السادت و العلما</p></div>
<div class="m2"><p>و ملامه الحساد و العذال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و اعاذنی من فرقه افاکه</p></div>
<div class="m2"><p>و اجارنی من امه جهال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و هجوم امواج البحار الزاخره</p></div>
<div class="m2"><p>و اذیه المکاس و العمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>و انا لنی اعلی المآرب و المنی</p></div>
<div class="m2"><p>اعنی لقاء المرشد المضال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من الآفاق بعد ظلامها</p></div>
<div class="m2"><p>و هدی جمیع الخلق بعد ظلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اعنی غلام علی القرم الذی</p></div>
<div class="m2"><p>من لحظه یحیی الرمیم البال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تمثیله ماساغ الا انه</p></div>
<div class="m2"><p>ما ناقش الادباء فی التمثال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هویم فضل طود طول والکرم</p></div>
<div class="m2"><p>یینبوع کل فضیله و خصال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نجم الهدی، بدرالدجی، بحر التقی</p></div>
<div class="m2"><p>کنز الفیوض، خزانه الاحوال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کالارض حلما، و الجبال تمکنا</p></div>
<div class="m2"><p>والشمس ضو، والسما معال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عین الشریعه، معدن العرفان</p></div>
<div class="m2"><p>عون البریه، منبع الافضال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قطب الطرائق، قدوه الاوتاد</p></div>
<div class="m2"><p>غوث الخلایق، رحله الابدال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شیخ الانام و قبله الاسلام</p></div>
<div class="m2"><p>صدر العظام و مرجع الاشکال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هاد الی الاولی بهدی مختف</p></div>
<div class="m2"><p>داع الی المولی بصوت عال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مجبوب رب العالمین من اقتدی</p></div>
<div class="m2"><p>بهداه اصبح قدوه الامثال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کم من جهول بالهوی مکبول</p></div>
<div class="m2"><p>نجاه من لحظ کحل عقال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کم من ولی کامل من صده</p></div>
<div class="m2"><p>قد صد عنه عجائب الاحوال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کم منکر لعلو شانه قدردی</p></div>
<div class="m2"><p>فاذاقه المولی اشد نکال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معطی کمال تمام اهل نقیصه</p></div>
<div class="m2"><p>و مزیل نقص جمیع اهل کمال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اخفاه رب العز جل جلاله</p></div>
<div class="m2"><p>فی قبه الاعزاز و الاجلال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یا اهل مکه حوله در طائفا</p></div>
<div class="m2"><p>واهجر حجازا ان سمعت مقالی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>و مبیت خیف دع ور کض محسر</p></div>
<div class="m2"><p>و منی منی والرمی للامیال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>واسکن بذا الوادی المقدس خالعا</p></div>
<div class="m2"><p>نعلی هوی الکونین باستعجال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حجر مقامک بالمطاف بلاصفا</p></div>
<div class="m2"><p>من طوف حضره کعبه الآمال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ما السعی الا فی رضاه بملتزم</p></div>
<div class="m2"><p>ما الطوف الا حوله بحلال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من شام لمعا من بروق دیاره</p></div>
<div class="m2"><p>بمشام روض الشام کیف یبالی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنست من تلقاء مدین مصره</p></div>
<div class="m2"><p>نارا تهیج البال بالبلبال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فهجرت اهلی قائلا له امثکوا</p></div>
<div class="m2"><p>ارجع الیکم غب الاستشعال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>و نویت هجران الاحبه والوطن</p></div>
<div class="m2"><p>و رکبت متن الادهم الصهال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فطوی منازل فی مسیره منزل</p></div>
<div class="m2"><p>فنسیت اصحابی علی میثاقهم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>و اها لجار سابح شملال</p></div>
<div class="m2"><p>و مواعدی من فرط شوق جمال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>من لی بتبلیغ السلام لاخوتی</p></div>
<div class="m2"><p>و ببسط عذر الغدر و الاهمال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سلب الهوی لبی فما فی خاطری</p></div>
<div class="m2"><p>غیر الحبیب و طیف شوق وصال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قدحان حین تشرفی بوصاله</p></div>
<div class="m2"><p>من لی بشکر عطیه الایصال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یارب لا احصی ثناءک انه</p></div>
<div class="m2"><p>سفه علی من شم ریح زوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>الله لو اعطیت عمرا خالدا</p></div>
<div class="m2"><p>و ترکت غیرالحمد کل فعال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>واتیح لی فی کل منبت شعره</p></div>
<div class="m2"><p>الفا لسان فی الوف مقال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>و امیط عنی النفس و الشیطان کی</p></div>
<div class="m2"><p>لا تلهیانی بخطره فی البال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فصرفت عمری کله فی حمده</p></div>
<div class="m2"><p>بشراشری ابدا بلا اهمال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ما اقدرن علی کفاء عطیه</p></div>
<div class="m2"><p>فضلا عن التفصیل بالاجمال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این العطایا و هی غیر عدیده</p></div>
<div class="m2"><p>کیف التنکر و هو بعض نوال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ام کیف احمد ناظما اوناثرا</p></div>
<div class="m2"><p>ذاتا ترقت عن حضیض خیال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سلب التجوز و المجاز ابلغ</p></div>
<div class="m2"><p>من تقدسه عن الامثال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اله الخلایق فی نعوث کماله</p></div>
<div class="m2"><p>سبحانه من خالق متعال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>فالعجز نطقی والتحیر فکرتی</p></div>
<div class="m2"><p>ما ینبغی الا السکوت بحالی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فکما قضیت الهنا فی اشهر</p></div>
<div class="m2"><p>طیا لبعد مسافه الاحوال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>و حبیتنا حفظا عن لافات</p></div>
<div class="m2"><p>و منتحنا امنا من الاهوال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>و رزقنا تقبیل عتبه قبله</p></div>
<div class="m2"><p>فاز المقبل منه بالاقبال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>فارزق اله العالمین بحقه</p></div>
<div class="m2"><p>ادبا یلیق بذالجناب العالی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>و امدنا بلقائه و بقائه</p></div>
<div class="m2"><p>و عطائه و نواله المتوالی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>زدمن حیاتی فی اطاله عمره.</p></div>
<div class="m2"><p>آدم الوری بحماه تحت ظلال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>و اجعلنی مسعودا بحسن قبوله</p></div>
<div class="m2"><p>و امنحنی ما یرضیه من اعمال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زد کل یوم فی فوادی وقعه</p></div>
<div class="m2"><p>ما دمت حیا فی جمیع الحال</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>و امتنی مرضیا لدیه و راضیا</p></div>
<div class="m2"><p>عنه رضا یجدی مفاز مآلی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>فالحمد للرب الرحیم المنعم</p></div>
<div class="m2"><p>القادر المتقدس المتعال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ثم الصلوه علی الرسول المجتبی</p></div>
<div class="m2"><p>خیر الوری و الصحب بعد الآل</p></div></div>