---
title: >-
    دوبیتی شمارهٔ ۲۶۲
---
# دوبیتی شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>اگر شاهین بچرخ هشتمینه</p></div>
<div class="m2"><p>کند فریاد مرگ اندر کمینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر صد سال در دنیا بمانی</p></div>
<div class="m2"><p>در آخر منزلت زیر زمینه</p></div></div>