---
title: >-
    دوبیتی شمارهٔ ۲۱
---
# دوبیتی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ته که می‌شی بمو چاره بیاموج</p></div>
<div class="m2"><p>که این تاریک شوانرا چون کرم روج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کهی واجم که کی این روج آیو</p></div>
<div class="m2"><p>کهی واجم که هرگز وا نه‌ای روج</p></div></div>