---
title: >-
    دوبیتی شمارهٔ ۲۴۲
---
# دوبیتی شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>دلم میل گل روی ته دیره</p></div>
<div class="m2"><p>سرم سودای گیسوی ته دیره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چشمم بماه نو کره میل</p></div>
<div class="m2"><p>نظر بر طاق ابروی ته دیره</p></div></div>