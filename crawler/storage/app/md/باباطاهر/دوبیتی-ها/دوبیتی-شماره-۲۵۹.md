---
title: >-
    دوبیتی شمارهٔ ۲۵۹
---
# دوبیتی شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>نوای ناله غم اندوته ذونه</p></div>
<div class="m2"><p>عیار قلب و خالص بوته ذونه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا سوته دلان با هم بنالیم</p></div>
<div class="m2"><p>که قدر سوته دل دل سوته ذونه</p></div></div>