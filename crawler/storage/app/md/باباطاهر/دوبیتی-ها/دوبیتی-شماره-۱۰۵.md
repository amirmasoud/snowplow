---
title: >-
    دوبیتی شمارهٔ ۱۰۵
---
# دوبیتی شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>به آهی گنبد خضرا بسوجم</p></div>
<div class="m2"><p>فلک را جمله سر تا پا بسوجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوجم ار نه کارم را بساجی</p></div>
<div class="m2"><p>چه فرمائی بساجی یا بسوجم</p></div></div>