---
title: >-
    دوبیتی شمارهٔ ۷۷
---
# دوبیتی شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>دلی دیرم ولی دیوانه و دنگ</p></div>
<div class="m2"><p>ز دستم شیشهٔ ناموس بر سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین دیوانگی روزی برآیم</p></div>
<div class="m2"><p>که در دامان دلبر برزنم چنگ</p></div></div>