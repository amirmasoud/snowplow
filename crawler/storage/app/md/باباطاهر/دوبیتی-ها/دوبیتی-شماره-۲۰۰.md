---
title: >-
    دوبیتی شمارهٔ ۲۰۰
---
# دوبیتی شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>شبی دیرم زهجرت تار تارو</p></div>
<div class="m2"><p>گرفته ظلمتش لیل و نهارو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خداوندا دلم را روشنی ده</p></div>
<div class="m2"><p>که تا وینم جمال هشت و چارو</p></div></div>