---
title: >-
    دوبیتی شمارهٔ ۳۵۷
---
# دوبیتی شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>به قبرستان گذر کردم صباحی</p></div>
<div class="m2"><p>شنیدم ناله و افغان و آهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدم کله‌ای با خاک می‌گفت</p></div>
<div class="m2"><p>که این دنیا نمی‌ارزد بکاهی</p></div></div>