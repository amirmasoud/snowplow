---
title: >-
    دوبیتی شمارهٔ ۲۶۸
---
# دوبیتی شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>عزیزان از غم و درد جدایی</p></div>
<div class="m2"><p>به چشمانم نمانده روشنائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدرد غربت و هجرم گرفتار</p></div>
<div class="m2"><p>نه یار و همدمی نه آشنائی</p></div></div>