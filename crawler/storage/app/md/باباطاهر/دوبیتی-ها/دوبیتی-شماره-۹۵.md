---
title: >-
    دوبیتی شمارهٔ ۹۵
---
# دوبیتی شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>مو آن دل داده یکتا پرستم</p></div>
<div class="m2"><p>که جام شرک و خود بینی شکستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم طاهر که در بزم محبت</p></div>
<div class="m2"><p>محمد را کمینه چاکرستم</p></div></div>