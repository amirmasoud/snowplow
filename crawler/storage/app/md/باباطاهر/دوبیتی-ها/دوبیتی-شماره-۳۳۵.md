---
title: >-
    دوبیتی شمارهٔ ۳۳۵
---
# دوبیتی شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>سمن زلفا بری چون لاله دیری</p></div>
<div class="m2"><p>ز نرگس ناز در دنباله دیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن رو سه بمهرم بر نیاری</p></div>
<div class="m2"><p>که در سرناز چندین ساله دیری</p></div></div>