---
title: >-
    دوبیتی شمارهٔ ۱۸۶
---
# دوبیتی شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>دلم تنگه ندانم صبر کردن</p></div>
<div class="m2"><p>زدلتنگی بوم راضی بمردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شرم روی ته مو در حجابم</p></div>
<div class="m2"><p>ندانم عرض حالم واته کردن</p></div></div>