---
title: >-
    دوبیتی شمارهٔ ۳۳۹
---
# دوبیتی شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>دل دیوانه‌ام دیوانه‌تر شی</p></div>
<div class="m2"><p>خرابه خانه‌ام ویرانه‌تر شی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشم آهی که گردون را بسوجم</p></div>
<div class="m2"><p>که آه سوته دیلان کارگر شی</p></div></div>