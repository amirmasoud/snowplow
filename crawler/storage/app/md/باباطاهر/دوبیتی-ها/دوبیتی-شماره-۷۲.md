---
title: >-
    دوبیتی شمارهٔ ۷۲
---
# دوبیتی شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>گلی که خود بدادم پیچ و تابش</p></div>
<div class="m2"><p>باشک دیدگانم دادم آبش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین گلشن خدایا کی روا بی</p></div>
<div class="m2"><p>گل از مو دیگری گیرد گلابش</p></div></div>