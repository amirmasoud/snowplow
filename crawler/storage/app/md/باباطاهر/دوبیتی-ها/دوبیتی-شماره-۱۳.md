---
title: >-
    دوبیتی شمارهٔ ۱۳
---
# دوبیتی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>نمیدانم دلم دیوانهٔ کیست</p></div>
<div class="m2"><p>کجا آواره و در خانهٔ کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمیدونم دل سر گشتهٔ مو</p></div>
<div class="m2"><p>اسیر نرگس مستانهٔ کیست</p></div></div>