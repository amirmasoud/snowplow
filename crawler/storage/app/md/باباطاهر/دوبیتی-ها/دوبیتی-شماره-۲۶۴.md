---
title: >-
    دوبیتی شمارهٔ ۲۶۴
---
# دوبیتی شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>فلک در قصد آزارم چرائی</p></div>
<div class="m2"><p>گلم گر نیستی خارم چرائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته که باری ز دوشم بر نداری</p></div>
<div class="m2"><p>میان بار سربارم چرایی</p></div></div>