---
title: >-
    دوبیتی شمارهٔ ۳۴۷
---
# دوبیتی شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>دل شاد از دل زارش خبر نی</p></div>
<div class="m2"><p>تن سالم زبیمارش خبر نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه تقصیره که این رسم قدیمه</p></div>
<div class="m2"><p>که آزاد از گرفتارش خبر نی</p></div></div>