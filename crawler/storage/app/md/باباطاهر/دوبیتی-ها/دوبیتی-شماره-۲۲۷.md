---
title: >-
    دوبیتی شمارهٔ ۲۲۷
---
# دوبیتی شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>دلت ای سنگدل بر ما نسوجه</p></div>
<div class="m2"><p>عجب نبود اگر خارا نسوجه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوجم تا بسوجانم دلت را</p></div>
<div class="m2"><p>در آذر چوب تر تنها نسوجه</p></div></div>