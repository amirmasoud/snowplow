---
title: >-
    دوبیتی شمارهٔ ۸۴
---
# دوبیتی شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>بشم واشم که تا یاری گره دل</p></div>
<div class="m2"><p>به بختم گریه و زاری گره دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگردی و نجوئی یار دیگر</p></div>
<div class="m2"><p>که از جان و دلت یاری گره دل</p></div></div>