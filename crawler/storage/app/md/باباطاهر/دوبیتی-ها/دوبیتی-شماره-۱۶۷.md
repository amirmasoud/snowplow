---
title: >-
    دوبیتی شمارهٔ ۱۶۷
---
# دوبیتی شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>نمیدانم که سرگردان چرایم</p></div>
<div class="m2"><p>گهی نالان گهی گریان چرایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه دردی بدوران یافت درمان</p></div>
<div class="m2"><p>ندانم مو که بیدرمان چرایم</p></div></div>