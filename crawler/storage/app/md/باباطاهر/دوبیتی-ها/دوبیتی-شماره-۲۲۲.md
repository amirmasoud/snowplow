---
title: >-
    دوبیتی شمارهٔ ۲۲۲
---
# دوبیتی شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>دل مو دایم اندر ماتم ته</p></div>
<div class="m2"><p>بدل پیوسته بی‌درد و غم ته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه پرسی که چرا قدت ببوخم</p></div>
<div class="m2"><p>خم قدم از آن پیچ و خم ته</p></div></div>