---
title: >-
    دوبیتی شمارهٔ ۳۷
---
# دوبیتی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>هر آنکس عاشق است از جان نترسد</p></div>
<div class="m2"><p>یقین از بند و از زندان نترسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل عاشق بود گرگ گرسنه</p></div>
<div class="m2"><p>که گرگ از هی هی چوپان نترسد</p></div></div>