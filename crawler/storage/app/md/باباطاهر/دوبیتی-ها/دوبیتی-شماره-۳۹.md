---
title: >-
    دوبیتی شمارهٔ ۳۹
---
# دوبیتی شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>پسندی خوار و زارم تا کی و چند</p></div>
<div class="m2"><p>پریشان روزگارم تا کی و چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته که باری ز دوشم برنگیری</p></div>
<div class="m2"><p>گری سربار بارم تا کی و چند</p></div></div>