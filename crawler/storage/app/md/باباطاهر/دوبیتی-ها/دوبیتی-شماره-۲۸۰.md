---
title: >-
    دوبیتی شمارهٔ ۲۸۰
---
# دوبیتی شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>بدریای غمت دل غوطه‌ور بی</p></div>
<div class="m2"><p>مرا داغ فراقت بر جگر بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مژگان خدنگت خورده‌ام تیر</p></div>
<div class="m2"><p>که هر دم سوج دل زان بیشتر بی</p></div></div>