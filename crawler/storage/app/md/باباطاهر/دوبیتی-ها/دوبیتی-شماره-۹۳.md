---
title: >-
    دوبیتی شمارهٔ ۹۳
---
# دوبیتی شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>دو زلفانت گرم تار ربابم</p></div>
<div class="m2"><p>چه میخواهی ازین حال خرابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته که با مو سر یاری نداری</p></div>
<div class="m2"><p>چرا هر نیمه شو آیی بخوابم</p></div></div>