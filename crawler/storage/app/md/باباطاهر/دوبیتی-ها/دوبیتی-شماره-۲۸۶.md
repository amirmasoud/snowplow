---
title: >-
    دوبیتی شمارهٔ ۲۸۶
---
# دوبیتی شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>مدامم دل پر از خون جگر بی</p></div>
<div class="m2"><p>چو شمع آتش بجان و دیده تر بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشینم بر سر راهت شو و روز</p></div>
<div class="m2"><p>که تا روزی ترا بر مو گذر بی</p></div></div>