---
title: >-
    دوبیتی شمارهٔ ۲۴۸
---
# دوبیتی شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>دل ارمهرت نورزه بر چه ارزه</p></div>
<div class="m2"><p>گل است آندل که مهر تو نورزه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریبانی که از عشقت شود چاک</p></div>
<div class="m2"><p>بیک عالم گریبان وابیرزه</p></div></div>