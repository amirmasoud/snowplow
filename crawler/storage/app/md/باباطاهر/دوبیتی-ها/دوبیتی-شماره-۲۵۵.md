---
title: >-
    دوبیتی شمارهٔ ۲۵۵
---
# دوبیتی شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>چو مو یک سوته دل پروانه‌ای نه</p></div>
<div class="m2"><p>به عالم همچو مو دیوانه‌ای نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه مارون و مورون لانه دیرن</p></div>
<div class="m2"><p>مو دیوانه را ویرانه‌ای نه</p></div></div>