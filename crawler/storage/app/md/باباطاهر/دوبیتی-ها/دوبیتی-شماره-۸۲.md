---
title: >-
    دوبیتی شمارهٔ ۸۲
---
# دوبیتی شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>خدایا داد از این دل داد از این دل</p></div>
<div class="m2"><p>نگشتم یک زمان من شاد از این دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فردا داد خواهان داد خواهند</p></div>
<div class="m2"><p>بر آرم من دو صد فریاد از این دل</p></div></div>