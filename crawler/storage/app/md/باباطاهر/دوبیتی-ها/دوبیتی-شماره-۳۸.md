---
title: >-
    دوبیتی شمارهٔ ۳۸
---
# دوبیتی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>بلا رمزی ز بالای ته باشد</p></div>
<div class="m2"><p>جنون سری ز سودای ته باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بصورت آفرینم این گمان بی</p></div>
<div class="m2"><p>که پنهان در تماشای ته باشد</p></div></div>