---
title: >-
    دوبیتی شمارهٔ ۱۸
---
# دوبیتی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>سیاهی دو چشمانت مرا کشت</p></div>
<div class="m2"><p>درازی دو زلفانت مرا کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به قتلم حاجت تیر و کمان نیست</p></div>
<div class="m2"><p>خم ابرو و مژگانت مرا کشت</p></div></div>