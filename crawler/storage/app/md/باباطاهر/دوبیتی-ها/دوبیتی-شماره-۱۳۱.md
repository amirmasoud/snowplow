---
title: >-
    دوبیتی شمارهٔ ۱۳۱
---
# دوبیتی شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>فلک بر هم زدی آخر اساسم</p></div>
<div class="m2"><p>زدی بر خمرهٔ نیلی لباسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر داری برات از قصد جانم</p></div>
<div class="m2"><p>بکن آخر ازین دنیا اساسم</p></div></div>