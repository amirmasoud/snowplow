---
title: >-
    دوبیتی شمارهٔ ۱۱۳
---
# دوبیتی شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>غم عالم همه کردی ببارم</p></div>
<div class="m2"><p>مگر مو لوک مست سر قطارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهارم کردی و دادی به ناکس</p></div>
<div class="m2"><p>فزودی هر زمان باری ببارم</p></div></div>