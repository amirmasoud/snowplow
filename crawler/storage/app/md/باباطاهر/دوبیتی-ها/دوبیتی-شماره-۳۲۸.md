---
title: >-
    دوبیتی شمارهٔ ۳۲۸
---
# دوبیتی شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>بمو واجی چرا ته بیقراری</p></div>
<div class="m2"><p>چو گل پروردهٔ باد بهاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا گردی بکوه و دشت و صحرا</p></div>
<div class="m2"><p>بجان او ندارم اختیاری</p></div></div>