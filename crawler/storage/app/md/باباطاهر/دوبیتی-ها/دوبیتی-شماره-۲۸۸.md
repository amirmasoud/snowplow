---
title: >-
    دوبیتی شمارهٔ ۲۸۸
---
# دوبیتی شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>هر آن باغی که نخلش سر بدر بی</p></div>
<div class="m2"><p>مدامش باغبون خونین جگر بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بباید کندنش از بیخ و از بن</p></div>
<div class="m2"><p>اگر بارش همه لعل و گهر بی</p></div></div>