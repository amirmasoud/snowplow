---
title: >-
    دوبیتی شمارهٔ ۲۷۸
---
# دوبیتی شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>بسوی باغ و بستان لاله وابی</p></div>
<div class="m2"><p>همه موها مثال ژاله وا بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر سوی خراسان کاروان را</p></div>
<div class="m2"><p>رهانم مو سوی بنگاله وا بی</p></div></div>