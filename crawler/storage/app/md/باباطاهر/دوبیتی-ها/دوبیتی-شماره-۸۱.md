---
title: >-
    دوبیتی شمارهٔ ۸۱
---
# دوبیتی شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>مو ام آن آذرین مرغی که فی‌الحال</p></div>
<div class="m2"><p>بسوجم عالم ار برهم زنم بال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصور گر کشد نقشم به گلشن</p></div>
<div class="m2"><p>بسوجه گلشن از تاثیر تمثال</p></div></div>