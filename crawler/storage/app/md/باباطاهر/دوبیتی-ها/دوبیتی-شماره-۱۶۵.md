---
title: >-
    دوبیتی شمارهٔ ۱۶۵
---
# دوبیتی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>اگر جسمم بسوزی سوته خواهم</p></div>
<div class="m2"><p>اگر چشمم بدوزی دوته خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر باغم بری تا گل بچینم</p></div>
<div class="m2"><p>گلی همرنگ و همبوی ته خواهم</p></div></div>