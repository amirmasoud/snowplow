---
title: >-
    دوبیتی شمارهٔ ۲۷۶
---
# دوبیتی شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>بدام دلبری دل مبتلا بی</p></div>
<div class="m2"><p>که هجرانش بلا وصلش بلا بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین ویرانه دل جز خون ندیدم</p></div>
<div class="m2"><p>نه دل گویی که دشت کربلا بی</p></div></div>