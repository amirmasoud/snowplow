---
title: >-
    دوبیتی شمارهٔ ۱۹۴
---
# دوبیتی شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>وای از روزی که قاضیمان خدا بو</p></div>
<div class="m2"><p>سر پل صراطم ماجرا بو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنوبت بگذرند پیر و جوانان</p></div>
<div class="m2"><p>وای از آندم که نوبت زان ما بو</p></div></div>