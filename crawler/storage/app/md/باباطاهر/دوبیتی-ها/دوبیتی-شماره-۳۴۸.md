---
title: >-
    دوبیتی شمارهٔ ۳۴۸
---
# دوبیتی شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>ته که دور از منی دل در برم نی</p></div>
<div class="m2"><p>هوایی غیر وصلت در سرم نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجانت دلبرا کز هر دو عالم</p></div>
<div class="m2"><p>تمنای دگر جز دلبرم نی</p></div></div>