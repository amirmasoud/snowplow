---
title: >-
    دوبیتی شمارهٔ ۱۷۳
---
# دوبیتی شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>بوره سوته دلان با ما بنالیم</p></div>
<div class="m2"><p>زدست یار بی پروا بنالیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشیم با بلبل شیدا به گلشن</p></div>
<div class="m2"><p>اگر بلبل نناله ما بنالیم</p></div></div>