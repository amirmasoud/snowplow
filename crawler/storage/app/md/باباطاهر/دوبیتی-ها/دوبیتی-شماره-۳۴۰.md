---
title: >-
    دوبیتی شمارهٔ ۳۴۰
---
# دوبیتی شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>الهی ای فلک چون مو زبون شی</p></div>
<div class="m2"><p>دلت همچون دل مو غرق خون شی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر یک لحظه ام بی غم بوینی</p></div>
<div class="m2"><p>یقین دانم کزین غم سرنگون شی</p></div></div>