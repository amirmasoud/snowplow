---
title: >-
    دوبیتی شمارهٔ ۳۴۴
---
# دوبیتی شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>اگر دل دلبری دلبر کدامی</p></div>
<div class="m2"><p>وگر دلبر دلی دل را چه نامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دلبر بهم آمیته وینم</p></div>
<div class="m2"><p>ندانم دل که و دلبر کدامی</p></div></div>