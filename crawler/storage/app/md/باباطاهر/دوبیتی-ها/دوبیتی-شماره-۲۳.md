---
title: >-
    دوبیتی شمارهٔ ۲۳
---
# دوبیتی شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ز دست دیده و دل هر دو فریاد</p></div>
<div class="m2"><p>که هر چه دیده بیند دل کند یاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسازم خنجری نیشش ز فولاد</p></div>
<div class="m2"><p>زنم بر دیده تا دل گردد آزاد</p></div></div>