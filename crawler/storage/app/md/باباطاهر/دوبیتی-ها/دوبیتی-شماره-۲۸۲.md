---
title: >-
    دوبیتی شمارهٔ ۲۸۲
---
# دوبیتی شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>ز آهم هفت گردون پر شرر بی</p></div>
<div class="m2"><p>زمژگانم روان خون جگر بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته که هرگز دلت از غم نسوجه</p></div>
<div class="m2"><p>کجا از سوته دیلانت خبر بی</p></div></div>