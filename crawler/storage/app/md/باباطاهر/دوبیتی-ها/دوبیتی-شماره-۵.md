---
title: >-
    دوبیتی شمارهٔ ۵
---
# دوبیتی شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>دلی دیرم خریدار محبت</p></div>
<div class="m2"><p>کز او گرم است بازار محبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لباسی دوختم بر قامت دل</p></div>
<div class="m2"><p>زپود محنت و تار محبت</p></div></div>