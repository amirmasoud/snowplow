---
title: >-
    دوبیتی شمارهٔ ۳۲۹
---
# دوبیتی شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>همه روزم فغان و بیقراری</p></div>
<div class="m2"><p>شوان بیداری و فریاد و زاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمو سوجه دل هر دور و نزدیک</p></div>
<div class="m2"><p>ته از سنگین دلی پروا نداری</p></div></div>