---
title: >-
    دوبیتی شمارهٔ ۱۷۱
---
# دوبیتی شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>بیا تا دست ازین عالم بداریم</p></div>
<div class="m2"><p>بیا تا پای دل از گل برآریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا تا بردباری پیشه سازیم</p></div>
<div class="m2"><p>بیا تا تخم نیکوئی بکاریم</p></div></div>