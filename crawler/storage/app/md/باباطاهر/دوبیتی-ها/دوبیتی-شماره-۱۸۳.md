---
title: >-
    دوبیتی شمارهٔ ۱۸۳
---
# دوبیتی شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>ز یاد خود بیا پروا کنیمان</p></div>
<div class="m2"><p>ازو کو التجا وا که بریمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیه این تاب داره تا مو دارم</p></div>
<div class="m2"><p>نداره تاب این سام نریمان</p></div></div>