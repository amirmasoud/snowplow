---
title: >-
    دوبیتی شمارهٔ ۱۷۵
---
# دوبیتی شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>برندم همچو یوسف گر به زندان</p></div>
<div class="m2"><p>ویا نالم زغم چون مستمندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر صد باغبان خصمی نماید</p></div>
<div class="m2"><p>مدام آیم به گلزار تو خندان</p></div></div>