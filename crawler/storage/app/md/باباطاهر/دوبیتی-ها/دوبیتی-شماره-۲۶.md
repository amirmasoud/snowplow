---
title: >-
    دوبیتی شمارهٔ ۲۶
---
# دوبیتی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>دلم بی وصل ته شادی مبیناد</p></div>
<div class="m2"><p>زدرد و محنت آزادی مبیناد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب آباد دل بی مقدم تو</p></div>
<div class="m2"><p>الهی هرگز آبادی مبیناد</p></div></div>