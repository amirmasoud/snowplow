---
title: >-
    دوبیتی شمارهٔ ۱۴
---
# دوبیتی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ته دوری از برم دل در برم نیست</p></div>
<div class="m2"><p>هوای دیگری اندر سرم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجان دلبرم کز هر دو عالم</p></div>
<div class="m2"><p>تمنای دگر جز دلبرم نیست</p></div></div>