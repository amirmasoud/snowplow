---
title: >-
    دوبیتی شمارهٔ ۷۳
---
# دوبیتی شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>قضا رمزی زچشمان خمارش</p></div>
<div class="m2"><p>قدر سری ز زلف مشگبارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه و مهر آیتی ز آنروی زیبا</p></div>
<div class="m2"><p>نکویان جهان آئینه دارش</p></div></div>