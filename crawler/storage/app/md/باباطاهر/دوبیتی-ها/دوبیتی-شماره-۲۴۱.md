---
title: >-
    دوبیتی شمارهٔ ۲۴۱
---
# دوبیتی شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>دلم میل گل باغ ته دیره</p></div>
<div class="m2"><p>درون سینه‌ام داغ ته دیره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشم آلاله زاران لاله چینم</p></div>
<div class="m2"><p>وینم آلاله هم داغ ته دیره</p></div></div>