---
title: >-
    دوبیتی شمارهٔ ۱۹۹
---
# دوبیتی شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>به والله که جانانم تویی تو</p></div>
<div class="m2"><p>بسلطان عرب جانم تویی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمیدونم که چونم یا که چندم</p></div>
<div class="m2"><p>همی دونم که درمانم تویی تو</p></div></div>