---
title: >-
    دوبیتی شمارهٔ ۱۳۹
---
# دوبیتی شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>بیا یک شو منور کن اطاقم</p></div>
<div class="m2"><p>مهل در محنت و درد فراقم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طاق جفت ابروی تو سوگند</p></div>
<div class="m2"><p>که همجفت غمم تا از تو طاقم</p></div></div>