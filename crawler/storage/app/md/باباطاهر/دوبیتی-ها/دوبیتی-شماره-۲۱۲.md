---
title: >-
    دوبیتی شمارهٔ ۲۱۲
---
# دوبیتی شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>بی ته هر شو سرم بر بالش آیو</p></div>
<div class="m2"><p>چو نی از استخوانم نالش آیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب هجران بجای اشک چشمم</p></div>
<div class="m2"><p>ز مژگان پاره‌های آتش آیو</p></div></div>