---
title: >-
    دوبیتی شمارهٔ ۳۴۱
---
# دوبیتی شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>دل بی عشق را افسردن اولی</p></div>
<div class="m2"><p>هر که دردی نداره مردن اولی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنی که نیست ثابت در ره عشق</p></div>
<div class="m2"><p>ذره ذره به آتش سوتن اولی</p></div></div>