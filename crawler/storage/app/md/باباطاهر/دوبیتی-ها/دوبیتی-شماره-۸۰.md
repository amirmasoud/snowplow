---
title: >-
    دوبیتی شمارهٔ ۸۰
---
# دوبیتی شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>حرامم بی ته بی آلاله و گل</p></div>
<div class="m2"><p>حرامم بی ته بی آواز بلبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرامم بی اگر بی ته نشینم</p></div>
<div class="m2"><p>کشم در پای گلبن ساغر مل</p></div></div>