---
title: >-
    دوبیتی شمارهٔ ۷۹
---
# دوبیتی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>دلی دیرم چو مو دیوانه و دنگ</p></div>
<div class="m2"><p>زده آئینه هر نام بر سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمو واجند که بی نام و ننگی</p></div>
<div class="m2"><p>هر آن یارش تویی چه نام و چه ننگ</p></div></div>