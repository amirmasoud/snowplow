---
title: >-
    دوبیتی شمارهٔ ۶۲
---
# دوبیتی شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>اگر شیری اگر میری اگر مور</p></div>
<div class="m2"><p>گذر باید کنی آخر لب گور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا رحمی بجان خویشتن کن</p></div>
<div class="m2"><p>که مورانت نهند خوان و کنند سور</p></div></div>