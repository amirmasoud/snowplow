---
title: >-
    دوبیتی شمارهٔ ۲۹۶
---
# دوبیتی شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>چه خوش بی وصلت ای مه امشبک بی</p></div>
<div class="m2"><p>مرا وصل تو آرام دلک بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمهرت ای مه شیرین چالاک</p></div>
<div class="m2"><p>مدامم دست حسرت بر سرک بی</p></div></div>