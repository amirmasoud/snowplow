---
title: >-
    دوبیتی شمارهٔ ۱۶۴
---
# دوبیتی شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>الهی دشمنت را خسته وینم</p></div>
<div class="m2"><p>به سینه اش خنجری تا دسته وینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر شو آیم احوالش بپرسم</p></div>
<div class="m2"><p>سحر آیم مزارش بسته وینم</p></div></div>