---
title: >-
    دوبیتی شمارهٔ ۲۱۶
---
# دوبیتی شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>بیته گلشن به چشمم گلخن آیو</p></div>
<div class="m2"><p>واته گلخن به چشمم گلشن آیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلم ته گلبنم ته گلشنم ته</p></div>
<div class="m2"><p>که واته مرده را جان بر تن آیو</p></div></div>