---
title: >-
    دوبیتی شمارهٔ ۲۱۹
---
# دوبیتی شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>غم عشقت ز گنج رایگان به</p></div>
<div class="m2"><p>وصال تو ز عمر جاودان به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفی از خاک کویت در حقیقت</p></div>
<div class="m2"><p>خدا دونه که از ملک جهان به</p></div></div>