---
title: >-
    دوبیتی شمارهٔ ۲۲
---
# دوبیتی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>اگر زرین کلاهی عاقبت هیچ</p></div>
<div class="m2"><p>اگر خود پادشاهی عاقبت هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ملک سلیمانت ببخشند</p></div>
<div class="m2"><p>در آخر خاک راهی عاقبت هیچ</p></div></div>