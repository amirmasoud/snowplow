---
title: >-
    دوبیتی شمارهٔ ۳۰۵
---
# دوبیتی شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>نپنداری که زندان خوشترم بی</p></div>
<div class="m2"><p>سرم بو گوی میدان خوشترم بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گلخن تار و تاریکه به چشمم</p></div>
<div class="m2"><p>گلستان بی ته زندان خوشترم بی</p></div></div>