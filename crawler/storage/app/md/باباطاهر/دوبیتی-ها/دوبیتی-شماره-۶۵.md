---
title: >-
    دوبیتی شمارهٔ ۶۵
---
# دوبیتی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>جره بازی بدم رفتم به نخجیر</p></div>
<div class="m2"><p>سبک دستی بزد بر بال من تیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو غافل مچر در کوهساران</p></div>
<div class="m2"><p>هران غافل چرد غافل خورد تیر</p></div></div>