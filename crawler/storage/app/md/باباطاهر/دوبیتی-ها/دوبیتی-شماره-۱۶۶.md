---
title: >-
    دوبیتی شمارهٔ ۱۶۶
---
# دوبیتی شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>بیا سوته دلان گردهم آئیم</p></div>
<div class="m2"><p>سخنها واکریم غم وانمائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترازو آوریم غمها بسنجیم</p></div>
<div class="m2"><p>هر آن سوته تریم وزنین تر آئیم</p></div></div>