---
title: >-
    دوبیتی شمارهٔ ۳۰۴
---
# دوبیتی شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>به لامردم مکان دلبرم بی</p></div>
<div class="m2"><p>سخنهای خوشش تاج سرم بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شاهم ببخشد ملک شیراز</p></div>
<div class="m2"><p>همان بهتر که دلبر در برم بی</p></div></div>