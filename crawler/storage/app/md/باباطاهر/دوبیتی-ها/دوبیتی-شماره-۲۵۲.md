---
title: >-
    دوبیتی شمارهٔ ۲۵۲
---
# دوبیتی شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>به والله و به بالله و به تالله</p></div>
<div class="m2"><p>قسم بر آیهٔ نصر من الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دست از دامنت من بر ندارم</p></div>
<div class="m2"><p>اگر کشته شوم الحکم لله</p></div></div>