---
title: >-
    دوبیتی شمارهٔ ۳۲۴
---
# دوبیتی شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>یقینم حاصله که هرزه گردی</p></div>
<div class="m2"><p>ازین گردش که داری برنگردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بروی مو ببستی هر رهی را</p></div>
<div class="m2"><p>بدین عادت که داری کی ته مردی</p></div></div>