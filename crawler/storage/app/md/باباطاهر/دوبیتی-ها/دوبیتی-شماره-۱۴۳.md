---
title: >-
    دوبیتی شمارهٔ ۱۴۳
---
# دوبیتی شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>دلم زار و حزینه چون ننالم</p></div>
<div class="m2"><p>وجودم آتشینه چون ننالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمو واجن که طاهر چند نالی</p></div>
<div class="m2"><p>چو مرگم در کمینه چون ننالم</p></div></div>