---
title: >-
    دوبیتی شمارهٔ ۲۷۵
---
# دوبیتی شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>عاشق آن به که دایم در بلا بی</p></div>
<div class="m2"><p>ایوب آسا به کرمان مبتلا بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن آسا بدستش کاسهٔ زهر</p></div>
<div class="m2"><p>حسین آسا بدشت کربلا بی</p></div></div>