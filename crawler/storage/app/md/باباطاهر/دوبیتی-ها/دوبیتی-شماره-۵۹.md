---
title: >-
    دوبیتی شمارهٔ ۵۹
---
# دوبیتی شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>الاله کوهسارانم تویی یار</p></div>
<div class="m2"><p>بنوشه جو کنارانم تویی یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الاله کوهساران هفته‌ای بی</p></div>
<div class="m2"><p>امید روزگارانم تویی یار</p></div></div>