---
title: >-
    دوبیتی شمارهٔ ۲۱۴
---
# دوبیتی شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>سحرگان که بلبل بر گل آیو</p></div>
<div class="m2"><p>بدامان اشک چشمم گل گل آیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روم در پای گل افغان کنم سر</p></div>
<div class="m2"><p>که هر سوته دلی در غلغل آیو</p></div></div>