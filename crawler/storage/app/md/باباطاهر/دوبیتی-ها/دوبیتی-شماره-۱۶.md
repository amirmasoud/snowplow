---
title: >-
    دوبیتی شمارهٔ ۱۶
---
# دوبیتی شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>خرم کوه و خرم صحرا خرم دشت</p></div>
<div class="m2"><p>خرم آنانکه این آلالیان کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی هند و بسی شند و بسی یند</p></div>
<div class="m2"><p>همان کوه و همان صحرا همان دشت</p></div></div>