---
title: >-
    دوبیتی شمارهٔ ۱۵۹
---
# دوبیتی شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>شبی خواهم که پیغمبر ببینم</p></div>
<div class="m2"><p>دمی با ساقی کوثر نشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگیرم در بغل قبر رضا را</p></div>
<div class="m2"><p>در آن گلشن گل شادی بچینم</p></div></div>