---
title: >-
    دوبیتی شمارهٔ ۱۰۷
---
# دوبیتی شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>مو که چون اشتران قانع به خارم</p></div>
<div class="m2"><p>جهازم چوب و خرواری ببارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدین مزد قلیل و رنج بسیار</p></div>
<div class="m2"><p>هنوز از روی مالک شرمسارم</p></div></div>