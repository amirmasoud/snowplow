---
title: >-
    دوبیتی شمارهٔ ۳۶۲
---
# دوبیتی شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>ز دل بیرون نبجتم ناله نایی</p></div>
<div class="m2"><p>ز مژگان تر مو ژاله نایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوی نایه که مو خوابت بوینم</p></div>
<div class="m2"><p>به بخت مو به چشم لاله نایی</p></div></div>