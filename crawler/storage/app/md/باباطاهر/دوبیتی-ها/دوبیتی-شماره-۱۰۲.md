---
title: >-
    دوبیتی شمارهٔ ۱۰۲
---
# دوبیتی شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>الهی ار بواجم ور نواجم</p></div>
<div class="m2"><p>ته دانی حاجتم را مو چه واجم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بنوازیم حاجت روا بی</p></div>
<div class="m2"><p>وگر محروم سازی مو چه ساجم</p></div></div>