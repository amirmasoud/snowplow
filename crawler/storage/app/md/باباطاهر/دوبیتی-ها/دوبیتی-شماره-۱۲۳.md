---
title: >-
    دوبیتی شمارهٔ ۱۲۳
---
# دوبیتی شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>زعشقت آتشی در بوته دیرم</p></div>
<div class="m2"><p>در آن آتش دل و جان سوته دیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سگت ار پا نهد بر چشمم ایدوست</p></div>
<div class="m2"><p>بمژگان خاک پایش روته دیرم</p></div></div>