---
title: >-
    دوبیتی شمارهٔ ۲۹۱
---
# دوبیتی شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>خوشا آندل که از غم بهره‌ور بی</p></div>
<div class="m2"><p>بر آندل وای کز غم بی‌خبر بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ته که هرگز نسوته دیلت از غم</p></div>
<div class="m2"><p>کجا از سوته دیلانت خبر بی</p></div></div>