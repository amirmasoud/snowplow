---
title: >-
    دوبیتی شمارهٔ ۵۸
---
# دوبیتی شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>زدل نقش جمالت در نشی یار</p></div>
<div class="m2"><p>خیال خط و خالت در نشی یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژه سازم بدور دیده پرچین</p></div>
<div class="m2"><p>که تا وینم خیالت در نشی یار</p></div></div>