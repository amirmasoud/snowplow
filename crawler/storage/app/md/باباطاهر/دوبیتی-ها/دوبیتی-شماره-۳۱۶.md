---
title: >-
    دوبیتی شمارهٔ ۳۱۶
---
# دوبیتی شمارهٔ ۳۱۶

<div class="b" id="bn1"><div class="m1"><p>من آن شمعم که اشکم آتشین بی</p></div>
<div class="m2"><p>که هر سوته دلی حالش همین بی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه شب گریم و نالم همه روز</p></div>
<div class="m2"><p>بیته شامم چنان روزم چنین بی</p></div></div>