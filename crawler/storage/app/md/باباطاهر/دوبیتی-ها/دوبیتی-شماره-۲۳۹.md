---
title: >-
    دوبیتی شمارهٔ ۲۳۹
---
# دوبیتی شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>سر سرگشته‌ام سامان نداره</p></div>
<div class="m2"><p>دل خون گشته‌ام درمان نداره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کافر مذهبی دل بسته دیرم</p></div>
<div class="m2"><p>که در هر مذهبی ایمان نداره</p></div></div>