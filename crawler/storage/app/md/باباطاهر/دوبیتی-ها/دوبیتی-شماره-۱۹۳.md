---
title: >-
    دوبیتی شمارهٔ ۱۹۳
---
# دوبیتی شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>بیته یک شو دلم بی غم نمی‌بو</p></div>
<div class="m2"><p>که آن دلبر دمی همدم نمی‌بو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران رحمت حق باد بر غم</p></div>
<div class="m2"><p>زمانی از دل ما کم نمی بو</p></div></div>