---
title: >-
    دوبیتی شمارهٔ ۲۶۹
---
# دوبیتی شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>به هر شام و سحر گریم بکوئی</p></div>
<div class="m2"><p>که جاری سازم از هر دیده جوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مو آن بی طالعم در باغ عالم</p></div>
<div class="m2"><p>که گل کارم بجایش خار روئی</p></div></div>