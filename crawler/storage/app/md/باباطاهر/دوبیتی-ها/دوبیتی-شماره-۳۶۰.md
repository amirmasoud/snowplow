---
title: >-
    دوبیتی شمارهٔ ۳۶۰
---
# دوبیتی شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>نگار تازه خیز ما کجایی</p></div>
<div class="m2"><p>بچشمان سرمه ریز ما کجایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس بر سینهٔ طاهر رسیده</p></div>
<div class="m2"><p>دم رفتن عزیز ما کجایی</p></div></div>