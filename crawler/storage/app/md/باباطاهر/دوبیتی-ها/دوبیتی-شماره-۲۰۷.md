---
title: >-
    دوبیتی شمارهٔ ۲۰۷
---
# دوبیتی شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>بی ته اشکم ز مژگان تر آیو</p></div>
<div class="m2"><p>بی ته نخل امیدم نی بر آیو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ته در کنج تنهائی شب و روز</p></div>
<div class="m2"><p>نشینم تا که عمرم بر سر آیو</p></div></div>