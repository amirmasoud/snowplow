---
title: >-
    دوبیتی شمارهٔ ۳۴۹
---
# دوبیتی شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>دلی چون مو بغم اندوته ای نی</p></div>
<div class="m2"><p>زری چون جان مو در بوته‌ای نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز شمعم ببالین همدمی نه</p></div>
<div class="m2"><p>که یار سوته دل جز سوته‌ای نی</p></div></div>