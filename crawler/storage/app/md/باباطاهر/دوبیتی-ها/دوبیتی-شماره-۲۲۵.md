---
title: >-
    دوبیتی شمارهٔ ۲۲۵
---
# دوبیتی شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>دلی دیرم چو مرغ پا شکسته</p></div>
<div class="m2"><p>چو کشتی بر لب دریا نشسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گویی طاهرا چون تار بنواز</p></div>
<div class="m2"><p>صدا چون میدهد تار گسسته</p></div></div>