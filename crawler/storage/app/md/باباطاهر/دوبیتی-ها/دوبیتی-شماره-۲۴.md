---
title: >-
    دوبیتی شمارهٔ ۲۴
---
# دوبیتی شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>گیج و ویجم که کافر گیج میراد</p></div>
<div class="m2"><p>چنان گیجم که کافر هم موی ناد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر این آئین که مو را جان و دل داد</p></div>
<div class="m2"><p>شمع و پروانه را پرویج میداد</p></div></div>