---
title: >-
    رباعی شماره ۶۶
---
# رباعی شماره ۶۶

<div class="b" id="bn1"><div class="m1"><p>هر چند که پوشیده ترم، عورترم</p></div>
<div class="m2"><p>هر چند که نزدیک‌ترم، دورترم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبحان اللّه در آن جمال از حیرت</p></div>
<div class="m2"><p>هر چند که بیننده‌ترم، کورترم</p></div></div>