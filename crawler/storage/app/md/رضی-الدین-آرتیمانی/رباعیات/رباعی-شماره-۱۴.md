---
title: >-
    رباعی شماره ۱۴
---
# رباعی شماره ۱۴

<div class="b" id="bn1"><div class="m1"><p>با درویشان کبر خود اندیش بد است</p></div>
<div class="m2"><p>با خویش بدست آنکه به درویش بد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه بدم بخویش، از خوبی خویش</p></div>
<div class="m2"><p>با من خوب است آنکه بدرویش بد است</p></div></div>