---
title: >-
    رباعی شماره ۹
---
# رباعی شماره ۹

<div class="b" id="bn1"><div class="m1"><p>آهم ز فراز آسمٰانها بگذشت</p></div>
<div class="m2"><p>اشکم ز محیط هفت دریا بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که به کار سازیت برخیزم</p></div>
<div class="m2"><p>بنشین بنشین که کار از اینها بگذشت</p></div></div>