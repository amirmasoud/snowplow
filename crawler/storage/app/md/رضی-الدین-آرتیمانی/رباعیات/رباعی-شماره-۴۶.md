---
title: >-
    رباعی شماره ۴۶
---
# رباعی شماره ۴۶

<div class="b" id="bn1"><div class="m1"><p>تا گلگون اشک و چهره کاهی نشود</p></div>
<div class="m2"><p>دل مشرق انوار الهی نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سالک که ز سر خویش واقف گردد</p></div>
<div class="m2"><p>او عارف اسرار کماهی نشود</p></div></div>