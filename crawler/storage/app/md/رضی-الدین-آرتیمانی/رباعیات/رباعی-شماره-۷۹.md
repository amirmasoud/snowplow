---
title: >-
    رباعی شماره ۷۹
---
# رباعی شماره ۷۹

<div class="b" id="bn1"><div class="m1"><p>صد شکر که نیستم من از بی‌خبران</p></div>
<div class="m2"><p>گه مست ز وصلم و گهی از هجران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانشمندان تمام گریٰان بر من</p></div>
<div class="m2"><p>خندان من دیوانه به دانشمندان</p></div></div>