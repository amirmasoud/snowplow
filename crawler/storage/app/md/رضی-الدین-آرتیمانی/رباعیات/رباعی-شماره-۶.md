---
title: >-
    رباعی شماره ۶
---
# رباعی شماره ۶

<div class="b" id="bn1"><div class="m1"><p>رفتم بر آن نگار سیمین غبغب</p></div>
<div class="m2"><p>گفتم بسفر می‌روم ای مه امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روئی چو قمر،زلف چو عقرب بنمود</p></div>
<div class="m2"><p>یعنی که مرو هست قمر در عقرب</p></div></div>