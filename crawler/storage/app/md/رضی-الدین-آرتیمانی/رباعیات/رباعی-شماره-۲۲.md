---
title: >-
    رباعی شماره ۲۲
---
# رباعی شماره ۲۲

<div class="b" id="bn1"><div class="m1"><p>هر دل که رهین تن بود او دل نیست</p></div>
<div class="m2"><p>در عالم دل خبر ز آب و گل نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راهی نبود که او بمنزل نرسد</p></div>
<div class="m2"><p>جز راه محبت، که در او منزل نیست</p></div></div>