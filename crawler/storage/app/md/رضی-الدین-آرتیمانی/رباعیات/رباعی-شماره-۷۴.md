---
title: >-
    رباعی شماره ۷۴
---
# رباعی شماره ۷۴

<div class="b" id="bn1"><div class="m1"><p>صد شکر که آشفته سر و دستارم</p></div>
<div class="m2"><p>بر گشته ز دوست خلوت و بازارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاصل که رسیده تا بجائی کارم</p></div>
<div class="m2"><p>کزیاد رود اگر بیادش آرم</p></div></div>