---
title: >-
    رباعی شماره ۵۱
---
# رباعی شماره ۵۱

<div class="b" id="bn1"><div class="m1"><p>تا چند رضی به گیر و دارت دارند</p></div>
<div class="m2"><p>گیرم بخزان چو نوبهارت دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر خیز رضی سنگ گرانی موقوف</p></div>
<div class="m2"><p>کاینده و رفته انتظارت دارند</p></div></div>