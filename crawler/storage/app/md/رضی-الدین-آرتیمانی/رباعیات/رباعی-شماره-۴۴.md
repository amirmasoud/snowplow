---
title: >-
    رباعی شماره ۴۴
---
# رباعی شماره ۴۴

<div class="b" id="bn1"><div class="m1"><p>تا در ره عشق پای از سر نشود</p></div>
<div class="m2"><p>ایمان با کفر ما برابر نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا آینه از آه منور نشود</p></div>
<div class="m2"><p>بر روی کسی گشاده این در نشود</p></div></div>