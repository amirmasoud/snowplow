---
title: >-
    رباعی شماره ۲۳
---
# رباعی شماره ۲۳

<div class="b" id="bn1"><div class="m1"><p>عشق است که بی زلزله وغلغله نیست</p></div>
<div class="m2"><p>گر ره نبری بجان جای گله نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این راه نرفت هر که سر در ننهاد</p></div>
<div class="m2"><p>گویا که در این قافله سر قافله نیست</p></div></div>