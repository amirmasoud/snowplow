---
title: >-
    رباعی شماره ۹۹
---
# رباعی شماره ۹۹

<div class="b" id="bn1"><div class="m1"><p>لعل میگون و چشم فتان داری</p></div>
<div class="m2"><p>کاکل آشفته، مو پریشان داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه بحسن ناز و طوفان داری</p></div>
<div class="m2"><p>هر سو هر دم هزار قربان داری</p></div></div>