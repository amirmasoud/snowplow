---
title: >-
    رباعی شماره ۱۰
---
# رباعی شماره ۱۰

<div class="b" id="bn1"><div class="m1"><p>سر کردهٔ اهل دانش و دید اینست</p></div>
<div class="m2"><p>شایسته تخت و تاج جمشید این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید هزار طعنه دارد با بدر</p></div>
<div class="m2"><p>بدری که زند طعنه بخورشید این است</p></div></div>