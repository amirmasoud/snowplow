---
title: >-
    رباعی شماره ۵
---
# رباعی شماره ۵

<div class="b" id="bn1"><div class="m1"><p>ای عشق بحسن دیده در ساز مرا</p></div>
<div class="m2"><p>عیبم همه سر بسر، هنر ساز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گیرم از آب زندگانی، دلگیر</p></div>
<div class="m2"><p>لب تشنه بخوناب جگر ساز مرا</p></div></div>