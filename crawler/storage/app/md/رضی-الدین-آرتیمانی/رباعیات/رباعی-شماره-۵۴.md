---
title: >-
    رباعی شماره ۵۴
---
# رباعی شماره ۵۴

<div class="b" id="bn1"><div class="m1"><p>در وادی معرفت نه گیر است و نه دار</p></div>
<div class="m2"><p>کانجا همه بر هیچ نهٰادند سوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتم که زمعرفت زنم دم، گفتا</p></div>
<div class="m2"><p>دریا به دهان سگ مگردان مردار</p></div></div>