---
title: >-
    رباعی شماره ۵۷
---
# رباعی شماره ۵۷

<div class="b" id="bn1"><div class="m1"><p>فریاد که سبحه در کفم شد زنار</p></div>
<div class="m2"><p>افسوس که یار عاقبت شد اغیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که بهیچ کار هرگز نایم</p></div>
<div class="m2"><p>چیزی نبودکه او نباشد در کار</p></div></div>