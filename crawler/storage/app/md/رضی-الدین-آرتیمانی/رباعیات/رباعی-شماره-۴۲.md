---
title: >-
    رباعی شماره ۴۲
---
# رباعی شماره ۴۲

<div class="b" id="bn1"><div class="m1"><p>این خلق جهان به یکدگر کینه ورند</p></div>
<div class="m2"><p>گویا که ز مرگ خویشتن بی‌خبرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون دو سگ گرسنه از بهر شکم</p></div>
<div class="m2"><p>از روی حسد بیکدگر مینگرند</p></div></div>