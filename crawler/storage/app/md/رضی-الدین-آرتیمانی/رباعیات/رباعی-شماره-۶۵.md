---
title: >-
    رباعی شماره ۶۵
---
# رباعی شماره ۶۵

<div class="b" id="bn1"><div class="m1"><p>ما دیدن عیش تو مدام انگاریم</p></div>
<div class="m2"><p>زهر غم تو لذت کام انگاریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما آب خضر بی تو حرام انگاریم</p></div>
<div class="m2"><p>یا زلف و رخ تو، صبح و شام انگاریم</p></div></div>