---
title: >-
    رباعی شماره ۶۴
---
# رباعی شماره ۶۴

<div class="b" id="bn1"><div class="m1"><p>با سبحه به چپ و راست ساغر گیریم</p></div>
<div class="m2"><p>وز ننگ ریا دین قلندر گیریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون باد به هر ناخوش و خوش در گذریم</p></div>
<div class="m2"><p>چون شعله بهر خار خسی درگیریم</p></div></div>