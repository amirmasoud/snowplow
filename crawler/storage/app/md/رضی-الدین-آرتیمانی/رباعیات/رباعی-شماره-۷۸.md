---
title: >-
    رباعی شماره ۷۸
---
# رباعی شماره ۷۸

<div class="b" id="bn1"><div class="m1"><p>ای یافته هر چه خواسته از یزدان</p></div>
<div class="m2"><p>اسکندر و مهدی و سلیمان زمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آنکه ز شٰان، میر در گاه تو را</p></div>
<div class="m2"><p>قیصر، قیصر خواند و خاقان، خاقان</p></div></div>