---
title: >-
    رباعی شماره ۳
---
# رباعی شماره ۳

<div class="b" id="bn1"><div class="m1"><p>شوخی که تمام پای بستم او را</p></div>
<div class="m2"><p>بی منت جام و باده مستم او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا مپرستید بغیر از من کس</p></div>
<div class="m2"><p>جز او نه کسی تا که پرستم او را</p></div></div>