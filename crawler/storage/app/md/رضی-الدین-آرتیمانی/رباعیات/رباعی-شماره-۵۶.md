---
title: >-
    رباعی شماره ۵۶
---
# رباعی شماره ۵۶

<div class="b" id="bn1"><div class="m1"><p>تا کی ز جفای چرخ باشم من زار</p></div>
<div class="m2"><p>جان خسته و دل شکسته خاطر افکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم بیدار بعکس بختم ایکاش</p></div>
<div class="m2"><p>بختم بودی بجای چشمم بیدار</p></div></div>