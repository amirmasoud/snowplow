---
title: >-
    رباعی شماره ۱۱
---
# رباعی شماره ۱۱

<div class="b" id="bn1"><div class="m1"><p>از کوتهی، ار عمر درازت هوس است</p></div>
<div class="m2"><p>جاوید اگر شوی همان یک نفس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خر تیرهٔ‌ای الاغ تا کی شرمی</p></div>
<div class="m2"><p>درماندهٔ‌ای مزبله تا چند بس است</p></div></div>