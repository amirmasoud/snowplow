---
title: >-
    رباعی شماره ۸
---
# رباعی شماره ۸

<div class="b" id="bn1"><div class="m1"><p>ای گشته تو را صفات، مانع از ذات</p></div>
<div class="m2"><p>از ذات فرو نمان به امید صفات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندم پرسی کز چه جهت روزی توست</p></div>
<div class="m2"><p>با آنکه خداست رازق از کل جهات</p></div></div>