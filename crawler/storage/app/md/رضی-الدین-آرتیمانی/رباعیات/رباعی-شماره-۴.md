---
title: >-
    رباعی شماره ۴
---
# رباعی شماره ۴

<div class="b" id="bn1"><div class="m1"><p>از بس در سر هوای آن دوست مرا</p></div>
<div class="m2"><p>روی دل از آنجهت بهر سوست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دوست نمی‌کند ز دشمن فرقم</p></div>
<div class="m2"><p>دشمن که نکرد فرق از دوست مرا</p></div></div>