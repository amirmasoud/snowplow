---
title: >-
    رباعی شماره ۳۷
---
# رباعی شماره ۳۷

<div class="b" id="bn1"><div class="m1"><p>گاهیم چو مرده در کفن میسازد</p></div>
<div class="m2"><p>گاهی از من، هزار من میسازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میسوخت مرا اگر نمیسوخت دلم</p></div>
<div class="m2"><p>این میسوزد که او بمن می‌سازد</p></div></div>