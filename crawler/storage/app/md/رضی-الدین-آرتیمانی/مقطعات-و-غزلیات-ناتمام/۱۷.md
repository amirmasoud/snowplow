---
title: >-
    ۱۷
---
# ۱۷

<div class="b" id="bn1"><div class="m1"><p>ما بهر هلاک خود هلاکیم</p></div>
<div class="m2"><p>ز آلایش آب و خاک، پاکیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عین عشقیم و آن حسنیم</p></div>
<div class="m2"><p>تادست بهم دهیم خشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشم بهم نهیم خاکیم</p></div>
<div class="m2"><p>روح محضیم و جان پاکیم</p></div></div>