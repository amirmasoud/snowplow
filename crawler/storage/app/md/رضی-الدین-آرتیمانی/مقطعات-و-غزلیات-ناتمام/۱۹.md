---
title: >-
    ۱۹
---
# ۱۹

<div class="b" id="bn1"><div class="m1"><p>چه افسون با من دیوانه کردی</p></div>
<div class="m2"><p>که از هر آشنا بیگانه کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بوی مشک نتوان کوچه‌ها گشت</p></div>
<div class="m2"><p>مگر زلف معنبر شانه کردی</p></div></div>