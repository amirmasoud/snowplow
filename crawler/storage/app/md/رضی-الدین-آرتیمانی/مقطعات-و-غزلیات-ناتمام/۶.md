---
title: >-
    ۶
---
# ۶

<div class="b" id="bn1"><div class="m1"><p>شدم صیدی که نتوان زد تغافل</p></div>
<div class="m2"><p>به صیادی که داند زخم کاری است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا گردان آن صیاد گردم</p></div>
<div class="m2"><p>که بی‌دانه درین دامم فکنده است</p></div></div>