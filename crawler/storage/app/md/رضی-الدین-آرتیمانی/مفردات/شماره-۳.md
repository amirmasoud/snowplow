---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>فیض عجبی یافتم از صبح ببینید</p></div>
<div class="m2"><p>این جادهٔ روشن ره میخانه نباشد</p></div></div>