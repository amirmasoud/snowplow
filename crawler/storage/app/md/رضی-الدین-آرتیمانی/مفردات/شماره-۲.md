---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>از آن هجران کند با من مدارا</p></div>
<div class="m2"><p>که بی او زیستن کم مردنی نیست</p></div></div>