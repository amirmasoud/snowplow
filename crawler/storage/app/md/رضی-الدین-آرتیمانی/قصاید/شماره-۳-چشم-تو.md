---
title: >-
    شمارهٔ  ۳ - چشم تو
---
# شمارهٔ  ۳ - چشم تو

<div class="b" id="bn1"><div class="m1"><p>بسکه بر سر زدم ز فرقت یار</p></div>
<div class="m2"><p>کارم از دست رفت ودست از کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشربم ننگ و عشق شور انگیز</p></div>
<div class="m2"><p>مرکبم لنگ و راه ناهموار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بحر پر شور و ناخدا ناشی</p></div>
<div class="m2"><p>دل به دریا همی کنی ناچار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات عشق و شور و جنون</p></div>
<div class="m2"><p>باختم دین و دل، قلندوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبح عشق است ساقیا بر خیز</p></div>
<div class="m2"><p>روز عیش است مطربا بردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بر آریم بانگ نوشانوش</p></div>
<div class="m2"><p>تا برقصیم جمله صوفی‌وار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه شوریم، ما کجا و شکیب</p></div>
<div class="m2"><p>همه سوزیم ما کجا و شرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه شوقیم، ما کجا و سکون</p></div>
<div class="m2"><p>غرق عشقیم، ما کجا و کنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی‌حضوریم ما کجا و شراب</p></div>
<div class="m2"><p>ناصبوریم، ما کجا و قرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که از عشق دم زنی بدروغ</p></div>
<div class="m2"><p>خویش را هرزه می‌کنی آزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنقدر شور نیست در سر تو</p></div>
<div class="m2"><p>که پریشان شود تو را دستار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خنده زان رو کنی چو بیدردان</p></div>
<div class="m2"><p>کت ندادند شوق گریهٔ زار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر به کعبه کجا فرود آری</p></div>
<div class="m2"><p>در خرابات اگر بیابی بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کارت از دیر و کعبه بر ناید</p></div>
<div class="m2"><p>یارت ار نیست بر در خمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا به هوش خودی نیاری گفت</p></div>
<div class="m2"><p>لیس فی الجنتی، سوی الجبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چند باشی ز غصه بوقلمون</p></div>
<div class="m2"><p>چند گردی ز غم چو بو تیمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آسمان و زمین هر چه در اوست</p></div>
<div class="m2"><p>همه پامال توست سر بردار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پشت پائی بزن به این هر دو</p></div>
<div class="m2"><p>دست خود را بشو ازین مردار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برو ای خواجه کان متاع نیم</p></div>
<div class="m2"><p>که فروشنده بر سر بازار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در ره دوست پوست پوشیدم</p></div>
<div class="m2"><p>تا فکندیم هفت پوست چو پار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هیچکس زو نمٰانداد نشان</p></div>
<div class="m2"><p>خاطر از هیچ جا نیافت قرار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا بجائی رسید شور جنون</p></div>
<div class="m2"><p>که بر افتاد پردهٔ پندار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دوست دیدم همه بصورت دوست</p></div>
<div class="m2"><p>یار دیدم همه بصورت یار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خانهٔ او زهر که جستم گفت</p></div>
<div class="m2"><p>لیس فی الدار، غیره دیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این به بازی نشسته در خلوت</p></div>
<div class="m2"><p>و ان به کاری روانه در بازار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یار ما در نیامد از خلوت</p></div>
<div class="m2"><p>کار ما در نیٰامد از بازار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هیچگه سبحه‌ای نگرداندیم</p></div>
<div class="m2"><p>که نگردید گرد آن زنار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پر مزن جز در آستانهٔ عشق</p></div>
<div class="m2"><p>سر مزن جز در آستانهٔ یار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دور اگر نیست بر مراد، مرنج</p></div>
<div class="m2"><p>که نه در دست ماست این پرگار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای که گوئی که دل ازو بر گیر</p></div>
<div class="m2"><p>گر توانی تو چشم ازو بردار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صوفی ار سجدهٔ صنم نکنی</p></div>
<div class="m2"><p>خرقه خصمت شود، کمر زنار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه در ذکر و ما همه خاموش</p></div>
<div class="m2"><p>همه تسبیح و ما همه زنار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرگ بهتر که صحبت بی‌دوست</p></div>
<div class="m2"><p>گور خوشتر که خلوت بی‌یار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>رضیا کوشش تو بیهوده است</p></div>
<div class="m2"><p>که نه در دست توست این افسار</p></div></div>