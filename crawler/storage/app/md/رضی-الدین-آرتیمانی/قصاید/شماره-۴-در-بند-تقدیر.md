---
title: >-
    شمارهٔ  ۴ - در بند تقدیر
---
# شمارهٔ  ۴ - در بند تقدیر

<div class="b" id="bn1"><div class="m1"><p>هیچ کاری نشد به تدبیرم</p></div>
<div class="m2"><p>چه کنم، مبتلای تقدیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قضا من نه مرد مصلحتم</p></div>
<div class="m2"><p>با قدر، من که و چه تدبیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون گریزم ز دست بختِ سیاه</p></div>
<div class="m2"><p>پشهٔ پای مانده در قیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محنت شهر را امانت‌دار</p></div>
<div class="m2"><p>غصه دهر را ضمان گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خم شد از غم قدم بسان کمان</p></div>
<div class="m2"><p>بسکه بر سنگ آمده تیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شده نخجیرم از کف و مانده</p></div>
<div class="m2"><p>چشم بر نقش پای نخجیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>محنت روزگار گرسنه چشم</p></div>
<div class="m2"><p>کرده از جان خویشتن سیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه شایسته‌ام به ناشایست</p></div>
<div class="m2"><p>گبر و ترسا کنند تکفیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غمش سوختیم و در نگرفت</p></div>
<div class="m2"><p>می‌ ندانم که چیست تقصیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشک و آهم دگر جهان گیر است</p></div>
<div class="m2"><p>شاید ار گوئیم جهان گیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بهاری چنین چه دلتنگم</p></div>
<div class="m2"><p>در هوائی چنین چه دلگیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مطربی کو که پرده‌ای سازد</p></div>
<div class="m2"><p>شاهدی کو که ساغری گیرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با جوانان همیشه بازم عشق</p></div>
<div class="m2"><p>هست این پند یاد از پیرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرغ و ماهی نمیکشم در دام</p></div>
<div class="m2"><p>شده ماهی و ماه تسخیرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گشته‌ام استخوانی از دردت</p></div>
<div class="m2"><p>بو که سازی نشانه تیرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در تمول اگر چه هیچ نیم</p></div>
<div class="m2"><p>در توکل ببین جهان گیرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون شوم زیر بار روی زمین</p></div>
<div class="m2"><p>کاسمان اوفتاده در زیرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غم پیریت در جوانی خور</p></div>
<div class="m2"><p>هست این پند یاد از پیرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شده‌ام چون مسخر عشقت</p></div>
<div class="m2"><p>ماه و ماهی شده است تسخیرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از تف دل چو موم بگدازم</p></div>
<div class="m2"><p>گر ز آهن کنند تصویرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه خرابم چنانکه روح‌اللّه</p></div>
<div class="m2"><p>بتواند نمود تعمیرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر بی شور ننگ مردان است</p></div>
<div class="m2"><p>تا کی این ننگ را به سر گیرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیر بر من چه میکشی چون من</p></div>
<div class="m2"><p>کشته شصت و دست زهگیرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در هلاکم چه میکنی تقصیر</p></div>
<div class="m2"><p>می ندانم که چیست تقصیرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه چنانست با تو پیوندم</p></div>
<div class="m2"><p>که بریدن توان به شمشیرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در چه پیچم گر از تو سرپیچم</p></div>
<div class="m2"><p>در که بندم، دل از تو بر گیرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شرح هجران اگر کنم، ریزد</p></div>
<div class="m2"><p>به دل حرف، خون ز تقریرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در غمت شام تا سحر چون شمع</p></div>
<div class="m2"><p>سوزم و سوختن ز سر گیرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بی لبت تلخ کامم از شکر</p></div>
<div class="m2"><p>بی‌رخت از حیات دلگیرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بخوانی ز شوق، میسوزم</p></div>
<div class="m2"><p>ور برانی ز ذوق، میمیرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دامن از من مکش که در محشر</p></div>
<div class="m2"><p>خیزم از خاک و دامنت گیرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همه حیرانی و جنون آرد</p></div>
<div class="m2"><p>گوش کس مشنواد تقریرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرگزم دل به هیچ در نگرفت</p></div>
<div class="m2"><p>گر چه هر دم چو شعله در گیرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غم بی‌درد میکشد زودم</p></div>
<div class="m2"><p>چه غم ار درد میکشد دیرم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هیچم از هیچکس نبودی کم</p></div>
<div class="m2"><p>گر بدی زهد و زرق و تزویرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اشک و آهم رضی جهانگیر است</p></div>
<div class="m2"><p>شاید ار گوییم جهانگیرم</p></div></div>