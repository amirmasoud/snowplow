---
title: >-
    غزل شمارهٔ ۱۰
---
# غزل شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>کسی که در رهش از پا و سر خبردار است</p></div>
<div class="m2"><p>نه عاشق است که در بند کفش و دستار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمی به گرد دلم جلو‌ه‌گر شده که از آن</p></div>
<div class="m2"><p>غباری ار بنشیند بر آسمان بار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدیگران ببر ای باد بوی نومیدی</p></div>
<div class="m2"><p>که در خرابهٔ ما زین متاع بسیار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آستانه او عاشقانه جان درباخت</p></div>
<div class="m2"><p>رضی که در غم عشقش هنوز بیمار است</p></div></div>