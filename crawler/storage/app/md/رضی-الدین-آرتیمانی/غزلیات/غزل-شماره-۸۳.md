---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>چه التفات به خار و خس چمن داری</p></div>
<div class="m2"><p>که عار و ننگ ز نسرین و یاسمن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام سحر و فسونی به دلفریبی خلق</p></div>
<div class="m2"><p>چه احتیاج به زلف و رخ و ذغن داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر تلافی ما در دلت گذشته که باز</p></div>
<div class="m2"><p>هزار عربده با خوی خویشتن داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورند خون همه اعضا ز ذوق شمشیرت</p></div>
<div class="m2"><p>مگر به خاطر خود فکر قتل من داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاط و عیش ببزم تو خوشه چینانند</p></div>
<div class="m2"><p>که می قدح قدح و گل چمن چمن داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه دوستیست به آن سنگدل رضی دیگر</p></div>
<div class="m2"><p>چه دشمنیست که با جان خویشتن داری</p></div></div>