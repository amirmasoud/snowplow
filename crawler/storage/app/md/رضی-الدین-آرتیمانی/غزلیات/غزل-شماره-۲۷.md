---
title: >-
    غزل شمارهٔ ۲۷
---
# غزل شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>جائی که به طاعات مباهات توان کرد</p></div>
<div class="m2"><p>محراب صنم قبلهٔ حاجات توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من روی به کعبه نهم از خاک در تو</p></div>
<div class="m2"><p>از کعبه اگر رو به خرابات توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون روح قدس در طلب زندهٔ شوقم</p></div>
<div class="m2"><p>در عشق تو اظهار کرامات توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه جرأت پروانه و نه تاب سمندر</p></div>
<div class="m2"><p>دعوی محبت به چه آیات توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که منم ز اهرمن اعجاز توان دید</p></div>
<div class="m2"><p>و آنجا که توئی بندگی لات توان کرد</p></div></div>