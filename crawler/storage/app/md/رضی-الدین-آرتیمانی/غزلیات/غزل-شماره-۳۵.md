---
title: >-
    غزل شمارهٔ ۳۵
---
# غزل شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای کاش که سجاده به زنار فروشند</p></div>
<div class="m2"><p>این طایفه دین چند به دینار فروشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق از طرف برهمنان است که امروز</p></div>
<div class="m2"><p>صد سبحه به یک حلقه زنار فروشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترسم که به خاکستر گلخن نستانند</p></div>
<div class="m2"><p>زان جنس که این طایف دربار فروشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کار دلم کرد همه عشوه چشمش</p></div>
<div class="m2"><p>خوبان دغا مهر به اغیار فروشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مخمور دو چشم تو رضی گشته نگاهی</p></div>
<div class="m2"><p>کاین باده نه در خانهٔ خمار فروشند</p></div></div>