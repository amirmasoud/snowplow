---
title: >-
    غزل شمارهٔ ۱۶
---
# غزل شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>مرا در دل غم جانانه‌ای هست</p></div>
<div class="m2"><p>درون کعبه‌ام بتخانه‌ای هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لب مهر خموشی بر ندارم</p></div>
<div class="m2"><p>که در زنجیر من دیوانه‌ای هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراباتم ز مسجد خوشتر آید</p></div>
<div class="m2"><p>که آنجا نالهٔ مستانه‌ای هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمیدانم اگر نار است اگر نور</p></div>
<div class="m2"><p>همی دانم که آتش خانه‌ای هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درخشان اختری شو گیتی افروز</p></div>
<div class="m2"><p>و گر نه شمع در هر خانه‌ای هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رضی گویی کجٰا آرام داری</p></div>
<div class="m2"><p>کهن ویرانه، ماتم خانه‌ای هست</p></div></div>