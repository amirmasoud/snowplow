---
title: >-
    غزل شمارهٔ ۳۶
---
# غزل شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>مگر شور عشقت ز طغیان نشیند</p></div>
<div class="m2"><p>که بحر سر شکم ز طوفان نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر بر کنار است زان روی زلفش</p></div>
<div class="m2"><p>که پیوسته چون من پریشان نشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب بادهٔ خوشگواریست عشقت</p></div>
<div class="m2"><p>که در خوان گبر و مسلمان نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشسته است ذوق لبت در مذاقم</p></div>
<div class="m2"><p>چو گنجی که در کنج ویران نشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشسته بر آن روی زلف سیاهش</p></div>
<div class="m2"><p>چو کفری که بالای ایمان نشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اجل گشته آنرا که در خوابش آئی</p></div>
<div class="m2"><p>سراسیمه خیزد پریشان نشیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر آنکو فکندم جدا از عزیزان</p></div>
<div class="m2"><p>الهی به مرگ عزیزان نشیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قبای سلامت به آن رند بخشند</p></div>
<div class="m2"><p>که از هستی خویش عریان نشیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رضی شد پریشان آن زلف یا رب</p></div>
<div class="m2"><p>پریشان کننده پریشان نشیند</p></div></div>