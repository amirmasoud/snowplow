---
title: >-
    غزل شمارهٔ ۴۵
---
# غزل شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ای عشق نگویم که به جای خوشم انداز</p></div>
<div class="m2"><p>یکبار دگر در تف آن آتشم انداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش چه زنی بر دلم از نام جدائی</p></div>
<div class="m2"><p>این حرف مگو با من و در آتشم انداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیماری خود داده به ما نرگس مستش</p></div>
<div class="m2"><p>ای دیده ز پر کالهٔ دل مفرشم انداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب نپسندی که بخواهم ز تو چیزی</p></div>
<div class="m2"><p>یا رب به کریمی خود از خواهشم انداز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مغز سر خویش رضی شعله بر افروز</p></div>
<div class="m2"><p>و اندر دل بی عزت خواری کشم انداز</p></div></div>