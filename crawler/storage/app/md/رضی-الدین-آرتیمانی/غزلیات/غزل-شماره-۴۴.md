---
title: >-
    غزل شمارهٔ ۴۴
---
# غزل شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>شور عشقی کرده بازم بیقرار</p></div>
<div class="m2"><p>باز دل را داده‌ام بی‌اختیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو قرار حیرت ماهم بده</p></div>
<div class="m2"><p>ای که داری در تکاپویش قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما به عهدت استوار استاده‌ایم</p></div>
<div class="m2"><p>گر چه عهد تو نباشد استوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند باشم همچو چشمت ناتوان</p></div>
<div class="m2"><p>چند باشم همچو زلفت بیقرار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا مرا یک روزگاری دست ده</p></div>
<div class="m2"><p>یا که دست از روزگار من بدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل تسلی میشود از وعده‌ات</p></div>
<div class="m2"><p>گر چه خواهی کشتنم از انتظار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نداری شوری از ما بر کران</p></div>
<div class="m2"><p>ور نداری شوقی از ما بر کنار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور از آن روح مجسّم زنده‌ای</p></div>
<div class="m2"><p>زین گران جانی رضی شرمی بدار</p></div></div>