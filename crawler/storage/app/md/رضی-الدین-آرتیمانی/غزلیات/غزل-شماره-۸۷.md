---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>نه رسم دیر و نه آیین کعبه می‌دانی</p></div>
<div class="m2"><p>ندانمت چه کسی، کافری، مسلمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مال و جاه چه نازی، که شخص نمرودی</p></div>
<div class="m2"><p>به خورد و خواب چه سازی که نفس حیوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمیز نیک و بد از هم نکردنت سهل است</p></div>
<div class="m2"><p>بلاست اینکه تو بد نیک و نیک بد دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین جهان ز تو حیوان به جان خود مانده</p></div>
<div class="m2"><p>که ره بسی است ز تو تا جهان انسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیر انسان هر چیز گویمت شادی</p></div>
<div class="m2"><p>به غیر آدم هر چیز خوانمت آنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه جانور کنمت نام مانده‌ام حیران</p></div>
<div class="m2"><p>به هیچ جانوری غیر خود نمی‌مانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه لازم است مدارا دگر به دشمن و دوست</p></div>
<div class="m2"><p>کنون که گشت رضی کشتی تو طوفانی</p></div></div>