---
title: >-
    غزل شمارهٔ ۵۱
---
# غزل شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>با رخ همچو صبح و زلف چو شام</p></div>
<div class="m2"><p>بامدادان بر آی بر لب بام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بدانند نور از ظلمت</p></div>
<div class="m2"><p>تا شناسند صبح را از شام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذری گر ز معبد گبران</p></div>
<div class="m2"><p>ور بر آئی به قبلهٔ اسلام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشناسند زاهدان محراب</p></div>
<div class="m2"><p>نپرستند کافران اصنام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محض عشوه است مر تو را ترکیب</p></div>
<div class="m2"><p>وز کرشمه است مر تو را اندام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دعای فرشته بیزارم</p></div>
<div class="m2"><p>گر از آن لب دهی مرا دشنام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بسنجی تو عقل را با عشق</p></div>
<div class="m2"><p>می بدانی تو نور را ز ظلام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکنی فرق نیک را از بد</p></div>
<div class="m2"><p>نشناسی حلال را ز حرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور از آن آستان نمی‌میرم</p></div>
<div class="m2"><p>آه از این روی، آه از این اندام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قصهٔ خود رضی بیا و مگو</p></div>
<div class="m2"><p>از تو چون کس نمیبرد پیغام</p></div></div>