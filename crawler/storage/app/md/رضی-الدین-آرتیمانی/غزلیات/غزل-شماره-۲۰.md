---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>کنم از شام تا سحر فریاد</p></div>
<div class="m2"><p>کس بدادم نمیرسد صد داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه ز نازم کشد گه از غمزه</p></div>
<div class="m2"><p>هر زمان شیوه‌ای کند بنیاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکشد لطفش، آه ازین جادو</p></div>
<div class="m2"><p>میبرد دستش، آه ازین جلاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه دیوانه پیش او عاقل</p></div>
<div class="m2"><p>همه شاگرد پیش او استاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرّ عشق ار چه گفتنی نبود</p></div>
<div class="m2"><p>گفتم این رمز هر چه بادا باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینت از عادت مُسلمانی</p></div>
<div class="m2"><p>روزی هیچ کافری مکناد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هجر بس نیست وصل غیرم کشت</p></div>
<div class="m2"><p>رضیا مرگ تو مبارک باد</p></div></div>