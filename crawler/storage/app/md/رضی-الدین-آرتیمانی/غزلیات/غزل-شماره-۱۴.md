---
title: >-
    غزل شمارهٔ ۱۴
---
# غزل شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>عشقی بتازه باز گریبان گرفته است</p></div>
<div class="m2"><p>آه این چه آتش است که در جان گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایدل ز اضطراب زمانی فرو نشین</p></div>
<div class="m2"><p>دستم بزور دامن جانان گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن لعل آبدار ز تسخیر کائنات</p></div>
<div class="m2"><p>خاصیت نگین سلیمان گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هر طرف که میشنوم بانگ غرقه است</p></div>
<div class="m2"><p>دریای عشق بین که چه طوفان گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد سر خرابی عٰالم به گریه باز</p></div>
<div class="m2"><p>این دل که، همچو شام غریبان گرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه و فغان شیونیانم بلند شد</p></div>
<div class="m2"><p>گویا طبیب دست ز درمان گرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیلی قبا و طره پریشان و سینه چاک</p></div>
<div class="m2"><p>آئین ماتمم به چه سامان گرفته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صوفی بیا که کعبهٔ مقصود در دلست</p></div>
<div class="m2"><p>حاجی به هرزه راه بیابان گرفته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا رب کجا رویم که در زیر آسمٰان</p></div>
<div class="m2"><p>هر جا که میرویم چو زندان گرفته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتوان گشودنش به نسیم ریاض جلد</p></div>
<div class="m2"><p>آندل که در فراق عزیزان گرفته است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کافر چنین مبٰاد ندانم رضی تو را</p></div>
<div class="m2"><p>دود دل کدام مسلمان گرفته است</p></div></div>