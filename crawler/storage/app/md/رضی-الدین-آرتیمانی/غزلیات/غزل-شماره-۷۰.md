---
title: >-
    غزل شمارهٔ ۷۰
---
# غزل شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>غمزه خونریز و عشوه در پی جان</p></div>
<div class="m2"><p>چون توان برد دین ودل ز میان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند از حسرت سراپایت</p></div>
<div class="m2"><p>بی‌سر و پا شویم و بی دل و جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گیرم ز غم به دندان دست</p></div>
<div class="m2"><p>آه از دست آن لب و دندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو آزاد جان از ین غم داد</p></div>
<div class="m2"><p>که گرفتار توست پیر و جوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچنان شد غمش گریبان گیر</p></div>
<div class="m2"><p>که گریبٰان ندانم از دامان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز وصل تو میروم از هوش</p></div>
<div class="m2"><p>شب مهتاب، وای بر کتّان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست هر چند دشمن است با ما</p></div>
<div class="m2"><p>ما بدو دوستیم از دل و جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکند در دلت اثر آهم</p></div>
<div class="m2"><p>چکند باد با دل سندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کاش درد دلم فزون نکنی</p></div>
<div class="m2"><p>چون به دردم نمیشوی درمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به عهدت زبون شویم چه باک</p></div>
<div class="m2"><p>سد اسکندریم در پیمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر شوریدهٔ رضی است مگر</p></div>
<div class="m2"><p>که چو گوئی فتاده در میدان</p></div></div>