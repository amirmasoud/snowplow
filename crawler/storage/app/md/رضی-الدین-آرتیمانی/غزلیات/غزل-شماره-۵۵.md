---
title: >-
    غزل شمارهٔ ۵۵
---
# غزل شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>تا بسر شوری از آن زلف پریشان دارم</p></div>
<div class="m2"><p>نه سر کفر و نه اندیشهٔ ایمان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده بردار که تا بر همه روشن گردد</p></div>
<div class="m2"><p>کز چه رو مذهب خورشید پرستان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیرم از رشک و شد آمیخته با جان غم یار</p></div>
<div class="m2"><p>یوسف و گرگ به یک چاه به زندان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خیال رخت آسوده‌ام از محنت هجر</p></div>
<div class="m2"><p>همره نوح، چه اندیشه ز طوفان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای رضی روزی کافر نشود امنی کو</p></div>
<div class="m2"><p>این خجالت که من از گبر و مسلمان دارم</p></div></div>