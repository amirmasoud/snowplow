---
title: >-
    غزل شمارهٔ ۳۲
---
# غزل شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>حیف که اوقات ما تمام هبا شد</p></div>
<div class="m2"><p>عمر گرانمایه صرف چون و چرا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما حصلی خود نداشت غیر ندامت</p></div>
<div class="m2"><p>حیف ز عمری که صرف مهر و وفا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه جمٰال تو دید بی دل و دین گشت</p></div>
<div class="m2"><p>و آنکه وصال تو یافت بی سر و پا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار شد اغیار و روزگار دگر شد</p></div>
<div class="m2"><p>روزی کافر مبٰاد آنچه به ما شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دین و دلی داشتیم و خاطر جمعی</p></div>
<div class="m2"><p>زلف پریشان و چشم مست بلا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر نکرد آنچه ما ز خویش کشیدیم</p></div>
<div class="m2"><p>هجر نکرد آنچه روز وصل بما شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دربدر افتاد و اختیار نماندش</p></div>
<div class="m2"><p>از درت آنکو به اختیار جدا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرگ رضی موجب ملال تو گردید</p></div>
<div class="m2"><p>زنده بلا بس نبود مرده بلا شد</p></div></div>