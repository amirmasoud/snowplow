---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>نتوان گذشتن آسان از آن کو</p></div>
<div class="m2"><p>گل تا بگردن، گل تا بزانو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست آن شست مشکل توان رست</p></div>
<div class="m2"><p>صیاد ما را سخت است بازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرف خلاصی فکر محالی است</p></div>
<div class="m2"><p>فکری دگر کن حرفی دگر گو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل میربایند جان میستانند</p></div>
<div class="m2"><p>شو خان به بازی، شیران به بازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان مه که گاهی پهلوی غیر است</p></div>
<div class="m2"><p>صد داغ داریم، پهلو به پهلو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا رو نهٰادیم در عٰالم عشق</p></div>
<div class="m2"><p>با هر دو عالم گشتیم یکرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دوست نتوان ما را بریدن</p></div>
<div class="m2"><p>ناصح تو مینال، مشفق تو میگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم جان ستانند، هم دلفریبند</p></div>
<div class="m2"><p>آن زلف و کاکل، آن چشم وابرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوئی که بوئی ز آن گل شنیدم</p></div>
<div class="m2"><p>خود را نیٰابی، یابی گران بو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون به توان کرد عاشق به تدبیر</p></div>
<div class="m2"><p>کی خوش توان کرد دندان به دارو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی می خرابم بی‌جرعه مدهوش</p></div>
<div class="m2"><p>زان لعل میگون زان چشم جادو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم رضی را سر نه بدین در</p></div>
<div class="m2"><p>کارش همین است در آن سر کو</p></div></div>