---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>ز خواب ناز خیز و فتنه سرکن</p></div>
<div class="m2"><p>جهان یکبارگی زیر و زبر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حذر از کوری خفاش طبعان</p></div>
<div class="m2"><p>سری از منظر خورشید در کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگویم صورتم را بخش معنی</p></div>
<div class="m2"><p>مرا از صورت و معنی بدر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیش این پردهٔ پندار بردار</p></div>
<div class="m2"><p>زمین و آسمان زیر و زبر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبر گوئی از آن عیٰار دارم</p></div>
<div class="m2"><p>برو ای بیخبر فکر دگر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جگر می پرور از خونابهٔ دل</p></div>
<div class="m2"><p>غذای دل هم از خون جگر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رضی تا چند ازین بسیار گفتن</p></div>
<div class="m2"><p>سخن اینجا رساندی، مختصر کن</p></div></div>