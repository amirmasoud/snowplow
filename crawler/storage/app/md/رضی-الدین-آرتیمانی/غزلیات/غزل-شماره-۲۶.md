---
title: >-
    غزل شمارهٔ ۲۶
---
# غزل شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>نمیاید چو از دل بر زبان درد</p></div>
<div class="m2"><p>ز دل بیرون کنم خود گو چسان درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهم از درد تو تا میتوان داغ</p></div>
<div class="m2"><p>کشم از داغ تو تا میتوان درد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر این است راحتها، همان رنج</p></div>
<div class="m2"><p>اگر این است آسایش همان درد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دردسر نمیارزد جهان هیچ</p></div>
<div class="m2"><p>سر ما خود ندارد هیچ از آن درد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دردم استخوان فرسود اکنون</p></div>
<div class="m2"><p>کند مغزم بجای استخوان درد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بخت ما بروید از زمین داغ</p></div>
<div class="m2"><p>به وقت ما ببارد ز آسمٰان درد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسیحا گو مدم بر ما که ندهیم</p></div>
<div class="m2"><p>به عمر جاودانی یک زمان درد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خواهد شد که گوید کشتهٔ ماست</p></div>
<div class="m2"><p>غمت را اینقدر آمد زبان درد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رضی سان کار بی دردان بسازم</p></div>
<div class="m2"><p>گر از مرگم دهد این بار امان درد</p></div></div>