---
title: >-
    غزل شمارهٔ ۷۶
---
# غزل شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>تو بدین چشم شوخ و روی چو ماه</p></div>
<div class="m2"><p>ببری دل ز دست سنگ سیاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر دستت چه آسمٰان چه زمین</p></div>
<div class="m2"><p>پایمالت چه آفتاب و چه ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز مستی نمیبریم بسر</p></div>
<div class="m2"><p>این زمان آمدیم بر سر راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ننالیم که از تماشایش</p></div>
<div class="m2"><p>باز گردد بسوی دیده نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه آن جلوه کرد با جانم</p></div>
<div class="m2"><p>برق هرگز نمیکند به گیاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که بی باک بر سر راهش</p></div>
<div class="m2"><p>میروی و نمیروی از راه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باش یک لحظه تا برون آید</p></div>
<div class="m2"><p>آفتابم ز زیر ابر سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفست از چه مرده زنده کند</p></div>
<div class="m2"><p>گر نه روح اللهی، بلا اشباه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنگ سوزم اگر ببٰارم اشک</p></div>
<div class="m2"><p>چرخ ریزم اگر بر آرم آه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاه و بی‌گاه منع ما نکنی</p></div>
<div class="m2"><p>چشمت ار بر رخش فتد ناگاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتمش میرود رضی گفتا </p></div>
<div class="m2"><p>هر کجا میرود خدا همراه</p></div></div>