---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>گلها ز من شکفته مگر بانگ بلبلم</p></div>
<div class="m2"><p>شب چشم من نخفت، مگر شبنم گلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون در دلم همی کند از آب کوثرم</p></div>
<div class="m2"><p>جا در دلش نمیکنم ار سحر باطلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن تو بی‌تأملم از هوش میبرد</p></div>
<div class="m2"><p>با آنکه در نگاه تو من بی تأملم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندک اندک بر سر کوی تو فندی میزنم</p></div>
<div class="m2"><p>پیش تو پستیم و یا هوی بلندی میزنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه می‌گوئیم از آن میدهد سرها بباد</p></div>
<div class="m2"><p>بر در اندیشه زین پس قفل و بندی میزنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو زما مشنو سخن با ما مگو و ز ما مپرس</p></div>
<div class="m2"><p>هر چه بادا باد گویا حرف چندی میزنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه میگرییم و گاهی خنده بر هم میکنیم</p></div>
<div class="m2"><p>ما و گردون یکدگر را ریشخندی میزنیم</p></div></div>