---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>هجران اگر نکردی آهنگ زندگانی</p></div>
<div class="m2"><p>بیچاره جان چه کردی با ننگ زندگانی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داراست هر که جان برد از چنگ مرگ بیرون</p></div>
<div class="m2"><p>ما جان به مرگ بردیم از چنگ زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی‌عشق کس ممیراد، بی درد کس مماناد</p></div>
<div class="m2"><p>کان عار مرگ باشد وین ننگ زندگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میبرد زندگانی گر جان ز چنگ مردن</p></div>
<div class="m2"><p>کس جان بدر نمیبرد از چنگ زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آنکه سنگ کوبی بر سینه از غم مرگ</p></div>
<div class="m2"><p>گویا سرت نخورد است بر سنگ زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آنکه زندگی را بر مرگ می‌گزینی</p></div>
<div class="m2"><p>یا رَب مبارک بادت او‌رنگ زندگانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیوسته زندگانی در جنگ بود با ما</p></div>
<div class="m2"><p>با مرگ صلح کردیم از ننگ زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوری او رضی را نزدیک گشته گویا</p></div>
<div class="m2"><p>کاثار مرگ پیداست از رنگ زندگانی</p></div></div>