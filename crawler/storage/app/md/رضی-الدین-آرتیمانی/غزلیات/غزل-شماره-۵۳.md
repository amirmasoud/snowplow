---
title: >-
    غزل شمارهٔ ۵۳
---
# غزل شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>پلاس تن به بر، از دست غم قبا کردم</p></div>
<div class="m2"><p>به این لباس برش عرض مدعا کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماند حاجت کس ناروا نمیدانم</p></div>
<div class="m2"><p>که گفت یا رب یا رب که من دعا کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار حیف ندانی که دور از تو بمن</p></div>
<div class="m2"><p>چها گذشت و چها دیدم و چها کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود غیر کمالت بهر چه کردم گوش</p></div>
<div class="m2"><p>مه جمال تو دیدم چو چشم وا کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان ز حرف تو پر بود تا بدم خاموش</p></div>
<div class="m2"><p>بریده باد زبانم سخن چرا کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اتفاق رضی آمدم به طوف درت</p></div>
<div class="m2"><p>تو را ندیدم آنجا و کربلا کردم</p></div></div>