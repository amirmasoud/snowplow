---
title: >-
    غزل شمارهٔ ۷۳
---
# غزل شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>مرا دستی است بالا دست گردون</p></div>
<div class="m2"><p>که نتوان ز آستینش کرد بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم بر درگهش چون حلقه بر در</p></div>
<div class="m2"><p>نه دست اندرون نه پای بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هژبرانند اینجٰا خفته در خاک</p></div>
<div class="m2"><p>دلیرانند اینجٰا غرقه در خون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن بی‌جان چگونه زنده ماند</p></div>
<div class="m2"><p>رضی بی‌ او بگو چون زنده‌ای چون</p></div></div>