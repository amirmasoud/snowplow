---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>داند آنکس که ز دیدار تو برخوردار است</p></div>
<div class="m2"><p>که خرابات و حرم غیر در و دیوار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که در طور ز بی‌حوصلگی مدهوشی</p></div>
<div class="m2"><p>دیده بگشای که عالم همگی دیدار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه پامال تو شد خواه سرو خواهی جان</p></div>
<div class="m2"><p>وآنچه در دست من از توست همین پندار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو ناقوس بدست من مست است که هست</p></div>
<div class="m2"><p>و ز تو طرفی که ببستیم همین زنار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخور از باغچهٔ حسن که نشکفته، هنوز</p></div>
<div class="m2"><p>گل رسوایی ما از چمن دیدار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باور از مات نیاید به لب بام در آی</p></div>
<div class="m2"><p>تا ببینی که چه شور از تو درین بازار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دو جهان بر سر دل باخت رضی منفعل است</p></div>
<div class="m2"><p>که فزایند بر آن بار گر این بازار است</p></div></div>