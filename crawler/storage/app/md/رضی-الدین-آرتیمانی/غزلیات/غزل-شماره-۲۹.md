---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>گر نقاب از رخ آن صنم گیرد</p></div>
<div class="m2"><p>ماه و خورشید را عدم گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور به بتخانه پرتو اندازد</p></div>
<div class="m2"><p>بتکده رونق حرم گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دو دست از دو دیده بر گیرم</p></div>
<div class="m2"><p>همه آفاق درد و غم گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم بوالهوس که فرمائی</p></div>
<div class="m2"><p>هرزه دو سگ شکار کم گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ بیچاره گر فرشته شود</p></div>
<div class="m2"><p>نشود کاهوی حرم گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دُودِ دل از قلم زبانه کشید</p></div>
<div class="m2"><p>چون بیٰاد رضی قلم گیرد</p></div></div>