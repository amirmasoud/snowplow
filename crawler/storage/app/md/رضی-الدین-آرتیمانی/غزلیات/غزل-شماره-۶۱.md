---
title: >-
    غزل شمارهٔ ۶۱
---
# غزل شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>آنچه من از تو، خدا می‌بینم</p></div>
<div class="m2"><p>همه جا خوف و رجا می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وجود همه نومیدیها</p></div>
<div class="m2"><p>همه امید روا می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای تا سر همه عصیان و خطا</p></div>
<div class="m2"><p>همه پاداش خطا می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده بر دوز ز خود تا بینی</p></div>
<div class="m2"><p>کز کجٰا تا به کجا می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با وجودی که تو را نتوان دید</p></div>
<div class="m2"><p>من چه گویم که چهٰا می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه چیز تو را میشنوم</p></div>
<div class="m2"><p>در همه چیز تو را می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست جائی که نباشی آنجٰا</p></div>
<div class="m2"><p>از سمک تا به سما می‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسته دلها همه خرم دیدم</p></div>
<div class="m2"><p>بسته درها همه وا می‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پا نهٰادم چو رضی در طلبت</p></div>
<div class="m2"><p>سر خود در ته پا می‌بینم</p></div></div>