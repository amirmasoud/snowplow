---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>از مردم چشم تو دل زلفت به یغما می‌برد</p></div>
<div class="m2"><p>جان دستمزد آن دزد را کز دزد کالا می‌برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترکی که از ملک دلم طاقت به یغما می‌برد</p></div>
<div class="m2"><p>ترک نگاهش یک‌تنه یغما ز تن‌ها می‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غمزه غارتگرش یرغو به سلطان می‌برم</p></div>
<div class="m2"><p>بی‌باک بین کز چابکی دل بی‌محابا می‌برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به خالش کی سیه بگریز از این آتشکده</p></div>
<div class="m2"><p>گفتا در آتش جایگه هندو به عمدا می‌برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلفش شکن اندر شکن چشمش به قصد جان من</p></div>
<div class="m2"><p>این جادو و آن راهزن یا می‌کشد یا می‌برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل گبر و وی زردشت او ایمان من در مشت او</p></div>
<div class="m2"><p>خون دلم انگشت او از بهر حنا می‌برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن ترک آذربایجان افروختم آذربایجان</p></div>
<div class="m2"><p>باد صبا خاکسترم اینک به صحرا می‌برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن نرگس خمار او و آن زلف چون زنار او</p></div>
<div class="m2"><p>گاهی به دیرم می‌کشد گه در کلیسا می‌برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید که تا آشفته را جان وارهد از این بلا</p></div>
<div class="m2"><p>بر دامن زلف بتان دست تولا می‌برد</p></div></div>