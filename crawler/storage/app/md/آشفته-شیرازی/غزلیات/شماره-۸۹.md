---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>در نهایت نظری بود باغیار امشب</p></div>
<div class="m2"><p>که زتب سوخت چو شمعم دل بیمار امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشب دیگرت این سوزن درون شرح دهم</p></div>
<div class="m2"><p>که مبادا بدرد پرده اسرار امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارنی گویم و از پای طلب ننشینم</p></div>
<div class="m2"><p>تا زطورم نرسد وعده دیدار امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستیت بود بهانه همه شب بهر رقیب</p></div>
<div class="m2"><p>فاش همصحبت اغیاری و هشیار است امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طمع سیم و زر از غیر مکن کیسه بیار</p></div>
<div class="m2"><p>زاشک و رخ سیم و زرم هست چو بسیار امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه بود مهلت دیدار و نه جای گفتار</p></div>
<div class="m2"><p>هست با ناله شبگیر سر و کار امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیف از آن سینه سیمین که شد اندوده بقیر</p></div>
<div class="m2"><p>از چه آئینه ات آلوده بزنگار امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محک آوردمت ای مدعی از بهر بتان</p></div>
<div class="m2"><p>برگرفتم ززر قلب تو معیار امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زر قلبت نخرد صیرفی عشق برو</p></div>
<div class="m2"><p>حبشی زاده مکن یوسف بازار امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو چون دایره آن مه بمیان بگرفتی</p></div>
<div class="m2"><p>ما همان پای بجائیم چو پرگار امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرو جان داد و دل و دین و خریدش زازل</p></div>
<div class="m2"><p>چون تو آشفته نبود است خریدار امشب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غیر چون رحم نکرد و بخطا دید بدوست</p></div>
<div class="m2"><p>کار با دست خداوند تو بگذار امشب</p></div></div>