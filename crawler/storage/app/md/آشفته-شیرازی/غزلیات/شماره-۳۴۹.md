---
title: >-
    شمارهٔ ۳۴۹
---
# شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>آنان که حجاب تن و جان را بدریدند</p></div>
<div class="m2"><p>در پرده بجز شاهد جانانه ندیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیمانه بدادند و قدح باز گرفتند</p></div>
<div class="m2"><p>گفتند هنیئا لک و پاسخ بشنیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک طایفه ی رشته تسبیح گرفتند</p></div>
<div class="m2"><p>یک سلسله زنار و چلیپا بگزیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی بخرابات بجامی شده آباد</p></div>
<div class="m2"><p>قومی بمناجات مرادند و مریدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی سعی گروهی همه در کعبه مجاور</p></div>
<div class="m2"><p>یک قوم طلبکار و بکعبه نرسیدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکسیر زخاک در میخانه گرفتیم</p></div>
<div class="m2"><p>بیهوده کسان از زر و زیبق طلبیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازم بخرابات که مستان خرابش</p></div>
<div class="m2"><p>پیمانه و ساغر نه که خمخانه کشیدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنان که زدی دست ملامت بزلیخا</p></div>
<div class="m2"><p>یوسف چو بدیدند همه دست بریدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فوجی بتجمل کمر و تاج ستاندند</p></div>
<div class="m2"><p>قومی زتغافل زسر خویش رمیدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صد سلسله دل داشت بزلف تو نشیمن</p></div>
<div class="m2"><p>مرغ دل آشفته چو دیدند پریدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر کس نسزد کسوت والای ولایت</p></div>
<div class="m2"><p>این جامه ببالای تو از ناز بریدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آنان که به تو دست خدا دست ندادند</p></div>
<div class="m2"><p>رفتند و سر انگشت بحسرت بگزیدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قومی که زتو روی باغیار نمودند</p></div>
<div class="m2"><p>از کعبه به بتخانه آزر گرویدند</p></div></div>