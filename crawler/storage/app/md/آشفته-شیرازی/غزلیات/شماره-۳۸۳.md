---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>هر که در گلشن دل سرو سمن بردارد</p></div>
<div class="m2"><p>بایدش دل زگل و سرو سمن بردارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالت بسمل عشق و مژه فتانش</p></div>
<div class="m2"><p>داند آن خسته که دل برسر خنجر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده چون پهلوی دارا دل مجروهم چاک</p></div>
<div class="m2"><p>آنکه ابروی چو شمشیر سکندر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود آرزوی کوثر و حورش در دل</p></div>
<div class="m2"><p>آنکه دلبر ببرو باده بساغر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاج جمشید بسرکی نهد و افسر کی</p></div>
<div class="m2"><p>هر که از خاک در میکده افسر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار ناصح بسر زلف تو افتادی کاش</p></div>
<div class="m2"><p>تا بدیدی دل آشفته چه بر سر دارد</p></div></div>