---
title: >-
    شمارهٔ ۱۰۱۲
---
# شمارهٔ ۱۰۱۲

<div class="b" id="bn1"><div class="m1"><p>ارغوان است که با غالیه آمیخته‌ای</p></div>
<div class="m2"><p>یا که بر برگِ سمن سنبلِ تر ریخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهر بر هم زده آشوب دو چشم سیهت</p></div>
<div class="m2"><p>این چه فتنه است ندانم که برانگیخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در طره خوبان به جز از فکر خطا</p></div>
<div class="m2"><p>تا کی ای دل تو در این سلسله آویخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامِ اغیار شده ذکر لبِ شیرینت</p></div>
<div class="m2"><p>این چه نوش است که با نیش درآمیخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاجرم مردم چشمان تو بیمار بماند</p></div>
<div class="m2"><p>بس که مشک از خمِ زلفینِ سیه بیخته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مظهر نور علی گر نه‌ای فتنهٔ شهر</p></div>
<div class="m2"><p>زابروان تیغ دودم از چه برآهیخته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه بر اوج جلالش نبری آشفته</p></div>
<div class="m2"><p>گرچه جبریل شوی بال فروریخته‌ای</p></div></div>