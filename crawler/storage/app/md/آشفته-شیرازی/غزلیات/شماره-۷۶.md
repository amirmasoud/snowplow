---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>تا برد رنج خمار و تا زداید رنج خواب</p></div>
<div class="m2"><p>ساغر می از خم گردون برآورد آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب این می از کجا خورده که همچون بخت من</p></div>
<div class="m2"><p>نرگس مخمور ساقی برنمیخیزد زخواب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب زهره جبین بذله بخوان چنگی بزن</p></div>
<div class="m2"><p>تا بنوشم باده گلگون بگلبانگ رباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی خمخانه در ده آن شراب آتشین</p></div>
<div class="m2"><p>کز شعاع او بسوزم پرده این نه حجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طره حوراوشی دارم که در باغ بهشت</p></div>
<div class="m2"><p>می نهم بر گردن حوری و غلمان زان طناب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خیال ماه رویت یکدمم دیده نخفت</p></div>
<div class="m2"><p>چون نکو دیدم همه شب میزدم نقشی بر آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چه یونس دل برون آمد زبطن حوت عشق</p></div>
<div class="m2"><p>هفت دریا در نظر آید مرا موج سراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنپیچم سر زجور بیحسابت از کمند</p></div>
<div class="m2"><p>دستت از دامن نمیدارم الی یوم حساب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بهشتم بی تو منزلگه بود بئس المصیر</p></div>
<div class="m2"><p>با تو گر دوزخ مرا مأوا بود حسن المآب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که را اندر ولای تو شکی باشد بدل</p></div>
<div class="m2"><p>گر در آبش افکنی آتش شود بی ارتیاب</p></div></div>