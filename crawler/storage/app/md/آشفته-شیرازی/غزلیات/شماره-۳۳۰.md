---
title: >-
    شمارهٔ ۳۳۰
---
# شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>دیدی دلا که اهل جهان را وفا نبود</p></div>
<div class="m2"><p>وقتی اگر که بوده در ایام ما نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی وفا بدهر چو سیمرغ و کیمیاست</p></div>
<div class="m2"><p>جستیم کیمیا و شنان از وفا نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من جفا پسندی و بر من مدعی وفا</p></div>
<div class="m2"><p>آن در خور جفا و این را وفا نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آیا چه شد که اهل هوس خانه کرده اند</p></div>
<div class="m2"><p>در آن حرم که محرم باد صفا نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی پرده گفت مردم چشمم رموز دل ‏</p></div>
<div class="m2"><p>ورنه مرا بدوست سر ماجرا نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی میفروش چه حشمت بود که دوش</p></div>
<div class="m2"><p>دیدم گدای او که بفکر غنا نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دردی که میدهند دوایش مقرر است</p></div>
<div class="m2"><p>جز دردمند عشق که هیچش دوا نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذشت شام وصل بیک شکوه از فراق</p></div>
<div class="m2"><p>آوخ که دور عمر جز اینش بقا نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او را هزار سلسله دل در قفا روان</p></div>
<div class="m2"><p>گرچه بغیر زلف کسش در قفا نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای هم نفس که هر نفسم بی تو دوزخیست</p></div>
<div class="m2"><p>پاداش غیر لایق کردار ما نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی قوت مرده بود مریض غم تو دوش</p></div>
<div class="m2"><p>او را زخون دیده و دل گر غذا نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مجنون نشان زمحمل لیلی چگونه یافت</p></div>
<div class="m2"><p>گر رهنما بقافله او را درا نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آشفته را که جز سر سودای تو نداشت</p></div>
<div class="m2"><p>راندن زچین حلقه زلفش سزا نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفتند هر یکی زحریفان بدرگهی</p></div>
<div class="m2"><p>ما را بجز رهی بدر مرتضی نبود</p></div></div>