---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>آن غمی را که کران نیست غم حرمانست</p></div>
<div class="m2"><p>آن شبی را که سحر نیست شب هجرانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت عشق نداری اثری در تو مگر</p></div>
<div class="m2"><p>یوسف تست زلیخا که در این زندانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می ‌نشاید که برد منت بی‌جا ز طبیب</p></div>
<div class="m2"><p>درمند تو که مستغنی از درمانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر خندیدن گل تا کی و چند اندر باغ</p></div>
<div class="m2"><p>ابر نیسان چو من سوخته دل گریانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر یکی رشحه از چشمه چشم تر ماست</p></div>
<div class="m2"><p>این که گویند که این قلزم و آن عمانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه که هر روز که خواهم غمت از دل بنهم</p></div>
<div class="m2"><p>شوق من بر رخ زیبای تو صد چندانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت چون گلشن شیراز به تاراج خزان</p></div>
<div class="m2"><p>بر سر آشفته کنونم هوس تهرانست</p></div></div>