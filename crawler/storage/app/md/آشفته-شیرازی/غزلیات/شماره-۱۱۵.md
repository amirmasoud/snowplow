---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>هر رهروی که خار مغیلان بپای اوست</p></div>
<div class="m2"><p>دیدار کعبه مرهم زخم و دوای اووست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفی مکان بدیهی عقل است و ای عجب</p></div>
<div class="m2"><p>آنرا که جای نیست دل خسته جای اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازم به پیر میکده کاین تیره خاکدان</p></div>
<div class="m2"><p>افشانده مشت گرد زطرف ردای اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویش ندیده دیده و زاین بحیرتم</p></div>
<div class="m2"><p>کز هر گذر که میگذرم ماجرای اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نائی اگر چه دم بدم نی دمد ولی</p></div>
<div class="m2"><p>گر نغمه ی زنی شنوی از نوای اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آزاد بنده ی که کند بندگی عشق</p></div>
<div class="m2"><p>در راحت آن دلی که قرین بلای اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خضر ار بآب زنده دل ما ببوی او</p></div>
<div class="m2"><p>آری بقای عاشق اندر بقای اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر از سپهر عشق کند کوکبی طلوع</p></div>
<div class="m2"><p>خورشید ذره وار برقص از هوای اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سالک بدیر و کعبه مساوی زند قدم</p></div>
<div class="m2"><p>زنار ما و سبحه شیخ از برای اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ارکان کشتی ار بکف ناخدا بود</p></div>
<div class="m2"><p>سالک سلوک کشتی او با خدای اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان غایب از نظر دل آشفته دردمند</p></div>
<div class="m2"><p>غافل از آن که یار مقیم سرای اوست</p></div></div>