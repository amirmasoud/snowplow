---
title: >-
    شمارهٔ ۷۲۲
---
# شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>عکس جمال این و آن هر چه فتاد در دلم</p></div>
<div class="m2"><p>میرود و نمیرود روی تو از مقابلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرتو شمع خاوری تافت زمشرق دلم</p></div>
<div class="m2"><p>شمع برید یک نفس همنفسان زمحفلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بانگ جرس زهر طرف میرسدم بگوش جان</p></div>
<div class="m2"><p>میشنوم صدا ولی نیست اثر زمحملم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده تیه حیرتم غرقه بحر کثرتم</p></div>
<div class="m2"><p>نوح کجاست تا برد رخت بسوی ساحلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشته این و آن کند دعوی خونبها بحشر</p></div>
<div class="m2"><p>من بخلاف کشتگان شکر گذار قاتلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوق برد مرا بسر بر سو کوی آن پسر</p></div>
<div class="m2"><p>وه که ندیده طلعتش بر رخ دوست مایلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم بت شکسته ام از کف غیر رسته ام</p></div>
<div class="m2"><p>آشفته طره بتی گشته چو بت حمایلم</p></div></div>