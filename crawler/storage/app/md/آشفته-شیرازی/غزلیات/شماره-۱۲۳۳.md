---
title: >-
    شمارهٔ ۱۲۳۳
---
# شمارهٔ ۱۲۳۳

<div class="b" id="bn1"><div class="m1"><p>با داورت سخن چه بود روز داوری</p></div>
<div class="m2"><p>یا خود بخون خلق بهانه چه آوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گیردت که داد ندادی بسلطنت</p></div>
<div class="m2"><p>با اینکه آمدت مه و ماهی بچاکری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پاسخ چه آوری و چه گوئی بمعذرت</p></div>
<div class="m2"><p>کانجا نمیخرند زتو ناز و دلبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می را حلال خوردی و خون زآن حلالتر</p></div>
<div class="m2"><p>تا بر تو باد هان که زخون جگرخوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر بنه غرور و بیاد آر عاقبت</p></div>
<div class="m2"><p>هرگه بخاک حلقه عشاق بگذری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد سپاه خط زعذارت بلند شد</p></div>
<div class="m2"><p>تا کبر و ناز و حسن زخاطر بدر بری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کی بنخوت کله و تاجی و کمر</p></div>
<div class="m2"><p>سر را فرود آر بکنج قلندری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درویش را بیار و باو یار شو زمهر</p></div>
<div class="m2"><p>چون بخت یاریت کند و حسن یاوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پندی بیادگار بگویم نگاهدار</p></div>
<div class="m2"><p>با بندگان شه مکن ای ترک داوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته راست داغ غلامی زمرتضی</p></div>
<div class="m2"><p>بهر نثار شاه بدامان در دری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او را بود بخاک نجف کان کیمیا</p></div>
<div class="m2"><p>از او بجو بتا عمل کیمیاگری</p></div></div>