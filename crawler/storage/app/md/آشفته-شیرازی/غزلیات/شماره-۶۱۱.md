---
title: >-
    شمارهٔ ۶۱۱
---
# شمارهٔ ۶۱۱

<div class="b" id="bn1"><div class="m1"><p>بر عشق صبر میکنم و بر جراحتش</p></div>
<div class="m2"><p>وز عقل میگریزم و داروی راحتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک دلی که خیمه واجب در او زنند</p></div>
<div class="m2"><p>ممکن چگونه پای گذارد بساحتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن منظر صبیح چگویم که در جهان</p></div>
<div class="m2"><p>صبح ازل نمونه بود از صباحتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محتاج چاشنی است اگر خوان پادشاست</p></div>
<div class="m2"><p>تا چاشنی گرفته نمک از ملاحتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آینه قبیح نماید رخ قبیح</p></div>
<div class="m2"><p>غفلت مکن حکیم زوجه قباحتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارف شراب ناب کشد گرچه شد حرام</p></div>
<div class="m2"><p>زاهد بآب ساخته و با اباحتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماهی اگر که راحت خود را در آب دید</p></div>
<div class="m2"><p>جوید سمندر آتش بر استراحتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی چو چشم یار بود نرگس چمن</p></div>
<div class="m2"><p>بگشای چشم و نیک ببین در وقاحتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید اگر که خیره بماند در او عقول</p></div>
<div class="m2"><p>بیند چو لطف عنصر او در سماحتش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیغمبری که گفت انا افصح از ازل</p></div>
<div class="m2"><p>کندست در حقیقت واجب فصاحتش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته یافته نمک مدح مرتضی</p></div>
<div class="m2"><p>شاید که التیام پذیرد جراحتش</p></div></div>