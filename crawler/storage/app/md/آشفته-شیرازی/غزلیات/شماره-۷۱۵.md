---
title: >-
    شمارهٔ ۷۱۵
---
# شمارهٔ ۷۱۵

<div class="b" id="bn1"><div class="m1"><p>برخیز تا بکوی مغان التجا کنیم</p></div>
<div class="m2"><p>داغ درون خسته بجامی دوا کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن به که صرف خدمت دردی کشان شود</p></div>
<div class="m2"><p>عمری که صرف فسق و فجور و ریا کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسق و فجور و زهد وغرور و ریا خطاست</p></div>
<div class="m2"><p>فکر صواب از پی رفع خطا کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعی و صفا و مروه بکعبه ثمر نکرد</p></div>
<div class="m2"><p>در میکده بحلقه مستان صفا کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکشب در سرای مغان گر کنند باز</p></div>
<div class="m2"><p>سی روزه روزه را بصبوحی قضا کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد زخوف دین نکند جا بمیکده</p></div>
<div class="m2"><p>در خوفگاه جا بامید رجا کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاده عکس ساقی ما در درون جام</p></div>
<div class="m2"><p>ساقی بیار باده که رو در خدا کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بارم دهد بمیکده گر پیر میفروش</p></div>
<div class="m2"><p>منزل بصحن بارگه کبریا کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یعنی که حلقه در شاه نجف علی</p></div>
<div class="m2"><p>گیریم و عمر دولت شه را دعا کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته جست رشته حبل المتین دین</p></div>
<div class="m2"><p>زنار زلف او زچه از کف رها کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر میشکی بساعد سیمین بروز حشر</p></div>
<div class="m2"><p>حاشا که از تو ما طلب خونبها کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قلب وجود ما بدر دوست چون رسد</p></div>
<div class="m2"><p>از خاک کوی او همه تن کیمیا کنیم</p></div></div>