---
title: >-
    شمارهٔ ۱۲۰۳
---
# شمارهٔ ۱۲۰۳

<div class="b" id="bn1"><div class="m1"><p>هیچ دانی که چه با این دل شیدا کردی</p></div>
<div class="m2"><p>صبر و دین طاقت و عقلش همه یغما کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داغ عشقی زدیش زان خم گیسو بجبین</p></div>
<div class="m2"><p>رانده اش از حرم و دیر و کلیسا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکشان خام شمارندم و زاهد خمار</p></div>
<div class="m2"><p>وه که در مسجدم و میکده رسوا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمان در طلب گوهر وصلت غواص</p></div>
<div class="m2"><p>دیده از خون دلم غیرت دریا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه فرهاد وشم تلخ زشیرین شد کام</p></div>
<div class="m2"><p>گاه مجنون صفتم واله لیلا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گاه یوسف صفتم جانب زندان بردی</p></div>
<div class="m2"><p>گاه رسوای جهانم چو زلیخا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه چون ویس شدم بسته دام رامین</p></div>
<div class="m2"><p>گاه وامق صفتم طالب عذرا کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه سمندر صفتم صبر بر آتش دادی</p></div>
<div class="m2"><p>گاه از پرتو شمعی زپرم واکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه خفاش صفت دشمن خورشید بجان</p></div>
<div class="m2"><p>گه پرستنده مهرم تو چو حربا کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوختی گاه چو قبطی تنم از شعله طور</p></div>
<div class="m2"><p>سینه ام گه زشرر غیرت سینا کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه ناچار بدرد دل بیمار و علیل</p></div>
<div class="m2"><p>که باحیاء نفوسم چو مسیحا کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>داشتم میل خردمندی و دانش چندی</p></div>
<div class="m2"><p>باز آشفته ام از زلف چلیپا کردی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل آشفته بود خانه مهر حیدر</p></div>
<div class="m2"><p>غدر تو با علی عالی اعلا کردی</p></div></div>