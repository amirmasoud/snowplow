---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>مشک فشان شد زتو باد صبا</p></div>
<div class="m2"><p>ای خم زلفین دو تا مرحبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه عشاق بهم میزند</p></div>
<div class="m2"><p>محرم این حلقه چرا شد صبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ سلیمان توئی ای نیک پی</p></div>
<div class="m2"><p>حجله بلقیس بیار از سبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای منه بر سر میدان عشق</p></div>
<div class="m2"><p>سر بقضا تا ننهی در رضا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاعت و عصیان نخرد شرع عشق</p></div>
<div class="m2"><p>خوف از این نیست وز آنم رجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تازه و تر مانده خطت گرد لب</p></div>
<div class="m2"><p>خصر بود زنده زآب بقا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون شده گرچه دلم از عشق تو</p></div>
<div class="m2"><p>کی بکسی جز تو کند ماجرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زخمی پیکان تو مرهم نخواست</p></div>
<div class="m2"><p>درد تو از کس نپذیرد دوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیب کف پادشهان کی شود</p></div>
<div class="m2"><p>گوی زچوگان نخورد گر قفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست دگر زاهد خلوت نشین</p></div>
<div class="m2"><p>حسن تو چون پرده کشد برملا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از همه بیگانه شدم در جهان</p></div>
<div class="m2"><p>تا بسگان تو شدم آشنا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دم مزن آشفته چو خم لب به بند</p></div>
<div class="m2"><p>کاز دهل از کوفتن آید صدا</p></div></div>