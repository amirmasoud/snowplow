---
title: >-
    شمارهٔ ۵۵۹
---
# شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>دیده برهم ننهم جز به حضور اغیار</p></div>
<div class="m2"><p>نکنم خواب مگر زیر خم تیغ نگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام شیرین نکنم جز بغم تلخی عشق</p></div>
<div class="m2"><p>بزم رنگین نکنم جز بجمال دلدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیش مشتاق نباشد بجز از ساحت دوست</p></div>
<div class="m2"><p>عاشقان را چه تمنای گل و باغ و بهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توئی آن تازه بهاری که تفاوت نکند</p></div>
<div class="m2"><p>در رخ و زلف تو سنجند اگر لیل و نهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای زآهوت بخون غرقه غزال ختنی</p></div>
<div class="m2"><p>ای خم زلفت کجت کعبه مشک تاتار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش روی تو صنم هر که برد تحفه بچین</p></div>
<div class="m2"><p>در برش سجده نمایند بتان فرخار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی تو نیش است و صداع است و خمار و غم و رنج</p></div>
<div class="m2"><p>نای و نوش و می و مطرب دف و عود و مزمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تو باغست و بهار است و گل و لاله و مل</p></div>
<div class="m2"><p>فصل دی داغ درون زهر بلا زحمت و خار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا تو ای برق جهانسوز کنی جلوه بدشت</p></div>
<div class="m2"><p>خرمن هستی آشفته بود وقف شرار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بسوزیش شود فانی و خاکستر او</p></div>
<div class="m2"><p>ببرد باد صبا بر در حیدر چو غبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرده دار حرم سر خدا مظهر حق</p></div>
<div class="m2"><p>کآفتابش به در خانه بود چون مسمار</p></div></div>