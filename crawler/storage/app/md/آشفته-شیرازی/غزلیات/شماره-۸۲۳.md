---
title: >-
    شمارهٔ ۸۲۳
---
# شمارهٔ ۸۲۳

<div class="b" id="bn1"><div class="m1"><p>نوبتی نو میزنی ای نوبتی امشب بنام</p></div>
<div class="m2"><p>این چه شادی بود و این نوبت چه وین عشرت کدام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست عیدی تازه یا نوروز فیروزی طلب</p></div>
<div class="m2"><p>مژده فتح است این یا نوبت دولت بنام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفی آسا از تو در رقصند ذرات وجود</p></div>
<div class="m2"><p>بازگو کاین عید فرخ روز را آخر چه نام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چه شد کامد صلای عیش عام از محتسب</p></div>
<div class="m2"><p>مطربان را چنگ بر کف ساقیانرا می بجام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مغبچه حوری و غلمان می شراب کوثری</p></div>
<div class="m2"><p>میفروشان همچو رضوان میکده دارالسلام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا رندی فشاند آستین بر زاهدی</p></div>
<div class="m2"><p>هر کجا مستی کشد از شیخ و واعظ انتقام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ بسته خانقه کنجی گرفته سوگوار</p></div>
<div class="m2"><p>میکشان را دل شکسته میگساران شادکام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آری آری غاصب حق علی شد در جهیم</p></div>
<div class="m2"><p>لاجرم بر شیعیان لازم بود عیش مدام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیز آشفته بزن جامی و دستی برفشان</p></div>
<div class="m2"><p>دوست را عشرت حلال و عیش بر دشمن حرام</p></div></div>