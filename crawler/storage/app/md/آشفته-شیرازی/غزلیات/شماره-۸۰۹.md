---
title: >-
    شمارهٔ ۸۰۹
---
# شمارهٔ ۸۰۹

<div class="b" id="bn1"><div class="m1"><p>چنان بطره لیلای خویش مفتونم</p></div>
<div class="m2"><p>که در فنون جنون اوستاد مجنونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زپرده های درون ای مژه چه میجوئی</p></div>
<div class="m2"><p>که غنچه وش نبود غیر قطره خونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار زآن می گلرنگ ساقی مجلس</p></div>
<div class="m2"><p>که کام تلخ بود عمرها زافیونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه احتیاج باین نه سرادق نیلی</p></div>
<div class="m2"><p>که خیمه هاست بسی بر فراز گردونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که یار بکامست و می بجام امشب</p></div>
<div class="m2"><p>چه شکرها که بگویم زبخت میگونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکه در دشت جنون پیشرو مجنونم</p></div>
<div class="m2"><p>شاید ار لیلی ایام شود مفتونم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار لب بر لب من دارد و مست از می غیر</p></div>
<div class="m2"><p>خون بدل جان بلب از آن دو لب میگونم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرقه بحر خودی شد تن من نوح کجاست</p></div>
<div class="m2"><p>تا از این ورطه مگر رخت کشد بیرونم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید کس نستاند زگرو تا صف حشر</p></div>
<div class="m2"><p>منکه خود در عوض خرقه بمی مرهونم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم ای عشق گرامی ملکی یا انسان</p></div>
<div class="m2"><p>گفت از دایره کون و مکان بیرونم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنگ بر دامن حیدر زده آشفته بجد</p></div>
<div class="m2"><p>تا رهائی دهد از منت دهر دونم</p></div></div>