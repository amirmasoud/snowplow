---
title: >-
    شمارهٔ ۷۱۰
---
# شمارهٔ ۷۱۰

<div class="b" id="bn1"><div class="m1"><p>تا در آیینه رویت صنما می‌نگرم</p></div>
<div class="m2"><p>کافرم گر به جز از نور خدا می‌نگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ظاهر بکنم دیده دل باز کنم</p></div>
<div class="m2"><p>تا نگویی به تو از نفس و هوا می‌نگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه آید ز تو گر خود همه جور است و جفا</p></div>
<div class="m2"><p>هم در آن جور اثر مهر و وفا می‌نگرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر خطت ننماید به من آن چشمه نوش</p></div>
<div class="m2"><p>با وجود لبت از آب بقا می‌نگرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک غمزه کند اخراجم از آن چین دو زلف</p></div>
<div class="m2"><p>گر به آهوی دو چشمت به خطا می‌نگرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو ای کعبه صلا دادیم از بهر طواف</p></div>
<div class="m2"><p>نیستم حاجی اگر بر سر و پا می‌نگرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره کعبه گذارم به خرابات افتاد</p></div>
<div class="m2"><p>این بصیرت ز کجا بوده کجا می‌نگرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکر درمان چه کنی از پی بیماری دل</p></div>
<div class="m2"><p>دردمند تو نیم گر به دوا می‌نگرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌رود جانم و جانان ز قفا آشفته</p></div>
<div class="m2"><p>نیست اندیشه بیشم به قفا می‌نگرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من علی را نستایم به خدایی اما</p></div>
<div class="m2"><p>اندر آیینه رویش به خدا می‌نگرم</p></div></div>