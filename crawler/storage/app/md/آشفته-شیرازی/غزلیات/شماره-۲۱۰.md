---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>فکرم دقیق گشت بسی در میان دوست</p></div>
<div class="m2"><p>نه زان میان خبر شد و نه از دهان دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمی خدایرا بمن ای باغبان یار</p></div>
<div class="m2"><p>خاشاک چون برون بری از بوستان دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاشا که گنجد آن بقلم یا که بر زبان</p></div>
<div class="m2"><p>سری که در میان منست و میان دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که چیست آن مژه بر دست ترک چشم</p></div>
<div class="m2"><p>تیری زغمزه مینهد اندر کمان دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضری اگر حیات ابد خواست از خدا</p></div>
<div class="m2"><p>خواهد برای آنکه شود جانفشان دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی اگر سواره بخاکم گذر کنی</p></div>
<div class="m2"><p>خیزم چو گرد و باز بگیرم عنان دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوئی که شکر است برآمیخته بزهر</p></div>
<div class="m2"><p>از بس که نام غیر رود بر زبان دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خاک میشوم بهل اینجا روم بخاک</p></div>
<div class="m2"><p>ای پاسبان مران تو مرا زآشینان دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هندو در آفتاب نشان تو جسته است</p></div>
<div class="m2"><p>هر ذره ای که هست درو چون نشان دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترسم که مهربان برقیبان شود دلش</p></div>
<div class="m2"><p>نامهربان کنیم دل مهربان دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته در طلب پی آنزلف خم بخم</p></div>
<div class="m2"><p>سر میدود چو گوی پی صولجان دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حب علی بعشق نکویان مرا فکند</p></div>
<div class="m2"><p>دشمن پرست گشته دل اندر گمان دوست</p></div></div>