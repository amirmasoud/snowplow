---
title: >-
    شمارهٔ ۹۵۰
---
# شمارهٔ ۹۵۰

<div class="b" id="bn1"><div class="m1"><p>مهی تابیده از مشکوی مشکین</p></div>
<div class="m2"><p>که شکر خنده اش را بند شیرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو صد چین نافه دارد هر غزالش</p></div>
<div class="m2"><p>اگر چین را بود آهوی مشکین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلش از سنگ و سر پنجه زفولاد</p></div>
<div class="m2"><p>حریر اندام دارد سینه سیمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگارینا نخواهد خون بها کس</p></div>
<div class="m2"><p>برآور زآستین دست نگارین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عیان عکس رخت زآئینه دل</p></div>
<div class="m2"><p>چو عکس باده از جام بلورین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل انداما بغیری تا هم آغوش</p></div>
<div class="m2"><p>مرا از خارو خارا گشته بالین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا نیش است بی تو نوش دارو</p></div>
<div class="m2"><p>ولی با توست نیشم راح نوشین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب کهنه و یار نوات باد</p></div>
<div class="m2"><p>زسر پروای جانبازان دیرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مه و پروین گواه اشک و آهم</p></div>
<div class="m2"><p>که هر شب بگذرد از ماه و پروین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنه گر مرد راهی دین و دل را</p></div>
<div class="m2"><p>عروس عشق را این است کابین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مکانت لامکان دادند ای عشق</p></div>
<div class="m2"><p>که در امکان نمیگنجی زتمکین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو سراللهی و آئینه حق</p></div>
<div class="m2"><p>نمی بیند کست جز چشم حق بین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منم مجنون لیلای ولایت</p></div>
<div class="m2"><p>نه فرهادم که بازم جان شیرین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غریب افتاده آشفته بکویت</p></div>
<div class="m2"><p>ترحم کن که درویش است و مسکین</p></div></div>