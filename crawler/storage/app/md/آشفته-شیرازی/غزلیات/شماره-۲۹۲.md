---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>چه شد آن فتنه که ناگاه زمحفل برخاست</p></div>
<div class="m2"><p>که زبرخواستنش طاقتم از دل برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتشی بود نهان در دل تنگم چون شمع</p></div>
<div class="m2"><p>بازم آتش بسرآمد چو زمحفل برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک و حوری و غلمان شمرندش از خویش</p></div>
<div class="m2"><p>آن پریزاده گل چهره که از گل برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من گرفتم که بصورت تو زمحفل رفتی</p></div>
<div class="m2"><p>کی زدل نقش تو ای کعبه مقبل برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصف آن لب زلب غیر شنیدم امشب</p></div>
<div class="m2"><p>وه که آب خضر از زهر هلاهل برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک چمن سرو چمانی تو و یک جنت حور</p></div>
<div class="m2"><p>آدمی کی به چنین شکل و شمایل برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ارنی گفت در این بادیه مجنون چو کلیم</p></div>
<div class="m2"><p>پرتو عارض لیلی چو زمحمل برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تو ای بحر محبت چه اثر بود که نوح</p></div>
<div class="m2"><p>دید طوفان تو از کشتی و ساحل برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من زخون خواهی خود دم نزدم پیش کسی</p></div>
<div class="m2"><p>جرم خونی است که از پنجه قاتل برخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفع دیوانگی از سلسله شد آشفته</p></div>
<div class="m2"><p>همه دیوانگی ما زسلاسل برخاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شعله عشق تو جانان تن و جان جمله بسوخت</p></div>
<div class="m2"><p>شکر لله زمیان پرده و حایل برخاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که بسته بمیان رشته مهر حیدر</p></div>
<div class="m2"><p>از سر سبحه و زنار و حمایل برخاست</p></div></div>