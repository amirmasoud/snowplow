---
title: >-
    شمارهٔ ۷۲۴
---
# شمارهٔ ۷۲۴

<div class="b" id="bn1"><div class="m1"><p>ایکه شد ما در فکرت پی وصف تو عقیم</p></div>
<div class="m2"><p>عقل در معرفت ذات تو چون رای سقیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر من قابل فتراک کمند تو نبود</p></div>
<div class="m2"><p>لاجرم در سم گلگون تو کردم تسلیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ شب گر بتواند صفت مهر منیر</p></div>
<div class="m2"><p>ره سوی کنه کمال تو برد وهم حکیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمزه از جا ببرد خلق و گرنه این کار</p></div>
<div class="m2"><p>ناید از نرگس مکحول و زابروی و سیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که آن لعل سخن گوی تو آمد بحدیث</p></div>
<div class="m2"><p>شده از گفته تو حادث اسرار قدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل در پرده عشاق ندارد راهی</p></div>
<div class="m2"><p>پرده عصمت کبری ندرد دیو رجیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایشه ملک حقیقت علی ای معنی عشق</p></div>
<div class="m2"><p>که بود عقل بخاک سر کوی تو مقیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید آشفته زحب تو درآید به بهشت</p></div>
<div class="m2"><p>داخل خلد نشد جز سگ اصحاب رقیم</p></div></div>