---
title: >-
    شمارهٔ ۱۰۶۴
---
# شمارهٔ ۱۰۶۴

<div class="b" id="bn1"><div class="m1"><p>ای هشت باب خلد ز روی تو آیتی</p></div>
<div class="m2"><p>وز قامت تو شور قیامت کنایتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تفسیر سر آیه نورش شود عیان</p></div>
<div class="m2"><p>هر کاو ز مصحف رخ تو خواند آیتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانده به این امید خضر زنده در جهان</p></div>
<div class="m2"><p>کز چشمه حیات تو بیند سقایتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرچم به فرق مهر و مه از غالیه که زد</p></div>
<div class="m2"><p>جز تو که برفراشتی از زلف رایتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر هوای حسن ازل عشق شد پدید</p></div>
<div class="m2"><p>آن را نهایتی نه و این را بدایتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقم خراب کرده و آری چنین کند</p></div>
<div class="m2"><p>چون پادشه به قهر بگیرد ولایتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمرم به سر رسید و نشد شام هجر صبح</p></div>
<div class="m2"><p>گوئی ندارد این شب تیره نهایتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر ای خط چو گرد لبت را گرفت گفت</p></div>
<div class="m2"><p>این چشمه جز به خضر ندارد کفایتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد نافه خون به ناف غزال ختا و چین</p></div>
<div class="m2"><p>کرده صبا ز زلف تو گویا حکایتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون شد که شد ز خون کسان پنجه‌ات خضاب؟</p></div>
<div class="m2"><p>در حق من رقیب نکرد ار سعایتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما کِشته تخم و منتظر ابر رحمتیم</p></div>
<div class="m2"><p>ای پادشه به کِشت رعیب رعایتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشفته را به کوی مغان راهبر که شد؟</p></div>
<div class="m2"><p>گر پیر می‌فروش نکردش هدایتی</p></div></div>