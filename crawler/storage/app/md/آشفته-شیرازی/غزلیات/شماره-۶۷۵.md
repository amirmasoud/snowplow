---
title: >-
    شمارهٔ ۶۷۵
---
# شمارهٔ ۶۷۵

<div class="b" id="bn1"><div class="m1"><p>من و شمع دوش حرفی بمیان نهاده بودیم</p></div>
<div class="m2"><p>دو زبان آتش افشان به بیان گشاده بودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمن آتشی بجست و بنشست در دل شمع</p></div>
<div class="m2"><p>بمقابله قراری چو بهم نهاده بودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب بسوخت شمع و بگریست تا سحرگه</p></div>
<div class="m2"><p>به نسیم صبح هر دو ززبان فتاده بودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنوای مرغ شب خیز که کوفت نوبت بام</p></div>
<div class="m2"><p>زده عطسه ای بجستیم که قرار داده بودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زگلی حدیث کرد او من و دل زگلعذاری</p></div>
<div class="m2"><p>بعیار عشق دلبر من و دل زیاده بودیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگزید او تعلق من و عشق ساده رویان</p></div>
<div class="m2"><p>من و شیخ هر دو آشفته نخست ساده بودیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من و کوی میفروشان تو و خانقاه زاهد</p></div>
<div class="m2"><p>بکه خضر رهنمون گشت که ما بجاده بودیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسم این تفاخر ایدل که بخواب دیدم امشب</p></div>
<div class="m2"><p>که من و سگ در دوست بیک قلاده بودیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بده ای یدالله از لطف مرا کلاه عزت</p></div>
<div class="m2"><p>که به جای پای با سر به در ایستاده بودیم</p></div></div>