---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>ز باغ وصل تو بوی فراق می‌آید</p></div>
<div class="m2"><p>ز نخل تو ثمر افتراق می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر شرار فراق تو زد به باغ که باز</p></div>
<div class="m2"><p>دلم چو لاله‌ای در احتراق می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شام هجر اگر شهد ریزیم در کام</p></div>
<div class="m2"><p>چو صبر و حنظلم اندر مذاق می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفاق‌پیشه سپهرا تو سخت بی‌مهری</p></div>
<div class="m2"><p>ز دوستی تو بوی نفاق می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنند عرضه به تو برگ کاهی ار ز فراق</p></div>
<div class="m2"><p>اگر که کوه شوی طاقتت به طاق آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نی اگر ببری بندبند من در گوش</p></div>
<div class="m2"><p>نوای عاشقی و اشتیاق می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته حسن تو ای ماه چارده چو کمال</p></div>
<div class="m2"><p>عجب نباشد اگر در محاق می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتم ای گل رعنا هزار یارت هست</p></div>
<div class="m2"><p>ولیک همچو منت یار اتفاق می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو روز عید شمارم به خود همایونش</p></div>
<div class="m2"><p>شبی که ماه من اندر وثاق می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبوری از غم هجران یار آشفته</p></div>
<div class="m2"><p>مرا به جان همه تکلیف شاق می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به وصل خود به نوازش وگرنه آشفته</p></div>
<div class="m2"><p>ز پارس شکوه‌کنان در عراق می‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به آستان شه لافتی ولی خدا</p></div>
<div class="m2"><p>که روز حشر به صد طمطراق می‌آید</p></div></div>