---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>گدای میکده در دست جام جم دارد</p></div>
<div class="m2"><p>چه هست جام جهان بین زجم چه کم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنکه جای گرفته بگلخن کویت</p></div>
<div class="m2"><p>کی اشتیاق گل و گلشن ارم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بپای ره نبرد رهروی بکعبه عشق</p></div>
<div class="m2"><p>کسی بسر برد این ره که سر قدم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدیر برهمن و شیخ در حرم در رقص</p></div>
<div class="m2"><p>که ساز عشق بسی نغمه زیر و بم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده بمستی هستی که حاصلی نبری</p></div>
<div class="m2"><p>ازین وجود که مرجع سوی عدم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجو زشیخ حرم راز پرده غیبی</p></div>
<div class="m2"><p>به پیر میکده نازم که این شیم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زممکنات همه حادثند غیر ازعشق</p></div>
<div class="m2"><p>که حادث است ولی جلوه قدم دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعرصه گاه قیامت رود چو آشفته</p></div>
<div class="m2"><p>زاحتساب خدائی شها چه غم دارد</p></div></div>