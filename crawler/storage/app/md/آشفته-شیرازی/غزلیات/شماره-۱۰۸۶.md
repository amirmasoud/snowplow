---
title: >-
    شمارهٔ ۱۰۸۶
---
# شمارهٔ ۱۰۸۶

<div class="b" id="bn1"><div class="m1"><p>تا چند در آزاری ای دل زهوسناکی</p></div>
<div class="m2"><p>حیف است غبار آلود آئینه باین پاکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآلایش فطرت خاک جا کرده باین مرکز</p></div>
<div class="m2"><p>گر تو نهی آلایش روشنتر از افلاکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مهر بتان بگسل کاین امر بود عاطل</p></div>
<div class="m2"><p>زین بحر بجو ساحل با چستی و چالاکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون زجهان ایدل خیز آب و هوائی جو</p></div>
<div class="m2"><p>کاتش بدورن دارم زین سلسله خاکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این خار جهانم کرد و آن رخنه بجانم کرد</p></div>
<div class="m2"><p>از دیده و دل هر روز شاید که شوم شاکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سلسله مویانرا وین سخت کمانانرا</p></div>
<div class="m2"><p>سست است همه پیمان بگذر زهوسناکی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس بی دل رفتی رسوای جهان گشتی</p></div>
<div class="m2"><p>آشفته بنه از سر قلاشی و بی باکی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشقی بطلب سرمد تا بیخ هوس سوزد</p></div>
<div class="m2"><p>کو شعله هستی را خاصیت خاشاکی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رو عالم اکبر جو زود عشق مطهر جو</p></div>
<div class="m2"><p>یعنی که علی عالی کامد زکی و زاکی</p></div></div>