---
title: >-
    شمارهٔ ۱۰۸۴
---
# شمارهٔ ۱۰۸۴

<div class="b" id="bn1"><div class="m1"><p>تو را که گفت کز احباب روی برتابی</p></div>
<div class="m2"><p>بعمد بی گناهانرا بقتل بشتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکوی دوست اگر تیغ بارد از اطراف</p></div>
<div class="m2"><p>نه مردیست که روی از مصاف برتابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان قافله من چون جرس بناله و تو</p></div>
<div class="m2"><p>میان محمل زرین بناز در خوابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجاهدان بغزا خون خصم مینوشند</p></div>
<div class="m2"><p>تو را چه رفت که تشنه بخون احبابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببست گر در دیر و حرم برهمن و شیخ</p></div>
<div class="m2"><p>چه احتیاج که تو قبله گاه اصحابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دید آتش دل گفت مردم چشمم</p></div>
<div class="m2"><p>بیا بیا که بر آتش فشانمت آبی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر زشمع شکایت کنی تو پروانه</p></div>
<div class="m2"><p>برو برو که بدعوی عشق کذابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکیم گفت که الاقصر احسن آشفته</p></div>
<div class="m2"><p>ولی زقصه زلفش سزاست اطنابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بیهده در جنت طلب مکن اعظ</p></div>
<div class="m2"><p>اگر زکوی مغانت گشاده شد بابی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر کجا که شوم خاک می‌روم به نجف</p></div>
<div class="m2"><p>که هست حب علی زر تو خود چو سیمابی</p></div></div>