---
title: >-
    شمارهٔ ۹۳۷
---
# شمارهٔ ۹۳۷

<div class="b" id="bn1"><div class="m1"><p>برق زند بباغ ما ابر بکشت دیگران</p></div>
<div class="m2"><p>نیک بود بفال ما طالع زشت دیگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که مراست خرمنی آتش خانه سوز من</p></div>
<div class="m2"><p>بهر خدا تو نگذری هیچ بکشت دیگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حور فرشته سیرتی آدم حور طینتی</p></div>
<div class="m2"><p>زآب و گل است از ازل جمله سرشت دیگران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشته طره ات بود سبحه ذکر مقبلان</p></div>
<div class="m2"><p>کعبه عاشقان تو هست کنشت دیگران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنت ما وصال تو دوزخ ما فراق تو</p></div>
<div class="m2"><p>گر بقیامت اوفتد نار و بهشت دیگران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفته خاک در لحد لیک برشک تا ابد</p></div>
<div class="m2"><p>گرچه شد آستان تو راست زخشت دیگران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه کتاب خوانده ای در حق بوتراب دان</p></div>
<div class="m2"><p>حجت خویش کرده ام دست تو دست دیگران</p></div></div>