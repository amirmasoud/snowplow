---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>ای کلک قضا را خط تو حاصل تحریر</p></div>
<div class="m2"><p>وز نقش نظیر تو خجل خامه تقدیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دوره زکریاس تو حاشا که کند طی</p></div>
<div class="m2"><p>صددور گر افلاک درآیند بتدویر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای تو افزود جنون کاست دل ودین</p></div>
<div class="m2"><p>گم شد بکف از زلف تو سررشته تدبیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنار ببر بت شکن و سبحه فروهل</p></div>
<div class="m2"><p>تا چند دهی رشته باین دانه تزویر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر گردن خورشید ززلف تو کمندی</p></div>
<div class="m2"><p>برپای مه چارده از خط تو زنجیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخساره نورانی تو آیه نور است</p></div>
<div class="m2"><p>خط تو بر آن آیه نور آمده تفسیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف تو زره ساز چو داود باعجاز</p></div>
<div class="m2"><p>ابروی تو چون تیغ علی گشت جهانگیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف تو چه دامی شده صیاد کدامست</p></div>
<div class="m2"><p>کاهوی سیه مست تواش آمده نخجیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا دور شد از حلقه آنزلف شب آسا</p></div>
<div class="m2"><p>آشفته ندارد بجز از ناله شبگیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سازند زخاکم همه اکسیر و عجب نیست</p></div>
<div class="m2"><p>تا بر من خاکی زده از مهر تو اکسیر</p></div></div>