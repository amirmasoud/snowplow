---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>خیره شد عشق که از عقلم نیرو برود</p></div>
<div class="m2"><p>چون بچوگان بزنی لاجرمت گو برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو آزاده چه حدداشت که آید با تو</p></div>
<div class="m2"><p>پیش رویت بچه جلوه گل خوشبو برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با نسیم سحری بود مگر بوی کسی</p></div>
<div class="m2"><p>که چو شمع سحری جانم با او برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوت پنجه عشق است ببازوی بتان</p></div>
<div class="m2"><p>تو مپندار که این زور زبازو برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیکمرد آنکه چو آشفته گریزد بدرت</p></div>
<div class="m2"><p>گو بیا زشت که از کوی تو نیکو برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه بر دشت ختن آهوی مشکین گذرد</p></div>
<div class="m2"><p>نشنیدم که بمه یک ختن آهو برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوطی باغ ولای توام ای شیر خدا</p></div>
<div class="m2"><p>مهل از گلشنت این مرغ سخن گو برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چه از چشم تر من بکناری ای سرو</p></div>
<div class="m2"><p>سرو گلزار کجا از طرف جو برود</p></div></div>