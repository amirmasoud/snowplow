---
title: >-
    شمارهٔ ۷۲۱
---
# شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>منکه در میکده منزل بود از آغازم</p></div>
<div class="m2"><p>شاید ار شیخ بمسجد نکند در بازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش سرانجام تر از من بخرابات مجوی</p></div>
<div class="m2"><p>کز می ناب سرشتند گل از آغازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من دل خون شده در دست نگارین دیدم</p></div>
<div class="m2"><p>پنجه با ساعد سیمین تو چون اندازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شهیدان چو درآیم بقیامت یکرنگ</p></div>
<div class="m2"><p>داغ عشق تو کند از دگران ممتازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخرت نیز ببازیم بسودای غمت</p></div>
<div class="m2"><p>دینی آنقدر ندارد که به تو در بازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع بزم دگران تا شده ای از غیرت</p></div>
<div class="m2"><p>همه شب شمع صفت مانده بسوز و سازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلفش آشفته گرم دست بگیرد زکرم</p></div>
<div class="m2"><p>خویشتن را بدر پیر مغان اندازم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیمیائی کنم از خاک در دوست بچشم</p></div>
<div class="m2"><p>این مس قلب باکسیر غمت بگدازم</p></div></div>