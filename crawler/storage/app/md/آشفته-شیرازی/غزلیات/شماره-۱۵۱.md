---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>ز لولیان چو بتم شاهدی به شنگی نیست</p></div>
<div class="m2"><p>به رنگ لعبت من لعبت فرنگی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال قافیه جز آن دهان نیندیشم</p></div>
<div class="m2"><p>که هیچ قافیه چو آن هان بتنگی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه رستم دستان بود که کشته تست</p></div>
<div class="m2"><p>که همچو چشم تو در رزم ترک جنگی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زچیست کرده بروی تو جای خال سیاه</p></div>
<div class="m2"><p>بباغ خلد اگر هندوان زنگی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو چنگ ناله آشفته در فلک پیچید</p></div>
<div class="m2"><p>به غیر زهره مگو در سپهر چنگی نیست</p></div></div>