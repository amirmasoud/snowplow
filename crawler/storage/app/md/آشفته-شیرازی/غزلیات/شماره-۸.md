---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>خواهم که در هم بشکنم این طاق مینافام را</p></div>
<div class="m2"><p>زین چارطبع مختلف برجا نمانم نام را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گم شده ره میخانه‌ام از دست شد پیمانه‌ام</p></div>
<div class="m2"><p>دستی بگیر ای نوجوان این پیر دُردآشام را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سگ از ره مهر و وفا هم‌صحبت اصحاب شد</p></div>
<div class="m2"><p>لابد سگ نفس و هوا رسوا کند بلعام را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل به باد صبحدم گفته حدیث از بیش و کم</p></div>
<div class="m2"><p>گل گوش داده تا صبا حالی کند پیغام را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمرت به چل گر می‌رسد چون در فراقش می‌رود</p></div>
<div class="m2"><p>عاشق ز عمر خویشتن گو مشمر این ایام را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد پرنیانی بسترم چون نوک خار و نشترم</p></div>
<div class="m2"><p>تا غیر هم‌آغوش شد یار حریراندام را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانان جان قوت روان روشن از او چشم جهان</p></div>
<div class="m2"><p>رامش گزین دل‌ها از آن کز دل ببرد آرام را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید که درویش سرا از فقر آید در غنا</p></div>
<div class="m2"><p>بر خوان یغما زد صلا سلطان چو خاص و عام را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زاهد ز عشق و عاشقی فراروش هارب بود</p></div>
<div class="m2"><p>بر آتش سودای تو پایی نباشد خام را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته مستوری مکن از می‌کشان دوری مکن</p></div>
<div class="m2"><p>ساقی به یاد مرتضی در دور افکن جام را</p></div></div>