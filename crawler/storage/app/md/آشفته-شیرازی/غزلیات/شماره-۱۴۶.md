---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>دلی که روز و شبان از پی نظر می‌گشت</p></div>
<div class="m2"><p>ز زخم تیزنظر دوش بی‌خبر می‌گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که پا نکشیدی ز کعبه در همه عمر</p></div>
<div class="m2"><p>به طوف میکده می‌دیدمش به سر می‌گشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلی مسیر قمر عقربست و این عجبست</p></div>
<div class="m2"><p>که دوش عقرب زلف تو بر قمر می‌گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تو موی میان را به جلوه ننمودی</p></div>
<div class="m2"><p>کس این خیال نکردی که مو کمر می‌گشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخوردی آب نی کلکم ار ز چشمه خضر</p></div>
<div class="m2"><p>نه میوه سخنش تازه بود و تر می‌گشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان به بزم طرب دوش چنگ زد مطرب</p></div>
<div class="m2"><p>که گوش زهره ز مزمار و عود کر می‌گشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه بود لذت این سوختن که آشفته</p></div>
<div class="m2"><p>نهاد خرمن و اندر پی شرر می‌گشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حساب حشر نداشتم چه می‌شدی با خلق</p></div>
<div class="m2"><p>اگر نه حیدر کرار دادگر می‌گشت</p></div></div>