---
title: >-
    شمارهٔ ۹۶۰
---
# شمارهٔ ۹۶۰

<div class="b" id="bn1"><div class="m1"><p>ما دردمند عشقیم عطار که دوا کو</p></div>
<div class="m2"><p>گمگشتگان راهیم آنخضر ره نما کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کعبه با خرابات داری اگر چه دعوی</p></div>
<div class="m2"><p>کو آن مقام امن و در مروه ات صفا کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهد مرا نپائی بر غیر آختی تیغ</p></div>
<div class="m2"><p>گیرم وفا نداری ای نازنین جفا کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود اثر زلیلی ناید صدای مجنون</p></div>
<div class="m2"><p>گر غافله گذشته پس ناله درا کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم حدیث هجرش پرسم زمردم چشم</p></div>
<div class="m2"><p>آمد بموج اشکم هنگام ماجرا کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق بباخت جان و دارد رقیب دعوی</p></div>
<div class="m2"><p>آن صدق را جزا چیست و آن کذب را سزا کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمش بزلف میگفت تو نافه من غزالم</p></div>
<div class="m2"><p>با این اثر که در ماست آهو کجا ختا کو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب و هوای شیراز گفتند عیش خیز است</p></div>
<div class="m2"><p>عیش و طرب کجا رفت آن آب و آن هوا کو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاک سرای حیدر مفتاح مشکلات است</p></div>
<div class="m2"><p>مشکل شده است کارم آشفته آنسرا کو</p></div></div>