---
title: >-
    شمارهٔ ۷۴۹
---
# شمارهٔ ۷۴۹

<div class="b" id="bn1"><div class="m1"><p>ببست غمزه شوخت بزلف دلبندم</p></div>
<div class="m2"><p>بخویش بست و زعالم گسست پیوندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکشتزار درون تخم مهرت افکندم</p></div>
<div class="m2"><p>نهال دوستی این و آن زدل کندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که خاک در دوست داده اند بنقد</p></div>
<div class="m2"><p>اگر بهشت بیاری بها که نپسندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که هیچ نسودم لبی بر آن لب نوش</p></div>
<div class="m2"><p>چرا بحق نمک میدهی تو سوگندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خبر نداری از اسرار ذوق مستی عشق</p></div>
<div class="m2"><p>به بیهده مده ای شیخ بیخبر پندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیوسف دگران مایلم نه چون یعقوب</p></div>
<div class="m2"><p>که جا بسینه کند مهر روی فرزندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مراست لذتی از چاشنی تیر نگاه</p></div>
<div class="m2"><p>بیا بیا که بآن نشئه آرزومندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر شکایتی آشفته کردم از زخمش</p></div>
<div class="m2"><p>نیم صدیق که دعوی بخوبش میبندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بتا زهجر لبان تو تلخ شد کامم</p></div>
<div class="m2"><p>بیار بوسه ای از آن لب شکر خندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برای آنکه شوم سرفراز در صف حشر</p></div>
<div class="m2"><p>بپای دلدل حیدر چو گو سرافکندم</p></div></div>