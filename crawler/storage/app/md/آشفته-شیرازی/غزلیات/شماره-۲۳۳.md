---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>هوس ساده زخامم سر سودائی سوخت</p></div>
<div class="m2"><p>شرر عشق بتان خرمن دانائی سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوق شکر دهنان دوخت لب گفتارم</p></div>
<div class="m2"><p>طوطی طبع مرا قوه گویائی سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنشین در قفس ایدل زتماشا بگذر</p></div>
<div class="m2"><p>کآه بلبل بچمن خیل تماشائی سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک در کشور یغما چو تو یغمائی نیست</p></div>
<div class="m2"><p>کزدم تیغ کجت چینی و یغمائی سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقی از آه دل خسته برون جست بدشت</p></div>
<div class="m2"><p>دل کوه از شررش با همه خارائی سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود بیاراست در آئینه بر آتش زد مشک</p></div>
<div class="m2"><p>آینه دید چون آن طرز خود آرائی سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگر زاهد اگر سخت بکانون زریا</p></div>
<div class="m2"><p>خرقه رند قدح خوار برسوائی سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چه باده است که تاریخت زمینا در جام</p></div>
<div class="m2"><p>اثر آتش او گنبد مینائی سوخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزن آبی بدل سوخته آشفته</p></div>
<div class="m2"><p>کاندر آتش بهوای بت هر جائی سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شمه عشق تو روزی به نیستان گفتم</p></div>
<div class="m2"><p>آتشی خاست زنی کز اثرش نائی سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواست دیده که شود آگه از نور علی</p></div>
<div class="m2"><p>چون تف چشمه خور حاصل بینائی سوخت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجبی نیست اگر سوخت مگس را پر و بال</p></div>
<div class="m2"><p>شکری بود کزو دکه حلوائی سوخت</p></div></div>