---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>ببزم جلوه گر ار روی ماه من باشد</p></div>
<div class="m2"><p>چه جای ماه فلک شمع انجمن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواص خویش دهد لیک دیو جم نشود</p></div>
<div class="m2"><p>نگین جم که در انگشت اهرمن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زره زحلقه زلفین تو بتن پوشم</p></div>
<div class="m2"><p>اگر کمان قضا در کمین من باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرفته خال و خطت گرد آن لب شکرین</p></div>
<div class="m2"><p>خوش اتحادی در طوطی و زغن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دیده سرو چمن را کله نهاده بسر</p></div>
<div class="m2"><p>که دیده ماه فلک را که در سخن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زباغ خلد بدامت نشست آشفته</p></div>
<div class="m2"><p>که چین حلقه زلف تواش وطن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو تو حرفی و خلق ازگمان برون آور</p></div>
<div class="m2"><p>که هر چه دل همه خواهد در آن دهن باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه غم زتابش خورشید در صف محشر</p></div>
<div class="m2"><p>مرا که تکیه به سلطان اباالحسن باشد</p></div></div>