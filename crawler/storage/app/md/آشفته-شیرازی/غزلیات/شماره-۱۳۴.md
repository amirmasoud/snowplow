---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>آمد زدرم خراب و سرمست</p></div>
<div class="m2"><p>شیشه بکف و پیاله در دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان فتنه که کرده بود برپا</p></div>
<div class="m2"><p>کردیم هزار سعی و ننشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از منظر خوب ماهرویان</p></div>
<div class="m2"><p>حاشا که ره نظر توان بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون دل ما بخورد چشمت</p></div>
<div class="m2"><p>پرهیز کجا زمی کند مست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای کوی مغان چه رفعتست این</p></div>
<div class="m2"><p>کت عرش برین بخاک پستست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر از در پیر میفروشان</p></div>
<div class="m2"><p>حاشا که مرا در دگر هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن پیر طریقت و حقیقت</p></div>
<div class="m2"><p>کش شرع مبین بخانه بنشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته شوی بدوست ملحق</p></div>
<div class="m2"><p>وقتی که زخویشتن توان رست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سلسله فارغ است فردا</p></div>
<div class="m2"><p>هر کس که بحلقه تو پیوست</p></div></div>