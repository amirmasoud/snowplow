---
title: >-
    شمارهٔ ۶۲۶
---
# شمارهٔ ۶۲۶

<div class="b" id="bn1"><div class="m1"><p>بازآ صنما نرمک، بنشین به سرا خوش‌خوش</p></div>
<div class="m2"><p>گه بذله شیرین گو گه باده رنگین کش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه فصل دیست آخر نه وقت میست آخر</p></div>
<div class="m2"><p>پس وقت کیست آخر مخموری و شو سرخوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سرو چمن رفته تو سرو چمان بازآ</p></div>
<div class="m2"><p>ور رفته گل حمرا تو پرده زرخ برکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون غنچه چه دلتنگی خندیده چو گل ساغر</p></div>
<div class="m2"><p>از سردی دی غم نیست با مشعله آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی چو بود صافی از می نکند پرهیز</p></div>
<div class="m2"><p>زآتش نکند پرهیز البته زر بیغش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرداخته بزم از غیر ظلمست نخوردن می</p></div>
<div class="m2"><p>با چون تو بتی ساده یا چون تو نگاری کش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پاکیزه تو را دامن پاکست مرا دیده</p></div>
<div class="m2"><p>بازآ که بنظم آریم با هم غزلی دلکش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مدحت شیر حق آن پادشه مطلق</p></div>
<div class="m2"><p>کاوراست سمند چرخ در رزم کمین ابرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ناحیه امکان یک صید بجا نبود</p></div>
<div class="m2"><p>ای سخت کمان زین تیر داری چو تو در ترکش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی مینهدت حیدر کافتی بجحیم اندر</p></div>
<div class="m2"><p>هر چند سزا باشد آشفته تو را آتش</p></div></div>