---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>صیاد زد بتیرم وبالم شکست و رفت</p></div>
<div class="m2"><p>از کثرت شکار ببندم نبست و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صد نیاز خواستم از وی نشستی</p></div>
<div class="m2"><p>آمد بناز بر سرم و برنشست و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عقل بس عقال نهادم بپای دل</p></div>
<div class="m2"><p>دستی نمود و رشته عقلم گسست و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزدیک بود زخم درون به شود که یار</p></div>
<div class="m2"><p>آمد بنوک تیر نظرباز خست و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم جگر کباب و زخون درون شراب</p></div>
<div class="m2"><p>خورد آن شراب بامزه گردید مست و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش بتان بشستم و شد جلوه گر بتی</p></div>
<div class="m2"><p>آشفته را نمود زنو بت پرست و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشغول غیر بود که حیدر پدید شد</p></div>
<div class="m2"><p>آورد بازیاد زعهد الست و رفت</p></div></div>