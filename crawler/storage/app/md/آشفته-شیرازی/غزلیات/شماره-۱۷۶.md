---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>خلق مشتاق و ندیده رخ همچون قمرت</p></div>
<div class="m2"><p>نه مباح است در این ماه من سفرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناقه رهوارو تو لیلی صفت اندر محمل</p></div>
<div class="m2"><p>دل من چو سگ لیلی زقفای اثرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا میان تنگ نه بستی پی خون ریختنم</p></div>
<div class="m2"><p>نشد آگه دل سرگشته زسر کمرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی منظر تو بی بصران کی دانند</p></div>
<div class="m2"><p>نشناسد بحقیقت مگر اهل نظرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر متاع دو جهان سربسر آرند بها</p></div>
<div class="m2"><p>نفروشم برضا یکسر موئی زسرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرنه آتشکده شد سینه ات آشفته چرا</p></div>
<div class="m2"><p>برق سان شعله فرومیچکد از شعر ترت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو همه شب می گلگون کشی از ساغر غیر</p></div>
<div class="m2"><p>چه غم ار خون بخورد عاشق خونین جگرت</p></div></div>