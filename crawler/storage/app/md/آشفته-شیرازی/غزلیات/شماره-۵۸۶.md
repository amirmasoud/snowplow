---
title: >-
    شمارهٔ ۵۸۶
---
# شمارهٔ ۵۸۶

<div class="b" id="bn1"><div class="m1"><p>امشب ایشمع انجمن افروز</p></div>
<div class="m2"><p>پر پروانه را زمهر مسوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بوصل تو خوش بود یکشب</p></div>
<div class="m2"><p>گو بسوزد دگر همه شب و روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بامدادش تو در سرا آئی</p></div>
<div class="m2"><p>هر کرا کوکبی بود فیروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم عاشق چو دوخت بر رخ دوست</p></div>
<div class="m2"><p>بر نگیرد بناوک دلدوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید حربا چو پرتو خورشید</p></div>
<div class="m2"><p>نتوان گفتمش که دیده بدوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل آشفته بسته ای بکمند</p></div>
<div class="m2"><p>کس نه بستست مرغ دست آموز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر حیدر بورز و می‌خور فاش</p></div>
<div class="m2"><p>بگذر از زاهد ریا اندوز</p></div></div>