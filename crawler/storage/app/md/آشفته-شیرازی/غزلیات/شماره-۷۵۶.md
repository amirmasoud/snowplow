---
title: >-
    شمارهٔ ۷۵۶
---
# شمارهٔ ۷۵۶

<div class="b" id="bn1"><div class="m1"><p>ای آفت دین و خصم اسلام</p></div>
<div class="m2"><p>ای فتنه دهر و شور ایام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای رهزن پارسا و زاهد</p></div>
<div class="m2"><p>ای صوفی شهر از تو بدنام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که بکوی من وطن کن</p></div>
<div class="m2"><p>شاید بسگان من شوی رام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا پوست و استخوانیم بود</p></div>
<div class="m2"><p>کردند از او نهار یا شام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اکنون که زهستی اوفتادم</p></div>
<div class="m2"><p>رانند مرا زهر در و بام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ماندم و دل که صید وحشی است</p></div>
<div class="m2"><p>با هیچکسی نمیشود رام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو سر خوش و با رقیب در بزم</p></div>
<div class="m2"><p>گه بوسه دهی و گه کشی جام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا مرغ اسیر خودرها کن</p></div>
<div class="m2"><p>یا زود بکش که گیرد آرام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آتش ما خبر ندارد</p></div>
<div class="m2"><p>مشنو تو حدیث زاهد خام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته زوصل روی خوبان</p></div>
<div class="m2"><p>خواهی که بری تمتع و کام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرواز کن از دیار شیراز</p></div>
<div class="m2"><p>بر طوف در نجف کن اقدام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ورنه بنشین و از خم دل</p></div>
<div class="m2"><p>چون من قدحی زخون بیاشام</p></div></div>