---
title: >-
    شمارهٔ ۱۱۵۲
---
# شمارهٔ ۱۱۵۲

<div class="b" id="bn1"><div class="m1"><p>سودای تو آتش زده در رخت صبوری</p></div>
<div class="m2"><p>از آب کجا تشنه کند صبر به دوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای از دل اغیار برون نه که بریبی</p></div>
<div class="m2"><p>در خانه ظلماتی ای پیکر نوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاموشم اگر ناله من گوش تو آزرد</p></div>
<div class="m2"><p>مردم اگر از هستی عاشق بنفوری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین داغ جگرسوز که دارم عجبی نیتس</p></div>
<div class="m2"><p>گر خاک مزارم بدماند گل سوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید بود پیش مه روی تو ذره</p></div>
<div class="m2"><p>غلمان بهشتیست غلام چو تو حوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کفر است بجز مهر تو در کیش محبت</p></div>
<div class="m2"><p>احباب تو را هست ولای تو ضروری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای عالم اسرار نهان پرده برانداز</p></div>
<div class="m2"><p>کاشفته بخواند زرخت علم حضوری</p></div></div>