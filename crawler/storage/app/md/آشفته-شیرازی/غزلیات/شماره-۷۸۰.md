---
title: >-
    شمارهٔ ۷۸۰
---
# شمارهٔ ۷۸۰

<div class="b" id="bn1"><div class="m1"><p>بباغ عشق بجز میوه فراق ندیدم</p></div>
<div class="m2"><p>زجان گذشته و جانانه را بوصل رسیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدل گذشتم و پیمان دلبران نشکستم</p></div>
<div class="m2"><p>زجان بریدم و از عهد دوستی نبریدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بترک دوستیم دشمنان اگر چه بگفتند</p></div>
<div class="m2"><p>بجان دوست که افسوس دشمنان نشنیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گسستم از دو جهان و بزلف یار ببستم</p></div>
<div class="m2"><p>فروختم دل و دین عشق او بجان خریدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببوی دانه خالت ببند عشق بماندم</p></div>
<div class="m2"><p>زدام عقل زوحشت چو آهوان برمیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمان کشیده نگاه تو تا از آن خم ابرو</p></div>
<div class="m2"><p>هزار تیر بجان خورده روی در نکشیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار سال بکویش نشسته گفت کدامی</p></div>
<div class="m2"><p>هزار ره بسرم پا نهاد و گفت ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبود آن خم چوگان زلف بر سر رحمت</p></div>
<div class="m2"><p>مگو چو گوی و بسر در ره طلب ندویدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زتلخ کامی عقلم خمار برد سحرگه</p></div>
<div class="m2"><p>زلعل ساقی مجلس شراب عشق چشیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بجای کوی مغان شیخ شهر از سر رأفت</p></div>
<div class="m2"><p>بهشت عدن بمن عرضه کرد و برنگزیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کدام کوی مغان آستان شاه ولایت</p></div>
<div class="m2"><p>که کرده کوثرش آسوده از شراب نبیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کبوتر حرم مرتضی است آشفته</p></div>
<div class="m2"><p>که جز به گرد در و بام کعبه‌اش نپریدم</p></div></div>