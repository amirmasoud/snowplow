---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>پسته دهن بسته زخندیدنت</p></div>
<div class="m2"><p>آینه حیران شده از دیدنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون بدل کبک دری میکنی</p></div>
<div class="m2"><p>آه از این طرز خرامیدنت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساخته با بوی خوشت بلبلان</p></div>
<div class="m2"><p>چون نرسد دست بگلچیدنت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواست بسنجد رخ تو با قمر</p></div>
<div class="m2"><p>عقل فرماند زسنجیدنت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قتل جهان بهر تو گر راحتست</p></div>
<div class="m2"><p>نیست کسی را سر رنجیدنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاطر مطبوع تو مشگل پسند</p></div>
<div class="m2"><p>کس چکند بهتر پسندیدنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاغ فزون شد بچمن عندلیب</p></div>
<div class="m2"><p>نیست بگل وقت سرائیدنت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نعل بر آتش چو نهد زلف تو</p></div>
<div class="m2"><p>غمزه بس از بهر گرائیدنت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند زلیخا تو زنی لاف عشق</p></div>
<div class="m2"><p>نیست زیوسف سر پرسیدنت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زلف وی آشفته بدست رقیب</p></div>
<div class="m2"><p>ماند و بخود باید پیچیدنت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دامن حیدر بکف افتد اگر ‏</p></div>
<div class="m2"><p>از همه بایست که ببریدنت</p></div></div>