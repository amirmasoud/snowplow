---
title: >-
    شمارهٔ ۴۵۶
---
# شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>تکوین خیر و شر نه زشمس و قمر بود</p></div>
<div class="m2"><p>عشقست و بس که صادر از او خیر و شر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآنزلف پر شکن بود و چشم فتنه خیز</p></div>
<div class="m2"><p>آشوب فتنه ای که بدور قمر بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقی که سوخت بیخ هوس خیر عاشقست</p></div>
<div class="m2"><p>ور تابع هواست زشرش اثر بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردد خبر زفکر دقیق مهندسان</p></div>
<div class="m2"><p>دستی که با خیال تو شب در کمر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهم کشید شعله که سوزد جهان تمام</p></div>
<div class="m2"><p>بس منتم بجان و دل از چشم تر بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردم شکایتی زخم زلف تو بحشر</p></div>
<div class="m2"><p>ناگفته ماند قصه زمان مختصر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمایه تجارت عشق است جان و سر</p></div>
<div class="m2"><p>عاشق نه قید نفع و غمین از ضرر بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایدیده طفل اشک بدامان چه پروری</p></div>
<div class="m2"><p>بیرون کنش زخانه که پر پرده در بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید علاج زخمی زوبین و تیغ و تیر</p></div>
<div class="m2"><p>مسکین دلی که خسته تیر نظر بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرزند دیگران چه وفا میکند بکس</p></div>
<div class="m2"><p>یعقوب را که شکوه بود از پسر بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رعنا غزال من ننهی پا بدشت عشق</p></div>
<div class="m2"><p>کاینجا بدام بسته بسی شیر نر بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاصل کجا برند از این کشته عاشقان</p></div>
<div class="m2"><p>هر جا که خرمنی است بوقف شرر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گو گنج را بپوش خداوند سیم و زر</p></div>
<div class="m2"><p>درویش را زخاک بسی گنج زر بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آشفته صاحبان نظر رابصیرتست</p></div>
<div class="m2"><p>نه هر کراست چشم بسر با بصبر بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر مرتضی است چشم خداوند عقل را</p></div>
<div class="m2"><p>هرگز جز این نکرده که عقلش بسر بود</p></div></div>