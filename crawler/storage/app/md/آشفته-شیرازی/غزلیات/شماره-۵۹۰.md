---
title: >-
    شمارهٔ ۵۹۰
---
# شمارهٔ ۵۹۰

<div class="b" id="bn1"><div class="m1"><p>بساز مطرب مجلس نوای شورانگیز</p></div>
<div class="m2"><p>که تا بری زرهم زآن اصول رنگ آمیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیار آتش سیاله ساقی مستان</p></div>
<div class="m2"><p>بسوز خرمن تقوی و حاصل پرهیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنای دهر زعشق است و عشق زآدم خاست</p></div>
<div class="m2"><p>بیا بتجربه طرحی زخاک آدم ریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام همت آن کشته ام که همچو خضر</p></div>
<div class="m2"><p>مدام زنده بود آبشار خنجر تیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدم بعجز بحبل المتین عشق تو چنگ</p></div>
<div class="m2"><p>که نیستم بجز این روز حشر دست آویز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بروز معرکه گر جوشنت زمهر علیست</p></div>
<div class="m2"><p>اگر جهان همه لشکر شود بیا مگریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگوی مدحت حیدر که با گران باری</p></div>
<div class="m2"><p>تو رستگار برآئی بروز رستاخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان آینه و دیده شد غبار حجاب</p></div>
<div class="m2"><p>علیست آینه تو گرد از میان برخیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عبادتت ندهد سود بی ولای علی</p></div>
<div class="m2"><p>بیاد او برو آشفته چون روی بحجیز</p></div></div>