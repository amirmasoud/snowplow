---
title: >-
    شمارهٔ ۶۴۲
---
# شمارهٔ ۶۴۲

<div class="b" id="bn1"><div class="m1"><p>بر بی نیاز قابل نبود نماز عاشق</p></div>
<div class="m2"><p>مگر اینکه قبله باشد بت دلنواز عاشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه بمسجد است و محراب نماز عشقبازان</p></div>
<div class="m2"><p>بخم دوابروی دوست بود نماز عاشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه روی در حجیزند پی عبادت اما</p></div>
<div class="m2"><p>سر کوی یار باشد بجهان حجاز عاشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجهان به بینیازی بزنند دم حریفان</p></div>
<div class="m2"><p>که برای ناز معشوق بود نیاز عاشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکنی زغیر شکوه رخ زرد و اشگ گلگون</p></div>
<div class="m2"><p>فکند زپرده بیرون همه روزه راز عاشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بده آن شراب ساقی که هوس زداید از دل</p></div>
<div class="m2"><p>بحقیقتی رسانی تو گر مجاز عاشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو که دست کردگاری گرهی زکار بگشا</p></div>
<div class="m2"><p>نبود بجز تو در دهر چو کارساز عاشق</p></div></div>