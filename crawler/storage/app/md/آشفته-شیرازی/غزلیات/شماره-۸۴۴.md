---
title: >-
    شمارهٔ ۸۴۴
---
# شمارهٔ ۸۴۴

<div class="b" id="bn1"><div class="m1"><p>زآن دهانم داد دشنامی که من می‌خواستم</p></div>
<div class="m2"><p>بعد عمری دید دل کامی که من می‌خواستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جُست دل زلف دلارامی که من می‌خواستم</p></div>
<div class="m2"><p>یافته امشب دلارامی که من می‌خواستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوزی اندر بلبلان افکند و آتش زد به گل</p></div>
<div class="m2"><p>داشت باد صبح پیغامی که من می‌خواستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چه پیر می‌فروشانم ز جامی خُم نکرد</p></div>
<div class="m2"><p>بود در پای خُم آن جامی که من می‌خواستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود چشم شوخ او را زلف خالی توأمان</p></div>
<div class="m2"><p>داشت بر کی دانه و دامی که من می‌خواستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدی را دید عاشق گشته رندی دوش گفت</p></div>
<div class="m2"><p>جمع شد آن کفر و اسلامی که من می‌خواستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با بناگوش تو شد دست و گریبان گیسویت</p></div>
<div class="m2"><p>هم‌عنان شد صبحی و شامی که من می‌خواستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیخ زد طعنه که آشفته سگ کوی علی‌ست</p></div>
<div class="m2"><p>شُکر شد نامیده بر نامی که من می‌خواستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند روزی شد که شور عشقم افتاده به سر</p></div>
<div class="m2"><p>وَهْ که آمده باز ایّامی که من می‌خواستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن کبوتر که هوا بگرفت از بام حرم</p></div>
<div class="m2"><p>شد مجاور بر در بامی که من می‌خواستم</p></div></div>