---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>برفکن از بدن دلا زرق سیه پلاس را</p></div>
<div class="m2"><p>آینه شو که تا بری لذت انعکاس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد و میگسار را نیک شناخت پیر ما</p></div>
<div class="m2"><p>شیخ زمانه گو بنه کسوت التباس را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامت تو کجا سزد خلعت اتحاد را</p></div>
<div class="m2"><p>تا نکنی زتن برون عاریتی لباس را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست قیاس و وهم را راه بعشق سرمدی</p></div>
<div class="m2"><p>گو بحکیم بشکند آینه قیاس را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی میکشان بده مرهم قلب خستگان</p></div>
<div class="m2"><p>تا که زکیمیای می زر کنم این نحاس را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنج حواس وقف شد بر لب و چشم و زلف و خط</p></div>
<div class="m2"><p>جادوی خمسه کزفسون پنج کند حواس را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رایض مدح شاه را توسن طبع رام شد</p></div>
<div class="m2"><p>تا چو فرس بزین کشم حکمت بوفراس را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته بر فلک پای گذاری از شرف</p></div>
<div class="m2"><p>بوسه زنی بخاک اگر شاه فلک اساس را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساقی بزم لم یزل مظهر شاهد ازل</p></div>
<div class="m2"><p>دست خدا که بر درش ره نبود هراس را</p></div></div>