---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>من کیستم که وصف کنم از جمال دوست</p></div>
<div class="m2"><p>ما ذره آفتاب حقیقت جمال دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دوست گر بدست فتد دیگری گر</p></div>
<div class="m2"><p>تا وصف گوید او زجمال کمال دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر آفتاب تیغ بعالم کشد صباح</p></div>
<div class="m2"><p>بر آفتاب تیغ کشیده هلال دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سرو ناز معتدلت باغبان مگو</p></div>
<div class="m2"><p>کامد بجلوه قامت باعتدال دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه وقت سخت از خود خبر نداشت</p></div>
<div class="m2"><p>مشغول خویشتن نشد از اشتغال دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقاش عاجز است چو از نقش آن نگار</p></div>
<div class="m2"><p>آشفته عاشق است بنقش خیال دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکقطره است بحر محبت زجوی عشق</p></div>
<div class="m2"><p>یک میوه است حسن بتان از نهال دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جم را که آن جلال و حشم داد روزگار</p></div>
<div class="m2"><p>این حشمتش کرانه بود از جلال دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با هجر او بسازم و سوزم همی چو شمع</p></div>
<div class="m2"><p>نبود اگر میسر ما را وصال دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این جمله نقش ماست کز آئینه شد عیان</p></div>
<div class="m2"><p>وصفی که شد زلف و رخ و خط و خال دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما را سؤال در صف محشر زحیدر است</p></div>
<div class="m2"><p>عاشق نداشت غیر جواب سؤال دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دوستی چو سیرت دشمن گرفته ایم</p></div>
<div class="m2"><p>سر برنیاوریم هم از انفعال دوست</p></div></div>