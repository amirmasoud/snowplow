---
title: >-
    شمارهٔ ۱۱۷۸
---
# شمارهٔ ۱۱۷۸

<div class="b" id="bn1"><div class="m1"><p>بود زمین و آسمان از دم مرتضی علی</p></div>
<div class="m2"><p>سود تمام کن فکان از دم مرتضی علی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش زمین و آسمان رنگ نداشت از ازل</p></div>
<div class="m2"><p>بود بنای این و آن از دم مرتضی علی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاجرم آب و خاک را این همه منزلت نبود</p></div>
<div class="m2"><p>آدم از او شده عیان از دم مرتضی علی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورتی ار بود عیان معنی ار بود نهان</p></div>
<div class="m2"><p>هست عیان و هم نهان از دم مرتضی علی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگ و کلوخ و جانور برگ گیاه و هم شجر</p></div>
<div class="m2"><p>جمله بذکر حق زبان از دم مرتضی علی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیش بمحشر ار بسی میکند از عمل کسی</p></div>
<div class="m2"><p>آشفته عیش شیعیان از دم مرتضی علی</p></div></div>