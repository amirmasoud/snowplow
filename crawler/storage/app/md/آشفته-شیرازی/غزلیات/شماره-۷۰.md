---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>خط و زلفت چیست با آن روی همچون آفتاب</p></div>
<div class="m2"><p>صبح روشن شام تیره غنبر ترسیم ناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خصم عقل و دین و صبر هوشم این چارند مست</p></div>
<div class="m2"><p>غمزه کافر چشم جادو خال هند و لب سراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چار چیز از چار چیزم کرده مستغنی بدهر</p></div>
<div class="m2"><p>لب زکوثر قد زطوبی رخ زگل بوی از گلاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شبان تار این چار است مستان تو را</p></div>
<div class="m2"><p>ناله مطرب اشک باده دیده ساغر دل کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهد و تقوی سبحه دفتر وقف بر این چار شد</p></div>
<div class="m2"><p>این بخاک و آن بباد و این بآتش آن بآب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چار چیز آشفته در بحر وجودم شد عیان</p></div>
<div class="m2"><p>عشق نوح و شوق کشتی صبر لنگر اشک آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست ما و دامن مهر علی آن بحر جود</p></div>
<div class="m2"><p>کامده کشتی نه افلاک در بحرش حباب</p></div></div>