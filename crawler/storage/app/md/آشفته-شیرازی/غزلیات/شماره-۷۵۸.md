---
title: >-
    شمارهٔ ۷۵۸
---
# شمارهٔ ۷۵۸

<div class="b" id="bn1"><div class="m1"><p>گلعذاران گلستانی داشتم</p></div>
<div class="m2"><p>بلبلان من هم فغانی داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برق میآید بطوف بوستان</p></div>
<div class="m2"><p>کاش منهم آشیانی داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصف میکردیم هر یک از گلی</p></div>
<div class="m2"><p>از هزاران هم زبانی داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی سبب پیرم مبین ای باغبان</p></div>
<div class="m2"><p>در چمن نخل جوانی داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر براه مصر بودم در طلب</p></div>
<div class="m2"><p>یوسفی در کاروانی داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله گر میکردم از ناسور دل</p></div>
<div class="m2"><p>طره عنبر فشانی داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میزدم گر لاف مردی در مصاف</p></div>
<div class="m2"><p>زابرویش تیر و کمانی داشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داشت از راز نهان من خبر</p></div>
<div class="m2"><p>چون صبا تا راز دانی داشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب بخلوت پیش شمع انجمن</p></div>
<div class="m2"><p>گفتم ار راز نهانی داشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که زد مهر خموشی بر لبم</p></div>
<div class="m2"><p>دوستان منهم زبانی داشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر عنان گیرم زترکی کج کلاه</p></div>
<div class="m2"><p>کج کله چابک عنانی داشتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه بودم در وصال و گه بهجر</p></div>
<div class="m2"><p>هم بهار و هم خزانی داشتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر روان میشد زچشمم جوی خون</p></div>
<div class="m2"><p>جلوه سرو روانی داشتم</p></div></div>