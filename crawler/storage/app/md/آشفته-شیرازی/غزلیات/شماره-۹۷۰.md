---
title: >-
    شمارهٔ ۹۷۰
---
# شمارهٔ ۹۷۰

<div class="b" id="bn1"><div class="m1"><p>با مه و آفتاب شد طلعت تو چو روبه‌رو</p></div>
<div class="m2"><p>عیب عیان چو آینه گفت ز هردو مو به مو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از همه سوی می‌وزد نفخه چین گیسویت</p></div>
<div class="m2"><p>سوی ختا و چین روم من به هواش سو به سو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست به فرّ قامتت نیست چو آب دیده‌ام</p></div>
<div class="m2"><p>خیز و بجوی در چمن سرو به سرو جو به جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ پرنده است دل طفلم و صعوه کرده گم</p></div>
<div class="m2"><p>از پی دل که می‌دوم خانه به خانه کو به کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست به دست می‌رود تا گل من در انجمن</p></div>
<div class="m2"><p>خون دلم ز رشک او بسته چو غنچه تو به تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل چه کنی به بوستان شاهد گل‌بدن ببین</p></div>
<div class="m2"><p>نافه چین چه می‌کنی طره مشک‌بوی او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته جا به طره‌ات کرد مگر که تا شبی</p></div>
<div class="m2"><p>حالت خویش مو به مو گوید با تو روبه‌رو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مصحف را گر زبان بود بسمله تا به خاتمه</p></div>
<div class="m2"><p>نام علی بگویدت نقطه به نقطه هو به هو</p></div></div>