---
title: >-
    شمارهٔ ۶۲۹
---
# شمارهٔ ۶۲۹

<div class="b" id="bn1"><div class="m1"><p>دلی کز هجر طاق ابرویت طاقت بود طاقش</p></div>
<div class="m2"><p>بزن با ناوک مژگان که از جانست مشتاقش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر مستی بدور عشق بسته عهد مستوری</p></div>
<div class="m2"><p>بیک پیمانه بشکن ساقیا پیمان و میثاقش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشا طوفانی بحری که گردابش بود ساحل</p></div>
<div class="m2"><p>خوشا مسموم عشق تو که باشد زهر تریاقش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد گر آفتاب می زشرق جام زر طالع</p></div>
<div class="m2"><p>چرا تابید مستان را بجان انوار اشراقش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگردان مطرب آهنگ مخالف را در این پرده</p></div>
<div class="m2"><p>حصاری را بدست آور که شد مغلوب عشاقش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر پروانه را چون سوخت شمع آتش بجان آمد</p></div>
<div class="m2"><p>که از سوز درون خویش آگه شد زاخراقش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عبث آشفته جستن از منافق کام دل تا کی</p></div>
<div class="m2"><p>بخواه از پیر میخانه که بس عام است انفاقش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علی آن ساقی کوثر علی شاه سخاپرور</p></div>
<div class="m2"><p>که باشد ریزه خوار خوان همه امکان و آفاقش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود چون طلعت تو مه اگر او را دهن باشد</p></div>
<div class="m2"><p>بود چون قامت تو سرو اگر سیمین بود ساقش</p></div></div>