---
title: >-
    شمارهٔ ۱۰۷۸
---
# شمارهٔ ۱۰۷۸

<div class="b" id="bn1"><div class="m1"><p>پیش از این بوده بچین صورتگری</p></div>
<div class="m2"><p>نقش او میرفته در هر کشوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که شد نقش بدیع تو عیان</p></div>
<div class="m2"><p>دفتر مانی ندارد زیوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سدره بالائی بهشتی طلعتی</p></div>
<div class="m2"><p>حور رخساری و غلمان منظری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو جا در خانه دل کرده ای</p></div>
<div class="m2"><p>در نمیگنجد بچشمم دیگری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در صف مژگان چه ای چشم مست</p></div>
<div class="m2"><p>ترک مستی در میان لشکری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال او هندو رخش آتشکده</p></div>
<div class="m2"><p>لیک از آتش بجوشد کوثری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب حیوان از دهانت نکته ای</p></div>
<div class="m2"><p>آفتابت پیش عارض اختری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه گفتم از تو بر داور برم</p></div>
<div class="m2"><p>داوری نتوان که خود تو داوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بآتش سوزدم آشفته دوست</p></div>
<div class="m2"><p>به که آبم برفشاند دیگری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر مس قلبت بسوزد نار عشق</p></div>
<div class="m2"><p>می‌شوی اکسیر اگر خاکستری</p></div></div>