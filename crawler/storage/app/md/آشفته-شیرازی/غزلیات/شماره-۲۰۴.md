---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>سرو چالاکی اگر سروی به رفتار آمده است</p></div>
<div class="m2"><p>ماه افلاکی اگر ماهی به گفتار آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حور را مانی اگر حوری به دنیا بگذرد</p></div>
<div class="m2"><p>یا پریزادی پری گر خود به دیدار آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم این چشم تو زابرو کرد اشارت سوی زلف</p></div>
<div class="m2"><p>گفت اینش نافه واین آهو ز تاتار آمده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده افکندی عزیزا تا که در بازار حسن</p></div>
<div class="m2"><p>یوسف و مصر و زلیخایت خریدار آمده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که در بتخانه چین نقش رویت برده‌اند</p></div>
<div class="m2"><p>برهمن همچون بت چین نقش دیوار آمده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بشیر ار سوی کنعان می‌روی تعجیل کن</p></div>
<div class="m2"><p>مژده بر یعقوب را یوسف به بازار آمده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل گوید دین تبه شد گرد مه‌رویان مگرد</p></div>
<div class="m2"><p>عشق می‌گوید که حسن از بهر این کار آمده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به قبرستان مشتاقان ز رحمت بگذری</p></div>
<div class="m2"><p>مرده می‌گوید مسیحایی دگربار آمده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال دل ناصِح چه داند در خم زلف کجت</p></div>
<div class="m2"><p>گوی می‌داند که در چوگان گرفتار آمده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زخمه زد مطرب از ناخن به جان نالید چنگ</p></div>
<div class="m2"><p>چون ننالد دل که بر وی زخم بسیار آمده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالمی خاموش شد زان لعل گویا در جهان</p></div>
<div class="m2"><p>یک جهان از آن چشم خواب‌آلود بیدار آمده است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پرده‌دار شاهد غیبی علی مرتضی</p></div>
<div class="m2"><p>ممکن است آشفته و واجب به او یار آمده است</p></div></div>