---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>مرا سزد که نگنجم چو غنچه اندر پوست</p></div>
<div class="m2"><p>که برشکفت به باغ دلم گل رخ دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناف آهوی چین اندر است نافه مشک</p></div>
<div class="m2"><p>تو را لطیمه عنبر ملازم آهوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کناره می‌کند از چشم تر سهی‌سروم</p></div>
<div class="m2"><p>که گفت سرور و آن را مقام بر لب جوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه جان برم از چشم تو که از دو طرف</p></div>
<div class="m2"><p>کمند طره زلف و کشاکش ابروست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواد زلف کجت قبله‌گاه مشک تتار</p></div>
<div class="m2"><p>به پیش هندوی خال تو غالیه هندوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکنج زلف تو مشک و رخ تو گل خواندم</p></div>
<div class="m2"><p>نه گل به رنگ رخ تو نه مشک را آن بوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریب سینه سیمین او مخور ای دل</p></div>
<div class="m2"><p>که زیر سیم سفیدش نهفته آهن و روست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حذر ز نرگس جادوفریب جماشش</p></div>
<div class="m2"><p>که سحر و حیله و نیرنگ و فتنه از جادوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه هر دلی که پریشان ببینی آشفته</p></div>
<div class="m2"><p>مرا سزاست که آشفته‌ام ز طره دوست</p></div></div>