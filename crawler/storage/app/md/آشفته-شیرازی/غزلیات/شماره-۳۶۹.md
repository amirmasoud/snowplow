---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>طرب ایبلبلان بهار آمد</p></div>
<div class="m2"><p>شاخ گل در چمن ببار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حشمت گل شکست شوکت خار</p></div>
<div class="m2"><p>دی برون رفت و نوبهار آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاغ در باغ گو مکن غوغا</p></div>
<div class="m2"><p>نوبت غلغل هزار آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب تبت ببرد خاک چمن</p></div>
<div class="m2"><p>ناف گل نافه تتار آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس از سر نهاد خواب و خمار</p></div>
<div class="m2"><p>ساقی لاله میگسار آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو طوبی و گلبنان حورا</p></div>
<div class="m2"><p>سلسبیل آب جویبار آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک نو کرد جامه نوروز</p></div>
<div class="m2"><p>جامه نو کن که کهنه عار آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاتم جم زدست اهرمنان</p></div>
<div class="m2"><p>در کف شاه تاجدار آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صاحب تاج هل اتی علی آنک</p></div>
<div class="m2"><p>کار فرمای ذوالفقار آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل آشفته با هزاران غمم</p></div>
<div class="m2"><p>در چنین روز کام کار آمد</p></div></div>