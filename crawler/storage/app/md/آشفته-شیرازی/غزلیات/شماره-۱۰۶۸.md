---
title: >-
    شمارهٔ ۱۰۶۸
---
# شمارهٔ ۱۰۶۸

<div class="b" id="bn1"><div class="m1"><p>دلا ز عشق مجاز از چه مهر برکندی؟</p></div>
<div class="m2"><p>حقیقتی به کف آور که باز پیوندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که تخم امیدم هنوز در دل هست</p></div>
<div class="m2"><p>اگر تو بیخ محبت ز ریشه برکندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن به بزم رقیبان نموده‌ای آغاز</p></div>
<div class="m2"><p>نمک به زخم درون‌ها چرا پراکندی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هر طرف که برندت برآری از نو سر</p></div>
<div class="m2"><p>تو ای نهال محبت عجب برومندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سرای مغان باز باد بر رویم</p></div>
<div class="m2"><p>که نیست غم گرم ای آسمان تو در بندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ماه راست دهان و نه سرو را رفتار</p></div>
<div class="m2"><p>که گفته است که با سرو و ماه مانندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه شد که نیستی ای دل به منزل جانان؟</p></div>
<div class="m2"><p>اگر حجاب تعلق ز جان برافکندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه قند مصر پرستد مگس نه شکّر هند</p></div>
<div class="m2"><p>اگر به آن لب شیرین کنی شکرخندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به خون طپید دل مستمند از تیرت</p></div>
<div class="m2"><p>عجب که آرزوئی یافت آرزومندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکش چنانکه تو دانی که شاد و خورسندم</p></div>
<div class="m2"><p>به خون ناحق آشفته گر تو خورسندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدای گفته اگر والضحی وگر و اللیل</p></div>
<div class="m2"><p>به روی و موی نکوی تو خورده سوگندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین جنون که مرا هست بگسلم زنجیر</p></div>
<div class="m2"><p>مگر مرا به خم زلف یار بربندی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدام یار؟ علیّ ولی حقیقت عشق</p></div>
<div class="m2"><p>که خور بسوخته در مجمرش چو اسپندی</p></div></div>