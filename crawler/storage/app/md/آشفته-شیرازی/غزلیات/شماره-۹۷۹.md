---
title: >-
    شمارهٔ ۹۷۹
---
# شمارهٔ ۹۷۹

<div class="b2" id="bn1"><p>ای صید نگاه تو در چین و ختن آهو</p>
<p>وای غالیه بو مویت چون نافهٔ تو بر تو</p></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید نهد هر روز بر خاک درتو رو</p></div>
<div class="m2"><p>مشک ختنی گیرد از چین دو زلفت بو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چشمه خور ای ماه یک روز مقابل شو</p></div>
<div class="m2"><p>تا آینه وش گوئی عیبش همه روبرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرخ شکر خنده آن خسرو شیرین برد</p></div>
<div class="m2"><p>گیسوش سر فرهاد آویخت بتار مو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چنبر آن گیسو سر تافته کس حاشا</p></div>
<div class="m2"><p>کاندر خم چوگانش خورشید فلک شد و</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از انی انا الله دم آنشوخ نزد هیهات</p></div>
<div class="m2"><p>چون شد شجر سینا یک پرتو حسن او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مدح علی گوید بشنو سخن مطرب</p></div>
<div class="m2"><p>واعظ کند ار منعت مشنو سخن بدگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خواجه کریم آمد غم نیست تهی دستی</p></div>
<div class="m2"><p>نیکست مرا معشوق گر زشتم اگر نیکو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته ام و شیدا بی پرده و بی پروا</p></div>
<div class="m2"><p>نه سعدی و نه حافظ سلمان نیم و خواجو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر شعر مرا مقدار نبود بر دانشور</p></div>
<div class="m2"><p>قلب است زرم اما شد سکه به نام او</p></div></div>