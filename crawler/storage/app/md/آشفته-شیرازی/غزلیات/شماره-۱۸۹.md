---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>به پیر میفروشان بر بشارت</p></div>
<div class="m2"><p>که زاهد کرد خمخانه زیارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمی سجاده رنگین کرد زاهد</p></div>
<div class="m2"><p>شد از وسواس فارغ بیمرارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبر زحمت پی تعمیر مسجد</p></div>
<div class="m2"><p>بیا دیر مغان را کن عمارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حذر زان مغبچه کز کفر زلفش</p></div>
<div class="m2"><p>متاع دین و دل را کرد غارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در آغوش کیست گل بدنت</p></div>
<div class="m2"><p>که صبا داشت بوی پیرهنت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسفی را که سالها گم کرد</p></div>
<div class="m2"><p>جست یعقوب در چه ذقنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکند میل عندلیب و شکر</p></div>
<div class="m2"><p>گل زآواز و طوطی از سخنت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیرهن کن بتا زنکهت گل</p></div>
<div class="m2"><p>که زگل رنجه میشود بدنت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر و پایم نثار پا و سرت</p></div>
<div class="m2"><p>تن و جانم فدای جان و تنت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناز از سر نهاد و بنده شدت</p></div>
<div class="m2"><p>تا چمان دید سرو در چمنت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در دهانت سخن نمیگنجد</p></div>
<div class="m2"><p>این سخنها که گفت از دهنت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چند ای خاتم سلیمانی</p></div>
<div class="m2"><p>بنگرم زیب دست اهرمنت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو گلی بلبلی بدست آور</p></div>
<div class="m2"><p>نسزد شوق زاغ با زغنت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با بناگوش و آن خم گیسو</p></div>
<div class="m2"><p>کس فروشد بسنبل و سمنت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این همه شور و مشتری که تو راست</p></div>
<div class="m2"><p>کی گذارند یک زمان بمنت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل مردم چو ناقه پرخون کرد</p></div>
<div class="m2"><p>که ختائیست آهوی ختنت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آخر این داغ عشق آشفته</p></div>
<div class="m2"><p>لاله گردد بروید از کفنت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا علی خصم سرکش است بکش</p></div>
<div class="m2"><p>از نیام آن حسام سرفکنت</p></div></div>