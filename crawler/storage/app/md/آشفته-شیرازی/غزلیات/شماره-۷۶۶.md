---
title: >-
    شمارهٔ ۷۶۶
---
# شمارهٔ ۷۶۶

<div class="b" id="bn1"><div class="m1"><p>زآن سرو زلف نافه بگشایم</p></div>
<div class="m2"><p>گو بخوانند خلق عطارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدح مولا نیاید از درویش</p></div>
<div class="m2"><p>سگ خود ای علی تو بشمارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهسوارا نه من شهید توام</p></div>
<div class="m2"><p>روزی آخر زخاک بردارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز عشق شررخیز زآغاز ندیدم</p></div>
<div class="m2"><p>میسوختم و موقع ابراز ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رازی که همه عمر دل از دیده نهان کرد</p></div>
<div class="m2"><p>ای دیده تو گفتی چو تو غماز ندیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک انجمن از روشنی شمع بعیشند</p></div>
<div class="m2"><p>پروانه برو جز تو بپرواز ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلچین پی یغما و حریفان بتماشا</p></div>
<div class="m2"><p>با گل بجز از بلبل دمساز ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حشر شهیدان تو گلگون کفنانند</p></div>
<div class="m2"><p>یک قوم چو این طایفه ممتاز ندیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر جا که دری بود زدم خانه امید</p></div>
<div class="m2"><p>یک در بجز از دیر مغان باز ندیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشتم همه اطراف گلستان زسردرد</p></div>
<div class="m2"><p>جز بلبل شوریده همه آواز ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کس سفری کرد بمنزل رود ایدل</p></div>
<div class="m2"><p>رفتی تو من آمدنت باز ندیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عمرم همه شد صرف وفاداری خوبان</p></div>
<div class="m2"><p>یک اهل وفا در همه شیراز ندیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نام آشفته ببر باری بگو</p></div>
<div class="m2"><p>طوطی شکر فشانی داشتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر گنه کردم بسی ایشیخ شهر</p></div>
<div class="m2"><p>چون نجف دارالامانی داشتم</p></div></div>