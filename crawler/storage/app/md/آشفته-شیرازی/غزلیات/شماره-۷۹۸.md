---
title: >-
    شمارهٔ ۷۹۸
---
# شمارهٔ ۷۹۸

<div class="b" id="bn1"><div class="m1"><p>بده می کز فروغش خرقه و دفتر بسوزانم</p></div>
<div class="m2"><p>بمستی از سر این سبحه و زنار برخیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکش آن سرمه در چشمم که غیر از دوست نشناسم</p></div>
<div class="m2"><p>بسازم ساز صلح کل و از پیکار برخیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلطف ای شاخ طوبی بر سر من سایه ای افکن</p></div>
<div class="m2"><p>که از وجد و شعف از سایه دیوار برخیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکن در میکده خاکم که وقت نفخ صور از جا</p></div>
<div class="m2"><p>چو گرد از آستان خانه خمار برخیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدوزخ چون بری آشفته را با مهر حیدر بر</p></div>
<div class="m2"><p>که با دامان پرگل چون خلیل از نار برخیزم</p></div></div>