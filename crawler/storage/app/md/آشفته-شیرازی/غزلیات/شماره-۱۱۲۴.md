---
title: >-
    شمارهٔ ۱۱۲۴
---
# شمارهٔ ۱۱۲۴

<div class="b" id="bn1"><div class="m1"><p>این چه روی است که تو ای بت ترسا داری؟</p></div>
<div class="m2"><p>بت و آتشکده محراب و کلیسا داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بهشتی ز چه هندو بچه داری به کنار</p></div>
<div class="m2"><p>ور کنشتی ز چه محراب و مصلی داری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو اگر کعبه‌ای ای زلف به بت سجده مکن</p></div>
<div class="m2"><p>ور بتی ای رخ در کعبه چه مأوا داری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کشی از خط مشکین رقمی بر گل تر</p></div>
<div class="m2"><p>تا که بر خون که از غالیه طغرا داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من گرفتم که بسوزی تو به یک جلوه ز دوست</p></div>
<div class="m2"><p>تو که پروانه‌ای از شمع چه پروا داری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بود عشق چه فرقت ز بهار و ز خزان</p></div>
<div class="m2"><p>بلبل از رفتن گل بهر چه غوغا داری؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظمت آشفته پریشان بود و شورانگیز</p></div>
<div class="m2"><p>سر سودای که برگو به سویدا داری؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غم بی‌سیم و زری از چه خوری ای درویش</p></div>
<div class="m2"><p>تو که در هردو جهان تکیه به مولا داری؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گناه دو جهانت چه غم و بی‌عملی</p></div>
<div class="m2"><p>تو که بر حیدر و اولاد تولا داری؟</p></div></div>