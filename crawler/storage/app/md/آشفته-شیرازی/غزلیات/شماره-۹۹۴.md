---
title: >-
    شمارهٔ ۹۹۴
---
# شمارهٔ ۹۹۴

<div class="b" id="bn1"><div class="m1"><p>حسنت نهاده دانه دامی عجب به راه</p></div>
<div class="m2"><p>کاندر کمند زلف کشد آهوی نگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید سربرهنه به پای تو سر بسود</p></div>
<div class="m2"><p>کز خاک مقدم تو به سر برنهد کلاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه چو کل به پیش رخت جامه بردرید</p></div>
<div class="m2"><p>شد آفتاب مقتبس از طلعتت چو ماه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود مجال سلطنت عقل چون به ناز</p></div>
<div class="m2"><p>بر ملک دل سپهبد غمزه کشید شاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم حذر کنم ز خدنگ نگاه او</p></div>
<div class="m2"><p>دیدم که نیست جز خم زلفش گریزگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مصر اگر تو چاه زنخدان بیاوری</p></div>
<div class="m2"><p>از اوج تخت یوسف مصری فتد به چاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اکنون که لعل یار به کام است جان بده</p></div>
<div class="m2"><p>امشب که زلف یار به دست است می بخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسکندر آب خضر همی‌جست و ره نبرد</p></div>
<div class="m2"><p>آب حیات خورده عاشق ز خاک راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم مگر که ترک ختائیست چشم تو</p></div>
<div class="m2"><p>چین و ختن ندارد این ترک دل‌سیاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آهم به دل گره شده است از جفای تو</p></div>
<div class="m2"><p>ترسم به پیش آینه تو برآرم آه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاهد مرا ز حشر مترسان و درگذر</p></div>
<div class="m2"><p>ما و می صبوح و تو و ورد صبحگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طوفان اگر دوباره بگیرد جهان چو نوح</p></div>
<div class="m2"><p>آشفته می‌برد به در میکده پناه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میخانه محبت حق مضجع علی</p></div>
<div class="m2"><p>خاکی که سوده‌اند ملایک بر او جناه</p></div></div>