---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>میان ما و تو الفت حکایتی است عجیب</p></div>
<div class="m2"><p>که تو به عهد شبابی و من به نوبت شبیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر تو مرکب تازی به خاک من تازی</p></div>
<div class="m2"><p>چو گرد روح روان خیزدت ز سم رکیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صلیب بستن اگر کار بت‌پرستان است</p></div>
<div class="m2"><p>بت من از چه فکنده ز زلف عود صلیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین نه هندوی خال تو ذوق از آن لب یافت</p></div>
<div class="m2"><p>بهشتیان همه دارند از این شراب نصیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر که بر دل مجروح عاشقان زد دست</p></div>
<div class="m2"><p>که خون چو سیل روانست زآستین طبیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از عتاب تو شکوه کنم غلطا حاشا</p></div>
<div class="m2"><p>بیار هرچه تو داری برای دوست عتیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه عاشقست که خود مدعی و کذابست</p></div>
<div class="m2"><p>محب اگر که شکایت کند ز جور حبیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به چنگ غیر بود زلف یارم آشفته</p></div>
<div class="m2"><p>فغان که رشته عمرم بود به دست رقیب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگرچه نامه سیاهم ولی خوشم با این</p></div>
<div class="m2"><p>که داوری نبود جز علی به روز حسیب</p></div></div>