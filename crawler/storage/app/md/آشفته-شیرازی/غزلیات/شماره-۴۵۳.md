---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>دوستان زود از این شهر کناری گیرید</p></div>
<div class="m2"><p>غیر شیراز ره شهر و دیاری گیرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز زیان سود که دیده است زکار دوران</p></div>
<div class="m2"><p>گر توانید از این به سر و کاری گیرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقت آنست که در کشف حقیقت در شهر</p></div>
<div class="m2"><p>همچو منصور مکان بر سر داری گیرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در میکده گیرید مکان شب همه شب</p></div>
<div class="m2"><p>باده هر صبح پی دفع خماری گیرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین شب و روز بجز فتنه نزاید خیزید</p></div>
<div class="m2"><p>غیر از این روز و شبان لیل و نهاری گیرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست در مرکز آفاق بجز رنج و ملال</p></div>
<div class="m2"><p>از ولای علی و آل حصاری گیرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نغمه زیر و بم مطرب دوران بنهید</p></div>
<div class="m2"><p>چون نی از سوز درون ناله زاری گیرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بسا پرده عصیان که بود حاجب جان</p></div>
<div class="m2"><p>از پی سوختن پرده شراری گیرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلشنی را که خزان هست گذارند بخاک</p></div>
<div class="m2"><p>بی خزان طرف گلستان و بهاری گیرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چهره شاهد غیبی ننماید در خاک</p></div>
<div class="m2"><p>تا توانید از آئینه غباری گیرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پشت آشفته دو تا آمده از بار گناه</p></div>
<div class="m2"><p>رحمی از دوش وی ای قافله باری گیرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهر شیراز شد از زلزله بی صبر و سکون</p></div>
<div class="m2"><p>تا که در خاک نجف بلکه قراری گیرید</p></div></div>