---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>چشم تو بربست از فسون بر خلق راه خواب را</p></div>
<div class="m2"><p>زلفت پریشان می‌کند جمعیت احباب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم دل سوداییم دارد دوا بگشود لب</p></div>
<div class="m2"><p>گفتا نبینی در شکر پرورده‌ام عناب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌نغنود شب تا سحر چشمم ز هجرت ای پسر</p></div>
<div class="m2"><p>کی خواب آید در نظر افتاده در غرقاب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفت بهر جامی کشد دل در هوایش می‌رود</p></div>
<div class="m2"><p>ناچار ماهی می‌رود تا می‌کشد قلاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبی که اسکندر نخورد چندان کش از پی دستبرد</p></div>
<div class="m2"><p>من جسته‌ام در آن دهان آن گوهر نایاب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب بر سر کوی تو من گویم ز هر بابی سخن</p></div>
<div class="m2"><p>شاید که با صد مکر و فن خواب آورم بواب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حقه نافش اگر گم شد دلم عیبم مکن</p></div>
<div class="m2"><p>هم نوح کشتی بشکند بیند گر این گرداب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من در آن چاه ذقن افتاده‌ام ای سیم‌تن</p></div>
<div class="m2"><p>کز دام تو نبود گریز ای شوخ شیخ و شاب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته از آن تار مو در بزم تا کی گفتگو</p></div>
<div class="m2"><p>آخر پریشان می‌کنی جمعیت اصحاب را</p></div></div>