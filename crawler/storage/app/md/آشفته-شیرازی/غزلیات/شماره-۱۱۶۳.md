---
title: >-
    شمارهٔ ۱۱۶۳
---
# شمارهٔ ۱۱۶۳

<div class="b" id="bn1"><div class="m1"><p>چرا به بزم رقیبان حدیث دوست نگفتی</p></div>
<div class="m2"><p>بست نبود خلاف مؤالفت که برفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل رمیده ما را که مرغ وحشی بود</p></div>
<div class="m2"><p>شکار خویش نمودی و دام بازگرفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه پایدار بماند عهدهای سخت که بستی</p></div>
<div class="m2"><p>نه استوار بود گفتهای سست که گفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مرغ زیرک و جا آشیانه عنقا</p></div>
<div class="m2"><p>کدام دانه بزیرم که تو بدام من افتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ ودرد که بی پرده گفت مردم چشمم</p></div>
<div class="m2"><p>حدیث عشق که اول زمردمان بنهفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهاده دوش من و دیده سر بخاره خارا</p></div>
<div class="m2"><p>تو خوش به بستر دیبا ببزم غیر بخفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا زشوق تو آشفته خاک راه علی کن</p></div>
<div class="m2"><p>زنوک خامه در نظم آبدار که سفتی</p></div></div>