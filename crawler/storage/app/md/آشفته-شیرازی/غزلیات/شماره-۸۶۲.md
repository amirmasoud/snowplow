---
title: >-
    شمارهٔ ۸۶۲
---
# شمارهٔ ۸۶۲

<div class="b" id="bn1"><div class="m1"><p>برآ زجامه نیل ای نگار سیم اندام</p></div>
<div class="m2"><p>که شد مهی که در او بود جشن و عیش حرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود ربیع نخستین و ماه عیش و طرب</p></div>
<div class="m2"><p>بپوش جامه گلگون بدور افکن جام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غزل سرای و نواسنج و بذله گوی و بچم</p></div>
<div class="m2"><p>بسوز عود و بزن رود و باده می آشام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می مغانه چه حاجت که خانگی است شراب</p></div>
<div class="m2"><p>بیار از آن لب و چشمم تو شکر و بادام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنوش باده و سرخوش شو و برآی برقص</p></div>
<div class="m2"><p>که سرو و ماه بمانند از قعود وقیام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخوان مدیح علی از زبان آشفته</p></div>
<div class="m2"><p>که تا کنم در مکنون تو را نثار کلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صله اگر طلبد از تو طبع مدح گذار</p></div>
<div class="m2"><p>ببوسه زآن دهنش ساز شاد و شیرین کام</p></div></div>