---
title: >-
    شمارهٔ ۴۲۳
---
# شمارهٔ ۴۲۳

<div class="b" id="bn1"><div class="m1"><p>چه برخیزد از این سودا کزو دایم شرر خیزد</p></div>
<div class="m2"><p>چه سود از این عمل داری کزو دایم ضرر خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای نفس بگذار و حدیث عقل را بشنو</p></div>
<div class="m2"><p>که خیر از عقل می‌آید ولی از نفس شر خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار طول امل ای دل ز دامان عمل مگسل</p></div>
<div class="m2"><p>چه می‌آید از آن عهدی کزو بوک و مکر خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر جانوران از پیشه غیر شیر هم آمد</p></div>
<div class="m2"><p>بنازم نیستان عشق کز وی شیر نر خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نه نار ابراهیم آمد گلشن رویت</p></div>
<div class="m2"><p>چرا اندر میان آتشش گل‌هایْ برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا موسی به کوی عشق وارنی گوی خوش بنشین</p></div>
<div class="m2"><p>چه کوهست اینکه هر شب نخل طورش از کمر خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشاید خواندنش بستان چه فرقست از بیابانش</p></div>
<div class="m2"><p>چو بید از بوستانی گر درخت بی‌ثمر خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپر سازند در میدان چو خیزد از کمان پیکان</p></div>
<div class="m2"><p>حذر کن ای دل از تیری که از شست نظر خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان دریای پرموج است و رخت ازو به ساحل بر</p></div>
<div class="m2"><p>اگر از قعر این دریا همه لعل و گهر خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود پیدا که داری آتشی چون شمع پنهانی</p></div>
<div class="m2"><p>از این دودی که آشفته تو را هرشب ز سر خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دفتر وصف حیدر می‌نویسی و عجب نبود</p></div>
<div class="m2"><p>تو را گر از نی خامه همه قند و شکر خیزد</p></div></div>