---
title: >-
    شمارهٔ ۷۰۶
---
# شمارهٔ ۷۰۶

<div class="b" id="bn1"><div class="m1"><p>عهد کردم که بجز حرف غم عشق نگویم</p></div>
<div class="m2"><p>یا رهی جز طلبت با قدم صدق نپویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه جز نقش تو زآئینه خاطر بزدایم</p></div>
<div class="m2"><p>هر چه جز یاد تو از لوح دل و دیده بشویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده بر پاک دلان صلا خسرو دوران</p></div>
<div class="m2"><p>بگذر ای محتسب و سنگ میکفن بسبویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غم عارض جان پرورت از ناله چو نالم</p></div>
<div class="m2"><p>بی خم زلف دلاویز تو از مویه چو مویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته بودی که گلو تر کنمت زآب بلارک</p></div>
<div class="m2"><p>تیغ برکش که رسیده زعطش جان بگلویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که دور است زچشمان تو آن سرو چمانم</p></div>
<div class="m2"><p>چه تمتع که بود سرو چمن بر لب جویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشب هجر بخوانم همه جا قصه از آن مو</p></div>
<div class="m2"><p>تا همه خلق بدانند که آشفته اویم</p></div></div>