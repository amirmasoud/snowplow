---
title: >-
    شمارهٔ ۹۸۵
---
# شمارهٔ ۹۸۵

<div class="b" id="bn1"><div class="m1"><p>سنبل باغ شانه زد گیسو</p></div>
<div class="m2"><p>دی برون برد رخت و شد یکسو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسته از خواب نرگس بیمار</p></div>
<div class="m2"><p>با بنفشه نشسته هم زانو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زیر لب خندخند غنچه سحر</p></div>
<div class="m2"><p>گفت بس رازهای تو بر تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فاخته باخته دل و از شوق</p></div>
<div class="m2"><p>بر سر سرو میزند کوکو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطره زد بسکه ابر مینائی</p></div>
<div class="m2"><p>گشت گلزار غیرت مینو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه نافه فشاند ابر بهار</p></div>
<div class="m2"><p>شد ختا نافه ختن آهو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتماشای شاهدان چمن</p></div>
<div class="m2"><p>سرو استاده است بر لب جو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عندلیب چمن چو صوفی مست</p></div>
<div class="m2"><p>بر گل ناز میزند هوهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لخلخه سا شده هوای چمن</p></div>
<div class="m2"><p>بسکه برخاست باد عنبر بو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساقیا خیز و در قدح افکن</p></div>
<div class="m2"><p>آنچه در خم نهفته ای و سبو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دور نو کن زمی در این دوران</p></div>
<div class="m2"><p>تا کنی زخمه های کهنه زنو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گلشن ما بود به دشت نجف</p></div>
<div class="m2"><p>خیز تا رو کنیم در آن کو</p></div></div>