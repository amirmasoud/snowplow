---
title: >-
    شمارهٔ ۱۰۷۲
---
# شمارهٔ ۱۰۷۲

<div class="b" id="bn1"><div class="m1"><p>ساقی به کجائی بده آن مایه مستی</p></div>
<div class="m2"><p>تا بر سر مستی بنهم کسوت هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنّار ببر سبحه بنه خرقه بسوزان</p></div>
<div class="m2"><p>در میکده عشق بیا کز همه رستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخاسته نقش بت عقلم ز دل و جان</p></div>
<div class="m2"><p>ز آن روز که ای عشق در این خانه نشستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیمان که ببستم که بپیمانه زنم سنگ</p></div>
<div class="m2"><p>باز آمدی و شیشه توبه بشکستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غمزه‌ات ای چشم چه ها رفت به دل دوش</p></div>
<div class="m2"><p>با ما نتوان گفتنش ای ترک که مستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش بسته‌ای ای دل چو به آن زلف تعلق</p></div>
<div class="m2"><p>حقا که تو خوش قید علایق بگسستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که چو سوسن به زبان وصف تو گویم</p></div>
<div class="m2"><p>چون غنچه ز گفتار لب نطق ببستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر عهد که بستند شکستند حریفان</p></div>
<div class="m2"><p>چون عهد غم عشق ندیدم به درستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته نشان جست ز دلدار به هر در</p></div>
<div class="m2"><p>در خانه نهان بود نشانش تو نجستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بتکده عشق چو آئی تو خلیلا</p></div>
<div class="m2"><p>بت ساز نه بینی و دگر بت نپرستی</p></div></div>