---
title: >-
    شمارهٔ ۷۳۵
---
# شمارهٔ ۷۳۵

<div class="b" id="bn1"><div class="m1"><p>در سلسله آرد کاش آن زلف دلاویزم</p></div>
<div class="m2"><p>تا شور دل شیدا زآن سلسله انگیزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلوای لبت گفتم کی دست دهد گفتا</p></div>
<div class="m2"><p>موران چو هجوم آرند بر لعل شکرریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ز درم آمد با آتش سیاله</p></div>
<div class="m2"><p>کافتاد از آن آتش در خرقه پرهیزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به خم زلفش برگو به چه کارستی</p></div>
<div class="m2"><p>گفتا به مه و خورشید من غالیه میبیزم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من صعوه مسکینم تو عربده‌جو شاهین</p></div>
<div class="m2"><p>با چون تو قوی‌بازو کو قوه که بستیزم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تیزی پیکانم حاشا که حذر باشد</p></div>
<div class="m2"><p>تا رخنه به جان کرده تیر نظر تیزم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرهاد تو شیرینم مجنون تو لیلایم</p></div>
<div class="m2"><p>نه در هوس شکر چون خسرو پرویزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرو آمده در جولان گل سر زده در بستان</p></div>
<div class="m2"><p>برخیز و گل افشان کن ای گلبن نوخیزم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سلسله‌ای زد چنگ هر کس به تمنائی</p></div>
<div class="m2"><p>من هم به تولائی در زلف تو آویزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مهر علی مستم ساقی چه دهی ساغر</p></div>
<div class="m2"><p>چون باده صافی هست دردش ز چه آمیزم؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گرمی روز حشر نبود چو مفر کس را</p></div>
<div class="m2"><p>در سایه تو ناچار بایست که بگریزم</p></div></div>