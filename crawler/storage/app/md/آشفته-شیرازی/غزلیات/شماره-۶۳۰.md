---
title: >-
    شمارهٔ ۶۳۰
---
# شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>آن غزالی که من از غمزه شدم نخجیرش</p></div>
<div class="m2"><p>بخت آن کو که به تدبیر کنم تسخیرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سرای تو و ویران تر از او جائی نیست</p></div>
<div class="m2"><p>خانه ای را که نشستی نکنی تعمیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرن اکسیر شدی ای می صافی در خم</p></div>
<div class="m2"><p>هر که نوشید تو را از چه دهی تغییرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آهم آتشکده ای داشت نهان وقت سحر</p></div>
<div class="m2"><p>دل سنگین تو انداخته از تأثیرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورتت سوره نور است و زلطف بیچون</p></div>
<div class="m2"><p>خط تو از رقم مشک کند تفسیرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که دیوانه سودای پریرویان است</p></div>
<div class="m2"><p>همچو آشفته زیک موی بود زنجیرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازم آن ترک مجاهد که بجولانگه حسن</p></div>
<div class="m2"><p>سوده بر عارض خورشید دم شمشیرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشته زلف تو اندر کف اغیار افتاد</p></div>
<div class="m2"><p>دل دیوانه بگو تا چه بود تدبیرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسته عاشق بمیان از خم زلفت زنار</p></div>
<div class="m2"><p>زاهد شهر بگو تا که کند تکفیرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پنجه قدرت حق است بود دست خدای</p></div>
<div class="m2"><p>علی عالی اعلا که تو خوانی شیرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر نهم بر درش ار بخت کند تعجیلی</p></div>
<div class="m2"><p>خاک آن در شوم ار عمر بود تأخیرش</p></div></div>