---
title: >-
    شمارهٔ ۱۰۹۶
---
# شمارهٔ ۱۰۹۶

<div class="b" id="bn1"><div class="m1"><p>آن دل که در او باشد از عشق تو آزاری</p></div>
<div class="m2"><p>حاشا که کشد آزار هرگز زدل آزاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هست وفا کارت دلبر کند آزادت</p></div>
<div class="m2"><p>تا از تو کشم آزار جز این نکنم کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر جمله جهان یارند در چشم من اغیارند</p></div>
<div class="m2"><p>یاران دیگر مارند جز تو نبود باری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ساحت گلزارت چون دورم مهجورم</p></div>
<div class="m2"><p>در پای دلم ایکاش زآن باغ خلد خاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رستم زبت زنار تسبیح زکف دادم</p></div>
<div class="m2"><p>بر گردن جان بستم از زلف تو زناری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هر جسدی جانی پیدائی و پنهانی</p></div>
<div class="m2"><p>امکان همه خارستان در خار تو گلزاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گرد جهان گردند چون چرخ کجا جویند</p></div>
<div class="m2"><p>یک دلشده ی چون من یک همچو تو دلداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گلچین بهوای گلاز خار نپرهیزد</p></div>
<div class="m2"><p>با یاد تو چون یاری سازم بهر اغیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پرده دری عشق شد سر حقیقت فاش</p></div>
<div class="m2"><p>عشاق باین پاداش هر یک بسر داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کی همچو سمندر کس بر آتش تو بنشست</p></div>
<div class="m2"><p>هر چند در این دعوی برخاسته بسیاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته زاسرارت آگاه نخواهد شد</p></div>
<div class="m2"><p>در پرده سر حق چون محرم اسراری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو جان عزیزستی بیمانه من مسکین</p></div>
<div class="m2"><p>در مصر درون ما تو یوسف بازاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خیل سگان تو ای شیر حق افتادم</p></div>
<div class="m2"><p>شاید زکرم ما را زین سلسله بشماری</p></div></div>