---
title: >-
    شمارهٔ ۵۶۷
---
# شمارهٔ ۵۶۷

<div class="b" id="bn1"><div class="m1"><p>زآن صافی صوفی زآن درد صفا پرور</p></div>
<div class="m2"><p>یک جام بصوفی بخش یک نیمه بمن آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآن آتش سیاله زآن شعله جواله</p></div>
<div class="m2"><p>بر سرد دل ما زن تا بار دهد آذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من شاخک خشکیده تو آب بقا داری</p></div>
<div class="m2"><p>همچون شجر طورم شاید که کنی مثمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من قالب افسرده تو روح روان در کف</p></div>
<div class="m2"><p>عیسی زمان هست بر مرده دلان بگذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس دگران ایدل این نقش مخالف بست</p></div>
<div class="m2"><p>تو آینه صافی در خویش دمی بنگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگست ترا امکان رو عرصه دیگر جوی</p></div>
<div class="m2"><p>شاید که بکام دل خاکی بکنم بر سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طوف حرمت ایدل حاشا که ثمر بخشد</p></div>
<div class="m2"><p>تا شد بضمیر تو این نقش بتان مضمر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای طایف بیت الله کعبه نبود بالله</p></div>
<div class="m2"><p>این راه رو دیر است و این بتکده آذر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سودای سر زلفش پیداست زدود طول</p></div>
<div class="m2"><p>ناچار برآرد دود عود است چو بر مجمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته مس قلبت از حب علی زر شد</p></div>
<div class="m2"><p>لابد اثری دارد کبریت چو شد احمر</p></div></div>