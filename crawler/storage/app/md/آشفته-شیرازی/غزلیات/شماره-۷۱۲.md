---
title: >-
    شمارهٔ ۷۱۲
---
# شمارهٔ ۷۱۲

<div class="b" id="bn1"><div class="m1"><p>بر بهشت رخت آن خال که دیدم گفتم</p></div>
<div class="m2"><p>من چو آدم زپی گندم جنت افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش پای بجا طاق منم پیش رخت</p></div>
<div class="m2"><p>ابرویت گفت در این مرحله با تو جفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک غماز شدش پرده در مردم چشم</p></div>
<div class="m2"><p>راز عشق تو که در پرده دل بنهفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توبه ام داد که آیم سوی مسجد از دیر</p></div>
<div class="m2"><p>شیخ پنداشت که من وسوسه اش پذرفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه به بتخانه چین بودم و گه در تبت</p></div>
<div class="m2"><p>با خیال رخ و زلفت چو ببستر خفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو می لعل بلب داشتی از ساغر غیر</p></div>
<div class="m2"><p>من زالماس مژه لؤلؤ تر میسفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بامدادان زدرم آمد و بر گریه من</p></div>
<div class="m2"><p>غنچه وش کرد تبسم که چو گل بشکفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خیال تو نگنجد بدلم غیر از تو</p></div>
<div class="m2"><p>که من این خانه پی مقدم سلطان رفتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیر میخانه توحید علی سر الله</p></div>
<div class="m2"><p>که بجز از لب او سر خدا نشنفتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل آشفته که جا در خم زلفینش کرد</p></div>
<div class="m2"><p>گفت آشفته زسودای تو من آشفتم</p></div></div>