---
title: >-
    شمارهٔ ۷۸۷
---
# شمارهٔ ۷۸۷

<div class="b" id="bn1"><div class="m1"><p>دوش بی شمع جمالت بزم عیش افروختم</p></div>
<div class="m2"><p>عود وش از یاد زلفینت بر آتش سوختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صدف دیده بدامان ریخت در شام فراق</p></div>
<div class="m2"><p>آن گهرهائی که از خون جگر اندوختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز فغان و سوختن اندر طریق عاشقی</p></div>
<div class="m2"><p>بلبل و پروانه را من شیوه ها آموختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش همچون شیخ صنعان بر در دیر از وفا</p></div>
<div class="m2"><p>دین و دل در عشق آن ترسا بچه بفروختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامهای عاریت آشفته افکندم بزیر</p></div>
<div class="m2"><p>تا که بر تن کسوت مهر علی را دوختم</p></div></div>