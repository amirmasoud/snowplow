---
title: >-
    شمارهٔ ۵۱۹
---
# شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>می‌فروشان مددی کار ز حد مشکل شد</p></div>
<div class="m2"><p>پنبه شد رشته و آن سعی طلب باطل شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتم از یاد حریفان و نمی‌پرسندم</p></div>
<div class="m2"><p>مگر از ذکر غم عشق دلم غافل شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه توان گفت از این لجه پرموجه عشق</p></div>
<div class="m2"><p>که هر آن بحر به جنبش بنهی ساحل شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحرش بر در میخانه به جامی دادم</p></div>
<div class="m2"><p>آنچه در مدرسه از وسوسه‌ام حاصل شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ننهد نقطه صفت پای برون تا باشد</p></div>
<div class="m2"><p>هرکه در دایره عشق بتان داخل شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکنم وصف تو ای عشق کمالت این بس</p></div>
<div class="m2"><p>تا که دم از تو نزد عقل کجا کامل شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش رشک بسوزد پر پروانه مدام</p></div>
<div class="m2"><p>که چرا شمع رخت شاهد هر محفل شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسم آلوده شود دامن پاکت بنشین</p></div>
<div class="m2"><p>که زآب مژه‌ام عرصه امکان گل شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که شد سینه تو را مطلع انوار شهود</p></div>
<div class="m2"><p>لاجرم کعبه اصحاب حقیقت دل شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تویی آن پیر مغان ساقی میخانه عشق</p></div>
<div class="m2"><p>که به ترتیب عوالم نظرت فاعل شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر مهم من آشفته بسازی چه عجب</p></div>
<div class="m2"><p>که مهمات جهان را کرمت کافل شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علی عالی اعلا تویی و مظهر حق</p></div>
<div class="m2"><p>وای بر آن که زیاد تو دمی غافل شد</p></div></div>