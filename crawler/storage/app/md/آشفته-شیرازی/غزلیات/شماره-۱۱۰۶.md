---
title: >-
    شمارهٔ ۱۱۰۶
---
# شمارهٔ ۱۱۰۶

<div class="b" id="bn1"><div class="m1"><p>سودای تو هر شب کشدم بر سر کوئی</p></div>
<div class="m2"><p>چون آینه هر لحظه کنم روی بروئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی یکسر مو جان زفسون تو برد دل</p></div>
<div class="m2"><p>کامیخته سحر تو بر هر سر و موئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب بکجا میروی ای سرو خرامان</p></div>
<div class="m2"><p>کاز هر بن مژگان رود از یاد تو جوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز کنج در میکده ما را نبود کنج</p></div>
<div class="m2"><p>من غیر تو هرگز نکنم روی بسوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خوی نگردانم و روی از تو نپیچم</p></div>
<div class="m2"><p>ترکانه تو هر لحظه برائی و بخوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عود اگر سوخته ام بوی خوشم نیست</p></div>
<div class="m2"><p>در آه من از زلف تو آمیخته بوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سینه سیمین تو مگر نقره خامی</p></div>
<div class="m2"><p>ای دل تو در آن سینه مگر آهن و روئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای محتسب از عاقبت خویش بیندیش</p></div>
<div class="m2"><p>روزی که زنی سنگ تغافل بسبوئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هو بجز از نام علی هیچ مجوئید</p></div>
<div class="m2"><p>صوفی بعبث چند زنی هائی و هوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افکنده سر آشفته بمیدان ارادت</p></div>
<div class="m2"><p>شاید که شمارند زچوگان تو گوئی</p></div></div>