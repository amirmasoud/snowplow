---
title: >-
    شمارهٔ ۱۲۴۱
---
# شمارهٔ ۱۲۴۱

<div class="b" id="bn1"><div class="m1"><p>چو خم از خون دل در می‌کشم می</p></div>
<div class="m2"><p>به خون دل به این می برده‌ام پی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا تا لعل ساقی داده ساغر</p></div>
<div class="m2"><p>نخواهم خوردن از جام دگر می</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر پیوسته او بی می کند عیش</p></div>
<div class="m2"><p>ولیکن من نیازم عیش بی‌وی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبش را بوسه زد تا ساغر غیر</p></div>
<div class="m2"><p>می از گرمی غیرت کرده‌ام خوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکش در کاخ دل ای عشق مسند</p></div>
<div class="m2"><p>بساط این هوسناکی بکن طی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دل گفتم نهادم داغ رستم</p></div>
<div class="m2"><p>طبیبم گفت کی به گردد از کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز مرغ بام بشنو ذکر هوهو</p></div>
<div class="m2"><p>به غفلت اندری ای دل تو هی هی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبر دیگر رگم بیهوده فصاد</p></div>
<div class="m2"><p>که غیر از دوست نبود در رگ و پی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوای عشق و داغ اشتیاقت ‏</p></div>
<div class="m2"><p>ز خاکم بردماند لاله و نی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ستم زآن ترک اگر آشفته دیدی</p></div>
<div class="m2"><p>به حیدر شکوه کن پنهانی از وی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزن درویش شئ الله به آن در</p></div>
<div class="m2"><p>که امکان خود به پیش اوست لاشئ</p></div></div>