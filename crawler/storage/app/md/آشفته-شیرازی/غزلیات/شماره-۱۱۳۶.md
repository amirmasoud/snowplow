---
title: >-
    شمارهٔ ۱۱۳۶
---
# شمارهٔ ۱۱۳۶

<div class="b" id="bn1"><div class="m1"><p>مخمور سروریم کجائی می غم های</p></div>
<div class="m2"><p>مردیم بامید وفا جور و ستم های</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رشحه باران نبود شعله برقی</p></div>
<div class="m2"><p>از من مگذر غافل ای ابر کرم های</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبریز چو شد ساغر چه درد و چه صافی</p></div>
<div class="m2"><p>شکوه مکن از ساقی از بیش و زکم های</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگذار که سوزم دو جهان سوزش آهی</p></div>
<div class="m2"><p>یکدم مژه برهم منه ای دیده نم های</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسودگی ایدل همه در نیستی آمد</p></div>
<div class="m2"><p>تا بود که بیاسائیم ای خواب عدم های</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخساره ما سیم بود اشک زر سرخ</p></div>
<div class="m2"><p>ما را به چه کار آئی دینار و درم های</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی ساز و نوا جمله برقصند حریفان</p></div>
<div class="m2"><p>مطرب چه کشی نغمه از زیر و بم های</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذار دم باقی و بنگر رخ ساقی</p></div>
<div class="m2"><p>جامی بکش و قصه مخوان از کی و جم های</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم کاش گشایند در دیر که نگشود</p></div>
<div class="m2"><p>یکدم دلم آشفته از طوف حرم های</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دیر مغان مهبط انوار الهی</p></div>
<div class="m2"><p>گنجینه اسرار خدا کان کرم های</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شیر خدا شرک گرفته است جهانرا</p></div>
<div class="m2"><p>وقتست که بیرون بکشی تیغ دو دم های</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر مصلحت وقت نباشد که کشتی تیغ</p></div>
<div class="m2"><p>امداد بفرما بشه ملک عجم های</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا طمعه شمشیر کند اهل ضلالت</p></div>
<div class="m2"><p>وز عدل نماند بجهان نام ستم های</p></div></div>