---
title: >-
    شمارهٔ ۳۹۰
---
# شمارهٔ ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>دل سودازده را کار به سامان نرسد</p></div>
<div class="m2"><p>تا مرا دست به آن زلف پریشان نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل من تنگ و غم هجر فراوان چه کنم</p></div>
<div class="m2"><p>گر بامداد من آن دیده گریان نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بدامان نرسد دست منت نیست عجب</p></div>
<div class="m2"><p>دست درویش همانا که بسلطان نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه را مینگرم هست بعالم پایان</p></div>
<div class="m2"><p>شب هجر از چه ندانم که بپایان نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم یعقوب سفید است ببیت الاحزان</p></div>
<div class="m2"><p>آه اگر قافله مصر بکنعان نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی صدف حامله لؤلؤی لالا گردد</p></div>
<div class="m2"><p>شب گر از چشم ترم اشک بعمان نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل اشکم شده پیوسته بهم در شب هجر</p></div>
<div class="m2"><p>عجب ار قامت این موج بطوفان نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چه چون گوی بمیدان طلب گردیدم</p></div>
<div class="m2"><p>دست من در خم آنزلف پریشان نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر آشفته مبر زحمت بیهوده طبیب</p></div>
<div class="m2"><p>درد عشق است همان به که به درمان نرسد</p></div></div>