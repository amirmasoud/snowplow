---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>گلبنان انجمن خاص بیاراسته‌اند</p></div>
<div class="m2"><p>بلبلان خوش به نواسنجی برخاسته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهدان چمن از لطف تقاضای بهار</p></div>
<div class="m2"><p>غازه کردند رخ خویش و خود آراسته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک گلچهره‌بتان در همه فصل و همه وقت</p></div>
<div class="m2"><p>آنچنانند نه خود یک سر مو کاسته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارفانی که به گل نغمه چو بلبل سنجند</p></div>
<div class="m2"><p>گلبنانند که از گلبن دل خاسته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه درویش تو آشفته‌صفت ای مولا</p></div>
<div class="m2"><p>کسوت مهر تو بر خویش به پیراسته‌اند</p></div></div>