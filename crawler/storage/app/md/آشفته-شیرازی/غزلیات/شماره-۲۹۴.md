---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>زآن ملک حذر کن که در او پادشهی نیست</p></div>
<div class="m2"><p>آنشه نکند زیست که او را سپهی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز از دهن تنگ تو دلها نگشاید</p></div>
<div class="m2"><p>جز از خم زلف تو بخاطر گرهی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیحد دلم از خانقه ایشیخ به تنگست</p></div>
<div class="m2"><p>زین خانه بمیخانه همانا که رهی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن گلبن عشق است که پیوسته ببار است</p></div>
<div class="m2"><p>گل بر سر شاخ ارچه گهی هست گهی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حشمت موران ره عشق چگویم</p></div>
<div class="m2"><p>بی شبهه سلیمان بچنین دستگهی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر گشته ات آید پی دعوی بقیامت</p></div>
<div class="m2"><p>جز ناخن مخضوب تو او را گوهی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای اطلس زرتار غم عشق چه جنسی</p></div>
<div class="m2"><p>کاین دیبه بیرنگ بهر کارگهی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مسلک عشاقش جز میته نخوانند</p></div>
<div class="m2"><p>آن مرغ که او بسمل تیر نگهی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس به پریشانی آشفته زند طعن</p></div>
<div class="m2"><p>او را خبر از حلقه زلف سیهی نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق است علی مظهر حق نور ولایت</p></div>
<div class="m2"><p>جز در دل ویرانه اش آرامگهی نیست</p></div></div>