---
title: >-
    شمارهٔ ۱۰۶۷
---
# شمارهٔ ۱۰۶۷

<div class="b" id="bn1"><div class="m1"><p>رشته ها بر گردنم ای زلف یار آویختی</p></div>
<div class="m2"><p>فتنه کردی ای نگاه مست خونم ریختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منتی بر گردنم داری تو ای زلف دو تا</p></div>
<div class="m2"><p>کز همه زنجیر مویان رشته ام بگسیختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میبری نام رقیبان بر لب شیرین چرا</p></div>
<div class="m2"><p>زهر با قند مکرر از چه رو آمیختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید از جادوئی ضحاک آسا دم زنم</p></div>
<div class="m2"><p>تا تو ای مار سیه بر گردنم آویختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نبودت در کمین آن آهوان شیر گیر</p></div>
<div class="m2"><p>از چه آشفته به چین زلف او بگریختی</p></div></div>