---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>پری گذشت ازین کوچه یا ملک می‌رفت</p></div>
<div class="m2"><p>که نورها به سماوات از سمک می‌رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عرش نعره مستان ز کاخ میکده رفت</p></div>
<div class="m2"><p>مگو که زمزمه ذکری از ملک می‌رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گریه بود صراحی به چشم خون‌پالا</p></div>
<div class="m2"><p>ز ذوق قهقه جام بر فلک می‌رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژه نهشت شب هجر خسبدم دیده</p></div>
<div class="m2"><p>به پای مردمک چشم چون خسک می‌رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بماند لنگ و و به ایوان جاه تو نرسید</p></div>
<div class="m2"><p>خیال من که چنان اسب تیزتک می‌رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو رفتی وز نظر رفت روشنی بصر</p></div>
<div class="m2"><p>گمان مردم اگرچه به مردمک می‌رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نجات داد ز طوفان شها ولای تواش</p></div>
<div class="m2"><p>وگرنه نوح مخاطب لقد هلک می‌رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نار عشق مس قلب شد زر خالص</p></div>
<div class="m2"><p>عیار صیرفی آشفته بر محک می‌رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل از یقین خلیل آمد آتش نمرود</p></div>
<div class="m2"><p>یقین بدان که در آتش نه او به شک می‌رفت</p></div></div>