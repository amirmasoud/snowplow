---
title: >-
    شمارهٔ ۲۹۱
---
# شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>ای سلطنت امکان کم پایه زخدامت</p></div>
<div class="m2"><p>تقدیر و قضا هر روز سر در خط احکامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چیست سلیمانرا شد ملک جهان در حکم</p></div>
<div class="m2"><p>بر خاتم انگشتش گر نقش نشد نامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مظهر یزدانی تو آیت سبحانی</p></div>
<div class="m2"><p>آغاز تو کی دانم گوئیم زانجامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عیسی دم روحانی بگرفته زلعل تو</p></div>
<div class="m2"><p>این زندگی جاوید برده خضر از جامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشنیده کلام حق کس جز زدهان تو</p></div>
<div class="m2"><p>جبریل نیاورده جز گفته و پیغامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذات الله علیائی وجه الله مولائی</p></div>
<div class="m2"><p>هر روز حرم گردد بر گرد درو بامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته زهر در روی در کوی تو آورده</p></div>
<div class="m2"><p>درویش سرا باشد شایسته انعامت</p></div></div>