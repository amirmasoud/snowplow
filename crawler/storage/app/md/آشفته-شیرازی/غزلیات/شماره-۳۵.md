---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای رفته و نشناخته قدر دل ما را</p></div>
<div class="m2"><p>باز آی که مردیم زهجر تو خدا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روزه جفا کردی و گفتی که وفا بود</p></div>
<div class="m2"><p>طفلی زوفا فرق نکردی تو جفا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردد بجفای تو گرفتار ادیبت</p></div>
<div class="m2"><p>کاداب وفا هیچ نیاموخت شما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خانه حق است خرابش چه پسندی</p></div>
<div class="m2"><p>غم نیست که نشناخته ی خانه خدا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بختم نشود یار که روی تو ببینم</p></div>
<div class="m2"><p>در منزل خورشید کجا بار سها را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد از تو و درمان زتو بهر چه طبیبا</p></div>
<div class="m2"><p>در دم بفرستی و کنی منع دوا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته نگفتم که مکن رخنه بمویش</p></div>
<div class="m2"><p>کاشفته کنی همچو خود آنزلف دو تا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مذهب ما خاک نجف آب حیات است</p></div>
<div class="m2"><p>گو خضر طلبکار بود آب بقا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر عرض تفاخر کند ار خاک تو شاید</p></div>
<div class="m2"><p>زیرا که در او خانه بود شیر خدا را</p></div></div>