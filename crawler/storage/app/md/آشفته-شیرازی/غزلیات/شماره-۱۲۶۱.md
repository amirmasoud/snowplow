---
title: >-
    شمارهٔ ۱۲۶۱
---
# شمارهٔ ۱۲۶۱

<div class="b" id="bn1"><div class="m1"><p>مردم دیده منی نور دو چشم مردمی</p></div>
<div class="m2"><p>جز تو پری کجا کند جلوه بشکل آدمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ و بهار مردمان گر زگلست و یاسمین</p></div>
<div class="m2"><p>باغ و بهار من توئی ای تو بهار خرمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون تو مسیح دم بتی شاید کاید از عدم</p></div>
<div class="m2"><p>روح قدس بمریمی دم زند ار بهمدمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف سیاه اهرمن چهره بهشت جاودان</p></div>
<div class="m2"><p>وای بحال آدمی خانه کند چو گندمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوبت شاهی ار زنی میسزدت که از شرف</p></div>
<div class="m2"><p>کاکل و زلف تو کند بر مه و مهر پرچمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست زجنبش مژه چاک دل رفوی او</p></div>
<div class="m2"><p>سحر مبین که در دمی تیری کرد و مرهمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زلف تو کعبه جهان خال سیاه تو حجر</p></div>
<div class="m2"><p>محرم تشنه را کند گو لب لعل زمزمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسرو محتشم شدم مالک ملک جم شدم</p></div>
<div class="m2"><p>کرد چو آن عقیق لب از سر مهر خاتمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جادوی چشم را بگو با همه مستی از کجا</p></div>
<div class="m2"><p>کرده بدست زابروان تیغ کجی بدین خمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سرمستی ای سیه تیغ مکش بروی مه</p></div>
<div class="m2"><p>شق قمر بود گنه جز زنبی هاشمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاتم خیل انبیا صاحب رتبه دنی</p></div>
<div class="m2"><p>آنکه بغیر حیدرش کس نه سزای محرمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشفته بعقده ی ناخن تو گره گشا</p></div>
<div class="m2"><p>شاید اگر برآریش زین خم و پیچ درهمی</p></div></div>