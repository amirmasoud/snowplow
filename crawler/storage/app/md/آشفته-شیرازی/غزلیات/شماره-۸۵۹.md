---
title: >-
    شمارهٔ ۸۵۹
---
# شمارهٔ ۸۵۹

<div class="b" id="bn1"><div class="m1"><p>مگو که شرح فراق تو را شماره نکردم</p></div>
<div class="m2"><p>شبی نشد که کناری پر از ستاره نکردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غریق بحر و خود مستحق طوفانم</p></div>
<div class="m2"><p>چرا که از نم چشمان تر کناره نکردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زچاک سینه عیان است مرغ دل بنگر</p></div>
<div class="m2"><p>مگو که جامه جان از غم تو پاره نکردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبیب عشق چه خوش گفت با مریضان دوش</p></div>
<div class="m2"><p>که گفت درد دل خوش را که چاره نکردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرفته لذت قربان شدن مرا از کام</p></div>
<div class="m2"><p>دریغ و درد که جانرا فدا دوباره نکردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار آیت غم دیده ام از این سودا</p></div>
<div class="m2"><p>مگر بمصحف روی تو استخاره نکردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه پرسی از من بیدل که چاه یوسف کو</p></div>
<div class="m2"><p>مگر بسیب زنخدان تو اشاره نکردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غمم زکشته شدن نیست این غمم گشته</p></div>
<div class="m2"><p>که از چه دردم کشتن تو را نظاره نکردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صنم پرست نه آشفته و ندیده صمد را</p></div>
<div class="m2"><p>بجز بمظهر حق مرتضی اشاره نکردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگیر خرده که زنار و سبحه بگسستم</p></div>
<div class="m2"><p>ولیک رشته مهر و وفات پاره نکردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زسیل اشک نشاید نشاند آتش دل را</p></div>
<div class="m2"><p>که آب شور زدم چاره شراره نکردم</p></div></div>