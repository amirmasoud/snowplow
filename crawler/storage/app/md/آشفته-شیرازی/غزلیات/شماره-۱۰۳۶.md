---
title: >-
    شمارهٔ ۱۰۳۶
---
# شمارهٔ ۱۰۳۶

<div class="b" id="bn1"><div class="m1"><p>جسمم چو شمع در غم جانان گداخته</p></div>
<div class="m2"><p>تنها نه تن که در بدنم جان گداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگداخت جان بجسمم و بس خوش دلم که هست</p></div>
<div class="m2"><p>خالص زری کز آتش سوزان گداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه سوخت زآتش دیدار و دم نزد</p></div>
<div class="m2"><p>افسوس از آن که از تف حرمان گداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چاک سینه جست زبس آه شعله دار</p></div>
<div class="m2"><p>سر همچو شمع تا بگریبان گداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد همچو زیب چشم و فرو ریخت از مژه</p></div>
<div class="m2"><p>کاندر چراغ در شب هجران گداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشکم گداخت در صدف دیده ایعجب</p></div>
<div class="m2"><p>لؤلؤ که دیده در دل عمان گداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود از کجا همی جهد این برق خانه سوز</p></div>
<div class="m2"><p>کز او تمام دشت و بیابان گداخته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خون است لاله سربسر اما نمیچکد</p></div>
<div class="m2"><p>خونش بدل زداغ گلستان گداخته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو آبشار رحمت و ساقی کوثری</p></div>
<div class="m2"><p>آشفته ات زتابش نیران گداخته</p></div></div>