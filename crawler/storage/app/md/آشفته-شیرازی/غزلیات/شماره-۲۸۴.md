---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>خوب باشد گر ز خوبان سر زند کردار زشت</p></div>
<div class="m2"><p>احولی باشد که کس زشتی ببیند در بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو در کعبه نباشی ای بدا حال حرم</p></div>
<div class="m2"><p>ور تو در بتخانه آیی ای خوشا وقت کنشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوثر و غلمان جنت را نیاری در نظر</p></div>
<div class="m2"><p>گر بنوشی می ز دست ساقی حوراسرشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه حوری و غلمان بهشتی تا به کی</p></div>
<div class="m2"><p>گر بهشتی‌روی یاری باشدت بر طرف کشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان زاهد نصیب ما بهشت نقد کرد</p></div>
<div class="m2"><p>آنکه بر طرف بناگوش بتان این خط نوشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک ما را گل کن ای معمار مستان از شراب</p></div>
<div class="m2"><p>تا ز خاکم دست دو ران برنیاوره است خشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چه دانی ساخت جم جام جهان‌بین ای حکیم</p></div>
<div class="m2"><p>از همان خشتی که بهر تجربت بر خم بهشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موبه‌مو آشفته را در حلقه زلف تو بست</p></div>
<div class="m2"><p>دست قدرت کز ازل این رشته صد تار رشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حال کون و مکان پیشش نمی‌ارزد جوی</p></div>
<div class="m2"><p>دانه حب علی هرکس به دشت دل نکشت</p></div></div>