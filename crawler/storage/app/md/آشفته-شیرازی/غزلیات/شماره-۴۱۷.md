---
title: >-
    شمارهٔ ۴۱۷
---
# شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>جهد کن جهد که تیرش ز کمان می‌گذرد</p></div>
<div class="m2"><p>هرکه آن تیر و کمان دید ز جان می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرسان می‌گذرد جان جهانش به رکیب</p></div>
<div class="m2"><p>جهدی ای جان که زره جان جهان می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک جهان فتنه بیاورد چو آمد در شهر</p></div>
<div class="m2"><p>تا چه از رفتن آن مه به جهان می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفته بودی که زمانی به لبت لب بنهم</p></div>
<div class="m2"><p>هان دم بازپسین است و زمان می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک فلک اخترش افتند به لرزه چو سهیل</p></div>
<div class="m2"><p>به یمن گر شبی آن برق یمان می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش رخسار بدیع تو بسی مختصر است</p></div>
<div class="m2"><p>وصف عشاق که از شرح و بیان می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتدش آتش غیرت به دهان آشفته</p></div>
<div class="m2"><p>شمع‌سان نام تواش چون به زبان می‌گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و سودای تو از سود و زیانم چه غمست</p></div>
<div class="m2"><p>عاشق روی تو از سود و زیان می‌گذرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کرا داغ غلامی علی بر جبهه است</p></div>
<div class="m2"><p>از سر سلطنت کون و مکان می‌گذرد</p></div></div>