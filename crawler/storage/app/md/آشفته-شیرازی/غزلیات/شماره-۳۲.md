---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>داده دو ترک تو صلا باز سپاه ناز را</p></div>
<div class="m2"><p>تا که بخاک و خون کشد عاشق نو نیاز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهوی دشت عشق را شیر اسیر دام شد</p></div>
<div class="m2"><p>صعوه کوه تو درد سینه شاهباز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طایف کعبه بنگرد گر بحریم معنوی</p></div>
<div class="m2"><p>گرد مقام کوی تو طوف بود حجاز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آشفته پاکباز شو این ورق دغل بنه</p></div>
<div class="m2"><p>چند بششدر افکنی رند قمارباز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی بهر طرف کنم قبله اهل دل توئی</p></div>
<div class="m2"><p>شاید اگر بشش جهة فرض نهم نماز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک ستم شعار من بو که شوم اسیر تو</p></div>
<div class="m2"><p>ترک مکن خدای را غارت و ترکتاز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مویه سزاست بعد از این راست روان پارس را</p></div>
<div class="m2"><p>ریخت چو در عراق خون پادشه حجاز را</p></div></div>