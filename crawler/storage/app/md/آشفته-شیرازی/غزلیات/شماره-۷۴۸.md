---
title: >-
    شمارهٔ ۷۴۸
---
# شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>بندم از زلف و خط سلسله مویان زنار</p></div>
<div class="m2"><p>تا که شایسته ات آشفته به تکفیر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست زنار کمند خم زلف جانان</p></div>
<div class="m2"><p>کفر بیشایبه بر عشق تو تفسیر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ای عشق بجز دست خدا گر قدرت</p></div>
<div class="m2"><p>بقفا گوئی همواره چه تقدیر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم شرح مدیحت زدبیر افلاک</p></div>
<div class="m2"><p>گفت جف القلم آشفته چه تحریر کنم</p></div></div>