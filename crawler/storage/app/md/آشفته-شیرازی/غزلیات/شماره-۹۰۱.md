---
title: >-
    شمارهٔ ۹۰۱
---
# شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>وه که بناف خون شود نافه آهوان چین</p></div>
<div class="m2"><p>تا که بباد داده ای طره و زلف عنبرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ببری هزار دل نیستشان خبر زهستیم</p></div>
<div class="m2"><p>بسکه سکنج زلف تو خم بخم است و چین بچین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار زراه میرسد غالیه سوده بر سمن</p></div>
<div class="m2"><p>باد زباغ میوزد مشک ختن در آستین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد لبت چه دید خم و هم فتاد بر غلط</p></div>
<div class="m2"><p>گفت هجوم مور بین باز بگرد انگبین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاخ تمام لاله شد تا تو کشیده ای نقاب</p></div>
<div class="m2"><p>بزم پر از ستاره شد خوی چو فشاندی از جبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده عشق ساز کن مطرب بزم عاشقان</p></div>
<div class="m2"><p>تا که بمزد چنگ تو بذل کنیم عقل و دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو چمان من چو خضر ار گذرد بسوی دشت</p></div>
<div class="m2"><p>جای گناه سر زند ناز و کرشمه ار زبین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف سیاه هندویش غمزه چشم جادویش</p></div>
<div class="m2"><p>آن زیسار میبرد و آن کشدم سوی یمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منع نظر نمیکند آشفته زاهدم دگر</p></div>
<div class="m2"><p>بنگرد ار بچشم ما آینه خدای بین</p></div></div>