---
title: >-
    شمارهٔ ۸۷۴
---
# شمارهٔ ۸۷۴

<div class="b" id="bn1"><div class="m1"><p>شک نیست که شکر لب و بسته دهن است آن</p></div>
<div class="m2"><p>اما نه سزای لب و دندان من است آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخساره نگویم که رخت باغ بهشت است</p></div>
<div class="m2"><p>بالای تو طوبی است نه سرو چمن است آن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرمی تن اوست دلیل دل مسکین</p></div>
<div class="m2"><p>هشدار که آهن دل و سیمین بدن است آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای غیر مبادا که شوی همسفر دوست</p></div>
<div class="m2"><p>گرگی تو و خود یوسف گل پیرهن است آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدین تو بر قامت رعنا به چه ماند</p></div>
<div class="m2"><p>بر سرو سهی رسته دو برگ سمن است آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخیز ززلفش که نیفتی سلاسل</p></div>
<div class="m2"><p>بگریز از آن چشم که ذات الفتن است آن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین نیست گریزم که درو دل شده مفتون</p></div>
<div class="m2"><p>زآنست گریزم که شکن در شکن است آن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانان نگذارم که بها جان بستاند</p></div>
<div class="m2"><p>یوسف نفروشم که محقر ثمن است آن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منصور اگر گفت انا لحق منکش عیب</p></div>
<div class="m2"><p>زیرا که در آن زمزمه بی خویشتن است آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر دل که در او نیست ولای شه مردان</p></div>
<div class="m2"><p>مردش بحقیقت مشماری که زن است آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاهنشه آفاق علی بحر ولایت</p></div>
<div class="m2"><p>کش گوهر بحرین حسین و حسن است آن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر رستم و روئین تنت آمد بصف رزم</p></div>
<div class="m2"><p>هر درع که پوشید برزمت کفن است آن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا خاتم مدح تو شدش زینت انگشت</p></div>
<div class="m2"><p>آشفته سلیمان شده گر اهرمن است آن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی دوست گرت جنت فردوس ببخشند</p></div>
<div class="m2"><p>بالله نه بهشت است که بیت الحزن است آن</p></div></div>