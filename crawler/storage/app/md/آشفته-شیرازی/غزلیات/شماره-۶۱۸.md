---
title: >-
    شمارهٔ ۶۱۸
---
# شمارهٔ ۶۱۸

<div class="b" id="bn1"><div class="m1"><p>شبی کان کودکم آید در آغوش</p></div>
<div class="m2"><p>کنم صبح جوانی را فراموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام لعبت حوری نژادم</p></div>
<div class="m2"><p>که شد غلمان بخلدش حلقه در گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نوشم چو خضر آب حیاتش</p></div>
<div class="m2"><p>نخواهم گفت حرف از چشمه نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر چون دوش بر چشمم زنی تیر</p></div>
<div class="m2"><p>نمیگیرم دو چشم از آن بر دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خیر است در کیش تو قربان</p></div>
<div class="m2"><p>منت قربان شوم در خیر میکوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پریشان بر رخ او زلف مشکین</p></div>
<div class="m2"><p>بمهر از غالیه بنهاده سرپوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخواهد اوفتاد از جوش خونم</p></div>
<div class="m2"><p>زنند اغیار تا در بزم تو جوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه گلزار است یا رب عشق کانجا</p></div>
<div class="m2"><p>لب بلبل بود چون غنچه خاموش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریفان مست می سرخوش زباده</p></div>
<div class="m2"><p>من آشفته زساقی مست و مدهوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه ساقی ساقی بزم محبت</p></div>
<div class="m2"><p>علی کاوراست امکان حلقه در گوش</p></div></div>