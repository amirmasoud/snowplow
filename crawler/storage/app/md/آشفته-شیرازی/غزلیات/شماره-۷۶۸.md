---
title: >-
    شمارهٔ ۷۶۸
---
# شمارهٔ ۷۶۸

<div class="b" id="bn1"><div class="m1"><p>حاشا که بجز تو بخیال دگرستم</p></div>
<div class="m2"><p>در کعبه و بتخانه اگر مینگرستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا هندوی خال تو بر آتشکده روست</p></div>
<div class="m2"><p>نشگفت که هندو شوم آتش بپرستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم میزنم از زندگی و کشته عشقم</p></div>
<div class="m2"><p>من غرق تو گشتم زخودی کی خبرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با کفر سر زلف تو زاسلام بریدم</p></div>
<div class="m2"><p>با بستگی عشق تو از عقل برستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد دل سودا زده با زلف تو پیغام</p></div>
<div class="m2"><p>جز آه سحرگاه ندانم که فرستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بشکستی اگر توبه و گر جام و گر عهد</p></div>
<div class="m2"><p>گر سر برود بر سر پیمان درستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابرم من آشفته و او غنچه بستان</p></div>
<div class="m2"><p>خندان شود آن لحظه که من خون بگرستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرده دل گشت عیان طلعت لیلا</p></div>
<div class="m2"><p>مجنون شدم و سلسله عقل گسستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برخواسته ام گرد صفت از سر کونین</p></div>
<div class="m2"><p>تا خاک شده بر در حیدر بنشستم</p></div></div>