---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>ممکنی را گرچه ممکن چاره ای در کار نیست</p></div>
<div class="m2"><p>لیک چون من هیچکس در کار خود ناچار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتشی دارم که نتوانم نهفتن در درون</p></div>
<div class="m2"><p>هست سری در دلم که ام قوه اظهار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک غماز است اشک و پرده در آه است آه</p></div>
<div class="m2"><p>دوست و دشمن خبر شد حاجت گفتار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستم و خواهم زهوشیاران دوای درد خویش</p></div>
<div class="m2"><p>آه کاندر دور ما یک عاقل و هشیار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بکوهی درد دل گویم بنالد با صدا</p></div>
<div class="m2"><p>پس اگر خواهم بگویم کس به از دیوار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو طبیبی تا کند درمان آزار دلم</p></div>
<div class="m2"><p>کس نمی بینم که ما را در پی آزار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من گرفتار یار نبود که ام برد باری زدل</p></div>
<div class="m2"><p>باری آن یاری کجا کز او بدوشم بار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست اندر مسجد و میخانه مطلب جز یکی</p></div>
<div class="m2"><p>حق پرستی بیگمان در سبحه و زنار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صرف مهر این و آن آشفته کردی عمر خویش</p></div>
<div class="m2"><p>گر جفا از این و آن میبری بسیار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چاره گر خواهی بکار خود مدد جو از عل</p></div>
<div class="m2"><p>آنکه عالم را جز او کس نقطه پرگار نیست</p></div></div>