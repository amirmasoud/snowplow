---
title: >-
    شمارهٔ ۱۰۴۸
---
# شمارهٔ ۱۰۴۸

<div class="b" id="bn1"><div class="m1"><p>چند مسافرت دلا سوی حجاز می‌کنی</p></div>
<div class="m2"><p>کعبه دل طواف کن گر تو نماز می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوف دل شکسته کن میل درون خسته کن</p></div>
<div class="m2"><p>چون به حقیقی رسی ترک مجاز می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش این نیاز من گفت نه یک‌هزار نه</p></div>
<div class="m2"><p>خود تو اگر ز ناز ما کسب نیاز می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگ حجاز عشق را توشه به جز صفا منه</p></div>
<div class="m2"><p>زاد سفر زآب و نان بیهده ساز می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس مست خفته‌اش دیده مردمان ببست</p></div>
<div class="m2"><p>نرگس باغ چشم خود بیهده باز می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه این و آن مخوان وصفی از آن دهان بگو</p></div>
<div class="m2"><p>رفت سبکتکین و تو وصف ایاز می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم آشفته را ز تو بود عجب فسانه</p></div>
<div class="m2"><p>گفت بگو ز زلف اگر قصه دراز می‌کنی</p></div></div>