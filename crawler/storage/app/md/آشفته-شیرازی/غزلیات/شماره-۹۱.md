---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>شاها تو خود غریبی و آشفته ات غریب</p></div>
<div class="m2"><p>رحمی که بر غریب بود مهربان غریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکشد عاقبتم درد غم عشق حبیب</p></div>
<div class="m2"><p>بود آیا که کند چاره این درد طبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه یاران سفری گشته و من مانده بجا</p></div>
<div class="m2"><p>خرم آن روز که گوئی سفری گشت رقیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو ای روح روان پای نهادی برکاب</p></div>
<div class="m2"><p>روح من گرد صفت رفت بدنبال رکیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم آن مرغ که غربت بودم صحن چمن</p></div>
<div class="m2"><p>زآشیان مرغی اگر دور شود هست غریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دام تزویر بود سبحه و زنار دلا</p></div>
<div class="m2"><p>خویشتن را تو باین دام بیا و مفریب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناشکیب است بلی عاشق صادق از دوست</p></div>
<div class="m2"><p>عاشق آن نیست که در دل بودش صبر و شکیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب آن نیست که از غیر خطا رفت بدوست</p></div>
<div class="m2"><p>گر خطائی زود از دوست بتو هست عجب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه مسلمان بمن آشفته وفا کرد نه گبر</p></div>
<div class="m2"><p>ببرم رشته سبحه شکنم عود و صلیب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دادخواهی بعلی کن زخطای اغیار</p></div>
<div class="m2"><p>که بود دست خداوند و رقیب است و حبیب</p></div></div>