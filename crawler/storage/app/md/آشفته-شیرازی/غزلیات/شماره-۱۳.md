---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بیدوست پایدار نباشد نشست ما</p></div>
<div class="m2"><p>اما چه چاره چاره نیاید زدست ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشست نقش دوست و برخاست ماسوا</p></div>
<div class="m2"><p>در بزم غیر هم بتو باشد نشست ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خون شده است و ریخته در ساغرم چو می</p></div>
<div class="m2"><p>بگذر تو محتسب زچه خواهی شکست ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قتلی اگر که مست کند لاجرم خطاست</p></div>
<div class="m2"><p>خاصه که از ختا بود این ترک ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رشته دو زلف بتی اهرمن فریب</p></div>
<div class="m2"><p>زنار بسته است دل بت پرست ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفته پی بمرتبه مرتضی که برد</p></div>
<div class="m2"><p>بر اوج عشق ره نبرد عقل پست ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حق رتبه اش شناسد و احمد ولیک من</p></div>
<div class="m2"><p>دانم طفیل اوست همه بود و هست ما</p></div></div>