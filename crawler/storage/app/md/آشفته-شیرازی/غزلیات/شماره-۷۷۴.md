---
title: >-
    شمارهٔ ۷۷۴
---
# شمارهٔ ۷۷۴

<div class="b" id="bn1"><div class="m1"><p>ای مظهر جان‌آفرین جانی تو از سر تا قدم</p></div>
<div class="m2"><p>هر جا و جودی شد عیان پیش وجودت شد عدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آسمانت آستان گرد رهت کون و مکان</p></div>
<div class="m2"><p>بر لوح کن بی‌امر تو کی کار فرماید قلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم به نخلی ماند او کز لب رطب افشاند او</p></div>
<div class="m2"><p>کی نخل می‌آرد رطب ای باغبان سر تا قدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده ز عارض باز کن قتل جهان آغاز کن</p></div>
<div class="m2"><p>بر مهر و بر مه ناز کن تو شاه مهر و مه خدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لا تقتلوا صید الحرم گفته نبی محترم</p></div>
<div class="m2"><p>تقصیر نبود لاجرم صیدار کشد صاحب حرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او قصد اگر زحمت کند عاشق از او راحت کند</p></div>
<div class="m2"><p>دشمن اگر رحمت کند بر دوستان باشد ستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کم کن جرس این ولوله لیلی است چون در قافله</p></div>
<div class="m2"><p>گر طی کند صد مرحله بر ساربانانش چه غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زلف عقده بر گسل بر دست اغیارش مهل</p></div>
<div class="m2"><p>آشفته را بر جان و دل مپسند جانا این ستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای نور طور از نار تو عرش برین بازار تو</p></div>
<div class="m2"><p>از آینهٔ رخسار تو پیداست انوار قدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو شمع بزم وحدتی محفل‌فروز کثرتی</p></div>
<div class="m2"><p>الحق مقام حیرتی ای حیدر صاحب کرم</p></div></div>