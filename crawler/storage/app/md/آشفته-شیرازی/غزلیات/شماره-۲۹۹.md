---
title: >-
    شمارهٔ ۲۹۹
---
# شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>این روز پی خجسته میلاد احمد است</p></div>
<div class="m2"><p>روز بروز پرتو انوار سرمد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم فرش را بعرش تفوق زمقدش</p></div>
<div class="m2"><p>هم خاک را تفاخر بر فرق فرقد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از مشعلش چراغ کواکب منور است</p></div>
<div class="m2"><p>از مطبخش سپهر بخاری مصعد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مریم اگر بفخر زروح مجسم است</p></div>
<div class="m2"><p>آن جان پاک غیرت روح مجرد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او را بهشت عدن کمین میهمان سراست</p></div>
<div class="m2"><p>جم را گر افتخار بصرح ممرد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر جا که طفل مکتب فضلش زبان گشود</p></div>
<div class="m2"><p>آنجا حکیم عقل بتحصیل ابجد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منسوخ گشت جمله ادیان ما سلف</p></div>
<div class="m2"><p>بشری لکم که نوبت دین مجدد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امی ولی معلم علم لدنی است</p></div>
<div class="m2"><p>بی سایه لیک سایه او ظل ممتد است</p></div></div>