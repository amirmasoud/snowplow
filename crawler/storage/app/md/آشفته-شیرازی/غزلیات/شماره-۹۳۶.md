---
title: >-
    شمارهٔ ۹۳۶
---
# شمارهٔ ۹۳۶

<div class="b" id="bn1"><div class="m1"><p>شاهی طلبی خود بدر عشق گدا کن</p></div>
<div class="m2"><p>چون شمع در این مرحله ترک سر و پا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردی که بدرمانش درمانده فلاطون</p></div>
<div class="m2"><p>از بوسه ساقی و لب جام دوا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنمای هلال خم ابرو همه روی</p></div>
<div class="m2"><p>زین جلوه تو خورشید و مه انگشت نما کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخرام بباغ ای گل و بر جا بنشان سرو</p></div>
<div class="m2"><p>چون غنچه تبسم کن و گل جامه قبا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عرصه محشر که شهیدان تو جمعند</p></div>
<div class="m2"><p>بر جای دیت گوشه چشمی سوی ما کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خازن جنت بعمل خانه فروشی</p></div>
<div class="m2"><p>در بر رخ ما باز تو از بهر خدا کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند گر آشفته بود رند و گنه کار</p></div>
<div class="m2"><p>بر ما تو نظر نه به رخ آل عبا کن</p></div></div>