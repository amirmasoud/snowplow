---
title: >-
    شمارهٔ ۹۴۷
---
# شمارهٔ ۹۴۷

<div class="b" id="bn1"><div class="m1"><p>ماند به زیر بار ناز این دل نو نیاز من</p></div>
<div class="m2"><p>باز کرشمه می‌کند دلبر عشوه‌ساز من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناز تو کی خرد کسی جز دل مستمند من</p></div>
<div class="m2"><p>عاشق یکدیگر بود ناز تو و نیاز من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ رود زبرگ گل سرو فتد زسرکشی</p></div>
<div class="m2"><p>گر بچمن چمان شود گلبن سرفراز من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاش که پرده برکشد آن مه خرگهی زرخ</p></div>
<div class="m2"><p>تا بسحر بدل شود تیره شب دراز من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون شوی ایدل از چه رو میکشیم تو کو بکو</p></div>
<div class="m2"><p>چند تو فاش میکنی طفل سرشک راز من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیهده عنکبوت شد مضطرب شکار خود</p></div>
<div class="m2"><p>صید مگس نمیکند هرگز شاهباز من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهر علیست در دل و مدح علیست بر زبان</p></div>
<div class="m2"><p>بر دگران از آن بود آشفته امتیاز من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر تو نگروم بکس غیر تو ننگرم بکس</p></div>
<div class="m2"><p>دوخته ناوک نظر هر سو چشم باز من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیخ بکعبه حجار ارچه مفاخرت کند</p></div>
<div class="m2"><p>کعبه من در علی دشت نجف حجاز من</p></div></div>