---
title: >-
    شمارهٔ ۶۳۲
---
# شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>یافتی از کمند هجر خلاص</p></div>
<div class="m2"><p>حمدی ایدل بخوان تو از اخلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زرخالص کند بر آتش صبر</p></div>
<div class="m2"><p>کاب و رنگش فزون شود زخلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون خم را اگر صراحی خورد</p></div>
<div class="m2"><p>ساقیا خون او بخور به قصاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو کدامی که آفتاب سپهر</p></div>
<div class="m2"><p>در هوایت چو ذره شد رقاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز علی گوهری بدست نکرد</p></div>
<div class="m2"><p>هر که در بحر عشق شد غواص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گلی دیگران شها همه خار</p></div>
<div class="m2"><p>تو زری دیگران نحاس و رصاص</p></div></div>