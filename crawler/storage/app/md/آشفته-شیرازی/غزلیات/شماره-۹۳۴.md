---
title: >-
    شمارهٔ ۹۳۴
---
# شمارهٔ ۹۳۴

<div class="b" id="bn1"><div class="m1"><p>به آن امید که مطلب بر او شود روشن</p></div>
<div class="m2"><p>شبان طور رود سوی وادی ایمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دست حسن بتان نیست غیر باد به دستت</p></div>
<div class="m2"><p>که خوشه چین نبرد دانه ای از آن خرمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زعجز و لابه به عاشق در او اثر نکند</p></div>
<div class="m2"><p>که هست با تن سیمین دل بتان آهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعشق و رندی و مستی سمر شدم چکنم</p></div>
<div class="m2"><p>نه زاهدم که بپوشم لباس شید بتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدست غیر مده لعل لب زبوالهوسی</p></div>
<div class="m2"><p>نگین جم چکنی زیب دست اهریمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکن تو دست بگردن رقیب را زنهار</p></div>
<div class="m2"><p>که خون عاشق مسکین بگیردت دامن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گمان مکن که گذاریم دامنت از دست</p></div>
<div class="m2"><p>اگر تو دست فشانی و برکشی دامن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو غنچه لب چه بزه کرده ای خدنگ نظر</p></div>
<div class="m2"><p>چو گل شهید تو پوشد زخون خویش کفن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاد زلف تو آشفته شب بود افزون</p></div>
<div class="m2"><p>که شب غریب بود بیشتر بیاد وطن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخبث نفس گنه کار من شکی نبود</p></div>
<div class="m2"><p>بآب مدح علی لیک شسته ام سروتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر که نافه چینم زخامه میریزد</p></div>
<div class="m2"><p>که بوی مشک شنیدند مردمم ز سخن</p></div></div>