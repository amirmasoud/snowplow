---
title: >-
    شمارهٔ ۴۰۲
---
# شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>زجانان ناگزیر است آنکه را در جسم جان باشد</p></div>
<div class="m2"><p>که بی جانانه جان در تن چو جسمی بی روان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر گنجیست لابد پاسبان ماری و زلفینت</p></div>
<div class="m2"><p>بود ماری که کارش پاس گنج شایگان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی آن خسرو شیرین که با لعل شکرخندت</p></div>
<div class="m2"><p>جهان گر سربسر قندمکرر رایگان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فروهل پرده از عارض که گل بر شاخ میبالد</p></div>
<div class="m2"><p>بچم تابنده بالات سرو بوستان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعهد یاریت دایه میان بربسته برمهدم</p></div>
<div class="m2"><p>همان عهدم بپایان لحد اندر میان باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمهر مهر تو گنج ازل در سینه بنهفتم</p></div>
<div class="m2"><p>گرم از قهر میرانی که مهر من همان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا مغبچه ی ساقی بکوی میفروشان بس</p></div>
<div class="m2"><p>اگر گویند حوری در بهشت جاودان باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ستاده جان بکف یعقوب با شوق و شعف در ره</p></div>
<div class="m2"><p>مگر پیراهن یوسف میان کاروان باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسوزانند چون شمعم اگر آشفته سر تا پا</p></div>
<div class="m2"><p>بگویم شرح عشقش تا که در کامم زبان باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کدامین عشق حیدر نور سرمد مظهر واجب</p></div>
<div class="m2"><p>که در کریاس او یک پرده هفتم آسمان باشد</p></div></div>