---
title: >-
    شمارهٔ ۷۰۳
---
# شمارهٔ ۷۰۳

<div class="b" id="bn1"><div class="m1"><p>سیل اشکم به شب هجر چو پیوست به هم</p></div>
<div class="m2"><p>کشتی و نوح به یک موجش بشکست به هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواستم نقش مه و سنبله از کلک قضا</p></div>
<div class="m2"><p>نقش رخساره و گیسوی تو پیوست به هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز خط و زلف که پیوست به هم کس نشنید</p></div>
<div class="m2"><p>خضر و اهریمن و جادو که دهد دست به هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمکان در خم ابروی کجت دانی چیست</p></div>
<div class="m2"><p>تیغ‌ها آخته در حمله دو بدمست به هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دیوانه آشفته چو زنجیری دید</p></div>
<div class="m2"><p>حلقه‌های خم زلف تو به پیوست به هم</p></div></div>