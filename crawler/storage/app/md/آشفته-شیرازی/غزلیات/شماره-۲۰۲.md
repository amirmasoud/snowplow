---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>غیر سرت بسر و غیر تو در خاطر نیست</p></div>
<div class="m2"><p>نیست ازاهل نظر هر که بتو ناظر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهد غیبی و بی پرده و درعین ظهور</p></div>
<div class="m2"><p>ظاهر این است که این بر همه کس ظاهر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه بینی زبدایت بنهایت برسد</p></div>
<div class="m2"><p>غیر عشق تو کش اول نبدو و آخر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق گل نبود و آن همه لافست و گزاف</p></div>
<div class="m2"><p>بلبل باغ که بر خار جفا صابر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکند فهم سخن گوش کسان ورنه بدهر</p></div>
<div class="m2"><p>نبود سنگ و کلوخی که تو را ذاکر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبض من دید طبیبی و همی گفت و گریست</p></div>
<div class="m2"><p>درد عشق است و فلاطون بدو قادر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نادر آنست که کس زنده رود از میدان</p></div>
<div class="m2"><p>گر جهان کشته آن دست شود نادر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفته بودم که چو غایب شود او شکوه کنم</p></div>
<div class="m2"><p>نیست یک لحظه که در دیده و دل حاضر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بمصاف دو جهان کی طلبد نصرت کس</p></div>
<div class="m2"><p>هر کرا غیر علی در دو جهان ناصر نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مدیح تو زآشفته اگر رفت قصور</p></div>
<div class="m2"><p>یک زبان نیست که در مدحت تو قاصر نیست</p></div></div>