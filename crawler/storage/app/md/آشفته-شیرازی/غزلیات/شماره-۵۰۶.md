---
title: >-
    شمارهٔ ۵۰۶
---
# شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>اسیرعشق شدن عقل را قرار نبود</p></div>
<div class="m2"><p>نظر بمنظر خوبان باختیار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرار داد که من بیقرار او باشم</p></div>
<div class="m2"><p>دریغ و درد که آن عهد برقرار نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم اینکه دلم صید نیم بسمل کیست</p></div>
<div class="m2"><p>بدشت حسن جز آن ترک شهسوار نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زسلسبیل لبت بسکه باده گشت سبیل</p></div>
<div class="m2"><p>حز آن دو نرگس بیمار در خمار نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بناز آمد و دامن فشان زمن بگذشت</p></div>
<div class="m2"><p>مگر که خرمن خاکی بره غبار نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار گل زگل از فیض نوبهار دمید</p></div>
<div class="m2"><p>بغیر لاله در این باغ داغدار نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهار طبع مخالف بشمع عرضه نمود</p></div>
<div class="m2"><p>بمشربش بجز از نار سازگار نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شد که صومعه بربست و میکده بگشود</p></div>
<div class="m2"><p>که این گمان بتو ای دور روزگار نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو زگفتن حق داده باد سر حلاج</p></div>
<div class="m2"><p>سزای کاشف اسرار غیر دار نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر نه خلق نبی بود و ذوالفقار علی</p></div>
<div class="m2"><p>بخلق سر خدا هرگز آشکار نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر نه عفو تو بودی سزای بندگیت</p></div>
<div class="m2"><p>که بود کز عمل خویش شرمسار نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبود کار تو مدح حیدر آشفته</p></div>
<div class="m2"><p>تو را بهر دو جان هیچ افتخار نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نه داغ توام بود زیب پیشانی</p></div>
<div class="m2"><p>مرا بکعبه و بتخانه اعتبار نبود</p></div></div>