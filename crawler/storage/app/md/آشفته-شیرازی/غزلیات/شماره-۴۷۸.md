---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>گر اهل عقل کار به تدبیر بسته‌اند</p></div>
<div class="m2"><p>دیوانگان مدار به تقدیر بسته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کف نهاده ما سر تسلیم پیش دوست</p></div>
<div class="m2"><p>گر عاقلان قرار به تدبیر بسته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما خوانده سر عشق ز رخسار نیکوان</p></div>
<div class="m2"><p>قومی اگر به مصحف و تفسیر بسته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندین اثر که گفته حکیمان در آب رز</p></div>
<div class="m2"><p>تا بر شراب عشق چه تأثیر بسته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمان و کفر سبحه و زنار نیست نیست</p></div>
<div class="m2"><p>بر خود عبث عبادت و تکفیر بسته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون که خضر زنده آب بقا بود</p></div>
<div class="m2"><p>جان‌های عاشقان بدم تیر بسته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمان سحر ساز تو از طره رسا</p></div>
<div class="m2"><p>آشفته را به دام و به زنجیر بسته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را نظر به دست خدا مرتضی بود</p></div>
<div class="m2"><p>مردم دل ار چه بر کرم پیر بسته‌اند</p></div></div>