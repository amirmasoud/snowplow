---
title: >-
    شمارهٔ ۸۹۲
---
# شمارهٔ ۸۹۲

<div class="b" id="bn1"><div class="m1"><p>مبند الفت دلا با ماه رویان</p></div>
<div class="m2"><p>که بی قیدند این زنجیر مویان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکنده هر طرف صیدی و از حرص</p></div>
<div class="m2"><p>بدنبال دگر صیدند پویان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین سودا ندیده جز زیان کس</p></div>
<div class="m2"><p>منه دل را بسودای نکویان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه ناسور است زخمت ایدل من</p></div>
<div class="m2"><p>نه پرهیزی چرا زین مشک بویان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بشویم چشم اگر از اشک زارم</p></div>
<div class="m2"><p>چو نصرانی بعید خارج شویان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجو آشفته مهر از خیل ترکان</p></div>
<div class="m2"><p>چو خو کردی تو با این تندخوبان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بلبل عندلیب طبعم امشب</p></div>
<div class="m2"><p>به مدح مرتضی گردید گویان</p></div></div>