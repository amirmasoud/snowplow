---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>خلقت هر چیز از آب و گل است</p></div>
<div class="m2"><p>عشق را منشاء تقاضای دل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نار نمرود است گلزار خلیل</p></div>
<div class="m2"><p>موج طوفان بهر سالک ساحل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کرا جز عشق باشد قبله گاه</p></div>
<div class="m2"><p>در طریقت آن عبادت باطل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتگان دیگران را خونبهاست</p></div>
<div class="m2"><p>چشم ما فردا بدست قاتل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیخبر از سوختن پروانه نیست</p></div>
<div class="m2"><p>شمع را مسکین بجان مستعجل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برق ما را خانه زاد خرمن است</p></div>
<div class="m2"><p>تا نگوئی کشت ما بیحاصل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چه از زلفت دلم دیوانه شد</p></div>
<div class="m2"><p>گر زهر زنجیر مجنون عاقل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که داغ عشق دارد برجبین</p></div>
<div class="m2"><p>گرچه آشفته است بختش مقبل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان و ایمان را نباشد منزلت</p></div>
<div class="m2"><p>هر که را در کوی جانان منزل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست غافل مست جام عشق باز</p></div>
<div class="m2"><p>هر که زین باده ننوشند غافل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مانده ام من زنده در هجران دوست</p></div>
<div class="m2"><p>جسم بیجان زیستن بس مشگل است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چیست دانی عشق سرکش مرتضی</p></div>
<div class="m2"><p>کاو قضا و هم قدر را عامل است</p></div></div>