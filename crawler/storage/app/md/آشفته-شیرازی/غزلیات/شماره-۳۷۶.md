---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>اگر آن ترک سیه چشم بشیراز آید</p></div>
<div class="m2"><p>بخت برگشته عشاق زدر باز آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که منصور صفت جا بسردار گزید</p></div>
<div class="m2"><p>در میان صف عشاق سرافراز آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا ماه رخی چهره فروزد چون شمع</p></div>
<div class="m2"><p>دل سراسیمه چو پروانه به پرواز آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جنون نیک سرانجام بود چون مجنون</p></div>
<div class="m2"><p>هر که در مکتب عشق تو زآغاز آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشنوی وصف گل الا بچمن از بلبل</p></div>
<div class="m2"><p>گرچه هر مرغ در این فصل نواساز آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهوای گل رویت همه شب مرغ دلم</p></div>
<div class="m2"><p>همره مرغ سحرگاه بآواز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون بسی خورد دل و کرد نهان قصه عشق</p></div>
<div class="m2"><p>می ندانست که چشم ترغماز آید</p></div></div>