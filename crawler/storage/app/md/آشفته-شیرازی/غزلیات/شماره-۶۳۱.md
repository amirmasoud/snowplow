---
title: >-
    شمارهٔ ۶۳۱
---
# شمارهٔ ۶۳۱

<div class="b" id="bn1"><div class="m1"><p>چشم دارم مرا نظارت بخش</p></div>
<div class="m2"><p>معنی از لطف بر عبارت بخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رند و آلوده دامنم ساقی</p></div>
<div class="m2"><p>از می صافیم طهارت بخش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر من ای نوبهار روحانی</p></div>
<div class="m2"><p>چون چمن از دمی خضارت بخش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دین و دنیا بغمزه ای دادم</p></div>
<div class="m2"><p>خیز و زآن لب مرا خسارت بخش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلخکامیم زآن لب شیرین</p></div>
<div class="m2"><p>بوسه ای چند بی مرارت بخش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود ستایند لشکر ترکان</p></div>
<div class="m2"><p>ملک یغما بیک اشارت بخش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نو عروسان فکر بکر مرا</p></div>
<div class="m2"><p>از کرم عصمت و طهارت بخش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نگویم بجز مدیح علی</p></div>
<div class="m2"><p>معنیم باز در عبارت بخش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر پیامت بود بساحت عرش</p></div>
<div class="m2"><p>آه آشفته را سفارت بخش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو خلیلم در آتش نمرود</p></div>
<div class="m2"><p>تو سلامت از این حرارت بخش</p></div></div>