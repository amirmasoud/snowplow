---
title: >-
    شمارهٔ ۷۴۲
---
# شمارهٔ ۷۴۲

<div class="b" id="bn1"><div class="m1"><p>شاهد عید از در آمد شد ز دل اندوه بیم</p></div>
<div class="m2"><p>ساقی گل‌چهره کو مطرب کجایی کو ندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از این دل مرده نتوان بود کز لطف هوا</p></div>
<div class="m2"><p>مردگان را زنده می‌دارد از این جنبش نسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همره ما باش تا بنمایمت دیر مغان</p></div>
<div class="m2"><p>زاهدا اینست رندان را صراط مستقیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌زند در پرده مطرب نکته توحید را</p></div>
<div class="m2"><p>فهم این معنی نخواهد کرد جز ذوق سلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس ساقی را به جام می همی‌بیند بصیر</p></div>
<div class="m2"><p>آنچنان کز می کند حل معما را حکیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارتکاب معصیت را گرچه شد انسان عجول</p></div>
<div class="m2"><p>نیست چندان در بر حلم خداوند حلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌کنم من در گنه اصرار کاندر روز حشر</p></div>
<div class="m2"><p>تا توانم کرد میزانش بر عفو رحیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست شیطان را طمع آمرزش از عفو خدای</p></div>
<div class="m2"><p>رحمت و انعام ایزد بس که شد عام عمیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با وجود بسمله نبود ضرر در هیچ شبی</p></div>
<div class="m2"><p>می بده ساقی به بسم الله الرحمن الرحیم</p></div></div>