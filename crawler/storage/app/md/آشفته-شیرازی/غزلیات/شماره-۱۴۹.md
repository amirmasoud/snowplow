---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>مرا تو حاصل گفتاری از جهان ای دوست</p></div>
<div class="m2"><p>بهای هر نظرت صدهزار جان ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه صعوه جا نکند در دهان افعی و مار</p></div>
<div class="m2"><p>چرا به زلف تو دل کرده آشیان ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آستان که اگر جم بود ندارد فخر</p></div>
<div class="m2"><p>سری که می نه بسودت بر آستان ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مراست قطره خونی نثار مقدم تست</p></div>
<div class="m2"><p>بیا و تیغ بیار و بریز هان ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوم چو عظم رمیم و ولیک همچون نی</p></div>
<div class="m2"><p>نوای عشق تو خیزد ز استخوان ای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه هرکجا که بود گو رود پی چوگان</p></div>
<div class="m2"><p>مرا سری‌ست چرایی تو سرگران ای دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خضر ز چشمه حیوان بقا گرفت تو را</p></div>
<div class="m2"><p>دمیده خضر بر اطراف آن دهان ای دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شوق بال‌فشانی است جان به خاک درت</p></div>
<div class="m2"><p>مرا ز تنگی این خانه وارهان ای دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ندانم که از پریشان شد است آشفته</p></div>
<div class="m2"><p>ولی به زلف تو دارد دلم گمان ای دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عشق معتقدم من که آن ولی خداست</p></div>
<div class="m2"><p>به اعتقاد بمردیم همچنان ای دوست</p></div></div>