---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>فحش شیرین زلعل شکرخاست</p></div>
<div class="m2"><p>زهر از دست نیکوان حلواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زشتی ای عاشق قباحت فهم</p></div>
<div class="m2"><p>کانچه زیبا کند همه زیباست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل باغ خار نشناسد</p></div>
<div class="m2"><p>که همه چشم بر گل رعناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هجوم رقیب در آن کو</p></div>
<div class="m2"><p>چه عجب گر قیامتی برپاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا میپزند حلوائی</p></div>
<div class="m2"><p>از هجوم مگس همین غوغاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوقم اندر کمند عشق افکند</p></div>
<div class="m2"><p>رفت شوق و تعلقم برجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل دیوانه خم زلفش</p></div>
<div class="m2"><p>چشم او رهزن دل داناست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو آئینه ام زصیقل عشق</p></div>
<div class="m2"><p>کاز سراپایم آن صنم پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار رشکم شکسته است بدل</p></div>
<div class="m2"><p>تا تو را جامه زاطلس و دیباست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست بگرفتمش که بوسم لب</p></div>
<div class="m2"><p>گفت هی هی برو مگر یغماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتی آشفته زلف او بگذار</p></div>
<div class="m2"><p>فکر سودائیان همیشه خطاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر بدنیاست چشم اهل معاش</p></div>
<div class="m2"><p>دیده زاهد از پی عقباست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من آشفته زین دو آسوده</p></div>
<div class="m2"><p>چشم بر دست حضرت مولاست</p></div></div>