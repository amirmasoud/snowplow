---
title: >-
    شمارهٔ ۸۹۷
---
# شمارهٔ ۸۹۷

<div class="b" id="bn1"><div class="m1"><p>دعوی عشق میکنم کذب شده دعای من</p></div>
<div class="m2"><p>رفتی و مانده ام بجا وای من و وفای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنگ شده است عیش دل بیگل گلستان دل</p></div>
<div class="m2"><p>رفت بهار و شد خزان تا چه کند خدای من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون جرسم بغلغله طی نشده چه مرحله</p></div>
<div class="m2"><p>باد صبا بقافله خیز و ببر دعای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم دیده خون شوی وز نظرم برون شوی</p></div>
<div class="m2"><p>تا نه کنی به پیش کس فاش تو ماجرای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آه سحرگهی بود محرم ماه خرگهی</p></div>
<div class="m2"><p>نامه دل باو رسان قاصد بادپای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم زنوا فروهلد برقش از درون جهد</p></div>
<div class="m2"><p>گر بشبی در انجمن نی شنود نوای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غم عاشقی زدل خواست گواه مدعی</p></div>
<div class="m2"><p>چهره و اشک و آه من شاهد مدعای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه که خلیل کی کند بابت بتکده چنین</p></div>
<div class="m2"><p>کان بت پارسی کند با دل پارسای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته جز از علی بیم و امید خود نبر</p></div>
<div class="m2"><p>کاز تو بود بهر دو سر خوف من و رجای من</p></div></div>