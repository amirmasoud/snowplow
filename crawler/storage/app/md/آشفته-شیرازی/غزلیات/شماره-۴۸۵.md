---
title: >-
    شمارهٔ ۴۸۵
---
# شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>عاشق آنست که در هجر شکیبا نشود</p></div>
<div class="m2"><p>گر شکیبد بشبی باز بفردا نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو بگردن ننهد سلسله زلف بتان</p></div>
<div class="m2"><p>هر که او خواهد دیوانه و شیدا نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدمش خرقه و دستار بمیخانه گرو</p></div>
<div class="m2"><p>صوفی شهر که میخواست که رسوا نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه اسباب طرب جمله مهیاست مرا</p></div>
<div class="m2"><p>بی تو ای شمع چگل عیش مهیا نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از این فکر خردمندی و دانش دارم</p></div>
<div class="m2"><p>فتنه پشم تو گر رهزن دانا نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواست گل جامه برنگ تو بپوشد در باغ</p></div>
<div class="m2"><p>هر که در کسوت زیبا شد زیبا نشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چمد در چمن و سر بکشد بر افلاک</p></div>
<div class="m2"><p>سرو چون قامت رعنای تو والا نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مده آن گوهر یکدانه بدست غیار</p></div>
<div class="m2"><p>خواهی ار دیده عشاق تو دریا نشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکنی سود دلا در سفر عشق بتان</p></div>
<div class="m2"><p>تا متاع دل و دینت همه یغما نشود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر آشفته رودگر بسر سودایت</p></div>
<div class="m2"><p>محو صرف غمت او را زسویدا نشود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زاغ طوطی نه و روبه نشود شیر ژیان</p></div>
<div class="m2"><p>مدعی چون علی عالی اعلا نشود</p></div></div>