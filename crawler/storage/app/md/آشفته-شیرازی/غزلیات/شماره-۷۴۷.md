---
title: >-
    شمارهٔ ۷۴۷
---
# شمارهٔ ۷۴۷

<div class="b" id="bn1"><div class="m1"><p>مرا چه کار بشکر لبان سیم اندام</p></div>
<div class="m2"><p>که گر دعا کنم آید سزای او دشنام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر زسوز محبت شود چو پروانه</p></div>
<div class="m2"><p>کسی که سوخت سراپای او چو شمع تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملول گشت ملامتگر ار زرندی ما</p></div>
<div class="m2"><p>تو شاد باش و میندیش از ملال ملام</p></div></div>