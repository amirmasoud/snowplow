---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>منم بگرفته دل از وصل دلبر</p></div>
<div class="m2"><p>بود بی دلبرم کوهی به دل بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلیلا بر تو آذر گشت گلزار</p></div>
<div class="m2"><p>شد آذر مه مرا بیدوست آذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی آهم جهد از سینه چون برق</p></div>
<div class="m2"><p>همی سیلم رود از سینه تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا خوش باد طرف باغ و بستان</p></div>
<div class="m2"><p>مرا داغ تواز باغ است خوشتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگفتم در نهان یاری باغیار</p></div>
<div class="m2"><p>نگفتم با رقیبان باشدت سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسم خوردی بآن روی دلا را</p></div>
<div class="m2"><p>قسم خوردی بآن موی معنبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو من بار سفر بستم بر افتاد</p></div>
<div class="m2"><p>از آن راز نهان پرده سراسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بریدی رشته محکمتر از جان</p></div>
<div class="m2"><p>شکستی عهد چون سد سکندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گل با خاربن گشتی هم آغوش</p></div>
<div class="m2"><p>زدی با مدعی در بزم ساغر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه بینی من سگ کوی رضایم</p></div>
<div class="m2"><p>نمی بینی که در طوسم مجاور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نترسیدی جفاکارا زپاداش</p></div>
<div class="m2"><p>نیندیشی ستمکارا زکیفر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که از مژگانزند نشتر بچشمت</p></div>
<div class="m2"><p>نهد بر گردنت از زلف چنبر</p></div></div>