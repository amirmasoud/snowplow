---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>گفتمش وقت تو کردیم دل ویران را</p></div>
<div class="m2"><p>گفت کس جای به ویرانه دهد سلطان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناوکش خواستم از سینه کشم کز سر عجز</p></div>
<div class="m2"><p>دل گرفتش بدو دستی که مکش پیکان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش رخنه کنم در دل سنگین از آه</p></div>
<div class="m2"><p>تیر پرتاب کجا رخنه کند سندان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط و خال و مژه و زلف تو خوش دام رهند</p></div>
<div class="m2"><p>کافران نیک به بستند ره ایمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز خم زلف تو کاو سایه بر آن چهره فکند</p></div>
<div class="m2"><p>سایبان بر مه و خورشید که کرده بان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی آشفته علی جوئی و گوئی از غیر</p></div>
<div class="m2"><p>هر که واجب طلبد نفی کند امکان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیخ پیمانه شکن رفت خدا را مددی</p></div>
<div class="m2"><p>که به پیمانه می تازه کنم ایمان را</p></div></div>