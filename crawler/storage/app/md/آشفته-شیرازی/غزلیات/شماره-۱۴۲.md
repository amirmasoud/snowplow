---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>عندلیبا در چمن آوازه آواز تست</p></div>
<div class="m2"><p>شورها در پرده عشاق باز از ساز تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نشاط نغمه تو غنچه خندد هر سحر</p></div>
<div class="m2"><p>گوش را گل پهن کرده صبح بر آواز تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به نوروز همایون راست برداری نوا</p></div>
<div class="m2"><p>ترک آذربایجان را تکیه بر شهناز تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این نیاز بلبل شیدا به بانگ زیر و بم</p></div>
<div class="m2"><p>ای گل نوخیز در گلشن برای ناز تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه را چون چنگل شهباز کردی بهر صید</p></div>
<div class="m2"><p>صعوه مسکین اسیر چنگل شهباز تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن تو ای گل قدیم و عشق بلبل حادث است</p></div>
<div class="m2"><p>هرکجا خیمه زنی او لاجرم انباز تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کنی دارش ز خار آماده حق بر دست تست</p></div>
<div class="m2"><p>کز چه افشا کرد سِرَّت چون امین راز تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لیک گر منصوروَش سِرّ اناالحق گفت فاش</p></div>
<div class="m2"><p>گر کنی بر دار یا سایش که سرافراز تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باری ای گل تو به حسن پنج‌روز خود مناز</p></div>
<div class="m2"><p>نازم آن گل را که بویش مایه ابراز تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بود بر عندلیبت فخر آشفته رواست</p></div>
<div class="m2"><p>کی گل گلزار همچون گلبن طناز تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن گل باقی که اندر گلشن وحدت شکفت</p></div>
<div class="m2"><p>عشق او شورافکن طبع سخن‌پرداز تست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نگارین کرده‌ای صفحه به وصف عارضش</p></div>
<div class="m2"><p>مانی چین بنده پیش کلک افسون‌ساز تست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای گل باغ ولایت ای بهار جاودان</p></div>
<div class="m2"><p>بی‌تفاوت در نظر انجام با آغاز تست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ار پرش سوزی بر آتش ارزد ای شمع ازل</p></div>
<div class="m2"><p>زان که گر پروانه را پر باشد از پرواز تست</p></div></div>