---
title: >-
    شمارهٔ ۸۱۹
---
# شمارهٔ ۸۱۹

<div class="b" id="bn1"><div class="m1"><p>زتو چون خبر بگیرم که زخود خبر ندارم</p></div>
<div class="m2"><p>بکه بنگرم که جز تو بکسی نظر ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشب فراق چون شمع که بسوزد اشتیاقم</p></div>
<div class="m2"><p>چکنم بشام وصلت که زخود خبر ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از این شکر دهانان همه عمر صبر کردم</p></div>
<div class="m2"><p>زدرخت صبر زین بس طمع شکر ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ثمر وفا و مهر است که بباغ عشق آن گل</p></div>
<div class="m2"><p>منم آن نهال بی بر که جز این ثمر ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه جای رانده گشتم چه زکعبه چه خرابات</p></div>
<div class="m2"><p>تو از این درم چه رانی که در دگر ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهوای پرفشانی بقفس زجای جستم</p></div>
<div class="m2"><p>خبرم نبود از خویش که بال و پر ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه تن شدم چو آتش زشرار عشق اما</p></div>
<div class="m2"><p>بتو سنگدل خدا را چکنم اثر ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم و دو دیده دل بمصاف ترک چشمت</p></div>
<div class="m2"><p>زبرای تیر مژگان بجز این سپر ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو بمصر حسن یوسف من و عشق پیر کنعان</p></div>
<div class="m2"><p>نتوانم اینکه گویم که غم پسر ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به علی بریم آشفته شکایت از اعادی</p></div>
<div class="m2"><p>که جز او بهر دو عالم شه دادگر ندارم</p></div></div>