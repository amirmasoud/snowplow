---
title: >-
    شمارهٔ ۹۴۲
---
# شمارهٔ ۹۴۲

<div class="b" id="bn1"><div class="m1"><p>بلبل خوش نوا بکش نغمه زصوت خار کن</p></div>
<div class="m2"><p>گل چو صلای وصل زد نوبت خوشدلی بزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطرب بزم عاشقان پرده عشق ساز کن</p></div>
<div class="m2"><p>ساقی بزم می بده ریشه غم زدل بکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهد پارسی بچم مست شو و قدح بده</p></div>
<div class="m2"><p>نقل و میم بیارهان زآن لب لعل و زآن دهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده دلی چو زاهدان چند زغم فسرده ای</p></div>
<div class="m2"><p>راح مسیح دم بکش زنده شو و بدر کفن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راح سبک بیارهی رطل گران بگیر هان</p></div>
<div class="m2"><p>تا همه جان جان شوی چند کشی تو بار تن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه بده تو پی زپی تلخی هجر تا بکی</p></div>
<div class="m2"><p>زآنکه بکنج آن لبان شهد نهفتی و لبن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیز بیا بسر مرا تنگ بکش ببر مرا</p></div>
<div class="m2"><p>تا که دو تن یکی شود هر دو درون پیرهن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من زسماع چنگ و نی رقص کنان چو صوفیان</p></div>
<div class="m2"><p>تا که برقص آورم شاهد و شمع انجمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از در آشتی درآ عهد مؤالفت بپا</p></div>
<div class="m2"><p>دولت حسن باشدت خلق نکو کن و حسن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته قصه ای بگو آن خم زلف مشکبو</p></div>
<div class="m2"><p>تا که زگفته ات برد نافه صبا سوی ختن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدح شه جهان بگو زآن شه راستان بگو</p></div>
<div class="m2"><p>مهدی صاحب الزمان آنشه عصر و الزمن</p></div></div>