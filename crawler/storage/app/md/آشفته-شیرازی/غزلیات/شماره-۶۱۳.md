---
title: >-
    شمارهٔ ۶۱۳
---
# شمارهٔ ۶۱۳

<div class="b" id="bn1"><div class="m1"><p>چشم مهجور کی بود خوابش</p></div>
<div class="m2"><p>که بر آبست صبر و پایابش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاش لله اگر بیارامد</p></div>
<div class="m2"><p>آنکه کشتی شکسته سیلابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبودش اشتیاق آب بسر</p></div>
<div class="m2"><p>ماهئی کاوفتد بقلابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دارد هوای صحبت دوست</p></div>
<div class="m2"><p>گو ببر جورها زاصحابش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طالب بارگاه سلطانی</p></div>
<div class="m2"><p>نازو منت کشد زبوابش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردمندی که شد زعشق علیل</p></div>
<div class="m2"><p>نیست جز بوسه تو جلابش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که مستسقی است از تف هجر</p></div>
<div class="m2"><p>نکند غیر وصل سیرابش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل من با رخت نمی تابد</p></div>
<div class="m2"><p>نقص باشد کتان زمهتابش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وه چه بازی بود محبت را</p></div>
<div class="m2"><p>که بشش در درند احبابش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک شب آشفته و حکایت زلف</p></div>
<div class="m2"><p>مختصر کن مده تو اطنابش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشته عشق را دیت بمثل</p></div>
<div class="m2"><p>گوسفند است و تیغ قصابش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر کنی غوص بحر وحدت را</p></div>
<div class="m2"><p>جز علی نیست در نایابش</p></div></div>