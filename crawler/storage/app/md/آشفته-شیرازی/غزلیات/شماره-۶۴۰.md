---
title: >-
    شمارهٔ ۶۴۰
---
# شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>صد گل شکفت صبحدم از بوستان عشق</p></div>
<div class="m2"><p>جز در درون لاله ندیدم نشان عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغان باغ گر همه دستانسرا شوند</p></div>
<div class="m2"><p>از عندلیب گل شنود داستان عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهراب وار بسمل بازوی رستمست</p></div>
<div class="m2"><p>رستم اگر که تیر خورد از کمان عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید که دم زرتبه عین الیقین زند</p></div>
<div class="m2"><p>در آن سری که کرده سرایت گمان عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روئینه تن بود به بر خنجر اجل</p></div>
<div class="m2"><p>آن دل که از ازل بود اندر ضمان عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشاق راست زهر بلا باده زلال</p></div>
<div class="m2"><p>از لخت دل کباب خورد میهمان عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی راه برد خضر بسر چشمه حیات</p></div>
<div class="m2"><p>نایافته هدایت از رهروان عشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشتی نوح غرقه بحر فنا شدی</p></div>
<div class="m2"><p>گر ناخدا در او نشدی بادبان عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کیمیای اهل صناعت ززر بود</p></div>
<div class="m2"><p>اکسیر ماست خاک در آستان عشق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یعقوب شد زقافله مصر دیده ور</p></div>
<div class="m2"><p>کحل من است گرد ره کاروان عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون سرو تا ابد بود ایمن زباد دی</p></div>
<div class="m2"><p>در هر چمن که پای گذارد خزان عشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این خرمی و تازگی نوبهار حسن</p></div>
<div class="m2"><p>باشد زحسن تربیت باغبان عشق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیسی شود ببزم محبت مریض شوق</p></div>
<div class="m2"><p>موسی شود بطور سعادت شبان عشق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایمن بود زحادثه دهر تا ابد</p></div>
<div class="m2"><p>هر کس که جای کرده بدار الامان عشق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوزم چو شمع و قصه وقت بیان کنم</p></div>
<div class="m2"><p>آشفته آتشین بود آری زبان عشق</p></div></div>