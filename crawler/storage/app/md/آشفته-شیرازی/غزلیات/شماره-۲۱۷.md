---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>کاش پاینده شدی وصل تو چون هجرانت</p></div>
<div class="m2"><p>کاش نایاب شدی درد تو چون درمانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ندیدند از آن درد نصیبی اغیار</p></div>
<div class="m2"><p>تا در آن وصل بماندند بسی یارانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر از ناله خود بلبل شیدا مطلب</p></div>
<div class="m2"><p>شرط آنست که گل گوش کند افغانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه پیوسته بهم تیر نظر میترسم</p></div>
<div class="m2"><p>زین میان غیر بناحق بخورد پیکانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه خالی کن از این وسوسه شیطانی</p></div>
<div class="m2"><p>کازیمن بود که بیارد نفس رحمانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی بمرآت دلت پرتو واجب افتد</p></div>
<div class="m2"><p>تا که در نفس بود کش مکش اسکانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگدائی در میکده خوش باش دلا</p></div>
<div class="m2"><p>گو براند زدر خود زستم سلطانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق همخابه بود نیست غم از شحنه عقل</p></div>
<div class="m2"><p>نوح همراه بود نیست غم از طوفانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون خلیلست گرت ریشه خلت در گل</p></div>
<div class="m2"><p>نار نمرود شود باغ گل و ریحانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شنیدند که زندان بودت حلقه زلف</p></div>
<div class="m2"><p>یوسف مصر بخواهد بدعا زندانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منم آشفته آلوده بعصیان دامان</p></div>
<div class="m2"><p>عملم نیست مران دست من و دامانت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بولای تو که جز حب توام نیست عمل</p></div>
<div class="m2"><p>گر تو گوئی ببرد بار مرا سلمانت</p></div></div>