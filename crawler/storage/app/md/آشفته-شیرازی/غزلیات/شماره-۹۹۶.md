---
title: >-
    شمارهٔ ۹۹۶
---
# شمارهٔ ۹۹۶

<div class="b" id="bn1"><div class="m1"><p>به زنجیر جفایی گر نشد بخت دژم بسته</p></div>
<div class="m2"><p>سگ لیلی به مجنون از چه رو راه حشم بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا حسن ار کرشمه بسته در زلف سیاه او</p></div>
<div class="m2"><p>گدایی را به زنجیری آمیزی محتشم بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراری گفتم از کویش کنم بهر قرار دل</p></div>
<div class="m2"><p>مرا راه گریز آن گیسوی پرپیچ و خم بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره دل‌های مسکین می‌زند طرّار جادویش</p></div>
<div class="m2"><p>نمی‌بینی که در یک چین مو صد دل به هم بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلیخاطلعتی در چاهِ غبغب کرده زندانم</p></div>
<div class="m2"><p>که صد چون یوسف مصری به زنجیر ستم بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه تنها من دل و دین کرده‌ام کابین به جام می</p></div>
<div class="m2"><p>که عقد دختر رَز را به جان زین پیش خم بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنازم غمزهٔ تُرکت که از مژگان و از ابرو</p></div>
<div class="m2"><p>عرب را خیل غارت کرده و ره بر عجم بسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگو آشفته طوفان را نشاید بست راه از حُسن</p></div>
<div class="m2"><p>نمی‌بینی که مژگان راه بر چشم چو یم بسته</p></div></div>