---
title: >-
    شمارهٔ ۱۲۲۴
---
# شمارهٔ ۱۲۲۴

<div class="b" id="bn1"><div class="m1"><p>در پرده قانون چند بی فایده آویزی</p></div>
<div class="m2"><p>مطرب ره عشقی زن تا شور برانگیزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مستی عشقت هست مستان می انگوری</p></div>
<div class="m2"><p>چون باده صافی هست با درد چه آمیزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقلت بمثل شیر است عشقت بیقین آتش</p></div>
<div class="m2"><p>ای شیر رسید آتش وقتست که بگریزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مجلس میخوران صوفی چو مقیمستی</p></div>
<div class="m2"><p>از توبه و از پرهیز آن به که بپرهیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جلوه بت کشمیر در جام می خلار</p></div>
<div class="m2"><p>تا از سر عقل و دین یکباره تو برخیزی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عقل مکن پنجه با عشق قوی بازو</p></div>
<div class="m2"><p>ای صعوه تو با شاهین بیهوده چه بستیزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته اسیرستی در دام هوای نفس</p></div>
<div class="m2"><p>در سایه شیر حق آن به که تو بگریزی</p></div></div>