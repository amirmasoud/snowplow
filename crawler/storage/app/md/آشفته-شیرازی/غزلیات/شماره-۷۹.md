---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>فکند آن پنجه داور گهی از راست گاهی چپ</p></div>
<div class="m2"><p>ز مرحب سر ز خیبر در گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علی یک تن در آن غوغا ولی در عرصه هیجا</p></div>
<div class="m2"><p>هزیمت کرد از او لشکر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهیل برق تک رخش و بدخشی ذوالفقار او</p></div>
<div class="m2"><p>یکی برق و یکی تندر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو پیکر صارم تیزش به قلب لشکر کافر</p></div>
<div class="m2"><p>نماید هر یکی نشتر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرانگشتان معجز را برای بندگی خور را</p></div>
<div class="m2"><p>ز مغرب برد در خاور گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روم و چین و بحر و بر اگر خاقان و گر قیصر</p></div>
<div class="m2"><p>از آن سر برد و زین افسر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسام برق فام و نیزه اژدر خصال او</p></div>
<div class="m2"><p>بلای جوشن و مغفر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن نیلوفری خرگه چه باشد تیر و ناهیدش</p></div>
<div class="m2"><p>دبیرستی و خنیاگر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قسیم دوزخ و جنت برای کافر و مؤمن</p></div>
<div class="m2"><p>شفیع عرصه محشر گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک را گر دو قطب آمد منجم دیده را بگشا</p></div>
<div class="m2"><p>بهر قطبی است او محور گهی از راست گاهی چپ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شها آشفته‌ات تنها میان یک جهان اعدا</p></div>
<div class="m2"><p>بیفکن سایه‌اش بر سر گهی از راست گاهی چپ</p></div></div>