---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>کرده در غنچه نهان تنگ شکر کاین دهنست</p></div>
<div class="m2"><p>ریزه قند زلب ریزد و گوید سخنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در اسلام بهند و نه حلال است بهشت</p></div>
<div class="m2"><p>از چه بر عارضت آنخال سیه را وطنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهره شهر شد از عشق تو گر دل چه عجب</p></div>
<div class="m2"><p>عشق شیرین سبب شهره گی کوهکنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف از چاه برون آمد و بر شد از ماه</p></div>
<div class="m2"><p>باز یعقوب ستمدیده ببیت الحزنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کاروان خطش از دست بگیرد شاید</p></div>
<div class="m2"><p>یوسف دل که گرفتار بچاه ذقنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم قضا از خطر تیز نظر در حذر است</p></div>
<div class="m2"><p>فتنه مفتون تو ای زلف شکن در شکنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم آشفته و شیدائی و سودائی عشق</p></div>
<div class="m2"><p>تا که سر سرو زلفت به سویدای منست</p></div></div>