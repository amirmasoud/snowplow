---
title: >-
    شمارهٔ ۱۰۰۹
---
# شمارهٔ ۱۰۰۹

<div class="b" id="bn1"><div class="m1"><p>شب گذشته چو مه زد علم بر این خرگاه</p></div>
<div class="m2"><p>درآمد از درم آن ماه خرگهی ناگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ببرگ سمن بسته سمبل بویا</p></div>
<div class="m2"><p>یکی بمشک ختن بر نهفته پیکر ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی بسرو بپوشیده پرنیان حله</p></div>
<div class="m2"><p>یکی زمشک بمه بسته طیلسان سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زتیر بسته یکی جعبه بر چشم سیه</p></div>
<div class="m2"><p>زنیزه داده بسی زخمها بترک نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار سلسله دل پای بند زلفینش</p></div>
<div class="m2"><p>هزار قافله جان خاک گشته بر سر راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی دهان که نگنجد بتنگی اندر وهم</p></div>
<div class="m2"><p>یکی دهان که نیاید بوصف در افواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کمند زلف رسا دام راه عقل و خرد</p></div>
<div class="m2"><p>نگاه چشم سیه فتنه دل آگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو چشم جادوی عابد فریب و گیسویش</p></div>
<div class="m2"><p>بسحر جادوی بابل فکنده اندر چاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنخ کدام یکی چاه سرنگون از سیم</p></div>
<div class="m2"><p>چه چاه یوسف مصری برو ببرده پناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیده ساغری و گشته مست و جام بدست</p></div>
<div class="m2"><p>شکسته بر سر سرو سهی زنار کلاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من و ندیم شبانه بپایش افتادیم</p></div>
<div class="m2"><p>چو زلف بر قدم او زعجز سوده جباه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفت وقت تواضع نماند جای نشست</p></div>
<div class="m2"><p>زجای خیز و بخوان چامه بمدحت شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهی که جام بکف ایستاده بر سر حوض</p></div>
<div class="m2"><p>بقول یشرب عینا بها عبادالله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بده به تشنه لب آشفته جامی ایساقی</p></div>
<div class="m2"><p>که از دو کون بکوی تو جسته است پناه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امام مشرق و مغرب امیر کل امیر</p></div>
<div class="m2"><p>شفیع عرصه محشر علی ولی الله</p></div></div>