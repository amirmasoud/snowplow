---
title: >-
    شمارهٔ ۵۶۸
---
# شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>کی شنیدستی هلالی خیزد از بدر منیر</p></div>
<div class="m2"><p>یا در آتش تازه و تر مانده عنبر یا عبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرخطر راهیست این وادی کجائی رهنما</p></div>
<div class="m2"><p>سر قدم کرم زشوقت پا ندارم دستگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رند از میخانه ام رانده است و شیخ از خانقه</p></div>
<div class="m2"><p>نه جوان را میل صحبت با من افتاده نه پیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوق کعبه هر کرا در سر در افتد لاجرم</p></div>
<div class="m2"><p>زیر پهلو سنگ خارا نرمتر شد از حریر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناشکیبا تشنه کی از خواب بتواند شکیب</p></div>
<div class="m2"><p>جسم از جانست لابد در حقیقت ناگزیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمهریرم گر بود جا دوزخش سازم زآه</p></div>
<div class="m2"><p>ور بدوزخ بگذرم از آه گردد زمهریر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر سم مرکبت این صید بسمل را بکش</p></div>
<div class="m2"><p>چون نیم آنمرغ لایق کم نپنداری به تیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشته جان مرا پیوند با جانان بود</p></div>
<div class="m2"><p>کی زباران حوادث شویدم نقش ضمیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض تو عامست زآشفته نظر باری مگیر</p></div>
<div class="m2"><p>گرچه خار و خس بود بارد بر او ابر مطیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیستم چون طاعتی جرمم بهمت در گذر</p></div>
<div class="m2"><p>عذر میخواهم زرحمت پوزشم را در پذیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا علی گر صاحب خانه زسگ دارد نفور</p></div>
<div class="m2"><p>سود کی دارد زسگ گر لابه دارد یا نفیر</p></div></div>