---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>هر کرا نقش بجانست کی از دل برود</p></div>
<div class="m2"><p>و گرش جان برود نقش تو مشگل برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح مجنون چو جرس پیش رود لیلی را</p></div>
<div class="m2"><p>گراز این دشت بصد مرحله محمل برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو چمان گر بچمی با گل رخساره بباغ</p></div>
<div class="m2"><p>پای سرو و گل از این خجلت در گل برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میروی عکس تو در دیده ما میماند</p></div>
<div class="m2"><p>همچو آئینه که مهرش زمقابل برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر عشق است نباشد بجز از طوفانش</p></div>
<div class="m2"><p>عجب از نوح از این بحر بساحل برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دادخواهان همه نالند زقاتل در حشر</p></div>
<div class="m2"><p>دادخواهی من آنست که قاتل برود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بیند نظری بر تو برد بهره زعمر</p></div>
<div class="m2"><p>آه از آن دیده که از کوی تو غافل برود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که آئینه بود نقش تو در او باقیست</p></div>
<div class="m2"><p>پیش آئینه چو آن طرفه شمایل برود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده گر بر تو فتد وقف تو ماند نظرش</p></div>
<div class="m2"><p>رستم ار آید در کوی تو بیدل برود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاهدا باده کش و نکته توحید بگوی</p></div>
<div class="m2"><p>ترسمت عمر گرانمایه بباطل برود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه غم آشفته گر از شور تو مجنون گردید</p></div>
<div class="m2"><p>هر که دیوانه شد از عشق تو عاقل برود</p></div></div>