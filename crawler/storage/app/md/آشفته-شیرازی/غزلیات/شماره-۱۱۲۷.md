---
title: >-
    شمارهٔ ۱۱۲۷
---
# شمارهٔ ۱۱۲۷

<div class="b" id="bn1"><div class="m1"><p>صبح عید است شبی کز در من بازآئی</p></div>
<div class="m2"><p>زآنکه شب را نبود صبح باین زیبائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود حاجت صبحم بشبان یلدا</p></div>
<div class="m2"><p>گر بناگوش بزیر خم مو بنمائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اوفتادت بقضا حلقه آن زلف رسا</p></div>
<div class="m2"><p>میزند با قد تو لاف زهم بالائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونم ار هیچ بود لیک کند ناخن رنگ</p></div>
<div class="m2"><p>تا بکی پنجه بخون دگران آلائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیکر تست نه مردم که بچشمان بیند</p></div>
<div class="m2"><p>میدهد عکس تو در دیده من بینائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امتحانم چه کنی خسته تیر نظرم</p></div>
<div class="m2"><p>تابکی از نظرم میروی و میآئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بعد از این در برخ مدعیان باید بست</p></div>
<div class="m2"><p>شایدت سیر توان دید در آن تنهائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان بسته برویم در رحمت امشب</p></div>
<div class="m2"><p>وقت آن شد که در میکده را بگشائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتی افسوس از آن سر که نشد خاک درم</p></div>
<div class="m2"><p>ما ستاده بغرامت تو چه میفرمائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن رخسار تو زآرایش کس مستغنی است</p></div>
<div class="m2"><p>از خط و خال چه حاجت که جمال آرائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر هوائی بجز از مهر علی هست تو را</p></div>
<div class="m2"><p>بهل آشفته عبث باد چه میپیمائی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جهان کس نشنیدم که بود همتایش</p></div>
<div class="m2"><p>کوفت بر بام حرم کوس چو بر یکتائی</p></div></div>