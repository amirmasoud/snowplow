---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>تا چند نافه ریزی از آنزلف مشک بیز</p></div>
<div class="m2"><p>بیمار شد دو چشم تو از بوی مشک خیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زلف و خال و عود سپندی برخ بسوز</p></div>
<div class="m2"><p>در جام و کام نقل میم زآن لبان بریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق تو آمد و دو جهانش بلا زپی</p></div>
<div class="m2"><p>داماد دهر دیده عروسی باین جهیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی کز آب بحر شود نار مشتعل</p></div>
<div class="m2"><p>بر آتش دل آب توای چشم تر مریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیبای عشق را که در این کارگاه ساخت</p></div>
<div class="m2"><p>کش تار و پود بافته از تیر و تیغ تیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برخاستی زجا و خرامان شدی بناز</p></div>
<div class="m2"><p>غوغا بود بشهر که برخاست رستخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه علاقان زخصم بپرهیز اندرند</p></div>
<div class="m2"><p>ایدل زنفس خویش بکن اندک احتریز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر علی بکعبه دل گر نباشدت</p></div>
<div class="m2"><p>خواهی بسومنات رو و خواه در حجیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته در پناه تو خواهد گریختن</p></div>
<div class="m2"><p>در روز رستخیز که نبود ره گریز</p></div></div>