---
title: >-
    شمارهٔ ۱۱۶۵
---
# شمارهٔ ۱۱۶۵

<div class="b" id="bn1"><div class="m1"><p>تو را که زیر لبان روح یک جهان داری</p></div>
<div class="m2"><p>دو چشم خود ز چه بیمار و ناتوان داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بلبلی که تو صد باغ وقف نغمه توست</p></div>
<div class="m2"><p>چه نوگلی تو که یک شهر باغبان داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز جور غیر شکایت مکن که یارت هست</p></div>
<div class="m2"><p>به جور خار بساز ای که بوستان داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکیم گو نزند دم دگر ز جوهر فرد</p></div>
<div class="m2"><p>تو را سزاست که معنیش در میان داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو با خیال میانش دلا خوشی همه شب</p></div>
<div class="m2"><p>به هیچ شب همه شب دست در میان داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی بکشت یکی زنده گشت و دعوی کرد</p></div>
<div class="m2"><p>بکن تو دعوی معجز که این و آن داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه مرغی و ز کدام آشیانه‌ای ای عشق ‏</p></div>
<div class="m2"><p>که هر کجا که دلی هست آشیان داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبان تو عجب آتش فشاند شد آشفته</p></div>
<div class="m2"><p>چه آتشیست که اندر میان جان داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آستان علی سر مپیچ ای درویش</p></div>
<div class="m2"><p>روی به خانه اگر ره بر آستان داری</p></div></div>