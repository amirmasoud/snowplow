---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>کسی سر از قدم یار برنمی‌گیرد</p></div>
<div class="m2"><p>که جان دهد دل از این کار برنمی‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو برق عالم اسراری و عجب که ز تو</p></div>
<div class="m2"><p>شرر به پرده اسرار برنمی‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عبث به چشم تو گفتم که خون خلق مخور</p></div>
<div class="m2"><p>که مست عادت هشیار برنمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تن چو بار گرانست جان بگو جانان</p></div>
<div class="m2"><p>چرا ز دوش من این بار برنمی‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزن تو تیغ به سر مدعی که من سپرم</p></div>
<div class="m2"><p>به تیغ یار دل از یار برنمی‌گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا که سوز درونست همچو شمع به دل</p></div>
<div class="m2"><p>عجب که شعله به یک بار برنمی‌گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگو که کشته تیغ تو جان به سختی داد</p></div>
<div class="m2"><p>که دل ز لذت دیدار برنمی‌گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به دوزخ آشفته را بسوزانی</p></div>
<div class="m2"><p>که دل ز حب ده و چار برنمی‌گیرد</p></div></div>