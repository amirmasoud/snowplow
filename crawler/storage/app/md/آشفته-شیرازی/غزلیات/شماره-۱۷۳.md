---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>بیداری خیال تو از خواب خوشترست</p></div>
<div class="m2"><p>زهر حبیب از شکر ناب خوشترست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب تشنگان بادیه شوق را زتیغ</p></div>
<div class="m2"><p>آبی بده که کشته سیراب خوشترست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل گرد طوف کعبه زلف تو از شعاع</p></div>
<div class="m2"><p>بر شبروان بادیه مهتاب خوشترست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم دو دیده باز کن از خواب بامداد</p></div>
<div class="m2"><p>گفتا خموش فتنه در خواب خوشترست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستر کنم زنشتر اندر شب فراق</p></div>
<div class="m2"><p>زیرا که خار بی تو زسنجاب خوشترست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه ماهیم سمندر عشقم خدای را</p></div>
<div class="m2"><p>گو آتشم بیار که از آب خوشترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در صحبت رقیب چه ذوقت ززندگیست</p></div>
<div class="m2"><p>جان باختن بمقدم احباب خوشترست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنویس نسخه زان لب شیرین مرا طبیب</p></div>
<div class="m2"><p>کان گلشکر بطبع زجلاب خوشترست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر دعوتم کنند اعادی سوی بهشت</p></div>
<div class="m2"><p>درد و زخم بصحبت احباب خوشترست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته در حدیث زهر باب درببند</p></div>
<div class="m2"><p>وصف علی و آل زهر باب خوشترست</p></div></div>