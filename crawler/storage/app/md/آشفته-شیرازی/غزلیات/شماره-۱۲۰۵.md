---
title: >-
    شمارهٔ ۱۲۰۵
---
# شمارهٔ ۱۲۰۵

<div class="b" id="bn1"><div class="m1"><p>متحیریم یا رب بکجاست خضر راهی</p></div>
<div class="m2"><p>که چو کور در شب تار فتاده ایم بچاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه کمست باغبانا زتو و زبوستانت</p></div>
<div class="m2"><p>که بپای گلبن تو خورد آب هم گیاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه خلق در گمانند زنقطه دهانت</p></div>
<div class="m2"><p>سخنی بگو خدا را پی رفع اشتباهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عمر بر جمالت نگران و منتظر من</p></div>
<div class="m2"><p>نظری بحالتم کن بنوازم از نگاهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژه ات چو جنبشی کرد دل من طپید و گفتا</p></div>
<div class="m2"><p>بخرابی حصاری شده متفق سپاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به که داوری توان برد و جز از دعا چه گوید</p></div>
<div class="m2"><p>بگدای ره نشینی چو ستم رسد زشاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه کشتگان چو بینند جمال تو بمحشر</p></div>
<div class="m2"><p>نزند زخون بها دم بحساب دادخواهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشنیدم آفتابی بجز از تو سرو گلروی</p></div>
<div class="m2"><p>که برآرد از گریبان بشبان تیره ماهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برسان بمستحقان تو زکوة حسن رویت</p></div>
<div class="m2"><p>که مباد خرمنت را شرری رسد زآهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زچه رانیم خدا را تو زمیکده که خامی</p></div>
<div class="m2"><p>که جز این سرا مرا نیست دگر گریزگاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بجز از نجف تو آشفته مبر پناه هرگز</p></div>
<div class="m2"><p>که بهر دو کون نبود بجز از علی پناهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر کوه رحمت اوست چو کاه جرمت ایدل</p></div>
<div class="m2"><p>بعبث کسی نسنجد بر کوه برگ کاهی</p></div></div>