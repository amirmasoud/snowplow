---
title: >-
    شمارهٔ ۴۵۸
---
# شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>زاهدان را چو بزلف تو سر و کار افتد</p></div>
<div class="m2"><p>کار با سبحه و زنار به پیکار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیز و در کوی مغانش بده ای شیخ بمی</p></div>
<div class="m2"><p>چند سجاده و تسبیح تو بی کار افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز ملامت ثمری ایدل شیدا نبری</p></div>
<div class="m2"><p>سرو کار تو چو با مردم هوشیار افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه زنار بود حلقه آن زلف نژند</p></div>
<div class="m2"><p>کاش بر حلق من آن حلقه زنار افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهدم رقص کنان جلوه کند چون در بزم</p></div>
<div class="m2"><p>زاهدان را همگی کیک بشلوار افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست ممکن که دگر باز برویت بیند</p></div>
<div class="m2"><p>نظر هر که برخسار تو یک بار افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منع چشمت نتوان کرد که خونم نخورند</p></div>
<div class="m2"><p>ترک چون مست شود لابد خونخوار افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدر آن صدمه که من میخورم از دل دانی</p></div>
<div class="m2"><p>گر سر و کار تو یکروز به بیمار افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سر کوی خود آشفته چه رانی نرود</p></div>
<div class="m2"><p>خار در ساحت گلزار بناچار افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از غم بی عملی شکوه مکن ای درویش</p></div>
<div class="m2"><p>در صف حشر بحیدر چو سر و کار افتد</p></div></div>