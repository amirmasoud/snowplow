---
title: >-
    شمارهٔ ۷۸۶
---
# شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>غباری گردم و روزی بدامان تو بنشینم</p></div>
<div class="m2"><p>شوم آئینه و بر کام دل روی ترا بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوم ابریشم و در جامه ات خود را کنم پنهان</p></div>
<div class="m2"><p>که افتد اتفاق بوسه بر آن دست سیمینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن فرهادم و یکدل هوس پیشه نیم خسرو</p></div>
<div class="m2"><p>نه سودای شکر دارم که خوش در خواب شیرینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگو بی دین بود عاشق نیم این طعن را لایق</p></div>
<div class="m2"><p>که شد محراب و میخانه بود عشق بتان دینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مناز ای آسمان بر ماه و پروینت که بی آن مه</p></div>
<div class="m2"><p>به هر شب اوفتد از چشم تر صد عقد پروینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری از تن برآوردم چو گو چندین قفا خوردم</p></div>
<div class="m2"><p>کنون بی سرهمی آیم ببخش ایشه که مسکینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب رحلت ببالین گر کسی را شمع افروزند</p></div>
<div class="m2"><p>به آن امید میمیرم که باشی شمع بالینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه بلبلم گل را بهنگام نواخوانی</p></div>
<div class="m2"><p>بتیمارم چو ناید گل چو بوتیمار بنشینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبادا تا گلی بینم بگلزار جهان جز تو</p></div>
<div class="m2"><p>بگرد چشم هر شب از مژه خاشاک میچینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سفتم گوهر مدح علی با خامه آشفته</p></div>
<div class="m2"><p>سزد گر میر بزم امشب گشاید لب به تحسینم</p></div></div>