---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>ای دل بسینه میطپی این اضطراب چیست</p></div>
<div class="m2"><p>جانت بلب رسیده دگر این شتاب چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جام می بکف بود و لعل او لب</p></div>
<div class="m2"><p>جان شادکام شد بدل این اضطراب چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق بدوست دیده حق بین چو برگشود</p></div>
<div class="m2"><p>غیری بجا نماند دگر اجتناب چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم زقدر نعمت اگر بیخبر بود</p></div>
<div class="m2"><p>ماهی چو اوفتد بزمین داند آب چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان را حجاب چهره جانان چه میکنی</p></div>
<div class="m2"><p>بردار پرده را زمیان این حجاب چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سرخوشند مدعیان ساقی از شراب</p></div>
<div class="m2"><p>من مستم آنچنان که ندانم شراب چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای چشم فتنه خیز کز افسون ساحری</p></div>
<div class="m2"><p>مردم بدور تو نشناسند خواب چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیز و بیا بطوف خرابات زاهدا</p></div>
<div class="m2"><p>تا گویمت که حاصل دیر خواب چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطرب بزن نای حسینی به پرده راست</p></div>
<div class="m2"><p>تا بشنوی که فتوی و چنگ و رباب چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با حب مرتضی روی آشفته چون بحشر</p></div>
<div class="m2"><p>دانی در آن مصاف گناه و صواب چیست</p></div></div>