---
title: >-
    شمارهٔ ۷۰۴
---
# شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>گردر میکده شهر به بستند چه غم</p></div>
<div class="m2"><p>سایه تاک مباد از سر میخواران کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن ای پیر خرابات زمن جام دریغ</p></div>
<div class="m2"><p>که بیک جام تو مستغنیم از کشور جم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بخلوتگه انسیم بکوری رقیب</p></div>
<div class="m2"><p>پاسبان گو بکند در برخم مستحکم ‏</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر گریان شده چون دیده مجنون در دشت</p></div>
<div class="m2"><p>تا مگر لیلی گل چهره نماید زحشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نای بلبل بنوا آمده از مقدم گل</p></div>
<div class="m2"><p>واعظ هرزه درا گو بنهد دم بر دم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی اندر شط می افکن و حکمت مفروش</p></div>
<div class="m2"><p>تا زمی حل کنمت قاعده جز رواصم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت گلشن خوش و می بیغش و ساقی مهوش</p></div>
<div class="m2"><p>لاجرم باده خمار آورد و شادی غم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روی سوی میکده کن و زدردونان بگذر</p></div>
<div class="m2"><p>که بود خاک در پیر مغان کان کرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف دلدار بکف داری و لب بر لب جام</p></div>
<div class="m2"><p>امشب آشفته شکایت مکن از بخت دژم</p></div></div>