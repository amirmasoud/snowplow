---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>بوفایت یکی از خیل نکورویان نیست</p></div>
<div class="m2"><p>چون گل روی تو در گلشن گلرویان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جویمت باز و گمانم که نه آدم باشد</p></div>
<div class="m2"><p>هر که گم کرده ما را بجهان جویان نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ره کعبه عشقت من تنها پویم</p></div>
<div class="m2"><p>نیست یکتن زنکویان که ترا جویان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منکه چون شیر و شکر در تو در آمیخته ام</p></div>
<div class="m2"><p>در مذاقم اثر از تلخی بدگویان نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود عیب نکوئی تو بدخوئی تو</p></div>
<div class="m2"><p>نیست یکتن که در این راه بسر پویان نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه دل برد پریوار و زمردم بگریخت</p></div>
<div class="m2"><p>نبود مردم و از جنس پریرویان نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مظهر نور علی بود خود آشفته مگر</p></div>
<div class="m2"><p>ورنه آتش به جهان قبله هندویان نیست</p></div></div>