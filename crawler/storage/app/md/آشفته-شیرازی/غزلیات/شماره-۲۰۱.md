---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>به کمانِ ابروان بست دو زلف چون کمندت</p></div>
<div class="m2"><p>که زنی به تیرش آن دل که گریخته ز بندت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه عجب سپند گر سوخت عجب از آنم</p></div>
<div class="m2"><p>که گرفته خو به آتش زچه خال چون سپندت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خدا گلی نماند بر روی دل‌فریبت</p></div>
<div class="m2"><p>نچمد به باغ سروی بر قامت بلندت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز وفا ببخش لیلی تو بر او که هست مجنون</p></div>
<div class="m2"><p>چو سگی بلا بیاید به قفای گوسفندت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو طبیب درد عشقی و دوای دل لب تو</p></div>
<div class="m2"><p>ز وفا ترحمی کن به علیلِ مستمندت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فریب خالش ای دل به شکنج زلف ماندی</p></div>
<div class="m2"><p>به هوای دانه رفتی و به دام درفکندت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ولای حیدر آشفته بجو به جان تمسک</p></div>
<div class="m2"><p>که ز هول روز محشر نبود دگر گزندت</p></div></div>