---
title: >-
    شمارهٔ ۱۰۱۵
---
# شمارهٔ ۱۰۱۵

<div class="b" id="bn1"><div class="m1"><p>خصم با انبوه لشکر آمد و خیل سپاه</p></div>
<div class="m2"><p>الغیاث ای دیده و دل الغیاث ای اشک و آه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه من بگذر زماه و اشک من ماهی بگیر</p></div>
<div class="m2"><p>گر گرفته لشکر اعدا زماهی تا بماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه ای دل بود خصم دغا اصحاب فیل</p></div>
<div class="m2"><p>هان ابابیلی بکن خود نیمه شب بر این سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه فرعون است دشمن رود نیل از چشم ساز</p></div>
<div class="m2"><p>خصم را با لشکرش در لجه خود کن تباه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خصم اگر نمرود باشد یا که خود تو پشه ‏</p></div>
<div class="m2"><p>همتی خواه از خلیل و مغز دشمن را بکاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاروان لطف حیدر آمد از پی غم مخمور</p></div>
<div class="m2"><p>گرچه یوسف وارت افکندندن از حیلت بچاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر جهان دشمن بود جویم مدد از ذوالفقار</p></div>
<div class="m2"><p>برق سوزد گر بود آشفته یک عالم گناه</p></div></div>