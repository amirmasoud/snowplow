---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>مدد ای عشق که مغلوب هوائیم هوا</p></div>
<div class="m2"><p>تو طبیب و همه محتاج دوائیم دوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسم اعظم توئی و دافع آفات و بلا</p></div>
<div class="m2"><p>همتی زآن که گرفتار بلائیم بلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ابر نیسان تو و ما کشته محتاج به آب</p></div>
<div class="m2"><p>کان احسان تو و ما جمله گدائیم گدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره حی گم شده مجنون صفتم سرگردان</p></div>
<div class="m2"><p>همگی گوش بر آواز درائیم درا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما مریضیم و شفاخانه رحمت در تست</p></div>
<div class="m2"><p>دردها مزمن و جویای شفائیم شفا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عملی کاو نبود بهر خدا عین ریاست</p></div>
<div class="m2"><p>وه که از زمره ارباب ریائیم ریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق آشفته کدام است علی دست خدا</p></div>
<div class="m2"><p>دست بر دامن آن دست خدائیم خدا</p></div></div>