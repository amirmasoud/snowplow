---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ای بلبل شوریده بکن تازه نفس را</p></div>
<div class="m2"><p>شکرانه که بشکسته ی امروز قفس را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد بچمن آمد و بلبل بفغان گفت</p></div>
<div class="m2"><p>این بوی ریا چیست که بربست نفس را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جستند رقیبان زنوا محمل لیلی</p></div>
<div class="m2"><p>ای کشا که از نافه گشایند جرس را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غوغای رقیبان بلب تو عجبی نیست</p></div>
<div class="m2"><p>شاید بشکر راه به بندند مگس را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای محتسب عقل چو آئی سوی بازار</p></div>
<div class="m2"><p>در مجلس عشاق مده راه عسس را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داری تو بسر ذوق تماشای گلستان</p></div>
<div class="m2"><p>ناچار بگلزار ببر زحمت خس را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز خوردن حلوا نبود مقصد اغیار</p></div>
<div class="m2"><p>از عشق نباشد خبر ارباب هوس را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای کاش گرفتار شدی اهل ملامت</p></div>
<div class="m2"><p>تا عیب نگویند بسودای تو کس را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی را که بهر بند حدیثی است نهانی</p></div>
<div class="m2"><p>محرم نشمرده است مگر نائی و بس را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما سوخته گانیم و تغافل نه ثوابست</p></div>
<div class="m2"><p>چون برق خدا را مجهان تند فرس را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مقصود وی آشفته بد از عشق شعاعی</p></div>
<div class="m2"><p>موسی که تمنا کرد از طور قبس را</p></div></div>