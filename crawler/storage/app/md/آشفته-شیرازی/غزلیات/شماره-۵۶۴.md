---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>بی گل رویت یکیست بوی گل و نیش خار</p></div>
<div class="m2"><p>فرش حریر است خار در ره جویای یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجمر روی تو را خال مجاور شده</p></div>
<div class="m2"><p>کی بود اسپند را بر سر آتش قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرق من و شیخ شهر چیست بگویم تمام</p></div>
<div class="m2"><p>او بعمل نازد و ما بتو امیدوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشترت ار در قطار هست بر او بار نه</p></div>
<div class="m2"><p>من که گسستم قطار چند کشی زیر بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاجرمم زینهار بر در حیدر کشد</p></div>
<div class="m2"><p>ترک ستمکار من بسکه خورد زینهار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیر گر از شست تست طعم رطب بخشدم</p></div>
<div class="m2"><p>زهر گر از دست تست چیست می خوشگوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده آنم که خفت جای نبی بر سریر</p></div>
<div class="m2"><p>غیر بگو خوش برو تو زپی یار غار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بخدا بنده شد شیر خدا دست حق</p></div>
<div class="m2"><p>او بهمه کاینات گشت خداوندگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دفتر آشفته را غیر مدیح تو نیست</p></div>
<div class="m2"><p>شایدش از این مدیح بر دو جهان افتخار</p></div></div>