---
title: >-
    شمارهٔ ۷۵۹
---
# شمارهٔ ۷۵۹

<div class="b" id="bn1"><div class="m1"><p>چندانکه وفا کردم و اظهار ارادت</p></div>
<div class="m2"><p>جز جور و جفا زآن بت طناز ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفته نهفتم بدرون گنج غم عشق</p></div>
<div class="m2"><p>جز سینه کسی محرم این راز ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب زعنایت ره این پرده بگردان</p></div>
<div class="m2"><p>زیرا که ره راست از این ساز ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد دل سودا زده با آل عبا گو</p></div>
<div class="m2"><p>کز خلق جز این سلسله اعجاز ندیدم</p></div></div>