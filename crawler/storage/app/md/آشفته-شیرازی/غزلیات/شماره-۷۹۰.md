---
title: >-
    شمارهٔ ۷۹۰
---
# شمارهٔ ۷۹۰

<div class="b" id="bn1"><div class="m1"><p>زشکنجه گر بمیرم نظر از تو برنگیرم</p></div>
<div class="m2"><p>چون زتست درد درمان زکسی نمی پذیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو زحسن بی نظیری بدیار ماه رویان</p></div>
<div class="m2"><p>من دلشده نظرباز و بعشق بی نظیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه رها کنی زبندم که سراست در کمندم</p></div>
<div class="m2"><p>نه گریز آید از ما که دل است ناگزیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخیال زلف و خطت چو شبی کنم تفکر</p></div>
<div class="m2"><p>همه ضیمران بروید زحدیقه ضمیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه همین فغانم از خلق ببرد خواب راحت</p></div>
<div class="m2"><p>که بمردگان دمد صور زناله نفیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو که کان کیمیائی تو که نور کبریائی</p></div>
<div class="m2"><p>تو که شاه اولیائی نظری که من فقیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو خضر زجوی شمشیر تو خورد آب حیوان</p></div>
<div class="m2"><p>بزن آبیم بر آتش که زتشنگی نمیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلبت حکایتی دوش شنیدم و نوشتم</p></div>
<div class="m2"><p>که بیادگار ماند سخنان دلپذیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه حلاوتم به منقار و چه شکرم به نطق است</p></div>
<div class="m2"><p>که زآشیان جنت همه شب رسد صفیرم</p></div></div>