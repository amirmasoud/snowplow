---
title: >-
    شمارهٔ ۵۹۸
---
# شمارهٔ ۵۹۸

<div class="b" id="bn1"><div class="m1"><p>کی در بهاران دیده ای بلبل فرو بندد نفس</p></div>
<div class="m2"><p>یا در میان کاروان بی غلغله ماند جرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پائی بدامان میکشم در دیده افغان میکشم</p></div>
<div class="m2"><p>فریاد پنهان میکشم چون نیستم فریادرس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر بیاد گلستان بیجا بود آه و فغان</p></div>
<div class="m2"><p>صیاد اگر شد مهربان سازیم با کنج قفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم فراغ از بال کشت من با تو ای حورا سرشت</p></div>
<div class="m2"><p>ور بی تو باشم در بهشت باشد بچشمم چون قفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قید هستی رسته ام وز زندگانی خسته ام</p></div>
<div class="m2"><p>تا دل بعشقت بسته ام برکنده ام بیخ هوس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای هوشمندان الحذر از راه عشق پرخطر</p></div>
<div class="m2"><p>وهم گمان را زین سفر فرسوده شد پای حرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دین و خرد در باختم تا توشه ره ساختم</p></div>
<div class="m2"><p>جانان جان بشناختم خود جان نخواهم زین سپس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کعبه و دیرم مگو زنار و تسبیحم مجو</p></div>
<div class="m2"><p>زین هر دو دارم بر تو رو محرابم ابروی تو بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیتی مرا دشمن بود دوران بقصد من بود</p></div>
<div class="m2"><p>آشفته ات یکتن بود جز تو ندارد هیچکس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرمانده کیهان توئی شاهنشه دوران توئی</p></div>
<div class="m2"><p>چون شحنه امکان توئی پروا ندارم از عسس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای صاحب عصر و زمان ای خضر راه گمرهان</p></div>
<div class="m2"><p>من مانده دور از کاروان وین رهزنان از پیش و پس</p></div></div>