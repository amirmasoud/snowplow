---
title: >-
    شمارهٔ ۵۹۴
---
# شمارهٔ ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>رتبه عشق برونست زادراک قیاس</p></div>
<div class="m2"><p>عقل مستأصل چونست قرین افلاس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پایه اش گاه بفر شست و گهی بر سر عرش</p></div>
<div class="m2"><p>پرتو عشق زآفاق عیان و زانفاس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاه را گر نبود تاج و نگین سلطان نیست</p></div>
<div class="m2"><p>عشق را رتبه نکاهد بود ار کهنه پلاس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق در کسوت درویش اگر جلوه کند</p></div>
<div class="m2"><p>قدر لؤلؤ نشود کم چه بپوشی افلاس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل شیر است ولی عشق بود آتش طور</p></div>
<div class="m2"><p>شیر را لاجرم از شعله نار است هراس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا تو خضری و جام میت آب حیات</p></div>
<div class="m2"><p>رحمتی بر من عطشان بچشان از این کاس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانده آشفته گم گشته به تیه حیرت</p></div>
<div class="m2"><p>همچون آن مور که بیچاره فتاده در طاس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزن ای دست خدا بر مسم اکسیر مراد</p></div>
<div class="m2"><p>کیمیا زر کند ار ذره‌ای افتد به نحاس</p></div></div>