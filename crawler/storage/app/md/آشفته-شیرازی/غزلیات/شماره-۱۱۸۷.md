---
title: >-
    شمارهٔ ۱۱۸۷
---
# شمارهٔ ۱۱۸۷

<div class="b" id="bn1"><div class="m1"><p>نشکفته است از چمن دلبری گلی</p></div>
<div class="m2"><p>کاندر هوای او نسرائیده بلبلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل که شوق گل بکمندش در افکند</p></div>
<div class="m2"><p>باید بزخم خار نماید تحملی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز نظر کبوتر دل دید در هوا</p></div>
<div class="m2"><p>تیهو صفت اسیر نمودش بچنگلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنها نه خاکیان متزلزل زعشق تو</p></div>
<div class="m2"><p>کاز تو بود بعالم علوی تزلزلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سرو و سنبل و گل و نسرین چه احتیاج</p></div>
<div class="m2"><p>کاز زلف و روی و قدتو گل و سرو سنبلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد تو را جمال بتا از ازل جمیل</p></div>
<div class="m2"><p>بندند شاهدان چمن گر تجملی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور نوای مرغ سحر خوان هوای گل</p></div>
<div class="m2"><p>مستان تو فکنده در آفاق غلغلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته چیست خاری از این بوستان سرا</p></div>
<div class="m2"><p>خود را بصد هزار فسون بسته بر گلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن گل که زیب گلشن امکان اگر نبود</p></div>
<div class="m2"><p>آدم نبود بر ملکوتش تفضلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور خدا علی ولی و صراط حق</p></div>
<div class="m2"><p>کز مهر او خدای برافراشته پلی</p></div></div>