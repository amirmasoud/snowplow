---
title: >-
    شمارهٔ ۶۶۹
---
# شمارهٔ ۶۶۹

<div class="b" id="bn1"><div class="m1"><p>گفتی زفراق یار چونم</p></div>
<div class="m2"><p>چون مردم دیده غرق خونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ماه رخ تو کوکب از چشم</p></div>
<div class="m2"><p>ریزد زستارگان فزونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ظلمت هجر راه گمشد</p></div>
<div class="m2"><p>ای خضر تو باش رهنمونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گشت مسلمم غم دوست</p></div>
<div class="m2"><p>در مدرس عشق ذی فنونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگذار بزیر تیغ غیرم</p></div>
<div class="m2"><p>غیرت کن و خود بریز خونم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تشنه و دوست آبحیوان</p></div>
<div class="m2"><p>بی یار بیا ببین که چونم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای حلقه زلف از ترحم</p></div>
<div class="m2"><p>زنجیر بیار بر جنونم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتا ظفر از منست زلفت</p></div>
<div class="m2"><p>پرچم شده گرچه سرنگونم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با جادوی چشم گو خدا را</p></div>
<div class="m2"><p>کز ره نبرند از فسونم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفتی تو و آسمان همیخواست</p></div>
<div class="m2"><p>کز این حرکت برد سکونم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا چشم رقیب شد سرایت</p></div>
<div class="m2"><p>صد عین روان شد از عیونم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر مردم دیده بی جمالت</p></div>
<div class="m2"><p>نشتر همه شب زند جفونم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای شیر خدا زلطف برهان</p></div>
<div class="m2"><p>از منت روزگار دونم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای ابر مژه بزن زرحمت</p></div>
<div class="m2"><p>آبی تو بر آتش درونم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آشفته تو بود گرفتار</p></div>
<div class="m2"><p>بگسل تو علاقه جنونم</p></div></div>