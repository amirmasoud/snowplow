---
title: >-
    شمارهٔ ۹۹۵
---
# شمارهٔ ۹۹۵

<div class="b" id="bn1"><div class="m1"><p>عاشقان را جز لب تو باده مستانه نه</p></div>
<div class="m2"><p>می‌کشان را غیر چشمت ساغر و پیمانه نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو در افکن کشتی عشاق را در شط می</p></div>
<div class="m2"><p>در خور مستی مستان غمت خمخانه نه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم طلبکارت برهمن هم هواخواه تو شیخ</p></div>
<div class="m2"><p>دیده کس مأوای تو کعبه نه و بتخانه نه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جهان غواص خواهد بود در بحر وجود</p></div>
<div class="m2"><p>بحر وحدت را بود دری چو تو دردانه نه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر براندازی تو ای لیلا زرخ پرده بحی</p></div>
<div class="m2"><p>همچو آشفته تو را مجنون بود دیوانه نه</p></div></div>