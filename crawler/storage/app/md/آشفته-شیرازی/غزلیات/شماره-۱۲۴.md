---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>جامه کعبه چو خم موی تست</p></div>
<div class="m2"><p>قبله عشاق در ابروی تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف مصری که برآمد زچاه</p></div>
<div class="m2"><p>چنبر دلوش رسن موی تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اژدر موسی که بود سحرخوار</p></div>
<div class="m2"><p>پی سپر نرگس جادوی تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این چه گلابست زخاک درت</p></div>
<div class="m2"><p>در گل حمرا اثر از روی تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طائف بیت الله تو جای ماست</p></div>
<div class="m2"><p>خانه کعبه حرم کوی تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی کوثر توئی و خضر وقت</p></div>
<div class="m2"><p>کوثر و تسنیم هم از جوی تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشعله افروز درت ماه و مهر</p></div>
<div class="m2"><p>چرخ جنیبت کش اردوی تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گل بستان که نشان از تو دید</p></div>
<div class="m2"><p>بلبل گلزار سخن گوی تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاطر آشفته پریشان مساز</p></div>
<div class="m2"><p>زآنکه پریشانیش از موی تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه زهر سود رود اسب سخن</p></div>
<div class="m2"><p>لیک زهر سوی رخش سوی تست</p></div></div>