---
title: >-
    شمارهٔ ۱۱۱۰
---
# شمارهٔ ۱۱۱۰

<div class="b" id="bn1"><div class="m1"><p>دوشم اسباب طرب جمله مهیا بودی</p></div>
<div class="m2"><p>شاهد و نقل و می و عیش مهنا بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنگ و نی ناله کنان بربط و دف در افغان</p></div>
<div class="m2"><p>خنده ساغر و هم گریه مینا بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطربان هر طرفی نغمه سرا چون داود</p></div>
<div class="m2"><p>همه خوش لحن تر از بلبل شیدا بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرده از صوت حسن محو اثر باربدی</p></div>
<div class="m2"><p>در مزامیر چه استاد نکیسا بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهدان رقص کنان دست زنان پاکوبان</p></div>
<div class="m2"><p>همه غلمان بهشتی همه حورا بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع آن نور که در طور تجلی میکرد</p></div>
<div class="m2"><p>ساحت انجمنش سینه سینا بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میر مجلس که یکی مغبچه ترسائی</p></div>
<div class="m2"><p>که بلب معجزه بخشای مسیحا بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اژدر گیسوی او سحر خور و جادوکش</p></div>
<div class="m2"><p>دربناگوش جمالش ید و بیضا بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می در آن بزم نه ساغر نه سبو خمخانه</p></div>
<div class="m2"><p>محفل از موج قدح غیرت دریا بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وه چه می آتش سیاله طلای محلولی</p></div>
<div class="m2"><p>کان یاقوت کزو قوت روانها بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا فشاند بسر مجلسیان شب شاباش</p></div>
<div class="m2"><p>دامن چرخ پر از لؤلؤ لالا بودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راست خواهی بمثل بود بهشت دنیا</p></div>
<div class="m2"><p>زآنکه آن جنت موعود هم آنجا بودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر حریفی بظریفی شده سرگرم طرب</p></div>
<div class="m2"><p>لیک آشفته در آن واقعه تنها بودی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جنگ از چنگ همی سرزد و رجمر خمار</p></div>
<div class="m2"><p>دوست دشمن شده محفل صف هیجا بودی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرنیان پوش بتی ساده و ساده زحریر</p></div>
<div class="m2"><p>فرش کویش همه از اطلس و دیبا بودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ترک چشمش بخورد خون سیاوش چون می</p></div>
<div class="m2"><p>مژه اش چاک زن پهلوی دارا بودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قامت معتدلش غیرت سرو کشمر</p></div>
<div class="m2"><p>غمزه متصلش آفت دلها بودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زابرو و زلف و رخ آن لعبتک فرخاری</p></div>
<div class="m2"><p>صاحب کعبه و محراب و کلیسا بودی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوش بادا همه دشنام شد و حلقه گسیخت</p></div>
<div class="m2"><p>عیش دوران بنگر کش چه تقاضا بودی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ساقی دور بمستان می زهر آگین داد</p></div>
<div class="m2"><p>ورنه این نشئه کجا درخور صهبا بودی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نازم آن باده که پیمود مغ باده فروش</p></div>
<div class="m2"><p>بحریفان که از این لوث مبرا بودی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وه چه باده می توحید ولای حیدر</p></div>
<div class="m2"><p>که شعاع قدحش آتش سینا بودی</p></div></div>