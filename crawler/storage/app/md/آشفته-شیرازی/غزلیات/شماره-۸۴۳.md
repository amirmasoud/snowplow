---
title: >-
    شمارهٔ ۸۴۳
---
# شمارهٔ ۸۴۳

<div class="b" id="bn1"><div class="m1"><p>ما ز سودای گل و از بوستان آسوده‌ایم</p></div>
<div class="m2"><p>نوبهاری جسته‌ایم و از خزان آسوده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اصل جانان هست گو یک سر جهان فانی بود</p></div>
<div class="m2"><p>یک جهان جان برده‌ایم و از جهان آسوده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بهشت نسیه‌ای دارند وقتی زاهدان</p></div>
<div class="m2"><p>ما به نقد از همت پیر مغان آسوده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بلا پیوسته بارد زآسمان ما را چه غم</p></div>
<div class="m2"><p>زآن که اندر سایه دارالامان آسوده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به خاک میکده جستیم آب زندگی</p></div>
<div class="m2"><p>خضر را گو کز حیات جاودان آسوده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژده کامد آن پری کاندر میان مردمان</p></div>
<div class="m2"><p>ما ز طعن مردمان اندر جهان آسوده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یوسفی در مصر دل جستم عزیز ای دوستان</p></div>
<div class="m2"><p>از تمنای بشیر و کاروان آسوده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما متاع دین و دل دادیم بر تاراج عشق</p></div>
<div class="m2"><p>از خطرهای ره وز رهزنان آسوده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برده عشقت از دل پیر و جوان تاب و توان</p></div>
<div class="m2"><p>خوش ز تیر طعنه پیر و جوان آسوده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن زره‌موی کمان‌ابرو که اندر خیل ماست</p></div>
<div class="m2"><p>ما ز تیغ تیز وز تیر و کمان آسوده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا که آشفته امام عصر را شد مدح‌خوان</p></div>
<div class="m2"><p>از بلای فتنه آخر زمان آسوده‌ایم</p></div></div>