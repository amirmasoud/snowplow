---
title: >-
    شمارهٔ ۴۳۰
---
# شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>صحبتی از جان مگر در پیش جانان گفته‌اند</p></div>
<div class="m2"><p>نور پیدا را حدیث از نار پنهان گفته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصه دل‌های زار عاشقان با زلف تو</p></div>
<div class="m2"><p>گرچه جمعی گفته‌اند اما پریشان گفته‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر بلند است ار سرش بر دار یاسا می‌زند</p></div>
<div class="m2"><p>چون گدایی را شکایت پیش سلطان گفته‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیره خاکی در بر اکسیر اعظم برده‌اند</p></div>
<div class="m2"><p>نام دیوی زشت را نزد سلیمان گفته‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با لبت گفتند درد دل مرا از دشمنی</p></div>
<div class="m2"><p>دوستی شد درد را چون بهر درمان گفته‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشتیاق پیر کنعان بهر یوسف برده‌اند</p></div>
<div class="m2"><p>حالت بستان جنت را به رضوان گفته‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچنان گفتند کآمد یارت امشب در وثاق</p></div>
<div class="m2"><p>مژده تشریف یوسف را به زندان گفته‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق را سودا شما رند ار حکیمان عیب نیست</p></div>
<div class="m2"><p>آنچه را با چشم ظاهر دیده‌اند آن گفته‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر پنهان دل ار مانده است آه و آب چشم</p></div>
<div class="m2"><p>هر دو در یک لمحه از سر تا به پایان گفته‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به دل مدحت بسی گفتند از اهل سخن</p></div>
<div class="m2"><p>دل‌نشین‌تر آمد آن مدحی که از جان گفته‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیخ گفت آشفته در وصف علی کرده علو</p></div>
<div class="m2"><p>بیش از این می‌دانمت جانا که ایشان گفته‌اند</p></div></div>