---
title: >-
    شمارهٔ ۱۰۱۱
---
# شمارهٔ ۱۰۱۱

<div class="b" id="bn1"><div class="m1"><p>بمسجد رخنه ای بگشای ای زاهد زمیخانه</p></div>
<div class="m2"><p>که از نورش کنی چون طور روشن صحن کاشانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن موقف که ساقی ساغر توحید میبخشد</p></div>
<div class="m2"><p>توهم جامی بزن کز خویش خواهی گشت بیگانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زبان عقل و سود عشق بر مستان بود واضح</p></div>
<div class="m2"><p>تو هم بهر زیان و سود عاقل باش و فرزانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی بزمی که روشن گشته از نور مه ساقی</p></div>
<div class="m2"><p>که شمع خاوری در پیش شمع اوست پروانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود تا سر سودای بتان اندر سویدایم</p></div>
<div class="m2"><p>بگوش من نخواهد رفت ناصح پند دانانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا از خانقه بیرون بهل افسانه و افسون</p></div>
<div class="m2"><p>که جز پیر مغان دیگر ندیدم مرد و مردانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخم بنشین چو می تا صاف گردد درد عقل تو</p></div>
<div class="m2"><p>فلاطون کسب حکمت کرد از مستان دیوانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جز شور علی آشفته نبود در دماغ من</p></div>
<div class="m2"><p>مرا از کاسه سر عشق تا بگشود دندانه</p></div></div>