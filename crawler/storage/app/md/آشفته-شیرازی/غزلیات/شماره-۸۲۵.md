---
title: >-
    شمارهٔ ۸۲۵
---
# شمارهٔ ۸۲۵

<div class="b" id="bn1"><div class="m1"><p>ثمر از نخل عیش دَهْرْ غیر از غم نمی‌بینم</p></div>
<div class="m2"><p>به جز حرمان ثمر از کشتهٔ عالم نمی‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسیحم گر طبیب آید که جز دردت نمی‌خواهم</p></div>
<div class="m2"><p>که غیر از زخم پیکانت به دل مرهم نمی‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی گر همدمم باشی و بگذاری لبم بر لب</p></div>
<div class="m2"><p>همان دم جان دهم چون جز لبت همدم نمی‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشسته می‌کشان غمناک گرداگرد میخانه</p></div>
<div class="m2"><p>بهشتست این چرا در او دل خرم نمی‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه طوفان دید دوش از اشک یارب مردمِ چشمم</p></div>
<div class="m2"><p>که امشب هفت دریا را به جز شبنم نمی‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببستم دیده بر اغیار بگشودم در دل را</p></div>
<div class="m2"><p>که بینم یار بی‌اغیار آن را هم نمی‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نپندارم ملک چون ما بود سرمست عشق تو</p></div>
<div class="m2"><p>که من این ذوق را جز در بنی‌آدم نمی‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گدای کوی میخانه بسی خاتم به جم بخشد</p></div>
<div class="m2"><p>به دست جم اگرچه غیر یک خاتم نمی‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در تو برتر از عرش است ای دست خدا الحق</p></div>
<div class="m2"><p>که من عرش برین را آنچنان اعظم نمی‌بینم</p></div></div>