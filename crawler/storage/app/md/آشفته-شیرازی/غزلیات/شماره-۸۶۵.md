---
title: >-
    شمارهٔ ۸۶۵
---
# شمارهٔ ۸۶۵

<div class="b" id="bn1"><div class="m1"><p>خبرت هست که چون با تو درآمیخته‌ام</p></div>
<div class="m2"><p>با تو پیوسته ز عالم همه بگسیخته‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد دل گمشده در خاک در تو ز ازل</p></div>
<div class="m2"><p>بیهده خاک سر کوی بتان بیخته‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دل در خم زلفت نه کنون منزل کرد</p></div>
<div class="m2"><p>آشیانی است که طرحش ز ازل ریخته‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه شب هم نفس مرغ شب‌آویزم من</p></div>
<div class="m2"><p>عجبی نیست که در زلف تو آویخته‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا حوادث نبرد ره به من طعن رقیب</p></div>
<div class="m2"><p>به در میکده رحمت بگریخته‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست حق آنکه پی تفرقه اهل هوس</p></div>
<div class="m2"><p>تیغش آشفته به صد جهد برآهیخته‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا علی دست مرا گیر که از خلق جهان</p></div>
<div class="m2"><p>همه بگریخته در دامنت آویخته‌ام</p></div></div>