---
title: >-
    شمارهٔ ۱۱۵۶
---
# شمارهٔ ۱۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ای که آگه نیستی از حال گوی</p></div>
<div class="m2"><p>چنبر چوگان ببین و ز گو مگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر گلت را برد گلچین عندلیب</p></div>
<div class="m2"><p>خار گو از دیده چون مژگان بروی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرمن عشاق را آتش بیار</p></div>
<div class="m2"><p>برق گو جز آشیان ما مجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون قد موزونت و چشم ترم</p></div>
<div class="m2"><p>سرو نه در باغ و آبی نه بجوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما ببوی موی جانان زنده ایم</p></div>
<div class="m2"><p>قوت روحانیان باشد ببوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که نقش مرتضی آشفته بست</p></div>
<div class="m2"><p>نقش غیر از دیده و دل گو بشوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خضر گو آب بقا اینجا مبر</p></div>
<div class="m2"><p>روح گو خاک سر کویش ببوی</p></div></div>