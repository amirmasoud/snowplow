---
title: >-
    شمارهٔ ۱۱۲۳
---
# شمارهٔ ۱۱۲۳

<div class="b" id="bn1"><div class="m1"><p>سلطانم ار بسازم زآن خاک در کلاهی</p></div>
<div class="m2"><p>خورشیدم ار بسایم روئی بخاک راهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی آمده ببازار سروی بساق سیمین</p></div>
<div class="m2"><p>با کس سخن نگفته اندر وثاق ماهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن گاه در غروب و رویت مدام تابان</p></div>
<div class="m2"><p>حسن تو راست خورشید روشن برین گواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه شکوه اش زقتلست جویای قاتلست آن</p></div>
<div class="m2"><p>گر بشنوی بمحشر فریاد دادخواهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون هست تیر غمزه با آن سپاه مژگان</p></div>
<div class="m2"><p>تو فوج خصم بشکن بی لشکر و سپاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو خود مسیح وقتی با صد مقام افزون</p></div>
<div class="m2"><p>کاز گفتنی دهی جان کز کشتنی نگاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ماه و مهر پیشت لافی زنند از حسن</p></div>
<div class="m2"><p>بر خرمن مه و مهر آتش زنم زآهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ماه و خور نگویند ما روشنیم بالذات</p></div>
<div class="m2"><p>آئینه را نگهدار در پرده گاه گاهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز دوستی حیدر ما را ثواب نبود</p></div>
<div class="m2"><p>زیرا که غیر بغضش نبود دگر گناهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته گر بروید مهر گیا زخاکم</p></div>
<div class="m2"><p>شاید که نیست جز مهر در این چمن گیاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید اگر بگیری دست چو من گدائی</p></div>
<div class="m2"><p>چون تو به خیل امکان بالجمله پادشاهی</p></div></div>