---
title: >-
    شمارهٔ ۷۷۱
---
# شمارهٔ ۷۷۱

<div class="b" id="bn1"><div class="m1"><p>زد هجر بصبح وصال فالم</p></div>
<div class="m2"><p>خور داد نشان از آن جمالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید اگر بخانه باشد</p></div>
<div class="m2"><p>اختر چه غم است ازو بالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای جمال آن پری روی</p></div>
<div class="m2"><p>هم خواب ببرد و هم خیالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز زفرقت تو سالی است</p></div>
<div class="m2"><p>چون است فراق تو بسالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عناب لبت ببزم بوسید</p></div>
<div class="m2"><p>خون خورد زعذر بدسگالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستقبل و ماضیم چه پرسی</p></div>
<div class="m2"><p>بر وصل کنون خوش است حالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زآئینه زدود آب می رنگ</p></div>
<div class="m2"><p>شادی برهاند از ملالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر شکوه ای از فراق او رفت</p></div>
<div class="m2"><p>الحمد که شاهد وصالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیراب شدم زلعل نوشش</p></div>
<div class="m2"><p>ای خضر چه میدهی زلالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر درد دل احتمال درمان</p></div>
<div class="m2"><p>گفتند و نبود احتمالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الا که بگیرم آن سر زلف</p></div>
<div class="m2"><p>در شام فراق بر تو نالم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشفته سگ در علی شو</p></div>
<div class="m2"><p>تا فخر کنی به اهل عالم</p></div></div>