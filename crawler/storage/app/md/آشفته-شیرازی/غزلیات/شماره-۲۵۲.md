---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>نقش رخ یار سرو قامت</p></div>
<div class="m2"><p>بر دل بنشست تا قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی سود کند مرا نصیحت</p></div>
<div class="m2"><p>سودا ننشیند از ملامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشتیم بپا و سر جهان را</p></div>
<div class="m2"><p>کردیم بکوی تو اقامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کار که کرده ایم جز عشق</p></div>
<div class="m2"><p>حاصل نشدش بجز ندامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگریز زعشق عافیت سوز</p></div>
<div class="m2"><p>کو آفت دین شد و سلامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاید شوم ار علم برندی</p></div>
<div class="m2"><p>کز عشق تو باشدم علامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته بکوی میکشان رو</p></div>
<div class="m2"><p>وز پیر مغان بجو کرامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن صاحب رتبه سلونی</p></div>
<div class="m2"><p>آن والی کشور امامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شیخ طمع ببر که هرگز</p></div>
<div class="m2"><p>ناید زلئیم جز لئامت</p></div></div>