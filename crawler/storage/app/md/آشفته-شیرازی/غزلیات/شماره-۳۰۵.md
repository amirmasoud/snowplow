---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>گویند بهار است و جهان رشک بهشت است</p></div>
<div class="m2"><p>اطراف چمن پر ز بت حورسرشت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عکس بهشتی بتکان ساحت گلزار</p></div>
<div class="m2"><p>اندر نظر باده کشان باغ بهشت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیخ و حرم و بلبل و گل مؤبد و دانش</p></div>
<div class="m2"><p>ما را نه سر کعبه نه گلشن نه کنشت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای هم نفسان باغ و گلستان بشما خوش</p></div>
<div class="m2"><p>زیرا که مرا کنج قفس دست نوشت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر طارم کیوان بودم پای زرفعت</p></div>
<div class="m2"><p>هر چند بمیخانه سرم بر سر خشت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خشت در کریاس علی قبله هشتم</p></div>
<div class="m2"><p>کز بهر من آشفته بسی به زبهشت است</p></div></div>