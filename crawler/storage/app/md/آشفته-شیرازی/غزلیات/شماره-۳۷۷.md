---
title: >-
    شمارهٔ ۳۷۷
---
# شمارهٔ ۳۷۷

<div class="b" id="bn1"><div class="m1"><p>گر سرو چو بالای تو چالاک نباشد</p></div>
<div class="m2"><p>ور گل چو جمال تو طربناک نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیباک بود ترک بخونریزی مردم</p></div>
<div class="m2"><p>چون ترک سیه مست تو بیباک نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناپاک بود اهرمن جادوی رهزن</p></div>
<div class="m2"><p>چون غمزه خونریز تو ناپاک نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آینه در بزم صفا پاکدلی نیست</p></div>
<div class="m2"><p>لیکن چو دل اهل نظر پاک نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چشم تو از زلف بر آویخت کمندی</p></div>
<div class="m2"><p>سر نیست که در آن خم فتراک نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این حسن و صباحت زگل باغ ندیدم</p></div>
<div class="m2"><p>وین لطف سخن در مه افلاک نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل نیست که از حسرت آن غنچه خندان</p></div>
<div class="m2"><p>چون گل ببرش سینه صد چاک نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ساحت میخانه مجو یکدل ناشاد</p></div>
<div class="m2"><p>هر کس ببهشت آمد غمناک نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن میکده فیض در شاه ولایت</p></div>
<div class="m2"><p>کادم نه که بر درگه او خاک نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن خسرو اقلیم خلافت که بجز او</p></div>
<div class="m2"><p>کس وارث شاهنشه لولاک نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته بگلزار ولای تو در آویخت</p></div>
<div class="m2"><p>در باغ نشاید خس و خاشاک نباشد</p></div></div>