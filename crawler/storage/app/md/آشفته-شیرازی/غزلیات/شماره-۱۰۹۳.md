---
title: >-
    شمارهٔ ۱۰۹۳
---
# شمارهٔ ۱۰۹۳

<div class="b" id="bn1"><div class="m1"><p>تو که از جهان و اهلش همه احتراز داری</p></div>
<div class="m2"><p>بکف آر زاد راهی که ره دراز داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمحب دوست دشمن شدن این خلاف باشد</p></div>
<div class="m2"><p>زچه از رقیب پیوسته تو احتراز داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار نقش اغیار و بکعبه شو مسافر</p></div>
<div class="m2"><p>تو که بت در آستینی چه سر حجاز داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخری بهیچ قیمت بود ار هزار یوسف</p></div>
<div class="m2"><p>تو که چون سبکتکین چشم سوی ایاز داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه پیرو هوائی و بخویش عشق بستی</p></div>
<div class="m2"><p>نبود چو قبله کعبه تو کجا نماز داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه که بلبل است ای گل بگشای گوشی اندک</p></div>
<div class="m2"><p>که هزار مرغ در باغ ترانه ساز داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چه یوسفی خدا را که سفر بمصر کردی</p></div>
<div class="m2"><p>که هزار چشم یعقوب براه باز داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو که آدمی بصورت نبود چو داغ عشقت</p></div>
<div class="m2"><p>نتوان زجانور گفت تو امتیاز داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحریم کعبه یکعمر مجاورت نباشد</p></div>
<div class="m2"><p>چو شبی بخلوت دوست دری فراز داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شب وصل دوست آشفته مگو حدیث هجران</p></div>
<div class="m2"><p>که تو راست عمر کوتاه و سخن دراز داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر بی نیازی ار هست زخلق روزگارت</p></div>
<div class="m2"><p>بدر علی همان به که رخ از نیاز داری</p></div></div>