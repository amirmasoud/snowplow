---
title: >-
    شمارهٔ ۷۸۸
---
# شمارهٔ ۷۸۸

<div class="b" id="bn1"><div class="m1"><p>به کام دل شبی گر بوسی از لعل تو بستانم</p></div>
<div class="m2"><p>اگرچه یک جهان جان باشدم دامن برافشانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه در همه عمرم به پیش ای دوست ننشینی</p></div>
<div class="m2"><p>ولی من گر همه جانست بر جای تو ننشانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنم هرشب به دل پیمان که توبه بشکنم دیگر</p></div>
<div class="m2"><p>سحرگه بر سر پیمانه خواهد رفت پیمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهم راه بردن سوی کعبه من از این وادی</p></div>
<div class="m2"><p>که بر دامان بسی آویخته خار مغیلانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهان ماند کجا رازم که باشد اشک غمازم</p></div>
<div class="m2"><p>دریغا گفت بی‌پرده بمردم راز پنهانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلاطون را ز درد عشق گفتم مختصر حرفی</p></div>
<div class="m2"><p>بگفت این درد را درمانده‌ام درمان نمی‌دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشیدم هفت دریا و همان مستسقی عطشان</p></div>
<div class="m2"><p>مسیحا عجز می‌آرد که خواهد کرد درمانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتم از هوسناکی کنم پرهیز و بی‌باکی</p></div>
<div class="m2"><p>بگفتا عقل مسکین من علاج نفس نتوانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من آن حلواییم کز شکرم خالی بود دکان</p></div>
<div class="m2"><p>بیا از خنده شیرین رواجی ده به دکانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زند تر خنده بر گلزار کابل غنچه آن لب</p></div>
<div class="m2"><p>که من بی‌زحمت خار از گل عارض گلستانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر حب علی آشفته گیرد دست در محشر</p></div>
<div class="m2"><p>وگرنه من در آن تیه ضلالت مانده حیرانم</p></div></div>