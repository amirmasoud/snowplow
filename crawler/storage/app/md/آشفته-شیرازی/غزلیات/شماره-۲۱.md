---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>بگویم ار ز غم عشق داستانی را</p></div>
<div class="m2"><p>چو خویش واله و شیدا کنم جهانی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوی آنکه شوم طعمه سگان درش</p></div>
<div class="m2"><p>نهفته‌ام به بدن مشت استخوانی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمانده است تمیزی میانه من و غیر</p></div>
<div class="m2"><p>بکش برای خدا تیغ امتحانی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعیب عشق چه شد رهنمون به طور ظهور</p></div>
<div class="m2"><p>امین سینه سینا کند شبانی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست پیر مغان اوفتد چو نقش بتم</p></div>
<div class="m2"><p>به بتکده ببرد طرفه ارمغانی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر چشم که برابردی تو حکم براند</p></div>
<div class="m2"><p>کسی چگونه کشید آنچنان کمانی را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب ز دلشدگان نیست این عجب باشد</p></div>
<div class="m2"><p>که دل ز کف بستانند دلستانی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عمر خویش شود بهره‌ور چو خضر کسی</p></div>
<div class="m2"><p>که دستگیر شود پیر ناتوانی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کند به دیدهٔ یعقوب جا چو کحل نسیم</p></div>
<div class="m2"><p>برد ز مصر اگر گرد کاروانی را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دادخواهی مردم تو را به عرصه حشر</p></div>
<div class="m2"><p>برای من نگذارند یک زمانی را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سرو فخر کند باغبان و آشفته</p></div>
<div class="m2"><p>به دیده آب دهد شاخ ارغوانی را</p></div></div>