---
title: >-
    شمارهٔ ۵۷۳
---
# شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>گر توئی ساقی ما باده خلر چه ضرور</p></div>
<div class="m2"><p>ور توئی شاهد ما لعبت کشمر چه ضرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر تسخیر دلم صف زده خیل مژه ات</p></div>
<div class="m2"><p>از پی ملک خراب این همه لشکر چه ضرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم دیده چو دید آن مژه با خود گفتا</p></div>
<div class="m2"><p>خون زقیفال روانست به نشتر چه ضرور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب وصلست و مه ما بوثاق است امشب</p></div>
<div class="m2"><p>بر فلک تابش این کوکب و اختر چه ضرور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنبر خام سر زلف بس و آتش رخ</p></div>
<div class="m2"><p>عود میسوزی از این بعد بمجمر چه ضرور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون غبار در او هست چه حاجت با کحل</p></div>
<div class="m2"><p>هست چون خاک در دوست به بستر چه ضرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند آشفته بری بر در اغیار پناه</p></div>
<div class="m2"><p>بر در پیر مغان رو در دیگر چه ضرور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طلب از خاک در میکده اکسیر مراد</p></div>
<div class="m2"><p>سجده جز بر قدم حیدر صفدر چه ضرور</p></div></div>