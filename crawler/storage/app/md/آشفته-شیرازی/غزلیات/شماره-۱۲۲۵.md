---
title: >-
    شمارهٔ ۱۲۲۵
---
# شمارهٔ ۱۲۲۵

<div class="b" id="bn1"><div class="m1"><p>ایکه مطبوع و شوخ و دلبندی</p></div>
<div class="m2"><p>از چه با مات نیست پیوندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ بر کش بکش ملول نباش</p></div>
<div class="m2"><p>اگر از قتل بنده خرسندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که بیند بکشته ام گوید</p></div>
<div class="m2"><p>کام دل یافت آرزومندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیر تسلیم نیست چاره عشق</p></div>
<div class="m2"><p>چکند بنده با خداوندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلخکامی کوه کن ببرد</p></div>
<div class="m2"><p>از تو شیرین دهن شکر خندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی و موی تو بد غرض که خدای</p></div>
<div class="m2"><p>بشب و روز خورد سوگندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید یعقوبت و زخاطر کرد</p></div>
<div class="m2"><p>که از او گم شده است فرزندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حال عقل و کشاکش عشقت</p></div>
<div class="m2"><p>جنگ دیوانه با خردمندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نام بردی زغیر از آن لب نوش</p></div>
<div class="m2"><p>بر بزخمم نمک پراکندی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند ای شیخ درس فقه و اصول</p></div>
<div class="m2"><p>درس عشقی بیا بخوان چندی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا چو آشفته بگسلی زجهان</p></div>
<div class="m2"><p>خویشتن را بدوست پیوندی</p></div></div>