---
title: >-
    شمارهٔ ۱۱۹۴
---
# شمارهٔ ۱۱۹۴

<div class="b" id="bn1"><div class="m1"><p>تا به کی بسمل خود را نگران میداری</p></div>
<div class="m2"><p>تیر در ترکش و پاس دگران میداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش ارباب هوس را زدل و دیده بشوی</p></div>
<div class="m2"><p>گر نظر جانب صاحب نظران میداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده در آینه ات تا بشفق مرغ سحر</p></div>
<div class="m2"><p>این بغوغا دگری جامه دران میداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیر سیم همگی آهن و رویست نهان</p></div>
<div class="m2"><p>چشم امید چه برسیم بران میداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ از پرده اسرار ندارد خبری</p></div>
<div class="m2"><p>به عبث گوش بر این بیخبران میداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا جام جهان بین شود انجام سفال</p></div>
<div class="m2"><p>گر باین دست گل کوزه گران میداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما بخود عاشق و شیدا و قلندر نشدیم</p></div>
<div class="m2"><p>باش ای عشق که ما را تو بدان میداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف وقت بصحرا و نیاید یعقوب</p></div>
<div class="m2"><p>چشم دیگر چه براه پسران میداری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمر بگذشت و گذر بر سرت ای سرو نکرد</p></div>
<div class="m2"><p>طمع آخر چه زعمر گذران میداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>توئی آئینه صاحب نظران چشم به تو</p></div>
<div class="m2"><p>آینه چند بر بی بصران میداری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دم زتوحید زن آشفته علی گوی علی</p></div>
<div class="m2"><p>تابکی چشم بسوی دگران میداری</p></div></div>