---
title: >-
    شمارهٔ ۲۰۷
---
# شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>فتنه کی از قد موزون تو چالاک‌تر است</p></div>
<div class="m2"><p>باده کی از لب نوش تو طربناک‌تر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه اهریمن جادوی بسی ناپاکست</p></div>
<div class="m2"><p>نتوان گفت ز جادوی تو ناپاک‌تر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه هر جامه که پوشی به بر تو زیباست</p></div>
<div class="m2"><p>کسوت ناز به بالای تو چالاک‌تر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه آلوده بود دامن ما بوالهوسان</p></div>
<div class="m2"><p>عشق را دامن تقدیس از این پاک‌تر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی ای برق جهان‌سوز بسوزم خاشاک</p></div>
<div class="m2"><p>نخل خشکیده ما از همه خاشاک‌تر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی آشفته اگر خاک شدی اکسیری</p></div>
<div class="m2"><p>وه که اکسیر به خاک در تو خاک‌تر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک راه علیم فخر کنم بر افلاک</p></div>
<div class="m2"><p>کآستان درش از نُه فلک افلاک‌تر است</p></div></div>