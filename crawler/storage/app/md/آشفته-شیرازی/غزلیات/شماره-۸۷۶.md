---
title: >-
    شمارهٔ ۸۷۶
---
# شمارهٔ ۸۷۶

<div class="b" id="bn1"><div class="m1"><p>ای آه خدا را سوی لیلا سفری کن</p></div>
<div class="m2"><p>او را زدل خسته مجنون خبری کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار حریفان دغا را تو در این کوی</p></div>
<div class="m2"><p>ای آن جهان سوز من امشب اثری کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هستی من گرد برانگیخت فراقت</p></div>
<div class="m2"><p>این تجربه یکچند بیا با دگری کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز میوه حرمان کس از این باغ نچیده</p></div>
<div class="m2"><p>ای نخل محبت بجز از این ثمری کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند شوی جلوه گه غیر خدا را</p></div>
<div class="m2"><p>ای آینه از آه دل ما حذری کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ترک چو گلگون بسر خاک بتازی</p></div>
<div class="m2"><p>بر خاک شهیدان زعنایت گذری کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه ار بتماشای گل و لاله خرامی</p></div>
<div class="m2"><p>بر حالت خونین کفنانت نظری کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بر سر پروانه کند شمع دمی صبح</p></div>
<div class="m2"><p>با ما تو هم ایشمع وفا تا سحری کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داری تو اگر شوق سر دار محبت</p></div>
<div class="m2"><p>منصور صفت در ره حق فکر سری کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیت الحزنی داری و کنعانی و چشمی</p></div>
<div class="m2"><p>یعقوب شور و ناله بیاد پسری کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرغ ار بپرد کس نکند صید به تیرش</p></div>
<div class="m2"><p>از بهر خود آشفته بیا فکر پری کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در مصر اگر چند عزیزی بر مردم</p></div>
<div class="m2"><p>ای یوسف گمگشته تو فکر پدری کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دردت نکند چاره بجز حیدر صفدر</p></div>
<div class="m2"><p>زین ملک سبک خیز و بکویش سفری کن</p></div></div>