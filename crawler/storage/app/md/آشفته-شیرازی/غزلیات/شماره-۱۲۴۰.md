---
title: >-
    شمارهٔ ۱۲۴۰
---
# شمارهٔ ۱۲۴۰

<div class="b" id="bn1"><div class="m1"><p>آفت دین و دل و فتنه هر مرد و زنی</p></div>
<div class="m2"><p>آوخ ای غمزه جادو که چه پرمکر و فنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبحه زاهد و زنار مغان هر دو گسست</p></div>
<div class="m2"><p>که نه زاهد دل و دین دارد و نه برهمنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کجا دعوی پرهیز کجا توبه کدام</p></div>
<div class="m2"><p>که اگر خاره شود توبه تواش میشکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بناگوش و خطت مشک و سمن شاید گفت</p></div>
<div class="m2"><p>هم اگر غالیه سایند به برگ سمنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بود رستم دستان که بود پابستت</p></div>
<div class="m2"><p>گر از آن زلف تواش رشته بگردون فکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الله الله که چه پیوسته ای ای سیل فراق</p></div>
<div class="m2"><p>که اگر کوه بود صبر زجایش بکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نظر وقف بر آن منظر زیبا کردم</p></div>
<div class="m2"><p>تا خلایق همه دانند تو منظور منی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حرف در جوهر فرد دهنت هیچ مگوی</p></div>
<div class="m2"><p>که در آن نکته موهوم نگنجد سخنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارم از لعل شکرخند اگر شیرین است</p></div>
<div class="m2"><p>در وفا داریش آشفته تو هم کوهکنی</p></div></div>