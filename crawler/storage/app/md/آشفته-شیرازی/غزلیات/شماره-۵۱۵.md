---
title: >-
    شمارهٔ ۵۱۵
---
# شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>آنان که بشمع تو پروانه صفت سوزند</p></div>
<div class="m2"><p>شاید که به هر محفل آتشکده افروزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخسار تو خورشید است زاغیار چه میپوشی</p></div>
<div class="m2"><p>کاینان همه خفاشند ناچار نظر دوزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشاق تو میسوزند در آتش و میسازند</p></div>
<div class="m2"><p>این بلبل و پروانه در عشق نو آموزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز کشته عاشق را برق تو نخواهد سوخت</p></div>
<div class="m2"><p>صد خرمن اگر در دشت زهاد براندوزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیا چه طمع دارند از وصل تو در فردا</p></div>
<div class="m2"><p>آنان که همه منکر در عشق تو امروزند</p></div></div>