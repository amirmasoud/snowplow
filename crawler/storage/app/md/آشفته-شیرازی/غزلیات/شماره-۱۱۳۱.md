---
title: >-
    شمارهٔ ۱۱۳۱
---
# شمارهٔ ۱۱۳۱

<div class="b" id="bn1"><div class="m1"><p>سرو چو نخل قامتت سر نزد چنین سهی</p></div>
<div class="m2"><p>نیست چو سیب غبغبت میوه خلد در بهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ همی نه از فسون رنگ بباخت بارهی</p></div>
<div class="m2"><p>شیر شکار میکند حیله او به روبهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایکه به روز و شب مرا در همه عمر همرهی</p></div>
<div class="m2"><p>بار بمنظر نظر از چه مرا نمیدهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون تو زسوز زخم دل از همه حال آگهی</p></div>
<div class="m2"><p>مشک مساز بوی مو با نفس سحرگهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آشفته خاک میکده چون تو ز کف نمی‌نهی</p></div>
<div class="m2"><p>غم نبود اگر مرا کیسه ز زر بود تهی</p></div></div>