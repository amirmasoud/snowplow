---
title: >-
    شمارهٔ ۱۱۳۵
---
# شمارهٔ ۱۱۳۵

<div class="b" id="bn1"><div class="m1"><p>سوختم کس نه دود دید و نه بوی</p></div>
<div class="m2"><p>پیش خامان حدیث پخته مگوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل شیرین زبانی دشمن</p></div>
<div class="m2"><p>نکنم گرچه دوست شد بدخوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبری ایشیخ بادیه فرسا</p></div>
<div class="m2"><p>خانه دل چو هست کعبه مجوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه در احتمال آب بمرد</p></div>
<div class="m2"><p>جاری آب فرات اندر جوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رندی و زهد و عشق و حکمت و دین</p></div>
<div class="m2"><p>آب و آتش شمار و سنگ و سبوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود درس عشق در دفتر</p></div>
<div class="m2"><p>گو بیا این ورق در آب بشوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی ای شهسوار عرصه حسن</p></div>
<div class="m2"><p>پیش چوگانت اوفتم چون گوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عارفان در سماع روحانی</p></div>
<div class="m2"><p>صوفیان در طلب بها یا هوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بکنی ذکر آن لب آشفته</p></div>
<div class="m2"><p>بذله های لطیف و نغز بگوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون نویسی مدیح حیدر را</p></div>
<div class="m2"><p>مشک از کلک تو بگیرد بوی</p></div></div>