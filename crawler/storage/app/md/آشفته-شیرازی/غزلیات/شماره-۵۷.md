---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>در دل و دیده ام ای دلبر جانانه بیا</p></div>
<div class="m2"><p>پی تشریف تو پاکست مرا خانه بیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوتی کرده ام از غیر و می ومطرب هست</p></div>
<div class="m2"><p>عذر یکسونه و بی پرده و رندانه بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر رقیب است تو را مانع دیدار حبیب</p></div>
<div class="m2"><p>رو خرابش بکن از یکدوسه پیمانه بیا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بکی صومعه و مسجد و سالوس وریا</p></div>
<div class="m2"><p>می صاف کش و مستانه بمیخانه بیا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرچم از عنبر تر بر سرمه زن از موی</p></div>
<div class="m2"><p>کله از سرنه و با تاج ملوکانه بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه شب گرد تو پروانه پرافشان در بزم</p></div>
<div class="m2"><p>شبی ای شمع پی پرستش پروانه بیا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میدهد وسوسه عقل و خرد رنج و صداع</p></div>
<div class="m2"><p>رفع کن درد سر از باده و مستانه بیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند خاموش نشینی تو چو صوفی غمناک</p></div>
<div class="m2"><p>بذله گو عیش کن ایشوخ و ظریفانه بیا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه آشفته ات ایدست خدا روسیه است</p></div>
<div class="m2"><p>تو ببالین وی از لطف کریمانه بیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو شه مملکت حسنی و در کشور دل</p></div>
<div class="m2"><p>با سپاه و حشم و کوکب شاهانه بیا</p></div></div>