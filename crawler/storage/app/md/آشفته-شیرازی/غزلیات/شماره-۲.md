---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>پرده برافتاد از جمال محمد</p></div>
<div class="m2"><p>شد زعلی ظاهر اعتدال محمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درخور اکملت دینکم چه عمل کرد</p></div>
<div class="m2"><p>شد بدو عالم عیان کمال محمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینکه طلب کار وصل شاهد غیبی</p></div>
<div class="m2"><p>گو بطلب در جهان وصال محمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم امکان و ماورای تو هم</p></div>
<div class="m2"><p>سایه نشینند در ظلال محمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدرنشین گشته عرش در شب معراج</p></div>
<div class="m2"><p>تا که جبین سود بر نعال محمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرص مه و خور که روشنی جهانند</p></div>
<div class="m2"><p>آینه دارند از جمال محمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فسحت امکان بود بجلوه او تنگ</p></div>
<div class="m2"><p>ساحت واجب بود مجال محمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه مجد و بزرگئی که شهانراست</p></div>
<div class="m2"><p>آمده یک پرتو از جلال محمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آیت وحدت بگوش خلق نخوردی</p></div>
<div class="m2"><p>گر نشنیدی کس از بلال محمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ دو پیکر که ذوالفقار علی بود</p></div>
<div class="m2"><p>بود چو ابری چون هلال محمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید اگر خانه خداش بخوانی</p></div>
<div class="m2"><p>رخنه بد گر کند خیال محمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همچو خضر جاودان زعمر خورد بر</p></div>
<div class="m2"><p>هر که کشد دردی از زلال محمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر کنی آشفته میل مدح سرائی</p></div>
<div class="m2"><p>مدح محمد بگوی و آل محمد</p></div></div>