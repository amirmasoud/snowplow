---
title: >-
    شمارهٔ ۷۶۴
---
# شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>آیا که داده فتوی بر بی گناهیم</p></div>
<div class="m2"><p>کز خون من گذشته ترک سپاهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خود دهم بفتویخونم تو را سند</p></div>
<div class="m2"><p>گر وحشتت بدل بود از دادخواهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونم چو ریختی سرانگشت خود بشوی</p></div>
<div class="m2"><p>تا ناخن خضاب تو ندهد گواهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چه مرا به بند نبندی که وحشیم</p></div>
<div class="m2"><p>از چه مرا بشست نگیری که ماهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من برندارم از سر کوی تو سر به تیغ</p></div>
<div class="m2"><p>بر سر اگر نهی کله پادشاهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مغرور ابروی چو کمانی و بی خبر</p></div>
<div class="m2"><p>از تیر آه نیم شب و صبحگاهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته بود بلبل باغت بگوز چیست</p></div>
<div class="m2"><p>ای گل خدای را که چنین خار خواهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من میگریزم از تو بخاک در نجف</p></div>
<div class="m2"><p>باشد مکان به سایه لطف الهیم</p></div></div>