---
title: >-
    شمارهٔ ۱۱۶۱
---
# شمارهٔ ۱۱۶۱

<div class="b" id="bn1"><div class="m1"><p>ای جنت جاوید ز رخسار تو بابی</p></div>
<div class="m2"><p>دلداری و خوبی ز صفات تو کتابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلبرگ چمن آب ز رخسار تو بگرفت</p></div>
<div class="m2"><p>سنبل خورد از حلقه گیسوی تو تابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نرگس بیمار بخوانی تو و سحرت</p></div>
<div class="m2"><p>نگذاشته در دیده مستان تو خوابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل زد ز مژه چشمک و جویای لب توست</p></div>
<div class="m2"><p>جوید نمکی مست چو شد پخته کبابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سر هوس کوثر و تسنیم ندارد</p></div>
<div class="m2"><p>هر کس خورد از میکده عشق شرابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>احباب به جز وصل نخواهند بهشتی</p></div>
<div class="m2"><p>عشاق به جز هجر ندارند عذابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب بر لب من نه تو پس از مرگ که چون نی</p></div>
<div class="m2"><p>هر بند من از عشق تو گوید به جوابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز زهد گنه هیچ ندیدیم و نکردیم</p></div>
<div class="m2"><p>جز عشق بتان هیچ در آفاق ثوابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این شعر تر از خامه به دفتر به چه ماند</p></div>
<div class="m2"><p>کاندر چمن از ابر سیه میچکد آبی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روزی بنمائی به دل شب زخم زلف</p></div>
<div class="m2"><p>گر نیم‌شبان برکشی از چهره نقابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز مدح علی هیچ نگفتیم و نگوئیم</p></div>
<div class="m2"><p>جز نام علی چون نشنیدیم جوابی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشفته فراتش به نظر موج سرابست</p></div>
<div class="m2"><p>آن را که به سر میرود از عشق تو آبی</p></div></div>