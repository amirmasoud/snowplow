---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>وه که از ما جز گنه بر می نیاید هیچکار</p></div>
<div class="m2"><p>نفس سرکش کرد صرف خودپرستی روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه احصامی نشاید کرد عصیان مرا</p></div>
<div class="m2"><p>در شمار اما نیاید پیش عفو کردگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاصر آمد چون زبان از شکر آلای الله</p></div>
<div class="m2"><p>عجز باید پیش شکر نعمت پروردگار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل طاعت بودم و تقوی و پرهیز ایدریغ</p></div>
<div class="m2"><p>عقل شد مغلوب نفس شوم ناپرهیزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست دردم زآتش دوزخ بپاداش عمل</p></div>
<div class="m2"><p>دردم این باشد که هستم زاهل محشر شرمسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیک باامید فضل و رحمت و احسان حق</p></div>
<div class="m2"><p>مینهم بر دوش جان بار گناه صد هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توبه میفرمایدم هر روزه عقل متقی</p></div>
<div class="m2"><p>لیک تا عشقم بود کی توبه ماند برقرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا برون آری زتاریکی نفسم از کرم</p></div>
<div class="m2"><p>آفتابی از شبستان امید من برار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست اندر دفتر اعمال من جز سیئات</p></div>
<div class="m2"><p>حرف خرجم چیست تا باشم بدین امیدوار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز ولای مرتضی و الله ما را هیچ نیست</p></div>
<div class="m2"><p>درگذر آشفته و او را به حیدر واگذار</p></div></div>