---
title: >-
    شمارهٔ ۸۸۴
---
# شمارهٔ ۸۸۴

<div class="b" id="bn1"><div class="m1"><p>چه‌ای ای عشق که دیدار تو نتوان دیدن</p></div>
<div class="m2"><p>وصل چون نیست بسازیم به هجران دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من همان روز که دل شد گرو مهر بتان</p></div>
<div class="m2"><p>شدم آماده بجان داغ فراوان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایدل از سر سر زلف بتانت چه گشود</p></div>
<div class="m2"><p>بجز از سلسله ها بی سر و سامان دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم آن تاب ندارد که ببیند رخ تو</p></div>
<div class="m2"><p>راستی چشمه خورشیدی و نتوان دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر جوید قد موزن تو از چشم ترم</p></div>
<div class="m2"><p>بر لب جوی تو آن سرو خرامان دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چو مستسقیم ای خضر علاجم چه بود</p></div>
<div class="m2"><p>که عطش خیزدم از چشمه حیوان دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناگزیر است دل از صدمه آنزلف سیاه</p></div>
<div class="m2"><p>گوی لابد بود از لطمه چوگان دیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز توکل که بود راهبر باغ خلیل</p></div>
<div class="m2"><p>خوش در آتش شدن و لاله و ریحان دیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چاره هجر چه و وصل کدام است بگوی</p></div>
<div class="m2"><p>دل و دین و سرو جان دادن و جانان دیدن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا خیال خم زلفت همه شب همره اوست</p></div>
<div class="m2"><p>رهد آشفته از این خواب پریشان دیدن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اصل ایمان علی آن آینه مظهر حق</p></div>
<div class="m2"><p>که توان در رخ او شاهد پنهان دیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دایره وار بگرد در میخانه بگرد</p></div>
<div class="m2"><p>تا توانی اثر از مرکز ایمان دیدن</p></div></div>