---
title: >-
    شمارهٔ ۱۰۶۱
---
# شمارهٔ ۱۰۶۱

<div class="b" id="bn1"><div class="m1"><p>تا که به مصر نیکوئی سکه زدی به دلبری</p></div>
<div class="m2"><p>یوسف مصریت ز جان بسته میان به چاکری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوبت سلطنت بود دعوی معجزه بکن</p></div>
<div class="m2"><p>ختم به توست نیکوئی چون به نبی پیمبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غم سیم طلعتت اشک چو زر بود مرا</p></div>
<div class="m2"><p>ما و تو هر دو را سزد دم زدن از توانگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطرب خوش‌نوا بخوان شاهد کشمری بچم</p></div>
<div class="m2"><p>ساقی پارسی بده باده صاف خلری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهر چو گنج شایگان جان ببری به رایگان</p></div>
<div class="m2"><p>چونکه به دوش افکنی طره چو مار چمبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنده شوند از طرب کرده کفن به تن قبا</p></div>
<div class="m2"><p>گر تو به خاک کُشتگان همچو مسیح بگذری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منع نظر ز منظرت عاشق خسته را مکن</p></div>
<div class="m2"><p>تا که ز باغ حسن خود در همه عمر برخوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آدمیان و وحشیان جمله اسیر بند تو</p></div>
<div class="m2"><p>زیبدت ار به جادوئی دل ببری تو از پری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه‌کنان به بتکده، ای بت سیمتن بیا</p></div>
<div class="m2"><p>تا چو خلیل بشکنی جمله بتان آذری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکوه مکن ز چشم او گر همه ریخت خون دل</p></div>
<div class="m2"><p>آشفته خون خور افتد ترک چو گشت لشگری</p></div></div>