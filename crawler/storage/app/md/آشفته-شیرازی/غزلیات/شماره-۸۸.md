---
title: >-
    شمارهٔ ۸۸
---
# شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>این چه غوغاست که در چنگ و ربابست امشب</p></div>
<div class="m2"><p>وین چه مستی است که در جام شرابست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده بی پرده بده ساقی مستان و مترس</p></div>
<div class="m2"><p>محتسب نیز چو مامست و خرابست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی ای نرگس جادو و بکف سیخ مژه</p></div>
<div class="m2"><p>بر سر سیخ تو دلهای کبابست امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نعمت وصل بکف شکوه هجران بر لب</p></div>
<div class="m2"><p>این شب قدر یا روز حسابست امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بتعجیل سر و جان به نثار آوردم</p></div>
<div class="m2"><p>بهر قتل دگرانت چه شتابست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باده ی از خم اغیار فکندی بقدح</p></div>
<div class="m2"><p>خون عشاق از این درد چو ابست امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب از پرده عشاق نوا سر کن راست</p></div>
<div class="m2"><p>کاز مخالف دل ما در تب و تابست امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چه بحر استت که مستغرق او را بنظر</p></div>
<div class="m2"><p>هفت دریا چو یکی موج سرابست امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبرم منت ساقی نکشم زنج خمار</p></div>
<div class="m2"><p>زآن لب لعل بکامم می نابست امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهد مست و می و مطرب و چنگ آشفته</p></div>
<div class="m2"><p>باز پیرانه سرت عهد شبابست امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می خور و مدح علی گوی و برافشان سرودست</p></div>
<div class="m2"><p>فتنه را تا که دمی دیده به خوابست امشب</p></div></div>