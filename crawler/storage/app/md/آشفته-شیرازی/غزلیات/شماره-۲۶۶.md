---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>بمددکاری عشقت زهوس شاید رست</p></div>
<div class="m2"><p>عشق هر جا بدرون شد در شهوت بربست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل را بار ندادند بخلوتگه عشق</p></div>
<div class="m2"><p>کی بود بار گدا را چو شهنشاه نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب رندی و هوسناکی عشاق مکن</p></div>
<div class="m2"><p>کاین مجازیست که لابد بحقیقت پیوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه گمانم که در آفاق پذیرد مرهم</p></div>
<div class="m2"><p>ناوک تیر نظر در دل هر کس بشکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طایر بی پر و بال دل و رستن از دام</p></div>
<div class="m2"><p>وه که جبریل باین بال از این دام نجست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم مگر بوسه نهد ریش درون را مرهم</p></div>
<div class="m2"><p>نمک قند لبت چون دل مجروح بخست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو اگر زاهد خود بینی و گر عارف حق</p></div>
<div class="m2"><p>لاجرم هیچکس از طعنه اغیار نرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو پرگار بسر میدوم از هر طرفی</p></div>
<div class="m2"><p>اختیاری چو در این دایره ام نیست بدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبرد منت ساقی بهمه دور حیات</p></div>
<div class="m2"><p>هر که آشفته کشد جرعه ای از جام الست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در حرم خانه دل نور علی همچو خلیل</p></div>
<div class="m2"><p>هر کجا نقش بتی دید سرا پا بشکست</p></div></div>