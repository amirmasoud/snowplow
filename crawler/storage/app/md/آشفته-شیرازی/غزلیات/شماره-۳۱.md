---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>عشق بعقل بارها کرده کارزارها ‏</p></div>
<div class="m2"><p>کرده از او فرارها داده باو قرارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل چو بختی اشتران عشق بر اوست ساربان</p></div>
<div class="m2"><p>برده از او قطارها کره از او مهارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غم آن عقیق لب از دل و دیده روز و شب</p></div>
<div class="m2"><p>روید لاله زارها جوشد چشمه سارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه چه لب آن عقیقها و چه رخان شقیقها</p></div>
<div class="m2"><p>برده از آن نگارها کرده از آن بهارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رخ ماه پارها باغ پر از ستارها</p></div>
<div class="m2"><p>چون قد گلعذارها سرو جویبارها</p></div></div>