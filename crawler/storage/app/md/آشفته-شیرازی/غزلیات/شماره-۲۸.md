---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>ساقی بده آن جوهر یاقوت نسب را</p></div>
<div class="m2"><p>مطرب بنواراست کن آن ساز طرب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افسر دگر از سردی خون برگ و پی</p></div>
<div class="m2"><p>گرمی بده از آتش سیاله عصب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن آب که از کوثر و تسنیم بر درنگ</p></div>
<div class="m2"><p>بر دوزخیان باز نشان نار غصب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوروز بزرگی بزن از پرده عشاق</p></div>
<div class="m2"><p>نوروز کن از لحن عرب ماه رجب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حوران بهشتی را بر بند تو زیور</p></div>
<div class="m2"><p>بر قامت غلمان کن ززنار سلب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پا بست بهر خاربنی چند در این باغ</p></div>
<div class="m2"><p>آن نخله مریم ببر آور در طب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امروز برآمد زحرم مظهر بیچون</p></div>
<div class="m2"><p>امروز بود میلاد آن میر عرب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز عیان کرد رخ آنشاهد غیبی</p></div>
<div class="m2"><p>بر کون و مکان کرد عیان سر عجب را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد خانه خدا ظاهر و بنشست بکعبه</p></div>
<div class="m2"><p>شد روز که دیگر نکنی قصه شب را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان دست خدا آمد تابت نگذارد</p></div>
<div class="m2"><p>بر بام حرم نصب کند رایت رب را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این روز طرب خیز بود نوبت شادی</p></div>
<div class="m2"><p>آشفته وداعی بکن آن رنج و تعب را</p></div></div>