---
title: >-
    شمارهٔ ۱۱۸۴
---
# شمارهٔ ۱۱۸۴

<div class="b" id="bn1"><div class="m1"><p>ای آهوی تتاری نافه اگر نداری</p></div>
<div class="m2"><p>زآن مو چرا نگیری زآن بو چرا نیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آب چشم عشاق رو وام کن دو قطره</p></div>
<div class="m2"><p>ای ابر نوبهاری باران اگر نداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خارخار عشقت در دل اگر اثر هست</p></div>
<div class="m2"><p>در روز تیرباران شاید که سر نخاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محصولت ار بباید تخم عمل بیفشان</p></div>
<div class="m2"><p>فردا شوی پشیمان امروز اگر نکاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سوختن عجب نیست نه پرده فلک را</p></div>
<div class="m2"><p>آهی اگر سحرگه از سوز دل برآری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما کشته تخم امید در رهگذار باران</p></div>
<div class="m2"><p>تو ابر نوبهاری بر ما چرا نباری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته عاشقانت از خود نمیشمارند</p></div>
<div class="m2"><p>چون ابر اگر نباری یا همچو نی نزاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطرب چو هست روحت ساقی چو هست راحت</p></div>
<div class="m2"><p>حسنی چرا نسازی جامی چرا نیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریوزه بایدت کردت از همرهان در این ره</p></div>
<div class="m2"><p>از حب آل حیدر گر توشه برنداری</p></div></div>