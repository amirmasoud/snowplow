---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>دیوانه ای که از تو در او وجد و حالتست</p></div>
<div class="m2"><p>گر حرف کفر گفت نه جای ضلالتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر فتوی حکیم بده می ببانگ چنگ</p></div>
<div class="m2"><p>زاهد که منع باده کند از جهالتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیغمبران بخلق کنند ار رسالتی</p></div>
<div class="m2"><p>جبریل عشق را برسولان رسالتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنگ ملال زآینه دل بشو زمی</p></div>
<div class="m2"><p>کائینه خانه را نه محل ملالتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرد رهت فکنده و ایستاده منفعل</p></div>
<div class="m2"><p>سر بر نیاورم که مقام خجالتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفی بوجد و حال نیابد سماع عشق</p></div>
<div class="m2"><p>حالات عشق را نه مقام و مقالتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درندگان دشت پرستاریش کنند</p></div>
<div class="m2"><p>لیلا گرش بحالت مجنون کفالتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشقت پی خرابی دل میکشد سپاه</p></div>
<div class="m2"><p>شه را بملک خود نظر استمالتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته پا زسلسله زلف او مکش</p></div>
<div class="m2"><p>عمری که صرف عشق نگردد بطالتست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهدی بکن که خاک شوی بر در مغان</p></div>
<div class="m2"><p>کاکسیر را بمس اثر استحالتست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه نجف امیر عرب خسرو عجم</p></div>
<div class="m2"><p>کاسلام را ز تیغ کج او دلالتست</p></div></div>