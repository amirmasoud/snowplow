---
title: >-
    شمارهٔ ۱۱۳۴
---
# شمارهٔ ۱۱۳۴

<div class="b" id="bn1"><div class="m1"><p>وقت آنست که پاکوبی و می نوش کنی</p></div>
<div class="m2"><p>شادی آری و غم رفته فراموش کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامه عاریت سلطنت از تن بنهی</p></div>
<div class="m2"><p>کسوت فقر از زیب برو دوش کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفی آسا بسماع آئی چون اشتر مست</p></div>
<div class="m2"><p>خم صفت کف بلب آری و بسی جوش کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکدو جرعه بکش از آن قدح هوش زدا</p></div>
<div class="m2"><p>تا که خود را زقدح سرخوش و مدهوش کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایگل ار پرده کشی از رخ و آواز کشی</p></div>
<div class="m2"><p>پرده گل بدری بلبل خاموش کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه ای عشق ندانم که چو زنبورعسل</p></div>
<div class="m2"><p>با همه نیش بکامم اثر نوش کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست در سلسله زلف دلاویزش کن</p></div>
<div class="m2"><p>تا بزنجیر جنون دست در آغوش کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بکف آری بدمی معرفت مائی را</p></div>
<div class="m2"><p>اگر این زمزمه از نی بنوا گوش کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز را نیست خطر از شب یلدا چندان</p></div>
<div class="m2"><p>که تو از زلف بر آن صبح بناگوش کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو آئینه صافیت نماید رخ دوست</p></div>
<div class="m2"><p>گربهر سنگ و گلی خود نظر از هوش کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید آشفته که سر حلقه شوی رندانرا</p></div>
<div class="m2"><p>حلقه بندگی شاه چو در گوش کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ذره ی مهر علی کرده سبکبار تو را</p></div>
<div class="m2"><p>گنه خلق جهان گر همه بر دوش کنی</p></div></div>