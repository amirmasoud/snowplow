---
title: >-
    شمارهٔ ۱۰۸۷
---
# شمارهٔ ۱۰۸۷

<div class="b" id="bn1"><div class="m1"><p>اوقات خوش کدام است ایام عشقبازی</p></div>
<div class="m2"><p>مشغول دوست بودن وز غیر بی نیازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشم بگوش میخواند نی نکته حقیقت</p></div>
<div class="m2"><p>مطرب بیا بگردان این پرده مجازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حرف حق میندیش گر میزند بدارت</p></div>
<div class="m2"><p>خواهی اگر چو منصور در عشق سرفرازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فخر تو درس عشق است نه صرف و نحو و حکمت</p></div>
<div class="m2"><p>گیرم بعلم باشی برتر زفخر رازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ترک غمزه تو زابرو اشارتی کرد</p></div>
<div class="m2"><p>ترکان بپارس کردند آغاز ترکتازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکجام می بگردش کار جهان بسازد</p></div>
<div class="m2"><p>ناید زدور گردون اینگونه کارسازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل که عاشق آمد بر سنگ و گل بنالد</p></div>
<div class="m2"><p>بر پنج روز حسنت ایگل تو چند نازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته شیخ مغرور از کعبه حجیز است</p></div>
<div class="m2"><p>من در عراق جستم شاهنشه حجازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرآت جلوه حق معنی عشق مطلق</p></div>
<div class="m2"><p>فرمان روای بر حق حیدر امیر غازی</p></div></div>