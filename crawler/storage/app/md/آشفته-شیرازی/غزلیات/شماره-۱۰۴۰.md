---
title: >-
    شمارهٔ ۱۰۴۰
---
# شمارهٔ ۱۰۴۰

<div class="b" id="bn1"><div class="m1"><p>بر این جا آن بلا بالا گرفته</p></div>
<div class="m2"><p>بلا اندر زمین بالا گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حذر کن شهسوار از آب چشمم</p></div>
<div class="m2"><p>که سیلش کوه تا صحرا گرفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر نه مردم آبی است چشمم</p></div>
<div class="m2"><p>نشیمن از چه در دریا گرفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمخموری چشمت شب خبر داشت</p></div>
<div class="m2"><p>که بر کف ساغر صهبا گرفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو پروانه زجانش نیست پرا</p></div>
<div class="m2"><p>کسی کاو یاربی پروا گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زتو خورشید کسب نور کرده</p></div>
<div class="m2"><p>زمن یاد این هنر حربا گرفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از این سوز نهانی شمع آسا</p></div>
<div class="m2"><p>مرا آتش زسر تا پا گرفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زسنگ خاره سیم آرند بیرون</p></div>
<div class="m2"><p>بسیمت جا چرا خارا گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عبث مجنون بحی لیلا چه جوئی</p></div>
<div class="m2"><p>که کوه و دشت را لیلا گرفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بخون کیست چشمت داده فتوی</p></div>
<div class="m2"><p>که رخسارت زخط طغرا گرفته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسودای سر زلف تو سر داد</p></div>
<div class="m2"><p>زسر آشفته این سودا گرفته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخاک آستان طوس منزل</p></div>
<div class="m2"><p>میان عقبی و دنیا گرفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دستی می‌زند بر سر ز حسرت</p></div>
<div class="m2"><p>به دستی دامن مولا گرفته</p></div></div>