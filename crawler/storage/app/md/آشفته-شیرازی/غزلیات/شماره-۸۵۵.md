---
title: >-
    شمارهٔ ۸۵۵
---
# شمارهٔ ۸۵۵

<div class="b" id="bn1"><div class="m1"><p>همرهان همتی که نوسفرم</p></div>
<div class="m2"><p>رهروان مژده ای که بیخبرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو یوسف فتاده ام در چاه</p></div>
<div class="m2"><p>گو به یعقوب تا رسد پسرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش چو نی ایدل مجروح</p></div>
<div class="m2"><p>گفت بستر بود زمشک ترم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون توانم گریختن از برق</p></div>
<div class="m2"><p>منکه در آشیان بود شررم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس سرکش چو آتش و من نی</p></div>
<div class="m2"><p>چون نسوزم که شعله را ببرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیلم اندر قفا و من خاشاک</p></div>
<div class="m2"><p>کی بجا ماند از وجود اثرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشته نخلم پی رطب دهقان</p></div>
<div class="m2"><p>حیرتم کاز چه مقل شد ثمرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیستم شبنمی برابر مهر</p></div>
<div class="m2"><p>یا کتان در مقابل قمرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست هر سو بزه خدنگ قضا</p></div>
<div class="m2"><p>نیست جز عشق در جهان سپرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من نظر برنگیرمت از چشم</p></div>
<div class="m2"><p>مژه چون تیر دوزد از نظرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرچه پرتم کند رقیب از کوه</p></div>
<div class="m2"><p>وه که با دوست دست در کمرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر حب مرتضی دارم</p></div>
<div class="m2"><p>تا نه پنداریم که بدگهرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاید آشفته ره برم بحرم</p></div>
<div class="m2"><p>ره این بادیه که می سپرم</p></div></div>