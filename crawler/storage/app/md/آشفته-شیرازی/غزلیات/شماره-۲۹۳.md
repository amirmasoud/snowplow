---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>از برم آن سرو خرامان گذشت</p></div>
<div class="m2"><p>برق یمانی به نیستان گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بجهان هجر و وصالی بود</p></div>
<div class="m2"><p>ود که مرا عمر به هجران گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوهری عقل چو لعل تو دید</p></div>
<div class="m2"><p>از هوس لعل بدخشان گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز چرا منتظر رحمتم</p></div>
<div class="m2"><p>کز برم آن شاهد غضبان گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردمم چشمم چونم اشک دید</p></div>
<div class="m2"><p>گفت که بر نوح چه طوفان گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه من اندر دل او کرد اثر</p></div>
<div class="m2"><p>تیر نظر بین که زسندان گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوز درونم بسر آمد چو شمع</p></div>
<div class="m2"><p>آتش پنهان زگریبان گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر ما تا که بدرمان کیست</p></div>
<div class="m2"><p>کاین همه امواج زدامان گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کعبه عشق تو فدا از که خواست</p></div>
<div class="m2"><p>تا که خلیل از سر قربان گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که پریشانی زلف تو دید</p></div>
<div class="m2"><p>این دل آشفته زسامان گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آدمئی کو بنجف راه یافت</p></div>
<div class="m2"><p>چون پدر از جنت رضوان گذشت</p></div></div>