---
title: >-
    شمارهٔ ۹۳۵
---
# شمارهٔ ۹۳۵

<div class="b" id="bn1"><div class="m1"><p>ساقی شراب مجلسیان در پیاله کن</p></div>
<div class="m2"><p>ما را به لعل باده فروشت حواله کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه زسر باده پرستی اگر نه ای</p></div>
<div class="m2"><p>لبریز شد چو جام نظر در پیاله کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید می بماه قدح ریز و فیض بخش</p></div>
<div class="m2"><p>خوان جهان زپرتو او پر نواله کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب چهره خوی افشان بصحن باغ</p></div>
<div class="m2"><p>دامن پر از ستاره چمن پر زلاله کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این عمر رفته را بدل از جام باده گیر</p></div>
<div class="m2"><p>چل ساله دفع غم زشراب دوساله کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راز کمند عشق کند عقل سرکشی</p></div>
<div class="m2"><p>زنجیریش زطره مشکین کلاله کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطرب بزن به پرده قانون نوای راست</p></div>
<div class="m2"><p>از شور عشق گوش فلک پر زناله کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدح علی بگوی مغنی بصوت خوش</p></div>
<div class="m2"><p>باغ بهشت را بدو بیتی قباله کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته گر کسی زتو پرسد نشان او</p></div>
<div class="m2"><p>تعبیر نام دوست به لفظ جلاله کن</p></div></div>