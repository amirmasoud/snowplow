---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>بی شایبه زنگ ازدل دیدار تو بزداید</p></div>
<div class="m2"><p>یوسف چو بمصر آید بازار بیاراید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان قیمت بوسش بود عشاق فزون کردند</p></div>
<div class="m2"><p>چون مشتری افزون شد بر نرخ بیفزاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دل که بود عطشان از شوق لب نوشت</p></div>
<div class="m2"><p>از خوردن آب خضر حاشا که بیاساید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من عاشق سودائی تو شاهد هر جائی</p></div>
<div class="m2"><p>دلدار چنان زیبد دلداده چنین باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن گوهر نایابی کز لعل شکر ریزی</p></div>
<div class="m2"><p>حاشا که چنین لؤلؤ از بحر برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد تو نسیم صبح من غنچه بستانم</p></div>
<div class="m2"><p>بازآ که دل تنگم از بوی تو بگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد غالیه بو خورشید مه مشک فشان گردید</p></div>
<div class="m2"><p>زلفت بمه و خورشید تا غالیه می ساید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسل تو بود زآدم یا زاده حورائی</p></div>
<div class="m2"><p>آدم که بیارد حور حوری که پری زاید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شور لب شیرینت سابد نمکم بر دل</p></div>
<div class="m2"><p>مجروحم اگر حرفم گاهی بزبان آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در سینه دل سختت چون آهن سیم اندود</p></div>
<div class="m2"><p>زآهن بجز از سختی هرگز که نمی آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>افکند زرخ پرد دل برد نهان شد باز</p></div>
<div class="m2"><p>آن شوخ پریزاده بنماید و برباید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با داغ تو آشفته می سوزد و می سازد</p></div>
<div class="m2"><p>شاید که بر او روزی لعل تو ببخشاید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تابد بهمه آفاق چون مهر کند اشراق</p></div>
<div class="m2"><p>کس چشمه خور با گل هر چند بینداید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ذرات جهان یکسر خورشید بود حیدر</p></div>
<div class="m2"><p>خفاش بود منکر زان نفی تو بنماید</p></div></div>