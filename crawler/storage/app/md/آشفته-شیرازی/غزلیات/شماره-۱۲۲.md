---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>زینهار از دو ترک خونخوارت</p></div>
<div class="m2"><p>حذر از عقربان جرارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داده خاتم لبت بزنهارم</p></div>
<div class="m2"><p>زآنسیه مست ترک خونخوارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآه درویش چون نیندیشی</p></div>
<div class="m2"><p>توئی آئینه آه زنگارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چه میخانه ی تو ای ساقی</p></div>
<div class="m2"><p>که پرستند مست و هشیارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو خود را بیارم ای قمری</p></div>
<div class="m2"><p>تا که سازم زسرو بیزارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو شیرین پسر بمصر آئی</p></div>
<div class="m2"><p>یوسف از جان شود خریدارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخرم سحر جادوی بابل</p></div>
<div class="m2"><p>در بر چشمکان سحارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو که شب خفته ی ببستر ناز</p></div>
<div class="m2"><p>چه غم از عاشقان بیدارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر خود بینیم که حاجب تست</p></div>
<div class="m2"><p>کس نه بینم که باشد اغیارت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برفکن بار نفس را از دوش</p></div>
<div class="m2"><p>تا بمنزل برد سبک بارت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فارغ از قید این و آن باشد</p></div>
<div class="m2"><p>هر که چون من شود گرفتارت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساقیا می حلال خواهد خورد</p></div>
<div class="m2"><p>هر که در جام دید دیدارت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زاهد و برهمن چو آشفته ‏</p></div>
<div class="m2"><p>گرد دیر و حرم طلبکارت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پرده دل بود مقام علی</p></div>
<div class="m2"><p>رو بدر پرده های پندارت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نافه چین به خون خورد غوطه ‏</p></div>
<div class="m2"><p>در بر زلفکان عطارت</p></div></div>