---
title: >-
    شمارهٔ ۵۷۰
---
# شمارهٔ ۵۷۰

<div class="b" id="bn1"><div class="m1"><p>عقل را از جنون بنه زنجیر</p></div>
<div class="m2"><p>که نماید کفایت از تدبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باری ای عشق چون خراب توئیم</p></div>
<div class="m2"><p>چشم داریم هم زتو تعمیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس رضا ده دلا بحکم قضا</p></div>
<div class="m2"><p>بسته بند را چه جای گزیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گو بساقی که باده پیماید</p></div>
<div class="m2"><p>تا بشوید زلوح دل تزویر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای مصور ببین بصورت دوست</p></div>
<div class="m2"><p>نقش مانی چه میکنی تصویر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از من ای کعبه گر خطائی رفت</p></div>
<div class="m2"><p>حاجی البته میکند تقصیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که شد مست از می عشقت</p></div>
<div class="m2"><p>باده در او کجا کند تأثیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باده دانی چه خاصیت دارد</p></div>
<div class="m2"><p>فاش سازد بدوست سر ضمیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشقان را شراب الفت بس</p></div>
<div class="m2"><p>می بمعشوق ده بهر تقدیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظم آشفته گر پریشان است</p></div>
<div class="m2"><p>شرح زلف تو میکند تفسیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه گویند وصف پادشهان</p></div>
<div class="m2"><p>من بمدح امیر کل امیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز بدست خدا گشایش نیست</p></div>
<div class="m2"><p>ایکه هستی بدام نفس اسیر</p></div></div>