---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>آفرینش را گروهی چار گوهر گفته اند</p></div>
<div class="m2"><p>قوم دیگر هفت ابا چار مادر گفته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان زین چار بیرون گوهری جسته لطیف</p></div>
<div class="m2"><p>اخترش را برتر از این هفت اختر گفته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگرانش علت غائی شمارند و سبب</p></div>
<div class="m2"><p>اهل حقش مرتبت از این فزونتر گفته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست دانی عشق اقدس کز نخستین جلوه اش</p></div>
<div class="m2"><p>قدسیان تسبیح و تهلیل مکرر گفته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تالی عقل نخستین صادر دوم علی</p></div>
<div class="m2"><p>کش یدالله خوانده اند او را و حیدر گفته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غالی اندر حق او جمعی و برخی قالی اند</p></div>
<div class="m2"><p>حق بحق او خداوند و پمبر گفته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچکس نشناخت او را جز خدا و مصطفی</p></div>
<div class="m2"><p>وصف او را این دو بر معیار و در خور گفته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در میان واجب و ممکن بود ربطی عجب</p></div>
<div class="m2"><p>بهر حقش آینه خوانند و مظهر گفته اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صد هزار آشفته لال اندر مدیح حضرتش</p></div>
<div class="m2"><p>زآنکه مدح او خدا و هم پیمبر گفته اند</p></div></div>