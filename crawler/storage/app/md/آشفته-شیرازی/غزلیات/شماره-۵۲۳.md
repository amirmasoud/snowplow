---
title: >-
    شمارهٔ ۵۲۳
---
# شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>یاد آن نوشین دهانم کام شیرین می‌کند</p></div>
<div class="m2"><p>می‌کشان را یاد باده بزم رنگین می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنجر خونریز جلادان نکرده با کسی</p></div>
<div class="m2"><p>آنچه با من ساعد و دست نگارین می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخ‌کامی کی بماند در لحد فرهاد را</p></div>
<div class="m2"><p>گر کسش تلقین به بالین نام شیرین می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چین اگر مشکین بود از ناف آهویی و بس</p></div>
<div class="m2"><p>چین یک مویت همه آفاق مشکین می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار تو با یار تو اندر میان دارد مهم</p></div>
<div class="m2"><p>حاش لله یاد اگر از یار پارین می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حبذا نقش بدیعی کز صفای منظرش</p></div>
<div class="m2"><p>نقشبند صنع بر خود فخر و تحسین می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنجر مژگان بود کافی به قتل عاشقان</p></div>
<div class="m2"><p>رنجه بر قتلم چرا او دست سیمین می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاید ار بخشد گنه آشفته را دارای حشر</p></div>
<div class="m2"><p>کاو مدیح مرتضی آن داور دین می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دولت شه را دعا باشد سزا صبح و مسا</p></div>
<div class="m2"><p>این دعا را چون ملک پیوسته آمین می‌کند</p></div></div>