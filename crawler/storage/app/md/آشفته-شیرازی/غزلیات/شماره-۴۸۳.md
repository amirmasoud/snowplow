---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>مگر که شهر دگر باز در نظر دارد</p></div>
<div class="m2"><p>کز این دیار مه من سر سفر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشود مملکت پارس را به نیم الارض</p></div>
<div class="m2"><p>بفتح ترک و عرب تا چه در نظر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو روز نیست فزون تر بمنزلی ساکن</p></div>
<div class="m2"><p>مهم به تندروی حالت قمر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر زحال دلم داد اشک خونینم</p></div>
<div class="m2"><p>که تازه آمده از خانه و خبر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر آن دعا که پی ماندنش رقم کردم</p></div>
<div class="m2"><p>ببین زبخت بدم واژگون اثر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ثمر بغیر دهد نونهال نوسفرم</p></div>
<div class="m2"><p>دریغ نخل محبت که این ثمر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را که خرمن حسنست در کمال نصاب</p></div>
<div class="m2"><p>زخوشه چین گدائی کجا ضرر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه گلشن حسن تو را خزانی نیست</p></div>
<div class="m2"><p>زبرق آه سحرگاهی صد شرر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو سنگدل که زآتش نمیکنی پرهیز</p></div>
<div class="m2"><p>بترس زآتس آهی که در جگر دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکن رعایت درویش گرچه سلطانی</p></div>
<div class="m2"><p>کز آه مور سلیمان بسی حذر دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکن بحلقه گیسوی خویشتن رحمی</p></div>
<div class="m2"><p>کز آه سینه آشفته بس خطر دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکن رعایت درویش خویش ای سلطان</p></div>
<div class="m2"><p>که او زمهر علی کسوتی ببر دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر رقیب بود حیله ساز چون روباه</p></div>
<div class="m2"><p>مگو بسوز که این بیشه شیر نر دارد</p></div></div>