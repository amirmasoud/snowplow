---
title: >-
    شمارهٔ ۸۷۷
---
# شمارهٔ ۸۷۷

<div class="b" id="bn1"><div class="m1"><p>ای مار زلفین سیه ای عقرب جرار من</p></div>
<div class="m2"><p>تا کی زنی نیشم بدل تا کی کنی آزار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دفتر بگیر و جام ده کام من بدنام ده</p></div>
<div class="m2"><p>با آن چلیپا طره ات تبدیل کن زنار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم تو خورده خون من چون من که خوردم خون دل</p></div>
<div class="m2"><p>زلف تو دارد بار دل چون دل که دارد بار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عشق تو افسانه ام شمع تو را پروانه ام</p></div>
<div class="m2"><p>کار تو باشد قتل من جان باختن شد کار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قتل من آهنگ کن دستان بحنا رنگ کن</p></div>
<div class="m2"><p>بر مدعی کن مشتبه خون من ای خونخوار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نالد چو نی در انجمن گریند از وی مرد و زن</p></div>
<div class="m2"><p>دارد بهر بندی نهان از نالهای زار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو با رقیبان در طرب من از شکایت بسته لب</p></div>
<div class="m2"><p>نالم نهانی روز و شب ای محرم اسرار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکرفروش بوسه اش خوش گفت دوش این نکته را</p></div>
<div class="m2"><p>تنگ از هجوم مشتری بر خلق شد بازار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عشق تو رسوا شدم ای مغبچه ترسا شدم</p></div>
<div class="m2"><p>اقرار کردم کفر را دیگر مکن انکار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفرم تو اسلامم توئی ننگم تو و نامم توئی</p></div>
<div class="m2"><p>عشق است دین و مذهبم بشنو بتا اقرار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کعبه کوی علی گشتی خلیل آشفته تو</p></div>
<div class="m2"><p>طوفان چه باشد در نظر آتش بود گلزار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کمتر بگرد شمع او ای مدعی پروانه شو</p></div>
<div class="m2"><p>پرهیز کن پرهیز کن از آه آتشبار من</p></div></div>