---
title: >-
    شمارهٔ ۱۱۲۰
---
# شمارهٔ ۱۱۲۰

<div class="b" id="bn1"><div class="m1"><p>از چه ای عشق تو در سینه ما جا کردی</p></div>
<div class="m2"><p>دل تاریک مرا سینه سینا کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر قتل نداری تو زابرو و مژه</p></div>
<div class="m2"><p>خنجر و تیر و سنان از چه مهیا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهی ار کس نشناسد که تو خونخوار منی</p></div>
<div class="m2"><p>ناخن از خون دلم از چه محنا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خیال لب ساقی چه غم از رنج خمار</p></div>
<div class="m2"><p>نقل و می داشتی و عیش مهیا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب نهادی بلب ساغر زین رشک ببزم</p></div>
<div class="m2"><p>لاجرم خون جگر در دل مینا کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله طور بود دلبر تو خود چو خسی</p></div>
<div class="m2"><p>وصل او را زچه آشفته تمنا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمه از خاک در میکده کردی زاهد</p></div>
<div class="m2"><p>کور بودی زکجا دیده بینا کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوردی از دست علی باده ی تو چند مگر</p></div>
<div class="m2"><p>اهل صورت بدی و رخنه به معنا کردی</p></div></div>