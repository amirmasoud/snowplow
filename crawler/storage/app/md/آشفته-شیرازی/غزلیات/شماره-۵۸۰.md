---
title: >-
    شمارهٔ ۵۸۰
---
# شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>شب قدر است این یا صبح نوروز</p></div>
<div class="m2"><p>که کوکب سعد گشت و بخت فیروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد در شب قدر این سعادت</p></div>
<div class="m2"><p>ندارد این صباحت صبح نوروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشیر از مصر می‌آرد بشارت</p></div>
<div class="m2"><p>که کنعان نور دیگر دارد امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نو آتشکده از پارس برخاست</p></div>
<div class="m2"><p>بشارت بر به پیر آتش‌افروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ار نالد شبی در دام زلفت</p></div>
<div class="m2"><p>نگیری خرده از مرغ نوآموز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلامت بگذر ای زاهد سلامت</p></div>
<div class="m2"><p>چه می‌خواهی ز عشق عافیت‌سوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگیرم گوش از گفتار ناصح</p></div>
<div class="m2"><p>اگر گوید ز خوبان دیده بردوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسید آن برق عالم‌سوز از راه</p></div>
<div class="m2"><p>برو چندان که خواهی خرمن اندوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نورزی غیر عشق آل طه</p></div>
<div class="m2"><p>طریق عشق زآشفته بیاموز</p></div></div>