---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>هر کرا خسرو دل در گرو شیرین است</p></div>
<div class="m2"><p>باغ فردوس دلش منزل حور العین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک فرهاد چه بر باد دهی ای خسرو</p></div>
<div class="m2"><p>که برآمیخته خاکش بغم شیرین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عنکبوتست که در پرده مگس صید کند</p></div>
<div class="m2"><p>آنکه بی پرده کبوتر ببرد شاهین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه اگر دست بخون دگران آلاید</p></div>
<div class="m2"><p>آنکه سر پنجه اش از خون دلم رنگین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار پشتست و مغیلان شب هجرم بستر</p></div>
<div class="m2"><p>گر زدیبا و حریرم همه شب بالین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوبهارم زفراق تو دهد رنگ خزان</p></div>
<div class="m2"><p>گر بدیماه بود وصل تو فروردین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوی لیلی بسر تربت مجنون چو رسید</p></div>
<div class="m2"><p>خیزد و گوید روح الله منظور این است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه بیعشق گدائیست بسی بی تمکین</p></div>
<div class="m2"><p>گرچه درویش بود عاشق با تمکین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدعا خواسته آشفته چو وصلت از خدا</p></div>
<div class="m2"><p>همه شب ذکر ملایک به سما آمین است</p></div></div>