---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>بغیر ساحت لیلی اگر چه صحرائیست</p></div>
<div class="m2"><p>گمان مدار که مجنون پی تماشائیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدشت عقل گذر کن که جای پرخطر است</p></div>
<div class="m2"><p>بکوی عشق بکش رخت را که خوش جائیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان تهی شمرندت که نیست پای شکیب</p></div>
<div class="m2"><p>دهل صفت گرت از ضرب دست غوغائیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بکشور یغماست جای لشکر ترک</p></div>
<div class="m2"><p>بترک چشم تو نازم که شهر یغمائیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجز بیاد لبت باده گر کشد باد است</p></div>
<div class="m2"><p>بروزگار تو هر جا که باده پیمائیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر که منزل دیوانگان بود زنجیر</p></div>
<div class="m2"><p>چرا به هر خم موی تو جای دانائیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که دست کش او راست حلقه کعبه</p></div>
<div class="m2"><p>چه غم که خار مغیلان شکسته در پائیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زقید قامت سرو چمن شدم آزاد</p></div>
<div class="m2"><p>چرا که دل بکمند بلند بالائیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو آستین چه فشانی که عاشقان نروند</p></div>
<div class="m2"><p>که لاجرم مگسی هست تا که حلوائیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانمت زکجا و چه مظهری ایعشق</p></div>
<div class="m2"><p>به هر سری زتو در روزگار سودائیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه غم زضربه طوفان عشق آشفته</p></div>
<div class="m2"><p>چو نوح هست چه اندیشه ات که دریائیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حذر زسطوت سلطان کجا بود درویش</p></div>
<div class="m2"><p>تو را که همچو علی در زمانه مولائیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهول روز حسابت نباشد اندیشه</p></div>
<div class="m2"><p>گرت زغیر تبرا بر او تولائیست</p></div></div>