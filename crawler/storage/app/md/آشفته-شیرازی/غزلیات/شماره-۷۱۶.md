---
title: >-
    شمارهٔ ۷۱۶
---
# شمارهٔ ۷۱۶

<div class="b" id="bn1"><div class="m1"><p>در چه نخشب ماهی دیدم</p></div>
<div class="m2"><p>یوسفی در تک چاهی دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر لب چشمه نوش از خط سبز</p></div>
<div class="m2"><p>اثر مهر گیاهی دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدویدم پی خونخواره دل</p></div>
<div class="m2"><p>زآن سرانگشت گواهی دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو دیدم که قبا پوشیده</p></div>
<div class="m2"><p>بر سر ماه کلاهی دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخته تیغ بقتل اسلام</p></div>
<div class="m2"><p>کافر چشم سیاهی دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط و خال و مژه و زلف سیاه</p></div>
<div class="m2"><p>صف بصف خیل و سپاهی دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من گریزان شده آشفته زبیم</p></div>
<div class="m2"><p>تا بمیخانه پناهی دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیر میخانه مگر حیدر بود</p></div>
<div class="m2"><p>کش گدایان همه شاهی دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بطلب گرد دو گیتی گشتم</p></div>
<div class="m2"><p>کی به جز کی تو راهی دیدم</p></div></div>