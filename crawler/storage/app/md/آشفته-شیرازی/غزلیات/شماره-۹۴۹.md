---
title: >-
    شمارهٔ ۹۴۹
---
# شمارهٔ ۹۴۹

<div class="b" id="bn1"><div class="m1"><p>رحمی ای عشق خدا را تو بحیرانی من</p></div>
<div class="m2"><p>که گذشته است زحد بی سر و سامانی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو هر جمع پریشان و پریشان تو زجمع</p></div>
<div class="m2"><p>وقت شد جمع شد اسباب پریشانی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچنان از تو خرابم که زمن جغد گریخت</p></div>
<div class="m2"><p>آخر ای گنج ببخشا تو بویرانی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمتی خاتم جم برده زکف اهرمنم</p></div>
<div class="m2"><p>هان مهل دیو برد ملک سلیمانی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر برآور زگریبان شب تیره چون صبح</p></div>
<div class="m2"><p>رحم کن رحم بر این سر بگریبانی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبحه از کف بشد و رشته زنار گسیخت</p></div>
<div class="m2"><p>نه بجا کفر و نه آثار مسلمانی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصه عشق که یک عمر نهفتم از خلق</p></div>
<div class="m2"><p>وه که افسانه شد اینقصه پنهانی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناوک تست نه روحست به تن برکش هان</p></div>
<div class="m2"><p>بود آن زیستن من زگران جانی من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل بود فانی و گلزار تو باقی ای عشق</p></div>
<div class="m2"><p>بلبل از گل بنوا بر تو غزلخوانی من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو کدامی و چه نامی بحقیقت ای عشق</p></div>
<div class="m2"><p>که گدائی تو شد مایه سلطانی من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق میگفت منم مظهر حق دست خدا</p></div>
<div class="m2"><p>کانبیا را نبود رتبه سلمانی من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سختی چاه شد و زحمت اخوان بگذشت</p></div>
<div class="m2"><p>رو عزیزی بکن ای یوسف زندانی من</p></div></div>