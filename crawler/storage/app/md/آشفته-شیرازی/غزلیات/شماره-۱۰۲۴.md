---
title: >-
    شمارهٔ ۱۰۲۴
---
# شمارهٔ ۱۰۲۴

<div class="b" id="bn1"><div class="m1"><p>زابروان تیغِ دوسَر بر مه و مهر آخته‌ای</p></div>
<div class="m2"><p>رتبهٔ خود چه توان کرد که نشناخته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس راه نبرده که کجا منزل تست</p></div>
<div class="m2"><p>چون کلیسا و حرم هردو بپرداخته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی ای عقل که با عشق کنم ساز نبرد</p></div>
<div class="m2"><p>پنجه ای صعوه به شاهین ز چه انداخته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مطربا راست نوازی ره عشاق بیار</p></div>
<div class="m2"><p>اینچنین نغمه از این پرده تو ننواخته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این صف‌آرایی مژگانِ سیه حاجت نیست</p></div>
<div class="m2"><p>که به یک غمزه تو کار دو جهان ساخته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دین و دل صبر و خرد رفت به تاراج نظر</p></div>
<div class="m2"><p>تا تو ای عشق دو اسبه به سرم تاخته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوبت سلطنت امروز بزن کز خم زلف</p></div>
<div class="m2"><p>پرچم از غالیه بر مهر و مه افراخته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سروناز که در این باغ شده جلوه‌گهت</p></div>
<div class="m2"><p>که تو را طوق به گردن بود و فاخته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاید آشفته‌ای که اکسیر مرادت بزنند</p></div>
<div class="m2"><p>زان که در بوته اخلاصش بگداخته‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبهٔ دل ز علی جا چه دهی نقش بتان</p></div>
<div class="m2"><p>بازی عشق و چنین نرد دغل باخته‌ای</p></div></div>