---
title: >-
    شمارهٔ ۶۹۲
---
# شمارهٔ ۶۹۲

<div class="b" id="bn1"><div class="m1"><p>محراب نخواهم خم ابروی تو دارم</p></div>
<div class="m2"><p>زنار نبندم شکن موی تو دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد بحرم نازد و راهب بکلیسا</p></div>
<div class="m2"><p>من فارغم از این دو سر کوی تو دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بت بپرستم من روی تو صنم بس</p></div>
<div class="m2"><p>ور کعبه ستایم خم گیسوی تو دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکهفته نپائید و بتاراج خزان رفت</p></div>
<div class="m2"><p>میگفت گل ار رنگ تو و بوی تو دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاشا که برم منتی از سد ره و طوبی</p></div>
<div class="m2"><p>در گلشن دل تا قد دلجوی تو دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تشنه و خال از لب نوشین تو سیراب</p></div>
<div class="m2"><p>من مسلمم و رشک بهندوی تو دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیر فلکت چشم سیه دید و همی گفت</p></div>
<div class="m2"><p>من شیرم و اندیشه زآهوی تو دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خضر بظلمات پی آب بقا رفت</p></div>
<div class="m2"><p>من آرزوی خاک سر کوی تو دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدست خدا دستی و برهان زجهانم</p></div>
<div class="m2"><p>چون روی امید از دو جهان سوی تو دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچون گل آمیخته با گل بودم خاک</p></div>
<div class="m2"><p>هر کس گذرد داند من بوی تو دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با پنجه پولاد بمن خصم جهانی</p></div>
<div class="m2"><p>آشفته ام و تکیه به نیروی تو دارم</p></div></div>