---
title: >-
    شمارهٔ ۷۴۵
---
# شمارهٔ ۷۴۵

<div class="b" id="bn1"><div class="m1"><p>هر که در میکده عشق شد امروز مقیم</p></div>
<div class="m2"><p>نیست فردا بدلش آرزوی باغ نعیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بپاداش عمل دوزخیم من چه عجب</p></div>
<div class="m2"><p>بنشان آتش هجران که عذابیست الیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو شفاعت نکند مست مرا زاهد شهر</p></div>
<div class="m2"><p>بر سر خوان لئیمان نرود مرد کریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوی بدرا نشود یار مگر مرد غیور</p></div>
<div class="m2"><p>حسن را بار کشی نیست مگر عشق سلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنه هر دو جهان خاص من ار شد غم نیست</p></div>
<div class="m2"><p>که خداوند کند عفو بالطاف عمیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیم و امید زحقست مرا باده بیار</p></div>
<div class="m2"><p>که مرا نیست از این خلق نه امید و نه بیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود آشفته هواخواه حسین شاه حجاز</p></div>
<div class="m2"><p>که سرو جان به ره دوست نموده تسلیم</p></div></div>