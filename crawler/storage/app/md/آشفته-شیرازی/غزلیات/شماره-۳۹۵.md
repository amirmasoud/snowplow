---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>زآن غنچه که از پرده بیک بار برآمد</p></div>
<div class="m2"><p>گلزار بجوش آمد گل زار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند زگل دوری مرغان گلستان</p></div>
<div class="m2"><p>تا آن گل نوخیز بگلزار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیزار شد از سرو چمن قمری خوشخوان</p></div>
<div class="m2"><p>تا با قد چون سرو برفتار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس خرقه پرهیز که شد در گرومی</p></div>
<div class="m2"><p>سرمست چو از خانه خمار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقل نتوان جست دگر در همه آفاق</p></div>
<div class="m2"><p>تا در نظر خلق پریوار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تسبیح بخاک افکن و سجاده بمی شوی</p></div>
<div class="m2"><p>کان مغبچه با زلف چو زنار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفی که بد از زرق رخش زرد همه عمر</p></div>
<div class="m2"><p>از میکده با چهره گلنار برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آورد پشیمانی بیع مه کنعان</p></div>
<div class="m2"><p>تا نقش بدیع تو ببازار برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته چو آن زلف سیه دید بدل گفت</p></div>
<div class="m2"><p>این مشک کی از طبله عطار برآمد</p></div></div>