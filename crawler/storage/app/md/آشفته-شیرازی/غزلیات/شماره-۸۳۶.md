---
title: >-
    شمارهٔ ۸۳۶
---
# شمارهٔ ۸۳۶

<div class="b" id="bn1"><div class="m1"><p>چنان بطره لیلای خویش مفتونم</p></div>
<div class="m2"><p>که در فنون جنون اوستاد مجنونم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زپرده های دورن ای مژه چه میجوئی</p></div>
<div class="m2"><p>که غنچه وش نبود غیر قطره خونم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار زآن می گلرنگ ساقی مجلس</p></div>
<div class="m2"><p>که کام تلخ بود عمرها زافیونم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه احتیاج باین نه سرادق نیلی</p></div>
<div class="m2"><p>که خیمه هاست بسی بر فراز گردونم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که یار بکامست و می بجام امشب</p></div>
<div class="m2"><p>چه شکرها که بگویم ز بخت میگونم</p></div></div>