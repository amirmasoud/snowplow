---
title: >-
    شمارهٔ ۱۲۵۵
---
# شمارهٔ ۱۲۵۵

<div class="b" id="bn1"><div class="m1"><p>پرده ز رخ کشیدند گل‌های نوبهاری</p></div>
<div class="m2"><p>بیجا چرا عنادل دارند بیقراری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق چون درآید جلوه‌کنان به بازار</p></div>
<div class="m2"><p>عاشق چرا بنالد از درد سوگواری؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون باد صبحدم را باشد ز تو پیامی</p></div>
<div class="m2"><p>شمع سحر نماید در پاش جانسپاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیلای لاله در دشت زد خیمه از سر ناز</p></div>
<div class="m2"><p>مجنون ابر کرده آغاز اشکباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاووس‌وش چو روزی اندر خرام آئی</p></div>
<div class="m2"><p>بر خویشتن بخندند کبکان کوهساری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکین تو ست نرگس ز آن تاج دارد از زر</p></div>
<div class="m2"><p>در عشق تو کند فخر لاله ز داغداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای لعبت بهشتی در جنت ار خرامی</p></div>
<div class="m2"><p>غلمان، غلامت آید وز حوریان حواری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از زلف او حدیثی آشفته در قلم آر</p></div>
<div class="m2"><p>کاز رشک خون کنی باز تو نافه تتاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهبود کی پذیرد ناسور دل خدا را</p></div>
<div class="m2"><p>زلفت به مشکبیزی نافت به مشکباری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بحر طبع الفت غوصی کن از سر شوق</p></div>
<div class="m2"><p>در مدح شاهت ار هست میل گهر نثاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حیدر شفیع محشر آئینه‌دار حیدر</p></div>
<div class="m2"><p>کو را نگفته مدحت غیر از جناب باری</p></div></div>