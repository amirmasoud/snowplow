---
title: >-
    شمارهٔ ۶۱۶
---
# شمارهٔ ۶۱۶

<div class="b" id="bn1"><div class="m1"><p>دل نمک سود لعل خندانش</p></div>
<div class="m2"><p>جان بر آتش زآب دندانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوک پیکانت ار خورد طفلی</p></div>
<div class="m2"><p>چه تمتع زشیر پستانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که دارد چنین گلی رعنا</p></div>
<div class="m2"><p>نبود شوق گشت بستانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساکن کوی آن بهشتی روی</p></div>
<div class="m2"><p>نیست حاجت بحور و غلمانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو به یعقوب تا که جوید باز</p></div>
<div class="m2"><p>یوسف اندر چه زنخدانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما و یک بوسه زآن لب نوشین</p></div>
<div class="m2"><p>خضر نازد بآب حیوانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که شد مطمئن خلیل آسا</p></div>
<div class="m2"><p>نار نمرود دان گلستانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این چه وادی بود که چون مجنون</p></div>
<div class="m2"><p>همه لیلی است در بیابانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتابش چه گوست در خم زلف</p></div>
<div class="m2"><p>تا خورد لطمه ی زچوگانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کرا سر بعهد یاری رفت</p></div>
<div class="m2"><p>ناگزیر است عهد یارانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کرا چشم و دل بابروئیست</p></div>
<div class="m2"><p>سینه شد وقف تیربارانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان آشفته خاک کوی علیست</p></div>
<div class="m2"><p>واعظ از این و آن مترسانش</p></div></div>