---
title: >-
    شمارهٔ ۱۱۰۴
---
# شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>چه خوش است روزگاری که بفقر بگذرانی</p></div>
<div class="m2"><p>بمتاع دین و دنیا دل و دست برفشانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بود ادیبت ای طفل و زکیست این طریقت</p></div>
<div class="m2"><p>که بدوستان کنی کین و بغیر مهربانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و دین خلق بردی و نگاه می نداری</p></div>
<div class="m2"><p>چکنی غنم نگارا که نمیکنی شبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه خط است طوطیانند مقیم شکرستان</p></div>
<div class="m2"><p>که زهندشان بیاری تو بآن شکردهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذار صحبت خضر و حدیث آب بشنو</p></div>
<div class="m2"><p>که زلعل دلستان است بقا و زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم آینه تو خورشید بقلب من نظر کن</p></div>
<div class="m2"><p>که چو عکس خود به بینی تو ضمیر من بدانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه عجب که همچو شمع است زبان آتشینم</p></div>
<div class="m2"><p>چکنم اگر نسوزم چو بر آتشم نشانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زپری و آدمیزاد ببرده ای دل و دین</p></div>
<div class="m2"><p>بنهان و آشکارا و بصورت و معانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عجب که جان فزاید سخنان دلفریبم</p></div>
<div class="m2"><p>که تو دلپذیر و دلدار مرا میان جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو چه یوسفی خدا را که بمصر خوبروئی</p></div>
<div class="m2"><p>دو جهان گرت بیارند بها که رایگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرم بگریه چشم بخندد او عجب نیست</p></div>
<div class="m2"><p>خبری نداشت ناصح چو زآتش نهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من دلشده که عمریست مقیم آستانم</p></div>
<div class="m2"><p>نه جفا بود خدا را سگ خویشم ار بخوانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعبث بخاک شیراز نشاندیم بحیرت</p></div>
<div class="m2"><p>نفرستیم بطوس و بنجف نمیرسانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخدای و احمد آشفته مدیح حیدر آمد</p></div>
<div class="m2"><p>تو بوصف او چه گوئی بزبان بیزبانی</p></div></div>