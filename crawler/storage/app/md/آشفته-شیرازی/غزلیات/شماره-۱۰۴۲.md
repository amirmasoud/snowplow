---
title: >-
    شمارهٔ ۱۰۴۲
---
# شمارهٔ ۱۰۴۲

<div class="b" id="bn1"><div class="m1"><p>چه اعتبار به سرو است چون تو در چمنی</p></div>
<div class="m2"><p>چه احتیاج به شمع است چون در انجمنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را که مشک ز مو نافه نافه می‌ریزد</p></div>
<div class="m2"><p>خطاست آنکه بگویند آهوی ختنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسحر چشم فسونگر عدوی زهادی</p></div>
<div class="m2"><p>بچین زلف چلیپا بلای برهمنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر گل نسزد جامعه دگر به تنت</p></div>
<div class="m2"><p>که سرو سیم تن و ارغوان گل بدنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تبسم لب تو غنچه را خموشی داد</p></div>
<div class="m2"><p>که دید نیست به پیش لب تواش دهنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو ای نسیم صبا گر زمصر میآئی</p></div>
<div class="m2"><p>مرا بیار زیوسف شمیم پیرهنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیاد طلعت لیلا و داغ مجنون است</p></div>
<div class="m2"><p>اگر که لاله زند سر بدامن دمنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهیر در همه شهری بدلبری ایشوخ</p></div>
<div class="m2"><p>ولی چه سود که بد عهد ترک دلشکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سزد که بخت نوی بخشدت بکسوت عمر</p></div>
<div class="m2"><p>حسام سلطنه را چون که بنده کهنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهی که ملک سلیمان به زیر خاتم اوست</p></div>
<div class="m2"><p>ولی به خاتم او ره نبرده اهرمنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زلف و چشم نه او فتنه می‌کند تنها</p></div>
<div class="m2"><p>که تو به شعر تر آشفته فتنه زمنی</p></div></div>