---
title: >-
    شمارهٔ ۱۱۲۱
---
# شمارهٔ ۱۱۲۱

<div class="b" id="bn1"><div class="m1"><p>اول ای عشق گمانم که تو سودا بودی</p></div>
<div class="m2"><p>عاقبت آفت دل دشمن جانها بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه در طلعت یوسف بتجلی در مصر</p></div>
<div class="m2"><p>گاه شور افکن مجنون شده لیلا بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودم که بدانائی از اوجان ببرم</p></div>
<div class="m2"><p>چون بدیدم بکمین دل دانا بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخنه در کوه چو فرهاد بسی کردم آه</p></div>
<div class="m2"><p>سخت تر ای دل سنگین تو زخارا بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدتی پرده فکندی زعذرا عذرا</p></div>
<div class="m2"><p>گاه در ربع و دمن مظهر سلمی بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام جمشید شدی آینه اسکندر</p></div>
<div class="m2"><p>گاه خنجر شده در پهلوی دارا بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتن و آمدنی بود به تبدیل لباس</p></div>
<div class="m2"><p>اول و آخر ای عشق تو تنها بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گه شدی روح و دمیدی بدم روح قدس</p></div>
<div class="m2"><p>گه بلب معجزه بخشای مسیحا بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ارنی گوی شدی گه بلباس موسی</p></div>
<div class="m2"><p>گاه در وادی طور آتش سینا بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بد و نیک تو بمقدار بصیرت گویند</p></div>
<div class="m2"><p>ورنه از پای بسر تو همه زیبا بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیشتر عشق بود باعث رسوائی تو</p></div>
<div class="m2"><p>گرچه آشفته از آغاز تو رسوا بودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر ازعشق توئی سر خداوند حکیم</p></div>
<div class="m2"><p>فاش گویم که علی عالی اعلا بودی</p></div></div>