---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>نبرد ره به کوی لیلی کس</p></div>
<div class="m2"><p>گر نگردد دلیل راه جرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا با شکر کنم خلوت</p></div>
<div class="m2"><p>انجمن گردد از هجوم مگس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق سرکش بعقل شده چیره</p></div>
<div class="m2"><p>دین و دل گو ببند راه نفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دزد بر شحنه چون شود حاکم</p></div>
<div class="m2"><p>کی بیندیشد از هجوم عسس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرتو شمع تو بجانها کرد</p></div>
<div class="m2"><p>با کلیم آنچه کرد نور قبس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میروی تند و نیست دسترسی</p></div>
<div class="m2"><p>تا نگهدارمت عنان فرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گواه آرمت بخون ریزی</p></div>
<div class="m2"><p>شاهدم پنجه نگارین بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکهت گل تو را سزد جامه</p></div>
<div class="m2"><p>نه که گل پس چه میکنی اطلس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو مگر نور شمع لم یزلی</p></div>
<div class="m2"><p>که زبان شد بوصف تو اخرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلشنت را خسی است آشفته</p></div>
<div class="m2"><p>باغبانا مران زباغت خس</p></div></div>