---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>عاقل مدار کار به تدبیر می‌نهد</p></div>
<div class="m2"><p>عارف زمام امر به تقدیر می‌نهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که از کمان قضا می‌جهد خدنگ</p></div>
<div class="m2"><p>عاشق دو دیده را به دم تیر می‌نهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اهل نظر به کعبه معنی برد نماز</p></div>
<div class="m2"><p>صورت‌پرست روی به تصویر می‌نهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دل که شد خراب در او جای دلبر است</p></div>
<div class="m2"><p>ویرانه رو به حلیه تعمیر می‌نهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی که چیست تکیه چشمت بر ابروان</p></div>
<div class="m2"><p>مستست و سر به قبضه شمشیر می‌نهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رند خراب بر در میخانه در سماع</p></div>
<div class="m2"><p>زاهد ز سبحه دام به تزویر می‌نهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن بی‌اثر دو جهان تیره شد به چشم</p></div>
<div class="m2"><p>در آه و ناله کیست که تأثیر می‌نهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باشد مؤثری که تغیرپذیر نیست</p></div>
<div class="m2"><p>هردم قرار کار به بتغییر می‌نهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زافتاده کمند محبت مکن سؤال</p></div>
<div class="m2"><p>چونست آن که پا به دم شیر می‌نهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته گفتمت که به زلفش مپیچ گفت</p></div>
<div class="m2"><p>دیوانه پا به حلقه زنجیر می‌نهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در این میانه کیست که محکوم او قضاست</p></div>
<div class="m2"><p>حیدر که پشت پای به تقدیر می‌نهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ما می‌رویم در پی خمار در کنشت</p></div>
<div class="m2"><p>آری مرید رو به در پیر می‌نهد</p></div></div>