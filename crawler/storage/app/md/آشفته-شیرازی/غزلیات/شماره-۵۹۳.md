---
title: >-
    شمارهٔ ۵۹۳
---
# شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>دل چو افغان برکشد پروا نمیدارد زکس</p></div>
<div class="m2"><p>مرغ عاشق را نه بیم از بند باشد نه قفس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق چون در دل نشست اندیشه غفلت خطاست</p></div>
<div class="m2"><p>رند کز می مست شد پروا ندارد از عسس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده را نبود نظر جز جانب منظور دل</p></div>
<div class="m2"><p>منظر دل لاجرم خلوتگه یار است و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از همچو مشتری گر عار دارد شکری</p></div>
<div class="m2"><p>یا شکر پنهان کند یا پر ببندد از مگس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغبان ما را مران از گلشن کوی نگار</p></div>
<div class="m2"><p>کاب و رنگ او نکاهد از هجوم خار و خس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محمل لیلی مگر در ره بود ای ساربان</p></div>
<div class="m2"><p>کامده دل در بر مجنون بافغان چون جرس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا توئی در محفل آشفته غزلخوانست و مست</p></div>
<div class="m2"><p>با حضور گل نبندد بلبل شیدا نفس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاشف اسرار یزدان مشعله افروز طور</p></div>
<div class="m2"><p>آنکه ساجد کرد موسی را زانوار قبس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل علی مرتضی و جای او گلزار دل</p></div>
<div class="m2"><p>بلبل طبعم به مدح او نواخوانست و بس</p></div></div>