---
title: >-
    شمارهٔ ۹۵۹
---
# شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>دریغ از گردش دوران و دور بی‌ثبات او</p></div>
<div class="m2"><p>که هر لحظه به شویی رام می‌گردد بنات او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه حرمان‌خیز این صحرا چه آتش‌زاست این بیدا</p></div>
<div class="m2"><p>که زاید تشنگی در کام جان‌ها از فرات او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جم هم عهد جام وی سریرش خوابگاه کی</p></div>
<div class="m2"><p>از این دو بر دوصد پرداخته بنگر ثبات او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجوزی عشوه‌گر دوران و خواهانش بود اعمی</p></div>
<div class="m2"><p>بتی سیمین بود دنیا و دل‌ها سومنات او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الا ای آنکه محصولی درودی اندر این مزرع</p></div>
<div class="m2"><p>به مسکینان ببخشا تا که بتوانی زکات او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسیر چار طبع مختلف تا کی در این دِیْری؟</p></div>
<div class="m2"><p>مجو عهد از موالید ثلاث امّهات او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان فانی بود آشفته و لابد فنا گردد</p></div>
<div class="m2"><p>نماند هیچ باقی غیر وجه حق و ذات او</p></div></div>