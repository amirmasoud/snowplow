---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>هر کرا بینی به کیش خویش دارد استقامت</p></div>
<div class="m2"><p>ای مسلمانان من از اسلام خود دارم ندامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قمر گرچه سریع‌السیرم اندر دور ادیان</p></div>
<div class="m2"><p>چون زحل بر آسمان عشق دارم استقامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس سفر کردم به کعبه بازگردیدم پشیمان</p></div>
<div class="m2"><p>ای خوشا آن روز کافکندم به میخانه اقامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورد خونم روز سی‌روز و به فتوای تو ساقی</p></div>
<div class="m2"><p>من خورم خونش به یک شب مرحبا از این غرامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهروان اندر طریق عشق سر دادند خوشدل</p></div>
<div class="m2"><p>گو سر خود گیر و بگذر ای که می‌جویی سلامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای حریف شخ کمان عشق هان مردش نخوانی</p></div>
<div class="m2"><p>هرکه رو گردانْد در میدانت از تیر ملامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاجرم آشفته آزاد است از آتش به محشر</p></div>
<div class="m2"><p>هرکه از داغ علی دارد به پیشانی علامت</p></div></div>