---
title: >-
    شمارهٔ ۸۰۷
---
# شمارهٔ ۸۰۷

<div class="b" id="bn1"><div class="m1"><p>ای ماهروی خرگهی ای صاحب تاج مهی</p></div>
<div class="m2"><p>تو باغ خوبی را بهی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر میسراید بلبلی در باغ و بستان بر گلی</p></div>
<div class="m2"><p>باید شنید ای گل ولی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل کند غوغا و بس گر گل شود رعنا و بس</p></div>
<div class="m2"><p>این واله آن زیبا و بس ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شور تو در هر مجلسی نام تو ورد هر کسی</p></div>
<div class="m2"><p>دارند سودایت بسی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه کی خرامد در زمی حوری نژاد از آدمی</p></div>
<div class="m2"><p>تو چشم جان را مردمی ما نیز هم بد نیستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کاخ ناید سرو بن خورشید کی گوید سخن</p></div>
<div class="m2"><p>کو سرو و مه دعوی کن ما نیز هم بد نیستیم</p></div></div>