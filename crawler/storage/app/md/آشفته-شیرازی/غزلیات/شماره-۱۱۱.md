---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>با نکویان عاشقان را آشنایی مشکلست</p></div>
<div class="m2"><p>پشّه را پرواز با فر همایی مشکلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغ شب را دیدن مهر منیر آمد محال</p></div>
<div class="m2"><p>از گدای ره‌نشینی پادشایی مشکلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم و جان را لاجرم روزی جدایی اوفتد</p></div>
<div class="m2"><p>لیکن از جانان دل و جان را جدایی مشکلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگسلد هر قید را سرپنجه دست اجل</p></div>
<div class="m2"><p>لیک عاشق را ز عشق تو رهایی مشکلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطره ناچیز را گو لاف عمانی مزن</p></div>
<div class="m2"><p>دعوی خورشید و جرم سهایی مشکلست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی را گو مزن دم از مقامات علی</p></div>
<div class="m2"><p>نیست را البته لاف از کبریایی مشکلست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه از خون نافه در ناف غزال آمد پدید</p></div>
<div class="m2"><p>لیک هر خون را دم از مشک ختایی مشکلست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنمی با خور نمی‌تابی دم از هستی مزن</p></div>
<div class="m2"><p>زاغی و با طوطیت این ژاژخایی مشکلست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردهٔ مرده تو را با معجز عیسی چه کار؟</p></div>
<div class="m2"><p>بندهٔ بنده تو را لاف خدایی مشکلست</p></div></div>