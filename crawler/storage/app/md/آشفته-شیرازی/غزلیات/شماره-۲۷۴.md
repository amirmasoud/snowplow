---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>آتشین روی تو را زلف نگویم دود است</p></div>
<div class="m2"><p>بلکه آن شعله طور است که مشک آلود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط بر آن آتش رخساره نه دود عود است</p></div>
<div class="m2"><p>یا ریاحین خلیل و شرر نمرود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب مستان چکنی زاهد پیمانه شکن</p></div>
<div class="m2"><p>عهد ما با می و مطرب زازل معهود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خویشتن بین نیم ای شیخ نکن سرزنشم</p></div>
<div class="m2"><p>عکس ساقیست که در جام و قدح مشهوداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بند پیر مغان شو در دیگر چه زنی</p></div>
<div class="m2"><p>که دو کونش چو یکی جام بکف موجود است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتیم از حرم کعبه فروریخت بتان</p></div>
<div class="m2"><p>بلکه از خلقت کعبه بت ما مقصود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه رجیم است همی دیود که یک سجده نکرد</p></div>
<div class="m2"><p>هر که در حلقه عشاق نشد مردود است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صاف نوشان خم عشق چه مستی کردند</p></div>
<div class="m2"><p>شور آشفته ازین ماده دردآلود است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که بینی بجهان نقش عبادت دارد</p></div>
<div class="m2"><p>پیر ما بود که هم عابد و هم معبود است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جزدر میکده عشق که دایم باز است</p></div>
<div class="m2"><p>هر دری هست گهی باز و گهی مسدود است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه در است آن در شاهنشه آفاق علیست</p></div>
<div class="m2"><p>که در میکده همت و بحر جود است</p></div></div>