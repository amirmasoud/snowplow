---
title: >-
    شمارهٔ ۴۷۷
---
# شمارهٔ ۴۷۷

<div class="b" id="bn1"><div class="m1"><p>زلیلی حسن کس افزون ندارد</p></div>
<div class="m2"><p>خبر زین جلوه جز مجنون ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوای عشق را تار دگر هست</p></div>
<div class="m2"><p>اگر ساقی می گلگون ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریفی گر بود مست محبت</p></div>
<div class="m2"><p>هوای باده و افیون ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدل دستی مرا ننهد طبیبی</p></div>
<div class="m2"><p>که دایم آستین پرخون ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عبث میخانه را بگشوده خمار</p></div>
<div class="m2"><p>خبر از آن لب میگون ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لبت را غنچه خواندم قامتت سرو</p></div>
<div class="m2"><p>که نیکوئی جز این مضمون ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد گفتا که غنچه کی سخن گفت</p></div>
<div class="m2"><p>ببین سرو آن قد موزون ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمانی چهره بنما تا نگویند</p></div>
<div class="m2"><p>جز او کس صحف انگلیون ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریف از زور بازو گشته مغرور</p></div>
<div class="m2"><p>خبر از پنجه بیچون ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علی دست خدا سرپنجه حق</p></div>
<div class="m2"><p>که کس قدرت از او افزون ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا آشفته سودائی بسر هست ‏</p></div>
<div class="m2"><p>که این سودا به جز مجنون ندارد</p></div></div>