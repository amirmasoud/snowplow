---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>تیغ بکف میرسد شاهد غضبان کیست</p></div>
<div class="m2"><p>تشنه خونست یار خون بسر خوان کیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غارت دل میکند غمزه فتان دوست</p></div>
<div class="m2"><p>کفر سر زلف او رهزن ایمان کیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش طور است این مایه نور است این</p></div>
<div class="m2"><p>شمع شبستان که برق نیستان کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلشده چون گوی رفت در صف میدان عشق</p></div>
<div class="m2"><p>شاه سواران من در خم چوگان کیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شور بعالم فکند از لب شیرین سخن</p></div>
<div class="m2"><p>این نمک خوان حسن تا زنمکدان کیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اژدر سحر آفرین از خم گیسو نمود</p></div>
<div class="m2"><p>پنجه موسی بگو سر بگریبان کیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش آن لعل لب خوش رطبی نور سست</p></div>
<div class="m2"><p>گفت رسیده ولی در خور دندان کیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه تبسم نمود تا لب لعل که بود</p></div>
<div class="m2"><p>ابر شده سیل خیز دیده گریان کیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش این نرگس است بر رخ تو خیره ماند</p></div>
<div class="m2"><p>گفت نظر کن ببین دیده حیران کیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش آشفته کرد وصف لبت دوش گفت</p></div>
<div class="m2"><p>طوطی شکر شکن بلبل خوشخوان کیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عید بود ایجوان سر بره دوست نه</p></div>
<div class="m2"><p>پیر چو شد گوسفند لایق قربان کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خاتمه این ورق مدحت شیر خداست</p></div>
<div class="m2"><p>جهد کن ایدل ببین نامه بعنوان کیست</p></div></div>