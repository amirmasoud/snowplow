---
title: >-
    شمارهٔ ۱۰۱۰
---
# شمارهٔ ۱۰۱۰

<div class="b" id="bn1"><div class="m1"><p>شبها که شمع من توئی هم خود شبان داج به</p></div>
<div class="m2"><p>گر ترک یغمائی توئی سرمایه بر تاراج به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشتر شکنج دام تو از گشت باغ و بوستان</p></div>
<div class="m2"><p>با شاهی کون و مکان در کوی تو محتاج به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مدعی نوش آورد من را غم نیش آورد</p></div>
<div class="m2"><p>مرهم نهد غیر ار بدل بر تیر تو آماج به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردار غیرت ار سرت خواهد شدن مگذر زحق</p></div>
<div class="m2"><p>از هر سری کو بنگری باشد سر حلاج به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ غلامی علی آشفته شد زیب جبین</p></div>
<div class="m2"><p>خاک در مولا بود درویش را از تاج به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ازدواج این جهان از چار گوهر شد عیان</p></div>
<div class="m2"><p>هم جوهر فردش توئی این فرد از آن ازدواج به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش نبی معراج تو معراج احمد بر فلک</p></div>
<div class="m2"><p>شاید اگر گوئی بود معراج از آن معراج به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما را شده کشتی تبه غرقیم در بحر گنه</p></div>
<div class="m2"><p>از موج بر بر ساحلم بحر گنه مواج به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غواص لؤلؤ می‌برد من موج بحرت می‌خورم</p></div>
<div class="m2"><p>از لؤلؤ و مرجان او این لطمه امواج به</p></div></div>