---
title: >-
    شمارهٔ ۲۱۳
---
# شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>جلوه صبح ازل یک پرتوی از روی اوست</p></div>
<div class="m2"><p>تاری شام ابد اندر شکنج موی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قاب و قوسین و کمان رستتم و تیغ دو سر</p></div>
<div class="m2"><p>این همه اندر اشارات خم ابروی اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کعبه و دار السلام و وادی طورو بهشت</p></div>
<div class="m2"><p>یک مقام امن از خیل گدای کوی اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اژدهای سحرخوار ان طره جادو فریب</p></div>
<div class="m2"><p>دست موسائی نهان اندر خم گیسوی اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هندوان گر میپرستند آفتاب آسمان</p></div>
<div class="m2"><p>آفتاب و ماه و انجم یکسره هندوی اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفرینش بسته یک نکته از لعل لبش</p></div>
<div class="m2"><p>معجزات انبیا در غمزه جادوی اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حاجیان روبر هجیر و بت پرستان روبدیر</p></div>
<div class="m2"><p>روی آشفته از این و آن همه بر روی اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوح و آدم را نجاتی هست از طوفان حشر</p></div>
<div class="m2"><p>زانکه ایشان را پناه وجای در پهلوی اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حیف باشد آبروی او بریزد پیش خصم</p></div>
<div class="m2"><p>ای امام راستان آن را که رودرروی اوست</p></div></div>