---
title: >-
    شمارهٔ ۶۷۷
---
# شمارهٔ ۶۷۷

<div class="b" id="bn1"><div class="m1"><p>ای من اسیر زلف خم اندر خمت شوم</p></div>
<div class="m2"><p>همچون صبا بحلقه مو محرمت شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خضر یافت زندگی از تیغ عشق تو</p></div>
<div class="m2"><p>ای من قتیل لعل مسیحا دمت شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم زبوی زلف تو ناسور زخم دل</p></div>
<div class="m2"><p>لعلش بخنده گفت که من مرهمت شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پرده راست پرده عشاق میزنی</p></div>
<div class="m2"><p>مطرب فدای نغمه زیر و بمت شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید گرچه آفت شبنم بود ولی</p></div>
<div class="m2"><p>من دارم آرزو بچمن شبنمت شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا می زلعل ساقی مجلس گرفته ام</p></div>
<div class="m2"><p>جام سفال گفت که جام جمت شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو آهوی ختائی ورم کردنت سزاست</p></div>
<div class="m2"><p>باز آی از خطا که فدای رمت شوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با ما کمست مهر تو و با رقیب بیش</p></div>
<div class="m2"><p>قربان مهربانی بیش و کمت شوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شادی نصیبه دگران بود و من بجهد</p></div>
<div class="m2"><p>آشفته گشته ام که اسیر غمت شوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هستند نوح و آدمت ایشاه در پناه</p></div>
<div class="m2"><p>من در پناه نوح و سگ آدمت شوم</p></div></div>