---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>زشور آن لب شیرین که در جهان انداخت</p></div>
<div class="m2"><p>گمان مکن که شکر در میان توان انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نقشها که عیان شد زسیم ساده او</p></div>
<div class="m2"><p>زطرح کینه که آن ماه مهربان انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن زنقطه موهوم رفت و باز حکیم</p></div>
<div class="m2"><p>حدیث لعل سخنگویت در میان انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصحن باغ نه گلهای آتشین است این</p></div>
<div class="m2"><p>که عکس روی تو آتش ببوستان انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه جلوه بود که حسن تو کرد بی پرده</p></div>
<div class="m2"><p>چه فتنه بود که موی تو در میان انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زطن غیر رهائی نیافت با همه جهد</p></div>
<div class="m2"><p>اگر چه عیسی خود را بآسمان انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سری بحلقه عشاق برکند عاشق</p></div>
<div class="m2"><p>بپای دوست اگر سر نهاد و جان انداخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه شمع آتشی آشفته داشت پنهانی</p></div>
<div class="m2"><p>که شور عشق تواش شعله در زبان انداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه عشق پرتو شمع ازل علی ولی</p></div>
<div class="m2"><p>که روح بر در او چوآسمان انداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بحیله با سگ کویت گرفته ام الفت</p></div>
<div class="m2"><p>که خویش را بتوانم در آستان انداخت</p></div></div>