---
title: >-
    شمارهٔ ۷۳۹
---
# شمارهٔ ۷۳۹

<div class="b" id="bn1"><div class="m1"><p>زاهد شهر نیم تا به تو تزویر کنم</p></div>
<div class="m2"><p>عاشقم عاشق با عشق چه تدبیر کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوشمالم دهی ای عشق بتان همچون چنگ</p></div>
<div class="m2"><p>که چو بربط همه شب ناله بم و زیر کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منکه دانم که کند گنج بویرانه مکان</p></div>
<div class="m2"><p>دل ویرانه خود بهر چه تعمیر کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت ترک تو مرا ملک ستان شاید گفت</p></div>
<div class="m2"><p>دو جهانی بیکی تجربه تسخیر کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نسیمم بکف افتد اگر آن زلف سیاه</p></div>
<div class="m2"><p>مو بمو شرح پریشانی تقریر کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت آهم من اگر برق جهان سوز شوم</p></div>
<div class="m2"><p>عجب است ار به دلی سنگین تأثیر کنم</p></div></div>