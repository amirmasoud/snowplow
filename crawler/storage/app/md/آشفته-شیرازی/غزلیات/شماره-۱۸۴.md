---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>هر که را سر زوفا خاک ره مقصود است</p></div>
<div class="m2"><p>بخت مقبل بود و طالع او مسعود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعله طور بود یا که گلستان خلیل</p></div>
<div class="m2"><p>پرتو عارض تو یا شرر نمرود است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر زخاکم نزند زآتش دل هیچ گیاه</p></div>
<div class="m2"><p>ور گیا روید از او لاله خون آلود است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبح نورانی وصل تو مرا عید سعید</p></div>
<div class="m2"><p>شب ظلمانی هجرت اجل موعود است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاش لله که نهد کس بسرش دست قبول</p></div>
<div class="m2"><p>هر که در حلقه صاحب نظران مردود است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر میخانه برهنش نستاند چکنم</p></div>
<div class="m2"><p>که مرا خرقه و سجاده ریا آلود است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی آشفته چه جنس است ترا رخت وجود</p></div>
<div class="m2"><p>تارش از زلف بتان رشته و عشقش پود است</p></div></div>