---
title: >-
    شمارهٔ ۴۴۶
---
# شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>چنان پیش رخت مهر منیر از تاب می‌افتد</p></div>
<div class="m2"><p>که در مهتاب وقتی کرمکی شب‌تاب می‌افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگم ببریدی و پیوند جان شد با تو محکم‌تر</p></div>
<div class="m2"><p>که گفت این رشته مهر و وفا از تاب می‌افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود این بحر عشق ای نوح زین ورطه کناری کن</p></div>
<div class="m2"><p>که آخر کشتیت از لطمه در گرداب می‌افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا آتش به جان دارد دلم از لعل پرتابت</p></div>
<div class="m2"><p>علاج خسته سوا اگر عناب می‌افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیبا نوشدارویی بیاور زآن لب نوشین</p></div>
<div class="m2"><p>شبی کز تاب تب بیمار دل بی‌تاب می‌افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مپرس از مردم چشمم که چون شد در شب هجران</p></div>
<div class="m2"><p>چه باشد حالت آن کاو که در غرقاب می‌افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدی بر تار دل زخمه بتا از ناخن مژگان</p></div>
<div class="m2"><p>حذر کن کاتش از این پرده در مضراب می‌افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در این سودا شکیبایی ز دل آشفته کمتر جو</p></div>
<div class="m2"><p>کجا ماند به جا آتش چو در سیماب می‌افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی باب الله واز وسوسه این نفس سرکش را</p></div>
<div class="m2"><p>به آیین گدایان کار در هر باب می‌افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محب مرتضی هرگز نخواهد رفت در دوزخ</p></div>
<div class="m2"><p>که چون خالص بود زر از چه در تیزآب می‌افتد</p></div></div>