---
title: >-
    شمارهٔ ۶۱۲
---
# شمارهٔ ۶۱۲

<div class="b" id="bn1"><div class="m1"><p>گرم چو جامه بگیری شبی تو در بر خویش</p></div>
<div class="m2"><p>چو شمع صبح بپایت فدا کنم سر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرت شکی است در اخلاص عاشق صادق</p></div>
<div class="m2"><p>ببین در آینه یعنی برأی انور خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کیقباد و جمم افتخارهاست بسی</p></div>
<div class="m2"><p>اگر مرا بشماری زخیل چاکر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمان مبر که روی از برابرم هرگز</p></div>
<div class="m2"><p>گرم برانی دایم تو از برابر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملامتم نکنی شاید از نظربازی</p></div>
<div class="m2"><p>اگر در آینه بینی بماه منظر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی که خاتم لعلت مرا بدست افتد</p></div>
<div class="m2"><p>بملک جم ندهم کلبه محقر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بغیر شاهد معنی نگنجدم در دل</p></div>
<div class="m2"><p>چه صورتست که من کرده ام مصور خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز بیاد حق آشفته زیستن غلطست</p></div>
<div class="m2"><p>بغیر یاد علی ره مده بخاطر خویش</p></div></div>