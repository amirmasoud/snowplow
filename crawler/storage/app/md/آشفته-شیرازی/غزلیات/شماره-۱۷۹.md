---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>زهجر زلف تو گفتم شبی کنیم شکایت</p></div>
<div class="m2"><p>فغان که این شب تیره نمیرسد بنهایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمیدهد دلم از دست دامن شب یلدا</p></div>
<div class="m2"><p>که در درازی و تاری ززلف تست کنایت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسوزی ار بعدالت ببخشی ار زکرامت</p></div>
<div class="m2"><p>زتست بخشش و رحمت زماست جرم و جنایت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعا کنم چو بدشنام نام من بلب آری</p></div>
<div class="m2"><p>که فحش از آن لب شیرین کرامتست و عنایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث عشق زپروانه پرس وصدق از او جو</p></div>
<div class="m2"><p>که سوخت جانش و از سوختن نکرد شکایت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بغیر شمع که میسوخت شب بحالت زارم</p></div>
<div class="m2"><p>کسی بشام فراق توام نکرد حمایت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدیده رشگ برد دل برای دیدن رویت</p></div>
<div class="m2"><p>ببین که بخل چه قدر است و رشگ تا به چه غایت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز نسیم سحر کو شمیم زلف تو آورد</p></div>
<div class="m2"><p>کسی بزخمی ناسور دل نکرد رعایت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فراق نامه آشفته گر بکوه بخوانی</p></div>
<div class="m2"><p>بسنگ خاره کند آه درمند سرایت</p></div></div>