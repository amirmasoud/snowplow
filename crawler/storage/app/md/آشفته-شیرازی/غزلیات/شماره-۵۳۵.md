---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>بُوَد آیا که ز ما پیر مغان یاد کند؟</p></div>
<div class="m2"><p>به یکی جرعه‌ام از قید تن آزاد کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تو را بیند و خاموش شود چون غنچه</p></div>
<div class="m2"><p>بلبل آنست که گل بیند و فریاد کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشکر خنده آن خسرو شیرین دهنان</p></div>
<div class="m2"><p>افکند شوری و خلقی همه فرهاد کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منکه از وسوسه عقل سراپای غمم</p></div>
<div class="m2"><p>هم مگر باز غم عشق توام شاد کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل آن کرد بت من که به بتخانه خلیل</p></div>
<div class="m2"><p>کو خلیلی که زنو خانه ای بنیاد کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خرابم زازل پیر خرابات کجاست</p></div>
<div class="m2"><p>که به معماری پیمانه ام آباد کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بدیها که سرشته بگلم روز نخست</p></div>
<div class="m2"><p>عشق گو نیست کند و از نوم ایجاد کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق چه دست خدا نو رهدی مظهر حق</p></div>
<div class="m2"><p>که سلیمان زدمش تکیه ابر باد کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی عالی اعلا که نبی مدنی</p></div>
<div class="m2"><p>از وجودش همه جا فخر به داماد کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاکبوس شه طوسش بدهد آشفته</p></div>
<div class="m2"><p>هر کرا پیر طریقت چو تو ارشاد کند</p></div></div>