---
title: >-
    شمارهٔ ۷۰۹
---
# شمارهٔ ۷۰۹

<div class="b" id="bn1"><div class="m1"><p>تا که بر طور دل این آتش سودا زده‌ایم</p></div>
<div class="m2"><p>آتش غیرت بر سینه سینا زده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته و سبحه زنّار گسستیم ز هم</p></div>
<div class="m2"><p>دست تا در خم آن زلف چلیپا زده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا که در گلشن عشق تو نواسنج شدیم</p></div>
<div class="m2"><p>طعن‌ها بر گل و بر بلبل شیدا زده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر عشق است و به کشتی نتوان کرد عبور</p></div>
<div class="m2"><p>از پی گوهر مقصود به دریا زده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصحف زهد ریایی بنهم در آتش</p></div>
<div class="m2"><p>کآتشین می ز کف آن بت ترسا زده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت مجنون ز پی لیلی اگر اندر حی</p></div>
<div class="m2"><p>ما سراپرده به سرمنزل سلمیٰ زده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که احرام ره کعبه عشقت بستیم</p></div>
<div class="m2"><p>پشت پا بر حرم و دیر و کلیسا زده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیر لب خنده‌زنان شاهد قدسم می‌گفت</p></div>
<div class="m2"><p>ما دم روح قدس در دم عیسیٰ زده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاصل ذکر ملک نعره مستانه توست</p></div>
<div class="m2"><p>تا به میخانه سحر ساغر صهبا زده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نازم آن ساغر مینا که ز تأثیر مِیَش</p></div>
<div class="m2"><p>خیمه بالاتر از این گنبد مینا زده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هرکس آشفته زده چنگ به دامان کسی</p></div>
<div class="m2"><p>ما به دامان علی دست تَوَلّا زده‌ایم</p></div></div>