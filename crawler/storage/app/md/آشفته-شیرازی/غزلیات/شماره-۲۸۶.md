---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>تا سوز دل از آن لب لعل نمکین است</p></div>
<div class="m2"><p>زخمی که نمک سوده بر او حالتش اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم از مژه دارد دل و دار و زلب لعل</p></div>
<div class="m2"><p>وز نافه مو بستر و بالین که چنین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک چین بجهانست و در او نافه آهو</p></div>
<div class="m2"><p>گر نیست خطا هر خمی از زلف تو چین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهی و سخن گوئی و سروی و روانی</p></div>
<div class="m2"><p>گوئید که این معجز یا سحر مبین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چشمه کوثر به بهشتست عجب نیست</p></div>
<div class="m2"><p>او را بنمک زار عجب ماء معین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غلمان بغلامی درت میکند اقرار</p></div>
<div class="m2"><p>حورت پی خدمت چو یکی بنده کمین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خود نه بر آنم که توئی کعبه مقصود</p></div>
<div class="m2"><p>هر کس که بود عاشق روی تو بر اینست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خانه است اگر کعبه تو خود خانه خدائی</p></div>
<div class="m2"><p>جبریل زتو گفته اگر روح الامین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته برد سجده بجز کوی تو حاشا</p></div>
<div class="m2"><p>تا داغ غلامی تواش زیب جبین است</p></div></div>