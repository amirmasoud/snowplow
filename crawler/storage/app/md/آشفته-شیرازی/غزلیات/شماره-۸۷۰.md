---
title: >-
    شمارهٔ ۸۷۰
---
# شمارهٔ ۸۷۰

<div class="b" id="bn1"><div class="m1"><p>زبسکه مهر تو آمیخته بجان و تنم</p></div>
<div class="m2"><p>توئی ندانم یا من درون پیرهنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهفته سوز غمت بسکه همچو جان بتنم</p></div>
<div class="m2"><p>چو شمع شعله برآید زچاک پیرهنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زخاک کوی تو گر بوئی آردم دم صبح</p></div>
<div class="m2"><p>زآب خضر و دم روح قدس دم نزنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زشور آن لب شیرین چو در حدیث آیم</p></div>
<div class="m2"><p>نمک بریزد و شکر مدام از دهنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیاد تو بلحد خضر زنده و جاوید</p></div>
<div class="m2"><p>بجامه غافل از تو چو مرده در کفنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای زلف تو خونم زبس بسوخت بجسم</p></div>
<div class="m2"><p>کنند مشک زخونم که آهوی ختنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا که شوق تو چون کهربا زجابر کند</p></div>
<div class="m2"><p>بروز عشق بسی کوهها زجا بکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بهشت نشد گر نصیب من غم نیست</p></div>
<div class="m2"><p>بدست آمده زاهد چو سیب آن ذقنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عجب مدار که چون خس بسوخت خرمن غم</p></div>
<div class="m2"><p>که من بگلشن تو مرغ آتشین سخنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم بشام غریبان زلف تو خو کرد</p></div>
<div class="m2"><p>بدان صفت که فراموش گشته از وطنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر چه رند و قدح خوار و مستم آشفته</p></div>
<div class="m2"><p>بدین خوشم که سنگ آستان بوالحسنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ریمن بندگی شاه اینک اندر طوس</p></div>
<div class="m2"><p>زبندگان شما خوان خسرو زمنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مهین سلاله شه نایب الایاله راد</p></div>
<div class="m2"><p>که با وجود کف او زبحر دم نزنم</p></div></div>