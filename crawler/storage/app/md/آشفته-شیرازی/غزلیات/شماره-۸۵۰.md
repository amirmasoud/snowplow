---
title: >-
    شمارهٔ ۸۵۰
---
# شمارهٔ ۸۵۰

<div class="b" id="bn1"><div class="m1"><p>به گلزار غم عشق تو من آن مرغ خاموشم</p></div>
<div class="m2"><p>که شد از بیم گلچین نغمه‌پردازی فراموشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو غنچه تا که زد مهر خموشی بر دهان من</p></div>
<div class="m2"><p>که من با صد زبان چون سوسن آزاده خاموشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن بی‌روحم و چون توام بادام دور از تو</p></div>
<div class="m2"><p>نمایانست جای خالیت جانا در آغوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل خونین چو خم گر می‌زند جوشی عجب نبود</p></div>
<div class="m2"><p>که تا هست آتش سودا به جان چون دیگ در جوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خدا را ساقیا صهبا به یاران دگر پیما</p></div>
<div class="m2"><p>که من از غمزهٔ آن ترکِ سرخوش مست و مدهوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شام هجر زلفت تا قیامت شکوه‌ها دارم</p></div>
<div class="m2"><p>مگر صبحی شود طالع از آن طرف بناگوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جان هندوی زلف و خال آن ترک قزلباشم</p></div>
<div class="m2"><p>که کرده هندوی مویش هزاران حلقه در گوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پریشان گفته آشفته کی افتد قبول شه</p></div>
<div class="m2"><p>مگر اکسیر عشق تو خورد بر قلب مغشوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زیر بار عصیان مانده‌ام در این سفر یارب</p></div>
<div class="m2"><p>مگر دست خدا بار گران بردارد از دوشم</p></div></div>