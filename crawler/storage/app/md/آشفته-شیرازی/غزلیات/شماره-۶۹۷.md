---
title: >-
    شمارهٔ ۶۹۷
---
# شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>رحمی ای عشق که من مانده بچنگ هوسم</p></div>
<div class="m2"><p>رحمت ای شحنه و برهان تو زقید عسسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا شمع رخی طوف چو پروانه کنم</p></div>
<div class="m2"><p>هر کجا قند لبی هست من آنجا مگسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا یوسف مصریست زلیخا لقبم</p></div>
<div class="m2"><p>هر طرف ناقه لیلی است من آنجا جرسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می ستایند کسانم که چنینی و چنان</p></div>
<div class="m2"><p>چون به بینی بحقیقت بجهان هیچکسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روسیه زاغم و غوغای عنادل بزبان</p></div>
<div class="m2"><p>نوبهاری کنم و باد خزان شد نفسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا سرو قدی هست منش فاخته ام</p></div>
<div class="m2"><p>هر کجا لاله ستانیست منش خار و خسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهسواری مگر از لطف بگیرد دستم</p></div>
<div class="m2"><p>لنگ شد بر سر میدان طلب چون فرسم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میزنم لاف که در حلقه عشقم محرم</p></div>
<div class="m2"><p>بحقیقت سرو سر حلقه اهل هوسم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من عمل هیچ ندارم بجز از کذب و دغل</p></div>
<div class="m2"><p>مهر حیدر مگر آشفته شود دادرسم</p></div></div>