---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>مدعی پنداشتی دور از در جانان مرا</p></div>
<div class="m2"><p>دور شد تن لیک مانده پیش جانان جان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با صبا هر صبحدم آیم به طوف کوی دوست</p></div>
<div class="m2"><p>چشم اگر داری بجو در خاک آن ایوان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشنوی گر هر سحر بانگ جرس از غافله</p></div>
<div class="m2"><p>هست با هر کاروانی ناله پنهان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی در گر رود غواص اندر قعر بحر</p></div>
<div class="m2"><p>هست اندر خاک کوی دوست بحر و کان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده طوفان می‌کند گر هرشب از موج سرشک</p></div>
<div class="m2"><p>نوح چون همره بود نبود غم از طوفان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکرم سر تا قدم راه است زان تابنده نور</p></div>
<div class="m2"><p>گرچه چون خورشید بینی با تن عریان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حبیبم زخم به تا نوشدارو وز طبیب</p></div>
<div class="m2"><p>من خوشم با درد نبود حاجت درمان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه بهشت جاودان خواهم نه غلمان و نه حور</p></div>
<div class="m2"><p>کرده روی و کوی او فارغ از این و آن مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر به صورت دورم از طوس و در هشتم امام</p></div>
<div class="m2"><p>مهر او جا کرده چون جا در رگ و شریان مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرغ روحم پرفشان دائم به گلزار رضاست</p></div>
<div class="m2"><p>گرچه منزلگاه به شیراز است یا تهران مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از گدایان درش آشفته انعامم رسید</p></div>
<div class="m2"><p>نیست بر جان منتی از حاتم و قاآن مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم به جای نیکیش یارب جزای نیک ده</p></div>
<div class="m2"><p>زآن که قدرت نیست بر پاداش آن احسان مرا</p></div></div>