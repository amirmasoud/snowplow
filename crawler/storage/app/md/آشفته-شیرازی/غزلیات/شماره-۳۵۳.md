---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>عجب مکن گرت آن ترک سیم‌تن بکشد</p></div>
<div class="m2"><p>بلی نسیم سحر شمع انجمن بکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه باک دارد اگر صدهزار خون بخورد</p></div>
<div class="m2"><p>بر او چه جرم اگر صدهزار تن بکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حذر ز مار دو گیسو و لعل ضحاکش</p></div>
<div class="m2"><p>که دل ز خندهٔ شیرین آن دهن بکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی حریف که ماتم به بازی عشقش</p></div>
<div class="m2"><p>که هم ببردنم او هم به باختن بکشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر ز بوسه شیرین به کام خسرو رفت</p></div>
<div class="m2"><p>به خنده‌ام لبت ای شوخ بی‌سخن بکشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به غیر زلف تو کآمد کمند گردن بت</p></div>
<div class="m2"><p>شنیده‌ای به چلیپا بتی وثن بکشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه مذهبش بود آن تُرک مست خون‌آشام</p></div>
<div class="m2"><p>که شیخ و برهمن از قهر مرد و زن بکشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پاکدامنی او دو عالمند گواه</p></div>
<div class="m2"><p>اگر به هر نفسی او دوصد چو من بکشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بزم شمع من آشفته گر برافروزد</p></div>
<div class="m2"><p>سزد ز غیرت اگر شمع خویشتن بکشد</p></div></div>