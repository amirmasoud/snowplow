---
title: >-
    شمارهٔ ۷۰۱
---
# شمارهٔ ۷۰۱

<div class="b" id="bn1"><div class="m1"><p>آخر ای پیر خرابات نه من مخمورم</p></div>
<div class="m2"><p>گر زنم حلقه بدر می طلبم معذورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چه در حلقه مستان تو را هم ندهند</p></div>
<div class="m2"><p>من که در سلسله دردکشان مشهورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میکشانت همه کشتی بشط می راندند</p></div>
<div class="m2"><p>در سراب از چه من مست بگو مخمورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه مرا خرقه و سجاده نه سیم و زر و زور</p></div>
<div class="m2"><p>زاری آورده ام و نیست جز این مقدورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفر و اسلام زمن هردو گریزند زننگ</p></div>
<div class="m2"><p>چه غم از آن که از این هر دو توئی منظورم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستی ای دست خدا بهر نجاتم زکرم</p></div>
<div class="m2"><p>زانکه در پنجه شاهین قضا مقهورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه در پرده جانان نبرم آشفته</p></div>
<div class="m2"><p>در حجاب تن و جان تا چو تو من مستورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهر گر گرد برانگیخته از هستی من</p></div>
<div class="m2"><p>چون تو معمار وجودی بخدا معمورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم غربت اگرم سخت سرا پا چون شمع</p></div>
<div class="m2"><p>چون که در بزم رضا سوخته ام مسرورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیخ غره زعمل شد زسپاه و زر و ملک</p></div>
<div class="m2"><p>من زخاک در کریاس علی مغرورم</p></div></div>