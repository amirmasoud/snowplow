---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>ساقیا خیز که یک نیمه زشعبان بگذشت</p></div>
<div class="m2"><p>باده پیش آر که چون باد بهاران بگذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فصل گل میرسد و ماه صیامش از پی</p></div>
<div class="m2"><p>این خزانیست که از نو بگلستان بگذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میخور امروز بگلزار که فردا فرداست</p></div>
<div class="m2"><p>هر که این صرفه زکف داد پشیمان بگذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژده کز مصر بشیر آمد و آورد پیام</p></div>
<div class="m2"><p>رفت آن صدمه که بر پیر بکنعان بگذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اهرمن گو بدهد باز پس آن خاتم ملک</p></div>
<div class="m2"><p>کان قضاهای بد از ملک سلیمان بگذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوبتی نوبت عشرت بزن امشب زطرب</p></div>
<div class="m2"><p>که شب وصل شد و نوبت هجران بگذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب مولود شهنشاه زمان مهدی عصر</p></div>
<div class="m2"><p>که زیمن قدمش خاک زکیوان بگذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه ثانی عشر آن کز شرف میلادش</p></div>
<div class="m2"><p>بر ملایک زشرف رتبه انسان بگذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چند در پرده ای ایشمع ازل پرده بسوز</p></div>
<div class="m2"><p>که بسی تیره بمن آنشب حرمان بگذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شرک بگرفت جهان را بکش آن تیغ دو سر</p></div>
<div class="m2"><p>بجهان اسب که دجال بجولان بگذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست آشفته بگیر از سر رحمت ایشاه</p></div>
<div class="m2"><p>آنکه گر عرق گناهست ثناخوان بگذشت</p></div></div>