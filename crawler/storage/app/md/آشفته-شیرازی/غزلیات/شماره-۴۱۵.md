---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>کشتن عاشق اگر عِقاب ندارد</p></div>
<div class="m2"><p>لیک به این قدر هم ثواب ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست حسابی به کار و روز جزایی</p></div>
<div class="m2"><p>کار نگویی که تو حساب ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوی به رخ تست یا گلاب چکیده است</p></div>
<div class="m2"><p>گرچه گل آتشین گلاب ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهره و زلفت مگر که ماه و سحاب است</p></div>
<div class="m2"><p>لیک مه این عنبرین‌سحاب ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو بکن از خون ما تو دست نگارین</p></div>
<div class="m2"><p>پنجه سیمینش ار خضاب ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب کند بخت من ولی به شب وصل</p></div>
<div class="m2"><p>دیده عاشق مگو که خواب ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت که خونت بریزم از دم خنجر</p></div>
<div class="m2"><p>با همه طفلی چرا شتاب ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای بت شیرین نهی تو زین چه به گلگون</p></div>
<div class="m2"><p>هست دو چشم من ار رکاب ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تخم فشاندیم و آب دیده ضرور است</p></div>
<div class="m2"><p>آه که این چشمه هیچ آب ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتمش آشفته را جواب سلامی</p></div>
<div class="m2"><p>گفت برو حرف تو جواب ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این دل سودازده که مانده پریشان</p></div>
<div class="m2"><p>راه به جز کوی بوتراب ندارد</p></div></div>