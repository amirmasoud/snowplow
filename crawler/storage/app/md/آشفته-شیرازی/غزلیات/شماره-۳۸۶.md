---
title: >-
    شمارهٔ ۳۸۶
---
# شمارهٔ ۳۸۶

<div class="b" id="bn1"><div class="m1"><p>اگر آه مرا اندر شب هجران اثر باشد</p></div>
<div class="m2"><p>کفایت خرمن کون و مکان را یک شرر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدی حالت یعقوب و حسن ماه کنعان را</p></div>
<div class="m2"><p>جفاکار است و زیبارو اگر چه خود پسر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهان بگشا و حرف قتل عاشق در میان آور</p></div>
<div class="m2"><p>که تا معلوم گردد کت دهان هست و کمر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدت را سرو گفتم لیک حیرانم که در بستان</p></div>
<div class="m2"><p>ثمرکی سرورا عناب و بادام و شکر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیارد منع عاشق از نظر بازی آن منظر</p></div>
<div class="m2"><p>نصیحت گو اگر از زمره اهل نظر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تراکت غنچه وش پرخنده لب باشد نه ای عاشق</p></div>
<div class="m2"><p>نشان عشاق را لبهای خشک و چشم تر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن از من دریغ ای باد خاک مقدم او را</p></div>
<div class="m2"><p>که خاک پای او آشفته را کحل البصر باشد</p></div></div>