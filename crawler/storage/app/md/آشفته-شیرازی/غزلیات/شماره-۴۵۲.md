---
title: >-
    شمارهٔ ۴۵۲
---
# شمارهٔ ۴۵۲

<div class="b" id="bn1"><div class="m1"><p>آزادگان بقید تعلق کم او فتند</p></div>
<div class="m2"><p>ور زانکه لغزشی برود محکم او فتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه زنیکوان نتوانم که گفته اند</p></div>
<div class="m2"><p>خوبان باوفا بزمانه کم او فتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نازم بآن دو لعل نمک پاش کز سخن</p></div>
<div class="m2"><p>بر داغهای سینه و دل مرهم او فتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون زلف تست شب همه شب در کف رقیب</p></div>
<div class="m2"><p>عشاق را عجب نه اگر در هم او فتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید و مه که مشعله افروز عالمند</p></div>
<div class="m2"><p>در پیش آفتاب تو چون شبنم او فتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب چه نشئه داشت محبت که عاشقان</p></div>
<div class="m2"><p>اندوهناک عشق وی خرم او فتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک جام کرد تعبیه جمشید و خسروان</p></div>
<div class="m2"><p>در سجده تا بحشر بپای خم او فتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پرده عشق نغمه سرا گشت و مطربان</p></div>
<div class="m2"><p>اندر گمان نغمه زیر و بم او فتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز قتل نیست شیوه خوبان این دیار</p></div>
<div class="m2"><p>با اینکه این گروه مسیحا دم او فتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از روح غافلند نصاری و از قیاس</p></div>
<div class="m2"><p>تا تهمتی نهند پی مریم او فتند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته کی رها شوی از آن شکنج زلف</p></div>
<div class="m2"><p>دیوانگان بسلسله مستحکم او فتند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خوش وقت آن گروه که در سایه علی</p></div>
<div class="m2"><p>تا بامداد حشر دل بیغم او فتند</p></div></div>