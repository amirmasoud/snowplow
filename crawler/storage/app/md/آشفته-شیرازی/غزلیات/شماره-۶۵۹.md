---
title: >-
    شمارهٔ ۶۵۹
---
# شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>می‌رفت و هزار دل دنبال</p></div>
<div class="m2"><p>سرپنجه ز خون مرد و زن آل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سمین ذقنش چو چاه بیژن</p></div>
<div class="m2"><p>گیسوش کمند رستم زال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رفت چو آهوان وحشی</p></div>
<div class="m2"><p>می‌کرد گهی نظر به دنبال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کرد گه از مژه اشارت</p></div>
<div class="m2"><p>می‌ریخت ز چشم خلق قیفال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌رفت الف صفت مجرد</p></div>
<div class="m2"><p>زلفش به قفا خمیده چون دال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سایه منش دویدم از پی</p></div>
<div class="m2"><p>در پای فتادمش چو خلخال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم مرو ای روان عشاق</p></div>
<div class="m2"><p>گفتم مرو ای همای اقبال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو رفتی و دیده ماند بی‌نور</p></div>
<div class="m2"><p>تو جانی و بی‌تو تن بزلزال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر مطلب تست خون عاشق</p></div>
<div class="m2"><p>برخیز چه می‌کنی تو احمال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من بسمل و غافل است صیاد</p></div>
<div class="m2"><p>گو شیر بدرّدم به چنگال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خم کرد کمان ابروان را</p></div>
<div class="m2"><p>بگذاشت در او خدنگ قتال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتا بگذار دامنم را</p></div>
<div class="m2"><p>ورنه کنمت ز خون قبا آل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آشفته تو بسته کمندی</p></div>
<div class="m2"><p>ای صعوه چه می‌زنی پر و بال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون عشق به زورمندی آمد</p></div>
<div class="m2"><p>از کار افتاد عقل فعال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر دامن مرتضی بزن چنگ</p></div>
<div class="m2"><p>بنشین همه عمر فارغ‌البال</p></div></div>