---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>زاهد که همی گفت می ناب حرام است</p></div>
<div class="m2"><p>می خورد و بگفت آنکه حرام است کدام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حوران بهشتی بجوار تو جواریست</p></div>
<div class="m2"><p>غلمان بسر کویت استاده غلام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر حکمتی آموخت زخم بود فلاطون</p></div>
<div class="m2"><p>ور حشمت جمشید زیک پرتو جام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمریست که زلفین تو بر آتش رخسار</p></div>
<div class="m2"><p>میسوزد و چون مینگری عنبر خام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عنبر نستانم زکس ونافه نبویم</p></div>
<div class="m2"><p>بوی سر زلفین توام تا بمشام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر زاهد شهرست که از جام تو مستست</p></div>
<div class="m2"><p>گر آهوی وحشیست که در دام تو رام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقص است بمه نسبت آن روی که از ماه</p></div>
<div class="m2"><p>هر روز شود ناقص وز دوست تمام است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی تو نکنم عیش اگر باغ بهشت است</p></div>
<div class="m2"><p>من با تو خورم باده اگر ماه صیام است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشمان تو بر چهره جادوست ببابل</p></div>
<div class="m2"><p>در زلف بناگوش تو شعرا که بشام است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرهاد که جان داد زشوق لب شیرین</p></div>
<div class="m2"><p>جانبازی و ناکامی او غایت کام است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته چه غم باشدش از هول قیامت</p></div>
<div class="m2"><p>آنرا که علی رهبر و مولی و امام است</p></div></div>