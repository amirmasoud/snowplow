---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>دوری از هم نفسان اندکی از خانه ما</p></div>
<div class="m2"><p>کامشب افراشت علم برق به کاشانه ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی میکده چون می به حریفان پیمود</p></div>
<div class="m2"><p>عوض باده شرر ریخت به پیمانه ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه سر زلف بتانست که دست ازلی</p></div>
<div class="m2"><p>ساخت زنجیر برای دل دیوانه ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده آگاه شد از سوز درون طوفان کرد</p></div>
<div class="m2"><p>آه اگر فاش شود پیش کس افسانه ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ببحرین غمش بیهده در غرقابم</p></div>
<div class="m2"><p>زانکه غواص دگر برده دردانه ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر میخانه سحر مژده بمستان میداد</p></div>
<div class="m2"><p>که خطابخش بود عفو کریمانه ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل و دین عقل و خرد گو بنهد طالب دوست</p></div>
<div class="m2"><p>جان بتنها نسزد تحفه جانانه ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه حمامم که بگندم بفریبد دامم</p></div>
<div class="m2"><p>زلف دام است و بود خال سیه دانه ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه خاموش نشینند ملایک از ذکر</p></div>
<div class="m2"><p>بفلک گر برسد نعره مستانه ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواست از پیر مغان سالکی اکسیر مراد</p></div>
<div class="m2"><p>گفت خاکست بر همت مردانه ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هندوی آتش عشقست دل آشفته</p></div>
<div class="m2"><p>شمع هر جمع نسوزد پر پروانه ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عشق چه مشعله طور ازل نور علی</p></div>
<div class="m2"><p>که بود روشن از او در دو جهان خانه ما</p></div></div>