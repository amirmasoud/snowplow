---
title: >-
    شمارهٔ ۱۰۳۳
---
# شمارهٔ ۱۰۳۳

<div class="b" id="bn1"><div class="m1"><p>ای مو بر آفتاب تو مشکین‌کلاله‌ای</p></div>
<div class="m2"><p>ای خط به ماه عارض دلدار هاله‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از احسن القصص نکند یاد یوسفش</p></div>
<div class="m2"><p>خواند از کتاب حسن تو هرکس مقاله‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ماه تا به ماهی و از عرش تا به فرش</p></div>
<div class="m2"><p>هرکس برد ز خوان عطایت نواله‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم مدیح حسن تو طغرایی خطت</p></div>
<div class="m2"><p>دادم به بوسه بر لب نوشت حواله‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندوختیم خرمن سالوس ساقیا</p></div>
<div class="m2"><p>زآن آب آتشین به من آور پیاله‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خوی ز تو طراوت آن چهره برفزود</p></div>
<div class="m2"><p>بر گل نگر تو شبنم و بر لاله ژاله‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نیک بنگری دل خونین عاشق است</p></div>
<div class="m2"><p>گر داغدار سر زند از خاک لاله‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانان جان و مظهر ذاتی و اصل نور</p></div>
<div class="m2"><p>از ماء و طین نه تنها فرخ‌سلاله‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معنی باء بسمله قرآن ناطقی</p></div>
<div class="m2"><p>دست خدا و مظهر لفظ جلاله‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته را نزاده به جز مدحت علی</p></div>
<div class="m2"><p>ای بکر طبع تا تو مرا در حباله‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای زلف تا قرین تو شد این دل پریش</p></div>
<div class="m2"><p>مجنون دل‌شکسته و حیران و واله‌ای</p></div></div>