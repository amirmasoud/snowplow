---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>در ساغر اگر باه گلنار بخندد</p></div>
<div class="m2"><p>نبود عجب ار مست به هوشیار بخندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن رند که در سلسله عشق نهد پای</p></div>
<div class="m2"><p>شک نیست که بر سبحه وزنار بخندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیوانه و عریان سر کوی نکویان</p></div>
<div class="m2"><p>هر جا نگرد خرقه و دستار بخندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنرا که تو کل بخدا همچو خلیل است</p></div>
<div class="m2"><p>بر صولت نمرودی و بر نار بخندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایعاشق دلداده تو از گریه بپرهیز</p></div>
<div class="m2"><p>بگذار ملامتگر بیکار بخندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگه که تبسم کند آن لعل شکرخند</p></div>
<div class="m2"><p>در حقه مرجان در شهوار بخندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا مشگ شمیم سر زلف تو شنیده</p></div>
<div class="m2"><p>بر آهوی چین نافه تاتار بخندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درویش اگر کسو فقر تو بپوشد</p></div>
<div class="m2"><p>بر تاج کی و جامه زرتار بخندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در ماتم پروانه اگر شمع بگرید</p></div>
<div class="m2"><p>بر سوختن خویش بناچار بخندد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوق حشم لیلیش از بس که بسر هست</p></div>
<div class="m2"><p>مجنون تو در وادی خونخوار بخندد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون آینه اش کاشف اسرار یقین شد</p></div>
<div class="m2"><p>منصور عجب نیست که بر دار بخندد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یعقوب کند گریه بیوسف همه عمر</p></div>
<div class="m2"><p>یوسف بخود اندر سر بازار بخندد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با گریه ابر و اثر ناله بلبل</p></div>
<div class="m2"><p>نبود عجب ار غنچه بگلزار بخندد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرسند شده زاهد از تولیت وقف</p></div>
<div class="m2"><p>کر کس چو رسد بر سر مردار بخندد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آشفته ثناخوان تو شد ایشه مردان</p></div>
<div class="m2"><p>بر حالت او دشمن مگذار بخندد</p></div></div>