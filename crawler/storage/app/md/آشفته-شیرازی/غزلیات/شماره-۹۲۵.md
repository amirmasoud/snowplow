---
title: >-
    شمارهٔ ۹۲۵
---
# شمارهٔ ۹۲۵

<div class="b" id="bn1"><div class="m1"><p>ساقیا من که خرابم ز نو آبادم کن</p></div>
<div class="m2"><p>تشنه‌ام خضری و از جرعهٔ امدادم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زازل گرچه نهادند مرا بنیان کج</p></div>
<div class="m2"><p>راست خواهی تو خرابم کن و آبادم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وجود من خاکی نبود آب طرب</p></div>
<div class="m2"><p>بکش از خاک در میکده ایجادم کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه شب همنفس مرغ گلستانم من</p></div>
<div class="m2"><p>یکنفس ای گل نو گوش بفریادم کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب از ساز حقیقت بزن این پرده</p></div>
<div class="m2"><p>ساقی از رنگ تعلق زمی آزادم کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بچم ای گلبن مه روی سهی قد در باغ</p></div>
<div class="m2"><p>فارغ از جلوه سرو و گل و شمشمادم کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو اجازت که بسر برد در تو طوف کنم</p></div>
<div class="m2"><p>همتی بدرقه طوسم و بغدادم کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسمل تیر نظر منتظر یک نگهست</p></div>
<div class="m2"><p>فارغ از کمکش دام و زصیادم کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیستون غم عشق تو بناخن کندم</p></div>
<div class="m2"><p>بده آن تیشه خونریزم و فرهادم کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهروان جمله بکعبه من آشفته بدیر</p></div>
<div class="m2"><p>آخرای پیر خرابات تو ارشادم کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای تا سر غمم از کثرت اغیار بیا</p></div>
<div class="m2"><p>بی خبر از درم و از همه غم شادم کن</p></div></div>