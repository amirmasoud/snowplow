---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>شمع گر هر نفسی انجمنی بگزیند</p></div>
<div class="m2"><p>چشم پروانه بجز پرتو او کی بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مور خط بر لب شیرین تو گر کرده هجوم</p></div>
<div class="m2"><p>مور گو خوشه ای از خرمن حسنت چیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآشیان بلبل شیدا چو پرافشان گردد</p></div>
<div class="m2"><p>حاش لله که بجز با گل خود بنشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره پیوست چو با بحر و چو ذره با مهر</p></div>
<div class="m2"><p>نسزد گر خودی خود زمیانه بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روح آشفته که ازخاک درت پرورده</p></div>
<div class="m2"><p>گرد تن تا نفشاند بدرت ننشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر سپرده به تو درویش تو ای دست خدا</p></div>
<div class="m2"><p>به ولایت که اگر جز تو ولی بگزیند</p></div></div>