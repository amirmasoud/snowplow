---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>رعنا غزالم فصل بهار است</p></div>
<div class="m2"><p>مشکوی گلزار رشگ تتار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی چو نرگس مخمور باشی</p></div>
<div class="m2"><p>در جام لاله می خوشگوار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر عمر رفته افسوس تا چند</p></div>
<div class="m2"><p>عید نو آمد عمر دو بار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابرست و ژاله باغ است و لاله</p></div>
<div class="m2"><p>این میفروش و آن میگسار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل پادشه وار بنشسته بر تخت</p></div>
<div class="m2"><p>شد آن که گفتی شوکت به خار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشبو نسیمی آمد زبستان</p></div>
<div class="m2"><p>گوئی بر آتش عود قمار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی زدوری کن عمر تازه</p></div>
<div class="m2"><p>کاین دور دوران بی اعتبار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از جم چه جوئی جام می آورد</p></div>
<div class="m2"><p>زر یا سفالین زو یادگار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در کش پیاپی جام لبالب</p></div>
<div class="m2"><p>دوران نگوئی بر یک مدار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از جان فرو شو رنگ تعلق</p></div>
<div class="m2"><p>کاین گرد هستی در ره غبار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نوروز آمد فیروز و خرم</p></div>
<div class="m2"><p>از فر حیدر چون تاجدار است</p></div></div>