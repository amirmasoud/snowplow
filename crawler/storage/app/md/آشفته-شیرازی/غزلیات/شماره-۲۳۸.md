---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>گر تو اقامت کنی به آن قد و قامت</p></div>
<div class="m2"><p>روز قیامت عیان کنی ز کرامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون روم از کوی تو به پند و نصیحت</p></div>
<div class="m2"><p>من که وطن کرده‌ام به کوی ملامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامن وصلت ز دست بیهده دادیم</p></div>
<div class="m2"><p>بر سر خود بیختیم خاک ندامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدر بهشت وصال هرکه ندانست</p></div>
<div class="m2"><p>سوخت زنار فراق تو به غرامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاهد روشن نداشت چون مه رویت</p></div>
<div class="m2"><p>دعوی سرو چمن که کرد به قامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محرم کعبه گرت به باد ببیند</p></div>
<div class="m2"><p>عمره و حج را به دل کند با قامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا که کنی عرضه کشتگان خود ای شوخ</p></div>
<div class="m2"><p>محشر دیگر به پا کنی به قیامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گنج نیاید هرآنکه رنج نبیند</p></div>
<div class="m2"><p>گو بنشیند طالبان سلامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلطنت باغ خلد برد مسلم</p></div>
<div class="m2"><p>هرکه ز داغت به جبهه داشت علامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست ازل از کرم در اول نوروز</p></div>
<div class="m2"><p>بر سر حیدر نهاد تاج کرامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر شده آشفته بنده تو عجب نیست</p></div>
<div class="m2"><p>بندگی تو بزرگی است و فخامت</p></div></div>