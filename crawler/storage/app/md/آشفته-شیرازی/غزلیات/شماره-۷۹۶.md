---
title: >-
    شمارهٔ ۷۹۶
---
# شمارهٔ ۷۹۶

<div class="b" id="bn1"><div class="m1"><p>آتش کیست که من دیگ صفت در جوشم</p></div>
<div class="m2"><p>با همه جوش چرا غنچه صفت خاموشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرب شمع است بلی آفت پروانه ولی</p></div>
<div class="m2"><p>سر رود بر سر این کار بجان میکوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده و بندی عشقت نه من امروز شدم</p></div>
<div class="m2"><p>کز ازل حلقه زلف تو بود در گوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر برم لب بلب جام شبی بی لب تو</p></div>
<div class="m2"><p>گر می صاف بود خون جگر مینوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون از این دلق ریائی نشدم حاصل هیچ</p></div>
<div class="m2"><p>میروم خرقه و سجاده بمی بفروشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر رود سر بسر مهر تو چون شمع مرا</p></div>
<div class="m2"><p>کی توانم که بدل آتش عشقت پوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بخود وصف تو گفتن نتوانم چون نی</p></div>
<div class="m2"><p>هر چه نائی بدمد بردم من مینوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا باده کشیدیم بود هوش زدا</p></div>
<div class="m2"><p>می عشق تو بنازم که فزاید هوشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست بر دوش من آشفته گناه دو جهان</p></div>
<div class="m2"><p>تا مگر دست خدا بار برد از دوشم</p></div></div>