---
title: >-
    شمارهٔ ۱۲۰۷
---
# شمارهٔ ۱۲۰۷

<div class="b" id="bn1"><div class="m1"><p>ترک من از می اغیار مگر سرمستی</p></div>
<div class="m2"><p>که مرا توبه و پیمانه و دل بشکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیو سازند رقیبان و توئی حور سرشت</p></div>
<div class="m2"><p>نور محضی تو بظلمت زچه رو پیوستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیا زآتش می پرده پندار بسوز</p></div>
<div class="m2"><p>تا که بر دنیی و عقبی بفشانم دستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیستم کن بیکی جرعه چنان الباقی</p></div>
<div class="m2"><p>تا از این پس نزنم لاف گزاف از مستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جستی از زلفش و در حلقه خط افتادی</p></div>
<div class="m2"><p>بر خود ای دل تو نبندی که زبندش رستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل تو در کف گلچین بود و همدم خار</p></div>
<div class="m2"><p>بیهده بلبل شیدا زطرب برجستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکند زیست دمی بیش بر آتش هندو</p></div>
<div class="m2"><p>تو بر آتشکده ای خال چه خوش بنشستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا ببستی دل آشفته بآن زلف رسا</p></div>
<div class="m2"><p>رشته الفتش از هر دو جهان بگسستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر در میکده رحمت حق رخت ببند</p></div>
<div class="m2"><p>تا نگویند که تو رخت بکعبه بستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درگه پیر مغان دشت نجف مظهر حق</p></div>
<div class="m2"><p>که توان گفت به افلاک که پیشش پستی</p></div></div>