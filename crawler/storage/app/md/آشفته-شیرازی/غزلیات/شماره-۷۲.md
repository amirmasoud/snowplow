---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>به چهره‌ات شده آن زلف مشک‌فام نقاب</p></div>
<div class="m2"><p>بدان صفت که حواصل به زیر پر عقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را بزلف نژند آمده حجاب جمال</p></div>
<div class="m2"><p>که دود تیره بر آتشکده شده است حجاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دل خراب تو شد زاولین نظر ای عشق</p></div>
<div class="m2"><p>برای چیست که لشکرکشی بملک خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زسیم خام تو ابریشم دلم بگسیخت</p></div>
<div class="m2"><p>کمند تافته شصت خم بچهره متاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بباغبان که بگوید حدیث از آن لب لعل</p></div>
<div class="m2"><p>که خون خلق بخورد آن دو غنچه سیرآب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی گزند دلم زلف بر رخت لرزان</p></div>
<div class="m2"><p>چو عقربی که برآید بسیر در مهتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهان تنگ تو رانیست جای گفت و شنید</p></div>
<div class="m2"><p>که فارغم زسؤالش که نیست جای جواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا سفینه دلرا سکون پدید آید</p></div>
<div class="m2"><p>مرا که مردم دیده فتاده در غرقاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را که آب حیات است در کف ایساقی</p></div>
<div class="m2"><p>بیا و تشنه لبی را بجرعه دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را که نفس پرستی چه لاف حق گوئیست</p></div>
<div class="m2"><p>مگو که باشد آشفته مدعی کذاب</p></div></div>