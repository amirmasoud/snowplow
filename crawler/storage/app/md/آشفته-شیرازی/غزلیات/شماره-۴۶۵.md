---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>قضای عشرت ماه صیام باید کرد</p></div>
<div class="m2"><p>شراب سی شبه امشب بجام باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریخت خون خم ار زاهدی زتیغ هلال</p></div>
<div class="m2"><p>بحکم شرع از او انتقام باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را چه سود زسی روز و شب قیام و قعود</p></div>
<div class="m2"><p>قعود زآن و بر این یک قیام باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدام خشک لب از روزه بودنت تا کی</p></div>
<div class="m2"><p>دوای خشکی لب از مدام باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلال عید چو از طرف بام رخ بنمود</p></div>
<div class="m2"><p>چو ماه جلوه بر اطراف بام باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود حرام می و میکده است بیت حرام</p></div>
<div class="m2"><p>الا طواف به بیت الحرام باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام میکده میخانه محبت عشق</p></div>
<div class="m2"><p>بکدیه جرعه آن می بجام باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شدی چو مست از آن باده همچو آشفته</p></div>
<div class="m2"><p>بکوی ساقی کوثر سلام باید کرد</p></div></div>