---
title: >-
    شمارهٔ ۱۰۸۸
---
# شمارهٔ ۱۰۸۸

<div class="b" id="bn1"><div class="m1"><p>فریب صید آن نخجیر خوردی</p></div>
<div class="m2"><p>که از صیاد دیگر تیر خوردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخوردستی بطفلی شیر مادر</p></div>
<div class="m2"><p>که خون عاشقان چون شیر خوردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اول نظره عشقش شهیدی</p></div>
<div class="m2"><p>مگر تیر نظر را دیر خوردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عبث افتاده ای در دام زاهد</p></div>
<div class="m2"><p>فریب رشته تزویر خوردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آن مستی که هشیارت نبود</p></div>
<div class="m2"><p>شراب عشق بی تغییر خوردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرا پا کیمیائی ای مس قلب</p></div>
<div class="m2"><p>زخاک میکده اکسیر خوردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل دیوانه از زلفش رمیدی</p></div>
<div class="m2"><p>همانا صدمه از زنجیر خوردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخود باز آمدی از مستی عشق</p></div>
<div class="m2"><p>چه کم این کیف بی تأثیر خوردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ززخمت خون چو می میجوشد ایدل</p></div>
<div class="m2"><p>مگر از چشم مستش تیر خوردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بصورت در نگنجد یار کاخر</p></div>
<div class="m2"><p>فریب پرده تصویر خوردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پریشانی مدام آشفته با عشق</p></div>
<div class="m2"><p>ز یک پستان همان شیر خوردی</p></div></div>