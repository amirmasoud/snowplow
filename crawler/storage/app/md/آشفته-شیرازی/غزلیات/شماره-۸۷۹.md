---
title: >-
    شمارهٔ ۸۷۹
---
# شمارهٔ ۸۷۹

<div class="b" id="bn1"><div class="m1"><p>نیمه شب ای برق آه من شرری زن</p></div>
<div class="m2"><p>شعله ببالا و پست و خشک و تری زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو چو پروانه شمع چهره نهان کرد</p></div>
<div class="m2"><p>جهد کن و خویش را تو بر شرری زن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا بکی ایجان به بند جسم اسیری</p></div>
<div class="m2"><p>از قفس ای مرغ بسته بال پری زن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقرب جراره تو ای خم گیسو</p></div>
<div class="m2"><p>تکیه که گفته تو را که بر قمری زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خم مو خون بناف آهوی چین کن</p></div>
<div class="m2"><p>طعنه ای ای لب به بسته شکری زن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن لب شکرفشان بکام رقیب است</p></div>
<div class="m2"><p>همچو مگس دست حسرتی بسری زن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با لب او گفت دل که هیچ کدام است</p></div>
<div class="m2"><p>گفت برو این لطیفه بر دگری زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حالت یوسف مگو مگر بر یعقوب</p></div>
<div class="m2"><p>شکوه برو از پسر تو بر پدری زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاجت تیر و کمان بکشتن من نیست</p></div>
<div class="m2"><p>بسمل خود را بناوک نظری کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشق بی سیم و زر بهیچ نیرزد</p></div>
<div class="m2"><p>جهد کن آشفته خود بسیم و زری زن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر زر و سیمت بود نثار کن و خیز</p></div>
<div class="m2"><p>دست بدامان سرو سیمبری زن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سیم تنان گر کشند سر زتو آنگاه</p></div>
<div class="m2"><p>شکوه از ایشان برو بدادگری زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه ولایت علی وصی پیمبر</p></div>
<div class="m2"><p>بر در او چهره ای بسای سری زن</p></div></div>