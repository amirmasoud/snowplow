---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>تا چند آیم بر درت با عجز و لابه نیم‌شب</p></div>
<div class="m2"><p>بینم ترا با مدعی مست می و گرم طرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تابم کجا با مهر تو من شبنم و خور چهر تو</p></div>
<div class="m2"><p>تو آتشی و من نیم تو ماهتاب و من قصب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سندان دل و سیمین تنی در سیم داری آهنی</p></div>
<div class="m2"><p>داری به خلد اهریمنی بس ساحری ای بوالعجب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالات نخل بارور وز لب رطب آورده بر</p></div>
<div class="m2"><p>وان غبغب چون سیم تر چون کوزه نخل رطب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد چو مویت مشک تر مشک ار کشد مه را به سر</p></div>
<div class="m2"><p>تابد چو روی تو قمر گر مه کند عنبر سلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جمع خیل دلبران تو شاه و باقی چاکران</p></div>
<div class="m2"><p>در دفتر جان‌پروران روی تو فرد منتخب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من خود نبینم پیش تو چون نوش نوشم نیش تو</p></div>
<div class="m2"><p>قربان منم در کیش تو بر من چه می‌آری غضب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف تو هندو ای پسر هندو کشد گر خور ببر</p></div>
<div class="m2"><p>خالت بود زنگی اگر زنگی ز مه دارد نسب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من خود گدا تو محتشم ای خاک کویت جام جم</p></div>
<div class="m2"><p>در منزلت شاه عجم در مرتبت میر عرب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو بوتراب ای پادشه خصمت بود خاک سیه</p></div>
<div class="m2"><p>بس فرق باشد در شبه از مصطفی تا بولهب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشفته خواهد چون غبار آرد به کوی گو گذار</p></div>
<div class="m2"><p>برخیز و اسبابش بیار ای آفرینش را سبب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا رب آمن روعتی یا صاحبی فی غربتی</p></div>
<div class="m2"><p>یا مونسی فی وحشتی یا راحتی عند التعب</p></div></div>