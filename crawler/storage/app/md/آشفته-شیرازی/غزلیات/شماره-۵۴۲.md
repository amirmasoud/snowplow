---
title: >-
    شمارهٔ ۵۴۲
---
# شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>خیمه زد لیلی گلی باز بطرف گلزار</p></div>
<div class="m2"><p>ابر چون دیده مجنون بچمن شد خونبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به بندد در این فصل دکان عطاران</p></div>
<div class="m2"><p>باز کن نافه از آن زلف دو تا در گلزار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه زد شانه صبا زلف بنفشه زنسیم</p></div>
<div class="m2"><p>اندر این ملک دگر کس نخرد مشک تتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله چون ساقی مستان می گلگون در کف</p></div>
<div class="m2"><p>تا که از نرگس مخمور کند رفع خمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار با گل شده همدوش برغم بلبل</p></div>
<div class="m2"><p>چون رقیبی که کند جای به پهلوی نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باغبان فکر گلابست و بیغما گلچین</p></div>
<div class="m2"><p>غنچه خون میخورد و خامش از افغان هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل نگهدار از آن غمزه خونریز که هست</p></div>
<div class="m2"><p>مست و خنجر بکف و عربده جوی و جرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلفت این رشته کفری که بهم پیوسته</p></div>
<div class="m2"><p>شیخ تسبیح کند پاره برهمن زنار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من و آن گلبن نوخیز که در گلشن دل</p></div>
<div class="m2"><p>هر نفس تازه زشوقش شودم فصل بهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خال هندو بچه ای گشته مقیم کوثر</p></div>
<div class="m2"><p>زلف بر بتکده ات وقف نموده زنار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وقتی از چشمت حال دل آشفته بپرس</p></div>
<div class="m2"><p>زآنکه بیمار خبردار شود از بیمار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ور نترسی تو زمن میبرمت شکوه بشاه</p></div>
<div class="m2"><p>شاه خیبر شکن آن حیدر عالیمقدار</p></div></div>