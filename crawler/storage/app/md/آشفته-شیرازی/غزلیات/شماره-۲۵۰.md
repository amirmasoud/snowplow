---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>دانی چه تمیز است میان تن و جانت</p></div>
<div class="m2"><p>تو جان جهانی و بود جسم جهانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تلخی جان باختنم کام نه تلخ است</p></div>
<div class="m2"><p>نامم ببری گر دم رفتن بزبانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظلمات خم زلف سیه من چو سکندر</p></div>
<div class="m2"><p>خضر است خطت چشمه خضر آب دهانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مهر منیر تو کجا ماه بتابد</p></div>
<div class="m2"><p>کی سرو چمد با قد چون سرو روانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سرو نیامیخته آن سنبل گیسو</p></div>
<div class="m2"><p>بر ماه کجا باشد ابروی کمانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر مشتری دین و دلت تاجر عشق است</p></div>
<div class="m2"><p>یکسان بنماید بنظر سود و زیانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تحسین بحسن تو بجز عجز نداریم</p></div>
<div class="m2"><p>این نکته مبین نشود جز به بیانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رویت که عیان دیده مگر عین حقیقت</p></div>
<div class="m2"><p>کذب است که گفتند که دیدیم عیانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته بداغ تو کند فخر بعالم</p></div>
<div class="m2"><p>گر داغ شده به که بجا هست نشانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جانان جهانی تو علی مظهر حقی</p></div>
<div class="m2"><p>جان بهر تو می‌خواهد آشفته به جانت</p></div></div>