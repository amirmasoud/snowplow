---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>آن لعل شکربار که صد بار نمک داشت</p></div>
<div class="m2"><p>بر قلب حریفان زخط سبز محک داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بود نمکزار چرا قند و شکر ریخت</p></div>
<div class="m2"><p>گر تنک شکر بود زچه شور نمک داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مسئله جزء دهانت سخنی گفت</p></div>
<div class="m2"><p>کاورد یقین هر که در این مسئله شک داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سرو خرامان نشنیدم زچمن خاست</p></div>
<div class="m2"><p>وین ماه سخن گوی ندیدم که فلک داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لعل تو نگین جم و خط بال فرشته</p></div>
<div class="m2"><p>جا اهرهن جادو بر بال ملک داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیگلشن رویت شب دوشین بگلستان</p></div>
<div class="m2"><p>جا مردم چشمم همه بر خار و خسک داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با ترک نگاه تو نتابد دل مسکین</p></div>
<div class="m2"><p>دل یکتن و او لشکر ترکان بکمک داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناچار گریزم به پناه شه مردان</p></div>
<div class="m2"><p>حیدر که ازو بیم سماتا بسمک داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته از آن غمزه جادو نبرد جان</p></div>
<div class="m2"><p>کاو راست بسی لشکر و این عجر زیک داشت</p></div></div>