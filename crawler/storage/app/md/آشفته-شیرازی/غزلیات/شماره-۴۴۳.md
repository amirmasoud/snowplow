---
title: >-
    شمارهٔ ۴۴۳
---
# شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>حال دل گشته دگرگون یا رب این سودا چه بود</p></div>
<div class="m2"><p>رفت نائی از میان اندرنی این غوغا چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوح کشتی ساخت و بگذشت از طوفان دریغ</p></div>
<div class="m2"><p>نوح را بشکست کشتی موج این دریا چه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بیابان هر که گم شد رهنمای اوست خضر</p></div>
<div class="m2"><p>خضر گمشد اندر این وادی در این صحرا چه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد مجنون و هنوزش داغ لیلی در درون</p></div>
<div class="m2"><p>جان فنا شد عشق باقی ماند در لیلا چه بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنگ مطرب برشکست این نغمه در پرده که زد</p></div>
<div class="m2"><p>سوخت جام از آتش می اندر این صهبا چه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ندیده عکس ساقی خنده ساغر زچیست</p></div>
<div class="m2"><p>ور ندیده مستی ما گریه مینا چه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منع ما زآن آفتاب چهره ای ناصح مکن</p></div>
<div class="m2"><p>در گذر ای مرغ شب طعن تو بر حربا چه بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خیال آن دهن تسنیم و کوثر هیچ نیست</p></div>
<div class="m2"><p>با هوای قامت او سدره و طوبی چه بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش یکشب بود کان چین زلف افتد بدست</p></div>
<div class="m2"><p>گفت بگذر زین هوس آشفته این سودا چه بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در درون سینه جز نور علی هرگز نتافت</p></div>
<div class="m2"><p>غیر نور حق بگو در طور و در سینا چه بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نفی و اثبات من و تو زاهد آورد این حدیث</p></div>
<div class="m2"><p>ورنه بس بود لا اله از پیش لا الا چه بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عرش فرش راه احمد بود و یارش در سرا</p></div>
<div class="m2"><p>رفتن معراج او در لیلة الاسری چه بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه بحیدر عهد بستی شیخنا اندر غدیر</p></div>
<div class="m2"><p>با همه کفرت بگو آن رب آمنا چه بود</p></div></div>