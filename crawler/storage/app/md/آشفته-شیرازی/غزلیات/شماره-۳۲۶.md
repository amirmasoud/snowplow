---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>یکدم آنرا که فراغت زدو عالم باشد</p></div>
<div class="m2"><p>گر بکف جام سفالین بودش جم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدم آنست که بر سیرت و خلق بشر است</p></div>
<div class="m2"><p>آدمیت نه همین صورت آدم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوری از نوع بنی آدم از آن میجویم</p></div>
<div class="m2"><p>که ندیدم یک ازین طایفه محرم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیو نفس ار بکشد آدم و شهوت بنهد</p></div>
<div class="m2"><p>میتوان گفت کز ارواح مکرم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مختلط بودن با خلق و ملوث نشدن</p></div>
<div class="m2"><p>در جهان بر نبی و آل مسلم باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبود سیرت و اخلاق سلیمان اورا</p></div>
<div class="m2"><p>اهرمن را بکف ار چند که خاتم باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمی از عشق مکن غفلت و غافل منشین</p></div>
<div class="m2"><p>که همه عیش جهان بسته باین دم باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طالب لیلیم آشفته چو مجنون در دشت</p></div>
<div class="m2"><p>وحشی آسا زبنی آدمم ازرم باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیلی ما که بود شیر خدا مظهر حق</p></div>
<div class="m2"><p>که طفیلی درش آدم و عالم باشد</p></div></div>