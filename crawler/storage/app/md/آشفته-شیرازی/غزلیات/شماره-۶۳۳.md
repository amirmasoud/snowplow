---
title: >-
    شمارهٔ ۶۳۳
---
# شمارهٔ ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>ای کرده آفتاب زروی تو تاب قرض</p></div>
<div class="m2"><p>عطری زخاک کوی تو کرده گلاب قرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه و ستاره را نرسد لاف روشنی</p></div>
<div class="m2"><p>جائی که نور از تو کند آفتاب قرض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل آب و رنگ کرده زرخساره تو وام</p></div>
<div class="m2"><p>کرده زچین زلف تو چین مشک ناب قرض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستان برند از دل خونین من کباب</p></div>
<div class="m2"><p>کرده زلعل نوش تو نشئه شراب قرض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی بمهر و ماه چرا ابر شد حجاب</p></div>
<div class="m2"><p>اینان زشرم روی تو کرده نقاب قرض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطرب نه وجد عارف از نغمه تو بود</p></div>
<div class="m2"><p>کاز عشق کرده نغمه چنگ و رباب قرض</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستغنی است زآب بقا آن لبان نوش</p></div>
<div class="m2"><p>آب خضر نکرده زموج سراب قرض</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مسکین دل از لب تو طلبکار بوسه ایست</p></div>
<div class="m2"><p>باید ادا نمود بحکم کتاب قرض</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود عجب که همچو کمان خم کنند پشت</p></div>
<div class="m2"><p>گر برنهی تو بر سر این نه حجاب قرض</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای دست حق حساب خلایق بدر شود</p></div>
<div class="m2"><p>آید اگر بمعرض یوم الحساب قرض</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاید گر از خرانه غیبش ادا کنی</p></div>
<div class="m2"><p>آشفته تو را که بود بیحساب قرض</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دولت چو بر نصاب رسد بایدش زکات</p></div>
<div class="m2"><p>من چون کنم که رفته ز حد نصاب قرض</p></div></div>