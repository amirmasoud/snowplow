---
title: >-
    شمارهٔ ۵۵۵
---
# شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>یارش مخوان که شکوه کند از جفای یار</p></div>
<div class="m2"><p>یا بر رضای خود نه پسندد رضای یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کار یار حمل کنی بر خطا خطاست</p></div>
<div class="m2"><p>باید صواب محض شماری خطای یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قربان کشند و خوان بنهند از برای دوست</p></div>
<div class="m2"><p>ما خویشتن بخون بکشیم از برای یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام حبیب کس نبرد پیش مدعی</p></div>
<div class="m2"><p>حاشا که با رقیب بگویم جفای یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هیهات کز جفا بنهم دامنش زدست</p></div>
<div class="m2"><p>با تیغ برندارم سر را زپای یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را هوای حور و قصور بهشت نیست</p></div>
<div class="m2"><p>جا کرده است در سر ما تا هوای یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باغ نعیم بی تو بود آتش جحیم</p></div>
<div class="m2"><p>طوطی و حور کس ننشاند بجای یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته شیخ شهر بذکر و نماز شب</p></div>
<div class="m2"><p>ما راست ورد صبح و شبانگه دعای یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفاق سربسر همه در سایه علی است</p></div>
<div class="m2"><p>ما افتاده سایه صفت از قفای یار</p></div></div>