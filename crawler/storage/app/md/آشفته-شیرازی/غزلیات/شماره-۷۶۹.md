---
title: >-
    شمارهٔ ۷۶۹
---
# شمارهٔ ۷۶۹

<div class="b" id="bn1"><div class="m1"><p>تو مگو که ناسزا بود خدایت ار بگفتم</p></div>
<div class="m2"><p>تو بتی و من برهمن سزد ار بسجده افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم آه آن گل زرد ببوستان صنعت</p></div>
<div class="m2"><p>تو چنین بپروریدی نه بخویشتن شکفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدل آتشم نهان بود و چو شمع شعله ای زد</p></div>
<div class="m2"><p>بفکنده پرده از راز که بارها نهفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهوای آنکه آئی تو بخلوت درونم</p></div>
<div class="m2"><p>همه پای تا بسر جان زغبار تن برفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخیال لعل نوشت که رقیب قوت جان کرد</p></div>
<div class="m2"><p>همه شب زنوک مژگان در آبدار سفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو که شیر کردگاری سگ خود مران خدا را</p></div>
<div class="m2"><p>سر خود نهاده بر دست بدرگهت بخفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخنی بجذبه آشفته مگو که کس نگیرد</p></div>
<div class="m2"><p>چو به خویش آمدی باز بگو که من نگفتم</p></div></div>