---
title: >-
    شمارهٔ ۳۵۸
---
# شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>تو آن نه ای که جفای تو دل بیازارد</p></div>
<div class="m2"><p>هلاکت غم تو جان رفته باز آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدهر چون شب هجر تو نیست پاینده</p></div>
<div class="m2"><p>اگر هزار چو یلدا شب دراز آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه حاجتست باکسیر کاوزرنابست</p></div>
<div class="m2"><p>مسی که آتش عشق تو در گداز آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر که صرصر عشقت بباغ و دشت وزد</p></div>
<div class="m2"><p>نه سرو کوه که باشد باهتزاز آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن بدار کند عشق تو سر منصور</p></div>
<div class="m2"><p>که کشتگان تو در حشر سرفراز آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و پرده برافکن بتا تو در فرخاز</p></div>
<div class="m2"><p>که سومنات به پیش توبت نماز آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر مدیح علی مینوشت آشفته</p></div>
<div class="m2"><p>که خامه اش همه اشعار دلنواز آرد</p></div></div>