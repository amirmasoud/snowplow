---
title: >-
    شمارهٔ ۵۸۷
---
# شمارهٔ ۵۸۷

<div class="b" id="bn1"><div class="m1"><p>بر گل فشاند غالیه از زلف مشک بیز</p></div>
<div class="m2"><p>ناسور شد جراحتت ایدل زجای خیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایشمع از شعاع جمالت جهان بسوخت</p></div>
<div class="m2"><p>اشکی سحر بماتم پروانه ات بریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایدل بخواند آیت فارالتنور چشم</p></div>
<div class="m2"><p>فرصت غنیمت است تو خاکی بسر به بیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شد که دل بحلقه زلفت پناه برد</p></div>
<div class="m2"><p>زخمی زبوی مشک اگر دارد احتریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تیغ امتحان زمیان آخت ترک من</p></div>
<div class="m2"><p>ممتاز کرد عاشق از اغیار بی تمیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مردم بطوف کعبه و این بوالعجب که من</p></div>
<div class="m2"><p>دیدم که طوف گرد بتی میکند حجیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن بت کدام نور خدا خانه زاد حق</p></div>
<div class="m2"><p>حیدر شفیع خیل خلایق برستخیز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمت بخاص و عام ببسته ره نظر</p></div>
<div class="m2"><p>زلفت بوحش و طیر به بسته ره گریز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز مدح حیدر ار سخن آشفته گفته ای</p></div>
<div class="m2"><p>دادی بسیم ناسره این یوسف عزیز</p></div></div>