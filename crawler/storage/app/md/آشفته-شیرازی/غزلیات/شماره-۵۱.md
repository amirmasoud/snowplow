---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>سینه شد زآتش دل جلوه گه طور مرا</p></div>
<div class="m2"><p>تا چه آید بدل از این سر پرشور مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده طوفان کند از شعله کانون درون</p></div>
<div class="m2"><p>گو بیا نوح و بخوان فار التنور مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پشه وادی عشقم به هما فخر کنان</p></div>
<div class="m2"><p>کند این عشق مخوان زاهد مغرور مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وامق ار گرد عذار تو ببیند خط سبز</p></div>
<div class="m2"><p>عذر عذرا نهد و دارد معذور مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در این دیر خرابم نبود آبادی</p></div>
<div class="m2"><p>کرده معمار غم عشق تو معمور مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکنم پیل بنخجیر گه عشق شکار</p></div>
<div class="m2"><p>گر بصورت نبود جثه عصفور مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاریم پیر مغان دید و جمم کرد بجام</p></div>
<div class="m2"><p>گر چه آشفته ندادند زرو زور مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیر میخانه توحید رضا قبله طوس</p></div>
<div class="m2"><p>که به چشم است ز خاک در او نور مرا</p></div></div>