---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی شراب خوشگواری کرده‌ام پیدا</p></div>
<div class="m2"><p>شراب خوشگوار بی‌خماری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز رنج باده دوشین حریف ار دردسر دارد</p></div>
<div class="m2"><p>به میخانه حریف میگساری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکست ار تار مطرب یا که مزمارش نمی‌خواند</p></div>
<div class="m2"><p>به قانون دگر در پرده تاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر صیاد ما را عار باشد از شکار ما</p></div>
<div class="m2"><p>برای صید دل‌ها جان‌شکاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا با مسافر شو ازین کشور بکش مفرش</p></div>
<div class="m2"><p>کزین عالم برون شهر و دیاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خزان چندان که می‌خواهد بتازد اسب در گلشن</p></div>
<div class="m2"><p>برای خویشتن من نوبهاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا یعقوب خاک راه یوسف را بکن سرمه</p></div>
<div class="m2"><p>که من این توتیا را از غباری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر گم کرده‌ای قاتل بیا ای کشته پیش من</p></div>
<div class="m2"><p>که خونت را من از دست نگاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهم برد منّت من ز عطاران پی نافه</p></div>
<div class="m2"><p>که من آهوی چین در مرغزاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رقیب زشت‌خو باید که باشد پاسبان او</p></div>
<div class="m2"><p>برای حفظ گلشن طرفه خاری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر تو حرف حق گفتی و راز از خلق ننهفتی</p></div>
<div class="m2"><p>شدی منصور و از بهر تو داری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخن بی‌پرده گو آشفته از زاهد چه می‌ترسی</p></div>
<div class="m2"><p>که من سرّ خدا را پرده‌داری کرده‌ام پیدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علی داماد پیغمبر در او سر خدا مضمر</p></div>
<div class="m2"><p>که از نسلش شه گلگون‌سواری کرده‌ام پیدا</p></div></div>