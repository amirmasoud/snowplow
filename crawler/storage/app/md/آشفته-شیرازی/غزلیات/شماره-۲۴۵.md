---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>گلزار مگوئید که قصری زبهشت است</p></div>
<div class="m2"><p>ساقی نه پری کادمی حور سرشت است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه باده بساغر بود و محمل مستان</p></div>
<div class="m2"><p>کان چشمه کوثر بود آن باغ بهشت است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جویند ترا شیخ و برهمن چه تفاوت</p></div>
<div class="m2"><p>کاین بر در کعبه بود آن رو به کنشت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد من و میخانه ملامت نکند سود</p></div>
<div class="m2"><p>در دفتر تقدیر ببین تا چه نوشت است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خشت از سر خم با ادب ای رند تن برگیر</p></div>
<div class="m2"><p>باشد سر جمشید مپندار که خشت است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خالیست مجاور بخم زلف سیاهت</p></div>
<div class="m2"><p>این دانه ندانم که بر آن دام که هشتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با این همه حسن و لطافت که چمن راست</p></div>
<div class="m2"><p>بی یار گلندام یکی خانه زشت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آشفته و مهر علی از حاصل اعمال</p></div>
<div class="m2"><p>در مزرعه دک بجز این تخم نکشت است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش حالت آن رند قلندر که بیادست</p></div>
<div class="m2"><p>اسباب بهشتی همه یکسوی بهشتست</p></div></div>