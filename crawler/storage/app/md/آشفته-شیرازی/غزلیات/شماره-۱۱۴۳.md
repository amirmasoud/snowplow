---
title: >-
    شمارهٔ ۱۱۴۳
---
# شمارهٔ ۱۱۴۳

<div class="b" id="bn1"><div class="m1"><p>گو چه کم آیدت زسلطانی</p></div>
<div class="m2"><p>گر عنان سوی ما بگردانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوکت سلطنت نیفزاید</p></div>
<div class="m2"><p>دل درویش اگر برنجانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دل و دین بباختم چه عجب</p></div>
<div class="m2"><p>نیست در عاشقی پشیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرخ حلوا مگر زیاده شود</p></div>
<div class="m2"><p>دامن ار بر مگس نیفشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینچنین چشم پرفسون که تر است</p></div>
<div class="m2"><p>گر بود کوه آتش بجنبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>توو پاکیزه دامنی در حسن</p></div>
<div class="m2"><p>من و در عشق پاکدامنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر فلاطون روزگاراستی</p></div>
<div class="m2"><p>که بدرمان عشق درمانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی زلف تو آید از دودم</p></div>
<div class="m2"><p>گر چو عودم شبی بسوزانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو چو شیرینی و منم فرهاد</p></div>
<div class="m2"><p>قصه از این و آن چه میخوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گو سگی هم در این سرا باشد</p></div>
<div class="m2"><p>چندم از خیل خویش میرانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا که زلف تو جان آشفته است</p></div>
<div class="m2"><p>نشود فارغ از پریشانی</p></div></div>