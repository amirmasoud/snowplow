---
title: >-
    شمارهٔ ۱۱۹۱
---
# شمارهٔ ۱۱۹۱

<div class="b" id="bn1"><div class="m1"><p>گر باد دی بگلشن دم میزند بسردی</p></div>
<div class="m2"><p>از باد دی بگرمی از می برآر گردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه از نوا و از زنگ گاهی زآب گلرنگ</p></div>
<div class="m2"><p>بگشای این دل تنگ بزدا زچهره زردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطرب بزن تو دستی ساقی بکوب پائی</p></div>
<div class="m2"><p>مینا بیار و بشکن این طاق لاجوردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوف حریم دلها از یکنظر توان کرد</p></div>
<div class="m2"><p>بیهوده کرده حاجی یکعمر رهنوردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندر حریم جانان بی درد ره ندارد</p></div>
<div class="m2"><p>ایدل اگر توانی از جان بجوی دردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفته باش اما اندر شکنج یک زلف</p></div>
<div class="m2"><p>دیوانه وار تا کی ایدل بهرزه گردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگر تو ای سکندر آب خضر نجوئی</p></div>
<div class="m2"><p>از آب عشق خوبان گر نیم جرعه خوردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آبشار وحدت خمخانه محبت</p></div>
<div class="m2"><p>کز یک نمش جهنم چون یخ شود بسردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب ولای حیدر آن شهسوار صفدر</p></div>
<div class="m2"><p>شاهی که کوفت نوبت در لامکان به مردی</p></div></div>