---
title: >-
    شمارهٔ ۷۱۸
---
# شمارهٔ ۷۱۸

<div class="b" id="bn1"><div class="m1"><p>در خرابات مغان تا که پناهی داریم</p></div>
<div class="m2"><p>بسموات و به اهلش همه راهی داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی سر و پای در میکده از پرتو جام</p></div>
<div class="m2"><p>بهتر از افسر جمشید کلاهی داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هجوم ار شکند قلب سپه لشکر خصم</p></div>
<div class="m2"><p>ما همه زخم بر گشته سپاهی داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دو زلفین رسن باز بیوسف گفتند</p></div>
<div class="m2"><p>کز زنخدان بسر راه تو چاهی داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشارت دو لب نوش تو گفتند بخط</p></div>
<div class="m2"><p>بر لب آب خضر مهر گیاهی داریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده پیر خراباتم و انعام مدام</p></div>
<div class="m2"><p>اگر از میر زمانه گه و گاهی داریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر درآئی بصف حشر بدست مخضوب</p></div>
<div class="m2"><p>کشتگانت همه گویند گواهی داریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست در کفه میزان بجز از کوه گناه</p></div>
<div class="m2"><p>از عمل می نتوان گفت که گاهی داریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قلزم مهر علی در دل ما موج زنست</p></div>
<div class="m2"><p>همچو آشفته اگر نامه سیاهی داریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بقلب سپه خصم شکستی آریم</p></div>
<div class="m2"><p>در کمین شب همه شب ناوک آهی داریم</p></div></div>