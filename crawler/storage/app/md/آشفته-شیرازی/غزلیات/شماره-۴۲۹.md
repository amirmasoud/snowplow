---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>ساقیا باده که ایام طرب می‌آید</p></div>
<div class="m2"><p>شوق در دل ز پی لهو و لعب می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاده زهد و ریا نیست به جز ماتم غم</p></div>
<div class="m2"><p>نازم آن آب کزو بوی طرب می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوجوانان به کرشمه دل پیران ببرند</p></div>
<div class="m2"><p>عشق و پیری ز منت از چه عجب می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبب مستی ما پرتو ساقیست به جام</p></div>
<div class="m2"><p>تا نگویی اثر از آب عنب می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دختر رز ادب آموخت ز چوب اندر خم</p></div>
<div class="m2"><p>زآن پی تربیت اهل ادب می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده بگرفت که تا پرده صوفی بدرد</p></div>
<div class="m2"><p>سالک از رایحه او به طلب می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بتابد به شبستان حریفان چون خور</p></div>
<div class="m2"><p>لاجرم در بر رندان همه‌شب می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوره عمر کند تازه به مستان ارچه</p></div>
<div class="m2"><p>جام را جان به همه دور به لب می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرچه راهست به عالم نسب و نام ولیک</p></div>
<div class="m2"><p>به جز از عشق که بی‌نام و نسب می‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر مسبب به جهانست خداوند حکیم</p></div>
<div class="m2"><p>عشق در عالم اسباب سبب می‌آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشق سرمد چه بود مطلع انوار ازل</p></div>
<div class="m2"><p>که گهی مظهر اسما و لقب می‌آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گاه سیف الله مسلول و گهی باب الله</p></div>
<div class="m2"><p>گاه آشفته علی میر عرب می‌آید</p></div></div>