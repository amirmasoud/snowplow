---
title: >-
    شمارهٔ ۷۸۹
---
# شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>بیهده عمری به صرف مهر خوبان کرده‌ایم</p></div>
<div class="m2"><p>درد بود است آنچه او را فکر درمان کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطمه‌ها چون گو بسی خوردیم از چوگان زلف</p></div>
<div class="m2"><p>بر سر میدان عشقت تا که جولان کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست در این شهر طفلی کاو نخوانده درس عشق</p></div>
<div class="m2"><p>خویشتن را تا ادیب این دبستان کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما که پیکان قضا را همچو پستان می‌مکیم</p></div>
<div class="m2"><p>تا چه‌ها در کودکی با شیر پستان کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاتم عشق تو زیب دست نامحرم چراست</p></div>
<div class="m2"><p>اهرمن‌وش تا خیانت با سلیمان کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زلیخا تهمتی از عشق بر خود بسته‌ایم</p></div>
<div class="m2"><p>یوسف خود را عبث در کند و زندان کرده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود قلندروار داده خرقه تقوی به می</p></div>
<div class="m2"><p>بر در میخانه بی‌جا عیب زندان کرده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هوای آن پری‌رو کز میان خلق رفت</p></div>
<div class="m2"><p>یک جهان جان را نثار راه دیوان کرده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کشد آن چشم مستم بر سر سیخ مژه</p></div>
<div class="m2"><p>خویش را بر آتشین‌روی تو بریان کرده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صرف زلف مهوشان آشفته کرده عمر خود</p></div>
<div class="m2"><p>خاطر خود را عبث ای دل پریشان کرده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سبحه بر زنار زلف گیسوان کرده بدل</p></div>
<div class="m2"><p>کفر را آورده‌ایم و نام ایمان کرده‌ایم</p></div></div>