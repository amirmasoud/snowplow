---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>دردمندان غم عشق تو را نیست طبیب</p></div>
<div class="m2"><p>مرگ بایست و یا داروی دیدار حبیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخمی یارم و مرهم نستانم از غیر</p></div>
<div class="m2"><p>شکوه از درد حبیبان نکنم پیش طبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکوه چندان نکنم من زجفای احباب</p></div>
<div class="m2"><p>داغ از آنم که بمن رحمتی آید زرقیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عود وعنبر چکنی عطر چه میسائی خیز</p></div>
<div class="m2"><p>بوی معشوق بعشاق بود خوشتر طیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نیم لایق انعام بده دشنامی</p></div>
<div class="m2"><p>گر عنایت نبود ساخته ام من به عتیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مرا بود دلی صبر بهجران کردم</p></div>
<div class="m2"><p>چون دل از دست بشد با چه کنم صبر و شکیب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشقت از کعبه و بتخانه چنان راند مرا</p></div>
<div class="m2"><p>که نه زاسلام بجاسبحه نه از کفر صلیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وه چه میخانه در رحمت حق خاک نجف</p></div>
<div class="m2"><p>که اگر عرش شود خاک درش نیست غریب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست آشفته گرای شه به عنانت نرسد</p></div>
<div class="m2"><p>آیدت گرد صفت ناچار دنبال رکیب</p></div></div>