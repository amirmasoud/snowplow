---
title: >-
    شمارهٔ ۵۴۸
---
# شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>خواند دل حورت و شد معترف اینک به قصور</p></div>
<div class="m2"><p>آن غلامی تو که بیرون کشی از جنت حور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چه با مردم دیده شده همخانه</p></div>
<div class="m2"><p>گر پری زآدمیانست به عالم مستور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قامتت را نتوان خواند که سرو چمنست</p></div>
<div class="m2"><p>عارف از همت عالی ننماید مقصور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طایر عقل فروریخت پر از صولت عشق</p></div>
<div class="m2"><p>پیش شهباز چه پرواز نماید عصفور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شب هجر حکایت مکن و روز فراق</p></div>
<div class="m2"><p>که رسد از پی شب و روز و پس از ظلمت نور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کرا چشم بود می‌نتوان گفت بصیر</p></div>
<div class="m2"><p>مگر آن را که نظر وقف بود بر منظور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودم که شکایت کنم از غیبت تو</p></div>
<div class="m2"><p>غیبتی نیست که گوییم حکایت به حضور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جز از عشق ندارد دل افسرده سماع</p></div>
<div class="m2"><p>مردگان را نکند زنده مگر نفخه حور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیر آن زلف بناگوش عیان شد گویی</p></div>
<div class="m2"><p>صبح عید است نمایان به شبان دیجور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقبت گرد لب لعل تو را خط بگرفت</p></div>
<div class="m2"><p>خاتم از دست سلیمان بستد لشکر مور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش‌افشان ز چه رو گشته زبانش در کام</p></div>
<div class="m2"><p>نشده سینه آشفته اگر وادی طور</p></div></div>