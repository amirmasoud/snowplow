---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دهد بباغ اگر جلوه قد دل جو را</p></div>
<div class="m2"><p>کناره جوی شود سرو بن لب جو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بغیر هندوی خال تو ای بهشتی روی</p></div>
<div class="m2"><p>نداده جای کسی در بهشت هندو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنند عید خلایق اگر بماه صیام</p></div>
<div class="m2"><p>زطرف بام نمائی هلال ابرو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمند رستمت ار باید و چه بیژن</p></div>
<div class="m2"><p>به بین بطرف زنخدان درست گیسو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را رسد که بروئین تنی زنی نوبت</p></div>
<div class="m2"><p>زره کنی چو بتن حلقه حلقه ی مو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روم بصیدگه آهوان که در نخجیر</p></div>
<div class="m2"><p>مگر خورم بغلط تیر غمزه او را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه جادوئیست خدا راکه چشم فتانت</p></div>
<div class="m2"><p>بر آفتاب کشیده است تیغ ابرو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتد بطره شیرین هر آنکه چون خسرو</p></div>
<div class="m2"><p>بمشک چین ندهد خاک کوی مشکو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زچهره پرده فروهل گره بزلف مزن</p></div>
<div class="m2"><p>که برگ گل بنهد رنگ و غالیه بو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنی خیال شبیخون اگر بترک نگاه</p></div>
<div class="m2"><p>بغمزه در شکنی اردوی هلاکو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زدست برد دل آشفته را خم زلفت</p></div>
<div class="m2"><p>بدان طریق که چوگان پادشه گو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدیو ایران کز بیم او بهامون شیر</p></div>
<div class="m2"><p>زمهر پرورد اندر کنار آهو را</p></div></div>