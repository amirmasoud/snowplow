---
title: >-
    شمارهٔ ۸۷۸
---
# شمارهٔ ۸۷۸

<div class="b" id="bn1"><div class="m1"><p>گو بمجنون کز من آموزد دگر درس جنون</p></div>
<div class="m2"><p>زآنکه دوشم خیمه زد لیلی بصحرای درون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشگ یعقوب از هوای یوسف آمد سوی مصر</p></div>
<div class="m2"><p>آب رود نیل اندر چشم قبطی کرد خون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوز عشقت جا گرفت در درون جان من</p></div>
<div class="m2"><p>وین نخواهد رفت از دل تا نیاید جان برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو کشی صهبای گلرنگ از ایاغ مدعی</p></div>
<div class="m2"><p>من خورم از خون دل هر شب شراب لاله گون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر افکندی بمن آمند غلط بر جان غیر</p></div>
<div class="m2"><p>وین نمی بینم مگر از تیره بخت واژگون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میکند آشفته گر زنجیر مجنون را علاج</p></div>
<div class="m2"><p>تو اسیر زلف یاری و جنونت شد فزون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دفع این سودا نباید از طبیبان جهان</p></div>
<div class="m2"><p>جز علی کاندر کف او بود امر کاف و نون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق بنشاند بدل بیخ هوس را برکند</p></div>
<div class="m2"><p>زانکه او دانا بود بر علم ما کان و یکون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که زنجیر جنون شد طره طرار او</p></div>
<div class="m2"><p>ما بدل کردیم عقل خویشتن را بر جنون</p></div></div>