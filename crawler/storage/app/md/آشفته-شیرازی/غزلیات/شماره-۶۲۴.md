---
title: >-
    شمارهٔ ۶۲۴
---
# شمارهٔ ۶۲۴

<div class="b" id="bn1"><div class="m1"><p>ساقیا فصل بهار است غنیمت دانش</p></div>
<div class="m2"><p>شیخ پیمانه شکن را بشکن پیمانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دور دوران ندهد هیچ گشایش ساقی</p></div>
<div class="m2"><p>افتتاحی بکن از جام می و دورانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک فانی چه محل دارد و عیش و طربش</p></div>
<div class="m2"><p>یار باقی طلب و صحبت جاویدانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه ابر ببین دیده خونبار بجوی</p></div>
<div class="m2"><p>عشوه گل چه خری و دهن خندانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه خون دل عشاق بخورد آن لب لعل</p></div>
<div class="m2"><p>گوئی آلوده بخون است در دندانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جرعه می بکف تست و مرا جان بر لب</p></div>
<div class="m2"><p>خیز ساقی بده آن جرعه و خوش بستانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر که منعم بنهد کز پس مرگش بدهند</p></div>
<div class="m2"><p>نوشدارو که پس از مرگ کند درمانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در دشت عمل تخم نکارد بشتاء</p></div>
<div class="m2"><p>چه تتمع برد از حاصل تابستانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که بر دامن حیدر نزند دست امروز</p></div>
<div class="m2"><p>گرچه نوح است بفردا ببرد طوفانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه بینی بجهان فانی و پایانش هست</p></div>
<div class="m2"><p>دولت سرمد عشق است و مجو پایانش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر عشق ار بلب آشفته بیارد چون شمع</p></div>
<div class="m2"><p>آتشی دارد نتوان که کند پنهانش</p></div></div>