---
title: >-
    شمارهٔ ۵۹۷
---
# شمارهٔ ۵۹۷

<div class="b" id="bn1"><div class="m1"><p>در آبحلقه مستان زننگ و نام مترس</p></div>
<div class="m2"><p>حریص دانه خالی زبند دام مترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بمصر محبت غلام عشق شدی</p></div>
<div class="m2"><p>هزار یوسف مصرت شود غلام مترس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بود بخرابات صد هزار خطر</p></div>
<div class="m2"><p>چو خضر ساقی دور است از این پیام مترس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آفتاب جمالش بصبح میتابد</p></div>
<div class="m2"><p>بسوز شمع زسر تا قدم تمام مترس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چه محتسب اندر کمین مستانست</p></div>
<div class="m2"><p>چو عکس ساقی دورت فتد بجام مترس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمهر و قهر علی گو مگو زجنت و نار</p></div>
<div class="m2"><p>بیاد طلعت مویش زصبح و شام مترس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصی خاص نبی مرتضی است باده بیار</p></div>
<div class="m2"><p>بگیر مذهب خاص وز گفت عام مترس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زشکرین لبش آشفته بوسی ار دهدت</p></div>
<div class="m2"><p>گشای روزه بحلوا و از صیام مترس</p></div></div>