---
title: >-
    شمارهٔ ۹۲۴
---
# شمارهٔ ۹۲۴

<div class="b" id="bn1"><div class="m1"><p>نیش غم تو نوش جان حنظل عشق قند من</p></div>
<div class="m2"><p>میل خوشی نمیکند خاطر دردمند من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط چو نبات خضر شد بر لب نوشخند تو</p></div>
<div class="m2"><p>وه که بمصر میرود تحفه نبات و قند من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف تو هر طرف کشان دل بقفای او دوان</p></div>
<div class="m2"><p>منکه بخود نمیروم گو نکشد کمند من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ببری رگم زپی بند به بند همچو نی</p></div>
<div class="m2"><p>قصه عشق بشنوی باز زبند بند من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل چاک چاک من خنده زنی و بگذری</p></div>
<div class="m2"><p>زآن نمکین دهان کنی چند تو ریشخند من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دید بخویش مست شد عاشق خودپرست شد</p></div>
<div class="m2"><p>دید در آینه چو خود آن بت خودپسند من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه زدوری رهت نیست مرا غمی که شد</p></div>
<div class="m2"><p>خاک در تو پرنیان خار رهت پرند من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاخت سمند فکرتم در هم عمرت از قفا</p></div>
<div class="m2"><p>لیک بگرد توسنت ره نبرد سمند من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیست قیاس و وهم را راه بآستان تو</p></div>
<div class="m2"><p>پاکتر است دامنت از چه و چون و چند من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرهم اگر نمی نهی زآن لب نوش بر دلم</p></div>
<div class="m2"><p>کیست که مرهم آورد بر دل مستمند من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدحت مرتضی بگو مرهم دل از او بجو</p></div>
<div class="m2"><p>چون دم عیسوی شمر گفته سودمند من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منکه جنون عشق را آشفته سر نهاده ام</p></div>
<div class="m2"><p>گو ندهند عاقلان بهر خدای پند من</p></div></div>