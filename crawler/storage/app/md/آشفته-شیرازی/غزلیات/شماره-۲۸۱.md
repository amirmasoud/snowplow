---
title: >-
    شمارهٔ ۲۸۱
---
# شمارهٔ ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>شیوه لاله رخان گر همه جور است و جفاست</p></div>
<div class="m2"><p>شکوه در مرحله عشق زمعشوق خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همگی عین صوابست خطای معشوق</p></div>
<div class="m2"><p>میشمارند وفا گرچه همه جور و جفاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به طبیبان چه حوالت کنیم چاره حبیب</p></div>
<div class="m2"><p>درد عشق است و مریضان تو را وصل شفاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنوائی بنوازم زکرم مطرب عشق</p></div>
<div class="m2"><p>که دل غمزده عاشق بی برگ و نواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواست کابین تو دل مادر ایام بگفت</p></div>
<div class="m2"><p>صبر و دین عقل و خرد مهر تو را شیر بهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غلط ار رفت زتو شکوه ببخشای بمن</p></div>
<div class="m2"><p>بخطاکار کرم کردن و اغماض کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه گلهای چمن خار بود در چشمم</p></div>
<div class="m2"><p>تا گلم را زستم خار جگردوز بپاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زگزند نظر خلق تو را تا چه رسید</p></div>
<div class="m2"><p>که نظر بستی و ما را همگی جا به فناست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه بیماری چشمان تو بر جان بخرم</p></div>
<div class="m2"><p>که بلا هر چه بعشاق ببارند رواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شفاخانه توحید دوایت جستم</p></div>
<div class="m2"><p>همه آمین ملایک پی امضای دعاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایشه ملک ازل ساقی میخانه علی</p></div>
<div class="m2"><p>جرم آشفته ببخشای که درویش سراست</p></div></div>