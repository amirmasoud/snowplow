---
title: >-
    شمارهٔ ۸۹۴
---
# شمارهٔ ۸۹۴

<div class="b" id="bn1"><div class="m1"><p>زاهدا ذوق بهشتت هست بر آن رو ببین</p></div>
<div class="m2"><p>چشمهٔ تسنیم و کوثر در دهان او ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نافه مشک ختن گر آید از آهو پدید</p></div>
<div class="m2"><p>یک لطیمه عنبرش پیرامن آهو ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیلهٔ هاروت و ماروتش نگر در چین زلف</p></div>
<div class="m2"><p>سحر بابل را عیان زان نرگس جادو ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبهٔ اهل دلش گیسو حجر خال سیاه</p></div>
<div class="m2"><p>قبلهٔ اهل نظر در آن خم ابرو ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حوری و غلمان چه خواهی آن رخ دلکش بخواه</p></div>
<div class="m2"><p>سدره و طوبی چه جویی آن قد دلجو ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آینه عیبت نگوید روبه‌رو از کف بهل</p></div>
<div class="m2"><p>تا بدانی زشتی خود در رخ نیکو ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شبان تیره تا کی روز می‌داری طمع</p></div>
<div class="m2"><p>صبح صادق را عیان زان حلقهٔ گیسو ببین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد دل شیدا شهید خنجر مژگان او</p></div>
<div class="m2"><p>صد سر سوداییش آویزهٔ آن مو ببین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا که چوگان کرده ترکم طرهٔ طرار زلف</p></div>
<div class="m2"><p>قرص خورشیدش اسیر صولجان چون گو ببین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می‌کنی تقریر وصف چین زلف آن پری</p></div>
<div class="m2"><p>بزم را آشفته از این گفت‌وگو خوشبو ببین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مظهر واجب بود مانا علی مرتضی است</p></div>
<div class="m2"><p>اسم او را مرتسم ای عارف اندر هو ببین</p></div></div>