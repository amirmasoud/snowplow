---
title: >-
    شمارهٔ ۵۳۴
---
# شمارهٔ ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>دیدی دلا که عهد شباب و طرب نماند</p></div>
<div class="m2"><p>آن نقشهای مختلف بوالعجب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخلی بود جوانی و او را رطب طرب</p></div>
<div class="m2"><p>پیری شکست شاخش و بروی رطب نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمرت بود کتان و قصب دور ماهتاب</p></div>
<div class="m2"><p>از تاب ماه تاب کتان و قصب نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگ طرب بر آب عنب بسته اند و بس</p></div>
<div class="m2"><p>خشکیده پاک تاک و اثر از عنب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن لب که بود بر لب جانان و جام می</p></div>
<div class="m2"><p>اینک بغیر نیمه جانش بلب نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روحی که همچو می رگ و پی سخت کرده بود</p></div>
<div class="m2"><p>رفت و بغیر سستیت اندر عصب نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پائید دیر چون شب یلدا شب فراق</p></div>
<div class="m2"><p>آمد صباح وصل و نشانی زشب نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عمری بخاکبوس شه طوس در طلب</p></div>
<div class="m2"><p>این آرزو بجان تو اندر طلب نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون خضر جا بچشمه حیوان گرفته ای</p></div>
<div class="m2"><p>بی برگیت زچیست که شاخ حطب نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست خدا زکعبه نقش بتان بشست</p></div>
<div class="m2"><p>در دل بغیر نقش امیرعرب نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از عمر رفته شکوه مکن جام می بگیر</p></div>
<div class="m2"><p>آب حیات هست چه غم گر رطب نماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشفته شد مجاور درگاه شاه طوس</p></div>
<div class="m2"><p>ماهی به دجله آمد آن تاب و تب نماند</p></div></div>