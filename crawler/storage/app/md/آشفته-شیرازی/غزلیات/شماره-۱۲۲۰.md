---
title: >-
    شمارهٔ ۱۲۲۰
---
# شمارهٔ ۱۲۲۰

<div class="b" id="bn1"><div class="m1"><p>حدیث درد دل ای باد با جانان من گفتی</p></div>
<div class="m2"><p>خطا کردی که این درد نهان با جان من گفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتم درد عشق از دل سپردم در میان جان</p></div>
<div class="m2"><p>در افغان دل که با جان قصه جانان من گفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبیبان درد رنجوران خود پوشند از یاران</p></div>
<div class="m2"><p>چرا با مدعی هم درد درمان من گفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت از بوستان و بر سرای او مجاور شد</p></div>
<div class="m2"><p>مگر با باغبان از لاله و ریحان من گفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبادا رنجه گردد یوسفم در مصر نیکویی</p></div>
<div class="m2"><p>چرا از بیت‌الاحزان و غم کنعان من گفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه سیلاب خونین بارد امشب ابر کهساری</p></div>
<div class="m2"><p>مگر با او حدیث از دیده گریان من گفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث از بحر عمان رفت و لؤلؤ در کنار او</p></div>
<div class="m2"><p>کنایت هم‌نشین از دیده و دامان من گفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمردم بر در میر مؤید رتبه کیوان</p></div>
<div class="m2"><p>خطاب آمد که امشب وصف از دربان من گفتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مِهر سر به مُهر دل مرا سینه نبود آگه</p></div>
<div class="m2"><p>تو فاش ای دیده با مردم غم پنهان من گفتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کشیدی قصه طولانی از آن زلف آشفته</p></div>
<div class="m2"><p>چه شد کامشب حکایت از سر و سامان من گفتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبانه می‌کشد آتش در افغان خلق در محشر</p></div>
<div class="m2"><p>به دوزخ گوییا یک شمه از عصیان من گفتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به جز حب علی کفر است در ایمان درویشان</p></div>
<div class="m2"><p>دلا خوش نکته‌ای از کفر و از ایمان من گفتی</p></div></div>