---
title: >-
    شمارهٔ ۱۲۲۹
---
# شمارهٔ ۱۲۲۹

<div class="b" id="bn1"><div class="m1"><p>آفرینش چیست بحر و پیش او گردون حبابی</p></div>
<div class="m2"><p>ما همه لب‌تشنگان و مانده بر نقش سرابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مدار کارها هیچ است باز از آن دهان گو</p></div>
<div class="m2"><p>نقش این هستی نماند رخت بر اندر خرابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب ار می‌پرستد هندویی من ابر گیسو</p></div>
<div class="m2"><p>کز شکنج ابر تو تابد شبانه آفتابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بپوشی هفت پرده باز رخ بی‌پرده داری</p></div>
<div class="m2"><p>نور سینا چون بتابد کی به جا ماند حجابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب ار این پرده بنوازد کسی عاقل نماند</p></div>
<div class="m2"><p>ساقی ار این باده پیماید نخواهد خضر آبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به ذوق عشق و مستی تو به شوق خودپرستی</p></div>
<div class="m2"><p>زاهدا تا بر که دشوار است اگر باشد حسابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نغمه‌ای برکش مطربا دستی برافشان</p></div>
<div class="m2"><p>شاهدا پایی بکوب و ساقیا آور شرابی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصل چون امشب میسر شد غم فردا چه باشد</p></div>
<div class="m2"><p>ور بهشتی نقد اگر در وعده‌ها باشد عذابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می بنوش و مدح حیدر گوی آشفته به مستی</p></div>
<div class="m2"><p>کاینچنین از هاتف غیبم به گوش آمد خطابی</p></div></div>