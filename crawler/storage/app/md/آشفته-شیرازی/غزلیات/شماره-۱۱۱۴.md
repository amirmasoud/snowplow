---
title: >-
    شمارهٔ ۱۱۱۴
---
# شمارهٔ ۱۱۱۴

<div class="b" id="bn1"><div class="m1"><p>دام براه مینهی طره کمند میکنی</p></div>
<div class="m2"><p>صید بود چو خانگی از چه ببند میکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که نهی در آتشم تا که کنی مشوشم</p></div>
<div class="m2"><p>خال نهی بعارض و زلف نژند میکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن خط و لعل دلنشین کرد شکر نبات بین</p></div>
<div class="m2"><p>چند نبات جوئی و پرسش قند میکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من زسواد مردمک زود سپندت آورم</p></div>
<div class="m2"><p>تا تو برفع چشم بد فکر سپند میکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم گشای باغبان و آن قد معتدل ببین</p></div>
<div class="m2"><p>سرو بگو که شرم کن سر چه بلند میکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فکر خطا حدیث گل پیش جمال گلبدن</p></div>
<div class="m2"><p>قصه زمشک یا خطش سهو تو چند میکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طایف کعبه رضا آشفته گو بسر رود</p></div>
<div class="m2"><p>چند بخانه خفته و فکر سمند میکنی</p></div></div>