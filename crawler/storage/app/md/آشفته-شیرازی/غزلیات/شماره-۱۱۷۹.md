---
title: >-
    شمارهٔ ۱۱۷۹
---
# شمارهٔ ۱۱۷۹

<div class="b" id="bn1"><div class="m1"><p>تا که بتخانه را حرم کردی</p></div>
<div class="m2"><p>همه را عابد ای صنم کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که بت در بغل نهان داری</p></div>
<div class="m2"><p>از چه رو سجده بر حرم کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گدای مغان شدی درویش</p></div>
<div class="m2"><p>خویش را شاه محتشم کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سفالین قدح فکندی می</p></div>
<div class="m2"><p>کاس چوبینه جام جم کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سگ میخانه را شدی همرنگ</p></div>
<div class="m2"><p>خود در آن خانه محترم کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای لیلی است در دل مجنون</p></div>
<div class="m2"><p>گر سراغش تو در حشم کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفی از وجد در طرب تو گمان</p></div>
<div class="m2"><p>بنواهای زیر و بم کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آفت ترک و فتنه ای بعرب</p></div>
<div class="m2"><p>رخنه در کشور عجم کردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه در گلخنی گرفتی جای</p></div>
<div class="m2"><p>غیرت گلشن ارم کردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شکوفه صبا چو خازن شاه</p></div>
<div class="m2"><p>دامن باغ پر درم کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاه امکان علی که هستی را</p></div>
<div class="m2"><p>بطفیلش خدا کرم کردی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جان آشفته را بشوی از زنگ</p></div>
<div class="m2"><p>کش بدل مهر خود رقم کردی</p></div></div>