---
title: >-
    شمارهٔ ۶۷۶
---
# شمارهٔ ۶۷۶

<div class="b" id="bn1"><div class="m1"><p>وصال دوست بمحشر زبس یقین دارم</p></div>
<div class="m2"><p>بزندگانی خود چون رقیب کین دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای اینکه بپوشم بعیب خود پرده</p></div>
<div class="m2"><p>هزار دلق ملمع در آستین دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو خاتم لب لعلت مرا بدست افتاد</p></div>
<div class="m2"><p>چو جم دو عالم در زیر این نگین دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون که خرمن حسنت زمهر و مه چربید</p></div>
<div class="m2"><p>گمان مدار که پرواز خوشه چین دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث زلف وی آشفته مینوشتم دوش</p></div>
<div class="m2"><p>بآستین همه گوئی غزال چین دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشاره کرد بابرو زغمزه چشمش و گفت</p></div>
<div class="m2"><p>کمان کشیده بقصد جهان کمین دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمود در ازلم جلوه ای و چهر نهفت</p></div>
<div class="m2"><p>بواپسین سر دیدار اولین دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باولین در کریاس عشق سودم سر</p></div>
<div class="m2"><p>که پا چو عیسی بر چرخ چارمین دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نماز و روزه و حج قبول از زاهد</p></div>
<div class="m2"><p>من و محبت حیدر عمل همین دارم</p></div></div>