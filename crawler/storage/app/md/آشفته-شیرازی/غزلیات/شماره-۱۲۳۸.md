---
title: >-
    شمارهٔ ۱۲۳۸
---
# شمارهٔ ۱۲۳۸

<div class="b" id="bn1"><div class="m1"><p>پارسی جامه بخوانید غزل‌های دری</p></div>
<div class="m2"><p>که برید آمد و آورد زری فتح هری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غازه کن چهره به هر هفت که شاه غازی</p></div>
<div class="m2"><p>غازیان را به غزا داد صلا حمله‌وری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همین فتح است هرات است تو را مژده وهم</p></div>
<div class="m2"><p>که از این فتح بسی ملک دول شد سپری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همت شه نه به این فتح کمین شد مقصور</p></div>
<div class="m2"><p>مکن ای وهم در این باب تو کوته‌نظری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عزیمت کندش عزم جهان گیر به رزم</p></div>
<div class="m2"><p>خنگ او بگذرد از بام ثریا و ثری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ بر پشت نهد شیر فلک را چو به رزم</p></div>
<div class="m2"><p>ناف هفتم ز پیش می‌کند آنجا سپری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مثل صارم تیزش چو درآید به میان</p></div>
<div class="m2"><p>از پسر قطع کند ربطه علاقه پدری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست بی‌جا که فلک منطقه دارد به میان</p></div>
<div class="m2"><p>می‌کند بندگیش کرده مجره کمری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر کند خصم تو در آب چو ماهی منزل</p></div>
<div class="m2"><p>می‌کند آب به اجزای وجودش شرری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور محب تو خورد زهر ز آب و حنظل</p></div>
<div class="m2"><p>حاش لله که بودشان اثری جز شکری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صیت عدل تو چو گردید بلندآوازه</p></div>
<div class="m2"><p>شد حصاری به عدم فتنه دور قمری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوبتی گو بزند نوبت فتح و نصرت</p></div>
<div class="m2"><p>تا کند رفع ز گوش فلک این رنج کری</p></div></div>