---
title: >-
    شمارهٔ ۱۰۹۷
---
# شمارهٔ ۱۰۹۷

<div class="b" id="bn1"><div class="m1"><p>فصل بهار و مستی و غوغای چنگ و نی</p></div>
<div class="m2"><p>مجنون بگو که لیلی بیرون کشد زحی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد آتشی بجان بخیلان زجام می</p></div>
<div class="m2"><p>ساقی که نام حاتم طائی نموده طی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف صفت هر آنکه در افتد بچاه عشق</p></div>
<div class="m2"><p>برخیزد از سریر و جم و تختگاه کی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن در مکان بجویدت این یک به لامکان</p></div>
<div class="m2"><p>بر ساکنان ارض و سما گم شده است پی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوخفته ای بخاک نجف یا بفوق عرش</p></div>
<div class="m2"><p>ای گلشن ولای تو فارغ زباد دی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی اگر حرم بطواف نجف شتاب</p></div>
<div class="m2"><p>تا چند های هوی کنی آشفته خیزهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منعم زعشق لاله رخان ای پدر مکن</p></div>
<div class="m2"><p>جز باده‌ام میار تو در پیش یا بنی</p></div></div>