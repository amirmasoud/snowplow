---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>برون از دیده غواص صد دریای خون آمد</p></div>
<div class="m2"><p>که تا یک گوهر ارزنده از دریا برون آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبس دلهای خونین کرده جا در نافه زلفش</p></div>
<div class="m2"><p>عجب نبود گر از آن موی مشکین بوی خون آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر زلف نژند تو کمند خاطر من شد</p></div>
<div class="m2"><p>که هر جا میکشد دل در قفایش بیسکون آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرنج از راستی کز سرو موزون بر بیالائی</p></div>
<div class="m2"><p>بمه سنجیده ام حسن رخت صدره فزون آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زکف شد دامن دلدار اکنون باد در دستم</p></div>
<div class="m2"><p>چها زین پس بمن یا رب زبخت واژگون آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه ره برم در شام هجران بر سر کویش</p></div>
<div class="m2"><p>اگر نه بوی زلف عنبرینش رهنمون آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر دیوانه زنجیر زلف آن پری روئی</p></div>
<div class="m2"><p>برت آشفته صد مجنون پی درس جنون آمد</p></div></div>