---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>بیا و پرده برانداز از جمال ای دوست</p></div>
<div class="m2"><p>به عاشقان بنما حسن لایزال ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتا ز عاشق صادق مپوش روی جمیل</p></div>
<div class="m2"><p>چرا که آینه شد مظهر جمال ای دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که در سم گلگون تو نشد سر خاک</p></div>
<div class="m2"><p>چگونه سر به در آرم ز انفعال ای دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حضور غیر به کویت ملالت از چه دهد</p></div>
<div class="m2"><p>که در بهشت ندیده است کس ملال ای دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به احتمال وفا عمرها به سر بردم</p></div>
<div class="m2"><p>نمانده است دگر جای احتمال ای دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد چشم من آن خال و زلف رشته جان</p></div>
<div class="m2"><p>کجا زره بردم کس به زلف و خال ای دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا دوهفته‌مها بی‌تو صبر هفته نبود</p></div>
<div class="m2"><p>چه شد که کار کشیده به ماه و سال ای دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درآ به خانه‌ام ای ماه مشتری‌غبغب</p></div>
<div class="m2"><p>برآر کوکب آشفته از وبال ای دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر مسیح که محتاج یک دم از لب تست</p></div>
<div class="m2"><p>وگر که خضر بود تشنه زلال ای دوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پناه ماست در آستان حضرت تو</p></div>
<div class="m2"><p>اگرچه هست مرا خصم بدسگال ای دوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر زمانه پلنگ‌افکنست و روبه‌باز</p></div>
<div class="m2"><p>بگیر سلسله سر ذوالجلال ای دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنون که پیر مغانم ز اهل میکده خواند</p></div>
<div class="m2"><p>به جام جم ندهد کاسه سفال ای دوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مزن دلا در دیگر بهر زه بهر طلب</p></div>
<div class="m2"><p>که نیست در دو جهان جز علی و آل ای دوست</p></div></div>