---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>حسن آن گوهر که عمانیش نیست</p></div>
<div class="m2"><p>عشق آن دریا که پایانیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سری کاو خالیست از سر عشق</p></div>
<div class="m2"><p>خانه ی باشد که بنیانش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم عاشق گر نبارد سیل اشک</p></div>
<div class="m2"><p>هست آن ابری که بارانیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجب سلطان عقلم جان بسوخت</p></div>
<div class="m2"><p>عشق را نازم که دربانیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد هستی دامنت آلوده کرد</p></div>
<div class="m2"><p>ای خوش آن رندی که دامانیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>واجب آمد حلم با امکان صبر</p></div>
<div class="m2"><p>آه از آن بیدل که امکانیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خم زلفش شد آشفته زدست</p></div>
<div class="m2"><p>این سر شوریده سامانیش نیست</p></div></div>