---
title: >-
    شمارهٔ ۱۲۱۶
---
# شمارهٔ ۱۲۱۶

<div class="b" id="bn1"><div class="m1"><p>توئی آن گل که معروفی بهر گلشن به بیرنگی</p></div>
<div class="m2"><p>اگر چه از تو دارد رنگ نقش کلک ارژنگی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زتو بس نقش پیدا و تو پنهان طرفه نقاشی</p></div>
<div class="m2"><p>بهر گل رنگ و بو دادی و معروفی به بیرنگی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطرها در بیابان طلب بس هست سالک را</p></div>
<div class="m2"><p>نترسد از هجوم خصم و رهزن غازی جنگی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سماع عاشقان از پرده عشق است ای صوفی</p></div>
<div class="m2"><p>نمی آرد بوجدش بانگ رود و زهره چنگی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو سلطان و همه امکان تو را خیل حشم باشد</p></div>
<div class="m2"><p>عجب دارم که چون جا کرده ای در دل باین تنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برد دل از پری پنهان و پیدا از بنی آدم</p></div>
<div class="m2"><p>ندیده دیده دوران چنین لولی بدین شنگی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه هر برگ گیاهی گل نه هر مرغی بود بلبل</p></div>
<div class="m2"><p>نه زآنها آید این بوی و زاینها آن خوش آهنگی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدیح مرتضی نور خدا میگویم آشفته</p></div>
<div class="m2"><p>چه حاجت مدح بوبکر و اتابک سعد بن زنگی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلوک ار میکنی اندر پی آل پیمبر رو</p></div>
<div class="m2"><p>نه درویشست هر ژولیده موی چرسی و بنگی</p></div></div>