---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>پرده بگرفت زرخ آن گل رعنا گستاخ</p></div>
<div class="m2"><p>نغمه برداشت زدل بلبل شیدا گستاخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت پرستان مگر از زشتی ما باخبر است</p></div>
<div class="m2"><p>که زرخ پرده کشید آن بت زیبا گستاخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش طور بود چهره اش دیده عبث</p></div>
<div class="m2"><p>بهر دیدار چرائی بتماشا گستاخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدان دار بپا کرده بقتل عاشق</p></div>
<div class="m2"><p>چون یهودان شده در کشتن عیسی گستاخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم ای طوطی خط بر چه زنی بر لب گفت</p></div>
<div class="m2"><p>هست بر تنک شکر مرغ شکرخا گستاخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب شیرین نفسی بر لب آشفته بنه</p></div>
<div class="m2"><p>که شکر بشکنهد این طوطی گویا گستاخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب خضر و دم عیسی بخرابات بجوی</p></div>
<div class="m2"><p>که در آن خانه نشد خضر و مسیحا گستاخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من زجنس دگرم نیستم از عالم خاک</p></div>
<div class="m2"><p>ماهیم میزنم از شوق بدریا گستاخ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیستم شب پره آخر که گریزم از مهر</p></div>
<div class="m2"><p>دوختم دیده بخورشید چو حربا گستاخ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه توئی اهرمن ایزلف و رخش بال ملک</p></div>
<div class="m2"><p>بر سر بال فرشته چو نهی پا گستاخ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قدمت خدمت آن باده فروش از لیست</p></div>
<div class="m2"><p>که کنون آدم و نوحند در آنجا گستاخ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عکسی ای ساقی توحید بیفکن در جام</p></div>
<div class="m2"><p>تا بنوشم زطرب ساغر صهبا گستاخ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شده غواص به عمان مدیح حیدر</p></div>
<div class="m2"><p>تا برآرم ز صدف لؤلؤ لالا گستاخ</p></div></div>