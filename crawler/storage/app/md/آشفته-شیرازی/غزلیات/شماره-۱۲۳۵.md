---
title: >-
    شمارهٔ ۱۲۳۵
---
# شمارهٔ ۱۲۳۵

<div class="b" id="bn1"><div class="m1"><p>مشتاق بود گوش به رودی و سرودی</p></div>
<div class="m2"><p>مطرب به نواساز بکن چنگی ورودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پرده عشاق مزن شور مخالف</p></div>
<div class="m2"><p>آهنگ مؤالف زن و وزر است سرودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاید که برد ضرب اصولت به حجازم</p></div>
<div class="m2"><p>کاز فرع نبرده است کسی رده بحدودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای شاهد شنگول پناهی تو بمستان</p></div>
<div class="m2"><p>برخیز و بجا آر قیامی و قعودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در میکده امشب همه ذکر ملکوت است</p></div>
<div class="m2"><p>دارند مگر خیل ملک قوس صعودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذاشت لبم بر لب و گفتیم بسی راز</p></div>
<div class="m2"><p>بانی همه شب بود مرا گفت و شنودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی بده آن قلزم مواج حقیقت</p></div>
<div class="m2"><p>کاین بحر سرابست همه بود و نبودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن جامه بیرنگ ده از کارگه عشق</p></div>
<div class="m2"><p>کاز رخت تعلق ننهم تاری و پودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شاهد قدسی زتو چون چشم بپوشم</p></div>
<div class="m2"><p>کم مردمک چشمی و در عین شهودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما سایه و خورشید توئی ای شه مردان</p></div>
<div class="m2"><p>ما جمله فروعیم و تو خود اصل وجودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مردود ابد بود چو شیطان زدر دوست</p></div>
<div class="m2"><p>جبریل نمیکرد اگر بر تو سجودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از دست یداله بکش سوی بهشتش</p></div>
<div class="m2"><p>آشفته که در نار عمل کرده خلودی</p></div></div>