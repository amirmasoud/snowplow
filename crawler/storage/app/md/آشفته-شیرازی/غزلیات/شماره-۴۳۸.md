---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>آتشی میلی به بالا می‌کند</p></div>
<div class="m2"><p>یا که برقی رو به صحرا می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا نشست آن آتشین چهره بزن</p></div>
<div class="m2"><p>یا بتم عزم تماشا می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه به بستان سر دایم مایل است</p></div>
<div class="m2"><p>سرو تو کی میل با ما می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه را با خار عشقت بستر است</p></div>
<div class="m2"><p>تکیه کی بر فرش دیبا می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهر یغما گر به ترکان شد شهیر</p></div>
<div class="m2"><p>ترک تو صد شهر یغما می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بایدش در انجمن چون شمع سوخت</p></div>
<div class="m2"><p>هرکه سرّی آشکارا می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بر عقلم به چستی چیره شد</p></div>
<div class="m2"><p>هرچه خواهد گو بهل تا می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناتمامی از قبول قابل است</p></div>
<div class="m2"><p>ورنه فاعل نقش زیبا می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زر شود مس از قبول کیمیا</p></div>
<div class="m2"><p>تا چه با ما مهر مولا می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرچه آشفته است اهل جنت است</p></div>
<div class="m2"><p>هرکه بر حیدر تولا می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیعیانت را تولا تام نیست</p></div>
<div class="m2"><p>گرنه ز اعدایت تبرا می‌کند</p></div></div>