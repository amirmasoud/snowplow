---
title: >-
    شمارهٔ ۲۹۸
---
# شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>منم که بندهٔ مملوکم و عبید جلالت</p></div>
<div class="m2"><p>چه جرم رفت که می‌رانیم ز بزم وصالت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرام باد مرا زندگی به شام فراقت</p></div>
<div class="m2"><p>بکش به بزم وصالت که خون بنده حلالت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فشاندم از مژه آبی که برنخیزد از آن گرد</p></div>
<div class="m2"><p>بهل که خاک شود تن بر آستان جلالت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خون دل شده میراب عمریش مژه من</p></div>
<div class="m2"><p>کنون که شد ثمری غیر گو مچین زنهالت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه خضر شوم غوطه‌ور به چشمه حیوان</p></div>
<div class="m2"><p>که چون سکندر لب تشنه‌ام به جام زلالت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من بر آینه‌ات گر نشسته گرد ملالی</p></div>
<div class="m2"><p>بشوی آینه از آب عفو گرد ملالت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلام حلقه‌به‌گوشت منم مرا مفروش</p></div>
<div class="m2"><p>اگرچه خلق جهانند هندوی خط و خالت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گمان مدار که کفران نعمت تو نمودم</p></div>
<div class="m2"><p>که هست قوت شبانروزیم ز خوان نوالت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زکات حسن ز درویش خویش باز گرفتی</p></div>
<div class="m2"><p>به آن گمان که فزاید مگر به گنج جمالت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی کمال چو دادت خدای مال ببخشای</p></div>
<div class="m2"><p>مباد اینکه رسد آفتی به عین کمالت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر به بزم خود آشفته را دوباره بخوانی</p></div>
<div class="m2"><p>دوام عمر بخواهد همی ز حیدر و آلت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علی که داد تمیزت علی که کرد عزیزت</p></div>
<div class="m2"><p>علی که داد جمالت علی که داد جلالت</p></div></div>