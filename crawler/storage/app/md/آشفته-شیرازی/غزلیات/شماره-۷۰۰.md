---
title: >-
    شمارهٔ ۷۰۰
---
# شمارهٔ ۷۰۰

<div class="b" id="bn1"><div class="m1"><p>مدتی بود که دور از در جانان بودم</p></div>
<div class="m2"><p>دور از آن روح ران صورت بیجان بودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صرف شد عمر درازم همه در ظلمت هجر</p></div>
<div class="m2"><p>خضر وش در طلب چشمه حیوان بودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلفش ار سلسله برپای دلم ننهادی</p></div>
<div class="m2"><p>همچو مجنون بجهان بی سر و سامان بودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم بر تو شمارم غم ایام فراق</p></div>
<div class="m2"><p>در شب وصل به تو واله و حیران بودم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که بیمار غم عشق تو شد این دل زار</p></div>
<div class="m2"><p>یعلم الله که اگر طالب درمان بودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وه که دستان خم زلف توام دست ببست</p></div>
<div class="m2"><p>گر بمردی بمثل رستم دستان بودم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرم اهرمن زلف تو نگرفتی دست</p></div>
<div class="m2"><p>خاتم لعل تو بوسیده سلیمان بودم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کعبه گو بازگشاید در رحمت بر من</p></div>
<div class="m2"><p>که همه عمر بپا خار مغیلان بودم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای زلیخا تو زندانی خود باز بپرس</p></div>
<div class="m2"><p>که چو یوسف بتک چاه زنخدان بودم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم ای صبح بناگوش چسانی بازلف</p></div>
<div class="m2"><p>گفت عمریست بشب دست و گریبان بودم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نکنم شکوه از آن زلف پریشان دیگر</p></div>
<div class="m2"><p>من خود آشفته زآغاز پریشان بودم</p></div></div>