---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>از عالمش چه غم که خداوند یار اوست</p></div>
<div class="m2"><p>کون و مکان همه بکف اختیار اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر حقیقتی که جهان غرقهٔ ویند</p></div>
<div class="m2"><p>چون بنگری به چشم یقین در کنار اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنون بعهد لیلی اگر عقل و دین بباخت</p></div>
<div class="m2"><p>لیلا بدشت و الهی از روزگار اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسوده از بهشت مقیمان گلشنش</p></div>
<div class="m2"><p>فارغ زنوشدارو مجروح خار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی که کیمیای سعادت بود کدام</p></div>
<div class="m2"><p>خاکی که گاه گاهی در رهگذار اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس که دم زند خلافت نه حد اوست</p></div>
<div class="m2"><p>این کار بیخلاف زحقست و کار اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امکان تمام کرده از نقش صورتش</p></div>
<div class="m2"><p>این قدرتی زصنعت صورت نگار اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او دست کردگار بود این کرامتش</p></div>
<div class="m2"><p>آیا چها به پنجه پروردگار اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عرش و فرش و کرسی و کروبیان مپرس</p></div>
<div class="m2"><p>بالجمله این تمامی خدمتگذار اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته رخ نساید بر درگه کسی</p></div>
<div class="m2"><p>جز بر در علی که خداوندگار اوست</p></div></div>