---
title: >-
    شمارهٔ ۱۰۲۸
---
# شمارهٔ ۱۰۲۸

<div class="b" id="bn1"><div class="m1"><p>ای لبت بر خون مردم تشنه چشمت گُرسَنه</p></div>
<div class="m2"><p>آن به بیداری بریزد خون و آن اندر سنه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوشن ترکان غازی آهنین شد تا کمر</p></div>
<div class="m2"><p>بر تو عنبر شد زره از فرق سر تا پاشنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم دیده غلط پنداشت خطی بر رخت</p></div>
<div class="m2"><p>آه مشتاقان اثر کردت مگر در آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه و خور پروانه‌سان آیند بر بام و درت</p></div>
<div class="m2"><p>گر بتابد پرتو شمعت شبی از روزنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پارس را کرده مسخر غمزه جادوی تو</p></div>
<div class="m2"><p>تاخت آرد بر سپاهان ترک چشمت یک‌تنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوخت زآه آتشیم خرقه و نبود عجب</p></div>
<div class="m2"><p>لاجرم آتش فتد در پنبه از آتش‌زنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کجا یرغو برم از ترک چشم کافرت</p></div>
<div class="m2"><p>خون هر مؤمن به گردن دارد و هر مؤمنه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وارهاند جان آشفته از این خون‌خوارگان</p></div>
<div class="m2"><p>آنکه سلمان را گرفت از شیر دشت ارژنه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر آن معرض که تابد آفتاب روز حشر</p></div>
<div class="m2"><p>جز به زیر سایه‌ات ما را پناهی هست نه</p></div></div>