---
title: >-
    شمارهٔ ۸۵۷
---
# شمارهٔ ۸۵۷

<div class="b" id="bn1"><div class="m1"><p>چندیست که از زمزمه عشق خموشم</p></div>
<div class="m2"><p>مطرب بزن آن پرده که چون نی بخروشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون میخورم و مهر خموشی بدهانم</p></div>
<div class="m2"><p>وقتست که همچون خم لبریز بجوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبریز چو شد خم زسرش باده بریزد</p></div>
<div class="m2"><p>بیهوده باخفای تو ای عشق چه کوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زمزمه عشق تو در گوش خرد هست</p></div>
<div class="m2"><p>این پند حکیمان همه باد است بگوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی لب میگون تو کافیست بمستی</p></div>
<div class="m2"><p>با هوش زدا چند بری عقلم و هوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیغام تو دوش از دهن غیر شنیدم</p></div>
<div class="m2"><p>بنگر که چو یک کاسه بود نیشم و نوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه دوش بر دوش تو بد دست کش غیر</p></div>
<div class="m2"><p>فریاد که امشب گذرد باز چو دوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر باده فروشم نخرد دفتر اعمال</p></div>
<div class="m2"><p>این دفتر باطل به که یا رب بفروشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پندم چه دهی پند ندارد اثر ای شیخ</p></div>
<div class="m2"><p>تا هوش بسر دارم پندت ننیوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آشفته بدل ذره از مهر علی هست</p></div>
<div class="m2"><p>گر بار گناه دو جهان هست بدوشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با مهر علی کس نشود زنجه زدوزخ</p></div>
<div class="m2"><p>در گوش خوش این نکته همیخواند سروشم</p></div></div>