---
title: >-
    شمارهٔ ۱۱۹۰
---
# شمارهٔ ۱۱۹۰

<div class="b" id="bn1"><div class="m1"><p>کند هر ملتی در بندگی بر قبله‌ای رویی</p></div>
<div class="m2"><p>چو نیکو بنگری دارند جمله رو بر ابرویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه تنها ذکر یاهو از لب نوشین به گوش آید</p></div>
<div class="m2"><p>که در وَجْدَند ذرات جهان با هایی و هویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زلف اوست شیدایی چه صحرایی چه دریایی</p></div>
<div class="m2"><p>تعالی یک جهان دارد اسیر هر خم مویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا آن نخل جان‌پرور به جوی دیده جا دارد</p></div>
<div class="m2"><p>نشاند باغبان گر سرو خود را بر لبِ جویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رند از چین زلفت نافه نه از آهوی چینی</p></div>
<div class="m2"><p>خطا باشد گرفته هرکه بر عشاق آهویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا زآن طره بو داری چرا در دیده نگذشتی</p></div>
<div class="m2"><p>جهانی را توانی زندگی دادن چو از بویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود آشفته را از هردو عالم رو به سوی تو</p></div>
<div class="m2"><p>اگرچه هرکه را بینی به عالم روست بر سویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا زخم درون مرحم ندارد در جهان اما</p></div>
<div class="m2"><p>لب نوشین دهان تو نهان کرده است دارویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا در آینه از می بشوی این زنگ خودبینی</p></div>
<div class="m2"><p>به روی مرتضی بنگر اگر مرد خداجویی</p></div></div>