---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>عید است مطرب را بگو چنگی به مزمر برکشد</p></div>
<div class="m2"><p>تا زهره در بزم فلک از وجد معجر برکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی صلا زد محتسب می در قدح کن برملا</p></div>
<div class="m2"><p>صوفی شکسته توبه و خواهد که ساغر برکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد که بودی زرد رو میدیدمش در جستجو</p></div>
<div class="m2"><p>گویا طبیبش گفته کاو زآن راح احمر برکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هل من مزید صوفیان از بزم شد بر آسمان</p></div>
<div class="m2"><p>هر کو کشیده ساغری خواهد مکرر برکشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آفتاب او میکشد پرده زعارض بر فلک</p></div>
<div class="m2"><p>چون هندوان خور بر جبین خط مزعفر برکشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نوبت پیر مغان گویند بر بام حرم</p></div>
<div class="m2"><p>نبود عجب واعظ که می بر فوق منبر برکشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن میفروش بزم دل آن ماورای آب و گل</p></div>
<div class="m2"><p>آنکو فلک را متصل گردن بچنبر برکشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن شهسوار لافتی آن تاجدار هل اتی</p></div>
<div class="m2"><p>کز دست قدرت درو غادر را زخیبر برکشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون دستاو شد دست حق نبود عجب کز معجزه</p></div>
<div class="m2"><p>در مشرق از مغرب اگر خورشید خاور برکشد</p></div></div>