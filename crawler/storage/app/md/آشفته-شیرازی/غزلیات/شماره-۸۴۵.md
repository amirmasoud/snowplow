---
title: >-
    شمارهٔ ۸۴۵
---
# شمارهٔ ۸۴۵

<div class="b" id="bn1"><div class="m1"><p>در گلستان محبت گل داغی دارم</p></div>
<div class="m2"><p>کز گل یاسمن باغ فراغی دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمد از تربت من لاله از آن فصل بهار</p></div>
<div class="m2"><p>تا بدانی بدل از عشق تو داغی دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو من قامت دلدار و چمن گلشن دل</p></div>
<div class="m2"><p>لوحش الله که عجب سروی و باغی دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طالب سینه سینا نشوم همچو کلیم</p></div>
<div class="m2"><p>تا بطور دل از آن شعله چراغی دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جم عهدم من درویش که از همت عشق</p></div>
<div class="m2"><p>بکف از باده گلرنگ ایاغی دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکرین لعل تو چون خال سیه دید بگفت</p></div>
<div class="m2"><p>طوطیان گرد من و میل بزاغی دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندهم آن سر زلفین پریشان از دست</p></div>
<div class="m2"><p>کز دل گمشده آشفته سراغی دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بود پیشه من عشق و بود عشق علی</p></div>
<div class="m2"><p>لله الحمد زهرکار فراغی دارم</p></div></div>