---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>مطرب به رقص آورده‌ای آن لعبت طناز را</p></div>
<div class="m2"><p>گو زهره بشکن در فلک از رشک امشب ساز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم اگر تو شاهدی زاهد گذارد زاهدی</p></div>
<div class="m2"><p>آری برقص از بی‌خودی صوفی شاهدباز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینند گر آهو بچین از تیر صیدش می‌کنند</p></div>
<div class="m2"><p>در چین و موی او ببین آهوی تیرانداز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من عاشق آن مهوشم مفتون موی دلکشم</p></div>
<div class="m2"><p>عاشق که رسوا می‌شود پنهان ندارد راز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آمد به باغ آن گل‌بدن با نغمهٔ صوت حسن</p></div>
<div class="m2"><p>ای گل تو بگریز از چمن بلبل مکش آواز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر تربتم روزی بیا از عشق برگو ماجرا</p></div>
<div class="m2"><p>بشنو زهر بندم جدا چون نی همین آواز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شهد ریزد در آن دو لب آشفته نبود بس عجب</p></div>
<div class="m2"><p>خواند اگر کان شکر کس بعد از این شیراز را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد عجایب‌ها بسی این عشق سحانگیز ما</p></div>
<div class="m2"><p>صعوه به جنگل می‌درد در دشت او شهباز را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من صید پر بشکسته‌ام در دام عشقت خسته‌ام</p></div>
<div class="m2"><p>چون بند بر پا بسته‌ام امکان کجا پرواز را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در عشقت ای پیمان‌گسل از گریه کی گردم کسل</p></div>
<div class="m2"><p>شب تا سحر با خون دل بیرون کنم غماز را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای شهسوار لافتی در دام نفسم مبتلا</p></div>
<div class="m2"><p>بر دفع سحر مفتری بگشا کف اعجاز را</p></div></div>