---
title: >-
    شمارهٔ ۱۱۳۷
---
# شمارهٔ ۱۱۳۷

<div class="b" id="bn1"><div class="m1"><p>بر دم تیغ کجت سر بنهم از خوشی</p></div>
<div class="m2"><p>تا مگر از مکرمت دست بخونم کشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل اگر بر گلی نغمه سرا شد بباغ</p></div>
<div class="m2"><p>روی تو گر بنگرد پیشه کند خامشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دزد بود هوشیار در گذر کاروان</p></div>
<div class="m2"><p>چشم تو ره میزند با همه بیهشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی صید وحوش دام فکنده به ره</p></div>
<div class="m2"><p>مرغ دلم میپرد تا تو بدامش کشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چمن ای شاخ گل با قد رعنا بچم</p></div>
<div class="m2"><p>تا نکند سروناز دعوی گردن کشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بر روی تو گل پرده خود میدرد</p></div>
<div class="m2"><p>پیش لبت غنچه را پیشه بود خامشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شایدت آشفته وار حال دگرگون شود</p></div>
<div class="m2"><p>از کف ساقی ما گر قدحی در کشی</p></div></div>