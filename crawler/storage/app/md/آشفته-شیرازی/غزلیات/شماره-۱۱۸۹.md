---
title: >-
    شمارهٔ ۱۱۸۹
---
# شمارهٔ ۱۱۸۹

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار جامی از آن مایه خوشی</p></div>
<div class="m2"><p>تا بیخ غم بسوزم از آن آب آتشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر داروی خوشی بقدح باشدت بیار</p></div>
<div class="m2"><p>زیرا که دل دیده بدوران آن خوشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش میزنی به پرده تو مطرب نوای عشق</p></div>
<div class="m2"><p>چون این نوا کسی نشنیده بدلکشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چشم یار حالت مستیت چون بود</p></div>
<div class="m2"><p>چون خون خلق خورده نگاهت بسرخوشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل که پیش گل بسراید غزل هزار</p></div>
<div class="m2"><p>پیش رخت چو غنچه کشد پیشه خامشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نشنوم نصحیت ارباب هوش را</p></div>
<div class="m2"><p>بگذار تا بمانم در خواب بیهشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته همچو مرغ بنخجیر گه پرد</p></div>
<div class="m2"><p>شاید باشتباه بدامش تو درکشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پندارم ای غلام ثناخوان حیدری</p></div>
<div class="m2"><p>ورنه کسی ندیده نگارین به این کشی</p></div></div>