---
title: >-
    شمارهٔ ۷۳۰
---
# شمارهٔ ۷۳۰

<div class="b" id="bn1"><div class="m1"><p>بر آن سرم که بگرد وفا و مهر نگردم</p></div>
<div class="m2"><p>که همچو ذره زمهر تو بر هوا شده گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیب عشق که گفت آخر الدواء الکی</p></div>
<div class="m2"><p>نشد علاج و شد این داغ درد بر سر دردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمانده باده بکاسه نه زر بکیسه خدا را</p></div>
<div class="m2"><p>برفت سرخی و مانده بجای چهره زردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منه تو دیگ تمنا چو کشتی آتش سودا</p></div>
<div class="m2"><p>که دیگ عشق نیاید بجوش زآتش سردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بغیر دردسر و صدمه خمار ندیدم</p></div>
<div class="m2"><p>که بود ساقی و این باده از کجاست که خوردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا و پرده بگردان دمی تو مطرب مجلس</p></div>
<div class="m2"><p>که زخم تازه کند زخمهای تار تو هر دم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوا گرفته دل آشفته را بسان کبوتر</p></div>
<div class="m2"><p>پرم زبام حرم تا که کشتنی گردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیال جستن از دام نفس و قید هوا را</p></div>
<div class="m2"><p>کنم به همت مردان که خود نه آن مردم</p></div></div>