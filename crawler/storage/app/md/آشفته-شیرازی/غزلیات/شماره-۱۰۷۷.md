---
title: >-
    شمارهٔ ۱۰۷۷
---
# شمارهٔ ۱۰۷۷

<div class="b" id="bn1"><div class="m1"><p>خلیلی صرمت حبال العهودی</p></div>
<div class="m2"><p>و احرقت قلبی بنار کعودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه آتش بجانم برافروخت عشقت</p></div>
<div class="m2"><p>که میسوزم اما نه پیداست دودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولو لم جرت دمعة من عیونی</p></div>
<div class="m2"><p>فما اطفاء النار ذات الوقودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا می نشست آتش عشقت از دل</p></div>
<div class="m2"><p>اگر چشمه اشک چشمم نبودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فی فیک عذب فرات و انی</p></div>
<div class="m2"><p>بظمان حیران بین النقودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را بر لب آب حیاتست اما</p></div>
<div class="m2"><p>من تشنه لب را نه بخشید سودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و اوجدت یوسفک فی بر نجد</p></div>
<div class="m2"><p>عزیز هو عند ابن سعودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تهی دست وارد شدم بر جنابت</p></div>
<div class="m2"><p>فیالیتنی مت قبل الورودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا خال هندو برخ جای دادی</p></div>
<div class="m2"><p>اما حرم الجنه للهنودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرا چون تو فرزند ناورد دیگر</p></div>
<div class="m2"><p>و ان کانت الدهر ذات الولودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا وصف تو دیده کرده سراپا</p></div>
<div class="m2"><p>و ان لم راتک بعین شهودی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگوئی که در کعبه غافل نشستم</p></div>
<div class="m2"><p>مع وجهک قبلتی فی السجودی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو ای عشق در کشور دل امیری</p></div>
<div class="m2"><p>و ما یوجد غیرک فی الوجودی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و قد ضاقت القلب و القصد عیشی</p></div>
<div class="m2"><p>خدا را بکش مطرب از دل سرودی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حرام است عیشی که در او نباشد</p></div>
<div class="m2"><p>نه گلبانگ چنگی نه آهنگ رودی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گره از سر زلفش ار باز کردی</p></div>
<div class="m2"><p>دل آشفته را از گره برگشودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ادیبم بود عشق و عشق است حیدر</p></div>
<div class="m2"><p>کجا نکته گیرد بنظمم حسودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زاشعار آشفته آهم برآمد</p></div>
<div class="m2"><p>خدایا رسان بر روانش درودی</p></div></div>