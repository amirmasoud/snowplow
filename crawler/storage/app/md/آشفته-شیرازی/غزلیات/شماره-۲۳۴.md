---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>این کجکله ترک قزلباش کدامست</p></div>
<div class="m2"><p>آشوب دل و دین بود این فتنه چه نامست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماهی بسرسر و کله گوشه شکسته</p></div>
<div class="m2"><p>یا ماه بخرگاهست یا سرو ببامست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دزدیده برو دید می از بیم رقیبان</p></div>
<div class="m2"><p>چون نیک بینی تو یکی ماه تمامست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او نیز نظر کرد بتندی بمن از خشم</p></div>
<div class="m2"><p>کاین رند نظرباز چه نامست و کدامست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم بدل از بیم باین عربده جو ترک</p></div>
<div class="m2"><p>نه قدرت گفتارم نه جای سلامست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مشرب او خون دل خلق حلال است</p></div>
<div class="m2"><p>در مذهب او پرسش عشاق حرامست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پیرهنش سینه نه یک صفحه زسیم است</p></div>
<div class="m2"><p>دل نیست در آن سینه که در سیم رخامست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشمش چو یکی ترک معربد بصف جنگ</p></div>
<div class="m2"><p>شمشیر بکف دارد و از اهل نظامست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیرین لب و شکردهن و عقل فریبست</p></div>
<div class="m2"><p>آهو روش و سرو قد و کبک خرامست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر حور بهشت است که او را زجواریست</p></div>
<div class="m2"><p>غلمان بخیلش چو یکی طرفه غلامست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوتاه بود در بر زلفش شب یلدا</p></div>
<div class="m2"><p>باطرف بناگوشش صد صبح چو شامست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آشفته تو را غالیه بو شد نفس از چیست</p></div>
<div class="m2"><p>زان زلف مگر بوئیت امشب بمشامست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خاک در میکده جو نعمت وصلش</p></div>
<div class="m2"><p>کاز باده فروشست که هر کار بکامست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر خضر بود زنده بیک جرعه آبست</p></div>
<div class="m2"><p>ور جم بود او بنده بیک گردش جامست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست ازل آن باده فروش خم معنی</p></div>
<div class="m2"><p>کو پیشرو اهل یقین است و امامست</p></div></div>