---
title: >-
    شمارهٔ ۱۱۲۵
---
# شمارهٔ ۱۱۲۵

<div class="b" id="bn1"><div class="m1"><p>اگر بمحفل مستان شبی کنند سماعی</p></div>
<div class="m2"><p>کنند اهل تصوف زوجد و حال وداعی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه نور بود که در طور عشق کرد تجلی</p></div>
<div class="m2"><p>که سوخت خرمن کون و مکان زبرق شعاعی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز متاع محبت بغیر جنس صداقت</p></div>
<div class="m2"><p>نبوده تاجر عشق تو را عقار و ضیاعی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ای عزیز برون آ زمصر نفس مجرد</p></div>
<div class="m2"><p>که یوسفت نکند متهم ببردن صاعی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر حریم دلت مولد است حب علی را</p></div>
<div class="m2"><p>چرا بکعبه دل جا دهی تو لات وسواعی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه صاحب گله را از شبان بود گله و بس</p></div>
<div class="m2"><p>بکن رعایت آشفته را لانک راعی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برون زحلقه اثنی عشر چگونه رود کس</p></div>
<div class="m2"><p>که نیست در سیر اهل حق ثلاث و رباعی</p></div></div>