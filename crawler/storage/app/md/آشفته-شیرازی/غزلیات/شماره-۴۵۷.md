---
title: >-
    شمارهٔ ۴۵۷
---
# شمارهٔ ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>خنک آنقوم که در کف می روشن دارند</p></div>
<div class="m2"><p>بزم از شعله می وادی ایمن دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بمیخانه چه رفتست که امشب مستان</p></div>
<div class="m2"><p>همه در مسجد و محراب نشیمن دارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر از سیمبران ایدل شیدا کاین قوم</p></div>
<div class="m2"><p>روی از آئینه دل از روی و آهن دارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من و کنج قفس و جور و جفای صیاد</p></div>
<div class="m2"><p>گرچه مرغان چمن جای بگلشن دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره بادیه ترکان بکمینند اما</p></div>
<div class="m2"><p>نتوان گفت چو چشمان تو رهزن دارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برفوی دل صد پاره ام اینان آیند</p></div>
<div class="m2"><p>رشته از زلف و زمژگان همه سوزن دارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در صف حشر که یعقوب گریزد زپسر</p></div>
<div class="m2"><p>ای خوش آنان که تو را دست بدامن دارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرچه آشفته سودای علی شد دو جهان</p></div>
<div class="m2"><p>تو نپندار که سودای تو چون من دارند</p></div></div>