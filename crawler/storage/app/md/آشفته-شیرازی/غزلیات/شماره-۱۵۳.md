---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>بهای عشق که داند که در جهان چند است</p></div>
<div class="m2"><p>همین بس است که بر عشق ماسوا بند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی بهشت برد شیخ روز و شب زحمت</p></div>
<div class="m2"><p>مگر بهشت بدیدار دوست مانند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه بگسلم از تو که تارهای وجود</p></div>
<div class="m2"><p>بتار زلف تواش بستگی و پیوند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آن بتی که بفرقان و آیه و برهان</p></div>
<div class="m2"><p>بروی و موی تو حق را عظیم سوگند است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجلوه رخ تو بنگرد بدیده دل</p></div>
<div class="m2"><p>بشاهد ازلی هر کس آرزومند است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>متاع این دل مسکین من که بشناسد</p></div>
<div class="m2"><p>زبسکه قافله دل بکویت افکند است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا صبور بمانم چسان بهوش زیم</p></div>
<div class="m2"><p>که سیل عشق تو بنیاد عقل برکند است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیثی از لب تو گفته مدعی در بزم</p></div>
<div class="m2"><p>بزخمهای درونم نمک پراکند است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشاد و بست جهان نیست جز به پنجه او</p></div>
<div class="m2"><p>چراکه دست علی پنجه خداوند است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غلام همت آنم که در طریق وفا</p></div>
<div class="m2"><p>هر آن جفا که ببیند زدوست خرسند است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حدیث زلف تو آشفته می‌نوشتی دوش</p></div>
<div class="m2"><p>که رشحه قلم تو عبیر آکند است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسیم لطف علی چون در اهتزاز آید</p></div>
<div class="m2"><p>چه غم که بار گناه تو کوه الوند است</p></div></div>