---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>اگر نه بخت من او دایم از چه خواب کند</p></div>
<div class="m2"><p>وگرنه جان من از چیست اضطراب کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدام می کشد آن چشم مست زآن لب لعل</p></div>
<div class="m2"><p>بلی چو ترک شود مست میل خواب کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر زپرسش روزحساب غافل شد</p></div>
<div class="m2"><p>نگاه او که بدل ظلم بیحساب کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال رحم و مروت بود که صیادی</p></div>
<div class="m2"><p>بقتل بسمل در خون طپان شتاب کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کرد گفتیم آن زلف با رخ جانان</p></div>
<div class="m2"><p>هر آنچه با رخ خورشید و مه سحاب کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهان تست اگر انگبین و کان شکر</p></div>
<div class="m2"><p>چرا بپاسخ تلخم همی جواب کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند بآینه ی رویت آه مشتاقان</p></div>
<div class="m2"><p>همان که ابر سیه رو بآفتاب کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سگان خیل خود آنگه که یار بشمارد</p></div>
<div class="m2"><p>مرا امید کز آنجمله انتخاب کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدام طوق بگردن فکنده آشفته</p></div>
<div class="m2"><p>که خویش را سگ درگاه بوتراب کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در جهان برخم بسته اند آشفته</p></div>
<div class="m2"><p>مگر که دست خداوند فتح باب کند</p></div></div>