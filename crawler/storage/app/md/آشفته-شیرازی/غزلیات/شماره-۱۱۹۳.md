---
title: >-
    شمارهٔ ۱۱۹۳
---
# شمارهٔ ۱۱۹۳

<div class="b" id="bn1"><div class="m1"><p>از وصل روی جانان امشب چو کامکاری</p></div>
<div class="m2"><p>شکوه ز بخت حیف است گر بر زبان برآری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شربت بود چو گیری از دست دوست حنظل</p></div>
<div class="m2"><p>عزت شمر چو بینی در عشق یار خواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل به موسم گل افغان و ناله‌ات چیست؟</p></div>
<div class="m2"><p>در روز وصل بیجاست غوغا و بیقراری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو خوانده‌ام گل زلف کج تو سنبل</p></div>
<div class="m2"><p>سنبل به گل نماید گر زآنکه مشکباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای ابر نوبهاری دریا در آستینی</p></div>
<div class="m2"><p>بر کشت تشنه‌کامان شاید که رحمت آری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز میکده که آنجا ما را امیدگاهست</p></div>
<div class="m2"><p>نشنیده‌ام ز خاکی بوی امیدواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دیگران به اعمال در حشر سربلندند</p></div>
<div class="m2"><p>من سر به زیر آنجا از بار شرمساری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقی به برتو هوشم ز آن عقل سوز باده</p></div>
<div class="m2"><p>تا برکشیم خطی بر رسم هوشیاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود مرا زر و زور تا ره برم به کویش</p></div>
<div class="m2"><p>بر خاکیان حیدر رو مینهم به زاری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از درگه علی رو، ای همرهان متابید</p></div>
<div class="m2"><p>کاشفته تاجور شد اینجا به خاکساری</p></div></div>