---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>یار با ما در انجمن باشد</p></div>
<div class="m2"><p>عیش خلوت نصیب من باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس سالک اگر بود سیاح</p></div>
<div class="m2"><p>گو سیاحت در انجمن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت روح الله مجرد فاش</p></div>
<div class="m2"><p>یار بر دوش روح تن باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرده و زنده هر دو در کسوت</p></div>
<div class="m2"><p>گرچه جامه است یا کفن باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن گلرا بوصف شد ممتاز</p></div>
<div class="m2"><p>صوت بلبل از آن حسن باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع را گو مریز اشک مدام</p></div>
<div class="m2"><p>کار پروانه سوختن باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمع خواهی تو بگذر از فانوس</p></div>
<div class="m2"><p>سد پروانه پیرهن باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای سلمی و خیمه لیلی</p></div>
<div class="m2"><p>گاه در ربع و گه دمن باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نکند آرزوی جنت و حور</p></div>
<div class="m2"><p>هر که را میکده وطن باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا که گل لاف زد به پیش رخت</p></div>
<div class="m2"><p>غنچه را خنده در دهن باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خود آشفته را مران ای گل</p></div>
<div class="m2"><p>گرچه او خار آن چمن باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر که جز وصف عشق حیدر گفت</p></div>
<div class="m2"><p>بایدش خاک در دهن باشد</p></div></div>