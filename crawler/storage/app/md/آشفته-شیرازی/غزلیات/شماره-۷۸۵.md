---
title: >-
    شمارهٔ ۷۸۵
---
# شمارهٔ ۷۸۵

<div class="b" id="bn1"><div class="m1"><p>با همه کثرت چه شد تا دم زتنهائی زدم</p></div>
<div class="m2"><p>یا باین زشتی چرا من لاف زیبائی زدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنده ام با کز لک توحید چون چشم دوبین</p></div>
<div class="m2"><p>با همه کثرت از آن نوبت زیکتائی زدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق بر یأجوج عقلم چون سکندر سد به بست</p></div>
<div class="m2"><p>پشت پا بر طارم این کاخ مینائی زدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنبه بودم رشته گشتم در کف نساج صنع</p></div>
<div class="m2"><p>حلقه ها بر حلق عجب و کبر و خورائی زدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رام گشته توسن عقلم چو رایض گشت عشق</p></div>
<div class="m2"><p>خوش بپای نفس پا بند شکیبائی زدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد فلاطون خم نشین و بحر حکمت شد دلش</p></div>
<div class="m2"><p>من ببحرمی فتادم لاف دانائی زدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوهر حب علی پروده ام تا در صدف</p></div>
<div class="m2"><p>طعنها بر لؤلؤ لالای دریائی زدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرتع رعنا غزالانست دشت خاطرم</p></div>
<div class="m2"><p>لاجرم از فخر گاهی دم زرعنائی زدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر کبوترهای بام کعبه دارم فخر از آنک</p></div>
<div class="m2"><p>چرخها بر آستان قرب مولائی زدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سینه ام آشفته آمد غیرت سینای طور</p></div>
<div class="m2"><p>تا که بر دل آتش از این عشق سودائی زدم</p></div></div>