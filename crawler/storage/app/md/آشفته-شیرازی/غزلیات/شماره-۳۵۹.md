---
title: >-
    شمارهٔ ۳۵۹
---
# شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>تنگست حرم سر زخرابات برآرید</p></div>
<div class="m2"><p>مستانه سرا زخرقه طامات برآرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا صومعه داران فلک را بنشانند</p></div>
<div class="m2"><p>سرمست هیاهوی مناجات برآرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر بر در میخانه بسائید سحرگاه</p></div>
<div class="m2"><p>تا روح صفت سر بسموات برآرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهاد زکین ریخته خون خم می را</p></div>
<div class="m2"><p>ای باده کشان دست مکافات برآرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دفتر آلوده بمی رنگ کن امشب</p></div>
<div class="m2"><p>گو سبحه و سجاده بهیهات ب رارید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از رخت وجودم بزدا رنگ تعلق</p></div>
<div class="m2"><p>هان کسوتم از آب خرابات برآرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته علی حاصل نفی آمد و اثبات</p></div>
<div class="m2"><p>عالم همه از نفی و زاثبات برآرید</p></div></div>