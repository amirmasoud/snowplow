---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>از سر کوی تو هرکو به سلامت می‌رفت</p></div>
<div class="m2"><p>خود به پیش و ز پیش خیل ملامت می‌رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خضر از میکده چون رفت پی آب حیات</p></div>
<div class="m2"><p>در دهان کرده سرانگشت ندامت می‌رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماند پا در گل و یک جوی سرشکش به کنار</p></div>
<div class="m2"><p>سرو چون دید که با آن قد و قامت می‌رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون مردم همه آن چشم سیه ریخت ولی</p></div>
<div class="m2"><p>بوسه آن لب شیرین به غرامت می‌رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک فروغ از خم می تافت به طور سینا</p></div>
<div class="m2"><p>موسی آنجا ز پی کسب کرامت می‌رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه آفاق بود پر ز نشان لیلی</p></div>
<div class="m2"><p>از چه مجنون پی آثار و علامت می‌رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اختر سعد می و بیت و شرف جام بلور</p></div>
<div class="m2"><p>زاهد از دیدنش از بیم شئامت می‌رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکوه زلف وی و شام فراق آشفته</p></div>
<div class="m2"><p>صحبتی بود که تا روز قیامت می‌رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ مشکل نگشاید ز مسک تا به سماک</p></div>
<div class="m2"><p>این سخن‌ها به سرانگشت امامت می‌رفت</p></div></div>