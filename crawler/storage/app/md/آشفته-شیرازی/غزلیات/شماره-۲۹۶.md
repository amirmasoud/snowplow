---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>مرا حدیث شکفتی است در کمال غرابت</p></div>
<div class="m2"><p>که از گدای طریقت بشه رسید مهابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بکوی عشق عجب میکنی زعجز سلاطین</p></div>
<div class="m2"><p>گدای او بشه از عجز کرده شد زغرابت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نثار خاک در دوست میکند دو جهانرا</p></div>
<div class="m2"><p>دعای عاشق صادق اگر کنند اجابت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیک مناظره عشق مانده است فلاطون</p></div>
<div class="m2"><p>مگو که رای حکیمان بود قرین اصابت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه غم که عاشق تو زنده پوش بیسرو پاشد</p></div>
<div class="m2"><p>فروغ عشق بس او را پی دلیل نجابت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود چه بار گران بار عشق روی تو یا رب</p></div>
<div class="m2"><p>که کوه تاب نیاورد با هزار صلابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زدیدنت برقیبان چسان حسد نبرم من</p></div>
<div class="m2"><p>که دل بدیده حسد میبرد زفرط رقابت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برغم غیر زد آشفته جامی از می وصلت</p></div>
<div class="m2"><p>دعای خسته‌دلان می‌رسد گهی به اجابت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علی است نایب اول ولیک صادر دوم</p></div>
<div class="m2"><p>به غیر عشق که دارد به جای عقل نیابت</p></div></div>