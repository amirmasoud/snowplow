---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>نقش یوسف تا بکی نقاش بر دیوارها</p></div>
<div class="m2"><p>یوسف ار داری بیاور بر سر بازارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغبانا گر تو بیرون میکنی ما را زباغ</p></div>
<div class="m2"><p>بوی گل شاید شنید از رخنه دیوارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من که دارم یکختن نافه ززلفت در ضمیر</p></div>
<div class="m2"><p>منت بیجا نخواهم بردن از عطارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی بگوش از سینه و دل آیدت بانگ سرود</p></div>
<div class="m2"><p>نشنوی جز ناله اندر منزل بیمارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرفها آموخت رندی دوشم از اسرار عشق</p></div>
<div class="m2"><p>بود در هر حرف او پنهان بسی اسرار ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شایدش گر فاش گوید سر حق در انجمن</p></div>
<div class="m2"><p>هر که چون حلاج خوش کرده است جابردارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه بر ذرات عالم عرضه شد روز ازل</p></div>
<div class="m2"><p>من گزیدم عشق خوبان را زدیگر کارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بار دلها داشت زلفش دل بزیر بار زلف</p></div>
<div class="m2"><p>وه که بر بار دلم افزوده شد سربارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه بدیرم هست راه و نه حرم ای همرهان</p></div>
<div class="m2"><p>عشق بازان فارغند از سبحه و زنارها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بگیرد شحنه ام یا شیخ تکفیرم کند</p></div>
<div class="m2"><p>کسوت مستان تو عاری بود از عارها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشکل آشفته نگشاید زشیخ خانقاه</p></div>
<div class="m2"><p>عقده کس حل نشد جز بر در خمارها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نسوزم زآتش عصیان ببالا دوختم</p></div>
<div class="m2"><p>کسوتی کز حب حیدر داشت پود و تارها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راه ایمان میزند آن چشمکان کافرت</p></div>
<div class="m2"><p>بر که شاید داوری بردن از این عیارها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر در میرمؤید آنکه چون نوشیروان</p></div>
<div class="m2"><p>هست از دیوان عدلش در جهان آثارها</p></div></div>