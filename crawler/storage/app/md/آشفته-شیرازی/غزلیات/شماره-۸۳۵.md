---
title: >-
    شمارهٔ ۸۳۵
---
# شمارهٔ ۸۳۵

<div class="b" id="bn1"><div class="m1"><p>من به بیداری شب‌های غمت معروفم</p></div>
<div class="m2"><p>به صِفات سگ کویت صنما موصوفم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست محروم‌تر از وصل تو کس چون من زار</p></div>
<div class="m2"><p>گرچه در عشق تو اندر همه‌جا معروفم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به امیدی که دم قتل ببینم رویت</p></div>
<div class="m2"><p>جان به کف منتظر از کشتن خود مشعوفم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که عکس تو به بتخانه آذر افتاد</p></div>
<div class="m2"><p>از حرم شوق تو کرده است عنان معطوفم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم آشفته برو از خم زلفش بیرون</p></div>
<div class="m2"><p>گفت حاشا که گذارم وطن مألوفم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که وقف است زبانم به مدیح حیدر</p></div>
<div class="m2"><p>حاش لله که کس این کار کند موقوفم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار در پرده و ساقی بده آن جام صفا</p></div>
<div class="m2"><p>شاید از لطف تو این پرده شود مکشوفم</p></div></div>