---
title: >-
    شمارهٔ ۴۵۱
---
# شمارهٔ ۴۵۱

<div class="b" id="bn1"><div class="m1"><p>طایف کعبه گر شبی بر حرم تو بگذرد</p></div>
<div class="m2"><p>فسخ کند عزیمت و سر بدر تو بسپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وجد و سماع صوفیان نیست عجب زچنگ و نی</p></div>
<div class="m2"><p>عشق چو نغمه ساز شد کوه برقص آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی مه عارضت بتا عاشق دردمند تو</p></div>
<div class="m2"><p>کوکب چند ریزد و چند ستاره بشمرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بمرور میبرد نقش حجر مطر ولی</p></div>
<div class="m2"><p>نقش تو را زلوح دل ریزش اشک نسترد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه که زعمر بر خورد کشته تیغ نیکوان</p></div>
<div class="m2"><p>خضر بود بلی چو کس زآب حیات برخورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایکه عجب همی کنی سیر نبی بر آسمان</p></div>
<div class="m2"><p>طایر آه ما ببین کز سر عرش بگذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شیشه پر زمهر دل بود بمهر تو ولی</p></div>
<div class="m2"><p>حیف که اشک پرده در پرده خلق میدرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بنهند سلسله بر سرو پای من جهان</p></div>
<div class="m2"><p>موی توام بجنبشی جانب خویش میبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته بند بند من گر ببرند همچو نی</p></div>
<div class="m2"><p>دل زشکنج زلف او رشته مهر کی برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذردش زعرش سر طوف کند برو ملک</p></div>
<div class="m2"><p>بر در حیدر از وفا پای کس ار نبفشرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که کشد بمیکده باده زدست مهوشان</p></div>
<div class="m2"><p>نام بهشت و حور او کی بزبان بیاورد</p></div></div>