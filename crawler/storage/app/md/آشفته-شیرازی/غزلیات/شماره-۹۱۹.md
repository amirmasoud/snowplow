---
title: >-
    شمارهٔ ۹۱۹
---
# شمارهٔ ۹۱۹

<div class="b" id="bn1"><div class="m1"><p>دور از او چشم بد بزم وصالست این</p></div>
<div class="m2"><p>بخت برآمد زخواب یا که خیالست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم در رقیبان نخفت دیده انجم بدوخت</p></div>
<div class="m2"><p>ورنه من و وصل دوست طرفه محالست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش بتان در نظر کعبه ات اندر قفا</p></div>
<div class="m2"><p>راه حقیقت کجاست عین ضلالست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون رزانت حرام خون جهانت حلال</p></div>
<div class="m2"><p>خود چه حرامست آن یا چه حلالست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادی دوران غمست عشرت او ماتمست</p></div>
<div class="m2"><p>غم بطرب مدغم است عین ملالست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکدمه دیدار دوست مغتنم است ای پسر</p></div>
<div class="m2"><p>نقش رقیبست نقض محض کمالست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غره شهر رجب موسم عیش و طرب</p></div>
<div class="m2"><p>باده ننوشی عجب نیک بفالست این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تو جهان شد جمیل پرده که برداشتی</p></div>
<div class="m2"><p>پرتو سینا و طور یا که جمالست این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدح علی را بگو دفتر فکرت بشو</p></div>
<div class="m2"><p>خیز که آشفته را موسم حالست این</p></div></div>