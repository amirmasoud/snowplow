---
title: >-
    غزل شمارهٔ ۴۹۱
---
# غزل شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>دوشینه خود شنیدم یک نکته از دهانی</p></div>
<div class="m2"><p>اما نمی‌توان گفت با هیچ نکته‌دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسرار عشقم آخر افتاد بر زبانها</p></div>
<div class="m2"><p>از بس که وصف او را گفتم به هر زبانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شامگه به یادش خفتم به لاله‌زاری</p></div>
<div class="m2"><p>هر صبح دم به بویش رفتم به بوستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخم وفای او را کشتم به هر زمینی</p></div>
<div class="m2"><p>خار جفای او را خوردم به هر زمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گردنم فکنده‌ست گیسوی او کمندی</p></div>
<div class="m2"><p>بر کشتنم کشیده‌ست ابروی او کمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیکان عشق جانان تا پر نشسته برجان</p></div>
<div class="m2"><p>هرگز چنین خدنگی ننشسته بر نشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عالم جوانی کاری نیامد از من</p></div>
<div class="m2"><p>دستی زدم به پیری در دامن جوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در وادی محبت حال دلم چه پرسی</p></div>
<div class="m2"><p>کردی فتاده دیدم دنبال کاروانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای آن که زیر تیغش امید رحم داری</p></div>
<div class="m2"><p>ترسم نکرده باشی رحمی به خسته جانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر بسته سحر چشمش دست قوی دلان را</p></div>
<div class="m2"><p>زور این چنین که دیده‌ست آنگه ز ناتوانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر با پری نداری نسبت چرا همیشه</p></div>
<div class="m2"><p>در خاطرم مقیمی وز دیده‌ام نهانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صفهای دلبران را بر یکدگر شکستی</p></div>
<div class="m2"><p>گویا کمین غلامی از خسرو جهانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاه سریر تمکین بخشنده ناصرالدین</p></div>
<div class="m2"><p>کز دست او نمانده‌ست گوهر به هیچ کانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یزدان به من فروغی هر لحظه صد لسان داد</p></div>
<div class="m2"><p>تا مدح سایه‌اش را گویم به هر لسانی</p></div></div>