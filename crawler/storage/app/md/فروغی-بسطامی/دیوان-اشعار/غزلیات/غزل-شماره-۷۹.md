---
title: >-
    غزل شمارهٔ ۷۹
---
# غزل شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>پیام باد بهار از وصال جانان است</p></div>
<div class="m2"><p>بیار باده که هنگام مستی جان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم به کوچهٔ دیوانگی بزن چندی</p></div>
<div class="m2"><p>که عقل بر سر بازار عشق حیران است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وجود آدمی از عشق می‌رسد به کمال</p></div>
<div class="m2"><p>گر این کمال نیابی، کمال نقصان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بقای عاشق صادق ز لعل معشوق است</p></div>
<div class="m2"><p>حیات خضر پیمبر ز آب حیوان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به راستی همه کس قدر وصل کی داند</p></div>
<div class="m2"><p>مگر کسی که به محنت‌سرای هجران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پسند خاطر مشکل پسند جانان نیست</p></div>
<div class="m2"><p>وگر نه جان گران‌مایه دادن آسان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب مدار که در عین درد خاموشم</p></div>
<div class="m2"><p>که در دیار پری‌چهره محص درمان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چراغ چشم من آن روی مجلس افروز است</p></div>
<div class="m2"><p>طناب عمر من آن موی عنبر افشان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یاد کاکل پرتاب و زلف پر چینش</p></div>
<div class="m2"><p>دل من است که هم جمع و هم پریشان است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهی که راز من از پرده آشکارا کرد</p></div>
<div class="m2"><p>هنوز صورت او زیر پرده پنهان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مه صفر ز برای همین مظفر شد</p></div>
<div class="m2"><p>که ماه عید همایون شاه ایران است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابوالمظفر منصور ناصرالدین شاه</p></div>
<div class="m2"><p>که زیر رایت او آفتاب تابان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طلوع صبح جمالش فروغ آفاق است</p></div>
<div class="m2"><p>بساط مجلس عیدش نشاط دوران است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فروغی از غزل عید شاه شادی کن</p></div>
<div class="m2"><p>که شادکامی شاعر ز عید سلطان است</p></div></div>