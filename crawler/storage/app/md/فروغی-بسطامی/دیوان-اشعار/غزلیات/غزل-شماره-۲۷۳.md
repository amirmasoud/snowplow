---
title: >-
    غزل شمارهٔ ۲۷۳
---
# غزل شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>بستهٔ زلف تو شوریده‌سرانند هنوز</p></div>
<div class="m2"><p>تشنهٔ لعل تو خونین‌جگرانند هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا در قدح باده چه پیمودی دوش</p></div>
<div class="m2"><p>که حریفان همه در خواب گرانند هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال عشاق تو گل‌های گلستان دانند</p></div>
<div class="m2"><p>که به سودای رُخَت جامه‌درانند هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از غم سینهٔ سیمین تو ای سیمین‌ساق</p></div>
<div class="m2"><p>سنگ بر سینه‌زنان سیم‌برانند هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه همین مات جمال تو منم کز هرسو</p></div>
<div class="m2"><p>واله حسن تو صاحب‌نظرانند هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاش برگردی از این راه که ارباب امید</p></div>
<div class="m2"><p>در گذرگاه تو حسرت نگرانند هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کس را نرسد دعوی آزادی کرد</p></div>
<div class="m2"><p>که همه بندهٔ زرین‌کمرانند هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همت ما ز سر هردو جهان تند گذشت</p></div>
<div class="m2"><p>دیگران قید جهان گذرانند هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کامی از ماهوَشان هیچ فروغی مطلب</p></div>
<div class="m2"><p>کز سر مهر به کام دگرانند هنوز</p></div></div>