---
title: >-
    غزل شمارهٔ ۴۳۱
---
# غزل شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>هر کس که نهد پای بر آن خاک سر کو</p></div>
<div class="m2"><p>ذکرش همه این است که گم گشته دلم کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از اثر عشق، سیه بخت و سیه روز</p></div>
<div class="m2"><p>او از مدد حسن، سیه چشم و سیه مو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیباچهٔ امید من آن صفحهٔ رخسار</p></div>
<div class="m2"><p>سرمایهٔ سودای من آن حلقهٔ گیسو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمعی همه آشفتهٔ آن سنبل مشکین</p></div>
<div class="m2"><p>شهری همه شوریدهٔ آن نرگس جادو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم لاله نرسته‌ست بدین آب و بدین تاب</p></div>
<div class="m2"><p>هم گل به شکفته ست بدین رنگ و بدین بو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من تشنه لب ساقی و او طالب کوثر</p></div>
<div class="m2"><p>حاشا که رود آب من و شیخ به یک جو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخاست ز هر گوشه بلایی به کمینم</p></div>
<div class="m2"><p>تا دیده‌ام افتاد بدان گوشهٔ ابرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آهوی من آن کار که با شیردلان کرد</p></div>
<div class="m2"><p>هرگز نکند شیر قوی پنجه به آهو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسرت برم از خسرو و فرهاد که در عشق</p></div>
<div class="m2"><p>نه زر به ترازویم و نه زور به بازو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیبا صنما پرده ز رخسار برانداز</p></div>
<div class="m2"><p>تا بر طرف قبله فروغی نکند رو</p></div></div>