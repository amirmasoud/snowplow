---
title: >-
    غزل شمارهٔ ۴۱۱
---
# غزل شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>نرگس بیمار تو گشته پرستار من</p></div>
<div class="m2"><p>تا چه کند این طبیب با دل بیمار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خفتهٔ بیدار گیر گر چه ندیدی ببین</p></div>
<div class="m2"><p>چشم پر از خواب خویش دیدهٔ بیدار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم تو عاشق کشی شیوهٔ من عاشقی</p></div>
<div class="m2"><p>تیغ زدن شغل تو، کشته شدن کار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با همه تیر بلا کامده بر دل مرا</p></div>
<div class="m2"><p>از مژه‌ات بر نگشت بخت نگون سار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب رخ گل به ریخت لالهٔ رخسار تو</p></div>
<div class="m2"><p>خرمن بلبل بسوخت زمزمهٔ زار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله برآمد ز کوه از اثر زاریم</p></div>
<div class="m2"><p>تا تو کمر بسته‌ای از پی آزار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتم و از دل نرفت حسرت خاک درت</p></div>
<div class="m2"><p>مردم و آسان نساخت عشق تو دشوار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا خم زلف تو را دام دلم کرده‌اند</p></div>
<div class="m2"><p>میل خلاصی نکرد مرغ گرفتار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بت و زنار من چهره و گیسوی توست</p></div>
<div class="m2"><p>قبله حسد می‌برد از بت و زنار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه لبم بوسه زد گندم خال تو را</p></div>
<div class="m2"><p>یک جو کمتر نشد خواهش بسیار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر دو جهان می‌شود از کرم می‌فروش</p></div>
<div class="m2"><p>مست نخواهد شدن خاطر هشیار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا سخنی گفته‌ام زان لب شیرین سخن</p></div>
<div class="m2"><p>خسرو ایران نمود گوش به گفتار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناصردین شاه راد، بارگه عدل و داد</p></div>
<div class="m2"><p>کز گهرش برده اب نظم گهر بار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا که فروغی شنید شعر مرا شهریار</p></div>
<div class="m2"><p>شهره هر شهر شد دفتر اشعار من</p></div></div>