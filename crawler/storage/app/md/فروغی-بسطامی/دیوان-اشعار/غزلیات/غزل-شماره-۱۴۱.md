---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>عهد همه بشکستم در بستن پیمانت</p></div>
<div class="m2"><p>دامن مکش از دستم، دست من و دامانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسرت خورم از خونی کش ریخته شمشیرت</p></div>
<div class="m2"><p>غیرت برم از چاکی کش دوخته پیکانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس جبهه که بر خاک است از طلعت فیروزت</p></div>
<div class="m2"><p>بس جامه که صد چاک است از چاک گریبانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس خانه که ویران است از لشگر بیدادت</p></div>
<div class="m2"><p>بس دیده که گریان است از غنچهٔ خندانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم‌خون خریداران پیرایهٔ بازارت</p></div>
<div class="m2"><p>هم جای طلب کاران پیرامن دکانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کشتن مظلومان عاجز شده بازویت</p></div>
<div class="m2"><p>وز کثرت مشتاقان تنگ آمده میدانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید نظربازان از چشم سیه مستت</p></div>
<div class="m2"><p>تشویق سحرخیزان از جنبش مژگانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیباچهٔ زیبایی رخسار دل‌آرایت</p></div>
<div class="m2"><p>مجموعهٔ دلبندی گیسوی پریشانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خون همه در مستی خوردی به زبر دستی</p></div>
<div class="m2"><p>دست همه بربستی گرد سر دستانت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن روز قیامت را بر پای کند ایزد</p></div>
<div class="m2"><p>کایی پی دل جویی بر خاک شهیدانت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>الهام توان گفتن اشعار فروغی را</p></div>
<div class="m2"><p>تا چشم وی افتاده‌ست بر لعل سخن دانت</p></div></div>