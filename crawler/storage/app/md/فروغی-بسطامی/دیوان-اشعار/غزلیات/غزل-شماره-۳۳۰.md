---
title: >-
    غزل شمارهٔ ۳۳۰
---
# غزل شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>امشب تو را به خوبی نسبت به ماه کردم</p></div>
<div class="m2"><p>تو خوب تر ز ماهی، من اشتباه کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشینه پیش رویت آیینه را نهادم</p></div>
<div class="m2"><p>روز سفید خود را آخر سیاه کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر صبح یاد رویت تا شام گه نمودم</p></div>
<div class="m2"><p>هر شام فکر مویت تا صبح گاه کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو آن چه دوش کردی از نوک غمزه کردی</p></div>
<div class="m2"><p>من هر چه کردم امشب از تیر آه کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد گوشمال دیدم تا یک سخن شنیدم</p></div>
<div class="m2"><p>صد ره به خون تپیدم تا یک نگاه کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خواجه روز محشر جرم مرا ببخشد</p></div>
<div class="m2"><p>گر وعده عطایش عمری گناه کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من هر غزل که گفتم در عاشقی فروغی</p></div>
<div class="m2"><p>یک جاگریز آن را بر نام شاه کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه همه سلاطین، شایسته ناصرالدین</p></div>
<div class="m2"><p>کز قهر دشمنش را در قعر چاه کردم</p></div></div>