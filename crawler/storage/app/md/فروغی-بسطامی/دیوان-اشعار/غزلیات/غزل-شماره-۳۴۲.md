---
title: >-
    غزل شمارهٔ ۳۴۲
---
# غزل شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>بر سر هر مژه چندین گل رنگین دارم</p></div>
<div class="m2"><p>یعنی از عشق تو در بر دل خونین دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو در سینهٔ سیمین دل سنگین داری</p></div>
<div class="m2"><p>من هم از دولت عشقت تن رویین دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سرم گر ز فلک سنگ ببارد غم نیست</p></div>
<div class="m2"><p>زان که از خشت سر کوی تو بالین دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به امیدی که سحر بر رخت افتد نظرم</p></div>
<div class="m2"><p>نظری شب همه شب بر مه و پروین دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چه کامم ز لب نوش تو تلخ است اما</p></div>
<div class="m2"><p>گر کسی گوش دهد، قصهٔ شیرین دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کامی از دیر و حرم هیچ ندیدم در عشق</p></div>
<div class="m2"><p>گله‌ای چند هم از کفر و هم از دین دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز تاریک و شب تیره و اقبال سیاه</p></div>
<div class="m2"><p>همه زان خال و خط و طرهٔ مشکین دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق هر روز ز نو داد مرا آیینی</p></div>
<div class="m2"><p>تا بدانند خلایق که چه آیین دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتمش مهر فروغی به تو روز افزون است</p></div>
<div class="m2"><p>گفت من هم به خلافش دل پر کین دارم</p></div></div>