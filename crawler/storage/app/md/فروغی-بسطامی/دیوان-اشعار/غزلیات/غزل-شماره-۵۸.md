---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>تا طرف نقاب از رخ رخشان تو برخاست</p></div>
<div class="m2"><p>خورشید فلک از پی فرمان تو برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تنگ دهان را به شکر خنده گشودی</p></div>
<div class="m2"><p>طوطی به هوای شکرستان تو برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر افسر شاهان سرافراز نشیند</p></div>
<div class="m2"><p>هر گرد که از گوشهٔ دامان تو برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داغی است که در سینهٔ صد چاک نهفتند</p></div>
<div class="m2"><p>هر لاله که از خاک شهیدان تو برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کار فروبسته عشاق فکندند</p></div>
<div class="m2"><p>هر عقدهٔ که از زلف پریشان تو برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد ولوله در مردم صاحب نظر انداخت</p></div>
<div class="m2"><p>هر فتنه که از نرگس فتان تو برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک فشاند آب رخ مشک ختن را</p></div>
<div class="m2"><p>هر نافه که از طرهٔ پیچان تو برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در انجمن باده کشانش ننشانند</p></div>
<div class="m2"><p>پیمانه‌کشی کز سر پیمان تو برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا سرزده خورشید جهان‌تاب ز مشرق</p></div>
<div class="m2"><p>خورشید فروغی ز گریبان تو برخاست</p></div></div>