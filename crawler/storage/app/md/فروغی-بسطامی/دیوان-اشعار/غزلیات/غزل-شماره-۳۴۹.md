---
title: >-
    غزل شمارهٔ ۳۴۹
---
# غزل شمارهٔ ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>هر کجا دم زدم از چشم بت کشمیرم</p></div>
<div class="m2"><p>خون مردم همه گردید گریبان گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج ها جسته‌ام از فیض خرابی ای کاش</p></div>
<div class="m2"><p>آن که کرده‌ست خرابم، بکند تعمیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر آبم نزنی آتش خرمن سوزم</p></div>
<div class="m2"><p>ور خموشم نکنی شعلهٔ عالم‌گیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر کوی جنون نعره‌زنان می‌آیم</p></div>
<div class="m2"><p>کو سر زلف تو آماده کند زنجیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت برگشتهٔ من بین که به میدان امید</p></div>
<div class="m2"><p>خم ابروی تو ننواخت به یک شمشیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرم خواهم دل سنگین تو را تا چه کند</p></div>
<div class="m2"><p>گریهٔ با اثر و نالهٔ بی‌تاثیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به عشق تو کنم دعوی دل سوختگی</p></div>
<div class="m2"><p>می‌توان سوز مرا یافتن از تقریرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون مرا می‌کشی از چره برانداز نقاب</p></div>
<div class="m2"><p>تا خلایق همه دانند که بی‌تقصیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش با زلف بلند تو فروغی می‌گفت</p></div>
<div class="m2"><p>دگری را به کمند آر که من نخجیرم</p></div></div>