---
title: >-
    غزل شمارهٔ ۳۵۶
---
# غزل شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>ای خط تو را دایرهٔ حسن مسلم</p></div>
<div class="m2"><p>وی نور رخت برده دل از نیر اعظم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم خیره ز انوار رخت موسی عمران</p></div>
<div class="m2"><p>هم زنده به انفاس خوشت عیسی مریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم منظر زیبای تو مهری است منور</p></div>
<div class="m2"><p>هم پیکر مطبوع تو روحی است مجسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم فتنهٔ مردم شدی از نرگس پر فن</p></div>
<div class="m2"><p>هم عقده به دلها زدی از سنبل پر خم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم کاستهٔ درد تو فارغ نه مداوا</p></div>
<div class="m2"><p>هم سوختهٔ داغ تو آسوده ز مرهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افراختی از قامت خود رایت خوبی</p></div>
<div class="m2"><p>آویختی از طرهٔ خود ... پرچم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی رایحهٔ سنبل مشکین تو هرگز</p></div>
<div class="m2"><p>خوش بو نشود مجمع ارواح مکرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس که ز کیفیت چشم تو خبر شد</p></div>
<div class="m2"><p>از خود خبرش نیست نه از کیف و نه از کم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو قبلهٔ عشاق رخت کعبهٔ مقصود</p></div>
<div class="m2"><p>وان خال و زنخدان حجرالاسود و زمزم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر طاق دو ابروی تو منظور نبودی</p></div>
<div class="m2"><p>مسجود ملایک نشدی قالب آدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زان کرده دلش را به تو تسلیم فروغی</p></div>
<div class="m2"><p>زیرا که به خوبی تویی امروز مسلم</p></div></div>