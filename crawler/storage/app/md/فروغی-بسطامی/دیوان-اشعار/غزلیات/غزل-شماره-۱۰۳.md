---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>کیفیتی که دیدم از آن چشم نیم مست</p></div>
<div class="m2"><p>با صدهزار جام نیارد کسی به دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جسم ناتوان ز سر راه او نخاست</p></div>
<div class="m2"><p>یک صید نیم‌جان ز کمین‌گاه او نجست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو آن دلی که نرگس فتان او نبرد</p></div>
<div class="m2"><p>کو سینه‌ای که خنجر مژگان او نخست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز یاد او امید بریدم ز هر چه بود</p></div>
<div class="m2"><p>جز روی او کناره گرفتم ز هر که هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از من دویی مجوی که یک بینم از ازل</p></div>
<div class="m2"><p>وز من ادب مخواه که سرمستم از الست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منت خدای را که ز هر سو به روی من</p></div>
<div class="m2"><p>در باز شد ز همت رندان می‌پرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با من مگو که بهر چه دیوانه گشته‌ای</p></div>
<div class="m2"><p>با آن پری بگوی که زنجیر من گسست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پهلو زند به شه‌پر جبرییل ناوکی</p></div>
<div class="m2"><p>کز شست او رها شد و بر جان من نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زلف گره‌گشای تو پیوند من برید</p></div>
<div class="m2"><p>چشم درست‌کار تو پیمان من شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از جعد سر بلند تو یک قوم دستگیر</p></div>
<div class="m2"><p>وز عنبری کمند تو یک جمع پای بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرو بلند من ننهد پا فروغیا</p></div>
<div class="m2"><p>بر فرق آن کسی که نگردد چو خاک پست</p></div></div>