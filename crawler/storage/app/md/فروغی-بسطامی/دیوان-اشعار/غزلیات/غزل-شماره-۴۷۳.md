---
title: >-
    غزل شمارهٔ ۴۷۳
---
# غزل شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>تو شکر لب که با خسرو بسی شیرین سخن داری</p></div>
<div class="m2"><p>کجا آگاهی از شوریده حال کوه کن داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا از انجمن در گوشهٔ خلوت نشانیدی</p></div>
<div class="m2"><p>ولی با مدعی خوش خلوتی در انجمن داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آن شهرم که سیلاب محبت ساخت ویرانم</p></div>
<div class="m2"><p>تو آن گنجی که در ویرانهٔ دلها وطن داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخواهی بر سر خاک من آمد روز محشر هم</p></div>
<div class="m2"><p>که از هر سو هزاران کشتهٔ خونین کفن داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفتار کمندت تازه گردیدم به امیدی</p></div>
<div class="m2"><p>که لطف بی نهایت با اسیران کهن داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر از پرده رازم آشکارا شد چه غم دارم</p></div>
<div class="m2"><p>که پنهان از همه عالم نگاهی سوی من داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم از موی تو پا بستم هم از بوی تو سر مستم</p></div>
<div class="m2"><p>که سنبل در سمن داری و گل در پیرهن داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو هم یوسف کنی در چاه و هم از چه کشی بیرون</p></div>
<div class="m2"><p>که هم چاه ذقن داری و هم مشکین رسن داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمان داری ندیدم در کمین گاه نظر چون تو</p></div>
<div class="m2"><p>که دلها را نشان غمزهٔ ناوک فکن داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سزد گر قدر قیمت بشکنی عنبر فروشان را</p></div>
<div class="m2"><p>که خط عنبرین و طره عنبر شکن داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نجات از تلخ کامی می‌توان دادن فروغی را</p></div>
<div class="m2"><p>که هم شکر فشان یاقوت و هم شیرین دهن داری</p></div></div>