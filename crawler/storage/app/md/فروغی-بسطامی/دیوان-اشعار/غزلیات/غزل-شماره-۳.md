---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>به جان تا شوق جانان است ما را</p></div>
<div class="m2"><p>چه آتش‌ها که بر جان است ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلای سختی و برگشته بختی</p></div>
<div class="m2"><p>از آن برگشته مژگان است ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن آلوده دامانیم در عشق</p></div>
<div class="m2"><p>که خون دل به دامان است ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حدیث زلف جانان در میان است</p></div>
<div class="m2"><p>سخن زان رو پریشان است ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان از درد خوبان زار گشتیم</p></div>
<div class="m2"><p>که بیزاری ز درمان است ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ما ای ناصح فرزانه بگذر</p></div>
<div class="m2"><p>که با پیمانه پیمان است ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس خو با خیال او گرفتیم</p></div>
<div class="m2"><p>وصال و هجر یکسان است ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر کوی نگاری جان سپردیم</p></div>
<div class="m2"><p>که خاکش آب حیوان است ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبی بی روی آن مه روز کردن</p></div>
<div class="m2"><p>برون از حد امکان است ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گریبان تو تا از دست دادیم</p></div>
<div class="m2"><p>اجل دست و گریبان است ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به غیر از مشکل عشقش فروغی</p></div>
<div class="m2"><p>چه مشکل‌ها که آسان است ما را</p></div></div>