---
title: >-
    غزل شمارهٔ ۱۱۵
---
# غزل شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>تو و آن حسن دل آویز که تغییرش نیست</p></div>
<div class="m2"><p>من و این عشق جنون خیز که تدبیرش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو و آن زلف سراسیمه که سامانش نه</p></div>
<div class="m2"><p>من و این خواب پراکنده که تعبیرش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردی اندر دل ما هست که درمانش نه</p></div>
<div class="m2"><p>آهی اندر لب ما هست که تاثیرش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زرهی نیست که در خط زره سازش نه</p></div>
<div class="m2"><p>گرهی نیست که در زلف گره گیرش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لشکری نیست که در سایهٔ مژگانش نه</p></div>
<div class="m2"><p>کشوری نیست که در قبضهٔ شمشیرش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کو سواری که در این عرصه گرفتارش نه</p></div>
<div class="m2"><p>کو شکاری که در این بادیه نخجیرش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ سر نیست که سودایی گیسویش نه</p></div>
<div class="m2"><p>هیچ دل نیست که دیوانهٔ زنجیرش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا درآید ز کمین ترک کمان ابروی من</p></div>
<div class="m2"><p>سینه‌ای نیست که آماجگه تیرش نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خم ابروی کسی خون مرا ریخت به خاک</p></div>
<div class="m2"><p>که سر تاجوران قابل شمشیرش نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچنان کعبهٔ دل را صنمی ویران ساخت</p></div>
<div class="m2"><p>که کس از بهر خدا در پی تعمیرش نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیخ گر شد به ره زهد چنین پندارد</p></div>
<div class="m2"><p>که کسی با خبر از حیله و تزویرش نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کامی از آهوی مقصود فروغی نبرد</p></div>
<div class="m2"><p>هر که در دشت محبت جگر شیرش نیست</p></div></div>