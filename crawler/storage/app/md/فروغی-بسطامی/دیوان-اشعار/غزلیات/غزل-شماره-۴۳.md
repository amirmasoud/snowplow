---
title: >-
    غزل شمارهٔ ۴۳
---
# غزل شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>تا به مستی نرسد بر لب ساقی لب ما</p></div>
<div class="m2"><p>بر نیاید ز خرابات مغان مطلب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق پیری است که ساغر زده‌ایم از کف او</p></div>
<div class="m2"><p>عقل طفلی است که دانا شده در مکتب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توبه از شرب دمادم نتوانیم نمود</p></div>
<div class="m2"><p>که جز این شیوهٔ شیرین نبود مشرب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملتی نیست به جز کفر محبت ما را</p></div>
<div class="m2"><p>هیچ کیشی نتوان جست به از مطلب ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا رب ما اثری در تو ندارد ورنه</p></div>
<div class="m2"><p>لرزه بر عرش فتاد از اثر یا رب ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس مبادا به سیه‌روزی ما در ره عشق</p></div>
<div class="m2"><p>که فلک تیره شد از تیرگی کوکب ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی سحر داد به ما وعدهٔ دیدار ولی</p></div>
<div class="m2"><p>ترسم از بخت سیه، روز نگردد شب ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نزد عشق به سر خط سعادت ما را</p></div>
<div class="m2"><p>خدمت حضرت معشوق نشد منصب ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر ره وادی مقصود فروغی این است</p></div>
<div class="m2"><p>لنگ خواهد شدن اینجا قدم مرکب ما</p></div></div>