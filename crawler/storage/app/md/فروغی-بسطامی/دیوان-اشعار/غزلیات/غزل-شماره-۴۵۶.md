---
title: >-
    غزل شمارهٔ ۴۵۶
---
# غزل شمارهٔ ۴۵۶

<div class="b" id="bn1"><div class="m1"><p>مسجد مقام عجب است، می‌خانه جای مستی</p></div>
<div class="m2"><p>زین هر دو خانه بگذر گر مرد حق‌پرستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی با تو می‌توان گفت اسرار نیستی را</p></div>
<div class="m2"><p>تا مو به مو اسیری در شهربند هستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بوی زلف او را از باد می‌شنیدی</p></div>
<div class="m2"><p>شب تا سحر ز شادی یک جا نمی‌نشستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن به هر بلایی آنجا که مبتلایی</p></div>
<div class="m2"><p>سر کن به هر جفایی آنجا که پای بستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستی که دادی آخر از دست من کشیدی</p></div>
<div class="m2"><p>عهدی که بستی آخر در انجمن شکستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر علم دوستی را تعلیم می‌گرفتی</p></div>
<div class="m2"><p>پیوند دوستان را هرگز نمی‌گسستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درمان نمی‌پسندد هر دل که درد دادی</p></div>
<div class="m2"><p>مرهم نمی‌پذیرد هر سینه‌ای که خستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آستان یارم برد آسمان غبارم</p></div>
<div class="m2"><p>بالا گرفت کارم در منتهای پستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدی دلا که آخر با صدهزار کوشش</p></div>
<div class="m2"><p>از قید او نرستی وز بند او نجستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر دست من بگیرد پیر مغان عجب نیست</p></div>
<div class="m2"><p>زیرا که من ندادم دستی به هیچ دستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هشیاریت فروغی معلوم نیست گویا</p></div>
<div class="m2"><p>مدهوش چشم ساقی مست می الستی</p></div></div>