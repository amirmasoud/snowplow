---
title: >-
    غزل شمارهٔ ۲۳۴
---
# غزل شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>صورتگران که صورت دلخواه می‌کشند</p></div>
<div class="m2"><p>چون صورت تو می‌نگرند آه می‌کشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمعی شریک حال پراکندهٔ من‌اند</p></div>
<div class="m2"><p>کز طرهٔ تو دست به اکراه می‌کشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب تشنگان چاه زنخدان دلکشت</p></div>
<div class="m2"><p>آب حیات دم به دم از چاه می‌کشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب چه گلبنی تو که با نقش قامتت</p></div>
<div class="m2"><p>سرو بلند را همه کوتاه می‌کشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من مات صورت تو که در کارگاه حسن</p></div>
<div class="m2"><p>خورشید را گدا و تو را شاه می‌کشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو در حجاب رفته به چندین هزار ناز</p></div>
<div class="m2"><p>من منتظر که دامن خرگاه می‌کشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌گیرد آفتاب ز دود درون ما</p></div>
<div class="m2"><p>چون عنبرین نقاب تو بر ماه می‌کشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسم خدا نکرده کشد از تو انتقام</p></div>
<div class="m2"><p>آهی که عاشقان به سحرگاه می‌کشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این است اگر صعوبت عشق تو، رهروان</p></div>
<div class="m2"><p>در اولین قدم، قدم از راه می‌کشند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برداشت عشق پرده به حدی که عاشقان</p></div>
<div class="m2"><p>بر لوح سینه نقش انا الله می‌کشند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فارغ ز رشک بوالهوسانم فروغیا</p></div>
<div class="m2"><p>چون داغ عشق بر دل آگاه می‌کشند</p></div></div>