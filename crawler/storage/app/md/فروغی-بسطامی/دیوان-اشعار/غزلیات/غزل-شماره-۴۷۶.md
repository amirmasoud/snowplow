---
title: >-
    غزل شمارهٔ ۴۷۶
---
# غزل شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>زان فشانم اشک در هر رهگذاری</p></div>
<div class="m2"><p>تا به دامان تو ننشیند غباری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلفت از هر حلقه می‌بندد اسیری</p></div>
<div class="m2"><p>چشمت از هر گوشه می‌گیرد شکاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برای بی قراران محبت</p></div>
<div class="m2"><p>آه اگر زلف تو نگذارد قراری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اختیاری آید اندر دست ما را</p></div>
<div class="m2"><p>گر گذارد عشق در دست اختیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تو گر گوشهٔ کارم نگیرد</p></div>
<div class="m2"><p>پیش نتوانم گرفتن هیچ کاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رنج عشقت راحت هر دردمندی</p></div>
<div class="m2"><p>زخم تیغت مرهم هر دل فکاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کنارم رفته تا آن سرو بالا</p></div>
<div class="m2"><p>جوی اشکم می‌رود از هر کناری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوشه‌ای خواهم نهان از چشم مردم</p></div>
<div class="m2"><p>تا به کام دل بگیریم روزگاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا گره بگشاید از کارم فروغی</p></div>
<div class="m2"><p>بسته‌ام دل را به زلف تاب داری</p></div></div>