---
title: >-
    غزل شمارهٔ ۴۰۲
---
# غزل شمارهٔ ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>با آن غزال وحشی گر خواهی آرمیدن</p></div>
<div class="m2"><p>چندین هزار احسنت می‌بایدت کشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی اگر در آغوش سروی کشی قباپوش</p></div>
<div class="m2"><p>سهل است در محبت پیراهنی دریدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر حلقهٔ سلامت در دام او فتادن</p></div>
<div class="m2"><p>سرمایهٔ ندامت از بام او پریدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیمانهٔ حیاتم پر شد فغان که نتوان</p></div>
<div class="m2"><p>پیمان ازو گرفتن، پیوند از او بریدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهوی چشمش آخر رامم نشد به افسون</p></div>
<div class="m2"><p>یارب به او که آموخت این شیوه رمیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانی که تفسیر از دوستی چیست</p></div>
<div class="m2"><p>از جان خود گذشتن، در خون خود تپیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصد رسید و مردم از رشک خود که نتوان</p></div>
<div class="m2"><p>پیغام آشنا را از دیگری شنیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ از تو حاصلم نیست دردا که عین خار است</p></div>
<div class="m2"><p>در پای گل نشستن، وان گه گلی نچیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسایشی ز کوشش در عاشقی ندیدیم</p></div>
<div class="m2"><p>تا کی توان فروغی دنبال دل دویدن</p></div></div>