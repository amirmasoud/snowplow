---
title: >-
    غزل شمارهٔ ۶۵
---
# غزل شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>آن که مرادش تویی از همه جویاتر است</p></div>
<div class="m2"><p>وان که در این جستجو است از همه پویاتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همه صورتگران صورت زیبا کشند</p></div>
<div class="m2"><p>صورت زیبای تو از همه زیباتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به چمن صف زنند خیل سهی قامتان</p></div>
<div class="m2"><p>قامت رعنای تو از همه رعناتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنبل مشکین تو از همه آشفته‌تر</p></div>
<div class="m2"><p>نرگس شهلای تو از همه شهلاتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن دل آرای تو از همه مشهورتر</p></div>
<div class="m2"><p>عاشق رسوای تو از همه رسواتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مست مقامات شوق از همه هشیارتر</p></div>
<div class="m2"><p>پیر خرابات عشق از همه برناتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن که به محراب گفت از همه مؤمن‌ترم</p></div>
<div class="m2"><p>گر دو سه جامش دهند از همه ترساتر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بادهٔ پایندگی از کف ساقی گرفت</p></div>
<div class="m2"><p>آن که به پای قدح از همه بی‌پاتر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر غم عشق را در دل اندوهناک</p></div>
<div class="m2"><p>هر چه نهان می‌کنی از همه پیداتر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون که سلاطین کنند دعوی بالاتری</p></div>
<div class="m2"><p>رایت سلطان عشق از همه بالاتر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر همه شاهان برند دست به برنده تیغ</p></div>
<div class="m2"><p>تیغ جهان‌گیر شاه از همه براتر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناصردین شهریار، تاج ده و تاج‌دار</p></div>
<div class="m2"><p>آن که به تدبیر کار از همه داناتر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اختر فیروز او از همه فیروزتر</p></div>
<div class="m2"><p>گوهر والای او از همه والاتر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرغ چمن دم نزد پیش فروغی بلی</p></div>
<div class="m2"><p>آن که زبانش تویی از همه گویاتر است</p></div></div>