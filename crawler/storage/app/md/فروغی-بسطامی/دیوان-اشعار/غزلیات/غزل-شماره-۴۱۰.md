---
title: >-
    غزل شمارهٔ ۴۱۰
---
# غزل شمارهٔ ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>غافل گذشتی از دل امیدوار من</p></div>
<div class="m2"><p>رسوای اگر چنین گذرد روزگار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب به بزم خندهٔ بی اختیار تو</p></div>
<div class="m2"><p>افزون نمود گریه بی اختیار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نیستم حریف تو با صدهزار دل</p></div>
<div class="m2"><p>کز یک کرشمه می‌شکنی صدهزار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک عمر را به روزه بسر برده‌ام مگر</p></div>
<div class="m2"><p>روزی لبت رسد به لب روزه‌دار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زلف بی قرار تو باشد قرار دل</p></div>
<div class="m2"><p>بر یک قرار نیست دل بی قرار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی مرا و تا سر خاکم نیامدی</p></div>
<div class="m2"><p>آه از سیاه‌بختی خاک مزار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند از آن نگاه نهانی چه دیده‌ای</p></div>
<div class="m2"><p>پیداست آن چه دیده‌ام از خاک زار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخت سیاه بین که دو چشمم سفید شد</p></div>
<div class="m2"><p>در کار گریه‌ای که نیامد به کار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز و شبی که مایهٔ چندین عقوبت است</p></div>
<div class="m2"><p>روز قیامت است و شب انتظار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر تا قدم کرشمه و ناز است و دلبری</p></div>
<div class="m2"><p>شاهین تیز پنجهٔ عاشق شکار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن بختم از کجاست فروغی که روزگار</p></div>
<div class="m2"><p>روزی کند نشیمن او در کنار من</p></div></div>