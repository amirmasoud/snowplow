---
title: >-
    غزل شمارهٔ ۴۳۸
---
# غزل شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>ساقی دل نرگس شهلای تو</p></div>
<div class="m2"><p>مستی جان از می مینای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز سر زلف چلیپای تو</p></div>
<div class="m2"><p>اهل جنون سلسله در پای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه نهادم به دم تیغ عشق</p></div>
<div class="m2"><p>دیده گشادم به تماشای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست بلای دل صاحب‌دلان</p></div>
<div class="m2"><p>جلوهٔ بالای دل آرای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو کند با همه آزادگی</p></div>
<div class="m2"><p>بندگی قامت رعنای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باخته‌ام از پی یک بوسه جان</p></div>
<div class="m2"><p>یافته‌ام قیمت کالای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده برانداز که نتوان نمود</p></div>
<div class="m2"><p>قطع نظر از رخ زیبای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پا نکشم از سر کوی امید</p></div>
<div class="m2"><p>تا ندهم جان به تمنای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان فروغی نرسد بر مراد</p></div>
<div class="m2"><p>تا نرود بر سر سودای تو</p></div></div>