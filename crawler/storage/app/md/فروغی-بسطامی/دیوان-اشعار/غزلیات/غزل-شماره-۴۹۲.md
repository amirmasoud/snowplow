---
title: >-
    غزل شمارهٔ ۴۹۲
---
# غزل شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>سر راهش افتادم از ناتوانی</p></div>
<div class="m2"><p>وزین ضعف کردم بسی کامرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کاو به دل ناوکش خورد گفتا</p></div>
<div class="m2"><p>که شوخی ندیدم بدین شخ کمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز چشمی است چشم امیدم که هرگز</p></div>
<div class="m2"><p>به کس ننگرد از ره سرگرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبان از شکایت بر دوست بستم</p></div>
<div class="m2"><p>ز بس یافتم لذت بی‌زبانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان خواهی از وی، ز خود بی‌نشان شو</p></div>
<div class="m2"><p>که من زو نشان جستم از بی‌نشانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی داند احوال پیران عشقش</p></div>
<div class="m2"><p>که پیرانه سر کرده باشد جوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به هجران مرا سهل شد دادن جان</p></div>
<div class="m2"><p>که سخت است دوری ز یاران جانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دریغا که از ماه رویان ندیدم</p></div>
<div class="m2"><p>به جز بی وفایی و نامهربانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شنیدن توان نغمهٔ ارغنون را</p></div>
<div class="m2"><p>چو ساقی دهد بادهٔ ارغوانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من و زخم کاری، تو و دل شکاری</p></div>
<div class="m2"><p>من و جان سپاری، تو و جان ستانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو و عشوه کردن، من و دل سپردن</p></div>
<div class="m2"><p>تو و جان گرفتن، من و جان فشانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکش خنجر کین به جان فروغی</p></div>
<div class="m2"><p>به طوری که خواهی، به طرزی که دانی</p></div></div>