---
title: >-
    غزل شمارهٔ ۲۷۲
---
# غزل شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>منت خدای را که خداوند بی‌نیاز</p></div>
<div class="m2"><p>عمر دوباره داد به شاه گدانواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داری تخت ناصردین شاه تاجور</p></div>
<div class="m2"><p>کز فضل کردگار بود عمر او دراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سرکشان دیومنش را کشد به خون</p></div>
<div class="m2"><p>پا بر سر سریر سلیمان نهاد باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستوفی قلمرو او مالک عراق</p></div>
<div class="m2"><p>طغرانویس دفتر او والی حجاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارکان خصم سوخته از قهر خصم سوز</p></div>
<div class="m2"><p>کار زمانه ساخته از لطف کارساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم دوستان او همه در عیش و در نشاط</p></div>
<div class="m2"><p>هم دشمنان او همه در سوز و در گداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم شحنه در ولایت اوباش ذوالجلال</p></div>
<div class="m2"><p>هم فتنه در ممالک او مست خواب ناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم در دعای او همه مردان پاک دل</p></div>
<div class="m2"><p>هم در ثنای او همه رندان پاک‌باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز با محب او نتوان گشتن آشنا</p></div>
<div class="m2"><p>جز از عدوی او نتوان کرد احتراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ایجاد اوست باعث امنیت جهان</p></div>
<div class="m2"><p>زان رو وجوب یافت دعایش به هر نماز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر او نبود مانع ترکان فتنه‌جو</p></div>
<div class="m2"><p>آسودگی نبود جهان را ز ترک تاز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاهی که تا ابد شده از فیض مدح او</p></div>
<div class="m2"><p>هم خامه با طراوت و هم نامه با طراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا روز رستخیز همین است شاه و بس</p></div>
<div class="m2"><p>وین نکته آشکار بود نزد اهل راز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعر از علو طبع فروغی است سربلند</p></div>
<div class="m2"><p>شاعر ز سجدهٔ در شه باد سرفراز</p></div></div>