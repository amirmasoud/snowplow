---
title: >-
    غزل شمارهٔ ۷۸
---
# غزل شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>شربتی در دو لعل جانان است</p></div>
<div class="m2"><p>که خیالش مفرح جان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی قتل مردم دانا</p></div>
<div class="m2"><p>تیغ در دست طفل نادان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌توان یافتن ز زخم دلم</p></div>
<div class="m2"><p>کاین جراحت نه کار پیکان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قتل‌گاهی است کوی او کان جا</p></div>
<div class="m2"><p>زخم بیداد و تیغ پنهان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم از نالهٔ شعله در خرمن</p></div>
<div class="m2"><p>چشمم از گریه خانه ویران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر زلفی چگونه گردد جمع</p></div>
<div class="m2"><p>که از آن مجمعی پریشان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم امید هر مسلمانی</p></div>
<div class="m2"><p>پی آن چشم نامسلمان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو درمان درد عشاقی</p></div>
<div class="m2"><p>درد الحق که عین درمان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منع زاری مکن فروغی را</p></div>
<div class="m2"><p>که گلت را هزار دستان است</p></div></div>