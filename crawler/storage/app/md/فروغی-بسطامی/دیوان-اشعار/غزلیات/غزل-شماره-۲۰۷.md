---
title: >-
    غزل شمارهٔ ۲۰۷
---
# غزل شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>تا خیل غمش در دل ناشاد من آمد</p></div>
<div class="m2"><p>هر جا که دلی بود به امداد من آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای سر زلف کمندافکن ساقی</p></div>
<div class="m2"><p>سیلی است که در کندن بنیاد من آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سیل که برخاست ز کهسار محبت</p></div>
<div class="m2"><p>اول به در خانهٔ آباد من آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا که بیان کرد کسی قصهٔ یوسف</p></div>
<div class="m2"><p>حال دل گم گشته خود یاد من آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر شب که فلک زان مه بی مهر سخن گفت</p></div>
<div class="m2"><p>یک شهر به فریاد ز فریاد من آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زلفش به عدم گر کشدم هیچ غمی نیست</p></div>
<div class="m2"><p>کاین سلسله سرمایهٔ ایجاد من آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از چنگل شاهین اجل باک ندارد</p></div>
<div class="m2"><p>هر صید که در پنجهٔ صیاد من آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیداست که از آب بقا خضر ندیده‌ست</p></div>
<div class="m2"><p>آن فیض که از خنجر جلاد من آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فریاد که داد از ستمش می‌نتوان زد</p></div>
<div class="m2"><p>بیدادگری کز پی بیداد من آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک آدم عاقل نتوان یافت فروغی</p></div>
<div class="m2"><p>شهری که در آن شوخ پری زاد من آمد</p></div></div>