---
title: >-
    غزل شمارهٔ ۳۲۱
---
# غزل شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>ز تجلی جمالش از دو کون بستم</p></div>
<div class="m2"><p>به صمد نمود راهم صنمی که می‌پرستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هوای مهر رویش همه مهرها بریدیم</p></div>
<div class="m2"><p>به امید عهد سستش همه عهده شکستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی دیدن خرامش سر کوچه‌ها ستادم</p></div>
<div class="m2"><p>پی جلوهٔ جمالش در خانه‌ها نشستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم اولین شکارش به شکارگاه نازش</p></div>
<div class="m2"><p>که به هیچ حیله آخر ز کمند او نجستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی آن غزال مشکین که نگشت صیدم آخر</p></div>
<div class="m2"><p>چه سمندها دواندم چه کمندها گسستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه انتقام خود را بکشم ز عمر رفته</p></div>
<div class="m2"><p>دهد ار زمانه روزی سر زلف او به دستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گناه عشق کشتیم و هنوز برنگشتیم</p></div>
<div class="m2"><p>ز ارادتی که بودم ز محبتی که هستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به لباس مرغ و ماهی روم ار به کوه و دریا</p></div>
<div class="m2"><p>تو درآوری به دامم تو درافکنی به شستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه میکشان محفل ز می شبانه سرخوش</p></div>
<div class="m2"><p>به خلاف من فروغی که ز چشم دوست مستم</p></div></div>