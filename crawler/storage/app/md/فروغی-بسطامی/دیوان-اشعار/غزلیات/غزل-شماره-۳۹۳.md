---
title: >-
    غزل شمارهٔ ۳۹۳
---
# غزل شمارهٔ ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>ما ز چشم تو مست یک نگهیم</p></div>
<div class="m2"><p>بی خبر از خمار صبح گهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به باد فنا دهی ما را</p></div>
<div class="m2"><p>سر مویت به عالمی ندهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه‌در گوش پیر میکده‌ایم</p></div>
<div class="m2"><p>خانه بر دوش ملک پادشهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک می‌خانه آب حیوان است</p></div>
<div class="m2"><p>همره ما بیا که خضر رهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک روب در سرای مغان</p></div>
<div class="m2"><p>خاکسار بتان کج کلهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود محیط رحمت دوست</p></div>
<div class="m2"><p>کشتی جرم و لنگر گنهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل به چشم سیاه او دادیم</p></div>
<div class="m2"><p>تا نگوید کسی که دل سیهیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش طفلی سپر بیفکندیم</p></div>
<div class="m2"><p>با وجودی که مرد صد سیهیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریخت بر چهره جعد ریحان را</p></div>
<div class="m2"><p>کز کمندش به هیچ رو نجهیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست ما را ببست نیروی عشق</p></div>
<div class="m2"><p>که ز اندازه پا برون ننهیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا فروغی جمال او دیدیم</p></div>
<div class="m2"><p>بی نیاز از فروغ مهر و مهیم</p></div></div>