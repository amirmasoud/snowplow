---
title: >-
    غزل شمارهٔ ۴۲
---
# غزل شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>هر چه کردم به ره عشق وفا بود، وفا</p></div>
<div class="m2"><p>وانچه دیدم به مکافات جفا بود ، جفا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شربت من ز کف یار الم بود، الم</p></div>
<div class="m2"><p>قسمت من ز در دوست بلا بود، بلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکه عشق زدن محض غلط بود ، غلط</p></div>
<div class="m2"><p>عاشق ترک شدن عین خطا بود، خطا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار خوبان ستم پیشه گران بود ، گران</p></div>
<div class="m2"><p>کار عشاق جگر خسته دعا بود، دعا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب حاصل احباب فغان بود، فغان</p></div>
<div class="m2"><p>همه جا شاهد احوال خدا بود، خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک ما نسخهٔ صد رشته گهر بود، گهر</p></div>
<div class="m2"><p>درد ما مایهٔ صد گونه دوا بود، دوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس ما از مدد عشق قوی بود، قوی</p></div>
<div class="m2"><p>سر ما در ره معشوق فدا بود، فدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دعوی پیر خرابات به حق بود، به حق</p></div>
<div class="m2"><p>عمل شیخ مناجات ریا بود، ریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که جز مهر تو اندوخت هوس بود، هوس</p></div>
<div class="m2"><p>آن که جز عشق تو ورزید هوا بود، هوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر ستم کز تو کشیدیم کرم بود،کرم</p></div>
<div class="m2"><p>هر خطا کز تو به ما رفت عطا بود، عطا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زخم کاری زفراق تو به جان بود، به جان</p></div>
<div class="m2"><p>جان سپاری به وصال تو به جا بود، بجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در همه عمر فروغی به طلب بود، طلب</p></div>
<div class="m2"><p>در همه حال وجودش به رجا بود، رجا</p></div></div>