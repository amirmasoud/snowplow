---
title: >-
    غزل شمارهٔ ۱۲۱
---
# غزل شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>من کیم، پروانهٔ شمعی که در کاشانه نیست</p></div>
<div class="m2"><p>خانه‌ام را سوخت بی‌باکی که او در خانه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست همت را کشیدم از سر دنیا و دین</p></div>
<div class="m2"><p>هر کسی را در طلب این همت مردانه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پس رنجی که بردم در وفا آخر مرا</p></div>
<div class="m2"><p>دامن گنجی به چنگ آمد که در ویرانه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌گساران فارغند از فتنه دور زمان</p></div>
<div class="m2"><p>کس حریف آسمان جز گردش پیمانه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبحهٔ صد دانه از بهر حساب ساغر است</p></div>
<div class="m2"><p>ور نه یک جو خاصیت در سبحهٔ صد دانه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریهٔ مستانه آخر عقده‌ام از دل گشود</p></div>
<div class="m2"><p>خندهٔ شادی به غیر از گریهٔ مستانه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقد زاهد قابل آن شاهد زیبا نشد</p></div>
<div class="m2"><p>زان که هر جان مقدس در خور جانانه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا غم دلبر درآمد خرمی از دل برفت</p></div>
<div class="m2"><p>زان که جای آشنا در منزل بیگانه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غم آن نوش لب افسانهٔ عالم شدم</p></div>
<div class="m2"><p>وین غم دیگر که تاثیری در این افسانه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم از دیوانگی زلفش بگیرم، عشق گفت</p></div>
<div class="m2"><p>لایق این حلقهٔ زنجیر هر دیوانه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا فروغی پرتو آن شمع در محفل فتاد</p></div>
<div class="m2"><p>هیچ کس از سوز من آگه به جز پروانه نیست</p></div></div>