---
title: >-
    غزل شمارهٔ ۵۰۵
---
# غزل شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>اولین گام ار سمند عقل را پی می‌کنی</p></div>
<div class="m2"><p>وادی بی منتهای عشق را طی می‌کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به دور چشم مستت فارغ از می‌خانه‌ایم</p></div>
<div class="m2"><p>کز نگاهی کار صد پیمانهٔ می می‌کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز محشر هم نمی‌آیی به دیوان حساب</p></div>
<div class="m2"><p>پس حساب کشتگان عشق را کی می‌کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی را وعده‌ای در وعده گاهی داده‌ای</p></div>
<div class="m2"><p>وعدهٔ قتل مرا نی می‌دهی نی می‌کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد جان را در بهای بوسه می‌گیری ز غیر</p></div>
<div class="m2"><p>کاش با ما می شد این سودا که با وی می‌کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو ای عیسی نفس می‌ریزی از مینا به جام</p></div>
<div class="m2"><p>زنده را جان می فزایی، مرده را حی می‌کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاه ساقی گاه مطرب می‌شوی در انجمن</p></div>
<div class="m2"><p>دل نوازی گاهی از می گاهی از نی می‌کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشمنان را هی به کف جام دمادم می‌دهد</p></div>
<div class="m2"><p>دوستان را هی به دل خون پیاپی می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کشور چین و ختا را زلف و مژگانت گرفت</p></div>
<div class="m2"><p>حالیا لشکر کشی بر روم و بر ری می‌کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر تو را تاج نمد بر سر نهد سلطان عشق</p></div>
<div class="m2"><p>کی به سر دیگر هوای افسر می‌کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وصل آن معشوق باقی را فروغی کس نیافت</p></div>
<div class="m2"><p>تا به کی از عشق او هو می‌زنی، هی می‌کنی</p></div></div>