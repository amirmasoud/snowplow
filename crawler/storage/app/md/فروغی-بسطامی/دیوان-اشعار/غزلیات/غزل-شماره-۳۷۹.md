---
title: >-
    غزل شمارهٔ ۳۷۹
---
# غزل شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>گر به گل‌زار رخش افتد نگاه گاه گاهم</p></div>
<div class="m2"><p>گل به دامن می‌توان برد از گلستان نگاهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش گل چیست، گفتا پیرهن چاک نسیمم</p></div>
<div class="m2"><p>گفتمش مه چیست، گفتا سایه پرورد کلاهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصهٔ توفان نوح افسانه‌ای از موج اشکم</p></div>
<div class="m2"><p>شعلهٔ نار خلیل انگاره‌ای از برق آهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو چنان عشقی که تا یک جا به فرساید وجودم</p></div>
<div class="m2"><p>کو چنان برقی که تا یک سر به سوزاند گیاهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مالک عفوش ندانم تا نپوشاند خطایم</p></div>
<div class="m2"><p>صاحب فضلش ندانم تا نبخشاید گناهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر شمشیر اجل بردم پناه از بی‌پناهی</p></div>
<div class="m2"><p>آه اگر محراب ابرویش نگیرد در پناهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به خاک من پس از کشتن گذار قاتل افتد</p></div>
<div class="m2"><p>ماجرا دیگر بگویم، خون بها هرگز نخواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاجت از بی حاجتی در عشق می‌باید گرفتن</p></div>
<div class="m2"><p>من خوشم با ناامیدی تا تویی امیدگاهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شربت وصلم ندادی تا نخوردم زهر هجران</p></div>
<div class="m2"><p>بوسه بر پایت ندادم تا نکردی خاک راهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه قمر پندارمت، گاهی پری، گاهی فرشته</p></div>
<div class="m2"><p>پرده از رخ برفکن یعنی برآر از اشتباهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من که از روز ازل دیدم جمالش را فروغی</p></div>
<div class="m2"><p>تا به فردای قیامت فارغ از خورشید و ماهم</p></div></div>