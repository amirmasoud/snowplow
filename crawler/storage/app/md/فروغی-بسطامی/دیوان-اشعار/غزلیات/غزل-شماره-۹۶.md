---
title: >-
    غزل شمارهٔ ۹۶
---
# غزل شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>هر دم ای گل از تو در گلشن فغان تازه است</p></div>
<div class="m2"><p>عندلیبان کهن را داستان تازه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کدامین خسته را کشتی ز تیغ بی‌دریغ</p></div>
<div class="m2"><p>زان که بر دامانت از خونش نشان تازه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برقی از هر گوشه آهنگ گلستان کرد باز</p></div>
<div class="m2"><p>گوییا بر شاخ طرح آشیان تازه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون جرس در سینه می‌نالد دل زارم مگر</p></div>
<div class="m2"><p>نوسفر ماهی میان کاروان تازه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی از مژگان و ابرو کشته در میدان عشق</p></div>
<div class="m2"><p>ترک سر مست مرا تیر و کمان تازه‌است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاش آن سرو روان بهر تماشا آمدی</p></div>
<div class="m2"><p>تا به جوی دیده‌ام آب روان تازه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز امتحان صد ره فزون‌تر گشت و بازم زنده کرد</p></div>
<div class="m2"><p>وه که با من هر زمانش امتحان تازه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایمنم از خون خود در عشق آن زیبا جوان</p></div>
<div class="m2"><p>کز خط سبزش مرا خط امان تازه است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق نیرنگی به کار آورده کز هرگوشه‌ای</p></div>
<div class="m2"><p>منحنی پیری گرفتار جوان تازه است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من فروغی گشتم از ذوق لب او نکته سنج</p></div>
<div class="m2"><p>شکری را طوطی شیرین زبان تازه است</p></div></div>