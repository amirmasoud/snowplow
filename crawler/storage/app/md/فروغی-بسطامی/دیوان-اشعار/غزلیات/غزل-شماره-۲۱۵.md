---
title: >-
    غزل شمارهٔ ۲۱۵
---
# غزل شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>مستان بزم عشق شرابی نداشتند</p></div>
<div class="m2"><p>در عین بی خودی می نابی نداشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز به غیر خون دل و پارهٔ جگر</p></div>
<div class="m2"><p>شوریدگان شراب و کبابی نداشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قربان قاتلی که شهیدان عشق او</p></div>
<div class="m2"><p>جز آب تیغ حسرت آبی نداشتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با قاتل از غرور ندارد سر حساب</p></div>
<div class="m2"><p>با کشتگان عشق حسابی نداشتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قومی به فیض پیر خرابات کی رسند</p></div>
<div class="m2"><p>کز جام باده حال خرابی نداشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنان که داغ و درد تو بردند زیر خاک</p></div>
<div class="m2"><p>خوف جحیم و بیم عذابی نداشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تمکین حسن بین که به کوی تو اهل عشق</p></div>
<div class="m2"><p>بعد از سؤال چشم جوابی نداشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز آشفتگی به حلقهٔ جمعی رسیده‌ام</p></div>
<div class="m2"><p>کز حلقه‌های زلف تو تابی نداشتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا چشم بند مردم صاحب نظر شدی</p></div>
<div class="m2"><p>شب ها ز سحر چشم تو خوابی نداشتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مکتب محبت آن مه فروغیا</p></div>
<div class="m2"><p>الا کتاب مهر کتابی نداشتند</p></div></div>