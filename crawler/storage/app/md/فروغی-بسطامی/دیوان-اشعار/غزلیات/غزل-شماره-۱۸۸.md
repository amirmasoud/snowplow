---
title: >-
    غزل شمارهٔ ۱۸۸
---
# غزل شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>هر گه که ناوکی ز کمانت کمانه کرد</p></div>
<div class="m2"><p>اول شکاف سینهٔ مرا نشانه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستی که بر میان وصال تو می‌زدم</p></div>
<div class="m2"><p>تیغ فراق منقطعش از میانه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشمم اوفتاد به شاهین زلف تو</p></div>
<div class="m2"><p>عنقای عشق بر سر من آشیانه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیل غمت فتاد به فکر خرابی‌ام</p></div>
<div class="m2"><p>چندان که در خرابه من جغد خانه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ناف آهوان ختا نافه گشت خون</p></div>
<div class="m2"><p>تا جعد مشک‌بوی تو را باد شانه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سر خبر ز سر محبت کجا شود</p></div>
<div class="m2"><p>الا سری که سجدهٔ آن آستانه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تنها من اسیر خط و خال او شدم</p></div>
<div class="m2"><p>بس مرغ دل که صید بدین دام و دانه کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیغ ستم کشیده به سر وقت من رسید</p></div>
<div class="m2"><p>الحق که در حقم کرم بی‌کرانه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم مگر ز باده به دامن نشانمش</p></div>
<div class="m2"><p>برخاست از میانه و مستی بهانه کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منت خدای را که شراب صبوحی‌ام</p></div>
<div class="m2"><p>فارغ ز ورد صبح و دعای شبانه کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی مهری از تو دید فروغی ولی مدام</p></div>
<div class="m2"><p>فریاد از آسمان و فغان از زمانه کرد</p></div></div>