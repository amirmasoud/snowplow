---
title: >-
    غزل شمارهٔ ۴۰
---
# غزل شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>کاش آن صنم آماده شدی جلوه‌گری را</p></div>
<div class="m2"><p>در پرده نشاندی صنم کاشغری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جعد تو مویی فکند بر سر آتش</p></div>
<div class="m2"><p>احضار کند روح هوا فوج پری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از منظر خورشید تو گر پرده برافتد</p></div>
<div class="m2"><p>هر ذره کند دعوی صاحب‌نظری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گه که چو طاوس خرامی عجبی نیست</p></div>
<div class="m2"><p>گر طوق به گردن فکنی کبک دری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خط تو بر صفحهٔ رخسار ندیدم</p></div>
<div class="m2"><p>واقف نشدم فتنهٔ دور قمری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر پای نهی از سر رحمت به گلستان</p></div>
<div class="m2"><p>درهم شکنی رونق گل‌برگ طری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سرو قباپوش تو در جلوه درآید</p></div>
<div class="m2"><p>البته پری شیوه کند جامه دری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کحال صبا از اثر گرد قدومت</p></div>
<div class="m2"><p>از نرگس شهلا ببرد بی‌بصری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس که دم از حور زند عین قصور است</p></div>
<div class="m2"><p>گفتن نتوان با تو حدیث دگری را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیغام فروغی نرسد بر سر کویت</p></div>
<div class="m2"><p>که آنجا گذری نیست نسیم سحری را</p></div></div>