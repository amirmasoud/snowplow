---
title: >-
    غزل شمارهٔ ۲۰
---
# غزل شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>گرفت خط رخ زیبای گل عذار مرا</p></div>
<div class="m2"><p>فغان که دهر، خزان کرد نوبهار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشیدسرمه به چشم و فشاند طره به رو</p></div>
<div class="m2"><p>بدین بهانه سیه کرد روزگار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرشته بندگیش را به اختیار کند</p></div>
<div class="m2"><p>پری رخی که ز کف برده اختیار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ربود هوش مرا چشم او به سرمستی</p></div>
<div class="m2"><p>که چشم بد نرسد مست هوشیار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه کار من از کار نگذرد شب هجر</p></div>
<div class="m2"><p>که طره‌اش به خود انداخت کار و بار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نداده است کسی روز بی‌کسی جز غم</p></div>
<div class="m2"><p>تسلی دل بی صبر و بی‌قرار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته‌ام به درستی شکنج زلف بتی</p></div>
<div class="m2"><p>اگر سپهر نخواهد شکست کار مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عزیز هر دو جهان باشی از محبت دوست</p></div>
<div class="m2"><p>که خواری تو فزون ساخت اعتبار مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروغی آن که به من توبه می‌دهد از عشق</p></div>
<div class="m2"><p>خدا کند که ببیند جمال یار مرا</p></div></div>