---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>طوطی وظیفه‌خوار لب نوشخند توست</p></div>
<div class="m2"><p>شکر فروش مصر خریدار قند توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوزخ کنایتی ز دل سوزناک من</p></div>
<div class="m2"><p>جنت حکایتی ز رخ دل پسند توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگسیختم دل از خم گیسوی حور عین</p></div>
<div class="m2"><p>تا حلق من به حلقهٔ مشکین کمند توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوبی سر از خجالت خویش افکند به زیر</p></div>
<div class="m2"><p>هر جا حدیث جلوهٔ سرو بلند توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ بر فروز و از نظر بد حذر مکن</p></div>
<div class="m2"><p>زیرا که خان بر سر آتش سپند توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که در قلمرو خوبی مسلمی</p></div>
<div class="m2"><p>چشم زمانه در پی دفع گزند توست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سر سزای عرصهٔ میدان عشق نیست</p></div>
<div class="m2"><p>الا سری که بر سم رعنا سمند توست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی ز شهر بند خیالم به در مرو</p></div>
<div class="m2"><p>بیرون کسی نرفت که در شهر بند توست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داند چگونه جان فروغی به لب رسید</p></div>
<div class="m2"><p>هر کس که در طریق طلب دردمند توست</p></div></div>