---
title: >-
    غزل شمارهٔ ۱۶۸
---
# غزل شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>آن که یک ذره غمت در دل پر غم دارد</p></div>
<div class="m2"><p>اگر انصاف دهد عیش دو عالم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده با قد تو کی سایه طوبی جوید</p></div>
<div class="m2"><p>سینه با داغ تو کی خواهش مرهم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کم و بیش آن که به دو چشم ترحم دای</p></div>
<div class="m2"><p>هرگز اندیشه نه از بیش و نه از کم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقلی کز شکن زلف تو دیوانه شود</p></div>
<div class="m2"><p>سر این سلسله باید که محکم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که کام از لب شیرین تو خواهد، باید</p></div>
<div class="m2"><p>نیش را بر قدح نوش مقدم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من سودا زدهٔ جمعم ز پریشانی دل</p></div>
<div class="m2"><p>کاین پریشانی از آن طرهٔ پر خم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاکرم شاکر اگر زهر پیاپی بخشد</p></div>
<div class="m2"><p>خوش‌دلم خوش‌دل اگر نیش دمادم دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مکرر سخن تلخ بگوید معشوق</p></div>
<div class="m2"><p>عاشق آن است که این نکته مسلم دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یارب از هیچ غمی خاطرت آزرده مباد</p></div>
<div class="m2"><p>که فروغی ز غمت خاطر خرم دارد</p></div></div>