---
title: >-
    غزل شمارهٔ ۵۱۵
---
# غزل شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>خرم آن عاشق که آشوب دل و دینش تویی</p></div>
<div class="m2"><p>کار فرمایش محبت، مصلحت بینش تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شورش عشاق در عهد لب شیرین لبت</p></div>
<div class="m2"><p>ای خوشا عهدی که شورش عشق و شیرینش تویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق روی تو می‌نازد به خیل عاشقان</p></div>
<div class="m2"><p>پادشاهی می‌کند صیدی که صیادش تویی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستی عشق تو را هشیاری از دنبال هست</p></div>
<div class="m2"><p>بر نمی‌خیزد ز خواب آن سر که بالینش تویی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاو جولان می‌نیاید بر زمین از سرکشی</p></div>
<div class="m2"><p>پای آن توسن که اندر خانهٔ زینش تویی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌برم رشک نظربازی که از بخت بلند</p></div>
<div class="m2"><p>در میان سرو قدان سرو سیمینش تویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ببارد اشک گلگون دیدهٔ من دور نیست</p></div>
<div class="m2"><p>کاین گل رنگین دهد باغی که گلچینش تویی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوستان حسن را یارب خزان هرگز مباد</p></div>
<div class="m2"><p>تا بهار سنبل ریحان و نسرینش تویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زندگی بهر فروغی در محبت مشکل است</p></div>
<div class="m2"><p>تا به جرم مهربانی بر سر کینش تویی</p></div></div>