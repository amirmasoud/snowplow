---
title: >-
    غزل شمارهٔ ۴۰۴
---
# غزل شمارهٔ ۴۰۴

<div class="b" id="bn1"><div class="m1"><p>بر صفحهٔ رخ از خط مشکین رقم مزن</p></div>
<div class="m2"><p>بر نامهٔ حیات محبان قلم مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ عتاب بر سر اهل وفا مکش</p></div>
<div class="m2"><p>تیر هلاک بر دل صید حرم مزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتادگان بند تو جایی نمی‌روند</p></div>
<div class="m2"><p>مرغان بال بسته به سنگ ستم مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلفی که جایگاه دخل خلق عالم است</p></div>
<div class="m2"><p>بر یکدگر میفکن و عالم به هم مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگی نماند پیش رخت هیچ باغ را</p></div>
<div class="m2"><p>برقع بپوش و طعنه به باغ ارم مزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی که کام دیدی از آن چاک پیرهن</p></div>
<div class="m2"><p>پیراهن دریدهٔ من بین و دم مزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جلوه‌گاه دوست نگاهی فزون مخواه</p></div>
<div class="m2"><p>در کارگاه عشق دم از بیش و کم مزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی ترک سر ز راه ارادت نشان مجو</p></div>
<div class="m2"><p>بی راهبر به کوی محبت قدم مزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر آسمان به کام تو گردد فروغیا</p></div>
<div class="m2"><p>بر آسمان میکده جز جام جم مزن</p></div></div>