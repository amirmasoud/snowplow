---
title: >-
    غزل شمارهٔ ۲۲۸
---
# غزل شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>چینیان گر به کف از جعد تو یک تار آرند</p></div>
<div class="m2"><p>آن چه خواهی به سر نافهٔ تاتار آرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زال گردون به کلافی نخرد یوسف را</p></div>
<div class="m2"><p>گر بدین حسن تو را بر سر بازار آرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز روشن ندهد کاش فلک جمعی را</p></div>
<div class="m2"><p>کز مه روی تو شمعی به شب تار آرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتگان تو کجا زندگی از سر گیرند</p></div>
<div class="m2"><p>که نه بر تربتشان مژدهٔ دیدار آرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم آخر همه مردند ز بیماری دل</p></div>
<div class="m2"><p>به امیدی که تو را بر سر بیمار آرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به کیش تو گناه است محبت، ترسم</p></div>
<div class="m2"><p>که جهان را به صف حشر گنه کار آرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندکی صبر کن از قابل صاحب نظران</p></div>
<div class="m2"><p>تا ز میدان غمت کشتهٔ بسیار آرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله هم در شکن دام تو نتوان که مباد</p></div>
<div class="m2"><p>خط آزادی مرغان گرفتار آرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل از شاخ گل افتد به زمین از مستی</p></div>
<div class="m2"><p>گر سحر بوی خوشت جانب گل‌زار آرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخت بی چشم تو در عین خمارم، ای کاش</p></div>
<div class="m2"><p>یک دو جامم ز در خانه خمار آرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خون بها را نبرد نام فروغی در حشر</p></div>
<div class="m2"><p>اگرش بر دم تیغ تو دگر بار آرند</p></div></div>