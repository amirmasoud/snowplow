---
title: >-
    غزل شمارهٔ ۱۵۵
---
# غزل شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>مصوری که تو را چین زلف مشکین داد</p></div>
<div class="m2"><p>ز مشک زلف تو ما را سرشک خونین داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فدای خامهٔ صورت گری توان گشتن</p></div>
<div class="m2"><p>که زیب عارضت از خط عنبرآگین باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره‌گشایی کارم کسی تواند کرد</p></div>
<div class="m2"><p>که تار زلف خم اندر تو را چین داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من از دو زلف پراکندهٔ تو حیرانم</p></div>
<div class="m2"><p>که جمع دل شدگان را چگونه تسکین داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همان که سکهٔ شاهی به نام حسن تو زد</p></div>
<div class="m2"><p>صلای عشق تو بر عاشقان مسکین داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تلخ کامی فرهاد کی خبر دارد</p></div>
<div class="m2"><p>کسی که بوسه دمادم به لعل شیرین داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهی ز مهر می از شیشه ریخت در جامم</p></div>
<div class="m2"><p>که خوشهٔ عرقش گوش مال پروین داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان حبیب خجل شد ز اشک رنگینم</p></div>
<div class="m2"><p>که در حضور رقیبم شراب رنگین داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمر به کشتن من نازنین نگاری بست</p></div>
<div class="m2"><p>که خون بهای مرا از کف نگارین داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ببین چه می‌کشم از دست پاسبان درش</p></div>
<div class="m2"><p>که می‌برم به در شاه ناصرالدین داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدیو روی زمین آفتاب دولت و دین</p></div>
<div class="m2"><p>که کمترین خدمش حکم بر سلاطین داد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکوه افسر و فر و سریر و زینت کاخ</p></div>
<div class="m2"><p>که تخت را قدمش صدهزار تمکین داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدام اهل دل امشب دعای شه می‌کرد</p></div>
<div class="m2"><p>که جبرئیل امین را زبان آیین داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شها برای فروغی همین سعادت بس</p></div>
<div class="m2"><p>که پیش تخت تو بختش لسان تحسین داد</p></div></div>