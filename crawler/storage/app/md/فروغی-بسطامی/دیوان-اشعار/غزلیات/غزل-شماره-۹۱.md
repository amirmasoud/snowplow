---
title: >-
    غزل شمارهٔ ۹۱
---
# غزل شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>نخست نغمهٔ عشاق فصل گل این است</p></div>
<div class="m2"><p>که داغ لاله‌رخان به ز باغ نسرین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان ز دامن باغی که باغبان آنجا</p></div>
<div class="m2"><p>همیشه چشم امیدش به دست گل‌چین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپرده مرهم زخمم فلک به دست مهی</p></div>
<div class="m2"><p>که صاحب خط خوش‌بوی و خال مشکین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علاج نیست خلاص از کمند او ورنه</p></div>
<div class="m2"><p>ز پای تا به سرم چشم مصلحت بین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عهد عارض گلگون او بحمدالله</p></div>
<div class="m2"><p>که کار اهل نظر ز اشک دیده رنگین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که شهد محبت چشیده می‌داند</p></div>
<div class="m2"><p>که تلخ از آن لب نوشین به طعم شیرین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسیر آن خط سبزم که مو به مو دام است</p></div>
<div class="m2"><p>غلام آن سر زلفم که سر به سر چین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هر کجا که منم شغل اختران مهر است</p></div>
<div class="m2"><p>به هر زمین که تویی کار آسمان کین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سواد زلف تو مجموعهٔ شب و روز است</p></div>
<div class="m2"><p>نگاه چشم تو غارتگر دل و دین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قد تو وقت روش رشک سرو و شمشاد است</p></div>
<div class="m2"><p>رخ تو زیر عرق شرم ماه و پروین است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فروغی از سخن دوست لب نمی‌بندد</p></div>
<div class="m2"><p>که نقل مجلس فرهاد نقل شیرین است</p></div></div>