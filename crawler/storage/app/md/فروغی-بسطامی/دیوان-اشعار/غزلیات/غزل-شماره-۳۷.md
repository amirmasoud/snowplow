---
title: >-
    غزل شمارهٔ ۳۷
---
# غزل شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>دی به رهش فکنده‌ام طفل سرشک دیده را</p></div>
<div class="m2"><p>در کف دایه داده‌ام کودک نورسیده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت رمیده رام شد وحشت من تمام شد</p></div>
<div class="m2"><p>کان سر زلف دام شد پای دل رمیده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لب شکرین او بوسه به جان خریده‌ام</p></div>
<div class="m2"><p>زان که حلاوتی بود جنس گران خریده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به سر من آن پری از سر ناز بگذرد</p></div>
<div class="m2"><p>بر سر راهش افکنم پیرهن دریده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرده ز رخ گشاده‌ای ، داد کرشمه داده‌ای</p></div>
<div class="m2"><p>داغ دگر نهاده‌ای لالهٔ داغ دیده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل به نگاه اولین گشت شکار چشم تو</p></div>
<div class="m2"><p>زخم دگر چه می‌زنی صید به خون تپیده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم سیاه خود نگر هیچ ندیده‌ای اگر</p></div>
<div class="m2"><p>مست کمین گشاده را، ترک کمان کشیده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهر اجل چشیده‌ام تلخی مرگ دیده‌ام</p></div>
<div class="m2"><p>تا ز لبت شنیده‌ام قصهٔ ناشنیده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ نصیب من نشد از دهنش فروغیا</p></div>
<div class="m2"><p>چون به مذاق بسپرم شربت ناچشیده را</p></div></div>