---
title: >-
    غزل شمارهٔ ۱۱۲
---
# غزل شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>هیچ سر نیست که با زلف تو در سودا نیست</p></div>
<div class="m2"><p>هیچ دل نیست که این سلسله‌اش در پا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سر از خاک بر آرند شهیدان در حشر</p></div>
<div class="m2"><p>بر سری نیست که از تیغ تو منت‌ها نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌توان یافتن از حالت چشم سیهت</p></div>
<div class="m2"><p>که نگاه تو نگهدار دل شیدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو ندانم ز کدامین گلی ای مایهٔ ناز</p></div>
<div class="m2"><p>زان که در خاک بشر این همه استغنا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده مستوجب دیدار جمالت نشود</p></div>
<div class="m2"><p>ذره شایستهٔ خورشید جهان‌آرا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس چرا سرو چمن از همه بند آزاد است</p></div>
<div class="m2"><p>گر به جان بندهٔ آن سرو سهی بالا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتمش چشم تو ای دوست هزاران خون کرد</p></div>
<div class="m2"><p>گفت سر مستم و زین کرده مرا حاشا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من به تحقیق صنم خانهٔ چین را دیدم</p></div>
<div class="m2"><p>صنمی را که دلم خواسته بود آنجا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاه کافر کندم گاه مسلمان چه کنم</p></div>
<div class="m2"><p>عشق بی‌قاعده را قاعده‌ای پیدا نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساغری خورده‌ام از بادهٔ لعل ساقی</p></div>
<div class="m2"><p>که مرا حسرت امروز و غم فردا نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مگر آن ماه به شهر از پی آشوب آمد</p></div>
<div class="m2"><p>که فروغی نفسی فارغ ازین غوغا نیست</p></div></div>