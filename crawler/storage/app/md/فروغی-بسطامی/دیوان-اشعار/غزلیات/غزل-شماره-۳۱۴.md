---
title: >-
    غزل شمارهٔ ۳۱۴
---
# غزل شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>دست در حلقهٔ آن جعد چلیپا زده‌ام</p></div>
<div class="m2"><p>دل سودازده را سلسله در پا زده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقم آتش زد و آب مژه از سر بگذشت</p></div>
<div class="m2"><p>پی آن گوهر یک دانه به دریا زده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بر غمزهٔ طفلی سپر انداخته‌ام</p></div>
<div class="m2"><p>من که بر قلب جهان با تن تنها زده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیم کرده چنان مست که هنگام سماع</p></div>
<div class="m2"><p>سنگ بر شیشه نه طارم مینا زده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با من ای زاهد گمراه مزن پنجه به جهل</p></div>
<div class="m2"><p>که ز آه سحری بر صف اعدا زده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منم آن عاشق دیوانه که از غایت شوق</p></div>
<div class="m2"><p>خم زنجیر تو را بر دل شیدا زده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لاله‌زاری شده‌ام بس که به گل‌زار وفا</p></div>
<div class="m2"><p>شعله داغ تو را بر همه اعضا زده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌توان یافت ز طغیان جنونم که مدام</p></div>
<div class="m2"><p>سر سودای تو دارد دل سودازده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پا به گل مانده ز بالای تو طوبی آری</p></div>
<div class="m2"><p>من در این مساله با عالم بالا زده‌ام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که فیض دم جان بخش تو بیند داند</p></div>
<div class="m2"><p>که چرا خنده به انفاس مسیحا زده‌ام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخت بیدار مدد کرد فروغی که به خواب</p></div>
<div class="m2"><p>بوسه‌ای چند بر آن لعل شکرخا زده‌ام</p></div></div>