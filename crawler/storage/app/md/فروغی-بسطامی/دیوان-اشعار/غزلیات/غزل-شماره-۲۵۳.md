---
title: >-
    غزل شمارهٔ ۲۵۳
---
# غزل شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>مانع رفتن به جز مهر و وفای من نبود</p></div>
<div class="m2"><p>ور نه در کوی بتان بندی به پای من نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نبودی کوه اندوه محبت در میان</p></div>
<div class="m2"><p>لقمه‌ای هرگز بقدر اشتهای من نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی از بهر چه کامم را دهان او نداد</p></div>
<div class="m2"><p>انتها در خواهش بی منتهای من نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که در هر پرده نقش صورت شیرین کشید</p></div>
<div class="m2"><p>با خبر از شاهد شیرین ادای من نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقهٔ گیسوی او با من سر سودا نداشت</p></div>
<div class="m2"><p>ور نه کوتاهی ز اقبال رسای من نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا فتادم در قفای چشم سحرانگیز او</p></div>
<div class="m2"><p>کو نظربازی که چشمش در قفای من نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرصهٔ نازش که از اندازه بیرون رفته بود</p></div>
<div class="m2"><p>تنگ شد از کشتگان چندان که جای من نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر شهیدان را به محشر خون بها خواهند داد</p></div>
<div class="m2"><p>پس چرا قاتل به فکر خون بهای من نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پس آتش زدن خاکسترم برباد داد</p></div>
<div class="m2"><p>این عنایت‌های گوناگون سزای من نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من که الا عاشقی جرمی نکردم هیچ وقت</p></div>
<div class="m2"><p>این عقوبت‌های پی در پی جزای من نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد گره زلفش گشود اما ز کار دیگران</p></div>
<div class="m2"><p>صد نگه چشمش نمود اما برای من نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقده‌ها زد بر دل گویا که آن زلف بلند</p></div>
<div class="m2"><p>واقف از عدل شه کشورگشای من نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناصرالدین شاه عادل آن که هنگام دعا</p></div>
<div class="m2"><p>جز بقای دولت او مدعای من نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دعا آخر فروغی حاصلم شد مدعا</p></div>
<div class="m2"><p>تا نپنداری اجابت در دعای من نبود</p></div></div>