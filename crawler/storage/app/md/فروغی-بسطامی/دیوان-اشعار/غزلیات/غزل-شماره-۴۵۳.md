---
title: >-
    غزل شمارهٔ ۴۵۳
---
# غزل شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>ای صورت زیبا که به سیرت ملکستی</p></div>
<div class="m2"><p>بر روی زمین غیرت ماه فلکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه سواری تو که بر غارت دل‌ها</p></div>
<div class="m2"><p>سرگرم ز هر گوشه پی تاز و تکستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کاش ببینند جراحات درونم</p></div>
<div class="m2"><p>تا خلق بدانند که کان نمکستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق آمده عقل از پی بیچارگیش رفت</p></div>
<div class="m2"><p>وین نیست یقین تو که در عین شکستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شیخ که منعم کنی از جنت کویش</p></div>
<div class="m2"><p>زین نکته توان یافت که اهل دَرَکستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عشق جهان‌سوز درآ از در اغیار</p></div>
<div class="m2"><p>تا یار بداند که چه مجرب محکستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نازم سرت ای شمع فروزان فروغی</p></div>
<div class="m2"><p>زیرا که در این بزم الف‌وار یکستی</p></div></div>