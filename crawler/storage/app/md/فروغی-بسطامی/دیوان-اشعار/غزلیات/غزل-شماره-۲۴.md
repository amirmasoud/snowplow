---
title: >-
    غزل شمارهٔ ۲۴
---
# غزل شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>گر به تیغت می‌زند گردن بنه تسلیم را</p></div>
<div class="m2"><p>که آتش نمرود گلشن گشت ابراهیم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا مرو در پیش رویش یا چو رفتی سجده کن</p></div>
<div class="m2"><p>کان خم ابروی واجب کرده این تعظیم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو به هم آمیزش قدر دهانش را ببین</p></div>
<div class="m2"><p>آن که گفتا با الف الفت نباشد میم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست دانی بهره‌مند از سینهٔ سیمین بران</p></div>
<div class="m2"><p>آن که در چشمش تفاوت نیست سنگ و سیم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه مرا امید فردوس است نه بیم جحیم</p></div>
<div class="m2"><p>یا او نگذاشت در خاطر امید و بیم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن که بر بندد کمر در خدمت پیر مغان</p></div>
<div class="m2"><p>می‌نیارد در نظر سلطان هفت اقلیم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواجه گر خونم بریزد جای چندین منت است</p></div>
<div class="m2"><p>بندهٔ شاکر شکایت کی کند تقسیم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر دلبندی فروغی دست نقاش قضا</p></div>
<div class="m2"><p>هیچ تعلیمی نداد آن زلف پر تعلیم را</p></div></div>