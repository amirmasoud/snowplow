---
title: >-
    غزل شمارهٔ ۱۴۵
---
# غزل شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>دل در اندیشهٔ آن زلف گره‌گیر افتاد</p></div>
<div class="m2"><p>عاقلان مژده که دیوانه به زنجیر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه هی منع من از باده‌پرستی تا کی</p></div>
<div class="m2"><p>چه کند بنده که در پنجهٔ تقدیر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامنش را ز پی شکوه گرفتم روزی</p></div>
<div class="m2"><p>که زبان از سخن و نطق ز تقریر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم از مسالهٔ عشق نویسم شرحی</p></div>
<div class="m2"><p>هم ز کف نامه و هم خامه ز تحریر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلبر آمد پی تعمیر دل ویرانم</p></div>
<div class="m2"><p>لیکن آن وقت که این خانه ز تعمیر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامی از جلوهٔ خورشید جهان‌آرا نیست</p></div>
<div class="m2"><p>گوییا پرده از آن حسن جهان‌گیر افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پری از شرم تو از چشم بشر پنهان شد</p></div>
<div class="m2"><p>قمر از رشک تو از بام فلک زیر افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل ز گیسوی تو بگسست و به ابرو پیوست</p></div>
<div class="m2"><p>کار زنجیری عشق تو به شمشیر افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس که بر نالهٔ دل گوش ندادی آخر</p></div>
<div class="m2"><p>هم دل از ناله و هم ناله ز تاثیر افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت زودت کشم آن شوخ فروغی و نکشت</p></div>
<div class="m2"><p>تا چه کردم که چنین کار به تاخیر افتاد</p></div></div>