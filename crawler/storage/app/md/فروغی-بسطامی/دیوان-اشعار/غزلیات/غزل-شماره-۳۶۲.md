---
title: >-
    غزل شمارهٔ ۳۶۲
---
# غزل شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>نرگسش گفت که من ساقی می‌خوارانم</p></div>
<div class="m2"><p>گر چه خود مست ولی آفت هشیارانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژه آراست که غوغای صف عشاقم</p></div>
<div class="m2"><p>طره افشاند که سر حلقهٔ طرارانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ برافروخت که من شمع شب تاریکم</p></div>
<div class="m2"><p>قد برافراخت که من دولت بیدارانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نکته خال و خطش از من سودازده پرس</p></div>
<div class="m2"><p>که نویسندهٔ طومار سیه کارانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد جان بر سر بازار محبت دادم</p></div>
<div class="m2"><p>تا بدانند که من هم ز خریدارانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر بسی بار گران بود ز دوش افکندم</p></div>
<div class="m2"><p>حالیا قافله‌سالار سبک بارانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا مگر بر سر من بگذرد آن یار عزیز</p></div>
<div class="m2"><p>روزگاری است که خاک قدم یارانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بزودی نشوم مست ببخش ای ساقی</p></div>
<div class="m2"><p>زان که دیری است که هم صحبت هشیارانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم از مکر فلک با تو سخن ها دارم</p></div>
<div class="m2"><p>گفت خاموش که من خود سر مکارانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا فروغی خم آن زلف گرفتارم کرد</p></div>
<div class="m2"><p>مو به مو با خبر از حال گرفتارانم</p></div></div>