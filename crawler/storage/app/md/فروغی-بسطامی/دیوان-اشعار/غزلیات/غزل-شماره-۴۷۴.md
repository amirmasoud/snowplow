---
title: >-
    غزل شمارهٔ ۴۷۴
---
# غزل شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>این چه دامی است که از سنبل مشکین داری</p></div>
<div class="m2"><p>که به هر حلقهٔ آن صد دل مسکین داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه را نیش محبت زده‌ای بر دل ریش</p></div>
<div class="m2"><p>این چه نوشی است که در چشمهٔ نوشین داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون بها از تو همین بس که ز خون دل من</p></div>
<div class="m2"><p>دست رنگین و کف پای نگارین داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرقت خوشهٔ پروین و رخت خرمن ماه</p></div>
<div class="m2"><p>وه که بر خرمن مه خوشهٔ پروین داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه صاحب نظران بر سر راهت جمعند</p></div>
<div class="m2"><p>خیز و بخرام اگر قصد دل و دین داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چمن گر نچمی بهر تماشا نه عجب</p></div>
<div class="m2"><p>گر خط و عارض خود سبزه و نسرین داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من اگر سنگ تو بر سینه زنم عیب مکن</p></div>
<div class="m2"><p>زان که در سینهٔ سیمین دل سنگین داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از شکرپاشی کلک تو فروغی پیداست</p></div>
<div class="m2"><p>که به خاطر هوس آن لب شیرین داری</p></div></div>