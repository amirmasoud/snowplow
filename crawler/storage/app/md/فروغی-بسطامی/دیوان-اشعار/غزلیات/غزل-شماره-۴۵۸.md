---
title: >-
    غزل شمارهٔ ۴۵۸
---
# غزل شمارهٔ ۴۵۸

<div class="b" id="bn1"><div class="m1"><p>یک جام با تو خوردن یک عمر می‌پرستی</p></div>
<div class="m2"><p>یک روز با تو بودن، یک روزگار مستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بندگی عشقت از دست رفت کارم</p></div>
<div class="m2"><p>ای خواجهٔ زبر دست رحمی به زیر دستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر باد می‌توان داد خاک وجود ما را</p></div>
<div class="m2"><p>تا کار ما به کویت بالا رود ز پستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مدعی ز مینا می در قدح نکردی</p></div>
<div class="m2"><p>تا خون من نخوردی تا جان من نخستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی دهم شرابت از شیشهٔ محبت</p></div>
<div class="m2"><p>پیمانه‌ام ندادی، پیمان من شکستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید ضعیف عشقم، با پنجهٔ توانا</p></div>
<div class="m2"><p>بیمار چشم یارم، در عین تندرستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با صد هزار نیرو، دیدی فروغی آخر</p></div>
<div class="m2"><p>از دست او نرستی وز بند او نجستی</p></div></div>