---
title: >-
    غزل شمارهٔ ۱۴۳
---
# غزل شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>مدام ذکر ملک این کلام شیرین باد</p></div>
<div class="m2"><p>که خسرو ملکان شاه ناصرالدین باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کبوتری که نیاید به زیر پنجهٔ شاه</p></div>
<div class="m2"><p>سرش ز دست قضا پایمال شاهین باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سمند چرخ که بی‌تازیانه می‌رقصد</p></div>
<div class="m2"><p>پی سواری او زیر زین زرین باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفش همیشه به شمشیر جوهرافشان است</p></div>
<div class="m2"><p>سرش هماره به دیهیم گوهر آگین باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشیب حضرت او سجده‌گاه خورشید است</p></div>
<div class="m2"><p>فراز رایت او بوسه‌گاه پروین باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساط بارگهش چهرهٔ امیران است</p></div>
<div class="m2"><p>چراغ انجمنش دیدهٔ سلاطین باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غبار رزمگهش بر سر سماوات است</p></div>
<div class="m2"><p>شهاب تیزپرش در دل شیاطین باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانه در صف میدان او به توصیف است</p></div>
<div class="m2"><p>ستاره بر در ایوان او به تحسین باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمال او همه روز آفتاب اجلال است</p></div>
<div class="m2"><p>جلال او همه شب آسمان تمکین باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخ محب وی از جام باده گلگون است</p></div>
<div class="m2"><p>کنار خصم وی از خون دیده رنگین باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه دعای فروغی به دولت شاه است</p></div>
<div class="m2"><p>همیشه ورد زبان فرشته آمین باد</p></div></div>