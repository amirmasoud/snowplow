---
title: >-
    غزل شمارهٔ ۱۸۳
---
# غزل شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>چشم مستش نه همین غارت دین و دل کرد</p></div>
<div class="m2"><p>که به یک جرعه مرا بی خود و لایعقل کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم بد دور ازین فتنه که عاقل برخاست</p></div>
<div class="m2"><p>که به یک جلوه مرا از دو جهان غافل کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد به یک تیغم و از زحمت سر فارغ ساخت</p></div>
<div class="m2"><p>رحمتی کرد اگر در حق من قاتل کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به شیرین دهنش دستی اگر خواهد یافت</p></div>
<div class="m2"><p>کام یک عمر به یک بوسه توان حاصل کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه مرا خواهش حور است و نه امید قصور</p></div>
<div class="m2"><p>یاد او آمد و فکر همه را باطل کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم آسان شود از عشق همه مشکل من</p></div>
<div class="m2"><p>آه از این کار که آسان مرا مشکل کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقتی از حالت عشاق خبردار شدم</p></div>
<div class="m2"><p>که مرا عشق تو خون در دل و در پا گل کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این سلاسل که تو داری همه را حیران ساخت</p></div>
<div class="m2"><p>وین شمایل که تو داری همه را مایل کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شبی افتاد به بزم تو فروغی را راه</p></div>
<div class="m2"><p>عشق تا محشرش افسانهٔ هر محفل کرد</p></div></div>