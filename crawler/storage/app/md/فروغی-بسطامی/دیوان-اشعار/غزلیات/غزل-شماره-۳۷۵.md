---
title: >-
    غزل شمارهٔ ۳۷۵
---
# غزل شمارهٔ ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>تا با کمان ابرو بنشست در کمینم</p></div>
<div class="m2"><p>در خون خویش بنشاند از تیر دلنشینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم طره‌اش بهم زد طومار صبر و تابم</p></div>
<div class="m2"><p>هم غمزه‌اش ز جا کند بنیاد عقل و دینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهی به دل کند جا، گاهی به دیده ما را</p></div>
<div class="m2"><p>یک جا نمی‌نشیند شاه حشم نشینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گوشه اهل رازی دارد بدو نیازی</p></div>
<div class="m2"><p>در راه عشق بازی تنها نه من چنینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خرمن جمالی، من خوشه‌چین مسکین</p></div>
<div class="m2"><p>تو خواجه بزرگی، من بنده کمینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو پادشاه حسنی، من دادخواه عشقم</p></div>
<div class="m2"><p>تو فتنهٔ زمانی، من شورش زمینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاری که از تو آید بهتر ز تو ستانم</p></div>
<div class="m2"><p>بویی که از تو باشد خوش تر ز یاسمینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست از تو بر ندارم گر می‌کشی به دارم</p></div>
<div class="m2"><p>مهر از تو برنگیرم گر می‌کشی به کینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روزی اگر ببینم خود را بر آستانت</p></div>
<div class="m2"><p>دیگر کسی نبیند جان را در آستینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن دم که بر لب آید جانم ز زهر هجران</p></div>
<div class="m2"><p>از لعل نوشخندت مشتاق انگبینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر آسمان خوبی دارم مهی فروغی</p></div>
<div class="m2"><p>کز سجده زمینش مهری است بر جبینم</p></div></div>