---
title: >-
    غزل شمارهٔ ۲۷۸
---
# غزل شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>دلا مقید آن گیسوان پرچین باش</p></div>
<div class="m2"><p>در این دو سلسله خاقان چین و ماچین باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام خواجه عنبرفروش نتوان شد</p></div>
<div class="m2"><p>اسیر حلقهٔ آن چین زلف مشکین باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شاهدان شکرخنده در حدیث آیند</p></div>
<div class="m2"><p>تو در مشاهده آن دهان نوشین باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به شربت شمشیر او سری داری</p></div>
<div class="m2"><p>حریف ضربت آن بازوان سیمین باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده به شیوهٔ فرهاد جان به شیرینی</p></div>
<div class="m2"><p>مرید پستهٔ شکرفشان شیرین باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی ز روی عرقناک او سخن سر کن</p></div>
<div class="m2"><p>پی شکستن بازار ماه و پروین باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببین خرابی دوران چرخ مینا رنگ</p></div>
<div class="m2"><p>تو هم خراب ز جام شراب رنگین باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا ز سینه برون رفتی ای کبوتر دل</p></div>
<div class="m2"><p>کنون ز طرهٔ او زیر چنگ شاهین باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نگار ساده اگر پیکرت به خون بکشد</p></div>
<div class="m2"><p>رهین منت سرپنجهٔ نگارین باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر ز مسکنت اورنگ سلطنت خواهی</p></div>
<div class="m2"><p>بر آستانهٔ سلطان عشق مسکین باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر از مقام مقیمان سدره بی‌خبری</p></div>
<div class="m2"><p>مقیم بارگه شاه ناصرالدین باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز فر طلعت او آفتاب تابان شو</p></div>
<div class="m2"><p>ز قرب حضرت او آسمان تمکین باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گهی ز دولت او مستحق احسان شو</p></div>
<div class="m2"><p>گهی ز خدمت او مستعد تحسین باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شها فروغی شاعر مدیح گستر تست</p></div>
<div class="m2"><p>گهی مراقب مدحت شعار دیرین باش</p></div></div>