---
title: >-
    غزل شمارهٔ ۴۴۰
---
# غزل شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>آهی که رخنه کردم از وی به سنگ خاره</p></div>
<div class="m2"><p>عاجز شد از دل دوست یارب دگر چه چاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیداریَم چه دانی، ای خفته‌، ای که شب‌ها</p></div>
<div class="m2"><p>ننشسته‌ای به حسرت، نشمرده‌ای ستاره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانان اگر نشیند یک بار در کنارم</p></div>
<div class="m2"><p>یک باره می‌توانم کردن ز جان کناره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم به شحنه نالم از چشم او ولیکن</p></div>
<div class="m2"><p>پروا ز کس ندارد مست شراب خواره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تاب داده گیسو حالی است بر دل من</p></div>
<div class="m2"><p>از تاب بی حسابت وز پیچ بی شماره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشفتگان عشقت گیرم که جمع گردند</p></div>
<div class="m2"><p>جمع از کجا توان کرد دلهای پاره پاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای شه سوار چالاک احوال ما چه دانی</p></div>
<div class="m2"><p>کز حالت پیاده غافل بود سواره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با این سپاه مژگان از خانه گر درآیی</p></div>
<div class="m2"><p>تسخیر می‌توان کرد شهری به یک اشاره</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از لعل و چشمت آخر دیدی که شد فروغی</p></div>
<div class="m2"><p>ممنون به یک تبسم، قانع به یک نظاره</p></div></div>