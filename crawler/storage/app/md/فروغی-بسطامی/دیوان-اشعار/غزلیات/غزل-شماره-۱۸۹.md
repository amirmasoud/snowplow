---
title: >-
    غزل شمارهٔ ۱۸۹
---
# غزل شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>ای خوشا رندی که رو در ساحت می‌خانه کرد</p></div>
<div class="m2"><p>چارهٔ دور فلک از گردش پیمانه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سال ها کردم به صافی خدمت میخانه را</p></div>
<div class="m2"><p>تا می صاف محبت در وجودم خانه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانهٔ تسبیح ما را حالتی هرگز نداد</p></div>
<div class="m2"><p>بعد از این در پای خم، انگور باید دانه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم آن چشم سیه کز یک نگاه آشنا</p></div>
<div class="m2"><p>مردم آگاه را از خویشتن بیگانه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمهٔ خورشید رویش چشم را بی تاب ساخت</p></div>
<div class="m2"><p>حلقهٔ زنجیر مویش عقل را دیوانه کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که در افسون گری افسانه‌ام در روزگار</p></div>
<div class="m2"><p>نرگس افسون گر ساقی مرا افسانه کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن آن گنج شادی را نیاوردم به دست</p></div>
<div class="m2"><p>سیل غم بیهوده یکسر خانه‌ای ویرانه کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر حق را بر سر دار فنا کرد آشکار</p></div>
<div class="m2"><p>در طلب منصور الحق همت مردانه کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن چه با جان فروغی کرد حسن روی دوست</p></div>
<div class="m2"><p>کی فروغی شمع با آتش به جان پروانه کرد</p></div></div>