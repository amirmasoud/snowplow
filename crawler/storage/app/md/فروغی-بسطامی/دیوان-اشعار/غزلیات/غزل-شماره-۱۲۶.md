---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>پیشتر زآن که مهی جلوه در این محفل داشت</p></div>
<div class="m2"><p>مهرهٔ مهر تو در حقهٔ دل منزل داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من همین از نظر افتاده چشمت بودم</p></div>
<div class="m2"><p>ور نه صد مساله با مردم صاحب دل داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش با سرو حدیث غم خود می‌گفتم</p></div>
<div class="m2"><p>کاو هم از قد تو خون در دل و پا در گل داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون بهای دلم از چشم تو نتوانم خواست</p></div>
<div class="m2"><p>که به یک غمزهٔ دو صد غرقه به خون بسمل داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر تنی در طلبت لایق جان دادن نیست</p></div>
<div class="m2"><p>نیک بخت آن که تن پاک و دل قابل داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خال هندوی تو بر آتش عارض شب و روز</p></div>
<div class="m2"><p>پی احضار دل سوختگان فلفل داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره عشق مرا حسرت مقتولی کشت</p></div>
<div class="m2"><p>که نگاهی گه کشتن به رخ قاتل داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساخت فارغ ز غم رفته و آینده مرا</p></div>
<div class="m2"><p>وه که ساقی خبر از ماضی و مستقبل داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با همه ناخوشی عشق فروغی خوش بود</p></div>
<div class="m2"><p>شادکام آن که غم روی ترا حاصل داشت</p></div></div>