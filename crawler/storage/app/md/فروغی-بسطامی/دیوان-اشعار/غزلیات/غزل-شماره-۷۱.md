---
title: >-
    غزل شمارهٔ ۷۱
---
# غزل شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>تا خانهٔ تقدیر بساط چمن آراست</p></div>
<div class="m2"><p>نشنید کس از سروقدان یک سخن راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا گذری اشک من از دیده پدیدار</p></div>
<div class="m2"><p>هر سو نگری روی وی از پرده هویداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماییم و جهانی که نه بیم است و نه امید</p></div>
<div class="m2"><p>ماییم و نگاری که نه زیر است و نه بالاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماییم و نشاطی که نه پیدا و نه پنهان</p></div>
<div class="m2"><p>ماییم و بساطی که نه جام است و نه میناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در پردهٔ تحقیق نه نور است و نه ظلمت</p></div>
<div class="m2"><p>در عالم توحید نه امروز و نه فرداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دیر و حرم نور رخش جلوه کنان است</p></div>
<div class="m2"><p>نازم صنمی را که هم این جا و هم آنجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم من دل سوخته سرچشمهٔ خون شد</p></div>
<div class="m2"><p>کاش آن رخ رخشنده نه می‌دید و نه می‌خواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم با سگ کوی تو شهان را دل الفت</p></div>
<div class="m2"><p>هم با خم موی تو جهان را سر سوداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم شیفتهٔ حسن تو صد واله بی دل</p></div>
<div class="m2"><p>هم سوختهٔ عشق تو صد عاشق شیداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم نسخهٔ لطف از تن سیمین تو ظاهر</p></div>
<div class="m2"><p>هم آیت جور از دل سنگین تو پیداست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>المنة لله که همه بزم فروغی</p></div>
<div class="m2"><p>دل‌بند و دل‌آویز و دل‌آرام و دل‌آراست</p></div></div>