---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>کسی پا به کوی وفا می‌گذارد</p></div>
<div class="m2"><p>که اول سری زیر پا می‌گذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبی تشنه لب داردم چون سکندر</p></div>
<div class="m2"><p>که منت بر آب بقا می‌گذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی باید از خویش بیگانه گردد</p></div>
<div class="m2"><p>که رو بر در آشنا می‌گذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سری کی شود قابل پای قاتل</p></div>
<div class="m2"><p>که از تیغ رو به قفا می‌گذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی می‌زند چنگ بر تار مویش</p></div>
<div class="m2"><p>که سر بر سر این هوا می‌گذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا کام حاصل شود رهروی را</p></div>
<div class="m2"><p>که کام از پی مدعا می‌گذارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا می‌توان بست کار کسی را</p></div>
<div class="m2"><p>که اسباب کامش خدا می‌گذارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل آخر ز دست غمش می‌گریزد</p></div>
<div class="m2"><p>مرا در میان بلا می‌گذارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز کویش به جای دگر می‌رود دل</p></div>
<div class="m2"><p>ولی هر چه دارد به جا می‌گذارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دو تا کرده قد مرا نازنینی</p></div>
<div class="m2"><p>که بر چهرهٔ زلف دوتا می‌گذارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دعای مرا بی اثر خواست ماهی</p></div>
<div class="m2"><p>که تاثیر در هر دعا می‌گذارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فتاده‌ست کارم به رعنا طبیبی</p></div>
<div class="m2"><p>که هر درد را بی‌دوا می‌گذارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سزد گر ببوسد لبت را فروغی</p></div>
<div class="m2"><p>که در بزم سلطان ثنا می‌گذارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عدو بند غازی ملک ناصرالدین</p></div>
<div class="m2"><p>که گردون به حکمش قضا می‌گذارد</p></div></div>