---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>یار بی پرده کمر بست به رسوایی ما</p></div>
<div class="m2"><p>ما تماشایی او ، خلق تماشایی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامت افروخته می‌رفت و به شوخی می‌گفت</p></div>
<div class="m2"><p>که بتی چهره نیفروخت به زیبایی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او ز ما فارغ و ما طالب او در همه حال</p></div>
<div class="m2"><p>خود پسندیدن او بنگر و خودرایی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قتل خود را به دم تیغ محبت دیدیم</p></div>
<div class="m2"><p>گو عدو کور شو از حسرت بینایی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان بیاسود به یک ضربت قاتل ما را</p></div>
<div class="m2"><p>یعنی از عمر همین بود تن آسایی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حالیا مست و خرابیم ز کیفیت عشق</p></div>
<div class="m2"><p>پس از این تا چه رسد بر سر سودایی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا جام می‌آن کودک خندان بخشد</p></div>
<div class="m2"><p>باده گو پاک بشو دفتر دانایی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد دنیا به بهای لب ساقی دادیم</p></div>
<div class="m2"><p>تا کجا صرف شود مایهٔ عقبایی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب ما تا به قیامت نشود روز، که هست</p></div>
<div class="m2"><p>پردهٔ روز قیامت شب تنهایی ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگرش زلف تو زنجیر نماید ور نه</p></div>
<div class="m2"><p>در همه شهر نگنجد دل صحرایی ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل ز وصلت نتوان کند، بهل تا بکند</p></div>
<div class="m2"><p>سیل هجران تو بنیاد شکیبایی ما</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناتوان چشم تو بر بست فروغی را دست</p></div>
<div class="m2"><p>ورنه کی خاسته مردی به توانایی ما</p></div></div>