---
title: >-
    غزل شمارهٔ ۳۰۱
---
# غزل شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>کمتر فکن به چاه زنخدان نگاه خویش</p></div>
<div class="m2"><p>ترسم خدای ناکرده درافتی به چاه خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در محبت تو بریزند خون من</p></div>
<div class="m2"><p>خود روز رستخیز شوم عذرخواه خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز اگر به جرم وفا می‌کشی مرا</p></div>
<div class="m2"><p>فردای حشر معترفم بر گناه خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اکنون که خاک راه تو شد جان پاک من</p></div>
<div class="m2"><p>زنهار پا مکش ز سر خاک راه خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگشته‌ام ز کوی تو تا یک جهان امید</p></div>
<div class="m2"><p>نومید کس مباد ز امیدگاه خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی اگر در آینه افتد نگاه تو</p></div>
<div class="m2"><p>مفتون شوی ز فتنهٔ چشم سیاه خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خاک غیر نرگس بیمار بر نخاست</p></div>
<div class="m2"><p>تا چشم ما گریست به حال تباه خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درمانده‌ام به عالم عشقش ز بی کسی</p></div>
<div class="m2"><p>آه ار نگیردم غم او در تباه خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نازد به خیل غمزه بت نازنین من</p></div>
<div class="m2"><p>چون خسروی که ناز کند بر سپاه خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با جور او بساز فروغی که اهل دل</p></div>
<div class="m2"><p>جایی نمی‌برند شکایت ز شاه خویش</p></div></div>