---
title: >-
    غزل شمارهٔ ۴۶۷
---
# غزل شمارهٔ ۴۶۷

<div class="b" id="bn1"><div class="m1"><p>گه جلوه‌گر ز بام و گه از منظر آمدی</p></div>
<div class="m2"><p>از هر دری به غارت دلها درآمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل نیابد از تو خلاصی به هیچ راه</p></div>
<div class="m2"><p>گه راهزن شدی و گهی رهبر آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلهای مرده زندگی از سر گرفته‌اند</p></div>
<div class="m2"><p>تا چون مسیح با لب جان پرور آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیراهن حیای زلیخا دریده شد</p></div>
<div class="m2"><p>تا در لباس یوسف پیغمبر آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ایمن ز خیل فتنه نشد هیچ کشوری</p></div>
<div class="m2"><p>تا با سپاه غمزه به هر کشور آمدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با صد جهان نیاز تو را بر در آمدم</p></div>
<div class="m2"><p>تا با هزار ناز مرا در بر آمدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیرت کشید رشته جان را ز پیکرم</p></div>
<div class="m2"><p>تا هم چون جان رفته به هر پیکر آمدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا اهل دل به آمدنت جان فدا کنند</p></div>
<div class="m2"><p>نیکو ز بزم رفتی و نیکوتر آمدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان رو به خدمت تو کمر بسته آفتاب</p></div>
<div class="m2"><p>کز جان غلام شاه فریدون فر آمدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاج الملوک ناصردین شه که آسمان</p></div>
<div class="m2"><p>می‌گویدش که بر همه شاهان سرآمدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاها کنون نیاز فروغی قبول کن</p></div>
<div class="m2"><p>کز فر بخت نازش هفت اختر آمدی</p></div></div>