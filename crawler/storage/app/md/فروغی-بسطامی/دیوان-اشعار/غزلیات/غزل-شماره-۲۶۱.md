---
title: >-
    غزل شمارهٔ ۲۶۱
---
# غزل شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>پیش من کام رقیب از لعل خندان می‌دهد</p></div>
<div class="m2"><p>از یکی جان می‌ستاند بر یکی جان می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌گشاید تا ز هم چشمان خواب آلوده را</p></div>
<div class="m2"><p>هر طرف بر قتل من از غمزه فرمان می‌دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کشد عشقم به میدانی که جان خسته را</p></div>
<div class="m2"><p>زخم مرهم می‌گذارد، درد درمان می‌دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوابم از غیرت نمی‌آید مگر امشب کسی</p></div>
<div class="m2"><p>دل به دلبر می‌سپارد جان به جانان می‌دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چنین چشم ترم خون‌آب دل خواهد فشاند</p></div>
<div class="m2"><p>خانهٔ همسایه را یک سر به توفان می‌دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که دست چرخ را می‌پیچم از نیروی عشق</p></div>
<div class="m2"><p>هر دمم صد پیچ و تاب آن زلف پیچان می‌دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب آن موی مسلسل را پریشانی مباد</p></div>
<div class="m2"><p>زان که گاهی کام دلهای پریشان می‌دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وای بر حال گرفتاری که دست روزگار</p></div>
<div class="m2"><p>دست او می‌گیرد و بر دست هجران می‌دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که می‌بوسد لب ساقی به حکم می فروش</p></div>
<div class="m2"><p>نسبت می را کجا با آب حیوان می‌دهد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک جهان جان در بهای بوسه می‌خواهد لبش</p></div>
<div class="m2"><p>گوهر ارزنده‌اش را سخت ارزان می‌دهد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا فروغی گفتگو زان شکرین لب می‌کند</p></div>
<div class="m2"><p>گفتهٔ خود را به سلطان سخن‌دان می‌دهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناصرالدین شاه غازی آن که در میدان جنگ</p></div>
<div class="m2"><p>نطق گوهربار او خجلت به مرجان می‌دهد</p></div></div>