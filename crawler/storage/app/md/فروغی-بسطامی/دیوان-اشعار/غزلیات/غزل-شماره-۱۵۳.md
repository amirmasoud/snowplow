---
title: >-
    غزل شمارهٔ ۱۵۳
---
# غزل شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>لعل تو به سر چشمهٔ زمزم نتوان داد</p></div>
<div class="m2"><p>این مهر خدا داده به خاتم نتوان داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشاق تو را زجر پیاپی نتوان کرد</p></div>
<div class="m2"><p>مستان تو را جام دمادم نتوان داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر چشم تو نتوان نظر از عین هوس کرد</p></div>
<div class="m2"><p>آهوی حرم را به خطا رم نتوان داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس خم ابروی تو را دید به دل گفت</p></div>
<div class="m2"><p>در هیچ کمانی به از این خم نتوان داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقد دل و دین بر سر سودای تو دادیم</p></div>
<div class="m2"><p>جنسی است محبت که جوی کم نتوان داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماییم و جهانی که به خاطر نتوان گفت</p></div>
<div class="m2"><p>ماییم و پیامی که به محرم نتوان داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سری که میان من و میگون لب ساقی است</p></div>
<div class="m2"><p>کیفیت آن را به دو عالم نتوان داد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانان مرا بار خدا داده ز رحمت</p></div>
<div class="m2"><p>جسمی که به صد جان مکرم نتوان داد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن معجزه کز لعل تو دیده‌ست فروغی</p></div>
<div class="m2"><p>هرگز به دم عیسی مریم نتوان داد</p></div></div>