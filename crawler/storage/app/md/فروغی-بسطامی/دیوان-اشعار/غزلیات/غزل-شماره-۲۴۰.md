---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>چون دم تیغ تو قصد جان ستانی می‌کند</p></div>
<div class="m2"><p>بار سر بر دوش جانان زان گرانی می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم بیمار تو را نازم که با صاحب دلان</p></div>
<div class="m2"><p>دعوی زورآوری در ناتوانی می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من غلام آن نظربازم که با منظور خود</p></div>
<div class="m2"><p>شرح حال خویش را در بی زبانی میکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حالتی در باغ او دارم که با من هر سحر</p></div>
<div class="m2"><p>بلبل دستان‌سرا هم داستانی می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ننالد مرغ مسکینی که او را داده‌اند</p></div>
<div class="m2"><p>دامن باغی که گل چین باغبانی می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من کجا و بزم آن شاهنشه اقلیم حسن</p></div>
<div class="m2"><p>صعوه با شهباز کی هم آشیانی می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نه باد صبح دم در گلشن او جسته راه</p></div>
<div class="m2"><p>برق آهم پس چرا آتش فشانی می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقیا می ده که آخر گنبد نیلوفری</p></div>
<div class="m2"><p>ارغوانی رنگ ما را زعفرانی می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عافیت خواهی زمین بوس در می‌خانه باش</p></div>
<div class="m2"><p>زان که می دفع بلای آسمانی می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رهروی از کعبه مقصود می‌جوید نشان</p></div>
<div class="m2"><p>کاو وطن در کوی بی نام و نشانی می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشق صادق فروغی بر سر سودای عشق</p></div>
<div class="m2"><p>نقد جان را کی دریغ از یار جانی می‌کند</p></div></div>