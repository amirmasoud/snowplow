---
title: >-
    غزل شمارهٔ ۲۶۸
---
# غزل شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>هر کس که دید روی تو آهی ز جان کشید</p></div>
<div class="m2"><p>هر دل که شد اسیر تو دست از جهان کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر خون که ریختی تو به محشر نشد حساب</p></div>
<div class="m2"><p>پنداشتم حساب تو را می‌توان کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیشب به یاد قد تو از دل کشیده‌ام</p></div>
<div class="m2"><p>آهی که انتقام من از آسمان کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را که چرخ داد به کف سر خط امان</p></div>
<div class="m2"><p>خود را به زیر سایه پیر مغان کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک بارگی خصومت عشاق و بوالهوس</p></div>
<div class="m2"><p>برخاست از میانه چو تیغ از میان کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابروی او که مایهٔ چندین گشایش است</p></div>
<div class="m2"><p>منت خدای را که به قتلم کمان کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسکین کسی که داد ز کف آستین تو</p></div>
<div class="m2"><p>مسکین‌تر آن که پای از آن آستان کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این است اگر تطاول گلچین و باغبان</p></div>
<div class="m2"><p>باید قدم فروغی از این گلستان کشید</p></div></div>