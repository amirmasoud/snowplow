---
title: >-
    غزل شمارهٔ ۳۴۵
---
# غزل شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>به جلوه کاش درآید مه نکوسیرم</p></div>
<div class="m2"><p>که آفتاب نتابد مقابل قمرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کار خلق به یک باره پرده بردارند</p></div>
<div class="m2"><p>اگر ز پرده درآید نگار پرده‌درم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به چشم درستی نظر کند معشوق</p></div>
<div class="m2"><p>من از شکسته سر زلف او شکسته‌ترم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسیده‌ام به مقامی ز فیض درویشی</p></div>
<div class="m2"><p>که از کلاه نمد پادشاه تاجورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به اعتبار من امروز هیچ شاهی نیست</p></div>
<div class="m2"><p>که پیش باده‌فروشان گدای معتبرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار مرتبه بالاترم ز چرخ اما</p></div>
<div class="m2"><p>به کوی میکده کمتر ز خاک رهگذرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخست عهد من این شد به پیر باده‌فروش</p></div>
<div class="m2"><p>که بی شراب کهن ساعتی به سر نبرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آن به خوردن می شاهدم اجازت داد</p></div>
<div class="m2"><p>که گول زاهد مردم فریب را نخورم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را به مستیم ای شیخ هوشمند چه کار</p></div>
<div class="m2"><p>که تو ز شهر دگر، من ز عالم دگرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فروغی از هنر شاعری بسی شادم</p></div>
<div class="m2"><p>که طبع شاه جهان مایل است بر هنرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدایگان سخن سنج ناصرالدین شاه</p></div>
<div class="m2"><p>که در مدایح ذاتش محیط پرگهرم</p></div></div>