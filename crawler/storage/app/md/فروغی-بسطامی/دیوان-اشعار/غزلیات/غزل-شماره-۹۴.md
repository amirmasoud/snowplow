---
title: >-
    غزل شمارهٔ ۹۴
---
# غزل شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>مرا زمانه در آن آستانه جا داده‌ست</p></div>
<div class="m2"><p>چنین مقام کسی را بگو کجا داده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشم به آه دل خسته خاصه در دل شب</p></div>
<div class="m2"><p>که این معامله را هم به آشنا داده‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مست گردش پیمانه‌اش چه می‌دانی</p></div>
<div class="m2"><p>که دور نرگس ساقی به ما چه‌ها داده‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون من صنمی پنجه را نگارین ساخت</p></div>
<div class="m2"><p>که کشته را ز لب لعل خون بها داده‌است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان ز درد به جان آمدم که از رحمت</p></div>
<div class="m2"><p>طبیب عشق به من مژدهٔ دوا داده‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به تشنه کامی خود خوش دلم که خضر خطش</p></div>
<div class="m2"><p>مرا نوید به سر چشمهٔ بقا داده‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خون خویش تپیدیم و سخت خرسندیم</p></div>
<div class="m2"><p>که آن دو لعل گواهی به خون ما داده‌ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر نداشت مگر از جراحت دل ما</p></div>
<div class="m2"><p>که زلف مشک فشان بر کف صبا داده‌ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خراش سینهٔ صاحب‌دلان فزون‌تر شد</p></div>
<div class="m2"><p>تراش خط مگر آن چهره را صفا داده‌ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمال حسن به یوسف رسید روز ازل</p></div>
<div class="m2"><p>جمال وجه حسن دولت خدا داده‌ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهی نشانده به روز سیه فروغی را</p></div>
<div class="m2"><p>که آفتاب فروزنده را ضیا داده‌ست</p></div></div>