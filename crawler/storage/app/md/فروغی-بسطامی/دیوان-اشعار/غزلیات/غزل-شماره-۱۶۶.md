---
title: >-
    غزل شمارهٔ ۱۶۶
---
# غزل شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>کسی ز فتنهٔ آخر زمان خبر دارد</p></div>
<div class="m2"><p>که زلف و کاکل و چشم تو در نظر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دیده از رخ خوب تو می‌توان برداشت</p></div>
<div class="m2"><p>نه آه سوختگان در دلت اثر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دل از طره خم برخمت توان برکند</p></div>
<div class="m2"><p>نه شام تیره هجران ز پی سحر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سحر نرگس جادوی تو عیانم شد</p></div>
<div class="m2"><p>که فتنه‌های نهانی به زیر سر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار نشه فزون دیده‌ام ز هر چشمی</p></div>
<div class="m2"><p>ولی نگاه تو کیفیت دگر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز ابروان تو پیوسته می‌تپد دل من</p></div>
<div class="m2"><p>که از مژه به کمان تیر کارگر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث سوختگانت به لاله باید گفت</p></div>
<div class="m2"><p>کز آتش ستمت داغ بر جگر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سری به عالم عشقت قدم تواند زد</p></div>
<div class="m2"><p>که پیش تیغ بلا سینه را سپر دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برغم غیر مکش دم به دم فروغی را</p></div>
<div class="m2"><p>که مهرت از همه آفاق بیشتر دارد</p></div></div>