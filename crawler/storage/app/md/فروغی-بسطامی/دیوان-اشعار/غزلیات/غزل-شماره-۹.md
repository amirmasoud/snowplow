---
title: >-
    غزل شمارهٔ ۹
---
# غزل شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>کی رفته‌ای ز دل که تمنا کنم تو را</p></div>
<div class="m2"><p>کی بوده‌ای نهفته که پیدا کنم تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیبت نکرده‌ای که شوم طالب حضور</p></div>
<div class="m2"><p>پنهان نگشته‌ای که هویدا کنم تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با صد هزار جلوه برون آمدی که من</p></div>
<div class="m2"><p>با صد هزار دیده تماشا کنم تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمم به صد مجاهده آیینه‌ساز شد</p></div>
<div class="m2"><p>تا من به یک مشاهده شیدا کنم تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالای خود در آینهٔ چشم من ببین</p></div>
<div class="m2"><p>تا با خبر زعالم بالا کنم تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مستانه کاش در حرم و دیر بگذری</p></div>
<div class="m2"><p>تا قبله‌گاه مؤمن و ترسا کنم تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهم شبی نقاب ز رویت بر افکنم</p></div>
<div class="m2"><p>خورشید کعبه، ماه کلیسا کنم تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر افتد آن دو زلف چلیپا به چنگ من</p></div>
<div class="m2"><p>چندین هزار سلسله در پا کنم تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوبی و سدره گر به قیامت به من دهند</p></div>
<div class="m2"><p>یک‌جا فدای قامت رعنا کنم تو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیبا شود به کارگه عشق کار من</p></div>
<div class="m2"><p>هر گه نظر به صورت زیبا کنم تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسوای عالمی شدم از شور عاشقی</p></div>
<div class="m2"><p>ترسم خدا نخواسته رسوا کنم تو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با خیل غمزه گر به وثاقم گذر کنی</p></div>
<div class="m2"><p>میر سپاه شاه صف‌آرا کنم تو را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جم دستگاه ناصردین شاه تاجور</p></div>
<div class="m2"><p>کز خدمتش سکندر و دارا کنم تو را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شعرت ز نام شاه، فروغی شرف گرفت</p></div>
<div class="m2"><p>زیبد که تاج تارک شعرا کنم تو را</p></div></div>