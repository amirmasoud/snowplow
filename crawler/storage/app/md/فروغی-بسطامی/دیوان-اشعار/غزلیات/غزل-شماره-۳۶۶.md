---
title: >-
    غزل شمارهٔ ۳۶۶
---
# غزل شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>چون ترک تیر افکن تویی، باید به خون غلطیدنم</p></div>
<div class="m2"><p>یارب کز این میدان مباد امکان برگردیدنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خنجر مردافکنت از هم ببرد خنجرم</p></div>
<div class="m2"><p>کی می‌توان از دامنت دست طمع ببریدنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امروز دادم را بده، امشب به فریادم برس</p></div>
<div class="m2"><p>زیرا که فردای جزا مشکل توانی دیدنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آب و در آتش مرا تو می‌دهی جنبش مرا</p></div>
<div class="m2"><p>ور نه کجا ممکن شود از جای خود جنبیدنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا در غمت گریان شدم هم شاد و هم خندان شدم</p></div>
<div class="m2"><p>این گریهٔ مستانه شد سرمایهٔ خندیدنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا پسته‌ات را دیده‌ام حرف کسی نشنیده‌ام</p></div>
<div class="m2"><p>یعنی سراسر بسته شد گوش سخن بشنیدنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خیمه زد گل در چمن حسرت نصیبی کو چو من</p></div>
<div class="m2"><p>نه بهره از شاخ سمن، نه قسمت از گل چیدنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیدادگر صیاد من نشنید چندان داد من</p></div>
<div class="m2"><p>تا خود برفت از یاد من کیفیت نالیدنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من طایر آزاده‌ام در دام خاک افتاده‌ام</p></div>
<div class="m2"><p>باید که بر بام فلک زین خاک دان پریدنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم ز شوق بوسه‌ات تا کی رسد جانم به لب</p></div>
<div class="m2"><p>گفتا بسی جان بر لب است از خواهش بوسیدنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا شد فروغی طبع من مدحت گر شاه زمن</p></div>
<div class="m2"><p>شد شهره در هر انجمن وضع ثنا سنجیدنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه ناصرالدین کز کرم وقتی که می‌بخشد درم</p></div>
<div class="m2"><p>گوید به معدن شد ستم از دست زر بخشیدنم</p></div></div>