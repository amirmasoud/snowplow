---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>هر سر موی تو را پیوندی از گیسوی تست</p></div>
<div class="m2"><p>حلقه‌ها در حلق من از حلقه‌های موی تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای مقصودم به هر راهی که پوید راه عشق</p></div>
<div class="m2"><p>روی امیدم به هر سویی که باشد سوی تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه‌پرداز سلامت عشق جان‌فرسای ماست</p></div>
<div class="m2"><p>فتنه‌انگیز قیامت قامت دل‌جوی تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چین زلفت ناف آهو، نافه‌اش خوناب دل</p></div>
<div class="m2"><p>آه از این خونی که اندر گردن آهوی تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی حضورت گر نمازی کرده باشم کافرم</p></div>
<div class="m2"><p>قبله‌ام تا از پی طاعت خم ابروی تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون هلاکم می‌کنی جز کوی خود خاکم مکن</p></div>
<div class="m2"><p>کز ازل مشت گلم مشتاق خاک کوی تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر به روی من در رحمت گشاید دست بخت</p></div>
<div class="m2"><p>گرد آن آیینه می‌گردم که رو بر روی تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که می‌بینی به بویی زندگانی می‌کند</p></div>
<div class="m2"><p>زندگانی کردن صاحب‌دلان از بوی تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اختر برج نحوست طالع منحوس من</p></div>
<div class="m2"><p>مطلع صبح سعادت طلعت نیکوی تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا زدی راه فروغی بر همه معلوم شد</p></div>
<div class="m2"><p>کافت اهل محبت غمزهٔ جادوی تست</p></div></div>