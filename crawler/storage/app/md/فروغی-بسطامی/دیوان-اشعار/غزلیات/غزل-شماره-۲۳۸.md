---
title: >-
    غزل شمارهٔ ۲۳۸
---
# غزل شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>کاشکی ساقی ز لعلش می به جام من کند</p></div>
<div class="m2"><p>چرخ مینا تا سحر گردش به کام من کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به جنت هم نشین با ابلهان باید شدن</p></div>
<div class="m2"><p>کاش دوزخ را خدا یک جا مقام من کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم‌تر از آتش حسرت بباید آتشی</p></div>
<div class="m2"><p>تا علاج سردی سودای خام من کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نریزم دانه‌های اشک رنگین را به خاک</p></div>
<div class="m2"><p>طایر دولت کجا تمکین دام من کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنجه‌ای در پنجهٔ شیر فلک خواهم زدن</p></div>
<div class="m2"><p>گر چنین آهو رمی را بخت رام من کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب آید ز گردون بر سجود بام من</p></div>
<div class="m2"><p>گر چنین تابنده ماهی رو به یاد من کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خیال روی و مویش غرق نور و ظلمتم</p></div>
<div class="m2"><p>کو نظربازی که سیر صبح و شام من کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قامتی دیدم که می‌گوید گه برخاستن</p></div>
<div class="m2"><p>کو قیامت تا تماشای قیام من کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بدان درگاه عالی گام من خواهد رسید</p></div>
<div class="m2"><p>سیرگاهش را فلک در زیر گام من کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر غلام خویشتن خواند مرا سلطان عشق</p></div>
<div class="m2"><p>هر چه سلطان است از این منصب غلام من کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به درویشی برد نام مرا آن شاه حسن</p></div>
<div class="m2"><p>هر خطیبی خطبه در منبر به نام من کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر شهوار شد نظم گهربارم بلی</p></div>
<div class="m2"><p>شاه می‌باید که تحسین کلام من کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ناصرالدین شه که فرماید به شاه اختران</p></div>
<div class="m2"><p>لشکرت باید که تعظیم نظام من کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیگر از مشرق نمی‌تابد فروغی آفتاب</p></div>
<div class="m2"><p>گر نظر بر منظر ماه تمام من کند</p></div></div>