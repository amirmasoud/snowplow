---
title: >-
    غزل شمارهٔ ۴۴۵
---
# غزل شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>امشب ای زلف سیه سخت پریشان شده‌ای</p></div>
<div class="m2"><p>مگر آگه ز دل بی سر و سامان شده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز دست تو به هر حلقه دلی لرزان نیست</p></div>
<div class="m2"><p>پس چرا با همه تاب این همه لرزان شده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم گره بر کمر سرو خرامان زده‌ای</p></div>
<div class="m2"><p>هم زره بر تن خورشید درخشان شده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سواری تو که از شیوهٔ چوگان بازی</p></div>
<div class="m2"><p>بر سر گوی قمر دست به چوگان شده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کسی کام خود از مهرهٔ لعلش نبرد</p></div>
<div class="m2"><p>بر سر گنج ز حسن افعی پیچان شده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرخ حیران شده از دست رسن بازی تو</p></div>
<div class="m2"><p>که چسان بر سر آن چاه زنخدان شده‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل معنی همه زین غصه گریبان چاکند</p></div>
<div class="m2"><p>بس که با صورت او دست و گریبان شده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه به دیر از تو نجات است و نه در کعبه خلاص</p></div>
<div class="m2"><p>طرفه دامی به ره گبر و مسلمان شده‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک سر مو نگرفتند مجانین آرام</p></div>
<div class="m2"><p>تا تو این سلسله را سلسله جنبان شده‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا مگر تازه شود زخم جگرسوختگان</p></div>
<div class="m2"><p>در گذرگاه نسیم از پی جولان شده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دگر دم نزند هیچ کس از نافهٔ چین</p></div>
<div class="m2"><p>در ره باد صبا مشک به دامان شده‌ای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همه شاهان جهان حلقه به گوشند تو را</p></div>
<div class="m2"><p>تا غلام در شاهنشه دوران شده‌ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آفتاب فلک جود ملک ناصردین</p></div>
<div class="m2"><p>که ز خاک قدمش غالیه افشان شده‌ای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر فروغی سخنت عین گهر شد نه عجب</p></div>
<div class="m2"><p>گر ثناگستر سلطان سخندان شده‌ای</p></div></div>