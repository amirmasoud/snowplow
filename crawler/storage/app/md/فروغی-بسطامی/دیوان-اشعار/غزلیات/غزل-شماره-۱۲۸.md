---
title: >-
    غزل شمارهٔ ۱۲۸
---
# غزل شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>دی چو تیر از برم آن ترک کمان دار گذشت</p></div>
<div class="m2"><p>تا خبردار شدم کار دل از کار گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وجودی که نه شب دیده‌ام او را و نه روز</p></div>
<div class="m2"><p>شب و روزم همه در حسرت دیدار گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه نگه بود که دل از کف عشاق ربود</p></div>
<div class="m2"><p>چه بلا بود که بر مردم هشیار گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر صفای می ناب و رخ ساقی این است</p></div>
<div class="m2"><p>کس نیارد ز در خانه خمار گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دلت خون نکند لاله رخی کی دانی</p></div>
<div class="m2"><p>که چه‌ها بر سرم از دیدهٔ خون‌بار گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قامت شاخ گل از بار خجالت خم شد</p></div>
<div class="m2"><p>هر گه آن سرو خرامنده به گلزار گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان رخ آن تازه جوان پیر شدند</p></div>
<div class="m2"><p>وقت آزادی مرغان گرفتار گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طالع خفته‌ام از خواب برآمد وقتی</p></div>
<div class="m2"><p>که به سر وقت من آن دولت بیدار گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من که از سلطنت امکان گذشتن دارم</p></div>
<div class="m2"><p>نتوانم ز گدایی در یار گذشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به یک لحظه دو صد بار کشی در خونم</p></div>
<div class="m2"><p>ممکنم نیست ز عشق تو به یک بار گذشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشقت از چار طرف بست ره چارهٔ ما</p></div>
<div class="m2"><p>که میسر نشود از تو به ناچار گذشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه کنم گر نکنم صبر فروغی در عشق</p></div>
<div class="m2"><p>گر نیارد دلم از صحبت دلدار گذشت</p></div></div>