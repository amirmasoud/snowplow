---
title: >-
    غزل شمارهٔ ۳۹۱
---
# غزل شمارهٔ ۳۹۱

<div class="b" id="bn1"><div class="m1"><p>از بس عرق شرم نشسته‌ست به رویم</p></div>
<div class="m2"><p>محروم ز نظارهٔ آن روی نکویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندی است که سودایی آن غالیه گیسو</p></div>
<div class="m2"><p>عمری است که زنجیری آن سلسله مویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل گمشده بر خاک درش بس که فزون است</p></div>
<div class="m2"><p>ترسم که نشان از دل گم گشته نجویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن ماه پری چهره گر از پرده درآید</p></div>
<div class="m2"><p>مردم همه دانند که دیوانهٔ اویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بزم که رندان خرابات نشینند</p></div>
<div class="m2"><p>نه قابل جامم نه سزاوار سبویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا باد بهار از همه سو بوی گل آرد</p></div>
<div class="m2"><p>من بر سر آنم که به جز باد نبویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دور از لب پر شکر او خون جگر باد</p></div>
<div class="m2"><p>هر باده که ریزند حریفان به گلویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتن نبود قاعده عشق وگرنه</p></div>
<div class="m2"><p>هم نکته طرازم من و هم قافیه گویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این است اگر جلوه معشوق فروغی</p></div>
<div class="m2"><p>در مرحله عشق نشاید که نپویم</p></div></div>