---
title: >-
    غزل شمارهٔ ۳۸۷
---
# غزل شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>تا از دو چشم مستت بیمار و دردمندیم</p></div>
<div class="m2"><p>هم ایمن از بلاییم، هم فارغ از گزندیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی برو ز کویم تا پای رفتنت هست</p></div>
<div class="m2"><p>زین جا کجا توان رفت زیرا که پای‌بندیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از طاق ابروانت وز تار گیسوانت</p></div>
<div class="m2"><p>هم خسته کمانیم، هم بسته کمندیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دعوی محبت هم خوار و هم عزیزم</p></div>
<div class="m2"><p>در عالم مودت هم پست و هم بلندیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او جز ملامت ما بر خود نمی‌پذیرد</p></div>
<div class="m2"><p>ما جز سلامت او بر خود نمی‌پسندیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عین تیرباران چشم از تو برنسبتم</p></div>
<div class="m2"><p>در وقت دادن جان دل از تو برنکندیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقتی نشد که بی دوست بر حال خود نگریم</p></div>
<div class="m2"><p>روزی نشد که در عشق بر کار خود نخندیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گو از کمان مزن تیر کز دل به خون تپیدیم</p></div>
<div class="m2"><p>گو از میان مکش تیغ کز کف سپر فکندیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با قهر و لطف معشوق در عاشقی فروغی</p></div>
<div class="m2"><p>هم چشمه‌سار زهریم، هم کاروان قندیم</p></div></div>