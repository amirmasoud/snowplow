---
title: >-
    غزل شمارهٔ ۴۰۶
---
# غزل شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>یا که دندان طمع را از لب جانان بکن</p></div>
<div class="m2"><p>یا تمام عمر از این حسرت به سختی جان بکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا به رسوایی قدم بگذار در بازار عشق</p></div>
<div class="m2"><p>یا همی چشم از جمال یوسف کنعان بکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا سر هر کوچه‌ای دیوانگی را پیشه‌کن</p></div>
<div class="m2"><p>یا دل از زنجیر آن زلف عبیر افشان بکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا به خاطر دم بدم آشفتگی را راه ده</p></div>
<div class="m2"><p>یا تعلق مو به مو زان طرهٔ پیچان بکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا به زخم سینهٔ فرسوده‌ات آسوده باش</p></div>
<div class="m2"><p>یا ز دل پیکان آن ترک سیه مژگان بکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا سر خار ستم را بر دل خونین نشان</p></div>
<div class="m2"><p>یا سراسر خیمه را از دامن بستان بکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا بیایی بر در می‌خانه تا ممکن شود</p></div>
<div class="m2"><p>یا لوای عیش را از عالم امکان بکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا می گلفام را در ساغر از مینا بریز</p></div>
<div class="m2"><p>یا غم ایام را یک باره از بنیان بکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا چو خضر از روی بینش پای در ظلمت گذار</p></div>
<div class="m2"><p>یا چو اسکندر دل از سرچشمهٔ حیوان بکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا حدیث عقل بشنو یا بیا دیوانه شو</p></div>
<div class="m2"><p>یا چو اسکندر دل از سرچشمهٔ حیوان بکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا فروغی مدح سلطان ناصرالدین ثبت کن</p></div>
<div class="m2"><p>یا نهاد شعر را از صفحه دیوان بکن</p></div></div>