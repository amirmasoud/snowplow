---
title: >-
    غزل شمارهٔ ۱۱۹
---
# غزل شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>بر سر راه تو افتاده سری نیست که نیست</p></div>
<div class="m2"><p>خون عشاق تو در ره‌گذری نیست که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیرت عشق عیان خون مرا خواهد ریخت</p></div>
<div class="m2"><p>که نهان با تو کسی را نظری نیست که نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نه تنها ز سر زلف تو مجنونم و بس</p></div>
<div class="m2"><p>شور آن سلسله در هیچ سری نیست که نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین لاله به دل داغ تو دارد ای گل</p></div>
<div class="m2"><p>داغ سودای رخت بر جگری نیست که نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اثری آه سحر در تو ندارد، فریاد</p></div>
<div class="m2"><p>ور نه آه سحری را اثری نیست که نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیل اشک ار بکند خانهٔ مردم نه عجب</p></div>
<div class="m2"><p>کز غمت گریه کنان چشم تری نیست که نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز شب تیرهٔ ما را که ز پی روزی نیست</p></div>
<div class="m2"><p>پی هر شام سیاهی سحری نیست که نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون خرامی، به قفا از ره رحمت بنگر</p></div>
<div class="m2"><p>کز پی‌ات دیدهٔ حسرت نگری نیست که نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خبر شو اگر از دوست خبر می‌خواهی</p></div>
<div class="m2"><p>زان که در بی خبری‌ها خبری نیست که نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک سر تا نکنی پای منه در ره عشق</p></div>
<div class="m2"><p>که درین وادی حیرت خطری نیست که نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من مسکین نه همین خاک درش می‌بوسم</p></div>
<div class="m2"><p>خاک بوس در او تاجوری نیست که نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قابل بندگی خواجه نگردید افسوس</p></div>
<div class="m2"><p>ور نه در طبع فروغی هنری نیست که نیست</p></div></div>