---
title: >-
    غزل شمارهٔ ۳۷۲
---
# غزل شمارهٔ ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>زان پرده می‌گشاید دل بند نازنینم</p></div>
<div class="m2"><p>تا در نظر نیاید زیبا نگار چینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی به عالم عشق بهر چه بی‌نظیرم</p></div>
<div class="m2"><p>وقتی اگر ببینی معشوق بی‌قرینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم نظر بدوزم تا بی دلم نخوانند</p></div>
<div class="m2"><p>پیشی گرفت عشقش بر عقل پیش بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای خسرو ملاحت در من نظر مپوشان</p></div>
<div class="m2"><p>زیرا که خرمنت را درویش خوشه چینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بالای خود میارا کز پا فتاده عقلم</p></div>
<div class="m2"><p>رخسار خو بپوشان کز دست رفت دینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند آستینت در دست من نیفتاد</p></div>
<div class="m2"><p>لیکن بر آستانت فرسوده شد جبینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا بر درت گذشتم، آسوده از بهشتم</p></div>
<div class="m2"><p>تا با تو دست گشتم، فارغ ز حور عینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بخت خفتهٔ من از خواب ناز خیزد</p></div>
<div class="m2"><p>هم با تو می‌کشم می، هم با تو می‌نشینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون جم مرا فروغی از اهرمن چه پروا</p></div>
<div class="m2"><p>تا اسم اعظم دوست نقش است بر نگینم</p></div></div>