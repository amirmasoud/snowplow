---
title: >-
    غزل شمارهٔ ۵۱۳
---
# غزل شمارهٔ ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>ای سر زلف تو سر رشتهٔ هر سودایی</p></div>
<div class="m2"><p>خاری از سوزن سودای تو در هر پایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رخ و زلف تو در دیر و حرم آشوبی</p></div>
<div class="m2"><p>از خط و خال تو در کون و مکان غوغایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو بالای تو پیرایهٔ هر بستانی</p></div>
<div class="m2"><p>تن زیبای تو آرایش هر دیبایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ نقاش نبسته‌ست چنان تصویری</p></div>
<div class="m2"><p>هیچ بازار ندیده‌ست چنین کالایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل ما و شکن جعد عبیرافشانی</p></div>
<div class="m2"><p>سر ما و قدم سرو سهی بالایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من و شور تو اگر تلخ و اگر شیرینی</p></div>
<div class="m2"><p>من و ذوق تو اگر زهر و اگر حلوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه عشاق جگر خسته به جایی نرسد</p></div>
<div class="m2"><p>که به قد سرو و به‌بر سیم و به دل خارایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعلهٔ شمع رخت بر همه کس روشن کرد</p></div>
<div class="m2"><p>کآتش خرمن پروانهٔ بی‌پروائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به سر زلف تو دستی به جنون خواهم زد</p></div>
<div class="m2"><p>تا بدانند که زنجیر دل شیدایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیره شد مهر و مه از جلوهٔ روی تو مگر</p></div>
<div class="m2"><p>حلقه در گوش مهین خواجهٔ روشن رایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر به کویت نکند جای، فروغی چه کند</p></div>
<div class="m2"><p>که ندارد به جهان خوش تر از اینجا جایی</p></div></div>