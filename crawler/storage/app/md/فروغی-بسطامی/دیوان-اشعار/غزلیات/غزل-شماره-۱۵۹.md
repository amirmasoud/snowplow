---
title: >-
    غزل شمارهٔ ۱۵۹
---
# غزل شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>هر سر که به سودای خط و خال تو افتد</p></div>
<div class="m2"><p>چون سایه همه عمر به دنبال تو افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واقف شده از حال شهیدان تو در حشر</p></div>
<div class="m2"><p>هر دیده که بر نامهٔ اعمال تو افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چشم که بندد نظر از منظر خورشید</p></div>
<div class="m2"><p>چشمی است که بر جلوهٔ تمثال تو افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کار که جز دادن جان چاره ندارد</p></div>
<div class="m2"><p>کاری است که با غمزهٔ قتال تو افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس که خبر شد ز گرفتاری من گفت</p></div>
<div class="m2"><p>بیچاره اسیری که به احوال تو افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مرغ دل ار باخبر از لذت دامی</p></div>
<div class="m2"><p>می‌کوش به حدی که پر و بال تو افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای خواجه گر این است طبیب دل عشاق</p></div>
<div class="m2"><p>مشکل که به فکر دل بدحال تو افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فالی بزن ای دل ز پی دولت وصلش</p></div>
<div class="m2"><p>باشد که خود این قرعه به اقبال تو افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از شعلهٔ آه تو فلک سوخت فروغی</p></div>
<div class="m2"><p>آتش به سراپردهٔ آمال تو افتد</p></div></div>