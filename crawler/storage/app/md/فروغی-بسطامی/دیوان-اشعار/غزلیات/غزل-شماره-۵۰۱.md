---
title: >-
    غزل شمارهٔ ۵۰۱
---
# غزل شمارهٔ ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>گر تو زان تنگ شکر خنده مکرر نکنی</p></div>
<div class="m2"><p>کار را از همه سو تنگ به شکر نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد جان تا ندهی کام تو جانان ندهد</p></div>
<div class="m2"><p>ترک سر تا نکنی، وصل میسر نکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ببینی به خم زلف درازش دل من</p></div>
<div class="m2"><p>یاد سر پنجهٔ شاهین کبوتر نکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ مینا شکند شیشهٔ عمر تو به سنگ</p></div>
<div class="m2"><p>گر ز مینا گل رنگ به ساغر نکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر خمار تو را خشت سر خم نکند</p></div>
<div class="m2"><p>تا گل قالبت از باده مخمر نکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم دارم ز لب لعل تو من ای ساقی</p></div>
<div class="m2"><p>که براتم به لب چشمهٔ کوثر نکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم بی خبری را به دو عالم ندهم</p></div>
<div class="m2"><p>تا مرا با خبر از عالم دیگر نکنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجلس نیست که بنشینی و غوغا نشود</p></div>
<div class="m2"><p>محفلی نیست که برخیزی و محشر نکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه کاشانه پر از عنبر سارا نشود</p></div>
<div class="m2"><p>گر شبی شانه بر آن جعد معنبر نکنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکر کز سلسلهٔ موی تو دیوانگیم</p></div>
<div class="m2"><p>به مقامی نرسیده‌ست که باور نکنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست از دامنت ای ترک نخواهم برداشت</p></div>
<div class="m2"><p>تا به خون ریزی من دست، به خنجر نکنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خون من ریخت دو چشم تو و عین ستم است</p></div>
<div class="m2"><p>دعوی خونم اگر زین دو ستمگر نکنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو بدین لعل گهربار که داری حیف است</p></div>
<div class="m2"><p>که ثنای کف بخشندهٔ داور نکنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آفتاب فلکت سجده فروغی نکند</p></div>
<div class="m2"><p>تا شبی سجدهٔ آن ماه منور نکنی</p></div></div>