---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>مرگ بر بالین وجانان غافل است</p></div>
<div class="m2"><p>جان بدین سختی سپردن مشکل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه‌ام مجروح و زخمم کاری است</p></div>
<div class="m2"><p>حسرتم جانکاه و دردم قاتل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که داند لذت شمشیر دوست</p></div>
<div class="m2"><p>بر هلاک خویشتن مستعجل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شربت مرگ از برای عاشقان</p></div>
<div class="m2"><p>صحت کامل، شفای عاجل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کمند عشق نتوان شد خلاص</p></div>
<div class="m2"><p>جهد من بی جا و سعی‌ام باطل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق طغیانش به حدی شد که جان</p></div>
<div class="m2"><p>در میان ما و جانان حایل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک کوی دوست دامن‌گیر ماست</p></div>
<div class="m2"><p>وین کسی داند که پایش در گل است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کس به مقصد کی رسد از سعی خویش</p></div>
<div class="m2"><p>کوشش ما سر به سر بی‌حاصل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جان نثار مقدمش کردم، بلی</p></div>
<div class="m2"><p>تحفهٔ ناقابلان ناقابل است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاشق آرامی ندارد ورنه یار</p></div>
<div class="m2"><p>مونس جان است و آرام دل است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قاتلی دارم فروغی کز غرور</p></div>
<div class="m2"><p>خود به خون بی‌گناهان قایل است</p></div></div>