---
title: >-
    غزل شمارهٔ ۲۸۵
---
# غزل شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>تویی آن آیت رحمت که نتوان کرد تفسیرش</p></div>
<div class="m2"><p>منم آن مایهٔ حسرت که نتوان داد تغییرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو و زلف گره گیری نتوان دید در چنگش</p></div>
<div class="m2"><p>من و خواب پریشانی که نتوان کرد تعبیرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تعال‌الله از این صورت که من ماتم ز تحسینش</p></div>
<div class="m2"><p>بنام ایزد از این معنی که من لالم ز تقریرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلارا صورتی دیدم که دل می‌برد دیدارش</p></div>
<div class="m2"><p>به صورت خانه‌ای رفتم که جان می‌داد تصویرش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریفی شد نگار من که شاهانند محتاجش</p></div>
<div class="m2"><p>غزالی شد شکار من که شیرانند نخجیرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلای جان مردم فتنهٔ چشم سیه مستش</p></div>
<div class="m2"><p>گشاد کار عالم حلقهٔ زلف گره گیرش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به قتل عاشقان مایل دل پرورده از کینش</p></div>
<div class="m2"><p>به خون بی دلان شایق لب ناشسته از شیرش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دستی خفته‌ام در خون که تن می‌نازد از تیغش</p></div>
<div class="m2"><p>ز شستی خورده‌ام پیکان که جان می‌رقصد از تیرش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن مجمع که بسرایند ذکر از جعد حورالعین</p></div>
<div class="m2"><p>من و امید گیسویش من و سودای زنجیرش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز دست کافری کی می‌توان دیدن سلامت را</p></div>
<div class="m2"><p>که خون صد مسلمان می‌چکد هر دم ز شمشیرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبی نگذشت کز دست غمش چون نی ننالیدم</p></div>
<div class="m2"><p>دریغ از نالهٔ پنهان که پیدا نیست تاثیرش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مردن هم علاجی نیست رنجور محبت را</p></div>
<div class="m2"><p>فغان زین درد بی‌درمان که درماندم ز تدبیرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر معماری ار داری بیا ای خواجهٔ منعم</p></div>
<div class="m2"><p>که من ویرانه‌ای دارم که ویرانم ز تعمیرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مسخر ساخت نیر تا دل پاک فروغی را</p></div>
<div class="m2"><p>تو پندار که از افسون پری کرده‌ست تسخیرش</p></div></div>