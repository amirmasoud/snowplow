---
title: >-
    غزل شمارهٔ ۳۱۸
---
# غزل شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>چندان به سر کوی خرابات خرابم</p></div>
<div class="m2"><p>کاسوده ز اندیشهٔ فردای حسابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کار تو فضل است چه پر وا ز گناهم</p></div>
<div class="m2"><p>ور شغل تو عدل است چه حاصل ز ثوابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسانه دوزخ همه باد است به گوشم</p></div>
<div class="m2"><p>تا ز آتش هجران تو در عین عذابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه سحر و اشک شبم شاهد حال است</p></div>
<div class="m2"><p>کز عشق رخ و زلف تو در آتش و آبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخجیر نمودم همه شیران جهان را</p></div>
<div class="m2"><p>تا آهوی چشمت سگ خود کرده خطابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر سلسله اهل جنون کرد مرا عشق</p></div>
<div class="m2"><p>تا برده ز دل سلسلهٔ موی تو تابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چشم سیه مست تو تحریک نمی‌کرد</p></div>
<div class="m2"><p>آب مژه بیدار نمی‌ساخت ز خوابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان پیش که دوران شکند کشتی عمرم</p></div>
<div class="m2"><p>ساقی فکند کاش به دریای شرابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر منظر ساقی نظر از شرم نکردم</p></div>
<div class="m2"><p>تا جام شراب آمد و برداشت حجابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم که به شب چشمهٔ خورشید توان دید</p></div>
<div class="m2"><p>گفت ار بگشایند شبی بند نقابم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از تنگی دل هر چه زدم داد فروغی</p></div>
<div class="m2"><p>شکردهنان هیچ ندادند جوابم</p></div></div>