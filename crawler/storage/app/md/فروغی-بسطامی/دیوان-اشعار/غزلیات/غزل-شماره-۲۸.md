---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>به یک پیمانه با ساقی چنان بستیم پیمان را</p></div>
<div class="m2"><p>که تا هستیم بشناسیم از کافر مسلمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کوی می‌فروشان با هزاران عیب خوشنودم</p></div>
<div class="m2"><p>که پوشیده‌ست خاکش عیب هر آلوده دامان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تکبر با گدایان در میخانه کمتر کن</p></div>
<div class="m2"><p>که اینجا مور بر هم می‌زند تخت سلیمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو هم خواهی گریبان چاک زد تا دامن محشر</p></div>
<div class="m2"><p>اگر چون صبح صادق بینی آن چاک گریبان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخواهد جمع شد هرگز پریشان حال مشتاقان</p></div>
<div class="m2"><p>مگر وقتی که سازد جمع آن زلف پریشان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و جان نظر بازان همه بر یکدیگر دوزد</p></div>
<div class="m2"><p>نهد چون در کمان ابروی جانان تیر مژگان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کجا خواهد نهادن پای رحمت بر سر خاکم</p></div>
<div class="m2"><p>کسی کز سرکشی برخاک ریزد خون پاکان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آن شاهد که دیدم من ببیند دیدهٔ زاهد</p></div>
<div class="m2"><p>نخست از سرگذارد مایهٔ سودای رضوان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ار محبوب خود را می‌پرستم، دم مزن واعظ</p></div>
<div class="m2"><p>که از کفر محبت اولیا جستند ایمان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دمی ای کاش ساقی، لعل آن زیبا جوان گردد</p></div>
<div class="m2"><p>که خضر از بی‌خودی بر خاک ریزد آب حیوان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فروغی، زان دلم در تنگنای سینه تنگ آید</p></div>
<div class="m2"><p>که نتوان داشت در کنج قفس مرغ گلستان را</p></div></div>