---
title: >-
    غزل شمارهٔ ۱۲
---
# غزل شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>دوش به خواب دیده‌ام روی ندیدهٔ تو را</p></div>
<div class="m2"><p>وز مژه آب داده‌ام باغ نچیدهٔ تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره خون تازه‌ای از تو رسیده بر دلم</p></div>
<div class="m2"><p>به که به دیده جا دهم تازه رسیدهٔ تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با دل چون کبوترم انس گرفته چشم تو</p></div>
<div class="m2"><p>رام به خود نموده‌ام باز رمیدهٔ تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که به گوش خویشتن از تو شنیده‌ام سخن</p></div>
<div class="m2"><p>چون شنوم ز دیگران حرف شنیدهٔ تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر و کمان عشق را هر که ندیده، گو ببین</p></div>
<div class="m2"><p>پشت خمیده مرا، قد کشیدهٔ تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قامتم از خمیدگی صورت چنگ شد ولی</p></div>
<div class="m2"><p>چنگ نمی‌توان زدن زلف خمیدهٔ تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شام نمی‌شود دگر صبح کسی که هر سحر</p></div>
<div class="m2"><p>زان خم طره بنگرد صبح دمیدهٔ تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسته طرهٔ تو را چاره نکرد لعل تو</p></div>
<div class="m2"><p>مهره نداد خاصیت، مار گزیدهٔ تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که به عشق او زدی خنده به چاک سینه‌ام</p></div>
<div class="m2"><p>شکر خدا که دوختم جیب دریدهٔ تو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست مکش به موی او مات مشو به روی او</p></div>
<div class="m2"><p>تا نکشد به خون دل دامن دیدهٔ تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز فروغی از درت روی طلب کجا برد</p></div>
<div class="m2"><p>زان که کسی نمی‌خرد هیچ خریدهٔ تو را</p></div></div>