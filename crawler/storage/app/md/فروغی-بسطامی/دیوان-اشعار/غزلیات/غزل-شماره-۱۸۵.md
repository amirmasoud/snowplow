---
title: >-
    غزل شمارهٔ ۱۸۵
---
# غزل شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>بیدادگر نگارا تا کی جفا توان کرد</p></div>
<div class="m2"><p>پاداش آن جفاها یک ره وفا توان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه رحمت آورد بر زحمت دل ما</p></div>
<div class="m2"><p>کی آن‌قدر تطاول با آشنا توان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخمور و تشنگانیم زان چشم و لعل میگون</p></div>
<div class="m2"><p>جانی به ما توان داد، کامی روا توان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وقتی به یک اشارت جانی توان خریدن</p></div>
<div class="m2"><p>گاهی به یک تبسم دردی دوا توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک بار اگر بپرسی احوال بی‌نصیبان</p></div>
<div class="m2"><p>با صد هزار حرمان دل را رضا توان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر مدعا که خواهی گر از دعا دهد دست</p></div>
<div class="m2"><p>چندی به سر توان زد عمری دعا توان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر جذبهٔ محبت آتش به دل فروزد</p></div>
<div class="m2"><p>برگ هوس توان سوخت ترک هوا توان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر پیر باده‌خواران گیرد ز لطف دستم</p></div>
<div class="m2"><p>هر سو به کام خاطر عیشی به پا توان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر جرعه‌ای بریزد بر خاک لعل ساقی</p></div>
<div class="m2"><p>خاک سبوکشان را آب بقا توان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر آدمی درآید در عالم خدایی</p></div>
<div class="m2"><p>آدم ز نو توان ساخت عالم بنا توان کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نیم شب بنالی از سوز دل فروغی</p></div>
<div class="m2"><p>راه قضا توان زد، دفع بلا توان کرد</p></div></div>