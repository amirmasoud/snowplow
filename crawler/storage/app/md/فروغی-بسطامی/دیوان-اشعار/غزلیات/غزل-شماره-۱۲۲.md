---
title: >-
    غزل شمارهٔ ۱۲۲
---
# غزل شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>ایمن از تیر نگاه تو دل زاری نیست</p></div>
<div class="m2"><p>مردم آزارتر از چشم تو بیماری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز در فکر اسیران کهن افتادی</p></div>
<div class="m2"><p>به کمند تو مگر تازه گرفتاری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی تواند که به سر تاج سلیمانی زد</p></div>
<div class="m2"><p>هر که از لعل تواش خاتم زنهاری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گز آن دولت بیدار نصیبش نشود</p></div>
<div class="m2"><p>هر که را وقت سحر دیدهٔ بیداری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دامن گوهر مقصود به دستش نفتد</p></div>
<div class="m2"><p>هرکه را در دل شب چشم گهرباری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدمی بیش نمانده‌ست میان من و دوست</p></div>
<div class="m2"><p>لیکن از ضعف مراقوت رفتاری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که گفتی غم دل در بر دلدار بگو</p></div>
<div class="m2"><p>خود چه گویم که مرا قدرت گفتاری نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاشکی بار غمش بر کمر کوه نهند</p></div>
<div class="m2"><p>تا بدانند که سنگین‌تر از این باری نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گاهی از حضرت معشوق نگاهی بکند</p></div>
<div class="m2"><p>خوش تر از مشغلهٔ عشق دگر کاری نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یار از پرده هویدا شد و یاران غافل</p></div>
<div class="m2"><p>یوسفی هست دریغا که خریداری نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اثری در نفس پیر مغان است ار نه</p></div>
<div class="m2"><p>سبحهٔ شیخ کم از حلقه زناری نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از لب ساقی سر مست فروغی ما را</p></div>
<div class="m2"><p>نشه‌ای هست که در خانه خماری نیست</p></div></div>