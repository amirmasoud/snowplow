---
title: >-
    غزل شمارهٔ ۳۴۷
---
# غزل شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>جنون گسسته بدانسان کمند تدبیرم</p></div>
<div class="m2"><p>که از سلاسل تو مستحق زنجیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نور حسن تو چشم و چراغ خورشیدم</p></div>
<div class="m2"><p>ز فر عشق تو فرمانروای تقدیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سحر چشم تو شاهین پنجهٔ شاهم</p></div>
<div class="m2"><p>ز بند زلف تو زنجیر گردن شیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان به جلوه درآمد جمال صورت تو</p></div>
<div class="m2"><p>که از کمال تحیر مثال تصویرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشسته‌ام به سر راه آرزو عمری</p></div>
<div class="m2"><p>که ابروی تو نشاند به زیر شمشیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون که دست تظلم زدم به دامانت</p></div>
<div class="m2"><p>عنان کشیدی و بستی زبان تقریرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز فرق تا قدم از سوز عشق ناله شدم</p></div>
<div class="m2"><p>ولی نبود در آن دل مجال تاثیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحر کمان دعا را به یکدگر شکنم</p></div>
<div class="m2"><p>خدا نکرده گر امشب خطا رود تیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به قاتلی سر و کارم فتاد در مستی</p></div>
<div class="m2"><p>که تیغ می‌کشد و می‌کشد ز تاخیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شراب داد ولیکن نخفت در بزمم</p></div>
<div class="m2"><p>خراب ساخت ولیکن نکرد تعمیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طلای احمر اگر خاک را کنم نه عجب</p></div>
<div class="m2"><p>که من ز تربیت عشق کان اکسیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر که خواجه فروغی ز بنده در گذرد</p></div>
<div class="m2"><p>و گر نه صاحب چندین هزار تقصیرم</p></div></div>