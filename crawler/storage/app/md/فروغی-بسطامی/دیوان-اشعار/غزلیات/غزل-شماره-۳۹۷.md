---
title: >-
    غزل شمارهٔ ۳۹۷
---
# غزل شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>به که باشم بی قرار از زلف یار خویشتن</p></div>
<div class="m2"><p>من که دادم بی قراری را قرار خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردم اظهار محبت پیش از زیبانگار</p></div>
<div class="m2"><p>پرده را برداشتم از روی کار خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز کار افتاد و روزم تیره شد در عاشقی</p></div>
<div class="m2"><p>فکر کار دل کنم یا روزگار خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس که کارم سخت شد از سخت گیریهای عشق</p></div>
<div class="m2"><p>مرگ را آسان گرفتم در کنار خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلبرا گر عاشقی از عاشقت پنهان مکن</p></div>
<div class="m2"><p>راز خود مخفی مدار از رازدار خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گرفتم جز تو دلداری نمودم اختیار</p></div>
<div class="m2"><p>چون نمایم با دل بی‌اختیار خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر امید از طرهٔ عنبرفشانت برکنم</p></div>
<div class="m2"><p>چون کنم با خاطر امیدوار خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ار زدی هر دو عالم را توان بردن به خاک</p></div>
<div class="m2"><p>گر تو را عاشق کند شمع مزار خویشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان فکندستی به محشر وعدهٔ دیدار خویش</p></div>
<div class="m2"><p>تا جهانی را کشی در انتظار خویشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا فروغی با خط مشکین او شد آشنا</p></div>
<div class="m2"><p>مشک می‌بارد ز کلک مشکبار خویشتن</p></div></div>