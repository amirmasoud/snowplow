---
title: >-
    غزل شمارهٔ ۱۵۱
---
# غزل شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>تا سوی من آن چشم سیه را نگه افتاد</p></div>
<div class="m2"><p>از یک نگهش دل به بلایی سیه افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بندهٔ آن خواجه که با مژدهٔ عفوش</p></div>
<div class="m2"><p>هر بنده که بر خواست به فکر گنه افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردید امید دلم از ذوق فراموش</p></div>
<div class="m2"><p>هرگه که مرا دیده به امیدگه افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد بار دل افتاد در آن چاه زنخدان</p></div>
<div class="m2"><p>یک بار اگر یوسف کنعان به چه افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دست جفای تو شکایت نتوان کرد</p></div>
<div class="m2"><p>مسکین چه کند کار چو با پادشه افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل از صف مژگان تو بیرون نبرد جان</p></div>
<div class="m2"><p>مانند شکاری که بر جرگ سپه افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مرحلهٔ عشق تو ای سرو قباپوش</p></div>
<div class="m2"><p>چندان بدویدیم که از سر کله افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز امید نگاهی که به حالش نفکندی</p></div>
<div class="m2"><p>دردا که مریض تو به حال تبه افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنجا که فروغ مه من یافت فروغی</p></div>
<div class="m2"><p>خورشید فروغی است که بر خاک ره افتاد</p></div></div>