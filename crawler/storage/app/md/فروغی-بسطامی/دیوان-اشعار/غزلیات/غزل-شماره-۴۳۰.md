---
title: >-
    غزل شمارهٔ ۴۳۰
---
# غزل شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>از بس که در خیال مکیدم لبان او</p></div>
<div class="m2"><p>یاقوت فام شد لب گوهرفشان او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد وجود من همه مصروف هیچ شد</p></div>
<div class="m2"><p>یعنی نداد کام دلم را دهان او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیرانه‌سر بلاکش ابروی او شدم</p></div>
<div class="m2"><p>با قامت خمیده کشیدم کمان او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاتل چگونه منکر خونم شود به حشر</p></div>
<div class="m2"><p>زخمی نخورده‌ام که نماند نشان او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستی که از رکاب سمندش بریده شد</p></div>
<div class="m2"><p>ترسم خدا نکرده نگیرد عنان او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان که در پیش به درستی دویده‌ام</p></div>
<div class="m2"><p>الا دل شکسته ندیدم مکان او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی پرده در حضور من امشب نشسته است</p></div>
<div class="m2"><p>گر صد هزار بار کنند امتحان او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سودا نگر که بر سر بازار عاشقی</p></div>
<div class="m2"><p>خواهم زیان خویش و نخواهم زیان او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عهد شه کلام فروغی بها گرفت</p></div>
<div class="m2"><p>یارب که در زمانه بماند زمان او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظل الله ناصردین شه که آمده‌ست</p></div>
<div class="m2"><p>چندین هزار آیت رحمت نشان او</p></div></div>