---
title: >-
    غزل شمارهٔ ۴۳۹
---
# غزل شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>چه عقده‌هاست به کار دلم ز بخت سیاه</p></div>
<div class="m2"><p>که زلف دوست بلند است و دست من کوتاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعوذبالله از این زاهدان جامه سفید</p></div>
<div class="m2"><p>تبارک الله از این شاهدان چشم سیاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی ز بند سر زلف او اسیر کمند</p></div>
<div class="m2"><p>یکی ز کنج زنخدان او فتاده به چاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی خراب لب لعل او نخورده شراب</p></div>
<div class="m2"><p>یکی قتیل دم تیغ او نکرده گناه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی ز غمزهٔ خونخواره‌اش تپیده به خون</p></div>
<div class="m2"><p>یکی ز حسرت نظاره‌اش نشسته به راه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی ز جنبش مژگان او به چنگ اجل</p></div>
<div class="m2"><p>یکی ز گردش چشمان او به حال تباه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی به خاک در او فشانده گوهر اشک</p></div>
<div class="m2"><p>یکی به رهگذر او کشیده لشکر آه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هوای مغبچگان آن چنان خرابم کرد</p></div>
<div class="m2"><p>که در سرای مغانم نمی‌دهند پناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دمی به چشم من آن سرو قد نهشت قدم</p></div>
<div class="m2"><p>گهی به حال من آن ماه رو نکرد نگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بپا نموده قیامت ز قامت دلجو</p></div>
<div class="m2"><p>پدید ساخته جنت ز عارض دلخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز رشک قامت او ناله خاست از دل سرو</p></div>
<div class="m2"><p>ز شرم عارض او هاله بست بر رخ ماه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خمیده ابروی آن پادشاه کشور حسن</p></div>
<div class="m2"><p>نمونه‌ای است ز شمشیر ناصرالدین شاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستوده خسرو لشکر شکاف کشور گیر</p></div>
<div class="m2"><p>که نقش رایت منصور اوست نصرالله</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شکسته حملهٔ او پشت صد هزار سوار</p></div>
<div class="m2"><p>دریده صارم او قلب صدهزار سپاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رخ منور او آفتاب کاخ و سپهر</p></div>
<div class="m2"><p>سر مبارک او زیب بخش تاج و کلاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه عاشق دیدار اوست دیدهٔ بخت</p></div>
<div class="m2"><p>مدام شایق بالای اوست جامهٔ جاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فروغی از کرم شاه دستگیر شود</p></div>
<div class="m2"><p>بر آن سرم که عروسی به برکشم دل خواه</p></div></div>