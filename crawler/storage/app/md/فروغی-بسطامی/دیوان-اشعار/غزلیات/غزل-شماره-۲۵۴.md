---
title: >-
    غزل شمارهٔ ۲۵۴
---
# غزل شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>قدح باده اگر چشم بت ساده نبود</p></div>
<div class="m2"><p>این همه مستی خلق از قدح باده نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبب باده ننوشیدن زاهد این است</p></div>
<div class="m2"><p>که سراسر همه اسباب وی آماده نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش در دامن پاک صنم باده‌فروش</p></div>
<div class="m2"><p>اثری بود که در دامن سجاده نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به درها نروی هر سحری کی دانی</p></div>
<div class="m2"><p>که دری غیر در میکده بگشاده نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که دل بردن معشوق بیند داند</p></div>
<div class="m2"><p>که گناه از طرف عاشق دل داده نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز ایجاد نمی‌کد خدا آدم را</p></div>
<div class="m2"><p>عین مقصود گر آن شوخ پری زاده نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاصد ار دوست به سویم نفرستاد خوشم</p></div>
<div class="m2"><p>که میان من و او جای فرستاده نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز محشر به چه امید ز جا بر خیزد</p></div>
<div class="m2"><p>هر سری کز دم شمشیر تو افتاده نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>واقف از داغ دل لاله نخواهد بودن</p></div>
<div class="m2"><p>هر نهادی که در آن داغ تو بنهاده نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با که من قابل قلاده نبودم هرگز</p></div>
<div class="m2"><p>یا سگ کوی تو محتاج به قلاده نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی فروغی ز فلک سر خط آزادی داشت</p></div>
<div class="m2"><p>گر به درگاه ملک بندهٔ آزاده نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتاب فلک جود ملک ناصردین</p></div>
<div class="m2"><p>که به قدر کرمش گوهر بیجاده نبود</p></div></div>