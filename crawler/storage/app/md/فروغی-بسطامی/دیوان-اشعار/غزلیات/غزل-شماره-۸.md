---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>گر باغبان نظر به گلستان کند تو را</p></div>
<div class="m2"><p>بر تخت گل نشاند و سلطان کند تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر صبح‌دم به دامن گلشن گذر کنی</p></div>
<div class="m2"><p>دست نسیم، گل به سرافشان کند تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مشرق هزار پاره کند جیب خویشتن</p></div>
<div class="m2"><p>گر یک نظر به چاک گریبان کند تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای کاش چهرهٔ تو سحر بنگرد سپهر</p></div>
<div class="m2"><p>تا قبله گاه مهر درخشان کند تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور فلک به چشم تو تعلیم سحر داد</p></div>
<div class="m2"><p>تا چشم بند مردم دوران کند تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مار زخم خورده، دل افتد به پیچ و تاب</p></div>
<div class="m2"><p>هرگه که یاد طرهٔ پیچان کند تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هیچ حال خاطر ما از تو جمع نیست</p></div>
<div class="m2"><p>قربان حالتی که پریشان کند تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با هیچ‌کس به کشتن من مشورت مکن</p></div>
<div class="m2"><p>ترسم خدا نکرده، پشیمان کند تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الحق سزد که تربیت خسرو عجم</p></div>
<div class="m2"><p>میر نظام لشکر ایران کند تو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جم احتشام ناصرالدین شه که عون او</p></div>
<div class="m2"><p>هم‌داستان رستم دستان کند تو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داند هلاک جان فروغی به دست کیست</p></div>
<div class="m2"><p>هر کس که سیر نرگس فتان کند تو را</p></div></div>