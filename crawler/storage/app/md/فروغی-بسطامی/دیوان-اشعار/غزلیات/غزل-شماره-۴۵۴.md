---
title: >-
    غزل شمارهٔ ۴۵۴
---
# غزل شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>کسی که دامنش آلودهٔ شرابستی</p></div>
<div class="m2"><p>دعای او به در دیر مستجابستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مستی از لب دردی‌کشی شنیدم دوش</p></div>
<div class="m2"><p>که چاره همه دردی شراب نابستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فغان که پرده ز کارم فکند پنجه عشق</p></div>
<div class="m2"><p>هنوز چهره معشوق در حجابستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نصیبم آن صف مژگان نشد به بیداری</p></div>
<div class="m2"><p>هنوز طالع برگشته‌ام به خوابستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی نظاره بدان شمع انجمن کردم</p></div>
<div class="m2"><p>هنوز ز آتش دل دیده‌ام پرآبستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گریه گفتمش از رخ نقاب یک سو نه</p></div>
<div class="m2"><p>به خنده گفت که خورشید در سحابستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زکانه بوسه زند پای شه سواری را</p></div>
<div class="m2"><p>که با تو از مدد بخت هم‌رکابستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خاک ریخته‌ای خون بی‌گناهان را</p></div>
<div class="m2"><p>مگر به کیش تو خون ریختن ثوابستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشا به حال شهیدی که در صف محشر</p></div>
<div class="m2"><p>به خون ناحق او ناخنت خضابستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حدیث قند نشاید بر دهان تو گفت</p></div>
<div class="m2"><p>که در میانهٔ این هر دو شکر آبستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فروغی از اثر پرتو محبت دوست</p></div>
<div class="m2"><p>کمین تجلی من ماه و آفتابستی</p></div></div>