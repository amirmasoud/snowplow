---
title: >-
    غزل شمارهٔ ۵۰۰
---
# غزل شمارهٔ ۵۰۰

<div class="b" id="bn1"><div class="m1"><p>چون به رخ چین سر زلف چلیپا فکنی</p></div>
<div class="m2"><p>سرم آن بخت ندارد که تو در پا فکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کی بار خم زلف کشی بر سر دوش</p></div>
<div class="m2"><p>کاش برداری و بر گردن دل‌ها فکنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده‌هایی که بدان طرهٔ پرچین زده‌ای</p></div>
<div class="m2"><p>کاش بگشایی و در سنبل رعنا فکنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به هم برفکنی طرهٔ مشک‌افشان را</p></div>
<div class="m2"><p>آتشی در جگر عنبر سارا فکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو زیبا صنم از پرده درآیی روزی</p></div>
<div class="m2"><p>کار خاصان حرم را به کلیسا فکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقتی ار سایهٔ بالای تو بر خاک افتد</p></div>
<div class="m2"><p>خاک را در طلب عالم بالا فکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی امروز دهم کام دل ناکامت</p></div>
<div class="m2"><p>آه اگر وعدهٔ امروز به فردا فکنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو یوسف‌صفت از خانه به بازار آیی</p></div>
<div class="m2"><p>دل شهری همه بر آتش سودا فکنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ ابروی تو را این همه پرداخته‌اند</p></div>
<div class="m2"><p>که سر دشمن دارای صف‌آرا فکنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناصرالدین شه غازی که سپهرش گوید</p></div>
<div class="m2"><p>باش تا روزی زمین گیری و اعدا فکنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چارهٔ آن دل بی‌رحم فروغی نکنی</p></div>
<div class="m2"><p>گر ز آه سحری رخنه به خارا فکنی</p></div></div>