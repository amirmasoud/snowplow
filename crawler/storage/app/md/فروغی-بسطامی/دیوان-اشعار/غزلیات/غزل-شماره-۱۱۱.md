---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>تا بر اطراف رخت جعد چلیپایی هست</p></div>
<div class="m2"><p>هر طرف پای نهی سلسله در پایی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قتل عشاق تو خالی ز تماشایی نیست</p></div>
<div class="m2"><p>وه که از هر طرفت طرفه تماشایی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد کشتن تن صد چاک مرا باید سوخت</p></div>
<div class="m2"><p>که هنوز از تو به دل باز تمنایی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی پی تجربه از کوی تو بیرون رفتم</p></div>
<div class="m2"><p>به گمانی که مرا از تو شکیبایی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان شیرین ز غم عشق به تلخی دارم</p></div>
<div class="m2"><p>به امیدی که تو را لعل شکرخایی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب جان بخش تو گر بوسه به جان بفروشد</p></div>
<div class="m2"><p>من سودا زده را هم سر سودایی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ از سایهٔ طوبی سخنی می‌گوید</p></div>
<div class="m2"><p>غیر قد تو مگر عالم بالایی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من و سودای تو تا دامن صحرا برجاست</p></div>
<div class="m2"><p>من و اندوه تو تا عشق غم افزایی هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که بی‌جرم خوری خون فروغی هر روز</p></div>
<div class="m2"><p>هیچ گویا خبرت نیست که فردایی هست</p></div></div>