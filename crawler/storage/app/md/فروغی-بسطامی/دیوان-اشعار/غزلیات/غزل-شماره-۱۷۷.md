---
title: >-
    غزل شمارهٔ ۱۷۷
---
# غزل شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>مهره توان برد، مار اگر بگذارد</p></div>
<div class="m2"><p>غنچه توان چید، خار اگر بگذارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه حسرت خوشم به گوشهٔ چشمی</p></div>
<div class="m2"><p>چشم بد روزگار اگر بگذارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام توان یافتن ز نرگس مستش</p></div>
<div class="m2"><p>یک نفسم هوشیار اگر بگذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر خوشم از دور جام و گردش ساقی</p></div>
<div class="m2"><p>گردش لیل و نهار اگر بگذارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فصل گل از باده توبه داده مرا شیخ</p></div>
<div class="m2"><p>غیرت باد بهار اگر بگذارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوسه توان زد بر آن دهان شکرخند</p></div>
<div class="m2"><p>گریهٔ بی‌اختیار اگر بگذارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده توانم کشید از آن رخ زیبا</p></div>
<div class="m2"><p>کشمکش پرده‌دار اگر بگذارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر سر آنم که در کمند نیفتم</p></div>
<div class="m2"><p>بازوی آن شهسوار اگر بگذارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وانگذارم به هیچ کس دل خود را</p></div>
<div class="m2"><p>غمزه آن دل شکار اگر بگذارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست نیابد کسی به خاطر جمعم</p></div>
<div class="m2"><p>زلف پریشان یار اگر بگذارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ نگردم به گرد عشق فروغی</p></div>
<div class="m2"><p>جلوهٔ حسن نگار اگر بگذارد</p></div></div>