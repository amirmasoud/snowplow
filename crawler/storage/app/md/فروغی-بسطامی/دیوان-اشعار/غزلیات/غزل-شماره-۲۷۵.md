---
title: >-
    غزل شمارهٔ ۲۷۵
---
# غزل شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>در میکده خدمت کن بی معرکه سلطان باش</p></div>
<div class="m2"><p>فرمان بر ساقی شو، فرمانده دوران باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در حلقهٔ می‌خواران بی‌کار نباید شد</p></div>
<div class="m2"><p>یا خواجهٔ فرمانده یا بندهٔ فرمان باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صحبت یوسف را پیوسته طمع داری</p></div>
<div class="m2"><p>با آینه روشن یا آینه گردان باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که به چنگ آری آن زلف مسلسل را</p></div>
<div class="m2"><p>یا سلسله بر گردن یا سلسله جنبان باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر باده ننوشیدی شرمندهٔ ساقی شو</p></div>
<div class="m2"><p>ور عشق نورزیدی از کرده پشیمان باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خنده زند لعلش در در دل دریا ریز</p></div>
<div class="m2"><p>چون گریه کند چشمم آماده طوفان باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرچشمهٔ حیوان را نسبت به لبش کم کن</p></div>
<div class="m2"><p>از عالم حیوانی بیرون رو و انسان باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بر سر کوی او افتد گذرت روزی</p></div>
<div class="m2"><p>نه طالب جنت شو نه مایل رضوان باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی که فلک گردد گرد خم چوگانت</p></div>
<div class="m2"><p>در عرصهٔ میدانش گوی خم چوگان باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسباب پریشانی جمع است برای من</p></div>
<div class="m2"><p>جمعیت اگر خواهی زان طره پریشان باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا آگهیت بخشند از مساله معنی</p></div>
<div class="m2"><p>در کارگه صورت عاشق شو و حیران باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در عهد ملک غم را از شهر به در کردند</p></div>
<div class="m2"><p>شکرانهٔ این شادی ساغرکش و خندان باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شه ناصردین کز دل پیر فلکش گوید</p></div>
<div class="m2"><p>تا مهر درخشان است، آرایش ایوان باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر روز فروغی را تاریک نمی‌خواهی</p></div>
<div class="m2"><p>در خانهٔ تاریکش خورشید درخشان باش</p></div></div>