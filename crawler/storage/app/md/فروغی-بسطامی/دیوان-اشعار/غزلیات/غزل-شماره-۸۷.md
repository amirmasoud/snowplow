---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>گر نه زلفش پی شبیخون است</p></div>
<div class="m2"><p>پس چرا حال دل دگرگون است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد شیرین دوای فرهاد است</p></div>
<div class="m2"><p>غم لیلی نشاط مجنون است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر در چنگ شوق مغلوب است</p></div>
<div class="m2"><p>عقل در کار عشق مفتون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ننالم که تیغ بر فرق است</p></div>
<div class="m2"><p>چون نگریم که بخت وارون است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون من ریخت قاتلی که به حشر</p></div>
<div class="m2"><p>کشته‌اش از حساب بیرون است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمت من ز کارخانهٔ عشق</p></div>
<div class="m2"><p>داغ و دردی که از حد افزون است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می حرام است خاصه در رمضان</p></div>
<div class="m2"><p>جز بر آن لعل لب که میگون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر ز دست تو گریه سر نکنم</p></div>
<div class="m2"><p>چه کنم با دلی که پر خون است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا فروغی غزل‌سرای تو شد</p></div>
<div class="m2"><p>صاحب صد هزار مصمون است</p></div></div>