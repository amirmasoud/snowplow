---
title: >-
    غزل شمارهٔ ۴۵
---
# غزل شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>چون خاک می‌شود به رهت جان پاک ما</p></div>
<div class="m2"><p>بگذار نخوت از سر و بگذر به خاک ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب که دامن تو نگیرد به روز حشر</p></div>
<div class="m2"><p>خونی که ریختی ز دل چاک چاک ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردا که هیچ در دل سختت اثر نکرد</p></div>
<div class="m2"><p>اشک روان و آه دل دردناک ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی که با خیال تو در خاک می‌کنیم</p></div>
<div class="m2"><p>خم‌خانه مست می‌شود از فیض تاک ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شد شریک غصهٔ ما در طریق عشق</p></div>
<div class="m2"><p>غیرت اگر بهم نزند اشتراک ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدیم روی قاتل خود را به زیر تیغ</p></div>
<div class="m2"><p>سرمایهٔ سلامت ما شد هلاک ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تکیه کرده‌ایم فروغی به لطف دوست</p></div>
<div class="m2"><p>از خصمی فلک نبود هیچ باک ما</p></div></div>