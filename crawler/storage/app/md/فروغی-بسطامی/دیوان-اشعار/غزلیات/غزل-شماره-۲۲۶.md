---
title: >-
    غزل شمارهٔ ۲۲۶
---
# غزل شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>مردان خدا پردهٔ پندار دریدند</p></div>
<div class="m2"><p>یعنی همه جا غیر خدا یار ندیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دست که دادند از آن دست گرفتند</p></div>
<div class="m2"><p>هر نکته که گفتند همان نکته شنیدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک طایفه را بهر مکافات سرشتند</p></div>
<div class="m2"><p>یک سلسله را بهر ملاقات گزیدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک فرقه به عشرت در کاشانه گشادند</p></div>
<div class="m2"><p>یک زمره به حسرت سر انگشت گزیدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمعی به در پیر خرابات خرابند</p></div>
<div class="m2"><p>قومی به بر شیخ مناجات مریدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک جمع نکوشیده رسیدند به مقصد</p></div>
<div class="m2"><p>یک قوم دویدند و به مقصد نرسیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریاد که در رهگذر آدم خاکی</p></div>
<div class="m2"><p>بس دانه فشاندند و بسی دام تنیدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همت طلب از باطن پیران سحرخیز</p></div>
<div class="m2"><p>زیرا که یکی را ز دو عالم طلبیدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار مزن دست به دامان گروهی</p></div>
<div class="m2"><p>کز حق ببریدند و به باطل گرویدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون خلق درآیند به بازار حقیقت</p></div>
<div class="m2"><p>ترسم نفروشند متاعی که خریدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کوتاه نظر غافل از آن سرو بلند است</p></div>
<div class="m2"><p>کاین جامه به اندازهٔ هر کس نبریدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرغان نظرباز سبک‌سیر فروغی</p></div>
<div class="m2"><p>از دام گه خاک بر افلاک پریدند</p></div></div>