---
title: >-
    غزل شمارهٔ ۴۶۶
---
# غزل شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>با آن که می از شیشه به پیمانه نکردی</p></div>
<div class="m2"><p>در بزم کسی نیست که دیوانه نکردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خانه شهری نگهت برده به یغما</p></div>
<div class="m2"><p>در شهر دلی کو که در او خانه نکردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گنج غمت را سر ویرانی دلهاست</p></div>
<div class="m2"><p>یک خانه دل نیست که ویرانه نکردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حال شکست دلم آگاه نگشتی</p></div>
<div class="m2"><p>تا زلف شکن بر شکنت شانه نکردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نه من از عشق رخت شهرهٔ شهرم</p></div>
<div class="m2"><p>صاحب نظری نیست که افسانه نکردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نازم سرت ای شمع که شهری زدی آتش</p></div>
<div class="m2"><p>واندیشه ز دود دل پروانه نکردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چشم تو محرم نشدم تا به نگاهی</p></div>
<div class="m2"><p>بیگانه‌ام از محرم و بیگانه نکردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آن که به مردی نشدی کشتهٔ جانان</p></div>
<div class="m2"><p>دردا که یکی همت مردانه نکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایمن دلی از دست ستم کاری صیاد</p></div>
<div class="m2"><p>خون خوردن و فریاد غریبانه نکردی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل تنگ شدی باز فروغی مگر امروز</p></div>
<div class="m2"><p>از دست غمش گریه مستانه نکردی</p></div></div>