---
title: >-
    غزل شمارهٔ ۷۳
---
# غزل شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>تا حلقهٔ زنجیر دل آن زلف دراز است</p></div>
<div class="m2"><p>درهای جنون بر من سودازده باز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شور دل فرهاد شکر خندهٔ شیرین</p></div>
<div class="m2"><p>تاج سر محمود و کف پای ایاز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمی که تویی شاهد او محو تماشا</p></div>
<div class="m2"><p>جایی که تویی قبلهٔ او گرم نماز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان عمر من و زلف تو کوتاه و بلند است</p></div>
<div class="m2"><p>زیرا که به هر ورطه نشیب است و فراز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صیدی که به چنگ تو نیفتاد چه داند</p></div>
<div class="m2"><p>حال دل آن صعوه که در چنگل باز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خشم کند لعبت منظور وگر ناز</p></div>
<div class="m2"><p>صاحب نظر آن است که در عین نیاز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوز دل عشاق ز پروانه بپرسید</p></div>
<div class="m2"><p>کز شمع فروزنده مهیای گداز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تشویش جزا با همه تقصیر نداریم</p></div>
<div class="m2"><p>چون خواجهٔ بخشندهٔ ما بنده‌نواز است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نازنده درآمد ز در آن شوخ فروغی</p></div>
<div class="m2"><p>هنگام نیاز من و هنگامهٔ ناز است</p></div></div>