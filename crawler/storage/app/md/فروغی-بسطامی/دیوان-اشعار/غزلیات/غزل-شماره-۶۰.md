---
title: >-
    غزل شمارهٔ ۶۰
---
# غزل شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>دی در میان مستی خنجر کشیده برخاست</p></div>
<div class="m2"><p>وز ما به جز محبت جرمی ندیده برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم سیاه مستش آیا چه دیده باشد</p></div>
<div class="m2"><p>کز کوی تیره بختان می‌ناچشیده برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم بر هوای بامش مرغ پریده بنشست</p></div>
<div class="m2"><p>هم بر امید دامش صید رمیده برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش از رخش نسیمی بگذشت سوی گلشن</p></div>
<div class="m2"><p>گل از فراز گلبن برقع دریده برخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بی‌خبر که خندید بر حسرت زلیخا</p></div>
<div class="m2"><p>آخر ز بزم یوسف کف را بریده برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صید دل حریصم از شوق تیر دیگر</p></div>
<div class="m2"><p>از صیدگاه خونین در خون تپیده برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوشینه ماه نو را دیدم به روی ماهی</p></div>
<div class="m2"><p>کز بهر پای بوسش چرخ خمیده برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر نیم شب که کردم یادی از آن بناگوش</p></div>
<div class="m2"><p>از مشرق امیدم صبح دمیده برخاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من بی رخش فروغی آفاق را ندیدم</p></div>
<div class="m2"><p>برخاست تا ز چشمم، نورم ز دیده برخاست</p></div></div>