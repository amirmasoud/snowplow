---
title: >-
    غزل شمارهٔ ۱۶۱
---
# غزل شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>نظر ز روی تو صاحب نظر نمی‌بندد</p></div>
<div class="m2"><p>که هیچ کس به چنین روی در نمی‌بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ز صورت خوب تو پی به معنی برد</p></div>
<div class="m2"><p>که چرخ نقشی ازین خوب تر نمی‌بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه زان لب شیرین اگر خبر گردد</p></div>
<div class="m2"><p>به راستی کمر نیشکر نمی‌بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک کوی تو شب نیست کاب دیدهٔ من</p></div>
<div class="m2"><p>ره گذرگه باد سحر نمی‌بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز قامت تو چنان پایمال شد طوبی</p></div>
<div class="m2"><p>که تا به روز قیامت کمر نمی‌بندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کبوتران حرم را به جز تو کافرکیش</p></div>
<div class="m2"><p>پس از هلاک کسی بال و پر نمی‌بندد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز آن پسر که منش دوست چون پدر دارم</p></div>
<div class="m2"><p>کسی میان پی قتل پدر نمی‌بندد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وفا نمودم و پاداش آن جفا دیدم</p></div>
<div class="m2"><p>که گفت نخل محبت ثمر نمی‌بندد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هزار بار به خون گر کشی فروغی را</p></div>
<div class="m2"><p>ز آستان تو بار سفر نمی‌بندد</p></div></div>