---
title: >-
    غزل شمارهٔ ۲۹۱
---
# غزل شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>چه غنچه‌ها که نپرود باغ نسرینش</p></div>
<div class="m2"><p>چه میوه‌ها که نیاورد سرو سیمینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه فتنه‌ها که نینگیخت چشم پرخوابش</p></div>
<div class="m2"><p>چه حلقه‌ها که نیاویخت زلف پرچینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه دانه‌ها که نپاشید خال هندویش</p></div>
<div class="m2"><p>چه دام ها که نگسترد خط مشکینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه کیسه‌ها که نپرداخت جعد طرارش</p></div>
<div class="m2"><p>چه کاسه‌ها که نپیمود لعل نوشینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه صیدها که نشد کشته در کمینگاهش</p></div>
<div class="m2"><p>چه تیغ‌ها که نزد پنجهٔ نگارینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه قلب‌ها که نیازرد لشکر نازش</p></div>
<div class="m2"><p>چه سینه‌ها که نفرسود خنجر کینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه پنجه‌ها که نپیچد زور بازویش</p></div>
<div class="m2"><p>چه کشته‌ها که نینداخت دست رنگینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه کلبه‌ها که نیفروخت ماه تابانش</p></div>
<div class="m2"><p>چه خوشه‌ها که نیندوخت عقد پروینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه شرم ها که نکرد آفتاب از رویش</p></div>
<div class="m2"><p>چه رشک ها که نبرد آسمان ز تمکینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه جامه‌ها که نپوشید قد دلکش او</p></div>
<div class="m2"><p>که در کنار کشد شاه ناصرالدینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خدیو مملکت آرا خدایگان ملوک</p></div>
<div class="m2"><p>که کرده بار خدا قبلهٔ سلاطینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر ملوک عجم مالک ممالک جم</p></div>
<div class="m2"><p>که مهر خیره شد از تاج گوهر آگینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابوالفوارس ببرافکن هژبرشکن</p></div>
<div class="m2"><p>که رفته خنگ فلک زیر زین زرینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابوالمظفر غازی سوار تیغ‌گذار</p></div>
<div class="m2"><p>که خون خصم گذر کرده از سر زینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی رسول فرستد ز خطهٔ رومش</p></div>
<div class="m2"><p>یکی سلام رساند ز ساحت چینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صفات ذات ورا شرح کی توانم داد</p></div>
<div class="m2"><p>اگر که وصف کنم صدهزار چندینش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گدا چگونه کند مدح پادشاهی را</p></div>
<div class="m2"><p>که خسروان همه جا کرده‌اند تحسینش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فروغی از لب نوشین او مگر دم زد</p></div>
<div class="m2"><p>که شهره در همه شهر است شعر شیرینش</p></div></div>