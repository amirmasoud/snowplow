---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>کار من تا به زلف یار من است</p></div>
<div class="m2"><p>صد هزاران گره به کار من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا روز تیره‌ای بینی</p></div>
<div class="m2"><p>دست پرورد روزگار من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادمانی به شدمن ارزانی</p></div>
<div class="m2"><p>تا غم دوست دوستدار من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناصح تیره‌دل چنان داند</p></div>
<div class="m2"><p>که محبت به اختیار من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که در هیچ جا قرارش نیست</p></div>
<div class="m2"><p>دل بی‌صبر و بی‌قرار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی طفلان نوش لب گیرد</p></div>
<div class="m2"><p>طفل اشکی که در کنار من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح محشر که گفت واعظ شهر</p></div>
<div class="m2"><p>از پس شام انتظار من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن قیامت که عاشقان خواهند</p></div>
<div class="m2"><p>قامت سرو گلعذار من است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلس‌آرای عالم معنی</p></div>
<div class="m2"><p>صورت نازنین نگار من است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من فروغی پیمبر سخنم</p></div>
<div class="m2"><p>معجزم نظم آب‌دار من است</p></div></div>