---
title: >-
    غزل شمارهٔ ۷۴
---
# غزل شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>تا دیدن آن ماه فروزنده محال است</p></div>
<div class="m2"><p>فیروزی‌ام از اختر فرخنده محال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زلف پراکندهٔ او جمع نگردد</p></div>
<div class="m2"><p>جمعیت دل‌های پراکنده محال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا از همه شیرین دهنان چشم نپوشی</p></div>
<div class="m2"><p>بوسیدن آن لعل شکرخنده محال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکل که به دستم رسد آن لعل گهر بار</p></div>
<div class="m2"><p>بر دست گدا گوهر ارزنده محال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عشق من از پرده عیان شده عجبی نیست</p></div>
<div class="m2"><p>پوشیدن این آتش سوزنده محال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من در همه احوال خوشم، تا تو نگویی</p></div>
<div class="m2"><p>کز بهر کسی شادی پاینده محال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خواجه مشفق بکشد یا که ببخشد</p></div>
<div class="m2"><p>الا روش بندگی از بنده محال است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشنو که دم تیشه چه خوش گفت به فرهاد</p></div>
<div class="m2"><p>رفتن ز سر کوی وفا، زنده محال است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کس در عقبش قوت رفتار ندارد</p></div>
<div class="m2"><p>همراهی آن سرو خرامنده محال است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آگاه نشد هیچکس از بازی گردون</p></div>
<div class="m2"><p>آگاهی از این گنبد گردنده محال است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سرمایهٔ دریای گران‌مایه فروغی</p></div>
<div class="m2"><p>بی‌ابر کف خسرو بخشنده محال است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شه ناصردین آن که بر رای منیرش</p></div>
<div class="m2"><p>تابیدن خورشید درخشنده محال است</p></div></div>