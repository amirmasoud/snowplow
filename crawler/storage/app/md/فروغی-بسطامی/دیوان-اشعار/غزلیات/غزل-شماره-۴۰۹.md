---
title: >-
    غزل شمارهٔ ۴۰۹
---
# غزل شمارهٔ ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>گاهی به نوشخند لبت را اشاره کن</p></div>
<div class="m2"><p>ما را به هیچ صاحب عمر دوباره کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمای روی خود ز پس پرده آشکار</p></div>
<div class="m2"><p>یک باره راز هر دو جهان آشکاره کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقتی که چارهٔ دل عشاق می‌کنی</p></div>
<div class="m2"><p>درد مرا به نیم شکرخنده چاره کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با جام می شبی به شبستان من بیا</p></div>
<div class="m2"><p>آسوده‌ام ز گردش ماه و ستاره کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهی که دامن تو نگیرم روز حشر</p></div>
<div class="m2"><p>در زیر تیغ جانب ما یک نظاره کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیر است آن چه می‌رسد از دست چون تویی</p></div>
<div class="m2"><p>کمتر به قتل خسته‌دلان استخاره کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اکنون که از کنار منت میل رفتن است</p></div>
<div class="m2"><p>اول بریز خونم و آخر کناره کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با مهربانی از دل سنگین او مخواه</p></div>
<div class="m2"><p>یا ناله را بگو گذر از سنگ خاره کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم فروغی از پی مژگان او مرو</p></div>
<div class="m2"><p>رفتی کنون علاج دل پاره پاره کن</p></div></div>