---
title: >-
    غزل شمارهٔ ۳۳۱
---
# غزل شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>اگر گاهی بدان مه پاره یک نظاره می‌کردم</p></div>
<div class="m2"><p>گریبان فلک را تا به دامان پاره می‌کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آن خورشید خرگاهی ندیم بزم من می‌شد</p></div>
<div class="m2"><p>بزرگی زین شرف بر ثابت و سیاره می‌کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانستم که دور چرخش از من دور می‌سازد</p></div>
<div class="m2"><p>و گر نه چارهٔ چشم بد استاره می‌کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس گر می‌شنید از من فسون و مکر گردون را</p></div>
<div class="m2"><p>بسی افسانه زین افسون گر مکاره می‌کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر می‌شد نصیب من سر کوی حبیب من</p></div>
<div class="m2"><p>به صد خواری رقیب سفله را آواره می‌کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌دیدم طبیبی غیر آن عیسی نفس، ورنه</p></div>
<div class="m2"><p>علاج درد بی درمان خود صد باره، می‌کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبی بر گردن مار غیرت حلقه‌ها می‌زد</p></div>
<div class="m2"><p>که زلفش را شبیه عقرب جراره می‌کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرو می‌ریخت خون دیده بر رخسار من وقتی</p></div>
<div class="m2"><p>که در خاطر خیال آن پری رخساره می‌کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنار مزرع سبز فلک یکباره تر می‌شد</p></div>
<div class="m2"><p>اگر در گریه شب ها دیده را فواره می‌کردم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسیر کودکی کردند چون من پهلوانی را</p></div>
<div class="m2"><p>که رستم را کمان کودک گهواره می‌کردم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون در کار خود بی چاره گردیدم، خوشا روزی</p></div>
<div class="m2"><p>که من هم درد هر بیچاره‌ای را چاره می‌کردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بپرس از من کرامت های پیر می‌پرستان را</p></div>
<div class="m2"><p>که در می‌خانه عمری کار هر میخواره می‌کردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغی من ثنای شاه را تنها نمی‌گفتم</p></div>
<div class="m2"><p>دعا هم بر دوام دولتش همواره می‌کردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدیو معدلت‌جو ناصرالدین شاه خوش طینت</p></div>
<div class="m2"><p>که تقسیم سر خصمش به سنگ خاره می‌کردم</p></div></div>