---
title: >-
    غزل شمارهٔ ۴۸
---
# غزل شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>ای زلف تو بر هم زن فرزانگی ما</p></div>
<div class="m2"><p>وین سلسله سرمایهٔ دیوانگی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بر دم تیغ تو نهادیم به مردی</p></div>
<div class="m2"><p>کس نیست درین عرصه به مردانگی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ما نشدی محرم و از خلق دو عالم</p></div>
<div class="m2"><p>سودای تو شد علت بیگانگی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن مرغ اسیریم به دام تو که خوردند</p></div>
<div class="m2"><p>مرغان گلستان غم بی دانگی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که کسی نیست به بیچارگی من</p></div>
<div class="m2"><p>گفتا که بتی نیست به جانانگی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که بود قاتل صاحب‌نظران، گفت</p></div>
<div class="m2"><p>چشمی که بود منشا مستانگی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم همه را سوخت به یک شعله فروغی</p></div>
<div class="m2"><p>شمعی که بود باعث پروانگی ما</p></div></div>