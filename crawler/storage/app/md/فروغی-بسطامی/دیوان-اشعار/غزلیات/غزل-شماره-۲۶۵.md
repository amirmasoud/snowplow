---
title: >-
    غزل شمارهٔ ۲۶۵
---
# غزل شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>دل به حسرت ز سر کوی کسی می‌آید</p></div>
<div class="m2"><p>مرغی از سدره به کنج قفسی می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکری چند بخواه از لب شیرین دهنان</p></div>
<div class="m2"><p>تا بدانی که چه‌ها بر مگسی می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ره عشق پی ناله دل باید رفت</p></div>
<div class="m2"><p>زان که رهرو به صدای جرسی می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌روم گریه‌کنان از سر کویی کانجا</p></div>
<div class="m2"><p>عاشقی می‌رود و بوالهوسی می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردیم مست به نوعی که ندانم امشب</p></div>
<div class="m2"><p>شحنه‌ای می‌گذرد یا عسسی می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفسی با تو به از زندگی جاوید است</p></div>
<div class="m2"><p>وین میسر نشود تا نفسی می‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو ستم پیشه برآنی که ستانی همه عمر</p></div>
<div class="m2"><p>من در اندیشه که فریادرسی می‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گذرگاه تو ای چشم و چراغ همه شهر</p></div>
<div class="m2"><p>دل شهری ز پی ملتمسی می‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نه در راه تو گم کرد فروغی دل را</p></div>
<div class="m2"><p>پس چرا بر سر این راه بسی می‌آید</p></div></div>