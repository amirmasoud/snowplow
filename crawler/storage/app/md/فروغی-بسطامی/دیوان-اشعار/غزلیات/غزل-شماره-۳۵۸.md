---
title: >-
    غزل شمارهٔ ۳۵۸
---
# غزل شمارهٔ ۳۵۸

<div class="b" id="bn1"><div class="m1"><p>ای کعبهٔ مقصودم، وی قبلهٔ آمالم</p></div>
<div class="m2"><p>مپسند بدین روزم، مگذار بدین حالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم سینه به تنگ آمد از نالهٔ شب گیرم</p></div>
<div class="m2"><p>هم دیده به جان آمد از گریهٔ سیالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شامگه هجرش بگداخت تن و جانم</p></div>
<div class="m2"><p>در دامگه عشقش بشکست پر و بالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حسرت دیدارش طی گشت شب و روزم</p></div>
<div class="m2"><p>در محنت بسیارش بگذشت مه و سالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زلف پریشانش در هم شده ایامم</p></div>
<div class="m2"><p>کز صف زده مژگانش وارون شده اقبالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شعلهٔ رخسارش می‌سوزم و می‌سازم</p></div>
<div class="m2"><p>وز جلوهٔ رفتارش می‌گریم و می‌نالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب نیست که در پایش تا روز به صد زاری</p></div>
<div class="m2"><p>یا جبهه نمی‌سایم یا چهره نمی‌مالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر با رخ زیبایش یکشام به صبح آرم</p></div>
<div class="m2"><p>فیروز شود روزم، فرخنده شود فالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زلف و خطش بینی معلوم شود بر تو</p></div>
<div class="m2"><p>هم معنی اوضاعم، هم صورت احوالم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فردا که گنهکاران در پای حساب آیند</p></div>
<div class="m2"><p>جز عشق گناهی نیست در نامهٔ اعمالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن روز فروغی من از قتل شوم ایمن</p></div>
<div class="m2"><p>کاو خط امان بخشد زان غمزهٔ قتالم</p></div></div>