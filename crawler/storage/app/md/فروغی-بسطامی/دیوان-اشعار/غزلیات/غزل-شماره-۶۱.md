---
title: >-
    غزل شمارهٔ ۶۱
---
# غزل شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>به هر غمی که رسد از تو خاطرم شاد است</p></div>
<div class="m2"><p>که بندهٔ تو ز بند کدورت آزاد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه پیش تو ناید پری به شاگردی</p></div>
<div class="m2"><p>که مو به موی تو در علم غمزه استاد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سیل حادثه غم نیست میگساران را</p></div>
<div class="m2"><p>که آستانه میخانه سخت بنیاد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم زمانه مرا سخت در میانه گرفت</p></div>
<div class="m2"><p>بیا فدای تو ساقی که وقت امداد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی که هیچ فسونگر نکرد تسخیرش</p></div>
<div class="m2"><p>کنون مسخر افسون آن پری‌زاد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای سور بلندی فتاده بر سر من</p></div>
<div class="m2"><p>که سایه‌اش به سر هیچکس نیفتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مذاق عیش مرا تلخ کرد شیرینی</p></div>
<div class="m2"><p>که تلخ‌کام لبش صدهزار فرهاد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان که داد ز دست ستمگری است مرا</p></div>
<div class="m2"><p>که هرگزش نتوان گفت این چه بیداد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهی به خون اسیران عشق فرمان داد</p></div>
<div class="m2"><p>که تیغ بر کف ترکان کج کله داد است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فروغی از ستم مهوشان به درگه عشق</p></div>
<div class="m2"><p>چرا خموش نشینی که جای فریاد است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جهان گشای عدوبند شاه ناصردین</p></div>
<div class="m2"><p>که تیغ اوهمه درهای بسته بگشاد است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر ملوک عجم تاجدار کشود جم</p></div>
<div class="m2"><p>که ذات او سبب دستگاه ایجاد است</p></div></div>