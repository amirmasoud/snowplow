---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>شب فراق تو گر ناله را اشاره کنم</p></div>
<div class="m2"><p>چه رخنه‌ها که در ارکان سنگ خاره کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه طاقتی که ز نظاره‌ات بپوشم چشم</p></div>
<div class="m2"><p>نه قدرتی که به رخساره‌ات نظاره کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه پای آن که به سوی تو ره بپیمایم</p></div>
<div class="m2"><p>نه دست آن که ز خوی تو جامه پاره کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کیش زمرهٔ عشاق دوزخی باشم</p></div>
<div class="m2"><p>به بوی سدره ز کوی تو گر کناره کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی بر غم فلک روی خویشتن بنما</p></div>
<div class="m2"><p>که زهره را بدرم، ماه را دو پاره کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بی تو آه شرر بار برکشم از دل</p></div>
<div class="m2"><p>علاج خرمن گردون به یک شراره کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشم به کشمکش خون خویش روز جزا</p></div>
<div class="m2"><p>که سیر روی زین رهگذر دوباره کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گره فتد به سر زلفت از پریشانی</p></div>
<div class="m2"><p>گر اشتیاقی ترا مو به مو شماره کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به غیر دادن جان چاره‌ای نخواهم جست</p></div>
<div class="m2"><p>اگر به درد تو چندین هزار چاره کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سر گنبد مینا نمی‌شوم آگاه</p></div>
<div class="m2"><p>مگر که خدمت رند شراب خواره کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فروغی از غم آن ماه خرگهی تا چند</p></div>
<div class="m2"><p>کنار خویشتن از اشک پر ستاره کنم</p></div></div>