---
title: >-
    غزل شمارهٔ ۵۵
---
# غزل شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>از جلوهٔ حسنت که بری از همه عیب است</p></div>
<div class="m2"><p>آسوده‌دل آن است که در پردهٔ غیب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم از رخ تو صحن چمن لاله به دامان</p></div>
<div class="m2"><p>هم از خط تو باد صبا نافه به جیب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مرحلهٔ شوق نه ننگ است و نه ناموس</p></div>
<div class="m2"><p>در مسئلهٔ عشق نه شک است و نه ریب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موسی چه کند گر نکند پیشه شبانی</p></div>
<div class="m2"><p>تا بر سرش اندیشهٔ فرزند شعیب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسانهٔ جان دادن خود هیچ فروغی</p></div>
<div class="m2"><p>در حضرت جانان نتوان گفت که عیب است</p></div></div>