---
title: >-
    غزل شمارهٔ ۲۹۹
---
# غزل شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>در پا مریز حلقهٔ زلف بلند خویش</p></div>
<div class="m2"><p>ترسم خدا نکرده شوی پای‌بند خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت خدای را که به تسخیر ملک دل</p></div>
<div class="m2"><p>حاجت بدان نشد که بتازی سمند خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیف است بر لب تو رساند لبی رقیب</p></div>
<div class="m2"><p>کالوده مگس نتوان کرد قند خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا از شکنج طره کمندی به ره منه</p></div>
<div class="m2"><p>یا رحمتی به آهوی سر در کمند خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ناله در غم تو ز بس خو گرفته‌ام</p></div>
<div class="m2"><p>آسوده‌ام به نالهٔ ناسودمند خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکل شده‌ست کار من از عشق روی تو</p></div>
<div class="m2"><p>لیکن چه چاره با دل مشکل‌پسند خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون می‌چکد ز غنچه به کارش اگر کنی</p></div>
<div class="m2"><p>شیرین تبسمی ز لب نوش‌خند خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوق سپند خال تو کرد آن چه با دلم</p></div>
<div class="m2"><p>مجمر نکرده ز آتش خود با سپند خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شه سوار حسن فروغی اسیر تست</p></div>
<div class="m2"><p>غافل مشو ز خاک گرفتار بند خویش</p></div></div>