---
title: >-
    غزل شمارهٔ ۱۲۵
---
# غزل شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>گر نه آن ترک سیه چشم سر یغما داشت</p></div>
<div class="m2"><p>مژه را بهر چه صف در صف جا بر جا داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تلخ کامی مرا دید و ترش روی نشست</p></div>
<div class="m2"><p>آن که صد تنگ شکر در لب شکرخا داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم آمد به لب از حسرت شیرین دهنی</p></div>
<div class="m2"><p>که در احیای دل مرده دم عیسی داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهدی تشنه لبم کشت که از غایت لطف</p></div>
<div class="m2"><p>چشمهٔ آب بقا در لب جان بخشا داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخت بدبین که ز اندوه کسی جان دادم</p></div>
<div class="m2"><p>کز پی کاهش غم روی نشاط افزا داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل دیوانه از آن کوی به حسرت می‌رفت</p></div>
<div class="m2"><p>ولی از سنبل او سلسله‌ها برپا داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت کشتن نظری جانب قاتل کردند</p></div>
<div class="m2"><p>تیغ بر گردن عشاق چه منت‌ها داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش بر حسن خود آن ماه نظر بگشاید</p></div>
<div class="m2"><p>تا بداند که چرا عشق مرا شیدا داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش از وجد فروغی به کلیسا می‌گفت</p></div>
<div class="m2"><p>که مرا جلوهٔ ترسابچه‌ای ترسا داشت</p></div></div>