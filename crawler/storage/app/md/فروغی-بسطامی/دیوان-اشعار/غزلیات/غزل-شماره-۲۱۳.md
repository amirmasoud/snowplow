---
title: >-
    غزل شمارهٔ ۲۱۳
---
# غزل شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>قتل ما ای دل به تیغ او مقدر کرده‌اند</p></div>
<div class="m2"><p>غم مخور زیرا که روزی را مقرر کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا ذکری از آن جعد معنبر کرده‌اند</p></div>
<div class="m2"><p>مشک چین را از خجالت خاک بر سر کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ز خونت نگذری، مگذار پا در کوی عشق</p></div>
<div class="m2"><p>زان که اینجا خاک را با خون مخمر کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقانش را به محشر وعدهٔ دیدار داد</p></div>
<div class="m2"><p>ساده لوحی بین که این افسانه باور کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با لب لعل بتان هیچ از کرامت دم مزن</p></div>
<div class="m2"><p>زان که اینان معجز عیسی مکرر کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر سر موی مرا در دیدهٔ بدبین او</p></div>
<div class="m2"><p>گاه نوک خنجر و گه نیش نشتر کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا شب هجرانش آمد روشنم شد مو به مو</p></div>
<div class="m2"><p>آن چه با تقصیرکاران روز محشر کرده‌اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا به بازار تو جان دادم نکو شد کار من</p></div>
<div class="m2"><p>سودمندان کی ازین سودا نکوتر کرده‌اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو به ابرو کرده‌ای تسخیر دلها گر مدام</p></div>
<div class="m2"><p>خسروان از تیغ عالم را مسخر کرده‌اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو ز مژگان کرده‌ای با قلب مشتاقان خویش</p></div>
<div class="m2"><p>آن چه جلادان سنگین دل ز خنجر کرده‌اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صورتی را کاو ز کف دین فروغی را ربود</p></div>
<div class="m2"><p>معنیش در پردهٔ خاطر مصور کرده‌اند</p></div></div>