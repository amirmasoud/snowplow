---
title: >-
    غزل شمارهٔ ۳۲۳
---
# غزل شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>ساقی نداده ساغر چندان نموده مستم</p></div>
<div class="m2"><p>کز خود خبر ندارم در عالمی که هستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس قدح کشیدم در کوی می فروشان</p></div>
<div class="m2"><p>هم جامه را دریدم، هم شیشه را شکستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید عارض او چون ذره برده تابم</p></div>
<div class="m2"><p>بالای سرکش او چون سایه کرده پستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام دلم تو بودی هر سو که می‌دویدم</p></div>
<div class="m2"><p>سر منزلم تو بودی هر جا که می‌نشستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغش جدا نسازد دستی که با تو دادم</p></div>
<div class="m2"><p>مرگش ز هم نبرد عهدی که با تو بستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیفیت جنون را از من توان شنیدن</p></div>
<div class="m2"><p>کز عشق آن پری رو زنجیرها گسستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترسم کز این لطافت کان نازنین صنم راست</p></div>
<div class="m2"><p>گرد صمد نگردد نفس صنم‌پرستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سنگین دلی که کرده‌ست رنگین به خون من دست</p></div>
<div class="m2"><p>فریاد اگر به محشر دامن کشد ز دستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هر طرف دویدم همچون صبا فروغی</p></div>
<div class="m2"><p>لیکن به هیچ حیلت از بند او نجستم</p></div></div>