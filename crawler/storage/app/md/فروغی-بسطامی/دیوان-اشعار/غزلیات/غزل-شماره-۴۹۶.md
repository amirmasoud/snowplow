---
title: >-
    غزل شمارهٔ ۴۹۶
---
# غزل شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>بس که فرخ رخ و شکر لب و شیرین دهنی</p></div>
<div class="m2"><p>رهزن دین و دلی، خانه کن مرد و زنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از این بخت سیه خواجهٔ شهر جبشم</p></div>
<div class="m2"><p>تو از آن روی چو مه خسرو ملک ختنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مادر دهر نیاورد چو تو شیرینی</p></div>
<div class="m2"><p>پدر چرخ نپرورده چو من کوه کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم ز کوثر نزنم تا لبت اندر نظر است</p></div>
<div class="m2"><p>یاد جنت نکنم تا تو در این انجمنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان سر زلف دوتا دست نخواهم برداشت</p></div>
<div class="m2"><p>تا مرا جمع نسازی و پریشان نکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به ساق تو رسد سیم سرشکم نه عجب</p></div>
<div class="m2"><p>که سیه چشم و سهی قامت و سیمین ذقنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فلک عاقبت از بیخ بنم خواهد کند</p></div>
<div class="m2"><p>ستم است اینکه تو بنیاد مرا برنکنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم ایام ندیده‌ست و نخواهد دیدن</p></div>
<div class="m2"><p>که وصال تو چو تویی دست دهد بر چو منی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نزنی سایه بر آن زلف مسلسل گه رقص</p></div>
<div class="m2"><p>تا از این سلسله صد سلسله بر هم نزنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده برداشتن از روی تو مستحسن نیست</p></div>
<div class="m2"><p>که به تصدیق نظر صاحب وجه حسنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هیچ دیوانه به زنجیر نگنجد به نشاط</p></div>
<div class="m2"><p>تا تو با سلسلهٔ زلف شکن برشکنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نازت افزون شده از عجز فروغی، فریاد</p></div>
<div class="m2"><p>که ستم پیشه و عاشق کش و عاجز فکنی</p></div></div>