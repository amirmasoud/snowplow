---
title: >-
    غزل شمارهٔ ۴۹۵
---
# غزل شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>عشق و کمین گشادنی، ما و ز جان بریدنی</p></div>
<div class="m2"><p>یار و کمان کشیدنی، ما و به خون تپیدنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی کشتگان او ضربت تیغ خوردنی</p></div>
<div class="m2"><p>قسمت عاشقان او حسرت دل کشیدنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پردهٔ صبر می‌درد عارضش از نظاره‌ای</p></div>
<div class="m2"><p>خون عقیق می‌خورد لعل وی از مکیدنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وه که بر آه عاشقی با همه آرزو شدم</p></div>
<div class="m2"><p>خوش دل از او به غمزه‌ای قانع ازو به دیدنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جلوه کند چو قامتش زیر قبای زرفشان</p></div>
<div class="m2"><p>ما و به جلوه‌گاه او جامهٔ جان دریدنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه کس تظلمی وز تو به لب تبسمی</p></div>
<div class="m2"><p>از همه سو قیامتی وز تو به ره چمیدنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون تو قیام می‌کنی ما و ز پا فتادنی</p></div>
<div class="m2"><p>چون تو به ناز می‌روی، ما و به سر دویدنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که به باغ عارضت واله و مست و بی خودم</p></div>
<div class="m2"><p>دست مرا نمی‌رسد نوبت میوه‌چیدنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شادم از آن فروغیا کز اثر محبتی</p></div>
<div class="m2"><p>نقد نشاط صرف شد بر سر غم خریدنی</p></div></div>