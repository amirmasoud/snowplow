---
title: >-
    غزل شمارهٔ ۳۲۲
---
# غزل شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>من این عهدی که با موی تو بستم</p></div>
<div class="m2"><p>به مویت گر سر مویی شکستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از عمری به زلفت عهد بستم</p></div>
<div class="m2"><p>عجب سر رشته‌ای آمد به دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مویت کافر زنار بندم</p></div>
<div class="m2"><p>ز رویت هندوی آتش پرستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمند عشق را گردن نهادم</p></div>
<div class="m2"><p>طناب عقل را درهم گسستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مستوری چه می‌پرسی که عورم</p></div>
<div class="m2"><p>ز هشیاری چه می‌گویی که مستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شراب شادکامی را چشیدم</p></div>
<div class="m2"><p>سبوی نیک نامی را شکستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به شمشیر از سر کویش نرفتم</p></div>
<div class="m2"><p>به تدبیر از خم بندش نجستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فزون تر شد هوای او پس از مرگ</p></div>
<div class="m2"><p>تو پنداری کزین اندیشه رستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین ساقی ز خویشم بی خبر ساخت</p></div>
<div class="m2"><p>که آگه نیستم از خود که هستم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گواه دعویم پیر مغان است</p></div>
<div class="m2"><p>که مست از جرعهٔ جام آلستم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قیامت چون نخوانم قامتت را</p></div>
<div class="m2"><p>که تا برخاستی، از پا نشستم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه گفتی زان سهی بالا فروغی</p></div>
<div class="m2"><p>که فارغ کردی از بالا و پستم</p></div></div>