---
title: >-
    غزل شمارهٔ ۹۹
---
# غزل شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>زین حلاوت‌ها که در کنج لب شیرین تست</p></div>
<div class="m2"><p>کی اجل بندد زبانی را که در تحسین تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کامیاب آن تن که تنها با تو در بستر بخفت</p></div>
<div class="m2"><p>نیک‌بخت آن سر که شب‌ها بر سر بالین تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه در کون مکان می‌بینم ای سلطان حسن</p></div>
<div class="m2"><p>بی‌سروسامان عشق بی‌دل و بی‌دین تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که چون طومار پیچیده‌ست دل‌ها را به هم</p></div>
<div class="m2"><p>چین زلف عنبرافشان و خط مشکین تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غالبا غالب نگردد با تو دست روزگار</p></div>
<div class="m2"><p>زین توانایی که در سرپنجهٔ سنگین تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون‌بهایی از تو نتوان خواست کز روز ازل</p></div>
<div class="m2"><p>عشق‌بازی کیش تو، عاشق‌کشی آیین تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر بلایی بر زمین نازل شود از آسمان</p></div>
<div class="m2"><p>جز بلای ما که از بالای با تمکین تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز مردم تیره شد از نالهٔ شبگیر ما</p></div>
<div class="m2"><p>وین هم از تحریک تار طرهٔ پرچین تست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر چو مینا خون بگریم بر من از حیرت مخند</p></div>
<div class="m2"><p>کاین هم از کیفیت جام می رنگین تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر من از لب‌تشنگی در عشق میرم باک نیست</p></div>
<div class="m2"><p>زآن که آب زندگی در شمهٔ نوشین تست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواجه هی چشم عنایت از فروغی برمدار</p></div>
<div class="m2"><p>زآن که مملوک قدیم و بندهٔ دیرین تست</p></div></div>