---
title: >-
    غزل شمارهٔ ۱۱
---
# غزل شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>من که مشتاقم به جان برگشته مژگان تو را</p></div>
<div class="m2"><p>کی توانم برکشید از سینه پیکان تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بدینسان نرگس مست تو ساغر می‌دهد</p></div>
<div class="m2"><p>هوشیاری مشکل است البته مستان تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وعده فردای زاهد قسمت امروز نیست</p></div>
<div class="m2"><p>بهر حور از دست نتوان داد دامان تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز سر زلف پریشانت نمی‌بینم کسی</p></div>
<div class="m2"><p>کاو به خاطر آورد خاطر پریشان تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دریغ از تیغ ابرویت که خون غیر ریخت</p></div>
<div class="m2"><p>سالها بیهوده رفتم خاک میدان تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز از جیب فلک سر بر نیارد آفتاب</p></div>
<div class="m2"><p>صبح‌دم بیند اگر چاک گریبان تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن آفاق را پر عنبر سارا کنند</p></div>
<div class="m2"><p>گر بر افشانند زلف عنبر افشان تو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم گریان مرا از گریه نتوان منع کرد</p></div>
<div class="m2"><p>تا به کام دل نبوسم لعل خندان تو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آه سوزان را فروغی اندکی آهسته تر</p></div>
<div class="m2"><p>ترسم آسیبی رسد شمع شبستان تو را</p></div></div>