---
title: >-
    غزل شمارهٔ ۲۳۹
---
# غزل شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>دل نداند که فدای سر جانان چه کند</p></div>
<div class="m2"><p>گر فدای سر جانان نکند جان چه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب شکر شکنت رونق کوثر بشکست</p></div>
<div class="m2"><p>تا دهان تو به سرچشمهٔ حیوان چه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جنبش اهل جنون سلسله‌ها را بگسست</p></div>
<div class="m2"><p>تا خم طرهٔ آن سلسله جنبان چه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرهٔ کار مرا دست فلک باز نگرد</p></div>
<div class="m2"><p>تا قوی پنجه آن طرهٔ پیچان چه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمع کردم همه اسباب پریشانی را</p></div>
<div class="m2"><p>تا پریشانی آن زلف پریشان چه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شام من صبح ز خورشید فروزنده نشد</p></div>
<div class="m2"><p>تا فروغ رخ آن ماه درخشان چه کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رازم از پردهٔ دل هیچ هویدا نشده‌ست</p></div>
<div class="m2"><p>تا که غمازی آن غمزهٔ پنهان چه کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خضر آب بقا داد و به جمشید شراب</p></div>
<div class="m2"><p>تا به پیمانهٔ ما ساقی دوران چه کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جنبشی کرد صنوبر که قیامت برخاست</p></div>
<div class="m2"><p>تا سهی قامت آن سرو خرامان چه کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نرگس مست به باغ آمد و پیمانه به دست</p></div>
<div class="m2"><p>تا قدح بخشی آن نرگس فتان چه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسته‌های شکر از هند به ری آمده باز</p></div>
<div class="m2"><p>تا شکر خندهٔ آن پستهٔ خندان چه کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صف ترکان ختایی همه آراسته شد</p></div>
<div class="m2"><p>تا صف آرایی آن صف زده مژگان چه کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پایه طبع فروغی ز نهم چرخ گذشت</p></div>
<div class="m2"><p>تا علو نظر همت سلطان چه کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناصرالدین شه بخشنده که دست کرمش</p></div>
<div class="m2"><p>می‌نداند که به سرمایهٔ عمان چه کند</p></div></div>