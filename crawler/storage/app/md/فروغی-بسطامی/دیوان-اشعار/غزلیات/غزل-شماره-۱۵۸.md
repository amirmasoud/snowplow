---
title: >-
    غزل شمارهٔ ۱۵۸
---
# غزل شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>ای کاش پی قتل من آن سیم تن افتد</p></div>
<div class="m2"><p>شاید که نگاهش گه کشتن به من افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد تیشه بباید زدنش بر دل هر سنگ</p></div>
<div class="m2"><p>تا سایهٔ شیرین به سر کوه کن افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واقف شود از حالت دل‌های شکسته</p></div>
<div class="m2"><p>هر دل که در آن جعد شکن بر شکن افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمیازه گشاید دهن زخم دلم باز</p></div>
<div class="m2"><p>چون دیده بدان غمزهٔ ناوک فکن افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم که ز زندان سر زلف توام دل</p></div>
<div class="m2"><p>آزاد نگردیده به چاه ذقن افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان دادم و بوسی ز دهان تو گرفتم</p></div>
<div class="m2"><p>فریاد گر این قصه دهن بر دهن افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو بخت بلندی که بر زلف تو یک چند</p></div>
<div class="m2"><p>من بر سر حرف آیم و غیر از سخن افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برخیزد و جان در قدمت بازفشاند</p></div>
<div class="m2"><p>گر چشم تو بر کشتهٔ خونین‌کفن افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صاحب نظری را که به چشم توفتد چشم</p></div>
<div class="m2"><p>حاشا که به دنبال غزال ختن افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذار که بیند قد و روی تو فروغی</p></div>
<div class="m2"><p>تا از نظرش جلوهٔ سرو و سمن افتد</p></div></div>