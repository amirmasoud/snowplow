---
title: >-
    غزل شمارهٔ ۱۷
---
# غزل شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>باعث مردن بلای عشق باشد مرا</p></div>
<div class="m2"><p>راجت جان من آخر آفت جان شد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس او با دل بیمار من الفت گرفت</p></div>
<div class="m2"><p>عاقبت درد محبت عین درمان شد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کو گریبان چاک سازد صبح از این حسرت که باز</p></div>
<div class="m2"><p>مطلع خورشید آن چاک گریبان شد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش پیچیدم به زلفش از پریشان خاطری</p></div>
<div class="m2"><p>عشق کامم داد تا خاطر پریشان شد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت جانی بر نمی‌دارد سر کوی وفا</p></div>
<div class="m2"><p>تا سپردم جان به جانان سختی آسان شد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داستان یوسف گم‌گشته دانستم که چیست</p></div>
<div class="m2"><p>یوسف دل پا در آن چاه زنخدان شد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسرت عشق از دل پر حسرتم خالی نشد</p></div>
<div class="m2"><p>هر چه خون دیده از حسرت به دامان شد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کام دل حاصل نکردم از صبوری ورنه من</p></div>
<div class="m2"><p>صبر کردم در غمش چندان که امکان شد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این تویی یا مشتری یا زهره یا مه یا پری</p></div>
<div class="m2"><p>یا مراد هر دو عالم حاصل جان شد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خانهٔ شهری خراب از حسن شهر آشوب اوست</p></div>
<div class="m2"><p>نی همین تنها فروغی خانه ویران شد مرا</p></div></div>