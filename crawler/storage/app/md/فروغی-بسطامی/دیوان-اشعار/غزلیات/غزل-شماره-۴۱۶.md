---
title: >-
    غزل شمارهٔ ۴۱۶
---
# غزل شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>وقت مرگ آمد ز رحمت بر سر بالین من</p></div>
<div class="m2"><p>تلخ شد کام حسود از مردن شیرین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او پی جور و جفا، من بر سر مهر و وفا</p></div>
<div class="m2"><p>من به فکر مهر او، او در خیال کین من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبری رسم وی و عاشق کشی قانون وی</p></div>
<div class="m2"><p>عاشقی کیش من و حسرت کشی آیین من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاش آن دیر آشنا با خنجر آید بر سرم</p></div>
<div class="m2"><p>تا مگر از دل برآید حسرت دیرین من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنگ شکر تلخ کام از خندهٔ شیرین او</p></div>
<div class="m2"><p>گلبن تر سرخ روی از گریهٔ رنگین من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز صحن گلستان گلهای رنگین می‌دهد</p></div>
<div class="m2"><p>تازه می‌گردد جراحات دل خونین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوش بوسیدم لب نوشین آن مه را به خواب</p></div>
<div class="m2"><p>خواب شیرین چیست تعبیر شب دوشین من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم از نیش جدایی جان من بر لب رسید</p></div>
<div class="m2"><p>گفت سهل است ار شبی بوسی لب نوشین من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم آهنگ جنون دارد دلم، خندید و گفت</p></div>
<div class="m2"><p>بایدش زنجیر کرد از طرهٔ مشکین من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر فروغی دیدن خوبان نبودی در نظر</p></div>
<div class="m2"><p>هیچ عالم را ندیدی چشم عالم بین من</p></div></div>