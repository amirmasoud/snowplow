---
title: >-
    غزل شمارهٔ ۳۳۴
---
# غزل شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>به بوسه‌ای ز دهان تو آرزومندم</p></div>
<div class="m2"><p>فغان که با همه حسرت به هیچ خرسندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از قبیله خوبان سست پیمانی</p></div>
<div class="m2"><p>من از جماعت عشاق سخت پیوندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برید از همه جا دست روزگار مرا</p></div>
<div class="m2"><p>بدین گناه که در گردنت نیفکندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرار شوق تو بر می‌جهد ز هر عضوم</p></div>
<div class="m2"><p>نوای عشق تو سر می‌زند ز هر بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو داغ گذاری چگونه نپذیرم</p></div>
<div class="m2"><p>و گر تو درد فرستی چگونه نپسندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدر علاقه به فرزند خویشتن دارد</p></div>
<div class="m2"><p>من از تعلق روی تو خصم فرزندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه تا نکند خیمه‌ات نمی‌دانی</p></div>
<div class="m2"><p>که من چگونه از آن کوی خیمه برکندم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به راه وعده خلافی نشسته‌ام چندی</p></div>
<div class="m2"><p>که زیر تیغ تغافل نشانده یک چندم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>معاشران همه در بزم پسته می‌شکنند</p></div>
<div class="m2"><p>شکسته دل من از آن پستهٔ شکرخندم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گریه گفتم از آن پسته یک دو بوسم بخش</p></div>
<div class="m2"><p>به خنده گفت مگس کی نشسته بر قندم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز باده دوش مرا توبه داد مفتی شهر</p></div>
<div class="m2"><p>بتان ساده اگر نشکنند سوگندم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نجات داد ملک هر کجا اسیری بود</p></div>
<div class="m2"><p>من از سلاسل زلفش هنوز در بندم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستوده ناصردین شه که از شرف گوید</p></div>
<div class="m2"><p>به هیچ دوره ندید آفتاب مانندم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی سزای ملامت به جز فروغی نیست</p></div>
<div class="m2"><p>که دایم از می و معشوق می‌دهد پندم</p></div></div>