---
title: >-
    غزل شمارهٔ ۴۱۵
---
# غزل شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>گفتم که چیست راهزن عقل و دین من</p></div>
<div class="m2"><p>گفتا که چین زلف و خط عنبرین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که الامان ز دم آتشین من</p></div>
<div class="m2"><p>گفتا که الحذر ز دل آهنین من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که طرف دامن دولت به دست کیست</p></div>
<div class="m2"><p>گفتا به دست آن که گرفت آستین من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که امتحان سعادت به کام کیست</p></div>
<div class="m2"><p>گفتا که کام آن که ببوسد زمین من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که بخت نیک بگو هم قرین کیست</p></div>
<div class="m2"><p>گفتا قرین آن که شود هم نشین من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که بهر چاک گریبان صبح چیست</p></div>
<div class="m2"><p>گفتا ز رشک تابش صبح جبین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که از چه خواجه انجم شد آفتاب</p></div>
<div class="m2"><p>گفتا ز بندگی رخ نازنین من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که ساحری ز که آموخت سامری</p></div>
<div class="m2"><p>گفتا ز چشم کافر سحر آفرین من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم کجاست مسکن دلهای بی قرار</p></div>
<div class="m2"><p>گفتا که جعد خم به خم چین به چین من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم هوای چشمهٔ کوثر به سر مراست</p></div>
<div class="m2"><p>گفتا که شرمی از لب پر انگبین من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم کدام دل به غمت خرمی نخواست</p></div>
<div class="m2"><p>گفتا دل فروغی اندوهگین من</p></div></div>