---
title: >-
    غزل شمارهٔ ۲۳۰
---
# غزل شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>بهل ز صورت خوبت نقاب بردارند</p></div>
<div class="m2"><p>که با وجود تو عشاق نقش دیوارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه خواری عشق تو را به جان بکشم</p></div>
<div class="m2"><p>که پیش روی تو گل های گلستان خوارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خلد خواندمان شیخ شهر و زین غافل</p></div>
<div class="m2"><p>که ساکنان درت از بهشت بیزارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گر به سینه دل سخت آهنین داری</p></div>
<div class="m2"><p>شکستگان تو هم آه آتشین دارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سخت‌گیری ایام هیچ کم نشوند</p></div>
<div class="m2"><p>گرفتگان کمندت ز بس که بسیارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو شادکامی و شهری مسخر غم عشق</p></div>
<div class="m2"><p>تو مست خوابی و خلقی ز غصه بیدارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز بنفشه نروید ز خاک پاکانی</p></div>
<div class="m2"><p>که از طپانچهٔ عشقت کبودرخسارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حساب خون من افتاده است با قومی</p></div>
<div class="m2"><p>که خون بی گنهان را به هیچ نشمارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گناهکار تر از من کسی فروغی نیست</p></div>
<div class="m2"><p>به کیش دولت اگر عاشقان گنه کارند</p></div></div>