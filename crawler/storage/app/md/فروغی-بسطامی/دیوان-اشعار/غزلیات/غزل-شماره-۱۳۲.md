---
title: >-
    غزل شمارهٔ ۱۳۲
---
# غزل شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>یک شب آخر دامن آه سحر خواهم گرفت</p></div>
<div class="m2"><p>داد خود را زان مه بیدادگر خواهم گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم گریان را به توفان بلا خواهم سپرد</p></div>
<div class="m2"><p>نوک مژگان را به خون آب جگر خواهم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعره‌ها خواهم زد و در بحر و بر خواهم فتاد</p></div>
<div class="m2"><p>شعله‌ها خواهم شد و در خشک و تر خواهم گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انتقامم را ز زلفش مو به مو خواهم کشید</p></div>
<div class="m2"><p>آرزویم را ز لعلش سر به سر خواهم گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا به زندان فراقش بی نشان خواهم شدن</p></div>
<div class="m2"><p>یا گریبان وصالش بی خبر خواهم گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا بهار عمر من رو بر خزان خواهد نهاد</p></div>
<div class="m2"><p>یا نهال قامت او را به بر خواهم گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا به پایش نقد جان بی‌گفتگو خواهم فشاند</p></div>
<div class="m2"><p>یا ز دستش آستین بر چشم تر خواهم گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یا به حاجت در برش دست طلب خواهم گشاد</p></div>
<div class="m2"><p>یا به حجت از درش راه سفر خواهم گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا لبانش را ز لب هم‌چون شکر خواهم مکید</p></div>
<div class="m2"><p>یا میانش را به بر هم‌چون کمر خواهم گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر نخواهد داد من امروز داد آن شاه حسن</p></div>
<div class="m2"><p>دامنش فردا به نزد دادگر خواهم گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر سرم قاتل اگر بار دگر خواهد گذشت</p></div>
<div class="m2"><p>زندگی را با دم تیغش ز سر خواهم گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز اگر بر منظرش روزی نظر خواهم فکند</p></div>
<div class="m2"><p>کام چندین ساله را از یک نظر خواهم گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با سر و پای مرا در خاک و خون خواهد کشید</p></div>
<div class="m2"><p>یا به رو دوش ورا در سیم و زر خواهم گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر فروغی ماه من برقع ز رو خواهد فکند</p></div>
<div class="m2"><p>صد هزاران عیب بر شمس و قمر خواهم گرفت</p></div></div>