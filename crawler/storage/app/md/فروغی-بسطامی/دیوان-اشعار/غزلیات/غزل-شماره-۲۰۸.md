---
title: >-
    غزل شمارهٔ ۲۰۸
---
# غزل شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>ز اختران جگرم چند پر شرر ماند</p></div>
<div class="m2"><p>خدا کند که نه خاور نه باختر ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شام گاه قیامت کسی نیندیشد</p></div>
<div class="m2"><p>که در فراق تو یک شام تا سحر ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سر پرده غیب آن کسی خبردار است</p></div>
<div class="m2"><p>که با حضور تو از خویش بی خبر ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی که زد به دو زلف تو لاف یک رنگی</p></div>
<div class="m2"><p>چو نافه غرق به خونابهٔ جگر ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار فتنه ز هر حلقه‌ای برانگیزد</p></div>
<div class="m2"><p>شبی که عقرب زلف تو بر قمر ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلت به سینهٔ سیمین ز سنگ ساخته‌اند</p></div>
<div class="m2"><p>که تیر نالهٔ عشاق بی اثر ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شام زلف تو سر منزل غریبان است</p></div>
<div class="m2"><p>دل غریب من آن به که در سفر ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر اعتقاد به دامان محشر است تو را</p></div>
<div class="m2"><p>مهل که دامنم از خون دیده که ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من از وجود تو غافل نی‌ام در آن غوغا</p></div>
<div class="m2"><p>که بی خبر پدر از حالت پسر ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نارسایی طومار عمر می‌ترسم</p></div>
<div class="m2"><p>که وصف جعد رسای تو مختصر ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فتد به روی تو ای کاش دیده یوسف را</p></div>
<div class="m2"><p>که محو حسن تو در اولین نظر ماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه دانه‌ها که نکشتیم در زمین امید</p></div>
<div class="m2"><p>دریغ و درد گر این کشته بی‌ثمر ماند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خواص باده ز آب حیات بیشتر است</p></div>
<div class="m2"><p>علی‌الخصوص که در شیشه بیشتر ماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن شراب مرا کاسه‌ای بده ساقی</p></div>
<div class="m2"><p>که سر نماند و کیفیتش به سر ماند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پرستش صنمی کن که روی روشن او</p></div>
<div class="m2"><p>برای انور گنجور نامور ماند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستوده خان معیر که در ممالک شاه</p></div>
<div class="m2"><p>به مهر او همه جا گنج معتبر ماند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یگانه گوهر درج شرف حسین علی</p></div>
<div class="m2"><p>که بحر با کف او خالی از گهر ماند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خدا یمین ورا آفریده بهر همین</p></div>
<div class="m2"><p>که زر فشاند و از زر عزیزتر ماند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدم به خاک فروغی نهد پی درمان</p></div>
<div class="m2"><p>به درد عشق جگر خسته‌ای که در ماند</p></div></div>