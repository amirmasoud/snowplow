---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>این چار رباعی از شه تاجور است</p></div>
<div class="m2"><p>کارایش دیوان قضا و قدر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بنویسی دهندهٔ کام دل است</p></div>
<div class="m2"><p>چون بسرایی برندهٔ هوش سر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«امروز سوار اسب رهوار شدم</p></div>
<div class="m2"><p>از بهر شکار سوی کهسار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن قدر به چنگ باز و تیهو آمد</p></div>
<div class="m2"><p>کز کثرت قتلشان در آزار شدم»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«باران ز هوا هم چو سرشکم آید</p></div>
<div class="m2"><p>وز آمدنش به دشت رشکم آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان راه که باریدن باران ز چه روست</p></div>
<div class="m2"><p>آنجا که چو سیل از مژه اشکم آید»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>«دیدار تو دیدنم میسر نشود</p></div>
<div class="m2"><p>هیچم به تو ماه روی رهبر نشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند کز آتش غمت می‌سوزم</p></div>
<div class="m2"><p>لیکن گویم که چون تو دلبر نشود»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«دوری تو کرد زار و رنجور مرا</p></div>
<div class="m2"><p>بی روی تو دیو است کنون حور مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر وصل تو بار دگرم دست دهد</p></div>
<div class="m2"><p>در هر دو جهان بس است منظور مرا»</p></div></div>