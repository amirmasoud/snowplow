---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>شاه جم جاه کلامی که بیان فرماید</p></div>
<div class="m2"><p>از کمال شرفش نقش نگین باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>« دل ما را ز چه رو زار و حزین باید کرد</p></div>
<div class="m2"><p>عاشقی کفر نباشد نه چنین باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بادهٔ صاف به یاران کهن باید کرد</p></div>
<div class="m2"><p>نظر لطف به عشاق غمین باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما گدایان را از درگه خود دور مکن</p></div>
<div class="m2"><p>که ترحم به گدایان به از این باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بر خسته دلان چند به تندی گذری</p></div>
<div class="m2"><p>بعد از این مرکب آهسته به زین باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چنین حسن و لطافت که تو داری تا حشر</p></div>
<div class="m2"><p>سجده بر آدم و حوا و به طین باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرتو روی تو روشن کند این عالم را</p></div>
<div class="m2"><p>پس از این روی تو با ماه قرین باید کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پرده از صورت زیبای تو باید برداشت</p></div>
<div class="m2"><p>ماه رویان همه را پرده‌نشین باید کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو طاووس چو سرمست خرامی در باغ</p></div>
<div class="m2"><p>توتیای مژه را خاک زمین باید کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روش کبک دری داری و چشم آهو</p></div>
<div class="m2"><p>صید این قسم شکاری به کمین باید کرد»</p></div></div>