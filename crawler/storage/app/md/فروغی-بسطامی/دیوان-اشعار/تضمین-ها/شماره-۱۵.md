---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>دوش در میکده با آن صنم قافیه‌دان</p></div>
<div class="m2"><p>خواندم این مطلع شه را و زدم رطل گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«برقع از روی برافکن که همه خلق جهان</p></div>
<div class="m2"><p>به یکی روز ببینند دو خورشید عیان»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخ رخشان بنما، دیدهٔ جان را بفروز</p></div>
<div class="m2"><p>لب میگون بگشا آتش دل را بنشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر خورشید رخت هیچ نگنجد به ضمیر</p></div>
<div class="m2"><p>وصف یاقوت لبت هیچ نیاید به زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلستانی تو ولی از همه دل‌ها به کنار</p></div>
<div class="m2"><p>آفتابی تو ولی از همه ذرات نهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موی عنبر شکنت سلسلهٔ گردن دل</p></div>
<div class="m2"><p>روی خورشیدوشت شعلهٔ عالم جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دستم از حلقهٔ مویت همه شب مشک فروش</p></div>
<div class="m2"><p>چشمم از تابش رویت همه روز اشک افشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>راستی جز خم ابروی تو نشنیدم من</p></div>
<div class="m2"><p>که مه نو بکشد بر سر خورشید کمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من ندیدم ز رخ خوب تو فرخنده‌تری</p></div>
<div class="m2"><p>جز بلند اختر فرخ ملک ملک ستان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتاب فلک جاه ملک ناصردین</p></div>
<div class="m2"><p>که قرینش ملکی نامده در هیچ قران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رفته تا طبع فروغی ز پی مطلع شاه</p></div>
<div class="m2"><p>شعرش افلاک نشین آمد و خورشید نشان</p></div></div>