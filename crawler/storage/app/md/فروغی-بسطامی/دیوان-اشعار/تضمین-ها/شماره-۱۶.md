---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای خامهٔ مشک افشان چون نامه نگار آیی</p></div>
<div class="m2"><p>این مطلع شاهی را عنوان کتابم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>« ای ساقی خوش منظر مست می نابم کن</p></div>
<div class="m2"><p>روی چو مهت بنمای بیهوش و خرابم کن»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیفیت بیداری خون کرد دلم ساقی</p></div>
<div class="m2"><p>برخیز و شرابم ده بنشین و به خوابم کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر وقت که می خواران پیمانهٔ می نوشند</p></div>
<div class="m2"><p>از چشم خمارینت سرمست شرابم کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من زهر فراقت را زین پیش نمی‌نوشم</p></div>
<div class="m2"><p>صهبای وصالم ده، فارغ ز عذابم کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش لب نوشینت تا کی به سؤال آیم</p></div>
<div class="m2"><p>گر بوسه نمی‌بخشی یک باره جوابم کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخساره نشان دادی بی دین و دلم کردی</p></div>
<div class="m2"><p>بگشای خم گیسو بی طاقت و تابم کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی که در این عالم یک عمر کنم شاهی</p></div>
<div class="m2"><p>در خیل غلامانت یک روز حسابم کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ترسم که بر خسرو داد از تو برم آخر</p></div>
<div class="m2"><p>شیرین دهنا رحمی بر چشم پرآبم کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شه ناصردین کز دل پیر فلکش گوید</p></div>
<div class="m2"><p>کز جمله حجابت یک باره خطابم کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صد بار فروغی من با دل بر خود گفتم</p></div>
<div class="m2"><p>بنواز دلم باری آن گاه عتابم کن</p></div></div>