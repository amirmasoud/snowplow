---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>شاه بیت غزل بنده سه بیت از شاه است</p></div>
<div class="m2"><p>که فروزنده‌تر از گوهر شهوار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>« دل من مایل آن لعبت فرخار بود</p></div>
<div class="m2"><p>جان من در ره آن شوخ دل آزار بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف مشکین خم اندر خمش از بوالعجبی</p></div>
<div class="m2"><p>تودهٔ مشک دمد طبلهٔ عطار بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست از خانهٔ خود چون بخرامد بیرون</p></div>
<div class="m2"><p>دل ز دستش برود هر چه که هشیار بود»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم آخر نرسد نوبت خون خواهی من</p></div>
<div class="m2"><p>بس که در ره گذرش کشتهٔ بسیار بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنگ در تار سر زلف بتی باید زد</p></div>
<div class="m2"><p>زان که حیف است کسی این همه بی کار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ره عشق بریزد آن چه تو را دربار است</p></div>
<div class="m2"><p>ره رو کعبه همان به که سبک بار بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به که در پرده بپوشند رخ خوبان را</p></div>
<div class="m2"><p>راز عشاق چرا بر سر بازار بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان خریدار سیه چشم غزالانم من</p></div>
<div class="m2"><p>که غزلهای مرا شاه خریدار بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبب نقطهٔ ایجاد ملک ناصر دین</p></div>
<div class="m2"><p>که مدار فلکش در خط پرگار بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملکا شعر فروغی همه در مدحت توست</p></div>
<div class="m2"><p>که چنین صاحب اشعار گهربار بود</p></div></div>