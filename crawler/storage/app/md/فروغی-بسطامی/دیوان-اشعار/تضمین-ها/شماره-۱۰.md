---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>شیرین دهنا بشنو اشعار خوش خسرو</p></div>
<div class="m2"><p>از چشمهٔ نوشینت یک قطره به کامم کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«من خضر و سکندروار ظلمات نپیمایم</p></div>
<div class="m2"><p>زان آب حیات اینک یک جرعه به جامم کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خوی تو می‌دانم از لطف تو مایوسم</p></div>
<div class="m2"><p>باری ز سر رحمت یک روز عتابم کن»</p></div></div>