---
title: >-
    رباعی شمارهٔ ۱۰
---
# رباعی شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>یک عمر شهان تربیت عیش کنند</p></div>
<div class="m2"><p>تا نیم نفس عیش به صد طیش کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازم به جهان همت درویشان را</p></div>
<div class="m2"><p>کایشان به یکی لقمه دو صد عیش کنند</p></div></div>