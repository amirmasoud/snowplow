---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>از کشت عمل بس است یک خوشه مرا</p></div>
<div class="m2"><p>در روی زمین بس است یک گوشه مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چند چو کاه گرد خرمن گردیم</p></div>
<div class="m2"><p>چون مرغ بس است دانه‌ای توشه مرا</p></div></div>