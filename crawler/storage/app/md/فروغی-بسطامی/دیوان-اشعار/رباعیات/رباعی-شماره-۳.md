---
title: >-
    رباعی شمارهٔ ۳
---
# رباعی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا قبلهٔ ابروی تو ای یار کج است</p></div>
<div class="m2"><p>محراب دل و قبلهٔ احرار کج است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما جانب قبلهٔ دگر رو نکنیم</p></div>
<div class="m2"><p>آن قبله مراست گر چه بسیار کج است</p></div></div>