---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>بگذار که خویش را به زاری بکشم</p></div>
<div class="m2"><p>مپسند که بار شرمساری بکشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دوست به مرگ من به هر حال خوش است</p></div>
<div class="m2"><p>من نیز به مرگ خود به هر حال خوشم</p></div></div>