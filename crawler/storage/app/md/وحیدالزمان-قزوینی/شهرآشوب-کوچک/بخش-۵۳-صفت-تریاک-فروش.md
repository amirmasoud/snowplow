---
title: >-
    بخش ۵۳ - صفت تریاک فروش
---
# بخش ۵۳ - صفت تریاک فروش

<div class="b" id="bn1"><div class="m1"><p>تریاک فروش کیف پرداز</p></div>
<div class="m2"><p>از چُرت بود چو دنگ رزّاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشخاش صفت به باغ دنیا</p></div>
<div class="m2"><p>یک سر دارد هزار سودا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان نشاء که می فزاید ادراک</p></div>
<div class="m2"><p>گردیده دوته مدام چون تاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون... مرد رفته از کار</p></div>
<div class="m2"><p>بوقش نکند ز خواب بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کیفش، بَحری، فراخ پهناست</p></div>
<div class="m2"><p>در چُرت خودش چو موج دریاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی یار ز عیش و برگ گشته</p></div>
<div class="m2"><p>خنثای حیات و مرگ گشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی نکته سروده نی شنفته</p></div>
<div class="m2"><p>چون یا دایم نشسته خُفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چُرت گرفته شکل محراب</p></div>
<div class="m2"><p>پیوسته نماز کرده در خواب</p></div></div>