---
title: >-
    بخش ۳ - صفت نعل بندان
---
# بخش ۳ - صفت نعل بندان

<div class="b" id="bn1"><div class="m1"><p>آن سخت دلان که خود پسندند</p></div>
<div class="m2"><p>بیجاده لبان نعل بندند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی چو آفتاب ایشان</p></div>
<div class="m2"><p>هر نعل چو ماه گشته تابان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز پرتو آن رخ گذاره</p></div>
<div class="m2"><p>هر مه را، چشم پر ستاره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق ز جمال شان مشوش</p></div>
<div class="m2"><p>نعلش ز فروغ شان در آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته ز فروغ روی ایشان</p></div>
<div class="m2"><p>هر میخ فتیله ی فروزان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروانه زده زبیمِ تو، بیخ</p></div>
<div class="m2"><p>گاهی بر نعل و گاه بر میخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چکّش به دکّان ز واله هان است</p></div>
<div class="m2"><p>انگشت ز دسته در دهان است</p></div></div>