---
title: >-
    بخش ۱ - تعریف شهر اصفهان
---
# بخش ۱ - تعریف شهر اصفهان

<div class="b" id="bn1"><div class="m1"><p>بنمود سوادِ شهری از دور</p></div>
<div class="m2"><p>مانند سوادِ دیده پر نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهری همه خانه هاش پر زر</p></div>
<div class="m2"><p>چون کاخِ خیالِ کیمیاگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دل همه خانه ها شمالی</p></div>
<div class="m2"><p>هر یک چو بنای چرخ عالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانند نهال گل، خیابان</p></div>
<div class="m2"><p>چون گل در خانه هاش خندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرون ز شمار مخزن گنج</p></div>
<div class="m2"><p>چون تصفیف بیوت شطرنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیّار ز خانه ها به صد سال</p></div>
<div class="m2"><p>بیرون نرود چو فکر رمّال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سبزی کشور و بلادش</p></div>
<div class="m2"><p>سر سبز چو سرو گرد بادش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر شاخ درخت، مرغ رنگین</p></div>
<div class="m2"><p>چون شمع، گشوده بال زرّین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگرفته ز سبزه در جنابش</p></div>
<div class="m2"><p>آیینه به موم سبز آبش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بنموده ز روشنایی آب</p></div>
<div class="m2"><p>هر قطره به شب چو کرم شب تاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آسان گردد، به طرف گلشن</p></div>
<div class="m2"><p>شمع، از آتش، چو لاله روشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آبش به صفایِ روح، غلطان</p></div>
<div class="m2"><p>جان بخش به رنگ آب حیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گندم چو دواند ریشه زان نم</p></div>
<div class="m2"><p>جان یافت چو عنکبوت در دَم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اجساد ز زندگی، بریده</p></div>
<div class="m2"><p>در خاک چو ریشه قد کشیده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر واله ی لاله ی بهاری</p></div>
<div class="m2"><p>مشغول شدی به گل شماری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشتیش آن حنا نهاده زان بام</p></div>
<div class="m2"><p>در کام زبان چو مغز بادام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنموده مناره های پُر فر</p></div>
<div class="m2"><p>همچون علمِ از میان لشکر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید و مه از فراز آنها</p></div>
<div class="m2"><p>چون سر عَلَم از عَلَم هویدا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر برجی را نموده از سر</p></div>
<div class="m2"><p>این چرخ کبود چون کبوتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در چار حدش بیوت مردم</p></div>
<div class="m2"><p>کز دیدن او شود نگه گم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بنموده به چشم اهل انصاف</p></div>
<div class="m2"><p>چون جوجه ی ماکیان ز اطراف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بازار و دکانش از عدد بیش</p></div>
<div class="m2"><p>هر صنفی از و محبت اندیش</p></div></div>