---
title: >-
    بخش ۸ - صفت بقّال
---
# بخش ۸ - صفت بقّال

<div class="b" id="bn1"><div class="m1"><p>فریاد ز حسنِ شوخ بقال</p></div>
<div class="m2"><p>وان خط ّ سیاه و چهره ی آل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ز آتش آن جمال پر نور</p></div>
<div class="m2"><p>پر آبله شد چو تفت انگور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از داغ نو و کهن دل ریش</p></div>
<div class="m2"><p>پُر گشت چو دخل آن جفا کیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنگ من و او چو اهل فرهنگ</p></div>
<div class="m2"><p>شد حلقه به گوش آن دل سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون جگرم به این فسانه</p></div>
<div class="m2"><p>خورد آن خط سبز هِندُوانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ارزان باشد به نقد صد جان</p></div>
<div class="m2"><p>بوییدن سیب آن زنخدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط سبزش ز نُور سُوره است</p></div>
<div class="m2"><p>در دیده چو توتیای غوره است</p></div></div>