---
title: >-
    بخش ۳۸ - صفت طبّاخ
---
# بخش ۳۸ - صفت طبّاخ

<div class="b" id="bn1"><div class="m1"><p>طبّاخ ز پختگی مرا سوخت</p></div>
<div class="m2"><p>از سوختنم رخش بر افروخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست از خط سبز آن گرامی</p></div>
<div class="m2"><p>صبحم، که تعب نموده شامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل در بر و من ز حیرت او</p></div>
<div class="m2"><p>هر لحظه کنم فغان که کوکو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم چشمی به روی جانان</p></div>
<div class="m2"><p>چون چشم پیاز حلقه، حیران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوز دلم از رقیب قلاش</p></div>
<div class="m2"><p>همچون مگسِ فتاده بر آش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دود، دلم شدست گریان</p></div>
<div class="m2"><p>من چون نشوم کباب بریان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سینه ی من دل مشوش</p></div>
<div class="m2"><p>کز دوری او بود در آتش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نالان شده، اشک چون چکیده</p></div>
<div class="m2"><p>چون روغنِ داغِ آب دیده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر گاه نفس کشم دهد بو</p></div>
<div class="m2"><p>دم پخت دلم ز آتش او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از حسرت آن عذار گل پوش</p></div>
<div class="m2"><p>باشد دل من چو دیگ در جوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر یک به زبان تُرک و تاجیک</p></div>
<div class="m2"><p>چون شعله بود بزیر آن دیگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل را افزود، از فغان درد</p></div>
<div class="m2"><p>این آش نگشت از نفس سرد</p></div></div>