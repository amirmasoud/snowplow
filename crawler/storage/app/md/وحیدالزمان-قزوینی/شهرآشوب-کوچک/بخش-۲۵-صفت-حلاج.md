---
title: >-
    بخش ۲۵ - صفت حلّاج
---
# بخش ۲۵ - صفت حلّاج

<div class="b" id="bn1"><div class="m1"><p>حلّاجانند چشم بد دُور</p></div>
<div class="m2"><p>هر یک شاهی ولیک منصور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستند ز خط، تذرو خوش فال</p></div>
<div class="m2"><p>وز خال سیه، غزال خوش حال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چک در کف شان کسی که دیده</p></div>
<div class="m2"><p>خون آب ز دیده اش چکیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشّاق ازین بتان که نغزند</p></div>
<div class="m2"><p>چون جَوزَقَه جمله خشک مغزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارند ز مغز خشک بنیاد</p></div>
<div class="m2"><p>مانند کمان ز پنبه فریاد</p></div></div>