---
title: >-
    بخش ۲۰ - صفت حدّاد
---
# بخش ۲۰ - صفت حدّاد

<div class="b" id="bn1"><div class="m1"><p>حداد خبر ندارد از درد</p></div>
<div class="m2"><p>بیهوده چه کوبم آهن سرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش خواهم زدن به عالم</p></div>
<div class="m2"><p>در کوره ی عشقِ یار چون دم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثابت قدمم بکوی جانان</p></div>
<div class="m2"><p>گر پتک بسر خورم چو سندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد شکر که تا اسیر اویم</p></div>
<div class="m2"><p>چون آهن تفته سرخ رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد دل سخت آن پریوش</p></div>
<div class="m2"><p>چون آهن تفته ی در آتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باشد، در چشم گریه ی من</p></div>
<div class="m2"><p>آبی که در او نهند آهن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرم است سرشک چشم بیخواب</p></div>
<div class="m2"><p>از آهن اوست جوش این آب</p></div></div>