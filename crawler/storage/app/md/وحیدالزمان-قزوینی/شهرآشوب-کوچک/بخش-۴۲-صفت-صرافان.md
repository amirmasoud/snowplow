---
title: >-
    بخش ۴۲ - صفت صرافان
---
# بخش ۴۲ - صفت صرافان

<div class="b" id="bn1"><div class="m1"><p>از غش پاکست چون عیارم</p></div>
<div class="m2"><p>با صّرافان فتاد کارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمن شده داغ دل ز اطراف</p></div>
<div class="m2"><p>چون نقد بروی نطع صّراف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست از خط زخم، پود و تارم</p></div>
<div class="m2"><p>سنگِ محکِ جفایِ یارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را شده صبر اگر چه روپوش</p></div>
<div class="m2"><p>رسوا شده ام چو نقد مغشوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل داده ی عشق تا نزار است</p></div>
<div class="m2"><p>طبعش با ضعف سازگار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فربه چو شود چو بدره ی زر</p></div>
<div class="m2"><p>در دیده ز فربهی ست لاغر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردد پیدا ز پهلوی او</p></div>
<div class="m2"><p>از فربهی استخوان پهلو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشید که شرح هجر من خواند</p></div>
<div class="m2"><p>ایام مرا ورق چو گرداند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد تیره ازین سیاه کاری</p></div>
<div class="m2"><p>چون دست به وقت زر شماری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از شب روزم نموده صد نیم</p></div>
<div class="m2"><p>زان گونه که بر محک خط از سیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای جان جهان که جور کوشی</p></div>
<div class="m2"><p>بهر چه ز گفتگو خموشی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با جور کشان بود به ابرو</p></div>
<div class="m2"><p>دایم سخن تو چون ترازو</p></div></div>