---
title: >-
    بخش ۴۳ - صفت حکاک
---
# بخش ۴۳ - صفت حکاک

<div class="b" id="bn1"><div class="m1"><p>حکّاک نظر به سویم افکند</p></div>
<div class="m2"><p>مانند نگین، دل مرا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیدن روی آن جفا کیش</p></div>
<div class="m2"><p>دل کنده شدم ز هستی خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن طفل ز بس که شرمگین است</p></div>
<div class="m2"><p>چون گل رنگش نگین نگین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشکیده از آن نگار موزون</p></div>
<div class="m2"><p>مانند عقیق در تنم خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانند نگین ازان گل اندام</p></div>
<div class="m2"><p>هر گُم نامیست صاحب نام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورد است دل اسیر بیتاب</p></div>
<div class="m2"><p>از جوی خط عقیق او آب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این باغ شکفته است بی نم</p></div>
<div class="m2"><p>همچون گل و برگِ نقش خاتم</p></div></div>