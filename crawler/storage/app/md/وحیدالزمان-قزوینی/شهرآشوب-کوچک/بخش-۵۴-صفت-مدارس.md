---
title: >-
    بخش ۵۴ - صفت مَدارِس
---
# بخش ۵۴ - صفت مَدارِس

<div class="b" id="bn1"><div class="m1"><p>دیدم چو صفای این مدارس</p></div>
<div class="m2"><p>افتاد رهم سویِ مدارس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنجا که همیشه باد آباد</p></div>
<div class="m2"><p>سوی فقها گذارم افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدم که دُر کلام می سُفت</p></div>
<div class="m2"><p>وعظی پی خاص و عام می گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کز عشوه ی نو خطان چون ماه</p></div>
<div class="m2"><p>از راه مرو میفت در چاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون سطرِ کتاب چند ازین سور</p></div>
<div class="m2"><p>در تن باشد رکت صف مور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشغولی خَطّ نفس صرعست</p></div>
<div class="m2"><p>اِنکشت زیاد دست شرعست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کامی که بُوَد ز زن بجوئید</p></div>
<div class="m2"><p>ار خوش پسران سخن نگویید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن سو نکنید زین ره آهنگ</p></div>
<div class="m2"><p>اندیشه کنید ازین ره تنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دختر رز که گل نگار است</p></div>
<div class="m2"><p>در حکم زنان حیض دار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>معشوقِ قمار سخت بد خوست</p></div>
<div class="m2"><p>با خلق چو کعبتین شش روست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حرفی که ز کذبش آب و تابست</p></div>
<div class="m2"><p>چون طایر آشیان خرابست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در هیچ ضمیر مسکنش نیست</p></div>
<div class="m2"><p>در کاخ دلی نشیمنش نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زین وعظ چو مستفید گشتم</p></div>
<div class="m2"><p>رندانه از آن مکان گذشتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فیض دگرم نصیب گردید</p></div>
<div class="m2"><p>علم ادبم ادیب گردید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>معشوقِ قمار سخت بد خوست</p></div>
<div class="m2"><p>با خلق چو کعبتین شش روست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حرفی که ز کذبش آب و تابست</p></div>
<div class="m2"><p>چون طایر آشیان خرابست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در هیچ ضمیر مسکنش نیست</p></div>
<div class="m2"><p>در کاخ دلی نشیمنش نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زین وعظ چو مستفید گشتم</p></div>
<div class="m2"><p>رندانه از آن مکان گذشتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فیض دگرم نصیب گردید</p></div>
<div class="m2"><p>علم ادبم ادیب گردید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آخوند دُر کلام می سفت</p></div>
<div class="m2"><p>فصلی ز حدیث وصل می گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتم درِ وصلِ عیش اگر هست</p></div>
<div class="m2"><p>آن از متعلقات فَعلَست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این عشق خزان بار و برگست</p></div>
<div class="m2"><p>یا آنکه کنایه ای ز مرگست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دارم دلکی ز مرگ مشعوف</p></div>
<div class="m2"><p>چون، محکومُ علیه، محذوف</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حسرت که لبم نموده پاره</p></div>
<div class="m2"><p>از بوسه ی اوست استعاره</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نتوان به کنایه کرد تصریح</p></div>
<div class="m2"><p>این گریه من بسست ترشیح</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یکبار ندیده ام درین تیه</p></div>
<div class="m2"><p>ماهی چو رخش بچشم تشبیه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی دوست ز دوزخم نشانه</p></div>
<div class="m2"><p>جامع ... است در میانه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من تشنه کلام تست چون آب</p></div>
<div class="m2"><p>ایجاز مکن بوقت اطناب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرّ دل من دُرِّ نَسُفته است</p></div>
<div class="m2"><p>این حرف نگفتنی نگفته است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بویی که درین سخن ز راز است</p></div>
<div class="m2"><p>چون نصب قرینه ی مجاز است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جان من و درد دوست با هم</p></div>
<div class="m2"><p>گردیده یکی چو حرف مدغم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر دل که شد است محو جانان</p></div>
<div class="m2"><p>هجران و وصال هست یکسان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باکیم ز روز هجر و شب نِی</p></div>
<div class="m2"><p>بی تغییرم چو اسم مَبنِی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در عشق ز ناز و عشوه ی او</p></div>
<div class="m2"><p>دانم پس از این چه می دهد رو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آینده مضارعیست مجزوم</p></div>
<div class="m2"><p>بر من چو گذشته هست معلوم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز آن روز که دوستیست کارم</p></div>
<div class="m2"><p>با خلق ز بس که سازگارم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باشد از من بنای تألیف</p></div>
<div class="m2"><p>همچون مصدر بوقت تصریف</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون دل، دادِ سخنوری داد</p></div>
<div class="m2"><p>سوی متکلمین شدم شاد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گاهی تصریح و گه به تعریض</p></div>
<div class="m2"><p>گفتند سخن ز جبر و تفویض</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من هم رفتم ازین فسانه</p></div>
<div class="m2"><p>چون حد وسط در آن میانه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در مجلسشان دلم گُهر سُفت</p></div>
<div class="m2"><p>با تفویضی سخن چنین گفت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نیکو نبود فتادن ای خام</p></div>
<div class="m2"><p>زین بس، رفتن، ز آن سوی بام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ما بسته ی عشق یار خویشیم</p></div>
<div class="m2"><p>مجبور به اختیار خویشیم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زان قوم چو آمدم به خود باز</p></div>
<div class="m2"><p>با منطقیان شدم سخن ساز</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چون دُر که سفر کند ز عمّان</p></div>
<div class="m2"><p>افتاد گذار من به میزان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جُستند چو از سر عنایت</p></div>
<div class="m2"><p>از منطق عاشقان حکایت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گفتم حرفی که دل شمارد</p></div>
<div class="m2"><p>هر چند نتیجه ای ندارد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صُغرای آن طفلِ سرو قامت</p></div>
<div class="m2"><p>باشد کبرای آن قیامت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>این هر دو به نزد صاحب دید</p></div>
<div class="m2"><p>آشوب جهان نتیجه بخشید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر چند که دیده ها دَویده</p></div>
<div class="m2"><p>زین سان شکلی دگر ندیده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از عاشق آن نگار جانی</p></div>
<div class="m2"><p>دور است قیاس اقترانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>دل پروانه است و یار شمعست</p></div>
<div class="m2"><p>این دوری ما ز منع جمع است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نتوان به شب فراق آن ماه</p></div>
<div class="m2"><p>خالی بودن ز اشک و از آه</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بحث از منطق چو گشت کوتاه</p></div>
<div class="m2"><p>افتاد به باغ حکمتم راه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رفتیم در آن مکان خرسند</p></div>
<div class="m2"><p>با مشّایی سراسری چند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گفتیم سخن نهان و پیدا</p></div>
<div class="m2"><p>از جسم و ز صورت و هیولی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از جوهر فرد کرد چون یاد</p></div>
<div class="m2"><p>سرِّ دهنش بیادم افتاد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چون حرف ز خط جوهری گفت</p></div>
<div class="m2"><p>دل از غم آن میان بر آشفت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گفتم آن به، که نفس مرتاض</p></div>
<div class="m2"><p>دایم کند از جواهر اعراض</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زین پس زشتست اگر کنی سر</p></div>
<div class="m2"><p>یک حرف ازین مقوله دیگر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گفتا که دل تو بی ملال است</p></div>
<div class="m2"><p>گفتم &quot;خامش! خلا! محال است&quot;</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گفتا که ملال را چه حالست</p></div>
<div class="m2"><p>و آن چیست که نام او ملال است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>گفتم چیست شرح این اسم</p></div>
<div class="m2"><p>از قسمت لاتناهی جسم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ره پیش ز کوشش کم تست</p></div>
<div class="m2"><p>این باب فراز سلّم تست</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون حق کلام یافت احقاق</p></div>
<div class="m2"><p>برخورد به من حکیم اشراق</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گردید فتیله ی زبانش</p></div>
<div class="m2"><p>روشن چون شمع از بیانش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>سر زد زایش صباح اظهار</p></div>
<div class="m2"><p>از نور نخست و نور انوار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>گفتم باشد چو شام دیجور</p></div>
<div class="m2"><p>از جهل تو این تعدّد نور</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آن لحظه که صبح علم خندید</p></div>
<div class="m2"><p>این جمله یکی شود چو خورشید</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هستی از جهل طبع، واهی</p></div>
<div class="m2"><p>با این همه نور در سیاهی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>افتاد چو یافت بحث &quot;تقریر&quot;</p></div>
<div class="m2"><p>راهم به مدرّسین تفسیر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>گفتم ز برای اهل عرفان</p></div>
<div class="m2"><p>علمی نبود چو علم قرآن</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ارباب دل این طریق پویند</p></div>
<div class="m2"><p>زین باب دوای درد جویند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وین رقّاصان، نام صوفی</p></div>
<div class="m2"><p>یا نقطویند یا حروفی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>مردان نکنند چون زنان رقص</p></div>
<div class="m2"><p>رقص است ز مرد سر به سر نقص</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>این قوم ز رقص اختراعی</p></div>
<div class="m2"><p>هستند مؤنث سماعی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بر درگهِ عدل شامل او</p></div>
<div class="m2"><p>از ضعف بود همیشه نیرو</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>زان سوره ی نَمل را به قرآن</p></div>
<div class="m2"><p>موری بگرفت از سلیمان</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>هر حرف که با زر است توأم</p></div>
<div class="m2"><p>بر هر سخنی بود مقدم</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>گردد به تو این حدیث آسان</p></div>
<div class="m2"><p>از اسم سُوَر بلوح قرآن</p></div></div>