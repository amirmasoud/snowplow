---
title: >-
    بخش ۳۰ - صفت کلاه دوز
---
# بخش ۳۰ - صفت کلاه دوز

<div class="b" id="bn1"><div class="m1"><p>سرگرم کلاه هر که گردید</p></div>
<div class="m2"><p>هر دیده وری که روی او دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیدن آن رخ چو ماهم</p></div>
<div class="m2"><p>رقصان چو حباب شد کلاهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده است زهرچه هست اعراض</p></div>
<div class="m2"><p>وز جمله بریده غیر مقراض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقراض صفت به دست، مشکل</p></div>
<div class="m2"><p>آیند دو هم زبان یک دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان جان جهان و نور دیده</p></div>
<div class="m2"><p>پیوسته مرا به دل خلیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ناز و عتاب و تندی خو</p></div>
<div class="m2"><p>هر حرف چو سوزن سه پهلو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بهر جمال یار دیدن</p></div>
<div class="m2"><p>وندر ره وصل او دویدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ضعف بدن نمانده از من</p></div>
<div class="m2"><p>جز چشم و قدم برنکِ سوزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پرتو روی همچو ماهش</p></div>
<div class="m2"><p>بنموده به دیده ها کلاهش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم ابره ی آستر مصفا</p></div>
<div class="m2"><p>مانند گل دو رنگ رعنا</p></div></div>