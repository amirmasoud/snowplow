---
title: >-
    بخش ۴۵ - صفت اتوکش
---
# بخش ۴۵ - صفت اتوکش

<div class="b" id="bn1"><div class="m1"><p>تا چند ز دوری اتوکش</p></div>
<div class="m2"><p>باشم چو اتو میان آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر روی دهد گذارمش پیش</p></div>
<div class="m2"><p>مانند خم اتو سر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانان چو در آتشم دهد جای</p></div>
<div class="m2"><p>مانند اتو ز سر کنم پای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پرتو آفتاب رویش</p></div>
<div class="m2"><p>پر باله بود خم اتویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم چو اتو به راه او من</p></div>
<div class="m2"><p>در پا شب و روز کفش آهن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پهلو استخوان خزیده</p></div>
<div class="m2"><p>چون تافته ی اتو کشیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خم از نگهش به گاه دیدن</p></div>
<div class="m2"><p>چون انگشتانه شد ز سوزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی هم چو اتو به او رسم من</p></div>
<div class="m2"><p>گر کفش و عصا کنم ز آهن</p></div></div>