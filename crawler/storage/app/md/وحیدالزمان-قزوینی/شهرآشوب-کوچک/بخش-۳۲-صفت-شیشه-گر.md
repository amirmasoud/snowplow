---
title: >-
    بخش ۳۲ - صفت شیشه گر
---
# بخش ۳۲ - صفت شیشه گر

<div class="b" id="bn1"><div class="m1"><p>بر شیشه گران گذارم افتاد</p></div>
<div class="m2"><p>آن جا دل خسته رفت بر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سینه ام آن غریب بگریخت</p></div>
<div class="m2"><p>هم جنس بدید و در وی آویخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند کبوتری که پرّید</p></div>
<div class="m2"><p>شد داخل کلّه بر نگردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این شیشه شکست از جدایی</p></div>
<div class="m2"><p>دارد ز گداز مومیائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن غیرت مهر و رشک مهتاب</p></div>
<div class="m2"><p>از بس حُسنش فتاده سیراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آبله شیشه ز آتش تیز</p></div>
<div class="m2"><p>آید بیرون ز آب لبریز</p></div></div>