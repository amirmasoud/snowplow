---
title: >-
    بخش ۲۲ - صفت تخمه فروش
---
# بخش ۲۲ - صفت تخمه فروش

<div class="b" id="bn1"><div class="m1"><p>از تخمه فروش و آن لب شور</p></div>
<div class="m2"><p>شد دیده ام آشیان زنبور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیدن روی آن فرشته</p></div>
<div class="m2"><p>بر آتش غم شدم برشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیدن آن نگار ساده</p></div>
<div class="m2"><p>چون پوست ز تخمه ام فتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد دل این اسیر حیران</p></div>
<div class="m2"><p>در تابه ی غم چو تخمه خندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کف دل این خراب خسته</p></div>
<div class="m2"><p>جَسته است چو تخمه ی شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیند همه عمر خاک مالی</p></div>
<div class="m2"><p>آنرا که بود دو دست خالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد فکر ازو مراست در سر</p></div>
<div class="m2"><p>چون تخم که در کدوست مُضمر</p></div></div>