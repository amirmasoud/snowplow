---
title: >-
    شمارهٔ ۲ - در نعت حضرت ختمی مرتبت موید مجتبی محمد مصطفی (ص)
---
# شمارهٔ ۲ - در نعت حضرت ختمی مرتبت موید مجتبی محمد مصطفی (ص)

<div class="b" id="bn1"><div class="m1"><p>ای مایه امید دل ای رحمت خدا</p></div>
<div class="m2"><p>ای اولین تجلی حق ختم انبیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کنیت مقدس و اسماء اقدست</p></div>
<div class="m2"><p>بوالقاسم و محمد و محمود و مصطفا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آید چو نام نامی جان پرورت بگوش</p></div>
<div class="m2"><p>در دل مقام خویش دهد خوف بر رجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود از تو بس شگفت اگر سایه داشتی</p></div>
<div class="m2"><p>خورشید را بلی نبود سایه در قفا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردید ختم بر تو نبوت که سعی تو</p></div>
<div class="m2"><p>از ابتدا رساند مکارم به انتها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موسی چو از جهان بسرای دگر شتافت</p></div>
<div class="m2"><p>طی گشت قصهٔ یدبیضا و اژدها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیسی چو جای در فلک چارمین گرفت</p></div>
<div class="m2"><p>روی زمین نماند از او معجزی بجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قرآن توست معجز باقی که تا ابد</p></div>
<div class="m2"><p>بر جن و انس روز و شبان میزند صلا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوید هر آنکه را که ز من هست شک و ریب</p></div>
<div class="m2"><p>یک سوره آورد چو من از فرو از بها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برهان خاتمیتت این بس که تا بحشر</p></div>
<div class="m2"><p>در کاینات هست طنین افکن این ندا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون لانبی بعدیت آید بگوش دل</p></div>
<div class="m2"><p>آساید از فسانه ارباب ادعا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در رفع اختلاف جهانی توان نمود</p></div>
<div class="m2"><p>بر یک حدیث متقن ثقلینت اکتفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صورت نهفتی از ره معنی درآمدی</p></div>
<div class="m2"><p>در صورت مقدس قرآن و اوصیا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قرآن تست روح تو و تا جهان بپاست</p></div>
<div class="m2"><p>بر اهل روزگار دلیل است و رهنما</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جسم تواند عترت و هرگز نمیشود</p></div>
<div class="m2"><p>این روح و جسم تا ابد از یکدیگر جدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آری چگونه روح تواند بدون جسم</p></div>
<div class="m2"><p>اندر زمانه زیست کند تا صف جزا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این هر دو خود توئی و به تحقیق بر تو رفت</p></div>
<div class="m2"><p>بر این دو از هر آنکه وفا رفت یا جفا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن حجه دوازدهم حالیا بغیب</p></div>
<div class="m2"><p>باشد شریک و حافظ قرآنت از وفا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن هم توئی به صورت و معنی که شخص او</p></div>
<div class="m2"><p>با اسم و کینه تو کند جلوه ز اختفا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ترویج دین تست بدست وی و شود</p></div>
<div class="m2"><p>در عهد او حقیقت اسلام بر ملا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون بهر دفع ظالم و ظلم آن ولی حق</p></div>
<div class="m2"><p>بر بام کعبه برزند از عدل حق لوا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گیتی بعدل و داد گراید ز جور و ظلم</p></div>
<div class="m2"><p>زحمت شود رفاه و کدورت شود صفا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آنگه دلیل خلق کتاب تو است و بس</p></div>
<div class="m2"><p>در امر و نهی کار برآید بمدعا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گردد پدید سلطنت حقه در جهان</p></div>
<div class="m2"><p>یابد ظهور معدلت ذات کبریا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنروز از سپید و سیه خلق عالمند</p></div>
<div class="m2"><p>قائل بیک کتاب و بیک دین و یک خدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گردد جهان بهشت برین زان یگانگی</p></div>
<div class="m2"><p>آری یکانگی کند این دردها دوا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای صاحب شریعت و ای خواجه رسل</p></div>
<div class="m2"><p>ای آسمان رفعت و خورشید اهتدا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از شر غیر و وسوسه نفس و هول حشر</p></div>
<div class="m2"><p>دارد صغیر بر تو و آل تو التجا</p></div></div>