---
title: >-
    شمارهٔ ۱۴ - در مدح قاتل کفار حیدر کرار حضرت علی ابن ابیطالب علی علیه‌السلام
---
# شمارهٔ ۱۴ - در مدح قاتل کفار حیدر کرار حضرت علی ابن ابیطالب علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>مقصود ز آفرینش کون و مکان علیست</p></div>
<div class="m2"><p>کون و مکان چو جسم و در آنجسم جان علیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرمان بر خدای احد آنکه می‌دهد</p></div>
<div class="m2"><p>فرمان به هفت اختر و نه آسمان علیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگر کمال و فضل که در هر کمال و فضل</p></div>
<div class="m2"><p>هرکس مقدم است مقدم بر آن علیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار و معین آدم و نوح آنکه آدمش</p></div>
<div class="m2"><p>چون نوح ملتجی شده بر آستان علیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها همین نه قاسم ارزاق مرتضی است</p></div>
<div class="m2"><p>کاندر جزا قسیم جحیم و جنان علیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهی که روشن است علو مقام او</p></div>
<div class="m2"><p>چون آفتاب بر همه خلق جهان علیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>استاد جبرئیل که بر آستان وی</p></div>
<div class="m2"><p>از شوق جبرئیل بود پاسبان علیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنشاه انس و جان که ز خلاق انس و جان</p></div>
<div class="m2"><p>واجب ولای او شده بر انس و جان علیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانای هر لسان که بوصف جلال او</p></div>
<div class="m2"><p>الکن بود ز خلق دو عالم لسان علیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن حی لایموت که در یکدم از دمی</p></div>
<div class="m2"><p>بخشد بصد چو عیسی مریم روان علیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای آنکه در دو کون تو را باید ایمنی</p></div>
<div class="m2"><p>سوی علی شتاب که حصن امان علیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن نیک و بدشناس که باشد ولای او</p></div>
<div class="m2"><p>از بهر نیک و بد محک و امتحان علیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنکس که مصطفی شب معراج هر طرف</p></div>
<div class="m2"><p>بنمود رو بدید جمالش عیان علیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این نکته فاش بشنو و در فاش و در نهان</p></div>
<div class="m2"><p>غیر از علی مجوی که فاش و نهان علیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن بندگان خاص که نقل مکان کنند</p></div>
<div class="m2"><p>بینند خود که پادشه لامکان علیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در عرش و فرش و خلوت و جلوت بمصطفی</p></div>
<div class="m2"><p>یار و انیس و هم سخن و هم زبان علیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عرش آستان شهی که پی بوسه درش</p></div>
<div class="m2"><p>چرخ بلند را شده قامت کمان علیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر خضر ره به گمشده‌گان میدهد نشان</p></div>
<div class="m2"><p>بی‌شک بخضر آنکه دهدره نشان علیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن سروری که بر نبی اندر غدیرخم</p></div>
<div class="m2"><p>نازل شدیش آیت بلغ بشان علیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرمود مصطفی که در امت ز بعد من</p></div>
<div class="m2"><p>مولی بخاص و عام و به پیر و جوان علیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای ناتوان توان ز علی ولی طلب</p></div>
<div class="m2"><p>کز راه لطف یاور هر ناتوان علیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوینده سلونی و قائل به لو کشف</p></div>
<div class="m2"><p>عالم بهر ضمیر شه غیب دان علیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن صاحب جلال که وصف جلال او</p></div>
<div class="m2"><p>ناید بدرک و فهم و خیال و گمان علیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شاهی که بر صغیر عطا کرده از کرم</p></div>
<div class="m2"><p>در مدح خویش قوه نطق و بیان علیست</p></div></div>