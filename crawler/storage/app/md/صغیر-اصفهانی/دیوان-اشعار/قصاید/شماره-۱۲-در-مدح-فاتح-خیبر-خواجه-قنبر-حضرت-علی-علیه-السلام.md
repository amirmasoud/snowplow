---
title: >-
    شمارهٔ ۱۲ - در مدح فاتح خیبر خواجه قنبر حضرت علی علیه‌السلام
---
# شمارهٔ ۱۲ - در مدح فاتح خیبر خواجه قنبر حضرت علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>با وجود اینکه دارد طوبی و کوثر بهشت</p></div>
<div class="m2"><p>از تو با این قامت و لب کی بود خوشتر بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست هرگز دلگشاتر از بهشت عارضت</p></div>
<div class="m2"><p>هرچه باشد دلکش و جانبخش و جانپرور بهشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری از قد طوبی از رخ حور و از رخ سلسبیل</p></div>
<div class="m2"><p>ای بهشتت خاکپا هستی ز پا تا سر بهشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میکند کسب ای بهشتی روز خاک پای تو</p></div>
<div class="m2"><p>آنچه می‌گویند دارد زینت و زیور بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش کی بر وعده فردای زاهد میدهم</p></div>
<div class="m2"><p>من که امروز از جمالت دارم اندر بر بهشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خیالت در بهشتم نبودم در دل غمی</p></div>
<div class="m2"><p>آری آری غم ندارد هرکه باشد در بهشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن بهشت آید بکارش کز تو افتاده است دور</p></div>
<div class="m2"><p>من تو را دارم چه کار آید مرا دیگر بهشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت زاهد ترک دلبر گو بهشت آور بدست</p></div>
<div class="m2"><p>گفتمش هرگز نخواهم بی‌رخ دلبر بهشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدم اول قامتت زان پس رخت صد شکر من</p></div>
<div class="m2"><p>طی نمودم چون قیامت را رسیدم بر بهشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذکر رخسارت بهر محضر رودای حوروش</p></div>
<div class="m2"><p>بس سخن دارد طراوت گردد آنمحضر بهشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر قصور خویشتن البته گردد معترف</p></div>
<div class="m2"><p>گر تو را گردد قرین با چهره انور بهشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با سر کوی تواش روزی اگر نسبت دهند</p></div>
<div class="m2"><p>بهر خود کی این شرافت را کند باور بهشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر نه گریانست از هجر بهشت عارضت</p></div>
<div class="m2"><p>دارد از تسنیم کوثر از چه چشم تر بهشت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بتابد ز آتشین روی تو بر وی پرتوی</p></div>
<div class="m2"><p>آنچنان سوزد که گردد تل خاکستر بهشت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایکه چون خویت ندارد آتش سوزان جحیم</p></div>
<div class="m2"><p>وی که چون رویت ندارد لاله احمر بهشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست دوزخ در دل اژدر تو عارض زیر زلف</p></div>
<div class="m2"><p>کردهٔی پنهان و داری در دل اژدر بهشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیشت ار نام بهشت آرم مرنج از من از آنک</p></div>
<div class="m2"><p>هست جای دوستان ساقی کوثر بهشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قاسم خلد و سقر حیدر که در روز ازل</p></div>
<div class="m2"><p>خلق شد بهر محبان وی از داور بهشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دوستان خاص او پیش از اجل در جنتند</p></div>
<div class="m2"><p>دیگران را گر شود منزل پس از محشر بهشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی‌ولایش نی همین محروم از آن باشند امم</p></div>
<div class="m2"><p>بلکه ممکن نیست بهر هیچ پیغمبر بهشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بمهرش متفق بودند بودی بی‌سخن</p></div>
<div class="m2"><p>جای خلق اولین و آخرین یکسر بهشت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این برآمد ز آنچه احمد گفت در خم غدیر</p></div>
<div class="m2"><p>که نباشد جز برای پیرو حیدر بهشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر بعمر خود هزاران حج اکبر کرده‌ای</p></div>
<div class="m2"><p>بی‌ولای او مخواه از خالق اکبر بهشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خلق عالم جمله مشتاق بهشتند و ز جان</p></div>
<div class="m2"><p>هست مشتاق محب حیدر صفدر بهشت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مادر نفس تو هرگه شد به بمهرش مطمئن</p></div>
<div class="m2"><p>پس ترا باشد بزیر مقدم مادر بهشت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کس بگندم کی فرو شد باغ رضوان بوالبشر</p></div>
<div class="m2"><p>داد از کف در هوای کوی آن سرور بهشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جان فدای کوی آن سرور که هست از روضه‌اش</p></div>
<div class="m2"><p>منفعل با آن صفای بی‌حد و بی‌مر بهشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از عبادت فی‌المثل گر بوذر و سلمان شوی</p></div>
<div class="m2"><p>باز بخشندت بمهر خواجه قنبر بهشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا مدیح او بدفتر زد رقم کلک صغیر</p></div>
<div class="m2"><p>شد ز فرط روح‌بخشی صفحه دفتر بهشت</p></div></div>