---
title: >-
    شمارهٔ ۴۶ - در تهنیت عید مولود مسعود
---
# شمارهٔ ۴۶ - در تهنیت عید مولود مسعود

<div class="b" id="bn1"><div class="m1"><p>زد امروز از نسل آدم به عالم</p></div>
<div class="m2"><p>خدیوی قدم کش طفیل است آدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه آدم چه عالم که گر او نبودی</p></div>
<div class="m2"><p>نه آدم پدیدار گشتی نه عالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانبان مطلق نماینده حق</p></div>
<div class="m2"><p>رسول مصدق نبی مکرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محمد (ص) که از نام پاکش برآید</p></div>
<div class="m2"><p>برآید هر آنکار از اسم اعظم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظهورش اگر بود بعد از رسولان</p></div>
<div class="m2"><p>مقامش فزود و جلالش نشد کم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر شه ز رجاله باشد مؤخر</p></div>
<div class="m2"><p>بر تبت از آن جمله باشد مقدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدی ختم شاهی به نام سلیمان</p></div>
<div class="m2"><p>چو نام ورا نقش کردی بخاتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یونس بداو دردل حوت مونس</p></div>
<div class="m2"><p>به آدم بداو در سر اندیب همدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ورا خوشه چینند از خرمن الحق</p></div>
<div class="m2"><p>چه موسی بن عمران چه عیسی بن مریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدرک مقامش خردهاست قاصر</p></div>
<div class="m2"><p>بوصف جلالش زبانهاست ابکم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خرد پی بذاتش توان برد گر پی</p></div>
<div class="m2"><p>برد ذره بر مهر یا قطره بریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بمعراج حق بود محرم بجائی</p></div>
<div class="m2"><p>که روح الامین هم نمیبود محرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تواند چو مه مهر را کرد منشق</p></div>
<div class="m2"><p>که آن بنده طلعت اوست وین هم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدرگاه او بندگانند یکسر</p></div>
<div class="m2"><p>فریدون و دارا سکندر کی و جم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>همه رایت ظلم و زحمت نگون شد</p></div>
<div class="m2"><p>چو از عدل و رحمت برافراشت پرچم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سلام حقش بر روان وه چه شاهی</p></div>
<div class="m2"><p>که او راست ملک دو عالم مسلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبودی اگر سعی آن قبله جان</p></div>
<div class="m2"><p>نه مروه صفا یافتی و نه زمزم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیفتاد از او سایه بر خاک زانرو</p></div>
<div class="m2"><p>که پا تا بسر بود جان مجسم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بهر درد باید از او جست درمان</p></div>
<div class="m2"><p>بهر ریش باید از او خواست مرهم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو منصور از او دید در خود تجلی</p></div>
<div class="m2"><p>سر دار زد از انا الحق همی دم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز ملک فنا شاه ملک بقا شد</p></div>
<div class="m2"><p>چو گردید مجذوب او پور ادهم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زند طعنه شرع منیر متینش</p></div>
<div class="m2"><p>بنای فلک را ز احکام محکم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو روز ازل دید آن راست قامت</p></div>
<div class="m2"><p>بتعظیم او تا ابد شد فلک خم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قضا و قدر بهر فرمان گزاری</p></div>
<div class="m2"><p>زنندش همی بوسه بر خاک مقدم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر او خواست صعوه درد جسم شاهین</p></div>
<div class="m2"><p>گر او گفت رو به خورد خون ضیغم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به امرش یکی زال از تار گیسو</p></div>
<div class="m2"><p>تواند به بندد بهم دست رستم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زیم با درافشان کف فیض بخشش</p></div>
<div class="m2"><p>مزن دم که اینجا بودیم کم از نم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نخواهی رسیدن به قصر جلالش</p></div>
<div class="m2"><p>نهی زیر پا گر زنه چرخ سلم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر اوست غمخوارت از هول محشر</p></div>
<div class="m2"><p>مده در دل اندیشه راه و مخور غم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خود او دست حق است و باشد بدستش</p></div>
<div class="m2"><p>کلید بهشت و کلید جهنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نجاتت دهد مهر او از مهالک</p></div>
<div class="m2"><p>ولی با ولای علی (ع) شد چو توأم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>علی آنکه او راست یار و برادر</p></div>
<div class="m2"><p>علی آنکه او راست صهر و پسر عم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>علی ولی آنکه سرش نگنجد</p></div>
<div class="m2"><p>نه در علم اعلم نه در فهم افهم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>علی آنکه دین خدا بود مختل</p></div>
<div class="m2"><p>بترویج آن گر نگشتی مصمم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>علی آنکه فضل و کرم معترف شد</p></div>
<div class="m2"><p>که اینست افضل که اینست اکرم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در انفس بود نام پاکش مروح</p></div>
<div class="m2"><p>وز آن گلشن جان بود شاد و خرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهر بزم و کاشانه او می‌فرستد</p></div>
<div class="m2"><p>اگر عیش و شادی است یا درد و ماتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برازنده نخلی است نخل ولایش</p></div>
<div class="m2"><p>که بر میشود ظاهر از آن چو میثم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو خوانم بمدحش چه گویم بوصفش</p></div>
<div class="m2"><p>کز این بیشه هی شیر طبعم خوردرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همان به که با یک جهان عجز و لابه</p></div>
<div class="m2"><p>بگویم بتوصیفش الله اعلم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>محب ورا باد دل بی‌ملالت</p></div>
<div class="m2"><p>عدوی ورا جان بغم باد مدغم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>الا ای که مدحت برای محبان</p></div>
<div class="m2"><p>بود شهد و از بهر اعدا بود سم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نه من سیر گردم ز مدح تو و نی</p></div>
<div class="m2"><p>حریصان دنیا ز دینار و درهم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تو خود مظهر ارحم الراحمینی</p></div>
<div class="m2"><p>صغیرت سگ آستانست اِرحَم</p></div></div>