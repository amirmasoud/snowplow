---
title: >-
    شمارهٔ ۷ - در منقبت امام‌المتقین حضرت امیرالمؤمنین علی علیه‌السلام
---
# شمارهٔ ۷ - در منقبت امام‌المتقین حضرت امیرالمؤمنین علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>اختران را گرچه یک اندر شمار است آفتاب</p></div>
<div class="m2"><p>لیک در آن جمع فرد از اقتدار است آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردد او گرد خود و انجم بگردش لاجرم</p></div>
<div class="m2"><p>در میان اختران دائر مدار است آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این که می‌بینی قراری نیست در سیارگان</p></div>
<div class="m2"><p>بی‌قرارند اینهمه چون بی‌قراراست آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الحق از این بی‌قراری وینهمه سرگشتگی</p></div>
<div class="m2"><p>می‌توان گفتن چون من عاشق بیار است آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آری آری عاشق یار است ور نه از چه رو</p></div>
<div class="m2"><p>چون رخ من دائما زرد و نزار است آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه هریک ز اختران را وصفها باشد ولی</p></div>
<div class="m2"><p>صاحب اوصاف بیرون از شمار است آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی همین جنس بشر را فیض بخشد کز وجود</p></div>
<div class="m2"><p>فیض بخش وحش و طیر و مور و مار است آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دائماً از قرب و بعد خویش نسبت با زمین</p></div>
<div class="m2"><p>کار پرداز زمستان و بهار است آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از نهان گشتن بمغرب و ز عیان گشتن ز شرق</p></div>
<div class="m2"><p>خود پدید آرنده لیل و نهار است آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق عالم گر همی فیض از معادن میبرند</p></div>
<div class="m2"><p>بر معادن فیض بخش و فیض بار است آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عابد الشمسش همی خواند خدای خویشتن</p></div>
<div class="m2"><p>گرچه زین نسبت بغایت شرمسار است آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون همه اشیاء عالم راست از وی پرورش</p></div>
<div class="m2"><p>فرقهٔی گویند خود پروردگار است آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عالمی را میکند یکدم مسخر گوئیا</p></div>
<div class="m2"><p>برق تیغ حیدر دلدل سوار است آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پادشاه ملک امکان آنکه او را بنده‌وار</p></div>
<div class="m2"><p>روز و شب فرمانبر و خدمتگذار است آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رجعت از دادش ز مغرب نی‌عجب کانشاه را</p></div>
<div class="m2"><p>همچو گوئی پیش پای اختیار است آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آسمانستش یکی نیلی حصار اطراف کاخ</p></div>
<div class="m2"><p>دیده بانی فوق آن نیلی حصار است آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا کند کسب ضیاء هر صبح بر خاک درش</p></div>
<div class="m2"><p>بوسه زن از روی عجز و انکسار است آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرد شمع عالم افروز وجود آنجناب</p></div>
<div class="m2"><p>تا قیامت دور زن پروانه‌وار است آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زیر بار عشق او زین گرم سیری در مثل</p></div>
<div class="m2"><p>اشتر بگسسته از مستی مهار است آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نازم آن قسام رزق عالمی را کز شرف</p></div>
<div class="m2"><p>مطبخ جود ورا جزئی شرار است آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا ببوسد خاک پای دوستانش یک بیک</p></div>
<div class="m2"><p>در تفحص گرد هر شهر و دیار است آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از جمال زاده او هست گوئی منفعل</p></div>
<div class="m2"><p>زین سبب گاهی نهان گه آشکار است آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حضرت صابر علی شه آفتاب برج دین</p></div>
<div class="m2"><p>آنکه پیش رأی او بی‌اعتبار است آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش چشم اهل بینش با وجود طلعتش</p></div>
<div class="m2"><p>ذره‌آسا بی‌شکوه و بی‌وقار است آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سایه‌اش را تا بسر دارد صغیر اندر بها</p></div>
<div class="m2"><p>پیش نظمش همچو زر کم‌عیار است آفتاب</p></div></div>