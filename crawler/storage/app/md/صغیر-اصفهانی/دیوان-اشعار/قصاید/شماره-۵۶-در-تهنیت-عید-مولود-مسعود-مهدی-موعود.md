---
title: >-
    شمارهٔ ۵۶ - در تهنیت عید مولود مسعود مهدی موعود
---
# شمارهٔ ۵۶ - در تهنیت عید مولود مسعود مهدی موعود

<div class="b" id="bn1"><div class="m1"><p>خالت بتا به عارض نیکو</p></div>
<div class="m2"><p>باشد حدیث آتش و هندو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم و خط تو در نظر آید</p></div>
<div class="m2"><p>یا در چمن همی چمد آهو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم ز زلف تو بر رخ</p></div>
<div class="m2"><p>کافر کجا و روضه مینو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونریخت بسکه چشم تو شد حک</p></div>
<div class="m2"><p>از لوح دهر نام هلاکو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جسمت ز جان لطیف‌تر اما</p></div>
<div class="m2"><p>باشد دل تو سخت‌تر از رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را کشی تو شوخ ولی کی</p></div>
<div class="m2"><p>جان ارزدت بزحمت بازو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در قتل ما به تیغ چه حاجت</p></div>
<div class="m2"><p>بس باشدت اشارت ابرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عطار دکه بندد هرگه</p></div>
<div class="m2"><p>افشان کنی بشانه تو گیسو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گیتی بود معطر خیزد</p></div>
<div class="m2"><p>این بو تو را ز غالیه مو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا از قدوم زاده نرجس</p></div>
<div class="m2"><p>اینسان هوا شده است سمن بو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شاهی کز او به نیمه شعبان</p></div>
<div class="m2"><p>طالع چو بدر شد رخ نیکو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورشید بر بخاک قدومش</p></div>
<div class="m2"><p>سائید بهر کسب ضیا رو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای عهده‌دار شخص شریفت</p></div>
<div class="m2"><p>یکتا به نظم گنبد نه تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مقصود عارفان تو ز یا حق</p></div>
<div class="m2"><p>منظور سالکان تو ز یا هو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روی تو سوی خالق و باشد</p></div>
<div class="m2"><p>سوی تو روی خلق ز هر سو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیضا به نزد روی تو ذره</p></div>
<div class="m2"><p>گردون بپیش پای تو چون گو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>الحق زند ز رفعت پایه</p></div>
<div class="m2"><p>با عرش آستان تو پهلو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دادن به کعبه نسبت کویت</p></div>
<div class="m2"><p>سنگ کمی بود به ترازو</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای آفتاب چهره عیان کن</p></div>
<div class="m2"><p>خفاش چند گرم تکاپو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا کی زنند منتظرانت</p></div>
<div class="m2"><p>چون فاخته ز هجر تو کوکو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باز آ و ساز چنگل شاهین</p></div>
<div class="m2"><p>از عدل آشیانهٔ تیهو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باز آ که مدعی رود از خود</p></div>
<div class="m2"><p>رسواست پیش معجزه جادو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ختم سخن توئی به میان آی</p></div>
<div class="m2"><p>تا چند این غریو و هیاهو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دست حق است دست تو دارد</p></div>
<div class="m2"><p>با دست حق که طاقت نیرو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باز آی و ساز جاری و ساری</p></div>
<div class="m2"><p>جوها ز خون خصم جفا جو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افتاده دین ز رونق باز آ</p></div>
<div class="m2"><p>باز آر آب رفته در این جو</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عاجز بود ز وصف جلالت</p></div>
<div class="m2"><p>نطق بلیغ و طبع سخنگو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاها صغیر عبد کمینت</p></div>
<div class="m2"><p>نبود مگر به مهر تواش خو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دارد امید آنکه بزودی</p></div>
<div class="m2"><p>بیند رخ تو چشم تر او</p></div></div>