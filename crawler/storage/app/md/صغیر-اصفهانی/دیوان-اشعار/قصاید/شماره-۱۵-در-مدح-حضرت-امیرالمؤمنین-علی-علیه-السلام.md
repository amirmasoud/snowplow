---
title: >-
    شمارهٔ ۱۵ - در مدح حضرت امیرالمؤمنین علی علیه‌السلام
---
# شمارهٔ ۱۵ - در مدح حضرت امیرالمؤمنین علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>آرام قلب و راحت جان مهر حیدر است</p></div>
<div class="m2"><p>نور یقین و روح روان مهر حیدر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌مهر حیدرت بجنان ره نمیدهند</p></div>
<div class="m2"><p>بالله کلید باب جنان مهر حیدر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ایمنی ز آتش دوزخ طلب کنی</p></div>
<div class="m2"><p>غافل مشو که حصن امان مهر حیدر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر باید از حقیقت ایمانت آگهی</p></div>
<div class="m2"><p>ایمان بدون شک و گمان مهر حیدر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکدانه گوهری که خریدار آن بحشر</p></div>
<div class="m2"><p>باشد خدای هر دو جهان مهر حیدر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر حرام‌زاده محک بغض مرتضی است</p></div>
<div class="m2"><p>بهر حلال‌زاده نشان مهر حیدر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی‌مهر او مجوی بکون و مکان نجات</p></div>
<div class="m2"><p>اصل نجات کون و مکان مهر حیدر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن اختر سپهر شرافت که تا بشش</p></div>
<div class="m2"><p>گردد بروز حشر عیان مهر حیدر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانی صغیر از چه همی مدح او کند</p></div>
<div class="m2"><p>او را سبب به نطق و بیان مهر حیدر است</p></div></div>