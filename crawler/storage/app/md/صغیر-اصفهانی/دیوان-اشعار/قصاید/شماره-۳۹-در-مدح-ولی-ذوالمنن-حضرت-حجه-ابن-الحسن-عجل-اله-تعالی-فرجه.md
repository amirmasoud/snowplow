---
title: >-
    شمارهٔ ۳۹ - در مدح ولی ذوالمنن حضرت حجه‌ابن‌الحسن عجل‌اله تعالی فرجه
---
# شمارهٔ ۳۹ - در مدح ولی ذوالمنن حضرت حجه‌ابن‌الحسن عجل‌اله تعالی فرجه

<div class="b" id="bn1"><div class="m1"><p>هر آنچه میزنم از دفتر وجود ورق</p></div>
<div class="m2"><p>نوشته است بخط جلی که جاءالحق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخست جلوه خالق امام آخر خلق</p></div>
<div class="m2"><p>یقین فراخته از غیب در عیان بیرق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به یمن مولد مسعود مهدی موعود</p></div>
<div class="m2"><p>زمین بعرش برین از شرف گرفته سبق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی مطلق حق آنکه کارگاه وجود</p></div>
<div class="m2"><p>بدست او ز ازل تا ابد بود مطلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود اوست مظهر حق کاینات از او ظاهر</p></div>
<div class="m2"><p>خود اوست مصدر کل ممکنات از او مشتق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه عالم ایجاد را نظامی بود</p></div>
<div class="m2"><p>نمیگرفت ز اضداد اگر که نظم و نسق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبق نماید اگر هیبتش هواداری</p></div>
<div class="m2"><p>کند فرار دوصد سال راه باد از بق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از او قوائد اسلام راست استحکام</p></div>
<div class="m2"><p>از او مسائل و احکام را بود رونق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد او به پردهٔ غیبت نهان و منتظرند</p></div>
<div class="m2"><p>بمقدمش همه خلق جهان فرق بفرق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برای آنکه نماید نثار مقدم او</p></div>
<div class="m2"><p>زمانه هستی خود را نهاده روی طبق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدست قدرت شد کلک صنع روز ازل</p></div>
<div class="m2"><p>پی نوشتن نام گرامیش منشق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدل ز معرفتش تا که بهره‌ای نرسد</p></div>
<div class="m2"><p>دل حزین نرهد هیچ ز اضطراب و قلق</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مر این عطیه میسر نمی‌شود دل را</p></div>
<div class="m2"><p>مگر که با دل صابر علی شود ملحق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صغیر دانی در بحر زورقی باید</p></div>
<div class="m2"><p>ببحر معرفت امروز او بود زورق</p></div></div>