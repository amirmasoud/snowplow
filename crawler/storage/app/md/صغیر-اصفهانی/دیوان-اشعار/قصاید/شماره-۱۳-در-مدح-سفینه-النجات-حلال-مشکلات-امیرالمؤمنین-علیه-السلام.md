---
title: >-
    شمارهٔ ۱۳ - در مدح سفینه النجات حلال مشکلات امیرالمؤمنین علیه‌السلام
---
# شمارهٔ ۱۳ - در مدح سفینه النجات حلال مشکلات امیرالمؤمنین علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>روی حق روی حق نمای علیست</p></div>
<div class="m2"><p>علی (ع) آئینهٔ خدای علیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بولای علی (ع) قسم ایمان</p></div>
<div class="m2"><p>به خدای علی (ع) ولای علیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب معراج شد لقاءالله</p></div>
<div class="m2"><p>کشف بر خلق کان لقای علیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مصطفی هر سخن شنید از حق</p></div>
<div class="m2"><p>یافت کان صوت دلربای علیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دستی آمد ز پشت پرده برون</p></div>
<div class="m2"><p>دید دست گره‌گشای علیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حمل بار ولایت علوی</p></div>
<div class="m2"><p>کان نه در خورد کس سوای علیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مصطفی را سزد که در کعبه</p></div>
<div class="m2"><p>دوش پاکش بزیر پای علیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این دو را جز یکی مدان و مخوان</p></div>
<div class="m2"><p>که بجز این خلاف رای علیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در رضای علی رضای خداست</p></div>
<div class="m2"><p>در رضای خدا رضای علیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حرکت در تمام موجودات</p></div>
<div class="m2"><p>باشد از عشق و آن هوای علیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یعنی این جنبشی که در اشیاست</p></div>
<div class="m2"><p>درحقیقت به مدعای علیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه همین در کنشت و دیر و حرم</p></div>
<div class="m2"><p>متواضع بشر برای علیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بلکه پیوسته در قیام و قعود</p></div>
<div class="m2"><p>ذکر کروبیان ثنای علیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با خداوند خویش بیگانه است</p></div>
<div class="m2"><p>هرکه جانش نه آشنای علیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسمان بی‌ستون از آن برپاست</p></div>
<div class="m2"><p>کاین معلق بنا بنای علیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مه ز خورشید کسب نور کند</p></div>
<div class="m2"><p>نور خورشید از ضیای علیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>انبیا را در آفتاب جزا</p></div>
<div class="m2"><p>سایبان بر سر از لوای علیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کان لعل از چه خون بدل دارد</p></div>
<div class="m2"><p>گر نه شرمندهٔ سخای علیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بحر بگرفته کاسه گرداب</p></div>
<div class="m2"><p>از چه بر کف نه گر گدای علیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جبرئیل آن امین وحی خدا</p></div>
<div class="m2"><p>بندهٔی بر در سرای علیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلبل از آن به گل فریفته شد</p></div>
<div class="m2"><p>که مصفا گل از صفای علیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در دل را به روی غیر ببند</p></div>
<div class="m2"><p>کاین مقام شریف جای علیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر زید صدهزار سال صغیر</p></div>
<div class="m2"><p>روز و شب منقبت سرای علیست</p></div></div>