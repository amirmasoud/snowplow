---
title: >-
    شمارهٔ ۱۸ - در تهنیت عید مولود صفدر بدر و حنین ابی‌الحسنین علی علیه‌السلام
---
# شمارهٔ ۱۸ - در تهنیت عید مولود صفدر بدر و حنین ابی‌الحسنین علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>در حریم کعبه شاه انس و جان آمد پدید</p></div>
<div class="m2"><p>آنکه مقصود دو عالم بود آن آمد پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکمران آسمان اندر زمین شد جلوه‌گر</p></div>
<div class="m2"><p>پادشاه لامکان اندر مکان آمد پدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات مطلق کان برون بود از مکان و از زمان</p></div>
<div class="m2"><p>شد مقید در مکان و در زمان آمد پدید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کعبه خود قلب جهانست و ز غیب ذات خویش</p></div>
<div class="m2"><p>سر غیب الغیب در قلب جهان آمد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ممکنی پیدا شد اما واجب آمد در ظهور</p></div>
<div class="m2"><p>گشت جسمی ظاهر اما سرجان آمد پدید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد عالم چند میجویی نشان از بی‌نشان</p></div>
<div class="m2"><p>بینش ار داری جمال بی‌نشان آمد پدید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا کند خود را تماشا با همه وصف و شئون</p></div>
<div class="m2"><p>در شهود از غیب آن گنج نهان آمد پدید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با تمام معنی اسم ذات و اسماء صفات</p></div>
<div class="m2"><p>صورتی گشت آنشه با عز و شان آمد پدید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیش از آن کاید فرود ایاک نعبد نستعین</p></div>
<div class="m2"><p>از درون قبله وجه مستعان آمد پدید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طالبان را مژده ده مطلوب آمد در کنار</p></div>
<div class="m2"><p>می‌کشان را کن خبر پیرمغان آمد پدید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عشقبازان را حبیب با وفا گشت آشکار</p></div>
<div class="m2"><p>دردمندان را طبیب مهربان آمد پدید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>احمد مرسل که خود روح روان عالمست</p></div>
<div class="m2"><p>بهر آن روح روان روح روان آمد پدید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا که بنماید بلاغت را نهج در روزگار</p></div>
<div class="m2"><p>آن خدای نطق و خلاق بیان آمد پدید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغ وحدت کش نهالند انبیاء و اولیاء</p></div>
<div class="m2"><p>باغبان آن خدائی بوستان آمد پدید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حق ندارم خوانمش جز حق چه پنهان از شما</p></div>
<div class="m2"><p>فاش می‌بینم که حق فاش و عیان آمد پدید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قادری آمد که چون دم زد بحرف کاف و نون</p></div>
<div class="m2"><p>ماه و خورشید و زمین و آسمان آمد پدید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با ولایش دل ز هول روز محشر ایمن است</p></div>
<div class="m2"><p>درحقیقت معنی کهف الامان آمد پدید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حب و بغضش میدهد از هالک و ناجی خبر</p></div>
<div class="m2"><p>بهر نقد قلب و خالص امتحان آمد پدید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کی توان خواندن کف فیاض او را بحر و کان</p></div>
<div class="m2"><p>آنکه از جود وجودش بحر و کان آمد پدید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خیزد از هرجا غلامی بهر درگاهش بلی</p></div>
<div class="m2"><p>قنبر از زنگ و صغیر از اصفهان آمد پدید</p></div></div>