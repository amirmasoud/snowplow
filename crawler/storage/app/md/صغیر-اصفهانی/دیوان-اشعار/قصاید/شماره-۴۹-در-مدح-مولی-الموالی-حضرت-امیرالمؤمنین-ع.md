---
title: >-
    شمارهٔ ۴۹ - در مدح مولی‌الموالی حضرت امیرالمؤمنین (ع)
---
# شمارهٔ ۴۹ - در مدح مولی‌الموالی حضرت امیرالمؤمنین (ع)

<div class="b" id="bn1"><div class="m1"><p>مظهری گردید ظاهر دوش بر عین الیقینم</p></div>
<div class="m2"><p>کز تماشای جمالش رفت از کف عقل و دینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لوحش الله از جمال او که چون دیدم بیاسود</p></div>
<div class="m2"><p>از غم و اندوه بی‌پایان دل اندوهگینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد دیدار بهشت جاودان طلعت وی</p></div>
<div class="m2"><p>فارغ از یاد بهشت و ازخیال حور عینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وین عجب کان دلبر یکتای بیهمتا عیان شد</p></div>
<div class="m2"><p>در جنوب و در شمال و در یسار و در یمینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتم از خویش و بگفتم با زبان بی‌زبانی</p></div>
<div class="m2"><p>کی حبیب دلفریبم ای نگار نازنینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تو جان جان جانم ای ضیاء دیدگانم</p></div>
<div class="m2"><p>دلبر و دلدار و دلجو دلستان و دلنشینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از کجائی کیستی نامت چه نسبت با که داری</p></div>
<div class="m2"><p>گفت من سر هویت هست هستی آفرینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نهانم کنز مخفی در عیانم کل هستی</p></div>
<div class="m2"><p>هستی آثار دو حرف است و من اصل آن و اینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسم اعظم گنج اسما عشق مطلق آمر کل</p></div>
<div class="m2"><p>داور کون و مکانم وجه رب العالمینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من رسول الله را در خور به تمجبا و ثنایم</p></div>
<div class="m2"><p>من کتاب الله را مصداق آیات مبینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طاوسین و میم و کاف و ها و یا و عین و صادم</p></div>
<div class="m2"><p>طا و سین و طا و ها و حا و میم و یا و سینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرالرحمن علی العرش استوی راگر ندانی</p></div>
<div class="m2"><p>آن منم کاندر سویدای دل انسان مکینم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ذاکر و مذکور و ذکرم حامد و محمود و حمدم</p></div>
<div class="m2"><p>من صراط المستقیمم مالک اندریوم دینم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من قیامم من قعودم من رکوعم من سجودم</p></div>
<div class="m2"><p>خود بخود گوینده ایاک نعبد نستعینم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزم وحدت را نوا و نغمه و نائی و نایم</p></div>
<div class="m2"><p>باده نوش و ساقی و مینا شراب و ساتکینم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اصفیا را من انیسم از کیا را من جلیسم</p></div>
<div class="m2"><p>انبیا را من ظهیرم اولیا را من معینم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پادشاه لا مکانم پیشوای انس و جانم</p></div>
<div class="m2"><p>مقتدای قدسیانم رهبر روح الامینم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نا امیدانرا امیدم بی‌پناهانرا پناهم</p></div>
<div class="m2"><p>خضر راه رهروانم هادیم حبل المتینم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست عالم جسم و در آن جسم من جان عزیزم</p></div>
<div class="m2"><p>هست امکان بحر و در آن بحر من در ثمینم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نوربخش مهر و ماه و زهره مریخ و عطارد</p></div>
<div class="m2"><p>زیور ارض و سما و لنگر عرش برینم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دست من بر پایدارد کرسی و لوح و قلم را</p></div>
<div class="m2"><p>من هوا دار سپهرم من نگهبان زمینم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز ابتدا تا انتها من خلق را قسام رزقم</p></div>
<div class="m2"><p>درحقیقت فیض بخش اولین و آخرینم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خستگان عشق را تیمار جان بی‌شکیبم</p></div>
<div class="m2"><p>تشنگان وصل را سرچشمه ماء معینم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عشق بایستی که تا عاشق بمن نزدیک گردد</p></div>
<div class="m2"><p>ور نه من بیرون ز استدراک عقل دوربینم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پای تا سر عشق و شور و جذبه‌ام همراه حسنم</p></div>
<div class="m2"><p>زین سبب گاه ظهور خویش با احمد قرینم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در مقام حسن کل محمود عبد من عبیدم</p></div>
<div class="m2"><p>نام نیکویم علی سرحلقه اهل یقینم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یا علی مدحت سرای درگه عرش آستانت</p></div>
<div class="m2"><p>من صغیر مستمند بی‌نوای دل غمینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>روسیاه و دل تباه و پرگناه و عذرخواهم</p></div>
<div class="m2"><p>عاجز و بیچاره و مسکین منیب و مستکینم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لیک شادم زینکه مداح تو هستم خاصه کز جان</p></div>
<div class="m2"><p>بنده فرزند تو صابر علی شاه امینم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مهر اوکان بیگمان مهر و تو لای تو باشد</p></div>
<div class="m2"><p>دست قدرت ریخته روز ازل در ماء وطینم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لطف او را کان بود لطف تو من امیدوارم</p></div>
<div class="m2"><p>منت او را که هست آن منت تو من رهینم</p></div></div>