---
title: >-
    شمارهٔ ۳۰ - در تهنیت عید مولود مسعود حضرت رسول اکرم صلی‌الله علیه و آله
---
# شمارهٔ ۳۰ - در تهنیت عید مولود مسعود حضرت رسول اکرم صلی‌الله علیه و آله

<div class="b" id="bn1"><div class="m1"><p>شد سر هویت بجهان جلوه گر امروز</p></div>
<div class="m2"><p>روشن شد از انوار رخش بحر و بر امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن اختر تابنده درخشید که تابید</p></div>
<div class="m2"><p>از خاک درش نور بشمس و قمر امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق را گهری بود به گنجینه رحمت</p></div>
<div class="m2"><p>بر عالمیان کرد عطا آن گهر امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین رتبه که شد مولد آن فخر رسولان</p></div>
<div class="m2"><p>بر عرش برین گشت زمین مفتخر امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آیت با میمنت نصر من الله</p></div>
<div class="m2"><p>افراشته شد رایت فتح و ظفر امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن شاهد غیبی که ز هر دیده نهان بود</p></div>
<div class="m2"><p>پیدا شد از این آینه پا تا بسر امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکو بودش آگهی از سر حقیقت</p></div>
<div class="m2"><p>بی‌پرده حقش جلوه کند در نظر امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناسوتی و لاهوتی از این مژده بوجدند</p></div>
<div class="m2"><p>پر شد ز شعف عالم زیر و زبر امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان گشت بشر اشرف مخلوق که ظاهر</p></div>
<div class="m2"><p>گردید در این سلسله خیرالبشر امروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخ داد در اقطار جهان ز آمدن وی</p></div>
<div class="m2"><p>آثار و غرائب بدر از حد و مر امروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز است بشرحش دهن طاق مدائن</p></div>
<div class="m2"><p>ز انشب بخردمند رساند خبر امروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در فارس ز میلاد شه یثرب و بطحا</p></div>
<div class="m2"><p>خاموش شد آتشکده پرشرر امروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کاهن و ساحر که از این مرحله بستند</p></div>
<div class="m2"><p>بار سفر خویش بسوی سقر امروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواهد صله بسیار صغیر از شه لولاک</p></div>
<div class="m2"><p>با اینکه بود چامه او مختصر امروز</p></div></div>