---
title: >-
    شمارهٔ ۳۱ - قصیده مولودیه ذیل در ماه رجب ۱۳۴۹ هجری
---
# شمارهٔ ۳۱ - قصیده مولودیه ذیل در ماه رجب ۱۳۴۹ هجری

<div class="n" id="bn1"><p>به عتبات مشرف شده بودند در اصفهان سروده شد و ارسال گردید</p></div>
<div class="b" id="bn2"><div class="m1"><p>زمین به عرش برین دارد افتخار امروز</p></div>
<div class="m2"><p>که شد در آن ملک العرش آشکار امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو بموسی ارنی بس است دیده گشای</p></div>
<div class="m2"><p>پی‌مشاهده طلعت نگار امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرو نهاد ز رخ پرده یار و در حق ما</p></div>
<div class="m2"><p>نکرد هیچ ز رحمت فروگذار امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علی‌پرستان ایمان خویش تازه کنید</p></div>
<div class="m2"><p>ز بادهٔ کهن ناب خوشگوار امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورید از کف هم باده بی‌حساب امشب</p></div>
<div class="m2"><p>زنید بر لب هم بوسه بی‌شمار امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببانک چنگ و به آواز تار مگذارید</p></div>
<div class="m2"><p>رها ز چنگ شود تار زلف یار امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورید باده و مستی کنید بی‌پرده</p></div>
<div class="m2"><p>که شد ز پرده پدیدار پرده‌دار امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر غم زاهد خودبین خودنما گردید</p></div>
<div class="m2"><p>بکام باده کشان دور روزگار امروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به شیخ شهر بگو عذر میکشان بپذیر</p></div>
<div class="m2"><p>که نیست در کف این قوم اختیار امروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهار آورد ار صدگل از عدم بوجود</p></div>
<div class="m2"><p>گلی شکفته که آورده صدبهار امروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سزد ز خاک شود مشک ناب ارزان‌تر</p></div>
<div class="m2"><p>چنین که باد صبا گشته مشگبار امروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برقص هفت آب و چار‌ام افتد که هست</p></div>
<div class="m2"><p>ولادت آب ذوالمجد هفت و چار امروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علی (ع) عالی اعلا بکعبه یافت ظهور</p></div>
<div class="m2"><p>حریم کعبه از آن یافت اعتبار امروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پدید گشت یدالله فوق ایدیهم</p></div>
<div class="m2"><p>محمد (ص) عربی یافت دستیار امروز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بجای دار جهودان به انتقام مسیح</p></div>
<div class="m2"><p>لوای نصر من الله شد استوار امروز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برای کشتن فرعون خصلتان دنی</p></div>
<div class="m2"><p>عصای موسی گردید ذوالفقار امروز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبا ز ملک صفاهان تو با هزار درود</p></div>
<div class="m2"><p>بسوی یار سفر کرده کن گذار امروز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ببوس خاک قدومش بجای ما آنگاه</p></div>
<div class="m2"><p>بگو که جای تو خالی در این دیار امروز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بباطن ار چه تو داری بدل قرار ولی</p></div>
<div class="m2"><p>بظاهریم ز هجر تو بی‌قرار امروز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیمن همتت ای شاه صابر اخوان را</p></div>
<div class="m2"><p>ملالتی نبود غیرانتظار امروز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بساط جشن فراهم نموده و خواهند</p></div>
<div class="m2"><p>به عشق مولا سازند جان نثار امروز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بحضرتت همه تبریک گو بویژه صغیر</p></div>
<div class="m2"><p>همان که جز بتو نبود امیدوار امروز</p></div></div>