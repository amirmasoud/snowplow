---
title: >-
    شمارهٔ ۵۵ - در مدح شاه انس و جان مولی الموالی امیرالمؤمنین علی علیه‌السلام
---
# شمارهٔ ۵۵ - در مدح شاه انس و جان مولی الموالی امیرالمؤمنین علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>چنانکه میشود از نور خور جهان روشن</p></div>
<div class="m2"><p>شود ز نام علی قلب دوستان روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چراغ بزم جهان روی مرتضی است بلی</p></div>
<div class="m2"><p>بود بنوروی این تیره خاکدان روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلامکان ز مکان رفت چون رسول خدای</p></div>
<div class="m2"><p>بدید از رخ او بزم لامکان روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیان روشن او میکند ز اهل خرد</p></div>
<div class="m2"><p>ضمیر روشن و دل روشن و روان روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آنچراغ که از برق تیغ خود افروخت</p></div>
<div class="m2"><p>نمود تا بابد راه انس و جان روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا ز ظلمت حیرت رهد کسی که نکرد</p></div>
<div class="m2"><p>چراغ مهر علی را به بزم جان روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کند ز خاک درش کسب فیض و گیرد نور</p></div>
<div class="m2"><p>بهر صباح که میگردد آسمان روشن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجوی خاک ره دوستان مخلص او</p></div>
<div class="m2"><p>مگر که چشم خرد را کنی بآن روشن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگو محب ورا غم مخور ز ظلمت قبر</p></div>
<div class="m2"><p>که هست خانهٔ گور تو جاودان روشن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی که شمع صفت سوزد از غم عشقش</p></div>
<div class="m2"><p>چه بزم‌ها کند از شعلهٔ زبان روشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صغیر بندهٔ عشق علی و آل علیست</p></div>
<div class="m2"><p>شود حقیقت گوینده از بیان روشن</p></div></div>