---
title: >-
    شمارهٔ ۶۹ - در مدح ولی‌الله اعظم امیرالمؤمنین علی «علیه‌السلام»
---
# شمارهٔ ۶۹ - در مدح ولی‌الله اعظم امیرالمؤمنین علی «علیه‌السلام»

<div class="b" id="bn1"><div class="m1"><p>ز جلوه مهر سماکی به بوتراب رسد</p></div>
<div class="m2"><p>چگونه ذره تواند به آفتاب رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی نعمت ما خاکیان علیست که فیض</p></div>
<div class="m2"><p>بزادگان تراب از ابوتراب رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجوی کام دل خویش را ز غیر علی</p></div>
<div class="m2"><p>که دیده آب بلب تشنه از سراب رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود بذکر علی نور حق بدل پیدا</p></div>
<div class="m2"><p>بدست جان بکن این چاه تا به آب رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بغیر احمد مرسل که جسم و جان همند</p></div>
<div class="m2"><p>که در مقام و جلالت بدان جناب رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکعبه زاد و برای طواف آن دایم</p></div>
<div class="m2"><p>ز رب کعبه بخلق جهان خطاب رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کتاب خواند از آن پیشتر برای رسول</p></div>
<div class="m2"><p>که از خدا به رسول خدا کتاب رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر بنده آل علی به عشق علیست</p></div>
<div class="m2"><p>که بوی گل به مشام وی از گلاب رسد</p></div></div>