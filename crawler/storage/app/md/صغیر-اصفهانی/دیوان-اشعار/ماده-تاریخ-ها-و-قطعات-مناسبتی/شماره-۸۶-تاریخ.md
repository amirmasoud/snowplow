---
title: >-
    شمارهٔ ۸۶ - تاریخ
---
# شمارهٔ ۸۶ - تاریخ

<div class="b" id="bn1"><div class="m1"><p>سید عالی نسب آن حامد محمود نام</p></div>
<div class="m2"><p>کز عبادت گشت نائل رحمت معبود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت مسعود آنچنان کامد سعید از بطن‌ام</p></div>
<div class="m2"><p>اختر فیروز بین و طالع مسعود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هفدهم روز از ربیع و عید مولود نبی</p></div>
<div class="m2"><p>کز همه ذرات صلوات آن بهین مولود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ارجعی بشنید و کرد از اینجهان نقل مکان</p></div>
<div class="m2"><p>نزد جد خود مکین شد جنت موعود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد صغیر اندریم فکرت شناور کاورد</p></div>
<div class="m2"><p>بهر تاریخش بکف این لؤلؤ منضود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناگهان‌ آمد یکی از جمع بیرون و بگفت</p></div>
<div class="m2"><p>عاقبت محمود شد از بندگی محمود را</p></div></div>