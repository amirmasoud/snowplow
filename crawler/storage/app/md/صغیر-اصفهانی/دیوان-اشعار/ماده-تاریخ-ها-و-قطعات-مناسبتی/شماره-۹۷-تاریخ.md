---
title: >-
    شمارهٔ ۹۷ - تاریخ
---
# شمارهٔ ۹۷ - تاریخ

<div class="b" id="bn1"><div class="m1"><p>حیف رضای جنانی آنکه وجودش</p></div>
<div class="m2"><p>بود یکی گلبن از ریاض معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل هنر بود از علوم غریبه</p></div>
<div class="m2"><p>داشت در این کار اشتهار جهانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه به تهران فکند رحل اقامت</p></div>
<div class="m2"><p>آن همه دان در نژاد بد همدانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل ادب بود و مرد مهر و محبت</p></div>
<div class="m2"><p>حل شد از او مشکلات عالی و دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الغرض او را اجل رسید بناگاه</p></div>
<div class="m2"><p>رفت به دار بقا ز عالم فانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفت صغیرش بشمسی از پی تاریخ</p></div>
<div class="m2"><p>جای به باغ جنان نمود جنانی</p></div></div>
<div class="n" id="bn7"><p>۱۳۳۷</p></div>