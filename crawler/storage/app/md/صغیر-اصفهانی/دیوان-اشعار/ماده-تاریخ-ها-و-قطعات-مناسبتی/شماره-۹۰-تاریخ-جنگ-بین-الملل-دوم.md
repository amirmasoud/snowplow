---
title: >-
    شمارهٔ ۹۰ - تاریخ جنگ بین‌الملل دوم
---
# شمارهٔ ۹۰ - تاریخ جنگ بین‌الملل دوم

<div class="b" id="bn1"><div class="m1"><p>دو لفظ بی‌جا معمول خلق در هر جاست</p></div>
<div class="m2"><p>از آندو لفظ یکی خود من است و دیگر ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این دو لفظ تهی عالمی پر است عجب</p></div>
<div class="m2"><p>چو سهو یا چو غلط یا چه خبط یا چه خطاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندانم این من و ماباقی است است روی چه اصل</p></div>
<div class="m2"><p>بنا که پی ننهادند سقف روی کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر این دو اصل بخند و به فرع آن گیتی</p></div>
<div class="m2"><p>که اصل و فرع روان از عدم بسوی فناست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی به خویش بنازد که خانه خانهٔ من</p></div>
<div class="m2"><p>بسی نرفته نه او باقی و نه خانه بپاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی به تخت ببالد چو نیک درنگری</p></div>
<div class="m2"><p>نه او نه تخت و نه تاج و نه مملکت برجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرض از این من و ما جان من زبان درکش</p></div>
<div class="m2"><p>که دعوی من و ما از زبان نفس دغاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین وخامت‌ امروز و جنگ عالم سوز</p></div>
<div class="m2"><p>که آتش من و ما برق خرمن دنیاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهد فریب بشر را کسی به آزادی</p></div>
<div class="m2"><p>که در اسارت او نسل آدم و حواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عالمی کند از حیله دعوی پدری</p></div>
<div class="m2"><p>کسی که با همه افراد خصم مادرزاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به میل یک دو زیانکار خلق را شب و روز</p></div>
<div class="m2"><p>فراز سر اجل ناگهان هواپیماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنند فخر که آتش به خانمان کسان</p></div>
<div class="m2"><p>زدیم و دیدیم آنجا چه شعله‌ها برخاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به هرکجا نگری جنگ و فتنه و آشوب</p></div>
<div class="m2"><p>بهر طرف که دهی گوش ضجه و غوغاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زهی به نوع پرستی بشر ز ظلم بشر</p></div>
<div class="m2"><p>خوراک ماهی دریا و وحشی صحراست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زهی بروز شجاعت که حمله ورگشتند</p></div>
<div class="m2"><p>بملک بی‌طرف و بی‌دفاع از چپ و راست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فقیر جورکش خرج خاندان غنی است</p></div>
<div class="m2"><p>ضعیف دستخوش ظلم صاحبان قواست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدین رویهٔ زشت ای بشر بجان خودت</p></div>
<div class="m2"><p>سر تو در خور تشریف تاج کرمناست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه دست‌ها به ستم بر فراشته است ولی</p></div>
<div class="m2"><p>فراز دست همه دست انتقام خداست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگرچه ابر سیاهی گرفته روی جهان</p></div>
<div class="m2"><p>ولی زرخنهٔ این ابر جلوه‌گر بیضاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گذشته است ز هجرت هزار و سیصد و شصت</p></div>
<div class="m2"><p>قضیه این بود‌ام روز تا چسان فرداست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صغیر قصه رها کن بقول نصرالدین</p></div>
<div class="m2"><p>نزاع خلق برای لحاف کهنه ماست</p></div></div>