---
title: >-
    شمارهٔ ۹۱ - تاریخ فوت شاعر شیرین سخن مرحوم رنجی
---
# شمارهٔ ۹۱ - تاریخ فوت شاعر شیرین سخن مرحوم رنجی

<div class="b" id="bn1"><div class="m1"><p>رفت از کفم مصاحب با جان برابری</p></div>
<div class="m2"><p>دانا دلی لطیفه سرائی سخنوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میگیریم از فشار غم اندوز عقده‌ئی</p></div>
<div class="m2"><p>میسوزم از شرار جگر سوز آذری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستغرق است زورق صبرم ببحر غم</p></div>
<div class="m2"><p>چون کشتی شکستهٔ بگسسته لنگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال مرا بمرگ تو ای رنجی عزیز</p></div>
<div class="m2"><p>داند کسی که رفته ز دستش برادری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند دل بمهر رفیق دگر به بند</p></div>
<div class="m2"><p>در من دلی نمانده که بندم بدیگری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با این همه چه چاره توانم که بنده را</p></div>
<div class="m2"><p>تدبیر نیست در بر ‌امر مقدری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد حیف از آن وجود سراپا ادب که بود</p></div>
<div class="m2"><p>فضل مجسمی و کمال مصوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او را بگاه نطق و بیان هرکه دید گفت</p></div>
<div class="m2"><p>از صائب و کلیم عیان گشته مظهری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مدح کسی نگفت ز روی طمع که بود</p></div>
<div class="m2"><p>قانع بدسترنجی و رزق مقرری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیموده راه حق و بمقصد رسید و گشت</p></div>
<div class="m2"><p>واصل بحق که همچو علی داشت رهبری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زو ماند شعر نغز فراوان و هریکی</p></div>
<div class="m2"><p>رخشان در آسمان ادب همچو اختری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفت و صغیر از پی تاریخ وی سرود</p></div>
<div class="m2"><p>رنجی چو رفت مانده ز او گنج گوهری</p></div></div>
<div class="c"><p>۱۳۸۰</p></div>