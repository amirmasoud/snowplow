---
title: >-
    شمارهٔ ۷۸ - تاریخ فوت مرحوم حاج عبدالحسین المتخلص بمشفق
---
# شمارهٔ ۷۸ - تاریخ فوت مرحوم حاج عبدالحسین المتخلص بمشفق

<div class="b" id="bn1"><div class="m1"><p>مشفق آنکو با ولای هشت و چار</p></div>
<div class="m2"><p>از ازل آب و گلش بودی عجین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مدیح و در مراثی ز اهل بیت</p></div>
<div class="m2"><p>نظم او زد طعنه بر در ثمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماند از او باقیات الصالحات</p></div>
<div class="m2"><p>نظم روح افزا و شعر دلنشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت تاریخ وفاتش را صغیر</p></div>
<div class="m2"><p>کرده مشفق جا بفردوس برین</p></div></div>
<div class="n" id="bn5"><p>۱۳۶۲</p></div>