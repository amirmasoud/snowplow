---
title: >-
    شمارهٔ ۷۷ - تاریخ رحلت پیر روشن ضمیر شمس فلک آگاهی هادی
---
# شمارهٔ ۷۷ - تاریخ رحلت پیر روشن ضمیر شمس فلک آگاهی هادی

<div class="b" id="bn1"><div class="m1"><p>آه کز رحلت ناصر علی آن لمعهٔ نور</p></div>
<div class="m2"><p>روز اخوان صفا گشت چو شام دیجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیست عمری همه با یاد خدا ذکر علی</p></div>
<div class="m2"><p>رفت و شد با علی و‌ آل‌گرامی محشور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخدا غیر علی هیچ نبودش مقصود</p></div>
<div class="m2"><p>بعلی غیر خدا هیچ نبودش منظور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستان را همه بنواختی از خوی نکو</p></div>
<div class="m2"><p>فقرا را همه خوش داشتی از فیض حضور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام نامیش حسین و لقبش شه ناصر</p></div>
<div class="m2"><p>هم بدرویشی معروف و برندی مشهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الغرض خرقه تهی کرد و بعشق مولا</p></div>
<div class="m2"><p>رفت از این منزل پرغم بسوی دار سرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر تاریخ وفاتش بأسف گفت صغیر</p></div>
<div class="m2"><p>از حسین‌ آمد ناصر بدوگیتی منصور</p></div></div>
<div class="n" id="bn8"><p>۱۳۶۰</p></div>