---
title: >-
    شمارهٔ ۸۴ - تاریخ
---
# شمارهٔ ۸۴ - تاریخ

<div class="b" id="bn1"><div class="m1"><p>دوش در خانقه ز درویشی</p></div>
<div class="m2"><p>بگرفتم سراغ انشائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت ای یادهٔ تجلی پیر</p></div>
<div class="m2"><p>دوش پر شد ایاغ انشائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت تا صبح حشر زیر لحد</p></div>
<div class="m2"><p>دل روشن چراغ انشائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوی پیر بزرگوار‌ آمد</p></div>
<div class="m2"><p>جنت و باغ و راغ انشائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بری از ماسوی بجلوهٔ دوست</p></div>
<div class="m2"><p>شد چو حاصل فراغ انشائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جست تاریخ او صغیر از دل</p></div>
<div class="m2"><p>دل نشان داد داغ انشائی</p></div></div>