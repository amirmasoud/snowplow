---
title: >-
    شمارهٔ ۸۵ - تاریخ اتمام مسجد جامع چالوس
---
# شمارهٔ ۸۵ - تاریخ اتمام مسجد جامع چالوس

<div class="b" id="bn1"><div class="m1"><p>شکر کز لطف خدای احد بنده نواز</p></div>
<div class="m2"><p>این بنا روی به انجام نهاد از آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسجد جامعی از پاکدلان شد تاسیس</p></div>
<div class="m2"><p>که بیارند نیاز و بگذارند نماز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرد بس همت خود صرف در اینطرفه بنا</p></div>
<div class="m2"><p>حجه موسوی آن فاضل فرد ممتاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باری از آب و گل صدق و صفا در چالوس</p></div>
<div class="m2"><p>گشت بنیاد چو این معبد فردوس طراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جست تاریخ وی از غیب خرد گفت صغیر</p></div>
<div class="m2"><p>که به چالوس در از رحمت یزدان شده باز</p></div></div>
<div class="n" id="bn6"><p>۱۳۷۸</p></div>