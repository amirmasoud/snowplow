---
title: >-
    شمارهٔ ۹۶ - تاریخ درب مجلل حرم مطهر مقدس کاظمیین علیهماالسلام
---
# شمارهٔ ۹۶ - تاریخ درب مجلل حرم مطهر مقدس کاظمیین علیهماالسلام

<div class="b" id="bn1"><div class="m1"><p>چون شد تراب مدفن اولاد بوتراب</p></div>
<div class="m2"><p>عرش عظیم گفت که یا لیتنی تراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بار بارک الله از این بارگاه قدس</p></div>
<div class="m2"><p>کزیمن آن بپا بود این نیلگون قباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین آستان کنند مگر چشم جانکحیل</p></div>
<div class="m2"><p>دایم فرشتگان به ایابند یا ذهاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبود عجب در این حرم از فرط احترام</p></div>
<div class="m2"><p>پیش از سئوال‌گر که دعا گشت مستجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پهلو به عرش میزند این مرقد شریف</p></div>
<div class="m2"><p>زین رو که با دو سبط نبی دارد انتساب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک روح در دو پیکر و یک شخص با دو اسم</p></div>
<div class="m2"><p>یک نور در دو دیده و یک شرح در دو باب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اول جناب موسی کاظم که گشته است</p></div>
<div class="m2"><p>موسی به طور مقتبس از نور آن جناب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ذات او نهفته صفات محمدی</p></div>
<div class="m2"><p>آنسانکه بوی گل بود‌ آماده در گلاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوم نهم‌ امام بحق حضرت جواد</p></div>
<div class="m2"><p>کز ذکر نام او برهد دل ز التهاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرزند مرتضی و جگر گوشهٔ رضا</p></div>
<div class="m2"><p>مقصود چهار مادر و منظور هفت باب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بانی برای این حرم این در تهیه کرد</p></div>
<div class="m2"><p>نائل شد از عنایت یزدان بدین ثواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تاریخ آن صغیر به مدح دوشه نوشت</p></div>
<div class="m2"><p>در این فلک بود متجلی دو آفتاب</p></div></div>