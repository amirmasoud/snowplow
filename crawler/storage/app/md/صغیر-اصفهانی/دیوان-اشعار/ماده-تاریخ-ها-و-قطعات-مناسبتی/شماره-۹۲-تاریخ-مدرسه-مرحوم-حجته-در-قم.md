---
title: >-
    شمارهٔ ۹۲ - تاریخ مدرسه مرحوم حجته در قم
---
# شمارهٔ ۹۲ - تاریخ مدرسه مرحوم حجته در قم

<div class="b" id="bn1"><div class="m1"><p>حجه آن حجه شریعت ودین</p></div>
<div class="m2"><p>که مؤبد شد از خدای علیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن وجد یگانه کز مثلش</p></div>
<div class="m2"><p>مادر روزگار مانده عقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساخت او بهر طالبان علوم</p></div>
<div class="m2"><p>مدرسی درنهایت تحکیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جست تاریخ سال اتمامش</p></div>
<div class="m2"><p>عقل عالی گهر ز ذوق سلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس به شمسی رقم نمود صغیر</p></div>
<div class="m2"><p>هی قد اسست علی التعلیم</p></div></div>