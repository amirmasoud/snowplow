---
title: >-
    شمارهٔ ۸۲ - تاریخ
---
# شمارهٔ ۸۲ - تاریخ

<div class="b" id="bn1"><div class="m1"><p>باقر عالی مقام سید‌ امجد</p></div>
<div class="m2"><p>زادهٔ هادی ستوده پیر دل آگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزد پدر رفت و سال رحلت او را</p></div>
<div class="m2"><p>یافت مطابق صغیر باغفر الله</p></div></div>