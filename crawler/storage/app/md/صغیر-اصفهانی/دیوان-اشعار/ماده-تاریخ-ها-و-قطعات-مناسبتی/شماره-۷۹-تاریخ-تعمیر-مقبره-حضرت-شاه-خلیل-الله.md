---
title: >-
    شمارهٔ ۷۹ - تاریخ تعمیر مقبره حضرت شاه خلیل الله
---
# شمارهٔ ۷۹ - تاریخ تعمیر مقبره حضرت شاه خلیل الله

<div class="b" id="bn1"><div class="m1"><p>خانقاه و مدفن پیر اجل سید خلیل</p></div>
<div class="m2"><p>شد ز نو تعمیر از اقدام مردان جلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهتمام شاه مونس خضر راه فقر گشت</p></div>
<div class="m2"><p>در ره خیری چنین بس خیرخواهانرا دلیل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حضرت مولا بداردشان دو گیتی سرفراز</p></div>
<div class="m2"><p>حق باقداماتشان یکیک دهد اجر جزیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسل پاک نعمت‌لله شه خلیل پاک جان</p></div>
<div class="m2"><p>آنکه حیدر راست شبل و باشد احمد را سلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود بجرم اینکه میخوانده است هر دمرا بحق</p></div>
<div class="m2"><p>در هرات‌ آمد ز جور دشمنان حق قتیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد نهصد چون دوده با پنج شد بگرفت جای</p></div>
<div class="m2"><p>آن شهید فی‌سیب‌الله کنار سلسبیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گهش دادی عطا زوار را از هر کنار</p></div>
<div class="m2"><p>حضرتش کردی رواحا جاترا از هر قبیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا در این ایام کز سعی بزرگان جهان</p></div>
<div class="m2"><p>یافت خوش تعمیر این عالی بنای بیعدیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای پی تاریخ آن شد مستعد ذوق صغیر</p></div>
<div class="m2"><p>پیر خود را پس بروح پرفتوح آمد دخیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگهان گفتش سروش غیب اندر گوش دل</p></div>
<div class="m2"><p>قبله‌گاه جان و دل شد کوی نیکوی خلیل</p></div></div>
<div class="n" id="bn11"><p>۱۳۶۳</p></div>