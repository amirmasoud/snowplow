---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای لطف تو بگشوده بمن باب عطا</p></div>
<div class="m2"><p>ناگفته و گفته حاجتم کرده روا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاجات دگر کنون مرا در نظر است</p></div>
<div class="m2"><p>بنمای روا به حرمت آل عبا</p></div></div>