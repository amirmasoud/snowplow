---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ای مرده دلان مردهٔ مرده‌پرست</p></div>
<div class="m2"><p>وقعی ننهد بر بزرگی تا هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از گرسنگی چو او درافتاد ز پای</p></div>
<div class="m2"><p>آنگاه به گور می‌برندش سردست</p></div></div>