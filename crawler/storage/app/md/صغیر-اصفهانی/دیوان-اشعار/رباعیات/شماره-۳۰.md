---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ای سجده‌گه اهل وفا ابرویت</p></div>
<div class="m2"><p>وی قبله جان حق‌پرستان کویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرسو که کنم روی و بهرجا که روم</p></div>
<div class="m2"><p>باشد به خدا روی دل من سویت</p></div></div>