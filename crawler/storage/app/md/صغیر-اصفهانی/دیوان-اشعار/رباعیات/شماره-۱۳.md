---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>خرم دل آنکس که ز خود آگاه است</p></div>
<div class="m2"><p>نفیش سوی اثبات دلیل راه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر هرچه نظر کند خدا بیند و بس</p></div>
<div class="m2"><p>این معنی لااله الا الله است</p></div></div>