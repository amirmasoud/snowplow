---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>در خوش چو بر مغز رسیدم از پوست</p></div>
<div class="m2"><p>گفتم بود این مقام جای من و دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نیک نظر نمودم از دیدهٔ دل</p></div>
<div class="m2"><p>دیدم بمیان منی نباشد همه اوست</p></div></div>