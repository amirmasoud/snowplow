---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>در کعبه و در کنشت موجود علی است</p></div>
<div class="m2"><p>عالم همه طالبند و مقصود علیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیک ار نگری حقیقت اشیا را</p></div>
<div class="m2"><p>ز آئینهٔ کاینات مشهود علیست</p></div></div>