---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>در مخزن لایموت دردانه علی است</p></div>
<div class="m2"><p>در کون و مکان‌ امیر فرزانه علیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کعبه ظهور کرد تا بر همه کس</p></div>
<div class="m2"><p>معلوم شود که صاحب‌خانه علیست</p></div></div>