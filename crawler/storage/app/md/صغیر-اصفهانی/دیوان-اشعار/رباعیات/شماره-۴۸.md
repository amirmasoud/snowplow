---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>شاهی که به اسرار جهان دانا بود</p></div>
<div class="m2"><p>از رتبه به کل ما خلق مولا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب‌ها به خرابه‌ها ز روی شفقت</p></div>
<div class="m2"><p>همدم به جذامیان نابینا بود</p></div></div>