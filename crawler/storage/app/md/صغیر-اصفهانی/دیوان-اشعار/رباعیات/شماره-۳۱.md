---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>گفتم به خرد کی به همه بالا دست</p></div>
<div class="m2"><p>بر گوی که هستی بچه باشد پابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او صفحه و خامه یی‌طلب کرد از من</p></div>
<div class="m2"><p>بنوشت محبت و قلم را بکشست</p></div></div>