---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>گر وعده بهشت یا جحیم است تو را</p></div>
<div class="m2"><p>از روز جزا نه جای بیم است تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خلقت خلق روی اصل کرم است</p></div>
<div class="m2"><p>خوش باش که کار با کریمست تو را</p></div></div>