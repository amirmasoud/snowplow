---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>میلاد سعید مهدی موعود است</p></div>
<div class="m2"><p>آفاق پر از نشاط زین مولود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مکمن غیب در شهود‌ آمده است</p></div>
<div class="m2"><p>آن ذات که عین شاهد و مشهود است</p></div></div>