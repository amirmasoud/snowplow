---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>مرگِ غنی مقدمهٔ جنگ وارث است</p></div>
<div class="m2"><p>رحمت به روح آنکه بمرد و کفن نداشت</p></div></div>