---
title: >-
    شمارهٔ  ۲۲۷
---
# شمارهٔ  ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>نه هر که تاخت بمیدان دلاوری داند</p></div>
<div class="m2"><p>نه هر که سوخت در آتش سمندری داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه هر ستاره درخشید صاحب نظری</p></div>
<div class="m2"><p>مهش شمارد و خورشید خاوری داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توانگر آنکه بدست آورد دلی ورنه</p></div>
<div class="m2"><p>نه هر که سوخت دلی را توانگری داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوای تاج کیانی ندارد اندر سر</p></div>
<div class="m2"><p>کسی که قدر کلاه قلندری داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشست دیو به اورنگ جم ولی آصف</p></div>
<div class="m2"><p>رویهٔ زحل و سیر مشتری داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میان معجزه و سحر فرق بسیار است</p></div>
<div class="m2"><p>نه هر مفتن و ساحر پیمبری داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان راه ز دزدان ره چه می‌پرسی</p></div>
<div class="m2"><p>نه هر که بر سر راه است رهبری داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو غلام کسی غیر خواجه قنبر</p></div>
<div class="m2"><p>کدام خواجه چو ا و بنده پروری داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صغیر تا شده از جان غلام درگه او</p></div>
<div class="m2"><p>غلامی در او به ز سروری داند</p></div></div>