---
title: >-
    شمارهٔ  ۱۸۵
---
# شمارهٔ  ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>به تغافل همه روزان و شبان می‌گذرد</p></div>
<div class="m2"><p>حیف از این عمر که در خواب گران می‌گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بد و نیک جهان قصه مخوان باده بخور</p></div>
<div class="m2"><p>شادی این که بد و نیک جهان می‌گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راستی قابل این نیست جهان گذران</p></div>
<div class="m2"><p>که بگوییم چنین است و چنان می‌گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بگیری کُلَه از سر رَوَد ایّامِ بهار</p></div>
<div class="m2"><p>تا نهی باز به سر فصل خزان می‌گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذراند ز کمان فلکت شستِ قضا</p></div>
<div class="m2"><p>همچو تیری که به ناگه ز کمان می‌گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وضع گیتی طلب از مهتر سیاحان مهر</p></div>
<div class="m2"><p>که بر او سیر کران تا به کران می‌گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر نفس عمر تو بی‌سود کسان است صغیر</p></div>
<div class="m2"><p>گر به تحقیق ببینی به زیان می‌گذرد</p></div></div>