---
title: >-
    شمارهٔ  ۳۹۰
---
# شمارهٔ  ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>ای مه ز مهر اگر نظری سوی ما کنی</p></div>
<div class="m2"><p>کام دل شکسته ما را روا کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آموختت که این روش دلبری که رخ</p></div>
<div class="m2"><p>بنمائی و بری دل و ترک وفا کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس فزوتر از همه شد مبتلای تو</p></div>
<div class="m2"><p>بر او فزونتر از همه جور و جفا کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر آنکه منع عشق تو از من همی کند</p></div>
<div class="m2"><p>بنمای روی تا چو منش مبتلا کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمت به پشت پرده خورد خون مردمان</p></div>
<div class="m2"><p>فریاد از دمی که ز رخ پرده واکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آزادی دو کون بود در کمند تو</p></div>
<div class="m2"><p>روزی مباد آنکه اسیران رها کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گاهی ز راه دیده کشی رخت خود بدل</p></div>
<div class="m2"><p>گاهی ز دل برآئی و در دیده حا کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای دیده از صغیر چه خواهی که پیش خلق</p></div>
<div class="m2"><p>راز نهان او همه جا برملا کنی</p></div></div>