---
title: >-
    شمارهٔ  ۸۳
---
# شمارهٔ  ۸۳

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی از یکدو جام شراب</p></div>
<div class="m2"><p>مرا کن چو دور زمانه خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنی تا خرابم به کلی بده</p></div>
<div class="m2"><p>شرابم شرابم شرابم شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر گوشه‌ای فتنه‌ای خواسته است</p></div>
<div class="m2"><p>مگر یار بگشوده چشمان ز خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بآتش رخی کارم افتاده است</p></div>
<div class="m2"><p>که از عشق او گشته جانم کباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه تاب سر زلف پرچین او</p></div>
<div class="m2"><p>بدیدم بدادم ز کف صبر و تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش رخش آفتاب فلک</p></div>
<div class="m2"><p>بود همچو مه در بر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تنها ربوده است تاب از صغیر</p></div>
<div class="m2"><p>که برده است صبر از دل شیخ و شاب</p></div></div>