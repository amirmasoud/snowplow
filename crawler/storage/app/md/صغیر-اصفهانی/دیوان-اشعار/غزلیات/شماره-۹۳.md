---
title: >-
    شمارهٔ  ۹۳
---
# شمارهٔ  ۹۳

<div class="b" id="bn1"><div class="m1"><p>کی توان گفتن که او با ما سر و کاری نداشت</p></div>
<div class="m2"><p>حس عالمگیر او جز ما خریداری نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر مژگانش نکردی جز دل ما را نشان</p></div>
<div class="m2"><p>حلقهٔ زلفش بغیر از ما گرفتاری نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه کنعان خسرو مصر ملاحت بود لیک</p></div>
<div class="m2"><p>بی زلیخا حسن او گرمی بازاری نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارها جز عشق بازی سربسر بازیچه بود</p></div>
<div class="m2"><p>زان دل ما در جهان جز عاشقی کاری نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار محنت بردن آسانتر ز بار منت است</p></div>
<div class="m2"><p>شادمان آنکو بغم خو کرد و غمخواری نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گیتیش در دخمهٔ محو و فراموشی سپرد</p></div>
<div class="m2"><p>آنکه بگذشت وزنام نیک آثاری نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ هرگز بر مراد راستان دوری نزد</p></div>
<div class="m2"><p>راستی جز کجروی این سفله رفتاری نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو بلبل از چه مینالید روز و شب صغیر</p></div>
<div class="m2"><p>گر به پای دل ز عشق گلرخی خاری نداشت</p></div></div>