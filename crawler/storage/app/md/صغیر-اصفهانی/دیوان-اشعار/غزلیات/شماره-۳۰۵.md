---
title: >-
    شمارهٔ  ۳۰۵
---
# شمارهٔ  ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>یار نگذارد اگر لعل شکر خایش ببوسم</p></div>
<div class="m2"><p>در قفای او روم نقش کف پایش ببوسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بدست من نیفتد دامنش در رهگذرها</p></div>
<div class="m2"><p>خاک گردم سایهٔ قد دلارایش ببوسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بپوشد دیده از من تا نبوسم نرگسش را</p></div>
<div class="m2"><p>من صبا گردم سر زلف سمن سایش ببوسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بایمائی ز ابرو جان شیرین خواهد از من</p></div>
<div class="m2"><p>بی تامل جان کنم ایثار و ایمایش ببوسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر زمان خواهد بیاراید جمال خویشتن را</p></div>
<div class="m2"><p>من شوم آئینه عکس روی زیبایش ببوسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بجرم عشق دست جور بگشاید برویم</p></div>
<div class="m2"><p>ناله گردم تا دل چو سنک خارایش ببوسم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیلتی همچون صغیر انگیزم و هر گاه و بیگه</p></div>
<div class="m2"><p>در تصور آرم او را جمله اعضایش ببوسم</p></div></div>