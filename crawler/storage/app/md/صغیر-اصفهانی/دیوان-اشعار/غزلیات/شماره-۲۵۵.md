---
title: >-
    شمارهٔ  ۲۵۵
---
# شمارهٔ  ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>اهل دل بر در دل حلقه چو رندانه زدند</p></div>
<div class="m2"><p>نه در کعبه دگر نی در بتخانه زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می و میخواره و ساقی همه دیدند یکی</p></div>
<div class="m2"><p>عارفان از می‌توحید چو پیمانه زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان پا بسر جان بنهادند آنگاه</p></div>
<div class="m2"><p>دست در زلف خم اندر خم جانانه زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل صد چاک بدان طره تواند ره برد</p></div>
<div class="m2"><p>بی سبب خود بجنون مردم فرزانه زدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکجهت باش که مردان حق از یکجهتی</p></div>
<div class="m2"><p>خیمه بالاتر از این طارم نه گانه زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرمنی نیست که ایمن بود از آتش عشق</p></div>
<div class="m2"><p>این شرر بر دل هر عاقل و دیوانه زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باخبر باش که بیرون نبرندت از راه</p></div>
<div class="m2"><p>رهزنانی که ره خلق به افسانه زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون صغیر از پی آنطایفه میپوی که پای</p></div>
<div class="m2"><p>بسر عالمی از همت مردانه زدند</p></div></div>