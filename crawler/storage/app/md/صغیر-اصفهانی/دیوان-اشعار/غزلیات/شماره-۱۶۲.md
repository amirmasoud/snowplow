---
title: >-
    شمارهٔ  ۱۶۲
---
# شمارهٔ  ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>بگو به آنکه موفق بحسن تدبیر است</p></div>
<div class="m2"><p>بخود مناز که این هم بحکم تقدیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی اگر نه به تقدیر بسته سیر‌ام ور</p></div>
<div class="m2"><p>بگو که چیست ز تدبیرها عنان گیر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس اتفاق فتد اینکه با تمام قوا</p></div>
<div class="m2"><p>فلان‌ امیر جهان را به فکر تسخیر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز تیر مرادش نرفته سوی هدف</p></div>
<div class="m2"><p>که از کمان فلک خود نشانهٔ تیر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسا شده است که شه خفته شب بروی سریر</p></div>
<div class="m2"><p>علی الصباح بزندان و زیر زنجیر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیل پشه و بر شیر مور چیره شود</p></div>
<div class="m2"><p>در این بیان سخن افزون ز حد تقدیر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر ارادهٔ خالق نباشد اندر کار</p></div>
<div class="m2"><p>بگو اراده مخلوق را چه تأثیر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به غیر بندگی حق که نیمه مختاریست</p></div>
<div class="m2"><p>بدست بنده کدام اختیار و تدبیر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدا مصور و مخلوق عالمی تصویر</p></div>
<div class="m2"><p>صغیر مات مصور ز حسن تصویر است</p></div></div>