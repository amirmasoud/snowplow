---
title: >-
    شمارهٔ  ۵۹
---
# شمارهٔ  ۵۹

<div class="b" id="bn1"><div class="m1"><p>شرح حال من و زلف تو که در گلشن گفت</p></div>
<div class="m2"><p>که چو حال من و چون زلف تو سنبل آشفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش کردی چو به آهم تو تبسم گفتم</p></div>
<div class="m2"><p>مژده ایدل که بباد سحری غنچه شگفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باختم جان بتو ای ابروی جانان و هنوز</p></div>
<div class="m2"><p>می ندانم که تو را طاق بخوانم یا جفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر من گفت کسی قصهٔ فرهاد و مرا</p></div>
<div class="m2"><p>نرود تا ابد از یاد ز بس شیرین گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیست خاک در میخانه که هر اهل نظر</p></div>
<div class="m2"><p>بروی از اشک روان آب زد و از مژه رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن پیر خرابات به جان می ارزد</p></div>
<div class="m2"><p>لیک حرف من و زاهد همه میباشد مفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم بیمار بتان دید صغیر و از غم</p></div>
<div class="m2"><p>گشت بیمار بدانحال که در بستر خفت</p></div></div>