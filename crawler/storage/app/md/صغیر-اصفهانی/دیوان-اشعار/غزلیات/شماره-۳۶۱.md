---
title: >-
    شمارهٔ  ۳۶۱
---
# شمارهٔ  ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>از غبار خودنمائیها عمل را پاک کن</p></div>
<div class="m2"><p>دانه تا حاصل دهد پنهان بزیر خاک کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جمال شاهد مقصودت آید در نظر</p></div>
<div class="m2"><p>زنگ غفلت پاک از آئینهٔ ادراک کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم اکبر توئی هم‌صحبت عالم گزین</p></div>
<div class="m2"><p>در وجود خویش سیر انجم و افلاک کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سبک روحی کند شبنم سوی گردون سفر</p></div>
<div class="m2"><p>بهر معراج حقیقت خویش را چالاک کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای رباخوار از خدا باکی ندارد نفس تو</p></div>
<div class="m2"><p>دوستی را ترک با این دشمن بی‌باک کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با خدا کن جنگ خود تعطیل در ماه صیام</p></div>
<div class="m2"><p>لااقل یکماه در سال از ربا ‌امساک کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خو اطرت را تا فرحناکی شود حاصل صغیر</p></div>
<div class="m2"><p>رفع اندوه و ملال از خو اطری غمناک کن</p></div></div>