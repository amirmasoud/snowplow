---
title: >-
    شمارهٔ  ۳۸۱
---
# شمارهٔ  ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>صاحب علم و عمل را رتبهٔ والاستی</p></div>
<div class="m2"><p>قامت او در خور تشریف کرمناستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که را علم و عمل حاصل نشد از قول حق</p></div>
<div class="m2"><p>معنی بل هم اضل در حق او برجاستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فخر در علم و ادب باشد نه در اصل و نسب</p></div>
<div class="m2"><p>وین سخن قولولی خالق یکتاستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علم باشد نور و تابد هر که را از حق بدل</p></div>
<div class="m2"><p>دیده‌اش در این جهان و آن جهانی بیناستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه جز علم است‌ام روزت بکار آید ولی</p></div>
<div class="m2"><p>علم همراه تو هم‌ امروز و هم فرداستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم را توصیف این بس کز برای بوالبشر</p></div>
<div class="m2"><p>حق معلم گشت و شاهد علم الاسماستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر تعلیم و تعلم هر کجا بنیاد شد</p></div>
<div class="m2"><p>بهترین منزلگه و نیکوترین مأواستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حل شود از علم هر جا مشکلی باشد صغیر</p></div>
<div class="m2"><p>علم آری در جهان حلال مشکلهاستی</p></div></div>