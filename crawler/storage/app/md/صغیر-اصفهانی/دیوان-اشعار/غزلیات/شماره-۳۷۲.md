---
title: >-
    شمارهٔ  ۳۷۲
---
# شمارهٔ  ۳۷۲

<div class="b" id="bn1"><div class="m1"><p>ما را چو از عدم بوجود اوفتاد راه</p></div>
<div class="m2"><p>پنداشتیم دار فنا را قرارگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آغازمان برفت ز خاطر دوصد فسوس</p></div>
<div class="m2"><p>انجاممان به یاد نیاید هزار آه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باری چو هست اول و آخر اله و بس</p></div>
<div class="m2"><p>مائیم از اله روان جانب اله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نک در رهیم و هر نفس ماست یکقدم</p></div>
<div class="m2"><p>طی منازلست شب و روز و سال و ماه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اینره چو منتهی شد و ناگه اجل رسید</p></div>
<div class="m2"><p>هر بنده دمده باز کند بر لقای شاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن مردگان زنده بنازم گه در حیات</p></div>
<div class="m2"><p>درک حضور شاه کنند از علو جاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هان کب مرو که سوی شه ار راسب شد رهت</p></div>
<div class="m2"><p>بینی ورا معاینه بر صدر بارگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای آنکه راه راست طلب میکنی بحق</p></div>
<div class="m2"><p>بایست بردنت بحق از غیر حق پناه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر فرقه ات بهمرهی خود صلا زنند</p></div>
<div class="m2"><p>گر راه حق همی طلبی جز ز حق مخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حالی شبست و روز جزا میشود عیان</p></div>
<div class="m2"><p>راه که راست بوده و راه که اشتباه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سر در هوا مرو بنگر پیش پای خویش</p></div>
<div class="m2"><p>زان پیشتر که در نگری خویش را بچاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه جهان علی است برو در قفای او</p></div>
<div class="m2"><p>راهی که شاه رفته همانست شاهراه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از ما سوی صغیر گذشت و باو رسید</p></div>
<div class="m2"><p>دنبال او گرفت که لا هادیاً سواه</p></div></div>