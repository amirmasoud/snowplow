---
title: >-
    شمارهٔ  ۴۲۱
---
# شمارهٔ  ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>غنچه را نیست جز این علت خونین جگری</p></div>
<div class="m2"><p>که کند پردهٔ آن پاره نسیم سحری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایکه از عیب کسان پرده دری در خود بین</p></div>
<div class="m2"><p>آن چه عیب است که باشد بتر از پرده‌دری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر زنی لاف هنر عیب کسان کمتر جوی</p></div>
<div class="m2"><p>عیب‌جوئی نبود شیوهٔ مرد هنری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذر ما همه بر ساعد و دست و سر و پاست</p></div>
<div class="m2"><p>ما بخود باز نیائیم ببین خیره‌سری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوح را زمزمه این بود بوقت مردن</p></div>
<div class="m2"><p>طول‌ آمال چرا عمر بدین مختصری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیبخر ها خبر از غیب دهند این عجب است</p></div>
<div class="m2"><p>که خبرها همه پیدا شود از بی‌خبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مفلس ایمن بود از صدمهٔ ارباب طمع</p></div>
<div class="m2"><p>نخل آسوده ز سنگست گه بی‌ثمری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میدهد پند همی کیفیت قارونم</p></div>
<div class="m2"><p>که بکن خاک بفرق غم بی‌سیم و زری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بماند اثر نیک صغیر از تو نکوست</p></div>
<div class="m2"><p>ور نه باشد اثر بد بتر از بی‌اثری</p></div></div>