---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>چه مایه میدهد اردی‌بهشت بستان را</p></div>
<div class="m2"><p>که میزند به صفا طعنه باغ رضوان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدام جلوه به گلشن کند چنین دلکش</p></div>
<div class="m2"><p>نوای زیر و بم بلبل خوش الحان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز داغ دل عاشقان نشان جویی</p></div>
<div class="m2"><p>برو به باغ و ببین لاله‌های نعمان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی مقدر بی‌مثل و صانع بیچون</p></div>
<div class="m2"><p>که از جماد بر آرد ثبات الوان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترانهٔ و من الماء کل شیئی حی</p></div>
<div class="m2"><p>از آبشار به رقص آورد سخندان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نرگس آنکه قدح کش بود در این ایام</p></div>
<div class="m2"><p>به عالمی ندهد گوشهٔ گلستان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر معرفت واجبت بود واجب</p></div>
<div class="m2"><p>نتیجه‌گر طلبی کارگاه امکان را</p></div></div>