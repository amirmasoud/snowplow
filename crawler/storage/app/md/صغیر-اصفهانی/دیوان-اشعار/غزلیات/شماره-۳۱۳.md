---
title: >-
    شمارهٔ  ۳۱۳
---
# شمارهٔ  ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>ای قبلهٔ جان ما چو بکوی تو رسیدیم</p></div>
<div class="m2"><p>تسبیح بیفکنده و زنار بریدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پا بر سر بازار محبت چو نهادیم</p></div>
<div class="m2"><p>با نقد دل و دین غم عشق تو خریدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جان و سرو مال و خرد مذهب و آئین</p></div>
<div class="m2"><p>هر پرده که بد حایل ما و تو دریدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر صفت حسن هر آن نکته که خواندیم</p></div>
<div class="m2"><p>معنی همه در صورت زیبای تو دیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر به کمند سر زلف تو فتادیم</p></div>
<div class="m2"><p>زان پس که بهر بام نشستیم و پریدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردیم فراموش حدیث دو جهان را</p></div>
<div class="m2"><p>تا یکسخن از آن لب جانبخش شنیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک در پیر مغان سر چو نهادیم</p></div>
<div class="m2"><p>مانند صغیر از دو جهان پای کشیدیم</p></div></div>