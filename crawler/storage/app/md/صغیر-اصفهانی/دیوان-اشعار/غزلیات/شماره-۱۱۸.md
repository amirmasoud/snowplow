---
title: >-
    شمارهٔ  ۱۱۸
---
# شمارهٔ  ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>روزگاریست که چون حلقه مقیمم بدرت</p></div>
<div class="m2"><p>از چه لطفی نبود با من بی پا و سرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و پیمان تو از لاغری و از سستی</p></div>
<div class="m2"><p>نه عجب هر دو نیاییم اگر در نظرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه نهان از بصری و نه عیان در نظری</p></div>
<div class="m2"><p>می ندانم که پری نام نهم یا بشرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه هستی تو خراباتی و هرجایی لیک</p></div>
<div class="m2"><p>جز بخلوت نتوان دید رخ چون قمرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکرلله که شدی از رخ او عکس پذیر</p></div>
<div class="m2"><p>شستم ای آینهٔ دل چو ز خون جگرت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو جبریل بجایی نرسی ای سالک</p></div>
<div class="m2"><p>گر بدل هست غم سوختن بال و پرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشوی باخبر از یار و نیابی مقصود</p></div>
<div class="m2"><p>مگر آندم که ز خود هیچ نباشد خبرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایکه اندر طلبش گرد جهان میگردی</p></div>
<div class="m2"><p>تا که از پای نیفتی ننهد پا به سرت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قندش از یاد رود در همهٔ عمر صغیر</p></div>
<div class="m2"><p>هر که آگه شود از گفتهٔ همچون شکرت</p></div></div>