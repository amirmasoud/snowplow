---
title: >-
    شمارهٔ  ۷۶
---
# شمارهٔ  ۷۶

<div class="b" id="bn1"><div class="m1"><p>لب تو ریخت چنان آبروی آب حیات</p></div>
<div class="m2"><p>که رخت خویش ز خجلت کشید در ظلمات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و گذشتن از چون تو دلبری حاشا</p></div>
<div class="m2"><p>تو و نشستن با عاشقی چو من هیهات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه من وفات ندیدم که همچو من بسیار</p></div>
<div class="m2"><p>بیافتند وفات و نیافتند وفات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسیر دادن جان آئیم بسر ورنه</p></div>
<div class="m2"><p>گر از وفات بود آئی از چه وقت وفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زنند عشق پس از مرگ هم خلاصی نیست</p></div>
<div class="m2"><p>گمان مبر دهدت مرگ از این کمند نجات</p></div></div>