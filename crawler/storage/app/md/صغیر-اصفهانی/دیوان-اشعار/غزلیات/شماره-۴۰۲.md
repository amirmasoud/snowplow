---
title: >-
    شمارهٔ  ۴۰۲
---
# شمارهٔ  ۴۰۲

<div class="b" id="bn1"><div class="m1"><p>چشم مستت همه مردم کشد از بی باکی</p></div>
<div class="m2"><p>ایعجب مست که دیده است بدین چالاکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خو از آن کرد دلم با غم عشقت که ندید</p></div>
<div class="m2"><p>عشرتی در دو جهان خوشتر از این غمناکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همین تیره گی از بخت من‌ آموخته شام</p></div>
<div class="m2"><p>کز من‌ آموخته هم صبح گریبان چاکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک عربده جو رام شود انسان را</p></div>
<div class="m2"><p>غافل از خود مشو ای طرفه طلسم خاکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معرفت پیشه کن ای آنکه مقامی خواهی</p></div>
<div class="m2"><p>که بجایی نرسد مرد ز بی ادراکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامنی در کفش البته فتد همچو صغیر</p></div>
<div class="m2"><p>هر که در عشق نهد گام بدامن پاکی</p></div></div>