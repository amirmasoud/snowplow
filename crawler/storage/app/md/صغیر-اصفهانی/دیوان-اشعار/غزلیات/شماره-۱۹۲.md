---
title: >-
    شمارهٔ  ۱۹۲
---
# شمارهٔ  ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>راستی مردم از آندم که بصلب پدرند</p></div>
<div class="m2"><p>چون به بینی بره مرگ و فنا ره سپرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب اینست که این قافله روزان و شبان</p></div>
<div class="m2"><p>بعیان در حضرند و به نهان در سفرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این جهان رهگذر و مردم غافل ز خدای</p></div>
<div class="m2"><p>پی آزار هم‌ام اده در این رهگذرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر از جمعیت مشرق و مغرب دارند</p></div>
<div class="m2"><p>وز پریشانی همسایهٔ خود بی خبرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر خود حمله باندوختن مال جهان</p></div>
<div class="m2"><p>بگذرانند و گذارند و از آن درگذرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبشان روز درخشنده کسانی که ز مهر</p></div>
<div class="m2"><p>روز و شب در پی آسایش نوع بشرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز ره سعی و عمل آنچه که آرند بدست</p></div>
<div class="m2"><p>با محبت بخورانند و بعزت بخورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ دانی که بود زندهٔ جاوید صغیر</p></div>
<div class="m2"><p>خیرخواهان که در این مرحله صاحب اثرند</p></div></div>