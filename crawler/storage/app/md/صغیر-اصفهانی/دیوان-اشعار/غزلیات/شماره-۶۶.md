---
title: >-
    شمارهٔ  ۶۶
---
# شمارهٔ  ۶۶

<div class="b" id="bn1"><div class="m1"><p>نه خط بروی تو کین میر کشور زنگ است</p></div>
<div class="m2"><p>کشیده لشکر و با شاه روم در جنگ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیاه شد ز غمت روز ما چو شام ولی</p></div>
<div class="m2"><p>از آن خوشیم که با طرهٔ تو همرنگ است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآر کام من ای قبله من ابرویت</p></div>
<div class="m2"><p>که کام خلق دهد کعبه گرچه دل سنگ است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بند بند من آید نوای ناله چو نی</p></div>
<div class="m2"><p>ولی بگوش تو خوشتر ز نغمهٔ چنگ است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را ز محنت من هیچ غم نباشد لیک</p></div>
<div class="m2"><p>ز فرقت دهنت بهر من جهان تنگ است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پا فتادم و منزل نشد پدید آری</p></div>
<div class="m2"><p>رهیست عشق که بیرون ز میل و فرسنگ است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فریب نام صغیر از تو کی خورد زاهد</p></div>
<div class="m2"><p>بکیش باده کشان نام مایهٔ ننگ است</p></div></div>