---
title: >-
    شمارهٔ  ۲۸۵
---
# شمارهٔ  ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>برفت عمر و نگشتم ز هجر یار خلاص</p></div>
<div class="m2"><p>نشد دلم ز غم و رنج بیشمار خلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجایی ای اجل ای چاره ساز مهجوران</p></div>
<div class="m2"><p>بیا که سازیم از درد انتظار خلاص</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا نه پای اجل بوسم از سر رغبت</p></div>
<div class="m2"><p>که مینمایدم از دست روزگار خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>براستی که رود هر که از جهان بیرون</p></div>
<div class="m2"><p>شود ز بازی این چرخ کجمدار خلاص</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه سالهاست که دارم بدل غمی زانغم</p></div>
<div class="m2"><p>ندانم آنکه شوم کی من فکار خلاص</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلاص یافتم از صد هزار غم بجهان</p></div>
<div class="m2"><p>وزین غمم نشود جان بیقرار خلاص</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر از آن طلبم مرک تا مگر گردم</p></div>
<div class="m2"><p>از این غمی که بدان گشته‌ام دچار خلاص</p></div></div>