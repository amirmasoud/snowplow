---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>اگر به جرم محبت کنند پوست مرا</p></div>
<div class="m2"><p>نمی رود بدر از سر هوای دوست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رضای دوست گزیدم بخود چو دانستم</p></div>
<div class="m2"><p>که هرچه دوست پسندد همان نکوست مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخواب خوش همه شب مهر و ماه می‌بینم</p></div>
<div class="m2"><p>ز بسکه در نظر آن یار ماه روست مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدش بچشم پرآبم مدام جلوه گر است</p></div>
<div class="m2"><p>چه احتیاج به سرو کنار جوست مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من به هر دو جهان پا زند ز سرمستی</p></div>
<div class="m2"><p>هرآنکه نوشد از این می‌که در سبوست مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمانده در دل من هیچ آرزو لیکن</p></div>
<div class="m2"><p>چو عرش خاک نجف گشتن آرزوست مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر کرد غلامی حیدرم آزاد</p></div>
<div class="m2"><p>بلی چه باک ز حشرم که خواجه اوست مرا</p></div></div>