---
title: >-
    شمارهٔ  ۴۱۹
---
# شمارهٔ  ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>تا در ره مقصود به تشویش نیفتی</p></div>
<div class="m2"><p>از قافله باید که پس و پیش نیفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آزاد توانی شدن از دام دو گیتی</p></div>
<div class="m2"><p>زنهار به دام هوس خویش نیفتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتادنت از بام فلک غصه ندارد</p></div>
<div class="m2"><p>هشدار ز بام دل درویش نیفتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوی سبعیت بهل ای دوست که چون کلب</p></div>
<div class="m2"><p>در جامهٔ هر خستهٔ دل ریش نیفتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر بنه اندیشهٔ بد تا به مکافات</p></div>
<div class="m2"><p>در دام حریفان بد اندیش نیفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذر ز کم و بیش که میزان قناعت</p></div>
<div class="m2"><p>این است که در فکر کم و بیش نیفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیگانه گزندت نرساند به حذر باش</p></div>
<div class="m2"><p>در زحمت بیگانگی خویش نیفتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانند صغیر از اسد الله مدد جوی</p></div>
<div class="m2"><p>تا در کف گرگان جفا کیش نیفتی</p></div></div>