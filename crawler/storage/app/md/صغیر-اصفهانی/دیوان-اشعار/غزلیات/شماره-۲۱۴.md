---
title: >-
    شمارهٔ  ۲۱۴
---
# شمارهٔ  ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>در ملک وجود آنچه که از خاک برآید</p></div>
<div class="m2"><p>آن به که عدم گردد و از تاک برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید فلک تیره شود روز جهان شب</p></div>
<div class="m2"><p>گر دود دل من سوی افلاک برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکتن ز همه خلق جهان زنده نماند</p></div>
<div class="m2"><p>آن روز که شمشیر تو بی باک برآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقتول خم ابروی تو روز قیامت</p></div>
<div class="m2"><p>از زیر لحد با دل صد چاک برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دانی چه بود صیقل آئینهٔ خاطر</p></div>
<div class="m2"><p>آهی که سحر از دل غمناک برآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز عجز و تضرع ببرت تحفه چه آریم</p></div>
<div class="m2"><p>خاکیم و بجز عجز چه از خاک برآید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حشر کند دوزخیان را که بهشتی</p></div>
<div class="m2"><p>این کار صغیر از شه لولاک برآید</p></div></div>