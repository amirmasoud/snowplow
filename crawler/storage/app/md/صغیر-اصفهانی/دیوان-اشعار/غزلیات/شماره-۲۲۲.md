---
title: >-
    شمارهٔ  ۲۲۲
---
# شمارهٔ  ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>ما را به یمن عشق روا از توکام شد</p></div>
<div class="m2"><p>صد شکر کار ما به محبت تمام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوشین ببزم قصهٔ خوبان همی گذشت</p></div>
<div class="m2"><p>تا عاقبت به نام تو ختم کلام شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زین بعد ما و عشق تو زیرا که پیش از این</p></div>
<div class="m2"><p>پختیم هر خیال به عشق تو خام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با یاد موی و روی تو ما را چه سالها</p></div>
<div class="m2"><p>هی شام و صبح گشت و همی صبح و شام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حیرتم که راحتی اندر زمانه چیست</p></div>
<div class="m2"><p>یا بهر عاشقان تو راحت حرام شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از من بمرغهای چمن ای صبا بگوی</p></div>
<div class="m2"><p>کان مرغ پرشکسته گرفتار دام شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جامه ها نبود مرا غیر حرقه ئی</p></div>
<div class="m2"><p>آن خرقه هم بمیکده در رهن جام شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردون بود کمینه غلامی ز درگهش</p></div>
<div class="m2"><p>آنکو بدرگه شه مردان غلام شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از روی سعی کعبه کند طوف آندلی</p></div>
<div class="m2"><p>کو از صفا محبت وی را مقام شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهرش مجو صغیر ز هر سقلهٔ دنی</p></div>
<div class="m2"><p>این نعمت عظیم نصیب عظام شد</p></div></div>