---
title: >-
    شمارهٔ  ۱۹۹
---
# شمارهٔ  ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>مردم چشم تو نازیم که در پرده درند</p></div>
<div class="m2"><p>واندر آن پرده ز مردم بملا پرده درند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهنت هیچ و از آن هیچ بعشاق چنان</p></div>
<div class="m2"><p>کار تنک است که از هستی خود بیخبرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسته گرد لب شیرین تو خط مشگین</p></div>
<div class="m2"><p>یا که موران سیه جمع بدور شکرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنج خود کم کن و عشاقت از این بیش مکش</p></div>
<div class="m2"><p>زانکه این طایفه را هر چه کشی بیشترند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وادی عشق تو را بی سر و پا باید رفت</p></div>
<div class="m2"><p>زین سبب جملهٔ عشاق تو بی پا و سرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بخوبان نشوم رام که از گندم خال</p></div>
<div class="m2"><p>این بهشتی پسران راه زنان پدرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه چشمی نبود در خور آنروی صغیر</p></div>
<div class="m2"><p>قابل دیدن رخسار وی اهل نظرند</p></div></div>