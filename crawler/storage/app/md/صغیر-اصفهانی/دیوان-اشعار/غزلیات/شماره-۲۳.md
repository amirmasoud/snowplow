---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>داده ام جا به سر هوای تو را</p></div>
<div class="m2"><p>می زنم بوسه خاک پای تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر من جان من بلاگردان</p></div>
<div class="m2"><p>قد سر تا به پا بلای تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه بنواز و خواه بگدازم</p></div>
<div class="m2"><p>که به جان می‌خرم رضای تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهر و ماه اوفتاده از نظرم</p></div>
<div class="m2"><p>تا بدیدم مها لقای تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کنم شکر اینکه ایزد داد</p></div>
<div class="m2"><p>در کفم رشتهٔ ولای تو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد از حق بهشت می‌طلبد</p></div>
<div class="m2"><p>من سر کوی با صفای تو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظری بر صغیر کن جز تو</p></div>
<div class="m2"><p>که نوازد شها گدای تو را</p></div></div>