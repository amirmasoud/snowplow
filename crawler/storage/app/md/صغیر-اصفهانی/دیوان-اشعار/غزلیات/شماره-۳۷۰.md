---
title: >-
    شمارهٔ  ۳۷۰
---
# شمارهٔ  ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>عشقت سپرد از دل دیوانه بدیوانه</p></div>
<div class="m2"><p>این گنج کند منزل ویرانه بویرانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مجلس ما دلها بخشند بهم صهبا</p></div>
<div class="m2"><p>می دور زند اینجا پیمانه به پیمانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می با همه نوشیدم یک می‌همه جا دیدم</p></div>
<div class="m2"><p>هرچند که گردیدم میخانه بمیخانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید ضیاگستر نبود زیک افزونتر</p></div>
<div class="m2"><p>بینیش به هر کشور کاشانه به کاشانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل مکن آسانرا یکجا بفشان جانرا</p></div>
<div class="m2"><p>تا چند بری آنرا جانانه به جانانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سوختن ار لذت نبود ز سر همت</p></div>
<div class="m2"><p>گیرد ز چه رو سبقت پروانه بپروانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس بجهان آید دانی چه از آن زاید</p></div>
<div class="m2"><p>چون رفت بیفزاید افسانه به افسانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس قصر که از شاهان بنیاد شد و بر آن</p></div>
<div class="m2"><p>شد فاخته کوکو خوان دندانه به دندانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از طبع صغیر اکنون‌ام د غزلی بیرون</p></div>
<div class="m2"><p>شد گنج ورا افزون دردانه به دردانه</p></div></div>