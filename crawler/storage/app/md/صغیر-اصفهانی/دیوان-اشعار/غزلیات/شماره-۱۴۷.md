---
title: >-
    شمارهٔ  ۱۴۷
---
# شمارهٔ  ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>تنها ستمت بهر من ای ماه جبین است</p></div>
<div class="m2"><p>یا با همه بی‌مهری و آئین تو این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوتاه نمود از همه جا دست خیالم</p></div>
<div class="m2"><p>بالای بلندت که بلای دل و دین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ظلمت زلف تو دل خضر شود گم</p></div>
<div class="m2"><p>از بسکه شکن در شکن و چین سرچین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این زلف سیاه است بر آن روی چو خورشید</p></div>
<div class="m2"><p>یا روز به شب کفر باسلام قرین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا آنکه مگر ره به دهان تو برد دل</p></div>
<div class="m2"><p>عمریستکه چون خال لبت گوشه نشین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کوی خود ای دوست مرانم سوی جنت</p></div>
<div class="m2"><p>بی روی توام کی سر فردوس برین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکذره مرا کار به خورشید فلک نیست</p></div>
<div class="m2"><p>خورشید من اینست که بر روی زمین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکدم نرود عمر که بی غصه نشینم</p></div>
<div class="m2"><p>تا رفته غم از دل غم دیگر به کمین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش دار صغیر آنچه که پیش آیدت آخر</p></div>
<div class="m2"><p>تا چند توان گفت چنانست و چنین است</p></div></div>