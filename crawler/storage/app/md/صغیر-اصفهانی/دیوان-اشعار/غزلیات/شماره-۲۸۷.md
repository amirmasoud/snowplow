---
title: >-
    شمارهٔ  ۲۸۷
---
# شمارهٔ  ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>بی دوست دست می‌ندهد بهر ما نشاط</p></div>
<div class="m2"><p>الا به وصل دوست نداریم انبساط</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم که با حبیب نشینیم فارغیم</p></div>
<div class="m2"><p>از خوف و ‌امن و راحت و محنت غم و نشاط</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن کس که کرد طی ره باریک عشق را</p></div>
<div class="m2"><p>گو شاد باش زانکه گذر کردی از صراط</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مربوط شد به عالم انسانیت یقین</p></div>
<div class="m2"><p>با اهل عشق یافت هر آن کس که ارتباط</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد مگو فسانه که بیزار شد دلم</p></div>
<div class="m2"><p>غیر از حدیث عشق ز هر گونه اختلاط</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آن که آرزوی سلیمانیت بود</p></div>
<div class="m2"><p>برگو که در کجاست سلیمان چه شد بساط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آرزوی منزل مقصود سوختیم</p></div>
<div class="m2"><p>خرم دمی که رخت ببندیم زی رباط</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سوی خدا گریخت صغیر از خودی بلی</p></div>
<div class="m2"><p>جز بر محیط رو به کجا آورد محاط</p></div></div>