---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>شد بزلف یار هر که مبتلا</p></div>
<div class="m2"><p>پا نمیکشد از سرش بلا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که را بخویش خواند از کرم</p></div>
<div class="m2"><p>بهر کشتنش می‌زند صلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه میکشد عاشقان خود</p></div>
<div class="m2"><p>گشته کوی او دشت کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل نهان کند عشق او ولی</p></div>
<div class="m2"><p>راز دل کند دیده بر ملا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی شود غرق بحر عشق</p></div>
<div class="m2"><p>کار افتدش با نهنگ لا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک می‌مکن زانکه میدهد</p></div>
<div class="m2"><p>دیده را ضیا سینه را جلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد ز عشق یار حاصل صغیر</p></div>
<div class="m2"><p>غصه و الم رنج و ابتلا</p></div></div>