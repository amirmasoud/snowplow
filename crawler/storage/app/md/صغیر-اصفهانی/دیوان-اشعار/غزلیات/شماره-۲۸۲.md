---
title: >-
    شمارهٔ  ۲۸۲
---
# شمارهٔ  ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>سلطان نفس خود شو و مالک رقاب باش</p></div>
<div class="m2"><p>روشن ضمیر خویش کن و آفتاب باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی بقاف قرب رسی از تمام خلق</p></div>
<div class="m2"><p>سیمرغ وار در پس قاف حجاب باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی روی بمدرسه از بهر قیل و قال</p></div>
<div class="m2"><p>تدریس خویشتن کن وام الکتاب باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین قیل وقال بگذر و الهام حق شنو</p></div>
<div class="m2"><p>مانند کوه تشنهٔ فیض سحاب باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابلیس از غرور عبادت رجیم شد</p></div>
<div class="m2"><p>ای شیخ بر حذر ز غرور ثواب باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر بد حساب بود گیرودار حشر</p></div>
<div class="m2"><p>زان گیرودار تا برهی خوش حساب باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر ز کام هر دو جهان در طریق عشق</p></div>
<div class="m2"><p>همچون صغیر از دو جهان کامیاب باش</p></div></div>