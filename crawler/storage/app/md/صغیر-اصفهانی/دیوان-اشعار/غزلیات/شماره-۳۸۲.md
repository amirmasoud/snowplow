---
title: >-
    شمارهٔ  ۳۸۲
---
# شمارهٔ  ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>زالعلم حجاب الاکبر‌ام د عقل سودائی</p></div>
<div class="m2"><p>که دانائیست نادانی و نادانیست دانائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی علمیکه بر جهلت بیفزاید از آن باید</p></div>
<div class="m2"><p>کنی پرهیز ورنه میکشد کارت برسوائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بتحقیق اطلبواالعلم ولو باالصین که گفت احمد</p></div>
<div class="m2"><p>نبودش مقصد از آن علم جز علم شناسائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحکم ان بعض الظن اثم ای عالم خودبین</p></div>
<div class="m2"><p>همان بهتر برندان دیدهٔ تحقیر نگشائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن علمی که باد نخوت آرد باده خوردن به</p></div>
<div class="m2"><p>تو و آن باد پیمائی من و این باده پیمائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعلم و عقل هرگز درنیایی ذوق عرفانرا</p></div>
<div class="m2"><p>مگر عاشق شوی روزی وزین خوان لقمه بربائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برو حالی بدست آور که عمری کرده‌ای ضایع</p></div>
<div class="m2"><p>اگر از رنج عقل و قال بیحاصل نیاسائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>توان دیدن صغیر از دل جمال شاهد یکتا</p></div>
<div class="m2"><p>ولی هرگاه زین آئینه زنگ شرک بزدائی</p></div></div>