---
title: >-
    شمارهٔ  ۳۴۹
---
# شمارهٔ  ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>دوش بهر گریه در هجرت مجالی داشتم</p></div>
<div class="m2"><p>با خیال طره‌ات آشفته حالی داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد ایامی که با لبخندی از لعل لبت</p></div>
<div class="m2"><p>میزدودی از دل من گر ملالی داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میگرفتم از اشارتهای ابرویت جواب</p></div>
<div class="m2"><p>در ضمیر از هر دو عالم گر سئوالی داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاش بودم با کبوترهای بامت همنشین</p></div>
<div class="m2"><p>باز خوش بودم اگر بشکسته بالی داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کنون هجر تو جسم و جان من فرسوده بود</p></div>
<div class="m2"><p>در دل خود گر نه‌ام ید وصالی داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیر شمشیر تو کردم ناله از بی‌طاقتی</p></div>
<div class="m2"><p>در شهیدانت از این فعل انفعالی داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این غزل با سوز دل‌ام یخت پا تا سر صغیر</p></div>
<div class="m2"><p>چونکه در دل لاله‌سان داغ غزالی داشتم</p></div></div>