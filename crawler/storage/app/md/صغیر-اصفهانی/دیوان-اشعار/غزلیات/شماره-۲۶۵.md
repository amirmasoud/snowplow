---
title: >-
    شمارهٔ  ۲۶۵
---
# شمارهٔ  ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>هر چه خواهم سخن از زلف تو سازم تحریر</p></div>
<div class="m2"><p>درهم افتد خط و گردد همه شکل زنجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب دیدم که رسیدم به لب آب بقا</p></div>
<div class="m2"><p>ای بت نوش لب این خواب چه دارد تعبیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نالهٔ من که اثر در دل فولاد کند</p></div>
<div class="m2"><p>در دل سخت تو از چیست ندارد تأثیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه همین من به کمند تو گرفتارم و بس</p></div>
<div class="m2"><p>کیست آنکس که نباشد بکمند تو اسیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژه و ابرویت ای ترک چه خواهند ز خلق</p></div>
<div class="m2"><p>کاین یک از تیغ ببندد ره و آن یک از تیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک کوی تو شود هر که بکوی تو فتد</p></div>
<div class="m2"><p>بسکه خاک سر کوی تو بود دامن گیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشبی وصل تو ای یار شود پیر جوان</p></div>
<div class="m2"><p>بدمی هجر تو ای دوست جوان گردد پیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مقامی که به عشاق دهی شربت وصل</p></div>
<div class="m2"><p>هست شایسته که اول بچشانی به صغیر</p></div></div>