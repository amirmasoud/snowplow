---
title: >-
    شمارهٔ  ۱۱۶
---
# شمارهٔ  ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>دلبرا نرا همه سخت است دل و پیمان سست</p></div>
<div class="m2"><p>یا که ای سنگدل این قاعده در مذهب تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ پیکان ز کمانخانهٔ ابروت نجست</p></div>
<div class="m2"><p>تا که اول دل زاری هدف خویس نجست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که سر خوان غم عشق تو ایدوست نشست</p></div>
<div class="m2"><p>کاولین مرتبه از هستی خود دست نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دلی را چو دل من فلک سفله نخست</p></div>
<div class="m2"><p>چه کنم این شده تقدیر من از روز نخست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان هیچکس از قید غم و غصه نرست</p></div>
<div class="m2"><p>بیغمی هست گیاهی که در این باغ نرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایندرست استکه حق در دل بشکسته درست</p></div>
<div class="m2"><p>کز شکست دل بشکسته شود کار درست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفرین بر تو صغیرا دگر این قاعده چیست</p></div>
<div class="m2"><p>که چنین طبع تواش کرد بیان چابک و چست</p></div></div>