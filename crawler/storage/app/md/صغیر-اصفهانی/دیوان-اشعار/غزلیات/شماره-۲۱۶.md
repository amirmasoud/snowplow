---
title: >-
    شمارهٔ  ۲۱۶
---
# شمارهٔ  ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>چون بدل ناوک آن شوخ پری زاد‌ آمد</p></div>
<div class="m2"><p>شست او را ز دل آواز مریزاد‌ آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوش می‌گفت که با آتش عشقم چونی</p></div>
<div class="m2"><p>ای عجب از من دل سوخته اش یاد‌ آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پری رو که تنت هست بنرمی چو حریر</p></div>
<div class="m2"><p>سخت تر از چه دلت ز آهن و فولاد‌ آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگران نقل و می‌و بوسه گرفتند از تو</p></div>
<div class="m2"><p>این منم کز تو نصیبم همه بیداد‌ آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ابد باد در میکده یارب مفتوح</p></div>
<div class="m2"><p>زانکه هر غمزدهٔی رفت در آن شاد‌ آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که طفل دل خود داد بشاگردی عشق</p></div>
<div class="m2"><p>به هنرهای جهان یکسره استاد‌ آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند ای شمع شرر بر پر پروانه زنی</p></div>
<div class="m2"><p>باخبر باش پی کشتن تو باد‌ آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد از آنی که ز شفقت دل شیرین شد نرم</p></div>
<div class="m2"><p>آهنین تیشه شد و بر سر فرهاد‌ آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوش بنوشت چو شرح ستم یار صغیر</p></div>
<div class="m2"><p>دفتر اوراق شد و خامه به فریاد‌ آمد</p></div></div>