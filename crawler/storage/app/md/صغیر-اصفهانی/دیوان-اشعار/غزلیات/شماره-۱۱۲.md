---
title: >-
    شمارهٔ  ۱۱۲
---
# شمارهٔ  ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>اکنون که بهار آمد و ایام بکام است</p></div>
<div class="m2"><p>در باغ گه بوسه زدن برلب جام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانند حلال از چه سبب خون کسان را</p></div>
<div class="m2"><p>آن قوم که در مذهبشان باده حرام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خلق همه در پی ترتیب کلامند</p></div>
<div class="m2"><p>گوش دل ما در پی تأثیر کلام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عقل نبرده است کسی راه بمقصود</p></div>
<div class="m2"><p>صد شکر که ما را بکف عشق زمام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق است چو عنقا و نشیمن بودش قاف</p></div>
<div class="m2"><p>عقل است چو عصفور و مقامش لب بام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کار که اتمام پذیرفته به عالم</p></div>
<div class="m2"><p>نیک ار نگری از مدد عشق تمام است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی عشق صغیرا نرسد کار باتمام</p></div>
<div class="m2"><p>آن کار که بی عشق تمام است کدامست</p></div></div>