---
title: >-
    شمارهٔ  ۲۵۳
---
# شمارهٔ  ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>ساقی از رطل گران دوش سبکبارم کرد</p></div>
<div class="m2"><p>تهی از خویش به یک ساغر سرشارم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش از این زودتر‌ام وختی استاد مرا</p></div>
<div class="m2"><p>پیشهٔ عشق که وارسته ز هر کارم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب از صورت زیبای تو ای معنی حسن</p></div>
<div class="m2"><p>که تماشای تو چون صورت دیوارم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلوه داد آن که گل روی ترا در نظرم</p></div>
<div class="m2"><p>سیر از سیر گل و گردش گلزارم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیشتر دارمت از جان خود ایدوست عزیز</p></div>
<div class="m2"><p>گرچه عشقت ببر خلق جهان خوارم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکرلله شدم آزاد ز هر دام که بود</p></div>
<div class="m2"><p>تا که تقدیر بدام تو گرفتارم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سالها معتکف صومعه بودم آخر</p></div>
<div class="m2"><p>عشقت انگشت نمای سر بازارم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فخرم این بس بدو عالم که خدا همچو صغیر</p></div>
<div class="m2"><p>یکجهت بندهٔ درگاه ده و چارم کرد</p></div></div>