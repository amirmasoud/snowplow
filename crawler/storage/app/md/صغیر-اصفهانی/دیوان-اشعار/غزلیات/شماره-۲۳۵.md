---
title: >-
    شمارهٔ  ۲۳۵
---
# شمارهٔ  ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>نگارا جانم از هجر تو نالان تا بکی باشد</p></div>
<div class="m2"><p>چو گیسوی تو احوالم پریشان تابکی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاد طره همچون شب و رخسار چون روزت</p></div>
<div class="m2"><p>شب و روزم بپیش دیده یکسان تا بکی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزن بر خرمنم یکباره آتش همچو پروانه</p></div>
<div class="m2"><p>چو شمعم جان بناکامی گدازان تا بکی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی چاه زنخدان و سر زلف چو زنجیرت</p></div>
<div class="m2"><p>دلم چونیوسف اندر چاه و زندان تا بکی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکن دست رقیب از لعل کوتاه ایپری پیکر</p></div>
<div class="m2"><p>بدست اهرمن مهر سلیمان تا بکی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآ از پرده ای مه تا فتد خورشید از گردون</p></div>
<div class="m2"><p>خود این بی پا و سر سرگرم جولان تا بکی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمایان ساز رخ این شهسوار عرصه خوبی</p></div>
<div class="m2"><p>صغیر از اشتیاقت مات و حیران تا بکی باشد</p></div></div>