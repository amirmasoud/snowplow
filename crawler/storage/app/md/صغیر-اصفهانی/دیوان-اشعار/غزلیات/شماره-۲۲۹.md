---
title: >-
    شمارهٔ  ۲۲۹
---
# شمارهٔ  ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>می بنوش اکنون که چون هنگام محشر میشود</p></div>
<div class="m2"><p>تاک طوبی گردد و خمخانه کوثر میشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوش آندردی که چون در ساغر صافش کنی</p></div>
<div class="m2"><p>عکس موجودات عالم نقش ساغر میشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهدان از زهد خشک و ما بمی تر دامنیم</p></div>
<div class="m2"><p>آتش آیا شعله ور در خشگ یا تر میشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان برقص آید مدامم زانکه هر سو بنگرم</p></div>
<div class="m2"><p>پیش چشمم صورت جانان مصور میشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون معلق زلف او دیدم بآذرگون عذار</p></div>
<div class="m2"><p>شد یقینم کافران را جا در آذر میشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهکن زد تیشه بر سر خویش را از پا فکند</p></div>
<div class="m2"><p>تا بدانی طی راه عشق از سر میشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگذری تا در ره جانان ز جان خود صغیر</p></div>
<div class="m2"><p>کی حیات جاودان بهرت میسر میشود</p></div></div>