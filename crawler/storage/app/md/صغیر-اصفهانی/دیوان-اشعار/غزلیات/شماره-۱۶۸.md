---
title: >-
    شمارهٔ  ۱۶۸
---
# شمارهٔ  ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>ببزم دل چو برافروختم ز عشق سراج</p></div>
<div class="m2"><p>دگر به مشعل خورشید نیستم محتاج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در سفینهٔ عشقم چه باک از این دارم</p></div>
<div class="m2"><p>که بحر حادثه از چار سو بود مواج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببوسه ای ز لبش زندهٔ ابد گشتم</p></div>
<div class="m2"><p>دهید مژده که جستم بدرد مرگ علاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم از تو چه اندر سر ا ست مردم را</p></div>
<div class="m2"><p>که دیده در بر تیر تو می‌کنند آماج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بماه روی تو نازم که سیم و زر شب و روز</p></div>
<div class="m2"><p>طبق طبق ز مه و مهر می‌ستاند باج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از دو کون به میخانه روی آوردم</p></div>
<div class="m2"><p>کجا روم گر از این درگهم کنند اخراج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهم کلاه نمد کج بشکر درویشی</p></div>
<div class="m2"><p>که شاه هیچ بجز دردسر ندید از تاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر هستی خود داد از آن بباد فنا</p></div>
<div class="m2"><p>که دید عاقبتش می‌کند اجل تاراج</p></div></div>