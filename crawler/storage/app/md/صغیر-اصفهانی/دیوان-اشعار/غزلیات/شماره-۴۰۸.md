---
title: >-
    شمارهٔ  ۴۰۸
---
# شمارهٔ  ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>گر نه ای دل پای بند طرهٔ طرار یاری</p></div>
<div class="m2"><p>از چه دایم تیره بختی از چه هر شب بیقراری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه عشق آن گل رخساره ات افتاده بر دل</p></div>
<div class="m2"><p>چون دل من از چه روای لاله دایم داغداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر درآید در چمن آن سرو گل رخسار روزی</p></div>
<div class="m2"><p>با قدش ایسرو پستی با لبش اینغنچه خواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عذر خواهم از خطای رفته ای گیسوی جانان</p></div>
<div class="m2"><p>خوانده‌ام کر عنبرت یا گفته‌ام مشک تتاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دو عالم از تو شاد و خرمم ای خط دلبر</p></div>
<div class="m2"><p>کانجانهم را بهشتی این جهانم را بهاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایخوش آندم کاینکدورت از میان نخیزد ببینم</p></div>
<div class="m2"><p>من ترا اندر کنارم تو مرا اندر کناری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی گر جان شیرین بیتو من چونتلخ کامم</p></div>
<div class="m2"><p>ورنئی عمر عزیزم از چه اینسان در گذاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این سر و جان صغیر آن تیغ ابروی تو جانا</p></div>
<div class="m2"><p>هر چه میخواهی بکن با او که صاحب اختیاری</p></div></div>