---
title: >-
    شمارهٔ  ۱۱۵
---
# شمارهٔ  ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>چون خصر ره بچشمهٔ حیوانم آرزوست</p></div>
<div class="m2"><p>یعنی دو بوسه زان لب خندانم آرزوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صیاد تا به دام تو گردیده ام اسیر</p></div>
<div class="m2"><p>دیگر نه طرف باغ و نه بستانم آرزوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نسبتی بزلف تو پیدا کنم مدام</p></div>
<div class="m2"><p>سرگشتگی و حال پریشانم آرزوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر کف گرفته ام پی ایثار جان و سر</p></div>
<div class="m2"><p>دیدار روی دلکش جانانم آرزوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهم که رو بکعبهٔ مقصود آورم</p></div>
<div class="m2"><p>یعنی جوار شاه خراسانم آرزوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم صغیر سیل سرشگت جهان گرفت</p></div>
<div class="m2"><p>گفتا چو نوح دیدن طوفانم آرزوست</p></div></div>