---
title: >-
    شمارهٔ  ۳۵۷
---
# شمارهٔ  ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>هرکس ندیده غارت و یغمای ترکمن</p></div>
<div class="m2"><p>ببند اسیر بردن و تاراج تُرک من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم که دل از او نتوانم دگر گرفت</p></div>
<div class="m2"><p>مشکل بود اسیر گرفتن ز ترکمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم مگر دو اسبه گریزم ز دست غم</p></div>
<div class="m2"><p>آن هم نشسته بود چو دیدم به تَرک من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از من که تَرک جان بنمودم به راه یار</p></div>
<div class="m2"><p>غیر از وفا چه دید که بنمود تَرک من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تاج شه صغیر برابر نمی‌کنم</p></div>
<div class="m2"><p>این افسر نمد که تو بینی به تَرک من</p></div></div>