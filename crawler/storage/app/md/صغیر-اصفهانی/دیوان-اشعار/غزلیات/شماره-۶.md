---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>چو من آراستم ز آیینه دل جلوه‌گاهش را</p></div>
<div class="m2"><p>ز مهر افکند در آن عکس روی به ز ماهش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه غمزه در هر ملک دل کانشه برانگیزد</p></div>
<div class="m2"><p>بویران ساختن اول دهد فرمان سپاهش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه تنها بر دلم تیر نگاه انداخت کز مژگان</p></div>
<div class="m2"><p>هزاران تیر آمد در قفا تیر نگاهش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهشتی رو بتی دارم که بهر سرمهٔ چشمان</p></div>
<div class="m2"><p>بزلف عنبرین روبند حوران خاک راهش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من دانم خراب و مات و بیخود گردی ای ناصح</p></div>
<div class="m2"><p>اگر چون من ببینی عشوه های گاه گاهش را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدشت عشق ای یاران کدامین ابر میبارد</p></div>
<div class="m2"><p>که غیر از درد و رنج و غم نمیبینم گیاهش را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی گر خواهد از حال صغیر آگه شود برگو</p></div>
<div class="m2"><p>بپرس از ماهی و مه داستان اشک و آهش را</p></div></div>