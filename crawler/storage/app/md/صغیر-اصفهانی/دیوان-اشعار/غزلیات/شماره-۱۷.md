---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>موشکافی به جهان گرچه بود پیشهٔ ما</p></div>
<div class="m2"><p>به میان تو نبرده است ره اندیشهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما که داریم بعشق تو عنان تا چه کند</p></div>
<div class="m2"><p>دل چون سنگ تو با این دل چون شیشهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی را هنری پیشه و کاری در پیش</p></div>
<div class="m2"><p>نیست جز عشق تو ای رشک پری پیشهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از فتنهٔ چشم تو بیابیم امان</p></div>
<div class="m2"><p>نیست از فتنهٔ دور فلک اندیشهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با چنین اشگ روان سرخوش و خرم دایم</p></div>
<div class="m2"><p>چون نباشیم که در آب بود ریشهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدعی تیشهٔ ما آه بود ریشهٔ خود</p></div>
<div class="m2"><p>رو نگهدار و بپرهیز از این تیشهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهوی چشم بتانیم در اندیشه صغیر</p></div>
<div class="m2"><p>با وجودی که رمد شیر نر از بیشهٔ ما</p></div></div>