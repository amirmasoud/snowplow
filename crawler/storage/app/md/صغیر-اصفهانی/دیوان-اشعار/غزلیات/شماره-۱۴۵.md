---
title: >-
    شمارهٔ  ۱۴۵
---
# شمارهٔ  ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>آنکه بهر جا عیان طلعت نیکوی اوست</p></div>
<div class="m2"><p>ای عجب از چشم خلق از چه نهان روی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز کنند آرزو تا سخنش بشنوند</p></div>
<div class="m2"><p>آنکه سرای وجود پر ز هیاهوی اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر ز دیدار او باز ندارد مرا</p></div>
<div class="m2"><p>چشم سرم سوی غیر چشم دلم سوی اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چارهٔ دیوانگی سلسله است و مرا</p></div>
<div class="m2"><p>مایهٔ دیوانگی سلسلهٔ موی اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزم مرا مشک و عود گر نبود گو مباش</p></div>
<div class="m2"><p>مشگ من و عود من جعد سمن بوی اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه دراز است باز هر شب اگر چه ببزم</p></div>
<div class="m2"><p>از سر شب تا بصبح صحبت گیسوی اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قصه مخوان زاهدا دوزخ و جنت تراست</p></div>
<div class="m2"><p>دوزخ من خوی یار جنت من روی اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سروری عالمی، پادشه عشق راست</p></div>
<div class="m2"><p>زانکه سر سروران خاک سر کوی اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از چه فکنده است شور در همه خلق جهان</p></div>
<div class="m2"><p>گرنه قیامت صغیر قامت دلجوی اوست</p></div></div>