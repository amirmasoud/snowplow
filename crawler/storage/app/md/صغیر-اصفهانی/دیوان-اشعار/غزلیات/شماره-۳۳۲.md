---
title: >-
    شمارهٔ  ۳۳۲
---
# شمارهٔ  ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>من نخواهم بکسی زهد و ریا بفروشم</p></div>
<div class="m2"><p>گو همه خلق بدانند که می‌مینوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واعظ بیهده‌ام وعظ مفرما که بود</p></div>
<div class="m2"><p>در بر پیر مغان رهن کلامی گوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستبرد غم عشق تو بنازم ای دوست</p></div>
<div class="m2"><p>که ببرداست ز تن طاقت و از سر هوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه از بهر نثار قدمت بود سرم</p></div>
<div class="m2"><p>زیر این بار گران هیچ نرفتی دوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پای تا سر همه در ذکر گل روی توام</p></div>
<div class="m2"><p>گرچه لب دوخته و غنچه صفت خاموشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با کسی انس نگیرد دلم از خلق جهان</p></div>
<div class="m2"><p>جز خیالت که مصور شده در آغوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونکه غمگینی من باعث خرسندی تست</p></div>
<div class="m2"><p>روز و شب در پی غمگینی خود میکوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زمان روی تو را مینگرم همچو صغیر</p></div>
<div class="m2"><p>دیده یکبارگی از هر دو جهان میپوشم</p></div></div>