---
title: >-
    شمارهٔ  ۸۷
---
# شمارهٔ  ۸۷

<div class="b" id="bn1"><div class="m1"><p>جمله خوبی های عالم در محبت مضمر است</p></div>
<div class="m2"><p>هر چه فعل نیک باشد مشتق از این مصدر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که را نبود محبت باید او را سوختن</p></div>
<div class="m2"><p>زانکه در بستان گیتی او نهال بی بر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی محبت هیچ موجودی نباید در وجود</p></div>
<div class="m2"><p>آفرینش را همانا مایه از این گوهر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز محبت را نداند جلوهٔ اول ز ذات</p></div>
<div class="m2"><p>هر که بعد از کنت کنز احببت را مستحضر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که چون سیمرغ بر قاب سعادت اوج یافت</p></div>
<div class="m2"><p>مرغ جانش را همان بال محبت شهپر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از محبت انبیا خواندند مردم را بحق</p></div>
<div class="m2"><p>گر توانی درک کن کاین رتبه پیغمبر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کیمیا را از محبت باشد اندر مس اثر</p></div>
<div class="m2"><p>هر که را باشد صغیر این کمیا کان زر است</p></div></div>