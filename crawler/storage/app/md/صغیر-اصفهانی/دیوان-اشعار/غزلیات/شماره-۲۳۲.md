---
title: >-
    شمارهٔ  ۲۳۲
---
# شمارهٔ  ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>هر دل به ‌امیدی پی دلدار برآمد</p></div>
<div class="m2"><p>زان جمله دل من پی دیدار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی همه جا مینگرم طلعت او را</p></div>
<div class="m2"><p>صد شکر که کام دلم از یار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگوش من آن زمزمه از جمله اشیاء</p></div>
<div class="m2"><p>آید که ز منصور سر دار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در قدم یار بیفکندم و صد شکر</p></div>
<div class="m2"><p>کز دست من دلشده این کار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز اهل دل او را نشناسد و نبینند</p></div>
<div class="m2"><p>با آنکه ز خلوت سر بازار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>الحق که بود آیتی از حس و جمالش</p></div>
<div class="m2"><p>هر گل که بطرف چمن از خار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می خواست که از اهل نظر دل برباید</p></div>
<div class="m2"><p>با این همه کرو فر و آثار برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حیف است نرفتن بخریداری آنشوخ</p></div>
<div class="m2"><p>کانشوخ بدنبال خریدار برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش چنک تو افکند بدان طره صغیرا</p></div>
<div class="m2"><p>آهی که ترا از دل افکار برآمد</p></div></div>