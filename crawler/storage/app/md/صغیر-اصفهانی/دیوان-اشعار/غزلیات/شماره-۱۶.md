---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>گر نبینم روزی آن مهر جهان افروز را</p></div>
<div class="m2"><p>تیره تر از شب به بیند چشم من آن روز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بشوق تیر مژگانش کشد از دیده سر</p></div>
<div class="m2"><p>تا مگر گردد هدف آن ناوک دل دوز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواست تا خلق جهانش بنده فرمان شوند</p></div>
<div class="m2"><p>زان خدایش داد این حسن جهان افروز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد سر نالایقم خاک ره پیر مغان</p></div>
<div class="m2"><p>اختر مسعود بین و طالع فیروز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایمم در نیستی رغبت فزاید آنچنانک</p></div>
<div class="m2"><p>حرص افزاید حریص سیم و زراندوز را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنگ از من یاد دارد این خروش دلخراش</p></div>
<div class="m2"><p>نی زمن آموخته این نالهٔ جان سوز را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کس به عالم کی شود استاد در کاری صغیر</p></div>
<div class="m2"><p>تا نکو شد خدمت استاد کارآموز را</p></div></div>