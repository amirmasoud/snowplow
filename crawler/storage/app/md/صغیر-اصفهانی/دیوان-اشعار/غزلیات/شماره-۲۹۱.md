---
title: >-
    شمارهٔ  ۲۹۱
---
# شمارهٔ  ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>عمریست تا به تیر غمت گشته‌ام هدف</p></div>
<div class="m2"><p>شاید زمام وصل تو را آورم بکف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازم بگیسوی تو که در آرزوی آن</p></div>
<div class="m2"><p>بس روزها سیه شد و بس عمرها تلف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکباره گر نقاب بگیری ز روی خود</p></div>
<div class="m2"><p>مه یکطرف برآید و خورشید یکطرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان و چشم مست تو را هر که دید گفت</p></div>
<div class="m2"><p>لشگر برابر شه ترکان کشیده صف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر مینهم بپای تو با عجز و با نیاز</p></div>
<div class="m2"><p>جان میدهم براه تو با شوق و با شعف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادا همیشه غرقهٔ دریای ابتلا</p></div>
<div class="m2"><p>آندل که نیست گوهر عشق تو را صدف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل در آز پرده که راز تو گفته شد</p></div>
<div class="m2"><p>با صورت تار و نغمهٔ مزمار و بانگ دف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیر مغان گرم بغلامی کند قبول</p></div>
<div class="m2"><p>در روزگار هست مرا بس همین شرف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خلق این زمانه نشد حل مشگلی</p></div>
<div class="m2"><p>دست صغیر و دامن شاهنشه نجف</p></div></div>