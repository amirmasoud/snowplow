---
title: >-
    شمارهٔ  ۳۴۸
---
# شمارهٔ  ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>خوانند گر چه خلق جهان اصفهانیم</p></div>
<div class="m2"><p>زین آب و خاک زاده‌ام‌ اما جهانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با بشر برادرم و زادهٔ جهان</p></div>
<div class="m2"><p>زنهار اصفهانی تنها نخوانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با نوع خویش در همه جا زیر آسمان</p></div>
<div class="m2"><p>روشن چو آفتاب بود مهربانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد زندگانیم بغم نوع خویش صرف</p></div>
<div class="m2"><p>این دولت است ماحصل زندگانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهم ز حق خود همه باشند کامیاب</p></div>
<div class="m2"><p>حق داند این بود بجهان کامرانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانم که هر چه بهر تو خواهم همان مراست</p></div>
<div class="m2"><p>ای دوست استفاده کن از نکته دانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بدگمان بعکس تو پیوسته حال من</p></div>
<div class="m2"><p>نیک است زانکه برحذر از بدگمانیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با خصم هم صغیر بصلحم بجان دوست</p></div>
<div class="m2"><p>تنها نه دوستدار محبان جانیم</p></div></div>