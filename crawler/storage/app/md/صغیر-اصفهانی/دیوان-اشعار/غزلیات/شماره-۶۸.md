---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>خوش میگذرد آنکه مرا روح و روانست</p></div>
<div class="m2"><p>واندر پی او از تن من روح روانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موئیست میانش که از آن هیچ نشان نیست</p></div>
<div class="m2"><p>جز لاغری من که ازآن موی میانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا دیده ام آن لعل لب و قامت و رخسار</p></div>
<div class="m2"><p>کی در نظرم کوثر و طوبی و جنانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمش همه تیر مژه پیوسته بر ابرو</p></div>
<div class="m2"><p>این ترک سیه مست عجب سخت کمانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نه مرا کرده پریشان سر زلفش</p></div>
<div class="m2"><p>آشفته آن طره دل خلق جهانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست از سر و جان شو بره عشق که این راه</p></div>
<div class="m2"><p>اول قدمش پای زدن بر سر جانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بادیهٔ عشق بزن خیمه صغیر!</p></div>
<div class="m2"><p>کانجا نه دگر صحبت کون و نه مکانست</p></div></div>