---
title: >-
    شمارهٔ  ۲۳۹
---
# شمارهٔ  ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>بندهٔ پادشهی باش که درویش بود</p></div>
<div class="m2"><p>پا بدنیا زده و عاقبت اندیش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست هرگز خبری پیش ز خود باخبران</p></div>
<div class="m2"><p>کانکه دارد خبری بی خبر از خویش بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خدا می‌طلبی از دل درویش طلب</p></div>
<div class="m2"><p>بخدا عرش الهی دل درویش بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از این راست رو و پیش رو قافله باش</p></div>
<div class="m2"><p>راست رو در همه جا از همه کس پیش بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش بدگو منشین تا نشوی رنجه از او</p></div>
<div class="m2"><p>روش و عادت کژدم زدن نیش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه ستمها که ز بوجهل رسید احمد(ص) را</p></div>
<div class="m2"><p>خویش را دشمن نزدیک همان خویش بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اهل هر کیش که هستی ز من این نکته شنو</p></div>
<div class="m2"><p>کان پسندیدهٔ هر ملت و هر کیش بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مکن آزار دل خلق که سنجیده صغیر</p></div>
<div class="m2"><p>این گناهی است که از هر گنهی بیش بود</p></div></div>