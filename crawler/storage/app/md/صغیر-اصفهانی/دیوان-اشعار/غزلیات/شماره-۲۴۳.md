---
title: >-
    شمارهٔ  ۲۴۳
---
# شمارهٔ  ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>بحشر عفو اله ار پناه خواهد بود</p></div>
<div class="m2"><p>مراد گرچه زیان از گناه خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهفته می‌ خور و اندیشه از گناه مکن</p></div>
<div class="m2"><p>که لطف پیر مغان عذرخواه خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز طاعتی که کنی بهر خلق از آن اندیش</p></div>
<div class="m2"><p>که آن رفیق ترا دزد راه خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز روی ریا جامه ات سفید کنی</p></div>
<div class="m2"><p>بحشر روی تو بی شک سیاه خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببر بدرگه حق مسکنت که این تحفه</p></div>
<div class="m2"><p>قبول درگه آن پادشاه خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخیر وقف کن اعضای خود که روز جزا</p></div>
<div class="m2"><p>بخیر و شر خود اعضاء گواه خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی است راه حق آنهم طریق عشق علی</p></div>
<div class="m2"><p>جز این طریق دگر اشتباه خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام شاه نجف چون صغیر شو که تو را</p></div>
<div class="m2"><p>بهر دو کون بس این عز و جاه خواهد بود</p></div></div>