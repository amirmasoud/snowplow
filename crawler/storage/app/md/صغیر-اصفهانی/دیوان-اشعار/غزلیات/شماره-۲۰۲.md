---
title: >-
    شمارهٔ  ۲۰۲
---
# شمارهٔ  ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>کس نیست تا نشان بمن از آن دهان دهد</p></div>
<div class="m2"><p>آری ز هیچکس نتواند نشان دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این غم کجا برم که مرا جان بلب رسید</p></div>
<div class="m2"><p>از حسرت لبی که بهر مرده جان دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدش به جلوه شور قیامت بپا کند</p></div>
<div class="m2"><p>چشمش خبر ز فتنهٔ آخر زمان دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم به اتحاد دو چشمش که غمزه را</p></div>
<div class="m2"><p>گاه آن کمک باین و گهی این بآن دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق نباشد آنکه چو بیند بلای عشق</p></div>
<div class="m2"><p>بر یار خویش نسبت نامهربان دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنهمه سیاه دلی زلف کافرش</p></div>
<div class="m2"><p>بر مرغ پر شکستهٔ دل آشیان دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامم شکست شیخ و سلامت سرش ولی</p></div>
<div class="m2"><p>گر سنگ سر شکاف مکافات‌ امان دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غافل مشو صغیر که هر بنده در بلا</p></div>
<div class="m2"><p>باید بقدر قوهٔ خود‌ امتحان دهد</p></div></div>