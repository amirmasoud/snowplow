---
title: >-
    شمارهٔ  ۳۰۶
---
# شمارهٔ  ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>سر و جان داده گدای در میخانه شدیم</p></div>
<div class="m2"><p>رایگان لایق این منصب شاهانه شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار در خانه دل بود و نمیدانستیم</p></div>
<div class="m2"><p>شکرلله که کنون محرم این خانه شدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجبی نیست که بیگانه شویم از همه خلق</p></div>
<div class="m2"><p>ما که از خویش بسودای تو بیگانه شدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنک طفلان بسر خویش خریدیم آنروز</p></div>
<div class="m2"><p>کز غم زلف چو زنجیر تو دیوانه شدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یافت چون گنج غمت جای بدل ز ابر مژه</p></div>
<div class="m2"><p>آنقدر سیل فرو ریخت که ویرانه شدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منعما ناز فرما ز زر و سیم که ما</p></div>
<div class="m2"><p>در ازل مخزن آن گوهر یکدانه شدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را مینگرم مست و خرابست صغیر</p></div>
<div class="m2"><p>ما در این شهر به مستی ز چه افسانه شدیم</p></div></div>