---
title: >-
    شمارهٔ  ۱۴۰
---
# شمارهٔ  ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>حجه قائم که جبریل‌امین دربان اوست</p></div>
<div class="m2"><p>او به فرمان خدا و چرخ در فرمان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطلع فجری که بادش هر دم از ما صد سلام</p></div>
<div class="m2"><p>حجه عصری که هم والعصر اندرشان اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسمانش خوان جود و خاص و عامش ریزه خوار</p></div>
<div class="m2"><p>مهر و مه را خواهی ار دانی دو قرص از خوان اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چارام و هفت اب را شیخ تزویج و طلاق</p></div>
<div class="m2"><p>سه ولدرا پرورش در دامن احسان اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خوشا دوران او هرچند دور روزگار</p></div>
<div class="m2"><p>ز ابتدا تا انتها چون بنگری دوران اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکه تا ز عرصهٔ ایجاد کاین گوی فلک</p></div>
<div class="m2"><p>تا ابد در گردش از یک لطمه چوگان اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه ایوان علووشان که خود عرش علا</p></div>
<div class="m2"><p>با علو قدر ادنی پایه ایوان اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نیاید کی رود ظلم از جهان ای عدل خواه</p></div>
<div class="m2"><p>عدل از دیوان چه خواهی عدل در دیوان اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض بی پایان مبدء میرسد مطلق بوی</p></div>
<div class="m2"><p>وانچه بر هر کس رسد از لطف بی پایان اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش بفردای جزا از لغزش پا ایمنست</p></div>
<div class="m2"><p>هر که را دست ولا‌ام روز بر دامان اوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش ترا کلب در خود خواند از شفقت صغیر</p></div>
<div class="m2"><p>حجته قائم که جبریل‌ام ین دربان اوست</p></div></div>