---
title: >-
    شمارهٔ  ۲۱۹
---
# شمارهٔ  ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>تا لبم بوسه چند از تو دلارام نگیرد</p></div>
<div class="m2"><p>نشود جان ز غم آزاد و دل آرام نگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه را بوسه عطا کردی و کردی ز غم آزاد</p></div>
<div class="m2"><p>این غم ماست که آغاز وی انجام نگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وعظ واعظ ز چه دانی نکند جا بدل ما</p></div>
<div class="m2"><p>پختگانیم که در ما سخن خام نگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد از خرقه سالوس مشو غره که ساقی</p></div>
<div class="m2"><p>صد چنین جامه بها بهر یکی جام نگیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرسیده است بسر منزل‌ آمال کس آری</p></div>
<div class="m2"><p>هیچکس کام از این زال جهان نام نگیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو رها کرد شکار و تن او گور فرو برد</p></div>
<div class="m2"><p>ابله آنکس که بخود پند ز بهرام نگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام می‌گیر و تو هم پیرو جم باش صغیرا</p></div>
<div class="m2"><p>کس از این عالم فانی به از او کام نگیرد</p></div></div>