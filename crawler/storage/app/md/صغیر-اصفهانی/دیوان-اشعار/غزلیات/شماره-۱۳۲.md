---
title: >-
    شمارهٔ  ۱۳۲
---
# شمارهٔ  ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>ببزم قرب چو دیدم میانهٔ من و دوست</p></div>
<div class="m2"><p>حجاب و پرده و حائل تصور من و اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جای جستم و خود را فدای او کردم</p></div>
<div class="m2"><p>که تا نماند کسی در مقام الا دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر اینسخن نپذیرد جز آنکه گوش دلش</p></div>
<div class="m2"><p>رهین زمزمهٔ لا اله الا هوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فکنده‌ام سر خود را چو گو بمیدانی</p></div>
<div class="m2"><p>که سروران سرافراز را سر آنجا گوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا بکعبه چه خوانی برب کعبه قسم</p></div>
<div class="m2"><p>که سجده گاه من ابروی آن کمان ابروست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار مرتبه مینو فدای حور وشی</p></div>
<div class="m2"><p>که یک تبسم او بهتر از دوصد مینوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر بر تو جفائی اگر ز دوست رسید</p></div>
<div class="m2"><p>منال هر چه که از دوست میرسد نیکوست</p></div></div>