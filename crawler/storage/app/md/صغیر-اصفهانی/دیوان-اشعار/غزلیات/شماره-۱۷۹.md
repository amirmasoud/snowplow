---
title: >-
    شمارهٔ  ۱۷۹
---
# شمارهٔ  ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>قربان آن زمان که زمان و مکان نبود</p></div>
<div class="m2"><p>او بود و حرفی از من و ما در میان نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غافل ز کسب سود و زیان خود از عدم</p></div>
<div class="m2"><p>سوی وجود ره سپر این کاروان نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی نداشت شرح فراق و حدیث وصل</p></div>
<div class="m2"><p>شکر و شکایت این همه ورد زبان نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اعجاز انبیا و کرامات اولیا</p></div>
<div class="m2"><p>سحر و فسانه در بر خلق جهان نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اضداد را نبود تنازع به یکدیگر</p></div>
<div class="m2"><p>گیتی دچار جنک بهار و خزان نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حرص و آز بین سلاطین روزگار</p></div>
<div class="m2"><p>این خصمی و مجادله در هر زمان نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوع بشر به آلت قتاله ساختن</p></div>
<div class="m2"><p>تا این حدود با هنر و کاردان نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ا نفجار بمب اتم شهرها چو دود</p></div>
<div class="m2"><p>با ساکنین خود سوی گردون روان نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنها همین ز عدل نمی رفت صحبتی</p></div>
<div class="m2"><p>ظلم از توانگر این همه بر ناتوان نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از فرط ناتوانی و از کثرت غرور</p></div>
<div class="m2"><p>پامال غم ضعیف و قوی سرگردان نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شایع نبود این همه تبعیض در بشر</p></div>
<div class="m2"><p>این نا مراد و آن دگری کامران نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هان بر صغیر تا نبری ظن بد که او</p></div>
<div class="m2"><p>بر دستگاه قدرت حق بدگمان نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر ذره گشت جلوه گر از غیب در شهود</p></div>
<div class="m2"><p>غیر از نشان قدرت آن بی نشان نبود</p></div></div>