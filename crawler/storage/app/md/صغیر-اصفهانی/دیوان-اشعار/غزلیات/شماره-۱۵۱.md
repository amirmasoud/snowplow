---
title: >-
    شمارهٔ  ۱۵۱
---
# شمارهٔ  ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>من و خاک سر آنکوی که دلدار آنجاست</p></div>
<div class="m2"><p>راحت جان و تن آرام دل زار آنجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رخ یار مرا حاجت گلشن نبود</p></div>
<div class="m2"><p>هر کجا یار بود گلشن و گلزار آنجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کنم خاک در میکده را کحل بصر</p></div>
<div class="m2"><p>نکنم عیب که نقش قدم یار آنجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب حلقه صفت بر در آن عیسی دم</p></div>
<div class="m2"><p>چون ننالم که دوای دل بیمار آنجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر گشایش طلبی جو ز در پیر مغان</p></div>
<div class="m2"><p>کانکه لطفش بگشاید گره از کار آنجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو خورشید که نورش همه جا جلوه گر است</p></div>
<div class="m2"><p>هر کجا میروم آن دلبر عیار آنجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یوسفا حسن تو را مردم کنعان نخرند</p></div>
<div class="m2"><p>جانب مصر روان شو که خریدار آنجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست از کعبهٔ گل ره بمقام مقصود</p></div>
<div class="m2"><p>رهرو کعبهٔ دل باش که دلدار آنجاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داشت اندیشه صغیر از صف حشر و خردش</p></div>
<div class="m2"><p>گفت تشویش مکن حیدر کرار آنجاست</p></div></div>