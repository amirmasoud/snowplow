---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>خدای ساخته گنجینه گهر ما را</p></div>
<div class="m2"><p>نموده مظهر خود پای تا بسر ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمال عزت ما بین که حق بحد کمال</p></div>
<div class="m2"><p>چو خواست جلوه کند ساخت جلوه گر ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای اینکه کند خویش جلوه در انظار</p></div>
<div class="m2"><p>بداد جلوه در انظار یکدگر ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عرش و فرش دل ما گزید مسکن خویش</p></div>
<div class="m2"><p>نهاد تاج کرامت از آن بسر ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وصل خویش بشارت میان جن و ملک</p></div>
<div class="m2"><p>به ما بداد و از آن نام شد بشر ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بر و بحر کند تا که صنع خود ظاهر</p></div>
<div class="m2"><p>نمود کاشف اسرار بحر و بر ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خشک و تر همه اشیا طفیل خلقت ماست</p></div>
<div class="m2"><p>بود تصرف از این رو بخشک و تر ما را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسا خواص ز حکمت نهاده در اشیاء</p></div>
<div class="m2"><p>ز حل و عقد همه کرده با خبر ما را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز ما پدید شود تا که قدرتش داده است</p></div>
<div class="m2"><p>به دست رشتهٔ هر صنعت و هنر ما را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برای ماست جهان ما برای طاعت حق</p></div>
<div class="m2"><p>نکرده خلعت هستی عبث ببر ما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چراغ عقل بما داده تا که پرتو آن</p></div>
<div class="m2"><p>ببخشد آگهی از راه خیر و شر ما را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو روی اصل محبت نموده خلقت ما</p></div>
<div class="m2"><p>برخ گشوده هم از عشق خویش در ما را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عجب که خوانده شب و روز پنج ره بحضور</p></div>
<div class="m2"><p>ز لطف و رحمت بیرون ز حد و مر ما را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صغیر دارد از او مسئلت که خود گردد</p></div>
<div class="m2"><p>به راه بندگی خویش راهبر ما را</p></div></div>