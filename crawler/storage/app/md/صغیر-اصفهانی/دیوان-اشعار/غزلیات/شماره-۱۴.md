---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>در روی این زمین که تویی صد هزارها</p></div>
<div class="m2"><p>پیش از تو بوده اند بشهر و دیارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر پیکر سپید و سیاه بشر بسی</p></div>
<div class="m2"><p>این مهر و ماه تافته لیل و نهارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس سیم تن مقیم سراهای زرنگار</p></div>
<div class="m2"><p>کامروز خفته اند به خاک مزارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس کاخ منهدم شده بینی ز خون دل</p></div>
<div class="m2"><p>اجزای آن پر است ز نقش و نگارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل سرزند برنگ رخ گلرخان ز گل</p></div>
<div class="m2"><p>رخ زیر گل نهفته ز بس گلعذارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سروهای ناز مثال سهی قدان</p></div>
<div class="m2"><p>گویی زمانه ساخته در جویبارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بس بزم عیش گشت سیه پوش و مطربان</p></div>
<div class="m2"><p>دادند جای خود همه بر سوگوارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس میگسار و ساقی مهوش که هیچ نیست</p></div>
<div class="m2"><p>حرفی ز ساقی و اثر از میگسارها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یا للعجب که در طلب دو گز کفن</p></div>
<div class="m2"><p>باشد همیشه بین بشرگیر و دارها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دانند فانی است جهان و برای آن</p></div>
<div class="m2"><p>بر پا همی کنند صف کار زارها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بس شهریار قادر و سلطان مقتدر</p></div>
<div class="m2"><p>رفتند و رفت از کفشان اقتدارها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بس کاردان که در عوض پیشرفت کار</p></div>
<div class="m2"><p>خود مضمحل شدند در انجام کارها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس شیرگیرها که ز هم شیرشان درید</p></div>
<div class="m2"><p>صیادها شدند شکار شکارها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شو راستکار و در دو جهان باش رستگار</p></div>
<div class="m2"><p>گشتند رستگار چنین رستگارها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کن صرف خدمت دگران روزگار خویش</p></div>
<div class="m2"><p>با نام نیک زنده بمان روزگارها</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غافل مشو صغیر ز درها که سفته اند</p></div>
<div class="m2"><p>از بهر هوشیاری ما هوشیارها</p></div></div>