---
title: >-
    شمارهٔ  ۴۰۱
---
# شمارهٔ  ۴۰۱

<div class="b" id="bn1"><div class="m1"><p>چو کسی بعذرخواهی نرود ز بی گناهی</p></div>
<div class="m2"><p>بگنه خوشم که آورد مرا بعذرخواهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه غم ای گناهکاران ز گناه ما که هرگز</p></div>
<div class="m2"><p>گنهی نمیشود بیش ز رحمت الهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگنه خوشیم و عذرش نه بزهد ونخوت آن</p></div>
<div class="m2"><p>که گنه بسی نکوتر ز غرور بی گناهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلی از تو چون شود شاد دل تو شاد گردد</p></div>
<div class="m2"><p>نرسی بخیر الا بطریق خیرخواهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خستهٔی بدست آراگر چه هست وحشی</p></div>
<div class="m2"><p>که سبکتکین رسیده است از آن بپادشاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکس ار پناه گشتی بخدای هر دو عالم</p></div>
<div class="m2"><p>که خدا شود پناه تو بوقت بی پناهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرت صغیر بایست دلی چو صبح روشن</p></div>
<div class="m2"><p>بدعای نیمه شب کوش و بورد صبحگاهی</p></div></div>