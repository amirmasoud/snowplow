---
title: >-
    شمارهٔ  ۱۵۲
---
# شمارهٔ  ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>خرم کسی که کام ز بخت جوان گرفت</p></div>
<div class="m2"><p>یعنی به صدق دامن پیر مغان گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردم چو نام عشق سرا پا بسوختم</p></div>
<div class="m2"><p>چون شمع کاتشش بوجود از زبان گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوبان دهند بوسه و گیرند جان بها</p></div>
<div class="m2"><p>نازم بدان حریف که این داد و آن گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم فتاد تا چو کمر با میان یار</p></div>
<div class="m2"><p>از هر کنار غصه مرا در میان گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلفش گرفت جا برخ و من بحیرتم</p></div>
<div class="m2"><p>کاین کافر از برای چه جا در جنان گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بعد از این چه آیدم ای دوستان بپیش</p></div>
<div class="m2"><p>حالی که عشق از کف عقلم عنان گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکس که چون صغیر بحیدر پناه برد</p></div>
<div class="m2"><p>از هر بلا و حادثه خط‌ام ان گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکو سپرد خط غلامی به مرتضی</p></div>
<div class="m2"><p>سر خط رستگاری کون و مکان کرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه نجف که بنده ای از بندگان او</p></div>
<div class="m2"><p>باج شرف ز تاجوران جهان گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفعت از آستانهٔ آن شه طلب که عرش</p></div>
<div class="m2"><p>این رفعتی که دارد از آن آستان گرفت</p></div></div>