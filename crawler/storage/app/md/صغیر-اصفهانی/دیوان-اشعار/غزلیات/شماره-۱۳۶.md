---
title: >-
    شمارهٔ  ۱۳۶
---
# شمارهٔ  ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>بیا ای عید مشتاقان که از هجر دو ابرویت</p></div>
<div class="m2"><p>هلالی گشت پشت روزه داران مه رویت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دین از کفر بشناسم نه روز از شب که بگذارد</p></div>
<div class="m2"><p>دمی افتم بفکر خویشتن یاد رخ و مویت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراهم می‌کنم بهر خود اسباب پریشانی</p></div>
<div class="m2"><p>که شاید نسبتی پیدا کند حالم بگیسویت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگشتی سجده گاه خلق عالم تا ابد کعبه</p></div>
<div class="m2"><p>نکردی در ازل گر سجده بر محراب ابرویت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکند از غمزهٔی ها رو ترا در چه من مسکین</p></div>
<div class="m2"><p>توانم چون سلامت جان برم از چشم جادویت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقیم آنکو بکویت شد نمیجوید بهشت آری</p></div>
<div class="m2"><p>بهشت است آرزومند مقیمان سر کویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من ار دیدار گل جویم و گر مشک ختن بویم</p></div>
<div class="m2"><p>از ایندارم غرض رنگت وزآندارم طمع بویت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر از آستانت کی تواند روی برتابد</p></div>
<div class="m2"><p>که هر سو روی بنماید دل او را میکشد سویت</p></div></div>