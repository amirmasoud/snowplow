---
title: >-
    شمارهٔ  ۲۳۴
---
# شمارهٔ  ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>بخدا رسد هر آن دل که پی هوا نباشد</p></div>
<div class="m2"><p>بلی از هوا چو بگذشت بجز خدا نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بره طلب سراپا شده‌ام قدم که رهرو</p></div>
<div class="m2"><p>بطریق عشق بایست کم از صبا نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو بلای عشق نبود بجهان دگر بلایی</p></div>
<div class="m2"><p>که باین بلا دلی نیست که مبتلا نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو تو خیر خلق خواهی همه خیر پیشت آید</p></div>
<div class="m2"><p>که برای نیک جز نیک دگر جزا نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو رضا نباشی از حق بدرش انابتی کن</p></div>
<div class="m2"><p>که چو برخوری بمطلب ز تو او رضا نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مطالب دو گیتی چو صغیر آنچه خواهی</p></div>
<div class="m2"><p>ز علی طلب که مطلب جز از او روا نباشد</p></div></div>