---
title: >-
    شمارهٔ  ۲۶۳
---
# شمارهٔ  ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>یاران ره عشق منزل ندارد</p></div>
<div class="m2"><p>این بحر مواج ساحل ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم غم عشق در مزرع دل</p></div>
<div class="m2"><p>جز درد و محنت حاصل ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق است کاری مشکل که عالم</p></div>
<div class="m2"><p>کاری بدینسان مشکل ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باری که حملش ناید ز گردون</p></div>
<div class="m2"><p>جز ما ضعیفان حامل ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ما نباشیم مجنون که لیلی</p></div>
<div class="m2"><p>غیر از دل ما محمل ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ما نگردیم پروانه کان شمع</p></div>
<div class="m2"><p>جز مجلس ما محفل ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقتول عشق است خود قاتل خود</p></div>
<div class="m2"><p>این کشته در حشر قاتل ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس نبندد بر دلبری دل</p></div>
<div class="m2"><p>یا آدمی نیست یا دل ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با چشم حق بین نقش جهان بین</p></div>
<div class="m2"><p>این نقش حق است باطل ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان صغیر است بینای جانان</p></div>
<div class="m2"><p>از دیده‌ای کان حایل ندارد</p></div></div>