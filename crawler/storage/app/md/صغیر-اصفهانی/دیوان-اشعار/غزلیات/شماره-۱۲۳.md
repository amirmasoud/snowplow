---
title: >-
    شمارهٔ  ۱۲۳
---
# شمارهٔ  ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>ایخوش آنعارف سالک که ز راه آگاهست</p></div>
<div class="m2"><p>حاصل بندگیش دیدن روی شاه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرجهان بینی و بس فرق تو با حیوان چیست</p></div>
<div class="m2"><p>چشم انسان همه بینای جمال الله است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر کویت شده از خون شهیدان دریا</p></div>
<div class="m2"><p>مگر ایجان جهان کوی تو قربانگاه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود که باشی تو که هر جامه بدوزم از وصف</p></div>
<div class="m2"><p>پیش بالای تو چون آورم آن کوتاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که اندر پی آن چاه ذقن میگردی</p></div>
<div class="m2"><p>واقف رفتن خود باش براهت چاه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه شود کامرواخواهی و خرم ما را</p></div>
<div class="m2"><p>ای که بر ما هر چه تو را دلخواه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه اگر لطف توام بدرقه ره نشود</p></div>
<div class="m2"><p>که گهر دارم و صد راه زنم در راه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر من از خود نیم آگاه صغیرا غم نیست</p></div>
<div class="m2"><p>بندهٔ پیر مغانم که زمن آگاه است</p></div></div>