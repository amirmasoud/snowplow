---
title: >-
    شمارهٔ  ۲۰۰
---
# شمارهٔ  ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>هر که را بوئی از آن طرهٔ پرچین‌ آمد</p></div>
<div class="m2"><p>فارغ از مشک خطا و ختن و چین‌ آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب و رنگ رخ یار است که در باغ عیان</p></div>
<div class="m2"><p>از گل و لاله و از سنبل و نسرین‌ آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چکنم گر نشوم کافر عشقش که مرا</p></div>
<div class="m2"><p>غمزه اش راهزن عقل و دل و دین‌ آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز آید دل از آن طرهٔ چون پر غراب</p></div>
<div class="m2"><p>باز اگر صعوهٔی از چنگل شاهین‌ آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلخ باشد بنظر تیشه زدن بر سر لیک</p></div>
<div class="m2"><p>آنچه بر کوهکن‌ آمد همه شیرین‌ آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش بود حالت آن عاشق دلخسته صغیر</p></div>
<div class="m2"><p>که نگارش بدم مرگ به بالین‌ آمد</p></div></div>