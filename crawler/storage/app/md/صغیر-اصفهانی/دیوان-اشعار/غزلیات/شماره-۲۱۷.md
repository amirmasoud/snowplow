---
title: >-
    شمارهٔ  ۲۱۷
---
# شمارهٔ  ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>عاقبت دلا دیدی کار یار و من چون شد</p></div>
<div class="m2"><p>حاصل من از عشقش دیدهٔ پر از خون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر اگر فزاید مهر از چه رو بآن مه من</p></div>
<div class="m2"><p>هرچه مهر افزودم جور و کینش افزونشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش مستی ما را جام می‌نشد باعث</p></div>
<div class="m2"><p>بل ز غمزهٔ ساقی حال ما دگرگونشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جنون عشق ایدل نام هم بدل گردد</p></div>
<div class="m2"><p>قیس در غم لیلی گفته گفته مجنون شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوی حاتمی باید نام حاتم ار خواهی</p></div>
<div class="m2"><p>ورنه زر همان زر بد ننگ نام قارون شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرو قد جانانرا تا نشاند اندر دل</p></div>
<div class="m2"><p>گفتهٔ صغیر الحق دلربا و موزون شد</p></div></div>