---
title: >-
    شمارهٔ  ۴۱۱
---
# شمارهٔ  ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>به بندگان نکنی لطف چون تو از یاری</p></div>
<div class="m2"><p>چگونه لطف خداوند را طمع داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بنده لطف کن و لطف حق طلب ورنه</p></div>
<div class="m2"><p>مجوی حاصل اگر دانه ئی نمی کاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طمع مدار که غیری تو را شود غمخوار</p></div>
<div class="m2"><p>اگر نکرده ئی از غیر خویش غمخواری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز خضوع و خشوع و شکستگی نخرند</p></div>
<div class="m2"><p>بخویش غره مباش از درست کرداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نداده اند تو را قدرت و زبردستی</p></div>
<div class="m2"><p>که زیر دست دل آزرده را بیازاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتیجه ای بکف آور ببر بهمره خویش</p></div>
<div class="m2"><p>ز ثروتی که از آن بگذری و بگذاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببین نهایت‌امساک اغنیا که کنند</p></div>
<div class="m2"><p>هم از جواب سلام فقیر خودداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر رفع گرفتاری از کسان کردن</p></div>
<div class="m2"><p>تو را نجات دهد بی شک از گرفتاری</p></div></div>