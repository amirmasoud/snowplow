---
title: >-
    شمارهٔ  ۸۶
---
# شمارهٔ  ۸۶

<div class="b" id="bn1"><div class="m1"><p>آنکه مانند تواش یار دل آزاری هست</p></div>
<div class="m2"><p>چون منش دیده خون بار و دل زاری هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازم آن چشم فریبنده بیمار تو را</p></div>
<div class="m2"><p>که بهر گوشه ز هجرش دل بیماری هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دورم از خود کنی و شادم از اینرو که شود</p></div>
<div class="m2"><p>از تو ثابت که بعالم گل بیخاری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بخوانی چه برانی من و خاک در تو</p></div>
<div class="m2"><p>کافرم گر بجهان غیر توام یاری هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدعی گوی به بیند دو رخ خوب تو را</p></div>
<div class="m2"><p>گر بشق القمر احمدش انکاری هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصحم گفت کند عشق ز هر کارت باز</p></div>
<div class="m2"><p>بگمانش که بجز عشق مرا کاری هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ابد زنده توان بود به آثار نکو</p></div>
<div class="m2"><p>آن نمرده است کز او اسمی و آثاری هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز خوش نیز تو در خواب نخواهی دیدن</p></div>
<div class="m2"><p>ای که شب از ستمت دیده بیداری هست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون توانی بکن آنروز که نتوانی یاد</p></div>
<div class="m2"><p>کز پی روز سپید تو شب تاری هست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست در دست تو گر درهم و دینار صغیر</p></div>
<div class="m2"><p>به ز گنج گهرت دفتر اشعاری هست</p></div></div>