---
title: >-
    شمارهٔ  ۱۷۰
---
# شمارهٔ  ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>تعالی الله از این بالا وزین رخ</p></div>
<div class="m2"><p>ندیده کس چنین قامت چنین رخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنی روز مرا شب چون نمائی</p></div>
<div class="m2"><p>نهان در زیر زلف عنبرین رخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز حیرت نقش بر دیوار گردند</p></div>
<div class="m2"><p>نمائی گر به نقاشان چین رخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگیرم زندگی از سر تو بر من</p></div>
<div class="m2"><p>نمائی گر به روز واپسین رخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی کو دیده دارد کی تواند</p></div>
<div class="m2"><p>که یکدم دیده بردارد از این رخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخی داری که خورشیدار به بیند</p></div>
<div class="m2"><p>به پیش آن بساید بر زمین رخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فکند اندر جهانی شور و غوغا</p></div>
<div class="m2"><p>تو را تا با ملاحت شد قرین رخ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد طاقت هجران خدا را</p></div>
<div class="m2"><p>مپوشان از صغیر دلغمین رخ</p></div></div>