---
title: >-
    شمارهٔ  ۱۱۰
---
# شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>گفتم کمند عشق تو در گردن منست</p></div>
<div class="m2"><p>گفت این کمند خلق جهانرا بگردنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم ز درد عشق تو کاهیده شد تنم</p></div>
<div class="m2"><p>گفتا میان ما و تو حایل همین تنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم براه عشق شد آلوده دامنم</p></div>
<div class="m2"><p>گفتا خموش این ره هر پاکدامن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ز دوستان تو خواهم نشانه ئی</p></div>
<div class="m2"><p>گفت آنمراست دوستکه با خویش دشمنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم سخن زار من و خوبان آن کنند</p></div>
<div class="m2"><p>گفت از منست آنچه حلاوت به ار من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم صغیر خواست ز حسن تو جلوهٔی</p></div>
<div class="m2"><p>گفتا بهر چه مینگرد جلوه من است</p></div></div>