---
title: >-
    شمارهٔ  ۲۴۹
---
# شمارهٔ  ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>شهان که مالک اورنگ و صاحب تاجند</p></div>
<div class="m2"><p>چو نیک در نگری بر فقیر محتاجند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام دولت فقرم اگر چه درویشان</p></div>
<div class="m2"><p>به تیر طعنه خلق زمانه‌ آماجند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه ایست که مردم بقصد یکدیگرند</p></div>
<div class="m2"><p>چو لشگری که مهیای قتل و تاراجند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم بملک وجودم شه است و عقل وزیر</p></div>
<div class="m2"><p>بحربگاه عدو خیل آهم افواجند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فدای سوختگانی شوم که با لب خشگ</p></div>
<div class="m2"><p>کنند العطش و خویش بحر مواجند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی کنند یقین بر وصال یار اغیار</p></div>
<div class="m2"><p>چنانکه بهر نبی منکران معراجند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلب ز احمد و آلش طریق حق کایشان</p></div>
<div class="m2"><p>برای خیل رسل هادیان منهاجند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر یافت بدل روشنی از آن انوار</p></div>
<div class="m2"><p>که بزم کون و مکان را سراج وهاجند</p></div></div>