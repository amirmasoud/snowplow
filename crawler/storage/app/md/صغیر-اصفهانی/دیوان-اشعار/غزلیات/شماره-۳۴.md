---
title: >-
    شمارهٔ  ۳۴
---
# شمارهٔ  ۳۴

<div class="b" id="bn1"><div class="m1"><p>فرقی نمانده در عمل خوب و زشت ما</p></div>
<div class="m2"><p>یکسان شده است کعبه و دیر و کنشت ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انجام خود ز خوی بد و نیک خود بیاب</p></div>
<div class="m2"><p>در ما نهفته دوزخ ما و بهشت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر برخلاف میل رود عمر چاره چیست</p></div>
<div class="m2"><p>بوده است این ز روز ازل سرنوشت ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حالی بود عمارت ما خاک و خشت غیر</p></div>
<div class="m2"><p>تا خود عمارت که شود خاک و خشت ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی به هیچ روی تغیر پذیر نیست</p></div>
<div class="m2"><p>دست طبیعت آنچه نهد در سرشت ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سعی ماست حاصل ما روز رستخیز</p></div>
<div class="m2"><p>نبود بغیر صاعقه در خورد کشت ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفر است ناامیدی و بیگانگی صغیر</p></div>
<div class="m2"><p>باشد که ننگرند بکردار زشت ما</p></div></div>