---
title: >-
    شمارهٔ  ۲۰۵
---
# شمارهٔ  ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>هر آنکو سر بخاک درگه پیر مغان دارد</p></div>
<div class="m2"><p>بیمن اختر فیروزخوش بختی جوان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوص‌آور بپیش و جان خلاص از حول محشر کن</p></div>
<div class="m2"><p>که زر تا نیست خالص بیم روز ‌امتحان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گمانم اینکه نفس مطمئنی نیست جز عاشق</p></div>
<div class="m2"><p>که عاشق نی ز دوزخ باک و نی شوق جنان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام همت آن عاشقم کز جور جانانه</p></div>
<div class="m2"><p>بجان صد آتش و قفل خموشی بر دهان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر و سامان ز درویشان مجو دل بر قفس بستن</p></div>
<div class="m2"><p>نه از مرغی است کاندر شاخ طوبی آشیان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهانرا خواجه خواهد در خط فرمان خویش آرد</p></div>
<div class="m2"><p>تو پنداری ز دیوان قضا خط‌ امان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیرا یاد مرک از دل کند مهر جهان بیرون</p></div>
<div class="m2"><p>بیاد آور بهار عمرت اندر پی خزان دارد</p></div></div>