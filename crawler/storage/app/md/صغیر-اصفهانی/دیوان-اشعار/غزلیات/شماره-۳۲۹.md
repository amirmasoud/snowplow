---
title: >-
    شمارهٔ  ۳۲۹
---
# شمارهٔ  ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>ببوسهٔ لب ساقی بس آرزو دارم</p></div>
<div class="m2"><p>بسان شیشهٔ می‌گریه در گلو دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاد چاک گریبان یار و غبغب او</p></div>
<div class="m2"><p>همیشه سر بگریبان غم فرو دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه صورتی تو که من در تو خویش مینگرم</p></div>
<div class="m2"><p>بدان قیاس که آئینه روبرو دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز داغ لاله رخان من که دیده‌ام دریاست</p></div>
<div class="m2"><p>کجا هوای گلستان کنار جو دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنزد خلق اگر خوارم این بس است مرا</p></div>
<div class="m2"><p>که پیش اهل خرابات آبرو دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صغیر من سگ درگاه شیر یزدانم</p></div>
<div class="m2"><p>همیشه دیدهٔ‌ امید سوی او دارم</p></div></div>