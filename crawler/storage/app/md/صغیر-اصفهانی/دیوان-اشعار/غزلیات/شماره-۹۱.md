---
title: >-
    شمارهٔ  ۹۱
---
# شمارهٔ  ۹۱

<div class="b" id="bn1"><div class="m1"><p>گرچه در پیش نظر قطره قرین بایم نیست</p></div>
<div class="m2"><p>لیک در اصل یم و قطره جدا از هم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدمی گنج الهی است که در بحر وجود</p></div>
<div class="m2"><p>گوهری نیست که در آب و گل آدم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پی باسرار دل کس نتوان برد بلی</p></div>
<div class="m2"><p>اندرین خانه کسی غیر خدا محرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دانست بنادانی خود کرد اقرار</p></div>
<div class="m2"><p>هیچ کس باخبر از کیفیت عالم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس پیر خرابات بنازم که سحر</p></div>
<div class="m2"><p>نفس باد صبا بی مددش خرم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا که از هستی عاشق سر موئی باقی است</p></div>
<div class="m2"><p>رشتهٔ عشق هنوزش ببتان محکم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکند ناله گر از درد صغیر این نه عجب</p></div>
<div class="m2"><p>دم از آن بسته که او را بجهان همدم نیست</p></div></div>