---
title: >-
    شمارهٔ  ۲۱۱
---
# شمارهٔ  ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>هر کس نه بعشق از سر اخلاص قدم زد</p></div>
<div class="m2"><p>از پای درافتاد و بسر دست ندم زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یار عرب زادهٔ مابین که دو ترکش</p></div>
<div class="m2"><p>یغمای عرب کرد و شبیخون بعجم زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون دل عشاق ز مژگان سیه ریخت</p></div>
<div class="m2"><p>آندم که میان دو سیه چشم بهم زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردم ز طبیبی طلب داروی غم گفت</p></div>
<div class="m2"><p>بایست که از تیشهٔ می‌ریشه غم زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مذهب رندان بتر از کفر دورنگیست</p></div>
<div class="m2"><p>بایست که دم یا ز صمد یا ز صنم زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خوان فلک دست فرو شوی دلا چند</p></div>
<div class="m2"><p>بتوان چو مگس دست بسر بهر شکم زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جوز مرا مغز سر از کاسه برون شد</p></div>
<div class="m2"><p>از بسکه فلک بر سر من سنگ ستم زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خاموش صغیرا که بد اندرید حکمت</p></div>
<div class="m2"><p>آن خام که بر صفحه تقدیر رقم زد</p></div></div>