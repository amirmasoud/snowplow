---
title: >-
    شمارهٔ  ۴۱۷
---
# شمارهٔ  ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>چوبدید خویشتن را همه حسن و دلربائی</p></div>
<div class="m2"><p>به هزار رنگ پوشید لباس خودنمائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمود خویشتن را بخود و ز فرط خوبی</p></div>
<div class="m2"><p>دل خود ربود از کف بنگر بدلربائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب از کمند زلفش که برای عالمی شد</p></div>
<div class="m2"><p>همه رشتهٔ اسیری همه دام مبتلائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ازل چو دانهٔ خال نمود تا قیامت</p></div>
<div class="m2"><p>همه مرغهای دل شد به هوای آن هوائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرهی نمیشود باز ز کار خلق عالم</p></div>
<div class="m2"><p>اگر او ز زلف پرچین نکند گره‌گشائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحقیقت ار ببینی ره بردن دلست این</p></div>
<div class="m2"><p>که بهرکسی گشوده است دری ز آشنائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفسی ز غم رهائی نبود برای عاشق</p></div>
<div class="m2"><p>که به گاه وصل هم دل تپد از غم جدایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گدای درگه عشق بود صغیر شاید</p></div>
<div class="m2"><p>به شهنشهان اگر فخر کند از این گدائی</p></div></div>