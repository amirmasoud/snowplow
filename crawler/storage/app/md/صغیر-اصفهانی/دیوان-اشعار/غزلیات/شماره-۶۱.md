---
title: >-
    شمارهٔ  ۶۱
---
# شمارهٔ  ۶۱

<div class="b" id="bn1"><div class="m1"><p>نتوان به مثل گفتن خورشید درخشانت</p></div>
<div class="m2"><p>زیرا که بود شمعی خورشید در ایوانت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی کشمت در خون بشتاب که میترسم</p></div>
<div class="m2"><p>اغیار کنند ایجان از گفته پشیمانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد سنک جفا هر دم گردون زندش بر سر</p></div>
<div class="m2"><p>آنرا که بود شوری از بستهٔ خندانت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی شب مشتاقان گردد همه صبح ایمه</p></div>
<div class="m2"><p>در اول شب بگشا از مهر گریبانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای وصل ترا دایم دل مایل و جان شایق</p></div>
<div class="m2"><p>باز آی که مشتاقان مردند ز هجرانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر لحظه کنی از نو شادم بغمی آیا</p></div>
<div class="m2"><p>گشتم به چه خدمت من شایستهٔ احسانت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا که بیاویزد در زلف تو دل ور نه</p></div>
<div class="m2"><p>بیرون نتواند شد از چاه زنخدانت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از تیغ جدا سازی گر بند ز بندم را</p></div>
<div class="m2"><p>من همچو قلم دارم سر در خط فرمانت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواهی اگر آگاهی از حال صغیر ایجان</p></div>
<div class="m2"><p>کن موی به مو تحقیق از زلف پریشانت</p></div></div>