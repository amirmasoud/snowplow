---
title: >-
    شمارهٔ  ۱۴۴
---
# شمارهٔ  ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>از حرم بگذر که اینجا خرگه آنشاه نیست</p></div>
<div class="m2"><p>کوی او را کعبه جز سنگ نشان راه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه در دیر و حرم تابیده انوارش ولی</p></div>
<div class="m2"><p>جز که در صحرای دل آنشاه را خرگاه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شکایت گر زنی دم یار پوشد از تو رخ</p></div>
<div class="m2"><p>آری آری در بر آئینه جای آه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عین وصلش می‌نماید هجر ورنه لحظهٔی</p></div>
<div class="m2"><p>دست از زلف بلند آن پری کوتاه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگردی نیست از هستی کجا یابی خبر</p></div>
<div class="m2"><p>جز ز راه لا الهت ره به الا الله نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خدا توفیق جو شاید ز خود آگه شوی</p></div>
<div class="m2"><p>کانکه از خود نیست آگه از خدا آگاه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناوک نمرود را یزدان بخون آلوده کرد</p></div>
<div class="m2"><p>تا بدانی هیچکس محروم از این درگاه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دولت فقرم چنانکرده است مستغنی که هیچ</p></div>
<div class="m2"><p>در دل من آرزوی مال و فکر جاه نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندم از بدخواه می‌ترسانی ای ناصح برو</p></div>
<div class="m2"><p>من نخواهم بهر کس بد با کم از بدخواه نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عالمی را می‌توانی رام کرد از دوستی</p></div>
<div class="m2"><p>هیچکس را در مقام دوستی اکراه نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من گدای آستان شاه مردانم صغیر</p></div>
<div class="m2"><p>چشم‌ام یدم بجز بر درگه آنشاه نیست</p></div></div>