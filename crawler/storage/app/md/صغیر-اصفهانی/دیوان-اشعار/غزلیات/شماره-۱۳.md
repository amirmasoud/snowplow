---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>جانا ز که آموختی این عشوه گری را</p></div>
<div class="m2"><p>عشاق کشی خانه کنی پرده دری را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو از تو خجل گشت چو سیب زقنت دید</p></div>
<div class="m2"><p>آری چه کند سرزنش بی ثمری را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر لحظه دلم در خم موئیت کند جای</p></div>
<div class="m2"><p>خوش کرده فلک قسمت او در بدری را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی بفراق گل رخسار تو هر شب</p></div>
<div class="m2"><p>هم ناله شوم نالهٔ مرغ سحری را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جور فلک و طعنه اغیار و غم یار</p></div>
<div class="m2"><p>یا رب چه کنم این همه خونین جگری را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بی خبری مدعیان بی خبرانند</p></div>
<div class="m2"><p>زان خرده گرفتند بمن بی خبری را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دست مرا چون هنری نیست همان به</p></div>
<div class="m2"><p>بر حضرت او عرضه دهم بی هنری را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آباد شدم از نظر پیر خرابات</p></div>
<div class="m2"><p>نازم روش رندی و صاحب نظری را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیوانه شود همچو صغیر آن که به بیند</p></div>
<div class="m2"><p>از چشم سیه غمزه آن رشگ پری را</p></div></div>