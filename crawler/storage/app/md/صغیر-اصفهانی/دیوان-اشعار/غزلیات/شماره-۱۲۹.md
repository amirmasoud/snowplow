---
title: >-
    شمارهٔ  ۱۲۹
---
# شمارهٔ  ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>دل مرا ز کمندت سر رهائی نیست</p></div>
<div class="m2"><p>کجا رود بکسش جز تو آشنائی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بیا که دلم بی تو از جهان سیر است</p></div>
<div class="m2"><p>مرو مرو که مرا طاقت جدائی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدور چشم تو و زلف تو دگر کارم</p></div>
<div class="m2"><p>بآهوی ختن و نافه ختائی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتان چو دل بربایند بوسه ئی بدهند</p></div>
<div class="m2"><p>ولی تو را صفتی غیر دلربائی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گدای شاه نجف شو که هیچ سلطنتی</p></div>
<div class="m2"><p>به روزگار برابر بدین گدائی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صغیر پیرهنی بیش از جهان نبرد</p></div>
<div class="m2"><p>کنون غمی بدل او را ز بی قبایی نیست</p></div></div>