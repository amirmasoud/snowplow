---
title: >-
    شمارهٔ  ۳۲۰
---
# شمارهٔ  ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>عمریست که در هجران میسوزم و میسازم</p></div>
<div class="m2"><p>با این غم بی پایان میسوزم و میسازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم فراق ای دوست شب تا بسحر دایم</p></div>
<div class="m2"><p>چون شمع سرشک افشان میسوزم و میسازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی روی تو شد عالم زندان بلا بر من</p></div>
<div class="m2"><p>با محنت ا ین زندان میسوزم و میسازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پویای ره کویت جویای گل رویت</p></div>
<div class="m2"><p>با خار در این بستان میسوزم و میسازم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حرمان وصال تو آتش زده بر جانم</p></div>
<div class="m2"><p>با آتش این حرمان میسوزم و میسازم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از طعن رقیب آذر دارم بجگر‌اما</p></div>
<div class="m2"><p>با سرزنش عدوان میسوزم و میسازم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مانند صغیر از دل آهم شرر انگیزد</p></div>
<div class="m2"><p>با این نفس سوزان میسوزم و میسازم</p></div></div>