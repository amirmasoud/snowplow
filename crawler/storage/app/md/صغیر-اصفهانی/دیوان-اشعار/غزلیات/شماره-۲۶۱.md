---
title: >-
    شمارهٔ  ۲۶۱
---
# شمارهٔ  ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>هر چه از اصلش جدا گردید باز آنجا رود</p></div>
<div class="m2"><p>موج اگر آید بساحل باز در دریا رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زشت باشد عقل را کردن مطیع نفس دون</p></div>
<div class="m2"><p>حیف بینایی که در دنبال نابینا رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای توانا دستگیر ناتوان شو در بلا</p></div>
<div class="m2"><p>دست مسئول است هر جا خاری اندر پا رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرمنال از غیر در خود بین و در کردار خود</p></div>
<div class="m2"><p>آری آری هر جفا بر ما رود از ما رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون مکر رشد سخن بر مستمع بخشد اثر</p></div>
<div class="m2"><p>قطره از تکرار بی شک در دل خارا رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در قفای کاروان رو وز خطر آسوده باش</p></div>
<div class="m2"><p>غول راهش ره زند رهرو اگر تنها رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه آسایش طمع دارد در این عالم صغیر</p></div>
<div class="m2"><p>مآند آن صیاد را کاندر پی عنقا رود</p></div></div>