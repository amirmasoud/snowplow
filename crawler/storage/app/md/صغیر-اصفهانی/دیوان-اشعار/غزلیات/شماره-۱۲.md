---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>تا بود زلف تو اسباب پریشانی ما</p></div>
<div class="m2"><p>رو به سامان ننهد بی سرو سامانی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه تو رحم آوری و نی اجل آید ما را</p></div>
<div class="m2"><p>ار دل سخت تو فریاد و گران جانی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید هرکس رخ تو واله و حیران تو شد</p></div>
<div class="m2"><p>نه همین حسن تو شد باعث حیرانی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آستین اشگ بیفزود و ز دامان بگذشت</p></div>
<div class="m2"><p>آه از این سیل که دارد سر ویرانی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو خورشید عیانست که در ملک جهان</p></div>
<div class="m2"><p>مهوشی نیست چو دلدار صفاهانی ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کافری سخت شد از سستی ما در رده دین</p></div>
<div class="m2"><p>سبب رونق کفر است مسلمانی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز محشر چو سر از خاک لحد برداریم</p></div>
<div class="m2"><p>نام نیکوی تو نقش است به پیشانی ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نبرد صرفه یقین روز جزا ای زاهد</p></div>
<div class="m2"><p>زهد فاش تو ز می ‌خوردن پنهانی ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما صغیر از پی زاهد سوی مسجد نرویم</p></div>
<div class="m2"><p>مسجد ارزانی او میکده ارزانی ما</p></div></div>