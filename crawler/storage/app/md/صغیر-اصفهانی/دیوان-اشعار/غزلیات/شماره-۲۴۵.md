---
title: >-
    شمارهٔ  ۲۴۵
---
# شمارهٔ  ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>دلم به طرهٔ پرچین یار افتد و خیزد</p></div>
<div class="m2"><p>چنان که مست بشبهای تار افتد و خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رخ نماید و تازد سمند ناز هزاران</p></div>
<div class="m2"><p>پیاده در پی آن شهسوار افتد و خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان که سنبل تر از نسیم بر ورق گل</p></div>
<div class="m2"><p>ز شانه زلف بروی نگار افتد و خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب ز نرگس بیمار او که هر که ببیند</p></div>
<div class="m2"><p>همی بنالد و بیماروار افتد و خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غمین مشو ز فتادن براه عشق که سالک</p></div>
<div class="m2"><p>هزار بار در این رهگذار افتد و خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای بوسهٔ یکجای پای ناقهٔ لیلی</p></div>
<div class="m2"><p>هزار مرتبه مجنون زار افتد و خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتادیش چو بپا عزم خاستن مکن آری</p></div>
<div class="m2"><p>نه عاشق است که در پای یار افتد و خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر چون نتواند بروزگار ستیزد</p></div>
<div class="m2"><p>همان به است که با روزگار افتد و خیزد</p></div></div>