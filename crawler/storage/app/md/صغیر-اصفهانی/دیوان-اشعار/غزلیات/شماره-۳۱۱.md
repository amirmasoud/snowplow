---
title: >-
    شمارهٔ  ۳۱۱
---
# شمارهٔ  ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>همیشه موی تو در پیچ و تاب می‌بینم</p></div>
<div class="m2"><p>وز آن بگردن دلها طناب می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی شود دمی از خواب بخت من بیدار</p></div>
<div class="m2"><p>مگر شبی که جمالت بخواب می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدور چشم تو ای شوخ حال مردم را</p></div>
<div class="m2"><p>بسان حالت مستان خراب می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا بغیر دهم نسبتش که من بجهان</p></div>
<div class="m2"><p>ز چشم مست تو هر انقلاب می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده ز آتش می‌هستیم بباد که من</p></div>
<div class="m2"><p>بنای عالم خاکی بر آب می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیب اهل خرد نامرادیست ولی</p></div>
<div class="m2"><p>همیشه بی خردان کامیاب می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر تا زده‌ام دم ز فاتح خیبر</p></div>
<div class="m2"><p>به روی دل همه دم فتح باب می‌بینم</p></div></div>