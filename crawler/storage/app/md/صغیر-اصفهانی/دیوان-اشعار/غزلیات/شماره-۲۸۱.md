---
title: >-
    شمارهٔ  ۲۸۱
---
# شمارهٔ  ۲۸۱

<div class="b" id="bn1"><div class="m1"><p>در خمر طرهٔ طرار کمند اندازش</p></div>
<div class="m2"><p>دل چنان رفت که در خواب ندیدم بازش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی ار حال من و یار بدانی اینست</p></div>
<div class="m2"><p>او بخون پیکر من میکشد و من نازش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ را بیضه وش آورده بزیر پر خویش</p></div>
<div class="m2"><p>مرغ دل تا بهوای تو بود پروازش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست تفسیر شکر خندهٔی از لعل لبت</p></div>
<div class="m2"><p>میرود هر سخن از عیسی و از اعجازش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام جم گیر بکف تا بتو گردد معلوم</p></div>
<div class="m2"><p>که چه انجام جهانست و چه بود آغازش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رازها بس به بر پیر مغانست ولی</p></div>
<div class="m2"><p>تا بدو سر ندهی پی نبری بر رازش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن عشق بود آتش سوزان آری</p></div>
<div class="m2"><p>نتوان کرد بهر خام طمع ابرازش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوشهٔ معرفت آباد خموشی جایی است</p></div>
<div class="m2"><p>کانکه شد ساکن آن هست ملک دمسازش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نی صغیر است که گوید سخن اینگونه بلی</p></div>
<div class="m2"><p>نائی است آنکه تو از نی شنوی آوازش</p></div></div>