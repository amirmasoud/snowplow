---
title: >-
    شمارهٔ  ۴۱۴
---
# شمارهٔ  ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>تو مرغ گلشن قدسی نه در خور قفسی</p></div>
<div class="m2"><p>قفس‌شکن که به گلزار قدس بازرسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را که بر سر طوبی است آشیان آخر</p></div>
<div class="m2"><p>در این چمن ز چه پابست مشت خاروخسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شه زمنی یا گدای گوشه‌نشین</p></div>
<div class="m2"><p>دمی که مرگ درآید نگویدت چه کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پادشاه توانا به وقت لهو و لعب</p></div>
<div class="m2"><p>نترسی و متزلزل ز ناتوان عسسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رسید مرکب چوب و ز تازیانهٔ آز</p></div>
<div class="m2"><p>تو روز و شب به تکاپو چو تندرو فرسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صغیر در پی لذات دنیوی تا کی</p></div>
<div class="m2"><p>همی دو دست به سر میزنی مگر مگسی</p></div></div>