---
title: >-
    شمارهٔ  ۹۰
---
# شمارهٔ  ۹۰

<div class="b" id="bn1"><div class="m1"><p>زنده نبود آنکه او را معرفت در کار نیست</p></div>
<div class="m2"><p>مرده پندارش که هیچ از عمر برخوردار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معرفت موقوف دیدار است و بس یا للعجب</p></div>
<div class="m2"><p>چو نشود عرفان حق حاصل اگر دیدار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم ایجاد از روی مثل آئینه ایست</p></div>
<div class="m2"><p>واندر آن آئینه پیدا جز جمال یار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمهٔ تحقیقت ار بخشد بچشم دل ضیاء</p></div>
<div class="m2"><p>غیر او بینی در این دیر کهن دیار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دفع پندار من و ما کن ز خود او را ببین</p></div>
<div class="m2"><p>کاین من و ما در حقیقت هیچ جز پندار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راستی کیفیت منصور ثابت می کند</p></div>
<div class="m2"><p>اینکه جای حرف حق جز بر فراز دار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بذل جان کردند مردان بهر حرف حق صغیر</p></div>
<div class="m2"><p>بیخودانرا ترک جان بهر خدا دشوار نیست</p></div></div>