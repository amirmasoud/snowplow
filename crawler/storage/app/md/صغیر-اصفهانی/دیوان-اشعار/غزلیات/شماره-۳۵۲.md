---
title: >-
    شمارهٔ  ۳۵۲
---
# شمارهٔ  ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>گریم بکار دل من و دل هم بکار من</p></div>
<div class="m2"><p>این است روزگار دل و روزگار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که اختیار دل آرم بکف کنون</p></div>
<div class="m2"><p>بینم که هست در کف دل اختیار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس در دل است داغ عزیزان عجب مدار</p></div>
<div class="m2"><p>گر بعد مرگ لاله دمد از مزار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با اینکه ناامید ز خویشم بلطف دوست</p></div>
<div class="m2"><p>دارد‌ امیدها دل ‌امیدوار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتشکده است سینهٔ من خواهی ار دلیل</p></div>
<div class="m2"><p>اینک حرارت نفس شعله بار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر هستی اعتبار فزاید چه حکمتست</p></div>
<div class="m2"><p>کافزوده نیستی به جهان اعتبار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در انتظار یارم و ترسم بسر رسد</p></div>
<div class="m2"><p>عمر من و بسر نرسد انتظار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر شب بیاد ان مه بی مهر تا سحر</p></div>
<div class="m2"><p>خون میچکد ز دیدهٔ اختر شمار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جور رقیب و فتنهٔ دور زمان صغیر</p></div>
<div class="m2"><p>سهل است لیک باشد اگر یار یار من</p></div></div>