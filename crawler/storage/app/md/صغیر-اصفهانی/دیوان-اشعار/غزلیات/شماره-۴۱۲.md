---
title: >-
    شمارهٔ  ۴۱۲
---
# شمارهٔ  ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>برو بجو ز عمل مونسی و داد رسی</p></div>
<div class="m2"><p>که مونست نبود جز عمل بگور کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عروس دهر گرت یار شد تو غره مشو</p></div>
<div class="m2"><p>که دیده همچو تو داماد این عجوزه بسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفس نفس گذرد عمر و سال گردد و مه</p></div>
<div class="m2"><p>تو را به هر نفسی در سر اوفتد هوسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه منکر حشری بحالتی که خدای</p></div>
<div class="m2"><p>حیات تازه ببخشد ترا به هر نفسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریب غول بیابان مخور مرو از راه</p></div>
<div class="m2"><p>بدار گوش دل خود بنالهٔ جرسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو ز حق بطلب راه وادی ایمن</p></div>
<div class="m2"><p>که چون کلیم دلیل رهت شود قبسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن ز دست رها دامن علی چو صغیر</p></div>
<div class="m2"><p>که جز علی نبود در دو کون دادرسی</p></div></div>