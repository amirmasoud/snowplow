---
title: >-
    شمارهٔ  ۱۶۰
---
# شمارهٔ  ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>تشبیه جمال تو به خورشید روا نیست</p></div>
<div class="m2"><p>این رتبه بلی در خور هر بی سر و پا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو تا نزند دم دگر از عشق و ارادت</p></div>
<div class="m2"><p>آن را که بپایت سر تسلیم و رضا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دوست اگر نوش ببینند و اگر نیش</p></div>
<div class="m2"><p>رندان جهان را بزبان چون و چرا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جور تو میسوزم و می‌سازم و شادم</p></div>
<div class="m2"><p>چون جور و جفای تو بجز مهر و وفا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با باد صبا چون بتو پیغام فرستم</p></div>
<div class="m2"><p>آنجا که توئی رهگذر باد صبا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر طایفه را روی نیاز است به سوئی</p></div>
<div class="m2"><p>ما را بجز ابروی تو محراب دعا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غیر از تو مرا نیست بخاطر ولی افسوس</p></div>
<div class="m2"><p>یادی نکنی از من و این از تو روا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیوسته صغیر است بدرگاه تو نالان</p></div>
<div class="m2"><p>جز این بدر شاه بلی رسم گدا نیست</p></div></div>