---
title: >-
    شمارهٔ  ۴۲۰
---
# شمارهٔ  ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>منه به زمرهٔ انعام نام انسانی</p></div>
<div class="m2"><p>که برتر از ملک‌ آمد مقام انسانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشمهٔ حیوان ننگرد بچشم طمع</p></div>
<div class="m2"><p>دو جرعه هرکه بنوشد ز جام انسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی مرام مقدس که مسلک رسل است</p></div>
<div class="m2"><p>همه مقدمه بهر مرام انسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجای ماند از آن جبرئیل در ره عشق</p></div>
<div class="m2"><p>که دید طی شود این ره بگام انسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن بسجدهٔ آدم ملک مکلف شد</p></div>
<div class="m2"><p>که تا پدید شود احترام انسانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گفت نیست فلکر استون که تا محشر</p></div>
<div class="m2"><p>قیام چرخ بود از قیام انسانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر جملهٔ اشیاء طفیل انسانند</p></div>
<div class="m2"><p>ببین تو حشمت حق ز احتشام انسانی</p></div></div>