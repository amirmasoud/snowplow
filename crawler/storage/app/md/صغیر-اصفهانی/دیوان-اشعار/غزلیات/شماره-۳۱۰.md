---
title: >-
    شمارهٔ  ۳۱۰
---
# شمارهٔ  ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>به هر چه می‌نگرم حسن یار می‌بینم</p></div>
<div class="m2"><p>تجلیات جمال نگار می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت آن که یکی را هزار می‌دیدم</p></div>
<div class="m2"><p>کنون یکی نگرم گر هزار می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال یار مرا تا که در کنار‌ آمد</p></div>
<div class="m2"><p>مراد هر دو جهان در کنار می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آندمم که خداوند چشم گل بین داد</p></div>
<div class="m2"><p>صفای برگ گل از نوک خار می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آن زمان که دلم ترک روزگار گرفت</p></div>
<div class="m2"><p>به کام دل همه ی روزگار می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار میکده را چون بدیدگان نکشم</p></div>
<div class="m2"><p>که نور دیدهٔ خود زین غبار می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که خلق جهانش نهفته میجویند</p></div>
<div class="m2"><p>بجان دوست منش آشکار می‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر این همه و صدهزار از این افزون</p></div>
<div class="m2"><p>ز لطف حیدر دلدل سوار می‌بینم</p></div></div>