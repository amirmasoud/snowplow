---
title: >-
    شمارهٔ  ۱۹۳
---
# شمارهٔ  ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>کسیکه بهر خدا ترک خودستائی کرد</p></div>
<div class="m2"><p>تواند آنکه بملک خدا خدائی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلام صافی آئینه‌ام که رد ننمود</p></div>
<div class="m2"><p>بهر لباس برش هر که خودنمائی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان خلق جهان همچو شانه باید بود</p></div>
<div class="m2"><p>که خویش را همه وقف گره گشائی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر نشان طلبی دوستان مخلص را</p></div>
<div class="m2"><p>ببین که یاد تو در روز بی نوائی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موافق اند بهم نوع خویش حیوانات</p></div>
<div class="m2"><p>ندانم از چه بشر از بشر جدائی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس است آنهمه بیگانگی دو روزی هم</p></div>
<div class="m2"><p>برای تجربه می‌باید آشنائی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شعر را به شعیری نمی خرند صغیر</p></div>
<div class="m2"><p>عجب که باز توانی سخن سرائی کرد</p></div></div>