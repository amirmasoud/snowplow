---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>ای نام عاشق‌سوز تو ورد زبان‌ها</p></div>
<div class="m2"><p>وی یاد جان‌افروز تو آرام جان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با اینکه بیرون از زمان و از مکانی</p></div>
<div class="m2"><p>شاه زمان‌ها هستی و ماه مکان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی نفخت فیه من روحی به قرآن</p></div>
<div class="m2"><p>هستی خود ای جان جهان روح روان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منزل گرفتی در دل دلدادگانت</p></div>
<div class="m2"><p>گرچه نگنجی در زمین و آسمان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرط ظهورت پرده روی نکو شد</p></div>
<div class="m2"><p>ای بی‌نشان پنهان شدستی در نشان‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش بدیعی باشدت از خامه صنع</p></div>
<div class="m2"><p>هر گل که می‌روید به طرف بوستان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرگز ندیدیم از تو غیر از مهربانی</p></div>
<div class="m2"><p>ای مهربان‌تر از تمام مهربان‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصفت نیامد در بیان یک از هزاران</p></div>
<div class="m2"><p>چندان که گفتند اهل بینش داستان‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کی می‌توان بردن صغیر این ره به پایان</p></div>
<div class="m2"><p>در این بیابان گشته گم بس کاروان‌ها</p></div></div>