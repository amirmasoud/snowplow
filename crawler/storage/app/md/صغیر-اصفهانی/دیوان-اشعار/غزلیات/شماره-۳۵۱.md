---
title: >-
    شمارهٔ  ۳۵۱
---
# شمارهٔ  ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>خوش بود دیده ز جان بستن و جانان دیدن</p></div>
<div class="m2"><p>رحمت وصل پس از زحمت هجران دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان مخوانید بخلدم که به طوبی ارزد</p></div>
<div class="m2"><p>یک نظر قامت آن سرو خرامان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بهشتی تو که دیدار تو را هر که بدید</p></div>
<div class="m2"><p>کرد صرف نظر از روضهٔ رضوان دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک ریزد بصر از دیدن روی تو که نیست</p></div>
<div class="m2"><p>دیده را طاقت خورشید درخشان دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که بی دوست شود عمر تو در عالم طی</p></div>
<div class="m2"><p>حاصلت چیست از این رنج فراوان دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کور شد چشم زلیخا بغم یوسف و گفت</p></div>
<div class="m2"><p>کوریم به بود از یار بزندان دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که در عرصهٔ میدان غمش افتادی</p></div>
<div class="m2"><p>بایدت گوی صفت لطمهٔ چوگان دیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دل خضر بود چشمهٔ حیوان چه روی</p></div>
<div class="m2"><p>چون سکندر ز پی چشمهٔ حیوان دیدن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیده بربند صغیر از خود و بین طلعت دوست</p></div>
<div class="m2"><p>چشم خودبین نتواند رخ جانان دیدن</p></div></div>