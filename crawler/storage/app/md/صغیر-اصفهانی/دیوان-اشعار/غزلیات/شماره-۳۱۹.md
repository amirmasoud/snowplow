---
title: >-
    شمارهٔ  ۳۱۹
---
# شمارهٔ  ۳۱۹

<div class="b" id="bn1"><div class="m1"><p>چگونه سر ز در پیر فقر بردارم</p></div>
<div class="m2"><p>که گنج گوهر مقصود زیر سر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آشیانه ندارم چه غم که چون عنقا</p></div>
<div class="m2"><p>جهان و هر چه در آن هست زیر پر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الا که مهلکهٔ آز را هنر دانی</p></div>
<div class="m2"><p>بجان دوست که من ننگ از این هنر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مبین به مفلسیم منعما که در ره عشق</p></div>
<div class="m2"><p>ز اشگ و عارض خود گنج سیم و زر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلندی نظرم بین که درگه پرواز</p></div>
<div class="m2"><p>فراز کنگرهٔ عرش در نظر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا بحالت خود واگذار ای ناصح</p></div>
<div class="m2"><p>از آنچه بی خبر استی تو من خبر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه سازم اینکه همی ناز یار گردد بیش</p></div>
<div class="m2"><p>نیاز حضرت او هر چه بیشتر دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صغیر از دل جانان مرا شکایت نیست</p></div>
<div class="m2"><p>شکایت ار بود از آه بی اثر دارم</p></div></div>