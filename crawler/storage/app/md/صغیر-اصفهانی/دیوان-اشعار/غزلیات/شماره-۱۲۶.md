---
title: >-
    شمارهٔ  ۱۲۶
---
# شمارهٔ  ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>دور هجران را مگر اتمام نیست</p></div>
<div class="m2"><p>یا مگر صبح از پی این شام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با که بفرستم غم دل را بدوست</p></div>
<div class="m2"><p>چون صبا هم محرم پیغام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی منت تنها به دام افتاده‌ام</p></div>
<div class="m2"><p>کیست آنکو بستهٔ این دام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوانده‌ام تاریخ دور روزگار</p></div>
<div class="m2"><p>هیچ دوری به ز دور جام نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون دل من عاشقی آغاز کرد</p></div>
<div class="m2"><p>گفتم آعاز تو را انجام نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کسی کامی گرفت از یار خویش</p></div>
<div class="m2"><p>کس چو من در عاشقان ناکام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقی را شرط بی اندازه است</p></div>
<div class="m2"><p>این قبا زیبا به هر اندام نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر طریق عشق می پوئی صغیر</p></div>
<div class="m2"><p>پخته شو کاین ره ره هر خام نیست</p></div></div>