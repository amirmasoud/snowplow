---
title: >-
    شمارهٔ  ۱۵۷
---
# شمارهٔ  ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>تا نگوئی بجهان دوست مرا بسیار است</p></div>
<div class="m2"><p>دوست اکسیر بود این سخن از اسرار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راستی ز اهل صفا دوست بدست آوردن</p></div>
<div class="m2"><p>آید آسان بنظر لیک بسی دشوار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه دارد بنظر نفع خود از صحبت دوست</p></div>
<div class="m2"><p>دوست نبود ز حریفان سر بازار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بسا دوست نما دشمن جانی که تمام</p></div>
<div class="m2"><p>در تظاهر پی منظور خود آن مکار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بسا دوست که با دوست زند لاف وفا</p></div>
<div class="m2"><p>لیک گاه عمل از وی بری و بیزار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بسا دوست که ازابلهی و نادانی</p></div>
<div class="m2"><p>دوست را مایهٔ صدگونه غم و آزار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جذب گفتار مشو دوست مدان آنکس را</p></div>
<div class="m2"><p>که نه گفتار وی‌ام یخته با کردار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوست آنست که هنگام گرفتاری دوست</p></div>
<div class="m2"><p>از طریق عمل آن غمزده را غمخوار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوستی خود ثمر نخل وجود من و تست</p></div>
<div class="m2"><p>سوختن در خور نخل است اگر بی بار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اثر دوستی و مهر و محبت باشد</p></div>
<div class="m2"><p>آنچه در دهر ز صاحب اثران آثار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهتر آنست کزین مسئلهٔ دور و دراز</p></div>
<div class="m2"><p>رشته کوتاه کنم ورنه سخن بسیار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون کنایت ز صراحت بود اولی اینجا</p></div>
<div class="m2"><p>با همه بی نظری ها نظری در کار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مختصر شرحی اگر گفته‌ام از مهر و وفا</p></div>
<div class="m2"><p>پی به مقصود برد آنکه دلش بیدار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با خدا باش و بپوش از همه کس دیده صغیر</p></div>
<div class="m2"><p>فارغست از همه کس آنکه خدایش یار است</p></div></div>