---
title: >-
    شمارهٔ  ۸۴
---
# شمارهٔ  ۸۴

<div class="b" id="bn1"><div class="m1"><p>بتی کو عزم دوری داشت با من با منست امشب</p></div>
<div class="m2"><p>گل از رخسار او در بزم گلشن گلشنست امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی ایثار او لعل و عقیق و لؤلؤ و مرجان</p></div>
<div class="m2"><p>مرا از اشک خون آلود دامن دامنست امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بده ساقی می گلگون بزن مطرب دف و بربط</p></div>
<div class="m2"><p>که بزم از غیر چون وادی ایمن ایمن است امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش ناوک مژگان آن ترک کمان ابرو</p></div>
<div class="m2"><p>زره آسا دل مجروح روزن روزن است امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباشد حاجت شمع و چراغی محفل ما را</p></div>
<div class="m2"><p>که بزم جان و دل زان روی روشن روشنست امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته توبه و طرف کله مستان بیا زاهد</p></div>
<div class="m2"><p>ببین در بزم میخواران چه بشکن بشکنست امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صغیر از حاصل وصلش نبود امید یکخوشه</p></div>
<div class="m2"><p>ولی شامل مرا آن فیض خرمن خرمنست امشب</p></div></div>