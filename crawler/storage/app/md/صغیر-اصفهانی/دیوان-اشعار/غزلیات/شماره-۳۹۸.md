---
title: >-
    شمارهٔ  ۳۹۸
---
# شمارهٔ  ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>ایکه نام آن صنم را در بر من میبری</p></div>
<div class="m2"><p>باخبر شو نام بت پیش برهمن میبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایدل افتادی چو در دنبال آن سیمین ذقن</p></div>
<div class="m2"><p>خویش دانستم سوی چاهم چو بیژن میبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حقیقت پهلوان عشقست ای دستان سرای</p></div>
<div class="m2"><p>چندنام از گیو و گودرز وتهمتن میبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچهٔی شکرانهٔ دولت بما اکرام کن</p></div>
<div class="m2"><p>ایکه از باغ وصالش گل بدامن میبری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر ای بیگانه‌پرور آشنا این شیوه چیست</p></div>
<div class="m2"><p>میدهی کام رقیبان و دل از من میبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزهٔ چشمان کنی با عشوهٔ ابرو قرین</p></div>
<div class="m2"><p>هر کجا باشد دلی آنرا باین فن میبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بری از دوستانت دین و دل نبود عجب</p></div>
<div class="m2"><p>کز لطافت ای پرویش دل ز دشمن میبری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ایکه ما دردی کشانرا دانی از اهل خطا</p></div>
<div class="m2"><p>بی سبب نیست از سوء خود در حق ما ظن میبری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شعر خود بر وزن شعر شیخ میگوئی صغیر</p></div>
<div class="m2"><p>شرم بادت خوشه ئی را پیش خرمن میبری</p></div></div>