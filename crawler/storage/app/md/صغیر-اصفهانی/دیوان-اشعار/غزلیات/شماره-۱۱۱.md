---
title: >-
    شمارهٔ  ۱۱۱
---
# شمارهٔ  ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>هی زلف و خال جلوه دهی این بهانه چیست</p></div>
<div class="m2"><p>هستیم خود اسیر تو این دام و دانه چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این خانه ای که زلف تو بر دل فروخته</p></div>
<div class="m2"><p>در آن دگر حساب صبا حق شانه چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی آگه از اذان مؤذن شوی اگر</p></div>
<div class="m2"><p>ناقوس را بدیر ندانی ترانه چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دهر نام نیک بنه نی سرای نیک</p></div>
<div class="m2"><p>آری ز نام نیک نکوتر نشانه چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکس گدای درگه حیدر بود صغیر</p></div>
<div class="m2"><p>داند که فیض بخشی این آستانه چیست</p></div></div>