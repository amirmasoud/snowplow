---
title: >-
    شمارهٔ  ۵۶
---
# شمارهٔ  ۵۶

<div class="b" id="bn1"><div class="m1"><p>آنچه میدانیش دنیا خورد و خوابی بیش نیست</p></div>
<div class="m2"><p>وانچه میخوانیش گردون پیچ و تابی بیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس کام مراد از بحر امکان تر نکرد</p></div>
<div class="m2"><p>راستی چون بنگری عالم سرابی بیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکنت و ثروت عیال و مال و ایوان و سرا</p></div>
<div class="m2"><p>روی هم چون جمع سازی اضطرابی بیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست هستی بحر ژرفی موج‌خیز و بی‌کران</p></div>
<div class="m2"><p>واندران دریا وجود ما حبابی بیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمر نوح و گنج قارون ملک اسکندر تو را</p></div>
<div class="m2"><p>گر میسر شد بوقت مرگ خوابی بیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اهل دنیا عمر خود را صرف دنیا می‌کنند</p></div>
<div class="m2"><p>گر حیات اینست خود سوءالعذابی بیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از حلال و از حرام مال مال اندوز را</p></div>
<div class="m2"><p>عاید و واصل حسابی یا عقابی بیش نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جهان آمد صغیر و چند روزی ماند و رفت</p></div>
<div class="m2"><p>یادگار از وی در اینعالم کتابی بیش نیست</p></div></div>