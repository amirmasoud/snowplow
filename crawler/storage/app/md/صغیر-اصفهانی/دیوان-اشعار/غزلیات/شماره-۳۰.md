---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>چو در کویت وطن دارم نجویم باغ رضوان را</p></div>
<div class="m2"><p>چو بر رویت نظر دارم نخواهم حور و غلمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه کافر نی مسلمانم که یاد طره و رویت</p></div>
<div class="m2"><p>ببرد از خاطرم یکباره شرح کفر و ایمان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو جرعه باده‌ام ساقی کرم کرد و یکی دیدم</p></div>
<div class="m2"><p>می و مینا و ساقی عشق و عاشق جان و جانان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی در خواب می‌دیدند کاش آن طره را آنان</p></div>
<div class="m2"><p>که بر من خرده می‌گیرند گفتار پریشان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندید ار زاهد آن رخسار این نبود عجب آری</p></div>
<div class="m2"><p>نبیند چشم نابینا رخ خورشید تابان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عالم هرکسی آورده بر کف از کسی دامان</p></div>
<div class="m2"><p>صغیر آورده بر کف دامن شاه خراسان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم خاک کف پای سگ کوی شهنشاهی</p></div>
<div class="m2"><p>که ضامن شد ز راه مرحمت وحش بیابان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی‌خواهم به آن چیزی که خود می‌داند آن سرور</p></div>
<div class="m2"><p>نوازد از طریق لطف این عبد ثناخوان را</p></div></div>