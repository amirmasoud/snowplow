---
title: >-
    شمارهٔ  ۳۴۲
---
# شمارهٔ  ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>کرده عشق تو چنان بیخبر و بی خویشم</p></div>
<div class="m2"><p>که به غیر از تو دگر هیچ نمی‌اندیشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم از نوش نوازی ورم از نیش زنی</p></div>
<div class="m2"><p>هم بنوش تو ز جان مایل و هم بر نیشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر من‌امید عنایت ز تو دارم شاید</p></div>
<div class="m2"><p>تو شه مملکت حسنی و من درویشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مذهب عشق بنازم که به یکباره نمود</p></div>
<div class="m2"><p>فارغ از هر روش و بی خبر از هر کیشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد فارغ طلب وصل تو از هر کارم</p></div>
<div class="m2"><p>ساخت بیگانه غم عشق تو از هر خویشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جنون من و مجنون بود ار فرق اینست</p></div>
<div class="m2"><p>که در این راه دو صد مرحله از وی پیشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب آن شوخ شکرخند نباتی است صغیر</p></div>
<div class="m2"><p>که نمک ریخته داغش بدرون ریشم</p></div></div>