---
title: >-
    شمارهٔ  ۶۳
---
# شمارهٔ  ۶۳

<div class="b" id="bn1"><div class="m1"><p>در جهان امری که بیرونست از تقدیر چیست</p></div>
<div class="m2"><p>وانچه تقدیر است در تغییر آن تدبیر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که دام خلق می سازی نماز خویش را</p></div>
<div class="m2"><p>پیش خیر الماکرین این حیله و تزویر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه داند جمله قرآنرا بجز لفظ زکوه</p></div>
<div class="m2"><p>مات و حیران مانده کاین یک صرف تفسیر چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دم مبند از ناله تا تأثیر آن بینی مگوی</p></div>
<div class="m2"><p>حاصلم زین ناله و افغان بی تأثیر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ آید ناگهانی ای به هر کاری عجول</p></div>
<div class="m2"><p>این همه در کار تو به علت تأثیر چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قول الناس نیامم کرده بس حالت پریش</p></div>
<div class="m2"><p>کاینهمه خواب پریشان مرا تعبیر چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون صغیر از مهر حیدر کن مس قلبت طلا</p></div>
<div class="m2"><p>تا بدانی در حقیقت معنی اکسیر چیست</p></div></div>