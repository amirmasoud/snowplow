---
title: >-
    شمارهٔ  ۱۷۶
---
# شمارهٔ  ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>مگذار اینکه راز دلت بر زبان رسد</p></div>
<div class="m2"><p>گر بر زبان رسید بگوش جهان رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره بر زیان ببند و زبان را نگاهدار</p></div>
<div class="m2"><p>بر شمع هر زیان که رسد از زبان رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی که حال روح چه باشد ز بعد مرک</p></div>
<div class="m2"><p>مرغی قفس شکسته که بر آشیان رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وامانده گان قافله را غول ره زند</p></div>
<div class="m2"><p>آن رهرو ایمن است که بر کاروان رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل به نوبهار از آن در ترنم است</p></div>
<div class="m2"><p>کز وصل گل به کام دل ناتوان رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل در تبسم است که از گلبن مراد</p></div>
<div class="m2"><p>برگی نچیده بلبل شیدا خزان رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا زنده ای مخور غم روزی که چون تنور</p></div>
<div class="m2"><p>باز است تا دهان تو هم بر تو نان رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش خواه بهر غیر صغیرا که از خدای</p></div>
<div class="m2"><p>خواهی هر آنچه بهر کسان بر تو آن رسد</p></div></div>