---
title: >-
    شمارهٔ ۳۲ - قطعه
---
# شمارهٔ ۳۲ - قطعه

<div class="b" id="bn1"><div class="m1"><p>یکی را گه و بی گه اندر سرای</p></div>
<div class="m2"><p>بسرقت همی رفت مال و منال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آورد دیوار خانه به ابر</p></div>
<div class="m2"><p>نبود ایمن از دزد در عین حال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی گفتش این دزد در خانه است</p></div>
<div class="m2"><p>تو بندی ره بام چشمت بمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن فهم کن زین مثل ای عزیز</p></div>
<div class="m2"><p>چو نفست زند ره ز شیطان منال</p></div></div>