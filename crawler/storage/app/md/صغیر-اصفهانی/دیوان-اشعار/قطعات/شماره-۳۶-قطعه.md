---
title: >-
    شمارهٔ ۳۶ - قطعه
---
# شمارهٔ ۳۶ - قطعه

<div class="b" id="bn1"><div class="m1"><p>به هر چیز مهرت فزونتر بود</p></div>
<div class="m2"><p>همان را پرستش کنی هرچه هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مهرت به دنیا ز حق بیش شد</p></div>
<div class="m2"><p>تو دنیاپرستی نئی حق‌پرست</p></div></div>