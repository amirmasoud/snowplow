---
title: >-
    شمارهٔ ۱۸ - قطعه
---
# شمارهٔ ۱۸ - قطعه

<div class="b" id="bn1"><div class="m1"><p>پرورد بهر خدمت خلق آنکه خویش را</p></div>
<div class="m2"><p>حاصل رضای حضرت پروردگار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نام نیک زنده بود خیرخواه خلق</p></div>
<div class="m2"><p>کی میرد آنکه خدمت مردم شعار کرد</p></div></div>