---
title: >-
    شمارهٔ ۲۸ - قطعه
---
# شمارهٔ ۲۸ - قطعه

<div class="b" id="bn1"><div class="m1"><p>مبر رشک بر آنکه او را ملک</p></div>
<div class="m2"><p>پی‌امتحان کرد روزی بلند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه بسیار کس را که این گنج نهاد</p></div>
<div class="m2"><p>فرا برد‌ام روز و فردا فکند</p></div></div>