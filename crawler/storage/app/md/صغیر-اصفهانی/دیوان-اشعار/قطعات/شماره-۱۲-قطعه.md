---
title: >-
    شمارهٔ ۱۲ - قطعه
---
# شمارهٔ ۱۲ - قطعه

<div class="b" id="bn1"><div class="m1"><p>چو باید عاقبت رفتن ز دنیا</p></div>
<div class="m2"><p>به دنیا دل نبندد مرد دانا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان باشد رباطی کهنه باید</p></div>
<div class="m2"><p>از اینجا بهر منزل شد مهیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدنیا بهر عقبی کار میکن</p></div>
<div class="m2"><p>که اینجا هرچه کردی داری آنجا</p></div></div>