---
title: >-
    شمارهٔ ۴۵ - قطعه
---
# شمارهٔ ۴۵ - قطعه

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که هرچند خامه فرسودم</p></div>
<div class="m2"><p>دهان به مدح و به ذم کسی نیالودم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا نبود طمع خلق را کرم زین رو</p></div>
<div class="m2"><p>من از معاملهٔ هجو و مدح آسودم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هیچ قیمت نفروختم جواهر خویش</p></div>
<div class="m2"><p>به کس برای صلت هیچ مدح نسرودم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلاف آنکه بانده زید ز طالع بد</p></div>
<div class="m2"><p>من از مساعدت بخت خویش خوشنودم</p></div></div>