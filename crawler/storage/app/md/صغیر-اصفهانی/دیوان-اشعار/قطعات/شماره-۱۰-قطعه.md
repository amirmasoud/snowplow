---
title: >-
    شمارهٔ ۱۰ - قطعه
---
# شمارهٔ ۱۰ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ز تبدیل خزانی و بهاری</p></div>
<div class="m2"><p>فزاید عاقلان را هوشیاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که دارد زندگانی مرک در پی</p></div>
<div class="m2"><p>اگرچه مردمند از آن فراری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک با کس نیابد یک قدم راست</p></div>
<div class="m2"><p>که او را پیشه باشد کجمداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گیری سخت دنیا را که دارد</p></div>
<div class="m2"><p>کمال سستی و بی‌اعتباری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت فرصت بود در دست بگذار</p></div>
<div class="m2"><p>به عالم نام نیکی یادگاری</p></div></div>