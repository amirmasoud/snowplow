---
title: >-
    شمارهٔ ۱۸ - فی‌النصیحه
---
# شمارهٔ ۱۸ - فی‌النصیحه

<div class="b" id="bn1"><div class="m1"><p>ایکه سرگرم باندوختن سیم و زری</p></div>
<div class="m2"><p>سیم و ز توشه ره نیست عجب بیخبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خصم جان تو بود اینکه تو دولت شمری</p></div>
<div class="m2"><p>آن نه دولت که بصد حسرت از آن درگذری</p></div></div>
<div class="b2" id="bn3"><p>دولت آنستکه در گور بهمره ببری</p></div>
<div class="b" id="bn4"><div class="m1"><p>چند اندیشه تعمیر سرایت بسر است</p></div>
<div class="m2"><p>تا بکی نقش فلان خانه تو را در نظر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند گوئی که چنین زشت و چنان خوبتر است</p></div>
<div class="m2"><p>وطنت جای دگر باشد و منزل دگر است</p></div></div>
<div class="b2" id="bn6"><p>تو در این شهر غریبی و کنون در بدری</p></div>
<div class="b" id="bn7"><div class="m1"><p>مقصد ذات حق از خلقت ما معرفت است</p></div>
<div class="m2"><p>مایه دولت تسلیم و رضا معرفت است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مطلقا دادرس روز جزا معرفت است</p></div>
<div class="m2"><p>فرق انسان و بهائم بخدا معرفت است</p></div></div>
<div class="b2" id="bn9"><p>گر تو بی‌معرفت استی ز بهائم بتری</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای تو را طرح وجود از ملک و از حیوان</p></div>
<div class="m2"><p>گاه رحمن بتو فرمانده و گاهی شیطان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سعی کن بنده این باش و مشو تابع آن</p></div>
<div class="m2"><p>که اگر تابع شیطان نشوی ای انسان</p></div></div>
<div class="b2" id="bn12"><p>در مقامات تو بر جن و ملک مفتخری</p></div>
<div class="b" id="bn13"><div class="m1"><p>چشم تحقیق گشا تا که بینی بملا</p></div>
<div class="m2"><p>همه را عاجز و بیچاره چه شاه و چه گدا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اعمی آن نیست که از دیده بود نابینا</p></div>
<div class="m2"><p>اعمی آنست که نبود نظرش سوی خدا</p></div></div>
<div class="b2" id="bn15"><p>گر بپوشی نظر از خلق تو صاحبنظری</p></div>
<div class="b" id="bn16"><div class="m1"><p>تا بکی عمر گرانمایه کنی صرف گناه</p></div>
<div class="m2"><p>وانگهی فاش که خلقت بنمایند نگاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میکنی فخر بنا بردن فرمان اله</p></div>
<div class="m2"><p>پا بدین میزنی و میشکنی طرف کلاه</p></div></div>
<div class="b2" id="bn18"><p>شرمی آخر ز خدا کن چقدر خیره‌سری</p></div>
<div class="b" id="bn19"><div class="m1"><p>وقت آنست کسان فاتحه‌خوان تو شوند</p></div>
<div class="m2"><p>دل تسلی ده خویشان و کسان تو شوند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اقربا بر دهن انگشت گزان تو شوند</p></div>
<div class="m2"><p>خلق از روی تحیر نگران تو شوند</p></div></div>
<div class="b2" id="bn21"><p>تو هنوز از پی ترتیب بخود مینگری</p></div>
<div class="b" id="bn22"><div class="m1"><p>سخنت جمله صغیرا بر ارباب عقول</p></div>
<div class="m2"><p>رای عقلست و پسندیده طبع و مقبول</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نا قبول آنکه کلامت نه بجان کرد قبول</p></div>
<div class="m2"><p>لیک آن به که نگردی توهم از پند ملول</p></div></div>
<div class="b2" id="bn24"><p>این نصایح بخود آموز که شایسته‌تری</p></div>