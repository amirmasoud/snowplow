---
title: >-
    شمارهٔ ۱۹ - نصیحت
---
# شمارهٔ ۱۹ - نصیحت

<div class="b" id="bn1"><div class="m1"><p>ای بشر ای تو گنج نهانی</p></div>
<div class="m2"><p>ای بشر ای جهان معانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمین کوکب آسمانی</p></div>
<div class="m2"><p>در قفس طایر لامکانی</p></div></div>
<div class="b2" id="bn3"><p>تا بکی قدر خود را ندانی</p></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌خود با خود آخود رها کن</p></div>
<div class="m2"><p>خود مبین و بخود دیده واکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده روشن بنور خدا کن</p></div>
<div class="m2"><p>دل به اسرار خود آشنا کن</p></div></div>
<div class="b2" id="bn6"><p>تا کی از خویشتن دورمانی</p></div>
<div class="b" id="bn7"><div class="m1"><p>ای بشر از دو بینی بدرباش</p></div>
<div class="m2"><p>وحدت حق ببین با نظر باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دل و یک جهت با بشر باش</p></div>
<div class="m2"><p>با بشر بد مکن با خبر باش</p></div></div>
<div class="b2" id="bn9"><p>کانچه دادی همان می‌ستانی</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای بشر کین و شر تا کی و چند</p></div>
<div class="m2"><p>کین و شر با بشر تا کی و چند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فتنه در بحر و بر تا کی و چند</p></div>
<div class="m2"><p>دشمن یکدگر تا کی و چند</p></div></div>
<div class="b2" id="bn12"><p>بس کن آخر از این دل گرانی</p></div>
<div class="b" id="bn13"><div class="m1"><p>ما گروه بشر هرچه هستیم</p></div>
<div class="m2"><p>در مقام ار به اوج ار به پستیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عضو هم چون سروپا و دستیم</p></div>
<div class="m2"><p>در دورنگی چو از هم گسستیم</p></div></div>
<div class="b2" id="bn15"><p>سخت از آن شد بما زندگانی</p></div>
<div class="b" id="bn16"><div class="m1"><p>آن رهی کان طریق رشاد است</p></div>
<div class="m2"><p>اتحاد اتحاد اتحاد است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون ز خلقت محبت مراد است</p></div>
<div class="m2"><p>هر سخن کان بضد و داد است</p></div></div>
<div class="b2" id="bn18"><p>زان حذر بایدت تا توانی</p></div>
<div class="b" id="bn19"><div class="m1"><p>ای بشر شو رهین محبت</p></div>
<div class="m2"><p>جان و دل کن قرین محبت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بلکه باز آ بدین محبت</p></div>
<div class="m2"><p>تا رهی با یقین محبت</p></div></div>
<div class="b2" id="bn21"><p>چون صغیر از غم بدگمانی</p></div>