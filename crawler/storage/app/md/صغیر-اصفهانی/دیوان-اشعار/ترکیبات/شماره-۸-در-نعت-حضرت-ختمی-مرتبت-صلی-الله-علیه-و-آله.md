---
title: >-
    شمارهٔ ۸ - در نعت حضرت ختمی مرتبت صلی‌الله علیه و آله
---
# شمارهٔ ۸ - در نعت حضرت ختمی مرتبت صلی‌الله علیه و آله

<div class="b" id="bn1"><div class="m1"><p>خواست چو از غیب ذات شاهد یکتا</p></div>
<div class="m2"><p>جلوه دهد در ظهور طلعت زیبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنز خفی را کند به خلق هویدا</p></div>
<div class="m2"><p>بهر تماشای آن جمال دلا را</p></div></div>
<div class="b2" id="bn3"><p>آینهٔی ساخت از جمال محمد</p></div>
<div class="b" id="bn4"><div class="m1"><p>حرخ مدور نبود ومهر مشعشع</p></div>
<div class="m2"><p>انجم و اختر نبود و ماه ملمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صادر و مصدر نبود و طالع و مطلع</p></div>
<div class="m2"><p>بود خود از خلعت وجود مخلع</p></div></div>
<div class="b2" id="bn6"><p>قامت قائم به اعتدال محمد</p></div>
<div class="b" id="bn7"><div class="m1"><p>یافته گانرا به یمن فر همایی</p></div>
<div class="m2"><p>میدهد از ذلت دو کون رهایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملتزم عزتست ورتبه فزایی</p></div>
<div class="m2"><p>عین جلالست و ذوالجلال ستایی</p></div></div>
<div class="b2" id="bn9"><p>بندگی در گه جلال محمد</p></div>
<div class="b" id="bn10"><div class="m1"><p>ذات قدیمی است آن روان مقدس</p></div>
<div class="m2"><p>خود به لباس حدوث گشته ملبس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حجت بی‌مثلی خدای همین بس</p></div>
<div class="m2"><p>کانچه تصور کنی ز خلق جهان کس</p></div></div>
<div class="b2" id="bn12"><p>نیست که تا خوانیش مثال محمد</p></div>
<div class="b" id="bn13"><div class="m1"><p>منکر او بر کمال اگر چه محالست</p></div>
<div class="m2"><p>ور که بود منکر وجود کمالست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ره سپر راه جهل و بغی و ضلالست</p></div>
<div class="m2"><p>گمشده دشت و هم و خواب و خیالست</p></div></div>
<div class="b2" id="bn15"><p>هر که شود منکر کمال محمد</p></div>
<div class="b" id="bn16"><div class="m1"><p>پس علی آنکو به کردگار ولی بود</p></div>
<div class="m2"><p>جای نشینش بمسند ازلی بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حامل اسرارش از خفی و جلی بود</p></div>
<div class="m2"><p>قول محمد همان مقال علی بود</p></div></div>
<div class="b2" id="bn18"><p>قول علی بد همان مقال محمد</p></div>
<div class="b" id="bn19"><div class="m1"><p>فاطمه آن زهره سپهر کرامت</p></div>
<div class="m2"><p>فاطمه آن در جزا شفیعه امت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>واسطه بین نبوت است و امامت</p></div>
<div class="m2"><p>حضرت او داده تا بروز قیامت</p></div></div>
<div class="b2" id="bn21"><p>با علی و آل اتصال محمد</p></div>
<div class="b" id="bn22"><div class="m1"><p>پس حسن آن بحر حلم و کشتی احسان</p></div>
<div class="m2"><p>رهبر جن و ملک حقیقت انسان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بی‌مددش مشکلی نمی‌شود آسان</p></div>
<div class="m2"><p>سبط محمد شهی که آمده یکسان</p></div></div>
<div class="b2" id="bn24"><p>خصلت او جمله با خصال محمد</p></div>
<div class="b" id="bn25"><div class="m1"><p>بعد حسن بر حسین بین و مقامش</p></div>
<div class="m2"><p>کوست نگهبان شرع جد گرامش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طرفه طلسمی است اسم اعظم نامش</p></div>
<div class="m2"><p>کامده اندر پناه یمن و دوامش</p></div></div>
<div class="b2" id="bn27"><p>تا به ابد شرع بی‌زوال محمد</p></div>
<div class="b" id="bn28"><div class="m1"><p>بعد حسین کرد آفتاب ولایت</p></div>
<div class="m2"><p>سیز بنه برج و تافت بهر هدایت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا ده و دو برج را رسید نهایت</p></div>
<div class="m2"><p>بسته و بگشوده‌اند خوش به حمایت</p></div></div>
<div class="b2" id="bn30"><p>سد حرام و ره حلال محمد</p></div>
<div class="b" id="bn31"><div class="m1"><p>وان علی و باقر است و جعفر و کاظم</p></div>
<div class="m2"><p>باز رضا و تقی دو بحر مکارم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پس تقی و عسگری دو فخر عوالم</p></div>
<div class="m2"><p>خاتم ایشان که هست حجت قائم</p></div></div>
<div class="b2" id="bn33"><p>اوست فرج از برای آل‌محمد</p></div>
<div class="b" id="bn34"><div class="m1"><p>آنکه ز فرط جلال و مرتبت و جاه</p></div>
<div class="m2"><p>از الف قامت است مطهر الله</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خواهی اگر نام او در آی بدین راه</p></div>
<div class="m2"><p>میم محمد بگیر و ساز پس آنگاه</p></div></div>
<div class="b2" id="bn36"><p>وصل بر آن حاء و میم و دال محمد</p></div>
<div class="b" id="bn37"><div class="m1"><p>ملک خدا را یگانه مالک مطلق</p></div>
<div class="m2"><p>ملک باو قائم اوست قائم بالحق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون بحرم برزند فراشته بیرق</p></div>
<div class="m2"><p>هر روشی را فتد شکست برونق</p></div></div>
<div class="b2" id="bn39"><p>جز روش شرع خوش مآل محمد</p></div>
<div class="b" id="bn40"><div class="m1"><p>من که صغیر و غریق بحر گناهم</p></div>
<div class="m2"><p>رشگ برد آسمان به رفعت و جاهم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کاین ده و چارند در دو کون پناهم</p></div>
<div class="m2"><p>حال تباهم مبین و روی سیاهم</p></div></div>
<div class="b2" id="bn42"><p>بین که سگی هستم از بلال محمد</p></div>