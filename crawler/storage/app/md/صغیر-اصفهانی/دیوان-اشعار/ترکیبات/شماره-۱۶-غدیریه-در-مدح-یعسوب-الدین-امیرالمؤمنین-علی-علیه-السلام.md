---
title: >-
    شمارهٔ ۱۶ - غدیریه در مدح یعسوب الدین امیرالمؤمنین علی علیه‌السلام
---
# شمارهٔ ۱۶ - غدیریه در مدح یعسوب الدین امیرالمؤمنین علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>ز ریا و کبر بگذر جلوات کبریا بین</p></div>
<div class="m2"><p>بمقام سعی دلرا همه روضه الصفا بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخم غدیر امروز تجلی خدا بین</p></div>
<div class="m2"><p>بملالقای حق را به لقای مرتضی بین</p></div></div>
<div class="b2" id="bn3"><p>که خدای جلوه‌گر شد به لباس مرتضائی</p></div>
<div class="b" id="bn4"><div class="m1"><p>اگرش خدای خوانم بیقین رضا نباشد</p></div>
<div class="m2"><p>و گرش جدای دانم بخدا روا نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر او خدا نباشد ز خدا جدا نباشد</p></div>
<div class="m2"><p>بود این عقیده من که گر او خدا نباشد</p></div></div>
<div class="b2" id="bn6"><p>بخدا قسم که داده است خدا باوخدائی</p></div>
<div class="b" id="bn7"><div class="m1"><p>اگرت خدای بخشد دل پاک و جان طاهر</p></div>
<div class="m2"><p>ببری باین سخن پی که چه اول و چه آخر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برسی بدین معما که بباطن و بظاهر</p></div>
<div class="m2"><p>چو ز چشم جان ببینی بحقیقت مظاهر</p></div></div>
<div class="b2" id="bn9"><p>علی است و بس که بر خود شده گرم خودنمائی</p></div>
<div class="b" id="bn10"><div class="m1"><p>مدد از علی طلب‌کن که بهربلا و هر غم</p></div>
<div class="m2"><p>متوسل جنابش دل آدم است و خاتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه سمائی مسبح چه زمینی مکرم</p></div>
<div class="m2"><p>بخدای هر دو گیتی ز کسی بهر دو عالم</p></div></div>
<div class="b2" id="bn12"><p>بجز از علی نیاید هنر گره‌گشائی</p></div>
<div class="b" id="bn13"><div class="m1"><p>با ما کن و نواحی بمساکن و مراحل</p></div>
<div class="m2"><p>بقبایل و عشایر بطوایف و سلاسل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه فیض اوست‌جاری همه لطف اوست شامل</p></div>
<div class="m2"><p>فلک خمیده بالانه اگر از اوست سائل</p></div></div>
<div class="b2" id="bn15"><p>بگرفته است بر کف ز چه کاسه گدائی</p></div>
<div class="b" id="bn16"><div class="m1"><p>بود او مؤلف و بس بکتابهای دیرین</p></div>
<div class="m2"><p>بود او مربی و بس بمربیان آئین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رشحات علم دانی به بشر شد از که تلقین</p></div>
<div class="m2"><p>بخدا قسم علی بود که ابتدای تکوین</p></div></div>
<div class="b2" id="bn18"><p>به ابوالبشر بیاموخت کتاب ابتدائی</p></div>
<div class="b" id="bn19"><div class="m1"><p>اگرت بهشت باید ره آن دهم نشانت</p></div>
<div class="m2"><p>بطریقه علی رو که رساند این بآنت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو ولای او نداری بسقر بود مکانت</p></div>
<div class="m2"><p>ز نسیم خلد بوئی نبرد مشام جانت</p></div></div>
<div class="b2" id="bn21"><p>همه عمر اگر بپوئی ره زهد و پارسائی</p></div>
<div class="b" id="bn22"><div class="m1"><p>پی سعد و نحس طالع چه منجمت سرآید</p></div>
<div class="m2"><p>شنوی چو نام حیدر غمت ار ز دل زداید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدو عالمت یقین حق در میمنت گشاید</p></div>
<div class="m2"><p>و گرت کدورت افزود به رنج و غم فزاید</p></div></div>
<div class="b2" id="bn24"><p>بوداین محک ترا بس پی بخت آزمائی</p></div>
<div class="b" id="bn25"><div class="m1"><p>علی ای ولی مطلق علی ای امام رهبر</p></div>
<div class="m2"><p>دگران و مال و منصب دگران و تخت و افسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من و خاک آستانت که بر آن نهاده‌ام سر</p></div>
<div class="m2"><p>بره غمت که رسته است بجای خارنشتر</p></div></div>
<div class="b2" id="bn27"><p>بتمام ملک عالم ندهم برهنه پائی</p></div>
<div class="b" id="bn28"><div class="m1"><p>علی ایکه جز به عشق تو نبوده‌ های و هویم</p></div>
<div class="m2"><p>علی ایکه جز بذکر تو نبوده گفتگویم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>می عشق تست تنها بصراحی و سبویم</p></div>
<div class="m2"><p>پی آب زندگانی ره ظلمت از چه پویم</p></div></div>
<div class="b2" id="bn30"><p>که رسیده‌ام بخاک در تو بروشنائی</p></div>
<div class="b" id="bn31"><div class="m1"><p>علی ایکه ذات پاکت زده کوس بیمثالی</p></div>
<div class="m2"><p>ملکوت را تو مالک جبروت را تو والی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بتو زیبد و بس اینهم که خدات خوانده غالی</p></div>
<div class="m2"><p>سر هر کسی نیز زد بکلاه ذوالجلالی</p></div></div>
<div class="b2" id="bn33"><p>تن هر کسی نزیبد بردای کبریائی</p></div>
<div class="b" id="bn34"><div class="m1"><p>علی ای که هست دلها همگی در آرزویت</p></div>
<div class="m2"><p>بدو مطلبست اینک نظر صغیر سویت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی اینکه خوانی او را ز ره کرم بکویت</p></div>
<div class="m2"><p>دگر اینکه بر در حق طلبد ز آبرویت</p></div></div>
<div class="b2" id="bn36"><p>که دهی ورابکلی تو ز غیر خود رهائی</p></div>