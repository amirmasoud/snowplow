---
title: >-
    شمارهٔ ۲۳ - تضمین غزل خواجه حافظ علیه‌الرحمه - بمناسبت عید مولود امیرالمؤمنین علی (ع)
---
# شمارهٔ ۲۳ - تضمین غزل خواجه حافظ علیه‌الرحمه - بمناسبت عید مولود امیرالمؤمنین علی (ع)

<div class="b" id="bn1"><div class="m1"><p>بخلق عالمی ای اهل عشق ناز کنید</p></div>
<div class="m2"><p>رسید یار ز ره ساز عیش ساز کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به طاق ابروی محرابیش نماز کنید</p></div>
<div class="m2"><p>معاشران گره از زلف یار باز کنید</p></div></div>
<div class="b2" id="bn3"><p>شبی خوش است بدین قصه‌اش دراز کنید</p></div>
<div class="b" id="bn4"><div class="m1"><p>در این مقام که یاران مهربان جمعند</p></div>
<div class="m2"><p>همه بعشق علی شاه انس و جان جمعند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جدائی افتدشان گر ز تن بجان جمعند</p></div>
<div class="m2"><p>حضور خلوت انس است و دوستان جمعند</p></div></div>
<div class="b2" id="bn6"><p>وان یکاد بخوانید و در فراز کنید</p></div>
<div class="b" id="bn7"><div class="m1"><p>قدح کشان که بعشرت مقیم این کویند</p></div>
<div class="m2"><p>اسیر طره آن یار عنبرین مویند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون پرده بر از و نیاز با اویند</p></div>
<div class="m2"><p>رباب و چنگ به بانگ بلند میگویند</p></div></div>
<div class="b2" id="bn9"><p>که گوش هوش به پیغام اهل راز کنید</p></div>
<div class="b" id="bn10"><div class="m1"><p>غلام دولت آنم که اوست بنده بعشق</p></div>
<div class="m2"><p>دل از مصاحب غیردوست کنده بعشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع سوزد و در گریه‌است و خنده بعشق</p></div>
<div class="m2"><p>هر آنکسی که در این حلقه نیست زنده بعشق</p></div></div>
<div class="b2" id="bn12"><p>بر او چو مرده به فتوای من نماز کنید</p></div>
<div class="b" id="bn13"><div class="m1"><p>خبر شوید که در عشق کار دشوار است</p></div>
<div class="m2"><p>عتاب و ناز و تکبر همیشه از یار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیاز و عجز و تضرع ز عاشق زار است</p></div>
<div class="m2"><p>میان عاشق و معشوق فرق بسیار است</p></div></div>
<div class="b2" id="bn15"><p>چو یار ناز نماید شما نیاز کنید</p></div>
<div class="b" id="bn16"><div class="m1"><p>همای اوج سعادت به هر هوا نپرد</p></div>
<div class="m2"><p>فریب کس نخورد شید هر دغا نخرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>غم نهفته به جز بر در خدا نبرد</p></div>
<div class="m2"><p>به جان دوست که غم پرده شما ندرد</p></div></div>
<div class="b2" id="bn18"><p>گر اعتماد به الطاف کارساز کنید</p></div>
<div class="b" id="bn19"><div class="m1"><p>بزرگ مایه ایمان ثبات و تمکین است</p></div>
<div class="m2"><p>مکن مصاحبت آن را که اهل تلوین است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که آن مخرب اخلاق و رهزن دین است</p></div>
<div class="m2"><p>نخست موعظه پیر می‌فروش این است</p></div></div>
<div class="b2" id="bn21"><p>که از مصاحب نا جنس احتراز کنید</p></div>
<div class="b" id="bn22"><div class="m1"><p>مگر خبر شده زین جشن جان فزا حافظ</p></div>
<div class="m2"><p>که نظم خویش فرستاده با صبا حافظ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صغیر یافت که دارد چه مدعا حافظ</p></div>
<div class="m2"><p>اگر طلب کند انعامی از شما حافظ</p></div></div>
<div class="b2" id="bn24"><p>حوالتش به لب یار دلنواز کنید</p></div>