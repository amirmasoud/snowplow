---
title: >-
    شمارهٔ ۵ - مخمس در مدح خواجه قنبر کننده در خیبر علی علیه‌السلام
---
# شمارهٔ ۵ - مخمس در مدح خواجه قنبر کننده در خیبر علی علیه‌السلام

<div class="b" id="bn1"><div class="m1"><p>امشب به هر کجا گذری وادایمن است</p></div>
<div class="m2"><p>روشن جهان ز پرتو انوار ذوالمن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم منور آمده گیتی مزین است</p></div>
<div class="m2"><p>امشب براستی شب ما روز روشن است</p></div></div>
<div class="b2" id="bn3"><p>عید وصال دوست علیرغم دشمن است</p></div>
<div class="b" id="bn4"><div class="m1"><p>هر گوشه مجلسی است ز رندان باده نوش</p></div>
<div class="m2"><p>می‌درخم و صراحی و ساغر بود بجوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تار است در ترانه و چنگ است در خروش</p></div>
<div class="m2"><p>دلداده رهن ناله نی کرده عقل و هوش</p></div></div>
<div class="b2" id="bn6"><p>می‌خواره با صراحی می‌دست و گردنست</p></div>
<div class="b" id="bn7"><div class="m1"><p>افراسیاب چرخ ز بس ریخت طرح جنگ</p></div>
<div class="m2"><p>گرسیوز غمم به نفس بسته راه تنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رشگ منیژه ترک من ای فتنهٔ فرنگ</p></div>
<div class="m2"><p>با بادهٔی چو خون سیاوش لاله رنگ</p></div></div>
<div class="b2" id="bn9"><p>باز آ که دل بچاه ملالت چو بیژن است</p></div>
<div class="b" id="bn10"><div class="m1"><p>چون با زمانه‌ام نبود قدرت ستیز</p></div>
<div class="m2"><p>بایست جستنم سوی مستی ره گریز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فصلی چنین بویژه که ابر است ژاله ریز</p></div>
<div class="m2"><p>وز ابر ژاله ریز گل اندوز و لاله خیز</p></div></div>
<div class="b2" id="bn12"><p>دامان کوه و طرف دمن صحن گلشن است</p></div>
<div class="b" id="bn13"><div class="m1"><p>در باغ رو طراوت فصل بهار بین</p></div>
<div class="m2"><p>آثار صنع حضرت پروردگار بین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سرو سهی بجلوه لب جویبار بین</p></div>
<div class="m2"><p>خندان دهان غنچه بدامان خار بین</p></div></div>
<div class="b2" id="bn15"><p>چون مادری که طفل رضیعش بدامن است</p></div>
<div class="b" id="bn16"><div class="m1"><p>ساقی در این خجسته بهار فرح فزا</p></div>
<div class="m2"><p>زن آب می‌بر آتش اندوه جان گزا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روزی چنین بویژه مبارک که از قضا</p></div>
<div class="m2"><p>عید محمد آمده میلاد مرتضی</p></div></div>
<div class="b2" id="bn18"><p>و ز این دو عید دیده و دل هر دو روشن است</p></div>
<div class="b" id="bn19"><div class="m1"><p>میلاد سروری است کز و جمله راست بهر</p></div>
<div class="m2"><p>نامش بدوست شهد چشاند بخصم زهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی‌مثل و بی‌نظیر خداوند لطف و قهر</p></div>
<div class="m2"><p>ز آوردن چو او پدر چرخ و مام دهر</p></div></div>
<div class="b2" id="bn21"><p>عنین بمانده این یک و آن یک سترونست</p></div>
<div class="b" id="bn22"><div class="m1"><p>تنها همین ز کعبه مبین طلعت علی</p></div>
<div class="m2"><p>مرآت دل نمای مصفا و صیقلی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز دیده دورساز دو بینی و احولی</p></div>
<div class="m2"><p>در کاینات جلوه او را ببین ولی</p></div></div>
<div class="b2" id="bn24"><p>آنسان که نور در بصر و روح در تن است</p></div>
<div class="b" id="bn25"><div class="m1"><p>در وقت نزع و گاه سئوال وصف شمار</p></div>
<div class="m2"><p>تنها ولای اوست که آید ترا بکار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل جای مهر اوست بهر سفله کم‌سپار</p></div>
<div class="m2"><p>و آنکو جز این بدوش دل خود نهاده بار</p></div></div>
<div class="b2" id="bn27"><p>بر کار او بخند که حمال گلخن است</p></div>
<div class="b" id="bn28"><div class="m1"><p>هنگام رزم قاتل کفار مرتضی</p></div>
<div class="m2"><p>دانی به آسمان چه مثل دارد اقتضا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر دست بنده‌اش که بود نام او قضا</p></div>
<div class="m2"><p>گردون فلاخنی استکه گردد در این قضا</p></div></div>
<div class="b2" id="bn30"><p>وان کوی آفتاب چو سنگ فلاخن است</p></div>
<div class="b" id="bn31"><div class="m1"><p>فرمان بزالی ار دهد آن میر ارجمند</p></div>
<div class="m2"><p>کاندر جدال خصم دغا را کشد به بند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از تار گیسوان خود او آورد کمند</p></div>
<div class="m2"><p>بی‌آنکه ذرهٔی رسد آنزال را گزند</p></div></div>
<div class="b2" id="bn33"><p>بندد دو دست خصم و گر خود تهمتن است</p></div>
<div class="b" id="bn34"><div class="m1"><p>یا قاهر العدو و یا والی الولی</p></div>
<div class="m2"><p>یا مظهر العجائب یا مرتضی علی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زیبد صغیر عبد کمینت ز پر دلی</p></div>
<div class="m2"><p>خصم ار فلک بود نکند بیم از آن بلی</p></div></div>
<div class="b2" id="bn36"><p>آن کو غلام تست چه با کش ز دشمن است</p></div>