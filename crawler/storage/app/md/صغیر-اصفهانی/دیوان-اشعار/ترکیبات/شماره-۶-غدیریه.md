---
title: >-
    شمارهٔ ۶ - غدیریه
---
# شمارهٔ ۶ - غدیریه

<div class="b" id="bn1"><div class="m1"><p>امروز روز نصب وصی پیمبر است</p></div>
<div class="m2"><p>اندر خم غدیر یکی طرفه محضر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از چشم دل ببین که نبی فوق منبر است</p></div>
<div class="m2"><p>روحش قرین وجد ز پیغام داور است</p></div></div>
<div class="b2" id="bn3"><p>پیغام آشنا سخن روح پرور است</p></div>
<div class="b" id="bn4"><div class="m1"><p>ارواح انبیا همه را با نیاز بین</p></div>
<div class="m2"><p>جن و ملک گرفته نشیب و فراز بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی زهند و روم و عراق و حجاز بین</p></div>
<div class="m2"><p>چشم همه به احمد محمود باز بین</p></div></div>
<div class="b2" id="bn6"><p>یا للعجب حکایت صحرای محشر است</p></div>
<div class="b" id="bn7"><div class="m1"><p>به به چه محضریست که آنرا نظیر نیست</p></div>
<div class="m2"><p>عنوان صدر و ذیل و غنی و فقیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناطق بجز رسول نذیر بشیر نیست</p></div>
<div class="m2"><p>گوید که جز علی بخلایق امیر نیست</p></div></div>
<div class="b2" id="bn9"><p>وین نیست قول من که ز خلاق اکبر است</p></div>
<div class="b" id="bn10"><div class="m1"><p>انوار لمعه لمعه برآید در آن مکان</p></div>
<div class="m2"><p>از منبر جحاز شتر تا به آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر گشته از شکوه بنی‌هاشمی جهان</p></div>
<div class="m2"><p>جبریل راست آیه اکملت ارمغان</p></div></div>
<div class="b2" id="bn12"><p>یعنی کمال دین به تولای حیدر است</p></div>
<div class="b" id="bn13"><div class="m1"><p>افکنده این قضیه بر اجسام ارتعاش</p></div>
<div class="m2"><p>بر دوست جان فزا شده از خصم دلخراش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حافظ ز دور ناظر و گوید ز صدق فاش</p></div>
<div class="m2"><p>گو زاهد زمانه و گو شیخ راه باش</p></div></div>
<div class="b2" id="bn15"><p>آنرا که دوستی علی نیست کافر است</p></div>
<div class="b" id="bn16"><div class="m1"><p>نور ولایت اسدالله ظهور یافت</p></div>
<div class="m2"><p>زین نور دهر بهجت و گیتی سرور یافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ارض و سما تجمل الله نور یافت</p></div>
<div class="m2"><p>شاهد ز غیب آمد و جانان حضور یافت</p></div></div>
<div class="b2" id="bn18"><p>صاحب دلان زمان ملاقات دلبر است</p></div>
<div class="b" id="bn19"><div class="m1"><p>یک دور بود بادهٔ عرفان کبریا</p></div>
<div class="m2"><p>در عهده سقایت افراد انبیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن دور منتهی شد و امروز مصطفی</p></div>
<div class="m2"><p>تفویض کرد امر سقایت بمرتضی</p></div></div>
<div class="b2" id="bn21"><p>زین بعد جام در کف ساقی کوثر است</p></div>
<div class="b" id="bn22"><div class="m1"><p>روزی چنین نکو که بیمن قدوم وی</p></div>
<div class="m2"><p>آمد زمان وصل و شد ایام هجر طی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با بانگ چنگ و ناله تار و نوای نی</p></div>
<div class="m2"><p>ساقی بعشق بوالحسنم بخش جام می</p></div></div>
<div class="b2" id="bn24"><p>کاین می‌مرا حلالتر از شیر مادر است</p></div>
<div class="b" id="bn25"><div class="m1"><p>رندان دهند از ره انصاف پروری</p></div>
<div class="m2"><p>ترجیح بندگی علی را بسروری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آری کند بچرخ گر از رتبه همسری</p></div>
<div class="m2"><p>یک ذره‌اش بخاک زمین نیست برتری</p></div></div>
<div class="b2" id="bn27"><p>هر سر که آن نه خاک کف پای قنبر است</p></div>
<div class="b" id="bn28"><div class="m1"><p>رسم است در میان دلیران پهلوان</p></div>
<div class="m2"><p>کارند وصف خود گه پیکار بر زبان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شیر خدای هم بمصاف دلاوران</p></div>
<div class="m2"><p>میکرد وصف خویش بگاه رجز بیان</p></div></div>
<div class="b2" id="bn30"><p>آنوصف چیست نعرهٔ الله اکبر است</p></div>
<div class="b" id="bn31"><div class="m1"><p>حکم قضا رود همه بر حکمت علی</p></div>
<div class="m2"><p>هستی ز کل و جزو بود حشمت علی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود صغیر نیست جز از رحمت علی</p></div>
<div class="m2"><p>وین نطق جانفزاش بود نعمت علی</p></div></div>
<div class="b2" id="bn33"><p>کی نعمتی چنین همه‌کس را میسر است</p></div>