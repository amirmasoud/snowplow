---
title: >-
    شمارهٔ ۲۸ - تضمین غزل خواجه حافظ علیه‌الرحمه
---
# شمارهٔ ۲۸ - تضمین غزل خواجه حافظ علیه‌الرحمه

<div class="b" id="bn1"><div class="m1"><p>نی در خیال مال و نه در فکر جاه باش</p></div>
<div class="m2"><p>نی در پی تدارک تخت و کلاه باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو بنده علی شه ایمان پناه باش</p></div>
<div class="m2"><p>ایدل غلام شاه جهان باش و شاه باش</p></div></div>
<div class="b2" id="bn3"><p>پیوسته در حمایت لطف اله باش</p></div>
<div class="b" id="bn4"><div class="m1"><p>یوم‌الجزا که هیچ ندارم ره گریز</p></div>
<div class="m2"><p>چشمم بمصطفی است در آن روز هول خیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد چه غم گرم دهد از هالکین تمیز</p></div>
<div class="m2"><p>چون احمدم شفیع بود روز رستخیز</p></div></div>
<div class="b2" id="bn6"><p>گو این تن بلاکش من پرگناه باش</p></div>
<div class="b" id="bn7"><div class="m1"><p>در حشر غیر پرده ناکس نمی‌درند</p></div>
<div class="m2"><p>وان دوزخی طیور به طوبی نمی‌برند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز شیعه علی سوی جنت نمی‌برند</p></div>
<div class="m2"><p>از خارجی هزار به یک جو نمیخرند</p></div></div>
<div class="b2" id="bn9"><p>گو کوه تا بکوه منافق سپاه باش</p></div>
<div class="b" id="bn10"><div class="m1"><p>مهر علی و آل به هر دل که اندر است</p></div>
<div class="m2"><p>سرمایه نجات وی از هول محشر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ایمان بدون ریب تولای حیدر است</p></div>
<div class="m2"><p>آن را که دوستی علی نیست کافر است</p></div></div>
<div class="b2" id="bn12"><p>گو زاهد زمانه و گو شیخ راه باش</p></div>
<div class="b" id="bn13"><div class="m1"><p>من بنده توام به خدای تو یا علی</p></div>
<div class="m2"><p>دل بسته‌ام به مهر و وفای تو یا علی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جان می‌دهم به شوق لقای تو یا علی</p></div>
<div class="m2"><p>امروز زنده‌ام به ولای تو یا علی</p></div></div>
<div class="b2" id="bn15"><p>فردا بروح پاک امامان گواه باش</p></div>
<div class="b" id="bn16"><div class="m1"><p>عرش آستان خدیو زمان و زمین رضا</p></div>
<div class="m2"><p>صاحب حریم کعبه اهل یقین رصا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای دل برو به کوی امام مبین رضا</p></div>
<div class="m2"><p>قبر امام هشتم و سلطان دین رضا</p></div></div>
<div class="b2" id="bn18"><p>از جان ببوس و بر در آن بارگاه باش</p></div>
<div class="b" id="bn19"><div class="m1"><p>این گلستان ک بهره از آن می‌برد کلاخ</p></div>
<div class="m2"><p>تو بلبلی برای چه گردی به سنگلاخ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن درا بدست فرا دامن فراخ</p></div>
<div class="m2"><p>دستت نمی‌رسد که بچینی گلی ز شاخ</p></div></div>
<div class="b2" id="bn21"><p>باری به پای گلبن ایشان گیاه باش</p></div>
<div class="b" id="bn22"><div class="m1"><p>در بندگی نه شرط حسب نی نسب بود</p></div>
<div class="m2"><p>نی فرق در میان عجم با عرب بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نی جامه سفید جنان را سبب بود</p></div>
<div class="m2"><p>مرد خداشناس که تقوی طلب بود</p></div></div>
<div class="b2" id="bn24"><p>خواهی سفید جامه و خواهی سیاه‌باش</p></div>
<div class="b" id="bn25"><div class="m1"><p>صدق و صفا صغیر شعار همیشه کن</p></div>
<div class="m2"><p>انصاف را تو در کف تحقیق تیشه کن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قطع نهال خویش پرستی ز ریشه کن</p></div>
<div class="m2"><p>حافظ طریق بندگی شاه پیشه کن</p></div></div>
<div class="b2" id="bn27"><p>وانگاه در طریق چو مردان راه باش</p></div>