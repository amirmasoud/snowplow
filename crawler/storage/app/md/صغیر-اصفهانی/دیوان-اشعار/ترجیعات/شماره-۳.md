---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>در ازل علم و قدرت یزدان</p></div>
<div class="m2"><p>ریخت خوش طرح عالم امکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علم چون هست و قدرت اندر کار</p></div>
<div class="m2"><p>کار انجام یابد و سامان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هنری نیست اندر آن بی این</p></div>
<div class="m2"><p>اثری نیست اندر این بی‌آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین دو یابند تا ابد هستی</p></div>
<div class="m2"><p>جزو جزو وجود خرد و کلان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست این نکته قابل انکار</p></div>
<div class="m2"><p>نیست حاجت به حجت و برهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که شد ایجاد خلق زین دو صفت</p></div>
<div class="m2"><p>از خداوند قادر منان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صفتش راست مظهری لازم</p></div>
<div class="m2"><p>کز نهان آرد آن صفت بعیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یعنی آید پدید از آن مظهر</p></div>
<div class="m2"><p>در مشیت هر آنچه هست نهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست روی سخن در این موضوع</p></div>
<div class="m2"><p>سوی اهل دقایق و عرفان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوش جان برگشا که در این بیت</p></div>
<div class="m2"><p>مطلب خویش می‌کنم عنوان</p></div></div>
<div class="b2" id="bn11"><p>مظهر علم و قدرت ازلی</p>
<p>ذات پاک محمد است و علی</p></div>
<div class="b" id="bn12"><div class="m1"><p>دین خود را چو خالق اکبر</p></div>
<div class="m2"><p>خواست القا کند به نوع بشر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منصب خاتمیت اعطا کرد</p></div>
<div class="m2"><p>به حبیبش جناب پیغمبر (ص)</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علم خود را ظهور داد از وی</p></div>
<div class="m2"><p>به صلاح جهانیان یکسر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>او در انجام امر تنها بود</p></div>
<div class="m2"><p>قدرت خود نمود از حیدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز پی نفی کفر بر دستش</p></div>
<div class="m2"><p>شکل لا داد ذوالفقار دو سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با همان قدرت الهی کند</p></div>
<div class="m2"><p>مرتضی در ز قلعه خیبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه به دستش فتاد عمر و از پای</p></div>
<div class="m2"><p>گه ز مرحب دو پاره شد پیکر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لقبش مظهر العجائب شد</p></div>
<div class="m2"><p>ز آنهمه قدرت شگفت‌آور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز نبی کس نبود فرمانده</p></div>
<div class="m2"><p>جز علی کس نبود فرمان بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>این دو با اتفاق هم دادند</p></div>
<div class="m2"><p>دین حق را رواج تا محشر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر شدت ثابت این بیان ای دوست</p></div>
<div class="m2"><p>بکن این بیت را بجان از بر</p></div></div>
<div class="b2" id="bn23"><p>مظهر علم و قدرت ازلی</p>
<p>ذات پاک محمد است و علی</p></div>
<div class="b" id="bn24"><div class="m1"><p>کار پرداز عالمی بامور</p></div>
<div class="m2"><p>عقل و عشقند تا بیوم نشور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فی‌المثل عقل گوید انسان را</p></div>
<div class="m2"><p>سروسامان برای تست ضرور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عشق از کوشش و عمل سازد</p></div>
<div class="m2"><p>همه آماده در خور دستور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گویدت عقل از برای بشر</p></div>
<div class="m2"><p>هست لازم بنای کاخ و قصور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عشق آید به پیش و از بهرت</p></div>
<div class="m2"><p>خانه آباد سازد و معمور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در بشر عقل و عشق جزئی بین</p></div>
<div class="m2"><p>کارپرداز در تمام امور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همچنین عقل و عشق کلی دان</p></div>
<div class="m2"><p>عالم کون را سبب بظهور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>علم آثار عقل و قدرت را</p></div>
<div class="m2"><p>اثر عشق دان و جذبه و شور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عقل کل احمد است آنکه بود</p></div>
<div class="m2"><p>گنج علم خدای را گنجور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عشق کل مرتضی است آنکه از او</p></div>
<div class="m2"><p>شد عیان قدرت خدای غفور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اهل دل را کند صغیر اکنون</p></div>
<div class="m2"><p>متذکر به مطلب مذکور</p></div></div>
<div class="b2" id="bn35"><p>مظهر علم و قدرت ازلی</p>
<p>ذات پاک محمد است و علی</p></div>
<div class="b" id="bn36"><div class="m1"><p>قلم بلوح نوشت این سخن بخط جلی</p></div>
<div class="m2"><p>نبی مدینه علم و در مدینه علی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در این مدینه از این درد را که در دو جهان</p></div>
<div class="m2"><p>رسی بحصن امان خدای لم یزلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو را حقیقت عرفان حق همین باشد</p></div>
<div class="m2"><p>که ره بری بشناسائی نبی و ولی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بجز ولای علی هیچ نیست راه نجات</p></div>
<div class="m2"><p>چنین شده است مقدر ز قادر ازلی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مددچو خواهی از آن مظهر العجائب خواه</p></div>
<div class="m2"><p>که از حق است با حمد خطاب نادعلی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خدای مثل ندارد ولی علی باشد</p></div>
<div class="m2"><p>خدای را مثل اندر مقام بی‌مثلی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بریخت خون معاند بذوالفقار دو سر</p></div>
<div class="m2"><p>نمود فتح معارک ببازوان یلی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز عمر و زید بجز عمروعاص از رزمش</p></div>
<div class="m2"><p>نبرد جان بدر آنهم ز فرط بوالحیلی</p></div></div>
<div class="b2" id="bn44"><p>شها بوصف تو الکن بود لسان صغیر</p>
<p>بدست هیچ ندارد بغیر منفعلی</p></div>