---
title: >-
    شمارهٔ ۲۵ - کیفیت صلحیهٔ اصفهان
---
# شمارهٔ ۲۵ - کیفیت صلحیهٔ اصفهان

<div class="b" id="bn1"><div class="m1"><p>گوش فرا دار که سازم بیان</p></div>
<div class="m2"><p>کیفیت صلحیهٔ اصفهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودم از این اسم بسی در شگفت</p></div>
<div class="m2"><p>کش به چه منظور توانم گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصلح این عالم اضداد کیست</p></div>
<div class="m2"><p>معنی این اسم بلا رسم چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهر پر از شرک و نفاق و جفاست</p></div>
<div class="m2"><p>صلحیه یعنی چه و صلح از کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلحیه بیرون بود از چار طبع</p></div>
<div class="m2"><p>نیست در این جامعهٔ مار طبع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صلحیه چندان نبود دست رس</p></div>
<div class="m2"><p>صلحیه در محضر حق است و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الغرض این سر شودم تا پدید</p></div>
<div class="m2"><p>کار به صلحیهٔ شهرم کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پی جزئی‌طلبی عرض حال</p></div>
<div class="m2"><p>دادم و عاید نشدم جز ملال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس به ره صلحیه پویان شدم</p></div>
<div class="m2"><p>جان تو از کرده پشیمان شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد ثبوت و سند معتبر</p></div>
<div class="m2"><p>از طلب خویش به بستم نظر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد ضرر فرع چو از اصل بیش</p></div>
<div class="m2"><p>صلح نمودم به طرف حق خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رو ز در صلحیه بر تافتم</p></div>
<div class="m2"><p>معنی صلحیه همین یافتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کانکه بدین‌جا سر وکارش فتاد</p></div>
<div class="m2"><p>حق و طلب بایدش از دست داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرچه طلبکار بود آن طلب</p></div>
<div class="m2"><p>صلح کند تا برهد از تعب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زین سببش صلحیه کردند نام</p></div>
<div class="m2"><p>جان عمو قصه ما شد تمام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر که یجوز است و گر لایجور</p></div>
<div class="m2"><p>بر نخورد بنده صغیرم هنوز</p></div></div>