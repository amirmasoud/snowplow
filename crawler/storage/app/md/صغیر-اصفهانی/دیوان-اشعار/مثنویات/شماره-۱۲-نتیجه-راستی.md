---
title: >-
    شمارهٔ ۱۲ - نتیجه راستی
---
# شمارهٔ ۱۲ - نتیجه راستی

<div class="b" id="bn1"><div class="m1"><p>قافله ئی بد برهی رهسپار</p></div>
<div class="m2"><p>شد بکف رهزن چندی دچار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکتن از آن قافله ز اندازه بیش</p></div>
<div class="m2"><p>گشت هراسان و فکار و پریش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت بوی دیگری اینحال چیست</p></div>
<div class="m2"><p>یا مگرت‌ امتعه و مال چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت مرا زر بود اندر ببار</p></div>
<div class="m2"><p>با گهر و لعل و در شاهوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت گمان کن ز کفت راهزن</p></div>
<div class="m2"><p>آن بر بوده است عطا کن بمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مگر آنرا بسلامت برم</p></div>
<div class="m2"><p>باز بدستت ز وفا بسپرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داد ز استر بکف وی عنان</p></div>
<div class="m2"><p>او بملا رفت سوی رهزنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بانک زدندش که چه داری ببار</p></div>
<div class="m2"><p>گفت زر و لعل و گهر بیشمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باورشان نامد از آن راست کیش</p></div>
<div class="m2"><p>از چه سبب از کجی طبع خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دست نکردند به سویش دراز</p></div>
<div class="m2"><p>او بشعف میشد و میگفت باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رحمت حق شامل آن طبع راست</p></div>
<div class="m2"><p>کاین سخن راست از آنطبع خاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>«راستی آور که شوی رستگار»</p></div>
<div class="m2"><p>«راستی از تو ظفر از کردگار»</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>راستی ار هم تو ظفر خواستی</p></div>
<div class="m2"><p>همچو صغیر آن طلب از راستی</p></div></div>