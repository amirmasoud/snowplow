---
title: >-
    شمارهٔ ۲۷ - شلتاق پلتاق
---
# شمارهٔ ۲۷ - شلتاق پلتاق

<div class="b" id="bn1"><div class="m1"><p>صیحگهی پر فن حیلت‌گری</p></div>
<div class="m2"><p>از همه در مکر و حیل برتری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چابک و طرار و ره‌ آموخته</p></div>
<div class="m2"><p>حیله و تزویر و فن اندوخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برد سوی دکهٔ صراف رخت</p></div>
<div class="m2"><p>گفت بآن بیدل برگشته بخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیر حسابم من و زود اشتباه</p></div>
<div class="m2"><p>مانده خر فکرت من نیم راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشف کن این مسئله‌ای هوشمند</p></div>
<div class="m2"><p>گوی که تومان چهل و شصت چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده زد آنمرد و بگفت از غرور</p></div>
<div class="m2"><p>تا بچه حد ابلهی و بی‌شعور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شصت و چهل صد شود این هست فاش</p></div>
<div class="m2"><p>گفت تو هم ابلهی آهسته باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازبزن جمع و ببین چون شود</p></div>
<div class="m2"><p>شصت و چهل ده ز صد افزون شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حوصله کردید بصراف تنگ</p></div>
<div class="m2"><p>بانگ برآورد و همی کرد جنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق بوی جمع شدند از دو سو</p></div>
<div class="m2"><p>جمله شنیدند مر آن گفت‌وگو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرد حیل پیشه بآواز نرم</p></div>
<div class="m2"><p>گفت مرا آید از این خلق شرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من صد و ده از تو طلب داشتم</p></div>
<div class="m2"><p>ده بتو بخشیده صد انگاشتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر صد و ده می‌ندهی صد بده</p></div>
<div class="m2"><p>آنچه که اقرار کنی خود بده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواست بانکار سراید سخن</p></div>
<div class="m2"><p>مشت زدندش همگی بر دهن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاین چه لجاج است تو در نزد ما</p></div>
<div class="m2"><p>داشتی اقرار بصد کن ادا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داد بناچار صدش آن گریخت</p></div>
<div class="m2"><p>خاک الم بر سر صراف بیخت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مدتی از زاویهٔ انزوا</p></div>
<div class="m2"><p>پا ننهادی بدر آن بینوا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هیچ نمیگفت مبادا که باز</p></div>
<div class="m2"><p>در رسد آن حیله‌گر حبله‌باز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از پس سالی بدکان کرد روی</p></div>
<div class="m2"><p>تا مگر آن دکه کند رفت و روی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کامدش از گرد ره آن اوستاد</p></div>
<div class="m2"><p>کرد سلامی و برش ایستاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت کجا‌ آمده‌ئی ای رفیق</p></div>
<div class="m2"><p>گفت بدیدار تو یار شفیق</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گفت چه باشد که ز روی صفا</p></div>
<div class="m2"><p>شرح دهی بهر من این ماجرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کان چه حبل بود و چه مکروفسون</p></div>
<div class="m2"><p>یا که بدان مکر شدت رهنمون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت مرا کرده وصیت پدر</p></div>
<div class="m2"><p>گفته ز سرمایه مخور ای پسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرچه خوری از ره شلتاق خور</p></div>
<div class="m2"><p>چون ندهد دست ز پلتاق خور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود همان واقعه شلتاق من</p></div>
<div class="m2"><p>حال بود نوبت پلتاق من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غمزده صراف به عجز اوفتاد</p></div>
<div class="m2"><p>درهم چندیش به کف بر نهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دکه فرو بست و بگفت ار که باز</p></div>
<div class="m2"><p>دیدیم اینجا کنمت صد نیاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای که تحیر بری از این مقال</p></div>
<div class="m2"><p>تا نگری دیدهٔ خود را بمال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این نه از آن یکتن تنهاستی</p></div>
<div class="m2"><p>بلکه کنون در خور تنهاستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ما همه شلتاق بود کارمان</p></div>
<div class="m2"><p>نیست جز این پیشه و کردارمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جامعه فاسد ز خیانت شده</p></div>
<div class="m2"><p>مسخره عنوان دیانت شده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خانهٔ ما کرده خیانت خراب</p></div>
<div class="m2"><p>نیست جز این باعث این انقلاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا که چنین است چنین است حال</p></div>
<div class="m2"><p>به شدن حالت ما دادن محال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هست صغیر این روش و این مرام</p></div>
<div class="m2"><p>علت بدبختی ما والسلام</p></div></div>