---
title: >-
    شمارهٔ ۱۹ - حاسد و محسود
---
# شمارهٔ ۱۹ - حاسد و محسود

<div class="b" id="bn1"><div class="m1"><p>بود دو تن را دو درخت کهن</p></div>
<div class="m2"><p>حاسد و محسود بدند آن دو تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواست شبی حاسد شوریده بخت</p></div>
<div class="m2"><p>قطع ز محسود نماید درخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جست یکی اره کش تیشه زن</p></div>
<div class="m2"><p>گفت درختی است فلانجا زمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زود برو قطع کن از ریشه‌اش</p></div>
<div class="m2"><p>ریشه بر آور ز دم تیشه‌اش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن شجر پیر فکن ای جوان</p></div>
<div class="m2"><p>باز بیا اُجرت زحمت ستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد نویدش بسی و ره نمود</p></div>
<div class="m2"><p>سوی درختی که ز محسود بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او بشب تیره روان شد براه</p></div>
<div class="m2"><p>راه غلط کرد و برفت اشتباه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقترن‌ آمد به درخت حسود</p></div>
<div class="m2"><p>قافیه را باخت که این آن نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الغرض افکند ز پا آن شجر</p></div>
<div class="m2"><p>با کشش اره و ضرب تبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>باز بیامد بر حاسد چو باد</p></div>
<div class="m2"><p>گفت کرم کن که درخت اوفتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>داد بوی اجرت و دلشاد شد</p></div>
<div class="m2"><p>ساعتی از قید غم آزاد شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رفت شب و روز پدیدار گشت</p></div>
<div class="m2"><p>خفته بدان فتنه و بیدار گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک تنش‌ آمد ز محبان ببر</p></div>
<div class="m2"><p>داد از آن واقعه بر وی خبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت درخت تو بریدند دوش</p></div>
<div class="m2"><p>زین سخنش رفت ز سر عقل و هوش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بافت چه رخ داده از آن مات شد</p></div>
<div class="m2"><p>مات همانا ز مکافات شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کفت ملامت به خود و این سخن</p></div>
<div class="m2"><p>ورد زبان ساخت بهر انجمن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای که ستم بر دگران میکنی</p></div>
<div class="m2"><p>تیشه تو بر ریشه خود می‌زنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آری اگر راه به وحدت بریم</p></div>
<div class="m2"><p>ما همه در اصل ز یک گوهریم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با هم اگر نیک و اگر بد کنیم</p></div>
<div class="m2"><p>هرچه کنیم آن همه با خود کنیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پند صغیر است در شاهوار</p></div>
<div class="m2"><p>ساز بگوش دل خود گوشوار</p></div></div>