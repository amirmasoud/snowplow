---
title: >-
    شمارهٔ ۱۸ - شیر و روباه
---
# شمارهٔ ۱۸ - شیر و روباه

<div class="b" id="bn1"><div class="m1"><p>گرسنه شیری چو حریصان بدشت</p></div>
<div class="m2"><p>در طلب طعمه بهر سوی گشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روبهی افتادیش اندر به چنگ</p></div>
<div class="m2"><p>خواست درد پیکر آن بیدرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت که ای بر تو سراسر سباع</p></div>
<div class="m2"><p>عبد مطیع و تو ‌امیر متاع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام روا از من آزرده گیر</p></div>
<div class="m2"><p>روزی یک روزهٔ خود خورده گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز شوی گرسنه روز دگر</p></div>
<div class="m2"><p>در طلب طعمه شوی در بدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به که نشینی تو بجا خواجه‌وار</p></div>
<div class="m2"><p>من چو یکی بندهٔ خدمتگذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر تو هر روز شکار آورم</p></div>
<div class="m2"><p>طعمه‌ات از جان بکنار آورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورد فریب وی و گفتا که هان</p></div>
<div class="m2"><p>زود در این کار بده‌ امتحان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور بگریزی تو دچار منی</p></div>
<div class="m2"><p>روز دگر باز شکار منی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رو به مکار دوان گشت و زود</p></div>
<div class="m2"><p>بره‌ئی از گله بمرتع ربود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آمد و اندر بر شیرش نهاد</p></div>
<div class="m2"><p>چاک زدش پیکر و دور ایستاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت بدو شیر که ای باوفا</p></div>
<div class="m2"><p>هم تو بیا باش مرا هم غذا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت مرا قدرت این کار نیست</p></div>
<div class="m2"><p>بهر من این کار سزاوار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت چرا گفت مباد از دو تن</p></div>
<div class="m2"><p>طعمه کم آید تو کنی قصد من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون کمی طعمه کند رنجه‌ات</p></div>
<div class="m2"><p>در شکمم جای کند پنجه‌ات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت مکن بیم بگفت ای‌ امیر</p></div>
<div class="m2"><p>پس ز کرم خواهش من در پذیر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست بنه روی هم اندر قفا</p></div>
<div class="m2"><p>تا که به بندم گه اکل غذا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گر که شوی سیر گشایم رسن</p></div>
<div class="m2"><p>ورنه که صید دگر آرم بفن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شیر پذیرفت ز وی از غرور</p></div>
<div class="m2"><p>گفت بیاور رسنی در حضور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جست و بامعاء بره برملا</p></div>
<div class="m2"><p>دست فرو بست ز شیر از قفا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دست چو بر بست ز شیر عرین</p></div>
<div class="m2"><p>رقص همی کرد در آنسر زمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مردم صحرا پی تسخیر آن</p></div>
<div class="m2"><p>روی بوی کرده ز خرد و کلان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دید شدش کام میسر گریخت</p></div>
<div class="m2"><p>خاک هلاکت بسر شیر ریخت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ماند بجا شیر بحال پریش</p></div>
<div class="m2"><p>شست دگر دست و دل از جان خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کرد برون موشکی از خانه سر</p></div>
<div class="m2"><p>جست پریشانی وی را خبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت مرا روبهکی بسته دست</p></div>
<div class="m2"><p>گفت مخور غم که گشاینده هست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آمد و بگسست بدندان رسن</p></div>
<div class="m2"><p>شیردوان گشت بکوه و دمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پرد چو از جنک اجل جان بدر</p></div>
<div class="m2"><p>گفت بخود فهم کن ای خیره‌سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>غره بسر پنجه و بازوی خویش</p></div>
<div class="m2"><p>بودی و این مهلکه‌ام د بپیش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پنجه و بازوی تو بر جای بود</p></div>
<div class="m2"><p>بست تو را رو به و موشت گشود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا که توانی چو صغیر از غرور</p></div>
<div class="m2"><p>بگذر وزین ره مکن ایجان عبور</p></div></div>