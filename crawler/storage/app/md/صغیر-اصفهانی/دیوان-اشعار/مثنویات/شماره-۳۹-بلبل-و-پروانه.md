---
title: >-
    شمارهٔ ۳۹ - بلبل و پروانه
---
# شمارهٔ ۳۹ - بلبل و پروانه

<div class="b" id="bn1"><div class="m1"><p>بلیلی از نالهٔ مستانه‌ای</p></div>
<div class="m2"><p>کرد مباهات به پروانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت اگر عاشقی ای بی نوا</p></div>
<div class="m2"><p>همچو من از سینه برآور نوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این همه اسرار نهفتن چرا</p></div>
<div class="m2"><p>در دل خویش نگفتن چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لحظهٔی از سینه بر آور خروش</p></div>
<div class="m2"><p>چند همی سوزی و باشی خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بین که ز من شهر پر از غلغل است</p></div>
<div class="m2"><p>در همه جا شرح گل و بلبل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفت به پروانه بسی ناگوار</p></div>
<div class="m2"><p>گفت که ای بی‌خبر از عشق یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خامشی و سوختن و ساختن</p></div>
<div class="m2"><p>نیست شدن هستی خود باختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این زمن آن نغمه سرودن ز تو</p></div>
<div class="m2"><p>دعوی بیهوده نمودن ز تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل بتو ارزان و تو ارزان بگل</p></div>
<div class="m2"><p>گل بتو خندان و تو نالان بگل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق تو شایستهٔ آن رنگ و بوست</p></div>
<div class="m2"><p>حسن گل اندر خور این های و هوست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر دو از این ره بدر افتاده‌اید</p></div>
<div class="m2"><p>رسم و ره عشق ز کف داده‌اید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لاف مزن عشق تو خام است خام</p></div>
<div class="m2"><p>جذبهٔ معشوق تو هم ناتمام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جذبه معشوق مرا بین که چون</p></div>
<div class="m2"><p>همچو منی آیدش از در درون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنگ بگیرد بوی آنگونه راه</p></div>
<div class="m2"><p>کان نتواند کشد از سینه آه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خیره بدان سان کندش از عذار</p></div>
<div class="m2"><p>کان نتواند کند از وی گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عشق مرا بین که به بزم حضور</p></div>
<div class="m2"><p>چونکه به معشوق رسم ناصبور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرد سرش گردم و قربان شوم</p></div>
<div class="m2"><p>سوخته‌ای جلوهٔ جانان شوم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رسم دوئی بر فکنم از میان</p></div>
<div class="m2"><p>جسم رها کرده شوم جمله جان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هم تو صغیر از پی جانانه باش</p></div>
<div class="m2"><p>فانی آن شمع چو پروانه باش</p></div></div>