---
title: >-
    شمارهٔ ۳۶ - حسن و مال
---
# شمارهٔ ۳۶ - حسن و مال

<div class="b" id="bn1"><div class="m1"><p>دوش ز من کرد عزیزی سئوال</p></div>
<div class="m2"><p>از بهی و برتری حسن و مال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش ارمال بدست سخی است</p></div>
<div class="m2"><p>بهر سخی ماحصلش فرخی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاصه چو اکرام کند او به جا</p></div>
<div class="m2"><p>هست معزز بر خلق خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در اثر بخشش و بذل نعم</p></div>
<div class="m2"><p>خلق پرستند ورا چون صنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یابد از این مال چو حسن مآل</p></div>
<div class="m2"><p>به بود این مال ز حسن جمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور که شود حسن به عصمت قرین</p></div>
<div class="m2"><p>نعمت خاصی است بنعمت قرین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر بر این حسن ندارد بها</p></div>
<div class="m2"><p>حسن چو خورشید بود زر سها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یوسف از اینحسن چو رأیت فراشت</p></div>
<div class="m2"><p>داد زلیخا به رهش هرچه داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایندو گه از عام و گه از خاص بین</p></div>
<div class="m2"><p>برتری هر دو در اشخاص بین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مختصر ار گفت صغیر این جواب</p></div>
<div class="m2"><p>فکر کن و باقی مطلب بیاب</p></div></div>