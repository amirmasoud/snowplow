---
title: >-
    شمارهٔ ۱ - ساقی‌نامه
---
# شمارهٔ ۱ - ساقی‌نامه

<div class="b" id="bn1"><div class="m1"><p>بیا ساقی ای رشگ حور بهشت</p></div>
<div class="m2"><p>که رشگ بهشتست گلزار و کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این فصل می‌خوردن از دست تو</p></div>
<div class="m2"><p>خوش است ایدل خسته پا بست تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا ساقی ای راحت جان من</p></div>
<div class="m2"><p>فشان گرد هستی ز دامان من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بده ساغری زان می‌خوشگوار</p></div>
<div class="m2"><p>که بیرون کند از سر جان خمار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا ساقی ای یار دیرینه‌ام</p></div>
<div class="m2"><p>فروز آتش عشق در سینه‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمی آگهم کن ز اسرار عشق</p></div>
<div class="m2"><p>مرا مست کن تا کشم بار عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا ساقی‌ام روز دوران ماست</p></div>
<div class="m2"><p>دهی‌گر مرا کوزهٔ می‌رواست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که فردا شود خاک من سر بسر</p></div>
<div class="m2"><p>گل کوزه اندر کف کوزه‌گر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا ساقی ای محرم راز من</p></div>
<div class="m2"><p>بمی باز کن راه پرواز من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بده ساغری زان می‌وحدتم</p></div>
<div class="m2"><p>رهان از هیاهوی این کثرتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیا ساقی ای آفت عقل و هوش</p></div>
<div class="m2"><p>چو خم میم از می‌آوربجوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بده آب انگور آن تاک پاک</p></div>
<div class="m2"><p>که پیچان کند میکشانرا چه تاک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بیا ساقی آن می‌مرا کن بجام</p></div>
<div class="m2"><p>که از جم رساند بمستان پیام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که تا جان بود می‌پرستی کنید</p></div>
<div class="m2"><p>غنیمت شمارید و مستی کنید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بیا ساقی آن باده کن در سبو</p></div>
<div class="m2"><p>که دل را تهی سازد از آرزو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشاید به رخ باب آزادگی</p></div>
<div class="m2"><p>دهد نفس را میل افتادگی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بیا ساقی اندر سبو کن شراب</p></div>
<div class="m2"><p>که من غم ندارم ز روز حساب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو لطف خدا شامل حال ماست</p></div>
<div class="m2"><p>برای خطا خوردن غم خطاست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیا ساقی از خود رها کن مرا</p></div>
<div class="m2"><p>دمی آشنا با خدا کن مرا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به قصد خرابی مرا می‌بیار</p></div>
<div class="m2"><p>دمادم کرم کن پیا پی بیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیا ساقی از زاهد اندیشه نیست</p></div>
<div class="m2"><p>بجز می‌کشیدن مرا پیشه نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزاهد بگو از تو زهد و ثواب</p></div>
<div class="m2"><p>من و مستی و عیش و جام شراب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیا ساقی آن می‌عطا کن بمن</p></div>
<div class="m2"><p>که در نغمه آیم چو مرغ چمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قدم صد ره از سدره برتر زنم</p></div>
<div class="m2"><p>دم از مدح ساقی کوثر زنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به شاهان عالم کنم افتخار</p></div>
<div class="m2"><p>ز مداحی شاه دلدل سوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز نظمم زنم طعنه بر سلسبیل</p></div>
<div class="m2"><p>به مداحی مرشد جبرئیل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>علی بن عم و جانشین رسول</p></div>
<div class="m2"><p>علی شیر حق زوج پاک بتول</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر کفر باشد خدا خوانمش</p></div>
<div class="m2"><p>ز حق کافرم گر جدا دانمش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به حول خداوند تا زنده‌ام</p></div>
<div class="m2"><p>قبول ار نماید ورا بنده‌ام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بیرون از این نشأه خواهم شدن</p></div>
<div class="m2"><p>بدامان او دست خواهم زدن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چنان در دلم آتش افروخته</p></div>
<div class="m2"><p>شرار غمش خرمنم سوخته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که چون در لحد منزل پرملال</p></div>
<div class="m2"><p>نکیرین از من کنند این سئوال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که ای بنده بر گو خدای تو کیست</p></div>
<div class="m2"><p>بآن هر دو شاید بگویم علی است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>علی ولی صهر خیرالبشر</p></div>
<div class="m2"><p>خداوند دین حیدر حیه در</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کسی گر بذات علی برد راه</p></div>
<div class="m2"><p>تواند برد ره به ذات اله</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیا ساقی از مهر او می‌بیار</p></div>
<div class="m2"><p>مرا مست کن زان می‌خوشگوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگرچه مرا جسم و جانمست اوست</p></div>
<div class="m2"><p>ولی هرچه باشد فزونتر نکوست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز مستی فزونتر مرا مست کن</p></div>
<div class="m2"><p>وزان مستی این نیست راهست کن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بده ساقی از آن شرابم بطی</p></div>
<div class="m2"><p>سبوئی حمی دجله‌ئی یا شطی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کبیرانه می‌ده صغیرم مگیر</p></div>
<div class="m2"><p>که صورت صغیر است و معنی کبیر</p></div></div>