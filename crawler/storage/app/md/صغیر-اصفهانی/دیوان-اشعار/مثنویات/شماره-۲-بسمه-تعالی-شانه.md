---
title: >-
    شمارهٔ ۲ - بسمه تعالی شانه
---
# شمارهٔ ۲ - بسمه تعالی شانه

<div class="b" id="bn1"><div class="m1"><p>ای همه هستی همه هستی توئی</p></div>
<div class="m2"><p>غیر تو کو تا که درآید دوئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت و معنی صفت و ذات تست</p></div>
<div class="m2"><p>نفی که سازم همه اثبات تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذات تو را نیست دوئی باصفات</p></div>
<div class="m2"><p>ذات صفاتست و صفاتست ذات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در پیت آن فرقه که بشتافتند</p></div>
<div class="m2"><p>خویش چو گم کرده ترا یافتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در تو شود منتهی این ما و من</p></div>
<div class="m2"><p>جمله توئی نیست مقام سخن</p></div></div>