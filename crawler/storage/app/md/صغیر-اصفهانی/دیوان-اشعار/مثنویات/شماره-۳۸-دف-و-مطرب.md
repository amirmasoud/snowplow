---
title: >-
    شمارهٔ ۳۸ - دف و مطرب
---
# شمارهٔ ۳۸ - دف و مطرب

<div class="b" id="bn1"><div class="m1"><p>دف به کف مطربکی تیز هوش</p></div>
<div class="m2"><p>داشت چنین از ره معنی خروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی عجب این جور و جفا تا بکی</p></div>
<div class="m2"><p>جور به این بی سروپا تا بکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلق به یک حلقه غلامی کنند</p></div>
<div class="m2"><p>خود به بر خواجه گرامی کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که به صد حلقه غلام‌ آمدم</p></div>
<div class="m2"><p>دایرهٔ عشرت عام‌ام دم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند خورم از کف مطرب قفا</p></div>
<div class="m2"><p>چند بر آرم گه و بی‌گه نوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این سخن از سوز چو آنساز گفت</p></div>
<div class="m2"><p>مطرب شیرین سخنش باز گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالی از این کت ز چه بنواختم</p></div>
<div class="m2"><p>من پی بنواختنت ساختم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناله‌ات‌ام د سبب و جد و حال</p></div>
<div class="m2"><p>من بهمین مایلم ای دف بنال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای دل دانا مکن از ناله بس</p></div>
<div class="m2"><p>دم مکش از ناله دمی چون جرس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو دف از دوست خوری گر قفا</p></div>
<div class="m2"><p>آن ز وفا دان نه ز راه جفا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنگ وشت گر که دهد گو شمال</p></div>
<div class="m2"><p>همچو نی از بهر دل او بنال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل که به دلدار ننالد صغیر</p></div>
<div class="m2"><p>مشت گلش بیش مخوان دل‌مگیر</p></div></div>