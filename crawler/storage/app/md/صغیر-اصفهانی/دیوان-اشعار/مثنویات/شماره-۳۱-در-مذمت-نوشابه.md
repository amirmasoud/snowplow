---
title: >-
    شمارهٔ ۳۱ - در مذمت نوشابه
---
# شمارهٔ ۳۱ - در مذمت نوشابه

<div class="b" id="bn1"><div class="m1"><p>بود یکی خانه چو باغ جنان</p></div>
<div class="m2"><p>سر بسر اسباب تجمل در آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیم و زر و لعل و در شاهوار</p></div>
<div class="m2"><p>نیز در آن خانه بدی بی‌شمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه خدا داشت ز دزدان هراس</p></div>
<div class="m2"><p>شب همه شب داد بدان خانه پاس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش ز بد اندیش بداور افراغ</p></div>
<div class="m2"><p>زانکه به شب هیچ نکشتی چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر وی و خانه‌اش آن روشنی</p></div>
<div class="m2"><p>بود به شبها سبب ایمنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کهنه حریفی شبی از سارقین</p></div>
<div class="m2"><p>سنگ زد او را به چراغ از کمین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت چو مستغرق ظلمت فضا</p></div>
<div class="m2"><p>دزد فرو جست ز بام سرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت خود آن لحظه غنیمت شمرد</p></div>
<div class="m2"><p>هرچه که میخواست از آن خانه برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیک چو بینی بر اهل کمال</p></div>
<div class="m2"><p>صورت حال بشر است این مقال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خانه کدام است وجود بشر</p></div>
<div class="m2"><p>کامده خود مخزن در و گهر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سربسر اسباب تجمل در اوست</p></div>
<div class="m2"><p>جزء وجود است ولی کل در اوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چیست چراغ آنچه تو خوانیش عقل</p></div>
<div class="m2"><p>کش نتوان قدر و بها کرد نقل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دزد که ابلیس رجیم لعین</p></div>
<div class="m2"><p>آنکه حقش خوانده عدوی مبین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سنگ چه نوشابه که آن دزد هوش</p></div>
<div class="m2"><p>شمع خرد را کند از آن خموش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و آنچه که خواهد ز بشر آن برد</p></div>
<div class="m2"><p>شرم و حیا عفت و وجدان برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رحم برد تا شود از سرکشی</p></div>
<div class="m2"><p>خیره برادر به برادرکشی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حق اخوت که بود از ازل</p></div>
<div class="m2"><p>امر طبیعی به میان ملل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرد و تن از نوع بشر در جهان</p></div>
<div class="m2"><p>گوی زمین باشدشان در میان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن دو چو از یک پدر و مادرند</p></div>
<div class="m2"><p>بلکه ز یک نفس و ز یک گوهرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باشدشان حق اخوت به جا</p></div>
<div class="m2"><p>بایدشان کرد مر آن حق ادا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرنه ز مستی است چرا تا بحال</p></div>
<div class="m2"><p>گشته چنین حق بجهان پایمال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خاصیت می‌بود این کز بشر</p></div>
<div class="m2"><p>روز و شبان سر زند انواع شر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غفلت مستی است که حایل شود</p></div>
<div class="m2"><p>بنده ز حق این همه غافل شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بلکه جهان را کند آن سنگدل</p></div>
<div class="m2"><p>ز آتش بیداد و ستم مشتعل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هیچ نگوید که جهان آفرین</p></div>
<div class="m2"><p>داشت چه منظور ز طرحی چنین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهرچه این ارض و سما آفرید</p></div>
<div class="m2"><p>از پی بازیچه ما آفرید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مست می‌آگه ز جنایات نیست</p></div>
<div class="m2"><p>با خبر از روز مکافات نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شد چو به تاثیر می‌از عقل فرد</p></div>
<div class="m2"><p>هیچ نداند که چه گفت و چه کرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نیست صغیر از ره کذب و عناد</p></div>
<div class="m2"><p>گر بنهی نام می‌ ام‌الفساد</p></div></div>