---
title: >-
    شمارهٔ ۲۶ - مصاحبت دو نادان
---
# شمارهٔ ۲۶ - مصاحبت دو نادان

<div class="b" id="bn1"><div class="m1"><p>بود یکی مرد تهی مغز خام</p></div>
<div class="m2"><p>با زن خود خفته شبی روی بام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد به حیرت سوی گردون نظر</p></div>
<div class="m2"><p>وز زن خود جست ز انجم خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زن ز کسان آنچه که بشنیده بود</p></div>
<div class="m2"><p>گفت و در آن مرد تحیر فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داد از آن جمله خط کهکشان</p></div>
<div class="m2"><p>با سر انگشت به شوهر نشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت که این جادهٔ بیت‌الله است</p></div>
<div class="m2"><p>قافلهٔ حاج روان زین ره است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد چو این مسئله از زن شنید</p></div>
<div class="m2"><p>سخت غمین گشت و زبان در کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت مباد اشتری از حاجیان</p></div>
<div class="m2"><p>بر سر من اوفتد از آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود در این فکر که از بخت بد</p></div>
<div class="m2"><p>اشتر همسایه ز دل بانگ زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت همانا شتر‌ آمد فرود</p></div>
<div class="m2"><p>به که گریزم من بیچاره زود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جست شتابان و به ره رو نهاد</p></div>
<div class="m2"><p>از زبر بام به زیر اوفتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونکه در افتاد ز بالا به پست</p></div>
<div class="m2"><p>دل شده را دست و سر و پا شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ابله از ابله سخنی کرد گوش</p></div>
<div class="m2"><p>نیم چراغ خردش شد خموش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نامده اشتر ز سما بر سرش</p></div>
<div class="m2"><p>گشت لگد کوب بلا پیکرش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زین مثل اربا خردی بی‌سخن</p></div>
<div class="m2"><p>کشف شود بهر تو مقصود من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بین دو نادان ز جواب و سئوال</p></div>
<div class="m2"><p>فایده چبود؟ غم و رنج و ملال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی دل از صحبت نادان بتاب</p></div>
<div class="m2"><p>چونکه ندانی بر دانا شتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صحیت دانا چه بود؟ کیمیا</p></div>
<div class="m2"><p>میشود از آن مس قلبت طلا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صحبت دانات معظم کند</p></div>
<div class="m2"><p>کعبه وشت قبله عالم کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکه به هر رتبه و هرجا رسید</p></div>
<div class="m2"><p>از اثر صحبت دانا رسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همدم دانا شوی ار یک نفس</p></div>
<div class="m2"><p>حاصل عمر تو همانست و بس</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مردم دانا که جهان دیده‌اند</p></div>
<div class="m2"><p>نیک چو بینی بجهان دیده‌اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیده چو بینا بود و برقرار</p></div>
<div class="m2"><p>هست یقین باقی اعصار بکار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رونق هر ملت و هر کشوری</p></div>
<div class="m2"><p>نیست جز از همت دانشوری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حاصل مطلب چو صغیر حزین</p></div>
<div class="m2"><p>در همه جا همدم دانا گزین</p></div></div>