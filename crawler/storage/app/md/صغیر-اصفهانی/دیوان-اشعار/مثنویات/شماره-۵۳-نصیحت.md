---
title: >-
    شمارهٔ ۵۳ - نصیحت
---
# شمارهٔ ۵۳ - نصیحت

<div class="b" id="bn1"><div class="m1"><p>بکن پندی سرور آور ز من گوش</p></div>
<div class="m2"><p>گرت فرسود غم بر دفع آن کوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظهور غم بود از نارضائی</p></div>
<div class="m2"><p>رضا شو تا زغم یابی رهائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت نارضائی خود ملال است</p></div>
<div class="m2"><p>چرا گفتن بکار ذوالجلال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا گفتن بکار حق فضولی است</p></div>
<div class="m2"><p>سزای آن فضولی این ملولی است</p></div></div>