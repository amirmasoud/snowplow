---
title: >-
    شمارهٔ ۴۰ - اسباب درویشی
---
# شمارهٔ ۴۰ - اسباب درویشی

<div class="b" id="bn1"><div class="m1"><p>بوالهوس دل به هوا بسته‌ئی</p></div>
<div class="m2"><p>گفت بدرویش ز خود رسته‌ئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاین دل من مایل درویشی است</p></div>
<div class="m2"><p>در طلب مصلحت‌اندیشی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر ره من شو و بنما رهم</p></div>
<div class="m2"><p>کن ز ره فقر و فنا آگهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که رسانیم بدین افتخار</p></div>
<div class="m2"><p>گو که فراهم کنم اسباب کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت که ای مانده به اسباب در</p></div>
<div class="m2"><p>باید از این مرحله کردن گذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچه که اسیاب بدین فن بود</p></div>
<div class="m2"><p>از سر اسباب گذشتن بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه که سرمایه درویشی است</p></div>
<div class="m2"><p>مایه ز کف دادن و بی‌خویشی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این ره هربوالهوس خام نیست</p></div>
<div class="m2"><p>راه حق است این ره حمام نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر طلبی حق ز خودی شو جدا</p></div>
<div class="m2"><p>می‌نشود جمع خودی با خدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم تو صغیر از خودی آزاد باش</p></div>
<div class="m2"><p>بیخودی آور بکف و شاد باش</p></div></div>