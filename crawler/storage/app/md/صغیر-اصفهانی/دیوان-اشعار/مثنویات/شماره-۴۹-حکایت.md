---
title: >-
    شمارهٔ ۴۹ - حکایت
---
# شمارهٔ ۴۹ - حکایت

<div class="b" id="bn1"><div class="m1"><p>بزشتی کرد تسخر عیب‌جوئی</p></div>
<div class="m2"><p>که از خوبی نداری آبروئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را حق کرده محروم از ملاحت</p></div>
<div class="m2"><p>ز اعضای تو میریزید قباحت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر عضو نو عیبی آشکار است</p></div>
<div class="m2"><p>ز دیدارت نگاه اندر فرار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملاقات تو افزاید کدورت</p></div>
<div class="m2"><p>گریز از چون توئی باشد ضرورت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگفت ار من همه عیبم سراپا</p></div>
<div class="m2"><p>در این صورت نباشم چون تو رسوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا یک عضو بد در کار باشد</p></div>
<div class="m2"><p>کزان کارت بسی دشوار باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخوبی گر سرا پا همچو جانی</p></div>
<div class="m2"><p>پسند کس نئی چون بد زبانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عیب نقش می‌گویی به هُش باش</p></div>
<div class="m2"><p>که گویی در حقیقت عیب نقاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صغیر از عیب جوئیها حذر کن</p></div>
<div class="m2"><p>گرت چشمی بود در خود نظر کن</p></div></div>