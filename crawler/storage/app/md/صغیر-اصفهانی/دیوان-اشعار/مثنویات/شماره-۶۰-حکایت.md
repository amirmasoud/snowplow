---
title: >-
    شمارهٔ ۶۰ - حکایت
---
# شمارهٔ ۶۰ - حکایت

<div class="b" id="bn1"><div class="m1"><p>به رستم چه خوش گفت فرزانه زال</p></div>
<div class="m2"><p>که ای پور نام آور بی‌همال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مترس از دوصد مرد شمشیرزن</p></div>
<div class="m2"><p>بپرهیز از آه یک پیرزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تیغ یلان را سپر حایل است</p></div>
<div class="m2"><p>وزان تیر پنهان شدن مشکل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز قد کمان وار بس تیر آه</p></div>
<div class="m2"><p>که در یک نفس رفته صد میل راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هدف جسته و بر نشان‌ آمده</p></div>
<div class="m2"><p>ز دل گشته پران به جان‌ آمده</p></div></div>