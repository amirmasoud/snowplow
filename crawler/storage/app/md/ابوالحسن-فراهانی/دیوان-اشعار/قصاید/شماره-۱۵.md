---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>ز جور دل که هیچ کس مباد چنین</p></div>
<div class="m2"><p>سرم مباد گرم سررسید بر بالین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن که می دهد از اجتماع یاران باد</p></div>
<div class="m2"><p>نمی توانم برداشت دیده از پروین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو طفل مکتب هر صبح از سیه روزی</p></div>
<div class="m2"><p>دهم به آمدن شاه خویشتن تسکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر این بود غم دوری به هر که بدخواهی</p></div>
<div class="m2"><p>برو به دوری احباب کن برو نفرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور آن که باید زین گونه زیست بی یاران</p></div>
<div class="m2"><p>به زندگانی یاران که مرگ بهتر ازین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر یکی نبود شام مرگ و شام فراق</p></div>
<div class="m2"><p>پس از برای چه از خشت می کنم بالین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل جهان مگر از من گرفت ورنه چرا</p></div>
<div class="m2"><p>همیشه با من بیهوده خشم ورزد و کین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب مدار گر از من و دل جهان گیرد</p></div>
<div class="m2"><p>کز اشک و آهم برگشت آسمان و زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبرد تلخی زهر فراق از جانم</p></div>
<div class="m2"><p>شکایت غم هجران مکر من مسکین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سر قصیده به سوی گریزگاه روم</p></div>
<div class="m2"><p>کنم زنام خداوند کام جان شیرین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپهر قدر زمین حلم میرزا نوری</p></div>
<div class="m2"><p>که برده مایه دانش به اوج علیین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضمیر فهم و زبان دان و آفتاب ضمیر</p></div>
<div class="m2"><p>سخن شناس و سبک روح و مشتری تمکین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر به عرصه شطرنج بگذرد رایش</p></div>
<div class="m2"><p>پیاده وار نهد رخ به راستی فرزین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زدر لفظش سین سخن توانگر شد</p></div>
<div class="m2"><p>بدان مثابه کزو مایه وام خواهد شین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو او بدیهه نویسد ضریر خامه او</p></div>
<div class="m2"><p>کند خطاب به گردون که خیز و در برچین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زاهل خطه شیراز آن چنان ممتاز</p></div>
<div class="m2"><p>که از فصول بهار از شهور فروردین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر به قدرش نازد فلک روا باشد</p></div>
<div class="m2"><p>بلی همیشه بود نازش مکان به مکین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حکیم راه به قدر بلند او نبرد</p></div>
<div class="m2"><p>زمن اقامت برهان زسامعان تحسین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر زقدرتش اگه بدی کجا گفتی</p></div>
<div class="m2"><p>که نیست چیزی بالای آسمان برین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خدایگانا تا دامنت ز دستم رفت</p></div>
<div class="m2"><p>ز اضطراب ندانم یسار را از یمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرا که عاصی تقصیر خدمتم چون شد</p></div>
<div class="m2"><p>ز وصل خط تو حاصل وصال حورالعین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نوشته ی که به من خویش را بنویس</p></div>
<div class="m2"><p>نوشتنی نبود حال من بیا و به بین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بیا بیا که زبس خون گریستم ز فراق</p></div>
<div class="m2"><p>چو نکته های تو گشتند اختران رنگین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من از فراق تو کان دیگرم نصیب مباد</p></div>
<div class="m2"><p>اگر نکرده ام از خون دیده خاک عجین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زتنگ عیشی ترسیده ام که بی تو فلک</p></div>
<div class="m2"><p>برات رزقم بر خون نوشته همچو جنین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شب سیاه شدی روز من ز درد فراق</p></div>
<div class="m2"><p>گه جدایی یاران و دوستان امین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>غم فراق تو برعکس دردهای دگر</p></div>
<div class="m2"><p>شب سیاه مرا کرد روز باز پسین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر بگویم غمگین نیم ز دوری تو</p></div>
<div class="m2"><p>عجب مدار که نفس غمم کنون به غمین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر کناره کنی برحقی چو باز آیی</p></div>
<div class="m2"><p>که کس نگردد با غم به اختیار قرین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به من سپهر زبیم تو دوستی میکرد</p></div>
<div class="m2"><p>وگرنه نیست محبت سپهر را آمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو در رکاب نهادی به عزم رفتن، پا</p></div>
<div class="m2"><p>به تازگی فلک سفله رفت بر سر کین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به اختیار جدا گشته ام ز همچو تویی</p></div>
<div class="m2"><p>به اعتماد صبوری کنون بحرم همین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من از دعای تو کان واجب است بر همه کس</p></div>
<div class="m2"><p>چو فارغ آیم بر خویشتن کنم نفرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا چه کار درین شهر کز تو وامانم</p></div>
<div class="m2"><p>گرم نه بخت بر این شیوه ها کند تلقین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چرا همیشه چو انگشترت نبوسم دست</p></div>
<div class="m2"><p>منی که خانه به دوش زمانه ام چو نگین</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا برتو فرستادن این چنین مدحی</p></div>
<div class="m2"><p>چنان بود که فرستی گلستان نسرین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگرچه در ثمین است شعر من، لیکن</p></div>
<div class="m2"><p>چه قدر دارد در پیش بحر در ثمین</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مدیح ذات تو هم کار طبع عالی توست</p></div>
<div class="m2"><p>بدیهه های تو کوتا من آن کنم تضمین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نهاد پاک تو در خاک طینت آدم</p></div>
<div class="m2"><p>نهاده بود ز روز ازل سپهر دفین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر نخوردی از بخل تیشه هرگز کان</p></div>
<div class="m2"><p>وگر نداشتی از موج روی دریاچین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کنون که قحط کرم شد تو را برون آورد</p></div>
<div class="m2"><p>بلی دفین بود از بهر روزهای چنین</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به بحر و کان دل و دست تو کزدمی نسبت</p></div>
<div class="m2"><p>ایا به جیب دلت بحر و کان دو خاک نشین</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به مشک خط تو تشبیه چون کنم نافه</p></div>
<div class="m2"><p>که در میانه ایشان تفاوتی یست مبین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زمشک خامه ی تو پر شود دماغ خرد</p></div>
<div class="m2"><p>بپوست آر و در مغز بوی نافه چین</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تویی فرید زمانه زردان مثلث</p></div>
<div class="m2"><p>زمین سترون گردید و آسمان عنین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سپهر پیر که برحسب گفته ی حکما</p></div>
<div class="m2"><p>پدر بود همه را خواه شاد و خواه حزین</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو شعر تضمین فرزند عاریت داند</p></div>
<div class="m2"><p>به جز تو هرکه بود زاده شهور و سنین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همیشه تا که امنیان عالم یاری</p></div>
<div class="m2"><p>به خاک پای امنیان خود خورند ثمین</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به خاک پای تو سوگند آسمان بادا</p></div>
<div class="m2"><p>تو هرکجا که نهی پای او نهاده جبین</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همیشه تا که روانی یست بر عقول فنا</p></div>
<div class="m2"><p>بقای عمر تو بادا تو هم بگو آمین</p></div></div>