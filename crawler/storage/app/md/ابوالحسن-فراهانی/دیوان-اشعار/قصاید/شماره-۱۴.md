---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>اشک نبود این که می بارم ز روز تار من</p></div>
<div class="m2"><p>روز اختر می شمارد چشم اختر بار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من پریشان و پریشان دوستم بر تارکم</p></div>
<div class="m2"><p>از پریشانی کند جاطره دستار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که شب تابم ز آه گرم این ویرانه را</p></div>
<div class="m2"><p>روز ننشیند کسی در سایه دیوار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شور و شیرین هرچه پیش آمد ز عشق آید به بین</p></div>
<div class="m2"><p>شوری بخت من و شیرینی گفتار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب و آتش را زیان دارد عجب نبود اگر</p></div>
<div class="m2"><p>گم کند گرمی چو بیند گریه بسیار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چرا نالید می چندین ازین سنگین دلان</p></div>
<div class="m2"><p>گر نبودی شیشه مانند دل دربار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تندخویان کارها به رغم دل گاهی کنند</p></div>
<div class="m2"><p>رحم کن چون آسمان گرم است در آزار من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره خون در بدن دارم نمی دانم که باز</p></div>
<div class="m2"><p>روی من گلگون کند یا پنج های یار من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پنجه او را کند گلگون ای کاش بس بود</p></div>
<div class="m2"><p>نعت پیغمبر برای سرخی رخسار من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش از آن کز نعت سازم خویشتن را سرفراز</p></div>
<div class="m2"><p>بود عالم گیر شوم چون در شهوار من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعت گفتم عالم عقبی گرفتم نیست لاف</p></div>
<div class="m2"><p>گر بگویم هر دو عالم را گرفت اشعار من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل اول منشأ ایجاد خیرالمرسلین</p></div>
<div class="m2"><p>خواجه ی اول محمد سید و سالار من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روز نعتش گوشه گیرم تا زنور نعت او</p></div>
<div class="m2"><p>در ضمیر من نه بیند مدعی اسرار من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت آن آمد که اندازم ز شعر آبدار</p></div>
<div class="m2"><p>میل در بنیاد او کو می کند انکار من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من سپهرم آفتاب من خیال روی دوست</p></div>
<div class="m2"><p>داغی بی حد اشک خونین ثابت و سیار من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چشم آن دارم که جز مهر خود و اولاد خود</p></div>
<div class="m2"><p>خط کشی بر هرچه آن ثبت است در طومار من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شعر اکذب احسنت اما زیمن نعت او</p></div>
<div class="m2"><p>اصدق اقوال من شد احسن اشعار من</p></div></div>