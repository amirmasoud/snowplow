---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>گرفتم آینه تا بنگرم حقیقت حال</p></div>
<div class="m2"><p>ز ضعف خویش نبینم در آینه تمثال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه گریم و هیچش سبب نمیدانم</p></div>
<div class="m2"><p>بسان طفلی که آتش گرفته در چنگال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زجور چرخ نه اکنون خمیده ام که نخست</p></div>
<div class="m2"><p>خمیده قامت زایند مادرم چو هلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکسته بالم از خلق از آن کناره کنم</p></div>
<div class="m2"><p>کناره گیر مرغی که بشکنندش بال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هیچ سو ننهادم قدم که روز سیاه</p></div>
<div class="m2"><p>مرا نیامد مانند سایه از دنبال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان به نوعی در چشم من شده تاری</p></div>
<div class="m2"><p>که روز و شب را بینم همین به یک تمثال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبس که سر به گریبان کشیده آه زدم</p></div>
<div class="m2"><p>تنور تافته گردید بر تنم سربار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خون ناب شود اندرو چو آب کنند</p></div>
<div class="m2"><p>از آن سپس که زخاکم کند زمانه سفال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر دو گام روم بیست جای بنشینم</p></div>
<div class="m2"><p>بسان پیران با آن که بیست دارم سال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدین طریق سراسیمه داردم گردون</p></div>
<div class="m2"><p>که می ندانم چون کودکان یمین ز شمال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون که لذت خونابه جگر دیدم</p></div>
<div class="m2"><p>اگر بسوزم لب تر نمی کنم به زلال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قدم نیارم بیرون نهاد از خانه</p></div>
<div class="m2"><p>زبس که برد رو بامم هجوم کرده کلال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان صفت که نیارد برون شد زایر</p></div>
<div class="m2"><p>ز کثرت ملک از روضه ی سپهر جلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امام ثالث کالبته ثانی او بودی</p></div>
<div class="m2"><p>اگر محال نبودی دو ایزد متعال</p></div></div>