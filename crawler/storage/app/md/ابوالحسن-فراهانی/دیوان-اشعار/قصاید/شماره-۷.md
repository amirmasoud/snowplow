---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>غمم نمی رود از دل به گریه بسیار</p></div>
<div class="m2"><p>کسی به آب ز آینه چون برد زنگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا چو چشمه شده چشم ها زجور فلک</p></div>
<div class="m2"><p>کنم به ناخن از آن روی وی بر رخسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمیده قدم بر بار دل بود شاید</p></div>
<div class="m2"><p>نهال خم نشود تا فزون نگردد بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زناله من جز بخت خواب روزی من</p></div>
<div class="m2"><p>کدام شب که نگشتند خفتگان بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام بخت که آواره گشته ام ز وطن</p></div>
<div class="m2"><p>کدام صبر که دوری گزیده ام از یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده وصلی عمرم گذشت در هجران</p></div>
<div class="m2"><p>نخورده جام جانم به لب رساند خمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب بنا شد ای دوستان کزین دو سه روز</p></div>
<div class="m2"><p>مرا نسوخته باشد فراق یار و دیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز آب دیده تنم نم گرفت وگرچه</p></div>
<div class="m2"><p>چونم گرفت بزودی نسوزد او از نار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو کرد بختم محبوس در حصار فراق</p></div>
<div class="m2"><p>نمود چرخ ستم پیشه حریف آزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم دو عالم محبوس در حصار دلم</p></div>
<div class="m2"><p>وز آب چشمم خندق فکند کرد حصار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا زمانه جدا کرد از گلستانی</p></div>
<div class="m2"><p>که از تصور دوریش رفتمی از کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورنق آیین باغی که وقت سیر درو</p></div>
<div class="m2"><p>ز بوی گل نشناسند مست از هشیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشاط بخش مقامی که عاشقان دروی</p></div>
<div class="m2"><p>تمام عمر تواند زیست بی دلدار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برآورد سر هر صبح مهر از خاور</p></div>
<div class="m2"><p>بدین امید که در سایه اش بباید یار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به باغبانش اگر می کند وزین غافل</p></div>
<div class="m2"><p>که در بهشت نمی یابد آفتاب گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درخت های گل آن بهشت جاویدان</p></div>
<div class="m2"><p>همی خلاند در دیده های طوبی خار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صبا رود سوی او هم چنان فتان خیزان</p></div>
<div class="m2"><p>که سوی عطار طبله میرود بیمار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سزد که حرباخصمی کند به بلبل از آن که</p></div>
<div class="m2"><p>به جای گل همه خورشید بیند اندر بار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شفا به بخشد اگر نامش آوری به زبان</p></div>
<div class="m2"><p>به جای فاتحه اندر عیادت بیمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به مهر لاله او خواستم کنم تشبیه</p></div>
<div class="m2"><p>خرد نفیر بر آورد کای مکن زنهار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فروغ مهر کند چهره را سیاه و کند</p></div>
<div class="m2"><p>فروغ لاله او سرخ چهره شب تار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن فلک سوی خود می کشد چنارش را</p></div>
<div class="m2"><p>که پیر گشت و عصایی به بایدش ناچار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز بس که آب زند ابر بر رخ غنچه</p></div>
<div class="m2"><p>زخواب نوشین پیش از سحر شود بیدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زلطف آب و هوایش چنان که دست در آب</p></div>
<div class="m2"><p>فرو رود به زمین ظلّ دست سرو و چنار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به عزم وصفش چون ناظمی قلم گیرد</p></div>
<div class="m2"><p>هنوز حرفی بر صفحه نکرده نگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قلم بسان اصابع فرو برد ریشه</p></div>
<div class="m2"><p>کهَ مسّوده در دست ناظم اشعار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسان طفل که بی باغبان به باغ رود</p></div>
<div class="m2"><p>نهالگان را از گل پرست جیب و کنار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درو به بیند رخسار خویش را به مثل</p></div>
<div class="m2"><p>اگر نشیند اعمیش روی بر دیوار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صبا ستاده شب و روز منتظر که اگر</p></div>
<div class="m2"><p>شکوفه ی فکند تند باد از اشجار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به عذرخواهی برگیردش ز خاک چمن</p></div>
<div class="m2"><p>که نازک است و ندارد تحمل آزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو خطبه خواند بلبل فراز منبر شاخ</p></div>
<div class="m2"><p>سپیده دم که زند ابر خیمه بر گلزار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زباد خیل ریا حین فتند در سجده</p></div>
<div class="m2"><p>برون نیامده نام گل از زبان هزار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درو همیشه بهار مست از مهابت شاه</p></div>
<div class="m2"><p>نمی رباید باد از درخت ها دستار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بلند مرتبه شاهی که در مدایح او</p></div>
<div class="m2"><p>خرد چو طفلان هر لحظه از گم کند هنجار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زهمتش نتوان حصر نقطه کردن</p></div>
<div class="m2"><p>اگر ز دایره آسمان کنی پرگار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بود سپهر گدایی ز آستانه او</p></div>
<div class="m2"><p>گرفته اینک کشتی زماه نو به کنار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>درم نیاید از آن در کفش که آتش را</p></div>
<div class="m2"><p>میان دریا هرگز نبوده است قرار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زسیلی کرمش بخل راست چهره سیه</p></div>
<div class="m2"><p>به اعتمادش گرم است حور را بازار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قلم به نامش تصریح اگر کند چه عجب</p></div>
<div class="m2"><p>که طوطیان را شکر خوش است در منقار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وصی احمد مرسل علی عالی قدر</p></div>
<div class="m2"><p>که هست سایه او را ز مهر تابان عار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فلک نواز آن ساحرم که از طبعم</p></div>
<div class="m2"><p>شدست بحر جهان پر ز لولوی شهوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زرشک کلکم طوطی به خویشتن پیچد</p></div>
<div class="m2"><p>بسان شخصی کورا گزیده باشد مار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>توانم آن که بهر چند روز در مدحت</p></div>
<div class="m2"><p>قصیده کنم انشاء ز طبع گوهر بار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ولی زشومی جمعی که از لکامتشان</p></div>
<div class="m2"><p>سفر گزیدم و هستم هنوز در آزار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جماعتی که اگر عمر خویش صرف کنند</p></div>
<div class="m2"><p>مسیح را نشناسند باز از بی طار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر ز خرمن غیری برند دانه چو مور</p></div>
<div class="m2"><p>برون روند ز شادی از پوست همچون مار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به زعم خویش مسلمان و بسته است اسلام</p></div>
<div class="m2"><p>زدست ایشان هر لحظه بر میان زنار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چنان گریزان از نان خود اگر یابند</p></div>
<div class="m2"><p>که از حرام گریزند، مردم دین دار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز هرچه داند آن را کمال عقل سلیم</p></div>
<div class="m2"><p>چنان بری که زنقص است ایزد دادار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تمام عمر به بد صرف کرده و هرگز</p></div>
<div class="m2"><p>نکرده الا از کار نیک استغفار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا دلی یست که گر شرح محنتش بدهم</p></div>
<div class="m2"><p>شروع ناشده بر خاطرت نشیند بار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>برادری یست مرا و تو نیز میدانی</p></div>
<div class="m2"><p>که هست پیشم از جان عزیزتر از یار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ترا سپارم و لازم بود سپردن جان</p></div>
<div class="m2"><p>گهی که حادثه بر مرد نیک گیرد کار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روا مدار که بعد از سپردن جان هم</p></div>
<div class="m2"><p>اسیر محنت باشم ز گنبد دوار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>الا که تا بهم آمیخته است شادی و غم</p></div>
<div class="m2"><p>الا که تا زپی هم بود خزان و بهار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>موافقت را لب ها زخنده باز چو گل</p></div>
<div class="m2"><p>مخالفت را بر فرق دست همچو چنار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مدار مرکز عالم تو باش تا باشد</p></div>
<div class="m2"><p>فراز چرخ مدار ثوابت و سیار</p></div></div>