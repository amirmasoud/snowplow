---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>شد چنان گرم جهان ز آمدن تابستان</p></div>
<div class="m2"><p>که رسد عاشق از گرمی معشوق به جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راست چون دانه که برتابه گرم اندازی</p></div>
<div class="m2"><p>برجهد هر دم از روی زمین کوه گران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کسی نسبت خورشید به معشوق کند</p></div>
<div class="m2"><p>همه عمر شود عاشق از دور و گردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا برون آید کانون هوا گرمی خور</p></div>
<div class="m2"><p>شعله را روی سیه گردد مانند دخان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چنین کاب شده کرم عجب نبود اگر</p></div>
<div class="m2"><p>با سمندر نکند ماهی تبدیل مکان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرمی مهر رسید است به حدی که کنون</p></div>
<div class="m2"><p>می کند حربا چون شب پره زورخ پنهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فتیله که کسی بر سر داغی سوزد</p></div>
<div class="m2"><p>تیر می سوزد، از گرمی پیکان انسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلق را اکنون خاصیت ماهست درست</p></div>
<div class="m2"><p>که ز نزدیکی خورشید رسدشان نقصان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زند زآنند خلایق که ز گرمی هوا</p></div>
<div class="m2"><p>ملک الموت نیاید ز پی بردن جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شخص از گرمی استاده به یک پای چو شمع</p></div>
<div class="m2"><p>سایه اش بینی چون ماهی بر خاک طپان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خود غلط گفتم شد تافته زان گونه زمین</p></div>
<div class="m2"><p>که نمی افتد از بیم کنون سایه بر آن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست ممکن که نسوزد کسی از گرمی خور</p></div>
<div class="m2"><p>گر رود بر فلک هفتم هم چون کیوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس که شد تافته از آتش خورشید فلک</p></div>
<div class="m2"><p>وصفش اکنون بکبودی نبود جز بهتان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ساده لوحی که ندارد به فلک حرق روا</p></div>
<div class="m2"><p>تا دگر نارد بر دعوی باطل برهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گو بیا بنگر کز دیدن خورشید فلک</p></div>
<div class="m2"><p>راست آن بیند کز دیدن مهتاب کتان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر نه به گداختن موم بود حاجت کس</p></div>
<div class="m2"><p>در ته آتش ناچارش سازد پنهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دیدن اشیاء ممکن نبود مردم را</p></div>
<div class="m2"><p>زان که سوزد چو جدا گشت نگاه از مژگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سرو را گر سر به گریختن از بستان نیست</p></div>
<div class="m2"><p>از چه دایم به میان بر زد ...ـان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدر را این همه کاهیدن از آن است که او</p></div>
<div class="m2"><p>کرد خواهد پس ازین وقتی با مهر قران</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من ندانم که عناصر همه آتش شده اند</p></div>
<div class="m2"><p>یا گرفتند خود آن باقی ازین فصل کران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نه خطا کردم کز عدل شهنشاه رسل</p></div>
<div class="m2"><p>با همه ضدی یک رنگ شدستند ارکان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>احمد مرسل سلطان عرب شاه عجم</p></div>
<div class="m2"><p>شافع محشر ابوالقاسم امین یزدان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن که گر نسبت رایش به مه و مهر کنند</p></div>
<div class="m2"><p>هم چنان است که گویند یقین است گمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ذات مستغنی او دست نفرسوده به خط</p></div>
<div class="m2"><p>خط سیه پوش از آن رو شده چون مایمتان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه دانند که مقصود دو عالم او بود</p></div>
<div class="m2"><p>گر مقدم شده باشند به صورت چه زیان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بین که در برهان هستند مقدم طرفین</p></div>
<div class="m2"><p>با وجود یک نتیجه عرض است از برهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خیمه جایی زده در خطه امکان کزوی</p></div>
<div class="m2"><p>تا به سرحد و جوبست به قدر دو کمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رتبه جاه تو ای از همه عالم برتر</p></div>
<div class="m2"><p>هست چون کنه خدا از نظر عقل نهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>برتو می نازد فردوس برین پیوسته</p></div>
<div class="m2"><p>آری آری به مکین باشد خوبی امکان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه عجب گر تو زجبریل شدی محرم تر</p></div>
<div class="m2"><p>کی به مطلوب رسد قاصد پیغام رسان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در ازل منع تو بر روی زمان دست افشاند</p></div>
<div class="m2"><p>چون رسن تاب رود پس پس تا حشر زمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دارد آن قدرت عدل تو که گر فرماید</p></div>
<div class="m2"><p>چرخ زنجیر حوادث کند از کاهکشان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جاهلی گر نکند گوش به امرت چه شود</p></div>
<div class="m2"><p>بد به خود میکند از سجده نکردن شیطان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرکجا قد تو افکند بساط عظمت</p></div>
<div class="m2"><p>فکر بیچاره سودا زده بر چیدن دکان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خواستم نعل براق تو بگویم مه را</p></div>
<div class="m2"><p>خردم گفت مشو مرتکب این هذیان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کان گذر می کند از چرخ بیکدم چو خیال</p></div>
<div class="m2"><p>وین به یک ماه کند ضمن فلک را جولان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شب معراج فلک دیدش و تا حشر برو</p></div>
<div class="m2"><p>انجم و ماه نو انگشت بسوی دندان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>طبع چون خواهد تا سرعت سیرش گوید</p></div>
<div class="m2"><p>بر ورق بی مد دوست شود خامه روان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لاف مدحت نزنم گرچه یقین است که نیست</p></div>
<div class="m2"><p>الفی پیش تفاوت ز حسن تا حسّان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرچه بر خاک نیفکندی هرگز سایه</p></div>
<div class="m2"><p>سایه بر سرم انداز و ز خلقم برهان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا چنین است که در برج اسد دارد جا</p></div>
<div class="m2"><p>تا برون آید خورشید منیر .....ـان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هرکه سر از خط فرمان تو برمیدارد</p></div>
<div class="m2"><p>باد دایم همه گر چرخ بود سرگردان</p></div></div>