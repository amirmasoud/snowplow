---
title: >-
    شمارهٔ  ۹
---
# شمارهٔ  ۹

<div class="b" id="bn1"><div class="m1"><p>ای زخط بگرفته خورشید رخت در بر هلال</p></div>
<div class="m2"><p>جز تو کس را نیست خورشید از گل ار عنبر هلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خطت سر بر نزد رخساره ات را کس ندید</p></div>
<div class="m2"><p>کس نه بیند عید را هرگز مقدم بر هلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه آن خط سیه برطرف رویت دید گفت</p></div>
<div class="m2"><p>راست بودست اینکه می برداست نور از خور هلال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه خط عنبرینت برده دل از دست او</p></div>
<div class="m2"><p>از شفق به هر چه دارد نعل در آذر هلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عکس خط خویش را در چشم پرخونم نگر</p></div>
<div class="m2"><p>گر ندیدی در شفق ایماه سیمین بر هلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه افلاک بشکستی ز سنگ حادثات</p></div>
<div class="m2"><p>گرنه بگرفتی به طاق ابرویت ساغر هلال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب نیست از خنده تا کردم و بدان خط نسبتش</p></div>
<div class="m2"><p>ظاهراً کردست از من این سخن باور هلال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این که می ماند به خط مشک رنگ یار من</p></div>
<div class="m2"><p>زان کند باور که مغزش نیست اندر سر هلال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرتوی از روی تو یارای مخدوم ار نزد</p></div>
<div class="m2"><p>از چه رو از پرتو خورشید پیچد سر هلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن جوانمردی که هنگام سخایش خم گرفت</p></div>
<div class="m2"><p>هم ز بار سیم چرخ و هم و ز بار زر هلال</p></div></div>