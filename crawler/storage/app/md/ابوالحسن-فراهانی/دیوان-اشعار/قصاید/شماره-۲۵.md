---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>من آن چه می کشم از جور چرخ مینایی</p></div>
<div class="m2"><p>گمان مبر که بود چرخ را توانایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صنف صنف خلایق معاشرت کردم</p></div>
<div class="m2"><p>چه روستایی و چه شهری و چه صحرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو یار یک دل اگر در زمانه می بینم</p></div>
<div class="m2"><p>خدای را نپرستیده ام به یکتایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روزگار من آن می کشم که کس مکشاد</p></div>
<div class="m2"><p>به محض تهمت دانشوری و دانایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعوذ بالله اگر فضل و دانشم بودی</p></div>
<div class="m2"><p>فتاده بودی هر ذره ام به صحرایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر که می نگرم بی غمت نمی بینم</p></div>
<div class="m2"><p>وزین سبب شده بی قرار و رسوایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غم تو بود زخوبان که پای برجا بود</p></div>
<div class="m2"><p>به طالع من آن نیز کشت هرجایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه زخم خورده بود قطره های خون و بود</p></div>
<div class="m2"><p>گل و ریاحین در دیده تماشایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زعالم ملکوت به ملک می آرند</p></div>
<div class="m2"><p>برای بادیه گردی و باد پیمایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگو ز دیده که این قلزمیست خون آشام</p></div>
<div class="m2"><p>ز دل مپرس که آن کشتی است دریایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر مصاحبت خلق را ثمر این است</p></div>
<div class="m2"><p>من و مصاحبت خویش و کنج تنهایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو فرقه اند که نبود گذر زخدمتشان</p></div>
<div class="m2"><p>یکی شهان و دگر دلبران یغمایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اقتضای قضا صرف خوب رویان شد</p></div>
<div class="m2"><p>عزیز عمرم یعنی اوان برنایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهی ز روبی بودم نشسته در آتش</p></div>
<div class="m2"><p>گهی ز مویی آشفته حال و سودایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی ز گردش چشم بتان ساده زنخ</p></div>
<div class="m2"><p>خیال واربدم کوچه گرد و هر جایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همیشه در حرکت بودمی و مقصد نه</p></div>
<div class="m2"><p>چو آن سفینه که باشد ز موجه دریایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون که نوبت پیری ست نوکر شاهم</p></div>
<div class="m2"><p>کمال من نه همین شاعری و ملایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شراب خواره ام و بذله گو و ریش تراش</p></div>
<div class="m2"><p>هزار تیشه خور و هرزه گرد و هر جایی</p></div></div>