---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>مژده وصل تو در گوش و بنا بر عادت</p></div>
<div class="m2"><p>دیده اسباب شب هجر مهیا می‌کرد</p></div></div>