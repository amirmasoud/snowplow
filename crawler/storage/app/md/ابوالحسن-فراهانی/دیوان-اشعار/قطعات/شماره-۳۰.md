---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ای که چرخت ندیده است نظیر</p></div>
<div class="m2"><p>در سر چارسوی چار ارکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو دکان تازه بگشادی</p></div>
<div class="m2"><p>عالم پیر تازه گشت و جوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من اگر لب به وصف نگشادم</p></div>
<div class="m2"><p>نکته‌ای هست گوش دار بدان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تماشای این دکان که کند</p></div>
<div class="m2"><p>چشم را خیره عقل را حیران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد پیر برنمی‌دارد</p></div>
<div class="m2"><p>سر انگشت حیرت از دندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته ام در بدیهه تاریخش</p></div>
<div class="m2"><p>بر تو بادا مبارک این دکان</p></div></div>