---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>عزیزی گفت با من دوش کای سلطان سوداگر</p></div>
<div class="m2"><p>چرا با این قدر سامان به جنت متهم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو ماهی می کند جمع درم اما نمیداند</p></div>
<div class="m2"><p>که صید ماهیی جایز بود کانرا درم باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدو گفتم کریمش گرچه نتوان گفت البته</p></div>
<div class="m2"><p>ولی اطلاق جنت هم به یک معنی ستم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیس مطلقش گفتن نشاید زانکه گر او را</p></div>
<div class="m2"><p>بود با لذات بخلی بالغرض گاهی کرم باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مردم میرسد فیضش ولی چون گریه شادی</p></div>
<div class="m2"><p>پس از عمری که واقع می‌شود بسیار کم باشد</p></div></div>