---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>هر پاره ای فتاده به جایی ز جور یار</p></div>
<div class="m2"><p>چو لشگر شکسته دل پاره پاره ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل دامنم گرفت و ز غم شکوه می نمود</p></div>
<div class="m2"><p>کو جای من گرفته و من برکناره ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاری نساخت زاری من پیش دشمنان</p></div>
<div class="m2"><p>بیچاره من اگر نکند دوست چاره ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عشق گاه جان طلبی گاه دین و دل</p></div>
<div class="m2"><p>اینها زدیگری یست بگو من چکاره ام</p></div></div>