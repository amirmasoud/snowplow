---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>رغبت به استخوان کسی کم کند سگش</p></div>
<div class="m2"><p>ترسد که بو کند به غلط استخوان ما</p></div></div>