---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>میر مادر منزل مردم بسی گرمی ولی</p></div>
<div class="m2"><p>داری اندر منزل خود مشرب ورای دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر کنی در منزل خود نیز گرمی باک نیست</p></div>
<div class="m2"><p>چون نداریم از تو جز گرمی تمنای دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب عالمی می بایدت چون آفتاب</p></div>
<div class="m2"><p>گرمی اندر خانه جزو بیش از جای دگر</p></div></div>