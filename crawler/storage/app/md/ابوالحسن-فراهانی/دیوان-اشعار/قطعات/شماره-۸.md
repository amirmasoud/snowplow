---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>صاحبا شد مدتی تا شاهدان فکرتت</p></div>
<div class="m2"><p>از طلبکاران خود دارند رخ اندر نقاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شعر تو آب روان است و روانم تشنه است</p></div>
<div class="m2"><p>چون روا داری که باشد تشنه محروم آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک دوش از حامت گفت است طبع دوربین</p></div>
<div class="m2"><p>آن چنان عذری که نتوان رد آن در هیچ باب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت معنی های او بکرند و ما نامحرمان</p></div>
<div class="m2"><p>بکر از نامحرمان واجب شمار و اجتناب</p></div></div>