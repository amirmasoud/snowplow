---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ز فرموده اوستادان پیش</p></div>
<div class="m2"><p>شبی آمد این بیتم اندر نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسر کو ندارد نشان از پدر</p></div>
<div class="m2"><p>تو بیگانه خوانش مخوانش پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وزین بیت اندیشه ی دوربین</p></div>
<div class="m2"><p>بدین معنی نغز شد راهبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که دوران دو رنگ است و ابنای او</p></div>
<div class="m2"><p>ندارند از آن از دو رنگی گذر</p></div></div>