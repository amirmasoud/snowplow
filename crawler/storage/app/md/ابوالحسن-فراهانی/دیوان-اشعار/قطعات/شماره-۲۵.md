---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>عالم به غیب اگر نیست چون هر که را قرین شد</p></div>
<div class="m2"><p>نشنیده زونویسد هرچش گذشت در دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب سیه برآرد و از قعر بحر تیره</p></div>
<div class="m2"><p>در ثمین نماید چون آورد به ساحل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ضعف میبرندش بر دوش و این عجب تر</p></div>
<div class="m2"><p>گز یک قدم تواند رفتن ز چین به بابل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جمله جهان را چومیر جمله عالم</p></div>
<div class="m2"><p>بخشد نمی‌کند سر بالا ز شرم سایل</p></div></div>