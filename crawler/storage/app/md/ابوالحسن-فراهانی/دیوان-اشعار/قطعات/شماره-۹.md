---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>خواجه آمد از سفر رفتم به قصد دیدنش</p></div>
<div class="m2"><p>خادمان گفتند نزد خواجه کس را یار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز گردیدم به سوی کلبه خود منفعل</p></div>
<div class="m2"><p>هیچ محنت بر هنرمندان چنین دشوار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قضا بعد از دو روزی خواجه را دیدم به خواب</p></div>
<div class="m2"><p>تکیه کرده بر بساط و نزد او دیار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم خواب است رفتم پیش و بنشستم برش</p></div>
<div class="m2"><p>گفتمش دارم سوالی از تو پرسش عار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت بسم الله گفتم این تکبر از کجاست</p></div>
<div class="m2"><p>بر هنرمندان که قدر مال این مقدار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندرین بودم که از خوابم یکی بیدار کرد</p></div>
<div class="m2"><p>چشم بگشودم و گلی دیدم که در گلزار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم ای گل از کدامین گلستانی، گفت من</p></div>
<div class="m2"><p>از گلستانی که اندروی صبا را بار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من غلام خواجه ام سوغات او آورده ام</p></div>
<div class="m2"><p>زود تر بستان که نزد خواجه خدمتکاریست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قصه کوته ارمغان را داد و عقل دوربین</p></div>
<div class="m2"><p>کرد تمثیلی که عاقل را درو وانکار نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت دنیا دارو قاذورات یک جنسند ازانک</p></div>
<div class="m2"><p>دیدن ایشان به بیداران به جز آزار نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر خلاف این اگر بر خوابشان بیند کسی</p></div>
<div class="m2"><p>چون شود بیدار تعبیرش به جز دینار نیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دی به من از روی یاری گفت یاری کان فلان</p></div>
<div class="m2"><p>گویمت حرفی اگر بر خاطرت دشوار نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفتم از دشمن گران آید ولی از دوستان</p></div>
<div class="m2"><p>بر تن چون کاه من گر کوه باشد یار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت که ایران را کسی باشد در انواع هنر</p></div>
<div class="m2"><p>چو تویی امروز نبود و ربود بسیار نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از هنر قطع نظر کردم برای بزم می</p></div>
<div class="m2"><p>همنشینی چون تو زیر گنبد دوار نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاه از خانت گرفت و داد از روی کرم</p></div>
<div class="m2"><p>راه در بزمی که شاهان را در آنجا بار نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کاهلی در خدمت خود میکنی نشنیده ای</p></div>
<div class="m2"><p>انکه الّا کفر شاخ کاهلی را بار نست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفتمش تصدیع را ترک ادب دانسته ام</p></div>
<div class="m2"><p>ورنه هرگز بنده را از خدمت شه عار نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاه خورشید جهانگیر است و من مه در مهی</p></div>
<div class="m2"><p>اجتماع ماه با خورشید جز یک بار نیست</p></div></div>