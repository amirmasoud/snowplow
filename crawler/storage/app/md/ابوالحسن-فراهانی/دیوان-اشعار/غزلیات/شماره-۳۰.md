---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>دیوار و در آلوده به خون جگرم کرد</p></div>
<div class="m2"><p>هجران تو شرمنده دیوار و درم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از لذت زخم آن مژه محرومی ما خواست</p></div>
<div class="m2"><p>زان بیش که شمشیر زند بی خبرم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عکس رخت در نظرم اشک به خون شد</p></div>
<div class="m2"><p>آسوده ز آمیزش لخت جگرم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروانه صفت سوختم و شمع ندیدم</p></div>
<div class="m2"><p>خون در تن من شعلگی بال و پرم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز جفای تو زاندازه برون ست</p></div>
<div class="m2"><p>تأثیر دگر دشمن آه سحرم کرد</p></div></div>