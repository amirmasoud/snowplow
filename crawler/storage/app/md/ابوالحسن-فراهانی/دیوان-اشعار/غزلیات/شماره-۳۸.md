---
title: >-
    شمارهٔ  ۳۸
---
# شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>اگر سوسن صفت بودی زبانی در دهان گل</p></div>
<div class="m2"><p>کسی نشنیدی الا وصف رویش از زبان گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیدم در زمان او کسی را با دل خرم</p></div>
<div class="m2"><p>اگرچه از برای خرمی باشد زمان گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه از شوخی رودهر دم به گلزار دگر یارم</p></div>
<div class="m2"><p>حدیث بینوایی می کند خاطر نشان گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلستان جهان را در میان غنچه گل باشد</p></div>
<div class="m2"><p>گلستان رخ او غنچه دارد در میان گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تفاوت از زمین تا آسمانت ارتوانی دید</p></div>
<div class="m2"><p>میان آفتاب چهره ی یار و میان گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان را اعتباری نیست زآن روز بد و نیکش</p></div>
<div class="m2"><p>نه رو در هم کشم چون غنچه نه خندم به سان گل</p></div></div>