---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>ز گریه منع مکن دیده پر آب مرا</p></div>
<div class="m2"><p>که برطرف کنی از کشتن اضطراب مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بسوز پس از کشتنم نه سیمابم</p></div>
<div class="m2"><p>مگر به آب دهد کلبه خراب مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکفته دیدم گل های داغ و دانستم</p></div>
<div class="m2"><p>که سوی من نظری هست آفتاب مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیده منت شب زنده داری از چه کشم</p></div>
<div class="m2"><p>که غمزه ی تو به تاراج برد خواب مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گداخت پیکرم از جوش خون چه چاره کنم</p></div>
<div class="m2"><p>که شیشه تاب نمی آورد شراب مرا</p></div></div>