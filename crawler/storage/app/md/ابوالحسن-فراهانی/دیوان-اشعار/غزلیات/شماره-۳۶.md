---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>به پیش آتش آهم زبانهٔ آتش</p></div>
<div class="m2"><p>چنان بود که ز آتش زبانهٔ آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دوریت چو کشم آه بیشتر سوزم</p></div>
<div class="m2"><p>بلی نسیم بود تازیانهٔ آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درون تربت من چیست غیر خاکستر</p></div>
<div class="m2"><p>جز این متاع چه خواهی ز خانهٔ آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیان درد دل خود بهر که کردم، سوخت</p></div>
<div class="m2"><p>مگر زبان من آمد زبانهٔ آتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان به سوختن اندر غم تو خو کردم</p></div>
<div class="m2"><p>که دور از آتشم اندر میانهٔ آتش</p></div></div>