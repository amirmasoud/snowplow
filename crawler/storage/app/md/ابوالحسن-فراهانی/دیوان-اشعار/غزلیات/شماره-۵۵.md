---
title: >-
    شمارهٔ  ۵۵
---
# شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>تا کی کنی آزار من زار شکسته</p></div>
<div class="m2"><p>آزردگی ای هست در آزار شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهوده چه رنجم که زمن زود گذشتی</p></div>
<div class="m2"><p>زودی گذرند از بر دیوار شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسان نرود محنت هجران تو از دل</p></div>
<div class="m2"><p>بیرون نرود زود ز ناچار شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان از کف عشاق پریشان نستانی</p></div>
<div class="m2"><p>تا خون نکنی دل چو خریدار شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحم است به دل ها که تو را بس که غیوری</p></div>
<div class="m2"><p>قانع نتوان کرد به آزار شکسته</p></div></div>