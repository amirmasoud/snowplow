---
title: >-
    شمارهٔ  ۳۵
---
# شمارهٔ  ۳۵

<div class="b" id="bn1"><div class="m1"><p>بحمدالله که در قتلم تعلل کرد گیسویش</p></div>
<div class="m2"><p>به خون من نشد آلوده دیوار و در کویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب خود را به زلفش می کنم نسبت وزین غافل</p></div>
<div class="m2"><p>که روزی همچو رویش دارد از پی زلف هندویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قصد کشتنم ترکان مژگانش صف اندر صف</p></div>
<div class="m2"><p>به چشمش اقتدا کردند در محراب ابرویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب نبود اگر از جلوه بر چشمم نمک پاشد</p></div>
<div class="m2"><p>به آب دیده پروردم نهال قد دلجویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زلفش کی دهم دل گرنه رویش در میان بینم</p></div>
<div class="m2"><p>دل من می برد زلفش به جانب داری رویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر یزای دیده اشک و خاک کویش را به حالش کن</p></div>
<div class="m2"><p>که از خونم بسی بهتر بود خاک سر کویش</p></div></div>