---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>ز ضعف تن مژه‌ام را به هم رسیدن نیست</p></div>
<div class="m2"><p>نبستن مژه از مرده به هم دیدن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشم به سنگدلی‌های او که درد مرا</p></div>
<div class="m2"><p>دل ار نه سنگ بود، طاقت شنیدن نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا جفای تو پابسته‌تر کند، آری</p></div>
<div class="m2"><p>چو پر بسوزد پروانه را پریدن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خون تپیدن بسمل، یقین نمود مرا</p></div>
<div class="m2"><p>که بعد کشته شدن نیز آرمیدن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شد که از رخ او گل نچیده‌ام کان گل</p></div>
<div class="m2"><p>برای زینت باغ است، بهر چیدن نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تأسفت که بر روزگار رفته خورد</p></div>
<div class="m2"><p>برای کشته شدن صید را تپیدن نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه آه کشم، کآنچنان گرفتارم</p></div>
<div class="m2"><p>به دست غم که مجال نفس کشیدن نیست</p></div></div>