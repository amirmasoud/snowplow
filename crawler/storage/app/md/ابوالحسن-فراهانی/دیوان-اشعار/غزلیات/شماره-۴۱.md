---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>ما پای در گل از دل دیوانهٔ خودیم</p></div>
<div class="m2"><p>ما غرق خون ز چشم سیه خانهٔ خودیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلخن نمود بر سرما اشک ما خراب</p></div>
<div class="m2"><p>پیوسته خود خراب کن خانهٔ خودیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون جگر خوریم و نگیریم می ز کس</p></div>
<div class="m2"><p>یعنی همیشه مست ز پیمانهٔ خودیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمری گذشت و شکوه زلفش نشد تمام</p></div>
<div class="m2"><p>در حیرت از درازی افسانهٔ خودیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق تو را که بیند از آشنایی‌ست</p></div>
<div class="m2"><p>ما ای حسن به هرزه نه بیگانهٔ خودیم</p></div></div>