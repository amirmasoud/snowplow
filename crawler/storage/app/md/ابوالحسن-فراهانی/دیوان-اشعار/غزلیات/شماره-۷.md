---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>چون گرم گریه کردم چشم گهرفشان را</p></div>
<div class="m2"><p>انداختم به ساحل چون موج آسمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم زننگ ننهد دیگر بر آستان پا</p></div>
<div class="m2"><p>ورنه به بوسه زحمت میدادم آستان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا زودتر بسوزی ای برق آه او را</p></div>
<div class="m2"><p>چون عندلیب از خس میسازم آشیان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بودی از سگانش امید التفاتی</p></div>
<div class="m2"><p>کی بود تاب بودن در سینه استخوان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان را نبود قوت کز سینه تا لب آید</p></div>
<div class="m2"><p>از چاک سینه صد در بررخ گشوده جان را</p></div></div>