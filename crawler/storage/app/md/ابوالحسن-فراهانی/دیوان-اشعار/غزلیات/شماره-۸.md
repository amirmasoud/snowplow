---
title: >-
    شمارهٔ  ۸
---
# شمارهٔ  ۸

<div class="b" id="bn1"><div class="m1"><p>سوختم ترسم که رویش دیده باشد بی نقاب</p></div>
<div class="m2"><p>زآن که می بینم که تابی هست اندر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه عمرم صرف قید و بند شد اما نبود</p></div>
<div class="m2"><p>هیچ بندی بر دل من بار چون بند نقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تار زلفش مانع وصل دلم شد از رخش</p></div>
<div class="m2"><p>تار مویی در میان این و دانش شد حجاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت در خوابم توانی دید گفتم خواب کو</p></div>
<div class="m2"><p>ور بود کوبخت بیداری که بیندت به خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نشستی یک نفس بنشین که من در عمر خود</p></div>
<div class="m2"><p>روز را امروزی می‌بینم که بنشست آفتاب</p></div></div>