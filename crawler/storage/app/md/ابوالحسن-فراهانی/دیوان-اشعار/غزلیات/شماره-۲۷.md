---
title: >-
    شمارهٔ  ۲۷
---
# شمارهٔ  ۲۷

<div class="b" id="bn1"><div class="m1"><p>دوزم شکاف سینه چو دل جلوه گاه کرد</p></div>
<div class="m2"><p>خود هم به روز رشک نیارم نگاه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خو به آه کرده بنوعی که روز وصل</p></div>
<div class="m2"><p>هرچند خواست در دلی گوید آه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای برق آه، تیرگی از روز ما مبر</p></div>
<div class="m2"><p>روزیست این که چشم سیاهش سیاه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم در آرزویت و ترسم که روز حشر</p></div>
<div class="m2"><p>باید زبیم خوی تو ضبط نگاه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم نظر به بندم و از گریه بس کنم</p></div>
<div class="m2"><p>از چاک های سینه خونابه راه کرد</p></div></div>