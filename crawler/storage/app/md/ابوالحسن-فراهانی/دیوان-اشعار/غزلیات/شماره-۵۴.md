---
title: >-
    شمارهٔ  ۵۴
---
# شمارهٔ  ۵۴

<div class="b" id="bn1"><div class="m1"><p>تا به گلشن رفته ای بلبل به فریاد آمده</p></div>
<div class="m2"><p>که آن که گل را بی وفایی می دهد یاد آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو را از بندگی سرو قدت آزاد کرد</p></div>
<div class="m2"><p>در چمن زان رو خطابش سرو آزاد آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلسله از بهر داد آویختندی پیش ازین</p></div>
<div class="m2"><p>زلف او را سلسله از بهر بیداد آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کشد عشق انتقام عاشق از هرکس که نیست</p></div>
<div class="m2"><p>شاید این قصه ی پرویز و فرهاد آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مژدهٔ قتلم مگر آورده قاصد از برش</p></div>
<div class="m2"><p>زآن که غمگین رفت از پیش من و شاد آمده</p></div></div>