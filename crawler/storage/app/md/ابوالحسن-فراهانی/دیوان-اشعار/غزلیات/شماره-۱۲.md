---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>تار زلفت گر چو بخت تار با ما یار نیست</p></div>
<div class="m2"><p>یک گره کمتر به دور افکن دلم دشوار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میکنی دانسته گرمی تا بسوزم سینه را</p></div>
<div class="m2"><p>گرمی خورشید بهر سوختن در کار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کجا و آرزوی سایه ی دیوار او</p></div>
<div class="m2"><p>آفتاب عالم آرا مرا در آنجا بار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که گویی یار هست اندر پی آزار تو</p></div>
<div class="m2"><p>منت آزار بر جان است اگر بیزار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خار مژگان بسته بر گل‌های اشکم راه را</p></div>
<div class="m2"><p>صبر باشد چاره‌ام چون هیچ گل بی‌خار نیست</p></div></div>