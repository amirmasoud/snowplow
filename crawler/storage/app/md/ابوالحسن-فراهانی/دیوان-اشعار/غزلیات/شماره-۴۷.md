---
title: >-
    شمارهٔ  ۴۷
---
# شمارهٔ  ۴۷

<div class="b" id="bn1"><div class="m1"><p>یا رضای دوست باید یا رضای خویشتن</p></div>
<div class="m2"><p>آشنای او نیاید آشنای خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتشم بی سوختن چون زندگانی می کنم</p></div>
<div class="m2"><p>تا نسوزم برنمی خیزم ز جای خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من سزای آتش وز دیده آبم برکنار</p></div>
<div class="m2"><p>در کنار خود نمی بینم سزای خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرنه در آینه خود را دیده زنجیر زلف</p></div>
<div class="m2"><p>از چه رو می افکنی هردم به پای خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو دل خون کردم و از دیده بیرون ریختم</p></div>
<div class="m2"><p>عاقبت از دل گرفتم خون بهای خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتمش دردل و را گفت از خدا شرمی بدار</p></div>
<div class="m2"><p>کس در آتش چون رود هردم به پای خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که می‌گویی چرا بر خود نمی‌سوزد دلت</p></div>
<div class="m2"><p>آتشم، آتش نمی‌سوزد برای خویشتن</p></div></div>