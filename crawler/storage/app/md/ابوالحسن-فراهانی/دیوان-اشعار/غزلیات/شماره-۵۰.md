---
title: >-
    شمارهٔ  ۵۰
---
# شمارهٔ  ۵۰

<div class="b" id="bn1"><div class="m1"><p>من گرفتم آفتاب از چارسو آید برون</p></div>
<div class="m2"><p>روزکی گردد شب ماگرنه او آید برون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زرد رویی ها کشید از رویش امروز آفتاب</p></div>
<div class="m2"><p>من نمیدانم که فردا با چه رو آید برون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که زلفش بر زبان می آورم نزدیک شد</p></div>
<div class="m2"><p>کز زبانم چون زبان شانه مو آید برون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی زلفش را شنید از باد اینک برگ گل</p></div>
<div class="m2"><p>با صبا از باغ بهر جستجو آید برون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو را نقصی یست عاشق شو که کامل می شوی</p></div>
<div class="m2"><p>گرچه بد باشد طلا زآتش نکو آید برون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توانم ساخت با ناسازگاری های چرخ</p></div>
<div class="m2"><p>کو کسی کز عهده آن تندخو آید برون</p></div></div>