---
title: >-
    شمارهٔ  ۳۱
---
# شمارهٔ  ۳۱

<div class="b" id="bn1"><div class="m1"><p>دردا که یار بر سر لطف نهان نماند</p></div>
<div class="m2"><p>نامهربان دو روز به ما مهربان نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرمنده ی سگان ویم، بعد مرگ هم</p></div>
<div class="m2"><p>کز سوز سینه در تن من استخوان نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اکنون کشید تیغ که در آستان او</p></div>
<div class="m2"><p>دیگر برای روح شهیدان مکان نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس به باغ برد صبا عطر بهر گل</p></div>
<div class="m2"><p>خاکم به سر که خاک در آن آستان نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او خود به اختیار کی این لطف می نمود</p></div>
<div class="m2"><p>تیرش ز جذبه دل ما در کمان نماند</p></div></div>