---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>بخت تارم سایه ای گر بر شب تار افکند</p></div>
<div class="m2"><p>تا قیامت خور نقاب شب ز رخسار افکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبلم اما نصیبم این که بعد از مرگ هم</p></div>
<div class="m2"><p>باد نتواند که خاک من به گلزار افکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی خون آید ازین وادی برو ای بی خبر</p></div>
<div class="m2"><p>کاروان خواب کی در چشم ما بار افکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هجر شمعی سوخت جانم را که گر بر آفتاب</p></div>
<div class="m2"><p>در فرو بند درخش خود را از دیوار افکند</p></div></div>