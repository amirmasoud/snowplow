---
title: >-
    شمارهٔ  ۲۷
---
# شمارهٔ  ۲۷

<div class="b" id="bn1"><div class="m1"><p>از تو ممنونم اگر از مژه خون می‌ریزم</p></div>
<div class="m2"><p>گر غمت نبود خون این همه چون می‌ریزم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورده‌ام زخمی و تا گم نکند صیادم</p></div>
<div class="m2"><p>هر قدم قطره از خون درون می‌ریزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبر کو تا جگرم خون شود و گریه کنم</p></div>
<div class="m2"><p>لخت‌لختش ز ره دیده برون می‌ریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده مشغول خیال است از آن امشب خون</p></div>
<div class="m2"><p>از شکاف دل بی‌صبر و سکون می‌ریزم</p></div></div>