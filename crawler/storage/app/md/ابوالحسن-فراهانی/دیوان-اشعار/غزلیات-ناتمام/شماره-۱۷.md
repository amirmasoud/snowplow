---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>دل ترک عشق آن بت دلجو نمی‌کند</p></div>
<div class="m2"><p>من ترک عشق می‌کنم و او نمی‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دیده‌ام نشین نفسی زآن که باغبان</p></div>
<div class="m2"><p>بی‌سرو لذتی ز لب جو نمی‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مایل به دیگران شود از منع من، بلی</p></div>
<div class="m2"><p>بی‌یاد نخل میل به هرسو نمی‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌عشق حسن را نبود قدر و قیمتی</p></div>
<div class="m2"><p>خار از گلی به است، کو کس بو نمی‌کند</p></div></div>