---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>چاره داغ گرفتم که به مرهم سازم</p></div>
<div class="m2"><p>غم دل را چه کنم، دل به چه خرّم سازم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده از نقش رخت پرگل از آن دردم مرگ</p></div>
<div class="m2"><p>دامن دیده نیارم که فراهم سازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که هر لحظه شکست دگرم پیش آمد</p></div>
<div class="m2"><p>صد مصیبت را یک حلقه ماتم سازم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه عادت شده در هجر توام ورنه مرا</p></div>
<div class="m2"><p>گریه نیست کز او درد دلی کم سازم</p></div></div>