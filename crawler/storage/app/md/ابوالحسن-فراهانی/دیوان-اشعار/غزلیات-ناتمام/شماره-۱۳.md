---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>اگر نبیند سوی من ساقی چه سود ار می دهد</p></div>
<div class="m2"><p>نشاء نظاره آن چشم را می کی دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مستش هر دمم مست از نگاهی می کند</p></div>
<div class="m2"><p>مست چون ساقی شود پیمانه پی در پی دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدتی شد کز ضمیرش رفته ام دشمن کجاست</p></div>
<div class="m2"><p>تا مرا بعد از فراموشی به یاد وی دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که گویی ناله کم کن لب به بندم من ولیک</p></div>
<div class="m2"><p>چون کنم با آن که هربندم نوای نی دهد</p></div></div>