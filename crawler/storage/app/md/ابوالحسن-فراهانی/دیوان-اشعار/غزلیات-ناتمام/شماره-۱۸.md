---
title: >-
    شمارهٔ  ۱۸
---
# شمارهٔ  ۱۸

<div class="b" id="bn1"><div class="m1"><p>آن که بی پرواییش هردم مرا رسوا کند</p></div>
<div class="m2"><p>کاش از رسوایی خود اندکی پروا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای آن که سوزد دوست را در پیش غیر</p></div>
<div class="m2"><p>شمع هم خود را وهم پروانه را رسوا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من به او مشغول و او با دیگران گرم سخن</p></div>
<div class="m2"><p>چون تهیدستی که با پرمایه سودا کند</p></div></div>