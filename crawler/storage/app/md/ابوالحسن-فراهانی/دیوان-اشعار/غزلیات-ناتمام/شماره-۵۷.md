---
title: >-
    شمارهٔ  ۵۷
---
# شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>ای دل من و آزادی ازین زمزمه بس کن</p></div>
<div class="m2"><p>اندیشه یی از طعنه ی مرغان قفس کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان منم و نیم نفس بی هده مخروش</p></div>
<div class="m2"><p>محتاج به آهم، نکنی ضبط نفس کن</p></div></div>