---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>عقل می گوید بحرف عشق ترک دیدن مکن</p></div>
<div class="m2"><p>عشق میگوید که حرف عقل را تمکین مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب بهتان است بر عشاق ای همدم مرا</p></div>
<div class="m2"><p>چون نهی در خاک از خشت لحد بالین مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که داغم می نهی بر سینه دل را چاره کن</p></div>
<div class="m2"><p>از قفس آزاد کن بلبل قفس رنگین مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریه میگوید مکن وانگه دلم خون می کند</p></div>
<div class="m2"><p>قدرتی کوتا بگویم آن بکن با این مکن</p></div></div>