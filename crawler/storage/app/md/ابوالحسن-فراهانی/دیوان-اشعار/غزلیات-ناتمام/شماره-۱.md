---
title: >-
    شمارهٔ  ۱
---
# شمارهٔ  ۱

<div class="b" id="bn1"><div class="m1"><p>منع سودی نکند کاش نصیحت‌گر ما</p></div>
<div class="m2"><p>گر تواند ببرد تیرگی از اختر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو مگر در دل ماهی که چنین می‌گردند</p></div>
<div class="m2"><p>ماه و خورشید چو پروانه به گرد سر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در شکست دل ما پرهیزی نیست به لاف</p></div>
<div class="m2"><p>ما حبابیم، نسیمی شکند ساغر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما از آن سوختگانیم که بزداید چرخ</p></div>
<div class="m2"><p>زنگ از آیینه ماه به خاکستر ما</p></div></div>