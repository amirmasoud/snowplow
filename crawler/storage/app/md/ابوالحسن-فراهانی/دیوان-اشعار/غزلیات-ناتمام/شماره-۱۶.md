---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>بس که در کوی تو چشمم گریه بسیار کرد</p></div>
<div class="m2"><p>خون دل دیدم روان چندان که در دل کار کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو بهر جانب نهادم راه بر من بسته شد</p></div>
<div class="m2"><p>ضعف پنداری هوا را در رهم دیوار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بیمارش ز خون خواری بپرهیز و بلی</p></div>
<div class="m2"><p>طول بیماری برو پرهیز را دشوار کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان سپردم از نگاه گرم او بر بوالهوس</p></div>
<div class="m2"><p>ناوکش بر صید دیگر خورد و بر من کار کرد</p></div></div>