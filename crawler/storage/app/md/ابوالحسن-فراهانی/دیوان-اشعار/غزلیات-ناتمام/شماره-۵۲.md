---
title: >-
    شمارهٔ  ۵۲
---
# شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>چون خون خورم بناله، میلم زیاده باشد</p></div>
<div class="m2"><p>بی نغمه خوش نباشد، جایی که باده باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست دل بناخن کندم تمام سینه</p></div>
<div class="m2"><p>چون خانه ای که در وی آتش فتاده باشد</p></div></div>