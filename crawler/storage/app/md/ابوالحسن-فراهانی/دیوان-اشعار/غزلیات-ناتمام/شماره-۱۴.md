---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>دل همان غمناک و شد در عشق چشم من سفید</p></div>
<div class="m2"><p>خانه تاریک است و از مهر رخش روزن سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که هردم مینهم بر چشم گریان نامه ات</p></div>
<div class="m2"><p>نامه ات ترسم شود آخر چو چشم من سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه گل گردد سفید از آفتاب اما ز شرم</p></div>
<div class="m2"><p>گر ترا بیند بخواهد کشت در گلشن سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامد از بخت سیه کاری تو کردی تیغ ناز</p></div>
<div class="m2"><p>سرخ از خونم که بادا رویت ای دشمن سفید</p></div></div>