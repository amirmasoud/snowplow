---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>به گریه چشم تهی کی کند دل ما را</p></div>
<div class="m2"><p>تهی به گریه نکردست ابر دریا را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبان گریه نمی دانم، این قدر دانم</p></div>
<div class="m2"><p>که قطره قطره تهی کرده ام دو دریا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراق روی عزیزان مرا به جان آورد</p></div>
<div class="m2"><p>فراق صعب بود خاصه ناشکیبا را</p></div></div>