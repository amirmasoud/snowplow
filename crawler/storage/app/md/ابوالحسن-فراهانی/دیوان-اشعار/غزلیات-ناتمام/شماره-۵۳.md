---
title: >-
    شمارهٔ  ۵۳
---
# شمارهٔ  ۵۳

<div class="b" id="bn1"><div class="m1"><p>عمریست که دل راه به دلدار ندارد</p></div>
<div class="m2"><p>بار از دلم و دل خبر از یار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز کسی در پی دلجویی من نیست</p></div>
<div class="m2"><p>این شیشه شکسته است خریدار ندارد</p></div></div>