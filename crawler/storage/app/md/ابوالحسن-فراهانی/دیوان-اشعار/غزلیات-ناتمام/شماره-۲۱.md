---
title: >-
    شمارهٔ  ۲۱
---
# شمارهٔ  ۲۱

<div class="b" id="bn1"><div class="m1"><p>مرا دلی ست که هرگز ندیدم او را شاد</p></div>
<div class="m2"><p>دلی سیاه تر از بخت اهل استعداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خاک تیره هنرها نشانده اند مرا</p></div>
<div class="m2"><p>مرا ز دست هنرهای خویشتن فریاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازین چه سود که قدم کلید وار خمید</p></div>
<div class="m2"><p>که بخت هرگز در روی من دری نگشاد</p></div></div>