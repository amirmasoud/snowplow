---
title: >-
    شمارهٔ  ۶۰
---
# شمارهٔ  ۶۰

<div class="b" id="bn1"><div class="m1"><p>گرفتارم میان چین زلف و چین ابرویی</p></div>
<div class="m2"><p>چون آن کشتی که هردم می رباید بادش از سویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به معنی برده ام پی نیستم پروانه و بلبل</p></div>
<div class="m2"><p>که گاهی سوزم از رنگی و گاهی نالم از بویی</p></div></div>