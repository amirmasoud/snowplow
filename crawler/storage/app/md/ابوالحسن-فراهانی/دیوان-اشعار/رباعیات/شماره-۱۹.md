---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>گویند کسان جمله چه هشیار و چه مست</p></div>
<div class="m2"><p>پیوستن شیشه نیست ممکن چوشکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت دارم از شیشه دل که چرا</p></div>
<div class="m2"><p>هرچند شکست باز با او پیوست</p></div></div>