---
title: >-
    شمارهٔ  ۱۵۵
---
# شمارهٔ  ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>باشد به مثل گر حاتم طی</p></div>
<div class="m2"><p>آن طبع که سرکشست کی گیرد کی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگاه که ابر کرمش ریزش کرد</p></div>
<div class="m2"><p>بنگر که چگونه می جهد برق ازوی</p></div></div>