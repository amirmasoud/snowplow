---
title: >-
    شمارهٔ  ۵۸
---
# شمارهٔ  ۵۸

<div class="b" id="bn1"><div class="m1"><p>آن چشم که خون خلق در خواب خورد</p></div>
<div class="m2"><p>کی سیر ز خون دل احباب خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون خوردن چشم های خواب آلودش</p></div>
<div class="m2"><p>آبی باشد که تشنه در خواب خورد</p></div></div>