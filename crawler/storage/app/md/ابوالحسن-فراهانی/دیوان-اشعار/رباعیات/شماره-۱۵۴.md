---
title: >-
    شمارهٔ  ۱۵۴
---
# شمارهٔ  ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>رحمت آید با همه مغروری تو</p></div>
<div class="m2"><p>گر شرح دهم قصه مهجوری تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اشکی که ز رخساره من بگذشتی</p></div>
<div class="m2"><p>اکنون ز سرم گذشته از دوری تو</p></div></div>