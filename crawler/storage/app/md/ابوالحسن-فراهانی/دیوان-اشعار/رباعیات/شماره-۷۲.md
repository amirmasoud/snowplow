---
title: >-
    شمارهٔ  ۷۲
---
# شمارهٔ  ۷۲

<div class="b" id="bn1"><div class="m1"><p>شوخی که جفا به از وفا میداند</p></div>
<div class="m2"><p>گویند که حال دل ما میداند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از دلش این گمان ندارم دیگر</p></div>
<div class="m2"><p>سرّ دل هر بنده خدا میداند</p></div></div>