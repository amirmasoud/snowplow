---
title: >-
    شمارهٔ  ۱۰۳
---
# شمارهٔ  ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>رفتن نتوان به کوی آن کافر کیش</p></div>
<div class="m2"><p>از بس که گلست ره به خون دل خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری مثل ست این که هر شخصی را</p></div>
<div class="m2"><p>هرچیز که در دل است می آید پیش</p></div></div>