---
title: >-
    شمارهٔ  ۱۴۹
---
# شمارهٔ  ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>زین سلسله تابه حال این فرزانه</p></div>
<div class="m2"><p>بکری ننشسته بود در کاشانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حیرت دارم که بکر فکر تو چرا</p></div>
<div class="m2"><p>هرگز ننهند قدم برون از خانه</p></div></div>