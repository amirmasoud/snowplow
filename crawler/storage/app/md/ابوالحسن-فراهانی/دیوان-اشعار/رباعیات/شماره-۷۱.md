---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>هرچند که عمر مایه ناز آمد</p></div>
<div class="m2"><p>از عمر عزیز یار ممتاز آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عمر گذشت برنمیگردد یار</p></div>
<div class="m2"><p>صد بار گذشت بر من و باز آمد</p></div></div>