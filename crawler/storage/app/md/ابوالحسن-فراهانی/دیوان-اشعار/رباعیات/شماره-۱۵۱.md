---
title: >-
    شمارهٔ  ۱۵۱
---
# شمارهٔ  ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>چون دل بستم به زلف آن غالیه مو</p></div>
<div class="m2"><p>بگشود و برم گشود از تندی خو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>استغنا را ببین که آخر دل را</p></div>
<div class="m2"><p>نتوانستم بست به زنجیر برو</p></div></div>