---
title: >-
    شمارهٔ  ۲۴
---
# شمارهٔ  ۲۴

<div class="b" id="bn1"><div class="m1"><p>آن غنچه که عالمی ازو در تاب است</p></div>
<div class="m2"><p>در گلشن یزد مثل او نایاب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پژمرده بود غنچه که بی آب بود</p></div>
<div class="m2"><p>این نادره غنچه تازه بی آب است</p></div></div>