---
title: >-
    شمارهٔ  ۱۱۹
---
# شمارهٔ  ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>چون مهر سفر به هفت کشور کردم</p></div>
<div class="m2"><p>رو زان تا شب ز خاک بستر کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب ها تا روز خاک بر سر کردم</p></div>
<div class="m2"><p>تا سجده آستان دلبر کردم</p></div></div>