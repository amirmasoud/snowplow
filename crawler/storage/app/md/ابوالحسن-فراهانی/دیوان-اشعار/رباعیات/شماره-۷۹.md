---
title: >-
    شمارهٔ  ۷۹
---
# شمارهٔ  ۷۹

<div class="b" id="bn1"><div class="m1"><p>ای آن که تو را میل به گلزار بود</p></div>
<div class="m2"><p>دانی که چرا گل همه رخسار بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعنی که مرا مبو که معشوقان را</p></div>
<div class="m2"><p>از عاشق خویش روی در کار بود</p></div></div>