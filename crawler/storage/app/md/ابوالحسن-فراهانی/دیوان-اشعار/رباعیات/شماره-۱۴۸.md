---
title: >-
    شمارهٔ  ۱۴۸
---
# شمارهٔ  ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>گر دل خواهی ای تن محنت پیشه</p></div>
<div class="m2"><p>مگذار دل شکسته بی اندیشه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شیشه شکسته گشت پا نگذارند</p></div>
<div class="m2"><p>دیگر نتوان ساختن از وی شیشه</p></div></div>