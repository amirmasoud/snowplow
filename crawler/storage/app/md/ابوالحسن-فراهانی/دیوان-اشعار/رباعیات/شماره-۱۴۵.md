---
title: >-
    شمارهٔ  ۱۴۵
---
# شمارهٔ  ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>در دل دو هزار مدّعا دارم من</p></div>
<div class="m2"><p>زان است که پیوسته جفا دارم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان زود شکسته می شود شیشه دل</p></div>
<div class="m2"><p>کورا چو حباب بر هوا دارم من</p></div></div>