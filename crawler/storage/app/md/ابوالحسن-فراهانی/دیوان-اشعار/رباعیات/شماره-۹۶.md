---
title: >-
    شمارهٔ  ۹۶
---
# شمارهٔ  ۹۶

<div class="b" id="bn1"><div class="m1"><p>یک ذره ز خاک پای آن ماه طراز</p></div>
<div class="m2"><p>گر زان که به دستت افتد ای محرم راز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم تر انداز که پیش از من و تو</p></div>
<div class="m2"><p>گفتند نکویی کن و در آب انداز</p></div></div>