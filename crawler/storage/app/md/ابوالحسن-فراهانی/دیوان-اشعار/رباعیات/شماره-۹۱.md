---
title: >-
    شمارهٔ  ۹۱
---
# شمارهٔ  ۹۱

<div class="b" id="bn1"><div class="m1"><p>در دام تو هر دم کشم آزار دگر</p></div>
<div class="m2"><p>ای کاش کند در قفسم بار دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دام قفس به که نخواهم دیدن</p></div>
<div class="m2"><p>در پهلوی خویشتن گرفتار دگر</p></div></div>