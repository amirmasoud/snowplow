---
title: >-
    شمارهٔ  ۳
---
# شمارهٔ  ۳

<div class="b" id="bn1"><div class="m1"><p>جانا از رشک می سپارم جان را</p></div>
<div class="m2"><p>درمان این است درد بی درمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی زچه در عشق تو خون شد جگرم</p></div>
<div class="m2"><p>بسیار فشردم به جگر دندان را</p></div></div>