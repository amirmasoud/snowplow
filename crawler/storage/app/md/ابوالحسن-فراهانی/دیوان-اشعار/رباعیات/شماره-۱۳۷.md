---
title: >-
    شمارهٔ  ۱۳۷
---
# شمارهٔ  ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>هرچند کیمیاست امروز سخن</p></div>
<div class="m2"><p>هستیم هنوز اهل معنی دوسه تن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دزدی شعریم شب و روز همه</p></div>
<div class="m2"><p>من میدزدم ز چرخ و ایشان از من</p></div></div>