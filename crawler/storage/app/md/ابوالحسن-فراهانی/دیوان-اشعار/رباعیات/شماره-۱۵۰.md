---
title: >-
    شمارهٔ  ۱۵۰
---
# شمارهٔ  ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>بی شمع جمالت ای به حسن افسانه</p></div>
<div class="m2"><p>روشن نشود ز آفتابم خانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آری ز فروغ شمع خاور همه را</p></div>
<div class="m2"><p>روزاست ولی شب است بر پروانه</p></div></div>