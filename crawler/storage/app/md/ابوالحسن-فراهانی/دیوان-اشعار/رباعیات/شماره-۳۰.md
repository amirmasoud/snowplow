---
title: >-
    شمارهٔ  ۳۰
---
# شمارهٔ  ۳۰

<div class="b" id="bn1"><div class="m1"><p>امشب که رخش خانه فروز من و تست</p></div>
<div class="m2"><p>خوش باش ای دل که وقت سوز من و تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشسته و جز شمع کسی به پیشش نیست</p></div>
<div class="m2"><p>پروانه بیا بیا که روز من و تست</p></div></div>