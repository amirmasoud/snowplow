---
title: >-
    شمارهٔ  ۱۲۳
---
# شمارهٔ  ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>گر بینم یار وگر نبینم میرم</p></div>
<div class="m2"><p>هر شیوه که در عشق گزینم میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار آتش و من شعله اگر از بر او</p></div>
<div class="m2"><p>خیزم سوزم وگر نشینم میرم</p></div></div>