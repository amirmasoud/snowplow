---
title: >-
    شمارهٔ  ۷۵
---
# شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>تا جان تو عزم رفتن از تن نکند</p></div>
<div class="m2"><p>در سینه خیال یار مسکن نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا شب نشود روز تو عاشق نشوی</p></div>
<div class="m2"><p>در روز کسی چراغ روشن نکند</p></div></div>