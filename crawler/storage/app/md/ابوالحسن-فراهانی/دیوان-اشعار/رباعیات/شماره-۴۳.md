---
title: >-
    شمارهٔ  ۴۳
---
# شمارهٔ  ۴۳

<div class="b" id="bn1"><div class="m1"><p>آن میر که خویش را کلامی می گفت</p></div>
<div class="m2"><p>درسی دو برای دوسه عامی می گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اعجاز ازو دیده ام از من بشنو</p></div>
<div class="m2"><p>در حاشیه درس شرح جامی میگفت</p></div></div>