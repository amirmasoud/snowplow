---
title: >-
    شمارهٔ  ۹۲
---
# شمارهٔ  ۹۲

<div class="b" id="bn1"><div class="m1"><p>اینک مشهد ای دل از غفلت کور</p></div>
<div class="m2"><p>اخلع نعلیک این چه عجب ست و غرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انصاف بده تو بهتر از موسایی</p></div>
<div class="m2"><p>یا روضه پاک طوس کمتر از طور</p></div></div>