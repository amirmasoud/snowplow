---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>زلف و رخ یار مبتلا داشت مرا</p></div>
<div class="m2"><p>هر یک مفتون خویش پنداشت مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر زلفش دراز دستی فرمود</p></div>
<div class="m2"><p>وز سایه به آفتاب بگذاشت مرا</p></div></div>