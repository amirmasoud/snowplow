---
title: >-
    شمارهٔ  ۱۱۴
---
# شمارهٔ  ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>دی توبه به امر دوستی بشکستم</p></div>
<div class="m2"><p>وامروز بتوبه کردن از غم رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون عضو شکسته ی که بد بسته شود</p></div>
<div class="m2"><p>بشکستم توبه را و از نو بستم</p></div></div>