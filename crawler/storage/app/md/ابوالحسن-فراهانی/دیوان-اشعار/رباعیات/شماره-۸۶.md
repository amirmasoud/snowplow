---
title: >-
    شمارهٔ  ۸۶
---
# شمارهٔ  ۸۶

<div class="b" id="bn1"><div class="m1"><p>اول طلب بخت بلندی باید</p></div>
<div class="m2"><p>وانگه ز لب تو نوش خندی باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بزم مرانم چو نشستی با غیر</p></div>
<div class="m2"><p>کای صحبت گرم را سپندی باید</p></div></div>