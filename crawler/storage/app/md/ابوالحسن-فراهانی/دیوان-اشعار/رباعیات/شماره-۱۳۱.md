---
title: >-
    شمارهٔ  ۱۳۱
---
# شمارهٔ  ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ای کوی تو درد و غم فراوان بردیم</p></div>
<div class="m2"><p>القصه که هرچه خواستیم آن بردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای سر زلف تو سود است همه</p></div>
<div class="m2"><p>یک دل دادیم و دل به دامان بردیم</p></div></div>