---
title: >-
    شمارهٔ  ۶۰
---
# شمارهٔ  ۶۰

<div class="b" id="bn1"><div class="m1"><p>چون بر رخت آن زلف پریشان لرزد</p></div>
<div class="m2"><p>در سینه ما دل طپد و جان لرزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز زلف سیه کار تو کس دید بگو</p></div>
<div class="m2"><p>کفری که چنین بر سرایمان لرزد</p></div></div>