---
title: >-
    شمارهٔ  ۴۷
---
# شمارهٔ  ۴۷

<div class="b" id="bn1"><div class="m1"><p>بی روی تو جان محنت اندوز مباد</p></div>
<div class="m2"><p>عالم بی آن شمع شب افروز مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو به روز ماند از نیکویی</p></div>
<div class="m2"><p>اما به روز من که آن روز مباد</p></div></div>