---
title: >-
    شمارهٔ  ۳۵
---
# شمارهٔ  ۳۵

<div class="b" id="bn1"><div class="m1"><p>ما را در دل جز آن گل رعنا نیست</p></div>
<div class="m2"><p>آن دل که این چنین بود از ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که دلم بگیرد از تنگی جا</p></div>
<div class="m2"><p>دلتنگیم از تنگی دل بی جا نیست</p></div></div>