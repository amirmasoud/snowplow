---
title: >-
    شمارهٔ  ۱۲۶
---
# شمارهٔ  ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>بر خاک کف پای تو چون رخ نالم</p></div>
<div class="m2"><p>ور پیرهنم نگنجم از بس بالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصل تو به بخت نیک هم نتوان یافت</p></div>
<div class="m2"><p>بیهوده ز بخت بدخود می نالم</p></div></div>