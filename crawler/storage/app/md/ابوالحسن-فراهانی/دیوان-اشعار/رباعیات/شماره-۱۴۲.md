---
title: >-
    شمارهٔ  ۱۴۲
---
# شمارهٔ  ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>شوخی که گسسته بود پیمان از من</p></div>
<div class="m2"><p>بنشست برم کشیده دامان از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برگ گلی که با صبا آمیزد</p></div>
<div class="m2"><p>هم با من بود و هم گریزان از من</p></div></div>