---
title: >-
    شمارهٔ  ۴
---
# شمارهٔ  ۴

<div class="b" id="bn1"><div class="m1"><p>با دیده بی خون که نه بینم آن را</p></div>
<div class="m2"><p>گر رسم بود بدی جگر خواران را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود عجبی رسم قدیم است که خلق</p></div>
<div class="m2"><p>دشمن دارند ابر بی باران را</p></div></div>