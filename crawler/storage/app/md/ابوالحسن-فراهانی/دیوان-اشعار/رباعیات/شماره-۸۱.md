---
title: >-
    شمارهٔ  ۸۱
---
# شمارهٔ  ۸۱

<div class="b" id="bn1"><div class="m1"><p>آن بی حاصل که وصل بگذاشته بود</p></div>
<div class="m2"><p>دوری از یار سهل پنداشته بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میرفت و دل شکسته با خود میبرد</p></div>
<div class="m2"><p>مسکین آتش به توشه برداشته بود</p></div></div>