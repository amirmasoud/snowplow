---
title: >-
    شمارهٔ  ۱۰۱
---
# شمارهٔ  ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>شهانه دلم ساخت مفتون زلفش</p></div>
<div class="m2"><p>نگذاشت دلی نکرده مجنون زلفش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کثرت دل ها بتوان گفت که نیست</p></div>
<div class="m2"><p>امروز سواد اعظمی چون زلفش</p></div></div>