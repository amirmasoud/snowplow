---
title: >-
    شمارهٔ  ۱۶۵
---
# شمارهٔ  ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>من کرده ام از هر مژده یی دریایی</p></div>
<div class="m2"><p>او ساخته بزم غیر را مأوایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بخت بد من است این ورنه کسی</p></div>
<div class="m2"><p>طوفان جائی ندید و دریا جایی</p></div></div>