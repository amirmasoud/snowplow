---
title: >-
    شمارهٔ  ۱۳۸
---
# شمارهٔ  ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>تا چند به بزم غیر تنها رفتن</p></div>
<div class="m2"><p>تنها بر هر بی سر و بی پا رفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم که چو خورشید رخت زرد کند</p></div>
<div class="m2"><p>ناخوانده چو خورشید به هرجا رفتن</p></div></div>