---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>چشم تو که با جهان عتاب است او را</p></div>
<div class="m2"><p>پیوسته ز خواب خوش نقاب است او را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریزد بسیار خون مردم به ستم</p></div>
<div class="m2"><p>بسیاری خون باعث خواب است او را</p></div></div>