---
title: >-
    شمارهٔ  ۱۰۷
---
# شمارهٔ  ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ای برده هوا سوی سماکت به سمک</p></div>
<div class="m2"><p>روخواهی کرد سوی پستی بی شک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>البته بود مسکن خاک آخر خاک</p></div>
<div class="m2"><p>گر فی المثلش باد رساند به فلک</p></div></div>