---
title: >-
    قطعه شمارهٔ ۳۱ - ماده تاریخ (میوهٔ بهشتی = ۷۷۸)
---
# قطعه شمارهٔ ۳۱ - ماده تاریخ (میوهٔ بهشتی = ۷۷۸)

<div class="b" id="bn1"><div class="m1"><p>آن میوهٔ بهشتی کآمد به دستت ای جان</p></div>
<div class="m2"><p>در دل چرا نکشتی از دست چون بهشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاریخ این حکایت گر از تو باز پرسند</p></div>
<div class="m2"><p>سرجمله‌اش فروخوان از میوهٔ بهشتی</p></div></div>