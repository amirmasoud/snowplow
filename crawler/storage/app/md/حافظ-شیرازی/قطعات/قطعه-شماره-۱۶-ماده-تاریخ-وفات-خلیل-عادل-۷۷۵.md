---
title: >-
    قطعه شمارهٔ ۱۶ - ماده تاریخ وفات (خلیل عادل = ۷۷۵)
---
# قطعه شمارهٔ ۱۶ - ماده تاریخ وفات (خلیل عادل = ۷۷۵)

<div class="b" id="bn1"><div class="m1"><p>برادر خواجه عادل طاب مثواه</p></div>
<div class="m2"><p>پس از پنجاه و نه سال از حیاتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سوی روضهٔ رضوان سفر کرد</p></div>
<div class="m2"><p>خدا راضی ز افعال و صفاتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلیل عادلش پیوسته بر خوان</p></div>
<div class="m2"><p>وز آنجا فهم کن سال وفاتش</p></div></div>