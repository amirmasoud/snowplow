---
title: >-
    شمارهٔ  ۱ - در انتقاد از اوضاع کشور
---
# شمارهٔ  ۱ - در انتقاد از اوضاع کشور

<div class="b" id="bn1"><div class="m1"><p>داش غلم مرگ تو حظ کردم از اشعار تو من</p></div>
<div class="m2"><p>متلذذ شدم از لذت گفتار تو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفرین گفتم بر طبع گهربار تو من</p></div>
<div class="m2"><p>بخدا مات شدم در تو و در کار تو من</p></div></div>
<div class="b2" id="bn3"><p>وصف مرکز را کس مثل تو بی پرده نگفت</p></div>
<div class="b2" id="bn4"><p>رفته و دیده و سنجیده و پی برده نگفت</p></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه در نمره ده بود منزه دیدم</p></div>
<div class="m2"><p>گر تو یک حسن در او دیدی من ده دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p></p></div>
<div class="m2"><p>نظم تو متقن و نثر تو موجه دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قابل محمدت و در خور به به دیدم</p></div>
<div class="b2" id="bn8"><p>هیچ یک از نمرات تو چنین خوب نبود</p></div>
<div class="b2" id="bn9"><p>یک فرازی که در او باشد معیوب نبود</p></div>
<div class="b" id="bn10"><div class="m1"><p>غیر تو پیش کسی این همه اخبار کجاست</p></div>
<div class="m2"><p>اگر اخبار بود جرأت اظهار کجاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p></p></div>
<div class="m2"><p>آنکه لوطی گریت را کند انکار کجاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پنطیند آن دگران، لوطی پادار کجاست</p></div>
<div class="b2" id="bn13"><p>آفرین ها به ثبات و به وفاداری تو</p></div>
<div class="b2" id="bn14"><p>پر و پا قرصی و رک گویی و پاداری تو</p></div>
<div class="b" id="bn15"><div class="m1"><p>که گمان داشت که این شور به پا خواهد شد</p></div>
<div class="m2"><p>هر چه دزد است ز نظمیه رها خواهد شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p></p></div>
<div class="m2"><p>دزد کت بسته رئیس الوزرا خواهد شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دور ظلمت بدل از دور ضیا خواهد شد</p></div>
<div class="b2" id="bn18"><p>مملکت باز همان آش و همان کاسه شود</p></div>
<div class="b2" id="bn19"><p>لعل ما سنگ شود لؤلؤ ما ماسه شود</p></div>
<div class="b" id="bn20"><div class="m1"><p>این رئیس الوزرا قابل فراشی نیست</p></div>
<div class="m2"><p>لایق آن که تو دل بسته او باشی نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p></p></div>
<div class="m2"><p>در بساطش بجز از مرتشی و راشی نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>همتش جز پی اخاذی و کلاشی نیست</p></div>
<div class="b2" id="bn23"><p>گر جهان را بسپاریش جهان را بخورد</p></div>
<div class="b2" id="bn24"><p>ور وطن لقمه نانی شود آن را بخورد</p></div>
<div class="b" id="bn25"><div class="m1"><p>از بیانات رئیس الوزرا با دو سه تن</p></div>
<div class="m2"><p>کرده یک رنده تآتری و فرستاده به من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p></p></div>
<div class="m2"><p>که کند دیدۀ ابناء وطن را روشن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من هم الساعه دهم شرح بر ابناء وطن</p></div>
<div class="b2" id="bn28"><p>تا بدانند چه نیکو امنایی دارند</p></div>
<div class="b2" id="bn29"><p>چه وطن خواه رئیس الوزرایی دارند</p></div>