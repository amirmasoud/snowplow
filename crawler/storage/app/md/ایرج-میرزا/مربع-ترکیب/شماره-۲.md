---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="l" id="bn1"><p>قوام السلطنه به پیشکار داخلی خود میرزا قاسم خان گوید:</p></div>
<div class="b" id="bn2"><div class="m1"><p>یک دو روزست دگر دست به کاری نزنی</p></div>
<div class="m2"><p>لیره‌ای میره‌ای از گوشه کناری نزنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشت و فتحی نکُنی دخل و قماری نزنی</p></div>
<div class="m2"><p>نروی مارُخ و دزدیه شکاری نزنی</p></div></div>
<div class="b2" id="bn4"><p>چه شنیدی که بدینگونه نه هراسان شده‌ای</p></div>
<div class="b2" id="bn5"><p>مگر آشفتهٔ اوضاع خراسان شده‌ای</p></div>
<div class="b" id="bn6"><div class="m1"><p>این وطن مایهٔ ننگست پی دخلت باش</p></div>
<div class="m2"><p>هر چه گویند جفنگست پی دخلت باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p></p></div>
<div class="m2"><p>شهر ما شهر فرنگست پی دخلت باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پای این قافله لنگست پی دخلت باش</p></div>
<div class="b2" id="bn9"><p>دست و پا کن که خرید چمدان باید کرد</p></div>
<div class="b2" id="bn10"><p>فکر کالسکۀ راه همدان باید کرد</p></div>
<div class="l" id="bn11"><p>پیشکار جواب گوید:</p></div>
<div class="b" id="bn12"><div class="m1"><p>دم مزن قافیه تنگست بیا تا برویم</p></div>
<div class="m2"><p>کُلُنِل بر سر جنگست بیا تا برویم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p></p></div>
<div class="m2"><p>نه دگر جای درنگست بیا تا برویم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قصّهٔ توپ و تفنگست بیا تا برویم</p></div>
<div class="b2" id="bn15"><p>هر چه از مردم بیچاره گرفتیم بس است</p></div>
<div class="b2" id="bn16"><p>بیش از این فکر مداخل شدن ما هوس است</p></div>
<div class="l" id="bn17"><p>قوام السلطنه گوید:</p></div>
<div class="b" id="bn18"><div class="m1"><p>ول مگو، گوش به گفتار تو نادان ندهم</p></div>
<div class="m2"><p>من سلامیّ و سِدِه را ز کف آسان ندهم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p></p></div>
<div class="m2"><p>من به ژاندارم اگر جان بدهم نان ندهم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اسب و اسباب به ژاندارم خراسان ندهم</p></div>
<div class="b2" id="bn21"><p>زنده باشم من و کالسکۀ من ضبط شود</p></div>
<div class="b2" id="bn22"><p>می زنم تا همه جا گر همه جا خَبط شود</p></div>
<div class="b" id="bn23"><div class="m1"><p>سی و شش اسبِ گرانمایه ز من کُلنِل زد</p></div>
<div class="m2"><p>سی و شش داغِ برافروخته ام بر دل زد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p></p></div>
<div class="m2"><p>بر جِراحاتِ من از بی نمکی فلفل زد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پاک بر روزنۀ دَخلِ خراسان گِل زد</p></div>
<div class="b2" id="bn26"><p>با چنین حادثه گر من نستیزم چه کنم</p></div>
<div class="b2" id="bn27"><p>خونِ سرتاسر این مُلک نریزم چه کنم</p></div>
<div class="b" id="bn28"><div class="m1"><p>تو مپندار که نه شاه و نه لشکر باقیست</p></div>
<div class="m2"><p>نه دگر روح و رمق در تن کشور باقیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p></p></div>
<div class="m2"><p>تا دوسر کرده به سَنگان و به لنگر باقیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عاقل آسوده بُوَد تا به جهان خر باقیست</p></div>
<div class="b2" id="bn31"><p>می کنم حُکم و همه حکم مرا گوش کنید</p></div>
<div class="b2" id="bn32"><p>وز شعف مصلحت خویش فراموش کنید</p></div>
<div class="b" id="bn33"><div class="m1"><p>من به هر حیله بُوَد مقصد خود صاف کنم</p></div>
<div class="m2"><p>به خوانینِ خراسان دو تلگراف کنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p></p></div>
<div class="m2"><p>وعده از جانب شَه رتبه و الطاف کنم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست خطی دو سه بر قاین و بر خواف کنم</p></div>
<div class="b2" id="bn36"><p>هم دیوان صفت قوۀ خود جمع کنند</p></div>
<div class="b2" id="bn37"><p>ریشِ ژاندارمری و ریشۀ خود قَمع کنند</p></div>
<div class="l" id="bn38"><p>یک نفر دوست دانا در آن مجلس بوده می گوید:</p></div>
<div class="b" id="bn39"><div class="m1"><p>گوش کن عقلِ من از خِسَّت تو بیشترست</p></div>
<div class="m2"><p>اینقدر جوش مزن جوش زدن بی ثمرست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p></p></div>
<div class="m2"><p>شُکرللّه که ترا در همه جا سیم و زرست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جان که باقیست ضررهایِ دگر مختصرست</p></div>
<div class="b2" id="bn42"><p>خیز و هر جایِ فرنگستان خواهی که برو</p></div>
<div class="b2" id="bn43"><p>بیش ازین باعثِ خون ریختنِ خلق مشو</p></div>
<div class="b" id="bn44"><div class="m1"><p>آتش فتنه ز هر گوشه برافروخته شد</p></div>
<div class="m2"><p>خَرمن هستیِ مسکین و غنی سوخته شد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p></p></div>
<div class="m2"><p>هر قدر پول که می خواستی اندوخته شد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پارگی‌هایِ خراسان تو هم دوخته شد</p></div>
<div class="b2" id="bn47"><p>بیش ازین صرفه ازین مُلک پریشان نَبَری</p></div>
<div class="b2" id="bn48"><p>غیر بد نامیِ آشوب خراسان نبری</p></div>
<div class="l" id="bn49"><p>مشار الملک که به مجلس وارد و از قضیه مستحضر شده می گوید:</p></div>
<div class="b" id="bn50"><div class="m1"><p>امشب اوقات شریف تو چرا خندان نیست</p></div>
<div class="m2"><p>راستست اینکه ضرر بابِ دلِ انسان نیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p></p></div>
<div class="m2"><p>لیک این مایه ضرر را عظمت چندان نیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>وز سلامی و سِده صرف نظر آسان نیست</p></div>
<div class="b2" id="bn53"><p>که به کُشتن بدهی خیلِ مسلمانان را</p></div>
<div class="b2" id="bn54"><p>دشمن خویش کنی قاطبه ایران را</p></div>
<div class="b" id="bn55"><div class="m1"><p>وانگهی کیست که فرمان ترا گوش کند</p></div>
<div class="m2"><p>از برای دلِ تو جامِ بلا نوش کند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p></p></div>
<div class="m2"><p>کیست آن خَر که مر این نکته فراموش کند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>زن و فرزند به راهِ تو سیَه پوش کند</p></div>
<div class="b2" id="bn58"><p>که نجنگیده و ننشانده فرو کینهٔ تو</p></div>
<div class="b2" id="bn59"><p>ناگهان سر بِرِسَد دورهٔ کابینهٔ تو</p></div>
<div class="b" id="bn60"><div class="m1"><p>در من از تقویتِ کارِ تو کوتاهی نیست</p></div>
<div class="m2"><p>لیک از این بیشترم قوّه ی همراهی نیست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p></p></div>
<div class="m2"><p>شاه را نیز از اعمالِ تو آگاهی نیست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>در من آنقدر خیانت که تو می خواهی نیست</p></div>
<div class="b2" id="bn63"><p>لیک تا چند توان مسأله را پنهان کرد؟</p></div>
<div class="b2" id="bn64"><p>شاه را غافل و یک ناحیه را ویران کرد؟</p></div>
<div class="b" id="bn65"><div class="m1"><p>بکن آن کار که کَردَست وثوق الدوله</p></div>
<div class="m2"><p>نه دگر کج شود از بهر وطن نه چَوله</p></div></div>
<div class="b" id="bn66"><div class="m1"><p></p></div>
<div class="m2"><p>والس می رقصد با مادموازل ژاکوله</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در هتل مقعد خود پاک کند با هوله</p></div>
<div class="b2" id="bn68"><p>برده پولی و کنون با دل خوش خرج کند</p></div>
<div class="b2" id="bn69"><p>متّصل قِر دِهَد و فِر زَنَد و فرج کند</p></div>
<div class="b" id="bn70"><div class="m1"><p>حالیه وقت فرنگ است بجنبان تنه را</p></div>
<div class="m2"><p>با خودت نیز ببر مُعتمدالسّلطنه را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p></p></div>
<div class="m2"><p>نیست در خارج لذّت سفر یکتنه را</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>از تنِ مالیۀ مُلک بِکَن این کَنِه را</p></div>
<div class="b2" id="bn73"><p>بگذار آتش افروخته خموش شود</p></div>
<div class="b2" id="bn74"><p>ضرَرِ اسب و سِدهِ نیز فراموش شود</p></div>