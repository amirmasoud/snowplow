---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>به انگشتان پا از زیر کرسی</p></div>
<div class="m2"><p>ز کس ها کرده‌ام احوال پرسی</p></div></div>