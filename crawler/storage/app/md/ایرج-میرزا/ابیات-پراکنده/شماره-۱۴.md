---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>شب عید عُمَر به قول زنان</p></div>
<div class="m2"><p>من در این خانه بوده‌ام مهمان</p></div></div>