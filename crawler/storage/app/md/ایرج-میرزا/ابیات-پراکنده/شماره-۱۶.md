---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای به درگاه تو نیاز همه</p></div>
<div class="m2"><p>کرم تست چاره ساز همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از چهره پرده برداری</p></div>
<div class="m2"><p>به حقیقت کشد مجاز همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه وشان مظهر جمال تواند</p></div>
<div class="m2"><p>بهر آن می کشیم ناز همه</p></div></div>