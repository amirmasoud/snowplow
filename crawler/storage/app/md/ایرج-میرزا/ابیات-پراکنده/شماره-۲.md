---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>کو خدا کیست خدا چیست خدا</p></div>
<div class="m2"><p>بی جهت بحث مکن، نیست خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که پیغمبر ما بود همی</p></div>
<div class="m2"><p>ما عَرَفناک بفرمود همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو دگر طالب پرخاش مشو</p></div>
<div class="m2"><p>کاسۀ گرم تر از آش مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه عقل تو در آن ها مات است</p></div>
<div class="m2"><p>تو بمیری همه موهومات است</p></div></div>