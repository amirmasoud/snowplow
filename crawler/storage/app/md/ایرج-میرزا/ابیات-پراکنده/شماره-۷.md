---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>نیست جهان جز همین که با تو بگویم</p></div>
<div class="m2"><p>روز و شبانی به یکدگر شده پیوند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق جهان هم اگر تو نیک بسنجی</p></div>
<div class="m2"><p>هیچ برون نیستند از این گُرۀ چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاقوی ظالمند و عاجز مظلوم</p></div>
<div class="m2"><p>............................ـند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عده‌ای از آنچه می‌ندارد غمگین</p></div>
<div class="m2"><p>عدۀ دیگر از آنچه دارد خرسند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر کوی تو باز سبز شوم</p></div>
<div class="m2"><p>گر چو بیدم قلم قلم بکنند</p></div></div>