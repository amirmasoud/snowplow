---
title: >-
    شمارهٔ  ۱۳
---
# شمارهٔ  ۱۳

<div class="b" id="bn1"><div class="m1"><p>دست در حلقۀ هوی تو کنم</p></div>
<div class="m2"><p>بوسه‌ای بر سر و روی تو کنم</p></div></div>