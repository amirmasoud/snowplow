---
title: >-
    شمارهٔ  ۸ - در مدح نصرة الدوله عم زاده ناصرالدین شاه در تبریز گفته
---
# شمارهٔ  ۸ - در مدح نصرة الدوله عم زاده ناصرالدین شاه در تبریز گفته

<div class="b" id="bn1"><div class="m1"><p>برخیز که باید به قدح خون رز افگند</p></div>
<div class="m2"><p>کِامد مهِ فروردین تا شد مهِ اسفند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد نسیم آنچه همی باید آورد</p></div>
<div class="m2"><p>افگند صبا آنچه همی شاید افگند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وقتست نگارا که تو هم چهره فروزی</p></div>
<div class="m2"><p>اکنون که گل و لاله همی چهره فروزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضلیست مساعد چه خوش آید که درین فصل</p></div>
<div class="m2"><p>با یارِ مساعد بزنی ساتگنی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من شاعرم و قدر تُرا نیک شناسم</p></div>
<div class="m2"><p>عُشّاقِ دگر قدرِ تو چون می نشناسند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ساده دل و باده کش و دوست پرستم</p></div>
<div class="m2"><p>نه زهد و ورع دارم نه حیله و ترفند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه مُبغِضِ اِنجیلَم و نه مُسلِمِ تَورات</p></div>
<div class="m2"><p>نه منکرِ فُرقانم و نه معتقدِ زَند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من مهر و وفایم همه تو جور و جفایی</p></div>
<div class="m2"><p>بگشای در صلح و در جنگ فروبند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر صبح به نوعی دگرم خسته بِمَگذار</p></div>
<div class="m2"><p>هر شام به طرزی دگرم رنجه بِمَپسند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برخیز و سمن بار از آن زلفِ سمن بار</p></div>
<div class="m2"><p>بنشین و شکر ریز از آن لعل شکرخند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میثاق شکستن ، بتِ من آخر تا کی</p></div>
<div class="m2"><p>پیوند گسستن ، مهِ من آخر تا چند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شایسته نباشد ، مشکن این همه میثاق</p></div>
<div class="m2"><p>بایسته نباشد مَگُسِل این همه پیوند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تنها نه دل من ز تو خرسند نباشد</p></div>
<div class="m2"><p>یک دل بِنَدیدَم که ز تو باشد خرسند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زود است که از جور تو آیم به تظلُّم</p></div>
<div class="m2"><p>در حضرتِ آن کش به جهان نیست همانند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این عَمِ شه ناصرِ دین نصرتِ دولت</p></div>
<div class="m2"><p>آن ناصرِ شرعِ نبی و دینِ خداوند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فّرخ گهر و پاک و نکوخوی و نکو روی</p></div>
<div class="m2"><p>فّرخ سیر و راد و عدوسوز و عدو بند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فیروز و جوانبخت و جوانمرد و هنر جوی</p></div>
<div class="m2"><p>بهروز و سخن سنج و سخندان و خردمند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عهدش همگی محکم و قولش همگی راست</p></div>
<div class="m2"><p>گفتش همگی حکمت و لفظش همگی پند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای خصمِ مَلِکزاده تُرا بهره خوشی نیست</p></div>
<div class="m2"><p>در هند و خُتَن باشی یا چین و سمرقند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خاصیت زهر آرد بر جانِ تو پا زهر</p></div>
<div class="m2"><p>کیفیّتِ سم بخشد اندر لب تو قند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیوسته تو فیروزی ای میر ازیرک</p></div>
<div class="m2"><p>فیروز پدر بودت و فیروزت فرزند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرخنده و فّرخ به تو نوروز و سرِ سال</p></div>
<div class="m2"><p>با نصرت و عزّت که نهال تو برومند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همدستِ تو بادا به حَضَر لطفِ الهی</p></div>
<div class="m2"><p>همراه تو بادا به سفر عون خداوند</p></div></div>