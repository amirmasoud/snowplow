---
title: >-
    شمارهٔ  ۷ - در مدح اعتضادالسَّلطنه
---
# شمارهٔ  ۷ - در مدح اعتضادالسَّلطنه

<div class="b" id="bn1"><div class="m1"><p>مقبول باد طاعتِ شَهزاده اعتضاد</p></div>
<div class="m2"><p>عیدِ سعیدِ فطر بر او فرخجسته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون از جوان بسنده بود طاعتِ خدای</p></div>
<div class="m2"><p>هر طاعتی که کرد خدا را پسنده باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واجب نمود سجدۀ حقّ را به خویشتن</p></div>
<div class="m2"><p>زان رو به خلق سجدۀ او واجب اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهان جبینِ عجز به درگاهِ او نهند</p></div>
<div class="m2"><p>چون او جبینِ عجز به درگاهِ حقّ نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر حقّ مطیع بود که چونان مُطاع شد</p></div>
<div class="m2"><p>شاگرد تا نباشی کی گردی اوستاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای شاه زاده‌ای که تُرا از ازل خدای</p></div>
<div class="m2"><p>شرم و حیا و دانش بنهاد در نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخنده زی به دَهر که چون جدّ و چون پدر</p></div>
<div class="m2"><p>شاهی و شاه زاده و پاکی و پاکزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کردۀ تو ایزد شاد است و شادمان</p></div>
<div class="m2"><p>زانرو کند همیشه تُرا شادمان و شاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون هیچ گه برون نَبُود حق زیادِ تو</p></div>
<div class="m2"><p>در هیچ گه نباشی حق را برون زیاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از عدل و داد چون که وجودِ تو خلق شد</p></div>
<div class="m2"><p>محکم شد از وجودِ تو بُنیانِ عدل و داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن جا که تو نشستی دولت همی نشست</p></div>
<div class="m2"><p>آن جا که تو ستادی دولت همی ستاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن کس که بنگرد به جبینِ مبینِ تو</p></div>
<div class="m2"><p>بیند همی عیان که تویی پادشه نژاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در روز جنگ دشمنِ تو بر تو جان دهد</p></div>
<div class="m2"><p>بنگر که تا چه پایم کریم آمدست شاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نامِ سلطنت به جهان جاودانِ کنی</p></div>
<div class="m2"><p>ایزد خدای نام تُرا جاودان کُناد</p></div></div>