---
title: >-
    شمارهٔ  ۲۳ - در نعتِ نبّی خاتَم
---
# شمارهٔ  ۲۳ - در نعتِ نبّی خاتَم

<div class="b" id="bn1"><div class="m1"><p>نه عاقل است که دارد در این سرایِ رحیل</p></div>
<div class="m2"><p>قصّیر عمرِ خود اندر امید هایِ طویل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهد به گردنِ جان رشته‌ای ز طولِ اَمَل</p></div>
<div class="m2"><p>که تا قیامت آن رشته را بود تطویل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مَناص جویی از این رشته لاتَ حِینَ مَناص</p></div>
<div class="m2"><p>خَلاص خواهی ازین عُقده لاعَلَیکَ سَبیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آن که بگسست این رشتۀ امید ز جان</p></div>
<div class="m2"><p>نهاد بر کف تقدیرِ کردگارِ جلیل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهاند خود را از منّتِ وضیع و شریف</p></div>
<div class="m2"><p>نجات داد هم از خجلتِ کریم و بخیل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خلیل وار توکّل به کردگار نمای</p></div>
<div class="m2"><p>که تا رهاند از آتَشِ غَمَت چو خلیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نصیرِ جانِ تو چون حق بُوَد فَنِعمَ نَصیر</p></div>
<div class="m2"><p>وکیلِ کار تو چون حق بُوَد فَنِعمَ وَکیل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رهینِ هر کس و ناکس مشو پیِ روزی</p></div>
<div class="m2"><p>چو او به روزیِ هر ناکس و کس است وکیل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان که او به تو جان داد نان دهد چه کنی</p></div>
<div class="m2"><p>ز بهر فانی ، جانِ عزیز خوار و ذلیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جمالِ صورتِ فردا کجا تُرا باشد</p></div>
<div class="m2"><p>اگر نباشد امروز سیرتِ تو جمیل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مسافری تو و ناچار بایدت زادی</p></div>
<div class="m2"><p>که زاد باید مر مرد را به گاهِ رحیل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کدام زاد نکوتر ز حُبِّ پیغمبر</p></div>
<div class="m2"><p>که خلق را سویِ ایزد وِلایِ اوست دلیل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نداشت سایه ولی رحمت و عطوفت او</p></div>
<div class="m2"><p>فُتادگان را بر سر فکنده ظِلِّ ظَلیل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود سراسر نَعتَش هر آنچه در فُرقان</p></div>
<div class="m2"><p>بود تمامی وصفش هر آنچه در اِنجیل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قتیلِ او را عیسی نیاورد جان داد</p></div>
<div class="m2"><p>اگر چه عیسی جان می دهد زِ دَم به قتیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر نه امرش ، نامی نبود از معروف</p></div>
<div class="m2"><p>اگر نه نهیش ، بودند خلق در تضلیل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رخِ نیاز نمی سود اگر به خاکِ دوش</p></div>
<div class="m2"><p>نمی رسید بدین جایگاه جبرائیل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز کاخ خسرویش نُه سپهرِ زنگاری</p></div>
<div class="m2"><p>مُعَلَّق است چو از کاخِ خسروان قِندیل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر نه قولش ، اسمی نبود از تسبیح</p></div>
<div class="m2"><p>اگر نه فعلش ، رسمی نبود از تَهلیل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز خُلقِ نیک و صفاتِ جمیل و خَلقِ بدیع</p></div>
<div class="m2"><p>نیافریدش ایزد هَمال و شِبه و عَدیل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کفیلِ روزیِ خلق است تا خدایِ جهان</p></div>
<div class="m2"><p>بود به شادیِ احبابِ او هماره کفیل</p></div></div>