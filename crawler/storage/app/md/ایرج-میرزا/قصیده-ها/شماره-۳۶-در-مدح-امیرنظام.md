---
title: >-
    شمارهٔ  ۳۶ - در مدح امیرنظام
---
# شمارهٔ  ۳۶ - در مدح امیرنظام

<div class="b" id="bn1"><div class="m1"><p>جانا چه شود گر تو درِ مِهر گشایی</p></div>
<div class="m2"><p>وز در به در آیی و چو جانم به بر آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانی چه گذشتست و زِ ما حال نپرسی</p></div>
<div class="m2"><p>وز هیچ دَردی هیچ دَرِ ما نگشایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تایی برِ ما ، ور گذرد عمری و آیی</p></div>
<div class="m2"><p>نشسته به پا خیزی و چون عمر نپایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو بیت ز خاقانیِ شروانی خوانَم</p></div>
<div class="m2"><p>استادِ سخن رانی و ممدوح سِتایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«هیچ افتدت امشب که برافتادگیِ من</p></div>
<div class="m2"><p>رحم آری و در کاهِشِ جانم نَفَزایی»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«یا بر شِکَرِ خویش مرا داری مهمان</p></div>
<div class="m2"><p>یا بر جگرِ ریش بمهمانِ من آیی»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدخو نبُدی ، تا که بیاموختت این خو</p></div>
<div class="m2"><p>یا تا چه خطا دیدی ای تُرکِ خطایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همواره پسِ یکدیگر آیند مه و مهر</p></div>
<div class="m2"><p>ای ماه ندانم که تو بی مهر چرایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با هیچ کَسَت می نبُوَد مهر و وفا یا</p></div>
<div class="m2"><p>با هر که ترا خواهد بی مهر و وفایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اوّل که بِنَنمائی با ما تو رُخِ مهر</p></div>
<div class="m2"><p>صد قِصّه به دل گیری ور زان که نمایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواهی که دلِ من بربایی و ندانی</p></div>
<div class="m2"><p>کاین دل نه دلی باشد کو را بربایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>من دل به هوای میر دادستم از اول</p></div>
<div class="m2"><p>«هرکس به هوایی شد و سعدی به هوایی»</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چرخِ عظمت میر نظام آن که نگردد</p></div>
<div class="m2"><p>الّا که به کامِ دل او چرخِ رَحایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرخنده خداوندا از ناخوشیِ تو</p></div>
<div class="m2"><p>شد پیر فلک ، کرد همی پشت دوتایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک شهر رها گشت ز بندِ تعب و رنج</p></div>
<div class="m2"><p>کامروز ز بندِ تعب و رنج رهایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از ضعف رهانید دعایِ ضُعَفایت</p></div>
<div class="m2"><p>زان روی که تو پشت و پناهِ ضعفایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در لیل و نهارت فقرا جمله دعاگو</p></div>
<div class="m2"><p>زیرا که تو ملجاء و مَلاذِ فقرایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کس را نبُدی یَد که نرفتی به سویِ حقّ</p></div>
<div class="m2"><p>کس را نبُدی لب که نکردیت دعایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ایزد به تو در عالم دردی نپسندد</p></div>
<div class="m2"><p>زیرا که به دردِ همه عالم تو دوایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دادارِ جهان رنج و بلا از تو کند دفع</p></div>
<div class="m2"><p>کز خلقِ جهان دافعِ رنجی و بلایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حیف است که رانم به زبان نامِ عدویت</p></div>
<div class="m2"><p>هر کس که ترا دوست بود باد فدایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دافع بُوَدت حق ضرر از خاکی و بادی</p></div>
<div class="m2"><p>نافع بُوَدت آن چه بود ناری و مایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ار شاخه‌ای افسرده شود باک نباشد</p></div>
<div class="m2"><p>بیخی تو ، که می‌باید سر سبز بپایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیوسته بر افراخته باشی و تن آسا</p></div>
<div class="m2"><p>کاندر صف دولت تو فرازنده لِوایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همواره به جا باشی و هرگز بِنَیُفتی</p></div>
<div class="m2"><p>کاندر کفِ مُلکَت تو بَرازنده عصایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا ارض و سما باشد باشیّ و مصون باد</p></div>
<div class="m2"><p>جان و تنت از آفتِ ارضیّ و سَمایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یک رأیِ تو دو مملکت آسوده نمودی</p></div>
<div class="m2"><p>فرخنده چنین رای و چنین صاحب رایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دشتی که وزد رایحۀ قهرِ تو آن جا</p></div>
<div class="m2"><p>تا حشر مرویاد در آن مهر گیایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قارن به تو شمشیر دهد چون تو بجنگی</p></div>
<div class="m2"><p>بهمن سپر اندازد چون تو به وَغایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در رزم چو کوشش کنی و بزم چو بخشش</p></div>
<div class="m2"><p>چون قهرِ خدا باشی و چون بحرِ عطایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یک نثرِ تو بهتر ز مقاماتِ حمیدی</p></div>
<div class="m2"><p>یک نظمِ تو خوشتر ز غزل هایِ سنایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>این بیت ز صدر الشعّرایِ پدر خویش</p></div>
<div class="m2"><p>آرم به مدیحِ تو در این چامه گُوایی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>«بر حاشیۀ مائدۀ فضلِ تو باشد</p></div>
<div class="m2"><p>کشکولِ گدایی به کفِ شیخ بَهایی»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صدرا و وزیرا و بلند اختر میرا</p></div>
<div class="m2"><p>صَدر الوُزَرایی و امیرُ الأُمَرایی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فَخرالشُعَرا خواندی در عیدِ عزیزم</p></div>
<div class="m2"><p>دیدی چو کرا داعیۀ مَدح سُرایی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چونان که نکردستم از بی لقبی عار</p></div>
<div class="m2"><p>فخری نکنم نیز به فَخرُ الشّعرایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خود عار بُوَد ، لیکِن فخرست و مُباهات</p></div>
<div class="m2"><p>ممدوح تو چون باشی ، ممدوح ستایی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نز با لقبی بوی و بَهایَم بفزودی</p></div>
<div class="m2"><p>نز بی لقبی کاست ز من بوی و بهایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فخر من از آنست که همچون تو امیری</p></div>
<div class="m2"><p>نامم به زبان آری و گویی که مَرایی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از شاعری و شعر بَری باشم و خواهم</p></div>
<div class="m2"><p>در سلکِ ادیبان لقبم لطف نمایی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از تربیتت هست به من ، گر به ادیبان</p></div>
<div class="m2"><p>فضل و هنری باید و ذوقی و ذُکایی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شِعرم همه چون شعرِ بُتان چِگِل و چین</p></div>
<div class="m2"><p>نثرم همه چون خطِّ نکویانِ خَتایی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بس سُخره نمایم من و بس ضِحکه زنم من</p></div>
<div class="m2"><p>گر صرف مُبَرَّد بُوَد و نحوِ کِسایی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ایدون که مرا تربیت از شاه بیفزود</p></div>
<div class="m2"><p>شاید که تو هم تربیتِ من بفزایی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر ساعدِ مُلکِ شه اینجا بُدی امروز</p></div>
<div class="m2"><p>تصدیق مرا کردی از پاک دَهایی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای ساعدِ مُلک ای که تو از فرّخ حالی</p></div>
<div class="m2"><p>بر ساعدِ مُلک اندر فرخنده همایی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اعیادِ گذشته که مدیح عرضه نمودم</p></div>
<div class="m2"><p>اینجا بُدی امروز ندانم به کجایی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>صد حیف که امروز جدا بینمت از میر</p></div>
<div class="m2"><p>ای کاش نبودی ، به جهان نامِ جدایی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نی نی نه جدایی که تو اندر دل اویی</p></div>
<div class="m2"><p>اندر دل او باشی و در دیده نیایی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از بسکه ترا دیده و دل خواهد و جوید</p></div>
<div class="m2"><p>بر هر که نماییم نظر چون تو نمایی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اندر برِ میر ارچه بُوَد خالی جایت</p></div>
<div class="m2"><p>اندر دلِ او خالی نبُوَد ز تو جایی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فرخنده دلِ میر یکی خانۀ آنست</p></div>
<div class="m2"><p>کو را به خدا می رسدی خانه خدایی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شاید اگر از فخر بنازی و ببالی</p></div>
<div class="m2"><p>در خانۀ اُنسی تو و همرازِ خدایی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هم مجلسِ عقلی تو و هم صحبتِ عشقی</p></div>
<div class="m2"><p>همخوابۀ صدقی تو و همدوشِ صفایی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در کعبۀ مقصود خود اکنون به طَوافی</p></div>
<div class="m2"><p>در مروۀ آمال خود ایدون به صفایی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کارِ دو جهان سامان زین دل بپذیرفت</p></div>
<div class="m2"><p>زنگِ تعب از این دل یا رب بِزُدایی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ای راد امیری که به گاهِ کرم و جود</p></div>
<div class="m2"><p>آمد به دَرَت حاتمِ طائی به گدایی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر خِلعتِ شاهی پیِ تبریک سُرایم</p></div>
<div class="m2"><p>فرخنده و فّرخ بُوَدَت خِلعتِ شایی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>زین پیش که بودی به امیران و وزیران</p></div>
<div class="m2"><p>اندر سفر و غیرِ سفر مدح سُرایی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>از بهرِ ستود نشان بود و ز پیِ مدح</p></div>
<div class="m2"><p>دادندی اگر سیم و زر و برگ و نوایی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو از پیِ مدحِ خود بر من ندهی زر</p></div>
<div class="m2"><p>خواهی که همه مَکرُمت و جود نمایی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ناچار بُوَد طبع تو از بخشش زان روی</p></div>
<div class="m2"><p>هر لحظه به یک واسطه و عذر برآیی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>قدر تو و شأن تو فزونتر بود از این</p></div>
<div class="m2"><p>کز مدح بیفزایی و از هجو بِکایی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>من در خورِ فضلِ خود مدحِ تو سُرایم</p></div>
<div class="m2"><p>اما نه بدان سان که بباییّ و بشایی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>فرخنده امیرا پیِ این نیک قصیده</p></div>
<div class="m2"><p>خواهم که کنم نیز یکی خوب دعایی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چون وعدۀ مهدی خان عمر تو مطوّل</p></div>
<div class="m2"><p>چون آرزویم دولتِ تو باد بقایی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>کان وعده نپندارم هرگز به سر آید</p></div>
<div class="m2"><p>وین آرزویِ من مپذیراد فنایی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>استاد منوچهری خوش گفت بدین وزن</p></div>
<div class="m2"><p>«ای تُرکِ من امروز نگویی به کجایی؟»</p></div></div>