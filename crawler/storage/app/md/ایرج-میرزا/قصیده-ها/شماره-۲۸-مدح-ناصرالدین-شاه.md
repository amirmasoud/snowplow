---
title: >-
    شمارهٔ  ۲۸ - مدح ناصرالّدین شاه
---
# شمارهٔ  ۲۸ - مدح ناصرالّدین شاه

<div class="b" id="bn1"><div class="m1"><p>مباش ایمن ز کَیدِ چرخِ ریمن</p></div>
<div class="m2"><p>که از کَیدش نشاید بود ایمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نماید خانۀ امّید تاریک</p></div>
<div class="m2"><p>که سازد هر دو چشمِ آز روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سؤالِ دادخواهی گر کنی ، کر</p></div>
<div class="m2"><p>جوابِ دادخواهی ، باشد الکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه او را دوستی باشد محقّق</p></div>
<div class="m2"><p>نه او را دشمنی باشد مُبَرهَن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی را بی جهت گاهی بُوَد دوست</p></div>
<div class="m2"><p>یکی را بی سبب گاهی است دشمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند مسجود خواری را چُنوبُت</p></div>
<div class="m2"><p>عزیزی ساجد او را چون بَرَهمَن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دانایان بُوَد رنجِ مجسَّم</p></div>
<div class="m2"><p>به نادانان بُوَد گنجِ معیّن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی را کِشت دارد تازه و تر</p></div>
<div class="m2"><p>یکی را برزند آتش به خرمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکی خندان به سانِ برقِ لامع</p></div>
<div class="m2"><p>یکی گریان مثالِ ابرِ بهمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی را روزِ روشن شامِ تیره</p></div>
<div class="m2"><p>یکی را شامِ تیره روزِ روشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی تَحتِ ثَری بنموده مأوی</p></div>
<div class="m2"><p>یکی فوقِ ثُرَیّا کرده مسکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی نالد ز عریانی شب و روز</p></div>
<div class="m2"><p>یکی بالد به دیبایِ مُلَوَّن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی را زایدی شادی ز شادی</p></div>
<div class="m2"><p>یکی را خیزدی شیون ز شیون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی را باده اندر کاسۀ دل</p></div>
<div class="m2"><p>یکی را خونِ دل در کاسۀ تن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی را بوریا آرامگاه است</p></div>
<div class="m2"><p>یکی را اطلسِ رومی نشیمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی را دست اندر گردنِ بخت</p></div>
<div class="m2"><p>یکی با بختِ خفته دست و گردن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر باشی به بزم اندر ارسطو</p></div>
<div class="m2"><p>وگر گردی به رزم اندر پَشُوتَن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بختت نیست در دِل ماند اَرمان</p></div>
<div class="m2"><p>اگر در چین گریزی یا به اَرمن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خُنک آن را که او با یاریِ بخت</p></div>
<div class="m2"><p>به توفیقِ خدایِ حیِ ذوالمنّ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نخواهد ساعتی آرامشِ دل</p></div>
<div class="m2"><p>نجوید لَحظه‌ای آسایشِ تن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سفر سازد پیِ کسبِ معالی</p></div>
<div class="m2"><p>که رسمِ زن بود یک جای بودن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر در کوی و برزن نگذرد مرد</p></div>
<div class="m2"><p>کجا دارد فضلیت مرد بر زن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رَوی زن وار در خانه نشینی</p></div>
<div class="m2"><p>که شاید روزیَت آید ز روزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگز نشنیده‌ای این را که گویند</p></div>
<div class="m2"><p>حَطَب باشد به جایِ خویش چَندَن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر دُر ناید از دریا به خارج</p></div>
<div class="m2"><p>نیاویزند خوبانش ز گردن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخواندی اینکه گفت اِبنِ قَلاقِس</p></div>
<div class="m2"><p>أَلا سارَاَلهِلالُ فَصارَ بدرأ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آب استاده شد یابد عفونت</p></div>
<div class="m2"><p>چو جاری گشت گردد صاف و روشن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بحمداللّه مرا تن گشته راحت</p></div>
<div class="m2"><p>همه بارِ سفر ننهاده بر تن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ندیده صدمه‌ای از سختیِ راه</p></div>
<div class="m2"><p>نخورده لطمه‌ای از بیم رهزن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه برگ سفر دارم مهیّا</p></div>
<div class="m2"><p>همه عیشِ حضر دارم معیّن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز فّرِ مدح کیوان فر خدیوی</p></div>
<div class="m2"><p>که امرش را قضا بنهاده گردن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ولی عهدِ مَلِک سلطان مُظَفّر</p></div>
<div class="m2"><p>خدیوِ شیر گیرِ پیل افگن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدیوِ پاک قلبِ پاک طینت</p></div>
<div class="m2"><p>خدیوِ پاک دینِ پاکدامن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خدیوا خوش بزی خرم گرفتی</p></div>
<div class="m2"><p>شبِ میلادِ شاهِ شیر اوژن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شهی کز دستِ او بالَنده شمشیر</p></div>
<div class="m2"><p>شهی کز فرقِ او رازننده گَرزَن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شهِ صاحبقران شه ناصرِالدّین</p></div>
<div class="m2"><p>که دارد نُه فلک در زیرِ دامن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنین شه زاد از مادر که گردید</p></div>
<div class="m2"><p>ز شِبهَش مادرِ گیتی سَتَروَن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز چِهرش چهرۀ دولت منوَّر</p></div>
<div class="m2"><p>ز نامش نامۀ ملّت مُعَنوَن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کجا دستش سراسر گوهر آگین</p></div>
<div class="m2"><p>کجا تیغش همه بیجاده معدن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به کوهِ آهن ار حُکمش بخوانی</p></div>
<div class="m2"><p>شود چون رودِ جیحون کوهِ آهن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>عروسِ بختِ او در دست دارد</p></div>
<div class="m2"><p>ز اقبال و شرف دست آورنجن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به دیدن صعب باشد روزِ میدان</p></div>
<div class="m2"><p>ولی در روز ایوان نیک دیدن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سرِ خصم و سِنانِ جان ستانش</p></div>
<div class="m2"><p>تو گوئی فی المثل گویست و مِحجَن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدیوا رسم باشد این که گویند</p></div>
<div class="m2"><p>پدر را بر پسر تبریک لیکن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تُرا من بر پدر تبریک گویم</p></div>
<div class="m2"><p>که برچونین پدر چشمِ تو روشن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>سریرِ سلطنت زو شد مشرَّف</p></div>
<div class="m2"><p>سرایِ معدلت زو شد مزیَّن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همه با عمرِ او دست خدا بست</p></div>
<div class="m2"><p>بقا را تا ابد دامن به دامن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>هر آنکس کج نگر باشد به جاهش</p></div>
<div class="m2"><p>شود در دیدگانش مژّه سوزن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کفِ رادش تو گویی روزِ بخشِش</p></div>
<div class="m2"><p>زبانِ پنج دارد پنج ناخَن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به در دِفاقه جوید هر که درمان</p></div>
<div class="m2"><p>همی گویند درمانِ تو در من</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>جهانداور خدیوا حِرصِ مَدحَت</p></div>
<div class="m2"><p>همی واداردم بر شعر گفتن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همه از رأفتت بنموده خلقت</p></div>
<div class="m2"><p>یگانه پاک خَلّاق مُهَیمَن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چُنُو دولت شتابد بر درِ تو</p></div>
<div class="m2"><p>که سنگِ رفته بیرون از فلاخن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>غلط مشهور گشتست اینکه گویند</p></div>
<div class="m2"><p>که از لاحَول بگریزد هَریمَن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز جانِ خصم ، شیطانِ سنانت</p></div>
<div class="m2"><p>نگردد دور از لاحول گفتن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سرایِ آز از دستِ تو ویران</p></div>
<div class="m2"><p>چُنُو زابلستان از تیغ بهمن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به روز رزمِ تو یک دشت لشکر</p></div>
<div class="m2"><p>چو پیشِ طایری یک مشت ارزن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنو ویسه ز قارن شد گریزان</p></div>
<div class="m2"><p>گریزان گردد از بیم تو قارن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه تنها قارن از بیمت گریزد</p></div>
<div class="m2"><p>اگر پاداشت کوهِ قارن ایضاً</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به خوانِ تن بود خصمِ تُرا دل</p></div>
<div class="m2"><p>ز آو آتشین مرغِ مُسَمَّن</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>الا تا هست در افواهِ مردم</p></div>
<div class="m2"><p>حسد ورزیدنِ گرگین به بیژن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نماید تا ابد گرگینِ جاهت</p></div>
<div class="m2"><p>چو بیژن در میانِ چاه مسکن</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نه او را دستگیری چون منیژه</p></div>
<div class="m2"><p>نه او را دستیاری چون تَهَمتَن</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>الا تو حرفِ جَزم و نصب باشد</p></div>
<div class="m2"><p>به قانونِ عرب حرفِ لَم و لَن</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو باشی ناصبِ اَعلامِ دولت</p></div>
<div class="m2"><p>تو باشی جازِمِ اَعناقِ دشمن</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>به دستِ عدل بیخ ظِلم بُگسِل</p></div>
<div class="m2"><p>به مشتِ دوست پشتِ خصم بشکن</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>اگر شد قافیه بعضی مکرّر</p></div>
<div class="m2"><p>وگر آید ردیفِ نون مُنَوَّن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>حضوراً عذر خواهم تا نگیرند</p></div>
<div class="m2"><p>غیاباً خرده استادانِ این فن</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>منوچهری بدین هنجار گفتست</p></div>
<div class="m2"><p>«شبی گیسو فروهشته به دامن»</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چنین گفته است خاقانی بدین وزن</p></div>
<div class="m2"><p>«ضمان دارِ سلامت شد دلِ من»</p></div></div>