---
title: >-
    شمارهٔ  ۲ - دیدار
---
# شمارهٔ  ۲ - دیدار

<div class="b" id="bn1"><div class="m1"><p>دیدم اندر گردش بازار عبداللّه را</p></div>
<div class="m2"><p>این عجب نبود که در بازار بینم ماه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردمان آیند استهلال را بالای بام</p></div>
<div class="m2"><p>من به زیر سقف دیدم روی عبداللّه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسُفِ ثانی به بازار آمد ای نَفسِ عزیز</p></div>
<div class="m2"><p>رو بخر او را و بر خوان اَکرِ می مَثواه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که او را دید ماهذا بَشَر گوید همی</p></div>
<div class="m2"><p>من درین گفته ستایش می کنم افواه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترسم این بازاریان از دیدن او بشکند</p></div>
<div class="m2"><p>کاش تغییری دهد یک چند گردشگاه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گم کند تاجر حساب ذرع و کاسب راه دخل</p></div>
<div class="m2"><p>چون ببیند بر دُکان آن شمسۀ خرگاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ور بیفتد چشم زاهد بر رخش وقت نماز</p></div>
<div class="m2"><p>لا اِله از گفته ساقط سازد اِلَا اللّه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که او را دید راه خانۀ خود گم کند</p></div>
<div class="m2"><p>بارها این قصه ثابت گشته این گمراه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در زبانم لکنت آید چون کنم بر وی سلام</p></div>
<div class="m2"><p>من که مفتون می کنم از صحبت خود شاه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای که گویی قصه از زلف پریشان دراز</p></div>
<div class="m2"><p>رو ببین آن طرۀ فر خوردۀ کوتاه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غبغبی دارد که دور از چشم بد بی اختیار</p></div>
<div class="m2"><p>می کشد از سینۀ بیننده بیرون آه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کوه نور است آن کفل در پشت آن دریای نور</p></div>
<div class="m2"><p>راستی زیبد خزانۀ خسرو جم جاه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ کس آگه نخواهد شد ز کار عشق ما</p></div>
<div class="m2"><p>مُغتَنَم دان صحبت این پیر کار آگاه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر تو عصمت خواه می باشم مَرَم از من که من</p></div>
<div class="m2"><p>پاسبان عصمتم اطفال عصمت خواه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من ز زلف مشک فامِ تو به بویی قانعم</p></div>
<div class="m2"><p>سال‌ها باشد که من بدرود گفتم باه را</p></div></div>