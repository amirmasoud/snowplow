---
title: >-
    شمارهٔ  ۱۵ - اندرز و نصیحت
---
# شمارهٔ  ۱۵ - اندرز و نصیحت

<div class="b" id="bn1"><div class="m1"><p>فکر آن باش که سالِ دگر ای شوخ پسر</p></div>
<div class="m2"><p>روزگارِ تو دِگر گردد و کارِ تو دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن تو بسته به مویی است ز من رنجه مشو</p></div>
<div class="m2"><p>که ز روزِ بدِ تو بر تو شدم یادآور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر تو این موی بود اَقرَبُ مِن حَبلِ وَرید</p></div>
<div class="m2"><p>ای تو در دیدۀ من اَبهی مِن نُورِ بَصَر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موی آنست که چون سرزند از عارضِ تو</p></div>
<div class="m2"><p>همه اعضایت تغییر کند پا تا سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه دگر وصف کند کس سرِ زلفت به عبیر</p></div>
<div class="m2"><p>نه دگر مدح کند کس لبِ لعلت به شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه دگر باشد رویِ تو چو ماهِ نَخشَب</p></div>
<div class="m2"><p>نه دگر مانَد قدِّ تو به سرو کَشمَر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشَت آن گوشست امّا نبُوَد همچو صدف</p></div>
<div class="m2"><p>چشمت آن چشمست امّا نبُوَد چون عَبهَر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طُرّه ات طرّۀ پیشست ولی کو زنجبر ؟</p></div>
<div class="m2"><p>سینه ات سینۀ قبلست ولی کو مرمر ؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو این مو که کند منعِ ورود از عُشاق</p></div>
<div class="m2"><p>خارِ آهن نکند دفعِ هجوم از سنگر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نه دگر کس زقفای تو فُتَد در کوچه</p></div>
<div class="m2"><p>نه دگر کس به هوای تو سِتَد در معبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه بر در بُوَد امسال دو چشمش شب و روز</p></div>
<div class="m2"><p>که تو باز آیی و بر خیزد و گیردت به بر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سالِ نو چون به در خانۀ او پای نهی</p></div>
<div class="m2"><p>خادم و حاجبِ او عذرِ تو خواهد بردر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه کم از موری در فکرِ زمستانت باش</p></div>
<div class="m2"><p>پیش کاین مو به رُخَت چون مور آرد لشکر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من تُرا طفلکِ باهوشی انگاشته ام</p></div>
<div class="m2"><p>طفلِ باهوش نه خود رای بود نه خود سر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر جوانیست بس ، ار خوشگذارا نیست بس است</p></div>
<div class="m2"><p>آخِرِ حال ببین ، عاقبتِ کار نِگَر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در کلوپ ها نتوان کرد همه وقت نَشاط</p></div>
<div class="m2"><p>در هتل ها نتوان برد همه عمر به سر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو به اصل و نسب از سلسلۀ اشرافی</p></div>
<div class="m2"><p>این شرافت را از سلسلۀ خویش مَبَر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وقت را مردم با عقل غنیمت شُمَرند</p></div>
<div class="m2"><p>اگرت عقل بود وقت غنیمت بِشُمَر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تکیه بر حسن مکن در طلبِ علم برای</p></div>
<div class="m2"><p>این درختیست که هر فصل دهد بر تو ثمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سیمِ امروز ز دستت برود تا فردا</p></div>
<div class="m2"><p>بادبَر باشد چیزی که بُوَد بادآور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خط برون آری نه خط به تو باشد نه سواد</p></div>
<div class="m2"><p>خَسِرَ الدُّنیا وَ الآخِرَه گردی آخر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کوش کز علم به خود تکیه گهی سازکنی</p></div>
<div class="m2"><p>چون ببندد حسن از خدمتِ تو سازِ سفر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درس را باید زان پیش که ریش آید خواند</p></div>
<div class="m2"><p>نشنیدی که بود درسِ صِغَر نَقشِ حَجَر ؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دانش و حسن به هم نورِ عَلی نور بُوَد</p></div>
<div class="m2"><p>وه از آن صاحب حسنی که بود دانشور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>علم اگر خواهی با مردمِ عالم بنشین</p></div>
<div class="m2"><p>گِل چو گُل گردد خوشبو چو به گُل شد همبر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ذرّه بر چرخ رسد از اثرِ تابشِ خور</p></div>
<div class="m2"><p>پِشک خوشبو شود از صحبتِ مُشکِ اَذفَر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو گر از خدمت نیکان نَچِنی غیر از خار</p></div>
<div class="m2"><p>به که در صحبتِ دُونان دِرَوی سِیسَنبَر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چارۀ کار تو این است که من می گویم</p></div>
<div class="m2"><p>باور از من کن و جز من مکن از کس باور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بعد از این از همه کس بگسل و با من پیوند</p></div>
<div class="m2"><p>کانچه از من به تو آید همه خیرست نه شرّ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکدل و یکجا در خانۀ من منزل کن</p></div>
<div class="m2"><p>آنچنان دان که خود این خریدی با زر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر چه بی مایه خریدارِ وِصالِ تو شدم</p></div>
<div class="m2"><p>علمِ من بین و به بی مایگی من مَنِگَر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هنری مرد به بدبختی و سختی نزیَد</p></div>
<div class="m2"><p>ور زیَد ، یک دو سه روزی نَبُوَد افزرونتر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من همان طُرفه نویسندۀ وقتم که بردند</p></div>
<div class="m2"><p>مُنشآتم را مشتاقان چون کاغذِ زر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من همان دانا گویندۀ دَهرم که خورند</p></div>
<div class="m2"><p>قَصَب الجَیبِ حدیثم را همچون شکّر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سعدیِ عصرم ، این دفتر و این دیوانم</p></div>
<div class="m2"><p>باورت نیست به دیوانم بین و دفتر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بهترین مردِ شرفمند در این مُلک منم</p></div>
<div class="m2"><p>همنشینِ تو که می باید از من بهتر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هیچ عیبی بجز از فقر ندارم باللّه</p></div>
<div class="m2"><p>فقر فخر است ولی تنها بر پیغمبر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همّتِ عالی با کیسۀ خالی دردی است</p></div>
<div class="m2"><p>که به آن درد گرفتار نگردد کافر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو مدارا کن امروز به درویشیِ من</p></div>
<div class="m2"><p>من تلافی کنم ار بخت به من شد یاور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای بسا مفلسِ امروز که فردا شده است</p></div>
<div class="m2"><p>صاحبِ خانه و ده ، مالکِ اسب و استر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من نه آنم که حقوقِ تو فراموش کنم</p></div>
<div class="m2"><p>گر رسد ریشِ تو از عارضِ تو تا به کمر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا مرا چشم بُوَد در عقبت می نگرم</p></div>
<div class="m2"><p>هم مگر کور شوم کز تو کنم صرفِ نظر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا مرا پای بُوَد بر اثرت می آیم</p></div>
<div class="m2"><p>مگر آن روز که بیچاره شوم در بِستر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به خدایی که به من فقر و به قارون زر داد</p></div>
<div class="m2"><p>گنجِ قارونم در دیده بود خاکستر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر چه کردم سخن از فقر تو اندیشه مدار</p></div>
<div class="m2"><p>نه چنان است که در کارِ تو مانم مضطر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با همه فقر کشم جورِ تو دارم جان</p></div>
<div class="m2"><p>با همه ضعف برم بارِ تو تا هست کمر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرچه آتش بِتَفَد چهرۀ آهنگر ، باز</p></div>
<div class="m2"><p>آرد از کوره برون آهنِ خود آهنگر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>من چو خورشیدِ جهان تابم و بینی خورشید</p></div>
<div class="m2"><p>خود برهنه است ولی بر همه بخشد زیور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هر چه از بهر تو لازم شود آماده کنم</p></div>
<div class="m2"><p>گرچه با کدِّیمین باشد و با خونِ جگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به فدایِ تو کنم جملۀ دارایی خویش</p></div>
<div class="m2"><p>ای رُخَت خوب تر از آینۀ اسکندر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حکم حکمِ تو و فرمایش فرمایشِ توست</p></div>
<div class="m2"><p>تو خداوندی در خانه و من فرمان بَر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نه به رویِ تو بیارم نه به کس شکوه کنم</p></div>
<div class="m2"><p>گر سرم بشکنی ار خانه کنی زیر و زبر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو به جز خنده نبینی به لبم گرچه مرا</p></div>
<div class="m2"><p>در دل انواع غُصَص باشد و اقسامِ فِکَر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر چه در کیسه من بینی برگیر و برو</p></div>
<div class="m2"><p>هر چه از خانۀ من می خواهی بردار و ببر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرچه از جامۀ من بینی خوبست بپوش</p></div>
<div class="m2"><p>جامۀ خوب تر ار هست به بازار ، بخر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>پیش رویِ تو نَهَم خوبترین لقمۀ چرب</p></div>
<div class="m2"><p>زیر بالِ تو کشم نرم ترین بالشِ پر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا توانم نگذارم که تو بی پول شوی</p></div>
<div class="m2"><p>گرچه بفروشم سرداریِ تن را به ضرر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آنچنان شیک و مد و خوب نگاهت دارم</p></div>
<div class="m2"><p>که زهر با مُدِ این شهر شوی با مُدتر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>جامه ات باید با جان متناسب باشد</p></div>
<div class="m2"><p>به پلاس اندر پیچید نَشاید گوهر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>پیشِ تو میرم پروانه صفت پیشِ چراغ</p></div>
<div class="m2"><p>دورِ تو گردم چون هاله که بر دورِ قمر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تنگ گیرم به برت نرم بخارم بدنت</p></div>
<div class="m2"><p>من یقیناً به تو دل سوزترم تا مادر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گَردِ سرداری و شلوارِ تو خود پاک کنم</p></div>
<div class="m2"><p>من به تزیینِ تو مشتاق ترم تا نوکر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پیرهن های تُرا جمله خود آهار زنم</p></div>
<div class="m2"><p>من ز آهار زدن واقفم و مستحضر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جا به خلوت دهمت تا که نبینند رخت</p></div>
<div class="m2"><p>تو پسر بچّه تفاوت نکنی با دختر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>زیر شلواری و پیراهن و شلوارِ تُرا</p></div>
<div class="m2"><p>شسته و رُفته و ناکرده بیارَمت به بر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>کفشِ تو واکس زده جامه اُطو خورده بُوَد</p></div>
<div class="m2"><p>هر سحر کان را در پاکنی این را در بر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یقه ات پاک و کلاهت نو و سردست تمیز</p></div>
<div class="m2"><p>عینک و دستکش و ساعت و پوتین در خور</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دستمالت را مخصوص معطّر سازم</p></div>
<div class="m2"><p>نه بدان باید تو خشک کنی عارضِ تر ؟</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تر و خشکت کنم آن سان که فراموش کنی</p></div>
<div class="m2"><p>آن شَفَقّت ها کز مادر دیدیّ و پدر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شب اگر بینم کز خواب گران گشته سرت</p></div>
<div class="m2"><p>سینه پیش آرم تا تکیه دهی بروی سر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نفس آهسته کشم دیده به هم نگذارم</p></div>
<div class="m2"><p>تا تو بر سینه ام آرامی شب تا به سحر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ور دلم خواست که یک بوسه به موی تو زنم</p></div>
<div class="m2"><p>آن چنان نرم زنم کت نشود هیچ خبر</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>شب بپوشانم رویِ تو چو یک کدبانو</p></div>
<div class="m2"><p>صبح برچینم جایِ تو چو یک خدمتگر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>چشم از خواب چو بگشودی پیشِ تو نَهَم</p></div>
<div class="m2"><p>سینی نان و پنیر و کره و شیر و شکر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>شانه و آینه و هوله و صابون و گلاب</p></div>
<div class="m2"><p>جمله با سینیِ دیگر نهمت در محضر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>آب ریزم که بشویی رخِ همچون قمرت</p></div>
<div class="m2"><p>آن که ناشُسته بَرَد آبِ رخِ شمس و قمر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>خود زنم شانه سرِ زلفِ دلارایِ تُرا</p></div>
<div class="m2"><p>نرم و هموار که یک مو نکند شانه هدر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بِسترِ خوابِ من ار تودۀ خاکستر بود</p></div>
<div class="m2"><p>از پیِ خوابِ تو آماده کنم تختِ فنر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>صندلی های تُرا نیز فنردار کنیم</p></div>
<div class="m2"><p>صندلی های فنردار بُوَد راحت تر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>آرم از بهر تو مشّاق و معلّم لیکِن</p></div>
<div class="m2"><p>درس و مشقت را خود گیرم در تحتِ نظر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>سعیِ استاد به کارِ تو نه چون سعیِ من است</p></div>
<div class="m2"><p>دایه هر قدر بُوَد خوب ، نگردد مادر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>هر قَدَر خسته کند مشغلۀ روز مرا</p></div>
<div class="m2"><p>شب ز تعلیمِ تو غفلت نکنم هیچ قَدَر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چشم بر هم نزنم گرچه مرا خواب آید</p></div>
<div class="m2"><p>تا تو درسِ خود پاکیزه نمایی از بر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>صد غلط داشته باشی همه را می گویم</p></div>
<div class="m2"><p>گربه یک بار نفهمیدی یک بارِ دگر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از کتاب و قلم و قیچی و چاقو و دوات</p></div>
<div class="m2"><p>هر چه دارم به تو خواهم داد ای شوخ پسر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>هفته‌ای یک شب از بهرِ نشاطِ دلِ تو</p></div>
<div class="m2"><p>تار و سنتور فراهم کنم و رامشگر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>جمعه ها پول درشه دهمت تا بروی</p></div>
<div class="m2"><p>گه معینیّه ، گهی شِمران ، گه قصرِ قَجَر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ور کنی گاهی در کوه و کمر قصدِ شکار</p></div>
<div class="m2"><p>از پس و پیشِ تو بشتابم در کوه و کمر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>هم انیسِ شبِ من باشی و هم مونسِ روز</p></div>
<div class="m2"><p>هم رفیقِ سفرم گردی و هم یارِ حضر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>شب که از درس شدی خسته و از مشق کسل</p></div>
<div class="m2"><p>نقل گویم به تو از روی تواریخ و سِیر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>قصّه ها بهر تو خوانم که بَرَش هیچ بُوَد</p></div>
<div class="m2"><p>به علی قصۀ عثمان و ابوبکر و عُمَر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>یک دو سالی که شوی مهمان در خانۀ من</p></div>
<div class="m2"><p>مرد آراسته‌ای کردی با فضل و هنر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>عربی خوان و زبان دان شوی و تاریخی</p></div>
<div class="m2"><p>صاحبِ بهره ز فقه و ز حدیث و ز خبر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>خط نویسی که اگر بیند امیرُالکُتاب</p></div>
<div class="m2"><p>کند فرار که به نوشته‌ای از وی بهتر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>شعر گویی که اگر بشنود آقای مَلِک</p></div>
<div class="m2"><p>آفرین گوید بر شاعر و شاعرپرور</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>داخلِ خدمتِ دولت کنمت چندی بعد</p></div>
<div class="m2"><p>آیی از جملۀ اعضای دوائر به شُمَر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ابتدا گردی نبّات و سپس آرشیویست</p></div>
<div class="m2"><p>بعد منشی شوی و بعد رئیسِ دفتر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>گر خدا خواست رئیس الوزرا نیز شوی</p></div>
<div class="m2"><p>من چنین دیده ام اندر نَفَسِ خویش اثر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>آنچه در کارِ تو از دستِ من آید اینست</p></div>
<div class="m2"><p>بیش از این آرزویی در دل تو هست مگر ؟</p></div></div>