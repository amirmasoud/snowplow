---
title: >-
    شمارهٔ  ۱ - در رثاء درة المعالی
---
# شمارهٔ  ۱ - در رثاء درة المعالی

<div class="b" id="bn1"><div class="m1"><p>شد فصل بهار و گُل صلا داد</p></div>
<div class="m2"><p>بر چهرۀ خوب خود صفا داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد سحری ز آشنایی</p></div>
<div class="m2"><p>پیغام وفا به آشنا داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل ز فراق چند ماهه</p></div>
<div class="m2"><p>باز آمد و شرح ماجرا داد</p></div></div>
<div class="b2" id="bn4"><p>افسوس که جای تُست خالی</p></div>
<div class="b2" id="bn5"><p>ای خانم درة المعالی</p></div>
<div class="b" id="bn6"><div class="m1"><p>آوخ که بهار ما خزان شد</p></div>
<div class="m2"><p>آن روی چو گُل ز ما نهان شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p></p></div>
<div class="m2"><p>از چشمۀ چشم ما روان شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوناب جگر ز فُرقتِ تو</p></div>
<div class="m2"><p>در باغ نصیب ما فَغان شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل صفت از فراق رویَت</p></div>
<div class="b2" id="bn10"><p>افسوس که جای تُست خالی</p></div>
<div class="b2" id="bn11"><p>ای خانم درة المعالی</p></div>
<div class="b" id="bn12"><div class="m1"><p>گرییم ز درد اشتیاقت</p></div>
<div class="m2"><p>سوزیم در آتش فراقت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p></p></div>
<div class="m2"><p>بینیم ز دوستان چو طاقت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جفت المیم و یار اندوه</p></div>
<div class="m2"><p>آییم چو بی تو در وثاقت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوییم ز روی درد و حسرت</p></div>
<div class="b2" id="bn16"><p>افسوس که جای تست خالی</p></div>
<div class="b2" id="bn17"><p>ای خانم درة المعالی</p></div>
<div class="b" id="bn18"><div class="m1"><p>از ما چه خلاف دیده بودی</p></div>
<div class="m2"><p>کاین گونه مفار نمودی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p></p></div>
<div class="m2"><p>رفتی و ز دست ما ربودی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سر رشتۀ اتحاد ما را</p></div>
<div class="m2"><p>در خاک سیه چرا غنودی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جای تو به روی چشم ما بود</p></div>
<div class="b2" id="bn22"><p>افسوس که جای تست خالی</p></div>
<div class="b2" id="bn23"><p>ای خانم درة المعالی</p></div>