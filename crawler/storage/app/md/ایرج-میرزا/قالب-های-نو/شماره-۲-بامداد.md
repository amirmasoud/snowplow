---
title: >-
    شمارهٔ  ۲ - بامداد
---
# شمارهٔ  ۲ - بامداد

<div class="b" id="bn1"><div class="m1"><p>صبح دم کاین طایر چرخ آشیان</p></div>
<div class="m2"><p>آفتابی گردد از بالای کوه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تفته رخ، بال کوبان، پرزنان</p></div>
<div class="m2"><p>از پر و بالش چمن گیرد شکوه</p></div></div>
<div class="b2" id="bn3"><p>نغمه خوان مرغ سحر بر شاخسار</p></div>
<div class="b" id="bn4"><div class="m1"><p>بینی آن پروانۀ خوش خال و خط</p></div>
<div class="m2"><p>جسته بیرون از غلاف پیرهن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با پر و بالی پر از زرین نُقَط</p></div>
<div class="m2"><p>سر زند یک یک به گُل های چمن</p></div></div>
<div class="b2" id="bn6"><p>بوسد این را غبغب و آن را عذار</p></div>
<div class="b" id="bn7"><div class="m1"><p>***</p></div>
<div class="m2"><p>***</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچنان آن طفلک شیرین زبان</p></div>
<div class="m2"><p>با رخی سرخ و سپید از شیر و خون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن دو چشم برق زن چون اختران</p></div>
<div class="m2"><p>سر کند شادان ز شادیجه برون</p></div></div>
<div class="b2" id="bn10"><p>بنگرد اطراف خود را شاد خوار</p></div>
<div class="b" id="bn11"><div class="m1"><p>با تبسم های شیرین تر ز قند</p></div>
<div class="m2"><p>همچو پروانه گشاید بال و پر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر جهد از جا چو از مجمر سپند</p></div>
<div class="m2"><p>دست مادر بوسد و روی پدر</p></div></div>
<div class="b2" id="bn13"><p>این در آغوشش کَشَد آن در کنار</p></div>