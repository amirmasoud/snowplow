---
title: >-
    شمارهٔ  ۶
---
# شمارهٔ  ۶

<div class="b" id="bn1"><div class="m1"><p>نشسته بودم و دیدم ز در بشیر آمد</p></div>
<div class="m2"><p>که خیز و جان و دل آماده کن امیر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امیرِ مملکتِ حُسن با چنان حشمت</p></div>
<div class="m2"><p>چه خواب دید که سر وقتِ این فقیر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دید از غمِ هجرانش سخت دلگیرم</p></div>
<div class="m2"><p>به دلنوازیِ این پیرِ گوشه گیر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمانده بود مرا طاقتِ جدایی او</p></div>
<div class="m2"><p>به موقع آمد و نیک آمد و هُژیر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکرده جنگ اسیرم نموده بود به خویش</p></div>
<div class="m2"><p>کنون به سرکشیِ موقفِ اسیر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکایتِ شبِ هجران به او نباید کرد</p></div>
<div class="m2"><p>که خود ز دردِ دلِ عاشقان خبیر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه زور بود که بر پیکرِ علیل رسید</p></div>
<div class="m2"><p>چه نور بود که در دیدۀ ضریر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون که آمده تا نصف شب نگاهش دار</p></div>
<div class="m2"><p>ز دست زود مده دامنش که دیر آمد</p></div></div>