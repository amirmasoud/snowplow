---
title: >-
    شمارهٔ  ۷
---
# شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>شکر خدا را که بخت هادیم آمد</p></div>
<div class="m2"><p>نامه‌ای از حاج شیخ هادیم آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پس سرگشتگی به وادی حیرت</p></div>
<div class="m2"><p>هادیِ سر منزلِ ایادیم آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پسِ یک عمر رنج در طلبِ گنج</p></div>
<div class="m2"><p>هادیِ آن کانِ فضل و رادیم آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وز رشحاتِ غَمامِ فضل و کمالش</p></div>
<div class="m2"><p>نامه‌ای امروز بهرِ شادیم آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده در آن نامه از مکارم و اَلطاف</p></div>
<div class="m2"><p>درج بدان حدّ که خود زیادیم آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد بساط مرا نشاط ربیعی</p></div>
<div class="m2"><p>گرچه مر آن نامه در جُمادیم آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ چو دانست بر مراد رسیدم</p></div>
<div class="m2"><p>دی پیِ تمهیدِ نامُرادیَم آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد ز خانه مرا برون و به خانه</p></div>
<div class="m2"><p>حضرتِ ذی قدرِ اوستادیم آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچ ز حرمانِ خود شگفت ندارم</p></div>
<div class="m2"><p>کاینهمه از سوء بختِ عادیم آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درکِ لقایش غنیمتی است مه برچنگ</p></div>
<div class="m2"><p>از سفرِ این خجسته وادیم آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خواستم افزون کنم سخن به مدیحش</p></div>
<div class="m2"><p>قافیه بُد تنگ کون‌گشادیم آمد</p></div></div>