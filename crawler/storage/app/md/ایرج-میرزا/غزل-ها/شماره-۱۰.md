---
title: >-
    شمارهٔ  ۱۰
---
# شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>به دست جام شراب و به گوش نغمۀ ساز</p></div>
<div class="m2"><p>شبی خوشست خدایا دراز باد دراز !</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چگونه کوته خواهم شبی که اندر وی</p></div>
<div class="m2"><p>وصالِ دوست مهیّا و برگِ عشرت ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه کوته خواهم شبی که سعدی گفت:</p></div>
<div class="m2"><p>«که دوست را ننماید شبِ وِصال دراز»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی بُوَد که ازو گشت شامِ دولت روز</p></div>
<div class="m2"><p>شبی بُوَد که ازو گشت صبحِ ملّت باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی بُوَد که بتابید اندرو ماهی</p></div>
<div class="m2"><p>که آفتاب نیارد شدن به او انباز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبی است فرّخ و شهزاده نصرت الدّولهُ</p></div>
<div class="m2"><p>نموده جشنی از عزّت و جلال جهاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه جشنی مانندِ جَنَّتِ موعود</p></div>
<div class="m2"><p>ز چار جانب بگشوده بابِ نعمت و ناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به وجد اندر هر سوی گلرخانِ چکل</p></div>
<div class="m2"><p>به رقص اندر هرجای مهوشانِ طَراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه درخشد مانند نارِ ذاتِ وَقود</p></div>
<div class="m2"><p>شرابِ گلگون اندر به سیمگون بِگماز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز هر طرف شنوی نغمه های رود و سرود</p></div>
<div class="m2"><p>به هر کجا نگری گونه های ساز و نواز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز چرخ گوید ناهید از پی تبریک</p></div>
<div class="m2"><p>خجسته بادا میلادِ شاهِ بنده نواز</p></div></div>