---
title: >-
    شمارهٔ  ۱۶
---
# شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>باز روز آمد به پایان شامِ دلگیر است و من</p></div>
<div class="m2"><p>تا سحر سودایِ آن زلفِ چو زنجیر است و من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگران سر مست در آغوشِ جانان خفته اند</p></div>
<div class="m2"><p>آنکه بیدارست هر شب مرغِ شبگیر است و من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته بودم زودتر در راهِ عشقت جان دهم</p></div>
<div class="m2"><p>بعد از این تا زنده باشم عُذرِ تأخیر است و من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سُبحَه و سَجّاده و مُهری مرتّب کرده شیخ</p></div>
<div class="m2"><p>تا چه پیش آید خدا یا دامِ تزویر است و من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از درِ شاهانِ عالَم لَذَّتی حاصل نشد</p></div>
<div class="m2"><p>بعد از این در کنجِ عُزلت خدمتِ پیر است و من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با چنین رعنا غزالی خدعه ساز و عشوه باز</p></div>
<div class="m2"><p>پنجه اندر پنجه کردن قوّۀ شیر است و من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر گرفتاری کند تدبیرِ استخلاصِ خویش</p></div>
<div class="m2"><p>تا گرفتارش شوم پیوسته تدبیر است و من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مَنعَم از کوشش مکن ناصح که آخِر می رسم</p></div>
<div class="m2"><p>یا به جانان یا به جان میدانِ تقدیر است و من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نویسم شِمّه‌ای از شرحِ دردِ اشتیاق</p></div>
<div class="m2"><p>از سرِ شب تا سحر اسبابِ تحریر است و من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه می خواهم که گوید در رخِ اعدایِ مُلک</p></div>
<div class="m2"><p>قطع و فصلِ این دعاوی کارِ شمشیر است و من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در نظامِ امرِ کشور در رواجِ خطِّ عشق</p></div>
<div class="m2"><p>آنکه بتواند سرافرازی کند میر است و من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خواجۀ اعظم نظام السّلطنه کز خدمتش</p></div>
<div class="m2"><p>آنکه نازد بر زمین و آسمان تیر است و من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پیش اربابِ هنر در یک دو بیت از این غزل</p></div>
<div class="m2"><p>قافیه گر شایگان شد عذر تقصیر است و من</p></div></div>