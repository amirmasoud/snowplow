---
title: >-
    شمارهٔ  ۲۲ - ماده تاریخ مهمانی شاه در باغ امیرنظام  (۱۳۰۷ هق)
---
# شمارهٔ  ۲۲ - ماده تاریخ مهمانی شاه در باغ امیرنظام  (۱۳۰۷ هق)

<div class="b" id="bn1"><div class="m1"><p>خسرو تاج بخش ناصر دین</p></div>
<div class="m2"><p>آن سرشته به عقل و دانش داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن که دست عطا و همت او</p></div>
<div class="m2"><p>حاصل بحر و کان به باد بداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود سیم سفر که از تبریز</p></div>
<div class="m2"><p>شد به سوی فرنگ خسرو راد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر به تبریز چون امیر نظام</p></div>
<div class="m2"><p>باغ و کاخی نُموده بود آباد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اِیاب و دِهاب مهمان شد</p></div>
<div class="m2"><p>اندر آن باغ شاه با دلِ شاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه قدم چون نهاد در آن باغ</p></div>
<div class="m2"><p>در دولت بد روی میر گشاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر و سیم زیاد بهر نثار</p></div>
<div class="m2"><p>زیر پای ملک امیر نهاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همۀ چاکران سلطان را</p></div>
<div class="m2"><p>شال و اسل و طلا و نقره بداد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با دل شاد شاه از این کشور</p></div>
<div class="m2"><p>به سوی پایتخت روی نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سفر شه بنای باغ امیر</p></div>
<div class="m2"><p>چون به یک سال اتفاق افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر تاریخ سال ایرج گفت</p></div>
<div class="m2"><p>«باغ میر اجل بُوَد آباد»</p></div></div>