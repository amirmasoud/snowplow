---
title: >-
    شمارهٔ  ۵ - دربارۀ رفتن مستوفی الممالک و آمدنِ صَمصامُ السلطنه
---
# شمارهٔ  ۵ - دربارۀ رفتن مستوفی الممالک و آمدنِ صَمصامُ السلطنه

<div class="b" id="bn1"><div class="m1"><p>این شنیدم که چوکابینۀ مستوفی رفت</p></div>
<div class="m2"><p>فرصت افتاد به کف مَردُمِ فرصت جُورا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وطن خواهان یک عدّه به هم جمع شدند</p></div>
<div class="m2"><p>عرضه کردند شهنشاهِ فلک نیرو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاندر این مُلکِ رئیس الوزرایی باید</p></div>
<div class="m2"><p>که به اِعجاز کند سُخرۀ خود جادو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاردانی که به تدبیرِ خرد حلّ سازد</p></div>
<div class="m2"><p>این همه مشکلِ خَم در خمِ تُودَر تُورا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پهلوان مردی فَعّال و زَبَردست و قوی</p></div>
<div class="m2"><p>که ببندد دهن و باز کند بازو را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیزهوشی که رهانَد وطن از بندِ بلا</p></div>
<div class="m2"><p>آن چنان سهل که از ماست کَشد کَس مو را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه فرمود: من اقدام به کاری نکنم</p></div>
<div class="m2"><p>تا نَسنجَم همه خوب و بد و زیر و رو را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکر باید، که رئیس الوزرا نتوان کرد</p></div>
<div class="m2"><p>!هر خرِ بی خردِ با طمع پُررو را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مُهلتی بایَد کاندیشم وزان پس بکنم</p></div>
<div class="m2"><p>انتخابی که ببندد دهنِ بدگو را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه گشتند از این عزمِ همایون خُرسند</p></div>
<div class="m2"><p>همه گفتند: مَلِک زنده بماند، هورا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعدِ یک هَفته مَلِک داد به آنان پیغام</p></div>
<div class="m2"><p>که نکو جُستم بر دردِ شما دارو را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از اندیشه مرا رای به صَمصام افتاد</p></div>
<div class="m2"><p>!از همه خلق پسندیدم این هالو را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکرِ خود کردم و کَردَمش رئیس الوزرا</p></div>
<div class="m2"><p>همه بِستایید این مُنتَخَبِ نیکو را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خلق رفتند در اندیشه و حَیران ماندند</p></div>
<div class="m2"><p>که از این کرده چه مقصود بُوَد یارو را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی از جمع بپرسید ز گوینده، که شاه</p></div>
<div class="m2"><p>!فکر هم کرد و رئیس الوُزَرا کرد او را؟</p></div></div>