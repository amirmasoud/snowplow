---
title: >-
    شمارهٔ  ۲۶ - بقای انسب
---
# شمارهٔ  ۲۶ - بقای انسب

<div class="b" id="bn1"><div class="m1"><p>قصّه شنیدم که بوالعَلا به همه عمر</p></div>
<div class="m2"><p>لَحم نخورد و ذَوات لَحم نیازرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مرض موت با اجازه دستور</p></div>
<div class="m2"><p>خادم او جوجه با به محضر او برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه چو آن طیر کشته دید برابر</p></div>
<div class="m2"><p>اشک تَحَسُر ز هر دو دیده بیفشرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت چرا ماکیان شدی نشدی شیر</p></div>
<div class="m2"><p>تا نتواند کَسَت به خون کشد و خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ برای ضعیف امر طبیعی است</p></div>
<div class="m2"><p>هر قوی اوّل ضعیف گشت و سپس مُرد</p></div></div>