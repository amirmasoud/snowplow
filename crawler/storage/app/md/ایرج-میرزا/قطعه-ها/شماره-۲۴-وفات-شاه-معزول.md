---
title: >-
    شمارهٔ  ۲۴ - وفات شاه معزول
---
# شمارهٔ  ۲۴ - وفات شاه معزول

<div class="b" id="bn1"><div class="m1"><p>اگر شاه معزول رفت از جهان</p></div>
<div class="m2"><p>ولی عهد منصوب پاینده باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمد علی میرزا گر بمرد</p></div>
<div class="m2"><p>محمد حسن میرزا زنده باد</p></div></div>