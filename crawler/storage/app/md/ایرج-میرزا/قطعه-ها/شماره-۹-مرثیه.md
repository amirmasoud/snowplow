---
title: >-
    شمارهٔ  ۹ - مرثیه
---
# شمارهٔ  ۹ - مرثیه

<div class="b" id="bn1"><div class="m1"><p>سرگشته بانوان وَسَطِ آتشِ خِیام</p></div>
<div class="m2"><p>چون در میانِ آبْ نُقُوش ستاره‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اطفالِ خردسال ز اطرافِ خیمه‌ها</p></div>
<div class="m2"><p>هرسو دوان چو از دِل آتشْ شَراره‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر از جگر که دسترسِ اَشْقِیا نبود</p></div>
<div class="m2"><p>چیزی نَمانْد در برِ ایشان ز پاره‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انگشت رَفت دو سرِ انگشتری به باد</p></div>
<div class="m2"><p>شد گوش‌ها دریده پیِ گوشْواره‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سِبطِ شهی که نامِ همایونِ او بَرَند</p></div>
<div class="m2"><p>هر صبح و ظهر و شام فرازِ مَناره‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خاک و خون فُتاده و تازَنْد بر تَنَش</p></div>
<div class="m2"><p>با نعل‌ها که ناله برآرَدْ ز خاره‌ها</p></div></div>