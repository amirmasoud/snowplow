---
title: >-
    شمارهٔ  ۶۴ - گریز از نادان
---
# شمارهٔ  ۶۴ - گریز از نادان

<div class="b" id="bn1"><div class="m1"><p>دادم به مسیوهال خراسان را</p></div>
<div class="m2"><p>عیسی صفت گریختم از احمق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نادان به کارها شده مستولی</p></div>
<div class="m2"><p>دانا به خون دل شده مستغرق</p></div></div>