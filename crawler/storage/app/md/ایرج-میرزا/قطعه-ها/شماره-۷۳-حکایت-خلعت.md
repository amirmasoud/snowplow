---
title: >-
    شمارهٔ  ۷۳ - حکایت خلعت
---
# شمارهٔ  ۷۳ - حکایت خلعت

<div class="b" id="bn1"><div class="m1"><p>مستوفی کل قصۀ چل طوطی شد</p></div>
<div class="m2"><p>امسال چرا حکایت خلعتِ من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر روز همی وعده به فردا دهیم</p></div>
<div class="m2"><p>فردا نشود تمام در دورِ زَمَن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عهدۀ تعویق گر اُفتَد ز این پیش</p></div>
<div class="m2"><p>این خلعتِ آخر است یعنی که کفن</p></div></div>