---
title: >-
    شمارهٔ  ۸۸ - تقاضا گله از مُعز الملک
---
# شمارهٔ  ۸۸ - تقاضا گله از مُعز الملک

<div class="b" id="bn1"><div class="m1"><p>ای معزالملک ای اندر سخا ضرب المثل</p></div>
<div class="m2"><p>از چه رو شعر و خط ما را گرفتی سرسری؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد نکردم من که چونین گوهر ارزنده را</p></div>
<div class="m2"><p>با ادب کردم نثار بزم چون تو گوهری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شعر و خط من بود آن گوهر سنگین بها</p></div>
<div class="m2"><p>که امیر مملکت باشد مر او را مشتری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فخر میران زمانه حضرت میر نظام</p></div>
<div class="m2"><p>آن که بر او فخر دارد دانش و دانشوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرخص می کنی اندر حضور این امیر</p></div>
<div class="m2"><p>می توانم تا برم من با ادب این داوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چو بر اسب سخن رانی سوار آیم بود</p></div>
<div class="m2"><p>هم رکابم فرخی و هم عنانم عنصری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ختم بر من گشته شعر و شاعری چونانکه شد</p></div>
<div class="m2"><p>بر محمد خاتم پیغمبران پیغمبری</p></div></div>