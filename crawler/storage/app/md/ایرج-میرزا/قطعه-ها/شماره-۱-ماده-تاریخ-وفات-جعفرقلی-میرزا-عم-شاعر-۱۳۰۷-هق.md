---
title: >-
    شمارهٔ  ۱ - مادّه تاریخ وفات جعفرقلی میرزا عَمِّ شاعر  ۱۳۰۷ هق
---
# شمارهٔ  ۱ - مادّه تاریخ وفات جعفرقلی میرزا عَمِّ شاعر  ۱۳۰۷ هق

<div class="b" id="bn1"><div class="m1"><p>هر که آمد در این جهان ناچار</p></div>
<div class="m2"><p>رود از این جهان چه شَه چه گدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جَهانِ دگر خدای آراست</p></div>
<div class="m2"><p>که بُوَد نامِ آن جهانِ بقا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سویِ دارِ بَقا رَوَد هر کس</p></div>
<div class="m2"><p>که بیامد در این سرایِ فنا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پورِ ایرج نَوادۀ خاقان</p></div>
<div class="m2"><p>آن مَلِک زادۀ فرشته لَقا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به او صِهر و او به من عَمّ بود</p></div>
<div class="m2"><p>نه من او را نه او پدید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیست پنجاه و اَندر سال به دَهر</p></div>
<div class="m2"><p>چون در این خاکدان ندید وفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سویِ جَنَّت برفت با دلِ شاد</p></div>
<div class="m2"><p>تا بماناد جاوادان آن جا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهرِ تاریخِ فوتش ایرج گفت</p></div>
<div class="m2"><p>رفت جعفرقُلی از این دنیا</p></div></div>