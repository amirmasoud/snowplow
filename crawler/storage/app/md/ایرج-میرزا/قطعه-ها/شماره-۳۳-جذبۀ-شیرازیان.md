---
title: >-
    شمارهٔ  ۳۳ - جذبۀ شیرازیان
---
# شمارهٔ  ۳۳ - جذبۀ شیرازیان

<div class="b" id="bn1"><div class="m1"><p>حضرت شوریده اوستاد سخن سنج</p></div>
<div class="m2"><p>آن که همه چیز بهتر از همه داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد صبا گر گذر به پارس نُماید</p></div>
<div class="m2"><p>شعر مرا از لحاظ او گذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده ندانیم که در کجا رَوَم آخِر</p></div>
<div class="m2"><p>جذبۀ شیرازیان مرا بکشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسکن شوریده است و مدفن سعدی</p></div>
<div class="m2"><p>شهر دگر همسری به او نتواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازم از این جایگاهِ نغز دل افروز</p></div>
<div class="m2"><p>تا به کجا دست روزگار براند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می روم آن جا که روزگار بخواهد</p></div>
<div class="m2"><p>می کَشَم آن جا که آسمان بکشاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده همین قدر شاکرم که به شیراز</p></div>
<div class="m2"><p>هر که شبی دلبری به پر بنشاند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاد من افتد در آن دقیقه و از دور</p></div>
<div class="m2"><p>بوسۀ چُندی به جایِ من بستانَد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوید جایِ جلال خالی و آن گاه</p></div>
<div class="m2"><p>لَذَّتِ آن بوسه را به من بپراند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بعد وفاتم میان مردمِ شیراز</p></div>
<div class="m2"><p>این سخن از من به یادگار بماند</p></div></div>