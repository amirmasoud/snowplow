---
title: >-
    شمارهٔ  ۱۲ - تقاضای وجهِ قبض
---
# شمارهٔ  ۱۲ - تقاضای وجهِ قبض

<div class="b" id="bn1"><div class="m1"><p>وزیرِخمسه اگر وجهِ قبضِ من ندهد</p></div>
<div class="m2"><p>به حقِّ خمسهٔ آلِ عبا که بَد کَرْدَست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر تَعَلُّل بیشتر روا دارد</p></div>
<div class="m2"><p>حقوقِ دوستی و مَردُمی لَگَد کردست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر چه عرض کُنَم دیرتر اگر بدهد</p></div>
<div class="m2"><p>به دست خود چه بلا ها به جان خود کردست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی شناسد من کیستم ، گمان دارد</p></div>
<div class="m2"><p>که این معامله با مادر صمَدْ کردست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیاده وقت ندارم همین قَدَرْ تو بگو</p></div>
<div class="m2"><p>که پول خواهد ایرج چو قبضْ رد کردست</p></div></div>