---
title: >-
    شمارهٔ  ۴۵ - رم
---
# شمارهٔ  ۴۵ - رم

<div class="b" id="bn1"><div class="m1"><p>یارب این عادت چه می‌باشد که اهل ملک ما</p></div>
<div class="m2"><p>گاه بیرون رفتن از مجلس ز در رم می‌کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله بنشینند با هم خوب و برخیزند خوش</p></div>
<div class="m2"><p>چون به پیش در رسند از همدگر رم می‌کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنان در موقع وارد شدن در مجلسی</p></div>
<div class="m2"><p>گه ز پیش رو گهی از پشت سر رم می‌کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردم در این یکی بر چپ رود آن یک به راست</p></div>
<div class="m2"><p>از دو جانب دوخته بر در نظر رم می‌کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر زبان آرند بسم الله بسم الله را</p></div>
<div class="m2"><p>گوییا جن دیده یا از جانور رم می‌کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این که وقت رفت و آمد بود اما این گروه</p></div>
<div class="m2"><p>در نشستن نیز یک نوع دگر رم می‌کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این یکی چون می‌نشیند دیگری ور می‌جهد</p></div>
<div class="m2"><p>تا دو نوبت گاه کم گه بیشتر رم می‌کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فرضا اندر مجلسی گر ده نفر بنشسته بود</p></div>
<div class="m2"><p>چون یکی وارد شود هر ده نفر رم می‌کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی اندر صحنه مجلس فنر بنشانده‌اند</p></div>
<div class="m2"><p>چون یکی پا می‌نهد روی فنر رم می‌کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نام این رم را چو نادانان ادب بنهاده‌اند</p></div>
<div class="m2"><p>بیشتر از صاحبان سیم و زر رم می‌کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از برای رنجبر رم مطلقا معمول نیست</p></div>
<div class="m2"><p>تا توانند از برای گنجور رم می‌کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر وزیری از در آید رم مفصل می‌شود</p></div>
<div class="m2"><p>دیگر آن جا اهل مجلس معتبر رم می‌کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ حیوانی ز جنس خود ندارد احتراز</p></div>
<div class="m2"><p>این بشرها از هیولای بشر رم می‌کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو آن اسبی که بر من داده میر کامگار</p></div>
<div class="m2"><p>بی‌خبر رم می‌کنند و باخبر رم می‌کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رم نه تنها کار این اسب سیاه مخلص است</p></div>
<div class="m2"><p>مردم این مملکت هم مثل خر رم می‌کنند</p></div></div>