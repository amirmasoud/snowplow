---
title: >-
    شمارهٔ  ۵۹ - مزاح با یکی از دوستان
---
# شمارهٔ  ۵۹ - مزاح با یکی از دوستان

<div class="b" id="bn1"><div class="m1"><p>چند تو را گفتم ای کمال مخور کیر</p></div>
<div class="m2"><p>تا نشوی مبتلا به رنج بواسیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون به جوانی تو پند من نشنیدی</p></div>
<div class="m2"><p>رنج بواسیر کش کنون که شدی پیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیر بواسیر آورد، همه دانند</p></div>
<div class="m2"><p>درد گلو زاید از زیادی انجیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرما افزون خوری خناق بگیری</p></div>
<div class="m2"><p>کیر ندارد به قدر خرما تاثیر؟!</p></div></div>