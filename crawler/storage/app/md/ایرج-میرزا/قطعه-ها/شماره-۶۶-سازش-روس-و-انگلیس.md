---
title: >-
    شمارهٔ  ۶۶ - سازش روس و انگلیس
---
# شمارهٔ  ۶۶ - سازش روس و انگلیس

<div class="b" id="bn1"><div class="m1"><p>گویند که انگلیس با روس</p></div>
<div class="m2"><p>عهدی کردست تازه امسال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاندر پُلتِیک هم در ایران</p></div>
<div class="m2"><p>زین پس نکنند هیچ اهمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسوس که کافیانِ این مُلک</p></div>
<div class="m2"><p>بنشسته و فارِغند ازین حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کز صلحِ میانِ گربه و موش</p></div>
<div class="m2"><p>بر باد رود دُکانِ بَقّال</p></div></div>