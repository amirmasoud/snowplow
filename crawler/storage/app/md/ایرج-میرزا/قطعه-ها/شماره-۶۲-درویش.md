---
title: >-
    شمارهٔ  ۶۲ - درویش
---
# شمارهٔ  ۶۲ - درویش

<div class="b" id="bn1"><div class="m1"><p>کیست آن بی شعور درویشی</p></div>
<div class="m2"><p>که همیشه به لب بُوَد خاموش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه کند هیچ گفتگو با کس</p></div>
<div class="m2"><p>نه به حرفِ کسی نماید گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کارهایی کند سفیهانه</p></div>
<div class="m2"><p>خارق عادت و مخالف هوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مثلا در هوای گرم تموز</p></div>
<div class="m2"><p>خرقۀ پشم افکند بر دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک در عین سَورت سرما</p></div>
<div class="m2"><p>تن برهنه نماید از تن پوش</p></div></div>