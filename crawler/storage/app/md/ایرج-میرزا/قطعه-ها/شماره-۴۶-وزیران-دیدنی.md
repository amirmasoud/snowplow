---
title: >-
    شمارهٔ  ۴۶ - وزیران دیدنی
---
# شمارهٔ  ۴۶ - وزیران دیدنی

<div class="b" id="bn1"><div class="m1"><p>وزرا از چه دیده می نشوند</p></div>
<div class="m2"><p>راستی مردمانِ دیدنیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی غلط گفتم این معیدی ها</p></div>
<div class="m2"><p>دیدنی نه، همان شنیدنیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا وزیرند از کسان ببرند</p></div>
<div class="m2"><p>الحق این ناکَسان بُریدنیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وِثاقند و نیستند در آن</p></div>
<div class="m2"><p>ثابت و محو چون شنیدنیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چه پرده وَصفشان گویم</p></div>
<div class="m2"><p>بعضی از پرده ها دَریدَنیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وُزَرا حکم ضِرطَه را دارند</p></div>
<div class="m2"><p>که شنیده شوند و دیده نیند</p></div></div>