---
title: >-
    شمارهٔ  ۷۵ - مزاح با مقبل دیوان
---
# شمارهٔ  ۷۵ - مزاح با مقبل دیوان

<div class="b" id="bn1"><div class="m1"><p>قنبل الدوله مقبل دیوان</p></div>
<div class="m2"><p>آن که نبُوَد مثال او شیطان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قد او نیست جز چهار وجب</p></div>
<div class="m2"><p>نصف او گشته در زمین پنهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ سروی به قامتش نرسد</p></div>
<div class="m2"><p>در زمانه به هیچ سروستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبُوَد همچو قد او سروی</p></div>
<div class="m2"><p>نه به تهران و نه به تویسرکان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طفولیت او گذر می کرد</p></div>
<div class="m2"><p>یکشب از راه رشت زی زنجان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این شنیدم که بچۀ گرگی</p></div>
<div class="m2"><p>کون او را درید با دندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لیک گویند زخم کیرست این</p></div>
<div class="m2"><p>زده او بچه گرگ را بهتان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفته تا در ادارۀ اوقاف</p></div>
<div class="m2"><p>کرده او کون خویش وقف جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بسا خورده وقف مردم را</p></div>
<div class="m2"><p>حال از کون خود دهد تاوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در میان تمام مأکولات</p></div>
<div class="m2"><p>میل دارد بسی به بادنجان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بیند ار عکس کیر در دریا</p></div>
<div class="m2"><p>دل به دریا زند بدون گمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خایه اش دانی از چه پاره شده</p></div>
<div class="m2"><p>بس زَدستند زیر او رندان</p></div></div>