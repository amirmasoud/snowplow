---
title: >-
    شمارهٔ  ۷۷ - حیله
---
# شمارهٔ  ۷۷ - حیله

<div class="b" id="bn1"><div class="m1"><p>دیشب دو نفر از رفقا آمده بودند</p></div>
<div class="m2"><p>در محضر من ساخته بر ماحضر از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همراه یکیشان پسری بود که گفتی</p></div>
<div class="m2"><p>چشمانش طلب میکند ارث پدر از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از در نرسیده به همان نظر اول</p></div>
<div class="m2"><p>دین و دل و دانش بربود آن پسر از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که خدایا ز من این قوم چه خواهند</p></div>
<div class="m2"><p>ثابت طلبی دارند اینان مگر از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناخوانده و خوانده چو بلا بر سرم آیند</p></div>
<div class="m2"><p>دارند تمنا همه بی حد و مر از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نرد آمد و مشغول شدند آن دو ولی من</p></div>
<div class="m2"><p>در حیله که خوش دل شود این یک نفر از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم تو هم ای مغبچه بی مشغله منشین</p></div>
<div class="m2"><p>کابینه قلبت نپذیرد کدر ازمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش آی و بزن با من دلباخته پاسور</p></div>
<div class="m2"><p>شاید که یکی سور بری معتبر از من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتا که سر سور زدن کار جفنگیست</p></div>
<div class="m2"><p>ضایع چه کنی وقت خوشی بی ثمر از من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم سر هرچ آنکه تو گویی و تو خواهی</p></div>
<div class="m2"><p>پیش آی و ورق ده که کلاه از تو سر از من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر من ببرم از تو دو جوراب ستانم</p></div>
<div class="m2"><p>بستان تو یکی قوطی سیگار زر از من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زیبا پسر این شرط چو بشنید پسندید</p></div>
<div class="m2"><p>زیرا که همه سود از او بُد ضرر از من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خادم شد و یک دسته ورق داد و کشیدیم</p></div>
<div class="m2"><p>شد چار ورق از وی و چارِ دگر از من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پشت سر هر یک ورقی یک عرقش داد</p></div>
<div class="m2"><p>خادم که در این فن بود استادتر از من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیمود بدانسان که زمانی نشده بیش</p></div>
<div class="m2"><p>من بدتر از او مست شدم او بتر از من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او جر زد و من جر زدم آنقدر که آخر</p></div>
<div class="m2"><p>شام آمد و کوتاه شد این جور و جر از من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوردند همه جز من و جز من همه خفتند</p></div>
<div class="m2"><p>کو برده بد از اول شب خواب و خور از من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پاسی چو ز شب رفت ز جا جستم و دیدم</p></div>
<div class="m2"><p>خوابند حریفان همگی بی خبر از من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آهسته به سر پنجه شدم زیر لحافش</p></div>
<div class="m2"><p>افتاده از این حال نفس در شُمَر از من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وا کردم از او تکمه شلوار و عیان شد</p></div>
<div class="m2"><p>کونی که نهان بود چو قرص قمر از من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تر کردمش آن موضع مخصوص بخوبی</p></div>
<div class="m2"><p>آری که فراوان زده سر این هنر از من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هشتم سر گرم ذکرم بر در نرمش</p></div>
<div class="m2"><p>آهسته در او رفت دو ثلث ذکر از من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دیدم که بر افتاد نفیرش ز تکاپو</p></div>
<div class="m2"><p>گویی که رسیدست دلش را خبر از من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وقتست که در غلتد و باطل شودم کار</p></div>
<div class="m2"><p>کاری که نخواهد شد حاصل دگر از من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چسبیدمش آنگونه که هرگز نتوانست</p></div>
<div class="m2"><p>کردنش تبردار جدا با تبر از من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا خایه فرو بردم و گفت آخ که مردم</p></div>
<div class="m2"><p>گویی به دلش رفت فرو نیشتر از من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون َ صعوة افتاده به سر پنجه شاهین</p></div>
<div class="m2"><p>درمانده به زیر اندر بی بال و پر از من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت این چه بساطست ولم کن پدرم سوخت</p></div>
<div class="m2"><p>برخیز و برو پرده عصمت مدر از من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>من اهل چنین کار نبودم که تو کردی</p></div>
<div class="m2"><p>خود را بکشم گر نکشی زودتر از من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در خواب نمی دید کسی ترکندم در</p></div>
<div class="m2"><p>غیر از تو که تر کردی در خواب در از من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با همچو منی همچو فنی؟ گفتمش آرام</p></div>
<div class="m2"><p>حق داری اگر پاره نمایی جگر از من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یک لحظه مکن داد که رسوا مکنیمان</p></div>
<div class="m2"><p>بشنو که چه شد تا که زد این کار سر از من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شیطان لعین وسوسه ام کرد و الا</p></div>
<div class="m2"><p>کس هیچ ندیدست خطا اینقدر از من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا رفت بگوید چه، دهانش بگرفتم</p></div>
<div class="m2"><p>گفتم صنما محض خدا در گذر از من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قربان تو ای درد و بلای تو به جانم</p></div>
<div class="m2"><p>عفوم کن و آزرده مشو این سفر از من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر بار دگر همچو خلافی به تو کردم</p></div>
<div class="m2"><p>برخیز و بزن مشت و بسوزان پدر از من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کاریست گذشتست و سبوییست شکستست</p></div>
<div class="m2"><p>بیخود مبر این آب رخ مختصر از من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>حالاست که یاران دگر سر بدر آرند</p></div>
<div class="m2"><p>ناچار تو شرمنده شوی بیشتر از من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مستیم و خرابیم و کسی شاهد ما نیست</p></div>
<div class="m2"><p>بگذار بجنبد کفل از تو کمر از من</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یک لحظه تو این جوش مزن حوصله پیش آر</p></div>
<div class="m2"><p>هم دفع شر از خود کن و هم دفع شر از من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دانی که تو گر بیش کنی همهمه و قال</p></div>
<div class="m2"><p>بدنام کنی خود را قطع نظر از من</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زیبا پسر از خشم در اندیشه فرو رفت</p></div>
<div class="m2"><p>وامانده از این حال به بوک و مگر از من</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفتا بخدا نیست بد اخلاق تر از تو</p></div>
<div class="m2"><p>گفتم بخدا نیست خوش اخلاق تر از من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتا ده بده قوطی سیگار طلا را</p></div>
<div class="m2"><p>گفتم تو نرو تا نستانی سحر از من</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگذار که بی همهمه فارغ شوم از کار</p></div>
<div class="m2"><p>چون صبح شود هرچه بخواهی ببر از من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شد صبح و برآورد سر آن سیمبر از خواب</p></div>
<div class="m2"><p>در بستر من دید که نبود اثر از من</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با خادم من گفت که مخدوم تو پس کو</p></div>
<div class="m2"><p>او داد جوابش که ندارد خبر از من</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پژمرد و در اندیشه فرو رفت و به خود گفت</p></div>
<div class="m2"><p>دیدی که چه تر کرد در این بد گهر از من ؟</p></div></div>