---
title: >-
    شمارهٔ  ۶۹ - ای خایه
---
# شمارهٔ  ۶۹ - ای خایه

<div class="b" id="bn1"><div class="m1"><p>ای خایه به دست تو اسیرم</p></div>
<div class="m2"><p>بنموده‌ای از جماع سیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستم نشود به تخم کَس بند</p></div>
<div class="m2"><p>تا باد تو کرده دست گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندان نشوی تو خوب تا من</p></div>
<div class="m2"><p>از حسرت کون و کس بمیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا حضرت مستطابِ عالی</p></div>
<div class="m2"><p>کوچک بشوید بنده پیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین پس ز جِماع رخ نتابم</p></div>
<div class="m2"><p>خوب ار نشدی، نشو به کیرم</p></div></div>