---
title: >-
    شمارهٔ  ۲ - شاه و جام
---
# شمارهٔ  ۲ - شاه و جام

<div class="b" id="bn1"><div class="m1"><p>پادشهی رفت به عزمِ شکار</p></div>
<div class="m2"><p>با حرم و خیل به دریا کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیمۀ شه را لبِ رودی زدند</p></div>
<div class="m2"><p>جشن گرفتند و سرودی زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود در آن رود یکی گردآب</p></div>
<div class="m2"><p>کز سَخَطَش داشت نهنگ اجتناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماهی از آن ورطه گذشتی چو برق</p></div>
<div class="m2"><p>تا نشود در دلِ آن ورطه غرق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که از آن لُجّه به خود داشت بیم</p></div>
<div class="m2"><p>از طرفِ او نوزیدی نسیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نشود غرقه در آن لُجّه بَط</p></div>
<div class="m2"><p>پا ننهادی به غلط رویِ شط</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوی بدان سوی نمی کرد روی</p></div>
<div class="m2"><p>تا نرود در گلویِ او فروی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شه چو کمی خیره در آن لُجهّ گشت</p></div>
<div class="m2"><p>طُرفه خیالی به دِماغش گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پادشاهان را همه این است حال</p></div>
<div class="m2"><p>سهل شُمارند امورِ مُحال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با سر و جانِ همه بازی کنند</p></div>
<div class="m2"><p>تا همه جا دست درازی کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامِ طلایی به کف شاه بود</p></div>
<div class="m2"><p>پرت به گردابِ کذایی نمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت که هر لشکریِ شاه دوست</p></div>
<div class="m2"><p>آورد این جام به کف آنِ اوست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ کس از ترس جوابی نداد</p></div>
<div class="m2"><p>نبضِ همه از حرکت ایستاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غیرِ جوانی که ز جان شست دست</p></div>
<div class="m2"><p>جَست به گرداب چو ماهی ز شَست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آب فرو برد جوان را به زیر</p></div>
<div class="m2"><p>ماند چو دُر دَر صدفِ آب گیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بعد که نومید شدندی ز وی</p></div>
<div class="m2"><p>کام اجل خوردۀ خود کرد قی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از دلِ آن آبِ جنایت شعار</p></div>
<div class="m2"><p>جَست برون چون گهرِ آب دار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پایِ جوان بر لب ساحل رسید</p></div>
<div class="m2"><p>چند نفس پشتِ هم از دل کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خَم شد و آبی که بُدش در گلو</p></div>
<div class="m2"><p>ریخت برون چون ز گلویِ سبو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جام به کف رفت به نزدیکِ شاه</p></div>
<div class="m2"><p>خیره در او چشمِِ تمام ِ سپاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت شها عمر تو پاینده باد</p></div>
<div class="m2"><p>دولت و بخت تو فزاینده باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جامِ بقایِ تو نگردد تُهی</p></div>
<div class="m2"><p>باد روانِ تو پر از فَرِّهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روی زمین مسکن و مأوایِ تو</p></div>
<div class="m2"><p>بر دلِ دریا نرسد پایِ تو</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جایِ مَلِک در زبر خاک به</p></div>
<div class="m2"><p>خاک از این آبِ غضبناک به</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کانچه من امروز بدیدم در آب</p></div>
<div class="m2"><p>دشمنِ شه نیز نبیند به خواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هَیبَتِ این آب مرا پیر کرد</p></div>
<div class="m2"><p>مرگِ من از وحشتِ خود دیر کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دید چو در جایِ مَهیب اندرم</p></div>
<div class="m2"><p>مرگ بترسید و نیامد برم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دید که آن جا که منم جای نیست</p></div>
<div class="m2"><p>جا که اجل هم بنهد پای نیست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آب نه، گرداب نه، دام بلا</p></div>
<div class="m2"><p>دیو در او شیرِ نر و اژدها</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پایِ من ای شه ترسیده بر او</p></div>
<div class="m2"><p>آب مرا برد چو آهن فرو</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بود سر راهِ منِ سرنگون</p></div>
<div class="m2"><p>سنگِ عظیمی چو کُهِ بیستون</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>‎آب مرا جانب آن سنگ برد</p></div>
<div class="m2"><p>‎وین سرِ بی ترسم بر سنگ خورد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>‎جَست به رویم ز کمرگاهِ سنگ</p></div>
<div class="m2"><p>‎سیلِ عظیمِ دگری چون نهنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>‎ماند تنم بین دو کورانِ آب</p></div>
<div class="m2"><p>‎دانه صفت در وسطِ آسیاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>‎گشتنِ این آب به آن آب ضم</p></div>
<div class="m2"><p>‎داد رهِ سَیرِ مرا پیچ و خم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>‎گشته گرفتار میانِ دو موج</p></div>
<div class="m2"><p>‎گه به حضیضم بَرَد و گه به اوج</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>‎با هم اگر چند بُدند آن دو چند</p></div>
<div class="m2"><p>‎لیک در آزردن من یک تنند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>‎همچو فشردند ز دو سو تنم</p></div>
<div class="m2"><p>‎گفتی در منگنۀ آهنم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>‎بود میانشان سرِ من گیر و دار</p></div>
<div class="m2"><p>‎همچو دو صیّاد سر یک شکار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>‎سیلی خوردی ز دو جانب سرم</p></div>
<div class="m2"><p>‎وه که چه محکم بُد سیلی خورم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>‎روی پر از آب و پر از آب زیر</p></div>
<div class="m2"><p>‎هیچ نه پا گیرم و نه دست گیر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>‎هیچ نه یک شاخ و نه یک برگ بود</p></div>
<div class="m2"><p>‎دست رسی نیز نه بر مرگ بود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>‎آب هم الفت ز پیم می گسیخت</p></div>
<div class="m2"><p>‎دم به دم از زیرِ پیم می گریخت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>‎هیچ نمی ماند مرا زیرِ پا</p></div>
<div class="m2"><p>‎سر به زمین بودم و پا در هوا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>‎جای نه تا بند شود پایِ من</p></div>
<div class="m2"><p>‎بود گریزنده ز من جایِ من</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>‎آب گهی لوله شدی همچو دود</p></div>
<div class="m2"><p>‎چند نی از سطح نمودی صعود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>‎باز همان لوله دویدی به زیر</p></div>
<div class="m2"><p>‎پهن شدی زیرِ تنم چون حصیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>‎رفتن و باز آمدنش کار بود</p></div>
<div class="m2"><p>‎دایماً این کار به تکرار بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>‎من شده گردنده به خود دوک وار</p></div>
<div class="m2"><p>‎در سرم افتده ز گردش دَوار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>‎فرفره سان چرخ زنان دورِ خود</p></div>
<div class="m2"><p>‎شایقِ جان دادنِ فی الفور خود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>‎گاه به زیر آمدم و گه به رو</p></div>
<div class="m2"><p>‎قرقر می کرد مرا در گلو</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>‎این سفر آبم چو فروتر کشید</p></div>
<div class="m2"><p>‎سنگ دگر شد سر راهم پدید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>‎شاخه مرجانی از آن رُسته بود</p></div>
<div class="m2"><p>‎جان من ای شاه بدان بسته بود</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>‎جام هم از بختِ خداوندگار</p></div>
<div class="m2"><p>‎گشته چو من میوه آن شاخسار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>‎دست زدم شاخه گرفتم به چنگ</p></div>
<div class="m2"><p>‎پای نهادم به سرِ تخته سنگ</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>‎غیر سیاهی و تباهی دگر</p></div>
<div class="m2"><p>‎هیچ نمی آمدم اندر نظر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>‎جوشش بالا شده آن جا خموش</p></div>
<div class="m2"><p>‎لیل خموشیش بتر از خروش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>‎کاش که افتاده نبود از برش</p></div>
<div class="m2"><p>‎جوشش آن قسمتِ بالاترش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>‎زان که در آن جایگه پر ز موج</p></div>
<div class="m2"><p>‎گه به حضیض آمدم و گه به اوج</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>‎لیک در این قسمتِ ژرفِ مَهیب</p></div>
<div class="m2"><p>‎روی نبودی مگرم بر نشیب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>‎گفتی دارم به سرِ کوه جای</p></div>
<div class="m2"><p>‎دره ژرفی است مرا زیرِ پای</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>‎مختصرک لرزشی اندر قدم</p></div>
<div class="m2"><p>‎راهبرم بود به قعر عدم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>‎هیچ نه پایان و نه پایاب بود</p></div>
<div class="m2"><p>‎آب همه آب همه آب بود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>‎ناگه دیدم که بر آورده سر</p></div>
<div class="m2"><p>‎جانورانی یله از دور و بر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>‎جمله به من ناب نشان می دهند</p></div>
<div class="m2"><p>‎وز پیِ بلعم همه جان می دهند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>‎شعله چشمانِ شرر بارشان</p></div>
<div class="m2"><p>‎بود حکایت کنِ افکارشان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>‎آب تکان خورد و نهنگی دمان</p></div>
<div class="m2"><p>‎بر سر من تاخت گشاده دهان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>‎دیدم اگر مکث کنم روی سنگ</p></div>
<div class="m2"><p>‎می روم السّاعه به کامِ نهنگ</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>‎جایِ فرارم نه و آرام نه</p></div>
<div class="m2"><p>‎دست ز جان شستم و از جام نه</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>‎جام چو جان نیک نگه داشتم</p></div>
<div class="m2"><p>‎شاخه مرجان را بگذاشتم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>‎پیش که بر من رسد آن جانور</p></div>
<div class="m2"><p>‎کرد خدایم به عطوفت نظر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>‎موجی از آن قسمتِ بالا رسید</p></div>
<div class="m2"><p>‎باز مرا جانبِ بالا کشید</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>‎موجِ دگر کرد ز دریا مدد</p></div>
<div class="m2"><p>‎رَستَم از آن کشمکش جزر و مد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>‎بحر مرا مرده چو انگار کرد</p></div>
<div class="m2"><p>‎از سرِ خود رفع چو مردار کرد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>‎شکر که دولت دهنِ مرگ بست</p></div>
<div class="m2"><p>‎جان من و جامِ مَلِک هر دو رست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>‎شاه بر او رأفتِ شاهانه راند</p></div>
<div class="m2"><p>‎دختر خود را به بر خویش خواند</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>‎گفت که آن جام پر از می کند</p></div>
<div class="m2"><p>‎با کف خود پیش کشِ وی کند</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>‎مردِ جوان جام ز دختر گرفت</p></div>
<div class="m2"><p>‎عمر به سر آمده از سر گرفت</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>‎لیک قضا کارِ دگر گونه کرد</p></div>
<div class="m2"><p>‎جامِ بشاشت را وارونه کرد</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>‎باده نبود آنچه جوان سر کشید</p></div>
<div class="m2"><p>‎شربتِ مرگ از کفِ دختر چشید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>‎شاه چو زین منظره خُشنود بود</p></div>
<div class="m2"><p>‎امرِ ملوکانه مکرّر نمود</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>‎بارِ دگر جام به دریا فکند</p></div>
<div class="m2"><p>‎دیده بر آن مردِ توانا فکند</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>‎گفت اگر باز جنون آوری</p></div>
<div class="m2"><p>‎جام ز گرداب برون آوری</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>‎جامِ دگر هدیۀ جانت کنم</p></div>
<div class="m2"><p>‎دختر خود نیز از آنت کنم</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>‎مرد وفا پیشه که از دیرگاه</p></div>
<div class="m2"><p>‎داشت به دل آرزویِ دختِ شاه</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>‎لیک به کس جرأتِ گفتن نداشت</p></div>
<div class="m2"><p>‎چاره بجز راز نهفتن نداشت</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>‎چون ز شه این وعده دلکش شنید</p></div>
<div class="m2"><p>‎جامه ز تن کند و سویِ شط دوید</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>‎دخترِ شه دید چو جان بازیش</p></div>
<div class="m2"><p>‎سویِ گران مرگ سبک تازیش</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>‎کرد یقین کاین همه از بهرِ اوست</p></div>
<div class="m2"><p>‎جان جوان در خطر از مِهرِ اوست</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>‎گفت به شه کای پدرِ مهربان</p></div>
<div class="m2"><p>‎رحم بکن بر پدرِ این جوان</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>‎دست و دلش کوفته و خسته است</p></div>
<div class="m2"><p>‎تازه ز گرداب بلا جسته است</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>‎جام در آوردن ازین آبگیر</p></div>
<div class="m2"><p>‎طعمه گرفتن بود از کامِ شیر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>‎ترسمش از بس شده زار و زبون</p></div>
<div class="m2"><p>‎خوب از این آب نیاید برون</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>‎شاه نفرموده به دختر جواب</p></div>
<div class="m2"><p>‎بود جوان آب نشین چون حباب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>‎بر لبِ سلطان نگذشته جواب</p></div>
<div class="m2"><p>‎از سرِ دلداده گذر کرد آب</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>‎عشق کند جامِ صبوری تهی</p></div>
<div class="m2"><p>‎آه مِنَ العِشقِ و حالاتِه</p></div></div>