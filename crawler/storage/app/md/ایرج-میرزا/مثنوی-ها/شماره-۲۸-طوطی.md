---
title: >-
    شمارهٔ  ۲۸ - طوطی
---
# شمارهٔ  ۲۸ - طوطی

<div class="b" id="bn1"><div class="m1"><p>مرا هست یک طوطی اندر قفس</p></div>
<div class="m2"><p>که مِثلَش به‌خوبی ندیدست کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرش سبز رنگست و دُمّش دراز</p></div>
<div class="m2"><p>به چنگال و منقار مانندِ باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوراکش دهم از نخودچی و قند</p></div>
<div class="m2"><p>نخودچی و قند است او را پسند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان هوشیارَست و با جوهَر است</p></div>
<div class="m2"><p>که از اکثر بچه ها خوشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تو هرچه بشنید گیرد به یاد</p></div>
<div class="m2"><p>چو شاگرد با فهم از اوستاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همین نکته بس باشد از هوش او</p></div>
<div class="m2"><p>که چیزی نگردد فراموش او</p></div></div>