---
title: >-
    شمارهٔ  ۱ - انقلاب ادبی
---
# شمارهٔ  ۱ - انقلاب ادبی

<div class="b" id="bn1"><div class="m1"><p>ای خدا باز شبِ تار آمد</p></div>
<div class="m2"><p>نه طبیب و نه پرستار آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز یاد آمدم آن چَشمِ سیاه</p></div>
<div class="m2"><p>آن سرِ زلف و بُناگوشِ چو ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردم از هر شبِ پیش افزون است</p></div>
<div class="m2"><p>سوزشِ عشق ز حد بیرون است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تندتر گشته ز هر شب تبِ من</p></div>
<div class="m2"><p>بدتر از هر شبِ من امشبِ من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکند یادِ من آن شوخ پسر</p></div>
<div class="m2"><p>نه به زور و نه به زاری نه به زر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کارِ هر دردِ دگر آسان است</p></div>
<div class="m2"><p>آه از این درد که بی درمان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب آن شوخ دگر باز کجاست</p></div>
<div class="m2"><p>کاتش از جانِ من امشب برخاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز چشمِ که بر او افتاده است</p></div>
<div class="m2"><p>که دلم در تک و پو افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بِساطِ که نهاده است قدم؟</p></div>
<div class="m2"><p>که من امشب نشکیبم یک دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دلم دایم از او بیم آمد</p></div>
<div class="m2"><p>تِلِگرافات که بی سیم آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ساعتِ ده شد و جانم به لب است</p></div>
<div class="m2"><p>آخر ای شوخ بیا نصف شب است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نیایی تو شوم دیوانه</p></div>
<div class="m2"><p>عاشقم بر تو، شنیدی یا نه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرچه گفتی تو اطاعت کردم</p></div>
<div class="m2"><p>صرفِ جان، بذلِ بِضاعت کردم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حق تو را نیز چو من خوار کند</p></div>
<div class="m2"><p>به یکی چون تو گرفتار کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوری و بی مَزِگی باز چرا</p></div>
<div class="m2"><p>من که مُردم ز فِراقت دِ بیا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بکشی همچو من آهِ دگری</p></div>
<div class="m2"><p>تا تو هم لَذّتِ دوری نَچَشی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست از کشتنِ عاشق نَکَشی</p></div>
<div class="m2"><p>این سخن‌ها به که می‌گویم من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چاره دل ز که می‌جویم من</p></div>
<div class="m2"><p>دایم اندیشه و تشویش کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که چه خاکی به سر خویش کنم</p></div>
<div class="m2"><p>یک طرف خوبیِ رفتار خودم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک طرف زحمتِ همکارِ بدم</p></div>
<div class="m2"><p>یک طرف پیری و ضعفِ بَصَرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک طرف خرجِ فرنگِ پِسَرَم</p></div>
<div class="m2"><p>دایم افکنده یکی خوان دارم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زائر و شاعر و مهمان دارم</p></div>
<div class="m2"><p>هرچه آمد به کَفَم گُم کردم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صرفِ آسایشِ مردُم کردم</p></div>
<div class="m2"><p>بعدِ سی سال قلم فرسایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نوکری، کیسه بُری، مُلاّیی</p></div>
<div class="m2"><p>گاه حاکم شدن و گاه دبیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه ندیمِ شه و گه یارِ وزیر</p></div>
<div class="m2"><p>با سفرهایِ پیاپی کردن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ناقۀ راحت خود پی کردن</p></div>
<div class="m2"><p>گَردِ سرداریِ سلطان رُفتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بله قربان بله قربان گفتن</p></div>
<div class="m2"><p>گفتنِ این که ملکِ ظِلِّ خُداست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سینه‌اش آینه غیب نماست</p></div>
<div class="m2"><p>مدّتی خلوتیِ خاصّ شدن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همسرِ لوطی و رقاّص شدن</p></div>
<div class="m2"><p>مرغ ناپُخته ز دَوری بُردَن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رویِ نان هِشتَن و فوری خوردن</p></div>
<div class="m2"><p>ساختن با کمک و غیر کمک</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از برایِ رفقا دوز و کلک</p></div>
<div class="m2"><p>باز هم کیسه‌ام از زر خالی است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کیسه‌ام خالی و همّت عالی است</p></div>
<div class="m2"><p>با همه جُفت و جَلا و تَک و پو</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دان ما پُش ایل نیامِم اَن سُل‌سو</p></div>
<div class="m2"><p>نه سری دارم و نه سامانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه دهی، مزرعهٔی، دُکاّنی</p></div>
<div class="m2"><p>نه سر و کار به یک بانک مراست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه به یک بانک یکی دانگ مراست</p></div>
<div class="m2"><p>بگریزد ز من از نیمه راه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پول غول آمد و من بسم‌الله</p></div>
<div class="m2"><p>من به بی سیم و زری مأنوسم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>لیک از جایِ دگر مأیوسم</p></div>
<div class="m2"><p>کارِ امروزه من کارِ بدی است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کارِ انسانِ قلیل الخردی است</p></div>
<div class="m2"><p>انقلابِ ادبی محکم شد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فارسی با عربی توأم شد</p></div>
<div class="m2"><p>درِ تجدید و تجدّد وا شد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ادبّیات شَلَم شُوربا شد</p></div>
<div class="m2"><p>تا شد از شعر برون وزن ورَوی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یافت کاخِ ادبیّات نُوی</p></div>
<div class="m2"><p>می‌کُنم قافیه‌ها را پس و پیش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا شوم نابغه دوره خویش</p></div>
<div class="m2"><p>گله من بود از مشغَله‌ام</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باشد از مشغَله من گِلِه‌ام</p></div>
<div class="m2"><p>همه گویند که من اُستادم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در سخن دادِ تجدّد دادم</p></div>
<div class="m2"><p>هر ادیبی به جَلالّت نرسد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر خَری هم به وکالت نرسد</p></div>
<div class="m2"><p>هر دَبَنگُوز که والی نشود</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دامَ اِجلالهُ العالی نشَود</p></div>
<div class="m2"><p>هرکه یک حرف بزد ساده و راست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نتوان گفت رئیس الوزراست</p></div>
<div class="m2"><p>تو مپندار که هر احمقِ خر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مُقبِلُ السَّلطَنِه گردد آخَر</p></div>
<div class="m2"><p>کارِ این چرخِ فلک تو در توست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کس نداند که چه در باطنِ اوست</p></div>
<div class="m2"><p>نقدِ این عمر که بسیار کم است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>راستی بد گذراندن ستم است</p></div>
<div class="m2"><p>این جوانان که تجدّد طلبند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>راستی دشمن علم و ادبند</p></div>
<div class="m2"><p>شعر را در نظرِ اهلِ ادب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>صَبر باشد وَتَد و عشق سَبَب</p></div>
<div class="m2"><p>شاعری طبعِ روان می‌خواهد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نه معانی نه بیان می‌خواهد</p></div>
<div class="m2"><p>آن که پیشِ تو خدایِ ادبند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نکته چینِ کلماتِ عربند</p></div>
<div class="m2"><p>هرچه گویند از آن جا گویند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هرچه جویند از آن جا جویند</p></div>
<div class="m2"><p>یک طرف کاسته شأن و شرفم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یک طرف با همه دارد طرفم</p></div>
<div class="m2"><p>من از این پیش معاون بودم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه غلط کار نه خائن بودم</p></div>
<div class="m2"><p>جاکِشی آمد و معزولم کرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>............ آواره و بی پولم کرد</p></div>
<div class="m2"><p>چه کنم؟ مرکزیان رِشوَه خورند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همگی کاسه بر و کیسه بُرند</p></div>
<div class="m2"><p>بعد گفتند که این خوب نشد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>لایقِ خادمِ محبوب نشد</p></div>
<div class="m2"><p>پیش خود فکر به حالم کردند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اَنپِکتُر زِلزالَم کردند</p></div>
<div class="m2"><p>چند مه رفت و مازرهال آمد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>شُشَم از آمدنش حال آمد</p></div>
<div class="m2"><p>یک معاون هم از آن کج کَلَهان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>پرورش دیده در امعاء شَهان</p></div>
<div class="m2"><p>جَسته از بینیِ دولت بیرون</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شده افراطیِ اِفراطیّون</p></div>
<div class="m2"><p>آمد از راه و مزَن بر دِل شد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کارِ اهلِ دل از او مشکِل شد</p></div>
<div class="m2"><p>چه کُند گر مُتِفَرعِن نشود</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>پس بگو هیچ معاون نشود!</p></div>
<div class="m2"><p>الغرض باز مرا کار افزود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>که مرا تجربه افزون‌تر بود</p></div>
<div class="m2"><p>چه بگویم که چه همّت کردم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>با ماژُرهال چه خدمت کردم</p></div>
<div class="m2"><p>بعد چون ار به سامان افتاد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آدژُوان تازه به کوران افتاد</p></div>
<div class="m2"><p>رشته کار به دست آوردند</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>در صفِ بنده شکست آوردند</p></div>
<div class="m2"><p>دُم علم کرد معاوِن که منم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>من در اطرافِ ماژر مؤتمنم</p></div>
<div class="m2"><p>کار با مَن بُوَد از سَر تا بُن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بنده گفتم به جهنم تو بِکُن</p></div>
<div class="m2"><p>داد ضمناً ماژرم دلداری</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که تو هر کار که بودت داری</p></div>
<div class="m2"><p>باز شد مشغله تفتیش مرا</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>دارد این مشغله دل ریش مرا</p></div>
<div class="m2"><p>کاین اداره به غلط دایره شد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چون یکی از شُعَبِ سایره شد</p></div>
<div class="m2"><p>اندر این دایره یک آدم نیست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>پِرسُنِل نیز به آن منعم نیست</p></div>
<div class="m2"><p>شُعَبِ دایره من کم شد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>شیرِ بی یال و دُم و اِشکم شد</p></div>
<div class="m2"><p>من رئیسِ همه بودم وقتی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>مایه واهمه بودم وقتی</p></div>
<div class="m2"><p>آن زمان شعر جلودارم بود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>اَسبَحی کاتبِ اَسرارم بود</p></div>
<div class="m2"><p>رؤسا جمله مطیعم بودند</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>تابعِ امرِ منیعم بودند</p></div>
<div class="m2"><p>حالیا گوش به عرضم نکنند</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>جز یکی چون همه فرضم نکنند</p></div>
<div class="m2"><p>آن کسانی که بُدند اَذنابم</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کار برگشت و شدند اربابم</p></div>
<div class="m2"><p>با حقوقِ کم و با خرجِ زیاد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>جِقّه چوبیم از رُعب افتاد</p></div>
<div class="m2"><p>روز و شب یک دم آسوده نیم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>من دگر ای رفقا مُردَنِیَم</p></div>
<div class="m2"><p>بسکه ردر لیوِر و هنگام لِتِه</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>دوسیه کردم و کارتُن تِرتِه</p></div>
<div class="m2"><p>بسکه نُت دادم و آنکِت کردم</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>اشتباه بروت و نت کردم</p></div>
<div class="m2"><p>سوزن آوردم و سنجاق زدم</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>پونز و پَنس و به اوراق زدم</p></div>
<div class="m2"><p>هی نشستم به مناعت پسِ میز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>هی تپاندم دوسیه لایِ شُمیز</p></div>
<div class="m2"><p>هی پاراف هِشتَم و امضا کردم</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>خاطرِ مُدّعی ارضا کردم</p></div>
<div class="m2"><p>گاه با زنگ و زمانی یا هو</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>پیشخدمت طلبیدم به بورو</p></div>
<div class="m2"><p>تو بمیری ز آمور افتادم</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>از شر و شور و شعور افتادم</p></div>
<div class="m2"><p>چه کنم زان همه شیفر و نومِرو</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>نیست در دست مرا غیر زِرو</p></div>
<div class="m2"><p>هر بده کارتُن و بستان دوسیه</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هی بیار از درِ دکّان نسیه</p></div>
<div class="m2"><p>شد گذارِ عَزَبی از درِ باغ</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>دید در باغ یکی ماده الاغ</p></div>
<div class="m2"><p>باغبان غایب و شهوت غالب</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ماده خر بسته به میلِ طالب</p></div>
<div class="m2"><p>سردَرون گرد و به هر سو نگریست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>تا بداند به یقین خر خرِ کیست</p></div>
<div class="m2"><p>اندکی از چپ و از راست دَوید</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>باغ را از سرِ خر خالی دید</p></div>
<div class="m2"><p>ور کسی نیز به باغ اندر بود</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>هوشِ خر بنده به پیشِ خر بود</p></div>
<div class="m2"><p>اری آن گم شده را سمع و بصر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بود اندر گروِ کادنِ خر</p></div>
<div class="m2"><p>آدمی پیشِ هوس کور و کر است</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>هرکه دنبال هوس رفت خر است</p></div>
<div class="m2"><p>او چه دانند که چه بدیا خوب است</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بیند آن را که بر او مطلوب است</p></div>
<div class="m2"><p>الغرض بند ز شلوار گرفت</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>ماده خر را به دمِ کار گرفت</p></div>
<div class="m2"><p>بود غافل که فلک پرده دَر است</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>پرده‌ها در پسِ این پرده دَر است</p></div>
<div class="m2"><p>ندهد شربتِ شیرین به کسی</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>که در آن یافت نگردد مگسی</p></div>
<div class="m2"><p>نوش بی‌نیش میسّر نشود</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>نیست صافی که مُکَدّر نشود</p></div>
<div class="m2"><p>ناگهان صاحبِ خر پیدا شد</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>مشت بیچاره خَرگاه وا شد</p></div>
<div class="m2"><p>بانگ برداشت بر او کای جاپیچ</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>چه کنی با خرمن؟ گفتا: هیچ!</p></div>
<div class="m2"><p>گفت أَامِنَّهُ لِلّه دیدم</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>معنیِ هیچ کنون فهمیدم</p></div>
<div class="m2"><p>نگذارد فلکِ مینایی</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>که خری هم به فراغت گایی</p></div>
<div class="m2"><p>گوش کن کامدم امشب به نظر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>قصه دیگر از این با مزه‌تر</p></div>
<div class="m2"><p>اندر آن سال که از جانبِ غرب</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>شد روان سیل سفت آتشِ حرب</p></div>
<div class="m2"><p>انگلیس از دلِ دریا برخاست</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>آتشی از سرِ دنیا برخاست</p></div>
<div class="m2"><p>پای بگذاشت به میدانِ وَغا</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>حافظِ صلحِ جهان آمریکا</p></div>
<div class="m2"><p>گاریِ لیره ز آلمان آمد</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>به تنِ مردمِ ری جان آمد</p></div>
<div class="m2"><p>جنبش افتاد در احزابِ غیور</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>آب داخل شد در لانه مور</p></div>
<div class="m2"><p>رشته طاعت ژاندارم گسیخت</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>عدّهٔی ماند و دگر عدّه گریخت</p></div>
<div class="m2"><p>همه گفتند که از وحدتِ دین</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>کرد باید کمکِ متّحدین</p></div>
<div class="m2"><p>اهل ری عرضِ شهامت کردند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چه بگویم چه قیامت کردند</p></div>
<div class="m2"><p>لیک از آن ترس که محصور شوند</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بود لازم که ز ری دور شوند</p></div>
<div class="m2"><p>لاجرم روی نهادند به قم</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>یک یک و ده ده و صد صد مردم</p></div>
<div class="m2"><p>مقصدِ عدّه معدودی پول</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>مقصدِ باقیِ دیگر مجهول</p></div>
<div class="m2"><p>من هم از جمله ایشان بودم</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>جزءِ آن جمعِ پریشان بودم</p></div>
<div class="m2"><p>من هم از دردِ وطن با رفقا</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>می‌روم لیک ندانم به کجا</p></div>
<div class="m2"><p>من و یک جمعِ دگر از احباب</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>شب رسیدیم به یک دیهِ خراب</p></div>
<div class="m2"><p>کلبهٔی یافته مَأوا کردیم</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>پا و پاتاوه ز هم وا کردیم</p></div>
<div class="m2"><p>خسته و کوفته و مست و خراب</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>این به فکرِ خور و آن در پیِ خواب</p></div>
<div class="m2"><p>یکی افسرده و آن یک در جوش</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>عدّهٔی ناطق و جمعی خاموش</p></div>
<div class="m2"><p>هر کسی هر چه در انبانش بود</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>خورد و در یک طرفِ حُجر .....</p></div>
<div class="m2"><p>همه خفتند و مرا خواب نبُرد</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>خواب در منزِل ناباب نبرد</p></div>
<div class="m2"><p>ساعتی چند چو از شب بگذشت</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>خواب بر چشم همه غالب گشت</p></div>
<div class="m2"><p>دیدم آن سیّده نرّه خره</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>رفته در زیرِ لحافِ پسره</p></div>
<div class="m2"><p>گوید آهسته به گوشش که امیر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>مرگِ من لفت بده، تخت بگیر!</p></div>
<div class="m2"><p>این چه بحسّی و بد اخلاقی است</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>رفته یک ثلث و دو ثلثش باقی است!</p></div>
<div class="m2"><p>تو که همواره خوش اخلاق بدی</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>چه شد این طور بداخلاق شدی</p></div>
<div class="m2"><p>من چو بشنیدم از او این تقریر</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>شد جوان در نظرم عالَمِ پیر</p></div>
<div class="m2"><p>هرچه از خُلق نکو بشنیدم</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>عملاً بینِ رفیقان دیدم</p></div>
<div class="m2"><p>معنی خلق در ایران این است!</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>بد بُوَد هر که بما بدبین است!</p></div>
<div class="m2"><p>هر که دم بیشتر از خُلق زند</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>قصدش این است که تا بیخ کُند</p></div>
<div class="m2"><p>گفت آن چاه کن اندر بُنِ چاه</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>کای خدا تا به کی این چاهِ سیاه</p></div>
<div class="m2"><p>نه از این دلو شود پاره رسن</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>نه مرا جان به در آید ز بدن</p></div>
<div class="m2"><p>رفت از دست به کلّی بدنم</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>تا به کی کار؟ مگر من چُدنم</p></div>
<div class="m2"><p>کاش چرخ از حرکت خسته شود</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>درِ فابریکِ فلک بسته شود</p></div>
<div class="m2"><p>موتورِ ناحیه از کار افتد</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>ترنِ رُشد ز رفتار افتد</p></div>
<div class="m2"><p>زین زلازل که در این فرش افتد</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>کاش یک زلزله در عرش افتد</p></div>
<div class="m2"><p>تا که بردارد دست از سرِ ناس</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>شرِّ این خلقت بی‌اصل و اساس</p></div>
<div class="m2"><p>گر بود زندگی این، مردن چیست</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>این همه بردن و آوردن چیست</p></div>
<div class="m2"><p>تو چو آن کوزه گرِ بوالهوسی</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>که کند کوزه به هر روز بسی</p></div>
<div class="m2"><p>خوب چون سازد و آماده کند</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>به زمین کوبد و در هم شکند</p></div>
<div class="m2"><p>باز مرغِ هوسش پر گیرد</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>عملِ لغوِ خود از سر گیرد</p></div>
<div class="m2"><p>آخدا خوب که سنجیدم من</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>از تو هم هیچ نفهمیدم من</p></div>
<div class="m2"><p>تو گر آن ذاتِ قدیمِ فردی</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>....................... نامردی</p></div>
<div class="m2"><p>یا تو آن نیستی ای خالقِ کُلّ</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>که به ما وصف نمودند رسل</p></div>
<div class="m2"><p>‌</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>کاش مرغی شده پر باز کنم</p></div>
<div class="m2"><p>تا لبِ بامِ تو پرواز کنم</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>این بزرگان که طلبکارِ من اند</p></div>
<div class="m2"><p>طالبِ طبعِ گهربار من‌ اند</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>کس نشد کِم ز غم آزاده کند</p></div>
<div class="m2"><p>فکر حالِ من افتاده کند</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>در دهی گوشه باغی بدهد</p></div>
<div class="m2"><p>گوسفندی و الاغی بدهد</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>نگذارد که من آزرده شوم</p></div>
<div class="m2"><p>با چنین ذوق دل افسرده شوم</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>فتنه‌ها در سرِ دین و وطن است</p></div>
<div class="m2"><p>این دو لفظ است که اصلِ فِتُن است</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>صحبتِ دین و وطن یعنی چه؟</p></div>
<div class="m2"><p>دینِ تو موطن من یعنی چه؟</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>همه عالم همه کس را وطن است</p></div>
<div class="m2"><p>همه جا موطنِ هر مرد و زن است</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>چیست در کلّه تو این دو خیال</p></div>
<div class="m2"><p>که کند خونِ مرا بر تو حلال</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>گر چه در مالیه‌ام حالیه من</p></div>
<div class="m2"><p>متأذّی شدم از مالیه من</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>حیف باشد که مرا فکرِ بلند</p></div>
<div class="m2"><p>صرف گردد به خُرافاتی چند</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>حیف امروز گرفتارم من</p></div>
<div class="m2"><p>ورنه مجموعه افکارم من</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>جهل از ملّتِ خود بردارم</p></div>
<div class="m2"><p>منتی بر سرشان بگذارم</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>آنچه را گفته‌ام از زشت و نفیس</p></div>
<div class="m2"><p>نیست فرصت که کنم پاک نویس</p></div></div>
<div class="b" id="bn167"><div class="m1"><p></p></div>