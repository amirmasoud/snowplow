---
title: >-
    شمارهٔ  ۳۲ - سوءِ ظَنّ
---
# شمارهٔ  ۳۲ - سوءِ ظَنّ

<div class="b" id="bn1"><div class="m1"><p>نمی‌دانم چرا حَتم است و واجب</p></div>
<div class="m2"><p>که بر ما یک نفر گردد مواظب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده نیمه بده آجر بده گچ</p></div>
<div class="m2"><p>مکُن با گفته استاد خود لَج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا ما مَردُمِ ایران چنینیم</p></div>
<div class="m2"><p>چرا دَر حَقِّ هم دایم ظَنینیم</p></div></div>