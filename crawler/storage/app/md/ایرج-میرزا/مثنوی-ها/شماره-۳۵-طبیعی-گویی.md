---
title: >-
    شمارهٔ  ۳۵ - طبیعی گویی
---
# شمارهٔ  ۳۵ - طبیعی گویی

<div class="b" id="bn1"><div class="m1"><p>هرچه گویی تو طبیعی می‌گوی</p></div>
<div class="m2"><p>بیشتر زانچه طبیعی است مجوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او معلِّم تو بر او شاگِردی</p></div>
<div class="m2"><p>چه کنی جهد کز او بِه کردی</p></div></div>