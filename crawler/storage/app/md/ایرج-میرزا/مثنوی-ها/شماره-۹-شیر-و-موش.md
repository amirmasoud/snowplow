---
title: >-
    شمارهٔ  ۹ - شیر و موش
---
# شمارهٔ  ۹ - شیر و موش

<div class="b" id="bn1"><div class="m1"><p>بود شیری به بیشه‌ای خفته</p></div>
<div class="m2"><p>موشکی کرد خوابش آشفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن قَدَر دورِ شیر بازی کرد</p></div>
<div class="m2"><p>در سرِ دُوشَش اسب نازی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن قَدَر گوشِ شیر گاز گرفت</p></div>
<div class="m2"><p>گه رها کرد و گاه باز گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا که از خواب، شیر شد بیدار</p></div>
<div class="m2"><p>متغیّر ز موشِ بد رفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست برد و گرفت کَلّۀ موش</p></div>
<div class="m2"><p>شد گرفتار موشِ بازی گوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواست در زیر پنجه لِه کُنَدَش</p></div>
<div class="m2"><p>به هوا برده بر زمین زَنَدش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ای موشِ لوس یک قازی</p></div>
<div class="m2"><p>با دُمِ شیر می‌کنی بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>موشِ بیچاره در هَراس افتاد</p></div>
<div class="m2"><p>گریه کرد و به التماس افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که تو شاهِ وُحوشی و من موش</p></div>
<div class="m2"><p>موش هیچ است پیشِ شاهِ وُحوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیر باید به شیر پنجه کند</p></div>
<div class="m2"><p>موش را نیز گربه رنجه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بزرگی و من خطا کارم</p></div>
<div class="m2"><p>از تو امّیدِ مَغفِرَت دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیر از این لابه رحم حاصل کرد</p></div>
<div class="m2"><p>پنجه واکرد و موش را وِل کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اتفاقاً سه چار روزِ دگر</p></div>
<div class="m2"><p>شیر را آمد این بلا بر سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پیِ صیدِ گرگ یک صیّاد</p></div>
<div class="m2"><p>در همان حَول و حَوش دام نهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دامِ صیّاد گیرِ شیر افتاد</p></div>
<div class="m2"><p>عوضِ گرگ شیر گیر افتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>موش چون حالِ شیر را دریافت</p></div>
<div class="m2"><p>از برایِ خلاصِ او بشتافت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بندها را جوید با دندان</p></div>
<div class="m2"><p>تا که در برد شیر از آن جا جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این حکایت که خوشتر از قند است</p></div>
<div class="m2"><p>حاویِ چند نکته از پند است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اولاً گر نیی قوی بازو</p></div>
<div class="m2"><p>با قوی‌تر ز خود ستیزه مجو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ثانیاً عفو از خطا خوب است</p></div>
<div class="m2"><p>از بزرگان گذشت مطلوب است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ثالثاً با سپاس باید بود</p></div>
<div class="m2"><p>قدرِ نیکی شناس باید بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رابعاً هر که نیک یا بد کرد</p></div>
<div class="m2"><p>بد به خود کرد و نیک با خود کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خامساً خلق را حفیر مگیر</p></div>
<div class="m2"><p>که گهی سودها بَری ز حقیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شیر چون موش را رهایی داد</p></div>
<div class="m2"><p>خود رها شد ز پنجه صیّاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در جهان موشکِ ضعیفِ حقیر</p></div>
<div class="m2"><p>می‌شود مایۀ خلاصی شیر</p></div></div>