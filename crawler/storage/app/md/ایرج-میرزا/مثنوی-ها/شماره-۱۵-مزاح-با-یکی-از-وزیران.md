---
title: >-
    شمارهٔ  ۱۵ - مزاح با یکی از وزیران
---
# شمارهٔ  ۱۵ - مزاح با یکی از وزیران

<div class="b" id="bn1"><div class="m1"><p>وزیرا از مبارک بیضه ات دور</p></div>
<div class="m2"><p>مرا امروز گشته بیضه رنجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی چون پُر ز باد و دَرد گشته</p></div>
<div class="m2"><p>ز جُفت خود به صورت فَرد گشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانم چه بادی در سر اوست</p></div>
<div class="m2"><p>که با جفتش نگنجد در یکی پوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان از باد و دم سرشار گشته</p></div>
<div class="m2"><p>که پنداری سپه سالار گشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بباید بند کردن پیکر او</p></div>
<div class="m2"><p>که تا بیرون رود باد از سرِ او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر داری به جعبه بیضه بندی</p></div>
<div class="m2"><p>کز آنها داشتی زین پیش چندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی را از برای بنده بفرست</p></div>
<div class="m2"><p>برای بنده شرمنده بفرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که از لطف تو گردد بیضه ام چاق</p></div>
<div class="m2"><p>به صِحَّت جُفت و از علت شود طاق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنی از بیضه ام گر دست گیری</p></div>
<div class="m2"><p>الهی علت بیضه نگیری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمال السلطنه با آن کمالات</p></div>
<div class="m2"><p>شده اندر عِلاج بیضه ام مات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وَرم با آنهمه دارو و مرهم</p></div>
<div class="m2"><p>به قدر مویی از تخمم نشد کم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بس روغن به تخم بنده مالید</p></div>
<div class="m2"><p>کمال السلطنه بر تخم من رید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو مَه دستش به تخم من بُوَد بند</p></div>
<div class="m2"><p>نیارد دل ز دست افتاده برکَند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گُمانِ من چنین باشد که عمدا</p></div>
<div class="m2"><p>تَعَلُل می نماید در مداوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمی خواهد که گردد بیضه ام خرد</p></div>
<div class="m2"><p>چنان دانم که خواهد بیضه ام خورد!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>و یا تا پر شود از بیضه مُشتش</p></div>
<div class="m2"><p>از این رو دوست می دارد دُرشتش</p></div></div>