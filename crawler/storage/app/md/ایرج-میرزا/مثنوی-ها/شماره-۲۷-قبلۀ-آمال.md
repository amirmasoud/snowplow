---
title: >-
    شمارهٔ  ۲۷ - قبلۀ آمال
---
# شمارهٔ  ۲۷ - قبلۀ آمال

<div class="b" id="bn1"><div class="m1"><p>حاجیان رَخت چو از مکه برند</p></div>
<div class="m2"><p>مدّتی در عقب سر نِگَرَند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به جایی که حرم در نظر است</p></div>
<div class="m2"><p>چشم حُجاج به دنبال سر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من هم از کوی تو گر بستم بار</p></div>
<div class="m2"><p>باز با کوی تو دارم سر و کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمِ دل سوی تو دارم شب و روز</p></div>
<div class="m2"><p>چشم بر کوی تو دارم شب و روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو صنم قبله آمال منی</p></div>
<div class="m2"><p>چون کنم صرف نظر ؟ مال منی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی رخشنده تو قبله ماست</p></div>
<div class="m2"><p>مَردُمِ دیده ما قبله نُماست</p></div></div>