---
title: >-
    شمارهٔ  ۳۱ - گفتگو با جوانِ فرنگی مَآب
---
# شمارهٔ  ۳۱ - گفتگو با جوانِ فرنگی مَآب

<div class="b" id="bn1"><div class="m1"><p>گفتم به جوانکی مُفَرَّنگ</p></div>
<div class="m2"><p>کای در خم و چم بسان خرچنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگو زِ سِبیل خود چه دیدی</p></div>
<div class="m2"><p>کاین سان دم گوش او بریدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا که سبیل بنده روزی</p></div>
<div class="m2"><p>دزدیده کون غیر گوزی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دزدی او به چشم دیدم</p></div>
<div class="m2"><p>زان رو دُم و گوش او بریدم!</p></div></div>