---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>نمی‌دانستم ای نامردِ کونی</p></div>
<div class="m2"><p>که منزل می‌کنی در باغِ خونی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌جویی نشانِ دوستانت</p></div>
<div class="m2"><p>نمی‌خواهی که کس جوید نشانت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و گر گاهی به شهر آیی ز منزل</p></div>
<div class="m2"><p>نبینم جایِ پایت نیز در گِل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بری با خود نشانِ جایِ پا را</p></div>
<div class="m2"><p>کنی تقلید مرغانِ هوا را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو عارف که واقع حرفِ مفتی</p></div>
<div class="m2"><p>مگر بَختی که روی از من نهفتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر یاد آمد از سی سال پیشت</p></div>
<div class="m2"><p>که بر عارض نبود آثارِ ریشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر از منزلِ خود قهر کردی</p></div>
<div class="m2"><p>که منزل در کنارِ شهر کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مگر در باغ یک منظور داری</p></div>
<div class="m2"><p>نشانِ نرگسِ مخمور داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر نسرین تنی داری در آغوش</p></div>
<div class="m2"><p>که کردی صحبتِ ما را فراموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر با سروِ قدّان آرمیدی</p></div>
<div class="m2"><p>که پیوند از تُهی دستان بریدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چرا در پرده می‌گویم سخن را</p></div>
<div class="m2"><p>چرا بر زنده می‌پوشم کفن را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگویم صاف و پاک و پوست کنده</p></div>
<div class="m2"><p>که علت چیست می‌ترسی ز بنده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا من می‌شناسم بهتر از خویش</p></div>
<div class="m2"><p>ترا من آوریدستم به این ریش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خبر دارم ز اعماقِ خیالت</p></div>
<div class="m2"><p>به من یک ذرّه مخفی نیست حالت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو از کون‌های گرد لاله زاری</p></div>
<div class="m2"><p>یکی را این سفر همراه داری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنار رستورانِ قُلّا نمودی</p></div>
<div class="m2"><p>ز کون‌کنهای تهران در ربودی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به کون‌کنها زدی کیر از زرنگی</p></div>
<div class="m2"><p>نهادی جمله را زیر از زرنگی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آن گربه که دُنبه از سرِ شام</p></div>
<div class="m2"><p>همی وَر دارد و ورمالد از بام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون ترسی که گر سوی من آیی</p></div>
<div class="m2"><p>کنی با من چو سابق آشنایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مَنَت آن دنبه از دندان بگیرم</p></div>
<div class="m2"><p>خیالت غیر از اینه من بمیرم؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>توی می‌خواهی بگویی دیر جوشی</p></div>
<div class="m2"><p>به من هم هیزمِ تر می‌فروشی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو ما را بسکه صاف و ساده دانی</p></div>
<div class="m2"><p>فلان کون را برادرزاده خوانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چرا هر جا که یک بی ریش باشد</p></div>
<div class="m2"><p>تو را فی‌الفور قوم و خویش باشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چرا در رویِ یک خویشِ تو مو نیست</p></div>
<div class="m2"><p>چرا هر کس که خویش تست کونیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برو عارف که اینجا خبط کردی</p></div>
<div class="m2"><p>مر این اندیشه را بی ربط کردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برو عارف که ایرج پاک بازست</p></div>
<div class="m2"><p>از این کونها و کسها بی نیاز است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من ار صیّاد باشم صید کم نیست</p></div>
<div class="m2"><p>همانا حاجتِ صیدِ حرم نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکارِ من در اتلالِ بلندست</p></div>
<div class="m2"><p>نه عبدی کاهویِ سر در کمندست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دُرُستَست اینکه طفلان گیج و گولند</p></div>
<div class="m2"><p>سفیه و ساده و سهل‌القبولند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>توان با یک تبسّم گولشان زد</p></div>
<div class="m2"><p>گهی با پول و گه بی‌پولشان زد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ولی من جانِ عارف غیر آنم</p></div>
<div class="m2"><p>که نامردی کنم با دوستانم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تو یک کون آری از فرسنگها راه</p></div>
<div class="m2"><p>من آن را قُر زنم؟ اَستَغفِرُالله</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برو مردِ عزیز این سوءظن چیست</p></div>
<div class="m2"><p>جنونست اینکه داری سوءظن نیست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>من ار چشمم بدین غایت بُوَد شور</p></div>
<div class="m2"><p>همانا سازدش چشم‌آفرین کور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اگر می‌آمد او در خانۀ من</p></div>
<div class="m2"><p>معزَّز بود چون دردانۀ من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بود مهمان همیشه دلخوش اینجا</p></div>
<div class="m2"><p>نباشد مسجدِ مهمان کُش اینجا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من و با دوستان نا دوستداری؟</p></div>
<div class="m2"><p>تو مخلص را از این دونان شماری؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو حق‌داری که گیرد خشمت از من</p></div>
<div class="m2"><p>که ترسیده از اوّل چشمت از من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نمیدانی که ایرج پیر گشته است</p></div>
<div class="m2"><p>اگر چیزی ازو دیدی گذشته است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرفتم کون کنم، من حالتم کو</p></div>
<div class="m2"><p>برای کوه کندن آلتم کو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر کون زیرِ دست و پا بریزد</p></div>
<div class="m2"><p>به جانِ تو که کیرم برنخیزد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بسانِ جوجۀ از بیضه جَسته</p></div>
<div class="m2"><p>شود سر تا نموده راست خَسته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دوباره گردنش بر سینه چسبد</p></div>
<div class="m2"><p>نهد سر رویِ بال خویش و خُسبَد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر گاهی نگیرد بولِ پیشم</p></div>
<div class="m2"><p>نیاید یادی از احلیل خویشم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پس از پروازِ بازِ تیز چنگم</p></div>
<div class="m2"><p>به کف یک تسمه باشد با دوزنگم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چنان چسبیده احلیلم به خایه</p></div>
<div class="m2"><p>که طفلِ مُنفَطِم بر ثَدیِ دایه</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مرا کون فی‌المثل چاهِ خرابی</p></div>
<div class="m2"><p>کنارش دلوی و کوته طنابی</p></div></div>