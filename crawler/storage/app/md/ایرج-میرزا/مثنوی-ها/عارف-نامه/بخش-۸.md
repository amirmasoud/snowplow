---
title: >-
    بخش ۸
---
# بخش ۸

<div class="b" id="bn1"><div class="m1"><p>خدایا کی شود این خلق خسته</p></div>
<div class="m2"><p>از این عقد و نکاحِ چشم بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بُوَد نزد خرد اَحلی و اَحسَن</p></div>
<div class="m2"><p>زِنا کردن از این سان زن گرفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگیری زن ندیده رویِ او را</p></div>
<div class="m2"><p>بری نا آزموده خویِ او را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو عصمت باشد از دیدار مانع</p></div>
<div class="m2"><p>دگر بسته به اقبال است و طالع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حرفِ عمّه و تعریفِ خاله</p></div>
<div class="m2"><p>کنی یک عمر گوزِ خود نواله</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان صورت که با تعریفِ بقّال</p></div>
<div class="m2"><p>خریداری کنی خربوزۀ کال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>و یا در خانه آری هندوانه</p></div>
<div class="m2"><p>ندانسته که شیرین است یا نه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شب اندازی به تاریکی یکی تیر</p></div>
<div class="m2"><p>دو روزِ دیگر از عمرت شوی سیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپس جویید کامِ خود ز هر کوی</p></div>
<div class="m2"><p>تو از یک سوی و خانم از دگر سوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نخواهی جَست چون آهو از این بند</p></div>
<div class="m2"><p>که مغزِ خر خوراکت بوده یک چند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برو گر می‌شود خود را کن اَخته</p></div>
<div class="m2"><p>که تا تُخمت نماند لایِ تخته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در ایران تا بُوَد مُلّا و مُفتی</p></div>
<div class="m2"><p>به روزِ بدتر از این هم بیفتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فقط یک وقت یک آزاده بینی</p></div>
<div class="m2"><p>یکی چون آیت‌الله زاده بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر باره مهار از دست در رفت</p></div>
<div class="m2"><p>مرا دیگِ سخن جوشید و سر رفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سخن از عارف و اطوارِ او بود</p></div>
<div class="m2"><p>شکایت در سرِ رفتارِ او بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که چون چشمش فتد بر کونِ کم پشم</p></div>
<div class="m2"><p>بپوشد از تمام دوستان چَشم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر روزی ببینم رویِ ماهش</p></div>
<div class="m2"><p>دو دستی می‌زنم تویِ کلاهش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شنیدم تا شدی عارف کلاهی</p></div>
<div class="m2"><p>گرفته حُسنت از مه تا به ماهی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز سر تا مولوی را بر گرفتی</p></div>
<div class="m2"><p>بساطِ خوشگلی از سر گرفتی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به هر جا می‌روی خلقند حیران</p></div>
<div class="m2"><p>که این عارف بود یا ماهِ تابان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زن و مرد از برایت غش نمایند</p></div>
<div class="m2"><p>برایت نعل در آتش نمایند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو می‌شد با کلاهی ماه گردی</p></div>
<div class="m2"><p>چرا این کار را زوتر نکردی؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرت یک نکته گویم دوستانه</p></div>
<div class="m2"><p>به خرجت می‌رود آن نکته یا نه؟</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من و تو گَر به سر مشعل فروزیم</p></div>
<div class="m2"><p>به آن جفتِ سبیلت هر دو گوزیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو دیگر بعد از این آدم نگردی</p></div>
<div class="m2"><p>ز آرایش فزون و کم نگردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نخواهی شد پس از چل سال زیبا</p></div>
<div class="m2"><p>تو خواهی مولوی بر سر بنه یا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیفزاید کُلَه بر مَردیَت هیچ</p></div>
<div class="m2"><p>تغیُّر هم مکُن بر مولوی پیچ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیا عارف بگو چون است حالت</p></div>
<div class="m2"><p>چه بود از مشهدی گشتن خیالت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترا بر این سفر کی کرد تشویق</p></div>
<div class="m2"><p>تو و مشهد، تو و این حسن توفیق؟</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو و محرم شدن در خرگهِ انس؟</p></div>
<div class="m2"><p>تو و محرم شدن در کعبۀ قدس؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو و این آستانِ آسمان جاه؟</p></div>
<div class="m2"><p>مگر شیطان به جنّت می‌برد راه؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرنج از من که امشب مست بودم</p></div>
<div class="m2"><p>به مستی با تو گستاخی نمودم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من امشب ای برادر مستِ مستم</p></div>
<div class="m2"><p>چه باید کرد؟ مخلص مِی پرستم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز فرطِ مستی از دستم فُتد کِلک</p></div>
<div class="m2"><p>چَکد مَی گر بیَفشارَم به هم پِلک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کنارِ سفره از مستی چنانم</p></div>
<div class="m2"><p>که دستم گُم کند راهِ دهانم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گهی بر در خورم گاهی به دیوار</p></div>
<div class="m2"><p>به هم پیچید دو پایم لام الف وار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو آن نو کوزه‌هایِ آب دیده</p></div>
<div class="m2"><p>عرق اندر مَساماتَم دویده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گَرَم در تن نبودی جامۀ کِش</p></div>
<div class="m2"><p>شدی غرقِ عَرق بالین و بالش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر کبریت خواهم بر فروزم</p></div>
<div class="m2"><p>همی ترسم که چون الکل بسوزم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو هم کاه از من و هم کاه دانم</p></div>
<div class="m2"><p>دلیلِ این همه خوردن ندانم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حواسم همچنان بر باده صَرفَست</p></div>
<div class="m2"><p>که گویی قاضیم وین مالِ وَقفَست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>من ایرج نیستم دیگر، شرابم</p></div>
<div class="m2"><p>مرا جامد مپندارید آبم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>الا ای عارفِ نیکو شمایل</p></div>
<div class="m2"><p>که باشد دل به دیدارِ تو مایل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو از دیدارِ رویت دور ماندم</p></div>
<div class="m2"><p>ترا بی مایه و بی نور خواندم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولی در بهترین جا خانه داری</p></div>
<div class="m2"><p>که صاحب خانه‌ای جانانه داری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گُوارا باد مهمانی به جانت</p></div>
<div class="m2"><p>که باشد بهتر از جان میزبانت</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>رشیدُ القَد صحیحُ الفِعلِ و القول</p></div>
<div class="m2"><p>فُتاده آن طرف حتّی ز لاَحول</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مودَّب، با حیا، عاقل، فروتن</p></div>
<div class="m2"><p>مُهَذَّب، پاک‌دل، پاکیزه دیدن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خلیق و مهربان و راست گفتار</p></div>
<div class="m2"><p>توانا با توانایی کم آزار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ندارد با جوانی هیچ شهوت</p></div>
<div class="m2"><p>به خُلوت پاک دامن تر ز جَلوت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو دیده مرکزی‌ها را همه دزد</p></div>
<div class="m2"><p>خیانت کرده و برداشته مزد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز مرکز رشتۀ طاعت گسسته</p></div>
<div class="m2"><p>کمر شخصاً به اصلاحات بسته</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>یکی ژاندارمری بر پا نموده</p></div>
<div class="m2"><p>که دنیا را پر از غوغا نموده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به هر جا یک جوانی با صلاحست</p></div>
<div class="m2"><p>در این ژاندارمری تحت السِّلاحست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همه با قوّت و با استقامت</p></div>
<div class="m2"><p>صحیح البُنیه و خوب و سلامت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چو یک گویند و پا کوبند بر خاک</p></div>
<div class="m2"><p>بیفتد لرزه بر اندامِ افلاک</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>در آن ژاندارمری کردست تأسیس</p></div>
<div class="m2"><p>منظّم مکتبی از بهرِ تدریس</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گروهی بچّه ژاندارمند در وی</p></div>
<div class="m2"><p>که الّلهُمَّ اَحفِظهُم مِنَ الُغَی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همه شِکّر دهن شیرین شمایل</p></div>
<div class="m2"><p>همانطوری که می‌خواهد تو را دل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به رزمِ دشمنِ دولت چو شیرند</p></div>
<div class="m2"><p>به خونِ عاشقان خوردن دِلیرند</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>عبوسانند اندر خانۀ زین</p></div>
<div class="m2"><p>عروسانند گاهِ عزّ و تمکین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همه بر هر فنونِ حرب حایز</p></div>
<div class="m2"><p>همه گوینده هَل مِن مُبارِز</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همه دانایِ فن، دارایِ علمند</p></div>
<div class="m2"><p>تو گویی از قشونِ ویلهلمند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به گاه جست و خیز و ژیمناستیک</p></div>
<div class="m2"><p>تو گویی هست اعضاشان ز لاستیک</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>کشند ار صف ز طهران تا به تجریش</p></div>
<div class="m2"><p>نبینی‌شان به صف یک مو پس و پیش</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چنان با نظم و با ترتیبِ عالی</p></div>
<div class="m2"><p>که اندر ریسمان، عِقدِ لآلی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همانا عارف این اطفال دیدست</p></div>
<div class="m2"><p>که در ژاندارمری منزل گُزیدست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بیا عارف که ساقَت سم در آرد</p></div>
<div class="m2"><p>میانِ لُنبَرَینَت دُم در آرد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>شنیدم سوء خُلقت دبّه کرده</p></div>
<div class="m2"><p>همان یک ذرّه را یک حَبّه کرده</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ترقی کرده ای در بد ادایی</p></div>
<div class="m2"><p>شدستی پاک مالیخولیایی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز منزل در نیایی همچو جوکی</p></div>
<div class="m2"><p>کُنی با مهربانان بد سلوکی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز گُل نازک‌تَرَت گویند و رَنجی</p></div>
<div class="m2"><p>مَجُنب از جایِ خود عارف که گَنجی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یکی گوید که این عارف خیالیست</p></div>
<div class="m2"><p>یکی گوید که مغزش پاک خالیست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یکی بی قید وبی حالت شناسد</p></div>
<div class="m2"><p>یکی وردار و ورمالَت شناسد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یکی گوید که آبِ زیرِ کاهست</p></div>
<div class="m2"><p>یکی گوید که خیر این اشتباهست</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>یکی اصلاً ترا دیوانه گوید</p></div>
<div class="m2"><p>یکی هم مثل من دیوانه جوید!</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>سرِ راهِ حکیمی فحل و دانا</p></div>
<div class="m2"><p>شنیدم داشت یک دیوانه ماوا</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بد آن دیوانه را با عاقلان جنگ</p></div>
<div class="m2"><p>سر و کارش همیشه بود با سنگ</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ولی چشمش که بر دانا فتادی</p></div>
<div class="m2"><p>بر او از مهر لبخندی گشادی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از این رفتارِ او دانا برآشفت</p></div>
<div class="m2"><p>در این اندیشه شد و با خویشتن گفت</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>یقیناً از جنون در من نشانست</p></div>
<div class="m2"><p>که این د یوانه با من مهربانست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>همانا بایدم کردن مداوا</p></div>
<div class="m2"><p>که تا زایل شود جنسیّت از ما</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>یقیناً بنده هم گمراه گشتم</p></div>
<div class="m2"><p>که عارف جوی و عارف خواه گشتم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>بود ناچار میل جنس بر جنس</p></div>
<div class="m2"><p>مُولیتر میل می‌ورزد به هِنسِنس</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>مگو عارف پرستیدن چه شیوست</p></div>
<div class="m2"><p>که در جنگل سبیکه جزء میوه‌ست</p></div></div>