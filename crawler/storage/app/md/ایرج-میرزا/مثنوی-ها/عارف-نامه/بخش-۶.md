---
title: >-
    بخش ۶
---
# بخش ۶

<div class="b" id="bn1"><div class="m1"><p>حجابِ زن که نادان شد چنینست</p></div>
<div class="m2"><p>زنِ مستورۀ محجوبه اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کُس دادن همانا وقع نگذاشت</p></div>
<div class="m2"><p>که با روگیری اُلفت بیشتر داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلی شرم و حیا در چَشم باشد</p></div>
<div class="m2"><p>چو بستی چَشم باقی پَشم باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر زن را بیاموزند ناموس</p></div>
<div class="m2"><p>زند بی‌پرده بر بامِ فلک کوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مستوری اگر پی بُرده باشد</p></div>
<div class="m2"><p>همان بهتر که خود بی‌پَرده باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون آیند و با مردان بجوشند</p></div>
<div class="m2"><p>به تهذیبِ خصالِ خود بکوشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زن تعلیم دید و دانش آموخت</p></div>
<div class="m2"><p>رواقِ جان به نورِ بینش افروخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به هیچ افسون ز عصمت بر نگردد</p></div>
<div class="m2"><p>به دریا گر بیفتد تر نگردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خود بر عالمی پرتو فشانَد</p></div>
<div class="m2"><p>ولی خود از تعرّض دور مانَد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زن رفته کُلِژ دیده فا کُولتِه</p></div>
<div class="m2"><p>اگر آید به پیشِ تو دِکُولتِه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو در وی عفّت و آزرم بینی</p></div>
<div class="m2"><p>تو هم در وی به چَشم شرم بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تمنّایِ غلط از وی مُحال است</p></div>
<div class="m2"><p>خیالِ بد در او کردن خیال است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برو ای مرد فکرِ زندگی کن</p></div>
<div class="m2"><p>نیی خر، ترکِ این خر بندگی کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برون کن از سرِ نحست خُرافات</p></div>
<div class="m2"><p>بجنب از جا که فِی التاخیرِ آفات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفتم من که این دنیا بهشتست</p></div>
<div class="m2"><p>بهشتی حور در لفّافه زشتست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر زن نیست، عشق اندر میان نیست</p></div>
<div class="m2"><p>جهان بی عشق اگر باشد جهان نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به قربانت مگر سیری؟ پیازی؟</p></div>
<div class="m2"><p>که توی بُقچه و چادر نمازی؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو مرآتِ جمالِ ذوالجلالی</p></div>
<div class="m2"><p>چرا مانندِ شلغم در جَوالی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر و ته بسته چون در کوچه آیی</p></div>
<div class="m2"><p>تو خانم جان نه، بادمجانِ مایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان خوبی در این چادر کریهی</p></div>
<div class="m2"><p>به هر چیزی بجز انسان شبیهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کجا فرمود پیغمبر به قرآن</p></div>
<div class="m2"><p>که باید زن شود غولِ بیابان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کدام است آن حدیث و آن خبر کو</p></div>
<div class="m2"><p>که باید زن کند خود را چو لولو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو باید زینت از مردان بپوشی</p></div>
<div class="m2"><p>نه بر مردان کنی زینت فروشی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنین کز پای تا سر در حریری</p></div>
<div class="m2"><p>زنی آتش به جان، آتش نگیری!</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به پا پوتین و در سر چادرِ فاق</p></div>
<div class="m2"><p>نمایی طاقتِ بی‌طاقتان طاق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیندازی گُل و گُلزار بیرون</p></div>
<div class="m2"><p>ز کیف و دستکش دل‌ها کنی خون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شود محشر که خانم رو گرفته</p></div>
<div class="m2"><p>تعالی الله از آن رو کو گرفته!</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پیمبر آنچه فرمودست آن کُن</p></div>
<div class="m2"><p>نه زینت فاش و نه صورت نهان کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حجابِ دست و صورت خود یقینست</p></div>
<div class="m2"><p>که ضدِّ نصِّ قرآنِ مبینست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به عصمت نیست مربوط این طریقه</p></div>
<div class="m2"><p>چه ربطی گوز دارد با شقیقه؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مگر اندر دهات و بینِ ایلات</p></div>
<div class="m2"><p>همه رو باز باشند آن جمیلات</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چرا بی عصمتی در کارشان نیست؟</p></div>
<div class="m2"><p>رواجِ عشوه در بازارشان نیست؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زنان در شهرها چادر نشینند</p></div>
<div class="m2"><p>ولی چادر نشینان غیرِ اینند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در اقطارِ دگر زن یار مردست</p></div>
<div class="m2"><p>در این محنت سراسر بارِ مردست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به هر جا زن بُوَد هم پیشه با مرد</p></div>
<div class="m2"><p>در این جا مرد باید جان کَنَد فرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو ای با مشک و گل همسنگ و همرنگ</p></div>
<div class="m2"><p>نمی‌گردد در این چادر دلت تنگ؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه آخِر غنچه در سیرِ تکامُل</p></div>
<div class="m2"><p>شود از پرده بیرون تا شود گُل؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو هم دستی بزن این پرده بردار</p></div>
<div class="m2"><p>کمالِ خود به عالم کن نمودار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو هم این پرده از رُخ دور می‌کُن</p></div>
<div class="m2"><p>در و دیوار را پر نور می‌کُن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فدای آن سر و آن سینۀ باز</p></div>
<div class="m2"><p>که هم عصمت در او جمعست هم ناز</p></div></div>