---
title: >-
    بخش ۳
---
# بخش ۳

<div class="b" id="bn1"><div class="m1"><p>دلم زین عمرِ بی‌حاصل سرآمد</p></div>
<div class="m2"><p>که ریشِ عمر هم کم‌کم در آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در سر عشق و نه در دل هوس ماند</p></div>
<div class="m2"><p>نه اندر سینه یارای نَفس ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی دندان به درد آید گهی چَشم</p></div>
<div class="m2"><p>زمانی معده می‌آید سرِ خشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فزاید چینِ عارض هر دقیقه</p></div>
<div class="m2"><p>نخوابد موی صد غَم بر شقیقه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ایّام جوانی بد دلم ریش</p></div>
<div class="m2"><p>که می‌روید چرا بر عارضم ریش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون پیوسته دل ریش و پریشم</p></div>
<div class="m2"><p>که میریزد چرا هر لحظه ریشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدین صورت که بارَد مویم از سر</p></div>
<div class="m2"><p>همانا گشت خواهم اُشتُرِگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>أ لا موت یباع فأشتریه</p></div>
<div class="m2"><p>فهذا العیش ما لا خیر فیه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببیند ایرج این اظهارِ غم دَم</p></div>
<div class="m2"><p>که غمگین می‌کنی خواننده را هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرفتم یک دو روزی زود مُردی</p></div>
<div class="m2"><p>چرا سوقِ کلام از یاد بردی؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که ماندَست اندر اینجا جاودانی</p></div>
<div class="m2"><p>که می‌ترسی تو جاویدان نمانی؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ترا صحبت ز عارف بود در پیش</p></div>
<div class="m2"><p>عبث رفتی سرِ بی‌حالیِ خویش</p></div></div>