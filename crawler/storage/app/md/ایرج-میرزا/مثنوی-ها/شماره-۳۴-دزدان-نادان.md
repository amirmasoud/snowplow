---
title: >-
    شمارهٔ  ۳۴ - دزدانِ نادان
---
# شمارهٔ  ۳۴ - دزدانِ نادان

<div class="b" id="bn1"><div class="m1"><p>دو دزد خری دزدیدند</p></div>
<div class="m2"><p>سرِ تقسیم به هم جنگیدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دو بودند چو گَرمِ زَد و خورد</p></div>
<div class="m2"><p>دُزدِ سِوُّم خَرشان را زَد و بُرد</p></div></div>