---
title: >-
    شمارهٔ  ۳۶ - حُرمتِ ربا
---
# شمارهٔ  ۳۶ - حُرمتِ ربا

<div class="b" id="bn1"><div class="m1"><p>گفت روزی به جعفرِ صادق</p></div>
<div class="m2"><p>حیله‌بازی منافقی فاسِق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز حرامِ رِبا چه مقصود است</p></div>
<div class="m2"><p>گفت زان رو که مانعِ جود است</p></div></div>