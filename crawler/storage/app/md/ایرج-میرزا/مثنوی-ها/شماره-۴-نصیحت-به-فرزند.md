---
title: >-
    شمارهٔ  ۴ - نصیحت به فرزند
---
# شمارهٔ  ۴ - نصیحت به فرزند

<div class="b" id="bn1"><div class="m1"><p>از مالِ جهان ز کهنه و نو</p></div>
<div class="m2"><p>دارم پسری به نام خسرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که سالِ او چهار است</p></div>
<div class="m2"><p>پیداست که طفلِ هوشیار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده من چنین نماید</p></div>
<div class="m2"><p>بر دیده غیر تا چه آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که طفل زشت باشد</p></div>
<div class="m2"><p>در چشم پدر بهشت باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آری مثل است که قَر نَبی</p></div>
<div class="m2"><p>در دیده ماد است حسنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان ای پسرِ عزیزِ دلبند</p></div>
<div class="m2"><p>بشنو ز پدر نصیحتی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز این گفته سعادتِ تو جویم</p></div>
<div class="m2"><p>بس یاد بگیر هر چه گویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌باش به عمرِ خود سحرخیز</p></div>
<div class="m2"><p>وز خوابِ سحرگهان بپرهیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندر نَفَسِ سحر نشاطی است</p></div>
<div class="m2"><p>کان را با روح ارتباطی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دریاب سحر کنارِ جو را</p></div>
<div class="m2"><p>پاکیزه بشوی دست و رو را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صابونت اگر بُوَد میسّر</p></div>
<div class="m2"><p>بر شستن دست و رو چه بهتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با هوله پاک خشک کن رو</p></div>
<div class="m2"><p>پس شانه بزن به موی و ابرو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کن پاک و تمیز گوش و گردن</p></div>
<div class="m2"><p>کاین کار ضرورت است کردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا آن که به پهلویت نشینند</p></div>
<div class="m2"><p>چرکِ کَل و گوشِ تو نبینند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در پاکیِ دست کوش کز دست</p></div>
<div class="m2"><p>دانند ترا چه مرتبت هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چرکین مگذار بیخِ دندان</p></div>
<div class="m2"><p>کان وقتِ سخن شود نمایان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیراهنِ خویش کن گزیده</p></div>
<div class="m2"><p>هم شسته و هم اطو کشیده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کن کفش و کلاه با بروس پاک</p></div>
<div class="m2"><p>نیکو بِستُر ز جامه‌ات خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آینه خویش را نظر کن</p></div>
<div class="m2"><p>پاکیزه لباسِ خود به بر کن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از نرم و خشن هر آنچه پوشی</p></div>
<div class="m2"><p>باید که به پاکیش بکوشی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر جامه گلیم یا که دیباست</p></div>
<div class="m2"><p>چون پاک و تمیز بود زیباست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون غیر به پیشِ خویش بینی</p></div>
<div class="m2"><p>انگشت مبر به گوش و بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دندان برِ کس خلال منمای</p></div>
<div class="m2"><p>ناخن برِ این و آن مپیرای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در بزم چنان دهن مدرّان</p></div>
<div class="m2"><p>کت قعرِ دهان شود نمایان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خمیازه کشید می‌نباید</p></div>
<div class="m2"><p>طوری که به خَلق خوش نیاید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون بر سر سفره‌ای نشستی</p></div>
<div class="m2"><p>زنهار نکن دراز دستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زان کاسه بخور که پیشِ دستست</p></div>
<div class="m2"><p>بر کاسه دیگری مبر دست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دِه قوت ز بیش و کم شکم را</p></div>
<div class="m2"><p>دربند مباش بیش و کم را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با مادرِ خویش مهربان باش</p></div>
<div class="m2"><p>آماده خدمتش به جان باش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با چشمِ ادب، نگر پدر را</p></div>
<div class="m2"><p>از گفته او مپیچ سر را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون این دو شوند از تو خُرسند</p></div>
<div class="m2"><p>خُرسند شود ز تو خداوند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در کوچه چو می‌روی به مکتب</p></div>
<div class="m2"><p>معقول گذر کن و مؤدّب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون با ادب و تمیز باشی</p></div>
<div class="m2"><p>پیشِ همه کس عزیز باشی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در مدرسه ساکت و متین شو</p></div>
<div class="m2"><p>بیهوده مگوی و یاوه مشنو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اندر سرِ درس گوش می‌باش</p></div>
<div class="m2"><p>با هوش و سخن نیوش می‌باش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>می‌کوش که هرچه گوید استاد</p></div>
<div class="m2"><p>گیری همه را به چابکی یاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کم گوی و مگوی هرچه دانی</p></div>
<div class="m2"><p>لب دوخته‌دار تا توانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بس سَر که فُتاده زبان است</p></div>
<div class="m2"><p>با یک نقطه زبان زیان است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن قدر رواست گفتنِ آن</p></div>
<div class="m2"><p>کاید ضرر از نهفتنِ آن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نادان به سرِ زبان نهد دل</p></div>
<div class="m2"><p>در قلب بود زبانِ عاقل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اندر وسطِ کلامِ مردم</p></div>
<div class="m2"><p>لب باز مکن تو بر تکلّم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زنهار مگو سخن بجز راست</p></div>
<div class="m2"><p>هرچند ترا در آن ضررهاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گفتارِ دروغ را اثر نیست</p></div>
<div class="m2"><p>چیزی ز دروغ زشت‌تر نیست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا پیشه تست راست‌گویی</p></div>
<div class="m2"><p>هرگز نبری سیاه رویی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از خجلتِ شرمش ار شود فاش</p></div>
<div class="m2"><p>یادآر و دگر دروغ متراش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون خوی کند زبان به دشنام</p></div>
<div class="m2"><p>آن به که بریده باد از کام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از عیبِ کسان زبان فرو بند</p></div>
<div class="m2"><p>عیبش به زیانِ خویش مپسند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زنهار مده بدان به خود راه</p></div>
<div class="m2"><p>کز مونسِ بد نَعوذُ بالله</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در صحبتِ سِفله چون درآیی</p></div>
<div class="m2"><p>بالطّبع به سفلگی گرایی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با مردمِ ذی شرف درآمیز</p></div>
<div class="m2"><p>تا طبعِ تو ذی شرف شود نیز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>لبلابِ ضعیف بین که چندی</p></div>
<div class="m2"><p>پیچد به چنارِ ارجمندی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در صحبتِ او بلند گردد</p></div>
<div class="m2"><p>مانندِ وی ارجمند گردد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در عهدِ شباب چند سالی</p></div>
<div class="m2"><p>کسبِ هنری کن و کمالی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا آن که به روزگارِ پیری</p></div>
<div class="m2"><p>در ذلّت و مسکنت نمیری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>امروز سه سال پیش از این نیست</p></div>
<div class="m2"><p>بی علم دگر نمی‌توان زیست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر صنعت و حرفتی ندانی</p></div>
<div class="m2"><p>زحمت ببری ز زندگانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از طبّ و طبیعی و ریاضی</p></div>
<div class="m2"><p>قلبِ تو به هرچه هست راضی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>یک فن بپسند و خاصِ خود کن</p></div>
<div class="m2"><p>تحصیل به اختصاصِ خود کن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چون خوبِ کم از بدِ فزون به</p></div>
<div class="m2"><p>ذی فن به جهان ز ذی فنون به</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خوانم به تو بیتی از نظامی</p></div>
<div class="m2"><p>آن میرِ سخنوران نامی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>«بالانگری به غایتِ خود</p></div>
<div class="m2"><p>بهتر ز کلاه دوزیِ بد»</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آن طفل که قدرِ وقت دانست</p></div>
<div class="m2"><p>دانستنِ قدرِ خود توانست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هرچ آن که رود ز دستِ انسان</p></div>
<div class="m2"><p>شاید که به دست آید آسان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جز وقت که پیشِ کس نباید</p></div>
<div class="m2"><p>چون رفت ز کف به کف نیاید</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر گوهری از کَفَت برون تافت</p></div>
<div class="m2"><p>در سایه وقت می‌توان یافت</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ور وقت رود ز دستت ارزان</p></div>
<div class="m2"><p>با هیچ گهر خرید نتوان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>هر شب که روی به جامه خواب</p></div>
<div class="m2"><p>کن نیک تأمل اندر این باب</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کان روز به علمِ تو چه افزود</p></div>
<div class="m2"><p>وز کرده خود چه برده‌ای سود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>روزی که در آن نکرده‌ای کار</p></div>
<div class="m2"><p>آن روز ز عمرِ خویش مشمار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>من می‌روم و تو ماند خواهی</p></div>
<div class="m2"><p>وین دفترِ درس خواند خواهی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>اینجا چو رسی مرا دعا کن</p></div>
<div class="m2"><p>با فاتحه روحم آشنا کن</p></div></div>
<div class="b" id="bn72"><div class="m1"><p></p></div>