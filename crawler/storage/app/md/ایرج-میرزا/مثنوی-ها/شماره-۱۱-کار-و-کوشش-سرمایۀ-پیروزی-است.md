---
title: >-
    شمارهٔ  ۱۱ - کار و کوشش سرمایۀ پیروزی است
---
# شمارهٔ  ۱۱ - کار و کوشش سرمایۀ پیروزی است

<div class="b" id="bn1"><div class="m1"><p>برزگری کِشته خود را دِرود</p></div>
<div class="m2"><p>تا چه خود از بَدوِ عمل کِشته بود!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار کَش آورد و بر آن بار کرد</p></div>
<div class="m2"><p>روی ز صحرا سویِ انبار کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سرِ ره تیره گِلی شد پدید</p></div>
<div class="m2"><p>بار کَش و مرد در آن گِل تپید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه بر آن اسب نهیب آزمود</p></div>
<div class="m2"><p>چرخ نجنبید و نبخشید سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برزگر آشفته از آن سوءِ بخت</p></div>
<div class="m2"><p>کرد تن و جامه به خود لَخت لَخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه لَگَدی چند به یابو نواخت</p></div>
<div class="m2"><p>گه دو سه مُشت از زِبَرِ چرخ آخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راه به ده دور بُد و وقت دیر</p></div>
<div class="m2"><p>کس نه به رَه تا شَوَدش دست گیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زار و حزین مویه کُنان موکَنان</p></div>
<div class="m2"><p>کردِ سرِ عَجز سویِ آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کای تو کَنَنده درِ خَیبَر ز جای</p></div>
<div class="m2"><p>بَر کَنَم این بار کَش از تیره لای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هاتِفی از غیب به دادش رسید</p></div>
<div class="m2"><p>کامَدَم ای مرد مشو نا امید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نَک تو بدان بیل که داری به بار</p></div>
<div class="m2"><p>هر چه گِلِ تیره بود کُن کِنار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا مَنَت از مِهر کنم یاوری</p></div>
<div class="m2"><p>بارِ خود از لای بُرون آوَری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برزگر آن کرد و دگر رَه سُروش</p></div>
<div class="m2"><p>آمَدَش از عالمِ بالا به گوش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حال بِنِه بیل و برآوَر کُلَنگ</p></div>
<div class="m2"><p>بر شکن از پیشِ رَه آن قِطعه سنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت شکستم، چه کنم؟ گفت خوب</p></div>
<div class="m2"><p>هرچه شکستی ز سرِ ره بِروب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت برُفتم همه از بیخ و بُن</p></div>
<div class="m2"><p>گفت کُنون دست به شَلاّق کُن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا شَوَم السّاعَه مددکارِ تو</p></div>
<div class="m2"><p>باز رهانم ز لَجَن بارِ تو</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرد نیاورده به شَلّاق دست</p></div>
<div class="m2"><p>بار زِ گِل برزگر از غم بِرَست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین مددِ غیبی گردید شاد</p></div>
<div class="m2"><p>وز سرِ شادی به زمین بوسه داد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کای تو مِهین راه نمایِ سُبُل</p></div>
<div class="m2"><p>نیک برآوَردیَم از گِل چو گُل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت سروشش به تقاضایِ کار</p></div>
<div class="m2"><p>کار ز تو یاوری از کِردِگار</p></div></div>