---
title: >-
    شمارۀ ۸
---
# شمارۀ ۸

<div class="b" id="bn1"><div class="m1"><p>دیروز چه گُلهای جهان افروزی</p></div>
<div class="m2"><p>امروز چه سرمای گلستان سوزی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرندۀ بَرد و آفرینندۀ وَرد</p></div>
<div class="m2"><p>روزی آن طور می پسندد روزی</p></div></div>