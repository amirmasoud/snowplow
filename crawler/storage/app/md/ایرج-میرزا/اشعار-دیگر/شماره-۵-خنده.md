---
title: >-
    شمارهٔ  ۵ - خنده
---
# شمارهٔ  ۵ - خنده

<div class="b" id="bn1"><div class="m1"><p>باغ خندان ز گل خندان است</p></div>
<div class="m2"><p>خنده آئین خرمندان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خنده هر چند که از جد دور است</p></div>
<div class="m2"><p>جد پیوسته نه از مقدور است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل شود رنجه زجد شام و صباح</p></div>
<div class="m2"><p>میکند اصلاح مزاجش به مزاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جد بود پا به سفر فرسودن</p></div>
<div class="m2"><p>هزل یک لحظه به راه آسودن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک نه که از دود دروغ</p></div>
<div class="m2"><p>برد از چهرهٔ قدر تو فروغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تخم کین در دل دانا کارد</p></div>
<div class="m2"><p>خیو خجلت به جبین‌ها آرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شو ز فیاض خرد تلقین جوی</p></div>
<div class="m2"><p>راست گو لیک خوش و شیرین گوی</p></div></div>