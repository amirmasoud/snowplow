---
title: >-
    شمارهٔ  ۱ - من گرفتم تو نگیر
---
# شمارهٔ  ۱ - من گرفتم تو نگیر

<div class="b" id="bn1"><div class="m1"><p>زن گرفتم شدم ای دوست به دام زن اسیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه اسیری که ز دنیا شده ام یکسره سیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود یک وقت مرا با رفقا گردش و سیر</p></div>
<div class="m2"><p>یاد آن روز بخیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زن مرا کرده میان قفس خانه اسیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد آن روز که آزاد ز غمها بودم</p></div>
<div class="m2"><p>تک و تنها بودم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زن و فرزند ببستند مرا با زنجیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بودم آن روز من از طایفه دُرد کشان</p></div>
<div class="m2"><p>بودم از جمع خوشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشی از دست برون رفت و شدم لات و فقیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مجرد که بود خوابگهت بستر گرم</p></div>
<div class="m2"><p>بستر راحت و نرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زن مگیر ؛ ار نه شود خوابگهت لای حصیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنده زن دارم و محکوم به حبس ابدم</p></div>
<div class="m2"><p>مستحق لگدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون در این مسئله بود از خود مخلص تقصیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من از آن روز که شوهر شده ام خر شده ام</p></div>
<div class="m2"><p>خر همسر شده ام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می دهد یونجه به من جای پنیر</p></div>
<div class="m2"><p>من گرفتم تو نگیر</p></div></div>