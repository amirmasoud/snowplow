---
title: >-
    شمارهٔ ۲۳ - در مدح حضرت موسی بن جعفر (ع)
---
# شمارهٔ ۲۳ - در مدح حضرت موسی بن جعفر (ع)

<div class="b" id="bn1"><div class="m1"><p>عاشق آن باشد که چون سودا کند یکجا کند</p></div>
<div class="m2"><p>هر دو عالم با سر یک موی او سودا کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای سوختن پروانه سان پر، وا کند</p></div>
<div class="m2"><p>نی ز سر در راه جانان نی ز جان پروا کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خَم چوگان حکم دوست گردد همچو گوی</p></div>
<div class="m2"><p>خود نبیند در میان تا فرق سر از پا کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق آن باشد که چون در بزم جانان بار یافت</p></div>
<div class="m2"><p>باده اشک سرخ و ساغر دیده دل مینا کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون حدیث لعل جانان بشنود از تار تار</p></div>
<div class="m2"><p>در مزاجش تار، کار نشئه صهبا کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنچنان سازد ز خود خود را، تهی وز دوست پُر</p></div>
<div class="m2"><p>دوست را، مجنون خویش و خویش را لیلا کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق آن باشد که عشقش طعنه بر، وامق زند</p></div>
<div class="m2"><p>وز عذار گلعذارش ناز بر عذرا کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بت بالابلایش گر فرستد صد بلا</p></div>
<div class="m2"><p>خود، نمی بیند بلا تا روی در بالا کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بلا هرگز نپرهیزد، که در راه طلب</p></div>
<div class="m2"><p>جذب جانان خار را گل خاره را، دیبا کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق را، نازم که چون می تازد اندر کشوری</p></div>
<div class="m2"><p>غیر خود هر چیز بیند سربسر یغما کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کیست آن عاشق که در زندان هارون هفت سال</p></div>
<div class="m2"><p>شکر تنهایی برای خالق تنها کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد پسند خاطرش یکتایی و زندان از آن</p></div>
<div class="m2"><p>تا، دوتا خود را به پیش ایزد یکتا کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست در توحید استثنا به غیر از ذات حق</p></div>
<div class="m2"><p>جان فدای آن شهی کوکار مستثنی کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر قَدر گردد مقدّر نیست بی فرمان او</p></div>
<div class="m2"><p>ور قضا باشد مصوّر حکم او امضا کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یک اشاره گر کند عالم شود یکسر عدم</p></div>
<div class="m2"><p>عالمی ایجاد باز از نو به یک ایما کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر جبین ابلیس را او داغ ابلیسی نهد</p></div>
<div class="m2"><p>بوالبشر را آدم او از «علّم الاسما» کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زآب و آتش نوح و ابراهیم را بخشد نجات</p></div>
<div class="m2"><p>آب را، غبرا و آتش لاله ی حمرا کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حضرت موسی بن جعفر کاظم و جاذم که او</p></div>
<div class="m2"><p>ناظم دین است و دین را عزم او انشا کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یارب این موسی چو موسائیست کز یک جلوه یی</p></div>
<div class="m2"><p>رخنه ها، درجان موسی و دل سینا کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>می شکافد سینه ی سینا و عمران زاده را</p></div>
<div class="m2"><p>از ظهور یک تجلّی «خرّمغشیّا» کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه عصا را، در کف موسی نماید اژدها</p></div>
<div class="m2"><p>گاه از همدستی اش موسی یدوبیضا کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکدمی شد همدمش تا یافت این دم ازدمش</p></div>
<div class="m2"><p>ورنه عیسی کی تواند مرده را احیا کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زان سبب باب الحوائج شد لقب او را که او</p></div>
<div class="m2"><p>هر مراد و مطلبی حاصل «کماترضی» کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر که شد امروز چون ابلیس زین در، بی خبر</p></div>
<div class="m2"><p>خاک محرومی به سر در موقف فردا کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مطلعی گردید طالع بازم از عرش خیال</p></div>
<div class="m2"><p>جبرئیل خامه را، برگو که تا انشا کند</p></div></div>