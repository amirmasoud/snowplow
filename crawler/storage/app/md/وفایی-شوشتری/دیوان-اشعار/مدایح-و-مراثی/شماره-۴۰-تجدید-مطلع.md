---
title: >-
    شمارهٔ ۴۰ - تجدید مطلع
---
# شمارهٔ ۴۰ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>خواهد اگر، به جلوه آن روی منوّر آورد</p></div>
<div class="m2"><p>آینه ی جمال خورشید مکدّر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز رخ و زلف و قامت معتدلش در این جهان</p></div>
<div class="m2"><p>کس نشنیده سرو را، سنبل و گل بر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برگذرد، به هر زمین با قد و قامتی چنین</p></div>
<div class="m2"><p>تا به قیامت از زمین سرو و صنوبر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیسوی چون کمندش افکنده زدوش تا کمر</p></div>
<div class="m2"><p>تا به کمند و بند خورشید به چنبر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه چه علی اکبری آنکه چو مهر خاوری</p></div>
<div class="m2"><p>برگذرد ز چرخ اگر، هی به تکاور آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برگذرد زچرخ و از سمّ سمند تیز تک</p></div>
<div class="m2"><p>شکل هلال و اختر و ماه مصوّر آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درگه رزم، رمحش از رامح چرخ بگذرد</p></div>
<div class="m2"><p>نیزه ی او شکست بر، گنبد اخضر آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الحذر الحذر، به گردون رسد از نبرد او</p></div>
<div class="m2"><p>بانگ امان والامان گوش جهان کر آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>العجل العجل زتیغش به قتال دشمنان</p></div>
<div class="m2"><p>قابض روح را، در آن مرحله مضطر آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شده زعفرانی از خوف رخ عدوی او</p></div>
<div class="m2"><p>چهره ی او ز تیغ چون لاله ی احمر آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در صف کارزار، با شوکت و سطوت نبی</p></div>
<div class="m2"><p>بر همه ظاهر و عیان صولت حیدر آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شور شهادتش به سر بود، وگرنه کی توان</p></div>
<div class="m2"><p>تیغ به تارکش فرو منقذ کافر آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهر طراز نیزه می خواست که بر سر سنان</p></div>
<div class="m2"><p>کاکل غرقه خون و آن جُعد معنبر آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون ز شراره ی عطش لعل لبش کبود شد</p></div>
<div class="m2"><p>خواست گلوی تشنه ی خویش زخون تر آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواست شود فدایی کوی پدر، به کربلا</p></div>
<div class="m2"><p>تا که به عرصه ی جزا، برکف خود سر آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر کف خود سرآورد، بهر چه از برای آن</p></div>
<div class="m2"><p>تا به گلوی تشنگان آب ز کوثر آورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آب ز کوثر آورد، بهر که از برای آن</p></div>
<div class="m2"><p>کس که ز آب دیده رخساره ی خودتر آورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواهد اگر رقم کند قصّه ی تشنه کامی اش</p></div>
<div class="m2"><p>کلک «وفایی» از غمش شعله ی آذر آورد</p></div></div>