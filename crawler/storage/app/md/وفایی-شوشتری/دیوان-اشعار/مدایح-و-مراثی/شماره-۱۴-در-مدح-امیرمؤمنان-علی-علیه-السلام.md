---
title: >-
    شمارهٔ ۱۴ - در مدح امیرمؤمنان علی علیه السلام
---
# شمارهٔ ۱۴ - در مدح امیرمؤمنان علی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>چه شود، زراه وفا اگر</p></div>
<div class="m2"><p>نظری به جانب ما کنی</p></div></div>
<div class="b2" id="bn2"><p>که به کیمیای نظر مگر</p>
<p>مس قلب تیره طلا کنی</p></div>
<div class="b" id="bn3"><div class="m1"><p>یمن از عقیق تو آیتی</p></div>
<div class="m2"><p>چمن از رُخ تو روایتی</p></div></div>
<div class="b2" id="bn4"><p>شکر از لب تو حکایتی</p>
<p>اگرش چو غنچه تو واکنی</p></div>
<div class="b" id="bn5"><div class="m1"><p>به شکنج طُرّه ی عنبرین</p></div>
<div class="m2"><p>که به مهر چهر تو شد قرین</p></div></div>
<div class="b2" id="bn6"><p>شب و روز تیره ی این حزین</p>
<p>تو بدل به نور و ضیا کنی</p></div>
<div class="b" id="bn7"><div class="m1"><p>بنما، ز پسته تبسّمی</p></div>
<div class="m2"><p>بنما، ز غنچه تکلّمی</p></div></div>
<div class="b2" id="bn8"><p>به تکلّمی و تبسّمی</p>
<p>همه دردها تو دوا کنی</p></div>
<div class="b" id="bn9"><div class="m1"><p>تو مراد من تو نجات من</p></div>
<div class="m2"><p>به حیات من به ممات من</p></div></div>
<div class="b2" id="bn10"><p>چه زیان کنی چه ضرر، بری</p>
<p>که برآوری و عطا کنی</p></div>
<div class="b" id="bn11"><div class="m1"><p>تو شه سریر ولایتی</p></div>
<div class="m2"><p>تو مه منیر هدایتی</p></div></div>
<div class="b2" id="bn12"><p>چه شود، گهی به عنایتی</p>
<p>نگهی به سوی گدا کنی</p></div>
<div class="b" id="bn13"><div class="m1"><p>تو چرا، الست بربّکم</p></div>
<div class="m2"><p>نزنی بزن که اگر، زنی</p></div></div>
<div class="b2" id="bn14"><p>ازل و ابد همه ذرّه ذرّه</p>
<p>پر از صدای بلا کنی</p></div>
<div class="b" id="bn15"><div class="m1"><p>ز غمم چرا نکنی رها</p></div>
<div class="m2"><p>و اگر کنی فمتی متی</p></div></div>
<div class="b2" id="bn16"><p>که زبطن حوت بسی رها</p>
<p>تو چو یونس بن متی کنی</p></div>
<div class="b" id="bn17"><div class="m1"><p>تو شهی شهان همه چاکرت</p></div>
<div class="m2"><p>تو مهی مهان همه بر درت</p></div></div>
<div class="b2" id="bn18"><p>که شوند قنبر قنبرت</p>
<p>تو قبول اگر، ز وفا کنی</p></div>
<div class="b" id="bn19"><div class="m1"><p>تو به شهر علم نبی دری</p></div>
<div class="m2"><p>تو ز انبیا همه برتری</p></div></div>
<div class="b2" id="bn20"><p>تو غضنفری و تو صفدری</p>
<p>چو میان معرکه جا کنی</p></div>
<div class="b" id="bn21"><div class="m1"><p>تو زنی به دوش نبی قدم</p></div>
<div class="m2"><p>فکنی بتان همه از حرم</p></div></div>
<div class="b2" id="bn22"><p>حرم از وجود تو محترم</p>
<p>ز صفا صفا تو صفا کنی</p></div>
<div class="b" id="bn23"><div class="m1"><p>تو چه صادری تو چه مصدری</p></div>
<div class="m2"><p>تو چه جلوه یی و چه مظهری</p></div></div>
<div class="b2" id="bn24"><p>که هم اوّلی و هم آخری</p>
<p>همه جا تو کار خدا کنی</p></div>
<div class="b" id="bn25"><div class="m1"><p>ز حدوث چتر و علم زنی</p></div>
<div class="m2"><p>قَدم از قِدم به عدم زنی</p></div></div>
<div class="b2" id="bn26"><p>ز عدم تو نقش و رقم زنی</p>
<p>و بنای هر دو سرا کنی</p></div>
<div class="b" id="bn27"><div class="m1"><p>من اگر خدای ندانمت</p></div>
<div class="m2"><p>متحیّرم که چه خوانمت</p></div></div>
<div class="b2" id="bn28"><p>که اگر خدای بدانمت</p>
<p>تو بری شوی و ابا کنی</p></div>
<div class="b" id="bn29"><div class="m1"><p>تو تمیز مؤمن و کافری</p></div>
<div class="m2"><p>تو قسیم جنّت و آذری</p></div></div>
<div class="b2" id="bn30"><p>که سعید را تو جزا، دهیّ</p>
<p>و عنید را تو جزا کنی</p></div>
<div class="b" id="bn31"><div class="m1"><p>شب و روز، را تو مقدّری</p></div>
<div class="m2"><p>تو مدبّری تو منوّری</p></div></div>
<div class="b2" id="bn32"><p>که مساء را تو کنی صباح</p>
<p>و صباح را تو مسا کنی</p></div>
<div class="b" id="bn33"><div class="m1"><p>به خدا «وفایی» با خطا</p></div>
<div class="m2"><p>همه خوف او بود از بداء</p></div></div>
<div class="b2" id="bn34"><p>که مباد دست رجای او</p>
<p>ز عطای خود تو جدا کنی</p></div>
<div class="b" id="bn35"><div class="m1"><p>دو جهان شود همه کربلا</p></div>
<div class="m2"><p>ز فغان و ناله چو در عزا</p></div></div>
<div class="b2" id="bn36"><p>گذری به عرصه ی نینوا و</p>
<p>بسان نی تو نوا کنی</p></div>
<div class="b" id="bn37"><div class="m1"><p>ز جگر تو نعره ی حیدری</p></div>
<div class="m2"><p>ز غم حسین چو برآوری</p></div></div>
<div class="b2" id="bn38"><p>ز خروش و ناله تو عرش و فرش</p>
<p>تمام کرب و بلا کنی</p></div>