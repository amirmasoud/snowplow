---
title: >-
    شمارهٔ ۱۶ - در مدح مولی الموحّدین امیرالمؤمنین علی علیه السلام
---
# شمارهٔ ۱۶ - در مدح مولی الموحّدین امیرالمؤمنین علی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>از هلال عید دوش ابرو کمان کرد آسمان</p></div>
<div class="m2"><p>تا به ابروی تو ماند کج کمان کرد آسمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا قیامت شد ز خجلت با سیه رویی قرین</p></div>
<div class="m2"><p>مهر خود تا با مه رویت قران کرد آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف حُسن ترا، تا پیر زال چرخ دید</p></div>
<div class="m2"><p>حلقه ی مه را، کلاف ریسمان کرد آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زشغل شیر گیری لحظه یی غافل شوند</p></div>
<div class="m2"><p>خواب را، شب برغزالانت شبان کرد آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قمر در عقرب زلف رُخت در هر مهی</p></div>
<div class="m2"><p>از پی تشبیه تصویری عیان کرد آسمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرتوی از مهر رویت تافت اندر کاخ من</p></div>
<div class="m2"><p>ای عجب ما را، زمین چون آسمان کرد آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرزویم را، برآورد و مرادم را بداد</p></div>
<div class="m2"><p>کوکبم را، سعد و عیشم رایگان کرد آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان با اینکه از ناکامی ما کامجوست</p></div>
<div class="m2"><p>خویش را ناکام و ما را کامران کرد آسمان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسمان در هر وجودی مایه ی حُزن و غم است</p></div>
<div class="m2"><p>در وجود ما اثر چون زعفران کرد آسمان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رشوتم داد آسمان از خوف این تیغ زبان</p></div>
<div class="m2"><p>تا نگویم من چنین یا آنچنان کرد آسمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من رهین رشوه ی او نیستم امّا حکیم</p></div>
<div class="m2"><p>خود نگوید که فلان یا بهمدان کرد آسمان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا خوشان خوش، ناخوشان را ناخوش و ناسازگار</p></div>
<div class="m2"><p>در مزاج نارضامندان زیان کرد آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آسمان مقهور و مجبور است در، ادوار خویش</p></div>
<div class="m2"><p>چند گویی آسمان کرد آسمان کرد آسمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آسمان را نیست تأثیری مؤثّر، دیگریست</p></div>
<div class="m2"><p>تهمّت است این گر کسی گوید فلان کرد آسمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در جهان نبود مؤثّر جز خداوند جهان</p></div>
<div class="m2"><p>آسمان را، هم خداوند جهان کرد آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن خداوندی که اسم اعظمش باشد علی</p></div>
<div class="m2"><p>مر، خداوندیش صدبار امتحان کرد آسمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ردّ شمسش را نمود و باز هم خواهد نمود</p></div>
<div class="m2"><p>حکم او در هر زمان برخود روان کرد آسمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از نهیبش سر برآرد چون ز مغرب آفتاب</p></div>
<div class="m2"><p>آن زمان باید مکان در لامکان کرد آسمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صولجان قدرتش تا بهر خلقت شد بلند</p></div>
<div class="m2"><p>خود چو گوی اندر خم آن صولجان کرد آسمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا شود، بر پرچم چتر جلالش آستر</p></div>
<div class="m2"><p>خویش را، بر شکل چتر و سایبان کرد آسمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیضه، زرّین جوجه سیمین آورد از ماه مهر</p></div>
<div class="m2"><p>تا به زیر قاف قصرش آشیان کرد آسمان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خون خصم از میغ تیغش تاکه باریدن گرفت</p></div>
<div class="m2"><p>تیغ خود، در میغ از بیمش نهان کرد آسمان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا که همرنگ غلامانش شود با صد نیاز</p></div>
<div class="m2"><p>خویشتن بر آستانش پاسبان کرد آسمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دلدش را داد میکائیل کیل از سُنبله</p></div>
<div class="m2"><p>مُشتی از وی ریخت نامش کهکشان کرد آسمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شکلی از نعل سمندش تا کند شاید بیان</p></div>
<div class="m2"><p>با هلال و خوشه ی پروین بیان کرد آسمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بس طریق بندگی پیمود تا از مهر و ماه</p></div>
<div class="m2"><p>خویش را، در فوج او صاحب نشان کرد آسمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خیر و شرّ، مهر و وفا نیک و بد و جور و جفا</p></div>
<div class="m2"><p>ای «وفایی» تهمّت است این کاسمان کرد آسمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسمان رسواست بی تقصیر و بی جرم و خطا</p></div>
<div class="m2"><p>چون تو هم از روز اوّل این زیان کرد آسمان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رعد را، می دانی امّا سرّ او را باز دان</p></div>
<div class="m2"><p>کز بلای کربلا از دل فغان کرد آسمان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برق باشد یک شرار، از شعله ی آه حسین</p></div>
<div class="m2"><p>کان شرار از سینه ی سوزان عیان کرد آسمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آسمان باران همی بارد ولی تا روز حشر</p></div>
<div class="m2"><p>گریه ها باشد که بر لب تشنگان کرد آسمان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روز عاشورا، مگر نشنیده یی این ماجرا</p></div>
<div class="m2"><p>خاک می افشاند و خون از دل روان کرد آسمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جامه زد، در نیل ماتم تا قیامت زین عزا</p></div>
<div class="m2"><p>زیر بار غم قدی همچون کمان کرد آسمان</p></div></div>