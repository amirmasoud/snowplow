---
title: >-
    شمارهٔ ۱۷ - در مدح حضرت زهرا علیهاالسلام
---
# شمارهٔ ۱۷ - در مدح حضرت زهرا علیهاالسلام

<div class="b" id="bn1"><div class="m1"><p>دختر طبعم از سخن رشته به گوهر آورد</p></div>
<div class="m2"><p>بهر طراز مدحت دخت پیمبر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دختر از این قبیل اگر هست هماره تا ابد</p></div>
<div class="m2"><p>مادر روزگار، ای کاش که دختر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آورد از کجا و کی مادر دهر این چنین</p></div>
<div class="m2"><p>فاطمه یی که مظهرش قدرت داور آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونکه خداش برگُزید از همه ی زنان سزد</p></div>
<div class="m2"><p>جاریه و کنیز او ساره و هاجر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پایه ی قدر وجاهش ار، خواست کند کسی بیان</p></div>
<div class="m2"><p>حامل عرش، عرش را پایه ی منبر آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حق چو ندید همسرش در همه ممکنات از آن</p></div>
<div class="m2"><p>لازم و واجب آمدش خلقت حیدر آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونکه به خدمتش ملک فخر کنند بایدی</p></div>
<div class="m2"><p>بوالبشر از نتاج سلمان و اباذر آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذوق «وفایی» از تواش بود حلاوت این چنین</p></div>
<div class="m2"><p>کز، نی، کلک صفحه را، معدن شکّر آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر طلوع انجم اشک عزاش بایدی</p></div>
<div class="m2"><p>اختر طبع من ز نو مطلع دیگر آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آه از آندمی که او روی به محشر آورد</p></div>
<div class="m2"><p>جامه ی نور دیده ی خویش ز خون تر آورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لرزه به عرش کبریا، رعشه به جسم انبیا</p></div>
<div class="m2"><p>اوفتد آن زمان که او بر کف خود سر آورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناله ی وا حسین از او سر زند آنچنان کزان</p></div>
<div class="m2"><p>گوش تمام اهل محشر، زفغان کر آورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مادر اکبرش ز پی مویه کنان بسان نی</p></div>
<div class="m2"><p>ناله و بانگ یا بنیّ در صف محشر آورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز جزا، شود ز سر شور قیامت دیگر</p></div>
<div class="m2"><p>چون ز جفا بُریده سر، قامت اکبر آورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رشته ی جان انس و جان بگسلد آن زمان که آن</p></div>
<div class="m2"><p>کاکل غرقه خون و آن جُعد معنبر آورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شافع عرصه ی جزا، اوفتدش ز کف لوا</p></div>
<div class="m2"><p>بیرق واژگون چو عبّاس دلاور آورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ناید اگر شفاعت آن روز به خونبهای او</p></div>
<div class="m2"><p>کیست که ایمنی در آن ورطه ز آذر آورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست «وفایی»اش امّید آنکه به جروز رستخیز</p></div>
<div class="m2"><p>از اثر شفاعتش چهره منّور آورد</p></div></div>