---
title: >-
    شمارهٔ ۲۶ - تجدید مطلع
---
# شمارهٔ ۲۶ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>شهنشاهی که مرآت مثال الله علیا شد</p></div>
<div class="m2"><p>جمال ایزدی از نور روی او هویدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ممکن غیر ممکن بود دیدن ذات واجب را</p></div>
<div class="m2"><p>چو آن شه جلوه گر شد در جهان حلّ معمّا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صفات ایزدی یکسر، به ذاتش مدغم و مضمر</p></div>
<div class="m2"><p>گهی شد مظهرالاسما و گاهی عین اسما شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در اوّل صفحه ی امکان چو صادر گشت لفظ کن</p></div>
<div class="m2"><p>کتاب نسخه ی هستی ز کِلک وی محشّا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امام هشتمین و قبله ی هفتم که نُه گردون</p></div>
<div class="m2"><p>چو صحن روضه اش از ثابت و سیّار زیبا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امیر عالم تجرید و شاه کشور تفرید</p></div>
<div class="m2"><p>امین خطّه ی توحید و شرط لا و الاّ شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدوثش یا قِدم همسر، گهی صادر گهی مصدر</p></div>
<div class="m2"><p>طفیلش ماسوی یکسر گواهم حرف لولا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رضای او رضای حق ز وی افعال حق مشتق</p></div>
<div class="m2"><p>وجودش از وجود اسبق بعینه عین یکتا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به امر او قدر، کاری به حکم او قضا جاری</p></div>
<div class="m2"><p>به عالم فیض او ساری ز اعلی تا به ادنی شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به هر دردیست او درمان از او هر مشکلی آسان</p></div>
<div class="m2"><p>خراسان شد خراسان زانکه او را، جا و مأوا شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امام ثامن و ضامن حرم از حرمتش آمِن</p></div>
<div class="m2"><p>به امر او زمین ساکن به حکمش چرخ پویا شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندانم کیست او یا چیست لیکن اینقدر دانم</p></div>
<div class="m2"><p>که دستش دست حق و پایه اش از هرچه بالا شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به دست قدرتش تاشد مخمّر آدم آدم شد</p></div>
<div class="m2"><p>ز فیض علّم الاسما، مکرّم گشت و والا شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهی شد نوح را کشتی گهی بر کشتی اش پشتی</p></div>
<div class="m2"><p>گهی شد ساحل جودی نجات وی ز دریا شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لباس خلّتش را داشت چون در بر خلیل الله</p></div>
<div class="m2"><p>سراسر نار نمرودی به وی برداً سلاما شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تمنّا کرد موسی تا که بیند روی یزدان را</p></div>
<div class="m2"><p>ز نور روی او یکذرّه در طور آشکارا شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نمی دانم چه شد زان ذرّه امّا اینقدر دانم</p></div>
<div class="m2"><p>تجلّی کرد در سینا و خرّ موسی شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به چاک جامه ی مریم دمید از روی رأفت دم</p></div>
<div class="m2"><p>که بی جُفت اندر این عالم تولّد زو مسیحا شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زفیض سایه ی سرو قد آن دوحه ی احمد</p></div>
<div class="m2"><p>چمان اندر چمن سرو صنوبر سبز و رعنا شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مگر حکم ابوّت داد لطفش ابر نیسان را</p></div>
<div class="m2"><p>که طفل قطره در بطن صدف لؤلوی لالا شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امین حضرت عزّت معین مذهب و ملّت</p></div>
<div class="m2"><p>قسیم دوزخ و جنّت نظام دین و دنیا شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به قدرت معجز آورده نه در مخفی نه در پرده</p></div>
<div class="m2"><p>به شیر پرده هی کرده که خصم جان اعدا شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به خلاّقی و رزّاقی و غفّاری و قهّاری</p></div>
<div class="m2"><p>به حول و قوّه ی باری به هر چیزی توانا شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز درگاه رضا کس نارضا هرگز نمی گردد</p></div>
<div class="m2"><p>که کویش قبله ی حاجات بر اهل سماوا شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>«وفایی» دارد اندر دل هزاران عقده ی مشکل</p></div>
<div class="m2"><p>نگردد، گر در اینجا حلّ کجا خواهد جز اینجا شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عدویت باد، سرگردان چو گوی اندر خم چوگان</p></div>
<div class="m2"><p>محبّت تا که سرگرم از تولاّ و تبرّا شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دلم سوزد، به حال آن شه مظلوم بی یاور</p></div>
<div class="m2"><p>که در شهر خراسان کشته اندر دست اعدا شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز جور و کینه ی مأمون دلش لبریز شد از خون</p></div>
<div class="m2"><p>به طشت از حلق او بیرون همه احشاء و امعا شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ملایک سر بسر گردیده مشغول عزاداری</p></div>
<div class="m2"><p>خدا صاحب عزا بهر رضا در عرش اعلا شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خداوند جهان کشتند امّا زین عجب دارم</p></div>
<div class="m2"><p>که نی افلاک ویران شد نه عالم زیر و بالا شد</p></div></div>