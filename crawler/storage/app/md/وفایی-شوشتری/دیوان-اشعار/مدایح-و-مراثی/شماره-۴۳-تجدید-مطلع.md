---
title: >-
    شمارهٔ ۴۳ - تجدید مطلع
---
# شمارهٔ ۴۳ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>به حکم شاه دین بر کوفه رفتن چون مصمّم شد</p></div>
<div class="m2"><p>بساط خرّمی برچیده و ماتم فراهم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرام اندر جهان گردید عیش و عشرت و شادی</p></div>
<div class="m2"><p>چو او ساز سفر بنمود آغاز محرّم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به وصف قدر و جاه او همین بس کز همه یاران</p></div>
<div class="m2"><p>پی تبلیغ فرمان حسین مسلم مسلّم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش اهل دانش چون مسلّم بود در رفعت</p></div>
<div class="m2"><p>به معراج شهادت از برای شاه سلّم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به فرد جان نثاری فرد بود از همگنان یکسر</p></div>
<div class="m2"><p>که در ثبت شهادت از همه یاران مقدّم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزد، بر ممکناتش افتخار اندر نسب کاو را</p></div>
<div class="m2"><p>حسین بن علی بن ابیطالب پسر عمّ شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جز با ابن عمّش شاه دین تمثیل قدر او</p></div>
<div class="m2"><p>مثال ذرّه و خورشید یا دریا و شبنم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقام تختِ بخت او به رفعت برتر از کرسی</p></div>
<div class="m2"><p>اساس قصر قدرش در فراز عرش اعظم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به میزان خرد با ذرّه یی از قدر و مقدارش</p></div>
<div class="m2"><p>دو عالم را بسنجیدم به وزن از، ارزنی کم شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانم پایه ی جاه و جلالش را ولی دانم</p></div>
<div class="m2"><p>پی تعظیم پیش رفعتش پشت فلک خم شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وجود و بود او نُه چنبر افلاک را مرکز</p></div>
<div class="m2"><p>نوال جود او در قسمت ارزاق مقسم شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>امیری شیرگیری آنکه در رزم پلنگانش</p></div>
<div class="m2"><p>به گاه صید شیر چرخ چون کلب معلّم شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قدر پیوسته هم پرواز شد با طایر تیرش</p></div>
<div class="m2"><p>اجل با تیغ خونریزش به روز رزم همدم شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همانا تیغ در دستش بسان آتش سوزان</p></div>
<div class="m2"><p>همانا نیزه در شستش بسان مار ارقم شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سراسر در جهان دشمن فرو نگذاشتی یک تن</p></div>
<div class="m2"><p>به میدانی که پای عزم او در رزم محکم شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>میان فرق خصم و برق تیغش فرق نتوانم</p></div>
<div class="m2"><p>که حرف حرقِ برق تیغ او با فرق مدغم شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عدو گردید یکدم جرعه نوش ساغر تیغش</p></div>
<div class="m2"><p>به کامش تا به روز حشر شهد زندگی سمّ شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هرکس صرصر تیغش وزیدی می توان گفتن</p></div>
<div class="m2"><p>اگر از اهل جنّت بود، واصل بر جهنّم شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رُخش جنّت قدش طوبی لبش کوثر دلش دریا</p></div>
<div class="m2"><p>به هر عضوی ز سر تا پا بهشتی را مجسّم شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کفش کافی دلش صافی به عهد خویشتن وافی</p></div>
<div class="m2"><p>گواهش در صفا رکن و مقام و حجر و زمزم شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ولی با اینهمه جاه و جلال و قوّت و قدرت</p></div>
<div class="m2"><p>اسیر کوفیان گردید و توام با دو صد غم شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو سوی کوفه شد بگرفت عهد و بیعت از کوفی</p></div>
<div class="m2"><p>ولیکن بستن و بشکستن آن عهد با هم شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در اوّل از وفا بستند عهد آن ناکسان امّا</p></div>
<div class="m2"><p>در آخر از جفا آن عهد، عهد قتل و ماتم شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وفا ز اهل جهان هرگز مجو کاسم وفاداری</p></div>
<div class="m2"><p>به عالم ناقص و کم چون منادای مرخّم شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بس جور و ستم زان بی وفایان رفت بر مسلم</p></div>
<div class="m2"><p>دل زار «وفایی» در غمش پیمانهٔ غم شد</p></div></div>