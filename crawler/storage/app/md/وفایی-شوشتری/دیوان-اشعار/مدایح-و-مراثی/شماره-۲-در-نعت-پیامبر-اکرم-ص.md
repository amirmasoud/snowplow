---
title: >-
    شمارهٔ ۲ - در نعت پیامبر اکرم (ص)
---
# شمارهٔ ۲ - در نعت پیامبر اکرم (ص)

<div class="b" id="bn1"><div class="m1"><p>روزگار از نکهت زلف نگارم عنبرین شد</p></div>
<div class="m2"><p>گیتی از عکس رخش رشک نگارستان چین شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توده ی غبرا، ملوّن از شقایق گشت و سنبل</p></div>
<div class="m2"><p>ساحت گلشن مزیّن ز ارغوان و یاسمین شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جویباران ز آب باران بهاری همچو کوثر</p></div>
<div class="m2"><p>آبها شیرین و صافی هر طرف چون انگبین شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست از یُمن قدوم آن نگار عنبرین مو</p></div>
<div class="m2"><p>کاین چنین روی زمین چون روضه ی خُلدبرین شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در، بهای یکسر مویش نباشد هر دو گیتی</p></div>
<div class="m2"><p>قیمت خاک کف پایش بهشت و حور عین شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل لعل شکّرینش در زبان دارم که گویی</p></div>
<div class="m2"><p>نظم شیرین روان بخشم چو لعل شکّرین شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خامه ام مانا، کلیم الله را ماند که اینسان</p></div>
<div class="m2"><p>مطلعی نو چون ید بیضا برونش ز آستین شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی نعت رسولم تا بُراق طبع زین شد</p></div>
<div class="m2"><p>طایر عقلم دلیل راه چون روح الامین شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نباشد جذبه یی در کار و عشقی در سر از وی</p></div>
<div class="m2"><p>بر مقامش کی برد، پی گرچه ز ارباب یقین شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست احمد با احد در هر صفت یکتا ولیکن</p></div>
<div class="m2"><p>این دوئیت در حقیقت کامل از یک اربعین شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قرنها پیش از وجود عالم و آدم نبی بود</p></div>
<div class="m2"><p>او نبوّت داشت کادم در میان ماء وطین شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اوست دست کردگار و دست دست اوست بالله</p></div>
<div class="m2"><p>گر شنیدی خاک آدم با، ید قدرت عجین شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه آخر از همه پیغمبران آمد ولیکن</p></div>
<div class="m2"><p>علّت ایجاد خلق اوّلین و آخرین شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شرع او متقن بود مانند عهد لایزالی</p></div>
<div class="m2"><p>دین و آئینش بسی محکم تر، از عرش برین شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون تمام رحمت حق در وجودش گشته مضمر</p></div>
<div class="m2"><p>لاجرم شخص شریفش رحمةٌ للعالمین شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عقل کل نفس مشیّت مبدء فیض نُخستین</p></div>
<div class="m2"><p>مظهر حق سیّد لولاک خیرالمرسلین شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ایزدش در بزم قُرب کبریایی برد، انسان</p></div>
<div class="m2"><p>تا گذشت از قاب قوسین بلکه با وی همنشین شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قصّه ی معراج را تقریر نتوانم ولیکن</p></div>
<div class="m2"><p>طالب و مطلوب را، دانم که در یکجا قرین شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیم تکفیر ار نبودی اقتران این و آن را</p></div>
<div class="m2"><p>بی تأمّل گفتمی کاین عین آن، آن عین این شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از چه معشوق، ازل بی پرده گردید آشکارا</p></div>
<div class="m2"><p>گرنه عشقش پرده افکن زان جمال نازنین شد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر نبودی او نبودی حرف توحیدی به عالم</p></div>
<div class="m2"><p>در تجلّی شاهد توحید را عشقش معین شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لا و الاّیی نبودی گر نبودی ذات پاکش</p></div>
<div class="m2"><p>حرف استثنایش اندر حفظ حق حصنی حصین شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از پی نعت جلالش مطلعی از شرق طبعم</p></div>
<div class="m2"><p>همچو خورشید جمالش آشکارا و مبین شد</p></div></div>