---
title: >-
    شمارهٔ ۳۳ - در مدح حضرت زینب سلام الله علیها
---
# شمارهٔ ۳۳ - در مدح حضرت زینب سلام الله علیها

<div class="b" id="bn1"><div class="m1"><p>نمی دانم چه بر سر خامه ی عنبر فشان دارد</p></div>
<div class="m2"><p>که خواهد سرّی از اسرار پنهانی عیان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به مدح دختر زهرا مگر خواهد سخن گوید</p></div>
<div class="m2"><p>که با نغمات منصوری اناالحق بر زبان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آهنگ حسینی مدح بانوی حجازی را</p></div>
<div class="m2"><p>به صد شور و نوا خواهد به عالم رایگان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بانو آنکه او را، نور حق در آستین باشد</p></div>
<div class="m2"><p>چه بانو آنکه جبریلش سر اندر آستان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حیا، بند نقاب او بود عفّت حجاب او</p></div>
<div class="m2"><p>ز عصمت آفتاب او مکان در لامکان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا عصمت تماشا کن که از بهر خریداری</p></div>
<div class="m2"><p>در این بازار یوسف هم کلاف و ریسمان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبوّت شأن پیغمبر ولایت در خور حیدر</p></div>
<div class="m2"><p>نه این دارد نه آن امّا نشان از این و آن دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تکلّم کردنش را هر که دیدی فاش می گفتی</p></div>
<div class="m2"><p>لسان حیدری گویا که در طیّ لسان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود ناموس حق آن عصمت مطلق که از رفعت</p></div>
<div class="m2"><p>کمینه چاکر او پا به فرق فرقدان دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود نُه کرسی افلاک کمتر پایه ی قدرش</p></div>
<div class="m2"><p>اگر گویم که قصر قدر و جاهش نردبان دارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز شرم روی او باشد که این مهر درخشان را</p></div>
<div class="m2"><p>به دامان زمینش آسمان هر شب نهان دارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبیند تا که عقرب پرتوی از ماه رخسارش</p></div>
<div class="m2"><p>فلک از قوس بهر کوری اش تیر و کمان دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به جرم اینکه نرگس دیده اش باز است در گلشن</p></div>
<div class="m2"><p>ز شرمش تا قیامت رخ به رنگ زعفران دارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیفتد تا نظر بر سایه اش خورشید تابان را</p></div>
<div class="m2"><p>به چشم خویش از خطّ شعاعی صد سنان دارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نگویم من بود مریم کنیز مادرش زهرا</p></div>
<div class="m2"><p>اگر راضی شود او مریمش منّت به جان دارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زنی با این همه شوکت ندیده دیده ی گردون</p></div>
<div class="m2"><p>زنی با این همه سطوت به عالم کی نشان دارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرا با این همه جاه و جلال و عصمتش دوران</p></div>
<div class="m2"><p>میان کوچه و بازار در هر سو عیان دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خرد گفتا خموش ای بی خبر از سرّ این معنی</p></div>
<div class="m2"><p>که هرکس قُربش افزون تر فزونتر امتحان دارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ندارم باور ار گویند دیدش دیده ی مردم</p></div>
<div class="m2"><p>که دود آه خویشش مخفی از نامحرمان دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر مستوره ی ایجاد چون خورشید رخشنده</p></div>
<div class="m2"><p>نه بر سر چادر و نه ساتر و نه سایبان دارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تجلّی کرد تا ظاهر شود حق ورنه در باطن</p></div>
<div class="m2"><p>ز بال قدسیان هم ساتر و هم سایبان دارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در این محفل بود زهرای اطهر حاضر و ناضر</p></div>
<div class="m2"><p>و گرنه گفتمی زینب چه آذرها به جان دارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن آهسته تر باید که شاید نشود زهرا</p></div>
<div class="m2"><p>وگرنه سوز آهش صد خطر بر حاضران دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صبا، رو در نجف برگو تو با آن شیریزدانی</p></div>
<div class="m2"><p>که زینب در دمشق و کوفه چشم خونفشان دارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگو از داغ مرگ نوجوانان پیر شد زینب</p></div>
<div class="m2"><p>به زیر بار محنت سرو قدّی چون کمان دارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خصوص از مرگ اکبر تا قیامت داغ و چون قمری</p></div>
<div class="m2"><p>به پای سرو قدّش ز اشک خود جویی روان دارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس از قتل حسین با یک جهان غم چون کند زینب</p></div>
<div class="m2"><p>کز اطفال صغیر و تشنه لب یک کاروان دارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر خواهم ز غمهایش بیان یک داستان سازم</p></div>
<div class="m2"><p>به هر یک داستان از غم هزاران داستان دارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بود بهر شفاعت هر کسی را حجّتی بر کف</p></div>
<div class="m2"><p>«وفایی» حجّتی قاطع از این تیغ زبان دارد</p></div></div>