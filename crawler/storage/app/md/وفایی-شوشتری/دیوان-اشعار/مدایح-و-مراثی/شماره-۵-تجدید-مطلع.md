---
title: >-
    شمارهٔ ۵ -  تجدید مطلع
---
# شمارهٔ ۵ -  تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>ای با، قدم حدوث وجود تو همسرا</p></div>
<div class="m2"><p>وی صادر نُخست تویی اصل مصدرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالله پس از خدا، تو خداوند عالمی</p></div>
<div class="m2"><p>نه غالی ام ترا و نه منکر، به داورا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم خدا، به چه می شد شناخته</p></div>
<div class="m2"><p>گر شخص کامل تو نبودیش مظهرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالله که واجب است وجود تو در جهان</p></div>
<div class="m2"><p>ورنه چگونه گشتی واجب مصوّرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم دست کردگاری و هم روی کردگار</p></div>
<div class="m2"><p>هم سرّ کردگاری و هم روی داورا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تیغ آبدار تو هست آتشی نهان</p></div>
<div class="m2"><p>کان را، کسی نداند جز عمرو کافرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارد کتاب فضل تو چندین هزار، باب</p></div>
<div class="m2"><p>یک باب از آن بیان شده در باب خیبرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وصف تو نیست رجعت خورشید ز آسمان</p></div>
<div class="m2"><p>مدح تو، نی دریدن در، مهد اژدرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با، یک اشاره شیر فلک بردری زهم</p></div>
<div class="m2"><p>زیر و زبر کنی به هم این چرخ چنبرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکم قضا، به امر و رضای تو برقرار</p></div>
<div class="m2"><p>کار قَدر، به حکم تو گردد مقدّرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی حکم تو نمیرد، یک نفس در جهان</p></div>
<div class="m2"><p>بی امر تو نزاید، یک طفل مادرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی اذن تو نبارد، یک قطره بر زمین</p></div>
<div class="m2"><p>بی رأی تو نیاید از بحر گوهرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی لطف تو نروید، یک گل ز گلستان</p></div>
<div class="m2"><p>بی مهر تو نباشد در باغ ضیمرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی امر تو نریزد، یک برگ از درخت</p></div>
<div class="m2"><p>بی حکم تو نخیزد یکمو به پیکرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی یاد تو نجنبد، جنبنده یی ز جا</p></div>
<div class="m2"><p>بی قهر تو نسوزد سوزنده اخگرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک شمه یی ز خُلق تو هر هشت باغ خُلد</p></div>
<div class="m2"><p>یک ذرّه یی ز مهر تو هر هفت اخترا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا مظهرالعجایب و یا مرتضی علی</p></div>
<div class="m2"><p>خواندن ترا، به یاری از هر چه بهترا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هستم دخیل قنبرت ای شاه لافتی</p></div>
<div class="m2"><p>فریادرس تو ما را، فضلاً لقنبرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاها امیدوار چنانم که خوانی ام</p></div>
<div class="m2"><p>از سلک چاکران غلامان این درا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر شعر من قبول تو افتد، مرا رسد</p></div>
<div class="m2"><p>فخر ار، کنم به اهل دو عالم سراسرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به به چه خوش بود، که بخوانند دوستان</p></div>
<div class="m2"><p>این شعر را، پس از من تا روز محشرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کز، زنگ قنبر آید و هم از حَبش بلا</p></div>
<div class="m2"><p>از روم هم مُهیب و «وفایی» ز شوشترا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دانم که این نه حدّ من است و نه جای من</p></div>
<div class="m2"><p>لیکن اگر تو خواهی از اینم فزونترا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بعد از ثنا، به یاد من آمد حسین تو</p></div>
<div class="m2"><p>آن تشنه لب شهید، به خون غرقه پیکرا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لب تشنه بود، بر لب آب فُرات و بود</p></div>
<div class="m2"><p>آب فرات یکسره اش مهر مادرا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بی کس حسین، غریب حسین، بینوا حسین</p></div>
<div class="m2"><p>نه مادرش بسر، نه پسر، نی برادرا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>امّا برادرش سرو دستش ز تن جدا</p></div>
<div class="m2"><p>عبّاس تشنه کام علمدار لشگرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امّا پسر که بود، شبیه پیمبرا</p></div>
<div class="m2"><p>شد پاره پاره از دم شمشیر و خنجرا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کردند تشنه لب همه اصحاب او شهید</p></div>
<div class="m2"><p>از کوچک و بزرگ چه اکبر چه اصغرا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اموالشان تمام به تاراج کینه رفت</p></div>
<div class="m2"><p>از گوهر و لباس و زر و زیب و زیورا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زینب کجا و مجلس آل زنا کجا</p></div>
<div class="m2"><p>زینب کجا و بزم یزید ستمگرا</p></div></div>