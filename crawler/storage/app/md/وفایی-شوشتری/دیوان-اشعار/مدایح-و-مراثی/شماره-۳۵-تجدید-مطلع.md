---
title: >-
    شمارهٔ ۳۵ - تجدید مطلع
---
# شمارهٔ ۳۵ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>شها تو ماهی و مهرت به دل گرفته قرار</p></div>
<div class="m2"><p>به بندگیّ تو دارم من از ازل اقرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تویی که ماه بنی هاشمت همی خوانند</p></div>
<div class="m2"><p>در آسمان نکویی و در سپهر وقار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آفتاب حجازی و ماه کنعانت</p></div>
<div class="m2"><p>کلاف جان به کف دل نهاده در بازار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شها تو یوسف حُسنی و یوسفان جهان</p></div>
<div class="m2"><p>به پیش حُسن تو چون صورتند بر دیوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا چنانکه تو هستی مدیح نتوان کرد</p></div>
<div class="m2"><p>که عقل را به سر کوی عشق نبود بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سفیر عقل کجا ره برد، به کشور عشق</p></div>
<div class="m2"><p>که جای عشق بلند است و ره بسی دشوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امیر کشور عشقی و در وفاداری</p></div>
<div class="m2"><p>نیامده است و نیاید به عصری از اعصار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پی وفای حسین اینقدر فشردی پای</p></div>
<div class="m2"><p>که هر دو دست برفتت ز دست و دست از کار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آستان تو سوگند کاستانه ی تو</p></div>
<div class="m2"><p>ز عرش برتر و بالاتر است چندین بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اساس قصر جلال تو بسکه هست رفیع</p></div>
<div class="m2"><p>جز ایزدش نتوان بود دیگری معمار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شها، به مدح و ثنای تو طایر طبعم</p></div>
<div class="m2"><p>چو مرغکی است که از بحر تر کند منقار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا چو مدح و ثنا در خور جلال تو نیست</p></div>
<div class="m2"><p>پس از ثنا ز ثنا، می نمایم استغفار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی به مدح تو چون ذات من بود مجبور</p></div>
<div class="m2"><p>از این قبیل سخن سر از او زند ناچار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنانکه از پی تجدید مطلعی دیگر</p></div>
<div class="m2"><p>زبان چو شعله ی تیغ تو گشته آتشبار</p></div></div>