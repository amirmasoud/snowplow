---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>لعل شکر افشانم گفتا نمکین باشد</p></div>
<div class="m2"><p>گفتم نمکم گفتا حقّ نمک این باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت من و زلفینش همرنگ همند، آری</p></div>
<div class="m2"><p>یکرنگی اگر باشد با، ماش همین باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه من و گردون را، فرقی که بود این است</p></div>
<div class="m2"><p>کان ماه فلک امّا این ماه زمین باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دختر رز، ما را خود پرده در افتاده</p></div>
<div class="m2"><p>بی پرده به ساغر، به تاپرده نشین باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد دل من نسبت با چین سرزلفش</p></div>
<div class="m2"><p>چون مشک بود از خون چون زآهوی چین باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زینسان که کند چشمت هر لحظه به من لطفی</p></div>
<div class="m2"><p>خوب است ولی خواهم قدری به از این باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق زغم جانان باشد به دلش پنهان</p></div>
<div class="m2"><p>آن داغ که زاهد را پیدا، به جبین باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند «وفایی» را، مهرش بزدای از دل</p></div>
<div class="m2"><p>بزدایمش از دل چون کان نقش نگین باشد</p></div></div>