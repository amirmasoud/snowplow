---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>به روی خوب تو دیدیم روی خوب یزدان را</p></div>
<div class="m2"><p>به کفر زلف تو دادیم نقد ایمان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بطوف کعبه ی اسلام بت پرست شدیم</p></div>
<div class="m2"><p>خبر دهید ز ما کافرو مسلمان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جز دلم که زند خویش را، بدان خم زلف</p></div>
<div class="m2"><p>کسی ندیده زند گوی لطمه چوگان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم به حلقه ی زلفش گزیده است مقام</p></div>
<div class="m2"><p>بود، که جمع کند خاطر پریشان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای کشتنم افراخته است پیوسته</p></div>
<div class="m2"><p>کمان ابرو و آن تیرهای مژگان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طلوع صبح سعادت شود، دمی که صبا</p></div>
<div class="m2"><p>زلطف باز کند چاک آن گریبان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جویبار دو چشمم گذر نما ای سرو</p></div>
<div class="m2"><p>که از نظر فکنم سروهای بُستان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یک تبسّم شیرین ربودی از من دل</p></div>
<div class="m2"><p>تبسّمی دگر، ای دوست تا دهم جان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«وفایی» از گل روی تو می زند دستان</p></div>
<div class="m2"><p>چنانکه بسته زبان هزار دستان را</p></div></div>