---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>حُسنت چو عشق من همه ساعت فزون شود</p></div>
<div class="m2"><p>تا منتهای کار ندانم که چون شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کار جان گرهی سخت هست لیک</p></div>
<div class="m2"><p>آسان شود دمیکه دل از عشق خون شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حاصل ز دور چرخ مرادم شود اگر</p></div>
<div class="m2"><p>این گردشش چو طالع من واژگون شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون با خیال روی تو خواب آیدم به چشم</p></div>
<div class="m2"><p>مژگان به جای سوزنم اندر جفون شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکباره سرنگون شود این چرخ بیستون</p></div>
<div class="m2"><p>در زیر بار محنت من گر، ستون شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناید برون زخانه اگر طفل اشک من</p></div>
<div class="m2"><p>ترسد که پایمال شود چون برون شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتی خوش است عقل «وفایی» به کیش عشق</p></div>
<div class="m2"><p>آری به شرط آنکه در آخر جنون شود</p></div></div>