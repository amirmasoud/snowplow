---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>هر مثل کز دهنت ای بت زیبا زده‌ایم</p></div>
<div class="m2"><p>گر به جز هیچ مثالی زده بی‌جا زده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان دهن دم نتوانیم زدن گر بزنیم</p></div>
<div class="m2"><p>حرفی از نقطهٔ موهوم به ایما زده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود، به یاد لب تو شیرهٔ شکّر نوشیم</p></div>
<div class="m2"><p>بوسه از تنگی الفاظ به معنی زده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما خریدیم به جان فتنهٔ ابروی ترا</p></div>
<div class="m2"><p>خویش را، بر دم شمشیر به عمدا زده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به جز عشق تو نبود، به دو گیتی هنری</p></div>
<div class="m2"><p>لاجرم زیر هنرها همه یکجا زده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر یک جلوه چو موسی ارنی گو همه عمر</p></div>
<div class="m2"><p>عَلَم عشق تو بر قلّهٔ سینا زده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نهادیم به سر تاج غلامیّ ترا</p></div>
<div class="m2"><p>طعنه بر افسر اسکندر و دارا زده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا که ما خاک‌نشین سر کوی تو شدیم</p></div>
<div class="m2"><p>خیمه بالاتر، از این گنبد مینا زده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دل نازک ما با دل سنگین بتان</p></div>
<div class="m2"><p>شیشه‌ای هست که بر صخرهٔ صمّا زده‌ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتنهٔ چشم تو از درد دل ماست که ما</p></div>
<div class="m2"><p>سرمهٔ ناز بر آن نرگس شهلا زده‌ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا «وفایی» نکند عشق بتان را اظهار</p></div>
<div class="m2"><p>بر دهان و دل او مُهر خموشا زده‌ایم</p></div></div>