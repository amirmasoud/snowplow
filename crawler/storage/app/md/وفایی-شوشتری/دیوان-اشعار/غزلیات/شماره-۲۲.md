---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>زهی علاقه که با تار زلف یار ببستم</p></div>
<div class="m2"><p>که از علاقه به زلفش بسی علاقه گسستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش خلق شدم متّهم به زهد و کرامت</p></div>
<div class="m2"><p>قسم به باده که زاهد نیم خدای پرستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهل میکده دارم امید آنکه پیاپی</p></div>
<div class="m2"><p>دهند و باز ستانند، هی پیاله ز دستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیُمن همّت ساقی که داد از آن می باقی</p></div>
<div class="m2"><p>زهر پیاله خمار دگر پیاله شکستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شیخ و پیر مغان هر دو رو سفیدم از آنرو</p></div>
<div class="m2"><p>که توبه یی ننمودم که توبه یی نشکستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببستی و بشکستی هزار عهد، ولی من</p></div>
<div class="m2"><p>درست بر سر پیمان و عهد روز الستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال چشم ترا، بسکه در نظر بگرفتم</p></div>
<div class="m2"><p>چو چشم شوخ تو اکنون نه هوشیار و نه مستم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتم آنکه نگیری مرا به هیچ گناهی</p></div>
<div class="m2"><p>همین گناه مرا بس که با وجود تو هستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کنج میکده خوش می سرود دوش «وفایی»</p></div>
<div class="m2"><p>جز اینکه باده پرستم زهر خیال برستم</p></div></div>