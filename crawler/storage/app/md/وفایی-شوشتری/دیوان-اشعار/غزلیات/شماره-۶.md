---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ره از همه جا بسته ولی راه تو باز است</p></div>
<div class="m2"><p>عالم همه را بردر تو روی نیاز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم گله از زلف تو بسیار ولیکن</p></div>
<div class="m2"><p>گر، بازنمایم سر این رشته درازاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارباب بصیرت همه دانند که محمود</p></div>
<div class="m2"><p>کحل بصرش خاک کف پای ایاز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچند نیم لایق بخشایشت امّا</p></div>
<div class="m2"><p>چشم طمعم بر در احسان تو باز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود قبله و چشم سیهت قبله نما شد</p></div>
<div class="m2"><p>وان طاق دو ابروی تو محراب نماز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هر دو جهان قبله ی کوی تو گزیدیم</p></div>
<div class="m2"><p>روسوی تو داریم که بهتر زحجاز است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم تو به هر، بی سر و پا بر سر لطف است</p></div>
<div class="m2"><p>جز با من دلخسته که پیوسته به ناز است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگر مزن آتش به دل زار «وفایی»</p></div>
<div class="m2"><p>کز آتش رخسار تو در سوز و گداز است</p></div></div>