---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>از سر کوی تو هرگز به ملامت نروم</p></div>
<div class="m2"><p>خواهم ار، رفت الهی به سلامت نروم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهشت سرکوی تو به فردوس برین</p></div>
<div class="m2"><p>نروم گر بروم تا به قیامت نروم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر روم روزی از ین در، به سوی روضه ی خُلد</p></div>
<div class="m2"><p>تا که جان را، ندهم من به غرامت نروم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون به جز، مستی ورندی نبود مذهب عشق</p></div>
<div class="m2"><p>می بده می که پی زهد و کرامت نروم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شده هر نقش و نگارم به نظر خار، چنان</p></div>
<div class="m2"><p>که به زلف و خط و خال و قد و قامت نروم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سرت گر، به سرم تیر چو باران بارد</p></div>
<div class="m2"><p>همچو طفلان نگریزم ز حجامت نروم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزمایش منما مورچه با سنگ گران</p></div>
<div class="m2"><p>که اگر رفت نشان ره به علامت نروم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر تو ای دوست وفادار «وفایی» باشی</p></div>
<div class="m2"><p>به خدا از سر کویت به ملامت نروم</p></div></div>