---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>مولای همه علیست مولای خدا</p></div>
<div class="m2"><p>او هم روی خداست هم رای خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر، می بود خدای را همتایی</p></div>
<div class="m2"><p>من می گفتم علیست همتای خدا</p></div></div>