---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>تا گشت رضای او رضای من و دل</p></div>
<div class="m2"><p>حاصل شده است مدّعای من و دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر، از غم او هلاک گردم چه غم است</p></div>
<div class="m2"><p>یک دم غم اوست خونبهای من و دل</p></div></div>