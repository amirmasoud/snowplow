---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>زهّاد، به دخت رز، ببندند نکاح</p></div>
<div class="m2"><p>بیزار شوید زین چنین زهد و صلاح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این زهد و صلاح را، طلاقی گویید</p></div>
<div class="m2"><p>وز، خُم شنوید دم به دم بانگ فلاح</p></div></div>