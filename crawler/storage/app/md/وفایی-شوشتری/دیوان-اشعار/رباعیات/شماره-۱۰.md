---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>در آرزوی جرعه ی می جانم سوخت</p></div>
<div class="m2"><p>از سر، تا پا تمام ارکانم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این حالت «وفایی» ار خواهم مُرد</p></div>
<div class="m2"><p>می دان تو یقین که دین و ایمانم سوخت</p></div></div>