---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>از علم بود عمل «وفایی» منظور</p></div>
<div class="m2"><p>گر، بی عمل است جمله کبر است، و غرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علمی که به پیش عالم بی عمل است</p></div>
<div class="m2"><p>مانند چراغ باشد اندر، کف کور</p></div></div>