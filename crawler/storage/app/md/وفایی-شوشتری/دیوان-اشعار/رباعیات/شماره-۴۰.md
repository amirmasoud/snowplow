---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>می نوش که تا، زندهٔ جاوید شوی</p></div>
<div class="m2"><p>در هر دو جهان قبله ی امّید شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ساغر، اگر خوری «وفایی» به خدا</p></div>
<div class="m2"><p>از سر تا پا تمام توحید شوی</p></div></div>