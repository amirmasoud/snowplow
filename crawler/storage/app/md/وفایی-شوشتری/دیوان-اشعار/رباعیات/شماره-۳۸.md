---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>شک نیست «وفایی» که خدا نیست علی</p></div>
<div class="m2"><p>امّا، دمی از خدا جدا نیست علی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم اگرش جدا خدا، نیست رضا</p></div>
<div class="m2"><p>خوانم اگرش خدا رضا، نیست علی</p></div></div>