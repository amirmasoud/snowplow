---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>گر، دوست خداست گو همه دشمن باش</p></div>
<div class="m2"><p>در حصن حصین قادر ذوالمن باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تکیه به حفظ او کنی چون یُونُس</p></div>
<div class="m2"><p>در کام نهنگ اگر روی ایمن باش</p></div></div>