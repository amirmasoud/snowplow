---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>مُشکی که ز نافه است و اصلش ز خطاست</p></div>
<div class="m2"><p>گویی اگرش غیر خطا عین خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با حُبّ علی نافهٔ هرکس نبرند</p></div>
<div class="m2"><p>شک نیست که او ز اصل مادر، به خطاست</p></div></div>