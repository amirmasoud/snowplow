---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>عشّاق ز عشقت همه در سوز و گداز</p></div>
<div class="m2"><p>زهّاد ز شوقت همه در وجد و نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم منِ محروم به حسرت چشمی</p></div>
<div class="m2"><p>از دور، که مانده است بر، روی تو باز</p></div></div>