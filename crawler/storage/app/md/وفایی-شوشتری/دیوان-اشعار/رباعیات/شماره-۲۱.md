---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>یک جرعه ی می اگر، دهندم چه شود</p></div>
<div class="m2"><p>آسوده اگر، زغم کنندم چه شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندان به یکی ساغر می گر بکنند</p></div>
<div class="m2"><p>فارغ ز خیال چون و چندم چه شود</p></div></div>