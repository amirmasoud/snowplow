---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>با موی سفید آمدم و روی سیاه</p></div>
<div class="m2"><p>نا کرده ترا، بندگی و کرده گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از کرده و ناکردهٔ خود منفعلم</p></div>
<div class="m2"><p>«لا حُولَ وَلا قوّة الاّ بالله»</p></div></div>