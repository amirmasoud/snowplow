---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بر دوش پیمبر چو علی بالا، شد</p></div>
<div class="m2"><p>بگذشت ز قوسین و به اوادنی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معراج نبی به هر کجا بود از وی</p></div>
<div class="m2"><p>یک قامت احمدی علی اعلی شد</p></div></div>