---
title: >-
    جواب «فارس» به قطعهٔ «وفایی»
---
# جواب «فارس» به قطعهٔ «وفایی»

<div class="b" id="bn1"><div class="m1"><p>ای مرا هم قبله هم مالک رقاب</p></div>
<div class="m2"><p>ای به چرخ عقل و دانش آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که از دیوان منشی ازل</p></div>
<div class="m2"><p>شد «وفایی» وفادارت خطاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که گلزار بدیع نظم را</p></div>
<div class="m2"><p>آبیاری کرده کلکت چون سحاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که پیش رأی و روی روشنت</p></div>
<div class="m2"><p>روز و شب در سجده ماه و آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای همایون نامه ی عمان عیون</p></div>
<div class="m2"><p>کز محیط خاطرت جُست انشعاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون به من آورد پیک نیک پی</p></div>
<div class="m2"><p>خواند او را فصل فصل و باب باب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب خطّش شعله زد بر چشم من</p></div>
<div class="m2"><p>آنچنان کز چشم مهر انگیز داب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریخت جزر و مد لفظ و معنی اش</p></div>
<div class="m2"><p>در کنارم در جهان درّ خوشاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد فرّخ لفظش از فرخندگی</p></div>
<div class="m2"><p>جان فارس تازه چون عهد شباب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز استعاراتش چو گشتم با نصیب</p></div>
<div class="m2"><p>در سخن سنجی شدم کامل نصاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در فنون فصل و ابواب حکم</p></div>
<div class="m2"><p>بود مانا دفتر فصل الخطاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مشک ساییّ مدادش نافه دید</p></div>
<div class="m2"><p>مشک نابش شد دوباره خون ناب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هرکه دید آن نافه و گفتار و خط</p></div>
<div class="m2"><p>گفت «ماذا انّهُ شئٌ عُجاب»</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>این سخنگو کیست یارب کز دمش</p></div>
<div class="m2"><p>مغز گیتی پر شد از بوی گلاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این «وفایی» قبله گاه «فارس» است</p></div>
<div class="m2"><p>کز فسون آتش بر انگیزد ز آب</p></div></div>