---
title: >-
    بند بیست و دوم
---
# بند بیست و دوم

<div class="b" id="bn1"><div class="m1"><p>هفتاد تن ز عشق چو از پا در اوفتاد</p></div>
<div class="m2"><p>پس قرعه اش به نام علی اکبر اوفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدار را که نرخ به جان بسته بود عشق</p></div>
<div class="m2"><p>دیگر از آن گذشت و ز جان برتر اوفتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بالا گرفت قیمت دیدار حسُن یار</p></div>
<div class="m2"><p>چون کار با جوان پری پیکر اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان جهان و روح روان آنکه از نخست</p></div>
<div class="m2"><p>در هر صفت شبیه به پیغمبر اوفتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پای تا به سر همه جان بود جسم او</p></div>
<div class="m2"><p>جان را چه گویمش که زبان قاصر اوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور شهادتش به سر افتاد و پس به کف</p></div>
<div class="m2"><p>بنهاد سر به پای پدر با سر اوفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت ای پدر ترا نتوانم غریب دید</p></div>
<div class="m2"><p>از بی پناهی ات به دلم آذر اوفتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رخصت گرفت و رفت و زد و کُشت می فکند</p></div>
<div class="m2"><p>نوعی که شور حشر در آن لشگر اوفتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در عرصه ی نبرد ز شمشیر او بسی</p></div>
<div class="m2"><p>تن های بی سر و سر بی مغفر اوفتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد عرصه گاه جنگ بر اسب عقاب تنگ</p></div>
<div class="m2"><p>از بس به روی هم به زمین پیکر اوفتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برگشت سوی باب ولی با دلی کباب</p></div>
<div class="m2"><p>از تاب تشنگی به شکایت در اوفتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتا ز سوز تشنگی و ثقل آهنم</p></div>
<div class="m2"><p>این تن بسان کوره ی آهنگر اوفتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یک قطره آب کاش میسّر شدی پدر</p></div>
<div class="m2"><p>کز التهاب بر جگرم اخگر اوفتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انگشتری ز گوهرش اندر دهان نهاد</p></div>
<div class="m2"><p>زین عقد عقده ها به دل گوهر اوفتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انسان مکید آب زگوهر که آتشی</p></div>
<div class="m2"><p>از حلق او به حلقه ی انگشتر اوفتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پس از پی وداع حرم سوی خیمه رفت</p></div>
<div class="m2"><p>شوری عجیب در حرم اطهر اوفتاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر حال آن ذبیح چو لیلا نظاره کرد</p></div>
<div class="m2"><p>در اضطراب و واهمه چون هاجر اوفتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گفت ای امید قلب من آیا چه واقع است</p></div>
<div class="m2"><p>شور شهادتت مگر اندر سر اوفتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مادر، فراق جسم زجان گرچه مشکل است</p></div>
<div class="m2"><p>امّا فراق روی تو مشکل تر اوفتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر خیال خال لبت ای پسر دگر</p></div>
<div class="m2"><p>دل همچو عود و سینه مرا مجمر اوفتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتش نظر نما و ببین زاده ی بتول</p></div>
<div class="m2"><p>در چنگ خصم بی کس و بی یاور اوفتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فرزند تست قابل قربانی حسین</p></div>
<div class="m2"><p>بهر تو نزد حق چه از این بهتر اوفتاد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فرزند تو فدایی فرزند بانویی است</p></div>
<div class="m2"><p>کاو از همه زنان به جهان اطهر اوفتاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>داغیست بر دل تو «وفایی» که آتشی</p></div>
<div class="m2"><p>زین شعر تر به مجلس و بر منبر اوفتاد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>داغم به دل فزون بود از چارده ولی</p></div>
<div class="m2"><p>این داغِ آخر از همه افزونتر اوفتاد</p></div></div>
<div class="b2" id="bn26"><p>یارب دلی زداغ «وفایی» خبر مباد</p>
<p>یعنی کسی به ماتم و داغ پسر مباد</p></div>