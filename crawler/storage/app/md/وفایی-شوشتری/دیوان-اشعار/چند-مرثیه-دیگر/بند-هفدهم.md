---
title: >-
    بند هفدهم
---
# بند هفدهم

<div class="b" id="bn1"><div class="m1"><p>آه از دمی که رو به ره آورد کاروان</p></div>
<div class="m2"><p>بر هفتم آسمان شد از آن کاروان فغان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک کاروان تمام زن و طفل خورد سال</p></div>
<div class="m2"><p>از جور چرخ بی کس و در، بند ناکسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک تن نبود محرمشان غیر عابدین</p></div>
<div class="m2"><p>آن هم علیل و زار و گرفتار و ناتوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردان کاروان همه بی سر به روی خاک</p></div>
<div class="m2"><p>سرها، به نیزه با سرِ سالار کاروان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آشوبِ حشر شور قیامت شد آشکار</p></div>
<div class="m2"><p>چون سوی قتلگاه شد آن کاروان روان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدند سروران همه تن داده بر قضا</p></div>
<div class="m2"><p>دل بر قدر نهاده و سر داده بر سنان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن های مهوشان همه افتاده بر زمین</p></div>
<div class="m2"><p>هر یک چو آفتابی و برتر ز آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی تاب بر زمین همه افکنده خویش را</p></div>
<div class="m2"><p>از ناقه ها چو برگ خزان موسم خزان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زن های بی برادر و اطفال بی پدر</p></div>
<div class="m2"><p>هریک کشیده در بر خود پیکری چو جان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن بلبلان زار، به گلزار قتلگاه</p></div>
<div class="m2"><p>چون جسم گلرخان همه از دیده خون فشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر بلبلی ز داغ گلی با هزار، شور</p></div>
<div class="m2"><p>افکنده غلغلی که گلم رفته از میان</p></div></div>
<div class="b2" id="bn12"><p>بر باد رفت گلشن زهرا، به نینوا</p>
<p>افتاده بلبلان خوش الحانش از نوا</p></div>