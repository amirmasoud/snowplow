---
title: >-
    تضمین ابیاتی از غزل صائب
---
# تضمین ابیاتی از غزل صائب

<div class="b" id="bn1"><div class="m1"><p>گر از این واقعه اشکت ز بصر می گذرد</p></div>
<div class="m2"><p>قدر این قطره ز دریای گهر می گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می دهد آهت از این غم به شبستان لحد</p></div>
<div class="m2"><p>نور شمعی که زخورشید و قمر می گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از آن شب که حسین گفت به یاران فردا</p></div>
<div class="m2"><p>هر که دارد سر تسلیم ز سر می گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«چون صدف مُهر خموشی بگذارید به لب»</p></div>
<div class="m2"><p>از شما ورنه در فیض خبر می گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دلیران ظفر پیشه در این دشت وصال</p></div>
<div class="m2"><p>تیر باران بلا همچو مطر می گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاج زیبای شفاعت نهد ای قوم به سر</p></div>
<div class="m2"><p>از شما هر که چو من از سر و زر می گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این مکانیست که از حلق علی اصغر من</p></div>
<div class="m2"><p>از کماندار قضا تیر قدر می گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>«جگر شیر نداری سفر عشق مکن»</p></div>
<div class="m2"><p>«سبزه ی تیغ در این ره ز کمر می گذرد»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>«دل دشمن به تهی برگی من می سوزد»</p></div>
<div class="m2"><p>«برق از این مزرعه با دیده ی تر می گذرد»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زینب از حرف جگر سوز برادر می گفت</p></div>
<div class="m2"><p>«چاردیوار مرا آب ز سر می گذرد»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اُف به دنیا که دل آزرده از او بادل خون</p></div>
<div class="m2"><p>قرّة العین نبی تشنه جگر می گذرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر شهیدی دم تسلیم به آن شه می گفت</p></div>
<div class="m2"><p>جان نثار تو چه با فتح و ظفر می گذرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرِ خود دید چو بر دامن آن شه حُرّ گفت</p></div>
<div class="m2"><p>«رشته چون بی گره افتد زگهر می گذرد»</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخرین گفته ی نور دل لیلا این بود</p></div>
<div class="m2"><p>«پای بر عرش نهد هر که زسر می گذرد»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مستمع باش «وفایی» که پی ذکر حسین</p></div>
<div class="m2"><p>«سخن صائب پاکیزه گهر می گذرد»</p></div></div>