---
title: >-
    بند سیزدهم
---
# بند سیزدهم

<div class="b" id="bn1"><div class="m1"><p>دگر چو نوبت آن کودک صغیر آمد</p></div>
<div class="m2"><p>ز چرخ پیر خروش ملک به زیر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان نثاری بابا، ز گاهواره ی ناز</p></div>
<div class="m2"><p>نخورد شیر تو گفتی چو بچه شیر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که گر، به جثّه صغیرم ولی به رتبه کبیر</p></div>
<div class="m2"><p>کبیر را ندهند آب چون صغیر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر به کار پدر نامد این پسر روزی</p></div>
<div class="m2"><p>درست آمده امروز اگر چه دیر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی چو گوهر بی آب را بهایی نیست</p></div>
<div class="m2"><p>پی نثار تو این دُر بسی حقیر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفت مادر و آوردش او به نزد پدر</p></div>
<div class="m2"><p>که این پسر دگر از جان خویش سیر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تشنگی نه به تن جان نه شیر در پستان</p></div>
<div class="m2"><p>مرا دل از غم این طفل در نفیر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگر عقیق لبش کز کبودی است سیاه</p></div>
<div class="m2"><p>نگر که لعل بدخشان به رنگ قیر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفت بر سر دستش چو گوهری غلطان</p></div>
<div class="m2"><p>به سوی معرکه ناچار و ناگزیر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به روی دست پدر در میانه ی میدان</p></div>
<div class="m2"><p>برای کشته شدن او بسی دلیر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشید ناله حسین کای سپاه کوفه و شام</p></div>
<div class="m2"><p>خود این پسر زرسولیست کو بشیر آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود نبیره و فرزند پادشاه رسل</p></div>
<div class="m2"><p>که او بشیر و نذیر است و بی نظیر آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر به نزد شما قدر او حقیر بود</p></div>
<div class="m2"><p>ولی به نزد خدا قدر او کبیر آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به غیر قطره ی آبی نخواهد او ز شما</p></div>
<div class="m2"><p>حقیر نیست ولی خواهشش حقیر آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمی کنید به طفلان اشک من رحمی</p></div>
<div class="m2"><p>کنید رحم به این طفل کو صغیر آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برای کودک بی شیر، آب می طلبید</p></div>
<div class="m2"><p>که تیر حرمله ی ملحد شریر آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به جای شیر طلب کرد، آب آن مظلوم</p></div>
<div class="m2"><p>به جای آب شرار از خدنگ تیر آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رسید آب ز پیکان به حلق تشنه ی او</p></div>
<div class="m2"><p>چو مرغ بسمل در خون زوی صفیر آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پی تسلّی بابا تبسّمی بنمود</p></div>
<div class="m2"><p>که سوز تیر به حلقم چه دلپذیر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگو به مادر زارم اگر که کودک تو</p></div>
<div class="m2"><p>ز شیر سیر نشد خود زتیر سیر آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دگر بگو به «وفایی» به ماتم فرزند</p></div>
<div class="m2"><p>صبور باش که عمر جهان قصیر آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حسین سبط رسول است و نور چشم بتول</p></div>
<div class="m2"><p>ببین چه بر سرش از دست چرخ پیر آمد</p></div></div>
<div class="b2" id="bn23"><p>دلی که در غم فرزند بوتراب بود</p>
<p>به روز حشر دگر فارغ از عذاب بود</p></div>