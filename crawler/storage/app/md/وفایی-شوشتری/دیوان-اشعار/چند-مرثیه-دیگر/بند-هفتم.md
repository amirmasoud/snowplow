---
title: >-
    بند هفتم
---
# بند هفتم

<div class="b" id="bn1"><div class="m1"><p>عشق آن بود، که از تو تویی را به در کند</p></div>
<div class="m2"><p>ویرانه ی وجود تو زیر و زبر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آن بود، که هرکه بدان گشت سربلند</p></div>
<div class="m2"><p>بر نیزه سر نماید و با نیزه سر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق آن بود، که تشنه ی دیدار یار را</p></div>
<div class="m2"><p>حنجر زآب خنجر فولاد تر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق کسی بود، که به دوران عاشقی</p></div>
<div class="m2"><p>بر خود حدیث عیش جهان مختصر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکس که در زمانه شود دردمند عشق</p></div>
<div class="m2"><p>از راحت زمانه به کلّی حذر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ جان هر آنکه نشاند نهال غم</p></div>
<div class="m2"><p>نبود غمش که خشک شود یا ثمر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق به جز حسین علی کیست در جهان</p></div>
<div class="m2"><p>کز بهر دوست از همه عالم گذر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کو چون حسین آنکه زسودای عاشقی</p></div>
<div class="m2"><p>نه شادمان به نفع و نه خوف از ضرر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کو چون حسین آنکه به میدان امتحان</p></div>
<div class="m2"><p>جانان هر آنچه گویدش او بیشتر کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق خواهدش که تن به خدنگ بلا، دهد</p></div>
<div class="m2"><p>او جان و تن به تیر بلایش سپر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خود گذشته اکبر از جان عزیزتر</p></div>
<div class="m2"><p>در راه دوست داده و ترک پسر کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای من غلام همّت والای آن شهی</p></div>
<div class="m2"><p>کز ممکنات یکسره قطع نظر کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم خواهران و دخترکانش دهد اسیر</p></div>
<div class="m2"><p>هم کودکان خورد نشان قدر کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از نینوا، به کوفه و از کوفه تا به شام</p></div>
<div class="m2"><p>رأس بریده با حرم خود سفر کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برتر بود ز عرش علا خاک کربلا</p></div>
<div class="m2"><p>نازم به عشق او که به خاک این اثر کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهتر بود ز آب بقا خاک درگهش</p></div>
<div class="m2"><p>خضر نبی کجاست که خاکی به سر کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفتی که چهره سرخ «وفایی» کند ز عشق</p></div>
<div class="m2"><p>آری کند ولیک ز خون جگر کند</p></div></div>