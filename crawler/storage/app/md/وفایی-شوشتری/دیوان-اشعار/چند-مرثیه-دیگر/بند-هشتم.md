---
title: >-
    بند هشتم
---
# بند هشتم

<div class="b" id="bn1"><div class="m1"><p>چون شهسوار عشق به دست بلا رسید</p></div>
<div class="m2"><p>بر، وی ز دوست تهنیت و مرحبا رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد از نشاط هروله با یک جهان صفا</p></div>
<div class="m2"><p>از مروه ی وفا چو به کوی صفا رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تذکار عهد پیش و بلای الست شد</p></div>
<div class="m2"><p>آمد بشارتش که زمان وفا رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در ازل به جان تو خریدار ما شدی</p></div>
<div class="m2"><p>اکنون بیا که وقت ادای بها رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خود، به عهد ثابت و بر، وعده صادقیم</p></div>
<div class="m2"><p>با جان شتاب کن که زمان لقا رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبقت گرفته عشق تو چون بر، بدای ما</p></div>
<div class="m2"><p>جان ده به کام دل که نخواهد بدا رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما خود ترا فدایی و ما خود ترا، جزا</p></div>
<div class="m2"><p>سبحان من جزا که ز وی این جزا رسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشکفت غنچه ی دلش از شوق همچو گل</p></div>
<div class="m2"><p>از گلشن وفا چو به وی این ندا رسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قربانیی نمود که حیرانش صد خلیل</p></div>
<div class="m2"><p>چون آن خلیل کعبه ی دل در منی رسید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خون از زمین به جوش و به گردون شدی خروش</p></div>
<div class="m2"><p>بر روی خاک تیره چو خون خدا رسید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روح روان او چو روان گشت از بدن</p></div>
<div class="m2"><p>لال است زان زبان که بگوید کجا رسید</p></div></div>
<div class="b2" id="bn12"><p>دلهای اهلبیت در آن سرزمین شکست</p>
<p>چون کشتی نجات به دریای خون نشست</p></div>