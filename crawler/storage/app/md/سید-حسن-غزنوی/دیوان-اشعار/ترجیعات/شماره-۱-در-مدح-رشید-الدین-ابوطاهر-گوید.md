---
title: >-
    شمارهٔ ۱ - در مدح رشید الدین ابوطاهر گوید
---
# شمارهٔ ۱ - در مدح رشید الدین ابوطاهر گوید

<div class="b" id="bn1"><div class="m1"><p>در همه عالم یکی محرم نماند</p></div>
<div class="m2"><p>اینت بی یاری مگر عالم نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غصه چنان شد که تو بر تو جای</p></div>
<div class="m2"><p>گریه چونان شد که نم در نم نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بود جای غم و نادرتر آنک</p></div>
<div class="m2"><p>ماند غم بر جای و جای غم نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه گهی لب خنده می کرد یار</p></div>
<div class="m2"><p>بر من مسکین گری کانهم نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد هزاران حیرت از دیدار دوست</p></div>
<div class="m2"><p>راست خواهی بیش ماند و کم نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دل از جان برگرفتم بر حقم</p></div>
<div class="m2"><p>زانکه یک دم ماند و یک همدم نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون رشید الدین که بر خوردار باد</p></div>
<div class="m2"><p>یک وفادار از بنی آدم نماند</p></div></div>
<div class="b2" id="bn8"><p>آنکه چون ماه از کواکب ظاهر است</p>
<p>کنیتش بوطاهر و او طاهر است</p></div>
<div class="b" id="bn9"><div class="m1"><p>از دل و دلبر جدا افتاده ایم</p></div>
<div class="m2"><p>خود چنین تنها چرا افتاده ایم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>او گل و من بلبل و از یکدگر</p></div>
<div class="m2"><p>هر دو بی برگ و نوا افتاده ایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاکپای و سر برهنه مانده ایم</p></div>
<div class="m2"><p>زانکه غم خوار و ز پا افتاده ایم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود بجو نخرید ما را هیچ کس</p></div>
<div class="m2"><p>تا بدین حد کم بها افتاده ایم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همچو سایه بر زمین هرکس فتد</p></div>
<div class="m2"><p>ما چو ذره در هوا افتاده ایم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جای آن کز جای برخیزیم نیست</p></div>
<div class="m2"><p>در چنین عصری که ما افتاده ایم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کافران بر ما گواهی می دهند</p></div>
<div class="m2"><p>ای مسلمانان کجا افتاده ایم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دستگیر ما نصیرالدین بس است</p></div>
<div class="m2"><p>گرچه درپای بلا افتاده ایم</p></div></div>
<div class="b2" id="bn17"><p>آنکه چون ماه از کواکب ظاهر است</p>
<p>کنیتش بوطاهر و او طاهر است</p></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه رایش رنگ گوهر می دهد</p></div>
<div class="m2"><p>وانکه خلقش بوی عنبر می دهد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دولتش طاوس را دم می دهد</p></div>
<div class="m2"><p>همتش سیمرغ را پر می دهد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صورتش نادیده هم دل می برد</p></div>
<div class="m2"><p>خدمتش ناکرده هم بر می دهد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ماه را هر شب که بنهد مهرها</p></div>
<div class="m2"><p>رای چابک دست او خور می دهد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغ خورشید است عالی رای او</p></div>
<div class="m2"><p>هر کجا سر می زند زر می دهد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سحر کلکش بین که همچون خط یار</p></div>
<div class="m2"><p>تعبیه در مشک شکر می دهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خاک را از حزم پائی می کند</p></div>
<div class="m2"><p>باد را از عزم در سر میدهد</p></div></div>