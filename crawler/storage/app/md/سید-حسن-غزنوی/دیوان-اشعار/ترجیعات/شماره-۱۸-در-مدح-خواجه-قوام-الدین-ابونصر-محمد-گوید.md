---
title: >-
    شمارهٔ ۱۸ - در مدح خواجه قوام الدین ابونصر محمد گوید
---
# شمارهٔ ۱۸ - در مدح خواجه قوام الدین ابونصر محمد گوید

<div class="b" id="bn1"><div class="m1"><p>امروز یکی خوش سخنی نزد من آمد</p></div>
<div class="m2"><p>کز آمدنش جان دگر در بدن آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین سخنی بود چنان چست که گوئی</p></div>
<div class="m2"><p>خایید چو طوطی شکر و در سخن آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بد از آن دور که چون آن سخن او</p></div>
<div class="m2"><p>در گوش من شیفته ممتحن آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل بشکفتم ز طرب گرچه از این بیش</p></div>
<div class="m2"><p>چون لاله همی خون دلم در دهن آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که ازاین پس من و مدح تو ازیراک</p></div>
<div class="m2"><p>بلبل بسراید چو گل اندر چمن آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اول سخن این بود که این یار دل آرام</p></div>
<div class="m2"><p>بس تاخته و باخته از تاختن آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خواهم معشوق خود اینجاست دل آرام</p></div>
<div class="m2"><p>ور جویم ممدوح خداوند من آمد</p></div></div>
<div class="b2" id="bn8"><p>ممدوح و خداوند من و آن همه خلق</p>
<p>کز دیدن او گشت خرم جان همه خلق</p></div>
<div class="b" id="bn9"><div class="m1"><p>ای بر صفت یوسفیت حسن گواهی</p></div>
<div class="m2"><p>ماننده یوسف شده در غربت شاهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن تو ترا بی بخبری برده به تختی</p></div>
<div class="m2"><p>مهر تو مرا بی گنهی کرده به چاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از لعل تو یک خنده و از عقل جهانی</p></div>
<div class="m2"><p>از جزع تو یک ناوک و از صبر سپاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل را ز خط عارض تو آب و گیائی</p></div>
<div class="m2"><p>زان در خم زلف تو گرفت است پناهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عیبش نتوان کرد که ناید بر من هیچ</p></div>
<div class="m2"><p>آنجا نکند خیره نه آبی و گیاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گوباش همانجا و برش بیش نیاید</p></div>
<div class="m2"><p>از بندگی صدر اجل ماند به جاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من خود کیم از هستی و ز نیستی دل</p></div>
<div class="m2"><p>دل داند و زلف تو و من دانم و آهی</p></div></div>
<div class="b2" id="bn16"><p>رادی که چنو گنبد دوار ندارد</p>
<p>یاری که در این عالم خود یار ندارد</p></div>
<div class="b" id="bn17"><div class="m1"><p>امسال ز هجران تو ای خوش پسر من</p></div>
<div class="m2"><p>یارب چه عنا بود که آمد به سر من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خونم چو جگر بسته شد از درد و عجب آنک</p></div>
<div class="m2"><p>از دیده تو بگشادی خون جگر من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر زین سفرت دیرتر آوردی گردون</p></div>
<div class="m2"><p>بودی که ز گردون بگذشتی سفر من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در کار دلم زهره همانا نظری کرد</p></div>
<div class="m2"><p>کافتاد برآن روی چو زهره نظر من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>می گفت ترا خیز که نزد تو رسد یار</p></div>
<div class="m2"><p>المنة لله که عیان شد خبر من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیدی که بر انداخت بر فرقت تو آب</p></div>
<div class="m2"><p>از غایت سوز جگر این چشم تر من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خود بود دلم روشن از بهر خداوند</p></div>
<div class="m2"><p>کاندر شب غم وصل تو باشد سحر من</p></div></div>
<div class="b2" id="bn24"><p>ای دهر تو دانی که چنین صدر نداری</p>
<p>وی چرخ تو دانی که چنو بدر نداری</p></div>
<div class="b" id="bn25"><div class="m1"><p>امروز اگرم همچو فلک دیده هزار است</p></div>
<div class="m2"><p>پیش رخ چون ماه تو ای دوست بکار است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کز خرمی و تازگی تو دل و جانم</p></div>
<div class="m2"><p>با خرمی و تازگی باغ و بهار است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از مشک خطت وقتم چو عنبر سارا است</p></div>
<div class="m2"><p>وز سیم برت کارم چون زر عیار است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با من سخن تو همه از لابه و ریو است</p></div>
<div class="m2"><p>باتو سخن من همه از بوس و کنار است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باری رخ زرینم اشکسته چو سیم است</p></div>
<div class="m2"><p>باری دل پر خونم خندان چو انار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>می خواست که باطل کندم هجر به بیداد</p></div>
<div class="m2"><p>از عدل قوام الدین حقا که نیارست</p></div></div>
<div class="b2" id="bn31"><p>قطب فلک دولت ابو نصر محمد</p>
<p>آن بخت بدو بوده موافق چه دو فرقد</p></div>
<div class="b" id="bn32"><div class="m1"><p>صدری که چنان هرگز یک راد نباشد</p></div>
<div class="m2"><p>شادیش مبادا که بدو شاد نباشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فرعی ندهد عمر کزو مایه نگیرد</p></div>
<div class="m2"><p>اصلی نکند تیغ چو پولاد نباشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فریاد رس خلق جهان است و ز جودش</p></div>
<div class="m2"><p>یک گنج نبینی که به فریاد نباشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>برفرق اجل بأسش جز خاک نباشد</p></div>
<div class="m2"><p>در دست امل بی او جز یاد نباشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از نرگس زر دار و زبان آور سوسن</p></div>
<div class="m2"><p>در دست و دلش جز به مثل باد نباشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بی نعمت او نرگس تر چشم نگردد</p></div>
<div class="m2"><p>بی خدمت او سوسن آزاد نباشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>انصاف بده کوکن کو تهمتن روز نبرد او است</p></div>
<div class="m2"><p>مدح ارنه چنین گویم از داد نباشد</p></div></div>
<div class="b2" id="bn39"><p>آب کف او خاک برآورد زهر کان</p>
<p>لطف دم او آتش بنشاند ز ارکان</p></div>
<div class="b" id="bn40"><div class="m1"><p>ای ذات تو مر ذات خرد را ز روان به</p></div>
<div class="m2"><p>نزد تو یکی راحت خلق از دو جهان به</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از باد نهیبت چو دل خصم سبک شد</p></div>
<div class="m2"><p>از باده جودت سر امید گران به</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از خامه چون تیرت خصمت چو کمان شد</p></div>
<div class="m2"><p>هر کس که گنه دارد در دل چو کمان به</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زان تا همه درد خود از اقبال تو بیند</p></div>
<div class="m2"><p>همواره بداندیش تو با زحمت جان به</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دیوی که سر آتش فتنه همه او دید</p></div>
<div class="m2"><p>از پیش تو بیرون چه در ملک جهان به</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در سنگ نهان شد ز تو چون آتش الحق</p></div>
<div class="m2"><p>هر کس که بود خصم تو در سنگ نهان به</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر کوه یکی بد گهر از شرم نهان کرد</p></div>
<div class="m2"><p>آورد به تو صد گهر دیگر از آن به</p></div></div>
<div class="b2" id="bn47"><p>هر روز چو ماه نو جاه تو فزون باد</p>
<p>از چرخ کهن خصم تو چون چرخ نگون باد</p></div>
<div class="b" id="bn48"><div class="m1"><p>آنی که به تو دیده دولت نگران است</p></div>
<div class="m2"><p>ذات تو پسندیده سلطان جهان است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هستی تو جهان را به کفایت چو عطارد</p></div>
<div class="m2"><p>شاید که ترا مرکز معمور نشان است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کز سنبله مخصوص شرف گشت عطارد</p></div>
<div class="m2"><p>چون به نگری او را خود خانه همان است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای آنکه به تو صورت اقبال یقین است</p></div>
<div class="m2"><p>وز جود تو در هستی خود کان به گمان است</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>می خواست رهی تا که هواداری خود را</p></div>
<div class="m2"><p>تقریر کند خود یکی از صد نتوان است</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>با این همه بنگاشت در این شعر چو دیبا</p></div>
<div class="m2"><p>دیباجه آن مهر که در دست نهان است</p></div></div>
<div class="b2" id="bn54"><p>بر صفحه ملک از تو و رای تو نشان باد</p>
<p>بر روی مه چارده چندان که نشان است</p></div>