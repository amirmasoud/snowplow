---
title: >-
    شمارهٔ ۱۶ - در مدح نجیب الملک پسر وی گوید
---
# شمارهٔ ۱۶ - در مدح نجیب الملک پسر وی گوید

<div class="b" id="bn1"><div class="m1"><p>در ده می سوری که در سور گشادند</p></div>
<div class="m2"><p>درهای طربخانه معمور گشادند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش قدح لاله سرمست ببستند</p></div>
<div class="m2"><p>راه نظر نرگس مخمور گشادند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شعله می آینه ماه ببستند</p></div>
<div class="m2"><p>وز خنده گل بوسه گه حور گشادند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم طره شاخ از شکن آب ستادند</p></div>
<div class="m2"><p>هم چهره باغ از قلم نور گشادند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر خون دل آلاله یاقوتین آمد</p></div>
<div class="m2"><p>هر تیر زر اندود که از دور گشادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ناربصد پاره نماید دل عشاق</p></div>
<div class="m2"><p>چون روزه به خون دل انگور گشادند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابرو شکفه دست و دل خواجه بدیدند</p></div>
<div class="m2"><p>زان است که درج در منثور گشادند</p></div></div>
<div class="b2" id="bn8"><p>فرزانه حسین حسن احمد خاصه</p>
<p>آن کرده خدایش ز همه خلق خلاصه</p></div>
<div class="b" id="bn9"><div class="m1"><p>خیز ای دل و جان تا ز خرد گرد برآریم</p></div>
<div class="m2"><p>چون بلبل و گل روی سوی یکدگر آریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لبیک زنان کعبه دل کرده گلستان</p></div>
<div class="m2"><p>تا گنبد نیلوفری آواز برآریم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکبارگی از روزه خرد دست برآورد</p></div>
<div class="m2"><p>بازش به سجود قدح از پای درآریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عاشقی و مستی گم کرده سرو پای</p></div>
<div class="m2"><p>با خاک کف پای تو عمری به سر آریم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل تنگ و ضعیف است نگارا و تو داری</p></div>
<div class="m2"><p>آن شکر گلگون که از او گلشکر آریم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>معذور همی دار چو عید آمد و نوروز</p></div>
<div class="m2"><p>گربار گرانی برتو بیشتر آریم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عیدی بر تو ازلب دریای بزرگی</p></div>
<div class="m2"><p>کز همت آن دانی عقد گهر آریم</p></div></div>
<div class="b2" id="bn16"><p>فرزانه حسین حسن احمد خاصه</p>
<p>آن کرده خدایش ز همه خلق خلاصه</p></div>
<div class="b" id="bn17"><div class="m1"><p>ای نقش لطیف گل خندان سعادت</p></div>
<div class="m2"><p>خاک قدمت چشمه حیوان سعادت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم اختر انصافی در برج بزرگی</p></div>
<div class="m2"><p>هم گوهر اقبالی در کان سعادت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چشم تو گرامی بر دیدار کرام است</p></div>
<div class="m2"><p>گوش که بزد بر در دستان سعادت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از چشمه خورشید روان شد عرق شرم</p></div>
<div class="m2"><p>از بس که بباریدی باران سعادت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر دیده نهادت خرد و گفت ببیند</p></div>
<div class="m2"><p>نوباوه امید ز بستان سعادت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عیدی ز تو جز مائده روح نخواهیم</p></div>
<div class="m2"><p>امشب چو فتادیم به مهمان سعادت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پرسیدم از اقبال که شاید به چنین مدح</p></div>
<div class="m2"><p>گفت آنکه دل بخت شد و کان سعادت</p></div></div>
<div class="b2" id="bn24"><p>فرزانه حسین حسن احمد خاصه</p>
<p>آن کرده خدایش ز همه خلق خلاصه</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای گلبن جان سبزه گردونت چمن باد</p></div>
<div class="m2"><p>خاک در تو هم نفس مشک ختن باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون باد نفس بخش ولی را همه جانی</p></div>
<div class="m2"><p>چون خاک عدم جان حسودت همه تن باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قدر تو ملک وار چو گیرد ره گردون</p></div>
<div class="m2"><p>پیش سر او ابر هوا مقرعه زن باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فرخنده جمالت که گل دولت و دین است</p></div>
<div class="m2"><p>باغ نظر منتخب الملک حسن باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای عزم تو تیغی که درو هیچ سخن نیست</p></div>
<div class="m2"><p>رأی تو جهانگیرتر از تیغ سخن باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هر چند چو من چرخ نیاورد و نیارد</p></div>
<div class="m2"><p>در مرکز اقبال تو هر بنده چو من باد</p></div></div>