---
title: >-
    شمارهٔ ۹ - در مدح حسین بن حسن گوید
---
# شمارهٔ ۹ - در مدح حسین بن حسن گوید

<div class="b" id="bn1"><div class="m1"><p>لشکری یار من امروز سر آن دارد</p></div>
<div class="m2"><p>که دلم همچو سر زلف پریشان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لؤلؤ چشم مرا کرد به رنگ لاله</p></div>
<div class="m2"><p>آنکه از لاله و لؤلؤ لب دندان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سکندر ز سفر هیچ نمی آساید</p></div>
<div class="m2"><p>گر چه در چاه زنخ چشمه حیوان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در غریبی به همه حال بیابد ملکی</p></div>
<div class="m2"><p>که رخش معجزه یوسف کنعان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ترسا ببرد چون فکند در غربت</p></div>
<div class="m2"><p>که چو خورشید هم از نور نگهبان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزم رفتنش حقیقت شد سبحان الله</p></div>
<div class="m2"><p>که هنوز این دل سختی کش من جان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دلم درد خداوند نگردد که دلم</p></div>
<div class="m2"><p>شرف بندگی خاصه سلطان دارد</p></div></div>
<div class="b2" id="bn8"><p>آن به حق خواجه و مخدوم ولی نعمت من</p>
<p>آرزوی دل و اقبال حسین بن حسن</p></div>
<div class="b" id="bn9"><div class="m1"><p>رفتن یار سفر پیشه من تنگ افتاد</p></div>
<div class="m2"><p>با که گویم که میان من و دل جنگ افتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل مرا طیره رها کرد ولی باز گرفت</p></div>
<div class="m2"><p>چکنم دل چو دلارامم هم سنگ افتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این چه نقش است کزین نقش در آب چشمم</p></div>
<div class="m2"><p>همچو در آتش رخساره او رنگ افتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بودی از دیدنش آیینه چشمم روشن</p></div>
<div class="m2"><p>آه کایینه زنم در خطر زنگ افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس نبود آنکه چو قندیل همی سوخت دلم</p></div>
<div class="m2"><p>تا در آن قندیل از بخت قضا سنگ افتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان رخم زرد پر از گرد چو آبی است که او</p></div>
<div class="m2"><p>ده دل و کژ دل ماننده نارنگ افتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دولتش بادا همره که چو تاج الدوله</p></div>
<div class="m2"><p>ناگهانیش به سوی سفر آهنگ افتاد</p></div></div>
<div class="b2" id="bn16"><p>آن به حق خواجه و مخدوم ولی نعمت من</p>
<p>آرزوی دل و اقبال حسین بن حسن</p></div>
<div class="b" id="bn17"><div class="m1"><p>پسرا بو که مرا باز فراموش کنی</p></div>
<div class="m2"><p>وین دل تنگ چو آتشکده پر جوش کنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بازی روبه از آن چشم چو آهو چه دهی</p></div>
<div class="m2"><p>تا مرا خفته و بیدار چو خرگوش کنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حلقه حلقه است سر زلف تو آخر چه شود</p></div>
<div class="m2"><p>که بیاری تو از آن حلقه و در گوش کنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوش بر من زفراق تو جهان شد تاریک</p></div>
<div class="m2"><p>ترسم امروز که تاریک تر از دوش کنی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه گه ار دانی کز تو نشود چیزی کم</p></div>
<div class="m2"><p>یاد کن تلخی عیشم چو مئی نوش کنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بوفائی که نداده است خدایت هرگز</p></div>
<div class="m2"><p>که مبادم که مرا باز فراموش کنی</p></div></div>
<div class="b2" id="bn23"><p>آن به حق خواجه و مخدوم ولی نعمت من</p>
<p>آرزوی دل و اقبال حسین بن حسن</p></div>
<div class="b" id="bn24"><div class="m1"><p>زین جوان بخت جهان باز جوان خواهد شد</p></div>
<div class="m2"><p>هر چه در خاطر من گشت همان خواهد شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آز در سیری او رو به یقین خواهد گشت</p></div>
<div class="m2"><p>گنج درهستی او رو به گمان خواهد شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاخ در باغ معالیش ثمر خواهد داد</p></div>
<div class="m2"><p>آب در جوی معانیش روان خواهد شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرخ بوسنده پایش چو رکاب آمد زود</p></div>
<div class="m2"><p>ملک در طاعت دستش چو عنان خواهد شد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تیر گردون ز پی مدح سگالش چو قلم</p></div>
<div class="m2"><p>هم گشاده لب و هم بسته میان خواهد شد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سود عمرست مرا مدحش از آن کم نکنم</p></div>
<div class="m2"><p>پیش جانم که در این کار زیان خواهد شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قرة العین جلالست و در اوج جاهش</p></div>
<div class="m2"><p>دیده چرخ به حیرت نگران خواهد شد</p></div></div>
<div class="b2" id="bn31"><p>آن به حق خواجه و مخدوم ولی نعمت من</p>
<p>آرزوی دل و اقبال حسین بن حسن</p></div>
<div class="b" id="bn32"><div class="m1"><p>ای گل تازه که در ملک به بار آمده ای</p></div>
<div class="m2"><p>در دل خصمان بادت که چه خار آمده ای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پایدار است نکو خواه تو چون سرو از آنک</p></div>
<div class="m2"><p>وقت رادی همه کف همچو چنار آمده ای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آشنا بط بچه را هیچکسی ناموزد</p></div>
<div class="m2"><p>باز خردی نه عجب گر به شکار آمده ای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حق همی داند و سلطان جهان و صاحب</p></div>
<div class="m2"><p>که پسندیده و شایسته کار آمده ای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر زمین آئی و بر چرخ عطارد گوید</p></div>
<div class="m2"><p>که لطافت همه را در همه بار آمده ای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گل دلها ز تو گر شاد بخندید سزد</p></div>
<div class="m2"><p>کاندرین خلعت همرنگ بهار آمده ای</p></div></div>
<div class="b2" id="bn38"><p>آن به حق خواجه و مخدوم ولی نعمت من</p>
<p>آرزوی دل و اقبال حسین بن حسن</p></div>
<div class="b" id="bn39"><div class="m1"><p>دوستکامی که در آفاق چنان نیست منم</p></div>
<div class="m2"><p>زانکه پرورده مخدوم زمانه حسنم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بلبل نعمت فضلم چو علی وچه عجب</p></div>
<div class="m2"><p>که شکفته است گل خلق نبی در چمنم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>این کم از شعر عمادیست اگر با شش ماه</p></div>
<div class="m2"><p>برقم کلک عطارد بنگارد سخنم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ای گل باغ جمال و قمر چرخ جلال</p></div>
<div class="m2"><p>هست امیدم که ز تو گوی به میدان فکنم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زود چون بید بر اقبال تو حالی نکنم</p></div>
<div class="m2"><p>دیر چون سرو در ایام تو دستی نزنم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل چون بحرم بر تو ز همه گرم تر است</p></div>
<div class="m2"><p>که برین مدح تو در می نهند اندر دهنم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زانکه برده است لبم شهدی ازین بردی خوش</p></div>
<div class="m2"><p>بوسه می آرزوی آید ز لب خویشتنم</p></div></div>
<div class="b2" id="bn46"><p>آن به حق خواجه و مخدوم ولی نعمت من</p>
<p>آرزوی دل و اقبال حسین بن حسن</p></div>
<div class="b" id="bn47"><div class="m1"><p>سرو را طارم ازرق در و درگاه تو باد</p></div>
<div class="m2"><p>کمر جوزا شایان کمرگاه تو باد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ظل حق جلوه گر رای چو خورشید تو شد</p></div>
<div class="m2"><p>چرخ پیرانه سر این دولت پر ماه تو باد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>منبع جود و بزرگی کف بخشنده تست</p></div>
<div class="m2"><p>مطلع نور الهی دل آگاه تو باد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>فی المثل حاسدت ار چشمه خورشید بود</p></div>
<div class="m2"><p>راست چون سایه اسیر حرس جاه تو باد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای ز قحف سر دشمن شده تیغت پرآب</p></div>
<div class="m2"><p>طاس افلاک پر آواز عفاالله تو باد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو زحل مایه ادبار بد اندیش تو گشت</p></div>
<div class="m2"><p>مشتری دایه اقبال نکو خواه تو باد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هر کجا روی نهی چون فلک پیروزه</p></div>
<div class="m2"><p>نجم پیروزی در کوکبه همراه تو باد</p></div></div>