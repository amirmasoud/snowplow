---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>شاه جهان گذشت و ما همچنان خموش</p></div>
<div class="m2"><p>کو صد هزار نعره و کو صد هزار جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای تیغ بهر قبضه مسعود خون ببار</p></div>
<div class="m2"><p>وی کوس بهر رایت بوالفتح برخروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای سلطنت چو صبح بدر جامه تا به ناف</p></div>
<div class="m2"><p>وی مملکت چو شام ببر موی تا به گوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای سکه بی عیار بماندی در آن مپیچ</p></div>
<div class="m2"><p>وی خطبه از خطاب فتادی در آن مکوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تیر آسمان کمر چرخ برگشای</p></div>
<div class="m2"><p>وان ترکش مکوکب شه باز کن زدوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای تاج عقد ملک چو بگسست خاک خور</p></div>
<div class="m2"><p>وی تخت جام شاه چو بشکست زهر نوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای چتر کسوت سیه اکنون سپید گشت</p></div>
<div class="m2"><p>چون تیغ شه تو نیز کبودی طلب بپوش</p></div></div>
<div class="b2" id="bn8"><p>شاه فرشته سیرت مسعود درگذشت</p>
<p>همچون فرشته از سر افلاک برگذشت</p></div>
<div class="b" id="bn9"><div class="m1"><p>شاها مگر به عرصه میدان شتافتی</p></div>
<div class="m2"><p>یا از برای انس به بستان شتافتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا چون نظام دادی ملک عراق را</p></div>
<div class="m2"><p>بهر قرار ملک خراسان شتافتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست ستم ملوک جهان برگشاده اند</p></div>
<div class="m2"><p>ناگه مگر به بستن ایشان شتافتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می بایدت که گنج زمین را دهی به باد</p></div>
<div class="m2"><p>ای شاه زیر خاک مگر زان شتافتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای شیر مرد مطلق بر عادت قدیم</p></div>
<div class="m2"><p>مانا که سوی بیشه شیران شتافتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا بر نشاط گوی ربودن به مرغزار</p></div>
<div class="m2"><p>با قامت خمیده چو چوگان شتافتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نی نی بخواند ناگه سلطان محمدت</p></div>
<div class="m2"><p>هم در زمان به روضه رضوان شتافتی</p></div></div>
<div class="b2" id="bn16"><p>شاه فرشته سیرت مسعود در گذشت</p>
<p>همچون فرشته از سر افلاک برگذشت</p></div>
<div class="b" id="bn17"><div class="m1"><p>ای بوده خسروان را همچون پیامبری</p></div>
<div class="m2"><p>پرورده بندگان را همچون برادری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر دیده از وفات تو گریان چو چشمه ای</p></div>
<div class="m2"><p>هرسینه از فراق تو سوزان چو مجمری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از حسرت تو چیست جهان پای در گلی</p></div>
<div class="m2"><p>در ماتم تو کیست فلک خاک بر سری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دی از تو سور بود به هر جا و مجلسی</p></div>
<div class="m2"><p>و امروز ماتمی است بهر شهر و کشوری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گوهر اگر ز خاک برآرند ای عجب</p></div>
<div class="m2"><p>در خاک چون نهاد فلک چون تو گوهری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دردا که دهر لشکر عمر تو بر شکست</p></div>
<div class="m2"><p>ای بارها شکسته به یک حمله لشکری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>این طرفه کز وفات پسر شد پدر یتیم</p></div>
<div class="m2"><p>اندر فراق خسرو چون شاه سنجری</p></div></div>
<div class="b2" id="bn24"><p>شاه فرشته سیرت مسعود در گذشت</p>
<p>همچون فرشته از سر افلاک بر گذشت</p></div>
<div class="b" id="bn25"><div class="m1"><p>ای آفتاب رفتی و ماهی گذاشتی</p></div>
<div class="m2"><p>وی پادشه گذشتی و شاهی گذاشتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای نوش کرده زهر گیاهی ز باغ عمر</p></div>
<div class="m2"><p>الحق خجسته مهر گیاهی گذاشتی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای رفته همچو یوسف بر تخت مملکت</p></div>
<div class="m2"><p>اقبال را ز هجر به چاهی گذاشتی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رفتی و به هر شاه ملکشاه روز به</p></div>
<div class="m2"><p>الحق ستوده سنت و راهی گذاشتی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تابنده تر ز ماه شهی بر گماشتی</p></div>
<div class="m2"><p>افزون تر از ستاره سپاهی گذاشتی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وانگه چو رکن دولت و دین خاصبک برای</p></div>
<div class="m2"><p>بهر سپاه و شاه پناهی گذاشتی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر دعویی که چون تو نبودست هیچ شاه</p></div>
<div class="m2"><p>چون امت رسول گواهی گذاشتی</p></div></div>
<div class="b2" id="bn32"><p>شاه فرشته سیرت مسعود درگذشت</p>
<p>همچون فرشته از سر افلاک برگذشت</p></div>
<div class="b" id="bn33"><div class="m1"><p>شاه جهان ملک شه محمود را شناس</p></div>
<div class="m2"><p>صاحب قران ملکشه محمود را شناس</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سلطان غیاث دولت و دین جان پاک بود</p></div>
<div class="m2"><p>آرام جان ملکشه محمود را شناس</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاهان و خسروان همه کان بوده اند و پس</p></div>
<div class="m2"><p>یاقوت کان ملکشه محمود را شناس</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پیش از یقین ملکشه محمود را شمر</p></div>
<div class="m2"><p>بیش از گمان ملکشه محمود را شناس</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر کام و آرزو که سلاطین نتیجه اند</p></div>
<div class="m2"><p>در جمله آن ملکشه محمود را شناس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در ملک و عز و دولت و جاه ابد همی</p></div>
<div class="m2"><p>تو جاودان ملکشه محمود را شناس</p></div></div>