---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>در مجلس خسرو همایون اختر</p></div>
<div class="m2"><p>آن طرفگی لاله سر مست نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم خال و رخست و هم ندیم ساقی</p></div>
<div class="m2"><p>هم مجمر و عود هم گلاب و شکر</p></div></div>