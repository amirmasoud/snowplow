---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>آن رفت که عشوه می خریدم ز تو من</p></div>
<div class="m2"><p>پیراهن صبر می دریدم ز تو من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من با تو چنان بدم که ناخن باگوشت</p></div>
<div class="m2"><p>لیکن چو دراز شد بریدم ز تو من</p></div></div>