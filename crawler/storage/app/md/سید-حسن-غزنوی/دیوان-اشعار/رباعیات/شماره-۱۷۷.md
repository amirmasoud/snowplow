---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>زان جان که نداشت هیچ سودم تو بهی</p></div>
<div class="m2"><p>زان دل که فرو گذاشت زودم تو بهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وان دیده که نقش روی تو نمود</p></div>
<div class="m2"><p>دیدم همه را و آزمودم تو بهی</p></div></div>