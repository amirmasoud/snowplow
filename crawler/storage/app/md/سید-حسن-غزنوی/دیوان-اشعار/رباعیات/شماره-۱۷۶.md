---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>گر تو به خلاف دولت سلطانی</p></div>
<div class="m2"><p>در کنج عدم نهان شدن نتوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینک بنگر که سورریش کرد خلاف</p></div>
<div class="m2"><p>جان داد و نمی رهد ز سرگردانی</p></div></div>