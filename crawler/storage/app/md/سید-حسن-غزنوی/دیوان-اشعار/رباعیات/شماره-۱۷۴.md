---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>خواهم که همه کار برایت کنمی</p></div>
<div class="m2"><p>بر مردمک دو دیده جایت کنمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور هیچ مرا دست به جان در شودی</p></div>
<div class="m2"><p>حقا که نثار خاک پایت کنمی</p></div></div>