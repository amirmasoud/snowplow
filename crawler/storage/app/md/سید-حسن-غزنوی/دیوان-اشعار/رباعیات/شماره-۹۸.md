---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>مهمان من آمده است جانان امروز</p></div>
<div class="m2"><p>با عیش خوشم به روی مهمان امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر شب برسد به لب رسد جان امروز</p></div>
<div class="m2"><p>شب را برما بار مده هان امروز</p></div></div>