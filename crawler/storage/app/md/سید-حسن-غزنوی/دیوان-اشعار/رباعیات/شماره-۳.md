---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>از زلف تو دل برون گرفتم مطلب</p></div>
<div class="m2"><p>آسان تر صلح چون گرفتم مطلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هست امیدت که خوری آبی خوش</p></div>
<div class="m2"><p>رنج دل من که خون گرفتم مطلب</p></div></div>