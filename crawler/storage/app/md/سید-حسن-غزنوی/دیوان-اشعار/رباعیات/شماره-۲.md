---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای چشم من از نقش رخت دفتر آب</p></div>
<div class="m2"><p>آورده غمت راز دلم برسر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من سوخته ام تو آب داری آری</p></div>
<div class="m2"><p>جان را از فراتش بود و گل بر آب؟</p></div></div>