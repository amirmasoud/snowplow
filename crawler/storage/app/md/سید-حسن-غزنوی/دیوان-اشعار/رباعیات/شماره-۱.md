---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>خورشید همی سجده برد قد ترا</p></div>
<div class="m2"><p>در نتوان یافت حسن بیحد ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخرام بتا که سرو آزاد چمن</p></div>
<div class="m2"><p>ناگه خط بندگی دهد قد ترا</p></div></div>