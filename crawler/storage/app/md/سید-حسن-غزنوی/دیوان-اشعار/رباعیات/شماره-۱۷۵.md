---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>جانم ز تو از واقعه تو حالی</p></div>
<div class="m2"><p>آمد به لب و تو لب همی جنبانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بنده چنان زئی که چشمت نزنند</p></div>
<div class="m2"><p>خواهی که عنایتی کنی ولی نتوانی</p></div></div>