---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>این طایفه را چو خویشتن دانستند</p></div>
<div class="m2"><p>خون را می و راز را سخن دانستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تجربه چشم خردم باز گشاد</p></div>
<div class="m2"><p>جمله نه چنان بود که می دانستند</p></div></div>