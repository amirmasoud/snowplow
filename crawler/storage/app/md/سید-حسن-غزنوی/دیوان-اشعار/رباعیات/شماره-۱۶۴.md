---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>شاها چو نشاط بزم خرم گیری</p></div>
<div class="m2"><p>هر ماه که در جهان بود کم گیری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشگفت که یک طرف ز عالم گیری</p></div>
<div class="m2"><p>گر مرد توئی همه جهان هم گیری</p></div></div>