---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>شبها ز تو در سرم چه سوداست که نیست</p></div>
<div class="m2"><p>وز هجر تو بر من چه ستمهاست که نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوگند همی خوری که دل بسته تست</p></div>
<div class="m2"><p>سوگند چه حاجت است پیداست که نیست</p></div></div>