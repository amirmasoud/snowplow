---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>بر دست من ای شوخ می روشن نه</p></div>
<div class="m2"><p>وین بار گران هجر بر دشمن نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هشیار نیم تهمتش از من بردار</p></div>
<div class="m2"><p>دیوانه زنجیر توام بر من نه</p></div></div>