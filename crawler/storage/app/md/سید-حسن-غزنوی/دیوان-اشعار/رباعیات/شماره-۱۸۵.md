---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>دل در زر و سیم و دولت و بخت مبند</p></div>
<div class="m2"><p>ای نیست بر اینها گره سخت مبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان در خور راحت است در رنج مدار</p></div>
<div class="m2"><p>تن در خور تخته است در تخت مبند</p></div></div>