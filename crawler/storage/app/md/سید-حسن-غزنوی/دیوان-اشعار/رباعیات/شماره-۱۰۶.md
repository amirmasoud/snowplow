---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>شاهنشه را که بخت بادا وطنش</p></div>
<div class="m2"><p>شد نیک فراموش ز بنده حسنش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز منم بنده نیکو سخنش</p></div>
<div class="m2"><p>ای رحمت شاه یاد ندهی ز منش</p></div></div>