---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>ای طایفه را چو خویشتن دانستم</p></div>
<div class="m2"><p>خون را می و ژاژ را سخن دانستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تجربه چشم خردم باز گشاد</p></div>
<div class="m2"><p>جمله نه چنان بود که من دانستم</p></div></div>