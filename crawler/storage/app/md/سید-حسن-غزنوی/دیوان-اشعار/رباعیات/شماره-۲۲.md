---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>از رنگ رخ تو لاله پرتاب شده است</p></div>
<div class="m2"><p>نرگس ز پی چشم تو بی خواب شده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از رشک خطت بنفشه در تاب شده است</p></div>
<div class="m2"><p>وز آتش رخسار تو گل آب شده است</p></div></div>