---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>چون خواست حسن مدح شهنشاهی گفت</p></div>
<div class="m2"><p>سوسن سخنیش ازسر آگاهی گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دارم ده زبان و هستم خاموش</p></div>
<div class="m2"><p>تو بنده یک زبان ثنا خواهی گفت</p></div></div>