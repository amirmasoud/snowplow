---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>هر لحظه مرا به کام دشمن تو کنی</p></div>
<div class="m2"><p>جان در تن من خشک چو گردن تو کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هجر تو والله که نبارم جز خون</p></div>
<div class="m2"><p>من با تو کی آن کنم با من تو کنی</p></div></div>