---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>افسوس که آن جان جهانم بفروخت</p></div>
<div class="m2"><p>نخریده هنوز در زمانم بفروخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش که توان گفت که بی عیب و هنر</p></div>
<div class="m2"><p>ارزان بخرید و رایگانم بفروخت</p></div></div>