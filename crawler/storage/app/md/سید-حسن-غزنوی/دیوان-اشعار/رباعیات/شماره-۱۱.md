---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>کس را ز فراق نیست این غم که مراست</p></div>
<div class="m2"><p>این سوز دل و دو چشم پرنم که مراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وین کار نکو ز عشق درهم که مراست</p></div>
<div class="m2"><p>جز مایه درد نیست مرهم که مراست</p></div></div>