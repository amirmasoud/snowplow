---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>درویش ز دور تو توانگر گردد</p></div>
<div class="m2"><p>بی دل ز قبول تو دلاور گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر در کف تو به خاک ماند لیکن</p></div>
<div class="m2"><p>گر دست به خاک برنهی زر گردد</p></div></div>