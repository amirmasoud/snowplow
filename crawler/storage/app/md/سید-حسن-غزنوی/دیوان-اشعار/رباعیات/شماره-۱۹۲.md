---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>یک چند نهان سوی دل آرام شدیم</p></div>
<div class="m2"><p>و اکنون به عیان جفت می و جام شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسیدن ما همه ز بدنامی ماست</p></div>
<div class="m2"><p>اکنون ز چه ترسیم که بدنام شدیم</p></div></div>