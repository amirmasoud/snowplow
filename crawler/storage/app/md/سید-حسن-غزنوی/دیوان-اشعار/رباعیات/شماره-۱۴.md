---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>با دل گفتم که جان غمگین بی تست</p></div>
<div class="m2"><p>دریاب که بیچاره مسکین بی تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گفت که ما هر دو ز تو سیر دلیم</p></div>
<div class="m2"><p>چندین غم بیهوده مخور کین بی تست</p></div></div>