---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>ترسان نه از آنم که دلم خون گردد</p></div>
<div class="m2"><p>یا در طلب تو حال من چون گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان می ترسم که دل به زلفت باریست</p></div>
<div class="m2"><p>یک تار ز زلف تو دگرگون گردد</p></div></div>