---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>سلطان بهرام تا جهان می گیرد</p></div>
<div class="m2"><p>چون هست مراد او چنان می گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پیش گرفت یک جهان در دو زمان</p></div>
<div class="m2"><p>اکنون دو جهان به یک زمان می گیرد</p></div></div>