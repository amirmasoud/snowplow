---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>نقد دلم از غمت عیاری دارد</p></div>
<div class="m2"><p>جان بی تو بر اندوه تو کاری دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نکنی به نیک و بد یاد مرا</p></div>
<div class="m2"><p>بیچاره کسی که چون تو یاری دارد</p></div></div>