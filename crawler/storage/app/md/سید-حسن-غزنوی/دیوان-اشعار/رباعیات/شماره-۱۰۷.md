---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>ای دل به دل آنچه می کند یار بکش</p></div>
<div class="m2"><p>این بار چنانکه باید این بار بکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از روی گلت اگر همی ناید روی</p></div>
<div class="m2"><p>از دیده به دیده سر از خار بکش</p></div></div>