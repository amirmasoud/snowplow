---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>ای نرگس تر جهان معطر کردی</p></div>
<div class="m2"><p>وین بزم چو چرخ را پر اختر کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ خدایگان چو سر بر کردی</p></div>
<div class="m2"><p>مجلس پر در کلاه پر زر کردی</p></div></div>