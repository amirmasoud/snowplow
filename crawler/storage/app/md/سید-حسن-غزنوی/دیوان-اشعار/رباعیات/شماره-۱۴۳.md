---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>ای دل ز غمش ناله زاری می کن</p></div>
<div class="m2"><p>با درد به حیله روزگاری می کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان به لب رسیده این روزی چند</p></div>
<div class="m2"><p>هرگونه که هست دارو آری می کن</p></div></div>