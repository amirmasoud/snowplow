---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>کس جز تو مرا طرب فزا نیست مباد</p></div>
<div class="m2"><p>جز کوی توام خانه وفا نیست مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که به عافیت چرا بنشستی</p></div>
<div class="m2"><p>بی روی تو عافیت (مرا) نیست مباد</p></div></div>