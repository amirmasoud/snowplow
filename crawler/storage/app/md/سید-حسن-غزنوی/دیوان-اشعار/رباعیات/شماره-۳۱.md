---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>بی عارض چون سیم توام سنگی نیست</p></div>
<div class="m2"><p>زین آمدنم جز به تو آهنگی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر چه گلی که هیچ فرسنگی نیست</p></div>
<div class="m2"><p>کز بوی وصال تو در آن رنگی نیست</p></div></div>