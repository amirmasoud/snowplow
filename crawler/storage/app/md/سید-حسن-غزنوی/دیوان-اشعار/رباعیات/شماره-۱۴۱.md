---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>سبحان الله بعهد تو آن گویم</p></div>
<div class="m2"><p>کز طلعت تو نبود خالی کویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پیش جهان به روی من می دیدی</p></div>
<div class="m2"><p>و اکنون چه شدت که می نبینی رویم</p></div></div>