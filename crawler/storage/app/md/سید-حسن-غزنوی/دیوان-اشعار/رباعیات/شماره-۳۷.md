---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>دل را چو هوای آن دل آرای گرفت</p></div>
<div class="m2"><p>دست غمش آمد و مرا پای گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که اگر صبر کنم نیک آید</p></div>
<div class="m2"><p>تا نیک آید بد آمد و جای گرفت</p></div></div>