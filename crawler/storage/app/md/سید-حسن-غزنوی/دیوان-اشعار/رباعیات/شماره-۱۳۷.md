---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>رفتیم و گرانی ز وصالت بردیم</p></div>
<div class="m2"><p>در دیده نمونه جمالت بردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا مونس هر دو یادگاری باشد</p></div>
<div class="m2"><p>دل را به تو دادیم و خیالت بردیم</p></div></div>