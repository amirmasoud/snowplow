---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>ای دل قدح بلاش چون نوش بکش</p></div>
<div class="m2"><p>هو بد به امید روی نیکوش بکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون حلقه بندگیش داری در گوش</p></div>
<div class="m2"><p>او گم نکند تو پنبه از گوش بکش</p></div></div>