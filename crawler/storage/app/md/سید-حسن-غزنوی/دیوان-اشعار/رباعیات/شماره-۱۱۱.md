---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ای زلف تر از سوسن و گل مفروش</p></div>
<div class="m2"><p>وی سوسنت از آب و گلت از آتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ده که بر آن سوسن و گل آید خوش</p></div>
<div class="m2"><p>گلرنگ شراب از قدح سوسن وش</p></div></div>