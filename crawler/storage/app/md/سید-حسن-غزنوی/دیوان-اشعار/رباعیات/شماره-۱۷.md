---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>در رزمت و بزمت ای شه عدل پرست</p></div>
<div class="m2"><p>شش چیز ز شش چیز بنازد پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ از کف و رایت از صف و تیر از شست</p></div>
<div class="m2"><p>تاج از سرو تخت از قدم و جام از دست</p></div></div>