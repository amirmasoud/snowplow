---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>هرگز به وفا ز تو گمان نتوان برد</p></div>
<div class="m2"><p>جز جور و جفا از تو نشان نتوان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وصل دو روزه تو دل نتوان بست</p></div>
<div class="m2"><p>وز هجر همیشه تو جان نتوان کرد</p></div></div>