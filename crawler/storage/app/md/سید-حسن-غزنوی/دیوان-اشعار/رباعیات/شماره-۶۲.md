---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>شد عمر برون و آرزو برنامد</p></div>
<div class="m2"><p>شد روز فزون و یک غرض برنامد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردا که به غربیل خرد عالم را</p></div>
<div class="m2"><p>سرسر کردیم و هیچ بر سر نامد</p></div></div>