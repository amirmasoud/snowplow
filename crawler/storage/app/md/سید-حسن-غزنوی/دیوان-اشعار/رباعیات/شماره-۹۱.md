---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>دوش آن بت زرین کمر سیمین بر</p></div>
<div class="m2"><p>می کرد نیاز بر من از پسته شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رومی به چه تا زاد ترا یک مادر</p></div>
<div class="m2"><p>می داد مرا در آب خشک آتش تر</p></div></div>