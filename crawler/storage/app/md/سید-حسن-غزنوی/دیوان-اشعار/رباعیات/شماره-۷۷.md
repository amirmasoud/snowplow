---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>مه باتو تا درون پیرانه رود؟</p></div>
<div class="m2"><p>وین پایه جز از من بیمایه رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی از سر نیست کرد به حامی خسته؟</p></div>
<div class="m2"><p>خورشید چو شد ذره که در سایه رود</p></div></div>