---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>ای آنکه به تو دیده ظاهر نرسد</p></div>
<div class="m2"><p>وآنجا که غمت رسید خاطر نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند غم هجر تو بی پایان است</p></div>
<div class="m2"><p>آخر نه همانا که به آخر نرسد</p></div></div>