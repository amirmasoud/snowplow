---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>ای شاه زمین دور زمان بی تو مباد</p></div>
<div class="m2"><p>تا حشر سعود را قران بی تو مباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسایش جان ز تست جان بی تو مباد</p></div>
<div class="m2"><p>مقصود جهان توئی جهان بی تو مباد</p></div></div>