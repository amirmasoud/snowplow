---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>چون باز رهد ز غصه جان پاکم</p></div>
<div class="m2"><p>مگری چو زنان که بهر تو غمناکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خنگ سوار کرده افلاکم</p></div>
<div class="m2"><p>خاکش برسر که سر نهد بر خاکم</p></div></div>