---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>این دوست‌وَشان که دشمن جان منند</p></div>
<div class="m2"><p>در حسرت دانش فراوان منند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانند هم ایشان که نه مردان منند</p></div>
<div class="m2"><p>این ننگ زنان نه مرد میدان منند</p></div></div>