---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>هر سنگی را که آفتاب از تک و تاز</p></div>
<div class="m2"><p>پیروزه و لعل کرد در عمر دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بزم انداخت خسرو بنده نواز</p></div>
<div class="m2"><p>کز همچو منی چنین سزد سنگ انداز</p></div></div>