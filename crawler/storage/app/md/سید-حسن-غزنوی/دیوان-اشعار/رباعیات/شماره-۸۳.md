---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>در شیوه عشق صبر و زر می باید</p></div>
<div class="m2"><p>آب مژه و خون جگر می باید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نیل زدم مرقع صبر ولیک</p></div>
<div class="m2"><p>یک پیوندش ز عمر در می باید</p></div></div>