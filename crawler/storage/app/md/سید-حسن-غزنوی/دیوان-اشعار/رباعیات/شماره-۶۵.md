---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>هر شب که دلم به حیلها درماند</p></div>
<div class="m2"><p>خواهد که به فال و قرعه حالی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد قرعه سیمین سرشک از چشمم</p></div>
<div class="m2"><p>بر قرعه زرین رخم گرداند</p></div></div>