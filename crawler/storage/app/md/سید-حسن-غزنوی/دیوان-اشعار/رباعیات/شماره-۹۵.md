---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>غم گنج نهاده ای چه خواهم دیگر</p></div>
<div class="m2"><p>اشکم چو گشاده ای چه خواهم دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که چه بایدت بخواه و مندیش</p></div>
<div class="m2"><p>چون هر همه داده ای چه خواهم دیگر</p></div></div>