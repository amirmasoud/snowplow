---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>بگشای تو مشرق ای به حجت خسرو</p></div>
<div class="m2"><p>در مغرب فتح کن میندیش به جو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود بهر تو چرخ کهنه و دولت نو</p></div>
<div class="m2"><p>هر یک به دو دست می زند تیغ تو رو</p></div></div>