---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ای عزم تو بر دولت و دین آکنده</p></div>
<div class="m2"><p>وی زلزله بر جان ملوک افکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>والله که ز تو خصم نماند زنده</p></div>
<div class="m2"><p>صادق صبح است این زبان بنده</p></div></div>