---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ای نیست شده در غم تو هست همه</p></div>
<div class="m2"><p>هشیار به تو بوده دل مست همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غمهای جهان چو در دلم بنشینند</p></div>
<div class="m2"><p>جای تو بود جای زبر دست همه</p></div></div>