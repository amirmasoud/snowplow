---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>از درد دل پر غمم ای ماه به ترس</p></div>
<div class="m2"><p>وی خفته ز ناله سحرگاه به ترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر نفسی تعبیه دارم آهی</p></div>
<div class="m2"><p>ای آینه میگویمت از آه به ترس</p></div></div>