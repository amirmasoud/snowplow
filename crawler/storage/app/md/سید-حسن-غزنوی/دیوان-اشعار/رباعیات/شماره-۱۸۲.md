---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>شاها کرم تو راز کان بگشاده ست</p></div>
<div class="m2"><p>تیر تو گره ز آسمان بگشاده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این صفه در این گشادگی دانی چیست</p></div>
<div class="m2"><p>از دیدن تو دل جهان بگشاده ست</p></div></div>