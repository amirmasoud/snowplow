---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>تا از تف سینه قبله زردشتیم</p></div>
<div class="m2"><p>در عشق تو محراب هزار انگشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو فارغ و ما خویشتن از غم کشتیم</p></div>
<div class="m2"><p>سبحان الله چگونه پشتاپشتیم</p></div></div>