---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>حاشا که دلم کم تو گیرد هرگز</p></div>
<div class="m2"><p>یا بر دگری مهر پذیرد هرگز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ظن نبری ای به دو لب آب حیات</p></div>
<div class="m2"><p>کین آتش عشق تو بمیرد هرگز</p></div></div>