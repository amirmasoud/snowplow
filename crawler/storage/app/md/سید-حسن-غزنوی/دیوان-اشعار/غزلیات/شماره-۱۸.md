---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>دل در طراز حلقه زلفت در اوفتاد</p></div>
<div class="m2"><p>جان گر چه نقش بست ز دل برتر اوفتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دام طره تو که پر دانه دل است</p></div>
<div class="m2"><p>سیمرغ حسن تو چه عجایب در اوفتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خو گرفته بر رخ و رخساره تو دید</p></div>
<div class="m2"><p>مسکین پیاده بود دلش در بر اوفتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که بر من آید دردا که رایگان</p></div>
<div class="m2"><p>بیماری دو چشم تو بر عبهر اوفتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید گرد سایه تو ناشکافته</p></div>
<div class="m2"><p>هر ذره را هوای تو اندر سر اوفتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی نگر که طوطی جانم سوی لبش</p></div>
<div class="m2"><p>بر بوی پسته آمد و بر شکر اوفتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این هم بر این نسق که ز کلک نجیب دین</p></div>
<div class="m2"><p>گر چه شبه نمود همه گوهر اوفتاد</p></div></div>