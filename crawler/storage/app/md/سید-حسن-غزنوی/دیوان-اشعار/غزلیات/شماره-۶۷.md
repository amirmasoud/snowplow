---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>ما را رخ آن نگار بایستی</p></div>
<div class="m2"><p>آن شاهد روزگار بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند حدیث یار خود ناری</p></div>
<div class="m2"><p>اول باید که یار بایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دست و دلم ز روضه وصلش</p></div>
<div class="m2"><p>چون گل نرسید خار بایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آید بر من شکار دل وقتی</p></div>
<div class="m2"><p>بازش هوس شکار بایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بردی به عشوه بردی جان</p></div>
<div class="m2"><p>این عشوه خوش دو بار بایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند ز عشق درد دل خیزد</p></div>
<div class="m2"><p>این درد دلم هزار بایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهمار خوش است لیک پاینده</p></div>
<div class="m2"><p>چون دولت شهریار بایستی</p></div></div>