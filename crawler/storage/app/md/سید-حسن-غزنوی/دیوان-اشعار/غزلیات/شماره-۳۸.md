---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>برآن زلف و لب و خال و بنا گوش</p></div>
<div class="m2"><p>سوی بازار عشقش برده ام دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخش روز است و ابرو گوشه روز</p></div>
<div class="m2"><p>نهاده است از برای فتنه شب پوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دهانش چشمه نوش و زبر جد</p></div>
<div class="m2"><p>رمیده است از کران چشمه نوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه ساله ز سودای فراقش</p></div>
<div class="m2"><p>دلی دارم چو دیگ تفته پرجوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عشقش تا بر آرم من یکی دم</p></div>
<div class="m2"><p>سه بار از من ز رنج دل رود هوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو چون روی آزادی نبینم</p></div>
<div class="m2"><p>مرا خسرو خریدار است بفروش</p></div></div>