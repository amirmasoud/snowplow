---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>روزم ز هجر تو بصفت چون شب آمده است</p></div>
<div class="m2"><p>وینک چو شمع جانم از آن بر لب آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد نور مه ز چاه زنخدان تو پدید</p></div>
<div class="m2"><p>این چه نگر که رشک چه نخشب آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو مرکب شب زلف است و خوشتر آنک</p></div>
<div class="m2"><p>خورشید یک سواره برآن مرکب آمده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم من از جفای فراوانت ای نگار</p></div>
<div class="m2"><p>چند آخر از جفانه وفا را شب آمده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز حسن ز هجر مکن تیره همچو شب</p></div>
<div class="m2"><p>خود بی تکلف تو جهان را شب آمده است</p></div></div>