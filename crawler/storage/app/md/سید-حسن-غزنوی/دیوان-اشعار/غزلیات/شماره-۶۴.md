---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>ای همچو ماه پیشرو لشکر آمده</p></div>
<div class="m2"><p>وی از تو آفتاب امیدم بر آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریان و مستمند و پریشان برون شده</p></div>
<div class="m2"><p>خندان و شادمانه به آیین در آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خورشید بر تو ز اول بسیار سرزده</p></div>
<div class="m2"><p>وانگه ز شرم چهره تو برسر آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفته ز بحر پاکی چون آب پاره ای</p></div>
<div class="m2"><p>واکنون بسی عزیزتر از گوهر آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خشکی غم دو گلت علت تری</p></div>
<div class="m2"><p>چشم مرا چو چشمه نیلوفر آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر عزم گریه سوی دو فواره سرشک</p></div>
<div class="m2"><p>آتش ز چشمه جگرم هم بر آمده</p></div></div>