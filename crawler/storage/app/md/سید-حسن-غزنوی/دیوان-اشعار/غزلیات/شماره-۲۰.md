---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ایکه قدرت صد چو گردون آورد</p></div>
<div class="m2"><p>دور گردون چون توئی چون آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رأی تو رسمی که آرد در جهان</p></div>
<div class="m2"><p>همچو رویت خوب و میمون آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ و بوی خلق و خلقت آسمان</p></div>
<div class="m2"><p>گر نیارد ماه گردون آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش خشمت که بر کوه اوفتد</p></div>
<div class="m2"><p>همچو لاله سنگ را خون آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان ز جودت زر ناموزون برد</p></div>
<div class="m2"><p>چون نثارت در موزون آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم آرد سعی تو تشریف من</p></div>
<div class="m2"><p>کی گمان بردم که اکنون آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه بشکستم در این تشریف نیک</p></div>
<div class="m2"><p>زان شکستم نیز بیرون آورد</p></div></div>