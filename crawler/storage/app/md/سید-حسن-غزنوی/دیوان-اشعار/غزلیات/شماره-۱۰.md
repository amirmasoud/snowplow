---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ای سرور کرده پادشاهت</p></div>
<div class="m2"><p>اوج فلکست پایگاهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بخت به پیش تو میان بست</p></div>
<div class="m2"><p>سوی رفعت گشاد راهت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برحق باشد که سعد افلاک</p></div>
<div class="m2"><p>از چشم رضا کند نگاهت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق تو چو نام تو بود پس</p></div>
<div class="m2"><p>با حسن فعال تو گواهت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>والله که محمد بن منصور</p></div>
<div class="m2"><p>همچون پدرست نیک خواهت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با دولت نام داری وهش</p></div>
<div class="m2"><p>عالم نرسد بدستگاهت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون فضل خدای عرش گشتست</p></div>
<div class="m2"><p>ای صدر جهان همه پناهت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دامن حشر حافظت باد</p></div>
<div class="m2"><p>در حشمت بیکران الهت</p></div></div>