---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ای مونس جان من کجائی</p></div>
<div class="m2"><p>از دیده من چرا جدائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل دهدت که هر زمانی</p></div>
<div class="m2"><p>صد باره به نزد من نیائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دل شغب و دغا چه داری</p></div>
<div class="m2"><p>بی رحمت و بی وفا چرائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دور ببینمی پریشان</p></div>
<div class="m2"><p>دندان چه زنی و لب چه خائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود نیک نیامدت کزین سان</p></div>
<div class="m2"><p>آید ز تو بوی بی وفائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این ناز و تکبر تو تا چند</p></div>
<div class="m2"><p>بازار غم ترا روائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانم تو که از بزرگواری</p></div>
<div class="m2"><p>فرزند مهین پادشائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دولت شه تا جور که دارد</p></div>
<div class="m2"><p>میراث ز خوی مصطفائی</p></div></div>