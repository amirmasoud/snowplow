---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>ای باد روح پرور زنهار اگر توانی</p></div>
<div class="m2"><p>امشب لطافتی کن آنجا گذر که دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شو چو مهربانی در تیرگی زمانی</p></div>
<div class="m2"><p>یابی مگر نشانی زان آب زندگانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ره ره گذر بکویش دم دم نگر برویش</p></div>
<div class="m2"><p>خوش خوش مگر زمویش بوئی به من رسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او را بگوی کای مه ما را بپرس گه گه</p></div>
<div class="m2"><p>برجانم الله الله رحمت کن ار توانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نزد او رسیدی خاک درش بدیدی</p></div>
<div class="m2"><p>آنچه از حسن شنیدی شاید که باز رانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقا که زرد و زارم وز خود خبر ندارم</p></div>
<div class="m2"><p>بی تو همی گذارم عمری چنانکه دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطفی بکن نگارا وزنی بنه وفا را</p></div>
<div class="m2"><p>کاین بار باتو ما را کاری فتاد جانی</p></div></div>