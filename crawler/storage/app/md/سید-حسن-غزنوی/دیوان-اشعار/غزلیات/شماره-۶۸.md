---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چو جانم گرامی همی داشتی</p></div>
<div class="m2"><p>سرم را به گردون برافراشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی بزرگی چه واجب کند</p></div>
<div class="m2"><p>بیفکندن آن را که برداشتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کردم نگوئی کزینسان مرا</p></div>
<div class="m2"><p>میان جهان خوار بگذاشتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو تا کردی از مهر من دل تهی</p></div>
<div class="m2"><p>دلم را ز حسرت بینباشتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتار بد خواه بی سنگ من</p></div>
<div class="m2"><p>ز من روی یکباره برگاشتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبودی به دل آگه از راز من</p></div>
<div class="m2"><p>در و غم همه راست پنداشتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخم را بزر آب کردی رقم</p></div>
<div class="m2"><p>پس آنگه چو آیینه بنگاشتی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو تا زادی از مادر پاک تن</p></div>
<div class="m2"><p>همه تخم آزادگی کاشتی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چرا بازگشتی ز آیین خویش</p></div>
<div class="m2"><p>چرا بر رخم چشم نگماشتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نخواهمت هرگز مگر نیکوئی</p></div>
<div class="m2"><p>از آن پس که بد خواهم انگاشتی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مبیناد چشم حسن هیچ روز</p></div>
<div class="m2"><p>که با دشمنانت بود آشتی</p></div></div>