---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>جانا تو به دیگران چه مانی</p></div>
<div class="m2"><p>کاسایش جان یک جهانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون یوسف توتیای چشمی</p></div>
<div class="m2"><p>چون عیسی کیمیای جانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چند بود چو گل دو روئی</p></div>
<div class="m2"><p>چون سوسن چیست ده زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حسن چنان که آن توان گفت</p></div>
<div class="m2"><p>شکر ایزد را که آن چنانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکی شده ام چو سایه تو</p></div>
<div class="m2"><p>در پرده نور خود نهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را همه در میان نهادم</p></div>
<div class="m2"><p>زیرا که تو یار بی میانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان تو که جان من نبیند</p></div>
<div class="m2"><p>بی روی تو روی زندگانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اکنون که همه به یاد دادم</p></div>
<div class="m2"><p>در عشق تو آتش جوانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اندیشه چه سود گر تو آنرا</p></div>
<div class="m2"><p>بو می نهاد این زمانی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون غرقه شدم ز آب دیده</p></div>
<div class="m2"><p>هر دم چه بر آتشم نشانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آواره مکن ز خان و مانم</p></div>
<div class="m2"><p>گر هیچ مرید خاندانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر شب گویم به عشوه دل را</p></div>
<div class="m2"><p>رنجی مکش ای دل ارتوانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکبارگی از وصال آن بت</p></div>
<div class="m2"><p>نومید مشو به بدگمانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهدی میکن چنانکه آید</p></div>
<div class="m2"><p>باشد که نکو شود چه دانی</p></div></div>