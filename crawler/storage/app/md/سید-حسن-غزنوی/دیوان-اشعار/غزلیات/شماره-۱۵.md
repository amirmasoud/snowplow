---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ماهیست کز آن روی چو ماهت خبرم نیست</p></div>
<div class="m2"><p>وان چهره زیبای تو پیش نظرم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون گل در خاکم و چون شکر در آب</p></div>
<div class="m2"><p>زان غم که ز رخسار و لبت گل شکرم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گردون ظفرم هست ولیکن</p></div>
<div class="m2"><p>بر تو که دل و دیده و جانی ظفرم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زرین کمران پیشم هر چند بپایند</p></div>
<div class="m2"><p>چه سود که از دست تو سیمین کمرم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی که ترا باد گران هست خوش و نوش</p></div>
<div class="m2"><p>طعنه مزن ای دوست چو دانی اگرم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داند به کرم عهد و وفای تو که امروز</p></div>
<div class="m2"><p>جز آرزوی روی تو کار دگرم نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر تو چو دیده که مقیم است و مسافر</p></div>
<div class="m2"><p>ره میروم و گوئی عزم سفرم نیست</p></div></div>