---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ما را به همه عمر سلامی نکند دوست</p></div>
<div class="m2"><p>تمکین درودی و پیامی نکند دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آید بر ما گه گه از روی ترحم</p></div>
<div class="m2"><p>بنشیند و بسیار مقامی نکند دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد عشوه و صد نادره و بذله بگویم</p></div>
<div class="m2"><p>در پیش من آغاز کلامی نکند دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بسته میان خدمت او را و مرا هیچ</p></div>
<div class="m2"><p>یک روز گرامی چو غلامی نکند دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده ست مرا بنده و بس در عجبم من</p></div>
<div class="m2"><p>کین بنده مسکین را نامی نکند دوست</p></div></div>