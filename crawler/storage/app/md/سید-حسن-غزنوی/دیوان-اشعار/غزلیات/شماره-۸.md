---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>هین در دهید باده که هنگام بی غمی است</p></div>
<div class="m2"><p>زان باده که مشرق خورشید خرمی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا روی چون دو پیکر در روی او کشم</p></div>
<div class="m2"><p>زیرا که مان چو پروین وقت فراهمی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن پسته شکر گر او را چه کوچکی است</p></div>
<div class="m2"><p>وآن سنبل زره ور او را چه درهمی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن جزع بین که بر کف موسیش ساحریست</p></div>
<div class="m2"><p>وان لعل بین که بر لب عیسیش همدمی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آزادی از غمش سبب طوق بندگی است</p></div>
<div class="m2"><p>محرومی از لبش اثر یار محرمی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید زرد چهره محرور در غمش</p></div>
<div class="m2"><p>آن قرص روشنش چو دخان سایه محتمی است؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وین ماه زردگونه مرطوب را ز رشک</p></div>
<div class="m2"><p>همچون درم نشان فزونی هم از کمی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطف فرشته داری و چالاک سیرتی</p></div>
<div class="m2"><p>ما دیو مردمیم گر آن حور آدمی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زان شد حسن لطیف که وقتی بر او بتافت</p></div>
<div class="m2"><p>رائی که نور مردمک چشم مردمی است</p></div></div>