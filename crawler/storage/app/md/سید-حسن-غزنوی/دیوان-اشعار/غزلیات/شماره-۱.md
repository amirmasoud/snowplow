---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای تخت را خجسته تر از تاج گاه را</p></div>
<div class="m2"><p>وی ملک را فریضه تر از نور ماه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نقش بند دولت بند قبای تو</p></div>
<div class="m2"><p>فر همای داد به سربر کلاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی که بر نشینی تاج سفیده دم</p></div>
<div class="m2"><p>گیرد پناه سایه چتر سیاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخت تو چرخ باد که تاجی سپهر را</p></div>
<div class="m2"><p>روی تو لعل باد که پشتی سپاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوسن گواه دولت تست و خوش اینکه چرخ</p></div>
<div class="m2"><p>وه نی کره زرین بار و گواه را؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا روز حشر جز تو که هستی فرشته ای</p></div>
<div class="m2"><p>مردم مباد دیده این بارگاه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب پناه دولت و دینش تو کرده ای</p></div>
<div class="m2"><p>اندر پناه خویش بدار این پناه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چندانکه ممکن است نگهدار و عمر ده</p></div>
<div class="m2"><p>سلطان یمین دولت بهرام شاه را</p></div></div>