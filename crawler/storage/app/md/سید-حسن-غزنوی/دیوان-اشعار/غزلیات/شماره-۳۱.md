---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>دل چون ز لبت شراب خواهد</p></div>
<div class="m2"><p>درد از جگرم کباب خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درآب دو دیده غرقه گردد</p></div>
<div class="m2"><p>هر تشنه که از تو آب خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج تو درین دل خرابم</p></div>
<div class="m2"><p>خود گنج وطن خراب خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاشا که دل خطا پرستم</p></div>
<div class="m2"><p>یک کار مرا صواب خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چشم حسن حواله می کن</p></div>
<div class="m2"><p>هرکو ز تو در ناب خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذره ز وجود خویشتن گفت</p></div>
<div class="m2"><p>آن باشد کافتاب خواهد</p></div></div>