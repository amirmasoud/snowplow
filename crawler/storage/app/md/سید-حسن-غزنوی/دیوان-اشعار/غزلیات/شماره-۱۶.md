---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>در عشق تو ای جان که چو تو خوش صنمی نیست</p></div>
<div class="m2"><p>سرگشته دلی چون من و ثابت قدمی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند کم از یک نبود هرگز و چندم</p></div>
<div class="m2"><p>از عشق تو سرگرم اگر کم ز کمی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را همه شادی ز غم تست فزون باد</p></div>
<div class="m2"><p>اندی که غمت هست اگر هیچ غمی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان است شکفته گل رخسار چو داری</p></div>
<div class="m2"><p>صد بدره دینار و مرا خود درمی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون صورت لا زلف تو در هم شده بینم</p></div>
<div class="m2"><p>نبود عجب ار ما را زان لانعمی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را بنعم کردن کس خود چه نیاز است</p></div>
<div class="m2"><p>در دولت رادی که چنو محتشمی نیست</p></div></div>