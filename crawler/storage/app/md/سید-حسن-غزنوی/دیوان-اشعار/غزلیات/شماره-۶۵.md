---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ای آرزوی دیده بینا چگونه‌ای</p></div>
<div class="m2"><p>وی مونس دل (من) تنها چگونه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ناز و نازکی اگر اینجا نیامدی</p></div>
<div class="m2"><p>باری یکی بگوی که آنجا چگونه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در است صورت تو و دریاست چشم من</p></div>
<div class="m2"><p>ای در دور مانده ز دریا چگونه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل هدیه تو کردم آن را نخواستی</p></div>
<div class="m2"><p>جان تحفه می‌فرستم این را چگونه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نور چشم مهر و گل بوستان حسن</p></div>
<div class="m2"><p>ما بی‌تو در همیم تو بی‌ما چگونه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از وصل تو که نیست دریغا در آتشم</p></div>
<div class="m2"><p>در هجر من که هست مبادا چگونه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما خود جهان گرفتیم از پیش عاشقی</p></div>
<div class="m2"><p>در سلسله تو ای دل شیدا چگونه‌ای</p></div></div>