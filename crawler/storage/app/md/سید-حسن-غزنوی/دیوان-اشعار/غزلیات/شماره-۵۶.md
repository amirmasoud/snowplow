---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>ای حلقه زلفین تو دام گل و سوسن</p></div>
<div class="m2"><p>هستیم به بوی تو غلام گل و سوسن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین سان که تو در عشق دو روئی و دورائی</p></div>
<div class="m2"><p>خو پیش تو چون گویم نام گل و سوسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دولت رخسار و بنا گوش تو ای جان</p></div>
<div class="m2"><p>امروز جهانیست به کام گل و سوسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید همه شاهان بهرام شه آن شه</p></div>
<div class="m2"><p>کو خود خورد امروز نظام گل و سوسن؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جام لبالب کن و در ده که دراین وقت</p></div>
<div class="m2"><p>بی باده نمی زیبد جام گل و سوسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بنده حسن گفت مگر مدح شهنشه</p></div>
<div class="m2"><p>زان پر زر ودر شد همه کام گل و سوسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی سکه شاه آمد از آن خوار و خجل رفت</p></div>
<div class="m2"><p>زر زده و نقره خام گل و سوسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باداش بقا تا نفس باد گذارد</p></div>
<div class="m2"><p>بی زحمت آواز پیام گل و سوسن</p></div></div>