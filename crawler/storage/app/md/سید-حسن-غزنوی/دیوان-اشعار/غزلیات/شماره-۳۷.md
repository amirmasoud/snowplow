---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>سرو را ساکن به تخت آباد باش</p></div>
<div class="m2"><p>همچنین باقی بمانی شاد باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست تنگ دشمنان درخاک باد</p></div>
<div class="m2"><p>پای مرد دوستان چون باد باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون به طالع با کرم همشیره ای</p></div>
<div class="m2"><p>در طبیعت با وفا همزاد باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سحرگاهی هم اکنون بر دمد</p></div>
<div class="m2"><p>صبح اقبالت که افزون باد باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جهانی بنده خلقت چو گل</p></div>
<div class="m2"><p>همچو سوسن از جهان آزاد باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با رخ خوبان سفلی کامران</p></div>
<div class="m2"><p>بر دل پاکان علوی یاد باش</p></div></div>