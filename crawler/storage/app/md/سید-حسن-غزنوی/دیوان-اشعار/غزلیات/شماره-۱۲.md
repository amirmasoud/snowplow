---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>چه کنم قصه کز آن مایه غم بر تن چیست</p></div>
<div class="m2"><p>با که گویم که از آن سرو روان با من چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهد آتش ز دل آهن و حیران من از آنک</p></div>
<div class="m2"><p>حال بی آتش دل آن دل چون آهن چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوست مارا غم عشق آمد و دشمن دل سوخت</p></div>
<div class="m2"><p>تو چه دانی که از آن دوست برین دشمن چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نگشتند ز رخسار و لبش خوار و خجل</p></div>
<div class="m2"><p>رنگ گل زرد و سرافکندگی سوسن چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرشب از حال دل گمشده پرسم صد بار</p></div>
<div class="m2"><p>کای شب تیره از این حال ترا روشن چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق چون آمد و بگرفت گریبان دلم</p></div>
<div class="m2"><p>در چنین حال مرا برچِدن دامن چیست</p></div></div>