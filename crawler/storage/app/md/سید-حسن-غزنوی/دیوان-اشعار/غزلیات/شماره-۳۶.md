---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>هر آینه که دگر بایدم گزیدن یار</p></div>
<div class="m2"><p>چو یار من ز من و مهر من شود بیزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه غم خورم ز پی او که غم نخورد مرا</p></div>
<div class="m2"><p>ز چند گونه توان بر دلی نهادن بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه نرگس چشمست و گرچه مشکین زلف</p></div>
<div class="m2"><p>به قد چو سرو و برخ مه ولی به پنج و چهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو برگرفت دل از من چرا روم بر او</p></div>
<div class="m2"><p>نه من نیابم یار ار دگر گزیند یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دگر گزینم و یکسو نشینم از ره او</p></div>
<div class="m2"><p>تن عزیز و دل خویشتن ندارم خوار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته عهدا چندین جفا به من منما</p></div>
<div class="m2"><p>که مهرت اندک گشت و جفای تو بسیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا نگارا با تو زبان خلاف دل است</p></div>
<div class="m2"><p>خلاف گفتار آید مرا همی کردار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم همیشه هوای تو جوید ای بت روی</p></div>
<div class="m2"><p>وگرچه دیگر گوید زبان من گفتار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گمان مبر که دل از مهر تو بگردانم</p></div>
<div class="m2"><p>به نیک و بد صنما هیچ روی هیچ شمار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر وفا کنی ای ماه روی دارم چشم</p></div>
<div class="m2"><p>وگرنه باری از من وفا تو چشم مدار</p></div></div>