---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>ای همچو گل مطیع تو با برگ و بانوان</p></div>
<div class="m2"><p>وی همچو گل حسود تو بیرنگ و ناروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو سایه خدائی تا روز حشر باد</p></div>
<div class="m2"><p>در سایه همای سریر ترا مکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون ذره اند لشکر منصور بی عدد</p></div>
<div class="m2"><p>تو همچو آفتاب به حجت جهان ستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله جهان ز هم دل و دل باد هم نفس</p></div>
<div class="m2"><p>هر یک چو سرو هم سرو چون بید هم زبان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشای صحن مشرق و مغرب چو تیغ صبح</p></div>
<div class="m2"><p>منت خدای را که تو هستی سزای آن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمایه تو شاها کردار خوب تست</p></div>
<div class="m2"><p>چون مایه آن بود به خدا ار کنی زیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا مشتری بتابد بر بندگان بتاب</p></div>
<div class="m2"><p>تا آسمان بماند در مملکت بمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر مرتبت که عقل ترجی کند بیار</p></div>
<div class="m2"><p>هر آرزو که وهم تمنا برد بران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو آفتاب وش پسرانت چو اختراند</p></div>
<div class="m2"><p>تا حشر باد مر همه را در شرف قران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم چشم اختران شده روشن به آفتاب</p></div>
<div class="m2"><p>هم روز آفتاب مبارک به اختران</p></div></div>