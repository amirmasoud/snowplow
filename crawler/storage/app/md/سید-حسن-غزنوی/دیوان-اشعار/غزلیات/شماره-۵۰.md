---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>تن در بد و نیک یار دادیم</p></div>
<div class="m2"><p>دل در غم آن نگار دادیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاجی برسر ز خاکپایش</p></div>
<div class="m2"><p>بر تخت وفاش بار دادیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همرنگ تو سوی گل نشاندیم</p></div>
<div class="m2"><p>هم پای بدست خار دادیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا چند مگر که خویشتن را</p></div>
<div class="m2"><p>در عشق هزار بار دادیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درقبضه زینهار خواری</p></div>
<div class="m2"><p>سرمایه به زینهار دادیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرمود که بی قرار می باش</p></div>
<div class="m2"><p>ما نیز براین قرار دادیم</p></div></div>