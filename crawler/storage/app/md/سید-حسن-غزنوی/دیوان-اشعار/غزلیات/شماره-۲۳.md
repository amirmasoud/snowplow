---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>از لعل آبدار تو پاسخ همی رسد</p></div>
<div class="m2"><p>وز زلف تابدار تو دل را دمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرورده شد ز خون دلم سالها غمت</p></div>
<div class="m2"><p>در انتظار آنکه مگر محرمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیکن به دولت شه شادان شود به حکم</p></div>
<div class="m2"><p>هر گه که بندگان را بر دل غمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهرام شاه آنکه به اقبال و نصرتش</p></div>
<div class="m2"><p>هر روز ذکر فتحی از عالمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوریست مخلصان را از تیغ او کز آن</p></div>
<div class="m2"><p>هر لحظه دشمنان را نو ماتمی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکر ز شاه گیتی نومیدی ز بخت؟</p></div>
<div class="m2"><p>بگذار ای زمانه اگر همدمی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشریفهای بنده حسن برقرار خویش</p></div>
<div class="m2"><p>تقریر کرد شاه ولیکن نمی رسد</p></div></div>