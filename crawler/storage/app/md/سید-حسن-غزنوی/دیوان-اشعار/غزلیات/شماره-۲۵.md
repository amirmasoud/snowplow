---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>روح ز تو خوبتر به خواب نبیند</p></div>
<div class="m2"><p>چشم فلک چون تو آفتاب نبیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنه آب حیات چشمه نوشت</p></div>
<div class="m2"><p>غرقه به نوعی شود که آب نبیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق تو در دل نشست و خاست نخواهد</p></div>
<div class="m2"><p>تا وطن خویش را خراب نبیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانکه چکد لؤلؤ خوشاب ز چشمم</p></div>
<div class="m2"><p>چشم تو در لؤلؤ خوشاب نبیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سینه همی درد را به درد نداند</p></div>
<div class="m2"><p>دیده همی خواب را به خواب نبیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست عجب با گشادنامه خطت</p></div>
<div class="m2"><p>کز گره نافه مشک ناب نبیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیهده باشد سؤال بوسه حسن را</p></div>
<div class="m2"><p>بر لب او چون ره جواب نبیند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوی نکوی تو رأی وصل کند لیک</p></div>
<div class="m2"><p>بخت بد مات هم به خواب نبیند</p></div></div>