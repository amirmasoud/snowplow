---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>صنما بسته آنم که در این منزل تست</p></div>
<div class="m2"><p>خبری یابم زان زلف شکسته به درست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد و غمهای تو و عهد وفایت بر ماست</p></div>
<div class="m2"><p>هم به جان تو که هوش و دل و جانم بر تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من نیست شد و سوز تو از سینه نرفت</p></div>
<div class="m2"><p>اشک من موج زد و نقش تو از دیده نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زشت نقشی بود ار جسم تو نشناسم خوب</p></div>
<div class="m2"><p>سخت کاری بود ارکار تو بگذارم سست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه در دیده من نقش خیال تو بماند</p></div>
<div class="m2"><p>ورچه برسینه من عکس جمال تو برست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش در دیده و در سینه نمی جویم از آنک</p></div>
<div class="m2"><p>دوش در آتش دورانت نمی دانم جست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر آن سرو بگردم که چو تو باشد راست</p></div>
<div class="m2"><p>پیش آن ماه بمیرم که به تو ماند چست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمدم کاسته و سوخته اندر بر تو</p></div>
<div class="m2"><p>که مرا روی چو بستان تو قبله است نه بست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای مرا کاسته چون ماه بیفزای اول</p></div>
<div class="m2"><p>وی مرا سوخته چون شمع بیفروز نخست</p></div></div>