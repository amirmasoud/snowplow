---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>هوای وصل جانانم گرفتست</p></div>
<div class="m2"><p>غم دلبر دل و جانم گرفتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دلبر نمی بینم چه دانی</p></div>
<div class="m2"><p>که چون سودای ایشانم گرفتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چگونه در کشم دامن ز عشقش</p></div>
<div class="m2"><p>که دست دل گریبانم گرفتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه گل چنم از باغ وصلش</p></div>
<div class="m2"><p>که از خارش گلستانم گرفتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل از صبر ارچه می گریم نه ننگست</p></div>
<div class="m2"><p>ره وصل ارچه می دانم گرفتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازینم پاره نومیدی آمد</p></div>
<div class="m2"><p>که در دشواری آسانم گرفتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر پیدا نمی آمد غمش زانک</p></div>
<div class="m2"><p>نشاط مدح سلطانم گرفتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک بهرام شاه آن گنج رحمت</p></div>
<div class="m2"><p>که در دشواری آسانم گرفتست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بحمدالله که شکر نعمت او</p></div>
<div class="m2"><p>همه پیدا و پنهانم گرفتست</p></div></div>