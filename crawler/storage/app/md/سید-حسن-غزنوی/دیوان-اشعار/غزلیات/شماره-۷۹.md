---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>ایکه دل را دل و جان را جانی</p></div>
<div class="m2"><p>وز دل و جان چه نکوتر آنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو دل در بر من عاریتی</p></div>
<div class="m2"><p>بی تو جان در تن من زندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل فدای تو که بس دلجوئی</p></div>
<div class="m2"><p>جان نثار تو که خوش جانانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه در دل چو جهان می دانی</p></div>
<div class="m2"><p>آه کز دیده چو جان پنهانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق زار توام می بینی</p></div>
<div class="m2"><p>بنده خاص شهم می دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه آتش دل من چکنم</p></div>
<div class="m2"><p>خود تو چون آب فرو می خوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در میان دلی و این چه عجب</p></div>
<div class="m2"><p>وطن گنج بود ویرانی</p></div></div>