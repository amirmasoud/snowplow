---
title: >-
    شمارهٔ ۴۳ - در راه مکه در مدح امیر فخرالدین گوید
---
# شمارهٔ ۴۳ - در راه مکه در مدح امیر فخرالدین گوید

<div class="b" id="bn1"><div class="m1"><p>من به راه مکه آن دیدم ز فخر روزگار</p></div>
<div class="m2"><p>کز پیمبر دید در راه مدینه یار غار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم یکی از معجزات ظاهر پیغمبر است</p></div>
<div class="m2"><p>این کرامتهای گوناگون فخر روزگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز آدینه که از بغداد پی بیرون نهاد</p></div>
<div class="m2"><p>ابر گوهر کرد برفرق غلامانش نثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شد زمین بادیه همچون بهشتی در بهشت</p></div>
<div class="m2"><p>وان هوای هاویه همچون بهار اندر بهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از میان خارهای دیده دوز جامه در</p></div>
<div class="m2"><p>صد هزاران گل پدید آمد به فضل کردگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سموم بادیه گلبوی گشت آگه شدم</p></div>
<div class="m2"><p>کاتش سوزان بر ابراهیم شد چون مرغزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هوای سرد بایستی بگرد موکبش</p></div>
<div class="m2"><p>گرم سقائی گرفتی ابر مروارید بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چارسو باران و فارغ در میان آن قافله</p></div>
<div class="m2"><p>همچو نیلوفر میانه خشک و گوشه آبدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این کرامت حق تعالی داند و خلقان که بود</p></div>
<div class="m2"><p>از حوالینا الهی لا علینا یادگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تابکوفه باز مانده حالش همچنین</p></div>
<div class="m2"><p>هم عجب بودی اگر یک بار بودی یا دو بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>استوارم گر نداری بر حقی کین حالها</p></div>
<div class="m2"><p>من به چشم خویش دیدم می ندارم استوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون امیر المومنین بوبکر در راه خدای</p></div>
<div class="m2"><p>همتش درباخت دیناری چهل پنجه هزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون سلیمان طاعتش بردند دیوان عرب</p></div>
<div class="m2"><p>آن خران بی فسار و اشتران بی مهار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همچو سگ بسیار گوی و همچو خر اندک خرد</p></div>
<div class="m2"><p>همچو بز بازیچه روی و همچو دد مردار خوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لشکر جرار با خود برده و با این همه</p></div>
<div class="m2"><p>داد هر یک را نهانی جامهای زرنگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون عرب خلعت به پوشیدی همی اندر سزد</p></div>
<div class="m2"><p>شهپر طاوس را افکند بر دنبال مار؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آخر این روی پری چون گستری بر پشت دیو</p></div>
<div class="m2"><p>آخر این برگ سمن تا کی نهی بر روی خار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرنه آن بودی که ره بر قافله بسته شدی</p></div>
<div class="m2"><p>تیغ خون خوارش از آن ماران برآوردی دمار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بندگان نیک مرد و شیر مرد مخلصش</p></div>
<div class="m2"><p>وان همه زهره چو آب و آن همه دل چون انار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن همه شیران شرزه و آن همه پیلان مست</p></div>
<div class="m2"><p>وان همه گردان گردون آن همه مردان کار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صد چو سایه از پس و صد چون نظر بازان ز پیش</p></div>
<div class="m2"><p>صد چو قوت بر یمین و صد چو قدرت بر یسار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در میان خود گرفته حاجیان را همچنانک</p></div>
<div class="m2"><p>عاشقان گیرند معشوقان خود را در کنار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قدر این راحت که تا این حال با او دیده ام</p></div>
<div class="m2"><p>آن کسی داند که او دیده و کشیده رنج پار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کار حق اینک چنین سازد امیری کش بود</p></div>
<div class="m2"><p>چون رشید الدین وزیر کاردان حق گزار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بر دیانت پای ثابت برنهاده همچو سرو</p></div>
<div class="m2"><p>وز سخاوت دست کوته برگشاده چون چنار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یارب این زیبا و زیر نیک پی بس مقبل است</p></div>
<div class="m2"><p>هم بدین مقبل امیر عادل ارزانیش دار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عالم عادل مجاهد فخر دین میر عراق</p></div>
<div class="m2"><p>سهم دولت بوالوفا مسعود امیر کامیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خلق خلق مشکبوی عنبرین رنگش نگر</p></div>
<div class="m2"><p>کش فدازیبد معنبر زلف و مشکین خال یار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طوق تاریکی شد از رأی سکندر روشنم؟</p></div>
<div class="m2"><p>چون شد از خاک حبش آب حیاتش آشکار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>طالع و نام و نشانش هر سه مسعود آمده است</p></div>
<div class="m2"><p>جز به شب داده است هرکز طالع مسعود بار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چشم اقبال و دل بخت تو باشد روز و شب</p></div>
<div class="m2"><p>از سواد و از سویدا چشم و دل را افتخار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شحنه بغداد می بایست بودش تا ابد</p></div>
<div class="m2"><p>در ازل زان خلعت عباس دادش کردگار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرو را حقیست ثابت گشت بر پایش بزرگ؟</p></div>
<div class="m2"><p>خرده نبود کز سرآن بگذرد دیوانه وار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در پناه جاه و ظل عدل و حرز دولتش</p></div>
<div class="m2"><p>آرزوی عمر ما ایزد نهاد اندر کنار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خواجگان حضرت غزنی که اهل سنت اند</p></div>
<div class="m2"><p>چون فریضه این دعا گویند روزی پنج بار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن دعا را بسته بر پر چون کبوتر می شدند</p></div>
<div class="m2"><p>دوش طاوسان فردوسی سوی دارالقرار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>با عروس طبع گفتم هین دمی نشاط باش</p></div>
<div class="m2"><p>شهپر هر یک مرصع کن بدر شاهوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مدح گفتم اینکه من بحر علومم در نگر</p></div>
<div class="m2"><p>دونم ار گویم که تو بحر سخائی زر بیار</p></div></div>