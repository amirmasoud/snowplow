---
title: >-
    شمارهٔ ۱۰ - این قصیده نیز در مدح بهرام شاه است
---
# شمارهٔ ۱۰ - این قصیده نیز در مدح بهرام شاه است

<div class="b" id="bn1"><div class="m1"><p>خاک را از باد بوی مهربانی آمد است</p></div>
<div class="m2"><p>در ده آن آتش که آب زندگانی آمد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نرگس خوشبوی مخمور طبیعی خاستست</p></div>
<div class="m2"><p>بید خرم روی سر مست جوانی آمد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ مهمان دوست برگ میزبانی ساخته است</p></div>
<div class="m2"><p>مرغ اندک زاد در بسیاردانی آمده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد نقاشی است یا عصار کو هر صبحدم</p></div>
<div class="m2"><p>این توانائیش بین کز ناتوانی آمد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش لاله چرا افزود آب چشم ابر</p></div>
<div class="m2"><p>آب را گر خاصیت آتش نشانی آمد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آری آری هم بر این طبع است تیغ شهریار</p></div>
<div class="m2"><p>گرچه او آب است از آتش نشانی آمد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبزه گر پذرفت شکل تیغ تیزش لاجرم</p></div>
<div class="m2"><p>همچو تیغ تیز در عالم ستانی آمد است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاف پیری زد شکوفه پیش رای صائبش</p></div>
<div class="m2"><p>لاجرم عمرش چنان کوته که دانی آمد است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا عروس ملک شاه از چشم بد ایمن شود</p></div>
<div class="m2"><p>چشم خواب نرگس اندر دیده بانی آمد است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش تخت شاه چون من طوطی شکرفشان</p></div>
<div class="m2"><p>بلبلم خوشتر که او در مدح خوانی آمد است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>راست خواهی هر کجا گل نافه ای از لب گشاد</p></div>
<div class="m2"><p>همچو غنچه لاله را پسته دهانی آمد است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل گرفته جام یاقوتین بدست زمردین</p></div>
<div class="m2"><p>پیش شاهنشه به بوی دوستکانی آمد است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرو نازان بین که گوئی این جهان لعبتی</p></div>
<div class="m2"><p>پیش سلطان در قبای آن جهانی آمد است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسرو اعظم خداوند جهان بهرام آنک</p></div>
<div class="m2"><p>رسم او جان بخشی و عالم ستانی آمد است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسمان پیش جمال او زمین گردد از آنک</p></div>
<div class="m2"><p>از جمال او زمین در آسمانی آمد است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کلک عقل از تیر او عالم گشائی یافتست</p></div>
<div class="m2"><p>تیر چرخ از کلک او در ترجمانی آمد است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خه خه ای شاهی که از بس بخشش و بخشایشت</p></div>
<div class="m2"><p>خرس در رادی و گرگ اندر شبانی آمد است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون بداد و دین صفت کردم ترا اقبال گفت</p></div>
<div class="m2"><p>گر چنین باشد نیایم چون چنانی آمد است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیل این صحرای اول با جلاجلهای نور</p></div>
<div class="m2"><p>گرد ملکت برطریق پاسبانی آمد است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صدر دیوان دوم تیرست تا یابد معین</p></div>
<div class="m2"><p>با خجسته کلک تو در همزبانی آمد است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مطرب صحن سوم در بزم تو عشرت پذیر</p></div>
<div class="m2"><p>زین غمین تر داشت اندر شادمانی آمد است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاه اقلیم چهارم تا فرستد هم خراج</p></div>
<div class="m2"><p>در فراهم کردن زرهای کانی آمد است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شحنه میدان پنجم تا سلحدار تو شد</p></div>
<div class="m2"><p>زخم او بر خصم جای گمانی آمد است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قاضی صدر ششم را طالع مسعود تو</p></div>
<div class="m2"><p>مقتدای فتوی صاحبقرانی آمد است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای که میر صفه هفتم سبک دل شد ز رشک</p></div>
<div class="m2"><p>کز وقار تو برو چندان گرانی آمد است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زاویه داران هشتم را به نور راستی</p></div>
<div class="m2"><p>رای عالی قدرت اندر میزبانی آمد است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای ضمیرت دیدبان کنگر طاقی که هست</p></div>
<div class="m2"><p>آفرینش را مکان در بی مکانی آمد است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از در دولت سبک بر بام همت رو که چرخ</p></div>
<div class="m2"><p>با چنین نه پایه بهر نردبانی آمد است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خسروا طبعم به اقبال قبولت زنده شد</p></div>
<div class="m2"><p>آب را آری حیات اندر روانی آمد است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنده را بختیست در هر فن ز شعر فارسی</p></div>
<div class="m2"><p>چشم زخمش را چو خاری گلستانی آمد است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لیک حرص بندگی و آرزوی مدح تو</p></div>
<div class="m2"><p>موجب این بیتهای امتحانی آمد است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون تو در هر کار سلطانی و خاصه در سخن</p></div>
<div class="m2"><p>من چه گویم کاین بدیهه چندگانی آمد است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اینک از اقبال تو پردخته شد آن خدمتی</p></div>
<div class="m2"><p>کاندکی الفاظ و بسیارش معانی آمد است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در او در آب قدرت آشنا ور آن چنانک</p></div>
<div class="m2"><p>راست گوئی گوهر تیغ یمانی آمد است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گرم بگشادم فقاعی بر سر خوان ثنات</p></div>
<div class="m2"><p>گرچه شیرین نیست باری ناردانی آمد است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا نهال عمر خلقان را به بستان حیات</p></div>
<div class="m2"><p>بیخ و شاخ از صحت و از کامرانی آمدست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاخ زن بادا نهال عمر تو زیرا که خود</p></div>
<div class="m2"><p>بیخش از بستان سرای جاودانی آمد است</p></div></div>