---
title: >-
    شمارهٔ ۶۲ - در صفت هندوستان و مدح سلطان بهرام شاه گوید
---
# شمارهٔ ۶۲ - در صفت هندوستان و مدح سلطان بهرام شاه گوید

<div class="b" id="bn1"><div class="m1"><p>می بنازد باز گوئی خطه هندوستان</p></div>
<div class="m2"><p>شکر حق گوید همی بسیار و هستش جای آن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم حریمش روشنائی می دهد بر آفتاب</p></div>
<div class="m2"><p>هم زمینش سرفرازی می کند بر آسمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب و آسمان در سایه اویند از آنک</p></div>
<div class="m2"><p>سایه گستردست بروی رایت شاه جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کدخدای شرق و غرب و پیشوای ملک و دین</p></div>
<div class="m2"><p>شهریار تاج و تخت و پادشاه انس و جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عادل عادل تبار و غازی غازی نسب</p></div>
<div class="m2"><p>مرکز مرکز ثبات و خسرو خسرو نشان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب دین و دولت ظل حق بهرام شاه</p></div>
<div class="m2"><p>آنکه چون او یک فرشته آدمی ندهد نشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رزم او نار مهین و بزم او ماء معین</p></div>
<div class="m2"><p>حزم او خاک متین و عزم او باد روان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برده مداحش چو سوسن زر و ناخوانده مدیح</p></div>
<div class="m2"><p>گشته خصمش چون شکوفه پیر و نابوده جوان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرک را بشکست پا تا خنگ او برداشت گام</p></div>
<div class="m2"><p>آز را پر شد شکم تا جود او بنهاد خوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ز شرم نطق او شد معتکف در قعر بحر</p></div>
<div class="m2"><p>زر ز بیم جود او شد منزوی در جوف کان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نگین و افسرت را جرم زهره واسطه</p></div>
<div class="m2"><p>وی همای همتت را شاخ سدره آشیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می فتند از پر تیرت بر زمین شیران نگون</p></div>
<div class="m2"><p>می پرند از فر عدلت بر هوا مرغان ستان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوه حلمی ورنه پس چون است کز جودت همی</p></div>
<div class="m2"><p>چون دهان درج پر لؤلؤ کنی درج دهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خسروا هر که مبارک تر بود از آب روی</p></div>
<div class="m2"><p>یمن دیوارت گرامی تر بسی از خان و مان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هیچ مقبل گرد خانه کی شود چون عنکبوت</p></div>
<div class="m2"><p>تا سلیمان چون توئی هستش به دولت میزبان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برخور از شاهی که امروز از فراوان خلعتت</p></div>
<div class="m2"><p>نوبهار هفت رنگ آمد پدید اندر خزان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صفدرا بر بندگانت بسته نصرت هین و هین</p></div>
<div class="m2"><p>می خور ای با دشمنانت گفته حیرت هان و هان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیکوان بزم را دینار بخش و باده خواه</p></div>
<div class="m2"><p>گردنان رزم را فرمان ده و کشور ستان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وقت کار آمد جهان بگشای سرتاسر از آنک</p></div>
<div class="m2"><p>لشکر جرار داری بسته جانها برمیان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغ زن تا برتو خواند رسم جدت آفرین</p></div>
<div class="m2"><p>غزو کن تا از تو گردد جان جدم شادمان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منکران شرع را در هم شکن همچون عنب</p></div>
<div class="m2"><p>خستوان شرک را بر هم فکن چون ناردان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا بدین توفیق با کام دل و نام بزرگ</p></div>
<div class="m2"><p>سوی دارالملک برتابی بفیروزی عنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نعره الله اکبر موکبت گفته بلند</p></div>
<div class="m2"><p>آیت نصرمن الله رایتت کرده بیان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا بنالد زیر و زان ناله بر آساید ضمیر</p></div>
<div class="m2"><p>تا بگرید ابر و زان گریه بخندد گاستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدسگالت را چو زیر از زخم نالان باد دل</p></div>
<div class="m2"><p>نیک خواهت را چو باغ از ابر خندان باد جان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در سرافرازی به پای و در خداوندی بچم</p></div>
<div class="m2"><p>از جوانمردی به ناز و در جهان بانی بمان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملک افریدون بگیر و عدل نوشروان بکن</p></div>
<div class="m2"><p>جام جمشیدی تو نوش و کام اسکندر تو ران</p></div></div>