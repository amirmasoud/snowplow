---
title: >-
    شمارهٔ ۷۵ - در مدح احمد عمر گفته از غزنین فرستاد
---
# شمارهٔ ۷۵ - در مدح احمد عمر گفته از غزنین فرستاد

<div class="b" id="bn1"><div class="m1"><p>ای باد سپیده دم سفر کن</p></div>
<div class="m2"><p>یک چند رفیقی قمر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با نغمت زهره همنفس باش</p></div>
<div class="m2"><p>درصورت مشتری نظر کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خاک بهشت بوی بردار</p></div>
<div class="m2"><p>وز پر همای بال و پر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از طایر یمن پای و سرساز</p></div>
<div class="m2"><p>وز آب حیات روی ترکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از طره مشکبار خوبان</p></div>
<div class="m2"><p>خود را چو بهار بهره ور کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر مجمر سینهای عشاق</p></div>
<div class="m2"><p>رو جلوه کنان یکی گذر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیکان زمردین غنچه</p></div>
<div class="m2"><p>از دیده بسدین سپر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر دیده بخت را که بینی</p></div>
<div class="m2"><p>همچون شکفه ز خواب بر کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنگه خوش و تازه و همایون</p></div>
<div class="m2"><p>منزل بر احمد عمر کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن مسند را که جای تخت است</p></div>
<div class="m2"><p>از عطر چو گلشن دگر کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاک قدمش چو بار دادت</p></div>
<div class="m2"><p>خدمت چو قلم همه به سر کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چند که نظم کردم این عقد</p></div>
<div class="m2"><p>نثری که کنی از این گهر کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نازک طبع و عزیز وقت است</p></div>
<div class="m2"><p>گوئی چو حدیث مختصر کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از بنده بگو که ای خداوند</p></div>
<div class="m2"><p>کار حسن از کرم چو زر کن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از خوی خوش و حدیث شیرین</p></div>
<div class="m2"><p>بهر دل خلق گل شکر کن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیکی کردی و نیکت آمد</p></div>
<div class="m2"><p>چون دیدی سود بیشتر کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یارب تو حسود جاه او را</p></div>
<div class="m2"><p>خسته دل و سوخته جگر کن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون روی بروی شد به حاجت</p></div>
<div class="m2"><p>چون محتاجانش دربدر کن</p></div></div>