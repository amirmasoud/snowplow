---
title: >-
    شمارهٔ ۳۵ - ایضا در مدح بهرام شاه گفته است
---
# شمارهٔ ۳۵ - ایضا در مدح بهرام شاه گفته است

<div class="b" id="bn1"><div class="m1"><p>خه بنا میزد این جهان نگرید</p></div>
<div class="m2"><p>خوشی باغ و بوستان نگرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج یاقوت ارغوان بینید</p></div>
<div class="m2"><p>تخت مینای گلستان نگرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک را زنده کرد باد از لطف</p></div>
<div class="m2"><p>ای عجب این دم روان نگرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنده زو شد جهان و او بیمار</p></div>
<div class="m2"><p>این توانای ناتوان نگرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه گله است از شکوفه و سبزه</p></div>
<div class="m2"><p>که به پروین آسمان نگرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر ایزد همی کند سوسن</p></div>
<div class="m2"><p>آن یکی گوی ده زبان نگرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدح لاله سرنگون بینید</p></div>
<div class="m2"><p>قمع یاسمین ستان نگرید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جام بر کف گل جوان خندید</p></div>
<div class="m2"><p>ای جوانان در این جوان نگرید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبع گل نازکست رنگ آرد</p></div>
<div class="m2"><p>هان و هان سوی او نهان نگرید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بدانید قدر فصل بهار</p></div>
<div class="m2"><p>در گران جانی خزان نگرید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اعتدال بهار در گذر است</p></div>
<div class="m2"><p>عدل شه دایم است آن نگرید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاه بهرامشه که دولت گفت</p></div>
<div class="m2"><p>کای رعایا خدایگان نگرید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر همی عمر جاودان طلبید</p></div>
<div class="m2"><p>در شهنشاه جاودان نگرید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفتهای حسن جهان بگرفت</p></div>
<div class="m2"><p>فر مدح شه جهان نگرید</p></div></div>