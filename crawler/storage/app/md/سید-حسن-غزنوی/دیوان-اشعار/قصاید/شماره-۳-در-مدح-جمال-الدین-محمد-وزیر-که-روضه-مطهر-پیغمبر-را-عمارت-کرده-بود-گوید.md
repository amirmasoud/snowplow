---
title: >-
    شمارهٔ ۳ - در مدح جمال الدین محمد وزیر که روضه مطهر پیغمبر را عمارت کرده بود گوید
---
# شمارهٔ ۳ - در مدح جمال الدین محمد وزیر که روضه مطهر پیغمبر را عمارت کرده بود گوید

<div class="b" id="bn1"><div class="m1"><p>چو دولت رفت بر تخت امارت</p></div>
<div class="m2"><p>مه تاجش پذیرفت استدارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وزیری جست فحل و شهم و مقبل</p></div>
<div class="m2"><p>که باشد در همه کارش مهارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسازد کار عقبی از کفایت</p></div>
<div class="m2"><p>نگیرد نام دنیا از حقارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عزت ماه گردون سعادت</p></div>
<div class="m2"><p>بگوهر در دریای طهارت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد را گفت کی نقاد مردان</p></div>
<div class="m2"><p>کجا و در که دیدی این امارت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از صدری چنینم مژده گیری</p></div>
<div class="m2"><p>دهم ملکیت حق این بشارت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد مسکین دراین خدمت فرو ماند</p></div>
<div class="m2"><p>فتاد اندر بحار استخارت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز رای پیر و از بخت جوان هم</p></div>
<div class="m2"><p>نمود الحق در این باب استشارت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سعادت کردش از دنباله چشم</p></div>
<div class="m2"><p>به مولانا جمال الدین اشارت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود خاموش چون گل جمله معنی</p></div>
<div class="m2"><p>شود بلبل چو آید در عبارت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خجسته حاتم ثانی محمد</p></div>
<div class="m2"><p>که جودش ملک کانها کرد غارت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دمش بس خرم و گرم است آری</p></div>
<div class="m2"><p>نسیم گل نباشد بی حرارت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فراخای دلش را بحر گفتم</p></div>
<div class="m2"><p>چو تنگ آمد مجال استعارت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزرگا هر چه آن مقلوب گردد</p></div>
<div class="m2"><p>شود منکوس و زاید استزارت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ترازو را نداری از کرم زانک</p></div>
<div class="m2"><p>ترازویست مقلوب وزارت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خهی در خلق عطارت حلاوت</p></div>
<div class="m2"><p>زهی در خط نقاشت مرارت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو از مکه شدم سوی مدینه</p></div>
<div class="m2"><p>خدایم داد توفیق زیارت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیامی داد جدم مصطفی خوب</p></div>
<div class="m2"><p>به دستوری رسانیدم سفارت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پیام آن است کای شایسته فرزند</p></div>
<div class="m2"><p>که بادا بحر علمت را غزارت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فرا شو نزد آن آزاد مردی</p></div>
<div class="m2"><p>که دارد در جوانمردی بصارت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگو کای خواجه مقبول مقبل</p></div>
<div class="m2"><p>غلامانت سزاوار امارت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بنای عمر تو معمور بادا</p></div>
<div class="m2"><p>که کردی روضه ما را عمارت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بخر از مال فانی جان باقی</p></div>
<div class="m2"><p>که میمون باد بر تو این تجارت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صبا دوش آمد و دادم بشارت</p></div>
<div class="m2"><p>که خیز ای در دریای طهارت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بده مژده که از ابر کرم یافت</p></div>
<div class="m2"><p>نهال باغ امیدت خضارت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نیک و بد ندانستم در این باب</p></div>
<div class="m2"><p>فتادم در بحار استخارت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ظهیر دولت و ملت محمد</p></div>
<div class="m2"><p>که دارد در جوانمردی مهارت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر از خورشید رأیش یافتی ماه</p></div>
<div class="m2"><p>بماندی تا ابد در استنارت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هزاران چاکر و خادم به پیشش</p></div>
<div class="m2"><p>که کمتر شان منم با این حقارت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی اندر فنون علم و حکمت</p></div>
<div class="m2"><p>شده چون مرد یک فن با غزارت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر چه آن دل پاکت دریغ است</p></div>
<div class="m2"><p>که بندی در مهمات غرارت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ولیکن راستی داند که چون تو</p></div>
<div class="m2"><p>نبوده است و نباشد در سفارت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مبارک روی محمودت بود روز</p></div>
<div class="m2"><p>که بر ملک سخن یابد امارت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حدیثی نیست رسمی آنچه گفتم</p></div>
<div class="m2"><p>که در سیماش دیدم این اشارت</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزرگا حسب حالی طرفه افتاد</p></div>
<div class="m2"><p>به دستوری نمایم این جسارت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جهان بر من که خیرش چون نیرزد</p></div>
<div class="m2"><p>همی خواهد که بفروشد شرارت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سزای او ببیتی کردمی لیک</p></div>
<div class="m2"><p>در این مجلس بترسم زان قذارت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مجیر من تو بس باشی که دادم</p></div>
<div class="m2"><p>به مهرت خانه دل را اجارت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو لاله سرخ رویم کن همان دان</p></div>
<div class="m2"><p>که کردی روضه جدم زیارت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نهالی را که بار و برگ او داد</p></div>
<div class="m2"><p>خزان هرگز نیارد کرد غارت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بنائی کن که همچون چرخ کهنه</p></div>
<div class="m2"><p>بود هر روز نوتر این عمارت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ور از تو بگذرد خود در جهان کیست</p></div>
<div class="m2"><p>که داند کرد کاری را کفارت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>الا تا از جهان تنگ ترکیب</p></div>
<div class="m2"><p>حلاوت کس نبیند بی مرارت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دل و چشمت مظفر با دو منصور</p></div>
<div class="m2"><p>ز رأیت شرع بپذیرد وقارت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سعادت های تو چندان که گیرد</p></div>
<div class="m2"><p>ز مغرب تا به مشرق این اشارت</p></div></div>