---
title: >-
    شمارهٔ ۹۵ - در مدح ابوعلی حسن احمد گوید
---
# شمارهٔ ۹۵ - در مدح ابوعلی حسن احمد گوید

<div class="b" id="bn1"><div class="m1"><p>زهی مبارک فال و زهی خجسته بنی</p></div>
<div class="m2"><p>که چرخ برتو سعادت کند نثار همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش شکل تو ناقص گذاردش آذر</p></div>
<div class="m2"><p>به پیش نقش تو مثله نمونه مانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرافق تو گشاده چو عرصه عالم</p></div>
<div class="m2"><p>مؤانس تو کشیده بگنبد اعلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشت قاعده تو ز مسکن قارون</p></div>
<div class="m2"><p>رسید کنگره تو به موقف عیسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو آسمانی اندر علو و اندر تو</p></div>
<div class="m2"><p>مجره وار یکی آب ساخته مجری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عکس آب روان و صفای دیوارت</p></div>
<div class="m2"><p>یکی سه گردد مهمان چو سازدت ماوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه آبی آبی که صورت دیوار</p></div>
<div class="m2"><p>حیات یابدی ار باشدی از آتش غذی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشان کوثر اگر خوانیش رواست که هست</p></div>
<div class="m2"><p>نهال دولت صاحب نمونه طوبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برو نهاده یکی سلسله چو بر مجنون</p></div>
<div class="m2"><p>اگر چه هست مر او را لطافت لیلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهاده صورت یکتای تو و گوش بدر</p></div>
<div class="m2"><p>که تاز آمدن سایلی رسد بشری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدای گنبد تو در موافقت آید</p></div>
<div class="m2"><p>ز بهر خواندن مهمان چو در دهند ندی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین همانا زان گشته تو مهمان دوست</p></div>
<div class="m2"><p>که سوی تو نظر کرد خواجه دنیی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابو علی حسن احمد آنکه زو پرسند</p></div>
<div class="m2"><p>همه بزرگان در شرع مکرمت فتوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مؤیدی که به نزدیک ابر بخشش او</p></div>
<div class="m2"><p>همه کسیر نماید مروت کسری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز جود فربه او جسم آز شد لاغر</p></div>
<div class="m2"><p>ز کلک لاغر او جسم مردمی فربی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خهی ز جود تو یک قطره دجله و جیحون</p></div>
<div class="m2"><p>زهی ز حلم تو یک ذره بوقبیس وحری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه دیده ای تو هنوز از سعادت ازلی</p></div>
<div class="m2"><p>که از هزار گل تو شکفته نیست یکی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سلاله ملکی سازد ای امین ملک</p></div>
<div class="m2"><p>سعادت ازلی ساید ای امین هدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بهر حاسد تو توتیای بی خوابی</p></div>
<div class="m2"><p>برای ناصح تو کیمیای استغنی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آن مهم نهانی که شه ترا فرمود</p></div>
<div class="m2"><p>چنان خبر بود از عین کارهات همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که گرد و پیکر ملک دگر بر اندیشد</p></div>
<div class="m2"><p>کند عطارد آن حال را بتوانهی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملک چو دست ترا برسخا سواری داد</p></div>
<div class="m2"><p>گرفت بخل ز بیم نیاز خر بکری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزرگوارا من بنده را بپرور از آن</p></div>
<div class="m2"><p>که جز حریم توام نیست ملجاء و ماوی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو من به تربیت تو همی زنم لافی</p></div>
<div class="m2"><p>چنان مکن که خجل گردم اندرین دعوی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مراست شعری در مدح تو بلند چنانک</p></div>
<div class="m2"><p>بدانکه هست چو شعری که نیست او شعری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی دهی ز رو دیبا همی ستانی مدح</p></div>
<div class="m2"><p>بلی بزرگان چونین کنند بیع و شری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنا نهادی قصری که هستش اوج و حضیض</p></div>
<div class="m2"><p>چو قدر تو به ثریا چو حلم تو بثری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برو درین دهه عید می شتابد خلق</p></div>
<div class="m2"><p>چو حاجیان به سوی کعبه از صفا و منی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان که کعبه ملت بنا نهاد خلیل</p></div>
<div class="m2"><p>خجسته کعبه دولت بنا نهاده توی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا که بروید به بوستان سوسن</p></div>
<div class="m2"><p>همیشه تا که بتابد بر آسمان شعری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسان شعری رای تو باد تابنده</p></div>
<div class="m2"><p>چو لاله روی حسودت به خون دیده طلی</p></div></div>