---
title: >-
    شمارهٔ ۲۸ - در مدح محمود بغراخان گوید
---
# شمارهٔ ۲۸ - در مدح محمود بغراخان گوید

<div class="b" id="bn1"><div class="m1"><p>وقت آن است که مستان طرب از سر گیرند</p></div>
<div class="m2"><p>طره شب ز رخ روز همی برگیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطربان را و ندیمان را آواز دهید</p></div>
<div class="m2"><p>تا سماعی خوش و عیشی به نوا در گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راویان هر نفسی تهنیتی نو خوانند</p></div>
<div class="m2"><p>مطربان هر کرتی پرده دیگر گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر فریاد نداریم پگاه است هنوز</p></div>
<div class="m2"><p>یک دو ابریشم شاید که فراتر گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیان گرم درآرند شراب گلگون</p></div>
<div class="m2"><p>که نسیمش ز دم خرم مجمر گیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزم را تازه تر از روضه رضوان دارند</p></div>
<div class="m2"><p>باده را چاشنی از چشمه کوثر گیرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقیانی و چگوئی و چگونه یارب</p></div>
<div class="m2"><p>که می گلگون از جام معنبر گیرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاهدانی که بدان معنی اگرشان یابند</p></div>
<div class="m2"><p>زاهدان هم به تبرک به بر اندر گیرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قطره خون بود از خنجر ایشان مریخ</p></div>
<div class="m2"><p>روز نصرت چو به کف قبضه خنجر گیرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهره در ساغرشان رقص کند همچو حباب</p></div>
<div class="m2"><p>گاه عشرت چو به کف گوشه ساغر گیرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بوسه ای از لبشان گر به مثل نقل کنی</p></div>
<div class="m2"><p>بوسه را در نمک و پسته و شکر گیرند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دوستان نیز حریفانه در آیند به کار</p></div>
<div class="m2"><p>وقت را یک دم بی مشغله در بر گیرند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رنگ در ساغر این باده احمر دارند</p></div>
<div class="m2"><p>سنگ در شیشه این قبه اخضر گیرند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترک این گنبد نه پوشش گردان گویند</p></div>
<div class="m2"><p>کم این خانه بی روزن بی در گیرند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گوی امید ز چوگان فلک بربایند</p></div>
<div class="m2"><p>توشه عمر ز دوران جهان بر گیرند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوش و خرم بنشینند چو خاقان محمود</p></div>
<div class="m2"><p>یاد اقبال شه عالم سنجر گیرند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو فلک تخت که شان در عدد تاجوران</p></div>
<div class="m2"><p>اول از خسرو و ثانی ز سکندر گیرند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که گمان برد که پیری و جوانی شب و روز</p></div>
<div class="m2"><p>رایت رأی مبارک زمه و خور گیرند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا که دانست که هرگز پدری و پسری</p></div>
<div class="m2"><p>فر داود و سلیمان پیمبر گیرند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه بیزارند از دین قلندر حاشا</p></div>
<div class="m2"><p>که نه آسان جهان همچو قلندر گیرند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر پدر بحر محیط است پسر عنبر اوست</p></div>
<div class="m2"><p>ساحل بحر به بوی دم عنبر گیرند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ور پدر کوه عظیم است پسر گوهر اوست</p></div>
<div class="m2"><p>خاتم از کوه نگیرند ز گوهر گیرند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور پدر چرخ رفیع است پسر اختر اوست</p></div>
<div class="m2"><p>تابش از چرخ نگیرند ز اختر گیرند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خه خه ای شاه زمانه که هزارت شمرند</p></div>
<div class="m2"><p>هم به امر تو چو اندازه لشکر گیرند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زر زدن هست همه تاجوران را لیکن</p></div>
<div class="m2"><p>نام محمود محمد همه در زر گیرند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبح و شام ار چه شود روشن و تاریک از آن</p></div>
<div class="m2"><p>تاج و چترت را در دیده و در سر گیرند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملاء اعلی چون خطبه بنامت شنوند</p></div>
<div class="m2"><p>هفت گردون را دو پایه منبر گیرند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>منتت گاهی در ذمت خاقان بینند</p></div>
<div class="m2"><p>خلعتت روزی بر قامت قیصر گیرند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حاسدان تو که شان عمر کم و حسرت بیش</p></div>
<div class="m2"><p>گر چه نهمار جهان دیده و کشور گیرند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بندگانت را از کشتن ایشان چه شرف</p></div>
<div class="m2"><p>ننگ بر بازان روزی که کبوتر گیرند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اندر آن رزم که گردان دل رستم یابند</p></div>
<div class="m2"><p>اندر آن حال که مردان پی حیدر گیرند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آسمان آتش بیکار بتابد چو تنور</p></div>
<div class="m2"><p>اختران از تف خون لعلی اخکر گیرند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باد تازی را بر عرصه خاکی رانند</p></div>
<div class="m2"><p>آب هندی را در شعله آذر گیرند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تیغ ها صیقل خورشید سپرکش گردند</p></div>
<div class="m2"><p>نیزه ها دامن گردون زره ور گیرند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گل رخها را از گلبن قامت چینند</p></div>
<div class="m2"><p>مشک جان ها را از نافه پیکر گیرند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شخص ها سوی سر قارون همره طلبند</p></div>
<div class="m2"><p>روح ها بر قدم عیسی رهبر گیرند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نای روئین سبک و کوس مسین برسرشان</p></div>
<div class="m2"><p>نوحه و ناله چو ماتم زدگان در گیرند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن زمان فتح و ظفر پیش دوند از چپ و راست</p></div>
<div class="m2"><p>پس دو فتراک تو منصور و مظفر گیرند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چون به لشکر نگری موکب انجم رانند</p></div>
<div class="m2"><p>چون جنیبت طلبی مقود صرصر گیرند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گه ز هندیت عمود فلق صبح کنند</p></div>
<div class="m2"><p>گه ز خطیت قیاس خط محور گیرند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قدسیان بانگ بر آرند به تکبیر سبک</p></div>
<div class="m2"><p>فتحنامت چو کبوتر همه در پر گیرند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شهریارا منم آن بحر که طبع و قلمم</p></div>
<div class="m2"><p>آفرینش را در گوهر و در زر گیرند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مدح مسعود و غزلهای معزی را خلق</p></div>
<div class="m2"><p>گرچه با آتش و با آب برابر گیرند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تازی و پارسی معجزم از باغ علوم</p></div>
<div class="m2"><p>خشگ خاری است که در شاخ گل تر گیرند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روی در دزدند از شرم گر این آینه را</p></div>
<div class="m2"><p>پیش آن دو صنم شاهد دلبر گیرند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر چه خردم ملکا نام بزرگ از من خواه</p></div>
<div class="m2"><p>که بط فربه از جره لاغر گیرند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرچه درویش بود باز ولیکن شاهان</p></div>
<div class="m2"><p>خوشترین وقتش در دست توانگر گیرند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا جهانداران خاصه ز پی جانداری</p></div>
<div class="m2"><p>بنده و چاکر شایسته و در خور گیرند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو چنان بادی ای شاه که جاندارانت</p></div>
<div class="m2"><p>از جهانداران صد بنده و چاکر گیرند</p></div></div>