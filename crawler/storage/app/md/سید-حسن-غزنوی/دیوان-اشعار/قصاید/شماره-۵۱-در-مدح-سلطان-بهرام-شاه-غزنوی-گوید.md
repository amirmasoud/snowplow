---
title: >-
    شمارهٔ ۵۱ - در مدح سلطان بهرام شاه غزنوی گوید
---
# شمارهٔ ۵۱ - در مدح سلطان بهرام شاه غزنوی گوید

<div class="b" id="bn1"><div class="m1"><p>گهر بر زر همی بارم ز یاقوت در افشانش</p></div>
<div class="m2"><p>شدم چون ذره ای در سایه خورشید رخشانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیالش همچو او هیچم نمی پرسد عجب نبود</p></div>
<div class="m2"><p>که از بیمار پرسیدن نگردی هم پشیمانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعمر ار چند ننهاده است یک شب روی بر رویم</p></div>
<div class="m2"><p>برآنم تا کنم یکروز جان خویش در جانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سکندر آب حیوان را ز ظلمت جست و اینک من</p></div>
<div class="m2"><p>همی در چشمه خورشید بینم آب حیوانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهانش نقطه موهوم را ماند کنون آری</p></div>
<div class="m2"><p>مبادا چون بر آرد خط بود بر نقطه برهانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشاط انگیز عقل افتاد و نزهتگاه جان آمد</p></div>
<div class="m2"><p>می یاقوت جام او گل خورشید بستانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آورد این غم از پایم که چون سر برکند روزی</p></div>
<div class="m2"><p>ز طرف چشمه یاقوت زمرد رنگ ریحانش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدست آن زلف را بر گوش هر ساعت کند حلقه</p></div>
<div class="m2"><p>چو من خود حلقه در گوشم چه باید کرد دستانش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بند زلف پر بندش دلم ناگاه اگر بجهد</p></div>
<div class="m2"><p>فرو افتد هر اندر حال در چاه زنخدانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم خواهد که بگریزد ز بند زلف او لیکن</p></div>
<div class="m2"><p>ز بیم آنکه خون گردد نباشد زهره آنش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین با عاشقان غمزده دامن بیفشاند</p></div>
<div class="m2"><p>مگر عاشق شود روزی و گیرد غم گریبانش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجب نبود اگر جاوید ماند اندرین عالم</p></div>
<div class="m2"><p>که پس با اعتدال افتاده هر جزوی ز ارکانش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بفرمانم نباشد دل ز یمن عشق آن دلبر</p></div>
<div class="m2"><p>همین باشد سزای آنکه نبود دل به فرمانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلم را درد هجرانش بخست و قصد جان دارد</p></div>
<div class="m2"><p>کنون جان من و انصاف شاه و درد هجرانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یمین دولت عالی ملک بهرام شاه آن شه</p></div>
<div class="m2"><p>که سایل را همی شرم آید از جود فراوانش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خداوند جهان بهرام شاه آن خسرو عادل</p></div>
<div class="m2"><p>که با عمر خضر داده است حق ملک سلیمانش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دهان مشکین شود هر گه که گوید بخت جمشیدش</p></div>
<div class="m2"><p>زبان شیرین شود هر گه که گوید عقل سلطانش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان داری که از چرخ برین بگذشت مقدارش</p></div>
<div class="m2"><p>شهنشاهی که از روی زمین بگزید یزدانش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سعادت چشم بگشاده وز آن مقصود دیدارش</p></div>
<div class="m2"><p>زمانه گوش بنهاده وز آن مطلوب فرمانش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر گنجی بدست آرد فراهم کرده چون پروین</p></div>
<div class="m2"><p>ز بخشش چون بنات النعش گرداند پریشانش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بکان اندر اگر صد ساله زر باشد بیک ساعت</p></div>
<div class="m2"><p>چنان بخشد که پنداری مگر کین است باکانش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خهی رای تو خورشیدی که پنهان نیست تأثیرش</p></div>
<div class="m2"><p>زهی طبع تو دریائی که پیدا نیست پایانش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز جاه ار پایه ای باشد بود پای تو برفرقش</p></div>
<div class="m2"><p>ز بخت ار نامه ای باشد بود نام تو عنوانش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگردد خصم تو کامل و گر گردد چو مه باشد</p></div>
<div class="m2"><p>که هر وقتی که کامل گشت باشد وقت نقصانش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از آن نیلوفری تیغت بهیجا رنگ ریز آمد</p></div>
<div class="m2"><p>که همچون معصفر اندر شکم بست است دندانش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگردد تیغت از زخم فراوان کند و کر گردد</p></div>
<div class="m2"><p>به از سنگین دل دشمن نباشد سنگ افسانش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زند مه خرگه خود را ز ابر تر مزاج آبی</p></div>
<div class="m2"><p>مگر در منزل اول فتد قدر تو مهمانش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همیشه تا بود روی گلستان تازه و خرم</p></div>
<div class="m2"><p>هر آنگاهی که آراید به گوهر ابر نیسانش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز ابر کف گوهر بار تو روی نکو خواهت</p></div>
<div class="m2"><p>چنان باشد که نشناسد کس از تازه گلستانش</p></div></div>