---
title: >-
    شمارهٔ ۳۰ - در مدح محمد منصور است
---
# شمارهٔ ۳۰ - در مدح محمد منصور است

<div class="b" id="bn1"><div class="m1"><p>عمری مرا هوای لهاوور بوده بود</p></div>
<div class="m2"><p>همت بر آن سعادت مقصور بوده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزدیک تر نمودی از جان به نزد من</p></div>
<div class="m2"><p>زان پس که خود ز من چو دلم دور بوده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزدیک نور نیک بدیع است و این عجب</p></div>
<div class="m2"><p>گوئی که آفتاب مگر دور بوده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی نی چو من بدیع بلهاور کجا بود</p></div>
<div class="m2"><p>این نکته بر ضمیرم مستور بوده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدم کنون که خاصیت نور آفتاب</p></div>
<div class="m2"><p>در همت محمد منصور بوده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدری که هر یک از پدر و جد او چنو</p></div>
<div class="m2"><p>در مملکت برادی مشهور بوده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پس که شد به مجلس او مست و خوش چو سرو</p></div>
<div class="m2"><p>زان پس که همچو نرگس مخمور بوده بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون لاله سر به سر شده و پای در گلت</p></div>
<div class="m2"><p>هر حاسدی که چون گل در سور بوده بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بودم ضعیف و داد ما قوت سپاس</p></div>
<div class="m2"><p>چون می که جای جان شد و انگور بوده بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای آنکه نظم کردم در سلک مدح تو</p></div>
<div class="m2"><p>درها که تا به عهد تو منثور بوده بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک هفته دور ماند ز دیدار تو حسن</p></div>
<div class="m2"><p>از لطف درگذار که معذور بوده بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ذکر آن رود که کلیم گزیده را</p></div>
<div class="m2"><p>تهدید لن ترانی بر طور بوده بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می خور برآنکه پنداری آن کسی؟</p></div>
<div class="m2"><p>روح است مادر و پدرش حور بوده بود</p></div></div>