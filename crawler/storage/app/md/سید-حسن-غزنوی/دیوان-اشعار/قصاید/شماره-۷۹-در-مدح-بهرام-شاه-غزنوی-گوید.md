---
title: >-
    شمارهٔ ۷۹ - در مدح بهرام شاه غزنوی گوید
---
# شمارهٔ ۷۹ - در مدح بهرام شاه غزنوی گوید

<div class="b" id="bn1"><div class="m1"><p>بزرگ جشن همایون و ماه فروردین</p></div>
<div class="m2"><p>خجسته بادا بر آفتاب روی زمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علاء چتر و کلاه و بهاء افسر و بخت</p></div>
<div class="m2"><p>جمال تیغ و نگین و جلال مسندو دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر قدرت و ناهید بزم و فرخ مهر</p></div>
<div class="m2"><p>شهاب حمله و مریخ رزم و کیوان کین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یمین دولت و دولت بدو گرفته کمال</p></div>
<div class="m2"><p>امین ملت و ملت بدو بمانده متین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ابوالمظفر بهرام شاه بن مسعود</p></div>
<div class="m2"><p>که جان صورت ملک است و نور دیده دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه قدرت شاهی که گر نگاه کند</p></div>
<div class="m2"><p>به چشم رأفت و رحمت به بنده مسکین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه همچو الف در جهان ندارد هیچ</p></div>
<div class="m2"><p>شود به دولت او تاجدار همچون شین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به طبع و کف جوادش زمانه برده یسار</p></div>
<div class="m2"><p>برای و قدر بلندش سپهر خورده یمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سمن نروید هرگز چو مدح او خوشبوی</p></div>
<div class="m2"><p>شکر نباشد هرگز چو یاد او شیرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رأی روشن او شد زمین فلک آثار</p></div>
<div class="m2"><p>ز عدل شامل او شد جهان بهشت آئین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همی بنازد تاج و کلاه و گاه بدو</p></div>
<div class="m2"><p>چو کان بزر عیار و صدف بدر ثمین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خدایگانا شاها مگر تو خورشیدی</p></div>
<div class="m2"><p>که طول و عرض زمین شد ز جود تو زرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توئی که صورت اقبال را ز سر تا پا</p></div>
<div class="m2"><p>ز بهر دیدن تو دیده گشت چون پروین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سهم تیر تو خصمت دو تا شود چو کمان</p></div>
<div class="m2"><p>شهاب وار چنان کو برون جهد ز کمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو مر عدو را گوئی بگاه کینه که هان</p></div>
<div class="m2"><p>چو مر ولی را گوئی به وقت حمله که هین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز بیم تو به جهد بی خلاف زهره آن</p></div>
<div class="m2"><p>ز قهر تو نکشد کوهسار جمله این</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهانیان و جهان را رسید گوئی چشم</p></div>
<div class="m2"><p>در آن زمان که تو با ابرو اندر آری چین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو آفتابی و بر مه شود ز فر تو خاک</p></div>
<div class="m2"><p>ز بس که پیش تو شاهان بره نهند جبین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خهی سعادت با طالع تو کرده قرآن</p></div>
<div class="m2"><p>زهی سلامت با دانش تو گشته قرین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دل ستم شده از عدل شامل تو ضعیف</p></div>
<div class="m2"><p>تن امل شده از فر بخشش تو سمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کسی که دید بیان تو دید عالم علم</p></div>
<div class="m2"><p>کسی که یافت امان تو یافت حصن حصین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدای داده بداد و دهش ترا الهام</p></div>
<div class="m2"><p>سپهر کرده برای و روش ترا تلقین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توئی ز عالم چون عالم از صدف مقصور</p></div>
<div class="m2"><p>توئی ز شاهان چون مصطفی ز خلق گزین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسان حلقه انگشتریست بر خصمت</p></div>
<div class="m2"><p>همه چنانکه ترا هست جمله زیر نگین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به قدر بیشتری از همه جهان هر چند</p></div>
<div class="m2"><p>توئی جهان را امروز شاه باز پسین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه تا که نشاط مراد را باشد</p></div>
<div class="m2"><p>ز جرم گنبد خضرا طلوع زهره جبین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز جام زهره زهرا می نشاط بنوش</p></div>
<div class="m2"><p>ز باغ گنبد خضرا گل مراد بچین</p></div></div>