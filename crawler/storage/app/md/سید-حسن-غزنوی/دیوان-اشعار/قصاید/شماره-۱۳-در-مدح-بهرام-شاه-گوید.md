---
title: >-
    شمارهٔ ۱۳ - در مدح بهرام شاه گوید
---
# شمارهٔ ۱۳ - در مدح بهرام شاه گوید

<div class="b" id="bn1"><div class="m1"><p>دلم زان پسته خندان شکر یافت</p></div>
<div class="m2"><p>و زان یاقوت جان افشان گهر یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وصف کشی او عقل خود را</p></div>
<div class="m2"><p>چو گردون بی سرو بسیار سر یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالش نزد من آمد به پرسش</p></div>
<div class="m2"><p>به جان او اگر از من اثر یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشاطی در دل من خواست آمد</p></div>
<div class="m2"><p>ز تو بر تو غمش کی رهگذر یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلاکم کرده بود آن چشم جادوش</p></div>
<div class="m2"><p>یک افسون لبش آن کار در یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تر و خشکم بداد و مهربان شد</p></div>
<div class="m2"><p>چو با من جان خشک و چشم تر یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین ناگاه بر جانم ببخشود</p></div>
<div class="m2"><p>مگر از لطف شاهنشه خبر یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خداوند جهان بهرامشه آنک</p></div>
<div class="m2"><p>ز بهر خدمتش جوزا کمر یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین رحمت که بر من بنده فرمود</p></div>
<div class="m2"><p>حقیقت دان که ملک بحر و بر یافت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز یمن آنکه هدهد را طلب کرد</p></div>
<div class="m2"><p>سلیمانی به ملک خود دگر یافت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم ز اندوه در شادی بپرورد</p></div>
<div class="m2"><p>چو این تشریف شاه دادگر یافت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلی خفاش خاکی بود تیره</p></div>
<div class="m2"><p>ز عیسی زنده گشت و بال و پر یافت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لقایش ذره را آورد پیدا</p></div>
<div class="m2"><p>از آن شاه کواکب تاج زر یافت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو رویم سرخ گشت از نور رأیش</p></div>
<div class="m2"><p>یقینم شد که گل رنگ از قمر یافت</p></div></div>