---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>ای رایت آفتاب و محلت بر آسمان</p></div>
<div class="m2"><p>راضی همیشه از تو خدای و خدایگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بر هزار میر و ملک تاج افتخار</p></div>
<div class="m2"><p>وی با دو پشت جد و پدر شاه و پهلوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرگ از نهیب عدل تو اندر دیار تو</p></div>
<div class="m2"><p>از بیم میش بدرقه گیرد سگ شبان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که تیغ تیز بگرید چو میغ کند</p></div>
<div class="m2"><p>وز خون تازه خاک بخندد چو گلستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی بود ز هول گمان اجل یقین</p></div>
<div class="m2"><p>گاهی بود ز بیم یقین امل گمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن در شکست پای امل را ز کوی دل</p></div>
<div class="m2"><p>وین برگشاد دست اجل را به سوی جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جویند جای فتنه دلیران جنگجوی</p></div>
<div class="m2"><p>سازند کار کینه شجاعان کاردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان را شود ز هیبت گرز تو دل سبک</p></div>
<div class="m2"><p>دل را بود ز ضربت رمح تو سرگران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرزت چنان بکوبد خصم ترا بحرب</p></div>
<div class="m2"><p>کش از مسام جوشد خون مغز از استخوان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گوئی که شرزه شیر گشاید همی کمین</p></div>
<div class="m2"><p>روزی که در شکار شها در کشی کمان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آرش اگر بدیدی تیر و کمانت را</p></div>
<div class="m2"><p>نشناختی ز سهم تو ترکش ز دوکدان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای جفت رای پاک ترا همت بلند</p></div>
<div class="m2"><p>وی یار عقل پیر ترا دولت جوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>این بنده سوی حضرت عالی نهاد روی</p></div>
<div class="m2"><p>تا از حوادث فلکی باشدش امان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسته میان خدمت صدر رفیع تو</p></div>
<div class="m2"><p>بگشاده بر مدیح دل آویز تو زبان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یابد اگر قبول خداوند بی خلاف</p></div>
<div class="m2"><p>خالی شود هوای دل بنده از هوان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تایید گل نگردد و شمشاد یاسمن</p></div>
<div class="m2"><p>تا ارغوان سمن نشود سرو خیزران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر حریم جود و جلال و بها به پای</p></div>
<div class="m2"><p>واندر سرای جاه و جلال و بقا بمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از باد گرز خاک ضلالت به باد ده</p></div>
<div class="m2"><p>وز آب تیغ آتش فتنه فرو نشان</p></div></div>