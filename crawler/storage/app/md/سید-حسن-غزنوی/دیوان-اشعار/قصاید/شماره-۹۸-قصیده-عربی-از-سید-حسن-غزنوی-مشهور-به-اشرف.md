---
title: >-
    شمارهٔ ۹۸ - قصیده عربی از سید حسن غزنوی مشهور به اشرف
---
# شمارهٔ ۹۸ - قصیده عربی از سید حسن غزنوی مشهور به اشرف

<div class="b" id="bn1"><div class="m1"><p>سلا کالطاف الاله الممجد</p></div>
<div class="m2"><p>سلام کاخلاق النبی المؤید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلام کتسلیم الحبیب الذی نای</p></div>
<div class="m2"><p>زمانا فزار الصب من غیر موعد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلام کمثل الصدغ یلهوبه الصبا</p></div>
<div class="m2"><p>علی صفحتی کافور خد مورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلام کمانم النسیم تنفسا</p></div>
<div class="m2"><p>باسرار ورد و هو متبسم ندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلام کطل صار (؟) عین نرجس</p></div>
<div class="m2"><p>معطر ما بین الجفون منهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلام کالحان العنادل سحرة</p></div>
<div class="m2"><p>یحاد بها سجع الحمام المغرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلام کما قارس فی دویقیه؟</p></div>
<div class="m2"><p>لدی القاع یشفی علة الکبد الصدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلام به فی لیلة القدر تنزل</p></div>
<div class="m2"><p>الملائکة والروح فیها الی الغد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلام کانفاسی اذا کنت ناطقا</p></div>
<div class="m2"><p>به مدح رسول الله جدی و سیدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علی من تصدی منصبا ای منصب</p></div>
<div class="m2"><p>علی من تولی سوددا ای سودد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>علی من تلقی حکمه ای حکمه</p></div>
<div class="m2"><p>علی من ترقی مصعدا ای مصعد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>علی من تحراه الخلائق اوجدت</p></div>
<div class="m2"><p>ولولم یکن ماکان شی ء بموجد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علی من تمطی قاب قوسین بالعلا؟</p></div>
<div class="m2"><p>ففاز باسهام العلی و التفرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علی من له عیسی بن مریم صاحب</p></div>
<div class="m2"><p>علی من به موسی بن عمران مقید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>علی من به عین القلوب تنبهت</p></div>
<div class="m2"><p>فنام بعین الله فی خیر مرقد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>امام جمیع المسلمین مطهر</p></div>
<div class="m2"><p>رسول الله العالمین محمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نظمت لکم عشرین من معجزاتة</p></div>
<div class="m2"><p>علی آنهاوالله لم یتعدد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فمنها سحاب کالمظلمة فوفه</p></div>
<div class="m2"><p>اقام و هو کالشمس بالنون یرتدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ومنها حصاة سبحت فی بنانه</p></div>
<div class="m2"><p>بنان الندی فاضت بماء مبرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>و منها بعیر جاه متظلما</p></div>
<div class="m2"><p>و عفر مثل الساجد المتعبد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ومنها شواء قال انی ملطخ</p></div>
<div class="m2"><p>فلا تدن منی یا فدینک و تعدی؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>و من ذاک بئرصب فیها طهوره</p></div>
<div class="m2"><p>ففاضت له بحر مغرق الموج مرتد؟</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هوی قصر کسری وانطفت نارشر کهم</p></div>
<div class="m2"><p>لمیلاده المیمون فی خیر مولد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عدا رفئه کحلا فلیس قیاده</p></div>
<div class="m2"><p>باعمی ولا المولی علی بارمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>لقد شهد الظبی النفور بصدقه</p></div>
<div class="m2"><p>کماقاله یا ظبی من انا فاشهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کذی نطق الضب الذی من ضلاله</p></div>
<div class="m2"><p>الی جحره المالوف لم یک یهتد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وقد حلب العجفاء فی غایة الطوا</p></div>
<div class="m2"><p>قدرت له فی خیمتی ام معبد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وایده فی الغار من غیره جاره</p></div>
<div class="m2"><p>باغلب جند کم برده مجند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لقد شق قرص البدر عند امهاتهم؟</p></div>
<div class="m2"><p>بایمائه مثل الرغیف المبرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تجلی کلام الله فی قلبه الذی</p></div>
<div class="m2"><p>هوالبحر ملی الدر لا فی الزبرجد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فسبحان من اسری فاسری بعبده</p></div>
<div class="m2"><p>وعرجه من مسجد ثم مسجد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ایا سیدالعباد یا من تورمت</p></div>
<div class="m2"><p>له قد ماه من دوام التهجد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نقلب من اصلاب النبیین آدم</p></div>
<div class="m2"><p>وشیث و نوح و الخلیل الموحد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایا خاتما الرسل کنت نبینا؟</p></div>
<div class="m2"><p>و آدم ملقی بین طین و جلمد؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فلولاک ما کان ملائکة التی</p></div>
<div class="m2"><p>سجدن لنور فیه الا تحجد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولولاک فی صلب الخلیل لما انطقت</p></div>
<div class="m2"><p>علی جسمه ما رد قیل لها ابردی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ولو لم تکن ما اخدوت السفر التی؟</p></div>
<div class="m2"><p>امرت علی حلق الذبیح و ماندی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سعی الشجر المجدی علی الارض نحوه</p></div>
<div class="m2"><p>وحی عماد کالکتیب المعمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ولو لم تکن نار و موسی مسلما</p></div>
<div class="m2"><p>الی الله فی ضمن مهد مهند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ولو لم تکن ما جاء عیسی مبشرا</p></div>
<div class="m2"><p>بمعهد مبعوث مسمی باحمد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ولولاک ماجاد الزمان بمکرم</p></div>
<div class="m2"><p>ولولاک ما فاز الانام به مرشد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ولولاک ماکان السماء تحرکت</p></div>
<div class="m2"><p>ولولاک الارضون الراسیات برکد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ولولاک ماکان النجوم برکع</p></div>
<div class="m2"><p>ولولاک ما کان الظلال بسجد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>علیک سلام الله یا قابض العدی</p></div>
<div class="m2"><p>علیک سلام الله یا باسط الندی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>علیک سلام الله یا دافع الردی</p></div>
<div class="m2"><p>علیک سلام الله یا شافع الردی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>الا ایها الحجاج صلوا و سلموا</p></div>
<div class="m2"><p>علی من به فریم نحله مخلد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>وصلو علی اصحابه انجم الهدی</p></div>
<div class="m2"><p>بایهم من یقتدی فهو مهتد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>(وبوبکر صدیق و) صاحب غاره</p></div>
<div class="m2"><p>فتی قبل الاسلام دون تردد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>(وعمر فاروق و) قائد جیشه</p></div>
<div class="m2"><p>یقول به اللهم دینک اید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>(وعثمان ذوالنورین) کاتب و حیه</p></div>
<div class="m2"><p>فتی جمع القرآن لم تتبد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وخصوا علیا کرم الله وجهه</p></div>
<div class="m2"><p>شجرا سخیا فی الملتقی بالمهند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اما ما قد اعطی خاتما فی رکوعه</p></div>
<div class="m2"><p>و صار شهیدا و هو بین التشهد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>علی قرنی عینیه و البضعه التی</p></div>
<div class="m2"><p>لدی ربها طلب برویح و یعبد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جزی الله عنا المصطفی ما استحقه</p></div>
<div class="m2"><p>و ما الله یجزی حید غیر حید</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اتینا الی الرحمان معتصما به</p></div>
<div class="m2"><p>و من یعتصم بالانبیاء فقد عدی</p></div></div>