---
title: >-
    شمارهٔ ۷۰ - در مدح صاحب نظام الملک ابو جعفر محمدبن عبدالمجید
---
# شمارهٔ ۷۰ - در مدح صاحب نظام الملک ابو جعفر محمدبن عبدالمجید

<div class="b" id="bn1"><div class="m1"><p>نسیم عدل همی آید از هوای جهان</p></div>
<div class="m2"><p>شعاع بخت همی تابد از لقای جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گزارد مژده میمون صدا خروس فلک</p></div>
<div class="m2"><p>فکند سایه خورشید بر همای جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز او که جای ندارد نداند اینکه چه کرد</p></div>
<div class="m2"><p>خدایگان جهان از کرم به جای جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سزد که دولت و دین هر دو تهنیت گویند</p></div>
<div class="m2"><p>خدایگان جهان را بکدخدای جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نظام ملک محمد که یمن صورت او</p></div>
<div class="m2"><p>خجسته آمد بر ملک پادشای جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کار بسته منال و عنان گشاده ببین</p></div>
<div class="m2"><p>که نقشبندی او شد گره گشای جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگوی جز فلک مستقیم کلکش را</p></div>
<div class="m2"><p>چو دیدی از روشش خط استوای جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ضمان شده است جهان را بقای او ورنه</p></div>
<div class="m2"><p>چه اعتماد توان کرد بر بقای جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد آن چنانک همه بانگ نام او شنوی</p></div>
<div class="m2"><p>اگر به صخره صما رسد صدای جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سایه پس رو او باش سال و مه همه عمر</p></div>
<div class="m2"><p>چو آفتاب همی کرد پیشوای جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آن نبود جهان را وفا که اهل نداشت</p></div>
<div class="m2"><p>کنون که یافت ببین تا ابد وفای جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خهی ز نوک قلم صد هزار در و گهر</p></div>
<div class="m2"><p>فرو گشاده برشته بهر قبای جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک مراد تو دارد خهی مراد فلک</p></div>
<div class="m2"><p>جهان هوای تو دارد زهی هوای جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو آمدی بسزا صاحب جهان ورنی</p></div>
<div class="m2"><p>نکرده بود مهیا فلک سزای جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نعوذ بالله اگر نه سر جهان شدئی</p></div>
<div class="m2"><p>نیامدی به زمین تا به حشر پای جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سزد که رای تو آیینه دار غیب آمد</p></div>
<div class="m2"><p>که هست رای تو جام جهان نمای جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امید گشت و دل آسوده شد چو سایه فکند</p></div>
<div class="m2"><p>درخت بخت تو بر بوستان سرای جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر که نبود عالم مباش باکی نیست</p></div>
<div class="m2"><p>که هست همت عالی تو و رای جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بخر ز غصه جهان را و هم تو کن آزاد</p></div>
<div class="m2"><p>سزا بود تو خداوند را ولای جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آب عدل نشان گرد فتنه را کز ظلم</p></div>
<div class="m2"><p>شکسته دانه دل دور آسیای جهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دریغ گوهر آزادگی و در سخن</p></div>
<div class="m2"><p>به بی زری شده زین چرخ مهره سای جهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آشنائی این چرخ موج زن کم کوش</p></div>
<div class="m2"><p>که غرقه گشته نمیرد در آشنای جهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دلم سراسر خوش بود چون گل و اکنون</p></div>
<div class="m2"><p>ز خون چو لاله لبالب شد از جفای جهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جهان ز در ضمیرم ببست پیرایه</p></div>
<div class="m2"><p>اگر از آن درماند یتیم وای جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مراست ملک سخن مطلق و تو میدانی</p></div>
<div class="m2"><p>وگر ندانی داند همی خدای جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به کیمیای کرم خاک آز را زر کن</p></div>
<div class="m2"><p>که هیچ گرد نخیزد ز کیمیای جهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همیشه تا گل و بلبل به جلوه گاه بهار</p></div>
<div class="m2"><p>کنند ساخته برگ و نوا برای جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جمال چون گل و لفظ چو بلبلت بادا</p></div>
<div class="m2"><p>چنانکه سازد این برگ و آن نوای جهان</p></div></div>