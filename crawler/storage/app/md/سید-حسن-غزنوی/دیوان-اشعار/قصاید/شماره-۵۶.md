---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>داند جهان که قره عین پیمبرم</p></div>
<div class="m2"><p>شایسته میوه دل زهرا و حیدرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریا چو ابر بار دگر آب شد ز شرم</p></div>
<div class="m2"><p>چون گشت روشنش که چه پاکیزه گوهرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دری پر از عجایب دریا شود به حکم</p></div>
<div class="m2"><p>هر قطره‌ای که در صدف دل بپرورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبعم چو آتش تر و هر دم خلیل وار</p></div>
<div class="m2"><p>خوشبو گلی دگر دمد از آتش ترم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روید نبات نیشکر از جویبار گوش</p></div>
<div class="m2"><p>چون نایژه گشاد زبان شکر گرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر طبع آب خوردن شکر بود چراست</p></div>
<div class="m2"><p>از آب طبع زادن لفظ چو شکرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر فلک که هست بدستش کمان سخت</p></div>
<div class="m2"><p>می بفکند سپر ز زبان چو خنجرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر صد هزار پیکر لفظ است جان نشان</p></div>
<div class="m2"><p>بخشیده من است که جان دو پیکرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی کور کرده چشم بدان را و چون صدف</p></div>
<div class="m2"><p>پیرایه دار حق ز درون است زیورم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سهل است اگر به منظر من بنگری از آنک</p></div>
<div class="m2"><p>منظور عالم ملکوتست مخبرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گل بلبلی گزیند در باغ سیرتم</p></div>
<div class="m2"><p>مه اختری نماید در پیش اخترم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارم زبان و ژاژ نخایم که سوسنم</p></div>
<div class="m2"><p>بینم به چشم و عشق نبازم که عبهرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی نقش همچو آینه آب منقشم</p></div>
<div class="m2"><p>بی عطر چون فریشته جان معطرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خون در تنم چو نافه ز اندیشه خشک شد</p></div>
<div class="m2"><p>جرمم همین که هم نفس مشک اذفرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفتی چو گوشوارت دریست در دهان</p></div>
<div class="m2"><p>در در دهان چه سود که چون حلقه بردرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر لحظه دور جام تهی در دهد چو گل</p></div>
<div class="m2"><p>این پر شکوفه گلشن سبز مدورم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گردون بد حریفم برسر همی زند</p></div>
<div class="m2"><p>وین کفر بین که کوبد سر زخم نشترم؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر گوشمال چرخ نهی چشم همچو نای</p></div>
<div class="m2"><p>اینک رگیست راست نهاده چو مزهرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر سر نیاید از من یک ذره تیرگی</p></div>
<div class="m2"><p>چرخ ار فرو گذارد صد بار دیگرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر من بنیم جو بخرم هفت خنک چرخ</p></div>
<div class="m2"><p>پس همدم مسیح نیم همتگ خرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاکیست رنگ دنیا پاکیست نقش دین</p></div>
<div class="m2"><p>خاکی همی فروشم و پاکی همی خرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر هستیم نه است چه باک است گو مباش</p></div>
<div class="m2"><p>چون حاجتم نیست به هستی توانگرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آبی معقد است که زیور دهد درم</p></div>
<div class="m2"><p>خاکی ملون است چه رنگ آورد زرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از تاب آفتاب دل کوه خون گرفت</p></div>
<div class="m2"><p>و آوازه در فکند که یاقوت احمرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آب دهان کرم گره شد بحیلتی</p></div>
<div class="m2"><p>بگشاد بهر لاف که دیبای ششترم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نقش طراز جامه دیباست هست و نیست</p></div>
<div class="m2"><p>یارب تو هستیی ده کین نیست در خورم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا نیرم برای عروسان قدس را؟</p></div>
<div class="m2"><p>در دل که هست آینه غیب بنگرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چند از زبان برای دل دیو مردمان</p></div>
<div class="m2"><p>در دیو لاخ غیبت مردم گیا خورم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان تا لبی سپید کند هر سیه زبان</p></div>
<div class="m2"><p>دردا که چون زبان قلم گشت دفترم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زین آبگون قفس که چو مرغان همی پرد</p></div>
<div class="m2"><p>چون عم خویش جعفر طیار بر پرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چندان در این مشبک سربسته خاکیم</p></div>
<div class="m2"><p>کز چار بند طبع گشایند شهپرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زین نه سرای پرده نیلوفری برون</p></div>
<div class="m2"><p>یک طاق گلشن است که آنجاست منظرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر چون قلم ز لوح وجودم بریده باد</p></div>
<div class="m2"><p>گر تا بساق عرش فرود آید این سرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>با این شرف ز غصه طفلان وقت خویش</p></div>
<div class="m2"><p>خونابه چون جنین دهن بسته می خورم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون سرو پاک دامن خواهم هزار دست</p></div>
<div class="m2"><p>تا از درون چو غنچه گریبان دل درم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون سرفکنده گریم گوئی صراحیم</p></div>
<div class="m2"><p>چون خون گرفته خندم گوئی که ساغرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در قهقهه ز گریه دل چون گلابزن</p></div>
<div class="m2"><p>در خرمی ز سوز جگر همچو مجمرم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گفتی دریغ رنج حسن هم دریغ نیست</p></div>
<div class="m2"><p>آخر به بوی رنگی این رنگ می برم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از روی آنکه روی دلم سوی هزل نیست</p></div>
<div class="m2"><p>من در گنه ز توبه بسی بی گنه ترم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>استغفرالله ار به مثل زلتی کنم</p></div>
<div class="m2"><p>الحمدلله از سر آن زود بگذرم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در خواب کم شود دل بیدار من از آنک</p></div>
<div class="m2"><p>بیدار کرده نفس صبح محشرم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>احوال خویش اگرچه بگفتم یگان یگان</p></div>
<div class="m2"><p>سوگند می خورم که ندارند باورم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ناورده برون چو منی در هزار سال</p></div>
<div class="m2"><p>اینک تو ایدری فلکا و من ایدرم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در عهد من هر آنکه کند دعوی سخن</p></div>
<div class="m2"><p>خصمش خدای اگر ننشیند برابرم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>با خلق داوری چکنم بهر نظم و نثر</p></div>
<div class="m2"><p>اندی که من نخواسته داده است داورم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مردانگی باز و جوانمردی خروس</p></div>
<div class="m2"><p>خرسندی همای و وفای کبوترم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>منت خدای را که نینداخت دست حرص</p></div>
<div class="m2"><p>اندر نشیب وقف ز بالای منبرم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سردی زر خشکی سالوس چون نبود</p></div>
<div class="m2"><p>حاجت نیوفتاد بزهد مزورم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نثر دروغ بر چو منی افترا کنند</p></div>
<div class="m2"><p>کز نظم راست گرد ز دریا برآورم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سر شستنی دهم همه را زآتش ضمیر</p></div>
<div class="m2"><p>گر هم بر آب کار بشویند محضرم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از کس چو مهر و ماه سپر نفکنم از آنک</p></div>
<div class="m2"><p>چون تیغ صبح و تیر سحرگه دلاورم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>از باطل زمانه کیم سایه در فتد</p></div>
<div class="m2"><p>کاندر پناه سایه حق بوالمظفرم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سلطان یمین دولت بهرام شاه شاه</p></div>
<div class="m2"><p>کاقبال او گرفت بانصاف در برم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ای کاشکی پذیرد و کاریش آمدی</p></div>
<div class="m2"><p>تا جان نهاده بر طبقی پیشش آورم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در آرزوی آرزو اندر نیامده است</p></div>
<div class="m2"><p>آنها که شد ز دولت جودش میسرم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گویم همی بشکر که هست و همیشه باد</p></div>
<div class="m2"><p>از آسمان سریر و ز خورشید افسرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ماه خجسته ام نه که مهر مبارکم</p></div>
<div class="m2"><p>جان مجسمم نه که عقل مصورم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در منزلت رفیع تر از چرخ اعظمم</p></div>
<div class="m2"><p>در مرتبت خجسته تر از سعد اکبرم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همسایه همایم و هم سایه خدای</p></div>
<div class="m2"><p>کرده است این دو سایه سعادت میسرم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>همچون حواس نوبت من پنج از آن شده است</p></div>
<div class="m2"><p>کامداد عقل یکسره هستند لشکرم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا آنکه نوش کردم آب حیات عقل</p></div>
<div class="m2"><p>بی آب می نماید ملک سکندرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بوسید تاج و تخت سراپای من از آنک</p></div>
<div class="m2"><p>چون تخت پایه دارم و چون تاج سرورم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>جز خیر ناید از من و گر نیستی چنین</p></div>
<div class="m2"><p>در ملک دین خدای نکردی مخیرم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>قصدی همی کنند به کوتاه دیدگی</p></div>
<div class="m2"><p>تا در حسن به چشم کرم بیش ننگرم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گر هست بنده ای که بگوید چنین دری</p></div>
<div class="m2"><p>پذرفتم از خدای که او را بپرورم</p></div></div>