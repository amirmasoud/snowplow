---
title: >-
    شمارهٔ ۳۶ - درمدح سلطان بهرام شاه غزنوی گوید
---
# شمارهٔ ۳۶ - درمدح سلطان بهرام شاه غزنوی گوید

<div class="b" id="bn1"><div class="m1"><p>هفته دیگر بسعی ابر مروارید بار</p></div>
<div class="m2"><p>آورد شاخ شکوفه عقد مروارید بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه باد از عارض سنبل برانگیزد نسیم</p></div>
<div class="m2"><p>گاه ابر از طره شمشاد بنشاند غبار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطه باغ از ریاحین سبزتر از خط دوست</p></div>
<div class="m2"><p>گوشه شاخ از شکوفه پر درر چون گوش یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زانکه تا بر مردم دیده عروس باغ را</p></div>
<div class="m2"><p>حقه آیینه گون جلوه دهد مشاطه وار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد میسوزد بخور و ابر میریزد گلاب</p></div>
<div class="m2"><p>چرخ می گوید نوید و باغ می سازد نثار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چو قارون پای لاله ماند اندر گل سزد</p></div>
<div class="m2"><p>زانکه گردد از شکوفه دست موسی آشکار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه را از خوش دلی در پوست کی یابد مجال</p></div>
<div class="m2"><p>باده را از خرمی در جام کی باشد قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز به جان یکدگر سوگند کم گویند از آنک</p></div>
<div class="m2"><p>رنگ و بو از یکدگر دارند هر دو مستعار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گلبنان چون دلبران هر صبحدم خندند خوش</p></div>
<div class="m2"><p>بلبلان چون بیدلان هر نیم شب نالند زار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبزه زنگارگون گردد عیان در بوستان</p></div>
<div class="m2"><p>لاله شنگرف رنگ آید پدید از کوهسار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باغ پر طوطی شود از سبزه های بی قیاس</p></div>
<div class="m2"><p>چون سرایند بنده بلبل وار مدح شهریار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن امین ملت و از فتنه ملت را امان</p></div>
<div class="m2"><p>وان یمین دولت و ازیمن دولت را مدار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مشتری منظر شده کیوان محل بهرام شاه</p></div>
<div class="m2"><p>کاسمان روز گار است آفتاب روز بار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از نوالش بچه امید گشته سیر شیر</p></div>
<div class="m2"><p>وز نهیبش فتنه بیدار خورده کوکنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نور چشم دین و دولت ظل حق بهرام شاه</p></div>
<div class="m2"><p>کاسمان اعتبار است آفتاب روزگار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خاک درگاهش خورد سیم و از آن باشد عزیز</p></div>
<div class="m2"><p>کز سخاوت سیم و زر کرده است همچون خاک خوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مدح علمست این نداند جز شمار سیم و زر</p></div>
<div class="m2"><p>از شمار علم بیرون شد مگر علم شمار؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماه اگر گشتی ز ماه رایت او بهره مند</p></div>
<div class="m2"><p>مهر اگر گشتی ز مهر طلعت او مایه دار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>این یکی از چرخ گردان نیستی هر نیمه شب</p></div>
<div class="m2"><p>وان دگر بر خاک غلطان نیستی روزی دو بار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای ترا در عدل کمتر چاکری نوشیروان</p></div>
<div class="m2"><p>وی ترا در حمله کهتر بنده اسفندیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیر گردون روی همچون خار پشت اندر کشد</p></div>
<div class="m2"><p>چون شود نیلوفر تیغ تو گلگون در شکار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هیچ اگر خورشید دیدی رأیت اندر کارها</p></div>
<div class="m2"><p>دیده خورشید از آن حسرت فرو ماندی ز کار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عزمت ار گوید زمین را کای زمین یکره بگرد</p></div>
<div class="m2"><p>حزمت ار گوید فلک را کای فلک یکدم بدار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>این یکی چون دائره بر خویشتن گردان شود</p></div>
<div class="m2"><p>وآن یکی چون قطب گیرد مرکز خوش استوار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زانکه کردی اختیار از کارها احیای حق</p></div>
<div class="m2"><p>حق ترا کرد از همه شاهان عالم اختیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پست همت گمرهی سر گر فرو نارد ترا</p></div>
<div class="m2"><p>شخص او را پایه برتر بود بر بالای دار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راست پنداری که روز بزم بحری در سخا</p></div>
<div class="m2"><p>راست پنداری که روز رزم کوهی در وقار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زان بود شاعر ز جودت چون صدف پر در دهان</p></div>
<div class="m2"><p>زان کند زائر ز مهرت همچو کان پر زر کنار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رتبت چتر سیاهت جست چرخ روز کور</p></div>
<div class="m2"><p>لاجرم باشد همه چشمش سفید از انتظار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از نسیم خلق گلبویت بسان عندلیب</p></div>
<div class="m2"><p>بندگانت یک زمان دارند آوازی هزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا ز دور چرخ روشن خاک را باشد ثبات</p></div>
<div class="m2"><p>تا بگرد خاک تیره چرخ را باشد مدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خاک درگاه نکو خواه تو بادا چرخ قدر</p></div>
<div class="m2"><p>چرخ اقبال بد اندیش تو بادا خاکسار</p></div></div>