---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>یارب چه شور بود که اندر جهان فتاد</p></div>
<div class="m2"><p>سود حسود صدر جهان را زیان فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدر جهانیان حسن احمد حسین</p></div>
<div class="m2"><p>کش دست و دل به جود سوی بحر و کان افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با کوه حزم ثابت او هم رکاب گشت</p></div>
<div class="m2"><p>با باد عزم ثاقب او هم عنان فتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رأی پیر و بخت جوان ملک طفل را</p></div>
<div class="m2"><p>درد هر کهل دایه بس مهربان فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاک درش که آب حیات مرادهاست</p></div>
<div class="m2"><p>از جان و دل خرند که بس رایگان فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بند بندگیش فتادند عالمی</p></div>
<div class="m2"><p>در بند بندگی چنو می توان فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگین دلی که هست بر آتش ز دشمنش</p></div>
<div class="m2"><p>زان پس که روشنیش بر آب روان فتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین سهمگین سراب که هرگز مباد آن</p></div>
<div class="m2"><p>آتش نگر که در دل پیر و جوان فتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از وهم این خیال فلک در فلک شکست</p></div>
<div class="m2"><p>وز سهم این محال جهان در جهان فتاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دشمن دو روئی که نموده است همچو گل</p></div>
<div class="m2"><p>از دل چو لاله آتشش اندر دهان فتاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زود از قفا کشید زبانش بنفشه وار</p></div>
<div class="m2"><p>چون سوسنش اگر چه زبان فتاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او خود چو دیو بود که افسوس بهر او</p></div>
<div class="m2"><p>چندین هزار لعنت ما بر زبان فتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مه نور می فشاند و سگ بانگ می کند</p></div>
<div class="m2"><p>مه را چه جرم خاصیت سگ چنان فتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موسی بدان کمال بیفتد بگوشه ای</p></div>
<div class="m2"><p>کز بانگ گاو سامری اندر میان فتاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چونانک طاس را رسد آسیب ناگهان</p></div>
<div class="m2"><p>زین گفتگوی زلزله در آسمان فتاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آوازه شان بسی است که دستی بر آن نهاد؟</p></div>
<div class="m2"><p>چون حکم آن به شاه زمین و زمان فتاد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای صاحبی که صورت و شکل مبارکت</p></div>
<div class="m2"><p>مر سیرت بدیع ترا ترجمان فتاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خاص و عام کیست در این ملک پایدار</p></div>
<div class="m2"><p>کز سعی تو بسر شد و بر جاه جان فتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کفران نعمت تو که کفر است نزد من</p></div>
<div class="m2"><p>خواهد رسید در همه این آن نشان فتاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شستی اگر گشاد عدو از کمین مکر</p></div>
<div class="m2"><p>تیرش خطا پرید و ز دستش کمان فتاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یا ذکر آن زود که یکی دون چهی بکند</p></div>
<div class="m2"><p>ز انصاف روزگار هم اندر میان فتاد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سر سبز و سرخ روی چو سرو و چو گل بمان</p></div>
<div class="m2"><p>کز هر بدی که هیچ مبادت امان فتاد</p></div></div>