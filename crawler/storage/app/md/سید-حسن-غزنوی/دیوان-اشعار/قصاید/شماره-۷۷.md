---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>از گریه اگر یکدم سربر کنمی من</p></div>
<div class="m2"><p>چون شمع بسی آتش بر سر کنمی من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دست نیارم به سر زلف تو بردن</p></div>
<div class="m2"><p>ورنه همه آفاق معطر کنمی من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چشمه نوشت دهدی آب حیاتم</p></div>
<div class="m2"><p>هرگز سخن چشمه کوثر کنمی من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون غنچه بخنده بگشائی لب و گوئی</p></div>
<div class="m2"><p>چون گل دهن تنگ تو پرزر کنمی من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو می بدهی باده و خاصه ز پی نقل</p></div>
<div class="m2"><p>زان پسته یاقوتین شکر کنمی من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانی است مرا خشک بیاور نه هم آخر</p></div>
<div class="m2"><p>از باده لبهات کمی تر کنمی من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دیده من پای چو ابرو بنهی تو</p></div>
<div class="m2"><p>برگردن تو دست چو چنبر کنمی من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشغول توام گرنه به سال و به شب و روز</p></div>
<div class="m2"><p>از دیده و جان خدمت مهتر کنمی من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرزانه امین الدین آن حایگه من</p></div>
<div class="m2"><p>کز نعل سمندش زر افسر کنمی من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مدحت آن سرور واجب بود اینک</p></div>
<div class="m2"><p>کز چشم و مژه خامه و دفتر کنمی من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان در سر خاک قدمش پاشمی آخر</p></div>
<div class="m2"><p>دانم که اگر کارش در خور کنمی من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>توفیق عزیز است و گرنه بثناهاش</p></div>
<div class="m2"><p>از گوی فلک حقه گوهر کنمی من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان پیکر بد خواه فرومایه او را</p></div>
<div class="m2"><p>از تیغ زبان همچو دو پیکر کنمی من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سربر خط فرمانش همی دارم اگر نه</p></div>
<div class="m2"><p>خاک از ستم هر دو بسر بر کنمی من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اقبال چنان گفت که گر خواهدی آن صدر</p></div>
<div class="m2"><p>از مهر و مهش باده و ساغر کنمی من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مردی نبود کشتن نامردان ورنی</p></div>
<div class="m2"><p>برجمله اعداش مظفر کنمی من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دین هست قوی ورنه برای مدد دین</p></div>
<div class="m2"><p>در حمله به میدانش چو حیدر کنمی من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندر دل کفار که همچون شب تیره است</p></div>
<div class="m2"><p>تیر چو شهابش را رهبر کنمی من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاجز شده ام الحق در حق بزرگیش</p></div>
<div class="m2"><p>ای کاش حوالت به پیمبر کنمی من</p></div></div>