---
title: >-
    شمارهٔ ۹۲ - در مدح خاقان محمود خان گوید
---
# شمارهٔ ۹۲ - در مدح خاقان محمود خان گوید

<div class="b" id="bn1"><div class="m1"><p>ای گوهر مطهر مردی و مردمی</p></div>
<div class="m2"><p>اصل تو کان گوهر مردی و مردمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جنگ و صلح رستم میدان و مجلسی</p></div>
<div class="m2"><p>در کین و مهر حیدر مردی و مردمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارد دل تو نور ز ایمان و معرفت</p></div>
<div class="m2"><p>بارد لب تو شکر مردی و مردمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک و بدت موافق هستی و نیستی</p></div>
<div class="m2"><p>جان و دلت مسخر مردی و مردمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر تو چرخ اعظم افلاک و انجم است</p></div>
<div class="m2"><p>رأی تو سعد اکبر مردی و مردمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سعد از تو گشت طالع ناهید و مشتری</p></div>
<div class="m2"><p>جان از تو یافت پیکر مردی و مردمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی ساقه و طلیعه بند و گشاد تو</p></div>
<div class="m2"><p>گردی نکرد لشکر مردی و مردمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بزم و رزم شکر بخندید و خون گریست</p></div>
<div class="m2"><p>پیش تو جام و خنجر مردی و مردمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در قبضه و بنان تو بادا بر امر و نهی</p></div>
<div class="m2"><p>دایم حسام و ساغر مردی و مردمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چرخ ار ز صبح صادق در پیش آفتاب</p></div>
<div class="m2"><p>پرسد که کیست یاور مردی و مردمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوید جلال دنیا محمود خان که هست</p></div>
<div class="m2"><p>بر چرخ ملک محور مردی و مردمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای سایه سعادت دنیا و آخرت</p></div>
<div class="m2"><p>وی معجز پیمبر مردی و مردمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای آن عطاردی که نزیبد به اتفاق</p></div>
<div class="m2"><p>برج تو جز دو پیکر مردی و مردمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>القاب عالی تو طرازیست بر کجا</p></div>
<div class="m2"><p>بررایت مظفر مردی و مردمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حاجت چو سایه گشت نهان تا چو آفتاب</p></div>
<div class="m2"><p>پیدا شدی ز خاور مردی و مردمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر طالعت نبودی از رجعت و وبال</p></div>
<div class="m2"><p>هرگز نرستی اختر مردی و مردمی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جام می بری ز خصم و جهان می دهی به دوست</p></div>
<div class="m2"><p>وین هر دو هست در خور مردی و مردمی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قدرت نعوذبالله اگر سرکشی کند</p></div>
<div class="m2"><p>عاطل بماند افسر مردی و مردمی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شد مردمی و مردی در عهد ما غریب</p></div>
<div class="m2"><p>می دار دست بر سر مردی و مردمی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یارب چو خلق و خلق تو هرگز گلی شکفت</p></div>
<div class="m2"><p>از گلبن معطر مردی و مردمی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آمد پدید نقش تو هرگه که داشتند</p></div>
<div class="m2"><p>آیینه در برابر مردی و مردمی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر نو عروس بخت تو در جلوه نیستی</p></div>
<div class="m2"><p>هستی گسسته زیور و مردی و مردمی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزی که دهر خطبه کند جز به نام تو</p></div>
<div class="m2"><p>بادا شکسته منبر مردی و مردمی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چندانکه در جهان ز سکندر کنند یاد</p></div>
<div class="m2"><p>ای پادشاه کشور مردی و مردمی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>می بر کف چو بحر تو آب حیات باد</p></div>
<div class="m2"><p>ای راستی سکندر مردی و مردمی</p></div></div>