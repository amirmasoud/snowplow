---
title: >-
    شمارهٔ ۸۹ - در مدح سپهسالار علی بن الحسین ماهوری گفته
---
# شمارهٔ ۸۹ - در مدح سپهسالار علی بن الحسین ماهوری گفته

<div class="b" id="bn1"><div class="m1"><p>که دهد یار مرا از من بیدل خبری</p></div>
<div class="m2"><p>که کند سوی من خسته به رحمت نظری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز راند ز من و قصه من حرفی چند</p></div>
<div class="m2"><p>پیش آن بت که چنو نیست به خوبی دگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوید ای حور ز شرم تو گرفته طرفی</p></div>
<div class="m2"><p>گوید ای ماه ز رشک تو گزیده سفری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست در هجر تو از دست خودم زرین تاج</p></div>
<div class="m2"><p>آه اگر باشدم از دست تو سیمین کمری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مایه من همه چشم تر و جانی خشک است</p></div>
<div class="m2"><p>تر و خشکم چه دهی بهر چنین خشک وتری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جان و دل و دیده ز تو ای ماه دریغ</p></div>
<div class="m2"><p>گر تو خشنود شوی هم به چنین ما حضری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پرو بال مرا هجر تو پرکنده ببین</p></div>
<div class="m2"><p>این خطا را که نکردیم منه بال وپری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درگذر از من اگر نیز خطائی کردم</p></div>
<div class="m2"><p>کز تو و خط وفای تو ندارم گذری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بد مکن ور بکنی سهل بود نیک آن است</p></div>
<div class="m2"><p>که گذشت از بد و نیک من و تو بیشتری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم از تف تو گمراه شده است و به خدای</p></div>
<div class="m2"><p>که شب زلف تو خواهم به دعای سحری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شب زلف تو گمراه نیم زانکه مرا</p></div>
<div class="m2"><p>روز اقبال امیر است نکو راهبری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتاب همه سادات حسین آن کامروز</p></div>
<div class="m2"><p>نیست در خطه اسلام چنو ناموری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قامع شرک شجاعی که به مژده هر دم</p></div>
<div class="m2"><p>زو برد باد سوی روضه جدش خبری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فلک مرکز اقبال و چه عالی فلکی</p></div>
<div class="m2"><p>قمر گلشن افضال و چه تابان قمری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نه امل یافت چنو گاه سخانیک دلی</p></div>
<div class="m2"><p>نه اجل دید چنو روز وغا پر جگری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پدر شرع و نسب هر دو ز زادش با نام</p></div>
<div class="m2"><p>اینت نامی پسری و اینت گرامی پدری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روی او را نگرو فاتحه برخوان و بدم</p></div>
<div class="m2"><p>که ندیدست کسی صورت جان از شکری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه چنو باشد هر کو بود از نسل علی</p></div>
<div class="m2"><p>گرچه از کوه بود لعل مدان هر حجری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او همه مردمی و مردی با بیم و امید</p></div>
<div class="m2"><p>راست سیبی به دو نیم او پسری با پدری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای کم و بیش فلک بحر دلت را صدفی</p></div>
<div class="m2"><p>وی زر و سیم جهان ابر کفت را مطری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>صبح تیغ تو درد پرده دلهای سیاه</p></div>
<div class="m2"><p>در چنان شب تو جز این صبح مدان پرده دری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر آن روز که از جسم نماند رمقی</p></div>
<div class="m2"><p>واندر آن حال که از روح نماند اثری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یافت گوئی اجل از حمله پیاپی مددی</p></div>
<div class="m2"><p>کرد گوئی زحل از فتنه فراهم حشری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گه چو سر نای نماید سر برنای گلو</p></div>
<div class="m2"><p>گاه چون طبل نماید دل در هیچ بری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تیغ بر سر زده بر خون تو چون غمزده ای</p></div>
<div class="m2"><p>کوس بر روی زده نالان چون نوحه گری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن چنان شیر علم سر بفرازد که مثل</p></div>
<div class="m2"><p>گوئی از چشمه خورشید کند آب خوری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو در آن حال چو تقدیر نمائی به مصاف</p></div>
<div class="m2"><p>که نباشد ز تو البته مجال گذری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نقش هر سم سمند تو هلال املی</p></div>
<div class="m2"><p>عکس هر گوهر تیغ تو شعاع ظفری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>صف کفار چنان بر شکنی کز تیغت</p></div>
<div class="m2"><p>سقری نقد ببینند و چه سوزان سقری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ایکه در روضه ملک از پی سر سبزی دین</p></div>
<div class="m2"><p>شجری گشتی نهمار مبارک شجری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون توئی را چو منی باید اگر بستاید</p></div>
<div class="m2"><p>قدر خورشید کجا داند هر بی بصری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه بدان نزد کسی می برم ابرام که هست</p></div>
<div class="m2"><p>بر جگر آبی شان یا به کف و کیسه زری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دردمند است دل زار بزرگا چکنم</p></div>
<div class="m2"><p>با چنین درد دلی گفته چنین دردسری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خود خسان را ننهم منزلت مدحت از آنک</p></div>
<div class="m2"><p>گهر نیک دریغ است بهر بدگهری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روی سوی درشان زان ننهم تا نشوم</p></div>
<div class="m2"><p>چون امل روی به روئی چو اجل دربدری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کار تعظیم تو و ذات تو چیزی دگر است</p></div>
<div class="m2"><p>نتوان کرد قیاس تو بهر مختصری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مدح تست اینکه چنین می دهد انصاف زمان</p></div>
<div class="m2"><p>عون گوهر بود ار تیغ نماید هنری</p></div></div>