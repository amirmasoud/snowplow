---
title: >-
    شمارهٔ ۶ - در مدح معزالدوله خسرو شاه پسر بهرام شاه گوید
---
# شمارهٔ ۶ - در مدح معزالدوله خسرو شاه پسر بهرام شاه گوید

<div class="b" id="bn1"><div class="m1"><p>جان را ز عارض و لب او شیر و شکر است</p></div>
<div class="m2"><p>دل را ز طره و رخ او مشک و عنبر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم دل در آن وصال چو با عنبر است مشک</p></div>
<div class="m2"><p>هم جان در آن فراق چو با شیر شکر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشوب عقلم آن شبه عاج مفرشست</p></div>
<div class="m2"><p>نقل امیدم آن شکر پسته شکر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دیده اشک هست ولیکن لبالب است</p></div>
<div class="m2"><p>در سینه درد هست ولیکن سراسر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن آشناوشی که خیالست نام او</p></div>
<div class="m2"><p>در موج خون چو دیده من آشناور است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانان خوش است تحفه به باغ بتان ولیک</p></div>
<div class="m2"><p>نوباوه جمال ترا آب دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عالم نگر که گوئی جان منقش است</p></div>
<div class="m2"><p>بستان ببین که گوئی خلد مصور است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن غنچه نیست طوطی سبز شکر لب است</p></div>
<div class="m2"><p>وان روضه نیست شاهد نغز سمنبر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله چو مجمری که هم از مجمر است عود</p></div>
<div class="m2"><p>نی نی چو باده ای که هم از باده ساغر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا بر سر تو مردم چشمم گلاب ریخت</p></div>
<div class="m2"><p>در آتش فراق دلم همچو مجمر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم رسد به گوش تو پندم چو گوشوار</p></div>
<div class="m2"><p>آری رسد ولیکن چون حلقه بر در است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در خون من شده است یکایک دو چشم تو</p></div>
<div class="m2"><p>لب های تو میان من و چشم داور است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل برده ای و قصد به جان میکنی هنوز</p></div>
<div class="m2"><p>یا این همه که داشتم این نیز در خور است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست از جفا بدار که در آب غرق شد</p></div>
<div class="m2"><p>چشم حسن که خاک کف پای صفدر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خورشید چرخ نصرت محمود غازی آن</p></div>
<div class="m2"><p>کو نور دین و قوت شرع پیامبر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>والا مغز دولت خسرو شه شجاع</p></div>
<div class="m2"><p>کان شیر مرد غازی محمود دیگر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن خسروی که روز سخا روی دولت است</p></div>
<div class="m2"><p>وان صفدری که وقت وغا پشت لشکر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آیینه در مقابل رایش معطل است</p></div>
<div class="m2"><p>اندیشه در حدیقه مدحش معطر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن آب رنگ تیغش در تف چو آتش است</p></div>
<div class="m2"><p>وان کوه پیکر اسبش در تک چو صرصر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای عقل شاد باش که در بذل حاتم است</p></div>
<div class="m2"><p>وی جان گواه باش که در رزم حیدر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از مهر او صحیفه جانها منقش است</p></div>
<div class="m2"><p>با جود او ذخیره کانها محقر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روی سپهر طالع او را سزد از آنک</p></div>
<div class="m2"><p>نور دو چشم شاه جهان بوالمظفر است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بهرام شاه شاه که او را به بارگاه</p></div>
<div class="m2"><p>از آسمان سریر و ز خورشید افسر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن خسروی که پایه اول ز قدر او</p></div>
<div class="m2"><p>از اوج چرخ هفتم صد پایه برتر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صیتش فراخ پهنا چون عرض عالم است</p></div>
<div class="m2"><p>قدرش بلند بالا چون اوج اختر است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون چشم دلربایان عزمش کمان کش است</p></div>
<div class="m2"><p>چون زلف خوبرویان حزمش زره در است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آن نقش ذات اوست اگر روح پرور است</p></div>
<div class="m2"><p>و آن شکل تیر اوست اگر مرگ باپر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حاشا که در دل آرم کز ابر و آسمان</p></div>
<div class="m2"><p>کف سخیش و همت عالیش کمتر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شرم ایدم که گویم دریا و آفتاب</p></div>
<div class="m2"><p>با طبع پاک و رأی منیرش برابر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای مکرمی که آز فرومایه از کفت</p></div>
<div class="m2"><p>چون کان ز زر و بحر ز لؤلؤ توانگر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تیغ تو رنگ ریز و ضمیر تو نقش بند</p></div>
<div class="m2"><p>خلق تو گل فروش و بیانت شکرگر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر حاسد تو منزلتی یافت گو بیاب</p></div>
<div class="m2"><p>نقصان عمر مور ز افزونی پر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آنک حمل نه پیشرو شیر اعظم است</p></div>
<div class="m2"><p>وانک زحل نه تاج سر سعد اکبر است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جای جمال ماه بر اندود کوکب است</p></div>
<div class="m2"><p>تنهای آفتاب نه محتاج لشکر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای چون عزیز مصر حفیظ و علیم ملک</p></div>
<div class="m2"><p>بالله که مملکت به تو همواره در خور است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاها به مجلس تو فرستاد خادمت</p></div>
<div class="m2"><p>خطی چو خط دوست که از مشک و شکر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر در به تربیت که تو سفتی برای من</p></div>
<div class="m2"><p>حقا که آن بحقه جان من اندر است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مقصود من توئی، چو توام آمدی به دست</p></div>
<div class="m2"><p>از ماه و آفتابم هم سیم و هم زر است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>این دهر رنگ ریز مرا صوف و اطلس است</p></div>
<div class="m2"><p>وین چرخ نقره خنگ مرا اسب و استر است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شاها به دولت تو که در چشم همتم</p></div>
<div class="m2"><p>این و از این هزار که گفتم برابر است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا ابر و باد تیره و گل خاک روشن است</p></div>
<div class="m2"><p>تا جام آب خشک و شراب آتش تراست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر خور که چار طبع جهان دشمن ترا</p></div>
<div class="m2"><p>اندر درون دیده و دل نیش و نشتر است</p></div></div>