---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>این منم یارب که چرخم سوی اختر می کشد</p></div>
<div class="m2"><p>چشمه روشن ز خاک تیره ام بر می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این منم یارب که از خاکم سوی بالا چو آب</p></div>
<div class="m2"><p>دور این گردنده دولاب مدور می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این منم کاختر بصد خواری مرا بر در گذاشت</p></div>
<div class="m2"><p>بازم اکنون با هزاران ناز در بر می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در زمین هر لحظه چون قارون فروتر می شدم</p></div>
<div class="m2"><p>چون مسیحم هر دم اکنون چرخ برتر می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این همایون حضرت سلطان و این چشم من است</p></div>
<div class="m2"><p>کان مبارک خاک را چون توتیا در می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاربم توفیق خدمت ده که بختم بنده وار</p></div>
<div class="m2"><p>سوی سلطان سلاطین شاه سنجر می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه از طبعش به منت بحر مایه می برد</p></div>
<div class="m2"><p>وانکه از جودش به دامن ابر گوهر می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیغ رای او سپر از مهر انور می کند</p></div>
<div class="m2"><p>تیر عزم او کمان در چرخ اخضر می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از خداوندی قدم بر هفت گردون می نهد</p></div>
<div class="m2"><p>وز جوانمردی قلم بر هفت کشور می کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بانگ کوسش حلقه اندر گوش نصرت می کند</p></div>
<div class="m2"><p>گرد خیلش سرمه اندر چشم اختر می کشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در تاجش را فلک در عقد انجم می نهد</p></div>
<div class="m2"><p>باز چترش را ملک در زیر شهپر می کشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روز چون خورشید و ذره شب چو ماه و اختران</p></div>
<div class="m2"><p>می رود در ملک و بی اندازه لشکر می کشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خورد بر تخت سلیمان آب حیوان همچو خضر</p></div>
<div class="m2"><p>چیست مطلوبش که لشکر چون سکندر می کشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای که موکب همتت بر چرخ اعظم می برد</p></div>
<div class="m2"><p>وی که دامن طالعت بر سعد اکبر می کشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خان ترکستان ز خوان تو ذخیره می نهد</p></div>
<div class="m2"><p>رای هندوستان برای تو نفس بر می کشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوستکامی یافت از تو زهره بربط نواز</p></div>
<div class="m2"><p>لاجرم آب حیات اینک به ساغر می کشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدمتی تا سوی دربار تو خاقان می برد</p></div>
<div class="m2"><p>غاشیه پیش سر اسب تو قیصر می کشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماه موسی دست شد هارون لشکرهای تو</p></div>
<div class="m2"><p>زان جلاجلهای گردان منور می کشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راست پنداری عطارد نامه فتحت نوشت</p></div>
<div class="m2"><p>زان کمر شمشیر زرینش دو پیکر می کشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حکم و فتوی سعادت را قلم در دست تست</p></div>
<div class="m2"><p>مشتری زان طیلسان از شرم در سر می کشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وین عجایب تر که تا خطبه به نامت بشنود</p></div>
<div class="m2"><p>آسمان این هفت پایه پیش منبر می کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آفتاب کیمیاگر تا ببخشی کوه کوه</p></div>
<div class="m2"><p>ذره ذره سوی کانها از عدم زر می کشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا مگر مریخ خونی را سلح داری دهی</p></div>
<div class="m2"><p>گرچه گوئی یا نه بر خصمانت خنجر می کشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خرقه پوشیده کیوان بس کبود و هر زمان</p></div>
<div class="m2"><p>روی زرد حاسدت را نیل دیگر می کشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صدق بوبکریت بر عدل عمر دارد همی</p></div>
<div class="m2"><p>شرم عثمانیت سوی علم حیدر می کشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خسروا بنده حسن را دولت جاوید تو</p></div>
<div class="m2"><p>سوی درگاه تو شاه بنده پرور می کشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بلبل فضل است لیک از بهر داغ بندگیت</p></div>
<div class="m2"><p>هر زمانی دل سوی طوق کبوتر می کشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بهر تو کانی اگر چه نیست خاطر می کند</p></div>
<div class="m2"><p>پیش تو جانی اگر چه نیست در خور می کشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در ثنا شیرین زبان و در دعا روشن دل است</p></div>
<div class="m2"><p>هم بدین جرمش فلک در آب و آذر می کشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر زبانش شکر و دل شمع شد او هم کشد</p></div>
<div class="m2"><p>آن عنا کز آب و آذر شمع و شکر می کشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا فلک هر شب نماید حقه آیینه گون</p></div>
<div class="m2"><p>و اندر آن حقه هزاران زر و زیور می کشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زیور تاج و سریر و حیلت چتر تو باد</p></div>
<div class="m2"><p>هر گهر کین حقه آیینه پیکر می کشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر جهان از عدل شاه آسوده شد بس دور نیست</p></div>
<div class="m2"><p>هر که دردی می کشد از بهر درمان می کشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر که جان دارد برو شه را حقوق نعمت است</p></div>
<div class="m2"><p>کفر باشد هر که بر حق خط نسیان می کشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چرخ تاوان دار بود از جور های ما مضی</p></div>
<div class="m2"><p>الحق اندر عهد شه انصاف تاوان می کشد</p></div></div>