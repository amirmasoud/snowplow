---
title: >-
    شمارهٔ ۷۴ - در مدح حسن بن احمد گوید
---
# شمارهٔ ۷۴ - در مدح حسن بن احمد گوید

<div class="b" id="bn1"><div class="m1"><p>گاه آن است که طفلان چمن</p></div>
<div class="m2"><p>اندر آیند چو عیسی به سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه گشایند دهان از لاله</p></div>
<div class="m2"><p>گه نمایند زبان از سوسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مددی از دم عیسی است نسیم</p></div>
<div class="m2"><p>ثری از کف موسی است چمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله غرقه به خون همچو حسین</p></div>
<div class="m2"><p>سوسن زنده نفس همچو حسن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چاک زد غنچه گریبان صد جای</p></div>
<div class="m2"><p>از بدی عهد گل تر دامن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل چو شاهان ز پس پرده غیب</p></div>
<div class="m2"><p>حرکت کرده بسوی گلشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد بر شه ره او غاشیه کش</p></div>
<div class="m2"><p>ابر در موکب او مقرعه زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشک بید از جهت تحفه باغ</p></div>
<div class="m2"><p>کرده صد لخلخه از مشک ختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح خندان چو گل و گل چون صبح</p></div>
<div class="m2"><p>سحری چاک زده پیراهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گل چنان آتش افروخت به لطف</p></div>
<div class="m2"><p>که قدح را شده پر آب دهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاسمن چون شد فواره می</p></div>
<div class="m2"><p>جوی می باید راندن به چمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفرین باد که بوئی دارند</p></div>
<div class="m2"><p>از نسیم کرم خواجه من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حسن احمد خاص آن صدری</p></div>
<div class="m2"><p>که نهادند سرانش گردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کلک او تیزتر از تیر فلک</p></div>
<div class="m2"><p>لطف او پاک تر از در عدن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روشن آمد ز جهان تاریک</p></div>
<div class="m2"><p>چون زر از سنگ و گهر از معدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه عجب کز قلم چون تیرش</p></div>
<div class="m2"><p>تیغ خورشید بپوشید جوشن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بدیهای عدو را لطفت</p></div>
<div class="m2"><p>کرده پاداش به نیکی کردن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لطف مفزای که گه گاه چراغ</p></div>
<div class="m2"><p>هم بمیرد چو فزون شد روغن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رای خورشید وشت چون درتافت</p></div>
<div class="m2"><p>ماه را سوخته گردد خرمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شدمت بنده بگردان نامم</p></div>
<div class="m2"><p>نسزد دون ترا نام حسن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوست کامم ز تو و بر من هست</p></div>
<div class="m2"><p>هم بدین جرم جهانی دشمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ریسمان و ارم از آن می تابند</p></div>
<div class="m2"><p>که تهی چشم تراند از سوزن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا شب روز مرقع پوشند</p></div>
<div class="m2"><p>سقف این خانگه بی روزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بادت از زاویه داران فلک</p></div>
<div class="m2"><p>داعیان پیش خدای ذوالمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خاک پای تو جهان سرکش</p></div>
<div class="m2"><p>رام امر تو سپهر توسن</p></div></div>