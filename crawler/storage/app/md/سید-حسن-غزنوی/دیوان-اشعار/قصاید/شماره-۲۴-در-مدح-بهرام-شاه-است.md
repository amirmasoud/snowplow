---
title: >-
    شمارهٔ ۲۴ - در مدح بهرام شاه است
---
# شمارهٔ ۲۴ - در مدح بهرام شاه است

<div class="b" id="bn1"><div class="m1"><p>هم اکنون باز نقاش طبیعی خامه بر گیرد</p></div>
<div class="m2"><p>ز نقش عالم آرایش جهان زیب دگر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی بر آب تر از ابر زنگاری زره دوزد</p></div>
<div class="m2"><p>گهی از لاله تیغ کوه شنگرفی سپر گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سحاب پر زنم چشم نبی بی پسر گردد</p></div>
<div class="m2"><p>نسیم صبحدم رسم رسول بی پدر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا نقاش و عطار است پنداری که پیوسته</p></div>
<div class="m2"><p>چو نقاشی به پایان برد عطاری ز سر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیعت گر درختان را مطرا میکند شاید</p></div>
<div class="m2"><p>که چون گردد مطرا عود قیمت بیشتر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم باد سحر چون مجمر گل را برافروزد</p></div>
<div class="m2"><p>سراسر طارم بستان بخار عود برگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براند بر گلستان ابر نیسان آب و نندیشد</p></div>
<div class="m2"><p>که رخسار لطیف گل خود از بادی اثر گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پیش آسیای آبگون ابریست چون گردی</p></div>
<div class="m2"><p>که دیده است آسیا هرگز که گرد بحر و بر گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم اکنون لاله چون اصحاب کهف از خواب برخیزد</p></div>
<div class="m2"><p>چو نرگس نیز یکچندیش سودای سهر گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو دلشدگان کنون بلبل هزاران راز بگشاید</p></div>
<div class="m2"><p>چو دلداران کنون گلبن هزاران ناز درگیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسا طوطی که از بیضه تر و تازه برون آید</p></div>
<div class="m2"><p>جهان را همچو باز چتر سلطان زیر پر گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خداوند جهان بهرامشاه آن شاه کز جودش</p></div>
<div class="m2"><p>چو روی آسمان هر شب زمین هر روز زر گیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مکارم را چو برخیزد امل جود علی یابد</p></div>
<div class="m2"><p>مظالم را چو بنشیند جهان عدل عمر گیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرش افتد بکشتن چرخ را عزمش فرود آرد</p></div>
<div class="m2"><p>ورش باید به قوت کوه را حزمش کمر گیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جگر از آب تری یابد و این از همه خوشتر</p></div>
<div class="m2"><p>که تیغ شهریار آب است و گرمی از جگر گیرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به پیش رای او خورشید را بیخردگی باشد</p></div>
<div class="m2"><p>اگر تا دامن محشر گریبان سحر گیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بنامیزد بنامیزد زهی شاه قضا قدرت</p></div>
<div class="m2"><p>که تیغ عزم تو گوهر ز الماس قدر گیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو خورشیدی و بدخواهت در آن عقده که سال و مه</p></div>
<div class="m2"><p>ز قربت چون زحل سوزد ز بعدت چون قمر گیرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گه از لطفت جهان ملک بوی گلستان یابد</p></div>
<div class="m2"><p>گه از نطقت دهان کلک طعم نیشکر گیرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه زانهائی بحمدالله که از لشکر مدد جوئی</p></div>
<div class="m2"><p>نه مرد آن بود خورشید کز ذره حشر گیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مکان صرصر ز شبدیز تو در صحن فلک سازد</p></div>
<div class="m2"><p>پناه آتش ز شمشیر تو در قلب حجر گیرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو آتش میخورد خود را حسود و دیر برناید</p></div>
<div class="m2"><p>که روز بخت او کوتاهی عمر شرر گیرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خداوندا ضمیر بنده چون مشاطه چابک</p></div>
<div class="m2"><p>عروس مدح شاهی را همی اندر گهر گیرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به از اقران شود بنده چو شد شایسته خدمت</p></div>
<div class="m2"><p>سر گوران خورد روبه چو پای شیر نر گیرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>محالی نیست این معنی که گردد لاله و گوهر</p></div>
<div class="m2"><p>چو خاک و سنگ را خورشید در ظل نظر گیرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی تا باز زرین بر سر این چتر پیروزه</p></div>
<div class="m2"><p>ز مشرق هر سپیده دم سواد بحر و بر گیرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز فر طایر میمون ز پیروزی چنان بادی</p></div>
<div class="m2"><p>که باز چتر تو هر لحظه سیمرغ ظفر گیرد</p></div></div>