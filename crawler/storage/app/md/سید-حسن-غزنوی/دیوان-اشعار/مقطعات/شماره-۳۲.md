---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>دوستان را بند گردان از وفا</p></div>
<div class="m2"><p>ورنه باری از جفا دشمن من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نکردی یک زبانی لاله وار</p></div>
<div class="m2"><p>ده زبانی نیز چون سوسن مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بد خوئی با هیچ کس هرگز مکن</p></div>
<div class="m2"><p>ور کنی با دیگران با من مکن</p></div></div>