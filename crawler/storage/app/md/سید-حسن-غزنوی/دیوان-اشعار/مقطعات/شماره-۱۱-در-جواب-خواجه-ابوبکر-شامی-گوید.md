---
title: >-
    شمارهٔ ۱۱ - در جواب خواجه ابوبکر شامی گوید
---
# شمارهٔ ۱۱ - در جواب خواجه ابوبکر شامی گوید

<div class="b" id="bn1"><div class="m1"><p>به من بوبکر حیدر تازه تازه</p></div>
<div class="m2"><p>همی دروی گل خندان فرستد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی زهر مرا تریاق سازد</p></div>
<div class="m2"><p>گهی درد مرا درمان فرستد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه از شاخ وفا نوباوه بخشد</p></div>
<div class="m2"><p>گه از باغ هنر ریحان فرستد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نسیم پر تحیت کان خداوند</p></div>
<div class="m2"><p>ز طبع خوشتر ازنیسان فرستد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهدیه چشم نزد دل رساند</p></div>
<div class="m2"><p>چو تحفه دل به سوی جان فرستد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندانستم من این یارب که دانست</p></div>
<div class="m2"><p>که ایزد بهر عیسی خوان فرستد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تحیت تا بود نور علی نور</p></div>
<div class="m2"><p>بدست سید اقران فرستد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عدیم المثل ساعد آنکه کلکش</p></div>
<div class="m2"><p>به تیر آسمان فرمان فرستد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلی خورشید چون گل را دهد نور</p></div>
<div class="m2"><p>ضیاء مهتر تابان فرستد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن این بیتها نزدیک آن صدر</p></div>
<div class="m2"><p>همی از خویشتن پنهان فرستد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنم جسمم قلم را شرم دارد</p></div>
<div class="m2"><p>که سوی چشمه حیوان فرستد</p></div></div>