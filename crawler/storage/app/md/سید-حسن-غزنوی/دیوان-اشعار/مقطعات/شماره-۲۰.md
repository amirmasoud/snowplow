---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>گه بنالی که وای نان ناید</p></div>
<div class="m2"><p>گه برنجی که آه جان برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انده نان و جان مخور بنشین</p></div>
<div class="m2"><p>کین بیاید به وقت و آن برود</p></div></div>