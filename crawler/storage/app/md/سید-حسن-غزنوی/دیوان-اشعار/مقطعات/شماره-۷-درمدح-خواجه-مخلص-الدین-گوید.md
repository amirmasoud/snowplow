---
title: >-
    شمارهٔ ۷ - درمدح خواجه مخلص الدین گوید
---
# شمارهٔ ۷ - درمدح خواجه مخلص الدین گوید

<div class="b" id="bn1"><div class="m1"><p>چندان که در دوازده برج است هفت مرغ</p></div>
<div class="m2"><p>ای مخلص کریم ترا بخت یار باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا باز روز از شرف اندر حمل بود</p></div>
<div class="m2"><p>باز بقات را همه دولت شکار باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ثور برج زهره بلبل طرب بود</p></div>
<div class="m2"><p>پیش تو زهره ساز طرب بر کنار باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد میان جوزا چون طوق فاخته</p></div>
<div class="m2"><p>از بهر خدمتت کمری استوار باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چنگ و ناخن سرطان و عقاب مرگ</p></div>
<div class="m2"><p>همواره جان و جسم حسودت فکار باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدر همای سایه خورشید سای تو</p></div>
<div class="m2"><p>بر اوج شیر بیشه گردون سوار باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا هست نسر طایر بر برج سنبله</p></div>
<div class="m2"><p>خصمت چو نسر واقع افتاده خوار باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا نقد مهرکان را میزان مقرر است</p></div>
<div class="m2"><p>طاوس دولت تو به رنگ بهار باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای خصم تو چو عقرب و خفاش روز کور</p></div>
<div class="m2"><p>چون ماه شب چراغ رفیعت هزار باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاقوس آتشی و بط آبی بود به طبع</p></div>
<div class="m2"><p>طبع تو آب رونق و آتش غبار باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جدیی که هست خانه کیوان بوم شکل</p></div>
<div class="m2"><p>گز طالع حسودت گردد نزار باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ارکان دولت تو که سیمرغ قدرت است</p></div>
<div class="m2"><p>چون اختران دلو به کوکب چهار باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زرهای سور چرخ و در مهاء عفوت آب؟</p></div>
<div class="m2"><p>بر بلبلی که چون تو سراید نثار باد</p></div></div>