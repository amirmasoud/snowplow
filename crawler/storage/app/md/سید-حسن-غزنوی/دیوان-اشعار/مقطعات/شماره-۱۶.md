---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>به لعبتان ضمیر من آن زمان نگری</p></div>
<div class="m2"><p>که عکس آن به سپهر چو آینه برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدای داند کز باغ فضل من نو نو</p></div>
<div class="m2"><p>به یک یک امت احمد هر آینه برسد</p></div></div>