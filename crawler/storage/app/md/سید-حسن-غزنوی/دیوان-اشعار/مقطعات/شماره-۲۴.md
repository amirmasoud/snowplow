---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>جانم غریق نعمت شمس الملوک شد</p></div>
<div class="m2"><p>وین طرفه تر که میزیم اکنون به جان شکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بس که ابر لطف ببارید بر سرم</p></div>
<div class="m2"><p>بشکفت از بهار دلم بوستان شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گلبن ثناش زبان چو بلبلم</p></div>
<div class="m2"><p>دستان مدح می زند و داستان شکر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقا که تیر مدح برون پرد از جهان</p></div>
<div class="m2"><p>گر درکشم به قوت مهرش کمان شکر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بر دلم گشاده به سعیت در امید</p></div>
<div class="m2"><p>جان بسته ام به جای کمر بر میان شکر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایم چو در رکاب سعادت بعون تست</p></div>
<div class="m2"><p>آن به که سوی صدر تو تابم عنان شکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین پس اگر خدای بخواهد به دولتت</p></div>
<div class="m2"><p>هرلحظه گوهری بدر آرم ز کان شکر</p></div></div>