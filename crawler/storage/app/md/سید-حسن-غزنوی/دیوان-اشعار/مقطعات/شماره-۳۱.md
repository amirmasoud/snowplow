---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>پاسبانان گنج فضل منند</p></div>
<div class="m2"><p>بر فلک دیده‌های بیداران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشک پرنافه‌ای که خلقم داد</p></div>
<div class="m2"><p>کیسه‌داری کند به عطاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رسیدم به جای بی‌جایان</p></div>
<div class="m2"><p>نکنم بیش کار بیکاران</p></div></div>