---
title: >-
    بخش ۹ - رفتن داماد به خانهٔ عروس و در راه فرود آمدن دیوار بر سر او
---
# بخش ۹ - رفتن داماد به خانهٔ عروس و در راه فرود آمدن دیوار بر سر او

<div class="b" id="bn1"><div class="m1"><p>چو صبح این لعبت خاور نشیمن</p></div>
<div class="m2"><p>لوای شعله زد در دشت ایمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگر خون عاشق شوریده ایام</p></div>
<div class="m2"><p>در آغاز محبت حسرت انجام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گنج از خانهٔ ویران بر آمد</p></div>
<div class="m2"><p>تو گفتی یوسف از زندان برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش رو فکند از گل نقابی</p></div>
<div class="m2"><p>به مهتابی نهفته آفتابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زگل آغوش زین رشک چمن شد</p></div>
<div class="m2"><p>زنکهت تازگی باد ختن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر بتخانه کرد و دل برهمن</p></div>
<div class="m2"><p>شکیبایی عنان و شوق توسن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدم بر آرزو می سود و می رفت</p></div>
<div class="m2"><p>نگاهش بر قفا می بود و می رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان سرشار شوق از شادی او</p></div>
<div class="m2"><p>عروسی خانهٔ دامادی او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خروش نای و بانگ شادیانه</p></div>
<div class="m2"><p>فکنده حلقه در گوش زمانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چراغان کرده بام و در گلستان</p></div>
<div class="m2"><p>گلستانی ز پا بوسش خیابان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جان شهری تماشامست شادی</p></div>
<div class="m2"><p>فلک گلدسته ای در دست شادی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی او بی نصیب از شادکامی</p></div>
<div class="m2"><p>تمامش کام دل در نا تمامی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدورت در دلش انبوه گشته</p></div>
<div class="m2"><p>هیولای غم و اندوه گشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دلش را گوئی از جائی خبر بود</p></div>
<div class="m2"><p>که هر کس بود ازو خوشحال تر بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوار شوق مستعجل نمی رفت</p></div>
<div class="m2"><p>قدم می رفت اما دل نمی رفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به هرگامی به دل خون کرد کامی</p></div>
<div class="m2"><p>به سعی از هر قدم در دیده گامی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زدل دور از طرب بیگانه می رفت</p></div>
<div class="m2"><p>تو می گفتی به ماتم خانه می رفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو نیم ره به این اعزاز رفتند</p></div>
<div class="m2"><p>ستادند از قضا و باز رفتند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رسیدند از قضا در تنگنائی</p></div>
<div class="m2"><p>چو دهلیز عدم تاریک جائی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برونی چون درون دخمه تاریک</p></div>
<div class="m2"><p>رهی چون نقب موران تنگ و باریک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هر سویش بلند ایوان قصری</p></div>
<div class="m2"><p>که بودی سایه اش بر طاق کسری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بس طوفان بر او شبنم نشانده</p></div>
<div class="m2"><p>درستی در گل و خشتش نمانده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شکست اندر شکست آن بام و دیوار</p></div>
<div class="m2"><p>به تار عنکبوتش بسته معمار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هوا مزدور پشتی بانی او</p></div>
<div class="m2"><p>نفس معذور در ویرانی او</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درونش همچو بیرون غارت اندای</p></div>
<div class="m2"><p>چو ایوان خیال از هیچ برپای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خروش صور چون از جای جنبید</p></div>
<div class="m2"><p>بنایش چون بنای قبر لرزید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بس زلزال کوس آتشین دم</p></div>
<div class="m2"><p>بنایش چون مقوا ریخت از هم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو از هم ریخت آن فرسوده پیکر</p></div>
<div class="m2"><p>نهان شد زیر هر خشتیش صد سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان با خاک خشتش تخم سر کشت</p></div>
<div class="m2"><p>که خشت از سرندانستی سرازخشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکست آن دخمه چون بر فرق داماد</p></div>
<div class="m2"><p>تو گفتی آسمان بر خاک افتاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خروش از چرخ نیلی پوش برخاست</p></div>
<div class="m2"><p>زهردل صد قیامت جوش برخاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نوای مطربان شد نوحه آهنگ</p></div>
<div class="m2"><p>شکستی گریه ناخن در دل تنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شد از نیرنگ چرخ، سندروسی</p></div>
<div class="m2"><p>عروسی ماتم و ماتم عروسی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عروس چرخ، زال پیر عالم</p></div>
<div class="m2"><p>لباس سور زد در نیل ماتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو در شهر این صدای ناخوش افتاد</p></div>
<div class="m2"><p>همی گفتی که در شهر آتش افتاد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رفیقان پسر سرمست و مجنون</p></div>
<div class="m2"><p>نشسته تا کمر در خاک و در خون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو بدمستان حیرت بزم افلاک</p></div>
<div class="m2"><p>شکسته شیشهٔ اقبال در خاک</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جگر معمول بذل اشک ریزی</p></div>
<div class="m2"><p>نظر مزدور شغل خاک بیزی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بمژگان نقب زن در خاک و درخشت</p></div>
<div class="m2"><p>خبرپرسان که آن تخم اجل کشت؟</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طریق خاکساری پیشه کرده</p></div>
<div class="m2"><p>نظر فرهاد و مژگان تیشه کرده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر خاکی به مژگان بیختندی</p></div>
<div class="m2"><p>به مرگش بر سر خود ریختندی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مژه در خاک چندان غوطه دادند</p></div>
<div class="m2"><p>کز آن کاوش رگ دریا گشادند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو کاوش یافت آن خاک جگر تاب</p></div>
<div class="m2"><p>برون آمد ز خاک آن در سیراب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو آن گوهر زخاک و گل برآمد</p></div>
<div class="m2"><p>گهر از چشم و لعل از دل برآمد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>برآسودند غواصان خاکی</p></div>
<div class="m2"><p>برافزودند بر دل دردناکی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روانش در عماری جای دادند</p></div>
<div class="m2"><p>عماری را چو گل بر سر نهادند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غبار آلوده بردندش مشوش</p></div>
<div class="m2"><p>که گرد از تن بشویندش به آتش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به رسم ملت آتش پرستان</p></div>
<div class="m2"><p>برو سازند آتش باغ و بستان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همان هنگامهٔ دامادیش گرم</p></div>
<div class="m2"><p>همان جشن مبارکبادیش گرم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همان مطرب ز هر سو نغمه پرداز</p></div>
<div class="m2"><p>ز نوشانوش ساقی لب پر آواز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همان شهری تماشایندهٔ او</p></div>
<div class="m2"><p>سر و جان در قفا افکندهٔ او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>همان با کوس و نای و مطرب و نی</p></div>
<div class="m2"><p>همی رفت و جهانی همره وی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عروس شعله شد جانانهٔ او</p></div>
<div class="m2"><p>شد آتشگه عروسی خانهٔ او</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو آن خواب پریشان دید دختر</p></div>
<div class="m2"><p>چو گل بر باد حسرت داد معجر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز عشرتخانه سرمستانه برجست</p></div>
<div class="m2"><p>خسک برپای و آتش بر کف دست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به ناخن برگل روخار بشکست</p></div>
<div class="m2"><p>زسیلی شیشه در گلزار بشکست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به دست خود ز چشم توتیا سای</p></div>
<div class="m2"><p>مژه بر کند چون خار از کف پای</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز بس بارید برگل ابر سیلی</p></div>
<div class="m2"><p>حنا در ناخنش گردید نیلی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز ناخن جوی خون در سینه می بست</p></div>
<div class="m2"><p>ز خون زنگار بر آئینه می بست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به خون از نرگس ترسومه می شست</p></div>
<div class="m2"><p>ز لب جای تبسم زهر می رست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تنش عریان تر از پیراهن گل</p></div>
<div class="m2"><p>گریبان چاک تر از دامن گل</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>برهنه پا و سرچون فتنه مفتون</p></div>
<div class="m2"><p>همی گفتی که دلیلی گشته مجنون</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو رسوایان مادر زاد بی ننگ</p></div>
<div class="m2"><p>به خود در خشم و باناموس در جنگ</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دلش نقش دوئی را بسته برباد</p></div>
<div class="m2"><p>اناالحق گوی واشوقاه داماد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز مستیهای شوق جانسپاری</p></div>
<div class="m2"><p>شده پروانهٔ شمع عماری</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو شب گم کرده راهان مشوش</p></div>
<div class="m2"><p>خرامان شد به استقبال آتش</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ز شوق سوختن در آتش دوست</p></div>
<div class="m2"><p>نمی گنجید همچون شعله در پوست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به خاکستر پرستیدن همی رفت</p></div>
<div class="m2"><p>تو می گفتی به گل چیدن همی رفت</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تمام راه با آتش جدل داشت</p></div>
<div class="m2"><p>پر پروانه گوئی در بغل داشت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو نخل شعله می بالید و می رفت</p></div>
<div class="m2"><p>بر آتش سینه می مالید و می رفت</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>جهانی خانه سوز آه و افسوس</p></div>
<div class="m2"><p>که جوید شیوهٔ پروانه طاووس</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>حکیم و فیلسوف و پیر و دانا</p></div>
<div class="m2"><p>فسون آموز آن دل ناشکیبا</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>که شوقش زان تمنا فرد سازند</p></div>
<div class="m2"><p>به جانش مهر آتش سرد سازند</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>برهمن ملتان بت پرستار</p></div>
<div class="m2"><p>هدایت مرشد ناقوس و زنار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ز هر سو نغمه سنج صد نویدش</p></div>
<div class="m2"><p>تسلی ده ز صد بیم و امیدش</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>ولی او مست آتش آشنائی</p></div>
<div class="m2"><p>زیان نشناس کافر ماجرائی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بگفت ار بت به منع من گراید</p></div>
<div class="m2"><p>سجود او دگر از من نیاید</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>وگر عیسی شکست آرد به کارم</p></div>
<div class="m2"><p>زبان از تهمت مریم ندارم</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>برهمن گر به منعم دیده شوخ است</p></div>
<div class="m2"><p>برهمن نیست او شیخ الشیوخ است</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>وگر مادر به منعم لب بسوزد</p></div>
<div class="m2"><p>جگر بر شعلهٔ یارب بسوزد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>اگر خود چون نخواهم زود میرش</p></div>
<div class="m2"><p>حرامم باد لذتهای شیرش</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کسی را اختیار جان کس نیست</p></div>
<div class="m2"><p>نه خود جان منست این جان کس نیست</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چرا تا زنده ام شرمنده باشم</p></div>
<div class="m2"><p>که سوزد دلبر و من زنده باشم</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>غرض ز آتش مرا ایثار جان است</p></div>
<div class="m2"><p>به غارت دادن بازار جان است</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>من لب تشنه دل، کز جان شدم سیر</p></div>
<div class="m2"><p>اگر آتش نباشد، زهر و شمشیر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ز پندش دل به آتش گرم خون شد</p></div>
<div class="m2"><p>به حکم غیرتش رغبت فزون شد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>مهندس مطربان آتش آموز</p></div>
<div class="m2"><p>زاحکام نصیحت حسرت اندوز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز ناکامی نفس را دود کرده</p></div>
<div class="m2"><p>ره صد چاره را مسدود کرده</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو از هر مکر و حیلت باز رستند</p></div>
<div class="m2"><p>زبان بستند و در ماتم نشستند</p></div></div>