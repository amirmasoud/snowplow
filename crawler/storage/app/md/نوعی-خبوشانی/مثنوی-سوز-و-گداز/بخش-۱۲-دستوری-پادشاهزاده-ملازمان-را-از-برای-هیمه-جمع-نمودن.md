---
title: >-
    بخش ۱۲ - دستوری پادشاهزاده ملازمان را از برای هیمه جمع نمودن
---
# بخش ۱۲ - دستوری پادشاهزاده ملازمان را از برای هیمه جمع نمودن

<div class="b" id="bn1"><div class="m1"><p>اطاعت پیشگان شاهزاده</p></div>
<div class="m2"><p>به طاعت نقد جان بر کف نهاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو از شه نغمهٔ رخصت شنیدند</p></div>
<div class="m2"><p>به سوی هیمه چون آتش دویدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بس چیدند بر هم صندل و عود</p></div>
<div class="m2"><p>جهان پر شد ز دود عنبر آلود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کم از مژگان به هم سودن زمانی</p></div>
<div class="m2"><p>مهیا شد سمندر آشیانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخست آن کشته را در وی نهادند</p></div>
<div class="m2"><p>بخور آسا به مجمر جای دادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه دودش با دماغ دختر آمیخت</p></div>
<div class="m2"><p>شدش جان عطسه و بر خاک ره ریخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سپند آسا به وجد افتاد و برخاست</p></div>
<div class="m2"><p>به شکرش شه زبان چون شعله پیراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفت ای ذره پرور سعد اکبر</p></div>
<div class="m2"><p>شنیدستم زخشت مهر و اختر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل و جانم کرم پروردهٔ تو</p></div>
<div class="m2"><p>من آتش محبت بردهٔ تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو در پاداش احسانت گرایم</p></div>
<div class="m2"><p>اگر سوزم ز خجلت بر نیایم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیالت را درین ره خضر دل کن</p></div>
<div class="m2"><p>مرا امروز در آتش بحل کن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بعد شه وداع یک به یک کرد</p></div>
<div class="m2"><p>دل و چشم جهان کان نمک کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همی رفت و به تحریک زمانی</p></div>
<div class="m2"><p>زغم می سوخت بی آتش جهانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>لب از پان سرخ و چشم از سرمه خونریز</p></div>
<div class="m2"><p>چو یاقوتی شد اندر آتش تیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان مستانه در آتش گذر کرد</p></div>
<div class="m2"><p>که از بد مستیش آتش حذر کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان از شوق دل بی تاب گردید</p></div>
<div class="m2"><p>که از گرمیش آتش آب گردید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو موج افکن شد از طوفان خون ریز</p></div>
<div class="m2"><p>درآمد در میان آتش تیز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آتش همچو صرصر پای کوبان</p></div>
<div class="m2"><p>غبار از خویش و دود از شعله رویان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پایش شعله چون گل بر کف دست</p></div>
<div class="m2"><p>ز خون شعله بر پایش حنابست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن برگ گل نادیده خاشاک</p></div>
<div class="m2"><p>گلاب لاله گون می ریخت بر خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>محیطش گشت آتش با صد افسوس</p></div>
<div class="m2"><p>تن او شمع و آتش گشت فانوس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز خون دل بر آتش روغن افشاند</p></div>
<div class="m2"><p>سپند اشک دامن دامن افشاند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز آتش وعده گاه یار پرسید</p></div>
<div class="m2"><p>سراغ جلوهٔ دیدار پرسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خبر داد آتش از راز درونش</p></div>
<div class="m2"><p>به کوثر گشت آتش رهنمونش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو آگه شد هم از ره بر سرش تافت</p></div>
<div class="m2"><p>نقابش را به بوس ازرو برانداخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سر شوریده بر زانو نهادش</p></div>
<div class="m2"><p>لبش بوسید و رو بر رو نهادش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به مژگان شعله بر پیچید از موی</p></div>
<div class="m2"><p>به خوی شستش غبار آتش از رو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کشیدش تنگتر از جان در آغوش</p></div>
<div class="m2"><p>چو جانان یافت کرده جان فراموش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به نوعی امتزاج آن دو تن شد</p></div>
<div class="m2"><p>که جان این، تن او را کفن شد</p></div></div>