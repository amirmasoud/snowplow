---
title: >-
    بخش ۱۱ - طلب نمودن پادشاه دختر را و انعام نمودن آن
---
# بخش ۱۱ - طلب نمودن پادشاه دختر را و انعام نمودن آن

<div class="b" id="bn1"><div class="m1"><p>طلب کرد آن بت کافر لقب را</p></div>
<div class="m2"><p>به کوثر بار داد آن تشنه لب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فرمان شه آمد آتش آلود</p></div>
<div class="m2"><p>چو سرکش شعله ای پیچیده دردود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرامان شد چو گل بر تخت آتش</p></div>
<div class="m2"><p>به دستی جان به دستی لخت آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قد چون شعله بر تعظیم خم داد</p></div>
<div class="m2"><p>زمین سجده را فیض ارم داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شه از لطفش به پای تخت بنشاند</p></div>
<div class="m2"><p>جواهرهای لب بر فرقش افشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشیدش از نوازش دست بر سر</p></div>
<div class="m2"><p>سر او تخت و دست شاه افسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تسلی دادش از مسکین نوازی</p></div>
<div class="m2"><p>به شیرین بزمهای لعب و بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به فرزندی خود داد اختصاصش</p></div>
<div class="m2"><p>به عصمتگاه خلوت کرد خاصش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به هر کشور خطاب را نیش داد</p></div>
<div class="m2"><p>به ملک هند فرمان را نیش داد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزارش اسب تازی داد و صد فیل</p></div>
<div class="m2"><p>متاع خزو دیبا میل در میل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هزارش از کنیزان ختائی</p></div>
<div class="m2"><p>دماغ آزاد خوی آشنایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزارش حقه از یاقوت و گوهر</p></div>
<div class="m2"><p>هزارش نافه پر از مشک اذفر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز هر جنسیش از مه تا به ماهی</p></div>
<div class="m2"><p>کرامت کرد غیر از پادشاهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و لیکن آن زن مردانه صولت</p></div>
<div class="m2"><p>شکر لب طوطی پروانه همت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز صد عالم تمنا بر تمنا</p></div>
<div class="m2"><p>نمی شد جز به جان دادن تسلا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لبش جز گوهر آتش نمی سفت</p></div>
<div class="m2"><p>به غیر از سوختن حرفی نمی گفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو عاجز شد شه از دلجوئی او</p></div>
<div class="m2"><p>عنان بر تافت ز آتش خویی او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اجازت گونه ای دادش نه از دل</p></div>
<div class="m2"><p>ز شادی برپرید آن نیم بسمل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هنوز از حرف رخصت لب تهی جام</p></div>
<div class="m2"><p>که شوقش بود مرغ شعله آشام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لبش با شاه در افسانه گفتن</p></div>
<div class="m2"><p>مژه سرگرم آتشخانه رفتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به آخر آن سپهر دانش و داد</p></div>
<div class="m2"><p>قرار چاره بر بیچارگی داد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اشارت کرد با پور جوان بخت</p></div>
<div class="m2"><p>که ای چشم و چراغ افسر و تخت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ببر این شعله را تا کان آتش</p></div>
<div class="m2"><p>بیفکن آتشی در جان آتش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دلجوئیش چون شیر و شکر شو</p></div>
<div class="m2"><p>چو خورشیدش به آتش راهبر شو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر نرمی پذیرد یاورش باش</p></div>
<div class="m2"><p>وگر سوزد در آتش بر سرش باش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به خرمن عود و صندل بر فروزان</p></div>
<div class="m2"><p>به رسم دخت رایانش بسوزان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گل بخت و بهارستان اقبال</p></div>
<div class="m2"><p>مراد انس و جان شهزاده دانیال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چراغ دودمان شهریاری</p></div>
<div class="m2"><p>فروغ جبههٔ امیدواری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به حکم شاه فرمان تماشا</p></div>
<div class="m2"><p>روان شد همره آن ناشکیبا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جهانی کرده وقف از هر کناره</p></div>
<div class="m2"><p>متاع جان به تاراج نظاره</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شهش در هر نظر دادی پیامی</p></div>
<div class="m2"><p>به هرگامی روا کردیش کامی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تمام ره برو افسانه می خواند</p></div>
<div class="m2"><p>دلش می داد و رخش آهسته می راند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ولی او از دوعالم بیخبر بود</p></div>
<div class="m2"><p>به جانش شوق آتش کارگر بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به افسون، را دل نرمی نمی شد</p></div>
<div class="m2"><p>هوس دل سرد آن گرمی نمی شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به جان آمد زبس افسون پرستی</p></div>
<div class="m2"><p>فغان برداشت از وسواس هستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به شه گفتا مرا بد نام کردی</p></div>
<div class="m2"><p>به افسون روز عیشم شام کردی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز صبرم رنجه خواهد گشت یارم</p></div>
<div class="m2"><p>بخواهد مرد آتش ز انتظارم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دلم سرگرم واشوقاه عشق است</p></div>
<div class="m2"><p>بن هر مویم آتشگاه عشق است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>من آن خاکستر آتش نهادم</p></div>
<div class="m2"><p>که از بال و پرپروانه زادم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر صد ره شوم از سوختن پست</p></div>
<div class="m2"><p>همان بازم به اصل خود رجوع است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به نزد عشق هر کو اهل عشق است</p></div>
<div class="m2"><p>به آتش زنده رفتن سهل عشق است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به آخر چون شه از خجلت فروماند</p></div>
<div class="m2"><p>گلاب یاس بر سوز دل افشاند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اجازت داد کاتش بر فروزند</p></div>
<div class="m2"><p>در آتش هر دو را با هم بسوزند</p></div></div>