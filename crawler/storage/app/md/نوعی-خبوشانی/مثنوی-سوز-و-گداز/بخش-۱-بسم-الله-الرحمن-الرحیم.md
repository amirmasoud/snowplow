---
title: >-
    بخش ۱ - بسم الله الرحمن الرحیم
---
# بخش ۱ - بسم الله الرحمن الرحیم

<div class="b" id="bn1"><div class="m1"><p>الهی خنده ام را نالگی ده</p></div>
<div class="m2"><p>سرشکم را جگر پر کالگی ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نفس را جلوهٔ آه جگر بخش</p></div>
<div class="m2"><p>نظر را سوی خود راه سفر بخش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجز عشق ار همه وحی است و اعجاز</p></div>
<div class="m2"><p>از او خلوت گه دل را بپرداز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم را عندلیب آوازه گردان</p></div>
<div class="m2"><p>گل باغم به آتش تازه گردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شوقم ده از پیمانهٔ عشق</p></div>
<div class="m2"><p>که جوشد برلبم پروانهٔ عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آتش آب ده تیغ زبانم</p></div>
<div class="m2"><p>که جز حمدت نروید از بیانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نخل ایمنم ده خانهٔ حمد</p></div>
<div class="m2"><p>که آرایم به نامت نامه ای چند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگیر از لطف پس با خامه دستم</p></div>
<div class="m2"><p>که طفل خامه بر کاغذ شکستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صریر خامه ام را لحن نی کن</p></div>
<div class="m2"><p>سخن را چاشنی مهمان می کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلامم را ده از عزت خطابی</p></div>
<div class="m2"><p>بلند افسر کن از ام الکتابی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پا انداز حمدت کارجمند است</p></div>
<div class="m2"><p>زبان با دل پرند اندر پرند است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی پائی که بر گل ناز دارد</p></div>
<div class="m2"><p>کجا پروای پا انداز دارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>من و حمدت زبان را خاک بر سر</p></div>
<div class="m2"><p>ادب را درع طاقت چاک در بر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سزاوارت ادای چون منی نیست</p></div>
<div class="m2"><p>که حمد تو سزای چون منی نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گاه خامشی محشر خروشم</p></div>
<div class="m2"><p>چوشد وقت سخن صید خموشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبان شوریده ای گنگانه نطقم</p></div>
<div class="m2"><p>فصاحت زاده ای دیوانه نطقم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>من و یارای حمد از من نیاید</p></div>
<div class="m2"><p>که پاس شعله از خرمن نیاید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان بهتر چو عجزم خامه بشکست</p></div>
<div class="m2"><p>که هم در عرض حال خود زنم دست</p></div></div>