---
title: >-
    بخش ۱۵ - در تمثیل مناظرهٔ ماهی و سمندر با یکدیگر
---
# بخش ۱۵ - در تمثیل مناظرهٔ ماهی و سمندر با یکدیگر

<div class="b" id="bn1"><div class="m1"><p>به ماهی طعنه زد روزی سمندر</p></div>
<div class="m2"><p>که تو ممنون ز آبی من ز آذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دلسردی است با آبت سر و کار</p></div>
<div class="m2"><p>من از دل گرمیم آتش پرستار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مزاج اهل عشق آتش نورد است</p></div>
<div class="m2"><p>ز آتش رو نتابد هرکه مرد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو کاتش از نقاب ما نه بینی</p></div>
<div class="m2"><p>عدم از بیم بر هستی گزینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آن خضرم کز استیلای امید</p></div>
<div class="m2"><p>در آتش سوختم با عمر جاوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خلد آباد راحت رخت بستم</p></div>
<div class="m2"><p>چو داغ عشق در آتش نشستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو ماهی این نوای طعنه بشنید</p></div>
<div class="m2"><p>صدف از تاب حسنش تابه گردید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فروزان شد ز سر تا پا چو اخگر</p></div>
<div class="m2"><p>زبان شد شعله و حرفش سمندر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگفت ای خام طبع خام گفتار</p></div>
<div class="m2"><p>ز بس خامیت با آتش سر و کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو از دلسردی و افسرده جانی</p></div>
<div class="m2"><p>کنی دایم در آتش زندگانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس سردست اجزای وجودت</p></div>
<div class="m2"><p>نبیند چشم آتش روی دورت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه سردی در تو آتش کارگر نیست</p></div>
<div class="m2"><p>همی سوزی و از سوزت خبر نیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر صد سال در آتش نشینی</p></div>
<div class="m2"><p>به طبعت یک شرر گرمی نبینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همان جسمت زسردی ناگزیر است</p></div>
<div class="m2"><p>همان در طبعت آتش زمهریر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوشا من کز تف و تاب محبت</p></div>
<div class="m2"><p>شدم غواص غرقاب محبت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>من آن برقم که با باران نشینم</p></div>
<div class="m2"><p>ز حرقت آب بر آتش گزینم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز سوز عشق دارم در جگر تاب</p></div>
<div class="m2"><p>بسوزم گر نسایم سینه بر آب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وصال آب شد هستی فروزم</p></div>
<div class="m2"><p>گر از دریا برون افتم بسوزم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرشتند آتشی در سینهٔ من</p></div>
<div class="m2"><p>که کردم قعر دریا بیخ گلخن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به آبم داد بخت سرکش من</p></div>
<div class="m2"><p>به صد طوفان نمیرد آتش من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر صد سال گردم غوطه پرورد</p></div>
<div class="m2"><p>سر موئی نگردد آتشم سرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وگر در دل بدزدم در جگر تاب</p></div>
<div class="m2"><p>ز سوز سینه خاکستر کنم آب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو از سردی کنی آتش پرستی</p></div>
<div class="m2"><p>من از گرمی به طوفان داده هستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ترا بامن نه لاف عشق نیکوست</p></div>
<div class="m2"><p>بر این فتوی نویسد دشمن [و] دوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به کیش عشق از من تا تو فرق است</p></div>
<div class="m2"><p>کسی داند که در خوناب غرق است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تو سوز عاشقی از من بیاموز</p></div>
<div class="m2"><p>چراغ خود ز آب من برافروز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الهی تافروزان است ز آذر</p></div>
<div class="m2"><p>چراغ ماهی و شمع سمندر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فروزان باد از مه تا به ماهی</p></div>
<div class="m2"><p>چراغ دولت دانیال شاهی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نهال شمع بزمش باد شاداب</p></div>
<div class="m2"><p>چراغ مجلسش باد آسمان تاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کند طغرای فرمانش منقش</p></div>
<div class="m2"><p>چو باد از جلوه روی آب و آتش</p></div></div>