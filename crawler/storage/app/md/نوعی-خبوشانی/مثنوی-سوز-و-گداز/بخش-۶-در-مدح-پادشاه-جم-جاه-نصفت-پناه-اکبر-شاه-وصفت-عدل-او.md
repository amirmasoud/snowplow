---
title: >-
    بخش ۶ - در مدح پادشاه جم جاه نصفت پناه اکبر شاه وصفت عدل او
---
# بخش ۶ - در مدح پادشاه جم جاه نصفت پناه اکبر شاه وصفت عدل او

<div class="b" id="bn1"><div class="m1"><p>زبان شوریده کلک شعله تحریر</p></div>
<div class="m2"><p>چنین کرد از زبان شعله تقریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در دوران شاه عیسی اورنگ</p></div>
<div class="m2"><p>که عیسی خواند پیشش درس فرهنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان کیوان خدیو عدل و انصاف</p></div>
<div class="m2"><p>اطاعت سنج امرش قاف تا قاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک قدری عطارد خیل تاشی</p></div>
<div class="m2"><p>قیامت از شکوهش دور باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تیغ صبحگاه و آه شبگیر</p></div>
<div class="m2"><p>زمین و آسمان را کرده تسخیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرامی گوهر نه بحر اخضر</p></div>
<div class="m2"><p>مسمی ذوالجلال الله اکبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرد کامل ترین حق شناسان</p></div>
<div class="m2"><p>سپاس آموزگار نا سپاسان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شاهی خوی درویشان گرفته</p></div>
<div class="m2"><p>طریق رهنما کیشان گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر موری شدی از فتنه پامال</p></div>
<div class="m2"><p>ز بازوی هما دادی پروبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگر خاری زدی برپای کس نیش</p></div>
<div class="m2"><p>به دست خویش بردی مرهمش پیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عهدش طفل نومیدی نزاده</p></div>
<div class="m2"><p>و گر هم زاده جان در راه داده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان آسوده عهدش از حوادث</p></div>
<div class="m2"><p>که در مستی نگشتی فتنه حادث</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جوانی زادش از عهد می اندود</p></div>
<div class="m2"><p>تو گفتی وصل یوسف عهد او بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهشتی بود عهد ناشناسی</p></div>
<div class="m2"><p>زبان پرمدح در دل ناسپاسی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمین شوره هرجا ابر می شست</p></div>
<div class="m2"><p>بغیر از شکر شکرش نمی رست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نبودی در چنین خرم بهاری</p></div>
<div class="m2"><p>مقیم خاک را در دل غباری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بجز نوعی که از ناکس نهادی</p></div>
<div class="m2"><p>مرادش شد شهید نامرادی</p></div></div>