---
title: >-
    بخش ۱۰ - خبر یافتن پادشاه و طلب نمودن دختر را و منع کردن و قبول نکردن او
---
# بخش ۱۰ - خبر یافتن پادشاه و طلب نمودن دختر را و منع کردن و قبول نکردن او

<div class="b" id="bn1"><div class="m1"><p>چمن پیرای این آتش هوا باغ</p></div>
<div class="m2"><p>نمک سود این چنین سازد گل داغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون این قصه در عالم سمرشد</p></div>
<div class="m2"><p>شه کار آزمایان را خبر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که دلکش دختری نادیده ایام</p></div>
<div class="m2"><p>لب از جلاب طفلی شیر آشام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درش در طبع نیسان قطره مانده</p></div>
<div class="m2"><p>صدف را حسرتش در خون نشانده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو گل در مهد عصمت پروریده</p></div>
<div class="m2"><p>نسیم دیده در وی نا دمیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز از شیر طفلی لب نشسته</p></div>
<div class="m2"><p>هنوز از صد گلش یک گل نرسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای تیره روزی شور بختی</p></div>
<div class="m2"><p>که در وصلش نیاسودست لختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گزیده بر دوعالم سوختن را</p></div>
<div class="m2"><p>شده آماده خاکستر شدن را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شوق دل بود جان خرابش</p></div>
<div class="m2"><p>کباب آتش و آتش کبابش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو طفلان گرم آتشبازی عشق</p></div>
<div class="m2"><p>قدم بر جای دست اندازی عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به منع هیچ کس سر در نیارد</p></div>
<div class="m2"><p>چو آتش از کسی پروا ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مزاجش را هوای جان مضر شد</p></div>
<div class="m2"><p>علاجش هم به آتش منحصر شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو شاه این ماجرا بشنید بگریست</p></div>
<div class="m2"><p>که عشقا این همه کافر دلی چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مروت دشمنا با او چه داری</p></div>
<div class="m2"><p>به آن ریحان آتشبو چه داری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جوان مرد آنکه با مردان ستیزد</p></div>
<div class="m2"><p>بجز ننگ از نبرد زن چه خیزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر مردی تو با نوعی در آمیز</p></div>
<div class="m2"><p>کف خونش به خاکستر بر آمیز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز غیرت مندی آن ناتوان دل</p></div>
<div class="m2"><p>چو آتش گشت شاه مهربان دل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شکوهش با ترحم آشنا شد</p></div>
<div class="m2"><p>به حکم امتحان فرمان روا شد</p></div></div>