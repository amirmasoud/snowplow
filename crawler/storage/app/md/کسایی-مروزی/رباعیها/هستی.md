---
title: >-
    هستی
---
# هستی

<div class="b" id="bn1"><div class="m1"><p>این هستی تو ، هستی هست دگر است</p></div>
<div class="m2"><p>وین مستی تو ، مستی مست دگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو ، سر به گریبان تفکر درکش</p></div>
<div class="m2"><p>کاین دست تو ، آستین دست دگر است</p></div></div>