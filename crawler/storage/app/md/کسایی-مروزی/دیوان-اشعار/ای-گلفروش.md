---
title: >-
    ای گلفروش ...
---
# ای گلفروش ...

<div class="b" id="bn1"><div class="m1"><p>گل نعمتی است هدیه فرستاده از بهشت</p></div>
<div class="m2"><p>مردم کریم تر شود اندر نعیم گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گلفروش ، گل چه فروشی برای سیم</p></div>
<div class="m2"><p>وز گل عزیزتر ، چه ستانی به سیم گل ؟</p></div></div>