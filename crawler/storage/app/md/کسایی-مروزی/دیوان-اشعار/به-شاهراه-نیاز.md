---
title: >-
    به شاهراه نیاز
---
# به شاهراه نیاز

<div class="b" id="bn1"><div class="m1"><p>به شاهراه نیاز اندرون سفر مسگال</p></div>
<div class="m2"><p>که مرد کوفته گردد بدان ره اندر سخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وگر خلاف کنی طمع را و هم بروی</p></div>
<div class="m2"><p>بدرّد ار به مَثَل آهنین بود هملخت</p></div></div>