---
title: >-
    دیده و اشک
---
# دیده و اشک

<div class="b" id="bn1"><div class="m1"><p>دو دیدهٔ من و از دیده اشک دیدهٔ من</p></div>
<div class="m2"><p>میان دیده و مژگان ستاره وار پدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جَزع ماند یک بر دگر سپید و سیاه</p></div>
<div class="m2"><p>به رشته کرده همه گرد جَزع مروارید</p></div></div>