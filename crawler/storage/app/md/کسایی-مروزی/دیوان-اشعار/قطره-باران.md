---
title: >-
    قطرهٔ باران
---
# قطرهٔ باران

<div class="b" id="bn1"><div class="m1"><p>بر پیلگوش قطرهٔ باران نگاه کن</p></div>
<div class="m2"><p>چون اشک چشم عاشق گریان همی شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که پرّ باز سپید است برگ او</p></div>
<div class="m2"><p>منقار باز ، لؤلؤ ناسفته بر چده</p></div></div>