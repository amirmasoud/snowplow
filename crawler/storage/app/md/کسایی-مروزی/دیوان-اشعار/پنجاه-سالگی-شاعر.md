---
title: >-
    پنجاه سالگی شاعر
---
# پنجاه سالگی شاعر

<div class="b" id="bn1"><div class="m1"><p>به سیصد و چهل یک رسید نوبت سال</p></div>
<div class="m2"><p>چهارشنبه و سه روز باقی از شوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیامدم به جهان تا چه گویم و چه کنم</p></div>
<div class="m2"><p>سرود گویم و شادی کنم به نعمت و مال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستوروار بدین سان گذاشتم همه عمر</p></div>
<div class="m2"><p>که بَرده گشتهٔ فرزندم و اسیر عیال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به کف چه دارم از این پنجَه شمرده تمام</p></div>
<div class="m2"><p>شمارنامهٔ با صدهزار گونه وبال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من این شمار آخر چگونه فصل کنم</p></div>
<div class="m2"><p>که ابتداش دروغ است و انتهاش مُحال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درم خریدهٔ آزم ، ستم رسیدهٔ حرص</p></div>
<div class="m2"><p>نشانهٔ حَدَثانم ، شکار ذلّ سؤال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ فر جوانی ، دریغ عمر لطیف</p></div>
<div class="m2"><p>دریغ صورت نیکو ، دریغ حسن و جمال !</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کجا شد آن همه خوبی ، کجا شد آن همه عشق ؟</p></div>
<div class="m2"><p>کجا شد آن همه نیرو ، کجا شد آن همه حال ؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرم به گونهٔ شیر است و دل به گونهٔ قیر</p></div>
<div class="m2"><p>رخم به گونهٔ نیل است و تن به گونهٔ نال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهیب مرگ بلرزاندم همی شب و روز</p></div>
<div class="m2"><p>چو کودکان بدآموز را نهیب دوال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گذاشتیم و گذشتیم و بودنی همه بود</p></div>
<div class="m2"><p>شدیم و شد سخن ما فسانهٔ اطفال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایا کسایی ، پنجاه بر تو پَنجه گذاشت</p></div>
<div class="m2"><p>بکند بال تو را زخم پنجه و چنگال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو گر به مال و اَمَل بیش از این نداری میل</p></div>
<div class="m2"><p>جدا شو از امل و گوش ِ وقت ِ خویش بمال</p></div></div>