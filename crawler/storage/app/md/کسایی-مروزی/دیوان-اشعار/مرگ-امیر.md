---
title: >-
    مرگ امیر
---
# مرگ امیر

<div class="b" id="bn1"><div class="m1"><p>آن کس که بر امیر در مرگ باز کرد</p></div>
<div class="m2"><p>بر خویشتن نگر نتواند فراز کرد</p></div></div>