---
title: >-
    گازُر
---
# گازُر

<div class="b" id="bn1"><div class="m1"><p>کوی و جوی از تو کوثر و فردوس</p></div>
<div class="m2"><p>دل و جامه ز تو سیاه و سپید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ تو هست مایهٔ تو ، اگر</p></div>
<div class="m2"><p>مایهٔ گازران بود خورشید</p></div></div>