---
title: >-
    پیری
---
# پیری

<div class="b" id="bn1"><div class="m1"><p>پیری مرا به زرگری افگند ، ای شگفت</p></div>
<div class="m2"><p>بی گاه دود ، زردم و همواره سُرف سُرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زرگر فرو فشاند کُرف سیه به سیم</p></div>
<div class="m2"><p>من باز برفشانم سیم سره به کُرف</p></div></div>