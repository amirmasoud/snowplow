---
title: >-
    آسیای زمانه
---
# آسیای زمانه

<div class="b" id="bn1"><div class="m1"><p>آس شدم زیر آسیای زمانه</p></div>
<div class="m2"><p>نیسته خواهم شدن همی به کرانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاد همی ساز و شغل خویش همی بر</p></div>
<div class="m2"><p>چند بری شغل نای و چنگ و چغانه</p></div></div>