---
title: >-
    شعر و غزل
---
# شعر و غزل

<div class="b" id="bn1"><div class="m1"><p>ای آنکه جز از شعر و غزل هیچ نخوانی</p></div>
<div class="m2"><p>هرگز نکنی سیر دل از تُنبُل و ترفند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیبا بود ار مرو بنازد به کسایی</p></div>
<div class="m2"><p>چونانکه جهان جمله به استاد سمرقند</p></div></div>