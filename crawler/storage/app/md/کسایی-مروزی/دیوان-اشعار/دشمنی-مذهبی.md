---
title: >-
    دشمنی مذهبی
---
# دشمنی مذهبی

<div class="b" id="bn1"><div class="m1"><p>هیچ نپذیری چون ز آل نبی باشد مرد</p></div>
<div class="m2"><p>زود بخروشی و گویی نه صواب است ، خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی گمان ، گفتن تو باز نماید که تو را</p></div>
<div class="m2"><p>به دل اندر غضب و دشمنی آل عباست</p></div></div>