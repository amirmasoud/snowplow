---
title: >-
    خواری مُرده
---
# خواری مُرده

<div class="b" id="bn1"><div class="m1"><p>دانم که هیچ کس نکند مرثیت مرا</p></div>
<div class="m2"><p>دانم که مرده بر دل میراثخوار ، خوار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرزند من یتیم و سر افکنده گرد کوی</p></div>
<div class="m2"><p>جامه وَسَخ گرفته و در خاک ، خاکسار</p></div></div>