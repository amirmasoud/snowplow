---
title: >-
    خضاب شاعر
---
# خضاب شاعر

<div class="b" id="bn1"><div class="m1"><p>از خضاب من و از موی سیه کردن من</p></div>
<div class="m2"><p>گر همی رنج خوری، بیش مخور، رنج مبر!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غرضم زو نه جوانی است؛ بترسم که زِ من</p></div>
<div class="m2"><p>خردِ پیران جویند و نیابند مگر!</p></div></div>