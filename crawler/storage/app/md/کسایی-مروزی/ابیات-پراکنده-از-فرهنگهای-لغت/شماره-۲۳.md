---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ای دریغا که مورد زار مرا</p></div>
<div class="m2"><p>ناگهان باز خورد برف ِ وغیش</p></div></div>