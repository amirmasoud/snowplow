---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>آنکه نداند همی سرود ز یاسین</p></div>
<div class="m2"><p>گیرَخ و گلدانش خسروانی بینی</p></div></div>