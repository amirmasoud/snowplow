---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>نان سیاه و خوردی بی چَربو</p></div>
<div class="m2"><p>و آنگاه مَه به مَه بود این هر دو</p></div></div>