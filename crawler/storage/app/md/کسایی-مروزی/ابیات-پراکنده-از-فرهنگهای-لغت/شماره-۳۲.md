---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>نسوز نامرده ، ای شگفتی کار</p></div>
<div class="m2"><p>راست با مردگان بگونه شدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوب گر سوی ما نگه نکند</p></div>
<div class="m2"><p>گو مکن ، شو که ما نمونه شدیم</p></div></div>