---
title: >-
    شمارهٔ  ۵
---
# شمارهٔ  ۵

<div class="b" id="bn1"><div class="m1"><p>چون که یکی تاج و بَساک ملوک</p></div>
<div class="m2"><p>باز یکی کوفتهٔ آسیاست</p></div></div>