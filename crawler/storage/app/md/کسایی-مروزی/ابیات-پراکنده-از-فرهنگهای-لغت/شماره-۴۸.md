---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>درهٔ من شده ست از نعمت</p></div>
<div class="m2"><p>چون زنخدان خصم پر غَدره</p></div></div>