---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>تا مهره اشعار ترا نخ کردم</p></div>
<div class="m2"><p>مردم ز بس آفرین و بخ بخ کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این معجزه بس بود ز شعر تو که من،</p></div>
<div class="m2"><p>در فصل تموز شهر ری یخ کردم</p></div></div>