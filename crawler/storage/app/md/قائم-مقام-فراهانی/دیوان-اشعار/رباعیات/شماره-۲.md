---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>این شعر بود که جان از او در تعب است</p></div>
<div class="m2"><p>یا ثالث بوی سیر و دود شطب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون میوه ری مایه لرزست، ولی</p></div>
<div class="m2"><p>لرز عجبی که مرگش از پی نه تب است</p></div></div>