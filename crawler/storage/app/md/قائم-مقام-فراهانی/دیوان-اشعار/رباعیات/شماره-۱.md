---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گفتی که نشد خوب که گشتی مغضوب</p></div>
<div class="m2"><p>بد شد که به شاه از تو شمردند عیوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خواجه: ترا چه با من و خواجه من؟</p></div>
<div class="m2"><p>من دانم و وان که بد کند با من و خوب</p></div></div>