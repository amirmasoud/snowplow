---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>رشتی علی از حجره سوی دشت مرو</p></div>
<div class="m2"><p>با ساده رخان جانب گلگشت مرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تبریز نشین و درس خوان، آدم شو</p></div>
<div class="m2"><p>سنگین بنشین، سبک مشو، رشت مرو</p></div></div>