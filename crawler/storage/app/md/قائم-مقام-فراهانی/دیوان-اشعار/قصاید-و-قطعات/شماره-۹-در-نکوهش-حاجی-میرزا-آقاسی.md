---
title: >-
    شمارهٔ  ۹ - در نکوهش حاجی میرزا آقاسی
---
# شمارهٔ  ۹ - در نکوهش حاجی میرزا آقاسی

<div class="b" id="bn1"><div class="m1"><p>زاهد چه بلائی تو که این رشته تسبیح</p></div>
<div class="m2"><p>از دست تو سوراخ به سوراخ گریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق ار همه دنبال تو افتند عجب نیست</p></div>
<div class="m2"><p>یک بره ندیدم که ز سلاخ گریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حرف از دهن تست کزین سان بجهد تیز</p></div>
<div class="m2"><p>یا تیز که از معده نفاخ گریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کو به تو همسایه شود در چمن خلد</p></div>
<div class="m2"><p>از جنت و از چشمه نضاخ گریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنی تو که چون نظم دری خوانی و تازی</p></div>
<div class="m2"><p>نظم از سخن عمعق و شماخ گریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من از تو گریزانم ازیرا که روا نیست</p></div>
<div class="m2"><p>گر صاحب تقوی نه ز اوساخ گریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ورنه نتوان گفت که در جرگه شاهان</p></div>
<div class="m2"><p>شاهین ز حمامات و زافراخ گریزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مذهب من از سگ گر باشد کمتر</p></div>
<div class="m2"><p>شیری که چو گاوش بزند شاخ گریزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مردی که زصد تیزی صمصام نترسد</p></div>
<div class="m2"><p>شاید که از یک ریزه صملاخ گریزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن غوک غدیرست که از روده بترسد</p></div>
<div class="m2"><p>وان موش بیابان که ز سلاخ گریزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وان دل که زصد نرگس جماش نه لغزد</p></div>
<div class="m2"><p>باشد که ز یک ناکس جماخ گریزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نبود عجب از مرد کشاور که به دی ماه</p></div>
<div class="m2"><p>از باغ برون آید و در کاخ گریزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس راکب و راجل که چو دی در رسد از دشت</p></div>
<div class="m2"><p>زی شهر به شملال و به شرواخ گریزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بلبل که بود عاشق رخسار گل از گل</p></div>
<div class="m2"><p>در باغ شود زاغ چو گستاخ گریزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سار است و چکاوک که ز بستان به زمستان</p></div>
<div class="m2"><p>هم چون ملخ از بدوی ملاخ گریزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با این همه عبدی که به مولا بودش انس</p></div>
<div class="m2"><p>بالله که به صد ناله و صد آخ گریزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>برفاخته نسبت نه توان داد که آسان</p></div>
<div class="m2"><p>از جلوه گه سرو به جلواخ گریزد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرغی که همه ساله خورد دانه ز یک تاک</p></div>
<div class="m2"><p>حاشا که ز عنقود وز شمراخ گریزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون باد خزان بار رزان جمله فرو ریخت</p></div>
<div class="m2"><p>آسیمه به هر لانه و هر لاخ گریزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی چاره چوزین باغ به در راه ندارد</p></div>
<div class="m2"><p>ناچار ازین شاخ به آن شاخ گریزد</p></div></div>