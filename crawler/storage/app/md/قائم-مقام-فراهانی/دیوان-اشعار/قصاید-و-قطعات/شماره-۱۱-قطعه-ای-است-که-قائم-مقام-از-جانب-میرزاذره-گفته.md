---
title: >-
    شمارهٔ  ۱۱ - قطعه ای است که قائم مقام از جانب میرزاذره گفته
---
# شمارهٔ  ۱۱ - قطعه ای است که قائم مقام از جانب میرزاذره گفته

<div class="b" id="bn1"><div class="m1"><p>خسروا! ای آن که خدام درت از یک نظر</p></div>
<div class="m2"><p>ذره را برتر ز خورشید جهان آرا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا از لای نفی مردمی باشد سخن</p></div>
<div class="m2"><p>قامت ذات ترا پیرایه از الا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر ترا فر سکندر داد یزدان از ازل</p></div>
<div class="m2"><p>دیگران گر خویشتن را خود لقب دارا کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیستند این خودپسندان کار زوی هم سری</p></div>
<div class="m2"><p>با غلامان رکاب حضرت والا کنند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغ تو بنیاد خصم از ملک دنیا برفکند</p></div>
<div class="m2"><p>کاین فقیران راحتی در ساحت دنیا کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بالله از انصاف باشد خود گنه از تیغ تست</p></div>
<div class="m2"><p>گر غنی گردند و بر تو عرض استغنا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر، نه بودی تیغ تو اینان کجا پیدا بدند</p></div>
<div class="m2"><p>کاین همه باد و بروت و عرضه را پیدا کنند؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غارتی کاکنون به بنگاه رعایا می کنند</p></div>
<div class="m2"><p>چون تو بایستی که بر لشکرگه اعدا کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لشکر اعدا بهل، اینان که من شان دیده ام</p></div>
<div class="m2"><p>کافرم گر حمله جز بر پشمک و حلوا کنند!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون تو نه نشاندی به جای خویششان اکنون به جاست</p></div>
<div class="m2"><p>گر زجا خیزند و هر دم دعوی بی جا کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بحثی ار باشد به تیغ تست و سرهنگان تو</p></div>
<div class="m2"><p>زو همی ترسند و بحث بی جهت بر پا کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خود گناه ما چه بود آخر که فراشان تو</p></div>
<div class="m2"><p>چوب و بند آرند و پای بنده را بالا کنند؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وان گهی ناپاک زادی را که اصل فتنه اوست</p></div>
<div class="m2"><p>قد نازیبا طراز خلعت دیبا کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پای او بایست بالا کرد و دست ذره را</p></div>
<div class="m2"><p>شایدی از گنج شه پر لولو لالا کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ایزد آنان را جزا بدهد که زیبا را چنین</p></div>
<div class="m2"><p>بی جهت پیش تو زشت و زشت را زیبا کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آه ازین اخوان که خود قصد برادر چون کنند</p></div>
<div class="m2"><p>باز خود در ماتمش افغان و واویلا کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یوسف صدیق را خود در تک چاه افکنند</p></div>
<div class="m2"><p>پیش یعقوب حزین بس شیون و غوغا کنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم رکابان من ار این قوم کافر نعمتند</p></div>
<div class="m2"><p>بالله از من بوالعجب تر خود بسی پیدا کنند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با وجود بوتراب بن ابی قحافه را</p></div>
<div class="m2"><p>در جهان، قائم مقام سید بطحا کنند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>میل جنسیت به بین کاین قوم نادان تا چه حد</p></div>
<div class="m2"><p>عظم بر نادان نهند و ظلم بر دانا کنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا یکی گوساله بر پا خیزد و بانگی کند</p></div>
<div class="m2"><p>دین او گیرند و نقض بیعت موسی کنند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عیسی بی چاره گر یک دم فرود آید ز خر</p></div>
<div class="m2"><p>رو، به خر آرند چست و پشت بر عیسی کنند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بس چراغ بی فروغ از روغن لاف و دروغ</p></div>
<div class="m2"><p>بر فروزند و عدیل مشعل بیضا کنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صد اساس بی ثبات از کذب ومین و ترهات</p></div>
<div class="m2"><p>بهر هر بی چاره در هر ساعتی بر پا کنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یک دو جوز پوچ اگر آید به کفشان از نشاط</p></div>
<div class="m2"><p>پای کوبان، کف زنان، صد فخر بر جوزا کنند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بالله ار این قوم نادان فرق گوهر از خزف</p></div>
<div class="m2"><p>یا زمرد از علف، یا خار از خرما کنند؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گاه چون من چاکری مداح و خدمت کار را</p></div>
<div class="m2"><p>بی گنه بر درگهت مستوجب یاسا کنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گاه زنگانی جهودی را که از اعدام بود</p></div>
<div class="m2"><p>در وجود آرند و شمع مجمع شورا کنند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس چنان در جوف او باد مکاید در، دمند</p></div>
<div class="m2"><p>کاهل نوبت خانه دم اندر دم سرنا کنند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا به زرق و شید ادنی مدبر مطرود را</p></div>
<div class="m2"><p>در خور قرب بساط بزم اوادنی کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رانده در گاه حق ابلیس بر تلبیس را</p></div>
<div class="m2"><p>عارج معراج اوج مسجد اقصی کنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دعوت باغ شمال اندر شب قدر وصال</p></div>
<div class="m2"><p>ثانی اثنین حدیث لیله اسری کنند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نیستند ار سامری در ساحری پس این گروه</p></div>
<div class="m2"><p>از چه نطق اعجم گوساله را گویا کنند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ورنه اعجاز مسیح آورده اند آخر چه سان</p></div>
<div class="m2"><p>مرده پژمرده صد ساله را احیا کنند؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ورنه شیادند بایستی کزان ده روزه حرف</p></div>
<div class="m2"><p>هر یکی خود را به عدل و راستی هم تا کنند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وعده ها را گر وفا بودی کنون بایست دید</p></div>
<div class="m2"><p>کاندرین هنگام چون هنگامه و غوغا کنند؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در بر عرش جلال اندر احادیث طوال</p></div>
<div class="m2"><p>عرض خدمت ها دهند و وضع منت ها کنند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لیک اکنون زان چه گفتند و شنیدیم و گذشت</p></div>
<div class="m2"><p>خامشی گیرند پیش و جمله را حاشا کنند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ور بگوئی کاین خطا بود و تو کردی ، در جواب</p></div>
<div class="m2"><p>روی و پیشانی ز روی و آهن و خارا کنند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گاه بی شرمی عیاذا بالله اندر گفت گوی</p></div>
<div class="m2"><p>روی سخت خویش هم چون صخره صما کنند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر کریمان دست خود دریا کنند این قوم نیز</p></div>
<div class="m2"><p>همزه بگذارند جای دال و پس دریا کنند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با چنین قوم ال خناس آن بد آموزان ناس</p></div>
<div class="m2"><p>شاید ار از منصب خود جمله استعفا کنند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>منشی اند ایشان خدا ناخواسته اکنون ولی</p></div>
<div class="m2"><p>در حق ماکاش قدی کمترک انشا کنند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بیم آن داریم کز بس نیشمان بر دل زنند</p></div>
<div class="m2"><p>تنگ مان آرند و نطق بسته مان را وا کنند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نی خطا گفتم نشاید ساق ایشان را گزید</p></div>
<div class="m2"><p>گر هزاران زخم گاز اندر دو ساق ما کنند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خود طلیق عرض خویشند این جماعت کی سزاست</p></div>
<div class="m2"><p>کز زبان شاعران اندیشه و پروا کنند؟</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لیک ذره خردتر زان است کاندر بزم تو</p></div>
<div class="m2"><p>خبث او گویند و او را آن قدر رسوا کنند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خود زبان شان چون قلم ببریده باد آخر دروغ</p></div>
<div class="m2"><p>تا چه حد بر رای ملک آرای تو املا کنند؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو همی شادان و خندان باش و صد زین ها بتر</p></div>
<div class="m2"><p>در حق ما گر کنند اعدای ما، گو، تا کنند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>من ندانستم که مشتی خار و خس دست مرا</p></div>
<div class="m2"><p>زین سعایت ها جدا زان عروه الوثقی کنند</p></div></div>