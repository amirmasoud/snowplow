---
title: >-
    شمارهٔ ۴۴ - در مدح میرزا حسین ولد میرزا محمد علی اشکبوس گفته
---
# شمارهٔ ۴۴ - در مدح میرزا حسین ولد میرزا محمد علی اشکبوس گفته

<div class="b" id="bn1"><div class="m1"><p>آن چه از مژگان خون ریز حسین بر من گذشت</p></div>
<div class="m2"><p>بر حسین کی از جفای لشکر دشمن گذشت؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خال و خط شامی، بناگوش اصبحی، قامت سنان</p></div>
<div class="m2"><p>در جفا زلف حسین از شمر ذی الجوشن گذشت</p></div></div>