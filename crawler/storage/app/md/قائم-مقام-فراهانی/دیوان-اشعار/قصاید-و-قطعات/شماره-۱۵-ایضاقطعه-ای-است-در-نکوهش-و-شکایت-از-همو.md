---
title: >-
    شمارهٔ  ۱۵ - ایضاقطعه ای است در نکوهش و شکایت از همو
---
# شمارهٔ  ۱۵ - ایضاقطعه ای است در نکوهش و شکایت از همو

<div class="b" id="bn1"><div class="m1"><p>خسروا جز دل من بنده که خود قابل نیست</p></div>
<div class="m2"><p>کو خرابی که نه در ملک تو آباد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکوه ها دارم اما ز فلک زان که فلک</p></div>
<div class="m2"><p>یار اوباش شود یاور اوغاد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندهد سیم و زر آن را که نه هم چون شب و روز</p></div>
<div class="m2"><p>خود به نمامی و شیادی معتاد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندهد دولت شغل و عمل آن را هرگز</p></div>
<div class="m2"><p>که نه در صنعت اخذ و عمل استاد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نه زراق و نه شیادم و در مذهب او</p></div>
<div class="m2"><p>وای بر آن که نه زراق و نه شیاد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه ها سازد خونین همه چون خرقه بکر</p></div>
<div class="m2"><p>تا یکی عنین در ملکی داماد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسجد و منبر و محراب به حجاج دهد</p></div>
<div class="m2"><p>گوشه گیری همه باسید سجاد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مثل بنده و این پیر مشعبد گوئی</p></div>
<div class="m2"><p>مثل زال فریبنده و فرهاد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ظلم باشد که به عهد تو و با عدل تو باز</p></div>
<div class="m2"><p>زان جفا پیشه مرا ناله و فریاد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواجه تاشان مرا بین که معطل دارند</p></div>
<div class="m2"><p>گنج در خاک و مرا بین که به کف باد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک درم نیست درین کلبه که ماراست ولی</p></div>
<div class="m2"><p>گنج قارون همه را در ارم عاد بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یک ره آخر تو از ازین پیر خرف گشته بپرس</p></div>
<div class="m2"><p>کاین چه افراط و چه تفریط و چه بیداد بود؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سایش ناس کجا شاید رقاص شود</p></div>
<div class="m2"><p>قاید قوم چرا باید قواد شود؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو چرا فاقد یک فلسی و سیم و زر تو</p></div>
<div class="m2"><p>گه به شیراز رود، گاه به بغداد بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گه عبورش به در حجره تجار افتد</p></div>
<div class="m2"><p>گه گذارش به دم کوره حداد بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه به کشمیر فرستد و زیانی که رسد</p></div>
<div class="m2"><p>از تو و سود ز هر کس که فرستاد بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدره شال که از بدره مال تو خرند</p></div>
<div class="m2"><p>بالوفش خری ار قیمتش آحاد بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بل که هر جنس که خواهی تو درین مرزش ارز</p></div>
<div class="m2"><p>گر بود هفت به دیوان تو هفتاد بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یارب این زهد ریائی چه بلائی بودست</p></div>
<div class="m2"><p>کاین بلاها همه در خرقه زهاد بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر چه افساد بود گر به حقیقت نگری</p></div>
<div class="m2"><p>زین گروه است و به شیطانش اسناد بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لعن بر شیخ عدی واضع قانون بدی</p></div>
<div class="m2"><p>کاول این قاعده در دین تو بنهاد بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عزلت بنده و مشغولی این قوم به کار</p></div>
<div class="m2"><p>یادگاری است که موروث ز اجداد بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لیک اگر آخر این قصه به یاد آرد شاه</p></div>
<div class="m2"><p>عبرتی زان چه درین واقعه افتاد بود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چه شد آن صاحب سلطان جلالت کامروز</p></div>
<div class="m2"><p>خلف الصدق تو سلطانش ز احفاد بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خود شهنشاه شد آگاه و گرنه بایست</p></div>
<div class="m2"><p>زین گروه آن چه مرا دیده مبیناد بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن که شه کشت و شهش کشت شهان را باید</p></div>
<div class="m2"><p>حذر از هر که ز تخم بد او زاد بود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرترا خونی سی ساله بود آن که مرا</p></div>
<div class="m2"><p>یک دو سال است که گویند ز حساد بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سود داد و ستد او همه چون سود قصیر</p></div>
<div class="m2"><p>که به بانوی یمن عرضه همی داد بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سختم آید عجب از خسر عادل کاین سان</p></div>
<div class="m2"><p>قصد آبا کند و ایمن از اولاد بود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ملک خود ایمن از این تخمه بدکن کاکنون</p></div>
<div class="m2"><p>هم چو صیدی است که در پنجه صیاد بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کیست زین فرقه خائن چه زمرد و چه ز زن</p></div>
<div class="m2"><p>که نه بد دیده ز فراش و زجلاد بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>راه این سیل بگردان که به معموره ملک</p></div>
<div class="m2"><p>رخنه فاحش اگر باز نه استاد بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>من خود این خار درین باغ نشاندم کامروز</p></div>
<div class="m2"><p>خرمن جان مرا شعله وقاد بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وان گهی تجربه ها کردم و دیدم کاین مرد</p></div>
<div class="m2"><p>چاپلوسی کند و در پی ارصاد بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>حال گوساله بر بسته ز نصر الدین پرس</p></div>
<div class="m2"><p>که چه سان چون رسن از میخش بگشاد بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آه از آن مسجد و آن خواندن اوراد، و نماز</p></div>
<div class="m2"><p>وان سخن ها که پس از خواندن اوراد بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه مگر پارس بود مولد سلمان کاکنون</p></div>
<div class="m2"><p>خود ز بخت بد ما مولد شداد بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به صفت آب طهارت نبود آب طهور</p></div>
<div class="m2"><p>پاک و ناپاک چو از جمله اضداد بود</p></div></div>