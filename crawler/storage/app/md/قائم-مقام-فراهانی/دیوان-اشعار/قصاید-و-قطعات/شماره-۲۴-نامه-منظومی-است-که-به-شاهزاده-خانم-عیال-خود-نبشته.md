---
title: >-
    شمارهٔ  ۲۴ - نامه منظومی است که به شاهزاده خانم عیال خود نبشته
---
# شمارهٔ  ۲۴ - نامه منظومی است که به شاهزاده خانم عیال خود نبشته

<div class="b" id="bn1"><div class="m1"><p>تا شد دل من بسته آن زلف چو زنجیر</p></div>
<div class="m2"><p>هم دل بشد از کارم و هم کار ز تدبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقدیر چنین بر من و دل رفت و نشاید</p></div>
<div class="m2"><p>با قوت تقدیرش اندیشه تغییر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دل که اسیر آمد در حلقه آن زلف</p></div>
<div class="m2"><p>تدبیر اسیر آمد در پنجه تقدیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای زیور ایوان من، ایوان من از تو</p></div>
<div class="m2"><p>گه طعنه به فرخار زند، گاه به کشمیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا با توام از بخت منم خرم و دل شاد</p></div>
<div class="m2"><p>چون بی توام از عمر منم رنجه و دل گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان ار بدهم شرم رخم خشیت املاق</p></div>
<div class="m2"><p>بوس ارندهی عذر لبت شنعت تبذیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رخسار تو خلدست که رضوانش بر آمیخت</p></div>
<div class="m2"><p>گوئی به شکر لعل و به گل مشک و به می شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جا کرده در آن خلد دو شیطان که به دستان</p></div>
<div class="m2"><p>دارند به خم دام و به کف تیغ و به زه تیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نشگفت که نخجیر کنندم دل و دین زانک</p></div>
<div class="m2"><p>بس هوش پیمبر بگرفتند به نخجیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تدبیر چنین است که شد بوالبشر از راه</p></div>
<div class="m2"><p>جرمی به جوان نیست چو گم راه شود پیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زآشفتگی عشق تو گردوش ز من رفت،</p></div>
<div class="m2"><p>در خدمت درگاه خداوندی تقصیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بخشید چو بر آدم دادار جهان دار</p></div>
<div class="m2"><p>شاید که به من بخشد دارای جهان گیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عباس شه آن خسرو فرخنده که گیرد</p></div>
<div class="m2"><p>اورنگ شهنشاهی با قبضه شمشیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ناگه به شبیخون سپه نور به ظلمت</p></div>
<div class="m2"><p>از تاختن آوردی چون بادبه ، شب گیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن گه به لب آب رسیدی که بدیدی</p></div>
<div class="m2"><p>از روز به شب شیر در آمیخته باقبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون صبح عیان گشت فکندند ز تشکیک</p></div>
<div class="m2"><p>بر صفحه تشویش همی مهره تشویر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این گفت صواب است کنون نهضت ما زود</p></div>
<div class="m2"><p>چون دوش مبادا که شود رکضت ما دیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>وان گفت دگر حرب روا نیست که امروز</p></div>
<div class="m2"><p>هم جیش به تقلیل است، هم خصم به تکثیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو تن به غزا داده که احکام قضا را</p></div>
<div class="m2"><p>نه قدرت تقدیم است نه مهلت تاخیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بردی به هنر جیش سوی حصن مخالف</p></div>
<div class="m2"><p>چونان که نبی برد سوی بدر به تدبیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از جیش تو آن رفت در آن حصن به تخریب</p></div>
<div class="m2"><p>کز شرع نبی رفت در اسلام به تعمیر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هم تیر و سنان آن جا بر صفحه هستی</p></div>
<div class="m2"><p>آجال رجال آورد در معرض تحریر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از روز جزا داد مگر روز غزا باد</p></div>
<div class="m2"><p>کانصار به تعزیر و نصار است به تقریر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>افتاده یکی بر خاک از صدمه ناچخ</p></div>
<div class="m2"><p>غلطیده یکی در خون از ضربت شمشیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>این در زرهش بر زو به کف گرزو به دل کین</p></div>
<div class="m2"><p>وان در گرهش کار و به غم یار و زجان سیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در موکب عالی است وزیری که قضا بست</p></div>
<div class="m2"><p>این ملک به تدبیرش چون چرخ به تدویر</p></div></div>