---
title: >-
    شمارهٔ  ۱ - قصیده ای است در تبریک ورود ولی عهد از طهران به تبریز
---
# شمارهٔ  ۱ - قصیده ای است در تبریک ورود ولی عهد از طهران به تبریز

<div class="b" id="bn1"><div class="m1"><p>این طارم فرخنده که پیداست زبیدا</p></div>
<div class="m2"><p>بالاتر و والاتر از این گنبد خضرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خود زمی است از چه فلک دارد در زیر</p></div>
<div class="m2"><p>ور خود فلک است از چه زمین دارد بالا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخی است که سیرش همه بر ماه زماهی</p></div>
<div class="m2"><p>سیلی است که موجش همه، برابر ز دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیلی که سپارد به فلک پیکر خورشید</p></div>
<div class="m2"><p>سیری که نگارد به زمین زهره زهرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آید همه زان اختر رخشنده سیار</p></div>
<div class="m2"><p>زاید همه زین گوهر ارزنده یک تا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه آرد و اختر چو کند میل به هر سو</p></div>
<div class="m2"><p>زر بارد و زیور چو کشد خیل به هر جا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید جهان گردد ازو تیره و پنهان</p></div>
<div class="m2"><p>خورشید شهان آید ازو روشن و پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر دل این گرد بر افروزد گوئی</p></div>
<div class="m2"><p>نوری که فروزان شده بر سینه سینا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من خود به عیان بینم امروز درین دشت</p></div>
<div class="m2"><p>رازی که شنیدم به خبر از شب اسرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا موکب مسعود ولی عهد درین روز</p></div>
<div class="m2"><p>بر خرگه عالی رسد از درگه اعلا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز آمده با کام دل از کعبه مقصود</p></div>
<div class="m2"><p>چون خواجه جن و بشر از مسجد اقصا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زان دشت همه اسب و سوارست سراسر</p></div>
<div class="m2"><p>زان شهر همه نقش و نگارست سراپا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دشت از تک اسبان و سواران دلاور</p></div>
<div class="m2"><p>شهر از قد رعنای جوانان دلارا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خلدی است بیاراسته در ساحت گیتی</p></div>
<div class="m2"><p>چرخی است به پا خاسته از مرکز غبرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>افروخته زین چرخ بسی زهره و پروین</p></div>
<div class="m2"><p>افراخته زان خلد بسی سدره و طوبی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر سو نگری ماهی آراسته برزین</p></div>
<div class="m2"><p>هر جا گذری سروی پیراسته بر پا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل روید و سرو امروز در کوچه و برزن</p></div>
<div class="m2"><p>مه پوید و مهر امروز بر پشته و صحرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مهر و مه و پروین همه در جوشن فولاد</p></div>
<div class="m2"><p>سرو و گل و نسرین همه در جامه دیبا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیبا همه زیباتر از استبرق جنت</p></div>
<div class="m2"><p>جوشن همه روشن تر از آئینه بیضا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک قوم گزیده سرانگشت تحیر</p></div>
<div class="m2"><p>یک قوم گزیده لب دیوار تماشا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یک قوم همی آمده از دشت به خرگاه</p></div>
<div class="m2"><p>یک قوم همی آمده از شهر به صحرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با بخت همی گفتم: کای روسیه آخر</p></div>
<div class="m2"><p>تا کی ز تو باشم من درمانده و دروا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من از تو، به رنج اندر و در صومعه زاهد</p></div>
<div class="m2"><p>امروز به رقص اندر و در مدرسه ملا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گفت: این گنه از تست که گویند ترا نیست</p></div>
<div class="m2"><p>در گفت بد از عرض خود اندیشه و پروا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفتم: به ملک، گفتند گفت:آری و گفتم:</p></div>
<div class="m2"><p>آوخ که شدم کشته به کام دل اعدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت: از چه هراسی که شه عادل هرگز</p></div>
<div class="m2"><p>بی حجت قاطع نکشد تیغ، بیاسا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گفتم: نه هراسم ز کس الا تو و گرنه</p></div>
<div class="m2"><p>نطق من و تقریر هجاکو، کی، حاشا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت: ازمن اگر بیم همی داری بگریز</p></div>
<div class="m2"><p>گفتم: به کجا؟گفت: به خاک در، دارا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عباس شه آن خسرو فرخنده کز آغاز</p></div>
<div class="m2"><p>هم یاور دین آمده، هم داور دنیا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنک از اثر تربیتش خیزد و ریزد</p></div>
<div class="m2"><p>از ابر نم، از لجه یم، لولو لالا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وان کز نظر مکرمتش آید و زاید</p></div>
<div class="m2"><p>از رز عنب، از آب عنب نشاه صهبا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر جا ز حدیثش سخنی افتد خیزد</p></div>
<div class="m2"><p>از خاک نی ، از نی، شکر ، از شکر، حلوا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر پرتو لطفش نبود بار ور آید</p></div>
<div class="m2"><p>کی شاخ به گل، تاک به مل، خار به خرما؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ور قوت حکمش نبود جلوه گر آید</p></div>
<div class="m2"><p>کی آینه صافی از صخره صما</p></div></div>