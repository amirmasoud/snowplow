---
title: >-
    شمارهٔ  ۲۰ - قطعه تقاضایی بره
---
# شمارهٔ  ۲۰ - قطعه تقاضایی بره

<div class="b" id="bn1"><div class="m1"><p>«رهی را، هست عرضی بر جنابت</p></div>
<div class="m2"><p>که بالاتر ازین زرین قباب است»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>«برای بره موعود دیروز</p></div>
<div class="m2"><p>دلش در آتش حسرت کباب است»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«نمی‌داند تمنای وصالش</p></div>
<div class="m2"><p>درین ایام تعجیل و شتاب است»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«پس از یک سال می‌باید رسیدن</p></div>
<div class="m2"><p>که گویا این حمل آن آفتاب است»</p></div></div>