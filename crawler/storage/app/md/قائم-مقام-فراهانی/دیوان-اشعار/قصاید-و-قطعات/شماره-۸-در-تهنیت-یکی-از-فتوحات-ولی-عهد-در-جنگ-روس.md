---
title: >-
    شمارهٔ  ۸ - در تهنیت یکی از فتوحات ولی عهد در جنگ روس
---
# شمارهٔ  ۸ - در تهنیت یکی از فتوحات ولی عهد در جنگ روس

<div class="b" id="bn1"><div class="m1"><p>خواب بس ای بخت خفته شب به سر آمد</p></div>
<div class="m2"><p>خیز که صبح است و آفتاب برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو انجم که دی بسیج سفر کرد</p></div>
<div class="m2"><p>اینک امروز باز از سفر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه عالم ار به زنگ فرو رفت</p></div>
<div class="m2"><p>باز فروزان ز صیقل سحر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ز خواب و خمار شوی که گوئی</p></div>
<div class="m2"><p>دولت بیدارم این زمان به سر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بگشا، پرده بر فراز که اینک</p></div>
<div class="m2"><p>حلقه به جنبش فتاد و بانگ در آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بار دگر آن به خشم رفته ما را</p></div>
<div class="m2"><p>بر سر بیمار خود مگر گذر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بر ما برگرفت و محنت ما خواست</p></div>
<div class="m2"><p>فضل خدا بین که باز چون به بر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شرم کنم گر کنم نثار رهش جان</p></div>
<div class="m2"><p>زان که به غایت حقیر و مختصر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر قدومش بگو، نه شکوه جورش</p></div>
<div class="m2"><p>جورش اگر چه فزون ز حد و مر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواست که با ما کند ز بد بتر اما</p></div>
<div class="m2"><p>در نظر ما ز خوب، خوب تر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جور خوش آید از آن که در چمن حسن</p></div>
<div class="m2"><p>سرو قدش هم ز ناز بارور آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرو که آزاد و بی ثمر بود از چه</p></div>
<div class="m2"><p>سوری و نسرین و سنبلش ثمر آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود ملک است آن پسر به صورت انسان</p></div>
<div class="m2"><p>یا پری اندر شمایل بشر آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زان لب و دندان به حیرتم که تو گوئی</p></div>
<div class="m2"><p>حقه مرجان و رشته گهر آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا لب شیرین به گفتگو نه گشاید</p></div>
<div class="m2"><p>کی شکر از لعل و گل ز گلشکر آمد؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زنده شود جان از او چنان که مگر باز</p></div>
<div class="m2"><p>معجز دیگر زعیسی دگر آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خاصه چو ناگه ز در، درآید و گوید:</p></div>
<div class="m2"><p>مژده بده کز قدوم شه خبر آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خیز و به درگاه شه شتاب که اینک</p></div>
<div class="m2"><p>شاه بر اورنگ بارگاه برآمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خسرو غازی ابوالمظفر عباس</p></div>
<div class="m2"><p>آمد و با فتح و نصرت و ظفر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن که مگر برق تیغ اوست که هر جا</p></div>
<div class="m2"><p>خرمنی از کفر دید شعله ور آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وان که مگر باغ لطف اوست که هر جا</p></div>
<div class="m2"><p>ساحتی از صدق یافت جلوه گر آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صید شهان جمله وحش و طیر بود، لیک</p></div>
<div class="m2"><p>صید شه ماست هرچه شیر نر آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر چه شکارش بهانه بود ولیکن</p></div>
<div class="m2"><p>در همه جا این حدیث مشتهر آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کز حد مسقو قرال روس به ناگاه</p></div>
<div class="m2"><p>رو به ولایات لیسه و خزر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وز حد تفلیس لشکری به تغلب</p></div>
<div class="m2"><p>زی سپه ایروان به شور و شرآمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شه چو شنید این سخن به صید برون تاخت</p></div>
<div class="m2"><p>تا به سر آن گروه بد سیر آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس خبر آمد به شاه روس که اینک</p></div>
<div class="m2"><p>موکب شه هم چو سیل منحدر آمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چاره ندید او جز آن که باز به مسقو</p></div>
<div class="m2"><p>راند و به حیلت ز راه صلح در آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لشکر تفلیس و گنجه نیز به ناچار</p></div>
<div class="m2"><p>جانب بنگاه خویش پی سپر آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جمله به عذر از خطای خویش که ما را</p></div>
<div class="m2"><p>دیو بدین کار زشت راه بر آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ورنه کفی خاک و مشتی از خس و خاشاک</p></div>
<div class="m2"><p>سیل دمان را چرا به رهگذر آمد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>الغرض از عزم شه چو لشکر دشمن</p></div>
<div class="m2"><p>جمله به سان جراد منتشر آمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاه به بخشود و گفت جرم عدو نیز</p></div>
<div class="m2"><p>چون طلبد زینهار مغتفر آمد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لیک قضا و قدر چو چشم به راهند</p></div>
<div class="m2"><p>تا چه صلاح ملیک مقتدر آمد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صاحب روس اندران کریوه وطن ساخت</p></div>
<div class="m2"><p>کش سر شیطان شکوفه شجر آمد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زین طمع او را که عهد شاهان بشکست</p></div>
<div class="m2"><p>نفع نیامد که سر به سر ضرر آمد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خواست که سود آورد ازین سفر اما</p></div>
<div class="m2"><p>مرگ همی سود او ازین سفر آمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>عهد شکن کام دل نیابد هرگز</p></div>
<div class="m2"><p>گرچه خداوند حشمت و حشر آمد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>دادگرا آن یگانه گوهر رخشان</p></div>
<div class="m2"><p>چیست که هم تیغ تیز و هم سپر آمد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر سپر دین نه تیغ تست پس از چه</p></div>
<div class="m2"><p>در کف تست آن که کف من کفر آمد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تیغ تو روز جهاد کافر تیغ است</p></div>
<div class="m2"><p>لیک به گاه حفاظ دین سپر آمد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شمس فلک مدرک قمر نبود لیک</p></div>
<div class="m2"><p>رای تو شمسی که مدرک قمر آمد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نور خور از روی ماه تست و گرنه</p></div>
<div class="m2"><p>مه زچه رو عاریت سنان زخور آمد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گرچه ز بخت تو خصم خام طمع را</p></div>
<div class="m2"><p>دولت ایام زندگی به سر آمد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>لیک ز روس ایمنی مجوی که دشمن</p></div>
<div class="m2"><p>هر چه بود خرد تر بزرگ تر آمد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چند هزاران خیل و حشم را</p></div>
<div class="m2"><p>گم شده کوار شماره یک نفر آمد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>آتش اگر خفت بس بود که چو برخاست</p></div>
<div class="m2"><p>باز نسیمی ز جا به شعله در آمد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کشور ما بین اگر چه حاکم پیشین</p></div>
<div class="m2"><p>کرد بد امروز خوب در نظر آمد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر پدر پخته از حکومت ما رفت</p></div>
<div class="m2"><p>از پس او خام قلتبان پسر آمد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>دشمن همسایه وانگهی شده نزدیک</p></div>
<div class="m2"><p>چون دو مصارع که دست در کمر آ مد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فرصت جوید نه صلح و شاه جهان را</p></div>
<div class="m2"><p>کاری در پیش سخت و پر خطر آمد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>زان که هم اسباب صلح باید و هم جنگ</p></div>
<div class="m2"><p>جمع دو ضد کار چون تو پر هنر آمد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ورنه، نه باور کند خرد که به یک جا</p></div>
<div class="m2"><p>ماء معین جفت نار مستعر آمد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جز تو که داند که کار دولت و دین را</p></div>
<div class="m2"><p>از چه رسد نفع و از کجا ضرر آمد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ژاژ طبیبان بی خرد مشنو زانک</p></div>
<div class="m2"><p>فکر همین کار علت سهر آمد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>خاصه به وقتی چنین از دل و دستت</p></div>
<div class="m2"><p>مخزن گیتی تهی ز سیم و زر آمد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>عالم در خواب و شاه عالم بیدار</p></div>
<div class="m2"><p>یاور و یارش خدای دادگر آمد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جان و سر عالمی به عدل و به انصاف</p></div>
<div class="m2"><p>شاه چنین را فدای جان و سر آمد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دادگرا دور از آستان تو یک چند</p></div>
<div class="m2"><p>در سقرم هم چو عاصیان مقر آمد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ترسم کآرد ملال شرح غم ارنه</p></div>
<div class="m2"><p>شرح دهم هر چه زین غمم به سرآمد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا تو برفتی به جای خوان نوالت</p></div>
<div class="m2"><p>ما حضرم جمله پاره جگر آمد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گرچه برای من و عدوی من امسال</p></div>
<div class="m2"><p>از تو همه بیم و ضرب و سیم و زر آمد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>لیک مرا ضرب و بیم و سیم و زر از تو</p></div>
<div class="m2"><p>جمله به یک طرز و طور در نظر آمد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>زان که ترا خواهم و هران چه تو خواهی</p></div>
<div class="m2"><p>غایت آمال منش بر اثر آمد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دور ز بزم تو لطف خازن خلدم</p></div>
<div class="m2"><p>سخت تر از عنف مالک سقر آمد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آن توئی ای پادشاه بس که ز دستت</p></div>
<div class="m2"><p>تلخی حنظل حلاوت شکر آمد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ورنه ز هر کس که جز تو باشد بالله</p></div>
<div class="m2"><p>شهد به کامم ز زهر تلخ تر آمد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>افسر اگر بر سرم نهند تو گوئی</p></div>
<div class="m2"><p>بر سرم از دهر دهره و تبر آمد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>خواب و نه بر خاک آستان توام سر</p></div>
<div class="m2"><p>چشم کجا آشنا به نیشتر آمد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ریزه خور خوان تست این که پس از تو</p></div>
<div class="m2"><p>ماحضرش جمله پاره جگر آمد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شکر خدا را که زنده ماندم چندانک</p></div>
<div class="m2"><p>خاک درت باز سرمه بصر آمد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>شرط حیات رهی دعای تو باشد</p></div>
<div class="m2"><p>گرچه دعای شریطه مختصر آمد</p></div></div>