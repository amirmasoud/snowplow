---
title: >-
    قطعات تازی
---
# قطعات تازی

<div class="b" id="bn1"><div class="m1"><p>وجهت وجهی مسلما</p></div>
<div class="m2"><p>لفاطر قد فطرک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمَنتُ بالله الَذی</p></div>
<div class="m2"><p>بصنعه قد صورک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احب من تحبه</p></div>
<div class="m2"><p>و من یحب منظرک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تالله کنت هالکا</p></div>
<div class="m2"><p>فی شقوتی لولم ارک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتحت قلبی عنوه</p></div>
<div class="m2"><p>روحی فِداک ای پسرک</p></div></div>