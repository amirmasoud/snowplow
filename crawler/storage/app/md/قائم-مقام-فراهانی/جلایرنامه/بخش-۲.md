---
title: >-
    بخش ۲
---
# بخش ۲

<div class="b" id="bn1"><div class="m1"><p>جلایر در سواری اوستاد ست</p></div>
<div class="m2"><p>به اسب اندازی از رستم زیادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرید افکن، تقلا زن، سواری است</p></div>
<div class="m2"><p>تفنگ اندازی، و نیزه گذاری است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش روی قیقاج و چپ و راست</p></div>
<div class="m2"><p>زندگوئی به هر جائی دلش خواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیاده گشته خفته رو به بالا</p></div>
<div class="m2"><p>به عون حضرت باری تعالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چنگی لوله، بر چخماق چنگی</p></div>
<div class="m2"><p>قراول رفته، در پشت تفنگی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تفنگ آورده پهلوی بناگوش</p></div>
<div class="m2"><p>که باشد جانب بالای سر، روش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشان کرده کلاه یک قراگوز</p></div>
<div class="m2"><p>که پشمش بد بسان پوست مرغوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلول اندر نشانه کرده گولی</p></div>
<div class="m2"><p>مثال مذهب شیخ حلولی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سه باج اقلو گرو از منشی نجد</p></div>
<div class="m2"><p>ببرد و عالمی آورد در وجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سواری نیزه دار از ایل گوران</p></div>
<div class="m2"><p>ز نزدیکا سلیمان خان نه دوران</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به میدان جلایر آمد آن روز</p></div>
<div class="m2"><p>که گردد بر جلایر بل که فیروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کهر جان هم چو آهو در دو آمد</p></div>
<div class="m2"><p>جریدی ازجلایر پرتو آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به گوران خورد و گوران بر زمین خورد</p></div>
<div class="m2"><p>معلق از جرید اولین خورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کهرجان اسم خاص اسب بنده است</p></div>
<div class="m2"><p>که خود از کر ه گی دل چسب بنده است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یکی اسب و دگر منقار قوشم</p></div>
<div class="m2"><p>که شیهش مثل شهنازست به گوشم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صراحی گردن و خوش چشم و سرسخت</p></div>
<div class="m2"><p>قلم باریک و سم گرد و کفل تخت</p></div></div>