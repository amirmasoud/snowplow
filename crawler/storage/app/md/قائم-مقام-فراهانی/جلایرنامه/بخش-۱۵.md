---
title: >-
    بخش ۱۵
---
# بخش ۱۵

<div class="b" id="bn1"><div class="m1"><p>جلایر شرح دیگر را بیان کن</p></div>
<div class="m2"><p>گهر آور نثار این و آن کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی عهد شهنشاه جوان بخت</p></div>
<div class="m2"><p>که ز آغاز آمد او شایسته تخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثنایش ذکر لب کن صبح تا شام</p></div>
<div class="m2"><p>بقایش خواه از قیوم علام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجودش فیض بخش خاص و عام است</p></div>
<div class="m2"><p>از این اندر دو عالم نیک نام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که تیغ او پناه ملک و دین شد</p></div>
<div class="m2"><p>یکی سدی است لیکن آهنین شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سدی کو سکندر بست بر آب</p></div>
<div class="m2"><p>بیانی می کنم نیکو تو دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که بستن سد به آبی از کم و بیش</p></div>
<div class="m2"><p>چو ممکن هست، چندان نیست تشویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ولی از آتش سوزان گریزند</p></div>
<div class="m2"><p>چه سان خلق جهان با او ستیزند؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز آتش صعب تر چون نیست در کار</p></div>
<div class="m2"><p>خدا زان خلق را ترساند از نار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی عهد شه از این تیغ تیزش</p></div>
<div class="m2"><p>که سی سال است با آتش ستیزش،</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پاس دین درین دریای آتش</p></div>
<div class="m2"><p>ببسته سدی اما سخت و دلکش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نموده حفظ خرمن های دین را</p></div>
<div class="m2"><p>از آن آتش مصون دین مبین را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هر آن کس شکر این نعمت ندارد</p></div>
<div class="m2"><p>ندانم بهره ای از عقل دارد؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه داند آن که دستش دور از آتش</p></div>
<div class="m2"><p>بود بر گردن یاران مه وش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عراق و فارس تا سر حد کرمان</p></div>
<div class="m2"><p>ز دارالمرز گویم تا خراسان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکی در فکر عیش و ناز و نوش است</p></div>
<div class="m2"><p>یکی هشیار و آن دیگر خموش است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکی را شوق گل کاری به سرهست</p></div>
<div class="m2"><p>یکی فکرش همه در جمع زر هست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی بر پا نموده کاخ دلکش</p></div>
<div class="m2"><p>درو هم شمع و فرش و آب و آتش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یکی بر ترمه و بر پول نازد</p></div>
<div class="m2"><p>به سودا کار خود را خوب سازد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی گوید که: چون رستم کنم رزم</p></div>
<div class="m2"><p>نه در میدان، ولی در مجلس بزم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یکی دیگر به تدبیرات و حیله</p></div>
<div class="m2"><p>به خورشید گوید:«ای نور قبیله»</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی با همگنانش در جدال است</p></div>
<div class="m2"><p>بگوید: صلح نزد من محال است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نخواهد خلق را یک روز راحت</p></div>
<div class="m2"><p>زمین بخل را دارد مساحت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگوید: کس ز من به تر نباشد</p></div>
<div class="m2"><p>که من زور و زرم کم تر نباشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یکی سرکش ولی بسیار مغرور</p></div>
<div class="m2"><p>که گویا هست دایم مست و مخمور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ندیده توپ هفتاد و دو پوندی</p></div>
<div class="m2"><p>چو رعد و برق پر زورست و تندی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نشسته سایه های سرو آزاد</p></div>
<div class="m2"><p>کجا جنگ ارس را کرده او یاد؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یکی خربوزه گرسنگ و گرگاب</p></div>
<div class="m2"><p>خورد با نعمت الوان کند خواب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه ببریده به سکین جز خیاری</p></div>
<div class="m2"><p>نه دیده رنگ خون جز آب ناری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی لیمو خورد بر دفع صفرا</p></div>
<div class="m2"><p>کجا دیده جهان سرد و گرما؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بدیده جنگ، لیکن از خروسی</p></div>
<div class="m2"><p>زمین آتش فشان دید از عروسی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کجا خوردند افسوس و دریغی</p></div>
<div class="m2"><p>کجا آغشته در خون دیده تیغی؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کجا هم جان و مالش را تلف دید</p></div>
<div class="m2"><p>کجا تیغی ز خصمانش به کف دید؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کجا تاراج کرد و گشت تاراج</p></div>
<div class="m2"><p>کجا تن را به دشمن کرد آماج؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کجا بر نان خشکی کرده افطار</p></div>
<div class="m2"><p>ز جان بگذشته سر برده به کهسار؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا وی را سپاهی در کمین بود</p></div>
<div class="m2"><p>کجا در بحر آتش های کین بود؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کجا بشنید ضرب و طعن اغیار</p></div>
<div class="m2"><p>نبودش وقت حاجت هیچ دینار؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رفاه خلق چون بودیش مقصود</p></div>
<div class="m2"><p>خدا هر مشکلش را زود بگشود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مشقت چون برای مرد باشد</p></div>
<div class="m2"><p>که راحت بهر هر بی درد باشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بلی هر کس پسندد کرده خویش</p></div>
<div class="m2"><p>نمی گویم سخن دیگر از این بیش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ولی افسانه باشد این خیالات</p></div>
<div class="m2"><p>خیالات است گویند از محالات</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خدا داند که هر کس قابل چیست</p></div>
<div class="m2"><p>سزاوار جهان داری است یا نیست؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو خورشید جهان آرا در آید</p></div>
<div class="m2"><p>ستاره محو و عمر او سر آید</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شهنشاه است چون خورشید تابان</p></div>
<div class="m2"><p>ولی عهدست چون ضوء نمایان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولی نبود جدا ضو چون ز خورشید</p></div>
<div class="m2"><p>یکی باشد اگر نامش دو گردید</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بود این لازم و ملزوم با هم</p></div>
<div class="m2"><p>یکی بادام باشد لیک توام</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ولی داند شهنشاه جهان دار</p></div>
<div class="m2"><p>که او بر سروری بودی سزاوار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جلایر حسب حالی را بگفتی</p></div>
<div class="m2"><p>در معنی به نوک کلک سفتی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بکن ختم سخن را بر دعایش</p></div>
<div class="m2"><p>چون حدت نیست تعریف و ثنایش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خداوندا پناه آن و این باش</p></div>
<div class="m2"><p>جهان را گو مدامی این چنین باش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر آن چیزی که خواهد روزگارش</p></div>
<div class="m2"><p>شود آماده آرد در کنارش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حسودش خون جگر، با غم قرین باد</p></div>
<div class="m2"><p>جهان تا هست هم خوار و حزین باد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>جلایر را کنی از رحمتت شاد</p></div>
<div class="m2"><p>ز قید غم شها سازیش آزاد</p></div></div>