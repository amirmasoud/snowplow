---
title: >-
    بخش ۷
---
# بخش ۷

<div class="b" id="bn1"><div class="m1"><p>امام و پیشوا در خنده افتاد</p></div>
<div class="m2"><p>دو لعلش در سخن تابنده افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مقصود تو با انجاح ماضی است</p></div>
<div class="m2"><p>ولی عهد ازمحمدشاه راضی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگوید با پدر جز راست هرگز</p></div>
<div class="m2"><p>نه منصب، نه حکومت، خواست هرگز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ولی عهد اربه او ملکی سپارد</p></div>
<div class="m2"><p>طمع در ملک همسایه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشوراند به حاکم ها رعیت</p></div>
<div class="m2"><p>نخواهد بر مسلمانان اذیت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه مفسد را دهد پول زیادی</p></div>
<div class="m2"><p>که خیزد قتل و آشوب و فسادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندارد پول اگر دارد همین است</p></div>
<div class="m2"><p>که در راه کرور هشتمین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از این رو کار او خوب است دایم</p></div>
<div class="m2"><p>قرین با هر چه مرغوب است دایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه گل ها کز مراد خود بچیند</p></div>
<div class="m2"><p>به دنیا و به عقبی بد نه بیند</p></div></div>