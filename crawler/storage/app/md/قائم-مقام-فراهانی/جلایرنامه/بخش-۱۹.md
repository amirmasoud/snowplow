---
title: >-
    بخش ۱۹
---
# بخش ۱۹

<div class="b" id="bn1"><div class="m1"><p>جلایر چند مغموم و حزینی</p></div>
<div class="m2"><p>به بیت الحزن با غم هم نشینی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مرغی بینمت پرها شکسته</p></div>
<div class="m2"><p>به بند غم دو پایت سخت بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به زندان غمت محبوس بینم</p></div>
<div class="m2"><p>ز عمر و زندگی مایوس بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غذایت از چه رو خون جگر شد</p></div>
<div class="m2"><p>دو دستت را ز غم دایم به سر شد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشینی تا به کی تنها شب و روز</p></div>
<div class="m2"><p>کجا آید ترا آن صبح فیروز؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پروانه طریق عشق آموز</p></div>
<div class="m2"><p>پر مرغ هوس را زودتر سوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا دائم فلک با تو به کین است</p></div>
<div class="m2"><p>به هر آزاده ای گویا چنین است؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خوش گفت این سخن را نکته دانی</p></div>
<div class="m2"><p>طبیبی، حاذقی، شیرین زبانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که من خوی جهان را می شناسم</p></div>
<div class="m2"><p>سرشت آسمان را می شناسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>«فلک را عادت دیرینه این است</p></div>
<div class="m2"><p>که با آزادگان دائم به کین است»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«به دل ها بی سبب کین دارد این زال</p></div>
<div class="m2"><p>نه دین دارد نه آئین دارد این زال»</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگو: اندوهت آخر از چه چیز است</p></div>
<div class="m2"><p>که خون دل ز چشمت چشمه خیزست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو که دائم ثنا گستر به شاهی</p></div>
<div class="m2"><p>چو باشد لطف شه، دیگر چه خواهی؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بزم خلد آئینش شب و روز</p></div>
<div class="m2"><p>مشرف می شوی ای روز فیروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تفقدها از آن خسرو به بینی</p></div>
<div class="m2"><p>چه غم داری که در کنجی نشینی؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر داری شکایت از زمانه</p></div>
<div class="m2"><p>مترس و عرض کن با یک فسانه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که شه باب امید و مرحمت هست</p></div>
<div class="m2"><p>چو کردی عرض، ز آن غم ها توان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر جا در بمانی دست گیرست</p></div>
<div class="m2"><p>چرا که قلب پاک او منیرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بباید عرض و درد خویش گفتن</p></div>
<div class="m2"><p>که دیده درد از درمان نهفتن؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بگو: آخر به هر دردست درمان</p></div>
<div class="m2"><p>چرا داری حواس خود پریشان؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ندانی این جهان بی اعتبارست</p></div>
<div class="m2"><p>نه در کارش کسی را اختیارست؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بباید ساخت با او گر نسارد</p></div>
<div class="m2"><p>عتابش بیش و کم گاهی نوازد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به بین جز صبر او را چاره باشد</p></div>
<div class="m2"><p>که در بندش هزار آواره باشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جواب ما صوابی ار تو داری</p></div>
<div class="m2"><p>بگو: ورنه مکن این قدر زاری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بلی انصاف این است آن چه گفتی</p></div>
<div class="m2"><p>سخن را چون در ناسفته سفتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دلی خون باشدم از چرخ گردون</p></div>
<div class="m2"><p>روان زان است اشگم هم چو جیحون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گهی بارم دهد دربار شاهی</p></div>
<div class="m2"><p>گهی محروم سازد بی گناهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به سر داده است عشق خدمت شاه</p></div>
<div class="m2"><p>ولی محروم دارد گاه و بی گاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ازین محرومیش دل ریش و زارست</p></div>
<div class="m2"><p>که این ظلمی به او از روزگارست</p></div></div>