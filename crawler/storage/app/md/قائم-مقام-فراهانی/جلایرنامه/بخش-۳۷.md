---
title: >-
    بخش ۳۷
---
# بخش ۳۷

<div class="b" id="bn1"><div class="m1"><p>جلایر بر دعا کن ختم، عرضت</p></div>
<div class="m2"><p>دعای اوست چون برجمله فرضت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ورد خود کنی نعت و ثنایش</p></div>
<div class="m2"><p>بخواهی از خدا ملک و بقایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوندا به حق حق پرستان</p></div>
<div class="m2"><p>به آب دیده های زیردستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حق احمد محمود مختار</p></div>
<div class="m2"><p>که تا در گردش است این چرخ دوار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمان دولتش را ساز دائم</p></div>
<div class="m2"><p>که نسلا بعد نسل تا به قائم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرام و مدعایش باد حاصل</p></div>
<div class="m2"><p>نماند آرزویش هیچ بر دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسودش در به در با غم قرین باد</p></div>
<div class="m2"><p>جهان تا هست هم خوار و حزین باد</p></div></div>