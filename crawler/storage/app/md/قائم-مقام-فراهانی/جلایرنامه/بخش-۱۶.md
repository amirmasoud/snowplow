---
title: >-
    بخش ۱۶
---
# بخش ۱۶

<div class="b" id="bn1"><div class="m1"><p>جلایر کن دعا این انجمن را</p></div>
<div class="m2"><p>بیار آن طوطی شکر سخن را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کند عرضی مگر او نغز و شیرین</p></div>
<div class="m2"><p>که در این انجمن ماه است و پروین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی عهد شهنشه شاد گردد</p></div>
<div class="m2"><p>ز قید غم دلش آزاد گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد خدمتش زین چیز خوش تر</p></div>
<div class="m2"><p>وگر آید به دستش هفت کشور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کدام است آن خبر جز نقل طهران</p></div>
<div class="m2"><p>ز ذات پاک شاهنشاه دوران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کز آسیب زمانه دور باشد</p></div>
<div class="m2"><p>مبارک خاطرش مسرور باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشسته شاد بر تخت همایون</p></div>
<div class="m2"><p>به اقبال بلند و بخت میمون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آن شه زاده یک خدمت گرفته</p></div>
<div class="m2"><p>چو پروین گرد آن ماه دو هفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شود رفع بلا بالمره، یک بار</p></div>
<div class="m2"><p>ز لطف قادر قیوم قهار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بکن عرضی که از دارالخلافه</p></div>
<div class="m2"><p>صبا آورد مشکی نافه نافه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صحیفه آمده بنوشته یک سر</p></div>
<div class="m2"><p>همه مقصود را با عنبرتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هوا زان نامه بس عنبر فشان است</p></div>
<div class="m2"><p>زمین از و جد سر بر کهکشان است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولی عهد شه از این مژده دل شاد</p></div>
<div class="m2"><p>شود از غم نیارد بعد ازین یاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحمدالله که از لطف خداوند</p></div>
<div class="m2"><p>هم غم رفت و خاطر گشت خرسند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شه صاحب قرآن با بخت فیروز</p></div>
<div class="m2"><p>ز تشریفش شب طهران بشد روز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز یمن مقدمش رشگ جنان شد</p></div>
<div class="m2"><p>چه طهران بل که فردوسی عیان شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه اهل ممالک شاد گشتند</p></div>
<div class="m2"><p>ز قید غم همه آزاد گشتند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دعا گو پیر و برنا بر وجودش</p></div>
<div class="m2"><p>همه از سروران سر بر سجودش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر آن چه خواستی از لطف داور</p></div>
<div class="m2"><p>بحمدالله به خوبی شد میسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کنون شاد است و خرم هر چه جان است</p></div>
<div class="m2"><p>که روز عید آذربایجان است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه بهجت فزا گشت و طرب خیز</p></div>
<div class="m2"><p>سراسر خطه معمور تبریز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به فصل دی بهار تازه آمد</p></div>
<div class="m2"><p>به گلشن مرغ خوش آوازه آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صبا بر بوستان آهسته خیزد</p></div>
<div class="m2"><p>مبادا شبنم از برگی بریزد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سمن با نسترن هم راز گشته</p></div>
<div class="m2"><p>به حسرت چشم نرگس بازگشته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فکنده شد نقاب از چهره گل</p></div>
<div class="m2"><p>خمارین نرگس و آشفته سنبل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گل صفرا رخش شد ارغوانی</p></div>
<div class="m2"><p>نمانده یعنی از صفرا نشانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز لاله لاله عناب است خوش رنگ</p></div>
<div class="m2"><p>شکفته صحن بستان رنگ در رنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شده خوش جعفری با مخملی جور</p></div>
<div class="m2"><p>زمین بوستان از لاله پر نور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چه خوش آیند مینا در میان است</p></div>
<div class="m2"><p>که گویا یاسمن با ارغوان است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بحمدالله که در عهد ولی عهد</p></div>
<div class="m2"><p>همه آسوده خفته خلق در مهد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به اردو زین خبر جشنی به پا کرد</p></div>
<div class="m2"><p>که الحق شادمانی را به جا کرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز لطفش مرحمت آباد گردید</p></div>
<div class="m2"><p>دل غم دیده یک سر شاد گردید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زیک سو ساز و بانگ نای برخاست</p></div>
<div class="m2"><p>دگر سو بانگ کوس وهای برخاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زمین چون آسمان شد پر ستاره</p></div>
<div class="m2"><p>در اطرافش خلایق در نظاره</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شب تاریک روشن گشت چون روز</p></div>
<div class="m2"><p>ز آتش بازهای شعله افروز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به آتش ها زند آبی ز رحمت</p></div>
<div class="m2"><p>که آسایند خلقی از مشقت</p></div></div>