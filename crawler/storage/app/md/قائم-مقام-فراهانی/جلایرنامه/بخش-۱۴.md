---
title: >-
    بخش ۱۴
---
# بخش ۱۴

<div class="b" id="bn1"><div class="m1"><p>جلایر رفت و برخود کرد واجب</p></div>
<div class="m2"><p>که گیرد پول و بدهدشان مواجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمر را، هم سفر با خویش کرده</p></div>
<div class="m2"><p>عجب ها زان سبیل و ریش کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشارت باد! کان سنی نجس رفت</p></div>
<div class="m2"><p>سری کو آمد اندر زیر فس رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فس یک منگله آویز کرده</p></div>
<div class="m2"><p>جهانی را عفونت خیز کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو اول منزلش مشکین جق آمد</p></div>
<div class="m2"><p>عمر را میل نان و قاطق آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جلایر بستد از دهقان لواشی</p></div>
<div class="m2"><p>ز ماش و لوبیا آورد آشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر زان گونه یورش بر طبق کرد</p></div>
<div class="m2"><p>که دهقانی معاوی را دمق کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آن گه رو به جام آب آورد</p></div>
<div class="m2"><p>که نتوان تشنگی را تاب آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عطش ساکت نشد از جام و کوزه</p></div>
<div class="m2"><p>سر اندر جو فرو شد تا به پوزه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خورش نفاخ و آن پر خوار گستاخ</p></div>
<div class="m2"><p>صداها آیدش هر دم ز سوراخ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو با اصحاب تا فرسنگی آمد</p></div>
<div class="m2"><p>ز ناقوسش صدای زنکی آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>همانا مهره را در طاس انداخت</p></div>
<div class="m2"><p>به ریش خویش، آن خناس انداخت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز درد دل فغانش بر سما شد</p></div>
<div class="m2"><p>بگفت آه! این بلا از لوبیا شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>علاجم کن جلایر جان که مردم</p></div>
<div class="m2"><p>که جان بر مالک دوزخ سپردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طبیبی گر بدی با یک اماله</p></div>
<div class="m2"><p>نمودی چاره های آن نواله</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>غلط کردم که از این آش خوردم</p></div>
<div class="m2"><p>ز آش لوبیا و ماش مردم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بود دست من و دامانت ای دوست</p></div>
<div class="m2"><p>اگر دستوری آید دست، نیکوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو میراث است دستور از خلیفه</p></div>
<div class="m2"><p>که تفصیلش رسید از بوحنیفه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ولی آن شیشه لحمی بود و آبم</p></div>
<div class="m2"><p>اگر میرم که وصل او نیابم!</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر آن شیشه بر دستم فتادی</p></div>
<div class="m2"><p>همین سدی که بستم می گشادی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگفتم: کو طبیب و کو دوائی</p></div>
<div class="m2"><p>کجا شیشه بود در ، هم چو جائی؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حکیم باشی به اردو ماند و شیشه</p></div>
<div class="m2"><p>هر آن جا خرس باشد هست بیشه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفتا یک سواری چست و چالاک</p></div>
<div class="m2"><p>به اردو گر رود از بهر غمناک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رساند شیشه دستور زودی</p></div>
<div class="m2"><p>که شاید سده از ریشم گشودی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سواری پس فرستادم به اردو</p></div>
<div class="m2"><p>که پیدا گر تواند کرد هر سو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بشد پیدا چو کرده او تلاشی</p></div>
<div class="m2"><p>به جیب نوکر حکیم باشی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>حکیم باشی شنید این های و هو را</p></div>
<div class="m2"><p>تفحص کرد چون احوال او را،</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بگفتا: شیشه هست اما به کار است</p></div>
<div class="m2"><p>چه گونه می دهم گر جان سپار است،</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مگر دینار نقدی ریزیم مشت</p></div>
<div class="m2"><p>به شیشه پس توانی برد انگشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فرستم آدم و خفته کنندش</p></div>
<div class="m2"><p>اگر زر نیست کردم ریشخندش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتا :این مگو خرس بزرگ است</p></div>
<div class="m2"><p>به حیله رو به، اما شکل گرگ است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تعارف داند و چربی زبانی</p></div>
<div class="m2"><p>ز سودایش نه سود و نه زیانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عمر گر ، به شود، بدهد ترا اسب</p></div>
<div class="m2"><p>عربی زاده تازی خوب و دل چسب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فرستاد این و دادم زود حالی</p></div>
<div class="m2"><p>که بد حال است دیگر کو مجالی؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دو درهم کن غذایش را معین</p></div>
<div class="m2"><p>که باشد این عمر شکل برهمن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفتا: آدمم دارد وقوفی</p></div>
<div class="m2"><p>که خواهد داد او را سفونی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ولی یک من نمک با مشگکی آب</p></div>
<div class="m2"><p>بر او ریزد کند پس اندکی خواب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دو ران ماده گامیش کهن سال</p></div>
<div class="m2"><p>خورانندش غذا چون هست بد حال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پس آن گه حال فورا باز گویند</p></div>
<div class="m2"><p>که گر این چاره نبود چاره جویند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برنجی شیشه ای بودی سه پاره</p></div>
<div class="m2"><p>به هم چون وصل شد گشتی مناره</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نهاد آن بوق بر سوراخ خیکش</p></div>
<div class="m2"><p>بر او می ریخت پس آبی ز دیگش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو پر شد مشگش، از حلقش به در شد</p></div>
<div class="m2"><p>سبیل و ریش و سر تا پاش تر شد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>غرض اعجوبه ای بود این حکایت</p></div>
<div class="m2"><p>که رفت از حال نحس او روایت</p></div></div>