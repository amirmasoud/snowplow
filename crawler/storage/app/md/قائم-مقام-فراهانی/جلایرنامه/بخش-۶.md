---
title: >-
    بخش ۶
---
# بخش ۶

<div class="b" id="bn1"><div class="m1"><p>جلایر یک سفر بغداد کرده</p></div>
<div class="m2"><p>زیاران و رفیقان یاد کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خصوصا در زیارت های مخصوص</p></div>
<div class="m2"><p>به زیر چلچراغ و پای فانوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول داده به باشماقچی فلوسی</p></div>
<div class="m2"><p>پس آن گه داده بر درگاه بوسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رواق اولین را کرده تعظیم</p></div>
<div class="m2"><p>به خادم داده یک باجاقلی و نیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وزان پس تا زیارت گاه رفته</p></div>
<div class="m2"><p>گدائی رو به تخت شاه رفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیارت نامه خوان خوش صدایی</p></div>
<div class="m2"><p>به پیش آورده و خوانده دعایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیارت کرده جای آن دو انگشت</p></div>
<div class="m2"><p>که بیرون آمد و بدخواه را کشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ایوان طلا کرده نمازی</p></div>
<div class="m2"><p>به گفته با خدای خویش رازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی حاجت گرفته بند قندیل</p></div>
<div class="m2"><p>زده سر بر زمین افکنده مندیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خروشی بر کشیده از دل ریش</p></div>
<div class="m2"><p>به آب دیده شسته سبلت و ریش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه بینی بر آن مخلوط کرده</p></div>
<div class="m2"><p>کثافت کاری مضبوط کرده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سجودی کرده و در خواب رفته</p></div>
<div class="m2"><p>کتانی در بر مهتاب رفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>میان نوم ویقظه دیده باغی</p></div>
<div class="m2"><p>در آن روشن ز هر لاله چراغی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سمن با ارغوان هم راز آن جا</p></div>
<div class="m2"><p>به حسرت چشم نرگس باز آن جا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقاب از رخ فکنده شاهد گل</p></div>
<div class="m2"><p>پریشان طره پرتاب سنبل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرابستان خوش آب و هوائی</p></div>
<div class="m2"><p>در آن بنهاده تخت پادشائی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملایک صف زده بر گرد آن تخت</p></div>
<div class="m2"><p>نشسته پادشه با هیبتی سخت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جلایر لرزه بر اندامش افتاد</p></div>
<div class="m2"><p>تو پنداری به سر سرسامش افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که یارب این بهشت دل گشا چیست؟</p></div>
<div class="m2"><p>نشسته روی تخت این پادشا کیست؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندا آمد که یا عبدی جلایر</p></div>
<div class="m2"><p>لکم من عندنا خیرالذخایر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دعای تو، به سوی آسمان شد</p></div>
<div class="m2"><p>هر آنچ از ما طلب کردی همان شد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>امام و پیشوای توست این شاه</p></div>
<div class="m2"><p>به دل گر حاجتی داری ازو خواه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جلایر زین بشارت شادمان گشت</p></div>
<div class="m2"><p>همه شکر خدا ورد زبان گشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دوید و رفت و خاک راه بوسید</p></div>
<div class="m2"><p>پس آن گه پای تخت شاه بوسید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شهنشه گفت: آخر مطلبت چیست؟</p></div>
<div class="m2"><p>جلایر گفت جز این مطلبم نیست،</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که شه زاده محمد را ز شاهان</p></div>
<div class="m2"><p>برافرازی به کام نیک خواهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وجودش تا ابد محفوظ باشد</p></div>
<div class="m2"><p>زعمر جاودان محظوظ باشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز آسیب جهان پایش نه لخشد</p></div>
<div class="m2"><p>خدا او را به شاه ما به بخشد</p></div></div>