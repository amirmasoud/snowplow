---
title: >-
    شمارهٔ  ۴۳ - خطاب به میرزا بزرگ وزیر نواب
---
# شمارهٔ  ۴۳ - خطاب به میرزا بزرگ وزیر نواب

<div class="b" id="bn1"><div class="m1"><p>هر کس که بدست جام دارد</p></div>
<div class="m2"><p>سلطانی جم مدام دارد</p></div></div>
<div class="n" id="bn2"><p>اگر خواجه راست میگفت میرزا علی خان که جام در دستش است، بایست یک دانه شلغم داشته باشد که خودش از گرسنگی نمیرد؛ تا بماها که مهمان این سرزمینیم چه رسد؟ حال ها نیز بگردد ز روش گاه بگاه، پریروز گندم در اردوی سرخس صد من، یک صاحبقران خریدار نداشت، امروز در منزل جام، جو یک من دو صاحبقران بهم نمیرسد؛ قوت حیوان وانسان منحصر است ببرف و برد. عالیجاه میرزا احمد میگفت کاغذی از خدمت میرزا برای تو آورده ام، اما هنوز این اخلاص مند بزیارت آن فایز نشده، جز جدال او و خلیفه چیزی ندیده ام.</p></div>
<div class="n" id="bn3"><p>بلی بعد از مجادلات ومحاورات شدیده عدیده بحمدالله تعالی نگار خامة معجز نگار زیادت شد و از این که بیاد شما بوده ام شکرها کردم و چون مضمون کاغذ جز معرفی و سفارش عالیجاه مشارالیه چیزی دیگر نبود، با آن که دراین وادی غیرذی زرع از هر جهه خجالت حاصل بود، باز یک طوری راه انداختم که چون خودش خوب کسی است؛ ان شاءالله تعالی در خدمت شما نارضائی از من نخواهد کرد و از خدا می خواهم تا زنده ام خلاف فرمایش شما از من صادر نشود، خواه جزئی و خواه کلی و توفیق کرامت فرماید که از عهدة خدمت توانم برآمد.</p></div>
<div class="n" id="bn4"><p>قو علی خدمتک جوارحی و اشدد علی العزیمه جوانحی وهب لی الجدء فی خشیتک والدوم فی الاتصال بخدمتک</p></div>
<div class="n" id="bn5"><p>والسلام</p></div>