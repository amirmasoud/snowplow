---
title: >-
    شمارهٔ  ۶۹ - قائم مقام به پسر خویش نوشته است
---
# شمارهٔ  ۶۹ - قائم مقام به پسر خویش نوشته است

<div class="n" id="bn1"><p>پسرم، نور بصرم، من از تو غافل نیستم، تو چرا از خود غافلی؟ گشت باغ و سر راغ شیوه درویشان است، نه عادت بی ریشان. سیاحت امردان با رندان، رسم لوندان است نه مردان.</p></div>
<div class="n" id="bn2"><p>هرگاه درین ایام جوانی که بهار زندگانی است دل صنوبری را بنور معرفت زنده کردی مردی والا بجهالت مردی.</p></div>
<div class="n" id="bn3"><p>هان ای پسر بکوش که روزی پدر شوی. والسلام</p></div>