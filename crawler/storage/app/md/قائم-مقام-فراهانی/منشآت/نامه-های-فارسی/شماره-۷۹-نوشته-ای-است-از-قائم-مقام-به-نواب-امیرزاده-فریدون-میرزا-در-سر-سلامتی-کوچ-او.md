---
title: >-
    شمارهٔ  ۷۹ - نوشته ای است از قائم مقام به نواب امیرزاده فریدون میرزا در سر سلامتی کوچ او
---
# شمارهٔ  ۷۹ - نوشته ای است از قائم مقام به نواب امیرزاده فریدون میرزا در سر سلامتی کوچ او

<div class="n" id="bn1"><p>فدایت شوم: میرزا محمد حسین که آمد همه خبرهاش خوب بود و ورود و شهودش بسیار مستحسن و مرغوب؛ اما از یک جهه خاطر پیر غلام قدیمی را زایدالوصف خسته و آزرده داشت. پس از مرگ جوانان گل مماناد.</p></div>
<div class="n" id="bn2"><p>در این حادثه بحدی شکسته دل و پریشان حواس میباشم که بشرح و بیان نمیگنجد و هر چند چندین عوض و بدل از همین دودمان مسعود همانجا آماده و موجود هست لکن، ماء لاکصدا و مرعی لاکسعدان و فتی لاکمالک چرا که آن وضع اتصال از کجا بحضرت ملک خصال شاهزاده بی همال خواهد بود؟ حق این است که تکلیف صبر و شکیب در این مصیبت مالایطاق است. اما بمرور روزگار عاقب کار بصبوری و شکیبائی خواهد کشید.</p></div>
<div class="b" id="bn3"><div class="m1"><p>فقلت لها یا عز کل مصیبه</p></div>
<div class="m2"><p>اذا وطنت یوما لها النفس ذلت</p></div></div>
<div class="n" id="bn4"><p>والسلام</p></div>