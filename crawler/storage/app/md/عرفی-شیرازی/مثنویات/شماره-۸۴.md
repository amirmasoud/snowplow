---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>گر محبت حمله بر ناموس کفار آورد</p></div>
<div class="m2"><p>برهمن را سبحه در گردن ببازار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میان گریه مستانه غرقم شحنه کو</p></div>
<div class="m2"><p>تا شراب آلوده مستم برسردار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خجل باشد زایمان لذت کفرش حرام</p></div>
<div class="m2"><p>عابدی کش زلف او در قید زنار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین که عالم کفر گیرد کی درآرد سربه تیغ</p></div>
<div class="m2"><p>گردل شیدای موسی تاب دیدار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قحط حسن چون توئی بگشود برقع لاجرم</p></div>
<div class="m2"><p>روزگار هجر یوسف را ببازار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عابدان گویند با شب زنده داری فیضهاست</p></div>
<div class="m2"><p>کو کسی کین مژده از دلهای بیدار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجز را ذوقیست عرفی تا شدم زنهار جوی</p></div>
<div class="m2"><p>ورنه کو زخمی که از در دم بزنهار آورد</p></div></div>