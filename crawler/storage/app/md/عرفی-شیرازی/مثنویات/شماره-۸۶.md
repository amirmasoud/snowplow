---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>چو با من در سخن آن لعل آتشناک خواهد شد</p></div>
<div class="m2"><p>بکامم هرچه زهر است از لبش تریاک خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجوم عاشقان در کوی او افزود و خوشحالم</p></div>
<div class="m2"><p>کزین پس در هلاک دوستان بیباک خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه غم گر دامن پاکت بخونم گردد آلود</p></div>
<div class="m2"><p>که فردا هم بآب دیده من پاک خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیم نومید اگر دستم بود کوته ز دامانش</p></div>
<div class="m2"><p>چو میدانم که در جولانگه او خاک خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمست افتادنم در مسجدای زاهد مشو رنجه</p></div>
<div class="m2"><p>که صحن مسجدت فردا زمین تاک خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه چاک پیرهن میدوزی ای زاهد و زین غافل</p></div>
<div class="m2"><p>که تا دامن گریبان کفن هم چاک خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شود سودای پابوس تو افزون درسر عرفی</p></div>
<div class="m2"><p>به این زودی همانا بسته فتراک خواهد شد</p></div></div>