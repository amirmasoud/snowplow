---
title: >-
    شمارهٔ ۲۶ - بامداد شیرین
---
# شمارهٔ ۲۶ - بامداد شیرین

<div class="b" id="bn1"><div class="m1"><p>صباحی دلگشا چون خنده حور</p></div>
<div class="m2"><p>که شادی مست بود اندوه مستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تتق می بست ابر نو بهاران</p></div>
<div class="m2"><p>چمن مشتاق شیرین بود و یاران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چمن برابر سودی سر و سیراب</p></div>
<div class="m2"><p>چراغ برق گشتی شاخ عناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمین طناز و گردون خشمگین بود</p></div>
<div class="m2"><p>که با آن زهره با این یاسمین بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عروسی در عروسی در درو دشت</p></div>
<div class="m2"><p>صبا مشاطگی میکرد و میگشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمهد ناز شیرین در شکر خواب</p></div>
<div class="m2"><p>گلش را خوی ز شبنم کرده شاداب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی بیدار وگه در خواب بودی</p></div>
<div class="m2"><p>گهی بستی نظر گاهی گشودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا بوی گلش دادی ره آورد</p></div>
<div class="m2"><p>شکر خواب صبوحش تلخ میکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسیم باغ گفتی در دماغش</p></div>
<div class="m2"><p>مقیمم تا برم در صحن باغش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گلی در گلشن آرم مست و چالاک</p></div>
<div class="m2"><p>که هر گل صد گریبانرا زند چاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بوی گل درآمد عطر در تاب</p></div>
<div class="m2"><p>بیک عطسه تهی شد چشمش از خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدل گفتا که هنگام صبوحست</p></div>
<div class="m2"><p>نسیم باغ و می معجون روحست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هوای ابرو بیم آفتابست</p></div>
<div class="m2"><p>همانا ترک آرایش صوابست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بی سرمه ماند چشم غم نیست</p></div>
<div class="m2"><p>تماشای خطش از سرمه کم نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عبیر امروز در جیبم نگنجد</p></div>
<div class="m2"><p>وگر گنجد نسیم گل برنجد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرامش کرده عمدا شستن روی</p></div>
<div class="m2"><p>که در گلزار شوید بر لب جوی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز جام شیشه سامان طرب کرد</p></div>
<div class="m2"><p>نقاب افکند و گلگون را طلب کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دوانیدند گلگون پیش راهش</p></div>
<div class="m2"><p>ندیدند آشنایی در نگاهش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نهان بودش چراغی زیر دامن</p></div>
<div class="m2"><p>بدل کردند گلگونرا بتوسن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان چابک بران بنشست و بشتافت</p></div>
<div class="m2"><p>که دستش را عنان در نیمره یافت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پرستاران خواب آلود و مخمور</p></div>
<div class="m2"><p>پریشان زان گهی نزدیک وگه دور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چنین رفتند تا نزدیک باغی</p></div>
<div class="m2"><p>هنوز آگه نه از بویش دماغی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نمودی از برون دیوار گلشن</p></div>
<div class="m2"><p>برنگ جامه فانوس روشن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درون آمد چو شمعی در شبستان</p></div>
<div class="m2"><p>دمی استاد بر درگاه بستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رسوم حاجبی و دیده بانی</p></div>
<div class="m2"><p>همی آراست رمزی و بیانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتا این حرمگاهست نی باغ</p></div>
<div class="m2"><p>که آنجا بار طاووس است نی زاغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر حور آمد این دروازه بستست</p></div>
<div class="m2"><p>بکوبد در کلید او شکست است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر آید باغبان گوئید میسوز</p></div>
<div class="m2"><p>که در باغ آتش آفتاده است امروز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نسیم از در درآید نی ز دیوار</p></div>
<div class="m2"><p>چو آید خلوتی باشد نه طرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وگر بیرون شتابد باد غماز</p></div>
<div class="m2"><p>بگیریدش که بوی ما دهد باز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وگر از بیستون پیغام آید</p></div>
<div class="m2"><p>نشیند تا اجابت در گشاید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو لعلش سیر گشت از درفشانی</p></div>
<div class="m2"><p>روان شد همچو آب زندگانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>روش داد آنچنان سرو روان را</p></div>
<div class="m2"><p>که از رشک زمین کشت آسمان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دلش از بند نامحرم رها شد</p></div>
<div class="m2"><p>نقابش غنچه و دستش صبا شد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نقاب از روی خود چون کرد مهجور</p></div>
<div class="m2"><p>گذشت از تارک سرو چمن نور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان گلشن زحسنش بهره ور شد</p></div>
<div class="m2"><p>که رنگ گل شگفت و تازه تر شد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز شکر خنده آن لعل شادات</p></div>
<div class="m2"><p>تبسم در دهان غنچه شد آب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بهر سو جلوه کرد آن چشم غماز</p></div>
<div class="m2"><p>خیابان در خیابان عشوه وناز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شمال آمد باستقبال بویش</p></div>
<div class="m2"><p>ولی در راه ماند از بیم خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هوا بروی عبیری کز چمن ریخت</p></div>
<div class="m2"><p>نخستش از حریر یاسمن بیخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بهر سو میچمید آن رشک طوبی</p></div>
<div class="m2"><p>نهانی می شکست از موج چوبی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>صبا تا دید او را در چمیدن</p></div>
<div class="m2"><p>نیارستی بشاخ گل وزیدن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو داده آنماه داد دلستانی</p></div>
<div class="m2"><p>یکایک عاشقان بوستانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>سرودندی بمعشوقان آگاه</p></div>
<div class="m2"><p>کنایت گونه از مهر آنماه</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بسرو این نغمه بلبل داشت در کار</p></div>
<div class="m2"><p>که بلبل را بگل زین پس چه آزار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>صنم میرفت و گلهای بهاری</p></div>
<div class="m2"><p>ز مرغان چمن در شرمساری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو دیدی سروشاه از دیده میرست</p></div>
<div class="m2"><p>چو خواندی فاخته فرهاد میمبت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تعالی الله چه خرم بوستانی</p></div>
<div class="m2"><p>ازو فردوس را هردم نشانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنان پر میوه جیب شاخسارش</p></div>
<div class="m2"><p>که گل ناکرده نو گردد بهارش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سراسر ناف آهو بید مشکش</p></div>
<div class="m2"><p>ز می مستی فزاتر تاک خشکش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بنوعی سنبلش مغرور و فتان</p></div>
<div class="m2"><p>که تمثیلش بزلف حور نتوان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>درختان جسته شوخ از جامه خواب</p></div>
<div class="m2"><p>همه خوی کرده و سرسبز و شاداب</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چنار سالخورده سر و نوخیز</p></div>
<div class="m2"><p>ز هم نشناختی بیننده تیز</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز روی سبزه سنبل رفته در تاب</p></div>
<div class="m2"><p>ز بوی گل بنفشه جسته از خواب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هوا ساقی و خار و گل قدح نوش</p></div>
<div class="m2"><p>چکاوک نغمه زن دیوار در نوش</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بآب از سایه گل آتش سپرده</p></div>
<div class="m2"><p>سمندر غوطه ها در آب خورده</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>صبا کز فیض نرگس شد سرابی</p></div>
<div class="m2"><p>گزد هر دم لبش در نیم خوابی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بحسن سرو واله شد چنان گل</p></div>
<div class="m2"><p>که صوت فاخته جوید زبلبل</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سراسیمه تذرو از حسن شمشاد</p></div>
<div class="m2"><p>ز سرو افتاده در دامان صیاد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چمن در دست گویی جام جم داشت</p></div>
<div class="m2"><p>که هر نقشی که بود از بیش و کم داشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ز خود رو سرو تا پرورده نسرین</p></div>
<div class="m2"><p>همه تمثال خسرو بود و شیرین</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو گویی باغبانی در رحم داشت</p></div>
<div class="m2"><p>که شکل نطفه ها زینگونه بنگاشت</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>صنم دلشاد از این عیش نهانی</p></div>
<div class="m2"><p>که از بازیچه های آسمانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>فضولی از کنیزان غلط ساز</p></div>
<div class="m2"><p>گشاد آن درکه محکم برکند باز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بناگه فیلسوفی نامه در دست</p></div>
<div class="m2"><p>ز طراران شاه از در درون جست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>سمومی از در گلشن درون تاخت</p></div>
<div class="m2"><p>که ناگه یکچمن گل رنگ درباخت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>نفسها سرد و بر لبها سرانگشت</p></div>
<div class="m2"><p>جبینها زرد و بر دیوارها پشت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>کنیزان سیه بخت اندرین کار</p></div>
<div class="m2"><p>همه حیرت زده چون نقش دیوار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نه بتوان آشنائی را عنان تافت</p></div>
<div class="m2"><p>نشاید کوچه بیگانگی یافت</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>متاع مصلحت صد رنگ چیدند</p></div>
<div class="m2"><p>گهی بفروختند وگه خریدند</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>یکی گفت این جماعت رمز دانند</p></div>
<div class="m2"><p>بمنع آشنا بیگانه رانند</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>یکی گفت این تمنا دلنشین است</p></div>
<div class="m2"><p>ولی فرمانبرانرا ره نه اینست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یکی گفتا ز حسن این شیوه آید</p></div>
<div class="m2"><p>که نازی روکش رغبت نماید</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یکی گفتا که حسنست این و سر مست</p></div>
<div class="m2"><p>اگر خواهد وگرنه رنجشی هست</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>یکی گفت از مروت ریش بودن</p></div>
<div class="m2"><p>گواراتر که راحت کیش بودن</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زخشم و ناز تا دشنام و شمشیر</p></div>
<div class="m2"><p>پذیرفتم زدم سر پنجه با شیر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>زد این دستان و دردم شد خرامان</p></div>
<div class="m2"><p>بدستی جان بدستی طرف دامان</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>گزیدی لب گهی از خود نهفته</p></div>
<div class="m2"><p>شکستی رنگ و رویش رفته رفته</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بدید از دور شمشاد گل اندام</p></div>
<div class="m2"><p>که میآید کنیزی نا بهنگام</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>لبش زین گفتگو در پوست خندید</p></div>
<div class="m2"><p>چو پیش آمد بحکم غمزه پرسید</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>کنیزی شیر دل آواز برداشت</p></div>
<div class="m2"><p>که ای صبح قیامت از رخت چاشت</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>حریمت قبله گاه کج کلاهان</p></div>
<div class="m2"><p>نسیمت باج خواه مغز شاهان</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>همین دم گرم رویی آمد از راه</p></div>
<div class="m2"><p>بدستش نامه سربسته شاه</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>اگر فرمان دهد شاه سبکدل</p></div>
<div class="m2"><p>بیارد نامه شاه تنگ دل</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>چو بشنید این سخن طاووس طناز</p></div>
<div class="m2"><p>گرفت از مو بمویش فتنه پرواز</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چنان رنگش برآشفت وزجا شد</p></div>
<div class="m2"><p>که یک یک تار زلف از هم جدا شد</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>ضمیرش در صد اندیشه می سفت</p></div>
<div class="m2"><p>بتمکین سر همی جنباند و میگفت</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>بشاه این شوخ چشمانرا سری هست</p></div>
<div class="m2"><p>وگر با شاه نی با دیگری هست</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>وگر نه هر که را دل باشد و هوش</p></div>
<div class="m2"><p>نگردد آن سفارشها فراموش</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>عتابش گفت میباید ادب کرد</p></div>
<div class="m2"><p>مگر سهو است کین سهو عجب کرد</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>پذیرفت این سخن از جای برخاست</p></div>
<div class="m2"><p>گلستانرا بحسن جلوه آراست</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>چو از رفتار طاووسانه خویش</p></div>
<div class="m2"><p>دماغش ترشد از جانانه خویش</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>گذر بر عفونا آمیزش افتاد</p></div>
<div class="m2"><p>کنهکار از پی قاصد فرستاد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بسروی تکیه زد بر طرف جویی</p></div>
<div class="m2"><p>که از صهبا کند خالی سبویی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>در افتاد از جمالش عکس در آب</p></div>
<div class="m2"><p>تو گفتی بیستونرا دید در خواب</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>هوای بیستونش در سر افتاد</p></div>
<div class="m2"><p>بتکلیف آمدش امید فرهاد</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>یکی ساغر زساقی خواست لبریز</p></div>
<div class="m2"><p>که عزم راه طبعش را کند تیز</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بشاهی کش وفا تعویذ بازوست</p></div>
<div class="m2"><p>بدرویشی که شاهش همترازوست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بنوشی طعنه زن یعنی لب شاه</p></div>
<div class="m2"><p>بجوش حسن من یعنی بت ماه</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>بنازی کز عتاب شاه کم نیست</p></div>
<div class="m2"><p>بحکمی کش علامت در عدم نیست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بیاقوتی که جان داروی شاهست</p></div>
<div class="m2"><p>بهاروتی گر نرگس دانش چاهست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بنا موسی که بر شیرین وبالست</p></div>
<div class="m2"><p>بطاووسی که پایش رشک بالست</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بشمعی کش سخن با آفتابست</p></div>
<div class="m2"><p>بفانوسی که یک نامش نقابست</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بتشویشی که با من هم سرشتست</p></div>
<div class="m2"><p>باندوهی که از من در بهشتست</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بگیسوئی که دانی چند تاراست</p></div>
<div class="m2"><p>بمژگانی که بینی در چه کار است</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بحسن من که شهر آشنائیست</p></div>
<div class="m2"><p>بعشق من که صیدش روستائیست</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بآب دیده فرهاد مهجور</p></div>
<div class="m2"><p>بدین روئی ز چشم کوهکن دور</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به پیوندی که با جان در میانست</p></div>
<div class="m2"><p>بسوگندی که با دل در زبانست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>به بهتانی که سنگ راه صلحست</p></div>
<div class="m2"><p>بآغوشی که عشرتگاه صلحست</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>که تا مالیده فرهاد آستین را</p></div>
<div class="m2"><p>ندیده پشت گلگون روی زین را</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>نه گلگون از شرف بر خویش بالید</p></div>
<div class="m2"><p>نه گوش بیکسی فرهاد مالید</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>اگر باشد هم این نسبت نبودی</p></div>
<div class="m2"><p>کجا بر من در تهمت گشودی</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>همایی چون تو باید بال گستر</p></div>
<div class="m2"><p>که در ظلش بر آرد چون منی سر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بسی بسیار با هم مهر بانند</p></div>
<div class="m2"><p>که آمیزش بهم لایق ندانند</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نباشند از رهم خوشدل ببوئی</p></div>
<div class="m2"><p>نماند دوستی را آبروئی</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>زناموسم ز کف چندین مثالست</p></div>
<div class="m2"><p>که فرهادت بدین نسبت حلالست</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>چنان تهمت کزان حنظل شود قند</p></div>
<div class="m2"><p>کجا باور کند شاه خردمند</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو رسم شه بود جوری که دیدم</p></div>
<div class="m2"><p>کشیدن عیب کس نبود کشیدم</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>چو طی شد نامه رو از گفتگو تافت</p></div>
<div class="m2"><p>به پیش نامه بر افکند و رو تافت</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بعهدی کان غلط راند آن دعا باز</p></div>
<div class="m2"><p>کجا بودم که باشه گویم این راز</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>که در سوگند داد صدق دادست</p></div>
<div class="m2"><p>نه بر گلگون بتوسن زین نهاد است</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>غلط گویم کجا لب می‌گشودم</p></div>
<div class="m2"><p>به کجبازیش صدره می‌نمودم</p></div></div>