---
title: >-
    شمارهٔ ۱۷۵
---
# شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>ای نه فلک ز خوشه صنع تو دانه</p></div>
<div class="m2"><p>وز قصر کبریای تو عرش آستانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تنگنای کوچه شهر جلال تو</p></div>
<div class="m2"><p>وسعت گه زمانه کمین کار خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرواز گاه طائر صنعت کجا بود</p></div>
<div class="m2"><p>جائی که دارد از دو جهان آشیانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه توسن سپهر سراسیمه میدود</p></div>
<div class="m2"><p>تا حکمتت گرفته بکف تازیانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذات تو قادر است بایجاد هر محال</p></div>
<div class="m2"><p>الا بآفریدن چون خود یگانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عفوت ثواب دشمن و حلمت گناه دوست</p></div>
<div class="m2"><p>هر گام چیده عاطفتت آب و دانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی تمام معصیت اما بدست او</p></div>
<div class="m2"><p>هست از عنایت تو عنان بهانه</p></div></div>