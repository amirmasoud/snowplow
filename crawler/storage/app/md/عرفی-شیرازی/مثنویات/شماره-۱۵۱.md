---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>دانی که کیست مصلحت ما گریستن</p></div>
<div class="m2"><p>پنهان ملول بودن و تنها گریستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمرم بگریهای هوس صرف شد کنون</p></div>
<div class="m2"><p>عمری بتازه باید حالا گریستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درمان درد من زمسیحا مجو که هست</p></div>
<div class="m2"><p>دردم جفای یار و مداوا گریستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاهی بیاد سرو قدی گریه هم خوش است</p></div>
<div class="m2"><p>تا کی زشوق سدره طوبی گریستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کس که هست گریه بجانش رواست بس</p></div>
<div class="m2"><p>نتوان به عالمی تن تنها گریستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی زگریه دست نداری که در فراق</p></div>
<div class="m2"><p>دردت زدل نمیبرد الا گریستن</p></div></div>