---
title: >-
    شمارهٔ ۱۲ - غفلت
---
# شمارهٔ ۱۲ - غفلت

<div class="b" id="bn1"><div class="m1"><p>ای گهر گنج ادب نام ما</p></div>
<div class="m2"><p>وی اثر رنج طلب نام ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در طلب آویز چه بنشسته</p></div>
<div class="m2"><p>بسته دامی ز چه وارسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه فلک بسته در کامها</p></div>
<div class="m2"><p>کرده به نگشودنش ابرامها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نحفه فرهاد به شیرین فشاند</p></div>
<div class="m2"><p>ناله شبدیز بگلگون رساند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راه طلب جوی و نه بیهوده رو</p></div>
<div class="m2"><p>دست ادب گیرد و بفرموده رو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تارسی از دیر به بیت الحرام</p></div>
<div class="m2"><p>طایر باغ حرم آری بدام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فوج طیور از همه سو نغمه سنج</p></div>
<div class="m2"><p>دام ترا خنده زنان بر شکنج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغ مراد آمده صدره بدام</p></div>
<div class="m2"><p>بسکه بدام آمده گردیده رام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلکه زامنیت انس مکان</p></div>
<div class="m2"><p>بر زبر دام گرفت آشیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیضه هم آورد برون و شکست</p></div>
<div class="m2"><p>بچه او باطیران عهد بست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باز شعور تو همان بسته بال</p></div>
<div class="m2"><p>بخت تو در خواب که خوابش حلال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پای تو برداشته صد زخم مار</p></div>
<div class="m2"><p>گنج هم از کوبش پایت فکار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هیچ گمان برده از این رنج نه</p></div>
<div class="m2"><p>هیچ تماشائی از آن گنج نه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روی شعور تو بمی شسته اند</p></div>
<div class="m2"><p>جلوه لیلیت زحی بسته اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون تو بدین صید نه ارزنده</p></div>
<div class="m2"><p>بهر چه دام طلب افکنده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>برتو حرام ابد این گنج کام</p></div>
<div class="m2"><p>راه طلب بیش میالا بگام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مستی و از فیض طلب رسته</p></div>
<div class="m2"><p>بی اثری را بطلب بسته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مستی غفلت نه پذیرفته اند</p></div>
<div class="m2"><p>ورنه بمستی همه درسفته اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وانکه برازنده امیدهاست</p></div>
<div class="m2"><p>تحفه او جنبش امید ماست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مردمک دیده دیدار دوست</p></div>
<div class="m2"><p>آبله پای طلبکار اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر طلب گنج کنی هوش دار</p></div>
<div class="m2"><p>بر نفس گنج وران گوشدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شیوه گوهر طلبان پیشه کن</p></div>
<div class="m2"><p>گر مروی وام ز اندیشه کن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صدره و صدکوچه درین شهر هست</p></div>
<div class="m2"><p>هر قدمی چشمه از زهر هست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یعنی از آن لعل که دل نام اوست</p></div>
<div class="m2"><p>آب ستان بهر لب جرعه دوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ور بطعامی کنی آلوده دست</p></div>
<div class="m2"><p>بره بریان تو در سینه هست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گرچه مرا هست هزاران هزار</p></div>
<div class="m2"><p>لیک ره راست یکی زان شمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا بنگاهی شوی آگه زراه</p></div>
<div class="m2"><p>مست و سراسیمه نماند نگاه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ریزه گوهر بلب افشانده اند</p></div>
<div class="m2"><p>تا در گنجینه ترا خوانده اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیده در بسته زهم باز کن</p></div>
<div class="m2"><p>قاعده رهروی آغاز کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شرم کن از همت و برتر شتاب</p></div>
<div class="m2"><p>تا شوی از رنج طلب گنج یاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر درگنجینه چو آری گذر</p></div>
<div class="m2"><p>بر تو فشاند در و بام الحذر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هیچ میندیش بکام ادب</p></div>
<div class="m2"><p>درشو و مگذار عنان طلب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر چه نتابد اجل او را عنان</p></div>
<div class="m2"><p>رو که باعجاز طلب می توان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پای منه بردم او قهرناک</p></div>
<div class="m2"><p>بر سر او کوب که گردد هلاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>وانگه از آن گنج ببرمزد رنج</p></div>
<div class="m2"><p>نغز در آویز بدامان گنج</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ای برهت دست طلب گنج ریز</p></div>
<div class="m2"><p>برگ ره آنست وره اینست خیز</p></div></div>