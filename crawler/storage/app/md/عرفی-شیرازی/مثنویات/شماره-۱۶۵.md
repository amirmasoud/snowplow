---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>ای عشق خوش تهیه لذات کرده</p></div>
<div class="m2"><p>طوطی سدره وقف خرابات کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نازم ببازی تو که در عرصه قریب</p></div>
<div class="m2"><p>منصوبه نچیده مرامات کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفی بگفته صیغه توحید باطل است</p></div>
<div class="m2"><p>یعنی که در معامله ذات کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد بیا که کفر تو ثابت کنم که تو</p></div>
<div class="m2"><p>کفر مرا بدین خود اثبات کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی دگر بطور تمنا مرو ببین</p></div>
<div class="m2"><p>کامشب چها بجان مناجات کرده</p></div></div>