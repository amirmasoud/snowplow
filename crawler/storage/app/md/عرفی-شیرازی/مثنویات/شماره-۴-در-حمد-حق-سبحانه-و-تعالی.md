---
title: >-
    شمارهٔ ۴ - در حمد حق سبحانه و تعالی
---
# شمارهٔ ۴ - در حمد حق سبحانه و تعالی

<div class="b" id="bn1"><div class="m1"><p>ای تو بامرزش و آلوده ما</p></div>
<div class="m2"><p>وی تو بغمخواری و آسوده ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحمت تو کعبه طاعت نواز</p></div>
<div class="m2"><p>عدل تو مشاطه عصیان طراز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطف تو دلال متاع گناه</p></div>
<div class="m2"><p>حلم تو بنشانده غضب را پناه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منفعلیم از عمل ناسزا</p></div>
<div class="m2"><p>گر همه نیکست بپوشان ز ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی ما ز ریا شرمسار</p></div>
<div class="m2"><p>بندگی از نسبت ما شرمسار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ابد از معصیت آزرم ده</p></div>
<div class="m2"><p>حوصله ضامن این شرم ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من که و سنجیدن بازوی عدل</p></div>
<div class="m2"><p>به که نباشم بترازوی عدل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ور کرمت میزندم در دهان</p></div>
<div class="m2"><p>تا بگشایم لب خواهش فشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم و دلم گرسنه چشمان تو</p></div>
<div class="m2"><p>سیر نگردند ز احسان تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه بآن می سزم آنم بده</p></div>
<div class="m2"><p>برتر از آن نیز عنانم بده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صاف امیدم بلب بیم ریز</p></div>
<div class="m2"><p>گرد مرا در ره تسلیم ریز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کام مرا شهد عبادت ببخش</p></div>
<div class="m2"><p>چون بچشم فهم حلاوت ببخش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شهپر جبریل نیازم بده</p></div>
<div class="m2"><p>راه بخلوتکه رازم بده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در حرم عشق درون آورم</p></div>
<div class="m2"><p>شیفته و مست برون آورم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>این گل پژمرده که در باغ جود</p></div>
<div class="m2"><p>دست بدست آورمش در وجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رایحه عطر وفایش بده</p></div>
<div class="m2"><p>گوشه دستار رضایش بده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا بدماغی که رساند نسیم</p></div>
<div class="m2"><p>غش کند اندیشه امید وبیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشاه توحید در آید بجوش</p></div>
<div class="m2"><p>مستی جاوید بر آرد خروش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بحر عطای تو جواهر شمار</p></div>
<div class="m2"><p>بی اثر باد طلب موج زار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا طلبم وای که دل خون کنم</p></div>
<div class="m2"><p>خواهشم آموخته چون کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با نفس این نغمه بشوئیم به</p></div>
<div class="m2"><p>حرف ادب سوز نگوئیم به</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>طره خواهش برضا نشکنیم</p></div>
<div class="m2"><p>بال و پر مرغ دعا نشکنیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عرفی از این نغمه زنی شرم دار</p></div>
<div class="m2"><p>عهد طلب بشکن و دل گرم دار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مصلحت کار چه دانیم ما</p></div>
<div class="m2"><p>بذر تمنا چه فشانیم ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آدمی هیچ تر از هیچ کیست</p></div>
<div class="m2"><p>تا کند اندیشه از بهر زیست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دیدی اگر مصلحتی در عدم</p></div>
<div class="m2"><p>بر اثر آن زدی اکنون قدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مصلحت ما دگری دیده است</p></div>
<div class="m2"><p>او بکند هر چه پسندیده است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شادم از او گر غم وگر شادیست</p></div>
<div class="m2"><p>معنی این بندگی آزادیست</p></div></div>