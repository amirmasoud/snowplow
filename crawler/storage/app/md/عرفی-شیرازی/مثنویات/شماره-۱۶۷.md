---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>تا مژده زخم دگر دامن کش جان کرده</p></div>
<div class="m2"><p>دشوار دادن جان من خوش بر من آسان کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستانه گریند از غمت اهل ورع در صومعه</p></div>
<div class="m2"><p>گویا تبسم گونه ای در کار ایشان کرده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش با دل جمع آمدی نازان بحسن خویشتن</p></div>
<div class="m2"><p>از عشوه گویا هر طرف دلها پریشان کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنار عصمت پیشگان پوشند عیب برهمن</p></div>
<div class="m2"><p>خوش توتیای آفتی در چشم انسان کرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر و وفا را جذبه می باشد ای اهل طلب</p></div>
<div class="m2"><p>رو گوشه بنشین چرا رو در بیابان کرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمی که بازش کرده از گریه خون آمد ولی</p></div>
<div class="m2"><p>خون گرید آن چشمی که تو پاکش بدامان کرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در حشر اگر نشناسدت معذور باید داشتن</p></div>
<div class="m2"><p>چشمی که از نظاره آن چهره حیران کرده</p></div></div>