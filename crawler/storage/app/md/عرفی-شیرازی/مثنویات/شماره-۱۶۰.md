---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>دانی که چیست مصلحت ما گریستن</p></div>
<div class="m2"><p>پنهان ملول بودن و تنها گریستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدرد را بصحبت ارباب دل چکار</p></div>
<div class="m2"><p>خندیدن آشنا نبود با گریستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایم بگریه غرقم و چون نیک بنگرم</p></div>
<div class="m2"><p>زین گریه ره دراز بود تا گریستن</p></div></div>