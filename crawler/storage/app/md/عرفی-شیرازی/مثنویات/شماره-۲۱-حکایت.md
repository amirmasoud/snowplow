---
title: >-
    شمارهٔ ۲۱ - حکایت
---
# شمارهٔ ۲۱ - حکایت

<div class="b" id="bn1"><div class="m1"><p>دید یکی باشه دراج قوت</p></div>
<div class="m2"><p>تافتن و بافتن عنکبوت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ریخت ببافند گیش زهر خند</p></div>
<div class="m2"><p>کی هوس اندیشه کوته کمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شربت دل ریزی و خون جگر</p></div>
<div class="m2"><p>تا مگسی را بربائی مگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیف که سرمایه این پود و تار</p></div>
<div class="m2"><p>از تو بود دام مگس را بکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دام چنین صید نیرزد بهیچ</p></div>
<div class="m2"><p>پیش بر آن رشته تنیدن مپیچ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طعنه زنان چون خذف هرزه سفت</p></div>
<div class="m2"><p>دام طرازنده بجوشید و گفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته این دام تنیدن خطاست</p></div>
<div class="m2"><p>صید تو معلوم که چندش بهاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه بود جذب کمندش بلند</p></div>
<div class="m2"><p>نیست غم ار گوتهش افتد کمند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خود ثمر کوتهی آنجا نرست</p></div>
<div class="m2"><p>کوتهی ار هست بر باع تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این دم سرد از جگرم دور کن</p></div>
<div class="m2"><p>شرمی از این جنبش منصور کن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دام من آنست که در راف غار</p></div>
<div class="m2"><p>کرد رسول عربی را شکار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز آلهیش در آمد بقید</p></div>
<div class="m2"><p>طوطی باغ قدمش بود صید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نغمه طرازنده بستان دوست</p></div>
<div class="m2"><p>طایر سر حلقه مرغان دوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایه نیفکنده بر این چار باغ</p></div>
<div class="m2"><p>سایه فکن بر یر طاووس زاغ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دام چنین صید مگس گیر نیست</p></div>
<div class="m2"><p>ور فتدش داخل نخجیر نیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دام من آنست که طاووس جان</p></div>
<div class="m2"><p>در کنفش داشته است آشیان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عرفی اگر دام تر صید نیست</p></div>
<div class="m2"><p>حیف برآن است که برقید نیست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بسته این دام کلید مراد</p></div>
<div class="m2"><p>رشته بندش گره بی گشاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بسته او گر زغن وگر تذرو</p></div>
<div class="m2"><p>خرم و آزاد بر آید چو سرو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سرو که آزادیش آمد به کف</p></div>
<div class="m2"><p>خوانده ز مکتوب حزن لاتخف</p></div></div>