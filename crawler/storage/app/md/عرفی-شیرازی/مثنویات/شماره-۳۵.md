---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>صبح گدا و شام زخورشید روشن است</p></div>
<div class="m2"><p>گر قادری به بخش چراغی بشام ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را بکام خویش بدید و دلش بسوخت</p></div>
<div class="m2"><p>دشمن که هیچگاه مبادا بکام ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خلوتی که دختررزنیست عیش نیست</p></div>
<div class="m2"><p>داغست شیخ شهر زعیش مدام ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در روزگار نیست رسولی که بی حسد</p></div>
<div class="m2"><p>درگوش چون توئی برساند پیام ما</p></div></div>