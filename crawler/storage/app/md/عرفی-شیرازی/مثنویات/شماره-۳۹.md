---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>ترک جان در ره آن سرو روان این همه نیست</p></div>
<div class="m2"><p>عشق اگر نرخ نهد قیمت جان این همه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جزو قیمت نیم اما به قناعت شادم</p></div>
<div class="m2"><p>کآنچه محصول زمینست و زمان این همه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغبان را از عشوه گل دل بگرفت</p></div>
<div class="m2"><p>ورنه پژمردگی بیم خزان این همه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر از شعبده دلگیر شود شعبده باز</p></div>
<div class="m2"><p>دل قوی دارکه دستان جهان این همه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفتی به ز ریا نیست مگر زاهد را</p></div>
<div class="m2"><p>ورنه چون باد بروت دگران این همه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منزل صلح میان تو دراز است فغان</p></div>
<div class="m2"><p>ورنه در دین تو با کیش مغان این همه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوق ما راه تماشاگه خود نشناسد</p></div>
<div class="m2"><p>ورنه آرایش گلزار جنان این همه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر توفیق مگر راهبرت شد عرفی</p></div>
<div class="m2"><p>ورنه خود رهبری نام و نشان این همه نیست</p></div></div>