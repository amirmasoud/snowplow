---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>زمن نبود فغانی که دوش میکردم</p></div>
<div class="m2"><p>نصیحت غم روی تو گوش میکردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان نه شیوه اهل دلست ای بلبل</p></div>
<div class="m2"><p>وگرنه من زتو افزون خروش میکردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم بمجمع افسردگان قدم میرفت</p></div>
<div class="m2"><p>بناله همه را شعله نوش میکردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زصد وصال نیاید شب آنچه من بخیال</p></div>
<div class="m2"><p>زشیوه های تو با عقل و هوش میکردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان حلاوت لعل تو می ستودم دوش</p></div>
<div class="m2"><p>که نیش را متأثر زنوش میکردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بر ازفشانی لبم اجازت داشت</p></div>
<div class="m2"><p>چها بعابد طاعت فروش میکردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهم باینهمه تردامنی همان عرفی</p></div>
<div class="m2"><p>که عیب زاهد پشمینه پوش میکردم</p></div></div>