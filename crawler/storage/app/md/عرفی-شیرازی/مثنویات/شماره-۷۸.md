---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>جان بیاد لبت شکر خاید</p></div>
<div class="m2"><p>دل بدندان غم جگر خاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظن سیری مبرکه لقمه خام</p></div>
<div class="m2"><p>بخت پیر است و دیرتر خاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل آشفته بخت من تا چند</p></div>
<div class="m2"><p>جای انگشت نیشتر خاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه گیرد مزاج پروانه</p></div>
<div class="m2"><p>شعله چون میوه های تر خاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس که یابد حلاوت از پرواز</p></div>
<div class="m2"><p>طایر شوق بال و پرخاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب شادی به بست یکچندی</p></div>
<div class="m2"><p>عرفی اکنون لب دگر خاید</p></div></div>