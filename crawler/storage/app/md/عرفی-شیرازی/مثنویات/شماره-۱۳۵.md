---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>میفروشم راحت و عشق ستمگر میخرم</p></div>
<div class="m2"><p>میدهم روز خوش و آسیب اختر میخرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که باز افکنده در تیغ گاه رغبتم</p></div>
<div class="m2"><p>گر متاع غم بود بکشا که اکثر میخرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سررشت من قبول شیوه انکار نیست</p></div>
<div class="m2"><p>ساده لوحم هر چه بفروشند یکسر میخرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک جان تلخکامست و شکر خواب عدم</p></div>
<div class="m2"><p>جام زهری میفشانم تنگ شکر میخرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او بخونم گرم و من زین شادمان کز شکر قتل</p></div>
<div class="m2"><p>صدره ازوی خون خود در روز محشر میخرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست غم کز درد هجران شهپرم برخاک ریخت</p></div>
<div class="m2"><p>اینک از جبریل شوقت باز شهپر میخرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر متاعی کز نگاهش میخرم در بزم وصل</p></div>
<div class="m2"><p>می نشینم گوشه وزخود مکرر میخرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی آوردم متاعی ترازو گوغم کجاست</p></div>
<div class="m2"><p>کان متاعی کس مخر با جان برابر میخرم</p></div></div>