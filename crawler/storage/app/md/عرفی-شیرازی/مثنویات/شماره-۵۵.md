---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>کسی کو در تب عشق تو نبض خویشتن گیرد</p></div>
<div class="m2"><p>نه عیب خودپرستی هر زمان بر مرد و زن گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم عیسی بخنداند گل امید صیادی</p></div>
<div class="m2"><p>که در فصل بهاران دام او مرغ چمن گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مه کنعان بخوابست ای صبا بر برهمن بگذر</p></div>
<div class="m2"><p>که گرگی ناگهان دنبال بوی پیرهن گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ازآن با رعشق هرگز التفاتی نیست تقوی را</p></div>
<div class="m2"><p>که عاشق نکته با زاهد بکیش برهمن گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم در گوشه تنها که ریزم خون خود عرفی</p></div>
<div class="m2"><p>مبادا وقت مردن ناشناسی دست من گیرد</p></div></div>