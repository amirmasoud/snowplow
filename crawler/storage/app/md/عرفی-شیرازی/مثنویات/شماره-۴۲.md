---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>گر بدیرم طلبد مغبچه حور سرشت</p></div>
<div class="m2"><p>بیم دوزخ برم از یاد چو امید بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسبت سبحه و زنار دو صد رنگ آمیخت</p></div>
<div class="m2"><p>ورنه این رشته همانست که بیرنگ سرشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشرت رفته مجو باز که دهقان فلک</p></div>
<div class="m2"><p>تخم هر کشته که بدرود دگر بار بکشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر می چو دهی بوسه زپی نیز بده</p></div>
<div class="m2"><p>بندامت به کشم گر بکنندم به بهشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک دین در ره معشون گناه است ولی</p></div>
<div class="m2"><p>نه گناه است که در نامه توانند نوشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اینقدر کعبه پرستی که تو داری عرفی</p></div>
<div class="m2"><p>از تو آید که کنی منع من از طوف کنشت</p></div></div>