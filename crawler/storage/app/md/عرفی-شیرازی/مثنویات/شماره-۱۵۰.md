---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>زخونم روی میدان تازه گردان</p></div>
<div class="m2"><p>تمنای شهیدان تازه گردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدل یک لخت دارم نیم خورده</p></div>
<div class="m2"><p>جگر بریان کن و خوان تازه گردان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعالم وقتی آسان مردنی بود</p></div>
<div class="m2"><p>ببالینم بیا آن تازه گردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر طوفان نوحی خواهی از خون</p></div>
<div class="m2"><p>کهن ریشم بمژگان تازه گردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقص ای نیم بسمل صید دردل</p></div>
<div class="m2"><p>شکستنهای مژگان تازه گردان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زچاک جامه گر دل می کشاید</p></div>
<div class="m2"><p>شکر خند گریبان تازه گردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا در خون سرشتی خاکم اکنون</p></div>
<div class="m2"><p>کهن دیوار ایمان تازه گردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز میدان رو متاب از شیر مردی</p></div>
<div class="m2"><p>مرو نام شهیدان تازه گردان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کومی شوقی که دل مست جنون آید برون</p></div>
<div class="m2"><p>هر نگاه از دیده با صد موج خون آید برون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناله تا نزدیک لب صد جا شود پامال او</p></div>
<div class="m2"><p>جان بیمار از درون سینه چون آید برون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون رود فرهاد با آن جذبه شاید گر شبی</p></div>
<div class="m2"><p>صورت شیرین زقید بیستون آید برون</p></div></div>