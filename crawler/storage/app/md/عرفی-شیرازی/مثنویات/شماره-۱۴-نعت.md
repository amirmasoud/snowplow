---
title: >-
    شمارهٔ ۱۴ - نعت
---
# شمارهٔ ۱۴ - نعت

<div class="b" id="bn1"><div class="m1"><p>پیشتر از جلوه آثار جود</p></div>
<div class="m2"><p>کز جگر شمع نمی خواست دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع ازل چهره بر افروختی</p></div>
<div class="m2"><p>نور فشاندی دل خود سوختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوستی خود بدلش کرد زور</p></div>
<div class="m2"><p>نعمت رازش بگلو گشت شور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمه مستانه زدل ساز کرد</p></div>
<div class="m2"><p>زمزمه مهر خود آغاز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان نفس گرم که از دل گشاد</p></div>
<div class="m2"><p>نور تعلق بمأثر فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژده دل داد بهر سینه</p></div>
<div class="m2"><p>نور فشان کرد هر آئینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب حیات از غم آن چشمه زاد</p></div>
<div class="m2"><p>چشمه گوهر هم از آن نم گشاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روح بود گوهری ازکان عشق</p></div>
<div class="m2"><p>مرگ بود نشأه حرمان عشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از اثر عشق پدید آمدیم</p></div>
<div class="m2"><p>زنده جاوید و شهید آمدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن و محبت همه را داده اند</p></div>
<div class="m2"><p>لیک نقاب همه نگشاده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعضی ازان میوه جوشان بخون</p></div>
<div class="m2"><p>تلخ برون آمد و شیرین درون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز برون مغز و درون پوستیم</p></div>
<div class="m2"><p>بسته دروغی که درون دوستیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گرد سر پوست شود مغز ما</p></div>
<div class="m2"><p>ننگ فنا زیستن نغز ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از پس این پرده مجو آفتاب</p></div>
<div class="m2"><p>جمله نقابست بروی نقاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مستی ما را چه شمارد کسی</p></div>
<div class="m2"><p>رو که نیرزید بمشت خسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آتش و بادی بهم آمیخته</p></div>
<div class="m2"><p>مشت گلی بر سرشان ریخته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در گره این رسن پیچ پیچ</p></div>
<div class="m2"><p>چون بگشایند چه بست است هیچ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>توده صحرای عدم تاج ما</p></div>
<div class="m2"><p>هیچتر از هیچی معراج ما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیستی از هستی ما برده ننگ</p></div>
<div class="m2"><p>تیزتر ای مرگ بسست این درنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که درین درد گران مبتلاست</p></div>
<div class="m2"><p>داروی بیهوشی مرگش دواست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابر عطا بر لب ما جرعه ریز</p></div>
<div class="m2"><p>ما بره تشنه لبی گرم خیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حسن ازل چون غم دل پرده سوز</p></div>
<div class="m2"><p>ما چو حیا بهر نظر پرده دوز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دیده ما تنگ و تماشا فراخ</p></div>
<div class="m2"><p>چون دل ازین غم نشود شاخ شاخ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لذت این نغمه بدل آشناست</p></div>
<div class="m2"><p>چشمه این شهر ندانم کجاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خضر رهی کو که نشانم دهد</p></div>
<div class="m2"><p>بر لب آن چشمه روانم دهد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا لب از آن چشمه شود مست کام</p></div>
<div class="m2"><p>تشنگی سینه بشویم تمام</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>معنی دل نغز هویدا شود</p></div>
<div class="m2"><p>هر سر مو چشمه دل زا شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کو دل گر می که ثنایش کنیم</p></div>
<div class="m2"><p>صد گهر جان بفدایش کنیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کو دل آغشته بخون جگر</p></div>
<div class="m2"><p>از جگر نزع خراشیده تر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این هوس افشان که در این سینه است</p></div>
<div class="m2"><p>دل نبود مرده دیرینه است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نام دل از مشت گلی دوربه</p></div>
<div class="m2"><p>وز علف این بتکده معمور به</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آب و علف چند در این گل رود</p></div>
<div class="m2"><p>تشنه لبی بر اثر دل رود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وای که تعمیر صدف می کنیم</p></div>
<div class="m2"><p>در گرانمایه تلف می کنیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کعبه دل و بار شکم می کشم</p></div>
<div class="m2"><p>مزبله بر روی حرم میکشم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دل حرم و دیر بود روح پاک</p></div>
<div class="m2"><p>تن چه بود هیچ ، یکی مشت خاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یارب از آن چشمه که دل نام اوست</p></div>
<div class="m2"><p>صاف معانی همه در جام اوست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آنقدری بخش که لب تر کنیم</p></div>
<div class="m2"><p>چاشنی شربت کوثر کنیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نی علفم چشمه تمامم بده</p></div>
<div class="m2"><p>کز جگر تشنه گشاید گره</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا من از آن چشمه به یاران دهم</p></div>
<div class="m2"><p>وز غم دریوزه عرفی رهم</p></div></div>