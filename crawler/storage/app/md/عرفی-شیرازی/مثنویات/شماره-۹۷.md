---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>پا بدامن درکش ایدل وز جهان ذلت مکش</p></div>
<div class="m2"><p>سهو کردم میکش و از دامنت منت مکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاف مردی میزنی در انجمن با دوست باش</p></div>
<div class="m2"><p>خویشتن را چون زنان درگوشه خلوت مکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزه را بازو مرنجان زخم راضایع مکن</p></div>
<div class="m2"><p>اینک آمد جان بلب کز کشتنم زحمت مکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمانت اینکه خاکم کشته تر دامن است</p></div>
<div class="m2"><p>آفتابست اینکه نازت میکند منت مکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهره در عافیت عرفی قبولی نیست لیک</p></div>
<div class="m2"><p>آستین غم بگیر و دامن عصمت مکش</p></div></div>