---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>امید عیش کجا و دل خراب کجا</p></div>
<div class="m2"><p>هوای باغ کجا طایر کباب کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمی نشاط جوانی بدست نتوان کرد</p></div>
<div class="m2"><p>سرور باده کجا نشاه شباب کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بذوق کلبه رندان کجاست خلوت شیخ</p></div>
<div class="m2"><p>حریم کعبه خلوت کجا شراب کجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلای دیده و دل را زپی شتابانم</p></div>
<div class="m2"><p>کسی نگویدم ای خانمان خراب کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند همتی ذره داغ می کندم</p></div>
<div class="m2"><p>وگرنه ذره کجا مهر آفتاب کجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای عشق ابد می سرود عرفی دوش</p></div>
<div class="m2"><p>کجاست مطرب و آهنگ این رباب کجا</p></div></div>