---
title: >-
    شمارهٔ ۵ - در مدح مالک قاب و قوسین
---
# شمارهٔ ۵ - در مدح مالک قاب و قوسین

<div class="b" id="bn1"><div class="m1"><p>ای طلب چشمه امید ما</p></div>
<div class="m2"><p>ذوق فروش غم جاوید ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج طلب زیر قدم سوده ایم</p></div>
<div class="m2"><p>وز طلب گنج نیاسوده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر نفسم چشمه گشای طلب</p></div>
<div class="m2"><p>هر طلبم غالیه سای طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست ادب روی زره تافتن</p></div>
<div class="m2"><p>ورنه که داند بتوره یافتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماعدم و ذات تو عین وجود</p></div>
<div class="m2"><p>دست عدم کی در هستی گشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عدم آرایش ما کرده ای</p></div>
<div class="m2"><p>گوهری از هیچ برآورده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی غلط این نغمه نه آئین بود</p></div>
<div class="m2"><p>نغمه زنی یأس برون بین بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر چه بزادیم ز بحر عدم</p></div>
<div class="m2"><p>نسبت گنج از پی این است کم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسبت این گنج به تعمیرماست</p></div>
<div class="m2"><p>زیب ده این گهر بی بهاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر خزفی از تو شود نور یاب</p></div>
<div class="m2"><p>خنده زند بر گهر آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این گهر از نور عطا بر فروز</p></div>
<div class="m2"><p>برقع مستوره نسبت بسوز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>برگ و بر باغ فتوحم بده</p></div>
<div class="m2"><p>ضعف تن و قوت روحم بده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ضعف ، چه ضعفی که ز جسم نزار</p></div>
<div class="m2"><p>سایه سیمرغ کنم آشکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر بضمیرم نهد اندیشه پای</p></div>
<div class="m2"><p>باد گرانیم بجنبد ز جای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور بفشارد، قدمی در دلم</p></div>
<div class="m2"><p>گردد از آن تحت ثری منزلم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون بضمیرم ، بپرد مرغ راز</p></div>
<div class="m2"><p>از طیرانم ، نتوان داشت باز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرغ سکون رم کند از دام من</p></div>
<div class="m2"><p>شهپر جبریل شود کام من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جلوه بمعراج معانی کنم</p></div>
<div class="m2"><p>درارنی چرب زبانی کنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وصل توام روزن ایمان شود</p></div>
<div class="m2"><p>هر سر مویم صمنستان شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>این زر اندوده بنه در گداز</p></div>
<div class="m2"><p>سکه اصلیش بر افروز باز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا نگرد چشم تماشای ما</p></div>
<div class="m2"><p>اسم تو بر لوح مسمای ما</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از ثمرات تو محمد، یکیست</p></div>
<div class="m2"><p>وین ثمر از باغ توبل اندکیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اندکی اما گل مقصود از اوست</p></div>
<div class="m2"><p>هر دو جهان از نفسش مشکبوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اندکی از میوه این بوستان</p></div>
<div class="m2"><p>هست گلوگیر همه دوستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وای که در باغ تو این مرغ دون</p></div>
<div class="m2"><p>نغمه شایسته نریزد برون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کو پر جبریلی گلزار حال</p></div>
<div class="m2"><p>تا بگشایم بهوای تو بال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میکده راز شود مشربم</p></div>
<div class="m2"><p>نغمه مستانه گشاید لبم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز شود قفل زبان بستگی</p></div>
<div class="m2"><p>زمزمه سنجد لب شایستگی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>رحمت خود بر دل عرفی گمار</p></div>
<div class="m2"><p>کشمکش چرخ از او باز دار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شام اجل کز در جان بگذرد</p></div>
<div class="m2"><p>از عدم آباد جهان بگذرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از نفسش دور مکن جود را</p></div>
<div class="m2"><p>نور شهادت بده این دود را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مژده گلزار مخلد بده</p></div>
<div class="m2"><p>برگ ره از دین محمد بده</p></div></div>