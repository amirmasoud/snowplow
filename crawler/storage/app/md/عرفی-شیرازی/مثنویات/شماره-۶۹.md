---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>گردل اهل حقیقت در راز افشاند</p></div>
<div class="m2"><p>زاهد از دامن دل گرد مجاز افشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت اینست که با اینهمه امید دلم</p></div>
<div class="m2"><p>آستین بر اثر عجز و نیاز افشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرق شبنم خلد است هرآن قطره خوی</p></div>
<div class="m2"><p>که سمند تو نگاه تک و تاز افشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه عجب کز دل محمود فرو ریزد خون</p></div>
<div class="m2"><p>گر صبا سلسله زلف ایاز افشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرنه اظهار شفق میکند از کشتن صید</p></div>
<div class="m2"><p>خون مرغان زچه در چنگل باز افشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جای رحم است به عرفی که بسی بی اثر است</p></div>
<div class="m2"><p>اشک گرمی که بشبهای دراز افشاند</p></div></div>