---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>مده تسلیم از صلح بی مدار هنوز</p></div>
<div class="m2"><p>که میشوم بفریبت امیدوار هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر گشت قیامت قیامت بوعده گاه بیا</p></div>
<div class="m2"><p>که دل نشسته در آنجا بانتظار هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدست بوس تو از ذوق جان برآمد لیک</p></div>
<div class="m2"><p>نبرده زخم از این لذت شکار هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فرو گرفت در و بام دیده را حسرت</p></div>
<div class="m2"><p>نگشته گرم نگاهم بروی یار هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خزان گرفت گلستان عیش را عرفی</p></div>
<div class="m2"><p>ندیده خرمی فصل نوبهار هنوز</p></div></div>