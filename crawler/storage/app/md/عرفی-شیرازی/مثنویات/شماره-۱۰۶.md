---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>بشرح غم نفس را ریش کردیم</p></div>
<div class="m2"><p>درون را عافیت اندیش کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمع بردیم چندان بر در عشق</p></div>
<div class="m2"><p>که از درد غمش درویش کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر رفتیم در جنت مکن عیب</p></div>
<div class="m2"><p>که اول درد و غم را پیش کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنون با ما نکرد این تیغ بازی</p></div>
<div class="m2"><p>که ما با عقل دور اندیش کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خواریم عرفی جرم اینست</p></div>
<div class="m2"><p>تحملهای بیش از بیش کردیم</p></div></div>