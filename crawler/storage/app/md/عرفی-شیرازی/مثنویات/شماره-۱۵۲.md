---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>خوش در خورست حسرت تو با گریستن</p></div>
<div class="m2"><p>بی یاد تو حلال مبادا گریستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی گریه دوستدار تو آرام گیرنیست</p></div>
<div class="m2"><p>یا کاو کاو دیده و دل یا گریستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوئی که یاد می کنمت گه گهی ولی</p></div>
<div class="m2"><p>بیهوده نیست در دل شبها گریستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازم بغمزه تو که یک کار کرده است</p></div>
<div class="m2"><p>صد ساله ره زدیده من تا گریستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من خود کیم که گریه بحالم کنی ولی</p></div>
<div class="m2"><p>می زیبدت بنرگس شهلا گریستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کام دل ز گریه میسر شود ز دوست</p></div>
<div class="m2"><p>صد سال میتوان به تمنا گریستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی حریف دیده تر نیستی ولی</p></div>
<div class="m2"><p>بسیار گریه آورد این ناگریستن</p></div></div>