---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>در آتش آمدیم و فغانی نداشتیم</p></div>
<div class="m2"><p>بودیم شمع شوق و زبانی نداشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شیوه یافتیم زمعشوق روز وصل</p></div>
<div class="m2"><p>وز بهر نیم شیوه بیانی نداشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدره بدیر وکعبه قدم رفت هیچگاه</p></div>
<div class="m2"><p>دستی نیافتیم و عنانی نداشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شیشه کاو کاو بسی عرض کرد لیک</p></div>
<div class="m2"><p>در شیشه ناشکسته فغانی نداشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایم زدیم غوطه در آتش برای خلق</p></div>
<div class="m2"><p>در هیچکس بمهر گمانی نداشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میلی نداشتیم بسودای کس ولی</p></div>
<div class="m2"><p>در هیچ شهر نرخ گرانی نداشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی بتافت پنجه ما جور بخت پیر</p></div>
<div class="m2"><p>شکر خدا که بخت جوانی نداشتیم</p></div></div>