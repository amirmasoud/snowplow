---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>کسی که بر اثر مدعای خویشتن است</p></div>
<div class="m2"><p>کشیده تیغ ستم در قفای خویشتن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که مایه امکان و شان مطلب دید</p></div>
<div class="m2"><p>اگر ملول نشیند بجای خویشتن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان زفیض قناعت بعیش مشغولم</p></div>
<div class="m2"><p>که نفس کام طلب در غذای خویشتن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار معجزه بنمود عشق و عقل جهول</p></div>
<div class="m2"><p>هنوز امت اندیشهای خویشتن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عدیل فطرت عرفست همت ساقی</p></div>
<div class="m2"><p>که حاتم دگران و گدای خویشتن است</p></div></div>