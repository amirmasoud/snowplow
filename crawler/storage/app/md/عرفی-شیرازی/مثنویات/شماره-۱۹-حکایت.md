---
title: >-
    شمارهٔ ۱۹ - حکایت
---
# شمارهٔ ۱۹ - حکایت

<div class="b" id="bn1"><div class="m1"><p>انجمن آرای درون با یزید</p></div>
<div class="m2"><p>محفلی آراست بجمعی مرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محفلی آرایش صحن فلک</p></div>
<div class="m2"><p>فرش حریمش ز جناح ملک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور فشاننده تر از جام جم</p></div>
<div class="m2"><p>کرده شبستانی و شمعی بهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دود چراغش چکند در دماغ</p></div>
<div class="m2"><p>انجمنی کو بودش شبچراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهره برافروخته از شرم عشق</p></div>
<div class="m2"><p>مست سماع از نفس گرم عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرده زمستی بلبش هرزه جوش</p></div>
<div class="m2"><p>هرزه نگویم که نیم اهل هوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راز دورن پرده گشائی گرفت</p></div>
<div class="m2"><p>نور نفس اوج گرائی گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت که میگویم و نبود گناه</p></div>
<div class="m2"><p>نیست در این جامه بغیر ازاله</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جلوه گر از جامه هستی منم</p></div>
<div class="m2"><p>معنی هشیاری و مستی منم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در حرم و دیر منم جلوه گر</p></div>
<div class="m2"><p>کافر و دیندار مرا سجده بر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رشته هردام زمن پیچ پیچ</p></div>
<div class="m2"><p>هر چه بجز هستی من هیچ ، هیچ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون دلش از رشته توحید رست</p></div>
<div class="m2"><p>رشته آمیزش وحدت گسست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جملگی آن میوه که افشانده بود</p></div>
<div class="m2"><p>باز فشاندند بر آن باغ جود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از اثر لذت آن لب مکید</p></div>
<div class="m2"><p>نی غلطم لب زندامت گزید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گفت که این دعوی قدوسیست</p></div>
<div class="m2"><p>و زلب ما نغمه ناقوسیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گرد گر این نغمه سراید لبم</p></div>
<div class="m2"><p>یا بچنین هرزه گر آید لبم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تیغ بر آرید و هلاکم کنید</p></div>
<div class="m2"><p>کنج نهانخانه بخاکم کنید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون می توحید دگر نوش کرد</p></div>
<div class="m2"><p>می زد و اندیشه فراموش کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرزه دوشینه درآمد بجوش</p></div>
<div class="m2"><p>لیک برآن هرزه فدا عقل و هوش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مستمعان تیغ برافراشتند</p></div>
<div class="m2"><p>تخم عدم خیزی خود کاشتند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر که بعضویش سبک تیغ راند</p></div>
<div class="m2"><p>تافته زو تیغ و بخونش نشاند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بود یکی زان همه آهسته تر</p></div>
<div class="m2"><p>دست و زبانی زکمر بسته تر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بست به بردست و زبان کرد باز</p></div>
<div class="m2"><p>تا چه برون آید از آن گنج راز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دید که هوش دل و دستیش سوخت</p></div>
<div class="m2"><p>زمزمه دعوی هستیش سوخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دیده بیاراست بدیدار بزم</p></div>
<div class="m2"><p>لاله فشان دید سمن زار بزم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفت چه باد ازره این روضه خاست</p></div>
<div class="m2"><p>کزورق گل چمن کربلاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صورت آنحال برنگی که بود</p></div>
<div class="m2"><p>خواند برآن بلبل معنی سرود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گفت چه با شعله ستیزد مگس</p></div>
<div class="m2"><p>سوختن وی نبود جرم کس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر که بمعشوق کشد تیغ کین</p></div>
<div class="m2"><p>مرگ برون آردش از آستین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آن نه منم کز لبش این نغمه زاد</p></div>
<div class="m2"><p>اوست که آن نغمه تواند گشاد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این منم از هر نفسی بسته لب</p></div>
<div class="m2"><p>بر نفس لب زده مهر ادب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عرفی ازین زمزمه لب را مسوز</p></div>
<div class="m2"><p>هان نتراود نفست لب مدوز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>راز فرو خور که دلت ریش باد</p></div>
<div class="m2"><p>حوصله معرفتت بیش باد</p></div></div>