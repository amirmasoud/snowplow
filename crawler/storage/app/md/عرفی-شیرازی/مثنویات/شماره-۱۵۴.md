---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>پیش بردم در قمار عشق جانان باختن</p></div>
<div class="m2"><p>صد شکافم بر دلست و یک گریبان باختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوی میدان وفا را زخم چوگان بشکند</p></div>
<div class="m2"><p>گر درین میدان سپهر آید بچوگان باختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردن جان دیده عشق چیده بازی هوشدار</p></div>
<div class="m2"><p>با حریف پیش بین مستانه نتوان باختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیدل و دینم وگرنه من کجا سهو از کجا</p></div>
<div class="m2"><p>از تهی دستی دلیرم در پریشان باختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشاه صد ساله ام از این درشتی کم شود</p></div>
<div class="m2"><p>کی بیک تلخی توان صد شکرستان باختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست عرفی از گریبان کس جدا هرگز ندید</p></div>
<div class="m2"><p>خواهد آخر دست در چاک گریبان باختن</p></div></div>