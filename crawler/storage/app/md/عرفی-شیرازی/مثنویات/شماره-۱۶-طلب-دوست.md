---
title: >-
    شمارهٔ ۱۶ - طلب دوست
---
# شمارهٔ ۱۶ - طلب دوست

<div class="b" id="bn1"><div class="m1"><p>ای هوس آرای محبت شکن</p></div>
<div class="m2"><p>عافیت انگیز ملامت فکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عید صفت صورت شادی نگار</p></div>
<div class="m2"><p>برگ طرب ساز چو طبع بهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منع اثر کرده شمشیر غم</p></div>
<div class="m2"><p>تشنه آسودگی وسیر غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله گشاید نفس زمهریر</p></div>
<div class="m2"><p>گریه کند طفل هوس مست شیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهر عدم کرده بجام حیا</p></div>
<div class="m2"><p>تا بکی این دایگی از مدعا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دلت از زخم اگر بسمل است</p></div>
<div class="m2"><p>چشمه حیوان همه خون دل است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دهن تیغ درا چون گهر</p></div>
<div class="m2"><p>و زجگر درد برا چون اثر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نور دل از پرتو سوز دل است</p></div>
<div class="m2"><p>دل که درو سوزنه مشت گل است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اخگر سوزان بصفا گوهر است</p></div>
<div class="m2"><p>سرد شود توده خاکستر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرگ بود نشاه حرمان عشق</p></div>
<div class="m2"><p>روح بود گوهری ازکان عشق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطره خون چیست دل رنج دوست</p></div>
<div class="m2"><p>دل که بود مغز گدازنده پوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی گهر آن دل که نه در محنتست</p></div>
<div class="m2"><p>بی گهری اصل جمادیت است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برگ عمارت برویرا نیست</p></div>
<div class="m2"><p>جمعیت از فرع پریشانیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چشم بتان گر نبود مست رنج</p></div>
<div class="m2"><p>گوهر دلها نبرد گنج گنج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سنبلشان گرنه پریشان بود</p></div>
<div class="m2"><p>کی گهر افزوز دل و جان بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مفلس راحت که نه رنجور درد</p></div>
<div class="m2"><p>گنج خرابی که نه معمور درد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای مگس شهد طرب جوش چند</p></div>
<div class="m2"><p>سیر شو آخر هوس نوش چند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در چمنت فصل جوانی گذشت</p></div>
<div class="m2"><p>عنبرت اندوده کافور گشت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاهد دل در حرم سینه مرد</p></div>
<div class="m2"><p>جوهر فیروزه بگنجینه برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ظلمت دل مایه فشان بر ضمیر</p></div>
<div class="m2"><p>وز نفست موج زنان ز مهریر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روح تو آسوده زتأثیر غم</p></div>
<div class="m2"><p>طبع تو بی بهره زتدبیر غم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی غمیت مایه رو زردیست</p></div>
<div class="m2"><p>ریش سفیدیت زدم سردیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>من که زآغاز وجودم هنوز</p></div>
<div class="m2"><p>نیمه گشا نامه بودم هنوز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بل صدفی بی در ناسفته ام</p></div>
<div class="m2"><p>صورت معنی نپذیرفته ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بسکه درین غمکده لاجورد</p></div>
<div class="m2"><p>ناله فشانیم زدل مست درد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از دل شب تا بلب صبحدم</p></div>
<div class="m2"><p>ناله فرو ریخته بر روی هم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در ازل این مزرع غم کشته اند</p></div>
<div class="m2"><p>حله حورم ز ازل رشته اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عشوه نما شاهد هستی لقب</p></div>
<div class="m2"><p>بود زبوس عدم آلوده لب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلکه عدم نیز جنین در نقاب</p></div>
<div class="m2"><p>بر اثر جوهر خود در شتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مایه لذت ز بقا میگرفت</p></div>
<div class="m2"><p>مرغ ملامت زهوا میگرفت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مرغ الم نغمه بر او میسرود</p></div>
<div class="m2"><p>شاهد غم بوسه از او میربود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمزمه سور بلب میشکست</p></div>
<div class="m2"><p>نیش ملامت بادب میشکست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>طره آشوب طرازنده بود</p></div>
<div class="m2"><p>برقع تشویش برافکنده بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ما الم افشان و ملامت شعار</p></div>
<div class="m2"><p>فتنه در آغوش و بلا در کنار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در تو هم این نشاه هویدا بود</p></div>
<div class="m2"><p>هستیت آغشته سودا بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لاجرم از هر چه به دست آوری</p></div>
<div class="m2"><p>می‌کندت بر دگری رهبری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرنه غبار در لیلی شوی</p></div>
<div class="m2"><p>وای بحالت که تسلی شوی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کفر بود گر طلبی غیر دوست</p></div>
<div class="m2"><p>مغز بدست آر و بینداز پوست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سبحه و زنار ز هم واشناس</p></div>
<div class="m2"><p>دیده عرفان بگشا در لباس</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جز طلب دوست مرو پیچ پیچ</p></div>
<div class="m2"><p>دوست طلب دوست دگر هیچ هیچ</p></div></div>