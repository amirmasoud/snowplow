---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>امشبم کشت غمت عشرت فردای تو خوش</p></div>
<div class="m2"><p>کار خود کرد بمن غم دل غمهای تو خوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چنین غمزه کند کاوش دل ممکن نیست</p></div>
<div class="m2"><p>که شود خاطرم از شغل تماشای تو خوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرصتم نیست که در پای تو جان افشانم</p></div>
<div class="m2"><p>بس که می آیدم از دیدن بالای تو خوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم از زلف شکن در شکن و چین در چین</p></div>
<div class="m2"><p>همه جا خاص تو ای دل بنشین جای تو خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مصر گلشن زتو ای یوسف کنعان خوشبوست</p></div>
<div class="m2"><p>شب یعقوب تو خوش روز زلیخای تو خوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سحر و معجز صفت چند عطا کرده تست</p></div>
<div class="m2"><p>هم دل سامری و هم دل زیبای تو خوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل عرفی خبر از ناخوشیش نیست که نیست</p></div>
<div class="m2"><p>پایدار تو خوش و پای تمنای تو خوش</p></div></div>