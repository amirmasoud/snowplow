---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>دلی داریم و با جمعی پریشان از غم اوئیم</p></div>
<div class="m2"><p>که میمیرد برای درد و ما در ماتم اوئیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باین آمیزش و این محرمی گر تو بدید آری</p></div>
<div class="m2"><p>مگن بیگانگی غم که ما هم محرم اوئیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر با مرد غم باشیم تاب آریم این غم را</p></div>
<div class="m2"><p>که ناشایسته چند آرزومند غم اوئیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجو فرزانه عرفی که گوید حالت عشقت</p></div>
<div class="m2"><p>که ما دیوانگان هرزه گرد عالم اوئیم</p></div></div>