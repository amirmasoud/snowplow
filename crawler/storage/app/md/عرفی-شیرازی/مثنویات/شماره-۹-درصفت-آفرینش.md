---
title: >-
    شمارهٔ ۹ - درصفت آفرینش
---
# شمارهٔ ۹ - درصفت آفرینش

<div class="b" id="bn1"><div class="m1"><p>بلبل طبعم زند این نغمه باز</p></div>
<div class="m2"><p>کامدم اینک بچمن نغمه ساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چمن نعت گلی دیده ام</p></div>
<div class="m2"><p>زمزمه تازه بر او چیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میشمرم نغمه مستانه را</p></div>
<div class="m2"><p>رنگ نوی می دهم افسانه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده ز اسرار درون می کشم</p></div>
<div class="m2"><p>ظل شه از پرده برون می کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کنم این دعوی عالی اساس</p></div>
<div class="m2"><p>تا بکی این نغمه زنم در لباس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله برآنند که بی سایه است</p></div>
<div class="m2"><p>وین سخن از صدق تهی مایه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه ورش چون نگرد بی بصر</p></div>
<div class="m2"><p>سایه او دیده ولی دیده ور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سایه صورت طلب از آب وگل</p></div>
<div class="m2"><p>سایه معنی نفتد جز بدل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه او صیقلی آفتاب</p></div>
<div class="m2"><p>نور درین سایه بسوزد نقاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور وی آرایش بود همه</p></div>
<div class="m2"><p>سایه او اصل وجود همه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایه او بود کزان بحرزاد</p></div>
<div class="m2"><p>وز نفسش چشمه طوفان گشاد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لوح وجود از رقم فتنه شست</p></div>
<div class="m2"><p>جنبش حرف از قلم فتنه شست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سایه او بود که در باغ ناز</p></div>
<div class="m2"><p>بود تماشائی گلهای راز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آتش نمرود بر باغ او</p></div>
<div class="m2"><p>لانه فروش چمنش داغ او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سایه او بود که زد کوس حسن</p></div>
<div class="m2"><p>جامه علم کرد بفانوس حسن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دشنه غم بر دل یعقوب راند</p></div>
<div class="m2"><p>زهر ملامت بزلیخا چشاند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سایه او بود که نور سراغ</p></div>
<div class="m2"><p>داشت براه ظلماتش چراغ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آب لبش چشمه حیوان مکید</p></div>
<div class="m2"><p>عمر ابد رخت بکویش کشید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دولت ما بین که صدفهای ما</p></div>
<div class="m2"><p>با گهران ذات نمود آشنا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سایه او بود که او رنگ داد</p></div>
<div class="m2"><p>برزبر باد صبا پر گشاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمزمه معدلت آغاز کرد</p></div>
<div class="m2"><p>صعوه و شهباز هم آواز کرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سایه او بود که در باغ جود</p></div>
<div class="m2"><p>روح امینش گل فطرت گشود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>باد بهشت از نفسش می وزید</p></div>
<div class="m2"><p>چشمه حیوان زلبش می چکید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای گهرت مبدأ  آثار دوست</p></div>
<div class="m2"><p>سایه تو مطلع انوار دوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سایه ذات تو مقدم بذات</p></div>
<div class="m2"><p>وین صفتت فاتحه معجزات</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جوهر آئینه شاهی توئی</p></div>
<div class="m2"><p>معجزه صنع الهی توئی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پایه ایوان تو معراج طور</p></div>
<div class="m2"><p>سایه تو گوهر دریای نور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آدم و آن جمع که پیغمبرند</p></div>
<div class="m2"><p>شهر تو را جمله عمارت گرند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر یکی افزایدش آرایشی</p></div>
<div class="m2"><p>روبد از او هر غش وآلایشی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا ز عمارت شود این ده تمام</p></div>
<div class="m2"><p>جلوه کنی دروی ، نبود حرام</p></div></div>