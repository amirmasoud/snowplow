---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>باز آی تا بذوق الم آشنا شویم</p></div>
<div class="m2"><p>با شیشه وزسنگ بهم آشنا شویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد بحث غم بیکدرم داغ میخرند</p></div>
<div class="m2"><p>زین ننگ با معامله کم آشنا شویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راز محبتیم زما گوش و لب تهیست</p></div>
<div class="m2"><p>حاشا که ما بلوح و قلم آشنا شویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باید کشید خون شهیدان سبوسبو</p></div>
<div class="m2"><p>تا اندکی بذوق عدم آشنا شویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی براه کعبه کنند آشنا قدم</p></div>
<div class="m2"><p>اول رهی که با قدم آشنا شویم</p></div></div>