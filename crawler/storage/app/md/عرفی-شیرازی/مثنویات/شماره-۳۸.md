---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>بخت جم وکاووس عنانش بکف تست</p></div>
<div class="m2"><p>پیش آمدن از بخت کشش از طرف تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصفی نبود کان شرف ذات تو گردد</p></div>
<div class="m2"><p>جز بندگی شاه جهان کان شرف تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ساز و نوا باش ببین تا چه سرودم</p></div>
<div class="m2"><p>ای آنکه سنان پای زن و نعره دف تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بشکسته عدو نامه فتح تو نوشتم</p></div>
<div class="m2"><p>دولت خبرم داد که فتح از طرف تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بشکنی آخر صف اعدا که بعالم</p></div>
<div class="m2"><p>هرجا که دعائست ماثر صدف تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خواب شب آلوده بخون دید خدنگت</p></div>
<div class="m2"><p>تعبیر جز این نیست که عالم هدف تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این قول نه کذب است کجا دیده شناسد</p></div>
<div class="m2"><p>آن بنده که پرورده آب و علف تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی چه همیگفت که آن مقبل ناچیز</p></div>
<div class="m2"><p>دانسته که راهش بدل پر شعف تست</p></div></div>