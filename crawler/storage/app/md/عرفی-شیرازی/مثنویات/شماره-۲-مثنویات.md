---
title: >-
    شمارهٔ ۲ - مثنویات
---
# شمارهٔ ۲ - مثنویات

<div class="b" id="bn1"><div class="m1"><p>بسم اله الرحمن الرحیم</p></div>
<div class="m2"><p>موج نخست است ز بحر قدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا برم این نکته بتکمیل عرش</p></div>
<div class="m2"><p>زان کنم آرایش قندیل عرش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به که بنام صمد بی نیاز</p></div>
<div class="m2"><p>نامه نوازیم و عنان طراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اثر او صمدیت رفیع</p></div>
<div class="m2"><p>بر گهر اواحدیت وسیع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ رز جامه ارباب شید</p></div>
<div class="m2"><p>دام نه عابد دل مرده صید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غازه فروش سر بازار شرم</p></div>
<div class="m2"><p>آبله ریز ته دلهای گرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخم چکان مژه دلبران</p></div>
<div class="m2"><p>حسن فزاینده عصمت وران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شهر گشاینده بستان صبح</p></div>
<div class="m2"><p>یاسمن افشان گریبان صبح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمزمه کاو لب ناقوس دل</p></div>
<div class="m2"><p>داغ فروز دم طاووس دل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زیور آوازه ناقوسیان</p></div>
<div class="m2"><p>چشمه آرایش طاووسیان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آستی افشان نسیم صبا</p></div>
<div class="m2"><p>آشتی انگیز اثر بادعا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جوهر آئینه حوری وشان</p></div>
<div class="m2"><p>جرعه پیمانه معنی کشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>انجمن آرای حریم سماع</p></div>
<div class="m2"><p>نوحه طراز لب گرم وداع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر نفس گرم ترحم فشان</p></div>
<div class="m2"><p>وز اثر گریه تبسم چکان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بار گشای فلک اندر صعود</p></div>
<div class="m2"><p>ناصیه سای ملک اندر سجود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سرمه کش عبهر زرین قدح</p></div>
<div class="m2"><p>وسمه نه ابروی قوس وقزح</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>راه نماینده آیندگان</p></div>
<div class="m2"><p>مایه هستی ده پایندگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لوح عمل ساز ورع پیشگان</p></div>
<div class="m2"><p>نامه بر انداز جزع پیشگان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمع فروز حرم احترام</p></div>
<div class="m2"><p>نامیه سوز چمن انتقام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر شفق گریه عطارد شمار</p></div>
<div class="m2"><p>بر ورق دیده عطارد نگار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تاب ده رشته کوتاه عمر</p></div>
<div class="m2"><p>تا بعدم رفته خس از راه عمر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صور دمی داده بباد بهار</p></div>
<div class="m2"><p>نعشکشی کرده خزان را شعار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرغ شکیبائی از او سینه تنگ</p></div>
<div class="m2"><p>چهره بیماری از او خیره رنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گوهر دل شسته بدریای جان</p></div>
<div class="m2"><p>نور اثر داده باو دودمان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کرده مصاحب بر زاغ صفات</p></div>
<div class="m2"><p>بوقلمون مزرعه کاینات</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بوسه نگیرد ز دماغ سخن</p></div>
<div class="m2"><p>کش سخن او ندمد در دهن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جل جلاله علم شان اوست</p></div>
<div class="m2"><p>عم نواله مگس خوان اوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برده دل از حسن چه یغماست این</p></div>
<div class="m2"><p>گوهر خود زاده چه دریاست این</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خاک نشین در او بندگی</p></div>
<div class="m2"><p>مرده بیماری او زندگی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بندگی از داغ قبولش فکار</p></div>
<div class="m2"><p>گردن آزادی از او طوق دار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بسکه بود تشنه عفو و عطا</p></div>
<div class="m2"><p>دست نیارد بره سهو ما</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دیر و حرم دوش بدوش آورد</p></div>
<div class="m2"><p>سبحه و زنار بجوش آورد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نغمه ناقوس خروشان از اوست</p></div>
<div class="m2"><p>سینه هر زمزمه جوشان از اوست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>لغزش مستانه دهد سهو را</p></div>
<div class="m2"><p>چشمه افسوس دهد لهو را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ناطقه را راز فروشی دهد</p></div>
<div class="m2"><p>قفل کری را بخموشی دهد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سامعه را نغمه بدست آورد</p></div>
<div class="m2"><p>باصره فانوس پرست آورد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تلخ کند میوه ناموس را</p></div>
<div class="m2"><p>دست گزان آورد افسوس را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا نزد این حله ایوان رقم</p></div>
<div class="m2"><p>بود برهنه عدم اندر عدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زندگی ازوی عدم مرده را</p></div>
<div class="m2"><p>نازکی از وی دل پژمرده را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>عشوه شیرین بکمان آورد</p></div>
<div class="m2"><p>وز دل فرهاد نشان آورد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غمزه که شمشیر بدست از وی است</p></div>
<div class="m2"><p>بر اثر نرگس مست از وی است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دایگی حسن دهد ناز را</p></div>
<div class="m2"><p>نغمگی آرا کند آواز را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عقل بجاسوسی راز آورد</p></div>
<div class="m2"><p>جهل زدانش بگداز آورد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روشنی سینه علم ازوی است</p></div>
<div class="m2"><p>مایه آرایش حلم ازوی است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نامیه عقل بتعلیم داد</p></div>
<div class="m2"><p>مرهم ناسور به تسلیم داد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چون در جودش باثر باز شد</p></div>
<div class="m2"><p>جنبش نبض عدم آغاز شد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>طوبی حکمت ثمر انداز کرد</p></div>
<div class="m2"><p>دست مآثر زحیا باز کرد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مصحف معنی بگشود از جمال</p></div>
<div class="m2"><p>سوره و الشمس بر آمد بفال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بانگ عروسان چمن زاد کرد</p></div>
<div class="m2"><p>شهر عدم را صنم آباد کرد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زیور صورت بکف خاک بست</p></div>
<div class="m2"><p>آهوی معینش بفتراک بست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کوشش اندیشه بافلاک داد</p></div>
<div class="m2"><p>ذوق تحمل بکف خاک داد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ناز بدرگاه جوانی نشاند</p></div>
<div class="m2"><p>عجز بدر یوزه ثانی نشاند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رنگرز عذر نمود انفعال</p></div>
<div class="m2"><p>بر قد اندازه برید اعتدال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ناصیه را لوح ادب نام کرد</p></div>
<div class="m2"><p>بوس زمین خودش انعام کرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نور عمل داد بشمع صفا</p></div>
<div class="m2"><p>دود دل افشاند بروی دعا</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>داد بآوازه شراب نوید</p></div>
<div class="m2"><p>بست ز خمیازه دهان امید</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>هاضمه را نامزد علم کرد</p></div>
<div class="m2"><p>حافظه را صافگه حلم کرد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>غرفه معنی ز تکلم گشاد</p></div>
<div class="m2"><p>چشمه کوثر ز تبسم گشاد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دانه غم در دل افکار کشت</p></div>
<div class="m2"><p>تخم کرشمه به سمن زار کشت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خنده بلب داد که بردار نوش</p></div>
<div class="m2"><p>گریه بدل ریخت که بر چین خروش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خون چمن بر ورق گل فشاند</p></div>
<div class="m2"><p>آب گل از نغمه بلبل چکاند</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>زمزمه غم بدل تنگ داد</p></div>
<div class="m2"><p>چاشنی نغمه بآهنگ داد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>حسن به آرایش سودا نشاند</p></div>
<div class="m2"><p>عشق بمعماری دلها نشاند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>خلوتی آراست برون از حساب</p></div>
<div class="m2"><p>سایه حسنی به نماز آفتاب</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پنجه فرهاد به هل زیر سنگ</p></div>
<div class="m2"><p>کو ز گهر میطلبد آب و رنگ</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چشمه شوق از دل مجنون گشود</p></div>
<div class="m2"><p>سینه او هودج لیلی نمود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دامن یوسف بمیان زد که خیز</p></div>
<div class="m2"><p>آنچه گرفتی بزلیخا بریز</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بینش یعقوب ز حرمان بشوی</p></div>
<div class="m2"><p>کو دلش از ما بتو آورد روی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نور وی آرایش هر محفلی</p></div>
<div class="m2"><p>می نشکیبد که نکاود دلی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>غیرت حسنش چو بجوش آورد</p></div>
<div class="m2"><p>دست تماشائی یوسف برد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تیشه زند بر دل فرهاد مست</p></div>
<div class="m2"><p>کز الم غیر پذیرد شکست</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هر که الم دوست باو بگرود</p></div>
<div class="m2"><p>وآنکه بر او زالم بگذرد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>عقل بهم برزده کاین جاهل است</p></div>
<div class="m2"><p>چشمه خون کرده عطا کاین دلست</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سینه بغم داده که این گنج تو است</p></div>
<div class="m2"><p>عشق بدل داده که این رنج تو است</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چشمه جوداست چو مولی است این</p></div>
<div class="m2"><p>عین وجود است چو مولی است این</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>زین متفرق شده مشت غبار</p></div>
<div class="m2"><p>ذره وشی کو که نماید شمار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>گر چه در این باغچه چند و چون</p></div>
<div class="m2"><p>خار و گل از یک شجر آید برون</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بهر چه در مشعله گاه شهود</p></div>
<div class="m2"><p>نور بیک جامه درونست و دود</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مه ز چه آغشته زنقص وکمال</p></div>
<div class="m2"><p>گه ز چه بدر آید و گاهی هلال</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از ته دل جرعه دیدار نوش</p></div>
<div class="m2"><p>گاه شود مست و گه آید بهوش</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گه رودش بر اثر سبحه دست</p></div>
<div class="m2"><p>گه کندش نغمه ناقوس مست</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بهر چه هر دل که برانگیخته</p></div>
<div class="m2"><p>از غم و شادی بهم آمیخته</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>کرده زیک چشمه طراوت گزین</p></div>
<div class="m2"><p>باد مسیح و نفس واپسین</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>گاه لب از نوحه کند خون چکان</p></div>
<div class="m2"><p>گه زترنم گل شادی فشان</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>گاه شود جلوه گر از طور ناز</p></div>
<div class="m2"><p>بی دلی انگیزد و عجز و نیاز</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>گر دهد از مستی و حسرت سرور</p></div>
<div class="m2"><p>شادی آموزد و ناز و غرور</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>حکمت از این رنگرزیهای نغز</p></div>
<div class="m2"><p>کاید از او بوی بهشتم بمغز</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>شاهد حالی است که آن رنگ وبوی</p></div>
<div class="m2"><p>در چمن ماست نه در باغ اوی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>باغ وی آلوده نیرنگ نی</p></div>
<div class="m2"><p>در چمنش آب نه ورنگ نی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>باغ وصالش که تمنا کند</p></div>
<div class="m2"><p>دیده که دارد که تماشا کند</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>از روش این راه نشانی ندید</p></div>
<div class="m2"><p>سایه دستی و عنانی ندید</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>وهم درآمد که نشیند برین</p></div>
<div class="m2"><p>تیره شدش دیده نابود بین</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>سرمه کش دیده ما اعمی است</p></div>
<div class="m2"><p>دیده همان در طلب سلمی است</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>عقل که در وادی حیرت شتافت</p></div>
<div class="m2"><p>رو بحرم داشت ولی دیر یافت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>رهبر ما راه صوابش یکیست</p></div>
<div class="m2"><p>چهره نگویم که نقابش یکیست</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>پای طلب سوده در اول قدم</p></div>
<div class="m2"><p>وه که نزد برتر از این کس قدم</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>معرفتش زینت بیرون در</p></div>
<div class="m2"><p>نقش و نگاری است ز خون جگر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>طفل محبت که حرم زاد اوست</p></div>
<div class="m2"><p>هم بدرون نغمه دیدار اوست</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>حسن که وی را بود آئینه دار</p></div>
<div class="m2"><p>دیده و دل صورت آینه دار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>حوصله وصل دلارام نیست</p></div>
<div class="m2"><p>باده باندازه نه و جام نیست</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ما که و اندازه دیدار دوست</p></div>
<div class="m2"><p>حسن تماشا و تماشای دوست</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>کو دل اندازه نعمت شناس</p></div>
<div class="m2"><p>تا طلبم نعمت و دارم سپاس</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>شمع طلب بر نفروزیم به</p></div>
<div class="m2"><p>در تب امید بسوزیم به</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>دست بدامان طلب چون زنم</p></div>
<div class="m2"><p>ور بزنم لاف ادب چون زنم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>ور بمیان آوردم رو سفید</p></div>
<div class="m2"><p>بر در فردوس نویسم امید</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>ور کند از راه عتابم دلیل</p></div>
<div class="m2"><p>شعله نپوشم نچشم سلسبیل</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>عرفی اگر بلبل اگر زاغ اوست</p></div>
<div class="m2"><p>نغمه توحید زن باغ اوست</p></div></div>