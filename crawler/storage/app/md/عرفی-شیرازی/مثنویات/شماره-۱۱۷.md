---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>بسهوار توبه  از می کردم و دیر مغان بستم</p></div>
<div class="m2"><p>کسی کو بازم آرد برسر خم از جهان رستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقتراکم به بندد عشق وگوید دست و پاکم زن</p></div>
<div class="m2"><p>که من بسیار از این صید زبون در خاک و خون کشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ردای عافیت بس خام بافست آتشی در زن</p></div>
<div class="m2"><p>که من زین پنبه عمری رشته و زنار می رشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراسر کامم و در چشمه لذت فرو رفتم</p></div>
<div class="m2"><p>سراپا ریشم و در پنبه الماس آغشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه طوبی داشت سرسبزی نه کوثر داشت نمناکی</p></div>
<div class="m2"><p>که من در شعله زار سینه تخم ناله می کشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تماشای جمال حور وغلمانم کجا باشد</p></div>
<div class="m2"><p>مرا آئینه ای باید که بینم تا چه حد زشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگوشم کاتب اعمال گوید عرفی انضافی</p></div>
<div class="m2"><p>که ننوشتم ثوابی ورگنه صد لوح دل شستم</p></div></div>