---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>بردیم ز کویش دم سردی وگذشتیم</p></div>
<div class="m2"><p>سودی بران در رخ زردی وگذشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران بستادند که این جلوه گه کیست</p></div>
<div class="m2"><p>ما سرمه گرفتیم ز گردی وگذشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگه که ره ما بیکی راه رو افتاد</p></div>
<div class="m2"><p>دیدیم چو خود بیهده گردی و گذشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون باد صبا روی بهر سو که نهادیم</p></div>
<div class="m2"><p>چیدیم غبار ره مردی و گذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن درد که پای دل ما داشت بزنجیر</p></div>
<div class="m2"><p>گفتیم بدیوانه فردی وگذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گه که گذار من و عرفی بهم افتاد</p></div>
<div class="m2"><p>دادیم بهم تحفه دردی وگذشتیم</p></div></div>