---
title: >-
    شمارهٔ ۲۴ - داستان رابعه
---
# شمارهٔ ۲۴ - داستان رابعه

<div class="b" id="bn1"><div class="m1"><p>رابعه آن مریم معنی مسیح</p></div>
<div class="m2"><p>آن چو لب دلبر کنعان ملیح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرسر مویش زغم عشق مست</p></div>
<div class="m2"><p>شرع زکیفیت او می پرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی او بر سر ناموس تاج</p></div>
<div class="m2"><p>میکده عصمت ازو با رواج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون در اندیشه بمستی گشاد</p></div>
<div class="m2"><p>دیده بمعموره هوشش فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیشتری در دل ریشش خلید</p></div>
<div class="m2"><p>خون دل از دیده برویش دوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله زشب تحفه گردون گرفت</p></div>
<div class="m2"><p>گریه ز دل برگ شبیخون گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناله اش آتش بدل اوج زد</p></div>
<div class="m2"><p>گریه بدریای دلش موج زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گریه گرمی بصفای ملک</p></div>
<div class="m2"><p>خنده لعلیش گدای نمک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همنفسی کرد ز وی جستجوی</p></div>
<div class="m2"><p>کین همه زاری زچه داری بگوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا منم این زمزمه سینه سوز</p></div>
<div class="m2"><p>وین گهر افشانی گنجینه سوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یاد ندارم زتو حال تو چیست</p></div>
<div class="m2"><p>موجب طوفان ملال تو چیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون لب سایل گهر نغمه سفت</p></div>
<div class="m2"><p>لعل برافشاند ز مژگان وگفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حوصله ام تنگ و ملولم بسی</p></div>
<div class="m2"><p>منفعل از روی رسولم بسی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ایکه نفهمیده دلم یاد اوست</p></div>
<div class="m2"><p>نام دلم بنده آزاد اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از غم او یارب معمور باد</p></div>
<div class="m2"><p>ورنه نهد مستی معذور باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عرفی ازین می قدحی نوش کن</p></div>
<div class="m2"><p>جز الم دوست فراموش کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ریش فزون کن غم بی سود چند</p></div>
<div class="m2"><p>کم ز زنی خود نتوان بود چند</p></div></div>