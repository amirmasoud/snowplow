---
title: >-
    شمارهٔ ۱۳ - داستان
---
# شمارهٔ ۱۳ - داستان

<div class="b" id="bn1"><div class="m1"><p>جوی طراز چمن بی ستون</p></div>
<div class="m2"><p>آن به بهشت غم شیرین درون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود بامر صنم دلپذیر</p></div>
<div class="m2"><p>در پی آراستن جوی شیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تیشه هر داغ که بر سنگ خورد</p></div>
<div class="m2"><p>لذت آن بر دل آن سنگ برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیشه هر آن نغمه که بر می کشید</p></div>
<div class="m2"><p>از لب وی ناله فرو می چکید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ریزه سنگیش که از تیشه جست</p></div>
<div class="m2"><p>نیشتر آن بدلش در نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ شرر چون طیران می نمود</p></div>
<div class="m2"><p>گرم بشهباز دلش می ربود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جنبشی از تیشه نرفتی بکار</p></div>
<div class="m2"><p>کز دل وی در نزد دی قرار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرزه در آئی زملامت گریز</p></div>
<div class="m2"><p>تیغ زبان کرده به بیهوده تیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفت درین شیوه مراد تو چیست</p></div>
<div class="m2"><p>کام دل رنج نهاد تو چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>می بری ای رنج بفرموده</p></div>
<div class="m2"><p>یا زجنون طالب بیهوده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمزمه برداشت که ای دلخراش</p></div>
<div class="m2"><p>مرهم داغم بطبر زد تراش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می برم این رنج بامر کسی</p></div>
<div class="m2"><p>کز طلبس رنج شمارم بسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منعم از این شیوه مکن کان نگار</p></div>
<div class="m2"><p>داده قراری بمن بیقرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رنج مرا مزد وفا می دهد</p></div>
<div class="m2"><p>گنج وصالش به بها می دهد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مزد از این رنج بیابم حلال</p></div>
<div class="m2"><p>زان بکنم بیع متاع وصال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گفت که ای ساده دل تیشه سنج</p></div>
<div class="m2"><p>وز طلب گنج در آشوب و رنج</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کس بصدف ریزه نجوید گهر</p></div>
<div class="m2"><p>کس گهر عمر نیابد بزر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چشمه حیوان بسرابی که داد</p></div>
<div class="m2"><p>شربت کوثر بحبابی که داد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت زفیض طلبت شرم باد</p></div>
<div class="m2"><p>وز من و رنج منت آزرم باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر همه دانم که نیاید بدست</p></div>
<div class="m2"><p>از طلب گنج نشاید نشست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پیروی حسن ادب کرده ام</p></div>
<div class="m2"><p>گنج نیابم زطلب کرده ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نام طلب نقش نگینم بس است</p></div>
<div class="m2"><p>گر نبرم گنج همینم بس است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زین طرف این زمزمه طعنه خیز</p></div>
<div class="m2"><p>بوم و هما برلب هم نغمه ریز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زان طرف آن طعنه زن آفتاب</p></div>
<div class="m2"><p>بر اثر جذب طلب در شتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پنجه تأثیر طلب برعنان</p></div>
<div class="m2"><p>بر لب جور انده تماشا کنان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آمد و آوازه آن رنج دید</p></div>
<div class="m2"><p>صاف عنایت زبیانش چکید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوهر تحسین بکنارش فشاند</p></div>
<div class="m2"><p>وز غم تسنیم غبارش فشاند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دست باشیاء وفا برگشاد</p></div>
<div class="m2"><p>آن گهر و گنج که بایست داد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>راهروی راه طلب برگزید</p></div>
<div class="m2"><p>بست گمانم که بجائی رسید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عرفی از این جاده عنان برمتاب</p></div>
<div class="m2"><p>خار ز پا بر مکش و می شتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رنج طلب به که در او گنج هست</p></div>
<div class="m2"><p>بس گهر و گنج درین رنج هست</p></div></div>