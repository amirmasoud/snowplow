---
title: >-
    شمارهٔ ۳۷ - محمل گل
---
# شمارهٔ ۳۷ - محمل گل

<div class="b" id="bn1"><div class="m1"><p>ای وفا پیشه یار هم مشرب</p></div>
<div class="m2"><p>که بعرفی دعا فرستادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه دعای تهی که در جیبش</p></div>
<div class="m2"><p>گوهر مدعا فرستادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عندلیب مونث گلریز</p></div>
<div class="m2"><p>از بهشت عطا فرستادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آنچه گویم بسوزد از من لب</p></div>
<div class="m2"><p>تا بگویم سزا فرستادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاس این شیوه دار تا گویم</p></div>
<div class="m2"><p>چه بدست صبا فرستادی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من گل تازه تحفه کردم و تو</p></div>
<div class="m2"><p>محمل گل مرا فرستادی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطف کردی ولی منه منت</p></div>
<div class="m2"><p>مه گرفتی سها فرستادی</p></div></div>