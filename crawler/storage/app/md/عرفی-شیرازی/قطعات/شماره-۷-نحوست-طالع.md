---
title: >-
    شمارهٔ ۷ - نحوست طالع
---
# شمارهٔ ۷ - نحوست طالع

<div class="b" id="bn1"><div class="m1"><p>عرفی بحیرت از فلک ظالمم کز او</p></div>
<div class="m2"><p>نجمی بهیچ دور عبورش باوج نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امید را عنان بکدامین طرف دهم</p></div>
<div class="m2"><p>کز خیل یأس بر اثرش فوج فوج نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعدی که از سعادت طالع بود مرا</p></div>
<div class="m2"><p>تحت الثری زاوج و سرابش ز موج نیست</p></div></div>