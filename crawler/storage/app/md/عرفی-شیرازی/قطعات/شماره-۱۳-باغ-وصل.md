---
title: >-
    شمارهٔ ۱۳ - باغ وصل
---
# شمارهٔ ۱۳ - باغ وصل

<div class="b" id="bn1"><div class="m1"><p>بیا ای بخت سرگردان و بنشین</p></div>
<div class="m2"><p>بزیر سایه سرو وگل و بید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که درباغی فروچیدیم محفل</p></div>
<div class="m2"><p>که در وی عندلیبی کرده ناهید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدامین باغ ، باغ وصل یاری</p></div>
<div class="m2"><p>که آبش میرود درجام جمشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی باغی که برگ لاله او</p></div>
<div class="m2"><p>زند سیلی بحسن ماه و خورشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازآن دم کاستین زد بردماغم</p></div>
<div class="m2"><p>نسیم این بهشت عیش جاوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و جان هردم از هم میربایند</p></div>
<div class="m2"><p>قبول منت و تاثیر امید</p></div></div>