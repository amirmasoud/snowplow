---
title: >-
    شمارهٔ ۱۰ - درتهنیت تولد فرزند ممدوح
---
# شمارهٔ ۱۰ - درتهنیت تولد فرزند ممدوح

<div class="b" id="bn1"><div class="m1"><p>صد شکر که فخر دوده جاه</p></div>
<div class="m2"><p>در دامن دایه بقا زاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریای توجه شهنشاه</p></div>
<div class="m2"><p>بنگر که چه دربی بها زاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این قطره شود هزار چشمه</p></div>
<div class="m2"><p>کز چشمه فیض کبریا زاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دانه شود هزار خوشه</p></div>
<div class="m2"><p>کز کشته سایه خدا زاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تربیت عنایت شاه</p></div>
<div class="m2"><p>خورشید شود اگر سهازاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دانم و آسمان که اقبال</p></div>
<div class="m2"><p>درکعبه آسمان کرازاد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکتا کهرمحیط اخلاق</p></div>
<div class="m2"><p>از بهر نثار پادشا زاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازجبهه طالعش چه جویی</p></div>
<div class="m2"><p>سر خیل قبیله وفا زاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او راچه دعا کنم که بختش</p></div>
<div class="m2"><p>دامان بقا گرفت تا زاد</p></div></div>