---
title: >-
    شمارهٔ ۱۲ - سعدی و عرفی
---
# شمارهٔ ۱۲ - سعدی و عرفی

<div class="b" id="bn1"><div class="m1"><p>دی کسی گفت که سعدی گهراندوز سخن</p></div>
<div class="m2"><p>قطعه‌ای گفته، که اندیشه بدان می‌نازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم این گوش بدان نغمه سزد گفت آری</p></div>
<div class="m2"><p>اینک از پرده عنان سوی تو می‌اندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن از عشق حرام است بر آن بیهده‌گو</p></div>
<div class="m2"><p>که چو ده بیت غرل گفت مدیح آغازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حبذا همت سعدی و غزل گفتن او</p></div>
<div class="m2"><p>که ز معشوق به ممدوح نمی‌پردازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم این خود همه عیب است که در راه هنر</p></div>
<div class="m2"><p>هرکه این لاف زند اسب دوی می‌تازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لوحش اله ز یک اندیشی عرفی کورا</p></div>
<div class="m2"><p>آنکه ممدوح بود، عشق بدو می‌بازد</p></div></div>