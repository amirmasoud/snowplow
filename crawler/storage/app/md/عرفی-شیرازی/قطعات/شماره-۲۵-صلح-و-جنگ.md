---
title: >-
    شمارهٔ ۲۵ - صلح و جنگ
---
# شمارهٔ ۲۵ - صلح و جنگ

<div class="b" id="bn1"><div class="m1"><p>عرفی نصیحت کنمت گوش دار ، گوش</p></div>
<div class="m2"><p>تاوارهی زکشمکش صلح و جنگ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عقل و روح گر، ید بیضایت آرزوست</p></div>
<div class="m2"><p>ناموس عشق جوی و مبین نام و ننگ خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن آفتاب پنجه کند اهل حسن را</p></div>
<div class="m2"><p>کز خویشتن نهفته حناز آب و رنگ خویش</p></div></div>