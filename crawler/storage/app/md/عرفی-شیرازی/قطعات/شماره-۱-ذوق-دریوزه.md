---
title: >-
    شمارهٔ ۱ - ذوق دریوزه!
---
# شمارهٔ ۱ - ذوق دریوزه!

<div class="b" id="bn1"><div class="m1"><p>نه از آن دیر بخشد ایزد کام</p></div>
<div class="m2"><p>که دهد جلوه کبریایی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان توقف کند که دریابی</p></div>
<div class="m2"><p>ذوق در یوزه گدایی را</p></div></div>