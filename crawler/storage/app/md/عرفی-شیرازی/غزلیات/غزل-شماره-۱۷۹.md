---
title: >-
    غزل شمارهٔ ۱۷۹
---
# غزل شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>دل ما را به فسون جادوی بابل نبرد</p></div>
<div class="m2"><p>هر که از بهر وفا جان ندهد دل نبرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی کسی رنگ وفا می طلبد، ور نه به حشر</p></div>
<div class="m2"><p>دست ما آب رخ دامن قاتل نبرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیخودی راه نماید به تو مجنون تو را</p></div>
<div class="m2"><p>هرگز از بانگ جرس راه به محمل نبرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر غم جمله کنار است که از خود گذری</p></div>
<div class="m2"><p>زورق اهل فنا منت ساحل نبرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که اندیشه ی او چشمه ی کوثر نشود</p></div>
<div class="m2"><p>پی به شیرینی آن شکل شمایل نبرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم شمشیر بود رهگذر عشق، ولی</p></div>
<div class="m2"><p>هر که این ره نرود پی به در دل نبرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عازم هیچ غم آباد نگردد غم دوست</p></div>
<div class="m2"><p>که مرا دست در آغوش حمایل نبرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه عدل است چرا برمن عاقل دگری</p></div>
<div class="m2"><p>عقل کل راه به این نکته ی مشکل نبرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سینه خالی مکن از درد که مرد ره عشق</p></div>
<div class="m2"><p>که سبکسار شود بار به منزل نبرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرفی آن شمع در آورد به محفل، کو را</p></div>
<div class="m2"><p>خجلت جلوه ی خورشید به محفل نبرد</p></div></div>