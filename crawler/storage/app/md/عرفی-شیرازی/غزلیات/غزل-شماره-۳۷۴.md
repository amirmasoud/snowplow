---
title: >-
    غزل شمارهٔ ۳۷۴
---
# غزل شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>ای دل ز شوق آن مه نامهربان بسوز</p></div>
<div class="m2"><p>تنها به گوشه ای رو تا می توان بسوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی قبول منصب پروانگی دلا</p></div>
<div class="m2"><p>خود را زدی به آتش او، این زمان بسوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این شعله در جگر نتوان بیش از این نهفت</p></div>
<div class="m2"><p>تا چند حفظ آه کنم، گو جهان بسوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسم به کوی او مبر ای همنشین، بیار</p></div>
<div class="m2"><p>این مشت استخوان و در این آستان بسوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسودگی مباد، که عادت کنی، دلا</p></div>
<div class="m2"><p>رو یک نگاه درکش و در صد گمان بسوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی بسوز داغ گلی بر جگر، ولی</p></div>
<div class="m2"><p>تا کس به مرهمت نفریبد، نهان بسوز</p></div></div>