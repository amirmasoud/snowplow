---
title: >-
    غزل شمارهٔ ۵۰۷
---
# غزل شمارهٔ ۵۰۷

<div class="b" id="bn1"><div class="m1"><p>نیشی گرفته سینهٔ خویش ریش می کنیم</p></div>
<div class="m2"><p>تا هست فرصتم ادب خویش می کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نایاب گوهریست مرادم و گر نه من</p></div>
<div class="m2"><p>دریوزه از نوانگر و درویش می کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیهوده رفتنم ز فروماندگی به است</p></div>
<div class="m2"><p>تا خضر نیست رهبری خویش می کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم که نیست چاره و هر دم ز اضطراب</p></div>
<div class="m2"><p>آزار عقل مصلحت اندیش می کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی اگر ز کاوش دل مانده ام چه باک</p></div>
<div class="m2"><p>ناخن ز کار شد، طلب نیش می کنیم</p></div></div>