---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>تا کوکبه ی رحمت جاوید بلند است</p></div>
<div class="m2"><p>بخت طلب و طالع امید بلند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آوازه ی رندی به جهان پست نگردد</p></div>
<div class="m2"><p>تا زمزمه ی جام ز جمشید بلند است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما گلخنی یان بس که ز بد نامی راحت</p></div>
<div class="m2"><p>از سایه نشینان گل بید بلند است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شیونی یان همدمی ما نگرفتند </p></div>
<div class="m2"><p>از محفل ما نغمه ی ناهید بلند است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی خبر از جلوه ی معشوق ندارد</p></div>
<div class="m2"><p>با ذره بگویند که خورشید بلند است</p></div></div>