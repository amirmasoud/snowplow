---
title: >-
    غزل شمارهٔ ۱۹۰
---
# غزل شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>خوبان که به هم گرمی بازار فروشند</p></div>
<div class="m2"><p>با هم بنشینند و خریدار فروشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما نامه و قاصد نشناسیم و نبینیم</p></div>
<div class="m2"><p>ارباب نظر دیده به دیدار فروشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیران شده گان تو به خورشید قیامت</p></div>
<div class="m2"><p>آسودگی سایهٔ دیوار فروشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما معتکف گوشهء تنهایی خویشیم</p></div>
<div class="m2"><p>آن کعبه روانند که رفتار فروشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن مکن ای مه شب دیجور که عشاق</p></div>
<div class="m2"><p>اندوه دل خود به شب تار فروشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسکین نفس ما که تذروان چمن گرد</p></div>
<div class="m2"><p>پرواز به مرغان گرفتار فروشند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آن که یقین است که در گلشن فردوس</p></div>
<div class="m2"><p> صد گل به تهی دستی هر خار فروشند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین دست تهی در غلط افتم که مبادا</p></div>
<div class="m2"><p>قفل در و خار سر دیوار فروشند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عرفی تو گهر جمع گن امروز که این جنس</p></div>
<div class="m2"><p>بسیار خرند آخر و بسیار فروشند</p></div></div>