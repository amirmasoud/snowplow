---
title: >-
    غزل شمارهٔ ۱۹
---
# غزل شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تا به کی معبچه ی می نوش و بیارا ایمان را</p></div>
<div class="m2"><p>تا به کی پیش بری لعمه ی شادروان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این مزاری است که صد چون تو در و مدفون است</p></div>
<div class="m2"><p>که تو امروز بر و طرح کنی ایوان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله در کشتی نوح اند حریفان در خواب</p></div>
<div class="m2"><p>ورنه هرگز ننشانید قضا توفان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحث با رد و قبول بت ترسا بچه است</p></div>
<div class="m2"><p>ور نه از کفر زبونی نبود ایمان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون اثر در تو کند عشق؟ که اعجاز مسیح</p></div>
<div class="m2"><p>مرده را جان دهد، آدم نکند حیوان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنس دین را چه کساد آمده عرفی در پیش؟</p></div>
<div class="m2"><p>که به جز مرده ز حافظ نخرد قرآن را</p></div></div>