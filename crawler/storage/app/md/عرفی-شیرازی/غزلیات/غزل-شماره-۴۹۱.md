---
title: >-
    غزل شمارهٔ ۴۹۱
---
# غزل شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>تا کی به حرم تشنه لب و مضمحل افتم</p></div>
<div class="m2"><p>کو دیر محبت که به دریای دل افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو معرکهٔ عشق که از بوی شهادت</p></div>
<div class="m2"><p>بی خود شده در لجهٔ خون به حل افتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر که مرا گفت که از باغچهٔ قدس</p></div>
<div class="m2"><p>بی فایده در دامگه آب و گل افتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستی ز من آموز که چون شعلهٔ مرهم</p></div>
<div class="m2"><p>از داغ جگر خیزم و در خون دل افتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو انجمن قرب که تا بال گشایم</p></div>
<div class="m2"><p>پر سوخته پیرامن شمع چگل افتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی که گمان داشت که از وادی اسلام</p></div>
<div class="m2"><p>باز آیم و در سجدهٔ بت منفعل افتم</p></div></div>