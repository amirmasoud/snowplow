---
title: >-
    غزل شمارهٔ ۳۶۴
---
# غزل شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>گر مرد وفایی ره بازار الم گیر</p></div>
<div class="m2"><p>رو پنجه ز الماس کن و دامن غم گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسباب پریشانی ات ای دل همه جمع است</p></div>
<div class="m2"><p>دامن به میان برزده و راه عدم گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیشی به غم دوست برابر نتوان یافت</p></div>
<div class="m2"><p>رو کام دو عالم همه را بر سر هم گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی هوس آموزی جام از دل ما نیست</p></div>
<div class="m2"><p>تاوان صراحی که شکستیم ز خم گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاکستر پروانه طلبکار سموم است</p></div>
<div class="m2"><p>آخر که تو را گفت که آهوی حرم گیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان زلف بر این صید مکش کاین دل عرفی ست</p></div>
<div class="m2"><p>ای باد مسیحی ره گلزار ارم گیر</p></div></div>