---
title: >-
    غزل شمارهٔ ۳۷۱
---
# غزل شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>همین معامله ما را بس است با زنار</p></div>
<div class="m2"><p>که با طبیعت ما گشته آشنا زنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمام عمر به تسبیح کرده ام بازی</p></div>
<div class="m2"><p>کجا طبیعت طفلانه و کجا زنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و تو بیهده کوشیم خود به این قسمت</p></div>
<div class="m2"><p>خبر دهد که که را سبحه و که را زنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو به دیر مغان آ و رایگان بربند</p></div>
<div class="m2"><p>امام ما که به جان خواهد از ریا زنار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشت عمر و ز مستی نیافتم عرفی</p></div>
<div class="m2"><p>که سبحه بود مرا دام ره یا زنار</p></div></div>