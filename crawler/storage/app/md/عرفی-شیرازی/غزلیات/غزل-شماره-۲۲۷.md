---
title: >-
    غزل شمارهٔ ۲۲۷
---
# غزل شمارهٔ ۲۲۷

<div class="b" id="bn1"><div class="m1"><p>در محبت لب خشک و دل تر می خندد</p></div>
<div class="m2"><p>مست مخمور در این تنگ شکر می خندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل دل خنده زنانند و نمی بیند کس</p></div>
<div class="m2"><p>لب این جمع به آیین دگر می خندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای کلیم، آتش ایمن، گل مقصود تو، چیست</p></div>
<div class="m2"><p>به تمنای محال تو شجر می خندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده از شاهد امید فروبند و ببین</p></div>
<div class="m2"><p>که لب شام به صد ذوق سحر می خندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم مباد آب و هوای چمن ما، که در او</p></div>
<div class="m2"><p>گل پژمرده به از لالهٔ تر می خندد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عرفی بود آن مرغ خزان پرورده</p></div>
<div class="m2"><p>که به حبس نفس و بستن پر می خندد</p></div></div>