---
title: >-
    غزل شمارهٔ ۳۰۴
---
# غزل شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>اگر ز کاوش مژگان او دلم خون شد</p></div>
<div class="m2"><p>خوشم که بهر من اسباب گریه افزون شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دم هلاک، به روی تو، بس که، حیران بود</p></div>
<div class="m2"><p>دلم نیافت، که کی، ز سینه، جان بیرون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام قطرهٔ خوی، لیلی، از جبین افشاند</p></div>
<div class="m2"><p>که گاه گریه، برون، ز چشم مجنون شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امید من به محبت، زیاده، چون نشود؟</p></div>
<div class="m2"><p>که دوشِ کوهکن، آرامگاه گلگون شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بت نه گوشهٔ چشمی، نه چین ابرویی</p></div>
<div class="m2"><p>به حیرتم که دل برهمن ز کف چون شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان ز طبع تو عرفی، مگو، بگو کز تو</p></div>
<div class="m2"><p>طبیعتت سبب شهرت همایون شد</p></div></div>