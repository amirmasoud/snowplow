---
title: >-
    غزل شمارهٔ ۳۶۶
---
# غزل شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>چگونه سوز غم او دهم به سوز دگر</p></div>
<div class="m2"><p>که دل فروغ نیابد به دلفروز دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب عشقم اگر بو کنند محشریان</p></div>
<div class="m2"><p>سوال روز قیامت فتد به روز دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز امر و نهی محبت رسوم شرع مجو</p></div>
<div class="m2"><p>که ان یجوز دگر گفت لایجوز دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیار بربط مجنون به مشهد عرفی</p></div>
<div class="m2"><p>که عشق نوحه طرازی کند به سوز دگر</p></div></div>