---
title: >-
    غزل شمارهٔ ۲۱۱
---
# غزل شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>چه مهربان به سفر شد، چه تند قهر آمد</p></div>
<div class="m2"><p>فرشته ای بشد و فتنه ای به شهر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرشمه ای که دگر ناخنی رساند باز</p></div>
<div class="m2"><p>گشود گریهٔ تلخ و هزار نهر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قیاس کن که چه آبم رود به جوی حیات</p></div>
<div class="m2"><p>که گاه گریهٔ شادی ز دیده زهر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شومی دل از عافیت رمیدهٔ من</p></div>
<div class="m2"><p>ز کوه و بادیهٔ آوارگی به شهر آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکو که بی خبر آمد به دهر عرفی و رفت</p></div>
<div class="m2"><p>هر آن که از عدم آمد، چنین به دهر آمد</p></div></div>