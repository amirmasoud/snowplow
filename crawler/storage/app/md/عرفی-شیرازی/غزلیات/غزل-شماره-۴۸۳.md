---
title: >-
    غزل شمارهٔ ۴۸۳
---
# غزل شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>دل کز لبت چغانه به گوشش نمی زنم</p></div>
<div class="m2"><p>مست است، این ترانه به گوشش نمی زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این بس جزای طعنهٔ زاهد که هیچ گاه</p></div>
<div class="m2"><p>قول شرابخانه به گوشش نمی زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عهدش نماند کاین دو جهان گشت باز رمز</p></div>
<div class="m2"><p>بی مهری زمانه به گوشش نمی زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل گوش جان گشوده و با بلبلان باغ</p></div>
<div class="m2"><p>یک بانگ بلبلانه به گوشش نمی زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی به نغمه گوش بیالوده و ما هنوز</p></div>
<div class="m2"><p>از ناله تازیانه به گوشش نمی زنم</p></div></div>