---
title: >-
    غزل شمارهٔ ۵۳۳
---
# غزل شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>ز خونم روی میدان تازه گردان</p></div>
<div class="m2"><p>تمنای شهیدان تازه گردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دل یک لخت دارم نیم خورده</p></div>
<div class="m2"><p>جگر بریان کن و خوان تازه گردان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عالم وقتی آسان مردنی بود</p></div>
<div class="m2"><p>به بالینم بیا وان تازه گردان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر توفان نوحی خواهی از خون</p></div>
<div class="m2"><p>کهن ریشم به مژگان تازه گردان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقص ای نیم بسمل صید، در دل</p></div>
<div class="m2"><p>شکستن های مژگان تازه گردان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چاک جامه گر دل می گشاید</p></div>
<div class="m2"><p>شکر خند گریبان تازه گردان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا در خون سرشتی خاکم، اکنون</p></div>
<div class="m2"><p>کهن دیوار ایمان تازه گردان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز میدان رو متاب، از شیر مردی</p></div>
<div class="m2"><p>مرو، نام شهیدان تازه گردان</p></div></div>