---
title: >-
    غزل شمارهٔ ۳۳۱
---
# غزل شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>دوش کز عشق تو، دل عیب سلامت می کرد</p></div>
<div class="m2"><p>ناگوارایی غم، کار حلاوت می کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان برفت ای غم و همراه نرفتی، آری</p></div>
<div class="m2"><p>این گنه داشت که عمری به تو عادت می کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش کآئینهٔ دل داشتمش پیش نظر</p></div>
<div class="m2"><p>تاب دل بین که تماشای قیامت می کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که توفیق مرا برگ فراغت می داد</p></div>
<div class="m2"><p>کاش خون در دلم از درد قناعت می کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر که مقصود دلم تلخ تر از زهر زیان بود</p></div>
<div class="m2"><p>کی دعا دست در آغوش اجابت می کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نه دوشینه اجل بهر تو می مُرد، چرا</p></div>
<div class="m2"><p>کشتن خلق به ناز تو وصیت می کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیسوی حور پریشانی ماتم بشناخت</p></div>
<div class="m2"><p>ورنه کی سنبل تر گلشن جنت می کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد مردن، به جهان شد، زر عرفی رایج</p></div>
<div class="m2"><p>کاش در عین حیات این همه شهرت می کرد</p></div></div>