---
title: >-
    غزل شمارهٔ ۲۴۸
---
# غزل شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>مقیم کعبه که عیب شرابخانه کند</p></div>
<div class="m2"><p>به این بهانه حدیث می مغانه کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم چکونه نتازد به صیدگاه کسی</p></div>
<div class="m2"><p>که شوق ناوک او کار تازیانه کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ستم فروش درآ، در زمانه، باک مدار</p></div>
<div class="m2"><p>که خوش معاملگی بیشتر زمانه کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکوه عشق نگه کن ، که موی مجنون را</p></div>
<div class="m2"><p>فلک به شعشعهٔ آفتاب شانه کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که خاک درت را کند چو سرمه به چشم</p></div>
<div class="m2"><p>ببین چه بی ادبی ها به آستانه کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جحیم با همه اسباب سوختن ، عرفی</p></div>
<div class="m2"><p>ز برق شمع تو دریوزهٔ زبانه کند</p></div></div>