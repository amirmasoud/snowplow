---
title: >-
    غزل شمارهٔ ۲۹۴
---
# غزل شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>از پی صید دگر، تا بجهاندی سمند</p></div>
<div class="m2"><p>ذوق رهایی نیافت، آهوی سر درکمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عشق ای بلا، مهلت گامی بس است</p></div>
<div class="m2"><p>جان سلامت روی، باد فدای گزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رو که ستم می کند، بر من آرام دوست</p></div>
<div class="m2"><p>دل که فراغش مباد، سینه که بر ما درند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مانده طبیب اجل، عاجز و حیرت زده</p></div>
<div class="m2"><p>همنفس ساده لوح، گو که بسوزد سپند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش که طاعتکده، مجمع بیگانه بود</p></div>
<div class="m2"><p>رخصت جامی نداد، محتسب بالوند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دلم از جام قرب، یافته کیفیتی</p></div>
<div class="m2"><p>ننگ خمار منست، نشأء عشق بلند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به حریم وصال، هم نفس عرفی است</p></div>
<div class="m2"><p>خون لبم می چکد، عاقبت از زهرخند</p></div></div>