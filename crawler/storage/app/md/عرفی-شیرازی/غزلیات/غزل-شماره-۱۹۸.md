---
title: >-
    غزل شمارهٔ ۱۹۸
---
# غزل شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>چند بی بهره شود دیدهٔ گریانی؛ چند؟</p></div>
<div class="m2"><p>زلف جمع آر که جمعند پریشانی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلرخان محنت نایافت بیابند مگر</p></div>
<div class="m2"><p>یک نفس چاک ببینند گریبانی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن که آماده کند پردهٔ نا کرده گناه</p></div>
<div class="m2"><p>کی در او پرده ای از کرده پشیمانی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کبرای تو، بر آنم، که نیارد به نظر</p></div>
<div class="m2"><p>مستی آلوده به آلایش دامانی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی افسانهٔ ما گوش کنان حلقه زدند</p></div>
<div class="m2"><p>خوان بیارآی، که جمع آمده مهمانی چند</p></div></div>