---
title: >-
    غزل شمارهٔ ۵۲۰
---
# غزل شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>به چه رو به جلوه آید، طلب نیازمندان</p></div>
<div class="m2"><p>نه دل نیاز خرم، نه لب امید خندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گله از تهی کمندی، نه روا بود، همین بس</p></div>
<div class="m2"><p>که غزال ما نیفتد به کمند صید بندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کند زبون شکاری، ز چنین شکارگاهی</p></div>
<div class="m2"><p>که خم کمند بوسد، لب عنبرین کمندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه گمان باطل است این، که بود عزیز صیدی</p></div>
<div class="m2"><p>که به عجز بسته گردد، به کمند ارجمندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کرشمه ای بنازم که ز باد دامن او</p></div>
<div class="m2"><p>زده موج زهر آفت، به گلوی نوشخندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه دل است، آه از آن دل، که ز حسن و عشق، در وی</p></div>
<div class="m2"><p>نه علامتی ز ناخن، نه جراحتی ز دندان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه چنان بتاز عرفی، که رود عنان ز دستت</p></div>
<div class="m2"><p>تو هم این حدیث می گوی، به سبک عنان سمندان</p></div></div>