---
title: >-
    غزل شمارهٔ ۲۶۰
---
# غزل شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>هر که حرصش گام زد، کامش روا هرگز نشد</p></div>
<div class="m2"><p>هر که سلطان قناعت شد، گدا هرگز نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کام جانم درمیان آب و آتش حاضر است</p></div>
<div class="m2"><p>هر که با همت برآید بینوا هرگز نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بندهٔ تمکین دل گردم که در راه وفا</p></div>
<div class="m2"><p>سیل غم هر چند افزون شد، ز جا هرگز نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی همین دل یافتست از کعبهٔ عشقت صفا</p></div>
<div class="m2"><p>هر چه در این چشمه شستم بی صفا هرگز نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگزت در دل نیاید کاین پریشان روزگار</p></div>
<div class="m2"><p>شرمسار از یک نگاه آشنا هرگز نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که این درد از من و دل دشمن آسایش است</p></div>
<div class="m2"><p>صد مرض به گشت مجنون را ، شفا هرگز نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هوای پارسایی، عرفی از هر معصیت</p></div>
<div class="m2"><p>گشت صد ره تایب، اما پارسا هرگز نشد</p></div></div>