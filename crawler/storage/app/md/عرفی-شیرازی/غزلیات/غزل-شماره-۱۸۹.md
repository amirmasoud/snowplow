---
title: >-
    غزل شمارهٔ ۱۸۹
---
# غزل شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>عزت گیتی اگر صحبت یوسف باشد</p></div>
<div class="m2"><p>نپذیری مگرت میل تاسف باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسدت بر سر امروز به آن می ماند</p></div>
<div class="m2"><p>که یکی ز اهل نظر دشمن یوسف باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم شهره به علم آفت دین شد، چه بلاست</p></div>
<div class="m2"><p>غلط اندیش که طبعش به تصرف باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این همه عالم و آدم که ز معنی عشق است</p></div>
<div class="m2"><p>گر به عاشق نهد این نام ، تکلف باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکته ای چند بگویم ز حقیقت ، عرفی</p></div>
<div class="m2"><p>لیک وقتی که تو را ذوق تصوف باشد</p></div></div>