---
title: >-
    غزل شمارهٔ ۴۳۰
---
# غزل شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>رفتیم و با غمت دل پر خون گذاشتیم</p></div>
<div class="m2"><p>جان را به صیدگاه تو در خون گذاشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتیم و دل رمیده و شبدیز غیر را</p></div>
<div class="m2"><p>با شوق بی عنانی گلگون گذاشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتیم و توبه کرده ز میخانهٔ مراد</p></div>
<div class="m2"><p>میل قدح به آن لب میگون گذاشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفتیم و در زمانه ز غمنامه های تو</p></div>
<div class="m2"><p>نشنودهٔ غم تو به مجنون گذاشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتیم و انتقام ستم های غیر را</p></div>
<div class="m2"><p>با عادت طبیعت گردون گذاشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رفتیم عرفی از چمن وصل ناامید</p></div>
<div class="m2"><p>در دل هوای آن قد موزون گذاشتیم</p></div></div>