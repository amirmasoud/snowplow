---
title: >-
    غزل شمارهٔ ۱۵۶
---
# غزل شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>گر دل عنان فرصت از آغاز می گرفت</p></div>
<div class="m2"><p>کام ابد ز طالع ناساز می گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سایه ی همای سعادت نمی گذاشت</p></div>
<div class="m2"><p>کبک دری ز چنگل شهباز می گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر در کمین وسوسه هشیاری کس است</p></div>
<div class="m2"><p>جاسوس طبع خانه برانداز می گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر در فریب گاه سلامت نمی غنود</p></div>
<div class="m2"><p>صد دزد خانگی به در راز می گرفت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیمانه ی غرور لیالب نمی کشید</p></div>
<div class="m2"><p>گر ساغری ز مردم طناز می گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر می گذاشت غمزه ی سافی به دست صبر</p></div>
<div class="m2"><p>از دست او پیاله به عهد ناز می گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک جام بی تبسمی اکنون نمی دهد</p></div>
<div class="m2"><p>مشتی که زهر چشم ز من باز می گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی ز پا فتاده همین بود در جهان </p></div>
<div class="m2"><p>مرعی که کام خویش ز پرواز می گرفت</p></div></div>