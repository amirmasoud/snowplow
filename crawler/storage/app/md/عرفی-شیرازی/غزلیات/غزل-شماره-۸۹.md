---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>ممنون ترکتازی گردون دل من است</p></div>
<div class="m2"><p>آماده ی هزار شبیخون دل من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نیامدش به غلط محملی به سر</p></div>
<div class="m2"><p>بیهوده گرد وادی مجنون دل من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد لاله زار داغ شکفته است بر دلم</p></div>
<div class="m2"><p>برگ چمن ز صد افزون دل من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دل ترا نکرد به آهنگ آشنا</p></div>
<div class="m2"><p>در مانده ی فسانه و افسون دل من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دور دهر سینه ی عرفی و جام زهر</p></div>
<div class="m2"><p>در بزم شوق شیشه ی پر خون دل من است</p></div></div>