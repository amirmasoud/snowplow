---
title: >-
    غزل شمارهٔ ۱۷۶
---
# غزل شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>هجران شب تار ما ندارد</p></div>
<div class="m2"><p>غم عقدهٔ کار ما ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا جان به هوای گل فشانیم</p></div>
<div class="m2"><p>گل میل کنار ما ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر عزم سفر کند خوشش باد</p></div>
<div class="m2"><p>جان طاقت بار ما ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فردوس شراب دارد اما </p></div>
<div class="m2"><p>پیمانه گُسار ما ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی می ناب دارد، اما</p></div>
<div class="m2"><p>در خورد خمار ما ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس که رهین حرف و صوت است</p></div>
<div class="m2"><p>پیغام-نگار ما ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس که رمیده ایم و ترسان</p></div>
<div class="m2"><p>غم ذوق شکار ما ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی نه ز دوست-دشمنان است</p></div>
<div class="m2"><p>اما غم کار ما ندارد</p></div></div>