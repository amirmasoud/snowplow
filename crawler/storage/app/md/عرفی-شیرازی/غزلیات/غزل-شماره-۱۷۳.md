---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>درد کیشان همه ناموس کش کیش همند </p></div>
<div class="m2"><p>غمگسار هم و ناسور کن نیش همند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبح تا شام گدای هم و شب تا به سحر</p></div>
<div class="m2"><p>شکر دریوزه گذار دل ریش همند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان به صورت بشتابند و به آمیزش هم</p></div>
<div class="m2"><p>که به خلوتگه معنی همه در پیش همند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست از ین جمع پریشان بنمای، کایشان</p></div>
<div class="m2"><p>همه بیگانه ی خویشند و بی خویش همند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفر و دین را ببر از یاد که این فتنه گران</p></div>
<div class="m2"><p>در بد آموزی ما مصلحت اندیش همند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی این نکته ی مجموعه ی احباب نویس</p></div>
<div class="m2"><p>که محبان وفا تازه کن ریش همند</p></div></div>