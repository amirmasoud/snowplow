---
title: >-
    غزل شمارهٔ ۳۲۱
---
# غزل شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>هرکس به روز نیک مرا غمگسار شد</p></div>
<div class="m2"><p>در روز بد مرا دژم روزگار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقی توئی و ساده دلی بین که شیخ شهر</p></div>
<div class="m2"><p>باور نمی کند که ملک میگسار شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنمای رخ که چهره نمی داند از نقاب</p></div>
<div class="m2"><p>چشمی که مست گریهٔ بی اختیار شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی ذوق در طریق عمل کامل اوفتاد</p></div>
<div class="m2"><p>زد تکیه بر قناعت و امیدوار شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد از هزار جام قدح نوش، ذوق را</p></div>
<div class="m2"><p>عادت به درد سر شد و دفع خمار شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حسن از عمل نتیجهٔ شرم است و بازگشت</p></div>
<div class="m2"><p>نی هر که خون چکاند ز رخ شرمسار شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز با گریستن مژه ای در جهان نبود</p></div>
<div class="m2"><p>آن هم ز حرص مردم دیدهٔ ما ناگوار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر چند دست و پا زدم، آشفته تر شدم</p></div>
<div class="m2"><p>ساکن شدم در میانهٔ دریاکنار شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عرفی بسی ملاف که بر چرخ تاختم</p></div>
<div class="m2"><p>مردی کنون بتاز که بختی سوار شد</p></div></div>