---
title: >-
    غزل شمارهٔ ۲۳۴
---
# غزل شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>عرض کردیم به زاهد که ریا نفروشد</p></div>
<div class="m2"><p>کفر اندودهٔ اسلام به ما نفروشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو بنه بر سر دل منت و بسیار منه</p></div>
<div class="m2"><p>آن که بیماری دل را به شفا نفروشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق آن است که گر جان بدهد بد نامی</p></div>
<div class="m2"><p>گرمی سینه وتاثیر دعا نفروشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر فروشند بهای مه کنعان داند</p></div>
<div class="m2"><p>به متاع دو جهانش ، به خدا، نفروشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرد سودای محبت بود آن کس عرفی</p></div>
<div class="m2"><p>که دهد عیش ابد مفت و بلا نفروشد</p></div></div>