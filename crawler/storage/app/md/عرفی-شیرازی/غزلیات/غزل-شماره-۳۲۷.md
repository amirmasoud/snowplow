---
title: >-
    غزل شمارهٔ ۳۲۷
---
# غزل شمارهٔ ۳۲۷

<div class="b" id="bn1"><div class="m1"><p>تا چند به زنجیر خرد بند توان بود</p></div>
<div class="m2"><p>بی مستی و آشوب جنون چند توان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی بکشم، تا به کی از اهل خرابات</p></div>
<div class="m2"><p>شرمنده ز نشکستن سوگند توان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی رنگی و دیوانگئی پیش بگیریم</p></div>
<div class="m2"><p>تا چند خود آرای و خردمند توان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ننگ فرورفتم و زین راحت و آرام</p></div>
<div class="m2"><p>دردی نه، بلایی نه، چنین چند توان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مژدهٔ الماس دما دم برسانند </p></div>
<div class="m2"><p>صد سال به یک زخم تو خرسند توان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یعقوب مده دل به جگر گوشهٔ مردم</p></div>
<div class="m2"><p>تا چند اسیر غم فرزند توان بود</p></div></div>