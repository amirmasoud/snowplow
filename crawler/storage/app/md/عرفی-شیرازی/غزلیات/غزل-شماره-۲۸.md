---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>نوش دارو نشأ ی علت نهد در جان ما</p></div>
<div class="m2"><p>در خمار معجز افتد عیسی از درمان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبروی شمع را بیهوده نتوان ریختن</p></div>
<div class="m2"><p>صد شب یلداست در هر گوشه ی زندان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما خجل اما سخن در صنعت مشاطه اشت</p></div>
<div class="m2"><p>گر نمود کفر دارد شاهد ایمان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم ها برداشتیم و فتح ها کردیم، لیک</p></div>
<div class="m2"><p>هرگز از خون کسی رنگین نشد میدان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم اگر باز است و گر پوشیده از هم نگسلد</p></div>
<div class="m2"><p>آمد و رفت نظر در دیده ی حیران ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی ز عصمت پاک دامانیم کز ناموس و ننگ </p></div>
<div class="m2"><p>می کند آلودگی پرهیز از دامان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معنی روشن برون می جوشدم عرفی ز دل</p></div>
<div class="m2"><p>در سیاهی می نگنجد چشمه ی حیوان ما</p></div></div>