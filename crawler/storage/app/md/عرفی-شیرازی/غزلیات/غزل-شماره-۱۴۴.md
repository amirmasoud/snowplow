---
title: >-
    غزل شمارهٔ ۱۴۴
---
# غزل شمارهٔ ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>شکستن دل ما کار زور بازو نیست</p></div>
<div class="m2"><p>هلاک اهل وفا جر به نوشدارو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عیب جویی مجنون بدم ولی گویم</p></div>
<div class="m2"><p>خوشا دلی که تسلی به چشم آهو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین گلی نه از این لاله زار دهر برست</p></div>
<div class="m2"><p>وگر نیست سخن در جهان که خودرو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علاچ زخم نه بازوی چاره خواست کند</p></div>
<div class="m2"><p>سرم که همدم درد است بار زانو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز فیض طبع کسی بهره ساز شد عرفی</p></div>
<div class="m2"><p>وگر نه چون دگران شاعر است، جادو نیست</p></div></div>