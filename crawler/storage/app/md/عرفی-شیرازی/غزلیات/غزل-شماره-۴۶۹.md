---
title: >-
    غزل شمارهٔ ۴۶۹
---
# غزل شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>ای ساقی بلا ز شراب تو سوختیم</p></div>
<div class="m2"><p>با آن که آتشیم ز آب تو سوختیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شب گذشت عمر و ندیدیم روی صبح</p></div>
<div class="m2"><p>ای بخت از گرانی خواب تو سوختیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پایت رکاب پرور و دستت عنان نواز</p></div>
<div class="m2"><p>از غیرت عنان و رکاب تو سوختیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طالع نگر که گرم عتاب آمدی و ما</p></div>
<div class="m2"><p>نا برده لذتی ز عتاب تو سوختیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گرمی محبت ما سوخت شرم یار</p></div>
<div class="m2"><p>ای عشق جلوه کن نقاب تو سوختیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خود روانه ایم به معمورهٔ عدم</p></div>
<div class="m2"><p>عرفی تحملی ز شتاب تو سوختیم</p></div></div>