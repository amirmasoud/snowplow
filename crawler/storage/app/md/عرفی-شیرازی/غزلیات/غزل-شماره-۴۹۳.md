---
title: >-
    غزل شمارهٔ ۴۹۳
---
# غزل شمارهٔ ۴۹۳

<div class="b" id="bn1"><div class="m1"><p>زین بزم نه این بار بر آشفتم و رفتم</p></div>
<div class="m2"><p>کی بود که تلخی ز تو نشنفتم و رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد اثر سودهٔ الماس به چشمم</p></div>
<div class="m2"><p>گردی که ز مژگان ز درت رُفتم و رفتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای هم نفسان رفتن از این غمکده کم نیست</p></div>
<div class="m2"><p>پژمرده مباشید که بشکفتم و رفتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امید که در نامهٔ من ثبت نباشد</p></div>
<div class="m2"><p>این راز که از غیر تو بنهفتم و رفتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناصح مفشان بر جگرم نیش و همان گیر</p></div>
<div class="m2"><p>این هرزه به جان از تو پذیرفتم و رفتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این تلخی جان دادن از آن غمزه ببینید</p></div>
<div class="m2"><p>ای اهل سلامت سخنی گفتم و رفتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی در ناسفته در این بحر بسی هست</p></div>
<div class="m2"><p>انگار که صد درج گهر سفتم و رفتم</p></div></div>