---
title: >-
    غزل شمارهٔ ۸۰
---
# غزل شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>رسید مژده و قاصد مقیم خرگه ماست</p></div>
<div class="m2"><p>که بر گزیده توفیق، جان آگه ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی که چاه ملامت به راه می کندی</p></div>
<div class="m2"><p>به ریسمان خود اکنون فتاده در چه ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شیخ شهر شنو درس و علم ما آموز</p></div>
<div class="m2"><p>که هر چه رد مشایخ بود موجه ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خروش و ولوله ی عالمان شهرآشوب</p></div>
<div class="m2"><p>گناه حوصله ی تنگ ظرف بی ته ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز طرف درگه دارا نتیجه ای مطلب</p></div>
<div class="m2"><p>که آستانه ی جانان دل مرفه ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقیم شهپر عنقاست محمل عشاق</p></div>
<div class="m2"><p>از این چه باک که صد کوه فتنه در ره ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مباش عمزده عرفی که زلف قامت دوست</p></div>
<div class="m2"><p>جزای همت عالی و دست کوته ماست</p></div></div>