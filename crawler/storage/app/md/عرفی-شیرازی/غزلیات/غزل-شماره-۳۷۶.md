---
title: >-
    غزل شمارهٔ ۳۷۶
---
# غزل شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>مُردم و دارد جمال او دلم روشن هنوز</p></div>
<div class="m2"><p>نور می بارد ز نخل وادی ایمن هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی پیراهن دماغ پیر کنعان می گزد</p></div>
<div class="m2"><p>ور نه باد مصر دارد بوی پیراهن هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که دوش از دود دل کاشانه را پر کرده ام</p></div>
<div class="m2"><p>خاک گشت و روشنایی نیست در گلخن هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد مردن بین که از صبح ازل معشوق و عشق </p></div>
<div class="m2"><p>رو به هم تازند، نی دست است و نی دامن هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بهاران می وزد باد نشاط و دهر را </p></div>
<div class="m2"><p>یک گلی زین باغ نشکفته است در گلشن هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرف مسندگاه جم، عرفی، میاور بر زبان</p></div>
<div class="m2"><p>با چنان مستی که می داند ره گلخن هنوز</p></div></div>