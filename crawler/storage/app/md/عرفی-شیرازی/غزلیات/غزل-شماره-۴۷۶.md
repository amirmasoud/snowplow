---
title: >-
    غزل شمارهٔ ۴۷۶
---
# غزل شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>به کوی صید بندان، دوش چون فریاد می کردم</p></div>
<div class="m2"><p>به یک صوت حزین صد عندلیب آزاد می کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان دوش از غمت مشتاق بودم بر هلاک خود</p></div>
<div class="m2"><p>که تا صبح آرزوی تیشهٔ فرهاد می کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه تاثیر نفس بی عمر جاویدان نمی دانم</p></div>
<div class="m2"><p>به امید چه پیشت درد دل بنیاد می کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گشایم دام بر گنجشگ و شادم، باد آن همت</p></div>
<div class="m2"><p>که گر سیمرغ می امد به دام، آزاد می کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان آمادهٔ عشقم که عشق ار ممتنع بودی</p></div>
<div class="m2"><p>به ذوق جلوهٔ حسن منش آزاد می کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو عرفی، دل یاران پریشان داشتن تا کی</p></div>
<div class="m2"><p>اگر می آمد از دستم، دل خود شاد می کردم</p></div></div>