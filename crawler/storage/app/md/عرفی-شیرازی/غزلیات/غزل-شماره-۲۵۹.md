---
title: >-
    غزل شمارهٔ ۲۵۹
---
# غزل شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>به یادم هرگز آن نخل قد موزون نمی آید</p></div>
<div class="m2"><p>که از هر دیده ام صد چشمه خون بیرون نمی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدامین دوست می آید به نزدیک من گریان</p></div>
<div class="m2"><p>که تا آمد برمن، صد قدم بیرون نمی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانم که سنگ فتنه در هنگامه می بارد</p></div>
<div class="m2"><p>که این بی رحمی از بیداد گردون نمی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داغ دل کند دست ملامت آن نمکسایی</p></div>
<div class="m2"><p>که هنگام تبسم زان لب میگون نمی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نام ناقه گاهی دوست را از نار می گیرد</p></div>
<div class="m2"><p>که دیگر جست و جوی لیلی از مجنون نمی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزد این گریه ها بر آتشم آبی و دانستم</p></div>
<div class="m2"><p>که صد توفان نوح از عهده اش بیرون نمی آید</p></div></div>