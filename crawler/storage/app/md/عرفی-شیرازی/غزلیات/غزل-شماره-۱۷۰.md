---
title: >-
    غزل شمارهٔ ۱۷۰
---
# غزل شمارهٔ ۱۷۰

<div class="b" id="bn1"><div class="m1"><p>چون عشق بت ز کعبه به دیرم حواله کرد</p></div>
<div class="m2"><p>تسبیح شکر گو شد و ناقوس ناله کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آستان دیر نهادیم روی گرم</p></div>
<div class="m2"><p>هر ذره صد معامله با روی لاله کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب حیات چون طلبد کس، که بخت ما</p></div>
<div class="m2"><p>این زهر هم به خون جگر در پیاله کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجموعه ساز عشق الم نامه ی مرا</p></div>
<div class="m2"><p>نا خوانده برد، خاتمه ی صد رساله کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیغی که تافت رو ز جگر گوشه ی خلیل</p></div>
<div class="m2"><p>امروز عشق بر سر عرفی حواله کرد</p></div></div>