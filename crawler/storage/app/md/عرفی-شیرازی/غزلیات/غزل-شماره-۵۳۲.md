---
title: >-
    غزل شمارهٔ ۵۳۲
---
# غزل شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>میان دعا بر دل شب مزن</p></div>
<div class="m2"><p>ز لب ناله برچین و یا رب مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مزن لاف اسلام، اگر می زنی</p></div>
<div class="m2"><p>چو ملزم بر آنی به مشرب مزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جولان خود هم مزن خنده ای</p></div>
<div class="m2"><p>همین گو ز بالای اشهب مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی حسنت الوانت این مست گل</p></div>
<div class="m2"><p>که در خون سرشتی به قالب مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شمشیر ترک طلب کشته شو</p></div>
<div class="m2"><p>شبیخون فرصت به مطلب مزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شبیخون زند غم به عرفی، بگو</p></div>
<div class="m2"><p>که بانگ هزیمت به مرکب مزن</p></div></div>