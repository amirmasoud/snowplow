---
title: >-
    غزل شمارهٔ ۵۴۵
---
# غزل شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>شب نشد از تاب تب پیرهن آتشکده</p></div>
<div class="m2"><p>پیرهنم شعله بود، انجمن آتشکده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت شیرین بکاشت، گلشنی از خار و خس</p></div>
<div class="m2"><p>بهر خود آماده ساخت، کوهکن آتشکده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینهٔ سوزان من، قبلهٔ گبران شده است</p></div>
<div class="m2"><p>روح من آتش بود، جسم من آتشکده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرد نگردد ز مرگ، ای دل آتش فروز</p></div>
<div class="m2"><p>می برم از پیرهن در کفن آتشکده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رو سوی گلخن دلا، باغ و گلستان مشو</p></div>
<div class="m2"><p>بس که بر افروختی در چمن آتشکده</p></div></div>