---
title: >-
    غزل شمارهٔ ۲۲۰
---
# غزل شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>خضر اگر بر لب کس منت آبی دارد</p></div>
<div class="m2"><p>بگذر از چشمهٔ حیوان که سرابی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>التفاتش به لب تشنهٔ ما نیست، دریغ</p></div>
<div class="m2"><p>هر که جام سخنی، زهر عتابی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عاشق نکند دست به زلف تو دراز</p></div>
<div class="m2"><p>هر جنون شوری و هر سلسله تابی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لن ترانی شنود مهتر ما، بی ارنی</p></div>
<div class="m2"><p>این حدیث است که هر وقت جوابی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگ گل را ندهد زحمت دیبا و حریر</p></div>
<div class="m2"><p>او که چون حیرت دیدار ، نقابی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسمان گر به جدل پای در آرد به رکاب</p></div>
<div class="m2"><p>رخش ما نیز عنانی و رکابی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظم عرفی ، تر و تازه است؛ چه عالی ، چه وسط</p></div>
<div class="m2"><p>خار و گل هر چه دهد، حسن شبابی دارد</p></div></div>