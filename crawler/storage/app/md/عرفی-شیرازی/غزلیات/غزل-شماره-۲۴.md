---
title: >-
    غزل شمارهٔ ۲۴
---
# غزل شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>تحفهٔ مرهم نگیرد سینهٔ افکار ما</p></div>
<div class="m2"><p>سایهٔ گل برنتابد گوشهٔ دستار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باعثی دارد رواج، سبحه کو، تزویر کو</p></div>
<div class="m2"><p>تا ببندد صد گره بر رشتهٔ زنار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما لب آلوده بهر توبه بگشاییم، لیک</p></div>
<div class="m2"><p>بانگ عصیان می‌زند ناقوس استغفار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش‌افروز تب هجریم و هرگز کس ندید</p></div>
<div class="m2"><p>جوش تبخال شفاعت بر لب زنهار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرحبا ای چاره، آسان می‌گشایی کار خلق</p></div>
<div class="m2"><p>ناخنی بس تیز داری رخنه‌ای در کار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساکن میخانهٔ ما باش عرفی زان که نیست</p></div>
<div class="m2"><p>چشمهٔ نور و صفا در سایهٔ دیوار ما</p></div></div>