---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ای دل پیاله گیر که وقت صبوح تست</p></div>
<div class="m2"><p>کز فیض جمله فتح، محل فتوح تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه ای که صورت و معنی نمایدت</p></div>
<div class="m2"><p>دست است، گر جمله سوخته، در جیب روح تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسباب عفو را، چه به ما، جلوه می دهی</p></div>
<div class="m2"><p>ما توبه دشمنیم و ستم را صبوح تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اهل مسیح را به فلک بر، مسیح وار</p></div>
<div class="m2"><p>این گریه من است، نه توفان نوح تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاران ز شیر دختر رز در صبوحی اند</p></div>
<div class="m2"><p>عرفی تو جام زهر کش، کین صبوح تست</p></div></div>