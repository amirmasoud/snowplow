---
title: >-
    غزل شمارهٔ ۴۳۱
---
# غزل شمارهٔ ۴۳۱

<div class="b" id="bn1"><div class="m1"><p>منم که بهر دل اسباب داغ می دزدم</p></div>
<div class="m2"><p>نسیم گلشن غم در دماغ می دزدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دمی که بر نفس اهل درد می جوشم</p></div>
<div class="m2"><p>هزار شعله از دود چراغ می دزدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بهر آن که چکانم به کام تشنه لبان</p></div>
<div class="m2"><p>به آستین نمک و خون داغ می دزدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر به وادی ایمن رسم وگر نه که من </p></div>
<div class="m2"><p>ز گرد بادیه کحل سراغ می دزدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیم ز فصل خزان عرفی از چمن بی فیض</p></div>
<div class="m2"><p>ترانهٔ ز نواهای زاغ می دزدم</p></div></div>