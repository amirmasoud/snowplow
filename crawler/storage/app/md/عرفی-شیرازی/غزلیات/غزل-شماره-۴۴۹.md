---
title: >-
    غزل شمارهٔ ۴۴۹
---
# غزل شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>ما دل به جان خریده و بر باد داده ایم</p></div>
<div class="m2"><p>مرغ حرم گرفته به صیاد داده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهل است با قفس، دل اگر رفت به سوی دوست</p></div>
<div class="m2"><p>ما مرغ کشته ایم که بر باد داده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمایهٔ متاع محبت به دست ماست</p></div>
<div class="m2"><p>این مشتهر به گوش نفریاد داده ایم</p></div></div>