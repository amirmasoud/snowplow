---
title: >-
    غزل شمارهٔ ۳۸۹
---
# غزل شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>منم که می کنم از درد بی کرانهٔ خویش</p></div>
<div class="m2"><p>مگو، مگو ز غم، آرایش زمانهٔ خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک به چرب زبانی، گدای فرصت نیست</p></div>
<div class="m2"><p>به مدعی ندهی، گوهر یگانهٔ خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نفخ صور نه توفان نوح بی خطر است</p></div>
<div class="m2"><p>چرا نتازد عنقا به آشیانهٔ خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وعده گاه تو امید آنقدر بنشاند</p></div>
<div class="m2"><p>که در دیار خودم سوخت خانهٔ خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراب آتش رمز محبتم عرفی</p></div>
<div class="m2"><p>که در شرار نهان می کند زبانهٔ خویش</p></div></div>