---
title: >-
    غزل شمارهٔ ۲۴۳
---
# غزل شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>نگرفتم از تو جامی، سرم این خمار دارد</p></div>
<div class="m2"><p>به ره تو دیر مردم، دلم این غبار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بهانهٔ ترحم ، نکشی مرا ، وگرنه</p></div>
<div class="m2"><p>سر خون گرفتهٔ من، به بدن چه کار دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل تنگ عیش مارا، که شمارد از صبوران</p></div>
<div class="m2"><p>که هزار زخم دندان، جگرش نگار دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخنم از آن نباشد، بر اهل عیش روشن</p></div>
<div class="m2"><p>که چو باد کوچهٔ غم، نفسم غبار دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز متاع شهر سنت، بود آن گران تجمل</p></div>
<div class="m2"><p>که ز عشوه جشم بندد، ز کرشمه عار دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه شهید غمزهٔ او، دهد این نشانه، عرفی</p></div>
<div class="m2"><p>که هزار شمع عشرت، ز سر مزار دارد</p></div></div>