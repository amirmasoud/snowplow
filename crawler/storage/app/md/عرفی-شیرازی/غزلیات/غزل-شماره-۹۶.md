---
title: >-
    غزل شمارهٔ ۹۶
---
# غزل شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>دلم به قبله ی اسلام مایل افتاده است</p></div>
<div class="m2"><p>صنم تراش من از کفر غافل افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا معامله در کوچه ایست با مرهم</p></div>
<div class="m2"><p>که صد مسیح به یک زخم بسمل افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دیر می رود ای کعبه رو رهت ، فریاد</p></div>
<div class="m2"><p>که مست خوابی و آتش به محمل افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طواف کعبه مبادا که نا امید شوم </p></div>
<div class="m2"><p>مدد کنید که جمازه در گل افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از فریب عمارت گدا شدم ور نه</p></div>
<div class="m2"><p>هزار گنج به ویرانه ی دل افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چگونه گریه بجوشد که چشم حیرانم </p></div>
<div class="m2"><p>به آفتاب قیامت مقابل افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بار درد سبک مایه دان شهیدان را</p></div>
<div class="m2"><p>که در محیط محبت به ساحل افتاده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بحر جود کریمی که تشته در طلب است</p></div>
<div class="m2"><p>هزار پایه گداتر ز سایل افتاده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به آستان محبت شهید شد عرفی </p></div>
<div class="m2"><p>برهمنی به در کعبه بسمل افتاده است</p></div></div>