---
title: >-
    غزل شمارهٔ ۱۶۴
---
# غزل شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>عشق اگر مرد است، مرد او تاب دیدار آورد </p></div>
<div class="m2"><p>ورنه چون موسی بسی آورد، بسیار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا فریبد ابلهان را در متاع روی دوست</p></div>
<div class="m2"><p>آسمان پیش از تو یوسف را به بازار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بس که زخم غمزه خوردم زمین مشهدم</p></div>
<div class="m2"><p>خرمن خنجر به جای بوته ی خار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کافری دان عشق را کز شغل من گر وارهد</p></div>
<div class="m2"><p>گردن روح القدس در قید زنار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذر از دارالشفای عشق کز بهر علاج</p></div>
<div class="m2"><p>هر نفس آید مسیح آن جا و بیمار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مو به مویم دوست شد، ترسم که استیلای عشق</p></div>
<div class="m2"><p>یک انالحق گوی دیگر بر سر دار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای که عرفی را مسلمان خوانده ای، او را بکاو</p></div>
<div class="m2"><p>تا ز کفرآباد دل، بت های پندار آورد</p></div></div>