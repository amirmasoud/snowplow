---
title: >-
    غزل شمارهٔ ۵۱۴
---
# غزل شمارهٔ ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>ز من نبوده فغانی که دوش می کردم</p></div>
<div class="m2"><p>نصیحت غم روی تو گوش می کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان به شیوهٔ اهل دل است ای بلبل</p></div>
<div class="m2"><p>وگر نه من ز تو افزون خروش می کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرم به جمع افسردگان قدم می رفت</p></div>
<div class="m2"><p>به نالهٔ همه را شعله نوش می کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز صد وصال نیاید به شب، آن چه من به خیال</p></div>
<div class="m2"><p>ز شیوه های تو با عقل و هوش می کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان حلاوت لعل تو می ستودم دوش</p></div>
<div class="m2"><p>که نیش را متاثر ز نوش می کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به راز فشانی لبم اجازت داشت</p></div>
<div class="m2"><p>چه ها به عابد طاعت فروش می کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهم به این همه تردامنی، همان، عرفی</p></div>
<div class="m2"><p>که عیب زاهد پشمینه پوش می کردم</p></div></div>