---
title: >-
    غزل شمارهٔ ۱۰۸
---
# غزل شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>نشاء مخموری ام با مستی مجنون یکی است</p></div>
<div class="m2"><p>صد شرابم هست در ساغر کزان ها خون یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فسون عافیت بر می فروزم روی زرد</p></div>
<div class="m2"><p>در مزاج من بخار دوزخ و افسون یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر فرهاد کز جام محبت بی خود است</p></div>
<div class="m2"><p>سایه ی شیرین و زخم تیشه ی گلگون یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جفایی گر تواند می کند گردون همان</p></div>
<div class="m2"><p>سوزم از غیرت که آیین بودن گردون یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو مزاج آب و آتش را یکی داند چه عیب</p></div>
<div class="m2"><p>آن که گوید اشک عرفی با در مکنون یکی است</p></div></div>