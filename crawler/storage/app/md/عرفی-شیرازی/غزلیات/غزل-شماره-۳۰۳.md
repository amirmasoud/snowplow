---
title: >-
    غزل شمارهٔ ۳۰۳
---
# غزل شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>دل بشد فرزانه و عقل از فسون دلگیر شد</p></div>
<div class="m2"><p>مُلک شوقم را فریبت از پی تعمیر شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسبت دل با خودم دیدم، بسی کم مایه بود</p></div>
<div class="m2"><p>بر جنون افزودمش تا قابل زنجیر شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یافتم تعبیر رنگی چون به بالینم نشست</p></div>
<div class="m2"><p>گر چه استغنای حسنش مانع تغییر شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیست تا گوید به شیرین کز هوای جلوه ات</p></div>
<div class="m2"><p>آب چشم کوهکن داخل به جوی شیر شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو را بی مهر گفتم، شکوه مقصودم نبود</p></div>
<div class="m2"><p>شکر درد خویش گفتم که بی تاثیر شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس که تابوتم گرانبار از دل پر حسرت است</p></div>
<div class="m2"><p>خلقی از همراهی تابوت من دلگیر شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با وجود آن که جرم از جانب عرفی نبود</p></div>
<div class="m2"><p>بی زبانی بین که قایل به صد تقصیر شد</p></div></div>