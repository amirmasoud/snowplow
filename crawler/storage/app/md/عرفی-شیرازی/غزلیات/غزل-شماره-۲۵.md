---
title: >-
    غزل شمارهٔ ۲۵
---
# غزل شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>فارغیم ای عاملان حشر ز احسان شما</p></div>
<div class="m2"><p>کشت و کار ما نمی گنجد به میزان شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندی ام ای میر دیوان، جزا ثابت بود</p></div>
<div class="m2"><p>من صبوحی کرده می آیم به دیوان شما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست غم ز آلودگی ای سالکان راه عشق</p></div>
<div class="m2"><p>دست کوثر می فشاند گرد ایوان شما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب ما طلوع از مشرق یثرب نمود</p></div>
<div class="m2"><p>فارغیم ای مصریان از ماه کنعان شما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفته رفته کار خود می ساختم ناپایدار</p></div>
<div class="m2"><p>گر بگشتی دستگیرم فیض احسان شما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب گذشت و جام می لب تر نکردی زاهدا</p></div>
<div class="m2"><p>مجلس رندان ندارد طاقت شأن شما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست عدل ای سینه ریشان گر نبخشد مرهمی</p></div>
<div class="m2"><p>طاق کسرا می کند چاک گریبان شما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرض مال، ای منعمان، بر می کشان، بی حرمتی است</p></div>
<div class="m2"><p>خرج یک بزم شراب ماست سامان شما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تبسم بر سر خوبان چرا منت نهند</p></div>
<div class="m2"><p>این ملاحت با نمک هست از نمکدان شما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سوخت عرفی از حجاب ناکسان کوی عشق</p></div>
<div class="m2"><p>شرم حرمت برنتابد روی مهران شما</p></div></div>