---
title: >-
    غزل شمارهٔ ۳۶۵
---
# غزل شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>شراب یاس به جام و سبوی ما بگذار</p></div>
<div class="m2"><p>شکسته رنگیِ ما را به روی ما بگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شراب، اگر خون دل، اگر الماس</p></div>
<div class="m2"><p>تو گوشه گیر و به کام گلوی ما بگذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به کشتزار غم، ای اشک، صد نظر دارم</p></div>
<div class="m2"><p>به ذوق گریه که آبی به جوی ما بگذار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نوحه وا نتوان داشت گریه مستان را</p></div>
<div class="m2"><p>تغافلی کن و ما را به خوی ما بگذار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن سراغ سراسیمه گان شوق را ای خضر</p></div>
<div class="m2"><p>نه آهنین قدمی، جست و جوی ما بگذار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهفته نذر تو ای محتسب دو جامی هست</p></div>
<div class="m2"><p>صراحی همه بشکن، سبوی ما بگذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بیع گاه مذلت چنین مبر عرفی</p></div>
<div class="m2"><p>تو این معامله با آبروی ما بگذار</p></div></div>