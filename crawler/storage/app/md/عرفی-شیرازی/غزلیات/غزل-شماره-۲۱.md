---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>تا تیز کرده ای به سیاست نگاه را</p></div>
<div class="m2"><p>صد منت است بر دل عاشق گناه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای روی غم سیاه که از شرم گریه ام</p></div>
<div class="m2"><p>بر پشت پا دوخته چشم سیاه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تلخی به عیش او نرساند ملال من</p></div>
<div class="m2"><p>از ماتم گدا چه زیان عید شاه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر گه فتاد رهم به صحرای معرفت</p></div>
<div class="m2"><p>با برق در معامله دیدم گیاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فردا به خلق تا بنمایم عطای دوست</p></div>
<div class="m2"><p>ثابت کنم به خویش، دو عالم گناه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی طمع مدار مدارا ز خوی دوست</p></div>
<div class="m2"><p>در دل نگاه دار سرآسیمه آه را</p></div></div>