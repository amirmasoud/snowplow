---
title: >-
    غزل شمارهٔ ۱۷۸
---
# غزل شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>در چمن حوروشان انجمنی ساخته اند</p></div>
<div class="m2"><p>چشم بد دور که بهشتی چمنی ساخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ننشیند دل این طایفه در قصر بهشت</p></div>
<div class="m2"><p>که به معموره ی دل ها وطنی ساخته اند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون بسنجید به فرهاد مرا، یا مجنون</p></div>
<div class="m2"><p>که به بازیچه ی هر یک سخنی ساخته اند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای برهمن بنگر معبد صوفی و ریا</p></div>
<div class="m2"><p>کاین طرف دیر بت و برهمنی ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شهید غم او بود که از شهر وجود</p></div>
<div class="m2"><p>آمد آواره که جای دهنی ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حله ها سوخته اند اهل بهشت از غیرت</p></div>
<div class="m2"><p>تا شهیدان تو گلگون کفنی ساخته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیر آن غمزه حلال است ولی جمعی را</p></div>
<div class="m2"><p>که ز دل جامه و از جان بدنی ساخته اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لذت شعر توعرفی به همه عالم گفت</p></div>
<div class="m2"><p>که ترا مایل شیرین دهنی ساخته اند</p></div></div>