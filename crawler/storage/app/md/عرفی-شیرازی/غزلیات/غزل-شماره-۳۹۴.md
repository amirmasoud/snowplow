---
title: >-
    غزل شمارهٔ ۳۹۴
---
# غزل شمارهٔ ۳۹۴

<div class="b" id="bn1"><div class="m1"><p>تا برده‌ام به مدرسهٔ عشق رخت خویش</p></div>
<div class="m2"><p>دارم وظیفه از جگر لخت‌لخت خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخمور خامشی‌ام، فراموش کرده‌ایم</p></div>
<div class="m2"><p>هم عهدهای ساقی و هم روی سخت خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهی که ظلم را به میانجی عنان دهد</p></div>
<div class="m2"><p>تیغ عدوی ملک رساند به تخت خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهلت مجو که بیشتر از عهد غنچگی</p></div>
<div class="m2"><p>گل باز بسته بود ز شاخ درخت خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دولت این بود که به درویش داده‌اند</p></div>
<div class="m2"><p>باید گریستن، جم و کی را، به تخت خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی هنوز مدحت دون‌همتان مکن</p></div>
<div class="m2"><p>توفان چو تند شد تو مینداز رخت خویش</p></div></div>