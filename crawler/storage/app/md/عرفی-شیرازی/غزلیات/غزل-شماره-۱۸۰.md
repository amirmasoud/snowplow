---
title: >-
    غزل شمارهٔ ۱۸۰
---
# غزل شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>ما کسی را نشناسیم که غم نشناسد</p></div>
<div class="m2"><p>هست بیگانه مرا آن که الم نشناسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و آن غمزه که چون تیغ برآرد ز میان</p></div>
<div class="m2"><p>طایر بتکده و مرغ حرم نشناسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرم باد از صنمی، برهمنی را که اگر</p></div>
<div class="m2"><p>در حرم دیده گشاید به صنم، نشناسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا رب آن کس که کند تهمت شادی بر من</p></div>
<div class="m2"><p>تا ابد کام دلش لذت غم نشناسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با شهیدان شهادت که غم راز لبم</p></div>
<div class="m2"><p>زخم ما مرهم و الماس به هم نشناسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل عرفی بود آسوده ز هر بود و نبود</p></div>
<div class="m2"><p>دو جهانی که وجود است، عدم نشناسد</p></div></div>