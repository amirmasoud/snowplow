---
title: >-
    غزل شمارهٔ ۲۴۲
---
# غزل شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>آن را که مراد حال باشد</p></div>
<div class="m2"><p>کی رغبت قیل و قال باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جرعه که دُرد شکوه دارد</p></div>
<div class="m2"><p>در ساغر من زلال باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شغل غمی که گفتنی نیست</p></div>
<div class="m2"><p>گویم به تو گر محال باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر نفس که در بهشت بینم</p></div>
<div class="m2"><p>در کارگه خیال باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقشی که نظاره بر نتابد</p></div>
<div class="m2"><p>می جویم و آن وصال باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون کینه ز طبع دوستانت</p></div>
<div class="m2"><p>مهر از دل او محال باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر تو که عید زندگانیست</p></div>
<div class="m2"><p>آرایش ماه و سال باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی گله کرده ای ز جورم</p></div>
<div class="m2"><p>بهتان چنین ملال باشد</p></div></div>