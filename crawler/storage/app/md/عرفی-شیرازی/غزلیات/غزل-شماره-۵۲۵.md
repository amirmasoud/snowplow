---
title: >-
    غزل شمارهٔ ۵۲۵
---
# غزل شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>بوستان پژمرده گردد، از دل ناشاد من</p></div>
<div class="m2"><p>یاسمن را خنده بر لب سوزد از فریاد من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغبان عشق می گوید که خاکستر شود</p></div>
<div class="m2"><p>شانهٔ باد صبا در طرهٔ شمشاد من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم آیین مغان پر ذوق بر باز آمدن</p></div>
<div class="m2"><p>عشق گفت آیین مجنون من و فرهاد من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفر نی، اسلام نی، اسلام کفر آمیز نی</p></div>
<div class="m2"><p>حکمت ایزد ندانم چیست در ایجاد من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بت از هر ذره نشناسی و ماند مایه ای</p></div>
<div class="m2"><p>گر کنی ای برهمن گلگشت کفر آباد من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی از من گر ملولی سعی در خونم مکن</p></div>
<div class="m2"><p>سیل غم را التفاتی نیست با بنیاد من</p></div></div>