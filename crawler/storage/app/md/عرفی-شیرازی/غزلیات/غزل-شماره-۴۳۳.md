---
title: >-
    غزل شمارهٔ ۴۳۳
---
# غزل شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>از بس که روی گرم به هر سو گذاشتیم</p></div>
<div class="m2"><p>صد داغ شعله خیز در آن کو گذاشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شرم ناکسی نگشودیم دیده را</p></div>
<div class="m2"><p>الماس فتنه در ته پهلو گذاشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گوهری که دل ز تعلق گرفته بود</p></div>
<div class="m2"><p>در دامن کرشمهٔ دلجو گذاشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما بر فریب چشم غزالانه باختیم</p></div>
<div class="m2"><p>مجنون بازمانده به آهو گذاشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز در زیارت دار است امنیت</p></div>
<div class="m2"><p>آن سر که بر سر زانو گذاشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک باره کرد خوب-خرابی مزاج دل</p></div>
<div class="m2"><p>دست از عمارت دل بدخو گذاشتیم</p></div></div>