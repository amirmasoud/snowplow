---
title: >-
    غزل شمارهٔ ۱۱۶
---
# غزل شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>بر میان فتنه شوخی طرف دامانی شکست</p></div>
<div class="m2"><p>ترکتاز غمزه هر سو فوج ایمانی شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک حسن از شیوه خالی گشت تا گشتم خراب</p></div>
<div class="m2"><p>کافرستانی به هم زد تا مسلمانی شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر طالع می کنم با آن که از بابم فکند</p></div>
<div class="m2"><p>زان که هر خاری به پایم در گلسنانی شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر سلیمانی است و گر موری که در معنی گداست</p></div>
<div class="m2"><p>هر که دست از آبرو شست او لب نانی شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شید صوفی طالبان کعبه را گمراه کرد</p></div>
<div class="m2"><p>نو مسلمانی در آمد فوج ایمانی شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که با آن نامسلمان یک زمان همراه شد</p></div>
<div class="m2"><p>با خدای خویش در هر گام پیمانی شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قابل رنج محبت کس نیاید در وجود </p></div>
<div class="m2"><p>رنگ روی خویش را هر کس به دستانی شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دل عرفی شکست آشوب در عالم فتاد</p></div>
<div class="m2"><p>این نه موری بود، پنداری سلیمانی شکست</p></div></div>