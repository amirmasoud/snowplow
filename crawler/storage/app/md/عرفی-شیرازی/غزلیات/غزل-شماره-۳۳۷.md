---
title: >-
    غزل شمارهٔ ۳۳۷
---
# غزل شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>گَرَم دعای مَلَک خاک رهگذر باشد</p></div>
<div class="m2"><p>به هر کجا نهم پا نیشتر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آفتاب طلب گشت بخت ما همه عمر</p></div>
<div class="m2"><p>نیافت سایهٔ نخلی که بارور باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امید عافیت از مردن است و می ترسم</p></div>
<div class="m2"><p>که مرگ دیگر و آسودگی دگر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بال خویش منال ای هما، به گلشن عشق</p></div>
<div class="m2"><p>در این چمن، قفس مرغ بال و پر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده بشارت طوبی که مرغ همت ما</p></div>
<div class="m2"><p>بر آن درخت ننشیند که بی ثمر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به آتش جگرتشنگان نگردد خشک</p></div>
<div class="m2"><p>ز آب دیدهٔ ما، دامنی که تر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تمام آتشم و نالهٔ بی اثر، عرفی</p></div>
<div class="m2"><p>فغان که دوزخیان را کجا اثر باشد</p></div></div>