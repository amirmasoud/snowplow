---
title: >-
    غزل شمارهٔ ۵۰۶
---
# غزل شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>بردیم ز کویش، دم سردی و گذشتیم</p></div>
<div class="m2"><p>سودیم بر آن در، رخ زردی و گذشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران بستادند که این جلوه گه کیست</p></div>
<div class="m2"><p>ما سرمه گرفتیم ز گردی و گذشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گه که ره ما به یکی راه رو افتاد</p></div>
<div class="m2"><p>دیدیم چو خود، یکی بیهده گردی و گذشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون باد صبا، روی به هر سو که نهادیم</p></div>
<div class="m2"><p>چیدیم غبار ره مردی و گذشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن در که پای دل ما داشت به زنجیر</p></div>
<div class="m2"><p>گفتیم به دیوانهٔ فردی و گذشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گه که گذار من و عرفی به هم افتاد</p></div>
<div class="m2"><p>دادیم به هم تحفهٔ دردی و گذشتیم</p></div></div>