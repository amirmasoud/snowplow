---
title: >-
    غزل شمارهٔ ۳۰۷
---
# غزل شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>چو مرغ سدره که در آشیان بیاساید</p></div>
<div class="m2"><p>به چین زلف تو جان بیاساید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برانم از در یار، ای ادب، که یک چندی</p></div>
<div class="m2"><p>ز ننگ بوسه ام آن آستان بیاساید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رشک حوصله ام آسمان بود دلگیر</p></div>
<div class="m2"><p>کرشمه ای که دل آسمان بیاساید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن هلاک به بازیچه ام، بزن زخمی</p></div>
<div class="m2"><p>که خون چکان لبم از الامان بیاساید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبر به باغ، ببر سوی گلخنم، کانجا</p></div>
<div class="m2"><p>ز بوی سوختگی مغز جان بیاساید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلش که مانده شود آسمان، در آزارم</p></div>
<div class="m2"><p>هزار سال پس از من جهان بیاساید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان به ماتم دل در غمت کنم شیون</p></div>
<div class="m2"><p>که کشته گان غمت را روان بیاساید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان که تلخ سرشتند، پیکرم، عرفی</p></div>
<div class="m2"><p>نشد که زاغی از این استخوان بیاساید</p></div></div>