---
title: >-
    غزل شمارهٔ ۴۱۶
---
# غزل شمارهٔ ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>باز این منم به صد دل خشنود در سماع</p></div>
<div class="m2"><p>دیوانه وش ز نغمهٔ داود در سماع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویم به روی دلبر و قوال در سرود</p></div>
<div class="m2"><p>دستم به دست شاهد مقصود در سماع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرهیز ای فرشته که اینک به عرش و فرش</p></div>
<div class="m2"><p>افشاندم آستین می آلوده در سماع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز این چه سوزش است که خونابه ریز شد</p></div>
<div class="m2"><p>چندین هزار زخم نمک سود در سماع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنگام مردن است، تپیدن بسی به خون</p></div>
<div class="m2"><p>دایم چو بی غمان نتوان بود در سماع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد که بود زمزمه دشمن به دیر عشق</p></div>
<div class="m2"><p>آمد به نیم زمزمهٔ عود در سماع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی سرود بزم که یادش آمد که باز</p></div>
<div class="m2"><p>بر روی آتش آمده چون عود در سماع</p></div></div>