---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>بیدلی کو از او پرسم دل آواره چیست</p></div>
<div class="m2"><p>از مزاج دل تفاوت تا به سنگ خاره چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهد پیش از خاطرم شد، عشق گویا بنگرم</p></div>
<div class="m2"><p>بی وفایی های بخت و شوخی سیاره چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چاره ای آخر ضرور است از پی تحصیل درد</p></div>
<div class="m2"><p>من ندانم هر که می داند بگوید چاره چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که می دزدد نزاکت نام مرهم از تنش</p></div>
<div class="m2"><p>کی شناسد شکر زخم غمزه ی خونخواره چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن که چین آستین ها را برابر می کند</p></div>
<div class="m2"><p>چون بداند ذوق چاک دامن صد پاره چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی این ها با که گویی عشق می بازد ز تو</p></div>
<div class="m2"><p>زود خواهی گفت کاین بیهوده ی کفاره چیست</p></div></div>