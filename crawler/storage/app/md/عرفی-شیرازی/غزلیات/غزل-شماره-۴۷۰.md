---
title: >-
    غزل شمارهٔ ۴۷۰
---
# غزل شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>زخمی شوق توام، سینهٔ جوشان دارم</p></div>
<div class="m2"><p>خانه در کوچهٔ الماس فروشان دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی مسلمان کندم صحبت اصحاب کرم</p></div>
<div class="m2"><p>که در آن زمره بسی حلقه به گوشان دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش پنبهٔ گوش دگرانم کامروز</p></div>
<div class="m2"><p>گوش را مزرعهٔ پنبه فروشان دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت عمر فرومایه ملولم دارد</p></div>
<div class="m2"><p>میل همدوشی تابوت به دوشان دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظا در گذر از قافلهٔ من که متاع</p></div>
<div class="m2"><p>همه گوش است ولی نذر خموشان دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی امروز به کاشانهٔ من باش که باز</p></div>
<div class="m2"><p>گله ای از دل بی شرم خروشان دارم</p></div></div>