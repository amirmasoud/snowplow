---
title: >-
    غزل شمارهٔ ۲۰۵
---
# غزل شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>چه فتنه در دل آن عشوه ساز می گذرد؟</p></div>
<div class="m2"><p>که گرم روی بر اهل نیاز می گذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دراین غمم که مبادا بگیرمش به ضمیر</p></div>
<div class="m2"><p>چو حرف اهل دل امتیاز می گذرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دل گذشتی و با آن که عمرها بگذشت</p></div>
<div class="m2"><p>هنوز دل ز بر جان به ناز می گذرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شهر عشق بنازم که ساکنانش را</p></div>
<div class="m2"><p>تمام عمر به عجز و نیاز می گذرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیرتم که ز ما غیر رنگ می یابند</p></div>
<div class="m2"><p>گهی که در دلم آن دلنواز می گذرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سزد ز غیرت اگر مانعم شوی، آن راز</p></div>
<div class="m2"><p>که در میان من و دل چه راز می گذرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراب حالی دل ها ببین که آن مغرور</p></div>
<div class="m2"><p>به عهد حسن جوانی و ناز می گذرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عنان دل و دین من ز کف رود، عرفی</p></div>
<div class="m2"><p>که آن کرشمه به این ترکتاز می گذرد</p></div></div>