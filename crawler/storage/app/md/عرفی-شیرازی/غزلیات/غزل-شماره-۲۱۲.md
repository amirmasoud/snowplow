---
title: >-
    غزل شمارهٔ ۲۱۲
---
# غزل شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>مستان عشق خانه در آتش گرفته اند</p></div>
<div class="m2"><p>دائم قدح ز خوی تو آتش گرفته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این هم عنایتی است که غم های روزگار</p></div>
<div class="m2"><p>دنبال بی کسان مشوش گرفته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خم به ته، ز چاه بلا، دُرد سرکشند</p></div>
<div class="m2"><p>آنان که خو به بادهٔ بی غش گرفته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینک ره گریز، چه سود از گریختن</p></div>
<div class="m2"><p>سر تاسر زمانه در آتش گرفته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی مرید خلوتیان پیاده شو</p></div>
<div class="m2"><p>کاین قوم جلوه ز ابرش گرفته اند</p></div></div>