---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>امید عیش کجا و دل خراب کجا</p></div>
<div class="m2"><p>هوای باغ کجا، طائر کباب کجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به می نشاط جوانی به دست نتوان کرد</p></div>
<div class="m2"><p>سرور باده کجا، نشئهٔ شباب کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ذوق کلبهٔ رندان کجاست خلوت شیخ</p></div>
<div class="m2"><p>حریم کعبهٔ خلوت کجا، شراب کجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلای دیده و دل را ز پی شتابانم</p></div>
<div class="m2"><p>کسی نگویدم ای خان‌ومان خراب کجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلندهمتی ذره داغ می‌کندم</p></div>
<div class="m2"><p>وگرنه ذره کجا، مهر آفتاب کجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای عشق ابد می‌سرود عرفی دوش</p></div>
<div class="m2"><p>کجاست مطرب و آهنگ این رباب کجا</p></div></div>