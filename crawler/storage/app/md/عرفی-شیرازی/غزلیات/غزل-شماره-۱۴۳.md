---
title: >-
    غزل شمارهٔ ۱۴۳
---
# غزل شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>دلم به زخم توان داد، بی تپیدن نیست</p></div>
<div class="m2"><p>که کشته ی تو نصیبش ز آرمیدن نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت و سوختم از انتظار و باز ندید</p></div>
<div class="m2"><p>درین دیار مگر رسم باز دیدن نیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز باغ وصل جه حاصل؟ دلا تصور کن</p></div>
<div class="m2"><p>که میوه بر سر شاخ است و دست چیندن نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تربتم بگذر ای مسیح دم ، زنهار</p></div>
<div class="m2"><p>کزین زیاده مرا تاب آرمیدن نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم کباب شد ز غصه ی غمت عرفی</p></div>
<div class="m2"><p>مگو مگو که مرا طافت شنیدن نیست</p></div></div>