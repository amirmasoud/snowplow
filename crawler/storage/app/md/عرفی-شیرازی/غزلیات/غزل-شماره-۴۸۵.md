---
title: >-
    غزل شمارهٔ ۴۸۵
---
# غزل شمارهٔ ۴۸۵

<div class="b" id="bn1"><div class="m1"><p>تا کی دهم به دست تماشا زمام چشم</p></div>
<div class="m2"><p>فالی زنم که گریه بر آید بنام چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گریه بی مضایقه از در درآ که من</p></div>
<div class="m2"><p>هر دم به خون دل بنویسم سلام چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که حیرت آمد و بیگانگی فزود</p></div>
<div class="m2"><p>امشب خیال دوست نگردید رام چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد نوحه هست بر لب و نسپرده راه گوش</p></div>
<div class="m2"><p>صد گریه هست در دل و نشنیده نام چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی فسرده چون نبود مجلسم که باز</p></div>
<div class="m2"><p>خالی است شیشهٔ دل و خشک است جام چشم</p></div></div>