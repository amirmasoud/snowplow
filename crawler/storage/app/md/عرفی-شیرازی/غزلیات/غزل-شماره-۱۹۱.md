---
title: >-
    غزل شمارهٔ ۱۹۱
---
# غزل شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>دلی چو مشعل حسن تو فرد می خیزد</p></div>
<div class="m2"><p>که چون فغان من از درد می خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه مرد بادهٔ عشقی ، وکر نه در طلبت</p></div>
<div class="m2"><p>فغان ز جوش خم لاجورد می خیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبین به عجز زلیخا، مصاف عشق است این</p></div>
<div class="m2"><p>که گرد فتنه ز بنیاد مرد می خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بزم کعبه روان کم نشین، کزان مجمع</p></div>
<div class="m2"><p>همیشه مردم بیهوده گرد می خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر فسانه شمارم وگر ترانه زنم</p></div>
<div class="m2"><p>توگوش دار که از روی درد می خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهید مضطربی خاک شد مگر به رهت</p></div>
<div class="m2"><p>که بی نسیم ز راه تو گرد می خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترانه ای بشنو ، کز هزار نغمه تراز</p></div>
<div class="m2"><p>یکی چو عرفی دستان نورد می خیزد</p></div></div>