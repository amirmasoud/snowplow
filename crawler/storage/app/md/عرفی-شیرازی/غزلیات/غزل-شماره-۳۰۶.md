---
title: >-
    غزل شمارهٔ ۳۰۶
---
# غزل شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>باز شاهین امیدم اوج پروازی کند</p></div>
<div class="m2"><p>لیک شوقم در هوای وصل شهبازی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نشانی هست در راه، از سم گلگون فیض</p></div>
<div class="m2"><p>بانگ بر شبدیز جان زن که سبکبازی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با هوسناکان نفاق آمیز دارم صحبتی</p></div>
<div class="m2"><p>عندلیب قدس با زاغان هم آوازی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دین اگر این است که این جمع پرشان را بود</p></div>
<div class="m2"><p>برهمن بر اهل دل شاید که طنازی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز عشق از این تراوش می کند، از من مرنج</p></div>
<div class="m2"><p>گر بود روح الامین محرم، که غمازی کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت بیگانه بندد، دست شوخی های عشق</p></div>
<div class="m2"><p>عشق را در پرده بر، تا با دلت بازی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فوج شادی را به خون افکنده است، دیگر دل کجاست</p></div>
<div class="m2"><p>کآفرین بر دست و تیغ عرفی غازی کند</p></div></div>