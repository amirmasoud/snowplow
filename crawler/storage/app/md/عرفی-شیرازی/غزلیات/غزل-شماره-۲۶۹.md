---
title: >-
    غزل شمارهٔ ۲۶۹
---
# غزل شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>زاهد بتکدهٔ عشق هراسان نرود</p></div>
<div class="m2"><p>دامن دل بکشد، از پی ایمان نرود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهر دل خاصهٔ سلطان محبت گردید</p></div>
<div class="m2"><p>بعد از آن عاقل تدبیر به دیوان نرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده دار تو اگر مژدهٔ دیدار دهد</p></div>
<div class="m2"><p>صد قیامت شود و کس در رضوان نرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پا منه بر سر بالین اسیران ، گاهی</p></div>
<div class="m2"><p>هیچ بیدرد نیاید که پریشان نرود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بروم بر دم خنجر که با آن بی باکی</p></div>
<div class="m2"><p>سایهٔ مرغ هما بر گل و ریحان نرود</p></div></div>