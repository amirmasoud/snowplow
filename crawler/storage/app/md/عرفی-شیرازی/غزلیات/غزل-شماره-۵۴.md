---
title: >-
    غزل شمارهٔ ۵۴
---
# غزل شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>از آن ز شربت صلحم هوای پرهیز است</p></div>
<div class="m2"><p>که آتش تب شوقم نه آن چنان تیز است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو زلف باز کنی ناله خیزد از دل ها</p></div>
<div class="m2"><p>که دام ما همه این طره ی دل آویز است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز طره مشک به دامان کوهکن پاشد</p></div>
<div class="m2"><p>اگر چه تکیه ی شیرین به دوش پرویز است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سمند سعی چه بیهوده رانی ای فرهاد</p></div>
<div class="m2"><p>که هم عنانی گردون نصیب شبدیز است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چگونه مانع نظاره ام شوی که مرا</p></div>
<div class="m2"><p>ز شوق روی تو سر تا قدم نگه خیز است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستزه باخت به میدان امتحان عرفی</p></div>
<div class="m2"><p>عنان کشیده چه داری، محل مهمیز است</p></div></div>