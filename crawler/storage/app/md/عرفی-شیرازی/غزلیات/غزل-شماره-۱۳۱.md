---
title: >-
    غزل شمارهٔ ۱۳۱
---
# غزل شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>مست آمدم به معرکه، آیین کارچیست</p></div>
<div class="m2"><p>دشمن کدام و مطلب از کار و بار چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خار و گل ز شاخچه ی عدل می دهد</p></div>
<div class="m2"><p>این عین تازه رویی و این شرمسار چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم زهر چشم و هم نگه از باب خوبی است</p></div>
<div class="m2"><p>پس دم مزن که این خوش و آن ناگوار چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم نعمتی همی خورد، اما ز خوان عشق</p></div>
<div class="m2"><p>ای اهل روزگار غم روزگار چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندیشه در حریم وصال است منتظر</p></div>
<div class="m2"><p>معشوق چون شناخته است انتظار چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو راز خود نهفته بهشتی ز راز دار</p></div>
<div class="m2"><p>امید پرده پوشی ات از راز دار چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظم جهان چو بوقلمون است و ریو و رنگ</p></div>
<div class="m2"><p>پس عیب زاهدان مشعبد شعار چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاد در میانه ی گرداب کشتی ام</p></div>
<div class="m2"><p>من رسته ام بگو رغم اهل کنار چیست</p></div></div>