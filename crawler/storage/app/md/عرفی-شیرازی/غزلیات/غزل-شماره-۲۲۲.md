---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>عاقلان آدابت آموزند و رسوایت کنند</p></div>
<div class="m2"><p>دامن جمعی به دست آور که شیدایت کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگهان عشقت کدازند، از حجاب ناکسی</p></div>
<div class="m2"><p>پرده بگشا تا ز نادانی تمنایت کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ گل پژمرده کردی، رو ز کس در هم مکش</p></div>
<div class="m2"><p>من هم از غیرت گذشتم، گو تماشایت کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس به کویی جلوه کن، بر مستحقان، زینهاد</p></div>
<div class="m2"><p>تا دعایی بهر حسن عالم آرایت کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی ار مانی قدم در وادی اهل وجود</p></div>
<div class="m2"><p>صد بیابا ن خار خذلان تحفهٔ پایت کنند</p></div></div>