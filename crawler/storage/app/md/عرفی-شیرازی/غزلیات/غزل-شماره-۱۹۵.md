---
title: >-
    غزل شمارهٔ ۱۹۵
---
# غزل شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>آه ازین دل کز گریبان غمی سر بر نزد</p></div>
<div class="m2"><p>صد مصیبت رفت و دست شیونی بر سر نزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وجود آن که زهر بی غمی نوشیده ام</p></div>
<div class="m2"><p>زهر خندی بر مزاج عافیت پرور نزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با چنین غوغا که در این بزم شورانگیز بود</p></div>
<div class="m2"><p>شیشه ای نشکست و سنگی بر سر ساغر نزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چنین بزمی که یک پروانه دارد صد چراغ</p></div>
<div class="m2"><p>با همه پروانگی گرد چراغی پر نزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وقت عرفی خوش که چون نگشودند در بر رخش</p></div>
<div class="m2"><p>بر در نگشوده ساکن شد، در دیگر نزد</p></div></div>