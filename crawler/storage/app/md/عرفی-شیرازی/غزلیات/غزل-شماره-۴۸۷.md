---
title: >-
    غزل شمارهٔ ۴۸۷
---
# غزل شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>هر چند بی غمانه به مسکن فتاده ایم</p></div>
<div class="m2"><p>زنجیر صد کرشمه به گردن فتاده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نعمت اوفتاده و شکری نمی کنیم</p></div>
<div class="m2"><p>بس ناشکفته گل در گلشن فتاده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشدل به نور شمع شبستنانت از برون</p></div>
<div class="m2"><p>شب ها به خاک دیده به روزن فتاده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد حریم دیرم و در دیده ام کشند</p></div>
<div class="m2"><p>تا از کدام گوشهٔ دامن فتاده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از قسمت ازل نکنی شکوه، هان ، خموش</p></div>
<div class="m2"><p>ما شاخ طوبی ایم که به گلخن فتاده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفکن به خاکم ار ثمر نارسم، به خشم</p></div>
<div class="m2"><p>کز شاخ نخل وادی ایمن فتاده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم عیش عرفی اگر روز ساکنم</p></div>
<div class="m2"><p>شب تا سحر به حلقهٔ شیون فتاده ایم</p></div></div>