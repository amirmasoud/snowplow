---
title: >-
    غزل شمارهٔ ۱۶۸
---
# غزل شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>بندهٔ دل شوم که او، خون فراغ می‌خورد</p></div>
<div class="m2"><p>خدمت درد می‌کند، نعمت داغ می‌خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوبی و خلد عافیت، می‌نخرم به مشت خس</p></div>
<div class="m2"><p>زان که تَذَروِ این چمن، طمعهٔ زاغ می‌خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چمنی نمی‌برد، نعمت برگزیده را</p></div>
<div class="m2"><p>آن که وظیفهٔ ثمر، از همه باغ می‌خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌ادبی است موسی‌ام، ره بدهی به طور خود</p></div>
<div class="m2"><p>کو لب شعله می‌گزد، شمع و چراغ می‌خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چمن محبت است، الحذر ای بهشتیان</p></div>
<div class="m2"><p>بوی گل بهشت ما، مغز دماغ می‌خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی تشنه را ز من، مژده که گر نایستد</p></div>
<div class="m2"><p>آب حیات از کف، خضر سراغ می‌خورد</p></div></div>