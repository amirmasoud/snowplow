---
title: >-
    غزل شمارهٔ ۱۰۲
---
# غزل شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>مدار صحبت ما بر حدیث زیر لبی است</p></div>
<div class="m2"><p>که اهل هوش عوام اند و گفتگو عربی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبول خاطر معشوق شرط دیدار است</p></div>
<div class="m2"><p>به حکم شوق تماشا مکن که بی ادبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکاح دختر رز بود دوش با عرفی</p></div>
<div class="m2"><p>هنوز قاضی شهرش در رطبی است</p></div></div>