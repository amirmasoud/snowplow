---
title: >-
    غزل شمارهٔ ۵۲
---
# غزل شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>دورم از کوی تو، جا در زیر خاکم بهتر است</p></div>
<div class="m2"><p>زندگی تلخ است با حرمان، هلاکم بهتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من که مجروح خمارم مرهم راحت چه سود</p></div>
<div class="m2"><p>جای مرهم بر جراحت برگ تاکم بهتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بکشتی از فراقم، سوختی، منت منه</p></div>
<div class="m2"><p>من که در دوزخ به زندان، هلاکم بهتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره به امیدم مده عرفی که بی باکم بسی</p></div>
<div class="m2"><p>من صلاح خویش دانم، ترسناکم بهتر است</p></div></div>