---
title: >-
    غزل شمارهٔ ۳۴۰
---
# غزل شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>حیف است که دستی به نمکدان تو یابند</p></div>
<div class="m2"><p>زاغان هوس را، مگس خوان تو یابند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای گل ز صبا راه بگردان که مبادا</p></div>
<div class="m2"><p>مرغان به نسیمش ره بستان تو یابند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باید که رسد جان به لب خضر و مسیحا</p></div>
<div class="m2"><p>تا قطرهٔ از چشمهٔ حیوان تو یابند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن فتنه که در خون کشد آشوب قیامت</p></div>
<div class="m2"><p>در سلسلهٔ زلف پریشان تو یابند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شعر تو عرفی نگزینند؟ که عالی است</p></div>
<div class="m2"><p>هر بیت که در صفحهٔ دیوان تو یابند</p></div></div>