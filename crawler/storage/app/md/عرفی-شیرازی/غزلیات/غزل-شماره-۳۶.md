---
title: >-
    غزل شمارهٔ ۳۶
---
# غزل شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>گشود برقع و توفان حسن عالم سوخت</p></div>
<div class="m2"><p>متاع شادی و غم جمع بود در هم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که زد به داغ دلم دامن کرشمه که باز</p></div>
<div class="m2"><p>به نیم شعله هم خان و مان مرهم سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروغ حسن تو در گلشن بهشت افتاد</p></div>
<div class="m2"><p>که برگ لاله و گل در میان شبنم سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به العطش مگشا لب که خضر وادی عشق</p></div>
<div class="m2"><p>گلوی تشنه به آب حیات و زمزم سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز آب ساقی عشقم که جام جرعه ی او</p></div>
<div class="m2"><p>کلیم را کف دست و مسیح را دم سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم به گوشه نشینان عشق می لرزد</p></div>
<div class="m2"><p>که حسن او گل شوخی بچید و عالم سوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به لوح مشهد پروانه این رقم دیدم</p></div>
<div class="m2"><p>که آتشی که مرا سوخت خویش را هم سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشم که سوخت دو کون از غمت وزین خوشتر</p></div>
<div class="m2"><p>که کس به داغ دل عرفی از غمت کم سوخت</p></div></div>