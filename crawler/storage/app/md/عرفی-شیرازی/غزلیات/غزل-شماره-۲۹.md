---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>نداد نور شراری چراغ هستی ما</p></div>
<div class="m2"><p>گلی نچید ز شاخ، دراز دستی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنایت صمدی رد کفر ما نکند</p></div>
<div class="m2"><p>اگر کمال به دیر و صنم پرستی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر فتادگی تا به عرش می ساید</p></div>
<div class="m2"><p>کلاه فخر بلندی ربود پستی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز نیم مستی ما زآن کرشمه می‌بارد</p></div>
<div class="m2"><p>که چشم شاهد عشق است نیم مستی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی که عشق بتازد به قلب ما عرفی</p></div>
<div class="m2"><p>به تاق عرش نشیند غبار هستی ما</p></div></div>