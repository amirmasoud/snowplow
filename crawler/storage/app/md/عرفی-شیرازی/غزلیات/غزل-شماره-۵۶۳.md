---
title: >-
    غزل شمارهٔ ۵۶۳
---
# غزل شمارهٔ ۵۶۳

<div class="b" id="bn1"><div class="m1"><p>تا خون نخوری چاشنی درد ندانی</p></div>
<div class="m2"><p>تا دل ندهی آن چه به من کرد ندانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بوی گلی نشنوی و کم نکنی ناز</p></div>
<div class="m2"><p>آشفتگی باد چمن گرد ندانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا سر نشود خاک به جولانگه معشوق</p></div>
<div class="m2"><p>بر سرمه مقدم شدنی گرد ندانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق غم معشوق به بازی نتوان یافت</p></div>
<div class="m2"><p>بر خیز که منصوبه از این نرد ندانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نوشم و گلگون شوم و بیهده خندم</p></div>
<div class="m2"><p>تا از غم دنیا رخ من زرد ندانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای آن که به درد دل عرفی جگرت سوخت</p></div>
<div class="m2"><p>امید که حال دل بی درد ندانی</p></div></div>