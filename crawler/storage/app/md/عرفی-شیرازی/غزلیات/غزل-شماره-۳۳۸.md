---
title: >-
    غزل شمارهٔ ۳۳۸
---
# غزل شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>بگو که نغمه سرایان عشق خاموشند</p></div>
<div class="m2"><p>که نغمه نازک و اصحاب پنبه در گوشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکست شیشه و در پا خلید و بی خبران</p></div>
<div class="m2"><p>هنوز میکده آشوب و عافیت کوشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر ز دیر برندت به طوف کعبه، مباد</p></div>
<div class="m2"><p>امید و یاس در این کوچه دوش بر دوش اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار شیشه تهی گشت و تنگ حوصله گان</p></div>
<div class="m2"><p>هنوز بی خبر از ته پیالهٔ دوشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه محنت آورد آن جمع را به ناله که تو</p></div>
<div class="m2"><p>به ریشهٔ دلشان می خلی و خاموشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغان ز عادت عرفی که تا تو دشمن جان</p></div>
<div class="m2"><p>رهش زدی، ز دلش دوستان فراموشند</p></div></div>