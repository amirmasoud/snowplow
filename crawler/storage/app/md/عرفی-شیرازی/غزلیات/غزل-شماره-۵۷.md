---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>تا روی دل فروز تو بستان آتش است</p></div>
<div class="m2"><p>دل نغمه سنج گلستان آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه آتشی تو که چندین هزار داغ</p></div>
<div class="m2"><p>از شعله ی جمال تو در جان آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر مست حیرتیم ز روی تو دور نیست</p></div>
<div class="m2"><p>آتش پرست واله و حیران آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسرده را نصیب نباشد دل کباب</p></div>
<div class="m2"><p>آن یابد این نواله که مهمان آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای طائر بهشت ز باغ دلم حذر</p></div>
<div class="m2"><p>کاین لاله زار داغ گلستان آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خون شهید عشق جهان را فرو گرفت</p></div>
<div class="m2"><p>کشتی مساز نوح که طوفان آتش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستم به محفلی که در او آتش جحیم</p></div>
<div class="m2"><p>ته جرعه ای ز ساغر مستان آتش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاد دامن دل عرفی به دست عشق</p></div>
<div class="m2"><p>یعنی که دست شعله به دامان آتش است</p></div></div>