---
title: >-
    غزل شمارهٔ ۴۹۹
---
# غزل شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>ساغر ز دست مردم آزاده چون کشیم</p></div>
<div class="m2"><p>لبریز گشته ایم ز خون، باده چون کشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما روی گرم را، دل و جان وقف کرده ایم</p></div>
<div class="m2"><p>این تحفه پیش ابروی نکشاده چون کشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را نداده اند و عنانش به دست اوست</p></div>
<div class="m2"><p>ما از کفش عنان دلِ داده چون کشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را بود معامله با عالم قدیم</p></div>
<div class="m2"><p>منت از این جهان عدم زاده چون کشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما مرد دستگیر کسی نیستیم، لیک</p></div>
<div class="m2"><p>دامن ز دست مردم آزاده چون کشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منزل دراز و طبع جوانمرد و وقت کم</p></div>
<div class="m2"><p>دست از میان دشمن استاده چون کشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل را عنان گرفته صنم می کشد به دیر</p></div>
<div class="m2"><p>او را به وعظ بر سر سجاده چون کشیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دست پیر منت سجاده لازم است</p></div>
<div class="m2"><p>این نقش بر جبین دل ساده چون کشیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عرفی بهشت نسیه و بزم وصال نقد</p></div>
<div class="m2"><p>دست از عنان دولت آزاده چون کشیم</p></div></div>