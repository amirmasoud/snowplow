---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>درد نایافت ز بی دردی اقبال من است</p></div>
<div class="m2"><p>ور نه مقصود من افتاده به دنبال من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با قضا سینه ی من صاف نگردد هرگز</p></div>
<div class="m2"><p>شکوه ی من همه از جانب اهمال من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز از محنت ایام نبودم آزاد</p></div>
<div class="m2"><p>فتنه همزاد من و حادثه هم سال من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آستینی که دو عالم بت و زنار در اوست</p></div>
<div class="m2"><p>گر به معنی نگری نامه ی اعمال من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی اصلاح پریشانی ام از یاد ببر</p></div>
<div class="m2"><p>کانچه ادبار بود پیش من، اقبال من است</p></div></div>