---
title: >-
    غزل شمارهٔ ۹۹
---
# غزل شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>هم صومعه را فیض به دستور نمانده است</p></div>
<div class="m2"><p>هم گوشه ی آتشکده را نور نمانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نشأ ذوقی نبود خفته و بیدار</p></div>
<div class="m2"><p>در صومعه و میکده مخمور نمانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیمار تو کش زندگی از شدت درد است</p></div>
<div class="m2"><p>امید هلاکش به دم صور نمانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باور نکنم گر چه اناالحق زده از عشق</p></div>
<div class="m2"><p>صد راز دگر در دل زنجور نمانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نام تو، چه پست و چه بلندش، چه مراد است</p></div>
<div class="m2"><p>بس شهره ی آفاق که مشهور نمانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی ارنی گو شنوای است، نه موسی</p></div>
<div class="m2"><p>دیر است که این قاعده در طور نمانده است</p></div></div>