---
title: >-
    غزل شمارهٔ ۶۸
---
# غزل شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>هزار حسن عبادت نه زشتی عمل است</p></div>
<div class="m2"><p>متاع من دل مجذوب و مستی ازل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی است نقد حکیمان و حسن نادانان</p></div>
<div class="m2"><p>هر آن چه در کتب حکمت است در مثل است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که گشته به تقلید آدمی سیر ت</p></div>
<div class="m2"><p> نه آدمی است، همان باز، آدمی بدل است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جنگ زاهد و صوفی خوشم به گلشن او</p></div>
<div class="m2"><p>میان بلبل و زاغ چمن همان جدل است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من از حدوث و قدم خامشم، ولی گویم</p></div>
<div class="m2"><p>نظیز عدت آینده عهد ماازل است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصیده نظم هوس پیشگاه بود عرفی</p></div>
<div class="m2"><p>تو از قبیله ی عشقی وظیفه ات غزل است</p></div></div>