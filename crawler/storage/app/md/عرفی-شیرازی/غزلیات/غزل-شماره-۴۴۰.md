---
title: >-
    غزل شمارهٔ ۴۴۰
---
# غزل شمارهٔ ۴۴۰

<div class="b" id="bn1"><div class="m1"><p>هرگز دل کس را به گناهی نشکستیم</p></div>
<div class="m2"><p>وز بهر جزا طرف کلاهی نشکستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد نخل نشاندیم ولی گوشهٔ دستار</p></div>
<div class="m2"><p>از طرف چمن شاخ گیاهی نشکستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میکده بردیم دو صد شیشه به کعبه</p></div>
<div class="m2"><p>یک شیشه دلی بر سر راهی نشکستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد ره نشکستیم سر از سنگ جنون، لیک</p></div>
<div class="m2"><p>یک ره به غلط طرف کلاهی نشکستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز هوس روی تو نگذشته به خاطر</p></div>
<div class="m2"><p>کز هّم تو در دیده نگاهی نشکستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ره به کمال تو ندیدیم که در دل</p></div>
<div class="m2"><p>عرفی صفت از بیم تو آهی نشکستیم</p></div></div>