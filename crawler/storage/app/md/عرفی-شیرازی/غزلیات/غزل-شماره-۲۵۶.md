---
title: >-
    غزل شمارهٔ ۲۵۶
---
# غزل شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>نغمهٔ کز ره تاثیر به شیون نکشد</p></div>
<div class="m2"><p>به سماعش دل ماتم زدهٔ من نکشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیت قتل من اینست که در روز جزا</p></div>
<div class="m2"><p>نزنم دست به دامانش و دامن نکشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جذبهٔ قهر تو ای ذره ندانم تا کی</p></div>
<div class="m2"><p>از ته غمکدهٔ سینه به روزن نکشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت درد همین است که در فصل بهار</p></div>
<div class="m2"><p>دل مرغان خزان دیده به گلشن نکشد</p></div></div>