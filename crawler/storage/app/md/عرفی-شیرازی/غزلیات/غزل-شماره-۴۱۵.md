---
title: >-
    غزل شمارهٔ ۴۱۵
---
# غزل شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>اگر تو خنده کنی از گل و شراب چه حظ</p></div>
<div class="m2"><p>وگر تو زهر دهی تشنه را ز آب چه حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه سایهٔ حسن تو جویم از خورشید</p></div>
<div class="m2"><p>ز دشمنی شب و مهر آفتاب چه حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمالِ حسنِ درونِ جمال در جلوه است</p></div>
<div class="m2"><p>هزار سال نهفتنش در نقاب چه حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنان این دل صد جا شکسته را بگذار</p></div>
<div class="m2"><p>ستم نواز شها، بر ده خراب چه حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آسمان طلبیدم نشان راحت، گفت</p></div>
<div class="m2"><p>اگر سوال غلط باشد، از جواب چه حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تلافی غم شب می کنم به خواب صبوح</p></div>
<div class="m2"><p>وگر نه تلخی غم بشکند ز خواب چه حظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سبوی دُردکشان محتسب شکست، ولی</p></div>
<div class="m2"><p>اگر دلی نخراشد ز احتساب چه حظ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نشاط فارغ و اندوه عاشق است شراب</p></div>
<div class="m2"><p>اگر ملال نیفزاید از شراب چه حظ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگو که گوش به واعظ نمی کند عرفی</p></div>
<div class="m2"><p>ندیم میکده را از شب عذاب چه حظ</p></div></div>