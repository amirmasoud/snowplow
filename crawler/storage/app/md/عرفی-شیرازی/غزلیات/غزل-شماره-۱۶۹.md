---
title: >-
    غزل شمارهٔ ۱۶۹
---
# غزل شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>حرم پویان دری را می‌پرستند</p></div>
<div class="m2"><p>فقیهان دفتری را می‌پرستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گروهی زشت‌خویند اهل دانش</p></div>
<div class="m2"><p>که زیب و زیوری را می‌پرستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن دعوی به شیخ و برهمن ماند</p></div>
<div class="m2"><p>که هر یک داوری را می‌پرستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برافکن پرده تا معلوم گردد</p></div>
<div class="m2"><p>که یاران دیگری را می‌پرستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجب داریم ما از اهل عصیان</p></div>
<div class="m2"><p>که دامان تری را می‌پرستند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر عزت که عشاق مجازی</p></div>
<div class="m2"><p>ز ما خود خوش‌تری را می‌پرستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اهل درد شو عرفی که این جمع</p></div>
<div class="m2"><p>گرامی‌گوهری را می‌پرستند</p></div></div>