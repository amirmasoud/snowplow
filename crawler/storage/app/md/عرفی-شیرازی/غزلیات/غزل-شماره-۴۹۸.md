---
title: >-
    غزل شمارهٔ ۴۹۸
---
# غزل شمارهٔ ۴۹۸

<div class="b" id="bn1"><div class="m1"><p>می فروشم راحت و عشق ستمگر می خرم</p></div>
<div class="m2"><p>می دهم روز خوش و آسیب اختر می خرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که باز افکنده ای در تیغ کاه رغبتم</p></div>
<div class="m2"><p>گر متاع غم بود بگشا که اکثر می خرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در سرشت من قبول شیوهٔ انکار نیست</p></div>
<div class="m2"><p>ساده لوحم هر جه بفروشند یک سر می خرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک جان تلخ کام است و شکر خواب عدم</p></div>
<div class="m2"><p>جام زهری می فشانم، تنگ شکر می خرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او به خونم گرم و من زین شادمان، کز شکر قتل</p></div>
<div class="m2"><p>صد ره از وی خون خود در روز محشر می خرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست غم کز درد هجران شهپرم بر خاک ریخت</p></div>
<div class="m2"><p>اینک از جبرییل شوقت باز شهپر می خرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر متاعی کز نگاهش می خرم در بزم وصل</p></div>
<div class="m2"><p>می نشینم گوشه ای در خود مکرر می خرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی آوردم متاعی، ترازو کو، غم کجاست</p></div>
<div class="m2"><p>آن متاعی کس مخرد، با جان برابر می خرم</p></div></div>