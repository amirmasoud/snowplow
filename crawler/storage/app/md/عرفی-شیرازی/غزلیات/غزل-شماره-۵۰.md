---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>ناله ام پرورش آموز نهال اثر است</p></div>
<div class="m2"><p>ور به دارت بنمایم که سراپا ثمر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله در سینه ی من، یک نفس آرامش نیست</p></div>
<div class="m2"><p>در دل خویش اثر کرد، چه کامل اثر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهبر بادیه ی عشق، تو را در هر گام</p></div>
<div class="m2"><p>نیستی پیشتر و عمر ابد بر اثر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرم دار، ای نمک، این زخم فریبی بگذار</p></div>
<div class="m2"><p>که دل و چشم من انباشته ی نیشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد بازارچه ی عشق بگردم، که در او</p></div>
<div class="m2"><p>عافیت سینه فروش و بلا دشنه گر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق را سینه ی سنگ و دل گرم است ضرور</p></div>
<div class="m2"><p>حسن نقشی است که هر لوحی از آن بهرور است</p></div></div>