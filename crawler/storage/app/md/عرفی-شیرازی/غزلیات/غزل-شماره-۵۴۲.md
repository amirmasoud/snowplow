---
title: >-
    غزل شمارهٔ ۵۴۲
---
# غزل شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>خیز و شراب حیرتم، زان قد جلوه ساز ده</p></div>
<div class="m2"><p>روی به روی عشق کن، دست به دست ناز ده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دل ساده گفتمت، نام وفا مبر کنون</p></div>
<div class="m2"><p>مرهم داغ خویش را، از نمک امتیاز ده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توسن ناز کرده زین، ای دل عافیت گزین</p></div>
<div class="m2"><p>موی به موی خویش را، مژدهٔ ترکتاز ده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی دو عروس را به هم، تاب مشارکت بود</p></div>
<div class="m2"><p>یا در مردمی بزن، یا سه طلاق آز ده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیوهٔ سامری بود، نیک کرشمه های تو</p></div>
<div class="m2"><p>یا به فدای عشوه کن، یا به زکات ناز ده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب از آن کرشمه ام، کاوش دل نصیب کن</p></div>
<div class="m2"><p>سینهٔ کبک زاده را، ناخن شاهباز ده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم زده عرفی از وفا، تا زنمش به امتحان</p></div>
<div class="m2"><p>دشنهٔ زهر داده ای، زان مژهٔ دراز ده</p></div></div>