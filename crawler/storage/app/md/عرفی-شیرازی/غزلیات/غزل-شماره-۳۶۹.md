---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>به لب آرام گیر ای جان غمگین یک دمی دیگر</p></div>
<div class="m2"><p>که شاید در حریم سینه بفریبد غمی دیگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گردم تنگدل شرح غمت هم با غمت گویم</p></div>
<div class="m2"><p>که در شرع محبت کفر باشد محرمی دیگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم از غم تنگدل گشتم هم از شادی، که را خوانم</p></div>
<div class="m2"><p>که بنماید دلم را ره به سوی عالمی دیگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی گردد عرقناک از حیا، گاهی ز می، هر دم</p></div>
<div class="m2"><p>گلستان جمالش تازه دارد شبنمی دیگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهید غمزهٔ او نیستم، حسرت به تیغم زد</p></div>
<div class="m2"><p>بهل ای همدم این شیون، به پا کن ماتمی دیگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدم چون رنجه فرمودی به بالینم، مرو در دم</p></div>
<div class="m2"><p>به غایت مشرفم بر مرگ، بنشین یک دمی دیگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشو ایمن گرت بر مسند جم دهر بنشاند</p></div>
<div class="m2"><p>که هر دو روز گردد مسند آرای غمی دیگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کفن شویم به خون دیده، نی در چشمهٔ زمزم</p></div>
<div class="m2"><p>پرستار صنم را هست، عرفی، زمزمی دیگر</p></div></div>