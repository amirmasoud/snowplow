---
title: >-
    غزل شمارهٔ ۲۰۹
---
# غزل شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>تا به کی عمر به افسوس و جهالت برود</p></div>
<div class="m2"><p>نشأ باده به تاراج ملامت برود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت بد را خجل از پرسش باطل چه کنم</p></div>
<div class="m2"><p>بهتر آن است که عمرم به بطالت برود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد از کعبه عنان تافته می آید، لیک</p></div>
<div class="m2"><p>کاین طمع داشت که خضرش به دلالت برود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره رو کعبه که دیر است حوالتگاهش </p></div>
<div class="m2"><p>برود، لیک ز دنبال حوالت برود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جای رحم است برآن جوهری لعل طراز</p></div>
<div class="m2"><p>کش همه عمر به آرایش آلت برود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جانم ار مالک غم های محبت گردد</p></div>
<div class="m2"><p>من گدا گردم و نامش به دلالت برود</p></div></div>