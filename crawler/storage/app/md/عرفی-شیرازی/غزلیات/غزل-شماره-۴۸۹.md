---
title: >-
    غزل شمارهٔ ۴۸۹
---
# غزل شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>نشسته بر سر گنج به فقر مشهورم</p></div>
<div class="m2"><p>نهفته در ته دامن چراغ بی نورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسیح تا دم آخر فسون دمید و هنوز</p></div>
<div class="m2"><p>به صد جراحت روز نخست رنجورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان به خواهش دیدار رفته ام شب وصل</p></div>
<div class="m2"><p>که شوق هم به تقاضا ندیده در طورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گمان مبر که دلم را توان تسلی داد</p></div>
<div class="m2"><p>که نا رسیده تر از زخم های ناسورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن به صورت دیوار نسبتم، عرفی</p></div>
<div class="m2"><p>که من کتابهٔ محراب بیت معمورم</p></div></div>