---
title: >-
    غزل شمارهٔ ۴۳۸
---
# غزل شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>ز معموری به تنگم، جز دل ویران نمی خواهم</p></div>
<div class="m2"><p>چو سلطان محبت ملک آبادان نمی خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی تا کی پریشان_جنبش و سر در هوا باشد</p></div>
<div class="m2"><p>دگر یار جنونم، عقل سرگردان نمی خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه داغ تازه می خارد نه زخم کهنه می کاود</p></div>
<div class="m2"><p>بده یا رب دلی، کاین صورت بی جان نمی خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تسکین دل غم دوستم، ناصح چه می گویی</p></div>
<div class="m2"><p>اگر شیون ندانی این زدن دستان نمی خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عالی دودمان عشقم، از راحت بود ننگم</p></div>
<div class="m2"><p>برهمن زادم و کیش مسلمانان نمی خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم گرم و خراش سینه را من دوست تر دارم</p></div>
<div class="m2"><p>بپوشان رخ که من جان کندن آسان نمی خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آب خضر نوشم بایدم از عشق فرمانی</p></div>
<div class="m2"><p>اگر خونم دهی می نوشم و فرمان نمی خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میفشان نشتر الماس بر داغ دلم، عرفی</p></div>
<div class="m2"><p>تهی دستم به سر جمعیت و سامان نمی خواهم</p></div></div>