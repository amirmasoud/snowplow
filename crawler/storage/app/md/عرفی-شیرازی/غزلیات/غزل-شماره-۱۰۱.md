---
title: >-
    غزل شمارهٔ ۱۰۱
---
# غزل شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>تا چشم عشوه ساز تو مهمان فتنه است</p></div>
<div class="m2"><p>شیرین تبسمت نمک خوان فتنه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب چه فتنه ای که به عهد تو روزگار</p></div>
<div class="m2"><p>در گوشه ای نشسته و حیران فتنه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناز آفت و کرشمه بلا، عشوه دل فریب</p></div>
<div class="m2"><p>یاران حذر کنید که توفان فتنه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فتنه ی غمش به که نالیم، چون مدام</p></div>
<div class="m2"><p>دیوان شاه حسن در ایوان فتنه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل گل فتاده پرتو رویت در انجمن</p></div>
<div class="m2"><p>این بزم عیش نیست، گلستان فتنه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسباب دلبری همه حسنش به فتنه داد</p></div>
<div class="m2"><p>در عهد حسن او که به سامان فتنه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون راز فتنه فاش نگردد که چشم او</p></div>
<div class="m2"><p>در خواب هم سرش به گریبان فتنه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی چه گونه حفظ دل خود کند، که باز </p></div>
<div class="m2"><p>چشم کرشمه ساز تو دوران فتنه است</p></div></div>