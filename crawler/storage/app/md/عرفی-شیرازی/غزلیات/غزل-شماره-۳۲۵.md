---
title: >-
    غزل شمارهٔ ۳۲۵
---
# غزل شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>کسی میوهٔ غم ز باغم نَخُورد</p></div>
<div class="m2"><p>که حسرت به عیش و فراغم نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیاسودم از خوردن غم، دمی</p></div>
<div class="m2"><p>که اندیشهٔ غم دماغم نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو صد شیشهٔ خون از دماغم چکید</p></div>
<div class="m2"><p>که مرهم شرابی ز داغم نخورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عهدم چنان عافیت مُرد زود</p></div>
<div class="m2"><p>که نو باوهٔ نخل باغم نخورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب غم چنان تلخ بر من گذشت</p></div>
<div class="m2"><p>که پروانه دود چراغم نخورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شدم شاخ گل، هیچ بلبل نخاست</p></div>
<div class="m2"><p>شدم استخوان، هیچ زاغم نخورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر خورد عرفی شراب از سفال</p></div>
<div class="m2"><p>که کوثر ز سیمین ایاغم نخورد</p></div></div>