---
title: >-
    غزل شمارهٔ ۵۵۹
---
# غزل شمارهٔ ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>من صید غم عشوه نمایی که تو باشی</p></div>
<div class="m2"><p>بیمار به امید دوایی که تو باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطفی به کسان گر نکند عیب بگیرند</p></div>
<div class="m2"><p>غارت زدهٔ مهر و وفایی که تو باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم همه جویند نشاط و طرب و عیش</p></div>
<div class="m2"><p>من فتنه و آشوب بلایی که تو باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای بخت ز شاهی به گدایی نرسیدیم</p></div>
<div class="m2"><p>در سایهٔ میمون همایی که تو باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس که ملایک به تماشای تو جمعند</p></div>
<div class="m2"><p>اندیشه نگنجد به سرایی که تو باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید به گرد سر هر ذره بگردد</p></div>
<div class="m2"><p>آن جا که خیال تو و جایی که تو باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی چه کند گر به ضیافت بردش وصل</p></div>
<div class="m2"><p>با نعمت دیدار گدایی که تو باشی</p></div></div>