---
title: >-
    غزل شمارهٔ ۴۷۸
---
# غزل شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>عفوت آوردم، دل شرمنده را آتش زدم</p></div>
<div class="m2"><p>خط آزادی نمودم بنده را آتش زدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاو کاو خانه کردم، جنس بی قیمت نبود</p></div>
<div class="m2"><p>شگر گفتم گوهر ارزنده را آتش زدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خنده ار با گریه دیدم بر در رد و قبول</p></div>
<div class="m2"><p>گریه را مقبول خواندم، خننده را آتش زدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بانگ هیهاتی ز دل بر داشتم کز گرمی اش</p></div>
<div class="m2"><p>مرده را بیدار کردم، زنده را آتش زدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده از مقصود بستم، چشمهٔ لذت گشود</p></div>
<div class="m2"><p>خان و مان طالع فرخنده را آتش زدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوستان را تا شدم آیینه دار از خوب و زشت</p></div>
<div class="m2"><p>مو به موی عرفی شرمنده را آتش زدم</p></div></div>