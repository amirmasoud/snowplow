---
title: >-
    غزل شمارهٔ ۲۰۰
---
# غزل شمارهٔ ۲۰۰

<div class="b" id="bn1"><div class="m1"><p>تا کی از لب-گهرِ آن مست تکلم ریزد</p></div>
<div class="m2"><p>این نمک چند به ریش دل مردم ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرفه حالی است که دارد اثر زهر ستم</p></div>
<div class="m2"><p>جرعهٔ لطف که در جام ترحم ریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مُردم از دُرد-سر و صاف نشد، کو ساقی</p></div>
<div class="m2"><p>کز من این جرعه بگیرد به سر خم ریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه ماتم زدگانیم و برین هست گواه</p></div>
<div class="m2"><p>مشت خاکی که صبا بر سر مردم ریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وای بر من که غیوری ز کف من دل بربود</p></div>
<div class="m2"><p>که گرش دست دهد خون به تبسم ریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی این غمزه بلاییست که در روز جزا</p></div>
<div class="m2"><p>نشتری بر در ارباب تظلم ریزد</p></div></div>