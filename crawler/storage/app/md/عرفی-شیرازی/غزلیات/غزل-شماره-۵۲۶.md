---
title: >-
    غزل شمارهٔ ۵۲۶
---
# غزل شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>نام حسنت چون برم، بر آسمان آید گران</p></div>
<div class="m2"><p>گر به گل بادی وزد بر باغبان آید گران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شهسوار حسن را، سر، مست باید بود، لیک</p></div>
<div class="m2"><p>نی چنان مستی که در دستش عنان آید گران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بر دل مانده از درد خردمندی بسی</p></div>
<div class="m2"><p>آن که بر دست و دلش رطل گران آید گران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی گناهی بین که ان بدخو به قصد کشتنم</p></div>
<div class="m2"><p>چون به زه بندد خدنگی بر کمان آید گران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر متاع وصل شیرین را بدان نتوان خرید</p></div>
<div class="m2"><p>بر دل پرویز گنج شایگان آید گران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک دلجویی کند چون منفعل گردم ز لطف</p></div>
<div class="m2"><p>بر کریمان شرم روی میهمان آید گران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در غمی زد غوطه عرفی، کان غم لذت سرشت</p></div>
<div class="m2"><p>بر دل یاران سبک، بر دشمنان آید گران</p></div></div>