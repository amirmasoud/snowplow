---
title: >-
    غزل شمارهٔ ۷۸
---
# غزل شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>کوی عشق است این که در هر گام صد عاقل گم است</p></div>
<div class="m2"><p>تا قیامت جان فراموش است و این جا دل گم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود چه راه است این که در صد سال یک منزل نیافت</p></div>
<div class="m2"><p>آن که در هر نیم گامش طی صد منزل گم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت جان دادنم بنگر که در روز جزا</p></div>
<div class="m2"><p>ننک قتلم در هجوم لذت قاتل گم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار در دل هست اگر دل نیست ایمن گو مباش</p></div>
<div class="m2"><p>کعبه گر محمل نشینم نیست از محمل گم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این که می گویند دریا می گشاید دست بخت</p></div>
<div class="m2"><p>تا در دل می شنو اما کلید دل گم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هجوم چاره اندیشی عرفی گشته گم</p></div>
<div class="m2"><p>عقل رهبر هم درین اندیشه ی هایل گم است</p></div></div>