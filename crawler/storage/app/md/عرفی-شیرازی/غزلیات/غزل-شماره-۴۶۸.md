---
title: >-
    غزل شمارهٔ ۴۶۸
---
# غزل شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>هرگز گله از دوست به محرم نفروشم</p></div>
<div class="m2"><p>گر مشتریم دوست شود هم نفروشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شورش غم با در و دیوار به حرفم</p></div>
<div class="m2"><p>رفت آن که به آسوده دلان غم نفروشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز نگشایم در دکان غم دل</p></div>
<div class="m2"><p>وانگه که دکان باز کنم کم نفروشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان اهل نفاقم نپسندند که هرگز</p></div>
<div class="m2"><p>قول غلط و فعل مسلم نفروشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی دل آباد به یک جو نخرد عشق</p></div>
<div class="m2"><p>من هم دل ویران به دو عالم نفروشم</p></div></div>