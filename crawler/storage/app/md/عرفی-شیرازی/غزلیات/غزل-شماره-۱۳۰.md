---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>خبری خواهم از آن کوی که اعزازی هست</p></div>
<div class="m2"><p>ز برون عرض نیازی ، ز درون نازی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاه گاهی به دعا یک دو بساطی در باز </p></div>
<div class="m2"><p>عشق این شیوه ضرور است، دغابازی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>های هایی ز من بلبل عشرت بشنو</p></div>
<div class="m2"><p>در مصیبت کده هم مرغ خوش آوازی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتشین بال و پرم دود بر آرد ز قفس</p></div>
<div class="m2"><p>گو ندانم که مرا رخصت پروازی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهتی دید و هوایی خوش و پرواز گرفت</p></div>
<div class="m2"><p>لیک مسکین چه خبر داشت که شهبازی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرفی آن زلف سیاه است کمندی که مراست</p></div>
<div class="m2"><p>مانده چین بر سر چین خم اندازی هست</p></div></div>