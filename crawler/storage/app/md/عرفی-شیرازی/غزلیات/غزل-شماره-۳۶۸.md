---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>باد دی گو ورق لاله و شمشاد ببر</p></div>
<div class="m2"><p>هر چه در معرض باد آمده گو باد ببر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عدل کسری چه کند با فلک و قدرت جم</p></div>
<div class="m2"><p>شکوهٔ کز تو کسی نشنود از یاد ببر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خسرو آوردی و بستیش در قصر بر او</p></div>
<div class="m2"><p>باز گرد ای فلک و مژده به فرهاد ببر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا دختر رز منتظر مقدم ماست</p></div>
<div class="m2"><p>بنشانش به سر حجله و داماد ببر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دلت مرده بگویم که چه کن؟ ماتم گیر!</p></div>
<div class="m2"><p>نام دل بر اثر ناله و فریاد ببر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی ای دل ز من افسانهٔ غم گوش کنی</p></div>
<div class="m2"><p>شکوه ای پیش کسی از من ناشاد ببر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهتر از شرم گناه است نبخشیدن جرم</p></div>
<div class="m2"><p>تو مرا عفو مکن، جرم من از یاد ببر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی اندیشه مرنچان چو تو نتوانی دید</p></div>
<div class="m2"><p>که همان شعر تر و نام تر از یاد ببر</p></div></div>