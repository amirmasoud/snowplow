---
title: >-
    غزل شمارهٔ ۵۶۹
---
# غزل شمارهٔ ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>با گلهٔ دوستان هست حلاوت بسی</p></div>
<div class="m2"><p>گر ز کسی نشنوی، خود گله ای کن، کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر رنجور من این همه غم سر مده</p></div>
<div class="m2"><p>کس نبرد دوزخی بر سر مشت خسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چه بود در جهان مایهٔ فخر خسان</p></div>
<div class="m2"><p>یا زر و سیمی بود، یا قصب و اطلسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من کیم از رهروان، راه روان کیستند</p></div>
<div class="m2"><p>واپسی از قافله، قافلهٔ واپسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی از ابنای دهر، عرفی خوش لهجه کیست</p></div>
<div class="m2"><p>بی هنری جاهلی، بی اثری ناکسی</p></div></div>