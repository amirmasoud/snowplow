---
title: >-
    غزل شمارهٔ ۳۰
---
# غزل شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>گفت و گوی غم یعقوب بود پیشه ی ما</p></div>
<div class="m2"><p>بوی پیراهن یوسف دهد اندیشه ی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر آن بیشه که با شیر دُمم آفت نیست</p></div>
<div class="m2"><p>روبه از بی جگری رم کند از بیشه ی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوهکن صنعت ما داشت ولی فرق بسی است</p></div>
<div class="m2"><p>قوت بازوی دل می طلبد تیشه ی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل ما غم دنیا غم معشوق شود</p></div>
<div class="m2"><p>باده گر خام بود پخته کند شیشه ی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عرفی افسانه تراشی به خموشی بفروخت</p></div>
<div class="m2"><p>لله الحمد که آزاد شد از پیشه ی ما</p></div></div>