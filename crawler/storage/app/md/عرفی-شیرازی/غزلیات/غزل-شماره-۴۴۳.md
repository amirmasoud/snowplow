---
title: >-
    غزل شمارهٔ ۴۴۳
---
# غزل شمارهٔ ۴۴۳

<div class="b" id="bn1"><div class="m1"><p>ما ره نشین مردم دیدار دوستیم</p></div>
<div class="m2"><p>سختی کشیم، حیف که غمخوار دوستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم خیال بازی و فکر کرشمهٔ </p></div>
<div class="m2"><p>دشمن تراش خاطر و آزار دوستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای نوحه سنج ناله بر زدی ز لب، که ما</p></div>
<div class="m2"><p>نازک دلان گریهٔ بسیار دوستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما می گزیم شهد ریا را نه زهد را </p></div>
<div class="m2"><p>تسبیح دشمنیم نه زنار دوستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عجز لذتیست، تو در کار خویش باش</p></div>
<div class="m2"><p>ما تشنهٔ شهادت و زنهار دوستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عندلیب گلبن دستان سرا، که من</p></div>
<div class="m2"><p>منصور نغمهٔ رسن و دار دوستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلوت نشینی از من و عرفی مجو که ما</p></div>
<div class="m2"><p>رسواییان کوچه و بازار دوستیم</p></div></div>