---
title: >-
    غزل شمارهٔ ۲۲۶
---
# غزل شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>حدیث عشق جان فرسا بگویید</p></div>
<div class="m2"><p>به دزدان اینسخن اما بگویید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متاع من نمی ارزد به تاراج</p></div>
<div class="m2"><p>حکایت با من از یغما بگویید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به طور ما نگنجد منع دیدار</p></div>
<div class="m2"><p>ولی این راز با موسی بگویید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قیامت را ز پی بستیم و رفتیم</p></div>
<div class="m2"><p>دگر افسانهٔ فردا بگویید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه باشد جان فسان این حکایت</p></div>
<div class="m2"><p>به دست و آستین ما بگویید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو ناحق کشتگان او شمارند</p></div>
<div class="m2"><p>به حق زخم او، کز ما بگویید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشانی از دل عرفی بیاور</p></div>
<div class="m2"><p>دگر غم را جهان بنما بگویید</p></div></div>