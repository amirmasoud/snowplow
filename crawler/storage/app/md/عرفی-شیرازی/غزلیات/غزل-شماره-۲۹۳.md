---
title: >-
    غزل شمارهٔ ۲۹۳
---
# غزل شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>برهمن کیشم، که صدقم طعنه بر اصحاب زد</p></div>
<div class="m2"><p>طاق آتشخانه ام صد خنده بر محراب زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرحبا ای عشق گلبانگی که بی آشوب تو</p></div>
<div class="m2"><p>عافیت خوش تکیه ها بر بالش سنجاب زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج توفان سایه هر گه بر سر کشتی فکند</p></div>
<div class="m2"><p>مُنعم از بهر تسلی ،تکیه بر اسباب زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو گلاب کفر تا بر چهرهٔ ایمان زنم</p></div>
<div class="m2"><p>گر تهی از هوش گشت و تکیه بر محراب زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خضر آب زندگی نوشید و عرفی خون دل</p></div>
<div class="m2"><p>این منور شعله گردید، آن قدح بر آب زد</p></div></div>