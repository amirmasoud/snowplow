---
title: >-
    غزل شمارهٔ ۵۰۲
---
# غزل شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>ما جام درد با دف و نی کم کشیده ایم</p></div>
<div class="m2"><p>دایم قدح نهفته ز محرم کشیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن ز جام می مکش ای محتسب که ما</p></div>
<div class="m2"><p>جام و سبو ز چشمهٔ زمزم کشیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانسته ایم تلخی عیش گذشته را </p></div>
<div class="m2"><p>تا خویش را به حلقهٔ ماتم کشیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناسور گشته زخم و نمک را چه می کنیم</p></div>
<div class="m2"><p>ما انتقام خویش ز مرهم کشیده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آسمان مناز به بیداد خود که دوش</p></div>
<div class="m2"><p>آهی برای مردم عالم کشیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما داده ایم شیوهٔ غم بی شکی قرار</p></div>
<div class="m2"><p>عرفی چه ها ز مردم بی غم کشیده ایم</p></div></div>