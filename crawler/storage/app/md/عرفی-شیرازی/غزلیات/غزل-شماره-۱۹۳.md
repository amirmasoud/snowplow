---
title: >-
    غزل شمارهٔ ۱۹۳
---
# غزل شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>سراپای وجودم در محبت ، حال دل دارد</p></div>
<div class="m2"><p>ز ذوق درد، بیرونم ، درون را مشتعل دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان از جلوهٔ حسنی که دل های شهیدان را</p></div>
<div class="m2"><p>ز ننگ آرمیدن های حیرانی خجل دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل امید ما را آفت پژمردگی نبود</p></div>
<div class="m2"><p>که باغ آرزوی ما هوای معتدل دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عهد حسن او گاه تبسم بینی از دل ها</p></div>
<div class="m2"><p>که گویی مردهٔ صد ساله در سینه دل دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی صد شد عذاب اهل عصیان،کز لحد عرفی</p></div>
<div class="m2"><p>ز خون گرم دل، سیلی به دوزخ متصل دارد</p></div></div>