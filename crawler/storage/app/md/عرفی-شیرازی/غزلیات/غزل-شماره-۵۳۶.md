---
title: >-
    غزل شمارهٔ ۵۳۶
---
# غزل شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>تو ای زاهد برو افسانهٔ باغ ارم بشنو</p></div>
<div class="m2"><p>ولی از وصف کوی او به بانگ شمه هم بشنو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ناکامی بمیرد هر که راه عشق پیماید</p></div>
<div class="m2"><p>عنان را نرم کن وین مژدگانی هر قدم بشنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب جام است در افسانه آن گه که می نوشی</p></div>
<div class="m2"><p>گمان دارم که گویم شمه ای از حال جم بشنو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مپر ای مرغ دل در صیدگاه ناز محبوبان</p></div>
<div class="m2"><p>ز هر جانب صدای بال شاهین راز هم بشنو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا ای آن که بر طرف حریم کعبه می تازی</p></div>
<div class="m2"><p>به گرد کوی او لبیک لبیک حرم بشنو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا در سینهٔ عرفی که مالامال غم گردی</p></div>
<div class="m2"><p>به حال او صدای آه درد آلود غم بشنو</p></div></div>