---
title: >-
    غزل شمارهٔ ۳۰۹
---
# غزل شمارهٔ ۳۰۹

<div class="b" id="bn1"><div class="m1"><p>به حکم عشق چو بر اهل صدق ره گیرند</p></div>
<div class="m2"><p>گناهکار ببخشند و بی گنه گیرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجو به محمل شاهی، که در ولایت عشق</p></div>
<div class="m2"><p>گدا به تخت نشانند و پادشه گیرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه ظلمت است که بینندگان نمی دانند</p></div>
<div class="m2"><p>که شبچراغ ستانند یا شبه گیرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمیر مایهٔ آسایش است لای شراب</p></div>
<div class="m2"><p>بگو که صاف کشان جرعه ای ز ته گیرند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمند کوته و بازوی سست و بام بلند</p></div>
<div class="m2"><p>به من حوالهٔ نومیدی ام گنه گیرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در معامله بگشا به کشور عرفی</p></div>
<div class="m2"><p>که خرده بر گهر آفتاب و مه گیرند</p></div></div>