---
title: >-
    غزل شمارهٔ ۳۰۰
---
# غزل شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>دودی ز دل برآمد و خون جوش می زند</p></div>
<div class="m2"><p>خون می چکد ز عقل و جنون جوش می زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سامری زیاده کن افسون و دم که باز</p></div>
<div class="m2"><p>دردم به رغم سحر و فسون جوش می زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پژمرده گشته بود کهن داغ های دل</p></div>
<div class="m2"><p>در لاله زار خنده کنون جوش می زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا جنتم به فال در آمد، بهشت را</p></div>
<div class="m2"><p>اندوه در برون و درون جوش می زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وادیی گمم که ز دل های تشنکان</p></div>
<div class="m2"><p>چندین هزار چشمهٔ خون جوش می زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا زخم دل گشوده و در خون نشسته ام</p></div>
<div class="m2"><p>در آتشم درون و برون جوش می زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرفی کجاست غمزه، به تقلید او که باز</p></div>
<div class="m2"><p>در صیدگاه، صید زبون جوش می زند</p></div></div>