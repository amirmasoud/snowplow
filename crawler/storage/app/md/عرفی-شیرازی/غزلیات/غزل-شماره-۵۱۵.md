---
title: >-
    غزل شمارهٔ ۵۱۵
---
# غزل شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>دل ر ا چه می دهی که به دارالشفا بریم</p></div>
<div class="m2"><p>این مرغ بسمل از دم تیغت کجا بریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاران مدد کنید که از وادی جنون</p></div>
<div class="m2"><p>دیوانه دل گرفته به دارالشفا بریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این مایه معصیت نه سزاوار بخشش است</p></div>
<div class="m2"><p>در حشر انتظار شفاعت چرا بریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این آبرو که صاف شراب خجالت است</p></div>
<div class="m2"><p>صد ره به خاک ریخته، دیگر کجا بریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما تاب انفعال نداریم، جور بس</p></div>
<div class="m2"><p>لازم شود، مباد که نام وفا بریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت ببین که وقت شبیخون احتیاج</p></div>
<div class="m2"><p>امیدهای کشته به پیش دعا بریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازار دوستت به دو عالم کجا برند</p></div>
<div class="m2"><p>جهدی کنیم و چشم و دل آشنا بریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عرفی غمین مشو که فلک دادش آمدست</p></div>
<div class="m2"><p>آمد که هر چه برده به یک نفس وا بریم</p></div></div>