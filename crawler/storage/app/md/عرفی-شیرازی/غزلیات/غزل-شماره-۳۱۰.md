---
title: >-
    غزل شمارهٔ ۳۱۰
---
# غزل شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>عیدی چنین، که زاهد، اندوه دین ندارد</p></div>
<div class="m2"><p>ناید ز دل که ما را، اندوهگین ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم به عید قربان، در عیش و من به حسرت</p></div>
<div class="m2"><p>کان حسرت شهادت، عیدی چنین ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت نبسته فرهاد، کارش، وگر نه شیرین</p></div>
<div class="m2"><p>گو یک نفس که گلگون، در زیر ران ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کافرتر است زاهد، از برهمن، ولیکن</p></div>
<div class="m2"><p>او را بت است در سر، در آستین ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خلوت ار به جاه است، این عرض و طول طاعت</p></div>
<div class="m2"><p>باور کنم که زاهد، خود را بر این ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ها که دانی ای دل، از زاهدان بی دین</p></div>
<div class="m2"><p>ظاهر مکن به عرفی، کو نیز دین ندارد</p></div></div>