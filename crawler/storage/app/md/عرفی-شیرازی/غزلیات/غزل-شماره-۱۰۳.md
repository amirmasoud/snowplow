---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>غمگساری در لباس دشمنی محبوبی است</p></div>
<div class="m2"><p>خشم و ناز آرایش بیرون و بزم خوبی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به سختی درد من ظاهر شود کاین اضطراب</p></div>
<div class="m2"><p>هم ترازوی متاع طاقت ایوبی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هوس آزادم اما آن چه دل را می گزد</p></div>
<div class="m2"><p>اشتیاق یوسفی و گریه ی یعقوبی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سدره ی آب و گلم پژمرده می گردد، ولی</p></div>
<div class="m2"><p>در نهادم شعله را نشو نمای طوبی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرح درد ما نباشد گفتن، ای عرفی خموش</p></div>
<div class="m2"><p>زحمت قاصد مده کاین داستان مکتوبی است</p></div></div>