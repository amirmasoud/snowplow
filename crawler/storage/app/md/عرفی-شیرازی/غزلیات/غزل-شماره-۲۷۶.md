---
title: >-
    غزل شمارهٔ ۲۷۶
---
# غزل شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>ز چشمم آب حسرت می تراود</p></div>
<div class="m2"><p>ز هر مویم شکایت می تراود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان در دل خلد گاه نمازم</p></div>
<div class="m2"><p>که کفرم از عبادت می تراود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی بی آبرو آن دل که از وی</p></div>
<div class="m2"><p>به کاویدن محبت می تراود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگو تیغ از چه شربت آب دادی</p></div>
<div class="m2"><p>که از هر زخم لذت می تراود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حذر کن زین دعای آتش آلود</p></div>
<div class="m2"><p> کزین چشمه اجابت می تراود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تراود از دل عرفی سخن ها</p></div>
<div class="m2"><p>ولی هنگام فرصت می تراود</p></div></div>