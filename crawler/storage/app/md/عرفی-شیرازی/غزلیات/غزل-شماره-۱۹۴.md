---
title: >-
    غزل شمارهٔ ۱۹۴
---
# غزل شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>گر باد شوم بر تو وزیدن نگذارند</p></div>
<div class="m2"><p>ور حسن شوم روی تو دیدن نگذارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سر زده شادی به دلم، سوخته عشقت</p></div>
<div class="m2"><p>این سبزه ازین خاک دمیدن نگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این رسم قدیم است که در گلشن مقصود</p></div>
<div class="m2"><p>بر خاک بریزد گل و چیدن نگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر شربت و گر زهر، به لب چون رسد این جام</p></div>
<div class="m2"><p>باید همه نوشید، چشیدن نگذارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از تربیت آب و هوا در چمن عشق</p></div>
<div class="m2"><p>نخلی که شود خشک، بریدن نگذارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما معتکف کعبه نشینم که در وی</p></div>
<div class="m2"><p>بیهوده به هر کوچه دویدن نگذارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیداست از آن حسن نظربازی عرفی</p></div>
<div class="m2"><p>کاین بلبل از آن باغ پریدن نگذارند</p></div></div>