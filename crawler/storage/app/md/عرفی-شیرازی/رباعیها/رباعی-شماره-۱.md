---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای شربت شیخ و شاب در کاسهٔ ما</p></div>
<div class="m2"><p>وی چشمهٔ آفتاب در کاسهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن جرعه کشانیم که از سیرابی</p></div>
<div class="m2"><p>یاقوت شود حباب در کاسهٔ ما</p></div></div>