---
title: >-
    رباعی شمارهٔ ۸۶
---
# رباعی شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>عشق تو خرابات نشین می باشد</p></div>
<div class="m2"><p>کوی تو بهشت عقل و دین می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دور تو هست جای دل در کف دست</p></div>
<div class="m2"><p>در عهد تو جان در آستین می باشد</p></div></div>