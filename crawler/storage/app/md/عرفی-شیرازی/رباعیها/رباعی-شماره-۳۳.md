---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>راهم ندهد سوی حرم زاهد زشت</p></div>
<div class="m2"><p>راند ز کنشت راهب نیک سرشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر لذت خواریم بدانند، از رشک</p></div>
<div class="m2"><p>هم آن کشد به کعبه، هم این به کنشت</p></div></div>