---
title: >-
    رباعی شمارهٔ ۵۱
---
# رباعی شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>ای عشق که مدح تو همین عشق بس است </p></div>
<div class="m2"><p>برقی ست که موسی اش یک مشت خس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی نی در مستی نزنم، گلزارست</p></div>
<div class="m2"><p>کش موسی عمرا ن گل مشکین نفس است</p></div></div>