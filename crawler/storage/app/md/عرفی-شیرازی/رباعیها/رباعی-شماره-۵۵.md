---
title: >-
    رباعی شمارهٔ ۵۵
---
# رباعی شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>عرفی منم آن که دوزخم تب‌شکن است</p></div>
<div class="m2"><p>روزم ز هجوم تیرگی شب‌شکن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امیدم اگر حاملهٔ حرمان‌زاست</p></div>
<div class="m2"><p>بپذیرم اگر سپاه مطلب‌شکن است</p></div></div>