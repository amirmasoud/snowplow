---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>عرفی دل من که منت جان من است</p></div>
<div class="m2"><p>از عالم قدس آمده، مهمان من است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار که پامال شود در ره کفر</p></div>
<div class="m2"><p>رحمی که جگر گوشهٔ ایمان من است</p></div></div>