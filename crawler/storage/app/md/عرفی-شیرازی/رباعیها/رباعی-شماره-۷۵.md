---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>ایوب به صبر خویشتن می نازد</p></div>
<div class="m2"><p>یعقوب به بوی پیرهن می نازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داوود به لحن خویشتن می نازد</p></div>
<div class="m2"><p>این عشق به ناله های من می نازد</p></div></div>