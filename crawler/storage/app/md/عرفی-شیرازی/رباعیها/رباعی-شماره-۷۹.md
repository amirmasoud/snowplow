---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>در دیدهٔ هجر خواب پژمرده شود</p></div>
<div class="m2"><p>دل بی لبت از شراب پژمرده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو چون گل ز دم سرد خزان</p></div>
<div class="m2"><p>از آه من آفتاب پژمرده شود</p></div></div>