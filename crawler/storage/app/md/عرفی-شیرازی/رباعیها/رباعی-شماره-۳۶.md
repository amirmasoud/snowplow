---
title: >-
    رباعی شمارهٔ ۳۶
---
# رباعی شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>در عهد من آن که لاف سنج سخن است</p></div>
<div class="m2"><p>خونش هدر است، قاتلش نظم من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوسالهٔ سامری اگر بانگ زند</p></div>
<div class="m2"><p>اعجاز مسیح لقمهٔ دندان شکن است</p></div></div>