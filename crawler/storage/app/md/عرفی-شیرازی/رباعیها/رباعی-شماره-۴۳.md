---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>در باغم و دل شکارگاه شیراست</p></div>
<div class="m2"><p>نگشوده نظر دل از تماشا سیر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دیده گشایم که چمن بیگانه ست</p></div>
<div class="m2"><p>چون سینه گشایم که هوا شمشیر است</p></div></div>