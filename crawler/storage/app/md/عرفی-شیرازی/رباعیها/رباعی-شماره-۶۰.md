---
title: >-
    رباعی شمارهٔ ۶۰
---
# رباعی شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ای شوق لبت ز صبر من برده ثبات</p></div>
<div class="m2"><p>تلخ از شکرین تبسمت کام نبات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشتاق لبت را چو اجل خون ریزد</p></div>
<div class="m2"><p>از تیغ اجل فرو چکد آب حیات</p></div></div>