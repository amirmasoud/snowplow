---
title: >-
    رباعی شمارهٔ ۱۱۶
---
# رباعی شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>در علم و عمل چو ذوفنون آید مرد</p></div>
<div class="m2"><p>آرایش بیرون و درون آید مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از معرکه بی زخم برون آید مرد</p></div>
<div class="m2"><p>وز پردهٔ کار غرق خون آید مرد</p></div></div>