---
title: >-
    رباعی شمارهٔ ۴۵
---
# رباعی شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>در دیدهٔ تو روشنی شرم به است</p></div>
<div class="m2"><p>در سینهٔ تو جان و دل نرم به است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرهیز کن از فسردگی در ره عشق</p></div>
<div class="m2"><p>کز گریه سر خندهٔ گرم به است</p></div></div>