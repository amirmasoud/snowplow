---
title: >-
    رباعی شمارهٔ ۹۵
---
# رباعی شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>ای ملک غمت هر چه فرازست و فرود</p></div>
<div class="m2"><p>وز تیغ تو چاک صبر را جوش وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن خال سیه نیست که از لطف جبین</p></div>
<div class="m2"><p>جای گرهٔ زلف تو گردیده کبود</p></div></div>