---
title: >-
    رباعی شمارهٔ ۷۷
---
# رباعی شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>عرفی که قدم در دهن تیشه نهد</p></div>
<div class="m2"><p>از بس غم دل بر دل غم پیشه نهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تحت الثری فرو شود، گر نه مدام</p></div>
<div class="m2"><p>بار دل خود به دوش اندیشه نهد</p></div></div>