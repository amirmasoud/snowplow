---
title: >-
    رباعی شمارهٔ ۶۴
---
# رباعی شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>دل دشمن شادی ست و در کار غم است</p></div>
<div class="m2"><p>از عافیت آسوده و بیمار غم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیماری دل مایهٔ او، زردی ماست</p></div>
<div class="m2"><p>رو زردی ما بهار گلزار غم است</p></div></div>