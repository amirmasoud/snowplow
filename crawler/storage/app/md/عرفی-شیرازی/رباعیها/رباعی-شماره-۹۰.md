---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>در سردی یخ بند که لرزد خورشید</p></div>
<div class="m2"><p>خون بسته شود چون بقم در رگ بید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل دسته ای از دود شرر بسته شود</p></div>
<div class="m2"><p>کاندر کف روزگار ماند جاوید</p></div></div>