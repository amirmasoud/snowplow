---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>یار آمده و در صدد دلداری ست</p></div>
<div class="m2"><p>من مست و خراب، این شب صد دشواری ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدار شو ای بخت، به خوابم کردی </p></div>
<div class="m2"><p>فریاد که خواب تو بهتر از بیداری ست</p></div></div>