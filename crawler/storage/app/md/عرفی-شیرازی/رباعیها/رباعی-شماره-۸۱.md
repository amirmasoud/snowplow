---
title: >-
    رباعی شمارهٔ ۸۱
---
# رباعی شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>شوخی که ز خنده چشمهٔ نوش شود</p></div>
<div class="m2"><p>خورشید به سایه اش هم آغوش شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خندید و کرشمه کرد و از خود رفتم</p></div>
<div class="m2"><p>آری دو شیرابه زود بیهوش شود</p></div></div>