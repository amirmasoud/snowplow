---
title: >-
    رباعی شمارهٔ ۱۱۷
---
# رباعی شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ای آهوی فتنه سنبلت را به کمند</p></div>
<div class="m2"><p>در دام فریبت اهل ایمان در بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد از تو به نزد ماست اسلام عزیز</p></div>
<div class="m2"><p>نازی که ز هم بریزد آن ترک بلند</p></div></div>