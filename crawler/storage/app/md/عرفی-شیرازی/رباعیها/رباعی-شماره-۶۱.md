---
title: >-
    رباعی شمارهٔ ۶۱
---
# رباعی شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ای کعبه رو این طرف که بی سازی نیست</p></div>
<div class="m2"><p>طوفی و خروشی و تک و تازی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر تا سر کوچهٔ خرابات مغان</p></div>
<div class="m2"><p>آشفته و مست رو، که طنازی نیست</p></div></div>