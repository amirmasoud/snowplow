---
title: >-
    رباعی شمارهٔ ۱۳
---
# رباعی شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>شاها کرم تو قلزم مواج است</p></div>
<div class="m2"><p>درویش تو اسکندر بی تاج است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منسوب به عالم نزول تو بود</p></div>
<div class="m2"><p>آرام گهی که نام او معراج است</p></div></div>