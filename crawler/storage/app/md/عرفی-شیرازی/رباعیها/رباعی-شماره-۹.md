---
title: >-
    رباعی شمارهٔ ۹
---
# رباعی شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>نادان به عمارت بدن مشغول است</p></div>
<div class="m2"><p>دانا به کرشمهٔ سخن مشغول است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی به فریب مرد و زن مشغول است</p></div>
<div class="m2"><p>عاشق به هلاک خویشتن مشغول است</p></div></div>