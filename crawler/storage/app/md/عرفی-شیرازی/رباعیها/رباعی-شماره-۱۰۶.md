---
title: >-
    رباعی شمارهٔ ۱۰۶
---
# رباعی شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>رفتم به حرم که درد ایمان دانند</p></div>
<div class="m2"><p>معموری دل ز کفر ویران دانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتند برو به دیر کاین سنگ سیاه</p></div>
<div class="m2"><p>قدر گهرش صنم تراشان دانند</p></div></div>