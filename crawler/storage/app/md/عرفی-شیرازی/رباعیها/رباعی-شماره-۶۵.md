---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>با معصیتم که کرده ای امن کنشت</p></div>
<div class="m2"><p>با عاطفتت که می برد آب بهشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوزخ همه عافیت چو دلسوزی خصم</p></div>
<div class="m2"><p>جنت همه زخم چون عشوهٔ زشت</p></div></div>