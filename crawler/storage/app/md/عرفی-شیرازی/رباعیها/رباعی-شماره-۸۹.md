---
title: >-
    رباعی شمارهٔ ۸۹
---
# رباعی شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>وقت است که یاران به گلستان ریزند</p></div>
<div class="m2"><p>گل های نشاط در گریبان ریزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل به هوای باغ بشکست قفس</p></div>
<div class="m2"><p>این مژده به شاخ و برگ بستان ریزند</p></div></div>