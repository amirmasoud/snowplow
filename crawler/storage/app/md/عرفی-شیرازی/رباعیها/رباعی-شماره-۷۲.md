---
title: >-
    رباعی شمارهٔ ۷۲
---
# رباعی شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>ای مهر تو هیچ وکین دشمن هم هیچ</p></div>
<div class="m2"><p>آهنگ سرود هیچ و شیون هم هیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هر چه نقاب می گشایی عشق است</p></div>
<div class="m2"><p>عرفی همه هیج و هیچ گفتن هم هیچ</p></div></div>