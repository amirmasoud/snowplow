---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای کرده زبون، ناز شجاع تو، مرا</p></div>
<div class="m2"><p>افکنده به صد رنج، نزاغ تو، مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا خیزم و آیمت در آغوش اجل</p></div>
<div class="m2"><p>گشست است به تکلیف وداع تو مرا</p></div></div>