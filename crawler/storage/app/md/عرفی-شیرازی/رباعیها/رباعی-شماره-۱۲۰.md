---
title: >-
    رباعی شمارهٔ ۱۲۰
---
# رباعی شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>خوش آن که شراب همتم مست کند</p></div>
<div class="m2"><p>آوازهٔ امید مرا پست کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دست زنم به کام، در دست دگر</p></div>
<div class="m2"><p>شمشیر دهم که قطع آن دست کند</p></div></div>