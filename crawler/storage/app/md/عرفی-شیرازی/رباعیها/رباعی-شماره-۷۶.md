---
title: >-
    رباعی شمارهٔ ۷۶
---
# رباعی شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>آن کس که عنان تافت ز ما گمره شد</p></div>
<div class="m2"><p>وان کس که عنان سپرد کارآگه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف به در آورد و زلیخا گردید</p></div>
<div class="m2"><p>هر کس که به ریسمان ما در چه شد</p></div></div>