---
title: >-
    رباعی شمارهٔ ۷۴
---
# رباعی شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>فردا که معاملان هر فن طلبند</p></div>
<div class="m2"><p>حسن عمل از شیخ و برهمن طلبند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان ها که دروده ای جوی نستانند</p></div>
<div class="m2"><p>آن ها که نکشته ای به خرمن طلبند</p></div></div>