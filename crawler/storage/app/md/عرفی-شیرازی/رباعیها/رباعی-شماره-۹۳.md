---
title: >-
    رباعی شمارهٔ ۹۳
---
# رباعی شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>آن کس که ز راه نفسم بسته کند</p></div>
<div class="m2"><p>دل را ز هجوم داغ گل دسته کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیماران را دم مسیح است علاج</p></div>
<div class="m2"><p>ای وای بر آن کس دم او تفته کند</p></div></div>