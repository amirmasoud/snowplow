---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>دردا که دگر سخن ز فرزانگی است</p></div>
<div class="m2"><p>چیزی که نه در شمار دیوانگی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانگی عافیتم ننگی بود</p></div>
<div class="m2"><p>اکنون به وی ام نسبت هم خانگی است</p></div></div>