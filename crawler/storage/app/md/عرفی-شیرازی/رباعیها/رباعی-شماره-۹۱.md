---
title: >-
    رباعی شمارهٔ ۹۱
---
# رباعی شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>شاهی که فلک هم گهر او نشود</p></div>
<div class="m2"><p>سنجیدن او به سعی بازو نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم سایهٔ او نهند در کفه مگر</p></div>
<div class="m2"><p>ور نه دو جهانش هم ترازو نشود</p></div></div>