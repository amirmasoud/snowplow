---
title: >-
    رباعی شمارهٔ ۴۹
---
# رباعی شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>عرفی علم هجر تو افراشننی است</p></div>
<div class="m2"><p>گنجی تو ولی نقد تو برداشتنی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عشق تویی، تخم تو ناکشتنی است</p></div>
<div class="m2"><p>ور حسن تویی، دل ز تو برداشتنی است</p></div></div>