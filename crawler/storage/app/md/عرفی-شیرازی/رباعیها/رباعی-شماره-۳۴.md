---
title: >-
    رباعی شمارهٔ ۳۴
---
# رباعی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>مسجود ملایک دو تن از آب و گل است</p></div>
<div class="m2"><p>ز آدم چو گذشت این نگار چگل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هست تفاوتی همین باشد و بس</p></div>
<div class="m2"><p>کان حکم اله بود وین حکم دل است</p></div></div>