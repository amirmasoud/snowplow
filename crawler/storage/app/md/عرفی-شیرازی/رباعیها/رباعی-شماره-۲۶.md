---
title: >-
    رباعی شمارهٔ ۲۶
---
# رباعی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>عرفی سر صفهٔ مغان مسند ماست</p></div>
<div class="m2"><p>تعظیم گه دیر مغان معبد ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گام به تیغی سر تسلیم نهیم</p></div>
<div class="m2"><p>سر تا سر کوی دوستی مشهد ماست</p></div></div>