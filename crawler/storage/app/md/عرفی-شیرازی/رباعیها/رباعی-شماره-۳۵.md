---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>معموری عقل فضلهٔ ویرانی ست</p></div>
<div class="m2"><p>سرمایهٔ علم خاک بی سامانی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازارچهٔ حیرت ما آبادان ست</p></div>
<div class="m2"><p>کافتاده متاع و غایت ارزانی ست</p></div></div>