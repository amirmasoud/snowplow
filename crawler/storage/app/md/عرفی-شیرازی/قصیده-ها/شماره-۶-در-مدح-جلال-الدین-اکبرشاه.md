---
title: >-
    شمارهٔ ۶ - در مدح جلال الدین اکبرشاه
---
# شمارهٔ ۶ - در مدح جلال الدین اکبرشاه

<div class="b" id="bn1"><div class="m1"><p>ای دل معنی سرشتت راز دان آفتاب</p></div>
<div class="m2"><p>تا ابد بر خوان دولت میهمان آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کمال دولتت هر کس که بیند بنگرد</p></div>
<div class="m2"><p>از شراب تربیت رطل گران آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولت جمشید همدوشی کند با دولتت</p></div>
<div class="m2"><p>گر تواند سایه بودن هم عنان آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوطی نطقم چو در مدحت شکرخائی کند</p></div>
<div class="m2"><p>آب گرم از ذوق گردد در دهان آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا لوای دولتت را نگذراند از اوج عرش</p></div>
<div class="m2"><p>اهل معنی را نشد معلوم شان آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاروان سالارشاهان آفتاب آمد ولی</p></div>
<div class="m2"><p>چون تو ناید یوسفی در کاروان آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دهر سر کش رام شد در زیر ران دولتت</p></div>
<div class="m2"><p>چون سمند آسمان در زیر ران آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم چو شمعی کان بر افروزند از شمع دگر</p></div>
<div class="m2"><p>از یکی نور است جان شاه و جان آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض می تابد زرویت چون نتابد کز ازل</p></div>
<div class="m2"><p>گوهرت را پرورش داد است کان آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سجده گاه هفت اقلیم است مسند گاه تو</p></div>
<div class="m2"><p>قبله هفت آسمان است آسمان آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسکه عکس آفتاب دیده در دل آسمان</p></div>
<div class="m2"><p>کرده نام سینه اش آئینه دان آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا آماج گاه طلعتت آماده کرد</p></div>
<div class="m2"><p>می جهد تیر سعادت از کمان آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر همای آفتاب آرامگه می داشتی</p></div>
<div class="m2"><p>جای اکبر شاه بودی آشیان آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وصف شاه از بی کس چون من کجا لایق شود</p></div>
<div class="m2"><p>هر چه کردم نقل کردم از زبان آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرچه سیر آفتاب اندر جهان ظاهر است</p></div>
<div class="m2"><p>باطن شاه است در معنی زبان آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر پس از قرنی بود سعدین را با هم قران</p></div>
<div class="m2"><p>چون بود هر صبحدم باشد قران آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حکم خورشید است و حکم شه که در معنی یکیست</p></div>
<div class="m2"><p>روزگار دولت شاه و زمان آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دمبدم چون ماه نو نور رخش افزون شود</p></div>
<div class="m2"><p>هر که پیشانی نهد بر آستان آفتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دیده از عینک چنان نظاره اشیا کند</p></div>
<div class="m2"><p>همچنان بیند دلت راز نهان آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدح خورشید و ثنای شه کند عرفی مدام</p></div>
<div class="m2"><p>کز مریدان شه است و عاشقان آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مزین رشته گوهر طرازان وجود</p></div>
<div class="m2"><p>گوهر ذات تو آذین دکان آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که مهر آفتابش جوشد از سرتا قدم</p></div>
<div class="m2"><p>نور بارد از سراپایش بسان آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا کند گردش عیان راز نهان آسمان</p></div>
<div class="m2"><p>تا دهد زیب جهان حسن عیان آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وقف دولت باد سر لایزال آسمان</p></div>
<div class="m2"><p>نور چشمت باد حسن جاودان آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مایه اخلاص من خاطر نشان شاه باد</p></div>
<div class="m2"><p>همچنان کاخلاص شه خاطر نشان آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر سر شه سایه افکن چون شود بال هما</p></div>
<div class="m2"><p>چون پر خفاش گردد سایه بان آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر بدان غایت که شه بشناسدش باید شناخت</p></div>
<div class="m2"><p>از مسیحا هم مجو نام و نشان آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسمان داند که چون شاه جهان هرگز نبود</p></div>
<div class="m2"><p>قدردان آفتاب اندر زمان آفتاب</p></div></div>