---
title: >-
    شمارهٔ ۲۲ - در ستایش رسول اکرم «ص»
---
# شمارهٔ ۲۲ - در ستایش رسول اکرم «ص»

<div class="b" id="bn1"><div class="m1"><p>ای مهر تو جان آفرینش</p></div>
<div class="m2"><p>نعت تو زبان آفرینش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف تو چمن طراز امکان</p></div>
<div class="m2"><p>خشم تو خزان آفرینش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جودت همه بخش عالم کون</p></div>
<div class="m2"><p>علمت همه دان آفرینش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با لقمه همت تو بس تنگ</p></div>
<div class="m2"><p>میدان دهان آفرینش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتای تو بهترین خطابش</p></div>
<div class="m2"><p>بی نام و نشان آفرینش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جنب تعینت دو عالم</p></div>
<div class="m2"><p>بهمان و فلان آفرینش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا گوهر فطرت تو گردید</p></div>
<div class="m2"><p>آیین دکان آفرینش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تیزی بگذاشت تیغه صنع</p></div>
<div class="m2"><p>در کاوش کان آفرینش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناشی ز هوای جلوه تو</p></div>
<div class="m2"><p>ارخای عنان آفرینش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ضمن شمردن عطایت</p></div>
<div class="m2"><p>افلاج بنان آفرینش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اندیشه احتمال شانت</p></div>
<div class="m2"><p>زآنسوی گمان آفرینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهمانی میزبان جودت</p></div>
<div class="m2"><p>عید رمضان آفرینش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شمشیر کمال تو نیامد</p></div>
<div class="m2"><p>محتاج فسان آفرینش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>معراج تو در هوای لاهوت</p></div>
<div class="m2"><p>حد طیران آفرینش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با طالع حاسد تو همزاد</p></div>
<div class="m2"><p>فوج حدثان آفرینش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با نطفه دشمن تو توأم</p></div>
<div class="m2"><p>صد مرثیه خوان آفرینش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>امکان وجود دشمن تو</p></div>
<div class="m2"><p>زنار میان آفرینش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عیسی مگس و تکلم تو</p></div>
<div class="m2"><p>حلوای دکان آفرینش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صافی شکر شفاعت تو</p></div>
<div class="m2"><p>قوت مگسان آفرینش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با دیدن آب گوهر تو</p></div>
<div class="m2"><p>دفع یرقان آفرینش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تأثیر ملال غیبت تو</p></div>
<div class="m2"><p>وجه خفقان آفرینش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نعلین تو تاج قاب قوسین</p></div>
<div class="m2"><p>تمکین تو شان آفرینش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در بازوی قدرت تو مضمر</p></div>
<div class="m2"><p>صد زور کمان آفرینش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>با علم تو آشنا نیفتاد</p></div>
<div class="m2"><p>یک مسئله دان آفرینش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نظاره چهره حسودت</p></div>
<div class="m2"><p>وجه غشیان آفرینش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افسانه سرنوشت خصمت</p></div>
<div class="m2"><p>تزریق بیان آفرینش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با مستی شوق تست عرفی</p></div>
<div class="m2"><p>از بیخبران آفرینش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در مغز دماغ او خبر نیست</p></div>
<div class="m2"><p>از عنبر و بان آفرینش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دعوی کن نعت لایق تو</p></div>
<div class="m2"><p>رسوای جهان آفرینش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دارد بعنایت تو عرفی</p></div>
<div class="m2"><p>حرفی ز زبان آفرینش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برخیز که شور کفر برخاست</p></div>
<div class="m2"><p>ای فتنه نشان آفرینش</p></div></div>