---
title: >-
    شمارهٔ ۵۷ - در تهنیت تولد فرزند خانخان
---
# شمارهٔ ۵۷ - در تهنیت تولد فرزند خانخان

<div class="b" id="bn1"><div class="m1"><p>بود درکتم عدم بکر طبیعت را جای</p></div>
<div class="m2"><p>که خرد بر سرش استاده همی گفت برآی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند در پرده نشیند خلف دوده کون</p></div>
<div class="m2"><p>محرمی نیست مگر هم تو شوی پرده گشای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه ترا عقد زفاف است در این پرده ضرور</p></div>
<div class="m2"><p>نه مرا صبر و سکون داده در این دیر خدای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مریمی کن تو که فرزند مسیح است مسیح</p></div>
<div class="m2"><p>خاتمی کن تو که توفیق گدای است گدای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سخن گوشزد بکر طبیعت چون گشت</p></div>
<div class="m2"><p>خنده زد گفت که رو صبر کن وژاژ مخای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه ای گیر و جگر میخور و تلخی میکش</p></div>
<div class="m2"><p>تا بعهدیکه شود صاحب تو ملک آرای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق از «مژده بر» و«مژده شنو» جمع شوند</p></div>
<div class="m2"><p>جمله جوهر طلب و جوهری و گنج ستای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک آماده شود زهره مهیا گردد</p></div>
<div class="m2"><p>آن یکی حله طراز آید و این غالیه سای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من به صد ناز و کرشمه همه رنگ و همه بوی</p></div>
<div class="m2"><p>برسر حجله ارکان نهم از خلوت پای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس درآید ببرم، آنکه منش نامزدم</p></div>
<div class="m2"><p>او کشد بند نقاب من و بند قبای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعد از آن کشمکش و طی شدن حالت حمل</p></div>
<div class="m2"><p>لب بگستاخی اگر باز کنی دارد جای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لله الحمد که آن وعده بپایان آمد</p></div>
<div class="m2"><p>هم خرد کامروا آمد و هم بار خدای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دوش بر دوش قضا دست در آغوش قدر</p></div>
<div class="m2"><p>آمد از پرده برون پردگی صنع خدای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وهم باطالع او گفت که باشم در عرش</p></div>
<div class="m2"><p>گفت گرگم نشوی پیشترک هم میآی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخت با گوهر او گفت که دولت بس نیست</p></div>
<div class="m2"><p>گفت دانم بچه ای حامله،رو ، رو میزای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سال مولودش از آن شاهگل بی بدل است</p></div>
<div class="m2"><p>که ندارد بدلی در چمن دولت ورای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرحبا ای گهرت را شرف ذات پدر</p></div>
<div class="m2"><p>مرحبا ای قدمت را اثر ظل همای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرحبا ای زعنایات ازل رمز فروش</p></div>
<div class="m2"><p>مرحبا ای بعلامات هنر خویش ستای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرحبا ای نظر بخت تو کیوان پرور</p></div>
<div class="m2"><p>مرحبا ای گهر ذات تو امکان آرای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرحبا ای بکنار آمده از صلب پدر</p></div>
<div class="m2"><p>جاودان در کنف فضل پدر میآسای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خان خانان که کمالی است مصور گهرش</p></div>
<div class="m2"><p>کوشناسای گهر، تا نگرد صنع خدای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ناخن قدرت او پرده تحقیق شکاف</p></div>
<div class="m2"><p>خامه دولت او چهره توفیق گشای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زیب فرماندهیش در شکن طرف کلاه</p></div>
<div class="m2"><p>نقد زیبندگیش در گره بند قبای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دشمنش را بود آن مایه شقاوت که بود</p></div>
<div class="m2"><p>گردآلایش او دامن جیحون آلای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دیده عقل شود خیره زآیینه وهم</p></div>
<div class="m2"><p>گر شود صیقل اندیشه او زنگ زدای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عدل او چون روش آموز مکافات شود</p></div>
<div class="m2"><p>پیرو جاذبه کاه شود کاه ربای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخت او گر بدل نغمه طرازان گذرد</p></div>
<div class="m2"><p>شاخ طوبی شود از برگ و ثمر پیکرنای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زآن بود زنده حسودش که جهان گشته ز ننگ</p></div>
<div class="m2"><p>در وجود عدم دشمن او بی پروای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنچنان پیرو شاه است که از غایت قرب</p></div>
<div class="m2"><p>گه گهی سایه رساند بسرش بال همای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اختلاف صور از نوع بشر برخیزد</p></div>
<div class="m2"><p>خامه معدلت او شود ار چهره گشای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای که در سایه عدلت همه امن است و امان</p></div>
<div class="m2"><p>عالم فتنه فروش و ملک نائبه زای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بهوش تو دهد صافی صهبای رموز</p></div>
<div class="m2"><p>گردد از پرده دل عاقله ، دانش پالای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شام احباب ترا طلعت خورشید اندود</p></div>
<div class="m2"><p>صبح اعدای ترا ظلمت خورشید اندای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نزد ادراک تو اسرار قضا برکف دست</p></div>
<div class="m2"><p>پیش فرمان تو احکام فلک برسر پای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسکه از لطف و عطا عزت و ثروت بخشد</p></div>
<div class="m2"><p>عالم آرا دل و دست تو بهر بی سر و پای</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وقت آنست که دختر طلبد از پی عقد</p></div>
<div class="m2"><p>دودمان کرم، از سلسله آز، گدای</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گر نگشتی کرمت حامی اصناف امم</p></div>
<div class="m2"><p>احتسابت نشدی عامل معزول نمای</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زهر ناز از نگه خود بمکد چشم بتان</p></div>
<div class="m2"><p>هر کجا عدل تو از ظلم شود پرده گشای</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای که از بهر ستایشگریت معتکف است</p></div>
<div class="m2"><p>بر لب نکته سرایم خرد نادره زای</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مدحت جز تو بفتوای یک اندیشی من</p></div>
<div class="m2"><p>چون غم و شادی مغلوب طبیعت پیمای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حرص کسب شرفم لب بثنای تو گشود</p></div>
<div class="m2"><p>وای اگر معذرتم عرض تو میبودی وای</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دیده نه فلکم زایر انگشتان است</p></div>
<div class="m2"><p>هر گه از نامه مدح تو شوم بوسه ربای</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جایم از دیده کند عقل و چنینم دارند</p></div>
<div class="m2"><p>هر که را کعبه مدح تو شود ناصیه سای</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گل اندیشه من سحر خطا معجزه رنگ</p></div>
<div class="m2"><p>بلبل نطق من الهام غلط وحی سرای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کلکم از بهر سخن چینی من سر در پیش</p></div>
<div class="m2"><p>وز علوم سخنم تارک او گردون سای</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رهرو طبعم اگر قطع کند وادی خواب</p></div>
<div class="m2"><p>بر سر گنج معانی همه ره دارد پای</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عرفی آهنگ دعای کن بس از این لاف و گزاف</p></div>
<div class="m2"><p>وجه کفاره بدست آر و دگر ژاژ مخای</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا محال است که مهتاب بگز پیمایند</p></div>
<div class="m2"><p>تا بود در عرض خلق فلک ناپروای</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باد مساح فلک در عرض آباد جهان</p></div>
<div class="m2"><p>بذراع عرضت مزرع دوران پیمای</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یأس و امید محبان تو مقصود انگیز</p></div>
<div class="m2"><p>بود و نابود حسودان تو حرمان آلای</p></div></div>