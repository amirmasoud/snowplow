---
title: >-
    شمارهٔ ۴ - در نعت پیغمبر اسلام «ص»
---
# شمارهٔ ۴ - در نعت پیغمبر اسلام «ص»

<div class="b" id="bn1"><div class="m1"><p>ای برزده ، دامن بلا را</p></div>
<div class="m2"><p>سر در پی خویش داده ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در ره مردمی نهی پای</p></div>
<div class="m2"><p>از کوچه ما طلب وفا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یادم نکنی و هیچ گه من</p></div>
<div class="m2"><p>بی مژده ندیده ام صبا را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانگی محبت تو</p></div>
<div class="m2"><p>کامروز مسلم است ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانه زتاج کرد تارک</p></div>
<div class="m2"><p>آواره زکفش کرد پا را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان و دل من پر از غم تست</p></div>
<div class="m2"><p>بهر تو تهی کنم چه جا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آماده صد، سرود دردم</p></div>
<div class="m2"><p>ناکرده تمام یک نوا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد چاک سپرده ام بهر دست</p></div>
<div class="m2"><p>ناکرده بدوش یک قبا را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بخت چنان مکن که آخر</p></div>
<div class="m2"><p>ممنون اثر کنم دعا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا دست جفای چرخ بر بند</p></div>
<div class="m2"><p>یا بخل عطای مدعا را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی بشکیب در پذیرم</p></div>
<div class="m2"><p>آفات نجوم فتنه زا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یارب چه عداوت است با من</p></div>
<div class="m2"><p>این کارکنان کبریا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با خویش چو راز دوست گویم</p></div>
<div class="m2"><p>از خانه برون کنم صبا را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ملک فرنگ و شهر اسلام</p></div>
<div class="m2"><p>معزول ندیده ام هوی را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا کی بمیان خود ببینم</p></div>
<div class="m2"><p>دست اجل شکسته پا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در انجمن جمال ، رویت</p></div>
<div class="m2"><p>بگرفته زآفتاب جا را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر نقش جمال تو نگیرد</p></div>
<div class="m2"><p>از سینه برون کنم صفا را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا کی فلکم بعشوه گوید</p></div>
<div class="m2"><p>کای وهم تو کرده پی صبا را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از عشق فلان بباد دادی</p></div>
<div class="m2"><p>سرمایه دانش و ذکا را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر ند که راست گوید اما</p></div>
<div class="m2"><p>خاموشی این ستم فزا را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رفتم که بگنج خانه طبع</p></div>
<div class="m2"><p>مرهون شرف کنم ثنا را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گنجی بکف آورم که شاید</p></div>
<div class="m2"><p>سرمایه نعت مصطفی را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درج گهر آورم که شاید</p></div>
<div class="m2"><p>آویزه گوش انبیا را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دستی سخن آورم که شاید</p></div>
<div class="m2"><p>مجموعه لطف اولیا را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اینک به زبان رساندم از دل</p></div>
<div class="m2"><p>تا داغ کنم دل شما را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای جود تو دست و دل سخا را</p></div>
<div class="m2"><p>وی عزم تو بال و پر صبا را</p></div></div>