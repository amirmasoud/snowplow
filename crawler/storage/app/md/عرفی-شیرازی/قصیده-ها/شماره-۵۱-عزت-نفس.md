---
title: >-
    شمارهٔ ۵۱ - عزت نفس
---
# شمارهٔ ۵۱ - عزت نفس

<div class="b" id="bn1"><div class="m1"><p>گر مرد همتی ز مروت نشان مخواه</p></div>
<div class="m2"><p>صد جا شهید شو ، دیت از دشمنان مخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان زجاج و در جگر افشان و نم مجوی</p></div>
<div class="m2"><p>بشکن سفال و در دهن انداز و نان مخواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک از فلک مخواه و مراد از زمین مجوی</p></div>
<div class="m2"><p>ماه از زمین مجوی و وفا زاسمان مخواه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترصیع تخت و تاجت اگر خسروی دهد</p></div>
<div class="m2"><p>بشکن کلاه مسند وگوهر زکان مخواه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ماه و آفتاب بمیرد عزا مگیر</p></div>
<div class="m2"><p>گر تیر و زهره کشته شود نوحه خوان مخواه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شریان ز پوست برکش و درکام تیغ نه</p></div>
<div class="m2"><p>لب را گلو بگیر و زقاتل امان مخواه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بی شهادت از در عشقت روان کنند</p></div>
<div class="m2"><p>تیغ کرشمه و دل نامهربان مخواه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر مژده وصال رسد در زمان بمیر</p></div>
<div class="m2"><p>وز بعد مرگ اگر برسد دوست جان مخواه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طاووس همتی سر منقار تیز کن</p></div>
<div class="m2"><p>یعنی که بال و پر بکن وسایبان مخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مجلس بنوحه گرم کن از نی نوامجوی</p></div>
<div class="m2"><p>خنجر بسینه تیز کن از کس فسان مخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رو بیضه را بسنگ زن ای هدهد بهشت</p></div>
<div class="m2"><p>برشاخ سدره جا مکن و آشیان مخواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر کعبه ات بزیر لب آرند لب بدوز</p></div>
<div class="m2"><p>بر خاک بوسه زن ز حرم آستان مخواه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای مرغ سدره ، در طیران ابد بمان</p></div>
<div class="m2"><p>منشین بخاک طوبی و انس مکان مخواه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آهوی عصمت ار بگریزد ز عید گاه</p></div>
<div class="m2"><p>گیرایی از کمند و شتاب از عنان مخواه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر ناگهت بروی هوس دیده وا شود</p></div>
<div class="m2"><p>بهر خراش تیزی نوک سنان مخواه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا میزبانیت نکشد در خم غرور</p></div>
<div class="m2"><p>تنها بطرف سفره نشین میهمان مخواه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دنیای حلاوتی نرساند بکام کس</p></div>
<div class="m2"><p>این لقمه را مناسبتی با دهان مخواه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دستان زنی و بال گشایی چه دلگشاست</p></div>
<div class="m2"><p>از کبک طالع من و زاغ گمان مخواه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از من بگیر عبرت و کسب هنر مکن</p></div>
<div class="m2"><p>با بخت خود عداوت هفت آسمان مخواه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نام قبیله را مبر از فضل خود بعرش</p></div>
<div class="m2"><p>تا نفخ صور طنطنه دودمان مخواه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرفی چه احتیاج که گوید بداستان</p></div>
<div class="m2"><p>کین از فلان مجوی وز به همان فلان مخواه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لب بستن از طلب روش همت است و بس</p></div>
<div class="m2"><p>گفتم مخواه تن زن و صد داستان مخواه</p></div></div>