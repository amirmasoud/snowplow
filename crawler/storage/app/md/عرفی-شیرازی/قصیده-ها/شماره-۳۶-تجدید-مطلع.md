---
title: >-
    شمارهٔ ۳۶ - تجدید مطلع
---
# شمارهٔ ۳۶ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>زهی رمیده مرا آهوی وصال از دام</p></div>
<div class="m2"><p>چنانچه از نظرم خواب و از دلم آرام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوی او نفرستم پیام از آن ترسم</p></div>
<div class="m2"><p>که بر حکایت من مطلع شود پیغام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگاه عربده دشنام چون دهد سوزم</p></div>
<div class="m2"><p>مباد از لب او لذتی برد دشنام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه نازکی است که بینم بگاه جلوه قدش</p></div>
<div class="m2"><p>گرانی نظرم باز داردش ز خرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اضطراب دلم پای هوش میلغزد</p></div>
<div class="m2"><p>چو میرسد بخیال آن نهال سیم اندام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نیم جرعه چه شوراست در دلم گویی</p></div>
<div class="m2"><p>کزآن لب نمکین رشحه ای فتاده بجام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدور حسرت او جام زهر می نوشم</p></div>
<div class="m2"><p>گه از نصیحت خاص وگه از ملامت عام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ذوق کشتن عرفی بحیرتم که چرا</p></div>
<div class="m2"><p>چوکینه در دل بی مهر او گرفته مقام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زتازیانه جورش سمند صبر من است</p></div>
<div class="m2"><p>عنان فکنده چو فرمان شهریار انام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی وجود سخاوت مشخص از کف تو</p></div>
<div class="m2"><p>چنانچه ذات بصورت ، چنانچه شخص بنام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بود برات عطایت بدست هر فردی</p></div>
<div class="m2"><p>چو نامه های عمل در حسابگاه قیام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فشرده ذوق سخا در دل توپا محکم</p></div>
<div class="m2"><p>چو استقامت زر درخزینه های لئام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنای دولت خصم توست و بی بیناد</p></div>
<div class="m2"><p>چو دوستی هوسناک و اعتقاد عوام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بعهد عدل تو شاید که توأمان نشوند</p></div>
<div class="m2"><p>صبیه و صبی اندر مشیمه ارحام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوام جاه تو آن عالمی که دورش را</p></div>
<div class="m2"><p>ذخیره ابد آید بیک دقیقه تمام</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درون مطبخ جاه تو مهر و ماه بود</p></div>
<div class="m2"><p>دو قرص نان که یکی پخته است و دیگر خام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زبان حادثه تا کی قضا تواند بست</p></div>
<div class="m2"><p>اگر بحجت تیغ تو ندهدش الزام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز زخم نشتر فساد انتقام تو شد</p></div>
<div class="m2"><p>درون حادثه پرخون چو شیشه حجام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حروف قدر ترا صورت فلک جرمی است</p></div>
<div class="m2"><p>که عکس قاعده پایین فتاده در ارقام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بعهد عدل تو کز کحل حزم همچو غزال</p></div>
<div class="m2"><p>بخون گرگ سیاه است دیده اغنام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خلاف قاعده صیاد پیشگان شاید</p></div>
<div class="m2"><p>که پرورند بآهنگ صید باز ، حمام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شها ببزم تو چون این قصیده بر خوانم</p></div>
<div class="m2"><p>که ملک نظم ز فیضش گرفته است نظام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سزد بجایزه با جیب پر گهر گردون</p></div>
<div class="m2"><p>بدوشم افکند این جامه زمرد فام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا ، زدم عنکبوت پرده صبح</p></div>
<div class="m2"><p>بود لعاب لوامع تنیده بر ایام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بجای شربت مقصود جاه خصم ترا</p></div>
<div class="m2"><p>لعاب افعی تیغ تو باد اندر کام</p></div></div>