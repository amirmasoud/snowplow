---
title: >-
    شمارهٔ ۲ - بنام خدا
---
# شمارهٔ ۲ - بنام خدا

<div class="b" id="bn1"><div class="m1"><p>ای داشته در سایه هم تیغ و قلم را</p></div>
<div class="m2"><p>وی ساخته آرایش هم حلم و کرم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جم مرتبه ، داری زمان کز اثر نطق</p></div>
<div class="m2"><p>چون گل همگی گوش کند جذر اصم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این جام که از رای منیر تو فلک ساخت</p></div>
<div class="m2"><p>زودا که کند غنچه گل شهرت جم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک شیوه شناسد غضبت عفو ومکافات</p></div>
<div class="m2"><p>یک نغمه شمارد کرمت لا و نعم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاوید همی بخشد و از مایه نکاهد</p></div>
<div class="m2"><p>رشح قلمت ثروت اصناف امم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنجینه احسانش ، تنگ مایه نگردد</p></div>
<div class="m2"><p>گر تا ابد انعام دهد صفر رقم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ از شرف خاک درت ساخت طلسمی</p></div>
<div class="m2"><p>کز درگهت آنسو نبود راه قسم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگرفت ز انصاف تو در معرکه لاف</p></div>
<div class="m2"><p>شادی طرف شادی و غم جانب غم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بشنود از دهر که مردود کف تست</p></div>
<div class="m2"><p>بیرون فکند سکه ز آغوش ،درم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا گوهر ذاتت ز حوادث بشمردند</p></div>
<div class="m2"><p>صدگونه تملق زحدوث است قدم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آگه نیم از شبه تودانم که نزاد است</p></div>
<div class="m2"><p>دوشیزه ای از دوده شبه تو عدم را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از عدل توگر طبع چنین معتدل آید</p></div>
<div class="m2"><p>آن عهد رسد عالم فرتوت دژم را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کز گم شدگی درقلم وهم نماند</p></div>
<div class="m2"><p>امکان رقم صورت مفهوم هرم را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر جاه حسودت بهنر هندسی افتد</p></div>
<div class="m2"><p>در مرتبه نقصان رسد از صفر رقم را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بد خواه تو خوشدل که بوی چرخ بصلح است</p></div>
<div class="m2"><p>غافل که کشد آشتی گرگ غنم را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر تشنه که لب ماند بر او ، آب لبش ده</p></div>
<div class="m2"><p>از بسکه فشرده است کف جودتو، یم را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بسکه کف راد تو بی فاصله بخش است</p></div>
<div class="m2"><p>در جود تو ، نی راه بودبیش و نه کم را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست تو زبس الفتشان داد بیک جای</p></div>
<div class="m2"><p>درمنصب هم دخل بود تیغ و قلم را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنروز که ایثار شجاعت نگذارد</p></div>
<div class="m2"><p>بی بهره زتیغت مگرآهوی حرم را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر عطسه که از مغز کمان تو گشاید</p></div>
<div class="m2"><p>ریزد بگریبان بقا خون عدم را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آنجا که نهیب توبتب لرزه کشد عام</p></div>
<div class="m2"><p>اعمی متحرک نگرد نبض سقم را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سلطان غم از عدل توبگریخته بگذاشت</p></div>
<div class="m2"><p>در سینه اعدای تو اوتاد خیم را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از بسکه بود یاد تو در طینت اشیا</p></div>
<div class="m2"><p>نسیان تو شرمنده کند شهرت جم را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>افلاک در آغوش مشیت بنهادند</p></div>
<div class="m2"><p>از بیع تمنای تو قانون سلم را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در کارگه عدل تو از بس هنر آموخت</p></div>
<div class="m2"><p>عدل تو بفرزندی ، برداشت ستم را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از بسکه ز، رای توستد داروی صحت</p></div>
<div class="m2"><p>عیسی بطبابت بنشانید سقم را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ردمی کند اسباب هرم بخت تو ترسم</p></div>
<div class="m2"><p>کز زلف بت من برد آرایش خم را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از بسکه حسد جمع کند سینه خصمت</p></div>
<div class="m2"><p>از سینه افلاک برد گوی ورم را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خصمت چو ز روبه صفتی لابه گراید</p></div>
<div class="m2"><p>از سردی او تب شکند شیر اجم را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زد کوس حیات ابدی خصم توچون دید</p></div>
<div class="m2"><p>سرمایه هستی ز وجود تو عدم را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تقدیر پی کاهش اجزای وجودش</p></div>
<div class="m2"><p>اکسیر فنا داد گداز شکر غم را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رامشگر عدل تو صد آهنگ مخالف</p></div>
<div class="m2"><p>بنواز دونی کوک کند زیر نه بم را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>محویست عدیل توکه درگم شدن او</p></div>
<div class="m2"><p>دخلی نبود ماحی نسیان وعدم را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای آنکه در ایام ستایشگری تو</p></div>
<div class="m2"><p>صوفی شمرد عیب تگهبانی دم را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بخرام ونظر کن که بجولانگه مدحت</p></div>
<div class="m2"><p>حور قلمم زاده گلستان ارم را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مدح تو کجا باده نطقم بکف آرد</p></div>
<div class="m2"><p>آنجا اثر نوش بود نشئه سم را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>انصاف بده بوالفرج و انوری امروز</p></div>
<div class="m2"><p>بهر چه غنیمت نشمارند عدم را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بسم اله از اعجاز نفس جان دهشان باز</p></div>
<div class="m2"><p>تا من قلم اندازم و گیرند قلم را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اول ره این نظم خود ایشان بسپردند</p></div>
<div class="m2"><p>پس باز نمودیم بهم منزل هم را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بالله که نه لافو نه گزاف آیه صدقست</p></div>
<div class="m2"><p>حاسد بودآن کو، شمرد کذب قسم را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زین دست مرا داشتی آن عالم انصاف</p></div>
<div class="m2"><p>کز رحلت خود داد شرف ملک قدم را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>معیار سخن بود تو هم گنج تمیزی</p></div>
<div class="m2"><p>دیگر چه توان گفت ببین معجز دم را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چندان که درت را بود از نسبت من عار</p></div>
<div class="m2"><p>از نسبت من فخر بود ملک عجم را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>من مدحگرم لیک نه هرجایی و طامع</p></div>
<div class="m2"><p>گردن ننهم منت هر بذل و کرم را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دستان نزند بلبل من بر گل هر شاخ</p></div>
<div class="m2"><p>باید گل خورشید نه این صوت و نغم را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یک منعم و یک نعمت و یک منت و یک شکر</p></div>
<div class="m2"><p>صد شکر که تقدیر چنین رانده قلم را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر جاهلی آوازه دهد این چه ترانه است</p></div>
<div class="m2"><p>حاجت ببر از یاد چه بسیار و چه کم را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گویم که برو، ژاژ مخا ، باد مپیما</p></div>
<div class="m2"><p>این پایه مسلم نبود حاتم و جم را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>امکان بود امکان که همه عجز و نیاز است</p></div>
<div class="m2"><p>سرمایه فطرت چه سلاطین چه خدم را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سلطان و گدا در طلب جامه و نانند</p></div>
<div class="m2"><p>تا باز بگیرند جسد را و شکم را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ممکن هنرش چیست ز یک در طلبیدن</p></div>
<div class="m2"><p>عیبش چه بهر در شدن ایثار نعم را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>یارب مده این عیب که زحمت ندهم باز</p></div>
<div class="m2"><p>در زیور این زشت، براهین و حکم را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>عرفی همه لافی بدعا، تیز قلم شو</p></div>
<div class="m2"><p>بشتاب که میدان نشود تنگ رقم را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا از کشش خواهش آویزش مقصود</p></div>
<div class="m2"><p>طبع که بی جاده بود آزو کرم را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>در خواهش عمر تو ابد باد موله</p></div>
<div class="m2"><p>زآویزش عهد تو شرف باد قدم را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>صنعتگهشان چشم و دل خصم تو بادا</p></div>
<div class="m2"><p>تا صنعت تحلیل بود آتش و نم را</p></div></div>