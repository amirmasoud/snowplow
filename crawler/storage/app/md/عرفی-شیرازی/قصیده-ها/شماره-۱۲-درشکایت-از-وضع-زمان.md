---
title: >-
    شمارهٔ ۱۲ - درشکایت از وضع زمان
---
# شمارهٔ ۱۲ - درشکایت از وضع زمان

<div class="b" id="bn1"><div class="m1"><p>سری در عهد ما سامان ندارد</p></div>
<div class="m2"><p>کسی کو آب دارد نان ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منادی میزند در شش جهت بانگ</p></div>
<div class="m2"><p>که درد مفلسی درمان ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشیرینی سخاوت جان بود لیک</p></div>
<div class="m2"><p>کسی کو زر ندارد جان ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان عام است بی آبی در این عهد</p></div>
<div class="m2"><p>که بهرام آب در پیکان ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز قحط نان بمهمانی عیسی</p></div>
<div class="m2"><p>بجز یک نان فلک در خوان ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنر در نان کجا یابد که عیسی</p></div>
<div class="m2"><p>بگردون رفت و جز یک نان ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجو لؤلؤ که از بس تنگدستی</p></div>
<div class="m2"><p>خزف هم در صدف عمان ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حدیثم از زبان دیگران است</p></div>
<div class="m2"><p>ز من این گفتگو امکان ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان از بی زری شاد است عرفی</p></div>
<div class="m2"><p>که پنداری بزر ایمان ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه این تنگ عیشی ها زفسق است</p></div>
<div class="m2"><p>و گرنه بذل حق پایان ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>غلط شد راه نعمت خانه ورنه</p></div>
<div class="m2"><p>نعیم حق در و دربان ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیابی هیچ شیخ پاکدامن</p></div>
<div class="m2"><p>که داغ فسق در تنبان ندارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کدامین ساده زن بر فعل یابی</p></div>
<div class="m2"><p>که بر سر چادر دامان ندارد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنان بر خضر بوی می گذر بست</p></div>
<div class="m2"><p>که ره در چشمه حیوان ندارد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان گرمند در عصیان که دوزخ</p></div>
<div class="m2"><p>غم بیکاری شیطان ندارد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عمل این وانگهی لب نغمه پرداز</p></div>
<div class="m2"><p>که مسکین این ندارد ، آن ندارد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکافات عمل ارزاق خلق است</p></div>
<div class="m2"><p>هوای نفس قوت جان ندارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرا دستی نگهدارد زمانه</p></div>
<div class="m2"><p>که گر دل بشکند تاوان ندارد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدریا در مشو کامروز از آشوب</p></div>
<div class="m2"><p>جهان یک قطره بی طوفان ندارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیابان طی مکن کش هر بن خار</p></div>
<div class="m2"><p>کم از صد غول سرگردان ندارد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیابان چیست؟آن عهد دگر بود</p></div>
<div class="m2"><p>کدامین شهر غولستان ندارد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز نافرمانی و ناشکری خلق</p></div>
<div class="m2"><p>هزاران عید ، یک قربان ندارد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کسی کز بیم حق نعمت شناس است</p></div>
<div class="m2"><p>بدست از شکر جز دستان ندارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لبی در شکر جنباند، نداند</p></div>
<div class="m2"><p>که منعم نعمت ارزان ندارد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>معاصی باعث خذلان روح است</p></div>
<div class="m2"><p>در این معنی کسی کتمان ندارد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیاید ترک این اعمال زنهار</p></div>
<div class="m2"><p>که روح آسایش از خذلان ندارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسی کو داند و مغلوب نفس است</p></div>
<div class="m2"><p>زمردم عیب خود پنهان ندارد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که دشمن چون بطعنش لب گشاید</p></div>
<div class="m2"><p>همان نفسش ز کبر انسان ندارد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی کو داند و ترکش تواند</p></div>
<div class="m2"><p>ولی آهنگ ترک آن ندارد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگرمؤمن بود زنجیر و قلاب</p></div>
<div class="m2"><p>و گر کافر به بت ایمان ندارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کسی کو ترک گیرد گر بداند</p></div>
<div class="m2"><p>همانا، ایزدش، حیران ندارد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کسی کو نه بداند نه تواند</p></div>
<div class="m2"><p>بمعشوق ازل ، پیمان ندارد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همین گفتن نکو آید زعرفی</p></div>
<div class="m2"><p>نکو بشنو که گوش آن ندارد</p></div></div>