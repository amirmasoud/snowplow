---
title: >-
    شمارهٔ ۳۸ - در منقبت علی (ع)
---
# شمارهٔ ۳۸ - در منقبت علی (ع)

<div class="b" id="bn1"><div class="m1"><p>چون گرد باد آه ز خاکم کشد علم</p></div>
<div class="m2"><p>بر فرق روزگار فشاند غبار غم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دل بجای خویش بود کز نهیب درد</p></div>
<div class="m2"><p>زین آشیانه طایر آرام کرده رم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عهد من زدهر مجو خوش دلی که هست</p></div>
<div class="m2"><p>در شیشه زمانه وجودم جهان غم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طور وعده تو فراموشی وفا</p></div>
<div class="m2"><p>وی طرز غمزه تو هم آغوشی ستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق غم توشانه کش طره طرب</p></div>
<div class="m2"><p>شوق لب تو سرشکن شحنه الم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از وعده تو شوق بتشویش مبتلا</p></div>
<div class="m2"><p>وز عشوه تو فتنه به آشوب متهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخشد هزار کشته چشم ترا حیات</p></div>
<div class="m2"><p>لعلت لطیفه ای که برون آرد از عدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گیرد بهر دو دست سر خود اجل زبیم</p></div>
<div class="m2"><p>جایی که غمزه تو کشد خنجر ستم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لعل حیات بخش تو جایی که دم زند</p></div>
<div class="m2"><p>نبود مسیح را زخجالت مجال دم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زاعجاز حسن توست که کلک قضا نسوخت</p></div>
<div class="m2"><p>برلعل آتشین خط سبزت چو زد رقم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هم خود بگو روا بود ای بی وفا که من</p></div>
<div class="m2"><p>محروم باشم از تو و اغیار محترم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>محرم ببزم وصل تو غیر و مرا زبیم</p></div>
<div class="m2"><p>مرغ امید پر نزند گرد آن حرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دست افکنی بدوش رقیبان بر غم من</p></div>
<div class="m2"><p>وز چنگ من برون کشی آن زلف خم بخم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من جان دهم برای تو آن لعل روح بخش</p></div>
<div class="m2"><p>از معجز مسیح زند با رقیب دم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با دوستان بکینی و با دشمنان بمهر</p></div>
<div class="m2"><p>منبعد اگر سلوک تو این است لاجرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خواهم شدن بمحکمه عدل تا شود</p></div>
<div class="m2"><p>طبع سلیم عادل شاه جهان حکم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سلطان دین وصی نبی قهرمان شرع</p></div>
<div class="m2"><p>شاه نجف علی ولی معدن کرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن واهب النعم که ز داوود نطق او</p></div>
<div class="m2"><p>نشنید گوش آز بجز نغمه نعم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اول به آب چشمه کوثر وضو کند</p></div>
<div class="m2"><p>جبریل گر بخاک جنابش خورد قسم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عزم طواف کعبه زکویش چنان بود</p></div>
<div class="m2"><p>کایند از برای تیمم برون زیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اندوزد از عبادت یزدان عدوی تو</p></div>
<div class="m2"><p>اجری که برهمن برد از طاعت صنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از قدر خواستم که فلک خوانمش ، قضا</p></div>
<div class="m2"><p>گفت ای بری ز شیوه تمیز مدح وذم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>او را سپهرگویی و این ننگری که هست</p></div>
<div class="m2"><p>او منبع عطوفت و این مصدر ستم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مشاطه ولایش اگر زیب گر شود</p></div>
<div class="m2"><p>ز اعجاز عیسوی کند آرایش صنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای طوف بارگاه تو پیرایه شرف</p></div>
<div class="m2"><p>وی دودمان جاه تو همسایه قدم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در باغ فطرت تو مسیحا چویک نسیم</p></div>
<div class="m2"><p>در فوج حشمت تو سلیمان چویک خدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مست غرور کرد عروسان خلد را</p></div>
<div class="m2"><p>دعوی باغ لطف تو با روضه ارم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرگز زمین رزم تو از خون نگشت خشک</p></div>
<div class="m2"><p>از بسکه خنجر تو رسانید نم بنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن کینه پروری که ز بغض تو دم زند</p></div>
<div class="m2"><p>وآن خون گرفته ای که بکینت کشد علم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با تیغ روزگار کند قصد کارزار</p></div>
<div class="m2"><p>با قهر کردار بمیدان نهد قدم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر شامگه که از اثر مهر خاوری</p></div>
<div class="m2"><p>رنگ بقم گرفته سپهر جفا رقم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون سرکشی بحکم تواندیشه کرده است</p></div>
<div class="m2"><p>خونش فکنده بیم سنان تو در شکم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حفظ تو گر ستون نشود برهم اوفتد</p></div>
<div class="m2"><p>از تند باد حادثه این نیلگون خیم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاها منم که درد و غم و غصه متصل</p></div>
<div class="m2"><p>آیندم از قفا چو سپاه از پی علم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا برکنار خوان وجود است جای من</p></div>
<div class="m2"><p>پرورده روزگار مرا از نعیم غم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هرجا غمی است کرده بتحویل من مگر</p></div>
<div class="m2"><p>ازبهر دیگران بمن اکنون کند رقم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عرفی شکایت تو نهایت پذیر نیست</p></div>
<div class="m2"><p>این قصه را بیا ، بدعا ساز مختتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا خامه خیال که نقاش معنویست</p></div>
<div class="m2"><p>مدح تو بر صحیفه هستی کند رقم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خصمت که هست صورت عصیان همیشه باد</p></div>
<div class="m2"><p>گریان و بی‌قرار و نگونسار چون قلم</p></div></div>