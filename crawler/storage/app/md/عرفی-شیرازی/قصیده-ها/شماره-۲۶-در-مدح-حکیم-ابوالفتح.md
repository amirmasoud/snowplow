---
title: >-
    شمارهٔ ۲۶ - در مدح حکیم ابوالفتح
---
# شمارهٔ ۲۶ - در مدح حکیم ابوالفتح

<div class="b" id="bn1"><div class="m1"><p>چهره پرداز جهان رخت کشد چون بجمل</p></div>
<div class="m2"><p>شب شود نیمرخ و روز شود مستقبل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم شب تنگ شود دایره مردمکش</p></div>
<div class="m2"><p>دیده روز بتدریج برآید احول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم دیده آن ژاله و گرما بصفت</p></div>
<div class="m2"><p>بیضه دیده این روغن و دیبا بمثل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون سودایی شب زائد و فاسد گردد</p></div>
<div class="m2"><p>لاجرم نشتر روزش بگشاید اکحل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز چون کرم بریشم همه بر خویش تند</p></div>
<div class="m2"><p>هر چه شب رد کند از معده چو زنبور عسل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از این ترجمه روز شود صاحب کل</p></div>
<div class="m2"><p>بعد از این شب بنگین نقش کند عبداقل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وقت آنست کنون کز اثر عیش و نشاط</p></div>
<div class="m2"><p>می نگنجد بصراحی و صراحی ببغل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جام یاقوت و می لعل بهم پالاید</p></div>
<div class="m2"><p>اثر نامیه چون لاله و داغش بمثل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامیه چون چمن سبزه دهد اتمامش</p></div>
<div class="m2"><p>ناقص از کارگه آرند بباغ از مخمل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عرق از شبنم گل داغ شود بر رخ حور</p></div>
<div class="m2"><p>اخگر از فیض هوا سبز شود در منقل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چمن آید بچمن بهر تماشای جمال</p></div>
<div class="m2"><p>بلبل آید بر بلبل بتمنای غزل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیرد از فیض هوا طبع جواهر دارو</p></div>
<div class="m2"><p>خصمت ار سوده الماس کند در مکحل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسکه هر خار گلی کرده عجب نیست اگر</p></div>
<div class="m2"><p>یاسمین بشکفد از نشتر زنبور عسل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیش باغ و چمن دهر کنون گر رضوان</p></div>
<div class="m2"><p>نسخه خلد برین باز گشاید بمثل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صورت خلد ازین باغ مفصل یابد</p></div>
<div class="m2"><p>سیرت این چمن از خلد ببیند مجمل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حور گیسو بمیان بسته درآید بچمن</p></div>
<div class="m2"><p>تا لبالب کند از سنبل وگل جیب و بغل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بسکه از سنبل وگل یافت صفا نزدیکست</p></div>
<div class="m2"><p>کز پی بوسه دو لب را بهم آرد جدول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شاید ار عذر پرستار پذیزند بحشر</p></div>
<div class="m2"><p>بسکه برداشت صفا صورت عزی و هبل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>انبساطی است در این فصل که بی کاوش عقل</p></div>
<div class="m2"><p>شاید ار باز شود عقده ما لاینحل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیلی از گوشه محمل بنمود است جمال</p></div>
<div class="m2"><p>یا بود لاله که سر برزده از دامن تل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حاسد آزار شوم زین غزل تازه که باز</p></div>
<div class="m2"><p>موسم شادی بلبل شد و اندوه جعل</p></div></div>