---
title: >-
    شمارهٔ ۴۶ - حکمت و موعظت
---
# شمارهٔ ۴۶ - حکمت و موعظت

<div class="b" id="bn1"><div class="m1"><p>عادت عشاق چیست مجلس غم داشتن</p></div>
<div class="m2"><p>حلقه شیون زدن ماتم هم داشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر عمان درد موج حلاوت زدن</p></div>
<div class="m2"><p>بر در میدان دل فوج ستم داشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حمد غم و نعت درد بر لب دل دوختن</p></div>
<div class="m2"><p>شهر دل و باغ جان وقف الم داشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نغمه داود را از لب شیون زدن</p></div>
<div class="m2"><p>آتش نمرود را باغ ارم داشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خط آزادگی بندگی آموختن</p></div>
<div class="m2"><p>با دل بی آرزو چشم کرم داشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ابدی ذوق غم روی زیان تافتن</p></div>
<div class="m2"><p>وز ازلی بیع درد سود سلم داشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن عبادات را برقع نسیان زدن</p></div>
<div class="m2"><p>زشتی اعمال را لوح و قلم داشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آینه دیده را صیقل حیرت زدن</p></div>
<div class="m2"><p>زاویه سینه را مخزن غم داشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هم ز غبار کنشت عطر کفن ساختن</p></div>
<div class="m2"><p>هم بترازوی دیر سنگ حرم داشتن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در دهن بخت عیش ناوک لا ریختن</p></div>
<div class="m2"><p>در کمر درس عشق دست نعم داشتن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا به ثری آب چشم از پی هم ریختن</p></div>
<div class="m2"><p>تا بفلک داغ دل بر سر هم داشتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در جگر اشتها آب هوس سوختن</p></div>
<div class="m2"><p>وز اثر امتلا درد شکم داشتن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مستی و دیوانگی جام مسیحا شکست</p></div>
<div class="m2"><p>صرفه در این بزم نیست ساغر جم داشتن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دین و دل و عمر جان جمله بسیلاب ده</p></div>
<div class="m2"><p>دشمن درویشی است خیل و حشم داشتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خامه تراشی ستم نامه تراشی گناه</p></div>
<div class="m2"><p>ساده و بی زخم به لوح و قلم داشتن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شیب نگویم بطبع به زشباب است لیک</p></div>
<div class="m2"><p>به ز رعونت بود قامت خم داشتن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهر نعیم بهشت طاعت ایزد مکن</p></div>
<div class="m2"><p>بر لب جیحون خطاست چشم بنم داشتن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با صنم آمیختن کفر ادب دان ولی</p></div>
<div class="m2"><p>شرط بود در میان فاصله کم داشتن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رهروی راه عشق بر تو شمارم که چیست</p></div>
<div class="m2"><p>گام بفرسخ زدن پاس قدم داشتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رو بقفا کن ببین عمر تلف کرده را</p></div>
<div class="m2"><p>تا بتو روشن شود روبعدم داشتن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چند بتزویر و فن پرده کشیدن بعیب</p></div>
<div class="m2"><p>صورت مدح آمدن معنی ذم داشتن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عدل وکرم خسروی است ورنه گدایی بود</p></div>
<div class="m2"><p>بهر دو ویرانه ده طبل و علم داشتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صرفه زبانم ببست ورنه بشه گفتمی</p></div>
<div class="m2"><p>کز دل درویش پرس ذوق ستم داشتن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دم مزن از جور چرخ ز آنکه نه آزادگی است</p></div>
<div class="m2"><p>زو متأثر شدن بس گله هم داشتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زین ره کثرت اساس بگذروآنگه بین</p></div>
<div class="m2"><p>مالک وحدت شدن ، ملک قدم داشتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نسخه این باغ رازیروزبرکن بس است</p></div>
<div class="m2"><p>از سر گل تا بکی منت شم داشتن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مایه نازندگی از گهر خویش گیر</p></div>
<div class="m2"><p>تا بکی این عزوناز ازاب وعم داشتن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مذهب عرفی بگیر ملت قارون بهل</p></div>
<div class="m2"><p>گنج هنر ریختن به ز درم داشتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اوست مسیحای وقت لیک مسیحا که هست</p></div>
<div class="m2"><p>دون اثرهای او معجز دم داشتن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تیغ زبانش فکند برسر هم مهروماه</p></div>
<div class="m2"><p>شهرت او را جلال ملک عجم داشتن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طی کنم این نامه را گر نکنم چون کنم</p></div>
<div class="m2"><p>حوصله خامه نیست تاب رقم داشتن</p></div></div>