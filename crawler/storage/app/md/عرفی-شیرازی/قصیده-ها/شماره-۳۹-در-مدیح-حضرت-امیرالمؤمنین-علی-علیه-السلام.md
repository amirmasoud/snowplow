---
title: >-
    شمارهٔ ۳۹ - در مدیح حضرت امیرالمؤمنین علی علیه السلام
---
# شمارهٔ ۳۹ - در مدیح حضرت امیرالمؤمنین علی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای مرتفع ز نسبت جود تو شان علم</p></div>
<div class="m2"><p>کلک گهر فشان تور طب اللسان علم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ساکنان مصر معانی بحسن عقل</p></div>
<div class="m2"><p>نادیده یوسفی چو تو درکاروان علم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطان دین علی که زشست کمال اوست</p></div>
<div class="m2"><p>هرناوکی که یافت گشاد از کمان علم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جیب و کنار عقل ز گوهر لبالب است</p></div>
<div class="m2"><p>تا باز کرده ای لب گوهر فشان علم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلک نقود نظم جواهر بباد رفت</p></div>
<div class="m2"><p>تا صیت گوهر تو برآمد زکان علم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش از وجود صلب فلک بود ذات تو</p></div>
<div class="m2"><p>در بطن صنع نادره زا توأمان علم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امکان اگر نه تکیه زدی بر وجود تو</p></div>
<div class="m2"><p>کی داشتی تحمل بار گران علم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست مجردات ستون ز نخ شود</p></div>
<div class="m2"><p>آنجا که فطرت تو زند سایبان علم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>علم است جان هر که بود معنوی نهاد</p></div>
<div class="m2"><p>الافطانت تو که گردید جان علم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذات تو اعتدال و سلیمان مزاج عدل</p></div>
<div class="m2"><p>عقل تو مغز و جوهر کل استخوان علم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدره فتد بچاه ضلالت بهر قدم</p></div>
<div class="m2"><p>دست هدایت ار نکنی بر میان علم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر گوش فطرت تو زاول نفس شمرد</p></div>
<div class="m2"><p>هر نکته ای که داشت لب داستان علم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنجا که دانش تو نهد رسم تقویت</p></div>
<div class="m2"><p>ای آیت شعور تو نازل بشان علم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دست ضعیف جهل که درآستین شکست</p></div>
<div class="m2"><p>از عقل اولین بر باید عنان علم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برآسمان علم ضمیر تو آفتاب</p></div>
<div class="m2"><p>اما مسیر تو نهمین آسمان علم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن مایه دشمنی که بعلم است جهل را</p></div>
<div class="m2"><p>ای کعبه وجود تو دارالامان علم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر ضمیر جوهر اول شدی تباه</p></div>
<div class="m2"><p>تقدیر هستیت نشدی گر ضمان علم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ارزان متاع روی دکان کنه هستی است</p></div>
<div class="m2"><p>آنجا که فطرت توگشاید دکان علم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا عزم خاکبوس حریم فطانتت</p></div>
<div class="m2"><p>دارند ساکنان نهم آسمان علم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بیم دور باش ادب هر صباح و شام</p></div>
<div class="m2"><p>صد بوسه برده بر لب روحانیان علم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر صنع ایزدی ز ازل مصلحت نداشت</p></div>
<div class="m2"><p>تا سازد امتیاز تو خاطر نشان علم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>الا درآستان حریم فطانتت</p></div>
<div class="m2"><p>ذیل ملازمت نزدی بر میان علم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزی ز روی نسبت اجزای یکدیگر</p></div>
<div class="m2"><p>ترتیب دادمی بتصور جهان علم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در دل فتاد سایه طبع بلند او</p></div>
<div class="m2"><p>گفتم که این سزد بصفت آسمان علم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر سایه طبیعت تو مهبطیش هست</p></div>
<div class="m2"><p>آن ذروه می سزد که شود لامکان علم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاها تویی که فیض هوای طبیعتت</p></div>
<div class="m2"><p>سازد بنوبهار مبدل خزان علم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از دست پخت طبع تو با لذت است و بس</p></div>
<div class="m2"><p>بر خوان عقل هر که شود میهمان علم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دارم امید آنکه بعرفی زعین لطف</p></div>
<div class="m2"><p>بخشی وظیفه ای ز نعیم جنان علم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در مجمعی که قوت معنی دهی بفیض</p></div>
<div class="m2"><p>دستم ز آستین بفرستی بخوان علم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مسند نشین خاک در دانشش کنی</p></div>
<div class="m2"><p>ای فضل مایه بخش تو سلطان نشان علم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>با آنکه دست بسته میدان دانشم</p></div>
<div class="m2"><p>گر نامزد کنی بکف من عنان علم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون دانه های گوهر مدحتت بسلک نظم</p></div>
<div class="m2"><p>سرهای خیل راز کشم بر سنان علم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا دل شکاف جهل بسیط و مرکب است</p></div>
<div class="m2"><p>زخم دلیل قطعی و تیغ زبان علم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بادا هدایت تو که معمار دانش است</p></div>
<div class="m2"><p>تیغ زبان جوهریان را فسان علم</p></div></div>