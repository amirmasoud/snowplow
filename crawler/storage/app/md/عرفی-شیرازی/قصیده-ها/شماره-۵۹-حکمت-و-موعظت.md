---
title: >-
    شمارهٔ ۵۹ - حکمت و موعظت
---
# شمارهٔ ۵۹ - حکمت و موعظت

<div class="b" id="bn1"><div class="m1"><p>بسعی جوهر اندیشه راز دین مگشای</p></div>
<div class="m2"><p>کلید موم و سرقفل آهنین مگشای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهشت راز مقام دراز دستان نیست</p></div>
<div class="m2"><p>در مشاهده بر روی میوه چین مگشای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مثال علم لدنی گرت زخامه چکد</p></div>
<div class="m2"><p>جمال ظن منما چهره یقین مگشای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهمنشین مگشا راز دل ، نه بیگانه</p></div>
<div class="m2"><p>وگر ملازم طبع است همنشین مگشای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنوز در رحم است آنکه طبع دایه اوست</p></div>
<div class="m2"><p>بروی سر ازل دیده جنین مگشای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن گره که نهد بر دلت نهفتن راز</p></div>
<div class="m2"><p>بکاوش نفس تیز واپسین مگشای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان و هرچه در اوهست سر بسردربند</p></div>
<div class="m2"><p>در معارضه با حکمت آفرین مگشای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهشت ما حضرخوان تنگ عیشان است</p></div>
<div class="m2"><p>باینقدر زجبین نیاز چین مگشای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خدنگ طعنه همت نشانه میطلبد</p></div>
<div class="m2"><p>مشبک مژه بر روی حور عین مگشای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر بکیش مروت عمل کنی زنهار</p></div>
<div class="m2"><p>گره زکار دل عافیت گزین مگشای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر دلت زخرابی عافیت تنگ است</p></div>
<div class="m2"><p>هزار گونه عمارت بهل همین مگشای</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>براه ملک قدم میروی بسعی حدوث</p></div>
<div class="m2"><p>بتاز و دیده بد و نان همنشین مگشای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دریچه ای که غمی سر برون نیارد از آن</p></div>
<div class="m2"><p>بروی صرفه کار دل حزین مگشای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محل شناس طرب باش یعنی آن ساعت</p></div>
<div class="m2"><p>که گرد غم ننشیند برخ جبین مگشای</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بطرف چشمه کوثر چو تشنه لب برسی</p></div>
<div class="m2"><p>فرو میای گر آیی زرخش زین مگشای</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر تو مرد رهی زحمت وجود مبر</p></div>
<div class="m2"><p>ز آسمان درتشنیع بر زمین مگشای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زجان و دل بگشا عقده ای که فرصت رفت</p></div>
<div class="m2"><p>گره ز رشته اسرار ماء وطین مگشای</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدست دل بگشا قفل معنی از در جان</p></div>
<div class="m2"><p>هرآن دری که بود بسته غیر از این مگشای</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلی که باید از افتادگی گشاده شود</p></div>
<div class="m2"><p>ببر فشاندن دامان و آستین مگشای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دلی که صحبت عشق است مایه طربش</p></div>
<div class="m2"><p>بنظم و نثر مکن خوش نهاد و هین مگشای</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زآب و رنگ چه خیزد بغنچه لاله</p></div>
<div class="m2"><p>بگو که بند قبا پیش یاسمین مگشای</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بتیغ غمزه جانان گشای پهلوی دل</p></div>
<div class="m2"><p>دلی که در غم او تنگ شد چنین مگشای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>متاع دل که نباید گشود جز بر دوست</p></div>
<div class="m2"><p>اگر بهاش سلیمان دهد نگین مگشای</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنای عفو بر الطاف دوست نه، نه زمان</p></div>
<div class="m2"><p>در شهور مزن غرفه سنین مگشای</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بمشت خاک نیرزد ولایت دارا</p></div>
<div class="m2"><p>دلی گشای که فتح است و ملک چین مگشای</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زشیخ و راهب اگر استماع میطلبی</p></div>
<div class="m2"><p>ز نیک و زشت بگو لب بفکر دین مگشای</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>لب صفا بگشا در بیان ساده دلی</p></div>
<div class="m2"><p>زبان عقل بتشریح مهر و کین مگشای</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیان وحدت و تفسیر آیت توحید</p></div>
<div class="m2"><p>زبان بوقلمون را بآن و این مگشای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هزار مرده بروی زمین بود، هشدار</p></div>
<div class="m2"><p>اگر تومرده ندیدی دل زمین مگشای</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زبخل صاحب خرمن نصیحت است این حرف</p></div>
<div class="m2"><p>که مرحمت کن و دامان خوشه چین مگشای</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زهر سخن در بازیچه ای فراز کنم</p></div>
<div class="m2"><p>بزاده خردم چشم هزل بین مگشای</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خموش عرفی از این نغمه های شورانگیز</p></div>
<div class="m2"><p>لب ترانه بلبل به آفرین مگشای</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>رموز حکمت اسرار قدس جلوه دهد</p></div>
<div class="m2"><p>به مدح خویش لب عقل اولین مگشای</p></div></div>