---
title: >-
    شمارهٔ ۴۲ - تفاخر شاعرانه
---
# شمارهٔ ۴۲ - تفاخر شاعرانه

<div class="b" id="bn1"><div class="m1"><p>ای طعن فلک نوشته برسم</p></div>
<div class="m2"><p>وی زلف صبا بریده از دم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای در بر توسن فلک شوخ</p></div>
<div class="m2"><p>ز آنگونه که پیش شعله هیزم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر غنچه سبکروی وی بدانسان</p></div>
<div class="m2"><p>کش خنده نزاید از تبسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نازی بلب فسانه پرداز</p></div>
<div class="m2"><p>ز آنگونه که نشکنی تکلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گام شمرده خط نگاری</p></div>
<div class="m2"><p>بر نقطه نوک نیش کژدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد از تو شتاب وام وز آن کرد</p></div>
<div class="m2"><p>سیمرغ وجود خویش را گم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هشتم فلکی و ذو ذوابه</p></div>
<div class="m2"><p>چون وقت روش علم کنی دم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زآن راست روی که طبع عرفی</p></div>
<div class="m2"><p>راندت بمسالک تعلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اول قدم ریاض طبعش</p></div>
<div class="m2"><p>آخر چمن بهشت هشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی فیض قبول آسمان بود</p></div>
<div class="m2"><p>جامی تهی از شراب صد خم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ننشست مگر بوقت خوابش</p></div>
<div class="m2"><p>دریای معانی از تلاطم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درهم شکند بگاه حمله</p></div>
<div class="m2"><p>صد فوج معانی از تصادم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون آتش طبع بر فروزد</p></div>
<div class="m2"><p>طوبی طلبد بجای هیزم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ابره اطلس فلک دوخت</p></div>
<div class="m2"><p>رایش زبیاض صبح قاقم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رضوان زپی شراب بزمش</p></div>
<div class="m2"><p>انگور ، بپرورد بطارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بر خاک در طبیعت او</p></div>
<div class="m2"><p>دریای محیط در تیمم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گردون بنظاره ضمیری</p></div>
<div class="m2"><p>یک دیده و آفتاب مردم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازآب سخاش خوشه برداشت</p></div>
<div class="m2"><p>نوک مژه چون درخت گندم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عرفی بمدیح خود شتابی</p></div>
<div class="m2"><p>هشدار مباد، ره کنی گم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داد سخنت بده که مردند</p></div>
<div class="m2"><p>معنی و عبارت از تظلم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هان شرم مکن ثنای خودگو</p></div>
<div class="m2"><p>گوباش حسود در تبسم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شایسته تویی به مدح امروز</p></div>
<div class="m2"><p>ای خاک رهت به فرق مردم</p></div></div>