---
title: >-
    شمارهٔ ۴۳ - چیستان در وصف شمع
---
# شمارهٔ ۴۳ - چیستان در وصف شمع

<div class="b" id="bn1"><div class="m1"><p>چیست آن جوهر هدایت فن</p></div>
<div class="m2"><p>آسمان مولد و زمین مسکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوخ آیینه روی روشندل</p></div>
<div class="m2"><p>رند ژولیده موی تر دامن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوزش در حراست رشته</p></div>
<div class="m2"><p>رشته اش درسیاست سوزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردنش تا بفرق سیمابی</p></div>
<div class="m2"><p>سیم ساق است پای تا گردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون عروسان هند دردم رقص</p></div>
<div class="m2"><p>ازدم گیسویش چکد روغن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زر قلب شاهد دنیا</p></div>
<div class="m2"><p>چهره تاریک و برقعش روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوزد باد ، لاله حمر است</p></div>
<div class="m2"><p>بوزد ، هست غنچه سوسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیمیایی است گوهر تاجش</p></div>
<div class="m2"><p>که ازو زر شود مس و آهن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عزت تاج او بیفزاید</p></div>
<div class="m2"><p>جلوه طلعت سهیل یمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوهر هیکلش هیولایی است</p></div>
<div class="m2"><p>در قبول صور چو جوهر ظن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جامه اش گاه سبز و گاه سپید</p></div>
<div class="m2"><p>چهره اش روز تیره شب روشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیسویش نورباف چون مریم</p></div>
<div class="m2"><p>ابرویش چون هلال چشمک زن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم زباد صبا شود جوزا</p></div>
<div class="m2"><p>هم زبرف صفا سهیل یمن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ماهتابی است بر درفش کیان</p></div>
<div class="m2"><p>آفتابش چه تیروچه بهمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قصب ماهتاب او ، اکسون</p></div>
<div class="m2"><p>شرف آفتاب او ایمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گه گهی از میان تاج خروس</p></div>
<div class="m2"><p>برفشاند بفرق خود ارزن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زندگانیش مردن شبگیر</p></div>
<div class="m2"><p>دیده بانیش کوری رهزن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دسته هاون طلاست ولی</p></div>
<div class="m2"><p>سوده آن سر که نیست در هاون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زاهد آسا زدانه های سرشک</p></div>
<div class="m2"><p>سبحه آویخته است در گردن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هم شکفته ست درمصیبت و سور</p></div>
<div class="m2"><p>هم برهنه است در دی و بهمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شاه تیر جهاز زرین است</p></div>
<div class="m2"><p>بر سرش موج نور سایه فکن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>راز دل بر زبان چو میآرد</p></div>
<div class="m2"><p>مستفیدند زیرک و کودن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بخلوت زبان بجنباند</p></div>
<div class="m2"><p>راز بیرون فشاند از روزن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>معنیش روح موسی عمران</p></div>
<div class="m2"><p>صورتش نخل وادی ایمن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صوفیان گرد او نشسته بذوق</p></div>
<div class="m2"><p>همه سبوح گوی و یا رب زن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>روز بر هم فشرده مژگان لیک</p></div>
<div class="m2"><p>شب گشاده ست دیده روشن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون شکر مشربان هندستان</p></div>
<div class="m2"><p>کله زر تار و چرب پیراهن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون بمیرد تنش نفرساید</p></div>
<div class="m2"><p>زنده گردد بکاهش سروتن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با همه حدت و حرارت طبع</p></div>
<div class="m2"><p>دامنش پرشود زآب دهن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرمن از سنگ آس گر باشد</p></div>
<div class="m2"><p>بر زبان آرد می کند خرمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پدرش مهر و مادرش مه لیک</p></div>
<div class="m2"><p>شب گشاده است دیده روشن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزبان خندد ، از گلو گرید</p></div>
<div class="m2"><p>خنده تا فرق ، گریه تا دامن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گریه از شوق دیدن خورشید</p></div>
<div class="m2"><p>خنده از عیش بزم شاه زمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شاخ گندم که دیده خوشه زر</p></div>
<div class="m2"><p>اینک از بزم شه ببین روشن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گریه و خنده اش گدازش عمر</p></div>
<div class="m2"><p>همچو اعدای شاه قلب شکن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جوهرش در حریم خاطر شاه</p></div>
<div class="m2"><p>ماه نخشب بود چه بیژن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همچو انگشت پنجه خورشید</p></div>
<div class="m2"><p>صد اشارت کند بشاه زمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاه اکبر که هست ترکیبش</p></div>
<div class="m2"><p>نور خورشید سایه ذوالمن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاه چین و حبش غلام تواند</p></div>
<div class="m2"><p>دور زین آستان اسیر محن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زآن نوشته است عبده بفداه</p></div>
<div class="m2"><p>بدیار تو ملک چین و ختن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بلبل باغ عمر دشمن تو</p></div>
<div class="m2"><p>نزند نغمه ای بجز شیون</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرغ جاهش بزیر شهپر حکم</p></div>
<div class="m2"><p>از بدخشان گرفته تا ، بدکن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بگذراند چو رشته حکمش</p></div>
<div class="m2"><p>آسمان را زچشمه سوزن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عدل او را بعدل نوشروان</p></div>
<div class="m2"><p>کی بسنجد سپهر نادره فن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>این بسنجد کسی که نشناسد</p></div>
<div class="m2"><p>صافی جام جم ز دردی دن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نطفه دشمنش بصلب پدر</p></div>
<div class="m2"><p>داده پیوند تار و پود کفن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تا ارادی بود جفای سفر</p></div>
<div class="m2"><p>تا طبیعی بود هوای وطن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>وطنم آستان جاه تو باد</p></div>
<div class="m2"><p>تا نکرده است جان سفر ز بدن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خاطرش بحر فیض را معبر</p></div>
<div class="m2"><p>گوهرش سر غیب را معدن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر که را لطف او حیات دهد</p></div>
<div class="m2"><p>نوبت جامه کی رسد بکفن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نصب را روی بخت او مرآت</p></div>
<div class="m2"><p>عزل را بخت خصم او مدفن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ای غبار حریم حرمت تو</p></div>
<div class="m2"><p>عطر پیراهن عروس چمن</p></div></div>