---
title: >-
    شمارهٔ ۱۶ - تجدید مطلع
---
# شمارهٔ ۱۶ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>زهی لوای نبوت ز نسبتت منصور</p></div>
<div class="m2"><p>مزاج عشق زآمیزش دلت رنجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنور و سایه چو امر سکون و سیر کنی</p></div>
<div class="m2"><p>زمانه فاصله یابد میان سایه و نور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بباغ طبع تو بر اوج استفاده فیض</p></div>
<div class="m2"><p>همای عقل طلبکار سایه عصفور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هدایت تو نماید بچشم صورت بین</p></div>
<div class="m2"><p>هرآنچه در حرم ایزدی بود مستور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نور ناصیه ات ماه گر ضیا گیرد</p></div>
<div class="m2"><p>به آفتاب دهد نسخه سنین و شهور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن نفس که برون داده اند گوهر تو</p></div>
<div class="m2"><p>بگنج صنع نمانده تعلق گنجور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعاع شعله قهر تو گرفتد بسحاب</p></div>
<div class="m2"><p>رماد برق شود سرمه صبا و دبور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه هست مبرهن که درمسیر وجود</p></div>
<div class="m2"><p>مؤثرند صفات اله بی مأثور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عداوت تو مبادا که از تأثر آن</p></div>
<div class="m2"><p>مزاج حلم خداوند میشود محرور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اجل رسید چو نامت بجبهه بنویسد</p></div>
<div class="m2"><p>خجل شود زنگه گردنش اجل از دور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سرکلاه حکومت بدامن تو نهاد</p></div>
<div class="m2"><p>قضا که هست دو عالم بحکم او مجبور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که این کلاه بسرمان و گوشه برشکنش</p></div>
<div class="m2"><p>که درد وکون تویی آمر و منم مأمور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعهد حکم تو امر قضا چنان منسوخ</p></div>
<div class="m2"><p>که از نزول کلام مجید حکم زبور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر ز روی ضمیرت نقاب برخیزد</p></div>
<div class="m2"><p>برنگ سایه شود آفتاب چشمه نور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شها تویی که زکات بضاعت کرمت</p></div>
<div class="m2"><p>دوکون را زگرانمایگی کند معمور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منم که کرده ام از ننگ شرکت نوعی</p></div>
<div class="m2"><p>نصیب فرقه انسان هزارگونه قصور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز روزگار من آثار یأس می تابد</p></div>
<div class="m2"><p>چو حالت سنوات از مآثر مأجور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تنزل عملم گر شود نصیب ریاض</p></div>
<div class="m2"><p>بطبع بر اثر غورگی رود انگور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز حرص نعمت عصیان که زهر معنویست</p></div>
<div class="m2"><p>بدون صوم کند نفس زله بند سحور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بشوی روی سیاهم ز آب احسانت</p></div>
<div class="m2"><p>که تیرگی برد از چهره شب دیجور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس است صاحب اعمال ناسزا بودن</p></div>
<div class="m2"><p>چه احتیاج که کس جاودان بود مقهور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نعوذ بالله اگر روز حشر طی نکند</p></div>
<div class="m2"><p>شفاعت تو عمل نامه اناث و ذکور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز شرم کثرت عصیان من برعشه فتد</p></div>
<div class="m2"><p>حسابگاه قیامت چو ارض نیشابور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دم سؤال که از تاب انفعال شود</p></div>
<div class="m2"><p>نفس شکسته گلو از زمانه مغرور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امید هست که مهر لب سؤال شود</p></div>
<div class="m2"><p>عنایتت که چو عصیان ماست نامحصور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر به پنجه خورشید دل بیفشارم</p></div>
<div class="m2"><p>بجای خون زمشامش چکدشب دیجور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>وفا نمیکند امید مغفرت با یأس</p></div>
<div class="m2"><p>نه ز آنکه عفو الهی نساردم مغفور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز طول معصیت استغفرالله اندیشم</p></div>
<div class="m2"><p>که گرد قصر نشیند بذیل عفو غفور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همین بس است اگر ناجیم اگر مغضوب</p></div>
<div class="m2"><p>که با ولای تو فردا همی شوم محشور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بعون نعمت عشق تو فارغم ز نعیم</p></div>
<div class="m2"><p>نه جوی شیر شناسم نه طارم انگور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز عود مهر وگلاب وفاست عنصر من</p></div>
<div class="m2"><p>اگر برفتن دوزخ همی شوم مأمور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببزم جنتیان انجمن طراز بهشت</p></div>
<div class="m2"><p>ز دود آتش دوزخ برد بخار بخور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز کوه مهر تو حاشا اگر دهم بطباع</p></div>
<div class="m2"><p>کند بباده تبسم طبیعت کافور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>محبت تو ندارد بسینه ام داغی</p></div>
<div class="m2"><p>که هست سوده الماس و معنی ناسور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تویی که کرده ضمیرت ز روی شاهد عقل</p></div>
<div class="m2"><p>به آستین هدایت غبار غفلت دور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز مستی می تلخ حمایتت در چین</p></div>
<div class="m2"><p>بسی پیاله شکستند بر سر فغفور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر ز نشئه طبعم اثر بباغ رسد</p></div>
<div class="m2"><p>سبوی می دمد از جای دانه انگور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>منم که از اثر حسن طبع من قلمی است</p></div>
<div class="m2"><p>که بر صحیفه کند رازهای دل مستور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سزد که بی اثر رنگ و بی تحرک دست</p></div>
<div class="m2"><p>بروی صفحه نگارد مثال صورت حور</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برون کنند ملایک سر از دریچه عرش</p></div>
<div class="m2"><p>دمی که شاهد طبعم کند بسدره عبور</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بیک لباس نگنجد بجوهر اول</p></div>
<div class="m2"><p>ز ازدحام معانی ز کبریای شعور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بخوان بر اهل فناشعر من ضرورت نیست</p></div>
<div class="m2"><p>که منت دم عیسی کشند یا دم صور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حسود جاه تو بادا از شاهد مقصود</p></div>
<div class="m2"><p>چو دست جود تو او وصل آستین مهجور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شبی ز دولت رؤیای افتخار رسل</p></div>
<div class="m2"><p>علم بعرش زدم در میان خواب و شعور</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>خمیر مایه این سر قصیده آن رؤیاست</p></div>
<div class="m2"><p>که شاخ و برگ فزودش زبان من چو طیور</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کسی گمان نبرد کز برای زینت شعر</p></div>
<div class="m2"><p>بر اصل خواب فزودم که نیست این منظور</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>لذیذ بود حکایت درازتر گفتم</p></div>
<div class="m2"><p>چنانکه حرف عصا گفت موسی اندر طور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همیشه تا جگر خونچکان گمراهان</p></div>
<div class="m2"><p>بود ز نشتر شرم آشیانه زنبور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>خرابه دل مجروح امتان تو باد</p></div>
<div class="m2"><p>ز نوشد روی الطاف شاملت معمور</p></div></div>