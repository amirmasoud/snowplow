---
title: >-
    شمارهٔ ۱۴ - ترجمه الشوق در ستایش مولای متقیان علی علیه السلام
---
# شمارهٔ ۱۴ - ترجمه الشوق در ستایش مولای متقیان علی علیه السلام

<div class="b" id="bn1"><div class="m1"><p>جهان بگشتم و دردا به هیچ شهر و دیار</p></div>
<div class="m2"><p>نیافتم که فروشند بخت در بازار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفن بیاور و تابوت و جامه نیلی کن</p></div>
<div class="m2"><p>که روزگار طبیب است و عافیت بیمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا زمانه طناز دست بسته و تیغ</p></div>
<div class="m2"><p>زند به فرقم و گوید که هان سری می‌خار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه مرد مصاف است و من ز ساده دلی</p></div>
<div class="m2"><p>کنم بجوشن تدبیر و هم دفع مضار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمنجنیق فلک سنگ فتنه می بارد</p></div>
<div class="m2"><p>من ابلهانه گریزم در آبگینه حصار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب که نشکنم این کارگاه مینایی</p></div>
<div class="m2"><p>که شیشه خالی و من در لجاجتم زخمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنین که ناله زدل جوشد و نفس نزنم</p></div>
<div class="m2"><p>عجب مدار گر آتش برآورم چو چنار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر کرشمه وصلم کشد و گر غم هجر</p></div>
<div class="m2"><p>نه آفرین زلبم بشوند و نه زنهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلم ز درد گرانمایه چون جگر زفغان</p></div>
<div class="m2"><p>دما غم از گله خالی چو خاطرم زغبار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل خراب مرا مطلبی است آیت یأس</p></div>
<div class="m2"><p>چو زود رفتن جان پیش نیم کشته شکار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم چو رنگ زلیخا شکسته در خلوت</p></div>
<div class="m2"><p>غمم چو تهمت یوسف دویده در بازار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سلک مدت عمرم که روزها دزدید</p></div>
<div class="m2"><p>که فصل شیب و شبابم گذشت در شب تار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل حیات من از بسکه هست پژمرده</p></div>
<div class="m2"><p>اجل نمیزند از ننگ بر سر دستار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز دوستان منافق چنان رمیده دلم</p></div>
<div class="m2"><p>که پیش روی زالماس می کنم دیوار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برون ز صورت دیبای بالشم کس نیست</p></div>
<div class="m2"><p>کز آستین نم اشگم بچیند از رخسار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عجوز بختم اگر زلفکان بیاراید</p></div>
<div class="m2"><p>سفید گردد زلفین شاهدان تتار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کدام فتنه بشب سرنهاده بر بالین</p></div>
<div class="m2"><p>که صبحدم نشد از خواب ، روی من بیدار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جراحتم چو بخارد بعزم خاریدن</p></div>
<div class="m2"><p>پلنک ناخن، گردد زمانه خونخوار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وگر طبیب دهد ناگوار دارویی</p></div>
<div class="m2"><p>کند بشیره دندان مارنوش گوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وگر زبوته خاری کنم شبی بالش</p></div>
<div class="m2"><p>بسعی زلزله در دیده ام خلاند خار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بصید موری اگر ناوکی بزه بندم</p></div>
<div class="m2"><p>دهان مار کند در گزیدنم سوفار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یقین شناس که منصور از آن اناالحق زد</p></div>
<div class="m2"><p>که وارهد ززمانه بدستگیری دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شب گذشته بزانو نهاده بودم سر</p></div>
<div class="m2"><p>که اوفتاد خرد را بر این خرابه گذار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سری چنان که نیاری شنید بی سامان</p></div>
<div class="m2"><p>غمی چنان که مبادا نصیب دیگر بار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بدید و گفت بعالم مباد چون تو کسی</p></div>
<div class="m2"><p>جهان بخویشتن آرای و خویشتن بیزار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سری چنین همه رای صواب و بی سامان</p></div>
<div class="m2"><p>دلی چنین همه صاف شراب و درد خمار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرض بببن و سبب جوی و خود معالجه کن</p></div>
<div class="m2"><p>طبیب کیست، فلاطون اگر شود بیمار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به گریه گفتمش آری طریق عقل این است</p></div>
<div class="m2"><p>ولیک جانب انصاف خود نگه می‌دار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کسی چگونه بسامان در آورد آن سر</p></div>
<div class="m2"><p>که چون ز زانو برداشت کوفت بر دیوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بخنده گفت سراسیمگیت گم دارد</p></div>
<div class="m2"><p>وگرنه هادی این ره تو بوده ای هموار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رهت نمایم و برخویشتن نهم منت</p></div>
<div class="m2"><p>که نقدهای مرا نیست جز تو کس معیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تهی کن از همه اندیشه خطا و بنه</p></div>
<div class="m2"><p>بخاک مرقد کحل الجواهر ابصار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چه مرقد آن که بود در شکنجه تا بفلک</p></div>
<div class="m2"><p>هوای منظر او از تراکم انظار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بحیرتم که چه صنعت بکار برد که کرد</p></div>
<div class="m2"><p>بتنگنای جهان وضع این بنا معمار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که گر بقدر بلندی بیفکند سایه</p></div>
<div class="m2"><p>محیط کون و مکان گردد آسمان کردار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کتابه اش که بود سرنوشت عالم کون</p></div>
<div class="m2"><p>چو بوی جامه یوسف بر دزدیده غبار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>زهی صفای عمارت که در تماشایش</p></div>
<div class="m2"><p>بدیده باز نگردد نگاه از دیوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زسقف گنبدش امسال باز می آید</p></div>
<div class="m2"><p>هرآن صدا که کسی داده در حریمش پار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه قدر صبح شناسند ساکنان درش</p></div>
<div class="m2"><p>که در حوالی او شام را نبوده گذار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گرآفتاب درآید بگنبدش گویی</p></div>
<div class="m2"><p>که در میانه فانوس شد مگس طیار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز ذره های پریشان شعاع نور افشان</p></div>
<div class="m2"><p>نجوم بی مدد آسمان در و سیار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>غبار فرش حریمش بتاج عرش نشست</p></div>
<div class="m2"><p>اگر زجنبش موری بلند گشت غبار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گلیست در چمن صنع شکل قبه او</p></div>
<div class="m2"><p>که عرش داشته بردور او ، زکنگره خار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسی نماند که خدام او درآمد و شد</p></div>
<div class="m2"><p>کنند کنگره عرش با زمین هموار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>زآستانه او طعنه های نشنوده</p></div>
<div class="m2"><p>بپایه پایه خود عرش میکند اظهار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بگاه جوش زیارت در آستانه او</p></div>
<div class="m2"><p>نا آسمان بته کفش گم کند دستار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>فلک به پنجه خورشید از هوا گیرد</p></div>
<div class="m2"><p>اگر عمامه ای افتد زتارک زوار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بداغ لاله توان دید یاسمن دروی</p></div>
<div class="m2"><p>چو بسترد زسرش مهر سایه دیوار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دریچه اش بضیا دیده سهیل یمن</p></div>
<div class="m2"><p>نشیمنش بهوا کعبه نسیم بهار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو صبح بیضه خورشید پرورد بشکم</p></div>
<div class="m2"><p>گرآشیانه کند بسپریش بر دیوار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>رموز غیب مصور شود درو هر دم</p></div>
<div class="m2"><p>چو خاطری که بود در تصور اسرار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ازآن زمان که فتادش نظر بشمسه او</p></div>
<div class="m2"><p>شدآفتاب پرست آفتاب حربا وار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ندانم ای فلک انصاف می دهی یا نه</p></div>
<div class="m2"><p>گر از هزار جفایت یکی کنم اظهار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>فرونشین بدو زانو و چین برابر، وزن</p></div>
<div class="m2"><p>بدان صفت که دغا پیشگان دعویدار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>اگر صواب نگویم بگوی و شرم مکن</p></div>
<div class="m2"><p>که آبروی مرا نیست شرم کس درکار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مرا بشوق چنین بینی از چنان مرقد</p></div>
<div class="m2"><p>مرا بدست تهی بینی از چنین بازار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه بال روح قدس میدهی نه پر مگس</p></div>
<div class="m2"><p>نه سیم قلب دهی نه زر تمام عیار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ازین معامله خود منفعل مباش که تو</p></div>
<div class="m2"><p>بمور پر دهی از پای من بری رفتار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بکاوش مژه از کور تا نجف بروم</p></div>
<div class="m2"><p>اگر بهند بخاکم کنی و گربه تتار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ستیزه با چو تو قاهر دلیل دانش نیست</p></div>
<div class="m2"><p>زبان گزیدم وکردم زگفته استغفار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ترحمی بکن آخر که عاجزم عاجز</p></div>
<div class="m2"><p>نگاه کن که چه خون میچکانم از گفتار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>سخن چرا نبود دردناک و خون آلود</p></div>
<div class="m2"><p>که تا لب از ته دل میکند بریش گذار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مرا که دست بگیرد که زیر دست توام</p></div>
<div class="m2"><p>مراکه کار گشاید که از تو خیزد کار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چه هرزه گوشدم از درد دل که شرمم باد</p></div>
<div class="m2"><p>تو کیستی که شوی دست گیر و کارگزار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همان که شوق طوافش مرا بطوفان داد</p></div>
<div class="m2"><p>به نیم جذبه کشاند زورطه ام بکنار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>شه سریر ولایت ، علی عالی قدر</p></div>
<div class="m2"><p>محیط عالم دانش ، جهان علم و وقار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>لغت نویس خرد در صحاح همت او</p></div>
<div class="m2"><p>بمعنی لغت اندک آورد بسیار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>مثال آینه اندیشه زنگ بردارد</p></div>
<div class="m2"><p>گرآورد بدل دشمنش بسهو گذار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>برنگ دایره در حصر جود او هر دم</p></div>
<div class="m2"><p>شود ملاقی آغاز انتهای شمار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>فلک بجوهرگل گفت روز میلادش</p></div>
<div class="m2"><p>هنوز سیر کنم یا رسید وقت قرار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز خلق اوست که قندیل شقف بارگهش</p></div>
<div class="m2"><p>ز نسبت دل روح القدس ندارد عار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز فیض خنده لطفش که کیمیا اثر است</p></div>
<div class="m2"><p>بگاه صیحه قهرش که هست صور آثار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>جحیم شاخ گلی از حدیقه احسان</p></div>
<div class="m2"><p>بهشت برگ خسی در شکنجه عصار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>فتد چو سایه حلمش برآفتاب سزد</p></div>
<div class="m2"><p>که نور ازو متعدی نگردد آینه وار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>نشسته شاهد خلقش بخلوتی که بود</p></div>
<div class="m2"><p>دریچه حرمش ناف آهوی تاتار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>چو مهر رای تو در صبحدم شود طالع</p></div>
<div class="m2"><p>شود ز فرط تهوع گلوی صبح فگار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>کمان قصد ترا جذبه ای بود که اگر</p></div>
<div class="m2"><p>زهش بگوش رسانی رسد بقبضه شکار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>عبادتیکه محلی باجتهاد تو نیست</p></div>
<div class="m2"><p>بود زسیئه محتاج تر باستغفار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>زبس بعهد تو لاغر شد از ریاضت زهد</p></div>
<div class="m2"><p>گرفت پهلوی ناهید شکل موسیقار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عمل طراز فلک در صلاح کون و فساد</p></div>
<div class="m2"><p>اگر نهد بخلاف مصالح تو مدار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>غبار صحن و سرای تو اوج هفت اورنگ</p></div>
<div class="m2"><p>شکنج زلف سخای تو موج دریا بار</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>اگر نه قهر تو یاد آرد آسمان شاید</p></div>
<div class="m2"><p>که خط منطقه اش بر میان شود زنار</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>شباب سدره طوبی شود بشیب بدل</p></div>
<div class="m2"><p>چو منع نشو کنی از مجاری اشجار</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>ز مردمک نرسد نور تا ابد بمژه</p></div>
<div class="m2"><p>چو بشکنی حرکت در مفاصل انظار</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>بهر دیار که آمد لوای عدل تو، ظلم</p></div>
<div class="m2"><p>دهد درازی دست ستم بپای فرار</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>بطور عالم معنی گشوده شوق کلیم</p></div>
<div class="m2"><p>بناز و نعمت حسن تو روزه دیدار</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>هنوز ناصیه آفتاب در عرق است</p></div>
<div class="m2"><p>از آن فروغ که بر وی فشاندی از رخسار</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>زشرم نور جمال تو آفتاب هنوز</p></div>
<div class="m2"><p>بهر جهت که رود هست روی بر دیوار</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>همه تراوش جودی و کاوش امید</p></div>
<div class="m2"><p>همه نوازش ناموسی وگذارش عار</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>محیط برکف جود تو کرد ، موج فدا</p></div>
<div class="m2"><p>سپهر برسر جاه تو کرد ، اوج نثار</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>غبار خشم تو آرایش کلاه خزان</p></div>
<div class="m2"><p>شعار لطف تو افزایش جمال بهار</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز شوق کوی تو پا درگلم زعمر چه سود</p></div>
<div class="m2"><p>هزار جان گرامی و یک قدم رفتار</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>چو خیمه دوره دامانم آسمان گوئی</p></div>
<div class="m2"><p>بصد طناب فرو بسته است و صد مسمار</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>بگلخن آمده از روضه مانده ام محروم</p></div>
<div class="m2"><p>که روی هندسیه بادوپای حرص فگار</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>ز شوق کوی تو هرجا شوم هلاک مرا</p></div>
<div class="m2"><p>بجای سبزه قدم بر دمد زخاک مزار</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>نه دین بجای ، نه ایمان بسوی خویشم خوان</p></div>
<div class="m2"><p>مگر ز شرم تو بگشایم از میان زنار</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>ز وعده ها که بخود کرده ام یکی اینست</p></div>
<div class="m2"><p>که در طواف تو خواهم گریستن بسیار</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>نثار کوی تو دارم هزار جان و هنوز</p></div>
<div class="m2"><p>متاع من همه دست تهی است همچو چنار</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>اگر زآتش شوقم شود فروغ پذیر</p></div>
<div class="m2"><p>به سلسبیل زند غوطه مرغ آتشخوار</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>مرا چو دیده بود ابلقی چه اندیشم</p></div>
<div class="m2"><p>که این کرنک هرونست و آن کهر رهوار</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>چگونه پای کم آرم ز آسمان آخر</p></div>
<div class="m2"><p>که بر در تو بود دایمش بر رفتار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>بدان خدای که در شهر بند امکان نیست</p></div>
<div class="m2"><p>متاع معرفتش نیم ذره در بازار</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>بجزر و مد محیط عطای او که کشد</p></div>
<div class="m2"><p>بنیم موجه دو عالم گناه را بکنار</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>بکنه او که تعجب نشد گران مایه</p></div>
<div class="m2"><p>ازین که کرد ز درکش نبی بعجر اقرار</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>بکلک او که نوشت و بسا که بنویسد</p></div>
<div class="m2"><p>بر وی صفحه عالم سطور لیل و نهار</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بحاذقیکه که ز داروی حکمتش گردید</p></div>
<div class="m2"><p>شکسته رنگ خزان و شکفته روی بهار</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بلطف او که زفیضش نمونه ایست بهشت</p></div>
<div class="m2"><p>بجود اوزدیگش نمک چشی است بخار</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>بخشم او که همش حلم اوست شعله فشان</p></div>
<div class="m2"><p>بکنه او که همش علم اوست آینه وار</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>بعشق او که بپهلوی جان نشاند درد</p></div>
<div class="m2"><p>بشوق او که ببازوی دل فرستد کار</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>بسایه علم مصطفی در آن عرصه</p></div>
<div class="m2"><p>که آفتاب شود هم علاقه دستار</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>بجاه او که برویش قدم گشاده نظر</p></div>
<div class="m2"><p>بشبه او که بگردش عدم کشیده حصار</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>به آستین کریمش که هست گنج افشان</p></div>
<div class="m2"><p>به آستان حریمش که هست ناصیه زار</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>بنعمت تو که اندازه را کند مغرول</p></div>
<div class="m2"><p>بمدحت تو که اندیشه را کند ببمار</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>بسلک یازده عقدی کز آن دو لؤلؤ نور</p></div>
<div class="m2"><p>علی است ابر مطیر و بتول دریا بار</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بطایر ارنی سنج بی اثر نغمه</p></div>
<div class="m2"><p>بلن ترانی هم ذوق مژده دیدار</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بعشوه ای که زلیخا برید از او کف دست</p></div>
<div class="m2"><p>بفتنه ای که مسیحا گزید ازو سردار</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>ببرقع مه کنعان که بود حسن آباد</p></div>
<div class="m2"><p>بحجله گاه زلیخا که بود یوسف زار</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>به آن متاع که گوهر فروش کنعانی</p></div>
<div class="m2"><p>بمصر برد و لباب ز حسن شد بازار</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>به آن دروغ که فرهاد از و شهادت یافت</p></div>
<div class="m2"><p>به آن ترانه که منصور را کشید بدار</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بناقه ای که بلیلی خیال مجنون برد</p></div>
<div class="m2"><p>بان کرشمه که لیلی بر آن نمود نثار</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>به تیشه ای که بر اطراف صورت شیرین</p></div>
<div class="m2"><p>همه کرشمه تراشید و ریخت برکهسار</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>بنوش نوش ندیم صبوحی مستان</p></div>
<div class="m2"><p>بکاو کاو کلید طبیعت هشیار</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>بغم فروشی آسودگان شکوه طراز</p></div>
<div class="m2"><p>بتازه رویی پژمردگان شکر گزار</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>برنج بازوی پر نفع کاسبان ضعیف</p></div>
<div class="m2"><p>بچین ابروی بی وجه خواجگان کبار</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>بخستی که کند جذب طعمه از کف مور</p></div>
<div class="m2"><p>بشهوتی که زند خال بوسه بر لب یار</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>بگوشه گیری عنقا که جوهر فعال</p></div>
<div class="m2"><p>ندید صورت او جز بصفحه پندار</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>بهوشمندی آن سایه خفت نخل حیات</p></div>
<div class="m2"><p>که دیده باز نکرد از کشاکش منشار</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>بعقد گوشه دستار شاعران حریص</p></div>
<div class="m2"><p>که بی برات صله سینه ایست پرآزار</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بدست همت من کز کنار گوشه گرفت</p></div>
<div class="m2"><p>ز ننگ آنکه بدر یوره آشناست کنار</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>بطبع گرسنه چشم محبت اندیشه</p></div>
<div class="m2"><p>که جز بنعمت جود تو نشکند بازار</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>بخاک جبهه که باد بروت عابد ازوست</p></div>
<div class="m2"><p>بتار سبحه که صوفی ازوست در زنار</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بناز حسن که بندد نقاب در خلوت</p></div>
<div class="m2"><p>بر از عشق که آید که برهنه در بازار</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>بنکته گیری ناموس روستایی طبع</p></div>
<div class="m2"><p>بلب گزیدن افسوس خویشتن بی زار</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>بمردمی که بود هم طویله عنقا</p></div>
<div class="m2"><p>بمحرمی که بود هم قبیله اسرار</p></div></div>
<div class="b" id="bn135"><div class="m1"><p>بگرم چشمی من در نظاره معنی</p></div>
<div class="m2"><p>بشر مگینی من در افاده اشعار</p></div></div>
<div class="b" id="bn136"><div class="m1"><p>بسنبلی که بگلزار حسن می روید</p></div>
<div class="m2"><p>نه از میانه گلشن نه گوشه گلزار</p></div></div>
<div class="b" id="bn137"><div class="m1"><p>بنافه ای که از آهوی صنع میافتد</p></div>
<div class="m2"><p>بهر کجا نمکین تر بود ز چهره یار</p></div></div>
<div class="b" id="bn138"><div class="m1"><p>بشور قمری دستان سرای یک نغمه</p></div>
<div class="m2"><p>که درس نکته توحید می کند تکرار</p></div></div>
<div class="b" id="bn139"><div class="m1"><p>بعندلیب چمن کز نوای گوناگون</p></div>
<div class="m2"><p>لباس بوقلمون دوخت بر رخ گلزار</p></div></div>
<div class="b" id="bn140"><div class="m1"><p>بدود گلخن امید و دودگاه هوس</p></div>
<div class="m2"><p>که با دماغ منش هر دور است قرب جوار</p></div></div>
<div class="b" id="bn141"><div class="m1"><p>به آفتاب مرا دو دریچه طالع</p></div>
<div class="m2"><p>که نیست هیچگهش با زمانه ما کار</p></div></div>
<div class="b" id="bn142"><div class="m1"><p>به نیم قطره شرابی که باز می ماند</p></div>
<div class="m2"><p>پس از پیاله کشیدن بساغر از لب یار</p></div></div>
<div class="b" id="bn143"><div class="m1"><p>بکان کسب که زاید بنام بذل درم</p></div>
<div class="m2"><p>بشان نصب که دوزد بدوش عزل غیار</p></div></div>
<div class="b" id="bn144"><div class="m1"><p>به آستین کلیم و دریچه مشرق</p></div>
<div class="m2"><p>به آستان کریم و پذیره ادرار</p></div></div>
<div class="b" id="bn145"><div class="m1"><p>بعرصه دادن شوق و به آب شستن یأس</p></div>
<div class="m2"><p>بدستیاری توفیق و رنگ دادن کار</p></div></div>
<div class="b" id="bn146"><div class="m1"><p>بانبساط مکان و بامتیاز جهت</p></div>
<div class="m2"><p>باختلاط میان و باحتراز کنار</p></div></div>
<div class="b" id="bn147"><div class="m1"><p>بعلت سکنات و بکوشش حرکات</p></div>
<div class="m2"><p>بعزت حسنات و بجوشش اذکار</p></div></div>
<div class="b" id="bn148"><div class="m1"><p>بتوبه و به پشیمانی دل تائب</p></div>
<div class="m2"><p>بمستی و بپریشانی سر و دستار</p></div></div>
<div class="b" id="bn149"><div class="m1"><p>بعیش زهره چنگی، بدرد ناله من</p></div>
<div class="m2"><p>بفیض سرمه مکی، بگرد کوچه یار</p></div></div>
<div class="b" id="bn150"><div class="m1"><p>بخوی فشانی شبنم ، بخود فروشی گل</p></div>
<div class="m2"><p>بنیزه بازی سوسن ، بدشنه سازی خار</p></div></div>
<div class="b" id="bn151"><div class="m1"><p>بیکه تازی وحدت بعرصه توحید</p></div>
<div class="m2"><p>بفوج داری کثرت بمعرض آثار</p></div></div>
<div class="b" id="bn152"><div class="m1"><p>بدعوت لب عابد که دوخت دلق مراد</p></div>
<div class="m2"><p>باتش دل عاشق که سوخت لوح مزار</p></div></div>
<div class="b" id="bn153"><div class="m1"><p>ببر شکفتن امروز و غنچه گشتن دی</p></div>
<div class="m2"><p>بتوشه پختن امسال و نام بردن پار</p></div></div>
<div class="b" id="bn154"><div class="m1"><p>بشیوه دانی شهر و بساده خویی ده</p></div>
<div class="m2"><p>بنخل بندی کشت و بخوشه چینی کار</p></div></div>
<div class="b" id="bn155"><div class="m1"><p>بصبح قاتم پوش و بشام اکسون باف</p></div>
<div class="m2"><p>بصلح آب فشان و بجنگ آتش بار</p></div></div>
<div class="b" id="bn156"><div class="m1"><p>بهوشمندی عدل و سیاه مستی ظلم</p></div>
<div class="m2"><p>بتر زبانی تیغ و بسر گرانی دار</p></div></div>
<div class="b" id="bn157"><div class="m1"><p>بکذب بی پدر و صدق آدمیزاده</p></div>
<div class="m2"><p>بجهل بی اثر و عقل جبرئیل آثار</p></div></div>
<div class="b" id="bn158"><div class="m1"><p>ببخل وعده تراش و قناعت عیاش</p></div>
<div class="m2"><p>بصدق تنگ معاش و خوش آمد جرار</p></div></div>
<div class="b" id="bn159"><div class="m1"><p>بناگواری نزع و بناگزیری مرگ</p></div>
<div class="m2"><p>به بی مداری عمروبه بیوفایی یار</p></div></div>
<div class="b" id="bn160"><div class="m1"><p>بهزل معرکه گیر و نفاق تو بر تو</p></div>
<div class="m2"><p>بصبر کم سخن و شوق آتشین گفتار</p></div></div>
<div class="b" id="bn161"><div class="m1"><p>بآبروی قناعت بذلت خواهش</p></div>
<div class="m2"><p>بکامرانی فرصت بدولت دیدار</p></div></div>
<div class="b" id="bn162"><div class="m1"><p>بتنگنای گریبان بوسعت دامن</p></div>
<div class="m2"><p>بخاکساری کفش و بنخوت دستار</p></div></div>
<div class="b" id="bn163"><div class="m1"><p>بداع پهلوی بیمارممتنع حرکت</p></div>
<div class="m2"><p>بدرد زانوی جویای منقطع رفتار</p></div></div>
<div class="b" id="bn164"><div class="m1"><p>بحق این همه سوگندهای صدق آمیز</p></div>
<div class="m2"><p>که نزد علم تو حاجت نداشتم بشمار</p></div></div>
<div class="b" id="bn165"><div class="m1"><p>که گر شود ره کوی تو جمله نشترخیز</p></div>
<div class="m2"><p>کنم بمردمک دیده طی نشتر زار</p></div></div>
<div class="b" id="bn166"><div class="m1"><p>رهی ز شوق سراسیمه طی کنم که قدم</p></div>
<div class="m2"><p>بکام تیشه نهم گرستانم از سرخار</p></div></div>
<div class="b" id="bn167"><div class="m1"><p>بآب مهر تو شستم گناهنامه خویش</p></div>
<div class="m2"><p>چه غم که کاتب اعمال دارد استحضار</p></div></div>
<div class="b" id="bn168"><div class="m1"><p>گدای کوچه مهرت بروزگار گناه</p></div>
<div class="m2"><p>گرفته یاج ز سلطان ملک استغفار</p></div></div>
<div class="b" id="bn169"><div class="m1"><p>نه در پناه ولای توام؟ چه غم که بود</p></div>
<div class="m2"><p>معاصیم نه باندازه قیاس و شمار</p></div></div>
<div class="b" id="bn170"><div class="m1"><p>وگر ولای تو ابلیس را شود زورق</p></div>
<div class="m2"><p>کشد زورطه نعتش بیک نفس بکنار</p></div></div>
<div class="b" id="bn171"><div class="m1"><p>شباهت تو کند آفتاب دریوزه</p></div>
<div class="m2"><p>که آورد بضمیرم بدین وسیله گزار</p></div></div>
<div class="b" id="bn172"><div class="m1"><p>هران عروس سخن کز دیار مدح تو نیست</p></div>
<div class="m2"><p>بعشوه گر کشدم در نیاورم بکنار</p></div></div>
<div class="b" id="bn173"><div class="m1"><p>مگر بدامن جود تو دست زد قلمم</p></div>
<div class="m2"><p>که گنجش از بن ناخن دمید نرگس وار</p></div></div>
<div class="b" id="bn174"><div class="m1"><p>چو کرم پیله بخود بر تند مدایح تو</p></div>
<div class="m2"><p>بگاه طاعت ایزد چو دارمش بی کار</p></div></div>
<div class="b" id="bn175"><div class="m1"><p>معلمی که تراشیده خامه طبعم</p></div>
<div class="m2"><p>زآفتاب نهد لوح ساده ام بکنار</p></div></div>
<div class="b" id="bn176"><div class="m1"><p>کجاست مانی صورت نگار تا بیند</p></div>
<div class="m2"><p>نگارخانه ارژنگ و صورت جان دار</p></div></div>
<div class="b" id="bn177"><div class="m1"><p>بچار سوی سخن نقد رایجی دارم</p></div>
<div class="m2"><p>نه همچو ماه زر اندوده آفتاب عیار</p></div></div>
<div class="b" id="bn178"><div class="m1"><p>کلام من که متاع ولایت سخن است</p></div>
<div class="m2"><p>بروی دست صبا میرود سلیمان وار</p></div></div>
<div class="b" id="bn179"><div class="m1"><p>نه انجم است فلک را؛ که همت عرفی</p></div>
<div class="m2"><p>دمادم آب دهانش فکنده بر رخسار</p></div></div>
<div class="b" id="bn180"><div class="m1"><p>از آن بعالم سفلی درآمدم که مرا</p></div>
<div class="m2"><p>غریب دوست نهاد است و آشنا بی زار</p></div></div>
<div class="b" id="bn181"><div class="m1"><p>زجهل جایزه یابم اگر هجا گویم</p></div>
<div class="m2"><p>بعلم تاج دهم چون شوم مدیح نگار</p></div></div>
<div class="b" id="bn182"><div class="m1"><p>بکام دنیویم چون زبان نمی گردد</p></div>
<div class="m2"><p>حدیث جایزه درحشر می کنم اظهار</p></div></div>
<div class="b" id="bn183"><div class="m1"><p>چو این قصیده در افواه خاص و عام افتاد</p></div>
<div class="m2"><p>خطاب ترجمه الشوق یافت از احرار</p></div></div>