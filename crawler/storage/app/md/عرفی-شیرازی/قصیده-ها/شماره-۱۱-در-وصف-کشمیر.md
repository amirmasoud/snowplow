---
title: >-
    شمارهٔ ۱۱ - در وصف کشمیر
---
# شمارهٔ ۱۱ - در وصف کشمیر

<div class="b" id="bn1"><div class="m1"><p>هر سوخته جانی که به کشمیر درآید</p></div>
<div class="m2"><p>گر مرغ کباب است که با بال پر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر که زفیضش بشود گوهر یکتا</p></div>
<div class="m2"><p>جاییکه خزف گر رود آنجا گهر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وآنگه بچنین فصل که در ساحت گلزار</p></div>
<div class="m2"><p>از لطف هوا چاشت نسیم سحر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بلبل خاموش دل باغ گرفته است</p></div>
<div class="m2"><p>او را چه گنه محمل گل دیر ترآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل هم چه کند باد صبا خواست که عرفی</p></div>
<div class="m2"><p>آید سوی کشمیر وگلش بر اثر آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهفته ای از شاهد گل حجله تهی باش</p></div>
<div class="m2"><p>تا بلبل شیراز در این باغ در آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشکفته گل اما بمثل بر رگ شاخی</p></div>
<div class="m2"><p>گر پایه نهم خون گلم تا کمر آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت است که کل برفکند پرده زرخ باز</p></div>
<div class="m2"><p>زآنسان که زفانوس چراغی بدر آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهتاب گل از هم بشکافد قصب شاخ</p></div>
<div class="m2"><p>وز لمعه او سیب قمر لعل تر آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فردوس بدروازه کشمیر رسیده است</p></div>
<div class="m2"><p>کو مدعیی گر نگرنده است درآید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زیبایی کشمیر گرش باعث عشوه ست</p></div>
<div class="m2"><p>من میخرم ار زال فلک عشوه گر آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این سبزه و این چشمه و این لاله و این گل</p></div>
<div class="m2"><p>آن شاخه ندارد که بگفتار در آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن چشمه که رضوان چو رود تشنه بسویش</p></div>
<div class="m2"><p>کوثر بسرش تیزتر و تشنه تر آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن لاله که هنگام تراشیدن خارا</p></div>
<div class="m2"><p>از رخنه سنگ و دهن تیشه برآید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در چاشت که از شبنم گل گرد فشانست</p></div>
<div class="m2"><p>آن باد که در هند گرآید جگر آید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا رنگ گلی نشکفد از تابش خورشید</p></div>
<div class="m2"><p>حربا نکند میل که خورشید برآید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بسکه کند جذب رطوبت خطرش نیست</p></div>
<div class="m2"><p>گر ساغر چینی ز هوا بر حجر آید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حاجت بدوزخم ارفتدش قطع محال است</p></div>
<div class="m2"><p>گر سنگدلی مایل قطع شجر آید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زن کز مدد نشو و نما، زخم نخستین</p></div>
<div class="m2"><p>مصمت شده تا زخم دگر بر اثر آید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کشمیر بهشتی است فریبنده که شبلی</p></div>
<div class="m2"><p>آید چو در او، صومعه بروی سقر آید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طاووس مثالی که نیفشاند پر و بال</p></div>
<div class="m2"><p>هر لمحه برنگ دگر اندر نظر آید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زیبنده عروسی که نیفزوده جمالش</p></div>
<div class="m2"><p>هر دم بنظر خوشتر و شاداب تر آید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر لحظه که شاداب و ترش بینم ، گویم</p></div>
<div class="m2"><p>بگشای بغل بو که در آغوش درآید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یاد از روش خود کنم و بزم خداوند</p></div>
<div class="m2"><p>هر گه که صبا از چمنش جلوه گر آید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بوی گل آید کنم از انجمنش یاد</p></div>
<div class="m2"><p>تا نگهت گل مایه صد درد سرآید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر گه که بعزم سفر از شوق تو عرفی</p></div>
<div class="m2"><p>آید بوداع وی و با چشم تر آید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زاری کند از شش جهت آغاز که مشتاب</p></div>
<div class="m2"><p>کین فصل و سه فصل دگرم بر اثر آید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لیک از همه خلدست که بی طوف جنانت</p></div>
<div class="m2"><p>چندان نکند مکث که وقت ثمر آید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کشمیر بر او واله او واله کشمیر</p></div>
<div class="m2"><p>اما نه چنان کش بدل از دیده در آید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کارش همه انباشتن چشمه گریه است</p></div>
<div class="m2"><p>هرگاه که سیمای تو در اندر نظر آید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ترسد که در این خاک چو از شوق تو گیرد</p></div>
<div class="m2"><p>خون جگرش گل شود آنگه بدر آید</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از بسکه ملایم صفت افتاده هوایش</p></div>
<div class="m2"><p>بیم است که آه سحرش بی اثر آید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حکم تواش آورد بکشمیر و گرنه</p></div>
<div class="m2"><p>کی از سر آن خاک بخاک دگر آید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>می آید و می سوزد از این رشک که کشمیر</p></div>
<div class="m2"><p>چون یافت که آید به کجا بر اثر آید</p></div></div>