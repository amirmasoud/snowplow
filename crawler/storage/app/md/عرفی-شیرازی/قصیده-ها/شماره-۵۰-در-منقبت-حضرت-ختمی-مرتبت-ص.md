---
title: >-
    شمارهٔ ۵۰ - در منقبت حضرت ختمی مرتبت (ص)
---
# شمارهٔ ۵۰ - در منقبت حضرت ختمی مرتبت (ص)

<div class="b" id="bn1"><div class="m1"><p>ای مرا بر زشتی اعمال ، نومیدی گواه</p></div>
<div class="m2"><p>دورم از حسن عمل چون رو سپیدی از گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت امید می بینم چو آب موج زن</p></div>
<div class="m2"><p>بسکه میگردد زشرمم رعشه در نور نگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بصورت کاه را گویم که همرنگ منی</p></div>
<div class="m2"><p>کهر با چون مردم چشم بتان گردد سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میل فعل زشت را با طبع من آمیزش است</p></div>
<div class="m2"><p>وین شبیه ربط کفر است و مکافات اله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ور بعصیان در نمی آمیزم از بی قوتی است</p></div>
<div class="m2"><p>وین بعینه چون حریص شهوتست و ضعف باه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که داری نامه اعمال را از فعل زشت</p></div>
<div class="m2"><p>چون مصیبت خانه عاشق زدود دل سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهره را از آب یاقوت ندامت بر فروز</p></div>
<div class="m2"><p>چون گل روی دل آرایان ز تاثیر نگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نگاه شاهد معنی عالم غوطه زن</p></div>
<div class="m2"><p>تا بجولانگاه صورت بسته ای دام نگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرحبا نیک آمدی ای یأس تا بیرون دهم</p></div>
<div class="m2"><p>گریه گرمی که شوید تیرگی را از گناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هان سمند آهسته ران ای گمره ناهوشمند</p></div>
<div class="m2"><p>منحرف میتازی و مستی و تاریکست راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حبذا ای نوبهار عجز کز تأثیر تو</p></div>
<div class="m2"><p>معصیت را میدهد آمرزش از طرف کلاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میتوان کردن تلافی عمر ضایع کرده را</p></div>
<div class="m2"><p>گر زنو برگ گیاه تازه گردد برگ کاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهد معنی عیان و ما بصورت ملتفت</p></div>
<div class="m2"><p>ای درون جهل ما چون روی نادانی سیاه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بسکه بی تأثیر ضایع گشت در دیر مجاز</p></div>
<div class="m2"><p>گریه های تلخ شام و ناله های صبحگاه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بعد از این در معبدی نالم که بی منت نهد</p></div>
<div class="m2"><p>گوهر کام ابد در دامن تأثیر آه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حالتی یابم که از تکفیر من کافر شوند</p></div>
<div class="m2"><p>گر تراود از زبانم لیس فی دلقی سواه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقصدت دور است عرفی گرباین ره میروی</p></div>
<div class="m2"><p>کام همت را روائی باید از امداد شاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قهرمان عرش مسند داور امی لقب</p></div>
<div class="m2"><p>صورتش مرات معنی معنیش صنع اله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر محیط رای او بر چرخ گردد موج زن</p></div>
<div class="m2"><p>دامن موجش بروید چشمه خورشید و ماه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در شب معراج کان یکتای بی شبه و نظیر</p></div>
<div class="m2"><p>جامه صورت زروش افکنده در آرامگاه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زان کسی محرم نبود اندر حریم ایزدی</p></div>
<div class="m2"><p>تابود و هم غلط بین در امان از اشتباه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای زروی نسبت ذاتت ولایت را شرف</p></div>
<div class="m2"><p>وی بزیر سایه جاهت نبوت راپناه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سایه یزدانی و انوار سیمایت دلیل</p></div>
<div class="m2"><p>داور کونینی و انواع احسانت سپاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دست حفظت بهر چابک خیزی و بربستگی</p></div>
<div class="m2"><p>بر میان شعله بر بندد تطاق از برگ کاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شاخ شاخ و برگ برگش تازه بر هم ریختند</p></div>
<div class="m2"><p>تاز باغ همتت خواندیم طوبی را گیاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاهد عدلت بدست خلق در ایوان دهر</p></div>
<div class="m2"><p>سنبل ریحان فشاند فتنه را در خوابگاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بسکه دست رحمتت آرایش هر چهره کرد</p></div>
<div class="m2"><p>عشق میورزد بحسن یأس و امید اشتباه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>توشه گیر انتفاع از ریزش جود ، توجود</p></div>
<div class="m2"><p>خوشه چین ارتفاع از مزرع جاه تو جاه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از خیال هیبتت اندیشه میرد در ضمیر</p></div>
<div class="m2"><p>وزنشان آستانت سجده رقصد در جباه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با ازل گوید ابد کین نا امید از ساحل است</p></div>
<div class="m2"><p>کر کند در بحر علمت گوهر اول شناه</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای که از احوالم آگاهی مهل اینسان مرا</p></div>
<div class="m2"><p>همچو سعیم در حصول طاعت و عفت تباه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می تراود آب شور از تیره بختم گر کسی</p></div>
<div class="m2"><p>تا ابد در ساحت تحت الثری میکند چاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سینه مد الف بشکافد و بیرون جهد</p></div>
<div class="m2"><p>چون در اثنای پریشانی نویسم تیر آه</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یوسف نفس مرا زآسیب اخوان دوردار</p></div>
<div class="m2"><p>کین حسودان مروت سوز با این بیگناه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با فریب غول همزادند در راه سلوک</p></div>
<div class="m2"><p>بافساد گرگ انبازند در نزدیک چاه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا اسیران محبت را بجولانگاه دوست</p></div>
<div class="m2"><p>احتمال سجده کردن مضمر است اندر جباه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>احتمال رو سپیدی دور باد از آنکه او</p></div>
<div class="m2"><p>جز به درگاه تو ساید چهره در عذر گناه</p></div></div>