---
title: >-
    شمارهٔ ۱۳ - در مدح حکیم ابوالفتح
---
# شمارهٔ ۱۳ - در مدح حکیم ابوالفتح

<div class="b" id="bn1"><div class="m1"><p>داورا! سال نوت محفل طراز سور باد</p></div>
<div class="m2"><p>تهنیت گویان عامت قیصر و فغفور باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ازل سال کهن برگشته بهر تهنیت</p></div>
<div class="m2"><p>جملگی در ساحت سال نوت محصور باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از در دروازه نوروز تا میدان عید</p></div>
<div class="m2"><p>هم چنین آرایش بازار عمرت سور باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میرابوالفتح آفتاب اوج عزت نام توست</p></div>
<div class="m2"><p>این مبارک نام یارب تا ابد مذکور باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت رای صائبت صنعت نگار عالمم</p></div>
<div class="m2"><p>آسمان گفت آفتاب من ترا ، مزدور باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دولتت در باغ عالم گفت شهلا نرگسم</p></div>
<div class="m2"><p>زهره گفتا چشم من چون چشم تو مخمور باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر معمائی کش افزایش بود مصداق اسم</p></div>
<div class="m2"><p>در میان کودکان دولتت مشهور باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر لغت کاندیشه یابد بهر مفهوم ابد</p></div>
<div class="m2"><p>جمله بر عنوان لوح هستیت مسطور باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در سماعند از صریر خامه ات اسرار غیب</p></div>
<div class="m2"><p>حشر و نشر لفظ و معنی ازدم این صور باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دولتت بر دشمنان نیش است و بر احباب نوش</p></div>
<div class="m2"><p>نوش و نیش هر دوکون از فیض این زنبور باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه فلک محصور باد اندر حصار دولتت</p></div>
<div class="m2"><p>نی غلط گفتم فضای لامکان محصور باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاخ تا کی کش بود بخت بلندت باغبان</p></div>
<div class="m2"><p>طارم گردون شکن زان شاخه انگور باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>قبضه شمشیر کینت دستگاه آفت است</p></div>
<div class="m2"><p>سایه شمشاد رایت چشمه سار نور باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عالم عیشت که تا تطبیق شرع آید قدیم</p></div>
<div class="m2"><p>آسمان او بهشت و زهره او حور باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عالمی هست ار جز این عالم که او را ناظم است</p></div>
<div class="m2"><p>هم ترا با ناظمش عدل ترا مزدور باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهر اخذ نعمت تسخیر عالم بردرت</p></div>
<div class="m2"><p>دامن دریوزه بر کف سایه باد و نور باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر قضا خود را شمارد دستیار حکم تو</p></div>
<div class="m2"><p>جای تعزیر است اما گویمش معذور باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در محیط عشق موسایی که موجش دائم است</p></div>
<div class="m2"><p>لجه قرب ترا هر موج ، کوه طور باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عشقت از بازیچه در بزمی اگر مستی کند</p></div>
<div class="m2"><p>شیشه می را شکستن برسر فغفور باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مدح لایق مشکل است اما بملک مدح تو</p></div>
<div class="m2"><p>رایت اندیشه روح القدس منصور باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چون دعای شاعرانه نیست عرفی بی اثر</p></div>
<div class="m2"><p>ساده گویی کن بگو هستیت نامحصور باد</p></div></div>