---
title: >-
    شمارهٔ ۵۲ - ذکر مفاخر خان خانان
---
# شمارهٔ ۵۲ - ذکر مفاخر خان خانان

<div class="b" id="bn1"><div class="m1"><p>بیا که با دلم آن می‌کند پریشانی</p></div>
<div class="m2"><p>که غمزه تو نکردست با مسلمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زدیده رفتی و مردم همان نفس ، فریاد</p></div>
<div class="m2"><p>که بی تو مردم وآنگه چنین بآسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی که تشنه لب ناز توست میداند</p></div>
<div class="m2"><p>که موج آب حیات است چین پیشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهشت غمزه اسلام دشمنت که دو روز</p></div>
<div class="m2"><p>محبت تو کنم جمع با مسلمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترحمی نکند حسن بر دلم ، گوئی</p></div>
<div class="m2"><p>که در زمانه یوسف نبود زندانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که گفت مطلع دیگر چنین نیاری گفت</p></div>
<div class="m2"><p>که تازه سازد از این مطلع‌آفرین خوانی</p></div></div>