---
title: >-
    شمارهٔ ۴۰ - بیان مفاخر
---
# شمارهٔ ۴۰ - بیان مفاخر

<div class="b" id="bn1"><div class="m1"><p>من کیستم آن سالک کونین مسیرم</p></div>
<div class="m2"><p>کز بیخته جوهر قدس است خمیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفحه تصویر جلال است مثالم</p></div>
<div class="m2"><p>در پرده تقدیر محال است نظیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون حسن کشد جام صفا رنگ شرابم</p></div>
<div class="m2"><p>چون عشق دهد رنگ جبین آب زریزم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در قامت عاشق شکن آموز کمانم</p></div>
<div class="m2"><p>در غمزه معشوق گشایش ده تیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که وفا تشنه شود چشمه خونم</p></div>
<div class="m2"><p>آنجا که صفا غسل کند آب غدیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر کتف ریاضت طلبان شال و پلاسم</p></div>
<div class="m2"><p>بر دوش زلیخا منشان برد و حریرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هندسه فقر و فنا صفرالوفم</p></div>
<div class="m2"><p>در مزرعه عز و علا ابر مطیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کوزه لذت شکنان چشمه زهرم</p></div>
<div class="m2"><p>در کاسه کودک منشان جرعه شیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنجا که ادب نغمه طراز است سمیعم</p></div>
<div class="m2"><p>و آنجا که هنر جلوه فروش است بصیرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مرسله جوهر فردم در یکتا</p></div>
<div class="m2"><p>در سلسله علت و معلول کثیرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای طلبم ، در روش سعی تمامم</p></div>
<div class="m2"><p>دست ادبم ، در کشش کام قصیرم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون سجده بت گرم شود ناصیه سوزم</p></div>
<div class="m2"><p>چون تیغ صنم کند شود بهیده میرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خفاشم و خورشید خرد در ته بالم</p></div>
<div class="m2"><p>در اجم و بلبل پرد از شاخ صفیرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عشقم که بر آسوده دلان نیست گذارم</p></div>
<div class="m2"><p>حسنم که ز خونین جگران نیست گزیرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در خانه مجنون که خراب است ، غبارم</p></div>
<div class="m2"><p>در حجله لیلی که بهشت است ، عبیرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با ناطقه گلریزم و با سامعه گلچین</p></div>
<div class="m2"><p>با وا همه نابالغ و با عاقله پیرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در دل قویم گر چه به آثار ضعیفم</p></div>
<div class="m2"><p>وز دین غنیم گرچه باظهار فقیرم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از کلک بنان ، لوح خراشنده ماهم</p></div>
<div class="m2"><p>وز تیغ زبان خامه تراشنده تیرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در کندی شمشیر زبان قاتل سیفم</p></div>
<div class="m2"><p>در پرده اندیشه خرد پوش ظهیرم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از اوج سخن بهر فرود آمدن طبع</p></div>
<div class="m2"><p>برداشتم این نغمه که اعشی و جریرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>طبعم ز غضب گفت ندانم بچه نسبت</p></div>
<div class="m2"><p>در دام سرشت تو قضا کرد اسیرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر جوهر خود می نشناسی زچه کانی</p></div>
<div class="m2"><p>از گوهر من شرم بکن کابر مطیرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر تافت عنان سخنم رخش طبیعت</p></div>
<div class="m2"><p>برگشتم ازین ره که نه این بود مسیرم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر تارک ارباب فنا ترک کلاهم</p></div>
<div class="m2"><p>در صفحه اصحاب صفا نقش حصیرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>درآب و هوای چمن خلد سرورم</p></div>
<div class="m2"><p>در بست وگشاد در فردوس صریرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>توفیق چه صورت شکند قوت دستم</p></div>
<div class="m2"><p>تحقیق چو معنی طلبد جوش ضمیرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میگویم و اندیشه ندارم زحریفان</p></div>
<div class="m2"><p>من زهره رامشگر و من بدر منیرم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سربرزده ام با مه کنعان زیکی جیب</p></div>
<div class="m2"><p>معشوق تماشا طلب وآینه گیرم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در بارگه سلطنتم چون گذرت نیست</p></div>
<div class="m2"><p>بر ناصیه ماه ببین نقش سریرم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هنگام رقم سنجی احکام کواکب</p></div>
<div class="m2"><p>برجیس نهد مجمره در پیش دبیرم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آن چشمه قربم که زلب تشنگی وحی</p></div>
<div class="m2"><p>جبریل در آید بحرمگاه ضمیرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عرفی بکجا میروی ، این راه کدام است</p></div>
<div class="m2"><p>مشتاب و عنان دار ازین راه خطیرم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز آشوب صریرش دل کونین برآشفت</p></div>
<div class="m2"><p>نای قلم نغمه گشا تنگ بگیرم</p></div></div>