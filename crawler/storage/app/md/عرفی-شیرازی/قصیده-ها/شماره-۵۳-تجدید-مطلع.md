---
title: >-
    شمارهٔ ۵۳ - تجدید مطلع
---
# شمارهٔ ۵۳ - تجدید مطلع

<div class="b" id="bn1"><div class="m1"><p>زهی وفای تو همسایه پشیمانی</p></div>
<div class="m2"><p>نگاه گرم تو تکلیف نامسلمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متاع حسن تو سرمایه تهیدستی</p></div>
<div class="m2"><p>خیال زلف تو مجموعه پریشانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب تو جرعه ده باده دل آشوبی</p></div>
<div class="m2"><p>غم تو شانه کش طره تن آسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل کرشمه بخندد چو چشم بازکنی</p></div>
<div class="m2"><p>بهار عشوه بریزد چو رخ بپوشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زدین خویش سوالش کنند در محشر</p></div>
<div class="m2"><p>کسی که عشق تو نگزید بر مسلمانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین که لشکری از ( مرغ نامه بر) دارم</p></div>
<div class="m2"><p>مرا سزد که کنم دعوی سلیمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی نوشت و نیامد جواب نامه دوست</p></div>
<div class="m2"><p>قلم که دست زمن میبرد بگریانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه دست درخم اندیشه میزند دیگر</p></div>
<div class="m2"><p>مگر بجوش درآمد شراب روحانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلی چوسینه الهام و وحی میجوشد</p></div>
<div class="m2"><p>ز شوق انجمن فهم میرزا خانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زفر عدل وی امروز یک بها دارد</p></div>
<div class="m2"><p>متاع نوشروانی و خان خانانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بعون مکرمت او نیاز کاسه تهی</p></div>
<div class="m2"><p>زفقر تا بغنا میبرد بمهمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دمی که دست برآرد زآستین جودش</p></div>
<div class="m2"><p>بچشم آز کند موج بحر سوهانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بعهد او شعرا در صفات زلف بتان</p></div>
<div class="m2"><p>کنند نقل بجمعیت از پریشانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سهم او که نیارد فشاندگرد فتور</p></div>
<div class="m2"><p>فلک بدامن احوال انسی و جانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کند ز حیله برای گزیدن مردم</p></div>
<div class="m2"><p>بگاه مستی از و التماس ترخانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بوصف رایش اگر خامه زن شوم گردد</p></div>
<div class="m2"><p>اناملم همگی چون هلال نورانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هوای وصف کمندش بخاطرم زد موج</p></div>
<div class="m2"><p>گره شد افعی اندیشه ام ز پیچانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دل حسود ز ویران ترست زان موضع</p></div>
<div class="m2"><p>که در زمانه جود تو میکند کانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو زیب محفل و من بینمت که در میدان</p></div>
<div class="m2"><p>سر زمانه بفتراک بسته میرانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نهال بخت تو در گلشنی بود سر سبز</p></div>
<div class="m2"><p>که راه کاهکشانش کند خیابانی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو سدره ریشه دوانیده در جهات آید</p></div>
<div class="m2"><p>درخت عمر تو در چار باغ ارکانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زحد گذشته حد خدمت فلک ترسم</p></div>
<div class="m2"><p>که زیر مسند خویشش چو عرش بنشانی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زمانه جمع کندشش جهت بیک جانب</p></div>
<div class="m2"><p>اگر تو رخش حکومت بیک جهت رانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سمند دولت جاویدیت که در هر گام</p></div>
<div class="m2"><p>بساط کون و مکان بایدش بمیدانی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برهنه پا و سرآید ابد بدنبالش</p></div>
<div class="m2"><p>اگر عنانش بسوی ازل بگردانی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بخرق عادت اگر ملتفت شوی شاید</p></div>
<div class="m2"><p>که کنه خویش در ادراک عقل گنجانی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شجاعت تو ولینعمتی بود که کند</p></div>
<div class="m2"><p>بمطبخش جگر شیر شرزه بریانی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو عرض معجزه را تربیت دهی شاید</p></div>
<div class="m2"><p>که سایه در بغل آفتاب بالانی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو رخش کینه بتازی بروزگار سزد</p></div>
<div class="m2"><p>که گرد تخت ثری بر سپهر بنشانی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قلم براه صلاح تو میرود ورنه</p></div>
<div class="m2"><p>کجا رسد بدو انگشت نی جهانبانی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان عصای کلیم است خامه تو ولی</p></div>
<div class="m2"><p>صلاح در قلمی دیده نی بثعبانی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رقم کشان یمین و یسار دشمن تو</p></div>
<div class="m2"><p>که می کنند سخن سنجی و قلم رانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بهر شدت خذلان او بدل کردند</p></div>
<div class="m2"><p>طبیعت ملکی را بنفس شیطانی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سه گانه گوهر والانژاد دوده کون</p></div>
<div class="m2"><p>که جنس معدنی و نامیه است و حیوانی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آن میان وجود وعدم فرود آیند</p></div>
<div class="m2"><p>که صرف رد و قبولیت شود بآسانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فلک بمردمک آفتاب اگر دیدی</p></div>
<div class="m2"><p>بروز عدل تو حسن زمانه فانی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بمانی از حرکت آفتاب در مطلع</p></div>
<div class="m2"><p>مثال دیده احول بگاه حیرانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گهر شناسا در پیش پای بین و بسنج</p></div>
<div class="m2"><p>نثار من که بفرق تو باد ارزانی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غلط مسنج و مبین ، پایمال نسیان کن</p></div>
<div class="m2"><p>مباد چیده دگر بار بر سر افشانی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سبک ز چاش بگیری که بس گران گهر است</p></div>
<div class="m2"><p>متاع من که نصیبش مباد ارزانی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>قماش دست زد شهروده ز من مطلب</p></div>
<div class="m2"><p>متاع من همه دریایی است و یا کانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بسکه لعل فشاندم بنزد اهل قیاس</p></div>
<div class="m2"><p>یکی است نسبت شیرازی و بدخشانی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بعهد جلوه حسن کلام من ، اندوخت</p></div>
<div class="m2"><p>قبول شاهد نظم کمال نقصانی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کنونکه یافت چومن سرمه سای درشیراز</p></div>
<div class="m2"><p>خرد ز دیده کشد سرمه صفاهانی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بین که تافته ابریشمش چه خامی یافت</p></div>
<div class="m2"><p>زتاب اطلس من شعر باف شروانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمانه بین که مرا جلوه داد تا از رشک</p></div>
<div class="m2"><p>بداغ رشک پس از مرگ سوخت خاقانی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گرفت روی زمین جمله آفتاب صفت</p></div>
<div class="m2"><p>بعون تیغ زبان شهرتم بآسانی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بخند ، ای در و دیوار روزگار خراب</p></div>
<div class="m2"><p>که برزمانه زدم تکیه سلیمانی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو کرم پیله لعابی تنیده ام ببروت</p></div>
<div class="m2"><p>که اصل خلعت دارایی است و خاقانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز شوق بوقلمون حله عبارت من</p></div>
<div class="m2"><p>مدام شاهد معنی نمود عریانی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زسحر خامه جادو اثر ، فرستادم</p></div>
<div class="m2"><p>بجای شعر بکاغذ شراب روحانی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بنوش و باک مدار این شراب خامه رسا</p></div>
<div class="m2"><p>که نیست خوردن این باده را پشیمانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از این شراب گر آلوده دامنی خیزد</p></div>
<div class="m2"><p>بکش که بر تو حرامست پاکدامانی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زمانه خواند فلک بر بیاض دیده نوشت</p></div>
<div class="m2"><p>که این قصیده بیاضی بود نه دیوانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>برآستان تو صد گنج شایگان ریزد</p></div>
<div class="m2"><p>چو آستینت اگر نامه ام بر افشانی</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مده براوی ناجنس نامه ام که مرا</p></div>
<div class="m2"><p>در این قصیده بروز کمال ننشانی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا زنسبت همدردی کمال غم است</p></div>
<div class="m2"><p>وگرنه شعر چه غم دارد از غلط خوانی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز همعنانی طبعم بشاعر شروان</p></div>
<div class="m2"><p>بعهد کودکیم ذهن کرده شروانی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>کنونکه رتبه حکمت گرفت شعر از من</p></div>
<div class="m2"><p>کند بنسبت این اعتبار یونانی</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>هنوز هست امیدش که یابد از فیضم</p></div>
<div class="m2"><p>بعون خدمت صاحب خطاب گیلانی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>مفرحی که من از بهر روح سازدهم</p></div>
<div class="m2"><p>به انوری نه فلانی دهد نه بهمانی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چه صاحب آنکه در اهمال خدمتش نشنید</p></div>
<div class="m2"><p>قضا ز صورت دیوار عذر بیجانی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همان که هست ترا باروان افلاطون</p></div>
<div class="m2"><p>خطاب لفظی و باوی تکلم جانی</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همان که گریه کلکت از آن روا داری</p></div>
<div class="m2"><p>که نوبهار طبیعت برو بخندانی</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همان که فرق فلک را بتیغ بشکافد</p></div>
<div class="m2"><p>گرت زحادثه چینی فتد بپیشانی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همان که ابر عتابش چو فتنه بار شود</p></div>
<div class="m2"><p>جهان زحفظ تو خواهد کلاه بارانی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>همان که نشکند از هیچ دست طرف کلاه</p></div>
<div class="m2"><p>که تو نثار و فاقی برآن نیفشانی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>سخن صریح بگویم حکیم ابوالفتح است</p></div>
<div class="m2"><p>که تو سپهر فضایل مآثرش خوانی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دلیر زانش پرستم که از لیاقت او</p></div>
<div class="m2"><p>گرفته برهمنی سیرت مسلمانی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ذخیره ای نهد از من که مانی از صورت</p></div>
<div class="m2"><p>تمتعی برم از وی که صورت از مانی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>از آن ندیده ثنا گویمت که می بینم</p></div>
<div class="m2"><p>ترا و او را یک تن بچشم روحانی</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دلیل وحدتم این بس که مدح خود میخواست</p></div>
<div class="m2"><p>مرا به مدح تو فرمود ، گوهر افشانی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>تو چون گذر کنی آنجا بنظم رنگینم</p></div>
<div class="m2"><p>که مصرعش چمنی کرد و بیت بستانی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ضمیر وی بمن اینجا نشان دهد هر جا</p></div>
<div class="m2"><p>که ناخنی بزنی یا سری بجنبانی</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>در این زمین دوسه بیتی گزیده در مدحش</p></div>
<div class="m2"><p>ذخیره دارم از انعامهای ربانی</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>قصیده ناشده و ناتمام میخوانم</p></div>
<div class="m2"><p>که شوق من بثنا خواندش تو میدانی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تبارک الله از آن گوهر محیط عطا</p></div>
<div class="m2"><p>که از افاضت خود قطره ، کرد عمانی</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>نه نفس کلی و دریای گوهر دانش</p></div>
<div class="m2"><p>نه عقل اول و استاد جوهر ثانی</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>عداوتش بگهر سیمیای مصلحتی</p></div>
<div class="m2"><p>عنایتش باثر کیمیای رحمانی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>بجای دیو ملک را کند بشیشه اگر</p></div>
<div class="m2"><p>کسی بخلوت خلقش کند پریخوانی</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>نخست خویشتنت بخشد از گران گهری</p></div>
<div class="m2"><p>چو دست همتش آید بگوهر افشانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>زمانه را و فلک را بوی خطابی بود</p></div>
<div class="m2"><p>نه دوش و دی ، دم اشراق صبح امکانی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>زمانه گفت تو پرویز و من ترنج زرم</p></div>
<div class="m2"><p>بکام خود بطرازم چنانکه میدانی</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>سپهر گفت تو آنی که توسن آنچه منم</p></div>
<div class="m2"><p>براه عجز برانم چنانکه میرانی</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شکفته بخت وی و دل شکسته طالع خصم</p></div>
<div class="m2"><p>ندیم میکده و کام جوی زندانی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>چو رسم خدمت او عام گشت ، گردون گفت</p></div>
<div class="m2"><p>که داغ صورت چین تازه شد ز بیجانی</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>زمانه گفت فلک را گهی بیابد ابر</p></div>
<div class="m2"><p>مراتب کف جودش، بگوهر افشانی</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>فرو گریست که آری گهی که نفس فلک</p></div>
<div class="m2"><p>بعلم جوهر اول رسد ز گردانی</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>سخن شناسا دیدی و دیده باشی هم</p></div>
<div class="m2"><p>علو پایه من در مقام سحبانی</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>فلان مربی و من تربیت پذیر این بس</p></div>
<div class="m2"><p>زفضل خود چه زنم لافهای طولانی</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>دراز شد سخنم جای شرم و تن زدن است</p></div>
<div class="m2"><p>گرفتم آنکه لآلی است جمله عمانی</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>طریق ذیل چه پویم در این خجالتگاه</p></div>
<div class="m2"><p>که لنک شد خردم را سمند جولانی</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>ثنای صاحب و مدح تو همچو شیر و شکر</p></div>
<div class="m2"><p>بهم سرشتم و بگرفت شکل و حدانی</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نوای لاف و گزافی که سنت شعر است</p></div>
<div class="m2"><p>زدم چنانکه دلم خون شد از پشیمانی</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>نمی وزد زجهان باد بر دلم هرگز</p></div>
<div class="m2"><p>که زلف شاهد نطقم کند پریشانی</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>حدیث آب و علف خود بنزد من باداست</p></div>
<div class="m2"><p>که نظم و نثر مرا کرده آبی و نانی</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تمام همت و سرتا قدم مراد دلم</p></div>
<div class="m2"><p>اگر دهی نستانم ، دهم چو بستانی</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>دگر چه مانده؟ دعایی کنون بگو چکنم؟</p></div>
<div class="m2"><p>طلب کنم که نه تحصیل حاصلش خوانی</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>همیشه نانبود ثانی اقدم از اول</p></div>
<div class="m2"><p>همیشه تا که بود سربتاج ارزانی</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>ز سایه تاج ده فرق بخت عرفی باد</p></div>
<div class="m2"><p>همای دولت مخدوم اول و ثانی</p></div></div>