---
title: >-
    شمارهٔ ۴۵ - در نعت حضرت رسول (ص)
---
# شمارهٔ ۴۵ - در نعت حضرت رسول (ص)

<div class="b" id="bn1"><div class="m1"><p>صبحدم چون در دمد دل صور شیون زای من</p></div>
<div class="m2"><p>آسمان صحن قیامت گردد از غوغای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوش اهل آسمان و حلقه ماتم یکی است</p></div>
<div class="m2"><p>شیونم تا برکشد آهنک هایا های من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مصر ویران کرد و رو در وادی ایمن نهاد</p></div>
<div class="m2"><p>رود نیل شوق یعنی گریه موسای من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان دل شوریده را بر تارک خود می نهم</p></div>
<div class="m2"><p>کاشیان مرغ مجنون شد دل شیدای من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آن ملایک چون مگس جوشندم از هر سو که هست</p></div>
<div class="m2"><p>چشمه لذت گشا هر موی غم پالای من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کام جان را تازه کردی ای غم لذت سرشت</p></div>
<div class="m2"><p>نی غلط گفتم چه غم ای من وای سلوای من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خمار انتظارم ز آنکه ایزد دور داشت</p></div>
<div class="m2"><p>باده کام دو کون از جام استغنای من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسمان در یوزه کرد و آفتابش کرد نام</p></div>
<div class="m2"><p>لعلی از آویزه گوش شب یلدای من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیلگون گردید دوش آفتاب از تکیه ام</p></div>
<div class="m2"><p>بسکه هر مو گشته کوهستانی از غمهای من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منت بازیچه عیسی مکش بهر حیات</p></div>
<div class="m2"><p>ارزش مردن بپرس از نفس مرگ آرای من</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خورده هر دم صد شکست از فوج قدس آشوب حسن</p></div>
<div class="m2"><p>شوق بیهنگام یار مست بی پروای من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منکه مستی کردن از خون جگر آموختم</p></div>
<div class="m2"><p>ننگ هوشم باد گر جز خون بود صهبای من</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهد عصمت تلاش صحبتم را کی سزد</p></div>
<div class="m2"><p>خون حیض دختر رز نوشد از لبهای من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منکه از دل تا دماغم چیده خمهای شراب</p></div>
<div class="m2"><p>کی شود مخمور و کی خالی شود مینای من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مریم من فیض جبریل از مزاج خود گرفت</p></div>
<div class="m2"><p>مریمی را برد بالا ذهن عیسی زای من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن بهشت معنیم کز بعد معزولی هنوز</p></div>
<div class="m2"><p>خدمت طوبی بود ننگ چمن پیرای من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرحبا ای باده کیفیت روح القدس</p></div>
<div class="m2"><p>کامدی چون عشق و در رفتی ز سر تاپای من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>من قیامت زار عشقم دیده کو تا بنگرد</p></div>
<div class="m2"><p>صد بهشت و دوزخ از هر گوشه صحرای من</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نفخ صور آمد بجای لحن داودی هنوز</p></div>
<div class="m2"><p>رقص معنی می کند طبع سهی بالای من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من مطیع ملک استغنا ولی رانند حکم</p></div>
<div class="m2"><p>دودمانهای هوس در ملک استغنای من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دامنم تر کرده طوفانی که در معنی یکیست</p></div>
<div class="m2"><p>موجه دریا و موج حله خارای من</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نور و ظلمت رابود یک مایه در تابندگی</p></div>
<div class="m2"><p>آن ز روی آفتاب و این یک از سیمای من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بسکه در معنی بطفلی باز می گردم ، ملک</p></div>
<div class="m2"><p>در حساب دی شمارد غفلت فردای من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آیت لاتقنطوا من رحمه الله شد گره</p></div>
<div class="m2"><p>بر زبان جبرئیل از شرم عصیانهای من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>معنی پنهان من آرایش بیت الله است</p></div>
<div class="m2"><p>گو شبیه دیر باشد صورت پیدای من</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لوح دل نقش صمد دارد چه غم کاستاد چین</p></div>
<div class="m2"><p>بافت تمثال صنم بر شقه دیبای من</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بال طاووس از گلاب و عود رضوان پرورد</p></div>
<div class="m2"><p>تا بسازد مروحه در موسم گرمای من</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اصل من از دودمان نوع انسانی مجوی</p></div>
<div class="m2"><p>حور غم رضوان درد است آدم و حوای من</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جوهر اول که فرزندم ز بیباکی نوشت</p></div>
<div class="m2"><p>آن زمان سنجد عیار گوهر یکتای من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کز جهان در یثرب آرم روی در گوش آیدش</p></div>
<div class="m2"><p>مرحبا یا امتی از مرقد مولای من</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر گزیند سرمه جز خاک درش مژگان چو باز</p></div>
<div class="m2"><p>چنگل اندازد بزاغ دیده بینای من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شقه دیبای جاهش گفت محسود که ام</p></div>
<div class="m2"><p>آسمان گفتا طراز خانه خضرای من</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>موجه دریای طبعش بانگ کوثر کردو گفت</p></div>
<div class="m2"><p>تشنه منشین ای فدای زاده دریای من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در دم اندیشه قدر تو بشکافد ز هم</p></div>
<div class="m2"><p>حله های علم بر دوش دل دانای من</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا تو گشتی غایب چشم از ره نسبت گرفت</p></div>
<div class="m2"><p>مردمک حکم سبل در دیده بینای من</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سایه من همچو من در ملک هستی امتت</p></div>
<div class="m2"><p>سایه تو در عدم پیغمبر همتای من</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آسمان وحدتم بر عالم فطرت محیط</p></div>
<div class="m2"><p>توامیت بر نتابد پیکر جوزای من</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دودمان عشق را از من گرامی تر نزاد</p></div>
<div class="m2"><p>جوهر من کرد روشن گوهر آبای من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نازش سعدی بمشت خاک شیراز از چه بود</p></div>
<div class="m2"><p>گر نبود آگه که گردد مولد و مأوای من</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>این کباب آتش جان و شراب درد دل</p></div>
<div class="m2"><p>کش سخن نامست تا کی ریزد از لبهای من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>من پریشان گوی و سهواندیش و سودا هرزه دوست</p></div>
<div class="m2"><p>من به سودا مانم و ماند به من سودای من</p></div></div>