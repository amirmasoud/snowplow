---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>از سوادالوجه فی الدارین اگر داری خبر</p></div>
<div class="m2"><p>چشم بگشا و سواد فقر و کفر ما نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سواد اینچنین کفر مجازی مردوار</p></div>
<div class="m2"><p>سوی دارالملک از کفر حقیقی کن سفر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفر باطل حق مطلق را بخود پوشیده نیست</p></div>
<div class="m2"><p>کفر حق خودرا بخود پوشیده نیست ای پرهنر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو در بند خودی حق را بخود پوشیده</p></div>
<div class="m2"><p>با چنین کفری ز کفر ما کجا داری دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه از سرچشمه کفر حقیقی آب خورد</p></div>
<div class="m2"><p>بحر کفر هر دو عالم را تو پیشش چون نهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بکلی یافت در شمس حقیقی مستتر</p></div>
<div class="m2"><p>بدر گردید از ظهور نور خوشید آن قمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کفر احمد چیست در شمس احد مخفی شدن</p></div>
<div class="m2"><p>چیست طاها مظ۶ر کل ظهور نور خور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس بگویید کاف کفر ما ز طاها برتر است</p></div>
<div class="m2"><p>آنکه باشد از معانی و حقایق بهره ور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایکه در بند قبول خاص و عامی روز و شب</p></div>
<div class="m2"><p>کفر و ایمان را رها کن نام این معنی مبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کفر و ایمان چون حجاب راه حقّند ای پسر</p></div>
<div class="m2"><p>رو بسان مغربی از کفر و ایمان دگذر</p></div></div>