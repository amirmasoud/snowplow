---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>ما از ازل مقامر و خمار آمدیم</p></div>
<div class="m2"><p>دردی کشان میکده یار آمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید باده بر سر ذرّات ما بتافت</p></div>
<div class="m2"><p>از روی مهر، سرخوش و خمار آمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خلوت عدم می هستی از جام دوست</p></div>
<div class="m2"><p>کردیم نوش و مست به بازار آمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنار زلف ساقی باقی چو شد عیان</p></div>
<div class="m2"><p>هر یک کمر ببسته بزنار امدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناگاه حلقه زد سر زلفش ب گِرد ما</p></div>
<div class="m2"><p>ما در میان حلقه گرفتار امدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر خاطر دل مختار مصطفی</p></div>
<div class="m2"><p>روزی دو سه که عاقل و هشیار آمدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاری بغیر عشق نداریم در جهان</p></div>
<div class="m2"><p>عشق است کار ما و بدین کار آمدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بودیم یکوجود ولیکن که ظهور</p></div>
<div class="m2"><p>بسیار در مظاهر بسیار آمدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از یار مغربی سخنی در ازل شنید</p></div>
<div class="m2"><p>ما جمله زان حدیث بگفتار آمدیم</p></div></div>