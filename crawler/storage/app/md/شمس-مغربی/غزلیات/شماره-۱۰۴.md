---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>دلا گر دیده ای داری بیا بگشا بدیدارش</p></div>
<div class="m2"><p>ز رخسار پریرویان ببین خوبی ز رخسارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خورشید پریرویان هزاران مشتری دارد</p></div>
<div class="m2"><p>بده خود را بجز او را اگر هستی خریدارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ببازار آمد آن دلبر ز خلوتخانه وحدت</p></div>
<div class="m2"><p>تماشا را ببازار آیین گرمی بازارش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگارم درگه جلوه نظر را دوست میدارد</p></div>
<div class="m2"><p>ز خلوت زان بصحرا شد که تا بینند نظارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهی را دوست میدارد گدای مفلس او شد</p></div>
<div class="m2"><p>بنقش فخر می آرد نمی آید از او عارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو گر دیده بدست آری توانی یار را دیدن</p></div>
<div class="m2"><p>گهی در کسوت یار و گهی در شکل اغیارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم هر دم به دلداری از آنرو میشود مایل</p></div>
<div class="m2"><p>که در رخسار دلداران نماید چهره دلدارش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا آشفته میدارد خرد در حال هشیاری</p></div>
<div class="m2"><p>الا ای ساقی باقی دمی مگذار هشیارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برآ از مشرق و مغرب الا ایمغربی یکدم</p></div>
<div class="m2"><p>که تا بی مشرق و مغرب ببینی شمس انوارش</p></div></div>