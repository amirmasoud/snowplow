---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>هر زمان خورشید او از مشرقی سر بر کند</p></div>
<div class="m2"><p>ماه مهر افزاش هر دم جلوه دیگر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای آنکه تا نشناسد او را هر کسی</p></div>
<div class="m2"><p>قامت زیباش هردم کسوتی دیگر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت او هر زمانی معنی دیگر دهد</p></div>
<div class="m2"><p>معنیش هر لحظه از صورتی سر بر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر فضلش چون ببارد بر زمین ممکنات</p></div>
<div class="m2"><p>آنزمین و آسمان را پر زماه و خور کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بتابد آفتاب حسن او بر کائنات</p></div>
<div class="m2"><p>نور او از روزن هر خانه سر بر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مظاهر تا شود ظاهر جمال روی او</p></div>
<div class="m2"><p>هر دو عالم را برای روی خود منظر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه از جان شد غلام درآستان گهش</p></div>
<div class="m2"><p>حضرت اورا برفعت شاه صد کشور کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مغربی گر سر بفرمانش درآرد بنده وار</p></div>
<div class="m2"><p>لطفش اورا بر همه گردنکشان سرور کند</p></div></div>