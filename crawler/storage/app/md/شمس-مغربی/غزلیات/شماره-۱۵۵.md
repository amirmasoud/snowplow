---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>عشق من حسن ترا درخور اگر هست بگو</p></div>
<div class="m2"><p>چون منت در دو جهان مظهر اگر هست بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منظری نیست ترا بِه ز‌دل و دیده من</p></div>
<div class="m2"><p>زین دل و دیده نظر بهتر اگر هست بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر سودای تو اندر دل ما چیزی نیست</p></div>
<div class="m2"><p>غیر سودای توام در سر اگر هست بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زیور حسن تو دایم نظر عشاق است</p></div>
<div class="m2"><p>حسن را بهتر ازین زیور اگر هست بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهتر از عشق من و حسن تو در عالم نیست</p></div>
<div class="m2"><p>زین دو در جمله جهان بهتر اگر هست بگو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لشکر حسن تو غارتگر جان و دل ماست</p></div>
<div class="m2"><p>بجز از لشکر او لشکر اگر هست بگو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشور دل بتو دادم که توئی حاکم او</p></div>
<div class="m2"><p>حاکمی جز تو در این کشور اگر هست بگو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر تو در دوجهان نیست دگر هیچ کسی</p></div>
<div class="m2"><p>غیر تو در دوجهان، دیگر اگر هست بگو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی پرتو خورشید تو عالم بگرفت</p></div>
<div class="m2"><p>آفتابی چو تو در خاور اگر هست بگو</p></div></div>