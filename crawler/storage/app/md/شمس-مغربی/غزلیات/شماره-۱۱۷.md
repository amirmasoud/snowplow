---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>ما سالها مقیم در یار بوده ایم</p></div>
<div class="m2"><p>اندر حریم محرم اسرار بوده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با یار خوشخرامم و خندان بکام دل</p></div>
<div class="m2"><p>بیزحمت و مشقت اغیار بوده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر حرم مجاور و در کعبه معتکف</p></div>
<div class="m2"><p>بی قطع راه و وادی خونخوار بوده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از ظهور این قفس تنگ کاینات</p></div>
<div class="m2"><p>ما عندلیب گلشن اسرا بوده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنیدن هزار سال در اوج فضای قدس</p></div>
<div class="m2"><p>بی پر و بال طایر و طیّار بوده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>والاتر از مظاهر اسماء ذات او</p></div>
<div class="m2"><p>بالاتر از ظهور و ز اظهار بوده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم نقطه که اصل وجود است دایره</p></div>
<div class="m2"><p>هم گرد نقطه دایر و دوّار بوده ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی ما و بی شما کجا و کدام و کی</p></div>
<div class="m2"><p>بی چند و چون و اندک و بسیار بوده ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با مغربی مغارب اسرار گشته ایم</p></div>
<div class="m2"><p>بیمغربی مشارق انوار بوده ایم</p></div></div>