---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>نهان بصورت اغیار یار پیدا شد</p></div>
<div class="m2"><p>عیان بنقش و نگار آن نگار پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان گرد و غبار آن سوار پنهان بود</p></div>
<div class="m2"><p>ولی چون گرد نشست، آن غبار پیدا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان خطی است که گردغذار او بدمید</p></div>
<div class="m2"><p>خطی خوش است که گرد غذار پیدا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای بلبل غمگین بینوای حزین</p></div>
<div class="m2"><p>هزار گلبن شادی ز خار پیدا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی که اصل عدد بود در شمار</p></div>
<div class="m2"><p>از آن سبب عدد بیشمار بیشمار پیدا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پدید گشت ز کثرت جمال وحدت را</p></div>
<div class="m2"><p>یکی بکسوت چندین هزار پیدا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نقطه در حرکت آمد از پی تدویر</p></div>
<div class="m2"><p>محیط و مرکز و دور مدار پیدا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نتایج سوی کاینات لشکر او</p></div>
<div class="m2"><p>بگو که از چه سبب این غبار پیدا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر تو طالب سرّ ولایتی بطلب</p></div>
<div class="m2"><p>ز مغربی که درین روزگار پیدا شد</p></div></div>