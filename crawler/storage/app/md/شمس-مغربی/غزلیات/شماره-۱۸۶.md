---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>منم مست از لب ساقی نه از می</p></div>
<div class="m2"><p>کز آن لب میکشم جام پیاپی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از گفتار مطرب در سماعم</p></div>
<div class="m2"><p>نه از آواز چنگ و ناله نی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بجان، من زنده چون باشم که جانم</p></div>
<div class="m2"><p>ندارد زندگی یک لحظه بی وی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>الا ای آفتاب سایه گستر</p></div>
<div class="m2"><p>مگردان روی را از جانب فی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خورشیدی و من سایه از آنرو</p></div>
<div class="m2"><p>گهی لاشی شوم از وی گهی شیء</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانی در پیم‌آیی چو خورشید</p></div>
<div class="m2"><p>زمانی آیمت چون سایه از پی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسان سایه ام‌ای مهربانان</p></div>
<div class="m2"><p>گهی میگستری گه میکنی طی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیابد بیتو عالم مغربی را</p></div>
<div class="m2"><p>که مجنون را غرض لیلی است از حی</p></div></div>