---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>دیده سرگردان و نور دیده دایم در نظر</p></div>
<div class="m2"><p>چشم در منظور ناظر لیک از وی بیخبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه عالم را بچشم دوست بیند دیده لیک</p></div>
<div class="m2"><p>از بصر پنهان بود پیوسته آن نور بصر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بسان کوی سرگردان و غافل زان که او</p></div>
<div class="m2"><p>وز خم چوگان زلف دوست باشد مستقر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بیرون از خم چوگان زلفش یکزمان</p></div>
<div class="m2"><p>دل که چون گوئی همیگردد در این میدان پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نمیدان که عالم چیست یا خود کیست این</p></div>
<div class="m2"><p>عقل و نفس و جسم و چرخش خوانی و شمس و قمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با همه سرگشتگی و جنبش و نور و صفات</p></div>
<div class="m2"><p>بیخبر گردون و ز گردون ماه از هر خور ز خور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایدل ار خواهی بببینی دلبران را عیان</p></div>
<div class="m2"><p>پاک و صافی ساز خود را آنگهی در خود نگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در صفای خویشتن باید رخ دلدار دید</p></div>
<div class="m2"><p>زانکه تو آیینه و دوست در تو جلوه گر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چونکه مطلوب تو از تو نیست بیرون بعد ازین</p></div>
<div class="m2"><p>مغربی در خویشتن باید ترا کردن سفر</p></div></div>