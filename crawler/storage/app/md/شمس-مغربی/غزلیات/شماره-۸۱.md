---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>دلی دارم که در روی غم نگنجد</p></div>
<div class="m2"><p>چه جای غم که شادی هم نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان ما و یار همدم ما</p></div>
<div class="m2"><p>اگر همدم نباشد دم نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث بیش و کم اینجا رها کن</p></div>
<div class="m2"><p>که اینجا وصف بیش ک کم نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان پر گشت گوش از نغمه دوست</p></div>
<div class="m2"><p>که در وی بانگ زیر و بم نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز انگشتی که عالم خاتم اوست</p></div>
<div class="m2"><p>دگر چیزی دراین خاتم نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلی کاو فارغست از سوز و ماتم</p></div>
<div class="m2"><p>در او هم سور هم ماتم نگنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسد هر کز بحالی آدمیزاد</p></div>
<div class="m2"><p>که آنجا عالم و آدم نگنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبان ای مغربی درکش ز گفتار</p></div>
<div class="m2"><p>مگو چیزی که در عالم نگنجد</p></div></div>