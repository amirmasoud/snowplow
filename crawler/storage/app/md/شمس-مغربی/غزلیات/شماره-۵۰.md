---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>این جوش که از میکده برخاست چه جوش است</p></div>
<div class="m2"><p>این جوش مگر از خم آن باده فروش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دیده ندانم که چرا مست و خراب است</p></div>
<div class="m2"><p>وین عقل ندانم که چرا رفته ز هوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل باده کجا خورده ندانم شب دوشین</p></div>
<div class="m2"><p>کاو بیخبر و مست و خراب از شب دوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این کیست که دردل گوش دل آهسته سخنگوست</p></div>
<div class="m2"><p>وان کیست که اندر پس این پرده بگوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گوش فلک از مه تو حلقه که انداخت</p></div>
<div class="m2"><p>این چرخ ندانم که چرا حلقه بگوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این مهره مهر از چه برین چرخ روانست</p></div>
<div class="m2"><p>بر اطلس گردون ز کواکب چه نقوشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای هدهد جان ره بسلیمان نتوات برد</p></div>
<div class="m2"><p>بر درکه او بسکه طیور است و وحوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساکن نشود بحر دل مغربی از جوش</p></div>
<div class="m2"><p>یارب ز چه بادست که در جنبش و جوش است</p></div></div>