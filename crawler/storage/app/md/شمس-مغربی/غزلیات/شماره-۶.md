---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>ورای مطلب هر طالب است مطلب ما</p></div>
<div class="m2"><p>برون زمشرب هر شارب است مشرب ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کام دل به کسی هیچ جرعه ای نرسید</p></div>
<div class="m2"><p>از آن شراب که پیوسته می کشد لب ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر کوکب ماست از سپهر ها برون</p></div>
<div class="m2"><p>که هست ذات مقدس سپهر کوکب ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بتاختند اسب دل ولی نرسید</p></div>
<div class="m2"><p>سوار هیچ روانی به گرد مرکب ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنوز روز و شب کائنات هیچ نبود</p></div>
<div class="m2"><p>که روز ما رخ او بود و زلف او شب ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی که جان و جهان داد عشق او بخرید</p></div>
<div class="m2"><p>وقوف یافت ز سود زیان  بکسب ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آه و یارب ما آن کسی  خبر دارد</p></div>
<div class="m2"><p>که سوخته است چو ما او ز آه یا رب ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو وین و مذهب ما گیر در اصول و فروع</p></div>
<div class="m2"><p>که دین و مذهب حق است دین و مذهب ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخست لوح دل تز نقش کائنات بشوی</p></div>
<div class="m2"><p>چو مغربیت هست اگر عزم مکتب ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه مهر بود که بسرشت دوست در گل ما</p></div>
<div class="m2"><p>چه گنج بود که بنهاد یار در دل ما</p></div></div>