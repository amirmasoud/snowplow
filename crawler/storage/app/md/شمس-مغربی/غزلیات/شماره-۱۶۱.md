---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>ای در پس هر لباس و پرده</p></div>
<div class="m2"><p>بر دیدهء دیده جلوه کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را بلباس هر دو عالم</p></div>
<div class="m2"><p>آورده بهر زمان و برده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیده ما بجز یکی نیست</p></div>
<div class="m2"><p>گر هست عدد هزار ورده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را ز شمرده گشت معلوم</p></div>
<div class="m2"><p>آن چیز که هست ناشمرده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای بیضه مرغ لامکانی</p></div>
<div class="m2"><p>ای هم تو سفید و هم تو زرده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی مرغ شوی و باز گردی</p></div>
<div class="m2"><p>آیی بدر از لباس و پرده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جنبش و جوش و در خروش آی</p></div>
<div class="m2"><p>تا کی باشی چنین فشرده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگشای کفن بیفکن این پوست</p></div>
<div class="m2"><p>چون روح برآ ز جسم مرده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگشای دو بال و پس برون پر</p></div>
<div class="m2"><p>از گنبد چرخ سالخورده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرگز نرسد کسی به منزل</p></div>
<div class="m2"><p>نارفته طریق ناسپرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای مغربی کی رسی بسیمرغ</p></div>
<div class="m2"><p>بر قله قاف پی نبرده</p></div></div>