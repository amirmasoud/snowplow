---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>چو بحر نامتناهیست دایما موّاج</p></div>
<div class="m2"><p>حجاب وحدت دریاست کثرت امواج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان و هرچه در او هست جنبش دریاست</p></div>
<div class="m2"><p>ز قعر بحر بساحل همی کند اخراج</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم که ساحل بینهایت اوست</p></div>
<div class="m2"><p>بود مدام بامواج بحر او محتاج</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علاج درد دلم غیر موج دریا نیست</p></div>
<div class="m2"><p>چو طرفه درد که موحش بود در او علاج</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر خسی برسد زین محیط در و گهر</p></div>
<div class="m2"><p>یکی بخس رسد از وی یکی بگوهر باج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از این محیط که عالم بجنّت اوست سراب</p></div>
<div class="m2"><p>مراست عذب و فرات و تراست ملح اجاج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلون و طعم اگر مختلف همی گردد</p></div>
<div class="m2"><p>ز اختلاف محل است و انحراف مزاج</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آنچه مغربی از کاینات حاصل کرد</p></div>
<div class="m2"><p>بگرد بحر محیطش بیکزمان تاراج</p></div></div>