---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ز دریا موج گوناگون برآمد</p></div>
<div class="m2"><p>ز بیچونی برنگ چون برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نیل از بهر موسی آب گردید</p></div>
<div class="m2"><p>برای دیگران چون خون برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که از هامون بسوی بحر شد باز</p></div>
<div class="m2"><p>گهی از بحر بر هامون برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو زین دریای بیچون موج زن شد</p></div>
<div class="m2"><p>حباب آسا بر او گردون برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از این دریا بدین امواج هر دم</p></div>
<div class="m2"><p>هزاران گوهر مکنون برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو یار آمد ز خلوتگاه بیرون</p></div>
<div class="m2"><p>بهر نقشی درین بیرون برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی در کسوت لیلی فرو شد</p></div>
<div class="m2"><p>گهی از صورت مجنون برآمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بصد دستان نگارم داستان شد</p></div>
<div class="m2"><p>بصد افسانه و افسون برآمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدین کسوت که می بینیش اکنون</p></div>
<div class="m2"><p>یقین میدان که هم اکنون برآمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بمعنی هیچ دیگرگون نگردیدد</p></div>
<div class="m2"><p>بصورت گرچه دیگرگون برآمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شعر مغربی در هر لباسی</p></div>
<div class="m2"><p>بغایت دلبر و موزون برآمد</p></div></div>