---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>آغاز مشتریست ببازار آمده</p></div>
<div class="m2"><p>خود را ز دست خویش خریدار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن گل رخت سوی گلستان روان شده</p></div>
<div class="m2"><p>وان بلبل است جانب گلزار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قد و قامت همه خوبان دلربا</p></div>
<div class="m2"><p>آن سرو قامت است برفتار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنهان از این جهان ز سرا پرده نهان</p></div>
<div class="m2"><p>یاری است در لباس چو اغیار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محبوب گشته است محب جمال خود</p></div>
<div class="m2"><p>مطلوب خویش راست طلبکار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روی اوست این همه مومن اعیان شده</p></div>
<div class="m2"><p>وز موی اوست این همه کفار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن یک ز روی اوست به تسبیح مشتغل</p></div>
<div class="m2"><p>وین یک ز موی اوست به زنّار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم ز یک حدیث پر از گفتگو شده</p></div>
<div class="m2"><p>زان نکته است جمله به گفتار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رویش به پیش زلف مقر آمده است لیک</p></div>
<div class="m2"><p>زلفش به پیش روی با نگار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک باده بیش نیست در اقداح کاینات</p></div>
<div class="m2"><p>ز اقداح باده مختلف آثار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عالم مثال علم و ظلال صفات اوست</p></div>
<div class="m2"><p>آدم ز جمله است نمودار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن تر تنگ چشم که امساب شد پدید</p></div>
<div class="m2"><p>از تازه تازه نیست به دیدار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن شاه تیز بست که در روم قیصر است</p></div>
<div class="m2"><p>و آن ماه رومی است عرب کار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکذات بیش نیست که هست از صفات خویش</p></div>
<div class="m2"><p>گه در ظهور و گاه در اظهار آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از ذات اوست این همه اسما اعیان شده</p></div>
<div class="m2"><p>و از نور اوست این همه انوار آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هم اسم و رسم و نعمت و صفت آمده پدید</p></div>
<div class="m2"><p>هم عین و غیر اندک و بسیار آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این نقشه ها هست سراسر نمایش است</p></div>
<div class="m2"><p>اندر نظر چو صورت پندار آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>این کثرتی است لیک ز وحدت شده عیان</p></div>
<div class="m2"><p>این وحدتی است لیک به تکرار آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تکرار نیست چونکه کتابی است مختلف</p></div>
<div class="m2"><p>وین موج ها ز قلزم ز خار آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از موج اوشده است عراقی و مغربی</p></div>
<div class="m2"><p>وز جوش او سنائی و عطار آمده</p></div></div>