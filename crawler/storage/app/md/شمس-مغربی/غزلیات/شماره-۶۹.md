---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>دلی نداشتم آنهم که بود، یار ببرد</p></div>
<div class="m2"><p>کدام دل که نه آن یار غمگسار ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نیم غمزه روان چه من هزار بود</p></div>
<div class="m2"><p>بیک کرشمه دل همچو من هزار ببرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار نقش برانگیخت آن نگار ظریف</p></div>
<div class="m2"><p>که تا بنقش دل از دستم آن نگار ببرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیادگار دلی داشتم از حضرت دوست</p></div>
<div class="m2"><p>ندانم ارچه سبب دوست یادگار ببرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم که آینه روی دوست داشت غبار</p></div>
<div class="m2"><p>صفای چهره او از دلم غبار ببرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در میانه درآمد خرد کنار گرفت</p></div>
<div class="m2"><p>چو در کنار درآمد دل از کنار ببرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگرچه درد دل مسکین من قرار گرفت</p></div>
<div class="m2"><p>ولیکن از دل مسکین من قرار ببرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهوش بودم یا اختیار در همه کار</p></div>
<div class="m2"><p>زمن بعشوه گری هوش و اختیار ببرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون نه جان و نه دل دارم، نه عقل و نه هوش</p></div>
<div class="m2"><p>چو عقل و هوش و دل و جان هر چهار ببرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو آمد او بمیان رفت مغربی زمیان</p></div>
<div class="m2"><p>چو او بکار درآمد مرا ز کار ببرد</p></div></div>