---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>چو تافت بر دل و بر جانم آفتاب تجلی</p></div>
<div class="m2"><p>بسان ذرّه شدم در فروغ و تاب تجلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رهیدم از شب دیجور نفس و ظلمت تن</p></div>
<div class="m2"><p>ز عکس پرتو انوار آفتاب تجلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنی چو طور و دلی چون کلیم میباید</p></div>
<div class="m2"><p>که آورد بمیقات دوست تاب تجلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از این حدیث چه کشته است حادث از حدسان</p></div>
<div class="m2"><p>طهارتی نتوان یافت جز بآب تجلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شد خراب تجلی دلم طهارت یافت</p></div>
<div class="m2"><p>خوشا عمارت آندل که شد خراب تجلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقاب ما و من از پیش دیده‌ام برخاست</p></div>
<div class="m2"><p>چو رخ نمود مرا یار از نقاب تجلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا بمجلس رندان پاکباز درآ</p></div>
<div class="m2"><p>ز دست ساقی باقی بخور شراب تجلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شراب ناب تجلی رهاندت از خود</p></div>
<div class="m2"><p>دلا مباش دمی بی‌شراب ناب تجلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مغربی نتوان یافت هیچ نام و نشان</p></div>
<div class="m2"><p>از آنزمان که نهان گشته در قباب تجلی</p></div></div>