---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>آنکس که دیده در طلب او مسافر است</p></div>
<div class="m2"><p>عمریست تا که در دل و جانم مسافر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وانکس که دید روی بتان حسن روی اوست</p></div>
<div class="m2"><p>در حسن روی خویش بهردیده ناظر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل را بسحر غمزه خوبان همی برد</p></div>
<div class="m2"><p>آن غمزه را نگر که زهی غمزه و ساحر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از چشم او مپرس که ترکیست جنگجوی</p></div>
<div class="m2"><p>از زلف او مگوی که هندوی کافر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که مگر ذاکرم آن دوست را بخود</p></div>
<div class="m2"><p>خود راست کز زبان من آندوست ذاکر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غایب نباش یک نفس از دوست زانکه دوست</p></div>
<div class="m2"><p>در غیبت و حضور تو پیوسته حاضر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن وی است آنکه مرا ورانه اوّلست</p></div>
<div class="m2"><p>عشق من است آنکه مرا ورانه آخر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کز فنون عشوه گری ماهر است دوست</p></div>
<div class="m2"><p>دل از فنون عشوه گری سخت ماهر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایمغربی نو دیده بدست آر زانکه دوست</p></div>
<div class="m2"><p>چون آفتاب در رخ هر ذره ظاهر است</p></div></div>