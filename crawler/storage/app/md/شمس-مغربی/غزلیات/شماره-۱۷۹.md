---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>رخ دلدار را نقاب توئی</p></div>
<div class="m2"><p>چهره یار را حجاب توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتو پوشیده است مهر رخش</p></div>
<div class="m2"><p>ابر بر روی آفتاب توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد یقینم که پیش اهل یقین</p></div>
<div class="m2"><p>پرده شک و ارتیاب توئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر بحر بینهایت او</p></div>
<div class="m2"><p>سر بر آورده چون حباب توئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو سرابی به پیش اهل نظر</p></div>
<div class="m2"><p>گرچه دعوی کنی که آب توئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگرفتم ترا بهیچ حساب</p></div>
<div class="m2"><p>باز دیدم که در حساب توئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برتو است این عذاب گوناگون</p></div>
<div class="m2"><p>علت این همه عذاب توئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه ناخورده او می ازلی</p></div>
<div class="m2"><p>مست گردید و شد خراب توئی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی این خطاب با کس نیست</p></div>
<div class="m2"><p>آنکه با اوست اینخطاب توئی</p></div></div>