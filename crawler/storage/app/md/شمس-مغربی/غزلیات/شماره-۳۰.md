---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>با تو است آن یار دائم ور تو یکدم دور نیست</p></div>
<div class="m2"><p>گرچه تو مهجوری ازو، وی از تو مهجور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده بگشا تا ببینی آفتاب روی او</p></div>
<div class="m2"><p>کافتاب روی او از دیده ها مستور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لیک رویش را بنور روی او دیدن توان</p></div>
<div class="m2"><p>گرچه مانع دیده را از دیدنش جز نور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنّت ارباب دل رخسار جانان دیدن است</p></div>
<div class="m2"><p>در چنین جنّت که گفتیم زنجبیل و حور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ترا دیدار او باید برآ بر طور دل</p></div>
<div class="m2"><p>حاجت رفتن چو موسی سوی کوه طور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو کتابی در تو مسطور است علم و هر چه هست</p></div>
<div class="m2"><p>چیست آن کاو در کتاب و لوح تو مسطور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کور آن باشد که او بینا بنفس خود نشد</p></div>
<div class="m2"><p>کان که او بینا بنفس خویشتن شد، کور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناصر منصور گوید انا الحق المبین</p></div>
<div class="m2"><p>بشنو از ناصر که آن گفتار از منصور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی را یار شمس مغربی خواند بنام</p></div>
<div class="m2"><p>گرچه شمس مغربی اندر جهان مستور نیست</p></div></div>