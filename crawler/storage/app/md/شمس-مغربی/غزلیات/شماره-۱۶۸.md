---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>سبو بشکن که آبی بی‌سبوئی</p></div>
<div class="m2"><p>زخود بگذر که دریایی نه‌جویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفر کن از من و مائی و مائی</p></div>
<div class="m2"><p>گذر کن از تو و اوئی که اوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا چون آس گرد خود نگردی</p></div>
<div class="m2"><p>چو آب آشفته سرگردان چو جوئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پشیمانی بود در هرزه گردی</p></div>
<div class="m2"><p>پشیمانی بود در سو بسوئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو باری از خود اندر خود سفر کن</p></div>
<div class="m2"><p>بگرد عالم اندر چند پوئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خود او را طلب هرگز نگردی</p></div>
<div class="m2"><p>اگر چه سالها در جست‌جویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرامی بینی و از خود نپرسی</p></div>
<div class="m2"><p>کرا گم کرده آخر نکوئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلاه فقر را برسر نیابی</p></div>
<div class="m2"><p>مگر وقتی که ترک سر بگویی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کجا فقر را بر سر نیابی</p></div>
<div class="m2"><p>مگر وقتی که ترک سر بگوئی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا برکوی او رفتن توانی</p></div>
<div class="m2"><p>که طفلی در پی چوگان و گویی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو یکرو شو که آیینه چو طومار</p></div>
<div class="m2"><p>سیه رو کرد آخر از دوروئی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نصیب ایمغربی از خوان وصلش</p></div>
<div class="m2"><p>نیابی تا که دست از خود نشوئی</p></div></div>