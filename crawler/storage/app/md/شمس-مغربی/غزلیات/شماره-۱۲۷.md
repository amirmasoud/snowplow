---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>ما از میان خلق کناری گرفته ایم</p></div>
<div class="m2"><p>واندر کنار خویش نگاری گرفته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن نخست بر همه عالم فشانده ایم</p></div>
<div class="m2"><p>وانگه بصدق دامن یاری گرفته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر قوت و طعمه شاهین جان و دل</p></div>
<div class="m2"><p>از مرغزا  قدس شکاری گرفته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگشته گشته ایم‌ چو پرگار سالها</p></div>
<div class="m2"><p>تا بر مسال نقطه قراری گرفته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد بار برون جسته ایم از حصار تن</p></div>
<div class="m2"><p>تا بهر جان خویش حصاری گرفته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر میان گرد به مردی رسیده ایم</p></div>
<div class="m2"><p>تا عاقبت عنان سواری گرفته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با آنکه هیچ کار نیاید ز مغربی</p></div>
<div class="m2"><p>او را بیاری از پی کاری گرفته ایم</p></div></div>