---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>ای همه صفات من آینه صفات تو</p></div>
<div class="m2"><p>نیست حیات من بجز شعبه ای از حیات تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام جهان نمای من صورت توست گرچه هست</p></div>
<div class="m2"><p>جام جهان نمای تو صورت کائنات تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج توئی، طلسم من ذات تویی و اسم من</p></div>
<div class="m2"><p>حل شده از ظهور تو جمله مشکلات تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عدم و وجود خودخفته بدم سحرگهی</p></div>
<div class="m2"><p>داد ندای بندگی حی علی الصلات تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زو در عقل خاستم چونکه شنیدم این ندا</p></div>
<div class="m2"><p>عشق فکنده خلعتی در برم از صفات تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی وجود آمدم خوش به سجود آمدم</p></div>
<div class="m2"><p>بود سجودگاه من مسجد کائنات تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مسجد کائنات تو بود پر از جماعتی</p></div>
<div class="m2"><p>جمله گرفته سربسر صورت مبدعات تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لوح وجود سربسر پر ز حروف و نقش شد</p></div>
<div class="m2"><p>گشت مفصلا عیان جمله مجملات تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت جهان آب و گل نقش جهان جان دل</p></div>
<div class="m2"><p>گشت جهان جان و دل نقش صفات ذات تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یوسف جان چو دور ماند از پدر وجود خویش</p></div>
<div class="m2"><p>کرد مقیدش بکل مصر تو و بنات تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در جهتی از آن جهت در جهتش طلب کنی</p></div>
<div class="m2"><p>بی جهتی بببینی ار محو شود جهات تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بود وجود مغربی لات و منات او بود</p></div>
<div class="m2"><p>نیست بتی چو بود او در همه سومنات تو</p></div></div>