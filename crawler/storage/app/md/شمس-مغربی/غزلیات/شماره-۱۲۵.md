---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>ز چشم مست ساقی من ‌‌‌‌‌خرابم</p></div>
<div class="m2"><p>نه آخر بیخود از جام شرابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن ساعت که دیدم جام رویش</p></div>
<div class="m2"><p>چو مویش روز و شب در پیچ و تابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارم هیچ آرامی و خوابی</p></div>
<div class="m2"><p>که چشم او ربود آرام و خوابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی از ناله ام چون چرخ دولاب</p></div>
<div class="m2"><p>که از سرگشتگی چون آسیابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بجای اشک خون میبارم از چشم</p></div>
<div class="m2"><p>نمان اندر جگر چون هیچ تابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا عشقت چنان گم کرد از من</p></div>
<div class="m2"><p>که من خود را اگر جویم نیابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا عشق تو فانی کرد از من</p></div>
<div class="m2"><p>چو دید از خود بغایت در عذابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان باقی شدم اکنون بعشقت</p></div>
<div class="m2"><p>که بی عشق تو چیزی در نیابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون از مغربی رستم بکلی</p></div>
<div class="m2"><p>که از مشرق برآمد آفتابم</p></div></div>