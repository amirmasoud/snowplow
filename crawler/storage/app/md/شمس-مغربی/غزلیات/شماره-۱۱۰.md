---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>تا شراب عشق از جام ازل کردیم نوش</p></div>
<div class="m2"><p>تا ابد هرگز نخواهیم آمد از مستی بهوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد آوازی بگوش جان از جانان ما</p></div>
<div class="m2"><p>ما بر آن آواز تا اکنون نهادستیم گوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سماع قولِ کُن وز نغمه روز الست</p></div>
<div class="m2"><p>نیست جان ما دمی خالی ز فریاد و خروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا درده شرابی کز شرار آتشش</p></div>
<div class="m2"><p>چون خم و دیگی و دل جان آید از گرمی بجوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده گر بهر آن صدره گرو کرده است پیش</p></div>
<div class="m2"><p>خویشتن را پیر ما در پیش یار میفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی هر ساعت بنقشی مینماید آن نگار</p></div>
<div class="m2"><p>مرد میباید که تا بشناسد او را در نقوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد جمال وحدتش را کثرت عالم حجاب</p></div>
<div class="m2"><p>روی او را نقشهای مختلف شد روی پوش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی تواند یافتن در پیش یار خویش یار</p></div>
<div class="m2"><p>هر که یار هر دو عالم را ندید ز دوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از زبان مغربی آن یار میگوید سخن</p></div>
<div class="m2"><p>مدتی باشد که او شد از سخن گفتن خموش</p></div></div>