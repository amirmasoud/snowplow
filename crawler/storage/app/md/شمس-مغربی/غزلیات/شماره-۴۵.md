---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>از دهانش بسخن جز اثری نتوان یافت</p></div>
<div class="m2"><p>از میانش بمیان جز کمری نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمش چون قمری گفت بگو چون قمرم</p></div>
<div class="m2"><p>چونکه بر سرو روانی قمری نتوان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمش ماه و خوری گفت که بر چرخ چنین</p></div>
<div class="m2"><p>سرو قد زهره جبین ماه خوری نتوان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر زلف وی اخبار دلم پرسیدم</p></div>
<div class="m2"><p>گفت از گمشده تو خبری نتوان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شده همچو نسیم سحری بی سر و پای</p></div>
<div class="m2"><p>سحری بر سر کویش گذری نتوان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست خالی نفسی روی تو از جلوه گری</p></div>
<div class="m2"><p>همچو رویت بجهان جلوه گری نتوات یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته بودی که تو بر ما دگری بگزیدی</p></div>
<div class="m2"><p>چون گزینم که بحسنت دگری نتوان یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر تیر غم عشقش سپری میجستم</p></div>
<div class="m2"><p>گفت جانا که به از من سپری نتوان یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی آینه سان تا نشوی پاک و لطیف</p></div>
<div class="m2"><p>سوی خو  هیچ ز خوبان نظری نتوان یافت</p></div></div>