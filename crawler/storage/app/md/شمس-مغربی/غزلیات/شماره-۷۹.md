---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>بی نقاب آن جمال نتوان دید</p></div>
<div class="m2"><p>در رخش جز مثال نتوان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی او را بزلف و خال توان</p></div>
<div class="m2"><p>دید بی زلف و خال نتوان دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخیالش از آن شدم قانع</p></div>
<div class="m2"><p>که از او جز خیال نتوان دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود جمال کمال روی ترا</p></div>
<div class="m2"><p>بی حجاب جلال نتوان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذات مخفی است از صفات کمال</p></div>
<div class="m2"><p>بی صفات کمال نتوان دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابی است در ظلال نهان</p></div>
<div class="m2"><p>زو بغیر از ظلال نتواندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپذیرد زوال مهر رخش</p></div>
<div class="m2"><p>مهر او را زوال نتواندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه گرد سراب میگردیم</p></div>
<div class="m2"><p>چونکه آب زلال نتواندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی هیچ چیز از آن عنقا</p></div>
<div class="m2"><p>بجز از پر و بال نتوان دید</p></div></div>