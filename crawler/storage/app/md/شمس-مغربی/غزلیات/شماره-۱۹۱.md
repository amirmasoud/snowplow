---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>ترا که دیده نباشد نظر چگونه کنی</p></div>
<div class="m2"><p>بدین قدم که تو داری سفر چگونه کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا که هیچ ز احوال خود خبر نبود</p></div>
<div class="m2"><p>بگو ز حال خود دیگران را خبر چگونه کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکره هیچ مریدی چگونه شیخ شوی</p></div>
<div class="m2"><p>پسر نبوده کسی را پدر چگونه کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا که نیست خبر از جهان زیر و زبر</p></div>
<div class="m2"><p>ز زیر عزم جهان زبر چگونه کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نکرده محو فراموش نقش لوح وجود</p></div>
<div class="m2"><p>حدیث عشق ندانم زبر چگونه کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نیست هیچ وقوفت ز صنعت اکسیر</p></div>
<div class="m2"><p>به‌پیش اهل نظر مس زر چگونه کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشته کوکب وار صفت مسخر و مایل</p></div>
<div class="m2"><p>نه مشتری و ز زهره قمر چگونه کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمغربی چو رسیدی روان روان بگذر</p></div>
<div class="m2"><p>از او نبرده نصیبی گذر چگونه کنی</p></div></div>