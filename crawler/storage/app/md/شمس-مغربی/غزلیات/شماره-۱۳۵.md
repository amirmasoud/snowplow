---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>دلبری دارم که در فرمان او باشد دلم</p></div>
<div class="m2"><p>همچو گوئی در خم چوگان او باشد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان هر جا که می خواهد دلم را میبرد</p></div>
<div class="m2"><p>زان سبب پیوسته سر‌گردان او باشد دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ با خود می‌نیاید تا بکی گوئی چنین</p></div>
<div class="m2"><p>واله و آشفته و حیران او باشد دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرضه عالم چو نیک آید که چوگان او</p></div>
<div class="m2"><p>لاجرم میدان که جولان او باشد دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بهر نقشی که او خواهد برآید هر زمان</p></div>
<div class="m2"><p>کان در او گوهر ز بحر و کان او باشد دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر مهمانی دل خوان تجلی میدنهد</p></div>
<div class="m2"><p>هر زمان از بهر آن مهمان او باشد دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چونکه گردد موج زن دریای بی پایان او</p></div>
<div class="m2"><p>ساحل دریای بی پایان او باشد دلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لولو و مرجان او خواهی ز بحز دل طلب</p></div>
<div class="m2"><p>زانکه بحر لولو و مرجان او باشد دلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی از بحر و ساحل بیش ازین چیزی مگوی</p></div>
<div class="m2"><p>زانکه دائم قلزم و عمان او باشد دلم</p></div></div>