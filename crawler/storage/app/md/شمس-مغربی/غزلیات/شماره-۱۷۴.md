---
title: >-
    شمارهٔ ۱۷۴
---
# شمارهٔ ۱۷۴

<div class="b" id="bn1"><div class="m1"><p>مرا به خلوت جان دلبریست پنهانی</p></div>
<div class="m2"><p>که هست جان دلم در جمال او فانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آنمقام که جانان جمال بنماید</p></div>
<div class="m2"><p>بود مقام دل و جان فنل و حیرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سریر سلطنت ذات ایزدیست دلم</p></div>
<div class="m2"><p>چنانکه عرش مجید است عرش رحمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا به حسن و جمال آنچنان که ثانی نیست</p></div>
<div class="m2"><p>مرا بعشق تو هم نیست در جهان ثانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا برم دل و جان را که در مقام فنا</p></div>
<div class="m2"><p>تو هم دلی بحقیقت مرا و هم جانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز‌من تو جمله ربودی و جمله ام گشتی</p></div>
<div class="m2"><p>چو جمله ام توئی اکنون مرا چه میخوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توئی مرا بدل دل اگر چه دلداری</p></div>
<div class="m2"><p>توئی مرا عوض جان اگرچه جانانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز چشم من همه و اکنون توئی که میبینی</p></div>
<div class="m2"><p>ز عقل من همه اکنون توئی که میدانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مغربی بشنو بعد ازین اگر شنوی</p></div>
<div class="m2"><p>ز او ندای انالحق و قول سبحانی</p></div></div>