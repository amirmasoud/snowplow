---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ایجمال تو در جهان مشهور</p></div>
<div class="m2"><p>لیکن از چشم انس و جان مستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور رویت بدید ها نزدیک</p></div>
<div class="m2"><p>لیکن از دیدنش نظر ها دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر گرمی کجا کند ادراک</p></div>
<div class="m2"><p>ز آفتاب منیر تابان کور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه باشد عیان چه شاید دید</p></div>
<div class="m2"><p>قرص خورشید را بدیده مور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم بتو میتوان ترا دیدن</p></div>
<div class="m2"><p>بل توئی ناظر و توئی منظور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی این گمان همیبردم</p></div>
<div class="m2"><p>که منم ذاکر و تویی مذکور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد یقینم کنون که غیر توست</p></div>
<div class="m2"><p>ذاکر و ذکر و شاکر و مشکور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر رویت چو تافت بر عالم</p></div>
<div class="m2"><p>یافت ذرّات کاینات ظهور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشت پیدا از عکس زلف و رخت</p></div>
<div class="m2"><p>در جهان کفر و دین و ظلمت و نور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لب شیرین او چشم فتانت</p></div>
<div class="m2"><p>در زمانه فکنده متنه و شور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مغربی را مدام تز لب و چشم</p></div>
<div class="m2"><p>در جهان مست دارد و مخمور</p></div></div>