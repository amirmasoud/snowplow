---
title: >-
    شمارهٔ ۱۲۱
---
# شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>بر دو عالم پادشاهی میکنم</p></div>
<div class="m2"><p>گرچه از ایزد گدایی می کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده حقم خداوند جهان</p></div>
<div class="m2"><p>بر جهان زو کدخدایی می کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مر سما ره چون زمین طی کرده ام</p></div>
<div class="m2"><p>بر زمین اکنون سمائی میکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دو عالم را ز پس بگذاشتیم</p></div>
<div class="m2"><p>تا که اکنون پیشوایی میکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارم از وجهی به عالم اتصال</p></div>
<div class="m2"><p>گرچه از عالم جدائی میکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان پس از بیگانگی از کائنات</p></div>
<div class="m2"><p>گاه گاهی آشنایی میکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خستگان را نوشدارو میدهم</p></div>
<div class="m2"><p>بستگاهن را درگشائی میکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لا تظن انّی فقیر مفلس</p></div>
<div class="m2"><p>چون بگنجت راهنمایی میکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مغربی مرده افسرده را</p></div>
<div class="m2"><p>روح بخشی جانفزائی میکنم</p></div></div>