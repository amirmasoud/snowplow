---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>اگر ز جانب ما ذلت و نیاز نباشد</p></div>
<div class="m2"><p>جمال روی ترا هیچ عز و ناز نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سوز عاشق بیچاره است ساز جمالت</p></div>
<div class="m2"><p>جمال را اگر آن سوز نیست ساز نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پیش ناز تو گر ما نیاوریم نیازی</p></div>
<div class="m2"><p>میان عاشق و معشوق امتیاز نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعشق ما بطرز جمال حسن تو دائم</p></div>
<div class="m2"><p>لباس حسن ترا به از این طراز نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجا شود بحقیقت عیان جمال حقیقت</p></div>
<div class="m2"><p>اگر مظاهر آیینه مجاز نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجوی در دل ما غیر دوست زانکه نیابی</p></div>
<div class="m2"><p>از آنکه دردل محمود جز ایاز نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوازشی نتوان از کس دگر طلبیدن</p></div>
<div class="m2"><p>اگرچنانچه دلارام و دلنواز نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برای این دل بیچاره مغربی تو بگو</p></div>
<div class="m2"><p>چه چاره سازم اگر یار چاره ساز نباشد</p></div></div>