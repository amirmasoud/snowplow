---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>صنما چرا نقاب از رخ خود نمیگشائی</p></div>
<div class="m2"><p>زکه رخ نهفته داری ز‌چه رو نمینمائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخت چو کس نگاهی نفکند غیر دیده</p></div>
<div class="m2"><p>چه شوی نهان ز دیده که ت عین دیده بانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو دل از منی و مائی نگذشت شد عیانش</p></div>
<div class="m2"><p>که توئی و اوئی و توئی من و مائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به‌هزار دیده خواهم که نظر کنم برویت</p></div>
<div class="m2"><p>به‌هزار کسوت ای‌جان چو تو هر زمان برآیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ اگر چنین نمائی همه وقت عاشقان را</p></div>
<div class="m2"><p>عجب ار نداندت کس که او از کجایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو اگرچه بس عیانی ز ره صفت ولیکن</p></div>
<div class="m2"><p>ز همه جهان جهانی بحجاب کبریائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشود کسی عراقی به حقایق عراقی</p></div>
<div class="m2"><p>نشود کسی سنائی به معارف سنائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشنو حدیث آنکس که به‌عشوه گفت با تو</p></div>
<div class="m2"><p>پسرا ره قلندر سزد ار بمن نمایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پسرا اگر هوای سر کوی دوست داری</p></div>
<div class="m2"><p>مگذار مغربی را مگزین ازو جدائی</p></div></div>