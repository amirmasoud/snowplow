---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>نخست دیده طلب کن پس آنگهی دیدار</p></div>
<div class="m2"><p>از آنکه یار کند جلوه بر الولابصار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترا که دیده نباشد کجا توانی دید</p></div>
<div class="m2"><p>بگاه عرض تجلی جمال چهره یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه جمله پرتو فروغ حسن ویست</p></div>
<div class="m2"><p>ولی چو دیده نباشد کجا شود نظار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا که دیده نباشد چه حاصل از شاهد</p></div>
<div class="m2"><p>ترا که گوش نباشد چه حاصل از گفتار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا که دیده پر غبار بود نتوانی</p></div>
<div class="m2"><p>ضفای چهره او دید با وجود غبار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرچه آینه داری برای حسن رخش</p></div>
<div class="m2"><p>غبار شرک که تا پاک کرد از زنگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نگار تو آینه طلب دارد</p></div>
<div class="m2"><p>روان تو دیده دل را به پیش دل بیدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمال حسن ترا صد هزار زیب افزود</p></div>
<div class="m2"><p>از آنکه حسن ترا مغز نیست آینه دار</p></div></div>