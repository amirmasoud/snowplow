---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>بیدل و دلدار نتوانم نشست</p></div>
<div class="m2"><p>بیجمال یار نتوانم نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صحبت یارم چه می آید بدست</p></div>
<div class="m2"><p>پیش با اغیار نتوانم نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقیم چون چشم مست او بود</p></div>
<div class="m2"><p>یک زمان هشیار نتوانم نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بت و زنّار، زلف روی اوست</p></div>
<div class="m2"><p>بی بت و زنّار نتوانم نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس امید وعده دیدار گل</p></div>
<div class="m2"><p>بیش از این با خار نتوانم نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل آسا در گلستان رخش</p></div>
<div class="m2"><p>یکدم از گفتار نتوانم نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار باز آمد ببازار ظهور</p></div>
<div class="m2"><p>گفت بی بازار نتوانم نشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زانکه در خلوتسرای خویشتن</p></div>
<div class="m2"><p>بی الوالابصار نتوانم نشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون هزاران کار دارد هر زمان</p></div>
<div class="m2"><p>یکزمان بیکار نتوانم نشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برفکندم پرده از رخسار خویش</p></div>
<div class="m2"><p>پرده بر رخسار نتوانم نشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مغربی را گفت، بنگر بر رخم</p></div>
<div class="m2"><p>زانکه بی نظاره نتوانم نشست</p></div></div>