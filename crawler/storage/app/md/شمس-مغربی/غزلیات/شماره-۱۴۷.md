---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>ایدل اینجا کوی جانان است از جان دم مزن</p></div>
<div class="m2"><p>از دل و جان جهان در پیش جانان دم مزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو مرد درد اویی هیچ از درمان مگو</p></div>
<div class="m2"><p>درد او را به ز درمان دان ز درمان دم مزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفر ایمان را به اهل کفر و ایمان واگذار</p></div>
<div class="m2"><p>باش مستغرق درو از کفر و ایمان دم مزن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب بدوز از گفتگو چون نیست وقت گفتگوی</p></div>
<div class="m2"><p>جای حیران است در وی باش حیران دم مزن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون یقین آید رها کن قصه شک و گمان</p></div>
<div class="m2"><p>چون عیان بنمود رخ دیگر ز برهان دم مزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصه کوران به پیش بینا مگوی</p></div>
<div class="m2"><p>بیش ازین در پیش بینایان ز کوران دم مزن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علم بیدینان رها کن جهل حکمت را مجوی</p></div>
<div class="m2"><p>از خیالات و ظنون اهل یونان دم مزن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب حیوان را گر انسانی بحیوانی کن رها</p></div>
<div class="m2"><p>پیش دریای حیات از آب حیوان دم مزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وصل و هجران نیست الّا وصف خاص عاشقان</p></div>
<div class="m2"><p>مغربی گر عارفی از وصل و هجران دم مزن</p></div></div>