---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>نظرت فی رمقی نظره فصا ز فداک</p></div>
<div class="m2"><p>وصلتی بوجودی وجدت ذاتک ذاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظرت فیک شهود او ما شهدت سوای</p></div>
<div class="m2"><p>نظرت فی وجود او ما وجدت سواک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اذا جلوت علینا محبته و رضاک</p></div>
<div class="m2"><p>وجدت عینک فینا فاننا مجلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترا هر آینه چون رخ تمام ننماید</p></div>
<div class="m2"><p>یکی هر آینه باید تمام صافی و پاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم که آینه دارم از دو کَون تمام</p></div>
<div class="m2"><p>توئی که کرده خود را درو تمام ادراک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که جلوه گه روی جانفزای توام</p></div>
<div class="m2"><p>بدست خویش جلا ده برار از گل و خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کسی که هست بوصل تو دائماً خرّم</p></div>
<div class="m2"><p>روا مدار ز هجر تو دائماً غمناک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا بناز چو پرورده مکن به نیاز</p></div>
<div class="m2"><p>که از برای نجاتم نه از برای هلاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>منم که نور توام کی ز نار اندیشم</p></div>
<div class="m2"><p>ز نار هر که بترسد بود خس و خاشاک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز رشمن است همه باک مغربی ور نه</p></div>
<div class="m2"><p>همه جهان چو بود دوستش ز دوست چه باک</p></div></div>