---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>گنج های بینهایت یافتم در کنج دل</p></div>
<div class="m2"><p>کنج جانرا بین که چون شد کان گنج بیکران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان من از عالم نام و نشان آمد برون</p></div>
<div class="m2"><p>بی نشان شد تا درآمد در جهان بی کران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کی آمد در حزاب آباد دل کنجی بدید</p></div>
<div class="m2"><p>تا خراب آباد دل شد سربسر  معموراران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چونکه شهرستان دل معمور شد در هر نفس</p></div>
<div class="m2"><p>کاروان ها گردد از حق سوی شهرستان روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل نبرده هیچ رنجی برسر گنجی رسید</p></div>
<div class="m2"><p>آمدش تا که بدست از غیب کنجی بیکران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شب تاریک در زمین دل فرود آمد ز چرخ</p></div>
<div class="m2"><p>تا زمین را بگذرانید از هزاران آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تجلی کرد مهر مشرقی در مغربی</p></div>
<div class="m2"><p>مغربی را جمله ذرات عالم شد نهان</p></div></div>