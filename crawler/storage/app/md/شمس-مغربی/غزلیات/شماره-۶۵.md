---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>نشان و نام مرا روزگار کی داند</p></div>
<div class="m2"><p>صفات و ذات مرا غیر یار کی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسیکه هستی خود را بخود بپوشاند</p></div>
<div class="m2"><p>دگر کسیش بجز از کردگار کی داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا که گمشده ام در تو، کس کجا یابد</p></div>
<div class="m2"><p>که غرق بحر ترا در کنار کی داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که نور نیم اهل نور کی داند</p></div>
<div class="m2"><p>مرا که نار نیم اهل نار کی داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو من زهر دو جهان رَخت خویش برچیدم</p></div>
<div class="m2"><p>بروز حشر از اهل شمار کی داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا که نیست شدم در تو، هست نشناسد</p></div>
<div class="m2"><p>مرا که مست توام، هشیار کی داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیش آنکه یکی دید صد هزار بگو</p></div>
<div class="m2"><p>ندیده غیر یکی صد هزار کی داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی که اسیر دل و جان و عقل و نفس بود</p></div>
<div class="m2"><p>مرا که رسته ام از هر چهار کی داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز مغربی خبری کز حصار کَون دهید</p></div>
<div class="m2"><p>کسیکه هست اسیر حصار کی داند</p></div></div>