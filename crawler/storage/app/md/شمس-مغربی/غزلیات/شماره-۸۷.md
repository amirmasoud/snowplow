---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>می حدیثی از لب ساقی روایت می کند</p></div>
<div class="m2"><p>باده از سرمستی چشمش حکایت میکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حدیث مستی چشمش دلم سرمست شد</p></div>
<div class="m2"><p>قصهمستان مگر تا چون سرایت میکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بدایت داشت جانم مشتی از جام لبش</p></div>
<div class="m2"><p>در نهایت زان سبب میل بدایت میکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست زلفش گشت در تاراج ملک جان دراز</p></div>
<div class="m2"><p>این تطاول بین که در شهر ولایت میکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکر ها دارد دلم از لعل شکر بار او</p></div>
<div class="m2"><p>گرچه از زلف پریشانش شکایت میکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مست دلنوازش بین که در مستی خویش</p></div>
<div class="m2"><p>جانب دلرا رعایت تا چه غایت میکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این کفایت بین که پیش خدمت جانان بصدق</p></div>
<div class="m2"><p>هر که یکدل می بود جانان کفایت میکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کسی دارن از بهر حمایت جانبی</p></div>
<div class="m2"><p>مغربی را چشم سرمستش حمایت میکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکس که نهان بود ز ما آمد و ما شد</p></div>
<div class="m2"><p>وانکس که ز ما بود و شما ما و شما شد</p></div></div>