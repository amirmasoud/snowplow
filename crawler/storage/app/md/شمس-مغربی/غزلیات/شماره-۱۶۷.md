---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>زد حلقه دوش بر دل ما یار معنوی</p></div>
<div class="m2"><p>گفتم که کیست گفت که در باز کن توی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که من چگونه توام گفت ما یکیم</p></div>
<div class="m2"><p>از بهر روی پوش نهان گشته در دوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و منی و او و توئی شد حجاب تو</p></div>
<div class="m2"><p>از خود بدینحجاب چو محجوب میشوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که ما و او بشناسی که چون یکیست</p></div>
<div class="m2"><p>بگذار زین منی و ازین مایی و توی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذر از این جهان که درین کهنه و نو است</p></div>
<div class="m2"><p>آنگه ببین یکیست درین کهنه و نوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش و نگار نقش نگار است بیگمان</p></div>
<div class="m2"><p>مائی نهان شده است درین نقش با توی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز مطربی بدان که درین پرتو خوش سر است</p></div>
<div class="m2"><p>گر صد هزار نغمه و اواز بشنوی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نی‌نی غلط که مهر سپهر حقیقتی</p></div>
<div class="m2"><p>گرچه گهی چو ذرّه و گاهی چو پرتوی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایمغربی تو سایه خورشید مشرقی</p></div>
<div class="m2"><p>زان سایه وارد ز پی خورشید میدوی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنچه تو جویای آنی گر شوی بی‌تو توئی</p></div>
<div class="m2"><p>در مثال سایه خورشید در پی میدوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا تو غیری را تصور کرده‌ای جویای من</p></div>
<div class="m2"><p>کی توانی گشت یکتا با چنین شرک و دوی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دیده بگشا باری اندر خود نظر کن گر کنی</p></div>
<div class="m2"><p>در جمالت وحدت خود شو چو یکتا میشوی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عزلتی گر زانکه میگیری بگیر از خویشتن</p></div>
<div class="m2"><p>منزوی گر میشوی باری هم از خود منزوی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا هر آنجا هست که میجوئی ز ‌خود گردد روا</p></div>
<div class="m2"><p>تا هر آنچیزی که میپرسی هم از خود بشنوی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رهروانرا راه به پایان کجا پایان رسد</p></div>
<div class="m2"><p>تا بساط راه بار هرو نگردد منزوی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رهرو و ره را بدور انداز بی هر دو برو</p></div>
<div class="m2"><p>چونکه میدانی حجاب تست راه رهروی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا تو با خویشی گدا و بینوا و مفلسی</p></div>
<div class="m2"><p>تا تو بی‌خویشی قباد و کیقباد و خسروی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرچه از خورشید تابان نیست پرتو منفصل</p></div>
<div class="m2"><p>مغربی را خود تو خورشیدی و خود پرتوی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>الغرض در مقطع از مطلع شهیدی آورم</p></div>
<div class="m2"><p>آنچه تو جویای آنی گر شوی بیخود توی</p></div></div>