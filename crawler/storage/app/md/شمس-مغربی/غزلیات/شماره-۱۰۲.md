---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>میکند بر دل تجلی مهر رویش هر نفس</p></div>
<div class="m2"><p>تا که گردد نور ماه دل ز مهرش مقتبس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه عالم خوانمش خورشید او راسایه است</p></div>
<div class="m2"><p>در حقیقت سایه و خورشید یک چیزند و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم عنقابین مگس  را نیست زان نشناسدش</p></div>
<div class="m2"><p>گرچه عنقا را بچشم خود عیان بیند مگس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده بگشا بر سر خوان خلیل شه نشین</p></div>
<div class="m2"><p>بهره از سر خلقت جو نه از نان و عدس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبلا اندر قفس گلشن ز یادت رفته است</p></div>
<div class="m2"><p>چند گویم قصه گلشن بمرغی در قفس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لقمه مردان نمیشاید بطفلی باز داد</p></div>
<div class="m2"><p>سرّ سلطان را نشاید گفت هرگز با عسس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرّ دریا را بقطره چند گویی مغربی</p></div>
<div class="m2"><p>رو زبان بر بند از ین گونه سخنها سپس</p></div></div>