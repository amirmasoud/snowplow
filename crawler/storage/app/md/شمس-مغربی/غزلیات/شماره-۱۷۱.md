---
title: >-
    شمارهٔ ۱۷۱
---
# شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>دارد نشان یارم هر دلبری و یاری</p></div>
<div class="m2"><p>بینم جمال رویش از روی هر نگاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز روی او نبینم از روی هر نگاری</p></div>
<div class="m2"><p>جز خط او نخوانم از خط هر عذاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عکسی از آن جمال است هر حسن و هر جمالی</p></div>
<div class="m2"><p>نقشی از آن نگار است هر نقش و هر نگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>او در دیار خاتم بوده همیشه ساکن</p></div>
<div class="m2"><p>من گشته در پی او سرگشته هر دیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون یار در دل من دائم قرار دارد</p></div>
<div class="m2"><p>پس از چه رو ندارد دل یک‌زمان قراری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دست برفشاند من جان براو فشانم</p></div>
<div class="m2"><p>نبود ز بهر جانان بهتر ز‌جان نثاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر میروی رها کن دلرا بیادگارت</p></div>
<div class="m2"><p>خوش باش ار بماند از دوست یادگاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جویبار گیتی بخرام تا بروید</p></div>
<div class="m2"><p>از سرو قامت تو هر سرو جویباری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز شمار دانم اندر حساب نایم</p></div>
<div class="m2"><p>از سرو قامت تو هر سرو جویباری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جایی که هردو عالم از هیچ کمترانند</p></div>
<div class="m2"><p>من خود چه چیز باشم یا همچو من بزاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روی ترا بیارم از هیچ کمترانند</p></div>
<div class="m2"><p>از رهگذار عالم بر دیده ام غباری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با گلشن جمالت خاریست هر دو عالم</p></div>
<div class="m2"><p>تا کی رسی به گلشن تا نگذری ز خاری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا گشته نیست هستیت بر کنج ره بمانی</p></div>
<div class="m2"><p>زان رو که تا تو هستی بر کنج اوست ماری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگذار مغربی را تا در میان درآید</p></div>
<div class="m2"><p>تا او درین میانست از تست برکناری</p></div></div>