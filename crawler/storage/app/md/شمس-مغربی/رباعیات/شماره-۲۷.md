---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که در ملک فنا آمده ای</p></div>
<div class="m2"><p>در ملک فنا بی بقا آمده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عالم حق بدین سرا آمده ای</p></div>
<div class="m2"><p>بنگر زکجا تا بکجا آمده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خالی نشوی یکنفس از علم و عمل</p></div>
<div class="m2"><p>گر زانکه بدانی که چرا آمده ای</p></div></div>