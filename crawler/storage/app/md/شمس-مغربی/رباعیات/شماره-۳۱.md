---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>هر نغمه که از هزار دستان شنوی</p></div>
<div class="m2"><p>آن را بحقیقت از گلستان شنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ناله که از باده پرستان شنوی</p></div>
<div class="m2"><p>آن می‌گوید ولی ز مستان شنوی</p></div></div>