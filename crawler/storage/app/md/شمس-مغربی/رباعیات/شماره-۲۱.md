---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای مهر رخت مظهر ذرّات دو کون</p></div>
<div class="m2"><p>ذاتت بصفت معیین ذات دو کون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وی داده به نیستی جمالت هستی</p></div>
<div class="m2"><p>ای کرده ز نفی عین اثبات دو کون</p></div></div>