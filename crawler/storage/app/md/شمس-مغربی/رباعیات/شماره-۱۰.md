---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>من مست و خراب و می پرست آمده ام</p></div>
<div class="m2"><p>مدهوش زباده الست آمده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ظن نبری که باز گردم هشیار</p></div>
<div class="m2"><p>هم هست شوم از آنکه مست آمده‌ام</p></div></div>