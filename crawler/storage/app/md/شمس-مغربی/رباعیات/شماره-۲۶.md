---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>تو مظهر مرآت خدا آمده ای</p></div>
<div class="m2"><p>آیینه وجه کبریا آمده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما بجمال خود تجلی کرده</p></div>
<div class="m2"><p>از حضرت خود بدین سرا آمده ای</p></div></div>