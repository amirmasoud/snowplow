---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>من دانه خال زلف چون دام  توام</p></div>
<div class="m2"><p>من آینه روی دلارام توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیمانه باده غم انجام توام</p></div>
<div class="m2"><p>هم جام جهان نمای و هم جام توام</p></div></div>