---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>در خانقه از بهر جهت میبویی</p></div>
<div class="m2"><p>در وی همه ذکر از این جهت میگویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر جهتی ز بیجهت بیخبری</p></div>
<div class="m2"><p>بگذر ز جهت چو بیجهت میجویی</p></div></div>