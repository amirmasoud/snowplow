---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>باما نتوان گفت چرا آمده ای</p></div>
<div class="m2"><p>با خود تو ای که و از کجا آمده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه ببازی و هوا مشغولی</p></div>
<div class="m2"><p>گوئی که ببازی و هوا آمده ای</p></div></div>