---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>از پیش خدا بهر خدا  آمده</p></div>
<div class="m2"><p>نی از پی بازی و هوا آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در معرفت و عبادت ایزد کوش</p></div>
<div class="m2"><p>کز بهر همین درین سرا آمده</p></div></div>