---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>گاه گاه بنفس خویش در پیچم من</p></div>
<div class="m2"><p>بینم چو رشته جمله در پیچم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی دعوی او کنم که من هیچ نیم</p></div>
<div class="m2"><p>با آنکه چو باز بنگرم هیچم من</p></div></div>