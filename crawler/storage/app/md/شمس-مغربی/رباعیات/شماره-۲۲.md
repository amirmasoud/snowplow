---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>تو مست خود و ما همه مست بتو</p></div>
<div class="m2"><p>تو هست خود و ما همه هست بتو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نسبت ما بتو بود از همه روی</p></div>
<div class="m2"><p>دادیم ازین سبب همه دست بتو</p></div></div>