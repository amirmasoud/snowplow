---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>بوئی از بوستان همی آید</p></div>
<div class="m2"><p>راحتی در روان همی آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده باد گشته ام کز وی</p></div>
<div class="m2"><p>بوی زلف فلان همی آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز دل بر فصول می پیچد</p></div>
<div class="m2"><p>عشق بر بوی جان همی آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش گلبرگ عارض تو ز شرم</p></div>
<div class="m2"><p>غنچه بسته دهان همی آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزر و سیم غره شد نرگس</p></div>
<div class="m2"><p>که چنین سرگران همی آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رمقی مانده از دل و غم عشق</p></div>
<div class="m2"><p>بتقاضای آن همی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه ترتیب مهد می سازد</p></div>
<div class="m2"><p>که صبا ناتوان همی آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هم ز خنده خجل شود روزی</p></div>
<div class="m2"><p>گل که خنده زنان همی آید</p></div></div>