---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>بنام ایزد جهان همچون بهشتست</p></div>
<div class="m2"><p>که خرم موسم اردیبهشتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین از سبزه گوئی آسمانست</p></div>
<div class="m2"><p>درخت از جامه پنداری فرشتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بصحرا شو تماشا را سوی باغ</p></div>
<div class="m2"><p>که روز بوستان و وقت کشتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه طوطی طرب را بال سستست</p></div>
<div class="m2"><p>نه طاوس چمن را پای زشتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریفان همچو نرگس مست خفته</p></div>
<div class="m2"><p>کلاه از زر ولی بالین زخشتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتاده مست عاشق در بر گل</p></div>
<div class="m2"><p>قبا آنجا کلاه اینجا بهشتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهشت ار نیست جای او مخور غم</p></div>
<div class="m2"><p>بنقد امروز باری در بهشتست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنفشه در چمن مانند خطیست</p></div>
<div class="m2"><p>که بنده در مدیح شه نبشتست</p></div></div>