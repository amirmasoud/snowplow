---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>نز هجر توأم بجان امانی هست</p></div>
<div class="m2"><p>نه وصل ترا بدل نشانی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جانم بگداخت در فراق آری</p></div>
<div class="m2"><p>جانی اگرم امید جانی هست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونان شده ام که گر مرا بینی</p></div>
<div class="m2"><p>شبهت فتدت که این فلانی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من طیره شوم چو تو کمر بندی</p></div>
<div class="m2"><p>یعنی که ترا مگر میانی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی ندهم بجان یکی بوسه</p></div>
<div class="m2"><p>آری مده ار در آن زیانی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود هیچ سخن مگوی با عاشق</p></div>
<div class="m2"><p>تا کس بنداندت دهانی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میکن زجفا هر آنچه بتوانی</p></div>
<div class="m2"><p>کاخر پس ازین جهان جهانی هست</p></div></div>