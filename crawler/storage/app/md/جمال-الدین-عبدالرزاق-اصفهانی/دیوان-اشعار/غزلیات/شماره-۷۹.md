---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>دلبرم تا ز من نهان باشد</p></div>
<div class="m2"><p>جوی خون بر رخم روان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نهان باشد او زمن چه عجب</p></div>
<div class="m2"><p>کوچوجانست و جان نهان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار بی خوی خوش نکو ناید</p></div>
<div class="m2"><p>ور همه ماه آسمان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وای آندل که پیش او آید</p></div>
<div class="m2"><p>دل چه باشد که بیم جان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم آخر بوصل تو برسم</p></div>
<div class="m2"><p>گفت آری در آن جهان باشد</p></div></div>