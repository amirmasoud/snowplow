---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>زنجیر چو آنزلف پراکنده نباشد</p></div>
<div class="m2"><p>خورشید چو آنعارض رخشنده نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید که باشد که ترا بنده نباشد</p></div>
<div class="m2"><p>زنجیر چو آنزلف سرافکنده نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی که تو برگرد گلت طره فشانی</p></div>
<div class="m2"><p>خورشید که باشد که ترا بنده نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانجا که نهی ناوک غمزه بکمان در</p></div>
<div class="m2"><p>مه کیست که از تو سپر افکنده نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش لب خندان تو گل گرچه بخندد</p></div>
<div class="m2"><p>خود داند کان خنده چو این خنده نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زینگونه بخون ریختن اردست بر آری</p></div>
<div class="m2"><p>ترسم که دگر سال کسی زنده نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین حسن مشو غره که بازار گل سرخ</p></div>
<div class="m2"><p>بس تیز بود لیکن پاینده نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببرید سرزلف و سزا کرد و بگفتم</p></div>
<div class="m2"><p>ما را مکش ایزلف که فرخنده نباشد</p></div></div>