---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>بی توام کار بر نمیآید</p></div>
<div class="m2"><p>بر من این غم بسر نمیآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترسم از تن بدر شود جانم</p></div>
<div class="m2"><p>کز درم دوست در نمیآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو دلدار دورگشت از من</p></div>
<div class="m2"><p>نیک و بد زو خبر نمیآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شبی تا بروزم از غم تو</p></div>
<div class="m2"><p>مژه بر یکدگر نمیآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میکنم جهد تا بپوشم حال</p></div>
<div class="m2"><p>دیده با اشک بر نمیآید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بننالم ز هیچ بد روزی</p></div>
<div class="m2"><p>کم ازان بد بتر نمیآید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز بگذشت و هم نیامدیار</p></div>
<div class="m2"><p>تو چه گوئی مگر نمیآید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این همه یارب سحرگاهی</p></div>
<div class="m2"><p>خود یکی کارگر نمیآید</p></div></div>