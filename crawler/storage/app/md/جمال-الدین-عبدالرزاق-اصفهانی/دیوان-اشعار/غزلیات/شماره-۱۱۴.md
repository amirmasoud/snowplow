---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>در رخ یار خویشتن خندم</p></div>
<div class="m2"><p>برگل و لاله و سمن خندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگه آن سر و قد خرام کند</p></div>
<div class="m2"><p>بر قد سرو در چمن خندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم از عشق خون همی گریم</p></div>
<div class="m2"><p>گفت من بر چنین سخن خندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاکی ازدوست رغم دشمن را</p></div>
<div class="m2"><p>چون بباید گریستن خندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی از ظن بیهده مگری</p></div>
<div class="m2"><p>چون توی گریی چه سود، من خندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو بصد دیده میگری چو نشمع</p></div>
<div class="m2"><p>تا چو گل من بصد دهن خندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من مسکین بدست چو نتو حریف</p></div>
<div class="m2"><p>گر نگریم بخویشتن خندم</p></div></div>