---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>بیگانه وار یار ز من بگذرد همی</p></div>
<div class="m2"><p>من میکنم سلام و بمن ننگرد همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود هیچ التفات بمردم نمیکند</p></div>
<div class="m2"><p>ما را بهیچ روی بکس نشمرد همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر قصه که دل بنویسد زهجر او</p></div>
<div class="m2"><p>چشمم بدست اشک همه بسترد همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آری کنند جور بعشاق پر ولیک</p></div>
<div class="m2"><p>او خود زحد قاعده بیرون برد همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عشق شرط نیست شکایت ز یار خویش</p></div>
<div class="m2"><p>ور چه مرا فراق بدان آورد همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر هر صفت که باشد جانی همیکنم</p></div>
<div class="m2"><p>کاینمایه عمر ناخوش و خوش بگذرد همی</p></div></div>