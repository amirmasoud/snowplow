---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>سخت آشفته جمال خودی</p></div>
<div class="m2"><p>ورچه نوعیست این زبیخردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصد جانم چرا کنی چندین</p></div>
<div class="m2"><p>نه بدین شرط دل همی ستدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من این شکل میکنی یا خود</p></div>
<div class="m2"><p>با همه کس چنین ترانه زدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دمم بی وفا همی خوانی</p></div>
<div class="m2"><p>راست گفتی هزار بار خودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چه نیکی بجای من کردی</p></div>
<div class="m2"><p>تا چه کردم بجای تو ز بدی؟</p></div></div>