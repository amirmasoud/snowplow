---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>غمش در دل تنگ ما می‌نشیند</p></div>
<div class="m2"><p>ندانم بر آتش چرا می‌نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم نیز مستوجب هر غمی هست</p></div>
<div class="m2"><p>که بر شاهراه بلا می‌نشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا بر سر آتشی می‌نشاند</p></div>
<div class="m2"><p>چو بینم که با ناسزا می‌نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا گفت با دیگری می‌نشینی</p></div>
<div class="m2"><p>ندانم که این بر کجا می‌نشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر بر چه بندد نداند نگارم؟</p></div>
<div class="m2"><p>که این بار بر جان ما می‌نشیند</p></div></div>