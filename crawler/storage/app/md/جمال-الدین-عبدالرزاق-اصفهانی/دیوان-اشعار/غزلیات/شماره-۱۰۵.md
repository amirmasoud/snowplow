---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>چگونه عاشقی را جان بماند</p></div>
<div class="m2"><p>که چندین روز بی جانان بماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغا جان که رفت اندر سردل</p></div>
<div class="m2"><p>بدل راضی شدم گر جان بماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهجرت هر شبی چندان بنالم</p></div>
<div class="m2"><p>کز آه من فلک حیران بماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دیده اشک چندانی برانم</p></div>
<div class="m2"><p>که چرخ از آب سرگردان بماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تو چشم وفا هرگز ندارم</p></div>
<div class="m2"><p>جفا کن تا توانی کان بماند</p></div></div>