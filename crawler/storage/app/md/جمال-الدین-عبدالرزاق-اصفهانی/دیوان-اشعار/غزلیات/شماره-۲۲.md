---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>امروز بتم بطبع خود نیست</p></div>
<div class="m2"><p>با ما سخنش بنیک و بد نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر ز برم براند و آن کیست</p></div>
<div class="m2"><p>کانداخته چنین لگد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکبوسه ازو بخواستم گفت</p></div>
<div class="m2"><p>بس دل که درو بسی خرد نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوچک دهنش بدید نتوان</p></div>
<div class="m2"><p>چون بوسه دهم برانکه خود نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز ندهد نشان ز شادی</p></div>
<div class="m2"><p>هرکو بغم تو نامزد نیست</p></div></div>