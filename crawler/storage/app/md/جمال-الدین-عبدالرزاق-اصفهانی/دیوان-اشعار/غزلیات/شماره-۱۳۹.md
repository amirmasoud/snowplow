---
title: >-
    شمارهٔ ۱۳۹
---
# شمارهٔ ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>سخن بی غرض از من بشنو</p></div>
<div class="m2"><p>بر دشمن مشو ایدوست مشو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دشمنان راه بدت آموزند</p></div>
<div class="m2"><p>مشنو هرزه دشمن مشنو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو امشب ندبی خواهم باخت</p></div>
<div class="m2"><p>جان زمن بوسه ز تو خصل وگرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گوئی تو که خیزم بروم</p></div>
<div class="m2"><p>دل من وا ده و برخیز وبرو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چه شیوه است که بنهادی باز</p></div>
<div class="m2"><p>وین چه راهست که آوردی نو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بیک چند درآی از در من</p></div>
<div class="m2"><p>چند دشنام بده گرم و بدو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویم ای خام چو کاهم ز غمت</p></div>
<div class="m2"><p>گوئی ای سوخته بر من بدوجو</p></div></div>