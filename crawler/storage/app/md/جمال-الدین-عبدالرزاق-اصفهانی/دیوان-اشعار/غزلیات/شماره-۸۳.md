---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>عشقت آتش در آب داند زد</p></div>
<div class="m2"><p>نرگست راه خواب داند زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف دلبند تو بدل بردن</p></div>
<div class="m2"><p>پایها بر صواب داند زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گره از غالیه تواند بست</p></div>
<div class="m2"><p>حلقه از مشگ ناب داند زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نمکدان لب از همه کاری</p></div>
<div class="m2"><p>نمکی بر کباب داند زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود نداند نواخت چون چنگم</p></div>
<div class="m2"><p>همه همچون رباب داند زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لب لعل تو در طرب زائی</p></div>
<div class="m2"><p>طعنه ها در شراب داند زد</p></div></div>