---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>رخ برون از پس نقاب مده</p></div>
<div class="m2"><p>بیش ازین شرم آفتاب مده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب خرگوش اگر دهی مارا</p></div>
<div class="m2"><p>جز ازان چشم نیم خواب مده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنگان وصال را چو دهی</p></div>
<div class="m2"><p>بجز از راه دیده آب مده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چیست عشوه بهر یکی بوسه</p></div>
<div class="m2"><p>نه خدائی تو؟ کم عذاب مده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بصد دوستی ز من بستان</p></div>
<div class="m2"><p>پس سلام مرا جواب مده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با حریفان خویش خوش بنشین</p></div>
<div class="m2"><p>بار ما خود بهیچ باب مده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما بجان بوسه همی خواهیم</p></div>
<div class="m2"><p>گر نبینی همی صواب مده</p></div></div>