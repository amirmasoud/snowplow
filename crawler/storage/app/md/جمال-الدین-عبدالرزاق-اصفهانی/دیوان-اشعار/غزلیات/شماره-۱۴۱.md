---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>دلم بستان و آنگه عشوه میده</p></div>
<div class="m2"><p>چنین خواهم زهی نامهربان زه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بقصد خون من زینسان چرائی</p></div>
<div class="m2"><p>کمان ابروان آورده در زه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میم، هر لحظه رو بر من ترش کن</p></div>
<div class="m2"><p>گلم، هر لحظه ام خاری دگرنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا زین پس مگو فردا و فردا</p></div>
<div class="m2"><p>کزین عشوه نخواهم گشت فربه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفتی ترایم گر مرائی</p></div>
<div class="m2"><p>همین شیوه، ازینم پرده میده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تو بوسی طلب کردم ندادی</p></div>
<div class="m2"><p>تو جان خواهی و نتوان گفتنت نه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گوئی ترک جان و دل بگویم</p></div>
<div class="m2"><p>بدین مایه رهم از تو مرا به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه قصدت بجانم بود و بردی</p></div>
<div class="m2"><p>ازان فارغ شدی الحمدلله</p></div></div>