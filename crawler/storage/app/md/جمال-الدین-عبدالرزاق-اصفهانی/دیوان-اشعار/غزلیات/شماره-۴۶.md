---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>هیچ کس را هوش عشق تو درسر نشود</p></div>
<div class="m2"><p>کش غم هجر تو با مرگ برابر نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان کشت مرا گر طمع وصل کنم</p></div>
<div class="m2"><p>هیچ عاشق بچنین جرمی کافی نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان زمن خواهی ودانی که محابانکنم</p></div>
<div class="m2"><p>بوسه خواهم و دانم که میسر نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دل و دوست بدردم من و میباید ساخت</p></div>
<div class="m2"><p>که کسی از دل و از دوست بداور نشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صفت درد دل من زسر زلف بپرس</p></div>
<div class="m2"><p>گر ترا از من دلسوخته باور نشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق روی تو شد دل چه ملامت کنمش</p></div>
<div class="m2"><p>بچنان رخ که تو داری چکندگر نشود</p></div></div>