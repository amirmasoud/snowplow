---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>ساعتی از عشق تو بی غم نیم</p></div>
<div class="m2"><p>بیغم و اندوه تو یکدم نیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبر و دل و جان بتو دادم کنون</p></div>
<div class="m2"><p>از همه محرومم و محرم نیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این همه جان کنده ام از بهر وصل</p></div>
<div class="m2"><p>هم ز غم هجر مسلم نیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ مبادم ز جهان خرمی</p></div>
<div class="m2"><p>گر من از اندوه تو خرم نیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سایه خود بر من بیدل فکن</p></div>
<div class="m2"><p>کاخر ازین خاک زمین کم نیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جور مکن بر دل من بیش ازین</p></div>
<div class="m2"><p>گیر که من در همه عالم نیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت که بوسه دهمت رایگان</p></div>
<div class="m2"><p>نی که چنین خام طمع هم نیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویم مردم ز غمت گویدم</p></div>
<div class="m2"><p>من چکنم عیسی مریم نیم</p></div></div>