---
title: >-
    شمارهٔ ۱۳۰
---
# شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>هر جور که من ز یار می بینم</p></div>
<div class="m2"><p>از نامه روزگار می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیشی نه بکام دل همیرانم</p></div>
<div class="m2"><p>رنجی نه باختیار می بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون ریزی وعده های او دیدم</p></div>
<div class="m2"><p>جان دادن انتظار می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بخت بدست این نه از عشقست</p></div>
<div class="m2"><p>من عاشق صدهزار می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان از غم عشق او نخواهم برد</p></div>
<div class="m2"><p>میدانم و روی کار می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آخر این حدیث میخوانم</p></div>
<div class="m2"><p>من حاصل این شمار می بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامردی صبر خویش میدانم</p></div>
<div class="m2"><p>بی رحمی آن نگار می بینم</p></div></div>