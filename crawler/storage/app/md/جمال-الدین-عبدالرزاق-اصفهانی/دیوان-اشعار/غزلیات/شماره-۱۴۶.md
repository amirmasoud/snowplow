---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>زهی روی تو خار گل نهاده</p></div>
<div class="m2"><p>قد تو کو شمال سرو داده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهت چون آفتاب افتاده در پای</p></div>
<div class="m2"><p>بسر چون سایه پیشت ایستاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بهر عشوه ما وعده تو</p></div>
<div class="m2"><p>دری ز امروز بر فردا گشاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز اشگ چشم من خیزد تف دل</p></div>
<div class="m2"><p>کسی دید آتشی از آب زاده؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شرم روی چون ماهت مه چرخ</p></div>
<div class="m2"><p>شود هر مه دو شب از خود پیاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بپیش روی خوبت چیست خورشید</p></div>
<div class="m2"><p>چراغی در ره بادی نهاده</p></div></div>