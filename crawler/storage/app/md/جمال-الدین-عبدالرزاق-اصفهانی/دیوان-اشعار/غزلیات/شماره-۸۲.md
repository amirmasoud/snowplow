---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>دل چو دم از دلربائی میزند</p></div>
<div class="m2"><p>عافیت را پشت پائی میزند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بازعاشق گشت و معذورست دل</p></div>
<div class="m2"><p>گرچه لاف از بیوفائی میزند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از میان موج خون چون غرقه</p></div>
<div class="m2"><p>دست هر ساعت بجائی میزند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر دمم دل پیش پائی مینهد</p></div>
<div class="m2"><p>هر زمانم غم قفائی میزند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غمت شادم که چون بیند مرا</p></div>
<div class="m2"><p>آخر از دل مرحبائی میزند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از همه عالم سر زلفین او</p></div>
<div class="m2"><p>زخم هم بر آشنائی میزند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه شد دل در سرکارش هنوز</p></div>
<div class="m2"><p>در غم او دست و پائی میزند</p></div></div>