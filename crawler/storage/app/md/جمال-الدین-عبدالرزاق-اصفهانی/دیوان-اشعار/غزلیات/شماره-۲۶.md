---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>چه عجب گر دلت زمن بگرفت</p></div>
<div class="m2"><p>که مرا دل زخویشتن بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم از ضعف آنچنان که مرا</p></div>
<div class="m2"><p>باد بربود و پیرهن بگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخنی با تو خواستم گفتن</p></div>
<div class="m2"><p>گریه خود راه بر سخن بگرفت</p></div></div>