---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>باز دل در غم جان می پیچد</p></div>
<div class="m2"><p>باز در عشق فلان می پیچد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب این باره کجا دارد عزم</p></div>
<div class="m2"><p>که دگرباره عنان می پیچد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه در زلف بتان پیچد ازان</p></div>
<div class="m2"><p>چون سر زلف بتان می پیچد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد از من دل و صبر و زر و سیم</p></div>
<div class="m2"><p>جان بماندست و در آن می پیچد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس ز دستش نزید تا غم او</p></div>
<div class="m2"><p>چون قضا در همگان میپیچد</p></div></div>