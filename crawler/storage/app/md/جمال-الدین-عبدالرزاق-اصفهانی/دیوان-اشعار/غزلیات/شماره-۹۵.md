---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>رفت آن کز لبت مرا می بود</p></div>
<div class="m2"><p>وز رخت بوسه ها پیاپی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاد باد آنکه از رخ تو مرا</p></div>
<div class="m2"><p>گل و نرگس شکفته در دی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو برطرف باغ پیش قدت</p></div>
<div class="m2"><p>صد کمر بسته راست چون نی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاله آتش زده میانه دل</p></div>
<div class="m2"><p>گل زشرم تو غرقه درخوی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی از من ببوسه قانع شود</p></div>
<div class="m2"><p>از تو خود این توقعم کی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبر روی از چه درکشید از من</p></div>
<div class="m2"><p>که همه پشت گرمی از وی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد حساب از تو برگفرت دلم</p></div>
<div class="m2"><p>چون فذلک بدید لاشی بود</p></div></div>