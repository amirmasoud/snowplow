---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>بی تو کارم همی بسر نشود</p></div>
<div class="m2"><p>دست کس با تو در کمر نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان خیال تو نایدم در چشم</p></div>
<div class="m2"><p>تا از آب دو دیده تر نشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا تو اندر نیائی از در من</p></div>
<div class="m2"><p>غم تو از دلم بدر نشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نگردی تو همچو من عاشق</p></div>
<div class="m2"><p>از غم من ترا خبر نشود</p></div></div>