---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>از تو بکبوسه همی درخواهم</p></div>
<div class="m2"><p>بده ایدوست که دیگر خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه خطا گفتم یک بوسه و بس</p></div>
<div class="m2"><p>بیشتر زین بسرت گر خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس چو من خام طمع نیست که من</p></div>
<div class="m2"><p>بی زر از لعل تو شکر خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان نهادم عوض بوسه او</p></div>
<div class="m2"><p>آه اگر گوید نی زر خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان نهادم عوض بوسه او</p></div>
<div class="m2"><p>آه اگر گوید نی زر خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکه وجه زرم ازرنگ رخست</p></div>
<div class="m2"><p>بچه دل بوسه ز دلبر خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شب زلف توام کافر تر</p></div>
<div class="m2"><p>گرمن این روز بکافر خواهم</p></div></div>