---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>هر کس که بعشق تو کمر بندد</p></div>
<div class="m2"><p>بس طرف که از رخ تو بر بندد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو ز رخ نقاب بگشاید</p></div>
<div class="m2"><p>تا عقل در فضول در بندد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن نرگس تو بجادوئی از دور</p></div>
<div class="m2"><p>صد خواب همی بیک نظر بندد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگ شکرست چشمه نوشت</p></div>
<div class="m2"><p>لعلت همه تب بدان شکر بندد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نیشکر ارچه صد حلاوت هست</p></div>
<div class="m2"><p>هم پیش لب تو صد کمر بندد</p></div></div>