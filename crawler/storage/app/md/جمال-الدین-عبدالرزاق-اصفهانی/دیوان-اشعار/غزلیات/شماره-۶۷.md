---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>دل را همه آن ز دست برخیزد</p></div>
<div class="m2"><p>کانگه که ز پا نشست برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هجر تو دل درآمدست از پای</p></div>
<div class="m2"><p>تا خود بکدام دست برخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس با تو شبی ز پای ننشیند</p></div>
<div class="m2"><p>تا از سر هر چه هست برخیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز بقصد جان صد عاشق</p></div>
<div class="m2"><p>آن سنبل گل پرست برخیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن لب چون میت عجب نبود</p></div>
<div class="m2"><p>چشم تو که نیم مست برخیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وصل تو گشاده روی بنشیند</p></div>
<div class="m2"><p>چون دید که دل ببست برخیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنجاه دل افکند بیک ساعت</p></div>
<div class="m2"><p>تیری که ترا ز شست برخیزد</p></div></div>