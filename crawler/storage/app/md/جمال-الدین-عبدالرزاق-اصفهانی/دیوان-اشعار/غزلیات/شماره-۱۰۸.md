---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>مرا دلیست نه در خوردِ من، که بستاند؟</p></div>
<div class="m2"><p>مرا ز دست دل خویشتن که بستاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر رخت نبود، دل ز بر، که برباید</p></div>
<div class="m2"><p>وگر لبت نبود، جان ز تن، که بستاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگر نه حسن تو بر ماه خط نویسد پس</p></div>
<div class="m2"><p>خراج تو ز گل و یاسمن که بستاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وگر مرا لب لعل تو یاریی ندهد</p></div>
<div class="m2"><p>ز زلف کافر تو داد من که بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در اشک غرقم و گویم که نیستم عاشق</p></div>
<div class="m2"><p>ز من ندانم تا این سخن که بستاند</p></div></div>