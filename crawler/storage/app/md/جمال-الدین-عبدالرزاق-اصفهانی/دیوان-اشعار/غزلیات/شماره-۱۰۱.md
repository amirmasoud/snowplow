---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>جان من گوئی زتن می بگسلد</p></div>
<div class="m2"><p>یار من هرگه زمن می بگسلد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته عهدی که خود بندد همی</p></div>
<div class="m2"><p>بی سبب هم خویشتن می بگسلد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تافکند او دامن اندر پای حسن</p></div>
<div class="m2"><p>سرو دامن از چمن می بگسلد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقبازی نیک داند دل و لیک</p></div>
<div class="m2"><p>اولین بازی رسن می بگسلد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ضعیفی گر همی نالم چونای</p></div>
<div class="m2"><p>همچو چنگم رگ ز تن می بگسلد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه گویم بوسه، میگوید که زر</p></div>
<div class="m2"><p>چون کنم اینجا سخن می بگسلد</p></div></div>