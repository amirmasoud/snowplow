---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>جور و جفای تو نیک و بد بسر آید</p></div>
<div class="m2"><p>خط تو آخر بدیر و زود برآید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناوک مژگان تو چو تیر سحرگه</p></div>
<div class="m2"><p>پوست ندارد خبر که بر جگر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه چوبیند رخت ز دست درافتد</p></div>
<div class="m2"><p>سرو چو بیند قدت زپای درآید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوی تو زین به شود که هست ولیکن</p></div>
<div class="m2"><p>کار بصبر و بروزگار برآید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو همه ناز بود و بی تو همه غم</p></div>
<div class="m2"><p>چون بسرآمد چنان چنین بسر آید</p></div></div>