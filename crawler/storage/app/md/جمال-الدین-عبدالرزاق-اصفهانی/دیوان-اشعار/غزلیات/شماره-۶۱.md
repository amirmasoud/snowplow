---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>از آه دلم قمر بسوزد</p></div>
<div class="m2"><p>وز نور رخت نظر بسوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عالم عشق مرغ جانرا</p></div>
<div class="m2"><p>اندر طلب تو پر بسوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شرم تو چون کمر ببندی</p></div>
<div class="m2"><p>جوزا بدرد کمر بسوزد</p></div></div>