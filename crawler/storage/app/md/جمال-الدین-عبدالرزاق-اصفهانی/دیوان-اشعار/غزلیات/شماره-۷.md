---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>بخوبی هیچکس چون یار ما نیست</p></div>
<div class="m2"><p>ولیکن در دلش بوی وفا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه سود ار تنک شکر شد دهانش</p></div>
<div class="m2"><p>که یک شکر ازان روزی ما نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخواهم بست دل در وصلت ایماه</p></div>
<div class="m2"><p>که وصل تو متاع هر گدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز من بیگانه گشتستی و کوئی</p></div>
<div class="m2"><p>که جز تو در جهانم آشنا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو خود دانی و من دانم ولیکن</p></div>
<div class="m2"><p>که یارد گفت چونین هست یا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعشقت هم نیارم کرد دعوی</p></div>
<div class="m2"><p>که زرخواهی و آن معنی مرا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جفا کن تا توانی کرد زیراک</p></div>
<div class="m2"><p>وفا در مذهب خوبان روانیست</p></div></div>