---
title: >-
    شمارهٔ ۱۳۱
---
# شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>روز و شب در هجر او غم میخورم</p></div>
<div class="m2"><p>وانده آن روی خرم میخورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صف دردی کشان درد او</p></div>
<div class="m2"><p>صدقدح خوشخوش بیکدم میخورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این قفاها بین که از دست غمش</p></div>
<div class="m2"><p>میخورم در هجر و محکم میخورم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغم او صد ملامت میکشم</p></div>
<div class="m2"><p>تا نگوئی غم مسلم میخورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش زلف تو دل از ما ببرد</p></div>
<div class="m2"><p>گفت ورجان میبرد غم میخورم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او چو چنگم در نوازش میزند</p></div>
<div class="m2"><p>من چو نی مینالم و دم میخورم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم بدم زین بیش نتوان زیستن</p></div>
<div class="m2"><p>ور دم عیسی مریم میخورم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چند توبت کردم ار غم خوردنش</p></div>
<div class="m2"><p>می ندارد فایده هم میخورم</p></div></div>