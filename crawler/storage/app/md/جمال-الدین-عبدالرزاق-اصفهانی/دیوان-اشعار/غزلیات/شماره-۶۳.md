---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>یاری که رخش ماه و قدش سرو روان بود</p></div>
<div class="m2"><p>دادیم بدو جان و دل و مصلحت آن بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دیدمش از دور بدانشکل و بدان قد</p></div>
<div class="m2"><p>گفتم که جفاکار بود راست چنان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فی الجمله مرا زیروزبر کرد که در عشق</p></div>
<div class="m2"><p>من سست عنان بودم و او سخت کمان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر هیچ ننالم زغمش گوید خاموش</p></div>
<div class="m2"><p>انصاف بده خامش ازین بیش توان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در من نگرد ناگه یعنی که ندانم</p></div>
<div class="m2"><p>گوید چه رسید او را بیچاره جوان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زودا که بانگشت بهم باز نمایند</p></div>
<div class="m2"><p>کاین گور فلانست که دربند فلان بود</p></div></div>