---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>ای ترک بیا و چنگ بنواز</p></div>
<div class="m2"><p>آهنگ بگیر و برکش آواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون مست شدی هنوز هم شرم؟</p></div>
<div class="m2"><p>از دست شدم هنوز هم ناز؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون چنگ تو زان خمیده پشتم</p></div>
<div class="m2"><p>تا روی بروی تو نهم باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برکش زتنم اگر رگی نیست</p></div>
<div class="m2"><p>اندر همه پرده یا تو دمساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چندین مزنم اگر نه چنگم</p></div>
<div class="m2"><p>ور میزنیم نخست بنواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی تو که عشق من نهاندار</p></div>
<div class="m2"><p>مگذار که فاش گردد این راز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگذاشتمی اگر نبودی</p></div>
<div class="m2"><p>رنگ رخ و آب دیده غماز</p></div></div>