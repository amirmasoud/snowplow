---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>دوش در گلستان سحرگاهی</p></div>
<div class="m2"><p>پرده برداشت غنچه ناگاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم بلبل بر او فتاد از دور</p></div>
<div class="m2"><p>کرد ربی و ربک اللهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل بصد لطف گفت خندانش</p></div>
<div class="m2"><p>برگ مهمان بساز یک ماهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت نرگس فدای مقدم گل</p></div>
<div class="m2"><p>شکل این شش ستاره و ماهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر گل دارم این ببار آری</p></div>
<div class="m2"><p>چه کند سیم؟ عمر کوتاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نرگس دوید بلبل و گفت</p></div>
<div class="m2"><p>که تو بر لشگر چمن شاهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقی مفلسم حریف بدست</p></div>
<div class="m2"><p>وجه یک ماه چاره راهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منم امروز از زر و سیمت</p></div>
<div class="m2"><p>وامخواهی برای دلخواهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بآن عاشقیت آموزم</p></div>
<div class="m2"><p>تا کنم مطربیت گه گاهی</p></div></div>