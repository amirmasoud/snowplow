---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>یکروز بکوی ما آخر گذرت افتد</p></div>
<div class="m2"><p>برحال من مسکین روزی نظرت افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر هیچ مرا بینی بس گوهر ناسفته</p></div>
<div class="m2"><p>کز نرگس بیمارت بر گلشکرت افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند برو بنشین در سایه زلف او</p></div>
<div class="m2"><p>باشد که ازو کاری در یکدگرت افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در سایه تو جایم چون باشد تا هر روز</p></div>
<div class="m2"><p>خورشید دوبار آید برخاک درت افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم که بجان بوسی در خشم شدی با من</p></div>
<div class="m2"><p>ایدوست ندانستم گفتم مگرت افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند ترا عادت خونریختنست ایجان</p></div>
<div class="m2"><p>با ما زسر عادت برخیز اگرت افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یارب که چو من بادی تو برد گری عاشق</p></div>
<div class="m2"><p>تا سنگدلی چون خودجائی بسرت افتد</p></div></div>