---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>عشقت ایدوست مرا همنفسست</p></div>
<div class="m2"><p>بی تو بر من همه عالم قفسست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حلقه زلف تو دل می گیرد</p></div>
<div class="m2"><p>در شب زلف تو حلقه عسست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من ز عشق تو کجا بگریزم</p></div>
<div class="m2"><p>کاشگم از پیش و غم تو زپسست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم تو میخورم و میگریم</p></div>
<div class="m2"><p>چون باشگی و غمی دسترسست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرگ نزدیک بمن چونان شد</p></div>
<div class="m2"><p>که میان من و او یک نفسست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که گوید که فلان را بنواز</p></div>
<div class="m2"><p>گوئی از خشم فلان خود چه کسست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خشم و دشنام تو در می باید</p></div>
<div class="m2"><p>طعنه دشمنم آخر نه بسست؟</p></div></div>