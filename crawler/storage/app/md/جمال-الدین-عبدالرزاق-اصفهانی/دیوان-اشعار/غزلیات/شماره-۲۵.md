---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>امروز چه بودش که ز من روی نهان داشت</p></div>
<div class="m2"><p>سراز چه سبب بر من بیچاره گران داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناکرده گنه باز ز من روی نهان کرد</p></div>
<div class="m2"><p>من هیچ نمیدانم او را که بر آن داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکبار بروی تو نگه کردم وایجان</p></div>
<div class="m2"><p>بنگر که بجان و دل و دیده چه زیان داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل در بر من نیست ندانم که کجا شد</p></div>
<div class="m2"><p>یارب بجهان در دل من نام و نشان داشت؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده چو ترا دید درو لعل برافشاند</p></div>
<div class="m2"><p>معذور همیدارش بیچاره همان داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتند سر زلف تو دل میبرد از خلق</p></div>
<div class="m2"><p>من در غم دل بودم و او قصد بجان داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردوست همیدارمت این طرفه نباشد</p></div>
<div class="m2"><p>آری بچنین روی ترا دوست توان داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زود ازمن و از تو همه این شهر بپرسند</p></div>
<div class="m2"><p>گویند نه این چشم فلانی بفلان داشت</p></div></div>