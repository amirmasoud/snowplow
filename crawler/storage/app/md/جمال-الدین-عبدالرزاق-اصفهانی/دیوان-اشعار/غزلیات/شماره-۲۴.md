---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>دوش آن صنم ز زانو سر برنمیگرفت</p></div>
<div class="m2"><p>با ما نفس نمیزد و ساغر نمیگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خشم رفته بود و ندانم سبب چه بود</p></div>
<div class="m2"><p>کان ماه لب بخنده زهم برنمیگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در عذر صد ترانه زدم تاکند قبول</p></div>
<div class="m2"><p>آهنگ ازانچه بود فراتر نمیگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چندین هزار لابه که کردم همی بدو</p></div>
<div class="m2"><p>یک ذره خود دران دل کافر نمیگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میگفتمش چه کرده ام آخر چه گفته ام</p></div>
<div class="m2"><p>البته نیک و بد سخن از سر نمیگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من پیش او بعذر بیک پای همچو سرو</p></div>
<div class="m2"><p>او درگرفته بود و سخن در نمیگرفت</p></div></div>