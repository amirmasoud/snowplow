---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>یک بار که لعل او سخن گفت</p></div>
<div class="m2"><p>بنگر که چه نغز و دلشکن گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سرد که دشمنی نگوید</p></div>
<div class="m2"><p>امروز بدوستی بمن گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد بار دروغ کرد وعده</p></div>
<div class="m2"><p>وانگاه مرا دروغ زن گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من این نه ازو شنیده ام کاین</p></div>
<div class="m2"><p>آن نرگس مست تیغ زن گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی نی که هر آنچه گفت با من</p></div>
<div class="m2"><p>حقا که بجای خویشتن گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خود همه کفر گفت دلبر</p></div>
<div class="m2"><p>آخر نه بدان لب و دهن گفت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشتست فراخ تنگ شکر</p></div>
<div class="m2"><p>تا شکر تنگ او سخن گفت</p></div></div>