---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>آن چیست که من از تو و عشق تو ندیدم</p></div>
<div class="m2"><p>وان چیست که در هجر تو از تو نشنیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>احسنت چنین کن همه خون دل من خور</p></div>
<div class="m2"><p>کاخر بگزافت ز جهان بر نگزیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتی و بر دشمن من خوش بنشستی</p></div>
<div class="m2"><p>آوخ بنمردم من و این نیز بدیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول ز تو و خوی تو عبرت نگرفتم</p></div>
<div class="m2"><p>تا عاقبت از تو بچنین روز رسیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل هم بستد نرگس جادوی تو وانگه</p></div>
<div class="m2"><p>صد حرز فروخواندم و بر خویش دمیدم</p></div></div>