---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>این مرا در جهان نه بس باشد</p></div>
<div class="m2"><p>که بدرد تو دسترس باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرزوی من از جهان غم تست</p></div>
<div class="m2"><p>هیچ کس را چنین هوس باشد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تو چون سخن ز من گویند</p></div>
<div class="m2"><p>گوئی از خشم کوچه کس باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه زان خواهم از لبت که شکر</p></div>
<div class="m2"><p>گه گهی طعمه مگس باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بتیغم زنی و دل گوید</p></div>
<div class="m2"><p>کز توأم این فتوح بس باشد</p></div></div>