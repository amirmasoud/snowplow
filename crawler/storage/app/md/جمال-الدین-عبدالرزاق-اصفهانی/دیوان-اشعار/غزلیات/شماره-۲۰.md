---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دل درد تو در میان جان بستست</p></div>
<div class="m2"><p>جان در طلب تو بر میان بستست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو ز دست در دلم آتش</p></div>
<div class="m2"><p>زان چشم من آب در جهان بستست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد چشمه خون گشاده ام بر رخ</p></div>
<div class="m2"><p>بر من در وصل همچنان بستست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دست قمر بگرد گلزارت</p></div>
<div class="m2"><p>از مشگ کمر بر ارغوان بستست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست فلک از کلف نقاب شرم</p></div>
<div class="m2"><p>بر چهره ماه آسمان بستست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایدوست بترس ازین دم سردم</p></div>
<div class="m2"><p>کاین دولت حسن تو دران بستست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرسبزی شاخ و سرخی رویش</p></div>
<div class="m2"><p>دریک دم سرد مهرگان بستست</p></div></div>