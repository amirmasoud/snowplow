---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>چه باشد اگر با همه دوستگاری</p></div>
<div class="m2"><p>مرا گوئی ای خسته چون میگذاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه با تو وصالی نه از تو سلامی</p></div>
<div class="m2"><p>بنام ایزد الحق چه فرخنده یاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی نوش صرفی گهی زهر نابی</p></div>
<div class="m2"><p>تو معشوقه نی مردم روزگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا دوست خوانی پسم بار ندهی</p></div>
<div class="m2"><p>زهی دوستداری زهی حق گذاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی جهد کردم که بگذاری این خو</p></div>
<div class="m2"><p>چو سودی نمیداشت هم سازگاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو گویم که بوسی تو گوئی که جانی</p></div>
<div class="m2"><p>بده بوس و بستان بدین جان چه داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمن بازده این دل ریش و رستیم</p></div>
<div class="m2"><p>تو از این تقاضا من از خواستاری</p></div></div>