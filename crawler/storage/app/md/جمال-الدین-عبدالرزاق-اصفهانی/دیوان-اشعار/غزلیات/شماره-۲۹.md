---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>وای ای دوست که بیوصل تو عیشم خوش نیست</p></div>
<div class="m2"><p>چو نبود خوشکه مرا آن دورخ مهوش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر رخت آتشی از عشق برافروخته اند</p></div>
<div class="m2"><p>کیست کش از پی دل نعل درین آتش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کند ماه که درششدرحسن از تو بماند</p></div>
<div class="m2"><p>که همه نقش مه و پروین بیش از شش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر یکبوسه که جان داده ام آنرا ببها</p></div>
<div class="m2"><p>اینهمه ناخوشی انصاف بده هم خوش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بی فایده فریاد کنم کاندر شهر</p></div>
<div class="m2"><p>هیچکس را غم این سوخته غمکش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هان بپرهیز ز تیر سحر من که مرا</p></div>
<div class="m2"><p>هیچ تیری بجز از یارب در ترکش نیست</p></div></div>