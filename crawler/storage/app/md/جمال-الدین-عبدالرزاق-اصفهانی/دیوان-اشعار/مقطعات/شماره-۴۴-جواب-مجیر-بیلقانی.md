---
title: >-
    شمارهٔ  ۴۴ - جواب مجیر بیلقانی
---
# شمارهٔ  ۴۴ - جواب مجیر بیلقانی

<div class="b" id="bn1"><div class="m1"><p>هجو میگوئی ای مجیرک هان</p></div>
<div class="m2"><p>تا ترا زین هجا بجان چه رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفاهان زبان نهادی باش</p></div>
<div class="m2"><p>تا سرت را ازین زبان چه رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند گوئی که در دقایق طبع</p></div>
<div class="m2"><p>خاطر اهل اصفهان چه رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>... در ... گنجه و تفلیس</p></div>
<div class="m2"><p>تا بشروان و بیلقان چه رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>... در ریش خواجه خاقانی</p></div>
<div class="m2"><p>تا بتو خام غلتبان چه رسد</p></div></div>