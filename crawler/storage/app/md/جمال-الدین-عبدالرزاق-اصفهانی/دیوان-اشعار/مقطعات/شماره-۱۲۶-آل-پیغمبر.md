---
title: >-
    شمارهٔ  ۱۲۶ - آل پیغمبر
---
# شمارهٔ  ۱۲۶ - آل پیغمبر

<div class="b" id="bn1"><div class="m1"><p>کاشکی برخاستستی روز حشر</p></div>
<div class="m2"><p>جمع گشتی باز این اجزای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ببینم آل پیغمبر بکام</p></div>
<div class="m2"><p>ورچه دوزخ بود خواهد جای من</p></div></div>