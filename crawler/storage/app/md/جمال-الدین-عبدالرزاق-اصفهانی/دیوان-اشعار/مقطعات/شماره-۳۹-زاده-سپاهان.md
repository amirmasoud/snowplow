---
title: >-
    شمارهٔ  ۳۹ - زاده سپاهان
---
# شمارهٔ  ۳۹ - زاده سپاهان

<div class="b" id="bn1"><div class="m1"><p>ای که همای کرم طبع تو</p></div>
<div class="m2"><p>عالم در سایه پر پرورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر نثار قدمت آفتاب</p></div>
<div class="m2"><p>در دل کان گوهر و زر پرورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردمک دیده شرعی ازان</p></div>
<div class="m2"><p>عقلت در دیده سر پرورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کر زچه خوانند صد فراازانک</p></div>
<div class="m2"><p>لفظ تو نشنیده گهر پرورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خوشی لفظ تو طرفی نی است</p></div>
<div class="m2"><p>تا بچه امید شکر پرورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>والله اگر مادر فضل و هنر</p></div>
<div class="m2"><p>مثل تو یکشخص دگر پرورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رسم قدر داری در تربیت</p></div>
<div class="m2"><p>کان پدر این نیز پسر پرورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون تو کنی تربیت از شرم تو</p></div>
<div class="m2"><p>شاخ سرافکنده ثمر پرورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه فکن بر من حیران که شاخ</p></div>
<div class="m2"><p>از نظر چشمه خور پرورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز تو مرا در همه عالم کسی</p></div>
<div class="m2"><p>بالله والله اگر پرورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مثل توئی تربیت من کند</p></div>
<div class="m2"><p>قرصه خورشید قمر پرورد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاد مراد خاک سپاهان ولیک</p></div>
<div class="m2"><p>خوی ندارد که پسر پرورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر چه شرر زاید ز آتش همی</p></div>
<div class="m2"><p>نیست بر آتش که شرر پرورد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خصم من ار گوید صدر جهان</p></div>
<div class="m2"><p>مردم را بهر هنر پرورد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نز هنر ذره بود کافتاب</p></div>
<div class="m2"><p>دایم در ظل نظر پرورد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آهوی آنکس چکند کو چو مشک</p></div>
<div class="m2"><p>مدح تو در خون جگر پرورد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فیض کف تست که طبعم چنین</p></div>
<div class="m2"><p>لفظ و معانی خوش و تر پرورد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گل همه زان دارد این رنگ و بوی</p></div>
<div class="m2"><p>کش نفس باد سحر پرورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدر من از مدح تو افزون شود</p></div>
<div class="m2"><p>ماه ز تأثیر سفر پرورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عقل اگر مدح چو تو سروری</p></div>
<div class="m2"><p>در من بی قدر و خطر پرورد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دور نباشد ز خرد کافتاب</p></div>
<div class="m2"><p>لعل اندر حجر حجر پرورد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرخ زمن عمر بر شوت ستد</p></div>
<div class="m2"><p>بر طمع بوک و مگر پرورد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رفت جوانی و نپرورد هیچ</p></div>
<div class="m2"><p>بعد خزان شاخ چه بر پرورد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ور بقضا فایت باز آورد</p></div>
<div class="m2"><p>گیر کز این پس چقدر پرورد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا که درین مهد بتحریک باد</p></div>
<div class="m2"><p>نامیه را شیر قدر پرورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بادی در غنچه عصمت که چرخ</p></div>
<div class="m2"><p>از گلت افروخته تر پرورد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فرخت اینعید که گردون عدوت</p></div>
<div class="m2"><p>از پی قربان تو بر پرورد</p></div></div>