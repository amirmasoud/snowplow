---
title: >-
    شمارهٔ  ۶۶ - وظیفه
---
# شمارهٔ  ۶۶ - وظیفه

<div class="b" id="bn1"><div class="m1"><p>شاعری را اگر دهی دشنام</p></div>
<div class="m2"><p>بر تو آنرا وظیفه پندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور قفائی خورد ز تو بمثل</p></div>
<div class="m2"><p>سر سال آن قفا طمع دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر امید وظایف مردم</p></div>
<div class="m2"><p>شب نباشد که روز نشمارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کرا راه و رسم این باشد</p></div>
<div class="m2"><p>بر تو مرسوم خویش بگذارد؟</p></div></div>