---
title: >-
    شمارهٔ  ۸۴ - مجیرالدین بیلقانی در مدح جمال الدین گفته
---
# شمارهٔ  ۸۴ - مجیرالدین بیلقانی در مدح جمال الدین گفته

<div class="b" id="bn1"><div class="m1"><p>قسم بواهب عقلی که پیش رای قدیم</p></div>
<div class="m2"><p>یکیست چشمه خورشید و سایه عنقاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشود بیکی امر او چو سایه بچاه</p></div>
<div class="m2"><p>در آبگون قفس این آفتاب آتش پاش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که هست طبع جمال آفتاب تأثیری</p></div>
<div class="m2"><p>که پیرویست کم از سایه گنبد خضراش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چو سایه سیه روی کرد و خانه نشین</p></div>
<div class="m2"><p>بنثر نثره صفت طبع آفتاب آساش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان بدست زبان آفتاب وار گشاد</p></div>
<div class="m2"><p>ازان فتاد معانی چو سایه اندرپاش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشد بزیر غمم همچو سایه زیر قدم</p></div>
<div class="m2"><p>زرشگ آنکه نشست آفتاب بر بالاش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان دوا زدمش برد اگر نه بگرفتی</p></div>
<div class="m2"><p>بسان سایه و خورشید دق و استسقاش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکست گوهر دریا و باد ابر نشاند</p></div>
<div class="m2"><p>بشعر چون گهر و طبع پاک چون دریاش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپید مهره طبعش چنان دمید چنان</p></div>
<div class="m2"><p>که رخنه خواست شد این سبز حقه از آواش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلطف اگر ید بیضا بدو نماید صبح</p></div>
<div class="m2"><p>بشکل شام گرفتست بی گمان سوداش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دلم زعقده غم چون میان بیت گشاد</p></div>
<div class="m2"><p>چو بستم از زبر دل قصیده غراش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسا که طره حورا دهد ز غیب بهشت</p></div>
<div class="m2"><p>بکلک سرزده مانند طره حوراش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بچشم مردم ازان گشت همچو مردم چشم</p></div>
<div class="m2"><p>که در سواد توان یافتن ید بیضاش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه لایقست باو مدح من که در خور نیست</p></div>
<div class="m2"><p>کلاه گوشه نرگس بچشم نابیناش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ثنای او چو مرا شد علاج جان نژند</p></div>
<div class="m2"><p>دعاش گویم و دانم که واجبست دعاش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیه سپیدی دوران قصیده بادا</p></div>
<div class="m2"><p>که او بود بهمه حال مقطع و مبداش</p></div></div>