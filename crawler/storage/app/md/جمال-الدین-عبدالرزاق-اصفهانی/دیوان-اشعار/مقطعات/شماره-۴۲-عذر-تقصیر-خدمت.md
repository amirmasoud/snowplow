---
title: >-
    شمارهٔ  ۴۲ - عذر تقصیر خدمت
---
# شمارهٔ  ۴۲ - عذر تقصیر خدمت

<div class="b" id="bn1"><div class="m1"><p>ای کریمی که همای نظرت</p></div>
<div class="m2"><p>بر ولی تو همایون آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پی شرم سخای تو حباب</p></div>
<div class="m2"><p>چون عرق بر رخ جیحون آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قبه هفتم با رفعت خویش</p></div>
<div class="m2"><p>پیش قدر تو چو هامون آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ در خون عدویت شد ازان</p></div>
<div class="m2"><p>صبح با جامه پر خون آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنده گر کرد بخدمت تقصیر</p></div>
<div class="m2"><p>تا نگوئی تو که بس دون آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانعی بود مر او را ظاهر</p></div>
<div class="m2"><p>بشنو این عذر که موزون آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاکرت چون ز قبول کرمت</p></div>
<div class="m2"><p>لایق حضرت میمون آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سربرافراشت بگردون شرف</p></div>
<div class="m2"><p>وینخبر چونسوی گردون آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواست حالی که نثاری سازد</p></div>
<div class="m2"><p>ورچه زان قدر من افزون آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه بر بنده فشاند اختر خویش</p></div>
<div class="m2"><p>وین نثاریست که اکنون آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همه پروین و نبات النعش است</p></div>
<div class="m2"><p>که زاشکال دگرگون آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه خطا گفتم به زین باید</p></div>
<div class="m2"><p>این خطا در سخنم چون آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبع من کز گهر مدحت تو</p></div>
<div class="m2"><p>صدف لؤلؤ مکنون آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در جهاداشت در او در که ازان</p></div>
<div class="m2"><p>هر یکی مایه قارون آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون ز مدح تو براندیشیدم</p></div>
<div class="m2"><p>بعضی از پوست به بیرون آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سر تو سبز و دلت خرم باد</p></div>
<div class="m2"><p>که رخ بخت تو گلگون آمد</p></div></div>