---
title: >-
    شمارهٔ  ۴۶ - غم خان و مان
---
# شمارهٔ  ۴۶ - غم خان و مان

<div class="b" id="bn1"><div class="m1"><p>تا کی غم خان و مان و فرزند</p></div>
<div class="m2"><p>چند انده نان و جامه تا چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه درین جهانی ای شیخ</p></div>
<div class="m2"><p>بر خویش گری و بر جهان خند</p></div></div>