---
title: >-
    شمارهٔ  ۱۱ - تواضع
---
# شمارهٔ  ۱۱ - تواضع

<div class="b" id="bn1"><div class="m1"><p>بر چو من بنده گر قیامی کرد</p></div>
<div class="m2"><p>آنکه مطلق جهان مستوفاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بدین مکرمت بزرگ شدم</p></div>
<div class="m2"><p>وز بلندی قدر او بنکاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکته دیگرست اینجا خرد</p></div>
<div class="m2"><p>که بدان نکته آن قیام رواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بقد حقیر یأجوجم</p></div>
<div class="m2"><p>بمن از بهر آن جهان برخاست</p></div></div>