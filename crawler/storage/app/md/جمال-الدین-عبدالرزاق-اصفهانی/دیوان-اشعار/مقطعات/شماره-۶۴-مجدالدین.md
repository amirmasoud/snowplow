---
title: >-
    شمارهٔ  ۶۴ - مجدالدین
---
# شمارهٔ  ۶۴ - مجدالدین

<div class="b" id="bn1"><div class="m1"><p>خداوندا تو آنشخصیکه چشم چرخ فیروزه</p></div>
<div class="m2"><p>نبیند در هزاران دور اگر چون تو بشر جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپهر مجد و بحر علم و کان جود مجدالدین</p></div>
<div class="m2"><p>که عقل کل زرای روشن تو راهبر جوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوقت بزم تو کان از کف رادت امان خواهد</p></div>
<div class="m2"><p>بروزرزم تو نصرت زشمشیرت ظفر جوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بهر مدحت تو تیر گردون کلک پیراید</p></div>
<div class="m2"><p>ز بهر خدمت تو چرخ چون جوزا کمر جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلطفت بلبل دلها همیشه میزند دستان</p></div>
<div class="m2"><p>ز نطقت طوطی جانها همه ساله شکر جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوقت عزم تو گردون برد از طبع تو سرعت</p></div>
<div class="m2"><p>بگاه حزم تو خورشید از رایت نظر جوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز از تو کیست در گیتی که او قدر هنر داند</p></div>
<div class="m2"><p>جز از تو کیست در عالم که او اهل هنر جوید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو من مدحتسرائی کو که دارد چون تو ممدوجی</p></div>
<div class="m2"><p>چو تو گوهرشناسی کو که مثل من گهر جوید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا تشریف فرمودی ولیکن دون قدر من</p></div>
<div class="m2"><p>مرا کس اینقدر بخشد ز تو کس اینقدر جوید؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم از فرط سخایت دان اگر این بنده مخلص</p></div>
<div class="m2"><p>همی زین پایه کوراست خود را بیشتر جوید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آنکس کو بمدح تو دهان بگشاد همچون گل</p></div>
<div class="m2"><p>عجب نبود اگر حالی چو نرگس تاج زر جوید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کسی کو چون تو مخدومی بدست آورد در عالم</p></div>
<div class="m2"><p>هم از دون همتی باشد گر از وی ما حضر جوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی کود کرد غواصی بدریائی پر از گوهر</p></div>
<div class="m2"><p>ز کوته دیدگی باشد که خرج مختصر جوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>من از تو نام گیرم زانکه ماه از مهر افزاید</p></div>
<div class="m2"><p>من از تو بوی جویم زانکه گل رنگ از قمر جوید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو کان جودی و ناچار رسم کان چنین باشد</p></div>
<div class="m2"><p>که چون زر بیشتر بخشد طمع زو بیشتر جوید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نباشد خام طبعی زارزوی عقل نزدیکت</p></div>
<div class="m2"><p>گر از دریا گهر خواهد ور از خورشید زر جوید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بمان در دولت جاوید و سرسبزی بکام دل</p></div>
<div class="m2"><p>همی تا چرخ فیروزه برین عالم گذر جوید</p></div></div>