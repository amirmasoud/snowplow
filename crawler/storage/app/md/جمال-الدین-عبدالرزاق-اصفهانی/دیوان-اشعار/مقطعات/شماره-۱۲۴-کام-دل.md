---
title: >-
    شمارهٔ  ۱۲۴ - کام دل
---
# شمارهٔ  ۱۲۴ - کام دل

<div class="b" id="bn1"><div class="m1"><p>چند گوئی که روز برنائی</p></div>
<div class="m2"><p>دستی آخر بکام دل برزن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بدین معطیان و مخدومان</p></div>
<div class="m2"><p>که نیرزند دانه ارزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست چون برزنم بکامه دل</p></div>
<div class="m2"><p>بچه دلگرمی آخر ای غرزن</p></div></div>