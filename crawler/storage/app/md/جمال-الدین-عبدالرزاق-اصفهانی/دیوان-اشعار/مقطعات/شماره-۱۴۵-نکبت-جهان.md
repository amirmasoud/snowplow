---
title: >-
    شمارهٔ  ۱۴۵ - نکبت جهان
---
# شمارهٔ  ۱۴۵ - نکبت جهان

<div class="b" id="bn1"><div class="m1"><p>اگر شلواربند مادر تو</p></div>
<div class="m2"><p>چو بند سفره تو بسته بودی،</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزایید آن جلب تو قلتبان را</p></div>
<div class="m2"><p>جهان از نکبت تو رَسته بودی</p></div></div>