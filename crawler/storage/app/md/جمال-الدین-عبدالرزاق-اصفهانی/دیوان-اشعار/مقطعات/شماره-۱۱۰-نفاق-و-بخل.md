---
title: >-
    شمارهٔ  ۱۱۰ - نفاق و بخل
---
# شمارهٔ  ۱۱۰ - نفاق و بخل

<div class="b" id="bn1"><div class="m1"><p>نفاق و بخل در اهل سپاهان</p></div>
<div class="m2"><p>چنان چون تشنگی در ریگ دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگ و خردشان دیدم وزایشان</p></div>
<div class="m2"><p>وفا در سگ کرم در دیگ دیدم</p></div></div>