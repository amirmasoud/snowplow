---
title: >-
    شمارهٔ  ۲۴ - قناعت
---
# شمارهٔ  ۲۴ - قناعت

<div class="b" id="bn1"><div class="m1"><p>تا حصه قناعت گشتست ملک من</p></div>
<div class="m2"><p>وا رسته ام زعشوه دو نان پیچ پیچ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستم حیات در همه عالم بآبروی</p></div>
<div class="m2"><p>زانرو که هیچ را نستایم بقصد هیچ</p></div></div>