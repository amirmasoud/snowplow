---
title: >-
    شمارهٔ  ۶۸ - دروغ
---
# شمارهٔ  ۶۸ - دروغ

<div class="b" id="bn1"><div class="m1"><p>الله الله مگرد گرد دروغ</p></div>
<div class="m2"><p>ور چه در گردن تو یوغ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکند هیچ خوب و زشت بقا</p></div>
<div class="m2"><p>گرش بنیاد بر دروغ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح کاذب اگر چه بفروزد</p></div>
<div class="m2"><p>مدتی اندکش فروغ بود</p></div></div>