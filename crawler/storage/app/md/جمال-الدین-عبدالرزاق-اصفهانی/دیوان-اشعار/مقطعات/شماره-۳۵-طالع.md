---
title: >-
    شمارهٔ  ۳۵ - طالع
---
# شمارهٔ  ۳۵ - طالع

<div class="b" id="bn1"><div class="m1"><p>نه بکوشش درست روزی خلق</p></div>
<div class="m2"><p>یا بجد و بجهد دادستند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تکاپوی رزق نفزاید</p></div>
<div class="m2"><p>ورچه هر کس دران فتادستند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانده بی برگ و بار سرو و چنار</p></div>
<div class="m2"><p>ور چه صد دست برگشادستند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز نرگس فکنده سر در پیش</p></div>
<div class="m2"><p>تاج زر بر سرش نهادستند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بدانی که طالعست همه</p></div>
<div class="m2"><p>هر کسی را بدانچه دادستند</p></div></div>