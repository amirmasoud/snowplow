---
title: >-
    شمارهٔ  ۹۹ - بزرگی در اصل
---
# شمارهٔ  ۹۹ - بزرگی در اصل

<div class="b" id="bn1"><div class="m1"><p>هر که در اصلش بزرگی بوده است</p></div>
<div class="m2"><p>آن ازو هرگز نگردد هیچ کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیل کو جز خدمت شاهی نکرد</p></div>
<div class="m2"><p>چون ز آسیب فنا گردد عدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاستخوان او اگر پیلی کنی</p></div>
<div class="m2"><p>خدمت شاهی کند او نیز هم</p></div></div>