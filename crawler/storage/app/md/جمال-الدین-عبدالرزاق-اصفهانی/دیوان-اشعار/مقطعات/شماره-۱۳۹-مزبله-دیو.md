---
title: >-
    شمارهٔ  ۱۳۹ - مزبله دیو
---
# شمارهٔ  ۱۳۹ - مزبله دیو

<div class="b" id="bn1"><div class="m1"><p>تا کی ایدل تو درین مزبله دیو زحرص</p></div>
<div class="m2"><p>خویشتن را زره عقل و خرد گم بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برجهانی چه نهی دل که زبس آزونیاز</p></div>
<div class="m2"><p>موج آفت را بر چرخ تلاطم بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند ازین بیخردان خیره ملامت شنوی</p></div>
<div class="m2"><p>چند ازین بیخبران هرزه تظلم بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زین خسان بی سببی چند مشقت یابی</p></div>
<div class="m2"><p>زین خران بی غرضی چند تحکم بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سرائی چه نهی رخت که در ساحت آن</p></div>
<div class="m2"><p>فتنه را تا بلب گور تصادم بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندرو بر علم رایت صبح صادق</p></div>
<div class="m2"><p>صبح کاذب را پیوسته تقدم بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در وی از ساقی غم درد دمادم نوشی</p></div>
<div class="m2"><p>بردل از بار شره زخم دمادم بینی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرهر با هنری زیر پی بی خردی</p></div>
<div class="m2"><p>پای هر بیخردی برسر انجم بینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در وی از ذره خاشاکی دردی یابی</p></div>
<div class="m2"><p>در وی از نیشتر پشه تألم بینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ظاهر از نغمه قمری همه کو کو شنوی</p></div>
<div class="m2"><p>حاصل از نوبت سلطان همه دم دم بینی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچنان فتنه دنیا مشوایدل که زحرص</p></div>
<div class="m2"><p>خار پشتی را در کسوت قاقم بینی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکجا دانککی هست سنانیست براو</p></div>
<div class="m2"><p>بعیان صورتش از خوشه گندم بینی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ژرف اگر درنگری بیشترین مردم عصر</p></div>
<div class="m2"><p>سگ بیدم یابی یاخر بی سم بینی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوستانرا همه چون نحل زافراط نفاق</p></div>
<div class="m2"><p>نوشی و نیشی اندر دم وبردم بینی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مردمی میطلبی گرد جهان نیک برآی</p></div>
<div class="m2"><p>بخدای اربجهان صورت مردم بینی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بازکن دیده عبرت نگر و معنی بین</p></div>
<div class="m2"><p>تا همه خوک و سگ و گربه و کژدم بینی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خیز و از زاویه فقر قناعت اندوز</p></div>
<div class="m2"><p>تا ز بی برگی انواع تنعم بینی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اندراو در دهن شیر سلامت یابی</p></div>
<div class="m2"><p>وندر او دردل شمشیر ترحم بینی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمع را بیجگر گرم زرفشان یابی</p></div>
<div class="m2"><p>صبح را بی نفس سرد تبسم بینی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طوطیانرا همه از نطق شکرخایابی</p></div>
<div class="m2"><p>بلبلانرا همه از شکر ترنم بینی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عاشقانرا همه با وجد اناالحق یابی</p></div>
<div class="m2"><p>عاقلانرا همه درشکر سقاهم بینی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز کریمانت چو حاجت بمهمی افتاد</p></div>
<div class="m2"><p>بکفایت شده بی مطل و تلعثم بینی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه کنی جمع زراززرنشود حرص تو کم</p></div>
<div class="m2"><p>بیشتر تشنگی اندر دل قلزم بینی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منصبی را چکنی خواجه که از هر نااهل</p></div>
<div class="m2"><p>گه تعرض کشی و گاه تزاحم بینی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچو خورشید چهارم فلکت اقطاعست</p></div>
<div class="m2"><p>ز بر خویش زحل بررف هفتم بینی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از تواضع طلب ار برتریی میجوئی</p></div>
<div class="m2"><p>کادمی نیست کش از نفخ تورم بینی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عادلی کو که بحق یاری مظلوم دهد</p></div>
<div class="m2"><p>تا هم از محتسب شهر تظلم بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو بشونقش امید از رخ آیینه دل</p></div>
<div class="m2"><p>تا هم از خویشتن آنلحظه تبرم بینی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یادگیر این سخن ایمرد سخن پیشه زمن</p></div>
<div class="m2"><p>که گر این فهم کنی عز تفهم بینی</p></div></div>