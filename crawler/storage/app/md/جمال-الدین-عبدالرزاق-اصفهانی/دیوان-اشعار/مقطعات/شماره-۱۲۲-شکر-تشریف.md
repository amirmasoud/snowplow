---
title: >-
    شمارهٔ  ۱۲۲ - شکر تشریف
---
# شمارهٔ  ۱۲۲ - شکر تشریف

<div class="b" id="bn1"><div class="m1"><p>دوستی دی بر من آمده بود</p></div>
<div class="m2"><p>دوستی بس ظریف و بس موزون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش بنهاد دفتر شعرم</p></div>
<div class="m2"><p>کرد ازو نقدها همه بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت آراستست دیوانت</p></div>
<div class="m2"><p>بهمه نوع شعر گوناگون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغزلهای همچو آب روان</p></div>
<div class="m2"><p>بمدیح چو لؤلؤ مکنون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمراثی و قطعه و تشبیب</p></div>
<div class="m2"><p>وان دوبیتی که خود چگویم چون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر تشریف چون نمی بینم</p></div>
<div class="m2"><p>باز گو شرح آن مرا اکنون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم احسنت نیک فرمودی</p></div>
<div class="m2"><p>زیر آهن هست نکته مضمون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من چو از کس نیافتم تشریف</p></div>
<div class="m2"><p>شکر چون گویم ای ... نت ... بون</p></div></div>