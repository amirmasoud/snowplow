---
title: >-
    شمارهٔ  ۵۲ - ترهات
---
# شمارهٔ  ۵۲ - ترهات

<div class="b" id="bn1"><div class="m1"><p>دوستی در سمر کتابی داشت</p></div>
<div class="m2"><p>پیش من صفحه ازان میخواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که فلان شخص در فلان تاریخ</p></div>
<div class="m2"><p>بیکی بیت بدره ها بفشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وان دگر پادشه بیک نکته</p></div>
<div class="m2"><p>فاضلی را فراز تخت نشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ایخواجه ترهاتست این</p></div>
<div class="m2"><p>این سخن بر زبان نباید راند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر آن قوم عادیان بودند</p></div>
<div class="m2"><p>که خود از نسلشان یکی بنماند؟</p></div></div>