---
title: >-
    شمارهٔ  ۱۲ - تکذیب حاسدان
---
# شمارهٔ  ۱۲ - تکذیب حاسدان

<div class="b" id="bn1"><div class="m1"><p>بخدائی که رازهای ضمیر</p></div>
<div class="m2"><p>پیش علمش برهنه و فاشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف او را درین نشیمن خاک</p></div>
<div class="m2"><p>آب زراد و باد فراشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کانچه گفتند حاسدان بغرض</p></div>
<div class="m2"><p>نقش سیمرغ و کلک نقاشست</p></div></div>