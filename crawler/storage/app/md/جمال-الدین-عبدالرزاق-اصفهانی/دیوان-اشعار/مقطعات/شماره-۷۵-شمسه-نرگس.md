---
title: >-
    شمارهٔ  ۷۵ - شمسه نرگس
---
# شمارهٔ  ۷۵ - شمسه نرگس

<div class="b" id="bn1"><div class="m1"><p>ای ملک بدیدار تو چون باغ بگل شاد</p></div>
<div class="m2"><p>عالم بوجود تو چو روح از جسد آباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با رحمت تو دود سقر مروحه روح</p></div>
<div class="m2"><p>با هیبت تو نکهت صبح آذر حداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از حزم تو پوشید زره قامت ماهی</p></div>
<div class="m2"><p>وزجود تو زد موج گهر صفحه فولاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شربت الطاف تو تحلیل پذیرد</p></div>
<div class="m2"><p>بحران سموم از مدد گرمی مرداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نعمت تو شمسه نرگس شده زرین</p></div>
<div class="m2"><p>وز طیبت تو گنبد گل شاخه شمشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منشی فلک اجری ارزاق نداند</p></div>
<div class="m2"><p>تا نشنود از کلک تو پروانه انفاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بگذرد از عدل تو بر بیشه نسیمی</p></div>
<div class="m2"><p>هرگز نکند شیر بر آهو بچه بیداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنجی که بهر وقت صداع تو نمودی</p></div>
<div class="m2"><p>این بار بعذر آمد و در پای تو افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر درگه عالیت جهان همچو و شاقی</p></div>
<div class="m2"><p>بر پای بماندست که هرگز منشیناد</p></div></div>