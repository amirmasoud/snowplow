---
title: >-
    شمارهٔ  ۱۳۸ - شادی و غم
---
# شمارهٔ  ۱۳۸ - شادی و غم

<div class="b" id="bn1"><div class="m1"><p>هر شادی و غم که هست اندر دهر</p></div>
<div class="m2"><p>بر زهره و بر زحل همی بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زهره و از زحل چه برخیزد</p></div>
<div class="m2"><p>چه بر ... این و ریش آن خندی</p></div></div>