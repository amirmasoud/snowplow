---
title: >-
    شمارهٔ  ۱۳۲ - هجو گفتن
---
# شمارهٔ  ۱۳۲ - هجو گفتن

<div class="b" id="bn1"><div class="m1"><p>مراخود نیست عادت هجو گفتن</p></div>
<div class="m2"><p>که کردستم طمع زین قوم کوتاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معاذالله که من کس را کنم هجو</p></div>
<div class="m2"><p>ز مدح گفته نیز استغفرالله</p></div></div>