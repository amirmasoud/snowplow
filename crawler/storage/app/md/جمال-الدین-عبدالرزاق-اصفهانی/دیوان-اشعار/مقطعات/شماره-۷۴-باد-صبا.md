---
title: >-
    شمارهٔ  ۷۴ - باد صبا
---
# شمارهٔ  ۷۴ - باد صبا

<div class="b" id="bn1"><div class="m1"><p>دم عیسی است مگر باد صبا</p></div>
<div class="m2"><p>که دل مرده بدو زنده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل چو بدعهدی و رعنائی کرد</p></div>
<div class="m2"><p>دولتش زود پراکنده شود</p></div></div>