---
title: >-
    شمارهٔ  ۱۴۴ - سوم چه فرمائی
---
# شمارهٔ  ۱۴۴ - سوم چه فرمائی

<div class="b" id="bn1"><div class="m1"><p>بزرگوارا در انتظار بخشش تو</p></div>
<div class="m2"><p>نمانده است مرا طاقت شکیبائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سه چیز رسم بود شاعران طامع را</p></div>
<div class="m2"><p>نخست مدح و دوم قطعه تقاضائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر بداد سوم شکرا اگر نداد هجا</p></div>
<div class="m2"><p>من آن دوگانه بگفتم سوم چه فرمائی</p></div></div>