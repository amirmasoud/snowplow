---
title: >-
    شمارهٔ  ۴۰ - جواب مجدالدین همگر
---
# شمارهٔ  ۴۰ - جواب مجدالدین همگر

<div class="b" id="bn1"><div class="m1"><p>یک قطعه سوی بنده فرستاد مجددین</p></div>
<div class="m2"><p>کان را بصد قصیده نشاید جواب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی روشن وی و الفاظ عذب وی</p></div>
<div class="m2"><p>آنکرده با سخن که بسنگ آفتاب کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط شریف او بنکوئی چوآن نگار</p></div>
<div class="m2"><p>کاندر بهار تازه بصحرا سحاب کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشویر خوردم الحق و چونان خجل شدم</p></div>
<div class="m2"><p>کز شرم خاطرم رخ از و در نقاب کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبعم بطعنه گفت که برخیز و شرم دار</p></div>
<div class="m2"><p>کس سنگ را معارض در خوشاب کرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از روی حسب حال بگفت این سه چاربیت</p></div>
<div class="m2"><p>پس توبه کرد طبعم و رای صواب کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلگونه نشاط چه رنگ آورد بگوی</p></div>
<div class="m2"><p>زیرا که از خجالت طبعم چو آب کرد</p></div></div>