---
title: >-
    شمارهٔ  ۲۸ - خرابی بن و خلل سقف
---
# شمارهٔ  ۲۸ - خرابی بن و خلل سقف

<div class="b" id="bn1"><div class="m1"><p>هر که را شد فراخ سفره زیر</p></div>
<div class="m2"><p>دانکه بر چشم او پدید آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اصل دیوار چون خراب شود</p></div>
<div class="m2"><p>خلل از سقف خانه بنماید</p></div></div>