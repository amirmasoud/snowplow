---
title: >-
    شمارهٔ  ۹۵ - دوری از خدمت
---
# شمارهٔ  ۹۵ - دوری از خدمت

<div class="b" id="bn1"><div class="m1"><p>بخدائی که فیض رحمت او</p></div>
<div class="m2"><p>کرد از بند حرص آزادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که من از خدمت چو تو مخدوم</p></div>
<div class="m2"><p>تا به ناکام دور افتادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه دو دیده بخواب دربستم</p></div>
<div class="m2"><p>نه دهن را بخنده بگشادم</p></div></div>