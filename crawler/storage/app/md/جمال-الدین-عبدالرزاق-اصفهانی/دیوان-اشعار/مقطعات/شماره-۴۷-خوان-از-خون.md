---
title: >-
    شمارهٔ  ۴۷ - خوان از خون
---
# شمارهٔ  ۴۷ - خوان از خون

<div class="b" id="bn1"><div class="m1"><p>خوان میفکند کنون مسلمانان</p></div>
<div class="m2"><p>آن خواجه که سگ بر او شرف آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوانی که ز خون آدمی باشد</p></div>
<div class="m2"><p>افطار بدان کسی روا دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خود کس نرود و گر رود آنجا</p></div>
<div class="m2"><p>دربانش و پرده دار نگذارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوانی چه کنی که میزبان او را</p></div>
<div class="m2"><p>هر لقمه هزار بار بشمارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سفره نحس مردریگش بین</p></div>
<div class="m2"><p>کش پیش شدن کسی نمی یارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان قرص حقیر چون هلال صوم</p></div>
<div class="m2"><p>کش گرسنگی ز لب همی بارد</p></div></div>