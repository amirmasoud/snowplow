---
title: >-
    شمارهٔ  ۱۴۱ - دوری
---
# شمارهٔ  ۱۴۱ - دوری

<div class="b" id="bn1"><div class="m1"><p>بخدائی که پس از طاعت او</p></div>
<div class="m2"><p>نیست چون خدمت تو بندگیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مرا بیتوهمه جمع شدست</p></div>
<div class="m2"><p>هر کجا بود پراکندگیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی تو از زندگی خود خجلم</p></div>
<div class="m2"><p>ورچه خود نیست چنان زندگیی</p></div></div>