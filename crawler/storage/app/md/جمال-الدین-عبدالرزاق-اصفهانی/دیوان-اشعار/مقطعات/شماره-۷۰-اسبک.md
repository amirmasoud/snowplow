---
title: >-
    شمارهٔ  ۷۰ - اسبک
---
# شمارهٔ  ۷۰ - اسبک

<div class="b" id="bn1"><div class="m1"><p>دعاگو اسکبی دارد که هر روز</p></div>
<div class="m2"><p>ز عشق کاه تا شب میخروشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غزل میخوانم و در وی نگیرد</p></div>
<div class="m2"><p>دو بیتی نیز کمتر می نیوشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توقع دارد از انعام مخدوم</p></div>
<div class="m2"><p>که بر وی توبواری کاه پوشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>و گر که نیست در اصطبل معمور</p></div>
<div class="m2"><p>درین همسایه شخصی میفروشد</p></div></div>