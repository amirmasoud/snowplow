---
title: >-
    شمارهٔ  ۱۱۲ - قصیده
---
# شمارهٔ  ۱۱۲ - قصیده

<div class="b" id="bn1"><div class="m1"><p>زهی گشاده بمدح تو روزگار دهن</p></div>
<div class="m2"><p>زهی نهاده بحکم تو آسمان گردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که محاوره چون آفتاب نورافشان</p></div>
<div class="m2"><p>که مناظره چون روزگار خصم شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنزد کوه وقار تو کوه بی سنگست</p></div>
<div class="m2"><p>بپیش جود و سخای تو ابرتردامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نور رای تو هرگز نتافت خورزفلک</p></div>
<div class="m2"><p>چو لفظ عذب تو هرگز نخاست در زعدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرم بطبع تو تازه است چون بآب شجر</p></div>
<div class="m2"><p>سخابدست تو زنده است چون بروح بدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنزد رای تو خورشید آسمان تیره</p></div>
<div class="m2"><p>بپیش نطق تو سحبان روزگار الکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز لطف طبع تو گشته خجل نسیم سحر</p></div>
<div class="m2"><p>زبوی خلق تو طیره شدست مشک ختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدر چو دید ترا گفت تا بروز قضا</p></div>
<div class="m2"><p>بمثل تو نشود روزگار آبستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بریده جامه عصمت بقدتست ازانک</p></div>
<div class="m2"><p>بگرد معصیت آلوده نیستت دامن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گشاده آب گه وعظ توزدیده سنگ</p></div>
<div class="m2"><p>فتاده آتش ز جرتو در دل آهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گه وعید تو ناهید بشکند بر بط</p></div>
<div class="m2"><p>بگاه وعده تو بهرام برکند جوشن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سخاو حلم و فصاحت شکوه و علم و ورع</p></div>
<div class="m2"><p>نداشت هیچ دریغ از تو ایزد ذوالمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زهی زدانش بحری میان عالم فضل</p></div>
<div class="m2"><p>زهی ز لطف جهانی بزیر پیراهن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کسی که قصد تو دارد چنان بود بمثل</p></div>
<div class="m2"><p>که کرم پیله ببافد بگرد خویش کفن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زسهم خشم تو سود الجنان شده لاله</p></div>
<div class="m2"><p>زبهر مدح تو رطب اللسان شده سوسن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آنکسی که برون برد سرز چنبر تو</p></div>
<div class="m2"><p>چو نای بینی او را گلو گرفته رسن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تو آسمانی از قدر و جاه بل اعلی</p></div>
<div class="m2"><p>تو آفتابی در حسن رای بل احسن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نسیم لطف تو گر بگذرد سوی صحرا</p></div>
<div class="m2"><p>برآید از بن هر خارو خاره صد گلشن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سموم قهر تو گر بگذرد بگردون بر</p></div>
<div class="m2"><p>بسوزد از زبر چرخ ماه را خرمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسیکه از بد ایام در حمایت تست</p></div>
<div class="m2"><p>اجل نیارد گشتنش نیز پیرامن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو من مدیح تو گویم ز آسمان جبریل</p></div>
<div class="m2"><p>بنعره گوید احسنت شاد باش احسن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بزرگوارا صدرا کنون بدستوری</p></div>
<div class="m2"><p>ز حال خود دو سه بیتی بخواهمت گفتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا زمانه جافی همی دهد مالش</p></div>
<div class="m2"><p>بنوع نوع حوادث بگونه گونه محن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نه هیچ راحت دیدم ز هیچ ممدوحی</p></div>
<div class="m2"><p>نه هیچ فایده بردم ز شعر و نظم سخن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی بپیچم بر خود چو ریسمان زین قوم</p></div>
<div class="m2"><p>که تنک چشم و سبک سرترند از سوزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدر گه تو همی التجا کنم زیشان</p></div>
<div class="m2"><p>که در گهت فضلا راست ملجا و مامن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بتن چو ذره ام ای آفتاب بر من تاب</p></div>
<div class="m2"><p>همای فضلی بر بنده نیز سایه فکن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من از پی چو تو صدری مدیح خواهم گفت</p></div>
<div class="m2"><p>نیم چو غنچه بهر باد بر گشاده دهن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو سایه در نشوم جز بجای آبادان</p></div>
<div class="m2"><p>نه همچو خورشید اندر جهم بهرروزن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه تا که چراغ فلک بود رخشان</p></div>
<div class="m2"><p>چنانکه حاجت ناید بماده روغن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زجاه صدر تو عین الکمال بادا دور</p></div>
<div class="m2"><p>که هست چشم شریعت بجاه تو روشن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اسیر حکم تو بادا سپهر گردنکش</p></div>
<div class="m2"><p>مطیع رای تو بادا زمانه توسن</p></div></div>