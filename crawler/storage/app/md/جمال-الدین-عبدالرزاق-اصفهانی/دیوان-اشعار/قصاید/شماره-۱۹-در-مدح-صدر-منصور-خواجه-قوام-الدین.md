---
title: >-
    شمارهٔ  ۱۹ - در مدح صدر منصور خواجه قوام الدین
---
# شمارهٔ  ۱۹ - در مدح صدر منصور خواجه قوام الدین

<div class="b" id="bn1"><div class="m1"><p>باد عنبر بیز بین کز روضه حور آمدست</p></div>
<div class="m2"><p>این گوهر باش بین کز چشمه نور آمدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نسیم آن هوا پر مشک و عنبر شداست</p></div>
<div class="m2"><p>وز سر شک این جهان پر در منثور آمدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از شکوفه شاخ چون موسی ید بیضا نمود</p></div>
<div class="m2"><p>لاله رخشان زکه چون آتش طور آمدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ چو دوس گشت از حله های گونگون</p></div>
<div class="m2"><p>شاخ چون رضوان میان جامه حور آمدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عیادت میکنی در باغ شو از بهر آنک</p></div>
<div class="m2"><p>نرگس بیمار الحق سخت رنجور آمدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل اندر باغ چون من ز ار مینالد از آنک</p></div>
<div class="m2"><p>گل بحسن خویشتن همچونتو مغرور آمدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکنفس بی جام نبود لاله اندر بوستان</p></div>
<div class="m2"><p>زان سیه دل شد که مرد آب انگور آمدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آب تیره کز میان برف میآیدبرون</p></div>
<div class="m2"><p>راست گوئی صندل سوده ز کافور آمدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لاله دانی بر که میخندد بطرف بوستان؟</p></div>
<div class="m2"><p>برکسی کو وقت گل چو نغنچه مستور آمدست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نغمه بلبل سحر گاهان فراز شاخ گل</p></div>
<div class="m2"><p>طیره آواز چنگ و لحن طنبور آمدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عهد گل نزدیک شد اینک فرود آید ز مهد</p></div>
<div class="m2"><p>خیز واستقبال او کن کزره دور آمدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گل بشکر باد بگشاید دهان در بامداد</p></div>
<div class="m2"><p>سعی باد از بهر گل بنگر چه مشکور آمدست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عمر گل خود مدت یکهفته باشد بیش نه</p></div>
<div class="m2"><p>غنچه گرزین وجه دلتنگست معذور آمدست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوسن خوشدم چگونه لال شد باده زبان</p></div>
<div class="m2"><p>نرگس بی می چرا سرمست و مخمور آمدست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گل زشرم آتش رخسار تو خوی میکند</p></div>
<div class="m2"><p>یا مگر او نیز همچو نشمع محرور آمدست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بربیاض ابر منشور ریاحین نقش شد</p></div>
<div class="m2"><p>در خم قوس قزح طغرای منشور آمدست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عندلیب از گل همی دستان گوناگون زند</p></div>
<div class="m2"><p>همچو من مدحت سرای صدر منصور آمدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواجه عالم قوام الدین سپهر اقتدار</p></div>
<div class="m2"><p>آنکه عقلش پیشکار و شرع دستور آمدست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنکه اندر رفعت و در بخشش و روشندلی</p></div>
<div class="m2"><p>همچو خورشید فلک معروف و مشهور آمدست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لطف او با دوستان و قهر او با دشمنان</p></div>
<div class="m2"><p>همچو نوش نحل و همچو نیش زنبور آمدست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خیمه جاهش ورای سقف مرفوع اوفتاد</p></div>
<div class="m2"><p>پایه قدرش فراز بیت معمور آمدست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دهر نزد طاعت اوست منقاد و مطیع</p></div>
<div class="m2"><p>چرخ پیش حکم او محکوم و مأمور آمدست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ذهن او در بحر علم و فضل غواصی شدست</p></div>
<div class="m2"><p>طبع او بر گنج عقل و شرع گنجور آمدست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش خشم او اجل ترسان و لرزان بگذر</p></div>
<div class="m2"><p>پیش عفو او گنه معفوو مغفور آمدست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از عطایش آزمانند قناعت ممتلی ست</p></div>
<div class="m2"><p>وز سخای او قناعت نیز آزور آمدست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ناصح او در جهان برتخت اقبال و ظفر</p></div>
<div class="m2"><p>کاشح او از فلک مخذول و مقهور آمدست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کمترینش پایه گردون اعلا میسزد</p></div>
<div class="m2"><p>کمتر ینش چاکری خاقان و فغفور آمدست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آستان درگه او کعبه آمال شد</p></div>
<div class="m2"><p>حج این کعبه مرا مقبول و مبرور آمدست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا که گوید آسمان از شکل آمدمستدیر</p></div>
<div class="m2"><p>تا که گوید آفتاب از طبع محرور آمدست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از فلک اورا همه خیر و سلامت باد از آنک روزگار او</p></div>
<div class="m2"><p>همه بر خیز مقصور آمدست</p></div></div>