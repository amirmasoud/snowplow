---
title: >-
    شمارهٔ  ۲۹ - در مدح ملک اعظم حسام الدین اسپهبد مازندران
---
# شمارهٔ  ۲۹ - در مدح ملک اعظم حسام الدین اسپهبد مازندران

<div class="b" id="bn1"><div class="m1"><p>گر کسی فیض جان همی بخشد</p></div>
<div class="m2"><p>شاه گیتی ستان همی بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاه غازی سپهبد اعظم</p></div>
<div class="m2"><p>کاشکار و نهان همی بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ مرادی حسام دولت و دین</p></div>
<div class="m2"><p>که روان را روان همی بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن قدر قدرت قضا قوت</p></div>
<div class="m2"><p>که قضا را توان همی بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کف او ابر وش همی بارد</p></div>
<div class="m2"><p>دل او بحر سان همی بخشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکم او را قدر ز روی نفاذ</p></div>
<div class="m2"><p>سرعت کن فکان همی بخشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلم اوست لوح محفوظ آنک</p></div>
<div class="m2"><p>روزی انس و جان همی بخشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست یکروزه خرج بخشش شاه</p></div>
<div class="m2"><p>هر چه هفت آسمان همی بخشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهره بحر و کان همی بچکد</p></div>
<div class="m2"><p>زان عطا کان بنان همی بخشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فضله خوان اوست اینکه فلک</p></div>
<div class="m2"><p>بر ملوک جهان همی بخشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سایه ایزد است در بخشش</p></div>
<div class="m2"><p>لا جرم همچنان همی بخشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کف او رزق را شود ضامن</p></div>
<div class="m2"><p>پس طبیعت دهان همی بخشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آنچه بخشد بعمر ها گردون</p></div>
<div class="m2"><p>در کم از یکزمان همی بخشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخت ارزان بود بملک جهان</p></div>
<div class="m2"><p>آنچه او رایگان همی بخشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ملک بخش است بر عبید و خدم</p></div>
<div class="m2"><p>ملک خاقان و خان همی بخشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تیغ و کلکش همی دو کارکند</p></div>
<div class="m2"><p>این همی گیرد آن همی بخشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هیچ سلطان نیافت صد یک آنک</p></div>
<div class="m2"><p>شاه سلطان نشان همی بخشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تاج طمغاج خان همی خواهد</p></div>
<div class="m2"><p>ملک هندوستان همی بخشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قطره از لعاب لطف ویست</p></div>
<div class="m2"><p>آنچه نحل از دهان همی بخشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جذوه از شرار خشم ویست</p></div>
<div class="m2"><p>فتنه کاخر زمان همی بخشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تیغ نیلوفر یش دشمن را</p></div>
<div class="m2"><p>کسوت ارغوان همی بخشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سگ از اندام خصم سگ صفتش</p></div>
<div class="m2"><p>استخوان استخوان همی بخشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همه بخشست می نشاید گفت</p></div>
<div class="m2"><p>که فلان یا فلان همی بخشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هم فزون از قیاس می پاشد</p></div>
<div class="m2"><p>هم برون از گمان همی بخشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آنچه زانگشت او فرود افتد</p></div>
<div class="m2"><p>آسمان صد قران همی بخشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آدمی را دعای او فرضست</p></div>
<div class="m2"><p>زان خدایش زبان همی بخشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تیغ او آخنه عدو زنده است</p></div>
<div class="m2"><p>تا بدانی که جان همی بخشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زود بینیم از تواتر فتح</p></div>
<div class="m2"><p>که ملک سیستان همی بخشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سیم وزر هر کسی ببخشد و خود</p></div>
<div class="m2"><p>شاه بخت جوان همی بخشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آفتابش رکاب می پوشد</p></div>
<div class="m2"><p>واسمانش عنان همی بخشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ابر جودش میان بادیه بر</p></div>
<div class="m2"><p>چشمه های روان همی بخشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سیم بر اهل فضل اگر چه بود</p></div>
<div class="m2"><p>عالمی درمیان همی بخشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دست جودش نگر که از ساری</p></div>
<div class="m2"><p>زانسوی اصفهان همی بخشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بچو من بنده بی سوابق مدح</p></div>
<div class="m2"><p>نعمت بی کران همی بخشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اطلس آتشی همی ریزد</p></div>
<div class="m2"><p>قصب و پرنیان همی بخشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گله ها اسب و تختها جامه</p></div>
<div class="m2"><p>بیگان و دو گان همی بخشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>باد پایان آسمان هیکل</p></div>
<div class="m2"><p>همچو کوه روان همی بخشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من گه باشم که شاه بهر شرف</p></div>
<div class="m2"><p>کعبه را طیلسان همی بخشد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نیست از سیم بیوه بخشش او</p></div>
<div class="m2"><p>گنج نوشروان همی بخشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با خرد گفتم از ملوک جهان</p></div>
<div class="m2"><p>کیست کو دخل کان همی بخشد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در چو ابر بهار می بارد</p></div>
<div class="m2"><p>زر چو باد خزان همی بخشد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت این بردل تو پوشیدست؟</p></div>
<div class="m2"><p>شاه ما زندران همی بخشد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آنچه کان ذره ذره بخشد،شاه</p></div>
<div class="m2"><p>کاروان کاروان همی بخشد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتمش تا کی این توان بخشید</p></div>
<div class="m2"><p>گفت تا میتوان همی بخشد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جاودان باد زندگانی شاه</p></div>
<div class="m2"><p>تا چنین جاودا ن همی بخشد</p></div></div>