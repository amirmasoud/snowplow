---
title: >-
    شمارهٔ  ۲۶ - پیغام بخاقانی شروانی
---
# شمارهٔ  ۲۶ - پیغام بخاقانی شروانی

<div class="b" id="bn1"><div class="m1"><p>کیست که پیغام من بشهر شروان برد</p></div>
<div class="m2"><p>یک سخن ازمن بدان مرد سخندان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوید خاقانیا اینهمه ناموس چیست</p></div>
<div class="m2"><p>نه هرکه دو بیت گفت لقب زخاقان برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعوی کردی که نیست مثل من اندر جهان</p></div>
<div class="m2"><p>که لفظ من گوی نطق زقیس و سبحان برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقل دعوی فضل خود نکند ور کند</p></div>
<div class="m2"><p>باید کز ابتدا سخن بپایان برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی بدین مایه علم دعوی دانش کند؟</p></div>
<div class="m2"><p>کسی بدین قدر شعر نام بزرگان برد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تحفه فرستی ز شعر سوی عراق اینت جهل</p></div>
<div class="m2"><p>هیچکس از زیرکی زیره بکرمان برد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد نماند از عراق فضل نماند از جهان</p></div>
<div class="m2"><p>که دعوی چون توئی سر سوی کیوان برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعر فرستادنت بما چنانست راست</p></div>
<div class="m2"><p>که مور پای ملخ نزد سلیمان برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظم گهر گیر تو گفته خود سربسر</p></div>
<div class="m2"><p>کس گهر از بهر سود باز بعمان برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا نه چنان دان که هست سحر حلال اینسخن</p></div>
<div class="m2"><p>سحر کسی خود برموسی عمران برد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زشت بود روز عیدانکه ز پی چایکی</p></div>
<div class="m2"><p>پیرزنی خرسوار گوی زمیدان برد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کس اینسخن بهر لاف سوی عراق آورد</p></div>
<div class="m2"><p>والله اگر عاقل این بکه فروشان برد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بمسجد اندر سگان هیچ خردمند بست؟</p></div>
<div class="m2"><p>بکعبه اندر بتان هیچ مسلمان برد؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر بشهر تو شعر هیچ نخواندست کس</p></div>
<div class="m2"><p>که هرکس از نظم تو دفتر و دیوان برد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخطه کاندر وهم در آید بسر</p></div>
<div class="m2"><p>بدینسخن ریزه کس اسب بجولان برد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عراق آنجای نیست که هرکس ازبیتکی</p></div>
<div class="m2"><p>زبهر دعوی در او مجال طیان برد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هنوز گویندگان هستند اندر عراق</p></div>
<div class="m2"><p>که قوه ناطقه مدد ازیشان برد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یکی ازیشان منم که چون کنم رای نظم</p></div>
<div class="m2"><p>سجده بر طبع من روان حسان برد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>منم که تا جای من خاک سپاهان بود</p></div>
<div class="m2"><p>خرد پی توتیا خاک سپاهان برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو گیرم اندر بنان کلک پی شاعری</p></div>
<div class="m2"><p>عطارد از شرم من سر بگریبان برد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز عکس طبعم بهاره جلوه بستان دهد</p></div>
<div class="m2"><p>زشرم لفظم گهر رخت سوی کان برد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زنثر و شعرم فلک نثره و شعری کند</p></div>
<div class="m2"><p>زلفظ پا کم صدف لؤلؤ و مرجان برد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مرا ست آنخاطری کانچه اشارت کنم</p></div>
<div class="m2"><p>بطبع پیش آورد بطوع فرمان برد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر شود عنصری زنده در ایام من</p></div>
<div class="m2"><p>زدست من بالله اربشاعری جان برد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من زتو احمق ترم تو زمن ابله تری</p></div>
<div class="m2"><p>کسی بباید که مان هر دوبزندان برد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاعر زر گر منم ساحر در گر توئی</p></div>
<div class="m2"><p>کیست که باد بروت زمادو کشخان برد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من و تو باری کنیم زشاعران جهان</p></div>
<div class="m2"><p>که خود کسی نام مازجمع ایشان برد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وه که چه خنده زنند برمن و تو کودکان</p></div>
<div class="m2"><p>اگر کسی شعر مان سوی خراسان برد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مایه ما خولیاست علت سودای ما</p></div>
<div class="m2"><p>صفح دبیقی و بس بود که درمان برد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اینهمه خود طیبتست بالله اگر مثل تو</p></div>
<div class="m2"><p>چرخ بسیصد قران گشت بدوران برد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نتایج فکر تو زینت گلشن دهد</p></div>
<div class="m2"><p>معانی بکر تو زیور بستان برد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فلک ز الفاظ تو زیور عالم دهد</p></div>
<div class="m2"><p>خرد زاشعار تو حجت و برهان برد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازدم نظمت فلک نظام پروین دهد</p></div>
<div class="m2"><p>وزنم کلکت جهان چشمه حیوان برد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بندگی تو خرد ازدل واز جان کند</p></div>
<div class="m2"><p>غاشیه تو ملک از بن دندان برد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چرخ از ینروی کرد پشت دو تا تا مگر</p></div>
<div class="m2"><p>قوت خرد زاین دهد قوت ملک زان برد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نهاده در قحط سال شعر تو خوانی ز فضل</p></div>
<div class="m2"><p>که عقل و نفس و حواس همی بمهمان برد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر بغزنی رسد شعر تو بس شرمها</p></div>
<div class="m2"><p>که روح مسعود سعد ابن سلمان برد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مایه برد هرکسی تز تو و پس سوی تواز</p></div>
<div class="m2"><p>شعر فرستد چنانک گل بگلستان برد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سنت ابراست این که گیرد از بحر آب</p></div>
<div class="m2"><p>پس بسوی بحر باز قطره باران برد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هرکه رساند بمن شعر تو چونان بود</p></div>
<div class="m2"><p>که بوی پیراهنی بپیر کنعان برد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یا که کسی ناگهان بعداز هجری دراز</p></div>
<div class="m2"><p>بعاشق سوخته مژده جانان برد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شکر خدارا که ثو نیستی از آنکه او</p></div>
<div class="m2"><p>شعر بدونان چو ما برای دونان برد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فضل تو پاینده بادصیت تو پوینده باد</p></div>
<div class="m2"><p>که از وجود تو فضل رونق و سامان برد</p></div></div>