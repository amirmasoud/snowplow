---
title: >-
    شمارهٔ  ۹۶ - درمدح امام زین الدین تاج الاسلام
---
# شمارهٔ  ۹۶ - درمدح امام زین الدین تاج الاسلام

<div class="b" id="bn1"><div class="m1"><p>زهی تو حاکم عدل و جهان ترا محکوم</p></div>
<div class="m2"><p>زهی بحم تو گردن نهاده چرخ ظلوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فکنده صیت تو در گوش روزگار طنین</p></div>
<div class="m2"><p>کشیده رای تو بر روی آفتاب رقوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جبلت تو مزین بخصلت محمود</p></div>
<div class="m2"><p>طبیعت تو منزه ز سیرت مذموم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی فروزد از تو چهار بالش شرع</p></div>
<div class="m2"><p>چو آفتاب ز شرق و چو آسمان ز نجوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپیش دست تو دریاست چون خط جدول</p></div>
<div class="m2"><p>بنزد رای تو خورشید نقطه موهوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جواب تو ندهد آرزو مگر لبیک</p></div>
<div class="m2"><p>خطاب تو نکند روزگار جز مخدوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گه نواختن خاص و عام خورشیدی</p></div>
<div class="m2"><p>که کوه و ذره نماند ز نور او محروم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخای حاتم طائی و عدل نوشروان</p></div>
<div class="m2"><p>عبارتیست کزان نام تو شود مفهوم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان بساط ستم در نوشتی از عالم</p></div>
<div class="m2"><p>که می بدست نیاید چو کیمیا مظلوم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجب نباشد ار از طبع تو تغییر یافت</p></div>
<div class="m2"><p>مزاج چرخ ستمکار و طبع عالم لوم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر نبودی فر و شکوه مسند تو</p></div>
<div class="m2"><p>نهاده بود فلک ظلمرا اساس و رسوم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و گرنه سنگ تو میآمدیش در دندان</p></div>
<div class="m2"><p>پدید کرده بداین چرخ ظلمهای سدوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهاده قاعدۀ عدل تو در این کشور</p></div>
<div class="m2"><p>چنانکه شاهین مرکب را دهد مرسوم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز جور آتش نگریستی بعهد تو در</p></div>
<div class="m2"><p>اگرنه نقش کجش راست مینمودی موم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مخالف تو اگر نوش کردی آب حیات</p></div>
<div class="m2"><p>شدی بحنجر او در چو خنجر مسموم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که بود هرگز کو بر خلاف تو دم زد</p></div>
<div class="m2"><p>که خیل مرگ زناگه بر او نکرد هجوم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه نقص جاه ترا گر کسی مخالف شد</p></div>
<div class="m2"><p>چه عیب گل را گر زو حذر کند مز کوم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نعذبالله اگر تو در ابرو آری چین</p></div>
<div class="m2"><p>زسهم و هیبت تو زلزله فتد در روم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر آنکه نیست دو تا پیش تو چو چنبر دف</p></div>
<div class="m2"><p>چو نای رود بود ریسمانش در حلقوم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عوطف کرم شاملت که دایم باد</p></div>
<div class="m2"><p>چو فیض فضل خدایست بر سبیل عموم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خدای عز و جل کرد قسمت روزی</p></div>
<div class="m2"><p>ولی ندانم سری که نیستت معلوم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز قصد دشمن و حاسد ترا چه باک بود</p></div>
<div class="m2"><p>چو هست امر تو قایم بایزد قیوم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بچرخ فرما زین پس چو خدمتی باشد</p></div>
<div class="m2"><p>ازانکه چرخ ترا چاکریست نیک خدوم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ستایش تو چه کارست تابدولت تو</p></div>
<div class="m2"><p>سخن بمدح تو بی من همی شود منظوم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تا که نباشد بطبع آتش آب</p></div>
<div class="m2"><p>همیشه تا که نگردد بطعم شهد ز قوم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زنائبات جهان باد جان تو محروس</p></div>
<div class="m2"><p>ز حادثات فلک باد ذات تو معصوم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمانه خادم آن کت بود ز جمع خدم</p></div>
<div class="m2"><p>سپهر خصم کسی کت بود ز جنس خصوم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جهان مسخر و گردون مطیع و بخت بکام</p></div>
<div class="m2"><p>خدای یار و ملک داعی و فلک محکوم</p></div></div>