---
title: >-
    شمارهٔ  ۱۲۹ - قصیده
---
# شمارهٔ  ۱۲۹ - قصیده

<div class="b" id="bn1"><div class="m1"><p>دارم زعشق روی تو پیوسته ترمژه</p></div>
<div class="m2"><p>وزخون دل زفرقت تو بارور مژه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیوسته شد بهم همه مژگان من بخون</p></div>
<div class="m2"><p>زینسان کسی نبیند پیوسته تر مژه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوئی که رنگرز شده اند این دو چشم من</p></div>
<div class="m2"><p>هردم کنند رنگ بخون جگر مژه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آب چشم من که جهانی فرو گرفت</p></div>
<div class="m2"><p>هم دیده بر خطاست هم اندر خطر مژه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از من قرار و خواب ربوده شد آنچنانک</p></div>
<div class="m2"><p>برهم نمیزنم همه شب تا سحر مژه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیرامن دو چشم من اینک نظاره کن</p></div>
<div class="m2"><p>صد دسته خون سوخته برطرف هر مژه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ازبسکه گشت بر مژه ام خون دیده جمع</p></div>
<div class="m2"><p>کردم غلط که سیخ کبابست هر مژه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این حیف بین که میرود اندر جهان عشق</p></div>
<div class="m2"><p>جرم از دودیده آمد و بیداد بر مژه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از جور یار و قصه احداث روزگار</p></div>
<div class="m2"><p>در غصه که بینم ازین مختصر مژه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وقتست اگر کنم گله نزدیک سروری</p></div>
<div class="m2"><p>کز فخر خاک پایش روید بسر مژه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دستور عصر و خواجه آفاق شمس دین</p></div>
<div class="m2"><p>کز نور راش تیره شود سر بسر مژه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صاحبقران عصر محمد که رای او</p></div>
<div class="m2"><p>مانند دیده ایست برو ماه و خورمژه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای صاحبی که چرخ نبیند نظیر تو</p></div>
<div class="m2"><p>بسیار اگر بر آرد گرد نظر مژه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باریک بین چنان شده ام در مدیح تو</p></div>
<div class="m2"><p>کز دست من همی جهد اندر نظر مژه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از گریه چشم حاسد تو کرته بدوخت</p></div>
<div class="m2"><p>کش ابره خون دیده شد و آسترمژه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرک او خلاف دوستی اندر تو بنگرد</p></div>
<div class="m2"><p>بر دیده اش آورد ز پی کین حشرمژه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اندر تموز گرم ز سردی حسود را</p></div>
<div class="m2"><p>هنگام گریه گردد همچون شمر مژه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دشمن زیان نکرد گه اشک ریختن</p></div>
<div class="m2"><p>میریخت سیم تا که شدش همچو زرمژه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در دیده مخالف نوک سنان تو</p></div>
<div class="m2"><p>اندر جهد چنانکه ندارد خبر مژه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گردر کمان و هم نهی تیر امتحان</p></div>
<div class="m2"><p>وقت نهیب آوری اندر نظر مژه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از چشم دشمنت بگه فرصت گشاد</p></div>
<div class="m2"><p>زان سوی سر کند بتراجع گذر مژه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا در جهان شیوه گری نیکوان زنند</p></div>
<div class="m2"><p>از بهر صید دلها بر یکدیگر مژه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صید همای دولت تو باد نیکوئی</p></div>
<div class="m2"><p>در چشم عدل و داد و بروزیب و فرمژه</p></div></div>