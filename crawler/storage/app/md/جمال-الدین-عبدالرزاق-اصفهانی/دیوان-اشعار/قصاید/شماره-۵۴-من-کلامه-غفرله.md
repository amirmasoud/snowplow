---
title: >-
    شمارهٔ  ۵۴ - من کلامه غفرله
---
# شمارهٔ  ۵۴ - من کلامه غفرله

<div class="b" id="bn1"><div class="m1"><p>زهی قدرت از عالم فکر برتر</p></div>
<div class="m2"><p>وجود تو بر فرق ایام افسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلال تو از فکرت عقل بیرون</p></div>
<div class="m2"><p>کمال تو از مدرج وهم بر تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بانصاف تو زنده جان شریعت</p></div>
<div class="m2"><p>باقبال تو تازه دین پیمبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجاه تو یک پایه این سقف ازرق</p></div>
<div class="m2"><p>زحلم تو یک ذره این گوی اغبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهر نقص چون عقل محضی مجرد</p></div>
<div class="m2"><p>زهر عیب چون روح پاکی مطهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی شعله از نور رای تو خورشید</p></div>
<div class="m2"><p>یکی قطره از رشح کلک تو کوثر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوآموز دستت دل بحر قلزم</p></div>
<div class="m2"><p>لگدکوب قدرت سر قدرت سر چرخ اخضر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخلق تو شد کسوت جان معطر</p></div>
<div class="m2"><p>بخط تو شد عارض دین معنبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وجود تو در شرع چون نور ئر چشم</p></div>
<div class="m2"><p>شکوه تو دردست چون عقل در سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جناب تو مظلوم را حصن محکم</p></div>
<div class="m2"><p>و جود تو درویش را حظ اوفر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر مهر چون رای تو بخشدی نور</p></div>
<div class="m2"><p>قراضات زر گرددی در هوا ذر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نسیم رضای تو هر جاکه بگذشت</p></div>
<div class="m2"><p>گل نو شکفته برآیدازآذر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر شعله خشمت آتش فروزد</p></div>
<div class="m2"><p>بآب اندر انگشت گردد سمندر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وگر بر خلاف مراد تو گردد</p></div>
<div class="m2"><p>فرو ماند از دور چرخ مدور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگوهر چو ماننده کردم ترا من</p></div>
<div class="m2"><p>خرد گفت این نیست تشبیه در خور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که گر زاصل پرسند دریا و ابرست</p></div>
<div class="m2"><p>ور از ذات گوئی سرا پای گوهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه معنی خوبست کایزد تعالی</p></div>
<div class="m2"><p>نکردست در طینت تو مخمر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخای تو بودست در هیچ معطلی؟</p></div>
<div class="m2"><p>ضیای تو بودست در هیچ اختر؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر شیر ابخر دهد شرح خلقت</p></div>
<div class="m2"><p>کسش باز نشناسد از شیر مجمر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کسی کاو ز بهر تو بد گفت یاخواست</p></div>
<div class="m2"><p>خلل یا زدین باشدش یا ز مادر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بوقتی که مشغول باشند هرکس</p></div>
<div class="m2"><p>بلهو و صبوح و سماع بدلبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صبوح تو ختم است و لهوت حکومت</p></div>
<div class="m2"><p>سماع تو قرآن و معشوق دفتر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه کوتاه دستی و چه پاک دامن</p></div>
<div class="m2"><p>ازآنی تو چون سرو آزاد و سرور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو آنی که درعهد تو کلک بیمار</p></div>
<div class="m2"><p>بتحقیق نشنید بوی مزور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بعهد تو سوگند خوردست مسند</p></div>
<div class="m2"><p>اگر دید و بیند دگر چون تو داور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه عمر چونین توانگفت مدحت</p></div>
<div class="m2"><p>که هرگز معانی نگردد مکرر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه مدحست این خود که گر باز پرسی</p></div>
<div class="m2"><p>همین گویدت خصم وزین نیز بهتر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در اخلاق تو هیچ عیبی نگنجد</p></div>
<div class="m2"><p>ترا عیب حلم است الله اکبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بلی حلم نیکوست لیکن نه چندان</p></div>
<div class="m2"><p>که دشمن بیکبار گردد دلاور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدی هم زبهر بدان می بباید</p></div>
<div class="m2"><p>که باآهن آهن بسی بهتر از زر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملکت فاسجح اگر چند نیکوست</p></div>
<div class="m2"><p>ضربت فاوجع از ان نیست کمتر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نه با هر مزاجی بسازد نکوئی</p></div>
<div class="m2"><p>بهرزه نگفت انکه گفتت و فی الشر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه در چشم خفاش ظلمت به از نور؟</p></div>
<div class="m2"><p>نه درکام بیمار تلخ است شکر؟</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه کوری افعیست سبزی زمرد؟</p></div>
<div class="m2"><p>نه مرگ جعل میشود ورد احمر؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو بگذر این لفظ با دشمنانت</p></div>
<div class="m2"><p>که خود خون شود در رگ خصم نشتر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو چین اندر ابرو فکن تا ببینی</p></div>
<div class="m2"><p>که در روم لرز آورد قصر قیصر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اگر باد خشمت بدنیا بجنبد</p></div>
<div class="m2"><p>بلارک شود بید را جمله خنجر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در ایام عدلت چه پنداشت دشمن</p></div>
<div class="m2"><p>که بشکست یأجوج سد سکندر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه مهدی شود هر که بیابد</p></div>
<div class="m2"><p>نه عیسی هر که بنشست بر خر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه هر کس که او ده درم سیم دارد</p></div>
<div class="m2"><p>بتاجی کند بر نهد همچو عبهر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چگونه بتیغی که برداشت لولی</p></div>
<div class="m2"><p>پس آنگاه با چرخ باشد برابر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو نارست مردم که بر تن همی پوست</p></div>
<div class="m2"><p>بدرد چو گردد زدانه توانگر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دلیل زوالست مر مهر را اوج</p></div>
<div class="m2"><p>نشان هلاکست مر مور را پر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بسیمی چرا کرد باید تفاخر</p></div>
<div class="m2"><p>که دخلش بتیغ است و خرجش بساغر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>عدوی تو گر صبح گردد چو صبحش</p></div>
<div class="m2"><p>نفس همچو خنجر بر آید زخنجر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کرامات گوئیم یا محض اقبال</p></div>
<div class="m2"><p>چنین نهضتی در زمانی مقدر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قدر بو العجب بازیی کرد پیدا</p></div>
<div class="m2"><p>که در وی بس لطفها بود مضمر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بسا شکل مشکل که گردون نماید</p></div>
<div class="m2"><p>که در ضمنش اغراض گردد میسر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>همی تا تو در نصرت دین کنی سعی</p></div>
<div class="m2"><p>ترا عون ایزد تمامست یاور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همی نصرت و فتح در جست و مدرج</p></div>
<div class="m2"><p>در انصاف مظلوم و قهر ستمگر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>حسود تو گر چند کور و کبودست</p></div>
<div class="m2"><p>معالی قدرت شد او را مصور</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شب تیره در غیبت مهر روشن</p></div>
<div class="m2"><p>اگر چه ز انجم همی ساخت لشگر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>چو آهیخت خورشید درآبگون تیغ</p></div>
<div class="m2"><p>چو سیماب در لرزه افتند اختر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>گل ار کرد بدعهدیی یا دو رویی</p></div>
<div class="m2"><p>که تا رنگ و بوئیش گردد مقزر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو منشور ملک ریاحین ستاند</p></div>
<div class="m2"><p>بیک بادش اوراق گردد مبتر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زدشمن چو ایمن شدی جای خوفست</p></div>
<div class="m2"><p>که زخم آورداندر گشاد از مششدر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو دشمن ز قصد تو ایمن نشیند</p></div>
<div class="m2"><p>بقصد تو بر خیزد آنگه مکابر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو در کار جزئی بسازی تو با خصم</p></div>
<div class="m2"><p>بکلی طمع آرد آنگاه یکسر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو دشمن بشاشت نماید بیندیش</p></div>
<div class="m2"><p>که زیر بشاشت بلائیست منکر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>اگر چند باز از کبوتر نترسد</p></div>
<div class="m2"><p>ولیک از پی حزم باشد زره ور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چنان لعب بر دشمن افکن که از دفع</p></div>
<div class="m2"><p>نپردازد او با تمنای دیگر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>عدو ار یکی ار صدند ار هزاراند</p></div>
<div class="m2"><p>تو و نیت خوب و رای مظفر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>همی تا برون آرد این زرد مهره</p></div>
<div class="m2"><p>سپیده دم از جیب این سبز چنبر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مباد اندر ایام یک لحظه خالی</p></div>
<div class="m2"><p>ز تو بالش و ازعدوی تو بستر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>مقاصد پس پشت افکن چو مسند</p></div>
<div class="m2"><p>فلک زیر پای اندر آور چو منبر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>همه جرم‌بخشا همه عفوفرما</p></div>
<div class="m2"><p>همه علم‌پرور همه عدل‌گستر</p></div></div>