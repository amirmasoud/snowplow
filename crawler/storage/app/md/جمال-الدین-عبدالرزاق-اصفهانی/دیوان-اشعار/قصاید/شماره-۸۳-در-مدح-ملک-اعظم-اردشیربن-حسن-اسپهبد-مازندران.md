---
title: >-
    شمارهٔ  ۸۳ - در مدح ملک اعظم اردشیربن حسن اسپهبد مازندران
---
# شمارهٔ  ۸۳ - در مدح ملک اعظم اردشیربن حسن اسپهبد مازندران

<div class="b" id="bn1"><div class="m1"><p>ایکه در دست تو هرگز نرسد دست زوال</p></div>
<div class="m2"><p>دور باد از تو و از دولت تو عین کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زمین حلم زمان جنبش دریا بخشش</p></div>
<div class="m2"><p>ایفلک قدر ملک سیرت خورشید جمال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم چشم خرد واسطه عقد ملوک</p></div>
<div class="m2"><p>اردشیر بن حسن شاه پسندیده خصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک مشرق و لشگر کش اسلام که هست</p></div>
<div class="m2"><p>بر جهانداریت از خلق جهان استقلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قاصر از کنه جلال تو مقادیر عقول</p></div>
<div class="m2"><p>عاجز از نقش مثال تو تصاویر خیال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز اقبال ترا هفت فلک زیر دو پر</p></div>
<div class="m2"><p>مرغ انصاف ترا هفت زمین زیر دو بال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماه منجوق تو در ساعد جوزا یاره</p></div>
<div class="m2"><p>نعل شبدیز تو در پای ثریا خلخال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاه وصف نظر تربیتت نامیه لنگ</p></div>
<div class="m2"><p>گاه شرح شرف مرتبتت ناطقه لال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهر درجی است درو نقطۀ از ذکر تو خط</p></div>
<div class="m2"><p>ملک روئی است براو دایره چترتو خال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذروه چرخ رفیعست ترا صحن سرای</p></div>
<div class="m2"><p>قمه عرش مجید است ترا سقف جلال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز خشم تو شیاطین همه در تف و گداز</p></div>
<div class="m2"><p>روز بار تو سلاطین همه در صف نعال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دور باد، ار تف خشم تو زبانه بکشد</p></div>
<div class="m2"><p>آهن اندر دل کوه آب شود سنگ زگال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عافیت در دو جهان رخت کجا بنهادی</p></div>
<div class="m2"><p>گر نه این خشم ترا حلم بدی در دنبال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر زمین ذرۀ از حلم تو حاصل کردی</p></div>
<div class="m2"><p>دگر از نفخه صورش نرسیددی زلزال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای که چاوش ره حشمت تو خاتم جم</p></div>
<div class="m2"><p>ایکه سرهنگ در هیبت تو رستم زال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چتر داری ترا کرده تمنا خاقان</p></div>
<div class="m2"><p>پاسبانی ترا کرده تقاضا چیپال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همه چیزیت توان خواند مگر فرد قدیم</p></div>
<div class="m2"><p>همه چیزیت توان گفت مگر عیب و مثال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پیش از ان کادم منشور خلافت بستد</p></div>
<div class="m2"><p>تو در آنعهد ملک بودی و آدم صلصال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آسمان طفل بد آنگه که زمین ملک تو بود</p></div>
<div class="m2"><p>وافتاب از عدم آنروز رسیده چو هلال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از نم قطره کمیخت زمین خشک نبود</p></div>
<div class="m2"><p>کاسمان میزد در پیش رکاب تو دوال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اولین روز عطارد که بدیوان بنشست</p></div>
<div class="m2"><p>بجهانداری از بهر تو بنوشت مثال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در ازل ملک تو پرداخته شد فارغ باش</p></div>
<div class="m2"><p>زانکه ملک ازلی را نبود بیم زوال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دولت آنست که از دور زمان نبشولد</p></div>
<div class="m2"><p>ورنه باشد همه کس را دو سه روزی اقبال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دوحه سلطنت هر که قوی تر زان نیست</p></div>
<div class="m2"><p>چون نکو بنگری از باغ تو برداست نهال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>حقتعالی بتوازن داد همه روی زمین</p></div>
<div class="m2"><p>که ترا خلق همه روی زمینند عیال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ساقی مجلس انعام تو بی مولامول</p></div>
<div class="m2"><p>جام ها بر کف امید نهد مالامال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مال و دشمن بر تو دیر نپایند ازانک</p></div>
<div class="m2"><p>ملک دشمن مالی وشه دشمن مال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همه شاهان جهان از تو وجودت خجلند</p></div>
<div class="m2"><p>این چه شیوه ست که شه دارد در بذل نوال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جمع مالست غرض این دگران را از ملک</p></div>
<div class="m2"><p>ملک مارا از ملک غرض بخشش مال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لاجرم مشرق و مغرب ز تو پر شکر و ثناست</p></div>
<div class="m2"><p>حاصل این دگران نیست بجز وزر ووبال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آنچنان تازه که طبع تو ز بخشش گردد</p></div>
<div class="m2"><p>جگر تشنه همانا نشود زاب زلال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گاه دانش چو خرد نقب زنی دردل غیب</p></div>
<div class="m2"><p>گاه بخشش نکنی فرق منی از مثقال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای که هرگز نبود حکم تو مشغول جواب</p></div>
<div class="m2"><p>وی که هرگز نبود جود تو موقوف سؤال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو بخر مدحکه از بیم عطا آن دگران</p></div>
<div class="m2"><p>نام شعر و شعرا نیک ندارند بفال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دی چو از مطلع خود تیغ زد آیینه چرخ</p></div>
<div class="m2"><p>طلعت شاه جهان دیدی، هم زان منوال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون برآمد بافق گرم درآمد بعتاب</p></div>
<div class="m2"><p>با من از راه مجارات نه از روی جدال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گفت کای شاعر شاهی که همی نازد ازو</p></div>
<div class="m2"><p>تاج و تخت ملکی راست چو عاشق زوصال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا تو آراستۀ شعر بالقاب ملک</p></div>
<div class="m2"><p>یافتست از تو سخن رونق بازار و کمال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شاهرا عادل خوانی نتوانگفت که نیست</p></div>
<div class="m2"><p>لیکن این عدل بود نزد تو؟نیکو بسگال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اوبیک لحظه بتاراج دهد بی جگری</p></div>
<div class="m2"><p>آنچه من در دل کان جمع کنم در دو سه سال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خاک بیز فلکم خیره چراغی بر دست</p></div>
<div class="m2"><p>شعله جاروبم و کان کیسه و گردون غربال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>راست چون چند قراضه بهم آمد اینک</p></div>
<div class="m2"><p>جودشه تاختنی آرد و بخشد در حال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آخر از بهر خدا چند کنم بر در او</p></div>
<div class="m2"><p>هر سحر گاه چو مظلوم ز کاغذ سربال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گفتم او راز سخا باز توان داشت بگفت؟</p></div>
<div class="m2"><p>این چه سودای غرور است و تمنای محال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ابر چون رای عطا کرد توانگفت مبار؟</p></div>
<div class="m2"><p>شاخ چو نقصد هوا کرد توانگفت مبال؟</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تو چه خدمت کنی ار زر نزنی بهر ملک</p></div>
<div class="m2"><p>چو نتوصد مشعله دارست بر این در بطال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>مکن ایشاه که کان کیسه تهیکرد از تو</p></div>
<div class="m2"><p>زر و سیمست که میبخشی نه سنگ و سفال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>کف کافی تو با کان چه عداوت دارد</p></div>
<div class="m2"><p>که بیکبار برآورد ازو استیصال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ملک از بخشش بسایر اگر نیست ملول</p></div>
<div class="m2"><p>بنده را باری از این بیش شدن خاست ملال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>عنصری کو که همیگفت که محمود کریم</p></div>
<div class="m2"><p>گو بیا از کرم شاه بخواه استحلال</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>کو غضاری که همیکفت بمن فخر کند</p></div>
<div class="m2"><p>هر که او بر سر یک بیتم بنویسد قال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گو بیا شاعر شه بین که همی از در شاه</p></div>
<div class="m2"><p>در بدامن کشد و زر بمن اطلس بجوال</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرکب خاطر من در ره اندیشه گسست</p></div>
<div class="m2"><p>در مدیح تو سخن را چو فراخست مجال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>غایت آنچه بدان دست تصرف نرسد</p></div>
<div class="m2"><p>در مدیحت چو بگویند بودوصف الحال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا همی چهره گشاید مه رومی صورت</p></div>
<div class="m2"><p>تا همی طره طرازد شب زنگی تمثال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>باد در قبضه حکم تو عنان گردون</p></div>
<div class="m2"><p>باد بر درگه جود تو مجال آمال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرغ انصاف ترا گوی زمین در منقار</p></div>
<div class="m2"><p>شیر اقبال ترا جان عدو در چنگال</p></div></div>