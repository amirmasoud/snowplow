---
title: >-
    شمارهٔ  ۵۹ - در مدح امام فاضل شمس الدین ابوافتح النطنزی
---
# شمارهٔ  ۵۹ - در مدح امام فاضل شمس الدین ابوافتح النطنزی

<div class="b" id="bn1"><div class="m1"><p>زهی در یای گوهر بخشش موج انگیز پهناور</p></div>
<div class="m2"><p>نه آنرا غایت و پایان نه انرا ساحل و معبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرازش عنبر و عود نشیبش لؤلؤ و مرجان</p></div>
<div class="m2"><p>هوا یش صافی و روشن زلالش عذب و جانپرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک باقدر او پست و زمین در جنب او ذره</p></div>
<div class="m2"><p>جهان نزدیک او ناقص محیط از پیش او فرغر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاراو همه عطر و زمین او همه مرجان</p></div>
<div class="m2"><p>درخت اوهمه بسد نبات اوهمه جانور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه دامنش پر عود و همه ساحلش پر سنبل</p></div>
<div class="m2"><p>همه قعرش پر از لؤلؤ همه سطحش پر از عنبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدفهائی اززو زاید بگوهر جمله آبستن</p></div>
<div class="m2"><p>نهنگانی ازو خیزد بصورت اژدها پیکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر شوری برانگیزد کمر از کوه بگشاید</p></div>
<div class="m2"><p>وگر جوشی بر اندازد فروشوید زچرخ اختر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه دریایی که از هر خس همی بر خود نهد باری</p></div>
<div class="m2"><p>که گر بادی زند بر وی شود در حال جو شنور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه آن بحری که از کاهی همی برخویشتن لرزد</p></div>
<div class="m2"><p>که گرکوهی فتد در وی نیاید چین برویش در</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سحاب ازوی همیبارد گهرها درمه نیسان</p></div>
<div class="m2"><p>زمین ازوی همیپوشد حواصل در مه آذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دمی بی خیزران هرگز نبودست او همه ساله</p></div>
<div class="m2"><p>چنان چون عادت دریاست دارد صد سفبنه زر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر موجی برانگیزد فلک را بس بود غوطه</p></div>
<div class="m2"><p>وگردری براندازد جهان را بس بود زیور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شناور اندر و عقلست و غواص اندر و فکرت</p></div>
<div class="m2"><p>زعلمست اندرو کشتی زحلمست اندر و لنگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو این دریا کرادانی تو این کشتی کراخوانی</p></div>
<div class="m2"><p>جز اینحر سخا پرور جز اینحبر سخن گستر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امام شرق شمس الدین ابوالفتح آنکه در هر فن</p></div>
<div class="m2"><p>یکی بحرست پر لولؤ یکی کانست پر گوهر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از و یکلفظ و صد معنی از و یک قول و صد برهان</p></div>
<div class="m2"><p>ازو یکبیت و صد دیوان ازویک فردوصد دفتر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جو خامه ذوالسانین و چو گردون ذوالبیانینست</p></div>
<div class="m2"><p>عربرانظم اوزینت عجمرا نثر او مفخر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شعاع روی او کردست چشم هفت اختر کور</p></div>
<div class="m2"><p>صدای فضل او کردستگوش هفتگردون کر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زعزم بادسیرش دان مدار عالم علوی</p></div>
<div class="m2"><p>زحلم کوه طبعش بین قرار مر کزاغبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه ترتیب کلک او چو در ترکیب لفظ آید</p></div>
<div class="m2"><p>کشد در سلک نظم آسان بنات النعشر ایکسر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فلک گرمیتوانستی زشرم نظم شیرینش</p></div>
<div class="m2"><p>نظام خوشه پروین جدا کردی زیکدیگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر اوفی المثل ابلیس را مدحی برآوردی</p></div>
<div class="m2"><p>چنان دار کان نگارد جبرئیل از فخر بر شهیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وگر ذمی براندیشد مرین خورشید رخشانرا</p></div>
<div class="m2"><p>که اندر حد نظم آرد بلفظ زشت و مستنکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان گوید که از بیمش زحل رایتر کدزهره</p></div>
<div class="m2"><p>زمین را بشکند مهره فلک را بگسلد چنبر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیانش معجز و سحرست در یک حال پنداری</p></div>
<div class="m2"><p>زبانش آتش و آبست در یکجای چون خنجر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر نثری کنداملی پر از گوهر کند گوشت</p></div>
<div class="m2"><p>وگرنظمی کندانشی شود طبعت پراز گوهر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>میسر گشته هر سرش مگر اسرار علم غیب</p></div>
<div class="m2"><p>محصل گشته هر علمش هر مگر علم شمارزر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مدد از علم او پذیرفت و از جودش ستد مایه</p></div>
<div class="m2"><p>هرآنکس کودلگیرم و دمیخوش یافت چونمجمر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی دریادلی گز شرم جودت ابر را دایم</p></div>
<div class="m2"><p>دلی پر آتش و دستی پر از بادست و چشمی تر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دوان چون باد صییت تو از بذ انعالم</p></div>
<div class="m2"><p>روانچون آب ذکر تو از ینکشور بدانکشور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تو آن شمسی که گردون را بجای دیده ورنه</p></div>
<div class="m2"><p>بدین یکچشمه خورشید ما ندستی فلک اعور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>توئی کز علم و از حکمت بدانمنصب رسیدستی</p></div>
<div class="m2"><p>که هفتم پایه چرخست اول پایه ت از منبر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کرادنیکه از تو نیست دست نعمتی برروی</p></div>
<div class="m2"><p>کرا دانی که از تو نیست طوق منتی در بر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زلفظ گوهر افشان تو جانرا توشه شد فربه</p></div>
<div class="m2"><p>زجود کاروان بخش تو کا ن را کیسه شد لاغر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جهانصدر امن این مدحت بدستوری همیگفتم</p></div>
<div class="m2"><p>وگرنه من بهر حالی نیم هم تا بدین حدخر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مدیح چون تویی گفتن نه کار چون منی باشد</p></div>
<div class="m2"><p>طبیبی پیش عیسی کردن ان کاری بود منکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مدیح چون توئی گفتن مجالی بس فراخ آید</p></div>
<div class="m2"><p>که هرچ از بحر وا گوئی بود مر عقل را باور</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وگر خوشیست اینگفته غر ضصد قست کاندر شعر</p></div>
<div class="m2"><p>محال از صدق چابکتر دروغ از راست شیرینتر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تازروی طبع نبود باد همچون خاک</p></div>
<div class="m2"><p>همیشه تازروی فعل نبود آب چون آذر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شکوه فضل تو دایم بماناد اندرین عالم</p></div>
<div class="m2"><p>که مثل تودگر شخصی نخواهد زادان از مادر</p></div></div>