---
title: >-
    شمارهٔ  ۶۸ - حاجی سلام از چاکران باید باشد
---
# شمارهٔ  ۶۸ - حاجی سلام از چاکران باید باشد

<div class="b" id="bn1"><div class="m1"><p>زمن بخواهد حاجی سلام پرسش خویش</p></div>
<div class="m2"><p>دعا و خدمت هریک همی کنم تکرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>توقعست مرا کز مجددات امور</p></div>
<div class="m2"><p>خبر دهند بهر وقت از مجار ومسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وگرمهمی یا خئمتی است فرماید</p></div>
<div class="m2"><p>که تا بشکر و بمنت شوم پذیرفتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درود ایزد بر مصطفی محمدباد</p></div>
<div class="m2"><p>براهل بیت وی و بر مهاجر و انصار</p></div></div>