---
title: >-
    شمارهٔ  ۱۳ - در جلوس طغرلشاه سلجوقی
---
# شمارهٔ  ۱۳ - در جلوس طغرلشاه سلجوقی

<div class="b" id="bn1"><div class="m1"><p>شاه جوانست و بخت ملک جوانست</p></div>
<div class="m2"><p>کار جهان لاجرم بکام جهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخت بنازد همی و در خور اینست</p></div>
<div class="m2"><p>تاج ببالد همی و لایق آنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک شهنشاه بین که ساحت عقلست</p></div>
<div class="m2"><p>عقل خداوند بین که نسخت جانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روضه فردوس بایدت که ببینی؟</p></div>
<div class="m2"><p>مملکت شاه بین که راست چنانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در همه اطرافهاش عصمت و عدلست</p></div>
<div class="m2"><p>در همه اقطارهاش امن و امانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیر در او بدرقه است ومار فسونگر</p></div>
<div class="m2"><p>غول دلیل رهست و گرگ شبانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دولت جوئی؟ بطبع حلقه بگوشست</p></div>
<div class="m2"><p>نصرت خواهی؟ بطوع بسته میانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شاه فلک رخش جان ستان جهانبخش</p></div>
<div class="m2"><p>شیر اجل رامح ستاره سنانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سایه یزدان و آفتاب سلاطین</p></div>
<div class="m2"><p>طغرل گردون نشین فتنه نشانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر چه بنی آدمند، بنده سلطان</p></div>
<div class="m2"><p>هر چه زمین، ملک شاه چرخ توانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن منگر تو که شاه اندک سالست</p></div>
<div class="m2"><p>پیر خرد بین مرید شاه جوانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سال گر اندک گذشت زان چه خلل یافت</p></div>
<div class="m2"><p>عمر اگر بیش ماند زان چه زیانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سایه حقست زین سبب همه بخشست</p></div>
<div class="m2"><p>مایه فضلست زین سبب همه دانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قوت حملش فزون ز وزن زمینست</p></div>
<div class="m2"><p>سرعت عزمش ورای سیر زمانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخشش و فرمانوری و عدل و سیاست</p></div>
<div class="m2"><p>قاعده خاندان سلجوقیانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دیرزی ای چشم سلطنت بتو روشن</p></div>
<div class="m2"><p>کز تو اثرهای خوب جمله عیانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کثرت جیشت فزون ز حد شمارست</p></div>
<div class="m2"><p>عرصه ملکت برون زحد گمانست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عزم سبک خیز تو چه تیزرکابست</p></div>
<div class="m2"><p>حلم گرانسنگ تو چه سخت کمانست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تخت تو پایه فراز عرش نهادست</p></div>
<div class="m2"><p>چتر تو دامن باوج چرخ کشانست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>امر تو و نهی تو چو چشمه خورشید</p></div>
<div class="m2"><p>در همه اقصای شرق و غرب روانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نامه فتحت بفتح خانه قیصر</p></div>
<div class="m2"><p>تیر مصافت بخیل خانه خانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش ضمیر تو سخت پرده دریده است</p></div>
<div class="m2"><p>هرچه پس پردهای غیب نهانست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شیر فلک از نهیب تیغ تو چونانک</p></div>
<div class="m2"><p>شیر علم روز باد در خفقانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باس تو در طبع آفتاب اثر کرد</p></div>
<div class="m2"><p>زردی رویش علامت یرقانست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرخ زخوان ریزه سخای تو دارد</p></div>
<div class="m2"><p>چند قراضه که زیر دامن کانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نیست بیکروزه خرج جود شهنشاه</p></div>
<div class="m2"><p>هر چه نبات و معادن و حیوانست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عقل نگر پیش خرده کاری لطفت</p></div>
<div class="m2"><p>تا چه سبک مایه هیکل و چه گرانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>صبح ببین پیش شعله های ضمیرت</p></div>
<div class="m2"><p>تا که چه دم سر دو چون دریده دهانست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تیغ تو بس پاسبان ملک تو زیراک</p></div>
<div class="m2"><p>هندوی بیدار خسب چیره زبانست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فی المثل ار خصم ملک تو همه شیر است</p></div>
<div class="m2"><p>پای سپر همچو شیر شادروانست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جود تو باشد کدام حاتم طائی</p></div>
<div class="m2"><p>عدل تو باشد چه نام نوش روانست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رخش ترا زین مه ورکاب ثریا</p></div>
<div class="m2"><p>طوقش از اکلیل و از مجره عنانست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کوه درنگست؟ نیست برق شتابست</p></div>
<div class="m2"><p>ابر بزینست؟ نیست باد بزانست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سبزه زچرب آخور سپهر چریدست</p></div>
<div class="m2"><p>ماه نو از نعل او کمینه نشانست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر نه علف زار اوست از چه فلک را</p></div>
<div class="m2"><p>خرمن ماهست و راه کهکشانست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ختم سخن را دعای ملک تو گویم</p></div>
<div class="m2"><p>کانچه دعای تو نیست آن هذیانست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دولت و نصرت سزای تخت تو بادا</p></div>
<div class="m2"><p>تا که فلک را بسعد و نحس قرانست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ملک تو پاینده باد و عمر توجاوید</p></div>
<div class="m2"><p>تا مدد دهر از بهار و خزانست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>میخ طناب وجود، چتر تو بادا</p></div>
<div class="m2"><p>بنده از این خوبتر دعا نتوانست</p></div></div>