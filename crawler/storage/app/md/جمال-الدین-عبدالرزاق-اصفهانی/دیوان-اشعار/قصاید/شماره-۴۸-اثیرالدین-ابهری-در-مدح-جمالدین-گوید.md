---
title: >-
    شمارهٔ  ۴۸ - اثیرالدین ابهری در مدح جمالدین گوید
---
# شمارهٔ  ۴۸ - اثیرالدین ابهری در مدح جمالدین گوید

<div class="b" id="bn1"><div class="m1"><p>از طبع تو جزگهر چه خیزد</p></div>
<div class="m2"><p>بالفظ تو جز شکر چه خیزد</p></div></div>
<div class="b2" id="bn2"><p>استاد جمال الدین در جواب فرماید</p></div>
<div class="b" id="bn3"><div class="m1"><p>با کلک تو از گهر چه خیزد</p></div>
<div class="m2"><p>بالفظ تو از شکر چه خیزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با پرتو خاطر شریفت</p></div>
<div class="m2"><p>از عکس شعاع خور چه خیزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نام تو از سخن چه آید</p></div>
<div class="m2"><p>بی نامیه ازشجر چه خیزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عالم جان و خطه عقل</p></div>
<div class="m2"><p>از نظم تو پاک تر چه خیزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در معرض لفظ روشن تو</p></div>
<div class="m2"><p>از اختر باختر چه خیزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بحر علوم تو زند موج</p></div>
<div class="m2"><p>از صد صدف دررچه خیزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وقتی که زشعر دم زنی تو</p></div>
<div class="m2"><p>از سرد ذم سحر دم سحر چه خیزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون کوزه زشعر تو گشایند</p></div>
<div class="m2"><p>از نغمه کاسه گر چه خیزد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لفظ خوش تست قوت دلها</p></div>
<div class="m2"><p>از شربت گلشکر چه خیزد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میر سخنی سخن غلامت</p></div>
<div class="m2"><p>به زین زجهان حشر چه خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کان هنری اگر چه گویند</p></div>
<div class="m2"><p>زر باید از هنر چه خیزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بی زر ز هنر هنر چه آید</p></div>
<div class="m2"><p>بی نم زشجر ثمر چه خیزد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از مردم بی درم چه آید</p></div>
<div class="m2"><p>وز دیلم بی تبر چه خیزد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنرا که نداد چرخ دولت</p></div>
<div class="m2"><p>از دانش بحر وبر چه خیزد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نادانی و دولت ای برادر</p></div>
<div class="m2"><p>زین دانش بی خطر چه خیزد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زر باید خاک بر سر شعر</p></div>
<div class="m2"><p>زوگرد نخیزد ار چه خیزد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون شعر در اصل معتبر نیست</p></div>
<div class="m2"><p>زین گفته پر عبر چه خیزد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون نیست سخن شناس دردهر</p></div>
<div class="m2"><p>پس زین در روغررچه خیزد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در جلوه بنات فکر ما را</p></div>
<div class="m2"><p>زین مشتی کور و کرچه خیزد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنکسکه شناسد او خود از ماست</p></div>
<div class="m2"><p>ور همچو خودی نگر چه خیزد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وانکس که نداند ارچه خواهد</p></div>
<div class="m2"><p>از جاهل مدح خر چه خیزد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقدی رایج شناس تحسین</p></div>
<div class="m2"><p>زاحسنت و زماقصر چه خیزد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گیرم که سری شوم زعالم</p></div>
<div class="m2"><p>از عالم سر بسر چه خیزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از دایره کو همه سر آمد</p></div>
<div class="m2"><p>جز گشتن گرد سر چه خیزد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون کشتی مانشست بر خشک</p></div>
<div class="m2"><p>زین بحر لطیف تر چه خیزد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون پی سپر همه جهانیم</p></div>
<div class="m2"><p>زین شعر فلک سپر چه خیزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر دوخته چشم باز او را</p></div>
<div class="m2"><p>از قوت بال و پر چه خیزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بافر چو هماست و استخوانست</p></div>
<div class="m2"><p>خوردش چو سگان ز فرچه خیزد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سرو آمده سایه دار لیکن</p></div>
<div class="m2"><p>از وی چو نداشت بر چه خیزد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صد دست چنار دارد اما</p></div>
<div class="m2"><p>از دست تهی نظر چه خیزد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این چه سخنست هم سخن به</p></div>
<div class="m2"><p>در روی زمین زهر چه خیزد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>معشوقه ذلگشا سخن دان</p></div>
<div class="m2"><p>از دلبر سیم بر چه خیزد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دانش طلب از درم چه آید</p></div>
<div class="m2"><p>معنی نگر از صور چه خیزد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بهتر خلف از جهان سخن خاست</p></div>
<div class="m2"><p>از ذختر و از پسر چه خیزد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرداز هنر آدمیست ور نه</p></div>
<div class="m2"><p>از نسبت بو البشر چه خیزد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فضل و هنر است زینت مرد</p></div>
<div class="m2"><p>از حلقه و از کمر چه خیزد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تن را چو برهنه ماند از علم</p></div>
<div class="m2"><p>از کسوت شوشتر چه خیزد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دل زنده بعلم باید ار نی</p></div>
<div class="m2"><p>از جنبش جانور چه خیزد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جان را بعلوم پرورش ده</p></div>
<div class="m2"><p>ایمرد زخواب وخور چه خیزد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حاصل ز طعام چرب و شیرین</p></div>
<div class="m2"><p>جز ضربت نیشتر چه خیزد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>با تازه سخن زر کهن چیست</p></div>
<div class="m2"><p>این رو حست از حجر چه خیزد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>وقتی که همی نفس زندصبح</p></div>
<div class="m2"><p>زاختر بسپهر بر چه خیزد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>جانی که سخن سزاست طوطی</p></div>
<div class="m2"><p>از همه هدهد تاجور چه خیزد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نرگس که بیافت شش درم سیم</p></div>
<div class="m2"><p>زانش بجز از سهر چه خیزد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از گل که زریست در میاننش</p></div>
<div class="m2"><p>جز خنده دلشکر چه خیزد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>جز سوزش و جز گداز و کریه</p></div>
<div class="m2"><p>از شمع و زتاج زر چه خیزد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ایدل دل از ینمقام بگسل</p></div>
<div class="m2"><p>بر خیز کزین مقر چه خیزد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زین منزل پر خطر چه آید</p></div>
<div class="m2"><p>زین گنبد پرگذر چه خیزد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در دهر دو رنگ دل چه بندی</p></div>
<div class="m2"><p>زان صفوت وزین کدر چه خیزد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چرب آخور تست عالم جان</p></div>
<div class="m2"><p>زین شورستان شر چه خیزد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از گردش چرخ و سیر اختر</p></div>
<div class="m2"><p>خیرو شر و نفع وضر چه خیزد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>زان دیده دیده دوز بندیش</p></div>
<div class="m2"><p>زین پرده پرده در چه خیزد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از بند چهار و هفت بر خیز</p></div>
<div class="m2"><p>زین مادر و زان پدر چه خیزد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>زین خانه چار حد چه آید</p></div>
<div class="m2"><p>زین حجره هفت در چه خیزد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>از چاه برآر یوسف جان</p></div>
<div class="m2"><p>زین شاه بچاه در چه خیزد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر بام فلک خرام یکره</p></div>
<div class="m2"><p>زین گشتن در بدر چه خیزد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دعوی نکنم که این بدیهه است</p></div>
<div class="m2"><p>در شعر زما حضر چه خیزد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>خود مبلغ علم و غایت جهد</p></div>
<div class="m2"><p>این بود وزین قدر چه خیزد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گفتم مگرم سخن دهد دست</p></div>
<div class="m2"><p>دانستم کز مگر چه خیزد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بهتر نیکی بدست پیشت</p></div>
<div class="m2"><p>با آنکه زبد بتر چه خیزد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>در عهد تو از تعرض شعر</p></div>
<div class="m2"><p>جز سوز جگر دگر چه خیزد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جایی که همی نفس زند مشک</p></div>
<div class="m2"><p>از سوخته جگر چه خیزد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>خورشید چو گشت سایه گستر</p></div>
<div class="m2"><p>از ذره مختصر چه خیزد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چون ابر کشید خنجر برق</p></div>
<div class="m2"><p>از ناوک یک شرر چه خیزد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>جائی که زره گر است داود</p></div>
<div class="m2"><p>از سلسله شمر چه خیزد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چون مهر کند فلک سوار ی</p></div>
<div class="m2"><p>از چالش لاشه خر چه خیزد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چون اختر جمله دیده آمد</p></div>
<div class="m2"><p>ا زنرگس بی بصر چه خیزد</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>گر چه زتوأم چنانکه گفتی</p></div>
<div class="m2"><p>کز طبع تو جز گهر چه خیزد</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>مه یافت نظر زجرم خورشید</p></div>
<div class="m2"><p>بنگر کش از ان نظر چه خیزد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>لیکن چو رسد بجرم خورشید</p></div>
<div class="m2"><p>از دایره قمر چه خیزد</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گفتم با شارت تو این شعر</p></div>
<div class="m2"><p>وز گفتن این بدر چه خیزد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>هم رخصت تست تا بگویی</p></div>
<div class="m2"><p>زین شاعر بی خبر چه خیزد</p></div></div>