---
title: >-
    شمارهٔ  ۴ - شکایت از روزگار
---
# شمارهٔ  ۴ - شکایت از روزگار

<div class="b" id="bn1"><div class="m1"><p>دگر باره چه صنعت کرد باما</p></div>
<div class="m2"><p>سپهر سر کش فرتوت رعنا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیک بازی سوی تحت الثری برد</p></div>
<div class="m2"><p>برونق رفته کاری چون ثریا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گفتم کاستقامت یافت کارم</p></div>
<div class="m2"><p>زگردون شد چو گردون زیر وبالا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جوانمردی غم ما خواست خوردن</p></div>
<div class="m2"><p>لگد بر کار زد این پیر رسوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغا آنچنان آزاد مردی</p></div>
<div class="m2"><p>که گردونش نخواهد دید همتا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو کشتی امید آمد بساحل</p></div>
<div class="m2"><p>بدو وا خورد ناگه موج دریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک بااهل معنی خود بکین است</p></div>
<div class="m2"><p>نه بر من میرود این ظلم تنها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل ریشم از او مرهم طلب کرد</p></div>
<div class="m2"><p>مر او داغی نهادش بی محابا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مگر کار دل از مرهم گذشتست</p></div>
<div class="m2"><p>به داغش می کند اکنون مداوا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندانم چرخ را با من چه کین است</p></div>
<div class="m2"><p>مگر با زهره بگرفته است مارا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بغارت برد عمرم نحس کیوان</p></div>
<div class="m2"><p>هم از ادبار این هندوی لا لا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن ای چرخ با ما هم نظر کن</p></div>
<div class="m2"><p>که هرکس از تو در کاریست الا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر بر جاهلان وقفست خیرت</p></div>
<div class="m2"><p>نیم من هم بدین حد نیز دانا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا دی بر گذشت از عمر و امروز</p></div>
<div class="m2"><p>ز دی بدتر گذشت ای وای فردا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرمن چون سر چرخست گردان</p></div>
<div class="m2"><p>دل من چون دل مهرست دروا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نه اندر رسم این ایام انصاف</p></div>
<div class="m2"><p>نه اندر طبع این مردم مواسا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان سیرم زجان کز غصه هر روز</p></div>
<div class="m2"><p>کنم صدره گذر بر مرگ عمدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا گویی چرا صابر نباشی</p></div>
<div class="m2"><p>که بر عمر اعتمادی نیست زیرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو از من عمر یکروزه ضمان کن</p></div>
<div class="m2"><p>که من سالی بوم آنگه شکیبا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قبای عمر چون بر تن بدرد</p></div>
<div class="m2"><p>نشاید کردنش دیگر مطرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منم در کام این ایام شکر</p></div>
<div class="m2"><p>چرا بر من کند بیهوده صفرا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرا از بهر دانش رنج بردیم</p></div>
<div class="m2"><p>چرا بیهوده می پختیم سودا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قلم را با قلم زن خاک بر سر</p></div>
<div class="m2"><p>چرا نه چنگ زن بودم دریغا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو موی رو بهست و ناف آهو</p></div>
<div class="m2"><p>وبال عمر ما این دانش ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هنر عیبست و فضل آفت چه تدبیر</p></div>
<div class="m2"><p>که با کفرست این هردو مساوا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه حکمت رست و نه یونان حکمت</p></div>
<div class="m2"><p>نه شد بر طور سینا پور سینا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چه نقص از جهل چون از جهل باشد</p></div>
<div class="m2"><p>دل آسوده و عیش مهنا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه سود از فضل چون از فضل دارم</p></div>
<div class="m2"><p>همه اسباب نا کامی مهیا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سگان را حشمت و مارا تحسر</p></div>
<div class="m2"><p>خران را دولت و ما را تمنا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>وجاهت در دروغست و تقدم</p></div>
<div class="m2"><p>برای العین میبین آشکارا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که از بهر دروغی صبح کاذب</p></div>
<div class="m2"><p>ز پیش صبح صادق گشت پیدا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دو روئی کن که تا جاهی بیابی</p></div>
<div class="m2"><p>نبینی اوج خورشید است جوزا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدی کن تا توانی و ددی کن</p></div>
<div class="m2"><p>که تا از تو بترسد پیر و برنا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همیشه همچو کژدم جان گزا باش</p></div>
<div class="m2"><p>که تا باشد چو مارت جامه دیبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تما شا کن در این چرخ مشعبد</p></div>
<div class="m2"><p>که هستش مهره زرین حله مینا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ولی جان خواهد از تو وقت بازی</p></div>
<div class="m2"><p>که اینجا رایگان نبود تماشا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فلک چون دست یابد در خلد نیش</p></div>
<div class="m2"><p>تو خواهی جنگ کن خواهی مدارا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو از من ایدل این یک پند بشنو</p></div>
<div class="m2"><p>اگر هستی بکار خویش بینا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو گردون سفله پرور گشت و خس طبع</p></div>
<div class="m2"><p>خس و سفله توانی بود؟ حاشا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>برو ملک قناعت جوی از یراک</p></div>
<div class="m2"><p>در آن عالم نبینی فقر اصلا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو گردر کوی حکمت خانه سازی</p></div>
<div class="m2"><p>نباشد با جهانت هیچ پروا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ترا چون هیچ حقی بر قضا نیست</p></div>
<div class="m2"><p>نه زشتست از قضا کردن تقاضا؟</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز درویشی ده آب کشت حکمت</p></div>
<div class="m2"><p>ز خاموشی حیات جان گویا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مکن بر چرخ نیک و بد حوالت</p></div>
<div class="m2"><p>که این از هیچ عاقل نیست زیبا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فلک سر گشته و بی اختیار است</p></div>
<div class="m2"><p>چرا با او همی گیری محاکا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فلک را بر خلاف حکم تقدیر</p></div>
<div class="m2"><p>بسعد و نحس گشتن نیست یا را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه فعل چرخ و سعی انجم است این</p></div>
<div class="m2"><p>که هست این کار دانای توانا</p></div></div>