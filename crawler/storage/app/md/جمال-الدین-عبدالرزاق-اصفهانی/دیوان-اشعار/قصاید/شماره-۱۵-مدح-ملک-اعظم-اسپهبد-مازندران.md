---
title: >-
    شمارهٔ  ۱۵ - مدح ملک اعظم اسپهبد مازندران
---
# شمارهٔ  ۱۵ - مدح ملک اعظم اسپهبد مازندران

<div class="b" id="bn1"><div class="m1"><p>زهی بمشرق و مغرب رسیده انعامت</p></div>
<div class="m2"><p>شکوه خطبه وسکه زحشمت نامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زتست نصرت اسلام از آن فلک خواند است</p></div>
<div class="m2"><p>حسام دولت و دین و علاء اسلامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزرگ سایه یزدان و آفتاب ملوک</p></div>
<div class="m2"><p>که فتح و نصرت فخر آورند از ایامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعاع رایت صبح است صبح رایاتت</p></div>
<div class="m2"><p>زهاب چشمه فتح است جوی صمصامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نجوم قبله شناسند طاق ایوانت</p></div>
<div class="m2"><p>ملوک سجده گذارند پیش پیغامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمان متابع فرمان آفتاب وشت</p></div>
<div class="m2"><p>زمین مسخر شمشیر آسمان فامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جلای چشم ستاره غبار موکب تست</p></div>
<div class="m2"><p>طراز دوش ثریا فروغ اعلامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مرک، قاطع آجال عکس شمشیرت</p></div>
<div class="m2"><p>چو ابر، واهب ارزاق رشح اقلامت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زیاد رفته ازل را بدایت ملکت</p></div>
<div class="m2"><p>نشان نداده ابد انتها ی فرجامت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نبود دانه انجم دراین دوازده برج</p></div>
<div class="m2"><p>که پرهمیزد سیمرغ ملک دردامت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بس بزرگی اندر نیافت ادراکت</p></div>
<div class="m2"><p>زبس معانی قابل نگشت اوهامت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بدست بخششت این هفت قصر یک قبضه</p></div>
<div class="m2"><p>بپای رفعت این نه سپهر یک گامت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خجل زجود تو نابوده کس مگر گنجت</p></div>
<div class="m2"><p>تهی زپیش تو کس برنگشته جز جامت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کهینه چاوش درگاه قیصر رومت</p></div>
<div class="m2"><p>کمینه هندوک بام زنگی شامت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بسا که رایض تقدیر زیرران میداشت</p></div>
<div class="m2"><p>سپهر تو سن تا کردش اینچنین رامت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز هر چه تتق غیب روی پوشیدست</p></div>
<div class="m2"><p>ضمیر پاک تو زانجمله کرده اعلامت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چه ماند مشکل بررای تو چو روح القدس</p></div>
<div class="m2"><p>کند بواسطه نور عقل الهامت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بذوق لفظ تو جان خرد نیافت شکر</p></div>
<div class="m2"><p>وگر نداری باور بدان باور بدان دوبادامت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طمع قوی شود از جود گنج پردازت</p></div>
<div class="m2"><p>گنه خجل شود از عفو دوزخ آشامت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زشرق وغرب گذشتست صیت انصافت</p></div>
<div class="m2"><p>بخاص و عام رسیده است فیض انعامت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برتو آمده دریا که تا بیاموزد</p></div>
<div class="m2"><p>سخا زدست گهر بخش معدن انجامت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا رسد که نهم زین برابلق ایام</p></div>
<div class="m2"><p>که خوانده بنده خاصم زبخشش عامت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منم زمحض سخایت چو کعبه دردنیا</p></div>
<div class="m2"><p>ندیده ذال سؤال و نداده ابرامت ذل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>طمع زدر گه تو غافل و بصد منزل</p></div>
<div class="m2"><p>عطا ندیده فرستاده لطف و اکرامت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گران نبود طمع را که از پی بخشش</p></div>
<div class="m2"><p>بهانه جوید از ینگونه جود خود کامت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از آن ملوک مسلم کنند تقدیمت</p></div>
<div class="m2"><p>که برسخاوت از اینگونه است اقدامت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو آرزوی زمین بو س حضرتت کردم</p></div>
<div class="m2"><p>زبان هیبت تو گفت نیست هنگامت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو نور خورشید از دور میطلب که کرم</p></div>
<div class="m2"><p>ضمان همیکند انعام شه باتمامت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا که گشایند صورت ارکانت</p></div>
<div class="m2"><p>همیشه تا که نمایند جنبش اجرامت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مباد جز همه در زیر چتر جنبش تو</p></div>
<div class="m2"><p>مباد جز همه بر تخت ملک آرامت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به پیش تخت تو باذند حلقه اندر گوش</p></div>
<div class="m2"><p>ملوک مشرق و مغرب برسم خدامت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کما نگشای وکش چو ماه و خورشیدت</p></div>
<div class="m2"><p>دویت دارو سلیحی چو تیر و بهرامت</p></div></div>