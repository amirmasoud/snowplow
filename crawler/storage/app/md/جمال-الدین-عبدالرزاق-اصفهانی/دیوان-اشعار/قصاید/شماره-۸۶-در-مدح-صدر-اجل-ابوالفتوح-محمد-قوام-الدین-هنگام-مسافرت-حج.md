---
title: >-
    شمارهٔ  ۸۶ - در مدح صدر اجل ابوالفتوح محمد قوام الدین هنگام مسافرت حج
---
# شمارهٔ  ۸۶ - در مدح صدر اجل ابوالفتوح محمد قوام الدین هنگام مسافرت حج

<div class="b" id="bn1"><div class="m1"><p>هزار منت و شکر خدای عز و جل</p></div>
<div class="m2"><p>که سوی صدر خرامید باز صدر اجل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امام مشرق و مغرب قوام دین خدای</p></div>
<div class="m2"><p>ابوالفتوح محمد سپهر مجد دول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گزیده طالع و طلعت ستوده سیرت و طبع</p></div>
<div class="m2"><p>سپر پایه و قدر و ستاره جاه و محل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگرد مسند او در، طوافگاه امل</p></div>
<div class="m2"><p>بچین ابروی او بر، مصافگاه اجل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز عرصه حرمش پای حادثه شده لنگ</p></div>
<div class="m2"><p>زدامن شرفش دست نایبه شده شل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود ز یکنظر او هزار غم راحت</p></div>
<div class="m2"><p>شود ز یکسخن او هزار مشکل حل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه عقل یک کلمت زو شنیده مستنکر</p></div>
<div class="m2"><p>نه طبع یکحرکت زو بدیده مستقبل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بلطف جان ز تن دشمنان کند بیرون</p></div>
<div class="m2"><p>که خلق او چو گلست و عدوی او چو جعل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخای بیش ز خواهش عطای بی منت</p></div>
<div class="m2"><p>دو آیتست که در شأن او بود منزل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در ابتدای جوانی ادای این مفروض</p></div>
<div class="m2"><p>عنایتیست بزرگ از خدای عزوجل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز یمن دولت او دان کزین صفت امسال</p></div>
<div class="m2"><p>شدست بادیه یکسر بمرغزار بدل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو خط و عارض دلدار شد زسبزه و آب</p></div>
<div class="m2"><p>رهی که بود چو چشم لئیم و تارک کل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز بس زهاب ببایست اندرو کشتی</p></div>
<div class="m2"><p>زبس گیاه ببایست اندرو منجل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بحوضهای وی انر زلال تر زان آب</p></div>
<div class="m2"><p>که بامدادان بر برگ گل نشیند طل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفته طبع صبا اندرو سموم چنان</p></div>
<div class="m2"><p>که گشته مستغنی نرگس اندراو ز بصل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دمد ز خار مغیلان کنون گل خودروی</p></div>
<div class="m2"><p>شود بطعم شکر زین سپس در او حنظل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بصحن بادیه بر کاسه های سر بودی</p></div>
<div class="m2"><p>کنون ز فرش پر مرتع آمد و منهل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رهی که بود در آنراه عافیت از بیم</p></div>
<div class="m2"><p>گرفته همچو بنفشه کلاه زیر بغل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون چو نرگس بودند طاس زر بر کف</p></div>
<div class="m2"><p>ز بسکه امن همیزد ندا که لاتوجل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چه رفت بظاهر سه اسبه همچو قلم</p></div>
<div class="m2"><p>بسر برید همی راه بارگاه ازل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تبارک الله ازان کوه شکل ناقه او</p></div>
<div class="m2"><p>زمین نورد و فلک سیر و آسمان هیکل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگام او بگه پویه صعب گشته ذلول</p></div>
<div class="m2"><p>بپای او بگه سیر سهل گشته جبل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رسنده تر ز قضا و دونده تر ز خیال</p></div>
<div class="m2"><p>جهنده تر زجهان و رونده تر ز مثل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز کوب زخمش تلها نموده همچو مغاک</p></div>
<div class="m2"><p>ز جرم ضخمش گشته مغاکها چون تل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خجسته طلعت او ازستام او تابان</p></div>
<div class="m2"><p>چنانکه طلعت خورشید از فراز قلل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دو کعبه دیدند امثال حاجیان بعیان</p></div>
<div class="m2"><p>نه آنچنان که دو بیننده دیده احول</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یکی است کعبه حجاج و عرضه گاه دعا</p></div>
<div class="m2"><p>یکی است قبله محتاج و تکیه گاه امل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حریم هر دو میادین حرمتست و قبول</p></div>
<div class="m2"><p>یمین هر دو محل میامنست و قبل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دل یکی شده فارغ زعشق آن دومین</p></div>
<div class="m2"><p>چنانکه شد دل آن خالی از منات و هبل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زهی چو روح مجسم بصورت و معنی</p></div>
<div class="m2"><p>زهی چو لطف مصور مفصل و مجمل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه در تو کبر بمقدار ذرۀ هرگز</p></div>
<div class="m2"><p>نه در تو بخل بمثقال حبۀ خردل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>براه دین چو خردنیست در دلت غفلت</p></div>
<div class="m2"><p>بکار خیر چو توفیق نیست در تو کسل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو گرد کعبه کشیدی تو دایره زطواف</p></div>
<div class="m2"><p>کشیده گشت خطی بر همه خطاوزلل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زخط و دایره کز طواف و سعی کشند</p></div>
<div class="m2"><p>صحیفه حسنات تو گشت پر جدول</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مثال کعبه و سعی تو مرکز و پرگار</p></div>
<div class="m2"><p>نشان حلقه و دست تو همچو گوی انکل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سوی مدینه خرامان شده بر اوج شرف</p></div>
<div class="m2"><p>چنانکه چشمۀ خورشید سوی برج حمل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سرای پرده عصمت زده بهر منزل</p></div>
<div class="m2"><p>شده ز حفظ خدا بدرقه بهر مرحل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چنان ز طلعت تو برفروخت آن بقعه</p></div>
<div class="m2"><p>کز آه صبح زد آیینه فلک مصقل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>قران علوی خود محترم از این بودست</p></div>
<div class="m2"><p>که چون توئی برسدنزد احمد مرسل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نشان اینسخن آنست کاندران تاریخ</p></div>
<div class="m2"><p>قران ز شرم نکردند مشتری و زحل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تو رفته وامده وز تو کسی نیازرده</p></div>
<div class="m2"><p>همین دلیل تمامست بر قبول عمل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زهی مبارک پی خواجۀ که از فرت</p></div>
<div class="m2"><p>بناب افعی در زهر یافت طعم عسل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سپاس و منت بیحد خدایرا که ترا</p></div>
<div class="m2"><p>نشد بگرد ریا آبروی مستعمل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدای داند مستغنیم ازین سوگند</p></div>
<div class="m2"><p>که بی حضور شما بود اصفهان مهمل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ز شوق گشته نفسهای ما همه یالیت</p></div>
<div class="m2"><p>ز امید گشته زبانهای ما عسی و لعل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز شوق طلعت عالیت بر وضیع و شریف</p></div>
<div class="m2"><p>چنان گذشت همی روز و شب که لاتسئل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز دل نشاط بیکبار گشته بیگانه</p></div>
<div class="m2"><p>ز دیده خواب بیک راه گشته مستأصل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همیشه تا که عرب را بپاید اندر شعر</p></div>
<div class="m2"><p>صفات یار و دیار و حدیث رسم و طلل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بقای مدت عمر تو باد چندانی</p></div>
<div class="m2"><p>که عاجز آید از اعداد آن حروف جمل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نهاده در دل تو روزگار نقطه بسط</p></div>
<div class="m2"><p>کشیده بر سر خصمت زمانه خط بطل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خجسته بادت و لابد خجسته خواهد بود</p></div>
<div class="m2"><p>چنین سفر که مثل بودش آخر و اول</p></div></div>