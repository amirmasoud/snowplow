---
title: >-
    شمارهٔ  ۳۲ - در مدح امیر یمین الدین
---
# شمارهٔ  ۳۲ - در مدح امیر یمین الدین

<div class="b" id="bn1"><div class="m1"><p>کسیکه قصد سر زلف آن نگار کند</p></div>
<div class="m2"><p>چو زلف او دل خود زار و بیقرار کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسیکه دارد امید کنار بوس ازو</p></div>
<div class="m2"><p>بسا که خون دل از دیده برکنار کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم ربود بدانزلف همچو چنگل باز</p></div>
<div class="m2"><p>تو هیچ باز شنیدی که دل شکار کند؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار جور کند بر دلم بیک ساعت</p></div>
<div class="m2"><p>وگر بنالم ازو هر یکی هزار کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بتی که مرکز مه لعل آبدار نهد</p></div>
<div class="m2"><p>مهی که پروز گل مشک تا بدار کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گه از بنفشه خطی برمه دو هفته کشد</p></div>
<div class="m2"><p>گهی ز سنبل پرچین لاله زار کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقاب برفکند خارگل نهد ازرنگ</p></div>
<div class="m2"><p>چو زلف بر شکند بوی مشک خوار کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلیم قلبم خواند که عشق جای دلم</p></div>
<div class="m2"><p>میان حلقه آنزلف مشگبار کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلیم دل بود آری درین چه باشد شک</p></div>
<div class="m2"><p>کسیکه جای دل اندردهان مار کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر زلعل لبش زلف می همی نوشد</p></div>
<div class="m2"><p>چرا دو چشم خوشش هر شبی خمار کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یکی نگارا رحمت نمای بردل من</p></div>
<div class="m2"><p>که همچو زیر زغم نالهای زار کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان مکن که ز بیطاقتی دل رنجور</p></div>
<div class="m2"><p>شکایتی ز تو با صدر روزگار کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کریم مطلق و حرز زمانه رکن الدین</p></div>
<div class="m2"><p>که روزگار بمثل وی افتخار کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیاز پیش سخایش دهن فرو بندد</p></div>
<div class="m2"><p>امید وقت عطایش دودیده چار کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زجود دستش سائل همی برد بدره</p></div>
<div class="m2"><p>نه آنکه وعده پذیرد نه انتظار کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زسهم خشم وی آتش همیشه لرزانست</p></div>
<div class="m2"><p>اگر چه خود را زاهن همی حصار کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سموم خشمش اگر بگذر بدریابار</p></div>
<div class="m2"><p>بخار شعله شود قطره ها شرار کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نسیم خلقش اگر بروزد بصحرا بر</p></div>
<div class="m2"><p>درخت عود شود شاخ مشک بار کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر بگوئی پیشت درم بر افشاند</p></div>
<div class="m2"><p>وگرنگوئی پیشت گهر نثار کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آنزمان که نشیند بصدر ایوان بر</p></div>
<div class="m2"><p>بدانگهی که قلم بربنان سوار کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سپهر خواهد تا بهر دفع چشم بدان</p></div>
<div class="m2"><p>زماه نو شده در ساعدش سوار کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بسیم نام نکو میخرد زاهل هنر</p></div>
<div class="m2"><p>وزین تجارت بهتر کسی چکار کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زشرم همت اوبحر در عرق غرقست</p></div>
<div class="m2"><p>زبیم جودش خورشید زینهار کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه باولیش بخت سازگار بود</p></div>
<div class="m2"><p>همیشه باعدویش چرخ کارزارکند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کسیکه دید دل و دست او گه بخشش</p></div>
<div class="m2"><p>بآفتاب و بدریا چه اعتبار کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بپیش لفظ گهربار او خجل گردد</p></div>
<div class="m2"><p>صدف که قطره همی در شاهوار کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>همی پذیرد منت چو میکند بخشش</p></div>
<div class="m2"><p>نه آن سخی است که بردادن اختصار کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زهی بزرگ عطائی که جود و بخشش تو</p></div>
<div class="m2"><p>بچشم هر کس زررا چو خاک خوار کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>توئی که بیش زمدحت مرا صلت دادی</p></div>
<div class="m2"><p>چنین کرم چو تو صدری بزرگوار کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو تو بزرگی را مادحی چو من باید</p></div>
<div class="m2"><p>که مدح تو بسخنهای آبدار کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زبهر مرکب خاص تو را یض تقدیر</p></div>
<div class="m2"><p>همیشه ابلق ایام راهوار کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سپهر خدمت درگاه تو بطوع و بطیع</p></div>
<div class="m2"><p>بروزی اندر بیش از هزاربار کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خلل نیاید در جاه تو که قاعده را</p></div>
<div class="m2"><p>چو بخت باشد معمار استوار کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببوی خلق تو هرگز کجا تواند بود</p></div>
<div class="m2"><p>نسیم صبح چو بر برگ گل گذار کند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نهاده گردون سوی تو صد هزاران چشم</p></div>
<div class="m2"><p>که رای عالی تو خود چه اختیار کند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گمان مبرکه رهی اندرین قصیده همی</p></div>
<div class="m2"><p>اشارتی بتقاضای رسم پار کند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>همیشه تا که فلک گرد خاک میگردد</p></div>
<div class="m2"><p>همیشه تا که قمر برفلک مدار کند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سر تو سبز و دلت شاد باد و مدت عمر</p></div>
<div class="m2"><p>فزون از آنکه مهندس براو شمار کند</p></div></div>