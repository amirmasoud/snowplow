---
title: >-
    شمارهٔ  ۱۰۶ - درمدح نصره الدین جهان پهلوان
---
# شمارهٔ  ۱۰۶ - درمدح نصره الدین جهان پهلوان

<div class="b" id="bn1"><div class="m1"><p>زهی بنفحه عدل تو زنده جان جهان</p></div>
<div class="m2"><p>بدست حکم تو داده فلک عنان جهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یگانه خسرو گیتی گشای نصرت دین</p></div>
<div class="m2"><p>ستوده پادشه شرق و پهلوان جهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>توئی سکندر ایام و شهریار زمین</p></div>
<div class="m2"><p>توئی سپه کش اسلام و مرزبان جهان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهان نثار تو کرده ستارگان فلک</p></div>
<div class="m2"><p>فلک خطاب تو کرده خدایگان جهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنور رای تو تابنده روشنان فلک</p></div>
<div class="m2"><p>بخاکپای تو سوگند سرکشان جهان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ثنا و مدحت تو سبحه زبان ملک</p></div>
<div class="m2"><p>دعای دولت تو رقیه امان جهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگشت چون تو ملک طبع در گمان گمان</p></div>
<div class="m2"><p>نخاست چون تو جهاندار از جهان جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخاکپای تو ماندست تشنه آب حیات</p></div>
<div class="m2"><p>بآب تیغ تو گشتست پخته نان جهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمینه شعله رای تو آفتاب فلک</p></div>
<div class="m2"><p>نخست پایه قدر تو لامکان جهان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعاع تیغ تو چون تیغ آفتاب رسید</p></div>
<div class="m2"><p>ازین گران جهان تابدان کران جهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خلاف رای تو گر صبح دم زند گردون</p></div>
<div class="m2"><p>بتیغ مهر دو نیمه کند میان جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز لوح غیب بخواند ضمیر روشن تو</p></div>
<div class="m2"><p>رمو ز هرچه معماست در نهان جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدای کوس تو چونانگرفت در گردون</p></div>
<div class="m2"><p>که مرغ فتنه بپرید از آشیا ن جهان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر نه شحنه عدلت کند جهانداری</p></div>
<div class="m2"><p>از آنسوی عدم آرد جهان نشان جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وگرنه حلم تو باشد زهیبت تو فتد</p></div>
<div class="m2"><p>بزلزله تب لرزاندر استخوان جهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تبارک الله ازان بی کرانه لشگر تو</p></div>
<div class="m2"><p>که قاصر است ز تفصیل آن بیان جهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگرچه مشکل جذر اصم شود زو حل</p></div>
<div class="m2"><p>بعقد صد یک ازو کی رسد بنان جهان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بحرص خدمت توتاخت از عدم بوجود</p></div>
<div class="m2"><p>هرآنه حشو مکانگشت وایرمان جهان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بتازیانه اشارت نما و لشگر بین</p></div>
<div class="m2"><p>زقیروان جهان تا بقیروان جهان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو بخت ملک باوج محیط گردون نه</p></div>
<div class="m2"><p>که نیست لایق تخت تو خاکدان جهان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جهان بعهد تو معهود بود از گردون</p></div>
<div class="m2"><p>بلب رسید ازین انتظار جان جهان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگرنه عدل تو دریافتی و شاق ستم</p></div>
<div class="m2"><p>بداده بود بتاراج خانمان جهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مثال فتنه یأجوج و سد آهن چیست</p></div>
<div class="m2"><p>مثال تیغ تو در آخر الزمان جهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خدایگانا حال جهان بلطف ببین</p></div>
<div class="m2"><p>که رفت در سر جود تو سوزیان جهان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهان بنیل سخای تو گل کجا دارد</p></div>
<div class="m2"><p>چه مایه گنجد در مکنت و توان جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>حدیث حاتم طائی و نام نوشروان</p></div>
<div class="m2"><p>بروزگار تو گمشد ز داستان جهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان اگر پس ازین نام آنگروه برد</p></div>
<div class="m2"><p>برون کشد ز پس سرقضا زبان جهان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه جا نکند پس از ینهر که کانکند که سخات</p></div>
<div class="m2"><p>رها نکرد جوی در دکان کان جهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بخاکپای تو سوگند میخورد گردون</p></div>
<div class="m2"><p>که مثل تو نزند سر زبادبان جهان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز تیغ تیز تو گر دشمنی امان جوید</p></div>
<div class="m2"><p>رهاش کن که بود دور از امان جهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سگ تو مغز سر گردنان خورد شاید</p></div>
<div class="m2"><p>که دشمن تو جگر میخورد ز خوان جهان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز دزد حادثه دهر ایمنست کز پاست</p></div>
<div class="m2"><p>جواز بدرقه دارند کاروان جهان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو هندوانه پسندیده پاسبانی را</p></div>
<div class="m2"><p>بس است هندوی تیغ تو پاسبان جهان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بپشت گرمی بازوی تو کند ور نی</p></div>
<div class="m2"><p>برهنه هندو کی کی کند ضمان جهان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همیشه تا که ببافد زرشته شب و روز</p></div>
<div class="m2"><p>سپهر کسوت اکسون و پرنیان جهان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بهار عدل تو سرسبز باد و پاینده</p></div>
<div class="m2"><p>که بس بود زدم دشمنان خزان جهان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در موافق تو قبله ملوک زمین</p></div>
<div class="m2"><p>دل مخالف تو لقمه دهان جهان</p></div></div>