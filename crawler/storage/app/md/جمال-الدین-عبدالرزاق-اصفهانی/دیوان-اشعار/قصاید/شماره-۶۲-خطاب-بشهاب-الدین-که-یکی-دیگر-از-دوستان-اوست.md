---
title: >-
    شمارهٔ  ۶۲ - خطاب بشهاب الدین که یکی دیگر از دوستان اوست
---
# شمارهٔ  ۶۲ - خطاب بشهاب الدین که یکی دیگر از دوستان اوست

<div class="b" id="bn1"><div class="m1"><p>خلاصه همه عالم اجل شهاب الدین</p></div>
<div class="m2"><p>که روشنند زرای تو ثابت وسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بریز سایه رای تو چشمه خورشید</p></div>
<div class="m2"><p>فرود پایه قدر تو گنبد دوار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمینه قطره زجود تو آب در قلزم</p></div>
<div class="m2"><p>کهینه شمه زخلق تو مشک در تاتار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دعا و خدمت خادم قبول فرمایند</p></div>
<div class="m2"><p>فزون زلشگر ذرات و قطره امطار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقین شناس که گر شرح اشتیاق دهم</p></div>
<div class="m2"><p>دراز گردد و آنگه ملالت آرد بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیاض روز اگر فی المثل شود کاغذ</p></div>
<div class="m2"><p>دگر مداد شود جمله آبهای بحار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوند موی بر اندامهای من همه دست</p></div>
<div class="m2"><p>قلم شود بجهان در هر آنچه هست اشجار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من آن نویسم تا جملگی زمن پرشد</p></div>
<div class="m2"><p>هنوز گفته نباشم مگر یکی زهزار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توئی که مر کز عقلی و دوستدارانت</p></div>
<div class="m2"><p>مدام گرد تو باشند حلقه دایره وار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>منم زحلقه برون مانده وزپی مرکز</p></div>
<div class="m2"><p>بسر همی دوم از گرد خویش چون پرگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنان بذکر تو آراسته است محفلها</p></div>
<div class="m2"><p>که نام عبدلطیف آید از در و دیوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرای تو که درآن نظم داشتیم اکنون</p></div>
<div class="m2"><p>در آن دیار نگردد زغیبتت دیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شدند جمله پراکنده چون بنات النعش</p></div>
<div class="m2"><p>جماعتی که چو پروین بدند پیش تو پار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو همچو شمعی و اصحاب جمله پروانه</p></div>
<div class="m2"><p>بشمع جمع توانند آمدن ناچار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدوستی و بنان و نمک که عزم آن بود</p></div>
<div class="m2"><p>که بر سبیل تماشا کنم سوی تو گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ولی توقفم از ضعف چارپایانست</p></div>
<div class="m2"><p>که بیش از انکه توانگفت لاغر ندونزار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدای داند و دانم تو نیز میدانی</p></div>
<div class="m2"><p>که بی تو نیستم از عیش خویش بر خوردار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رخم چو آبی زردست و بروی از غم گرد</p></div>
<div class="m2"><p>فسرده از دم سرد اشک من چو دانه نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپیده دم که نسیم آورد بمن بویت</p></div>
<div class="m2"><p>کنم براو زدل خوش روان خویش نثار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هزار جان گرامی بناز پرورده</p></div>
<div class="m2"><p>فداش بادکه بوی آورد مرا ازیار</p></div></div>