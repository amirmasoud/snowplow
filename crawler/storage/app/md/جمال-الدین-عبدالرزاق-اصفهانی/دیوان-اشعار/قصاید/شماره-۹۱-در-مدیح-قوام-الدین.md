---
title: >-
    شمارهٔ  ۹۱ - در مدیح قوام الدین
---
# شمارهٔ  ۹۱ - در مدیح قوام الدین

<div class="b" id="bn1"><div class="m1"><p>ای یافته از قدر بر افلاک تقدم</p></div>
<div class="m2"><p>وی کرده گه حکم بر اجرام تحکم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخدوم جهان صدر قوام الدین کاینچرخ</p></div>
<div class="m2"><p>گفته است که مأمور توأم فأمرو احکم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رای تو منزه بود از ننگ تردد</p></div>
<div class="m2"><p>جود تو مسلم بود از مطل و تلعثم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فخر آورد از روی شرف بر سر گردون</p></div>
<div class="m2"><p>خاکی که بر او مرکب خاص تو نهدسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون درجگر آهوی چین مشک شود زانک</p></div>
<div class="m2"><p>کرد اوز نسیم دم خلق تو تنسم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیش دلت ار موج زند بحر بجنبش</p></div>
<div class="m2"><p>هم موج قفایش زند از دست تلاطم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دست گهر بار تو کی باشد هرگز</p></div>
<div class="m2"><p>ورچند ز گوهر بود انباشته قلزم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرگز جگر تشنه کجا سیر کند خاک</p></div>
<div class="m2"><p>ور چه عوض آب بود وقت تیمم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کانست در ایام تو مظلوم ولیکن</p></div>
<div class="m2"><p>در عهد تو معهود نبودست تظلم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پروین بنمودست باعدای تو دندان</p></div>
<div class="m2"><p>تا در رخ احباب تو کردست تبسم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای قدر تو افزون شده از دایره چرخ</p></div>
<div class="m2"><p>وی جاه تو بیرون شده از حد توهم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم کرده گنه را کرم و عفو توپی کور</p></div>
<div class="m2"><p>هم کرده ستم را اثر عدل تو ره گم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندیشه مدیح تو نهان کرده گه نظم</p></div>
<div class="m2"><p>از بسکه معانی کند آنجای تراکم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر محترق و راجع و منحوس نبودی</p></div>
<div class="m2"><p>چون رای تو بودی بدرخشیدن انجم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ممدوح نباشد چو تو کز مدح تو مارا</p></div>
<div class="m2"><p>تحسین دمادم بود احسان دمادم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صدرا بگرم گرچه صداعست ولیکن</p></div>
<div class="m2"><p>بشنو سخن بنده و فرمای تجشم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنم که گه مدح تو چون موی شکافم</p></div>
<div class="m2"><p>سرگشته شود عقل از ادراک و تفهم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خوندل من میخورد اینچرخ وزینروی</p></div>
<div class="m2"><p>در خوندل خویش همی جوشم چون خم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>محروم چنانم که ز حرمان بغایت</p></div>
<div class="m2"><p>بر حال من اعدای مراهست ترحم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من چونسمت خدمت صدری چو تو دارم</p></div>
<div class="m2"><p>بر من چه کند چرخ سراسیمه تهاجم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من صبحم و در مدح تو جز صدق نگویم</p></div>
<div class="m2"><p>این یافه درایان را کذبست تکلم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اینان همه صبحند ولیکن همه کاذب</p></div>
<div class="m2"><p>بر من همه زین یافته باشند تقدم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شد تیزی خاطر سبب سوختن من</p></div>
<div class="m2"><p>شد نرمی قاقم سبب کشتن قاقم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آهوی من آنست که بر دو نان از حرص</p></div>
<div class="m2"><p>چون سگ بنجنبانم صدبار سر و دم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من مدح چرا گویم از بهر دو من نان</p></div>
<div class="m2"><p>آنرا که سمن باز نداند ز تورم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر من نکند هیچ اثر نکبت ایام</p></div>
<div class="m2"><p>چونانکه خدررا نرسد رنج تألم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فضلم عوض رزق من آمد مگر از چرخ</p></div>
<div class="m2"><p>چون نیش که آمد عوض دیده کژدم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاید که بدین شهر کند چرخ تفاخر</p></div>
<div class="m2"><p>شاید که بدین مدح کند عقل ترنم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا شعشعشۀ نور توان جست ز خورشید</p></div>
<div class="m2"><p>تا خاصیت نطق توان یافت ز مردم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پایاد وجود تو که آنمعنی جودست</p></div>
<div class="m2"><p>از تو همه تسلیم و ز زوار تسلم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خصمت ز فلک کوفته و پشت خمیده</p></div>
<div class="m2"><p>بدریده شکم پوست ز سر کنده چو گندم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اعدای تو از نکبت ایام بحالی</p></div>
<div class="m2"><p>کش مرگ فجی باشد از انواع تنعم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فی العز و فی الدوله ماشئت تعش عش</p></div>
<div class="m2"><p>فی القدرة و النصرة ماشئت تدم دم</p></div></div>