---
title: >-
    شمارهٔ  ۲۸ - یمدح الصدر السعید امین الدین صالح المستوفی
---
# شمارهٔ  ۲۸ - یمدح الصدر السعید امین الدین صالح المستوفی

<div class="b" id="bn1"><div class="m1"><p>ایکه خورشید ز رای تو منور گردد</p></div>
<div class="m2"><p>عالم از نفحه خلق تو معطر گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه شرق امین الدین صالح که فلک</p></div>
<div class="m2"><p>پیش فرمان تو خم گیرد و چنبر گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرم خورشید اگر از دل تو نور برد</p></div>
<div class="m2"><p>همه ذرات در اقطار هوا زر گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل هر گه در آیینه طبعت نگرد</p></div>
<div class="m2"><p>پیش او سردل غیب مصور گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هفت دریا زنهیب کف تو خشک شود</p></div>
<div class="m2"><p>وقت بخشش چو زبان قلمت تر گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ننهد پای ز خط تو چو نقطه بیرون</p></div>
<div class="m2"><p>هرکه چون دایره خواهد که همه سر گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر مجسم شود این منصب رفعت که تراست</p></div>
<div class="m2"><p>چرخ هیهات که پیرا من این در گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیر ابخر اگراخلاق ترا شرح دهد</p></div>
<div class="m2"><p>دمش از خوش نفسی چون دم مجمر گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دم آهو زدم خلق تو و فضله گاو</p></div>
<div class="m2"><p>نافه مشک شود بیضه عنبر گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بردیاری که در او صرصر خشم تو جهد</p></div>
<div class="m2"><p>کوه بر خیزد از جای و زمین در گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرگ را آرزوی دایگی میش کند</p></div>
<div class="m2"><p>گرش انصاف تو در طبع مقرر گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زورق چرخ زموج حرکت باز استد</p></div>
<div class="m2"><p>گرش از حلم گر انسنگ تو لنگرگردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسر انگشت اشارت کن اگر فرمانست</p></div>
<div class="m2"><p>تات هفت اخترونه چرخ مسخر گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای کریمی که سخای تو بحدی برسید</p></div>
<div class="m2"><p>کانکه در خواب ترا دید توانگر گردد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رای خوبت مدد لشگر نصرت بخشد</p></div>
<div class="m2"><p>نوک کلکت سبب عطلت خنجر گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نزد جود تو زطع فارغ و گستاخ آید</p></div>
<div class="m2"><p>پیش حلم تو گنه شوخ و دلاور گردد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ اگر خاک درت نیست بتحقیق چرا</p></div>
<div class="m2"><p>هر شبی سرمه چشم مه و اختر گردد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر که در پای تو افتاد شود صدر نشین</p></div>
<div class="m2"><p>وانکه او دست تو بوسد سرو سرور گردد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میکشد چرخ زدور اینهمه سرگردانی</p></div>
<div class="m2"><p>بو که باخیمه قدر تو برابر گردد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صدف چرخ بساغصه و غوطه که خورد</p></div>
<div class="m2"><p>تا چو تو قطره دراودانه گوهر گردد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عرصه ملک بهشتیست ز عدلت که در او</p></div>
<div class="m2"><p>کلک تو طوبی و الفاظ تو کوثر گردد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش چشمست مرا آنکه وشاق در تو</p></div>
<div class="m2"><p>آمر و ناهی این گنبد اخضر گردد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روی مظلوم درین عهد نبیند گردون</p></div>
<div class="m2"><p>اگر اطراف ممالک مثلا برگردد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر سر کلک تو یکلحظه دهد آسایش</p></div>
<div class="m2"><p>ملک دیگر شود و قاعده دیگر گردد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همتت ملک زمین راننهد وزن همی</p></div>
<div class="m2"><p>همه گرد طرف عالم اکبر گردد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بهرروزی که کند خطبه بنام تو فلک</p></div>
<div class="m2"><p>شاخ سد ره سزد ار پایه منبر گردد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آتش خشم تو گر شعله ببالا بکشد</p></div>
<div class="m2"><p>وای طاوس فلک گرنه سمندر گردد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسمان از تو ضمان کرد که آمال ترا</p></div>
<div class="m2"><p>هرچه از بخت تمناست میسر گردد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چونکه من بهر مدیح تو قلم برگیرم</p></div>
<div class="m2"><p>خواهد این نه ورق چرخ که دفتر گردد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آسمان را نبود زهره این خطبه اگر</p></div>
<div class="m2"><p>بر عروس سخنم مدح تو زیور گردد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر همه عمر معانی ترا حصر کنم</p></div>
<div class="m2"><p>معنیش کم نشود لفظ مکرر گردد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا همی طبع هوا گرم شود سرد شود</p></div>
<div class="m2"><p>تا همی نور مه افزاید و کمتر گردد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باد انعام تو چندانکه بهر شهر رسد</p></div>
<div class="m2"><p>باد بدخواه تو چونانکه بهر در گردد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مدت عمر تو چندانکه محاسب گه عقد</p></div>
<div class="m2"><p>از شمار عددش عاجز و مضطر گردد</p></div></div>