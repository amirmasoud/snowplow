---
title: >-
    شمارهٔ  ۱۰۰ - در مرثیه امام سعید جمال الدین محمود
---
# شمارهٔ  ۱۰۰ - در مرثیه امام سعید جمال الدین محمود

<div class="b" id="bn1"><div class="m1"><p>چه شد که عالم معنی خراب می‌بینم</p></div>
<div class="m2"><p>چه شد که ماه کرم در سحاب می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شد که با همه‌کس اضطرار می‌یابم</p></div>
<div class="m2"><p>چه شد که در همه‌کس اضطراب می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سوگ طوطی سرسبز شکرین‌الفاظ</p></div>
<div class="m2"><p>جهان سیاه چو پر غراب می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغ حنجر مشکل‌گشای فضل‌نمای</p></div>
<div class="m2"><p>که فارغش ز سؤال و جواب می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریغ عالم امید را که ناگاهان</p></div>
<div class="m2"><p>ز سیل قهر خراب و یباب می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغ بحر هنرها جمال دین محمود</p></div>
<div class="m2"><p>کش از سموم اجل چون سراب می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ چون تو جوانی که زیر خاک شدی</p></div>
<div class="m2"><p>که همچو گنجت تحت‌التراب می‌بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چگونه باشد حالم که پس ز صدر کبیر</p></div>
<div class="m2"><p>همی امام سعیدت خطاب می‌بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو جان دانش بودی عجب نباشد اگر</p></div>
<div class="m2"><p>به سوی عالم جانت شتاب می‌بینم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتاده در دل آهن ز مرگ تو آتش</p></div>
<div class="m2"><p>ز چشم سنگ روان گشته آب می‌بینم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوا چرا نفس سرد می‌زند که در او</p></div>
<div class="m2"><p>همی از آتش دل التهاب می‌بینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا نمی‌شود از خویشتن این سخن باور</p></div>
<div class="m2"><p>مگر به خواب درم وین به خواب می‌بینم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه خاندانی از مرگ تو خراب شدست</p></div>
<div class="m2"><p>که عالمی ز غم تو خراب می‌بینم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو ذره گردند اهل هنر پراکنده</p></div>
<div class="m2"><p>ز بعد مرگ تو چون آفتاب می‌بینم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برانده جوی مجره ز آب دیده فلک</p></div>
<div class="m2"><p>در اشک او ز ستاره حباب می‌بینم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز مرگ تست که این خیمه معلق را</p></div>
<div class="m2"><p>شکسته میخ و گسسته طناب می‌بینم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو با تویی بنماند جهان و جان برود</p></div>
<div class="m2"><p>به ترک هردو بگفتن صواب می‌بینم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لعاب گرم بیفسرد و خون نافه ببست</p></div>
<div class="m2"><p>از این بریشم وزان مشک ناب می‌بینم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اجل خجل شد ازین و فلک پشیمانست</p></div>
<div class="m2"><p>میان هردو ازین رو عتاب می‌بینم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مراست قهر مروت که در مصیبت تو</p></div>
<div class="m2"><p>ز دیده‌ها اثر فتح باب می‌بینم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دست مردمک دیده بر ز خون دو چشم</p></div>
<div class="m2"><p>به یاد روی تو جام شراب می‌بینم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز سرخیی که گه صبح و شام در افقست</p></div>
<div class="m2"><p>به خون شده رخ گردون خضاب می‌بینم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز شرم کرده خود چرخ را زنان بر سر</p></div>
<div class="m2"><p>دودست عقرب همچون ذباب می‌بینم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز خون دیده دل سنگ لعل می‌یابم</p></div>
<div class="m2"><p>ز آه دل جگر شب کباب می‌بینم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چرا به مرگ تو شادست دشمنت که به عمر</p></div>
<div class="m2"><p>فذلک همه هم زین حساب می‌بینم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عنان آز به دست نیاز باید داد</p></div>
<div class="m2"><p>که مکرمت را پا در رکاب می‌بینم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ولیک با همه محنت بدان خوشست دلم</p></div>
<div class="m2"><p>که یادگار ازان گل گلاب می‌بینم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>امیدوارم کز قدر بگذرد ز اقران</p></div>
<div class="m2"><p>ازانکه گوهر تیغ از قراب می‌بینم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه خصمش مقهور با دو دوست به کام</p></div>
<div class="m2"><p>چنان بود که دعا مستجاب می‌بینم</p></div></div>