---
title: >-
    شمارهٔ  ۴۶ - قصیده
---
# شمارهٔ  ۴۶ - قصیده

<div class="b" id="bn1"><div class="m1"><p>ایکه خورشید زشرم دل تو آب شود</p></div>
<div class="m2"><p>هفت دریا زسرانگشت تو غرقاب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاکم مشرق و مغرب که همی بهر شرف</p></div>
<div class="m2"><p>باغ اقبال ترا چرخ چو دولاب شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلک دین پرور تو واهب ارزاق شدست</p></div>
<div class="m2"><p>رای روشنگر تو ملهم الباب شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جرم خورشید اگر عکس پذیرد زدلت</p></div>
<div class="m2"><p>در هوا ذره چو گاورسه زرناب شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کان و در یاز دل و دستت اراندیشه کنند</p></div>
<div class="m2"><p>دل دریا بچکد زهره کان آب شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل هرگه که کند رای تو تعلیم گری</p></div>
<div class="m2"><p>لوح زیر بغل آورده بکتاب شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد لطف تو اگر بردل افعی گذرد</p></div>
<div class="m2"><p>قطره زهر در او شربت جلاب شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حزمت ارحصن شود دافع تقدیر بود</p></div>
<div class="m2"><p>عزمت ارتیغ شود قاطع انساب شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کوه اگر از گره ابروی تو یاد کند</p></div>
<div class="m2"><p>آهن اندر دل او قطره سیماب شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرگ از انصاف تو همخوابه میشان گردد</p></div>
<div class="m2"><p>قصب از عدل تو پیرایه مهتاب شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جرم خوشخندد چو ن لطف تو عفو اندیشد</p></div>
<div class="m2"><p>مرک خون گرید چو نقهر تو در تاب شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر کجا پرتورای تو کند جلوه گری</p></div>
<div class="m2"><p>صبح روشنگر ازودر پس جلباب شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خرج یکروزه خوانت نه همانا که بود</p></div>
<div class="m2"><p>گر زمین زر شود و نامیه ضراب شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عقل اگر شرح دهد جزوی از اخلاق ترا</p></div>
<div class="m2"><p>صفحه نه ورق چرخ درین باب شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چونکه من دست ترا دریا خوانم زشرف</p></div>
<div class="m2"><p>قطره در حلق صدف لؤلؤ خوشاب شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خم این قوس قزح چیست برین روی سحاب</p></div>
<div class="m2"><p>گرنه از دست تو هر وقت بمحراب شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ اعدای ترا بهر چه فربه دراد</p></div>
<div class="m2"><p>بهر روزی که در او خشم تو قصاب شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرکه او قصد بجاه تو کند زودنه دیر</p></div>
<div class="m2"><p>کسوه حجر او جامه حجاب شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خصم از قلعه پیروزه حصار ار سازد</p></div>
<div class="m2"><p>قهر بارو فکنت معول و نقاب شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دم خلق تو اگر روی بصحرا آرد</p></div>
<div class="m2"><p>خار پشت از اثر لطفش سنجاب شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد قهر تو اگر بر جگر دهر وزد</p></div>
<div class="m2"><p>لعل دل کان قطره خوناب شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرکه در خدمت تو پشت نکردست کمان</p></div>
<div class="m2"><p>سرش از دوش چنان تیر بپرتاب شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش خطت چو شود خازن اسرار تو کلک</p></div>
<div class="m2"><p>مقله خواهد که ترا نایب بواب شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای بزرگی که مباهات کند تیر فلک</p></div>
<div class="m2"><p>گر ترا روزی از زمره نواب شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه صاحب هنران بنده این در گاهند</p></div>
<div class="m2"><p>چه شود بنده گر از جمله اصحاب شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخن من به ازین گردد در مدحت تو</p></div>
<div class="m2"><p>غوره گویند بتدریج که دوشاب شود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گوهر از لفظ تو دزدیم و فروشیم بتو</p></div>
<div class="m2"><p>کاب دریا بهمه حال بدریاب شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا ببستان زسحاب کرم و شبنم جود</p></div>
<div class="m2"><p>کشت امید کسی تازه و سیراب شود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خیمه دولت و جاه تو چنان ثابت باد</p></div>
<div class="m2"><p>کش ازل میخ عمود و ابد اطناب شود</p></div></div>