---
title: >-
    شمارهٔ  ۱۰۴ - قصیده
---
# شمارهٔ  ۱۰۴ - قصیده

<div class="b" id="bn1"><div class="m1"><p>ای بحق پادشاه روی زمین</p></div>
<div class="m2"><p>وی بتو تازه گشته دولت و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز آواز کوس نصرت تو</p></div>
<div class="m2"><p>مانده در گوش روزگار طنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سکه از فر تست با رونق</p></div>
<div class="m2"><p>منبر از نام تست با تمکین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد سپهری فراز پایه تخت</p></div>
<div class="m2"><p>صد جهانی میان خانه زین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخرا پیش حکمت ازمه نو</p></div>
<div class="m2"><p>حلقه در گوش چون ینال و تکین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جود تو ریش فقر را مرهم</p></div>
<div class="m2"><p>عدل تو دیو ظلمرا یاسین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطره عفو تست آب حیات</p></div>
<div class="m2"><p>شعله رای تست نور یقین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه بر سمت حکم تو گردد</p></div>
<div class="m2"><p>چنبر چرخ بگسلد در حین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خرج یکروزه جود تست هر آنچ</p></div>
<div class="m2"><p>کرد خورشید زیر خاک دفین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش چوکان حکم تو بر عقل</p></div>
<div class="m2"><p>مختصر می نمود گوی زمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مهره ظلم از تو ماند گشاد</p></div>
<div class="m2"><p>بیدق ملک ا زتو شد فرزین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرش عدل تو در بسیط جهان</p></div>
<div class="m2"><p>تخت قدرت بر اوج علیین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابلق دهر زیر فرمانت</p></div>
<div class="m2"><p>حلقه آسمانت زیر نگین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نزند صبح هیچ روز نفس</p></div>
<div class="m2"><p>که نه بر دشمنت کند نفرین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شمه خلق تست باد صبا</p></div>
<div class="m2"><p>اثر عدل تست فروردین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چه عجب گر ز عدل شامل تو</p></div>
<div class="m2"><p>از کبوتر حذر کند شاهین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خصم تو ناید از عدم بوجود</p></div>
<div class="m2"><p>که نه او را اجل بود بکمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تیر سندان سم تو بر دوزد</p></div>
<div class="m2"><p>در شب تیره دیده پروین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ملک از بهر فخر نام ترا</p></div>
<div class="m2"><p>کرده در وردهای خود تضمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تیغ تو سرفشان و فتنه نشان</p></div>
<div class="m2"><p>خصم تو پای بند و قلعه نشین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زلزله در فلک فتد ازبیم</p></div>
<div class="m2"><p>چون ز خشم اندر ابرو آری چین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بشکند هیبت تو پشت فلک</p></div>
<div class="m2"><p>بگسلد قوت تو کوه متین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دهن مادح تو آکنداست</p></div>
<div class="m2"><p>چون دهان صدف بدر ثمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>من چو نظم مدیحت اندیشم</p></div>
<div class="m2"><p>جبرئیلم همی کند تلقین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون من از جان دعای تو گویم</p></div>
<div class="m2"><p>آسمانم همی کند آمین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>عزم و قاد و حزم ثابت تو</p></div>
<div class="m2"><p>پست کردست حصن های حصین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اینچنین فتح و این چنین نصرت</p></div>
<div class="m2"><p>باز گویند در جهان پس ازین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مژده دولتست و معجز ملک</p></div>
<div class="m2"><p>ورنه هرگز که دید فتح چنین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا مکانرا بودحدوث و جهت</p></div>
<div class="m2"><p>تا زمانرا بود شهور وسنین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باد قدر تو برتر ازکیوان</p></div>
<div class="m2"><p>عمر افزونتر از الوف ومئین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا قیامت بهر چه رای کنی</p></div>
<div class="m2"><p>نصرت ایزدیت باد معین</p></div></div>