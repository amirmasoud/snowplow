---
title: >-
    شمارهٔ  ۱۳۵ - در وصف کاخ
---
# شمارهٔ  ۱۳۵ - در وصف کاخ

<div class="b" id="bn1"><div class="m1"><p>ای حریم حرمت یزدانی</p></div>
<div class="m2"><p>وی نهاد لطفت جسمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نکوئی دوم فردوسی</p></div>
<div class="m2"><p>وز بلندی شرف کیوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشتر از کارگه ار تنگی</p></div>
<div class="m2"><p>برتر از بارگه رضوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منزل معدلت و انصافی</p></div>
<div class="m2"><p>معدن مکرمت و احسانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسختیئی ز بهشت باقی</p></div>
<div class="m2"><p>داده تشریف سرای فانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلس عشرت را بنیادی</p></div>
<div class="m2"><p>کعبه بخشش را ارکانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سقف مرفوع ز تو سر گردان</p></div>
<div class="m2"><p>بیت معمور ز تو زندانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش خاک تو بروزی صد بار</p></div>
<div class="m2"><p>چرخ بر خاک نهد پیشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بتحقیق مقام محمود</p></div>
<div class="m2"><p>وی بانصاف بهشت ثانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فارغ از نایبه گردونی</p></div>
<div class="m2"><p>ایمن از حادثه دورانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نکوئی چون سپهری مطلق</p></div>
<div class="m2"><p>وز خوشی باغ ارم را مانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درنیاید ز درت محنت و غم</p></div>
<div class="m2"><p>ره نیابد بسویت ویرانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طیره از ساحت تو رنگ فلک</p></div>
<div class="m2"><p>خیره از طرز تو طبع مانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هست بر درگهت بهر شرف</p></div>
<div class="m2"><p>چرخ را آرزوی دربانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>داده ایام ترا منشوری</p></div>
<div class="m2"><p>بهمه نعمت جاویدانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هرگز این جای مبادا خالی</p></div>
<div class="m2"><p>از می و مطرب و از مهمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر تو آراسته ربع مسکون</p></div>
<div class="m2"><p>بتو افروخته آبادانی</p></div></div>