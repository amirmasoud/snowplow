---
title: >-
    شمارهٔ  ۸۲ - در تحمید باری و نعت پیغمبر
---
# شمارهٔ  ۸۲ - در تحمید باری و نعت پیغمبر

<div class="b" id="bn1"><div class="m1"><p>هر نفس کان نز پی یاد جلال ذوالجلال</p></div>
<div class="m2"><p>در جهان جان بر آری آن وبالست آن وبال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هان بتوحید خدای و نعت پیغمبر گرای</p></div>
<div class="m2"><p>تا شود کفارت آن ترهات خط و خال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قادری کز قدرتش خالی نباشد هیچ چیز</p></div>
<div class="m2"><p>عالمی کز علم او بیرون نباشد هیچ حال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خالق جسمست و جان و رازق انسست و جان</p></div>
<div class="m2"><p>مبدع عقلست و نفس و واهب جاهست و مال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذات او بی آفتست و قدرتش بی علتست</p></div>
<div class="m2"><p>صنع او بی آلتست و ملک او بی انتقال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حکم او بر اختیار و فعل او بی احتیاج</p></div>
<div class="m2"><p>منع او بر اتصال و بعد او بر انفصال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف دیمومیت او هست حی لاینام</p></div>
<div class="m2"><p>نعت وحدانیت او هست فرد لایزال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هست وهم تیز روز ادراک ذاتش مانده لنگ</p></div>
<div class="m2"><p>هست نفس ناطقه از وصف کنهش مانده لال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل کل خود کیست دهلیزی ز درگاه قدم</p></div>
<div class="m2"><p>نفس کل خود چیست ناموسی زدیوان جلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کمالش نیست تغییر و تناهی را جواز</p></div>
<div class="m2"><p>در کلامش نیست طغیان و تباهی را مجال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل اندر راه او دیده بچشم معرفت</p></div>
<div class="m2"><p>وهم را در منزل اول شکسته پر و بال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتاب قدرت او هست بر چرخ قدم</p></div>
<div class="m2"><p>فارغ از نقص کسوف و ایمن از ننگ زوال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کوه بهر طاعتش بسته میانست از کمر</p></div>
<div class="m2"><p>چرخ بهر امتثالش حلقه در گوش از هلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قطرۀ از نعت او دان یم آب فرات</p></div>
<div class="m2"><p>شمۀ از رحمت او خوان دم باد شمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صنعش از خاری برود آرد همی صدگونه گل</p></div>
<div class="m2"><p>لطفش از خارا برون آرد همی آب زلال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حکمتش آرد ز باد مهرگان زر درست</p></div>
<div class="m2"><p>قدرتش بارد ز ابر ماه دی سیم حلال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرده از یک قطره آب و خون بقدرت تعبیه</p></div>
<div class="m2"><p>لعل اندر جان سنگ و مشک در ناف غزال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تربیت زویافت اطفال نبات اندر نما</p></div>
<div class="m2"><p>ورنه نفس ناطقه هرگز نپروردی نهال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بی قضا و قدرتش والله که جمله عاجزند</p></div>
<div class="m2"><p>هم عناصر ز اختلاف وهم هیولی ز اعتدال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گوی گردونرا که سرگردان چو گانقضاست</p></div>
<div class="m2"><p>چون مدبر خوانی او را عقل کی داند حلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ماه و خورشیدی که آن صباغ و این طباخ تست</p></div>
<div class="m2"><p>گر مقدر دانی ایشانرا بود عین ضلال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نقش بی نقاش چون صورت نمی بندد بعقل</p></div>
<div class="m2"><p>کی پذیرد نظم بی صانع جهانرا اتصال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ذات او گر جوهر ستی یا عرض چون ذات ما</p></div>
<div class="m2"><p>همچو ما آفتت پذیرستی ز دور ماه و سال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست در راهش ز بهر امر و نهی شرع او</p></div>
<div class="m2"><p>علم از بهر عقیله عقلت از بهر عقال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زنگی و ترک و بدو نیک از قضایش زاده اند</p></div>
<div class="m2"><p>تا نگوئی این ز دیوست آندگر از ذوالجلال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس ز مرگ بعث خواهد بود تا در حضرتش</p></div>
<div class="m2"><p>از تو کلیات و جزئیات را باشد سؤال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شو پی قرآن و اخبار پیمبر گیر و رو</p></div>
<div class="m2"><p>تا برون آرد ترا از چند و چون و قیل و قال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عقل بهر معرفت دان شرع از بهر وجوب</p></div>
<div class="m2"><p>انبیا از بهر حجت نقل بهر امتثال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مقصد پیغمبران مقصود موجودات کل</p></div>
<div class="m2"><p>احمد مرسل که عالم یافت از قدرش کمال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آنکه ارکان طبایع یافت از خلقتش نظام</p></div>
<div class="m2"><p>وانکه اخلاق مکارم یافت از خلقش جمال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جرم را بر آبروی او حواله صد کرم</p></div>
<div class="m2"><p>فقر را از کان خوان او نواله صد نوال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قدر او اندوخته بی مایگان را اقتدار</p></div>
<div class="m2"><p>عدل او آمیخته نوروز ها را اعتدال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عقل کش تخت از دلست و قبه از کاخ دماغ</p></div>
<div class="m2"><p>ساخت منصب در سرای شرع اوصف نعال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بولهب در مکه زو اعراض میکرد و صهیب</p></div>
<div class="m2"><p>مانده از شوق رخش در روم بی آرام و حال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دین ز درویشان طلب نز خواجگان با شکوه</p></div>
<div class="m2"><p>زانکه گوهر از صدف یابی نه از ماهی وال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گاه جویان لطف طبع او انس را مهربان</p></div>
<div class="m2"><p>گاه کویان شوق جان او ارحنایابلال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چشم او با کحل مازاغ ایمنست از چشم زخم</p></div>
<div class="m2"><p>گوش او با سر او حی فارغست لز گوشمال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دشمن اولاد او هستند اولاد الزنا</p></div>
<div class="m2"><p>مبغض اصحاب او هستند اصحاب الشمال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>باد از یزدان درودی هم بقدر قدر او</p></div>
<div class="m2"><p>برروان اوی و بر یاران و بر اصحاب و آل</p></div></div>