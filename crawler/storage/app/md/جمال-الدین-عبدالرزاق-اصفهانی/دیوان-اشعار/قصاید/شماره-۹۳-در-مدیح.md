---
title: >-
    شمارهٔ  ۹۳ - در مدیح
---
# شمارهٔ  ۹۳ - در مدیح

<div class="b" id="bn1"><div class="m1"><p>منم که جز بمدیح تو هیچ دم نزنم</p></div>
<div class="m2"><p>بجز بقوت تو گوی مدح و دم نزنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرای ضرب سخن زان مسلمست مرا</p></div>
<div class="m2"><p>که جز بنام شهنشاه دین درم نزنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجاور فلک و بحرم اندرین حضرت</p></div>
<div class="m2"><p>سزاست خیمه اگر بر کنار یم نزنم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر آن نفس که زنم جز بیاد دولت تو</p></div>
<div class="m2"><p>وبال باشد بر عمر لاجرم نزنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روا بود که ز نعمت تهی بود دستی؟</p></div>
<div class="m2"><p>که جز بدامن چون تو ولی نعم نزنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه عذر سازم اگر بر جناب این دولت</p></div>
<div class="m2"><p>طناب خیمه برین طارم نهم نزنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز روزگار تو میگویم وز تو مخدوم</p></div>
<div class="m2"><p>ازان دمی که زنم بی هزار غم نزنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چو شیر علم گر زباد باید زیست</p></div>
<div class="m2"><p>چو طبل و بوق دم از حلق و از شکم نزنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنزد همت من آسمان کمینه گداست</p></div>
<div class="m2"><p>اگر چه پهلو با هیچ محتشم نزنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنر ز خویش نمایم چو تیغ و چون خورشید</p></div>
<div class="m2"><p>ز خویش فخر کنم لاف ازین و عم نزنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توکل آمیز الحق قناعتیست مرا</p></div>
<div class="m2"><p>که تکیه بر زر قارون و ملک جم نزنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بچشم و گوش و بدست و زبان امین باشم</p></div>
<div class="m2"><p>دغا نبازم و دم بیش متهم نزنم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز روی حرص و هوا پرده بر حرم ندرم</p></div>
<div class="m2"><p>بدست دزد طمع نقب بر حرم نزنم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگرچه عاجز باشم زبون کس نشوم</p></div>
<div class="m2"><p>اگرچه قادر گردم در ستم نزنم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بخوش حریفی هنگام خلوت مجلس</p></div>
<div class="m2"><p>ززهره کم نزنم گر چه زیروبم نزنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سپید بازم و هر دست را نشایم من</p></div>
<div class="m2"><p>سیاه چترم و بر هر سری رقم نزنم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بطبع شاعرم آری ولی بوقت بیان</p></div>
<div class="m2"><p>چو آفتابم و چون آفتابه نم نزنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گه فصاحت بادم زبان بریده چو کلک</p></div>
<div class="m2"><p>اگر عرب بسر کلک بر عجم نزنم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا بگاه دبیری در دست باد قلم</p></div>
<div class="m2"><p>اگر برابر این خواجگان قلم نزنم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>علوم شرعی معلوم هرکسست که من</p></div>
<div class="m2"><p>ز هیچ چیز درین شیوه کم قدم نزنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر بنظم رسد کار و شعر باید گفت</p></div>
<div class="m2"><p>تو خود بگو که من اینجای هیچ دم نزنم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حدیث فضل رها کن من این نمی گویم</p></div>
<div class="m2"><p>و گرچه میرسدم لاف فخر هم نزنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو لاله گر کلهی بر نهم رکاب ترا</p></div>
<div class="m2"><p>زبندگان تو شمشیر کم زنم نزنم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا بمرد مخوان گر بوقت جانبازی</p></div>
<div class="m2"><p>شراره وار معلق سوی عدم نزنم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بوقت مردی چو من تیغ کار باید بست</p></div>
<div class="m2"><p>بمردمی که زخورشید تیغ کم نزنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زیاد حمله چو من آبدار کشم</p></div>
<div class="m2"><p>زنم گر آتش در خاک روستم نزنم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرفتم آنکه مرا نیست معنی ذاتی</p></div>
<div class="m2"><p>هنر ندارم و تیغ و قلم بهم نزنم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تمام نیست مرا این هنر که بعدرکوع</p></div>
<div class="m2"><p>بجز بخدمت خاص تو پشت خم نزنم؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه آن خسم که زهر بادگوشۀ گیرم</p></div>
<div class="m2"><p>که بی رضای تو من گام در ارم نزنم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدین وسایل و چندین هنر چو تو مخدوم</p></div>
<div class="m2"><p>دریغ باشد اگر بر فلک علم نزنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حقوق خدمت دارم مرا مکن ضایع</p></div>
<div class="m2"><p>که من همایم و جز فال مغتنم نزنم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زدرگه تو بجای دگر نخواهم شد</p></div>
<div class="m2"><p>که پنج نوبت جز بر در کرم نزنم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز درگه تو بجای دگر نخواهم شد</p></div>
<div class="m2"><p>که پنج نوبت جز بر در کرم نزنم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دعا بشعر نگفتم که حلقه یارب</p></div>
<div class="m2"><p>بجز بوقت سحر بر در قدم نزنم</p></div></div>