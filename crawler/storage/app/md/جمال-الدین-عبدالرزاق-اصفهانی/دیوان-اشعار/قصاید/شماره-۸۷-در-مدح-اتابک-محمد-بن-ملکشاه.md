---
title: >-
    شمارهٔ  ۸۷ - در مدح اتابک محمد بن ملکشاه
---
# شمارهٔ  ۸۷ - در مدح اتابک محمد بن ملکشاه

<div class="b" id="bn1"><div class="m1"><p>بساط عدل بگسترد باز در عالم</p></div>
<div class="m2"><p>خدایگان جهان خسرو بنی آدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قوام چتر سلاطین سکندر ثانی</p></div>
<div class="m2"><p>پناه دولت سلجوق اتابک اعظم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطاب کرده بدو چرخ شهریار زمین</p></div>
<div class="m2"><p>لقب نهاده بر او عقل مرزبان عجم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شهی که تا در او قبله ملوک شدست</p></div>
<div class="m2"><p>چهار گوشه عالم زامن شد چو حرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنای ظلم ز شمشیر اوست مستأصل</p></div>
<div class="m2"><p>اساس عدل بانصاف اوست مستحکم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مکونات بنزدیک قدر او ناچیز</p></div>
<div class="m2"><p>محقرات بنزدیک حزم او معظم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بتیغ بستد ملک زمین ز دست ملوک</p></div>
<div class="m2"><p>بتازیانه ببخشید بر عبید و خدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همی ستاند جان و همی دهد روزی</p></div>
<div class="m2"><p>گهی بصفحه تیغ و گهی بنوک قلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غبار موکب او توتیای چشم ملوک</p></div>
<div class="m2"><p>دعای دولت او مومیای جان کرم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی سر یر تو ارکان عرش را مانند</p></div>
<div class="m2"><p>زهی ضمیر تو اسرار غیب را محرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>توئیکه ذکر تو زیبد طراز دوش ملوک</p></div>
<div class="m2"><p>توئیکه نام تو شاید نگار روی درم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کهینه بارگه تست گرد خیمه چرخ</p></div>
<div class="m2"><p>کمینه پرده گه تست ساحت عالم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بنور رای تو شد چشم سلطنت روشن</p></div>
<div class="m2"><p>زعکس تیغ تو شد آستین دین معلم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تراست زیر نگین طول و عرض رویزمین</p></div>
<div class="m2"><p>که هست بر عدوی تو چو حلقه خاتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فلک چو زنگی اگر پاسبان بام تو نیست</p></div>
<div class="m2"><p>چرا بطبلک خورشید و مه شود خرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو حکم کن ز تو فرمان و از فلک خدمت</p></div>
<div class="m2"><p>تو امرده ز تو در خواست و زستاره نعم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نه بی اجازت تو شام بر گشاید چتر</p></div>
<div class="m2"><p>نه بی اشارت تو صبح بر کشید علم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولایتیکه ز تشویش پیش ازین بودست</p></div>
<div class="m2"><p>بسان دوزخ نمرود، شد بهشت ارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کمینه تاختن تو ز گنجه تا بغداد</p></div>
<div class="m2"><p>ازان کناره دریا بدین سواحل یم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بسم اسب زمین را زهفت شش کرده</p></div>
<div class="m2"><p>پس آسمانرا گرده زی گرد آن هشتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو آفتاب تو بر نقره خنگ آخته تیغ</p></div>
<div class="m2"><p>چو کوه پیش و پس تو گرفته خیل و حشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو پاک کردی این عرصه از مخالف ملک</p></div>
<div class="m2"><p>چنانکه کرد علی پاک کعبه را ز صنم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه روی ملک خراشیده هیچ ناخن ظلم</p></div>
<div class="m2"><p>نه زلف عدل پژولیده هیچ دست ستم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو بیخ خصم بآهستگی ز بن بر کن</p></div>
<div class="m2"><p>که چشم عقل شتاب و ظفر ندید بهم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تبارک الله ازین بیکرانه لشگر تو</p></div>
<div class="m2"><p>که ضبط آن نکند شکل هندسی بقلم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هرآنگهی که بجنبید شیر رایت تو</p></div>
<div class="m2"><p>سروی گاو زمین را در آورید بخم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بهر زمین که در او لشگر تو حمله کند</p></div>
<div class="m2"><p>عظیم کاسد باشد در او متاع بقم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که داشت از ملکان لشگری چنین انبوه</p></div>
<div class="m2"><p>ز عهد سنجر بر گیر تا بدولت جم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در آنزمان که دلیران ز حرص گیر و بدار</p></div>
<div class="m2"><p>فرو گذارند اسباب زندگی مبهم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>غریو کوس برنجین و بانگ روئین نای</p></div>
<div class="m2"><p>بگوش گردون آواز زیر باشد و بم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سران نیارند از سر فرو نهادن ننگ</p></div>
<div class="m2"><p>یلان ندارند از جان فراسپردن غم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز خون شیران گلگونه کرده صفحه تیغ</p></div>
<div class="m2"><p>ز گرد گردان سرمه کشیده شیراجم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بس خروش در افتاده کوهها را لرز</p></div>
<div class="m2"><p>ز بس نهیب فرو رفته آسمانرا دم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بپیش تیغ اجل درامل فکنده سپر</p></div>
<div class="m2"><p>بپیش حمله فتنه بلا فشرده قدم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سوار و نیزه نماید میان صف نبرد</p></div>
<div class="m2"><p>بشکل شیری پیچان بدست درار قم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روان شیران چون زلف دلبران پرتاب</p></div>
<div class="m2"><p>دهان مردان چون چشم سفلگان بی نم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سر مبارز مالیده زیر پای فنا</p></div>
<div class="m2"><p>دل دلاور خائیده در دهان عدم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز کر و فر سواران و ضرب و طعن سپاه</p></div>
<div class="m2"><p>در او ز بیم بلرزد مفاصل رستم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بهوش رزم پژوهان اجل گشاده بغل</p></div>
<div class="m2"><p>ز مغز کینه وران مرگ باز کرده شکم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تو اندر آئی آنجا عنان گرفته ظفر</p></div>
<div class="m2"><p>چو ژنده پیل دمان و چو شرزه شیر دژم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بنعل اسب بپوشیده خاک هفت اقلیم</p></div>
<div class="m2"><p>ببانگ کوس بدریده سقف نه طارم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فکنده رایت سلطان قهر بر بیرق</p></div>
<div class="m2"><p>کشیده طره خاتون فتح بر پرچم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهر کجا که دو صف روی سوی رزم آرند</p></div>
<div class="m2"><p>بسنده باشد شمشیر تو بعدل حکم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جهان ندید و نبیندبعدل تو ملکی</p></div>
<div class="m2"><p>ز دور آدم تا عهد عیسی مریم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه تا عدد آسمان نگردد بیش</p></div>
<div class="m2"><p>همیشه تا مدد فیض خور نگردد کم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نثار بارگه شهریار عادل باد</p></div>
<div class="m2"><p>همه میامن نصرت ز بارگاه قدم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شده ممالک عالم برای تو مضبوط</p></div>
<div class="m2"><p>شده قواعد ملت بتیغ تو محکم</p></div></div>