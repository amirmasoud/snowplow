---
title: >-
    شمارهٔ  ۴۵ - در مدح معین الدین حسین هنگام ادای حج
---
# شمارهٔ  ۴۵ - در مدح معین الدین حسین هنگام ادای حج

<div class="b" id="bn1"><div class="m1"><p>شیرمردان چو عزم کار کنند</p></div>
<div class="m2"><p>کار ازین گونه استوار کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبخور زاتش سموم آرند</p></div>
<div class="m2"><p>خوابگه در دهان مار کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش تیر بلا سپر گردند</p></div>
<div class="m2"><p>نزد شیر اجل گذار کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای بر گردن مراد نهند</p></div>
<div class="m2"><p>پشت بر روی روزگار کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کم ناموس و سروری گیرند</p></div>
<div class="m2"><p>ترک آشوب و کاروبار کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرقت از اهل و از وطن جویند</p></div>
<div class="m2"><p>هجرت ازیار و از دیار کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس پشت افکنند منصب و جاه</p></div>
<div class="m2"><p>روی در روی اضطرارکنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کله خواجگی فروگیرند</p></div>
<div class="m2"><p>بندگی محض اختیار کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنک بردل نهند و بار کشند</p></div>
<div class="m2"><p>مهر برلب نهند و کار کنند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان شیرین نهند برکف دست</p></div>
<div class="m2"><p>بس حدیث دیار و یار کنند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کاخ و کاشانه راکنند وداع</p></div>
<div class="m2"><p>در دل بادیه قرا ر کنند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سهم ان راههای مردم خوار</p></div>
<div class="m2"><p>پیش چشم امید خوار کنند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جان و جاه و سر و زر اندازند</p></div>
<div class="m2"><p>خرج آن راه از ین چهار کنند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هرکجا عشق لایزال آمد</p></div>
<div class="m2"><p>سر وزر را چه اعتبار کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دیده از هر چه هست بردوزند</p></div>
<div class="m2"><p>تا چنین دولتی شکار کنند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تکیه گه بر حضیض کوه زنند</p></div>
<div class="m2"><p>پای گه در شکاف غار کنند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بالش پر زسنگ خاره نهند</p></div>
<div class="m2"><p>بستر گل زنوک خارکنند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گله از عندلیب و از طوطی</p></div>
<div class="m2"><p>باشتر مرغ و سوسمار کنند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر امید وصال خانه دوست</p></div>
<div class="m2"><p>شربت زهر خوشگوار کنند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از بزرگان عجب نباشد اگر</p></div>
<div class="m2"><p>کار های بزرگوار کنند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مقبلانرا چو وحی باشد رای</p></div>
<div class="m2"><p>همه اندیشه استوار کنند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس برهنه شوند چون شمشیر</p></div>
<div class="m2"><p>تا که بانفس کار زار کنند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جان روحانیان قدسی را</p></div>
<div class="m2"><p>وقت لبیک شرمسار کنند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چیست رمی الجمار نزد خرد</p></div>
<div class="m2"><p>نفس اماره سنگسار کنند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون بموقف رسند از پس شوط</p></div>
<div class="m2"><p>سنگ آن راه اشکبار کنند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دستهای نیاز وقت دعا</p></div>
<div class="m2"><p>راست چون پنجه چنار کنند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون عروس حرم کند جلوه</p></div>
<div class="m2"><p>جان شیرین براو نثار کنند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سقف مرفوع و خانه معمور</p></div>
<div class="m2"><p>همه سکنی در آن جوار کنند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وز پی بوس خال رخسارش</p></div>
<div class="m2"><p>سر فشانند و جانسپار کنند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خواجه لالای حجره بینی</p></div>
<div class="m2"><p>که بدیدارش افتخار کنند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شب و مشک و سواد دیده زدل</p></div>
<div class="m2"><p>کسوت او همی شعار کنند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خلفا جامه و سلاطین چتر</p></div>
<div class="m2"><p>همه زان رنگ مستعار کنند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حبشی صورتی که سلطانان</p></div>
<div class="m2"><p>دست بوسش هزار بارکنند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن سبه جامه میر حاجب بار</p></div>
<div class="m2"><p>کش امیر سرای بار کنند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سیر نادیده هیچ چهره وصل</p></div>
<div class="m2"><p>مرکب هجر راهوار کنند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>لاجرم از برای محملشان</p></div>
<div class="m2"><p>ناقه الله بر قطار کنند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ملاء عرش از سرادق حفظ</p></div>
<div class="m2"><p>گردشان خندق و حصار کنند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مرتبتشان ز چرخ بگذارند</p></div>
<div class="m2"><p>خواجگیشان یکی هزار کنند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر غباری که در فضای هوا</p></div>
<div class="m2"><p>گر پیاده و گر سوار کنند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روشنان فلک برای شرف</p></div>
<div class="m2"><p>کحل اغبر ازان غبار کنند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>حور خلخال ناقه حجاج</p></div>
<div class="m2"><p>بهر تشریف گوشوار کنند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زانسپس سالکان راه خدای</p></div>
<div class="m2"><p>چون سوی قبله پاقرار کنند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اولا نام صاعد مسعود</p></div>
<div class="m2"><p>حرز بازوی روزگار کنند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شرح اخلاق او حدی سازند</p></div>
<div class="m2"><p>حاج چون بر شتر مهار کنند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ساکنان صوامع ملکوت</p></div>
<div class="m2"><p>بر دعای وی اقتصار کنند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ذکر باقیش بر صحیفه روز</p></div>
<div class="m2"><p>بسیاهی شب نگار کنند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ا ی بساکز برای این تشریف</p></div>
<div class="m2"><p>کعبه و روضه انتظار کنند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا همی کعبه را بهر دو جهان</p></div>
<div class="m2"><p>مأمن هر گناهکار کنند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>حرم خواجه باد کعبه خلق</p></div>
<div class="m2"><p>تاکش از کعبه یادگار کنند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سال عمرش چنانکه هندسه زان</p></div>
<div class="m2"><p>قاصر آید اگر شمار کنند</p></div></div>