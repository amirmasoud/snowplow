---
title: >-
    شمارهٔ  ۹۷ - قطعه
---
# شمارهٔ  ۹۷ - قطعه

<div class="b" id="bn1"><div class="m1"><p>من ز جمع شاعران باری کیم</p></div>
<div class="m2"><p>من ز لاف دانش و دعوی کیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز نثر پاک چون نثره چه ام</p></div>
<div class="m2"><p>من ز نظم شعر چون شعری کیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان گویم که شعر من چنین</p></div>
<div class="m2"><p>من ازین دعوی بیمعنی کیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویم از من زنده شد شخص سخن</p></div>
<div class="m2"><p>من نه نفخ صور و نه عیسی کیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ید بیضا نمایم در هنر؟</p></div>
<div class="m2"><p>ای مسلمانان من از موسی کیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طعنه ها در شعر استادان زنم</p></div>
<div class="m2"><p>من بدین دانش ز استهزی کیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چیست این باد و بروت خواجگی</p></div>
<div class="m2"><p>سیم دارم فاضلم آری کیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من فراز توده غبرا چه ام</p></div>
<div class="m2"><p>من بزیر قبه اعلی کیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من زمدت یا زخدمت چیستم</p></div>
<div class="m2"><p>من ز رسم و خلعت اجری کیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من ندانم تا من عامی صفت</p></div>
<div class="m2"><p>ز اختراع و صنعت انشی کیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من ز شرح گوهر ثانی چه ام</p></div>
<div class="m2"><p>من ز سر علت اولی کیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میتوان دانست قدر زرگری</p></div>
<div class="m2"><p>این تکبر چیست پس یعنی کیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گر ز بهر بندگی نپذیردم</p></div>
<div class="m2"><p>رکن دین من در همه گیتی کیم</p></div></div>