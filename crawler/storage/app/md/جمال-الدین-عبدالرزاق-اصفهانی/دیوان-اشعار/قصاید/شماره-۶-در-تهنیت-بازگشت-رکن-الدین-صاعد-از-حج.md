---
title: >-
    شمارهٔ  ۶ - در تهنیت بازگشت رکن الدین صاعد از حج
---
# شمارهٔ  ۶ - در تهنیت بازگشت رکن الدین صاعد از حج

<div class="b" id="bn1"><div class="m1"><p>ای زده لبیک شوق از غایت صدق و صفا</p></div>
<div class="m2"><p>بسته احرام وفا در عالم خوف ورجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زنقص نقض فارغ حکم تو گاه نفاذ</p></div>
<div class="m2"><p>وی زننک فسخ ایمن عزم تو وقت مضا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای چو ابراهیم آزر کرده فرزندی فدی</p></div>
<div class="m2"><p>وی چو ابراهیم ادهم کرده ملکی رارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مسلم منصب میمونت از آسیب غدر</p></div>
<div class="m2"><p>وی منزه جامه احرامت از گرد ریا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هاتف والله یدعو برزبان شوق حق</p></div>
<div class="m2"><p>گفته اندر گوش هوشت هان چه میپائی هلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق بیت الله ترا از خویشتن اندر ربود</p></div>
<div class="m2"><p>همچو عشق شمع کز خود میبرد پروانه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطف یزدانت بمحمل رخت از مسند نهاد</p></div>
<div class="m2"><p>رخت گل از شاخ در مهد افکند دست صبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کرا توفیق ربانی گریبان گیر شد</p></div>
<div class="m2"><p>دامنش هرگز نگیرد ملکت دارالفنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>توزراه خواجگی بر خاستی از بندگی</p></div>
<div class="m2"><p>لاجرم کشتی برآز وخشم و شهوت پادشا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده گردون کجا دیده است شخصی مثل تو</p></div>
<div class="m2"><p>در جوانی باورع در پادشاهی پارسا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فرخ آنساعت که بربستی کمر در راه دین</p></div>
<div class="m2"><p>سائقت توفیق ایزد قائدت عون خدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نعل سم مرکبت گوش فلک را گوشوار</p></div>
<div class="m2"><p>خاک خیل موکبت چشم ملک راتوتیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هم سیاه از پشت پای همتت گوی زمین</p></div>
<div class="m2"><p>هم کبود از پشت دست رفعتت روی سما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>غایت توفیق این باشد که در شغل و فراغ</p></div>
<div class="m2"><p>هرگز اندر کار خیر از تو کسی نشنیده لا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چون همه هم تو مقصور است در دین پروری</p></div>
<div class="m2"><p>لاجرم دایم بود همراه عزم تو قضا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون رکاب اشرف تو کرد قصد بادیه</p></div>
<div class="m2"><p>رب سلم گوی گشت آندم روان انبیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد تماشا گاه از اقبال وصدق نیتت</p></div>
<div class="m2"><p>ره که چونان صعب و هایل مینمود از ابتدا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از تو اندر بادیه دیدند دریای روان</p></div>
<div class="m2"><p>کاندر وغوطه خورد عقل ارکندر ای شنا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ابر رحمت گشت باران ریزبر فرق تو زانک</p></div>
<div class="m2"><p>هر کجا باشد محیای توکم نبودحیا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایمن آبادی شد اندر عهد توآنره چنانک</p></div>
<div class="m2"><p>سال رکن الدین کنون تاریخ باشد سالها</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرکجا عدل جهان آرای توسایه فکند</p></div>
<div class="m2"><p>کاه برک ایمن بود آنجا ز جذب کهربا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دور نبود گر زفرت زاید از آتش نبات</p></div>
<div class="m2"><p>طرفه نبود گرزیمنت روید از آهن گیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو مجرد گشته از جمله علایق مردوار</p></div>
<div class="m2"><p>اندران موقفکه هر کس میشد از جامه جدا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>صوفیانه گفته ترک دوخته و اندوخته</p></div>
<div class="m2"><p>گشته از جامه برهنه همچو تیغ اندروغا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چند تشریف قبا تو مردم چشمی ترا</p></div>
<div class="m2"><p>خلعت بی جامگی بهتر بود ازصد قبا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آسمانی در علو از جامه خواهی کرد بس</p></div>
<div class="m2"><p>آسمان را از مجره خوبتر باشد ردا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گرد رخسار تو بفزود آبروی تو از انک</p></div>
<div class="m2"><p>گوهر شمشیر بر شمشیر بفزاید بها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جبرئیل از وجد لبیک تو در رقص آمده</p></div>
<div class="m2"><p>گفته هردم هکذا یا بالمعالی هکذا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>باداگر بردی زموقف سوی کعبه بوی تو</p></div>
<div class="m2"><p>کعبه استقبال کردی مقدمت را تامنا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاید اربطحای کعبه بارگیر زر شود</p></div>
<div class="m2"><p>تا براو کردست دست زرفشانت کیمیا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حکمت ایزدتعالی در ازل چون حکم کرد</p></div>
<div class="m2"><p>از ادای این فریضه این همیکرد اقتضا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا تو هستی برخلایق در شریعت پیشرو</p></div>
<div class="m2"><p>هم تو باشی در مناسک بر خلایق پیشوا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا حرم را یمن فر تو بیفزاید شرف</p></div>
<div class="m2"><p>تاعرب را جود دست تو بیاموزد سخا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا شود از سعی مشکور تو گاه سعی تو</p></div>
<div class="m2"><p>پر مروت از تو مروه پرصفا از تو صفا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کمترین سعی طوافت گرد این هفت آسمان</p></div>
<div class="m2"><p>کمترین رمی جمارت جرم این هفت آسیا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گاه قربان تو این معنی حمل باثور گفت</p></div>
<div class="m2"><p>کاش این منصب مرا گشتی مسلم یاترا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون قدم در خانه کعبه نهادی آنزمان</p></div>
<div class="m2"><p>میشنیدند از در و دیوار کعبه مرحبا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از وجودت رکن کعبه پنجگشت ازبسشرف</p></div>
<div class="m2"><p>وز کف تو چشمه زمزم دو گشت از بس عطا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کعبه خود داند که جز تو هیچ حاکم در عمل</p></div>
<div class="m2"><p>گرد او هرگز نگشت الا در ایام بلا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دولت الحق اینچنین فرمان بجا آوردنست</p></div>
<div class="m2"><p>ورنه روزی چند خود هرکس بود فرمانروا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چون زکعبه روی زی روضه نهادی کش خرام</p></div>
<div class="m2"><p>کعبه گوید هردمت کایخوشتر از جان تا کجا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بسته کعبه بردل از دوری تو سنگ سیاه</p></div>
<div class="m2"><p>منتظر تا کی بود باز اتفاق التقا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>وان بنائی را که بد دینان همی افراشتند</p></div>
<div class="m2"><p>هم باقبال تو شد باخاک هوارآن بنا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کی شود با محدثی انصاف تو همداستان</p></div>
<div class="m2"><p>کی دهد بر بدعتی عدل تو در عالم رضا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هراساسی کش قواعد نیست برشرح رسول</p></div>
<div class="m2"><p>اندر ایام تو آنرا کمترک باشد بقا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اندر آنحالت که در روضه فرستادی سلام</p></div>
<div class="m2"><p>سرخ رخ بودی بحمدالله بنزد مصطفی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>هم زکلکت شرع او اندر حریم احترام</p></div>
<div class="m2"><p>هم ز عدلت ملت او باردای کبریا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>منت ایزدرا که رفتی و امدی آسوده دل</p></div>
<div class="m2"><p>گشته امیدت وفا و بوده حاجاتت روا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گشته هریک خطوه تو صد خطا را عذر خواه</p></div>
<div class="m2"><p>ور چه معصوم است ذات پاکت از جرم وخطا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مصطفی خواهشگرت هرجا که میبردی نماز</p></div>
<div class="m2"><p>جبرئیل آمین گرت هرجا که میکردی دعا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>وز پی آن تا کنی آنجا گذر باردگر</p></div>
<div class="m2"><p>کعبه دل دارد کز اوچو نروضه جانداردنوا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تاازل را ز ابتدا هرگز کسی ندهد نشان</p></div>
<div class="m2"><p>راست چونان کزابد نتوان نهادن انتها</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باد چون حکم ازل حکم تو ازروی نفاذ</p></div>
<div class="m2"><p>مدت عمر تو بادا چون ابد بی منتها</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بر تو میمون وز تو مقبول بادا از خدای</p></div>
<div class="m2"><p>این فریضت را که برقانون سنت شدادا</p></div></div>