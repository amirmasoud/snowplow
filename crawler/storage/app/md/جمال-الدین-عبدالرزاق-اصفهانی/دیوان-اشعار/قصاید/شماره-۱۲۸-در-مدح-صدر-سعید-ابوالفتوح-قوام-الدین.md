---
title: >-
    شمارهٔ  ۱۲۸ - در مدح صدر سعید ابوالفتوح قوام الدین
---
# شمارهٔ  ۱۲۸ - در مدح صدر سعید ابوالفتوح قوام الدین

<div class="b" id="bn1"><div class="m1"><p>ای جهان از تو معطر گشته</p></div>
<div class="m2"><p>وی فلک از تو منور گشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای نکو طلعت فرخنده تو</p></div>
<div class="m2"><p>زینت مسند و منبر گشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وز پی مقدم میمون تو صدر</p></div>
<div class="m2"><p>آسمان پر زر و زیور گشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرع را علم تو رونق داده</p></div>
<div class="m2"><p>عقل را رای تو رهبر گشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مسند از درس تو ناظر بوده</p></div>
<div class="m2"><p>منبر از وعظ تو جانور گشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشها از تو بهنگام نکت</p></div>
<div class="m2"><p>چون صدف پر در و گوهر گشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با زمین حلم تو همسنگ شده</p></div>
<div class="m2"><p>با فلک قدر تو همبر گشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از پی خوش سخنت گاه سخن</p></div>
<div class="m2"><p>دل بدخواه تو مجمر گشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وز پی خدمت تو پشت فلک</p></div>
<div class="m2"><p>خم پذیرفته و چنبر گشته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حکم و فرمان تو از روی نفاذ</p></div>
<div class="m2"><p>با قضا راست برابر گشته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست تو جود مجسم بوده</p></div>
<div class="m2"><p>شخص تو لطف مصور گشته</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لفظت آسایش دلها داده</p></div>
<div class="m2"><p>مدحت آرایش دفتر گشته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ابلق سرکش ایام بطبع</p></div>
<div class="m2"><p>زیر ران تو مسخر گشته</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بودی ار مهر چو رایت بودی</p></div>
<div class="m2"><p>همه ذرات هوا زر گشته</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طمع و آز ز جودت فربه</p></div>
<div class="m2"><p>کان و دریا ز تو لاغر گشته</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لفظ تو همنفس صبح شده</p></div>
<div class="m2"><p>خلق تو همدم عنبر گشته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دست راد تو چو ابری بمثل</p></div>
<div class="m2"><p>لفظ عذب تو چو کوثر گشته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از کفت ابر خجل گشته چنان</p></div>
<div class="m2"><p>که زمین از عرقش تر گشته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گاه ترکیب سخن در مدحت</p></div>
<div class="m2"><p>دهن نی همه شکر گشته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا که این زورق ازرق باشد</p></div>
<div class="m2"><p>سقف این توده اغبر گشته</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بادت ایام مطیع و منقاد</p></div>
<div class="m2"><p>فلکت بنده و چاکر گشته</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش چوگان بلاخصم چو گوی</p></div>
<div class="m2"><p>بی سر و پای شده سرگشته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>همچنین بادی در دولت و عز</p></div>
<div class="m2"><p>قوت پشت برادر گشته</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر زمانی بمکان هر دو</p></div>
<div class="m2"><p>دل اسلام قوی تر گشته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همه مقصود محصل بوده</p></div>
<div class="m2"><p>همه اغراض میسر گشته</p></div></div>