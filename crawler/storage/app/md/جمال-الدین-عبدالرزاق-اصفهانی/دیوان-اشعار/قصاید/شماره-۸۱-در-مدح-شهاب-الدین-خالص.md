---
title: >-
    شمارهٔ  ۸۱ - در مدح شهاب الدین خالص
---
# شمارهٔ  ۸۱ - در مدح شهاب الدین خالص

<div class="b" id="bn1"><div class="m1"><p>زهی ز فر تو سر سبز چرخ مینارنگ</p></div>
<div class="m2"><p>ز مقدم تو سپاهان گرفته صد اورنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلاصه همه عالم شهاب دین خالص</p></div>
<div class="m2"><p>که مثل او ننماید سپهر آینه رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک ز قدر تو اندخته بسی رفعت</p></div>
<div class="m2"><p>خرد زرای تو آموخته بسی فرهنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوی مدارج مدح تو بال خاطر سست</p></div>
<div class="m2"><p>سوی مصاعد قدر تو پای فکرت لنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلند قدر تو خواندست اوج گردون پست</p></div>
<div class="m2"><p>فراخ جود تو کردست کار کانها تنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آفتاب کمال تو مهر یک ذره</p></div>
<div class="m2"><p>ز منجنیق نکال تو چرخ یک خرسنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سمند بختت و سیر فلک فراخ عنان</p></div>
<div class="m2"><p>کمند عزمت و دست قضا دراز آهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود بدست تو اندر حسام جان آهنج</p></div>
<div class="m2"><p>بدانصفت که بود در میان بحر نهنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمین بوقت درنگ و زمانه گاه شتاب</p></div>
<div class="m2"><p>زعزم و حزم تو آموخته شتاب و درنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بیم لرزه در اجزای کوهها افتد</p></div>
<div class="m2"><p>چو بر نهی تو بشاخ گوزن تیر خدنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز سهم تیغ تو بدخواه تو ز قید حیاة</p></div>
<div class="m2"><p>گر یختست از انسوی مرگ صد فرسنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از انکه خاست چوتو تیغزن ز آل حبش</p></div>
<div class="m2"><p>ز تیغ هندی ترکان همی برآمد زنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گه شکار چنان خون چکانی از دل شیر</p></div>
<div class="m2"><p>که روی چرخ منقط شود چو پشت پلنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نسیم خلقت اگر بگذرد بچین نه عجب</p></div>
<div class="m2"><p>که جان پذیر شود دردیار چین سترنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جز از برای وجودت که عالم معنی است</p></div>
<div class="m2"><p>قلم صحیفه ابداع را نزد بیرنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نفاد امر تو انگیخته فلک را سیر</p></div>
<div class="m2"><p>شکوه حلم تو آموخته زمین را سنگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خدای داند کز غیبت رکاب شریف</p></div>
<div class="m2"><p>همی نمود همه نوش عیش ما چو شرنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>میان صبر و دل از آرزوی توصد میل</p></div>
<div class="m2"><p>میان دیده و خواب از فراق تو صد جنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز ضعف همچون کلک و زسوز همچون شمع</p></div>
<div class="m2"><p>ز گریه همچو صراحی ز ناله همچون چنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو نقطه بی تو همه طول و عرض کرده رها</p></div>
<div class="m2"><p>چو غنچه بی تو بخود در گریخته دلتنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز انتظار چو نرگس نهاده چشم براه</p></div>
<div class="m2"><p>ز شوق همچو ترازو نهاده بر دل سنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز خواب دیده همیداشت بی خیال تو شرم</p></div>
<div class="m2"><p>دهان ز خنده همیداشت بی جمال تو ننگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هزار منت و شکر خدای عز و جل</p></div>
<div class="m2"><p>که باز کردست اقبال سوی ما آهنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بدولت تو ازین پس بچرخ دون با ما</p></div>
<div class="m2"><p>نه نیش یازد عقرب نه کج رود خرچنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تا که پدید آید از مدار فلک</p></div>
<div class="m2"><p>گهی طلایه روم و گهی طلایه زنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمینه پایه جاه تو باد نه گردون</p></div>
<div class="m2"><p>کهینه چاکر قدر تو باد صد هوشنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دل عدوی تو از جور آسمان مجروح</p></div>
<div class="m2"><p>نه آنچنان که شود ملتئم بمرد اسنگ</p></div></div>