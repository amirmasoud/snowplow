---
title: >-
    شمارهٔ  ۴۲ - قصیده
---
# شمارهٔ  ۴۲ - قصیده

<div class="b" id="bn1"><div class="m1"><p>همه میامن این روزگار میمون باد</p></div>
<div class="m2"><p>همه سعدت این حضرت همایون باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآسمان معالی و اوج برج شرف</p></div>
<div class="m2"><p>قران مشتری و آفتاب میمون باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نثار گردون بر فرق رفعتت نرسد</p></div>
<div class="m2"><p>نثار گردون هم درخور چنو دون باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طواف چرخ بگرد سرای میمونت</p></div>
<div class="m2"><p>چو گرد خیمه لیلی طواف مجنون باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صدای صیت تو مساح قطر گردون گشت</p></div>
<div class="m2"><p>نفاذ امر تو سیاح ربع مسکون باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوامر تو سر تازیانه قدرست</p></div>
<div class="m2"><p>نواهی تو دوال رکاب گردون باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>معانی تو برون از تو هم چندست</p></div>
<div class="m2"><p>معالی تو فزون از تصرف چون باد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیم خلق تو مشموم مشک اذفر شد</p></div>
<div class="m2"><p>لعاب کلک تو جلباب در مکنون باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان ز کلک تو و خاتمت چو بدرقه یافت</p></div>
<div class="m2"><p>ز درد حادثه تا نفخ صور مأمون باد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر برای تو نبود مسیر هفت انجم</p></div>
<div class="m2"><p>نه آشکوب فلک پست همچو هامون باد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عدو اگر زر نابست و گوهر ناسفت</p></div>
<div class="m2"><p>چو زر و گوهر مضروب باد و مطعون باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بروزگار تو اندر که موسم عدلست</p></div>
<div class="m2"><p>وشاق تیغ بحبس نیام مسجون باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز یمن دولت بیدار و حسن تدبیرت</p></div>
<div class="m2"><p>دماغ فتنه پر از شاخهای افیون باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گه ترشح جود تو هر سر انگشتی</p></div>
<div class="m2"><p>زهاب دجله و نیل و فرات و جیهون باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وظیفه های ملک بر دعات مقصورست</p></div>
<div class="m2"><p>سفینه های فلک از ثنات مشحون باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر آن نفس که ز تو روزگار برباید</p></div>
<div class="m2"><p>بطول عمری بردور چرخ مضمون باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سوار ابلق چرخ ارنه در حمایت تست</p></div>
<div class="m2"><p>ز لشگر حدثان بر سرش شبیخون باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عدوت را چو دویتت قصب بناخن در</p></div>
<div class="m2"><p>چو لیقه ات درو دیوارهاش اکسون باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز خوان زندگی و از نواله روزی</p></div>
<div class="m2"><p>نصیب دشمن جاه تو طشت و صابون باد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گه مباحثه زاسرار علم خاطر تو</p></div>
<div class="m2"><p>دلیل حجت معقول و علم مظنون باد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حسودت ار بمثل هست جمله گنج روان</p></div>
<div class="m2"><p>زبهر عصمت در زیر خاک مدفون باد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زقرعه های شرائین خصم دولت تو</p></div>
<div class="m2"><p>همیشه صفحه شمشیر مرگ پر خون باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز شیشه های تهی فلک بدهن غرور</p></div>
<div class="m2"><p>سر عدو که بریده بهست مدهون باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چوریش احمق او دستمال موسی شد</p></div>
<div class="m2"><p>سر مطوق او پایمال قارون باد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گه مدیح تو در جلوه معانی بکر</p></div>
<div class="m2"><p>ضمیر من تتق صد هزار خاتون باد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمینه لفظ چو مریم هزار عیسی زای</p></div>
<div class="m2"><p>کمینه حرف چونون ظرف چند ذوالنون باد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو طبع تو همه وزنش لطیف باد و سلیس</p></div>
<div class="m2"><p>چو شکل تو همه الفاظهاش موزون باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>فزون ازین نبود جاه می ندانم گفت</p></div>
<div class="m2"><p>که منصب و شرفت زانچه هست افزون باد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا که قدر از بهر خیل وجود</p></div>
<div class="m2"><p>گه توالد پیوند کاف با نون باد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بقا و مدت عمر تو در علو مکان</p></div>
<div class="m2"><p>زضبط عقل وزحد شمار بیرون باد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همت بدست سخا اندرون ید بیضا</p></div>
<div class="m2"><p>همت بسر سبزی روی بخت گلگون باد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا زفرت امانی بخیر محصولست</p></div>
<div class="m2"><p>ترا زچرخ مقاصد بنجح مقرون باد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بدین دعا که بگفتم عقیب هر بیتی</p></div>
<div class="m2"><p>زسدره روح امین گفته یارب ایدون باد</p></div></div>