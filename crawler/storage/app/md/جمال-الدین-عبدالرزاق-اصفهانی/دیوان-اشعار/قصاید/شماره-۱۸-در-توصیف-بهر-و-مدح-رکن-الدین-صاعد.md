---
title: >-
    شمارهٔ  ۱۸ - در توصیف بهر و مدح رکن الدین صاعد
---
# شمارهٔ  ۱۸ - در توصیف بهر و مدح رکن الدین صاعد

<div class="b" id="bn1"><div class="m1"><p>تا صبا در نقشبندی خامه در عنبر زدست</p></div>
<div class="m2"><p>صد هزاران لعبت از جیب زمین سر بر زدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه منجوق گل اینک کرد از گلبن طلوع</p></div>
<div class="m2"><p>شاه چیر لاله اینک نوبتی بردر ز دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک چون طوطی خوش جامه سراندر سر نهاد</p></div>
<div class="m2"><p>شاخ چون طاوس فردوسی پر اندرپرزدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم نرگس نیم خوابست و دهانش پرززر</p></div>
<div class="m2"><p>دوش پنداری بنام گل همه شب زرزدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از شکوفه شاخ گوئی دست عطار صبا</p></div>
<div class="m2"><p>کله کافور بر اطراف عود ترزدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاشبانگاهی اطراف کواکب کلک شب</p></div>
<div class="m2"><p>صد هزاران کوکبه بر سقف نیلوفرزدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد دم باد سحر گاهی زخوشبوئی چنانک</p></div>
<div class="m2"><p>کس نمیداند که این دم مشک یا عنبرزدست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بگشاد است پیش سرو آزاده چنار</p></div>
<div class="m2"><p>پنجه در خواهد فکندن تا که با او برزدست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طره شاخ بنفشه بس پشولیده نمود</p></div>
<div class="m2"><p>دوش بالاله مگر در بوستان ساغر زدست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بلبل از شوق رخ گل جامه برخود چاکزد</p></div>
<div class="m2"><p>گل بغنچه در تتق برسینه و تن برزدست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بید پنداری بقصد دشمنان صدر شرق</p></div>
<div class="m2"><p>دست نصرت بهر دین برقبضه خنجر زدست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صدر عالم رکن دین اقضی القضاة شرق و غرب</p></div>
<div class="m2"><p>آنکه تو عدل عمر درد انش حیدر زدست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>انکه تو تاپشت بهردین ببالش باز داد</p></div>
<div class="m2"><p>دشمن او پهلوی تیمار بربستر زدست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عدل او آواز در اقطار شرق و غرب داد</p></div>
<div class="m2"><p>فضل اوآواز دراقصای بحر وبرزدست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خلق و خلعش مایه بخش ماه و خورشید آمدند</p></div>
<div class="m2"><p>کلک و طقش کاروان عسکرو شوشتر زدست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صدر او پایه ورای عالم بالا نهاد</p></div>
<div class="m2"><p>قدر او خیمه فراز گنبد اخضر زدست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روز درسش دین شرف زانگوشه مسند گرفت</p></div>
<div class="m2"><p>روزوعظش جان علم برذروه منبرزدست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرع پیغمبر بجاه او ممکن میشود</p></div>
<div class="m2"><p>نزگزاف این تکیه او برجای پیغبر زدست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نارسیده باهمه مردان بمیدان آمداست</p></div>
<div class="m2"><p>نو رسیده با همه شیران بعالم برزدست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لفظ پاکش ز انخط شبرنگ پنداری درست</p></div>
<div class="m2"><p>نقد روشن بر محک تیره شب از اختر زدست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مسرع عزمش سبق از جنبش از گردون ببرد</p></div>
<div class="m2"><p>باره حزمش مثل از سداسکندر زدست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آیت انصاف او خورشید برجبهت نبشت</p></div>
<div class="m2"><p>رایت اقبال او جبریل بر شهپر زدست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>لطف او را بین که چون در چشم خاک انداخت آب</p></div>
<div class="m2"><p>خشم اورا بین که چون در جان آب آذر زدست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دست رادش صد هزاران رخنه در طوبی فکند</p></div>
<div class="m2"><p>لفظ عذبش صد هزاران طعنه بر کوثر زدست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کلک تیر و تیغ مریخ از چه دارد چرخ انک</p></div>
<div class="m2"><p>دشمنش را تیغ برگردن قلم بر سر زدست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پیش لفظ درفشان و کلک گوهر بار او</p></div>
<div class="m2"><p>گک ازان گرددصدف کولاف ازگوهر زدست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نکته کان از زبان کلک او بیرون جهد</p></div>
<div class="m2"><p>عقل بهر حفط را بر گوشه دفتر زدست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شعله دان از شعاع رای دهر آرای او</p></div>
<div class="m2"><p>پرتو نوری که چشم چشمه انورزدست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شادباش ای حاکمی کز عدل تو در عهد تو</p></div>
<div class="m2"><p>روبه لنگ آستین برروی شیر نرزدست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ذکر عدلت چارحد عالم سفلی گرفت</p></div>
<div class="m2"><p>صیت فضلت پنج نوبت در همه کشور زدست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>افتتاح آن دعای صاعد مسعود دان</p></div>
<div class="m2"><p>بامدادان هر نفس کان صبح روشنگر زدست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نصرت دین حنیفی کن که از کل جهان</p></div>
<div class="m2"><p>دست در فتراک اقبال تو دین پرور زدست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دشمنانت را اجل نزدیک شد اقبال دور</p></div>
<div class="m2"><p>زقه روح القدس این فال بر چاکر زدست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>حاسدت از بندگی تو کسی شد گر شدست</p></div>
<div class="m2"><p>شاخ وبیخ اندر حریم دولتت زد گر زدست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گو مشو مغرور اگر چرخش زبهر جاه تو</p></div>
<div class="m2"><p>گوشمالی دیرتر یا سیلیی کمتر زدست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از برای نسخت این مدح گوئی آسمان</p></div>
<div class="m2"><p>این ورقها رابخط استوا مسطر زدست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر یکی بیت از مدیح تو که در نظم آمدست</p></div>
<div class="m2"><p>زهره زهرا هزاران بار بر مزمرزدست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا شبانگاهان زاجرام کواکب کلک شب</p></div>
<div class="m2"><p>صد هزاران کوکبه برسقف نیلوفر زدست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مسند شرع از شکوه طلعتت خالی مباد</p></div>
<div class="m2"><p>کاسمان پشت از برای خدمتت چنبر زدست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مدت عمر توصد چندانکه گوئی دور چرخ</p></div>
<div class="m2"><p>ثابت وسیار را در ضعف یکدیگر زدست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تنگ باداروزی خصمت بدانسانکه مگر</p></div>
<div class="m2"><p>رزق اوراقفل از دست قضا بر در زدست</p></div></div>