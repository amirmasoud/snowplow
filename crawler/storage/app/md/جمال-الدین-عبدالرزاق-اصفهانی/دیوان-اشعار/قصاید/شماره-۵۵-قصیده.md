---
title: >-
    شمارهٔ  ۵۵ - قصیده
---
# شمارهٔ  ۵۵ - قصیده

<div class="b" id="bn1"><div class="m1"><p>ای کرده تو بر ملک تکبر</p></div>
<div class="m2"><p>وی کرده فلک بتو تفاخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای پایه منصب رفیعت</p></div>
<div class="m2"><p>بر تر ز تصرف تفکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از قدر تو چرخ در ترفع</p></div>
<div class="m2"><p>وز رای تو عقل در تحیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حزم تو مسلم از تهاون</p></div>
<div class="m2"><p>عزم تو منزه از تغیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شرح خصایل حمیدت</p></div>
<div class="m2"><p>مستغرق مد سبعه ابحر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مصعد همتت فلک را</p></div>
<div class="m2"><p>از عجز فتاده سنگ در سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از جود تو گشته تهی دست</p></div>
<div class="m2"><p>وز صیت تو گوش آسمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بشنید صدف ز ماهی گنگ</p></div>
<div class="m2"><p>در مدح تو لفظهای چون در</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد نامیه طفل حجر تو صدر</p></div>
<div class="m2"><p>شد ناطقه مدح گوی تو حر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زان ناطقه میکند تحدث</p></div>
<div class="m2"><p>زان نامیه می کند تکبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با جود تو کیست کان که ندهد</p></div>
<div class="m2"><p>یک قطره لعل بی تزجر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>انعام تو خاص و عام را هست</p></div>
<div class="m2"><p>چون فیض خدای بر تواتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حق از سر کلک شب نگارت</p></div>
<div class="m2"><p>چون صبح همی کند تظاهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زانصاف تو آهوی سبکدل</p></div>
<div class="m2"><p>با شیر همی کند تنمر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردون ز شرف همی نماید</p></div>
<div class="m2"><p>بر فور بخدمتت تو فر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با دست و دل تو کان و دریا</p></div>
<div class="m2"><p>دعوی نکنند از تکاثر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد عدل تو دشمن تظلم</p></div>
<div class="m2"><p>شد عفو تو عاشق تعثر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم لطف تو چون هواروا نبخش</p></div>
<div class="m2"><p>هم قهر تو چون اجل گلوبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای با همه کس ترا تفضل</p></div>
<div class="m2"><p>وی درهمه فن ترا تبحر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>محکوم تو شد سپهر فاحکم</p></div>
<div class="m2"><p>مأمور تو شد زمانه فامر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با عدل تو ظلم گشت منصف</p></div>
<div class="m2"><p>در عهد تو جود گشت لمتر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با خشم تو نیست ذوق جان حلو</p></div>
<div class="m2"><p>از لفظ تو نیست حرف حق مر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در عهد تو تیغ میکشد مهر؟</p></div>
<div class="m2"><p>این باشد غایت تهور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بارأی تو خنده میزند صبح؟</p></div>
<div class="m2"><p>اینست نهایت تمسخر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در صف نعال روز بارت</p></div>
<div class="m2"><p>جویند صدور دین تصدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از صدمت خشمت او فتادست</p></div>
<div class="m2"><p>در سینه آسمان تکسر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کردست زبهر مرکبت چرخ</p></div>
<div class="m2"><p>از جاده کهکشان شب آخور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندیشه مدح تو بخاطر</p></div>
<div class="m2"><p>بهتر ز هزار من بلادر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هردم که زنم نه در محدیحت</p></div>
<div class="m2"><p>زان دم زدنم بود تحیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جاوید بکام زی که مارا</p></div>
<div class="m2"><p>هم بر ز مدیح تست و هم بر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در بندگیت مرا چه باقیست</p></div>
<div class="m2"><p>جز حلقه گوش و نام سنقر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مدح تو و التقات غیری</p></div>
<div class="m2"><p>هرگز نکنم من این تجاسر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا هست حواس را تصرف</p></div>
<div class="m2"><p>تا هست خیال را تصور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بادات بقای عمر چندان</p></div>
<div class="m2"><p>کاندر عددش بود تعذر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روزت همه عید و از پی عید</p></div>
<div class="m2"><p>بد خواه ترا کغن بگازر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اعدای ترا قبول چونان</p></div>
<div class="m2"><p>کامروز بود قبول اشتر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در منصب جاه تو قدم</p></div>
<div class="m2"><p>در مدت عمر تو تأخر</p></div></div>