---
title: >-
    شمارهٔ  ۵۲ - در مدح طغرل شاه سلجوقی
---
# شمارهٔ  ۵۲ - در مدح طغرل شاه سلجوقی

<div class="b" id="bn1"><div class="m1"><p>عشقت سوی هر که راه بر گیرد</p></div>
<div class="m2"><p>اول غم تو گواه برگیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر عکس رخت برآسمان افتد</p></div>
<div class="m2"><p>مه وای فضیحتاه بر گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنمای خود آن چه زنخدان را</p></div>
<div class="m2"><p>تا یوسف راه چاه برگیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگشای چو گل قبای زنگاری</p></div>
<div class="m2"><p>تا لاله ز سر کلاه بر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشاطه تست چرخ ازآن هر روز</p></div>
<div class="m2"><p>آیینه خود پکاه بر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترسد که از آه عاشقان تو</p></div>
<div class="m2"><p>آیینه چرخ آه برگیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر رنجه شود شبی خیال تو</p></div>
<div class="m2"><p>سوی رهی تو راه بر گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رنجی ز تن ضعیف بردارد</p></div>
<div class="m2"><p>دردی ز دل تباه بر گیرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لیکن نتواند از سرشک من</p></div>
<div class="m2"><p>الا که ره شناه بر گیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل چون زغمت تسلی جوید</p></div>
<div class="m2"><p>اندیشه مدح شاه بر گیرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سلطان زمین شه زمان طغرل</p></div>
<div class="m2"><p>کش گردون بارگاه بر گیرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در موکب او فلک تفاخر را</p></div>
<div class="m2"><p>چتری ز شب سیاه بر گیرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فرمان وی ا ربحواهد از گردون</p></div>
<div class="m2"><p>این جنبش عمر کاه برگیرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عدلش پی آن رود که در عالم</p></div>
<div class="m2"><p>رسم بد داد خواه بر گیرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای آنکه سخایت ارتفاع کان</p></div>
<div class="m2"><p>هر روز هزار راه بر گیرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گردون کبودکی دلش آید</p></div>
<div class="m2"><p>کش مثل تو پادشاه برگیرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>این دلق کبود چیست بگذارش</p></div>
<div class="m2"><p>تا خادم خانقاه بر گیرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مبداء بسلام تو کند خورشید</p></div>
<div class="m2"><p>پهلو چو ز خوابگاه بر گیرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر رأی تو بر فلک زند شعله</p></div>
<div class="m2"><p>مه زحمت خود زراه برگیرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ور خود غلط افتد آفتاب ازوی</p></div>
<div class="m2"><p>خود پرده اشتباه بر گیرد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیاره ز بهر توتیای چشم</p></div>
<div class="m2"><p>خاک درت از جباه بر گیرد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جود تو همه سؤال بر تابد</p></div>
<div class="m2"><p>عفو تو همه گناه بر گیرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خشم تو کمر زکوه بگشاید</p></div>
<div class="m2"><p>رأی تو کلف زماه بر گیرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>حلم تو بقوت ثبات خویش</p></div>
<div class="m2"><p>گردون و ومأسواه برگیرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>والله که اگر حساب جود تست</p></div>
<div class="m2"><p>لا از سر لااله بر گیرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گر عدل تو بر ستم زند بانگی</p></div>
<div class="m2"><p>بیچاره چگونه کاه بر گیرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طرفه نبود اگر بعدل تو</p></div>
<div class="m2"><p>آتش رمش از گیاه بر گیرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر کس که بجاه تو بد اندیشد</p></div>
<div class="m2"><p>دل زود زمال و جاه بر گیرد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با حمله تو عدو کم از کاهست</p></div>
<div class="m2"><p>ور کوه ز جایگاه بر گیرد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در رزم اگرش بجان امان دادی</p></div>
<div class="m2"><p>زان باید کانتباه بر گیرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شطرنجی اگر چه چربدست آمد</p></div>
<div class="m2"><p>عادت نبود که شاه بر گیرد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا چرخ مشعبد اندرین حقه</p></div>
<div class="m2"><p>گه بنهد مهره گاه بر گیرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بادات جهان بکام تا حظی</p></div>
<div class="m2"><p>زین دولت و تاج و گاه بر گیرد</p></div></div>