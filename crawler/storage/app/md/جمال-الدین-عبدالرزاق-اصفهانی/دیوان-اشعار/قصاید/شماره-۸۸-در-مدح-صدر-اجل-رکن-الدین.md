---
title: >-
    شمارهٔ  ۸۸ - در مدح صدر اجل رکن الدین
---
# شمارهٔ  ۸۸ - در مدح صدر اجل رکن الدین

<div class="b" id="bn1"><div class="m1"><p>زهی دهان تو میم و زلعل حلقه میم</p></div>
<div class="m2"><p>زهی دو زلف تو جیم و زمشک نقطه جیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مهر میم تو وز جور جیم تو هستم</p></div>
<div class="m2"><p>فراخ حوصله چون جیم و تنگدل چون میم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو جیم یکدله ام با تو پس چرا با من</p></div>
<div class="m2"><p>زمیم بسته دهانی همی کنی تعلیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدآتش رخ تو بر دو زلف تو بستان</p></div>
<div class="m2"><p>مگر که زلف و رخت آتشست و ابراهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر شد دل مسکین بدام عشق چو دید</p></div>
<div class="m2"><p>بزیر دانه نار آن دو رسته در یتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زده است افعی زلفت دل مرا و هنوز</p></div>
<div class="m2"><p>امید وصل همیدارد اینت قلب سلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیه گلیمی من شد زعارض تو پدید</p></div>
<div class="m2"><p>زند ازین پس حسن تو طبل زیر گلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازان ستیزد بیگانه وار با من یار</p></div>
<div class="m2"><p>که میگریزد سیماب وار از من سیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چوباد او همه تن جان چو شمع من گریان</p></div>
<div class="m2"><p>بیک سلام ازو جان بدو کنم تسلیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ززخم و زردی رویم چو روی اسطرلاب</p></div>
<div class="m2"><p>زجوی خون همه خطش جدول تقویم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخن چه رانم چندین دراز گشت حدیث</p></div>
<div class="m2"><p>جفا کشیدن عشاق عادتی است قدیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تراست جان، ببر و گرد دل مگرد از آن</p></div>
<div class="m2"><p>که هست مدحت صدر جهان در او تصمیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کریم مطلق و حرز زمانه رکن الدین</p></div>
<div class="m2"><p>که شاید ارز کرم خوانیش رئوف و رحیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخای وافر داده لقبش بحر محیط</p></div>
<div class="m2"><p>حیای مفرط نامش نهاده لطف جسیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عبارتی بود از لفظ او دعای مسیح</p></div>
<div class="m2"><p>اشارتی بود از کلک او عصای کلیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بپیش قطره جودش کم از بخار بحار</p></div>
<div class="m2"><p>بنزد شعله خشمش کم از شرار جحیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقدمست چو شرع رسول در تفضیل</p></div>
<div class="m2"><p>مسلمست چو نام خدای در تقدیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شهاب هست نسیمی زرای روشن او</p></div>
<div class="m2"><p>کز آسمان شرف دور کرد دیو رجیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن خزانه حکمت بدو سپرد خدای</p></div>
<div class="m2"><p>که هست در سنن شرع و دین حفیظ و علیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بزیر هر سخن او هزار گونه نکت</p></div>
<div class="m2"><p>بزیر هر نعم او هزار گونه نعیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خجل همی شود از جود دست او طوبی</p></div>
<div class="m2"><p>عرق همیکند از شرم لفظ او تسنیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تبارک الله از ان رمزو نکتهای سخن</p></div>
<div class="m2"><p>که عقل مدرک عاجز بماند از تفهیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهی ز قدر تو سر زیر همچو آب آتش</p></div>
<div class="m2"><p>زهی ز حلم تو سرکش چو باد خاک حلیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو نام فضل و سخا زنده کردۀ ورنی</p></div>
<div class="m2"><p>سخا و فضل بیکباره گشته بود عدیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زحکمت ار بکشد پای چرخ دست قضا</p></div>
<div class="m2"><p>بتیغ صبح کند چرخ را میان بدو نیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز باد قهر تو کشته شود چراغ حیات</p></div>
<div class="m2"><p>بآب لفظ تو زنده شود عظام رمیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز رای روشن تو گیرد ارتفاع فلک</p></div>
<div class="m2"><p>چنانکه گیرند از آفتاب در تنجیم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر مجسم بودی جلال تو بمثل</p></div>
<div class="m2"><p>نگنجدی بمکان و زمان دراز تعظیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عطای سایل واجب شناختی چو نان</p></div>
<div class="m2"><p>که باز می نشناسند سایلت ز غریم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بهار عدل تو است آن هوای خوش که در او</p></div>
<div class="m2"><p>نه لاله است سیه دل نه نرگسست سقیم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فلک چو مرکب قدرت بزین کند سازد</p></div>
<div class="m2"><p>زپوست خصم تو فتراکرا دوال ادیم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بزرگوارا صدرا ترا توانم گفت</p></div>
<div class="m2"><p>شکایت از بد چرخ خسیس و دهر لئیم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو کعبه فضلائی و خلق عالم را</p></div>
<div class="m2"><p>جناب عالی تو از بد زمانه حریم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همیخورم ز جفای زمانه زخم درشت</p></div>
<div class="m2"><p>همی کشم زعنای زمانه رنج عظیم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنات فکرم هستند یکجهان همه بکر</p></div>
<div class="m2"><p>نکرده خطبه ایشان سخای هیچ کریم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چه سود نکته بکرم چو شد کرم عنین</p></div>
<div class="m2"><p>چه سود نطفه فکرم چو جود گشت عقیم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روا بود که بنازم بدینقصیده که هست</p></div>
<div class="m2"><p>بدیعتر ز بهار و لطیف تر ز نسیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سبک چو روح خفیف و سلس چو طبع لطیف</p></div>
<div class="m2"><p>روان چو ماءمعین و قوی چو رای حکیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بفردولت مدحت چنان شود سخنم</p></div>
<div class="m2"><p>که چون صدای سخایت رسد بهفت اقلیم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه تا نبود چون عقیق سنگ سیاه</p></div>
<div class="m2"><p>همیشه تا نبود چون رحیق ماءحمیم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بآب حیوان بادات مشتری ساقی</p></div>
<div class="m2"><p>بفر باقی بادات دور چرخ ندیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>میان جام نکو خواه تو شراب طهور</p></div>
<div class="m2"><p>درون جان بداندیش تو عذاب الیم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شب عدوی تو بادا همی دراز و سیاه</p></div>
<div class="m2"><p>که دم در او نزند صبح تیغ زن از بیم</p></div></div>