---
title: >-
    شمارهٔ  ۴۷ - کتب الیه رشید الدین الوطواط
---
# شمارهٔ  ۴۷ - کتب الیه رشید الدین الوطواط

<div class="b" id="bn1"><div class="m1"><p>چون روی تو ماه سمانباشد</p></div>
<div class="m2"><p>چون زلطف تو مشک ختانباشد</p></div></div>
<div class="b2" id="bn2"><p>فاجابه رحمه الله</p></div>
<div class="b" id="bn3"><div class="m1"><p>چون دلبر من بیوفا نباشد</p></div>
<div class="m2"><p>کش هیچ غم کار ما نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر دل او جز ستم نیاید</p></div>
<div class="m2"><p>وندر سر او جز جفا نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>د رسایه آن آفتاب رخشان</p></div>
<div class="m2"><p>یک ذره دلم بی هوا نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنرا که زدست افعی دوزلفش</p></div>
<div class="m2"><p>بهتر زلبش مومیا نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک بوسه بجانی قرار دادیم</p></div>
<div class="m2"><p>آه ار لب اورا رضا نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتم که چرا دل همی بری گفت</p></div>
<div class="m2"><p>در مذهب عاشق چرا نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ایدل تو همه برگ عیش سازی</p></div>
<div class="m2"><p>زان کار تو جز بینوا نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان میدهی آنرا من ندانم</p></div>
<div class="m2"><p>کو باشد یار تو یا نباشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عاشق شده تن فرا بلا ده</p></div>
<div class="m2"><p>کاین عشق بتان بی بلا نباشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایدوست روا داری اینکه هرگز</p></div>
<div class="m2"><p>یک حاجتم از تو روا نباشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صد وعده که دادی یکی وفا کن</p></div>
<div class="m2"><p>یا با رخ نیکو وفا نباشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر زلف تو رشک آیدم از انروی</p></div>
<div class="m2"><p>کز روی تو یکدم جدا نباشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواهی که نباشدبشهر فتنه</p></div>
<div class="m2"><p>آن روی نهان دار تا نباشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خنده مزن ایجان زگریه من</p></div>
<div class="m2"><p>کاین لایق آنخوش لقا نباشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ا زخنده شنیدی که دل بمیرد</p></div>
<div class="m2"><p>زانست که گل را بقا نباشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیگانه مشو چون دلست و جانست</p></div>
<div class="m2"><p>بیگانه چنین آشنا نباشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یک بوسه بده خونبهایم آْخر</p></div>
<div class="m2"><p>هر چند مرا خونبها نباشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جز جور مکن گر کنی که انصاف</p></div>
<div class="m2"><p>در سنت خوبان روا نباشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفتی که مخورغم که من ترایم</p></div>
<div class="m2"><p>این دولت دانم مرا نباشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روی تو تمنای مقبلانست</p></div>
<div class="m2"><p>شایسته چون من گدا نباشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وصل تو چو شعر رشید دینست</p></div>
<div class="m2"><p>کو محرم هر ناسزا نباشد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رادی که بجز بهر خدمت او</p></div>
<div class="m2"><p>گردون خمیده دوتا نباشد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بحری که نکوتر زگفته او</p></div>
<div class="m2"><p>الا سخن انبیا نباشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون خط شریفش نگار نبود</p></div>
<div class="m2"><p>چون طبع لطیفش هوا نباشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون رای وی از روشنی و رفعت</p></div>
<div class="m2"><p>خورشید بوسط السما نیاشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زان داد سخن میدهد که در شعر</p></div>
<div class="m2"><p>قادر تر ازو پادشا نباشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وقتی که کند عقل حل اشکال</p></div>
<div class="m2"><p>جز رای ویش مقتدا نباشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای آنکه ببالای مدحت تو</p></div>
<div class="m2"><p>از کسوه دانش قبا نباشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چون لفظ تو آب حیات نبود</p></div>
<div class="m2"><p>چون خلق تو باد صبا نباشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون تو یدبیضا نمائی از کلک</p></div>
<div class="m2"><p>ناموس کلیم و عصا نباشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از روی شرف جز زخاکپایت</p></div>
<div class="m2"><p>در چشم خرد توتیا نباشد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بی سایه کلک تو نیست علمی</p></div>
<div class="m2"><p>چون گنج که بی اژدها نباشد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر گویم بحری محال نبود</p></div>
<div class="m2"><p>ور گویم ابری خطا نباشد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کاین چون تو بوقت سخن نبارد</p></div>
<div class="m2"><p>وان چون تو بگاه سخا نباشد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جز شعر تو خواندن حلال نبود</p></div>
<div class="m2"><p>جز مدح تو گفتن روا نباشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نزدیک من آن یک قصیده تو</p></div>
<div class="m2"><p>ملکی است که آنرا فنا نباشد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جاوید بماناد صیت فضلت</p></div>
<div class="m2"><p>دردست من الادعا نباشد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آری بعا اقتصار باید</p></div>
<div class="m2"><p>چون قوت مدح و ثنا نباشد</p></div></div>