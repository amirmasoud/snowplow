---
title: >-
    شمارهٔ  ۱۱۶ - چیستان بنام شمشیر و تخلص بمدح ملک عز الدین
---
# شمارهٔ  ۱۱۶ - چیستان بنام شمشیر و تخلص بمدح ملک عز الدین

<div class="b" id="bn1"><div class="m1"><p>چیست آن آخته آینه گون</p></div>
<div class="m2"><p>نه صدف لیک بگوهر مشحون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوده در تنگ یکی سنک نهان</p></div>
<div class="m2"><p>مانده در حبس یکی جنس نگون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تندیش را اثر خاطر تیز</p></div>
<div class="m2"><p>نرمیش را صفت طبع زبون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتشی گشته مرکب با آب</p></div>
<div class="m2"><p>لاجوردی که بلولو مقرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشن و پاک چو دست موسی</p></div>
<div class="m2"><p>بزر و سیم چو گنج قارون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقشها یافته بی خامه و رنگ</p></div>
<div class="m2"><p>همه درهم شده چون بوقلمون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نظر گوهر و رنگش بمثل</p></div>
<div class="m2"><p>چون ستاره است بر اوج گردون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سیاوش و خلیل از پاکی</p></div>
<div class="m2"><p>سرخ روی آمده زاتش بیرون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی پر اشک و دلش پر آتش</p></div>
<div class="m2"><p>همچو اندر غم لیلی مجنون</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آتشی بوالعجب آمد گهرش</p></div>
<div class="m2"><p>که شود تیزیش از آب فزون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>وین عجب تر که چو آبش دادی</p></div>
<div class="m2"><p>تشنه تر باشد آنگاه بخون</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پوست باز آورد آنگه که شود</p></div>
<div class="m2"><p>بدل خصم چو اندیشه درون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برق کردار همی بدرفشد</p></div>
<div class="m2"><p>زابر دستی که فزون از جیحون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فخر میران جهان عز الدین</p></div>
<div class="m2"><p>که کهین چاکر او افریدون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هنرش را نتوانگفت که چند</p></div>
<div class="m2"><p>خردش را نتوان گفت که چون</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدمتش را متحرک شده اند</p></div>
<div class="m2"><p>ساکنان همه ربع مسکون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باد عزمست بوقت حرکت</p></div>
<div class="m2"><p>کوه حزمست بهنگام سکون</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرخ چون خیمه جاهش آمد</p></div>
<div class="m2"><p>فارغ آمد ز طناب و زستون</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عاجز از خاطر او بطلیموس</p></div>
<div class="m2"><p>قاصر از نکته او افلاطون</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای کرم بر دل پاکت عاشق</p></div>
<div class="m2"><p>وی سخا بر کف رادت مفتون</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه کار تو چو طبع تو لطیف</p></div>
<div class="m2"><p>همه لفظ تو چو شکلت موزون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فلک پیر بصد دور ندید</p></div>
<div class="m2"><p>یک فنی همچو تو در کل فنون</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حکم و فرمان تو از روی نفاذ</p></div>
<div class="m2"><p>مددی یافته از کن فیکون</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیش رایت فلک اعلی پست</p></div>
<div class="m2"><p>نزد قدرت شرف گردون دون</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خرج یکروزه تو نیست هر آنچ</p></div>
<div class="m2"><p>کرد خورشید بعمری مدفون</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طالع تست سپهر مسعود</p></div>
<div class="m2"><p>طلعت تست همای میمون</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا بر اشجار بنالند طیور</p></div>
<div class="m2"><p>تا از اشجار ببالند غصون</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از فلک کام تو بادا موصول</p></div>
<div class="m2"><p>با ابد عمر تو بادا مقرون</p></div></div>