---
title: >-
    شمارهٔ  ۱۳۲ - در وصف بهار و مدح نظام الدین بوالعلای صاعد برادر رکن الدین
---
# شمارهٔ  ۱۳۲ - در وصف بهار و مدح نظام الدین بوالعلای صاعد برادر رکن الدین

<div class="b" id="bn1"><div class="m1"><p>اینک نوبهار آورد بیرون لشگری</p></div>
<div class="m2"><p>هر یکی چو ن نو عروسی درد گرگونزیوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تماشا میکنی برخیز کاندر باغ هست</p></div>
<div class="m2"><p>باد چون مشاطه و باغ چون لعبت گری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هر آنجانب که روی آری ز بس نقش بدیع</p></div>
<div class="m2"><p>جبرئیل آنجا بگستر دست گوئی شهپری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لعبتان باغ پنداری ز فردوس آمدند</p></div>
<div class="m2"><p>هر یکی در سر کشیده از شکوفه چادری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آسمان بر فرق نرگس دوخت شش ترکی کلاه</p></div>
<div class="m2"><p>بوستان در پای سوسن ریخت هر سیم و زری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پر طوطی گشت گوئی جامه هر غنچه</p></div>
<div class="m2"><p>چشم شاهین گشت گوئی دیده هر عبهری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عرض لشگر میدهد نوروز و ابرش عارضست</p></div>
<div class="m2"><p>وز گل و نرگس مراو را چون ستاره لشگری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد اندر آب میپوشد بهر دم جوشنی</p></div>
<div class="m2"><p>خاک از آتش مینهد بر فرق لاله مغفری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچه پیدا میکند زان آب داده بیلکی</p></div>
<div class="m2"><p>بید بیرون میکشد زین گندناگون خنجری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هست هر شاخی بزیبائی کنون چون طوطیی</p></div>
<div class="m2"><p>هست هر حوضی بنیکوئی کنون چو نکوثری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لاله و نرگس نگر در باغ سرمست آمده</p></div>
<div class="m2"><p>بر سر این افسری و بر کف آن ساغری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر هوا چون معتدل گردد زعدلش بشکفد</p></div>
<div class="m2"><p>غنچه ها کز بوی او گردد معطر کشوری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زاعتدال عدل سلطان شریعت بشکفد</p></div>
<div class="m2"><p>غنچه کز یکدمش گردد فلک چون مجمری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خواجه عالم نظام دولت و دین بوالعلا</p></div>
<div class="m2"><p>آنکه فرزندی چنو هرگز نزاید مادری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه در صدر سیادت نیست چون او خواجه</p></div>
<div class="m2"><p>وانکه بر چرخ سعادت نیست چون او اختری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه از شاخ کرم ناید چنو نو باوه</p></div>
<div class="m2"><p>وانکه در باغ شرف چون او نروید نوبری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه تا خورشید قدرش تافت از او جشرف</p></div>
<div class="m2"><p>مثل او نامد زکان آفرینش گوهری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زهره طبعی مشتری فری عطارد خامه</p></div>
<div class="m2"><p>مهر تأثیری و کیوان رفعتی مه منظری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قرة العین شریعت میوه جان خرد</p></div>
<div class="m2"><p>مردم چشم سلاطین خواجه هر سروری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه خرد از زاد و چرخ اعظم او را بنده</p></div>
<div class="m2"><p>وانکه طفل از سال و عقل پیر او را چاکری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست خوشبو تر ز خلقش دین و دولت را گلی</p></div>
<div class="m2"><p>نیست شیرینتر ز لفظش عقل و جانرا شکری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چشم ملت روشنست از وی اگر چه هست خرد</p></div>
<div class="m2"><p>لعبت چشم ار بود خرد آن نباشد منکری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خرد مشناس ار بچشم ستاره کوچکست</p></div>
<div class="m2"><p>زانکه هست او در نهاد خود بزرگی رهبری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کلکرا منگر بخردی آنزبان دانیش بین</p></div>
<div class="m2"><p>کز زبان دانی به آید مردم از هر جانوری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اوست همچون نقطه و عقلست خط مستقیم</p></div>
<div class="m2"><p>خط ز نقطه حاصل آید لاشک از هر مسطری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اوست همچو نمر کز و چرخست همچو ندایره</p></div>
<div class="m2"><p>لابد از مرکز پذیرد دایره هر چنبری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شد معنبر گوی گردون از نسیم خلق او</p></div>
<div class="m2"><p>زانکه خوش دم ترازو ناید ز دریا عنبری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسمان گر رسم نوروزی فرستد در خورش</p></div>
<div class="m2"><p>از هلالش طوق باید زافتابش افسری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای تو اندر مهد چون عیسی سخنگوی آمده</p></div>
<div class="m2"><p>وی تو در طفلی چو موسی خصم زن دین پروری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از کمال منصب تست آفرینشها تمام</p></div>
<div class="m2"><p>ورنه هستی آفرینش بی تو همچون ابتری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چشم روشن کرد گردون از وجودت ورنه چرخ</p></div>
<div class="m2"><p>بود ازینیک چشمه خورشید همچون اعوری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون وقار و علم و عقل و فر تو پیدا شود</p></div>
<div class="m2"><p>مشتری از شرم سازد طیلسان را معجری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>باش تا بر خط حکمت سر نهد هر گردنی</p></div>
<div class="m2"><p>باش تا سرمه کشد از خاک پایت هر سری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باش تا تو کلک گیری وانگهی فتوی دهی</p></div>
<div class="m2"><p>تا شود کلکت میان حق و باطل داوری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>باش تاطوطی نطق تو شکر خائی کند</p></div>
<div class="m2"><p>تا کند روح القدس از شاخ سدره منبری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا هلالت بدر گردد بدر گردد نور پاش</p></div>
<div class="m2"><p>تا نهالت سبز گردد سبز سایه گستری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهر دفع چشم بد هر یک دو ماهی آفتاب</p></div>
<div class="m2"><p>مر عطارد را بسوزد چون سپندی ز اخگری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زیبد از چرخ کبودت حلقه چوگان نیل</p></div>
<div class="m2"><p>زانکه نامد ز آفتابی مثل تو نیلوفری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا چو من باشند ابر و باد دایم در دو فصل</p></div>
<div class="m2"><p>در ربیع این نقشبندی در خزان آن زرگری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با دی اندر سایه خورشید عالم رکن دین</p></div>
<div class="m2"><p>ساخته در مدح هر دو بنده هر دم دفتری</p></div></div>