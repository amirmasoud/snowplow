---
title: >-
    ابیاتی چند از قصیده که تمام آن یافت نشد
---
# ابیاتی چند از قصیده که تمام آن یافت نشد

<div class="b" id="bn1"><div class="m1"><p>بلند همت و بسیار دان و اندک سال</p></div>
<div class="m2"><p>جهان گشای و ممالک ستان و دنیا دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پلنگ خاصیت و شیر زور و پیل افکن</p></div>
<div class="m2"><p>همای سایه و طوطی حدیث و باز شکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درشت ماشطه و نرم گوی و سخت کمان</p></div>
<div class="m2"><p>گران عطا و سبک حمله و لطیف آثار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زجود تست امل را هزار دلگرمی</p></div>
<div class="m2"><p>بعفو تست گنه را هزار استظهار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخانه های کمان تو پی برد فکرت</p></div>
<div class="m2"><p>چو مرگ نقب زند بر خزینه اعمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کند زمرد تیغت بحلقهای زره</p></div>
<div class="m2"><p>چنانکه عکس زمرد بچشم افعی مار</p></div></div>