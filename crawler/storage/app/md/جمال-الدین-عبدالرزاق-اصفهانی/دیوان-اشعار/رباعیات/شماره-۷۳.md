---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>یک بوسه زلعل خویش کم گیر و ببخش</p></div>
<div class="m2"><p>زنهار روا مدار تقصیر و ببخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان پیش کشیده ام نه از بهر بها</p></div>
<div class="m2"><p>این هدیه آن عطاست بپذیر و ببخش</p></div></div>