---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>جانا نم دیده وتف سینه نگر</p></div>
<div class="m2"><p>این عشق تو و فراق دیرینه نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر یوسف و یعقوب ندیدی بعیان</p></div>
<div class="m2"><p>در من نگر ایجان و در آیینه نگر</p></div></div>