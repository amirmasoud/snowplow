---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>در ساخته بر غم من دیگر جای</p></div>
<div class="m2"><p>ز (آری مگر) ت تعمیه می باشد رای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باقی کن باوی ور شگم منمای</p></div>
<div class="m2"><p>بر جان من و جوانی خود بخشای</p></div></div>