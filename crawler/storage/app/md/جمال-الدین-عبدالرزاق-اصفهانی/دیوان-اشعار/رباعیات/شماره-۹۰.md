---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>جز در سر زلف تو نیاساید جان</p></div>
<div class="m2"><p>وندر تن من بی تو نمی پاید جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سیم و زرم نماند جان دربازم</p></div>
<div class="m2"><p>کز بهر چنین روز بگار آید جان</p></div></div>