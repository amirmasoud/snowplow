---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>گر روی چو مه ز من نهانی نکنی</p></div>
<div class="m2"><p>وان بوالعجبی ها که تو دانی نکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم سر آنکه باقی عمر که هست</p></div>
<div class="m2"><p>با تو بسر آرم ار گرانی نکنی</p></div></div>