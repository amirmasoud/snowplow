---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>گفتم چو خطت برنگ موی تو شود</p></div>
<div class="m2"><p>او آفت این روی نکوی تو شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خاطرم از خط تو این خود نگذشت</p></div>
<div class="m2"><p>گو هم مدد جمال روی تو شود</p></div></div>