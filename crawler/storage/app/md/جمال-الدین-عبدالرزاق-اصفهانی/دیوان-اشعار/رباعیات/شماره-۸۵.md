---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>از دور زمانه هیچ می ناسایم</p></div>
<div class="m2"><p>میگویم و با بخت همی برنایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هیچ نصیبی ز جهان نیست مرا</p></div>
<div class="m2"><p>اینجا ز چه مانده ام کرا میبایم</p></div></div>