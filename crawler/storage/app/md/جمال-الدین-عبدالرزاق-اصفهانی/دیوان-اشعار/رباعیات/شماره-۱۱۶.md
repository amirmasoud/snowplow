---
title: >-
    شمارهٔ ۱۱۶
---
# شمارهٔ ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>ای بیش ز مه بنیکوئی و کم نی</p></div>
<div class="m2"><p>وقتست که رحمتی کنی یا هم نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که غمت هست ز جان شیرینتر</p></div>
<div class="m2"><p>زیرا که ز جان سیر شدم وز غم نی</p></div></div>