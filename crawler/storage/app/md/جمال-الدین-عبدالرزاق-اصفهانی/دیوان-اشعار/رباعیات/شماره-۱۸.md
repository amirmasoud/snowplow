---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>دل قصد وصال دلکشی کرد و برفت</p></div>
<div class="m2"><p>خود را بفدای مهوشی کرد و برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون نوبت روز ناخوشی پیش آمد</p></div>
<div class="m2"><p>جانم زمیانه شبخوشی کرد و برفت</p></div></div>