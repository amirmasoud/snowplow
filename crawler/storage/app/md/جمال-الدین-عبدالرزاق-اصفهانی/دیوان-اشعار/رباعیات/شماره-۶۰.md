---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ناگاه چنین کرانه جوئی که چه بود</p></div>
<div class="m2"><p>یکباره نمود سنگ خوئی که چه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی آنهمه مهر کرده دوش آنهمه شرط</p></div>
<div class="m2"><p>امروز چه عذر آری و گوئی که چه بود</p></div></div>