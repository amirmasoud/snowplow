---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>دی وعده خلاف کردم ای عهد شکن</p></div>
<div class="m2"><p>چشم تو نخفت تا بروز روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشگفت گر از گردش چرخ توسن</p></div>
<div class="m2"><p>من خوی تو میگیرم و تو عادت من</p></div></div>