---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>دلدار کمان دلبری کرد بزه</p></div>
<div class="m2"><p>وافکند بگرد مه بر از مشک زره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عهد خود و پشت من آورد شکست</p></div>
<div class="m2"><p>بر کار من و زلف خودافکند گره</p></div></div>