---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>یک شب بمراد دل کسی شاد نزیست</p></div>
<div class="m2"><p>کو با غم دل نشد دیگر روزی بیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک روز نخندید گلی از بادی</p></div>
<div class="m2"><p>کو روز دگر در آتشی خوش نگریست</p></div></div>