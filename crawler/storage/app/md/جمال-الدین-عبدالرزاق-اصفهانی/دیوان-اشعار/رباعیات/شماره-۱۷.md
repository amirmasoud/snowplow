---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که شد گرمی بازار تو سست</p></div>
<div class="m2"><p>هرگز نشدم بمهر در کار تو سست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این کین تو چون سرین سیمین تو سخت</p></div>
<div class="m2"><p>وی عهد تو همچو بند شلوار تو سست</p></div></div>