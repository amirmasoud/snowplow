---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>گر چه ز تو بر دلم ستم میگذرد</p></div>
<div class="m2"><p>ور چه شب و روز من بغم میگذرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل تنگ ندارم که بهر حال که هست</p></div>
<div class="m2"><p>گر ناخوش و گر خوشست هم میگذرد</p></div></div>