---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>جائی که غمت بقصد جان برخیزد</p></div>
<div class="m2"><p>دل را چه زیان گر از میان برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارم سر آن که تا بدستت نکنم</p></div>
<div class="m2"><p>از پا ننشینم ار جهان برخیزد</p></div></div>