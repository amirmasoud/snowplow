---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>شیرین سخنم گر چه لطیف و نیکوست</p></div>
<div class="m2"><p>هم می نرهد ز طعنه دشمن و دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند که بادست همه گفته او</p></div>
<div class="m2"><p>با دست که هم قوت و هم لطف دروست</p></div></div>