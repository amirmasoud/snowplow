---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>گه تاب سر زلف مشوش میده</p></div>
<div class="m2"><p>گه عشوه این جان ستمکش میده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ما سر راستی نداری شاید</p></div>
<div class="m2"><p>باری بدروغ عشوه خوش میده</p></div></div>