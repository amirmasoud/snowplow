---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>زینسان که منم بعشق در کیست بگو</p></div>
<div class="m2"><p>در محنت ازینگونه کسی زیست بگو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون با تو همه دوستیم از جان بود</p></div>
<div class="m2"><p>این دشمنی تو با من از چیست بگو</p></div></div>