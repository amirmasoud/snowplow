---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>یکباره ز ما فلک فراغت دادت</p></div>
<div class="m2"><p>یکباره فراموش شدیم از یادت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با کم ز منی رای وصال افتادت</p></div>
<div class="m2"><p>گفتی که با ازتست مبارک بادت</p></div></div>