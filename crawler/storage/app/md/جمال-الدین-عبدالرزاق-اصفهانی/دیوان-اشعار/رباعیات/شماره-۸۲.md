---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>من جمله زبان ز حرص چون بید شدم</p></div>
<div class="m2"><p>پیش همه چون سایه خورشید شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرد همگان بر آمدم هیچ نشد</p></div>
<div class="m2"><p>یارب تو بده کز همه نومید شدم</p></div></div>