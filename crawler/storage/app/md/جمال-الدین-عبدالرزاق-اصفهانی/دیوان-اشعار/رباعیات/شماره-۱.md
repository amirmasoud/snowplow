---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>از چشم تو صد زخم درشتست مرا</p></div>
<div class="m2"><p>چون زلف توزان خمیده پشتست مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت را گو نهفته دار آن سرخی</p></div>
<div class="m2"><p>تا کس بنداندی که کشتست مرا</p></div></div>