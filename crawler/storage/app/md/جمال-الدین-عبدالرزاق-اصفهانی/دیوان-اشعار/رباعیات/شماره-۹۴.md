---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>تا چند کشیم جور عالم من و تو</p></div>
<div class="m2"><p>شادان همه بر حوصله در غم من و تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکشب خواهم نشسته خرم تو و من</p></div>
<div class="m2"><p>چون زلف تو پیچان شده در هم من و تو</p></div></div>