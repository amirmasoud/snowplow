---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>نی رای تو بر سر عنایت کردن</p></div>
<div class="m2"><p>نی روی مرا از تو حکایت کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان بدروغ گفته ام شکر از تو</p></div>
<div class="m2"><p>کم شرم آید کنون شکایت کردن</p></div></div>