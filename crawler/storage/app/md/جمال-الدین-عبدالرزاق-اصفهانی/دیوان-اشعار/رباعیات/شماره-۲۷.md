---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>آن یار جفا جوی وفادار شدست</p></div>
<div class="m2"><p>کامروز مرا ز دل خریدار شدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا چشم فلک ز جور و بیداد بخفت</p></div>
<div class="m2"><p>یا بخت که خفته بود بیدار شدست</p></div></div>