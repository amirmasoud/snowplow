---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>هجران تو از دو چشم من خواب ببرد</p></div>
<div class="m2"><p>بی آب دو چشمت از رخم آب ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان بگریستم من از فرقت تو</p></div>
<div class="m2"><p>کز آب دو چشمم مژه سیلاب ببرد</p></div></div>