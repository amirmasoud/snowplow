---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>گفتم که چراست گرد آن تنگ شکر</p></div>
<div class="m2"><p>باریک خطی نبشته از عنبر تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا که عقیق را بباید نقشی</p></div>
<div class="m2"><p>تا مهر توان نهاد بر درج گهر</p></div></div>