---
title: >-
    شمارهٔ  ۵ - در مدح ظهیرالدین و شکایت از روزگار
---
# شمارهٔ  ۵ - در مدح ظهیرالدین و شکایت از روزگار

<div class="b" id="bn1"><div class="m1"><p>نالم همی و سود نبینم ز ناله‌ام</p></div>
<div class="m2"><p>فریاد من نمی‌رسد این اشک ژاله‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آنکه نیست هیچ به فردا امید من</p></div>
<div class="m2"><p>باشد ذخیره محنت پنجاه‌ساله‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک لقمه بی‌جگر ندهد ور دهد فلک</p></div>
<div class="m2"><p>هم استخوان بود چو ببینی نواله‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حرص هر کجا که جهد باد دولتی</p></div>
<div class="m2"><p>بر خاک سر نهاده من آنجا حواله‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گریان به گاه قهقهه همچون صراحیم</p></div>
<div class="m2"><p>خندان میان خون جگر چون پیاله‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع هست یک شب و صد بار گریه‌ام</p></div>
<div class="m2"><p>چون نای هست یک دم و صد گونه ناله‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکلم چو نقطه آمد و چرخ چو دایره</p></div>
<div class="m2"><p>بر من کشد خط از چه که نیکو مقاله‌ام</p></div></div>
<div class="b2" id="bn8"><p>چرخ ارچه ذره ز جفا کم نمی‌کند</p>
<p>از وی گله مکن که کراهم نمی‌کند</p></div>
<div class="b" id="bn9"><div class="m1"><p>افسوس دست من که به کیوان نمی‌رسد</p></div>
<div class="m2"><p>آوخ که دور چرخ به پایان نمی‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر من نماند هیچ ملالی و محنتی</p></div>
<div class="m2"><p>کز جور دور گنبد گردان نمی‌رسد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بادا شکسته چنبر گردون دون ازانک</p></div>
<div class="m2"><p>زو راحتی به هیچ مسلمان نمی‌رسد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانی نشان مردم آزاده چیست آن</p></div>
<div class="m2"><p>کز رویش آب رفته و درنان نمی‌رسد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حرمان اهل فضل نگر تا بدان حدست</p></div>
<div class="m2"><p>کز لب گذشته لقمه به دندان نمی‌رسد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرگ ار نمی‌رسد به من این نیست دولتی</p></div>
<div class="m2"><p>کان نیز هم ز غایت حرمان نمی‌رسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر کو نریخت خون و نشد جان شکر چو باز</p></div>
<div class="m2"><p>بر دستگاه پایه سلطان نمی‌رسد</p></div></div>
<div class="b2" id="bn16"><p>بر من جفای چرخ فزون از حکایتست</p>
<p>دیرینه محنتست نه اول شکایتست</p></div>
<div class="b" id="bn17"><div class="m1"><p>چرخ این کمان کین همه بر ما همی‌کشد</p></div>
<div class="m2"><p>خورشید تیغ بر دل دانا همی‌کشد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرجا که خشک‌مغزی و تردامنی بود</p></div>
<div class="m2"><p>دامن بر اوج قبه خضرا همی‌کشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکس که او عنان مروت ز دست داد</p></div>
<div class="m2"><p>او پای در رکاب ثریا همی‌کشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دست اجل گرفته گریبان عمر ما</p></div>
<div class="m2"><p>ز امروز در ربوده به فردا همی‌کشد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دو رویه نیستیم چو کاغذ به هیچ روی</p></div>
<div class="m2"><p>گردون قلم ز بهر چه بر ما همی‌کشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر تشنه که جوید ازین چرخ آبروی</p></div>
<div class="m2"><p>بل شاخ آرزویش بسودا همی‌کشد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کاین چرخ خود برشته زرین آفتاب</p></div>
<div class="m2"><p>از دلو ابر آب ز دریا همی‌کشد</p></div></div>
<div class="b2" id="bn24"><p>شادم ازانکه عمر گذشت ایزد آگهست</p>
<p>عمری چنین گذشته ز ناآمده بهست</p></div>
<div class="b" id="bn25"><div class="m1"><p>یک واقعه نماند که بر من به سر نشد</p></div>
<div class="m2"><p>یک قاعده نماند که زیر و زبر نشد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گفتم درین جوانی چون نیست پایدار</p></div>
<div class="m2"><p>دستی به کام دل بزنم هم به سر نشد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یا دولتست یا هنر از دو یکی‌ست زانک</p></div>
<div class="m2"><p>دولت قرین مردم صاحب هنر نشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چندین هزار جانوران ضایع و صدف</p></div>
<div class="m2"><p>تا کور و کر نبود محل گهر نشد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آزاد سر و بین که تهیدست ماند و نی</p></div>
<div class="m2"><p>تابند بند نامد ظرف شکر نشد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>امروز هر که او دو زبان نیست چون قلم</p></div>
<div class="m2"><p>یا چون دوات تیره دل و بد جگر نشد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همچون دولت فرخ و کلک ظهیر دین</p></div>
<div class="m2"><p>آراسته به حلیت تاج و کمر نشد</p></div></div>
<div class="b2" id="bn32"><p>کان کمال و بحر علوم آسمان فضل</p>
<p>شخصی که زنده از نفس اوست جان فضل</p></div>
<div class="b" id="bn33"><div class="m1"><p>ای کلک نقشبند تو برهان نظم و نثر</p></div>
<div class="m2"><p>وی طبع دلگشای تو سلطان نظم و نثر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>غواص بحر علمی و نقاد عین فضل</p></div>
<div class="m2"><p>معیار جد و هزلی و میزان نظم و نثر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تازه ز خلق خوب تو شد باغ مکرمت</p></div>
<div class="m2"><p>زنده به لفظ عذب تو شد جان نظم و نثر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شد طبع غم زدای تو فهرست عقل و علم</p></div>
<div class="m2"><p>شد لفظ نکته‌زای تو عنوان نظم و نثر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در عالم فصاحت والله که مثل تو</p></div>
<div class="m2"><p>سر بر نزد کسی ز گریبان نظم و نثر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هرگه که سوی فضل گرایی زبان فضل</p></div>
<div class="m2"><p>گوید زهی فرزدق و سحبان نظم و نثر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو آفتاب فضلی و بر هرکه تافتی</p></div>
<div class="m2"><p>گردد به فر تو گهر کان نظم و نثر</p></div></div>
<div class="b2" id="bn40"><p>شد کلک نقشبند تو صورت نگار عقل</p>
<p>گشته مرصع از سخنت گوشوار عقل</p></div>
<div class="b" id="bn41"><div class="m1"><p>ای گاه نطق لفظ تو عیسی روزگار</p></div>
<div class="m2"><p>وی گاه زهد نام تو یحیی روزگار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>انفاس تست قوت ارواح اهل فضل</p></div>
<div class="m2"><p>الفاظ تست حجت دعوی روزگار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>یک لفظ بی‌تو نیست در اوراق آسمان</p></div>
<div class="m2"><p>یک نکته بی‌تو نیست در املی روزگار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ترکیب روز و شب ز سواد و بیاض تست</p></div>
<div class="m2"><p>اینست خود حقیقت معنی روزگار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در خدمت تو هست تسلی فاضلان</p></div>
<div class="m2"><p>جز طاعت تو نیست تمنی روزگار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خوانند در نماز همی لفظ جزل تو</p></div>
<div class="m2"><p>ابنای روزگار به فتوی روزگار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر برخلاف رای تو یک روز بگذرد</p></div>
<div class="m2"><p>حالی قلم نهند بر اجری روزگار</p></div></div>
<div class="b2" id="bn48"><p>هم عقل پیش رای مشیبت جوان‌صفت</p>
<p>هم روح پیش طبع لطیفت گران‌صفت</p></div>
<div class="b" id="bn49"><div class="m1"><p>ای ابر نکته قطره و بحر گهر سخن</p></div>
<div class="m2"><p>وی مهر نور بخشش و ای کان زر سخن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ای بلبل غریب نوای لطیف طبع</p></div>
<div class="m2"><p>ای طوطی بدیع‌سرای شکر سخن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو بحر فضل را صدف در حکمتی</p></div>
<div class="m2"><p>زان التفات می‌ننمایی به هر سخن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مستغنی است طبع تو از ترهات ما</p></div>
<div class="m2"><p>زان مستمع نشد بر هر مختصر سخن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>هستی تو ذوالبیانین وز خوب گفتنت</p></div>
<div class="m2"><p>پیش توایم همچو قلم چشم بر سخن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>سمعت چو کرد خوب گهربار لفظ تو</p></div>
<div class="m2"><p>هرگز چگونه میل کند بر دگر سخن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شد عقل کل به شرم ز لفظ تو در بیان</p></div>
<div class="m2"><p>شد ناطقه خجل ز بیان تو در سخن</p></div></div>
<div class="b2" id="bn56"><p>در عالم کفایت عقل مجسمی</p>
<p>وز غایت لطافت روح مسلمی</p></div>
<div class="b" id="bn57"><div class="m1"><p>نثره برد ز نثر بدیعت نثارها</p></div>
<div class="m2"><p>شعری کند ز شعر لطیفت شعارها</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گشته خجل زرای تو خورشید روزها</p></div>
<div class="m2"><p>بشکسته تیر کلک ز شرم تو بارها</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عاجز بود ز شرح کمالت زبان‌ها</p></div>
<div class="m2"><p>قاصر بود ز حصر خصالت شمارها</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بر روی دهر از قلم تو نگارها</p></div>
<div class="m2"><p>در گوش چرخ از سخنت گوشوارها</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تا چو نه تویی ز پرده غیب آورد برون</p></div>
<div class="m2"><p>برداشت روزگار به سر روزگارها</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بگشای نطق تا که شود تازه روح‌ها</p></div>
<div class="m2"><p>بردار کلک تا کنی از در نثارها</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تا مادر زمانه بزاید چو تو پسر</p></div>
<div class="m2"><p>ای بس که چشم چرخ کشد انتظارها</p></div></div>
<div class="b2" id="bn64"><p>جایی که هست نظم تو سحر حلال چیست</p>
<p>وآنجا که هست نثر تو آب زلال چیست</p></div>
<div class="b" id="bn65"><div class="m1"><p>آنکو سخن به چون تو سخندان برد همی</p></div>
<div class="m2"><p>شوراب سوی چشمه حیوان برد همی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>وان کس که نظم و نثر بدین حضرت آورد</p></div>
<div class="m2"><p>خرما به بصره زیره به کرمان برد همی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>لحنی بود عظیم و خطایی بود وسیع</p></div>
<div class="m2"><p>طیان اگر قصیده به حسان برد همی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>معذور نیست آنکه فرستد بر تو شعر</p></div>
<div class="m2"><p>ور خود گهر بود نه به عمان برد همی</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>بی‌خردگی مدار اگر مورکی ضعیف</p></div>
<div class="m2"><p>پای ملخ به سوی سلیمان برد همی</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طبعم ز بحر فضل تو دزدیده قطره</p></div>
<div class="m2"><p>وان هم به مدحت تو به پایان برد همی</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چون ابر کوز بحر برد قطره وانگهی</p></div>
<div class="m2"><p>تحفه به بحر قطره باران برد همی</p></div></div>
<div class="b2" id="bn72"><p>ای مشتری به شرم ز فرخ‌بقای تو</p>
<p>بادا چو دور گردون دایم بقای تو</p></div>
<div class="b" id="bn73"><div class="m1"><p>یارب ظهیر دین را حشمت مدام باد</p></div>
<div class="m2"><p>اقبال و جاه و دولت او بر دوام باد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>یمن لقا و ناصیتش منقطع مباد</p></div>
<div class="m2"><p>فر و شکوه طلعت او مستدام باد</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>او باغ فضل را به حقیقت چو گلبنست</p></div>
<div class="m2"><p>دایم شکوفه باد و فنا را زکام باد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>در عالم معانی چون او مقیم نیست</p></div>
<div class="m2"><p>بز ذروه معالی او را مقام باد</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>از حلقه هلال و ز شکل بنات نعش</p></div>
<div class="m2"><p>بر مرکب جلالش طوق و ستام باد</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>جایی که نام او ز کرم بر زبان رود</p></div>
<div class="m2"><p>نام کرم بر اهل مکارم حرام باد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بختش ندیم باد و سعادت رفیق باد</p></div>
<div class="m2"><p>چرخش مطیع باد و سپهرش غلام باد</p></div></div>
<div class="b2" id="bn80"><p>باد او سخن سرای و فلک گشته مستمع</p>
<p>وانفاس او مباد ابدالدهر منقطع</p></div>