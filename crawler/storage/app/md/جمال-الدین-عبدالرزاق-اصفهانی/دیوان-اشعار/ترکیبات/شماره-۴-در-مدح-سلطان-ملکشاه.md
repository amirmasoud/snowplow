---
title: >-
    شمارهٔ  ۴ - در مدح سلطان ملکشاه
---
# شمارهٔ  ۴ - در مدح سلطان ملکشاه

<div class="b" id="bn1"><div class="m1"><p>ای مملکت ببال که عهد شهنشهست</p></div>
<div class="m2"><p>وی سلطنت بناز که سلطان ملک شهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر زدست سر ز گریبان ملک شاه</p></div>
<div class="m2"><p>دست ستم زدامن انصاف کوتهست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آفتاب دولت تابنده باش از آنک</p></div>
<div class="m2"><p>بدخواه جاه تو ز تو چون سایه در چهست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این خیمه معلق گردان سبز پوش</p></div>
<div class="m2"><p>در خدمت تو بسته کمر همچو خرگهست</p></div></div>
<div class="b2" id="bn5"><p>در حمله تو رایت الله اکبرست</p>
<p>بر رایت تو آیت الله اکبرست</p></div>
<div class="b" id="bn6"><div class="m1"><p>ای بر میان چرخ کمر از وفای تو</p></div>
<div class="m2"><p>وی بر زبان خلق دعا و ثنای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قائم صلاح دولت و دین در حسام تو</p></div>
<div class="m2"><p>بسته بقای عالم جان در بقای تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آراستست خطبه بفرخنده نام تو</p></div>
<div class="m2"><p>و افروختست سکه بفروبهای تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>انصاف نوبهار ز تأثیر عدل تست</p></div>
<div class="m2"><p>تأثیر آفتاب زتابنده رای تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گردون ز روشنان کواکب همیکند</p></div>
<div class="m2"><p>چتر شب سیاه مرصع برای تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این نوبهار خرم و نوروز دلگشای</p></div>
<div class="m2"><p>فرخنده باد بر تو و بر ما لقای تو</p></div></div>
<div class="b2" id="bn12"><p>در حمله تو رایت الله اکبرست</p>
<p>بر رایت تو آیت الله اکبرست</p></div>
<div class="b" id="bn13"><div class="m1"><p>شاها خدای عزوجل یاور تو باد</p></div>
<div class="m2"><p>توفیق رهنما و خرد رهبر تو باد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انصاف خدای عزوجل یاور تو باد</p></div>
<div class="m2"><p>توفیق رهنما و خرد رهبر تو باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انصاف همعنانت و توفیق همرکاب</p></div>
<div class="m2"><p>اقبال همنشین و ظفر همبر تو باد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گردون مسخر تو و شاهان غلام تو</p></div>
<div class="m2"><p>ایام زیر دست و فلک چاکر تو باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوسه گه ملوک شدست آستان تو</p></div>
<div class="m2"><p>سجده گه ملایک خاک در تو باد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید اگر چه نیست مرصع چو تاج تو</p></div>
<div class="m2"><p>تا این شرف بیابد تاج سر تو باد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بادی تو در پناه خداوند ذوالجلال</p></div>
<div class="m2"><p>و اسلام در پناه سر خنجر تو باد</p></div></div>
<div class="b2" id="bn20"><p>در حمله تو رایت الله اکبرست</p>
<p>بر رایت تو آیت الله اکبرست</p></div>