---
title: >-
    شمارهٔ  ۱ - در نعت رسول اکرم (ص)
---
# شمارهٔ  ۱ - در نعت رسول اکرم (ص)

<div class="b" id="bn1"><div class="m1"><p>ای از بر سدره شاهراهت</p></div>
<div class="m2"><p>وی قبه ی عرش تکیه گاهت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای طاق نهم رواق بالا</p></div>
<div class="m2"><p>بشکسته ز گوشه ی کلاهت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هم عقل، دویده در رکابت</p></div>
<div class="m2"><p>هم شرع ، خزیده در پناهت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چرخ کبود ژنده دلقی</p></div>
<div class="m2"><p>در گردن پیر خانقاهت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مه ، طاسکِ گردن سمندت</p></div>
<div class="m2"><p>شب، طره ی پرچم سیاهت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جبریل مقیم آستانت</p></div>
<div class="m2"><p>افلاک، حریم بارگاهت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرخ ارچه رفیع، خاک پایت</p></div>
<div class="m2"><p>عقل ار چه بزرگ ، طفل راهت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورده است خدا ز روی تعظیم</p></div>
<div class="m2"><p>سوگند به روی همچو ماهت</p></div></div>
<div class="b2" id="bn9"><p>ایزد که رقیب جان خرد کرد</p>
<p>نام تو ردیف نام خود کرد</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای نام تو دستگیر آدم</p></div>
<div class="m2"><p>وی خُلق تو پایمرد عالم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از نام محمدیت میمی</p></div>
<div class="m2"><p>حلقه شده این بلند طارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو در عدم و گرفته قدرت</p></div>
<div class="m2"><p>اقطاع وجود زیر خاتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در خدمتت انبیا مشرف</p></div>
<div class="m2"><p>وز حرمتت آدمی مکرم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از امر مبارک تو رفته</p></div>
<div class="m2"><p>هم بر سر حرفت خود آدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نابوده بوقت خلوت تو</p></div>
<div class="m2"><p>نه عرش و نه جبرئیل محرم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نا یافته عز التفاتی</p></div>
<div class="m2"><p>پیش تو زمین و آسمان هم</p></div></div>
<div class="b2" id="bn17"><p>کونین نواله ای ز جودت</p>
<p>افلاک طفیلیِ وجودت</p></div>
<div class="b" id="bn18"><div class="m1"><p>روح الله با تو خرسواری</p></div>
<div class="m2"><p>روح القدست رکاب داری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از مطبخ تو سپهر، دودی</p></div>
<div class="m2"><p>وز موکب تو زمین، غباری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در شرح رموز غیب گویت</p></div>
<div class="m2"><p>بر ساخته عقل کار و باری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عفوت ز گناه، عذر خواهی</p></div>
<div class="m2"><p>جودت ز سؤال، شرمساری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>این ، کیسه هر نیازمندی</p></div>
<div class="m2"><p>وان ، عُدّتِ هر گناهکاری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر بوی شفاعت تو مانده ست</p></div>
<div class="m2"><p>ابلیس چنان امیدواری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آری چه شود اگر بشوید</p></div>
<div class="m2"><p>لطف تو گلیم خاکساری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی خردگیَست نا امیدی</p></div>
<div class="m2"><p>در عهد چو تو بزرگواری</p></div></div>
<div class="b2" id="bn26"><p>آنجا که ز تو نواله پیچند</p>
<p>هفت و شش و پنج و چار هیچند</p></div>
<div class="b" id="bn27"><div class="m1"><p>ای مسند تو ورای افلاک</p></div>
<div class="m2"><p>صدر تو و خاک توده ، حاشاک</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هرچ آن سمَت حدوث دارد</p></div>
<div class="m2"><p>در دیده ی همت تو خاشاک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>طُغرای جلال تو لَعَمرُک</p></div>
<div class="m2"><p>منشور ولایت تو لولاک</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نُه حقه و هفت مهره پیشت</p></div>
<div class="m2"><p>دست تو و دامن تو زان پاک</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در راه تو زخم، محض مرهم</p></div>
<div class="m2"><p>بر یاد تو زهر عین تریاک</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در عهد نبوت تو آدم</p></div>
<div class="m2"><p>پوشیده هنوز خرقه ی خاک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو کرده اشارت از سر انگشت</p></div>
<div class="m2"><p>مه قرطه ی پرنیان زده چاک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نقش صفحات رایت تو</p></div>
<div class="m2"><p>لولاک لما خلقت الافلاک</p></div></div>
<div class="b2" id="bn35"><p>خواب تو و لاینام قلبی</p>
<p>خوان تو ابیت عند ربی</p></div>
<div class="b" id="bn36"><div class="m1"><p>ای آرزوی قدر لقایت</p></div>
<div class="m2"><p>وی قبله آسمان سرایت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در عالم نطق، هیچ ناطق</p></div>
<div class="m2"><p>نا گفته سزای تو ثنایت</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر جای که خواجه ای، غلامت</p></div>
<div class="m2"><p>هر جای که خسروی، گدایت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هم تابش اختران ز رویت</p></div>
<div class="m2"><p>هم جنبش آسمان برایت</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جانداروی عاشقان حدیثت</p></div>
<div class="m2"><p>قفل دل گمرهان دعایت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اندوخته ی سپهر و انجم</p></div>
<div class="m2"><p>بر نامده ده یک عطایت</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بر شهپر جبرئیل، نه زین</p></div>
<div class="m2"><p>تا لاف زند ز کبریایت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر دیده ی آسمان قدم نه</p></div>
<div class="m2"><p>تا سرمه کشد ز خاکپایت</p></div></div>
<div class="b2" id="bn44"><p>ای کرده به زیر پای، کونین</p>
<p>بگذشته ز حد قاب قوسین</p></div>
<div class="b" id="bn45"><div class="m1"><p>ای از نفس تو صبح زاده</p></div>
<div class="m2"><p>آهت در آسمان گشاده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>علم تو فضول جهل برده</p></div>
<div class="m2"><p>حلم تو غرور کفر داده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در حضرت قدس مسند تو</p></div>
<div class="m2"><p>بر ذروه ی لامکان نهاده</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>آدم ز مشیمه ی عدم نام</p></div>
<div class="m2"><p>در حِجر نبوت تو زاده</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تو کرده چو جان فلک سواری</p></div>
<div class="m2"><p>در گَرد تو انبیا پیاده</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خورشید فلک چو سایه در آب</p></div>
<div class="m2"><p>در پیش تو بر سر ایستاده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از لطف و ز عنفت آب و آتش</p></div>
<div class="m2"><p>اندر عرق و تب اوفتاد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن در بر ساوه غوطه خورده</p></div>
<div class="m2"><p>وین در دل فارس جان بداده</p></div></div>
<div class="b2" id="bn53"><p>خاک قدم تو اهل عالم</p>
<p>زیر علم تو نسل آدم</p></div>
<div class="b" id="bn54"><div class="m1"><p>ای حجره دل بتو منور</p></div>
<div class="m2"><p>وی عالم جان ز تو معطر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ای شخص تو عصمت مجسم</p></div>
<div class="m2"><p>وی ذات تو رحمت مصور</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بی یاد تو ذکرها مزور</p></div>
<div class="m2"><p>بی نام تو وردها مبتر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خاک تو نهال شاخ طوبی</p></div>
<div class="m2"><p>دست تو زهاب آب کوثر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ای از نفس نسیم خلقت</p></div>
<div class="m2"><p>نه گوی فلک چو گوی عنبر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از یعصمک الله اینت جوشن</p></div>
<div class="m2"><p>وز یغفرک الله آنت مغفر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>تو ایمنی از حدوث گوباش</p></div>
<div class="m2"><p>عالم همه خشک یا همه تر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>تو فارغی از وجود گو شو</p></div>
<div class="m2"><p>بطحا همه سنگ یا همه زر</p></div></div>
<div class="b2" id="bn62"><p>طاوس ملائکه بریدت</p>
<p>سر خیل مقربان مریدت</p></div>
<div class="b" id="bn63"><div class="m1"><p>ای دستکش تو این مقرنس</p></div>
<div class="m2"><p>وی دستخوش تو این مقوس</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ای خاشکدانت سقف ازرق</p></div>
<div class="m2"><p>وی شادروانت چرخ اطلس</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون روح ز عیب ها منزه</p></div>
<div class="m2"><p>چون عقل ز نقص ها مقدس</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>از بنگه تو کمینه شش طاق</p></div>
<div class="m2"><p>این چرخ معلق مسدس</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>شد شهر روان بفر نامت</p></div>
<div class="m2"><p>این فلس مکلس مطلس</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در مدح تو هر جماد ناطق</p></div>
<div class="m2"><p>در وصف تو هر فصیح اخرس</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از عهد تو تا بدور آدم</p></div>
<div class="m2"><p>در خیل تو هر چه ز انبیا کس</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>هم کوس نبوت تو در پیش</p></div>
<div class="m2"><p>هم چتر رسالت تو از پس</p></div></div>
<div class="b2" id="bn71"><p>فلج ندب بقیت وحدی</p>
<p>قفل در لا نبی بعدی</p></div>
<div class="b" id="bn72"><div class="m1"><p>ای شرع تو چیره چون به شب روز</p></div>
<div class="m2"><p>وی خیل تو بر ستاره پیروز</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>ای عقلِ گره گشای معنی</p></div>
<div class="m2"><p>در حلقه درس تو نوآموز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ای تیغ تو کفر را کفن باف</p></div>
<div class="m2"><p>نعلین تو عرش را کله دوز</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>ای مذهب ها ز بعثت تو</p></div>
<div class="m2"><p>چون مکتب ها به عید نوروز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>از موی تو رنگِ کسوت شب</p></div>
<div class="m2"><p>وز روی تو نورِ چهره روز</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>حلم تو شگرف دوزخ آشام</p></div>
<div class="m2"><p>خشم تو عظیم آسمان سوز</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ماه سر خیمه ی جلالت</p></div>
<div class="m2"><p>در عالم عِلو مجلس افروز</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>بنموده نشان روی فردا</p></div>
<div class="m2"><p>آیینه ی معجز تو امروز</p></div></div>
<div class="b2" id="bn80"><p>ای گفته صحیح و کرده تصریح</p>
<p>در دست تو سنگ ریزه تسبیح</p></div>
<div class="b" id="bn81"><div class="m1"><p>ای سایه ز خاک بر گرفته</p></div>
<div class="m2"><p>وز روی تو نور خور گرفته</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>ای بال گشاده باز چترت</p></div>
<div class="m2"><p>عالم همه زیر پر گرفته</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>طوطی شکر نثار نطقت</p></div>
<div class="m2"><p>جانها همه در شکر گرفته</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>افکنده وجود را پس پشت</p></div>
<div class="m2"><p>پس فقر فکنده بر گرفته</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>از بهر قبول توبه خویش</p></div>
<div class="m2"><p>آدم سخن تو در گرفته</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>آنجا که جنیبت تو رفرف</p></div>
<div class="m2"><p>عیسی دم لاشه خر گرفته</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>آنجا که نشیمن تو طوبی</p></div>
<div class="m2"><p>موسی ره طور بر گرفته</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>در مکتب جان ز شوق نامت</p></div>
<div class="m2"><p>لوح ارنی ز سر گرفته</p></div></div>
<div class="b2" id="bn89"><p>تا حصن تو نسج عنکبوتست</p>
<p>اوهن نه که احصن البیوتست</p></div>
<div class="b" id="bn90"><div class="m1"><p>هر آدمیئی که او ثنا گفت</p></div>
<div class="m2"><p>هرچ آن نه ثنای تو خطا گفت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>خود خاطر شاعری چه سنجد</p></div>
<div class="m2"><p>نعت تو سزای تو خدا گفت</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>گر چه نه سزای حضرت تست</p></div>
<div class="m2"><p>بپذیر هر آنچه این گدا گفت</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هر چند فضول گوی مردیست</p></div>
<div class="m2"><p>آخر نه ثنای مصطفی گفت؟</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>در عمر هر آنچه گفت یا کرد</p></div>
<div class="m2"><p>نادانی کرد و ناسزا گفت</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>زان گفته و کرده گر بپرسند</p></div>
<div class="m2"><p>کز بهر چه کرد یا چرا گفت</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>این خواهد بود عُدّه ی او</p></div>
<div class="m2"><p>کفاره ی هر چه کرد یا گفت</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>تو محو کن از جریده او</p></div>
<div class="m2"><p>هر هرزره که از سر هوا گفت</p></div></div>
<div class="b2" id="bn98"><p>چون نیست بضاعتی ز طاعت</p>
<p>از ما گنه و ز تو شفاعت</p></div>