---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>چون نعره زنان قصد بکوی تو کنم</p></div>
<div class="m2"><p>جان در سر کار آرزوی تو کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر نفسم هزارجان می باید</p></div>
<div class="m2"><p>تا رقص کنان نثار روی تو کنم</p></div></div>