---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>آن سبزه که از عارض او خاسته شد</p></div>
<div class="m2"><p>تا ظن نبری که حسن آن کاسته شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در باغ رخش بهر تماشای دلم</p></div>
<div class="m2"><p>گل بود و بسبزه نیز آراسته شد</p></div></div>