---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>هر دیده که عاشقست خوابش مدهید</p></div>
<div class="m2"><p>هر دل که در آتشست آبش مدهید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از بر من رمیده، از بهر خدای</p></div>
<div class="m2"><p>گر آید و در زند جوابش مدهید</p></div></div>