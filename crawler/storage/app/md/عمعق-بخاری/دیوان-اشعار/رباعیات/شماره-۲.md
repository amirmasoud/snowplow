---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای شاه، بهار دشمنانت دی باد</p></div>
<div class="m2"><p>در دست تو بند زلف و جام می باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم عدو از خون جگر رنگین باد</p></div>
<div class="m2"><p>هر جا که روی تو نصرة اندر پی باد</p></div></div>