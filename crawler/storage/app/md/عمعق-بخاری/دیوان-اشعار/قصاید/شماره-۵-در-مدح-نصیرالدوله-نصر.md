---
title: >-
    شمارهٔ ۵ - در مدح نصیرالدوله نصر
---
# شمارهٔ ۵ - در مدح نصیرالدوله نصر

<div class="b" id="bn1"><div class="m1"><p>الا یا مشعبد شمال معنبر</p></div>
<div class="m2"><p>بخار بخوری تو، یا گرد عنبر؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه روحی، ولیکن چو روحی مصفا</p></div>
<div class="m2"><p>نه نوری، ولیکن چو نوری منور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آرام گیری هوای تو بی جان</p></div>
<div class="m2"><p>چو جنبش پذیری فضا بر تو جانور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسهای فردوسیانی بصنعت</p></div>
<div class="m2"><p>روانهای روحانیانی بگوهر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه خلقی؟ که نه جسم داری و نه جان</p></div>
<div class="m2"><p>چه مرغی؟ که نه بال داری و نه پر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همی پویی و پای تو در تو پنهان</p></div>
<div class="m2"><p>همی پری و پر تو در تو مضمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اشکال تو روی دریا منقش</p></div>
<div class="m2"><p>ز آثار تو روی صحرا مسطر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رسول بهشتی ز عالم بعالم</p></div>
<div class="m2"><p>برید بهاری ز کشور بکشور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نسیم تو نافه گشاید بصحرا</p></div>
<div class="m2"><p>صریر تو دستان زند بر صنوبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گه از لطف گردی تو برهان عیسی</p></div>
<div class="m2"><p>گه از سحر گردی تو ارتنگ آزر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخاک اندرت صد هزاران مطرا</p></div>
<div class="m2"><p>بآب اندرت صد هزاران زره ور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>الا، ای خجسته براق سلیمان</p></div>
<div class="m2"><p>یکی بر سر کوی معشوق بگذر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یکی صورت انگیز بر خاکش از خون</p></div>
<div class="m2"><p>نزار و جگر خسته و زرد و لاغر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یکی صورتی چون هلال مزور</p></div>
<div class="m2"><p>یکی صورتی چون خیال مزور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خروشان و جوشان و بریان و گریان</p></div>
<div class="m2"><p>بری گشته از خواب و بیزار از خور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در آویخته از خیالی معرا</p></div>
<div class="m2"><p>شمن وار پیشش نشسته چو عنبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گذشته بنا گوشش از گوشه پا</p></div>
<div class="m2"><p>رسیده دو زانوش بر تارک سر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه پیش پیراهن او مخطط</p></div>
<div class="m2"><p>همه خاک پیراهن او معصفر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روان گشته رنجور از درد هجران</p></div>
<div class="m2"><p>زبان گشته مجروح از یاد دلبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو خوی قطره قطره برخساره از خون</p></div>
<div class="m2"><p>چو دل پاره پاره شده جامه در بر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز داغ درونش جوارح جراحت</p></div>
<div class="m2"><p>ز پیکان هجرانش افگار پیکر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکسته ز احداث گردونش گردن</p></div>
<div class="m2"><p>بریده زمانه بخنجرش حنجر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بحالی که گر بر صفت بگذرانی</p></div>
<div class="m2"><p>شرر بارد از کلک و توفان ز دفتر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>الا باد مشکین، چو این نقش کردی</p></div>
<div class="m2"><p>در آویزش از دامن آن ستمگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگویش که: بر خون این سوخته دل</p></div>
<div class="m2"><p>چه عذر آوری پیش دادار داور؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر شرط مهر آزمایی نداری</p></div>
<div class="m2"><p>کم از پرسشی، باری، از حال چاکر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بیا، ای صنم، بر سر راه یاری</p></div>
<div class="m2"><p>یکی بر سر راه بگری و بگذر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ببین چون ره صید مجروح راهم</p></div>
<div class="m2"><p>منقط ز بس قطره های مقطر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فرازش ز خونم چو کوه تبر خون</p></div>
<div class="m2"><p>نشیبش ز اشکم چو آغار فرغر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همه خاک و خاره چو لعل بدخشی</p></div>
<div class="m2"><p>همه سنگ ریزه چو یاقوت احمر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هوا پر ز دل پارهای معلق</p></div>
<div class="m2"><p>زمین پر ز بیجادهای معصفر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>یکی از علمهای گلگون منقش</p></div>
<div class="m2"><p>یکی از نقطهای زرین مشجر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شجرها نگر چون شررهای سوزان</p></div>
<div class="m2"><p>شمرها نگر چون صدفهای گوهر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هماوار بعضی و بعضی کیاگن</p></div>
<div class="m2"><p>چو اندر مغاک چغندر چغندر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بخروارها خاک بین همچو روین</p></div>
<div class="m2"><p>بفرسنگها سنگ بین همچو اخگر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه سنگ و چشمه است بر کوه و صحرا</p></div>
<div class="m2"><p>همه خاک و خونست و بر وادی و جر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از آن سنگ پر خون و خاک عقیقین</p></div>
<div class="m2"><p>بپرس، ای نگارین، همه حال کهتر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کزان سان که من بر فراق تو رفتم</p></div>
<div class="m2"><p>برادر رود زیر بار برادر؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بدان، ای نگارین، که بردندم از تو</p></div>
<div class="m2"><p>بدانسان که آرند اسیران ز کافر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو بیمار بر پشت حمال نالان</p></div>
<div class="m2"><p>دو لب از تفش خشک و دو آستین تر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زمانی ستاده، چو بر طور موسی</p></div>
<div class="m2"><p>زمانی نشسته، چو دجال بر خر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خری بد نژادی، خری بد طبیعت</p></div>
<div class="m2"><p>خری چفته بالای مصروع منظر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خری زیر من چون خیزد و ولیکن</p></div>
<div class="m2"><p>برو من چنان چون کلاوی اعور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دو دستش چنان چون دو چوگان گلگون</p></div>
<div class="m2"><p>دو پایش چو دو خر کمان کمانگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همه پشتش، از گوش تا دم، مغربل</p></div>
<div class="m2"><p>همه خامش، از پای تا سر، مجدر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بخفتی گر از باد پالانش بودی</p></div>
<div class="m2"><p>بماندی گر از سایه بودیش افسر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز هر موی او دیده ای رسته گریان</p></div>
<div class="m2"><p>بهر دیده ای نوحه کردی بر آخر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زمانی فتادی چو مصروع بیخود</p></div>
<div class="m2"><p>زمانی معلق زدی چون کبوتر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>دو بی طاقت و دو ضعیف و دو بیدل</p></div>
<div class="m2"><p>دو بیچاره و دو حزین و دو مضطر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>همی ره بریدیم چون مار بشکم</p></div>
<div class="m2"><p>که این هر دو بر ره عجب مانده رهبر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مرا گفتیی: دست بر کتف گردون</p></div>
<div class="m2"><p>ورا گفتیی: پای بر پای لنگر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شنیدم که: عیسی چو بر آسمان شد</p></div>
<div class="m2"><p>پیاده شد و ماند خر را هم ایدر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرا با چنین خر بمعراج عیسی</p></div>
<div class="m2"><p>ببردند با جان پاکان برابر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بدشتی رسیدم بمانند دریا</p></div>
<div class="m2"><p>که کس جز ملایک ندیدیش معبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه خورشید کردی رسومش مساحت</p></div>
<div class="m2"><p>نه تقدیر کردی حدودش مقدر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گیاش، از درشتی، چو دندان افعی</p></div>
<div class="m2"><p>هواش، از عفونت، چو کام غضنفر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز آبش اجل رسته وز باد پیکان</p></div>
<div class="m2"><p>ز خاکش خسک رسته وز خار خنجر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نه جز دیو در ساحتش کس مساعد</p></div>
<div class="m2"><p>نه جز وحش در وحشتش خلق یاور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی رفتمی در چنین حال لرزان</p></div>
<div class="m2"><p>چو کتف یتیمان عریان در آذر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>حصاری پدید آمد از دور، گفتی</p></div>
<div class="m2"><p>سپهرست رسته ز فولاد و مرمر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>نشیبش ز الماس گسترده مفرش</p></div>
<div class="m2"><p>فرازش ز کافور پیچیده چادر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ببالاش پوشیده افلاک و انجم</p></div>
<div class="m2"><p>بدامانش پنهان شده خاور و خور</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نه خورشید را سوی بالای او ره</p></div>
<div class="m2"><p>نه اندیشه را سوی پهنای او در</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>یکی صورتی چون جهانی بپهنا</p></div>
<div class="m2"><p>بر آورده پیکر بفرق دو پیکر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>ز وادیش عالم پر از تف دوزخ</p></div>
<div class="m2"><p>ز بادش دو دیده پر از نیش نشتر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>هوایی پر از آسمانهای سیمین</p></div>
<div class="m2"><p>زمینی پر از بوستانهای بی بر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>درین بوستان خاره و خار گلشن</p></div>
<div class="m2"><p>در آن آسمان چشم نخجیر اختر</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>چو روحانیان بر بساط بهشتی</p></div>
<div class="m2"><p>بر آن سیم غلتان پلنگان بربر</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نگاران خلدند، گفتی غزالان</p></div>
<div class="m2"><p>گرازان و تازان بر آن فرش عبقر</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>طریقی بر آن آسمان، چون صراطی</p></div>
<div class="m2"><p>چو موی سر زلف خوبان کشمر</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بباریکی پای موران، ولیکن</p></div>
<div class="m2"><p>بتنگی و تاریکی دیده ذر</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>بجایی مسلسل چو هنجار ماران</p></div>
<div class="m2"><p>بجایی شده راست چون خط محور</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو شکل هلالی بصرح ممرد</p></div>
<div class="m2"><p>چو شکل دوالی بسد سکندر</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>رهی چون شهابی بپهنای گردون</p></div>
<div class="m2"><p>رهی چون طنابی فرو هشته از بر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>رهی هم بکردار زنار راهب</p></div>
<div class="m2"><p>بر آویخته طرف محراب و منبر</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>رهی تنگ ازان سان، که گویی مهندس</p></div>
<div class="m2"><p>نمونه خطی بر نگارد بمسطر</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو بر روی حراقه بر، کرم پیله</p></div>
<div class="m2"><p>همی رفتمی من بر آن راه منکر</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>چو دیوانه بر نردبان دوالین</p></div>
<div class="m2"><p>چو مصروع سر مست بر شاخ عرعر</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>گهی دوخته پای بر پشت ماهی</p></div>
<div class="m2"><p>گهی برده سر بر رخ نجم ازهر</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>عدیل و رفیق من اندر چنین ره</p></div>
<div class="m2"><p>یکی اژدهایی خروشان چو تندر</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>بقوت چو گردون، بصولت چو دریا</p></div>
<div class="m2"><p>بتندی چو توفان، بتیزی چو صرصر</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>شکنهای او چون نهنگان سیمین</p></div>
<div class="m2"><p>ولیکن در آمیخته یک بدیگر</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو پیلان و برگستوانهای چینی</p></div>
<div class="m2"><p>پراگنده بر بوی دریای اخضر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>چنان اژدهایی که از سهم و وهمش</p></div>
<div class="m2"><p>فسرده شدی بحر و بگداختی بر</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>من اندر کنارش پشیمان و حیران</p></div>
<div class="m2"><p>همی رفتمی همچو عاصی بمحشر</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>ازین سان شدم تا یکی سنگلاخی</p></div>
<div class="m2"><p>چو قعر جهنم مخوف و مقعر</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>یکی وادیی چون یکی کنج دوزخ</p></div>
<div class="m2"><p>درون گنده مشتی خسیس و محقر</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>گروهی چو یک مشت عفریت عریان</p></div>
<div class="m2"><p>بکنجی چو گور جهودان خیبر</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>چو دیوان بمطمورهای سلیمان</p></div>
<div class="m2"><p>چو رهبان بکنج ستودان قیصر</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>سلب سایه و سنگ فرش و غذا غم</p></div>
<div class="m2"><p>هنر فتنه و فخر شور و شرف شر</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>چون نسناس ناکس، چو خنزیر خیره</p></div>
<div class="m2"><p>چو یاجوج بی حد، چو ماجوج بی مر</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>سواران، ولی بر نمد زین و چارخ</p></div>
<div class="m2"><p>شجاعان، ولیکن بفسق و بساغر</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>همه غافل از حکم دین و شریعت</p></div>
<div class="m2"><p>همه بی خبر از خدا و پیمبر</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>نه هرگز کسی دیده هنجار قبله</p></div>
<div class="m2"><p>نه هرگز شنیده کس الله اکبر</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چو دیوان بندی همه پیر و برنا</p></div>
<div class="m2"><p>چو غولان دشتی همه ماده و نر</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>چو زاغان بصحرا، چو ماغان بوادی</p></div>
<div class="m2"><p>چو سیمرغ در که، چو نخجیر درجر</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>گروهی کریهان سگ طبع سگ خو</p></div>
<div class="m2"><p>گروهی خسیسان خس خوار خس بر</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>بیک روزه نان جمله درویش، لیکن</p></div>
<div class="m2"><p>ز سنگ و سگ و ترف و بچه توانگر</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>بیک تای نان آن کند دیده زن</p></div>
<div class="m2"><p>بیک استخوان این خورد خون مادر</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>همه دیو چهران و دیوانه طبعان</p></div>
<div class="m2"><p>همه سگ پرستان گوساله پرور</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بهر زیر سنگی گروهی برهنه</p></div>
<div class="m2"><p>خزیده بیک دیگر اندر، سراسر</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>جلا گاه ابلیس بودست گویی</p></div>
<div class="m2"><p>هم از وی برد جانور رخت بی مر</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>چه دارند این قوم بند سلیمان؟</p></div>
<div class="m2"><p>اگر نیستی سهم شاه مظفر</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ملک ناصر حق و سلطان مشرق</p></div>
<div class="m2"><p>که جمشید ملکست و خورشید لشکر</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>خداوند عالم، شهنشاه عادل</p></div>
<div class="m2"><p>نصیر دول، نصر با نصرة و فر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>بزرگی، که اندر شروط تفاخر</p></div>
<div class="m2"><p>بزرگیش بگذاشت غایت ز جوهر</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>بدان جا رسیده که گوینده گوید:</p></div>
<div class="m2"><p>نه خالق، ولیکن ز مخلوق برتر</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>چه عزست؟ کان مر ورا نیست زیبا</p></div>
<div class="m2"><p>چه جاهست؟ کان مر ورانیست در خور</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>جهان را بدو گوهر ناموافق</p></div>
<div class="m2"><p>بتوفیق ایزد بکرد او مسخر</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>یکی کلک روشن تن تیره صورت</p></div>
<div class="m2"><p>یکی تیغ خون خوار یاقوت پیکر</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>دو گوهر که هرگز مثالش نیابند</p></div>
<div class="m2"><p>یکی خاک میدان، یکی مشک اذفر</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>یکی دولت افشاند از تاج محنت</p></div>
<div class="m2"><p>یکی آتش انگیزد از آب کوثر</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>ایا پادشاهی، که از دولت تو</p></div>
<div class="m2"><p>جوان گشت باز این جهان معمر</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>فلک زان شرف، تا شود خاک پایت</p></div>
<div class="m2"><p>شود هر شبی چون بساط مدبر</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>بروزی که بخت آزمایند مردان</p></div>
<div class="m2"><p>برد هر کس از کرده خویش کیفر</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>زمین گردد از نعل اسبان مقرنس</p></div>
<div class="m2"><p>هوا گردد از گرد میدان مغبر</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>یکی پوشد از چتر فیروزه خفتان</p></div>
<div class="m2"><p>یکی بندد از فرش بیجاده بر زر</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>جهان گردد از خون مردان چو دریا</p></div>
<div class="m2"><p>تو چون نوح و کشتی تو خنگ رهور</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>گهی همچو خورشید بر روی گردون</p></div>
<div class="m2"><p>گهی چون فرامرز بر پشت اشقر</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>بنوک سنان بستری موی دشمن</p></div>
<div class="m2"><p>بگرز گران بشکنی ترگ و مغفر</p></div></div>
<div class="b" id="bn121"><div class="m1"><p>بدانگه که حمله بری بر معادی</p></div>
<div class="m2"><p>چو ثعبان موسی، چو شیر دلاور</p></div></div>
<div class="b" id="bn122"><div class="m1"><p>سر کینه جویان بتن در گریزد</p></div>
<div class="m2"><p>زره بر کتف گردد از بیم چادر</p></div></div>
<div class="b" id="bn123"><div class="m1"><p>ایا پادشاهی، که از سهم تیغت</p></div>
<div class="m2"><p>مؤنث شود در رحمها مذکر</p></div></div>
<div class="b" id="bn124"><div class="m1"><p>زمین ار چو دوزخ شود، یا چو دریا</p></div>
<div class="m2"><p>زمان ار چو حنظل شود، یا چو شکر</p></div></div>
<div class="b" id="bn125"><div class="m1"><p>منم بر زبان و دل خویش ایمن</p></div>
<div class="m2"><p>ز ریبت مصفا ز شبهت مطهر</p></div></div>
<div class="b" id="bn126"><div class="m1"><p>ز گفتار بد گوی چون گرگ یوسف</p></div>
<div class="m2"><p>ز تلبیس بد خواه چون شیر مادر</p></div></div>
<div class="b" id="bn127"><div class="m1"><p>میان من و دشمن من شریعت</p></div>
<div class="m2"><p>طریقی نهادست سهل و میسر</p></div></div>
<div class="b" id="bn128"><div class="m1"><p>اگر گشت راضی باحکام ایزد</p></div>
<div class="m2"><p>و گر سر نتابد ز دین پیمبر</p></div></div>
<div class="b" id="bn129"><div class="m1"><p>بحکم نیاگان او باز گردم</p></div>
<div class="m2"><p>سیاوخش وار اندر آیم بآذر</p></div></div>
<div class="b" id="bn130"><div class="m1"><p>همی تا موافق نگشت آب و آتش</p></div>
<div class="m2"><p>همی تا مساعد نشد نفع با ضر</p></div></div>
<div class="b" id="bn131"><div class="m1"><p>همی تا جهان گردد از نور ظلمت</p></div>
<div class="m2"><p>زمانی مصفا، زمانی مکدر</p></div></div>
<div class="b" id="bn132"><div class="m1"><p>بقا بادت، ای شاه، در عز و دولت</p></div>
<div class="m2"><p>سر چتر تو گشته با چرخ همبر</p></div></div>
<div class="b" id="bn133"><div class="m1"><p>همیشه دو چشمت به ترک پری‌رخ</p></div>
<div class="m2"><p>همیشه دو دستت به زلف معنبر</p></div></div>
<div class="b" id="bn134"><div class="m1"><p>رخ بدسگال تو از آب دریا</p></div>
<div class="m2"><p>دل دشمن تو پر آتش چو مجمر</p></div></div>