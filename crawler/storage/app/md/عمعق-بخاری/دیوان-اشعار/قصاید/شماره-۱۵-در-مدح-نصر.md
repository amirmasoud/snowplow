---
title: >-
    شمارهٔ ۱۵ - در مدح نصر
---
# شمارهٔ ۱۵ - در مدح نصر

<div class="b" id="bn1"><div class="m1"><p>خیال آن صنم سرو قد سیم ذقن</p></div>
<div class="m2"><p>بخواب دوش یکی صورتی نمود بمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلال وار رخ روشنش گرفته خسوف</p></div>
<div class="m2"><p>کمند وار قد راستش گرفته شکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار شعله آتش فروخته در دل</p></div>
<div class="m2"><p>هزار چشمه توفان گشاده کرده ز تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه بر دو عارض گل رنگ او نشانه گل</p></div>
<div class="m2"><p>نه گرد سینه سیمین او نسیم سمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سمنش سوخته و ریخته گلش در گل</p></div>
<div class="m2"><p>یکی ز درد و دریغ ویکی زیاد محن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رخی، که بود چو جان فریشته رخشان</p></div>
<div class="m2"><p>ز خاک و خون شده همچون لباس اهریمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهیدوار بخون اندرون گرفته مقام</p></div>
<div class="m2"><p>غریب وار بخاک اندرون گرفته وطن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی سرشک و هزاران هزار درد و دریغ</p></div>
<div class="m2"><p>یکی دریغ و هزاران هزار گونه حزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشاده بر رخ بیجاده گون طویله در</p></div>
<div class="m2"><p>گرفته در عرق گوهرین عقیق یمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چه گفت؟ گفت: دریغا امید من، که مرا</p></div>
<div class="m2"><p>غلط فتاد همی در وفا و مهر تو ظن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گمان نبرده بدم من که تو بدین زودی</p></div>
<div class="m2"><p>صبور وار ببندی زیاد بنده دهن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هنوز نرگس سیراب من ندیده جهان</p></div>
<div class="m2"><p>هنوز سوسن آزاد من ندیده چمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هنوز ناچده از بوستان من کس گل</p></div>
<div class="m2"><p>هنوز ناشده سیر این لبان من زلبن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بخاک تیره سپردی مرا بدست اجل</p></div>
<div class="m2"><p>بدل گزیدی کمتر کسی زمن بر من</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنار پر گل من رفته بر کنار زمین</p></div>
<div class="m2"><p>تو در کنار سمن سینگان سیم بدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنفشه موی مرا خاک بر گشاده گره</p></div>
<div class="m2"><p>تو با بنفشه عذاران گره زده دامن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همان کسم که بدی صورتم جمال بهار</p></div>
<div class="m2"><p>همان کسم که بدی عارضم نگارختن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همان کسم که مرا هر که دیدمی گفتی:</p></div>
<div class="m2"><p>سهیل مشکین زلفی و ماه زهره ذقن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کنون بزیر زمینم چو صد هزار غریب</p></div>
<div class="m2"><p>گرفته آن تن مسکین من بگل مسکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز خاک و خشت همی کرده بستر و بالین</p></div>
<div class="m2"><p>ز درد و حسرت کرده ازار و پیراهن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو چشمهای یتیمان ز آب دیده لحد</p></div>
<div class="m2"><p>چو جامهای شهیدان بخون بشسته کفن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه کس بیارد روزی بروزگار یاد</p></div>
<div class="m2"><p>نه کس بگردد روزی مرا بپیرامن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بزیر خاک فراموش گشته از دل خلق</p></div>
<div class="m2"><p>ستم رسیده ز جور زمانه ریمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرفته یاد ترا دوست وار اندر بر</p></div>
<div class="m2"><p>نهاده عهد ترا طوق وار برگردن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ایا بچنگ اجل در سپرده مان بحیل</p></div>
<div class="m2"><p>و یا بدام بلا درفگنده مان بفتن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صنم بدیم و شمن تاکنون و باز کنون</p></div>
<div class="m2"><p>خیال تو صنمست و روان تو چو شمن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گذاشتیم و گذشتیم و آمدیم و شدیم</p></div>
<div class="m2"><p>تو شاد زی و بکن نوش باده روشن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کنون دلیل بهارست و روزگار نشاط</p></div>
<div class="m2"><p>نشاط کن، که جهان پر گلست و پر سوسن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بخواه جام و بر افروز آذر برزین</p></div>
<div class="m2"><p>که پر شمامه کافور شد که و برزن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>رسوم بهمن و بهمنجنه است و روز سده</p></div>
<div class="m2"><p>الا، ببهمن پیش آر قبله بهمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمین صحفیه سیمست و ابر گنج گهر</p></div>
<div class="m2"><p>درخت قبه کافور و سنگ در عدن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ملک درخش همی بارد وفلک الماس</p></div>
<div class="m2"><p>ز خاک سنگ همی روید وز آب آهن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شمامهای بلورست شاخ هر گلبن</p></div>
<div class="m2"><p>خزینهای عبیرست خاک هر معدن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بخواه آن گهر پاک نابسوده، که اوست</p></div>
<div class="m2"><p>بیان قدرت در شان قادر ذوالمن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از آنکه چون بفرازد شعاع آن بفلک</p></div>
<div class="m2"><p>کند کنار نگارینش خلد بر گلشن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر فروخته باشد بود چو زرین کوه</p></div>
<div class="m2"><p>چو آرمیده بود باز بسدین خرمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شبی که او بنماید بخلق صورت خویش</p></div>
<div class="m2"><p>عقیق بار گلست از میان مشک ختن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شعاعهاش پدید آرد از زمین یاقوت</p></div>
<div class="m2"><p>شرارهاش برویاند از زمین روین</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زبانهاش چو شمشیرهای خون آلود</p></div>
<div class="m2"><p>برزمگه بکف شهریار شیر اوژن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شه مظفر منصور، نصر، ناصرحق</p></div>
<div class="m2"><p>که پادشاه زمینست و شهریار زمن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>امان خلق خدای و امین دین رسول</p></div>
<div class="m2"><p>نظام حجت و حق و قوام دین و سنن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بزرگوار کسی، کز بزرگی ملکت</p></div>
<div class="m2"><p>بتیغ دولت بر کند اصل و بیخ فتن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مبارک اختر شاهی، که از ملوک و راست</p></div>
<div class="m2"><p>زمانه زیر مراد و جهان بزیر منن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بدست دولت اسلام را دهد تعلیم</p></div>
<div class="m2"><p>بفرق همت افلاک را کند روزن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چه سد آهن پیشش، چه کاغذین دیوار</p></div>
<div class="m2"><p>چه کوه زرین پیشش، چه دانه ارزن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شجاعت و هنر و جود و جاه و دولت او</p></div>
<div class="m2"><p>جمال و خوبی و خلق کریم و خلق حسن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خدای دادست این دولتش بفضل عطا</p></div>
<div class="m2"><p>برغم حاسد بدخواه و کوری دشمن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ایا گزیده سواری، که در صف میدان</p></div>
<div class="m2"><p>شوند مردان پیشت زنان آبستن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>هزار لشکر باشی تو در یکی میدان</p></div>
<div class="m2"><p>هزار رستم باشی تو در یکی جوشن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نهنگ کوه او باری و شیر آهن خای</p></div>
<div class="m2"><p>هزبر خون افشانی و پیل کوه فگن</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سوار تیغ گزاری، شجاع حیدر زخم</p></div>
<div class="m2"><p>سپهر گرز گرایی، سهیل ناچخ زن</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تبارک الله! روزی که در مصاف آیی</p></div>
<div class="m2"><p>نشسته قارون کردار بر که قارن</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شعاع تیغ تو مر جان کند همه میدان</p></div>
<div class="m2"><p>نهیب زخم تو سندان کند خزاد کن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ز گرز رستم بیشست تازیانه تو</p></div>
<div class="m2"><p>چنانکه نیزه رستم تراکم از سوزن</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بروزگار تو باطل شد، ای ملک، یکسر</p></div>
<div class="m2"><p>فسانهای فرامرز و قصه بیژن</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>جهان تویی و سر تیغ تست دولت وملک</p></div>
<div class="m2"><p>چنانکه خواهی زی و چنانکه خواهی زن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدست دولت شخص موافقان بردار</p></div>
<div class="m2"><p>بتیغ نصرة بیخ مخالفان بر کن</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همیشه تا بدلایل جداست روز از شب</p></div>
<div class="m2"><p>همیشه تا بحقیقت بهست مرد از زن</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همیشه باش با نشاط آزمای و جان پرور</p></div>
<div class="m2"><p>جهان گشای، ولایت ستان و خصم افگن</p></div></div>