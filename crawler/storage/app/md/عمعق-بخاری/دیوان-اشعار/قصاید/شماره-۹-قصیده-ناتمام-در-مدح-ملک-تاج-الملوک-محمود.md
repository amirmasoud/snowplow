---
title: >-
    شمارهٔ ۹ - قصیدهٔ ناتمام در مدح ملک تاج الملوک محمود
---
# شمارهٔ ۹ - قصیدهٔ ناتمام در مدح ملک تاج الملوک محمود

<div class="b" id="bn1"><div class="m1"><p>وقت گل سوری، خیز ای نگار</p></div>
<div class="m2"><p>بر گل سوری می سوری بیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر بط سغدی را گردن بگیر</p></div>
<div class="m2"><p>زخمه زیر و بم او برگمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان می نوشین، که چو جانم بدی</p></div>
<div class="m2"><p>گر شدی اندر تن من پایدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه بود در تن آزادگان</p></div>
<div class="m2"><p>از همه شادی و طرب دستیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر جودست، که گردد بدو</p></div>
<div class="m2"><p>از گهر مردم جود آشکار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نبدی خاصیت او بجود</p></div>
<div class="m2"><p>جای نبودیش کف شهریار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خسرو محمود شهنشاه دهر</p></div>
<div class="m2"><p>تاج ملوک و ملک شهریار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش سوزنده بهنگام رزم</p></div>
<div class="m2"><p>مهر فروزنده بهنگام بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آنکه ازو باغ بهارست ملک</p></div>
<div class="m2"><p>کف زر افشانش ابر بهار</p></div></div>