---
title: >-
    شمارهٔ ۱۰ - در نکوهش اغل نام
---
# شمارهٔ ۱۰ - در نکوهش اغل نام

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب ملک، رهی خفته بود دوش</p></div>
<div class="m2"><p>غایب شده ز عقل و جدا مانده ای ز هوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت سحر، که چشم شود باز از قضا</p></div>
<div class="m2"><p>دیدم بکوی خلقی ماننده سروش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتند بنده را که: اغل را شه جهان</p></div>
<div class="m2"><p>از بندگان بنده زنی هدیه داد دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکم خدای و حکم خداوند نافذست</p></div>
<div class="m2"><p>من بنده مطیعم و فرمان بر خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیکن ستم بود بکنار چنان سگی</p></div>
<div class="m2"><p>سرو ستاره عارض و خورشید لاله پوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او زن بمزد باشد و این عورتان ما</p></div>
<div class="m2"><p>هنجار زن بمزدند ایدون و زن فروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داماد او چگونه بود آنکه مرو را</p></div>
<div class="m2"><p>صد غرچه بیش گاده بود بر ره غموش؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از گوش تابگوش دهانی نهاده باز</p></div>
<div class="m2"><p>چون ماهیان کر بمیان پار گین زوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رویی چو روی دیو و دهانی چو کون مغ</p></div>
<div class="m2"><p>گوشی چو باد بیزن و کونی چو گاو دوش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای کون تو دریده تر از چارغ بلیس</p></div>
<div class="m2"><p>جز ما نیافتی بهمه شهر دست خوش؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا هیچ زن نیابی آن کن که مر تراست</p></div>
<div class="m2"><p>از فرق تابساق وز پایشنه تا بگوش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ریش تو سپید نشد شوی داشتی</p></div>
<div class="m2"><p>اکنون ز بی زنیت چرا باید این خروش؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا کون تو بستن فرسود و ریش گشت</p></div>
<div class="m2"><p>خواهی کسی که داری در پیش، کن تو توش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندر جهان ز جانوران هیچ کس نماند</p></div>
<div class="m2"><p>کز . . . او نیامد در کون تو دروش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اندر ستور گاه و کیلی ازان من</p></div>
<div class="m2"><p>صد سگ تو و به از تو سگ روسبی فروش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای مادر و تبار و کسکهات روسبی</p></div>
<div class="m2"><p>این یک حدیث بشنو و چون سگ تو دار هوش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آغوش زنت هرگز بی پور من مباد</p></div>
<div class="m2"><p>تا بشکفد بنفشه و شب بوی و پیلگوش</p></div></div>