---
title: >-
    شمارهٔ ۸ - در مدح نصیر الدوله ناصرالدین ابوالحسن نصر
---
# شمارهٔ ۸ - در مدح نصیر الدوله ناصرالدین ابوالحسن نصر

<div class="b" id="bn1"><div class="m1"><p>خیز، ای بت بهشتی، آن جام می بیار</p></div>
<div class="m2"><p>کار دی بهشت کرد جهان را بهشت وار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش خورنقست همه باغ و بوستان</p></div>
<div class="m2"><p>فرش ستبرقست همه دشت و کوهسار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرشی فگنده دشت، پر از نقش بافرین</p></div>
<div class="m2"><p>تاجی نهاده باغ، پر از در افتخار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن چون بهار خانه چین پر از نقش چین</p></div>
<div class="m2"><p>این چون نگار خانه مانی پر از نگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن افسر مرصع شاخ سمن نگر</p></div>
<div class="m2"><p>و آن پرده موشح گلهای کامگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چون عذار حورا پر گوهرین سرشک</p></div>
<div class="m2"><p>و آن چون بساط خلد پر از عنبرین عذار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلبن عروس وار بیاراست خویشتن</p></div>
<div class="m2"><p>ابرش مشاطه وار همی شوید از غبار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گاهی طویله بندد از گوهرین صدف</p></div>
<div class="m2"><p>گاهی نقاب پوشد از پرده بخار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن لاله نهفته در و آب چشم ابر</p></div>
<div class="m2"><p>گویی که جامهای عقیقست پر عقار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یا شعلهای آتش ترست اندر آب</p></div>
<div class="m2"><p>یا موجهای لعل بدخشیست در شرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یا لعبتان باغ بهشتی شدند باز</p></div>
<div class="m2"><p>آراسته بدرو گهر گوش و گوشوار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن از ردای رضوان پوشید قرطه ای</p></div>
<div class="m2"><p>وین از پر فریشتگان دوخته ازار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن لوحهای موسی بر گرد کوه و دشت</p></div>
<div class="m2"><p>و آن صفحهای مانی بر سرو و بر چنار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ژاله نقش این همه پر گوهر بدیع</p></div>
<div class="m2"><p>وز لاله فرش آن همه یاقوت آبدار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رنگست، رنگ رنگ، همه کوهسار و دشت</p></div>
<div class="m2"><p>طیره است، طرفه طرفه، همه طرف جویبار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یک کوهسار نعره نخجیر جفت جوی</p></div>
<div class="m2"><p>یک مرغزار ناله مرغان زار زار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هامون ستاره رخ شد و گردون ستاره بخش</p></div>
<div class="m2"><p>صحرا ستاره بر شد و گلبن ستاره بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالم شده بوصل چنین نوبهار خوش</p></div>
<div class="m2"><p>من زار و دور از آن رخ مانند نو بهار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای نوبهار عاشق، آمد بهار نو</p></div>
<div class="m2"><p>نو بنده دور مانده از آن روی چون بهار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روزی هزار بار بپیش خیال تو</p></div>
<div class="m2"><p>دیده کنم بجای سرشک، ای صنم، نثار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ما را چو روزگار فراموش کرده ای</p></div>
<div class="m2"><p>یارا، شکایت از تو کنم یا ز روزگار؟</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر آرزوی روی تو جرمیست عفو کن</p></div>
<div class="m2"><p>ور انتظار وصل تو خونیست در گذار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرد وداعگاه تو،ای دوست، روز و شب</p></div>
<div class="m2"><p>یعقوب وار مانده خروشان و سوگوار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیرامنم ز آب دو دیده چو آبگیر</p></div>
<div class="m2"><p>پیراهنم ز خون دو چشمم چو لاله زار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه بر وصال روی تو ای دوست، دسترس</p></div>
<div class="m2"><p>نه بر دریغ و حسرت هجران تو قرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گه لاله بر دمد بر خم بر، ز خون دل</p></div>
<div class="m2"><p>گه سبزه بر دمد، زنم دیده بر کنار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر قطره ای کز آب دو چشمم فرو چکد</p></div>
<div class="m2"><p>گردد ز آتش دلم اندر زمان شرار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای یادگار مانده مرا یاد روی خویش</p></div>
<div class="m2"><p>یاد رهی نوشته تو بر پشت یادگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از تو بیاد روی تو خرسند گشته ام</p></div>
<div class="m2"><p>زان پس که می بداشتمت در دل استوار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر یک نفس فراق تو اندیشه کردمی</p></div>
<div class="m2"><p>گشتی ز بیم هجر دل و جان من فگار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اکنون تو دوری از من و من بی تو زنده ام</p></div>
<div class="m2"><p>سختا که آدمیست بر احداث روزگار!</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شرطیست مر مرا که: نگیرم بجز تو دوست</p></div>
<div class="m2"><p>عهدیست مر مرا که: نخواهم بجز تو یار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر کالبد بخاک رساند مرا فراق</p></div>
<div class="m2"><p>در زیر خاک باشمت، ای دوست، خواستار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ما بندگان شاه جهانیم و نیک عهد</p></div>
<div class="m2"><p>جز نیک عهد نبود نزدیک شهریار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شاه جهان، سپهر هنر، آفتاب جود</p></div>
<div class="m2"><p>سلطان شرق، ناصر دین، شمسه تبار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گنج محاسن و سر اخیار، ابوالحسن</p></div>
<div class="m2"><p>نصر، آن نصیر دولت، منصور کردگار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاهی، که تا خدای جهان را بیافرید</p></div>
<div class="m2"><p>چون او ندید چشم ستاره بزرگوار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از جود او نهایت موجود شد نهان</p></div>
<div class="m2"><p>وز فضل او کمال شرف گشت آشکار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اندازه هنر هنر او کند پدید</p></div>
<div class="m2"><p>آوازه خرد خرد او کند عیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فخرست ملک را بچنو شاه ملک بخش</p></div>
<div class="m2"><p>عزست بخت را بچنو شاه تاجدار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نی در خرد قیاسش معقول در خرد</p></div>
<div class="m2"><p>نی در هنر صفاتش معدود در شمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>معلوم اوست هر چه معانیست در علوم</p></div>
<div class="m2"><p>موروث اوست هر چه نهانیست در بحار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>آثار عدل او چو ستاره است بی عدد</p></div>
<div class="m2"><p>دریای جود او چو سپهرست بی کنار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رایش چو اصل پاکش پاکیزه از عیوب</p></div>
<div class="m2"><p>رسمش چو اعتقادش تابنده تر زنار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر باد جاه او بزمین بر گذر کند</p></div>
<div class="m2"><p>ور گرد موکبش بفلک بر کند گذار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>این توتیای چشم شرف گردد از شرف</p></div>
<div class="m2"><p>و آن یک قبول عالم اقبال از افتخار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای خسروی، که دولت و اقبال روز و شب</p></div>
<div class="m2"><p>دارند گرد درگه میمون تو قرار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>این از منازعان تو صافی کند جهان</p></div>
<div class="m2"><p>و آن از مخالفان تو خالی کند دیار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ابری تو روز بزم و هزبری تو روز رزم</p></div>
<div class="m2"><p>نیلی بروز بخشش و پیلی بروز کار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>میدان پر اژدها شود از تو بروز جنگ</p></div>
<div class="m2"><p>مجلس پر آفتاب بود از تو روز بار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شمشیر تو قضای بدست، ای ملک، که او</p></div>
<div class="m2"><p>نه در قراب راحت دارد، نه در قرار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا او پدید نامد معلوم کس نشد</p></div>
<div class="m2"><p>خورشید خون فشان و سپهر سرشک بار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر ذوالفقار معجز دین بود، ای ملک</p></div>
<div class="m2"><p>تیغ ذوالفقار و صفات تو ذوالفقار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روزی که گرد معرکه تیره کند هوا</p></div>
<div class="m2"><p>گردد زمین چو قیر و فلک تار همچو قار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>کیمخت کوه بگسلد از زخم بانگ کوس</p></div>
<div class="m2"><p>گوش زمانه کر شود از هول گیر و دار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بی مهر چهرهای دلیران شود زریر</p></div>
<div class="m2"><p>بی باده چشمهای شجاعان کند خمار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بر حلقهای جوشن خون مبارزان</p></div>
<div class="m2"><p>گردد چو لعل خرده بپیروزه بر نگار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شوریده پیل وار در آیی تو در مصاف</p></div>
<div class="m2"><p>چون شیر گرسنه که شتابد پی شکار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گه گرد بر فشانی بر گوشه فلک</p></div>
<div class="m2"><p>گه آب بر جهانی در دیده سوار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>گاهی کنی ز کشته همه روی دشت کوه</p></div>
<div class="m2"><p>گاهی کنی بنیزه همه روی کوه غار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از موج خون کنی تو پر جبرئیل سرخ</p></div>
<div class="m2"><p>وز جان بدسگال رخ آفتاب تار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هر حمله ای که آری بوسه دهد ز جان</p></div>
<div class="m2"><p>بر نعل توسن تو جان سفندیار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از جود دست تو عجب آید مرا همی</p></div>
<div class="m2"><p>تا بر عنان چگونه کنی دست استوار؟</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>رمح تو بند حادثه بگشاید از سپهر</p></div>
<div class="m2"><p>گرز تو برج کنگره بر دارد از حصار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>آسیب نعل اسب تو اندر زمین جنگ</p></div>
<div class="m2"><p>بر آسمان زمین دگر سازد از غبار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گور افگند بباد و سوار افگند بعکس</p></div>
<div class="m2"><p>تیغ تو در نبرد و خدنگ تو در شکار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>ور عکس تیغ تو بهوا روشنی دهد</p></div>
<div class="m2"><p>ارواح کشتگان شود اندر هوا فگار</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای کار زار کرده بر اعدای ملک خویش</p></div>
<div class="m2"><p>وای آن کسی که پیش تو آید بکار زار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>تو سایه خدایی و از روی حفظ خلق</p></div>
<div class="m2"><p>نشگفت اگر عذاب تو باشد خدای وار</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>ابلیس را خدای تعالی عزیز کرد</p></div>
<div class="m2"><p>آنگه چنانکه خواست لعین کردو خاکسار</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چندین هزار دست بر آورده در دعا</p></div>
<div class="m2"><p>با یا رب و تضرع و زاری و زینهار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>هرگز خدای ضایع کی ماند، ای ملک؟</p></div>
<div class="m2"><p>خوش زی و عمر خویش بشادی همی گذار</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>رنجه مباش هرگز، زین پس بدولتت</p></div>
<div class="m2"><p>از لشکر تو یک تن وز دشمنان هزار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ای خسروی، که دولت بی رنج و گنج تو</p></div>
<div class="m2"><p>از جان بد سگال بر آرد همی دمار</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>من بنده گر زیاد تو جان پرورم ز دور</p></div>
<div class="m2"><p>حاسد چه خواهد از من رنجور دل فگار؟</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>تا آب و خاک و آتش و باد، این چهار ضد</p></div>
<div class="m2"><p>با یک دگر بطبع نگردند سازگار</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>تا هر شبی کنار فلک گردد از نجوم</p></div>
<div class="m2"><p>چون چشم عشق بازان پر در شاهوار</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>شاه جهان مظفر و منصور باد و باد</p></div>
<div class="m2"><p>از عمر شادمانه و از ملک شاد خوار</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>درگاه او ز جاه شده قبله ملوک</p></div>
<div class="m2"><p>میدان او ز فخر شده مقصد کبار</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>نیکو سگال دولت او همچو او عزیز</p></div>
<div class="m2"><p>بدخواه جان او شده از غم ذلیل و خوار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چشمش همه بقد سواران سرو قد</p></div>
<div class="m2"><p>دستش همه بزلف نگاران گل عذار</p></div></div>