---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>گله ها دارم و نگویم از آنک</p></div>
<div class="m2"><p>عشق را مهر بر دهن باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان را چو کرم ابریشم</p></div>
<div class="m2"><p>جامه هم گور و هم کفن باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقی کز بلا بیندیشد</p></div>
<div class="m2"><p>عاشق جان خویشتن باشد</p></div></div>