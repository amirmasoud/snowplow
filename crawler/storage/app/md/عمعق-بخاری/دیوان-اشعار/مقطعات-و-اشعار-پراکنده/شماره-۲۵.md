---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>همیشه بود نعمتت را خورنده</p></div>
<div class="m2"><p>ز آزاد و بنده، چه خرد و چه رنده</p></div></div>