---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>اندر زمانه جود تو تنگی رها نکرد</p></div>
<div class="m2"><p>بیمست ازین سخن دهن و چشم تنگ را</p></div></div>