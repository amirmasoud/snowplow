---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>گر نیستی درون دلم آتش فراق</p></div>
<div class="m2"><p>کم هر زمان بسوزد از و استخوان و پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندان بگریمی، که مرا آب چشم من</p></div>
<div class="m2"><p>برداردی روان و ببردی به کوی دوست</p></div></div>