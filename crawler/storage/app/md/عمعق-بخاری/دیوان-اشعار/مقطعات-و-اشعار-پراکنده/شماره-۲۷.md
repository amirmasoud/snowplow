---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>غم تو خجسته بادا، که غمی‌ست جاودانی</p></div>
<div class="m2"><p>ندهم چنین غمی را به هزار شادمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم آنکه خدمت تو کنم و نمی‌توانم</p></div>
<div class="m2"><p>تویی آنکه چاره من نکنی و می‌توانی</p></div></div>