---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>نه چشم است آن که چشمانش ندیده‌ست</p></div>
<div class="m2"><p>نه گوش است آن که صوتش ناشنیده‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپرس از چشم فایز حسن دلبر</p></div>
<div class="m2"><p>که شبها رنج بی خوابی کشیده‌ست</p></div></div>