---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>به هنگام تبسمهای جانان</p></div>
<div class="m2"><p>شکر ریزد ز مروارید و مرجان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبانش آب حیوانست فایز</p></div>
<div class="m2"><p>دهم اندر کنارش تشنه لب جان</p></div></div>