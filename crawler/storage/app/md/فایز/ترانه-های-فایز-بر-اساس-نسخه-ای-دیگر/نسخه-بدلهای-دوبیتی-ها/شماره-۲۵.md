---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>نه از من سر زد ای دلبر خطایی</p></div>
<div class="m2"><p>نه از تو بود یارا بی وفایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز مقدر این چنین شد</p></div>
<div class="m2"><p>چه باید کرد با حکم الهی؟</p></div></div>