---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>انیس من به جز آه سحر نیست</p></div>
<div class="m2"><p>غذای من به جز خون جگر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آه و ناله فایز بترسید</p></div>
<div class="m2"><p>که آه دردمندان بی اثر نیست</p></div></div>