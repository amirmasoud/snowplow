---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>لبت گاه تکلم شکر افشان</p></div>
<div class="m2"><p>شکر زاییده گویی لعل جانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شنیدم در صف فایز بود در</p></div>
<div class="m2"><p>که دیده لؤلؤ اندر لعل و مرجان؟</p></div></div>