---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>بهشت است این زمین یا کوی یار است</p></div>
<div class="m2"><p>که خاکش نافه مشک تتار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر فایز بود مدفون آن خاک</p></div>
<div class="m2"><p>نه بیمش از سوال و نه فشار است</p></div></div>