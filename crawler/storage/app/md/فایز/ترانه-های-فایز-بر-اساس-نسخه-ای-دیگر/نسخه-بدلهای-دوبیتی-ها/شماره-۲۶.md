---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ز دستم رفتی ای عهد جوانی</p></div>
<div class="m2"><p>مرا شد تیره بی تو زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خبر داری که پیری کشت فایز</p></div>
<div class="m2"><p>دریغا باز می آمد جوانی</p></div></div>