---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>به قد طوبا، به لب کوثر، به رخ حور</p></div>
<div class="m2"><p>بدین حسن خدایی چشم بد دور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز ز خوبی بی نیاز است</p></div>
<div class="m2"><p>بود سر تا قدم نور علی نور</p></div></div>