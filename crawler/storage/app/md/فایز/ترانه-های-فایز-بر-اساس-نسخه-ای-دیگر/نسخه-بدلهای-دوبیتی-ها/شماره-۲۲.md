---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>به عارض طره سنبل فکنده</p></div>
<div class="m2"><p>ز سنبل سایه ای بر گل فکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز ز وضع و طرح رفتار</p></div>
<div class="m2"><p>نوا در قمری و بلبل فکنده</p></div></div>