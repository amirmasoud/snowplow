---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>به دل گفتم مکن این قدر فریاد</p></div>
<div class="m2"><p>که اندر خرمن صبر آتش افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوزم شمع سان فایز سراپا</p></div>
<div class="m2"><p>گهی کآن چشم شهلا آورم یاد</p></div></div>