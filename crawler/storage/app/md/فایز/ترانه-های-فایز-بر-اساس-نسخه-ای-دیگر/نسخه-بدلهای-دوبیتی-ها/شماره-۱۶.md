---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>شب عید است و هر کس با عزیزش</p></div>
<div class="m2"><p>کند بازی به زلف مشک ریزش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجز فایز که او یاری ندارد</p></div>
<div class="m2"><p>نشیند با دل خونابه ریزش</p></div></div>