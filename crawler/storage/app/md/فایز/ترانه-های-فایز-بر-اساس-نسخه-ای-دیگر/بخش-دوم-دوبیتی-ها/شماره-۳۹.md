---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>دلا ترسم از این آزار بسیار</p></div>
<div class="m2"><p>خدا نا کرده گویی ترک دلدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر دانی دل فایز چنین است</p></div>
<div class="m2"><p>به خنجر آرمش بیرون به یکبار</p></div></div>