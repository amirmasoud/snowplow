---
title: >-
    شمارهٔ  ۱۲
---
# شمارهٔ  ۱۲

<div class="b" id="bn1"><div class="m1"><p>مبر ای دل به زلف یار انگشت</p></div>
<div class="m2"><p>بدین گیسو منه زینهار انگشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیندیش ای دل فایز، حذر کن</p></div>
<div class="m2"><p>منه اندر دهان مار انگشت</p></div></div>