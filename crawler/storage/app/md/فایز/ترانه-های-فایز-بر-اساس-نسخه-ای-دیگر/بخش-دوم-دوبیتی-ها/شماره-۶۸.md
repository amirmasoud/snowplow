---
title: >-
    شمارهٔ  ۶۸
---
# شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>بتا یکباره اخراجم نمودی</p></div>
<div class="m2"><p>به تیر عشق آماجم نمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دام زلف و چشم یار فایز</p></div>
<div class="m2"><p>کمین کردی و تاراجم نمودی</p></div></div>