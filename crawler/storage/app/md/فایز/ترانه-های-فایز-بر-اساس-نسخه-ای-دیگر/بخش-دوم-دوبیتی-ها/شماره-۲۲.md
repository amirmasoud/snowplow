---
title: >-
    شمارهٔ  ۲۲
---
# شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>جهان امشب معطر دارد این باد</p></div>
<div class="m2"><p>تو گویی مشک عنبر دارد این باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز مگر در بین راهست</p></div>
<div class="m2"><p>که بوی زلف دلبر دارد این باد؟</p></div></div>