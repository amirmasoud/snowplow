---
title: >-
    شمارهٔ  ۷۱
---
# شمارهٔ  ۷۱

<div class="b" id="bn1"><div class="m1"><p>تو خلاقی و هم رزاق مایی</p></div>
<div class="m2"><p>تو که خلق دو عالم را خدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو رزاقی و فایز در پی رزق</p></div>
<div class="m2"><p>دوان هر سو ز بی شرم و حیایی</p></div></div>