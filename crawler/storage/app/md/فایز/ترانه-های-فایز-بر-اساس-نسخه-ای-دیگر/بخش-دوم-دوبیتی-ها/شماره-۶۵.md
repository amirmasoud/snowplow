---
title: >-
    شمارهٔ  ۶۵
---
# شمارهٔ  ۶۵

<div class="b" id="bn1"><div class="m1"><p>بت من دست پیش رو گرفته</p></div>
<div class="m2"><p>به من کج گوشه ابرو گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زده تیری به من تنها اثر کرد</p></div>
<div class="m2"><p>به فایز جنگ رویا رو گرفته</p></div></div>