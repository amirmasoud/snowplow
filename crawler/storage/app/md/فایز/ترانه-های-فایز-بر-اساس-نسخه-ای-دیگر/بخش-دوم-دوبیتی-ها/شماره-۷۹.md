---
title: >-
    شمارهٔ  ۷۹
---
# شمارهٔ  ۷۹

<div class="b" id="bn1"><div class="m1"><p>سفر در پیش، فکر یار درپی</p></div>
<div class="m2"><p>خدایا این سفر کی می کنم طی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود فایز ولی شوقش کشد پس</p></div>
<div class="m2"><p>...</p></div></div>