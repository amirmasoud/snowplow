---
title: >-
    شمارهٔ  ۶۴
---
# شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>گر آید حرف زلفش در میانه</p></div>
<div class="m2"><p>شوم مست و روم بیرون ز خانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میاور نام زلفش پیش فایز</p></div>
<div class="m2"><p>بود دیوانه، زنجیرش بهانه</p></div></div>