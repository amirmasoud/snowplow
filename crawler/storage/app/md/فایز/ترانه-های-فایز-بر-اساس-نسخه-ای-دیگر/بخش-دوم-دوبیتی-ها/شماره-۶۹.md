---
title: >-
    شمارهٔ  ۶۹
---
# شمارهٔ  ۶۹

<div class="b" id="bn1"><div class="m1"><p>به عزم فاتحه رفتم که حمدی</p></div>
<div class="m2"><p>بخوانم بر مزار ارجمندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتی ابرو کمان در راه فایز</p></div>
<div class="m2"><p>به من سوفار مژگان می فکندی</p></div></div>