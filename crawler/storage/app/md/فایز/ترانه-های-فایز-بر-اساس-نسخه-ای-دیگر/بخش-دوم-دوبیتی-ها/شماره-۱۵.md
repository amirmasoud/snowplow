---
title: >-
    شمارهٔ  ۱۵
---
# شمارهٔ  ۱۵

<div class="b" id="bn1"><div class="m1"><p>کشم تا چند جانا انتظارت؟</p></div>
<div class="m2"><p>نشینم تا به کی در رهگذارت؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وطن سازد سر راه تو فایز</p></div>
<div class="m2"><p>که تاگیرم دوباره در کنارت</p></div></div>