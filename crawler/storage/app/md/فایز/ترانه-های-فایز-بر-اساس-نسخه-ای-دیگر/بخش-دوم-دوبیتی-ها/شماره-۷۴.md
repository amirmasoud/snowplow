---
title: >-
    شمارهٔ  ۷۴
---
# شمارهٔ  ۷۴

<div class="b" id="bn1"><div class="m1"><p>اگر امشب کند اقبال یاری</p></div>
<div class="m2"><p>بدست آرم من آن ماه حصاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو رودابه کمند زلف مشکین</p></div>
<div class="m2"><p>کند فایز جزایش بوسه کاری</p></div></div>