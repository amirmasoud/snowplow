---
title: >-
    شمارهٔ  ۲
---
# شمارهٔ  ۲

<div class="b" id="bn1"><div class="m1"><p>مکحل نرگسان منما نگارا</p></div>
<div class="m2"><p>زآب آتش مزن بر جان ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا بودن ز تو نز بیوفائیست</p></div>
<div class="m2"><p>چه سازد فایز از حکم قضا را؟</p></div></div>