---
title: >-
    شمارهٔ  ۴۸
---
# شمارهٔ  ۴۸

<div class="b" id="bn1"><div class="m1"><p>ز وصلت عاقبت مهجور رفتم</p></div>
<div class="m2"><p>خدا داند که من مجبور رفتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوق چشم مخمور تو فایز</p></div>
<div class="m2"><p>دوان تا چشمه معمور رفتم</p></div></div>