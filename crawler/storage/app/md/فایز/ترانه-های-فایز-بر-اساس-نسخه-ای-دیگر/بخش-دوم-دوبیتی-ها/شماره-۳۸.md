---
title: >-
    شمارهٔ  ۳۸
---
# شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>سحر گلبرگ تر شد از جواهر</p></div>
<div class="m2"><p>به دور مه چو پروین گشت ظاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز عرق بر چهره جاری</p></div>
<div class="m2"><p>چو وانهاری به جنت شد مظاهر</p></div></div>