---
title: >-
    شمارهٔ  ۱۱
---
# شمارهٔ  ۱۱

<div class="b" id="bn1"><div class="m1"><p>دو خیل فتنه از زیر سر تست</p></div>
<div class="m2"><p>جهانی پایمال لشکر تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه فایز، عالم از این چشم و ابرو</p></div>
<div class="m2"><p>قتیل تیر و تیغ خنجر تست</p></div></div>