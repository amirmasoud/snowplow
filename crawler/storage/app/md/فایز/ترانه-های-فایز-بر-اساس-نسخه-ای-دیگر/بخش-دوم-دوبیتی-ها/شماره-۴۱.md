---
title: >-
    شمارهٔ  ۴۱
---
# شمارهٔ  ۴۱

<div class="b" id="bn1"><div class="m1"><p>دو معنی برمن آمد صعب و دشوار</p></div>
<div class="m2"><p>در اول پیری، آخر فرقت یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر پیدا شود فایز پرستی</p></div>
<div class="m2"><p>جوانی را کجا آرم دگر بار؟</p></div></div>