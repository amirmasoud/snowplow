---
title: >-
    شمارهٔ  ۴۶
---
# شمارهٔ  ۴۶

<div class="b" id="bn1"><div class="m1"><p>دریغا دل نمی شد روز اول</p></div>
<div class="m2"><p>گرفتار بت گیسو مسلسل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خوش باشد نشینم در وثاقش</p></div>
<div class="m2"><p>به ترکی وار گویم:«گل پری گل»</p></div></div>