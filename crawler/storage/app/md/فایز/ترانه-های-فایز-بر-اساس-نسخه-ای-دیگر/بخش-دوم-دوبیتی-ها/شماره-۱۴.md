---
title: >-
    شمارهٔ  ۱۴
---
# شمارهٔ  ۱۴

<div class="b" id="bn1"><div class="m1"><p>مه حوری و شم قدسی جمالست</p></div>
<div class="m2"><p>رخش خورشید، ابرویش هلالست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب و دندان و چشم یار فایز</p></div>
<div class="m2"><p>پری آیین، ملک سیما همالست</p></div></div>