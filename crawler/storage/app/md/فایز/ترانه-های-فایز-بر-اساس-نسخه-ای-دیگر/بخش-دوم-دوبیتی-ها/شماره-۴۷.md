---
title: >-
    شمارهٔ  ۴۷
---
# شمارهٔ  ۴۷

<div class="b" id="bn1"><div class="m1"><p>اگر برتر بتم بعد از وفاتم</p></div>
<div class="m2"><p>گذار آری به قربان وفاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آب لعل لب فایز ره مهر</p></div>
<div class="m2"><p>حیاتم ده که پامال حیاتم</p></div></div>