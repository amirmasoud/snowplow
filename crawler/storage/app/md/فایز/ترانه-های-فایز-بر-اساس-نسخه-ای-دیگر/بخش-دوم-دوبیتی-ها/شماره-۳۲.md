---
title: >-
    شمارهٔ  ۳۲
---
# شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>سحر عطر خوش گیسوی دلدار</p></div>
<div class="m2"><p>مرا از خواب نوشین کرد بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز استشمام زلف یار فایز</p></div>
<div class="m2"><p>پریده هوشم از سر، دستم از کار</p></div></div>