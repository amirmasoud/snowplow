---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>زهر سو تیر مژگان عاجل آید</p></div>
<div class="m2"><p>پیاپی، بی تامل بر دل آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشد معلوم فایز کشته کیست</p></div>
<div class="m2"><p>که از مقتول حاصل قاتل آید</p></div></div>