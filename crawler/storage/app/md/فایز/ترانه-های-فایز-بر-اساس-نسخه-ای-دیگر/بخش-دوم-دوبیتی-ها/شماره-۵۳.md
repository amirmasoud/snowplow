---
title: >-
    شمارهٔ  ۵۳
---
# شمارهٔ  ۵۳

<div class="b" id="bn1"><div class="m1"><p>خوش آن ساعت که روبرویت افتم</p></div>
<div class="m2"><p>چو بوی مشک در گیسویت افتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحرگه خوش بود فایز پریشان</p></div>
<div class="m2"><p>چو گیسوی تو در پهلویت افتم</p></div></div>