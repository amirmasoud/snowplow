---
title: >-
    شمارهٔ  ۱۹
---
# شمارهٔ  ۱۹

<div class="b" id="bn1"><div class="m1"><p>سحر شور نگارم در سر افتاد</p></div>
<div class="m2"><p>دلم در فکر زلف دلبر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز به عزم دستبردی</p></div>
<div class="m2"><p>که از زلفش زند در لشکر افتاد</p></div></div>