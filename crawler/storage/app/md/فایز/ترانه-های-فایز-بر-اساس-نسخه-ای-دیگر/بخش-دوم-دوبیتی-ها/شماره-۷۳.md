---
title: >-
    شمارهٔ  ۷۳
---
# شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>تو که از پای تا سر دلپسندی</p></div>
<div class="m2"><p>به راز لعل لبها نوشخندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا بشکسته زلف یار فایز</p></div>
<div class="m2"><p>مگر افتاده از جای بلندی؟</p></div></div>