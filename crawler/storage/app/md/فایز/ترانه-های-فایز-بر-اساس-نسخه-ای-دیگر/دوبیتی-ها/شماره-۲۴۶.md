---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>دخیلا شانه بر زلفت میاویز</p></div>
<div class="m2"><p>مزن بر سنبل تر دشنه تیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مبادا کس ببیند تار زلفت</p></div>
<div class="m2"><p>ز آزار دل فایز بپرهیز</p></div></div>