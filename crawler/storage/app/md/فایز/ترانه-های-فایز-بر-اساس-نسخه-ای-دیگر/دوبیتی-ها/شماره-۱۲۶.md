---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>خبر داری به من هجران چها کرد</p></div>
<div class="m2"><p>دلم را ریش و جانم مبتلا کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مردم عشق تو پوشیده فایز</p></div>
<div class="m2"><p>ولی شوق تو رازش بر ملا کرد</p></div></div>