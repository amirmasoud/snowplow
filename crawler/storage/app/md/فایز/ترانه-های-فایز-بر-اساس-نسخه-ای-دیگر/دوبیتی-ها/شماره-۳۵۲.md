---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>دل از من چشم شهلا دلبر از تو</p></div>
<div class="m2"><p>لب خشکیده از من کوثر از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنه بر جان فایز منت از لطف</p></div>
<div class="m2"><p>سر از من سینه از من خنجر از تو</p></div></div>