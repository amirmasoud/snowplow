---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>دلم از دست خوبان چاک چاکست</p></div>
<div class="m2"><p>تنم از هجرشان اندوهناکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو فایز خورده باشد تیر غمزه</p></div>
<div class="m2"><p>ز تیر طعنه دشمن چه باکست؟</p></div></div>