---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>دو نرگس مست و بی باک و ستمگر</p></div>
<div class="m2"><p>ز هندو جمع کرده چار لشکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان عزمند فایز تا سحرگاه</p></div>
<div class="m2"><p>شبیخون آورد بر شهر کشمر</p></div></div>