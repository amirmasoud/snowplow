---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>سر و زلف سیاهت شام یلداست</p></div>
<div class="m2"><p>طلوع صبح در جیب تو پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب و روز تو فایز همچنین است</p></div>
<div class="m2"><p>خوشم کاین هر دو نعمت قسمت ماست</p></div></div>