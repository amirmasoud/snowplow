---
title: >-
    شمارهٔ ۱۷۲
---
# شمارهٔ ۱۷۲

<div class="b" id="bn1"><div class="m1"><p>به هنگام وداع آن لاله رخسار</p></div>
<div class="m2"><p>ز نرگس ریخت بر گل ژاله بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گریه گفت فایز عهدت این بود</p></div>
<div class="m2"><p>شکستی عهد و پیمانت به یکبار؟</p></div></div>