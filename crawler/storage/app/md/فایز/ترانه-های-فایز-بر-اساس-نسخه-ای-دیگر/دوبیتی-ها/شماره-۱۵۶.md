---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>پری رویان سلام از من رسانید</p></div>
<div class="m2"><p>که ای سیمین تنان تا می توانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پا افتاده ای را دست گیرید</p></div>
<div class="m2"><p>چو فایز بی دلی از در مرانید</p></div></div>