---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>برو قاصد که در رفتن ثوابست</p></div>
<div class="m2"><p>به تعجیلی برو حالم خرابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تعجیلی برو در پیش دلبر</p></div>
<div class="m2"><p>بگو فایز دمادم در عذابست</p></div></div>