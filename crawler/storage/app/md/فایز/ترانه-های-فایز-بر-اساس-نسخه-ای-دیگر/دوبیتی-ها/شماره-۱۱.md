---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>خوش آن ملک و خوش آن دلبر خوش آن جا</p></div>
<div class="m2"><p>خوش آن جایی که دلبر کرده ماوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز بود هر جا بهشت است</p></div>
<div class="m2"><p>چه ترکستان چه هندوستان چه بطحا</p></div></div>