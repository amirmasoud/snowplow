---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>لا نتوان به زلفش آرمیدن</p></div>
<div class="m2"><p>از این زنجیر بهتر پا کشیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ای فایز مکن بازی به زلفش</p></div>
<div class="m2"><p>که این مار آخرت خواهد گزیدن</p></div></div>