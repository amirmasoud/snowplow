---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>خوشا روزی که گل بودی و بلبل</p></div>
<div class="m2"><p>تو گفتی صبر کن کردم تحمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الهی دشمن فایز بمیرد</p></div>
<div class="m2"><p>گل از بلبل برید و بلبل از گل</p></div></div>