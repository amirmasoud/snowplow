---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>معاذالله دهم زآن زلف، یک تار</p></div>
<div class="m2"><p>به ملک تبت و ماچین و تاتار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر شهر ختن هم بر تو بخشند</p></div>
<div class="m2"><p>مده فایز زیان کاری زیان کار</p></div></div>