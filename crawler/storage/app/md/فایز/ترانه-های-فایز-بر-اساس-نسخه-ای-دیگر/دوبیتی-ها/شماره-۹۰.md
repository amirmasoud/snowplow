---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>دلم در گوشه چشمش مکین است</p></div>
<div class="m2"><p>نشسته تا ابد منزل گزین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز گرفته خوش مقامی</p></div>
<div class="m2"><p>بلی خوشدل دل کوثر نشین است</p></div></div>