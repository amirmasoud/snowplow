---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>مبر نام جدایی ترسم ای دوست</p></div>
<div class="m2"><p>که همچون مار بیرون آیم از پوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکش فایز که هجران کشت او را</p></div>
<div class="m2"><p>تن مقتول آزردن نه نیکوست</p></div></div>