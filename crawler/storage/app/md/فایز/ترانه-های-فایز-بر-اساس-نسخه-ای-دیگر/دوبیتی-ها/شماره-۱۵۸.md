---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>سحر دل ناله های زار می کرد</p></div>
<div class="m2"><p>چنان که دیده را خونبار می کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکایتهای ایام جوانی</p></div>
<div class="m2"><p>به فایز یک به یک اظهار می کرد</p></div></div>