---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>به شب نالم شب شبگیر نالم</p></div>
<div class="m2"><p>گهی از بخت بی تدبیر نالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنالم چون پلنگ تیر خورده</p></div>
<div class="m2"><p>گهی چون شیر در زنجیر نالم</p></div></div>