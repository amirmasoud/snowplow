---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>سهی سرو این قد و بالا ندارد</p></div>
<div class="m2"><p>گل احمر چنین سیما ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در جنت نه باغ خلد فایز</p></div>
<div class="m2"><p>خدا حوری چنین زیبا ندارد</p></div></div>