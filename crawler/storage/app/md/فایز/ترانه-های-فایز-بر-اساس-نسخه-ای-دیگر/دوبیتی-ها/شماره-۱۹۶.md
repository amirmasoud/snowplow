---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>گهی که یادم آمد صحبت یار</p></div>
<div class="m2"><p>لب و دندان و زلف و چشم و رخسار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و دین و قرار و صبر فایز</p></div>
<div class="m2"><p>فکندند در طلسم چار در چار</p></div></div>