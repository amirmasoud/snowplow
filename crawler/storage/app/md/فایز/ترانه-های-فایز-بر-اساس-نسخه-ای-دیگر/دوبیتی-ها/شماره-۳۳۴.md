---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>پس از ببریدن و پیوند جانان</p></div>
<div class="m2"><p>مرا خوش آمده از دادن جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از مرگ جوانی همچو فایز</p></div>
<div class="m2"><p>تو را خوش باد صحبت با رقیبان</p></div></div>