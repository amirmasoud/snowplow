---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>گهی در خواب بینم قد و بالاش</p></div>
<div class="m2"><p>گهی یاد آورم زلف چلیپاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر بخشد به فایز حور و کوثر</p></div>
<div class="m2"><p>به غیر از یار خود نبود تمناش</p></div></div>