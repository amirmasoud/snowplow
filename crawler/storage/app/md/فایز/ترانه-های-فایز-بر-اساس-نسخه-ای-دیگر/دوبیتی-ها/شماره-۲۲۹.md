---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>نگفتم جا مده بر چهره عنبر</p></div>
<div class="m2"><p>سپاه زنگ بر ماچین میاور؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همی ترسم که جیش کفر، فایز!</p></div>
<div class="m2"><p>نماید ملک دین یکسر مسخر</p></div></div>