---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>سراغ جان جانان از که پرسم</p></div>
<div class="m2"><p>نشان ماه کنعان از که پرسم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو اسکندر به ظلمت رفت فایز</p></div>
<div class="m2"><p>گذار آب حیوان از که پرسم؟</p></div></div>