---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>دل و شوق و خیال و میل هر چار</p></div>
<div class="m2"><p>کشانندم همی تا منزل یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را این چار فایز! دشمنانند</p></div>
<div class="m2"><p>از این خصمان به مردی خود نگه دار</p></div></div>