---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>به رویش زلف اندر پیچ و تابست</p></div>
<div class="m2"><p>همان این اضطراب از التهابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن فایز سیه فام است زلفش</p></div>
<div class="m2"><p>که دایم در جوار آفتابست</p></div></div>