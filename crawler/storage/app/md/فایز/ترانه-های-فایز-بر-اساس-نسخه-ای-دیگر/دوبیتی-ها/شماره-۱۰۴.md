---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>به جز من هر که با دلبر نشیند</p></div>
<div class="m2"><p>الهی بر دلش خنجر نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به والله که راضی نیست فایز</p></div>
<div class="m2"><p>اگر با دوست پیغمبر نشیند</p></div></div>