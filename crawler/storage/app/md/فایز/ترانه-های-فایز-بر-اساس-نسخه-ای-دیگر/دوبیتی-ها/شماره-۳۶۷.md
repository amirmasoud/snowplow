---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>کسی که آن پری رخسار دیده</p></div>
<div class="m2"><p>بر احوال دل من وا رسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پری دیده است آن دیوانه فایز!</p></div>
<div class="m2"><p>که بر تن جامه را صد جا دریده</p></div></div>