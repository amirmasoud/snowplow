---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>بتا بر طرف معجر کن نقابت</p></div>
<div class="m2"><p>مهل بی پرده مانند آفتابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز بپوشان رخ که ترسم</p></div>
<div class="m2"><p>شبی نامحرمی بیند به خوابت</p></div></div>