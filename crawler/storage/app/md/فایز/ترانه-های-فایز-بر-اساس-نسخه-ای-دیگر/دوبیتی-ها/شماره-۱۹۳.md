---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>عجب دارم ز سرو قامت یار</p></div>
<div class="m2"><p>که مشک و لعل و گوهر آورد بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو گیسوهاش فایز مشک نابست</p></div>
<div class="m2"><p>لبانش لعل و دندان در شهوار</p></div></div>