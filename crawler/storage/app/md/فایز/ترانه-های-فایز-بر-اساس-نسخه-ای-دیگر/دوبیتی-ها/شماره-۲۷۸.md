---
title: >-
    شمارهٔ ۲۷۸
---
# شمارهٔ ۲۷۸

<div class="b" id="bn1"><div class="m1"><p>مرا جان بر لب آمد از غم دل</p></div>
<div class="m2"><p>نشینم تا به کی در ماتم دل؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه در دنیا نه مافیهاش، فایز!</p></div>
<div class="m2"><p>نبینم عالمی جز عالم دل</p></div></div>