---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>سحرگه برگ گل تر شد ز شبنم</p></div>
<div class="m2"><p>نسیم! آهسته بگشا زلف از هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاور عطر زلفش سوی فایز</p></div>
<div class="m2"><p>مرا فارغ کن از غم‌های عالم</p></div></div>