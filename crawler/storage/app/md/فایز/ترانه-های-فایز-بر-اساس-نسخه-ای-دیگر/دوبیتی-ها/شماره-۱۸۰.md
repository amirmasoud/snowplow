---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>تو پنداری که تا گشتی ز من دور</p></div>
<div class="m2"><p>ندارم در نظر چشم تو منظور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رود از سر خیالش فایز!آن دم</p></div>
<div class="m2"><p>که آواز سرافیل آید از صور</p></div></div>