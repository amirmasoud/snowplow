---
title: >-
    شمارهٔ ۲۶۶
---
# شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>زما آن چشم و ابرو می برد دل</p></div>
<div class="m2"><p>لب و دندان و گیسو می برد دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز ز وضع طرز و رفتار</p></div>
<div class="m2"><p>نه من دل داده ام او می برد دل</p></div></div>