---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>سحرگه از نوای مرغ گلزار</p></div>
<div class="m2"><p>سرم پرشور گشت و دیده بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر گل بلبلی فایز نواخوان</p></div>
<div class="m2"><p>«چه خوش باشد نشستن یار با یار»</p></div></div>