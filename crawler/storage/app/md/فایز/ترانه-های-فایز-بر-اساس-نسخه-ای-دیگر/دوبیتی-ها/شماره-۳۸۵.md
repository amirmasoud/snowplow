---
title: >-
    شمارهٔ ۳۸۵
---
# شمارهٔ ۳۸۵

<div class="b" id="bn1"><div class="m1"><p>در اول مستم از پیمانه کردی</p></div>
<div class="m2"><p>چو مرغم آشنا با دانه کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتی انس با فایز ولیکن</p></div>
<div class="m2"><p>به خون آشامیم پروا نکردی</p></div></div>