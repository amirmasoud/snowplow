---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>به عارض طره سنبل فکنده</p></div>
<div class="m2"><p>زسنبل سایه ای بر گل فکنده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز ز وضع و طبع گفتار</p></div>
<div class="m2"><p>نوا در قمری و بلبل فکنده</p></div></div>