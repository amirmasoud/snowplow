---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>چرا خواهی که من آیم به غربت</p></div>
<div class="m2"><p>چرا باید کشم این رنج و محنت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درازی عمر فایز این چنین شد</p></div>
<div class="m2"><p>که لعنت بر چنین عمری به ذلت</p></div></div>