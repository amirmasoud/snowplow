---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>به زیر پیرهن پستان دلبر</p></div>
<div class="m2"><p>نمایان چون گهر در حقه زر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی نادیده و نشنیده فایز</p></div>
<div class="m2"><p>که سرو ناز لیمو آورد بر</p></div></div>