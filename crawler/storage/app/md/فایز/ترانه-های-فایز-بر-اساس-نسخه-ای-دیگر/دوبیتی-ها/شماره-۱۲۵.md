---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>بتا زلف تو سر از سر کشان برد</p></div>
<div class="m2"><p>به میدان گوی حسن از مهوشان برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز چو رستم پور دستان</p></div>
<div class="m2"><p>که در میدان «کشانی» ره کشان برد</p></div></div>