---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>مسلسل زلف عنبر بار دارد</p></div>
<div class="m2"><p>مکحل نرگس خمار دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ابروی کمند و تیر مژگان</p></div>
<div class="m2"><p>چو فایز کشته بسیار دارد</p></div></div>