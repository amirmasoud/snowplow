---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>ندانم چون کنم دل بی قرار است</p></div>
<div class="m2"><p>به چشمم روز روشن شام تاراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم راز دل با کی بگویم</p></div>
<div class="m2"><p>دو چشمم فایز اینک اشکبار است</p></div></div>