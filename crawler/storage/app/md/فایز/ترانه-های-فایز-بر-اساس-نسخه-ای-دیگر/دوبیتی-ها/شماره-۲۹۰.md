---
title: >-
    شمارهٔ ۲۹۰
---
# شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>جوانی! رفتی و نایی به سویم</p></div>
<div class="m2"><p>نمی دانم کجایت باز جویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فایز پرس و جو احوال پیری</p></div>
<div class="m2"><p>که تا با تو حکایت بازگویم</p></div></div>