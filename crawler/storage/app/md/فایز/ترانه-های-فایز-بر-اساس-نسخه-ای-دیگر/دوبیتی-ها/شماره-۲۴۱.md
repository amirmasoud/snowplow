---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>شود فردا که منشیان تقدیر</p></div>
<div class="m2"><p>نویسند جرم عصیانم به قطمیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن محضر ندارد عذر فایز</p></div>
<div class="m2"><p>به جز امید عفو و رحم تقصیر</p></div></div>