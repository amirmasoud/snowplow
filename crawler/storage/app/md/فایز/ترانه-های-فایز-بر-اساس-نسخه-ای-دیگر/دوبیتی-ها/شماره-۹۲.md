---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>به دوشش گیسوان خوش دلپسند است</p></div>
<div class="m2"><p>که این مخصوص آن قد بلند است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حمایلهای گیسو یار فایز</p></div>
<div class="m2"><p>تو گویی جنگجویی با کمند است</p></div></div>