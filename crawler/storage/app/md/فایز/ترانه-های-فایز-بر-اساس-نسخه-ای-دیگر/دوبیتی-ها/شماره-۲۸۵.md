---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>ندانم از چه رو آشفته حالم</p></div>
<div class="m2"><p>چه کردم کز من آزرده ست یارم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبیبم اوست گاهی حال فایز</p></div>
<div class="m2"><p>نمی پرسد که من بیمار دارم</p></div></div>