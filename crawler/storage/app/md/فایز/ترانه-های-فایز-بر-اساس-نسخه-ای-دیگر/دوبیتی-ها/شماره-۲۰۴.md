---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>ز تاثیر هوا خوی بر رخ یار</p></div>
<div class="m2"><p>شده جاری چو در فردوس انهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از آن عرق یک قطره فایز</p></div>
<div class="m2"><p>چکد بر تو جوان گردی دگر بار</p></div></div>