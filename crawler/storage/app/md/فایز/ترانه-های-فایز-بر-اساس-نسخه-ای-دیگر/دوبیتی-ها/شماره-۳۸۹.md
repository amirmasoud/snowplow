---
title: >-
    شمارهٔ ۳۸۹
---
# شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>به گیتی هر چه کردم جستجویی</p></div>
<div class="m2"><p>به گلزارش ندیدم رنگ و بویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اوضاع نشاط و عیش فایز</p></div>
<div class="m2"><p>ندیدم هیچ غیر از گفتگویی</p></div></div>