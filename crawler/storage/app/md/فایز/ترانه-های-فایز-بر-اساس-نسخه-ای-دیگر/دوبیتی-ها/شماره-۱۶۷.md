---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>ز آب و آتش و از خاک و از باد</p></div>
<div class="m2"><p>خدا رخسار خوبان را صفا داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو چشم ما نظر بگشاد، فایز!</p></div>
<div class="m2"><p>غضوا ابصارکم را کرد ارشاد</p></div></div>