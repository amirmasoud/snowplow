---
title: >-
    شمارهٔ ۴۰۶
---
# شمارهٔ ۴۰۶

<div class="b" id="bn1"><div class="m1"><p>ز دستم رفتی ای حور بهشتی</p></div>
<div class="m2"><p>مرا در دوزخ هجران، بهشتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگفتی: «فایزی هم داشتم من»</p></div>
<div class="m2"><p>بریدی بی سبب تخمی که کشتی</p></div></div>