---
title: >-
    شمارهٔ ۱۰۸
---
# شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>خیالت گر نبودی دل چه می کرد</p></div>
<div class="m2"><p>وگر شوقت نبد حاصل چه می کرد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود ار عشق جانان فایز این جان</p></div>
<div class="m2"><p>در این ویرانه تن منزل چه می کرد؟</p></div></div>