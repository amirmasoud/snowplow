---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>خوشا برنایی و نیسان و آزار</p></div>
<div class="m2"><p>که بد با گلعذارانم سر و کار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحر در دست فایز زلف جانان</p></div>
<div class="m2"><p>چو سبحه می نمودم تار با تار</p></div></div>