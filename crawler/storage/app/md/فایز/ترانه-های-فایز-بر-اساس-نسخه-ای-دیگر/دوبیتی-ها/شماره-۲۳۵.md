---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>بتا! بیژن صفت در چه گرفتار</p></div>
<div class="m2"><p>منیژه وار اگر هستی وفادار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمند زلف بگشا، چون تهمتن</p></div>
<div class="m2"><p>تو فایز را ز چاه غم برون آر</p></div></div>