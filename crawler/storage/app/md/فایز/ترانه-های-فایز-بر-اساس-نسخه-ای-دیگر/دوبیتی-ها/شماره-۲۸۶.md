---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>دگر شب شد که مشتاق حضورم</p></div>
<div class="m2"><p>کنم یاد از رفیق گشته دورم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسوزد هیچ عاشق مثل فایز</p></div>
<div class="m2"><p>خمیر آسا گرفتار تنورم</p></div></div>