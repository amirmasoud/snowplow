---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>خدایم گر کند فردای محشر</p></div>
<div class="m2"><p>مخیر از بهشت و وصل دلبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آرد بانگ مشتاقانه فایز</p></div>
<div class="m2"><p>که ما را وصل یار از هر چه بهتر</p></div></div>