---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>تو که ای دل مکان در کوی یار است</p></div>
<div class="m2"><p>تو که روز و شبان آنجا قرار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که با فایزت نبود علاقه</p></div>
<div class="m2"><p>بگو دیگر تو را با ما چه کار است؟</p></div></div>