---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>بتا از دوریت حالی ندارم</p></div>
<div class="m2"><p>زعین و شین و قافت بی قرارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ت و ب گرفتارم شب و روز</p></div>
<div class="m2"><p>به غیر از لام وب درمان ندارم</p></div></div>