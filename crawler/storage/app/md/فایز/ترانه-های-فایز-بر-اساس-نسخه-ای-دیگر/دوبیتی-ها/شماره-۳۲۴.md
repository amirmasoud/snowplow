---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>خیال کشتن من داشت جانان</p></div>
<div class="m2"><p>کدامین سنگدل کردش پشیمان؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانست عید فایز آن زمانست</p></div>
<div class="m2"><p>که گردد در منای دوست قربان</p></div></div>