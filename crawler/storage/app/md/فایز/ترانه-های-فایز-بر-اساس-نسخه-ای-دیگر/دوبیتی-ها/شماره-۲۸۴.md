---
title: >-
    شمارهٔ ۲۸۴
---
# شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>پری پیکر بت عیسا پرستم</p></div>
<div class="m2"><p>به یک نظاره دل بردی ز دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا بنشین به فایز مهربان شو!</p></div>
<div class="m2"><p>که من دین مسلمانی شکستم</p></div></div>