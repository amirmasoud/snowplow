---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>دلم را جز تو کس دلبر نباشد</p></div>
<div class="m2"><p>به جز شور توام در سر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز تو عمدا می کنی تنگ</p></div>
<div class="m2"><p>که تا جای کس دیگر نباشد</p></div></div>