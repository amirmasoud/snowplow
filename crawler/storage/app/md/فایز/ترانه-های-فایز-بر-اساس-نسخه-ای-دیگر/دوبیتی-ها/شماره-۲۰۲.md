---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>گرفتم آن که اقبالم شود یار</p></div>
<div class="m2"><p>شود این بخت من از خواب بیدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکسته قامت فایز ز پیری</p></div>
<div class="m2"><p>دگر شاخ شکسته کی دهد بار</p></div></div>