---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>منه بر رخ دو گیسوی معنبر</p></div>
<div class="m2"><p>مسوزان اندر آتش سنبل تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به افسرده شب فایز بیندیش</p></div>
<div class="m2"><p>که سرما خورده داند قدر آذر</p></div></div>