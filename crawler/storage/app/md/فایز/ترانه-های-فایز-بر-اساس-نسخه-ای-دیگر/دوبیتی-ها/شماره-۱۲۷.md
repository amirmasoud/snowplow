---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>دو زلف یار عنبر می تراود</p></div>
<div class="m2"><p>جبینش نور اظهر می تراود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگر فایز لبش را نام بردی</p></div>
<div class="m2"><p>که از لبهات شکر می تراود؟</p></div></div>