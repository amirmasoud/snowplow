---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>سحر چون زهره از مشرق برآمد</p></div>
<div class="m2"><p>نگارم همچو مه از در درآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فیض مقدم دلدار فایز</p></div>
<div class="m2"><p>بحمدالله شب هجران سرآمد</p></div></div>