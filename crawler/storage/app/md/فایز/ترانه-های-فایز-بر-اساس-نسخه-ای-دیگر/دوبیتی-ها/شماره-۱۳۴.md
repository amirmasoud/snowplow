---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>خروس عرش دیشب التجا کرد</p></div>
<div class="m2"><p>برای عاشق مسکین دعا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الهی خیر از عمرش نبیند</p></div>
<div class="m2"><p>هر آن کس یار از فایز جدا کرد</p></div></div>