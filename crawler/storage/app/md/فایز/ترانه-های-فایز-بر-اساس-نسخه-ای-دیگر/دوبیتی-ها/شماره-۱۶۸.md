---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>بتا بیهوده منما گریه بسیار</p></div>
<div class="m2"><p>بمیرم تا نبینم گریه ات یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امیدم بود جانا بعد فایز</p></div>
<div class="m2"><p>بگریی تا قیامت بر من زار</p></div></div>