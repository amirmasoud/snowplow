---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>سر راهم دو تا شد وای بر من</p></div>
<div class="m2"><p>رفیق از من جدا شد وای بر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا شد از من آن دلدار فایز</p></div>
<div class="m2"><p>به غربت آشنا شد وای بر من</p></div></div>