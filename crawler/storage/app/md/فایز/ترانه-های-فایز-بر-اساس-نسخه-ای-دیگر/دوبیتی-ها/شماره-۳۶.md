---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>جهان رفت و جوانی و چمن رفت</p></div>
<div class="m2"><p>گل نسرین و سرو و یاسمن رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از من دوستان گویند:«افسوس</p></div>
<div class="m2"><p>که آخر فایز شیرین سخن رفت»</p></div></div>