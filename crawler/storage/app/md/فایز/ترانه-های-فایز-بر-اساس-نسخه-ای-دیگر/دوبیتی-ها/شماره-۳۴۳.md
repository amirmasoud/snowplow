---
title: >-
    شمارهٔ ۳۴۳
---
# شمارهٔ ۳۴۳

<div class="b" id="bn1"><div class="m1"><p>سحر دلبر چو آمد بر سر من</p></div>
<div class="m2"><p>خدا داند چه آمد بر سر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیامت قامت فایز چو بنشست</p></div>
<div class="m2"><p>ز نو برخاست شور محشر من</p></div></div>