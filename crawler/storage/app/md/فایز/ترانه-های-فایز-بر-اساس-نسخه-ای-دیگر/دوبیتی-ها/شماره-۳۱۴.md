---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>خم ابروست یا شمشیر بهمن</p></div>
<div class="m2"><p>مژه یا نیزه یا تیر تهمتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز منیژه سان به یکبار</p></div>
<div class="m2"><p>به چاهم در فکن مانند بیژن</p></div></div>