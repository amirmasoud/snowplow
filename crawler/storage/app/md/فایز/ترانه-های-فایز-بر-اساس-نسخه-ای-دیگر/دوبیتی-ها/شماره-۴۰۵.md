---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>دلا! امشب نه بر جای خودستی</p></div>
<div class="m2"><p>کجا رفتی، بگو با کی نشستی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز چو گل در دست خوبان</p></div>
<div class="m2"><p>فتاد و رفت از دستی به دستی</p></div></div>