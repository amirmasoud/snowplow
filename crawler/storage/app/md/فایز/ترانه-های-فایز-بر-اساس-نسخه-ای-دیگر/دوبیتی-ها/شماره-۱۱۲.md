---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>بتم سرپنجه با لوح و قلم زد</p></div>
<div class="m2"><p>زمین و آسمان از نو به هم زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس پرده در آمد یار فایز</p></div>
<div class="m2"><p>چو خورشیدی که از مشرق علم زد</p></div></div>