---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>دلا دیدی که دلبر عهد بشکست</p></div>
<div class="m2"><p>زما ببرید و با اغیار پیوست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو فایز از جفای بی وفایان</p></div>
<div class="m2"><p>بسایی تا قیامت دست بر دست</p></div></div>