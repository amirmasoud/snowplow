---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>اگر دوران دهد بر بادم ای دوست</p></div>
<div class="m2"><p>وگر هجران کند بنیادم ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن باور که فایز چشم و زلفش</p></div>
<div class="m2"><p>رود از خاطر و از یادم ای دوست</p></div></div>