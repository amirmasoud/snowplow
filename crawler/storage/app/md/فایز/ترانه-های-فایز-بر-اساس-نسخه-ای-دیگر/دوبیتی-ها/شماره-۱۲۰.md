---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>که بر نخل امید من تبر زد؟</p></div>
<div class="m2"><p>که بر مبنای اقبالم حجر زد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا فایز که کرد از وصل جانان</p></div>
<div class="m2"><p>که بر جانم خدنگ بی خبر زد؟</p></div></div>