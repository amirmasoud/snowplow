---
title: >-
    شمارهٔ ۳۱۷
---
# شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>از این ره می روی رویت به من کن</p></div>
<div class="m2"><p>جوانی کشته ای فکر کفن کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو فایز کشته ای با تیر مژگان</p></div>
<div class="m2"><p>بنه لب بر لب و جانم به تن کن</p></div></div>