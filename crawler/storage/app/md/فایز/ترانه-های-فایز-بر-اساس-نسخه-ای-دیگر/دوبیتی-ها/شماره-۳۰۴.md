---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>نشد ز آمد شد نظاره مفهوم</p></div>
<div class="m2"><p>به لب دندان بود یا در منظوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهانش نون تنوین است فایز!</p></div>
<div class="m2"><p>که آید در حساب و نیست معلوم</p></div></div>