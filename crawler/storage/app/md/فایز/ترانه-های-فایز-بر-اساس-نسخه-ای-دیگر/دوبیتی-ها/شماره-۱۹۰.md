---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>به زیر زلف مشکین عارض یار</p></div>
<div class="m2"><p>نمایان چون قمر اندر شب تار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان جلوه کند بر چشم فایز</p></div>
<div class="m2"><p>که زاغی برگ گل دارد به منقار</p></div></div>