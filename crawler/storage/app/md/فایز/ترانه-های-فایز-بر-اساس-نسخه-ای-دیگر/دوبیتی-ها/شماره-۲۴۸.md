---
title: >-
    شمارهٔ ۲۴۸
---
# شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>پی تعظیم قدر دلبر امروز</p></div>
<div class="m2"><p>زخاور خور فرود آرد سر امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برون آرند بهر یار فایز</p></div>
<div class="m2"><p>زکه زر و ز دریا گوهر امروز</p></div></div>