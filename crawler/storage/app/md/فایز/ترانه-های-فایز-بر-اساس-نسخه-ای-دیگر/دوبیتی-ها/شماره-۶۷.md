---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>خبر از دل ندارم نیست یا هست</p></div>
<div class="m2"><p>برید از ما و با دلدار پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گله از دل مکن فایز که پیری</p></div>
<div class="m2"><p>تو را از پا فکند و رفت از دست</p></div></div>