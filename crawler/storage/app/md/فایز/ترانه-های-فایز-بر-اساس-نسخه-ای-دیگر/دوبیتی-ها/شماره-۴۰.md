---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>هنوزم بوی زلفش در مشام است</p></div>
<div class="m2"><p>هنوزم ذوق لبهایش به کام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کجا فایز شود از ناله خاموش</p></div>
<div class="m2"><p>مگر آن دم که در خاکش مقام است</p></div></div>