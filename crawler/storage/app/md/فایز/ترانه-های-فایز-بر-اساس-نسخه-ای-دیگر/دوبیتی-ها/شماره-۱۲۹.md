---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>دو چشمت چون به چشمانم نگه کرد</p></div>
<div class="m2"><p>لب لعل و رخت روزم سیه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن عشوه دگر بر فایز زار</p></div>
<div class="m2"><p>که ابروی کجت جانم تبه کرد</p></div></div>