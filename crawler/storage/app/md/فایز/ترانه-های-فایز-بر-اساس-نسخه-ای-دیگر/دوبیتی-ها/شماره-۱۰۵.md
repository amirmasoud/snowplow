---
title: >-
    شمارهٔ ۱۰۵
---
# شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>اگر صد تیر ناز از دلبر آید</p></div>
<div class="m2"><p>مکن باور که آه از دل بر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از صد سال بعد از مرگ فایز</p></div>
<div class="m2"><p>هنوز آواز دلبر دلبر آید</p></div></div>