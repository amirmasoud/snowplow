---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>به جز فکر وصال دلبر ای دل!</p></div>
<div class="m2"><p>مده راه خیال دیگر ای دل!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز رها کن هر دو گیتی</p></div>
<div class="m2"><p>یکی گیر از دو عالم بگذر ای دل</p></div></div>