---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>مکحل چشم شورانگیز کرده</p></div>
<div class="m2"><p>چو شیرین عشوه با پرویز کرده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن فایز چو فرهاد کمر کن</p></div>
<div class="m2"><p>لگدکوب سم شبدیز کرده</p></div></div>