---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>ذلیل و خوار و زارم کردی آخر</p></div>
<div class="m2"><p>پریشان روزگارم کردی آخر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فایز بستی اول عهد و پیمان</p></div>
<div class="m2"><p>به غربت رهسپارم کردی آخر</p></div></div>