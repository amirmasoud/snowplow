---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>زهجرت سینه و دامن کنم چاک</p></div>
<div class="m2"><p>روان سازم سرشک از دیده بر خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز سر بر لحد ننهاده فایز</p></div>
<div class="m2"><p>که مهرش را ز دل بنموده ای پاک</p></div></div>