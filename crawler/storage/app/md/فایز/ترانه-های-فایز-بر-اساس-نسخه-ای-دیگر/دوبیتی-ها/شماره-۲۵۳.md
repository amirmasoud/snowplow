---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>سحر! امشب دمی بر جای خود باش</p></div>
<div class="m2"><p>که من ترسانم از تو همچو خفاش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهل فایز دمی آسوده خوابد</p></div>
<div class="m2"><p>مکش تیغ و دل غمدیده مخراش</p></div></div>