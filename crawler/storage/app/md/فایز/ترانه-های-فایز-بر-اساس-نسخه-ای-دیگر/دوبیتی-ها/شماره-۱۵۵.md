---
title: >-
    شمارهٔ ۱۵۵
---
# شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>عسل از معدن زنبور خیزد</p></div>
<div class="m2"><p>کلام الله ز کوه طور خیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نشنیده ای بشنو تو فایز!</p></div>
<div class="m2"><p>به قبرت یا محمد نور خیزد</p></div></div>