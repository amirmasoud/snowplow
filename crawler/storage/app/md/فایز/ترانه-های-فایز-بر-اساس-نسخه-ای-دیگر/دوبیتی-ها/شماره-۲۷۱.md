---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>قدت گل، قامتت گل، کفش پا گل</p></div>
<div class="m2"><p>سخن گل، معرفت گل، مدعا گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گل چیدن در آمد یار فایز</p></div>
<div class="m2"><p>سر و گردن گل و نشو و نما گل</p></div></div>