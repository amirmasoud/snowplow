---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>نه هر دل عشق جانان قابل افتد</p></div>
<div class="m2"><p>نه این قرعه به نام هر دل افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران دل بباید تا یکی دل</p></div>
<div class="m2"><p>چو فایز در محبت کامل افتد</p></div></div>