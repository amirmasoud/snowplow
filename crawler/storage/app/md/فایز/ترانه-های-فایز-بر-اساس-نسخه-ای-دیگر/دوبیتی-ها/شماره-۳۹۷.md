---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>ذلیل و زار و بیمارم نمودی</p></div>
<div class="m2"><p>به پیش دشمنان خوارم نمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتی برقع از رو پیش فایز</p></div>
<div class="m2"><p>به تار مو گرفتارم نمودی</p></div></div>