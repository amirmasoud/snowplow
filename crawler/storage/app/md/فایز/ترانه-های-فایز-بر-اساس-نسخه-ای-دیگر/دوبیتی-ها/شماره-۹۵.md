---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>بت زورق نشینم در امان باد</p></div>
<div class="m2"><p>خدایش از بلایا حرز جان باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دریا باد فایز یارش الیاس</p></div>
<div class="m2"><p>به صحرا خضر باوی همعنان باد</p></div></div>