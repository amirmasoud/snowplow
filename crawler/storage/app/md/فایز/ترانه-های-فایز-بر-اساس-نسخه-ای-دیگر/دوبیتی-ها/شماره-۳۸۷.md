---
title: >-
    شمارهٔ ۳۸۷
---
# شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>صنم سرخیل ترسازادگانی</p></div>
<div class="m2"><p>مهی اما نه اندر آسمانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مسیحا مردگان را زنده می کرد</p></div>
<div class="m2"><p>تو هم جان می دهی هم جان ستانی</p></div></div>