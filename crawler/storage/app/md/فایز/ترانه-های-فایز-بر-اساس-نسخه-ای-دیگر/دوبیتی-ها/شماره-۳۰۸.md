---
title: >-
    شمارهٔ ۳۰۸
---
# شمارهٔ ۳۰۸

<div class="b" id="bn1"><div class="m1"><p>اگر جانان به هنگام تکلم</p></div>
<div class="m2"><p>دهد بیجاده را اذن تبسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شکر خنده ای فایز تواند</p></div>
<div class="m2"><p>کند احیا همه اموات مردم</p></div></div>