---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>به تیرم زد کمان افکند در پشت</p></div>
<div class="m2"><p>که یعنی من ندانم کی تو را کشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خود اثبات کردی قتل فایز</p></div>
<div class="m2"><p>به خونم کرده ای رنگین سرانگشت</p></div></div>