---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>سخن آهسته تر گو دلبر این جاست</p></div>
<div class="m2"><p>بت حوراوش مه پیکر این جاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگر! قد و جمال یار فایز</p></div>
<div class="m2"><p>گل این جا سرو این جا عبهر این جاست</p></div></div>