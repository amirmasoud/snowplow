---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>شب هجران مرا جان بر لب آورد</p></div>
<div class="m2"><p>فلک از گردش افتد کاین شب آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب آمد فایز دل خسته از نو</p></div>
<div class="m2"><p>به خاطر یار سیمین غبغب آورد</p></div></div>