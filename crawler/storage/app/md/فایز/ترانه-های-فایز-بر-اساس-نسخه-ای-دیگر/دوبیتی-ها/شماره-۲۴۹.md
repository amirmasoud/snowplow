---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>نگفتم دل از آن گیسو بپرهیز؟</p></div>
<div class="m2"><p>تو زخم آلوده با غیری میامیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز گرفته خو به زلفش</p></div>
<div class="m2"><p>شب است این زلف و دل مرغ شباویز</p></div></div>