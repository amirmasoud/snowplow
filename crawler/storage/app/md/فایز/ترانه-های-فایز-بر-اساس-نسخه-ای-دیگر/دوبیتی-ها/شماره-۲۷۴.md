---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>سحر با باغبانی گفت بلبل</p></div>
<div class="m2"><p>که گر مردم تو باری بی تامل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز برگ نسترن بهرم کفن ساز</p></div>
<div class="m2"><p>بکن دفنم به زیر شاخه گل</p></div></div>