---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>به دارالملک تن دل پادشاهست</p></div>
<div class="m2"><p>جوارح در اطاعت چون سپاهست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر جا عزم دارد شاه فایز</p></div>
<div class="m2"><p>که را یارا که گوید این نه راهست</p></div></div>