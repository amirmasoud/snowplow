---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>زاسب افتادم و شد کار مشکل</p></div>
<div class="m2"><p>سراپا معجری شد در مقابل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزیر معجرش آهسته می گفت</p></div>
<div class="m2"><p>« که فایز رفت و داغش ماند بر دل»</p></div></div>