---
title: >-
    شمارهٔ ۳۴۶
---
# شمارهٔ ۳۴۶

<div class="b" id="bn1"><div class="m1"><p>مده یادم ز روز وصل جانان</p></div>
<div class="m2"><p>مده بر یاد آدم باغ رضوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذشت ایام وصل دوست فایز!</p></div>
<div class="m2"><p>ندارد سود دیگر آه و افغان</p></div></div>