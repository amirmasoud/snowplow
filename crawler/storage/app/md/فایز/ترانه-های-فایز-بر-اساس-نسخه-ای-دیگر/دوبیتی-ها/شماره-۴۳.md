---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>مرا در پیش راهی پر زبیم است</p></div>
<div class="m2"><p>از این ره در دلم خوفی عظیم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو فایز میندیش از مهابت</p></div>
<div class="m2"><p>که آن جا حکم با رب رحیم است</p></div></div>