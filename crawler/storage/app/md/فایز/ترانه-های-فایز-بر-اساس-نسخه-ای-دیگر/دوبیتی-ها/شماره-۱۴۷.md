---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>وفا دخلی به محبوبی ندارد</p></div>
<div class="m2"><p>جفاهم بیش از این خویی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتا زخم دلم را مرهمی نه</p></div>
<div class="m2"><p>که فایز صبر ایوبی ندارد</p></div></div>