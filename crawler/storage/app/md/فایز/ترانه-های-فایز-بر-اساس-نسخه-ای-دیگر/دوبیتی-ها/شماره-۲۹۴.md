---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>چو آید فکر یار اندر ضمیرم</p></div>
<div class="m2"><p>بسوزد خرمن ماه از نفیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه فایز پیر عمر ماه و سالست</p></div>
<div class="m2"><p>غم هجران جانان کرده پیرم</p></div></div>