---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>حصار دوست گر ز آهن بر آرند</p></div>
<div class="m2"><p>به دورش خندقی از اخگر آرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رساند خود به یار خویش فایز</p></div>
<div class="m2"><p>به راهش گر هزاران لشکر آرند</p></div></div>