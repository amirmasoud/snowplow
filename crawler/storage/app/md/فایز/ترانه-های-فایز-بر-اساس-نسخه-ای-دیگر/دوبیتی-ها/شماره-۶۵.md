---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>بتا از کجرویهایت شکایت</p></div>
<div class="m2"><p>ولی با کس نگویم این حکایت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر در کلبه فایز نهی گام</p></div>
<div class="m2"><p>کنم جانم نثار خاک پایت</p></div></div>