---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>مرا یاران وصیت این چنین است</p></div>
<div class="m2"><p>که در هر جا که آن جانان مکین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دوش آن جا برید تابوت فایز</p></div>
<div class="m2"><p>که جای تربتم آن سرزمین است</p></div></div>