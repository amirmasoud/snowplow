---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>سحر از بس که نالیدم ز هجران</p></div>
<div class="m2"><p>بر احوالم ترحم کرد جانان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرامان مو پریشان سویم آمد</p></div>
<div class="m2"><p>به فایز بست از نو عهد و پیمان</p></div></div>