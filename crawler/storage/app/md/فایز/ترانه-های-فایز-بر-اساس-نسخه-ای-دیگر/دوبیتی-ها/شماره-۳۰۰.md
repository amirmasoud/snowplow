---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>مگر یار آمده بر پشت بامم</p></div>
<div class="m2"><p>که بوی جنت آید بر مشامم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرود آ گر چه فایز نیست قابل</p></div>
<div class="m2"><p>بیا بنشین نگه دار احترامم</p></div></div>