---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>مرا شب سیل آه از دل بر آید</p></div>
<div class="m2"><p>که یادم از دو زلف دلبر آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشیند چشم در ره فایز هر شب</p></div>
<div class="m2"><p>که شاید یارم از سویی در آید</p></div></div>