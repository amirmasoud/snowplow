---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>بیا جانا که دنیا را وفا نیست</p></div>
<div class="m2"><p>جوی راحت در این محنت سرانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این ره هر چه فایز دیده بگشود</p></div>
<div class="m2"><p>زهمراهان اثر جز نقش پا نیست</p></div></div>