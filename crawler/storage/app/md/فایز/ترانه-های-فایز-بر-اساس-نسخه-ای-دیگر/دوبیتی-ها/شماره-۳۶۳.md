---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>به هر محفل که آن دلبر نشسته</p></div>
<div class="m2"><p>تو گویی حور با زیور نشسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میان جمع خوبان یار فایز</p></div>
<div class="m2"><p>چو مه در مجمع اختر نشسته</p></div></div>