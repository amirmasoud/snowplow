---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>شب آمد تا شب وصل آردم یاد</p></div>
<div class="m2"><p>دهد خاک وجودم جمله بر باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یقین می سوخت فایز ز آتش خشم</p></div>
<div class="m2"><p>نمی کردش گر آب دیده امداد</p></div></div>