---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>بیا تا گویمت شرح جدایی</p></div>
<div class="m2"><p>حدیث صبر و سوز و بی نوایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو خود گفتی که:«آیم سوی فایز»</p></div>
<div class="m2"><p>ولی ترسم بمیرم تو نیای</p></div></div>