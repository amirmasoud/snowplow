---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>نه افلاطون کند فکری به حالم</p></div>
<div class="m2"><p>نه جالینوس داند من چه حالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علاج درد فایز نیست درمان</p></div>
<div class="m2"><p>بیا مطرب بزن تا من بنالم</p></div></div>