---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>دو زلفانت فتاده چون سر مار</p></div>
<div class="m2"><p>نمایان می کنی هر دم به دلدار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر خواهی که فایز بنده گردد</p></div>
<div class="m2"><p>نقاب از چهره مهتاب بردار</p></div></div>