---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>بلندی سیر عالم می کنم من</p></div>
<div class="m2"><p>به جای عیش ماتم می کنم من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفیقان دور فایز جمع گردید !</p></div>
<div class="m2"><p>که فردا درد سر کم می کنم من</p></div></div>