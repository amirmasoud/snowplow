---
title: >-
    شعری در قالب غزل در مدح امام حسین(ع)
---
# شعری در قالب غزل در مدح امام حسین(ع)

<div class="b" id="bn1"><div class="m1"><p>ای نام تو آرایش هر مسجد و منبر</p></div>
<div class="m2"><p>وای ذکر تو زینت ده هر محفل و محضر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی نام تو مسجد چه بود، طرح مهندس</p></div>
<div class="m2"><p>بی ذکر تو محفل چه بود، نقش مصور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفاق پر از زمزمه نام تو باشد</p></div>
<div class="m2"><p>نام تو شفابخش همه عاجز مضطر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تیغ تو در کرب و بلا جلوه نمی کرد</p></div>
<div class="m2"><p>تا حشر بدی خلق جهان یکسره کافر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن جلوه که شمشیر علی کرد به خندق</p></div>
<div class="m2"><p>ضرب تو فزونتر بود از ضربت حیدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زیرا که نبی بود و علی بود و سپاهی</p></div>
<div class="m2"><p>عمرو آمد و تنها به علی گشت برابر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه شهدا یک تنه با خلق جهانی</p></div>
<div class="m2"><p>آن کرد که حیدر نکند در صف خیبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد از آن دم که گرفت او به کمر دست</p></div>
<div class="m2"><p>دیدش که فتاده به زمین نعش برادر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خود شد و از اسب بیفتاد به زاری</p></div>
<div class="m2"><p>زآنسان که برفت طاقت و هوش از تن و از سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای ماه بنی هاشم و ای صف شکن من</p></div>
<div class="m2"><p>ای در همه احوال مرا مونس و یاور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فایز! به عزای شه لب تشنه فغان کن</p></div>
<div class="m2"><p>تا شافع جرم تو شود در صف محشر</p></div></div>