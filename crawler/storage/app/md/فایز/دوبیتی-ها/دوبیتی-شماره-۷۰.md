---
title: >-
    دوبیتی شمارهٔ ۷۰
---
# دوبیتی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>گذشت ایام گل ای بلبل زار</p></div>
<div class="m2"><p>بکن چون من ز هجران ناله بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل تو سر زند هر ساله از نو</p></div>
<div class="m2"><p>گل فایز نمی‌روید دگر بار</p></div></div>