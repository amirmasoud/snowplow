---
title: >-
    دوبیتی شمارهٔ ۱۲
---
# دوبیتی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>اگر از رخ براندازی نقابت</p></div>
<div class="m2"><p>کنند مردم خیال آفتابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این پوشیده‌ای رخ، یار فایز</p></div>
<div class="m2"><p>که ترسی شب کسی بیند به خوابت؟</p></div></div>