---
title: >-
    دوبیتی شمارهٔ ۱۱۲
---
# دوبیتی شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>نمی‌بینم ز مردم آشنایی</p></div>
<div class="m2"><p>نمی‌آید ز کس بوی وفایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مده فایز به وصل گلرخان دل</p></div>
<div class="m2"><p>که آخر می‌کشندت از جدایی</p></div></div>