---
title: >-
    دوبیتی شمارهٔ ۷۹
---
# دوبیتی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>مکش سرمه به چشم نازپرور</p></div>
<div class="m2"><p>مکن روز مرا از شب سیه‌تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌سوزد دلت از بهر فایز</p></div>
<div class="m2"><p>چه خواهی گفت فردا روز محشر؟</p></div></div>