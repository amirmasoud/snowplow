---
title: >-
    دوبیتی شمارهٔ ۱۰۵
---
# دوبیتی شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>غم و غصه تن و جانم گرفته</p></div>
<div class="m2"><p>فراق یا دامانم گرفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کشتی اجل فایز سوار است</p></div>
<div class="m2"><p>میان آب، طوفانم گرفته</p></div></div>