---
title: >-
    دوبیتی شمارهٔ ۱۰۶
---
# دوبیتی شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>پری‌رویان به ما کردند نظاره</p></div>
<div class="m2"><p>یکی چون ماه و باقی چون ستاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمان ابرو و مژگان تیز کردند</p></div>
<div class="m2"><p>زدند بر جان فایز چون هزاره</p></div></div>