---
title: >-
    دوبیتی شمارهٔ ۲۱
---
# دوبیتی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>سر زلف تو جانا لام و میم است</p></div>
<div class="m2"><p>چو بسم الله الرحمن الرحیم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هفتاد و دو ملت برده حسنت</p></div>
<div class="m2"><p>قدم از هجر تو مانند جیم است</p></div></div>