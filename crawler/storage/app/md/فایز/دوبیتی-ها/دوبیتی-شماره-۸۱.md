---
title: >-
    دوبیتی شمارهٔ ۸۱
---
# دوبیتی شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>دل من در خم زلفت گره گیر</p></div>
<div class="m2"><p>چنان شیری که افتاده به زنجیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترحم کن دمی بر فایز زار</p></div>
<div class="m2"><p>مگر در مذهبت کردم چه تقصیر</p></div></div>