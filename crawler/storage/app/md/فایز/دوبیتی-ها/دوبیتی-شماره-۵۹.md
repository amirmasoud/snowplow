---
title: >-
    دوبیتی شمارهٔ ۵۹
---
# دوبیتی شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>بهشت از روی تو بهتر نباشد</p></div>
<div class="m2"><p>ز حوران حسن تو کمتر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن لعل لب و دندان، فایز</p></div>
<div class="m2"><p>یقین دارم که در کوثر نباشد</p></div></div>