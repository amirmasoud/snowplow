---
title: >-
    دوبیتی شمارهٔ ۴۰
---
# دوبیتی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>اگر دورم من از تو ای پریزاد</p></div>
<div class="m2"><p>فراموشم نکن زنهار زنهار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان عهدی که با تو بست فایز</p></div>
<div class="m2"><p>وفادارم اگر هستی وفادار</p></div></div>