---
title: >-
    دوبیتی شمارهٔ ۱
---
# دوبیتی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>مرا خلد برین دی بودیم جا</p></div>
<div class="m2"><p>کنونم دوزخ است امروز ماوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمانده دی نماند فایز امروز</p></div>
<div class="m2"><p>خدا داند چه باشد حال فردا</p></div></div>