---
title: >-
    دوبیتی شمارهٔ ۹۹
---
# دوبیتی شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>دو گیسو را به دوش انداختی تو</p></div>
<div class="m2"><p>ز ملک دل دو لشکر ساختی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به استمداد چشم و زلف و رخسار</p></div>
<div class="m2"><p>به یکدم کار فایز ساختی تو</p></div></div>