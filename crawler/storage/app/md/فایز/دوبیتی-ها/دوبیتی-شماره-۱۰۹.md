---
title: >-
    دوبیتی شمارهٔ ۱۰۹
---
# دوبیتی شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>بتا در گوش، گوش آویز داری</p></div>
<div class="m2"><p>لب می‌گون شکّرریز داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشارت می‌دهد این دل به فایز</p></div>
<div class="m2"><p>دلی در سینه مهرانگیز داری</p></div></div>