---
title: >-
    دوبیتی شمارهٔ ۶۲
---
# دوبیتی شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>صنم عشق تو همچون نار نمرود</p></div>
<div class="m2"><p>مرا در منجنیق عشق فرسود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلیل آسا رود فایز در آتش</p></div>
<div class="m2"><p>تو قل یا نار کونی برد کن زود</p></div></div>