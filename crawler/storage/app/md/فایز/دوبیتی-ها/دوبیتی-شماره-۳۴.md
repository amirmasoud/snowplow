---
title: >-
    دوبیتی شمارهٔ ۳۴
---
# دوبیتی شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>بیا تا برگ گل نارفته بر باد</p></div>
<div class="m2"><p>گلی چینیم و بنشینیم دلشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بت فایز مکن تاخیر چندان</p></div>
<div class="m2"><p>که تعجیل است عمر آدمیزاد</p></div></div>