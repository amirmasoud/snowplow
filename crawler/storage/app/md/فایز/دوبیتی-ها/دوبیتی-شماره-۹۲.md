---
title: >-
    دوبیتی شمارهٔ ۹۲
---
# دوبیتی شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>مرو ای جان شیرین از بر من</p></div>
<div class="m2"><p>توقف کن که آید دلبر من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بده فایز به تلخی جان شیرین</p></div>
<div class="m2"><p>که جانانت بگیرد سر به دامن</p></div></div>