---
title: >-
    دوبیتی شمارهٔ ۵۲
---
# دوبیتی شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>اگر آهی کشم افلاک سوزد</p></div>
<div class="m2"><p>در و دشت و بیابان پاک سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر آهی کشد فایز از این دل</p></div>
<div class="m2"><p>یقین دارم گل نمناک سوزد</p></div></div>