---
title: >-
    دوبیتی شمارهٔ ۱۰۳
---
# دوبیتی شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>نسیم آهسته آهسته سحرگاه</p></div>
<div class="m2"><p>روان شو سوی یار از راه و بیراه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجنبان حلقه زنجیر زلفش</p></div>
<div class="m2"><p>ز حال زار فایز سازش آگاه</p></div></div>