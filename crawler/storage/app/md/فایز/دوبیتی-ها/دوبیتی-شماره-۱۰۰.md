---
title: >-
    دوبیتی شمارهٔ ۱۰۰
---
# دوبیتی شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>نسیم، امشب عجب دفع غمی تو</p></div>
<div class="m2"><p>یقین دارم نه از این عالمی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمیم زلف یار فایزستی</p></div>
<div class="m2"><p>و یا ز انفاس بن مریمی تو؟</p></div></div>