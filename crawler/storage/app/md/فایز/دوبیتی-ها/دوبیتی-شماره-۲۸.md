---
title: >-
    دوبیتی شمارهٔ ۲۸
---
# دوبیتی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>رخت تا در نظر می‌آرم ای دوست</p></div>
<div class="m2"><p>خودم را زنده می‌پندارم ای دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی چون تو برفتی یار فایز</p></div>
<div class="m2"><p>بگو این دل به کی بسپارم ای دوست</p></div></div>