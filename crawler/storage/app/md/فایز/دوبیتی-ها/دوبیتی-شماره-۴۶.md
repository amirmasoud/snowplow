---
title: >-
    دوبیتی شمارهٔ ۴۶
---
# دوبیتی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>مرا هم ساق و هم زانو کند درد</p></div>
<div class="m2"><p>کمر با ساعد و بازو کند درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر عضو تو فایز پیری آمد</p></div>
<div class="m2"><p>جوانی رفت و جای او کند درد</p></div></div>