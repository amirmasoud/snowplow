---
title: >-
    دوبیتی شمارهٔ ۴۳
---
# دوبیتی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>به دل گفتم مکن اینقدر فریاد</p></div>
<div class="m2"><p>که اندر خرمن صبر آتش افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسوزد هستی فایز، سراپا</p></div>
<div class="m2"><p>گهی کان چشم شهلا آیدم یاد</p></div></div>