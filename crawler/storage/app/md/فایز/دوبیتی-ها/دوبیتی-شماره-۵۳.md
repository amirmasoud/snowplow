---
title: >-
    دوبیتی شمارهٔ ۵۳
---
# دوبیتی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>دل من همچو هدهد در سبا شد</p></div>
<div class="m2"><p>خیالم چون سلیمان در قفا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل فایز ز استحضار بلقیس</p></div>
<div class="m2"><p>مثال آصف بن برخیا شد</p></div></div>