---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>ای خاک در تو سرمه بینائی</p></div>
<div class="m2"><p>افسوس که بعد از این جهان پیمائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لشکر همه در شهر فرود آمد و من</p></div>
<div class="m2"><p>در خانه زین بماندم از بیجائی</p></div></div>