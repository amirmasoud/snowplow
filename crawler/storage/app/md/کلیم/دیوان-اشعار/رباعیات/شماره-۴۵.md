---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>افسوس که جمعیت از احوالم رفت</p></div>
<div class="m2"><p>شیرازه اوراق مه و سالم رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من بلبل بینوایم از بی برگی</p></div>
<div class="m2"><p>هم گلشن رفت و هم پر و بالم رفت</p></div></div>