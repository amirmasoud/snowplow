---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>در معرکه این تفنگ فریادرس است</p></div>
<div class="m2"><p>خصم افکن و گرم خوی و آتش نفس است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موقوف اشاره ایست در کشتن خصم</p></div>
<div class="m2"><p>سویش نگهی ز گوشه چشم بسست</p></div></div>