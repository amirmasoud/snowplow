---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>بنگی عربی سوار جمازه چرت</p></div>
<div class="m2"><p>از خویش سفر کند به اندازه چرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز نگسیخت چرتش از چرت دگر</p></div>
<div class="m2"><p>بستست ز تار مژه شیرازه چرت</p></div></div>