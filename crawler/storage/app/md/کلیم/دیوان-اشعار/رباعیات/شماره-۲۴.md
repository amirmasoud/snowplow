---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>قسمت کردند ماه و خور بی کم و بیش</p></div>
<div class="m2"><p>بر خود الم شهنشه عدل اندیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برداشت بمنت مه نو ضعفش را</p></div>
<div class="m2"><p>خورشید پسندید تبش بر تن خویش</p></div></div>