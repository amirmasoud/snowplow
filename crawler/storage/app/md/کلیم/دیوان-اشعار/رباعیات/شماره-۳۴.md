---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>شاهی که حمایت خدایش سپر است</p></div>
<div class="m2"><p>مایل به سپر نه بهر دفع ضرر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هیچ مصاف رو نمی گرداند</p></div>
<div class="m2"><p>منظور شجاعتش ازین رهگذرست</p></div></div>