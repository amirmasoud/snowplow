---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>در بادیه گر دو گام بی آب شوی</p></div>
<div class="m2"><p>بیدرد چرا اینهمه بیتاب شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آبله پای تو یکره خاری</p></div>
<div class="m2"><p>سیراب نشد چرا تو سیراب شوی</p></div></div>