---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ذاتت که زمجموعه گل منتخب است</p></div>
<div class="m2"><p>حرف تب و لرز او خطائی عجبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کس موج محیط را نگوید لرز است</p></div>
<div class="m2"><p>کی گرمی خورشید جهانتاب تب است</p></div></div>