---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>شیرینم و مغز سخنانم تلخ است</p></div>
<div class="m2"><p>عیش همه عالم از زبان تلخ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منهم از خویش در عذابم که مدام</p></div>
<div class="m2"><p>از گفتن حرف حق دهانم تلخ است</p></div></div>