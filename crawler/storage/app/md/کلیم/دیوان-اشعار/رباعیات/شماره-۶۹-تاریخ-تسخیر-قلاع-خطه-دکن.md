---
title: >-
    شمارهٔ ۶۹ - تاریخ تسخیر قلاع خطهٔ دکن
---
# شمارهٔ ۶۹ - تاریخ تسخیر قلاع خطهٔ دکن

<div class="b" id="bn1"><div class="m1"><p>از جلوه شاهدان فرخ پی فتح</p></div>
<div class="m2"><p>داد از پی هم ساقی دوران می فتح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاریخ فتوحات شهنشاه جهان</p></div>
<div class="m2"><p>کلکم بنوشت (آمده فتح از پی فتح)</p></div></div>