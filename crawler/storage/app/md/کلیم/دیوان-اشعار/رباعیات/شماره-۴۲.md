---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>شد تنگ ز کم ظرفی ما مشرب جام</p></div>
<div class="m2"><p>مشکل که دگر سیر کند کوکب جام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آید بفقان ز دست بد مستی ما</p></div>
<div class="m2"><p>انگشت زند اگر کسی بر لب جام</p></div></div>