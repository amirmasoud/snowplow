---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>کس نیست درین زمانه غمخوار کسی</p></div>
<div class="m2"><p>دوریست که کس نمی شود یار کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون ناخن سرش سزای تیغست</p></div>
<div class="m2"><p>هر کس گرهی گشاید از کار کسی</p></div></div>