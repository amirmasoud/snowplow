---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>ای شوخ بغمزه بر سر جنگ مباش</p></div>
<div class="m2"><p>وی گل ز خزان حسن بیرنگ مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمشیر که زنگش بزدایند خوشست</p></div>
<div class="m2"><p>ابروی تو گر ریخته دلتنگ مباش</p></div></div>