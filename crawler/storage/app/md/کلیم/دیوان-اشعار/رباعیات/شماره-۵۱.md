---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>حافظ چو بنغمه روح فرسا افتد</p></div>
<div class="m2"><p>در سیر مقامات که از پا افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز در ره آهنگ بهر سوی رود</p></div>
<div class="m2"><p>چون آب که از جوی بصحرا افتد</p></div></div>