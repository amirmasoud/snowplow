---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>شاه از حسب و نسب شه شاهانست</p></div>
<div class="m2"><p>یک یک اجداد او سکندر شانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرزندی او نام پدر کرده بلند</p></div>
<div class="m2"><p>چون ابر که روشناس از بارانست</p></div></div>