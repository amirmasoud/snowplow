---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>با ما کین سپهر و انجم پیداست</p></div>
<div class="m2"><p>ناسازی بخت بی ترحم پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خشکی آشیانه در گلبن سبز</p></div>
<div class="m2"><p>بیبرگی ما میان مردم پیداست</p></div></div>