---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>آنکس که ترا رخصت میخواری داد</p></div>
<div class="m2"><p>صیقل پی آئینه هشیاری داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا باده ز کم حوصلگان رسوا شد</p></div>
<div class="m2"><p>از موج بمستان خط بیزاری داد</p></div></div>