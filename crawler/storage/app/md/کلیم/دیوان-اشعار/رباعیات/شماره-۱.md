---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>هر چند که مرد قول و فعلش تبه است</p></div>
<div class="m2"><p>برداشتن پرده ز کارش گنه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسوا شود آنکه می درد پرده خلق</p></div>
<div class="m2"><p>زر قلب در آید و محک روسیه است</p></div></div>