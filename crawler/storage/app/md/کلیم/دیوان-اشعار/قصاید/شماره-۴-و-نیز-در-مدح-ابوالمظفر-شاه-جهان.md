---
title: >-
    شمارهٔ ۴ - و نیز در مدح ابوالمظفر شاه جهان
---
# شمارهٔ ۴ - و نیز در مدح ابوالمظفر شاه جهان

<div class="b" id="bn1"><div class="m1"><p>سحاب آراست باغ و بوستانرا</p></div>
<div class="m2"><p>فدای باغبان کن خان و مان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همان آبی که گرد از روی گل شست</p></div>
<div class="m2"><p>بدینسان مست دارد بلبلان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان گلبن گرانبارست از گل</p></div>
<div class="m2"><p>که بلبل بست بر خاک آشیان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگلشن می کش و بر خود مخندان</p></div>
<div class="m2"><p>دهان غنچه پر زعفران را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مروت خانه زاد می فروشست</p></div>
<div class="m2"><p>که ارزان می دهد رطل گران را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصحرا سیر چون آبروان کن</p></div>
<div class="m2"><p>بدست خود نگردانی عنان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین موسم که صحراها بهشتست</p></div>
<div class="m2"><p>بفرزندان رها کن خانمان را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل و لاله که می غلطد بر خاک</p></div>
<div class="m2"><p>تنگ ظرفند گویا، بوستان را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بروی سبزه می غلطد بنفشه</p></div>
<div class="m2"><p>عجب پیری که می مالد جوان را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگلشن عمر جدول خوش گذشته</p></div>
<div class="m2"><p>همیشه گرچه بر لب دیده جان را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چمن با آنکه همکار مسیحاست</p></div>
<div class="m2"><p>نمی بندد رعاف ارغوان را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>قبای تنگ بر تن چاک گردد</p></div>
<div class="m2"><p>شکافد گل حصار گلستان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بهم آمیزش گلها جنانست</p></div>
<div class="m2"><p>که گلدسته نخواهد ریسمان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دم کوره ز تأثیر رطوبت</p></div>
<div class="m2"><p>زند آب آتش آهنگران را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز پهلو بوریا گردد نشان دار</p></div>
<div class="m2"><p>رطوبت آب داد از بس جهان را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هوای برشکالی مومیائیست</p></div>
<div class="m2"><p>ز گلبن شاخ بشکن امتحان را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهار گلشن فردوس خواهی</p></div>
<div class="m2"><p>درین موسم ببین هندوستان را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کند دندان مارش کار شبنم</p></div>
<div class="m2"><p>فزون باد ایمنی دارالامان را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خوشا ملکی که از یک آب شمشیر</p></div>
<div class="m2"><p>گل و لاله دمد سنگ فسان را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بیکدم ز آب پیکان می شود سبز</p></div>
<div class="m2"><p>هدف سازد کسی گر استخوان را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بس موج رطوبت اوج دارد</p></div>
<div class="m2"><p>دماند خوشه کاه کهکشان را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نهفته جاده ها در زیر سبزه</p></div>
<div class="m2"><p>ز مسطر خط بپوشاند نشان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستون خانه ها شد سبز و قمری</p></div>
<div class="m2"><p>فرامش کرده سرو بوستان را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خس و خاشاک زانسان سبز گردید</p></div>
<div class="m2"><p>که بلبل می کند، گم آشیان را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خطیب فاخته بر منبر سرو</p></div>
<div class="m2"><p>مناقب خوان بود شاه جهان را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زبان سبزه در هر سرزمین گفت</p></div>
<div class="m2"><p>ثنای ثانی صاحبقران را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فلک از کوکب بختش عزیزست</p></div>
<div class="m2"><p>ز آئینه است قدر آئینه دان را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدورانش که ایام نشاطست</p></div>
<div class="m2"><p>جرس در خنده می پیچد فغان را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز عید وزن، شاهنشاه عالم</p></div>
<div class="m2"><p>جهان اندوخت عیش جاودان را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو نوبت رخصت پابوس دارد</p></div>
<div class="m2"><p>بساط پادشاه کامران را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز رشک کفه هایش بسکه داغند</p></div>
<div class="m2"><p>نماند رنگ بر رو اختران را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بعهدش آنچنان در خواب امنست</p></div>
<div class="m2"><p>که باید پاسبانی پاسبان را</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بملکش راهزن، مانند جاده</p></div>
<div class="m2"><p>بمنزل می رساند کاروان را</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بعهد عدل او واپس ستاند</p></div>
<div class="m2"><p>چمن از خاک زرهای خزان را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بعهدش عدل کسری هر که سنجید</p></div>
<div class="m2"><p>بهم سنجید قصاب و شبان را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آن بزمی که خلقش میزبانست</p></div>
<div class="m2"><p>طفیلی داغ دارد میهمان را</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بدوران تمیزش همچو لاله</p></div>
<div class="m2"><p>نگردد زیر دست آتش دخان را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>مدد نیروی اقبالش نخواهد</p></div>
<div class="m2"><p>نمی تابد فسان تیغ زبان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز حفظش پیره زال چرخ ازین پس</p></div>
<div class="m2"><p>بدوک شمع ریسد ریسمان را</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر از آستانش سر بتابند</p></div>
<div class="m2"><p>گریبان طوق گردد سرکشان را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز خورشید ضمیرش پرتوی دان</p></div>
<div class="m2"><p>صفای خاطر اشراقیان را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ضعیفان را چو در زنهار گیرد</p></div>
<div class="m2"><p>سحاب روی مه سازد کتان را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر چه احتشامش همچو گردون</p></div>
<div class="m2"><p>مسخر کرد سرتاسر جهان را</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شکوهش هند را خوش کرده مسکن</p></div>
<div class="m2"><p>بشب روشن شود شان آسمان را</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>قلم چون قصه رزمش نویسد</p></div>
<div class="m2"><p>کند از خون رقم سر دوستان را</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نیاید زخم تیغ او فراهم</p></div>
<div class="m2"><p>برای خنده دارد گل دهان را</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز زخمش پوست بر اندام دشمن</p></div>
<div class="m2"><p>نموداری بود توز کمان را</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سنانش کنجکاوی چون کند سر</p></div>
<div class="m2"><p>چو نی مغزی نماید استخوان را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سلیمانی چشم دشمنانش</p></div>
<div class="m2"><p>دو خاتم باشد انگشت سنان را</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زبان را درفشانی از کف اوست</p></div>
<div class="m2"><p>ز ابرست آب در جو ناودان را</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز سر حد مکان، خیمه برون زد</p></div>
<div class="m2"><p>عطایش تا که گیرد لامکان را</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قلم در وصف جودش جای نگذاشت</p></div>
<div class="m2"><p>به پشت و روی طومار زبان را</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کفش پرداخت کان گوهر و زر</p></div>
<div class="m2"><p>فلک برچید آخر این دکان را</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نباشد وعده چون بی انتظاری</p></div>
<div class="m2"><p>پسندیده عطای بیگمان را</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درون شیشه افلاک بیند</p></div>
<div class="m2"><p>بسان می فضای آسمان را</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همیشه با ترازو تا بود کار</p></div>
<div class="m2"><p>سبکبار و گران سنگ جهان را</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بغیر از دشمنش گردون نه بیند</p></div>
<div class="m2"><p>سبکباری بخاطرها گران را</p></div></div>