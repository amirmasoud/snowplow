---
title: >-
    شمارهٔ ۳ - در مدح صاحبقران ثانی ابوالمظفر شاه جهان
---
# شمارهٔ ۳ - در مدح صاحبقران ثانی ابوالمظفر شاه جهان

<div class="b" id="bn1"><div class="m1"><p>در دور ما زمانه گلستان بی درست</p></div>
<div class="m2"><p>عیش رسا چو رزق مقرر مقدرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون غنچه انبساط جلبیست خلق را</p></div>
<div class="m2"><p>شیرینی طرب شکر شیر مادرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها همین نه از لب ساغر موظفیم</p></div>
<div class="m2"><p>انعام ما هم از لب ساقی مقررست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مست می نشاط بود، روزگار ما</p></div>
<div class="m2"><p>از مست هر مراد که خواهی میسرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آواز پای و کف زدن مطربان یکیست</p></div>
<div class="m2"><p>از بس زمین بسان زمان عیش پرورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چسبان شد اختلاط مرادات با حصول</p></div>
<div class="m2"><p>عشرت بدل زیاده بجام آشناترست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامان دل بدست کدورت نمی رسد</p></div>
<div class="m2"><p>مابین رنگ و آینه سد سکندرست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم تمام همچو سرمست پرنشاط</p></div>
<div class="m2"><p>دوران همه فرح چو دل کیمیاگرست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دریاکشان محفل عیش و نشاط را</p></div>
<div class="m2"><p>از شش جهت گشایش درهای ششدرست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این خوان عشرتی که بعالم کشیده اند</p></div>
<div class="m2"><p>از عید وزن پادشه هفت کشورست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صاحبقران ثانی شاه جهان پناه</p></div>
<div class="m2"><p>شاهی که آفتابش یک لعل افسرست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میزان وزن تا صدف آفتاب شد</p></div>
<div class="m2"><p>از قدر یکسرش بدو دنیا برابرست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پابوس پادشه که شهانرا نداده دست</p></div>
<div class="m2"><p>سالی دو نوبتش ز سعادت میسرست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آسمان زخاک در او نشان دهند</p></div>
<div class="m2"><p>جائیکه آفتاب کرم ذره پرورست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گردون اگرچه بر ره او پیر گشته است</p></div>
<div class="m2"><p>سرگرم تر بخدمتش از تازه چاکرست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دریا چو در متاع گهر آب می کند</p></div>
<div class="m2"><p>لرزان ز بیم پادشه عدل پرورست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون مرغ خانه بال و پرش دست دیگریست</p></div>
<div class="m2"><p>گردون ز یمن اختر بختش توانگرست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>رسم حساب بهر شمار مه است و سال</p></div>
<div class="m2"><p>در عهد همتش که سپر کیله زرست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شهباز قدس را پر پرواز فطرتش</p></div>
<div class="m2"><p>چون استخوان سینه قفس جزو پیکرست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از یمن ایمنی زمانش سفینه را</p></div>
<div class="m2"><p>در بحر سنگ راهی اگر هست لنگرست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هرگز باوج مدحت او ره نبرده است</p></div>
<div class="m2"><p>با آنکه مرغ دفتر من جمله تن پرست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دانی که چیست در کفت این تیغ شعله بار</p></div>
<div class="m2"><p>برقی که در کمین سیاهی لشگرست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>افتد شراره غضبت گر بجوی تیغ</p></div>
<div class="m2"><p>بیم کباب گشتن ماهی جوهرست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>یک چین بود ولایت خاقان ز آستینش</p></div>
<div class="m2"><p>آن جامه ای که بر قد ملکت مقررست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بهر شکار ماهی فتح و ظفر بود</p></div>
<div class="m2"><p>جوهر در آب تیغ تو گر دامگسترست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دریاست مومیائی کشتی چو بشکند</p></div>
<div class="m2"><p>باد مراد همتت آنجا که یاورست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از صفحه مدیح تو طوطی خامه را</p></div>
<div class="m2"><p>آئینه نشاط ابد در برابرست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دلخواه طبع فوج معانی همی رسد</p></div>
<div class="m2"><p>جائیکه عرض مدح تورای سخنورست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر لحظه کوچه قلمم بسته می شود</p></div>
<div class="m2"><p>تنگست شارعی که گذرگاه لشگرست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لب تشنگان عفوت سیرآب از آن شوند</p></div>
<div class="m2"><p>در جنت طبیعت تو حلم کوثرست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پیوسته تا که از پی تحریر جزو عیش</p></div>
<div class="m2"><p>نی شکل خامه دارد و قانون چو مستطرست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>پر نغمه نشاط بود بزم عشرتت</p></div>
<div class="m2"><p>تا مهر و مه جلاجل این کهنه چنبرست</p></div></div>