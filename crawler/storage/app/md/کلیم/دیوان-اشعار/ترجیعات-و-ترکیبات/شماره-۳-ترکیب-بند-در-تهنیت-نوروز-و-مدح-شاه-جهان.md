---
title: >-
    شمارهٔ ۳ - ترکیب بند در تهنیت نوروز و مدح شاه جهان
---
# شمارهٔ ۳ - ترکیب بند در تهنیت نوروز و مدح شاه جهان

<div class="b" id="bn1"><div class="m1"><p>باد نوروزی ببستان مژده ها آورده است</p></div>
<div class="m2"><p>بلبلان را مایه برگ و نوا آورده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل چها دربار خواهد داشت کز فیض بهار</p></div>
<div class="m2"><p>هر کجا خاریست یک گلشن صفا آورده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه می آرد بهار از دیگری زیباترست</p></div>
<div class="m2"><p>رونمائی از برای رونما آورده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقلان را تا درین موسم چو خود دیوانه دید</p></div>
<div class="m2"><p>بید مجنون سجده شکری بجا آورده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر متاعی را خریداریست در بازار عشق</p></div>
<div class="m2"><p>گل همه برگ از سفر بلبل نوا آورده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شود نظارگی بیگانه هوش و خرد</p></div>
<div class="m2"><p>دیده نرگس نگاه آشنا آورده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نو عروس لاله را وقف حنا بندان رسید</p></div>
<div class="m2"><p>در میان، گل خورده خود را بجا آورده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خار گشت از چرب نرمی رشته گلدسته ها</p></div>
<div class="m2"><p>باغبان این سازگاری از کجا آورده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یاسمن در محشر نشو و نمای بوستان</p></div>
<div class="m2"><p>نامه ای چون روی ارباب وفا آورده است</p></div></div>
<div class="b2" id="bn10"><p>زاری بلبل ز شوق گل بود پوشیده نیست</p>
<p>سبزه را مژگان تر یارب ز شوق نام کیست</p></div>
<div class="b" id="bn11"><div class="m1"><p>بهر ضبط گوهر شبنم که زیب گلشن است</p></div>
<div class="m2"><p>غنچه سرتاپا گریبان گل سراپا دامنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یوسف گل در میان عصمت و تر دامنیست</p></div>
<div class="m2"><p>کز پس و پیشست هر چاکی که در پیراهنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون نسوزد آتش غیرت سراپا شمع را</p></div>
<div class="m2"><p>کز پر پروانه در پای شقایق خرمنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر بپیش افکنده نرگس فکر اینش برده است</p></div>
<div class="m2"><p>کافتاب لاله چون دایم بشب آبستنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رنگ و بوی لاله و گل را چه می سنجی بهم</p></div>
<div class="m2"><p>از تن بیجان بسی ره تا بجان بی تنست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مفلسان باغ را بین غرق انعام بهار</p></div>
<div class="m2"><p>سبزه کاتش زسر نگذشته سرو گلشنست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در لباس این رخصت عیش است کز نقاش صنع</p></div>
<div class="m2"><p>لاله ساغر پیکر و نرگس صراحی گردنست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست شبنم اینکه تیغش را مرصع ساخته</p></div>
<div class="m2"><p>حرف شادابی گلشن بر زبان سوسنست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سرو چون طاووس می بودی زپای خود خجل</p></div>
<div class="m2"><p>گرنه جوش سبزه ساقش را بجای دامنست</p></div></div>
<div class="b2" id="bn20"><p>سبزه را از بسکه سعی نامیه افراخته</p>
<p>در چمن هر دم غلط کرده بسروش فاخته</p></div>
<div class="b" id="bn21"><div class="m1"><p>گل گرو از روی لیلی برده از خوش منظری</p></div>
<div class="m2"><p>سبزه چون مژگان مجنون می نماید از تری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باغبان چون دید لطف طبع موزونان باغ</p></div>
<div class="m2"><p>سرو را خضری تخلص داد و گلرا آذری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نوبهار از بسکه آمد مهربان در روزگار</p></div>
<div class="m2"><p>غنچه را آورد باز از صنعت پیکانگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقده ها از بسکه وا شد زانبساط روزگار</p></div>
<div class="m2"><p>مشکل ار از دست آید بازی انگشتری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غنچه و مینای می در کار هم خوش می کنند</p></div>
<div class="m2"><p>بیزبان خنیاگری، بیدست پیراهن دری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زآب و رنگ لاله و گل نهرها بینی روان</p></div>
<div class="m2"><p>در جهان آرا بپهنای خیابان هری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روز تا شب لاله زارش در نظر دارد سپهر</p></div>
<div class="m2"><p>شام بردارد از آنرو نسخه نیک اختری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>لطف خاک گلشنش زانسانکه از آسیب پا</p></div>
<div class="m2"><p>گشته گلهای زمینش سربسر نیلوفری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تحفه دریا و کان مخزون جیب غنچه است</p></div>
<div class="m2"><p>در بغل دارد دکان خویشتن را جوهری</p></div></div>
<div class="b2" id="bn30"><p>برگ ناخن گشت و وا کرد از نقاب غنچه بند</p>
<p>پرده تقوی چو گل باید بیک جانب فکند</p></div>
<div class="b" id="bn31"><div class="m1"><p>شمع گلبن هر قدر برگی که برمی آورد</p></div>
<div class="m2"><p>از پی پروانه خود بال و پر می آورد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ناله بلبل ز بس در مغز گل جا کرده است</p></div>
<div class="m2"><p>گر کسی گل را ببوید دردسر می آورد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرو یک یک راز گلشن را بگوش ابر گفت</p></div>
<div class="m2"><p>هر چه آنجا گفته او باران خبر می آورد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باغبان چشمش ز انوار تجلی پر شدست</p></div>
<div class="m2"><p>یوسف گلرا از آن تاب نظر می آورد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سبزه را چون جوهر تیغست سجاده بر آب</p></div>
<div class="m2"><p>پاکدامن رخت خود از آب بر می آورد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خار اگر بر روی بلبل می کشد تیغ جفا</p></div>
<div class="m2"><p>در میان گل از وفاداری سپر می آورد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چشم بر راه بهار موکب شاهنشهیست</p></div>
<div class="m2"><p>سرو کز دیوار گلشن سربدر می آورد</p></div></div>
<div class="b2" id="bn38"><p>ظل حق صاحبقران ثانی و شاه جهان</p>
<p>رازدان آفرینش کار آگاه جهان</p></div>
<div class="b" id="bn39"><div class="m1"><p>ای بصورت پادشاه پادشاهان آمده</p></div>
<div class="m2"><p>وز ره معنی بعالم قطب دوران آمده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گوهر از رشک کلامت می درد بر خویشتن</p></div>
<div class="m2"><p>زان صدف را جبه دایم بی گریبان آمده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا نسیمی از قبولت بر گلستانها وزید</p></div>
<div class="m2"><p>خار با گل خوش نما، چون چشم و مژگان آمده</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>میتراود از بلندیهای قدرت همچو ابر</p></div>
<div class="m2"><p>کز پی اصلاح حال زیردستان آمده</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از سرایتهای خلقت در سیاستگاه قرب</p></div>
<div class="m2"><p>زخم تیغت همچو گل خونریز و خندان آمده</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از پی دریوزه گوهر زدست و تیغ تو</p></div>
<div class="m2"><p>زخم شمشیرت چو گل وا کرده دامان آمده</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>می شود سویش روان تیغ زبانها همچو موج</p></div>
<div class="m2"><p>بسکه با طبعت سخن با آب حیوان آمده</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جوی آب زندگی با عرض فیض دست تو</p></div>
<div class="m2"><p>تنگ میدان تر بسی از نهر شریان آمده</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از نثار عالم بالاست ما را هرچه هست</p></div>
<div class="m2"><p>گر گهر اعلاست ور ادنا ز عمان آمده</p></div></div>
<div class="b2" id="bn48"><p>گرنه تیغت را خدا مفتاح هر فتح آفرید</p>
<p>می گشائی چون هزاران قلعه را از یک کلید</p></div>
<div class="b" id="bn49"><div class="m1"><p>ز آسمانت هر زمان امداد فتح دیگرست</p></div>
<div class="m2"><p>چون ظفر لشکر کشد اقبال تو سردفترست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کشته تیغ جهادت دیرتر جان می دهد</p></div>
<div class="m2"><p>تیغ روحش چون پرد کز خون پر و بالش پرست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چون نباشد در شکار مملکتها تیز پر</p></div>
<div class="m2"><p>تیغ اقبال تو شهباز ظفر را شهپرست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>هر چه از رایت رسد خورشید بردارد بتن</p></div>
<div class="m2"><p>آینه روئین تنست و عاجز روشنگرست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>ساحل دریای جودت از وفور تشنگان</p></div>
<div class="m2"><p>پایمال آرزو چون آبگاه لشکرست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>انتظام کار و بار روزگار از عدل تست</p></div>
<div class="m2"><p>خط اگر کرسی نشین شد هم ز سعی مسطرست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر رقم از جوهر تیغت گواه نصرتست</p></div>
<div class="m2"><p>هست مضمونش یکی صد خط اگر بر محضرست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در بدن جائیکه گم کرده است خصم از هیبت</p></div>
<div class="m2"><p>جسته اندر کوچه تیغت که آن روشنترست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>صاحب بحر و بری از روی استحقاق وارث</p></div>
<div class="m2"><p>دشمنت را چشم و لب قسمت ازین خشک و ترست</p></div></div>
<div class="b2" id="bn58"><p>سایه پروردگاری، آفتاب عدل و داد</p>
<p>تا بقای صاحب سایه است عمر سایه باد</p></div>
<div class="b" id="bn59"><div class="m1"><p>پادشاها شمع تیغت آفتاب آثار باد</p></div>
<div class="m2"><p>بر زبانش هر چه گفتارست آن کردار باد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>صفحه هر سینه کز مهر تو چون خورشید نیست</p></div>
<div class="m2"><p>نزد اهل دل چو تقویم کهن بیکار باد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خواه روز و خواه شب از بهر پاس دولتت</p></div>
<div class="m2"><p>دیده اقبال چون چشم زره بیدار باد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بر جراحتها که خصم از ناوکت برداشته</p></div>
<div class="m2"><p>سبزه تیغت بجای مرهم زنگار باد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>هر که چون گل نشکفد در نوبهار عدل تو</p></div>
<div class="m2"><p>گریه اش لاینقطع چون خنده سوفار باد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>از تف دل شمع گردد گر همه مژگان خصم</p></div>
<div class="m2"><p>از نهیبت همچنان عالم بچشمش تار باد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا نشان خار و گل باشد ببستان سخن</p></div>
<div class="m2"><p>تیغ خورشید سخن خار سر دیوار باد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بر سر هر ماه تا گردون زند گل از هلال</p></div>
<div class="m2"><p>هر سر سال از گل فتح نوت گلزار باد</p></div></div>