---
title: >-
    غزل شمارهٔ  ۴۶۵
---
# غزل شمارهٔ  ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>غم مسکن و فکر مأوا ندارم</p></div>
<div class="m2"><p>عجب نیست گر در دلی جا ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درین بحر از خجلت تنگ ظرفی</p></div>
<div class="m2"><p>حبابم که چشمی ببالا ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکفته رخ از فقر همچون سرابم</p></div>
<div class="m2"><p>ترشروئی ابر و دریا ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرد چیست از فکر دنیا گذشتن</p></div>
<div class="m2"><p>نگوئی که من عقل دنیا ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا در غم ماست پیوسته زلفت</p></div>
<div class="m2"><p>در آن کوچه من خانه تنها ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنونم دل از سنگ طفلان فکندست</p></div>
<div class="m2"><p>ز شرمندگی روی صحرا ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گدای در دلبرانم چو شانه</p></div>
<div class="m2"><p>بجای دگر دست گیرا ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بآئینه زانوی خویش گاهی</p></div>
<div class="m2"><p>سری می کشم روی درها ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نخواهد رسیدن بمقصود دستم</p></div>
<div class="m2"><p>اگر آبله در ته پا ندارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلیم از سر آرزوها گذشتم</p></div>
<div class="m2"><p>گواهم که بر بخت دعوا ندارم</p></div></div>