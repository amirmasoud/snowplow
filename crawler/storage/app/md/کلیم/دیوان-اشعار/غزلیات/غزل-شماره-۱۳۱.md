---
title: >-
    غزل شمارهٔ  ۱۳۱
---
# غزل شمارهٔ  ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>اگر ز هستی ما نام بینشانی هست</p></div>
<div class="m2"><p>در آشیان هما مشت استخوانی هست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمال اختر بختم نمی شود زایل</p></div>
<div class="m2"><p>چو شمع دایم در طالعم زیانی هست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بیزبانی ما را حریف حرف نئی</p></div>
<div class="m2"><p>بداد ما برس ایشوخ تا زبانی هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تهی ز لخت جگر نیست اشک ماهرگز</p></div>
<div class="m2"><p>همیشه قافله را میر کاروانی هست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسیکه مایل خونریزی است می فهمم</p></div>
<div class="m2"><p>میانه دل و مژگان او نشانی هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رود به سیر چمن برق بیشتر زسحاب</p></div>
<div class="m2"><p>مگر بشاخ گلی از من آشیانی هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجود خاک درت با سر بریده خوشست</p></div>
<div class="m2"><p>که هیچ باک نباشد چو پاسبانی هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلیم دل بهمین قرب بیوصال منه</p></div>
<div class="m2"><p>چه شد که در پس دیوار گلستانی هست</p></div></div>