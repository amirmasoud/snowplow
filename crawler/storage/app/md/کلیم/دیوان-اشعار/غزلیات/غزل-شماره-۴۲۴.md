---
title: >-
    غزل شمارهٔ  ۴۲۴
---
# غزل شمارهٔ  ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>تا من از صیقل می آینه روشن کردم</p></div>
<div class="m2"><p>شیشه را شمع ره شیخ و برهمن کردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب آهن همه از دیده زنجیر چکید</p></div>
<div class="m2"><p>بسکه چون سلسله در بند تو شیون کردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لایق برق نشد باد هم از ننگ نبرد</p></div>
<div class="m2"><p>کشته های عمل خویش چو خرمن کردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان طالع خاکستر صیقل دارم</p></div>
<div class="m2"><p>خود سیه روز و هزار آینه روشن کردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنج تاریک من از چشم بد روزن دور</p></div>
<div class="m2"><p>با خیال تو در او دست بگردن کردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همتم آتش داغ از در همسایه نخواست</p></div>
<div class="m2"><p>من دیوانه از آن جای بگلخن کردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاغذ گرده شد از سوزن مژگان تو دل</p></div>
<div class="m2"><p>رنگش از سرمه آن نرگس پرفن کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای یک خار نه در پای و نه در دامن ماند</p></div>
<div class="m2"><p>چشم بد دور که خوش غارت گلشن کردم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرصت دوختن چاک دلم نیست کلیم</p></div>
<div class="m2"><p>تیغ برداشته تا رشته بسوزن کردم</p></div></div>