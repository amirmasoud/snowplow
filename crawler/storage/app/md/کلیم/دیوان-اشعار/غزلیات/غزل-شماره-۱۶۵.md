---
title: >-
    غزل شمارهٔ  ۱۶۵
---
# غزل شمارهٔ  ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>فراق همنفسان جان بیقرارم سوخت</p></div>
<div class="m2"><p>گیاه خشکم و هجران نوبهارم سوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو من مباد کس آواره هزار وطن</p></div>
<div class="m2"><p>فلک ز داغ فراقت هزار بارم سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زمانه از شب تارم چراغ باز گرفت</p></div>
<div class="m2"><p>پس از وفات من آورده بر مزارم سوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرشک راه بدامن نبرد در شب هجر</p></div>
<div class="m2"><p>چو شمع لخت جگر گرچه بر کنارم سوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبیب مرده دلان بعد مرگ مشفق شد</p></div>
<div class="m2"><p>بوعده کرد وفا چون در انتظارم سوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا جدائی جانان دگر نکشت کلیم</p></div>
<div class="m2"><p>چه منت است تف آه شعله بارم سوخت</p></div></div>