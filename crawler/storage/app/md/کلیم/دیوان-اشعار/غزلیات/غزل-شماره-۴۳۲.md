---
title: >-
    غزل شمارهٔ  ۴۳۲
---
# غزل شمارهٔ  ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>بسکه از بار غم دهر گرانبار شدم</p></div>
<div class="m2"><p>همه رو سجده کنان تا در خمار شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیشه پیچ دل از مستی من خود نشکست</p></div>
<div class="m2"><p>من باین دل شکنان از چه گرفتار شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرم از ابر بهاری نشدم طالع بین</p></div>
<div class="m2"><p>که درین باغ چو خار سر دیوار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم آئینه دگر روی بمن ننماند</p></div>
<div class="m2"><p>بسکه از زشتی خود بر دل خود تار شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی ایدل زغم تنگدهانان زاری</p></div>
<div class="m2"><p>من بتنگ آمدم از وضع تو بیزار شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد عمریکه بخواب من بیدل آمد</p></div>
<div class="m2"><p>گریه آبی برخم ریخت که بیدار شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتم از هوش مکن مستم ازین بیش کلیم</p></div>
<div class="m2"><p>چشم بردار از آن چشم که از کار شدم</p></div></div>