---
title: >-
    غزل شمارهٔ  ۵۷۴
---
# غزل شمارهٔ  ۵۷۴

<div class="b" id="bn1"><div class="m1"><p>از فیض دل ار گوهر شب تاب نباشی</p></div>
<div class="m2"><p>چون خاک بهر جا که روی باب نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناخوانده مرو بر در کس تا زگرانی</p></div>
<div class="m2"><p>بار دل یک شهر چو سیلاب نباشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگشای زبان به ز خودی را چو به بینی</p></div>
<div class="m2"><p>زنهار که شمع شب مهتاب نباشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جائیکه رفیقان چو جرس خواب ندارند</p></div>
<div class="m2"><p>باری تو چنان کن که گران خواب نباشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی لاف توکل ببغل گر ننهی نان</p></div>
<div class="m2"><p>آنروز کم از ماهی بی آب نباشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که توئی خود سبب کلفت خویشی</p></div>
<div class="m2"><p>می کوش که در عالم اسباب نباشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسایش دیوانگی ایدل مده از دست</p></div>
<div class="m2"><p>یعنی پی وادیدن احباب نباشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حلقه زنار فسادی ندهد روی</p></div>
<div class="m2"><p>پرهیز که در حلقه اصحاب نباشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زنهار وفا را غرض آلود نسازی</p></div>
<div class="m2"><p>در کوی توقع سگ قصاب نباشی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حیفست کلیم از تو که بیدجله اشکی</p></div>
<div class="m2"><p>یکتا گهری بهر چه شاداب نباشی</p></div></div>