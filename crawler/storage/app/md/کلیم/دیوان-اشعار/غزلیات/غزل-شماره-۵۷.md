---
title: >-
    غزل شمارهٔ  ۵۷
---
# غزل شمارهٔ  ۵۷

<div class="b" id="bn1"><div class="m1"><p>هرگز دلت نشان گذار وفا نداشت</p></div>
<div class="m2"><p>سنگی که ره فتاد بر او نقش پا نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از هجوم درد تو شرمندگی کشید</p></div>
<div class="m2"><p>ویرانه حیف درخور سیلاب جا نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمعم ز باد دامن فانوس می کشد</p></div>
<div class="m2"><p>آن محنتی که در ره باد صبا نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هایهای گریه من تا دلش گرفت</p></div>
<div class="m2"><p>دیگر چو آب تیغ، سر شکم صدا نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سینه خط زخم چو خوانا نوشته شد</p></div>
<div class="m2"><p>داغ ارچه بود حاجت این نقطه ها نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی هزار بار اگر گریه دیده را</p></div>
<div class="m2"><p>میشست بیتو خانه چشمم صفا نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز خاک کوی دوست که نتوان ازو گذشت</p></div>
<div class="m2"><p>از چاک سینه بستن خونم دوا نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر آب ودانه در قفس مرغ دل نبود</p></div>
<div class="m2"><p>صیاد را چو جرم قفس این فضا نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گریه ام که زیب عروسان گلشنست</p></div>
<div class="m2"><p>پای گلی نبود که رنگ حنا نداشت</p></div></div>