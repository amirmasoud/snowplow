---
title: >-
    غزل شمارهٔ  ۴۷۵
---
# غزل شمارهٔ  ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>باین دماغ که از سایه اجتناب کنیم</p></div>
<div class="m2"><p>بر آن سریم که تسخیر آفتاب کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگریه سحری سعی بیش ازین خوش نیست</p></div>
<div class="m2"><p>چه لایقست که در شیر صبح آب کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود بصیر بدل عجز چون کمال گرفت</p></div>
<div class="m2"><p>گذشت از آنکه توانیم اضطراب کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شور ناله بود جمله بیقراری اشک</p></div>
<div class="m2"><p>نمی گذارد کاین طفل را بخواب کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفینه می رود این سعی ناخدا عبث است</p></div>
<div class="m2"><p>چو عمر می گذرد ما چرا شتاب کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای خانه ناموس و ننگ دلگیر است</p></div>
<div class="m2"><p>خوش آنکه بر سر عقل این بنا خراب کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام سوخته جان راست تاب آتش ما</p></div>
<div class="m2"><p>بآه سرد دلی را مگر کباب کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیمن عشق ز خاک وجود می سازیم</p></div>
<div class="m2"><p>گلی که غازه رخسار آفتاب کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود کلیم که باز از نشان دندانها</p></div>
<div class="m2"><p>برای بوسه لبی چند انتخاب کنیم</p></div></div>