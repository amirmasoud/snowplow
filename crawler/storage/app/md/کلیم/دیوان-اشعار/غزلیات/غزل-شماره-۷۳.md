---
title: >-
    غزل شمارهٔ  ۷۳
---
# غزل شمارهٔ  ۷۳

<div class="b" id="bn1"><div class="m1"><p>زان سینه چه راحت که ره زخم بدر نیست</p></div>
<div class="m2"><p>بادی نخورد بر دل اگر خانه دو در نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این همه تنگی که نصیب دهن اوست</p></div>
<div class="m2"><p>داغم که چرا روزی ارباب هنر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمت غم آن زلف سیه روز ندارد</p></div>
<div class="m2"><p>از ماتم همسایه درین خانه خبر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خضر مکش منت بیجا، بره عشق</p></div>
<div class="m2"><p>کز بحر ره قافله موج بدر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان غمزه بدل می رسدم از ره دیده</p></div>
<div class="m2"><p>صد زخم که در پیش رهش سینه سپر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چرخ چه مینالی اگر بخت نداری</p></div>
<div class="m2"><p>بی طالعی طفل ز تقصیر پدر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین صرفه که در طینت ایام سرشتند</p></div>
<div class="m2"><p>در باغ جهان مایه اگر هست ثمر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بار به دوزخ نگشائیم چه سازیم</p></div>
<div class="m2"><p>ما را که مطاعی بجز از هیزم تر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خاک وطن تخم مرادی نشود سبز</p></div>
<div class="m2"><p>بیهوده کلیم اینهمه سرگرم سفر نیست</p></div></div>