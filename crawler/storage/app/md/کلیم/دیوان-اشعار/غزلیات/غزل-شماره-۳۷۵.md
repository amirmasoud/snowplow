---
title: >-
    غزل شمارهٔ  ۳۷۵
---
# غزل شمارهٔ  ۳۷۵

<div class="b" id="bn1"><div class="m1"><p>شکفت غنچه و این عقده ام بدل جا کرد</p></div>
<div class="m2"><p>که دهر چون گره از کار بسته ای وا کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پسند خاطر یک تن نیم چه چاره کنم</p></div>
<div class="m2"><p>که بی نفاق بیکدل نمی توان جا کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکشوری که سر زلف ها پریشانست</p></div>
<div class="m2"><p>نمی توان سر شوریده مداوا کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه دشمنم برقیبان چرا بمن نرسید</p></div>
<div class="m2"><p>فلک وصال تو را گر نصیب اعدا کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسیکه مشق مدارائی از کمان گرفت</p></div>
<div class="m2"><p>بهیچ خصم نمی بایدش مدارا کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که دید دیده گریان من که گریه نکرد</p></div>
<div class="m2"><p>بغیر دوست که پنداشت سیر دریا کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بضبط دامنم اکنون سرشک تن ندهد</p></div>
<div class="m2"><p>که طفل خود سر عادت بسیر صحرا کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زطره تو هر آن عقده ایکه شانه گشاد</p></div>
<div class="m2"><p>بیادگاری زلف تو در دلم جا کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجور دوست که تن همچو ما نهاد کلیم</p></div>
<div class="m2"><p>بگل حساب شد ار خاک بر سرما کرد</p></div></div>