---
title: >-
    غزل شمارهٔ  ۵۲۷
---
# غزل شمارهٔ  ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>چیست کارم، زخم کاری هر زمان برداشتن</p></div>
<div class="m2"><p>وز خدنگ جور او زخم سنان برداشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کتاب صنع یزدانی خیانت کردنست</p></div>
<div class="m2"><p>سرو من جزو کمر را از میان برداشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توانم آب بردارم ز جوی کهکشان</p></div>
<div class="m2"><p>لیک نتوانم زخوان خلق نان برداشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ثبوت بردباری عاشقان را محضریست</p></div>
<div class="m2"><p>چون هدف از هر ستمکاری نشان برداشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کعبه عشق ترا خوف و خطر در راه نیست</p></div>
<div class="m2"><p>آب اگر نبود توان ریگ روان برداشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نباشم لایق برداشتن بفکن مرا</p></div>
<div class="m2"><p>یکرهم از خاک خواری می توان برداشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضعف در بیماری عشقت بجائی می رسد</p></div>
<div class="m2"><p>کاستین نتوان ز چشم خونفشان برداشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کامجویان را گریزی نیست از جور سپهر</p></div>
<div class="m2"><p>هر که گلچین بایدش از باغبان برداشتن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بردباری چیست جور از دشمنان بردن کلیم</p></div>
<div class="m2"><p>ورنه جان پروردنست از دوستان برداشتن</p></div></div>