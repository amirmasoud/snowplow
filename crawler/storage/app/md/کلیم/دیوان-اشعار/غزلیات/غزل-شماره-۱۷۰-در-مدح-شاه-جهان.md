---
title: >-
    غزل شمارهٔ  ۱۷۰ - در مدح شاه‌جهان
---
# غزل شمارهٔ  ۱۷۰ - در مدح شاه‌جهان

<div class="b" id="bn1"><div class="m1"><p>کردست تیغت از سر خصم ابتدای فتح</p></div>
<div class="m2"><p>اینست ابتدا چه بود انتهای فتح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای از ازل بقامت شمشیر نصرتت</p></div>
<div class="m2"><p>همچون غلاف آمده چسبان قبای فتح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمد ز بحر لطف الهی بدرگهت</p></div>
<div class="m2"><p>چون موج سوی ساحل فتح از قفای فتح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر دوش باد سیر کند همچو بوی گل</p></div>
<div class="m2"><p>در گلشن جهان خبر غمزدای فتح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرها که همچو غنچه نشکفته چیده اند</p></div>
<div class="m2"><p>مشت گلیست از چمن دلگشای فتح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آب کس بنا ننهد، دست قدرتت</p></div>
<div class="m2"><p>دایم بر آب تیغ گذارد بنای فتح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوفار را چو غنچه زبان تر زبان شود</p></div>
<div class="m2"><p>در عرصه ای نبرد ز شوق صلای فتح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مردان کار همچو نی تیر یک بیک</p></div>
<div class="m2"><p>پا کرده سخت و بسته کمر از برای فتح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ابرو مثال موی دمد از زبان تیغ</p></div>
<div class="m2"><p>از بس کند برای دلیران دعای فتح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ و سنان بحال زره رشک می برند</p></div>
<div class="m2"><p>کو از هزار دیده به بیند لقای فتح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلزار رزم شاه جهان پادشاه را</p></div>
<div class="m2"><p>آن بلبلم کلیم که دارم نوای فتح</p></div></div>