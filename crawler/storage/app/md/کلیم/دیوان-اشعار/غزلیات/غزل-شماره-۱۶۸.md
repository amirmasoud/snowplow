---
title: >-
    غزل شمارهٔ  ۱۶۸
---
# غزل شمارهٔ  ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>دجله اشک از بهار شوق طغیان کرده است</p></div>
<div class="m2"><p>رازهای سینه را خاشاک طوفان کرده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل گمان دارد که پوشیده است راز عشق را</p></div>
<div class="m2"><p>شمع را فانوس پندارد که پنهان کرده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد از حسن جهان آرای جانان می کند</p></div>
<div class="m2"><p>آنقدر ذوقی که دیوار گلستان کرده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت باران بکشت آرزویش می نهد</p></div>
<div class="m2"><p>غمزه ات گر خسته ایرا تیرباران کرده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می شود اول ستمگر کشته بیداد خویش</p></div>
<div class="m2"><p>سیل دایم بر سر خود خانه ویران کرده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلستان وفا، بلبل بگل هرگز نکرد</p></div>
<div class="m2"><p>آن نظربازی که چشمم با مغیلان کرده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ربط سرها ماند با زانوی غم دیگر سپهر</p></div>
<div class="m2"><p>هرکجا دیده است پیوندی پریشان کرده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف هندوی ترا از دلبری خط توبه داد</p></div>
<div class="m2"><p>کافری را کافر دیگر مسلمان کرده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فکر پرواز گلستان دارد اندرسر کلیم</p></div>
<div class="m2"><p>ساز راه گلشن کشمیر سامان کرده است</p></div></div>