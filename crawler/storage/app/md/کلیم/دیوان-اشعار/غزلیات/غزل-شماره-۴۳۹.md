---
title: >-
    غزل شمارهٔ  ۴۳۹
---
# غزل شمارهٔ  ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>دوش در خواب چو آن طره پیچان دیدم</p></div>
<div class="m2"><p>صبح در بستر خود سنبل و ریحان دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هواداری آنزلف چنانم که اگر</p></div>
<div class="m2"><p>برد خواب اجلم خواب پریشان دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایخوش آندم که زحیرت نزنم دیده بهم</p></div>
<div class="m2"><p>تا زدم چشم بهم آفت طوفان دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنچه از لشکر تاتار ندیدست کسی</p></div>
<div class="m2"><p>من زیک تار از آن زلف پریشان دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرد راه طلبم سرمه بینائی شد</p></div>
<div class="m2"><p>چمنی در دل هر خار مغیلان دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر صدق چو دستار بگردش گشتم</p></div>
<div class="m2"><p>گر سری خالی از اندیشه سامان دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که ز ابنای جهان است بمن حق دارد</p></div>
<div class="m2"><p>زانکه از چین جبین همه سوهان دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد ار منفتعی صحبت این چرخ چرا</p></div>
<div class="m2"><p>خضر را معتقد سیر بیابان دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>راست گویند بود توبه پشیمان بودن</p></div>
<div class="m2"><p>هر کرا دیدم، از توبه پشیمان دیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهر بر عکس توقع چو کند کار کلیم</p></div>
<div class="m2"><p>هر چه دشوار شمردم بخود آسان دیدم</p></div></div>