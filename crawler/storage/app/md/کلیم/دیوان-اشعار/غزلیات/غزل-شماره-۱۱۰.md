---
title: >-
    غزل شمارهٔ  ۱۱۰
---
# غزل شمارهٔ  ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>دلم با چشم تر یکرنگ از آنست</p></div>
<div class="m2"><p>که پای اشک خونین در میانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بآب تیغ او نازم که در خاک</p></div>
<div class="m2"><p>همان خونابه زخمش روانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه طفلست اینکه گاه مشق بیداد</p></div>
<div class="m2"><p>خطش زخمست و لوحش استخوانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهد از خاک ما فواره خون</p></div>
<div class="m2"><p>همین شمع مزار کشتگانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پر و بالم ز سنگ سردمهران</p></div>
<div class="m2"><p>زهم پاشیده تر از آشیانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان و دل یکی گر دست در عشق</p></div>
<div class="m2"><p>جرس را ناله پرتأثیر از آنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زگریه دامن ما گرچه دریاست</p></div>
<div class="m2"><p>ولی آلوده دامانی همانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین وادی منم درمانده، ورنه</p></div>
<div class="m2"><p>بمنزل رفته گر ریگ روانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بس در زیر بار لخت دل رفت</p></div>
<div class="m2"><p>نگه بر دیده ام بار گرانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اسیر تست دل ور خاک گردد</p></div>
<div class="m2"><p>غبار طره عنبر فشانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلیم از هند دلگیری ندارد</p></div>
<div class="m2"><p>پس از الفت قفس هم آشیانست</p></div></div>