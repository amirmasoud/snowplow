---
title: >-
    غزل شمارهٔ  ۵۱۴
---
# غزل شمارهٔ  ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>شب عیدست می باید در میخانه وا کردن</p></div>
<div class="m2"><p>بمی خشکی زهد روزه داران را دوا کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صراحی گر چنین پیوسته خواهد در سجود آمد</p></div>
<div class="m2"><p>بیک شب طاعت سی روز را خواهد قضا کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز ماه عید بی ابروی ساقی کار نگشاید</p></div>
<div class="m2"><p>بیک ناخن گره نتوان زکار عیش وا کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستم باشد کشیدن جام می را یکنفس بر سر</p></div>
<div class="m2"><p>بیکدم اینچنین آئینه ای را بی صفا کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیابی مستحق تر از من مخمور ای ساقی</p></div>
<div class="m2"><p>زکوة فطر می رطلی گران باید جدا کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خمار باده در چشمم سیه کردست عالمرا</p></div>
<div class="m2"><p>بیا ساقی که وقت شام باید روزه وا کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا بیتابی مژگان او می سوزد از غیرت</p></div>
<div class="m2"><p>زچشمانش جدا ناگشتن و رو بر قفا کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرو از ما برد در تیره روزی و پریشانی</p></div>
<div class="m2"><p>چرا زلفت بجد دارد شکست کار ما کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنان کز هر مژه ناید دواندن ریشه در دلها</p></div>
<div class="m2"><p>زهر چشمی نمی آید نگاه آشنا کردن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا هر بی بصیرت را رسد این کحل بینائی</p></div>
<div class="m2"><p>فلاطون می تواند خشت خم را توتیا کردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فزون از پایه خود هیچکس پستی نمی بیند</p></div>
<div class="m2"><p>فلک هرگز نخواهد نیشکر را بوریا کردن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درین دریای بی ساحل کلیم از من چه می آید</p></div>
<div class="m2"><p>زکار افتاده اینجا بازوی موج از شنا کردن</p></div></div>