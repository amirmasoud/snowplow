---
title: >-
    غزل شمارهٔ  ۴۲۸
---
# غزل شمارهٔ  ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>به دور خویش ز مینا حصار می‌خواهم</p></div>
<div class="m2"><p>در آن میانه ترا در کنار می‌خواهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به توبه نامه نمی‌شویَم از گُنَه که به حشر</p></div>
<div class="m2"><p>به کف مسوده زلف یار می‌خواهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو چشم حسرتم افتد به تیغ ابروی دوست</p></div>
<div class="m2"><p>یکی‌ست عمر و شهادت دوبار می‌خواهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به روی کار جهان رنگ دیگرم هوس است</p></div>
<div class="m2"><p>درین چمن نه خزان نه بهار می‌خواهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستم بود که گل زخم مشکبو نشود</p></div>
<div class="m2"><p>ز تار زلف تو یک بخیه‌وار می‌خواهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غبار اخگر دل را به آب نتوان برد</p></div>
<div class="m2"><p>نسیمی از سر زلف نگار می‌خواهم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سیل اشک سپردم سرای هستیِ خویش</p></div>
<div class="m2"><p>ز خود سفر چه کنم خانه‌دار می‌خواهم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غبار خاطر از آن می‌دهم به شِکوه برون</p></div>
<div class="m2"><p>که خاک بر سر این روزگار می‌خواهم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به بادیه نبرم گر کلیم را چه کنم؟</p></div>
<div class="m2"><p>برای مجنون شمع مزار می‌خواهم</p></div></div>