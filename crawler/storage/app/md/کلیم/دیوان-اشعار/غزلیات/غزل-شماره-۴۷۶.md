---
title: >-
    غزل شمارهٔ  ۴۷۶
---
# غزل شمارهٔ  ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>جان کاهدم چو حق سخن را ادا کنم</p></div>
<div class="m2"><p>گر نقد جان دهند سخن را بها کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عالمی مرا سر همخانگی کجاست</p></div>
<div class="m2"><p>کو مرگ تا که خلوت راحت جدا کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندانکه جای در دل آتش کند سپند</p></div>
<div class="m2"><p>خواهم که جا بخاطر آن بیوفا کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرگشتگی عجب بمیانم گرفته است</p></div>
<div class="m2"><p>دلدار در کنارم و رو در قفا کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از گریه دیده رفته زدست و بدست نیست</p></div>
<div class="m2"><p>غیر از غبار خاطر تا توتیا کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک بزم را ببوی سخن مست می کنم</p></div>
<div class="m2"><p>چون شیشه هر کجا که سر حرف وا کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سامان خونفشانی روز و شبم نماند</p></div>
<div class="m2"><p>دیگر باشک شام چو شمع اکتفا کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داروی یأس با همه دردی موافقست</p></div>
<div class="m2"><p>زین یک دوا هزار مرض را دوا کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن را چو در لباس قناعت بپرورم</p></div>
<div class="m2"><p>همچون غرابه پیرهن از بوریا کنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر هجو نیست در سخن من زعجر نیست</p></div>
<div class="m2"><p>حیف آیدم که زهر در آب بقا کنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنبیه منکران سخن می توان کلیم</p></div>
<div class="m2"><p>گر اژدهای خانه بآنها رها کنم</p></div></div>