---
title: >-
    غزل شمارهٔ  ۵۴۶
---
# غزل شمارهٔ  ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>دل از غم بیش و کم تقدیر گذشته</p></div>
<div class="m2"><p>وز نیک و بد عالم دلگیر گذشته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرواز وطن شیوه بال و پر من نیست</p></div>
<div class="m2"><p>عمرم بغریبی چو پر تیر گذشته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون در نگری در کف شوریدگی ماست</p></div>
<div class="m2"><p>سر رشته هر کار زتدبیر گذشته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز بافسون وفا پیش سلامست</p></div>
<div class="m2"><p>ترکی که زما دست بشمشیر گذشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه طلب همت این هر دو بلند است</p></div>
<div class="m2"><p>آهم ز اثر، اشک ز تأثیر گذشته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه دل و جان غمزه او زد بنگاهی</p></div>
<div class="m2"><p>یک ناوک کاری ز دو نخجیر گذشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خارم بجگر کاشته و داغ بسینه</p></div>
<div class="m2"><p>در دل چو گل و لاله کشمیر گذشته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در کوی جنون کلبه ما نیز نشان است</p></div>
<div class="m2"><p>گامی دو سه از خانه زنجیر گذشته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکباره کلیم از لب و دندان تو دل کند</p></div>
<div class="m2"><p>طفل هوسش زین شکر و شیر گذشته</p></div></div>