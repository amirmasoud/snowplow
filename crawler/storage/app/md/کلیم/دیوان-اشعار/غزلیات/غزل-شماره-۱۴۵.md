---
title: >-
    غزل شمارهٔ  ۱۴۵
---
# غزل شمارهٔ  ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>آن بلبلم که عقده دل دانه منست</p></div>
<div class="m2"><p>آبی که هست در قفسم آب آهنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طالع نگر که کشت امیدم ز آب سوخت</p></div>
<div class="m2"><p>در کشوری که برق هوادار خرمنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او را ز حال دیده حیران چه آگهی</p></div>
<div class="m2"><p>کی آفتاب را خبر از چشم روزنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلشن امید نچیدم اگر گلی</p></div>
<div class="m2"><p>از وصل خار صد گل جا کم بدامنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی چه سود، کاتش شوقت بما چه کرد</p></div>
<div class="m2"><p>احوال خانه سوخته بر خلق روشنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس مرا شناخت زهمراهیم رمید</p></div>
<div class="m2"><p>نشناخته است سایه هنوزم که با منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چار موسمش نبود رنگ و بو کلیم</p></div>
<div class="m2"><p>این عالم فسرده که نزد تو گلشنست</p></div></div>