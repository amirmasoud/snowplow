---
title: >-
    غزل شمارهٔ  ۲۹۹
---
# غزل شمارهٔ  ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>مرنج از کس که هر محنت که آید زآسمان آید</p></div>
<div class="m2"><p>همه تیر حوادث از کمان کهکشان آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلا هم پا بیفشارد چو جان سختانه پیش آید</p></div>
<div class="m2"><p>که پیکان بر نیاید زود چون بر استخوان آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دل تا لب ره گفتار را از گریه می بندم</p></div>
<div class="m2"><p>که می ترسم حدیث عشقت از لب بر زبان آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چراغ از حرف رخسار تو افروزند در مجلس</p></div>
<div class="m2"><p>حدیث زلف شبرنگ تو هر جا در میان آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخواهد سوخت عالم ز آتش پیکان او آخر</p></div>
<div class="m2"><p>چه خواهد شد اگر تیر مرادی بر نشان آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آن گلزار می نالم که اشک عندلیبانش</p></div>
<div class="m2"><p>اگر شبنم شود بر خاطر گلبن گران آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراپایم ز دردت آنچنان لبریز شیون شد</p></div>
<div class="m2"><p>که از مضراب مژگان تارا شکم در فغان آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دماغ عالمی از بوی زلف او چنان پر شد</p></div>
<div class="m2"><p>که در صحن چمن خون از دماغ ارغوان آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم از حرف تیغ او جراحت آب بردارد</p></div>
<div class="m2"><p>زبس هر زخم ها را آب حسرت در دهان آید</p></div></div>