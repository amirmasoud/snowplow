---
title: >-
    غزل شمارهٔ  ۱۵۴
---
# غزل شمارهٔ  ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>چاره خاموشی بود هر جا سخن در شیر نیست</p></div>
<div class="m2"><p>تیر بر سنگ آزمودن جز زیان تیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بخلق الفت نمی گیرم گناه من بدان</p></div>
<div class="m2"><p>طینت ابنای دهر از خاک دامن گیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواری و عزت درین محنت سرا یکسان بود</p></div>
<div class="m2"><p>آستان و مسندی در خانه زنجیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مادر گیتی که باشد نار پستان زین انار</p></div>
<div class="m2"><p>خون بود گر بهره ای طفلان شیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق و معشوق بی آمیزش هم ناقصند</p></div>
<div class="m2"><p>شاهد این مدعی به از کمان و تیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار فردا با کریمی دان که آن از شوق عفو</p></div>
<div class="m2"><p>عذرها را نشنود گر بدتر از تقصیر نیست</p></div></div>