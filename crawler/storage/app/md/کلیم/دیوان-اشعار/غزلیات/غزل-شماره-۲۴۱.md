---
title: >-
    غزل شمارهٔ  ۲۴۱
---
# غزل شمارهٔ  ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>زیک قطره سرشکم تن زجا شد</p></div>
<div class="m2"><p>بلی اشک از رخ من کهربا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمن نوبت نداد آنچشم پرحرف</p></div>
<div class="m2"><p>پس از عمریکه راه حرف وا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حنای پنجه قاتل نشد حیف</p></div>
<div class="m2"><p>که خونم آب از شرم بها شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه در طریق حق شناسی</p></div>
<div class="m2"><p>اگر گم گشت راه از رهنما شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیکتائی علم گردید زلفش</p></div>
<div class="m2"><p>بزیر بار دلها تا دو تا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیدم جز غبار خاطر از چرخ</p></div>
<div class="m2"><p>نصیبم کرد ازین نه آسیا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بافسر گر رسد رفعت نیابد</p></div>
<div class="m2"><p>سری کز کرسی زانو جدا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو ندهد فرصت بر خوردن از کام</p></div>
<div class="m2"><p>بگیر آن کام کز گردون جدا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم از ننگ عریانی برآمد</p></div>
<div class="m2"><p>تنش را جامعه نقش بوریا شد</p></div></div>