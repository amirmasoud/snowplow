---
title: >-
    غزل شمارهٔ  ۲۶۶
---
# غزل شمارهٔ  ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>دل تمنای درد او دارد</p></div>
<div class="m2"><p>خانه سیلاب آرزو دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خویش یکدیگرند عجز و غرور</p></div>
<div class="m2"><p>تیغ پیوند با گلو دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کنم شرح حال دیده رقم</p></div>
<div class="m2"><p>خامه ام گریه در گلو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کو بکو دربدر زبس گردید</p></div>
<div class="m2"><p>گریه در پیش ناله رو دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکزبانم من و نمی گویم</p></div>
<div class="m2"><p>سخنی را که پشت و رو دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم باریک بین اگر باشد</p></div>
<div class="m2"><p>قدح آفتاب مو دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عکس را نیست جا در آینه ام</p></div>
<div class="m2"><p>بدلم بسکه درد رو دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سر کوی میفروشانست</p></div>
<div class="m2"><p>گر کسی مغز در کدو دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرغبارست دل ز غمخواری</p></div>
<div class="m2"><p>خانه ام گرد رفت و رو دارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از مریدان آن در است کلیم</p></div>
<div class="m2"><p>خرقه داغ آرزو دارد</p></div></div>