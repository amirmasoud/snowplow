---
title: >-
    غزل شمارهٔ  ۴۴۵
---
# غزل شمارهٔ  ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>بی‌دماغم دست رد بر وصل جانان می‌نهم</p></div>
<div class="m2"><p>پنبه در گوش از صدای آب حیوان می‌نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهاری اینچنین از زهد خشک محتسب</p></div>
<div class="m2"><p>ساغرم تا تر شود در زیر دامان می‌نهم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه صراحی غلغلی دارد نه ساغر خنده‌ای</p></div>
<div class="m2"><p>گوش چندانی که بر بزم حریفان می‌نهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از کجا مرهم بیابم چون ز مغز استخوان</p></div>
<div class="m2"><p>پنبه می‌آرم به روی داغ حرمان می‌نهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نباشد یک گلستان خار پاانداز من</p></div>
<div class="m2"><p>کی ز کنج غم قدم در باغ و بستان می‌نهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پایه اهل هوس بالاتر است از من کلیم</p></div>
<div class="m2"><p>پای همت گرچه دائم بر سر جان می‌نهم</p></div></div>