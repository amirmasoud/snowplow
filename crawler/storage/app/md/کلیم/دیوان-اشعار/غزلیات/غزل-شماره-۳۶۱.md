---
title: >-
    غزل شمارهٔ  ۳۶۱
---
# غزل شمارهٔ  ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>کجاست بخت که تنگش کسی ببر گیرد</p></div>
<div class="m2"><p>نگین لعل لبش نقش بوسه بر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین که صحبت من با زمانه در نگرفت</p></div>
<div class="m2"><p>عجب که بر سر خاکم چراغ در گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر اشک کسی حال دل نمی داند</p></div>
<div class="m2"><p>همیشه طفل ز دیوانگان خبر گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همای تربیت عشق جانور کندش</p></div>
<div class="m2"><p>اگرچه بیضه فولاد زیر پر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه آن دهان بشکایت گشاده زخم ستم</p></div>
<div class="m2"><p>که سعی بخیه لبش را بیکدگر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آن نیم که کند یار اجتناب از من</p></div>
<div class="m2"><p>همیشه صحبت آتش بشمع در گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنای خانه آسودگی کلیم نهاد</p></div>
<div class="m2"><p>کزین خرابه همین خشت زیر سر گیرد</p></div></div>