---
title: >-
    غزل شمارهٔ  ۷
---
# غزل شمارهٔ  ۷

<div class="b" id="bn1"><div class="m1"><p>لب فرو بستم زیان دارد زبان‌دانی مرا</p></div>
<div class="m2"><p>چشم پوشیدم نمی‌زیبد عریانی مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شانه و زلف تو بادی می‌دهد از جان من</p></div>
<div class="m2"><p>بی‌تو زینسان در میان دارد پریشانی مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکته‌سنجی چیست عیب کس نفهمیدن بود</p></div>
<div class="m2"><p>می‌کند فهمیدگی تعلیم نادانی مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک دو گامی از سر کویش سفر خواهم گزید</p></div>
<div class="m2"><p>بازپس گر ناورد اشک پشیمانی مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بندگی را در ره خدمت ز بس شایسته‌ام</p></div>
<div class="m2"><p>می‌شود داغ غلامی خط پیشانی مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه خوارم عزتم این بس که در بیع نیاز</p></div>
<div class="m2"><p>می‌دهی خود را به من تا اینکه بستانی مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چنین از بار غم خواهم فرو رفتن به خود</p></div>
<div class="m2"><p>شمع‌سان آخر کند دامن گریبانی مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خرابی کس نمی‌گردد به گرد خانه‌ام</p></div>
<div class="m2"><p>پاسبانی نیست مشفق‌تر ز ویرانی مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روشناس ابر رحمت گشته‌ام از فیض او</p></div>
<div class="m2"><p>عاقبت آمد به کار آلوده‌دامانی مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرم کردم جای خود در گوشه گلخن کلیم</p></div>
<div class="m2"><p>کی دگر از جا برد تخت سلیمانی مرا</p></div></div>