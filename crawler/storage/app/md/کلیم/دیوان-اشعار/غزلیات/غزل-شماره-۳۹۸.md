---
title: >-
    غزل شمارهٔ  ۳۹۸
---
# غزل شمارهٔ  ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>چشم جادوی تو در دلجویی اهل نیاز</p></div>
<div class="m2"><p>هیچ کوتاهی ندارد عمر مژگانش دراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته جان و رگ دل در خم مژگان اوست</p></div>
<div class="m2"><p>هیچکس دیدی به یک مضراب بنوازد دو ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکسی سازی به ذوق خویشتن سر می‌کند</p></div>
<div class="m2"><p>دل میان مطربان خوش کرده یار دلنواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامه دیوانگی بر قد هرکس راست نیست</p></div>
<div class="m2"><p>از دوصد دیوانه یک تن نیست عریانی تراز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در قمار عشق بازی با تو نقشم خوش نشست</p></div>
<div class="m2"><p>چون؟ نباشد اینچنین تو پاک بر، من پاکباز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نشان خون ناحق کشتگان او را چه باک</p></div>
<div class="m2"><p>بال گنجشک است فرش آشیان شاهباز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نبود این تاج زرین بر سرش آسوده بود</p></div>
<div class="m2"><p>شمع افتاد از هوای سرفرازی در گداز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعر اگر وحی است محتاج سخن‌فهمان بود</p></div>
<div class="m2"><p>چون ممیز در میان نبود چه سود از امتیاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیشتر ما را کلیم آفت رسد ز ابنای جنس</p></div>
<div class="m2"><p>شیشه از سنگست و از وی بیش دارد احتراز</p></div></div>