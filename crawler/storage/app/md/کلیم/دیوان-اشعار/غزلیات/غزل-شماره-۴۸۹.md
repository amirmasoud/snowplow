---
title: >-
    غزل شمارهٔ  ۴۸۹
---
# غزل شمارهٔ  ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>ریخت ناخن بس که خار یأس از پا می‌کشم</p></div>
<div class="m2"><p>بر در دل می‌نشینم پا ز درها می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساعدم از زیر بار آستین بیرون نرفت</p></div>
<div class="m2"><p>چون نگویم دست همت را ز دنیا می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شحنه را بر من گرفتی، محتسب را دست نیست</p></div>
<div class="m2"><p>شیشه در بارم نباشد گرچه صهبا می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور من چون می‌رسد ساقی دو ساغر ده مرا</p></div>
<div class="m2"><p>می به یاد آن دو چشم مست شهلا می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حلقه‌ای در گوش بخت افکنده آن چشم سیاه</p></div>
<div class="m2"><p>کز نگاهش سرمه در چشم تماشا می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌رسد مستی به سرحدی که نشناسم ترا</p></div>
<div class="m2"><p>جام سرشار تغافل سخت تنها می‌کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنگ در دیوارها از شوخی طفلان نماند</p></div>
<div class="m2"><p>شهر اگر ویران شود خود را به صحرا می‌کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناخدای کشتی می می‌توانم شد کلیم</p></div>
<div class="m2"><p>بردبارم همچو کشتی گرچه دریا می‌کشم</p></div></div>