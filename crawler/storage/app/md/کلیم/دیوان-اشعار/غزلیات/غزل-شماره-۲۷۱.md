---
title: >-
    غزل شمارهٔ  ۲۷۱
---
# غزل شمارهٔ  ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>یاد تو از ضمیر به نسیان نمی رود</p></div>
<div class="m2"><p>نقش رخت ز دیده به طوفان نمی رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بخت تیره چون بتماشای او روم</p></div>
<div class="m2"><p>در شب کسی بسیر گلستان نمی رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق بسان شمع بود از غرور عشق</p></div>
<div class="m2"><p>در زندگی سرش به گریبان نمی رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع قلم زنامه گرمم بته رسید</p></div>
<div class="m2"><p>شوقم هنوز بر سر عنوان نمی رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تن سرد گشت و داغ جنون گرم سوختن</p></div>
<div class="m2"><p>سر در ره رفته و سامان نمی رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقی ز می کدورت دل کم نمی شود</p></div>
<div class="m2"><p>بنشین که داغ لاله ز باران نمی رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندانکه می رویم بجائی نمی رسیم</p></div>
<div class="m2"><p>ریگ ار روان بود ز بیابان نمی رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتم بفکر زلف تو هنگام بیخودی</p></div>
<div class="m2"><p>مستش مدان کسیکه پریشان نمی رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیگر کلیم اگر ز لگدکوب حادثه</p></div>
<div class="m2"><p>چون سرمه می شود ز صفاهان نمی رود</p></div></div>