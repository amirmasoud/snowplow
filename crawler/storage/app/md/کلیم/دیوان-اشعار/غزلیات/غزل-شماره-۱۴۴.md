---
title: >-
    غزل شمارهٔ  ۱۴۴
---
# غزل شمارهٔ  ۱۴۴

<div class="b" id="bn1"><div class="m1"><p>دیده چشم می پرستی دیده است</p></div>
<div class="m2"><p>اشکم از مستی بسر غلطیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر او رفت اینجا جا نبود</p></div>
<div class="m2"><p>سینه تنگ و آرزو بالیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف در گوش تو شرح حال ما</p></div>
<div class="m2"><p>گفته است اما بهم پیچیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسکه می بیند ز ما دیوانگی</p></div>
<div class="m2"><p>دیده داغ جنون ترسیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزگار اندر کمین بخت ماست</p></div>
<div class="m2"><p>دزد دایم در پی خوابیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه اش در بند دارد خنده را</p></div>
<div class="m2"><p>زاب لب شیرین شکر دزدیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خویش و قومی نیست تا رسوا شویم</p></div>
<div class="m2"><p>عیب ما را بیکسی پوشیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارم از غم رونقی دارد کلیم</p></div>
<div class="m2"><p>دست بر سر آستین بر دیده است</p></div></div>