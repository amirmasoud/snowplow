---
title: >-
    غزل شمارهٔ  ۵۳۴
---
# غزل شمارهٔ  ۵۳۴

<div class="b" id="bn1"><div class="m1"><p>ز خجلت تا دل ما را شکسته</p></div>
<div class="m2"><p>بود چون ساقی مینا شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سزاوار جفایت هیچکس نیست</p></div>
<div class="m2"><p>براهت خار قدر پا شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدستت باده ساقی مومیائی است</p></div>
<div class="m2"><p>پر از می کن اگر مینا شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکست توبه پیروزی و فتحست</p></div>
<div class="m2"><p>کزو شد لشکر غمها شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکسته خاطری یکسوی دارم</p></div>
<div class="m2"><p>تنی چون نامه سرتاپا شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل زارم بسان توبه می</p></div>
<div class="m2"><p>نرست از دست مردم نا شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رواج قمریان از ناله من</p></div>
<div class="m2"><p>چو قدر سرو از آن بالا شکسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نمازم را درستی نیست هرچند</p></div>
<div class="m2"><p>زبار سجده هفت اعضا شکسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم اصلاح دل تا چند، گو باش</p></div>
<div class="m2"><p>درست از دیگران، از ما شکسته</p></div></div>