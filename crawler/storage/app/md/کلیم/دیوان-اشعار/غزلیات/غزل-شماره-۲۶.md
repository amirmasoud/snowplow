---
title: >-
    غزل شمارهٔ  ۲۶
---
# غزل شمارهٔ  ۲۶

<div class="b" id="bn1"><div class="m1"><p>بر سر خود می کند ویران سرای دیده را</p></div>
<div class="m2"><p>پختگی حاصل نشد اشک جهان گردیده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی توانی ترک ما کردن که با هم الفتیست</p></div>
<div class="m2"><p>طالع برگشته و مژگان برگردیده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دستگاه ما کجا شایسته تاراج اوست</p></div>
<div class="m2"><p>غیرزانو نیست سامانی سر شوریده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوه محنت سخت می کاهد مرا ساقی بده</p></div>
<div class="m2"><p>باده تندی که بگدازد غم بالیده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زمان تیره روزی دوست دشمن می شود</p></div>
<div class="m2"><p>بیتو مژگان می زند دامن چراغ دیده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاصل پرهیز زاهد نیست جز آلودگی</p></div>
<div class="m2"><p>کرده پر خار تعلق دامن برچیده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ترازوی صدف گوهر نگنجد از نشاط</p></div>
<div class="m2"><p>با گهر همسر کنم گر نکته سنجیده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کند تدبیر بخت بدمروت مانعست</p></div>
<div class="m2"><p>سهل باشد چاره کردن دشمن خوابیده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامه ات را قاصد آورد و نمی خواند کلیم</p></div>
<div class="m2"><p>از دلش ناید که بردارد ز راهت دیده را</p></div></div>