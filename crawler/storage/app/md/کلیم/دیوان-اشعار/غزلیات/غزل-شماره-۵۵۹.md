---
title: >-
    غزل شمارهٔ  ۵۵۹
---
# غزل شمارهٔ  ۵۵۹

<div class="b" id="bn1"><div class="m1"><p>رواج جهل مرکب رسیده است بجائی</p></div>
<div class="m2"><p>که کرده هر مگسی خویشرا خیال همائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز طور مرتبه موسوی فرود نیاید</p></div>
<div class="m2"><p>بدست کور گر افتد درین زمانه عصائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رغم مائده عیسوی بخویش ببالد</p></div>
<div class="m2"><p>اگر چه کاسه خالی بود بدست گدائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زند ز نغمه داود طعنه صوت صدایش</p></div>
<div class="m2"><p>زمانه بر گلوی هر خری که بست درائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خاک بیمدد دستگیر هر که نخیزد</p></div>
<div class="m2"><p>زند بافسر خورشید نخوتش سرپائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیاز و عجز گدایانه می خرند و ندارند</p></div>
<div class="m2"><p>مروتی که گدائی از آن رسد بنوائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دانه خرمن اهل غرور مایه ندارد</p></div>
<div class="m2"><p>رود بغارت اگر برخورد بکاهربائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تمام در شب تاریک جهل، یوسف وقتند</p></div>
<div class="m2"><p>سری بر آور ای شمع امتیاز کجائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه ببانگ سگ نفس می روند بمنزل</p></div>
<div class="m2"><p>عجب تر اینکه به از خضر جسته راهنمائی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلیم خاطر روشن زغم چه عکس پذیرد</p></div>
<div class="m2"><p>بر آینه تیرگی است زنگ زدائی</p></div></div>