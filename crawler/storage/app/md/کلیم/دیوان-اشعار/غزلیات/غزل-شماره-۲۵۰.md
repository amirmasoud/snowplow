---
title: >-
    غزل شمارهٔ  ۲۵۰
---
# غزل شمارهٔ  ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>بپرسش آمد و عاشق همین دو دم دارد</p></div>
<div class="m2"><p>شکسته پای بمقصود یک قدم دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز راز خاطر هم آگهیم و سینه ما</p></div>
<div class="m2"><p>ز کاوش مژه چون سبحه ره بهم دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز نقش پای بیابان نورد غم پیداست</p></div>
<div class="m2"><p>نشان هر سر خاری که در قدم دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن زمن نتراود چو سینه چاک نیم</p></div>
<div class="m2"><p>همیشه نال تنم عادت قلم دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جدا زکوی تو خونم سبیل شد چکنم</p></div>
<div class="m2"><p>که مرغ ایمنی از پرتو حرم دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روان چو کاغذ بادش کنم نه پیچیده</p></div>
<div class="m2"><p>زبسکه دیده ام از خون دیده نم دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بغیر خون نتراود ز نامه های کلیم</p></div>
<div class="m2"><p>بکف مگر زنی تیر او قلم دارد</p></div></div>