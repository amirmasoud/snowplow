---
title: >-
    غزل شمارهٔ  ۳۳
---
# غزل شمارهٔ  ۳۳

<div class="b" id="bn1"><div class="m1"><p>هر کس بقبله ای کرد روی نیاز خود را</p></div>
<div class="m2"><p>هند و صنم پرستد، من سرو ناز خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگذاشت آستانش در جبهه ام سجودی</p></div>
<div class="m2"><p>بی سجده می گذارم اکنون نماز خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کنج نامرادی تا کی ز منع دشمن</p></div>
<div class="m2"><p>در زیر سر گذارم دست دراز خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نقش پا بر شکم گرچه همی گذارد</p></div>
<div class="m2"><p>بر آستان جانان روی نیاز خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانه سان نگردد هر دم بگرد شمعی</p></div>
<div class="m2"><p>خواهد کلیم بیدل عاشق گداز خود را</p></div></div>