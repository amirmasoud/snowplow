---
title: >-
    غزل شمارهٔ  ۲۸۷
---
# غزل شمارهٔ  ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>بکن بیخ صبوری حسرت دیدار می‌آرد</p></div>
<div class="m2"><p>چو میرد باغبان این نخل برگ و بار می‌آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کدورت می‌فزاید جام خاکی، حیرتی دارم</p></div>
<div class="m2"><p>که این آیینه چون بی‌نم بود زنگار می‌آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلی دارم چنان بیگانه از عشرت که در گلشن</p></div>
<div class="m2"><p>پی نظاره گل روی در دیوار می‌آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیاری کش تو بی‌پروا طبیب دردمندانی</p></div>
<div class="m2"><p>اجل از رحم شربت بر سر بیمار می‌آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نصیبم نیست شهد راحتی بی‌زهر اندوهی</p></div>
<div class="m2"><p>صبا بوی گل گر آورد با خار می‌آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلیم از گریه گفتم آبرویی رو دهد ما را</p></div>
<div class="m2"><p>چه دانستم که اشک آتش بروی کار می‌آرد</p></div></div>