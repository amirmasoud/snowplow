---
title: >-
    غزل شمارهٔ  ۴۸۷
---
# غزل شمارهٔ  ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>تمام دردم و روی دوا نمی بینم</p></div>
<div class="m2"><p>بچشم خواهش خود توتیا نمی بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه دیدنت از بس نگاه ضبط کنم</p></div>
<div class="m2"><p>دمیکه راه روم پیش پا نمی بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرچه پرده حیرت غبار چشم منست</p></div>
<div class="m2"><p>ز عشوه های نهانی چها نمی بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزخم اگر چه بسی کار تنگ می گیرد</p></div>
<div class="m2"><p>خوشم به بخیه که روی دوا نمی بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان لشکر بیگانه تغافل او</p></div>
<div class="m2"><p>بجز نگاه دگر آشنا نمی بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برون نمی روم از خانه همچو آئینه</p></div>
<div class="m2"><p>اگر بدیدنم آیند وا نمی بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز کدورت از افلاک هیچ حاصل نیست</p></div>
<div class="m2"><p>بغیر گرد درین آسیا نمی بینم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم بدست تو، دستم بسر زماتم دل</p></div>
<div class="m2"><p>فغان که دست و دل خود بجا نمی بینم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم گر همه تن چون حباب دیده شوم</p></div>
<div class="m2"><p>بچشم حرص در آب بقا نمی بینم</p></div></div>