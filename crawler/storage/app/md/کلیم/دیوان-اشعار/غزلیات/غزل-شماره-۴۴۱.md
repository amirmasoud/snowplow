---
title: >-
    غزل شمارهٔ  ۴۴۱
---
# غزل شمارهٔ  ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>شکوه درد ترا کی پیش درمان می‌کنیم</p></div>
<div class="m2"><p>تشنه می‌میرم و شکر آب حیوان می‌کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌تو تاریکست کشمیر ای چراغ دیده‌ها</p></div>
<div class="m2"><p>ما سیه‌روزیم در شب سیر بستان می‌کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل اگر تا سینه در کشمیر می‌آید چه سود</p></div>
<div class="m2"><p>ما که گل از اشک خونین در گریبان می‌کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کمین عیش از بس دیده بد دیده‌ایم</p></div>
<div class="m2"><p>باده را از چشم ساغر نیز پنهان می‌کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماجرای دیده می‌گوییم پیش سیل اشک</p></div>
<div class="m2"><p>ابلهی بین شکوه کشتی به طوفان می‌کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تو رفتی دل تفکر خویشتن افتاده است</p></div>
<div class="m2"><p>سر چو می‌بازیم آنگه فکر سامان می‌کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باده کشمیر از بزم تو صاحب نشئه بود</p></div>
<div class="m2"><p>بی‌تو ما خاطرنشان می‌پرستان می‌کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغ می‌ماند کلیم ار لاله‌زار از دست رفت</p></div>
<div class="m2"><p>هرچه دشوارست ما بر خویش آسان می‌کنیم</p></div></div>