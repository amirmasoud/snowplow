---
title: >-
    غزل شمارهٔ  ۵۶۹
---
# غزل شمارهٔ  ۵۶۹

<div class="b" id="bn1"><div class="m1"><p>زتیغ تو بر دل در آشنائی</p></div>
<div class="m2"><p>گشادیم شاید ازین در در آئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه را بمژگان رسان، چند باشد</p></div>
<div class="m2"><p>میان دو همخانه ناآشنائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر الفت ابروان تو گردم</p></div>
<div class="m2"><p>که یک مو ندارند از هم جدائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش فریبنده چشم تو میرم</p></div>
<div class="m2"><p>که مژگان ز مژگان کند دلربائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدریوزه خاکپایت بتان را</p></div>
<div class="m2"><p>شود دیده ها کاسه های گدائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>براه تو ای صید وحشی ز هر سو</p></div>
<div class="m2"><p>شد از دیده دامها روشنائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا شمع در هیچ بزمی نبیند</p></div>
<div class="m2"><p>که نگدازد از خجلت خودنمائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبر گشته مژگانت آخر نپرسی</p></div>
<div class="m2"><p>که رو بر قفا از چه بی جدائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم آتش داغت افسرده گشته</p></div>
<div class="m2"><p>منه دل برین چشم بی روشنائی</p></div></div>