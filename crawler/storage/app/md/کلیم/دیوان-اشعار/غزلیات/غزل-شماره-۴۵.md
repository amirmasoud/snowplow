---
title: >-
    غزل شمارهٔ  ۴۵
---
# غزل شمارهٔ  ۴۵

<div class="b" id="bn1"><div class="m1"><p>کسیکه مانده به بند لباس زندانیست</p></div>
<div class="m2"><p>پریدن از قفس نام و ننگ عریانیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پختگی جنون کی بمن رسد مجنون</p></div>
<div class="m2"><p>همین بسست که من شهری او بیابانیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زچشم گریان، بیقدر شد متاع جنون</p></div>
<div class="m2"><p>بهر دیار که بارندگیست ارزانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهار آمده یارب چه رهن باده کنم</p></div>
<div class="m2"><p>مرا که جامه عیدی قبای عریانیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا حقیقت این هر دو نشئه از من پرس</p></div>
<div class="m2"><p>حیات گردی و این مرگ دامن افشانیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلیم دعوی دل را بزلف یار ببخش</p></div>
<div class="m2"><p>دگر مپیچ بران، عالم پریشانیست</p></div></div>