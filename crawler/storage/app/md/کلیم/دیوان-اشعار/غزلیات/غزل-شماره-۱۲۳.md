---
title: >-
    غزل شمارهٔ  ۱۲۳
---
# غزل شمارهٔ  ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>روشنی در خانه معمور نیست</p></div>
<div class="m2"><p>نیست یک ویرانه کان پر نور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه در بزم نشاط ما گریست</p></div>
<div class="m2"><p>قطره ای خون در رگ طنبور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ز مهر گلرخان پرداختیم</p></div>
<div class="m2"><p>در بپشت خاطر ما حور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها پروانه او بوده ایم</p></div>
<div class="m2"><p>در چراغ آشنائی نور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو باشی رو بخورشید آورد؟</p></div>
<div class="m2"><p>اینقدر هم چشم روشن کور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسکه دیگرگون شد احوال جهان</p></div>
<div class="m2"><p>فکر می در خاطر مخمور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نظر دارم لبی را روز و شب</p></div>
<div class="m2"><p>چون توانم گفت چشمم شور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می کنم قطع امید از تیغ تو</p></div>
<div class="m2"><p>زخم اگر در تازگی ناسور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده بر زخمم چه می پوشی کلیم</p></div>
<div class="m2"><p>شمع در فانوس هم مستور نیست</p></div></div>