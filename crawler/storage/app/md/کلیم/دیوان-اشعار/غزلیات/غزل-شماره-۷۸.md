---
title: >-
    غزل شمارهٔ  ۷۸
---
# غزل شمارهٔ  ۷۸

<div class="b" id="bn1"><div class="m1"><p>بملک حسن که فیضی ز آشنائی نیست</p></div>
<div class="m2"><p>در آشنائی خورشید روشنائی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنچه رفت زدستم برون ز دل هم رفت</p></div>
<div class="m2"><p>میان دست و دلم چون صدف جدائی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار خاطرم از شش جهت گرفته فرو</p></div>
<div class="m2"><p>چو اخگرم سرو پروای خودنمائی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بکشوری که فتد عکس تیره روزی ما</p></div>
<div class="m2"><p>زآب و آینه امید روشنائی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که شیوه افتادگی هنر باشد</p></div>
<div class="m2"><p>شکست نفس بجز عیب خودستائی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز درد فقر دلا غیرتی اگر داری</p></div>
<div class="m2"><p>مخواه مرگ که خواهش بجز گدائی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باضطراب گرفتارم آنقدر که قفس</p></div>
<div class="m2"><p>شکسته است و مرا فرصت رهائی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو پاز آبله پوشیده ای، برو بنشین</p></div>
<div class="m2"><p>که ناقص است سلوک ار برهنه پائی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که را کلیم ستودم که بر سپهر نرفت</p></div>
<div class="m2"><p>هزار حیف که پروای خودستائی نیست</p></div></div>