---
title: >-
    غزل شمارهٔ  ۲۶۲
---
# غزل شمارهٔ  ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>آشوب طلب خاطر فرزانه ندارد</p></div>
<div class="m2"><p>زنبور هوس در دل ما خانه ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندازه مستی نتوانیم نگهداشت</p></div>
<div class="m2"><p>زان باده خرابیم که پیمانه ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مزرعه طاقت ما تخم ریا نیست</p></div>
<div class="m2"><p>اینجاست که تسبیح عمل دانه ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم چو پریشانی زلفت جگرم سوخت</p></div>
<div class="m2"><p>غیر از دل صد رخنه من شانه ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جائی ننشستیم کز آنجا نرمیدیم</p></div>
<div class="m2"><p>جغدیم در آن شهر که ویرانه ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کشور این زهدفروشان نتوان یافت</p></div>
<div class="m2"><p>یک صومعه کان راه به بتخانه ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق همه جا شیفته ناز و عتابست</p></div>
<div class="m2"><p>شمعی که نیفروخته پروانه ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن گرد کدورت که بود همره یکخویش</p></div>
<div class="m2"><p>هرگز قدم لشکر بیگانه ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیداست که غارتگر سامان کلیمست</p></div>
<div class="m2"><p>کاندوخته جز درد بکاشانه ندارد</p></div></div>