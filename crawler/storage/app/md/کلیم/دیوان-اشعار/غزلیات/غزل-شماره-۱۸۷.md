---
title: >-
    غزل شمارهٔ  ۱۸۷
---
# غزل شمارهٔ  ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>عمر سیرش کوته است ار جورت از دل می‌رود</p></div>
<div class="m2"><p>چند گامی از ضرورت مرغ بسمل می‌رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب غفلت بس که چشم کاروان عمر بست</p></div>
<div class="m2"><p>بانگ باید بر جرس‌ها زد که محمل می‌رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کینه‌اش ای کاش باعث می‌شدی بر قتل ما</p></div>
<div class="m2"><p>خون ناحق کشته زود از یاد قاتل می‌رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهر اگر بحر پرآشوبست مستان را چه غم</p></div>
<div class="m2"><p>کشتی من بی‌خطر دایم به ساحل می‌رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زبان گنگ باید در سخن خود را گرفت</p></div>
<div class="m2"><p>راه باریکست کار از طبع کاهل می‌رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر زبان دارد حدیث چشم طوفان‌زای من</p></div>
<div class="m2"><p>خامه محذورست گر با سینه در گل می‌رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جذب شوقم می‌برد رهبر نمی‌خواهم کلیم</p></div>
<div class="m2"><p>هرکه سیلابش برد بی‌خود به منزل می‌رود</p></div></div>