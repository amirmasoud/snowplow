---
title: >-
    غزل شمارهٔ  ۶۳
---
# غزل شمارهٔ  ۶۳

<div class="b" id="bn1"><div class="m1"><p>ابر را دیدیم چون ما چشم گریانی نداشت</p></div>
<div class="m2"><p>برق هم کم مایه بود از شعله سامانی نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با مسیحا درد خود گفتیم پرسودی نکرد</p></div>
<div class="m2"><p>زانکه چون بیماری چشم تو درمانی نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سینه ما هیچگه بی ناوک جوری نبود</p></div>
<div class="m2"><p>این مصیبت خانه کم دیدم که مهمانی نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لذت رو بر قفا رفتن چه می داند که چیست</p></div>
<div class="m2"><p>هر که در دل حسرت برگشته مژگانی نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از در و دیوار می بارد بلا در راه عشق</p></div>
<div class="m2"><p>یک سرابم پیش ره نامد که طوفانی نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نامه ام را می بری قاصد زبانی هم بگو</p></div>
<div class="m2"><p>خانه شد فرسوده و این شکوه پایانی نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مایه حزنست هر بیتم زسوز دل کلیم</p></div>
<div class="m2"><p>هیچ محنت دیده چون من بیت احزانی نداشت</p></div></div>