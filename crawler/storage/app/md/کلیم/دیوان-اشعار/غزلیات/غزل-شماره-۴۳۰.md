---
title: >-
    غزل شمارهٔ  ۴۳۰
---
# غزل شمارهٔ  ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>آتش دیگ هوس از دل سوزان گیرم</p></div>
<div class="m2"><p>آب لب تشنگی از آهن پیکان گیرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابم اینست که در دیدنت از هوش روم</p></div>
<div class="m2"><p>خوردنم اینکه سرانگشت بدندان گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرق خجلت من سیل وجودم گردد</p></div>
<div class="m2"><p>فقر را گر دهم و ملک سلیمان گیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجه می گر نبود منکه ببوئی مستم</p></div>
<div class="m2"><p>جا بهمسایگی باده فروشان گیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روش سوختن داغ ز دام آموزم</p></div>
<div class="m2"><p>وز قفس قاعده چاک گریبان گیرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تف آتش آن تب که تنم را بگداخت</p></div>
<div class="m2"><p>از گل داغ گلاب از پی درمان گیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داده خویشتن ایام چو می گیرد باز</p></div>
<div class="m2"><p>حیف باشد که بجز پند زدوران گیرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارم آن حوصله و صبر که غم هم نخورم</p></div>
<div class="m2"><p>از تهیدستی اگر روزه حرمان گیرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتوان بود کلیم اینهمه در بند لباس</p></div>
<div class="m2"><p>بهر اطفال سرشکی که بدامان گیرم</p></div></div>