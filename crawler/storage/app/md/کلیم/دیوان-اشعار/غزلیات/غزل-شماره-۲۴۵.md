---
title: >-
    غزل شمارهٔ  ۲۴۵
---
# غزل شمارهٔ  ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>دل جز کجی ز زلف تو نامهربان ندید</p></div>
<div class="m2"><p>رو چشم بست و روی ترا در میان ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند خرمی جهان را سبب منم</p></div>
<div class="m2"><p>مانند ابر هیچکسم شادمان ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دامان من که قافله گاه سرشک بود</p></div>
<div class="m2"><p>چیزی بغیر آتش ازین کاروان ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکس که خودنمای بود مایه دار نیست</p></div>
<div class="m2"><p>هرگز کسی گلی بسر باغبان ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آنکه بی نقاب تر از آفتاب بود</p></div>
<div class="m2"><p>چون صبح از تبسم او کس نشان ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کامی بغیر دانه بی آب اختران</p></div>
<div class="m2"><p>صید اسیر در قفس آسمان ندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می کاهم از شکفتگی خویشتن مدام</p></div>
<div class="m2"><p>شمعم که کس بهار مرا بیخوان ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خامند سربسر همه ابنای روزگار</p></div>
<div class="m2"><p>کس میوه رسیده درین بوستان ندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا کی کلیم گریه کنی گاه دیدنش</p></div>
<div class="m2"><p>کس ماه را همیشه در آب روان ندید</p></div></div>