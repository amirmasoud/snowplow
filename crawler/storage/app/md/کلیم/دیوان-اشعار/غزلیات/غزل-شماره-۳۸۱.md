---
title: >-
    غزل شمارهٔ  ۳۸۱
---
# غزل شمارهٔ  ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>بیا که بیتو سیاهی ز چشم روشن شد</p></div>
<div class="m2"><p>ز گریه دیده ما همچو چشم روزن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جدا ز لعل لبت جام ماتمی دارد</p></div>
<div class="m2"><p>زدم چو بر لبش انگشت گرم شیون شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای سوختن آماده ام چنانکه کسی</p></div>
<div class="m2"><p>اگر بر آتش من آب ریخت روغن شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قفس بدیده مرغ اسیر تاریکست</p></div>
<div class="m2"><p>چه شد که بام و در او تمام روزن شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زچاک پیرهن آن بهینه را ببین ای بخت</p></div>
<div class="m2"><p>سری ز خواب برآور که صبح روشن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبسکه بر سر هم ریختیم و سبز نشد</p></div>
<div class="m2"><p>بزیر خاکم تخم امید خرمن شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیانت است اگر در ره بهشت نهی</p></div>
<div class="m2"><p>کلیم پای تو هر وقت وقف دامن شد</p></div></div>