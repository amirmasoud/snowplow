---
title: >-
    غزل شمارهٔ  ۳۸۳
---
# غزل شمارهٔ  ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>خوش آنزمان که عتاب بهانه ساز نبود</p></div>
<div class="m2"><p>زبان تیغ جفا اینقدر دراز نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بروی طوفان روزیکه دیده وا کردم</p></div>
<div class="m2"><p>بروی دریا چشم حباب باز نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلند و پست جهان با هم است پس زچه رو</p></div>
<div class="m2"><p>نشیب بخت مرا طالع از فراز نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشسته ایم بخاک سیه ز طبع بلند</p></div>
<div class="m2"><p>سزای آنکه طبیعت زمانه ساز نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهر خزف بود آنجا که گوهری نبود</p></div>
<div class="m2"><p>هنر غریب شد آنجا که امتیاز نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگیر سهل اگر درد عشق یکروز است</p></div>
<div class="m2"><p>کدام روز تب شمع جانگداز نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمیکه تخته مشق جراحتش بودم</p></div>
<div class="m2"><p>هنوز آن مژه شوخ تیغ باز نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شکایتی که دل از زلف تابدار تو داشت</p></div>
<div class="m2"><p>کم از شکایت شمع از شب دراز نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم نسبت شمع ار بشعله چسبان شد</p></div>
<div class="m2"><p>بتنگ درزی ربط نیاز و ناز نبود</p></div></div>