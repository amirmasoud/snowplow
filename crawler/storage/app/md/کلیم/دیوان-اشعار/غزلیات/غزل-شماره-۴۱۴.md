---
title: >-
    غزل شمارهٔ  ۴۱۴
---
# غزل شمارهٔ  ۴۱۴

<div class="b" id="bn1"><div class="m1"><p>خم زلفی است دگر دام گرفتاری دل</p></div>
<div class="m2"><p>که درو موی نگنجیده ز بسیاری دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راهزن را نبود باک ز فریاد جرس</p></div>
<div class="m2"><p>ترک یغما نکند غمزه ات از زاری دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید چون بیکسی ما دل آهن شد نرم</p></div>
<div class="m2"><p>ماند پیکان تو در سینه به غمخواری دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خنده بر بخت زنم یا به وفاداری دوست</p></div>
<div class="m2"><p>گریه بر خویش کنم یا به گرفتاری دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طاقت صبر و سکون در سر کار دل رفت</p></div>
<div class="m2"><p>عاشقان خانه خرابند ز معماری دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه بگذاشت چنین نرگس بیمار تو را</p></div>
<div class="m2"><p>گفت من هم نکنم چاره ی بیماری دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مذهب بنده و آزاد همین یک حرف است</p></div>
<div class="m2"><p>چیست آزادی کونین؟ سبکباری دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق چون تیغ کشد بر دل بیچاره کلیم</p></div>
<div class="m2"><p>کیست جز داغ که آید به سپرداری دل</p></div></div>