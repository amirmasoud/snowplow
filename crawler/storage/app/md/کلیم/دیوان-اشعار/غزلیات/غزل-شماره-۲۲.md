---
title: >-
    غزل شمارهٔ  ۲۲
---
# غزل شمارهٔ  ۲۲

<div class="b" id="bn1"><div class="m1"><p>دنبال اشک افتاده ام جویم دل آزرده را</p></div>
<div class="m2"><p>از خون توان برداشت پی نخجیر پیکان خورده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این رخ افروخته هر جا خرامان بگذری</p></div>
<div class="m2"><p>از باد دامن می کنی روشن چراغ مرده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ترک چشم رهزنت نشناخت قدر دل چه شد</p></div>
<div class="m2"><p>قیمت چه داند لشکری جنس بغارت برده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاری ز زلف آن صنم در گردن ایمان فکن</p></div>
<div class="m2"><p>ای شیخ تا پیدا کنی سررشته گم کرده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر جان بجانان نسپرم دل بسته آن نیستم</p></div>
<div class="m2"><p>نتوان بدست پادشه دادن گل پژمرده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد ز بی سرمایگی کرده است در صد جا گرو</p></div>
<div class="m2"><p>دین بدنیا داده را ایمان شیطان برده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دشمنی با خویشتن فرصت بخصم خود مده</p></div>
<div class="m2"><p>خود برفکن همچون حباب از روی کارت پرده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دوران بیک زخم جفا کی از سر ما واشود</p></div>
<div class="m2"><p>صیاد از پی می رود نخجیر ناوک خورده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر بجان آمد کلیم، از پاس خاطر داشتن</p></div>
<div class="m2"><p>تا کی بدل واپس برد حرف بلب آورده را</p></div></div>