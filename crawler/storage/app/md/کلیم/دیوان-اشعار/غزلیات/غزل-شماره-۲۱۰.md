---
title: >-
    غزل شمارهٔ  ۲۱۰
---
# غزل شمارهٔ  ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>کند گر آرزوی دیدنت آیینه جا دارد</p></div>
<div class="m2"><p>که از خورشید رویت در برابر رونما دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارد بزم میخواران به غیر از ما تنگ‌ظرفی</p></div>
<div class="m2"><p>صراحی بر رخ هرکس که می‌خندد به ما دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نویسم نامه و از بس که خون می‌گریم از هجرت</p></div>
<div class="m2"><p>تو گویی کاغذ مکتوب من رنگ حنا دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشد بیروی او چشم سفید از توتیا روشن</p></div>
<div class="m2"><p>نبیند بهره‌ای هرچند کاغذ توتیا دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز هم ربط نیاز و ناز را نتوان گسست آری</p></div>
<div class="m2"><p>کشش باقی بود تا کاه رنگ کهربا دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سرگردان شوی از بهر روزی پا به دامان کش</p></div>
<div class="m2"><p>کز آب و دانه این سرگشتگی را آسیا دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کویت چون کلیم آمد چو مستان هر قدم افتد</p></div>
<div class="m2"><p>نبیند پیش پا بیچاره چون رو بر قفا دارد</p></div></div>