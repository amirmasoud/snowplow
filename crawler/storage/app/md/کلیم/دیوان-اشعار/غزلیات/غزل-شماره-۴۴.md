---
title: >-
    غزل شمارهٔ  ۴۴
---
# غزل شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>باده در دور غمت بسکه نشاط افزا نیست</p></div>
<div class="m2"><p>پنبه را نیز سر همدمی میناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نمایند مه عید بانگشت، بهم</p></div>
<div class="m2"><p>سوی ابروی تو میل مژه ها بیجا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هیچ ازین دیده خونابه گشادیم نشد</p></div>
<div class="m2"><p>چکنم گوهر مقصود درین دریا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب ز هم وانشود تا ز می اش پر نکنم</p></div>
<div class="m2"><p>شیشه سان غلغل نطقم بجز از صهبا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوش دادم بصبا بوی تو نگرفته هنوز</p></div>
<div class="m2"><p>تا نگویند که مجنون تو خوش سودا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ندارد غم ما دهر، نرنجیم ازو</p></div>
<div class="m2"><p>زانکه در خاطر ما نیز غم دنیا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر دور فلک شد، بکدورت خو کن</p></div>
<div class="m2"><p>باده صاف دگر در ته این مینا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک بیک وعده او را همه دیدیم کلیم</p></div>
<div class="m2"><p>نیست یکروز که شرمنده صد فردا نیست</p></div></div>