---
title: >-
    غزل شمارهٔ  ۱۹۶
---
# غزل شمارهٔ  ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>گر حق نگری لایق منصور نباشد</p></div>
<div class="m2"><p>داری که ز چوب شجر طور نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سهلست، بغمنامه ما یک نظر افکن</p></div>
<div class="m2"><p>این مهر و وفائیست که منظور نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی پنبه کند کار نمک بر سر داغم</p></div>
<div class="m2"><p>بخت من سودا زده گر شور نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب نمک لعل لبت باد حرامش</p></div>
<div class="m2"><p>هر زخم جفای تو که ناسور نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خویش توان دید چوبینش بکمالست</p></div>
<div class="m2"><p>آن کعبه مقصد که رهش دور نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست هوسم از لب ساغر نشود دور</p></div>
<div class="m2"><p>تا پای امیدم بلب گور نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوریست که با دستکش خویش نسازد</p></div>
<div class="m2"><p>گر عقل ترا نفس تو مأمور نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر اهل رضا راه بفردوس نیابند</p></div>
<div class="m2"><p>در دوزخشان شعله کم از نور نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قسمت بکلیم از اثر بخت بد افتاد</p></div>
<div class="m2"><p>کامی که میسر بزر و زور نباشد</p></div></div>