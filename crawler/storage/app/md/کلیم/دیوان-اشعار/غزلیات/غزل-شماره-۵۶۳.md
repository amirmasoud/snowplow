---
title: >-
    غزل شمارهٔ  ۵۶۳
---
# غزل شمارهٔ  ۵۶۳

<div class="b" id="bn1"><div class="m1"><p>چه نیکو گفت با گردن‌کشی سر در گریبانی</p></div>
<div class="m2"><p>که ما را نیز در میدان دلتنگی‌ست جولانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بی‌برگی متاع خانه من نیست غیر از این</p></div>
<div class="m2"><p>به جز بلبل نباشد آشیان را برگ و سامانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل رخساره‌ات آب دگر دارد، سرت گردم</p></div>
<div class="m2"><p>به رویت بوده امشب باز حیران چشم گریانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گریبان‌گیر من شد آشنایی، وادی‌ای خواهم</p></div>
<div class="m2"><p>که از بیگانگی خارش نگیرد طرف دامانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزارم عقده پیش آمد به راه ناامیدی هم</p></div>
<div class="m2"><p>درین وادی سرابی را ندیدم بی‌نگهبانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگردان گرد هر مویت، دل و جان اسیران را</p></div>
<div class="m2"><p>که امشب بهر زلفت دیده‌ام خواب پریشانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زیر سنگ طفلان شد تن دیوانه پوشیده</p></div>
<div class="m2"><p>جنون خلقت ز خارا داد هرجا دید عریانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو در گلشن نشینی شاخ گل در گوشه بزمت</p></div>
<div class="m2"><p>نشیند منفعل از خویش چون ناخوانده مهمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپند از گرمی آتش نبیند آنچه می‌بیند</p></div>
<div class="m2"><p>کلیم از آب حیوان تغافل تا برد جانی</p></div></div>