---
title: >-
    غزل شمارهٔ  ۵۱۷
---
# غزل شمارهٔ  ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>کمر از تار جان باید بران نازک میان</p></div>
<div class="m2"><p>کی از هر رشته ای آن دسته گل می توان بستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزور رعشه شوق اضطرابی آرزو دارم</p></div>
<div class="m2"><p>که مغزم را نباشد فرصت در استخوان بستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بروز ار عندلیم، شام چون پروانه خاموشم</p></div>
<div class="m2"><p>دران کو صرفه من نیست خواب پاسبان بستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علاج اضطراب دل نمی آید ز من ورنه</p></div>
<div class="m2"><p>بافسون می توانم لرزه آب روان بستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همیشه پیشه من عجز و کار اوست استغنا</p></div>
<div class="m2"><p>ز گلچین در زدن می آید و از باغبان بستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دکان گلفروشم رونق من موسمی دارد</p></div>
<div class="m2"><p>بخود نتوان گل داغ جنونرا در خزان بستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرس این ناله را از پهلوی دلبستگی دارد</p></div>
<div class="m2"><p>نبایستی ز اول خویش را بر کاروان بستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنازم ترک چشمت را که ترکش بسته می خواهد</p></div>
<div class="m2"><p>بخونریز اسیران اینچنین باید میان بستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم از یک الف زخم اینهمه بهر چه مینالی</p></div>
<div class="m2"><p>سخن کوتاه کن تا کی ز حرفی داستان بستن</p></div></div>