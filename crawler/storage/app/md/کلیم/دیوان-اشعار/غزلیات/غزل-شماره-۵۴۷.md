---
title: >-
    غزل شمارهٔ  ۵۴۷
---
# غزل شمارهٔ  ۵۴۷

<div class="b" id="bn1"><div class="m1"><p>ز آتش پنهان عشق، هر که شد افروخته</p></div>
<div class="m2"><p>دود نخیزد ازو چون نفس سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلبر بیخشم و کین، گلبن بیرنگ و بوست</p></div>
<div class="m2"><p>دلکش پروانه نیست، شمع نیفروخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در وطن خود گهر، آبله ای بیش نیست</p></div>
<div class="m2"><p>کی بعزیزی رسد، یوسف نفروخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مایه آرام دل، چشم هوس بستن است</p></div>
<div class="m2"><p>از طپش آسوده است، باز نظر دوخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاید آید بدام مرغ پریده ز چنگ</p></div>
<div class="m2"><p>گرم نگردد اگر عاشق وا سوخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داروی بیماریش مستی پیوسته است</p></div>
<div class="m2"><p>چشم تو این حکمت از پیش که آموخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد و آورد باز از سر کویش کلیم</p></div>
<div class="m2"><p>بال و پر ریخته جان و دل سوخته</p></div></div>