---
title: >-
    غزل شمارهٔ  ۱۸۴
---
# غزل شمارهٔ  ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>عمرها رفت که قانون طرب تار ندید</p></div>
<div class="m2"><p>دل بجز دیده تر ساغر سرشار ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جهان دار شفائیست که یک بیمارش</p></div>
<div class="m2"><p>خدمتی غیر تغافل ز پرستار ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که رفعت طلبد بهره نیابد از فیض</p></div>
<div class="m2"><p>خار را سبز کسی بر سر دیوار ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد آزاده گرش کار بسوگند افتاد</p></div>
<div class="m2"><p>قسم او بسری بود که دستار ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست رد گر بشناسی سپر حادثه است</p></div>
<div class="m2"><p>از بلا رست سپندی که خریدار ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیشه با آنکه سر حرف مکرر وا کرد</p></div>
<div class="m2"><p>دوش در بزم ترا در سر گفتار ندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دفترم گر شکرستان سخن گشت چه سود</p></div>
<div class="m2"><p>که بغیر از مگس نقطه هوادار ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر توفیق که از تربیتم دست کشید</p></div>
<div class="m2"><p>آن طبیبی است که پرهیز ز بیمار ندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دهر خود مجلس می نیست کلیم، از چه سبب</p></div>
<div class="m2"><p>کس در او آگهی از کار خبردار ندید</p></div></div>