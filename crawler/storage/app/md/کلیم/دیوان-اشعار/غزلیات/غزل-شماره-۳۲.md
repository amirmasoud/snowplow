---
title: >-
    غزل شمارهٔ  ۳۲
---
# غزل شمارهٔ  ۳۲

<div class="b" id="bn1"><div class="m1"><p>منم بکنج قناعت رمیده از درها</p></div>
<div class="m2"><p>بخویش بسته ز نقش حصیر زیورها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار خاطر خود گرد هم بسیل سرشگ</p></div>
<div class="m2"><p>شود ببحر گل آلود آب گوهرها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بمن عداوت گردون بجا بود، تا کی</p></div>
<div class="m2"><p>نشان ناوک آهم شوند اخترها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسلم است مرا دعوی وفاداری</p></div>
<div class="m2"><p>خجل ز داغ وفای منند محضرها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زجام لاله و گل قطره ای نریزد می</p></div>
<div class="m2"><p>تمام حیرتم از این شکسته ساغرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بد نهادی ابنای این زمان چه عجب</p></div>
<div class="m2"><p>که شیر باز شود خون بطبع مادرها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهیچ بزم نرفتم که روی دل بینم</p></div>
<div class="m2"><p>منم سپند و مجالس تمام مجمرها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر نه در غم عشقت زنند سر بر سنگ</p></div>
<div class="m2"><p>چرا چنین شده مودار کاسه سرها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس است بهر رمیدن ز خویش و قوم کلیم</p></div>
<div class="m2"><p>هر آنچه یوسف دیدست از برادرها</p></div></div>