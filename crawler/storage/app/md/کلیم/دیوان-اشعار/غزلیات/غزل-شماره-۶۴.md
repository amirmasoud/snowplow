---
title: >-
    غزل شمارهٔ  ۶۴
---
# غزل شمارهٔ  ۶۴

<div class="b" id="bn1"><div class="m1"><p>دائم گله چرخ دلاورد زبان چیست</p></div>
<div class="m2"><p>گر ناوک خاری رسدت جرم کمان چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیباکی آن غمزه خونریز از آن است</p></div>
<div class="m2"><p>کز تیر نپرسند که تقسیر کمان چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خاک نشینان فلک سیر نباشند</p></div>
<div class="m2"><p>بر چرخ پس این جاده کاهکشان چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خویش جهان را زغم خویش نهان کن</p></div>
<div class="m2"><p>کاگه نشود لب که ترا ورد زبان چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن خال که در کنج لبت کرده فروکش</p></div>
<div class="m2"><p>گر گوشه نشین است سپاه دل و جان چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرد ره اگر کرم طلب نیست درین راه</p></div>
<div class="m2"><p>در بادیه سرگشتگی ریگ روان چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در پیری اگر باشد امیدی ز شکفتن</p></div>
<div class="m2"><p>دایم گره قبضه بابروی کمان چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیرون نکشم پا ز گل اشک ندامت</p></div>
<div class="m2"><p>تا یافته ام قاعده راهروان چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا هست کلیم آگهی از صرفه کارت</p></div>
<div class="m2"><p>با عقل سبک آرزوی رطل گران چیست</p></div></div>