---
title: >-
    غزل شمارهٔ  ۶۸
---
# غزل شمارهٔ  ۶۸

<div class="b" id="bn1"><div class="m1"><p>دل از سر کوی تو اگر پای کشیده است</p></div>
<div class="m2"><p>باز آمدنش زودتر از رنگ پریده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناصح هذیان گوید و ما را تب عشق است</p></div>
<div class="m2"><p>مابسمل و او می طپد اینرا که شنیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال دل صدپاره که در نامه نوشتم</p></div>
<div class="m2"><p>در یار اثر کرده که ناخوانده دریده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جیب تفکر سر خود کرده فراموش</p></div>
<div class="m2"><p>کس به ز جرس سر بگریبان نکشیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ دل ما را روش کاغذ باد است</p></div>
<div class="m2"><p>بی رشته بپا از کف طفلان نپریده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در پیرهن طاقت گلها زده آتش</p></div>
<div class="m2"><p>آن سبزه که شبنم ز در گوش تو دیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون در جگرم کرده رم طایر معنی</p></div>
<div class="m2"><p>تا بر سر تیر قلم فکر رسیده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانی عرق نقطه بروی سخن از چیست</p></div>
<div class="m2"><p>بسیار بدنبال سخن فهم دویده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن طفل که پرورده بدامان قناعت</p></div>
<div class="m2"><p>گل راچو شکر خورده و از شیر بریده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خرسند بهیچست کلیم از چمن حسن</p></div>
<div class="m2"><p>بر سر زده است آن گل وصلی که نچیده است</p></div></div>