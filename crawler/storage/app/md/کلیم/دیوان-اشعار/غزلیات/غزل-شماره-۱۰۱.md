---
title: >-
    غزل شمارهٔ  ۱۰۱
---
# غزل شمارهٔ  ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>از کمی مشتری جنس سخن خوار نیست</p></div>
<div class="m2"><p>تحفه گرانقیمت است جوش خریدار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست قضا همچو شمع در چمن خوشدلی</p></div>
<div class="m2"><p>گل بسری می زند کش غم دستار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهی خاشاک سیل، گاه خس شعله باش</p></div>
<div class="m2"><p>ساکن یک مرحله، سالک اطوار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر روشندلان، زخم جفا می خورد</p></div>
<div class="m2"><p>صیقل آئینه جز مرهم زنگار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم پریشان نظر، عاشق هر جائیست</p></div>
<div class="m2"><p>دیده اگر بسته نیست، لایق دیدار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پست و بلند سخن تابع احوال ماست</p></div>
<div class="m2"><p>ناله کنج قفس نغمه گلزار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غمزه او مست ناز، نرگس او ناتوان</p></div>
<div class="m2"><p>غیر پرستار مست بر سر بیمار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاشق دلباخته باک ندارد، کلیم</p></div>
<div class="m2"><p>سنگ ستم گو ببار شیشه چو دربار نیست</p></div></div>