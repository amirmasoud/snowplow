---
title: >-
    غزل شمارهٔ  ۳۷۶
---
# غزل شمارهٔ  ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>به راه عشق که هرگز به سر نمی‌آید</p></div>
<div class="m2"><p>به غیر گم شدن از راهبر نمی‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه عقل در اصلاح نفس عاجز بود</p></div>
<div class="m2"><p>که پندگوی به دیوانه بر نمی‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به است پایی کز وی برآید آبله‌ای</p></div>
<div class="m2"><p>ز دست ما که ازو هیچ بر نمی‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن کمر نتوانم دمی نظر بستن</p></div>
<div class="m2"><p>ز نازکی به نظر گرچه در نمی‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یگانگی که نفاقی در آن میان نبود</p></div>
<div class="m2"><p>درین زمانه ز شیر و شکر نمی‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سیل خود خبر خود برم به هر وادی</p></div>
<div class="m2"><p>خبر ز گرم‌روان پیشتر نمی‌آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به روزگار چنان عیب شد سلامت نفس</p></div>
<div class="m2"><p>کم غیر کار شرر از گهر نمی‌آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دهر دانش و سامان سوال کردم گفت</p></div>
<div class="m2"><p>که از نهال هنر برگ و بر نمی‌آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیال آن کمر از سر نمی‌رود چه کنم</p></div>
<div class="m2"><p>که مو ز کاسه چینی به در نمی‌آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلیم در دل اگر شعله‌ای ز شوق بود</p></div>
<div class="m2"><p>به سوی لب نفس بی‌اثر نمی‌آید</p></div></div>