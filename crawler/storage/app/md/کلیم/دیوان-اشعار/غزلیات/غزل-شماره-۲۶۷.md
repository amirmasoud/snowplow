---
title: >-
    غزل شمارهٔ  ۲۶۷
---
# غزل شمارهٔ  ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>گهی که بر لب او چشم اشکبار افتد</p></div>
<div class="m2"><p>دلم ز دیده نمکسود در کنار افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز جنگجوئی او ایمنم ز کینه دهر</p></div>
<div class="m2"><p>نمی گذارد نوبت بروزگار افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک بخشک نبسته است آنچنان کشتی</p></div>
<div class="m2"><p>که اشک حسرت ما نیز آبدار افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز دوری تو بچشمم سیاه شد عالم</p></div>
<div class="m2"><p>بسان آینه ای کان بزنگبار افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهشت دست جنون دامنی که بند شود</p></div>
<div class="m2"><p>بخار زار علایق اگر گذار افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بچشم مست تو خون را حلال باید کرد</p></div>
<div class="m2"><p>که ترک عربده جوید چو در خمار افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا ز صید دل ما چراست اینهمه عار</p></div>
<div class="m2"><p>نه پادشاه گهی در پی شکار افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بمن زیاده ازین چهره شعله خیز مکن</p></div>
<div class="m2"><p>چه لازمست که آتش بگوشوار افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زرشک روی تو گلشن چنان خورد بر هم</p></div>
<div class="m2"><p>که آشیانه مرغان ز شاخسار افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نجات غرقه بحر تعلق آسان نیست</p></div>
<div class="m2"><p>مگر ز تخته تابوت بر کنار افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلیم عجز من و آن غرور یار همند</p></div>
<div class="m2"><p>بسان گرد که دایم پی سوار افتد</p></div></div>