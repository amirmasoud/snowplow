---
title: >-
    غزل شمارهٔ  ۵۴۰
---
# غزل شمارهٔ  ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>قربان آن بناگوش، وان برق گوشواره</p></div>
<div class="m2"><p>با هم چه خوش نمایند آن صبح و این ستاره</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مائیم و کهنه دلقی دلگیر از دو عالم</p></div>
<div class="m2"><p>سر چون جرس کشیده در جیب پاره پاره</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کار رفت از دست، گیرد سپهر دستت</p></div>
<div class="m2"><p>دریا غریق مرده، افکنده بر کناره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز از برم چو رفتی شب آمدی بخوابم</p></div>
<div class="m2"><p>اینست اگر کسی را، عمری بود دوباره</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روشندلان ندارند دلبستگی بفرزند</p></div>
<div class="m2"><p>بر شعله سهل باشد مهجوری شراره</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن نشئه ایکه بخشد بگذشتن از دو عالم</p></div>
<div class="m2"><p>در کیش میکشان چیست یک مستی گذاره</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با چرخ سرفرازی نتوان ز پیش بردن</p></div>
<div class="m2"><p>جائیکه سقف پست است نتوان شدن سواره</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچون کلیم دیگر یک نامشخصی کو</p></div>
<div class="m2"><p>آگاه و مست غفلت پر شغل و هیچکاره</p></div></div>