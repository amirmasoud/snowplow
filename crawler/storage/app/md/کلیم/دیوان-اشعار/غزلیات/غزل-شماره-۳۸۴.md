---
title: >-
    غزل شمارهٔ  ۳۸۴
---
# غزل شمارهٔ  ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>کشش اوست که ما را بسر کار برد</p></div>
<div class="m2"><p>بلبل از نکهت گل راه بگلزار برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در میکده مستی بترنم می گفت</p></div>
<div class="m2"><p>باده آبیست که از آینه زنگار برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سود این داد و ستد چیست که در خلوت قرب</p></div>
<div class="m2"><p>فرصت حرف دهد قوت گفتار برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>استخوانم نشود پیش خدنگ تو سفید</p></div>
<div class="m2"><p>گر نه زخمم گرو خنده ز سوفار برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک چمن آب خورد از عرق خجلت گل</p></div>
<div class="m2"><p>نکهت زلف تو گر باد بگلزار برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مژه را داد ز کف چشم تو در آخر حسن</p></div>
<div class="m2"><p>ترک مفلس چو شود تیغ بازار برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شور بختیم و شهید لب او کاش کسی</p></div>
<div class="m2"><p>استخوانهای مرا سوی نمکزار برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاب بیداد کلیم اینهمه چون می آرد</p></div>
<div class="m2"><p>گر نه دل میدهدش آنکه دل از کار برد</p></div></div>