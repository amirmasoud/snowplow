---
title: >-
    غزل شمارهٔ  ۹۲
---
# غزل شمارهٔ  ۹۲

<div class="b" id="bn1"><div class="m1"><p>پیوسته دل ز قطع امید آرمیده است</p></div>
<div class="m2"><p>راحت درین چمن بر نخل بریده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبرم بجستن دل گمگشته رفته است</p></div>
<div class="m2"><p>طفل سرشک در پی رنگ پریده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با گریه خنده رویم و با ناله گرم خون</p></div>
<div class="m2"><p>باز از شراب غصه دماغم رسیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاد است بخت بد که بمفتم زدست داد</p></div>
<div class="m2"><p>گوئی مرا فروخته یوسف خریده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیمزد دست، خار ز پائی نمی کشد</p></div>
<div class="m2"><p>همراهی زمانه بدینجا کشیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند نیش عقربی از دخل کج خورم</p></div>
<div class="m2"><p>کسب کمال شعر دلم را گزیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رنگین سخن گمان نبری خویشرا، کلیم</p></div>
<div class="m2"><p>کز خامه بریده زبان خون چکیده است</p></div></div>