---
title: >-
    غزل شمارهٔ  ۴۰۹
---
# غزل شمارهٔ  ۴۰۹

<div class="b" id="bn1"><div class="m1"><p>بروی مرهم مرهم نهیم بر دل ریش</p></div>
<div class="m2"><p>که زخم بر سر زخمست و نیش بر سر نیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر ببادیه چون بیکسان هلاک شویم</p></div>
<div class="m2"><p>زگردباد به بندیم نخل ماتم خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پر است خاطر آن بیوفا ز کینه ما</p></div>
<div class="m2"><p>بغایتی که نگردد ز حرف دشمن بیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخون فشانی چشم بهانه جوست چنان</p></div>
<div class="m2"><p>که خون زدیده جهد بر رگم زپیکر نیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم ز ناز و نعیم جهان ندارد رنگ</p></div>
<div class="m2"><p>چه جای نقش و نگارست خانه درویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلیم بهر خط زخم دلبران تن را</p></div>
<div class="m2"><p>زدیم مسطری از استخوان پهلوی خویش</p></div></div>