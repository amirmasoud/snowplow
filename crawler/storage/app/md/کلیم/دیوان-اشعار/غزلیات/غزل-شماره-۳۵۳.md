---
title: >-
    غزل شمارهٔ  ۳۵۳
---
# غزل شمارهٔ  ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>بت پیمان شکم دم از وفا زد</p></div>
<div class="m2"><p>اثر نقشی بر آب گریه ها زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوشا آسایش دردی که ما را</p></div>
<div class="m2"><p>چنان گیرد که نتوان دست و پا زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز درد رشک همکاران کبابیم</p></div>
<div class="m2"><p>بمجلس اشک شمع آتش بما زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بخت تیره روز هر که شب شد</p></div>
<div class="m2"><p>بجای شمع آتش در سرا زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا آب بقا نبود سیه روز</p></div>
<div class="m2"><p>که راه راحت آباد فنا زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قمار پاکبازی خوش نشین شد</p></div>
<div class="m2"><p>دوشش فقرم ز نقش بوریا زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکر خند گل ساغر صدا داشت</p></div>
<div class="m2"><p>حریفان صبوحی را صلا زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خدنگ آه چون تیر هوائیست</p></div>
<div class="m2"><p>کزو نتوان شکار مدعا زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سموم عشق رخت هستیم سوخت</p></div>
<div class="m2"><p>در آن وادی که مجنون را هوا زد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کلیم از مطلب نایاب بگذشت</p></div>
<div class="m2"><p>به دست آورده را هم پشت پا زد</p></div></div>