---
title: >-
    غزل شمارهٔ  ۵۰۶
---
# غزل شمارهٔ  ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>کار دوران چیست جمعیت پریشان ساختن</p></div>
<div class="m2"><p>سیل مجبورست در معموره ویران ساختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاک طینت را بکین کس نشاید گرم کرد</p></div>
<div class="m2"><p>بهر خونریز از طلا شمشیر نتوان ساختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر طبیب همت ایام عیسی دم شود</p></div>
<div class="m2"><p>باید از وی درد فقر خویش پنهان ساختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ابر اگر از طینت اهل جهان آگه شود</p></div>
<div class="m2"><p>قطره سازی را بدل سازد به پیکان ساختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترک دنیا پیش این دنیاپرستان کافریست</p></div>
<div class="m2"><p>چون بکیش هندوان بتخانه ویران ساختن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گریه ما را گر میرابی گلشن دهند</p></div>
<div class="m2"><p>عاجز آید نوبهار از غنچه ویران ساختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با همه ناقابلی دارد هنرها بخت ما</p></div>
<div class="m2"><p>می تواند از گل و ریحان مغیلان ساختن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیکدورت راحت از گیتی نشاید چشم داشت</p></div>
<div class="m2"><p>زانکه ناچارست با دود چراغان ساختن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نار پستان دست فرسود هوا باشد کلیم</p></div>
<div class="m2"><p>بعد از این خواهیم با سیب زنخدان ساختن</p></div></div>