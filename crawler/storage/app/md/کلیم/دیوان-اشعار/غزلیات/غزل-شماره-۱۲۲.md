---
title: >-
    غزل شمارهٔ  ۱۲۲
---
# غزل شمارهٔ  ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>دل کار خود بطالع ناساز واگذاشت</p></div>
<div class="m2"><p>شمع اختیار خویش بباد صبا گذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ماندگان بساز که کفر طریقتست</p></div>
<div class="m2"><p>رهرو اگر نشان قدم را بجا گذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل را شکفته در چمن دهر کس ندید</p></div>
<div class="m2"><p>تا غنچه خنده را بلب یار وا گذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خونم ز بس سرشته مهر و وفا شدست</p></div>
<div class="m2"><p>رنگش نرفت آنکه بدست این حنا گذاشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نفس پیش چو خامه سیه شد ز دود دل</p></div>
<div class="m2"><p>سرگرم اشتیاق تو هر جا که پا گذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هر کرانه برق بلا در وزیدنست</p></div>
<div class="m2"><p>باید کلیم بخت سیه را بما گذاشت</p></div></div>