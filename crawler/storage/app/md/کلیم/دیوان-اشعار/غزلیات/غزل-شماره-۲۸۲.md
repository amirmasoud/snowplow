---
title: >-
    غزل شمارهٔ  ۲۸۲
---
# غزل شمارهٔ  ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>چند دل تلخی غم را شکرستان داند</p></div>
<div class="m2"><p>خاک را بر سر سودازده سامان داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر حق راه طلب را بشناسد سالک</p></div>
<div class="m2"><p>دیده را خاتم انگشت مغیلان داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که سوداگر کالای وفا شد باید</p></div>
<div class="m2"><p>که کسادی را آرایش دکان داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاهل ار خود را دانسته بچاه اندازد</p></div>
<div class="m2"><p>از جفای فلک و گردش دوران داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کرا تنگدلی عینک بینائی داد</p></div>
<div class="m2"><p>صبح را تیره تر از شام غریبان داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل که از چاشنی درد خبردار بود</p></div>
<div class="m2"><p>پاس غمهای ترا خدمت مهمان داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پند گو ترک من غمزده نتواند کرد</p></div>
<div class="m2"><p>وز بهشتی چو تو قطع نظر آسان داند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرد بیداد کلیم است که بر تارک خویش</p></div>
<div class="m2"><p>سایه تیغ ترا سنبل و ریحان داند</p></div></div>