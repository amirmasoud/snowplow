---
title: >-
    غزل شمارهٔ  ۹۵
---
# غزل شمارهٔ  ۹۵

<div class="b" id="bn1"><div class="m1"><p>ما را طپیدن از غم دنیا شعار نیست</p></div>
<div class="m2"><p>صد شکر کاب طینت ما موج دار نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جذبه جنون نرسد کس بهیچ جا</p></div>
<div class="m2"><p>سالک براه ماند اگر نی سوار نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشندلان حباب سفت دیده بسته اند</p></div>
<div class="m2"><p>روزن چه احتیاج اگر شیشه تار نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن را که دل ز مشرب منصور آب خورد</p></div>
<div class="m2"><p>کشکول فقر را بجز از چوب دار نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطع امید کرده نخواهد نعیم دهر</p></div>
<div class="m2"><p>شاخ بریده را نظری بر بهار نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را که باشد آتش شوقی، بغم چکار</p></div>
<div class="m2"><p>آئینه گداخته جای غبار نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجلس فروز گبر و مسلمان یک آتشست</p></div>
<div class="m2"><p>در سنگ دیر و کعبه بجز یک شرار نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لوح مزار خویش ز دیوان خود کنم</p></div>
<div class="m2"><p>یعنی مرا بغیر سخن یادگار نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در گلشنی که عشق بود باغبان کلیم</p></div>
<div class="m2"><p>جز آشیان سوخته بر شاخسار نیست</p></div></div>