---
title: >-
    غزل شمارهٔ  ۱۲۱
---
# غزل شمارهٔ  ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>زلف تو که طفلان هوس را شب عیدست</p></div>
<div class="m2"><p>شامیست که آبستن صد صبح امیدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رفته، باو نامه ننوشته فرستم</p></div>
<div class="m2"><p>یعنی که زهجران توام دیده سفیدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقل سر فرمان نکشد از خط ساغر</p></div>
<div class="m2"><p>پیر است شراب کهن و عقل مریدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من مست بهشیاری چشم تو ندیدم</p></div>
<div class="m2"><p>مدهوش ولی با همه در گفت و شنیدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس تنم از فرقت می در رمضان کاست</p></div>
<div class="m2"><p>انگشت نماتر ز هلال شب عیدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما تشنه یکقطره، تو سیلاب محیطی</p></div>
<div class="m2"><p>ساقی قدح نیمه زلطف تو بعیدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سهلست کلیم از همه پیوند بریدن</p></div>
<div class="m2"><p>چیزیکه بود مشکل ازو قطع امیدست</p></div></div>