---
title: >-
    غزل شمارهٔ  ۳۷
---
# غزل شمارهٔ  ۳۷

<div class="b" id="bn1"><div class="m1"><p>شهید آن قد رعنا وصیت کرد همدم را</p></div>
<div class="m2"><p>که بندد نیزه بالا در عزایش نخل ماتم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر گویم که خاتم چون دهان اوست، از شادی</p></div>
<div class="m2"><p>شود به زخم ناسورش علم سازد قد خم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدانی تا که شهد زندگانی نیست بی تلخی</p></div>
<div class="m2"><p>خدا در سال عمرت داده جا ماه محرم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درشتند اهل عالم خواه شهری خواه صحرائی</p></div>
<div class="m2"><p>قضا ناپخته گل کردست گوئی خاک آدم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو هم از فیض خاموشی چو غواصان گهر یابی</p></div>
<div class="m2"><p>نگهداری گر از بیهوده گوئی یکنفس دم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک می آورد ما را برون از کوره محنت</p></div>
<div class="m2"><p>ولی روزیکه خود بیرون کند این رخت ماتم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنرمی چاره داغ جفای دوستداران کن</p></div>
<div class="m2"><p>که داخل گر نباشد موم نفعی نیست مرهم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علاج دیده بی آب جستم از خرد، گفتا</p></div>
<div class="m2"><p>مقابل دار با خورشید روئی چشم بی نم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبینی پایه پستی که کس نبود طلبکارش</p></div>
<div class="m2"><p>شرر این آرزو دارد که یابد عمر شبنم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بغیر از خانه ویران سازی و رخت سرا سوزی</p></div>
<div class="m2"><p>کلیم آخر چه حاصل آتشین اشک دمادم را</p></div></div>