---
title: >-
    غزل شمارهٔ  ۵۴۹
---
# غزل شمارهٔ  ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>جنون تا بداد اسیران رسیده</p></div>
<div class="m2"><p>ز داغش چه سرها بسامان رسیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم از هر طرف ساغری پیشم آرد</p></div>
<div class="m2"><p>چو هشیار در بزم مستان رسیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه از لخت دل خانه ام گلستان شد</p></div>
<div class="m2"><p>کزین گل بخار بیابان رسیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق تماشای تو باز گشته</p></div>
<div class="m2"><p>بچشمم سرشک بدامان رسیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بچشم من از هر نسیمی که آید</p></div>
<div class="m2"><p>سلامی ز خار مغیلان رسیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بر گشتگی های بخت سیاهم</p></div>
<div class="m2"><p>خبرها بآن زلف و مژگان رسیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلیم از نگون بختی خود چه نالی</p></div>
<div class="m2"><p>ببین ناله ات را بکیوان رسیده</p></div></div>