---
title: >-
    غزل شمارهٔ  ۴۸۴
---
# غزل شمارهٔ  ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>طالع وارون بر آن برگشته‌مژگان بسته‌ایم</p></div>
<div class="m2"><p>گرچه بی‌قدریم خود را بر عزیزان بسته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موری از تاب کمر ما را تواند صید کرد</p></div>
<div class="m2"><p>چشم همت گرچه از ملک سلیمان بسته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده گر سیراب شد، دل تشنه یک قطره ماند</p></div>
<div class="m2"><p>خانه ویران کرده تا آیین دکان بسته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه دام تعلق مزرع گیتی نداشت</p></div>
<div class="m2"><p>ما به امید چه یارب دل به دوران بسته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاطر آشفته ما هست عیب روزگار</p></div>
<div class="m2"><p>بر سر ایام دستار پریشان بسته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما و می در این چمن چون توبه فصل بهار</p></div>
<div class="m2"><p>روز اول با شکستن عهد و پیمان بسته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از شکسته کشتی ما تا گهی یاد آورد</p></div>
<div class="m2"><p>رشته‌های موج بر انگشت طوفان بسته‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در حصار آهن ما غم نخواهد راه کرد</p></div>
<div class="m2"><p>رخنه‌های سینه را یکسر ز پیکان بسته‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خار مژگان را به چشم کم مبین دیگر کلیم</p></div>
<div class="m2"><p>چار موسم از گلش نخل شهیدان بسته‌ایم</p></div></div>