---
title: >-
    غزل شمارهٔ  ۳۹۰
---
# غزل شمارهٔ  ۳۹۰

<div class="b" id="bn1"><div class="m1"><p>صاحب همت که دست از کار دنیا می‌کشد</p></div>
<div class="m2"><p>کی دگر زان دست خار یأس از پا می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ستم بر ناتوانان بالد آن سرکش به خویش</p></div>
<div class="m2"><p>شعله چون مشت خسی را سوخت بالا می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه از باطن صافست محنت‌کش ز زنگ</p></div>
<div class="m2"><p>شیشه از روشندلی بیداد خارا می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک‌ریزان تا غبار جلوه کاهش رفته‌اند</p></div>
<div class="m2"><p>زلف را در خون کشد گاهی که تا پا می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاهلان را فخر می‌باید ز جهل خود که دهر</p></div>
<div class="m2"><p>انتقام جرم نادان را ز دانا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما به این سامان چرا شرمنده باشیم از سپهر</p></div>
<div class="m2"><p>گوهر بی‌آب کی منت ز دریا می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا قلم برداشت قمری آشیان خواهد نهاد</p></div>
<div class="m2"><p>سر و بالای ترا نقاش هرجا می‌کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشمنی را باعثی باید، نمی‌دانم کلیم</p></div>
<div class="m2"><p>اشک از بهر چه لشکر بر سر ما می‌کشد</p></div></div>