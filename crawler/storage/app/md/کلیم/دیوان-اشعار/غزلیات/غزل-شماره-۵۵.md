---
title: >-
    غزل شمارهٔ  ۵۵
---
# غزل شمارهٔ  ۵۵

<div class="b" id="bn1"><div class="m1"><p>عشق را بخت تیره در کارست</p></div>
<div class="m2"><p>جلوه شمع در شب تارست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش بگرد سر تو می گردد</p></div>
<div class="m2"><p>جگرم خون ز رشک دستارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه بازار خار و خس گرم است</p></div>
<div class="m2"><p>شاهد گل غریب گلزارست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رشک ابروی تو زکارش برد</p></div>
<div class="m2"><p>پشت محراب زان بدیوارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موبمویم ز بسکه مضطربست</p></div>
<div class="m2"><p>کوکب داغ سینه سیارست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه بی ناوکی نخواهد ماند</p></div>
<div class="m2"><p>مرغ این آشیانه بسیارست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست مژگان بگرد چشم کلیم</p></div>
<div class="m2"><p>در رهت پای دیده بر خارست</p></div></div>