---
title: >-
    غزل شمارهٔ  ۳۹۳
---
# غزل شمارهٔ  ۳۹۳

<div class="b" id="bn1"><div class="m1"><p>تا تیغ او به داد اسیران نمی‌رسد</p></div>
<div class="m2"><p>یک سر به کوی عشق به سامان نمی‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که پای خاطر من در میان بود</p></div>
<div class="m2"><p>آشفتگی به زلف پریشان نمی‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خود چو نگذری به مرادی نمی‌رسد</p></div>
<div class="m2"><p>سر تا بریده نیست به سامان نمی‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بیت ابروی تو که بی‌عیب آمده است</p></div>
<div class="m2"><p>جز دخل کج به خاطر مژگان نمی‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما طفل بوده‌ایم و شب جمعه دیده‌ایم</p></div>
<div class="m2"><p>هرگز به صبح شنبه مستان نمی‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک حرف بیش نیست سراسر بیان عشق</p></div>
<div class="m2"><p>این طرفه تر که هیچ به پایان نمی‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوتاهی زمانه به جایی رسیده است</p></div>
<div class="m2"><p>کز می دماغ باده‌پرستان نمی‌رسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شیشه شکسته ز ما دست شسته‌اند</p></div>
<div class="m2"><p>اصلاح ما به خاطر یاران نمی‌رسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پهلو تهی کنند ز مست برهنه تیغ</p></div>
<div class="m2"><p>ز آن رو نگاه یار به مژگان نمی‌رسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شعرت کلیم اگر همه شعری نسب بود</p></div>
<div class="m2"><p>نبود بلند تا به سخن‌دان نمی‌رسد</p></div></div>