---
title: >-
    غزل شمارهٔ  ۴۳۶
---
# غزل شمارهٔ  ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>بیجوهریم و دست ز شمشیر می بریم</p></div>
<div class="m2"><p>موریم و پنجه هنر از شیر می بریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داریم تحفه تو دل پاره پاره ای</p></div>
<div class="m2"><p>سودا ببین، که لاله بکشمیر می بریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا عاقلان بمأمن تدبیر می رسند</p></div>
<div class="m2"><p>ما رخت خود بخانه زنجیر می بریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهیم خو گرفت بروز سیاه خویش</p></div>
<div class="m2"><p>ما تیرگی ز بخت بتدبیر می بریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار مجردان طریقت سبک خوشست</p></div>
<div class="m2"><p>از ناله ها گرانی تأثیر می بریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با آنکه احتیاج ندارند می خرند</p></div>
<div class="m2"><p>چندانکه ما خجالت تقصیر می بریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در انتخاب وادی آوارگیست بخت</p></div>
<div class="m2"><p>زان دردسر ز خاک درت دیر می بریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پنهان نمی کنیم زدشمن متاع خویش</p></div>
<div class="m2"><p>مشت پری که هست بر تیر می بریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را کلیم گرمی تب آب و آتشست</p></div>
<div class="m2"><p>کی تشنگی ز دل بطباشیر می بریم</p></div></div>