---
title: >-
    غزل شمارهٔ  ۳۲۰
---
# غزل شمارهٔ  ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>از جهان بخت بابرام گدا می خواهد</p></div>
<div class="m2"><p>مشت خاکی که برای سر ما می خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ازین عمر سیه روز بتنگ آمده است</p></div>
<div class="m2"><p>شمع کوتاهی شب را زخدا می خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرم از افسر و از ظل هما بیزارست</p></div>
<div class="m2"><p>موی ژولیده و سودای رسا می خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه خار رهت از پای کشیدن حیفست</p></div>
<div class="m2"><p>چکنم گر نکشم آبله جا می خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود صاحب همت که زاهل طمعست</p></div>
<div class="m2"><p>تنگ چشمی که اجابت زدعا می خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این خسیسان که تو بینی بجهان در کارند</p></div>
<div class="m2"><p>که خس و خار زسیلاب فنا می خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنقدر می رود از راه برون مرشد شهر</p></div>
<div class="m2"><p>که گر از هوش رود راهنما می خواهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین تلون که فلک را بنهادست کلیم</p></div>
<div class="m2"><p>نتوان یافت که رو کرده کرا می خواهد</p></div></div>