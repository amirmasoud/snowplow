---
title: >-
    غزل شمارهٔ  ۴۲
---
# غزل شمارهٔ  ۴۲

<div class="b" id="bn1"><div class="m1"><p>چند از شرم تو باشد در نقاب</p></div>
<div class="m2"><p>رخ بپوشان تا برآید آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر هر عضو من دردت نهاد</p></div>
<div class="m2"><p>نقطه داغی نشان انتخاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا در آب افتاده عکس عارضت</p></div>
<div class="m2"><p>می نیاسودست موج از اضطراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر بیاض دیده از خون جگر</p></div>
<div class="m2"><p>می نویسم خط بیزاری خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند هر شام در تحت الثری</p></div>
<div class="m2"><p>خاک از رشک تو بر سر آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دسته گل تحفه می آرد نسیم</p></div>
<div class="m2"><p>تا برد از سینه ام بوی کباب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب کلیم از دیده می بارد سرشک</p></div>
<div class="m2"><p>روز از منزل برون می ریزد آب</p></div></div>