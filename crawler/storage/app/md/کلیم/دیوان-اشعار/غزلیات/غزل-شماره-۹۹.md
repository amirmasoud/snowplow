---
title: >-
    غزل شمارهٔ  ۹۹
---
# غزل شمارهٔ  ۹۹

<div class="b" id="bn1"><div class="m1"><p>ز اختر طالع که مهر او همه کین است</p></div>
<div class="m2"><p>خیر ندیدیم اگر چه خیر درین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوست بهیچم فروخت با همه زاری</p></div>
<div class="m2"><p>یار فروشی درین زمانه همین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه حسن و عشق روی برویند</p></div>
<div class="m2"><p>شوری بختم از آن لب نمکین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده عزیزست از سرشک جگر گون</p></div>
<div class="m2"><p>قیمت خاتم باعتبار نگین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل ما از غبار محنت گیتی</p></div>
<div class="m2"><p>زخم جفاها چو جاده خاک نشین است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوبی ظاهر مخر بهیچ که دریا</p></div>
<div class="m2"><p>دشمن جان آمد و گشاده جبین است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت حال مرا چو روی نکویان</p></div>
<div class="m2"><p>زلف پریشانی از یسار و یمین است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ریش بقدر عصا گذار که امروز</p></div>
<div class="m2"><p>کوتهی ریش هتک حرمت دین است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل پر کلفت کلیم ز هجران</p></div>
<div class="m2"><p>بسکه غبارست نقد داغ دفین است</p></div></div>