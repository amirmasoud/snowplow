---
title: >-
    غزل شمارهٔ  ۱۹۱
---
# غزل شمارهٔ  ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>از غمی شکوه نکن تا غم دیگر ندهند</p></div>
<div class="m2"><p>از لب خشک مگو تا مژه تر ندهند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوبرویان چو نشینند در ایوان غرور</p></div>
<div class="m2"><p>منصب آینه داری بسکندر ندهند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیاری که رهائی زاسیری مرگست</p></div>
<div class="m2"><p>صید تا لایق کشتن نشود سر ندهند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط آزادی ما از غم دوران که دهد</p></div>
<div class="m2"><p>ساقیان باده اگر تا خط ساغر ندهند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاجت از فقر طلب روی طلب گر داری</p></div>
<div class="m2"><p>که زیک در دهدت آنچه ز صد در ندهند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه خود گشته زن حرص و طمع می گوید</p></div>
<div class="m2"><p>مفتی شهر که یکزن بدو شوهر ندهند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جامه عرض نکویان چو درد نتوان دوخت</p></div>
<div class="m2"><p>زانکه پیراهن گل را برفوگر ندهند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از سخن غیر زیان نفع سخنور نبود</p></div>
<div class="m2"><p>بصدف جوهریان قیمت گوهر ندهند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دیاریکه بود گردش آنچشم کلیم</p></div>
<div class="m2"><p>نسبت فتنه ببدگردی اختر ندهند</p></div></div>