---
title: >-
    غزل شمارهٔ  ۵۷۲
---
# غزل شمارهٔ  ۵۷۲

<div class="b" id="bn1"><div class="m1"><p>خموش باش دلا عرض مدعا کردی</p></div>
<div class="m2"><p>زبان به بند، سر گریه را چو وا کردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شوخی ارچه بیکجا قرار نیست ترا</p></div>
<div class="m2"><p>برون نمی روی از خاطری که جا کردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگلشن از قدمت داغ لاله مرهم یافت</p></div>
<div class="m2"><p>بخنده هم گره از کار غنچه وا کردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزیر خاک تب هجر و رنج رشک بجاست</p></div>
<div class="m2"><p>کدام درد مرا ای اجل دوا کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بناله ام دل صد مرغ می کشد آنجا</p></div>
<div class="m2"><p>مرا برای چه از دام خود رها کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوشم که دفتر دل نم کشیده بود ز خون</p></div>
<div class="m2"><p>به تیغ هر ورقش را ز هم جدا کردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه شاعرم ار کرد زو نمی رنجم</p></div>
<div class="m2"><p>چه کردمی اگرم شاعر گدا کردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین زمانه که مرغ کباب در قفس است</p></div>
<div class="m2"><p>کلیم فکر رهائی تو از کجا کردی</p></div></div>