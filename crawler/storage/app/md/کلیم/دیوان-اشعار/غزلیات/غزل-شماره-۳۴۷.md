---
title: >-
    غزل شمارهٔ  ۳۴۷
---
# غزل شمارهٔ  ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>تا تو رفتی جان دگر آمیزشی با تن نکرد</p></div>
<div class="m2"><p>عکس در آئینه بیصورت دمی مسکن نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پاک طینت با گرانان سازگاری می کند</p></div>
<div class="m2"><p>آب آهنگ جدائی هرگز از آهن نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفلسان را کس نمی خواهد، زمینا کن قیاس</p></div>
<div class="m2"><p>تا تهی شد دیگرش کس دست در گردن نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توده خاکستر دلها بگردون تا نرفت</p></div>
<div class="m2"><p>روزگار آئینه خورشید را روشن نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسکه بی آرامیم در عشق او تأثیر داشت</p></div>
<div class="m2"><p>کینه ام یک لحظه جا در خاطر دشمن نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبزه گل را که بینی آتش و خاکسترست</p></div>
<div class="m2"><p>چشم یک بین امتیاز گلشن از گلخن نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گلستان هم دل خرم نباید داشتن</p></div>
<div class="m2"><p>غنچه تا نشکفت کس بیرونش از گلشن نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بس که با تاریکی شب‌ها کلیم الفت گرفت</p></div>
<div class="m2"><p>خانه روشن از چراغ وادی ایمن نکرد</p></div></div>