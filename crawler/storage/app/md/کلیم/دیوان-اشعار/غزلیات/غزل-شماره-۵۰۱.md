---
title: >-
    غزل شمارهٔ  ۵۰۱
---
# غزل شمارهٔ  ۵۰۱

<div class="b" id="bn1"><div class="m1"><p>همین نه در سفر آشفته تر ز سیلابم</p></div>
<div class="m2"><p>که در وطن همه سر گشته تر ز گردابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا فریب شراب هوس خورم که چو شمع</p></div>
<div class="m2"><p>تمام عمر بیکقطره آب سیرابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سر نهادن و از سر گذشتن است سجود</p></div>
<div class="m2"><p>به کیش من که خم تیغ اوست محرابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه رهبر و نه رفیق و نه منزلست مرا</p></div>
<div class="m2"><p>براه شوق عنان بر عنان سیلابم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدست عشق یکی ساز دلخراشم من</p></div>
<div class="m2"><p>که تارم از رگ جان، نشتر است مضرابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا ز وضع نو غفلت زیاده شد ناصح</p></div>
<div class="m2"><p>زبان بیند کز افسانه می برد خوابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بر و بحرم سرگشتگی رفیق رهست</p></div>
<div class="m2"><p>گمان برم که خس گردباد گردابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه تیغ نیم روزگار دریا دل</p></div>
<div class="m2"><p>در آتشم فکند تا دمی دهد آبم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز اشک و آه که یارب زیاده باد کلیم</p></div>
<div class="m2"><p>همیشه آتش سامان و سیل اسبابم</p></div></div>