---
title: >-
    غزل شمارهٔ  ۴۳۷
---
# غزل شمارهٔ  ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>نه همین از بخت بد طوفان ز عمان دیده‌ام</p></div>
<div class="m2"><p>دایم از جوش تری از قطره طغیان دیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد خلل در راحت تنهاییم افتاد اگر</p></div>
<div class="m2"><p>زآشنایان گردبادی در بیابان دیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از غم بی‌خانمانی گریه‌ام رو داده است</p></div>
<div class="m2"><p>آشیان بلبلی گر در گلستان دیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شانه تاری چند از زلفت به چنگ آورد و من</p></div>
<div class="m2"><p>حاصلی گردیده‌ام، خواب پریشان دیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکوه بخت از زبانم سر نزد، گویی که من</p></div>
<div class="m2"><p>در سواد تیره‌بختی آب حیوان دیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هدف صابرترم هرجا بلایی رو دهد</p></div>
<div class="m2"><p>شکرباران کرده‌ام گر تیرباران دیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک ما از گرمی شوق دگر آید به وجد</p></div>
<div class="m2"><p>رقص آزادی طفلان از دبستان دیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>استخوان من قناعت بر هما شیرین کند</p></div>
<div class="m2"><p>زین شکرریزی کزان لب‌های خندان دیده‌ام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌توان دریافت فیض سینه‌چاکی را کلیم</p></div>
<div class="m2"><p>زین گشایش‌ها که از چاک گریبان دیده‌ام</p></div></div>