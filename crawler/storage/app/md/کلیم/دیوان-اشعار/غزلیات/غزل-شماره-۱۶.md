---
title: >-
    غزل شمارهٔ  ۱۶
---
# غزل شمارهٔ  ۱۶

<div class="b" id="bn1"><div class="m1"><p>بی‌تو از گلشن چه حاصل خاطر افسرده را</p></div>
<div class="m2"><p>خنده گل دردسر می‌آورد آزرده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساغری خواهم دم آخر مگر همراه او</p></div>
<div class="m2"><p>سوی تن باز آورم جان به لب آورده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه همین بی‌سوز عشقست، از هوس هم گرم نیست</p></div>
<div class="m2"><p>سینه تابوتست گویی زاهد دل‌مرده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کاغذ غم‌نامه را کردم حنایی از سرشک</p></div>
<div class="m2"><p>تا به یاد او دهم چشم به خون پرورده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت ظاهر اگر در حسن باشد، آفتاب</p></div>
<div class="m2"><p>آورد تاریکی دل پی به معنی برده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل مکن از دوست گر خواهی به او پیوست باز</p></div>
<div class="m2"><p>کس به گلبن تا یکی بندد گل افسرده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ز خاک خاکساری گل دمیدن سر کند</p></div>
<div class="m2"><p>سر شود یک دسته گل خاک بر سر کرده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم مست او کجا پروای دل دارد کلیم</p></div>
<div class="m2"><p>هیچ نسبت نیست با می خورده پیکان خورده را</p></div></div>