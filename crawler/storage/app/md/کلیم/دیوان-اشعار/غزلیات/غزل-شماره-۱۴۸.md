---
title: >-
    غزل شمارهٔ  ۱۴۸
---
# غزل شمارهٔ  ۱۴۸

<div class="b" id="bn1"><div class="m1"><p>در طریق خودنمائی شیوه دلخواه نیست</p></div>
<div class="m2"><p>غیر دعوی بلند و همت کوتاه نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کیسه ای بر وعده های بخت نتوان دوختن</p></div>
<div class="m2"><p>خفته گر در خواب حرفی گفت از آن آگاه نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نفاق صحبت مردم ز بس رم کرده ایم</p></div>
<div class="m2"><p>ناله ما نیز با خضر اثر همراه نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاطر آشفته ای دارم که هر ساعت نفس</p></div>
<div class="m2"><p>راه لب را می کند گم گر چراغ آه نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه ترکش می توان کردن، بدست آورده گیر</p></div>
<div class="m2"><p>غم زناکامی نباشد همت ار کوتاه نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرگ تلخ و زندگی هم سربسر درد و غمست</p></div>
<div class="m2"><p>پشت و روی کار عالم هیچگه دلخواه نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کعبه عشق تو پنداری سر کوی فناست</p></div>
<div class="m2"><p>می توان رفتن ولی در بازگشتن راه نیست</p></div></div>