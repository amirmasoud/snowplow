---
title: >-
    غزل شمارهٔ  ۲۹۱
---
# غزل شمارهٔ  ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>نه ز می هرجا تنک‌ظرفی که برد از پا فتاد</p></div>
<div class="m2"><p>آنکه لاف پهلوانی زد هم از صهبا فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردباد از سیر صحرا پای در دامن کشید</p></div>
<div class="m2"><p>نوبت هامون‌نوردی تا با شک ما فتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه نبود دیده‌ام گر دجله‌افشانی کند</p></div>
<div class="m2"><p>کآب در چشمم ز دود آتش سودا فتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دم آخر بود سر در هوا مانند شمع</p></div>
<div class="m2"><p>دیده هرکس که بر آن قامت زیبا فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌دهد ز آشفتگی درس سیه‌روزی ز ما</p></div>
<div class="m2"><p>زلف او با این پریشانی چه خوش انشا فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عندلیب آن گلستانم که بندد باغبان</p></div>
<div class="m2"><p>دیده را هر گه نقاب از چهره گل‌ها فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه در راه طلب خو کرد با آوارگی</p></div>
<div class="m2"><p>گر به سان شمع یک جا شد مقیم از پا فتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کمال اتحاد حسن و عشق آخر کلیم</p></div>
<div class="m2"><p>هر گره کز زلف او وا شد به کار ما فتاد</p></div></div>