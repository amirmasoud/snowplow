---
title: >-
    غزل شمارهٔ  ۳۵۰
---
# غزل شمارهٔ  ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>به بزمت شب خوش آن عاشق که سرگرم فغان افتد</p></div>
<div class="m2"><p>شود چون صبح روشن راست چون شمع از زبان افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چمن از بس که تاریکست بی‌شمع جمال او</p></div>
<div class="m2"><p>فروزم گر چراغ ناله مرغ از آشیان افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خلوت هم نقاب از چهره هرگز برنیندازد</p></div>
<div class="m2"><p>مبادا شمع را زین بیشتر آتش به جان افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبول عشق اگر داری طمع، از خرمی بگذر</p></div>
<div class="m2"><p>که گل چون بشکفد اینجا ز چشم باغبان افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر بر هم خورد عالم همان بر جای خود باشم</p></div>
<div class="m2"><p>نخواهد بردنش گر سایه در آب روان افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سوز سینه پرناوکم گاهی نگاهی کن</p></div>
<div class="m2"><p>تماشا دارد آن آتش که اندر نیستان افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلیم از چشم یار افکند این بخت سیه ما را</p></div>
<div class="m2"><p>الهی کوکب بختم ز بام آسمان افتد</p></div></div>