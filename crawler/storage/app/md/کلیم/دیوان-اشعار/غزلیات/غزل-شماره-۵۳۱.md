---
title: >-
    غزل شمارهٔ  ۵۳۱
---
# غزل شمارهٔ  ۵۳۱

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و لشکر گل در رکاب او</p></div>
<div class="m2"><p>صحرانشین بود سپه بیحساب او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نوبهار طفل دبستان گلشنست</p></div>
<div class="m2"><p>هر غنچه ایکه وا شده باشد کتاب او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل بروی گل غزلی را که سر کند</p></div>
<div class="m2"><p>بیدردم ار بدیهه نگویم جواب او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش آب و رنگ لاله فزون شد مگر نوبهار</p></div>
<div class="m2"><p>آمیخت خون توبه ما با شراب او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس بلاله بیند و دارد تأسفی</p></div>
<div class="m2"><p>چون سرخوشی که سوخته باشد کباب او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر شاخ از شکوفه فکندست نوبهار</p></div>
<div class="m2"><p>پیراهن تری که نیفشرده آب او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر جا که خوشدلی است ز محنت نشانه ایست</p></div>
<div class="m2"><p>بنگر بشاهد گل و نیلی نقاب او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با شرم او کلیم چه سازم که همچو گل</p></div>
<div class="m2"><p>هر چند مست گشت فزون شد حجاب او</p></div></div>