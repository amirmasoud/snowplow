---
title: >-
    غزل شمارهٔ  ۱۱۶
---
# غزل شمارهٔ  ۱۱۶

<div class="b" id="bn1"><div class="m1"><p>منم که تنگدلی باغ دلگشای منست</p></div>
<div class="m2"><p>بدستم آبله جام جهان نمای منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسید همرهی بخت واژگون جائی</p></div>
<div class="m2"><p>که هر که خاک رهم بود خار پای منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدستگیری افلاک احتیاجی نیست</p></div>
<div class="m2"><p>کلیم وقتم و افتادگی عصای منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخاک و خون کشدم هر کجا که سرو قدیست</p></div>
<div class="m2"><p>هر آن نهال که بالا کشد بلای منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنینکه دیدن وضع زمانه جانکاهست</p></div>
<div class="m2"><p>بدیده هرچه غبارست توتیای منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طبیب از عرق شرم نسخه ها را شست</p></div>
<div class="m2"><p>ز بسکه منفعل از درد بیدوای منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بسکه موج غمم در میان گرفته کلیم</p></div>
<div class="m2"><p>ز من کناره کند هر که آشنای منست</p></div></div>