---
title: >-
    غزل شمارهٔ  ۲۱۴
---
# غزل شمارهٔ  ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>بحال بد دل از چشم تر افتاد</p></div>
<div class="m2"><p>سیه گردد چو در آب اخگر افتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گر با این لب شیرین بخندی</p></div>
<div class="m2"><p>بشیر صبح خواهد شکر افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه خواری کز وفاداری ندیدم</p></div>
<div class="m2"><p>کنم صد شکر کز عالم بر افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنر کم ورز گیتی باغبانیست</p></div>
<div class="m2"><p>که خواهان نهال بی بر افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کوکب جز سیه روزی ندیدم</p></div>
<div class="m2"><p>خوشا بختی که او بی اختر افتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گزیدم بند بند نیشکر را</p></div>
<div class="m2"><p>سرانگشت ندامت خوشتر افتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث عقل و عشق از من چه پرسی</p></div>
<div class="m2"><p>چراغی بود با صر صر در افتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه چسبانست با دل صحبت عشق</p></div>
<div class="m2"><p>بدست طفل، مرغ بی پر افتاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم آخر ز بیداد که نالیم</p></div>
<div class="m2"><p>بکشت ما گذار لشکر افتاد</p></div></div>