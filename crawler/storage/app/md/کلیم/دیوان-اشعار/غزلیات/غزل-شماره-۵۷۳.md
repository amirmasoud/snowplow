---
title: >-
    غزل شمارهٔ  ۵۷۳
---
# غزل شمارهٔ  ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>فزون از صبر ایوبست تاب محنت دوری</p></div>
<div class="m2"><p>که رنجوری نباشد آنچنان مشکل که مهجوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان بیروی تو دست و دلم از کار خود مانده</p></div>
<div class="m2"><p>که ساغر در کفم لبریز و من مردم ز مخموری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گوش این نکته پیر مغان بیرون نخواهد شد</p></div>
<div class="m2"><p>که مستی خاکساری آورد، پرهیز مغروری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم اعتبار خلق چون پنهان شوی دانی</p></div>
<div class="m2"><p>که باشد مستی و رسوایی ما عین مستوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو همچون شعله سرکش زهر آلایشی پاکی</p></div>
<div class="m2"><p>ز ما گردی به دامان تو ننشیند مگر دوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نصیب ما نشد یک بار دیدار تو را دیدن</p></div>
<div class="m2"><p>به خوابت هم نمی‌بینم، زهی کوری زهی کوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنان عالم به بند اعتبار ظاهر افتاده</p></div>
<div class="m2"><p>که پروانه نسوزد گر نباشد شمع کافوری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگویی بی‌اثر دیگر کلیم این اشکریزی را</p></div>
<div class="m2"><p>ز بختم گریه آخر هم سیاهی برد و هم شوری</p></div></div>