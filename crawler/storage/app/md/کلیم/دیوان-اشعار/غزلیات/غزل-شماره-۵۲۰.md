---
title: >-
    غزل شمارهٔ  ۵۲۰
---
# غزل شمارهٔ  ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>تا چند همچو سوفار خندان بخون نشستن</p></div>
<div class="m2"><p>دلتنگ و روگشاده خود را بکار بستن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از نسق فتاده است احوال ما چه نقصان</p></div>
<div class="m2"><p>عقد گهر ز قیمت کی افتد از گسستن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیماری غم او آن ناتوانی آرد</p></div>
<div class="m2"><p>کز ضعف کس نیارد پرهیز را شکستن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سیل گریه آخر دل را کدورت افزود</p></div>
<div class="m2"><p>آورد روسیاهی آخر بآب شستن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گوشم این در پند، از پیر گوشه گیر است</p></div>
<div class="m2"><p>دام است صحبت خلق باید زدام جستن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتی کزین تک و دو کی می رسی بآرام</p></div>
<div class="m2"><p>روزی که زی تیغش روزی شود نشستن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ملک خاکساری رسمی است اهل دل را</p></div>
<div class="m2"><p>در صدر هر چه گم شد در آستانه جستن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دنیا خیال و خوابیست، این خواب نزد دانا</p></div>
<div class="m2"><p>آسایشی ندارد بهتر ز چشم بستن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد کلیم اگرچه شیشه دل و تنک ظرف</p></div>
<div class="m2"><p>چون توبه تاب دارد در بستن و شکستن</p></div></div>