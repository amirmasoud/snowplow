---
title: >-
    غزل شمارهٔ  ۴۴۸
---
# غزل شمارهٔ  ۴۴۸

<div class="b" id="bn1"><div class="m1"><p>فرصتی کو که دوای دل رنجور کنیم</p></div>
<div class="m2"><p>پنبه شیشه می مرهم ناسور کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طمع خام نشد ز آتش حرمان پخته</p></div>
<div class="m2"><p>گر بدوزخ برویم آرزوی حور کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدمت بزم شراب تو زما می آید</p></div>
<div class="m2"><p>می توانیم که از گریه گزک شور کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پی کینه ما تیغ به بندد بمیان</p></div>
<div class="m2"><p>ما اگر دست هوس در کمر مور کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زندگی بسکه زبیداد فلک تلخ شده است</p></div>
<div class="m2"><p>خسته به شه را پرسش رنجور کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرده هرچند فزون جلوه افشا خوشتر</p></div>
<div class="m2"><p>فهم این نکته ز راز دل طنبور کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چاره زاریست بر دلبر مغرور کلیم</p></div>
<div class="m2"><p>نتوانیم چو رامش بزر و زور کنیم</p></div></div>