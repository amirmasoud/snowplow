---
title: >-
    غزل شمارهٔ  ۲۶۰
---
# غزل شمارهٔ  ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>عاشق از حیرت درین وادی به جایی می‌رسد</p></div>
<div class="m2"><p>تا نگردد راه گم کی رهنمایی می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون خود بر گل‌رخان شهر قسمت می‌کنم</p></div>
<div class="m2"><p>هرکه می‌آید به دست او حنایی می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رشک بر سنگ فلاخن برده سرگردانیم</p></div>
<div class="m2"><p>کو پس از سرگشتگی آخر به جایی می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه سیلم برنمی‌دارد ز راه انتظار</p></div>
<div class="m2"><p>می‌روم از جا اگر آواز پایی می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با رخت افسانه گلشن ز بس کوتاه شد</p></div>
<div class="m2"><p>نه ز گل بویی نه از بلبل نوایی می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وعده وصلت به دل گر می‌دهم بر من مخند</p></div>
<div class="m2"><p>هرکه بیند خسته را گوید شفایی می‌رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر کوی تغافل نیستم بی‌کس کلیم</p></div>
<div class="m2"><p>گر به فریادم نگاه آشنایی می‌رسد</p></div></div>