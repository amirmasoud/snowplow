---
title: >-
    غزل شمارهٔ  ۵۱۳
---
# غزل شمارهٔ  ۵۱۳

<div class="b" id="bn1"><div class="m1"><p>بسینه ناوک غم تا بکی روان کردن</p></div>
<div class="m2"><p>چه ذوق رو دهد از آینه نشان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا بگلشن حسن معاش می باید</p></div>
<div class="m2"><p>بقدر پایه پرواز آشیان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قفس فراخ اگر گشت گلستان نشود</p></div>
<div class="m2"><p>بجاست شکر و شکایت ز آسمان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غذای ماست فریب سراب نومیدی</p></div>
<div class="m2"><p>مگو بهیچ قناعت نمی توان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ترا چنینکه سر و برگ بدگمانی هست</p></div>
<div class="m2"><p>چرا نداری پروای امتحان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسلم است بدل درد عمر کاه ترا</p></div>
<div class="m2"><p>زجان نهفتن و پنهان زلب فغان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنینکه قبله خود کرده ایم دنیا را</p></div>
<div class="m2"><p>نشان کفر بود پشت بر جهان کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمانه را بتو یکرنگ می کند اول</p></div>
<div class="m2"><p>بنزد جهل فروشان هنر نهان کردن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جفای خار نه از بهر گل کشید کلیم</p></div>
<div class="m2"><p>رساند مشق تنزل ز باغبان کردن</p></div></div>