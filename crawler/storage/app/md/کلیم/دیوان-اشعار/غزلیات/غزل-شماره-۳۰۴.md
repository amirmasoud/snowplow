---
title: >-
    غزل شمارهٔ  ۳۰۴
---
# غزل شمارهٔ  ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>گر بتحریر ستم نامه هجران آید</p></div>
<div class="m2"><p>خامه ام پیشتر از نامه بپایان آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه در راه طلب سستی ازو می بیند</p></div>
<div class="m2"><p>جرس از همرهی ناله با فغان آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بد و نیک جهان خرم و غمگین نشوم</p></div>
<div class="m2"><p>خار تا زانو و گل تا بگریبان آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پنجه اش باز فراهم نشود چون شانه</p></div>
<div class="m2"><p>گر بدست کسی آنزلف پریشان آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقدمگاه من آید بزیارت اول</p></div>
<div class="m2"><p>گر نسیمی ز سر خار مغیلان آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی باده عجب گر بسلامت ماند</p></div>
<div class="m2"><p>ساقی از تاب می آندم که بطوفان آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زینت میکده افزود درش تا بستند</p></div>
<div class="m2"><p>گل بماند چو کسی کم بگلستان آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کنارم بسفر رفته جگر گوشه اشک</p></div>
<div class="m2"><p>چاک باید که بپرسیدن دامان آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپر عجز بود سد ره حادثه اش</p></div>
<div class="m2"><p>بر سر مور اگر خیل سلیمان آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر فلک آب دهد صرفه کند در آتش</p></div>
<div class="m2"><p>باده آخر شود آنروز که باران آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پای در یوزه کلیم از در افلاک بکش</p></div>
<div class="m2"><p>سرخوش از یک قدح باده بسامان آید</p></div></div>