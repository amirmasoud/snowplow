---
title: >-
    غزل شمارهٔ  ۹۶
---
# غزل شمارهٔ  ۹۶

<div class="b" id="bn1"><div class="m1"><p>چمن ز سردی ایام برگ و بار گذاشت</p></div>
<div class="m2"><p>خوش آنکه عاریتی را باختیار گذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسست سردی فصل خزان کنون باید</p></div>
<div class="m2"><p>هوای زهد خنک را بیک کنار گذاشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خزان رسید و بآزادگی ثمر شد نخل</p></div>
<div class="m2"><p>فشاند برگ بشکر همینکه بار گذاشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو نیز پنجه ز می رنگ کن که باد خزان</p></div>
<div class="m2"><p>حنا بدست عروسان شاخسار گذاشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سایه در قدم شاهدان بستان باش</p></div>
<div class="m2"><p>که برگ ریز بپای همه نگار گذاشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم بحلقه زلف نگار خود را بست</p></div>
<div class="m2"><p>باین وسیله سری در کنار یار گذاشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز انقلاب سپهر دو رو عجب دارم</p></div>
<div class="m2"><p>که بیقراری ما را بیک قرار گذاشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنان ممیر که چیزی بماند از تو بجا</p></div>
<div class="m2"><p>بغیر نام نباید بیادگار گذاشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه می توان ز پریشان تیره روز گرفت</p></div>
<div class="m2"><p>کلیم دعوی دل را بزلف یار گذاشت</p></div></div>