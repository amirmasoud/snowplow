---
title: >-
    غزل شمارهٔ  ۵۵۶
---
# غزل شمارهٔ  ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>ز بزمی برنمی خیزد سرود نغمه پردازی</p></div>
<div class="m2"><p>همین از خانه تنگ جرس می آید آوازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم پر مایه است از درد چاکی خواهد از تیغت</p></div>
<div class="m2"><p>که باید خانه ارباب دولت را در بازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگیتی گر چه مشهورم ولی از کام دل دورم</p></div>
<div class="m2"><p>چه سود از امتیاز من دریغا بخت ممتازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدای آشنا زین شش جهت نشنیده ام هرگز</p></div>
<div class="m2"><p>مگر گاهی که از کوه غمم می آید آوازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زرشک چشم خود خون می خورم در جستجوی او</p></div>
<div class="m2"><p>که هر مژگانش هم پائی بود هم بال پروازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بزنجیرم نشاید داشت در بزم ورع کیشان</p></div>
<div class="m2"><p>بکوی مطربان در بندم از ابریشم سازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منم آن بلبل کز شوق گل بیخود روم آنجا</p></div>
<div class="m2"><p>نشان یابم گل خونین اگر در چنگل بازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلیم از دست دادم اختیار خانه دل را</p></div>
<div class="m2"><p>چنان کانجا ندارم جای پنهان کردن رازی</p></div></div>