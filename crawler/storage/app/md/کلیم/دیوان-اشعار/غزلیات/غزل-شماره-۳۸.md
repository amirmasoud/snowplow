---
title: >-
    غزل شمارهٔ  ۳۸
---
# غزل شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>قرار می برد از خلق آه و زاری ما</p></div>
<div class="m2"><p>باین قرار اگر مانده بیقراری ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شویم گرد و بدنبال محملش افتیم</p></div>
<div class="m2"><p>دگر برای چه روزست خاکساری ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمار صحبت تو عقل و هوش از من برد</p></div>
<div class="m2"><p>چه مستئی ز قفا داشت هوشیاری ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چون روی، بره انتظار دیده خلق</p></div>
<div class="m2"><p>بهم نیاید چون زخمهای کاری ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بروی دشت اگر گردبادت آید پیش</p></div>
<div class="m2"><p>ازو بپرس ز احوال بیقراری ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کدام بار غم از خاطری زیاد آید</p></div>
<div class="m2"><p>که دهر ننهد بر دوش بردباری ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمانده جان و دلی تا بیادگار دهیم</p></div>
<div class="m2"><p>کلیم را ببر از ما به یادگاری ما</p></div></div>