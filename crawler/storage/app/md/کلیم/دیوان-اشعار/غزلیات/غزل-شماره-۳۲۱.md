---
title: >-
    غزل شمارهٔ  ۳۲۱
---
# غزل شمارهٔ  ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>مرا مسوز که نازت ز کبریا افتد</p></div>
<div class="m2"><p>چو خس تمام شود شعله هم ز پا افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم زمانه ز ما بیدلان ندارد رنگ</p></div>
<div class="m2"><p>بسان دزد که در خانه گدا افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لباس فقر بزاری نصیب هر کس نیست</p></div>
<div class="m2"><p>خوشا تنی که بر آن نقش بوریا افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز همرهی اشک وانمی ماند</p></div>
<div class="m2"><p>نه آتشی است که از کاروان جدا افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تلافی ار نکند روزگار عقده گشاست</p></div>
<div class="m2"><p>گره ز هر چه گشاید بکار ما افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بغیر دیده که از گریه آب و تابش رفت</p></div>
<div class="m2"><p>که دیده ز آب روان خانه از صفا افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قرعه در بدنم استخوان شکسته شود</p></div>
<div class="m2"><p>ز ضعف گر بسرم سایه هما افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشنده تر ز مرض منت طبیبان است</p></div>
<div class="m2"><p>خوشست درد بشرطی که بیدوا افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حریص چشم طمع دارد از کریم و لئیم</p></div>
<div class="m2"><p>مگس بخوان شه و کاسه گدا افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر حمایت فقرش کند سپرداری</p></div>
<div class="m2"><p>نمی گذارد کاتش ببوریا افتد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیاه روزی ما رنگ بست خواهد شد</p></div>
<div class="m2"><p>کلیم اگر بمن آن چشم سرمه سا افتد</p></div></div>