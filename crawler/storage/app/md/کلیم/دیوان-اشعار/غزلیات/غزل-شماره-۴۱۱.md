---
title: >-
    غزل شمارهٔ  ۴۱۱
---
# غزل شمارهٔ  ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>بخانه چند نشینی سری ببستان کش</p></div>
<div class="m2"><p>چو چشم خویش دمی باده در گلستان کش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کنجکاوی دلها غبار می گیرد</p></div>
<div class="m2"><p>زلطف گاهی دستی بتیغ مژگان کش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا بگوشه مکتوب غیر یاد مکن</p></div>
<div class="m2"><p>جدا بنام من ایدوست خط نسیان کش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه ایست که مستی زبلبلان عیب است</p></div>
<div class="m2"><p>بسان غنچه در این باغ باده پنهان کش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر قبول نداری که کشته لب تست</p></div>
<div class="m2"><p>بیا بگلشن و از زخم غنچه پیکان کش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنانکه آب زگل می شود کدورت ناک</p></div>
<div class="m2"><p>اگر تو صافدلی بار زیر دستان کش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بیقراری منعم نمی توان کردن</p></div>
<div class="m2"><p>کسی بشعله نگوید که پا بدامن کش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بطاق گنبد فانوس این رقم دیدم</p></div>
<div class="m2"><p>که سر بباد رود زود در گریبان کش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسان شیشه خالی دماغ ما خشک است</p></div>
<div class="m2"><p>کلیم رخت ببازار میفروشان کش</p></div></div>