---
title: >-
    غزل شمارهٔ  ۵۲۳
---
# غزل شمارهٔ  ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>شکارگاه معانی است کنج خلوت من</p></div>
<div class="m2"><p>زه کمان شکارم کمند وحدت من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدنگ خامه چو پر از بنان من یابد</p></div>
<div class="m2"><p>خطا نمی شود از صید تیر فکرت من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدور گردی جائی روم بدشت خیال</p></div>
<div class="m2"><p>که گم شود ره طی کرده گاه رجعت من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگونه معنی غیری برم که معنی خویش</p></div>
<div class="m2"><p>دوبار بستن دزدیست در شریعت من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوق شاهد معنی همیشه همچو دوات</p></div>
<div class="m2"><p>براه عالم بالاست چشم حیرت من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هلاک گوهر قدر خودم که شیشه بسنگ</p></div>
<div class="m2"><p>اگر خورد شکند در میانه قیمت من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر بچاه در افتم رسم باوج کمال</p></div>
<div class="m2"><p>ز سازگاری افتادگی بطینت من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مسافتی است که صد عقده سد ره دارد</p></div>
<div class="m2"><p>میان سبحه تزویر و دست همت من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کفن بخلوت گور است برگ و سامانی</p></div>
<div class="m2"><p>باین غبار نیالوده کنج عزلت من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از اینکه دست امیدم کلیم کوتاه است</p></div>
<div class="m2"><p>خدا معانی برجسته داد قسمت من</p></div></div>