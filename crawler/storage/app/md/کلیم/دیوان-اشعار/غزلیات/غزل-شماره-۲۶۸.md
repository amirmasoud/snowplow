---
title: >-
    غزل شمارهٔ  ۲۶۸
---
# غزل شمارهٔ  ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>ناخلف را بکسی فخر ز آبا نرسد</p></div>
<div class="m2"><p>نسب گوهر بی آب بدریا نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشته طول امل عارف روشندل را</p></div>
<div class="m2"><p>راست چون رشته شمعست بفردا نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخت چون سدره کام شود عاشق را</p></div>
<div class="m2"><p>اثر گرمی خورشید بحر با نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو جهان حسرت بالات الف کش دارد</p></div>
<div class="m2"><p>سرو را با تو بیک فاخته دعوا نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنقدر کارشکن صافدلان را شده جمع</p></div>
<div class="m2"><p>که دگر دشمنی شیشه بخارا نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حالت شمع دلیلست که در کشور عشق</p></div>
<div class="m2"><p>سر بسامان بجز از آتش سودا نرسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظاهر و باطنم از بسکه بود دور از هم</p></div>
<div class="m2"><p>رنگ خونم ز ته پوست بسیما نرسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما درین غمکده هم طالع زخم آمده ایم</p></div>
<div class="m2"><p>خنده بی گریه خونین بلب ما نرسد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همتم هست رسا بختم اگر کوتاهست</p></div>
<div class="m2"><p>پشت پایم رسد ار دست بدنیا نرسد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کس چه داند که کلیم ابر کدامین وادیست</p></div>
<div class="m2"><p>قاصد سیل گر از جانب صحرا نرسد</p></div></div>