---
title: >-
    غزل شمارهٔ  ۳۲۸
---
# غزل شمارهٔ  ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>همه محروم و ازو دست کسی دور نبود</p></div>
<div class="m2"><p>کس ندیدم که درین میکده مخمور نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فقر و روشندلی آئینه رخسار همند</p></div>
<div class="m2"><p>هیچ ویرانه ندیدم که پر نور نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم از کاوش مژگان تو از سینه گریخت</p></div>
<div class="m2"><p>جای آسایش در خانه زنبور نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من درین میکده پیش قدحی بنشستم</p></div>
<div class="m2"><p>که سرش بسته تر از کاسه طنبور نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط اگر سرکشد از خطه حسن تو مرنج</p></div>
<div class="m2"><p>که بفرمان سلیمان هم این مور نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شعله داغ برونم بدرون نور نداد</p></div>
<div class="m2"><p>شمع تربت سبب روشنی کور نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تبسم بدلم مشت نمک می پاشد</p></div>
<div class="m2"><p>چشم داغم بره مرهم کافور نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حال سوز دل ما یار ندانست کلیم</p></div>
<div class="m2"><p>از سیه بختی در آتش ما نبور نبود</p></div></div>