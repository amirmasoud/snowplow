---
title: >-
    غزل شمارهٔ  ۳۷۴
---
# غزل شمارهٔ  ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>از لذت جور تو خبردار نباشد</p></div>
<div class="m2"><p>زخمی که لبش بر لب سوفار نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمان توام تشنه بخونند مبادا</p></div>
<div class="m2"><p>این شربت کم بخش دو بیمار نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیروی تو چشم از همه بستم که ندیدم</p></div>
<div class="m2"><p>عکسی که برین آینه زنگار نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واپس ترم از سایه در آن کوی که هرگز</p></div>
<div class="m2"><p>از ناکسیم جا پس دیوار نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز مهر توام نیست متاعی وزغیرت</p></div>
<div class="m2"><p>جائی بفروشم که خریدار نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجنون نتوان بود بژولیدگی موی</p></div>
<div class="m2"><p>مستی به پریشانی دستار نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک ناله بانگیز نخیزد ز رگ دل</p></div>
<div class="m2"><p>ابروی تو گر ناخن این تار نباشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنهار کلیم از مدد بخت بپرهیز</p></div>
<div class="m2"><p>این بخت همان به که بکس یار نباشد</p></div></div>