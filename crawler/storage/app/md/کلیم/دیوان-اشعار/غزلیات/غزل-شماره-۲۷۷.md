---
title: >-
    غزل شمارهٔ  ۲۷۷
---
# غزل شمارهٔ  ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>بدلم اینهمه پیکان ستم بار نبود</p></div>
<div class="m2"><p>گره غنچه گران بر دل گلزار نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و جان صبر و شکیب از شب هجرت چه کشد</p></div>
<div class="m2"><p>داغ آسایش بختیم که بیدار نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرح هجران تو میکرد بنامت چو رسید</p></div>
<div class="m2"><p>خامه را با دو زبان قوت گفتار نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ازل دشمن سامان شده ویرانه ما</p></div>
<div class="m2"><p>در اگر بود درین غمکده دیوار نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق جائی که صف آراست بخونریزی من</p></div>
<div class="m2"><p>خنده از بیم بلا بر لب سوفار نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کس ندانست که چشم توجه بیماری داشت</p></div>
<div class="m2"><p>که دوایش بجز از مستی سرشار نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سرم بخت ز گلزار جهان چونگل شمع</p></div>
<div class="m2"><p>نزد آن گل که وبال سرو دستار نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ثمر نخل وجودم همه اشکست کلیم</p></div>
<div class="m2"><p>چکنم شعله بغیر از شررش بار نبود</p></div></div>