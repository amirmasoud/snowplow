---
title: >-
    غزل شمارهٔ  ۲۲۱
---
# غزل شمارهٔ  ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>بملک عشق دل شادمان نمی ماند</p></div>
<div class="m2"><p>گل شکفته درین گلستان نمی ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی خورد غم روزی کسیکه قانع شد</p></div>
<div class="m2"><p>همای هرگز بی استخوان نمی ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرا چو موج همیشه است بیقراری ما</p></div>
<div class="m2"><p>بیک قرار چو وضع جهان نمی ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیاه روزی ما همچنین نخواهد ماند</p></div>
<div class="m2"><p>شب ار دراز بود جاودان نمی ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا مکش همه شب آه جانگداز چو شمع</p></div>
<div class="m2"><p>که وقت صبح بکامت زبان نمی ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین رمی که تر از من است پیکان هم</p></div>
<div class="m2"><p>زتیر جور تو در استخوان نمی ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شمار زخم ستمهای دوست نتوان کرد</p></div>
<div class="m2"><p>که از خدنگ جفاها نشان نمی ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>براه پرخطری می روم که نقش قدم</p></div>
<div class="m2"><p>زبیم در عقب کاروان نمی ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم ناوک آهت گشاد خواهد یافت</p></div>
<div class="m2"><p>همیشه تیر کسی در کمان نمی ماند</p></div></div>