---
title: >-
    غزل شمارهٔ  ۱۱۹
---
# غزل شمارهٔ  ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>حالت از تنگی جا غنچه بکنج دهنست</p></div>
<div class="m2"><p>چکند، ساخته با گوشه خود بیوطنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پستی پایه چو غواص شگونست مرا</p></div>
<div class="m2"><p>پر ز یوسف بود آن چاه که در راه منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چند در خانه اش آتش فتد از پرتو تو</p></div>
<div class="m2"><p>زین ستم آینه در فکر جلای وطنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از جنونم بسوی عقل دلالت مکنید</p></div>
<div class="m2"><p>گمشدن بهتر از آن ره که درو راهزنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روز محشر که نشانها زشهیدان طلبند</p></div>
<div class="m2"><p>کشته تیغ تو آنست که خونین کفنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکر معنی را، مشاطه سخن فهمانند</p></div>
<div class="m2"><p>ناخن دخل بجا شانه زلف سخنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسن و عشق از همشان نیست جدائی هرگز</p></div>
<div class="m2"><p>آنقدر هست که آن یوسف و این پیرهنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز نمک باری در قافله اشکم نیست</p></div>
<div class="m2"><p>دیده ام تاجر کان نمک آن دهنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرمی آخر شده در فکر باش کلیم</p></div>
<div class="m2"><p>سخن تازه مگر کم ز شراب کهنست</p></div></div>