---
title: >-
    غزل شمارهٔ  ۹۳
---
# غزل شمارهٔ  ۹۳

<div class="b" id="bn1"><div class="m1"><p>آزادگی ز منت احسان رمیدنست</p></div>
<div class="m2"><p>قطع امید دست طلب را بریدنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحریست زندگی که نهنگش حوادثست</p></div>
<div class="m2"><p>تن کشتی است و مرگ بساحل رسیدنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیر ریاض عالم جان با حجاب تن</p></div>
<div class="m2"><p>گلزار را ز رخنه دیوار دیدنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دور ما زخست ابنای روزگار</p></div>
<div class="m2"><p>دشوارتر ز مرگ، گریبان دریدنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوی دوست خاک نشینی ز حد گذشت</p></div>
<div class="m2"><p>ای تیغ جور، نوبت در خون طپیندست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تدبیر تنگدستی جستم ز عقل، گفت</p></div>
<div class="m2"><p>دستی که کوتهست علاجش بریدنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افتاد پیش در سخن آنکس که ایستاد</p></div>
<div class="m2"><p>عیب کمیت خامه درین ره دویدنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بند خانه با همه آزادگی، کلیم</p></div>
<div class="m2"><p>از اشتیاق پای بدامان کشیدنست</p></div></div>