---
title: >-
    غزل شمارهٔ  ۲۷۰
---
# غزل شمارهٔ  ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>اشکی که رخت خانه به طوفان نمی دهد</p></div>
<div class="m2"><p>راهش بخویش دیده گریان نمی دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر بر تن صدف نبود زانکه روزگار</p></div>
<div class="m2"><p>یکجا به هیچکس سر و سامان نمی دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کار خویشتن دل دیوانه عاقلست</p></div>
<div class="m2"><p>ویرانه را به ملک سلیمان نمی دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جامست بی تعلق دوران که غیر او</p></div>
<div class="m2"><p>خندان و روشکفته بکس جان نمی دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصلش گران خری، ندهی جان اگر به نقد</p></div>
<div class="m2"><p>کالای نسیه را کسی ارزان نمی دهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تیغ جور حادثه ای در زمانه هست</p></div>
<div class="m2"><p>میراب دهر آب به بستان نمی دهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشمی که از سواد سخن روشنی گرفت</p></div>
<div class="m2"><p>این سرمه را بملک صفاهان نمی دهد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با رهنما چه کار اگر شوق کاملست</p></div>
<div class="m2"><p>کس سیل را سراغ بیابان نمی دهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دل نگه مدار کلیم اشک شوق را</p></div>
<div class="m2"><p>این طفل را کسی بدبستان نمی دهد</p></div></div>