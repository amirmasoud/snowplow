---
title: >-
    غزل شمارهٔ  ۴۴۷
---
# غزل شمارهٔ  ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>عمریستکه یک مستی سرشار ندیدم</p></div>
<div class="m2"><p>در پای خم افتادن دستار ندیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دولت وصلی که فلک رشک نیارد</p></div>
<div class="m2"><p>جز صحبت آئینه و زنگار ندیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ظلمت بخت سیه خویش نماندم</p></div>
<div class="m2"><p>چون آب خضر روی خریدار ندیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسوس که چون نخل گرانبار درین باغ</p></div>
<div class="m2"><p>دستی ز رفیقان بته بار ندیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون رشته گلدسته بگرد همه خوبان</p></div>
<div class="m2"><p>گردیدم و یک یار وفادار ندیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادا سر آئینه زانو بسلامت</p></div>
<div class="m2"><p>روئی مگر از آینه رخسار ندیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچون هدفم بخت نوازش زکسی نیست</p></div>
<div class="m2"><p>هر جا که شدم غیر دل آزار ندیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا از مدد ناخن تدبیر گذشتم</p></div>
<div class="m2"><p>در راه طلب عقده دشوار ندیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با آنکه کسی چیزی دربار ندارد</p></div>
<div class="m2"><p>در قافله یک مرد سبکبار ندیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کوی توکل که بحق پشت امید است</p></div>
<div class="m2"><p>کاهی که دهد تکیه بدیوار ندیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با اهل طرب نیز کلیم ارچه نشستم</p></div>
<div class="m2"><p>از خنده بجز نام چو سوفار ندیدم</p></div></div>