---
title: >-
    غزل شمارهٔ  ۳۱۲
---
# غزل شمارهٔ  ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>گاه اندیشه ای از روز جزا باید کرد</p></div>
<div class="m2"><p>گذری بر سر خاک شهدا باید کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو که ضبط نگه خود نتوانی کردن</p></div>
<div class="m2"><p>منع رسوائی احباب چرا باید کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه سرکشی افتادگی از دست مده</p></div>
<div class="m2"><p>گر همه شعله شوی کار گیا باید کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طلب شاهد مقصود ز هر سو، شرطست</p></div>
<div class="m2"><p>هر قدم در ره او رو بقفا باید کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب شود روز حیات و نرود حسرت وصل</p></div>
<div class="m2"><p>ما چنان روزه نگیریم که وا باید کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدلم حسرت در خاک طپیدن مگذار</p></div>
<div class="m2"><p>بسملم کرده ای از دست رها باید کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرفه حالیستکه در خون دل خویش کلیم</p></div>
<div class="m2"><p>دست و پائی نه و چون موج شنا باید کرد</p></div></div>