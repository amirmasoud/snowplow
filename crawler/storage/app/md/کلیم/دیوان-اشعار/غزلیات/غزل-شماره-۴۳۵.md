---
title: >-
    غزل شمارهٔ  ۴۳۵
---
# غزل شمارهٔ  ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>با که گویم آنچه زان نخل تمنا دیده‌ام</p></div>
<div class="m2"><p>زان قد آشوب قیامت را دو بالا دیده‌ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی من شد که در هر حال باید شاد زیست</p></div>
<div class="m2"><p>قهقهه کبک دری را در قفس تا دیده‌ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فاخته آن روز تا شب گشته بر گرد سرم</p></div>
<div class="m2"><p>گر شبی در خواب سو قامتش را دیده‌ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در رهایی تلاشم گرچه سیلابم برد</p></div>
<div class="m2"><p>تا صلاح کار خود را در مدارا دیده‌ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرم چشم عیب‌بین خویشتن دانسته‌ام</p></div>
<div class="m2"><p>هر قدر ناخوش که از ابنای دنیا دیده‌ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نخوتی دارد قناعت، حیف کان نقص منست</p></div>
<div class="m2"><p>خویش را تا قانعم همسر به دریا دیده‌ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار خود هرجا که محکم کرده دهر بی‌تمیز</p></div>
<div class="m2"><p>مرغ را، زنجیر جای رشته بر پا دیده‌ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در قفس یک سال می‌باید به سر بردن کلیم</p></div>
<div class="m2"><p>دل‌گشایی گر همه یک دم ز صحرا دیده‌ام</p></div></div>