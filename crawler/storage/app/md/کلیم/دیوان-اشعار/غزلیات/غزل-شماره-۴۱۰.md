---
title: >-
    غزل شمارهٔ  ۴۱۰
---
# غزل شمارهٔ  ۴۱۰

<div class="b" id="bn1"><div class="m1"><p>در مصاف عافیت لرزان تر از سیماب باش</p></div>
<div class="m2"><p>تیغ موج خون چو بینی پنجه قصاب باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت بیداری نمی یابد تجرد پیشه را</p></div>
<div class="m2"><p>خانه چون خالی بود گو پاسبان در خواب باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا باریک شد راهت، قدم از سر بنه</p></div>
<div class="m2"><p>جاده گر از تار در پیش آیدت مضراب باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار یکرو کن، مدارا نیست جز مشق نفاق</p></div>
<div class="m2"><p>گرنه سیلاب سرائی آتش اسباب باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سجده گر پیشت برند ابروی تمکین خم مکن</p></div>
<div class="m2"><p>از قبول خلق از جا در میا محراب باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شهادت رتبه بالاترت گر آرزوست</p></div>
<div class="m2"><p>در تلاش تشنه مردن در کنار آب باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ اگر بر خوری رنگ رضامندی مباز</p></div>
<div class="m2"><p>با بلاها تازه رو چون عکس در خوناب باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخت جانی مایه صد دردسر باشد کلیم</p></div>
<div class="m2"><p>در کشاکش ناتوان چون رشته بیتاب باش</p></div></div>