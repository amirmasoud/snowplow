---
title: >-
    غزل شمارهٔ  ۱۶۱
---
# غزل شمارهٔ  ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>چشم دلجوئی دلم از مردم عالم نداشت</p></div>
<div class="m2"><p>داغ من مرهم ندید و راز من محرم نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل این گلستان صد آشیانرا کهنه کرد</p></div>
<div class="m2"><p>آن گل خودرو وفایش عمر یک شبنم نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منکه غمخوار دلم از من مپرس احوال او</p></div>
<div class="m2"><p>عالمی غم داشت دل اما غم عالم نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر ما تیغ بیداد تو ابر رحمتست</p></div>
<div class="m2"><p>رحمتی زین به که زخمش حاجت مرهم نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خموشی گوهر مقصود می آید بچنگ</p></div>
<div class="m2"><p>هیچ غواصی نکرد آنکس که پاس دم نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در وداعش دیده طوفان خیز می بایست حیف</p></div>
<div class="m2"><p>کز تف دل دیده ام چون چشم عینک نم نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر لب لعلت خراشی دیدم و مردم زرشک</p></div>
<div class="m2"><p>این نگین کی کنده شد نقشی خود این خاتم نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسکه در خاطر خیال خال آن لب جا گرفت</p></div>
<div class="m2"><p>کعبتین آرزویم غیر نقش کم نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت از دیده دست تربیت شستم کلیم</p></div>
<div class="m2"><p>زان که آن گوهر که من زین بحر می‌جستم نداشت</p></div></div>