---
title: >-
    غزل شمارهٔ  ۱۱۴
---
# غزل شمارهٔ  ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>جز قامتت بچشم و دلم جای گیر نیست</p></div>
<div class="m2"><p>مهمان خانه های کمان غیر تیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیا و آخرت بره او دو نقش پاست</p></div>
<div class="m2"><p>دلبستگی بنقش قدم دلپذیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جائیکه من فتاده ام، آنجا که می رسد</p></div>
<div class="m2"><p>از بیکسی مدان اگرم دستگیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گشته ام ز آمد و رفت نفس ملول</p></div>
<div class="m2"><p>وادید و دید هیچکسم در ضمیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر دل نهم چو دست کفم پر گهر شود</p></div>
<div class="m2"><p>گر دست مفلس است ولی دل فقیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرز فلک بهیچ دلی جا نمی کند</p></div>
<div class="m2"><p>پیری به بی مریدی این چرخ پیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عیب از نهاد سخت دلان در نمی رود</p></div>
<div class="m2"><p>ای خواجه موی کاسه چو موی خمیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>محروم باد چشم کلیم از رخت اگر</p></div>
<div class="m2"><p>گلدسته بی تو در نظرش دسته تیر نیست</p></div></div>