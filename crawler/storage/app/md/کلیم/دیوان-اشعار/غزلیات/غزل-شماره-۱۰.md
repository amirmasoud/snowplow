---
title: >-
    غزل شمارهٔ  ۱۰
---
# غزل شمارهٔ  ۱۰

<div class="b" id="bn1"><div class="m1"><p>تا یافتم رسایی دست کشیده را</p></div>
<div class="m2"><p>آورده ام بچنگ مراد رمیده را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عریان تنی خوشست ولی ذوق دیگرست</p></div>
<div class="m2"><p>جیب دریده دامن در خون کشیده را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاری اگر ز صورت بیمعنی آمدی</p></div>
<div class="m2"><p>می بود دلبری خم زلف بریده را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاری اگر بپای طلب ناخلیده ماند</p></div>
<div class="m2"><p>از سر مگیر راه بپایان رسیده را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منکر شدن ز صحبت پنهان چه فایده</p></div>
<div class="m2"><p>نتوان نهفت خون، لب لعل گزیده را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنجا که شمع روی تو افروخت باغبان</p></div>
<div class="m2"><p>دامن زند چراغ گل نو دمیده را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جائیکه کار دانه کند قطره شراب</p></div>
<div class="m2"><p>آرد بدام طبع ز عالم رمیده را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گردن هزار تمنا فکنده ای</p></div>
<div class="m2"><p>ای شیخ شهر دست ز دنیا کشیده را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اشک سبک عنان برفیقان نایستاد</p></div>
<div class="m2"><p>در ره بجا گذاشته رنگ پریده را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>این بخت بی تصرف ما رام خود نکرد</p></div>
<div class="m2"><p>یکره کلیم دلبر عاشق ندیده را</p></div></div>