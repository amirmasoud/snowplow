---
title: >-
    غزل شمارهٔ  ۲۸۴
---
# غزل شمارهٔ  ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>می‌آشام غمت پیمانه و ساغر نمی‌دارد</p></div>
<div class="m2"><p>به جز تبخاله بر لب ساغر دیگر نمی‌دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندانم از خدا برگشته مژگانت چه می‌خواهد</p></div>
<div class="m2"><p>که سر از سجده محراب ابرو برنمی‌دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو بی‌پروا درون دل، ولی از حال او غافل</p></div>
<div class="m2"><p>که آتش آگهی از سوزش مجمر نمی‌دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنم از هر نگاهت مستی دیگر چه بزمست این</p></div>
<div class="m2"><p>کسی صد رنگ می ای شوخ در ساغر نمی‌دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نقش پا ندارد بستم بالین به کنج غم</p></div>
<div class="m2"><p>که از سر در گریبانی تن ما سر نمی‌دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>متاع صبر و آرام از دلم جستی، عجب از تو</p></div>
<div class="m2"><p>نمی‌دانی که گلخن غیر خاکستر نمی‌دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرت گردم مگردان اینچنین یکباره محرومم</p></div>
<div class="m2"><p>که دارد سایه‌ای سرو سهی گر بر نمی‌دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من بی‌کس هلاک گرمی داغ جنون گردم</p></div>
<div class="m2"><p>که تا شد پاسبانم چشم، از من بر نمی‌دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم از شعر رنگین نیست بیت ساده می‌گوید</p></div>
<div class="m2"><p>عروس تنگدستان بیش ازین زیور نمی‌دارد</p></div></div>