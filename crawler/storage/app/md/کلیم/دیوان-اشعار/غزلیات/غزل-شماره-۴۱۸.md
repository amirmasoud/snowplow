---
title: >-
    غزل شمارهٔ  ۴۱۸
---
# غزل شمارهٔ  ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>دورم از فتنه که در سایه مژگان توام</p></div>
<div class="m2"><p>خاطرم از همه جمعست پریشان توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله هرچند غبار تنم از جا برداشت</p></div>
<div class="m2"><p>طالع دون نرسانید بدامان توام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زانجمن پیشتر از شمع برون خواهم رفت</p></div>
<div class="m2"><p>اینچنین گر بگدازد تب هجران توام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منت دیده دگر بهر تماشا نکشم</p></div>
<div class="m2"><p>بسته ام چشم ز نظاره و حیران توام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سررشته نسبت دو بود تاب یکیست</p></div>
<div class="m2"><p>موبمو در هم چون طره پیچان توام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>استخوانم همگی شانه شود بعد از مرگ</p></div>
<div class="m2"><p>بسکه در آرزوی زلف پریشان توام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه بمن سر و سری دارد و نه گل نظری</p></div>
<div class="m2"><p>این ثمر داد هواداری بستان توام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرم آنم که نهم داغ بفرق تو کلیم</p></div>
<div class="m2"><p>دگر امروز بفکر سر و سامان توام</p></div></div>