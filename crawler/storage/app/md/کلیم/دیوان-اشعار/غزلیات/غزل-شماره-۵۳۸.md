---
title: >-
    غزل شمارهٔ  ۵۳۸
---
# غزل شمارهٔ  ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>ننشستن نقش امید، از نقش بد بسیار به</p></div>
<div class="m2"><p>آئینه را عریان تنی، از جامه زنگار به</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غواص تا دم می زند گوهر نمی آید بکف</p></div>
<div class="m2"><p>گوهرشناس ار کس بود خاموشی از گفتار به</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر لب که بی آهی بود، کم از لب چاهی بود</p></div>
<div class="m2"><p>چشمی که نبود خونفشان از رخنه دیوار به</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ذره کامل بود، به ز آفتاب ناقص است</p></div>
<div class="m2"><p>گر نیمه باشد خم ز می، زو ساغر سرشار به</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر رابود ربط دگر با می کدو شاید بود</p></div>
<div class="m2"><p>گر گنج قارون باشدت در رهن می دستار به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همت بطاقی نه کز آن، دستت نباشد نارسا</p></div>
<div class="m2"><p>پرواز چون کوته بود، صد بار از آن رفتار به</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلخسته هجر ترا، از وصل می باید دوا</p></div>
<div class="m2"><p>ای چاره ساز از برگ گل، مرهم بزخم خار به</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاری ز مستی در جهان بهتر نمی باشد ولی</p></div>
<div class="m2"><p>آنهم مکرر می شود، بیکاری از هر کار به</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نتوان کلیم از وصل می دلشاد در غربت شدن</p></div>
<div class="m2"><p>گر می کشی داری هوس در خانه خمار به</p></div></div>