---
title: >-
    غزل شمارهٔ  ۴۴۹
---
# غزل شمارهٔ  ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>باغبان بیمهر و ما در اصل نخل بی بریم</p></div>
<div class="m2"><p>عاقبت در گلخن گیتی کف خاکستریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچکس نبود که نبود در پی آزار ما</p></div>
<div class="m2"><p>اهل عالم جمله طفل و ما چو مرغ بی بریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقانت تیغ کین در یکدگر خوش می نهند</p></div>
<div class="m2"><p>خون هم چون آب می ریزیم و از یک لشکریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهرورزی چون رسن تابیست کین بر رشته را</p></div>
<div class="m2"><p>پیشتر چندانکه داریم از همه واپس تریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ یک اصلیم عیب ما بود عیب همه</p></div>
<div class="m2"><p>از چه همچون موج دائم در پی یکدیگریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک ما را ز پی سرگشتگی گل کرده اند</p></div>
<div class="m2"><p>دهر گوئی بزم مستانست و ما چون ساغریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندرین گلخن بچشم کم مبین ما را کلیم</p></div>
<div class="m2"><p>با همه افسردگی دل زنده تر از اخگریم</p></div></div>