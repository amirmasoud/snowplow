---
title: >-
    غزل شمارهٔ  ۳۲۲
---
# غزل شمارهٔ  ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>وقتی زبار هستی چیزی بجا نماند</p></div>
<div class="m2"><p>کز تو بره نشانی از نقش پا نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیا ز سخت گیری هرگز بکس نپاید</p></div>
<div class="m2"><p>هرچند بفشری مشت رنگ حنا نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه بی ثباتی، شادی و غم رفیقند</p></div>
<div class="m2"><p>بر سر گلی نپاید خاری بپا نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبر و خرد بیکدل با شوق او نگنجند</p></div>
<div class="m2"><p>چون سیل میهمان شد کس در سرا نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اکسیر سیرچشمی، خاک سیه کند زر</p></div>
<div class="m2"><p>غیرت چو کامل افتد، کس بینوا نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش قمار طالع، گر اینچنین نشیند</p></div>
<div class="m2"><p>غیر از نشان دندان، در دست و پا نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن غمزه جهانسوز، پروای کس ندارد</p></div>
<div class="m2"><p>آتش چه باک دارد، گر بوریا نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناداری قناعت، همسر بملک داراست</p></div>
<div class="m2"><p>این جوی آب باریک، از سیل وا نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باشد کلیم خاموش، پیوسته با دل پر</p></div>
<div class="m2"><p>جامی که گشت لبریز، با او صدا نماند</p></div></div>