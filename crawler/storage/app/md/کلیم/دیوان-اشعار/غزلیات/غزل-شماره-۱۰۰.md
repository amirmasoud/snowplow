---
title: >-
    غزل شمارهٔ  ۱۰۰
---
# غزل شمارهٔ  ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>نخل امید ز بار افتادست</p></div>
<div class="m2"><p>با غم از چشم بهار افتادست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیحسابست همان درد دلم</p></div>
<div class="m2"><p>نفسم گر بشمار افتادست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گریه زین تخم که بر سینه فشاند</p></div>
<div class="m2"><p>ناله ها آبله دار افتادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برد بر سر کشیم سرکوبی</p></div>
<div class="m2"><p>حیف دستم که زکار افتادست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد را در خور طاقت بدهند</p></div>
<div class="m2"><p>شعله در جان شرر افتادست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل زمانیست حق رهگذراست</p></div>
<div class="m2"><p>هرچه در راهگذار افتادست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دکانم ز کسادی چه که نیست</p></div>
<div class="m2"><p>گرد بر روی غبار افتادست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اضطراب نگهت از دل ماست</p></div>
<div class="m2"><p>باز چشمت بشکار افتادست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن تو با همه بی پروائی</p></div>
<div class="m2"><p>در پی خون بهار افتادست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه جا آه کلیم از پی دوست</p></div>
<div class="m2"><p>گرد دنبال سوار افتادست</p></div></div>