---
title: >-
    غزل شمارهٔ  ۲۰۵
---
# غزل شمارهٔ  ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>فلک اسباب دولت را ز بهر ناکسان دارد</p></div>
<div class="m2"><p>هما گر سایه‌ای دارد برای استخوان دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز محرومی‌ست گر دل زاری‌ای دارد درین وادی</p></div>
<div class="m2"><p>به قدر دوری منزل جرس دایم فغان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رشک طالع تردامنان داغم درین گلشن</p></div>
<div class="m2"><p>که شبنم خانه از گل بلبل از خس آشیان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خموشی پیشه کن کز نطق آفت‌هاست سالک را</p></div>
<div class="m2"><p>جرس دایم زبان با رهزنان کاروان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عاشق ناز معشوقان به یک نسبت نمی‌ماند</p></div>
<div class="m2"><p>که تیر رفته آخر بازگشتی با کمان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر راحت هوس داری به کوی ناامیدی رو</p></div>
<div class="m2"><p>که دایم باغبان آسودگی فصل خزان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هواداران گروه دیگرند و عاشقان دیگر</p></div>
<div class="m2"><p>نگیرد جای بلبل گل اگر صد باغبان دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان زاهدان خشک کمتر اهل دل بینی</p></div>
<div class="m2"><p>نه هرجا استخوانی هست مغزی در میان دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صراحی چون دلی خالی کند دیگر نمی‌گرید</p></div>
<div class="m2"><p>کلیم است اینکه دایم دیده‌های خون‌فشان دارد</p></div></div>