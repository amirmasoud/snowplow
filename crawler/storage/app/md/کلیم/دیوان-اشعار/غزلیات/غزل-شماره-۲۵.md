---
title: >-
    غزل شمارهٔ  ۲۵
---
# غزل شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>به سان شانه‌ات سرپنجه گردانم گریبان را</p></div>
<div class="m2"><p>به چنگ آرم مگر زین دست آن زلف پریشان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشد نیک باطن در پی آرایش ظاهر</p></div>
<div class="m2"><p>به نقاش احتیاجی نیست دیوار گلستان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگو از گریه بی‌حاصلم کاری نمی‌آید</p></div>
<div class="m2"><p>به دامن رهنمایی می‌کند چاک گریبان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک آستانت جبهه ما دارد آن نسبت</p></div>
<div class="m2"><p>که ندهد رخصت آنجا نشستن نقش دربان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چشم ترم یک روز میراب چمن باشد</p></div>
<div class="m2"><p>به فرق باغبان ویران کنم دیوار بستان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه از خواریست گر قدر سخن را کس نمی‌داند</p></div>
<div class="m2"><p>به بازار جهان قیمت که داند آب حیوان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلیم از عشوه‌های او چه خوش کردی، نمی‌دانم</p></div>
<div class="m2"><p>تغافل‌های رسوا یا نوازش‌های پنهان را</p></div></div>