---
title: >-
    غزل شمارهٔ  ۳۱۷
---
# غزل شمارهٔ  ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>خیال گلشن رویت بدل گذار نکرد</p></div>
<div class="m2"><p>که مو بموی تنم را چو لاله زار نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه شانه زسر تا بپای شد انگشت</p></div>
<div class="m2"><p>حساب حلقه آنزلف تابدار نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیاده وادی دیوانگی بسر نرساند</p></div>
<div class="m2"><p>کسیکه شور جنونش به نی سوار نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کوه در ته تیغست سربلندی او</p></div>
<div class="m2"><p>کسیکه شیوه افتادگی شعار نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبسکه گرد کدورت نشست بر سر هم</p></div>
<div class="m2"><p>بدل جفای خدنگ زمانه کار نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمال اجر شهادت بآن شهید دهند</p></div>
<div class="m2"><p>که غیر شمع کسش گریه بر مزار نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبردم از سخن خویش بهره ای که صدف</p></div>
<div class="m2"><p>بگوش از گهر خویش گوشوار نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلیم باده خون سبیل یک اقلیم</p></div>
<div class="m2"><p>وفا بمستی آن چشم پرخمار نکرد</p></div></div>