---
title: >-
    غزل شمارهٔ  ۲۳۴
---
# غزل شمارهٔ  ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>دل که چندین آه از جان می کشد</p></div>
<div class="m2"><p>نقش آن زلف پریشان می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام پست و بلند روزگار</p></div>
<div class="m2"><p>دل بآن چاه زنخدان می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیشه ناموس را خوش جذبه ایست</p></div>
<div class="m2"><p>سنگ را از دست طفلان می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تواند بر سر من خاک بیخت</p></div>
<div class="m2"><p>بخت دست از آب حیوان می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مور خط لعل لبت را خوش گرفت</p></div>
<div class="m2"><p>خاتم از دست سلیمان می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیغ بیداد تو هر جا شد علم</p></div>
<div class="m2"><p>شعله هم سر در گریبان می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک رسوا کرد ما را ورنه دل</p></div>
<div class="m2"><p>ناله را از سینه پنهان می کشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاش بگذارد گریبان مرا</p></div>
<div class="m2"><p>یار از دستم چو دامان می کشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مزرع امید دل آبی نخورد</p></div>
<div class="m2"><p>انتظار تیرباران می کشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در کشاکش تا بکی باشم کلیم</p></div>
<div class="m2"><p>دل بدرد و جان بدرمان می کشد</p></div></div>