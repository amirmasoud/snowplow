---
title: >-
    غزل شمارهٔ  ۳۲۶
---
# غزل شمارهٔ  ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>ریاض ملک را دیگر بهار دلگشا آمد</p></div>
<div class="m2"><p>بفرق دوست از نو سایه بال هما آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بروی ترکش اقبال تیر رفته برگشته</p></div>
<div class="m2"><p>دعای مستجاب از آسمان حاجت روا آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز گرد موکب اقبال چشم بخت روشن شد</p></div>
<div class="m2"><p>بباغ خاطر افسردگان آب بقا آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهای سرمه با خاک سیه خواهد برابر شد</p></div>
<div class="m2"><p>چنین کز گرد راهت کاروان توتیا آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین کحل الجواهر قسمت من بیشتر باید</p></div>
<div class="m2"><p>که اندر راه او چشم امیدم چارتا آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبارک رجعتت مستلزم صدگونه عشرت شد</p></div>
<div class="m2"><p>ببین تا آمدی نوروز فیروز از قفا آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کلیم از باغ امیدت گل شادی بدامن کن</p></div>
<div class="m2"><p>نهال خوشدلی را موسم نشو و نما آمد</p></div></div>