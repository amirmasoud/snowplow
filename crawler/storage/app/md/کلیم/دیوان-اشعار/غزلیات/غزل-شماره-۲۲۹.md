---
title: >-
    غزل شمارهٔ  ۲۲۹
---
# غزل شمارهٔ  ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>سرفراز آن سر که فارغ از غم سامان شود</p></div>
<div class="m2"><p>بر سرت گل زن که از دستار روگردان شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که چون سوزن زتجریدش بود سررشته ای</p></div>
<div class="m2"><p>صد رهش گر جامه پوشانی دگر عریان شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق بیچاره از یک دیده در پاس رقیب</p></div>
<div class="m2"><p>وز دگر چشمی بکار خویشتن حیران شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیچ جا بهر وطن غیر از دیار عشق نیست</p></div>
<div class="m2"><p>خانه در آن ملک از سیلاب آبادان شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق زخم ما چو سازد جذبه خویش آشکار</p></div>
<div class="m2"><p>تیرها در ترکش او جمله چون پیکان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن ها لاله نبود بلکه ایام حسود</p></div>
<div class="m2"><p>می زند آتش بباغ ار غنچه ای خندان شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو برق آن آفت صد خرمن هوش و خرد</p></div>
<div class="m2"><p>خویش را زان می نماید کز نظر پنهان شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در تماشای پریرویان اقلیم خیال</p></div>
<div class="m2"><p>دیده گر بر هم نهی چشمت نگارستان شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر غم کز حال دل غافل نمی باشد کلیم</p></div>
<div class="m2"><p>کس ندیدم پاسبان خانه ویران شود</p></div></div>