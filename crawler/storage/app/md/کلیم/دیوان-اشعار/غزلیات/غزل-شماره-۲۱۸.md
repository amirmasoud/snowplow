---
title: >-
    غزل شمارهٔ  ۲۱۸
---
# غزل شمارهٔ  ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>در زنگبار خاطر من کار می‌کند</p></div>
<div class="m2"><p>هر صیقلی که آینه را تار می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در بضاعت هنر آتش زند سپهر</p></div>
<div class="m2"><p>آن را حساب گرمی بازار می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم بدل ز پرتو غم‌های روزگار</p></div>
<div class="m2"><p>عکسی که جانشینی زنگار می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اعضا چنین که تحفه دردت به هم دهند</p></div>
<div class="m2"><p>آزار خار پا به جگر کار می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دل به پاسبانی نقد وفای تو</p></div>
<div class="m2"><p>هر داغ کار دیده بیدار می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف به نسیه کس نخرد در زمان ما</p></div>
<div class="m2"><p>دل آرزوی جوش خریدار می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سنگ خاره نیز اثر می‌کند سخن</p></div>
<div class="m2"><p>کوه از صدا همین سخن اظهار می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برداشت بخت اگر زر هم سنگر قضا</p></div>
<div class="m2"><p>اندیشه کشیدن دیوار می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینجا کلیم دعوی خون را گواه نیست</p></div>
<div class="m2"><p>کی پادشه ز قتل کس انکار می‌کند</p></div></div>