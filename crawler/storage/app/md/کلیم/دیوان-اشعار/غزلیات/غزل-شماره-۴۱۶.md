---
title: >-
    غزل شمارهٔ  ۴۱۶
---
# غزل شمارهٔ  ۴۱۶

<div class="b" id="bn1"><div class="m1"><p>بوی کین هرگز کسی نشنیده از آب و گلم</p></div>
<div class="m2"><p>گر به خس آتش فتد از مهر می‌سوزد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون قلم دارم سر تسلیم را در زیر تیغ</p></div>
<div class="m2"><p>هرکسم سر می‌زند گویی که خط باطلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشئه آگاهیم، لیکن درین نخجیرگاه</p></div>
<div class="m2"><p>بر سر تیر همه مانند صید غافلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از در و دیوار می‌گیرم سراغ مرگ را</p></div>
<div class="m2"><p>رهنورد مانده‌ام در آرزوی منزلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع را مانم که از سیر و سلوکم ناامید</p></div>
<div class="m2"><p>هرکجا هستم زاشک خویشتن اندر گلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاله‌وارم دل ز غم صد چاک شد در بی‌کسی</p></div>
<div class="m2"><p>هیچکس ننهاد غیر از داغ دستی بر دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آرزوی یک دل از من در جهان حاصل نشد</p></div>
<div class="m2"><p>مایه نومیدیم، گویی جواب سائلم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌ثمر نخلم، مرا یاری به غیر سایه نیست</p></div>
<div class="m2"><p>سایه خود با خاک یکسانست بنگر حاصلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا قیامت خار غم در جان نمی‌ماند کلیم</p></div>
<div class="m2"><p>گر ز دل بیرون نمی‌آید، برآید از گلم</p></div></div>