---
title: >-
    غزل شمارهٔ  ۴۶۱
---
# غزل شمارهٔ  ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>بر رگ دل گاه ناخن گاه نشتر می زنم</p></div>
<div class="m2"><p>هر زمان بر ساز غم مضراب دیگر می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در لباس شید زاهد در حرم ره می روند</p></div>
<div class="m2"><p>من درین میخانه بدنامم که ساغر می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقده مکتوب ما را از گشادن بهره نیست</p></div>
<div class="m2"><p>این گره بیهوده بر بال کبوتر می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام چون لبریز شد دیگر نمی دارد صدا</p></div>
<div class="m2"><p>با دل پر درد حرف شکوه کمتر می زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه گریبان می درم گه می شکافم سینه را</p></div>
<div class="m2"><p>جستجوئی می کنم خود را بهر در می زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می توان گاهی بمکتوبی مرا خورسند کرد</p></div>
<div class="m2"><p>من ز مردی داستان شکوه را سر می زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تازه می گردد دلم هرگاه آهی می کشم</p></div>
<div class="m2"><p>هر نفس کز دل کشم دامن بر اخگر می زنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خودنمائی شیوه من نیست، چون دیوار باغ</p></div>
<div class="m2"><p>گل بدامن دارم اما خار بر سر می زنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت بر شمع رویش می زنم خود را کلیم</p></div>
<div class="m2"><p>منکه چون پروانه ام خود را برین در می زنم</p></div></div>