---
title: >-
    غزل شمارهٔ  ۱۷۹
---
# غزل شمارهٔ  ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>زان رخنه ها که تن را از ناوک جفا شد</p></div>
<div class="m2"><p>در دشت استخوانم دام ره بلا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دیده توقع از روزگار بستم</p></div>
<div class="m2"><p>در چشمم از غباری بنشست توتیا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکباره عشق کس را زیر و زبر نسازد</p></div>
<div class="m2"><p>دستم بسر همانست پایم اگر ز جا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاطر شکسته بارست مومیائی</p></div>
<div class="m2"><p>آسود از کشاکش دردیکه بیدوا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عریانی جنون را نتوان لباس پوشید</p></div>
<div class="m2"><p>پنهان نمی توان کرد رازی که برملا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در باغ آفرینش آسایشی نمانده است</p></div>
<div class="m2"><p>ناسازگاری گل بدتر ز خار پا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کوی میفروشان در یوزه که گردیم</p></div>
<div class="m2"><p>هر کاسه گدائی جام جهان نما شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا دل طپیده اشکم بنیاد شوره کرده</p></div>
<div class="m2"><p>زنجیر می خروشد دیوانه چون ز جا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دارد کلیم امید از تیره روزی خویش</p></div>
<div class="m2"><p>تا چشم نیم مستش با سرمه آشنا شد</p></div></div>