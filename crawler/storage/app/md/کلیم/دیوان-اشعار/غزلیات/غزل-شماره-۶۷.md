---
title: >-
    غزل شمارهٔ  ۶۷
---
# غزل شمارهٔ  ۶۷

<div class="b" id="bn1"><div class="m1"><p>ضعفم مدد ز قوت صهبا گرفته است</p></div>
<div class="m2"><p>دستم عصا ز گردن مینا گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلک قضا مداد خط سرنوشت ما</p></div>
<div class="m2"><p>گوئی ز درد آتش سودا گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این نه صدف زگوهر آسودگی تهیست</p></div>
<div class="m2"><p>آهم خبر ز عالم بالا گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تخم نهال سرد شود دانه های اشک</p></div>
<div class="m2"><p>تا قامتش بچشم دلم جا گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چیزیکه باز پس طلبند از جهان مگیر</p></div>
<div class="m2"><p>عاقل همین کناره ز دنیا گرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم رهی به پیش که انگشت خارها</p></div>
<div class="m2"><p>از من حساب آبله پا گرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبحیست عارضت که دل از آب و رنگ آن</p></div>
<div class="m2"><p>سامان اشگ ریزی شبها گرفته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان برق حسن کافت هر گوشه گیر شد</p></div>
<div class="m2"><p>آتش در آشیانه عنقا گرفته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر از زبان ندیده براه طلب کلیم</p></div>
<div class="m2"><p>گر زانکه قطره داده و دریا گرفته است</p></div></div>