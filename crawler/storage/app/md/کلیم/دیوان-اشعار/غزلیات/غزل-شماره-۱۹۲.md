---
title: >-
    غزل شمارهٔ  ۱۹۲
---
# غزل شمارهٔ  ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>حسنی که باو عشق سروکار ندارد</p></div>
<div class="m2"><p>مانند طبیبی است که بیمار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حرفیکه دل غمزده ای زو بگشاید</p></div>
<div class="m2"><p>غیر از لب پرخنده سوفار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ضعفم نکند تکیه بنیروی بزرگان</p></div>
<div class="m2"><p>کاه تن من پشت بدیوار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بخت سیه ناله ما یافت رواجی</p></div>
<div class="m2"><p>شب تا نشود شمع خریدار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از روی تنک تن بکدورت دهد ارنه</p></div>
<div class="m2"><p>آئینه سر صحبت زنگار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خارست به پیراهن فانوس گل شمع</p></div>
<div class="m2"><p>گر رنگی از آن گلشن رخسار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در جستن من آبله زد پای کسادی</p></div>
<div class="m2"><p>یکجنس نیابی که خریدار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شوریدگی از خاطر ما دور نگردد</p></div>
<div class="m2"><p>دیوانه ز ویرانه خود عار ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهتر ز گلی کو دل بلبل نخراشد</p></div>
<div class="m2"><p>خاریکه بدامان کسی کار ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مشرب رندان بنسب نیست بزرگی</p></div>
<div class="m2"><p>در بزم سر آنستکه دستار ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در چشم کلیم از اثر گریه گل افتاد</p></div>
<div class="m2"><p>دیگر هوس دیدن گلزار ندارد</p></div></div>