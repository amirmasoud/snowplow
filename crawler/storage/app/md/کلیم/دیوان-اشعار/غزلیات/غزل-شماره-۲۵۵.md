---
title: >-
    غزل شمارهٔ  ۲۵۵
---
# غزل شمارهٔ  ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>ایدل چو راز دوست نخواهی سمر شود</p></div>
<div class="m2"><p>نامش چنان مبر که زبان را خبر شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر دارد الفتی بهوایت که چون حباب</p></div>
<div class="m2"><p>با او سفر کند اگر از سر بدر شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جاهل برو ز مرشد بیمعرفت چه فیض</p></div>
<div class="m2"><p>کوری کجا عصاکش کور دگر شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنجیر زلف او دل دیوانه را شناخت</p></div>
<div class="m2"><p>سودا مقررست که شب بیشتر شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منت کش از حمایت کس نیست عجز ما</p></div>
<div class="m2"><p>تا نقش سینه هست که ما را سپر شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دود سپند بیهنری چون شود بلند</p></div>
<div class="m2"><p>آتش زن ستاره اهل هنر شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر اهل عقل فیض جنون کم ز باده نیست</p></div>
<div class="m2"><p>باید کسی ز کار جهان بی خبر شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس اگر بقدر هنر بهره یافتی</p></div>
<div class="m2"><p>بایست آب بحر نصیب گهر شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از هیچیک ندارد امید اثر کلیم</p></div>
<div class="m2"><p>گره آه شعله گردد و اشکش شرر شود</p></div></div>