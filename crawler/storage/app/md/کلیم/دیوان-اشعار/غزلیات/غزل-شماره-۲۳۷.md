---
title: >-
    غزل شمارهٔ  ۲۳۷
---
# غزل شمارهٔ  ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>شُکر گویم هرچه غم با جان مسکین می‌کند</p></div>
<div class="m2"><p>در مذاقم مرگ را دور از تو شیرین می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک کوی خاکساران افسر هرکس که شد</p></div>
<div class="m2"><p>دارد ار بستر ز دیبا خشت بالین می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر حدیث بی‌وفایی‌های خوبان بشنود</p></div>
<div class="m2"><p>بیستون پهلو تهی از نقش شیرین می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل درین گلشن زدبس آسیب دارد در کمین</p></div>
<div class="m2"><p>بال بلبل را خیال دست گلچین می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طفل اشکم از تلون خانه‌های دیده را</p></div>
<div class="m2"><p>گاه می‌سازد سفید و گاه رنگین می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صوفیان از سینه روشن به عجب افتاده‌اند</p></div>
<div class="m2"><p>آری آری مرد را آیینه خودبین می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با عصای عقل هرکس می‌رود در راه عشق</p></div>
<div class="m2"><p>طی دشت آتشین از پای چوبین می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شیخ شهر از باده خاک سبحه را گل ساخته</p></div>
<div class="m2"><p>فرصتش بادا علاج رخنهٔ دین می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله را از لب به دل هرگز نمی‌آرد کلیم</p></div>
<div class="m2"><p>شعله را از ابلهی تعلیم تمکین می‌کند</p></div></div>