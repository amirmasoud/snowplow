---
title: >-
    غزل شمارهٔ  ۳۳۶
---
# غزل شمارهٔ  ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>دل بیهده افغان ز تو ناساز ندارد</p></div>
<div class="m2"><p>چون شیشه که تا نشکند آواز ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این عیب بگیرائی مژگان تو ماند</p></div>
<div class="m2"><p>از رفتن اگر اشک مرا باز ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خلوت دل پرده نشین کیست بجز تو</p></div>
<div class="m2"><p>در سینه صدف غیر گهر راز ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر راز که دل داشت نهان، اشک دگر گفت</p></div>
<div class="m2"><p>پیکان تو رازیست که غماز ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من لب اگر از نوحه و فریاد به بندم</p></div>
<div class="m2"><p>پروانه درین بزم هم آواز ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون دام در و سر زده نتوان بدرون رفت</p></div>
<div class="m2"><p>عیبست قفس را که در باز ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در محفل دیوان کلیمش نتوان یافت</p></div>
<div class="m2"><p>گر شمع سخن شعله آن راز ندارد</p></div></div>