---
title: >-
    غزل شمارهٔ  ۱۶۰
---
# غزل شمارهٔ  ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>در کلبه ما تا بکمر موج شرابست</p></div>
<div class="m2"><p>تا ساغر تبخاله ما پر می نابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت لب ما غمزدگان را ز فغان بست</p></div>
<div class="m2"><p>خاموش نشینیم که بیمار بخوابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیتابی پروانه بر او چه نماید</p></div>
<div class="m2"><p>آن شعله که خورشید ازو در تب و تابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گریه ندانم که چرا می روم از خود</p></div>
<div class="m2"><p>بیهوشیم از چیست چو در ساغرم آبست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک گل بهواداری گلشن بکفم نیست</p></div>
<div class="m2"><p>از تربیت باغ چه در دست سحابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ویرانه من پرتو خورشید ندیدست</p></div>
<div class="m2"><p>هرچند که این خانه زبنیاد خرابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سربسر ملک وی از گریه خلل نیست</p></div>
<div class="m2"><p>تا ساقی ما پادشه عالم آبست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امید درین ره بدل سوخته دارم</p></div>
<div class="m2"><p>پرواز من از بال و پر مرغ کبابست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می رنجم ازو، رنجش دیوانه ز طفلان</p></div>
<div class="m2"><p>پروای که دارد گله ام در چه حسابست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن شعله که در جان کلیم آتش کین زد</p></div>
<div class="m2"><p>بر بوالهوسان هر شررش قطره آبست</p></div></div>