---
title: >-
    غزل شمارهٔ  ۱۹۵
---
# غزل شمارهٔ  ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>عیش در کلبه ما گوشه نشین می باشد</p></div>
<div class="m2"><p>دید و وادید مکن عید همین می باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر و سامانم چون شیشه می نیست زخود</p></div>
<div class="m2"><p>روش اهل خرابات چنین می باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که حرصش فکند هر دری و هر جائی</p></div>
<div class="m2"><p>همه جا صدرنشین همچو نگین می باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نیاید نگهش از پس مژگان بیرون</p></div>
<div class="m2"><p>چه عجب شیوه صیاد کمین می باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتنی نیست غبار دل آزرده ما</p></div>
<div class="m2"><p>همچو گردیست که بر روی زمین می باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب در دیده آئینه خورشید آرد</p></div>
<div class="m2"><p>آب و تابی که در آن صبح جبین می باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو بمحراب چو زهاد نشستن زچه روست</p></div>
<div class="m2"><p>چشم جادوی تو چون آفت دین می باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کلبه فقر هم اسباب تجمل دارد</p></div>
<div class="m2"><p>بوریا مسند ویرانه نشین می باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خانه صبر من از دیدن او سوخت کلیم</p></div>
<div class="m2"><p>این چه شمعیست که در خانه زین می باشد</p></div></div>