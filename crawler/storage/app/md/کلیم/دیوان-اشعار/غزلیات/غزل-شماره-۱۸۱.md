---
title: >-
    غزل شمارهٔ  ۱۸۱
---
# غزل شمارهٔ  ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>دل که لبریز الم شد ز نوا می افتد</p></div>
<div class="m2"><p>جام هرچند که پر شد ز صدا می افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخت اسباب تعلق دل و آسوده نشست</p></div>
<div class="m2"><p>قدم برق بسر منزل ما می افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامه در خون شهیدان کش و بخرام بناز</p></div>
<div class="m2"><p>بتو ای شاخ گل این رنگ قبا می افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوستداری مرا دهر شگون نگرفته</p></div>
<div class="m2"><p>گر بمن سایه کند بال هما می افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف پرکار تو چون تن بشکستن ندهد</p></div>
<div class="m2"><p>هر که از روی تو برخاست بجا می افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نتوان ناصح عریانی ما را پوشید</p></div>
<div class="m2"><p>راز پنهان نشود چون بملا می افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست کس در ره افتادگی از ما در پیش</p></div>
<div class="m2"><p>هر که از پای فتد بر سر ما می افتد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه بگویم که شبم بیتو چسان می گذرد</p></div>
<div class="m2"><p>صبحم از تیرگی شب ز صفا می افتد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شب آدینه بدریوزه میخانه روم</p></div>
<div class="m2"><p>زانکه از هفته همین شب بگدا می افتد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر که عاجزتر ازو خواسته امداد کلیم</p></div>
<div class="m2"><p>دستگیرش بود آنکس که زپا می افتد</p></div></div>