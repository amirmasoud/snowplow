---
title: >-
    غزل شمارهٔ  ۸۸
---
# غزل شمارهٔ  ۸۸

<div class="b" id="bn1"><div class="m1"><p>ای به از گل بر سر احباب خاک خواریت</p></div>
<div class="m2"><p>چاره ساز جان کار افتاده زخم کاریت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کنار نامه اغیار یادم کرده ای</p></div>
<div class="m2"><p>تا بدانم بعد از این قدر فرامش کاریت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایدل از آبحیات نامه های دوستان</p></div>
<div class="m2"><p>بر کناری همچو خس دائم ز بیمقداریت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه قاصد را بمژگان رفت و چشم انتظار</p></div>
<div class="m2"><p>عاقبت آورد بهر ما خط بیزاریت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرهم زخم دلم چون لاله غیر از داغ نیست</p></div>
<div class="m2"><p>چشم دارم اینقدر دلجوئی از غمخواریت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخت شورم منفعل دارد که با آن بیکسی</p></div>
<div class="m2"><p>بسته مرهم از نمک هر دم بزخم کاریت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده امید را کردی سفید از انتظار</p></div>
<div class="m2"><p>دوستان را خود نبود این چشم از دلداریت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشور مهر و وفا بسیار بدآب و هواست</p></div>
<div class="m2"><p>تا درین ملکی دلا لازم بود بیماریت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناله بلبل درین گلزار بس باشد کلیم</p></div>
<div class="m2"><p>خاطر گل را چه رنجانی توهم از زاریت</p></div></div>