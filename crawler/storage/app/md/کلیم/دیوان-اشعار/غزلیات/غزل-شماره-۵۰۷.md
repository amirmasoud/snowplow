---
title: >-
    غزل شمارهٔ  ۵۰۷
---
# غزل شمارهٔ  ۵۰۷

<div class="b" id="bn1"><div class="m1"><p>پیشی ار خواهی بهر پس مانده همراهی گزین</p></div>
<div class="m2"><p>سربلندی بایدت دیوار کوتاهی گزین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ره عصیان هم ایدل همتی باید بلند</p></div>
<div class="m2"><p>بهتر از شیطان رفیق راه گمراهی گزین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز از خجلت بکاه آنجا که شب مهمان شدی</p></div>
<div class="m2"><p>گر غیوری شیوه شمع سحرگاهی گزین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حرمت این خانه را لایق نباشد هر چراغ</p></div>
<div class="m2"><p>از برای کعبه دل شمع آگاهی گزین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پا چو از درها کشیدی، گنج در دامن بیاب</p></div>
<div class="m2"><p>دیده از دنیا چو بستی هر چه می خواهی گزین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نمانی از گرانی ناامید از جذب عشق</p></div>
<div class="m2"><p>از درون جان کاهی از بیرون رخ کاهی گزین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاهان با نزاکت بار عالم می برند</p></div>
<div class="m2"><p>بار بر عالم گذار و فقر بر شاهی گزین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر درون لبریز نشتر باشدت از نیش خلق</p></div>
<div class="m2"><p>لب مبند از شکوه کس مشرب ماهی گزین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رهبر عامی کلیم از وی عصا بهتر بود</p></div>
<div class="m2"><p>در طریقت مرشدت گر اوست گمراهی گزین</p></div></div>