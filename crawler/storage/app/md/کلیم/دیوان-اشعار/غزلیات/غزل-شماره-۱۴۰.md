---
title: >-
    غزل شمارهٔ  ۱۴۰
---
# غزل شمارهٔ  ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>منم که داغ بلا گلشنی بنام منست</p></div>
<div class="m2"><p>گل شکفته من حلقه های دام منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان نمک که توان بست خون ناحق از آن</p></div>
<div class="m2"><p>ملاحتی است که با سرو خوشخرام منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلم نمی شکند، نامه ات نمی سوزد</p></div>
<div class="m2"><p>زبان کلک تو بیزار چون زنام منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا بدام حوادث، زحرص دانه کشید</p></div>
<div class="m2"><p>کدام دانه بغیر از گره بدام منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان بحوصله ممتازم از قدح نوشان</p></div>
<div class="m2"><p>که درد ته خم افلاک وقف جام منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرض زاشک فشانی گهر فروشی نیست</p></div>
<div class="m2"><p>که گریه در غم او ورد صبح و شام منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نیست بهره ام از کام دل، همان گیرم</p></div>
<div class="m2"><p>که هر چه صید مرادست جمله رام منست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه سلسله زلف تست در خاطر</p></div>
<div class="m2"><p>که با کمال جنون ربط با کلام منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کدورت من از ابنای دهر نیست، کلیم</p></div>
<div class="m2"><p>تمام کلفتم از بخت ناتمام منست</p></div></div>