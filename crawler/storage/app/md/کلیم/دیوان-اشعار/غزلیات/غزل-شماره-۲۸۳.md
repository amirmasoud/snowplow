---
title: >-
    غزل شمارهٔ  ۲۸۳
---
# غزل شمارهٔ  ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>خیال روی تو هر گاه سینه تاب شود</p></div>
<div class="m2"><p>بسینه آینه داغم آفتاب شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو گل بسر زدی و شمع گل زسر برداشت</p></div>
<div class="m2"><p>زبیم آنکه مبادا ز شرم آب شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتشم زتغافل نشانده ای، باری</p></div>
<div class="m2"><p>تبسمی که نمک پاش این کباب شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زشوق سوختم و تاب یک نگاهم نیست</p></div>
<div class="m2"><p>حریص باده مبادا تنگ شراب شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروغ دیده ز می جسته ام مرا چشمیست</p></div>
<div class="m2"><p>که چون حباب قدح روشن از شراب شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید کام ز مغرور سرکشی دارم</p></div>
<div class="m2"><p>کزو نگاه بوصل ابد حساب شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کم نگاهی چشمش کلیم می سازم</p></div>
<div class="m2"><p>امید هست که آن مست بی حجاب شود</p></div></div>