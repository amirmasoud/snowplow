---
title: >-
    غزل شمارهٔ  ۴۵۷
---
# غزل شمارهٔ  ۴۵۷

<div class="b" id="bn1"><div class="m1"><p>بار ناموسی نداریم از پی دل می رویم</p></div>
<div class="m2"><p>از تهی پائی چه بی اندیشه در گل می رویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز از سرگشتگی راهی بسر ناورده ایم</p></div>
<div class="m2"><p>مضطرب هر سو چو مرغ نیم بسمل می رویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالع وارون ما از بس به سستی مایلست</p></div>
<div class="m2"><p>پا اگر بر سنگ بگذاریم در گل می رویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون خس و خاشاک سیلاب ایمنیم از گمرهی</p></div>
<div class="m2"><p>پا بدوش راهبر دائم بمنزل می رویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد ما میکن گهی پربار خاطر نیستیم</p></div>
<div class="m2"><p>با همه دیر آمدنها زود از دل می رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست خاشاک وجود ما جدا از سیل غم</p></div>
<div class="m2"><p>ما خس و خاریم، اما کم بساحل می رویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فیض کوی میفروش این بس کز آسیب خمار</p></div>
<div class="m2"><p>بر درش دیوانه می آئیم و عاقل می رویم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوشن تدبیر از تن کنده و آسوده ایم</p></div>
<div class="m2"><p>راه اگر دارد خطر ما نیز غافل می رویم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رنگ خون ما نخواهد رفت از دستش کلیم</p></div>
<div class="m2"><p>این حنا تا هست کی از یاد قاتل می رویم</p></div></div>