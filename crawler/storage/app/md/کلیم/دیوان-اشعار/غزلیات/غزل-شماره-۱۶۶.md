---
title: >-
    غزل شمارهٔ  ۱۶۶
---
# غزل شمارهٔ  ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>ناله می آید بکویت راه چندان دور نیست</p></div>
<div class="m2"><p>گر تو هم گاهی کنی یاد اسیران دور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه ما را می دهی بر باد بفشان دامنت</p></div>
<div class="m2"><p>تا بدانی خاک مشتاقان زدامان دور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کیست در کویت که شبها ناله ام نشنیده است</p></div>
<div class="m2"><p>جمله می دانند کاین بلبل زبستان دور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کند هجرت مدار از آنکه می داند که من</p></div>
<div class="m2"><p>گر کشد کارم بمردن آبحیوان دور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دل و جان بود دادم، ای صبا آخر تو هم</p></div>
<div class="m2"><p>بوی گل را قیمت ارزان کن گلستان دور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست بیتابی بفرقم مشت خاکی هم نریخت</p></div>
<div class="m2"><p>تا زدامانت جدا شد از گریبان دور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با بلا هم پیرهن یارب کسی چون من مباد</p></div>
<div class="m2"><p>پا اگر در دامن آرم از مغیلان دور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دور از آن درگه ندارد خاطرجمعی کلیم</p></div>
<div class="m2"><p>از وطن آواره گر باشد پریشان دور نیست</p></div></div>