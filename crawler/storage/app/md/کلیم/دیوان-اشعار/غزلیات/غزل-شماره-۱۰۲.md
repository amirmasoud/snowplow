---
title: >-
    غزل شمارهٔ  ۱۰۲
---
# غزل شمارهٔ  ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>تمام کاهش تن جمله آفت جانست</p></div>
<div class="m2"><p>مگوی عشق که این آتش و نیستانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>براه عشق که پائی نمی رسد بزمین</p></div>
<div class="m2"><p>غمی که هست ز محرومی مغیلانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکن لباس تعلق که خار وادی قرب</p></div>
<div class="m2"><p>گرفته دامن دیوانه ایکه عریانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز سود راه فنا قطره می شود دریا</p></div>
<div class="m2"><p>حباب دشمن سر بهر جمع سامانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رواج شور جنون کو که بینمک شد شهر</p></div>
<div class="m2"><p>درین دو روز که دیوانه در بیابانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز انقلاب زمان در پناه جهل گریز</p></div>
<div class="m2"><p>که آنچه مانده بیک حال عیش نادانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فروغ عارضت از حلقه های زلف سیاه</p></div>
<div class="m2"><p>چو روشنائی ایمان بکافر ستانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بترک سر نتوانم ز سرنوشت برید</p></div>
<div class="m2"><p>وگرنه چون قلم از سر گذشتن آسانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملایمت کن اگر طاقت جدل تنگست</p></div>
<div class="m2"><p>کلیم چربی کاغذ علاج بارانست</p></div></div>