---
title: >-
    غزل شمارهٔ  ۵۲
---
# غزل شمارهٔ  ۵۲

<div class="b" id="bn1"><div class="m1"><p>پنبه ها بر روی داغ از آتش دل در گرفت</p></div>
<div class="m2"><p>وقت مرهم خوش که بازم سوختن از سر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرکشی با خاکساران کی بجائی می رسد</p></div>
<div class="m2"><p>سرو من از خاک نتوان سایه خود برگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کجا، بد گردی افلاک و انجم از کجا</p></div>
<div class="m2"><p>خاطرم در بزم عیش از گردش ساغر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلستان چون ساقی مستان ندارد گلبنی</p></div>
<div class="m2"><p>تا گل ساغر ازو چیدم گل دیگر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خشن پوشی برون آورد فیض گلخنم</p></div>
<div class="m2"><p>تن قبای تن نما اکنون ز خاکستر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بستگی در کار عاشق مایه کام دلست</p></div>
<div class="m2"><p>رشته نتواند گهر را بی گره در بر گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اشک را در چشم از لخت جگر نتوان شناخت</p></div>
<div class="m2"><p>طفل خودسر بود رنگ همنشینان برگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برنمی خیزد کلیم، از بستر راحت تنم</p></div>
<div class="m2"><p>پیکر و بستر ز خون دل به یکدیگر گرفت</p></div></div>