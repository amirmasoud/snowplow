---
title: >-
    غزل شمارهٔ  ۷۵
---
# غزل شمارهٔ  ۷۵

<div class="b" id="bn1"><div class="m1"><p>سردمهری‌های دوران را تلافی از تبست</p></div>
<div class="m2"><p>سوزن خار ملامت‌ها ز نیش عقربست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه همین ما می‌گدازیم از غم بخت سیاه</p></div>
<div class="m2"><p>هرکجا روشندلی دیدیم شمع این شبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله هرجا می‌رسد رنگ دگر بر می‌کند</p></div>
<div class="m2"><p>آتش غمخانه و باد چراغ کوکبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌دلان از یک نگاه گرم از جا می‌روند</p></div>
<div class="m2"><p>ظرف‌های طاقت ما را مگر یک قالبست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتگوی اهل عالم بر سر دنیا به هم</p></div>
<div class="m2"><p>جمله‌ای اصلست جنگ طفل‌های مکتبست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خدا کامی اگر خواهی، به از آرام نیست</p></div>
<div class="m2"><p>در حقیقت یک سوالست و در او صد مطلبست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطع راه کعبه و بتخانه در یک گام کرد</p></div>
<div class="m2"><p>طی ارض عارف از گام فراخ مشربست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانه دام ملایک در زمین حسن تست</p></div>
<div class="m2"><p>کس نمی‌داند در گوشست یا خال لبست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از طبیبان حال خود پوشیده چون دارم کلیم</p></div>
<div class="m2"><p>جامه‌ام پیراهن فانوس از تاب و تبست</p></div></div>