---
title: >-
    غزل شمارهٔ  ۱۳۹
---
# غزل شمارهٔ  ۱۳۹

<div class="b" id="bn1"><div class="m1"><p>نخل قد تو را چون، صورت نگار جان بست</p></div>
<div class="m2"><p>گلدسته سرین را، زان رشته بر میان بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بسکه شد بریده، پیوند راحت از ما</p></div>
<div class="m2"><p>بر زخم ما بشمشیر، مرهم نمی توان بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جائیکه غنچه سنگست، بر آشیان بلبل</p></div>
<div class="m2"><p>عاشق چسان تواند، خود را بگلرخان بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب و گل وجودم از رعشه موج دارست</p></div>
<div class="m2"><p>بی می نمی تواند، مغزم در استخوان بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بستگی که باشد موج می اش کلیدست</p></div>
<div class="m2"><p>پیرمغان گشاید، هر در که آسمان بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گلشن خوش و هوا خوش، گفتی گر چه باید</p></div>
<div class="m2"><p>باید نقاب گل را، بر روی باغبان بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تاب تلافی جور، نازک دلان ندارند</p></div>
<div class="m2"><p>بر زخم لاله و گل، مرهم نمی توان بست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از وضع ناگوار اهل جهان دلی پر</p></div>
<div class="m2"><p>دارم کلیم و باید، از نیک و بد زبان بست</p></div></div>