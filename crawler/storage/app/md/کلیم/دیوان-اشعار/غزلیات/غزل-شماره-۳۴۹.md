---
title: >-
    غزل شمارهٔ  ۳۴۹
---
# غزل شمارهٔ  ۳۴۹

<div class="b" id="bn1"><div class="m1"><p>اگر چه نخل هنر را ثمر نمی باشد</p></div>
<div class="m2"><p>ز سنگ بدگهران بیخطر نمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآه خلق بپرهیز کاینه است گواه</p></div>
<div class="m2"><p>که در زمانه دم بی اثر نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین محیط گر از سود چشم می پوشی</p></div>
<div class="m2"><p>سفینه را ز شکستن خطر نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر که سینه صد چاک را نمودم گفت</p></div>
<div class="m2"><p>برو که مرهم زخم سپر نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپهر تا پدری می کند نمی بینم</p></div>
<div class="m2"><p>پسر که تشنه خون پدر نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل آن بود که نجوید زتیغ جور پناه</p></div>
<div class="m2"><p>دمی که سینه سپر شد جگر نمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زتیغ خط بمزار شهید خویش کشد</p></div>
<div class="m2"><p>ستیزه جوئی ازین بیشتر نمی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنزد پایه شناسان بلند پروازی</p></div>
<div class="m2"><p>بغیر ریختن بال و پر نمی باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهوش رفته دل ما بخود نیاید باز</p></div>
<div class="m2"><p>باین درازی عمر سفر نمی باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرم ز پنبه مینا سبکترست کلیم</p></div>
<div class="m2"><p>که مغز در سرم از دردسر نمی باشد</p></div></div>