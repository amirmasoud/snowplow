---
title: >-
    غزل شمارهٔ  ۸۳
---
# غزل شمارهٔ  ۸۳

<div class="b" id="bn1"><div class="m1"><p>جز از غبار مذلت دلم جلا نگرفت</p></div>
<div class="m2"><p>بخاک تا نفتاد این گهر صفا نگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دستبرد حوادث گریخت یک سر تیر</p></div>
<div class="m2"><p>هدف بدشت بلا نیز جای ما نگرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رمیده اند چنان از خط هواداران</p></div>
<div class="m2"><p>که زلف جانب رخساره ترا نگرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکار نعمت دنیا نمی شود قانع</p></div>
<div class="m2"><p>بلی ز دانه فشانی کسی هما نگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زکینه جوئی ما دشمنان ملول شدند</p></div>
<div class="m2"><p>ولی هنوز دل دوست از جفا نگرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زعشق رنگ نداری بدوست رو منما</p></div>
<div class="m2"><p>سرشک اگر زرخت رنگ کهربا نگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>براه فقر و فنا منت از کسی داریم</p></div>
<div class="m2"><p>که گر ز پای فتادیم دست ما نگرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اصول رقص سپند از نهاد او مطلب</p></div>
<div class="m2"><p>کسی کز آبله اخگر بزیر پا نگرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کلیم یک ره از آن شوخ زود سیر بپرس</p></div>
<div class="m2"><p>وفا چه کرد که در خاطر تو جا نگرفت</p></div></div>