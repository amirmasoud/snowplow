---
title: >-
    غزل شمارهٔ  ۲۵۸
---
# غزل شمارهٔ  ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>بهره ای نگرفت گر کام دل بیتاب دید</p></div>
<div class="m2"><p>بخت ما دایم رخ مقصود را در خواب دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر روشندلان از گرد کلفت‌های دهر</p></div>
<div class="m2"><p>تیره شد چندانکه نتوانیم رو در آب دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلبه ویران ما از رخنه سنگ ستم</p></div>
<div class="m2"><p>پای تا سر چشم گردیده و ره سیلاب دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من درین بحر از پی سرگشتگی افتاده ام</p></div>
<div class="m2"><p>کشتیم در رقص آمد هر کجا گرداب دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که در راه عبادت دیده اش بیناترست</p></div>
<div class="m2"><p>قبله منصور دارد دار را محراب دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهرو بحر فنا در طی بحر زندگی</p></div>
<div class="m2"><p>آب چون بگذشتش از سر آن زمان پایاب دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد از بس در متاع دعوی خود آب کرد</p></div>
<div class="m2"><p>در گمان افتاد فسق و دامن تر باب دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر شکافی سینه ام پیکان ز دل نتوان شناخت</p></div>
<div class="m2"><p>رنگ اختر دارد آهن کز آتش تاب دید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب دریا را بجوی تیغ بیدادت مبند</p></div>
<div class="m2"><p>بسکه سیر آبست شمشیر تو زخم از آب دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لابه بی نفع است دربد گردی گردون کلیم</p></div>
<div class="m2"><p>چرخ بی پروا چه زاری‌ها که از دولاب دید</p></div></div>