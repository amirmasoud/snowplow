---
title: >-
    غزل شمارهٔ  ۱۳۳
---
# غزل شمارهٔ  ۱۳۳

<div class="b" id="bn1"><div class="m1"><p>در مزرع بختم اثر نشو و نما نیست</p></div>
<div class="m2"><p>از گریه من آب اگر هست هوا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کج نرود آنکه زمیخانه در آمد</p></div>
<div class="m2"><p>این کج روشی ها گنه آن مژه ها نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمع بهر جا که نشاندند نشستیم</p></div>
<div class="m2"><p>با هیچکسم گفت و شنو بر سر جا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که مژگان تو برگشت ز عاشق</p></div>
<div class="m2"><p>آن هست که روی سخنش جانب ما نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صد ره اگرم بخت ببازار فرستد</p></div>
<div class="m2"><p>چون خون هدر بر سر من نام بها نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمیزش ابنای جهان عین نفاقست</p></div>
<div class="m2"><p>هر جا قدم صلح رسیدست صفا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی و غم عشق بهر کس نپسندیم</p></div>
<div class="m2"><p>خار و گل او لایق هر بی سروپا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی قطع تعلق عبث است اینهمه طاعت</p></div>
<div class="m2"><p>سر تا نبریدست ازو سجده روا نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کوش کلیم ار ندهد فیض سخن روی</p></div>
<div class="m2"><p>اینجاست که ابرام خنک عیب گدا نیست</p></div></div>