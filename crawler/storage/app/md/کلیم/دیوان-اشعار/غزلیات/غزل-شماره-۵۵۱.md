---
title: >-
    غزل شمارهٔ  ۵۵۱
---
# غزل شمارهٔ  ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>آمد آن هوش ربای دل کار افتاده</p></div>
<div class="m2"><p>زلف آشفته بپایش چو نگار افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسرت ناوک او می کشدم این چه بلاست</p></div>
<div class="m2"><p>که اگر تیر خطا گشته شکار افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همرهان دشمن و من بیکس و رهزن در پی</p></div>
<div class="m2"><p>دستم از کار فرو مانده و باز افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامه کاغذ آتش زده را می ماند</p></div>
<div class="m2"><p>جابجا اشک چو افشان شرار افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسن در کسوت یکرنگی عشق ار نبود</p></div>
<div class="m2"><p>گل بخون لاله در آتش بچکار افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحساب زر خود می کند ایمان تازه</p></div>
<div class="m2"><p>خواجه آندم که نفسها بشمار افتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشته عشق شو ایدل که زخس خوارترست</p></div>
<div class="m2"><p>هر که زین بحر سلامت بکنار افتاده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیست در محفل این تیره دلان راه چراغ</p></div>
<div class="m2"><p>کار پروانه بسرهای هزار افتاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قیمت و قدر کلیم ای بت رعنا بشناس</p></div>
<div class="m2"><p>سرو بی فاخته از چشم بهار افتاده</p></div></div>