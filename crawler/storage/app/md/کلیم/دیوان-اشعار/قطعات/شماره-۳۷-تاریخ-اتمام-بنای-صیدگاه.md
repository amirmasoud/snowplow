---
title: >-
    شمارهٔ ۳۷ - تاریخ اتمام بنای صیدگاه
---
# شمارهٔ ۳۷ - تاریخ اتمام بنای صیدگاه

<div class="b" id="bn1"><div class="m1"><p>پادشاه زمانه شاه جهان</p></div>
<div class="m2"><p>شد بعهدش شکفته گلشن عیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نذر صاحبقران ثانی کرد</p></div>
<div class="m2"><p>دهر کشت مراد خرمن عیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پای در راه بندگیش نهد</p></div>
<div class="m2"><p>هر که خواهد بدست دامن عیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا بود دست کوتاهی</p></div>
<div class="m2"><p>شد بدورانش طوق گردن عیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبع از انبساط ایامش</p></div>
<div class="m2"><p>همچو می عاجز از نهفتن عیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طرح در صیدگاه بازی کرد؟</p></div>
<div class="m2"><p>این بنا را که شد نشیمن عیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت تاریخ سال اتمامش</p></div>
<div class="m2"><p>(صیدگاه نشاط و مسکن عیش)</p></div></div>