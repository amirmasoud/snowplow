---
title: >-
    شمارهٔ ۸ - از بزرگی طلب ادای قروض خود را کرده است
---
# شمارهٔ ۸ - از بزرگی طلب ادای قروض خود را کرده است

<div class="b" id="bn1"><div class="m1"><p>ای خداوندی که باشد نسبت انعام تو</p></div>
<div class="m2"><p>راست با حرص و طمع چون نسبت دست و دهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست جودت از جهان، رسم قناعت برفکند</p></div>
<div class="m2"><p>می کند اکنون هما پهلو تهی از استخوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقطه شک بر سر دریا نهد ابر از حباب</p></div>
<div class="m2"><p>بحر دستت را اگر روزی سپند در فشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همتت می خواست یک را از عدد بیرون کند</p></div>
<div class="m2"><p>گشت آخر وحدت واجب شفاعتخواه آن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کام بخشا، از هجوم قرض خواهان می کشم</p></div>
<div class="m2"><p>آن پریشانی که زر در دست صاحب همتان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منکه چون عیسی مجرد گشته ام از مفلسی</p></div>
<div class="m2"><p>می گریزم از کف ایشان کنون بر آسمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در زمین صدره فرو رفتم من از شرمندگی</p></div>
<div class="m2"><p>از تهی دستی بدستم نیست اکنون ناخنان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقد می خواهند از من وجه قرض خویشرا</p></div>
<div class="m2"><p>وین تعدی بین که نستانند از من نقد جان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسکه هر دم بر سر راه من آیند از غرور</p></div>
<div class="m2"><p>در گمان افتم، که معشوقم من، ایشان عاشقان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قافیه گر شایگان افتاد عیب من مکن</p></div>
<div class="m2"><p>شایگان بندم همین بر باد گنج شایگان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسکه سنگینم زبار قرض ایشان بعد مرگ</p></div>
<div class="m2"><p>استخوانم بر هما بارست چون کوه گران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست از من برنمی دارند بهر هر درم</p></div>
<div class="m2"><p>تا نمی گیرند از من همچو قارون صد زمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روزگار ار قرض ایشان داشتی مانند من</p></div>
<div class="m2"><p>می نمودندی ز رفتن منع اجزای زمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کاشکی می داشتی تا روزگار دولتت</p></div>
<div class="m2"><p>می بماندی در جهان چون نام نیکت جاودان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مردمان گویند مفلس در امان حق بود</p></div>
<div class="m2"><p>سایه حق چون توئی، زان از تو می خواهم امان</p></div></div>