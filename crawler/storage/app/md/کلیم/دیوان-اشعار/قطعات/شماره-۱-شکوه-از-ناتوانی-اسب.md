---
title: >-
    شمارهٔ ۱ - شکوه از ناتوانی اسب
---
# شمارهٔ ۱ - شکوه از ناتوانی اسب

<div class="b" id="bn1"><div class="m1"><p>خدایگانا اسبی که داده ای به کلیم</p></div>
<div class="m2"><p>ز ناتوانی هرگز نرفته رو به نسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه از عرق خویش کشتی است در آب</p></div>
<div class="m2"><p>شده به یک جا از لنگر رکاب مقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای رفتن هر گام خوش کند ساعت</p></div>
<div class="m2"><p>زرگ کشیده بر اندام جدول تقویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بسکه کاهل طبعش ز راه ترسیده</p></div>
<div class="m2"><p>رمد ز جاده همچون ز مار شخص دهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نه اسب مرا دیده است افلاطون</p></div>
<div class="m2"><p>چنین دلیر نگفتی که عالمست قدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سکندری خور و گه گیر و بدلجام و حرون</p></div>
<div class="m2"><p>کسی ندارد زینگونه اسب خوش تعلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بکون نشست چو سر از سکندری برداشت</p></div>
<div class="m2"><p>بچوب دنگ تو گوئی نشسته است کلیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه تازیانه که ازو صنع ایزدی خورده</p></div>
<div class="m2"><p>بدینقدر که سرش کرد بر دمش تقدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پل صراط شده گردنش ز باریکی</p></div>
<div class="m2"><p>چو اهل حشر بر او یال مضطرب از بیم</p></div></div>