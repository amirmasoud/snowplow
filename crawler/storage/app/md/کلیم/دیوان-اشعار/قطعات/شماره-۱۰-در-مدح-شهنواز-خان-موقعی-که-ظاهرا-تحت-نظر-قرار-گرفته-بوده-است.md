---
title: >-
    شمارهٔ ۱۰ - در مدح شهنواز خان موقعی که ظاهرا تحت نظر قرار گرفته بوده است
---
# شمارهٔ ۱۰ - در مدح شهنواز خان موقعی که ظاهرا تحت نظر قرار گرفته بوده است

<div class="b" id="bn1"><div class="m1"><p>حدیث شکوه گردون بلند خواهم کرد</p></div>
<div class="m2"><p>مگر بدرگه خان جهان رسد فریاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه اهل هنر شهنواز خان، که کند</p></div>
<div class="m2"><p>ز رای روشن او آفتاب استمداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان بذات عدیم المثال او نازان</p></div>
<div class="m2"><p>بدان مثابه که اهل هنر باستعداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشد شمار عطاهای بیحدش هر دم</p></div>
<div class="m2"><p>ز صفر، حلقه بگوش مراتب اعداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خرد ز وسعت میدان همتش گفته</p></div>
<div class="m2"><p>هزار برهان بر لاتناهی ابعاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعهد عدلش خنجر کشیده می آید</p></div>
<div class="m2"><p>بانتقام کشیدن چراغ بر ره باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دمیکه خامه نگارد حدیث قدرش را</p></div>
<div class="m2"><p>سیاهی شب قدر آورد بجای مداد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قضا بهفته و ایام کرد تعبیرش</p></div>
<div class="m2"><p>چو تیغ تیزش پیوندهای دهر گشاد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز شرم ناخن اندیشه اش همی فکند</p></div>
<div class="m2"><p>سر خجالت در پیش تیشه فرهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زهی شکسته اهل هنر درست از تو</p></div>
<div class="m2"><p>چه واقعست که ما را نمی کنی امداد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سزای بیگنهان گر چنین بود، چکنم</p></div>
<div class="m2"><p>بفرض اگر گنهی کس بما کند اسناد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بکنج ده من سی روزه، مست رسوا را</p></div>
<div class="m2"><p>زمانه چله نشین کرده است چون زهاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روا بود که فراموش کرده ای از من</p></div>
<div class="m2"><p>خصوص از پی صد گونه شکوه و بیداد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرفتم اینکه رهم می دهی بخاطر خویش</p></div>
<div class="m2"><p>زبسکه مضطربم زود می روم از یاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رای آمدن ار نیست، رخصت رفتن</p></div>
<div class="m2"><p>کرم نما که درین ده نمی توان استاد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان مثابه ازین آمدن سبک شده ام</p></div>
<div class="m2"><p>که همچو موج به پس می روم زجنبش باد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هزار کوه غمم سنگ راه شد، تا کی</p></div>
<div class="m2"><p>ز نوک خامه کنم کار تیشه فرهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کلیم گوهر ارزنده ایست، حیرانم</p></div>
<div class="m2"><p>که از کجا بکف طالع زبون افتاد</p></div></div>