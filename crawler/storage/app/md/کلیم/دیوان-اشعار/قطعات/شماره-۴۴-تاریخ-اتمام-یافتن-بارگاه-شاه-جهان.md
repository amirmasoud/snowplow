---
title: >-
    شمارهٔ ۴۴ - تاریخ اتمام یافتن بارگاه شاه جهان
---
# شمارهٔ ۴۴ - تاریخ اتمام یافتن بارگاه شاه جهان

<div class="b" id="bn1"><div class="m1"><p>غبار دیده بد رفته باد ازین درگاه</p></div>
<div class="m2"><p>که سربلند و فلک دستگاه آمده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهار نقش و نگاری چو خط مه رویان</p></div>
<div class="m2"><p>بجلوه بهر فریب نگاه آمده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز رشته های شعاعی طناب تابد مهر</p></div>
<div class="m2"><p>که بهر سایه حق جلوه گاه آمده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ستوده ثانی صاحبقران و شاه جهان</p></div>
<div class="m2"><p>که بارگاهی عرش اشتباه آمده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>براه بندگیش جبهه سرافرازان</p></div>
<div class="m2"><p>بسان نقش قدم روبراه آمده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تمام گشت بسر کاری علی مردان</p></div>
<div class="m2"><p>که او بخاک شه دین پناه آمده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بارگاه شهنشاه بود تاریخش</p></div>
<div class="m2"><p>(اتاق و بارگه پادشاه) آمده است</p></div></div>