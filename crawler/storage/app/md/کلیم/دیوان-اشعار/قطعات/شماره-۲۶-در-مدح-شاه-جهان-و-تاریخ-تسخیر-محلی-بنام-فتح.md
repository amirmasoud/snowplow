---
title: >-
    شمارهٔ ۲۶ - در مدح شاه جهان و تاریخ تسخیر محلی بنام فتح
---
# شمارهٔ ۲۶ - در مدح شاه جهان و تاریخ تسخیر محلی بنام فتح

<div class="b" id="bn1"><div class="m1"><p>شاه آفاق گیر، شاه جهان</p></div>
<div class="m2"><p>که بود خاک راهش افسر فتح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لشگرش در ذخیره اقبال</p></div>
<div class="m2"><p>می گذارند فتح بر سر فتح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خط زخمها به پیکر خصم</p></div>
<div class="m2"><p>می نویسند جمله محضر فتح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر دشمنش نهال سنان</p></div>
<div class="m2"><p>می دمد چار فصل نوبر فتح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعل اسب سپاهش اندر رزم</p></div>
<div class="m2"><p>هر یکی حلقه ایست بر در فتح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همعنان ظفر روانه نمود</p></div>
<div class="m2"><p>لشگری را بسوی کشور فتح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت هر کس بجنگ تاریخی</p></div>
<div class="m2"><p>تا بکام که گردد اختر فتح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تاج اقبال را نهاده بسر</p></div>
<div class="m2"><p>آنکه تاریخ یافت(لشکر فتح)</p></div></div>