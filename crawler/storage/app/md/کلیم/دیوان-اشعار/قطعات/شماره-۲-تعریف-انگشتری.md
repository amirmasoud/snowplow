---
title: >-
    شمارهٔ ۲ - تعریف انگشتری
---
# شمارهٔ ۲ - تعریف انگشتری

<div class="b" id="bn1"><div class="m1"><p>بدستم آمده انگشتری که گردیده</p></div>
<div class="m2"><p>ز درد رشکش عیش دهان خوبان تنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بس که انگشت از ذوق آن به خود بالید</p></div>
<div class="m2"><p>برون نیاید از دست من به صد نیرنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دست کار حنا می‌کند ز رنگ نگین</p></div>
<div class="m2"><p>ببین ز پرتو یاقوت پنجه گلرنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دست هرکه چراغی ازین نگین دادند</p></div>
<div class="m2"><p>دگر به راه طلب پا نمی‌زند بر سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برین خجسته نگین اسم خویش نقش کنم</p></div>
<div class="m2"><p>چو نام خود را خواهم برآورم از ننگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مادری که جگرگوشه کرده باشد گم</p></div>
<div class="m2"><p>همیشه کان به فراقش به سینه کوبد سنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه بینی از آن آب و رنگ یاقوتش</p></div>
<div class="m2"><p>روان ز جدول انگشت آب آتش رنگ</p></div></div>