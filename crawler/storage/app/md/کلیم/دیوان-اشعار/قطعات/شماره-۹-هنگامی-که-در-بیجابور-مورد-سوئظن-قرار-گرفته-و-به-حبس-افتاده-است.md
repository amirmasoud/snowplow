---
title: >-
    شمارهٔ ۹ - هنگامی که در بیجابور مورد سوئظن قرار گرفته و به حبس افتاده است
---
# شمارهٔ ۹ - هنگامی که در بیجابور مورد سوئظن قرار گرفته و به حبس افتاده است

<div class="b" id="bn1"><div class="m1"><p>فلک قد را نمی پرسی که گردون</p></div>
<div class="m2"><p>چرا آزرد ما را بی محابا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا زد راه بیمار غمی را</p></div>
<div class="m2"><p>که می آید بدرگاه مسیحا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث طرفه ای دارم که باشد</p></div>
<div class="m2"><p>برای بیدماغان به ز صهبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعزم سیر بیجابور گشتیم</p></div>
<div class="m2"><p>رهی با اختری خوش دشت پیما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو بال طایر شوقیم و هر دو</p></div>
<div class="m2"><p>نمی بودیم یکساعت شکیبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی آخر زچشم زخم گردون</p></div>
<div class="m2"><p>عجایب سنگ راهی گشت پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بچنگ زاهد از آن اوفتادیم</p></div>
<div class="m2"><p>چگویم تا چها کردند با ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه اندر تجسس موشکافان</p></div>
<div class="m2"><p>همه در کنجکاوی ذهن دانا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسرحد عدم گر جای گیرند</p></div>
<div class="m2"><p>نخواهند رفت کس بیرون ز دنیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یکی گوید که دزدانند و باشند</p></div>
<div class="m2"><p>بزندان چند گه زنجیر فرسا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دگر گوید که جاسوس فلانند</p></div>
<div class="m2"><p>که از تفتیش ما گشتند رسوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی می گوید اینان را بکاوید</p></div>
<div class="m2"><p>که شاید نامه ای گردد هویدا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زبس تفتیش از هم می گشودند</p></div>
<div class="m2"><p>مگر دربار ما بودی معما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بجرم اینکه می ماند بنامه</p></div>
<div class="m2"><p>کشیدند استخوانهارا ز اعضا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن غوغا ز ترس خود دریدند</p></div>
<div class="m2"><p>ملایک نامه اعمال ما را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بغیر از سرنوشت بد که کم باد</p></div>
<div class="m2"><p>نوشته همره ما نیست اصلا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خط پیشانیم از خاک مالی</p></div>
<div class="m2"><p>بشد ارنه وبالی بود ما را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کنون در چنگ ایشان مبتلائیم</p></div>
<div class="m2"><p>نمی دانیم چاره جز مدارا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو مژگان پیش چشمم ایستاده</p></div>
<div class="m2"><p>سیاهان روز و شب بهر تماشا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بهر پاس ما جمع دگرشان</p></div>
<div class="m2"><p>چو مو استاده دایم بر سر پا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برای ضبط ما پر بسته مرغان</p></div>
<div class="m2"><p>همه هم پشت همچون موج دریا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عجب دارم که با آن منع جاده</p></div>
<div class="m2"><p>چنان بیخواست آمد تا بآنجا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نباشد عار اگر خاک درت را</p></div>
<div class="m2"><p>ز نقش جبهه هر بی سروپا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اشارت کن که چون اقبال گردیم</p></div>
<div class="m2"><p>بخاک آستانت جبهه فرسا</p></div></div>