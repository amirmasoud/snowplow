---
title: >-
    شمارهٔ ۴۱ - تاریخ شهادت صلابت خان
---
# شمارهٔ ۴۱ - تاریخ شهادت صلابت خان

<div class="b" id="bn1"><div class="m1"><p>دلا دیدیکه تیغ جور گردون</p></div>
<div class="m2"><p>چه زخم منکری زد بر جگرها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب زخمی که گردد کهنه هرچند</p></div>
<div class="m2"><p>خلاند تازه در دل نیشترها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صلابت خان عزیز مصر دولت</p></div>
<div class="m2"><p>که رویش بود عید دیده ورها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگلزار صلابت کرد پرواز</p></div>
<div class="m2"><p>چو شهبازی بخون آغشته پرها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین غم سربلندان همچو خاتم</p></div>
<div class="m2"><p>ز زانو برنمی دارند سرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدامان صدف در این مصیبت</p></div>
<div class="m2"><p>تمامی اشک حسرت شد گهرها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رویش پرتو روشن ضمیری</p></div>
<div class="m2"><p>هویدا بود چون فیض سحرها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عجب نبود که گردد اشگ خونین</p></div>
<div class="m2"><p>ازین غم در دل خارا شررها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرایند ار حدیث این شهادت</p></div>
<div class="m2"><p>شود پرخون دهان نوحه گرها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زدود دل جهان زانگونه پر شد</p></div>
<div class="m2"><p>که شد بسته ره سیر خبرها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کنون کز غم شکسته پشت احباب</p></div>
<div class="m2"><p>چو شاخ نخل پر بار از ثمرها</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان باید فشاند اشک مصیبت</p></div>
<div class="m2"><p>که پشتیبان شود بهر کمرها</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود تاریخ سال این شهادت</p></div>
<div class="m2"><p>(کباب از ماتم او شد جگرها)</p></div></div>