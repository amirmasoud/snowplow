---
title: >-
    شمارهٔ ۵۱ - تاریخ اتمام بنائی در کشمیر
---
# شمارهٔ ۵۱ - تاریخ اتمام بنائی در کشمیر

<div class="b" id="bn1"><div class="m1"><p>باز طبع هزار دستان را</p></div>
<div class="m2"><p>سر اندیشه در گریبانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روضه فکر در نظر دارد</p></div>
<div class="m2"><p>که در آن فیض عام رضوانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر چشمه اش نشاط مقیم</p></div>
<div class="m2"><p>در عمارت سرور مهمانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمه را آب آینه است بجوی</p></div>
<div class="m2"><p>خوشگواری ازو نمایانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی چشمه دایم الوجدست</p></div>
<div class="m2"><p>حالش از واردات بارانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حوضها هر یکی ز رخشانی</p></div>
<div class="m2"><p>چشمه آفتاب تابانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرد فواره گشته پروانه</p></div>
<div class="m2"><p>که چو شمع از صفا فروزانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر رخ آبشار زلف سفید</p></div>
<div class="m2"><p>دلربا گشته عقل حیرانست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گوهر ریگ جو زصافی آب</p></div>
<div class="m2"><p>چو حباب از رخش نمایانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیش بیننده جدولش گوئی</p></div>
<div class="m2"><p>راست طومار نقره افشانست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنجه موج حوض گشته کبود</p></div>
<div class="m2"><p>بهر سدری آب برهانست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با وجود ولایت کشمیر</p></div>
<div class="m2"><p>چشم ایران چراغ تورانست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از بلندی بخت ز کوه بود</p></div>
<div class="m2"><p>کش چنین دلبری بدامانست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از رخ افکندنش نقاب خفا</p></div>
<div class="m2"><p>زالتفات خدیو دورانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صاحب عالم آنفرشته خصال</p></div>
<div class="m2"><p>که تنش جسم دهر را جانست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با وجود شکوه دارائی</p></div>
<div class="m2"><p>در تواضع فرید دورانست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>میزبانست اگر چه عالم را</p></div>
<div class="m2"><p>بر سر خوان فقر مهمانست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فقر را قدردان بجز او نیست</p></div>
<div class="m2"><p>لیک فقری که فخر مردانست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هست درویش بینوا بر او</p></div>
<div class="m2"><p>کعبه ای کز لباس عریانست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرشد خانقاه تجریدست</p></div>
<div class="m2"><p>عالمش گرچه زیر فرمانست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دل آگاهش و علائق دهر</p></div>
<div class="m2"><p>آن یکی آتش این نیستانست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>با یک اندیشیش بود یک نقش</p></div>
<div class="m2"><p>هر چه در کارگاه امکانست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دو لبش چون زبان یکی گردد</p></div>
<div class="m2"><p>چو بتوحید گوهر افشانست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نقش گلزار خرمی بیند</p></div>
<div class="m2"><p>این بنور چراغ عرفانست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نزد بینائی بصیرت او</p></div>
<div class="m2"><p>ذره خورشید و قطره عمانست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نزد حق بینیش زهر ذره</p></div>
<div class="m2"><p>مور را حشمت سلیمانست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>با وجود شکفتگی رخش</p></div>
<div class="m2"><p>بر گل صبح خنده بهتانست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون ازو یافت صورت اتمام</p></div>
<div class="m2"><p>این بنائی که زیب دورانست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هاتفی گفت بهر تاریخش</p></div>
<div class="m2"><p>(راحت آباد اهل عرفان) است</p></div></div>