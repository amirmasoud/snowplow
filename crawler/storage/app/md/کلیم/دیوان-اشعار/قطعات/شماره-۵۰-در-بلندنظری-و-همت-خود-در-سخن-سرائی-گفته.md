---
title: >-
    شمارهٔ ۵۰ - در بلندنظری و همت خود در سخن سرائی گفته
---
# شمارهٔ ۵۰ - در بلندنظری و همت خود در سخن سرائی گفته

<div class="b" id="bn1"><div class="m1"><p>منم کلیم بطور بلندی همت</p></div>
<div class="m2"><p>که استغاصه معنی جز از خدا نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخوان فیض الهی چو دسترس دارم</p></div>
<div class="m2"><p>نظر به کاسه دریوزه گدا نکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صیدگاه سخن با زیر سیر چشم منم</p></div>
<div class="m2"><p>به صید بسته کس پنجه آشنا نکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زفیض دریا پهلو تهی کنم چو حباب</p></div>
<div class="m2"><p>زرشح قطره بیمایه خود چرا نکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیر گلشن معنی صاحبان سخن</p></div>
<div class="m2"><p>چو غنچه چشم تماشای فکر وا نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زگوهری که بغوص کسی برون آید</p></div>
<div class="m2"><p>اگر بفرض شوم کور توتیا نکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بریده باد ز روحم غذای معنی اگر</p></div>
<div class="m2"><p>برزق موهبت عیسی اکتفا نکنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باخذ معنی در پیش پا فتاده خلق</p></div>
<div class="m2"><p>قد طبیعت برجسته را دوتا نکنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز جذب معنی بیمغز هر تنگ مایه</p></div>
<div class="m2"><p>به ننگ تن ندهم کار کهربا نکنم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو خوشه هر سر مو بر تنم سنان بادا</p></div>
<div class="m2"><p>ز خوشه چینی افلاک اگر ابا نکنم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرچه در فن خود کیمیاگر سخنم</p></div>
<div class="m2"><p>زفکر خود مس معنی کس طلا نکنم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی علاج توارد نمی توانم کرد</p></div>
<div class="m2"><p>مگر زبان سخن گفتن آشنا نکنم</p></div></div>