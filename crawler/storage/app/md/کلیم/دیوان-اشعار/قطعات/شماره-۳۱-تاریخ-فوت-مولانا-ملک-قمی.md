---
title: >-
    شمارهٔ ۳۱ - تاریخ فوت مولانا ملک قمی
---
# شمارهٔ ۳۱ - تاریخ فوت مولانا ملک قمی

<div class="b" id="bn1"><div class="m1"><p>ملک آن پادشاه ملک معنی</p></div>
<div class="m2"><p>که نامش سکه نقد سخن بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان آفاق گیر از اهل معنی</p></div>
<div class="m2"><p>که حد ملکش از قم تا دکن بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زدی از سوز دل در خانه آتش</p></div>
<div class="m2"><p>دوات و کلک او شمع و لگن بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بصورت گر بکلکش آرمیدی</p></div>
<div class="m2"><p>بمعنی ساکن بیت الحزن بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر جا بکر معنی جلوه کردی</p></div>
<div class="m2"><p>باو نزدیکتر از پیرهن بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوی گلزار جنت رفت آخر</p></div>
<div class="m2"><p>که دلگیر از هوای این چمن بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلم چون نی اگر نالد عجب نیست</p></div>
<div class="m2"><p>نه او را در بنان او وطن بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی کانگشت بر حرفش نهادی</p></div>
<div class="m2"><p>زغم انگشت حسرت در دهن بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بجستم سال تاریخش ز ایام</p></div>
<div class="m2"><p>بگفتا (او سر اهل سخن بود)</p></div></div>