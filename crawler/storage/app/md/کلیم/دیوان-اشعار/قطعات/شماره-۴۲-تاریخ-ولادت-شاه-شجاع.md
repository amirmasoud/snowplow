---
title: >-
    شمارهٔ ۴۲ - تاریخ ولادت شاه شجاع
---
# شمارهٔ ۴۲ - تاریخ ولادت شاه شجاع

<div class="b" id="bn1"><div class="m1"><p>داده حق سایه خود را دگر از نو خلفی</p></div>
<div class="m2"><p>که توان دید درو فر فلک جاهی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرنوشتش که از آن یکرقم آمد اقبال</p></div>
<div class="m2"><p>بهترین قطعه بود کلک یداللهی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می توان یافتن از ناحیه شاه شجاع</p></div>
<div class="m2"><p>جوهر دوست نوازی و عدو کاهی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر تاریخ ولادت بعدو گفته فلک</p></div>
<div class="m2"><p>(دو یمین نیر بادا فلک شاهی را)</p></div></div>