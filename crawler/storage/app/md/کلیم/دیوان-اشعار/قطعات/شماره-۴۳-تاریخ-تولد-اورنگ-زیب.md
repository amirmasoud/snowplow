---
title: >-
    شمارهٔ ۴۳ - تاریخ تولد اورنگ زیب
---
# شمارهٔ ۴۳ - تاریخ تولد اورنگ زیب

<div class="b" id="bn1"><div class="m1"><p>داد ایزد بپادشاه جهان</p></div>
<div class="m2"><p>خلفی همچو مهر عالمتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاج صاحبقران ثانی را</p></div>
<div class="m2"><p>گوهر بحر ازو گرفته حساب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامش اورنگ زیب کرده فلک</p></div>
<div class="m2"><p>تخت ازین پایه گشته عرش جناب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون باین مژده آفتاب انداخت</p></div>
<div class="m2"><p>افسر خویش بر هوا چو حباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طبع دریافت سال تاریخش</p></div>
<div class="m2"><p>زد رقم (آفتاب عالمتاب)</p></div></div>