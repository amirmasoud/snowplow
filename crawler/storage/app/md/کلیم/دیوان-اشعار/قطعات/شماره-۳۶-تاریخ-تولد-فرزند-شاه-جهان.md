---
title: >-
    شمارهٔ ۳۶ - تاریخ تولد فرزند شاه جهان
---
# شمارهٔ ۳۶ - تاریخ تولد فرزند شاه جهان

<div class="b" id="bn1"><div class="m1"><p>یکی نیر از برج شاهی دمیده</p></div>
<div class="m2"><p>که نورش گرفته ز مه تا بماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز شاه جهان باد شه تا بآدم</p></div>
<div class="m2"><p>پدر بر پدر صاحب تاج شاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زشاهان کسی این نسب را ندارد</p></div>
<div class="m2"><p>بخوانم نسب نامه هر که خواهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حسب در خور این نسب گشته تعیین</p></div>
<div class="m2"><p>برایش ز دیوان فیض الهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرامی خلف این چنین ناید الحق</p></div>
<div class="m2"><p>ز صاحبقران خلافت پناهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بفر فریدونیش هر که دیده</p></div>
<div class="m2"><p>بدارالشکوهیش داده گواهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگوش دل از بهر تاریخ آمد</p></div>
<div class="m2"><p>(گل اولین گلستان شاهی)</p></div></div>