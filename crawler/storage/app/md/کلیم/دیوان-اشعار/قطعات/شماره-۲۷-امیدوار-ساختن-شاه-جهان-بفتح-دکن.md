---
title: >-
    شمارهٔ ۲۷ - امیدوار ساختن شاه جهان بفتح دکن
---
# شمارهٔ ۲۷ - امیدوار ساختن شاه جهان بفتح دکن

<div class="b" id="bn1"><div class="m1"><p>عرش سیرا شاهباز دولتت</p></div>
<div class="m2"><p>آورد در زیر پر فتح دکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منهی غیب از همایون نهضتت</p></div>
<div class="m2"><p>آورد هر دم خبر فتح دکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینهم از بسیاری کوچک دلیست</p></div>
<div class="m2"><p>کاوری اندر نظر فتح دکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کنند اقبال و دولت در برت</p></div>
<div class="m2"><p>خوش برغم یکدگر فتح دکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انتظار مقدمت را می کشند</p></div>
<div class="m2"><p>گر نشد زین بیشتر فتح دکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت از صندل تدبیر تو</p></div>
<div class="m2"><p>می شود بی دردسر فتح دکن</p></div></div>