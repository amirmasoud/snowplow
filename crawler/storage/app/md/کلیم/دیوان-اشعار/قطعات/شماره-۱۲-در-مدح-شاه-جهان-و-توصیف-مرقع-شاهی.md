---
title: >-
    شمارهٔ ۱۲ - در مدح شاه جهان و توصیف مرقع شاهی
---
# شمارهٔ ۱۲ - در مدح شاه جهان و توصیف مرقع شاهی

<div class="b" id="bn1"><div class="m1"><p>پرورده کدام بهارست این چمن</p></div>
<div class="m2"><p>کز بهر دیدنش نگه از هم کنیم وام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر خط او چو خطه کشمیر دلفریب</p></div>
<div class="m2"><p>وز حلقه حروف براه نظاره دام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دیدنش نظارگیان مست می شوند</p></div>
<div class="m2"><p>آن باده ایکه دایره ها را بود بجام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بسکه دیده خیره شود در نظاره اش</p></div>
<div class="m2"><p>نتوان شناخت دیده کدامست و خط کدام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاقوت ثلث این خط اگر می نگاشتی</p></div>
<div class="m2"><p>مستعصمش بدیده نشاندی ز احترام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تذهیب داد شاهد خط را چه زینتی</p></div>
<div class="m2"><p>آری شفق فزوده بحسن و جمال شام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آراسته بهشتی تصویر حوریان</p></div>
<div class="m2"><p>حوری که باشد او را غلمان کمین غلام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چسبان شد اختلاط خط و صورتش بهم</p></div>
<div class="m2"><p>پیچد بموی طره تصویر زلف لام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مو از زبان چو خامه نقاش سر زند</p></div>
<div class="m2"><p>نطق ار ز حسن صورت او سر کند کلام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تصویر و خط چو صورت و معنی بهم قرین</p></div>
<div class="m2"><p>وز اتحاد کرده در آغوش هم مقام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تمکین حسن اگر نشدی مانع آمدی</p></div>
<div class="m2"><p>در باغ صفحه شاهد تصویر در خرام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چندین هزار نقش بدیع انتخاب کرد</p></div>
<div class="m2"><p>دوران که شد مرقع شاه جهانش نام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صاحبقران ثانی از اقبال سرمدی</p></div>
<div class="m2"><p>شاه ستاره لشکر خورشید احتشام</p></div></div>