---
title: >-
    شمارهٔ ۱۳ - شکایت از تب و لرز
---
# شمارهٔ ۱۳ - شکایت از تب و لرز

<div class="b" id="bn1"><div class="m1"><p>سرور ازین میهمان پُرتَعَب یعنی که تب</p></div>
<div class="m2"><p>چند روزی شد که تصدیع فراوان می‌کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنچه از دست من آمد ز اشک سرخ و روی زرد</p></div>
<div class="m2"><p>پیش او هر لحظه نعمت‌های الوان می‌کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نسوزد در دل من یادگار دوست را</p></div>
<div class="m2"><p>زاستخوان پیکان جانان را به دندان می‌کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ‌های آبدارم هست از فوج سرشک</p></div>
<div class="m2"><p>لیک بر روی مرض از ضعف لرزان می‌کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو طفل نوخطی کاستاد گیرد خامه‌اش</p></div>
<div class="m2"><p>من عصا را بر زمین ز امداد یاران می‌کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در طلسم بی‌قراران من ز خود افتاده‌ام</p></div>
<div class="m2"><p>کم به لوح خاطر آن زلف پریشان می‌کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست چون مقراض انگشت طبیبان آهنین</p></div>
<div class="m2"><p>زخم خوش چیزیست، دست از دست ایشان می‌کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا شمار نوبت تب را نگه دارم درست</p></div>
<div class="m2"><p>بر زمین هرگاه غلتم خط به مژگان می‌کشم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسترم از پهلوی من صفحه مسطر زدست</p></div>
<div class="m2"><p>داستان فربهی را خط نسیان می‌کشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نقاهت گر چو برقم کرده پر سودی نکرد</p></div>
<div class="m2"><p>من هم آخر انتقام خود ز دوران می‌کشم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشک تا بر لب رسد از گرمی ره سوخته</p></div>
<div class="m2"><p>منت خشکی همین از چشم گریان می‌کشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ساغر تبخاله‌ها کو تا دگر پُر می‌شود</p></div>
<div class="m2"><p>جان اگر بر لب رسد خجلت ز مهمان می‌کشم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بوده‌ام فرمانروای عالم آب و کنون</p></div>
<div class="m2"><p>حسرت یک قطره از منع طبیبان می‌کشم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مشتی از خاک درت پنهانی از چشم طبیب</p></div>
<div class="m2"><p>گر فرستی سوی من زان آب حیوان می‌کشم</p></div></div>