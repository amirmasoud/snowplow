---
title: >-
    شمارهٔ ۱۹ - ایضا
---
# شمارهٔ ۱۹ - ایضا

<div class="b" id="bn1"><div class="m1"><p>تفنگ عدو سوز شاه جهان</p></div>
<div class="m2"><p>کزو عمر دشمن شود کاسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگ تیره ابریست پررعد و برق</p></div>
<div class="m2"><p>زرعد کف شاه برخاسته</p></div></div>