---
title: >-
    شمارهٔ ۱۱ - کتابه عمارت شاهنواز خان (از امرای شاه جهان)
---
# شمارهٔ ۱۱ - کتابه عمارت شاهنواز خان (از امرای شاه جهان)

<div class="b" id="bn1"><div class="m1"><p>زهی قصری که گردونت دهد باج</p></div>
<div class="m2"><p>سخن را برده تعریفت بمعراج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زشوق دیدن ایوانت خورشید</p></div>
<div class="m2"><p>نخوابد همچو طفل اندر شب عید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملایک بال بر سقفت کشیده</p></div>
<div class="m2"><p>بطاقت شیشه افلاک چیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشیده طاقت از همت نشانست</p></div>
<div class="m2"><p>کمان قدرت بازوی خانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که بحر همتش را طی نمودی</p></div>
<div class="m2"><p>اگر زین طاق پل بروی نبودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سبک سیری چنین کم دیده ایام</p></div>
<div class="m2"><p>که طی کرده است عالم را بیک گام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نثار کنگر او نقد گردون</p></div>
<div class="m2"><p>فدای پایه او گنج قارون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجز نواب دیگر هیچ موجود</p></div>
<div class="m2"><p>بگل خورشید نتوانست اندود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز انبوه سران سجده پرداز</p></div>
<div class="m2"><p>درش از نقش جبهه سینه باز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگه تا بسته اینجا آشیانه</p></div>
<div class="m2"><p>غریبی می کشد در چشمخانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فلک را رشته جان در کشاکش</p></div>
<div class="m2"><p>ز استغنای این معشوق سرکش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند تا صورت ایوان تماشا</p></div>
<div class="m2"><p>نهاده عرش کرسی تا ته پا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فلک از مهر عالم گرد پرسید</p></div>
<div class="m2"><p>که بر خاک که دیدی روی امید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سوی این آستان کو باد جاوید</p></div>
<div class="m2"><p>بده انگشت اشارت کرد خورشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>قدم کی در خور این سرزمین است</p></div>
<div class="m2"><p>که فرش این زمین نقش جبین است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلندی داده خاک پی سپر را</p></div>
<div class="m2"><p>چو فرزند خلف نام پدر را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>باو باید که نازد عالم خاک</p></div>
<div class="m2"><p>که از طاقش شکسته پشت افلاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملایک جمله زانجا رخت بسته</p></div>
<div class="m2"><p>که نتوان ماند در طاق شکسته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تلاش کهربائی کرده خورشید</p></div>
<div class="m2"><p>کزین دیوار کاهی دارد امید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گل خورشید از خاکش توان چید</p></div>
<div class="m2"><p>فروغ آتش از سنگش توان دید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فلک را بین که با چندین بضاعت</p></div>
<div class="m2"><p>بیک خورشید چون کرده قناعت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درو از صورت نواب دوران</p></div>
<div class="m2"><p>بهر سو هست صد خورشید تابان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زبس افراخت او را دست همت</p></div>
<div class="m2"><p>بچین صورتگران حیران صورت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بعاشق پروری زان سان سر آمد</p></div>
<div class="m2"><p>که در آغوش چندین کشور آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>محیط حوض را تا ابر دیده</p></div>
<div class="m2"><p>بسان موج از دریا رمیده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی کز آب پاکش مایه دارد</p></div>
<div class="m2"><p>بجز بر گلشن جنت نیارد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زلال کوثرست و صاف زمزم</p></div>
<div class="m2"><p>نم او زخم جدول راست مرهم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز تمثال شه و گلهای بیخار</p></div>
<div class="m2"><p>در ایوان بینی ابراهیم و گلزار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شه عادل خدیو ملک اقبال</p></div>
<div class="m2"><p>گشاد جبهه اش امید را فال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بنزد همت او داشتن عار</p></div>
<div class="m2"><p>خوشش ناید گرش خوانم جهاندار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر آن یوسف لقای مسند آرا</p></div>
<div class="m2"><p>عروس ملک مفتون چون زلیخا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خلیل آسا بنوعی بت شکسته</p></div>
<div class="m2"><p>که نظم باد تا از هم گسسته</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو گیرد گاه مرگ اعداش را تب</p></div>
<div class="m2"><p>بهم پیوندد آنهم نامرتب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ستم در روزگارش میر عدل است</p></div>
<div class="m2"><p>سر زلف بتان زنجیر عدل است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزیر خاتمش زانسان زمین است</p></div>
<div class="m2"><p>که پنداری زمین نقش نگین است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زتیغ تیز و از تدبیر نواب</p></div>
<div class="m2"><p>پی تسخیر عالم دارد ابواب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>وزیر پیش بین دستور دانا</p></div>
<div class="m2"><p>دلش آئینه احوال فردا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز حال دشمنان آنسان خبر یافت</p></div>
<div class="m2"><p>که می داند چه می بینند در خواب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خبر دار از دل بیگانه و خویش</p></div>
<div class="m2"><p>چو صاحبخانه از کاشانه خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز دستش آنچه ناید انتقام است</p></div>
<div class="m2"><p>که تیغ کینه اش عالم نیام است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کسی کز آستانش رو بتابد</p></div>
<div class="m2"><p>عجب کز آینه هم رو بیابد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همیشه شاهد بختش جوان باد</p></div>
<div class="m2"><p>پناه دوستان و دشمنان باد</p></div></div>