---
title: >-
    شمارهٔ ۱۵ - وله ایضا
---
# شمارهٔ ۱۵ - وله ایضا

<div class="b" id="bn1"><div class="m1"><p>کلید سخن را چو پیدا کنم</p></div>
<div class="m2"><p>در وصف دولتسرا وا کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زبانی ز همت بلندان بوام</p></div>
<div class="m2"><p>بگیرم که گویم ز قدرش کلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر رفعت و پای بنیاد او</p></div>
<div class="m2"><p>که عرش آشنا شد بامداد او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سراپا چو طوبی است راحت فزا</p></div>
<div class="m2"><p>چو زلف سیه سایه اش دلربا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرافکند در پیش جاهش حباب</p></div>
<div class="m2"><p>که با آن نماندست آن آب و تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمانه بسی گر چه آرایدش</p></div>
<div class="m2"><p>ولی مقدم شاه می باشدش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه معدلتخواه، شاه جهان</p></div>
<div class="m2"><p>ملاذ سلاطین، شاه جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که بر درگهش صبحدم سرگماشت؟</p></div>
<div class="m2"><p>که شب تاج خورشید بر سر نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی را تب شمع کمتر غلام</p></div>
<div class="m2"><p>مقرر کند حاصل ملک شام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تواند دو صد صف شکستن برزم</p></div>
<div class="m2"><p>که یکدل نیارد شکستن ببزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>درش راز شاه و گدا نیست ننگ</p></div>
<div class="m2"><p>که در پیش دریاچه خس چه نهنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمانش بهاریست پر رنگ و بو</p></div>
<div class="m2"><p>درم چون شکوفه است ریزان ازو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود یارب از فضل پروردگار</p></div>
<div class="m2"><p>حیات خضر سبزه ای زین بهار</p></div></div>