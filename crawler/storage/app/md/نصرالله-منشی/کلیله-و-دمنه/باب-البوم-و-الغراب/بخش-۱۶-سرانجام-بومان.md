---
title: >-
    بخش ۱۶ - سرانجام بومان
---
# بخش ۱۶ - سرانجام بومان

<div class="n" id="bn1"><p>ملک بومان را چنانکه رسم بی دولتان است این نصایح ندانست شنود و عواقب آن را نتوانست دید. وزاغ هر روزی برای ایشان حکایت دل گشای و مثل غریب و افسانه عجیب می‌آوردی، و بنوعی در محرمیت خویش می‌افزود تا بر غوامض اسرار اخبار ایشان وقوف یافت. ناگاه فرومولید و نزدیک زاغان رفت. چون ملک زاغان او را بدید پرسید: ما وراءک یا عصام؟ گفت: </p></div>
<div class="b" id="bn2"><div class="m1"><p>شاد شو ای منهزم، که در مدد تو </p></div>
<div class="m2"><p>حمله تایید و رکضت ظفر آید </p></div></div>
<div class="n" id="bn3"><p>و بدولت ملک آنچه می‌بایست بپرداختم، کار را باید بود. گفت: از اشارت تو گذر نیست، صورت مصلحت باز نمای تا مثال داده شود. گفت: تمامی بومان در فلان کوه‌اند و روزها درغاری جمله می‌شوند. و در آن نزدیکی هیزم بسیار است. ملک زاغان را بفرماید تا قدری ازان نقل کنند و بر در غاری بنهند. و برخت شبانان که در آن حوالی گوسپند می‌چرانند آتش باشد، من فروغی ازان بیارم و زیر هیزم نهم. ملک مثال دهد تا زاغان بحرکت پر آن را بچلانند. چون آتش بگرفت هر که از بومان بیرون آید بسوزد و هرکه در غار بماند از دود بمیرد. </p></div>
<div class="n" id="bn4"><p>بر این ترتیب که صواب دید پیش آن مهم باز رفتند، و تمامی بومان بدین حیلت بسوختند، و زاغان را فتح بزرگ برآمد و همه شادمان و دوستکام بازگشتند. و ملک و لشکر در ذکر مساعی حمید و مآثر مرضی آن زاغ غلو و مبالغت نمودند و اطناب و اسهاب واجب دیدند. و او ملک را دعاهای خوب گفت، د راثنای آن بر زبان راند که: هرچه از این نوع دست دهد بفر دولت ملک باشد. من مخایل آن روز دیدم که آن مدبران قصدی پیوستند و از آن جنس اقدامی جایز شمردند. </p></div>
<div class="b" id="bn5"><div class="m1"><p>کرد آن سپید کار بملک تو چشم سرخ </p></div>
<div class="m2"><p>تا زرد روی گشت و جهان شد برو سیاه</p></div></div>