---
title: >-
    بخش ۱۲
---
# بخش ۱۲

<div class="n" id="bn1"><p>آورده‌اند که روباهی در بیشه ای رفت آنجا طبلی دید پهلوی درختی افگنده و هرگاه که باد بجستی شاخ درخت بر طبل رسیدی، آوازی سهمناک بگوش روباه آمدی. چون روباه ضخامت جثه بدید و مهابت آواز بشنید طمع دربست که گوشت و پوست فراخور آواز باشد می‌کوشید تا آن را بدرید الحق چربوی بیشتر نیافت. مرکب زیان در جولان کشید و گفت: بدانستم که هرکجا جثه ضخیمتر و آواز آن هایل تر منفعت آن کمتر. </p></div>
<div class="n" id="bn2"><p>و این مثل بدان آوردم تا رای ملک را روشن شود که بدین آواز متقسم خاطر نمی باید شد. و اگر مرا مثال دهد بنزدیک او روم و بطان حال و حقیقت کار ملک را معلوم گردانم. </p></div>