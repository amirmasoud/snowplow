---
title: >-
    بخش ۴۳ - بازرگان آهن‌فروش
---
# بخش ۴۳ - بازرگان آهن‌فروش

<div class="n" id="bn1"><p>و مثل دوستان با تو چون مثل آن بازرگان است که گفته بود:زمینی که موش آن صد من آهن بخورد چه عجب اگر باز کودکی در قیاس ده من برباید؟ دمنه گفت:چگونه؟ گفت:آورده‌اند که بازرگانی اندک مال بود و می‌خواست که سفری رود. صد من آهن داشت، در خانه دوستی بر وجه امانت بنهاد و برفت. چون بازآمد امین، ودیعت فروخته بود و بها خرج کرده. بازرگان روزی بطلب آهن بنزدیک او رفت. مرد گفت: آهن در پیغوله خانه بنهاده بودم و دران احتیاطی نکرده، تا من واقف شدم موش آن را تمام خورده بود. بازرگان گفت: آری، موش آهن را نیک دوست داردو دندان او برخائیدن آن قادر باشد. امین راست کار شاد گشت، یعنی «بازرگان نرم شد و دل از آن برداشت. » گفت: امروز مهمان من باش. گفت: فردا باز آیم. </p></div>
<div class="n" id="bn2"><p>بیرون رفت و پسری را ازان او ببرد. چون بطلبیدند و ندا در شهر افتاد بازرگان گفت: من بازی را دیدم کودکی را می‌برد. امین فریاد برآورد که: محال چرا می‌گویی؟ باز کودک را چگونه برگیرد؟ بازرگان بخندید و گفت: دل تنگ چرا می‌کنی؟ در شهری که موش آن صد من آهن بتواند خورد آخر باز کودکی را هم برتواند داشت. امین دانست که حال چیست، گفت:آهن موش نخورد، من دارم، پسر بازده و آهن بستان.</p></div>
<div class="n" id="bn3"><p>و این مثل بدان آوردم تا بدانی که چون ملک این کردی دیگران را در تو امید وفاداری و طمع حق گزاری نماند. و هیچیز ضایع تر از دوستی کسی نیست که در میدان کرم پیاده و در لافگه وفا سرافگنده باشد، و همچنان نیکوی کردن بجای کسی که در مذهب خود اهمال حق و نسیان شکر حایز شمرد؛ و پند دادن آن را که نه در گوش گذارد و نه در دل جای دهد؛ و سر گفتن با کسی که غمازی سخره بیان و پیشه بنان او باشد. </p></div>