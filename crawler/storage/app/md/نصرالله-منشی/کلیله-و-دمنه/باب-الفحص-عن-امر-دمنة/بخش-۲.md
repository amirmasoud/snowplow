---
title: >-
    بخش ۲
---
# بخش ۲

<div class="n" id="bn1"><p>برهمن گفت:خون هرگز نخسبد، و بیدار کردن فتنه بهیچ تاویل مهنانماند، و در تواریخ و اخبار چنان خوانده ام که چون شیر از کارگاو بپرداخت از تعجیلی که دران کرده بود بسی پشیمانی خورد و سرانگشت ندامت خایید.</p></div>
<div class="b" id="bn2"><div class="m1"><p>نیک برنج اندرم از خویشتن </p></div>
<div class="m2"><p>گم شده تدبیر و خطا کرده ظن</p></div></div>
<div class="n" id="bn3"><p>و بهروقت حقوق متاکد و سوالف مرضی او را یاد می‌کرد و فکرت و ضجرت زیادت استیلا و قوت می‌یافت، که گرامی تر اصحاب و عزیزتر اتباع او بود، و پیوسته می‌خواست که حدیث او گوید و ذکر او شنود. و با هریک از وحوش خلوتها کردی و حکایتها خواستی. شبی پلنگ تا بیگاهی پیش او بود، چون بازگشت برمسکن کلیله و دمنه گذرش افتاد. کلیله روی بدمنه آورده بود و آنچه از جهت او در حق گاو رفت باز می‌راند. پلنگ بیستاد و گوش داشت. سخن کلیله آنجا رسیده بود که: هول ارتکابی کردی، و این غدر و غمزرا مدخلی نیک باریک جستی، و ملک را خیانت عظیم روا داشتی. و ایمن نتوان بود که ساعت بساعت بوبال آن ماخوذ شوی و تبعت آن بتو رسد و هیچکس از و حوش ترا دران معذور ندارد، و در تخلص تو ازان معونت و مظاهرت روانبیند، و همه برکشتن و مثله کردن تو یک کلمه شوند. و مرا بهمسایگی تو حاجت نیست از من دورباش و مواصلت و ملاطفت در توقف دار. دمنه گفت که:</p></div>
<div class="b" id="bn4"><div class="m1"><p>گر بر کنم دل از تو و بردارم از تو مهر</p></div>
<div class="m2"><p>آن مهر برکه افگنم آن دل کجا برم؟</p></div></div>
<div class="n" id="bn5"><p>نیز کار گذشته تدبیر را نشاید، خیالات فاسد از دل بیرون کن و دست از نیک و بد بدار و روی بشادمانگی و فراغت آر، که دشمن برافتاد و جهان مراد خالی و هوای آرزو صافی گشت.</p></div>
<div class="b" id="bn6"><div class="m1"><p>سرفراز و بفرخی بگراز</p></div>
<div class="m2"><p>لهو جوی و بخرمی می‌خور</p></div></div>
<div class="n" id="bn7"><p>و ناخوبی موقع آن سعی در مروت و دیانت بر من پوشیده نبد، و استیلای حرص و حسد مرا بران محرض آمد. </p></div>