---
title: >-
    بخش ۱ - بازجست کار دمنه
---
# بخش ۱ - بازجست کار دمنه

<div class="n" id="bn1"><p>رای گفت برهمن را: معلوم گشت داستان ساعی نمام که چگونه جمال یقین را بخیال شبهت بپوشانید تا مروت شیر مجروح شد و سمت نقض عهد بدان پیوست و دشمنایگی در موضع دوستی و وحشت بجای الفت قرار گرفت و دستور ملک و گنجور او در سر آن شد. </p></div>
<div class="n" id="bn2"><p>اکنون اگر بیند عاقبت کار دمنه و کیفیت معذرتهای او پیش شیر و وحوش بیان کند، که شیر در آن حادثه چون بعقل خود رجوع کرد و در دمنه بدگمان گشت تدارک آن ا زچه نوع فرمود، و بر غدر او چگونه وقوف یافت، و دمنه بچه حجت تمسک نمود،و تخلص از چه جنس طلبید، و از کدام طریق گرد جستن پوزش آن درآمد. </p></div>