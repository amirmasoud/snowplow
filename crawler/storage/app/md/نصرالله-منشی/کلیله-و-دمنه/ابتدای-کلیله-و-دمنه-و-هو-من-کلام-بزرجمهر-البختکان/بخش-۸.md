---
title: >-
    بخش ۸
---
# بخش ۸

<div class="n" id="bn1"><p>و نیکوتر آنکه سیرتهای گذشتگان را امام ساخته شود و تجارب متقدمان را نمودار عادات خویش گردانیده آید. که اگر در هرباب ممارست خویش را معتبر دارد عمر در محنت گزارد. با آنچه گویند «در هر زیانی زیرکیی است » لکن از وجه قیاس آن موافق تر که زیان دیگران دیده باشد و سود از تجارب ایشان برداشته شود، چه اگر از این طریق عدول افتد هر روز مکروهی باید دید، و چون تجارب اتقیانی حاصل آمد هنگام رحلت باشد. </p></div>