---
title: >-
    بخش ۲۱ - ذکر منصور عباسی
---
# بخش ۲۱ - ذکر منصور عباسی

<div class="n" id="bn1"><p>روزی او را گفتند: فلان مقدم فرمان یافت و از او ضیاع بسیار مانده است و فرزندان او بدرجه استقلال نرسیده اند، اگر مثال باشد تا عمال بعضی در تصرف گیرند و در قبض آرند دیوان را توفیری تمام باشد. جواب داد که: هرکرا خلافت روی زمین سیر نگرداند از ضیاع یتیمان هم سیر نگردد. </p></div>