---
title: >-
    شمارهٔ  ۱۱۰ - )
---
# شمارهٔ  ۱۱۰ - )

<div class="b" id="bn1"><div class="m1"><p>شد زدار محن به دار نعیم</p></div>
<div class="m2"><p>رادمردی بزرگوار و حکیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فخر دوران طبیب پاک سرشت</p></div>
<div class="m2"><p>که دمش بد شفای جان سقیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مَلک الطّب سلیل نادر شاه</p></div>
<div class="m2"><p>شاهزاده محمد ابراهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن که او مستشار اطبّا را</p></div>
<div class="m2"><p>بود از حسن رأی و ذوق سلیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سپنجی سرا و دار بقا</p></div>
<div class="m2"><p>بود پنجه و هشت سال مقیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دو سال هزار و سیصد و شش</p></div>
<div class="m2"><p>در محرم نمود جان تسلیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیمه عشر سومین از ماه</p></div>
<div class="m2"><p>شد ز دار محن به دار نعیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون به خاکش گذر نمود محیط</p></div>
<div class="m2"><p>بهر تاریخ آن یگانه حکیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست پای ادب به پیش و سرود</p></div>
<div class="m2"><p>به جان باد مستشار مقیم</p></div></div>