---
title: >-
    شمارهٔ  ۱۲۴ - و له ایضاً علیه الرحمة و الغفران
---
# شمارهٔ  ۱۲۴ - و له ایضاً علیه الرحمة و الغفران

<div class="b" id="bn1"><div class="m1"><p>در سفر گویند فردا می‌روی</p></div>
<div class="m2"><p>خود دروغ است این خبر یا می‌روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای شکارافکن تو هرجا می‌روی</p></div>
<div class="m2"><p>از برای صید دل‌ها می‌روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکجا تو سرو بالا می‌روی</p></div>
<div class="m2"><p>فتنه برپا می‌کنی تا می‌روی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زنی راه غزلان از نگاه</p></div>
<div class="m2"><p>ای غزال من، به صحرا می‌روی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نهان یک شهر دل همراه تو است</p></div>
<div class="m2"><p>گرچه در ظاهر تو تنها می‌روی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهزنان از خلق پنهان می‌روند</p></div>
<div class="m2"><p>تو قوی‌دل آشکارا می‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نشینی می‌نشانی فتنه را</p></div>
<div class="m2"><p>فتنه برپا می‌کنی تا می‌روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرکجا بوده دلی، بردی کنون</p></div>
<div class="m2"><p>از پی تاراج جان‌ها می‌روی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با کمند زلف و تیغ ابروان</p></div>
<div class="m2"><p>بهر خونریزی دل‌ها می‌روی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا نشانی آتش دل یک زمان</p></div>
<div class="m2"><p>می‌نشینی در برم یا می‌روی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهتر است از هر تماشا روی تو</p></div>
<div class="m2"><p>تو کجا بهر تماشا می‌روی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دانی از هردو جهان بهتر کجاست</p></div>
<div class="m2"><p>هر کجا تو، سرو بالا می‌روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا رواج مذهب ترسا دهی</p></div>
<div class="m2"><p>کرده گیسو را چلیپا می‌روی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌شود از رفتنت غوغا به پا</p></div>
<div class="m2"><p>تا کنی برپای غوغا، می‌روی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>می‌شود خرم بهشت آنجا که تو</p></div>
<div class="m2"><p>با قد دلکش چو طوبی می‌روی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می‌کنی یغما دل ترکان تمام</p></div>
<div class="m2"><p>ترک من، هرگه به یغما می‌روی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر نهی بر چشم مه‌رویان قدم</p></div>
<div class="m2"><p>جای دارد، بس که زیبا می‌روی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جان من تو با کمند گیسوان</p></div>
<div class="m2"><p>از برای صید دل‌ها می‌روی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ ابرویت اشارت می‌کند</p></div>
<div class="m2"><p>کز پی قتل احبا می‌روی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آگهی کز رفتنت جان می‌دهم</p></div>
<div class="m2"><p>بهر قتل ما، به عمدا می‌روی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روز و شب ای دل پی آن زلف و لب</p></div>
<div class="m2"><p>با سری پرشور و سودا می‌روی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چون توانی رفت بر چشم و سرم</p></div>
<div class="m2"><p>از چه رو بر خاک و خارا می‌روی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>راستی رفتار تو این گونه است</p></div>
<div class="m2"><p>یا چنین ای سرو بالا می‌روی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا کشی بیمار هجران را، ز شوق</p></div>
<div class="m2"><p>بر سرش گاهی به عمدا می‌روی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جان درویشان فدایت باد چون</p></div>
<div class="m2"><p>بهر طوف کوی بر عرش اعلا می‌روی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاه درویشان که بر درگاه او</p></div>
<div class="m2"><p>چون روی بر عرش اعلا می‌روی</p></div></div>