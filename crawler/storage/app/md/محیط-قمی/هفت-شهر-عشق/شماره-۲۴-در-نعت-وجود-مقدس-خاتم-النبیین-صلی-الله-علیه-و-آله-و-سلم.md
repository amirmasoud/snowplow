---
title: >-
    شمارهٔ  ۲۴ - در نعت وجود مقدس خاتم النبیّین صلی الله علیه و آله و سلم
---
# شمارهٔ  ۲۴ - در نعت وجود مقدس خاتم النبیّین صلی الله علیه و آله و سلم

<div class="b" id="bn1"><div class="m1"><p>اول صبح دوم، آخر دوران شب است</p></div>
<div class="m2"><p>هله عید آمد و روزی خوش و وقتی عجب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که را می نگری سرخوش صهبای سرور</p></div>
<div class="m2"><p>هر کجا می گذری بزم نشاط و طرب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آید از هر شجری صوت انا الله در گوش</p></div>
<div class="m2"><p>چشم دل بازنما گاه تجلی رب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه سکّان سماوات هم آواز شده</p></div>
<div class="m2"><p>زهق الباطل و جاء الحقشان ورد لب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشت مولود چنین روز مهین فرزندی</p></div>
<div class="m2"><p>که وجودش سبب خلقت هر أمّ و أب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قائل کُنتُ نبیاً که مبارک ذاتش</p></div>
<div class="m2"><p>خلقت آدم و ذریه ی او را سبب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید خیل رسل ختم نبیین احمد</p></div>
<div class="m2"><p>شه لولاک محمد که امینش لقب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حجة الله علی الخلق نبی الرحمه</p></div>
<div class="m2"><p>مصطفی کز همه ی کون و مکان منتخب است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیبة الحمد بُدش جدّ و پدر عبدالله</p></div>
<div class="m2"><p>مادرش آمنه و آمنه بنت وهب است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هفدهم روز مبارک ز ربیع الاول</p></div>
<div class="m2"><p>مولدش مکه در آنجای که نامش شعب است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تافت هنگام ولادت ز جبینش نوری</p></div>
<div class="m2"><p>که فروغ مه و خورشید از آن مُکتسب است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از حرم یک سره با پای خود اصنام شدند</p></div>
<div class="m2"><p>گه میلادش، این واقعه بس بوالعجب است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هجرتش سیزدهم سال ز بعثت بشمار</p></div>
<div class="m2"><p>بعثتش هفتم عشرین ز ماه رجب است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>معجز ثابته ی باقیه ی او، قرآن</p></div>
<div class="m2"><p>آن کلام الله مُنزل، به لسان عرب است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زو عجب نبود، شق القمر و ردّ الشمس</p></div>
<div class="m2"><p>کآسمان هاش چوگویی، به کف از لطف رب است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شب معراجش سابع عشر شهر صیام</p></div>
<div class="m2"><p>بلکه همواره به هر سال و مه و روز و شب است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دین او ناسخ ادیان تمامی رسل</p></div>
<div class="m2"><p>ذات او از همه برتر به عُلو و حسب است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>منظر تابع او روضه ی فردوس برین</p></div>
<div class="m2"><p>منزل عاصی او ناراً ذات لهب است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نتوان گفت قدیمش که قدیم ازلی</p></div>
<div class="m2"><p>ذات بی چون خداوند بری از نسب است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>حادثش هم نتوان خواند که حادث خواندن</p></div>
<div class="m2"><p>ایزدی ذات ورا دور ز رسم ادب است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فیض پاینده ی حق باشد و حادث خواندن</p></div>
<div class="m2"><p>فیض پاینده ی حق را نه طریق ادب است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صِهر و ابن عم و اول وصی او است علی</p></div>
<div class="m2"><p>کز نهیبش به تن و جان عدو تاب و تب است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به نبی و علی و آل تولا کردن</p></div>
<div class="m2"><p>مایه ی أمن و امان دافع رنج و تعب است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شرف از مدحتشان یافته تا نظم «محیط»</p></div>
<div class="m2"><p>قدسیان را زپی کسب شرف ورد لب است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>باد بر آن نبی رحمت و آلش صلوات</p></div>
<div class="m2"><p>تا بود نخل ثمرآور و بارش رطب است</p></div></div>