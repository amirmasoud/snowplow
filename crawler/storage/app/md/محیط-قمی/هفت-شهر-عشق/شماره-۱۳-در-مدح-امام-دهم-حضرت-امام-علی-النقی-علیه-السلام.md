---
title: >-
    شمارهٔ  ۱۳ - در مدح امام دهم حضرت امام علی النّقیّ علیه السلام
---
# شمارهٔ  ۱۳ - در مدح امام دهم حضرت امام علی النّقیّ علیه السلام

<div class="b" id="bn1"><div class="m1"><p>سر رفت و دل هوی تو بیرون زسر نکرد</p></div>
<div class="m2"><p>ترک طلب نگفت و خیال دگر نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم سپید شد به ره انتظار و باز</p></div>
<div class="m2"><p>از گرد رهگذار تو قطع نظر نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گذشت عمر و سروقد من دمی زمهر</p></div>
<div class="m2"><p>بر جویبار دیده ی گریان گذر نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع وجود بی مه رویش نداد نور</p></div>
<div class="m2"><p>نخل حیات بی قد سروش ثَمر نکرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سنگ خاره ناله ی من رخنه کرد لیک</p></div>
<div class="m2"><p>سختی نگر که در دل جانان اثر نکرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بد عهدی زمانه نظر کن که آسمان</p></div>
<div class="m2"><p>گردش دمی به خواهش اهل هنر نکرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل در جهان مبند که این تندخو حریف</p></div>
<div class="m2"><p>با هیچ کس شبی به محبت سحر نکرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معمار روزگار کدامین بنا نهاد</p></div>
<div class="m2"><p>کز تندباد حادثه زیر و زبر نکرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دنیا متاع مختصری غم فزا بود</p></div>
<div class="m2"><p>خرم کسی که میل بدین مختصر نکرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فرخنده بخت آن که در این عاریت سرا</p></div>
<div class="m2"><p>جز کسب نیک نامی، کار دگر نکرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل با ولای حجّت یزدان دَهُم امام</p></div>
<div class="m2"><p>از کید نه سپهر مخالف حذر نکرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلطان دین علی نقی آن که آسمان</p></div>
<div class="m2"><p>سر پیش آستانش، از شرم بر نکرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترک مراد خاطر او را قضا نگفت</p></div>
<div class="m2"><p>اندیشه ی خلاف رضایش قدر نکرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خورشید آسمان ولایت که ذره ای</p></div>
<div class="m2"><p>بی مهر او به عالم امکان گذر نکرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>انوار فیض عامش بر ذره نتافت</p></div>
<div class="m2"><p>کان ذرّه جلوه ها بر شمس و قمر نکرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طبع «محیط» غیرت دریا است نظم او</p></div>
<div class="m2"><p>هر کس شنید فرق زعقد گهر نکرد</p></div></div>