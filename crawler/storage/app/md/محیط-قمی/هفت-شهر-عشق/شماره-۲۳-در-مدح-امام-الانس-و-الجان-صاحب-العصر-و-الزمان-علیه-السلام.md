---
title: >-
    شمارهٔ  ۲۳ - در مدح امام الانس و الجّان صاحب العصر و الزمان علیه السلام
---
# شمارهٔ  ۲۳ - در مدح امام الانس و الجّان صاحب العصر و الزمان علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای پایه ی جلال تو آن سوتر از جهات</p></div>
<div class="m2"><p>برتر بود مقام تو زادراک ممکنات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی رسیده ای ز جلالت که آمده</p></div>
<div class="m2"><p>در عالم تصور ذاتت، عقول، مات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرخ سلیل حجت حق، عسکری تویی</p></div>
<div class="m2"><p>جدّ تو هست ختم رسل، فخر کائنات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هستی تو نخبه ی عرب و زبده ی عجم</p></div>
<div class="m2"><p>بابت ز طیّبین بُد و مامت، ز طیّبات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای برترین سلاله ی ارواح محترم</p></div>
<div class="m2"><p>ای بهترین نتیجه ی آباء و اُمّهات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامان عصمت تو مبرّا بود ز عیب</p></div>
<div class="m2"><p>زاصلاب شامخاتی و ارحام طاهرات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرخنده درگه تو بود ساحل امید</p></div>
<div class="m2"><p>در بحر روزگار، تویی کشتی نجات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگشوده فیض عام تو بر چهر ماسوی</p></div>
<div class="m2"><p>ابواب جاودانه عطیات وافرات</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر زهر، بَر دمند اگر نام مهر تو</p></div>
<div class="m2"><p>مانند آب خضر شود مایه ی حیات</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوانند گر، به آب بقا وصف قهر تو</p></div>
<div class="m2"><p>چون زهر جانگزای شود مورث ممات</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می خواست مهر و قهر ترا حق نشانه ای</p></div>
<div class="m2"><p>ایجاد کرد دوزخ و جنات عالیات</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در رتبه ماسوی همه جسمند و تو روان</p></div>
<div class="m2"><p>نی نی خطا سرودم، اینان صفت تو ذات</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باشد گه تجلی یزدان ظهور تو</p></div>
<div class="m2"><p>حق راست در وجود تو هر دم تجلّیات</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نبود ولی قائم بالسیف، جز تو کس</p></div>
<div class="m2"><p>هم صاحب الزمانی و هم مالک الجهات</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هستی امام غایب و مهدی منتظر</p></div>
<div class="m2"><p>ای مظهر غرائت آثار و معجزات</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از پا فتاده ام ز کرم دست من بگیر</p></div>
<div class="m2"><p>یا ناصر الأحبة و یا حامی الولات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبود مرا ز کار فرو بسته غم که هست</p></div>
<div class="m2"><p>دست گره گشای تو حلال مشکلات</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دستی بر آر و ریشه ی کافر دلان بکن</p></div>
<div class="m2"><p>یا قاهر الاعادی و یا قامع الطُّغات</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا خاتم الائمه و یا هادی الأمم</p></div>
<div class="m2"><p>با مبدأ الهدایة و یا منتهی الهُدات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در حضرت تو حاجت اظهار حال نیست</p></div>
<div class="m2"><p>باشد چه احتیاج به توضیح واضحات</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دارد زلطف عام تو چشم کرم «محیط»</p></div>
<div class="m2"><p>ای ریزه خوار خوان عطای تو کاینات</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گوید بدین امید ثنایت که روز حشر</p></div>
<div class="m2"><p>انعام او، به روضه ی رضوان کنی برات</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در خورد حضرت تو نباشد ثنای من</p></div>
<div class="m2"><p>ای عاجز از بیان ثنای تو ممکنات</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر دم ترا نثار فرستند قدسیان</p></div>
<div class="m2"><p>از بارگاه قُدس تحیّات زاکیات</p></div></div>