---
title: >-
    شمارهٔ  ۱۲۵ - منه علیه الرحمة و الغفران
---
# شمارهٔ  ۱۲۵ - منه علیه الرحمة و الغفران

<div class="b" id="bn1"><div class="m1"><p>دام ره خلقی شده، بندی که تو داری</p></div>
<div class="m2"><p>افتاده همه کس، به کمندی که تو داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای زلف گره گیر، رهایی زتو ما را</p></div>
<div class="m2"><p>مشکل شده از حلقه و بندی که تو داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بالای خوش سرو، بر افروخته قامت</p></div>
<div class="m2"><p>پست است بر قد بلندی که تو داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوشین دهنا، با همه شیرینی شکر</p></div>
<div class="m2"><p>تلخ است بر لعل چو قندی که تو داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجمر صفت افروخته دارم همه ی عمر</p></div>
<div class="m2"><p>دل از پی خال چو سپندی که تو داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز نبود طبع تو را میل به یاران</p></div>
<div class="m2"><p>آه از دل اغیار پسندی که تو داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آزاده دل آنان که اسیرند، همه عمر</p></div>
<div class="m2"><p>در بند دل آویز کمندی که تو داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عشق جانسوز، زآسایش و راحت</p></div>
<div class="m2"><p>خوشتر بود آسیب و گزندی که تو داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دلبستگیت با قد جانانه «محیطا»</p></div>
<div class="m2"><p>پیداست ازین طبع بلندی که تو داری</p></div></div>