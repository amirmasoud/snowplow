---
title: >-
    شمارهٔ  ۱۱۵ - در تهنیت قرن شهید مبرور ناصرالدّین شاه طاب ثراه
---
# شمارهٔ  ۱۱۵ - در تهنیت قرن شهید مبرور ناصرالدّین شاه طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>به ماند بر سریر کامرانی</p></div>
<div class="m2"><p>شهنشاه مظفر جاودانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدیو دادگستر ناصرالدین</p></div>
<div class="m2"><p>شرف افزای دیهیم کیانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانداری که زد بر خوان احسان</p></div>
<div class="m2"><p>جهانی را صلای میهمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نام وی به فیروزی و اقبال</p></div>
<div class="m2"><p>برآمد سکه ی صاحب قرانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زآغاز جهانبانی قاجار</p></div>
<div class="m2"><p>چو یک صد سال شد با کامرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فرخ روزگارش تازه گردید</p></div>
<div class="m2"><p>جهان پیر را عهد جوانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اساس ظلم و کین را داد بر باد</p></div>
<div class="m2"><p>بنای معدلت را گشت بانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کف بخشنده اش ابر است گر ابر</p></div>
<div class="m2"><p>نماید جاودان گوهر فشانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همایون رأی شه را خواندمی مهر</p></div>
<div class="m2"><p>نبودی گر فروغ مهر فانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پی کسب شرف بر آستانش</p></div>
<div class="m2"><p>کند بهرام هرشب پاسبانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین شاهنشه عالی همم را</p></div>
<div class="m2"><p>نشاید خواند ذوالقرنین ثانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سکندر گر بدی در روزگارش</p></div>
<div class="m2"><p>ز وی آموختی کشور ستانی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بود در فیض بخشی خاک راهش</p></div>
<div class="m2"><p>بسی خوشتر زآب زندگانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ملک ظلّ خداوند است و وصفش</p></div>
<div class="m2"><p>بود بیرون زحدّ نکته دانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>توانی گر سپس ذات بی چون</p></div>
<div class="m2"><p>ثنای داور دوران توانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>جهان تا پایدی پیوسته بادا</p></div>
<div class="m2"><p>به کام شاه، دور آسمانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نهال دولت فرخنده ی وی</p></div>
<div class="m2"><p>نگردد رنجه از باد خزانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قرون بی شُمَر بر تخت شاهی</p></div>
<div class="m2"><p>به پاید با نشاط و شادمانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به جو تاریخ قرن ناصری را</p></div>
<div class="m2"><p>«محیطا» بیت مقطع را چو خوانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به نام شاه عادل آمده نیک</p></div>
<div class="m2"><p>همایون سکه ی صاحب قرانی</p></div></div>