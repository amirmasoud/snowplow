---
title: >-
    شمارهٔ  ۱۲۶ - و له ایضاً فی التّشبیب و التّحبیب
---
# شمارهٔ  ۱۲۶ - و له ایضاً فی التّشبیب و التّحبیب

<div class="b" id="bn1"><div class="m1"><p>جم رفت و نماند از وی، بر جای به جز جامی</p></div>
<div class="m2"><p>می ده که جهان را نیست، جز نیستی انجامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رند و خراباتی، گشتم مکنیدم عیب</p></div>
<div class="m2"><p>کز زهد فروشی هیچ، حاصل نشدم کامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بد نام تری از من، در حلقه ی رندان نیست</p></div>
<div class="m2"><p>هر لحظه بود دلقم، جایی گرو جامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوریده دلی دارم وز هر طرفی شوخی</p></div>
<div class="m2"><p>گسترده پی صیدش از زلف سیه دامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نفکند از بام، طشت من سودایی</p></div>
<div class="m2"><p>هر دم فکند عشقش، طشتی زلب بامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیوسته نمایندم، خوبان ز رخ و گیسو</p></div>
<div class="m2"><p>شامی زپی صبحی، صبحی ز پی شامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رو کسب قناعت کن تا با زرهی ای دل</p></div>
<div class="m2"><p>از منِّت هر خاصی، از طعنه ی هر عامی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جز احمد و ال او، ما را به دو عالم نیست</p></div>
<div class="m2"><p>از کس طمع لطفی یا دیده ی انعامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مانند «محیط» امروز در شهر نمی باشد</p></div>
<div class="m2"><p>قلّاش نظر بازی، دُردی کش بد نامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بی ساقی آتش دست، بی باده ی آتش وش</p></div>
<div class="m2"><p>کی رام شود شوخی، کی پخته شود خامی</p></div></div>