---
title: >-
    شمارهٔ  ۱۲ - در مدح بابُ المراد حضرت امام محمد تقی جواد علیه السلام
---
# شمارهٔ  ۱۲ - در مدح بابُ المراد حضرت امام محمد تقی جواد علیه السلام

<div class="b" id="bn1"><div class="m1"><p>کجا است زنده دلی کاملی مسیح دمی</p></div>
<div class="m2"><p>که فیض صحبتش از دل برد غبار غمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلیل بت شکنی کو که نفس دون شکند</p></div>
<div class="m2"><p>که نیست در حرم دل به غیر او صنمی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کید چرخ در آن دور گشت نوبت ما</p></div>
<div class="m2"><p>که نیست ساقی ایام را سر کرمی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانه خرمن دانش نمی خرد به جوی</p></div>
<div class="m2"><p>بهای گنج هنر را نمی دهد درمی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباد آن که شود سفله خوی کامروا</p></div>
<div class="m2"><p>که هر زمان کند آغاز فتنه و ستمی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذشت عمر و دریغا نداد ما را دست</p></div>
<div class="m2"><p>حضور نیم شبی و صفای صبح دمی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قسم به جان عزیزان به وصل دوست رسی</p></div>
<div class="m2"><p>اگر از این تن خاکی برون نهی قدمی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خلاف گوشه نشینان دل شکسته مجو</p></div>
<div class="m2"><p>که نیست جز دل این قوم دوست را حرمی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غم زمانه مخور ای رفیق باده بنوش</p></div>
<div class="m2"><p>که دور چرخ به جامی گذاشته، نه جمی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بینوایی و دولت غمین و شاد مباش</p></div>
<div class="m2"><p>که در زمانه نماند گدا و محتشمی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز اشتیاق بلند آستان شه هر شب</p></div>
<div class="m2"><p>فراز عرش فرازم زآه خود عَلَمی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خَلق آن چه رسد فیض زآشکار و نهان</p></div>
<div class="m2"><p>ز بحر جود شه دین جواد هست نَمی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محمد بن علی تاسع الائمه تقی</p></div>
<div class="m2"><p>که بحر همت او است بی کرانه یَمی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان خدای که باشد زکلک قدرت او</p></div>
<div class="m2"><p>نقوش دفتر هستی ماسِوی رَقمی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که با ولای شفیعان حشر احمد و آل</p></div>
<div class="m2"><p>«محیط» را نبود از گناه خویش غمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شهان کشور نظمیم ما ثناگویان</p></div>
<div class="m2"><p>اساس سلطنت ما است دفتر و قلمی</p></div></div>