---
title: >-
    شمارهٔ  ۱۳۳ - قطعه
---
# شمارهٔ  ۱۳۳ - قطعه

<div class="b" id="bn1"><div class="m1"><p>ای وفا ای که در وفاداری</p></div>
<div class="m2"><p>صفت روزگار را داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل نمایی عبث مکن بر خلق</p></div>
<div class="m2"><p>که همان طبع خار را داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهره ی دوستی به کس مفروش</p></div>
<div class="m2"><p>که همان نیش مار را داری</p></div></div>