---
title: >-
    شمارهٔ  ۱۲۳ - در منقبت شاه ولایت حضرت علی بن ابیطالب علیه السلام
---
# شمارهٔ  ۱۲۳ - در منقبت شاه ولایت حضرت علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>خوشا دمی که لبم را به لب چو جام نهی</p></div>
<div class="m2"><p>لبت به بوسم و قالب کنم زشوق تهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخ نکوی تو دیدن بود صباح الخیر</p></div>
<div class="m2"><p>از آن که خرم و خندان چو گل به صبح گهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن زمان که عیان شد، چه زنخدانت</p></div>
<div class="m2"><p>کبوتر دل یوسف زجان شده است چهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زیب قامت و حسن جمال سروی و ماه</p></div>
<div class="m2"><p>ولیک سرو قباپوش و ماه کج کلهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود زوصف بنا گوش تو گشوده دلم</p></div>
<div class="m2"><p>چنان که غنچه گل از نسیم صبح گهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن زمان که شدم با خبر ز رحمت دوست</p></div>
<div class="m2"><p>نه از گناه که شرمنده ام زکم گنهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان بگشتم و دیدم تمام خوبان را</p></div>
<div class="m2"><p>جهان فدای تو بادا که از تمام بهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو زکثرت عصیان زلطف حق نومید</p></div>
<div class="m2"><p>که لطف صرصر و تو با گنه چو پرکهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به شست و شوی نگردد سفید جامه ی بخت</p></div>
<div class="m2"><p>که را که رفته قلم در حقش به رو سیهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>میانه ی تو و جانان حجاب هستی تو است</p></div>
<div class="m2"><p>به وصل می نرسی تا زخویشتن نرهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ترا نموده علایق اسیر دام بلا</p></div>
<div class="m2"><p>به کوش تا به تجرد، زدام او برهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خطا است رفتن نزد کریم چون بازاد</p></div>
<div class="m2"><p>بر آستان تو وارد شدیم دست تهی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خطا سرودم، آورده ام مدیح شهی</p></div>
<div class="m2"><p>که از تمام جهانش فزونی است و بهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قسیم دوزخ و جنت، علی که با حبش</p></div>
<div class="m2"><p>زیان نمی رسدت گرچه غرفه ی گنهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شها مدیح تو گویم برای آن که به حشر</p></div>
<div class="m2"><p>مرا زورطه ی اندوه و غم، نجات دهی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خاک پای تو سوگند و آسمان بلند</p></div>
<div class="m2"><p>که با غلامی تو عار آیدم زشهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گزافه گفتم و من در خور غلامی تو</p></div>
<div class="m2"><p>نیم غلام و سگت را سگم زجان و رهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>«محیط» را به حمایت زبیم ایمن کن</p></div>
<div class="m2"><p>به راه پر خطر آخرت شود چو رهی</p></div></div>