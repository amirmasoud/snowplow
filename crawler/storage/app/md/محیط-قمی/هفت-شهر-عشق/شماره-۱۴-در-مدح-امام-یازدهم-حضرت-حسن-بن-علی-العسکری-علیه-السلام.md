---
title: >-
    شمارهٔ  ۱۴ - در مدح امام یازدهم حضرت حسن بن علی العسکری علیه السلام
---
# شمارهٔ  ۱۴ - در مدح امام یازدهم حضرت حسن بن علی العسکری علیه السلام

<div class="b" id="bn1"><div class="m1"><p>دلم که بود زآلایش طبیعت پاک</p></div>
<div class="m2"><p>گرفته گرد کدورت از این نشیمن خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مَلول گشتم از این همرهان سست عنان</p></div>
<div class="m2"><p>کجا است راه نوردی مُجرّدی چالاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از این تن خاکی سفر کنی ای دل</p></div>
<div class="m2"><p>نخست گام نهی پای بر سر افلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلند و پست جهان ای رفیق بسیار است</p></div>
<div class="m2"><p>گهی به چرخ برد که گذارت برخاک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نیستی است سرانجام، هرچه پیش آید</p></div>
<div class="m2"><p>به هر طریق که باشی مدار دل غمناک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یاوه ابر بهاری به دجله می بارد</p></div>
<div class="m2"><p>به راه بادیه لب تشنگان شدند هلاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رواق صومعه را آن زمان شکست آمد</p></div>
<div class="m2"><p>که سرکشید به اوج سپهر طارم تاک</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نکو است هرچه کند دِلستان چه جور و چه مهر</p></div>
<div class="m2"><p>خوش است آن چه دهد او، چه زهر و چه تریاک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کیش اهل کرم کافری، اگر ای دل</p></div>
<div class="m2"><p>کنی به راه عزیزان زبذل جان امساک</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرم رسد به گریبان جامه ی جان دست</p></div>
<div class="m2"><p>کنم به روز فراق تو تا به دامان چاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من و خیال خلاف رضای تو، هیهات!</p></div>
<div class="m2"><p>تو و هوای حصول مراد من؟ حاشاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز یُمن دوستی بندگان خسرو دین</p></div>
<div class="m2"><p>ز دشمنی زمانه، مرا نباشد باک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ولیّ حق حسن بن علی، شه کونین</p></div>
<div class="m2"><p>امام یازدهم، سبط خواجه ی لولاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بزرگ آیت یزدان که درک ذاتش را</p></div>
<div class="m2"><p>توان نمودن گر ذات حق شود ادراک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خدیو کون و مکان، شهسوار مسک وجود</p></div>
<div class="m2"><p>که بسته سلسله ی کائنات بر فتراک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شها وجود دو عالم طُفیل هستی تو است</p></div>
<div class="m2"><p>تو اصل فیضی و ارواح عالمین، فداک</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به لطف عام تو دارد «محیط» چشم امید</p></div>
<div class="m2"><p>در آن زمان که سپارد طریق تیره مغاک</p></div></div>