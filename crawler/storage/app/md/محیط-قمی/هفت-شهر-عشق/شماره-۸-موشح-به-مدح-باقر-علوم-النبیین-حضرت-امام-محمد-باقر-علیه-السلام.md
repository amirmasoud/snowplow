---
title: >-
    شمارهٔ  ۸ - موشّح به مدح باقر علوم النبیّین حضرت امام محمد باقر علیه السلام
---
# شمارهٔ  ۸ - موشّح به مدح باقر علوم النبیّین حضرت امام محمد باقر علیه السلام

<div class="b" id="bn1"><div class="m1"><p>به بزم قرب حجابی به غیر دوست ندیدم</p></div>
<div class="m2"><p>چو زین حجاب گذشتم به وصل دوست رسیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لوح دل چو زدودم غبار ظلمت کثرت</p></div>
<div class="m2"><p>جمال شاهد وحدت به چشم خویش بدیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کام خود بگذشتم به خواهش دل جانان</p></div>
<div class="m2"><p>رضای خاطر او بر مراد خویش گزیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز غیر دوست رمیدن بود طریقه ی عاشق</p></div>
<div class="m2"><p>عجب مدار اگر من زخویشتن برمیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دور نرگس مخمور لعل باده پرستت</p></div>
<div class="m2"><p>وداع عقل به گفتم طمع زهوش بریدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به یمن همت عشقت چه قیدها بشکستیم</p></div>
<div class="m2"><p>ز شور جذبه ی شوقت چه خرقه ها که دریدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ترا به خلوت دل بوده منزل و من غافل</p></div>
<div class="m2"><p>به هرزه در طلبت گرد هر دیار دویدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به عالمی نفروشم غم محبت جانان</p></div>
<div class="m2"><p>که این متاع گرامی به نقد عمر خریدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسان نرگس شوخت مدام سرخوش و مستم</p></div>
<div class="m2"><p>از آن زمان که زلعلت شراب شوق چشیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمان عشق که گردن کشیدنش نتواند</p></div>
<div class="m2"><p>به یک اشاره ی ابروی دلکش تو کشیدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جز محمد و آلش زکس امید ندارم</p></div>
<div class="m2"><p>به کامرانی جاوید زین امید رسیدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خاک درگه پنجم امام رفت حدیثی</p></div>
<div class="m2"><p>شمیم روضه ی رضوان زشش جهت بشنیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>محمّد بن علی باقرالعلوم شه دین</p></div>
<div class="m2"><p>که داغ بندگیش از ازل به جبهه کشیدم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به عرض حال چه حاجت بود که هست یقینم</p></div>
<div class="m2"><p>که واقف است به درد نهان و رنج پدیدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«محیط» در حق من ظنّ بد مبر به خرابی</p></div>
<div class="m2"><p>که مست باده ی عشق شهم نه مست نبیدم</p></div></div>