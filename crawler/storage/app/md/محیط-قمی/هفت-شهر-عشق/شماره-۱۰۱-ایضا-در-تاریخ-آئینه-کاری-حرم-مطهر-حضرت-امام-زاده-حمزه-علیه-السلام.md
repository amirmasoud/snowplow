---
title: >-
    شمارهٔ  ۱۰۱ - ایضاً در تاریخ آئینه کاری حرم مطهر حضرت امام زاده حمزه علیه السلام
---
# شمارهٔ  ۱۰۱ - ایضاً در تاریخ آئینه کاری حرم مطهر حضرت امام زاده حمزه علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ایزد بخشنده در پناه شهنشاه</p></div>
<div class="m2"><p>میر زمن را دهاد عمر مؤبد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت به دوران شهریار مؤید</p></div>
<div class="m2"><p>ملت اسلام را اساس مشیّد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماحی آثار کفر و ظلم و ظلالت</p></div>
<div class="m2"><p>حامی شرع مبین متقن احمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سایه ی یزدان خدیو عادل باذِل</p></div>
<div class="m2"><p>فخر سلاطین غلام آل محمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناصر دین شاه بی همال که او را</p></div>
<div class="m2"><p>دولت جاوید بادو ملک مخلد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یافت به دوران زدادگستری او</p></div>
<div class="m2"><p>ملت بیضا شکوه فر مجدد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هریک از چاکران درگه شاهی</p></div>
<div class="m2"><p>پیشه نمودند کسب دولت سرمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این حرم محترم که تربت پاکش</p></div>
<div class="m2"><p>صد ره بهتر بود ز روح مجرّد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سال هزار و دویست و نود و یک</p></div>
<div class="m2"><p>چون به شد از هجرت رسول مسدّد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد خجسته بنای آینه اش را</p></div>
<div class="m2"><p>صِهر شهنشه امیر اکرم امجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فخر خوانین سپهر شوکت و حشمت</p></div>
<div class="m2"><p>خان معیر جهان همت و سودّد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زاده ی گنجور شاه دوست محمد علی خان</p></div>
<div class="m2"><p>خازن خسرو و امیر دوست محمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وارد این بقعه چون شوی که بروبد</p></div>
<div class="m2"><p>خاک درش حور عین ز زلف مجعّد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن چه به خواهی طلب نما که خداوند</p></div>
<div class="m2"><p>هیچ دعا را درین مکان نکند رد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سجده کن و خاک را به بوس که اینجا</p></div>
<div class="m2"><p>آمده مدفون سلسل اطهر احمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زاده ی هفتم امام حمزه که هر دم</p></div>
<div class="m2"><p>بادا بوی درود و رحمت بی حد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>روز قیامت سفید رو بود و شاد</p></div>
<div class="m2"><p>هرکه بساید به خاک درگه او خّد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در حرمش مهر و مه بود چو دو قندیل</p></div>
<div class="m2"><p>روز شبش خادمان ابیض و اسود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عارف او ناجی است و صالح و مؤمن</p></div>
<div class="m2"><p>منکر وی هالک است و طالح و مرتد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آل پیمبر همه مظاهر حقند</p></div>
<div class="m2"><p>مدحتشان چون ثنای حق شده بیعد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست چو ممکن مدیح حضرت حمزه</p></div>
<div class="m2"><p>ختم سخن بر دعا نمود محمد</p></div></div>