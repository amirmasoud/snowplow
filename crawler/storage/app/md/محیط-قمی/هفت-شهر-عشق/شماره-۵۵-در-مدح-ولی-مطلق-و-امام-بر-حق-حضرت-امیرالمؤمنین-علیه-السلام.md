---
title: >-
    شمارهٔ  ۵۵ - در مدح ولیّ مطلق و امام بر حق حضرت امیرالمؤمنین علیه السلام
---
# شمارهٔ  ۵۵ - در مدح ولیّ مطلق و امام بر حق حضرت امیرالمؤمنین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>زچهره پرده بر افکند، پردار امروز</p></div>
<div class="m2"><p>کمال قدرت حق گشت آشکار امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمود شمس حقیقت طلوع و رنگ مجاز</p></div>
<div class="m2"><p>زدوده گشت زمرآت روزگار امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ثبوت وحدت حق را بر غم مدعیان</p></div>
<div class="m2"><p>گرفت پرده ز رخ دست کردگار امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فضای عالم ایجاد، مطلع الانوار</p></div>
<div class="m2"><p>شده زجلوه ی آن ایزدی عذار امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد از تجلی او عقل، بی خود و مدهوش</p></div>
<div class="m2"><p>فتاده روح مجرد، کلیم وار امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان ناطق حق آشکار شد که رسد</p></div>
<div class="m2"><p>به گوش صوت اناالحق زهر کنار امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به رهگذار طلب گرچه داشت عمری چشم</p></div>
<div class="m2"><p>زمانه آمده بیرون زانتظار امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنام فرخ سلطان عشق رز وجود</p></div>
<div class="m2"><p>گرفت زینت و شد کامل العیار امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زدند سکه ی دولت به نام خسرو دین</p></div>
<div class="m2"><p>فراز گنبد این نیلگون حصار امروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امین خلوت وحدت به عالم کثرت</p></div>
<div class="m2"><p>قدم نهاد زالطاف بی شمار امروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نثار مقدم او را به ساحت گیتی</p></div>
<div class="m2"><p>نمود عقد ثریا، فلک نثار امروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصول حضرت او را سوی بسیط زمین</p></div>
<div class="m2"><p>نمود عیسی گردون نشین گذار امروز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پدید آمد، حلّال مشکلات و گشود</p></div>
<div class="m2"><p>جهانیان همه را عقده ها، زکار امروز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قسیم جنت و دوزخ ولی بار خدای</p></div>
<div class="m2"><p>نمود قسمت روزی مور و مار امروز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به خوان جود، صلا زد جهانیان همه را</p></div>
<div class="m2"><p>رسید صیت سخایش، به هر دیار امروز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زبطن مام هویت به مهد امکان پای</p></div>
<div class="m2"><p>نهاد والد والای هفت و چهار امروز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عدو به کَتمِ عدم شد نهان زملک وجود</p></div>
<div class="m2"><p>زبیم صاحب برنده، ذوالفقار امروز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سخن نهفته چه گویم درون پرده غیب</p></div>
<div class="m2"><p>هرآن چه بود نهان گشت آشکار امروز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شه سریر ولایت، علی نمود ظهور</p></div>
<div class="m2"><p>اساس ملت حق گشت استوار امروز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درون خانه ی حق در وجود آمد و یافت</p></div>
<div class="m2"><p>زیمن مقدم او خانه، اعتبار امروز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زغیر خانه به پرداخت، صاحب خانه</p></div>
<div class="m2"><p>حریم خلوت او گشت، خاص یار امروز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پی ذخیره ی روز شمار، کرد «محیط»</p></div>
<div class="m2"><p>زمدح بی حد او، شمه ی شمار امروز</p></div></div>