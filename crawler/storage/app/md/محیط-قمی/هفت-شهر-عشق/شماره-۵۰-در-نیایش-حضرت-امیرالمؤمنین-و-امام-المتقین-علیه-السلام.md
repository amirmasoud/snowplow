---
title: >-
    شمارهٔ  ۵۰ - در نیایش حضرت امیرالمؤمنین و امام المتقین علیه السلام
---
# شمارهٔ  ۵۰ - در نیایش حضرت امیرالمؤمنین و امام المتقین علیه السلام

<div class="b" id="bn1"><div class="m1"><p>مه من هر که تو را ناظر و مایل نبود</p></div>
<div class="m2"><p>هیچش از دیده و دل بهره و حاصل نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدمیزاده نه از خیل بهائم باشد</p></div>
<div class="m2"><p>حیوانی که به دیدار تو مایل نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست چون حلقه ی گیسوی تو هر سلسله ی</p></div>
<div class="m2"><p>تیره بخت آن که گرفتار سلاسل نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درگذر از تن خاکی که میان تو و یار</p></div>
<div class="m2"><p>غیر این گرد برانگیخته حائل نبود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زخود دور شوی در بر جانانه رسی</p></div>
<div class="m2"><p>حاجت قطع ره و طی منازل نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رهروی را که بود قائد توفیق، دلیل</p></div>
<div class="m2"><p>جز سرکوی تواش مقصد و منزل نبود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برکنم دیده گرش غیر تو منظور بود</p></div>
<div class="m2"><p>دل بر آتش فکنم گر به تو مایل نبود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه همین غرقه ی احسان تو من باشم و بس</p></div>
<div class="m2"><p>لطف عام تو که را که آفل و شامل نبود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دست می داد مرا کاش زجان خوبتری</p></div>
<div class="m2"><p>زان که جان از پی تقدیم تو قابل نبود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هرکه شد غرقه ی دریای محبت، امید</p></div>
<div class="m2"><p>که دگر باره کشد رخت به ساحل نبود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حل هر عقده ی مشکل ز ولی الله خواه</p></div>
<div class="m2"><p>غیر او کافی و حلال مشاکل نبود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دست حق ضارب خندق که به یک ضربت او</p></div>
<div class="m2"><p>طاعت جن و بشر جمله مقابل نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یا علی گرچه مرا عقده بسی در کار است</p></div>
<div class="m2"><p>حل آن با مدد لطف تو مشکل نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز محشر که دل کوه، بلرزد زنهیب</p></div>
<div class="m2"><p>با تولای توام، دل متزلزل نبود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاه پیوسته چه آگاه و کریم است «محیط»</p></div>
<div class="m2"><p>بیم محرومی و ناکامی سائل نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ابلهی را که به سر شور پری رویان نیست</p></div>
<div class="m2"><p>به جنون وصف کن ای خواجه که عاقل نبود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقبلی را که به شمشیر تو گردد مقتول</p></div>
<div class="m2"><p>دیتی خوبتر از دیدن قاتل نبود</p></div></div>