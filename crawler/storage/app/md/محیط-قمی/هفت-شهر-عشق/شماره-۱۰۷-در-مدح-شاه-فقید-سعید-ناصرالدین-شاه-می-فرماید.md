---
title: >-
    شمارهٔ  ۱۰۷ - در مدح شاه فقید سعید ناصرالدین شاه می فرماید
---
# شمارهٔ  ۱۰۷ - در مدح شاه فقید سعید ناصرالدین شاه می فرماید

<div class="b" id="bn1"><div class="m1"><p>نو بهار آمد و آراست چمن را چو عروس</p></div>
<div class="m2"><p>موسم عشرت و هنگام کنار آمد و بوس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ ز انواع ریاحین چو پرطاووس است</p></div>
<div class="m2"><p>گلستان از گل حمری همه چون چشم خروس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارض باغ زمشّاطه گی باد بهار</p></div>
<div class="m2"><p>شده آراسته تر از رخ زیبای عروس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرت امروز دهد دست به جای می و نقل</p></div>
<div class="m2"><p>چشم جانانه ببین و لب معشوقه به بوس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به یک بوسه بدان شوخ فرنگی به خشم</p></div>
<div class="m2"><p>گر میسّر شودم سلطنت روس و پروس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز عید است و پی دعوت ارباب نیاز</p></div>
<div class="m2"><p>خاست از درگه شاهنشه دوران غوکوس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناصرالدین شه قاجار خداوند ملوک</p></div>
<div class="m2"><p>که کمین چاگر او راست فر کیکاوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا فلک خاسته بانگ طرب از ساحت ملک</p></div>
<div class="m2"><p>تا به اقبال بر اورنگ شهی کرده جلوس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عهد او عهد سلامت بود و دور امان</p></div>
<div class="m2"><p>حبّذا این شه و این عهد سعادت مأنوس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کاروان گر ببرد دفتر اشعار محیط</p></div>
<div class="m2"><p>شکر از هند عوض آورد و قند از روس</p></div></div>