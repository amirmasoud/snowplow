---
title: >-
    شمارهٔ  ۱۳۴ - در مدح جنّت مکان علی قلی میرزا طاب ثراه
---
# شمارهٔ  ۱۳۴ - در مدح جنّت مکان علی قلی میرزا طاب ثراه

<div class="b" id="bn1"><div class="m1"><p>وقت است زچهره پرده برداری</p></div>
<div class="m2"><p>دلداده هزار، بیشتر داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک بار اگر جمال به نمایی</p></div>
<div class="m2"><p>بازار دو کون، بی خبر داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر راه تو دیده ها است از هر سو</p></div>
<div class="m2"><p>تا رأی کدام، رهگذر داری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوسته علاج ضعف دل ها را</p></div>
<div class="m2"><p>از عارض و لعل گل شکر داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای تازه نهال باغ زیبایی</p></div>
<div class="m2"><p>افسوس که جور و کین ثمر داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر دل شدگان عیان ز روی و مو</p></div>
<div class="m2"><p>روز دگر و شب دگر داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از طره ی مشک سا، به طراری</p></div>
<div class="m2"><p>افکنده کمند بر قمر داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر مهر و مه از غلامی خواجه</p></div>
<div class="m2"><p>بس فخر تو شوخ سیمبر داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهزاده علی قلی که بر بابش</p></div>
<div class="m2"><p>مانند فلک مدام سر داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاها به مثل تو آن درخت استی</p></div>
<div class="m2"><p>کز بخشش وجود برگ و بر داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خوش باش «محیط» کز عطای میر</p></div>
<div class="m2"><p>دامان مه و سال پرگهر داری</p></div></div>