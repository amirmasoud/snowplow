---
title: >-
    شمارهٔ  ۱۳۱ - در تاریخ وفات نجف قلی خان فرماید
---
# شمارهٔ  ۱۳۱ - در تاریخ وفات نجف قلی خان فرماید

<div class="b" id="bn1"><div class="m1"><p>ای آن که بر مزار غریبان کنی گذار</p></div>
<div class="m2"><p>آهسته پای نه که بود چشم و روی و سر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آهسته پای نه که به هر جا کنی گذار</p></div>
<div class="m2"><p>بر پیکر لطیف عزیزی بود گذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این خفته زیر خاک که خوارش نمود مرگ</p></div>
<div class="m2"><p>بودی در این جهان عزیزتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>والاگهر سلیل علی خان نجف قلی</p></div>
<div class="m2"><p>خان خجسته خوی، امیر نکوسیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هجرت نمود عهد صغر را زسیستان</p></div>
<div class="m2"><p>آمد به سوی خطه ی ری با مهین پدر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عز و جاه زیست چهل سال و برد عمر</p></div>
<div class="m2"><p>در کف نیک نامی و آزادگی بسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سال هزار و سیصد و سه جانب جنان</p></div>
<div class="m2"><p>گردید روز عاشر ذی حجه ره سپر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ری وداع دار فنا گفت و مدفنش</p></div>
<div class="m2"><p>شد خاک پای قم که بود در شرف ثمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در درگهی که عالمیان را بود مطاف</p></div>
<div class="m2"><p>مانند کعبه هست در آفاق مشتهر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تاریخ سال رحلت وی جستم از «محیط»</p></div>
<div class="m2"><p>به نمود چون به سوی ریاض جنان سفر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتات نجف قلی خان طوبی مقر بود</p></div>
<div class="m2"><p>تاریخ سال رحلت او هست در شمر</p></div></div>