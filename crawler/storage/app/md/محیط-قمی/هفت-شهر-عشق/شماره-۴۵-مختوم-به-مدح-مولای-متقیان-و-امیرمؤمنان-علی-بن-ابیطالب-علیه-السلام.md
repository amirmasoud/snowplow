---
title: >-
    شمارهٔ  ۴۵ - مختوم به مدح مولای متقیان و امیرمؤمنان علی بن ابیطالب علیه السلام
---
# شمارهٔ  ۴۵ - مختوم به مدح مولای متقیان و امیرمؤمنان علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>قد پی تعظیم خلق خم نتوان کرد</p></div>
<div class="m2"><p>بندگی غیر ذوالکرم نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سو مو گر شود هزار زبان باز</p></div>
<div class="m2"><p>شکر تو یا سابغ النعم نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منت دونان پی دونان نتوان برد</p></div>
<div class="m2"><p>بهر درم حال خود دژم نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن همت که پاک آمده از عیب</p></div>
<div class="m2"><p>طمع و حرص، متهم توان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غره مشو بر دو روز دولت دنیا</p></div>
<div class="m2"><p>زنده دلا، تکیه بر عدم نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از پی مخلوق، ترک حق نتوان گفت</p></div>
<div class="m2"><p>ترک صمد، سجدهٔ صنم نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با تو برادر هر آن چه شرط وفا بود</p></div>
<div class="m2"><p>گفتم و تکرار، دم به دم نتوان کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن چه زدیوان غیب، گشته مقرر</p></div>
<div class="m2"><p>هیچ به تدبیر، بیش و کم نتوان کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جز زنبی و علی و فاطمه و آل</p></div>
<div class="m2"><p>از دگری خواهش کرم نتوان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ترک ولای علی «محیط» نگوید</p></div>
<div class="m2"><p>بر خود و بر جان خود، ستم نتوان کرد</p></div></div>