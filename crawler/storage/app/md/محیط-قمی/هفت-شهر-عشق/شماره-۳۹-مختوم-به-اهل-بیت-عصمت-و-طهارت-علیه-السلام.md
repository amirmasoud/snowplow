---
title: >-
    شمارهٔ  ۳۹ - مختوم به اهل بیت عصمت و طهارت علیه السلام
---
# شمارهٔ  ۳۹ - مختوم به اهل بیت عصمت و طهارت علیه السلام

<div class="b" id="bn1"><div class="m1"><p>جماعتی که دل و جان به عشق نسپارند</p></div>
<div class="m2"><p>به حیرتم! چه تمتع ز زندگی دارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آن سرم که برآرم دمی به خاطر جمع</p></div>
<div class="m2"><p>گرم دو زلف پریشان دوست بگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صاحبان نظر ساقیا مده ساغر</p></div>
<div class="m2"><p>که با حضور تو پیوسته مست دیدارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دور چشم تو مستی ما عجب نبود</p></div>
<div class="m2"><p>عجب زحالت آنان بود که هوشیارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهل مدرسه ای دل، امید حال مدار</p></div>
<div class="m2"><p>که اهل قال وز سر تا به پای گفتارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روی خویش سوی بسته راه یقین</p></div>
<div class="m2"><p>نشسته در پس هفتم حجاب بیدارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخیل خاک نشینان جماعتی دانم</p></div>
<div class="m2"><p>که چون سپهر رفیع و بلند مقدارند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو نوش، راحت روحند و در مذاق چو نیش</p></div>
<div class="m2"><p>چو گل عزیز و به چشم جهانیان خارند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکسته قید علایق به زور بازوی عشق</p></div>
<div class="m2"><p>نه چون من و تو به دام هوس گرفتارند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نگاهدار زاندیشه های باطل، دل</p></div>
<div class="m2"><p>حضورشان که ز راز درون خبر دارند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مدد زهمت ایشان رسد به پیل دمان</p></div>
<div class="m2"><p>ولی به زیر قدم، مور را نیازارند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شهان عالم ایجاد و ملکان وجود</p></div>
<div class="m2"><p>غلام خواجه ی لولاک و آل اطهارند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به اهل بیت رسالت مرا است چشم امید</p></div>
<div class="m2"><p>چه بر گناه تنم را به خاک بسپارند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به غیر آن که نشاید خدایشان خواندن</p></div>
<div class="m2"><p>به هرچه وصف نمایندشان سزاوارند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>«محیط» از شرف مدحت محمد و آل</p></div>
<div class="m2"><p>متاع نظم تو را خسروان خریدارند</p></div></div>