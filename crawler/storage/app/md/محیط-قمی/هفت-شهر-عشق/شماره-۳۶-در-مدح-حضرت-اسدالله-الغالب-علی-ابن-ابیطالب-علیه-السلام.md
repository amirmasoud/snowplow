---
title: >-
    شمارهٔ  ۳۶ - در مدح حضرت اسدالله الغالب علی ابن ابیطالب علیه السلام
---
# شمارهٔ  ۳۶ - در مدح حضرت اسدالله الغالب علی ابن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>آن شاهد مقصود که در پرده نهان بود</p></div>
<div class="m2"><p>دوشینه ی بر دیده ی من، جلوه کنان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوشین لب و رخشان رخ و زلف به خم او</p></div>
<div class="m2"><p>نور بصر و تاب دل و قوت روان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواندم مه و مهرش به نکویی چو بدیدم</p></div>
<div class="m2"><p>بهتر ز مه و مهر، نه این بود و نه آن بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خواندمیش ماه اگر، ماه سخنگو</p></div>
<div class="m2"><p>می گفتمیش سرو، اگر سرو روان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیدار شد از خواب چو چشمان تو شد باز</p></div>
<div class="m2"><p>هر فتنه ی خوابیده که در ملک زمان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود را نگه از فتنه ی ایام، توان داشت</p></div>
<div class="m2"><p>وز نرگش فتان تو، ایمن نتوان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سود که گفتند به بازار جهان هست</p></div>
<div class="m2"><p>دیدیم به جز سود غمت جمله زیان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معذور همی دار اگر بی خود و مستیم</p></div>
<div class="m2"><p>هشیار به دور لب لعلت نتوان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر زلف و بناگوش ترا خلق نمی کرد</p></div>
<div class="m2"><p>ایزد، نه زدین نام و نه از کفر نشان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در حلقه ی نوشین دهنان، هرکه بدیدم</p></div>
<div class="m2"><p>این ملطع جان بخش منش ورد زبان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روزی که نه از ماه نه از مهر نشان بود</p></div>
<div class="m2"><p>روشن زتجلی علی کون و مکان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وجه الله باقی که عیان شد زشهودش</p></div>
<div class="m2"><p>آن شاهد غیبی که پس برده نهان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>موسی ار نی گوی، چه در طور درآمد</p></div>
<div class="m2"><p>نوری که تجلی بر آن بود، همان بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با کشتی خود خود نوح زحملش سخنی گفت</p></div>
<div class="m2"><p>زان روی زطوفان حوادث، به امان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آتش به خلیل الله از آن برد سلامت</p></div>
<div class="m2"><p>گردید که حبّ علیش جوشن جان بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صوت خوش داود که بردی دل عالم</p></div>
<div class="m2"><p>یک شمه اش از فیض لب و لطف میان بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از شادی قرب حرمش از دل آدم</p></div>
<div class="m2"><p>رفت آن غم و اندوه که از بعد جنان بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون یافت مدد از دم او عیسی مریم</p></div>
<div class="m2"><p>جان بخش دمش غیرت آب حیوان بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون نام علی نقش نگین کرد سلیمان</p></div>
<div class="m2"><p>حکمش چو قضا بر همه ی خلق روان بود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تنها نه همین بود معین، ختم رسل را</p></div>
<div class="m2"><p>بر خیل رسل یار، به هر عهد و زمان بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در منقبتش هر چه «محیط» از دل و جان گفت</p></div>
<div class="m2"><p>صدق است و یقین دان، نه دروغ و نه گمان بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاهی که تولاش بود معنی ایمان</p></div>
<div class="m2"><p>کافر بود آن کس که بگوید، نه چنان بود</p></div></div>