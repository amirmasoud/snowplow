---
title: >-
    شمارهٔ  ۱۰۸ - در مدح سلطان سعید شهید ناصرالدین شاه
---
# شمارهٔ  ۱۰۸ - در مدح سلطان سعید شهید ناصرالدین شاه

<div class="b" id="bn1"><div class="m1"><p>فرخنده باد نوروز بر شهریار آفاق</p></div>
<div class="m2"><p>بوالنّصر ناصرالدین شاه خجسته اخلاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید تاجداران فرخنده ظلّ یزدان</p></div>
<div class="m2"><p>فرمانده سلاطین مولی الملوک آفاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ظلّ کردگارش خواندم بود سزاوار</p></div>
<div class="m2"><p>باشد خدیو عادل بر خلق ظلّ خلّاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جمع خسروانش نتوان نظیر جستن</p></div>
<div class="m2"><p>این بی همال خسرو باشد زخسروان طاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باشد فروغ رویش بهتر زچارمین نجم</p></div>
<div class="m2"><p>باشد اساس قدرش برتر زهفتمین طاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون مفلساتن به دولت خستگان به راحت</p></div>
<div class="m2"><p>دل ها به او است راغب جان ها به او است مشتاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ خجسته ی او است مفتاح هفت کشور</p></div>
<div class="m2"><p>با این خجسته مفتاح مفتوح گردد آفاق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جستم زنکته دانی مصداق سوره ی فتح</p></div>
<div class="m2"><p>گفتا که رایت او این آیه راست مصداق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برهان فیض سرمد ذات ستوده ی او است</p></div>
<div class="m2"><p>که آسوده عالمی را دارد زفرط اشفاق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر روز او چو نوروز فیروز باد تا هست</p></div>
<div class="m2"><p>روز وصال معشوق عید سعید عشاق</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دیوان نظم من گشت حِرز شهان محیطا</p></div>
<div class="m2"><p>تا نام نیک شاهش گردیده زیب اوراق</p></div></div>