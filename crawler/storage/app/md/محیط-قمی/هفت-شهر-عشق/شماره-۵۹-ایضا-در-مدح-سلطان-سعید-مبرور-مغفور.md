---
title: >-
    شمارهٔ  ۵۹ - ایضاً در مدح سلطان سعید مبرور مغفور
---
# شمارهٔ  ۵۹ - ایضاً در مدح سلطان سعید مبرور مغفور

<div class="b" id="bn1"><div class="m1"><p>ای دل به راه عشق بتان استوار باش</p></div>
<div class="m2"><p>گر سر رود ز ره مرو، و پای دار باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گسترده دام ها پی صید تو، دیو نفس</p></div>
<div class="m2"><p>پا در ره طلب چو نهی، هوشیار باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر طالبی ز دام رهی، ترک دانه کن</p></div>
<div class="m2"><p>به پذیر، پند نغز من و رستگار باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محنت فزاست شش جهت خاندان خاک</p></div>
<div class="m2"><p>جویی سلامت از همه سو، بر کنار باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیتی چو تل خاکی و گردون غبار او است</p></div>
<div class="m2"><p>آئینه ای تو! دور، زخاک و غبار باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون عنکبوت رای به صید مگس مکن</p></div>
<div class="m2"><p>تو شیر شیرزه هستی مردم شکار باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نی نی چو شیر در پی آزار کس مباش</p></div>
<div class="m2"><p>مانند مور جور کش و بردبار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منصور وار لاف انا الحق اگر زدی</p></div>
<div class="m2"><p>آماده ی سیاست، تکفیر وار باش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگذار تو از مجال است پست همتان</p></div>
<div class="m2"><p>خاک قدوم مردم عالی تبار باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خواهی که روسیه نشوی گاه امتحان</p></div>
<div class="m2"><p>مانند زر خالص کامل عیار باش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا کی غم زمانه خوری، باده نوش کن</p></div>
<div class="m2"><p>جانا ترا گفت؟ که دایم فکار باش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرمست شو زساغر سرشار نیستی</p></div>
<div class="m2"><p>ایمن زبیم شحنه و رنج خمار باش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از عاکفان درگه سلطان عشق شو</p></div>
<div class="m2"><p>از محرمان خلوت اسرار یار باش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آزادی دو کون گرت هست آرزو</p></div>
<div class="m2"><p>از بندگان درگه هشت و چهار باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگریز در پناه شه اولیا علی علیه السلام</p></div>
<div class="m2"><p>آسوده از کشاکش روز شمار باش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یا دعوی بزرگی و آزادگی مکن</p></div>
<div class="m2"><p>یا چون امین خلوت نیکو شعار باش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا روزگار، پایدار ای میر کامکار</p></div>
<div class="m2"><p>دلشاد در پناه شه روزگار باش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ظل الاه ناصر دین شه که آفتاب</p></div>
<div class="m2"><p>گوید به چرخ، در ره وی خاکسار باش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نزد امین خلوت شه خواجه ی «محیط»</p></div>
<div class="m2"><p>ای نظم دلفریب، زمن یادگار باش</p></div></div>