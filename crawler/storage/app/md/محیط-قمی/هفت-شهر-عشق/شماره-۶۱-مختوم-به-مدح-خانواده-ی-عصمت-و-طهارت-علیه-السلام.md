---
title: >-
    شمارهٔ  ۶۱ - مختوم به مدح خانواده ی عصمت و طهارت علیه السلام
---
# شمارهٔ  ۶۱ - مختوم به مدح خانواده ی عصمت و طهارت علیه السلام

<div class="b" id="bn1"><div class="m1"><p>عجب مدار اگر وضع عالم است پریش</p></div>
<div class="m2"><p>که یار کرده پریشان شکنج طُرّه ی خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن به حلقه ی گیسوی او گذار، ای دل</p></div>
<div class="m2"><p>که هر که یافت در آن پرده راه، گشت پریش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جد و جهد گشودم گره زطره ی دوست</p></div>
<div class="m2"><p>به سعی خویش نمودم پریش، حالت خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخست گام، نمودم وداع هستی خود</p></div>
<div class="m2"><p>طریق پرخطر عشق چون گرفتم، پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جز لبم که زلعل تو یافت بهبودی</p></div>
<div class="m2"><p>شنیده ای زنمک به شود، جراحت ریش؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذشت در غم هجران، زمان عمر و هنوز</p></div>
<div class="m2"><p>امید وصل تو دارد، دل محال اندیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز رنج و راحت دوران غمین و شاد مباش</p></div>
<div class="m2"><p>که نیستی است سرانجام هر دو، ای درویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به جد و جهد نخواهد شدن، میسر نوش</p></div>
<div class="m2"><p>زخوان غیب تو را، چون نصیب آمده نیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ولای احمد و آل است کیش ما یا رب</p></div>
<div class="m2"><p>بدار باقی ما را، برین شریعت و کیش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طریق صدق و ارادت گرفته پیش «محیط»</p></div>
<div class="m2"><p>امید هست کزین ره رسد به مقصد خویش</p></div></div>