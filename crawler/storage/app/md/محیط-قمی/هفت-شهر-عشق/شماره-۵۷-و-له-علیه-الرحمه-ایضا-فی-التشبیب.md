---
title: >-
    شمارهٔ  ۵۷ - وَ لَهُ عَلَیهِ الرَّحمه ایضاً فی التَّشبیب
---
# شمارهٔ  ۵۷ - وَ لَهُ عَلَیهِ الرَّحمه ایضاً فی التَّشبیب

<div class="b" id="bn1"><div class="m1"><p>غیر بوسیدن تو نداریم هوس</p></div>
<div class="m2"><p>هوس ما به همه عمر، همین باشد و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیگران را هوس حور و قصور است و مرا</p></div>
<div class="m2"><p>نیست جز دیدن روی و سر کوی تو هوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی گل روی تو ای سرو قد و لاله عذار</p></div>
<div class="m2"><p>هست بر دلشدگان، ساحت گلشن چو قفس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختم از غم هجران تو چونان که بود</p></div>
<div class="m2"><p>بر سوز دل من، آتش دوزخ چو قبس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شوق ما کم نشود با تو زغوغای رقیب</p></div>
<div class="m2"><p>بادبیزن نکند منع تقاضای مگس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز باد سحری غالیه بو گشته مگر</p></div>
<div class="m2"><p>به هوای سر زلف تو برآورده قفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفت جانانه و صد قافله دل گشت روان</p></div>
<div class="m2"><p>از پی محمل وی ناله کنان هم چو جرس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کیست آن شاهسوار خوش شیرین حرکات</p></div>
<div class="m2"><p>که فتاده دل خلقیش بدنبال فرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا مگر اهل دلی را به کف آرم روزی</p></div>
<div class="m2"><p>روزگاری است که محرم شده ام با همه کس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به فصاحت شده ام شهره آفاق ولی</p></div>
<div class="m2"><p>در بیان سخن عشق تو باشم اخرس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به همه عمر دمی با تو بسر برده «محیط»</p></div>
<div class="m2"><p>حاصل عمر گرانمایه همین باشد و بس</p></div></div>