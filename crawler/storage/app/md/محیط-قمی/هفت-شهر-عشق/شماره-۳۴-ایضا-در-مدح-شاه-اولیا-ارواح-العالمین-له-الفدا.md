---
title: >-
    شمارهٔ  ۳۴ - ایضاً در مدح شاه اولیا ارواح العالمین له الفدا
---
# شمارهٔ  ۳۴ - ایضاً در مدح شاه اولیا ارواح العالمین له الفدا

<div class="b" id="bn1"><div class="m1"><p>غیرت طوبی بود، قامت دلجوی دوست</p></div>
<div class="m2"><p>رشک ریاض جنان، خاک سر کوی دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قید غلایق گسست، قوّت بازوی عشق</p></div>
<div class="m2"><p>حجاب هستی به سوخت، تجلّی روی دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلسلهٔ کائنات، جمله به رقصند و هست</p></div>
<div class="m2"><p>سلسله‌جنبانشان، سلسلهٔ موی دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی زمین را گرفت، تیغ زبانم تمام</p></div>
<div class="m2"><p>تا که حکایت شود، زتیغ ابروی دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول ایام عمر، روز وفات من است</p></div>
<div class="m2"><p>زان که به روز وفات، ببینمی روی دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کف داود از آن، آهن چون موم بود</p></div>
<div class="m2"><p>که می رسیدنش مدد، زسخت بازوی دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واسطه ی فیض روح، بود دم عیسوی</p></div>
<div class="m2"><p>منبع آن فیض بود، لعل سخنگوی دوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با ید بیضای کلیم، گشت زخود بی خبر</p></div>
<div class="m2"><p>کرد تجلی به طور، چو ایزدی روی دوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دوست که باشد علی، هست مراد «محیط»</p></div>
<div class="m2"><p>وان که بود بعد مرگ، خاک سرکوی دوست</p></div></div>