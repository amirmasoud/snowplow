---
title: >-
    شمارهٔ  ۵۶ - در مدح معیّر الممالک دام مجدُهُ
---
# شمارهٔ  ۵۶ - در مدح معیّر الممالک دام مجدُهُ

<div class="b" id="bn1"><div class="m1"><p>آب خضر از لب لعل تو نه من جویم و بس</p></div>
<div class="m2"><p>چون سکندر طلبد چشمهٔ حیوان همه کس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط آزادی و منشور سرافرازی یافت</p></div>
<div class="m2"><p>سرو تا بندگی قد تو را کرد هوس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خم زلف تو مرغ دل دیوانه اسیر</p></div>
<div class="m2"><p>هم چو دزدی که شود بسته به زنجیر عسس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طپد از هجر گل روی تو در سینه دلم</p></div>
<div class="m2"><p>بلبل آری چه کند غیر طپیدن، به قفس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار در محمل و مرغ دلم از سوز نوا</p></div>
<div class="m2"><p>شور در قافله افکند، به آهنگ جرس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب وصل است مکن زمزمه ای مرغ سحر</p></div>
<div class="m2"><p>تو هم ای صبح خدا را، مکش از سینه قفس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می فروشد لب شیرین تو شکر، لیکن</p></div>
<div class="m2"><p>راه بر مشتریان بسته، رقیبان چو مگس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر به گنجور تو نالد فرزین</p></div>
<div class="m2"><p>شاطرش حلقه به گوشت کند از نعل فرس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاصه داماد ملک، دوست محمد که جهان</p></div>
<div class="m2"><p>بود اندر نظر همت او کم ز عدس</p></div></div>