---
title: >-
    شمارهٔ  ۷۷ - موشّح و مختوم به نام نامی حضرت صاحب الزمان علیه السلام
---
# شمارهٔ  ۷۷ - موشّح و مختوم به نام نامی حضرت صاحب الزمان علیه السلام

<div class="b" id="bn1"><div class="m1"><p>گدای میکده ام خشت زیر سر دارم</p></div>
<div class="m2"><p>زمهر افسر و از کهکشان کمر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیمن عاطفت پیر می فروش مدام</p></div>
<div class="m2"><p>شراب صافی و ساقی سیمبر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبین به چشم حقارت به وضع مختصرم</p></div>
<div class="m2"><p>که بس جلال بدین وضع مختصر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوشم به بی سر و پایی که تا چنین شده ام</p></div>
<div class="m2"><p>نه رنج پاس کلاه و نه بیم سر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سلطنت ندهم پیشه ی قناعت را</p></div>
<div class="m2"><p>که اهل دانشم و بینش و بصر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فراغ خاطر و عیش مدام و خلوت امن</p></div>
<div class="m2"><p>هرآن چه دارم ازین پیشه سربسر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخویش بی خبرم تا نموده جذبه ی عشق</p></div>
<div class="m2"><p>به جان دوست که از عالمی خبر دارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فقیر و عاجز و درویش و عورم ای خواجه</p></div>
<div class="m2"><p>مرا بخر که از این گونه بس هنر دارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همان همایون شاخم که باغبان از من</p></div>
<div class="m2"><p>مر آن ثمر که به جوید، همان ثمر دارم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل و دمی چه جهان تاب مهر و روشن روز</p></div>
<div class="m2"><p>زفیض آه شب و ناله ی سحر دارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به جز محبت نیکان زمن مجو هنری</p></div>
<div class="m2"><p>که در سرشت نهفته همین هنر دارم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیازمندم به چشم امید در همه عمر</p></div>
<div class="m2"><p>به لطف حجت موعود منتظر دارم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پناه کون و مکان صاحب الزمان مهدی</p></div>
<div class="m2"><p>که خاک درگه والاش تاج سر دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به جرم دوستی او اگر بُرند سرم</p></div>
<div class="m2"><p>گمان مبر که سر از آستانش بردارم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زیمن تربیت بندگان او است محیط</p></div>
<div class="m2"><p>که طبع هم چو یم و نظم چون گهر دارم</p></div></div>