---
title: >-
    شمارهٔ  ۸۹ - مختوم و موشّح به مدح کننده ی در خیبر ارواح العالمین له الفدا
---
# شمارهٔ  ۸۹ - مختوم و موشّح به مدح کننده ی در خیبر ارواح العالمین له الفدا

<div class="b" id="bn1"><div class="m1"><p>بوی جان آید زتار موی تو</p></div>
<div class="m2"><p>زنده دارد عالمی را بوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نباشد قبله جانا کوی تو</p></div>
<div class="m2"><p>از چه باشد روی عالم سوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خط کعبه یافتم، از قطب دل</p></div>
<div class="m2"><p>راست باشد با خم ابروی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس دل خونین بر او آویخته</p></div>
<div class="m2"><p>شد چو شاخ ارغوان گیسوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست دامی در ره اهل نظر</p></div>
<div class="m2"><p>سخت تر از حلقه های موی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در برم دل از طرب آید به رقص</p></div>
<div class="m2"><p>چون به خاطر آورم پهلوی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نبودی آب لعل دلکشت</p></div>
<div class="m2"><p>می زدی آتش به عالم خوی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد زفیض آستان بوسی شاه</p></div>
<div class="m2"><p>جان فزا لعل لب نیکوی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاه درویشان که از خاک درش</p></div>
<div class="m2"><p>هست فیضی آب و رنگ روی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حق پرستان را نباشد یا علی</p></div>
<div class="m2"><p>در دو عالم روی دل جز سوی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زنده گردم بعد رحلت گر وزد</p></div>
<div class="m2"><p>بر مزار من نسیم کوی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیست ای دست خدا کاری شگفت</p></div>
<div class="m2"><p>در زخیبر کندن از نیروی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می تواند کوه را کندن زجای</p></div>
<div class="m2"><p>قوه ی سر پنجه و بازوی تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای هژبر بیشه ی دین با ولات</p></div>
<div class="m2"><p>حمله بر شیران کند آهوی تو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شادمان گردد، دم رحلت «محیط»</p></div>
<div class="m2"><p>زانکه در آن دم ببیند روی تو</p></div></div>