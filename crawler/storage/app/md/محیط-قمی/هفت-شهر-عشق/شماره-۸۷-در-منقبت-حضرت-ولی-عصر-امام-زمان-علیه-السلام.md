---
title: >-
    شمارهٔ  ۸۷ - در منقبت حضرت ولی عصر امام زمان علیه السلام
---
# شمارهٔ  ۸۷ - در منقبت حضرت ولی عصر امام زمان علیه السلام

<div class="b" id="bn1"><div class="m1"><p>صباح الخیر زد بلبل به گلشن</p></div>
<div class="m2"><p>گل من عید شد، چشم تو روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلی در گلشن ایجاد به شکفت</p></div>
<div class="m2"><p>که گل می نشکفد چون او به گلشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به آزادی سمر گشتند زآغاز</p></div>
<div class="m2"><p>زیمن بندگیش، سرو و سوسن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زبویش ساحت گیتی معطر</p></div>
<div class="m2"><p>زرویش عرصه ی کیهان مزین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تجلی گرد آن ذات مقدس</p></div>
<div class="m2"><p>که نورش تافت در، وادی ایمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بود آن خلد روحانی وجودش</p></div>
<div class="m2"><p>که خاص الخاص را آمد معین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیط خاک آمد رشک افلاک</p></div>
<div class="m2"><p>زفرخ مقدمش چون شد مزین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همایون عهد میلاد خدیوی است</p></div>
<div class="m2"><p>که گردد سر خلقت زو معین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سمی و جانشین و سبط احمد</p></div>
<div class="m2"><p>ولیّ مطلق دادار ذوالمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی قائم بالسیف، شاهی</p></div>
<div class="m2"><p>که تیغش بهر دین حِصنی است زآهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مثال ماسوی، با حضرت او است</p></div>
<div class="m2"><p>مثال ظل ذی ظل فن و ذی فن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کنند انکار بودش را به غیبت</p></div>
<div class="m2"><p>گروهی منکران دیو دیدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>«تعالی شأنُه عَمَّا یقُولُون»</p></div>
<div class="m2"><p>بود هستی او از شمس، ابین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گواه هستی شئی است، آثار</p></div>
<div class="m2"><p>در این معنی نباشد شُبهه و ظن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>وجود شمس را روشن دلیلی است</p></div>
<div class="m2"><p>فروغ شمس، تابیدن ز روزن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بقای ملک امکان است برهان</p></div>
<div class="m2"><p>به بود آن شه لاهوت مسکن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خوش آن ساعت که امر آید زیزدان</p></div>
<div class="m2"><p>بدان شه کی ولی غایب من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هویدا شو که هنگام ظهور است</p></div>
<div class="m2"><p>زطلعت پرده ی غیبت، برافکن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زعدل و قسط، گیتی را بیارای</p></div>
<div class="m2"><p>نهال ظلم را، از بیخ برکن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زوال دولت سیفیانیان را</p></div>
<div class="m2"><p>به نام باقی خود، سکّه برزن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به گفتن نیست حاجت، آن چه دانی</p></div>
<div class="m2"><p>بکن ای علم یزدان را تو مخزن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به اقلیم شهود، عالم غیب</p></div>
<div class="m2"><p>درآید آن شه، خورشید گرزن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بدان جاه و جلال کبریایی</p></div>
<div class="m2"><p>که باشد در بیانش، نطق الکن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر ادهم بر شود وز گرد موکب</p></div>
<div class="m2"><p>نماید چشم مهر و ماه، روشن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>طریق حضرتش پویند، نیکان</p></div>
<div class="m2"><p>زسر پا کرده ره را طی نمودن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کند بر پیشوایان، پیشوایی</p></div>
<div class="m2"><p>رسوم دین حق را زنده کردن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>درآرد بر «انّی امرالله» آواز</p></div>
<div class="m2"><p>الهی منهی عرش نشیمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جنوداً لم تروها، در رکابش</p></div>
<div class="m2"><p>پی طاعت کمر بر بسته متقن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مسیح و دانیال و خضر و الیاس</p></div>
<div class="m2"><p>زمانی در یسارش گه در ایمن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به ظلّ رأیت فرخنده ی او</p></div>
<div class="m2"><p>تمام ماسِوَی الله جسته مأمن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قدر قلاده ی امرش قضاوار</p></div>
<div class="m2"><p>پی طاعت نموده طوق گردن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>عطارد گاه عرض لشگر وی</p></div>
<div class="m2"><p>شود درمانده از احصا نمودن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نخستین آن که یابد فیض قربش</p></div>
<div class="m2"><p>بود جبریل فرخ پیک ذوالمن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نخست از بندگان خاص آن شاه</p></div>
<div class="m2"><p>زانسان سیصد است و سیزده تن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قوی دل مردمانی، سخت بازو</p></div>
<div class="m2"><p>که گردد مویشان در دست آهن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>دل حق جویشان بر دشمن و دوست</p></div>
<div class="m2"><p>زآهن سخت تر وز موم الین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>«اشدّاء علی الکفار» فرمود</p></div>
<div class="m2"><p>پی توصیفشان حیّ مهیمن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به جبهه جمله را، داغ محبت</p></div>
<div class="m2"><p>کمند عشق یک سر را به گردن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کشد تیغ از نیام و برق تیغش</p></div>
<div class="m2"><p>زند کفار را آتش به خرمن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>زعدل و داد پر سازد جهان را</p></div>
<div class="m2"><p>که پر باشد زجور و ظلم، دامن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کشد بر دار، آن دونان که دانی</p></div>
<div class="m2"><p>زند آتش بر آن دیوان ریمن</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زبیم شحنه ی قهار عدلش</p></div>
<div class="m2"><p>برآید از نهاد جور، شیون</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فراخای جهان بر خصم گردد</p></div>
<div class="m2"><p>زخوفش تنگ تر، از چشم سوزن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کُشد دجّال را، عیسی بن مریم</p></div>
<div class="m2"><p>به امر آن شهنشاه، مهیمن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شود آفاق زیباتر زگلزار</p></div>
<div class="m2"><p>که بودی زشت تر صدره زگلخن</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>عرب را فخر از بابش به کونین</p></div>
<div class="m2"><p>عجم را مام آن شه بهترین زن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>حدیث روح پرور آب حیوان</p></div>
<div class="m2"><p>زفیض خاک راه او مبرهن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بریدش قابض ارواح باشد</p></div>
<div class="m2"><p>گه پیغام فرمودن به دشمن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>محبش هرکه خواهد زنده گردد</p></div>
<div class="m2"><p>برای کیفر از دشمن کشیدن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>«محیط» آن روز مقصودی ندارد</p></div>
<div class="m2"><p>به جز جان در رکاب او فشاندن</p></div></div>