---
title: >-
    شمارهٔ  ۵۳ - در تهنیت غدیر و منقبت حضرت امیر علیه السلام
---
# شمارهٔ  ۵۳ - در تهنیت غدیر و منقبت حضرت امیر علیه السلام

<div class="b" id="bn1"><div class="m1"><p>گرفت عهد زاشیا، دو روز، رب قدیر</p></div>
<div class="m2"><p>یکی به روز الست و یکی به روز غدیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفت عهد ز ذرات، بر خدایی خویش</p></div>
<div class="m2"><p>نخست روز و دویم روز بر خلاف میر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه سریر ولایت، علی عمرانی</p></div>
<div class="m2"><p>که از فزونی نتوان، فضائلش تقریر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخست روز الست بربکم فرمود</p></div>
<div class="m2"><p>بدون واسطه ای، بعثت رسول و سفیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الست اولی بالمؤمنین من انفسهم</p></div>
<div class="m2"><p>سرود روز دوم زامر حق، رسول و بشیر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ولی به روز دوم یافت دین حق تکمیل</p></div>
<div class="m2"><p>به نصّ آیه ی اکمال و بیّنات کثیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشای گوش حقیقت، نیوش تا برتو</p></div>
<div class="m2"><p>زشرح روز دوم، شمّه ای کنم، تقریر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حکم نصّ صریح و تواتر و اجماع</p></div>
<div class="m2"><p>ثبوت یافته در نزد عالمان خبیر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که روز ثامن عشر دوم ز ذی لاحجّه</p></div>
<div class="m2"><p>که از الست بعید غدیر، گشته شهیر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از فراغت اعمال حجّ، باز پسین</p></div>
<div class="m2"><p>رسید خواجه ی لولاک چون، به خمّ غدیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بُدند ملتزم موکب شرف زایش</p></div>
<div class="m2"><p>زسر فرازان، جمعی کثیر و جمع غفیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به حضرت نبوی جبرئیل شد نازل</p></div>
<div class="m2"><p>به امر بار خدا، ایزد سمیع و بصیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خواند آیه ی یا ایّها الرّسول بر او</p></div>
<div class="m2"><p>که هست امر به نصب، امیر خیبر گیر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مفاد آیه که اصلی، غرض رسالت را</p></div>
<div class="m2"><p>بود رساندن و تبلیغ این مهم خطیر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نکرده ای تو رسالات خویش را تبلیغ</p></div>
<div class="m2"><p>گر این رسالت ماند، به پرده ی تستیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مدار بیم زمردم که حفظ یزدانت</p></div>
<div class="m2"><p>نگاه دارد از شرّ منکران شریر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>رسول اکرم، ابلاغ امر یزدان را</p></div>
<div class="m2"><p>فرود آمد، در آن مقام بی تأخیر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نمود انجمنی آن چنان که مانندش</p></div>
<div class="m2"><p>ندیده است و نبیند، دگر سپهر اثیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شمار خلق زسبعین الف افزون بود</p></div>
<div class="m2"><p>سخن کنم زکمی، درگذشتم از تکثیر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برای آن که تمامیّ خلق بینندش</p></div>
<div class="m2"><p>که کس نگوید، تبلیغ را شده تقصیر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمود منبری آماده از جهاز شتر</p></div>
<div class="m2"><p>فراز عرشه برآمد، رسول عرش سریر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خواند آیت تبلیغ را به صوت بلند</p></div>
<div class="m2"><p>پس از ستایش یزدان بی شریک و نظیر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلی به پاسخ گفتند، اهل انجمنش</p></div>
<div class="m2"><p>تمام متّفق القول، از کبیر و صغیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گرفت عهد از ایشان چو بر رسالت خویش</p></div>
<div class="m2"><p>نمود آمدن جبرئیل را تقریر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرفت دست علی را به دست و کرد بلند</p></div>
<div class="m2"><p>چنان که در نظر ناظران نماند ستیر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به گفت هر که منش مقتدا و مولایم</p></div>
<div class="m2"><p>علی است او را مولا، علی بر او است امیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چنان که هارون از بهر موسی عمران</p></div>
<div class="m2"><p>علی مرا است وصیّ و علی مرا است وزیر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نمود از پی اتمام حجّت و تبلیغ</p></div>
<div class="m2"><p>مر این کلام فرح بخش جان فزا تقریر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سپس سرود که یا رب، والِ مَن و والاه</p></div>
<div class="m2"><p>ظهیر و ناصر، او را، ظهیر باش و نصیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نخست تابع او را عزیز دار مدام</p></div>
<div class="m2"><p>حسود و منکر او را، نمای خوار و حقیر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نزول آیه ی الیوم را پس از این امر</p></div>
<div class="m2"><p>به گفت از پی تکمیل امر حق، تکبیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سه روز کرد در آنجا وقوف و از مردم</p></div>
<div class="m2"><p>گرفت بیعت بهر، امیر خیبر گیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زبان به بخ بخ گشود، بن خطاب</p></div>
<div class="m2"><p>برای تهنیت میر بی عدیل و نظیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ازین قضیه برآشفت، حرث بن نعمان</p></div>
<div class="m2"><p>که بُد منافق و کافر دل و خبیث و شریر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر رسول خدا آمد و گشود، زبان</p></div>
<div class="m2"><p>ز روی کینه ی خصمانه، برکشید نفیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به خشم گفت که ما را، بهر چه کردی امر</p></div>
<div class="m2"><p>به ظاهر از تو شنیدیم، چون نبود گزیر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون به گویی باشد علی پسر عم من</p></div>
<div class="m2"><p>امیر بر همه ی خلق از صغیر و کبیر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>خدای گفته چنین یا تو خویش می گویی</p></div>
<div class="m2"><p>رسول اکرم فرمود: گفته حیّ قدیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سرود حرث: خدایا گر این سخن صدق است</p></div>
<div class="m2"><p>به من فرست عذابی، در آن مکن تأخیر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>فرود آمد سنگی، زآسمان به سرش</p></div>
<div class="m2"><p>زخشم ایزد و شد رهسپار، سوی سعیر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>«محیط» را خط بطلان کشیده شد به گناه</p></div>
<div class="m2"><p>به دست شوق چه کرد، این حدیث را تحریر</p></div></div>