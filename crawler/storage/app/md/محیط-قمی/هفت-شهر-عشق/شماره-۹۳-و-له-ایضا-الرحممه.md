---
title: >-
    شمارهٔ  ۹۳ - و له ایضاً الرَّحممَه
---
# شمارهٔ  ۹۳ - و له ایضاً الرَّحممَه

<div class="b" id="bn1"><div class="m1"><p>المنة الله که گرفتار کمندی</p></div>
<div class="m2"><p>گشتیم و برستیم زهر قیدی و بندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد شکر که بخت خوش و اقبال بلندم</p></div>
<div class="m2"><p>به نمود گرفتار سر زلف بلندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادم که به پای دل دیوانه نهادم</p></div>
<div class="m2"><p>از سلسله ی زلف گره گیر تو بندی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز خال به روی تو ندیدم که به ماند</p></div>
<div class="m2"><p>از سوخته بر آتش سوزنده سپندی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشا به شکر خنده لب لعل و به بخشا</p></div>
<div class="m2"><p>شوریده دلان را زکرم پسته و قندی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون خاک شدم پست، مگر بر سرم از مهر</p></div>
<div class="m2"><p>روزی فکند سایه ی قد سرو بلندی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا شاه سواری زسر لطف و کرامت</p></div>
<div class="m2"><p>روزی کندم پی، سپهر سم سمندی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سایه الطاف علی، شاه ولایت</p></div>
<div class="m2"><p>ایمن شده، رستیم ز هر بیم و گزندی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در صومعه تا چند توان بود «محیطا»</p></div>
<div class="m2"><p>برخیز و قدم زن به ره میکده چندی</p></div></div>