---
title: >-
    شمارهٔ  ۲۲ - در میلاد سعادت بنیاد امام زمان علیه السلام
---
# شمارهٔ  ۲۲ - در میلاد سعادت بنیاد امام زمان علیه السلام

<div class="b" id="bn1"><div class="m1"><p>عید است و کرده دلبر من دست و پا خضاب</p></div>
<div class="m2"><p>خوش رنگ تازه ریخته از بهر دل بر آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دارد برای بردن دل، شیوه ها به چشم</p></div>
<div class="m2"><p>ریزد بر آب رنگ و بَرَد دل ز شیخ و شاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تنها نه من ز نرگس مستش شدم ز دست</p></div>
<div class="m2"><p>بر هر که بنگری ز همین باده شد خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه هلال ابروی من، بر فروخت چهر</p></div>
<div class="m2"><p>وز پرتو جمال بِزد، راه آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سالی است در میان دل و لعل آن نگار</p></div>
<div class="m2"><p>باقی است از برای یکی بوسه شکرآب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیدانه را ستانم، امروز بوسه ای</p></div>
<div class="m2"><p>زان لعل لب که ریزدم از کان شهد ناب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شد سال نو، زدست مده باده ی کهن</p></div>
<div class="m2"><p>کین آب زندگی است در ایام، دیریاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد بهار و روز گُل است و زمان مُل</p></div>
<div class="m2"><p>ساقی بیار باده به بانگ دف و رباب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از سبزه شد بساط زُمرّد بسیط خاک</p></div>
<div class="m2"><p>وز کف فشاند لؤلؤ لالا، بر آن سَحاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بهر دلربایی عُشّاق بی قرار</p></div>
<div class="m2"><p>سُنبل بِتار طُره درافکند، پیچ و تاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز افغان عندلیب و هیاهوی می کِشان</p></div>
<div class="m2"><p>بیدار چشم نرگس مَخمور شد ز خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم چون صنوبر قد جانان به چشم من</p></div>
<div class="m2"><p>افکنده سایه سرو سهی در میان آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ذرات کائنات به رقصند و در سماع</p></div>
<div class="m2"><p>از یُمن مولد مَلک مالک الرقاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کَهف امان پناه جهان صاحب الزمان</p></div>
<div class="m2"><p>شاهی که سوده نُه فلکش جبهه بر جناب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مهدی ولی قائم موعود و منتظر</p></div>
<div class="m2"><p>آخر امام و یازدهم نجل بوتراب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عنوان آفرینش و فهرست کُن فکان</p></div>
<div class="m2"><p>کز دفتر وجود بود، فرد انتخاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>با پایه ی عنایت او، پاید آسمان</p></div>
<div class="m2"><p>در سایه ی حمایت وی، تابد آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هستند ریزه خوار ز خوان عطای او</p></div>
<div class="m2"><p>از پیل تا به پشه، ز شهباز تا ذباب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>راعی شود ز معدلتش، گرگ بَر غَنم</p></div>
<div class="m2"><p>غالب شود به مملکتش صعوه بر عقاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی امر او نریزد، یک برگ از درخت</p></div>
<div class="m2"><p>بی فیض او، نبارد یک قطره از سحاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باشد گه تجلّی یزدان بی مثال</p></div>
<div class="m2"><p>آن ذات حق نما، چو برون آید از حجاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گیرد ز عدل و دادش، آرام روزگار</p></div>
<div class="m2"><p>از ظلم و جور گیرد، هرگاه انقلاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>حَصر مَحامدش نتوان کرد از آن که هست</p></div>
<div class="m2"><p>نطق الکن و محامد بی حد و بی حساب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درمانده ام (اَغِثُنی) یا صاحب الزمان</p></div>
<div class="m2"><p>یا خاتم الائمه و یا تالی الکتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگشا در عطا زکَرم بر رخ «محیط»</p></div>
<div class="m2"><p>ای بی کف عطای تو مسدود، فتح باب</p></div></div>