---
title: >-
    شمارهٔ  ۷۲ - موشّح به نام نامی حضرت علی بن ابیطالب علیه السلام
---
# شمارهٔ  ۷۲ - موشّح به نام نامی حضرت علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>چشمان تو امروز نموده است خرابم</p></div>
<div class="m2"><p>آن گونه که مدهوش به فردای حسابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هجران و وصال تو بود خلد و جحیمم</p></div>
<div class="m2"><p>اندیشه خود جرم و خیال تو صوابم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با عدل تو حسن عمل نیست امیدم</p></div>
<div class="m2"><p>با بخشش و فضلت نبود بیم عذابم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز آی که دور از رخ و زل تو شب و روز</p></div>
<div class="m2"><p>ماهی و سمندر شده در آتش و آبم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر هرچه شوم ناظر و با هرکه مخاطب</p></div>
<div class="m2"><p>کوی تو بود منظر و سوی تو خطابم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا دل به خم زلف گره گیر تو بستم</p></div>
<div class="m2"><p>بگسست زکف رشته ی آسایش و تابم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خواب اگر جان جهان را نتوان دید</p></div>
<div class="m2"><p>آمد رخ خوب تو چرا، دوش به خوابم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستم زمی عشق علی، ساقی کوثر</p></div>
<div class="m2"><p>زنهار مپندار که مدهوش شرابم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون آب بقا یافته ام از کرم خضر</p></div>
<div class="m2"><p>کی راه زدن، غول تواند به سرابم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر معصیتم گر بکشد خواجه خط محو</p></div>
<div class="m2"><p>شاید که شده منقبتش ثبت کتابم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با مدح «محیط» از کرم شاه ولایت</p></div>
<div class="m2"><p>آسوده چو امروز، به فردای حسابم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر باد روی ای تن خاکی که غبارت</p></div>
<div class="m2"><p>گردیده پی دیدن جانانه حجابم</p></div></div>