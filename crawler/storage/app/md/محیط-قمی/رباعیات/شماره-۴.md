---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ایران همه دلکش و منظم باشد</p></div>
<div class="m2"><p>این گونه زعدل صدر اعظم باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوالمجد علی اصغر بن ابراهیم</p></div>
<div class="m2"><p>کش بنده ی جود معن و حاتم باشد</p></div></div>