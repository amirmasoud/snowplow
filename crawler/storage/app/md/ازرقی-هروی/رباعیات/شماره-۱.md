---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آن کس که ز ناصواب بشناخت صواب</p></div>
<div class="m2"><p>بی خدمت تو کرد طلب حشمت و آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معلوم بود که دانۀ در خوشاب</p></div>
<div class="m2"><p>غواص خردمند نجوید ز سراب</p></div></div>