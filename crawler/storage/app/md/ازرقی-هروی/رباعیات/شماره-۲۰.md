---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای رای تو با ضمیر گردون شد جفت</p></div>
<div class="m2"><p>پیدا بر تو هر چه فلک راست نهفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدح چو تویی چو من رهی داند گفت</p></div>
<div class="m2"><p>الماس خرد در سخن داند سفت</p></div></div>