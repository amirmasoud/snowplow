---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>ای گشته پراکنده سپاه و حشمت</p></div>
<div class="m2"><p>گرینده ندیمان و غریوان خدمت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر کوس و سپاه تو ز تیمار غمت</p></div>
<div class="m2"><p>خون می بارد ز دیده شیر علمت</p></div></div>