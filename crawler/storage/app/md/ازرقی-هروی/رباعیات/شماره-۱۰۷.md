---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>آن قوم کجا نزد تو پویند همی</p></div>
<div class="m2"><p>در جز و وز کل ز هر تو جویند همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل همه مهرتو بشویند همی</p></div>
<div class="m2"><p>تا می نکنی یقین چه گویند همی</p></div></div>