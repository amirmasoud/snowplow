---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>از خاک چمن بوی سمن می آاید</p></div>
<div class="m2"><p>وز ابر طراوتی بتن می آاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آتش عشق می فزاید در دل</p></div>
<div class="m2"><p>هر باد که از سوی چمن می آاید</p></div></div>