---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>ای عادت تو بوعده صادق بودن</p></div>
<div class="m2"><p>وی سیرت تو یار موافق بودن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر موجب این دو چیز نیکو که تراست</p></div>
<div class="m2"><p>جز بر تو حلال نیست عاشق بودن</p></div></div>