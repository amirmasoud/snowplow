---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تویی نور دل و شمع روان</p></div>
<div class="m2"><p>تا بی خبرم از تو ، نه پیدا نه نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی من تو بکام خویش ای جان جهان</p></div>
<div class="m2"><p>من بی تو چنانم که مبادی تو چنان</p></div></div>