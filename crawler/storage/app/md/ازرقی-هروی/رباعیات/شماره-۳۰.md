---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>گم بوده زتو جنت و کوثر یابد</p></div>
<div class="m2"><p>شاخ خرد از فکرت تو بر یابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طبع از نکت تو گنج گوهر یابد</p></div>
<div class="m2"><p>جان از سخن تو جان دیگر یابد</p></div></div>