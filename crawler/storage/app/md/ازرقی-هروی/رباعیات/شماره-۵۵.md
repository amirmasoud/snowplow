---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ملک تو ، شها ، درخت نو بود ببار</p></div>
<div class="m2"><p>وانگه اثر خزان برو کرد گذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون چو همی بشکفد از بوی بهار</p></div>
<div class="m2"><p>آن میوه شکفته خوشترای شاه ببار</p></div></div>