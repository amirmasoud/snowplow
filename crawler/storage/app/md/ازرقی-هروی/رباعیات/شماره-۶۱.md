---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>یک چند بدام عشق بودم بگداز</p></div>
<div class="m2"><p>باز این دلم آن گداز می جوید باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با این دل عشق بستۀ صحبت ساز</p></div>
<div class="m2"><p>عیشیست مرا تیره و کاریست دراز</p></div></div>