---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چون بد عهدی گشت از تو این عهد درست </p></div>
<div class="m2"><p>در سستی دست از تو چرا دارم سست ؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دست نشستمی ز تو روز نخست</p></div>
<div class="m2"><p>امروز بخون روی خود باید شست</p></div></div>