---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>دردا و دریغا که چنین در هوسی</p></div>
<div class="m2"><p>کردیم تن عزیز خس بهر خسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر غم رو گار خوردیم بسی</p></div>
<div class="m2"><p>از دست دل خویش ، نه از دست کسی</p></div></div>