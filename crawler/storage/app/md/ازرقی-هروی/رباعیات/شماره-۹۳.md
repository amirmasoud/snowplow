---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>ای فخر زمانه را ز پیوندی تو</p></div>
<div class="m2"><p>آدم شده محتشم ز فرزندی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین گونه برنج بنده خرسند شدی</p></div>
<div class="m2"><p>چون در خورداز روی خداوندی تو ؟</p></div></div>