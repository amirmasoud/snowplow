---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>عشن تو مرا توانگری آرد بر</p></div>
<div class="m2"><p>از دیده بلؤلؤ و ز رخسار بزر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشق توام عیش خوشست ، ای دلبر</p></div>
<div class="m2"><p>آری ز توانگری چه باشد خوشتر ؟</p></div></div>