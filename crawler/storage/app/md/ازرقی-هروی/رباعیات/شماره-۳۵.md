---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای مه ، بکف ابر زبون خواهی شد</p></div>
<div class="m2"><p>وی برگ سمن ، بنفشه گون خواهی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای رایت نیکویی ، نگون خواهی شد</p></div>
<div class="m2"><p>در چنشم مست آنکه تو چون خواهی شد</p></div></div>