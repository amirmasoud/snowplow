---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>در شهر هری عاشق زار تو منم</p></div>
<div class="m2"><p>با عشق تو یار پایدار تو منم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خو کرده بجور بی شمار تو منم</p></div>
<div class="m2"><p>بیچاره و در مانده بکار تو منم</p></div></div>