---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>ناگاه همی زدم من ، ای شمع و چراغ</p></div>
<div class="m2"><p>از شهر بباغ با دلی پر غم و داغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باغ ار چه بود جای تماشا و فراغ</p></div>
<div class="m2"><p>دوزخ بود ، ای نگار ، بی روی تو باغ</p></div></div>