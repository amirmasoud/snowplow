---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>یزدان خرد و کمال راه تو نهاد </p></div>
<div class="m2"><p>اجرام سپهر نیک خواه تو نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردون ز جمال پایگاه تو نهاد</p></div>
<div class="m2"><p>عالم عرض جوهر جاه تو نهاد</p></div></div>