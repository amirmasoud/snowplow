---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>گر خواهی ، ازین حشمت والا بمثل</p></div>
<div class="m2"><p>بر تارک خورشید نهی پای محل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مر جاه ترا خدای ما ، عزوجل</p></div>
<div class="m2"><p>جاوید رقم ز دست بر لوح ازل</p></div></div>