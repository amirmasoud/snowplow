---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>اندر خوبی ترا فزودست جمال</p></div>
<div class="m2"><p>در فبضۀ آن کمان ابروی تو خال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مشک ستاره ایست بر چرخ جلال</p></div>
<div class="m2"><p>کز غالیه در دو ظرف دارد و هلال</p></div></div>