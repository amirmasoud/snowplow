---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>مر جاه ترا بلندی جوزا باد </p></div>
<div class="m2"><p>درگاه ترا سیاست دریا بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رای تو ز روشنی فلک سیما باد</p></div>
<div class="m2"><p>خورشید سعادت تو بر بالا باد</p></div></div>