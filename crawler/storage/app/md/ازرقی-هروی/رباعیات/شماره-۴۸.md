---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>چون لعل کند سنان سر از خون جگر</p></div>
<div class="m2"><p>وز تیغ کبود تو بجنبد گوهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ز آب روان بود عدو را پیکر</p></div>
<div class="m2"><p>در آتش زخم تو شود خاکستر</p></div></div>