---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>گر عقل مکان گیر مصور بودی</p></div>
<div class="m2"><p>بر چهرۀ ملکت تو زیور بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور دانش را جنبش و محور بودی</p></div>
<div class="m2"><p>اندر فلک رای تو اختر بودی</p></div></div>