---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>آن شد که ترا رفت همی با ما ناز</p></div>
<div class="m2"><p>و آن شد که مرا بود بروی تو نیاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ناز تو و نیاز خویش ، ای پرساز</p></div>
<div class="m2"><p>بر سنگ زدیم و صبر کردیم آغاز</p></div></div>