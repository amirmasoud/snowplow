---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>زان گونه ز پولاد ترا دست بخست</p></div>
<div class="m2"><p>کاندر رگت آویخت چو ماهی درشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نادره بر گوشۀ جان باید بست</p></div>
<div class="m2"><p>الماس که الماس فرو برد بدست</p></div></div>