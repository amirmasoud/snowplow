---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>ناشاد مرا ، ای بت نوشاد ، مکن</p></div>
<div class="m2"><p>از داد خدا بترس و بیداد مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیکویی کن مرا ببد یاد مکن</p></div>
<div class="m2"><p>مر خصم مرا از غم من شاد مکن</p></div></div>