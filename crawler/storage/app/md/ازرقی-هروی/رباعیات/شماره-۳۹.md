---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>نوروز شکفته از لقای تو برند</p></div>
<div class="m2"><p>فردوس خجسته از رضای تو برند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنیاد درستی از وفای تو برند</p></div>
<div class="m2"><p>ارکان تمامی از بقای تو برند</p></div></div>