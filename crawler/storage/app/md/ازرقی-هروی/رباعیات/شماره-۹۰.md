---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>هر چند بدردم از دل محکم تو</p></div>
<div class="m2"><p>گیرم کم جان و دل ، نگیرم کم تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاهست کنم آنچه ترا کام و هواست</p></div>
<div class="m2"><p>یا نیست کنم جوانی اندر غم تو</p></div></div>