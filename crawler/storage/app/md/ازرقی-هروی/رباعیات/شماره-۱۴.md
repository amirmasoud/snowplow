---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ایام درشت رام تاج الملکست</p></div>
<div class="m2"><p>جان ابدی بنام تاج الملکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرام جهان قوام تاج الملکست</p></div>
<div class="m2"><p>گردنده فلک غلام تاج الملکست</p></div></div>