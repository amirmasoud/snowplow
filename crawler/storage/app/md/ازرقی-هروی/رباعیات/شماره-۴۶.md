---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>مرد آنکه شدن را بشتاب آراید</p></div>
<div class="m2"><p>نه همچو زنان رخ بخضاب آراید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مرد رهی امید را جفت مگیر</p></div>
<div class="m2"><p>کامید چو زن بستر خواب آراید</p></div></div>