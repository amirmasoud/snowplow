---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>ای سخن زیر دست خامۀ تو</p></div>
<div class="m2"><p>عقد لؤلؤست نظم نامۀ تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق در سایۀ خرد باشد</p></div>
<div class="m2"><p>خرد اندر حصار سایۀ تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مایۀ صد ادیب بتراشند</p></div>
<div class="m2"><p>ار بکاوند دست و جامۀ تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کامۀ من بفضل خویش بجوی</p></div>
<div class="m2"><p>که منم زنده بهر کامۀ تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل در اندام من نیاساید</p></div>
<div class="m2"><p>گر برنجد بنان ز خامۀ تو</p></div></div>