---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>در سپهر دولت آمد کامجوی و کامران</p></div>
<div class="m2"><p>از شکار خسروی آن آفتاب خسروان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان داد و همت ، آفتاب تاج و تخت</p></div>
<div class="m2"><p>نور جان میر جغری ، شمع شاه الب ارسلان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مفخر سلجوقیان ، شمشیر میرالمؤمنین</p></div>
<div class="m2"><p>شمس دولت ، زین ملت ، کهف امت ، شه طغان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خون و آتش در بلارگ ، زهر و باد اندر خدنگ</p></div>
<div class="m2"><p>کوه و گردون در جنیبت ، ابر و دریا در سنان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوک زوبین خسته اندر نافۀ آهوی مشک</p></div>
<div class="m2"><p>زهر پیکان رانده اندر زهرۀ شیر ژیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که او نخجیرگاه خسرو ایران بدید</p></div>
<div class="m2"><p>از شگفتی های عالم نیست طبعش را بیان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سپهر کوه پیکر هر طرف پر گنده بود</p></div>
<div class="m2"><p>لالۀ شمشاد پوش و گلبن پروین نشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جعدشان بر سوسن سیمین فکنده عودتر</p></div>
<div class="m2"><p>زلفشان بر لالۀ رنگین نهاده ضیمران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آهوان خیمگی هر ساعتی بر کوه و دشت</p></div>
<div class="m2"><p>بر کشیدندی بروی شیر گردنکش فغان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خاک چون اشکال اقلیدس شد از شاخ گوزن</p></div>
<div class="m2"><p>در بر هر شکل حرفی از خدنگ جان ستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنگ باز اندر هوا و شاخ رنگ اندر زمین</p></div>
<div class="m2"><p>این معلق ، آن مجعد ، این ز مشک ، آن زعفران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر زمبن چشم گوزنان ، راست گویی ضف زده</p></div>
<div class="m2"><p>اختران جزع پیکر در عقیقن آسمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روی آهو پیکر پروین نمود اندر زمین</p></div>
<div class="m2"><p>وز هلال منخسف بر پیکر پروین نشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خامۀ مانی تو گفتی بر زمین بیرنگ زد</p></div>
<div class="m2"><p>صد هزاران صورت رنگین بآب ناردان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر گهی کان آفتاب خسروان از بهر صید</p></div>
<div class="m2"><p>در بر افگندی بلارگ ، در زه آوردی کمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گورو نخجیر و گوزن از روی دشت و تیغ و کوه</p></div>
<div class="m2"><p>رو کشیدندی بهامون ، کاروان در کاروان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مر تفاخر را ، بتحریض از گشاد زخم او </p></div>
<div class="m2"><p>زود می خوردند آهن ، خوش همی دادند جان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه از زخم گشاد دیگران بی جان شدی</p></div>
<div class="m2"><p>زنده گشتی از غبار اسب او اندر زمان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از نسیم خلق او بر سنگ سخت و خار خشک</p></div>
<div class="m2"><p>سبز شد نسرین و سوسن ، شاخ زد کافور و بان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سایۀ شبدیز او بر هر زمینی کاوفتاد</p></div>
<div class="m2"><p>صورتی شد با رکاب و پیکری شد با عنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p> ای شهنشاهی ، که پیش تاج گردون سای تو</p></div>
<div class="m2"><p>در بلندی چشمۀ خورشیدباشد ناتوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا ندیدم تیغ و تیرت را ندانستم درست</p></div>
<div class="m2"><p>کآفت از بلغار خیزد ، فتنه از هندوستان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زهره چون لخت زمرد ، صدره از بیم تو شیر</p></div>
<div class="m2"><p>برگسستست از جگر ، بیرون فکندست از دهان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سنگ و آهن را بدوزی ، چون بیندازی خدنگ</p></div>
<div class="m2"><p>چرخ و دریا را بسوزی ، چون بجنبانی سنان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کوه بالا گرز رومی بشکنی از زور دست</p></div>
<div class="m2"><p>پیل پیکر خنگ،ختلی بگسلی در زیر ران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مر عدو را از خیال رمح افعی شکل تو</p></div>
<div class="m2"><p>مغز و تارک مار و افعی گردد اندر استخوان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر تنی چندان روان یابد که شمشیر تو یافت</p></div>
<div class="m2"><p>همچو خضر اندر دو گیتی زنده ماند جاودان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پرنیان کردار فولادی که پیش زخم او</p></div>
<div class="m2"><p>روز کین بر آهن و پولاد خندد پرنیان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آتش ارواح لمع و جوهر نصرت عرض </p></div>
<div class="m2"><p>ابر پیروزی سرشک و اختر هیجا قران</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کان بیجاده است گویی در نقاب لاژورد</p></div>
<div class="m2"><p>صد هزاران چشمۀ سیماب در اجزای آن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیست نادر سنگ مغناطیس اگر آهن کشد</p></div>
<div class="m2"><p>آهن شمشیر خسرو هست مغناطیس جان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آب و آتش را تو پنداری مرکب کرده اند</p></div>
<div class="m2"><p>آب یاقوتین سرشک و آتش مرجان دخان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>با چنین تیغی ، خداوندا ، چو در میدان شوی </p></div>
<div class="m2"><p>بر زمرد معصفر روید ، ز لؤلؤ زعفران</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خوار و آسان آری اندر فکرت ، ای شه ، مرد را</p></div>
<div class="m2"><p>کشتن دیو سپید و قصۀ مازندران</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>آفرین بر مرکبی . کز ماه پیکر نعل او</p></div>
<div class="m2"><p>جرم خاک اندر سپهر نیلگون گیرد مکان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون بپیچد ، چون بتازد ، راست پنداری که هست</p></div>
<div class="m2"><p>استخوان اندر تن او حلقه های خیزران</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چون برانگیزد بهیجا آتش تحریک او</p></div>
<div class="m2"><p>همچو موم اندر فروزد غیبۀ بر گستوان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>در میان نقش خاتم ره برد مانند موم</p></div>
<div class="m2"><p>بگذرد از چشمۀ سوزن چو تار ریسمان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تیزرو همچون سپهر و بارکش همچون زمین</p></div>
<div class="m2"><p>راه دان همچون یقین و دور رو همچون گمان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای خداوندی ، که از یک صلت تو روز بزم</p></div>
<div class="m2"><p>شرم دارد گنج باد آورد و گنج شایگان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کاردار و عامل تست ، ای خداوند زمین </p></div>
<div class="m2"><p>در زمین هند رای و در بلاد ترک خان </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>هر چه در بالا و پهنای جهان جنبنده ایست</p></div>
<div class="m2"><p>نیستند از خویشتن بی مهر تو هم داستان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بندۀ مهر تو از جان خدمتی ساز دهنی</p></div>
<div class="m2"><p>خرم و زیبا و رنگین چون شکفته بوستان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>داستانی طرفه ، کز اخبار و از اشکال او</p></div>
<div class="m2"><p>بر گشاید طبع دانا را هزاران داستان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پر طاوسست ، بر وی بسته مرواریدتر</p></div>
<div class="m2"><p>شکل پروینست ، در وی رسته برگ ارغوان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از معانی اندرو پر گنده لختی گفته ام</p></div>
<div class="m2"><p>از ره فرهنگ و جهل و از ره سود و زیان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر یپرد ختن خداوند جهان فرمان دهد</p></div>
<div class="m2"><p>بنده اندر دانش از اندیشه بگذارد روان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خدمتی سازم ، که جان مرد دانش پیشه را</p></div>
<div class="m2"><p>چون بقای شاه جاویدان بماند در جهان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>قصۀ منثور حاشاکی بود باریک و پست </p></div>
<div class="m2"><p>گوهری گردد چو منظوم اندر آری برزبان</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از قصص هایی که در شهنامه پیدا کرده اند</p></div>
<div class="m2"><p>نظم فردوسی بکار آید ، نه رزم هفت خوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا نگردد پیکر کوه گران باد سبک</p></div>
<div class="m2"><p>تا نگردد گوهر باد سبک کوه گران</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تا برخشد لاله در نوروز مه بر کوهسار</p></div>
<div class="m2"><p>تا بخندد گل بهنگام بهار از گلستان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کام ران و ملک ساز و شادباش و دیرزی</p></div>
<div class="m2"><p>در نعیم بی زوال و در بقای بی کران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>رایت ملک تو بگذشته سپهر اندر سپهر</p></div>
<div class="m2"><p>مرکب جاه تو افگنده عنان اندر عنان</p></div></div>