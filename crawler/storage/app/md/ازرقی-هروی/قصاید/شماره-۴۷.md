---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>المنه لله که خورشید خراسان</p></div>
<div class="m2"><p>از برج شرف گشت دگر باره درخشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>المنه لله که آراست دگر بار</p></div>
<div class="m2"><p>دیوان خراسان بسزاوار خراسان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>المنه الله که از کشتی عصمت</p></div>
<div class="m2"><p>شد نوح نبی بی خطر از آفت توفان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>المنه الله که یوسف بامارت</p></div>
<div class="m2"><p>بنشست و عدو گشت اسیر چه خذلان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دیدۀ دینست خردمندی او نور</p></div>
<div class="m2"><p>در پیکر ملکست هنرمندی او جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محتاج بزرگان بتو چون دهر بخورشید</p></div>
<div class="m2"><p>ممتاز کریمان بتو چون کشت بباران</p></div></div>