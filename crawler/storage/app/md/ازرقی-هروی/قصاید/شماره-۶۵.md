---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>طالع پیروز بختی ، مایۀ نیک اختری</p></div>
<div class="m2"><p>آسمان کامگاری ، آفتاب سروری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسم دانی ، ملک سازی ، رزم جویی ، خسروی</p></div>
<div class="m2"><p>پیشوای روزگاری ، پادشاه کشوری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شمس دولت ، زین ملت کهف امت ، شه طغان</p></div>
<div class="m2"><p>آنکه نیکو همت او گشت از بدها بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن خداوندی که جمشید دگر در حشمت اوست</p></div>
<div class="m2"><p>امر او چون امر جمشیدست بر عالم جری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای شهنشاهی که جمشیدی ، از آن معنی که هست</p></div>
<div class="m2"><p>آدمی فرمان بر تو ،همچو دیو و چون پری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نادران ملک بودند اردوان و اردشیر</p></div>
<div class="m2"><p>اردوان دیگری ، یا اردشیر دیگری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کمان در دست گیری مایۀ سعدی ، شها</p></div>
<div class="m2"><p>مایۀ سعدست چون در فوس باشد مشتری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خسروی را همچو شخصی ، کامگاری را دلی</p></div>
<div class="m2"><p>کامگاری را چو جانی ، خسروی را پیکری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فرق شاهی را چو عقلی ، نور دانش را دلی</p></div>
<div class="m2"><p>ایمنی را همچو حصنی ، رستگاری را دری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کارساز سعد چرخی ، کیمیای دولتی</p></div>
<div class="m2"><p>بر سر اقبال تاجی ، بر تن دولت سری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مایۀ اثبات کامی ، عین نفی اندهی</p></div>
<div class="m2"><p>مر سخارا چون سرشتی ، مر وفارا گوهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیر همت پادشاهی ، شیر هیبت خسروی</p></div>
<div class="m2"><p>شیر گیری ، صف پناهی ، ببر خویی ، صفدری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فکرت ما درخور تو چون ستاید مر ترا ؟</p></div>
<div class="m2"><p>ز آنکه تو در فکرت ما از ستایش برتری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در جهان گر وحی جایز بودی اندر وقت ما</p></div>
<div class="m2"><p>بر حقیقت مر ترا جایز بدی پیغمبری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرز سد اسکندر رومی چنان معروف شد</p></div>
<div class="m2"><p>کمترین فرمان تو سدی بود اسکندری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیزه از بیم تو لرزانست تا با نیزه ای</p></div>
<div class="m2"><p>خنجر از سهم تو ترسانست تا با خنجری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای شهنشاه ، ای خداوند ای کریم بن الکریم</p></div>
<div class="m2"><p>جان من داند که اندر نور جانم زیوری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عالم علمی ولیکن پادشاه عالمی</p></div>
<div class="m2"><p>اختر فضلی ولیکن کارساز اختری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدر دیهیم و نگینی ، جاه ملک و حشمتی</p></div>
<div class="m2"><p>فخر شمشیر و سنانی ، عز تخت و افسری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خداوندی ،که ایامت نماید بندگی</p></div>
<div class="m2"><p>وی شهنشاهی ، که افلاکت کند فرمان بری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر بر آزادان بر افتد ، شهریارا ، عقدبیع</p></div>
<div class="m2"><p>چون من و بهتر زمن در ساعتی سیصد خری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پس ز اقلیمی باقلیمی بدست و خط خویش</p></div>
<div class="m2"><p>بنده را فرمان دهی و اندر سخن یادآوری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از کدامین چشم ، شاها ، از تفاخر بنگرم ؟</p></div>
<div class="m2"><p>کین نه قدر چون منی باشد چو نیکو بنگری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عنصری در خدمت محمود دایم فخر کرد</p></div>
<div class="m2"><p>زانکه دادش پاره ای در شعر فتح نودری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خواست گفتن : من خدایم در میان شاعران</p></div>
<div class="m2"><p>کز خداوندم چنین فخری ، رسید از شاعری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندرین میدان فخر اکنون بتو مر بنده راست</p></div>
<div class="m2"><p>گو درین میدان فخر آی ار تواند عنصری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای خداوندی ، که اندر خاور و در باختر</p></div>
<div class="m2"><p>بر چو تو شاهی نتابد آفتاب خاوری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای شهنشاهی ، که اندر روزبار و روز رزم</p></div>
<div class="m2"><p>از سیاست موج دریایی و سوزان آذری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از چو تو شاهی اگر لافی زنم از افتخار</p></div>
<div class="m2"><p>نیست لافی بر گزاف و نیست فخری سرسری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا سپهر چنبری هرگز نگیرد طبع خاک</p></div>
<div class="m2"><p>تا نپاید جرم خاک اندر سپهر چنبری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملک بادت بی قیاس و عمر بادت بی کران</p></div>
<div class="m2"><p>تا زعمر و ملک خویش اندر جوانی برخوری</p></div></div>