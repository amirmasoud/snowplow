---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>بمژده خواستی آن نور چشم و راحت جان</p></div>
<div class="m2"><p>بر من آمد پروین نمای و ماه نشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهفته انجم او در عقیق عنبر بوی</p></div>
<div class="m2"><p>شکسته سنبل او در سهیل مشک افشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درست گفتی بر مه بنفشه کاشت همی</p></div>
<div class="m2"><p>شکسته سنبل آن آفتاب ترکستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزیر سنبل مشکین او همی رفتند</p></div>
<div class="m2"><p>هزار دل بخروش و هزار جان بفغان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب و میانش تو گفتی شهاب بود و سهیل</p></div>
<div class="m2"><p>یکی زرنگ چنین و یکی ز شکل چنان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهاب دیدی جوزا در آن شهاب پدید ؟</p></div>
<div class="m2"><p>سهیل دیدی پروین در آن سهیل نهان ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهفته لالۀ رنگین او بتاب کمند</p></div>
<div class="m2"><p>نموده نرگس مشکین او بزیر کمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یکی ز مشک و ز عنبر ، یکی ز شیر و شبه</p></div>
<div class="m2"><p>یکی ز سوسن و نسرین ، یکی ز عنبر و بان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پدید کرد ثریا و ماه چون بنمود</p></div>
<div class="m2"><p>سمن ز سنبل سیراب و لاله از مرجان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بهر مژده رخش ساخت چون ستاره و ماه</p></div>
<div class="m2"><p>پدید کرد سمن زار گرد لاله ستان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه گفت ؟ گفت : اگر رامش دل تومنم</p></div>
<div class="m2"><p>برامش دل من جان بیار و مژده ستان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیار مژده که نو عز و خلعتش فرمود</p></div>
<div class="m2"><p>خدایگان ترا ، شهریار شاه جهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شجاع دولت پاینده ، سعد ملک حسن</p></div>
<div class="m2"><p>امیر شاه عجم ، میر غور و غرجستان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سخن سرای و منقش قصیده ای اندیش</p></div>
<div class="m2"><p>بفهم کردن دشوار و خواندن آسان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گزین خاطر خود نکته های رنگین گوی</p></div>
<div class="m2"><p>سزای مدحت او لفظهای چابک ران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو رایض سخنی ، مرکب تفکر را</p></div>
<div class="m2"><p>عنان عقل فرو گیر و بر گزاف مران</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سخن تمام کن و سوی آفتاب فرست</p></div>
<div class="m2"><p>بدو سپار و بگویش که : پیش میر بخوان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کزین تفاخر قدرت بآفتاب رسد</p></div>
<div class="m2"><p>ز فخر عار نماید ز جنبش دوران</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عجب مدار ، که آن مهتر سپهر آیین</p></div>
<div class="m2"><p>هزار بنده فزون دارد آفتاب توان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدست همت با آسمان کند بازی</p></div>
<div class="m2"><p>بپای قدرت سازد ز ماه شادروان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمونه ایست ز آثار رای او پروین</p></div>
<div class="m2"><p>نشانه ایست ز اجزای قدر او سرطان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بهر زخم جگر گوشۀ مخالف او</p></div>
<div class="m2"><p>بزهر تیز کند اژدها سر دندان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زبیم خامۀ چون خیزران او شب و روز</p></div>
<div class="m2"><p>چو خیزران بود اندر تن عدو ستخوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بنام خشمش روباه ماده در گسلد</p></div>
<div class="m2"><p>ز شیر پنجه و ساعد ، ز پیل گردن و ران</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ایا سپهر هنر را ستارۀ سیار</p></div>
<div class="m2"><p>و یا جهان خرد را طبایع و ارکان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هنر بطبع تو جوید ببرتری بنیاد</p></div>
<div class="m2"><p>خرد ز رای تو گیرد بمردمی سامان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز طبع و خشم تو آب روان و آتش تیز</p></div>
<div class="m2"><p>ز لفظ و حلم تو خاک گران و باد بزان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دو نایبند فلک رای و آفتاب هنر</p></div>
<div class="m2"><p>دو چاکرند فزونی تن و بزرگی جان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر شک خصم ترا گر صفت کنم بدرر</p></div>
<div class="m2"><p>شود دهان صدف جای آتشین پیکان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عجب نباشد اگر زر زبهر جود ترا</p></div>
<div class="m2"><p>نگار گیرد و دینار گردد اندر کان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر غم ابر همی موج دست فرخ تو</p></div>
<div class="m2"><p>بماه دی گل سوری برآورد از سندان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چنان شوی تو ازین پس که ابر ژاله زند</p></div>
<div class="m2"><p>مدیح دست تو باشد بابر در باران</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر سپهر روان با ستاره جنگ کند</p></div>
<div class="m2"><p>ز حشمت تو زره سازد و ز خامه سنان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خدایگانا ، فرخنده و مبارک باد</p></div>
<div class="m2"><p>خجسته خلعت خسرو بردار سلطان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سرای پردۀ میری و نوبت از همه خلق</p></div>
<div class="m2"><p>ترا سزد ، که سزا بینمت بصد چندان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه دیر پاید تا شاه سازد از پی تو</p></div>
<div class="m2"><p>سرای پرده ز خورشید و نوبت از کیوان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نشست گاه تو باشد بشرق در بلغار</p></div>
<div class="m2"><p>شکارگاه تو باشد بضرب در عمان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>صهیل اسب تو گیرد نوای نارایین</p></div>
<div class="m2"><p>فروغ چتر تو یابد هوای ترکستان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فسار مرکب سازی ، بقهر ، رایت رای</p></div>
<div class="m2"><p>پلاس آخر سازی ، بجنگ ، خیمۀ خان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بچرم شیر ببندی دو دست شیر نژند</p></div>
<div class="m2"><p>بیشک پیل بکوبی دو پای پیل دمان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ایا معانی مدحت بلندتر ز فلک</p></div>
<div class="m2"><p>و یا شمایل جودت رونده تر ز گمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>حدیث شاعر فالی بود قضا پیوند</p></div>
<div class="m2"><p>که فال و قصه بهم بسته اند جاویدان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر آن حدیث که بر لفظ شاعران گذرد</p></div>
<div class="m2"><p>ز روزگار بیابی مثال آن بعیان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خدایگانا ، من بستۀ هوای توام</p></div>
<div class="m2"><p>بجان و دیده بقای تو خواهم از یزدان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بجان تو که ز انفاس خود مدیح ترا</p></div>
<div class="m2"><p>مشاطه وار کنم پر نگار ده دیوان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همیشه تا نبود باد جفت خاک نژند</p></div>
<div class="m2"><p>همیشه تا نشود آب شکل کوه گران</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بقا و عز خداوندی تو دایم باد</p></div>
<div class="m2"><p>ز تیر رمح شده قد دشمنت چو کمان</p></div></div>