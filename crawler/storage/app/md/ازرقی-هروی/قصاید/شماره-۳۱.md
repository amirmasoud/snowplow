---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ز موج دریا این آبر آسمان آهنگ</p></div>
<div class="m2"><p>کشیر رایت پروین نمای بر خرچنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشعبد آمد پروین او ، که در دل کوه</p></div>
<div class="m2"><p>چو وهم مرد مشعبد همی نماید رنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سپهر رنگین زو گشت کوه سیم اندود</p></div>
<div class="m2"><p>ستاره وار روان بر سپهر رنگین رنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سحاب گویی در منضدست بکیل</p></div>
<div class="m2"><p>شمال گویی عود مثلثست بتنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شکفته شاخ سمن گرد بوستان گویی</p></div>
<div class="m2"><p>همی بر آرد در ثمین هزار از سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دهان ابر بهاری همی فشاند در</p></div>
<div class="m2"><p>گلوی مرغ نو آیین همی نوازد چنگ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شاخهای سمن مرغکان باغ پرست</p></div>
<div class="m2"><p>بلحن باربدی بر کشیده اند آهنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دهان لاله تو گویی همی که نوش کند</p></div>
<div class="m2"><p>بروی سبزۀ زنگار گون نبید چو زنگ</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ابر فندق سیمین بر آید آن ریزد</p></div>
<div class="m2"><p>بر آرد از دل پیروزه شکل سیمین زنگ</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشعبدیست که بر خرد مهره های رخام</p></div>
<div class="m2"><p>بحقهای بلورین همی کند بیرنگ</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زمین ز زخم صبا شد نگارخانۀ چین</p></div>
<div class="m2"><p>چمن ز شاخ سمن شد بهار خانۀ گنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکفته لاله تو گویی همی که عرضه کنند </p></div>
<div class="m2"><p>بزیر سایۀ رایات سرخ لشکر زنگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بزخم نازده ، برق از مسام سنگ سیاه</p></div>
<div class="m2"><p>همی فشاند خون چون سنان شاه بجنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گزیده شمس دول ، شهریار کهف امم</p></div>
<div class="m2"><p>طغانشه بن محمد طبایع فرهنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رکاب مرکب او بر کرانۀ خورشید</p></div>
<div class="m2"><p>زبان نیزۀ او در دهان هفت اورنگ</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخاوت وهمم وکلک و طبع روشن او</p></div>
<div class="m2"><p>ز چرخ و انجم و افلاک و کوه دارد ننگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زرشک زین پلنگش ، زچرخ ، بدر منیر</p></div>
<div class="m2"><p>سیاه وزرد نماید همی چو پشت پلنگ</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هلاک دشمن او را ، ز هند و از بلغار</p></div>
<div class="m2"><p>شکنج وافعی روید بجای رمح وخدنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نماید از دل شاه و بقا و همت او</p></div>
<div class="m2"><p>زمانه کوته وافلاک خرد و دریا تنگ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدان سبب که ورا بندگان ز چین آرند</p></div>
<div class="m2"><p>بشبه مردم روید بحد چین سترنگ</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایا ز گوشۀ تاج تو چرخ جسته علو</p></div>
<div class="m2"><p>ویا ز پایۀ تخت تو خاک برده درنگ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تویی که پیش تو شیر ژیان چنان باشد</p></div>
<div class="m2"><p>که پیش شیر ژیان دست بسته رو به لنگ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدنگ پر مکش اندر کمان ، که گاه کشاد</p></div>
<div class="m2"><p>زمین ندارد در خورد سیر او فرسنگ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چنان رود ، که ز آسیب نصل خون آلود</p></div>
<div class="m2"><p>کند کنارۀ گردون چو نار گون نارنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هزار لشکر داری ، که هر یکی زیشان</p></div>
<div class="m2"><p>فزون ترند ز دیو پلید و از ارژنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زمانه سیرت و دریا نهیب و چرخ توان</p></div>
<div class="m2"><p>سهیل رایت و مه چتر و مشتری فرهنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو رستم آسا در چنگ تیغ کینه کشند</p></div>
<div class="m2"><p>بچهر دیو سپید اندر افکنند آژنگ</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بیک اشارت تو در زمان گشاده کنند</p></div>
<div class="m2"><p>ز هند تا بلغار و ز روم تا کیرنگ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تویی که ناز مخالف کنی بنیزه نیاز</p></div>
<div class="m2"><p>تویی که شهد معادی کنی بکینه شرنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سنان خصم ترا گر ستاره وصف کنم</p></div>
<div class="m2"><p>ستاره در روش آسمان بر آرد زنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدف چو بیند تیغ نهنگ وار ترا</p></div>
<div class="m2"><p>فرو رود گهر از حلق او بکام نهنگ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بدان امید که ارواح دشمنت در رزم</p></div>
<div class="m2"><p>شود چو گوهر تیغ تو ارغوانی رنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهاب را بکمان بر نهی چو چوبۀ تیر</p></div>
<div class="m2"><p>سپهر را بحنا در کشی چو حلقۀ تنگ</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زمان زمان بفلک بر ، سهیل مرجان جرم</p></div>
<div class="m2"><p>ز سیر و ز حرکت جمله باز دارد چنگ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مگر که شاه ز بهر نگین خاتم ملک</p></div>
<div class="m2"><p>بدست همت عالی بدو کند آهنگ</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر چه خاتم ملک سپهر صحن ترا</p></div>
<div class="m2"><p>ستارۀ فلکی به بود ز پارۀ سنگ</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مکن ، شها، که گر این پایه او بدست آرد</p></div>
<div class="m2"><p>بر آفتاب کند پرده های گردون تنگ</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همیشه تا نرود بر سپهر چشمۀ آب</p></div>
<div class="m2"><p>همیشه تا نبود در ستاره چوب زرنگ</p></div></div>
<div class="b" id="bn39"><div class="m1"><p> موافق تو کند در سعود ناز و طرب</p></div>
<div class="m2"><p>مخالف تو کند در عنا غریو و غرنگ</p></div></div>