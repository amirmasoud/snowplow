---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>میرود سنجابگون بر چرخ از دریا بخار</p></div>
<div class="m2"><p>می کند پر حواصل بر سر عالم نثار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرکز خاک آهنین شد پاک و مستولی شدست </p></div>
<div class="m2"><p>بر زر گردون سرب سیما سحاب سیم بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بود از سیم پشت هر کسی گرم ، از چه رو</p></div>
<div class="m2"><p>عالمی لرزان شدند از بیم او سیماب وار ؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرخ چرخه ، ابر پنبه ، رشتۀ باران کناغ</p></div>
<div class="m2"><p>دوک ریسی طرفه پیش آورد زال روزگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی قرص آفتاب از ابر میگردد نهان</p></div>
<div class="m2"><p>همچو قرص گرم مانده از بر سنگین تغار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مارو کژدم تا ز سرما در زمین پنهان شدند</p></div>
<div class="m2"><p>آب دارد نیش کژدم باد دارد زهر مار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پس کنون آن به که دارد رنگ رخسار تذرو</p></div>
<div class="m2"><p>چون ز ابر فاخته گون شد حواصل کوهسار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کوه اگر چه چون حواصل شد از آن غمگین مباش</p></div>
<div class="m2"><p>کین حواصل زود خواهد گشت طاووس بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب از بس عزیزی نازها در سر گرفت</p></div>
<div class="m2"><p>می کشد بیچارگان را در فراغ انتظار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز آفتاب آسمان کر دست ما را بی نیاز</p></div>
<div class="m2"><p>یک نظر از آفتاب جود مخدوم کبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مخلص این جا کرده بودم ختم بازار سخن</p></div>
<div class="m2"><p>عقل را گفتم : مشو کاهل سخن ، معنی بیار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل گفتا : آرمت از باده و آتش سخن</p></div>
<div class="m2"><p>کاندرین سرما جزین ناید پسند هوشیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باده ای باید که اندازد بانجم پر شعاع</p></div>
<div class="m2"><p>آتشی باید که افروزد بگردون بر شرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از شعاع آن یکی رخسارها یاقوت رنگ</p></div>
<div class="m2"><p>وز شرار این دگر یاقوت ریزان بی شمار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی آن این یکی را گشته جامه از بلور </p></div>
<div class="m2"><p>وز پی این آن دگر را گشته از آهن حصار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن یکی مر شاخ گل را خاک سازد در زمان</p></div>
<div class="m2"><p>وین دگر بر چهره اندر حال گل آرد ببار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ما در آن تاک و کرده خویشتن آنرا غذا</p></div>
<div class="m2"><p>مادر این سنگ و پرورده مر او را در کنار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هم بدان نسبت که باشد ظاهر هر نار نور</p></div>
<div class="m2"><p>باده نور ظاهرست و مضمر اندر نور نار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می بآتش گفت روزی : هست نورت یاردود</p></div>
<div class="m2"><p>گفت آتش : لذت تو هست هم جفت خمار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باده گفتا : من زروی دلبران دارم صفت</p></div>
<div class="m2"><p>گفت آتش : من زرای سر کشان دارم عیار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باده او را گفت : هر جسمی که یابی تو خوری</p></div>
<div class="m2"><p>آتش او را گفت : بهتر جسم خواراز عقل خوار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باده گفتا:من مرکب گشته ام از چار طبع</p></div>
<div class="m2"><p>چار یک باشی زنی ، چون تو یکی باشی ز چار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت آتش : راست گفتی ، لیک میدانی منم</p></div>
<div class="m2"><p>در تو آن رکن حرارت کز تو آن آید بکار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عقل گفتا : هر دوان باشید و مکنید این لجاج</p></div>
<div class="m2"><p>من بگویم یک سخن ، باید بدین کرد اختصار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فخر می جویید هر دو ، بررسم امروز من</p></div>
<div class="m2"><p>در حضور هر دو این معنی ز اصل افتخار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خواجۀ دنیا ، ضیاءالدین نظام ملک شاه</p></div>
<div class="m2"><p>آنکه فضلش هست در آفاق فضل کردگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنکه تا بر چرخ خواهد بود انجم را مسیر</p></div>
<div class="m2"><p>بود خواهد بروجود او ممالک را مدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر چه دارد هم وزیری و هم اسم خواجگی</p></div>
<div class="m2"><p>هست در فرمان و معنی پادشاه کامگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر نباشد بهر بذلش زر برون ناید ز کان</p></div>
<div class="m2"><p>ور ندارد عشق بزمش گل برون ناید ز خار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>صحن دیوان کفایت آنکه دیوان نام یافت</p></div>
<div class="m2"><p>مثل او هرگز نبیند در همه عالم سوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صدر اورا فی المثل گر آسمان خوانی ز قدر</p></div>
<div class="m2"><p>فخر آرد آسمان و صدر او زین نام عار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسکه خلق آسوده شد در سایۀ انعام او</p></div>
<div class="m2"><p>چرخ خواند هر زمانش آسمان سایه دار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دهشت آمد خلق را از رای او در اصطناع</p></div>
<div class="m2"><p>عزت آمد چرخ را از امر او در اقتدار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمنان مملکت را کلک او مقهور کرد</p></div>
<div class="m2"><p>همچو در عهد نبی مر کافران را ذوالفقار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هر که بی فرمان او یک دم قلم گیرد بدست</p></div>
<div class="m2"><p>سالها دستش بود بی کار چون دست چنار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از همه کاری که جوید باد در دست آیدش</p></div>
<div class="m2"><p>بلکه از آن باد افتد در میان خاک خوار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حاسدان ار معتبر بودند دیدم جایشان</p></div>
<div class="m2"><p>احمق آن باشد که نکند جایشان را اعتبار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ای خداوند،آشنای خدمت در گاه تو</p></div>
<div class="m2"><p>گشت اندر هر زمان نامش از ین پس بختیار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر که چون او باشد اندر خدمت و اخلاص تو</p></div>
<div class="m2"><p>از میان جان شود چون بختیارش بخت یار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روز چند از غفلت ار بنده بخدمت کم رسید</p></div>
<div class="m2"><p>گردش ایام کردش از حوادث دل فگار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پیش از این بودی چو گل در مجلس تو تازه روی</p></div>
<div class="m2"><p>چون بنفشه کرد چرخش سرنگون وسوکوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یار دیگر بر سر ناساز گاری کی شود</p></div>
<div class="m2"><p>دهر ؟ گر رای تو شد با بندۀ تو ساز گار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بس بود تنبیه او را اینقدر گر گوییش :</p></div>
<div class="m2"><p>دست بیداد وتعرض از فلانی بازدار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تنگدل ماندست بنده کاندر ایام چنین</p></div>
<div class="m2"><p>لشگر آید همره رایات فرخ شهریار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آشکارا رنگ حال خویش نتواند نمود</p></div>
<div class="m2"><p>گر چه بعضی هست بر رای رفیعت آشکار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>جز تن لاغر ندارد در جهان دستور خاص</p></div>
<div class="m2"><p>جز دل غمگین ندارد در زمین دستور بار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>با چنین برگ و نوا اندر زمستانی چنین</p></div>
<div class="m2"><p>گر سفر خواهد شد از بنده خدایا زینهار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خودکسی زابنای جنس من بپیش تخت شاه</p></div>
<div class="m2"><p>روزوشب اندر سفرها بندگی دارد شعار؟</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا تواند کرد خدمت ؛ بایدش زین به مثال</p></div>
<div class="m2"><p>شغل رفتن چون بدارد ، بایدش به زین یسار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>این سخن گر پیش تو یابد محل استماع</p></div>
<div class="m2"><p>اصطناع تو کند کارش بزودی چون نگار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>با اشارات تو کردند انجم وافلاک ختم</p></div>
<div class="m2"><p>در زمانه امرو نهی و حل و عقد و گیر ودار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>باد یا اهل هنر را آفتاب دستگیر</p></div>
<div class="m2"><p>وندرین معنی بماند هر قراری پایدار</p></div></div>