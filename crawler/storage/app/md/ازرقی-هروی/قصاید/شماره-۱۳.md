---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>همایون جشن عید و ماه آذر </p></div>
<div class="m2"><p>خجسته باد بر شاه مظفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امیر انشاه بن قاورد جغری</p></div>
<div class="m2"><p>جمال دین و دین را پشت و یاور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداوندی ، کجا کوته نماید </p></div>
<div class="m2"><p>بپیش خطی او خط محور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر خورشید بودی دست رادش</p></div>
<div class="m2"><p>شدی جرم زمین یاقوت احمر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زمین باران جودش گر بیابد </p></div>
<div class="m2"><p>بجای سبزه روید از زمین زر </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدربند بجستان آنچه او کرد</p></div>
<div class="m2"><p>مثالی کرده بد حیدر بخیبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حنا و کوهۀ زین داشت شش ماه </p></div>
<div class="m2"><p>بجای خوابگه بالین و بستر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین شش مه زمانی برنیاسود</p></div>
<div class="m2"><p>ز دار و گیر پیلان معسکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگرد اندر همی شد مهر پنهان</p></div>
<div class="m2"><p>بخون اندر همی زد چرخ چنبر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بانگ کوس غران چشم کودک</p></div>
<div class="m2"><p>همی احوال شد اندر رحم مادر </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زبیم جان همی تن کرد پنهان </p></div>
<div class="m2"><p>چو دراج از پس صید غضنفر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زمین دریای موج افکن شد از خون</p></div>
<div class="m2"><p>درو کشتی سوار و کشته لنگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اجل بازو زنان هرسو همی شد</p></div>
<div class="m2"><p>بخون اندر ، چو مرد آشناور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جهانی دیده بر خسرو نهاده </p></div>
<div class="m2"><p>بتیر و نیزه از دیوار و از در</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز شه برجی قضا را چرخ داری </p></div>
<div class="m2"><p>ملک را یافت در میدان برابر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خون شمشیر هندی در کفش لعل</p></div>
<div class="m2"><p>ز خوی خفتان رومی بر تنش تر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو آتش چرخ را بر کرد و بشتافت</p></div>
<div class="m2"><p>کز آتش بیند آن پاداش و کیفر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بزد بر بازوی بر گستوان دار</p></div>
<div class="m2"><p>خدنگی راست رو ، بر گستوان در</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز زخم تیر تا پای خداوند</p></div>
<div class="m2"><p>بدستی مانده بد ، یا نیز کمتر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بدیگر سو از آن سان تیز بگذشت </p></div>
<div class="m2"><p>که از تیزی نیالودش بخون پر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ملک چون سرو و گل نازان و خندان</p></div>
<div class="m2"><p>نشاطی باد پایی خواست دیگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ملایک بر هوا آواز دادند</p></div>
<div class="m2"><p>ز شادی وز شگفت : الله اکبر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز فر ایزد و آثار دولت</p></div>
<div class="m2"><p>نشانی باشد این واضح ، نه مضمر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو پیکر بود : اسب و مرد جنگی </p></div>
<div class="m2"><p>بسوزانی و تیزی برق و صرصر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزخم اندر چه داند تیر بی جان</p></div>
<div class="m2"><p>تفاوت کردن از پیکر بپیکر؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در افسر در مکنون کی شناسد</p></div>
<div class="m2"><p>که افسر چیست یا دارای افسر ؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگیتی ز آب و آتش تیزتر نیست</p></div>
<div class="m2"><p>دو جان او بار سلطان ستمگر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سیاوش را و خسرو را نیازرد</p></div>
<div class="m2"><p>چو فر ایزدی بود آب و آذر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تهور گر نه بد بودی ز شاهان </p></div>
<div class="m2"><p>نه جوشن داردی در کین ، نه مغفر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه باید مغقر از آهن مر او را</p></div>
<div class="m2"><p>که یزدان داده باشد مغفر از فر ؟</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ایا شاهی ، که شخصت را بیار است </p></div>
<div class="m2"><p>بعقل و حلم یزدان گرو گر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فزون شد دولتت ، تا باز گشتی</p></div>
<div class="m2"><p>ز جنگ سکزیان دیو منظر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو ان بردن هنوز از جای جنگت</p></div>
<div class="m2"><p>دریده زهرۀ سکزی بزنبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از اکنون تا پسین روزی ز گیتی</p></div>
<div class="m2"><p>بر آن خاک ار فرود آید کبوتر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز بس آغار خون ، گردانه چیند</p></div>
<div class="m2"><p>تبر خون رویدش در حلق و ژاغر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چنان کردی که در ایوان شاهان </p></div>
<div class="m2"><p>بجای جنگهای رستم زر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ازین پس مرترا برزین نگارند</p></div>
<div class="m2"><p>تن تنها دریده قلب لشکر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بعون زال و رخش و پر سیمرغ</p></div>
<div class="m2"><p>ز یک تن کرد رستم پاک کشور</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تو تنها با سپاهی گر بکوشی</p></div>
<div class="m2"><p>چو قوم عاد بر بالی اشقر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چنانشان باز گردانی ، که از بیم </p></div>
<div class="m2"><p>بردار سبق جوید از برادر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ترا سیمرغ و تیر گز نباید </p></div>
<div class="m2"><p>نه رخش جادو و زال فسونگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زمردی و جگر نگذاشت باقی</p></div>
<div class="m2"><p>مصور بر تو ، ای زیبا مصور</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شجاعت هدیه ای باشد خدایی</p></div>
<div class="m2"><p>یلان را ، در دماغ و دل مستر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کسی رادرجهان ضامن نگیرد</p></div>
<div class="m2"><p>بشخص فربه و بالای سنگر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بپیش شیر لاغر پیل فربه </p></div>
<div class="m2"><p>چنان باشد که کوهی پیش یک ذر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ولیکن گاه کوشش بردارند</p></div>
<div class="m2"><p>دوال از پیل فربه شیر لاغر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>الا ای نامور شاهی ، که هستی </p></div>
<div class="m2"><p>ز شاهان در هر انواعی مخیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ز سهم افزای کاری باز گشتی</p></div>
<div class="m2"><p> که آن نادیده ، کس را نیست باور</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز حرص کین برون نا کرده خفتان </p></div>
<div class="m2"><p>ز خون دشمنان ناشسته خنجر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز خون خوردن دلت ناسیر ، لیکن</p></div>
<div class="m2"><p>ز خون در خنجرت سیراب گوهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ز خفتان معصفر بند بگشای</p></div>
<div class="m2"><p>ز ساقی باده ای بستان ، معصفر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بجای جوشن اندر پوش قاقم</p></div>
<div class="m2"><p>بجای نیزه بر کف گیر ساغر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قدح بر کف نه و عبهر بینبوی</p></div>
<div class="m2"><p>بر افروز آتشی چون چشم عبهر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگر بستان آزاری بیفسرد </p></div>
<div class="m2"><p>ز آذر بوستانی کن در آذر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>درختان رز اکنون ، تانه بس دیر</p></div>
<div class="m2"><p>یکایک زرد کرده سبز چادر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>برین گردون دریا چهر از میغ</p></div>
<div class="m2"><p>بپیوندد سماریهای عنبر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سماریهای عنبر چون گران شد </p></div>
<div class="m2"><p>فرو بارد ز عنبر عقد گوهر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وزان باریدن گوهر بنیسان </p></div>
<div class="m2"><p>بخندد باغ و بر بالا صنوبر </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ایا شاهی ، که از نظم مدیحت </p></div>
<div class="m2"><p>نگردد سیر طبع نظم گستر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا از نظم در خاطر عروسیست </p></div>
<div class="m2"><p>که ازنام تو خواهد نقش وزیور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بقای ذکر مردم نظم عالیست</p></div>
<div class="m2"><p>که دارد پای با ارکان و اختر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بسا کاشعار من در مدحت تو</p></div>
<div class="m2"><p>بخواهد گشتن از دفتر بدفتر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>الا تا هر درختی نیست طوبی</p></div>
<div class="m2"><p>الا تا هر غدیری نیست کوثر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو کوثر عمر وعیشت باد شیرین</p></div>
<div class="m2"><p>چو طوبی شاخ بختت باد پر بر</p></div></div>