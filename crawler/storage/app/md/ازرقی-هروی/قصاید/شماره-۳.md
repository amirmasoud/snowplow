---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ای از کمال حسن تو جزوی در آفتاب</p></div>
<div class="m2"><p>خطت کشیده دایرۀ شب بر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف چو مشک ناب ترا بنده مشک ناب</p></div>
<div class="m2"><p>روی چو آفتاب ترا چاکر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که زلف تست همه یکسره شبست</p></div>
<div class="m2"><p>و آنجا که روی تست همه یکسر آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغیست چهرۀ تو که دارد بنفشه بار </p></div>
<div class="m2"><p>سرویست قامت تو که دارد بر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ماه مشک داری و بر سرو بوستان</p></div>
<div class="m2"><p>در لاله نوش داری و در عنبر آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از چهره آفتابی و از روی شکری</p></div>
<div class="m2"><p> بس شاهدست با شکرت همبر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ار نایب سپهر نشد زلف تو چرا</p></div>
<div class="m2"><p> در حلقه ماه دارد و در چنبر آفتاب ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خالیست بر رخ تو ، بنام ایزد ، آن چنانک</p></div>
<div class="m2"><p>نارد همی بخویشتن از زیور آفتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی که نوک خامۀ دستور شهریار</p></div>
<div class="m2"><p>ناگه ز مشک شب نقطی زد بر آفتاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مخدوم ملک پرور ، صدر جهان که هست</p></div>
<div class="m2"><p> در پیش بارگاهش خدمت گر آفتاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سردار مجد دولت و دین کز برای فخر</p></div>
<div class="m2"><p> دارد ز رای روشن او رهبر آفتاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لشکر کشی که هستش لشکر گه آسمان</p></div>
<div class="m2"><p> فرماندهی که هستش فرمان بر آفتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر طالع قویش دعا گوی مشتری</p></div>
<div class="m2"><p> بر طالع بهیش ثنا گستر آفتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کامل بذات اوست خرد پرور آدمی</p></div>
<div class="m2"><p>فاخر ز جود اوست ثنا پرور آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر منبری که خطبۀ مدحش ادا کنند</p></div>
<div class="m2"><p>بوسد ز فخر پایۀ آن منبر آفتاب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زیبد زمانه را ز برای مدیح او</p></div>
<div class="m2"><p>خامه شهاب و نقش شب و دفتر آفتاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای صاحبی که دایم بر آفتاب ملک</p></div>
<div class="m2"><p>دارد ز رای روشن تو مفخر آفتاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای از محل چنانکه زهر آفریده جان</p></div>
<div class="m2"><p>وی از شرف چنانکه زهر اختر افتاب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آنجا بود که رای تو باشد در آسمان</p></div>
<div class="m2"><p>و آنجا نهد که پای تو باشد سر آفتاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از گرد مو کب تو کشد سرمه حور عین</p></div>
<div class="m2"><p>و ز ماه رایت تو کند افسر آفتاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نام شب از صحیفۀ ایام بسترد</p></div>
<div class="m2"><p> از رای تو اجازت یابد گر آفتاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر عزم آن که ریزد خون عدوی تو</p></div>
<div class="m2"><p>هر روز بامداد کشد خنجر آفتاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا کیمیای خاک درت بر نیفکند</p></div>
<div class="m2"><p>در ضمن هیچ کان ننهد گوهر آفتاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیمرغ صبح را ندهد مژدۀ صباح</p></div>
<div class="m2"><p>تا نام تو نبیند بر شهیر آفتاب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون تیغ نصرۀ تو بر آرد سر از نیام</p></div>
<div class="m2"><p>گویی همی بر آید از خاور آفتاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>با بند گانت پای ندارند سرکشان</p></div>
<div class="m2"><p>بر او سپاه شب چو کشد معجر آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آنجا که رزم جویی و لشکرکشی بفتح</p></div>
<div class="m2"><p>در بحر خون نیابد بر معبر آفتاب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از تف و تاب خنجر مردان لشکرت</p></div>
<div class="m2"><p>از سر کشد بشکل زنان چادر آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای آفتاب دولت عالیت بی زوال</p></div>
<div class="m2"><p>وی در ضمیر روشن تو مضمر آفتاب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای چاکری جاه ترا لایق آسمان</p></div>
<div class="m2"><p> وی بندگی رای ترا در خور آفتاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر شعر آفتاب که نبود برین نمط</p></div>
<div class="m2"><p>خصمی کند هر آینه در محشر آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نوبهار سبز بود ، آسمان کبود</p></div>
<div class="m2"><p>تا لاله سایه جوید و نیلوفر آفتاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سر سبز باد ناصحت از دور آسمان</p></div>
<div class="m2"><p>پژمرده لاله زار حسودت در آفتاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در جشن آسمان صفتت ریخته نثار</p></div>
<div class="m2"><p> ساقی ماهروی تو در ساغر آفتاب</p></div></div>