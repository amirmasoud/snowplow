---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>عروس ماه نوروزی چه کرد آن دانۀ گوهر؟</p></div>
<div class="m2"><p>که نورش ماه تابان بود و سعدش زهرۀ ازهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزاران صورت رنگین نگاریده برو مانی</p></div>
<div class="m2"><p>هزاران پیکر طبعی بر آورده از و آزر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن هر صورتی رخشان ، زمشک لعلگون صدره</p></div>
<div class="m2"><p>بر آن هر پیکری تابان ، زلعل مشکبوی افسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون هر صورتی دارد ز رنگ زعفران جامه</p></div>
<div class="m2"><p>کنون هر پیکری دارد ز شاخ کهربا زیور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمال زرفشان هر روز طاوسان بستان را</p></div>
<div class="m2"><p>نهد زرچوبه در منقار و مالد زعفران برپر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سپهسالار دریا را بر اسب باد پران بین</p></div>
<div class="m2"><p>خدنگش نرگس مسکین سنانش برگ نیلوفر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبه خفتان و در پیکان ، که از پرنده تیر او</p></div>
<div class="m2"><p>پس از ششماه در کهسار شخها بینی از خون تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک پیمای بحر آشوب عالم صحن انجم تگ</p></div>
<div class="m2"><p>شبه خفتان در پیکان آتشبار بانگ آور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بروی چشمۀ خورشید هزمان تند بخروشد</p></div>
<div class="m2"><p>سمک در دامن خفتان ، فلک در گوشة مغفر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نپاید دیر تا گردد ز مشک آلوده درع او</p></div>
<div class="m2"><p>هوا پرسیم پرنده زمین پر زر بازی گر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو باغ از نرگس مسکین فروزد شمع زنگاری</p></div>
<div class="m2"><p>هوا پروانۀ سیمین فرو ریزد برو بی مر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو گویی ذرۀ سیمین بزیر گنبد گردون</p></div>
<div class="m2"><p>بیاشوبند هر ساعت همی بر رغم یک دیگر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دهان ابر لؤلؤ بیز عنبر سای هر ساعت</p></div>
<div class="m2"><p>ز مینا بر کشد لؤلؤ بنیل اندر دمد عنبر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو برگ عبهر از عنبر نماید چرخ بر صحرا</p></div>
<div class="m2"><p>بچرخ اندر دمد صحرا ز سنبل دیدۀ عبهر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مصفا جوهری عالی که گیرد خاک از و صفوت</p></div>
<div class="m2"><p>منقش جرم نورانی که گردد دهر ازو انور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شرارش شهپر طوطی زند بر پهلوی پروین</p></div>
<div class="m2"><p>سرشکش دیدۀ شاهین نهد در چشم دو پیکر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گل و لاله است پنداری ز زر ساده و مرجان</p></div>
<div class="m2"><p>دهان لاله از سیماب و روی گل زسیسنبر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شد آمد های او گویی همی عمدا فرو گیرد</p></div>
<div class="m2"><p>نوا در پردۀ یاقوت و در انگشت خنیاگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تو گویی چشمۀ خورشید ازین گردون نورانی</p></div>
<div class="m2"><p>ز بهر خدمت خسرو فرستد بر زمین اختر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>وزان هر اختر روشن که از گردون جدا گردد</p></div>
<div class="m2"><p> ز فال فتح و فیروزی نشان آرد بهر محضر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خجسته شمس دولت را ، همایون زین ملت را</p></div>
<div class="m2"><p>مبارک کهف امت را ، طغانشاه آیت مفخر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خداوندیکه گر خواهد بیک ساعت فرو بندد</p></div>
<div class="m2"><p>خدنگش خانه بر خاقان سنانش قصر بر قیصر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تن اعدا بجان اندر نهان گردد ز بیم او</p></div>
<div class="m2"><p>چنان کاندر فروغ می نهان گردد همی ساغر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز اقبال وی اسکندر بدیدی چشمۀ حیوان</p></div>
<div class="m2"><p>اگر جزوی ز رای او بدی در رای اسکندر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر از بحر دو دست او بخار اندر هوا گیرد</p></div>
<div class="m2"><p>ازین زرین شود گردون از ان سیمین شود کشور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ببوی خلقش ارخواهی کنی از آذر آذریون</p></div>
<div class="m2"><p>بتاب خشمش ار خواهی ز آذریون کنی آذر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>قدم بر آسمان بنهاد پای همتش روزی</p></div>
<div class="m2"><p>ز جرم آسمان بگشاد در حین چشمۀ کوثر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الا یا نامور شاهی که پیش تخت و تاج تو</p></div>
<div class="m2"><p>ثنا خواند همی انجم سجود آرد همی محور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو در دریای دست تو بجنبد موج زرافشان</p></div>
<div class="m2"><p>ستاره بادبان باید ، فلک کشتی ، زمین لنگر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خرد چون پیکری گردد ز بهر آنکه پیش تو</p></div>
<div class="m2"><p>اشار تهای خدمت را بکار آید همی پیکر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جهان از تیغ تو ترسد چه ترس افتاد تیغت را ؟</p></div>
<div class="m2"><p>که از مغز عدوی تو نیارد کرد بیرون سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>طبابع گر خبر یابد ز سهم جان ستان تو</p></div>
<div class="m2"><p>مر آثار طبایع را عرض بگریزد از جوهر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز بهر زخم و پریدن خدنگ دیده دوزت را</p></div>
<div class="m2"><p>ز پر بیرون جهد پیکان ز پیکان سر برآرد پر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان گردر کفت بودی سخای تو بیک ساعت</p></div>
<div class="m2"><p>ز آبش بر کشیدی در ، زخاکش بر فشاندی زر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زمین از زخم گرز تو همی خواهد که بگریزد</p></div>
<div class="m2"><p>ولیکن راه او بسته است ازین گردون پهناور</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر آن گوهر کز آب و خاک پیدا شد ببخشیدی</p></div>
<div class="m2"><p>کنون تدبیر آن داری کز آهن بر کشی گوهر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر آن سر کان بتیغ تو زتن ، شاها، جدا گردد</p></div>
<div class="m2"><p>تنش بیسر برانگیزند روز حشر در محشر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زجاه و همتت روزی دو معنی در سخن راندم</p></div>
<div class="m2"><p> جهان دیدم درو مدغم فلک دیدم درو مضمر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در آن روزی کجا ختلی ، فعال ماه پیکررا</p></div>
<div class="m2"><p>نهد بر دیدۀ جنگی زند بر سینۀ صفدر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدانسان آتش پیکار در دلها برافروزد</p></div>
<div class="m2"><p>که درع جوشن و خفتان شود بر سینه خاکستر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو آتش نطفۀ بی جان زبهر کین برون آید</p></div>
<div class="m2"><p>ز پشت مرد جوشن پوش بازو بین و با مغفر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>زهاب چشمه را ماند زخون کشتگان صحرا</p></div>
<div class="m2"><p>صفیر مرغ را ماند ز آواز یلان تندر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مبارزتر کسی ، شاها ، که مرزخم سنانش را</p></div>
<div class="m2"><p>بهیجا آفرین خواند روان رستم و نوذر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو بیند صورت خود را بتیغ اندر چنان داند</p></div>
<div class="m2"><p>کز آهن مر نبردش را برون آید همی لشکر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو آن شبرنگ تازی را بمیدان چون برانگیزی</p></div>
<div class="m2"><p>عدو را روز بنوردی بدان تیغ بلا گستر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز بیم خنجر و پیکان مبارز پیش زخم تو</p></div>
<div class="m2"><p>نه بر بشناسد از پیکان نه سر بشناسد از خنجر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نبود آگاه اسکندر چوشد از حد تاریکی</p></div>
<div class="m2"><p>که بر گوهر همی راند، نه بر خاک ، ادهم واشقر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر جز وی ز رای توچراغ راه او بودی</p></div>
<div class="m2"><p>بدیدی در شب تاریک گام مور بر مرمر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>وگر تخت سلیمان را همی صرصر خداوندا</p></div>
<div class="m2"><p>کشید اندر هوا پران بامر داد ده داور</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تو آتش طبع گردونی همی در زیر ران داری </p></div>
<div class="m2"><p>که اندر دست او ابرست و اندر پای او صرصر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>و گر خضر پیمبر را مباح آمد که بی کشتی</p></div>
<div class="m2"><p>گذارد گام را بر موج در دریای بی معبر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو از پولاد مینارنگ دریایی بکف داری</p></div>
<div class="m2"><p>که صد دریای خون دارد روان در آب و در گوهر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>و گر در قبض انگشتان همی پولاد چینی را</p></div>
<div class="m2"><p>چو موم تفته بگسستی همی داود پیغمبر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نیابد رنج دست تو خیال دست تو شاها</p></div>
<div class="m2"><p>ز گیتی بر کند ارکان ز گردون بگسلد چنبر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>خداوندا ، همی خواهم که انقاس مدیحت را </p></div>
<div class="m2"><p>شود مژگان من اقلام و گردد دیدگان اختر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>باندک روزگار ، ای شه ، دو چیزم داد بخت تو</p></div>
<div class="m2"><p>یکی لفظ خرد رتبت ،دوم طبع سخن گستر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>مرا گر پیش ازین ، شاها ، بشعر اندر بسی بودی</p></div>
<div class="m2"><p>معانی سست و نازیبا ، قوافی سرد و نا درخور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>کنون بخت توام ، شاها ، همی تلقین کند نونو</p></div>
<div class="m2"><p>معانی های چون لؤلؤ قوافیهای چون شکر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>همی تا گنبد گردون نگیرد با زمین پستی</p></div>
<div class="m2"><p>همی تا چشمۀ خورشید سر بر دارد از خاور</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ولایت گیر و دشمن کش ، جهان پیمای و لشگر کش</p></div>
<div class="m2"><p>نشاط افزای و شادی کن ، سخاوت ورز و ملکت خور</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بمان چندان ، خداوندا ، که اندر گردش گردون</p></div>
<div class="m2"><p>ز اخگر بردمد دریا ، زدریا بر جهد اخگر</p></div></div>