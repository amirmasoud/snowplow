---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>دوش در گردن شب عقد ثریا دیدم</p></div>
<div class="m2"><p>نوعروسان فلک را به تماشا دیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رانده بودم همه شب گرد زوایای فلک</p></div>
<div class="m2"><p>ماه را در فلک عقد ثریا دیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود آوردۀ غواص شب از قلزم غیب</p></div>
<div class="m2"><p>هر جواهر که درین قبۀ مینا دیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیک فرخنده مهی بود ز شاخ طوبی</p></div>
<div class="m2"><p>هر شکوفه که درین قبۀ خضرا دیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیز پایان تخیل ، که سبک می رفتند</p></div>
<div class="m2"><p>همه را در حرس عالم بالا دیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حیدر رزم فلک را ، که نسب مریخست</p></div>
<div class="m2"><p>عاشق شیفتۀ زهرۀ زهرا دیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیل اشکم که چو زد موج ز گردون بگذشت</p></div>
<div class="m2"><p>چشمه ای بود که پر آب مصفا دیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کند بر سر خورشید سحر گاه نثار</p></div>
<div class="m2"><p>دامن چرخ پر از لؤ لؤ لالا دیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این غزل زهره ادا کرد مگر خرم بود</p></div>
<div class="m2"><p>که شفق در رقمش مصفی صهبا دیدم ؟</p></div></div>