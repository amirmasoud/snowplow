---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ز روی و قد تو بی شک صنوبر آید و ماه</p></div>
<div class="m2"><p>ز روشنی و بلندی که هستی ، ای دلخواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر صنوبر و ماهی شگفت و طرفه است این</p></div>
<div class="m2"><p>شگفت و طرفه بود مردم از صنوبر و ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و شاق حلقۀ زلف ترا بشهر ختن</p></div>
<div class="m2"><p>شود بنافه درون حلقه حلقه مشک سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غلام و بندۀ آن ساعتم ، کجا سرمست</p></div>
<div class="m2"><p>همی روی سوی درگاه بامداد پگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خواب خاسته در وقت و چشم خواب آلود</p></div>
<div class="m2"><p>ز ناز بسته کمر تنگ و کژ نهاده کلاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه لاله برگی و هستی برنگ لالۀ سرخ</p></div>
<div class="m2"><p>نه شاخ سروی و هستی بقد چو سرو ستاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سیم و مشک و گناهست و توبه زلف و رخت</p></div>
<div class="m2"><p>ز سیم توبه شگفت آید و ز مشک گناه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غلام آن خط مشکین نیم دایره ام</p></div>
<div class="m2"><p>ز قیر و مشک چو طغر ای میر میرانشاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شهنشهی که بروز و بشب همی گویند</p></div>
<div class="m2"><p>ستاره و فلک و جوهر و تراب و میاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که بو شجاع امیرانشه بن قارودست</p></div>
<div class="m2"><p>سپهر همت و دریای جود و عنصر جاه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تمامی خرد اندر مدیح او عاجز</p></div>
<div class="m2"><p>درازی امل اندر بقای او کوتاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ایا ستوده شهی ، کز خیال خنجر تو</p></div>
<div class="m2"><p>تن عدو بگذارد چو نقره اندر گاه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هزار جای مرا ابر بیش سجده برد</p></div>
<div class="m2"><p>اگر بدست تو من ابر را کنم اشباه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بهر مدحت تو زین سپس ز روی زمین </p></div>
<div class="m2"><p>زبان طوطی بیرون دمد بجای گیاه </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دست دشمن تو نوش خوردن اکراهست</p></div>
<div class="m2"><p>بنام تو بتوان خورد زهر بی اکراه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در آن زمان که چو دریای موج برخیزند</p></div>
<div class="m2"><p>زبهر کینه نمودن سپاه سوی سپاه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز زخم سم ستوران چو کاه گردد کوه</p></div>
<div class="m2"><p>ز نوک نیزۀ گردان چو کوه گردد کاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یقین شناس که تا روز حشر برناید</p></div>
<div class="m2"><p>از آب تیغ تو جان عدوی تو بشناه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بروز کینه چو پای تو در شود برکاب</p></div>
<div class="m2"><p>رکاب وزین بداندیش بند گردد و چاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیاز فتنۀ یأجوج بود در گیتی</p></div>
<div class="m2"><p>بفر جود بر آن فتنه تنگ بستی راه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سکندری توازین کآرزوی حضرت تست</p></div>
<div class="m2"><p>هری بهشت ، که کرد سکندرست هراه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از آن بقوس قزح ابر سرخ و زرد شود</p></div>
<div class="m2"><p>که از سخای تو اندیشها کند گه گاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تویی که حال ولی را کنی بجود نکو</p></div>
<div class="m2"><p>تویی که روز عدو را کنی بخشم تباه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خدایگانا ، تا روز چند بنمایم</p></div>
<div class="m2"><p>که با ستاره کند راز خاک آن درگاه</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سه چیز باشد ازین پس خطاب تو ز ملوک :</p></div>
<div class="m2"><p>ستاره لشکر و خورشید تاج و گردون گاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر بجود و شجاعت دهد ولایت بخت</p></div>
<div class="m2"><p>ترا ولایت باید چو این جهان پنجاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یقین بدان که برون از برای ملکت تو</p></div>
<div class="m2"><p>در آفرینش عالم غرض نداشت اله</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تو نادر الاقرانی و اندرین معنی</p></div>
<div class="m2"><p>بسست نسبت تو شهریار زاده گواه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>همیشه تا نبود پشه همچو پیل بزور</p></div>
<div class="m2"><p>همیشه تا نبود معنی شفا بشفاه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>موافقان ترا باد ناز و شادی و لهو</p></div>
<div class="m2"><p>مخالفان ترا باد رنج و سختی و آه</p></div></div>