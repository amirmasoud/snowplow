---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>مبارکی و سعادت نمود روی بشاه</p></div>
<div class="m2"><p>از آن مبارک و مسعود تحفهای زاله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه تحفه ایست ؟ یکی فر خجسته فرزندست </p></div>
<div class="m2"><p>موافقان را شادی فزای و انده کاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشهریاری و شاهی تمام نسبت او</p></div>
<div class="m2"><p>زهر دو روی نسب شهریار و زادۀ شاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه پادشاه چنو بیند از فراز و نشیب</p></div>
<div class="m2"><p>نه شهریار چنو یابد از سپید و سیاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دیبه سلب باد روز در پوشد </p></div>
<div class="m2"><p>کجا ز غیبه بود تارو پود آن دیباه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کلاه ملک ز شاهان بتیغ بستاند </p></div>
<div class="m2"><p>خزانه شان گه بخشش تهی کند بکلاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ببزم ورزم ببینی که او چه خواهد کرد</p></div>
<div class="m2"><p>ببدره های زر سرخ و قلبهای سیاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پسر بود بحقیقت پناه و پشت پدر</p></div>
<div class="m2"><p>چه خوب تر بجهان مر تر از پشت و پناه ؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر آنچه خواستی و جستی از خدای بزرگ</p></div>
<div class="m2"><p>بیافتی و بداری ، دگر بجوی و بخواه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو گل بخند و بیفروز ، زان جهت که هنوز</p></div>
<div class="m2"><p>بباغ بخت تو نشکفت یک گل از پنجاه</p></div></div>