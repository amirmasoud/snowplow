---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>به فال همایون و فرخنده اختر</p></div>
<div class="m2"><p>به بخت موفی و سعد موفر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به وقتی که هست اندر او فال خوبی</p></div>
<div class="m2"><p>به روزی که هست اندر او سعد و اکبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بزم نو، اندر سرای نو آمد</p></div>
<div class="m2"><p>خداوند فرزانه شاه مظفر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخی شمس دولت ، گزین کهف امت</p></div>
<div class="m2"><p>ملک بوالفوارس ، طغانشاه صفدر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روان بزرگی و طبع مروت</p></div>
<div class="m2"><p>سپهر معالی و خورشید گوهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بباغی خرامید خسرو ، که او را</p></div>
<div class="m2"><p>بهار و بهشتست مولی و چاکر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چمن های او را ز نزهت ریاحین</p></div>
<div class="m2"><p>روشهای او را ز خوبی صنوبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگاه بهار اندر و روی لاله</p></div>
<div class="m2"><p>بوقت خزان اندر و چشم عبهر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز دستان قمری درو بانگ عنقا</p></div>
<div class="m2"><p>ز آواز بلبل درو زخم مزمر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درختانش از عود و برگ از زمرد</p></div>
<div class="m2"><p>نباتش ز مینا و خاکش ز عنبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکشی چو اندیشة مرد عاشق</p></div>
<div class="m2"><p>بخوبی چو رخسارة یار دلبر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی برکۀ ژرف در صحن بستان</p></div>
<div class="m2"><p>چو جان خردمند و طبع سخنور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهادش نه دریا ، نه کوثر ولیکن</p></div>
<div class="m2"><p>بژرفی چو دریا، بپاکی چو کوثر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپاکی چو جان و بخوبی چو دانش</p></div>
<div class="m2"><p>ز صفوت هواو ز لطافت چو آذر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>روان اندر و ماهی سیم سیما</p></div>
<div class="m2"><p>چو ماه نو اندر سپهر منور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بیک موی این باغ خرم سرایی</p></div>
<div class="m2"><p>پر از صفه و کاخ و ایوان و منظر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نگویم که عین بهشتست لیکن</p></div>
<div class="m2"><p>بهشتست اندر سرای مکدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برافراز او چنبر چرخ گردان</p></div>
<div class="m2"><p>سر پاسبان را بساید بچنبر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز بس نقره کاری ، چو کاخ سلیمان</p></div>
<div class="m2"><p>ز پس استواری ،چو سد سکندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تصاویر او دهشت طبع مانی</p></div>
<div class="m2"><p>تماثیل او حسرت جان آزر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همه سایه و صورت و شکل ایوان</p></div>
<div class="m2"><p>در آن برکۀ لاژوردی مصور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو گویی مگر جام کیخسرو ستی</p></div>
<div class="m2"><p>منقش در و پیکر هفت کشور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سر کنگره ، گرد دیوار باغش</p></div>
<div class="m2"><p>بساید همی پیکر اندر دو پیکر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گو زنان بالیده شاخند گویی</p></div>
<div class="m2"><p>برآمیخته زخم را یک بدیگر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نپوید مگر صحن او را بسالی</p></div>
<div class="m2"><p>مهندس باندیشه ، عنقا بشهپر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مزین درو صفه های مربع</p></div>
<div class="m2"><p>منقش درو شمسه های مدور</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بصفه درون پیکر پیل جنگی</p></div>
<div class="m2"><p>بشمسه درون صورت شاه سرور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خداوند گنج و بزرگی و دولت</p></div>
<div class="m2"><p>خداوند شمشیر و دیهیم و افسر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بشمشیر او باز بستست گیتی</p></div>
<div class="m2"><p>عرض باز بستست لابد بجوهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باندیشه اندر نگنجد مدیحش</p></div>
<div class="m2"><p>که مدحش تماست و اندیشه ابتر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر از باختر بر کشد تیغ هندی</p></div>
<div class="m2"><p>رسد موج خون در زمان تا بخاور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بتشریف ملکت درون ، عین معنی</p></div>
<div class="m2"><p>بتعریف دولت درون ، لفظ مصدر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی کوندیدست مر ناوکش را</p></div>
<div class="m2"><p>در آتش مرکب ندیدست صرصر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایا شهریاری ، که با همت تو</p></div>
<div class="m2"><p>ز اعراض زایل شمارند محور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پلنگ از نهیب سنانت بخواهد </p></div>
<div class="m2"><p>بخواهشگری بال و پر از کبوتر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز تف سنان تو ، نازاده دشمن</p></div>
<div class="m2"><p>چو سیماب بگریزد از ناف مادر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کسی کز سنان تو جان داده باشد</p></div>
<div class="m2"><p>ز بیم سنان تو ناید به محشر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر آب تیغ تو در رفتن آید</p></div>
<div class="m2"><p>درو هفت دریا بود هفت فرغر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو نام تو خاطب ز منبر بخواند</p></div>
<div class="m2"><p>سخن گوی گردد بفر تو منبر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شعاع درفش تو بر هر که تابد</p></div>
<div class="m2"><p>نیاید ز اولاد آن دوده دختر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>فلک را بسوزانی از عکس زوبین</p></div>
<div class="m2"><p>زمین را بدرانی از نعل اشقر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو آنی که شیر ژیان روز هیجا</p></div>
<div class="m2"><p>همی بر سنان تو افسر کند سر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زمین پیکر از یکدیگر بگسلاند </p></div>
<div class="m2"><p>بروز نبرد تو ، ز آهنگ لشکر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز خنجر کنی چشمۀ زندگانی</p></div>
<div class="m2"><p>اگر نام خود بر نگاری بخنجر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بنام خلاف تو گر گل نشانند</p></div>
<div class="m2"><p>سنان جگر دوز و خنجر دهد بر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>فری سیر آن بارۀ کوه پیکر</p></div>
<div class="m2"><p>که با آب و آتش بپوید برابر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بچشم و بموی و بسم و سرین گه</p></div>
<div class="m2"><p>چو جزع و چو مشک و چو پولاد و مرمر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بکبر پلنگ و برفتار شاهین</p></div>
<div class="m2"><p>به قد هیون و به زور غضنفر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بهنگام نرمی و هنگام تندی</p></div>
<div class="m2"><p>سبک تر ز کشتی ، گران تر ز لنگر</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بآب اندرون همچو لؤلؤی بیضا</p></div>
<div class="m2"><p>بآتش درون همچو یاقوت احمر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بر افراز او شاه هنگام هیجا</p></div>
<div class="m2"><p>چو بر کوه خارا ز پولاد عرعر</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ایا شهریاری که کوه سیه را</p></div>
<div class="m2"><p>بسنبی به پیکان پولاد پیکر</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درین بزم شاهانه بر رسم شاهان</p></div>
<div class="m2"><p>به نور می لعل بفروز ساغر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>میی گیر ، شاها ، که از بوی و رنگش</p></div>
<div class="m2"><p>شود دیده و مغز پر مشک اذفر</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بلطف روان و بنور ستاره</p></div>
<div class="m2"><p>ببوی گلاب و برنگ معصفر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بروشن می لعل خوشبوی خوش زی</p></div>
<div class="m2"><p>ز فرخ وزیر خردمند برخور</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وزیری ، که او را کفایت مهیا</p></div>
<div class="m2"><p>وزیری ، که او را جلالت مسخر</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وزیری ، که جان سخن راست دانش</p></div>
<div class="m2"><p>وزیری که شخص خرد راست زیور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>وزیری ، که پرداخت جایی بماهی</p></div>
<div class="m2"><p>به از قصر کسری و ایوان قیصر</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدل ناصح ملک پیروز دولت</p></div>
<div class="m2"><p>بجان بندۀ شاه پیروز اختر</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>ایا شهریاری ، کجا تیغ عدلت</p></div>
<div class="m2"><p>ز گیتی ببرید دست ستم گر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بمان اندرین دولت و ملک چندان </p></div>
<div class="m2"><p>کجا آب حیوان برآید ز اخگر</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فلک را بجز بندۀ خویش مشناس</p></div>
<div class="m2"><p>زمین جز به کام دل خویش مسپر</p></div></div>