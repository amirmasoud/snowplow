---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>رخسار و قد و زلف و بناگوش یار من</p></div>
<div class="m2"><p>ما هست بر صنوبر و مشکست بر سمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ماه و با صنوبر او نور و راستی</p></div>
<div class="m2"><p>اندر سمن طراوت و در مشک اوشکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این هر چهار فتنۀ دین دیده و دلند</p></div>
<div class="m2"><p>بر هر چهار من بدل و دیده مفتتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدم بنفشه وار شد و رخ بنفشه فام</p></div>
<div class="m2"><p>زان تودۀ بنفشه او بر دو نسترن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشک ختن بنفشۀ او را سزد رهی</p></div>
<div class="m2"><p>نقش ختا دو نسترنش را سزد شمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور مشک در ختن بود و نقش در ختا</p></div>
<div class="m2"><p>زلفین و روی اوست پس اندر ختاختن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نازکی و کوچکی اندر جهان که دید</p></div>
<div class="m2"><p>نازک تر از میانش و کوچک تر از دهن ؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیبا و دلفریب بدان نازکی کمر</p></div>
<div class="m2"><p>شیرین و جان فزای بدان کوچکی سخن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صافی و دور بین دل و جانیست مرمرا</p></div>
<div class="m2"><p>هر دو بدست مهر و مدیحند مرتهن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهر نگار یاسمن اندام ماهروی</p></div>
<div class="m2"><p>مدح سدید دین شرف الدولة بو الحسن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن پاک جان و پاکدل و پاک اعتقاد</p></div>
<div class="m2"><p>آن راستگوی و راستی آرای و راست ظن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جز مدح او مگوی و جز از خدمتش مجوی</p></div>
<div class="m2"><p>کان پرورد روانت و این پرورد بدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با هر کسی که بینی و با هر کسی ازو</p></div>
<div class="m2"><p>جنسیست از محامد و نوعیست از منن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شغلی که او گزارد و از پیش او رود</p></div>
<div class="m2"><p>ز انصاف و راستی شود آن شغل چون سنن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در مدح میغ گفته شدست این که : بهره زو</p></div>
<div class="m2"><p>یکسان برند شوره گز و شاخ یاسمن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در کثرت سخاوت ازین مدح عالیست</p></div>
<div class="m2"><p>باری من این مدیح نخواهم بهیچ فن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اسراف در حدود سخاوت ستوده نیست</p></div>
<div class="m2"><p>واجب بود حدود سخاوت شناختن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بخشنده ایست او ، که ببازی و ناوجوب</p></div>
<div class="m2"><p>ز احسان او نصیب بیابند مرد و زن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ور دادخواه مستحقش عالمی بوند</p></div>
<div class="m2"><p>زو حق و داد خویش بیابند تن بتن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>موقوف بر مروت و بر اعتقاد اوست</p></div>
<div class="m2"><p>تشریف اهل فضل و مراعات ممتحن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای مدحت مجرد تو جلوۀ نعات</p></div>
<div class="m2"><p>وی سیرت مهذب تو تحفۀ فطن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شاداب بوستان بهارست سیرتت</p></div>
<div class="m2"><p>وندر تو از فنون بزرگی بسی فنن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>از قدر و روشنی چمنش جفت آسمان</p></div>
<div class="m2"><p>گلهای او چو ماه و چو خورشید در چمن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از نظم شاعران و ز الفاظ فاضلان</p></div>
<div class="m2"><p>آواز عندلیبش و دستان چنگ زن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هرگز دو چیز جفت نگردند با دو چیز</p></div>
<div class="m2"><p>با دشمنانت شادی و با دوستان حزن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هنگام دستشوی تو ز اقبال دست تو</p></div>
<div class="m2"><p>نشگفت ار آب زر شود و کیمیا لگن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای مهتر فریشته خو ، گشت روزگار</p></div>
<div class="m2"><p>تاری ترست بر سرم از جان اهرمن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ننماید آنچه چرخ نماید مرا همی</p></div>
<div class="m2"><p>سودا بهیچ مرد هراسنده در وسن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرگشته تر زمن نبود در یقین حال</p></div>
<div class="m2"><p>مرغ شب بطیره برون کرده از وطن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زیرا که چون بشعر نمایم شکار باز</p></div>
<div class="m2"><p>ننگ آیدم ربودن مردار چون زغن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در مدح ناکسان نکنم کهنه تن بدرد</p></div>
<div class="m2"><p>زان باک نایدم که بود کهنه پیرهن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آراسته بجامه تن از صلت کریم</p></div>
<div class="m2"><p>به ز آنکه غم کشیدن و پوشیدن کفن</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اول بمدح تو ز جهان کردم اقتصار</p></div>
<div class="m2"><p>در باب شاعری چو بشستم لب از لبن</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>و زجور روزگار از آن روز تاکنون</p></div>
<div class="m2"><p>صد ره مرا خریدی و بگزاردی ثمن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در غیبت تو سال دو از گونه گونه رنج</p></div>
<div class="m2"><p>بر تارکم گذشت بناکام من حزن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>امروز چون بطلعت و فر تو در هری</p></div>
<div class="m2"><p>سر برفراخت دولت و بفروخت انجمن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بی هوش و مست مانده ام از خدمت تو دور</p></div>
<div class="m2"><p>گاهی ز جوش شیره و گه از بلای دن</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از غفلت وز خوی من آگاه گشته ای</p></div>
<div class="m2"><p>بر خوی من فراخ بمن داده ای رسن</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تقصیر بی قیاس و مرا روی عذر نی</p></div>
<div class="m2"><p>تقصیرها عفو کن و بپذیر عذر من</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تا از حدود غرب نداند کسی ختا</p></div>
<div class="m2"><p>تا از دیار شرق نخواهد کسی یمن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بر هر سری ز نعمت خود بهره ای فشان</p></div>
<div class="m2"><p>بر هر تنی ز کردۀ خود منتی فگن</p></div></div>