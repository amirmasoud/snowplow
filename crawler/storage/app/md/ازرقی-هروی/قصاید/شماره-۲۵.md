---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>در روزگار کامروا باد و شاد خوار</p></div>
<div class="m2"><p>شاه ملوک و صدر سلاطین روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان ابوالملوک ملک ارسلان ، که چرخ</p></div>
<div class="m2"><p>ایوانش را بدیده نهادست بر کنار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شاهی که تاج محمود از افتخار او</p></div>
<div class="m2"><p>در آفتاب ننگرد الا بچشم عار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهی که تخت دارا از انتظار او</p></div>
<div class="m2"><p>هر ساعتی چو زیر کند نالهای زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از عشق نام شاه نگین عزیز مصر</p></div>
<div class="m2"><p>خون شد ز غبن او و پذیرفتن نگار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز بی اجازت رأی خدایگان</p></div>
<div class="m2"><p>برناید آفتاب درخشان ز کوهسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عز جواز او را بیش از هزار سال</p></div>
<div class="m2"><p>بودست آفرینش عالم در انتظار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از روزگار آدم تا روزگار او</p></div>
<div class="m2"><p>شاهان قدوم او را بودند جان سپار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیراستند ملک و بینباشتند گنج</p></div>
<div class="m2"><p>افراختند تخت و برآراستند کار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر بجمله دولت پاینده را بطوع</p></div>
<div class="m2"><p>پیش بقای شاه نهادند بنده وار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او هست خسروی که سلاطینش بوده اند</p></div>
<div class="m2"><p>مستوفی و مهندس و ضراب و جامه دار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عزمیست استوار فلک را چنانکه چرخ</p></div>
<div class="m2"><p>دارد بنای ملک بر آن عزم استوار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا آسمان عدل بری ماند از خلل</p></div>
<div class="m2"><p>تا آفتاب ملک صمی باشد از غبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رای بلند او بوزیری سپرده ملک</p></div>
<div class="m2"><p>کز رای اوست گوهر اسلام را عیار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن یوسفی که دیدۀ یعقوب زو ضریر</p></div>
<div class="m2"><p>او کرد بوی پیرهن یوسفش نثار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیری که بخت او بجوانی نهاد روی</p></div>
<div class="m2"><p>نوری که خصم او بحمایت گرفت نار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر عالمی حکایت او کارزار کرد</p></div>
<div class="m2"><p>کان جا فلک نبود کفایت بکارزار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست و بنانش مایۀ تیغ آمد و قلم</p></div>
<div class="m2"><p>بأس و امانش مایۀ لیل آمد و نهار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در مسند جلال نیاید چنو وزبر</p></div>
<div class="m2"><p>بر عرصۀ کمال نتازذ چنو سوار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای تاج تیغ داران اسب ترا نعال</p></div>
<div class="m2"><p>وی جان پادشاهان تیغ ترا شکار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عزم شکار تو ز هزبران ملک چند</p></div>
<div class="m2"><p>پر کرد غار و سمج و تهی کرد مرغزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روزی که چون سلیمان اهل زمانه را</p></div>
<div class="m2"><p>از روی فخر دادی بر پشت باد بار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در خدمت رکاب تو سربر زمین نهاد</p></div>
<div class="m2"><p>خورشید ز آسمان چهارم هزار بار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن زلزله زبأس تو اندر جهان فتاد</p></div>
<div class="m2"><p>ار آب خورد گیتی از آن عزم نامدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کاندر همه خراسان تخمی نکرد بیخ</p></div>
<div class="m2"><p>وندر همه عراق نهالی نداد بار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از سختی کمند بلند تو گشت پست</p></div>
<div class="m2"><p>مسمار های ملک سلاطین روزگار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هر برج و هر حصار که شاخ گوزن داشت</p></div>
<div class="m2"><p>پنهان شد از نهیب خدنگ تو در حصار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای شاه ، تاجداران دانند سر این</p></div>
<div class="m2"><p>تیرت گوزن را نبود سخت خواستار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خرسندیی دهش چو بینی که پاک رفت</p></div>
<div class="m2"><p>در آرزوی تیر تو ، شاها ، ازو قرار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بینند ، خسروا ، که اکر پیش رای تو</p></div>
<div class="m2"><p>زین پس کند شکاری زین گونه آشکار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عاجز شود ستاره و بگریزد از سپهر</p></div>
<div class="m2"><p>واله شود سپهر و فرو ماند از مدار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فرمانده سپهری ، فرمان دهش بجبر</p></div>
<div class="m2"><p>تا بیخ دشمنانت ببرد باختیار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر چند دل رمیده و آسیمه سر شدست</p></div>
<div class="m2"><p> از دست گنج پاش تو آن ابر گنج بار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ای رفته چون سکندر و از تیغ سد گشای</p></div>
<div class="m2"><p>بربسته پیش لشکر یأجوج رهگذار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر کشوری زده که فلک بر فراز او</p></div>
<div class="m2"><p>نگذشت تا نخواست از آن قوم زینهار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن صبح دم چه بود که از کوه جنگوان</p></div>
<div class="m2"><p>سر بر زد آفتابی اندوه رخ بقار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ابری ز گرد لشکر سر بر هوا نهاد</p></div>
<div class="m2"><p>بر فرق آن گروه ببارید ذوالفقار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از غار بر فراخت سر موج خون بکوه</p></div>
<div class="m2"><p>وز کوه در فتاد سر سیل خون بغار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سیلی چنان عظیم ، که در کم ز ساعتی</p></div>
<div class="m2"><p>دیار جای گیر نماند اتدر آن دیار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یا بردۀ اجل شد ، یا بردۀ سپاه</p></div>
<div class="m2"><p>یا خستۀ یمین شد ، یا بستۀ یسار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنست امید بخت تو کز خشم و بأس تو</p></div>
<div class="m2"><p>از لشکر عراق برآرد کنون دمار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگشاید آن ولایت و بربندد آن طریق</p></div>
<div class="m2"><p>بنوردد آن رسوم و بپردازد آن شعار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از ملک بی زوال تو و بخت بی ملال</p></div>
<div class="m2"><p> وز عز بی فنای تو و عمر بی کنار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا گوهر از فروغ شرف گیرد و خطر</p></div>
<div class="m2"><p>تا عالم از بهار شود تبت و تتار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رای تو باد گوهر انصاف را فروغ</p></div>
<div class="m2"><p>فتح تو باد عالم اسلام را بهار</p></div></div>