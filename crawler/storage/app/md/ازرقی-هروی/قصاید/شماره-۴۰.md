---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>ایا بفضل و کرم یاد کرده از کارم</p></div>
<div class="m2"><p>زیاد کرد تو بسیار شکرها دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خصایل تو سزاوار مدحتند همه</p></div>
<div class="m2"><p>بجلوه کردن آن من رهی سزاوارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان کنم بسعادت که تا کم از یکسال</p></div>
<div class="m2"><p>بود قصاید مدح تو تاج اشعارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا مدیح نگویم ترا ؟ که ناگفته</p></div>
<div class="m2"><p>همی ز گنج سخای تو بهره بردارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر خدای بخواهد به بخت من پس ازین</p></div>
<div class="m2"><p>به مدحت تو سخن ز آفتاب بگذارم</p></div></div>