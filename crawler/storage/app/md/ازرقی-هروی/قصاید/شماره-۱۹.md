---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>خوش و نکو ز پی هم رسید عید و بهار</p></div>
<div class="m2"><p>بسی نکوتر و خوشتر ز پار و از پیرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ز جشن عجم جشن خسرو افریدون</p></div>
<div class="m2"><p>یکی ز دین عرب دین احمد مختار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان بسان یکی چادری شدست یقین</p></div>
<div class="m2"><p>کجا ز عید و ز نوروز پود دارد و تار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز روی پیری گلزار چون زلیخا بود</p></div>
<div class="m2"><p>دعای یوسف گشت آب و ابر در گلزار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نسیم گل نو چو خضر در سفرست</p></div>
<div class="m2"><p>ردای خضر چرا بر سر افگنند اشجار ؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو میغ گوشۀ چتر سیه برافرازد</p></div>
<div class="m2"><p>بآسمان کبود از میان دریا بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدنگ بارد بر آسمان جوشن پوش</p></div>
<div class="m2"><p>ز دامن زره زنگیان تیغ گزار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز عکس لاله و از عکس سبزه بزخیزد</p></div>
<div class="m2"><p>دو نیم دایره از روی ابر باران بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گمان بری که ز بس سبزی و ز بس سرخی</p></div>
<div class="m2"><p>که سبزی خط یارست و سرخی لب یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بسان مهرۀ مارست شکل ژاله و زو</p></div>
<div class="m2"><p>بشکل مار در آید بدشت سیل بهار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر ز مار همی مهره خاست ، از چه سبب</p></div>
<div class="m2"><p>کنون ز مهره همی خیزد ، ای شگفتی ، مار ؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو پشت مشکین سارست شکل لاله و زو</p></div>
<div class="m2"><p>چکان بسان نقطهای پشت مشکین سار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستارگان بمجره ، درست پنداری</p></div>
<div class="m2"><p>گل سپید برو آب زر برده بکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دریده پیرهن سبز غنچه بر گل زرد</p></div>
<div class="m2"><p>چنانکه طوطی بر زعفران زند منقار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز باد چفته شود برگ زرد گل ، گویی</p></div>
<div class="m2"><p>مگر کسی بفسان برهمی زند دینار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبا بسوی گل سرخ برد وقت سحر</p></div>
<div class="m2"><p>سماع بلبل روشن روان ز شاخ چنار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درید لالۀ کوهی نقاب زنگاری</p></div>
<div class="m2"><p>چو شمع سوزان مومش سرشته بازنگار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تصوفست همانا طریقت گل سرخ</p></div>
<div class="m2"><p>که بر سماع بدرید جامه صوفی وار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گمان بری که گه زخم بازوی خسرو </p></div>
<div class="m2"><p>سنان لعل ز خفتان سبز کرد گذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گزیده شمس دول شهریار زین ملوک</p></div>
<div class="m2"><p>که دین و دولت ازو گشت جفت عز و فخار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ابوالفوارس خسرو طغانشه ، آن ملکی</p></div>
<div class="m2"><p>که شاهی از اثر جاه او برد مقدار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خدایگانی ، کز قدر و جاه و بخشش اوست</p></div>
<div class="m2"><p>مدار چرخ و سکون زمین و موج بخار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خصایلش همه تهذیب دانشست و خرد</p></div>
<div class="m2"><p>جوارحش همه ترکیب بخششست و وقار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسی بلیغ تر آید ز گفت افلاطون</p></div>
<div class="m2"><p>اگر معانی یک لفظ او کنی تکرار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چه لفظ او بسخن در ، چه ابر گوهرپاش</p></div>
<div class="m2"><p>چه سهم او بو غادر، چه شیر مردم خوار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ایا بزرگ عطا خسرو بزرگ اثر </p></div>
<div class="m2"><p>و یا بلند همم سرور بلند آثار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ایا بنزد تو عاقل بلند و جاهل پست</p></div>
<div class="m2"><p>و یا بپیش تو دانش عزیز و خواسته خوار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر آن تنی که شراب خلاف تو بچشید</p></div>
<div class="m2"><p>زهاب تیغ تو سازد سرش علاج خمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مخالفان تو هر چند کآدمی گهرند</p></div>
<div class="m2"><p>نه آدمی خردند و نه آدمی کردار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز نسل آدم مشمارشان که نشاسند</p></div>
<div class="m2"><p>زمی خمار و زطاوس پای و از گل خار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دل عدوی تو مانند سنگ مغناطیس</p></div>
<div class="m2"><p>کشد سنان ترا سوی خویش در پیکار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بطبع و خلق هماییست تیغ تو ، که همی</p></div>
<div class="m2"><p>بخاصیت شکند ز خمش استخوان سوار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنان ببندد سهم تو خصم را گویی</p></div>
<div class="m2"><p>که گشت موی تنش بر مسام او مسمار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هزار بار بهر لحظه ای فزون خواهد</p></div>
<div class="m2"><p>ز شیر رایت تو شیر آسمان زنهار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عقاب آهن منقار تیر تست و شود</p></div>
<div class="m2"><p>روان خصم ز منقار او بگونۀ قار</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرکبست ز بلغار و هند ، ز آنکه همی</p></div>
<div class="m2"><p>سرش ز هند پدید آید و تن از بلغار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بچرم غرم و بشاخ گوزن بشتابد</p></div>
<div class="m2"><p>بزخم غرم و بصید گوزن روز شکار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بنزد عقل چو هنجار زخم خواهد جست</p></div>
<div class="m2"><p>چو نور عقل در آید براه بی هنجار</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>اگر عدوی تو ار شست بر گشاید تیر </p></div>
<div class="m2"><p>بروید ، ای ملک ، اندر زه کمان سوفار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>طلسم ساخت سکندر ، که مال گیتی را</p></div>
<div class="m2"><p>بقهر بستد و در خشک خاک کرد انبار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر بسد سکندر درون بود زر تو</p></div>
<div class="m2"><p>بطمع سایل بشکافد آهنین دیوار</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شعاع دیدۀ آن کیمیای زر گردد</p></div>
<div class="m2"><p>که دست راد تو بیند بخواب در ، یک بار</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>از آن جهت ، ملکا ، زرد گشت گونۀ زر</p></div>
<div class="m2"><p>که با سخای تو از ذل خویش دارد عار</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو زر بسایل بخشی بدست خویش ببخش</p></div>
<div class="m2"><p>که از نهیب تو گردد برو کآشفته نگار</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حدیث میر خراسان و قصۀ توزیع</p></div>
<div class="m2"><p>بگفت رودکی از روی فخر در اشعار</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بدانچه داده بد او را هزار دیناری</p></div>
<div class="m2"><p>بنا وجوب بهم کرده از صغار و کبار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>تو در هری بشبی ، خسروا ، ببخشدی</p></div>
<div class="m2"><p>زر مدور صافی دو بار بیست هزار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>سخا و فضل و شجاعت ز تو جدا نشود</p></div>
<div class="m2"><p>چو جان ز لفظ و خط از حرف و مرکز از پرگار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز دست طبع و زبانت چنان گریزد بخل</p></div>
<div class="m2"><p>که دیو از آتش ولاحول و لفظ استغفار</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ایا شهنشه مردم شناس مردم دوست</p></div>
<div class="m2"><p>و یا شهنشه چاکر نواز چاکردار</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بگاه مدح تو گویی که روح روشن من</p></div>
<div class="m2"><p>از این کثافت ارکان همی ندارد یار</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چنان صفاوت مدح توام کند صافی</p></div>
<div class="m2"><p>که در دو عالم سازد روان من دیدار</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر روان و زبان مدح تو نگفتندی</p></div>
<div class="m2"><p>نه با روان خردستی ، نه با زبان گفتار</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برنج و سختی یک سال روز بشمردم</p></div>
<div class="m2"><p>بغیبت تو در ، ای عالی آفتاب تبار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رهی ز دانش خواهیم یافت سخت آسان</p></div>
<div class="m2"><p>پس از شمردن این روزگار بس دشوار</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>بدان دلیل که رامش همی شود ، ملکا</p></div>
<div class="m2"><p>که با شکوفه تأمل کنی حروف شمار</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>خدایگا نا،آن روزگار کی باشد</p></div>
<div class="m2"><p>که رایت تو زند در هری لوای شکار؟</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ببینم آخر کز نعل سم مرکب تو</p></div>
<div class="m2"><p>رسد ز خاک فراوان سوی ستاره غبار</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هزار قبه شود بر مثال کوه بلند</p></div>
<div class="m2"><p>بجلوه صف زده طاوس بر یمین و یسار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز قبه های ملون بپای اسب تو در </p></div>
<div class="m2"><p>بجای سیم و زر ، ای شاه ، جان کنیم نثار</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>خجسته روی چو خورشید تو همی بینم</p></div>
<div class="m2"><p>گهی بمجلس بزم و گهی بصفۀ بار</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>همیشه تا نشود خاک چون سپهر لطیف</p></div>
<div class="m2"><p>همیشه تا نکند کوه چون ستاره مدار</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غلام و چاکر و فرمانبر و رهی بادت</p></div>
<div class="m2"><p>بملکت اندر فغفور و رای و قیصر و شار</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همیشه تا که جوانی و ملک بستایند</p></div>
<div class="m2"><p>تو بادیا ، ز جوانی و ملک ، برخوردار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نگاهدار تو بادا خدای عزوجل</p></div>
<div class="m2"><p>بسال و ماه و بنیک و بد و بلیل و نهار</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>باعتقاد من ، ای شاه ، و سوخته دل من</p></div>
<div class="m2"><p>باستجابت پیوندد این دعا ناچار</p></div></div>