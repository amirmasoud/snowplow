---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>گویی که ماه و مشتری از جرم آسمان</p></div>
<div class="m2"><p>تحویل کرده اند بباغ خدایگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز ماه و مشتری شده آن خاک پرنگار</p></div>
<div class="m2"><p>نوری عجیب صورت و شکلی بدیع سان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی نی ، که ماه و مشتری از وی ربوده اند</p></div>
<div class="m2"><p>در نیکویی فزونی و در روشنی توان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویی که بوستان بهشتست بر زمین</p></div>
<div class="m2"><p>رضوان بماه و مشتری آگنده بوستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرجان عود سوز درو شاخ نسترن</p></div>
<div class="m2"><p>مینای مشک سای درو برگ ضیمران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد اندرو بزیده ز پهنای آسگون</p></div>
<div class="m2"><p>ابر اندرو گذشته ز بالای قیروان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دست باد عنبر سارای بی قیاس</p></div>
<div class="m2"><p>در چشم ابر لؤلؤی شهوار بی کران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زلف بنفشه عنبر این سوده در شکن</p></div>
<div class="m2"><p>رخسار لاله لولوی آن کرده در دهان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پروین ارغوان زسر لشکر سمن</p></div>
<div class="m2"><p>بر آسمان کشیده علمهای پرنیان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از سیم خام برگ برآورده یاسمن</p></div>
<div class="m2"><p>با زر پخته گونه بدل کرده اقحوان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در زیر سرو نغمۀ کبکان رود زن</p></div>
<div class="m2"><p>بر شاخ بید نعرۀ مرغان شعر خوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و آن آب نیلگون معلق گمان بری</p></div>
<div class="m2"><p>مالیده قرطه ایست ز پیروزه بهرمان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گویی که باد سودۀ سوهان آژده است</p></div>
<div class="m2"><p>گاهی زند بصیقل و گاهی زند فسان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دانش و ز جان اثری نی درو ولیک</p></div>
<div class="m2"><p>از نیکویی چو دانش و از روشنی چو جان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>و آن قصر کوه پیکر انجم لقا درو</p></div>
<div class="m2"><p>پهنای خاک دارد و بالای آسمان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز آسیب چنبر فلک اندر فراز او</p></div>
<div class="m2"><p>بر کنگره خمیده رود مرد پاسبان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از صحن باغ کنگرۀ او چو بنگری</p></div>
<div class="m2"><p>زان هر یکی خیال خیالی کند عیان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گویی که خرد بچۀ سیمرغ بی عدد</p></div>
<div class="m2"><p>بر کرده اند تیزی منقار از آشیان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>و آن گردش مزمل زرین شگفت را</p></div>
<div class="m2"><p>آبی ، بروشنی چو روان ، اندرو روان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پیروزه همچو سیم کشیده فرو رود</p></div>
<div class="m2"><p>از گوشۀ مزمل زرین بآبدان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گویی ز زر پخته همی پوست بفگنند</p></div>
<div class="m2"><p>تعبان سیم پیکر پیروزه استخوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باغی بدین نشان و بنایی بدین نسق</p></div>
<div class="m2"><p>پاکیزه تر ز کوثر و خرم تر از جنان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در پیش او نشسته و بر پای صف زده</p></div>
<div class="m2"><p>گردان کار دیده و شاهان کامران</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمشید وار شاه نشسته میان باغ</p></div>
<div class="m2"><p>بربسته آدمی و پری پیش او میان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شمس دول ، گزیدۀ ایام ، فخر ملک</p></div>
<div class="m2"><p>تیغ خلیفه ، سایۀ اسلام ، شه طغان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یاقوت ناب در کف او گشته آفتاب</p></div>
<div class="m2"><p>مینای سبز بر سر او بسته سایبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از صوت شعر خوان دل افلاک پر خروش</p></div>
<div class="m2"><p>وز زخم رودزن سر خورشید پر فغان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بر کف نهاده لعل میی کز خیال او</p></div>
<div class="m2"><p>اندیشه لاله زار شود ، دیده گلستان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن می ، که گر زدور بداری ، ز عکس او</p></div>
<div class="m2"><p>شنگرف سوده گردد مغز اندر استخوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر بگذرد پری بشب اندر شعاع او</p></div>
<div class="m2"><p>از چشم آدمی نتواند شدن نهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رنگین میی که بر کفن مرده گر چکد</p></div>
<div class="m2"><p>در تن رگ فسرده شود شاخ ارغوان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن می که بر سپهر اگر پرتو افگند</p></div>
<div class="m2"><p>شاید که آفتاب شود یکسر آسمان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ساقی ز عکس نورش گویی سیاوشست</p></div>
<div class="m2"><p>آتش پناه ساخته از بهر امتحان</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مشکست و لعل و شعری و پروین ، اگر بود</p></div>
<div class="m2"><p>شعری برنگ بسد و پروین ببوی بان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوشبوی تر ز عنبر و رنگین تر از عقیق</p></div>
<div class="m2"><p>روشن تر از ستاره و صافی تر از روان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جامی چو بحر ژرف ، کز و نگذرد همی</p></div>
<div class="m2"><p>عنقا بزخم شهپر و زورق ببادبان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاه آنچنان میی بچنین جام کرده نوش</p></div>
<div class="m2"><p>از دست سیم ساق مهی نوش ناردان</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>دوران خود سپرده بفرمان او فلک</p></div>
<div class="m2"><p>اشغال خویش داده بتوقیع او جهان </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>با حلم او زمین گران چون هوا سبک</p></div>
<div class="m2"><p>با طبع او هوای سبک چون زمین گران</p></div></div>
<div class="b" id="bn40"><div class="m1"><p> ای سروری که نام ترا بندگی کنند</p></div>
<div class="m2"><p>در حد روم قیصر و در خاک ترک خان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از پای همت تو همی تابد آفتاب</p></div>
<div class="m2"><p>وز دست حشمت تو همی گردد آسمان</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از قوت سخای تو هیچ آفرید ه ای</p></div>
<div class="m2"><p>در دست تو قرار نگیرد مگر عنان</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هرچ آن گمان بری تو ، قضا هم بر آن رود</p></div>
<div class="m2"><p>گویی ز کیمیای قضا کرده ای گمان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زان پایدار ماند ستاره ، که روز جنگ</p></div>
<div class="m2"><p>از عکس خنجر تو بیابد همی نشان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در خاک هند رمح ز بیم سنان تو</p></div>
<div class="m2"><p>بگداخت شاخ شاخ و لقب یافت خیزران</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>روزی که آب و آتش ریزد ز تیغ و رمح</p></div>
<div class="m2"><p>این لاله قطره گردد و آن ارغوان دخان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شنگرف بارد از دل زنگار چهره تیغ</p></div>
<div class="m2"><p>بیجاده ریزد از سر پیروزه گون سنان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ور باد زخم ژاله زند ابر هندوی</p></div>
<div class="m2"><p>بر درع لاله کارد و بر جوشن ارغوان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>از هیبت استخوان مبارز چنان شود</p></div>
<div class="m2"><p>کز خوردنش همای کند قصد زعفران</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وز نیزه های رمح دگر عالمی کنند</p></div>
<div class="m2"><p>در دامن ستاره پر افعی و افعوان</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دشمن چو بحر آتش بیند جهان زتو</p></div>
<div class="m2"><p>در موج او نهنگ دلیران جان ستان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مالک کشان کشان سوی دوزخ برد نگون</p></div>
<div class="m2"><p>آنرا که زخم تیغ تو باز افکند سنان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بیرون فگنده نیزۀ خطی بروی دست</p></div>
<div class="m2"><p>و اندر کشیده کرۀ ختلی بزیر ران</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>پیدا شود ز چهرۀ دشمن بچند میل</p></div>
<div class="m2"><p>در گوهر بلارگ تو گنج شایگان</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پیکان بقبضه در کشد از بهر جنگ تو</p></div>
<div class="m2"><p>وز سوی زه خدنگ برون پرد از کمان</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ای اختر سخا ، که ز سیر نوال خویش</p></div>
<div class="m2"><p>هر روز بر سپهر تفاخر کنی قران</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>آب حیات خورد سنان عدوی تو</p></div>
<div class="m2"><p>هر کس که خورد شربت او زیست جاودان</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر طبع جود شکل مکان گیردی ازو</p></div>
<div class="m2"><p>جود ترا هزار فلک بایدی مکان</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>بر کان زر ز دست تو گر صورتی کنند</p></div>
<div class="m2"><p>زر نقش مهر گیرد و بیرون جهد زکان</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بر سکه گر نگار کنی شکل دست خویش</p></div>
<div class="m2"><p>بر زر رقم شود که : ببخشید رایگان</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>از حرص آنکه خواسته بخشی بخواستار </p></div>
<div class="m2"><p>خواهی که موی بر تن سایل شود زبان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>هرکس که بر زبان نیاز از تو بار خواست</p></div>
<div class="m2"><p>او را زجاه وجود تو بودست ترجمان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>خواهی که دشمنانت همه دوستان شوند</p></div>
<div class="m2"><p>تا بیشتر بخلق دهی جاه و سوزیان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>جود تو بی گمان که ضمان را وفا کند</p></div>
<div class="m2"><p>گر خلق را بدادن روزی شود ضمان</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رمح ترا یقین خلیلست روز جنگ</p></div>
<div class="m2"><p>کز آتش سنان تو ناید برو زیان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>گر گوهری ز چشمۀ تیغ تو برکشند</p></div>
<div class="m2"><p>صد جان زنگ خورده برون آرد از میان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>فردوس را بمجلس تو سرزنش کند</p></div>
<div class="m2"><p>آن کس که در سرای تو بودست میهمان</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای خسروی که از کف رادتو زایرت</p></div>
<div class="m2"><p>بر صد هزار گنج فزونست قهرمان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>من بنده از زمانه نژند زمانه ام</p></div>
<div class="m2"><p>گردم مگر بفضل خداوند شادمان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>بیرون نکرد خواهم ، تا عمر من بود</p></div>
<div class="m2"><p>خدمت زجان ، مدیح زدل ، خامه از بنان</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تا ارغوان نگار بود خاک نوبهار</p></div>
<div class="m2"><p>تا زعفران فشان گذرد باد مهرگان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>افزون ز روزگار ملک شادمان زیاد</p></div>
<div class="m2"><p>در نعمت گزیده و در دولت جوان</p></div></div>