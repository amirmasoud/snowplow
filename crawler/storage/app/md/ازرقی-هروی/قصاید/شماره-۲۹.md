---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای مبارک تر از ستارۀ روز</p></div>
<div class="m2"><p>صدمۀ آفتاب صدر افروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل تو علم بین و علم گشای</p></div>
<div class="m2"><p>طبع تو جود و رز و جود آموز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شست آذر مه از کمان هوا</p></div>
<div class="m2"><p>بادها زد چو تیر مردم دوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست سرما فرو درید و سترد</p></div>
<div class="m2"><p>کسوت شاخ و صنعت نوروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جامۀ باغ سوخت بی آتش</p></div>
<div class="m2"><p>خانه ای گرم خواه و آتش سوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیزم گوز را بر آتش نه</p></div>
<div class="m2"><p>که توان بر شمر شکستن گوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زال شد باغ تا نه دیر از برف</p></div>
<div class="m2"><p>چون سر زال زر شود سریوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بند فولاد بر دهن یابد</p></div>
<div class="m2"><p>آهو ، ار بر شمر نهد پتفوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای بهر فضل و شادی ارزانی</p></div>
<div class="m2"><p>بکش این رنج من به فضل امروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طبع اگر آفتاب نظم شود</p></div>
<div class="m2"><p>دست سرما برو بود پیروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر زمستان من تموز کنی</p></div>
<div class="m2"><p>باز رستی زبنده تا بتموز</p></div></div>