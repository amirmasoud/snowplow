---
title: >-
    بخش ۴۶ - نامه نوشتن خسرو به شیرین به تعزیت فرهاد
---
# بخش ۴۶ - نامه نوشتن خسرو به شیرین به تعزیت فرهاد

<div class="b" id="bn1"><div class="m1"><p>چنین دادم خبر مرد سخندان</p></div>
<div class="m2"><p>که چون فرهاد داد از عاشقی جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل شیرین ز داغش گشت مجروح</p></div>
<div class="m2"><p>مدامش کار نوحه بود چون نوح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب دیده ها می شد دلش سست</p></div>
<div class="m2"><p>خیالش روز و شب در آب می جست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو آب چشم خویشش در نظر بود</p></div>
<div class="m2"><p>چرا کز آب صدره پاکتر بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خسرو گر چه بودی خاطرش شاد</p></div>
<div class="m2"><p>ولیکن چیز دیگر بود فرهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو زان تقدیر تدبیری نبودش</p></div>
<div class="m2"><p>نبود آنجا نشستن هیچ سودش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرشکی چند بر خاکش ببارید</p></div>
<div class="m2"><p>به خاکش کرد و زانجا باز گردید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خبر بردند غمازان سوی شاه</p></div>
<div class="m2"><p>که شاها دور شد خرسنگ از راه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مرگش شد دل شیرین پر از درد</p></div>
<div class="m2"><p>برای مردن او گریه ها کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز سوز سینه زد آتش در افلاک</p></div>
<div class="m2"><p>به اعزاز تمامش کرد در خاک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خسرو را خبر دادند ازین حال</p></div>
<div class="m2"><p>ز غم شد چهره اش گه زرد و گه آل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن آتش بر آمد بر سرش دود</p></div>
<div class="m2"><p>چرا کز حال او، او باخبر بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس از یک دم دبیری پیش خود خواند</p></div>
<div class="m2"><p>نثار از مشک بر کافور افشاند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخستین زد رقم بر نام یزدان</p></div>
<div class="m2"><p>که او هم جان ستاند هم دهد جان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خداوندی سزاوار خدایی</p></div>
<div class="m2"><p>خداوندی که از فرمانروایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به باغ و بوستانها باد گستاخ</p></div>
<div class="m2"><p>نمی یارد فکندن برگی از شاخ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پس از نام خدا کردش چنین یاد</p></div>
<div class="m2"><p>که ای شیرین مموی از مرگ فرهاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که از دانا شنیدستم به تکرار</p></div>
<div class="m2"><p>که کس را نیست با تقدیر او کار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه تنها شد به خاک آن تیر از کیش</p></div>
<div class="m2"><p>که ما را نیز هست این راه در پیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چه جای او که عالم سر به سر پاک</p></div>
<div class="m2"><p>همه را نیست منزل جز دل خاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز مرد و زن که بر روی زمین است</p></div>
<div class="m2"><p>چو وابینی همه را راه این است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشد خرم دلم از مرگ فرهاد</p></div>
<div class="m2"><p>وگر گویم دروغ این مرگ من باد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یقینم من که باید مردن آخر</p></div>
<div class="m2"><p>کسی زین جان نخواهد بردن آخر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر چه گاه کامم زو غمی بود</p></div>
<div class="m2"><p>دریغ از وی که خوبم همدمی بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به مرگ او نیم بالله خرم</p></div>
<div class="m2"><p>که اینک می رسد نوبت به من هم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چه داند کس که بی آن یار چونم</p></div>
<div class="m2"><p>دریغ آن محرم راز درونم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرا این تاج و تخت از دولت اوست</p></div>
<div class="m2"><p>مراد و کام و بخت از همت اوست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مراگر شاه صورت کرد مولی</p></div>
<div class="m2"><p>مر او را خواند هم سلطان معنی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو سر بودیم چون پرگار و یک تن</p></div>
<div class="m2"><p>به ملک عشق من او بودم، او من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چه گویم بخت من با من چها کرد</p></div>
<div class="m2"><p>چرا زین سان مرا از من جدا کرد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نبود اسرار من زو هیچ پنهان</p></div>
<div class="m2"><p>که من بودم تن او را او مرا جان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز کوه سنگ اگر برد او ملامت</p></div>
<div class="m2"><p>من و کوه غمم زو تا قیامت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به کوه ار صرف کرد او هستی خویش</p></div>
<div class="m2"><p>مرا صد کوه هستی هست در پیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کمال عاشقی این بود کو برد</p></div>
<div class="m2"><p>من این دولت طلب می کردم او برد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چه خوش زد این مثل آن مرد عاقل</p></div>
<div class="m2"><p>که بد در عاقلی خویش کامل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که پر از بهر دولت دل مکن خون</p></div>
<div class="m2"><p>که آید ناگهان از سنگ بیرون</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کنون ای یار، چون تقدیر این بود</p></div>
<div class="m2"><p>ز تقدیرش حوالت این چنین بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به چشم او نظر کن در من ای جان</p></div>
<div class="m2"><p>مرا خسرو مخوان، فرهاد خود خوان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به مرگ من مکن بر خویش بیداد</p></div>
<div class="m2"><p>مرا می بین و می خوانم به فرهاد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>تویی باقی و ما یکسر فناییم</p></div>
<div class="m2"><p>تو شمعی ما همه پروانه هاییم</p></div></div>