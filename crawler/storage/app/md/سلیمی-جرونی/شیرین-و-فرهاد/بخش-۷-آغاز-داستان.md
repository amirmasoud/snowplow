---
title: >-
    بخش ۷ - آغاز داستان
---
# بخش ۷ - آغاز داستان

<div class="b" id="bn1"><div class="m1"><p>چنین دادم خبر پیر سخن سنج</p></div>
<div class="m2"><p>که در تاریخ عمری برده بد رنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون بر ملک کسری کسر شد ضم</p></div>
<div class="m2"><p>به هرمز گشت سلطانی مسلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عالم کرد زان سان عدل بنیاد</p></div>
<div class="m2"><p>که نام ظلم از عالم برافتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عدلش میش با گرگ آب خوردی</p></div>
<div class="m2"><p>ببردی بچه و او را سپردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل دشمن زتیرش تاب می خورد</p></div>
<div class="m2"><p>که شمشیرش به جیحون آب می خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازو فرزندی آمد به زخورشید</p></div>
<div class="m2"><p>به فر رستم و سیمای جمشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک چون در بلوغیت رساندش</p></div>
<div class="m2"><p>زمانه خسرو پرویز خواندش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بد خلق و دلاویزی تمامش</p></div>
<div class="m2"><p>میان خلق شد پرویز نامش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به عهدش بد بزرگ امید نامی</p></div>
<div class="m2"><p>به حکمت در همه بابی تمامی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملازم ساختش با او شب و روز</p></div>
<div class="m2"><p>که هرچیزی ز هر چیزش بیاموز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دانشور شد آن شاه تنومند</p></div>
<div class="m2"><p>خیالش جانب نخجیر افکند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو سوی صید تیرانداز می شد</p></div>
<div class="m2"><p>دو اسبه صید پیشش باز می شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سوی میدان چو چوگان بازگشتی</p></div>
<div class="m2"><p>سواد نه فلک زو بازگشتی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو آتش در کمانداری می افروخت</p></div>
<div class="m2"><p>شب تاریک چشم مور می دوخت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در بزم آمدی از رسم تعظیم</p></div>
<div class="m2"><p>ازو آموختی جمشید تعلیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مع القصه ز تقدیر الهی</p></div>
<div class="m2"><p>به عزم صید روزی با سپاهی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به رسم خسروان چون بهمن و کی</p></div>
<div class="m2"><p>سوی نخجیر شد با چنگ و بامی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به سوی کشته دهقان گدر کرد</p></div>
<div class="m2"><p>سراسر کشت شان زیر و زبر کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فرود آمد شبی در جای شان نیز</p></div>
<div class="m2"><p>خبر بردند هرمز را که پرویز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرابی کرد اندر کشت دهقان</p></div>
<div class="m2"><p>و زو شد مردمی بی خان و بی مان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان شد تند هرمز زین حکایت</p></div>
<div class="m2"><p>که تندی را نبد زین بیش غایت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که رسم این بود اندر دین ایشان</p></div>
<div class="m2"><p>که می بودند بر آیین پیشان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که در پیش آنچنان بودی شهنشاه</p></div>
<div class="m2"><p>که در ملکش نبودی ظلم را راه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سپهشان هم چه از پیش و چه از پس</p></div>
<div class="m2"><p>نیاوردند خون از بینی کس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر در پای کس یک خار رفتی</p></div>
<div class="m2"><p>به جان شاه صد مسمار رفتی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنون در عهد ما درویش صد خار</p></div>
<div class="m2"><p>خورد تا باغ شاه آرد گلی بار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر آن شاه بود این نیز شاه است</p></div>
<div class="m2"><p>تفاوت بین که صد فرسنگ راه است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن بیداد شاها زانکه یزدان</p></div>
<div class="m2"><p>رعیت گله کرد و شاه چوپان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از آن گشتند شاهان صاحب انساب</p></div>
<div class="m2"><p>که درویشی کند در گوشه ای خواب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس آنگه گفت هرمز تا سپاهی</p></div>
<div class="m2"><p>روند اندر پی اش هر یک به راهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>فرود آرند او را از سواری</p></div>
<div class="m2"><p>بیارندش به خواری و به زاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کسی برد این سخن در دم به پرویز</p></div>
<div class="m2"><p>که شد بر کشتنت شمشیر شه تیز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برند این دم ازین منزل زبونت</p></div>
<div class="m2"><p>گریز ار نه همی پاکست خونت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو خسرو این سخن بشنید ازیشان</p></div>
<div class="m2"><p>به کار خود به غایت شد پریشان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صلاح آن دید کز راهی که دارد</p></div>
<div class="m2"><p>رود زان بوم و رو در غربت آرد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که در غربت ز کربت رنج و خواری</p></div>
<div class="m2"><p>به از ملک خود و نا استواری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به غربت هر که بی آرام باشد</p></div>
<div class="m2"><p>به از جایش که دشمنکام باشد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر آن زد رای خسرو تا که در دم</p></div>
<div class="m2"><p>کند رو در سفر با چند همدم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نباشد زان تنعم بیش خرسند</p></div>
<div class="m2"><p>کند خوش گرم و سرد دهر یک چند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو زر در بوته دوران گدازد</p></div>
<div class="m2"><p>که دوران آدمی را پخته سازد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که آبی کان چراغ دیده گردد</p></div>
<div class="m2"><p>چو در یک جا ستد گندیده گردد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نه مرد است آن که در یک جا اسیر است</p></div>
<div class="m2"><p>که زن در کنج خانه جایگیر است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نداند قدر صحبت هیچ بی درد</p></div>
<div class="m2"><p>جهان دیدن ز خوردن به نهد مرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>هر آن کو روز و شب یک جا نشسته ست</p></div>
<div class="m2"><p>بود چون بازکو را چشم بسته ست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو روگردان بخار از خانه گردد</p></div>
<div class="m2"><p>چو باز آید همه در دانه گردد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چه خوش زد این مثل آن موبد پیر</p></div>
<div class="m2"><p>مثلهای چنین در گوش جان گیر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که پیری کو ندیده علو و سفل است</p></div>
<div class="m2"><p>گرش صد سال عمر آمد که طفل است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شه از اندیشه خوابش در سر افتاد</p></div>
<div class="m2"><p>ز پا بنشست و سر یک لحظه بنهاد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو فکرش شد مصمم شب درین باب</p></div>
<div class="m2"><p>نیای خویشتن را دید در خواب</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که گفتی قدرت افزون می شود زود</p></div>
<div class="m2"><p>سفر کن کاین زیان آمد تو را سود</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به رویش بوسه ها دادی و از جیب</p></div>
<div class="m2"><p>به دستش چارگوهر دادی از غیب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>پس آنگه کردی آنها را تمیزی</p></div>
<div class="m2"><p>نشان از هر یکش دادی به چیزی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نخستین آنکه این تلخی که دیدی</p></div>
<div class="m2"><p>و زان خوشها بدین ناخوش رسیدی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به شیرینی رسی شیرین تر از قند</p></div>
<div class="m2"><p>که باشی از وصالش شاد و خرسند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>دوم در زیر ران یک مرکب آری</p></div>
<div class="m2"><p>که در دولت کنی بر وی سواری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کند دوران چو برتابی لگامش</p></div>
<div class="m2"><p>به روز دولتت شبدیز نامش</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز تو وز مرکبت مانی کشد نقش</p></div>
<div class="m2"><p>مثل ماند ز تو چون رستم و رخش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>سیوم یابی ندیمی نام شاپور</p></div>
<div class="m2"><p>که در وی عقل بیند خیره از دور</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به نقاشی چو بر دیبا زند رنگ</p></div>
<div class="m2"><p>کشد بر سنگ خارا نقش ارژنگ</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چهارم باربد نامی غزل گوی</p></div>
<div class="m2"><p>که راز از چنگ گوید موی برموی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو بلبل از چمن هر گل که بوید</p></div>
<div class="m2"><p>هزاران صورت از یک پرده گوید</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چو گیرد راست در بزم تو ای شاه</p></div>
<div class="m2"><p>مخالف را نیابی سوی خود راه</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو خوابش برد و خواب این نقش در بست</p></div>
<div class="m2"><p>ز شادی در زمان از خواب برجست</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به یاران گفت برخیزید تا زود</p></div>
<div class="m2"><p>روان گردیم ازین جای غم آلود</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>که ما را گر زمانه خواریی کرد</p></div>
<div class="m2"><p>زخار ما دمدگل، سرخ و هم زرد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>که فرمان شد ز جانان کز پی دل</p></div>
<div class="m2"><p>رویم از این زمین منزل به منزل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو شد خسرو ز ملک خود روانه</p></div>
<div class="m2"><p>دلش تیر جفا را شد نشانه</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>خلاف افتاد در یارانش بی حد</p></div>
<div class="m2"><p>یکش گفتی نکو کردی دگر بد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>به ایشان گفت خسرو کای حریفان</p></div>
<div class="m2"><p>مباشید از غم دوران پریشان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که با ما بخت دارد روی یاری</p></div>
<div class="m2"><p>نخواهد بود ما را شرمساری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>چو خسرو این سخن گفت و روان شد</p></div>
<div class="m2"><p>برآمد بادی و گردی عیان شد</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زبعد گرد، از تقدیر بی چون</p></div>
<div class="m2"><p>سواری آمد از آن گرد بیرون</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>چو چشمش بر جمال خسرو افتاد</p></div>
<div class="m2"><p>فرود آمد ز اسب آنجا باستاد</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>ز سختی اسب شه هم نرم گردید</p></div>
<div class="m2"><p>ز مهر او درونش گرم گردید</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بتابید از ره بیگانگی رو</p></div>
<div class="m2"><p>که بوی آشنایی یافت از او</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>بدو گفت از کجایی و چه نامی</p></div>
<div class="m2"><p>که در خوبی، به از ماه تمامی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بگفتا بنده را شاپور نام است</p></div>
<div class="m2"><p>غلام شاه را از جان غلام است</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بسی شیرینی و تلخی چشیده</p></div>
<div class="m2"><p>ز دارالملک چین اینجا رسیده</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>چو دید از خواب خود یک جزو پرویز</p></div>
<div class="m2"><p>به دولت گفت کانها می رسد نیز</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>ز رویش شاد شد کردش به خود یار</p></div>
<div class="m2"><p>نمودش دلنوازیهای بسیار</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>فرود آمد در آن منزل زمانی</p></div>
<div class="m2"><p>همی جستش ز هر چیزی نشانی</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>کزین ره تا به نزد ما رسیدی</p></div>
<div class="m2"><p>بیا برگو چه کردی و چه دیدی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>حکایتها بگو پیشم به تفسیر</p></div>
<div class="m2"><p>که خواهی کرد خوابم را تو تعبیر</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همه احوال خود خسرو بدو هم</p></div>
<div class="m2"><p>بگفت و شد دلش آسوده از غم</p></div></div>