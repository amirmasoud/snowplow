---
title: >-
    بخش ۱۹ - صفت مجلس خسرو و آگاه شدن مهین بانو از حال شیرین
---
# بخش ۱۹ - صفت مجلس خسرو و آگاه شدن مهین بانو از حال شیرین

<div class="b" id="bn1"><div class="m1"><p>غنیمت دان دلا روز جوانی</p></div>
<div class="m2"><p>کزان، خوشتر نباشد زندگانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلالت چون فزونی جست و شد بدر</p></div>
<div class="m2"><p>غنیمت دان مه بدر و شب قدر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهار زندگی روز جوانیست</p></div>
<div class="m2"><p>جوانی خود بهار زندگانیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو مطرب تیز کرد از نغمه آهنگ</p></div>
<div class="m2"><p>جوانا بشنو این از گفته چنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که عاقل او بود کو تا تواند</p></div>
<div class="m2"><p>جوانی را به پیری نگدراند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به فر و دولت و خوبی، شه نو</p></div>
<div class="m2"><p>جوان بخت جهان یعنی که خسرو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشسته بود با خاصان درگاه</p></div>
<div class="m2"><p>که پیش آمد مهین با نوش ناگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپرسیدش که چونی با کرمها</p></div>
<div class="m2"><p>کشیدن نیز چندین زحمت ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر تشریفهای خاص دادش</p></div>
<div class="m2"><p>بر آن داغی که بد مرهم نهادش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دگر گفتش شنیدستم که چند است</p></div>
<div class="m2"><p>که بانو را دل از غم دردمند است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به عالم یک برادر زاده دارد</p></div>
<div class="m2"><p>که مهرش در دل و جان می نگارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ازو گشته ست در نخجیر گه گم</p></div>
<div class="m2"><p>پری سان رفته است از چشم مردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا امروز پیکی آمد از راه</p></div>
<div class="m2"><p>حکایت کرد نزد من از آن ماه</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز من بانو گرش این است مقصود</p></div>
<div class="m2"><p>فرستم پیش بانو آردش زود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بانو این سخن بشنید فی الحال</p></div>
<div class="m2"><p>رخ زردش ز خون دیده شد آل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل غمدیده اش بسیار شد شاد</p></div>
<div class="m2"><p>به دست و پای خسرو بوسه ها داد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که گر خسرو کند زین گونه احسان</p></div>
<div class="m2"><p>کنیزی باشم او را از کنیزان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ولی خواهم که چون بینم شهنشاه</p></div>
<div class="m2"><p>رود آنجا که آرد پیشم آن ماه</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مرا اسبی است آن همزاد شبدیز</p></div>
<div class="m2"><p>که همچون اوست اندر شبروی تیز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ورا گلگون باد آهنگ نام است</p></div>
<div class="m2"><p>که او را باد در تیزی غلام است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر این دولت ز دست او برآید</p></div>
<div class="m2"><p>وزین بند غمم دل برگشاید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دهم شکرانه اش آن اسب نیکو</p></div>
<div class="m2"><p>به جان هم نیز منت دانم از او</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نشیند بر وی و پیشش رود زود</p></div>
<div class="m2"><p>که این آتش بود شبدیز چون دود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که با شبدیز چون در ره کند گرد</p></div>
<div class="m2"><p>بجز گلگون نیارد هم تکی کرد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس آنگه گفت خسرو تا که در حال</p></div>
<div class="m2"><p>رود شاپور شیرین را به دنبال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نگیرد هیچ گه آرام در دل</p></div>
<div class="m2"><p>به یک منزل فرو راند دو منزل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بشنید این سخن شاپور برخاست</p></div>
<div class="m2"><p>عزیمت کرد و برگ ره بیاراست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی ملک مداین رفت چون باد</p></div>
<div class="m2"><p>در آن ره هیچ گه یک دم ناستاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شد از مشکوی خسرو جست آن ماه</p></div>
<div class="m2"><p>سوی قصرش فرستادند از راه</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به سوی قصر شد شاپور در زد</p></div>
<div class="m2"><p>سر از بام آن پری چون ماه برزد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتا کیست کانجا یافت دستور</p></div>
<div class="m2"><p>بگفتا بنده درگاه، شاپور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بشنید این سخن شمع شب افروز</p></div>
<div class="m2"><p>شبش گفتی برآمد ناگهان روز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنیزی را بفرمود او کز ایدر</p></div>
<div class="m2"><p>به تعظیمی تمام آریدش از در</p></div></div>