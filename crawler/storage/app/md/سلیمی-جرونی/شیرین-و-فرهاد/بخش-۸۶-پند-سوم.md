---
title: >-
    بخش ۸۶ - پند سوم
---
# بخش ۸۶ - پند سوم

<div class="b" id="bn1"><div class="m1"><p>سیوم پند این بود ای سرو آزاد</p></div>
<div class="m2"><p>که بر حسن و جوانی پر مشو شاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من بشنو سخن تا می توانی</p></div>
<div class="m2"><p>مشو مغرور، بر حسن و جوانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد را با جوان، بیگانگی دان</p></div>
<div class="m2"><p>جوانی شاخی از دیوانگی دان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به ملک عاقلی باشد امیری</p></div>
<div class="m2"><p>جوانی را به سر کردن به پیری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جوانا پرستم بر خویش مپسند</p></div>
<div class="m2"><p>که از پیران شنیدستم من این پند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که نبود در جوانی غیر مستی</p></div>
<div class="m2"><p>غم پیری، جوانی خور که رستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنه روز جوانی بهر خود برگ</p></div>
<div class="m2"><p>جوان تا پیر باشد، پیر تا مرگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوانی را که از وی در عذابی</p></div>
<div class="m2"><p>غنیمت دان که مشکل بازیابی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مباش از کار و کرد خویش خشنود</p></div>
<div class="m2"><p>که غفلت نقد عمرت جمله بر بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوانی را کزو هستت حسابی</p></div>
<div class="m2"><p>گه پیری، خیالی دان و خوابی</p></div></div>