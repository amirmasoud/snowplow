---
title: >-
    بخش ۴ - گفتار در نعت سید المرسلین صلی الله علیه و سلم
---
# بخش ۴ - گفتار در نعت سید المرسلین صلی الله علیه و سلم

<div class="b" id="bn1"><div class="m1"><p>ندارم وصف و تحمیدی که نیکوست</p></div>
<div class="m2"><p>سزای آنکه مقصود از جهان اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محمد آن که در ملک نکویی</p></div>
<div class="m2"><p>بود صد بار به از هر چه گویی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرد حیران در آن وصف و بیانست</p></div>
<div class="m2"><p>که هر وصفش که گویم بیش از آنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امام جمله نزدیکان درگاه</p></div>
<div class="m2"><p>چراغ بارگاه لی مع الله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زده جبریل از چاووشیش لاف</p></div>
<div class="m2"><p>کشیده خوان جودش قاف تا قاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندیده از بزرگی هیچ قلت</p></div>
<div class="m2"><p>ازو در گفت هفتاد و دو ملت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدان در تا که باشد نقش عالم</p></div>
<div class="m2"><p>غلامش یوسف است و داه مریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سپردند از برای یاری او</p></div>
<div class="m2"><p>به ابراهیم خوانسالاری او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به وادی وادی از نوعی که دانی</p></div>
<div class="m2"><p>شعیب و موسیش کرده شبانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بهر چشم زخم او به فرمان</p></div>
<div class="m2"><p>ذبیح الله کرده خویش قربان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بمردند و ندیدند آن رخ خوب</p></div>
<div class="m2"><p>دریغا عمر نوح و صبر ایوب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نیامد تا بنا کردند عالم</p></div>
<div class="m2"><p>خلف تر زو ز فرزندان آدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تمام انبیا کردند امامش</p></div>
<div class="m2"><p>زدند این سکه دولت به نامش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگو بیش از ره صورت کم و بیش</p></div>
<div class="m2"><p>که بود از راه معنی از همه بیش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به صورت چون زدی لطفش تکلم</p></div>
<div class="m2"><p>کلیم الله کردی دست و پا گم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به معنی روح ازو بودش تاسف</p></div>
<div class="m2"><p>خجل کردی به صورت حسن یوسف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در آن منزل که کرسی آمدش فرش</p></div>
<div class="m2"><p>قدم یک پایه بالاتر زد از عرش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به معراج آمده از حق خبرده</p></div>
<div class="m2"><p>که «سبحان الذی اسری بعبده»</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>وزان کروبیان یکجا خزیده</p></div>
<div class="m2"><p>ستاده وز تحیر لب گزیده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شب اسری که حق رخسار دیدش</p></div>
<div class="m2"><p>خرد الکن شد از گفت و شنیدش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بدش چاکر سلیمان بن داوود</p></div>
<div class="m2"><p>از آن دیو و پری فرمانبرش بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کویش گر نه خود را بنده کردی</p></div>
<div class="m2"><p>مسیحا مرده را چون زنده کردی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شده الیاسش اندر بحر جویان</p></div>
<div class="m2"><p>نهاده خضر ازو رو در بیابان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز لفظ چون درش گوش جهان پر</p></div>
<div class="m2"><p>جهانی را به فرمانش تبختر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به عالم پنج نوبت چون فرو داد</p></div>
<div class="m2"><p>مسیحا از فلک نوبت بدو داد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که نتوان داشتن این کار را سست</p></div>
<div class="m2"><p>شدم من، باش، کاکنون نوبت توست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به چاووشی درگاهت چو دم زد</p></div>
<div class="m2"><p>از آن بر طارم چارم علم زد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بدو گر حق نگفتی «قم فانذر»</p></div>
<div class="m2"><p>کسی هرگز ندانستی هر از بر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جهان افروز بد زان اندرین باغ</p></div>
<div class="m2"><p>به چشم نرگسش دادند«ما زاغ»</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو تیغ معجزش در دو جهان زد</p></div>
<div class="m2"><p>سرانگشتش قمر را بر میان زد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز ابرویش چو بگشودند روپوش</p></div>
<div class="m2"><p>فلک را حلقه ای کردند در گوش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>سلیمی هر دری کز وصف او سفت</p></div>
<div class="m2"><p>ز دریای دو عالم قطره ای گفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خداوندا سلیمی خوش فقیر است</p></div>
<div class="m2"><p>به دست نفس نافرمان اسیر است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به روی او در از هر باب بگشای</p></div>
<div class="m2"><p>وزان یک یک به خویشش راه بنمای</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همه توفیق و دولت یار او کن</p></div>
<div class="m2"><p>سعادت جاودان در کار او کن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدین نظم و کلامش یاوری ده</p></div>
<div class="m2"><p>مدد از قوت پیغمبری ده</p></div></div>