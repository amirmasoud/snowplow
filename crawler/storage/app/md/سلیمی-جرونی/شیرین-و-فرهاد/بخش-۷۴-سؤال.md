---
title: >-
    بخش ۷۴ - سؤال
---
# بخش ۷۴ - سؤال

<div class="b" id="bn1"><div class="m1"><p>دگر گفتش زمان با آدمی زاد</p></div>
<div class="m2"><p>چه نسبت دارد این را یاد کن یاد</p></div></div>