---
title: >-
    بخش ۳ - گفتار در مناجات
---
# بخش ۳ - گفتار در مناجات

<div class="b" id="bn1"><div class="m1"><p>خدایا رحمتی در کار من کن</p></div>
<div class="m2"><p>به لطف خود هدایت یار من کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در کار تو از بایسته تو</p></div>
<div class="m2"><p>نکردم آنچه بد شایسته تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آن روزی که باشد عرض اکبر</p></div>
<div class="m2"><p>نداند کس در آنجا پای از سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر تو نامه ام را در نوردی</p></div>
<div class="m2"><p>وگر نه چون کشم این روی زردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به درگاه تو آوردم الهی</p></div>
<div class="m2"><p>گناهی بیشتر از هر چه خواهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گناه من اگر بر رویم آری</p></div>
<div class="m2"><p>فغان از خجلت، آه از شرمساری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زبس کز من گناه آمد پدیدار</p></div>
<div class="m2"><p>ز تقصیرات و زلتهای بسیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهود از ملت من عار دارد</p></div>
<div class="m2"><p>زمن به آنکه او زنار دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زمستانست و راهی دور در پیش</p></div>
<div class="m2"><p>بضاعت اندک و من خسته و ریش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی در گو فتاده گاه در چاه</p></div>
<div class="m2"><p>وز اینها جمله بدتر کاندرین راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به سرما مردم و دودی نکردم</p></div>
<div class="m2"><p>بضاعت دادم و سودی نکردم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بیابانست و شب تاریک و مستم</p></div>
<div class="m2"><p>بکن رحمی که این حالت که هستم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه جان همره، نه دل با خویش دارم</p></div>
<div class="m2"><p>نه راه پس، نه راه پیش دارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظر کن کاندرین عجز و اسیری</p></div>
<div class="m2"><p>چه خواهم کرد اگر دستم نگیری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدم، یک ذره نیکی نیست در من</p></div>
<div class="m2"><p>اگر نه تو ببخشی وای بر من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به بیدای ضلالت گشته ام گم</p></div>
<div class="m2"><p>نمایم ره، مکن رسوای مردم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مده فخری که آن در عارم آرد</p></div>
<div class="m2"><p>بزن شاخی که خجلت بارم آرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بازاری که مردم در خروشند</p></div>
<div class="m2"><p>در آن چیزی خرند و هم فروشند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر کس ننگرد سویم به چیزی</p></div>
<div class="m2"><p>نرنجم چون نمی ارزم پشیزی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>درین ره کاندرو آرامگه نیست</p></div>
<div class="m2"><p>نه از پیش و نه از پس هیچ ره نیست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو من کس نیست ره سویی نبرده</p></div>
<div class="m2"><p>همه سود و همه سرمایه خورده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شبم حمال سرگین همچو مبرز</p></div>
<div class="m2"><p>همه روز آفتابی می کنم گز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پریشان حال و سرگردان، چه دانی</p></div>
<div class="m2"><p>سگی مانم که ماند از کاروانی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر چه مدتی گمراه بودم</p></div>
<div class="m2"><p>به عصیان رانده درگاه بودم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خدایا آمدم بازت به درگاه</p></div>
<div class="m2"><p>و زان تقصیرها استغفرالله</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در آن ساعت که پیشت عذرخواهان</p></div>
<div class="m2"><p>به عذر آیند از شرم گناهان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گنه شوید گر ابر رحمتت چه؟</p></div>
<div class="m2"><p>چه کم گردد ز بحر رحمتت چه؟</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>توقع از تو هست ای پادشاهم</p></div>
<div class="m2"><p>که شویی جامه جان از گناهم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که آبی نبود اندر بحر و در جوی</p></div>
<div class="m2"><p>ز آب رحمتت بهتر گنه شوی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آن روزی که تو از پادشاهی</p></div>
<div class="m2"><p>ازین مشتی گدا اعمال خواهی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نمی دانم چه چیزت پیش آرم</p></div>
<div class="m2"><p>مگر تقصیرهای خویش آرم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بکن لطفی و راهم ده سوی خویش</p></div>
<div class="m2"><p>مکن واپس در این راهم از آن بیش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کاجل این شربت مرگم چشاند</p></div>
<div class="m2"><p>وز این خاک خودی گردم نماند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پر از گرد خودم آنگه شوم پاک</p></div>
<div class="m2"><p>کاجل گردم برد زین تخته خاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در آن لحظه که پیشت جان سپارم</p></div>
<div class="m2"><p>فرومانم به بدهایی که دارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه راهی کایم و دمساز گردم</p></div>
<div class="m2"><p>نه روی آنکه از در باز گردم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ندارم شک که بدهایم ببخشی</p></div>
<div class="m2"><p>نرانی از در و جایم ببخشی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>رهانی نفس من از رنگ مایی</p></div>
<div class="m2"><p>کنی عفوم ز کافر ماجرایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>الها خالقا پروردگارا</p></div>
<div class="m2"><p>کریما راحما آمرزگارا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>توقع دارم از انعام عامت</p></div>
<div class="m2"><p>به نور احمد و سر کلامت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که باز آری سلیمی را به راهش</p></div>
<div class="m2"><p>ببخشی از کرم جرم و گناهش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دگر او را ز راه لطف و احسان</p></div>
<div class="m2"><p>نگه داری ز بازیهای شیطان</p></div></div>