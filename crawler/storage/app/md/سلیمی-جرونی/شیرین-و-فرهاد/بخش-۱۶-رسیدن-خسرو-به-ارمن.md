---
title: >-
    بخش ۱۶ - رسیدن خسرو به ارمن
---
# بخش ۱۶ - رسیدن خسرو به ارمن

<div class="b" id="bn1"><div class="m1"><p>چو شب اشک زلیخا ریخت برخاک</p></div>
<div class="m2"><p>برآمد یوسف خورشید از افلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سپاه روم، شد بر زنگ، فیروز</p></div>
<div class="m2"><p>به تخت شرق، آمد خسرو روز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برآمد رومیی ناگه به صد فن</p></div>
<div class="m2"><p>سر زنگی شب، برداشت از تن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب اشک خویش ازین حسرت ببارید</p></div>
<div class="m2"><p>سحر از گریه های او بخندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خسرو کشیده زحمت تن</p></div>
<div class="m2"><p>فرود آمد سوی صحرای ارمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سوی چشمه ای شد رخت بگشود</p></div>
<div class="m2"><p>سر و تن شست و یک ساعت بیاسود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بعد یک دو ساعت لشکرش نیز</p></div>
<div class="m2"><p>فرو راندند از دنبال پرویز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو لشکر سر به سر آنجا رسیدند</p></div>
<div class="m2"><p>به صحرا خیمه بر خیمه کشیدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بربانو کسی بشتافت از راه</p></div>
<div class="m2"><p>که آمد خسرو پرویز ناگاه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مهین بانو چو آگه گشت زین کار</p></div>
<div class="m2"><p>سوی پرویز شد با گنج بسیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تبرکهای شایسته که شایست</p></div>
<div class="m2"><p>نثار و پیشکش چندان که بایست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فرسها را همه زین کرده از زر</p></div>
<div class="m2"><p>غلامان و کنیزان نیز یکسر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خروش غلغل از مه تا به ماهی</p></div>
<div class="m2"><p>می و رامشگران چندانکه خواهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز بس کافتاده بد در هر کناره</p></div>
<div class="m2"><p>نیامد نقل و میوه در شماره</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بس کالوان نعمت بود بر هم</p></div>
<div class="m2"><p>ز مرغ و بره ها چیزی نبدکم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستاره حاجب و خود چون مه نو</p></div>
<div class="m2"><p>زمین بوسید پیش تخت خسرو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که خسرو چون علم زین بام افراشت</p></div>
<div class="m2"><p>مر این بیچاره را از خاک برداشت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>توقع دارم از سلطان عالم</p></div>
<div class="m2"><p>که در دولت در اینجا شاد و خرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بود ما را به سلطانی پناهی</p></div>
<div class="m2"><p>بیاساید در اینجا چند گاهی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو خسرو این همه اعزاز ازو دید</p></div>
<div class="m2"><p>زمینی خوب و صحرایی نکو دید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اجابت کرد و آنگه با دل شاد</p></div>
<div class="m2"><p>به دارالملک ارمن رخت بنهاد</p></div></div>