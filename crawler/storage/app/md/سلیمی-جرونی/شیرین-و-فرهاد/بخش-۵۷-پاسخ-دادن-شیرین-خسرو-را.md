---
title: >-
    بخش ۵۷ - پاسخ دادن شیرین، خسرو را
---
# بخش ۵۷ - پاسخ دادن شیرین، خسرو را

<div class="b" id="bn1"><div class="m1"><p>دگر ره آن سهی سرو از سر ناز</p></div>
<div class="m2"><p>به خسرو کرد چشم از دلبری باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو لب بگشود و چشم از خواب بر کرد</p></div>
<div class="m2"><p>جهان پر شکر و بادام تر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو گیسو را زپیش رو در آویخت</p></div>
<div class="m2"><p>بنفشه با گل نسرین برآمیخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گه او شب راه بر مهتاب می زد</p></div>
<div class="m2"><p>ز نرگس، گاه بر گل آب می زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو بادامش ز نرگس خواب می برد</p></div>
<div class="m2"><p>به شیرینی ز شکر آب می برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دلجویی چو سروی، بلکه بهتر</p></div>
<div class="m2"><p>ز رعنایی، هزارش ناز در سر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پاسخ گفت کای سلطان عالم</p></div>
<div class="m2"><p>به زیبایی و خوبی، جان عالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نیامد همچو تو شاهی به عالم</p></div>
<div class="m2"><p>به زیبایی ز آدم تا بدین دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را در شاهی ار فغفور چین است</p></div>
<div class="m2"><p>به درگاهت غلام کمترین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تو را منشور شاهی هست در مشت</p></div>
<div class="m2"><p>ز آدم تا بدین دم پشت بر پشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز هر مهتر که پیش آمد مهی تو</p></div>
<div class="m2"><p>ز جمشید و ز کیخسرو بهی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه کیخسرو، چه افریدون، چه جمشید</p></div>
<div class="m2"><p>که بادا تا ابد ملک تو جاوید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو رو با عالم عقبی نهادند</p></div>
<div class="m2"><p>همه رفتند و دولت با تو دادند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا گفتی که تا کی مستمندی</p></div>
<div class="m2"><p>کشم وز دست جورت دردمندی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منه زین بیش بارم بر دل ریش</p></div>
<div class="m2"><p>که هر کس می کشد بار دل خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بباید ز آرزوی دل، بریدن</p></div>
<div class="m2"><p>وگر نه بایدت اینها کشیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر گفتی که چشمت شیرگیر است</p></div>
<div class="m2"><p>کمانت ابرو و مژگانت تیر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه از حق، نی ز کس اندیشه داری</p></div>
<div class="m2"><p>هزاران مکر و دستان پیشه داری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دو چشمت جادوی مردم فریب است</p></div>
<div class="m2"><p>دل و جانم ز عشقت ناشکیب است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نکردی هیچ در کارم نگاهی</p></div>
<div class="m2"><p>نمی بینم رخ خوبت به ماهی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز تیر غم مزن بر سینه ام چاک</p></div>
<div class="m2"><p>میفکن بیشم و برگیرم از خاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مکن، چون تیر بر خاکم مینداز</p></div>
<div class="m2"><p>بهل تا گردم از تیغت سرافراز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگردیدم ز تیغت خسته و ریش</p></div>
<div class="m2"><p>که این از طالع خویش آمدم پیش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز تیرت جان خود را زنده دارم</p></div>
<div class="m2"><p>ز تیغت سر به بر افکنده دارم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زنم چندان به زاری بر درت سر</p></div>
<div class="m2"><p>که بر رویم گشایی عاقبت در</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بلی اینها که گفتی خوب و زیباست</p></div>
<div class="m2"><p>که همچون جامه ای بر قامت ماست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دگر گفتی به دیوان الهی</p></div>
<div class="m2"><p>یکی کردند درویشی و شاهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گذشتم از سر شاهی به بویت</p></div>
<div class="m2"><p>به مسکینی شدم درویش کویت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نشستم بر سر کوی تو خاموش</p></div>
<div class="m2"><p>شدم همچون در تو حلقه در گوش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نکو رفتی و خوش کردی، چنین به</p></div>
<div class="m2"><p>مده از دست، این حالت که این به</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که هر شاهی که او درویش باشد</p></div>
<div class="m2"><p>به قدر از هر دو عالم بیش باشد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خوشا آن کو به هستی مبتلا نیست</p></div>
<div class="m2"><p>چو درویشی و شاهی را بقا نیست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شهان زین سر برین ایوان کشیدند</p></div>
<div class="m2"><p>که سر در پای درویشان کشیدند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شهان آن به که با ایشان پناهند</p></div>
<div class="m2"><p>که درویشان به عالم پادشاهند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوش آن شاهی که بگدشت از سر ناز</p></div>
<div class="m2"><p>به پابوس فقیران شد سرافراز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو شاهی را بود رو در تباهی</p></div>
<div class="m2"><p>خوشا درویش و ملک پادشاهی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو را کردند از آن شاه جهانی</p></div>
<div class="m2"><p>که درویشی بیاساید زمانی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز بهر گنج و شاهی جان مفرسا</p></div>
<div class="m2"><p>برو در کنج درویشی بیاسا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غم شاه از پی گنج و سپاه است</p></div>
<div class="m2"><p>گدا در کنج وحدت پادشاه است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو شاهی می نهد بر سینه داغت</p></div>
<div class="m2"><p>خوشا درویشی و کنج فراغت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دگر شاها تو گر این فکر داری</p></div>
<div class="m2"><p>که زین سان کام خود از من برآری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مکن اندیشه این، کان خیال است</p></div>
<div class="m2"><p>خیالی باطل و فکری محال است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به یکتایی که مثلش کس ندیده ست</p></div>
<div class="m2"><p>به دانایی که ما را آفریده ست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به سبحانی که سیاحان افلاک</p></div>
<div class="m2"><p>بدو تسبیح گویند از دل پاک</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به معماری که بر فیروزه درگاه</p></div>
<div class="m2"><p>گهی مهر آورد گاهی برد ماه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به علامی که هر چه او کرد نیکوست</p></div>
<div class="m2"><p>جهان حرف و کلام و نسخه اوست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به معبودی که ما با هم رسانید</p></div>
<div class="m2"><p>که نتوانی چنین کام از لبم دید</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مگر با من به پاکی عقد بندی</p></div>
<div class="m2"><p>که از پستی نخیزد سر بلندی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس آنگه روی از خسرو بگرداند</p></div>
<div class="m2"><p>و زان خسرو به کار خویش درماند</p></div></div>