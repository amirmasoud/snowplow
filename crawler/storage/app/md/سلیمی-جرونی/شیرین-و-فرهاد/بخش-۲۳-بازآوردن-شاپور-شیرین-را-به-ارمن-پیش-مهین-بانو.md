---
title: >-
    بخش ۲۳ - بازآوردن شاپور شیرین را به ارمن پیش مهین بانو
---
# بخش ۲۳ - بازآوردن شاپور شیرین را به ارمن پیش مهین بانو

<div class="b" id="bn1"><div class="m1"><p>چو شد شاپور و از آن قصر سنگین</p></div>
<div class="m2"><p>سوی خسرو به ارمن برد شیرین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جان درماند کز آن جای، پرویز</p></div>
<div class="m2"><p>سوی شهر مداین رفته بد تیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گلزار مهین بانو به اعزاز</p></div>
<div class="m2"><p>فرود آورد شیرین را دگر باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چمن را رونق از گل داد دیگر</p></div>
<div class="m2"><p>جهان، دیگر جوانی یافت از سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنیزان چون که او با ارمن آمد</p></div>
<div class="m2"><p>تو گفتی باز جانشان با تن آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنان کز هجر بیند یار را یار</p></div>
<div class="m2"><p>ز شادی گریه ها کردند بسیار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مهین بانو هم از آن داغ و آن درد</p></div>
<div class="m2"><p>چنین دولت به خود باور نمی کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی آتش همی شد گه می افسرد</p></div>
<div class="m2"><p>به رویش زنده می گردید و می مرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حدیثی کز لب شیرین شنفتی</p></div>
<div class="m2"><p>شدی در گریه و در گریه گفتی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ازین روزی به عالم بهترم نیست</p></div>
<div class="m2"><p>تویی اینجا نشسته باورم نیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز دوری تو جان بد رفته از تن</p></div>
<div class="m2"><p>خدا دیگر عنایت کرد با من</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در این انده که هرگز نیستم یاد</p></div>
<div class="m2"><p>بدم مرده که بازم جان نو داد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگر جان منی ای کامرانی</p></div>
<div class="m2"><p>که دور از تو ندیدم زندگانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه زان بهتر مرا ای جان شیرین</p></div>
<div class="m2"><p>که وقت مردنم باشی به بالین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه باشد به پس از مردن جز اینم</p></div>
<div class="m2"><p>که در ارمن تو باشی جانشینم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بانو این نوازشها نمودش</p></div>
<div class="m2"><p>به بهتر مدح و تعریفی ستودش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دگر با دختران شیرین چو پیوست</p></div>
<div class="m2"><p>به عیش و ناز و نوشانوش بنشست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از اول آن پری رویان سراسر</p></div>
<div class="m2"><p>نمودندش نوازشهای بهتر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در آن عشرت که می کرد آن مه نو</p></div>
<div class="m2"><p>تن آنجا داشت دل (در) پیش خسرو</p></div></div>