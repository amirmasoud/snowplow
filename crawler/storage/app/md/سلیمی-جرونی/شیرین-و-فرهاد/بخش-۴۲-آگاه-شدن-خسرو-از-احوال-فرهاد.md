---
title: >-
    بخش ۴۲ - آگاه شدن خسرو از احوال فرهاد
---
# بخش ۴۲ - آگاه شدن خسرو از احوال فرهاد

<div class="b" id="bn1"><div class="m1"><p>چو از فرهاد خسرو آگهی یافت</p></div>
<div class="m2"><p>دلش چون کوره آهنگری تافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان زآن آتش غیرت برافروخت</p></div>
<div class="m2"><p>که قهرش تا به مغز استخوان سوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به حالش ظاهرا یک دم بخندید</p></div>
<div class="m2"><p>ولی در اندرون چون مار پیچید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفس می زد گهی سخت و گهی نرم</p></div>
<div class="m2"><p>ولی چون سیخ کز آبش بود گرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خلوت رفت و در بر مردمان بست</p></div>
<div class="m2"><p>به غم در کنج خلوت زار بنشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مژگان خانه را جاروب گشته</p></div>
<div class="m2"><p>زبانش در دهان چون چوب(گشته)</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی گشتی کج و گاهی شدی راست</p></div>
<div class="m2"><p>زمانی می نشست و گاه می خاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بیمار از تب محرق به بستر</p></div>
<div class="m2"><p>گهی پایش به بالین بود و گه سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرفته چشمهاش از گریه آماس</p></div>
<div class="m2"><p>ولی با صد هزار اندوه و وسواس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دندان پشتهای دست خسته</p></div>
<div class="m2"><p>به دل هر دم هزاران نقش بسته</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی گفتی کشم زارش به خواری</p></div>
<div class="m2"><p>دگر گفتی که نبود شرط یاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نشاید کرد چون خاک رهش پست</p></div>
<div class="m2"><p>که او را نیز همچون من دلی هست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نه روی آنکه بگدارد چنانش</p></div>
<div class="m2"><p>نه رای آنکه آرد قصد جانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آنگه رای زد آن شاه با داد</p></div>
<div class="m2"><p>که خواند سوی تخت خویش، فرهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تعظیمش به نزد خود نشاند</p></div>
<div class="m2"><p>و زو احوال او را باز داند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر عشقش نباشد پاک و بی غش</p></div>
<div class="m2"><p>کشد او را و سوزاند در آتش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>وگر باشد در او عشق خدایی</p></div>
<div class="m2"><p>به حرمت یابد از چنگش رهایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پس آنگه قاصدی را خواند چون باد</p></div>
<div class="m2"><p>روان کردش به جست و جوی فرهاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتش چون بدان بیدل بدی راه</p></div>
<div class="m2"><p>به تعظیمش بیاری سوی درگاه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به خاطر سازش و می باش حاضر</p></div>
<div class="m2"><p>که گردی نایدش از ما به خاطر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو بشنید این سخن قاصد ز پرویز</p></div>
<div class="m2"><p>به جست و جوی آن گم گشته شد تیز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به کوه و دشت و صحرا گشت بسیار</p></div>
<div class="m2"><p>بدیدش بعد از آن در گوشه غار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فتاده زار و رو بنهاده بر خاک</p></div>
<div class="m2"><p>پریشان خاطر و مجروح و غمناک</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو قاصد دید آن دیوانه مست</p></div>
<div class="m2"><p>ز دورش مرحبایی گفت و بنشست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پسش گفتا که خیر است ای جوانمرد</p></div>
<div class="m2"><p>چرا با اشک سرخی و رخ زرد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جوابش داد فرهاد از سر سوز</p></div>
<div class="m2"><p>که هرگز دیده ای کس را بدین روز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که ای تو وز کدامین سرزمینی</p></div>
<div class="m2"><p>چه می پرسی ز من اینم که بینی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه می پرسی ز حالم، حالم اینست</p></div>
<div class="m2"><p>زبانی لال دارم فالم اینست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز زلف ماه رویی تاب دارم</p></div>
<div class="m2"><p>نه روز آرام و نی شب خواب دارم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نیم هرگز ز بخت خویش فیروز</p></div>
<div class="m2"><p>نه روز از شب شناسم نی شب از روز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مبین بگدشته آب چشم از فرق</p></div>
<div class="m2"><p>دلم را بین در او صد بحر خون غرق</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بود زین سان که بینی حال فرهاد</p></div>
<div class="m2"><p>تو را اینجا گدر بهر چه افتاد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جوابش گفت قاصد، گفت برخیز</p></div>
<div class="m2"><p>که می خواند تو را این لحظه پرویز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو بشنید این سخن فرهاد غمناک</p></div>
<div class="m2"><p>بزد آهی که زد آتش بر افلاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بگفت آوه که کار من تبه شد</p></div>
<div class="m2"><p>همه روز سفید من سیه شد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نشد کارم به بخت تیره از پیش</p></div>
<div class="m2"><p>ندیدم راحت از بیگانه و خویش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به قاصد گفت کای مرد جهانگرد</p></div>
<div class="m2"><p>برو ما را رها کن با غم و درد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو من سر با سر تقدیر دارم</p></div>
<div class="m2"><p>چه فکر از پادشاه و میر دارم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا پروای جان خویشتن نیست</p></div>
<div class="m2"><p>سخن گفتم همه، دیگر سخن نیست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ازو قاصد چو این گفتار بشنید</p></div>
<div class="m2"><p>به پیشش روی خود بر خاک مالید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که فرهادا به امیدی که داری</p></div>
<div class="m2"><p>که برخیزی و کام من بر آری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که گر من بی تو روی آرم سوی شاه</p></div>
<div class="m2"><p>بیاویزند بر حلقم ز درگاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر نه تشنه خون منی خیز</p></div>
<div class="m2"><p>که تا آریم رو نزدیک پرویز</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کنون رحمی بکن بر حال زارم</p></div>
<div class="m2"><p>سیه بر من مگردان روزگارم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو بشنید این سخن فرهاد پر درد</p></div>
<div class="m2"><p>روان برجست و با وی عزم ره کرد</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به همراهی قاصد با دل ریش</p></div>
<div class="m2"><p>به درگاه شه آمد مست و بی خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چو سوی درگه آوردندش از راه</p></div>
<div class="m2"><p>ببردندش به نزدیک شهنشاه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو دید او را شهنشه پیش خواندش</p></div>
<div class="m2"><p>به صد تعظیم نزد خود نشاندش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>پس آنگه نیک چون در چهره اش دید</p></div>
<div class="m2"><p>ز هیبت بند بر بندش بلرزید</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگفتا هیبتش از پارسائیست</p></div>
<div class="m2"><p>ندارم شک که این عشق خدائیست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو شد در لرزه شاه از هیبت او</p></div>
<div class="m2"><p>به خود دانست واجب عزت او</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>در او و هیبت او گشت حیران</p></div>
<div class="m2"><p>سؤالی چند کرد از وی بدین سان</p></div></div>