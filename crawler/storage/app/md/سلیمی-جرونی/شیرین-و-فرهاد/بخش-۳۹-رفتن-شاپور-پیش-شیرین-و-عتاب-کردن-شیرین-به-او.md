---
title: >-
    بخش ۳۹ - رفتن شاپور پیش شیرین و عتاب کردن شیرین به او
---
# بخش ۳۹ - رفتن شاپور پیش شیرین و عتاب کردن شیرین به او

<div class="b" id="bn1"><div class="m1"><p>چو بشنید این سخن شاپور از شاه</p></div>
<div class="m2"><p>به پا برجست و روی آورد بر راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روان شد سوی قصر آن مه نو</p></div>
<div class="m2"><p>رسانید آنچه بد پیغام خسرو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس آنگه گفت شاهت عذرخواه است</p></div>
<div class="m2"><p>برای مقدمت چشمش به راه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شمع از آتش دل هست در سوز</p></div>
<div class="m2"><p>ندارد غیر فکر تو شب و روز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا سوگند بر جان و سر توست</p></div>
<div class="m2"><p>که او را گر تن آنجا، دل، بر توست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنون گر مصلحت بینی ز راهت</p></div>
<div class="m2"><p>برم پنهان سوی مشکوی شاهت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مشکو شه تو را جایی نشاند</p></div>
<div class="m2"><p>که نه مریم که روح الله نداند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بشنید این سخن شیرین بر آشفت</p></div>
<div class="m2"><p>به شاپور از سر خشم و غضب گفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که ای شاپور تا کی مکر سازی</p></div>
<div class="m2"><p>چو طفلانم دهی هر لحظه بازی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مفرما بیش ازین تحویل و نقلم</p></div>
<div class="m2"><p>که رسته ست این زمان دندان عقلم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بازی تو تا کی افتم از راه</p></div>
<div class="m2"><p>روم بر ریسمانت چند درچاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تو کردی بهر مردن ساز و برگم</p></div>
<div class="m2"><p>کنون خوش می دهی تعلیم مرگم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>منه بار فراوان بر تن من</p></div>
<div class="m2"><p>مکن زین بیش سعی کشتن من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خاک و خون به هم اینجای خفتن</p></div>
<div class="m2"><p>که پای خود به سلاخانه رفتن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تویی در دوستی آن دشمن من</p></div>
<div class="m2"><p>که با صد مکر و صد دستان و صد فن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا از خان و مان آواره کردی</p></div>
<div class="m2"><p>چنینم عاجز و بیچاره کردی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدان راضی نگشتی و کنونم</p></div>
<div class="m2"><p>نمایی سعیها در قصد و خونم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا بگدار با این دردمندی</p></div>
<div class="m2"><p>کمر بر خون من تا چند بندی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میفکن بیش ازینم در کم و کاست</p></div>
<div class="m2"><p>که اینها نیست در پیش خدا راست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به کشتن خواهیم داد و نگویی</p></div>
<div class="m2"><p>جواب حق در آن عالم چه گویی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من امروز ار ز دستانت بمیرم</p></div>
<div class="m2"><p>به دستان دامنت فردا بگیرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدار آخر کنون دستم ز دامن</p></div>
<div class="m2"><p>که دادی بازیم باری به کشتن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو اینها ساختی ای جادوی چین</p></div>
<div class="m2"><p>مگو دیگر سخن برخیز و منشین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برو از من سلامم بر به خسرو</p></div>
<div class="m2"><p>بگو بادت مبارک آن مه نو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سلامم چون رسانیدی به سویش</p></div>
<div class="m2"><p>رسان دیگر دعا وز من بگویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که ای عهد و وفا بر باد داده</p></div>
<div class="m2"><p>به دین عشق رسم نو نهاده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مه نو گر چه دارد در جهان قدر</p></div>
<div class="m2"><p>ولیکن کی بود همچون مه بدر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مه نو را نگویم کان نه زیباست</p></div>
<div class="m2"><p>که او را شیوه ای از ابروی ماست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو داری یار نو بشنو سخن را</p></div>
<div class="m2"><p>مبر از یاد یاران کهن را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه هر کو یار نو گیرد در آغوش</p></div>
<div class="m2"><p>کند یاران دیرین را فراموش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>درین مدت نگفتی هیچ باری</p></div>
<div class="m2"><p>که ما را نیز وقتی بود یاری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کسی هرگز به یاری، یار را سوخت؟</p></div>
<div class="m2"><p>ز تو باید طریق یاری آموخت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زیان کردی سراسر جمله سودم</p></div>
<div class="m2"><p>نکردی آتش و کشتی به دودم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو تا خرمای مریم نقش بستی</p></div>
<div class="m2"><p>مرا صد خار غم در دل شکستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز شمع مریمت تا دل فروزی ست</p></div>
<div class="m2"><p>تو خرما خور که ما را خار روزی ست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تو و شادی، من و اندیشه غم</p></div>
<div class="m2"><p>من و خار و تو و خرمای مریم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو خرمای تو دارد خار بسیار</p></div>
<div class="m2"><p>تو خرما خور که تا من می خورم خار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو از خرمای تو حظی ندارم</p></div>
<div class="m2"><p>مرا بگدار با این خار خارم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برو بگدار تا با این دل تنگ</p></div>
<div class="m2"><p>نشینم همچو مرغی بر سر سنگ</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من و مریم به یک خانه زهی زه</p></div>
<div class="m2"><p>هنوز این محنت و تنهائیم به</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به هم هر چند در یاری بکوشیم</p></div>
<div class="m2"><p>عجب گر هر دو در یک دیگ جوشیم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که خوش زد این مثل آن مرد باهوش</p></div>
<div class="m2"><p>که هرگز دیگ انبازی نزد جوش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برو پیشم منه دیگر چنین راه</p></div>
<div class="m2"><p>بلندان را نباشد فکر کوتاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>میاور دیگر این اندیشه در دل</p></div>
<div class="m2"><p>محال اندیش نبود مرد عاقل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو را تا آفتابت در وبال است</p></div>
<div class="m2"><p>خیال وصل من جستن محال است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مکن در وصل من اندیشه زنهار</p></div>
<div class="m2"><p>که مارا و تو را هر دو درین کار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نشاید بیش ازین تیمار بردن</p></div>
<div class="m2"><p>تو و خرما و، ما و خار خوردن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>چو لاله بر دل تنگم منه داغ</p></div>
<div class="m2"><p>که بلبل خوش ندارد صحبت زاغ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>مرا آن به که با مریم نخوانی</p></div>
<div class="m2"><p>که چون عیسی شدم من آسمانی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو عیسی همنشین آفتابم</p></div>
<div class="m2"><p>که دل بگرفت ازین دیر خرابم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بیا تا هر دو در سازیم با هم</p></div>
<div class="m2"><p>تو و مریم من و عیسای مریم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مخوانم پیش و مگدارم بدین روز</p></div>
<div class="m2"><p>وگر خوانی پری خواندن بیاموز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اگر خواهی که آیم پیش تو خوش</p></div>
<div class="m2"><p>چو خالم خویشتن را نه بر آتش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مکن افسون که هر افسون که خوانی</p></div>
<div class="m2"><p>شده ست از من فرامش، تا تو دانی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مرا هر غمزه سحری همنشین است</p></div>
<div class="m2"><p>که او همسایه بابل زمین است</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز خوابم وانکردی بخت فیروز</p></div>
<div class="m2"><p>شبی گر دیدمی در خواب، این روز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نزاد از دهر چون من نامرادی</p></div>
<div class="m2"><p>چه بودی مادرم هرگز نزادی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>به روزم تا به شب در آتش تب</p></div>
<div class="m2"><p>به بخت خود همی گریم همه شب</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نه شب خواب و نه روز آرام دارم</p></div>
<div class="m2"><p>شب و روزی بدین سان می گدارم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چنین تا کی ز بهر دلفروزی</p></div>
<div class="m2"><p>همه شب خون خورم بر یاد روزی</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بخواهم کز دهانش کام گیرم</p></div>
<div class="m2"><p>که می ترسم به ناکامی بمیرم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>نمی آرم در این خواهش بهانه</p></div>
<div class="m2"><p>که می گوید مرا، هر دم زمانه،</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>بسا کس کو نشد بر بخت چیره</p></div>
<div class="m2"><p>بس امیدیکه شد آن، خاک تیره</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>مرا گفتی که روزی آیمت پیش</p></div>
<div class="m2"><p>منه بر جان خود اندوه ازین بیش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بخواهم مرد اگر بختم دهد دست</p></div>
<div class="m2"><p>که هر کس کو بمرد از غصه وارست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>مرا آن زندگانی نیست در خور</p></div>
<div class="m2"><p>که باشد مرگ از آن صد بار بهتر</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به شمشیرم مکش مفکن به تیرم</p></div>
<div class="m2"><p>رها کن تا به مرگ خود بمیرم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>درین حالت که من هستم تو دانی</p></div>
<div class="m2"><p>که مرگم بهتر است از زندگانی</p></div></div>