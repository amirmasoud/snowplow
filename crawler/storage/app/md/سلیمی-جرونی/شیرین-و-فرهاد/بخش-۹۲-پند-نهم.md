---
title: >-
    بخش ۹۲ - پند نهم
---
# بخش ۹۲ - پند نهم

<div class="b" id="bn1"><div class="m1"><p>بود پند نهم اینت که تدبیر</p></div>
<div class="m2"><p>نیارد کردکس با امر تقدیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میفکن پنجه با تقدیر رفته</p></div>
<div class="m2"><p>کزان دل گرددت رنجور و تفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آن کس را که نیکی سرنوشت است</p></div>
<div class="m2"><p>مدام از نیکی خود در بهشت است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر آن را که بد دادندش از پیش</p></div>
<div class="m2"><p>به دوزخ باشد از فعل بد خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دارند از ازل هر یک قراری</p></div>
<div class="m2"><p>نباشد هیچ کس را اختیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بارد تیغ در بی اختیاری</p></div>
<div class="m2"><p>بنه گردن که تدبیری نداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که سرگردان این مهرند ذرات</p></div>
<div class="m2"><p>و زین کار است عقل عاقلان مات</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درین صحرا که ره زو نیست بیرون</p></div>
<div class="m2"><p>به هر چه آید مکن چندین جگر خون</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>متاب از امر فرمان سر، که دانا</p></div>
<div class="m2"><p>نمی یارد نهادن زو برون پا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو واقف گشتی از پایان این راه</p></div>
<div class="m2"><p>به هر چه آید بگو الحکم لله</p></div></div>