---
title: >-
    بخش ۳۴ - نصیحت کردن مهین بانو شیرین را
---
# بخش ۳۴ - نصیحت کردن مهین بانو شیرین را

<div class="b" id="bn1"><div class="m1"><p>بدین گفتن چو روزی ده، برآمد</p></div>
<div class="m2"><p>زمانه بر مهین بانو سرآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خود را ز جان بی برگ می یافت</p></div>
<div class="m2"><p>که اندر خود نشان مرگ می یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس آنگه خواند شیرین را و بنشاند</p></div>
<div class="m2"><p>به پیش او ز هر بابی سخن راند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نخستش گفت ای شیرین دلبند</p></div>
<div class="m2"><p>ز خوبی بر خداوندان خداوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اجل پیغام مرگ من در آورد</p></div>
<div class="m2"><p>زمانه عمر من بر من سرآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخواهم رفت و اندوهی ندارم</p></div>
<div class="m2"><p>که خواهی ماند از من یادگارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه غم آن کز جهان گیرد کناری</p></div>
<div class="m2"><p>کزو ماند به عالم یادگاری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخواهم رفت و دارم یک دو پیغام</p></div>
<div class="m2"><p>گزارم زانکه مرگ آمد سرانجام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مده بر باد، ایام جوانی</p></div>
<div class="m2"><p>که چون پیری رسد آنگاه دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشو مغرور بر این عمر ده روز</p></div>
<div class="m2"><p>که از وی کس نگردیده ست فیروز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منه پر بار عالم بر دل خویش</p></div>
<div class="m2"><p>که دیدم نیست جز عالم دمی بیش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو می باید شدن آخر ازین در</p></div>
<div class="m2"><p>سبکباری به هر حال است بهتر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو دل خوش کن که تا عالم نهادند</p></div>
<div class="m2"><p>برات خوشدلی، کس را ندادند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرو از ره که در عمرم فتوح است</p></div>
<div class="m2"><p>که آید سر اگر خود عمر نوح است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مشو ایمن که چرخم ملک داده ست</p></div>
<div class="m2"><p>که گر ملک سلیمانست باد است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>منه دل بر مدار ماه و خورشید</p></div>
<div class="m2"><p>که دلخون رفت ازو کاووس و جمشید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بکش دامن که دلها زین کشاکش</p></div>
<div class="m2"><p>گهی خوش باشد وگاهی ست ناخوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جهان را هیچ خویشی با خوشی نیست</p></div>
<div class="m2"><p>خوشی او به غیر ناخوشی نیست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کسی در کام او پایی نیفشرد</p></div>
<div class="m2"><p>که آخر نی به ناکامی به سر برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از آن نام جهان، دار سپنج است</p></div>
<div class="m2"><p>که حاصل زو همه اندوه و رنج است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلی را زو نشد یک کام حاصل</p></div>
<div class="m2"><p>که ننهادش دگر صد داغ بر دل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شقایق را که کردی لاله اش نام</p></div>
<div class="m2"><p>به داغ دل ز مادر زاد ایام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منه بر باغ دوران دل که بی داغ</p></div>
<div class="m2"><p>نمی روید گلی هرگز درین باغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زمان کین اسپ گردون را کشد تنگ</p></div>
<div class="m2"><p>درین ره ماند در گل چو خر لنگ</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مگردان پایها بر وی، مشو شاد</p></div>
<div class="m2"><p>به هر حالی دو دستی بایدش داد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یقینم کین سرای دهر تا بود</p></div>
<div class="m2"><p>به آسایش درو یک شخص نغنود</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منه دل بر سیاهی و سفیدی</p></div>
<div class="m2"><p>که در کاسه است آش ناامیدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرو بر لطف او از ره که قهرست</p></div>
<div class="m2"><p>مخور این آش او زیرا که زهر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مچش پالوده این صحن گردون</p></div>
<div class="m2"><p>که آمد سر به سر دو شاب او خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمان کو هر زمان نقشی نگارد</p></div>
<div class="m2"><p>به دستی تاج و دستی تیغ دارد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز رخ نگشوده ماهی را، کشد میغ</p></div>
<div class="m2"><p>به سر ننهاده تاجی را زند تیغ</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>رهی تاریک پیش است و همه چاه</p></div>
<div class="m2"><p>ز من بشنو طریق خود درین راه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برافروزان چراغ دیده خویش</p></div>
<div class="m2"><p>به پیش پای خود دار و برو پیش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در این شب کز سیاهی عین سود است</p></div>
<div class="m2"><p>ز تاریکی نه چشم از چشم پیداست،</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خوشا آنان که شب گردیدشان روز</p></div>
<div class="m2"><p>نه جان دادند با صد گریه و سوز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نشان راه پرسیدند و رفتند</p></div>
<div class="m2"><p>به سان صبح خندیدند و رفتند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در آن منزل که ما اکنون رسیدیم</p></div>
<div class="m2"><p>دم آخر ز یک تا صد که دیدیم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زدند از کار خویش و حاصل خویش</p></div>
<div class="m2"><p>ز آه سرد بادی بر دل خویش</p></div></div>