---
title: >-
    بخش ۶۰ - صفت مجلس خسرو و گفتن شاپور احوال شیرین در پرده
---
# بخش ۶۰ - صفت مجلس خسرو و گفتن شاپور احوال شیرین در پرده

<div class="b" id="bn1"><div class="m1"><p>چو بر زد نور خورشید از فلک سر</p></div>
<div class="m2"><p>سحر رو تازه کرد از چشمه خور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهان بر بست مرغ شب ز افغان</p></div>
<div class="m2"><p>زبان بگشاد از آن مرغ سحر خوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوانی چرخ پیر از سر دگر باز</p></div>
<div class="m2"><p>گرفت و کرد عیش و عشرت آغاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندیده کس جز از زلف بتان تاب</p></div>
<div class="m2"><p>شده یکباره چشم فتنه در خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک کز زنگی شب می هراسید</p></div>
<div class="m2"><p>به چشم مهر در عالم دگر دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه از خواب سحر برخاست از جای</p></div>
<div class="m2"><p>به دولت شد دگر ره مجلس آرای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ندیمان جمله چون نسرین و نرگس</p></div>
<div class="m2"><p>همه کردند ساز و برگ مجلس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که اول باربد، آمد به آواز</p></div>
<div class="m2"><p>نواها کرد از عشاق آغاز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نغمه شمع جانها را می افروخت</p></div>
<div class="m2"><p>دل عشاق را چون عود می سوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بربط ناله های زار می کرد</p></div>
<div class="m2"><p>درونها را از آن افگار می کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گهی کز سوز همچون عود گشتی</p></div>
<div class="m2"><p>خجل زو نغمه داود گشتی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نواهایی کزو تازه شدی جان</p></div>
<div class="m2"><p>ازو آموختی مرغ سحر خوان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شکفتی از هوای او دل گل</p></div>
<div class="m2"><p>و زو آموختی مرغول، بلبل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نکیسا نام هم خواننده ای بود</p></div>
<div class="m2"><p>که رنگی داشت از وی صوت داوود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گهی کو چنگ را در بر گرفتی</p></div>
<div class="m2"><p>جوانی، چرخ پیر از سر گرفتی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو کردی در نوا آهنگ نوروز</p></div>
<div class="m2"><p>فتادی عود ازو در آتش و سوز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زنی برده دمش یکبارگی هوش</p></div>
<div class="m2"><p>به مجلس کرده دف را حلقه در گوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ازو بلبل نشسته شاخ بر شاخ</p></div>
<div class="m2"><p>دل نی گشته زو سوراخ سوراخ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طپانچه صد اگر بر دف زدی بیش</p></div>
<div class="m2"><p>صد و یک بار دف رو داشتی پیش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر بربط شدی ز آواز نی کر</p></div>
<div class="m2"><p>بکندی هم به مجلس، گوشش از سر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به نی گر ناله ای فرمودی از خشم</p></div>
<div class="m2"><p>نهادی نی از آن انگشت بر چشم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز دستش چنگ اگر صد زخمه خوردی</p></div>
<div class="m2"><p>به بالا سر زپشت پا نکردی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در آن مجلس که آن هر دو هم آواز</p></div>
<div class="m2"><p>به هم گه سوز می کردند و گه ساز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آمد مست در خرگاه شاپور</p></div>
<div class="m2"><p>ز مجلس هر که بد ناساز، شد دور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن مجلس نزد دیگر کسی دم</p></div>
<div class="m2"><p>به غیر از یک دو خاص الخاص محرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو شاپور آنچنان مجلس بپرداخت</p></div>
<div class="m2"><p>ببین تا بعد از آن دیگر چه بر ساخت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به خرگاهی دگر بنشاند شیرین</p></div>
<div class="m2"><p>زبان بر مدحتش بگشاد و تحسین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به خسرو گفت کای شاه سرافراز</p></div>
<div class="m2"><p>شنیدم بارها، زان سرو طناز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که بی کاوین نگردم با ملک جفت</p></div>
<div class="m2"><p>اگر صد سال آنجا بایدم خفت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ملک فرمود با خود عهد کردم</p></div>
<div class="m2"><p>که بی کابین به گرد او نگردم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آنگه گفت، دیگر بار شاپور</p></div>
<div class="m2"><p>که ای از روی خوبت چشم بد دور</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>همی خواهم بدین شکرانه امروز</p></div>
<div class="m2"><p>که گویند این دو مطرب از سر سوز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>غزلهای روان در پرده راز</p></div>
<div class="m2"><p>همه هر یک به صوت و نقش و آواز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>یکی از قول شاه دهر، خسرو</p></div>
<div class="m2"><p>یکی دیگر ز قول آن مه نو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که تا زین بیش ننشینند غمناک</p></div>
<div class="m2"><p>کنند آن هر دو مه آیینه ها پاک</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که عالم سر به سر یک غم نیرزد</p></div>
<div class="m2"><p>همه عالم غم عالم نیرزد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو از عالم نشد کس با دل شاد</p></div>
<div class="m2"><p>مخور غم، گر همه عالم برد باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو عالم غیر باد و دمدمه نیست</p></div>
<div class="m2"><p>مشو ناخوش که عالم این همه نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بود عالم دمی و آن دمی تو</p></div>
<div class="m2"><p>بدان این را که جان عالمی تو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جز این یک دم که هستی عالمی نیست</p></div>
<div class="m2"><p>غنیمت دان که عالم جز دمی نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دو عالم گر رود یک دم مخور غم</p></div>
<div class="m2"><p>که دو عالم نباشد غیر یک دم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>جهان و کار او بی اعتباری ست</p></div>
<div class="m2"><p>بنای دهر بر نااستواری ست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو گفت اینها و مجلس را بیاراست</p></div>
<div class="m2"><p>مغنی از مخالف زد ره راست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به نغمه باربد از قول خسرو</p></div>
<div class="m2"><p>غزل گفت و نکیسا زان مه نو</p></div></div>