---
title: >-
    بخش ۲۸ - گفتار در خلوت نشستن خسرو با شیرین
---
# بخش ۲۸ - گفتار در خلوت نشستن خسرو با شیرین

<div class="b" id="bn1"><div class="m1"><p>چو خسرو روز در عشرت به شب کرد</p></div>
<div class="m2"><p>خیالش وصل از شیرین طلب کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیمان را به مجلس پیش خود خواند</p></div>
<div class="m2"><p>یکایک را به جای خویش بنشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به ساقی گفت تا جام لبالب</p></div>
<div class="m2"><p>به گردش آورد از اول شب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دوری چند بگذشت از می ناب</p></div>
<div class="m2"><p>هجوم آورد بر سر، لشکر خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریفان را ز بس ساغر که دادند</p></div>
<div class="m2"><p>همه سرها به جای پا نهادند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز مجلس خاست هر کس کو توانست</p></div>
<div class="m2"><p>بیفتاد آن که سر از پا ندانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز مستی چشم ساقی نیمه ای باز</p></div>
<div class="m2"><p>ز بیهوشی شده مطرب ز آواز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به مجلس سازها بد رفته از خویش</p></div>
<div class="m2"><p>جز از نی کو نبودش یک نفس بیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صراحی پنبه ها افکنده از گوش</p></div>
<div class="m2"><p>شده گوینده ها یکباره خاموش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حریفان مست هر سویی فتاده</p></div>
<div class="m2"><p>ندیمان دیده ها بر هم نهاده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک چون مست شد مجلس چنان دید</p></div>
<div class="m2"><p>فتاد و پای شیرین را ببوسید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پسش گفتا به چشم پر فن تو</p></div>
<div class="m2"><p>که هست این دست ما ودامن تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنین کامشب ازین دولت منم مست</p></div>
<div class="m2"><p>من این دولت نخواهم دادن از دست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همی گفت این و از آن چشم غماز</p></div>
<div class="m2"><p>ز خود می رفت و می آمد به خود باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز نوک دیده مروارید می سفت</p></div>
<div class="m2"><p>به خاک پاش می افشاند و می گفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>درین شب کز قدت بختم بلند است</p></div>
<div class="m2"><p>مکن بیگانگی کان ناپسند است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دو زلفت کز برش تابد مه بدر</p></div>
<div class="m2"><p>شب قدر است و من می دانمش قدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به گیسویت که هست آن بخت پیروز</p></div>
<div class="m2"><p>که هست این شب برم بهتر که صدر وز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>میان ما و تو، ما و تویی نیست</p></div>
<div class="m2"><p>دویی بگدار کین جای دویی نیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مکش سر بیش و با من سر در آور</p></div>
<div class="m2"><p>مراد نامرادی را برآور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مکن امشب به کارم هیچ اهمال</p></div>
<div class="m2"><p>خدا داند که فردا چون شود حال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به شادی بگدران با من جهان را</p></div>
<div class="m2"><p>غنیمت دان حضور دوستان را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یکم امشب به زلف خویش ده جا</p></div>
<div class="m2"><p>تو می دانی دگر اللیل حبلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به خویش آی و مشو بیگانه ما</p></div>
<div class="m2"><p>که غیری نیست اندر خانه ما</p></div></div>