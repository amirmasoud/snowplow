---
title: >-
    بخش ۳۷ - صفت باربد در بزم خسرو
---
# بخش ۳۷ - صفت باربد در بزم خسرو

<div class="b" id="bn1"><div class="m1"><p>درآمد باربد اول به آواز</p></div>
<div class="m2"><p>نواهای عجایب کرد آغاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به عشاق آن نوا را کرد آهنگ</p></div>
<div class="m2"><p>که زهره بر زمین زد زآسمان چنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بربط ناله ها زان گونه می خواست</p></div>
<div class="m2"><p>که می شد پرده عشاق زان راست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز مستی رفته بود از پرده بیرون</p></div>
<div class="m2"><p>همی زد دم به دم راهی به قانون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از نوروز گفتی در بهاران</p></div>
<div class="m2"><p>تو گفتی هست بلبل صد هزاران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو بنمودی نوایی از سر درد</p></div>
<div class="m2"><p>شکستی توبه صد ساله مرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گهی کز جان نوایی ساز دادی</p></div>
<div class="m2"><p>ز سوزش عود بر آتش فتادی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی کز دل سرودی بر کشیدی</p></div>
<div class="m2"><p>ز مردم آه بر گردون رسیدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی کز ارغنون آواز می داد</p></div>
<div class="m2"><p>نی از دستش همی آمد به فریاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زنی زان خاطرش ناشاد بودی</p></div>
<div class="m2"><p>که در گوشش سخنها باد بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به مجلس دم به دم بر حسب مقدور</p></div>
<div class="m2"><p>بدادی گوشمال عود و طنبور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جهان کو مست چشم ناز او بود</p></div>
<div class="m2"><p>پر از آوازه آواز او بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو آوازی به سازی کردی آغاز</p></div>
<div class="m2"><p>شدی از حیرتش بلبل ز آواز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو کردی نغمه اش با چنگ آهنگ</p></div>
<div class="m2"><p>زدی چنگ از خوشی در دامنش چنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر با نی بنالیدی زمانی</p></div>
<div class="m2"><p>بسوزانیدی از ناله جهانی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به طنبور ار دم خود باز دادی</p></div>
<div class="m2"><p>به طفلی چون مسیح آواز دادی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر با نی حدیثی بازگفتی</p></div>
<div class="m2"><p>نی اندر دم هزاران راز گفتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر با دف زدی دستی که بخروش</p></div>
<div class="m2"><p>گرفتی در زمان دف پیش او گوش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نگشتی از کمانچه یک نفس خوش</p></div>
<div class="m2"><p>نیفتادی به او تا در کشاکش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رباب این حال ازو هم چونکه دیدی</p></div>
<div class="m2"><p>به همپایی او دستی کشیدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گهی کز سوز دل بنواختی عود</p></div>
<div class="m2"><p>ز هر نغمه نمودی لحن داوود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گهی کز زخمه ای می کرد برداشت</p></div>
<div class="m2"><p>به هر یک زخمه خسرو ناله ای داشت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به هر نغمه که می کرد از سر سوز</p></div>
<div class="m2"><p>شبی می کرد با آن نغمه اش روز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گهی با عود می بودی هم آهنگ</p></div>
<div class="m2"><p>گهی دستی زدی بر دامن چنگ</p></div></div>