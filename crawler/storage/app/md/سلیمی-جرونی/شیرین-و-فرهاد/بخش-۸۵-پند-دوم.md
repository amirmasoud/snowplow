---
title: >-
    بخش ۸۵ - پند دوم
---
# بخش ۸۵ - پند دوم

<div class="b" id="bn1"><div class="m1"><p>دوم پند آنکه با نادان مشو یار</p></div>
<div class="m2"><p>که از نادان، کشی اندوه بسیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز نادان، عاقل آن به کو گریزد</p></div>
<div class="m2"><p>که از او جز سیه رویی نخیزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دانا گر رسد صد جور بهتر</p></div>
<div class="m2"><p>که نادانت دهد صد بدره زر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببر از مرد جاهل تا توانی</p></div>
<div class="m2"><p>که با او هست ضایع، زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مکن هم صحبتی با هیچ جاهل</p></div>
<div class="m2"><p>که خوش زد این مثل، آن مرد کامل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که با ناجنس، صحبت داشت یک دم</p></div>
<div class="m2"><p>اگر جنت بود، باشد جهنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر خواهی که هرگز نبودت بد</p></div>
<div class="m2"><p>نشین تا می توان، با بهتر از خود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن بشنو ز دانا، تا که بتوان</p></div>
<div class="m2"><p>مبین نادان که بادا مرگ نادان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو شیرینی شنو این پند شیرین</p></div>
<div class="m2"><p>که دانایان پیشین گفته اند این</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که با ناجنس منشین و میامیز</p></div>
<div class="m2"><p>بود تا سعیت از نادان بپرهیز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که یک نادان، برانگیزد از آن گرد</p></div>
<div class="m2"><p>که صد دانا نیارد چاره اش کرد</p></div></div>