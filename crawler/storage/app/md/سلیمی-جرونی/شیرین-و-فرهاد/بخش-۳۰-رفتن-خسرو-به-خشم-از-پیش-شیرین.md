---
title: >-
    بخش ۳۰ - رفتن خسرو به خشم از پیش شیرین
---
# بخش ۳۰ - رفتن خسرو به خشم از پیش شیرین

<div class="b" id="bn1"><div class="m1"><p>چو خسرو دیدکان سرو سمنبر</p></div>
<div class="m2"><p>دلی دارد به بر از سنگ سختر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره و رسم وفاداری نپوید</p></div>
<div class="m2"><p>همه همچون دل خود سخت گوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو زلفش یک زمان بر خود بپیچید</p></div>
<div class="m2"><p>سرشکی چند از مژگان ببارید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آنگه خاست از مجلس چو آتش</p></div>
<div class="m2"><p>به قهر و خشم گفتا وقت تان خوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روم زین آستان گیرم سر خویش</p></div>
<div class="m2"><p>که نتوان داد درد سر، ازین بیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شوم تیر ملامت را نشانه</p></div>
<div class="m2"><p>برم درد سر از این آستانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا بس از تو این خواری کشیدن</p></div>
<div class="m2"><p>جفا و جور بی اندازه دیدن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چنین کز مردمی چشمت مرا سوخت</p></div>
<div class="m2"><p>ز چشمت مردمی می باید آموخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن هندو ندارم ناسپاسی</p></div>
<div class="m2"><p>که آمد حق او مردم شناسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا زین گفته ها بیهوش کردی</p></div>
<div class="m2"><p>غریبم حلقه ها در گوش کردی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سخنهایی که لعلت گفت فاشم</p></div>
<div class="m2"><p>بس است آنها مرا تا زنده باشم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عجب گر سگ کشد این خواری از کس</p></div>
<div class="m2"><p>اگر من آدمی باشم همین بس</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سخنهایی که گوشم از تو بشنفت</p></div>
<div class="m2"><p>عجب گر آن به گور من توان گفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روا باشد گر از این غم زنم شور</p></div>
<div class="m2"><p>کنم گوری و خفتم زنده در گور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نخواهم باز شد یکبارت از سر</p></div>
<div class="m2"><p>ولیکن حالیا ناچار ازین در</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روم باز آیم ار باشد مرا برگ</p></div>
<div class="m2"><p>بخواهم عذرت ار مهلت دهد مرگ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفت اینها و شد بر پشت مرکب</p></div>
<div class="m2"><p>روان شد سوی روم اندر همان شب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو رفت آوازه خسرو بدان بوم</p></div>
<div class="m2"><p>به استقبال او آمد شه روم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بهتر مدح و تعریفی ستودش</p></div>
<div class="m2"><p>نوازش کرد و پوزشها نمودش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فشاندش گنج و گوهرهای بسیار</p></div>
<div class="m2"><p>چنان کز خسروان باشد سزاوار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به شهرش برد و بازش پیش خود خواند</p></div>
<div class="m2"><p>به پهلوی خودش بر تخت بنشاند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کمر بست و کله بر سر نهادش</p></div>
<div class="m2"><p>هر آن چیزی که می بایست دادش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز گنج و لشکرش چون کرد دل شاد</p></div>
<div class="m2"><p>پس آنگه دختر خود را بدو داد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به دیرش برد و پیمان داد محکم</p></div>
<div class="m2"><p>که تو عیسائی و هست اینت مریم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو خسرو آن نوازشها و آن گنج</p></div>
<div class="m2"><p>زقیصر دید، شد آسوده از رنج</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>برآرایید لشکر از پی جنگ</p></div>
<div class="m2"><p>سوی بهرام چوبین کرد آهنگ</p></div></div>