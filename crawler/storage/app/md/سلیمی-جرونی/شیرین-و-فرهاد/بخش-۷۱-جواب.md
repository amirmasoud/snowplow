---
title: >-
    بخش ۷۱ - جواب
---
# بخش ۷۱ - جواب

<div class="b" id="bn1"><div class="m1"><p>جوابش داد استاد سخنور</p></div>
<div class="m2"><p>که بشنو شرح این چرخ مدور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حدیث«کل یوم» گر بخوانی</p></div>
<div class="m2"><p>تمام اسرار این را بازدانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن دارد فلک، دور پیاپی</p></div>
<div class="m2"><p>که در هر دور، لاشی را کند شیء</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین معنی کند شانش ضرورت</p></div>
<div class="m2"><p>به هریک دور چندین نقش و صورت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازو آمد پدید این نقش و اشکال</p></div>
<div class="m2"><p>و زو پیداست، ماه و هفته (و) سال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین گر چه بود یک جا معین</p></div>
<div class="m2"><p>ولی بشنو بیانش روشن از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمین و آسمان از هم جدا نیست</p></div>
<div class="m2"><p>جداشان مشمر از هم، کین روا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زمین و آسمان یک شخص پیر است</p></div>
<div class="m2"><p>که آن را نام، انسان کبیر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود این دورها نشو و نمایش</p></div>
<div class="m2"><p>تعینهاست یکسر بچه هایش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین آمد دل این شخص عالم</p></div>
<div class="m2"><p>بود این شخص را هم نام آدم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر خواهی که یابی این معما</p></div>
<div class="m2"><p>ببین در نطفه تا گردد هویدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شود از نطفه اول نقطه دل</p></div>
<div class="m2"><p>و زو گردد جوارح جمله حاصل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دل عالم چو آمد گوهر خاک</p></div>
<div class="m2"><p>و زو گردید ظاهر دور افلاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد از این خاک ظاهر چرخ و انجم</p></div>
<div class="m2"><p>و زو پیدا شد این انواع مردم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس این عالم درختی بیش مشمر</p></div>
<div class="m2"><p>که تخمش شد زمین و آدمی بر</p></div></div>