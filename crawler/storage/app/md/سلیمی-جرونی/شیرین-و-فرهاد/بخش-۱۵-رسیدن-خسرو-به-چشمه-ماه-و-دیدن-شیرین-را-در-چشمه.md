---
title: >-
    بخش ۱۵ - رسیدن خسرو به چشمه ماه و دیدن شیرین را در چشمه
---
# بخش ۱۵ - رسیدن خسرو به چشمه ماه و دیدن شیرین را در چشمه

<div class="b" id="bn1"><div class="m1"><p>سخن پرداز، کین در دری سفت</p></div>
<div class="m2"><p>ز شیرین و ز خسرو این چنین گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که خسرو از پدر چون روی برتافت</p></div>
<div class="m2"><p>در آن ره صحبت شاپور دریافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوی ارمن روانه کرد او را</p></div>
<div class="m2"><p>که جست و جو کند آن ماهرو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روان شد در پی اش چون باد در حال</p></div>
<div class="m2"><p>که نتوانست در آن کار اهمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که ناگه یک سحر تنها ز لشکر</p></div>
<div class="m2"><p>به یک سو شد هوای یار در سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قضا را گشت پیدا مرغزاری</p></div>
<div class="m2"><p>زمینی در نکویی چون نگاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آنجا چشمه ای چون چشمه خور</p></div>
<div class="m2"><p>چه باشد خور کزو صد بار بهتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آن سرچشمه اسبی دید بسته</p></div>
<div class="m2"><p>میان چشمه هم سروی نشسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تنش مانند سیم و چشمه سیماب</p></div>
<div class="m2"><p>زاندامش فتاده لرزه بر آب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پری مثلش ندیده قاف تا قاف</p></div>
<div class="m2"><p>پرندی نیلگون بر بسته تا ناف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خسرو دید آن شبدیز با ماه</p></div>
<div class="m2"><p>برآمد از دل گم گشته اش آه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتا آنکه وصفش می شنیدم</p></div>
<div class="m2"><p>نمردم تا به چشم خویش دیدم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زمانی گوشه ای بگرفت و بنشست</p></div>
<div class="m2"><p>که جای ماهی اش مه بود در شست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو یک دم کرد آن مه را نظاره</p></div>
<div class="m2"><p>بدید آن ماه او را از کناره</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خجل گردید و برد از کار خود زشت</p></div>
<div class="m2"><p>ز هر سو موی بر اعضا فرو هشت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو شد موهاش بر اعضا پریشان</p></div>
<div class="m2"><p>تو گفتی ماه شد در ابر پنهان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خود گفت این کدامین مه چنین است</p></div>
<div class="m2"><p>عجب گر آنکه می جستم نه این است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>و زان سوی دگر آن سرو آزاد</p></div>
<div class="m2"><p>دلش در بر، روان در لرزه افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرا کان صورت زیبا که شاپور</p></div>
<div class="m2"><p>به من بنمود دیدم اینک از دور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دگر گفت این کی او باشد، خیال است</p></div>
<div class="m2"><p>وصال او بدین زودی محال است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز آب آمد برون و آهنگ ره ساخت</p></div>
<div class="m2"><p>چه دانست او که یارش بود و نشناخت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو شیرین این چنین زانجا برون شد</p></div>
<div class="m2"><p>شنو احوال خسرو تا که چون شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بیرون رفت شیرین از میانه</p></div>
<div class="m2"><p>دل خسرو شد از تیرش نشانه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به جست و جوی او هر گوشه گردید</p></div>
<div class="m2"><p>نه از او و نه از گردش اثر دید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس آنگه در طلب بیچاره خسرو</p></div>
<div class="m2"><p>به جست و جوی آن مه در تک و دو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز گیسویش همه شب آه می کرد</p></div>
<div class="m2"><p>حدیث روی او با ماه می کرد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نظر می کرد هر دم سوی پروین</p></div>
<div class="m2"><p>ستاره می شمرد از اشک رنگین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز مژگان، لعل و مروارید می سفت</p></div>
<div class="m2"><p>به راه یار می افشاند و می گفت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببین دولت که چون بر ما در این دشت</p></div>
<div class="m2"><p>چو برقی آمد و چون ماه بگذشت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بود عمر آدمی را چون مه بدر</p></div>
<div class="m2"><p>بر آن عمر این بدن همچون شب قدر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در این ره بس کساکو می توانست</p></div>
<div class="m2"><p>که قدرش داند و قدرش ندانست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مشو غافل که پیش چشم محرم</p></div>
<div class="m2"><p>سراسر عمر نبود غیر یک دم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی کو پایه عالم شناسد</p></div>
<div class="m2"><p>ازل را با ابد یک دم شناسد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کنون کت مرغ در دام است دریاب</p></div>
<div class="m2"><p>که شاید برپرد چون گیردت خواب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو اینها گفت با خود یک زمانی</p></div>
<div class="m2"><p>و زان دلبر ندادش کس نشانی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آن صحرا از آن گل یاد می کرد</p></div>
<div class="m2"><p>چو بلبل نعره و فریاد می کرد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گهی می گفت آه ای سرو آزاد</p></div>
<div class="m2"><p>گلی بودی و بربودت ز من باد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به دستم آمدی در غایت ناز</p></div>
<div class="m2"><p>ندانستم ز دستم چون شدی باز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سعادت آمدم نشناختم پیش</p></div>
<div class="m2"><p>گواهی می دهم بر کوری خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>حدیث من بدان ماند که ایام</p></div>
<div class="m2"><p>به سر کرد و ندید از عمر جز نام</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ازین پوشش چرا من عور گشتم</p></div>
<div class="m2"><p>به چشمم خاک شد زان کور گشتم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شوم صابر بسازم با چنین درد</p></div>
<div class="m2"><p>نکوبم بیش از این من آهن سرد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ازین آتش بسازم من به دودی</p></div>
<div class="m2"><p>پشیمانی ندارد هیچ سودی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز بس زاری از آن چشمان پر درد</p></div>
<div class="m2"><p>به گرد چشمه هر سو چشمه ای کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شد از اشکش به یاد روی شیرین</p></div>
<div class="m2"><p>همه صحرا پر از گلهای رنگین</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>هر آن منزل کز آب چشم می شست</p></div>
<div class="m2"><p>در آن ره نرگس و بادام می رست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز عکس عارضش هنگام رفتار</p></div>
<div class="m2"><p>شدی صحرا سراسر زعفران زار</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زاشک سرخ او رستی در آن باغ</p></div>
<div class="m2"><p>هزاران لاله با دلهای پر داغ</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سخن چون گفتی از آن زلف در هم</p></div>
<div class="m2"><p>بنفشه سر به پیش افکندی از غم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز راهش بس که از هر سو نظر کرد</p></div>
<div class="m2"><p>ز چشمش چشمه ها هر گوشه سر کرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>زبس کابش شدی از چشم بی خواب</p></div>
<div class="m2"><p>بدی دایم به پیشش چشمه آب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>ز راه خویش هر گردی که رفتی</p></div>
<div class="m2"><p>ز چشم آبش زدی وین بیت گفتی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>اجل، گو خاک در چشمم میفکن</p></div>
<div class="m2"><p>که آب ماست زین سرچشمه روشن</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بسی می بایدش خون جگر خورد</p></div>
<div class="m2"><p>زسختی تا به آسانی رسد مرد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو شمعش دل بسی پر سوز گردد</p></div>
<div class="m2"><p>شبی بر خسته ای تا روز گردد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کشد بسیار گرم و سرد بشنو</p></div>
<div class="m2"><p>درختی تا بر آرد میوه نو</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>به خود، بی خود، به صد زاری و صد سوز</p></div>
<div class="m2"><p>همه شب این مثل می گفت تا روز</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>فلک بسیار دوری با سر آرد</p></div>
<div class="m2"><p>زمین تا خوشه گندم بر آرد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چنین شد حال خسرو وان(آن)ماه</p></div>
<div class="m2"><p>ببین تا چون شد از تقدیر الله</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نیاسود و نیارامید یک دم</p></div>
<div class="m2"><p>سوی شهر مداین رفت خرم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چو شد سوی مداین آن صنم تیز</p></div>
<div class="m2"><p>خبر پرسید از مشکوی شبدیز</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خبر پرسان چو شد درگاه شه دید</p></div>
<div class="m2"><p>روانی شد درون از کس نترسید</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فرود آمد درون خانه شاه</p></div>
<div class="m2"><p>ازو گشته خجل هم مهر و هم ماه</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شدند از شکل او و نقش شبدیز</p></div>
<div class="m2"><p>همه حیران پری رویان پرویز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پرستارانه پیشش صف کشیدند</p></div>
<div class="m2"><p>به بانوییش بر خود برگزیدند</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بدان گل، گرچه می بودند مایل</p></div>
<div class="m2"><p>همی خوردند ازو صد خار بر دل</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>وی از احوال خسرو نیز پرسید</p></div>
<div class="m2"><p>وز ایشان چون حدیث شاه بشنید</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>پس از حالش تفحص ها نمودند</p></div>
<div class="m2"><p>به بهتر مدحتی او را ستودند</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>زبان بگشود شیرین بر دعاشان</p></div>
<div class="m2"><p>بر آن افزود هم بی حد، ثناشان</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>که ای خوبان حدیث من دراز است</p></div>
<div class="m2"><p>مع القصه به پرویزم نیاز است</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز حال خویشتن حالی منم لال</p></div>
<div class="m2"><p>چو آید او شود معلوم تان حال</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>توقع دارم از خوبان پرویز</p></div>
<div class="m2"><p>که باشند آگه از تیمار شبدیز</p></div></div>