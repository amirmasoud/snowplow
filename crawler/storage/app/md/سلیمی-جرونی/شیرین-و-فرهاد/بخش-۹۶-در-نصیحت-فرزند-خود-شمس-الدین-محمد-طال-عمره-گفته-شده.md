---
title: >-
    بخش ۹۶ - در نصیحت فرزند خود شمس الدین محمد طال عمره گفته شده
---
# بخش ۹۶ - در نصیحت فرزند خود شمس الدین محمد طال عمره گفته شده

<div class="b" id="bn1"><div class="m1"><p>ببین ای از ازل گشته موید</p></div>
<div class="m2"><p>دو چشمم خواجه شمس الدین محمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دانش در جهان بگزیده من</p></div>
<div class="m2"><p>به بینش نور هر دو دیده من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بخت خود، دل خرسند دارم</p></div>
<div class="m2"><p>که در عالم چو تو فرزند دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایا تا بود دور و زمانت</p></div>
<div class="m2"><p>خدا دارد به نیکی در امانت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم از دم روز و شب به خواهدت بود</p></div>
<div class="m2"><p>که تا هستی تو، هستم از تو خشنود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین سی سال تا هستی به مردی</p></div>
<div class="m2"><p>ز خدمت هیچ تقصیری نکردی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو تا غایت نکردی هیچ تقصیر</p></div>
<div class="m2"><p>نخواهی کرد تا خواهی شدن پیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبادت چشم زخم از چرخ ریمن</p></div>
<div class="m2"><p>که هست امید من اینکه پس از من،</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نیکویی بمانی جاودانه</p></div>
<div class="m2"><p>شوی در شعر، مشهور زمانه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که در دوران، به از اقران خویشی</p></div>
<div class="m2"><p>چه اقران، بلکه از صد ساله بیشی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نشاط انگیز و غم کاه و دل افروز</p></div>
<div class="m2"><p>درین دوران بحمدالله که امروز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارد کس چو من فرزانه فرزند</p></div>
<div class="m2"><p>گران قدر و سبک روح و خردمند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یقینم ای مرا چون عمر در خور</p></div>
<div class="m2"><p>که از پندم نخواهی تافتن سر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نخست آنکه از خودی خود جدا باش</p></div>
<div class="m2"><p>به هر حالی که باشی با خدا باش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چنان از ما قبولی کن تحاشی</p></div>
<div class="m2"><p>که مقبول خدا و خلق باشی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مکن آزار موری تا توانی</p></div>
<div class="m2"><p>که این آمد مراد زندگانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو جباران به نخوت سر میفراز</p></div>
<div class="m2"><p>تواضع را شعار خویشتن ساز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو آتش سرمکش، چون آب شو پاک</p></div>
<div class="m2"><p>که آخر خاک می باید شدن خاک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رها کن هر چه نابایست باشد</p></div>
<div class="m2"><p>مکن چیزی که ناشایست باشد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوانی را مکن ضایع به غفلت</p></div>
<div class="m2"><p>که ما کردیم و پر دیدیم از آن لت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر خواهی که ننشینی جگر خون</p></div>
<div class="m2"><p>منه پا از طریق شرع بیرون</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تحمل کن به هر چیز و مشو گیج</p></div>
<div class="m2"><p>که نبود بی تحمل مرد را هیچ</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تحمل کن ز اعلا و ز ادنا</p></div>
<div class="m2"><p>که خوش گفت این سخن آن مرد دانا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به چشم کم مبین در هیچ ذره</p></div>
<div class="m2"><p>که او را هم ز خورشید است بهره</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مکش پا واپس، افتادی چو در پیش</p></div>
<div class="m2"><p>مکش پا از گلیم خویش هم بیش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود تفریط در هر حالتی شوم</p></div>
<div class="m2"><p>مکن افراط کان هم هست مذموم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو اول خویش را از غم بری کن</p></div>
<div class="m2"><p>پس آنگه دیگری را غمخوری(کن)</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن انفاق چون خود بی نوایی</p></div>
<div class="m2"><p>که گر خود را نه ای کس را نشایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چراغی کان ز بهر خانه باید</p></div>
<div class="m2"><p>مثل باشد که مسجد را نشاید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مده از دست حزم و پیش بینی</p></div>
<div class="m2"><p>وگر نه پر به روز غم نشینی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مکن در رنج بردن هیچ تقصیر</p></div>
<div class="m2"><p>که هستم این نصیحت یاد از پیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو از رنج تو گنج آید پدیدار</p></div>
<div class="m2"><p>ببر رنج ای جوان و گنج بردار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تو شو مشغول کار و باش حاضر</p></div>
<div class="m2"><p>که هر کاریش مزدی هست آخر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>متاب از رنج در کار و مجو هیچ</p></div>
<div class="m2"><p>که ضایع نیست در درگاه او هیچ</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چو نوشی، لقمه گرد سر مگردان</p></div>
<div class="m2"><p>که راهی نیست از لب تا به دندان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>اگر خواهی که در محنت نمیری</p></div>
<div class="m2"><p>بکن روز جوانی فکر پیری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو مشکلهای دوران غم فزاید</p></div>
<div class="m2"><p>چنان زی کانت آسانتر برآید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به گیتی گر حضور ار عمر خواهی</p></div>
<div class="m2"><p>مگرد از هیچ رو گرد مناهی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برای روز بد چیزی بنه زر</p></div>
<div class="m2"><p>که باشد احتیاج از مرگ بدتر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو حیوان درمیفت اندر علفزار</p></div>
<div class="m2"><p>که دایم خوار باشد مرد پرخوار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ازین بهتر نباشد سرفرازی</p></div>
<div class="m2"><p>که نفس خود زبون خویش سازی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مگو تا جهد داری با زنان راز</p></div>
<div class="m2"><p>که زن رازت بگوید سر به سرباز</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به احمق در سخن گفتن نکوشی</p></div>
<div class="m2"><p>جواب احمقان باشد خموشی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر خواهی که باشی با سعادت</p></div>
<div class="m2"><p>سعادت نیست غیر از ترک عادت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مکن هر چیز کان خیزد ز دستت</p></div>
<div class="m2"><p>که بیراهی کند چون خاک، پستت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>مکن گر داری ای جان پدر هوش</p></div>
<div class="m2"><p>خدا را در همه کاری فراموش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز صحبتهای بد بگریز چون تیر</p></div>
<div class="m2"><p>کزان بدتر نباشد هیچ تقصیر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>دمی غم جمله عالم نیرزد</p></div>
<div class="m2"><p>همه عالم به یک دم غم نیرزد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چرا باید نهادن بر جهان دل</p></div>
<div class="m2"><p>چو آخر بایدت کندن از آن دل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>مکن بد تا توانی و مخور غم</p></div>
<div class="m2"><p>فراغت جوی از کل دو عالم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مکن خود را به زندان جهان بند</p></div>
<div class="m2"><p>جهان بگذار و بر ریش جهان خند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مکش سختی و می کن درجهان زیست</p></div>
<div class="m2"><p>به هر نوعی که آسان بر توان زیست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرو تا می توانی از پی دل</p></div>
<div class="m2"><p>که جز ناکامی از وی نیست حاصل</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مکن ویران سرای دهر آباد</p></div>
<div class="m2"><p>که آن را جز خرابی نیست بنیاد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چو گنج و ملک را رو در تباهی ست</p></div>
<div class="m2"><p>گدا بودن درین ره پادشاهی ست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نبینی یک سر مو آن زمان بد</p></div>
<div class="m2"><p>که بینی هر بدی را بهتر از خود</p></div></div>