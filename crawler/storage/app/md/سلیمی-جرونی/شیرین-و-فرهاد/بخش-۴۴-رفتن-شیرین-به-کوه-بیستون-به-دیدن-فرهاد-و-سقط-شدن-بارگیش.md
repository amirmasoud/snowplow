---
title: >-
    بخش ۴۴ - رفتن شیرین به کوه بیستون به دیدن فرهاد و سقط شدن بارگیش
---
# بخش ۴۴ - رفتن شیرین به کوه بیستون به دیدن فرهاد و سقط شدن بارگیش

<div class="b" id="bn1"><div class="m1"><p>چنین دارم خبر از باستان گوی</p></div>
<div class="m2"><p>چو می آورد در این داستان روی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که یک روزی نشسته بود شیرین</p></div>
<div class="m2"><p>به گردش دختران چون ماه و پروین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حدیث از هر دری آغاز کرده</p></div>
<div class="m2"><p>در از هر سرگذشتی باز کرده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی افسانه پیشینه می گفت</p></div>
<div class="m2"><p>دگر درد دل دیرینه می گفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکی می گفت اگر دولت بود یار</p></div>
<div class="m2"><p>بخواهد بود ما را عیش بسیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که شیرین گفت از دی و ز فردا</p></div>
<div class="m2"><p>نمی گویم که آنها نیست پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث دی و فردا محض سوداست</p></div>
<div class="m2"><p>که ماضی رفت و مستقبل نه پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا نابود را باشیم دنبال</p></div>
<div class="m2"><p>همه یکباره برخیزید تا حال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زین آریم مرکبهای چون باد</p></div>
<div class="m2"><p>رویم و بیستون بینیم و فرهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو مهرویان شیرین این شنیدند</p></div>
<div class="m2"><p>زشادی هر یکی بیرون دویدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهادند اسب خود را هر یکی زین</p></div>
<div class="m2"><p>نبود آن جایگه گلگون شیرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زبهرش مرکبی دیگر کشیدند</p></div>
<div class="m2"><p>دو سه جام لبالب در کشیدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پس آنگه شاد و خوشدل با می و چنگ</p></div>
<div class="m2"><p>به سوی بیستون کردند آهنگ</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به سان برگ گل کان را برد باد</p></div>
<div class="m2"><p>همه رفتند تا نزدیک فرهاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو دید آن حال فرهاد سبک دست</p></div>
<div class="m2"><p>ز شادی در زمان بر پای برجست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوان آمد به استقبال آن ماه</p></div>
<div class="m2"><p>به دست و پای اسب افتادش از راه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو شیرین شکر لب آنچنان دید</p></div>
<div class="m2"><p>به شیرین کاری او را باز پرسید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیایش کرد و از وی عذرها خواست</p></div>
<div class="m2"><p>که ای فرهاد منتهات بر ماست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به غیر از تو ندارم منت از کس</p></div>
<div class="m2"><p>به عذرت ایستادستم ازین پس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو عذرش خواست شیرین نکونام</p></div>
<div class="m2"><p>ز شیر چون شکر دادش دو سه جام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نبشته بود بر آن جام چون زر</p></div>
<div class="m2"><p>خطی خوشبوی تر از مشک و عنبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که بوی شیر آید از دهانم</p></div>
<div class="m2"><p>بنوش این شیر بر یاد لبانم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو فرهاد آن ز دست یار نوشید</p></div>
<div class="m2"><p>چو شیرمست از مستی خروشید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو مستان، مست گشت و بی خبر شد</p></div>
<div class="m2"><p>ز عشقش مست بود او مست تر شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزد آهی و رو مالید بر خاک</p></div>
<div class="m2"><p>چنان کافتاد ازو آتش در افلاک</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پس آنگه روی را از خاک برداشت</p></div>
<div class="m2"><p>فغان از جان آتشناک برداشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به کوه بیستون بگشاد بازو</p></div>
<div class="m2"><p>فرود آورد سنگ بی ترازو</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چنان در کار خود بودش شکوهی</p></div>
<div class="m2"><p>که می کندی به هر یک حمله کوهی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو شیرین دید آن بازو و آن دست</p></div>
<div class="m2"><p>ورا درکوه کندن آنچنان مست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در او هم حیرتی آمد به دیدار</p></div>
<div class="m2"><p>که شد بیهوش در بالای رهوار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>وز آن بیهوشی از کف شد عنانش</p></div>
<div class="m2"><p>سقط شد بارگی در زیر رانش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو دید آن حال، فرهاد تنومند</p></div>
<div class="m2"><p>سر خود را به پای اسپش افکند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس آنگه در زمان برخاست بر پای</p></div>
<div class="m2"><p>ورا با بارگی برداشت از جای</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>فرود آمد ز کوه و مست و بی خویش</p></div>
<div class="m2"><p>ره قصرش گرفت آنگاه در پیش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان شد تیز در آن راه فرهاد</p></div>
<div class="m2"><p>که از وی ماند در همراهیش باد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>پری رویان ز پی هر یک سواره</p></div>
<div class="m2"><p>همی راندند حیران در نظاره</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>که برد او را چنان فرهاد هموار</p></div>
<div class="m2"><p>که یک مو بر تنش نگرفت آزار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرود آورد سوی قصر خویشش</p></div>
<div class="m2"><p>سری بنهاد و باز آمد زپیشش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چو باز آمد به از اول به صد بار</p></div>
<div class="m2"><p>شد و در بیستون استاد در کار</p></div></div>