---
title: >-
    بخش ۱۳ - نمودن شاپور خود را به شیرین و روانه ساختن او را به طرف مداین
---
# بخش ۱۳ - نمودن شاپور خود را به شیرین و روانه ساختن او را به طرف مداین

<div class="b" id="bn1"><div class="m1"><p>چو شاپور این چنین نیرنگ بر ساخت</p></div>
<div class="m2"><p>به غیبت آنچنان شطرنج ها باخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی طومار نقاشانه بگشود</p></div>
<div class="m2"><p>که هر نقشی که می خواهی در او بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفت او یک سر راهی و بنشست</p></div>
<div class="m2"><p>گرفته گوشه طومار در دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه طوماری، جهانی پر عجایب</p></div>
<div class="m2"><p>عجایب چه غرایب در غرایب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن طومار بد نقش همه چیز</p></div>
<div class="m2"><p>کشیده در میانه نقش پرویز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو آن خوبان بدان منزل رسیدند</p></div>
<div class="m2"><p>عنان اسبها را واکشیدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو شیرین نقش او وان نقشها دید</p></div>
<div class="m2"><p>چو بید از یاد سرو خویش لرزید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگفتش کیستی و از کجایی</p></div>
<div class="m2"><p>که یابم از تو بوی آشنایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چه نامی، وز کدامین سرزمینی</p></div>
<div class="m2"><p>همانا مانیی از شهر چینی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جوابش داد شاپور سخن دان</p></div>
<div class="m2"><p>که راز خود ندارم از تو پنهان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من آن استادم اندر نقش سازی</p></div>
<div class="m2"><p>که مانی را دهم در نقش، بازی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مکن در صورت از مانی قیاسم</p></div>
<div class="m2"><p>که من صورتگری معنی شناسم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تو هر صورت که بنمایی تمامش</p></div>
<div class="m2"><p>بگویم کیست او و چیست نامش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شیرین دید از دلبر نشانی</p></div>
<div class="m2"><p>فرود آمد در آن منزل زمانی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفت آن گوشه طومار در دست</p></div>
<div class="m2"><p>بر شاپور شیرین کار بنشست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یکایک نقش از طومار می جست</p></div>
<div class="m2"><p>به آب دیده آن طومار می شست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو یک یک صورت از روی خرد دید</p></div>
<div class="m2"><p>نظر بگشاد ناگه نقش خود دید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان دیوانه گردید آن پری زاد</p></div>
<div class="m2"><p>که شد چون دود و آتش در وی افتاد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتش بازگو کاین صورت کیست</p></div>
<div class="m2"><p>کجا دارد مقام و نام او چیست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جوابش داد شاپور جهان بین</p></div>
<div class="m2"><p>که ای تنگ شکر یعنی که شیرین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه می پرسی، ازین صورت چه گویم</p></div>
<div class="m2"><p>که من هم همچو تو حیران اویم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو این کس را که پرسی هست شاهی</p></div>
<div class="m2"><p>بود از تخمه جمشید ماهی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رخش ماهیست اما در تمامی</p></div>
<div class="m2"><p>نویسد یوسفش خط غلامی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خطش مشکی بود لالاش عنبر</p></div>
<div class="m2"><p>قدش سرویست خورشیدیش بر سر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سپاه کاکلش صد دل شکسته</p></div>
<div class="m2"><p>خم زلفش هزار اشکسته بسته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به طاق ابرویش چشمان غماز</p></div>
<div class="m2"><p>دو تا ترکند هر یک ناوک انداز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کمان ابرویش هنگام دیدن</p></div>
<div class="m2"><p>ز من مانی نمی یارد کشیدن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به پاکی، روش، روی آب شسته</p></div>
<div class="m2"><p>هنوزش گرد گل عنبر نرسته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دو زلف سرکشش بی راه گشته</p></div>
<div class="m2"><p>بدان رخها کمند ماه گشته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>من آن نقشی که خورشیدش غلام است</p></div>
<div class="m2"><p>به یک ماه ارکشم کاری تمام است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر نی جرعه ای زان لعل خوردی</p></div>
<div class="m2"><p>مسیحا مرده را چون زنده کردی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گه هیجا چو پا در مرکب آرد</p></div>
<div class="m2"><p>به روی روز کردار شب آرد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو کاووس است گاه تاجداری</p></div>
<div class="m2"><p>و زو رستم بیاموزد سواری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نه بارویش برآید ماه و خورشید</p></div>
<div class="m2"><p>نه کیخسرو بود چون او نه جمشید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز مژگان خنجرش در ترکتازی</p></div>
<div class="m2"><p>کند در روی خور، شمشیربازی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گه حمله چو دست آرد به خنجر</p></div>
<div class="m2"><p>تن او و جهانی پر ز لشکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدنگش هست صد فرسنگ دلدوز</p></div>
<div class="m2"><p>سمندش باد را باشد تک آموز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو ناگه خاطرش صیدی پذیرد</p></div>
<div class="m2"><p>کمندش آهوی خورشید گیرد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به چوگان بازی آید چون به میدان</p></div>
<div class="m2"><p>برد از چرخ، گوی مه به چوگان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به نیزه چون نهد بر اسب زین را</p></div>
<div class="m2"><p>رباید حلقه انگشترین را</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو دست اندر کمان و تیر یازد</p></div>
<div class="m2"><p>به نوک تیر مویی را دو سازد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به زیر رانش یک اسب است گلرنگ</p></div>
<div class="m2"><p>که هریک گام او باشد دو فرسنگ</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر گویم که ابر است آن مدان سهل</p></div>
<div class="m2"><p>که برقش می جهد از آتش نعل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زره چون افکند از زلف بردوش</p></div>
<div class="m2"><p>کند مه را به خوبی حلقه در گوش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به فر و حشمت و جاه و جوانی</p></div>
<div class="m2"><p>سلیمانی ست اندر کامرانی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به وصفش هر در، ای دلبر که سفتم</p></div>
<div class="m2"><p>هنوز از صد هزاران یک نگفتم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بود این وصف و خسرو هست نامش</p></div>
<div class="m2"><p>چه خسرو بلکه کیخسرو، غلامش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>مقام خسروان ماوای دارد</p></div>
<div class="m2"><p>همی تخت مداین جای دارد</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به مهر توست همچون صبح صادق</p></div>
<div class="m2"><p>شده بر چهره ات نادیده عاشق</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به داغت روز و شب می سوزد از غم</p></div>
<div class="m2"><p>ندارد روز و شب آرام یک دم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به غم خیزد همه باغم نشیند</p></div>
<div class="m2"><p>چه خواهد کرد اگر رویت نبیند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تو را نادیده اینها شد خیالش</p></div>
<div class="m2"><p>چو بیند چون بود خودگوی حالش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شنیده باشی ای نور دو دیده</p></div>
<div class="m2"><p>شنیده کی بود هرگز چو دیده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو اینها خواند شاپور جهانسوز</p></div>
<div class="m2"><p>تو گفتی شد شب هجران او روز</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>ز رویی شاد شد و ز روی دیگر</p></div>
<div class="m2"><p>چنان شد کز غمش شد دودش از سر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>که دیرست این مثل کاندر میانست</p></div>
<div class="m2"><p>که هر چه آن سود دل، شش رازیانست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>پس آنگه گشت نه مرده نه زنده</p></div>
<div class="m2"><p>به خود پیچان چو مار تیر خورده</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چنان تیر غمش در سینه بنشست</p></div>
<div class="m2"><p>که از پای اوفتاد و رفت از دست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو شاپور پری خوان آنچنان دید</p></div>
<div class="m2"><p>که یکبار آن پری دیوانه گردید</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بدو گفتا مشو یکباره از دست</p></div>
<div class="m2"><p>که این کار تو را اندیشه (ای) هست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>بباید رفت و کردن اولت زود</p></div>
<div class="m2"><p>مهین بانوی را از خویش خشنود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به شرط آنکه داری راز پنهان</p></div>
<div class="m2"><p>نباشی هیچ ازین حالت پریشان</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>و زو خواهی اجازه سوی نخجیر</p></div>
<div class="m2"><p>که کارت را بجز این نیست تدبیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو کردی این چنین زو خواه شبدیز</p></div>
<div class="m2"><p>بر او بنشین برو تا پیش پرویز</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>سپه بگدار با گنج و خزاین</p></div>
<div class="m2"><p>برو تنها تنه، سوی مداین</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تو گفتی روز را یکباره جان رفت</p></div>
<div class="m2"><p>از آن گفت و گزارشها که شان رفت</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>خرد دیوانه ماند و در عجب شد</p></div>
<div class="m2"><p>در آن گفت و شنید آن روز شب شد</p></div></div>