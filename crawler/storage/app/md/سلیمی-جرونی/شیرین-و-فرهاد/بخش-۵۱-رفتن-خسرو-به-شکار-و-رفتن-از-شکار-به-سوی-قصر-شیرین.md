---
title: >-
    بخش ۵۱ - رفتن خسرو به شکار و رفتن از شکار به سوی قصر شیرین
---
# بخش ۵۱ - رفتن خسرو به شکار و رفتن از شکار به سوی قصر شیرین

<div class="b" id="bn1"><div class="m1"><p>سحر کز کوه سر زد خسرو شرق</p></div>
<div class="m2"><p>ز تیغ کوه عالم شد پر از برق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شه عالم ستان، یعنی که پرویز</p></div>
<div class="m2"><p>سوی صیدش سمند عزم شد تیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون آمد به صحرا با سواران</p></div>
<div class="m2"><p>به دولت همرکابش تاجداران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آن روزش نبد جز بی غمی هیچ</p></div>
<div class="m2"><p>نبود از بیشی آن مه را کمی هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته شرق تا غرب آفتابش</p></div>
<div class="m2"><p>ز خاقان تا به قیصر در رکابش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر چترش به گردون سرکشیده</p></div>
<div class="m2"><p>جهان در سایه او آرمیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سواران همرهش از انجم افزون</p></div>
<div class="m2"><p>پیاده هم ز حد و حصر بیرون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فلک مست از رخ گیتی فروزش</p></div>
<div class="m2"><p>ملک حیران ز چرخ و باز و یوزش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آن صحرا از انبوه سپاهی</p></div>
<div class="m2"><p>زمین در زلزله تا گاو و ماهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز نعل و میخ اسبانشان هماره</p></div>
<div class="m2"><p>زمین گردیده پر ماه و ستاره</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سوارانش سراسر بر ستوران</p></div>
<div class="m2"><p>نهاده سر همه دنبال گوران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در آن نخجیرگه از زخم شمشیر</p></div>
<div class="m2"><p>رها کرده به هر جا پنجه ها شیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به قتل مرغ، شاهین گشته غازی</p></div>
<div class="m2"><p>سگ صیاد در روباه بازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زدی هم باز گاهی بال و گه پر</p></div>
<div class="m2"><p>که تشنه بود بر خون کبوتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دماغ گاو کوهی شد فراموش</p></div>
<div class="m2"><p>که عمری رفته بد در خواب خرگوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پلنگ و ببر هم، زان و هم گستاخ</p></div>
<div class="m2"><p>نمی کردند بیرون سر ز سوراخ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زچندان صید کانجا گشته کشته</p></div>
<div class="m2"><p>همی بردند خلقان کشته پشته</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز چندان طمعه کاندر دست کس بود</p></div>
<div class="m2"><p>جهان را تا سر صد سال بس بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سزد چرخ ار ز صید خسروانی</p></div>
<div class="m2"><p>کند تا دور دارد میزبانی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در آن نخجیر دلکش از که و مه</p></div>
<div class="m2"><p>که بودند از نکویی یک ز یک به</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به صیدی گرچه هر یک را نظر بود</p></div>
<div class="m2"><p>ملک را چشم بر صیدی دگر بود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>زصید ار کرده بود او شیر را قید</p></div>
<div class="m2"><p>شده بود آهویی را در جهان صید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بنه گردن که آخر نیست تدبیر</p></div>
<div class="m2"><p>چو گردیدی اسیر صید تقدیر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مکش سر وز سر تقدیر مگدر</p></div>
<div class="m2"><p>که صید تیر تقدیریم یکسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فلک را گردش پرگار این است</p></div>
<div class="m2"><p>زمان را روز و شب خود، کار این است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که این یک را به زنجیر قضا قید</p></div>
<div class="m2"><p>کند وان را به تدبیر قدر صید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برو خوش باش و با تقدیر می ساز</p></div>
<div class="m2"><p>مده گر سر برندت هیچ آواز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>که در نخجیرگه خوش گفت پیری</p></div>
<div class="m2"><p>که صیدی نیست بی آسیب تیری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس آنگه با سواران هم آهنگ</p></div>
<div class="m2"><p>به سان مرد جنگی در صف جنگ</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شکار انداز دشت و کوه، پرویز</p></div>
<div class="m2"><p>همی شد مست، بر بالای شبدیز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>که ناگاهان در آن نخجیر کردن</p></div>
<div class="m2"><p>قضا بردش کشان بربسته گردن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهانه کرده صید و زان بهانه</p></div>
<div class="m2"><p>به سوی قصر شیرین شد روانه</p></div></div>