---
title: >-
    بخش ۲۱ - رسیدن خبر فوت هرمز به خسرو و رفتن به مداین
---
# بخش ۲۱ - رسیدن خبر فوت هرمز به خسرو و رفتن به مداین

<div class="b" id="bn1"><div class="m1"><p>نشسته بود روزی شاه خوشدل</p></div>
<div class="m2"><p>طمع بسته که گردد کام حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ناگه پیکی آمد تیز چون باد</p></div>
<div class="m2"><p>به پیش خسرو اندر خاک افتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان بارید خون از دیده ها تیز</p></div>
<div class="m2"><p>کزآن، آب آمد اندر چشم پرویز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس آنگه گفت از تخت کیانی</p></div>
<div class="m2"><p>به خسرو داد سلطان زندگانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو تا جستی ز پیش او جدایی</p></div>
<div class="m2"><p>برفت از دیده او روشنایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو را تا کرد بخت از وی بریده</p></div>
<div class="m2"><p>دگر بر روی کس نگشاده دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسی از بخت خود برد این ندامت</p></div>
<div class="m2"><p>که دیدار اوفتادش با قیامت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز دوری کز تو دید آن شاه کشور</p></div>
<div class="m2"><p>دو چشمش رفت و هم جان داد بر سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنون آن تخت و آن دولت تو را باد</p></div>
<div class="m2"><p>بود تا دهر در عمرت بقاباد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو خسرو دید کین چرخ ستمکار</p></div>
<div class="m2"><p>کشید این طرح نو از نوک پرگار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشوش بود چون گیسوی جانان</p></div>
<div class="m2"><p>مشوش تر شد از این داغ هجران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شدش روشن که هرگز این مه و مهر</p></div>
<div class="m2"><p>به کس ننموده است از مردمی چهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کسی عیشی نکرد از ساغر دهر</p></div>
<div class="m2"><p>که دیگر ره ندادش کاسه زهر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز می کس جام در چنگش نیامد</p></div>
<div class="m2"><p>که آخر پای بر سنگش نیامد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به لوح روز و شب چرخ این طرازد</p></div>
<div class="m2"><p>که این یک را کشد آن را نوازد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مگرد از گردش ایام، غافل</p></div>
<div class="m2"><p>چه داند کس که او را چیست در دل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مباش ایمن که اینک کار شد راست</p></div>
<div class="m2"><p>که هرکش کار شد زو راست برخاست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مگو دارم ازین مکاره سودی</p></div>
<div class="m2"><p>که عارف کی نهد او را وجودی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو نیک و بد ندارد هیچ بنیاد</p></div>
<div class="m2"><p>خوشا آن کس که دل بر هیچ ننهاد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فلک تا نفکند از تن سری را</p></div>
<div class="m2"><p>به شاهی بر ندارد دیگری را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زمان کین عرصه نقش از پیش بیند</p></div>
<div class="m2"><p>نبازد تا بساطی بر نچیند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مشو ایمن ز بازی زمانه</p></div>
<div class="m2"><p>که در وی کس نماند جاودانه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>منه خاطر بر این ایوان که جاوید</p></div>
<div class="m2"><p>نه کیخسرو درو ماند نه جمشید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مگرد از دولت ده روزه خرسند</p></div>
<div class="m2"><p>کزو دل خوش نگرداند خردمند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>الا ای آن که در عیش و سروری</p></div>
<div class="m2"><p>و زان چون غافلان اندر غروری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرو از بازی ایام از راه</p></div>
<div class="m2"><p>مشو مغرور بر این عمر کوتاه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مخر زو عشوه زین بیش و مشو خر</p></div>
<div class="m2"><p>برو با عقل، با این سگ به سر بر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>میفکن خویش با ایام در پیچ</p></div>
<div class="m2"><p>که کار و بار او هیچ است بر هیچ</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که خوش زد این مثل آن مرد عاقل</p></div>
<div class="m2"><p>که برناچیز ننهد هیچ کس دل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دل عارف ز شادی جمله غم دید</p></div>
<div class="m2"><p>وجود عارضی عین عدم دید</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو هستت عمر و دولت همنشین است</p></div>
<div class="m2"><p>غنیمت دان که مرگ اندر کمین است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو هر کامد به دنیا رخت بربست</p></div>
<div class="m2"><p>غنیمت دان دمی تا فرصتی هست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سلیمی چون مسیحا رخت ازین دیر</p></div>
<div class="m2"><p>برون آر و سوی افلاک کن سیر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو او منشین دمی از سیر افلاک</p></div>
<div class="m2"><p>چه می گردی به گرد عرصه خاک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سر خود گوی ساز و قد چو چوگان</p></div>
<div class="m2"><p>چو مردان شو ببرگویی ز میدان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو آمد کار دوران بی وفایی</p></div>
<div class="m2"><p>همان بهتر کزو جویی جدایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برون کش رخت، از این دار دنیا</p></div>
<div class="m2"><p>فلک رفتن بیاموز از مسیحا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>که زین دیر فنا هشیار و گرمست</p></div>
<div class="m2"><p>نشاید رفتنت تا سوزنی هست</p></div></div>