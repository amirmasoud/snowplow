---
title: >-
    بخش ۱۸ - صفت مجلس خسرو و آمدن شاپور
---
# بخش ۱۸ - صفت مجلس خسرو و آمدن شاپور

<div class="b" id="bn1"><div class="m1"><p>چو شد احوال شیرین جمله مفهوم</p></div>
<div class="m2"><p>ز خسرو کن حکایت نیز معلوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین دارم ز استاد سخن یاد</p></div>
<div class="m2"><p>که خسرو چون به ارمن رخت بنهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عشرت روز و شب با جام بودی</p></div>
<div class="m2"><p>دلی از عشق بی آرام بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه روز و شب که روز از محنت و تب</p></div>
<div class="m2"><p>رسانیدی به صد اندوه با شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شب کز شب درون پر سوز کردی</p></div>
<div class="m2"><p>به یارب یارب آن شب روز کردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو رامشگر سرودی بر کشیدی</p></div>
<div class="m2"><p>دلش در بر چو مرغی برپریدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برون رفتی دلش صد بار بررو</p></div>
<div class="m2"><p>ولی ننمودی از خود یک سر مو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو گفتن با کس آن حالش نبد سود</p></div>
<div class="m2"><p>دلی پر خون لبی پرخنده می بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مطرب روی خود را تازه می داشت</p></div>
<div class="m2"><p>زهر سوگوش بر آوازه می داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی چون شمع در مجلس می افروخت</p></div>
<div class="m2"><p>زمانی همچو عود از ناله می سوخت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن مجلس که جنت داشت زو رنگ</p></div>
<div class="m2"><p>همی کرد این سخن را فهم از چنگ</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که فرصت دان زمان و نوش کن جام</p></div>
<div class="m2"><p>چه داند کس که چون باشد سرانجام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنی هم گوش می کرد این به آواز</p></div>
<div class="m2"><p>که بر عالم چو من کن دیده ها باز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منه بر هست و بود دهر بنیاد</p></div>
<div class="m2"><p>که دنیا سر به سر باد است بر باد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو خسرو گشت زان گفتارها مست</p></div>
<div class="m2"><p>به می خوردن به مجلس شاد بنشست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به ساقی گفت در ده جام رنگین</p></div>
<div class="m2"><p>که بر عمر اعتمادی نیست چندین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مکن در عیشم امشب هیچ اهمال</p></div>
<div class="m2"><p>که می داند که فردا چون شود حال؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>درین دم یک نفس بی می مدارم</p></div>
<div class="m2"><p>که من این دم غنیمت می شمارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لب ساغر به دور ما بخندان</p></div>
<div class="m2"><p>که دوران را بقایی نیست چندان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جهان را نیست هرگز استواری</p></div>
<div class="m2"><p>زمان را نیست جز بی اعتباری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نمی بینم بجز بادی سرانجام</p></div>
<div class="m2"><p>حیاتی را که عمرش کرده ای نام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جهان باد است و آتش این دل ریش</p></div>
<div class="m2"><p>وزم زو باد تا کی بر دل خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هنوز از بیخودی تا نیستم مست</p></div>
<div class="m2"><p>همان بهتر که ندهم فرصت از دست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که خوش گفت این سخن آن مرد دانا</p></div>
<div class="m2"><p>که می دانست حکمت بهتر از ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>که در این دم که در اویی منه دل</p></div>
<div class="m2"><p>که دل بر باد ننهد هیچ عاقل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو می دانی که حال آخرین چیست</p></div>
<div class="m2"><p>چرا می بایدت چون غافلان زیست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منه بنیاد بر این عمر موهوم</p></div>
<div class="m2"><p>که امشب حال فردا نیست معلوم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>درین گفت و گزارش بود پرویز</p></div>
<div class="m2"><p>که از محرم یکی پیش آمدش نیز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که اینک بر در استاده ست شاپور</p></div>
<div class="m2"><p>بیاید یا نیاید چیست دستور</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بشنید این سخن پرویز برجست</p></div>
<div class="m2"><p>دلش می رفت دل بگرفت بر دست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بگفتا هان در آریدش به درگاه</p></div>
<div class="m2"><p>که دیگر چشم نتوان داشت بر راه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به از بی انتظاری نیست برگی</p></div>
<div class="m2"><p>که در هر انتظاری هست مرگی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در آریدش که دریابم وصالی</p></div>
<div class="m2"><p>که هر ساعت به چشمم بود سالی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برون شد شخصی و آورد شاپور</p></div>
<div class="m2"><p>زمین را بوسه زد استاد از دور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس آنگه خسروش از روی اعزاز</p></div>
<div class="m2"><p>به خلوت خواند و فرمودش بگو راز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>زبان بگشود شاپور سخن دان</p></div>
<div class="m2"><p>که بادا بر مرادت چرخ گردان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بمانی تا ابد بر تخت شاهی</p></div>
<div class="m2"><p>به فرمان بادت از مه تا به ماهی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اجازت گر دهی ای شاه عالم</p></div>
<div class="m2"><p>بگویم قصه ها از بیش و از کم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>شدن در دیر و ترسایی گزیدن</p></div>
<div class="m2"><p>به صنعتها رخ آن ماه دیدن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کشیدن نقش شاه و غصه خوردن</p></div>
<div class="m2"><p>به جادویی ورا از راه بردن</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس از صنعت به حیله کردن انگیز</p></div>
<div class="m2"><p>روان کردن ورا بر پشت شبدیز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کنون روشن بود پیشم که آن ماه</p></div>
<div class="m2"><p>نخواهد بود جز در خانه شاه</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو خسرو گشت این حالت عیانش</p></div>
<div class="m2"><p>به شادی بوسه زد چشم و دهانش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بگفتا این مثل از روزگار است</p></div>
<div class="m2"><p>که کار افتاده را یاری ز یار است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نکو رفتی و هم نیکو رسیدی</p></div>
<div class="m2"><p>کرم کردی و زحمتها کشیدی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حکایتهای خود هم شاه برخواند</p></div>
<div class="m2"><p>غبار غم تمام از دل برافشاند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کشیدن غصه ها و شدت راه</p></div>
<div class="m2"><p>رسیدن بی خبر بر چشمه ماه</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>میان آب دیدن آن پری را</p></div>
<div class="m2"><p>پریشان کرده زلف عنبری را</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>و زان پس غصه ها بر غصه خوردن</p></div>
<div class="m2"><p>به هر ساعت ز غم صد بار مردن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو اینها گفت با شاپور پرویز</p></div>
<div class="m2"><p>بگفتا می رود کار از تو، برخیز</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مکن آرام در راه و مکن خواب</p></div>
<div class="m2"><p>سوی شهر مداین تیز بشتاب</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>تحیات و درود از من بخوانش</p></div>
<div class="m2"><p>پس آنگه همچو باد اینجا رسانش</p></div></div>