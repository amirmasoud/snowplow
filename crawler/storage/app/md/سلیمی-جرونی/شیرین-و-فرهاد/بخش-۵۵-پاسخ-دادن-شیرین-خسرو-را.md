---
title: >-
    بخش ۵۵ - پاسخ دادن شیرین خسرو را
---
# بخش ۵۵ - پاسخ دادن شیرین خسرو را

<div class="b" id="bn1"><div class="m1"><p>دگر ره آن مه حسن از سر مهر</p></div>
<div class="m2"><p>نمود از اوج برج خویشتن چهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو برق از ظلمت شب گشت لامع</p></div>
<div class="m2"><p>به خوبی و سعادت گشت طالع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دولت به روی شاه بگشود</p></div>
<div class="m2"><p>چو ماه چارده رخسار بنمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشانید از طبرزد طفل را قوت</p></div>
<div class="m2"><p>گشاد از درج گوهر قفل یاقوت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شاها تا جهان را زندگانی</p></div>
<div class="m2"><p>بود باشی به تخت کامرانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک همچون زمین خاک رهت باد</p></div>
<div class="m2"><p>فلک خاشاک روب درگهت باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهان تا هست بادت پادشاهی</p></div>
<div class="m2"><p>خلایق بنده و انجم سپاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز روی من دو چشمت باد پر نور</p></div>
<div class="m2"><p>رخ خوب تو باد از چشم بد دور</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گل رویت همیشه باد رنگین</p></div>
<div class="m2"><p>دهانت چون لبم همواره شیرین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کمان دولتت پیوسته بر زه</p></div>
<div class="m2"><p>همه روزت یکی بادا ز یک به</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو زلف من پریشانی مبادت</p></div>
<div class="m2"><p>بود کارت چو ابرو در گشادت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو مویم دامنت پر مشک چین باد</p></div>
<div class="m2"><p>مدامت شاهی روی زمین باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو گیسو و دهانم از دو رنگی</p></div>
<div class="m2"><p>مبادت خود پریشانی و ننگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا گفتی جواب تلخ گفتی</p></div>
<div class="m2"><p>نگفتم کانچه خود گفتی شنفتی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دگر گفتی شدی بالا نشستی</p></div>
<div class="m2"><p>به روی من در از هر باب بستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از آن رفتم نشستم برتر تو</p></div>
<div class="m2"><p>که گردم هر نفس گرد سر تو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ازین بالا مرا آنست مقصود</p></div>
<div class="m2"><p>که تو شمعی درین ایوان منت دود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مرا گر بر فلک دوران برآورد</p></div>
<div class="m2"><p>زشبدیز تو باشد بر فلک گرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن بالا شدم چون دود آهت</p></div>
<div class="m2"><p>که تا ناگه نباشم گرد راهت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو چشمی و منت ابرو، مکن خشم</p></div>
<div class="m2"><p>بود پیوسته ابرو بر سر چشم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مگو از زیر و بالا، بیش با ما</p></div>
<div class="m2"><p>که عاشق خود نگوید زیر و بالا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین ره تا رهین زود و دیری</p></div>
<div class="m2"><p>چو گرد آشفته بالا و زیری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دگر گفتی که چشمی زیر پاکن</p></div>
<div class="m2"><p>طریق سرکشی با من رها کن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درین بالا که دل زان در بلای است</p></div>
<div class="m2"><p>به دیدارت که چشمم زیر پای است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دگر گفتی شدم راضی و خشنود</p></div>
<div class="m2"><p>زهی دولت، تو را این هست بهبود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که گرد غم ز راهش بیشکی شد</p></div>
<div class="m2"><p>هر آنکو تلخ و شیرینش یکی شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دگر گفتی تو دوری من نه دورم</p></div>
<div class="m2"><p>که با تو روز و شب اندر حضورم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ندارم از تو یک ساعت جدایی</p></div>
<div class="m2"><p>نه بی مایی که دایم پیش مایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دگر گفتی که تلخی نیست آیین</p></div>
<div class="m2"><p>تو خود تلخی، مکن نسبت به شیرین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مگو تلخم که این از ترهات است</p></div>
<div class="m2"><p>که تلخ من چو حلوای نبات است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ورت گفتم ز شکر تلخ بپذیر</p></div>
<div class="m2"><p>که هست آن از لب من چاشنی گیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر گفتی غریبم چاره جویم</p></div>
<div class="m2"><p>بگو من این سخن پیش که گویم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>حدیثی مشکل و حالی عجیب است</p></div>
<div class="m2"><p>مگو دیگر چنین کز تو غریب است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نظر بر چشم غمازم میاور</p></div>
<div class="m2"><p>برو دیگر به آوازم میاور</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که در عالم اگر بیچاره ای هست</p></div>
<div class="m2"><p>غریبی، خسته ای، آواره ای هست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به زندان عقوبت مانده مهجور</p></div>
<div class="m2"><p>ز خان و مان خویش و دوستان دور</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>غم و دردش به روز بد نشانده</p></div>
<div class="m2"><p>ز ده مانده به جور از شهر رانده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه راه پیشم از غم نی ره پس</p></div>
<div class="m2"><p>پریشان حال و دشمن کام و بی کس</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>درین زندان محنت رو به دیوار</p></div>
<div class="m2"><p>چو بدبختان به درد دل گرفتار</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شب و روز از خیال یار دلتنگ</p></div>
<div class="m2"><p>نشسته چون کلاغی بر سرسنگ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یکایک آنچه گفتم نیک بشمر</p></div>
<div class="m2"><p>منم آنها و صد چندان دیگر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>دگر گفتی که گیرم دامن تو</p></div>
<div class="m2"><p>اگر میرم، بمیرد دشمن تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مگو اینها بادا زنده جانت</p></div>
<div class="m2"><p>رود شیرین، به قربان دهانت</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>الهی تا ابد فرخنده باشی</p></div>
<div class="m2"><p>برای من همیشه زنده باشی</p></div></div>