---
title: >-
    بخش ۱۴ - رفتن شیرین پیش مهین بانو و اجازت خواستن به نخجیر و رفتن به مداین
---
# بخش ۱۴ - رفتن شیرین پیش مهین بانو و اجازت خواستن به نخجیر و رفتن به مداین

<div class="b" id="bn1"><div class="m1"><p>سحر کاشک زلیخا جمله پالود</p></div>
<div class="m2"><p>ز خاور خسرو خور چهره بنمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه حیلت را مجالی ماند نه ریو</p></div>
<div class="m2"><p>پریها گم شدند از صحبت دیو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به صد عشوه پری روی پری زاد</p></div>
<div class="m2"><p>بر تخت مهین بانو بایستاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به رخ ماهی ز مه صد بار بهتر</p></div>
<div class="m2"><p>به قد سروی هزارش ناز در سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دو زلفش کرد رخ چون مار بر گنج</p></div>
<div class="m2"><p>به هر یک غمزه ای یک ناز و صد غنج</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سمن را از بنفشه تاب داده</p></div>
<div class="m2"><p>کله چه بسته و ابرو گشاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بانو گفت کای چشم از تو روشن</p></div>
<div class="m2"><p>چه لطفت از نکویی نیست بر من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه غم کان از برای من نخوردی</p></div>
<div class="m2"><p>چه نیکویی ست کان با من نکردی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>درین موسم که عالم گلستانست</p></div>
<div class="m2"><p>زمین در سایه سنبل نهانست،</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هوس دارم که روی آرم به نخجیر</p></div>
<div class="m2"><p>به دست خود زنم بر آهویان تیر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ولی خواهم که بانو زاستواری</p></div>
<div class="m2"><p>دهد شبدیزم از بهر سواری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهین بانوش گفت ای مهربانم</p></div>
<div class="m2"><p>به دیدار تو روشن جسم(و)جانم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تویی از مردمی چشم مرا نور</p></div>
<div class="m2"><p>که باد از روی خوبت چشم بد دور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به نخجیر ار هوس داری برو تیز</p></div>
<div class="m2"><p>ولی ترسم نباشی مرد شبدیز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که آن دیو آتشی آمد هوایی</p></div>
<div class="m2"><p>پری را نیست با دیو آشنایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پری هرگز نگشت از دیو خرسند</p></div>
<div class="m2"><p>همان بهتر که باشد دیو در بند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشد از دیو هرگز آدمی شاد</p></div>
<div class="m2"><p>چه نسبت دیو را با آدمی زاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>پری رو گفتش ای بانو مخور غم</p></div>
<div class="m2"><p>که در چستی ز مردان نیستم کم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر تو تاب میدانش نداری</p></div>
<div class="m2"><p>دهش با من که هنگام سواری،</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنانش سخت در این بوم تازم</p></div>
<div class="m2"><p>که از نرمیش همچون موم سازم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس آنگه گفت بانویش تو دانی</p></div>
<div class="m2"><p>سواری کن برو چون می توانی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بانو چون صواب افتاد رایش</p></div>
<div class="m2"><p>بشد چون باد و بگرفت از هوایش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو بر شبدیز قایم شد پری زاد</p></div>
<div class="m2"><p>تو گفتی برگ گل را می برد باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زهر سویش شدند آن دلبران هم</p></div>
<div class="m2"><p>یکی بر پشت اشهب یک، بر ادهم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در آن صحرا یکایک در تک و تاز</p></div>
<div class="m2"><p>نماندند از وی و از اسپ وی باز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هزار آهو به هر مژگان گرفته</p></div>
<div class="m2"><p>کمند مویشان شیران گرفته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بدان صحرا همان سگهای تازی</p></div>
<div class="m2"><p>فتاده جمله در روباه بازی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در آن نخجیرکآهو نافه انداخت</p></div>
<div class="m2"><p>کسی کس را ز باد و گرد نشناخت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان شیرین سوی صیدی به تک شد</p></div>
<div class="m2"><p>که جای گرد عنبر بر فلک شد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در آن صحرا پی او تاخت چندان</p></div>
<div class="m2"><p>که گشت از چشم اهل صید پنهان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پس آنگه دختران آن پری زاد</p></div>
<div class="m2"><p>شدند اندر رهش تازنده چون باد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دگر پیکان قدمها برکشیدند</p></div>
<div class="m2"><p>دویدند از پی و گردش ندیدند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نه شبدیزش ز ره چون تیر می برد</p></div>
<div class="m2"><p>کزان بوم و برش تقدیر می برد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>جهان دشتی ست در وی آدمی صید</p></div>
<div class="m2"><p>بدو هر یک به نوعی مانده در قید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که دراین دشت از برنا و از پیر</p></div>
<div class="m2"><p>یکی بیرون نرفت از حکم تقدیر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا بردیش از ره، آن سواری</p></div>
<div class="m2"><p>اگر نی کردی اش تقدیر یاری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر آنکس را که چیزی سرنوشت است</p></div>
<div class="m2"><p>رسد پیشش اگر خوب است و زشت است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدین اسرار واقف هر کسی نیست</p></div>
<div class="m2"><p>که این سر رشته در دست کسی نیست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به راهی هر کسی خوش می زند گام</p></div>
<div class="m2"><p>چه می داند که چون باشد سرانجام</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چه خوش زد این مثل آن پیر گستاخ</p></div>
<div class="m2"><p>که بی تقدیر برگی نفتد از شاخ</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو تدبیر تو با تقدیر شد هیچ</p></div>
<div class="m2"><p>مشو با رشته تقدیر در پیچ</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>پس از رفتن رفیقان بازگشتند</p></div>
<div class="m2"><p>همه با خون دل دمساز گشتند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>شدند از کار شیرین جمله غمگین</p></div>
<div class="m2"><p>خبر بردند بانو را که شیرین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر چه صید آهو بی عدد کرد</p></div>
<div class="m2"><p>رسید آهویی او را صید خود کرد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مهین بانو ازین غم جامه زد چاک</p></div>
<div class="m2"><p>به صد زاری ز تخت افتاد بر خاک</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زمانی نوحه و فریادش آمد</p></div>
<div class="m2"><p>ولی از خواب دیده یادش آمد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>که یک شب پیش از آن در خواب دیدی</p></div>
<div class="m2"><p>که بازی ناگه از دستش پریدی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>پس از روزی دو با صیدی جهانگیر</p></div>
<div class="m2"><p>به دستش آمدی از امر تقدیر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو با یاد آمدش آن خواب دیده</p></div>
<div class="m2"><p>از آن اندوه و غم گشت آرمیده</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به دل گفتا که بی صبری نشاید</p></div>
<div class="m2"><p>که در کار آدمی را صبر باید</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شوم در صابری راضی و خشنود</p></div>
<div class="m2"><p>که صبر آمد کلید گنج مقصود</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مهین بانو بدی زان غم شب و روز</p></div>
<div class="m2"><p>به دل صابر ولی در آتش و سوز</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گهی کز روی خویش یاد کردی</p></div>
<div class="m2"><p>زمانی نوحه و فریاد کردی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دگر چون باز، در آن خواب دیدی</p></div>
<div class="m2"><p>شدی خاموش و یکدم آرمیدی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدی دایم ز خود بیگانه گشته</p></div>
<div class="m2"><p>پری رویانش هم دیوانه گشته</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز بعد آنکه افتاد این تباهی</p></div>
<div class="m2"><p>پس از فرمان و تقدیر الهی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو از این سو دل احباب خون شد</p></div>
<div class="m2"><p>از آن سو حال شیرین بین که چون (شد)</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شب و روز آن پری چون باد می رفت</p></div>
<div class="m2"><p>دمی غمگین زمانی شاد می رفت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>سرو کارش گذشت از شادی و غم</p></div>
<div class="m2"><p>نمی خفت و نمی آسود یک دم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جگر پر خون پریشان گشته اوقات</p></div>
<div class="m2"><p>همه ره بود با حق در مناجات</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>گهی گفتی که یارب چیست تدبیر</p></div>
<div class="m2"><p>که سرگردانم اندر دست تقدیر</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>درین حالت چو تقدیر من از توست</p></div>
<div class="m2"><p>به حالم بین که تدبیر من از توست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چه خواهی کرد تدبیری برایم</p></div>
<div class="m2"><p>به غیر از آنکه باشی رهنمایم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همی نالید و بی تدبیر می ساخت</p></div>
<div class="m2"><p>ز خود می رفت و با تقدیر می ساخت</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گهی گفتی خداوندا تو دانی</p></div>
<div class="m2"><p>که بر من تلخ گشت این زندگانی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بهاری ده، دی ام نوروز گردان</p></div>
<div class="m2"><p>به فضل خود شبم را روزگردان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مسوزم بیش، از داغ جدایی</p></div>
<div class="m2"><p>وزین تاریکی ام ده روشنایی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>به لطف خویش حل کن مشکلم را</p></div>
<div class="m2"><p>بمردم طاقتی بخش این دلم را</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>پس از آن زاری و آن بی قراری</p></div>
<div class="m2"><p>در آن بیچارگی و سوگواری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همی شد راه اندر تاب و در تب</p></div>
<div class="m2"><p>به روزش تا بر آمد چارده شب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>سپیده دم که خور سر زد ز کهسار</p></div>
<div class="m2"><p>شد از عالم سیاهی ناپدیدار</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز گشت شب، دل مهتاب شد سست</p></div>
<div class="m2"><p>سوار روز، روی از گرد شب شست</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>گهی شادان به صد دل، گاه غمگین</p></div>
<div class="m2"><p>سوار تیزتک یعنی که شیرین</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>به جایی دلکش(و) خرم فرو راند</p></div>
<div class="m2"><p>که در زیبایی او عقل درماند</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>چه جایی، جنتی، جنت چه باشد</p></div>
<div class="m2"><p>در آنجا چشمه کوثر چه باشد</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>حیاتی اندرو زان سان که دانی</p></div>
<div class="m2"><p>و زو در خجلت آب زندگانی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>چو دید آن چشمه آن ماه جهانتاب</p></div>
<div class="m2"><p>رخ خود دید چون مهتاب در آب</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>بدان چشمه ز هر سو چشم بگشاد</p></div>
<div class="m2"><p>ندید از هیچ سویی آدمی زاد</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>فرود آمد ز اسپ آن شوخ سرمست</p></div>
<div class="m2"><p>درختی جست و پس شبدیز را بست</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>پس آنگه یک پرند از بقچه بگشود</p></div>
<div class="m2"><p>به خود بربست و در آن آب شد زود</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چه گویم نام آن چشمه از آن گاه</p></div>
<div class="m2"><p>که او در آب شد، شد چشمه ماه</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>عجب دارم که از مه تابه ماهی</p></div>
<div class="m2"><p>تواند گفت کس وصفش کماهی</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بسا کس چشمه ماهی شنیده ست</p></div>
<div class="m2"><p>به عالم چشمه مه کس ندیده ست</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>دگر خورشید شب راهی که پوید</p></div>
<div class="m2"><p>شود چون روز رو زان چشمه شوید</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>شب تیره مهی روشن نمودی</p></div>
<div class="m2"><p>درو گر دانه خشخاش بودی</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>پری رو غوطه ای خورد اندر آن آب</p></div>
<div class="m2"><p>چنان کافتد میان آب مهتاب</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به هر باری که سر از آب برزد</p></div>
<div class="m2"><p>تو گفتی آفتاب از آب سرزد</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ز چشمه نور بر مهتاب می شد</p></div>
<div class="m2"><p>فلک را چشمها پر آب می شد</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>رخش در آب با آن جعد سنبل</p></div>
<div class="m2"><p>میان آب رسته سبزه و گل</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>در آن چشمه چو بر تن آب می ریخت</p></div>
<div class="m2"><p>به روی سیم، مروارید می بیخت</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>هر آبی کو از آن چشمه به سر کرد</p></div>
<div class="m2"><p>سراسر پر ز مروارید تر کرد</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>قد او در میان چشمه یکسر</p></div>
<div class="m2"><p>نشان می داد از طوبی و کوثر</p></div></div>