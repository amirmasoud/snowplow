---
title: >-
    بخش ۴۸ - قصه شکر اصفهانی
---
# بخش ۴۸ - قصه شکر اصفهانی

<div class="b" id="bn1"><div class="m1"><p>چو مریم را سر آمد عمر، خسرو</p></div>
<div class="m2"><p>طلب کرد از بزرگان همسر نو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزرگانی که بودندش ملازم</p></div>
<div class="m2"><p>همه یکسر بدین گشتند جازم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که در شهر صفاهان دلبری هست</p></div>
<div class="m2"><p>که مانندش عجب گر دیگری هست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حسن اعجوبه دور زمان است</p></div>
<div class="m2"><p>به مجلس داری آشوب جهان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ملک دلربایی بس که نیکوست</p></div>
<div class="m2"><p>ز مشرق تا به مغرب شهرت اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شیرینی که دارد آن دلارام</p></div>
<div class="m2"><p>زمانه کرده است او را شکر نام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به نطق آمد غلام شکرش قند</p></div>
<div class="m2"><p>هزارش دل به هر یک موست در بند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مجو زان حسن در اطراف عالم</p></div>
<div class="m2"><p>که مثلش نیست در اکناف عالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو او ماهی ندیده کس در آفاق</p></div>
<div class="m2"><p>که در خوبی ست چون ابروی خود طاق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کنیز و خادمش هر دو به درگاه</p></div>
<div class="m2"><p>یکی را نام مهر است و یکی ماه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخش ماهی ست، مهرش گشته همبر</p></div>
<div class="m2"><p>قدش سرویست، خورشیدیش بر سر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دو چشمش در نکویی شهره هر دو</p></div>
<div class="m2"><p>خجل زو مشتری و زهره هر دو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دو گیسویش به گاه سرفرازی</p></div>
<div class="m2"><p>شب عشاق را داده درازی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر رویش که خورشیدی ست در خور</p></div>
<div class="m2"><p>دهانش ذره ای و ذره کمتر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شکر صد تنگ ریزد هر طرف بیش</p></div>
<div class="m2"><p>چو جنباند به نام خود، لب خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اگر خواهی ز لعل و چشم او نام</p></div>
<div class="m2"><p>برو آن را ز شکر پرس و بادام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر خواهد دلت از لعل او قوت</p></div>
<div class="m2"><p>گهر هایش بجو از در و یاقوت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لب چون شکرش به از نبات است</p></div>
<div class="m2"><p>دهانش چشمه آب حیات است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن چشمان که هر سو ناز دارد</p></div>
<div class="m2"><p>عجب ترکان تیرانداز دارد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به چشمان حاجب از ابرو نشانده</p></div>
<div class="m2"><p>به هریک غمزه، صد جادو نشانده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان لب، کی بت چینیش باشد</p></div>
<div class="m2"><p>مگر شیرین به شیرینیش باشد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو خسرو ذوق شیرینی ازو یافت</p></div>
<div class="m2"><p>به خواهش در طلبکاریش بشتافت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگفتا بارها من وصف آن ماه</p></div>
<div class="m2"><p>شنیدستم ز مجلس ها به افواه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عجب گر سوی او باشد نگاهم</p></div>
<div class="m2"><p>که من محبوب هر جایی نخواهم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بزرگانی که او را دیده بودند</p></div>
<div class="m2"><p>صفتهایش همه بشنیده بودند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دارای جهان خوردند سوگند</p></div>
<div class="m2"><p>که او هرگز به کس نگرفت پیوند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بلی دارد کنیزی چند چون ماه</p></div>
<div class="m2"><p>که بر مردم زند از دلبری راه</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به مجلسها رود، لیکن دم کار</p></div>
<div class="m2"><p>رود او و کنیزان را دهد بار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به رعناییش سرو بوستان نیست</p></div>
<div class="m2"><p>به عیاری آن مه در جهان نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بشنید این سخن، خسرو طمع کرد</p></div>
<div class="m2"><p>که از خرما شکر بهتر توان خورد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مزاجم را به غایت هست در خور</p></div>
<div class="m2"><p>به جای نخل مریم تنگ شکر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نمی باید ز شکر داشتن دست</p></div>
<div class="m2"><p>که خرما را به شکر نسبتی هست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل خود را دهم یکبار تسکین</p></div>
<div class="m2"><p>بدین شکر ز تلخیهای شیرین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دل شه چون بدین گردید مایل</p></div>
<div class="m2"><p>بگفت این کار باید کرد حاصل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بزرگی از صفاهان پیش شه بود</p></div>
<div class="m2"><p>فرستادش به سوی اصفهان زود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بدو داد از خزاین مال بسیار</p></div>
<div class="m2"><p>دگر زاسباب آنچش بود در کار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بزرگ آنگه ندید آن کار را خرد</p></div>
<div class="m2"><p>ببرد آنها و شکر را بیاورد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو آمد سوی خسرو تنگ شکر</p></div>
<div class="m2"><p>به استقبال او رفتند لشکر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در آوردند در باغی به نازش</p></div>
<div class="m2"><p>شدند اهل مداین پیشوازش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>رسانیدند آنگاه آن مه نو</p></div>
<div class="m2"><p>به اعزاز تمامش پیش خسرو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملک آنگاه، آن در یگانه</p></div>
<div class="m2"><p>ببستش عقد و بردش سوی خانه</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شبی تا روز، با وی عیش بگداشت</p></div>
<div class="m2"><p>ز درج سر به مهرش، مهر برداشت</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>دلش هر گه ز شیرین یاد می کرد</p></div>
<div class="m2"><p>به شکر خاطر خود شاد می کرد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>به شکر چند روزی گشت خشنود</p></div>
<div class="m2"><p>که در خرمای مریم استخوان بود</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ولی هر چند خود می ساخت مشغول</p></div>
<div class="m2"><p>ز عقلش، عاشقی می کرد معزول</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نمی کرد از غم شیرین به شب خواب</p></div>
<div class="m2"><p>که نبود تشنه را تدبیر، جز آب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نمی شد از شکر، تلخیش تسکین</p></div>
<div class="m2"><p>به خود هر چند کان می کرد شیرین</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به تلخی با شکر، ناچار می ساخت</p></div>
<div class="m2"><p>شکر، شیرینی ای با خویش می باخت</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز گرمی شکر شد چهره اش زرد</p></div>
<div class="m2"><p>که بیش از پیش، حلوای شکر خورد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>نبد چون شربت شیرینش در خور</p></div>
<div class="m2"><p>از آن، دل سوختش حلوای شکر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>از آن گرمی، تن خسرو بر افروخت</p></div>
<div class="m2"><p>که از شیرینی شکر، دلش سوخت</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شکر کرد این احوال معلوم</p></div>
<div class="m2"><p>تو گفتی آتش افکندند در موم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به تنگ آمد دلش، بگداشت نازش</p></div>
<div class="m2"><p>ز آب چشم خود شد در گدازش</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تنش بگداخت، ز آب چشم بی خواب</p></div>
<div class="m2"><p>بلی، تنگ شکر بگدازد از آب</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>شود تا شربت قندش چشیده</p></div>
<div class="m2"><p>گلابی می زدش از آب دیده</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>در آن آب و عرق بگداخت شکر</p></div>
<div class="m2"><p>که خسرو داشت دل، با جای دیگر</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>غم شیرینش منزل کرد در دل</p></div>
<div class="m2"><p>شکر در کام او شد زهر قاتل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز جام عاشقی شد باز سرمست</p></div>
<div class="m2"><p>دگر سودای شیرین بردش از دست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>دگر ره با سر پرگار خود شد</p></div>
<div class="m2"><p>ز کار افتاده بد، با کار خود شد</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>بسی سرگشتگیها آیدش پیش</p></div>
<div class="m2"><p>چو افتد مرد از سر رشته خویش</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چه در عشق حقیقی چه مجازی</p></div>
<div class="m2"><p>مهل تا رشته خود گم نسازی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به بازار جهان بازی خورد کم</p></div>
<div class="m2"><p>هر آن کو دارد این سر رشته محکم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>مزن پر، پا به سر کز کنه بی عیب</p></div>
<div class="m2"><p>چو فرمان شد که بیرون آیی از غیب</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به بختت هر چه آن گردید طالع</p></div>
<div class="m2"><p>کنندت باز با آن خیر راجع</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>قضا این دان و تقدیر این چنین است</p></div>
<div class="m2"><p>همین حب الوطن را معنی این است</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>اگر این راز پنهان باز یابی</p></div>
<div class="m2"><p>به درد خویش درمان بازیابی</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چو شه را از ازل این راز دادند</p></div>
<div class="m2"><p>به دستش رشته دیگر باز دادند</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دگر با رشته تقدیر پیوست</p></div>
<div class="m2"><p>که بودش از ازل آن رشته در دست</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دگر با عشق سر آورد و بر راه</p></div>
<div class="m2"><p>بگفت از رفته ها، استغفرالله</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>زکفر غیر رو آورد با دین</p></div>
<div class="m2"><p>فتادش باز در سر شور شیرین</p></div></div>