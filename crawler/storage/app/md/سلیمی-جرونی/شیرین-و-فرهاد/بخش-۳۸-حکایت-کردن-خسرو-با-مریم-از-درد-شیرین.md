---
title: >-
    بخش ۳۸ - حکایت کردن خسرو با مریم از درد شیرین
---
# بخش ۳۸ - حکایت کردن خسرو با مریم از درد شیرین

<div class="b" id="bn1"><div class="m1"><p>در آن عشرت چو از شب رفت پاسی</p></div>
<div class="m2"><p>ز می نوشید خسرو چند کاسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پا برخاست بی خود همچو مستان</p></div>
<div class="m2"><p>چو شمعی روی کرد اندر شبستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مریم راز شیرین جمله بگشاد</p></div>
<div class="m2"><p>چو دامن ساعتی در پایش افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای مریم به روح الله سوگند</p></div>
<div class="m2"><p>که با غیرت ندارم مهر و پیوند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولی دانی که این شیرین مهجور</p></div>
<div class="m2"><p>به مهر من شده ست از خان و مان دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی داند از آنجا رو به سویی</p></div>
<div class="m2"><p>به غیر از ما ندارد راه و رویی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غریبست و غریبی نیست بازی</p></div>
<div class="m2"><p>غریبی را چه باشد گر نوازی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اجازت ده که آریمش برین در</p></div>
<div class="m2"><p>تو او را جز کنیز خویش مشمر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببین حالش مگو دیگر ز ماضی</p></div>
<div class="m2"><p>که هست او بر کنیزی تو راضی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیاید او به تو ای مه به میزان</p></div>
<div class="m2"><p>که هستت یک کنیزی از کنیزان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مریم آن حکایتهای جانکاه</p></div>
<div class="m2"><p>در آن مستی همه بشنید از شاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو زلف خویشتن یکدم بر آشفت</p></div>
<div class="m2"><p>به خود چون مار از آن پیچید و پس گفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که رو شاها که این دستان و این فن</p></div>
<div class="m2"><p>نگیرد گر دم عیسی ست با من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مجو این سود من، کم جز زیان نیست</p></div>
<div class="m2"><p>که مریم همنشین جادوان نیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منی را کزدم عیسی ملال است</p></div>
<div class="m2"><p>نشستن با چنین جادو محال است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بدان سویی که من باشم، بدان سو</p></div>
<div class="m2"><p>فرشته ره ندارد خاصه جادو</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مگو شیرین که جادوی جهان است</p></div>
<div class="m2"><p>از آن همسایه بابل ستان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کسی کز جادویی ملک جهان سوخت</p></div>
<div class="m2"><p>ازو جادوئیش می باید آموخت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مگو با من دگر زین نکته ای شاه</p></div>
<div class="m2"><p>وگر نه یک شبی بینی که ناگاه،</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زموی خود بتابم یک رسن من</p></div>
<div class="m2"><p>بیاویزم ازو خود را به گردن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شوم نابود از آزردن تو</p></div>
<div class="m2"><p>بود خون من اندر گردن تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو گر زین سان به خون بنده تازی</p></div>
<div class="m2"><p>نماند در جهانت سرفرازی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو خسرو دید کان رومی طناز</p></div>
<div class="m2"><p>جواب تلخ از شیرین دهد باز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به مریم گفت کای عیسی دم من</p></div>
<div class="m2"><p>مخور غم تا نیفزاید غم من</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مباش از آنچه گفتم هیچ دلتنگ</p></div>
<div class="m2"><p>که او را جا خوش است اندر سر سنگ</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>من این گفتم ولی در دل نبودم</p></div>
<div class="m2"><p>بدان ای مه تو را می آزمودم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز شیرین تلخیی کز تو شنفتم</p></div>
<div class="m2"><p>حدیثی زو اگر گفتم نگفتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مکن از آنچه گفتم وقت ناخوش</p></div>
<div class="m2"><p>تودل خوش دار کو را هست جا خوش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس آنگه گفت با شاپور برخیز</p></div>
<div class="m2"><p>بر آن سرو گلرخسار رو تیز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ببوسش پای و شو چون خاک راهش</p></div>
<div class="m2"><p>بپرس و عذرها از من بخواهش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به مشکوی من از مریم نهانی</p></div>
<div class="m2"><p>بیار او را به هر نوعی که دانی</p></div></div>