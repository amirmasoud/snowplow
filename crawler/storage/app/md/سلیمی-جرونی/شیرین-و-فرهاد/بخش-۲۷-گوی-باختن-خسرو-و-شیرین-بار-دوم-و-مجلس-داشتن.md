---
title: >-
    بخش ۲۷ - گوی باختن خسرو و شیرین بار دوم و مجلس داشتن
---
# بخش ۲۷ - گوی باختن خسرو و شیرین بار دوم و مجلس داشتن

<div class="b" id="bn1"><div class="m1"><p>با یکدیگر در کنار شهرود و پیدا شدن شیر،</p></div>
<div class="m2"><p>و کشته شدن به دست خسرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شباهنگام کان ترک پری روی</p></div>
<div class="m2"><p>ربود از صحن میدان فلک، گوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کواکب کرده بهر زرفشانی</p></div>
<div class="m2"><p>طبقهای فلک را زر نشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شب عنبر فروش از زلف در هم</p></div>
<div class="m2"><p>نهاده توده های مشک بر هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه شب کز هر طرف نور کواکب</p></div>
<div class="m2"><p>چو آتش باز بنموده عجایب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگویم گر نپنداری محالی</p></div>
<div class="m2"><p>در آن شب کز درازی بود سالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوار شرق چندان کرده بد ره</p></div>
<div class="m2"><p>که نتوانست دم زد تا سحرگه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مرغ صبح بال و پر بر افشاند</p></div>
<div class="m2"><p>سحرگه سوره و الفجر بر خواند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تکاورها به جولان برگرفتند</p></div>
<div class="m2"><p>همه گو باختن از سر گرفتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو گو از صحن میدان در ربودند</p></div>
<div class="m2"><p>محبان گنج گوهر برگشودند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برافشاندند هر جا گوهری بود</p></div>
<div class="m2"><p>به میدان گوی شد هر جا سری بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چوگان گرچه هر یک دست یازید</p></div>
<div class="m2"><p>چو شیرین هیچ کس شیرین نبازید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ملک هر گه که با او بازخوردی</p></div>
<div class="m2"><p>نمی بودش مجال دستبردی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>و زان چون نقره در آتش همی تافت</p></div>
<div class="m2"><p>که با وی یک زمان فرصت نمی یافت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن میدان که بد هر یک ز یک به</p></div>
<div class="m2"><p>فلک احسن همی گفت و ملک زه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو خسرو دید کز چوگان طرازی</p></div>
<div class="m2"><p>نخواهد کام ازو دیدن به بازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به شیرین گفت کای سرو سرافراز</p></div>
<div class="m2"><p>به رویت دیده اهل نظر باز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیا تا از کمان گوییم و از تیر</p></div>
<div class="m2"><p>زگو بازی، کنیم آهنگ نخجیر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که گشته ست از خوشی هم کوه و هم راغ</p></div>
<div class="m2"><p>گلستان در گلستان باغ در باغ</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نگر از یاسمین و جعد سنبل</p></div>
<div class="m2"><p>همه روی زمین پر سبزه و گل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرصع شد زمین چون چتر کاووس</p></div>
<div class="m2"><p>ملمع شد زمان چون پر طاووس</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو گفتی می پرد رنگ از رخ ماه</p></div>
<div class="m2"><p>صبوحی می کند چون گل سحرگاه</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صبا گویی که عنبر بار دارد</p></div>
<div class="m2"><p>بنفشه بوی زلف یار دارد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز بوی عطر کز هر سوفتاده</p></div>
<div class="m2"><p>چمن، دکان عطاری گشاده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چمن بر تخت باغ انداخته رخت</p></div>
<div class="m2"><p>نشسته خسرو گل بر سر تخت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبوحی کرده مرغان سحر مست</p></div>
<div class="m2"><p>چمن آراسته خود را به صد دست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بس کز گریه بلبل روی گل شست</p></div>
<div class="m2"><p>به صحن باغ گل از خنده شد سست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زمین از لاله و گلهای بادام</p></div>
<div class="m2"><p>صراحی بر صراحی جام بر جام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تذروان کرده خون لاله پامال</p></div>
<div class="m2"><p>زبان سوسن است از این سخن لال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کشیده باد گیسوی ریاحین</p></div>
<div class="m2"><p>بنفشه تاب داده زلف پرچین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز مستی باد صبح افتان و خیزان</p></div>
<div class="m2"><p>شده با غنچه ها دست و گریبان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شکر لب، دستبرد شاه چون دید</p></div>
<div class="m2"><p>به خاک افتاد و پای شاه بوسید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به پای شاه خود را چون زمین کرد</p></div>
<div class="m2"><p>زبان بگشاد و شه را آفرین کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>که شاها تا سفیدی و سیاهی</p></div>
<div class="m2"><p>بود، بادی به تختت پادشاهی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سعادت یار و اقبالت ز هر سو</p></div>
<div class="m2"><p>شب و روزت غلام ترک و هندو</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همه کارتو با رامشگران باد</p></div>
<div class="m2"><p>سرت سبز و لبت خندان و دل شاد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بود حق تو در عالم دلیری</p></div>
<div class="m2"><p>تو را زیبد به عالم شیرگیری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو این دستی که بنمودی به عالم</p></div>
<div class="m2"><p>نه دستان کرد نه سام و نه رستم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نگردد با تو گردون هم ترازو</p></div>
<div class="m2"><p>هزارت آفرین بر دست و بازو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به نور آتش و سیمای خورشید</p></div>
<div class="m2"><p>که افزونی ز افریدون و جمشید</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز شاهان جهان آنان که دانی</p></div>
<div class="m2"><p>کیان تا جند و تو تاج کیانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو را زیبد به عالم شهریاری</p></div>
<div class="m2"><p>که در ملک جهان همتا نداری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو گفت این وصفهای شاه شیرین</p></div>
<div class="m2"><p>دگر با گردش آمد جام زرین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ندیمان باز در مجلس نشستند</p></div>
<div class="m2"><p>حریفان از پریشانی برستند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شدند از گردش ساغر همه مست</p></div>
<div class="m2"><p>یکی در رقص پا می زد یکی دست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>چو مجلس گرم گشت از باده نوشان</p></div>
<div class="m2"><p>شدند آن بلبلان بر گل، خروشان</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در آن بستان ز سیب و به گزیدن</p></div>
<div class="m2"><p>ملک آمد به شفتالود چیدن</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بلی چون آتشش از می بیفزود</p></div>
<div class="m2"><p>برش از سیب شفتالود به بود</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ملک زان شور شیرین یافت اکرام</p></div>
<div class="m2"><p>که یابد مرد در آشوبها کام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به عالم فتنه ای تا در نگیرد</p></div>
<div class="m2"><p>کسی کامی ز عالم بر نگیرد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر آن فتنه کز آن افزون نباشد</p></div>
<div class="m2"><p>بدان، کز حکمتی بیرون نباشد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو آید پیش غوغای زمانی</p></div>
<div class="m2"><p>مشو غمگین در آن غوغا چه دانی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بسا بد، کان همه بهبود باشد</p></div>
<div class="m2"><p>زیان باشد بسی کان سود باشد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>مبین بد، بدگرت آتش به جان زد</p></div>
<div class="m2"><p>که از چیزی نباشد خالی آن بد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>مگو کز بد دل من بی قرار است</p></div>
<div class="m2"><p>که نیک و بد به عالم در گدار است</p></div></div>