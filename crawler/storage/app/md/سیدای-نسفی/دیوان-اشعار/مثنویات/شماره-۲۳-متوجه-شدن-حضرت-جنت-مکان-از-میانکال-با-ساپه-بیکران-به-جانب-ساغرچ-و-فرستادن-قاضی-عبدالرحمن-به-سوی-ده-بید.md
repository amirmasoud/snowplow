---
title: >-
    شمارهٔ  ۲۳ - متوجه شدن حضرت جنت مکان از میانکال با ساپه بیکران به جانب ساغرچ و فرستادن قاضی عبدالرحمن به سوی ده بید
---
# شمارهٔ  ۲۳ - متوجه شدن حضرت جنت مکان از میانکال با ساپه بیکران به جانب ساغرچ و فرستادن قاضی عبدالرحمن به سوی ده بید

<div class="b" id="bn1"><div class="m1"><p>دم صبح کاین خسرو تاج بخش</p></div>
<div class="m2"><p>به اقلیم گیری طلب کرد رخش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمد برین وادی پر نفاق</p></div>
<div class="m2"><p>گذشت از میانکال با طمطراق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به مردم خروش روا روفگند</p></div>
<div class="m2"><p>به عالم چو خورشید پرتو فگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جنبش درآمد زمان و زمین</p></div>
<div class="m2"><p>به اهل سمرقند شد این یقین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که شاه فلک قدر انجم سپاه</p></div>
<div class="m2"><p>چو مه بر سر ما زند خیرگاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازین مژده کردند مردم حضور</p></div>
<div class="m2"><p>برآمد بر افلاک بانگ سرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شکفتند چون گل سمرقندیان</p></div>
<div class="m2"><p>که آمد سوی باغ آن باغبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بزرگان شهر از پی یکدگر</p></div>
<div class="m2"><p>پی پیشبازش نهادند سر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به ده بیدیان چون رسید این خبر</p></div>
<div class="m2"><p>که آمد شهنشاه خیرالبشر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همه سرکشان گشته فرمان برش</p></div>
<div class="m2"><p>همه از دل و جان شده چاکرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سپاهی که بیرون بود از حساب</p></div>
<div class="m2"><p>ندیدست زین پیش دوران به خواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به خدمت کمربسته چون مور چست</p></div>
<div class="m2"><p>به هم ساخته عهد و پیمان درست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به زهر آب داده نظرهایشان</p></div>
<div class="m2"><p>بود تیغ بازی هنرهایشان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر کوه آهن شود رو به رو</p></div>
<div class="m2"><p>نتابند مانند فرهاد رو</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلیرند چون غمزه دلبران</p></div>
<div class="m2"><p>به خون ریختن تیز کرده سنان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود حلقه چشم ایشان کمند</p></div>
<div class="m2"><p>به یک دیدن آرند دشمن به بند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نشینند پیوسته پیر و جوان</p></div>
<div class="m2"><p>به یک خانه آرند تیر و کمان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو این قصه را خواجه رازق شنید</p></div>
<div class="m2"><p>سراسیمه با خواجه مهدی رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفتا که شاه فلک آشیان</p></div>
<div class="m2"><p>شنیدم به ساغرچ برده عنان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هنوزش که آن آفتاب سپهر</p></div>
<div class="m2"><p>نکرده به مردم عیان کین و مهر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هنوزش که آن برق پر از ستیز</p></div>
<div class="m2"><p>نگشته به صحرای ما شعله ریز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گروهی که غارتگر عالمند</p></div>
<div class="m2"><p>ز بهر اطاعت به ما همدمند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگیریم این قوم را بی دریغ</p></div>
<div class="m2"><p>ببریم سرهای ایشان به تیغ</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به اقبال شاهنشه پاک و دین</p></div>
<div class="m2"><p>نباشد به ما تحفه ای به ازین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به آن شه شویم از ته دل مطیع</p></div>
<div class="m2"><p>بیاریم روح بزرگان شفیع</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود او شهنشاه صاحب کرم</p></div>
<div class="m2"><p>به بختش چو خورشید باشد علم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>محیط است آن شاه جوئیم ما</p></div>
<div class="m2"><p>نمک خورده خوان اوئیم ما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خصومت از این خاندان نیست باب</p></div>
<div class="m2"><p>چه سازد صف ذره با آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شهان را به شاهان بود همسری</p></div>
<div class="m2"><p>کند شیر با شیر کین آوری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نکرده به شه دشمنی هیچ کس</p></div>
<div class="m2"><p>چه سازد به آتش صف خار و خس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زبان بعد از آن خواجه مهدی کشاد</p></div>
<div class="m2"><p>چنین بود با او جوابی که داد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ندانسته ای خود که من مهدیم</p></div>
<div class="m2"><p>به این قوم تاراجگر عهدیم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چنین مصلحت سر به سر هست خام</p></div>
<div class="m2"><p>بود آمد شه سخن های عام</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مکن هیچ اندیشه بنشین به جا</p></div>
<div class="m2"><p>بخارا کجا باشد و ما کجا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به کار خود آن خلق درمانده اند</p></div>
<div class="m2"><p>از این ناحیت دامن افشانده اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بگفتا به او خواجه رازق جواب</p></div>
<div class="m2"><p>مرا آمد ارواح امشب به خواب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز هر جانبی آتش افروختند</p></div>
<div class="m2"><p>تر و خشک ده بید را سوختند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به افلاک شد بانگ بیداد داد</p></div>
<div class="m2"><p>بدادند خاکسترش را به باد</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نباشد مرا در دل از غیر پاک</p></div>
<div class="m2"><p>ازین خواب شوریده ام هولناک</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مرا هست امروز خاطر ملول</p></div>
<div class="m2"><p>ندارند ارواح ما را قبول</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>همان به که زین جای بیرون شویم</p></div>
<div class="m2"><p>وداع وطن کرده یکسو رویم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگفت این و برخاست از جای خویش</p></div>
<div class="m2"><p>ره رفتن خویش بگرفت پیش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>به صد عز و شأن پادشاه و سپاه</p></div>
<div class="m2"><p>چو کردند ساغرچ را تکیه گاه</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز هر سو خلایق به دیدار شه</p></div>
<div class="m2"><p>نهادند شادی کنان رو به ره</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>رسیدند هر یک بر آن آستان</p></div>
<div class="m2"><p>به کف تحفه های گران نقد جان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همه حال خود را بیان ساختند</p></div>
<div class="m2"><p>وطن های خود را عیان ساختند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ندیدند مردم بر آن آستان</p></div>
<div class="m2"><p>نشانی ز ده بید و ده بیدیان</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شه مرحمت کیش و دشمن نواز</p></div>
<div class="m2"><p>بود دایما در جهان سرفراز</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>رسولی طلب کرد با احترام</p></div>
<div class="m2"><p>که از ما رسان خواجگان را سلام</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بگویش که اینست خوان شما</p></div>
<div class="m2"><p>خلایق شده میهمان شما</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>به مهمان نکرده کسی ترشروی</p></div>
<div class="m2"><p>بود صاحب خانه گر تند خوی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نکردند هرگز چنین ماجرا</p></div>
<div class="m2"><p>به اجداد ما بزرگان شما</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>شما تا به کی بی وفایی کنید</p></div>
<div class="m2"><p>به ما چند ناآشنایی کنید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نباشد چنین کار کار شما</p></div>
<div class="m2"><p>خدا می کند حق ز باطل جدا</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه باشد اگر کار آسان کنید</p></div>
<div class="m2"><p>توجه به روح بزرگان کنید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به هر جانبی میل ایشان شود</p></div>
<div class="m2"><p>به ما و شما کار آسان شود</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>ز اقبال قاصد سبک خیز شد</p></div>
<div class="m2"><p>زمین بوسه داد و به ره تیز شد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو قاصد ز تاب ره افروخته</p></div>
<div class="m2"><p>به ده بید آمد نفس سوخته</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نظر باز کرد از یمین و یسار</p></div>
<div class="m2"><p>به گردون رسیده زبان شرار</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>ز گرمی گریزان شده آفتاب</p></div>
<div class="m2"><p>به زیر زمین گاو ماهی کباب</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>شراری که ماننده اژدها</p></div>
<div class="m2"><p>لبی بر زمین یکی لب در هوا</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گریزان به هر سوی خورد و بزرگ</p></div>
<div class="m2"><p>چو از لشکر شاه روباه و گرگ</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چو بودند ده بیدیان خودپسند</p></div>
<div class="m2"><p>در آتش فتادند همچون سپند</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بزرگی که سرکش شود چون چنار</p></div>
<div class="m2"><p>به جان خود آخر زند خود شرار</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>بود زلف از سرکشی در شکست</p></div>
<div class="m2"><p>ز گردنکشی می شود شعله پست</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>فرستاده آمد ز ده بید زود</p></div>
<div class="m2"><p>فرو رفته در گرد مانند دود</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>درآمد برافروخته در سخن</p></div>
<div class="m2"><p>چو شمعی که روشن کند انجمن</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>در آغاز گفت ای خداوند کار</p></div>
<div class="m2"><p>قیامت به ده بید شد آشکار</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>فتادست آتش در آن سرزمین</p></div>
<div class="m2"><p>چه آتش که سر تا به پا قهر و کین</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>چو بودند آن قوم دور از ادب</p></div>
<div class="m2"><p>خدا کرده آخر به ایشان غضب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>تبه شد در ایام احوالشان</p></div>
<div class="m2"><p>به تاراج بردند اموالشان</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>زمینی که می رست مهر گیاه</p></div>
<div class="m2"><p>برابر شد آخر به خاک سیاه</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>به هر کس بلایی که آمد ز پیش</p></div>
<div class="m2"><p>یقین دان که باشد ز کردار خویش</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>همین است تنبور را حسب حال</p></div>
<div class="m2"><p>ز دست نوا می خورد گوشمال</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بیا ساقی آن جام آتش نفس</p></div>
<div class="m2"><p>که گرم است ازو صحبت خار و خس</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به من ده که بخشد دلم را حضور</p></div>
<div class="m2"><p>بسوزد به فرقم کلاه غرور</p></div></div>