---
title: >-
    شمارهٔ  ۱۶ - در بیان تعریف و توصیف شاطر پسری
---
# شمارهٔ  ۱۶ - در بیان تعریف و توصیف شاطر پسری

<div class="b" id="bn1"><div class="m1"><p>نگار شاطری دارد رخم زرد</p></div>
<div class="m2"><p>نه شاطر بلکه خورشید جهانگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزاکت جلوه او را هم آغوش</p></div>
<div class="m2"><p>خرام قامتش از دل برد هوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبانش همچو آب زندگانی</p></div>
<div class="m2"><p>خضر زان یافت عمر جاودانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهال قامتش هنگام رفتار</p></div>
<div class="m2"><p>قیامت را کند از خواب بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدش در گلشن جانهاست سروی</p></div>
<div class="m2"><p>بود رفتار او همچون تذروی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به صحرا آهوان وقت رسیدن</p></div>
<div class="m2"><p>ازو گیرند سرمشق دویدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به ماه و مهر در هم پیشگی ها</p></div>
<div class="m2"><p>گرو بر دست در بالا دویها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ندارد ساعتی در یک زمین تاب</p></div>
<div class="m2"><p>سراپا در تحرک همچو سیماب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دونده چون خیال دوربینان</p></div>
<div class="m2"><p>پرنده همچو چشم نوغزالان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به عشوه آهوان را بنده کرده</p></div>
<div class="m2"><p>به جلوه سرو را شرمنده کرده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو دیده جلوه آن قد دلجو</p></div>
<div class="m2"><p>تذرو و باغ شهپر داده با او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگویم پر یکی پروانه دشت</p></div>
<div class="m2"><p>که سرگردان به گرد او همی گشت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همای آسا بهر جانب روان است</p></div>
<div class="m2"><p>پر طاوس او را سایه بان است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رسانده پر بدانجا پایه خویش</p></div>
<div class="m2"><p>کشیده سرو را در سایه خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کمر بسته چو کوه آن دشت پیما</p></div>
<div class="m2"><p>نهاده پای در دامان صحرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدنگ غمزه اش تا پر نشسته</p></div>
<div class="m2"><p>میان سینه پیکان زنگ بسته</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فغان زنگ او تا در میان است</p></div>
<div class="m2"><p>مرا افغان میان جان نهان است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو زنگ او لب پر خنده دارم</p></div>
<div class="m2"><p>دل خود را با فغان زنده دارم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هوای شهپر او حاصلم برد</p></div>
<div class="m2"><p>فغان زنگ او زنگ از دلم برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آن شاخ گل تا پر نهاده</p></div>
<div class="m2"><p>مرا سودای او بر سر فتاده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نهال قامت آن سرو مانند</p></div>
<div class="m2"><p>فرو رفته به گرداب کمربند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیا ای سیدا ختم سخن کن</p></div>
<div class="m2"><p>سخن با عندلیبان چمن کن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به یادش بعد ازین دلریش منشین</p></div>
<div class="m2"><p>کمربسته به خون خویش بنشین</p></div></div>