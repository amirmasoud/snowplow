---
title: >-
    شمارهٔ  ۴ - در صفت شهر بخارا گفته ملا سیدا
---
# شمارهٔ  ۴ - در صفت شهر بخارا گفته ملا سیدا

<div class="b" id="bn1"><div class="m1"><p>خوشا شهر بخار و خاک پاکش</p></div>
<div class="m2"><p>که مشک سوده می ریزد ز خاکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شهر مصر باشد یوسف آرا</p></div>
<div class="m2"><p>خرابش هفت کشور چون زلیخا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گل خوبانش از عصمت مزین</p></div>
<div class="m2"><p>چو بلبل عاشقانش پاکدامن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک از پاسبانان سرایش</p></div>
<div class="m2"><p>هلال از شبروان کوچهایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درو پیوسته درویشان بی خواب</p></div>
<div class="m2"><p>خمیده در رکوع حق چو محراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترازو دار دکان های او جور</p></div>
<div class="m2"><p>خریداران او سوداگر نور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود بیرون او معمور از باغ</p></div>
<div class="m2"><p>کند صحرای او فردوس را داغ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دورش قلعه دایره آثار</p></div>
<div class="m2"><p>منارش در میان یکپای پرگار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شریعت از بخارا یافت آئین</p></div>
<div class="m2"><p>منار او ستون خانه دین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرا و روز و شب بر کهکشان است</p></div>
<div class="m2"><p>توان گفتن ستون آسمان است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نهم گردون سر او را بود تاج</p></div>
<div class="m2"><p>رسانده پایه خود را به معراج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بلندی کار او جایی رسانده</p></div>
<div class="m2"><p>سر او را ز گردون بگذرانده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نگه خواهد که بالایش برآید</p></div>
<div class="m2"><p>نفس رفته ز ره برگشته آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به قامت نسبتی دارد باد هم</p></div>
<div class="m2"><p>از آن شد روشناس اهل عالم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهاده بر دهان طفل افلاک</p></div>
<div class="m2"><p>سر پستان خود را مادر خاک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زمین از خویش می افگند بیرون</p></div>
<div class="m2"><p>به پایش دسترس می داشت قارون</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کسی نبود درین شهر از بزرگان</p></div>
<div class="m2"><p>چو او در دیده مردم نمایان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قدم را در ره وحدت نهاده</p></div>
<div class="m2"><p>شب و روزش به یک پا ایستاده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کلاهی دوخته بر سر ز تمکین</p></div>
<div class="m2"><p>به گردش فوطه ای از رخت سنگین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به دستش گر بیفتد دزد بی باک</p></div>
<div class="m2"><p>خورد مغز سرش چون مار ضحاک</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا نبود به خود پیوسته مغرور</p></div>
<div class="m2"><p>که در چشمش نماید آدمی مور</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به بالایش نشیند هر که یکدم</p></div>
<div class="m2"><p>کند یکجا ستاده سیر عالم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به عیسی شد ز گردون آمدن فرض</p></div>
<div class="m2"><p>که پیدا شد به عالم دابة الارض</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به وحدانیت مهر رسالت</p></div>
<div class="m2"><p>نمایان کرده انگشت شهادت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مساجدهای او چون بیت معمور</p></div>
<div class="m2"><p>مؤذن خانهایش گنبد نور</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پر است از آب کوثر حوضهایش</p></div>
<div class="m2"><p>تماشا سبز و خرم از هوایش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خصوصا مسجد و حوض ندر بی</p></div>
<div class="m2"><p>که باشد اهل تقوی را مربی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چه مسجد، مسجد اقصا خرابش</p></div>
<div class="m2"><p>ملک قوم و مؤذن آفتابش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فلک رخ سوده بر خاک نیازش</p></div>
<div class="m2"><p>ردای کهکشان جای نمازش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمین اوست با طاعت سرشته</p></div>
<div class="m2"><p>درو چون بوریا بال فرشته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به محرابش امامت کرده آدم</p></div>
<div class="m2"><p>به خاکش معتکف عیسی مریم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>منور از سجودش جبهه خاک</p></div>
<div class="m2"><p>خمیده در رکوعش پشت افلاک</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز سنگ خانه کعبه اساسش</p></div>
<div class="m2"><p>قبای حاجیان رنگین پلاسش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به طاعت هر که آنجا سر نهاده</p></div>
<div class="m2"><p>دری بر خود ز هر جانب کشاده</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنای منبرش را عرش بسته</p></div>
<div class="m2"><p>خطیبش بر سر کرسی نشسته</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چراغ خفتن او مشعل ماه</p></div>
<div class="m2"><p>دلیل صبح او شمع سحرگاه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>منقش گنبدش چون بال طاووس</p></div>
<div class="m2"><p>درو گردون معلق همچو فانوس</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بود طاقش چو طاق آسمان جفت</p></div>
<div class="m2"><p>به معمارش توان صدآفرین گفت</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بود شیرین و دلکش در نظرها</p></div>
<div class="m2"><p>چو کوه بیستون گردیده بر پا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به زیر صفه او حوض سیراب</p></div>
<div class="m2"><p>عروسی در کنار او چو سیماب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چه حوض آبش مصفا همچو گوهر</p></div>
<div class="m2"><p>بود آمیخته با شیر و شکر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چنان صاف است آبش از سیاهی</p></div>
<div class="m2"><p>توان دیدن به پشت گاو ماهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>در آبش عکس اگر سازد قدم تر</p></div>
<div class="m2"><p>شود چون آدم آبی شناور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حبابش را فلک دارد نظاره</p></div>
<div class="m2"><p>در آب افتاده گردون را ستاره</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مدام آب حیات از رشک این آب</p></div>
<div class="m2"><p>به خود پیچیده می گردد چو گرداب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بود میراب آب جویش الماس</p></div>
<div class="m2"><p>ازین سودا خضر گردیده وسواس</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چه گوهر سنگهایش آبدار است</p></div>
<div class="m2"><p>چو آب تیغ آبش پایدار است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شد این حوض آبروی شهر و صحرا</p></div>
<div class="m2"><p>به جستجوی این حوض است دریا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>کند در دیده ها عمقش سیاهی</p></div>
<div class="m2"><p>در آبش غرق گشته گاو ماهی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خضر روزی که بیند ناودانش</p></div>
<div class="m2"><p>ز حسرت آب ریزد از دهانش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مصور در گلشن مرغی کند ساز</p></div>
<div class="m2"><p>درآید همچو مرغابی به پرواز</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به اطرافش ز سرسبزی درختان</p></div>
<div class="m2"><p>چو خضری بر کنار آب حیوان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از آن آبی که باشد همچو شکر</p></div>
<div class="m2"><p>کند سیراب تا فردای محشر</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>به بیرونش یکی قصریست عالی</p></div>
<div class="m2"><p>که مهمانخانه او نیست خالی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>چه قصر او را فلاطون کرده بنیاد</p></div>
<div class="m2"><p>اساس او ز حکمت می دهد یاد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به دیوارش مدارس روی بر روی</p></div>
<div class="m2"><p>زند بامش به کوی علم پهلوی</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>محشا خوانیش چون بیت معمور</p></div>
<div class="m2"><p>نوشته شرح او را خامه نور</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>در او تخته تعلیم و تدبیر</p></div>
<div class="m2"><p>به روی اوست خطی همچو زنجیر</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چو گردد تخته های در بهم وصل</p></div>
<div class="m2"><p>نماید پشتبان با سری فصل</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چه در منقوش همچون برگ لاله</p></div>
<div class="m2"><p>کشاده همچو اوراق رساله</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>چنین منزل که در عالم هویداست</p></div>
<div class="m2"><p>مقام حضرت بهرام داناست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>چه حضرت قبله ارباب حاجات</p></div>
<div class="m2"><p>چه حضرت صاحب کشف و کرامات</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>حدیث او به بزمی مرهم جان</p></div>
<div class="m2"><p>به پیشش بوعلی باشد شفاخوان</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>بود در حلقه درسش فلاطون</p></div>
<div class="m2"><p>کند در پرده اش آهنگ قانون</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>رموز عقل اول تا به عاشر</p></div>
<div class="m2"><p>ز ده انگشت او گردیده ظاهر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>زبان خامه اش شمع شبستان</p></div>
<div class="m2"><p>خطش شیرازه جزو گلستان</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>دوات سرخی او غنچه گل</p></div>
<div class="m2"><p>قلم قطعش بود منقار بلبل</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز نظم او به خود بالیده دیوان</p></div>
<div class="m2"><p>ز دیوانش پریرویان غزلخوان</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>دلش کوه است اما کان حلم است</p></div>
<div class="m2"><p>اگر دریا بود دریای علم است</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>لبش بسته لب مرغ چمن را</p></div>
<div class="m2"><p>گرفته در نگین تخت سخن را</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>نباشد حاجت او را با رسایل</p></div>
<div class="m2"><p>به روی ناخنش حل مسایل</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>ز قرآن خواندش حفاظ محفوظ</p></div>
<div class="m2"><p>نهان در سینه او لوح محفوظ</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>کلامش گر کند تفسیر اظهار</p></div>
<div class="m2"><p>مفسرخوان شود چون نقش دیوار</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>خدایا ذات این پاکیزه گوهر</p></div>
<div class="m2"><p>بود تا روز محشر تازه و تر</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>بیا ساقی دم صبح است برخیز</p></div>
<div class="m2"><p>می معنی به جام معرفت ریز</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به من ده در سخن تا در نمانم</p></div>
<div class="m2"><p>شود امروز مفتاح الجنانم</p></div></div>