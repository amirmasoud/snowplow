---
title: >-
    شمارهٔ  ۱۰ - مثنوی از برای گرمای تابستان گفته
---
# شمارهٔ  ۱۰ - مثنوی از برای گرمای تابستان گفته

<div class="b" id="bn1"><div class="m1"><p>دم صبح کز تابش آفتاب</p></div>
<div class="m2"><p>جهان گشت همچون تنور کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا سوخت از گرمی مرز و بوم</p></div>
<div class="m2"><p>نسیم صبا گشت باد سموم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد از مرکز خاک آتش بلند</p></div>
<div class="m2"><p>پرید از زمین سایه همچون سپند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان طبع کافور گردید گرم</p></div>
<div class="m2"><p>که شد پشت آئینه چون موم نرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد امروز پروانه مرغ تذرو</p></div>
<div class="m2"><p>گریزان شد از سایه خویش سرو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمین را فتاد آتش اندر نهاد</p></div>
<div class="m2"><p>سوی آسمان رفت چون گرد باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جان گلخنی آتش افروختی</p></div>
<div class="m2"><p>ز گلخن برون آمدی سوختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فتاد آشپز را دکان از رواج</p></div>
<div class="m2"><p>ندارد به آتش طعام احتیاج</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد از باغ مرغ چمن عذرخواه</p></div>
<div class="m2"><p>برد چون سمندر به آتش پناه</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز گرمی به بتخانه آمد شکست</p></div>
<div class="m2"><p>ز آتش گریزان شد آتش پرست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کمان شد ز تاب هوا گوشه گیر</p></div>
<div class="m2"><p>به بحر کمان غوطه زد چوب تیر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به هر خانه ای شمع افروخته</p></div>
<div class="m2"><p>ز پروانه شد بیشتر سوخته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شد آب از حرارت ورق های گل</p></div>
<div class="m2"><p>به جوها روان شد عرقهای گل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پریدی اگر مرغ بر آفتاب</p></div>
<div class="m2"><p>چو پروانه گشتی هماندم کباب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدنها شد از تاب گرما تنگ</p></div>
<div class="m2"><p>چو بیمار جویای خوی خنک</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مزاج همه شد ز گرمی به جوش</p></div>
<div class="m2"><p>نشست خجل گرم دارو فروش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به خوناب شد شسته روی شفق</p></div>
<div class="m2"><p>ز تاب هوا کرد آتش عرق</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه خلق در آرزوی پناه</p></div>
<div class="m2"><p>گریزان به درگاه ظل اله</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو ظل اله شاه سبحانقلی</p></div>
<div class="m2"><p>شه آئینه دهر ازو منجلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من امروز از تابش آفتاب</p></div>
<div class="m2"><p>دویدم سوی شاه عالیجناب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بلند است همچون فلک پایه اش</p></div>
<div class="m2"><p>پناه غریبان بود سایه اش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نهال حیات شد کامران</p></div>
<div class="m2"><p>بود ایمن از گرم و سرد جهان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خزان از گلستان او دور باد</p></div>
<div class="m2"><p>حسودش همه عمر رنجور باد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به عهدش جوان گشت دوران پیر</p></div>
<div class="m2"><p>در ایام او فتنه شد گوشه گیر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عزیز است در دیده روزگار</p></div>
<div class="m2"><p>ز شاهان پیشین بود یادگار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز حکمش به صحرا شبان ارجمند</p></div>
<div class="m2"><p>زند گرگ را پیش پا گوسفند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز پابوس او گرم سودای بخت</p></div>
<div class="m2"><p>بخارا شد از مقدمش پایتخت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نویسند اگر نام او بر کمان</p></div>
<div class="m2"><p>کشد در بغل تیر کج را نشان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بیا ساقی آن باده دلربا</p></div>
<div class="m2"><p>که موجش بود قبله گاه دعا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به من ده که او را به ساغر کنم</p></div>
<div class="m2"><p>دعای شه هفت کشور کنم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الهی زمین تا بود برقرار</p></div>
<div class="m2"><p>بود قصر اقبال شه پایدار</p></div></div>