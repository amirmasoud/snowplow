---
title: >-
    شمارهٔ  ۸ - مثنوی درباره قحطی در سمرقند و بخارا  از برای قیمتی نظم کرده
---
# شمارهٔ  ۸ - مثنوی درباره قحطی در سمرقند و بخارا  از برای قیمتی نظم کرده

<div class="b" id="bn1"><div class="m1"><p>دم صبح دهقان زرینه طشت</p></div>
<div class="m2"><p>برآمد پی آبداری به دشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز تاب نفس دهر را برفروخت</p></div>
<div class="m2"><p>سراپای کشت جهان را بسوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیان شد به یک بار قحط از جهان</p></div>
<div class="m2"><p>چو مه نرخ نان رفت بر آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گیاهی نماندش به روی زمین</p></div>
<div class="m2"><p>به یکدانه گندم دو صد خوشه چین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک گشت از بار گردش سبک</p></div>
<div class="m2"><p>شد ایلک ز بی آردی ها تنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دوران به گندم رسیدش شکست</p></div>
<div class="m2"><p>ز غم بر شکم آسیا سنگ بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز امساک پرویزن آبنوس</p></div>
<div class="m2"><p>شده قرص خورشید نان سبوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کف تخته چوبی پلاسی به دوش</p></div>
<div class="m2"><p>به درها گدایی کنان نان فروش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دیگ هوس آب شد پیه و جز</p></div>
<div class="m2"><p>به گلخن شده پیش کار آشپز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکم ها به فریاد مانند سنج</p></div>
<div class="m2"><p>ز غم ریش ها گشت ماش و گرنج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فراموش شد خوردنی از میان</p></div>
<div class="m2"><p>نمی برد کس نام دستار خوان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بی قوتی شد کمان گوشه گیر</p></div>
<div class="m2"><p>به دریوزه سوی نشان رفت تیر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنان جمع گشتند گرد تنور</p></div>
<div class="m2"><p>که نان کرده روزی از اینجا عبور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تنوری که بودی درو قوت روح</p></div>
<div class="m2"><p>بگردید گرداب طوفان نوح</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به بالای دیگ ایستاده سپاه</p></div>
<div class="m2"><p>به کفگیرها می کشیدند آه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نهادند در پهلوی میهمان</p></div>
<div class="m2"><p>به جای تنک سفره شمع دان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زنان گشت دستار خوان ها تهی</p></div>
<div class="m2"><p>فقیران خراب از غم فربهی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز هم ساختند اقربایان نفور</p></div>
<div class="m2"><p>نشستند از یکدیگر دور دور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در کوی کردند خلق استوار</p></div>
<div class="m2"><p>ببستند همچون لب روزه دار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به یاد لب نان گندم گدا</p></div>
<div class="m2"><p>زدی سنگ بر سینه چون آسیا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>میسر نشد دیدن روی نان</p></div>
<div class="m2"><p>بسی خلق نان گفته دادند جان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لبالب شد از مرده بازار و کوی</p></div>
<div class="m2"><p>جهان پاک گردید از مرده شوی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بخارا تل خواجه اسحاق شد</p></div>
<div class="m2"><p>سمرقند یک کوچه قاق شد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز غم گشت پشت جوانان دو تا</p></div>
<div class="m2"><p>ز سودای نان پیر شد اشتها</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دهن هر که جنبانید بیرون ز کو</p></div>
<div class="m2"><p>گدایان گرفتند راه گلو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ملاحت ربود از جوانان فلک</p></div>
<div class="m2"><p>ز روی زمین رفت آب و نمک</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کنی گر خمیر از سبوس درشت</p></div>
<div class="m2"><p>به بالای او می شود جنگ مشت</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برون رفت سیری ز خورد و کلان</p></div>
<div class="m2"><p>به مور و ملخ شد برابر دهان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شد از آرد غربال ضیق النفس</p></div>
<div class="m2"><p>به ایلک نشد حاجت هیچ کس</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زن از شوهر خویش جستی نفاق</p></div>
<div class="m2"><p>به دستش همان لحظه دادی طلاق</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به دختر نمی کرد مادر نظر</p></div>
<div class="m2"><p>ز فرزند بیزار گشته پدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهان گشت در چشم مردم سیاه</p></div>
<div class="m2"><p>برون آمد از خانها دود آه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>برآمد فغان از طبق ها چو سنج</p></div>
<div class="m2"><p>ورم کرد چون کوس دیگ گرنج</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان ماش پنهان شد از دیده ها</p></div>
<div class="m2"><p>شدند اهل عالم همه ماشبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>رود گر ز کشک جواری سخن</p></div>
<div class="m2"><p>شود آب دندان به دیگ دهن</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دوکان قصاب آمد گزند</p></div>
<div class="m2"><p>خجل شد کبابی ز سیخ بلند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز پروانها رفت دلهای جمع</p></div>
<div class="m2"><p>ز بی روغنی آب شد مغز شمع</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پی دانه صیاد شد سینه چاک</p></div>
<div class="m2"><p>غم دام را برد آخر به خاک</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز بی قوتی اسپها زیر پا</p></div>
<div class="m2"><p>نجنبند چون اسب چوبین ز جا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز فکر شتر ساربان شد خراب</p></div>
<div class="m2"><p>شب و روز کنجاره بیند به خواب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز بس صرف شد عمر مردم بدو</p></div>
<div class="m2"><p>نشد حاصل هیچ کس نیم جو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بنان شد گرو جامه مرد و زن</p></div>
<div class="m2"><p>زمین پر شد از مرده بی کفن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جهان آنچنان گشت بی آب و تاب</p></div>
<div class="m2"><p>که شد خانه دین مردم خراب</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از این محنت آنها که بیرون شدند</p></div>
<div class="m2"><p>یقین دان که از مادر اکنون شدند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بیا ساقی آن شربت دلپذیر</p></div>
<div class="m2"><p>که از دیدن او شود چشم سیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>به من ده که نعمت فزاید مرا</p></div>
<div class="m2"><p>در رزق و روزی کشاید مرا</p></div></div>