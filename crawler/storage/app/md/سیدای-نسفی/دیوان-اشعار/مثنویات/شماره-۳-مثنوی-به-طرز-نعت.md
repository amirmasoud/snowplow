---
title: >-
    شمارهٔ  ۳ - مثنوی به طرز نعت
---
# شمارهٔ  ۳ - مثنوی به طرز نعت

<div class="b" id="bn1"><div class="m1"><p>دم صبح آفتاب عالم آرا</p></div>
<div class="m2"><p>کشاد از هر طرف بهر تماشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دماغم گشت خالی از بخارات</p></div>
<div class="m2"><p>نهادم روی بر طوف مزارات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چشمم روضه ای بنمود از دور</p></div>
<div class="m2"><p>به شاه نقشبند او بود مشهور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو روضه روضه ای همچون مدینه</p></div>
<div class="m2"><p>بود صندوقه اش صندوق سینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گردش ز ایران پاک دامن</p></div>
<div class="m2"><p>رداها فوطه زاری به گردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیمبر روح او فرزند خوانده</p></div>
<div class="m2"><p>به عهد او را به قطبیت نشانده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کراماتش عیان چون صبح صادق</p></div>
<div class="m2"><p>اجابت منتظر چون چشم عاشق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلش آگه ز اسرار حقیقت</p></div>
<div class="m2"><p>شریعت جمع کرده با طریقت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر گردن کشان سنگ نشانش</p></div>
<div class="m2"><p>کف شاهان گدای آستانش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گریبان چاک نذر او زره ها</p></div>
<div class="m2"><p>نیاز دست مفتاحش گره ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو چوگان سایلانش سرفرازان</p></div>
<div class="m2"><p>گدایانش ز دنیا بی نیازان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو شمع صبح پیرانش سحرخیز</p></div>
<div class="m2"><p>چو شبنم ذاکران او عرق ریز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز خاک او منور دیده و دل</p></div>
<div class="m2"><p>در او روز و شب محتاج سایل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه در یعنی مبارک آستانش</p></div>
<div class="m2"><p>در فردوس باشد پشتبانش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>منقش چون سپهر پرکواکب</p></div>
<div class="m2"><p>عصای کهکشانش چوب حاجب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بود زنجیر او سر حلقه نور</p></div>
<div class="m2"><p>چو زلف افتاده بر رخساره حور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز طاق اوست مقصدها نمایان</p></div>
<div class="m2"><p>ز نام اوست مشکل ها گریزان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فلک تا گرددش مشهور آفاق</p></div>
<div class="m2"><p>نشسته بر رواقش سینه بر طاق</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>خم طاق رواقش بسته از مو</p></div>
<div class="m2"><p>نمایان چون جوان چار ابرو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اسیر طاق او بالا بلندان</p></div>
<div class="m2"><p>کمند مارپیچش زلف جانان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به روی صفه اش پیران رهبر</p></div>
<div class="m2"><p>نشسته همچو اصحاب پیمبر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>فلک بر آستان او مجاور</p></div>
<div class="m2"><p>ملک در زیر ایوانش مسافر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه ایوان آسمان محو درونش</p></div>
<div class="m2"><p>ز کوه طور سنگ پا ستونش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو ابروی بتان محراب او طاق</p></div>
<div class="m2"><p>به سقف او بود نظاره مشتاق</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به سوی روضه باشد رو کشاده</p></div>
<div class="m2"><p>ز حیرت پشت خود بر قبله داده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>امام او بود معصوم چون گل</p></div>
<div class="m2"><p>مؤذن را مرید آواز بلبل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صف محشر بود قوم امامش</p></div>
<div class="m2"><p>ملک او را دخان بر پشت بامش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گلیم او حجاب چهره حور</p></div>
<div class="m2"><p>بود جای نمازش پرده نور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چراغش را فتیله زلف سنبل</p></div>
<div class="m2"><p>دماغش چرب و نرم از روغن گل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فلک سیلی خور باد و هوایش</p></div>
<div class="m2"><p>زمین پامال نقش بوریایش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کبوترهای او هر گه پریده</p></div>
<div class="m2"><p>نشسته چون نگه بر بام دیده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گره از بال ایشان چنگل باز</p></div>
<div class="m2"><p>چو مرغ روح اهل دل به پرواز</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گرفته هر یکی بر یک ستون خو</p></div>
<div class="m2"><p>چو بر بالای شاخ سرو حق گو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شوند ایشان بهر جا سایه افگن</p></div>
<div class="m2"><p>شمان سازند خون خویش روشن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>همازین غم طلبگار قفس باف</p></div>
<div class="m2"><p>کباب خون چکان سیمرغ در قاف</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز حوض او گهر سیراب گشته</p></div>
<div class="m2"><p>دل دریا ز حسرت آب گشته</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو حوض افلاک او را گشته سرپوش</p></div>
<div class="m2"><p>ز آبش آب رحمت می زند جوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نمایان چشمه حیوان سرابش</p></div>
<div class="m2"><p>خضر از تشنگی بی آب و تابش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>لب خود هر که از آبش کند تر</p></div>
<div class="m2"><p>نگردد تشنه در صحرای محشر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>وضو هر کس که می سازد ازین آب</p></div>
<div class="m2"><p>شود روز قیامت مغفرت یاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز آبش آبرو باشد زمین را</p></div>
<div class="m2"><p>ز خاکش سرفرازی مشک چین را</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به گرد او درختان حی و قایم</p></div>
<div class="m2"><p>به ذکر اره مشغولند دایم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>درختان در سراندازی چو شیخان</p></div>
<div class="m2"><p>زبانها سبز در تسبیح سبحان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو گردون برگهایش پهن دامن</p></div>
<div class="m2"><p>به عالم همچو طوبی سایه افگن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گذر کرده ز گردون شاخهایش</p></div>
<div class="m2"><p>درخت سدره باشد منتهایش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ید بیضا بود لوح مزارش</p></div>
<div class="m2"><p>گل خورشید دامنگیر خارش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز قندیلش نمایان شعله طور</p></div>
<div class="m2"><p>بود چوگان او فواره نور</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شده از پرتو او روشن افلاک</p></div>
<div class="m2"><p>چنین شمعی که دیده بر سر خاک</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>علم گردیده از خاکش سرافراز</p></div>
<div class="m2"><p>هما از سایه او کرده پرواز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گل سرخ از مزارش بوی برده</p></div>
<div class="m2"><p>ز خاکش برگ رعنا آب خورده</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>سر کروبیان جاروب راهش</p></div>
<div class="m2"><p>چراغ طور شمع خانقاهش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نباشد گنبد او را مثالی</p></div>
<div class="m2"><p>فلک در وی چو فانوس خیالی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به صحن او سراسر نانوایان</p></div>
<div class="m2"><p>دکان بکشاده بهر بینوایان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چه نان همچون شفق رخساره گلگون</p></div>
<div class="m2"><p>تنور چرخ او را کرده بیرون</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>سفید و گرم همچون قرص خورشید</p></div>
<div class="m2"><p>به دیدن سیر گردد چشم امید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>ز یکسو در نوا حلوافروشان</p></div>
<div class="m2"><p>چو طوطی در کنار شکرستان</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>چو حلوا جان شیرین عشقبازش</p></div>
<div class="m2"><p>خریداران چو محمود و ایازش</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز دیگر سوی بقالان دکانها</p></div>
<div class="m2"><p>مزین کرده بهر میهمانها</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چه دکان تخته هایش ماه پاره</p></div>
<div class="m2"><p>تبنگش آسمان نقش ستاره</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>نماید در نظر از جوش مردم</p></div>
<div class="m2"><p>زمین او سپهر پر ز انجم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کنند از روی خود هر شب جوانان</p></div>
<div class="m2"><p>چراغان از برای روح پیران</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>بیا ساقی ازین درگاه عالی</p></div>
<div class="m2"><p>مگردان سیدا را دست خالی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز جای خود صراحی وار برخیز</p></div>
<div class="m2"><p>می توفیق در مینای او ریز</p></div></div>