---
title: >-
    شمارهٔ  ۱ - مثنویات به طرز مناجات
---
# شمارهٔ  ۱ - مثنویات به طرز مناجات

<div class="b" id="bn1"><div class="m1"><p>خداوندا بکن روشن دلم را</p></div>
<div class="m2"><p>برآر از تیره گی آب و گلم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر است از گرد کلفت سینه من</p></div>
<div class="m2"><p>به زنگار آشنا آئینه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناهی کرده ام اندیشه ناکم</p></div>
<div class="m2"><p>کمر بستست سودا بر هلاکم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی گشتم به ساقی همپیاله</p></div>
<div class="m2"><p>تلف شد طاعت هفتاد ساله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموشم غنچه وار از شرمساری</p></div>
<div class="m2"><p>سرم در جیب گردیده حصاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خداوندا خطایی سر زد از من</p></div>
<div class="m2"><p>ز نادانی زدم آتش به خرمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ضمیرم از گنهکاری هراسان</p></div>
<div class="m2"><p>قدم گردیده خم از بار عصیان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سری در جیب دارم از خجالت</p></div>
<div class="m2"><p>ز چشم رفته بیرون خواب راحت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تصورها مرا کردت نسناس</p></div>
<div class="m2"><p>نهاده بر در دل قفل وسواس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از این اندیشه یارب بی قرارم</p></div>
<div class="m2"><p>مگردان ای کریما شرمسارم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو آتش می زند شهوت زبانه</p></div>
<div class="m2"><p>گرفته نفس شیطان در میانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندارم چاره ای غیر از تو یارب</p></div>
<div class="m2"><p>نه در روز است آرامم نه در شب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فدای آستانت کرده ام سر</p></div>
<div class="m2"><p>نرفته هیچ کس محروم از این در</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گنه کارم تو غفارالذنوبی</p></div>
<div class="m2"><p>همه عیبم تو ستارالعیوبی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیم نومید از لطفت رحیمی</p></div>
<div class="m2"><p>نهادم رو به درگاهت کریمی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو را پهن است دایم خون احسان</p></div>
<div class="m2"><p>گدایان راست امید از کریمان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر با من نمی سازی عنایت</p></div>
<div class="m2"><p>ز دستم می رود دامان عصمت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تو را دریای رحمت می زند موج</p></div>
<div class="m2"><p>گنه کاران ز هر سو فوج در فوج</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دلم در لرزه همچون شعله بر تن</p></div>
<div class="m2"><p>ترحم گر نسازی وای بر من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زبانم را ز ناشکری نگه دار</p></div>
<div class="m2"><p>دهانم را به بد گفتن مکن یار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چراغ انجمن کن نامه ام را</p></div>
<div class="m2"><p>مده کوته زبانی خامه ام را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گلستانی ده از گلهای بی خار</p></div>
<div class="m2"><p>ز محنت های دورانم نگه دار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ندارم جز تو در عالم پناهی</p></div>
<div class="m2"><p>به غیر از آستانت تکیه گاهی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مرا دایم به عصیانست کوشش</p></div>
<div class="m2"><p>ز من جرم پیاپی از تو بخشش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گناهم بارها بخشیده یی تو</p></div>
<div class="m2"><p>به دامان کرم پوشیده یی تو</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>زبانم روز و شب در توبه کردن</p></div>
<div class="m2"><p>ولی در دل تمنای شکستن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز مینا توبه ها کردم شکستم</p></div>
<div class="m2"><p>کشد شرمندگی ساغر ز دستم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بکن از لطف یارب دستگیری</p></div>
<div class="m2"><p>جوانی را رسانیدی به پیری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بده از آب رحمت شست و شویم</p></div>
<div class="m2"><p>مکن رو جزا بی آبرویم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نگه دار از حوادث های ایام</p></div>
<div class="m2"><p>که تا از عفو تو گردم نکونام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر لطفت مرا گردد حمایت</p></div>
<div class="m2"><p>نیابد در دل من راه غفلت</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به غفلت رفت ایام جوانی</p></div>
<div class="m2"><p>شده پیدا در اعضا ناتوانی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پشیمانم ز کردار بد خویش</p></div>
<div class="m2"><p>هراسانم ز جرم بی حد خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>برآوردم ز خاطر یاد عصیان</p></div>
<div class="m2"><p>به درگاه تو کردم عهد و پیمان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نگه دار از شبیخون ملامت</p></div>
<div class="m2"><p>سلامت دار تا روز قیامت</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مرا روزی که آید سر به دیوار</p></div>
<div class="m2"><p>دلم از غارت شیطان نگه دار</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بهار رنگ و بوی من خزان شد</p></div>
<div class="m2"><p>گل امیدواری زعفران شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به بال سعی روی آورد سستی</p></div>
<div class="m2"><p>پرید از سر هوای تندرستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نهالم شد درخت سالخورده</p></div>
<div class="m2"><p>ز سر تا پای گردیده فسرده</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز صحبت ها هوس باشد گریزان</p></div>
<div class="m2"><p>گریزان جانب خلوت نشینان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تماشای چمن رفتست از یاد</p></div>
<div class="m2"><p>نظر پوشیده چشم از سرو شمشاد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>لبالب کن ز بوی بی وفایی</p></div>
<div class="m2"><p>نمانده با نسیم آشنایی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بیا ساقی که امشب در خمارم</p></div>
<div class="m2"><p>قدح را پر ز می کن خاکسارم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ندارم طاقت اندوه دیگر</p></div>
<div class="m2"><p>بکن لطف و به گردش آر ساغر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بده تا من شوم مست و توانا</p></div>
<div class="m2"><p>جوانی روی آرد چون زلیخا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از آن می تا شوم چون غنچه خاموش</p></div>
<div class="m2"><p>کنم چون غنچه عالم را فراموش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>الهی محو کن از دل گناهم</p></div>
<div class="m2"><p>بده در سایه عصمت پناهم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>توانایی در اعضایم کرم کن</p></div>
<div class="m2"><p>به سرسبزی عصایم را علم کن</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شوم در دیده ها چون سرو ممتاز</p></div>
<div class="m2"><p>به گلزار جهان گردم سرافراز</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>بده یارب به طاعت استقامت</p></div>
<div class="m2"><p>که سازم عمر باقی صرف طاعت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>مکن بیرون ز خاطر سیدا را</p></div>
<div class="m2"><p>به گل باشد سری باد صبا را</p></div></div>