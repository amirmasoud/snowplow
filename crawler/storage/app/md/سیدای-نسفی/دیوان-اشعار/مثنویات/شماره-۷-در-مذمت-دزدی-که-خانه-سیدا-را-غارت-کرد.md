---
title: >-
    شمارهٔ  ۷ - در مذمت دزدی که خانه سیدا را غارت کرد
---
# شمارهٔ  ۷ - در مذمت دزدی که خانه سیدا را غارت کرد

<div class="b" id="bn1"><div class="m1"><p>شبی از مقتضای آسمانی</p></div>
<div class="m2"><p>دلم کرد آرزوی کامرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا بود آشنای عیش پرور</p></div>
<div class="m2"><p>مهیا داشت دایم شیر و شکر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه یار از پای تا سر مهربانی</p></div>
<div class="m2"><p>جبینش بوسه گاه شادمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محبت در نهاد او سرشته</p></div>
<div class="m2"><p>دلش پاکیزه چون طبع فرشته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نقاشی علم چون کلک مانی</p></div>
<div class="m2"><p>روان دستش چو آب زندگانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز تصویر گل او لاله مضطر</p></div>
<div class="m2"><p>شفق از رنگ شنگرفش به خون تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوات غنچه او جوشه گل</p></div>
<div class="m2"><p>قلمدانش بود منقار بلبل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو روی برگ گل پرداز سازد</p></div>
<div class="m2"><p>چو بلبل کلک مو پرواز سازد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گل گر صورت آدم نگارد</p></div>
<div class="m2"><p>به صورت خانه او جان درآرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برای امتحان گردد چو سرکش</p></div>
<div class="m2"><p>کشد بر صورت تصویر برگش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز قوت خامه مانی بماند</p></div>
<div class="m2"><p>کمانش را کشیدن کی تواند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به تحریرش سیاهی داغ لاله</p></div>
<div class="m2"><p>نهاده پیش او رنگین پیاله</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گل تصویر او پیوسته خندد</p></div>
<div class="m2"><p>بدین صورت کسی صورت نبندد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به هر صورت کشد او چشم و ابرو</p></div>
<div class="m2"><p>توان عاشق شدن بر صورت او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گره از پیچک او ناف آهو</p></div>
<div class="m2"><p>گلش چون غنچه گل می دهد بو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کشد گر صورت آهوی مشکین</p></div>
<div class="m2"><p>به بویش کاروان آیند از چین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به مهمان کرده بود او خانه بنیاد</p></div>
<div class="m2"><p>به طرح او قلم انگشت بهزاد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرنگی بر زمینش ریخته رنگ</p></div>
<div class="m2"><p>به گردش چیده از لعل بتان سنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چه خانه صورت او نقش شیرین</p></div>
<div class="m2"><p>خرابش خانه صورتگر چین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود آراسته چون بیت معمور</p></div>
<div class="m2"><p>چراغش را فتیله شمع کافور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خورد آب از زمینش سبزه باغ</p></div>
<div class="m2"><p>ز دیوارش گلستان ارم داغ</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چراغ او کند در هر نفس گل</p></div>
<div class="m2"><p>کشد دود چراغش نقش سنبل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>درش دایم برای میهمانان</p></div>
<div class="m2"><p>کشاده چون کف دست کریمان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قماش بستر او مخمل و گل</p></div>
<div class="m2"><p>به بالینش پر قو بال بلبل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز مأوای خود آن شب پافشاندم</p></div>
<div class="m2"><p>در آن منزل شب خود بگذراندم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو شب صبحدم او عید اقبال</p></div>
<div class="m2"><p>مبارک روز او چون اول سال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو صبح از جامه خواب ناز برخاست</p></div>
<div class="m2"><p>جهان را باز زو زیور بیاراست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وداعش کردم و رفتم به خانه</p></div>
<div class="m2"><p>توجه زد قدم بر آستانه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز جا برخاست آن فرخنده اختر</p></div>
<div class="m2"><p>چون گل جیب مرا پر کرده از زر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گلاب خرمی زد از دلم جوش</p></div>
<div class="m2"><p>کشیدم شادمانی در آغوش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قدم سوی مکان خود کشیدم</p></div>
<div class="m2"><p>به چندین عیش و خوشوقتی رسیدم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دری دیدم به ناکامی کشاده</p></div>
<div class="m2"><p>به راهم دیده واء ایستاده</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>درون خانه آن دم پانهادم</p></div>
<div class="m2"><p>ز حیرت پشت بر دیوار دادم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نظر را چون به یکجا جمع کردم</p></div>
<div class="m2"><p>منور دیده را چون شمع کردم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیامد پیش چشمم هیچ اسباب</p></div>
<div class="m2"><p>شدم از بی قراری همچو سیماب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به خود گفتم که بالین سرم کو</p></div>
<div class="m2"><p>به پهلو از پر قو بسترم کو</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نمد از زیر پای من که برده</p></div>
<div class="m2"><p>متاع خانه ام چشم که خورده</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به روی خانه خود دوختم چشم</p></div>
<div class="m2"><p>نمانده در بساط خانه ام پشم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فغان ناگه ز قفل در برآمد</p></div>
<div class="m2"><p>که امشب طرفه دزدی بر سر آمد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قوی چنگال گرگی تیز خشمی</p></div>
<div class="m2"><p>به بازو فیل زور و تنگ چشمی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پریده از جبینش نور اسلام</p></div>
<div class="m2"><p>ز کار او شده ابلیس بدنام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز بندی خانه ایام جسته</p></div>
<div class="m2"><p>در ناموس را صفها شکسته</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>حسد می زد درون سینه اش جوش</p></div>
<div class="m2"><p>لبش همچون لب دیوار خاموش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ز بیلش ناخن و از تیشه دندان</p></div>
<div class="m2"><p>فگنده رخنه ها در چاه زندان</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شده از گردنش زنجیر دلگیر</p></div>
<div class="m2"><p>به پای او نکرده کنده تأثیر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کشند از دست او زندانیان بنگ</p></div>
<div class="m2"><p>بود پیوسته زندانیان ازو تنگ</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عسس کردست او را بارها بند</p></div>
<div class="m2"><p>نمک را بارها خوردست چون قند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به پیش شبروان کردند نالان</p></div>
<div class="m2"><p>ز دستش ریسمان شانه گردان</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>سر او را ز دار اندیشه ای نی</p></div>
<div class="m2"><p>به غیر از دزدی او را پیشه ای نی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>قدی دارد برابر با لب بام</p></div>
<div class="m2"><p>نهان در سایه او سایه شام</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>گرسنه چشم چون چشم گدایان</p></div>
<div class="m2"><p>لبش افتاده از یاد لب نان</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به هر قفلی که انگشتش رسیدی</p></div>
<div class="m2"><p>شدی آن قفل را پیدا کلیدی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به دزدی هر کجا او پا نهاده</p></div>
<div class="m2"><p>ز بیم او شده آن در کشاده</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ولی با این همه اوصاف خس دزد</p></div>
<div class="m2"><p>برد از زیر سر مزدور را مزد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>من آنجا تا سحر در خواب راحت</p></div>
<div class="m2"><p>کشاده دزد اینجا دست غارت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>من آنجا در سخن سرگرم چون شمع</p></div>
<div class="m2"><p>نشسته دزد اینجا با دل جمع</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درون خانه ام جز بوریا نی</p></div>
<div class="m2"><p>به زیر پای غیر از نقش پا نی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بود پیه سوز من از پیه خالی</p></div>
<div class="m2"><p>فتاده بر سرش یاد کلالی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>چراغم گشته سرگردان چو گرداب</p></div>
<div class="m2"><p>دهد جان از برای روغن آب</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>به روی سینه منقل می زند خشت</p></div>
<div class="m2"><p>نشسته سر گران از فکر انگشت</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>اگر آتش کنم در غمخانه روشن</p></div>
<div class="m2"><p>ز دودش کور گردد چشم روزن</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز بی اسبابیم در سینه چاک است</p></div>
<div class="m2"><p>متاع خانه من آب و خاک است</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>چه گویم من به روی خانه خود</p></div>
<div class="m2"><p>شوم شرمنده از کاشانه خود</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>شدش تاراج او کنجی فراموش</p></div>
<div class="m2"><p>ز دست دزد گشتم خانه بر دوش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تو را باد ای فلک دوران مسلم</p></div>
<div class="m2"><p>به یک شادی کنی آماده صد غم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>چو این غوغای من یاران شنیدند</p></div>
<div class="m2"><p>به دل پرسی مرا یک یک رسیدند</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>یکی گفتا چو زینجا پافشردی</p></div>
<div class="m2"><p>چرا این خانه را همره نبردی</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>یکی گفتا چو دل در رفتنت بود</p></div>
<div class="m2"><p>در این خانه می کردی گل اندود</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>یکی گفتا اگر می داشتی پاس</p></div>
<div class="m2"><p>چرا بر در نکردی قفل وسواس</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یکی می گفت ای فرخنده همدم</p></div>
<div class="m2"><p>چرا بر در نگفتی باش محکم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>همان به دست از این و آن بشویم</p></div>
<div class="m2"><p>سخن از سرگذشت خود نگویم</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>خدایا داد من زین دزد بستان</p></div>
<div class="m2"><p>تن او را اسیر بند گردان</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>مده دیگر ازین بندش خلاصی</p></div>
<div class="m2"><p>که تا جانش برآید در معاصی</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>بیا ای سیدا از جور گردون</p></div>
<div class="m2"><p>دل خود را مکن چون غنچه پر خون</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>به کنج خانه خود چار زانو</p></div>
<div class="m2"><p>نشین از جاده ساز خود عوض جو</p></div></div>