---
title: >-
    شمارهٔ  ۲۷ - ایلچی فرستادن خان فردوس مکان یوسف خواجه نقشبندی را جواب مقرر نایافتن دویم قلیچ بی را فرستاده و ابواب متفصل مفتوح شدن و داخل به شهر بلخ و مغضوب شدن
---
# شمارهٔ  ۲۷ - ایلچی فرستادن خان فردوس مکان یوسف خواجه نقشبندی را جواب مقرر نایافتن دویم قلیچ بی را فرستاده و ابواب متفصل مفتوح شدن و داخل به شهر بلخ و مغضوب شدن

<div class="b" id="bn1"><div class="m1"><p>دم صبح کاین خسرو قلعه گیر</p></div>
<div class="m2"><p>برآراست بزم از صغیر و کبیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شد ملک ایران و توران ازو</p></div>
<div class="m2"><p>سراسیمه هندو صفاهان ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتادند خصمان او از نظر</p></div>
<div class="m2"><p>حسودان او کور گشتند و کر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حکایت ز هر جانب آغاز کرد</p></div>
<div class="m2"><p>در مشورت جمله را باز کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که او هوشمندان با نام و ننگ</p></div>
<div class="m2"><p>ره صلح جوئیم یا راه جنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوزش که این آتش فتنه باز</p></div>
<div class="m2"><p>نکردست از سنگ بیرون گداز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فشانیم آب سیاست درو</p></div>
<div class="m2"><p>ببندیم با او در آرزو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبان را کشادند خورد و کلان</p></div>
<div class="m2"><p>که ای شهریار شجاعت نشان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به این قوم اول مدارا کنیم</p></div>
<div class="m2"><p>نشد صلح جنگ آشکارا کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رسولی فرستیم از سوی خویش</p></div>
<div class="m2"><p>ره نرم خویی بگیریم پیش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بینیم مقصود این قوم چیست</p></div>
<div class="m2"><p>چنین فتنه و جنگ جویی ز کیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز حرف رسالت چو شد گفتگوی</p></div>
<div class="m2"><p>سوی خواجه یوسف بکردند روی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که ای خواجه امروز بهر خدای</p></div>
<div class="m2"><p>سوی کشور بلخ بگذار پای</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در این امر مشکل تویی دلپسند</p></div>
<div class="m2"><p>تویی نور چشم شه نقشبند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بکن تکیه بر روح اجداد خود</p></div>
<div class="m2"><p>از ایشان طلب ساز امداد خود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دگر تکیه بر دولت شاه کن</p></div>
<div class="m2"><p>تأمل بنه روی بر راه کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز ما گوی اول به سلطان سلام</p></div>
<div class="m2"><p>سلام دگر بر خواص و عوام</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که اینک رسیدست خان شما</p></div>
<div class="m2"><p>خلایق شده میهمان شما</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رسانید قدر شما بر فلک</p></div>
<div class="m2"><p>فراموش گردید حق نمک</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو این قصه با خواجه انجام یافت</p></div>
<div class="m2"><p>عنان عزیمت سوی بلخ تافت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دروازه خود را رسانید زود</p></div>
<div class="m2"><p>به دل داشت زاری به لب یا ودود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یکی قلعه ای دید آراسته</p></div>
<div class="m2"><p>به معماریش چرخ برخاسته</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چه قلعه چو غارتگران سنگدل</p></div>
<div class="m2"><p>بود کوه قاف از سوادش خجل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سر گرد برجش به اوج کمال</p></div>
<div class="m2"><p>به معماریش ایستاده هلال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به بالای او دیدبان ستیز</p></div>
<div class="m2"><p>به اطراف او بیستون خاک ریز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به دروازه هایش ملک پاسبان</p></div>
<div class="m2"><p>بود سد اسکندرش پشتبان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فگندند در شهر آوازه را</p></div>
<div class="m2"><p>به رویش کشادند دروازه را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>رسیدند جمعیت بیکران</p></div>
<div class="m2"><p>گرفته به کف تیغ و تیر و سنان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روان بود از تیغشان جوی خون</p></div>
<div class="m2"><p>که می آمد از هر طرف بوی خون</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی چاکری خواجه همراه داشت</p></div>
<div class="m2"><p>به دل هر زمان صورتی می نگاشت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به خود هر دم اندیشه ها می شمرد</p></div>
<div class="m2"><p>دلش از توهم شد و آب برد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هجوم خلایق ز دنبال و پیش</p></div>
<div class="m2"><p>رساندند او را به سلطان خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>همان دم نشست و زبان برکشاد</p></div>
<div class="m2"><p>سخن آنچه بودش همه عرضه داد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بوبستند لبها همه از جواب</p></div>
<div class="m2"><p>نشستند گم کرده راه صواب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یکی زان میانه زبان برکشاد</p></div>
<div class="m2"><p>که ما را نباشد به شه اعتماد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>رسولی ز ما هم رود سوی شاه</p></div>
<div class="m2"><p>گناهان ما را شود عذرخواه</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پس آنگه همه عهد و پیمان کنید</p></div>
<div class="m2"><p>به ما و به خود کار آسان کنید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یکی خواجه عصمت الله بنام</p></div>
<div class="m2"><p>که او بود منظور خواص و عوام</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به او راز خود را عیان ساختند</p></div>
<div class="m2"><p>به همراه خواجه روان ساختند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>به بیرون دروازه شاه و سپاه</p></div>
<div class="m2"><p>نشسته به امید چشمی به راه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دو کس ناگه از دور پیدا شدند</p></div>
<div class="m2"><p>رسیدند و در بارگه جا شدند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بگفتند هر یک ز احوال شهر</p></div>
<div class="m2"><p>به هم بود آمیخته قند و زهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رسیدند بر انتهای کلام</p></div>
<div class="m2"><p>نشد فهم ازو انقیاد تمام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>جوانان ز هر گوشه برخاستند</p></div>
<div class="m2"><p>پی جنگجویی قد آراستند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بگفتند ای شاه اقلیم گیر</p></div>
<div class="m2"><p>تو را باد پاینده تاج و سریر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز شاهان پیشین تویی انتخاب</p></div>
<div class="m2"><p>تویی وارث ملک افراسیاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ز تو امر کردن دلیری ز ما</p></div>
<div class="m2"><p>ستادن ز تو قلعه گیری ز ما</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اگر آسمانست این شهر بند</p></div>
<div class="m2"><p>بود همت ما رسا چون کمند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر باشد این قلعه از کوه تن</p></div>
<div class="m2"><p>بود دست ما تیشه کوه کن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر خاکریزش بود کوه قاف</p></div>
<div class="m2"><p>سر نیزه ماست خارا شکاف</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>اگر هست دروازه اش آهنین</p></div>
<div class="m2"><p>بود تیغ ما را دم آتشین</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو شه دید آشوب گردنکشان</p></div>
<div class="m2"><p>شد از لعل سیراب گوهرفشان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگفت ای جوانان تحمل کنید</p></div>
<div class="m2"><p>درین تندخویی تأمل کنید</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>برآید به تدبیر از پیش کار</p></div>
<div class="m2"><p>به از زور دار است تدبیر وار</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>فرستیم در بلخ دیگر پیام</p></div>
<div class="m2"><p>پیامی که باشد سراسر نظام</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>رسولی که باشد لبالب ز فن</p></div>
<div class="m2"><p>کهنسال با رأی رنگین سخن</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>رسولی که چون شمع در گفتگوی</p></div>
<div class="m2"><p>زبان آتشین باشد و نرم خوی</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چراغ رسالت منور کند</p></div>
<div class="m2"><p>به خود این ره پر خطر سر کند</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز فانوس بیرون کند شمع را</p></div>
<div class="m2"><p>به پروانه آمد دل جمع را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>زیان برکشادند از هر طرف</p></div>
<div class="m2"><p>گهر ریختند از دهان چون صدف</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>قلیچ یی در این امر لایق بود</p></div>
<div class="m2"><p>به هر کار رایش موافق بود</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به علم و ادب هست آراسته</p></div>
<div class="m2"><p>به تدبیر بنشسته و خاسته</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>به افسون تواند کند کار را</p></div>
<div class="m2"><p>ز سوراخ بیرون کند مار را</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>دلیر و سخن سنج و فهمیده است</p></div>
<div class="m2"><p>نهنگان و نام آوران دیده است</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>پدر تا پدر آمده قلعه گیر</p></div>
<div class="m2"><p>کند حمله شیر اولاد شیر</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>طلب کرد او را شه کامگار</p></div>
<div class="m2"><p>نشایند در مجلس اعتبار</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>به کف جام از لطف آماده کرد</p></div>
<div class="m2"><p>ز شهد عنایت در او باده کرد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ز ساقی دوران شده سرفراز</p></div>
<div class="m2"><p>در آن مجمع او را بداد امتیاز</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>چو او باده مرحمت کرد نوش</p></div>
<div class="m2"><p>لباس رسالت کشیدش به دوش</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شه مرحمت کیش گردون رکاب</p></div>
<div class="m2"><p>به او تحفه داد از سئوال و جواب</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ز رخصت چو هنگامه انجام یافت</p></div>
<div class="m2"><p>ز صحبت همان لحظه بیرون شتافت</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>سوی قلعه بلخ آورد روی</p></div>
<div class="m2"><p>زبان و دلش از خدا چاره جوی</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>سمندش به ره چون قدم کرد تیز</p></div>
<div class="m2"><p>رسیدش ته قلعه چون خاکریز</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نظر کرد از کنگرش دیدبان</p></div>
<div class="m2"><p>ستاده یکی مرد آتش عنان</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>فغان کرد کای غافلان دیار</p></div>
<div class="m2"><p>یکی مرد تورانی نامدار</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>به بیرون دروازه ایستاده است</p></div>
<div class="m2"><p>لبش در سخن گفتن آماده است</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>بود نام و آوازه او علم</p></div>
<div class="m2"><p>به تیغش قلیچ بی نموده رقم</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>به گوش همه چون رسید این ندا</p></div>
<div class="m2"><p>کشادند دروازه را بی ابا</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>عنانش گرفتند خورد و کلان</p></div>
<div class="m2"><p>کشیدند در پیش نام آوران</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>رسید و زبان رسالت کشاد</p></div>
<div class="m2"><p>که ای تیره روزان غافل نهاد</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>چرا اینچنین کارها می کنید</p></div>
<div class="m2"><p>به شاه خود این ماجرا می کنید</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>رود بعد از این نیکخواهی به باد</p></div>
<div class="m2"><p>شهان را نماند به کس اعتماد</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>ز طعن خلایق ملامت کشید</p></div>
<div class="m2"><p>به روز قیامت خجالت کشید</p></div></div>
<div class="b" id="bn84"><div class="m1"><p>همه دست پرورد خوان ویند</p></div>
<div class="m2"><p>به پابوسی او به سرها روند</p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به گردن همه ترکش آویخته</p></div>
<div class="m2"><p>روید اشک از دیدها ریخته</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>منازید بر قلعه و بند خود</p></div>
<div class="m2"><p>مسازید فخری به پیوند خود</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>مبادا شود بر شما کار تنگ</p></div>
<div class="m2"><p>ز مردم بمانید در زیر ننگ</p></div></div>
<div class="b" id="bn88"><div class="m1"><p>جدا گر شود بند از بند من</p></div>
<div class="m2"><p>همین است بهر شما پند من</p></div></div>
<div class="b" id="bn89"><div class="m1"><p>ز هر سو کشادند مردم زبان</p></div>
<div class="m2"><p>سخنهایت آورده ما را به جان</p></div></div>
<div class="b" id="bn90"><div class="m1"><p>یکی گفت تیغی به کارش کنید</p></div>
<div class="m2"><p>همین دم سزا در کنارش کنید</p></div></div>
<div class="b" id="bn91"><div class="m1"><p>یکی گفت زندان بود جای او</p></div>
<div class="m2"><p>یکی گفت چاه است مأوای او</p></div></div>
<div class="b" id="bn92"><div class="m1"><p>یکی گفت اینها بود ناپسند</p></div>
<div class="m2"><p>همان به که سازیم در خانه بند</p></div></div>
<div class="b" id="bn93"><div class="m1"><p>هر آن کس که با او زبان برکشاد</p></div>
<div class="m2"><p>جوابش بگفت و سزایش بداد</p></div></div>
<div class="b" id="bn94"><div class="m1"><p>در آخر ببردند در یک مکان</p></div>
<div class="m2"><p>در آنجا بوبستند بی ریسمان</p></div></div>
<div class="b" id="bn95"><div class="m1"><p>چه خانه جنون کرده تعمیر او</p></div>
<div class="m2"><p>بود قفل وسواس زنجیر او</p></div></div>
<div class="b" id="bn96"><div class="m1"><p>منقش به دیوار او نام مرگ</p></div>
<div class="m2"><p>لب بام او داده پیغام مرگ</p></div></div>
<div class="b" id="bn97"><div class="m1"><p>اجل را به خود کرده هر دم رقم</p></div>
<div class="m2"><p>نشسته در اندیشه در کنج غم</p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ز در ناگاه آواز پایی شنید</p></div>
<div class="m2"><p>ز خود دست شست و ز جان کند امید</p></div></div>
<div class="b" id="bn99"><div class="m1"><p>یکی آمده گفت ای پر ستیز</p></div>
<div class="m2"><p>تو را خان طلب می کند زود خیز</p></div></div>
<div class="b" id="bn100"><div class="m1"><p>گرفتند و بردند در پیش شاه</p></div>
<div class="m2"><p>دعا کرد و بنشست در پیشگاه</p></div></div>
<div class="b" id="bn101"><div class="m1"><p>ز بعد ثنا گفت ای شهریار</p></div>
<div class="m2"><p>به مهمانیت آمدیم از بخار</p></div></div>
<div class="b" id="bn102"><div class="m1"><p>به مهمان نکرده کسی این چنین</p></div>
<div class="m2"><p>خصوصا به شاهان روی زمین</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>جواب و سئوالی که شه گفته بود</p></div>
<div class="m2"><p>به رنگی به او هر زمان می نمود</p></div></div>
<div class="b" id="bn104"><div class="m1"><p>به پند و نصیحت چنان گشت گرم</p></div>
<div class="m2"><p>دل سخت او کرد چون موم نرم</p></div></div>
<div class="b" id="bn105"><div class="m1"><p>امامقل اتالیق ز سوی دگر</p></div>
<div class="m2"><p>پی تقویت گرم شد چون شرر</p></div></div>
<div class="b" id="bn106"><div class="m1"><p>عوض بی ز سوی دگر شد روان</p></div>
<div class="m2"><p>شدندش همه چون قلم یک زبان</p></div></div>
<div class="b" id="bn107"><div class="m1"><p>ز هر دو طرف این دو صاحب کمال</p></div>
<div class="m2"><p>کشادند از بهر پرواز بال</p></div></div>
<div class="b" id="bn108"><div class="m1"><p>به افسون شود اژدها زیر دست</p></div>
<div class="m2"><p>به زنجیر گردن نهد شیر مست</p></div></div>
<div class="b" id="bn109"><div class="m1"><p>گر آتش زند شعله چون آفتاب</p></div>
<div class="m2"><p>به آتش توان کرد بی آب و تاب</p></div></div>
<div class="b" id="bn110"><div class="m1"><p>هماندم طلب کرد رخش مراد</p></div>
<div class="m2"><p>ز دروازه بیرون برآمد چو باد</p></div></div>
<div class="b" id="bn111"><div class="m1"><p>ز دنبال او مردمان فوج فوج</p></div>
<div class="m2"><p>روان بر تماشای دریای موج</p></div></div>
<div class="b" id="bn112"><div class="m1"><p>ستاده به نظاره اش آسمان</p></div>
<div class="m2"><p>ستاره به مه می کند چون قران</p></div></div>
<div class="b" id="bn113"><div class="m1"><p>چو نزدیک شد بر در بارگاه</p></div>
<div class="m2"><p>پیاده روان شد به پابوس شاه</p></div></div>
<div class="b" id="bn114"><div class="m1"><p>دو رخ سود بر مسند خسروی</p></div>
<div class="m2"><p>چو فرزین برون آمد از کجروی</p></div></div>
<div class="b" id="bn115"><div class="m1"><p>نشست و سوی شهر شد رهنما</p></div>
<div class="m2"><p>به تکلیف برخاست آندم ز جا</p></div></div>
<div class="b" id="bn116"><div class="m1"><p>بگفتا فلک باد دمساز تو</p></div>
<div class="m2"><p>سر من بود پای انداز تو</p></div></div>
<div class="b" id="bn117"><div class="m1"><p>به دولت هماندم شه کامیاب</p></div>
<div class="m2"><p>درآورد پا در هلال رکاب</p></div></div>
<div class="b" id="bn118"><div class="m1"><p>چو روشن شد از مقدمش چشم شهر</p></div>
<div class="m2"><p>قیامت شد آن روز قایم به دهر</p></div></div>
<div class="b" id="bn119"><div class="m1"><p>بیا ساقی آن باده لاله گون</p></div>
<div class="m2"><p>که از تیغ موجش زند بوی خون</p></div></div>
<div class="b" id="bn120"><div class="m1"><p>به من ده که شوید ز طبعم ملال</p></div>
<div class="m2"><p>سر دشمنان را کنم پایمال</p></div></div>