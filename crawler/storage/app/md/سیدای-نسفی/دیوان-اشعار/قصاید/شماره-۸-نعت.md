---
title: >-
    شمارهٔ  ۸ - نعت
---
# شمارهٔ  ۸ - نعت

<div class="b" id="bn1"><div class="m1"><p>ای روضه تو قبله ارباب انس و جان</p></div>
<div class="m2"><p>بام تو را ملایکه عرش پاسبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آماده کرده اند به او نعمت بهشت</p></div>
<div class="m2"><p>هر کس شدست بر سر خوان تو میهمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معمار کرده صفه قدرت چنان رفیع</p></div>
<div class="m2"><p>یک پایه ای فروتر او هفتم آسمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید خبرگاه تو را گشته پرده پوش</p></div>
<div class="m2"><p>مه گرد او چو کاغذ زردی به نا بدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتن ز روضه تو به جایی چه زندگیست</p></div>
<div class="m2"><p>بودن بر آستان تو عمریست جاودان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با شوکت آن شبی که ز معراج آمدی</p></div>
<div class="m2"><p>دیدند رفعت تو به چشم اهل کاروان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اصحاب صفه ات همه نور مجسمند</p></div>
<div class="m2"><p>جمعند همچو غنچه و دارند یک زبان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مسند خلافت حق چار یار تو</p></div>
<div class="m2"><p>چشم و چراغ و صحبتشان شاهزادگان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اولاد تو همیشه عزیز و مکرمند</p></div>
<div class="m2"><p>چشم بدی مباد درین پاک خاکدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ارکان دولتت همه اهل سخاوتند</p></div>
<div class="m2"><p>فتح و ظفر متابع و اقبال در عنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دارد هوای بندگیت گردن فلک</p></div>
<div class="m2"><p>بر دوش چرخ طوق غلامیست کهکشان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از مکه ای نسیم سحر تا برآمدی</p></div>
<div class="m2"><p>چون بوی گل شدند پریشان مهاجران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا در مدینه پای مبارک نهاده ای</p></div>
<div class="m2"><p>سرهای آن گروه رسیده به آسمان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر نخل خشک تکیه کنی سبز می شود</p></div>
<div class="m2"><p>بر می دهد به مردم عالم همان زمان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>امروز هر لبی که تهی از درود توست</p></div>
<div class="m2"><p>چون کاسه شکسته بود خاک بر دهان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فردای حشر دولت ما اینقدر بس است</p></div>
<div class="m2"><p>ما امت توئیم و تو غمخوار امتان</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنها که صرف راه تو کردند نقد عمر</p></div>
<div class="m2"><p>از گیر و دار روز حسابند در امان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نخل وجود هر که نظر کرده تو شد</p></div>
<div class="m2"><p>ایمن بود بهار وی از آفت خزان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شاهنشها به گوشه چشمی همی نگر</p></div>
<div class="m2"><p>دارم قد خمیده تر از قامت کمان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دوش از زبان بیهوده گوی من از خطا</p></div>
<div class="m2"><p>ناشکریی که سرزده گفتن نمی توان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>افتاده ام به گوشه محنت سرای خویش</p></div>
<div class="m2"><p>دست تهی و پیر و کسلمند و ناتوان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از لطف سایه بر سر بالین من فگن</p></div>
<div class="m2"><p>ای بر سر مبارک تو ابر سایه بان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>می خواهم از خدا شفاعت کنی مرا</p></div>
<div class="m2"><p>بخشد حیات خضر دهد دولت جوان</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دارم هوای مکه به پابوسیت روم</p></div>
<div class="m2"><p>مالم رخ نیاز بر آن خاک آستان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همچون عصابه است رویها مثل شدم</p></div>
<div class="m2"><p>ای آستانه تو بود جای راستان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چشم به خاکپای مقیمان روضه ات</p></div>
<div class="m2"><p>انشا کند دعا و سلامی ز حاجیان</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جاروب آستان تو موی سپید من</p></div>
<div class="m2"><p>روزی شود خدای کند از مجاوران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زاد سفر ز سفره تو دارم آرزو</p></div>
<div class="m2"><p>ای منزل تو پشت و پناه مسافران</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پیچیده سر به جیب نشستست غنچه وار</p></div>
<div class="m2"><p>کرد آن شکوفه روی تر از شاخ ارغوان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر حال سیدا نظر کن ز مرحمت</p></div>
<div class="m2"><p>گردیده است گلشن امید او خزان</p></div></div>