---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>فراق دوستان چون لاله داغم بر جگر دارد</p></div>
<div class="m2"><p>نشاط باده ایام در پی درد سر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم خاک و مرا باد صبا آواره تر دارد</p></div>
<div class="m2"><p>غبارم را نسیم از ناتوانی در به در دارد</p></div></div>
<div class="b2" id="bn3"><p>غریب کشور طالع چه پروای سفر دارد</p></div>
<div class="b" id="bn4"><div class="m1"><p>به دور حسن خود هم جام ما خواهی شدن آخر</p></div>
<div class="m2"><p>برای دانه ای در دام ما خواهی شدن آخر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می روزی تو هم بر جام ما خواهی شدن آخر</p></div>
<div class="m2"><p>به افسون محبت رام ما خواهی شدن آخر</p></div></div>
<div class="b2" id="bn6"><p>چنین بیگانه گشتن ز آشنائی ها خبر دارد</p></div>
<div class="b" id="bn7"><div class="m1"><p>بتی دارم که چشم مرحمت پیوسته می پوشد</p></div>
<div class="m2"><p>به قول مدعی با قتل من هر لحظه می کوشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بعد از مرگم آن نو خط بغیری باده می نوشد</p></div>
<div class="m2"><p>ز خاکم سبزه می روید ز خونم لاله می جوشد</p></div></div>
<div class="b2" id="bn9"><p>بهار گلشن خونین دلان فیض دگر دارد</p></div>
<div class="b" id="bn10"><div class="m1"><p>فتاده بر سرم ای همنشینان شعله سودا</p></div>
<div class="m2"><p>چو شمع از سوز دل آتش زنم بر کشور اعضا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا از هر طرف گردیده صد درد و الم پیدا</p></div>
<div class="m2"><p>فلک تا آشنا طالع زبون معشوق بی پروا</p></div></div>
<div class="b2" id="bn12"><p>مگر افتادگی روزی مرا از خاک بردارد</p></div>
<div class="b" id="bn13"><div class="m1"><p>چو شبنم سیدا هر کس که از خود دست می شوید</p></div>
<div class="m2"><p>ره افلاک را ماننده یی خورشید می پوید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به گلشن طوطی و بلبل به صد فریاد می گوید</p></div>
<div class="m2"><p>به قدر همت خود هر که باشد فخر می جوید</p></div></div>
<div class="b2" id="bn15"><p>چمن گل نیشکر صایب غزل دریا گهر دارد</p></div>