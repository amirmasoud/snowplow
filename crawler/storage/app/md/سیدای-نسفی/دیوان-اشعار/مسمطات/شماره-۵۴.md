---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>دیده را عمری به مردم آشنا کردم نشد</p></div>
<div class="m2"><p>سینه را آئینه صدق و صفا کردم نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با بد و نیک جهان عهد و وفا کردم نشد</p></div>
<div class="m2"><p>در دل هر کس ز روی مهر جا کردم نشد</p></div></div>
<div class="b2" id="bn3"><p>خانه شادی به روی خویش وا کردم نشد</p></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی چون سایه بودم در قفای شیخ و شاب</p></div>
<div class="m2"><p>روزگاری همنشین بودم به مینای شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به تنهایی برآرم بعد ازین چون آفتاب</p></div>
<div class="m2"><p>پاس خاطر تا به کی دارم درین دیر خراب</p></div></div>
<div class="b2" id="bn6"><p>با قلندر شرب و با زاهد ریا کردم نشد</p></div>
<div class="b" id="bn7"><div class="m1"><p>مهربانی ها نمودم دوستان خویش را</p></div>
<div class="m2"><p>باز با مدح و ثنا کردم زبان خویش را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون قلم گویم من اکنون داستان خویش را</p></div>
<div class="m2"><p>خاک پای هر یک از همصحبتان خویش را</p></div></div>
<div class="b2" id="bn9"><p>سالها در دیده خود توتیا کردم نشد</p></div>
<div class="b" id="bn10"><div class="m1"><p>مجلس اغیار روشن کرده است آن می پرست</p></div>
<div class="m2"><p>ای مسلمانان چه باید کرد با آن شوخ مست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در فراق زلف کافر کیش می خوردم شکست</p></div>
<div class="m2"><p>آبروی رفته را می خواستم آرم به دست</p></div></div>
<div class="b2" id="bn12"><p>در حریم کعبه چندانی دعا کردم نشد</p></div>
<div class="b" id="bn13"><div class="m1"><p>سیدا دارم دلی چون غنچه گل پر ز خون</p></div>
<div class="m2"><p>کوکب بختم در آب افتاده طالع شد زبون</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می روم زنجیر در پا در بیابان جنون</p></div>
<div class="m2"><p>بس که ناکام از دل آفاق افتادم برون</p></div></div>
<div class="b2" id="bn15"><p>قیمت خود را به نرخ خاک پا کردم نشد</p></div>