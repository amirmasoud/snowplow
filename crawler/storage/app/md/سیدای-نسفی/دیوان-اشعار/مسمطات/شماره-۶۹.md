---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>برای پرسشم ای آهوی حرم برخیز</p></div>
<div class="m2"><p>ز جای خود به خدا می دهم قسم برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکن به جان و دلم این همه ستم برخیز</p></div>
<div class="m2"><p>سبک ز سینه من ای غبار غم برخیز</p></div></div>
<div class="b2" id="bn3"><p>ز همنشینی من می کشی الم برخیز</p></div>
<div class="b" id="bn4"><div class="m1"><p>تکلم تو دهد بر رقیب آب حیات</p></div>
<div class="m2"><p>تبسمت دهن غیر پر کند ز نبات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرا به ملک خرابم کنی ز جور برات</p></div>
<div class="m2"><p>سر قلم بشکن مهر کن دهان دوات</p></div></div>
<div class="b2" id="bn6"><p>به این سیاه دلان کم نشین و کم برخیز</p></div>
<div class="b" id="bn7"><div class="m1"><p>به منعمان دو جهان را خریدن آسانست</p></div>
<div class="m2"><p>گل از نتیجه زر رونق گلستانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا به گوش حدیثی ز حور و رضوانست</p></div>
<div class="m2"><p>کلید گلشن فردوس دست احسانست</p></div></div>
<div class="b2" id="bn9"><p>بهشت اگرطلبی از سر درم برخیز</p></div>
<div class="b" id="bn10"><div class="m1"><p>نگاه دار به خود چشم گوهر افشان را</p></div>
<div class="m2"><p>چو گل مدار نباشد نشاط دوران را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مباش این همه پیر و خط جوانان را</p></div>
<div class="m2"><p>به دار عزت موی سفید پیران را</p></div></div>
<div class="b2" id="bn12"><p>چو آفتاب به تعظیم صبحدم برخیز</p></div>
<div class="b" id="bn13"><div class="m1"><p>مرا چو غنچه بود کار سینه را خستن</p></div>
<div class="m2"><p>به زلف یار تو را آرزوی پیوستن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به بوستان مکن ای سرو قصد به نشستن</p></div>
<div class="m2"><p>درین جهان نبود فرصت کمر بستن</p></div></div>
<div class="b2" id="bn15"><p>ز خاک تیره کمر بسته چون قلم برخیز</p></div>
<div class="b" id="bn16"><div class="m1"><p>به باغ مرغ چمن از پی خوش الحانیست</p></div>
<div class="m2"><p>به کنج خانه خود جغد در ثنا خوانیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بهار فیض شب و روز در گل افشانیست</p></div>
<div class="m2"><p>درین دو وقت اجابت کشانده پیشانیست</p></div></div>
<div class="b2" id="bn18"><p>دل شب ار نتوانی سفیده دم برخیز</p></div>
<div class="b" id="bn19"><div class="m1"><p>چو سیدا به جهان عیش رانده‌ای صایب</p></div>
<div class="m2"><p>به سینه تخم غم اکنون نشانده‌ای صایب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو سرو دست ز حاصل فشانده‌ای صایب</p></div>
<div class="m2"><p>چه پای در گل اندیشه مانده‌ای صایب</p></div></div>
<div class="b2" id="bn21"><p>بساز با کم و بیش و ز بیش و کم برخیز</p></div>