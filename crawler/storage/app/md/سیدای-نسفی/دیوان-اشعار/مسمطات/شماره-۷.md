---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>دلا ز بزم حریفان چو غنچه پنهان باش</p></div>
<div class="m2"><p>بپوش دیده و دور از شکست دوران باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو ز گلشن و در گوشه بیابان باش</p></div>
<div class="m2"><p>ز خارزار تعلق کشیده دامان باش</p></div></div>
<div class="b2" id="bn3"><p>بهر چه می کشد دل ازو گریزان باش</p></div>
<div class="b" id="bn4"><div class="m1"><p>مرو به باغ اگر باغبان تو را پدر است</p></div>
<div class="m2"><p>نظر به سایه سنبل مکن که دردسر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بوستان شب و روز این نوا زنی شکر ست</p></div>
<div class="m2"><p>قد نهال خم از بار منت ثمر است</p></div></div>
<div class="b2" id="bn6"><p>ثمر قبول مکن سرو این گلستان باش</p></div>
<div class="b" id="bn7"><div class="m1"><p>بهار عمر گذر کرده است نادانی</p></div>
<div class="m2"><p>دمی بیا و نشین صحن سنبلستانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنوش باده هر سوی ساز جولانی</p></div>
<div class="m2"><p>در این دو هفته که چون گل در این گلستانی</p></div></div>
<div class="b2" id="bn9"><p>گشاده روی تر از راز می پرستان باش</p></div>
<div class="b" id="bn10"><div class="m1"><p>همیشه حرف تو از باده نوشی خلق است</p></div>
<div class="m2"><p>قبا بدوش تو از خودفروشی خلق است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بدهر زینت اگر جامه پوشی خلق است</p></div>
<div class="m2"><p>کدام جامه به از پرده پوشی خلق است</p></div></div>
<div class="b2" id="bn12"><p>بپوش چشم خود از عیب خلق عریان باش</p></div>
<div class="b" id="bn13"><div class="m1"><p>به گوش زاغ نواهای زاغ دلخواه است</p></div>
<div class="m2"><p>به چشم خویش زغن بلبل سحرگاه است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آشیانه خود جغد صاحب جاه است</p></div>
<div class="m2"><p>درون خانه خود هر گدا شهنشاه است</p></div></div>
<div class="b2" id="bn15"><p>قدم برون منه از حد خویش سلطان باش</p></div>
<div class="b" id="bn16"><div class="m1"><p>خزان شدی دیگر امید از بهار تو نیست</p></div>
<div class="m2"><p>می نشاط به اندازه خمار تو نیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کنون شکایت اهل جهان شعار تو نیست</p></div>
<div class="m2"><p>تمیز نیک و بد روزگار کار تو نیست</p></div></div>
<div class="b2" id="bn18"><p>چو چشم آئینه بر خوب و زشت حیران باش</p></div>
<div class="b" id="bn19"><div class="m1"><p>چو سیدا به چمن کرده ام وطن صایب</p></div>
<div class="m2"><p>چو کلک خود شده ام شمع انجمن صایب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مرا به گوش رسیدست این سخن صایب</p></div>
<div class="m2"><p>ز بلبلان خوش الحان این چمن صایب</p></div></div>
<div class="b2" id="bn21"><p>مرید زمزمه حافظ خوش الحان باش</p></div>
<div class="b" id="bn22"><div class="m1"><p>تا مرا گوشه نشین کرد غم تنهایی</p></div>
<div class="m2"><p>قصر افلاک شد از گریه من دریایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چند گویم من ماتمزده یی سودایی</p></div>
<div class="m2"><p>ای مرا دل ز غمت واله جان شیدایی</p></div></div>
<div class="b2" id="bn24"><p>من بدین حال و تو در غایت بی پروایی</p></div>
<div class="b" id="bn25"><div class="m1"><p>تا زده دست من آن غمزه بی باک به سر</p></div>
<div class="m2"><p>گردد از بی خودیم گردش افلاک به سر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بنشین یک نفسی ای بت چالاک به سر</p></div>
<div class="m2"><p>دامن از ناز مکش تا نکنم خاک به سر</p></div></div>
<div class="b2" id="bn27"><p>زلف بر باده مده تا نشوم شیدایی</p></div>
<div class="b" id="bn28"><div class="m1"><p>شوخ من بند نقاب از رخ گلبرگ کشای</p></div>
<div class="m2"><p>جلوه یی سر کن و از خانه خورشید برای</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حرف پروانه خود گوش کن از بهر خدای</p></div>
<div class="m2"><p>یک شب ای شمع بتان سوی من غمزده آی</p></div></div>
<div class="b2" id="bn30"><p>که درین کلبه غم سوختم از تنهایی</p></div>
<div class="b" id="bn31"><div class="m1"><p>بس که آورد به کوی تو مرا دیده تر</p></div>
<div class="m2"><p>بعد ازین از سر کوی تو نبردارم سر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آستان تو بود سجدگهم تا به سحر</p></div>
<div class="m2"><p>تو به خواب خوش و من همچو غلامان بر در</p></div></div>
<div class="b2" id="bn33"><p>همه شب منتظرم تا تو چه می فرمایی</p></div>
<div class="b" id="bn34"><div class="m1"><p>سیدا همچو خط سبز به تمکین گفتی</p></div>
<div class="m2"><p>در بناگوش وی افسانه رنگین گفتی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کوه کن تیشه زدی بر سر و تحسین گفتی</p></div>
<div class="m2"><p>جامیا بس که سخن زآن لب شیرین گفتی</p></div></div>
<div class="b2" id="bn36"><p>طوطی طبع تو شد شهره به شکرخوایی</p></div>