---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>خوردی شراب ناب سرانجامیی تو رفت</p></div>
<div class="m2"><p>شرم نگه ز نرگس بادامیی تو رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ از رخ حیا ز می آشامیی تو رفت</p></div>
<div class="m2"><p>رفتی به بزم غیر نکو نامیی تو رفت</p></div></div>
<div class="b2" id="bn3"><p>ناموس صد قبیله به یک خامیی تو رفت</p></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد اگر به دامن پاکت برد سجود</p></div>
<div class="m2"><p>عاشق به عصمت تو فرستد اگر درود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دم رسد ز گردش افلاک این سرود</p></div>
<div class="m2"><p>اکنون اگر فرشته نکو گویدت چه سود</p></div></div>
<div class="b2" id="bn6"><p>در شهرها حکایت بدنامیی تو رفت</p></div>
<div class="b" id="bn7"><div class="m1"><p>امروز از عذار تو رفتست نور حسن</p></div>
<div class="m2"><p>از هرزه گردی تو نمانده شعور حسن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می گفت همچو زلف به گوش تو شور حسن</p></div>
<div class="m2"><p>همصحبت رقیب شدی از غرور حسن</p></div></div>
<div class="b2" id="bn9"><p>نام خوش تو بر سر خودکامیی تو رفت</p></div>
<div class="b" id="bn10"><div class="m1"><p>آنها که خویش را به تو غمخوار کرده اند</p></div>
<div class="m2"><p>رسوا تو را به کوچه و بازار کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از آشنائیت دل و جان عار کرده اند</p></div>
<div class="m2"><p>یاران متفق به تو انکار کرده اند</p></div></div>
<div class="b2" id="bn12"><p>هر جا حدیث نیک سرانجامی تو رفت</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای سیدا به نرگس شوخت نظر نماند</p></div>
<div class="m2"><p>تیری که در بساط کمان داشت پر نماند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زخمی ز نیش خنجر او بر جگر نماند</p></div>
<div class="m2"><p>با کاو کاو غمزه نظیری نظر نماند</p></div></div>
<div class="b2" id="bn15"><p>فارغ نشین که خون دل آشامیی تو رفت</p></div>