---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>از شوق تو افتادم در بادیه پیمایی</p></div>
<div class="m2"><p>دارد من مجنون را سودای تو سودایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفتی و مرا ماندی در کنج شکیبایی</p></div>
<div class="m2"><p>ای پادشه خوبان داد از غم تنهایی</p></div></div>
<div class="b2" id="bn3"><p>دل بی تو به جان آمد وقتیست که بازآیی</p></div>
<div class="b" id="bn4"><div class="m1"><p>افتاده ام از چشمت در کشور گمنامی</p></div>
<div class="m2"><p>می گریم و می سوزم چون شمع من از خامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد تو مرا آورد بالین سرانجامی</p></div>
<div class="m2"><p>ای درد توام درمان در بستر ناکامی</p></div></div>
<div class="b2" id="bn6"><p>وی یاد توام مونس در گوشه تنهایی</p></div>
<div class="b" id="bn7"><div class="m1"><p>روزی که تو را ایام از دیده نهانم کرد</p></div>
<div class="m2"><p>صد شعله بیدادی قصه دل و جانم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردون نه مرا آن نوع رسوای جهانم کرد</p></div>
<div class="m2"><p>مشتاقی و مهجوری دور از تو چنانم کرد</p></div></div>
<div class="b2" id="bn9"><p>کز دست نخواهد شد دامان شکیبایی</p></div>
<div class="b" id="bn10"><div class="m1"><p>رخسار تو را از بت آن تاب نمی ماند</p></div>
<div class="m2"><p>چشم سیهت از ناز در خواب نمی ماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>باغ رخ تو تا حشر سیراب نمی ماند</p></div>
<div class="m2"><p>دایم گل این بستان شاداب نمی ماند</p></div></div>
<div class="b2" id="bn12"><p>دریاب ضعیفان را در وقت توانایی</p></div>
<div class="b" id="bn13"><div class="m1"><p>تا بر سرت ای سید آن شوخ سوار آمد</p></div>
<div class="m2"><p>افروخته سر تا پا همچون گل نار آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فصل غم و محنت رفت ایام بهار آمد</p></div>
<div class="m2"><p>حافظ شب هجران شد بوی خوش یار آمد</p></div></div>
<div class="b2" id="bn15"><p>شادیت مبارک باد ای عاشق شیدایی</p></div>