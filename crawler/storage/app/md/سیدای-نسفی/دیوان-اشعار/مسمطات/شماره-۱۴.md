---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>رخ چو برگ گل و قد دلربا داری</p></div>
<div class="m2"><p>به چشم هر که نهی پای خویش جا داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به غیر ما به همه کس سر وفا داری</p></div>
<div class="m2"><p>چه حالتست که با ما سر جفا داری</p></div></div>
<div class="b2" id="bn3"><p>چه موجب است که خود را ز ما جدا داری</p></div>
<div class="b" id="bn4"><div class="m1"><p>قسم به ذات تو ای نونهال باغ حیا</p></div>
<div class="m2"><p>مراست کوی تو بهتر ز جنت المأوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمی کنی نظری سوی ما به استغنا</p></div>
<div class="m2"><p>چه کرده ام چه شنیدی چه دیده ای از ما</p></div></div>
<div class="b2" id="bn6"><p>که جان من هدف ناوک بلا داری</p></div>
<div class="b" id="bn7"><div class="m1"><p>شکفته غنچه داغم به یاد مرهم تو</p></div>
<div class="m2"><p>نشسته در ته تیغم ز ابروی خم تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حال مرگ فتادم ز پرسش کم تو</p></div>
<div class="m2"><p>رو مدار جوانی بمیرد از غم تو</p></div></div>
<div class="b2" id="bn9"><p>تو هم جوانی و از خود امیدها داری</p></div>
<div class="b" id="bn10"><div class="m1"><p>خمیده قامتم از بار غصه چون مه نو</p></div>
<div class="m2"><p>به خان و مان من آتش فگنده شد مرو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دمی ز روی مروت حدیث من بشنو</p></div>
<div class="m2"><p>ز یک سخن که رقیبم بگفت رنجه مشو</p></div></div>
<div class="b2" id="bn12"><p>که این ز شرط کرم باشد و وفاداری</p></div>
<div class="b" id="bn13"><div class="m1"><p>تو را همیشه بود قتل سیدا مقصود</p></div>
<div class="m2"><p>نهاده غمزه به دست تو تیغ زهرآلود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر چه می شوی این دم ز مرگ ناخشنود</p></div>
<div class="m2"><p>چنین مکن که پشیمان شوی ندارد سود</p></div></div>
<div class="b2" id="bn15"><p>که با عراقی مسکین سر جفا داری</p></div>