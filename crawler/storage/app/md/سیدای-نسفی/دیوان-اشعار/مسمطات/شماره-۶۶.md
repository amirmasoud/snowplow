---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>نسازد توبه از کردار خود جانی که من دارم</p></div>
<div class="m2"><p>نمی آید به خود تا حشر نسیانی که من دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی گنجد به عالم جرم پنهانی که من دارم</p></div>
<div class="m2"><p>زند پهلو به گردون کوه عصیانی که من دارم</p></div></div>
<div class="b2" id="bn3"><p>به صد دریا نگردد پاک دامانی که من دارم</p></div>
<div class="b" id="bn4"><div class="m1"><p>در آن وادی که من هستم به جز گردون نمی گردد</p></div>
<div class="m2"><p>برای دستگیری رهبری بیرون نمی گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقینم شد که لیلی همدمم اکنون نمی گردد</p></div>
<div class="m2"><p>ز وحشت سایه یی بر گرد من مجنون نمی گردد</p></div></div>
<div class="b2" id="bn6"><p>ندارد کعبه گرد خود بیابانی که من دارم</p></div>
<div class="b" id="bn7"><div class="m1"><p>درین ایام گردون حاجتم بیرون نمیارد</p></div>
<div class="m2"><p>هوس دیگر ز کنج عزلتم بیرون نمیارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به تکلیف آفتاب از صحبتم بیرون نمیارد</p></div>
<div class="m2"><p>تماشای بهشت از خلوتم بیرون نمیارد</p></div></div>
<div class="b2" id="bn9"><p>به است از جنت در بسته زندانی که من دارم</p></div>
<div class="b" id="bn10"><div class="m1"><p>مرا در گوشه محنت فگنده گردش دوران</p></div>
<div class="m2"><p>به پای افگنده ام زنجیر از کوتاهی دامان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نباشد در دل من آرزوی میوه بستان</p></div>
<div class="m2"><p>ز اکسیر قناعت می شمارم نعمت الوان</p></div></div>
<div class="b2" id="bn12"><p>اگر رنگین به خون گردد لب نانی که من دارم</p></div>
<div class="b" id="bn13"><div class="m1"><p>کمانداری که از بیمش سر خورشید می لرزد</p></div>
<div class="m2"><p>به دامنگیریی او بازوی امید می لرزد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به خود از غیرت او رستم و جمشید می لرزد</p></div>
<div class="m2"><p>ز سهمش پنجه شیران چو برگ بید می لرزد</p></div></div>
<div class="b2" id="bn15"><p>درون سینه از تیرش نیستانی که من دارم</p></div>
<div class="b" id="bn16"><div class="m1"><p>مرا چون سیدا برد آن نگار خرگهی صایب</p></div>
<div class="m2"><p>نباشد هیچ کس را آن پری رو آگهی صایب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به زلفش می توان چون خضر کردن همرهی صایب</p></div>
<div class="m2"><p>ز مد عمر جاویدان ندارد کوتهی صایب</p></div></div>
<div class="b2" id="bn18"><p>ز دست تیغ او زخم نمایانی که من دارم</p></div>