---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>چو غنچه تا به تبسم کشاده‌ای لب‌ها</p></div>
<div class="m2"><p>برند نام تو مردم به جای یارب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندا کنند به کوی تو زاهدان شب‌ها</p></div>
<div class="m2"><p>زهی به غمزه جانسوز برق مذهب‌ها</p></div></div>
<div class="b2" id="bn3"><p>به خنده نمکین نوبهار مشرب‌ها</p></div>
<div class="b" id="bn4"><div class="m1"><p>شبی که روی خود ای ماه من عیان کردی</p></div>
<div class="m2"><p>الف به سینه گردون ز کهکشان کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هلال را ز شفق شاخ ارغوان کردی</p></div>
<div class="m2"><p>به یک کرشمه که در کار آسمان کردی</p></div></div>
<div class="b2" id="bn6"><p>هنوز می‌پرد از شوق چشم کوکب‌ها</p></div>
<div class="b" id="bn7"><div class="m1"><p>خزان رسید و ز باغ اهل عیش و غم رفتند</p></div>
<div class="m2"><p>وداع کرده چمن را به یک قلم رفتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسیم و نکهت گل از قفای هم رفتند</p></div>
<div class="m2"><p>سبک روان به نهانخانه عدم رفتند</p></div></div>
<div class="b2" id="bn9"><p>بر آستانه چو نعلین مانده قالب‌ها</p></div>
<div class="b" id="bn10"><div class="m1"><p>چو شمع بود ز سودای او دلم در تب</p></div>
<div class="m2"><p>رسید بر سر بالینم آن نگار امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو غنچه هست کنون این سخن مرا بر لب</p></div>
<div class="m2"><p>گذشتم از سر مطلب تمام شد مطلب</p></div></div>
<div class="b2" id="bn12"><p>نقاب چهره مقصود بود مطلب‌ها</p></div>
<div class="b" id="bn13"><div class="m1"><p>چو گردباد کنم سیر دشت و هامون را</p></div>
<div class="m2"><p>زنم به خاک دل غوطه خورده در خون را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دهم به زلف و خط یار جان محزون را</p></div>
<div class="m2"><p>از آن به تیرگی شب خوشم که مجنون را</p></div></div>
<div class="b2" id="bn15"><p>سیاه خیمه لیلی بود دل شب‌ها</p></div>
<div class="b" id="bn16"><div class="m1"><p>ستاره ها به فلک گر چه شکر و شیرند</p></div>
<div class="m2"><p>برای ریختن خون خلق شمشیرند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>همیشه در پی ما اوفتاده چون تیرند</p></div>
<div class="m2"><p>نه روز ثابت سیاره ترک ما گیرند</p></div></div>
<div class="b2" id="bn18"><p>نه شب به خواب روند این گزنده عقرب‌ها</p></div>
<div class="b" id="bn19"><div class="m1"><p>چو سیدا به همه کرده پیروی صایب</p></div>
<div class="m2"><p>نهاده بعد غزل رو به مثنوی صایب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شده به اهل سخن یار معنوی صایب</p></div>
<div class="m2"><p>فتاده تا به ره طرز مولوی صایب</p></div></div>
<div class="b2" id="bn21"><p>سپند شعله فکرش شدست کوکب‌ها</p></div>