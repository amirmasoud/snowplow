---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>به میدان آمدی و گوی و چوگان باختی رفتی</p></div>
<div class="m2"><p>کشیدی تیغ و خونم ریختی و تاختی رفتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از کشتن مرا بر سر علم افراختی رفتی</p></div>
<div class="m2"><p>گذشتی بر مزارم شورشی انداختی رفتی</p></div></div>
<div class="b2" id="bn3"><p>کف خاک مرا صحرای محشر ساختی رفتی</p></div>
<div class="b" id="bn4"><div class="m1"><p>به هوش از بزرگان افزون تری در سال اگر خوردی</p></div>
<div class="m2"><p>به صورت بیژنی با زور بازو رستم گردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به اوج آستانت چون هلالم رخت گستردی</p></div>
<div class="m2"><p>مرا اول به معراج قبول بندگی بردی</p></div></div>
<div class="b2" id="bn6"><p>در آخر همچو نقش پا به خاک انداختی رفتی</p></div>
<div class="b" id="bn7"><div class="m1"><p>به جستجوی تو شبنم صفت از دیده پا کردم</p></div>
<div class="m2"><p>به کویت آمدم از خان و مان خود را جدا کردم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز من دل خواستی در خدمتت جان را فدا کردم</p></div>
<div class="m2"><p>تمام عمر خود را صرفت ای ناآشنا کردم</p></div></div>
<div class="b2" id="bn9"><p>شدی بیگانه و حق وفا نشناختی رفتی</p></div>
<div class="b" id="bn10"><div class="m1"><p>مرا افتاده از عشق تو بر جان آتش سودا</p></div>
<div class="m2"><p>نکردی هیچ چون پروانه بر احوال من پروا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به شاخ شعله مانند سمندر کرده ام مأوا</p></div>
<div class="m2"><p>چو شمع از بی کسی سوختم ایستاده بر یکجا</p></div></div>
<div class="b2" id="bn12"><p>ز تاب آتش غم پیکرم بگداختی رفتی</p></div>
<div class="b" id="bn13"><div class="m1"><p>نمی کردی تو هرگز ناله عشاق را سامع</p></div>
<div class="m2"><p>بود پیوسته از دست نگاهت خنجر قاطع</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگفت سیدا هرگز نگشتی از ستم مانع</p></div>
<div class="m2"><p>جفا کردی جفا کردی نکردی رحم بر جامع</p></div></div>
<div class="b2" id="bn15"><p>فگندی بر زمین و نیم بسمل ساختی رفتی</p></div>