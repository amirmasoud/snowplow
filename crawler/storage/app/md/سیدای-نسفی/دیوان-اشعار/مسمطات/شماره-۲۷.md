---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>ز خون بیگناهان قد شمشیرت دو تا باشد</p></div>
<div class="m2"><p>شهیدان تو را در سینه چون گل چاکها باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خاک کشتگان تا روز محشر این ندا باشد</p></div>
<div class="m2"><p>گریبان چاکی عشاق از ذوق فنا باشد</p></div></div>
<div class="b2" id="bn3"><p>الف در سینه گندم ز شوق آسیا باشد</p></div>
<div class="b" id="bn4"><div class="m1"><p>نمی گردی مرا ای اشک جنت یک نفس همدم</p></div>
<div class="m2"><p>که بر احوال زار خویشتن سازی مرا محرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود چون مهر تابان این سخن مشهور در عالم</p></div>
<div class="m2"><p>به اندک روی گرمی پشت بر گل می نهد شبنم</p></div></div>
<div class="b2" id="bn6"><p>چرا در آشنایی اینقدر کس بی وفا باشد</p></div>
<div class="b" id="bn7"><div class="m1"><p>ز اسباب جهان وارستگی چون سایه پیدا کن</p></div>
<div class="m2"><p>تن خود را به راه خلق چون نقش کف پا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بپوش از هستی خود چشم سیر ملک بالا کن</p></div>
<div class="m2"><p>قدم در چشم خاکی نه سرافرازی تماشا کن</p></div></div>
<div class="b2" id="bn9"><p>به این تل چون برایی آسمان در زیر پا باشد</p></div>
<div class="b" id="bn10"><div class="m1"><p>تعجب نیست از فرهاد کوه بیستون کندن</p></div>
<div class="m2"><p>ندارد تاب تیر ناله عاشق دل آهن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شبی می گفت زلفش از سر بازی به گوش من</p></div>
<div class="m2"><p>به آهی می توان افلاک را زیر و زبر کردن</p></div></div>
<div class="b2" id="bn12"><p>در آن کشور که چاک سینه محراب دعا باشد</p></div>
<div class="b" id="bn13"><div class="m1"><p>شبی چون سیدا رفتم به سیر بوستان صایب</p></div>
<div class="m2"><p>به هر جانب نهادم روی چون آب روان صایب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شنیدم این نوا را از زبان قمریان صایب</p></div>
<div class="m2"><p>توانی سبز شد در حلقه آزادگان صایب</p></div></div>
<div class="b2" id="bn15"><p>تو را چون سرو اگر در چار موسم یک قبا باشد!</p></div>