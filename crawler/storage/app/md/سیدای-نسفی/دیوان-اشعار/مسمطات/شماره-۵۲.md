---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>قامت خم‌گشته را مرگ خبر می‌کند</p></div>
<div class="m2"><p>محمل گل را خزان زیر و زبر می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو جرس مرغ دل زمزمه سر می‌کند</p></div>
<div class="m2"><p>قافله‌سالار ما عزم سفر می‌کند</p></div></div>
<div class="b2" id="bn3"><p>قافله شب گذشت صبح اثر می‌کند</p></div>
<div class="b" id="bn4"><div class="m1"><p>دوش به گلشن مرا ذوق تماشا کشید</p></div>
<div class="m2"><p>رفتم و کردم به شوق تکیه به نخل امید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبنم بی‌دست و پا داد مرا این نوید</p></div>
<div class="m2"><p>هرکه شبی راه رفت صبح به منزل رسید</p></div></div>
<div class="b2" id="bn6"><p>هرکه شبی خواب کرد خاک به سر می‌کند</p></div>
<div class="b" id="bn7"><div class="m1"><p>نیست تو را ذره‌ای شمع صفت تاب و تب</p></div>
<div class="m2"><p>هست به پهلوی تو بستر عیش و طرب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ته سر مانده یی بالش پر روز و شب</p></div>
<div class="m2"><p>خفته چنین بی‌ادب شرم نداری عجب</p></div></div>
<div class="b2" id="bn9"><p>خالق پروردگار با تو نظر می‌کند</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای که تو در چشم خود گوهر ارزنده‌ای</p></div>
<div class="m2"><p>نی به کسی چاکری نی به خدا بنده‌ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کی تو در این روزگار باقی و پاینده‌ای</p></div>
<div class="m2"><p>وقت غنیمت شمار گر نفسی زنده‌ای</p></div></div>
<div class="b2" id="bn12"><p>عمر به مانند باد از تو گذر می‌کند</p></div>
<div class="b" id="bn13"><div class="m1"><p>از دلت ای سیدا نقش جهان را تراش</p></div>
<div class="m2"><p>ناخن الماس جو سینه خود را تراش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چند کنی از غرور تکیه به جوش معاش</p></div>
<div class="m2"><p>خود چو بدین عقل و هوش کم ز فروغی مباش</p></div></div>
<div class="b2" id="bn15"><p>ناله و فریاد بین وقت سحر می‌کند</p></div>