---
title: >-
    شمارهٔ ۷۴ - واسوخت
---
# شمارهٔ ۷۴ - واسوخت

<div class="b" id="bn1"><div class="m1"><p>ای پری چهره لبی چون گل خندان داری</p></div>
<div class="m2"><p>قد چون سرو رخ چون مه تابان داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر خود هوس گشت گلستان داری</p></div>
<div class="m2"><p>جانب غیر ز خط سلسله جنبان داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر جمع مرا چند پریشان داری</p></div>
<div class="m2"><p>مدتی شد که مرا بی سر و سامان داری</p></div></div>
<div class="b2" id="bn4"><p>وقت آنست که لطفی به من زار کنی</p>
<p>نظری جانب این مرغ گرفتار کنی</p></div>
<div class="b" id="bn5"><div class="m1"><p>چند چون گل ز غمت جامه جان چاک زنم</p></div>
<div class="m2"><p>چند آتش به سرا پرده افلاک زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پا به دامن کشم و بر سر خود خاک زنم</p></div>
<div class="m2"><p>سنگ بر دارم و بر سینه غمناک زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ از دست تو بر جان هوسناک زنم</p></div>
<div class="m2"><p>آتش افروزم و بر دیده نمناک زنم</p></div></div>
<div class="b2" id="bn8"><p>از غمت من به چنین حال و تو یار دگران</p>
<p>کار من رفته ز دست تو به کار دگران</p></div>
<div class="b" id="bn9"><div class="m1"><p>بس که سودا زده زلف دو تای تو منم</p></div>
<div class="m2"><p>با قد خم شده در دام بلای تو منم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همچو خورشید فلک کاسه گدای تو منم</p></div>
<div class="m2"><p>چون شفق کشته شمشیر جفای تو منم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دست برداشته از بهر دعای تو منم</p></div>
<div class="m2"><p>راست گویم سبب نشو نمای تو منم</p></div></div>
<div class="b2" id="bn12"><p>آنکه هرگز به سر عهد و وفا نیست تویی</p>
<p>آنکه اندیشه اش از روز جزا نیست تویی</p></div>
<div class="b" id="bn13"><div class="m1"><p>ناله اهل غرض ای مه من گوش مکن</p></div>
<div class="m2"><p>سایه خویش به این قوم هماغوش مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باده خیره نگاهان هوس نوش مکن</p></div>
<div class="m2"><p>با چنین طایفه چون شیشه می جوش مکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نرگس خویش تو از سرمه سیه پوش مکن</p></div>
<div class="m2"><p>غیر را دیده مرا باز فراموش مکن</p></div></div>
<div class="b2" id="bn16"><p>هیچ کس همچو منت عاشق صادق نبود</p>
<p>از تو معشوق شدن با همه لایق نبود</p></div>
<div class="b" id="bn17"><div class="m1"><p>مروی بر تافتن و ناز تو را بنده شوم</p></div>
<div class="m2"><p>جلوه سرو سرافراز تو را بنده شوم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیوه نرگس غماز تو را بنده شوم</p></div>
<div class="m2"><p>نگه چپ غلط انداز تو را بنده شوم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ناخن چنگل شهباز تو را بنده شوم</p></div>
<div class="m2"><p>چهره آئینه پرداز تو را بنده شوم</p></div></div>
<div class="b2" id="bn20"><p>یک شب ای ماه جبین سوی من آیی چه شود</p>
<p>بر رخ من دری از غیب کشایی چه شود</p></div>
<div class="b" id="bn21"><div class="m1"><p>غیر را شمع شبستان شده‌ای حیف از تو</p></div>
<div class="m2"><p>یار این جمع پریشان شده‌ای حیف از تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سرو بیرون گلستان شده‌ای حیف از تو</p></div>
<div class="m2"><p>با خزان دست گریبان شده‌ای حیف از تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خط برآورده و حیران شده‌ای حیف از تو</p></div>
<div class="m2"><p>این زمان بی‌سر و سامان شده‌ای حیف از تو</p></div></div>
<div class="b2" id="bn24"><p>آنچنان باش که کس را به تو حجت نشود</p>
<p>دامن پاک تو آلوده تهمت نشود</p></div>
<div class="b" id="bn25"><div class="m1"><p>روشن از روی تو گردیده چراغ دگران</p></div>
<div class="m2"><p>شده یی مرهم کافوریی داغ دگران</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از رخت رنگ گرفته گل باغ دگران</p></div>
<div class="m2"><p>نکهت زلف تو پیوسته دماغ دگران</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مست و سرخوش شده چشمت ز ایاغ دگران</p></div>
<div class="m2"><p>هست پیوسته نگاهت به سراغ دگران</p></div></div>
<div class="b2" id="bn28"><p>از من ای برق جهان سوز چه می پرهیزی</p>
<p>سوختم سوختم امروز چه می پرهیزی</p></div>
<div class="b" id="bn29"><div class="m1"><p>پیش از آن دم که ز خط حسن تو زایل گردد</p></div>
<div class="m2"><p>غمزه ات هر طرفی در طلب دل گردد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>لشکر شام به صبح تو مقابل گردد</p></div>
<div class="m2"><p>کار با سلسله زلف تو مشکل گردد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>لب شکر شکنت زهر هلاهل گردد</p></div>
<div class="m2"><p>پایت از آبله سد ره منزل گردد</p></div></div>
<div class="b2" id="bn32"><p>آن دم ای سیم بدن یار تو من خواهم بود</p>
<p>رحم کن رحم که غمخوار تو من خواهم بود</p></div>
<div class="b" id="bn33"><div class="m1"><p>از سر کوی تو امروز سفر خواهم کرد</p></div>
<div class="m2"><p>سود خود صرف به سودای دگر خواهم کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ابروی سیمبری مد نظر خواهم کرد</p></div>
<div class="m2"><p>شکوه از زلف پریشان تو سر خواهم کرد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دامن خویش پر از خون جگر خواهم کرد</p></div>
<div class="m2"><p>از گلستان تو چون آب گذر خواهم کرد</p></div></div>
<div class="b2" id="bn36"><p>در پی سرو تو چون سایه دویدن تا کی</p>
<p>وز لب لعل تو حرفی نشنیدن تا کی</p></div>
<div class="b" id="bn37"><div class="m1"><p>از پریشان نظری پا نکشیدی هرگز</p></div>
<div class="m2"><p>سر از این شعله سودا نکشیدی هرگز</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گردن از بزم چو مینا نکشیدی هرگز</p></div>
<div class="m2"><p>دامن از خار تمنا نکشیدی هرگز</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یوسفی درد زلیخا نکشیدی هرگز</p></div>
<div class="m2"><p>غم بیداریی شب‌ها نکشیدی هرگز</p></div></div>
<div class="b2" id="bn40"><p>از اسیران تو چه دانی که چه‌ها می‌گذرد</p>
<p>کوه بی‌تاب شود آنچه به ما می‌گذرد</p></div>
<div class="b" id="bn41"><div class="m1"><p>از من ای شوخ مکدر شده‌ای دانستم</p></div>
<div class="m2"><p>یار با مردم دیگر شده‌ای دانستم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شوخ و بی‌باک و ستمگر شده‌ای دانستم</p></div>
<div class="m2"><p>شعله جان سمندر شده‌ای دانستم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>مایل باده و ساغر شده‌ای دانستم</p></div>
<div class="m2"><p>طالب کیسه پر زر شده‌ای دانستم</p></div></div>
<div class="b2" id="bn44"><p>کاش چون غنچه مرا مشت زری می بودی</p>
<p>تا تو را با من دیوانه سری می بودی</p></div>
<div class="b" id="bn45"><div class="m1"><p>ای خوش آن روز که هر سو نگرانت بینم</p></div>
<div class="m2"><p>خار در پا و چو گل جامه درانت بینم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>زیر مشق نظر کج نظرانت بینم</p></div>
<div class="m2"><p>روی گردان شده یی بی بصرانت بینم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون گل افتاده به چشم دگرانت بینم</p></div>
<div class="m2"><p>آخر دولت حسن گذرانت بینم</p></div></div>
<div class="b2" id="bn48"><p>خویش را زود تو بی برگ و نوا خواهی کرد</p>
<p>آن زمان یاد من بی سر و پا خواهی کرد</p></div>
<div class="b" id="bn49"><div class="m1"><p>مدتی بر سر کوی تو دویدیم بس است</p></div>
<div class="m2"><p>خاک پایت به سر و دیده کشیدیم بس است</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چون گل از دست غمت جامه دریدیم بس است</p></div>
<div class="m2"><p>گفتگوها ز برای تو شنیدیم بس است</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>قطره ای از می وصل تو چشیدیم بس است</p></div>
<div class="m2"><p>چند روزی به وصال تو رسیدیم بس است</p></div></div>
<div class="b2" id="bn52"><p>بعد از این ما و دل و دامن یار دگری</p>
<p>تو و جام می اندیشه کار دگری</p></div>
<div class="b" id="bn53"><div class="m1"><p>چند چون زلف خود ای شوخ پریشان باشی</p></div>
<div class="m2"><p>شعله جان من بی سر و سامان باشی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بهر خون ریختم تیر چو مژگان باشی</p></div>
<div class="m2"><p>تیغ خون ریز به کف بر زده دامان باشی</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>همره غیر تو همچون گل خندان باشی</p></div>
<div class="m2"><p>چیست باعث که به من دست و گریبان باشی</p></div></div>
<div class="b2" id="bn56"><p>سیدا بهر خود امروز زری پیدا کن</p>
<p>یا نشین کنج غم و گوش کری پیدا کن</p></div>