---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>شد مدتی که بخت نسازد مدد را</p></div>
<div class="m2"><p>افگنده روزگار به فکر ابد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سینه نیست کینه ز اهل حد مرا</p></div>
<div class="m2"><p>غمگین نیم که خلق شمارند بد مرا</p></div></div>
<div class="b2" id="bn3"><p>نزدیک می کند به خدا دست رد مرا</p></div>
<div class="b" id="bn4"><div class="m1"><p>تا ناله از لب من مخمور شد بلند</p></div>
<div class="m2"><p>از شیشه ها نوای دم صور شد بلند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امشب ز من ترانه مقصود شد بلند</p></div>
<div class="m2"><p>کیفیتم ز باده انگور شد بلند</p></div></div>
<div class="b2" id="bn6"><p>چندان که زد به فرق حوادث لگد مرا</p></div>
<div class="b" id="bn7"><div class="m1"><p>خون می چکد چو مرغ کباب از نظاره ام</p></div>
<div class="m2"><p>شرمنده است پرتو مه از ستاره ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردیده آب زهره برق از شراره ام</p></div>
<div class="m2"><p>چون لعل اگر چه در جگر سنگ خاره ام</p></div></div>
<div class="b2" id="bn9"><p>از نور آفتاب مدد می رسد مرا</p></div>
<div class="b" id="bn10"><div class="m1"><p>خلوت نشین میکده را پاره شد لباس</p></div>
<div class="m2"><p>دایم تهیست خانه آزاده از پلاس</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از خانقاه شیخ برآمد به صد اساس</p></div>
<div class="m2"><p>شد جوش خلق پرده چشم خداشناس</p></div></div>
<div class="b2" id="bn12"><p>غافل ز بحر کرد هجوم زبد مرا</p></div>
<div class="b" id="bn13"><div class="m1"><p>تا بسته ام به فکر سخن سیدا میان</p></div>
<div class="m2"><p>چون شمع انجمن شده ام آتشین زبان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داد امتیاز شعر مرا کلک نکته دان</p></div>
<div class="m2"><p>صایب میان تازه خیالان اصفهان</p></div></div>
<div class="b2" id="bn15"><p>بس باشد این غزل گل روی سبد مرا</p></div>