---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>در چمن امروز بلبل مست گفتار خود است</p></div>
<div class="m2"><p>کبک در کهسارها پابند رفتار خود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو مغرور قد و گل محو رخسار خود است</p></div>
<div class="m2"><p>هر که را بینم در عالم گرفتار خود است</p></div></div>
<div class="b2" id="bn3"><p>کار حق در طاق نسیان مانده در کار خود است</p></div>
<div class="b" id="bn4"><div class="m1"><p>دل درون سینه ام در آتش غم در گرفت</p></div>
<div class="m2"><p>ناله جانسوز من آفاق را در بر گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ نتواند مرا از زیر خاکستر گرفت</p></div>
<div class="m2"><p>کیست از دوش کسی باری تواند بر گرفت</p></div></div>
<div class="b2" id="bn6"><p>گر همه عیسی است در فکر خر و بار خود است</p></div>
<div class="b" id="bn7"><div class="m1"><p>از شکست شیشه غم در خاطر پیمانه نیست</p></div>
<div class="m2"><p>خانه فانوس را پروای صاحب خانه نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زندگانی آشنایان را به هم یارانه نیست</p></div>
<div class="m2"><p>گریه شمع از برای ماتم پروانه نیست</p></div></div>
<div class="b2" id="bn9"><p>صبح نزدیک است در فکر شب کار خود است</p></div>
<div class="b" id="bn10"><div class="m1"><p>روزگاری شد که در ویرانه‌ای هستم مقیم</p></div>
<div class="m2"><p>می کنم پیوسته یاد از عهد یاران قدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این ندا آمد برون از خاک موسی کلیم</p></div>
<div class="m2"><p>خضر آسودست از تعمیر دیوار یتیم</p></div></div>
<div class="b2" id="bn12"><p>هر کسی را روی در تعمیر دیوار خود است</p></div>
<div class="b" id="bn13"><div class="m1"><p>خانه بر دوشیم ما را حاجت دستار نیست</p></div>
<div class="m2"><p>پیکر ما را لباس تازه‌ای در کار نیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیدا ما را نظر بر دست دنیا دار نیست</p></div>
<div class="m2"><p>چشم صایب چون صدف بر ابر گوهر بار نیست</p></div></div>
<div class="b2" id="bn15"><p>زیر بار منت طبع گهر بار خود است</p></div>