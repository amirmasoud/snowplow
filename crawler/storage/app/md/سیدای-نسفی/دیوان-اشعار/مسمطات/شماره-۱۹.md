---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>به دل تا در سخن آورده بودم لعل نابش را</p></div>
<div class="m2"><p>به خود پیوسته می خوردم چو می زهر عتابش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهان می داشتم از چشم شبنم آفتابش را</p></div>
<div class="m2"><p>گل اندامی که می دادم به خون دیده آبش را</p></div></div>
<div class="b2" id="bn3"><p>چه سان بینم که آخر دیگری گیرد گلابش را</p></div>
<div class="b" id="bn4"><div class="m1"><p>به یاد سرو قد او فنا شد رسم و آئینم</p></div>
<div class="m2"><p>تمنای نگاه او برون آورد از دینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نرگس به که سر در پای او افگنده بنشینم</p></div>
<div class="m2"><p>در آغوش نسیم صبحدم بی پرده چون بینم</p></div></div>
<div class="b2" id="bn6"><p>گل رویی که من واکرده ام بند نقابش را</p></div>
<div class="b" id="bn7"><div class="m1"><p>نهال قامتش در خانه زین کرده مأوایی</p></div>
<div class="m2"><p>پی پابوس او اهل هوس دارند غوغایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حال من کنید ای همدمان امروز پروایی</p></div>
<div class="m2"><p>به دست غیر چون بینم عنان طفل خودرایی</p></div></div>
<div class="b2" id="bn9"><p>که وقت نی سواری می گرفتم من رکابش را</p></div>
<div class="b" id="bn10"><div class="m1"><p>نمی آید برون از صفحه آشوب تا هستش</p></div>
<div class="m2"><p>به مشق فتنه کرده کاتب ایام پابستش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برای قتلم انشا کرد خطی نرگس مستش</p></div>
<div class="m2"><p>به خونم زد رقم تا با قلم شد آشنا دستش</p></div></div>
<div class="b2" id="bn12"><p>پری رویی که می بردم به مکتب من کتابش را</p></div>
<div class="b" id="bn13"><div class="m1"><p>چرا ایستاده یی ای سیدا چون سرو پا در گل</p></div>
<div class="m2"><p>درین گلزار نتوان ساختن یک ساعتی منزل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کرا افتاده در باغ جهان این قصه مشکل</p></div>
<div class="m2"><p>نهالی را که من چون تاک پروردم به خون دل</p></div></div>
<div class="b2" id="bn15"><p>چه سان بینم به کام دیگران صایب شرابش را</p></div>