---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>اگر چو غنچه کف زر نگار داشتمی</p></div>
<div class="m2"><p>به هر طرف ز ثناگو هزار داشتمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باغ دهر چو بو اعتبار داشتمی</p></div>
<div class="m2"><p>زبان شکوه اگر همچو خار داشتمی</p></div></div>
<div class="b2" id="bn3"><p>همیشه خرمن گل در کنار داشتمی</p></div>
<div class="b" id="bn4"><div class="m1"><p>نمی توان به کف آورد وقت خوش با جهد</p></div>
<div class="m2"><p>به زهر پرورشم داده دایه ام در مهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ضرب نیش من تلخکام در این عهد</p></div>
<div class="m2"><p>هزار خانه چو زنبور کردی پر شهد</p></div></div>
<div class="b2" id="bn6"><p>اگر گزیدن مردم شعار داشتمی</p></div>
<div class="b" id="bn7"><div class="m1"><p>چو شمع بزم به سر برده ام بسی شب را</p></div>
<div class="m2"><p>ندیده دیده من هیچ روی مطلب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گرفتی به سر انگشت مهر منصب را</p></div>
<div class="m2"><p>ز دست راست ندانستمی اگر چپ را</p></div></div>
<div class="b2" id="bn9"><p>چه گنج ها به یمین و یسار داشتمی</p></div>
<div class="b" id="bn10"><div class="m1"><p>در این زمانه اگر خاک پا نمی گشتم</p></div>
<div class="m2"><p>به چشم اهل نظر توتیا نمی گشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به گرد کوی بتان چون صبا نمی گشتم</p></div>
<div class="m2"><p>به درد عشق اگر مبتلا نمی گشتم</p></div></div>
<div class="b2" id="bn12"><p>چه دلخوشی من از این روزگار داشتمی</p></div>
<div class="b" id="bn13"><div class="m1"><p>چو سیدا به جهان پافشردمی صایب</p></div>
<div class="m2"><p>رساله را به حریفان سپردمی صایب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز پشت آئینه خود را شمردی صایب</p></div>
<div class="m2"><p>به عیب خویش اگر راه بردمی صایب</p></div></div>
<div class="b2" id="bn15"><p>به عیب جویی مردم چه کار داشتمی!</p></div>