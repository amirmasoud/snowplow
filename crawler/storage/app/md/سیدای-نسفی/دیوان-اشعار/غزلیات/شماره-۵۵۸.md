---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>قدت در چشم خود جا کرده ام ای دوست می رنجی</p></div>
<div class="m2"><p>نهالت گفته ام سرو لب این جوست می رنجی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد از نظاره رخسار تو گلدسته مژگانم</p></div>
<div class="m2"><p>نگاهم مارپیچ طاق این ابروست می رنجی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به فکر روی تو عمریست من پشت خمی دارم</p></div>
<div class="m2"><p>سرم چون غنچه در آئینه زانوست می رنجی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا جرمی نباشد آه من غیر از گرفتاری</p></div>
<div class="m2"><p>کمند گردن من حلقه آن موست می رنجی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حال سیدا عمریست پروایی نمی سازی</p></div>
<div class="m2"><p>بیابان گرد از دست تو چون آهوست می رنجی</p></div></div>