---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>چشم یارم خواب و سنگین است بیداری مرا</p></div>
<div class="m2"><p>می کشم داروی بیهوشی است هشیاری مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوی پیراهن به کنعان رفت پیش از کاروان</p></div>
<div class="m2"><p>جا دهد در دیده منزل سبکباری مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ندامت پشت دستم گر چه روی دست شد</p></div>
<div class="m2"><p>بر کف ایستادست دامان گنه کاری مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خرقه من چون گریبان از گلوی من گرفت</p></div>
<div class="m2"><p>شد ردا آخر به گردن فوطه زاری مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خیال چشم او هر جا که منزل می کنم</p></div>
<div class="m2"><p>نیست دور از زیر سر بالین بیماری مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که را بینم به نفس خویش دارد بندگی</p></div>
<div class="m2"><p>کیست می سازد درین کشور خریداری مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به از می کردم و یاد جوانی می کنم</p></div>
<div class="m2"><p>در خیالات محال افگند بیکاری مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا زدم در حلقه این زاهدان دست نیاز</p></div>
<div class="m2"><p>رشته تسبیح من شد دام طراری مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شاخ و برگم را غم افتادگان افتاده کرد</p></div>
<div class="m2"><p>اره یی بودم که سوهان کرد همواری مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیدن خورشید را مانع نباشد هیچ کس</p></div>
<div class="m2"><p>از جوانان خوش بود معشوق بازاری مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با فش و مسواک زاهد را به کوی خویش دید</p></div>
<div class="m2"><p>خنده زد گفتا سری نبود به دستاری مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح حشر از روی کارم پرده خواهد برگرفت</p></div>
<div class="m2"><p>گر نسازد دامن عفو تو ستاری مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یوسف از زندان برون آمد عزیز مصر شد</p></div>
<div class="m2"><p>مژده امیدواری ها بود خواری مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سیدا در فکر خوبان استخوانم شد سفید</p></div>
<div class="m2"><p>کو جوانی کاندرین پیری دهد یاری مرا</p></div></div>