---
title: >-
    شمارهٔ ۴۷۸
---
# شمارهٔ ۴۷۸

<div class="b" id="bn1"><div class="m1"><p>ای سرمه صید کشته چشم سیاه تو</p></div>
<div class="m2"><p>باشد کمند گردن آهو نگاه تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیلی زند خرام تو موج سراب را</p></div>
<div class="m2"><p>صیاد را فریب دهد جلوه گاه تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دست برد شبنم آفت منزه است</p></div>
<div class="m2"><p>چون برگ غنچه دامن عصمت پناه تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمریست همچو چشم گدا کوچه باغها</p></div>
<div class="m2"><p>ایستاده اند منتظر گرد راه تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>طفلان بی پدر هوس تاج زر کنند</p></div>
<div class="m2"><p>دل بسته است غنچه بطرف کلاه تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که انتظار هلاکم چو سیدا</p></div>
<div class="m2"><p>چشمم شدست جوهر تیغ نگاه تو</p></div></div>