---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>کوه را افغانم آتش در جگر می افگند</p></div>
<div class="m2"><p>بحر را اشکم به گرداب خطر می افگند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جود نیسان را چه باشد آبرو پیش صدف</p></div>
<div class="m2"><p>در عوض هر قطره یی او را گهر می افگند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلربایان می کنند از یکدگر کسب هنر</p></div>
<div class="m2"><p>گل ز طفلی غنچه را در فکر زر می افگند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه میخانه را زاهد به چشم کم مبین</p></div>
<div class="m2"><p>هر که آنجا پا نهد او را بسر می افگند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بد و نیک امتیازی نیست خورشید مرا</p></div>
<div class="m2"><p>پرتو خود را به هر دیوار و در می افگند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ما خو کرده چون یعقوب بر رخسار دوست</p></div>
<div class="m2"><p>کور گردد هر که ما را از نظر می افگند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا گر از لبش گویم حدیثی در چمن</p></div>
<div class="m2"><p>غنچه از گل پیش روی خود سپر می افگند</p></div></div>