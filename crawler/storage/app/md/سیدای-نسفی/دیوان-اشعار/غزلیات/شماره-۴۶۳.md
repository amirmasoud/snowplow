---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>به دریا کرده ام خو موج آبم می توان گفتن</p></div>
<div class="m2"><p>به کف دارم سر بی تن حبابم می توان گفتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شاخ شعله دارم آشیان مانند پروانه</p></div>
<div class="m2"><p>سمندر طینتم مرغ کبابم می توان گفتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به باغ دهر چون شبنم ندارم خواب آسایش</p></div>
<div class="m2"><p>چو اهل کاروان پا در رکابم می توان گفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مردم می نمایم خویش را و لیک نابودم</p></div>
<div class="m2"><p>در این صحرای بی پایان سرابم می توان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لباس فقر و طبع روشنی ای سیدا دارم</p></div>
<div class="m2"><p>به زیر ابر پنهان آفتابم می توان گفتن</p></div></div>