---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>روزی که صبح حشر کند رو به یک طرف</p></div>
<div class="m2"><p>گردد بهشت یک طرف آنکو به یک طرف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنجند اگر به روز قیامت گناه من</p></div>
<div class="m2"><p>افتد ز بار جم ترازو به یک طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شوق تو مست کرده چنان اهل باغ را</p></div>
<div class="m2"><p>افتاده گل به یک طرف و بو به یک طرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون شانه پاره پاره دلم گر شود رواست</p></div>
<div class="m2"><p>خط یک طرف کشیده و گیسو به یک طرف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمی که چون نگاه به هر سو می دوید</p></div>
<div class="m2"><p>امروز کرده گوشه ابرو به یک طرف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون سیدا اگر چه بهر سوی می تپم</p></div>
<div class="m2"><p>آخر نهم ز درد تو پهلو به یک طرف</p></div></div>