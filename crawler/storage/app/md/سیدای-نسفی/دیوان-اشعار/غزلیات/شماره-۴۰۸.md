---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>امشب از مستی به پای خم چو خشت افتاده ام</p></div>
<div class="m2"><p>عشرتی دارم که گویا در بهشت افتاده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از پس آئینه می کردم تماشا عکس را</p></div>
<div class="m2"><p>بس که دور از امتیاز خوب و زشت افتاده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از توکل میر سید از غیب رزقم بی حساب</p></div>
<div class="m2"><p>روزیم شد تنگ تا در فکر کشت افتاده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه ام در فکر آن نقاش شد بتخانه یی</p></div>
<div class="m2"><p>دیده ام تا صورت او در کنشت افتاده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه پایم کرده بود امروز دادندش جزا</p></div>
<div class="m2"><p>سیدا اکنون به فکر سرنوشت افتاده ام</p></div></div>