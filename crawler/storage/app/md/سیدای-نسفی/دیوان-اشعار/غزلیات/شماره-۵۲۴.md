---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>هر کوچه باغ دیده ام ای گل خوش آمدی</p></div>
<div class="m2"><p>جوش بهار خانه بلبل خوش آمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خون عاشقان شده کوی تو لاله زار</p></div>
<div class="m2"><p>ای ارغوان ز کشور کابل خوش آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان می دهیم و نیم نگاهی نمی کنی</p></div>
<div class="m2"><p>بر کف گرفته تیغ تغافل خوش آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دیدن تو گشت پریشانیم زیاد</p></div>
<div class="m2"><p>ای شانه بهر پرسش کاکل خوش آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجینه خانه تو بود چشم سیدا</p></div>
<div class="m2"><p>پیچیده رخت خویش چو سنبل خوش آمدی</p></div></div>