---
title: >-
    شمارهٔ ۴۱۷
---
# شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>چون گل تمام داغم و خرم نشسته ام</p></div>
<div class="m2"><p>بر روی زخم خویش چو مرهم نشسته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمریست از هوا و هوس چشم بسته ام</p></div>
<div class="m2"><p>چون غنچه فارغ از غم عالم نشسته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پوشیده ام چو خامه لباس سیاه را</p></div>
<div class="m2"><p>در مرگ اهل هوش بمانم نشسته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>انگشت تر نکرده ام از بزم اهل جود</p></div>
<div class="m2"><p>سیلی زنان به سفره حاتم نشسته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر ز دست مهر گل بی مروتی</p></div>
<div class="m2"><p>حیران به روی باغ چو شبنم نشسته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانند نفس به لب من گره شدست</p></div>
<div class="m2"><p>با اهل روزگار چو یک دم نشسته ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سیدا ز سهو به بزمی که رفته ام</p></div>
<div class="m2"><p>از اشک خود به سلسله غم نشسته ام</p></div></div>