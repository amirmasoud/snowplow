---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>بهر روزی آسمان کردست سرگردان مرا</p></div>
<div class="m2"><p>مور لنگم نیست امیدی ازین دهقان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از متاعم دامن افشان بگذرد نظاره گر</p></div>
<div class="m2"><p>بس که از گرد کسادی پر بود دکان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قسمت من چون هما نبود به غیر از استخوان</p></div>
<div class="m2"><p>کرده این روزی خلاص از منت دوران مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می گذارم چون چراغ و آب می گردم چو شمع</p></div>
<div class="m2"><p>هر که چون پروانه می گردد شبی مهمان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا لب نانی چو گندم روز بی من کرده اند</p></div>
<div class="m2"><p>در گریبان چاک ها افتاده تا دامان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تبسم پسته را مغز سرآمد از دهان</p></div>
<div class="m2"><p>وقف دندان ندامت شد لب خندان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغها چون لاله از اعضای من گل کرده است</p></div>
<div class="m2"><p>چاک پیراهن نماید رخنه بستان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر روم بهر تماشا سوی گلشن چون نسیم</p></div>
<div class="m2"><p>غنچه هم چون بوی سازد در بغل پنهان مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گشته ام خلوت نشین از تهمت عریان تنی</p></div>
<div class="m2"><p>چاک پیراهن چو یوسف کرده در زندان مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چون فلاخون بسته ام سنگ ملامت بر شکم</p></div>
<div class="m2"><p>کرده مشرف بر سر دیوانگان دوران مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می رود ای سیدا از دیده ام دریای خون</p></div>
<div class="m2"><p>خار مژگان می نماید پنجه مرجان مرا</p></div></div>