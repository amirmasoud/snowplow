---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>تیره بختی گفت در گوش خطش راز مرا</p></div>
<div class="m2"><p>کرد روشن عاقبت این سرمه آواز مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیر از خود رفتن من می دهد بوی وداع</p></div>
<div class="m2"><p>کرده اند از نکهت گل بال پرواز مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تهمت نقصان پر و بال مرا از کو تهیست</p></div>
<div class="m2"><p>کرده است آسودگی مقراض پرداز مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دولت دنیا دل آسوده ام را نقش پاست</p></div>
<div class="m2"><p>سایه بال هما صید است شهباز مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از چراغ خانه ام بزم حریفان در گرفت</p></div>
<div class="m2"><p>چرب و نرمی شد زبان شکوه غماز مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی رخت در انجمن چون مرده پروانه ام</p></div>
<div class="m2"><p>شمع خواهد با تو گفت انجام و آغاز مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا امروز کلکم اژدهایی می کند</p></div>
<div class="m2"><p>خصم نادان می شمارد سحر اعجاز مرا</p></div></div>