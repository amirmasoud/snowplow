---
title: >-
    شمارهٔ ۱۵۳
---
# شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>بیا به کلبه ام ای ماهرو صفا اینجاست</p></div>
<div class="m2"><p>نشین به دیده ام ای نور چشم جا اینجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیارت دل ما ساز و سیر جنت کن</p></div>
<div class="m2"><p>کلید قفل در باغ دلگشا اینجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز داغ توست مرا سینه روضه شهدا</p></div>
<div class="m2"><p>به خنجر تو قسم دشت کربلا اینجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا زبان نگاه تو یار حرف شده</p></div>
<div class="m2"><p>کجا روم که سخنهای آشنا اینجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کویت آمدم و گشت چشم من روشن</p></div>
<div class="m2"><p>قسم به خاک قدومت که توتیا اینجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنای کعبه بود دل به دست آوردن</p></div>
<div class="m2"><p>اگر خداطلبی می کنی خدا اینجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گریه گفتمش آب حیات می جویم</p></div>
<div class="m2"><p>به خنده گفت لبش چشمه بقا اینجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رقیب خواست که امشب به بزم بنشیند</p></div>
<div class="m2"><p>اشاره کرد به ابرو که سیدا اینجاست</p></div></div>