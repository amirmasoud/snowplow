---
title: >-
    شمارهٔ ۲۵۳
---
# شمارهٔ ۲۵۳

<div class="b" id="bn1"><div class="m1"><p>چند روزی در محبت درد می باید کشید</p></div>
<div class="m2"><p>زردروئی ها ز رنگ زرد می باید کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله های گرم را از بس که تأثیری نماند</p></div>
<div class="m2"><p>بعد از این از سینه آه سرد می باید کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوالهوس را سر به تیغ امتحان باید برید</p></div>
<div class="m2"><p>انتقام از دشمن نامردمی باید کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از آن ساعت که خاک را دهد دوران بنیاد</p></div>
<div class="m2"><p>از بنای هستی خود گردمی باید کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برگ کاهی از کف نامرد بار عالم است</p></div>
<div class="m2"><p>کوه اگر منت شود از مردمی باید کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیدا امروز از بالا بلندان چمن</p></div>
<div class="m2"><p>دامن آن سرو یکتا گرد می باید کشید</p></div></div>