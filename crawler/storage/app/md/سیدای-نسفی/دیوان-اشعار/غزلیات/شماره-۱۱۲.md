---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>بر گلویم تیغ خون افشان چو آب کوثر است</p></div>
<div class="m2"><p>داغ سودا بر سر من آفتاب محشر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تابش خورشید و مه از پرتو رخسار اوست</p></div>
<div class="m2"><p>جبهه نورانی آئینه از روشنگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست خوبان را به جز آغوش عاشق جای امن</p></div>
<div class="m2"><p>سرو قمری را چو طفلی در کنار مادر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخر از هنگامه ایام می باید گذشت</p></div>
<div class="m2"><p>شمع را دایم از این اندیشه آتش بر سر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پهلوی خود وقف خورشید قیامت می کند</p></div>
<div class="m2"><p>هر که را امروز همچون شبنم از گل بستر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوکب آسایش من نیست در هفت آسمان</p></div>
<div class="m2"><p>سرنوشت خود ندانم در کدامین دفتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی رخ آن سبز خط گر جانب بستان روم</p></div>
<div class="m2"><p>سبزه و گل پش چشمم آتش و خاکستر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل چرا بندد کسی بر هستی خود سیدا</p></div>
<div class="m2"><p>شمع ما آزادگان در رهگذار صرصر است</p></div></div>