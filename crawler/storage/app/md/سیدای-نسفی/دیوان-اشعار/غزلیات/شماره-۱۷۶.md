---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>بر سرم روزی که از زلف تو سودا گل کند</p></div>
<div class="m2"><p>حلقه زنجیر را در پای من سنبل کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهربانی می کند هر کس شکستم می دهد</p></div>
<div class="m2"><p>بر سر من پا گذارد جای چون کاکل کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جوار بزرگان یابند خوردان تربیت</p></div>
<div class="m2"><p>بید مجنون در گلستان سایه بر سنبل کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرط معشوقی دل عاشق به دست آوردن است</p></div>
<div class="m2"><p>سینه را گل وا برای خاطر بلبل کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دست او از شانه می سازم جدا با زور آه</p></div>
<div class="m2"><p>بعد از این مشاطه گر بازی به آن کاکل کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیدا معشوق من از بس که عالی مشرب است</p></div>
<div class="m2"><p>مهربانی بیشتر با عاشق بی پل کند</p></div></div>