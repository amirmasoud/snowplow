---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>سایه از بی تابیم سیلی زند مهتاب را</p></div>
<div class="m2"><p>اضطرابم جان درآرد کشته سیماب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می شود خصم قوی از چرب و نرمی زیر دست</p></div>
<div class="m2"><p>می کند پامال خود یک قطره روغن آب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر ضعیفان بیشتر می آید از دوران شکست</p></div>
<div class="m2"><p>موج پندارد گلوی آسیا گرداب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تیغ استاد مکمل باشد از نقصان تهی</p></div>
<div class="m2"><p>نیست بر شمشیر ابرو دست پیچ و تاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زور بازو را چه نسبت پیش عقل حیله گر</p></div>
<div class="m2"><p>رستم از تدبیر زد بر خاک و خون سهراب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیک و بد را از حوادث های دوران چاره نیست</p></div>
<div class="m2"><p>کی تواند سد ره شد خار و خس سیلاب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از تواضع قبله عالم توان شد سیدا</p></div>
<div class="m2"><p>می کند پشت دو تا مد نظر محراب را</p></div></div>