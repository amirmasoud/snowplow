---
title: >-
    شمارهٔ ۱۴۳
---
# شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>شمعم و پیوسته در رگهای جانم آتش است</p></div>
<div class="m2"><p>در دهانم موج آب و بر زبانم آتش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظالمان از بی کسی ویرانه ام را سوختند</p></div>
<div class="m2"><p>جغدم از بی خان و مانی آشیانم آتش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبلم اما مقام دلنشینم گلخن است</p></div>
<div class="m2"><p>سبزه ام خاکستر است و بوستانم آتش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر کوی بتان رخت سفر تا بسته ام</p></div>
<div class="m2"><p>محمل من گرد باد و کاروانم آتش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که با من می کنی سودا به خود اندیشه کن</p></div>
<div class="m2"><p>در بساطم دود آه و بر دکانم آتش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبت من بی می و بی مطرب امشب در گرفت</p></div>
<div class="m2"><p>خانه روشن می کنم تا میهمانم آتش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست دلسوزی به ملک سینه من غیر داغ</p></div>
<div class="m2"><p>سیدا امروز یار مهربانم آتش است</p></div></div>