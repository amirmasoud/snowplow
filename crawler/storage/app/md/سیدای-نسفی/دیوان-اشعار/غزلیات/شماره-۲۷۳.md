---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>شنیدم زین دیار آن گل بسر عزم سفر دارد</p></div>
<div class="m2"><p>مرا همچون نسیم ناامیدی در بدر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی دانم که بی او حال زار من چه خواهد شد</p></div>
<div class="m2"><p>فلک در کار من امروز آهنگ دگر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وداعش می کنم شاید سرم در پیش زین بندد</p></div>
<div class="m2"><p>مرصع خنجری بر دست تیغی بر کمر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دوش کاروان ناله بستم رخت هستی را</p></div>
<div class="m2"><p>ز دنبالش رود هر کس که داغی بر جگر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده تعلیم خاکسترنشینی با من ای آتش</p></div>
<div class="m2"><p>که آن طوطی سخن سودای هندوستان بسر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگر بلبل به گلشن خیربادش کرده می آید</p></div>
<div class="m2"><p>که گل با خون دل رو شسته شبنم چشم تر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل از خود رفته و می جوید از مردم سراغش را</p></div>
<div class="m2"><p>خبر می پرسد از دلدار و از خود کی خبر دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روم ای سیدا مانند نقش پا ز دنبالش</p></div>
<div class="m2"><p>مرا چون سرمه شاید چشم او از خاک بردارد</p></div></div>