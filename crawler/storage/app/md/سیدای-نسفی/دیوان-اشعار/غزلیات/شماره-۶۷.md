---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>خدایا تازه کن چون شمع مغز استخوانم را</p></div>
<div class="m2"><p>توانایی کرم فرمای جسم ناتوانم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهالم را خزان کردست ایام کهنسالی</p></div>
<div class="m2"><p>شکفتن ها کرامت ساز شاخ ارغوانم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قد خم گشته یی دارم الهی دستگیری کن</p></div>
<div class="m2"><p>نگه دار از کشاکش های بازوها کمانم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پی مدح و ثنایت چون قلم عمریست سرگرمم</p></div>
<div class="m2"><p>مشوی از حفظ نام خویش طومار زبانم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لب خود را کنم چون لعل پرخون از پشیمانی</p></div>
<div class="m2"><p>تهی دستی مده از گوهر دندان دهانم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن افسرده را احیا نمودن از تو می آید</p></div>
<div class="m2"><p>به سرسبزی مبدل کن خزان بوستانم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>متاع چند جمع آورده ام از مصر خرسندی</p></div>
<div class="m2"><p>مکن پوشیده از چشم خریداران دکانم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا چون سیدا در باغ عالم سرفرازی ده</p></div>
<div class="m2"><p>میان عندلیبان سبز گردان آشیانم را</p></div></div>