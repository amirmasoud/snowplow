---
title: >-
    شمارهٔ ۳۳۲
---
# شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>پیر گشتیم ز غم یاد جوان ما را بس</p></div>
<div class="m2"><p>دیدن تیر در آغوش کمان ما را بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قرص خورشید به عیسی نفسان ارزانی</p></div>
<div class="m2"><p>زیر گردون چو مه نو لب نان ما را بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار ما نیست در این باغ چو گل خندیدن</p></div>
<div class="m2"><p>آنکه چون غنچه کرم کرد دهان ما را بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت خم به صراحی و قدح باد عزیز</p></div>
<div class="m2"><p>دیدن روی بزرگان جهان ما را بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمر و تاج بود را تبه موج و حباب</p></div>
<div class="m2"><p>کلهی بر سرو مویی به میان ما را بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چمن از سردیی دی خانه غارت زده شد</p></div>
<div class="m2"><p>گل اگر نیست تماشای خزان ما را بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه چون تیر ز دلها گذرد مژگانش</p></div>
<div class="m2"><p>خویش را گر دهد از دور نشان ما را بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از خدا جز سخن نرم نداریم طلب</p></div>
<div class="m2"><p>شمعان شعله آتش به زبان ما را بس</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما چه باشیم که ره در حرم دل یابیم</p></div>
<div class="m2"><p>گر بود جا به صف بی خبران ما را بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیدا از سفر عمر کسی آگه نیست</p></div>
<div class="m2"><p>به شتاب آمدن آب روان ما را بس</p></div></div>