---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>جان ز دنبالش ز جسم ناتوانم می رود</p></div>
<div class="m2"><p>ای طبیبا خیر بادم کن که جانم می رود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نشینم بر سر راهش نمایان چون نشان</p></div>
<div class="m2"><p>هر کجا چون تیر آن ابرو کمانم می رود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذت پیکان او از بس که دارم بر جگر</p></div>
<div class="m2"><p>پیش پیش ناوک او استخوانم می رود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانهای خویش را ای قمریان آتش زنید</p></div>
<div class="m2"><p>از کنار بوستان سرو روانم می رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می کند امروز صبر و عقل و هوش از من وداع</p></div>
<div class="m2"><p>بار خود را بسته فردا کاروانم می رود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد یقین کار من بیمار اکنون با خداست</p></div>
<div class="m2"><p>از سر بالین طبیب مهربانم می رود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهم از بهر سلامت رفتنش گویم دعا</p></div>
<div class="m2"><p>جوهر گفتار از تیغ زبانم می رود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همچو نقش پای خواهم کرد خود را خاکمال</p></div>
<div class="m2"><p>سوی گلبن بس که شاخ ارغوانم می رود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدا روز وداعش چون تصور می کنم</p></div>
<div class="m2"><p>اشک ناکامی ز چشم خونفشانم می رود</p></div></div>