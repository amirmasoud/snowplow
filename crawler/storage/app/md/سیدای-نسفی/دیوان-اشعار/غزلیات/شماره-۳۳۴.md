---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>می کنم چون شمع بهر سوختن امداد خویش</p></div>
<div class="m2"><p>چند سازم تکیه بر دیوار بی بنیاد خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله من خنده کبک است در کهسارها</p></div>
<div class="m2"><p>می دهم منبعد سنگ سرمه بر فریاد خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پنجه هر شب می زنم بر روی طفل آرزو</p></div>
<div class="m2"><p>می کنم روشن چراغ سیلی استاد خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیستون را صورت شیرین ز جا برداشته</p></div>
<div class="m2"><p>سنگ بر سر می زنم بر ماتم فرهاد خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاصل سرگشتگان جز دست بر هم سوده نیست</p></div>
<div class="m2"><p>آسیا دربار کلفت باشد از ایجاد خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ بی بالم ز من اقبال آزادی خطاست</p></div>
<div class="m2"><p>کرده ام بیعت به دام ودانه صیاد خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیره بختی در بغل دارد دل صاف مرا</p></div>
<div class="m2"><p>در نمد پیچیده ام آئینه فولاد خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیدا از هستی خود آنقدر رم کرده ام</p></div>
<div class="m2"><p>سر به صحرا می زنم روزی که سازم یاد خویش</p></div></div>