---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>بود منعم عزیز کشور و آزاد خوار اینجا</p></div>
<div class="m2"><p>ز سرو باغ رعناتر درخت میوه دار اینجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آغاز محبت عاشق از مردن نیندیشد</p></div>
<div class="m2"><p>زند خود را به کام شیر طفل نیسوار اینجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خداجو را سحرخیزی شود افزونتر از پیری</p></div>
<div class="m2"><p>در ایام خزان گلدسته می بندد بهار اینجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>علمهای تو را فردا گواهانند در محشر</p></div>
<div class="m2"><p>مکن آزرده از خود خاطر لیل و نهار اینجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز زهر چشم ارباب طمع را نیست اندیشه</p></div>
<div class="m2"><p>کنند این قوم میل سرمه از مژگان مار اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد بید مجنون بهتر از زنجیر مجنون را</p></div>
<div class="m2"><p>بود از دست گیرا نازنین تر رعشه دار اینجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بیرویی توان از اهل دنیا بهره ور گشتن</p></div>
<div class="m2"><p>ز طفلان شکوه ها دارد درخت میوه دار اینجا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود کار تو یارب در دو عالم پرده پوشیدن</p></div>
<div class="m2"><p>مکن محجوب آنجا و مگردان شرمسار اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد ره به بزم خلوت ما هرزه گویان را</p></div>
<div class="m2"><p>سر منصور سیلی می خورد از پای دار اینجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بزم شعر نبود بهره یی افسرده طبعان را</p></div>
<div class="m2"><p>بود کلک سخن پرداز چون شمع مزار اینجا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به ملک اصفهان و هند می خوانند اشعارم</p></div>
<div class="m2"><p>ز ترکستانم و هرگز ندارم اعتبار اینجا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نباشد با فش و مسواک زاهد هیچ تأثیری</p></div>
<div class="m2"><p>نیندیشد کسی از کوکب دنباله دار اینجا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خط و زلفش کمر بربسته اند از بهر خون من</p></div>
<div class="m2"><p>حذر کن سیدا از اتفاق مور و مار اینجا</p></div></div>