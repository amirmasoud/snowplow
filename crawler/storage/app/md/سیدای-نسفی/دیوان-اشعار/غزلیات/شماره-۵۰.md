---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>خال لبت نشانده در آتش خلیل را</p></div>
<div class="m2"><p>زلف تو بسته بال و پر جبرئیل را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ارباب حرص اهل طعم را خورد به چشم</p></div>
<div class="m2"><p>باشد حلال خون گدایان بخیل را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیلاب گریه کوه گنه را برد ز جا</p></div>
<div class="m2"><p>فرعون سد ره نشود رود نیل را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صورت بزرگ مروت طمع مدار</p></div>
<div class="m2"><p>تنگ آفریده دست قضا چشم پیل را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تدبیر عقل راه نیابد به کوی عشق</p></div>
<div class="m2"><p>سازند منع بی سند اینجا دلیل را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از وصل یار کام گرفتیم سیدا</p></div>
<div class="m2"><p>بردیم زین محیط در بی عدیل را</p></div></div>