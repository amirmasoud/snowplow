---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>صرف شد عمر من ای یار غلط کردم حیف</p></div>
<div class="m2"><p>در پی چون تو ستمگار غلط کردم حیف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی بود در این شهر گمان می کردم</p></div>
<div class="m2"><p>من تو را یار وفادار غلط کردم حیف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوده یی با من سودا زده چون مهر و فلک</p></div>
<div class="m2"><p>آشنایی سر بازار غلط کردم حیف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر کوی تو هر خار غمی می دیدم</p></div>
<div class="m2"><p>می زدم بر سر دستار غلط کردم حیف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیدا از غم او شب همه شب همچون شمع</p></div>
<div class="m2"><p>داشتم دیده بیدار غلط کردم حیف</p></div></div>