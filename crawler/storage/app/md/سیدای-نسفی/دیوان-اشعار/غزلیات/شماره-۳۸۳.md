---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>تا به کوی آن نگار سیمبر جا کرده ام</p></div>
<div class="m2"><p>خانه یی از سیم بهر خویش برپا کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برای سیم دنیا پا در این ره مانده ام</p></div>
<div class="m2"><p>شهر و صحرا از نسیم اشک دریا کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا نریزد آبروی لنگر من بر زمین</p></div>
<div class="m2"><p>از چپ و از راست آغوش نظر واکرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سپند روی آتش هست کارم جست و خیز</p></div>
<div class="m2"><p>در عجایب منزلی امروز مأوا کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو تار سیم هر دم می خورم صد پیچ و تاب</p></div>
<div class="m2"><p>سیدا تا دار دنیا را تماشا کرده ام</p></div></div>