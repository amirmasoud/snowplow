---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>جهان پر شهد از لبهای خندان تو می گردد</p></div>
<div class="m2"><p>حلاوت کامیاب از شکرستان تو می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگه از گوشه چشم تو ترکش بسته می خیزد</p></div>
<div class="m2"><p>سپاه فتنه سرگردان مژگان تو می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نزاکت می خورد از روی آتشناک تو سیلی</p></div>
<div class="m2"><p>لطافت آب از چاه زنخدان تو می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک داری هوای دست بوس آستین تو</p></div>
<div class="m2"><p>زمین چون گرد در دنبال دامان تو می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پریشان کرده خود را نافه آهو به کشورها</p></div>
<div class="m2"><p>به جستجوی زلف عنبرافشان تو می گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شب تا روز گردون از کواکب آسمانها را</p></div>
<div class="m2"><p>چراغان کرده بر صحن گلستان تو می گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کنار حیرتم واکرده آغوش تهیدستی</p></div>
<div class="m2"><p>چو طوق هاله گرد ماه تابان تو می گردد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آتش می نشیند می گدازد آب می گردد</p></div>
<div class="m2"><p>چراغ طور تا شمع شبستان تو می گردد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گردن کرده سرو از طوق قمری فوطه زاری</p></div>
<div class="m2"><p>به استقبال شمشاد خرامان تو می گردد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حصاری کی تواند ساختن فانوس مشعل را</p></div>
<div class="m2"><p>سر خود می خورد هر کس نگهبان تو می گردد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نظر از کوچه باغ آستینت تازه می آید</p></div>
<div class="m2"><p>تماشا خرم از چاک گریبان تو می گردد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کند در دیده اش نظاره کار میل آتش را</p></div>
<div class="m2"><p>همان چشمی که محروم از گلستان تو می گردد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تنم را پیرهن چون تار تا گردن فرو برده</p></div>
<div class="m2"><p>مگر دست نسیمی طرف دامان تو می گردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز چشم انتظارت سیدا بادام می روید</p></div>
<div class="m2"><p>کدام آهو نگه امروز مهمان تو می گردد</p></div></div>