---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>به دور خط رخسارت دو چندان گشت آه من</p></div>
<div class="m2"><p>شد آخر سبزه پشت لبت مهر گیاه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا طاق دو ابروی تو محراب دعا باشد</p></div>
<div class="m2"><p>مگردان روی از من کعبه من قبله گاه من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بیخانمان غیر از تو عرض دل که را گویم</p></div>
<div class="m2"><p>تو هم میری و هم سلطانی و هم پادشاه من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چون گردباد آخر بیابان مرگ خواهی کرد</p></div>
<div class="m2"><p>نگار من قلندر مشرب من کج کلاه من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر جا قصد رفتن می کنم پیش تو می آیم</p></div>
<div class="m2"><p>فلک سوی تو واکرد است از هر کوچه راه من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم را گه در آب و گه در آتش می زند خشمت</p></div>
<div class="m2"><p>به دست ظالمی افتاده طفل بی گناه من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هدف از شست پاک قادراندازان حذر دارد</p></div>
<div class="m2"><p>بترس ای جنگجو هنگام صبح از تیر آه من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زبانت درد من چون مغز بادام است در شکر</p></div>
<div class="m2"><p>بود خال لبت ای رشک گل قند سیاه من</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طبیبا بر سر بالینم از بهر چه می آیی</p></div>
<div class="m2"><p>توان فهمید حال زارم از نبض نگاه من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کنج خانه خود سیدا بیرون نمی آیم</p></div>
<div class="m2"><p>مبادا پی برد اغیار از حال تباه من</p></div></div>