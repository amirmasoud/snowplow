---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>غنچه خسبی ها در آغوش چمن دارد مرا</p></div>
<div class="m2"><p>حفظ آب روی سر در پیرهن دارد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سپند امروز بی آرامم از سودای هند</p></div>
<div class="m2"><p>یاد خاکسترنشینی بی وطن دارد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می خورم از بهر یک مصراع چندین پیچ و تاب</p></div>
<div class="m2"><p>موی آتش دیده سودای سخن دارد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه تصویر را از ناله آوردم به حرف</p></div>
<div class="m2"><p>بر لب انگشت تحیر آن دهن دارد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا شد او از بزم بیرون من شدم خلوت نشین</p></div>
<div class="m2"><p>یاد عمر رفته دور از انجمن دارد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پشت خود بر بیستون چون نقش شیرین مانده ام</p></div>
<div class="m2"><p>همچو شیرین کاری خود کوهکن دارد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا بر کشته سیماب حسرت می خورم</p></div>
<div class="m2"><p>بس که بی آرام آن سیمین بدن دارد مرا</p></div></div>