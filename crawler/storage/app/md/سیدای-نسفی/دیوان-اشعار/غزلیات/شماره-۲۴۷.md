---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>جبهه می خواهم که وقف سجده آن پا شود</p></div>
<div class="m2"><p>تا دری بر رویم از پیشانی او وا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زود برخیزم به تعظیمش ز جا چون گردباد</p></div>
<div class="m2"><p>از کنار دشت اگر سرگشته یی پیدا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حسن روزافزون او خواهد صف دلها شکست</p></div>
<div class="m2"><p>باده گر این است خونها بر سر مینا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش پا زنجیر می گردد به پای آهوان</p></div>
<div class="m2"><p>نرگس صیدافگنت روزی که در صحرا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل نزدیکان خود را عشق اندازد به دور</p></div>
<div class="m2"><p>ساحل از همسایگی سیلی خورد دریا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده خود سرخ می سازند ارباب طمع</p></div>
<div class="m2"><p>از میان بحر خون دستی اگر بالا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم او در بردن دل غمزه را استاد کرد</p></div>
<div class="m2"><p>هر که بر دامان دانا دست زد دانا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خانه چون تاریک باشد سیدا روزن چه سود</p></div>
<div class="m2"><p>دیده کو بی نور شد باید که دل بینا شود</p></div></div>