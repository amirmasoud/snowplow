---
title: >-
    شمارهٔ ۴۱۱
---
# شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>می کند پهلو تهی کنج غم از تنهائیم</p></div>
<div class="m2"><p>در کنار طاق نسیان مانده یی بی جائیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در پی مکن سایه چون نقش قدم از پا فتاد</p></div>
<div class="m2"><p>نیست در عالم کسی را طاقت همپائیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جنونم سد ره زنجیر نتوان شدن</p></div>
<div class="m2"><p>حلقه فتراک می جوید سر سودائیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شورش فرزند می آرد پدر را در سماع</p></div>
<div class="m2"><p>زینهار اندیشه کن ای چرخ از رسوائیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزیی جوهرشناس از سنگ می آید برون</p></div>
<div class="m2"><p>کرده مستغنی ز عالم سرمه بینائیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه گیری می برآرد آدمی را ز احتیاج</p></div>
<div class="m2"><p>می شمارد کفش تنگ چرخ را بی پائیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود از کاهلی همیشه را بازار گرم</p></div>
<div class="m2"><p>خصم می بالد به خود چون گل ز بی پروائیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می دود از دیدنش اشکم به روی کاه رنگ</p></div>
<div class="m2"><p>از تماشای رخش گل می کند رعنائیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کند در وصل عشق پاک عاشق را هلاک</p></div>
<div class="m2"><p>جان به لب آید مرا روزی که بر سر آئیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیدا در جوی ارباب مروت نم نماند</p></div>
<div class="m2"><p>خاک می لیسد به ساحل کشتی دریائیم</p></div></div>