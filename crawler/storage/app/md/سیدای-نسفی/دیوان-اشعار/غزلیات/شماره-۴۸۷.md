---
title: >-
    شمارهٔ ۴۸۷
---
# شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>شبها بود چراغم از دود آه بی تو</p></div>
<div class="m2"><p>چون شمع کشته دارم روز سیاه بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او را ز صبح و شامم باشد دعای حیات</p></div>
<div class="m2"><p>محرابم آسمان است ای قبله گاه بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر نقش پای هر کس از بی کسی زنم دست</p></div>
<div class="m2"><p>دامن فشاند از من چون گرد راه بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بینم به پرده چشم خاصیت کتان را</p></div>
<div class="m2"><p>شبها اگر به مهتاب سازم نگاه بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کنج غم نشسته شب تا سحر به یادت</p></div>
<div class="m2"><p>ریزم ز دیده انجم ای رشک ماه بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیوار خانه ام بود پیوسته تکیه گاهم</p></div>
<div class="m2"><p>رفتی و رفت بر باد پشت و پناه بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاید که بر سر من روزی قدم گذاری</p></div>
<div class="m2"><p>چون نقش پا نشینم سرهای راه بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جرمی که سر زد ای شاه بر سیدا ببخشای</p></div>
<div class="m2"><p>یعنی که زنده بودن باشد گناه بی تو</p></div></div>