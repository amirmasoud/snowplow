---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>از سر کوی تو با صد حسرت ای گل می روم</p></div>
<div class="m2"><p>محمل خود بسته ام از بال بلبل می روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشنی بودم مرا باد خزان تاراج کرد</p></div>
<div class="m2"><p>با دماغ خشک همچون نکهت گل می روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>استقامت نیست در یکجای با دیوانگان</p></div>
<div class="m2"><p>رخت خود پیچیده زین گلشن چو سنبل می روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کودکی گردد چو سنگ سرمه سد راه من</p></div>
<div class="m2"><p>چون نگه افتاد از چشمم تغافل می روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زادراه خاکساران از هوا پیدا شود</p></div>
<div class="m2"><p>گردبادم در بیابان توکل می روم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شانه ام غیر از پریشانی مرا در بار نیست</p></div>
<div class="m2"><p>تیره بختم در خیال زلف و کاکل می روم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جوش اشکم سیدا پامال سازد چرخ را</p></div>
<div class="m2"><p>موج سیل نوبهارم از سر پل می روم</p></div></div>