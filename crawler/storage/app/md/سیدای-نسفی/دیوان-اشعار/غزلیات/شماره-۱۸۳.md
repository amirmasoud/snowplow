---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>مبادا کار من با چشمت ای مژگان خدنگ افتد</p></div>
<div class="m2"><p>گرفتار تو هر کس گشت در قید فرنگ افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من کرداست سنگ سرمه رو گردان نگاهت را</p></div>
<div class="m2"><p>الهی خاک گردد سرمه و آتش به سنگ افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفیق از کف مده خواهی که در منزل بری خود را</p></div>
<div class="m2"><p>شود سنگ سر ره چون عصا از دست لنگ افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لبالب می شود از شیر ماهی دام تن پرور</p></div>
<div class="m2"><p>به کام ناتوانان اره پشت نهنگ افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نباشد روزیی جز پاره دل غنچه گل را</p></div>
<div class="m2"><p>خورد خون هر که را همت وسیع و دست تنگ افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آن مجنون بدبختم که از صحرا به شهر آیم</p></div>
<div class="m2"><p>شود اطفال هم دیوانه و قحطی به سنگ افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دشمن سیدا بگذار تیغ پیشدستی را</p></div>
<div class="m2"><p>سر خود می خورد چون خار هر کس تیز چنگ افتد</p></div></div>