---
title: >-
    شمارهٔ ۴۲۵
---
# شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>می روم در باغ و سر در پای سنبل می کنم</p></div>
<div class="m2"><p>عمر را چون شانه صرف زلف و کاکل می کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می نویسم از چمن با آن گل رو نامه یی</p></div>
<div class="m2"><p>خامه تحریر از منقار بلبل می کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیست حاجت زاد راه از خویش بیرون رفته را</p></div>
<div class="m2"><p>گردبادم پا به دامان توکل می کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه خسبم سیدا عمریست در باغ جهان</p></div>
<div class="m2"><p>عندلیبی گر به سر وقتم رسد گل می کنم</p></div></div>