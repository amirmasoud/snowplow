---
title: >-
    شمارهٔ ۲۰۱
---
# شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>رفتند اهل جود در ایام کس نماند</p></div>
<div class="m2"><p>در بزم روزگار به غیر از مگس نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زد قفل باغبان به در باغ آرزو</p></div>
<div class="m2"><p>ما را دگر به شاخ هوس دسترس نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت از دماغ گوشه نشینان حضور قلب</p></div>
<div class="m2"><p>آسوده خاطری به بهشت قفس نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رفت عید و آمد نوروز فارغم</p></div>
<div class="m2"><p>طفل مرا ز بس که هوا و هوس نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بستند سیدا در ارباب جود را</p></div>
<div class="m2"><p>از ناله لب ببند که فریادرس نماند</p></div></div>