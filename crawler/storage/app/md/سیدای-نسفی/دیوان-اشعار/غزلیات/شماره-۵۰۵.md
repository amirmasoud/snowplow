---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>دل همچو اشک بر سر مژگان برآمده</p></div>
<div class="m2"><p>بهر نظاره رخ جانان برآمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر برگ لاله یی ز دل پاره کیست</p></div>
<div class="m2"><p>از دست داغ من به بیابان برآمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سودای من ز خط لب او زیاده شد</p></div>
<div class="m2"><p>این شور بر سرم ز نمکدان برآمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمریست دل ز سینه به پای پرآبله</p></div>
<div class="m2"><p>در جستجوی خار مغیلان برآمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا رسیده قصه چشم و تبسمش</p></div>
<div class="m2"><p>نرگس دمیده و گل خندان برآمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دستم ز کو تهی به گریبان نمی رسد</p></div>
<div class="m2"><p>پایم ز جمع کردن دامان برآمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هر زمین نشسته برون گشته نخل گل</p></div>
<div class="m2"><p>هر جا گذشته سرو خرامان برآمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خط غبار نیست به رخسار آن پری</p></div>
<div class="m2"><p>گردیست از بنای سلیمان برآمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شمع پای صبر به دامن کشیده ام</p></div>
<div class="m2"><p>از بس که آتشم ز گریبان برآمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نتوان گزید لب چو شود موی سر سفید</p></div>
<div class="m2"><p>این ریشه از کشیدن دندان برآمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آتش زدی به کعبه و بتخانه سوختی</p></div>
<div class="m2"><p>دود از نهاد گبر و مسلمان برآمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر غنچه در چمن به امید خدنگ تو</p></div>
<div class="m2"><p>از شاخ گل به صورت پیکان برآمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زنار بند زلف تو هر جا که رفته است</p></div>
<div class="m2"><p>گردن نهاده لشکر ایمان برآمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شبنم در آرزوی گلستان روی تو</p></div>
<div class="m2"><p>گریان به باغ آمده گریان برآمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از چاکهای سینه دل داغدار من</p></div>
<div class="m2"><p>چون سایلان در آرزوی نان برآمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هر قطره خون که از دل من بر زمین چکد</p></div>
<div class="m2"><p>لعلی بود ز کوه بدخشان برآمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرغان چو غنچه سر به گریبان کشیده اند</p></div>
<div class="m2"><p>آن گل مگر به سیر گلستان برآمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از خامه ریخت معنی پر زور سیدا</p></div>
<div class="m2"><p>این شیر تندخو ز نیستان برآمده</p></div></div>