---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>رویت گل سرسبد لاله زارها</p></div>
<div class="m2"><p>خطت متاع قافله نوبهارها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بوستان ز حسرت پابوس سرو تو</p></div>
<div class="m2"><p>خمیازه می کشند لب جویبارها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفتار تو گرفته سر راه کبک را</p></div>
<div class="m2"><p>تمکین تو شکسته سر کوهسارها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای چشمه حیات تغافل ز حد گذشت</p></div>
<div class="m2"><p>موج سراب اشک من است آبشارها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز ز دیدنت نشود سیر چشم ما</p></div>
<div class="m2"><p>بی انتهاست سلسله انتظارها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگ است جامه گل رعنا به دوش من</p></div>
<div class="m2"><p>آسوده ام ز پیرهن اعتبارها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زین قوم مرده دل چه طمع می کند کسی</p></div>
<div class="m2"><p>بینند پیش پا به چراغ مزارها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقت است سیدا ز پی بیخودی رویم</p></div>
<div class="m2"><p>ما را گداخت پیرویی اختیارها</p></div></div>