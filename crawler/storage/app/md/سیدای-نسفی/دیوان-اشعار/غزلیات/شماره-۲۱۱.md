---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>آهم امشب در چمن سرگشتگان را یاد کرد</p></div>
<div class="m2"><p>آشیان بلبلان را آسیایی یاد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آید از هر حلقه زنجیر آواز درا</p></div>
<div class="m2"><p>بس که در کوی تو گوشم تا سحر فریاد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل درا آغاز محبت کرد کار خود تمام</p></div>
<div class="m2"><p>طفل من در تخته خوانی خویش را استاد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه گل از نسیمی در گلستان تازه روست</p></div>
<div class="m2"><p>می توان ما را به اندک التفاتی یاد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از وصال شمع چون پروانه گردد کامیاب</p></div>
<div class="m2"><p>هر که ما را از برای سوختن امداد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گردش دوران کند خاکش به سر چون گردباد</p></div>
<div class="m2"><p>هر که اینجا تکیه بر دیوار بی بنیاد کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغ کاهل طبعم از دام فلک بیرون نرفت</p></div>
<div class="m2"><p>عمر خود را صرف آب و دانه صیاد کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق او ای سیدا دل را به درد غم سپرد</p></div>
<div class="m2"><p>این ستمگر ملک خود را وقف بر اولاد کرد</p></div></div>