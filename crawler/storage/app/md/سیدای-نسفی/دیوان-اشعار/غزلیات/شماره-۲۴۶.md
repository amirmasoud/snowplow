---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>آنها که تکیه بر کرم کبریا کنند</p></div>
<div class="m2"><p>سر در قبا کشند و به محراب جا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که قانع اند به یک قطره چون صدف</p></div>
<div class="m2"><p>گوهر اگر دهند هماندم عطا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنها که بر کلاه نمد خوی کرده اند</p></div>
<div class="m2"><p>کی آرزوی سایه بال هما کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنها که آگهند ز اسرار نیک و بد</p></div>
<div class="m2"><p>از کاه دانه را به نگاهی جدا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنها از نسیم سحر بوی برده اند</p></div>
<div class="m2"><p>دلهای غنچه را چمن دلگشا کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنها که پا ز بستر راحت کشیده اند</p></div>
<div class="m2"><p>پهلوی خویش وقف نی بوریا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنها که روز و شب بخدایند سیدا</p></div>
<div class="m2"><p>ما را چه می شود که به خود آشنا کنند</p></div></div>