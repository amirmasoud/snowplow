---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>ز خونم بعد کشتن شورش محشر شود پیدا</p></div>
<div class="m2"><p>چو بر خاک اوفتد یکدانه چندی سر شود پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شستشو لباس بدقماش اطلس نمی گردد</p></div>
<div class="m2"><p>به صیقل کی ز تیغ آهنین جوهر شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به دیوار قفس تا رخنه ها دیدیم یقینم شد</p></div>
<div class="m2"><p>که یک در بسته گردد صد در دیگر شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر امروز دست خود به نان دادن علم سازی</p></div>
<div class="m2"><p>ز خورشید قیامت بر سرت چادر شود پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شود چون شعله گردنکش به اندک تقویت ظالم</p></div>
<div class="m2"><p>به آتش از خس و خاشاک بال و پر شود پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به اندک رتبه ته چوبکاری فخر می سازی</p></div>
<div class="m2"><p>بود معراج واعظ هر کجا منبر شود پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر در راه حق ای سیدا با صدق رو آری</p></div>
<div class="m2"><p>تو را چون خضر از ریگ روان رهبر شود پیدا</p></div></div>