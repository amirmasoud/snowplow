---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>به دستم کرد گل تا داغهای آتشین من</p></div>
<div class="m2"><p>ز جوش بلبلان گرداب خون شد آستین من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم از سوختن خود را دمی فارغ نمی دارد</p></div>
<div class="m2"><p>کباب شعله خوبان است داغ دلنشین من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز فکر زلف او از بسکه روز تیره یی دارم</p></div>
<div class="m2"><p>کند تکلیف هندم بخت خاکسترنشین من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد برق را بر حاصل روشندلان دستی</p></div>
<div class="m2"><p>به گرد خرمنم از دور گردد خوشه چین من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز وصل آن پری هرگز نشد کام دلم شیرین</p></div>
<div class="m2"><p>سلیمانم ولی زهر است در زیر نگین من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مده زین بیش بهر سجده ام درد سرای زاهد</p></div>
<div class="m2"><p>چو صندل سوده شد بر آستان او جبین من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدیدم سیدا تا در کنار شانه زلفش را</p></div>
<div class="m2"><p>گمانهایی که با او کرده بودم شد یقین من</p></div></div>