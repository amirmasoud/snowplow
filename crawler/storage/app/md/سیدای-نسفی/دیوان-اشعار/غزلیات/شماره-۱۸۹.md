---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>شمع با آتش مدارا تا قیامت می کند</p></div>
<div class="m2"><p>صحبت دیر آشنایان استقامت می کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سفره خود هر که اینجا پهن می سازد چو صبح</p></div>
<div class="m2"><p>بر سر خود سایه بانی تا قیامت می کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون صیادان بلبل هست چون شبنم حلال</p></div>
<div class="m2"><p>باغبان از دفتر گل این روایت می کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گل بگو بیهوده می ریزد زر خود را به خاک</p></div>
<div class="m2"><p>آشنایان عندلیبان را ملامت می کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع می آید سوی فانوس با چشم پر آب</p></div>
<div class="m2"><p>مشهد پروانه را هر شب زیارت می کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شود از بانگ گوشم زنگ گردون پر صدا</p></div>
<div class="m2"><p>از لب او غنچه گل گر حکایت می کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاد خال او مرا بی تاب دارد چون سپند</p></div>
<div class="m2"><p>شمع را نازم که چون در بزم طاقت می کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می زند بر گردنش محراب شمشیر از دو رو</p></div>
<div class="m2"><p>بی خیال ابرویش هر کس که طاعت می کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکوه همیشه عاجز نمی باید شنید</p></div>
<div class="m2"><p>دشمن از کوتاه دستی ها شکایت می کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر مزار بی کسان شمعی که روشن می شود</p></div>
<div class="m2"><p>بر سر خود باد را دست حمایت می کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تنگ چشمان را به نعمت سیر کردن مشکل است</p></div>
<div class="m2"><p>مرد می دانم که موری را ضیافت می کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دری روی سایلان هر کس که بندد سیدا</p></div>
<div class="m2"><p>خانه خود را به دست خویش غارت می کند</p></div></div>