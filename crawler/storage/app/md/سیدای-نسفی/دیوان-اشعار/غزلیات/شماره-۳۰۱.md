---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>آمد خزان نسیم گل و یاسمن نماند</p></div>
<div class="m2"><p>باد بهار رفت و هوای چمن نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاراج کرد باد خزان اهل باغ را</p></div>
<div class="m2"><p>در غنچه رنگ و در بر گل پیرهن نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیرون شدم ز بیضه و گشتم اسیر غم</p></div>
<div class="m2"><p>آسایشی که بود مرا در وطن نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد ره به یار نامه نوشتم نکرد گوش</p></div>
<div class="m2"><p>اکنون به نامبر چه نویسم سخن نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگداختم ز گرمی خویش و به دیگران</p></div>
<div class="m2"><p>مهری که بود در دل از آن سیمتن نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرهاد همچو لاله برآمد ز کوهسار</p></div>
<div class="m2"><p>داغ غمت شهید تو را در کفن نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بستند قمریان ز چمن بار سیدا</p></div>
<div class="m2"><p>در شاخسار سرو به غیر از زغن نماند</p></div></div>