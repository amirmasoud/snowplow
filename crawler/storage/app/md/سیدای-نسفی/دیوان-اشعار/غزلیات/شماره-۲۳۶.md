---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>شعله خوبانی که هر یک آفت پروانه اند</p></div>
<div class="m2"><p>پیش شمع روی او چون شمع ماتمخانه اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقبازان مو به مو دانند حال تیره ام</p></div>
<div class="m2"><p>سینه چاکان روشناس زلف همچون شانه اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست اگر یابند به چشمان یکدگر را می خورند</p></div>
<div class="m2"><p>اهل عالم صورت دیوار را همخانه هند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافلان را می برد از جای اندک وسوسه</p></div>
<div class="m2"><p>سر به بالین ماندگان محتاج یک افسانه اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی خودند از یاد محشر سیدا فرزانگان</p></div>
<div class="m2"><p>عاقلان در فکر کار خویشتن دیوانه اند</p></div></div>