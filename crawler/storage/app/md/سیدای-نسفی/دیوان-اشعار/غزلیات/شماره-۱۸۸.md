---
title: >-
    شمارهٔ ۱۸۸
---
# شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>نگار من به دم چون مسیح می آید</p></div>
<div class="m2"><p>رسول حق به کلام فصیح می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دهد با هل هوس یاد سوره اخلاص</p></div>
<div class="m2"><p>پی ندامت مشرک صریح می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگیر از سبد گلفروش لاله و گل</p></div>
<div class="m2"><p>به دست داغ ز خوبان قبیح می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چو دید ز دور او به زلف و کاکل گفت</p></div>
<div class="m2"><p>که این شکسته سراپا صحیح می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به باغ در پی او دوش سیدا رفتم</p></div>
<div class="m2"><p>به خنده گفت که یار ملیح می آید</p></div></div>