---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>پیشانی ناز او از موج نگه چین بست</p></div>
<div class="m2"><p>مژگان ستم برخاست ابروی کمر کین بست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم به عتاب آمد خونم به تغافل ریخت</p></div>
<div class="m2"><p>شمشاد خرامانش در پای نگارین بست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از داغ سراپایم انگشت نما گردید</p></div>
<div class="m2"><p>زآئینه سیمایش ملک دلم آئین بست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلزار تماشا گشت هر جا که اقامت کرد</p></div>
<div class="m2"><p>گلدسته رعنایی از رشته تمکین بست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوبان ز شکر خندش در شهد فرو رفتند</p></div>
<div class="m2"><p>لبهای شکرریزان از خنده شیرین بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانند گل آن سید از خار کند بستر</p></div>
<div class="m2"><p>آسایش خود هر کس با جامه رنگین بست!</p></div></div>