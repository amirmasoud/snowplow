---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>خون می چکد چو غنچه گل از سخن مرا</p></div>
<div class="m2"><p>تیغ برهنه ایست زبان در دهن مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هیچ جا قرار ندارم چو آفتاب</p></div>
<div class="m2"><p>مهر رخ تو کرده چنین بی وطن مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه را به بزم خود ای شمع ره مده</p></div>
<div class="m2"><p>کوته زبان مساز بهر انجمن مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از هلاکم از سر خاکم چو بگذری</p></div>
<div class="m2"><p>سازد علم میان شهیدان کفن مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی قامت تو سرو چو دو دست در نظر</p></div>
<div class="m2"><p>بی روی تست طشت پر آتش چمن مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که بهر قتل اسیران شوی سوار</p></div>
<div class="m2"><p>هر موی تازیانه شود بر بدن مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی که پنجه ام ز گریبان جدا شود</p></div>
<div class="m2"><p>ای سیدا چو مار خورد پیرهن مرا</p></div></div>