---
title: >-
    شمارهٔ ۴۰۳
---
# شمارهٔ ۴۰۳

<div class="b" id="bn1"><div class="m1"><p>از عدم جسم خراب و رنگ زرد آورده ام</p></div>
<div class="m2"><p>تحفه یی امروز بهر اهل درد آورده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطری دارم غبارآلود از رنج سفر</p></div>
<div class="m2"><p>گردبادم پیکری در زیر گرد آورده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانب گلشن مکن تکلیف ای بلبل مرا</p></div>
<div class="m2"><p>نوبهارم لیک با خود آه سرد آورده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاجزم افتاده ام ای چرخ از من کن حذر</p></div>
<div class="m2"><p>لشکری همراه خود بهر نبرد آورده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تکیه گاهم سیدا باشد به شاه نقشبند</p></div>
<div class="m2"><p>خویش را در سایه این شیرمرد آورده ام</p></div></div>