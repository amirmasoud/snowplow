---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>غنچه ام آخر چو گل کام به عریانی بود</p></div>
<div class="m2"><p>لب گزیدن های من از بی گریبانی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم و گوشم قاصد جاسوس حیرانی بود</p></div>
<div class="m2"><p>همچو گل اعضایم اسباب پریشانی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل چو گردد ساده او را حل مشکل ها کنند</p></div>
<div class="m2"><p>پرده این قفل را مفتاح نادانی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نامه اعمال گردد در بر نیکان قبا</p></div>
<div class="m2"><p>پرده پوش صبح محشر پاکدامانی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل شکستن کفر را ترجیح با دین کرده است</p></div>
<div class="m2"><p>کعبه را بتخانه کردن خانه ویرانی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نظرها خوش نما باشد کمان نقشدار</p></div>
<div class="m2"><p>جوهر شمشیر ابرو چین پیشانی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن پری رو را به خاموشی مسخر ساختم</p></div>
<div class="m2"><p>لب فرو بستن مرا مهر سلیمانی بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانه را صیاد ریزد پیش مرغان بر زمین</p></div>
<div class="m2"><p>کار زاهد در نظرها سبحه گردانی بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنچه دل واز انگشت ندامت می شود</p></div>
<div class="m2"><p>ناخن این عقده در دشت پشیمانی بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خانه بر دوشی لباس عافیت باشد مرا</p></div>
<div class="m2"><p>پاسبان سفره درویش بی نانی بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسم اعظم خوان شود ایمن ز آفتاب پری</p></div>
<div class="m2"><p>چون دچار او شوم کارم دعاخوانی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فصل گل باز است دست باغبان گلفروش</p></div>
<div class="m2"><p>خوان هر کس پهن در ایام ارزانی بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می شود آخر سر بی مغز پامال هوا</p></div>
<div class="m2"><p>این صدا در گوش من از طبل سلطانی بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا نسازم سینه را صد چاک خندان کی شوم</p></div>
<div class="m2"><p>همچو گل دل جمعی من در پریشانی بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خواب آسایش نبیند دیده دنیاپرست</p></div>
<div class="m2"><p>باغبان را روز تا شب کار دربانی بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بست طاق خانه آئینه را ابروی او</p></div>
<div class="m2"><p>این کمان پیوسته در بازوی حیرانی بود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>می توان در پشت بام خود علمها ساختن</p></div>
<div class="m2"><p>گر فش و مسواک اسباب مسلمانی بود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن پسر هنگام خط از خانه می آید برون</p></div>
<div class="m2"><p>یوسف من تا به روز حشر زندانی بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رزق طوطی سیدا باشد مهیا از شکر</p></div>
<div class="m2"><p>روزیی دلخواه در خوان سخندانی بود</p></div></div>