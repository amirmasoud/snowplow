---
title: >-
    شمارهٔ ۳۹۵
---
# شمارهٔ ۳۹۵

<div class="b" id="bn1"><div class="m1"><p>شکایت نامه آن روی چون گل بود در دستم</p></div>
<div class="m2"><p>قلم در ناله چون منقار بلبل بود در دستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحر زلف تو از روی تصور شانه می کردم</p></div>
<div class="m2"><p>به گردن داشتم زنجیر و سنبل بود در دستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بزم می پرستان دوش رفتم همره ساقی</p></div>
<div class="m2"><p>ز شب تا صبحدم جام توکل بود در دستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شبنم باغبان از چشم خود می داد آب من</p></div>
<div class="m2"><p>بسان غنچه گل کیسه پل بود در دستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گلشن سیدا قطع نظر می کردم از نرگس</p></div>
<div class="m2"><p>به یاد چشم او تیغ تغافل بود در دستم</p></div></div>