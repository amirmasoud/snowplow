---
title: >-
    شمارهٔ ۴۶۲ - قطعه
---
# شمارهٔ ۴۶۲ - قطعه

<div class="b" id="bn1"><div class="m1"><p>فلک رتبه سبحانقلی خان که باشد</p></div>
<div class="m2"><p>گدای درش قیصر روم و خاقان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخارا ز یمن قدوم شریفش</p></div>
<div class="m2"><p>زند خاک در چشم هندوصفاهان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بهر جلوس وی ایستاد گیتی</p></div>
<div class="m2"><p>بنا کرد تختی چو تخت سلیمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز ابروی خوبان بود متکایش</p></div>
<div class="m2"><p>بود پایه او سر تاجداران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فلک از فرازش فتادست بر خاک</p></div>
<div class="m2"><p>زمین از گران سنگیش گشته حیران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکسته کمر کوه قاف از وقارش</p></div>
<div class="m2"><p>کشیدست پای خجالت به دامان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به تاریخ اتمام او سیدا گفت</p></div>
<div class="m2"><p>محل جلوس شهنشاه دوران</p></div></div>