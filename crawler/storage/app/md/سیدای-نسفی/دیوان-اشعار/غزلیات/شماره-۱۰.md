---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>ز خون دل شده رنگین دو دیده تر ما</p></div>
<div class="m2"><p>بهار لاله ما گل کند ز ساغر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا چو شمع به بالین ما نمی آیی</p></div>
<div class="m2"><p>ز انتظاریی بی حد سفید شد سر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت عمر و دل ما به آرزو نرسید</p></div>
<div class="m2"><p>در آشیانه ما پیر شد کبوتر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پای تکیه ما سفر فرو نمی آرد</p></div>
<div class="m2"><p>کلاه گوشه معشوق ما قلندر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ستاره سوختگان چون سپند سبز شدند</p></div>
<div class="m2"><p>کجاست گریه ابر بهار اختر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زدی به تیغ و بریدی و ساختی پامال</p></div>
<div class="m2"><p>چه روزها که نه افگنده ای تو بر سر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سیدا نظر مرحمت نمی سازی</p></div>
<div class="m2"><p>ز چشم دام تو افتاده صید لاغر ما</p></div></div>