---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>فتنه جویی دوش تاراج دل و جان کرد و رفت</p></div>
<div class="m2"><p>خانه ام را آمد و چون سیل ویران کرد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو گل اوراق اجزایم به هم پیوسته بود</p></div>
<div class="m2"><p>چون دم صبح خزان آمد پریشان کرد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلبه ام را داد بر باد فنا چون گردباد</p></div>
<div class="m2"><p>دامن خود بر زد و رو در بیابان کرد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه ام بود از وصال او گلستان ارم</p></div>
<div class="m2"><p>خیر باد او به خاک تیره یکسان کرد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بست بر بازو کمان و گوشه ابرو نمود</p></div>
<div class="m2"><p>سینه را سوراخ ها از خار مژگان کرد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمد و بال پر پروانه را مقراض کرد</p></div>
<div class="m2"><p>شمع بزمم را چراغ زیر دامان کرد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزگاری داغ او را داشتم در دل نهان</p></div>
<div class="m2"><p>بر سر من آمد و چون گل نمایان کرد و رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غنچه وار افگند در اندیشه دور و دراز</p></div>
<div class="m2"><p>جوش سودایش سرم را پر ز سودا کرد و رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غم او شیشه ها کردند ساغر را وداع</p></div>
<div class="m2"><p>آمد و اسباب عیشم را پریشان کرد و رفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روز محشر دامنش خواهم گرفت ای سیدا</p></div>
<div class="m2"><p>در حق من ظلم ها آن نامسلمان کرد و رفت</p></div></div>