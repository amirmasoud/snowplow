---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>دل در چمن مبندید آتشزده سرائیست</p></div>
<div class="m2"><p>کام از جهان مجوئید صحرای کربلائیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باغبان در این باغ دانسته نه قدم را</p></div>
<div class="m2"><p>هرگل سر شهیدیست هر برگ بینوائیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی عشقبازی مردانه پا گذارید</p></div>
<div class="m2"><p>هر منزلی طلسمیست هر گام اژدهائیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق وصال وحدت زاهد سماع کثرت</p></div>
<div class="m2"><p>در هر دلی خیالی در سری هوائیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون شمع آب و آتش کردند سازواری</p></div>
<div class="m2"><p>ما را به نفس سرکش هر روز ماجرائیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که بهر روزی گردیده ام جهان را</p></div>
<div class="m2"><p>دستار بر سر من چون سنگ آسیائیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسبت دهند خوبان با سرو قد خود را</p></div>
<div class="m2"><p>باشد سری بتان را هر جا برهنه پائیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از بس که باغبانان کردند پنبه در گوش</p></div>
<div class="m2"><p>هر جغد در گلستان مرغ سخن سرائیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون زلف خویش آن شوخ پیچیده سر ز پندم</p></div>
<div class="m2"><p>هر حرف من به گوشش گویا هزار پائیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آغاز عشق دل را ای سیدا میازار</p></div>
<div class="m2"><p>باو مکن مدارا یار نوآشنائیست</p></div></div>