---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>تیغ ابرویش اگر مد نظر باشد مرا</p></div>
<div class="m2"><p>ذوالفقار شاه مردان بر کمر باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنجه اش را کرده رنگین چون حنا از خون من</p></div>
<div class="m2"><p>بهر دامنگیریش دست دگر باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رود برگ خزان از جای با اندک نسیم</p></div>
<div class="m2"><p>پیرم و بر سر تمنای سفر باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از رطوبت خار و دیوار چمن گل می کند</p></div>
<div class="m2"><p>روز حشر امید از مژگان تر باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناوک بی پر نمی سازد ز ترکش سر برون</p></div>
<div class="m2"><p>در درون سینه آه بی اثر باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدر حاجتمند را محتاج می داند که چیست</p></div>
<div class="m2"><p>گوش همچون حلقه بر آواز در باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سفر چون آسیا نبود مرا اندیشه‌ای</p></div>
<div class="m2"><p>آب بر پا توشه ره به کمر باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لاله ام رو در بیابان عدم خواهم نهاد</p></div>
<div class="m2"><p>زاد ره داغ دل و خون جگر باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدا می گردد آخر غنچه باغ دلگشا</p></div>
<div class="m2"><p>چین پیشانی دوای دردسر باشد مرا</p></div></div>