---
title: >-
    شمارهٔ ۴۱۲
---
# شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>در چمن از گریه آبی بر رخ گل می زنم</p></div>
<div class="m2"><p>آتشی در غنچه منقار بلبل می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تعلق دست می شویم چو ابر نوبهار</p></div>
<div class="m2"><p>می شوم دیوانه و پا بر سر پل می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرده پروانه را تا دیده ام بر پای شمع</p></div>
<div class="m2"><p>خویش را در آب و آتش بی تأمل می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تردد پا به دامن می کشم محراب وار</p></div>
<div class="m2"><p>پنجه ای در پنجه اهل توکل می زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حاجت دربان نباشد خانه زنجیر را</p></div>
<div class="m2"><p>دور اگر اینست خود را بر تسلسل می زنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من چه کردم تیره بختان را سرآمد گشته ام</p></div>
<div class="m2"><p>بهر عرض حال خود زانو به کاکل می زنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پریشانی به گلشن دست بر سر می نهم</p></div>
<div class="m2"><p>خلق پندارند بر دستار سنبل می زنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیدا دندان بدگو را خموشی بشکند</p></div>
<div class="m2"><p>بر دهان خصم خود سنگ تغافل می زنم</p></div></div>