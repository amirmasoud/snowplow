---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>تا به آن گلگون قبا چون سایه همدوشیم ما</p></div>
<div class="m2"><p>پیرهن در خون خود آلوده می پوشیم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش ما ای شمع لاف صحبت آرایی مزن</p></div>
<div class="m2"><p>صد زبان داریم همچون شانه خاموشیم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بال قمری سرو را باشد حصار عافیت</p></div>
<div class="m2"><p>یار در هر جا بغل بگشاید آغوشیم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از صفای سینه بی کینه گوهر می کنیم</p></div>
<div class="m2"><p>چون صدف یک قطره آبی که می نوشیم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزها چون سایه همراهیم هر جا می رویم</p></div>
<div class="m2"><p>شب چو می گردد ز یاد او فراموشیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در لباس شبروی کردست پنهان خویش را</p></div>
<div class="m2"><p>داغ داغ از دست آن ماه سیه پوشیم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می پرستانیم از سودای فردا فارغیم</p></div>
<div class="m2"><p>هر چه با ما می دهند امروز می نوشیم ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پاس خاطر داری ای آئینه از ما یاد گیر</p></div>
<div class="m2"><p>عیب های خلق می بینیم و می پوشیم ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدا جز شکوه نبود پیشه بازار گیر</p></div>
<div class="m2"><p>نیست توفیری به هر کاری که می کوشیم ما</p></div></div>