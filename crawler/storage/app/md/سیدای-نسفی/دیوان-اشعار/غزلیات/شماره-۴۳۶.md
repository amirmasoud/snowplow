---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>روزگاری شد که خون از دیده تر می خوریم</p></div>
<div class="m2"><p>در بهشت افتاده ایم و آب کوثر می خوریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغز اندر استخوان داریم و محنت روزیم</p></div>
<div class="m2"><p>سنگها از هر طرف چون بسته بر سر می خوریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما کباب سینه بی کینه یار خودیم</p></div>
<div class="m2"><p>باده گلگون تر از خون کبوتر می خوریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بسته ایم از خلق چشم و کامرانی می کنیم</p></div>
<div class="m2"><p>شسته ایم از بحر دست و آب گوهر می خوریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما کجا ای خضر آب چشمه حیوان کجا</p></div>
<div class="m2"><p>رحم بر لب تشنگی های سکندر می خوریم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر شیرین بیستون را کوهکن گهواره کرد</p></div>
<div class="m2"><p>ما چو طفلان شیر از پستان مادر می خوریم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می توان پی برد از پیشانی خورشید و ماه</p></div>
<div class="m2"><p>پیش پاهایی که از چرخ ستمگر می خوریم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با زبان دوستی از خلق گیریم انتقام</p></div>
<div class="m2"><p>خوان مردم آشکارا همچو نشتر می خوریم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آب حیوان تا قیامت با خضر پیوند باد</p></div>
<div class="m2"><p>ما شراب زندگی از دست دلبر می خوریم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیدا چون غنچه سر بر زانوی خود مانده ام</p></div>
<div class="m2"><p>کس چه داند ما غم فردای محشر می خوریم</p></div></div>