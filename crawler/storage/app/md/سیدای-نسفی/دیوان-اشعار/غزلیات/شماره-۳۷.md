---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>ز بخت تیره بود تازه داغ لاله ما</p></div>
<div class="m2"><p>فتیله آه بود در چراغ ناله ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بحر کاسه گرداب پر نمی گردد</p></div>
<div class="m2"><p>چگونه آب نگردد دل پیاله ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهانیان در گفت و شنود بربستند</p></div>
<div class="m2"><p>نصیب رخنه دیوار شد رساله ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دماغ سوختگان را که می کند روشن</p></div>
<div class="m2"><p>تهی شدست ز روغن چراغ لاله ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خانه پا نگذاریم سیدا بیرون</p></div>
<div class="m2"><p>مباد کهنه براتی شود حواله ما</p></div></div>