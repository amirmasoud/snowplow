---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>در باغ اثر ز ناله بلبل نمانده است</p></div>
<div class="m2"><p>امروز در بساط چمن گل نمانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست هیچ کس گرهی وا نمی شود</p></div>
<div class="m2"><p>ربطی میان شانه و کاکل نمانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مانند سبزه سرو لب جو کند سراغ</p></div>
<div class="m2"><p>آزاده را ز بس که توکل نمانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دام پاره گوشه نشینان خورند خاک</p></div>
<div class="m2"><p>گیرایی کمند تغافل نمانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیلاب برده خانه ارباب جود را</p></div>
<div class="m2"><p>در جویبار اهل کرم پل نمانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر کشیده کوه ز فرهاد انتقام</p></div>
<div class="m2"><p>در بزرگان وقت تحمل نمانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل ز بیضه سر نکشد از برهنگی</p></div>
<div class="m2"><p>جز جیب پاره در بدن گل نمانده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز ساکنان چمن کوچ کرده اند</p></div>
<div class="m2"><p>بر روی باغ سبزه و سنبل نمانده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سودای ما زیاده شد از فکرهای پوچ</p></div>
<div class="m2"><p>ما را دگر خیال تخیل نمانده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>امروز گشت سلسله جود منتهی</p></div>
<div class="m2"><p>در دور این گروه تسلسل نمانده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر باد رفته گلشن ایام سیدا</p></div>
<div class="m2"><p>خاری در آشیانه بلبل نمانده است</p></div></div>