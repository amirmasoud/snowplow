---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>ای بی‌لب تو خشک دهانِ پیاله‌ها</p></div>
<div class="m2"><p>وز دوریی تو غرقه به خون داغ لاله‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطی است آنکه بر رخ جانان دمیده است</p></div>
<div class="m2"><p>خوبان نوشته‌اند به نامش رساله‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در بزم عشق رتبه خرد و کلان یکی‌ست</p></div>
<div class="m2"><p>طفلان برابرند به هفتادساله‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایجادیان ز خوان کریمان برند فیض</p></div>
<div class="m2"><p>گردند سرخ‌روی ز مینا پیاله‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر چرخ فتنه‌بار نمایان ستاره نیست</p></div>
<div class="m2"><p>سوراخ‌هاست بر بدن او ز ناله‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن‌پروران به تربیت آدم نمی‌شوند</p></div>
<div class="m2"><p>بیهوده می‌دهند به فیلان نواله‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نبود مرا سری به جوانان خردسال</p></div>
<div class="m2"><p>دست من است و دامن هفتادساله‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کلک سیدا همه‌جا مشکبار شد</p></div>
<div class="m2"><p>گویا بریده‌اند به ناف غزاله‌ها</p></div></div>