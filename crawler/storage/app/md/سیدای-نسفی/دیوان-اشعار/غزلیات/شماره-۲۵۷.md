---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>دلم در کوی او رفتست حیرانم که چون آید</p></div>
<div class="m2"><p>نفس هرگه که با یادش برآرم بوی خون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا لیلی وشی کردست سرگردان به صحرایی</p></div>
<div class="m2"><p>که جای گردباد از خاک او مجنون برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا از سنگ پیدا می کند رزق هنرور را</p></div>
<div class="m2"><p>برای روزیی فرهاد شیراز بیستون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز پیچ و تاب آه من بکن اندیشه ای ظالم</p></div>
<div class="m2"><p>بترس از خانه زان ماری که بی افسون برون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردد مرگ سد راه گیر و دار عاشق را</p></div>
<div class="m2"><p>صدای تیشه در گوشم هنوز از بیستون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز دیوانخانه ارباب دولت پای کوته کن</p></div>
<div class="m2"><p>کزین درها به گوش آواز زنجیر جنون آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنرور می شناسد سیدا قدر هنرور را</p></div>
<div class="m2"><p>به تکلیف من دیوانه از صحرا جنون آید</p></div></div>