---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>بی پر و بالی درین گلشن هوس باشد مرا</p></div>
<div class="m2"><p>همچو مرغ بیضه عریانی قفس باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنه ام از سادگی می جویم امداد از حیات</p></div>
<div class="m2"><p>التماس از همدم کوته نفس باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاروان را گوش از غفلت به آواز دراست</p></div>
<div class="m2"><p>کوس رحلت بانگ پرواز مگس باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ شاخ شعله ای ای برق بر من رحم کن</p></div>
<div class="m2"><p>قوت پرواز بال از خار و خس باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس نمی گوید خبر از چشمه آب حیات</p></div>
<div class="m2"><p>خضر این ره آمد و رفت نفس باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواب خوش در خانه صیاد کردن ابلهیست</p></div>
<div class="m2"><p>تکیه چون صورت به دیوار قفس باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتی خود را به ساحل می رسانم همچو موج</p></div>
<div class="m2"><p>گر ازین دریا حبابی همنفس باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرغ آزادم ز من پرواز کردن رفته است</p></div>
<div class="m2"><p>خواب راحت زیر دیوار قفس باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس که کلکم از ریاضت نیشکر گردیده است</p></div>
<div class="m2"><p>بوریای خانه از بال مگس باشد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی بازار سمندر گرم از داغ من است</p></div>
<div class="m2"><p>حق بسیاری به آن آتش نفس باشد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در بلا بودن اسیران را به از بیم بلاست</p></div>
<div class="m2"><p>آشیان در گوشه بام قفس باشد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشته ام از فاقه بسیار تار عنکبوت</p></div>
<div class="m2"><p>انتظاری بر پر و بال مگس باشد مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیدا امروز از دزدان معنی فارغم</p></div>
<div class="m2"><p>خانه همچون بام زندان عسس باشد مرا</p></div></div>