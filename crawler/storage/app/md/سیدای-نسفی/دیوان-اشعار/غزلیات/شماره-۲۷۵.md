---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>آئینه را جمال تو صاحب نظر کند</p></div>
<div class="m2"><p>عکس رخ تو بی خبران را خبر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوتاه کن حدیث پریشانی مرا</p></div>
<div class="m2"><p>کلکم مباد شکوه ز لطف تو سر کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خون می خورد ز تربیت غنچه باغبان</p></div>
<div class="m2"><p>این طفل را مباد خدا بی پدر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا آمدم ز ملک عدم در ترددم</p></div>
<div class="m2"><p>ظلم است هر که از وطن خود سفر کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوران همان نفس کشد از شمع انتقام</p></div>
<div class="m2"><p>انگشت خود به روغن آبی که تر کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پوشیده نیست چشم خود از بزم روزگار</p></div>
<div class="m2"><p>این صندلیست دیدن او دردسر کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز اهل عمر گریز قلب آشنا شوی</p></div>
<div class="m2"><p>منشین به این گروه که صحبت اثر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مژگان چشم شوخ تو بر جان سیدا</p></div>
<div class="m2"><p>از روی لطف دوستی نیشتر کند</p></div></div>