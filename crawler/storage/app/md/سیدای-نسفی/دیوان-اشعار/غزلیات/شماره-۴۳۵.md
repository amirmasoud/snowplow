---
title: >-
    شمارهٔ ۴۳۵
---
# شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>نگاه یار با من بر سر جنگ است می دانم</p></div>
<div class="m2"><p>دل بی رحم او از آهن و سنگ است می دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سراغم می کند از هر کس و احوال می پرسد</p></div>
<div class="m2"><p>سلامم می دهد این جمله نیرنگ است می دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نظر از ابروی ساقی و مطرب برنمی دارم</p></div>
<div class="m2"><p>قد خم گشته همچون قامت چنگ است می دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن تکلیف سیر گلشنم ای باغبان دیگر</p></div>
<div class="m2"><p>به دوشم این قبای نارسا تنگ است می دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز اهل جاه دست مطلب خود کرده ام کوته</p></div>
<div class="m2"><p>به پای خواهشم این کفشها تنگ است می دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گلشن گوشه ویرانه ام ای سیدا خوشتر</p></div>
<div class="m2"><p>نوای جغد را خالی ز آهنگ است می دانم</p></div></div>