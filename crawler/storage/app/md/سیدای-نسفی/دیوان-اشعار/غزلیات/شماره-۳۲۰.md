---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>سبزه خط بخشد از لعل لب او جان به ابر</p></div>
<div class="m2"><p>می دهد این خضر آب از چشمه حیوان به ابر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشت ما بی حاصلان از تشنگی لب خشک ماند</p></div>
<div class="m2"><p>گوشه چشمی نما ای دیده گریان به ابر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جود ذاتی بس که از اهل مروت برده اند</p></div>
<div class="m2"><p>می ستاند گردم آبی دهد عمان به ابر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشت زار عالم از چشم تر ما خرم است</p></div>
<div class="m2"><p>در زمان ما ندارد حاجتی دهقان به ابر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آستین از گریه من حلقه گرداب شد</p></div>
<div class="m2"><p>می شود دریا چو بگذارد کسی دامان به ابر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سد راهم آن سر کو گر نگردد سیدا</p></div>
<div class="m2"><p>موج اشکم می رساند تیغ چون توفان به ابر</p></div></div>