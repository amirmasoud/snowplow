---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>میل خوبان ز اسیران به توانگر باشد</p></div>
<div class="m2"><p>در چمن غنچه گل را سخن از زر باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که دارد لب خاموش توانگر باشد</p></div>
<div class="m2"><p>دهن غنچه همه عمر پر از زر باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب در نظر زنده دلان یکرنگ است</p></div>
<div class="m2"><p>پیش آئینه بدو نیک برابر باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا توانی سر من زود درآور به کمند</p></div>
<div class="m2"><p>سایه دام است به آن صید که لاغر باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهره کیست برد پیش تو پیغام مرا</p></div>
<div class="m2"><p>سرخی نامه ام از بال کبوتر باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل آئینه شد از ناله من چشمه خون</p></div>
<div class="m2"><p>اشک من رخنه گر سد سکندر باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تیغ چون آب حیات است به پرریختگان</p></div>
<div class="m2"><p>می خورد خون خود آن مرغ که بی پر باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنت روی زمین صحبت میخواران است</p></div>
<div class="m2"><p>ای خوش آن روز که لب بر لب ساغر باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسن آن نیست بهر انجمن اندازد نور</p></div>
<div class="m2"><p>خانه روشن کند آن شمع که بی سر باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نیست غیر از لب شیرین تو افسانه من</p></div>
<div class="m2"><p>سخن قند همان به که مکرر باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با کلاه نمد و صورت رنگین امروز</p></div>
<div class="m2"><p>خامه موی در این شهر قلندر باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تن پرستان جهان پیر و نفس اندو هوا</p></div>
<div class="m2"><p>رهبر قافله مرده دلان خر باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیدا زود به خورشید رسانی خود را</p></div>
<div class="m2"><p>هر سحر چشم تو چون شبنم اگر تر باشد</p></div></div>