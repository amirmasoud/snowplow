---
title: >-
    شمارهٔ ۴۸۶
---
# شمارهٔ ۴۸۶

<div class="b" id="bn1"><div class="m1"><p>پیچیده با نگاهم از سیر باغ بی تو</p></div>
<div class="m2"><p>از سینه ام چو لاله گل کرده داغ بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه ام نشسته بیرون در چو کوران</p></div>
<div class="m2"><p>فانوس من ندیده روی چراغ بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو می کشی به اغیار من با دو چشم خونبار</p></div>
<div class="m2"><p>تو خوش دماغ بی من من بی دماغ بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون گل تمام زخمم دارم سری به ناصور</p></div>
<div class="m2"><p>الماس پاره ها را سازم سراغ بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گلبانگ عندلیبان از نوحه گر دهد یاد</p></div>
<div class="m2"><p>ماتم سرا نماید از دور باغ بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آید تبسم تو هر گه به خاطر من</p></div>
<div class="m2"><p>پیمانه نمک را ریزم به داغ بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کس ز هند آید پرسم ز خط و خالت</p></div>
<div class="m2"><p>خود را دهم تسلی برگشت زاغ بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای غنچه سیدا را سیر چمن مفرمای</p></div>
<div class="m2"><p>باشد سر بریده گلهای باغ بی تو</p></div></div>