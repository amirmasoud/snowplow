---
title: >-
    شمارهٔ ۴۱۹
---
# شمارهٔ ۴۱۹

<div class="b" id="bn1"><div class="m1"><p>شبنمم پهلو به روی بستر گل می زنم</p></div>
<div class="m2"><p>آتشی در غنچه منقار بلبل می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیره بختی بین که می پیچم سر از فرمان زلف</p></div>
<div class="m2"><p>پشت دستی از پریشانی به سنبل می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذکر حق از حلقه خلوت نشینان برده اند</p></div>
<div class="m2"><p>دست رد بر سینه اهل توکل می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر بازار پیش گلفروشان می روم</p></div>
<div class="m2"><p>دست خود چون غنچه بر همیان بی پل می زنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیدا ویرانه ام باغ بهار عالم است</p></div>
<div class="m2"><p>هر که بر سر وقتم آید بر سرش گل می زنم</p></div></div>