---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>با یار و دوست شیوه عهد و وفا نماند</p></div>
<div class="m2"><p>بر برگ کاه رابطه کهربا نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم به هم کنند چو بیگانگان سلوک</p></div>
<div class="m2"><p>در چشم هیچ کس نگه آشنا نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغان در آشیانه خورند استخوان خویش</p></div>
<div class="m2"><p>امروز روزیی ز برای هما نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بستند اهل جاه در خانهای خود</p></div>
<div class="m2"><p>در کوچه های شهر صدای گدا نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سنگین دلان شدند ز اهل طمع خلاص</p></div>
<div class="m2"><p>جذبی که بود در دل آهن ربا نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشکفته غنچه ها به گلستان خزان شدند</p></div>
<div class="m2"><p>دل گرمی که بود به باد صبا نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کردند جا بدیده مردم غبارها</p></div>
<div class="m2"><p>در چشم هیچ کس اثر از توتیا نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خیرگاه حاتم طی نیست پشه یی</p></div>
<div class="m2"><p>بر باد رفت و هیچ کسی را بجا نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطرب ز پا فتاد و به آخر رسید بزم</p></div>
<div class="m2"><p>آهنگ ها دگر شد و در نی نوا نماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رنگین کنند خلق کف از خون یکدگر</p></div>
<div class="m2"><p>امروز اعتبار به رنگ حنا نماند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از شمع انجمن دل پروانه سرد شد</p></div>
<div class="m2"><p>از بس که اعتبار به عهد و وفا نماند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>این بار سیدا به خدا تکیه می کنم</p></div>
<div class="m2"><p>در روزگار بس که مرا متکا نماند</p></div></div>