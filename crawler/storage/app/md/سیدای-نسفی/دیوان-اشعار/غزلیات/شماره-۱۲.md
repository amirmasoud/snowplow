---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>زهی فراش زلف عنبرینت طره شب‌ها</p></div>
<div class="m2"><p>سپند خال روی آتشینت چشم کوکب‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در این دوران ز میخواران صدایی برنمی‌آید</p></div>
<div class="m2"><p>چو چشم شیشه می تنگ گردیدست مشرب‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ردای شیخ و زنار برهمن رفته‌اند از کار</p></div>
<div class="m2"><p>ز غفلت داده‌اند از دست خود سررشته مذهب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود در چشم زاهد خلوت بی‌انجمن دوزخ</p></div>
<div class="m2"><p>معلم راست بی‌اطفال زندان کنج مکتب‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>متاع بی‌ثمر جویای چشم کور می‌باشد</p></div>
<div class="m2"><p>دکان وامی‌شود افسانه را چون شمع در شب‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>محال است از ته دل مهربان کردن حسودان را</p></div>
<div class="m2"><p>به افسون کی رود بدطینتی از طبع عقرب‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوایی ساز ای مطرب که مستان در خروش آیند</p></div>
<div class="m2"><p>که محتاج دم صورند این افسرده‌قالب‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به کویت شبروان از فتنه گردون حذر دارند</p></div>
<div class="m2"><p>کند کار عسس با دوربینان چشم کوکب‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در احسان بروی خویش اهل جود بربستند</p></div>
<div class="m2"><p>عبث واکرده می‌گردند ارباب طمع لب‌ها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا مانند گل عیش از گریبان سر برون آورد</p></div>
<div class="m2"><p>چو دست خویش کوته کردم از دامان مطلب‌ها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مشو از یاد حق یک ساعتی ای سیدا غافل</p></div>
<div class="m2"><p>که می‌گردد مربی در دو عالم ذکر یارب‌ها</p></div></div>