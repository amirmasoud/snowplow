---
title: >-
    شمارهٔ ۵۴۵
---
# شمارهٔ ۵۴۵

<div class="b" id="bn1"><div class="m1"><p>از کدامین چمن ای سرو روان می آیی</p></div>
<div class="m2"><p>همچو گل برزده دامان به میان می آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چین به ابرو زده چشم لبالب و غضب</p></div>
<div class="m2"><p>ترکش ناز پر از تیر و کمان می آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه را پیرهن هاله به تن چاک شده</p></div>
<div class="m2"><p>پرده بر روی کشیده ز کتان می آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آستین برزده و خنجر خونریز به دست</p></div>
<div class="m2"><p>ای جفاپیشه به قصد دل و جان می آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوی خون از سخن زیر لبت می آید</p></div>
<div class="m2"><p>همچو مینا به می آلوده دهان می آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می روی چون نگه از خانه برون چابک و چست</p></div>
<div class="m2"><p>زود می آیی و از دیده نهان می آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغها دست به دامان تو آویخته اند</p></div>
<div class="m2"><p>مگر آتش زده بر لاله ستان می آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیدا را به نگاه رم آهو دریاب</p></div>
<div class="m2"><p>گر به دلپرسی صاحب نظران می آیی</p></div></div>