---
title: >-
    شمارهٔ ۱۵۲
---
# شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>آنکه می گرید به حالم چشم خونین من است</p></div>
<div class="m2"><p>وانکه بردارد سرم از خاک بالین من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قامتش را کی ز آغوش نظر بیرون کنم</p></div>
<div class="m2"><p>چون پری در شیشه پنهان جان شیرین من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند تسلیم بر شاخ گلم از راه دور</p></div>
<div class="m2"><p>نارسا افتاده دستش آنکه گلچین من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرزوی لعل و مرجان کی بود جیب مرا</p></div>
<div class="m2"><p>تکمه چاک گریبان اشک رنگین من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می زند سیلی به صیقل جوهر آئینه ام</p></div>
<div class="m2"><p>چشم پوشیدن ز روی زینت آئین من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناله گرمم چراغ کشته روشن می کند</p></div>
<div class="m2"><p>میل آهم سرمه چشم جهان بین من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می تپد در خون ز رشک خامه من مدعی</p></div>
<div class="m2"><p>خصم بسمل کشته شمشیر خونین من است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دزد معنی سیدا هرگز نمی یابد رواج</p></div>
<div class="m2"><p>تیغ بر خود می‌کشد هرکس سخن‌چین من است</p></div></div>