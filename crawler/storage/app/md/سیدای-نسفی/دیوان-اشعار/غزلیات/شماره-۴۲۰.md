---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>بی جمالت در گلستان چاک چون گل می زنم</p></div>
<div class="m2"><p>سینه را بر سوزن مژگان بلبل می زنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه بیگانه روی بیابان نیستم</p></div>
<div class="m2"><p>خط یارم حلقه بر پهلوی سنبل می زنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند خاموشی صیاد گیرا دام را</p></div>
<div class="m2"><p>یار را از دور اگر بینم تغافل می زنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سیدا سودا اگر مشک خطای نیستم</p></div>
<div class="m2"><p>شانه مشاطه ام دستی به کاکل می زنم</p></div></div>