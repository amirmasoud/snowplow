---
title: >-
    شمارهٔ ۴۹۵
---
# شمارهٔ ۴۹۵

<div class="b" id="bn1"><div class="m1"><p>آمد بهار بر کف ساقی پیاله کو</p></div>
<div class="m2"><p>در صحن بوستان گل و بر دشت لاله کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل کرده غنچه و قفس از جوش نوبهار</p></div>
<div class="m2"><p>ای مرغ بال بسته تو را آه و ناله کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گردیده اند رام به صیاد آهوان</p></div>
<div class="m2"><p>آن وحشی که بود به چشم غزاله کو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کردی چو صفحه نامه اعمال خود سیاه</p></div>
<div class="m2"><p>ای بوالفضول مزد کتاب و رساله کو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر هوای نفس ز خلوت شدی برون</p></div>
<div class="m2"><p>ای شیخ شهر طاعت هفتاد سال کو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خط آمد و گرفت ز یار انتقام ما</p></div>
<div class="m2"><p>ای آنکه بود صاحب چندین حواله کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سیدا ز عشق نشانی به دهر نیست</p></div>
<div class="m2"><p>داغی که بود بر جگر ما و لاله کو</p></div></div>