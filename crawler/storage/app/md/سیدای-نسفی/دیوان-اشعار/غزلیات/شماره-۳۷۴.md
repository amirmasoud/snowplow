---
title: >-
    شمارهٔ ۳۷۴
---
# شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>روی دلی از آن بت سرکش نیافتم</p></div>
<div class="m2"><p>در روزگار باده بیغش نیافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی او به سوختن خود رضا شدم</p></div>
<div class="m2"><p>از غم کباب گشتم و آتش نیافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا حال من به یار کند مو به مو بیان</p></div>
<div class="m2"><p>یاری به خود چو زلف مشوش نیافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم به فکر موی میانش کمان کشی</p></div>
<div class="m2"><p>داغم از این که دست چو ترکش نیافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سیدا به غیر دل داغدار خود</p></div>
<div class="m2"><p>در عاشقی اسیر بلاکش نیافتم!</p></div></div>