---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>مرغ بی بالم گلستان کی هوس باشد مرا</p></div>
<div class="m2"><p>خنده گل چاک دیوار قفس باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>التماس سوزن از عیسی مریم کی کنم</p></div>
<div class="m2"><p>گر به نقش کفش پایی دسترس باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی شوم پابست تار و پود خود چون عنکبوت</p></div>
<div class="m2"><p>قوت پرواز اگر همچون مگس باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه نشکفته را گلچین به سر برد از چمن</p></div>
<div class="m2"><p>همچو گل جا در میان خار و خس باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نیست صیادی که در دامش کنم خود را اسیر</p></div>
<div class="m2"><p>شکرها گویم اگر جا در قفس باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای که می گویی خموشی سد راه قسمت است</p></div>
<div class="m2"><p>می کنم فریاد اگر فریادرس باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاروان بوی پیراهن نمی آید ز مصر</p></div>
<div class="m2"><p>ناله ها در دل گره همچون جرس باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از چمن عمریست دور افتاده ام ای باغبان</p></div>
<div class="m2"><p>چاکهای پیرهن چاک نفس باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تماشای چمن سر در گریبان کرده ام</p></div>
<div class="m2"><p>غنچه گل در نظر حفظ نفس باشد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از چمن زینهار پا بیرون منه ای عندلیب</p></div>
<div class="m2"><p>در گلستان غنچه های نیمرس باشد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پنجه امید پای سعی کوته کی کنم</p></div>
<div class="m2"><p>گر به شاخ آرزوها دسترس باشد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روزیی مرغ درون بیضه باشد بی حساب</p></div>
<div class="m2"><p>رزق از اندازه بیرون در قفس باشد مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در به روی آرزوها بسته ام چون سیدا</p></div>
<div class="m2"><p>تا به کی چشم طمع بر دست کس باشد مرا</p></div></div>