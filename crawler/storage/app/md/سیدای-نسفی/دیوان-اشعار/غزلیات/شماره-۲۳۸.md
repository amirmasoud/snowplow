---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>می شود هنگام پیری موی چون عنبر سفید</p></div>
<div class="m2"><p>این بیابان می شود زین برف سرتاسر سفید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس که شب تا روز می سوزم به یاد قد دوست</p></div>
<div class="m2"><p>استخوان گشت همچون شمع در پیکر سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده خورشید فلک از کهکشان بر کف عصا</p></div>
<div class="m2"><p>می رود در کوی او همچون گدای سر سفید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمرها بر آستانش سر نهادم سیدا</p></div>
<div class="m2"><p>خانه ام یک ره نشد از روی آن دلبر سفید</p></div></div>