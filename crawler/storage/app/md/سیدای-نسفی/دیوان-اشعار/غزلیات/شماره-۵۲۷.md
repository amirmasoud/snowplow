---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>سر مگو آنکه شود خم پی احسان کسی</p></div>
<div class="m2"><p>بشکن آن دست که بوسد لب دامان کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تمنای تو محراب بغل وا کرده</p></div>
<div class="m2"><p>سوی مسجد مرو از بهر خدا جان کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شانه بر دست چو مشاطه صبا می گردد</p></div>
<div class="m2"><p>خویش را جمع کن ای زلف پریشان کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای که از حال دل بی خبرم می پرسی</p></div>
<div class="m2"><p>همچو آئینه سراپا شده حیران کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمم از کاوش مژگان تو شد خانه مور</p></div>
<div class="m2"><p>مرحمت چشم نداریم ز مژگان کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که را می نگرم جامه چو گل چاک ز دست</p></div>
<div class="m2"><p>از غمت نیست درستی به گریبان کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون سکندر به لب تشنه ز عالم مفرست</p></div>
<div class="m2"><p>دم آبی بده ای چشمه حیوان کسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون نگین نام تو را نقش به دل ساخته ام</p></div>
<div class="m2"><p>دستگیری بکن ای لعل بدخشان کسی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما به تکلیف تو چون مهر بهر کوچه دوان</p></div>
<div class="m2"><p>از تو ما خانه بدوشیم تو مهمان کسی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیدا سینه ام از داغ گلستان شده است</p></div>
<div class="m2"><p>بسته ام چشم خود از سیر گلستان کسی</p></div></div>