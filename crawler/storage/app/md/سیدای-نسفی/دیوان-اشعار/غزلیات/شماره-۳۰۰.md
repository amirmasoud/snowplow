---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>فصل خزان رسید و نشاط و طرب نماند</p></div>
<div class="m2"><p>کاری مرا به مشغله روز و شب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد خزان ربود حرارت ز آفتاب</p></div>
<div class="m2"><p>در شمع بزم اهل جهان تاب و تب نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تمکین ز شیخ شد ز مریدان سکوت رفت</p></div>
<div class="m2"><p>در بزرگان حیا و به طفلان ادب نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باغ امیدواریی ما را هوا رسید</p></div>
<div class="m2"><p>در عهد ما به نخل تمنا رطب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد صفحه ها سفید زبان قلم خموش</p></div>
<div class="m2"><p>بر لوح سینه ها رقم منتخب نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا خیرگاه حاتم طی گشت سرنگون</p></div>
<div class="m2"><p>جز خاک توده یی به دیار عرب نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مردود کرد خصم تو را دهر سیدا</p></div>
<div class="m2"><p>در هیچ سینه دوستی بولهب نماند</p></div></div>