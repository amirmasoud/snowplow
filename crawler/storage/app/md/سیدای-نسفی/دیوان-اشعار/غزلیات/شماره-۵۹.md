---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>خاک مجنون داد تا بر باد آه سرد ما</p></div>
<div class="m2"><p>خواهد آمد خانه خیز اکنون به صحرا گرد ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کهربا در دست ما کی می تواند آب ریخت</p></div>
<div class="m2"><p>می کشد بی آبرویی پیش رنگ زرد ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه خواهد جست همچون نبض بیماران ز جا</p></div>
<div class="m2"><p>گر به پشت او گذارد بار خود را درد ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لشکر ما خاکساران را به چشم کم مبین</p></div>
<div class="m2"><p>شهسواری هست پنهان در میان گرد ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیدا با دردمندان دیار معرفت</p></div>
<div class="m2"><p>اشک سرخ و چهره کاهیست راه آورد ما</p></div></div>