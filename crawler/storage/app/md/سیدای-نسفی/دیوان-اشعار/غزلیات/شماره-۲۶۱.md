---
title: >-
    شمارهٔ ۲۶۱
---
# شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>بی تو امشب ساغر و پیمانه ام در جنگ بود</p></div>
<div class="m2"><p>می به کام شیشه ام چون در فلاخن سنگ بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چراغ خانه ام هر دم نفس می شد گره</p></div>
<div class="m2"><p>وقت همچون غنچه گل بر چراغم تنگ بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغر عیشم به آب کهربا می شست روی</p></div>
<div class="m2"><p>برگ گلزار نشاطم با خزان یکرنگ بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پرده گوش من از قانون گرانی می کشید</p></div>
<div class="m2"><p>ناله مطرب ندانم در کدام آهنگ بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نگاهم تا سحر چون اشک آتش می پرید</p></div>
<div class="m2"><p>همچو شبنم خواب آسایش به چشمم تنگ بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چین کلفت را نمی برد از جبینم موج می</p></div>
<div class="m2"><p>دست روشنگر رخ آئینه ام را زنگ بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا اکنون شکفتن از بهارم رفته است</p></div>
<div class="m2"><p>باغبان گلشنم زین بیش آب و رنگ بود</p></div></div>