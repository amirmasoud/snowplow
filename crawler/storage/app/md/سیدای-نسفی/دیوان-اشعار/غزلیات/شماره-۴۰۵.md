---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>دل را اسیر خط بناگوش کرده ام</p></div>
<div class="m2"><p>این خانه را چو کعبه سیه پوش کرده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا روم به سوی توام هوش می برد</p></div>
<div class="m2"><p>عمریست راه خانه فراموش کرده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایمن کنم ز خیره نگاهان رخ تو را</p></div>
<div class="m2"><p>آئینه را گرفته نمدپوش کرده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مژگان من چو سنبل تر سبز گشته است</p></div>
<div class="m2"><p>زلف تو شب به خواب در آغوش کرده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مأوای من که تنگ تر از روی ناخن است</p></div>
<div class="m2"><p>همچون نگین گرفته و خاموش کرده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برتر بتم رسیده مرا نام برده یی</p></div>
<div class="m2"><p>سر در کفن کشیده ام و گوش کرده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سیدا به بحر گهر داده ام عوض</p></div>
<div class="m2"><p>هر قطره یی که همچو صدف نوش کرده ام</p></div></div>