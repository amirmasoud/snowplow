---
title: >-
    شمارهٔ ۵۰۹
---
# شمارهٔ ۵۰۹

<div class="b" id="bn1"><div class="m1"><p>پنجه‌ام هرگز نرفته بر در کاشانه‌ای</p></div>
<div class="m2"><p>نقش پای من ندیده آستان خانه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن زلف تمنا کی به چنگ آید مرا</p></div>
<div class="m2"><p>نی زبان گفت‌و‌گو و نی اصول شانه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغر خود می‌برم پیش حباب از سادگی</p></div>
<div class="m2"><p>تر نمی‌گردد ز دریاها لب پیمانه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر در ارباب دولت نیست غیر از گردباد</p></div>
<div class="m2"><p>پشّه‌ای بیرون نمی‌آید ز مهمانخانه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جود حاتم بر زبان سایلان گر بگذرد</p></div>
<div class="m2"><p>می‌رسد در گوش این دون‌همتان افسانه‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای ز خود غافل مکن از اهل دنیا آرزو</p></div>
<div class="m2"><p>کل دهند این طفل‌طبعان سنگ بر دیوانه‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دهان مردم غافل‌زبانِ هرزه‌گوی</p></div>
<div class="m2"><p>جغد بی‌بالی‌ست افتادست در ویرانه‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌روند آش خدایی گفته اهل روزگار</p></div>
<div class="m2"><p>گر برآید دود آهی از درون خانه‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشه‌چینان خرمن ناموس را آتش زنند</p></div>
<div class="m2"><p>از دهان مور اگر ناگاه افتد دانه‌ای</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیدا پا از بنای خلق کوته کرده‌ام</p></div>
<div class="m2"><p>می‌نماید پیش چشمم آسمان ویرانه‌ای</p></div></div>