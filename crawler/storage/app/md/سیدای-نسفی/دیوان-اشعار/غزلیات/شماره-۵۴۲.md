---
title: >-
    شمارهٔ ۵۴۲
---
# شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>شمع بزمی و چو یوسف به نظر می آیی</p></div>
<div class="m2"><p>از کدام انجمن ای جان پدر می آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست مرغان چمن را خبر از آمدنت</p></div>
<div class="m2"><p>بس که چون بوی گل و باد سحر می آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو چون سایه نفس سوخته در دنبالت</p></div>
<div class="m2"><p>از کجا برزده دامن به کمر می آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چهره افروخته پوشیده قبای گل نار</p></div>
<div class="m2"><p>بر سر سوختگان همچو شرر می آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا جلوه کنی سبز شود شاخ نبات</p></div>
<div class="m2"><p>همچو طوطی مگر از کان شکر می آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی نبردست کسی جای تو شبها به کجاست</p></div>
<div class="m2"><p>می روی شام چو خورشید و سحر می آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشنه گان را ز لب خود دم آبی ندهی</p></div>
<div class="m2"><p>گر چه سیرابتر از لعل و گهر می آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شبنم از روی تو می ریزد و گل می روید</p></div>
<div class="m2"><p>از کدامین چمن ای غنچه تر می آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدا پیکر خود فرش رهت ساخته است</p></div>
<div class="m2"><p>بامیدی که تو از خانه به در می آیی</p></div></div>