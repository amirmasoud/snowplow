---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ز برق تیغ ابرویت فتاد آتش به کشورها</p></div>
<div class="m2"><p>مه نو گشت میل آتشین در چشم اخترها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتار تن خاکیست روح از پستی همت</p></div>
<div class="m2"><p>به دام افتاده است این مرغ از کوتاهی پرها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندارد مادر از تأدیب فرزند خود آسایش</p></div>
<div class="m2"><p>صدف را سینه پر نم می گذارد حفظ گوهرها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباشد رونقی در عهد ما کامل عیاران را</p></div>
<div class="m2"><p>نهان در پرده زنگار گردیدند جوهر ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز نخل خشک آخر بهره می گیرند حق گویان</p></div>
<div class="m2"><p>به مقصد می رساند واعظان را چوب منبرها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود احوال من معلوم او از جبهه قاصد</p></div>
<div class="m2"><p>بود مکتوب من منقوش بر بال کبوترها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جای آب مردم بس که خون یکدگر خوردند</p></div>
<div class="m2"><p>حباب آسا شدند از مغز خالی کاسه سرها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ارباب طمع خوان کریمان تخته بندی شد</p></div>
<div class="m2"><p>کشادی نیست بر روی کسی دیگر از این درها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نباشد جز تردد روزیی دنیاپرستان را</p></div>
<div class="m2"><p>به زیر بار ناکامی بمیرند آخر این خرها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به روی اهل عالم سفره خود پهن اگر سازی</p></div>
<div class="m2"><p>شود روز قیامت بر سرت بر پای چادرها</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گدازد خون گرمم سیدا مژگان خوبان را</p></div>
<div class="m2"><p>به قتل من چو برگ بید می لرزند خنجرها</p></div></div>