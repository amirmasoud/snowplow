---
title: >-
    شمارهٔ ۲۳۱
---
# شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>هر کس که زین محیط دم آب می خورد</p></div>
<div class="m2"><p>تا هست پیچ و تاب چو گرداب می خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب زنده دار منت ساقی نمی کشد</p></div>
<div class="m2"><p>تا صبح می ز ساغر مهتاب می خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستی که ز احتساب نیاید به خویشتن</p></div>
<div class="m2"><p>سیلی ز نیشتر به رگ خواب می خورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون داغ لاله هر که سیه کاسه اوفتاد</p></div>
<div class="m2"><p>از تشنگی ز چشمه خون آب می خورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آرزوی خود نرسد دل ز اضطراب</p></div>
<div class="m2"><p>این تشنه آب گفته و سیماب می خورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گوشه یی نشین و به دیوار رو بیار</p></div>
<div class="m2"><p>شیخ آب و نان ز پشته محراب می خورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای سیدا مربی عالی طلب نمای</p></div>
<div class="m2"><p>نیک و بدی که هست به ارباب می خورد</p></div></div>