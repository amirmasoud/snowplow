---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>تکلم از دهانش گر ز تنگی دیر می ریزد</p></div>
<div class="m2"><p>تبسم از لب شیرین او چون شیر می ریزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صورتخانه دل شوخ نقاشی که من دارم</p></div>
<div class="m2"><p>ز کلک خویش جان در قالب تصویر می ریزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل فولاد را از چرب و نرمی موم می سازم</p></div>
<div class="m2"><p>به مغز استخوان من دم شمشیر می ریزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگردد وا گره از رشته تسبیح زاهد را</p></div>
<div class="m2"><p>پی این عقده ها دندان آن بی پیر می ریزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهد گردون به صد اندیشه کام تنگدستان را</p></div>
<div class="m2"><p>از آن شبنم به کام غنچه گل دیر می ریزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مجنون بس که بر ویرانی منزل سری دارم</p></div>
<div class="m2"><p>به دوش من غبار از خانه زنجیر می ریزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روند اهل طمع دنبال قاتل بر سر و گردن</p></div>
<div class="m2"><p>اگر دارند جوهر از دم شمشیر می ریزد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز خون رنگین دهان محتسب را دیدم و گفتم</p></div>
<div class="m2"><p>می از پیمانه من آخر این بی پیر می ریزد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر یأجوج دوران بشکند سد سکندر را</p></div>
<div class="m2"><p>تغافل بر در ارباب همت قیر می ریزد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی باشد دلم را سیدا ذوقی به آبادی</p></div>
<div class="m2"><p>مرا بر سر غبار کلفت از تعمیر می ریزد</p></div></div>