---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>بی تو امشب بوی خون می آید از کاشانه ام</p></div>
<div class="m2"><p>سنگ می بارد از دیوار و در بر خانه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از درون شیشه من تا سحر می رفت خون</p></div>
<div class="m2"><p>بی لبت می ریخت می از جبهه پیمانه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چراغ خانه ام می کرد روغن کار آب</p></div>
<div class="m2"><p>باد صرصر بود لرزان از پر پروانه ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتشی در خرمن بنیاد من افتاده بود</p></div>
<div class="m2"><p>برق همچون خوشه چین می گشت گرد دانه ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیکرم چون موی آتشدیده پیچ و تاب داشت</p></div>
<div class="m2"><p>زلف بی تابی جدا می کرد دست از شانه ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع از بی طاقتی می جست از جا چون سپند</p></div>
<div class="m2"><p>آب می گردید و می شد کوه اگر همخانه ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا از خانه ام تا آن پری رو رفته است</p></div>
<div class="m2"><p>بعد از این در کوچه و بازارها دیوانه ام</p></div></div>