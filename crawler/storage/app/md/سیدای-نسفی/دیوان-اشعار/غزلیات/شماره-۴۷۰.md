---
title: >-
    شمارهٔ ۴۷۰
---
# شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>شبی ای شمع در آغوش ما جا می توان کردن</p></div>
<div class="m2"><p>چو گل در گلشن ما سینه را وا می توان کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرا یک ره نظر بر عالم ای نوخط نمی سازی</p></div>
<div class="m2"><p>بهار آمد گلستان را تماشا می توان کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دکان واکرده در بازار محتاج خریداریم</p></div>
<div class="m2"><p>متاع کم بها داریم و سودا می توان کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوی شیر آمد رخنه ها در بیستون پیدا</p></div>
<div class="m2"><p>به نرمی کوه را از جای بیجا می توان کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدح را تا کی ای ساقی نهان در آستین داری</p></div>
<div class="m2"><p>گهی سوی حریفان دست بالا میتوان کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلزار را ای باغبان تا چند بربندی</p></div>
<div class="m2"><p>به حال عندلیبان گاه پروا میتوان کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشک سرخ و رنگ کهربای سیدا بنگر</p></div>
<div class="m2"><p>لبالب دامن از گلهای رعنا میتوان کردن</p></div></div>