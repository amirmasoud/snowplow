---
title: >-
    شمارهٔ ۴۲۸
---
# شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>شمعم و پروانه ام با شد دل بی کینه ام</p></div>
<div class="m2"><p>رخنه دیوار فانوس است چاک سینه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سبکروحان به خاطرها نمی آید گران</p></div>
<div class="m2"><p>عکس را از خود نمی سازد جدا آئینه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گنه کاران نوید مغفرت آب بقاست</p></div>
<div class="m2"><p>روز عید میکشان باشد شب آدینه ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دلم سودای زلف او نمی آید برون</p></div>
<div class="m2"><p>حلقه ماراست زنجیر در گنجینه ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردش دوران نگردد بر مرادم سیدا</p></div>
<div class="m2"><p>کهنه تقویمم به دست مردم پارینه ام</p></div></div>