---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>به کوی انتظاری آرمیدن ها چه می دانی</p></div>
<div class="m2"><p>گل از شاخ نهال صبر چیدن ها چه می دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز هم شیر از لبهای شیرین در قدح داری</p></div>
<div class="m2"><p>شراب تلخ ناکامی چشیدن ها چه می دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگه از گوشه چشم تو سر بیرون نمی آرد</p></div>
<div class="m2"><p>به هر سو اضطراب آلوده دیدن ها چه می دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شود از سایه مژگان عاشق پایت آزرده</p></div>
<div class="m2"><p>مذاق خار از پا کشیدن ها چه می دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز راحت بستر و از ناز بالین زیر سر داری</p></div>
<div class="m2"><p>تصور کردن و از جا پریدن ها چه می دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هنوز هم در چمن داری ز بلبل صد قفس بسمل</p></div>
<div class="m2"><p>به دام افتادن و در خون تپیدن ها چه می دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز ای گل ندیده غنچه ای روی تبسم را</p></div>
<div class="m2"><p>ز حسرت پشت دست خود گزیدنها چه می دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نهد دوش تو پهلو بر زمین از سایه کاکل</p></div>
<div class="m2"><p>به زیر بار کلفت آرمیدن ها چه می دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود در آستین پنهان چو بوی غنچه انگشتت</p></div>
<div class="m2"><p>گل از خار جای یار چیدن ها چه می دانی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هنوز ای شوخ با مژگان عاشق می کنی یاری</p></div>
<div class="m2"><p>تو نور چشم از مردم رمیدن ها چه می دانی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سراسر می روی هر روز بازار محبت را</p></div>
<div class="m2"><p>زلیخا نیستی یوسف خریدن ها چه می دانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چاک جیب ما و سیدا داری تبسم ها</p></div>
<div class="m2"><p>تو طفلی ذوق پیراهن دریدن ها چه می دانی</p></div></div>