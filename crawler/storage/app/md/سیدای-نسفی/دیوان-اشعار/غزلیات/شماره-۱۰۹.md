---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>پی صید افگنی بیرون شد از بزم شراب امشب</p></div>
<div class="m2"><p>ز شاخ شعله در پرواز شد مرغ کباب امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و معشوق در یک پیرهن بودیم از مستی</p></div>
<div class="m2"><p>گریان کتان را بخیه می زد ماهتاب امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خواب خویش تا صبح قیامت برنمی خیزم</p></div>
<div class="m2"><p>نهادم سر به بالین و تو را دیدم به خواب امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز چشم چون نگه بیرون شدی گفتی که باز آیم</p></div>
<div class="m2"><p>مگر از جانب مغرب برآمد آفتاب امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گلشن آمدی و جلوه را دادی سرافرازی</p></div>
<div class="m2"><p>چو باد صبح دادی سرو را پا در رکاب امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشستم تا سحر مانند شمع از وعده خامت</p></div>
<div class="m2"><p>به گرد چشم من می گشت ناله چنگ و رباب امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث ابروان دلکشت تکرار می کردم</p></div>
<div class="m2"><p>به روی صفحه بالین چوبیت انتخاب امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دنیا فتنه ها پیدا شود از غفلت شاهان</p></div>
<div class="m2"><p>تو مست افتادی و شد انجمن بی آب و تاب امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مجلس آمدی و شمع را چون سیدا کشتی</p></div>
<div class="m2"><p>کشیدی باده و پروانه را کردی کباب امشب</p></div></div>