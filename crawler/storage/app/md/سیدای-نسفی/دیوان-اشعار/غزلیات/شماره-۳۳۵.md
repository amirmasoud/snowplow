---
title: >-
    شمارهٔ ۳۳۵
---
# شمارهٔ ۳۳۵

<div class="b" id="bn1"><div class="m1"><p>با که روشن سازم احوال دل افگار خویش</p></div>
<div class="m2"><p>تا یکی چون شمع سوزم بر سر بیمار خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر آسایش گر از غمخانه سر بیرون کنم</p></div>
<div class="m2"><p>می نهم چون سایه پهلو بر ته دیوار خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف من از خریداران کسادی می کشد</p></div>
<div class="m2"><p>بر دکان آتش زنم از سردیی بازار خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر من آسیا گردد تحمل می کنم</p></div>
<div class="m2"><p>چون نمی بینم کسی را زیر گردون بار خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بدن از ناله . . .</p></div>
<div class="m2"><p>. . . را خویش</p></div></div>