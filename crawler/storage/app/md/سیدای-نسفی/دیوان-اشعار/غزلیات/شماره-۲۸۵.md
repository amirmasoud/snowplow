---
title: >-
    شمارهٔ ۲۸۵
---
# شمارهٔ ۲۸۵

<div class="b" id="bn1"><div class="m1"><p>دل قمری سرو قد رعنای محمد</p></div>
<div class="m2"><p>سر در هوس نقش کف پای محمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیچیده دو گیسوی کمندش ز دو جانب</p></div>
<div class="m2"><p>چون سنبل تر بر رخ زیبای محمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جبریل که سر خیل جمیع ملک آمد</p></div>
<div class="m2"><p>تاج سر او کرد قدم های محمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون نافه آهو بود امروز مدینه</p></div>
<div class="m2"><p>لبریز ز بوی چمن آرای محمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیاط ازل دوخته با سوزن تقدیر</p></div>
<div class="m2"><p>پیراهن اقبال به بالای محمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شاخ گل و سرو به گلزار نبوت</p></div>
<div class="m2"><p>ممتاز بود قامت یکتای محمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید و مه و مشتری و زهره و سید</p></div>
<div class="m2"><p>هستند شب و روز به سودای محمد</p></div></div>