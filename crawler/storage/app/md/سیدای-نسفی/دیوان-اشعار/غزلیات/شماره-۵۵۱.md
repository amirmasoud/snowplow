---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>تا لبت نام برآورده به شیرین سخنی</p></div>
<div class="m2"><p>طوطیان را به سر افتاده غم کوهکنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گردبادم به بیابان شده ام سرگردان</p></div>
<div class="m2"><p>دامن دشت جنون است به دوشم کفنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسمل فتنه چشمان تو آهوی خطا</p></div>
<div class="m2"><p>بسته زلف تو سوداگر مشک ختنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو گل چاک زده جیب حسینی بر دوش</p></div>
<div class="m2"><p>سرو پوشیده به یاد تو لباس حسنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی قراری ز من و از تو تماشا کردن</p></div>
<div class="m2"><p>با تو خرگاه نشینی و به من بی وطنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نسیم سحر و بوی گل آزرده شوی</p></div>
<div class="m2"><p>سنگ بر سینه زند پیش تو نازک بدنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چمن سرو ندارد قد رعنای تو را</p></div>
<div class="m2"><p>پیرهن زعفری و رنگ قبا یاسمنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صف مژگان تو از صف شکنان برده گرو</p></div>
<div class="m2"><p>ترک چشم تو بود منظر راه زنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طوطیان بر لب خود مهر خموشی زده اند</p></div>
<div class="m2"><p>چغد را داده فلک منصب شکرشکنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خلق گیرند چو آئینه فولاد به زر</p></div>
<div class="m2"><p>هر که را هست به بر خلعت روئینه تنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تابش نور مه از پرده برون می آید</p></div>
<div class="m2"><p>رفته از دوش من اندیشه بی پیرهنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>میوه خلد کجا نعمت دیدار کجا</p></div>
<div class="m2"><p>سیب جنت نکند دعویی سیب ذژقنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سیدا اهل هنر عزت دیگر دارند</p></div>
<div class="m2"><p>نرسد آهوی وحشی به غزال ختنی</p></div></div>