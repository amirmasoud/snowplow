---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>به سوی کلبه ام ای سیمبر نمی آیی</p></div>
<div class="m2"><p>خبر نکرده چرا بی خبر نمی آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز چشم من شده یی چون پری به شیشه نهان</p></div>
<div class="m2"><p>چه دیده یی که مرا در نظر نمی آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تشنگی لب من گشته خشک همچو صدف</p></div>
<div class="m2"><p>چرا ز بحر برون چون گهر نمی آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسیر دام تو را نیست قوت پرواز</p></div>
<div class="m2"><p>به دستگیریی این مشت پر نمی آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خانه ای که چو خورشید روی میاری</p></div>
<div class="m2"><p>طلوع تا نکند صبح برنمی آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کله شکسته و چون صبح سینه واکرده</p></div>
<div class="m2"><p>کمر گشاده ز موی کمر نمی آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به جستجوی تو گردیده سوده پا و سرم</p></div>
<div class="m2"><p>به دیدن من بی پا و سر نمی آیی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به باغ دهر چو شبنم سفید شد چشمم</p></div>
<div class="m2"><p>هنوز ای گل صدبرگ تر نمی آیی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پرسش دل بیمار سیدای غریب</p></div>
<div class="m2"><p>چرا تو از همه کس پیشتر نمی آیی</p></div></div>