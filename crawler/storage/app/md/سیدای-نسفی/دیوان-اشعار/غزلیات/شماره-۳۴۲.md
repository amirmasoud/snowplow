---
title: >-
    شمارهٔ ۳۴۲
---
# شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>روز محشر بزم دست سوی افسر خویش</p></div>
<div class="m2"><p>بید مجنونم و خود سایه کنم بر سر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدف من نگشادست دهن پیش سحاب</p></div>
<div class="m2"><p>خاک مالیده حبابم به لب ساغر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله دور است ز زنجیر در گوشه نشین</p></div>
<div class="m2"><p>نیستم منفعل از حلقه گوش کر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنچه خسپیست مرا کار چو مرغان قفس</p></div>
<div class="m2"><p>خواب آسایش من هست به زیر پر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاد راه سفر ملک عدم ایثار است</p></div>
<div class="m2"><p>در چمن دوخته گل چشم به مشت زر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر خود در قدم دشمن خود بگذارم</p></div>
<div class="m2"><p>می زنم بوسه کف پای ملامت گر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنچه ام خاطرم از گفت و شنودن جمع است</p></div>
<div class="m2"><p>روزگاریست که آسوده ام از دفتر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیدا لعل ز کان آمد و شد صاحب نام</p></div>
<div class="m2"><p>بهتر آنست هنرور رود از کشور خویش</p></div></div>