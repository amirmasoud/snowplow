---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>بهر خصم از خسروان امداد می باید گرفت</p></div>
<div class="m2"><p>انتقام کوه از فرهاد می باید گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی بر زلف دل آویز تو می باید نهاد</p></div>
<div class="m2"><p>حلقه زنجیر عدل و داد می باید گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند تعظیم پیش ساغر و می می دهد</p></div>
<div class="m2"><p>خلق احسان را ز مینا یاد می باید گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طبع روشن تیره گردد از تمناهای نفس</p></div>
<div class="m2"><p>این چراغ از رهگذار باد می باید گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل صد پاره ام راحت مجو ای آسمان</p></div>
<div class="m2"><p>خرج و باج از کشور آباد می باید گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر قتل بیگناهان بیع ها دارد فلک</p></div>
<div class="m2"><p>تیغ کین از دست این جلاد می باید گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به کی مغرور خود باشی تو ای دنیاپرست</p></div>
<div class="m2"><p>عبرت از فرعون و از شداد می باید گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر بنای قصر هستی تکیه چون صورت مکن</p></div>
<div class="m2"><p>پشت از این دیوار بی بنیاد می باید گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می کند دوران تو را آخر به تنهایی اسیر</p></div>
<div class="m2"><p>خود به دام و دانه ی صیاد می باید گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فتنه را ای نرگس از بادام چشمان یاد گیر</p></div>
<div class="m2"><p>این سبق تعلیم از استاد می باید گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دل او سیدا بیرون نمی گردد ستم</p></div>
<div class="m2"><p>جوهر از آئینه فولاد می باید گرفت</p></div></div>