---
title: >-
    شمارهٔ ۲۷۶
---
# شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>فصل خزان رسید ز گلها اثر نماند</p></div>
<div class="m2"><p>از بلبلان به باغ به جز بال و پر نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردیم عمر خویش چو گل صرف رنگ و بوی</p></div>
<div class="m2"><p>زاد رهی که بود برای سفر نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فانوس کرده در ته دامن چراغ را</p></div>
<div class="m2"><p>پروانه را کسی که شود راهبر نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنعان سپرد یوسف خود را به دست گرگ</p></div>
<div class="m2"><p>دیگر محبت پدری بر پسر نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبریز شد ز جوش گدا کوچه های شهر</p></div>
<div class="m2"><p>چندانکه بر نسیم تلاش گذر نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امروز بس که آئینه ها را گرفت زنگ</p></div>
<div class="m2"><p>خاصیتی که بود به صاحب نظر نماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دادند اهل جود به سایل جواب ها</p></div>
<div class="m2"><p>اکنون به این گروه به جز گوش کر نماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیغام ها ز یار شنیدیم و گل نکرد</p></div>
<div class="m2"><p>ما از این نهال امید ثمر نماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از گریه سیدا سر خاری نگشت سبز</p></div>
<div class="m2"><p>این بار اعتبار به مژگان تر نماند</p></div></div>