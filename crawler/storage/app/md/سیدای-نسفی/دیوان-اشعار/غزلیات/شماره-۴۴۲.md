---
title: >-
    شمارهٔ ۴۴۲
---
# شمارهٔ ۴۴۲

<div class="b" id="bn1"><div class="m1"><p>تازه می سازم ز برق ناله داغ خویشتن</p></div>
<div class="m2"><p>می کنم روشن به آه دل چراغ خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به کی ای لاله دامن می زنی بر آتشم</p></div>
<div class="m2"><p>روزگاری شد که می سوزم به داغ خویشتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزوهای سپندم مضطرب دارد مرا</p></div>
<div class="m2"><p>وقت آن آمد زنم آتش به باغ خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد از مرهم حذر پروانه داغ خودم</p></div>
<div class="m2"><p>می زنم گل بر سر خود از چراغ خویشتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه بر گرداب می پیچم گهی بر گردباد</p></div>
<div class="m2"><p>رفته ام از خود به سودای سراغ خویشتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرصت بر گرد خود گشتن نمی باشد مرا</p></div>
<div class="m2"><p>ساعتی از غم نمی یابم فراغ خویشتن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شام و صبح رفته من باز آید بر سرم</p></div>
<div class="m2"><p>می کنم هر شب تماشا گشت زاغ خویشتن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اهل صحبت سیدا عمریست سرگرم خودند</p></div>
<div class="m2"><p>با که همچون شمع می سوزی دماغ خویشتن</p></div></div>