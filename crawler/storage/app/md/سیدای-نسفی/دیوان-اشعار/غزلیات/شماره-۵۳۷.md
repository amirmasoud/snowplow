---
title: >-
    شمارهٔ ۵۳۷
---
# شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>دلم در سینه باشد در تنوری مرغ بریانی</p></div>
<div class="m2"><p>سرم در جیب گردد گردبادی در بیابانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو گل غلتیده ام در خون ز فکر جامه گلگونی</p></div>
<div class="m2"><p>گریبان چاکم از دست نگار پاکدامانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به پشت بام آن لیلی جبین مهتاب مجنون</p></div>
<div class="m2"><p>سر کو باشد از زنجیرمویان سنبلستانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مغز استخوانم سبز شد از بس که پیکانش</p></div>
<div class="m2"><p>مرا گردیده چاک سینه از تیرش خیابانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز دیوان بوستانی کرده بر پا معنی بکرم</p></div>
<div class="m2"><p>بود هر نخل مصراعش عروس نارپستای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خوان اهل دنیاتر نکرده دستم انگشتی</p></div>
<div class="m2"><p>قناعت کرده ام همچون مه نو با لب نانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عریانی سرم را در کنار آورده زانویم</p></div>
<div class="m2"><p>مرا از پیرهن باشد گریبانی و دامانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کشم از خوان خود شرمندگی و عذر پیش آرم</p></div>
<div class="m2"><p>به سر وقتم اگر سازد گذر ناگاه مهمانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آغوش پدر ای سیدا دارم لب خشکی</p></div>
<div class="m2"><p>مرا زادست مادر در چه وقتی در چه دورانی</p></div></div>