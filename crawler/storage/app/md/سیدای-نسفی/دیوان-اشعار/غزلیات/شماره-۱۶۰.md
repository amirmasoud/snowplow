---
title: >-
    شمارهٔ ۱۶۰
---
# شمارهٔ ۱۶۰

<div class="b" id="bn1"><div class="m1"><p>از رفیق رهنما طبع گدا آسوده است</p></div>
<div class="m2"><p>بر کف دست طمع کینان عصا آسوده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساران را بود بی اعتباری اعتبار</p></div>
<div class="m2"><p>از شکست خویش رنگ بوریا آسوده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرمی دوزخ چه خواهد کرد با آتش نفس</p></div>
<div class="m2"><p>از هجوم شعله کام اژدها آسوده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با سبکروحان گر آن جانان ندارند التفات</p></div>
<div class="m2"><p>برگ کاه از جذبه آهن ربا آسوده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزیی صاحب توکل می رسد از آسمان</p></div>
<div class="m2"><p>از تردد سنگ زیر آسیا آسوده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از هلاک سرکشان پروا نباشد تاج را</p></div>
<div class="m2"><p>از شکست استخوان طبع هما آسوده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علت ناصور از مرهم ندارد بهره یی</p></div>
<div class="m2"><p>از علاج چشم کوران توتیا آسوده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواب آسایش بود در دیده دزدان حرام</p></div>
<div class="m2"><p>پهلوی شبرو ز فکر متکا آسوده است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آسیا از دانه انجیر می سازد حذر</p></div>
<div class="m2"><p>خاطر دیوانه از ارض و سما آسوده است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>استخوان خشک بی تشویش کی آید به دست</p></div>
<div class="m2"><p>از پی روزی مگو طبع هما آسوده است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیدا مرد هنرمند از تردد فارغ است</p></div>
<div class="m2"><p>دست اگر در کار مشغول است پا آسوده است</p></div></div>