---
title: >-
    شمارهٔ ۴۸۹
---
# شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>شبها چو شمع دارم حال خراب بی تو</p></div>
<div class="m2"><p>گاهی روم در آتش گاهی در آب بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با غیر اگر چه حور است صحبت نمی برآید</p></div>
<div class="m2"><p>همچون کتان گریزم از ماهتاب بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ کباب گردد در بزم من سمندر</p></div>
<div class="m2"><p>طشت است پر ز آتش جام شراب بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر شکست اعضا چون موج سعی دارم</p></div>
<div class="m2"><p>خود را زنم به دریا همچون حباب بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جویبار جنت انگشت تر نسازم</p></div>
<div class="m2"><p>در چشم من نماید موج سراب بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سایه می گریزم در فکر جستجویت</p></div>
<div class="m2"><p>تنهارویست کارم ای آفتاب بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بزم می پرستان آب و نمک نباشد</p></div>
<div class="m2"><p>پرواز کرده رفته مرغ کباب بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا رخت خود ز کشتی بیرون کشیده رفتی</p></div>
<div class="m2"><p>پیچید به خود چو گرداب دریای آب بی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آید به بستر من خاصیت فلاخن</p></div>
<div class="m2"><p>پهلو اگر گذارم بر جامه خواب بی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عمر گذشته ام را از سیدا چه پرسی</p></div>
<div class="m2"><p>کی در حساب آید روز حساب بی تو</p></div></div>