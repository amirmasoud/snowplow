---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای بهارت را گل خودرو گریبان‌پاره‌ها</p></div>
<div class="m2"><p>خار دیوار گلستانت صف نظاره‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرم‌رفتاران ز عالم رخت هستی برده‌اند</p></div>
<div class="m2"><p>برق گرد کاروان شد از وطن آواره‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسبت عشاق چون یوسف خطا باشد ز جرم</p></div>
<div class="m2"><p>همچو گل چاک است دامان گریبان‌پاره‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کام دل از مردم دنیا گرفتن مشکل است</p></div>
<div class="m2"><p>تر نمی‌گردد لبی از آب این فواره‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی گلزار تو را نبود به شبنم احتیاج</p></div>
<div class="m2"><p>آب حسرت می‌چکد از دیده سیاره‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره‌های اشک از خشکی به مژگان شد گره</p></div>
<div class="m2"><p>می‌مکند انگشت خود طفلان این گهواره‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کوی تو ما و سیدا افتاده‌ایم</p></div>
<div class="m2"><p>دست ما گیر از کرم ای چاره بیچاره‌ها</p></div></div>