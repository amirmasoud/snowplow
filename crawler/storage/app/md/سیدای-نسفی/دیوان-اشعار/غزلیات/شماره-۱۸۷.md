---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>چو بهر پرسشم آن شهسوار می آید</p></div>
<div class="m2"><p>صف ملک ز یمین و یسار می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی که از غم او می زنم به دل ناخن</p></div>
<div class="m2"><p>صدای کوهکن از کوهسار می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برم چو نامه اعمال در ترازوگاه</p></div>
<div class="m2"><p>کتابتی که به سویم ز یار می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به باغ نوخط من دوش می کشید و هنوز</p></div>
<div class="m2"><p>نسیم مست صبا مشکبار می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چاک سینه روشندل خدا ترسم</p></div>
<div class="m2"><p>پیمبریست که بیرون ز غار می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز بال مرغ چمن شد قفس گل صد برگ</p></div>
<div class="m2"><p>که گفته است به بلبل بهار می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کدام لاله رخ امروز در چمن رفتست</p></div>
<div class="m2"><p>به هر که می نگرم داغدار می آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر آستانه خود همچو نقش پا عمریست</p></div>
<div class="m2"><p>نشسته ام به امیدی که بار می آید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کمند بوی گل آرد به باغ بلبل را</p></div>
<div class="m2"><p>دلم به سوی تو بی اختیار می آید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به گرد کوی تو هر روز گردباد از دشت</p></div>
<div class="m2"><p>به جستجوی من خاکسار می آید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر به کوه نهم پشت بر زمین ماند</p></div>
<div class="m2"><p>غمی که بر من از این روزگار می آید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگاه گرم تو می سوزد استخوانم را</p></div>
<div class="m2"><p>کجا ز برق ترحم بخار می آید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خجالتی که کشی از گناه خود امروز</p></div>
<div class="m2"><p>نگاه دار که فردا به کار می آید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سپندوار از آن کوی سیدا امروز</p></div>
<div class="m2"><p>چه شد که سوخته و بی قرار می آید</p></div></div>