---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>در باغ نخل خشک ز بادام مانده است</p></div>
<div class="m2"><p>در دهر ز اهل جود همین نام مانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق جهان به راه عجب اوفتاده اند</p></div>
<div class="m2"><p>یک سوی کفر و یک طرف اسلام مانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادند آنچه بود بزرگان به سایلان</p></div>
<div class="m2"><p>اکنون به اهل مرتبه دشنام مانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا لبی که بود دلم کام ازو گرفت</p></div>
<div class="m2"><p>این بار نردبان به لب بام مانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی زلف او شدست پریشان حواس ما</p></div>
<div class="m2"><p>ما را کجا دماغ سرانجام مانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر هر طرف که روی نهی پای کج منه</p></div>
<div class="m2"><p>ایام بر کنار رهت دام مانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتند اهل بزم ز ایام سیدا</p></div>
<div class="m2"><p>کار جهان به مردم خودکام مانده است</p></div></div>