---
title: >-
    شمارهٔ ۳۹۸
---
# شمارهٔ ۳۹۸

<div class="b" id="bn1"><div class="m1"><p>تا گرفتم خو به سحر نرگسش جادو شدم</p></div>
<div class="m2"><p>گوشه چشمی ز خالش دیدم و هندو شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوریی زنجیر رسوا می کند دیوانه را</p></div>
<div class="m2"><p>تا ز دستم رفت زلف او پریشان گو شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ژنده پوشیدن نگردد جمع با تن پروری</p></div>
<div class="m2"><p>خرقه پشمینه کردم تا به بر چون مو شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز پریشانی زلف او نصیب من نشد</p></div>
<div class="m2"><p>عمرها چون شانه خدمتگار آن گیسو شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دل خوبان توان از رفتگی کردند جای</p></div>
<div class="m2"><p>هر کجا آئینه یی دیدم سراپا رو شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقده ها دارم به دل از حرف پهلودار خلق</p></div>
<div class="m2"><p>تا چو بند جامه اش یک روز در پهلو شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکهت سنبل دماغم را پریشان کرد و رفت</p></div>
<div class="m2"><p>چون صبا تا محرم آن زلف عنبربو شدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قدر صاحب درد صاحب درد می داند که چیست</p></div>
<div class="m2"><p>در تفکر هم کجا دیدم سر زانو شدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می نوازم پشت تیغی بردم تیغ هلال</p></div>
<div class="m2"><p>آشنا تا با سلام گوشه ابرو شدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از ملامت سیدا یوسف عزیز مصر شد</p></div>
<div class="m2"><p>تا به بدنامی دریدم پیرهن نیکو شدم</p></div></div>