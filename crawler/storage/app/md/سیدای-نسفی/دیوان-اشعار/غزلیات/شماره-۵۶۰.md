---
title: >-
    شمارهٔ ۵۶۰
---
# شمارهٔ ۵۶۰

<div class="b" id="bn1"><div class="m1"><p>چرا به کلبه ام مهربان نمی آیی</p></div>
<div class="m2"><p>به دستگیری این ناتوان نمی آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در انتظار تو چشمم شدست خانه نشین</p></div>
<div class="m2"><p>قدم نهاده تو ای میهمان نمی آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشاده شب همه شب همچو هاله آغوشم</p></div>
<div class="m2"><p>به سویم ای مه نامهربان نمی آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریده از غم تو همچو کهربا رنگم</p></div>
<div class="m2"><p>تو ای بهار به سیر خزان نمی آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو سیدا به رهت چشم روز و شب دارم</p></div>
<div class="m2"><p>چو بوی گل ز نظرها نهان نمی آیی</p></div></div>