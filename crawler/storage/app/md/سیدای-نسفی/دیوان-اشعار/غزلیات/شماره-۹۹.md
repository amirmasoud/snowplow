---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>پیرم و بار گران بر کف عصا باشد مرا</p></div>
<div class="m2"><p>افتم از پا گر به پهلو متکا باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رحم می آید به سرگردانیم پروانه را</p></div>
<div class="m2"><p>خانه روشن از چراغ آسیا باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زبان خامه ام بیرون نمی آید صدا</p></div>
<div class="m2"><p>در گلستان عندلیب بینوا باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می زند پهلو کلاه من به تاج خسروی</p></div>
<div class="m2"><p>ساغر جم کاسه دست گدا باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می زند چون سبزه بیگانه بر من دست رد</p></div>
<div class="m2"><p>با وجود آنکه گلچین آشنا باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست همچون غنچه ام دلتنگی با مشت زر</p></div>
<div class="m2"><p>همچو برگ تاک دایم دست وا باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارم از فکر سخن پیوسته رو بر آسمان</p></div>
<div class="m2"><p>در گلستان سینه چون گل بر هوا باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شعله را نبود به جز بال سمندر قدردان</p></div>
<div class="m2"><p>شمعم و در دیده پروانه جا باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم پیران را تماشای چمن زیبنده است</p></div>
<div class="m2"><p>بید مجنونم نظر بر پشت پا باشد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیدا چون غافلان در بند هستی مانده ام</p></div>
<div class="m2"><p>بر کمر زنجیر از بند قبا باشد مرا</p></div></div>