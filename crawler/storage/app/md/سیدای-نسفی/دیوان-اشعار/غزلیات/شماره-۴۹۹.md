---
title: >-
    شمارهٔ ۴۹۹
---
# شمارهٔ ۴۹۹

<div class="b" id="bn1"><div class="m1"><p>ای گل نرگس فدای چشم چون بادام تو</p></div>
<div class="m2"><p>سرو چون فواره سیمابی آرام تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه چون فانوس از شمع جمالت روشن است</p></div>
<div class="m2"><p>ماه چون پروانه می گردد به گرد بام تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>احتیاج نامه قاصد نمی باشد مرا</p></div>
<div class="m2"><p>خط پشت لب به سویم آورد پیغام تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه چشمت به سوی من نمی افتد نظر</p></div>
<div class="m2"><p>من کیم تا بهره مندی یابم از انعام تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته همچون حلقه در خواب در چشمم حرام</p></div>
<div class="m2"><p>روزگاری شد که دارم گوش بر پیغام تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش پایت می دهد چشم غزالان را فریب</p></div>
<div class="m2"><p>دیده آهوست گویا حلقه های دام تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سروها کردند همچون سایه خود را خاکمال</p></div>
<div class="m2"><p>تا به گلشن جلوه گر شد سرو خوش اندام تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهر خاموشی به لب چون سیدا افگنده ام</p></div>
<div class="m2"><p>کرده ام همچون نگین خود را فدای نام تو</p></div></div>