---
title: >-
    شمارهٔ ۴۸۸
---
# شمارهٔ ۴۸۸

<div class="b" id="bn1"><div class="m1"><p>پریده رنگ ز گلها و لاله بی تو</p></div>
<div class="m2"><p>شدست خشک دهان پیاله ها بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز هیچ گوشه صدایی برون نمی آید</p></div>
<div class="m2"><p>گره شد به گلو آه و ناله ها بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کبوتری که برد نامه ها را نمی یابم</p></div>
<div class="m2"><p>نوشته طوطی کلکم رساله ها بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خمیده پشت جوانان به زیر بار غمت</p></div>
<div class="m2"><p>برابرند به هفتاد ساله ها بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی باغ بود سبزه نیش زهرآلود</p></div>
<div class="m2"><p>شدست چشمه خون داغ لاله ها بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستاده اند به یکجا چو آهوی تصویر</p></div>
<div class="m2"><p>نگه رمیده ز چشم غزاله ها بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دستبرد فلک ساغری به جای نماند</p></div>
<div class="m2"><p>شکسته سنگ حوادث پیاله ها بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کسی به داد دل ما و سیدا نرسید</p></div>
<div class="m2"><p>به گلشن آمده کردیم ناله ها بی تو</p></div></div>