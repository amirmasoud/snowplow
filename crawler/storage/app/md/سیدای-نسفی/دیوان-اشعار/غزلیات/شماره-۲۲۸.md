---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>وقت شد خط پا بر آن گلبرگ تر خواهد نهاد</p></div>
<div class="m2"><p>رسم و آئین تو را نوع دگر خواهد نهاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیر مژگان تو خواهد از کسادی خاک خورد</p></div>
<div class="m2"><p>چون کمان ابروی تو هر گوشه سر خواهد نهاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سواد زلف گردد حسن شوخت ناپدید</p></div>
<div class="m2"><p>شام چون آمد شفق رو در سفر خواهد نهاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فوطه زاری ز گردن فاخته خواهد گرفت</p></div>
<div class="m2"><p>سرو تو زین غصه بر دیوار سر خواهد نهاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پنجه زورآوران از دامنت کوتاه بود</p></div>
<div class="m2"><p>بهله دست اکنون بر آن موی کمر خواهد نهاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هندوی خط را اگر صدبار از پا افگنی</p></div>
<div class="m2"><p>چون ز جا برداشت سرپا پیشتر خواهد نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل نبندد سیدا غیر از تو با شوخ دگر</p></div>
<div class="m2"><p>پیش او از لب اگر حلوای تر خواهد نهاد</p></div></div>