---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>بید مجنونم سر خود دیده ام در پای خویش</p></div>
<div class="m2"><p>گر زنند آتش نمی جنبم چو شمع از جای خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاسه گردابم و ابر طمع جو نیستم</p></div>
<div class="m2"><p>می دهد چشمم به مردم آب از دریای خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می زنم بر استان اهل دولت پشت پا</p></div>
<div class="m2"><p>تا به دست آورده ام دامان استغنای خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشه ویرانه شهرستان نماید جغد را</p></div>
<div class="m2"><p>گردبادم می روم در دامن صحرای خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دکان دارم متاع کس میاب و کس مخر</p></div>
<div class="m2"><p>روزگاری شد خجالت دارم از کالای خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست کوته کرده ام از بزم اهل روزگار</p></div>
<div class="m2"><p>می برم خالی از این میخانه ها مینای خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در قفس افتادم و صیاد من آگه نشد</p></div>
<div class="m2"><p>داغم از دست بدام افتادن بیجای خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزی من می رساند سیدا روزی رسان</p></div>
<div class="m2"><p>مانده ام امروز بر فردا غم فردای خویش</p></div></div>