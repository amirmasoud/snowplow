---
title: >-
    شمارهٔ ۴۲۴
---
# شمارهٔ ۴۲۴

<div class="b" id="bn1"><div class="m1"><p>بس که دلگیر از تماشای گلستان گشته ام</p></div>
<div class="m2"><p>همچو بوی گل درون غنچه پنهان گشته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک تبسم کرده ام اجزای خود پاشیده ام</p></div>
<div class="m2"><p>چون دماغ گل ز خندیدن پریشان گشته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پشت بر دیوار هستی همچو صورت مانده ام</p></div>
<div class="m2"><p>چشم تا وا کرده ام بر خلق حیران گشته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر به پیش افگنده ام چشم از هوس پوشیده ام</p></div>
<div class="m2"><p>آرزو را تکمه چاک گریبان گشته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ دل را از ندامت چشمه خون کرده ام</p></div>
<div class="m2"><p>مدتی بهر دوا گرد طبیبان گشته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تن من از حوادث های دوران رخنه هاست</p></div>
<div class="m2"><p>از تحمل سیدا کوه بدخشان گشته ام</p></div></div>