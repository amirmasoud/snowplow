---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>خانه بر دوشم دو زانو متکا باشد مرا</p></div>
<div class="m2"><p>بستر و بالین ز نقش بوریا باشد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان باشد سیه بر چشم چون سرمه دان</p></div>
<div class="m2"><p>از زمین گردی که خیزد توتیا باشد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت آئینه منع از سیر گلشن می کند</p></div>
<div class="m2"><p>جبهه واکرده باغ دلگشا باشد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد خود هر روز می گردم برای دانه یی</p></div>
<div class="m2"><p>بر سر این دستار سنگ آسیا باشد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزیی من نیست از گردون به غیر از استخوان</p></div>
<div class="m2"><p>سایه بان گر بر سر از بال هما باشد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشته از بیمددگاری ضعیف و ناتوان</p></div>
<div class="m2"><p>می شوم بر پا اگر مویی عصا باشد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آهوی تصویر را در دام نتوانم کشید</p></div>
<div class="m2"><p>دست کوتاه و کمند نارسا باشد مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نسیم نوبهاران غنچه می گردد گلم</p></div>
<div class="m2"><p>باد ایام خزان باد صبا باشد مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوشه ها از دانه تسبیح خواهد سر کشید</p></div>
<div class="m2"><p>سبحه گردانی اگر بهر خدا باشد مرا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر کجا بینم دل خونین زیارت می کنم</p></div>
<div class="m2"><p>سینه پر داغ دشت کربلا باشد مرا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از دهانم دمبدم چون نافه آید بوی مشک</p></div>
<div class="m2"><p>روز و شب ورد زبان نام خدا باشد مرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون حباب آسایشی دارم ز اسباب جهان</p></div>
<div class="m2"><p>خانه مالامال از باد و هوا باشد مرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرو همچون سایه در دنبال من افتاده است</p></div>
<div class="m2"><p>با وجود آن که در بر یک قبا باشد مرا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دانه ای از کهکشان هرگز نصیب من نشد</p></div>
<div class="m2"><p>روزگاری شد که جا در آسیا باشد مرا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خار دیوار گلستان پاسبان گلشن است</p></div>
<div class="m2"><p>همچو مژگان در کنار دیده جا باشد مرا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خامه معجز بیان من عصای موسویست</p></div>
<div class="m2"><p>روز میدان بر کف دست اژدها باشد مرا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بخارا خامه ام از تشنگی خون می خورد</p></div>
<div class="m2"><p>این زمین بی مروت کربلا باشد مرا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سیدا حرف طمع در خاطرم گر بگذرد</p></div>
<div class="m2"><p>می شود بیگانه هر جا آشنا باشد مرا</p></div></div>