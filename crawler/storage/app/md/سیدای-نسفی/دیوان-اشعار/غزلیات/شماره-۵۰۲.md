---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>فصل نوروز است می‌آید هوای تازه‌ای</p></div>
<div class="m2"><p>بلبل و گل را شده برگ و نوای تازه‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر افتادگان افتاده سودای لباس</p></div>
<div class="m2"><p>سرو می‌خواهد درین موسم قبای تازه‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه تسبیح خود کردند حق‌گویان جدید</p></div>
<div class="m2"><p>فاخته افگنده در گردن ردای تازه‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داده خورشید فلک تبدیل جای خویش را</p></div>
<div class="m2"><p>تا شود آیینه‌رویان را صفای تازه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌توان دانست همچون شیشه می سیدا</p></div>
<div class="m2"><p>در دل هر کس که باشد مدعای تازه‌ای</p></div></div>