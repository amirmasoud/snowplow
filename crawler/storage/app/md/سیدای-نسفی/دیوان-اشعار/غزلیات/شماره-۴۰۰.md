---
title: >-
    شمارهٔ ۴۰۰
---
# شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>گر به کف مانند گل مشت زری می داشتم</p></div>
<div class="m2"><p>در بغل چون غنچه گلبرگ تری می داشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>راه دور عشق با مقراض پا نتوان برید</p></div>
<div class="m2"><p>قطع می کردم من این ره گر سری می داشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب می دادند چون دریا سخن های مرا</p></div>
<div class="m2"><p>چون صدف گر در کف خود گوهری می داشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سبز شد همچون لباس خضر زنگار دلم</p></div>
<div class="m2"><p>می شدم آئینه گر اسکندری می داشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مانده ام در خانه صیاد از بی قوتی</p></div>
<div class="m2"><p>می شکستم صد قفس را گر پری می داشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می شدم شب تا سحر هنگامه آرایش چو شمع</p></div>
<div class="m2"><p>پیش تیغش می نهادم گر سری می داشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم او را سیدا همکاسه می کردم به خود</p></div>
<div class="m2"><p>گر به کف مانند نرگس ساغری می داشتم</p></div></div>