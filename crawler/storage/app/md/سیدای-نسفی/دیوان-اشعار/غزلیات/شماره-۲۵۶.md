---
title: >-
    شمارهٔ ۲۵۶
---
# شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>خدای ذات تو را از بلا نگه دارد</p></div>
<div class="m2"><p>ز حادثات جهانت خدا نگه دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل حیات تو را روزگار تا دم حشر</p></div>
<div class="m2"><p>ز دستبرد نسیم صبا نگه دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهال عمر تو را همچو سرو تازه و تر</p></div>
<div class="m2"><p>خضر به چشمه آب بقا نگه دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که تکیه به دیوار دولتت سازد</p></div>
<div class="m2"><p>به زیر سایه بالش هما نگه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مروتی که تو را با سخن شناسان است</p></div>
<div class="m2"><p>همیشه در دل خود سیدا نگه دارد</p></div></div>