---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>دری گشاده نگردد اگر گدای شوم</p></div>
<div class="m2"><p>به استخوان قند آتش اگر همای شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا چو گرد به دنبال خویش نگذارند</p></div>
<div class="m2"><p>اگر به قافله خضر رهنمای شوم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کاکلش نگذارند بر کفم تاری</p></div>
<div class="m2"><p>اگر چو شانه سراسر گره گشای شوم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>متاع قافله ها نیست جز گرانی گوش</p></div>
<div class="m2"><p>فغان بلند نسازم اگر درای شوم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که در کف دل نیست یک درم داغی</p></div>
<div class="m2"><p>به بزم لاله عذاران چگونه جای شوم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر به چشمه آئینه عکس اندازم</p></div>
<div class="m2"><p>ز بخت تیره سراپا میان لای شوم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به بزم بی سر و پایان سریست خوبان را</p></div>
<div class="m2"><p>به مصلحت دو سه روزی برهنه پای شوم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به باد صبح مرا بس که سیدا خویشیست</p></div>
<div class="m2"><p>چه لازم است که بی خانه و سرای شوم</p></div></div>