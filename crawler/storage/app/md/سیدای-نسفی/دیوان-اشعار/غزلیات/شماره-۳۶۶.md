---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>خم گشت قد و با لب نانی نرسیدیم</p></div>
<div class="m2"><p>صدساله شدیم و به جوانی نرسیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بر نکشیدیم شبی سیمبری را</p></div>
<div class="m2"><p>آغوش شدیم و به میانی نرسیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون لاله ز سر منزل ما دود برآید</p></div>
<div class="m2"><p>داغیم که با سوخته جانی نرسیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ناوک پر ریخته از پای فتادیم</p></div>
<div class="m2"><p>از سعی کمانی به نشانی نرسیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بس که ندامت ز گنه نیست کسی را</p></div>
<div class="m2"><p>انگشت شدیم و به دهانی نرسیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون شمع شدیم انجمن آرای حریفان</p></div>
<div class="m2"><p>افسوس که با چرب زبانی نرسیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رفتیم و کشیدیم کمان همه کس را</p></div>
<div class="m2"><p>در معرکه سخت کمانی نرسیدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد نوخط ما پیر و کناری نگرفتیم</p></div>
<div class="m2"><p>هرگز به بهاری و خزانی نرسیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمریست که هستیم در این باغ چو سید</p></div>
<div class="m2"><p>با سرو قد غنچه دهانی نرسیدیم</p></div></div>