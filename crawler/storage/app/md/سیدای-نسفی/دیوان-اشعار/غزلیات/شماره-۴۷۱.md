---
title: >-
    شمارهٔ ۴۷۱
---
# شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>به خاک افتاده گرد سرمه چشم سیاهت من</p></div>
<div class="m2"><p>به خون غلطیده صید بسمل تیغ نگاهت من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسیر غنچه خندان لعلت صد قفس بلبل</p></div>
<div class="m2"><p>گریبان پیرهن چاک گل طرف کلاهت من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به گلشن بنده آزاد سرو قامتت قمری</p></div>
<div class="m2"><p>غلام حلقه در گوش خط سنبل پناهت من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گدای مفلس بی خان و مان پیر تهیدستی</p></div>
<div class="m2"><p>ز پا افتاده از خود رفته سرهای راهت من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بزم دلبران گر دعویی شیرین لبی سازی</p></div>
<div class="m2"><p>چو کوه بیستون امروز پا بر جا گواهت من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگه بر عارضت چون سیدا دزدیده می سازم</p></div>
<div class="m2"><p>چو پشت آئینه شرمنده روی چو ماهت من</p></div></div>