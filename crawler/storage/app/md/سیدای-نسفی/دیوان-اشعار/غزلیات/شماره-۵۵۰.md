---
title: >-
    شمارهٔ ۵۵۰
---
# شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>فلک از کهکشان چون سینه چاکست پنداری</p></div>
<div class="m2"><p>زمین از جوش غمناکان سر خاکست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم خونفشانم گر کنی نظاره مژگان را</p></div>
<div class="m2"><p>به گرداب اوفتاده مشت خاشاکست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرم کیفیتی چون باده بخشد جان سایل را</p></div>
<div class="m2"><p>کف دست کرامت پنجه تاکست پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریشان می کند از سرکشی مغز سر گل را</p></div>
<div class="m2"><p>به گلشن شاخ سنبل مار ضحاکست پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس همچون هدف گیرم به دندان ناوک خود را</p></div>
<div class="m2"><p>خدنگش در دهانم چوب مسواکست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز پابوسش جبینم سیدا روزی که آساید</p></div>
<div class="m2"><p>سرم تا دامن محشر به افلاکست پنداری</p></div></div>