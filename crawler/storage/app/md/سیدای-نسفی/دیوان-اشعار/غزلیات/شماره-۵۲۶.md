---
title: >-
    شمارهٔ ۵۲۶
---
# شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>رفتی و شوری به جان ناتوان انداختی</p></div>
<div class="m2"><p>آمدی و آتشی در خان و مان انداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناوک اندازان چشمت هر طرف در جلوه اند</p></div>
<div class="m2"><p>تا چو ابرو بر سر بازو کمان انداختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهربانیها نمودی اول و آخر چو شمع</p></div>
<div class="m2"><p>شعله بیدادیی در مغز جان انداختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خجالت چون کمان مشکل که سر بالا کنم</p></div>
<div class="m2"><p>تا مرا دور از نظر همچون نشان انداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عندلیبان از سیه بختی همه خون می خورند</p></div>
<div class="m2"><p>برگ گل را تا ز سنبل سایه بان انداختی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتشم در جان زدی و از نظر غائب شدی</p></div>
<div class="m2"><p>تشنه ام کردی و در ریگ روان انداختی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لطف ها کردی و افگندی به یکبار از نظر</p></div>
<div class="m2"><p>از زمین برداشتی وز آسمان انداختی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوی پیراهن که بودی نور چشم اهل دل</p></div>
<div class="m2"><p>آتشی کردی به جان کاروان انداختی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهر خندی کردی و بنیاد عالم سوختی</p></div>
<div class="m2"><p>این چه شوری بود از آن لب در جهان انداختی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفته بودی دوش خواهم ریخت خون سیدا</p></div>
<div class="m2"><p>خود یقین کردی و ما را در گمان انداختی</p></div></div>