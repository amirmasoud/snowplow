---
title: >-
    شمارهٔ ۴۲۱
---
# شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>نگاه باده پرست تو برده از هوشم</p></div>
<div class="m2"><p>سودا سرمه چشم تو کرده خاموشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تبسم لب تو تازه کرده داغ مرا</p></div>
<div class="m2"><p>نمی شود نمکت سالها فراموشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسیده است رگ و ریشه ام به سنبل تو</p></div>
<div class="m2"><p>به خدمتت ز غلامان حلقه در گوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به یک لباس همه عمر قانعم چون سرو</p></div>
<div class="m2"><p>قبای تازه کشیدست رخت از دوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می رسیده ام و آب سرد ریخته اند</p></div>
<div class="m2"><p>تمام آتشم اما فتاده از جوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو سیدا نگشایم زبان عجیب کسی</p></div>
<div class="m2"><p>نشسته قاصد غماز در بناگوشم</p></div></div>