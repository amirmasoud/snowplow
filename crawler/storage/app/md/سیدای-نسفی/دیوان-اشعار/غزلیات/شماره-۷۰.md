---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>از خزان محفوظ کن یارب گلستان مرا</p></div>
<div class="m2"><p>آب ده از جویبار خضر بستان مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بند بند من ز سستی از پی پاشیدنیست</p></div>
<div class="m2"><p>استخوان بندی کرم فرما نیستان مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کلبه ام را ماهتابی ده ز نور معرفت</p></div>
<div class="m2"><p>روشن از صبح سعادت کن شبستان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تندرستی و حیات و قوت طاعت بده</p></div>
<div class="m2"><p>پر ز نعمت های الوان کن سر خوان مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو گل سودا حواسم را مشوش کرده است</p></div>
<div class="m2"><p>جمع کن چون غنچه اوراق پریشان مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنجه ام را کامیاب از دامن امید کن</p></div>
<div class="m2"><p>دور کن ازدست نومیدی گریبان مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نامه ام را شستشویی ده ز دریای کرم</p></div>
<div class="m2"><p>کرده چشمم حلقه گرداب دامان مرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بندگان نام تو را خوانند ستارالعیوب</p></div>
<div class="m2"><p>روز محشر هم بکن پوشیده عصیان مرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای شفاده با تو روی آورده ام چون سیدا</p></div>
<div class="m2"><p>درد را چون داده یی خود ساز درمان مرا</p></div></div>