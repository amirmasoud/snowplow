---
title: >-
    شمارهٔ ۵۰۶
---
# شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>دل از خود رفته شوری بر من دیوانه افتاده</p></div>
<div class="m2"><p>به جستجوی این طفل آتشم در خانه افتاده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذر آن ماهرو را بر من دیوانه افتاده</p></div>
<div class="m2"><p>به پابوسی سرم بر آستان خانه افتاده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا آن شمع بی پروا نظر بر حالم اندازد</p></div>
<div class="m2"><p>که دلها در رهش چون مرده پروانه افتاده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوش بوالهوس از کوی او بیرون کنم خود را</p></div>
<div class="m2"><p>که این کشور به دست مردم بیگانه افتاده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جستجوی زلف او شکسته تا کمر پایم</p></div>
<div class="m2"><p>به دامنگیریش دستم جدا از شانه افتاده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به حرف آشنا هرگز دلش مایل نمی گردد</p></div>
<div class="m2"><p>بتی دارم که طبعش از سخن بیگانه افتاده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فلک ای سیدا هرگز به کام من نمی گردد</p></div>
<div class="m2"><p>ز دست کوته ام عمریست این پیمانه افتاده</p></div></div>