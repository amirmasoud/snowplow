---
title: >-
    شمارهٔ ۴۸۲
---
# شمارهٔ ۴۸۲

<div class="b" id="bn1"><div class="m1"><p>نمی نهم به تماشای گل قدم بی تو</p></div>
<div class="m2"><p>نمی زنم به کسی همچو غنچه دم بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کتابتی به تو انشا نمی توانم کرد</p></div>
<div class="m2"><p>فتاده است ز انگشت من قلم بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدست کشورم از رفتن تو زیر و زبر</p></div>
<div class="m2"><p>گرفته ملک دلم را سپاه غم بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به داغ سینه خود مرهمی که بگذارم</p></div>
<div class="m2"><p>فتیله می شود و می کند ستم بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو شمع شب همه شب سوختن بود کارم</p></div>
<div class="m2"><p>ز دیده ریخته اشک و نشسته ام بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به روی بستر خود خواب را نمی بینم</p></div>
<div class="m2"><p>چو چشم آهوی وحشی نموده رم بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل کباب من از جای برنمی خیزد</p></div>
<div class="m2"><p>نهاده آتش من سینه را به نم بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خط غبار جمال تو بود سر خط من</p></div>
<div class="m2"><p>قلم شکسته و گم کرده ام رقم بی تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز رفتن تو هلالی شدست ماه تمام</p></div>
<div class="m2"><p>شدست نور چراغ سپهر کم بی تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نمی کنند لبم تر ز چشمه زمزم</p></div>
<div class="m2"><p>نمی دهند مرا جای در هرم بی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو شمع ماتمیان دود شعله آهم</p></div>
<div class="m2"><p>در آسمان زده چون کهکشان علم بی تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غلام حلقه به گوش تو قمریان شده اند</p></div>
<div class="m2"><p>چو طوق فاخته گردیده سرو خم بی تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نمی کنی به لب خشک سیدا رحمی</p></div>
<div class="m2"><p>کشیده است ارادت ز جام جم بی تو</p></div></div>