---
title: >-
    شمارهٔ ۴۲۹
---
# شمارهٔ ۴۲۹

<div class="b" id="bn1"><div class="m1"><p>بس که خو کردست با کلفت دل غم پیشه ام</p></div>
<div class="m2"><p>سنگ می گردد اگر ریزند می در شیشه ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برقم و بر خرمن اهل ستم دارم گذر</p></div>
<div class="m2"><p>هر کجا خار مغیلان است باشد بیشه ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخل سبزم از کدامین جوی آبم داده اند</p></div>
<div class="m2"><p>سایه من بید مجنون است سنبل ریشه ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا آتش علم گردد پر و بال من است</p></div>
<div class="m2"><p>نیست چون مرغ کباب از سوختن اندیشه ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیدا با کوهکن از بس که دارم نسبتی</p></div>
<div class="m2"><p>نیست غیر از نام شیرین بر زبان تیشه ام</p></div></div>