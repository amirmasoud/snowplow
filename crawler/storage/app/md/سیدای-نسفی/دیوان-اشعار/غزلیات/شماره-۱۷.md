---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ای فرش بوی سنبل زلفت دماغ‌ها</p></div>
<div class="m2"><p>پامال شبنم گل روی تو باغ‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب بیا به کلبه‌ام ای رشک بوستان</p></div>
<div class="m2"><p>کز روغن گل است لبالب چراغ‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نامم میان سوختگان تا بلند شد</p></div>
<div class="m2"><p>خود را به لاله زار کشیدند داغ‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زان یار خانگی خبری هیچ کس نگفت</p></div>
<div class="m2"><p>لبریز شد ز آبله پای سراغ‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز بس که ریخته‌ای خون بلبلان</p></div>
<div class="m2"><p>بربسته شد به خلق ره کوچه‌باغ‌ها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آفتاب سوخته گشتند قمریان</p></div>
<div class="m2"><p>بر سایه‌های سرو نشستند زاغ‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هیچ دل نماند غم عشق سیدا</p></div>
<div class="m2"><p>شد پیر در خیال جوانان دماغ‌ها</p></div></div>