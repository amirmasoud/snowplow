---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>تا تو آلوده به خون ساخته‌ای خنجر خویش</p></div>
<div class="m2"><p>من برآورده ام از چاک گریبان سر خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا تو ای شعله جواله نمودار شدی</p></div>
<div class="m2"><p>شمع و پروانه نشستند به خاکستر خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ید بیضا شود و بوسه زند پای تو را</p></div>
<div class="m2"><p>بهر تسلیم تو دستی بنهم بر سر خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنبل زلف تو بازوی مرا خواهد تافت</p></div>
<div class="m2"><p>مار بسیار گرو بردن ز افسونگر خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیرخت سوختم و نیست قرارم چو سپند</p></div>
<div class="m2"><p>می کنم بازی طفلانه به خاکستر خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لب آب روان سرو برومند شود</p></div>
<div class="m2"><p>می دهم قد تو را جای به چشم تر خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به کجا شکوه کنم از ستم کاکل تو</p></div>
<div class="m2"><p>من خود انداخته ام روز سیه بر سر خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاق ابروی تو را مد نظر ساخته ام</p></div>
<div class="m2"><p>می دهم تیغ تو را آب ز چشم تر خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدا هست دماغم همه شبها روشن</p></div>
<div class="m2"><p>کرده ام روغن این شمع ز مغز سر خویش</p></div></div>