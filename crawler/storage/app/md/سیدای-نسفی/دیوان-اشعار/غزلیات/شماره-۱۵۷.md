---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>جود و سخا به مردم عالم نمانده است</p></div>
<div class="m2"><p>نام و نشان ز منزل حاتم نمانده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکسان کند به باغ تماشای خار و گل</p></div>
<div class="m2"><p>امروز امتیاز به شبنم نمانده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دل می صبوح کدورت نمی برد</p></div>
<div class="m2"><p>خاصیتی که بود به مرهم نمانده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گلها چو غنچه سر به گریبان کشیده اند</p></div>
<div class="m2"><p>در هیچ سینه یی دل خرم نمانده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر ز پیش خم به لب خشک می رود</p></div>
<div class="m2"><p>در شیشه ها باده کشان نم نمانده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باقیست گرد راه به مژگان حاجیان</p></div>
<div class="m2"><p>آبی مگر به چشمه زمزم نمانده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خانه های اهل کرم سیدا مرو</p></div>
<div class="m2"><p>آنجا به غیر صورت آدم نمانده است</p></div></div>