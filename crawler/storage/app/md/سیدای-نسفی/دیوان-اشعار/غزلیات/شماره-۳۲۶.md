---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>صد بیابان طی شد و از کاروان دورم هنوز</p></div>
<div class="m2"><p>کشتی توفانی دریای پرشورم هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم آخر گشت و دوران باده چندانی ندارد</p></div>
<div class="m2"><p>شد تهی میخانه افلاک و مخمورم هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرتو خورشیدم و دارم هوای کوی دوست</p></div>
<div class="m2"><p>عالم از من روشن است و طالب نورم هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از توکل روزیم هر روز می گردد زیاد</p></div>
<div class="m2"><p>خوشه چین خرمن ایام چون مورم هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج در ویرانه بانگ خیر مقدم می زند</p></div>
<div class="m2"><p>منزل من خانه جغد است معمورم هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمدی و خون عرق کردم ز بالینم مرو</p></div>
<div class="m2"><p>بر سر من ساعتی بنشین که رنجورم هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناوکت را می کشم خواهی نخواهی برکنار</p></div>
<div class="m2"><p>چون کمان در خانه بازو بود زورم هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مدتی شد ساغرم را کرده دوران سرنگون</p></div>
<div class="m2"><p>در شکست کاسه چینی و فغفورم هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدا یا آنکه دوران تلخ کامم کرده است</p></div>
<div class="m2"><p>می خلد چون نیش بر تن نوش زنبورم هنوز</p></div></div>