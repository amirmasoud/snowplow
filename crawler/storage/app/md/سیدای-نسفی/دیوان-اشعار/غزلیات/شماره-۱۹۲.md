---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>دیده چرخ به ظالم نگران خواهد بود</p></div>
<div class="m2"><p>تیر را گوشه ابرو ز کمان خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قسمت بیهوده گو در همه جا روزردیست</p></div>
<div class="m2"><p>از سفر سود جرس آه و فغان خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مال غارت زده از حاکم ده باید جست</p></div>
<div class="m2"><p>گرگ را نیست گنه جرم شبان خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر و شیشه ز مهمانی خم سیر شدند</p></div>
<div class="m2"><p>خانه اهل کرم از دگران خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سفله از حرص دم مرگ پشیمان گردد</p></div>
<div class="m2"><p>باغبان را کف افسوس خزان خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظاهر و باطن عشاق چو گل یکرنگ است</p></div>
<div class="m2"><p>هر چه باشد به دل ما به زبان خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظاهر و باطن عشاق چو گل یکرنگ است</p></div>
<div class="m2"><p>هر چه باشد به دل ما به زبان خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قصر دولت که به او خلق بنایی دارند</p></div>
<div class="m2"><p>چون حبابیست که بر آب روان خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روز محشر که ز هر گوشه علم جلوه دهند</p></div>
<div class="m2"><p>خاکساران تو را نام و نشان خواهد بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سبزه پشت لبت جوهر مغز جگر است</p></div>
<div class="m2"><p>ریشه سنبل زلفت رگ جان خواهد بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که چون غنچه گل کیسه پر زر دارد</p></div>
<div class="m2"><p>تکمه تاج سر اهل جهان خواهد بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منزل اول او گوشه کوثر گردد</p></div>
<div class="m2"><p>هر که را چشم به آن کنج دهان خواهد بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خامه حرفی ز خطش گفت بریدند سرش</p></div>
<div class="m2"><p>بی ادب کشته شمشیر زبان خواهد بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رشته تا گشت مرصع ز گهر دانستم</p></div>
<div class="m2"><p>چرخ در تربیت بی هنران خواهد بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نشوند اهل طمع دور ز اطراف تنور</p></div>
<div class="m2"><p>به امیدی که درو صورت نان خواهد بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سیدا دهر به نوکیسه نکو پردازد</p></div>
<div class="m2"><p>میل پیران ز مریدان به جوان خواهد بود</p></div></div>