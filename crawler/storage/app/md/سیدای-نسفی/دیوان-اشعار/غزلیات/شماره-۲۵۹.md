---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>فلک به قامت پیر خمیده می ماند</p></div>
<div class="m2"><p>جهان بدیهه تاراج دیده می ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهال گر به نظر نیزه است خون آلود</p></div>
<div class="m2"><p>به باغ سرو به تیر خزیده می ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام صید گشادست سینه را چو هدف</p></div>
<div class="m2"><p>که ابرویش به کمان کشیده می ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جوش لاله و گل باغ شد چنان گلگون</p></div>
<div class="m2"><p>که برگ تاک به دست بریده می ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس که اهل جهان خون یکدگر خوردند</p></div>
<div class="m2"><p>سر سپهر بنار مکیده می ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دست سبزخطان جام لاله گون در باغ</p></div>
<div class="m2"><p>به چشم آهوی سنبل چریده می ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمانه آب طراوت ز جوی گلشن برد</p></div>
<div class="m2"><p>زمین باغ به جیب دریده می ماند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون جامه رنگین خویش دنیا دار</p></div>
<div class="m2"><p>به کرمهای بریشم تنیده می ماند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مکن نصیحت دنیا به مردم ای واعظ</p></div>
<div class="m2"><p>که این سخن به حدیث شنیده می ماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دلم زیاد قفس سیدا ملول شد</p></div>
<div class="m2"><p>به مرغ خانه صیاد دیده می ماند</p></div></div>