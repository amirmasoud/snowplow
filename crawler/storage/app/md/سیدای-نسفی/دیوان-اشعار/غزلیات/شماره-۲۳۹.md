---
title: >-
    شمارهٔ ۲۳۹
---
# شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>ز دل پروانه آهم به لب مأیوس می آید</p></div>
<div class="m2"><p>مرا بوی چراغ کشته زین فانوس می آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زده سیلی به روی نامه من دست اقبالش</p></div>
<div class="m2"><p>که قاصد با لب خشک و کف افسوس می آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسازد برق همچون شمع مجلس گرم صحبت را</p></div>
<div class="m2"><p>کجا از مرغ دشتی جلوه طاووس می آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تبسم بر لب آید وقت رحلت غنچه خسپان را</p></div>
<div class="m2"><p>به گوش از خنده های گل صدای کوس می آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گرد خاطر گنجینه داران غم نمی گردد</p></div>
<div class="m2"><p>از این ویرانه ها این جغد را ناموس می آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چون سرو نام خود علم سازی به آزادی</p></div>
<div class="m2"><p>سرافرازی ز هر سو از پی پابوس می آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نباشد سیدا ربطی به هم زنار بندان را</p></div>
<div class="m2"><p>لب پرشکوه از بتخانه ها ناقوس می آید</p></div></div>