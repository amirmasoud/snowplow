---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>در آغوشم چو می‌آیی میان بهر خدا بگشا</p></div>
<div class="m2"><p>گره از کار من تا وا شد بند قبا بگشا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>متاع خویش نتوان کرد پنهان از خریداران</p></div>
<div class="m2"><p>دکان رنگ و بو ای غنچه در پیش صبا بگشا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مکن کوتاه دامان کرم از دست محتاجان</p></div>
<div class="m2"><p>گره از کیسه زر وا کن و چشم گدا بگشا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب بربسته گردد سد راه رزق بر سایل</p></div>
<div class="m2"><p>دهان خویش را بهر طلب چون آسیا بگشا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خود بیرون شود پیوند هستی را شکستی ده</p></div>
<div class="m2"><p>میان خویش را چون نی برویی بوریا بگشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم صبح است ای ساقی در میخانه را واکن</p></div>
<div class="m2"><p>بروی دردمندان رخنه دارالشفا بگشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مکن در نوبهاران پیشه خود غنچه خسبی را</p></div>
<div class="m2"><p>برو در باغ چون گل سینه بر کسب هوا بگشا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگه را گل به دامن از بهار صبح پیری کن</p></div>
<div class="m2"><p>چو شبنم چشم خود وقت سحر ای سیدا بگشا</p></div></div>