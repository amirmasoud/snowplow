---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>جانب ما دیده را وا کرده پوشیدن چرا</p></div>
<div class="m2"><p>آشنایی کردن و بیگانه گردیدن چرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چرخ را در ناله آرد تیر آه خستگان</p></div>
<div class="m2"><p>از نظر افتادگان را دیر پرسیدن چرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمه بیگانه را در چشم خود جا داده یی</p></div>
<div class="m2"><p>اینقدر از آشنایی دور گردیدن چرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خفتگان خاک را یاد رقیبان داغ کرد</p></div>
<div class="m2"><p>دوستان از یکدگر بیهوده رنجیدن چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>استماع صد سخن از خار چون گل می کنی</p></div>
<div class="m2"><p>یک سخن از عندلیب خویش نشنیدن چرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غم موی میان نازک او سیدا</p></div>
<div class="m2"><p>سنبل آسا اینقدر بر خویش پیچیدن چرا</p></div></div>