---
title: >-
    شمارهٔ ۱۳۵
---
# شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>ای مقدم تو قبله چشم تر من است</p></div>
<div class="m2"><p>هر جا که نقش پای تو باشد سر من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوابم بدیده بی تو چو شبنم بود حرام</p></div>
<div class="m2"><p>شبها اگر چه خرمن گل بستر من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی تو غبار مرا می برد نسیم</p></div>
<div class="m2"><p>هر جا که منزل تو بود محشر من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیرون شدم ز بیضه و گشتم اسیر دام</p></div>
<div class="m2"><p>ایام در شکنجه بال و پر من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون غنچه سایه پرور پیراهن خودم</p></div>
<div class="m2"><p>هر جا روم کلاه سرم چادر من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریا دلان به کس دم آبی نمی دهند</p></div>
<div class="m2"><p>گرداب چشمه تر ز لب ساغر من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ابر گریه را نکنم خار سیدا</p></div>
<div class="m2"><p>این اشک نیست طفل به جان پرور من است</p></div></div>