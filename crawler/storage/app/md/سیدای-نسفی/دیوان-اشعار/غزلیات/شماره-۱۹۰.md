---
title: >-
    شمارهٔ ۱۹۰
---
# شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>شمع ها جمله به فانوس وطن ساخته اند</p></div>
<div class="m2"><p>بهر پروانه خود گور و کفن ساخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عندلیبان دل صد پاره خود غنچه صفت</p></div>
<div class="m2"><p>جمع آورده به یکجا و چمن ساخته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله ها دست به دامان تو آویخته اند</p></div>
<div class="m2"><p>پیرهن بر تنت از برگ سمن ساخته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو هر جا که روی بوی چمن می آید</p></div>
<div class="m2"><p>چشمت از نرگس و از غنچه دهن ساخته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ملامت نکند اصل طمع اندیشه</p></div>
<div class="m2"><p>دل این طایفه را روئینه تن ساخته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزگارم همه عمر به سختی گذرد</p></div>
<div class="m2"><p>استخوان را چو هما روزیی من ساخته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا روی به روشن گهران باید کرد</p></div>
<div class="m2"><p>طوطیان آئینه را یار سخن ساخته اند</p></div></div>