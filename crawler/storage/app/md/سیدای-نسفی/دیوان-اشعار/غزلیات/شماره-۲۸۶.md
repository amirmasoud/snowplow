---
title: >-
    شمارهٔ ۲۸۶
---
# شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>دامن گلستانش تا مرا به چنگ آمد</p></div>
<div class="m2"><p>پیرهن بر اعضایم همچو غنچه تنگ آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلوه چون تذرو باغ جامه چون پر طاووس</p></div>
<div class="m2"><p>چهره چون گل رعنا با هزار رنگ آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته بر کمر ترکش تیغ شعله و آتش</p></div>
<div class="m2"><p>بر سر من آن سرکش از برای جنگ آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در محیط سودایش کشتی دل افگندم</p></div>
<div class="m2"><p>شد حباب او گرداب موج او نهنگ آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خط نامسلمانش داد عقل و دین بر باد</p></div>
<div class="m2"><p>بهر غارت روی لشکر فرنگ آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برشکست بزم من آسمان فلاخن شد</p></div>
<div class="m2"><p>بر دهان مینایم جای پنبه سنگ آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل بسر هوس کردم خارم از بدن گل کرد</p></div>
<div class="m2"><p>مرهم آرزو بردم ناخن پلنگ آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیدا شده فرهاد بر هلاک خود راضی</p></div>
<div class="m2"><p>تیشه بس که شد دلگیر بیستون به تنگ آمد</p></div></div>