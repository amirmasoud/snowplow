---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>نفس عمریست می پیچد مرا در سینه می ترسم</p></div>
<div class="m2"><p>عجب ماری شده پیدا در این گنجینه می ترسم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم بس که گردد شکلهای مختلف ظاهر</p></div>
<div class="m2"><p>چو بینم عکس خود در خانه آئینه می ترسم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شهر از شالپوشان بس که پیدا شد خیانتها</p></div>
<div class="m2"><p>به دوش هر که بینم خرقه پشمینه می ترسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به حق چشم مخمور خود ای ساقی شرابم ده</p></div>
<div class="m2"><p>سیه مست توام کی از شب آدینه می ترسم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به محشر می دهند ای سیدا اعضا گواهی را</p></div>
<div class="m2"><p>به روز گیرودار از همدم دیرینه می ترسم</p></div></div>