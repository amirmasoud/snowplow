---
title: >-
    شمارهٔ ۴۸۳
---
# شمارهٔ ۴۸۳

<div class="b" id="bn1"><div class="m1"><p>تا کی چو گل نشینم در خون تپیده بی تو</p></div>
<div class="m2"><p>خارم شکسته بر پا دستم بریده بی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی روی تو نگاهم صیدیست زخم خورده</p></div>
<div class="m2"><p>مژگان بود به چشمم تیر خلیده بی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بوی صندل آید درد سرم به فریاد</p></div>
<div class="m2"><p>از یاد سرمه افتد خاکم به دیده بی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگ خزان به رویم سیلی زنان نشسته</p></div>
<div class="m2"><p>ای گل بیا که عمریست رنگم پریده بی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از برق انتظاری بگداخت چشم زارم</p></div>
<div class="m2"><p>بر کشتزار صبرم آفت رسیده بی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر رشته امیدم از دست غم گسسته</p></div>
<div class="m2"><p>پیراهن حیاتم بر تن دریده بی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر زانوی تفکر دارم سر ارادت</p></div>
<div class="m2"><p>در زیر بار کلفت پشتم خمیده بی تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون سیدا نداریم با خویش آشنایی</p></div>
<div class="m2"><p>گردم به شهر و صحرا از خود رمیده بی تو</p></div></div>