---
title: >-
    شمارهٔ ۴۲۲
---
# شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>نمی شود دم پیری اجل فراموشم</p></div>
<div class="m2"><p>قد خمیده من حلقه ایست در گوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو هاله دائره مشربم نباشد تنگ</p></div>
<div class="m2"><p>هلال عیدم و باشد کشاده آغوشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز کوی باده فروشان نمی روم بیرون</p></div>
<div class="m2"><p>نموده اند می و برده اند از هوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز ز منزل سرگشتگان نمی یابم</p></div>
<div class="m2"><p>چو گردباد در این دشت خانه بر دوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توکلی که تهیدستیم کرم کرده</p></div>
<div class="m2"><p>اگر تمام جهان را دهند نفروشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بزم باده کشان نشاء نمی بینم</p></div>
<div class="m2"><p>همان به است که دوران کند فراموشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به یاد سیر گلستان انتظارم کن</p></div>
<div class="m2"><p>لبالب از گل خمیازه است آغوشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کمند زلف که وا کرده سیدا امروز</p></div>
<div class="m2"><p>ز جای خویش سراسیمه می رود هوشم</p></div></div>