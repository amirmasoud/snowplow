---
title: >-
    شمارهٔ ۳۳۶
---
# شمارهٔ ۳۳۶

<div class="b" id="bn1"><div class="m1"><p>زین گلستان سرو قدی را نکردم رام خویش</p></div>
<div class="m2"><p>همچو قمری بر گلو پیچیدم آخر دام خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعم و یک پیرهن مهتاب فانوس من است</p></div>
<div class="m2"><p>می کنم روشن ز روی خانه پشت بام خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگاری شد که دوران کرده سرگردان مرا</p></div>
<div class="m2"><p>بر کمر پیچیده ام چون گردباد آرام خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می کنم هر شب دماغ خشک خود از گریه چرب</p></div>
<div class="m2"><p>می کشم از دیده خود روغن بادام خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جواب تلخ گردد حرص سایل بیشتر</p></div>
<div class="m2"><p>دادن دشنام را داند گدا انعام خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حلقه بیرون در گردیده گوش باغبان</p></div>
<div class="m2"><p>بعد از این چون غنچه می پیچم زبان در کام خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می خورم چون شمع از پهلوی خود تا زنده ام</p></div>
<div class="m2"><p>می دهم خود را تسلی بر کباب خام خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مادر دوران به جای شیر خونم داد و رفت</p></div>
<div class="m2"><p>می توان دانست از آغاز کار انجام خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مجلس آرایان بقید هستی خود نیستند</p></div>
<div class="m2"><p>سرو آزاد است از رعنایی اندام خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آبروی ساغر خود تا به کی ریزم به خاک</p></div>
<div class="m2"><p>برده ام لب خشک از دریای فکرت جام خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پاس آب رو چو شبنم غنچه خسپم کرده است</p></div>
<div class="m2"><p>می خورم خون جگر از حفظ ننگ و نام خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صبح عمرم همچو شبنم در تردد بگذرد</p></div>
<div class="m2"><p>سازم از دود چراغ کشته روشن شام خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دانه سبز به کنج آسیا ای سیدا</p></div>
<div class="m2"><p>در کنار افتاده ام از گردش ایام خویش</p></div></div>