---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>باغبانی کو که آید گل در آغوشم کند</p></div>
<div class="m2"><p>جامه برگ کرم چون سرو بر دوشم کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو قدی کو که آید جا در آغوشم کند</p></div>
<div class="m2"><p>یاد عمر رفته از خاطر فراموشم کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می روم از بزم می ناخورده آن ساقی کجاست</p></div>
<div class="m2"><p>دست من بندد سبوی باده بر دوشم کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغرم چون کاسه گرداب عمری شد تهیست</p></div>
<div class="m2"><p>نیست در دریا حبابی رفته سرپوشم کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون کمانی حلقه سازم گوشه گیری اختیار</p></div>
<div class="m2"><p>پیش از آن ساعت که دوران خانه بر دوشم کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در چمن مشت پر من لایق تکلیف نیست</p></div>
<div class="m2"><p>باغبان را به که از خاطر فراموشم کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا از شادی و غم فارغ است آئینه ام</p></div>
<div class="m2"><p>گر به زر گیرد سپهرم ور نمد پوشم کند</p></div></div>