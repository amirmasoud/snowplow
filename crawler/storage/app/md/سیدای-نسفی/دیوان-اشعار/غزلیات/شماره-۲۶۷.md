---
title: >-
    شمارهٔ ۲۶۷
---
# شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>ای پسر آنها که پیش از برگ بارت داده اند</p></div>
<div class="m2"><p>بر علف زاری نشان گوساله وارت داده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خشک خواهد شد دماغت چون زمین شوره زار</p></div>
<div class="m2"><p>بس که آب از جویبار کوکنارت داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست بر دامان زلفت بردن آنجا مشکل است</p></div>
<div class="m2"><p>در ره هندوستان یاران قرارت داده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زودگردی چار پا و زین بدنامی به پشت</p></div>
<div class="m2"><p>زیر رانت گر چه رخش راهوارت داده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی دارالضرب قلابان اشارت کرده اند</p></div>
<div class="m2"><p>برده اند و وعده های بی شمارت داده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنگ شد عالم به چشمت از هجوم خط و زلف</p></div>
<div class="m2"><p>جای خوابی در میان مور و مارت داده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برده اند از جا دل خشت تو با نقش فریب</p></div>
<div class="m2"><p>تنگه های روی بستی در قمارت داده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صورت خود را به رنگ غازه کن بازار گیر</p></div>
<div class="m2"><p>نقش بینان دست بر نقش و نگارت داده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرمه خاموشی امشب به کارت کرده اند</p></div>
<div class="m2"><p>کاسه های می به چشم پیر خمارت داده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در گلستان برده سروت را ز پا افگنده اند</p></div>
<div class="m2"><p>گل به چشمانت نمایان کرده خارت داده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خیره چشمان کرده اند آئینه ات را همچو موم</p></div>
<div class="m2"><p>پشت کارت دیده اند و روی کارت داده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به تعلیمت زبانش نرم گردد همچو مغز</p></div>
<div class="m2"><p>پشت نان تازه بر آموزگارت داده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در پی حسن خود اکنون رخت بندی بهتر است</p></div>
<div class="m2"><p>بس که اینجا روزگار نابرارت داده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بند در پایت نشد محکم ز پند سیدا</p></div>
<div class="m2"><p>این زمان بر کف عنان اختیارت داده اند</p></div></div>