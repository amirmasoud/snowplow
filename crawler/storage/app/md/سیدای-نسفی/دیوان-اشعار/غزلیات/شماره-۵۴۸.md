---
title: >-
    شمارهٔ ۵۴۸
---
# شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>گل به سر برزده از سیر چمن می آیی</p></div>
<div class="m2"><p>خنده بر لب گره ای غنچه دهن می آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امشب ای ماه کجا ساخته باده کشتی</p></div>
<div class="m2"><p>می روی از خود و گاهی به سخن می آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شود آزرده میان تو در آغوش نظر</p></div>
<div class="m2"><p>نازک اندام تر از شاخ سمن می آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی سبب عیش مرا کرده پریشان رفتی</p></div>
<div class="m2"><p>باز از بهر چه ای عهد شکن می آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع و پروانه به فانوس حصاری شده اند</p></div>
<div class="m2"><p>چه بلایی تو که در خانه من می آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود از گرد رهت باد صبا مشک فروش</p></div>
<div class="m2"><p>نو خط من زیبا بان ختن می آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدا روز و شب از غیب ندا می آید</p></div>
<div class="m2"><p>ای مسافر شده ام کی به وطن می آیی</p></div></div>