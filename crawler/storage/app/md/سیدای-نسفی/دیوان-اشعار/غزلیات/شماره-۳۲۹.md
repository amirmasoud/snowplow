---
title: >-
    شمارهٔ ۳۲۹
---
# شمارهٔ ۳۲۹

<div class="b" id="bn1"><div class="m1"><p>فرهاد ناله می کند از تیشه ام هنوز</p></div>
<div class="m2"><p>آید صدا ز تربت همپیشه ام هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیمانه ها به محتسبان آشنا شدند</p></div>
<div class="m2"><p>پنهان درون سنگ بود شیشه ام هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گلها خزان شدند و چمن ماند از نشاط</p></div>
<div class="m2"><p>نشکفته است غنچه اندیشه ام هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لب تشنگان ز سایه من بهره می برند</p></div>
<div class="m2"><p>آبی نخورده است رگ و ریشه ام هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر به کوی باده فروشان نبرده ام</p></div>
<div class="m2"><p>بیرون نرفته است می از شیشه ام هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای برق پا منه به نیستان خانه ام</p></div>
<div class="m2"><p>آسودگی ندیده ام از بیشه ام هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خاک کوهکن شب و روز آید این صدا</p></div>
<div class="m2"><p>در آرزوی آب دم تیشه ام هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مانند غنچه سر به گریبان کشیده ام</p></div>
<div class="m2"><p>گلچین رسید و رفت در اندیشه ام هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمریست سیدا ز می انکار کرده ام</p></div>
<div class="m2"><p>ساقی دهد قسم به سر شیشه ام هنوز</p></div></div>