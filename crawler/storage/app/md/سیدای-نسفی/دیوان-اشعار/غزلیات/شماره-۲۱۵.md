---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>درد و داغ غم ز جان عشقبازان برده اند</p></div>
<div class="m2"><p>مدتی شد نعمت از خوان کریمان برده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزگاری شد ز صحرا برنمی خیزد غبار</p></div>
<div class="m2"><p>خاک مجنون را ز دامان بیابان برده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوه را دل گشت سوراخ و به دریا نم نماند</p></div>
<div class="m2"><p>گوهر از آغوش بحر و لعل از کان برده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دفتر اشعار خود را به که اندازم در آب</p></div>
<div class="m2"><p>بس که امروز امتیاز از نکته فهمان برده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می برد باد خزان گل را ز پیش باغبان</p></div>
<div class="m2"><p>پرده بیداری از چشم نگهبان برده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار ظالم بی ابا از پیش آتش بگذرد</p></div>
<div class="m2"><p>تندی و گردنکشی از شعله خوبان برده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حلقه شد پشت کمان و تیر شد پر ریخته</p></div>
<div class="m2"><p>قوت از پیران و کوشش از جوانان برده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد ازین اهل طمع را منع کردن مشکل است</p></div>
<div class="m2"><p>کاین گروه اول عصا از دست دربان برده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیشتر اهل سخن ناکام رفتند از جهان</p></div>
<div class="m2"><p>طوطیان را خشک لب از شکرستان برده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر لب جان پرور خوبان نمی بینم نمک</p></div>
<div class="m2"><p>روزگاری شد اثر از آب حیوان برده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عندلیبان در گلستان بانگ رحلت می زنند</p></div>
<div class="m2"><p>غنچه ها امروز دستی در گریبان برده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در چمن شب تا سحر آنها که عشرت کرده اند</p></div>
<div class="m2"><p>وقت رفتن همچو شبنم چشم گریان برده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر زمین افتاده اند و خاک بر سر می کنند</p></div>
<div class="m2"><p>دردمندانی که گوی از دست چوگان برده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حلقه تسبیح خود را زاهدان گم کرده اند</p></div>
<div class="m2"><p>رشته جمعیت از زنار بندان برده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای خوش آن قومی که در بر روی حاکم بسته اند</p></div>
<div class="m2"><p>از سر بازار نعمت های الوان برده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رهروان را پای در گل مانده است از آبله</p></div>
<div class="m2"><p>دستگیری کردن از خار مغیلان برده اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در چمن قمری همی گوید به آواز بلند</p></div>
<div class="m2"><p>راستی از طینت سرو خرامان برده اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا خلایق در پی نفس و هوا افتاده اند</p></div>
<div class="m2"><p>اختیار از دست فرزندان شیطان برده اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با لب نانی که مسکینان قناعت کرده اند</p></div>
<div class="m2"><p>روزیی خود را درست از خوان دوران برده اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>طرفه صاحبدولتان آورده دوران روی کار</p></div>
<div class="m2"><p>سفره وا ناکرده کفش از پای مهمان برده اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سیدا آنها که خرمن ها به غارت داده اند</p></div>
<div class="m2"><p>در قفای خویش آه خوشه چینان برده اند</p></div></div>