---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>سبزه خطش کشیده در کنار آئینه را</p></div>
<div class="m2"><p>عکس رویش کرده گلبرگ بهار آئینه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می برد مشاطه بهر جستجویی آن پری</p></div>
<div class="m2"><p>بر سر بازارها دیوانه وار آئینه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر روشندلان از شیشه نازکتر بود</p></div>
<div class="m2"><p>کوه کلفت می شود اندک غبار آئینه را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می شوند از لطف شاهان سنگ و آهن شناس</p></div>
<div class="m2"><p>کرده اسکندر به عالم روشناس آئینه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که منظور نظرها گشته یی اندیشه کن</p></div>
<div class="m2"><p>می کند زنگار روزی سنگسار آئینه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست بیعت ده به پیران تا شوی روشن ضمیر</p></div>
<div class="m2"><p>از بغل در پیش روشنگر برار آئینه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می شود از عکس من گلدسته یی برگ خزان</p></div>
<div class="m2"><p>هر که می سازد به روی من دچار آئینه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با سبکروحان گران جانان ندارم التفات</p></div>
<div class="m2"><p>نیست با عکس آشنایی پایدار آئینه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که با من خصم گردد تیغ بر خود می کشد</p></div>
<div class="m2"><p>از عناصر کرده ام در بر چهار آئینه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم عینک را کجا باشد به مژگان احتیاج</p></div>
<div class="m2"><p>آشنایی نیست با خط غبار آئینه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیدا در شهر ما خودبین ندارد اعتبار</p></div>
<div class="m2"><p>ره مده در صحبت خود زینهار آئینه را</p></div></div>