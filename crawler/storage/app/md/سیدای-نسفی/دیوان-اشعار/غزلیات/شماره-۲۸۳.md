---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>مرا از طوف کویت شکوه ی در دل نمی باشد</p></div>
<div class="m2"><p>غبارم را نظر بر دوری منزل نمی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل مجروح مشتاق تو دارد بر جفا صبری</p></div>
<div class="m2"><p>به خاک و خون تپیدن رسم این بسمل نمی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهال سرو دارد با تهیدستی سر و برگی</p></div>
<div class="m2"><p>دل آزاده هرگز با ثمر مایل نمی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه دارد شبان زآفات گرگان گوسفندان را</p></div>
<div class="m2"><p>تعدی در دیار حاکم عادل نمی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ندارد خانه درویزه گر زنجیر دربندی</p></div>
<div class="m2"><p>گره چون غنچه بر پیشانی سایل نمی باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نظر بر سفره منعم ندارد مفلس قانع</p></div>
<div class="m2"><p>ز دریا شکوه هرگز بر لب ساحل نمی باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تحمل می کند اهل رضا تحقیر گردون را</p></div>
<div class="m2"><p>جدل با خصم کار مردم عاقل نمی باشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نگردد چون حریصان چشم گرداب از تردد پر</p></div>
<div class="m2"><p>به جز سرگشتی این قوم را حاصل نمی باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی گردد برون گرد کسادی از دکان من</p></div>
<div class="m2"><p>به سودای متاع من کسی مایل نمی باشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز کشتم روزگاری شد کف افسوس می روید</p></div>
<div class="m2"><p>به دست خوشه چینم دانه حاصل نمی باشد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حباب از دست برد موج بی پروا چه غم دارد</p></div>
<div class="m2"><p>کدورت از کسی در طبع دریا دل نمی باشد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گشادی می شود از اهل همت بستگی ها را</p></div>
<div class="m2"><p>گره وا کردن صاحب کرم مشکل نمی باشد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو باد صبحدم دارند سودای سفر بر سر</p></div>
<div class="m2"><p>به گلزار جهان یک سرو پا در گل نمی باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بپا زنجیر شد یک سوزن بی رشته عیسی را</p></div>
<div class="m2"><p>بلایی بدتر از همراه ناقابل نمی باشد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز دنیا سیدا ما را مرادی برنمی آید</p></div>
<div class="m2"><p>یقین شد غیر نومیدی در این منزل نمی باشد</p></div></div>