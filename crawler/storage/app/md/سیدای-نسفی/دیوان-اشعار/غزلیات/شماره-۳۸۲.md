---
title: >-
    شمارهٔ ۳۸۲
---
# شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>خشک است ز بی مهریی ایام دماغم</p></div>
<div class="m2"><p>در آرزوی روغن آبست چراغم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زخم جگرم کرده بناصور ارادت</p></div>
<div class="m2"><p>ای دست مروت منه انگشت به داغم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریزند اگر در چمنم آب زمرد</p></div>
<div class="m2"><p>چون برگ خزان دیده دمد سبز دماغم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آبله ام چشمه خون سر هر خار</p></div>
<div class="m2"><p>انگشت نما گشت ره از پای سراغم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفتم به تماشای چمن همره سید</p></div>
<div class="m2"><p>از آتش رخساره گل سوخت دماغم</p></div></div>