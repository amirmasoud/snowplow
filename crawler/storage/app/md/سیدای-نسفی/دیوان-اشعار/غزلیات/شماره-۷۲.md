---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>خط رخسار تو شبها برد از هوش مرا</p></div>
<div class="m2"><p>پر ز مهتاب شود هاله آغوش مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به تماشای تو هرگاه که بی خود گردم</p></div>
<div class="m2"><p>نبض جنباند و فریاد کند گوش مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرده چشم حجاب دل روشن نشود</p></div>
<div class="m2"><p>نتوان کرد چو آئینه نمد پوش مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کارم از کم سخنی غنچه صفت در گره است</p></div>
<div class="m2"><p>کرده عمریست حصاری لب خاموش مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف بخت من از چاه برون خواهد شد</p></div>
<div class="m2"><p>نکند جوش خریدار فراموش مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیدا شکوه اغیار خموشم نکند</p></div>
<div class="m2"><p>نه نشاند سخن سرد کس از جوش مرا</p></div></div>