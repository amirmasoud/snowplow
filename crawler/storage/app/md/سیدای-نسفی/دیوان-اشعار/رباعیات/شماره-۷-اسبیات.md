---
title: >-
    شمارهٔ  ۷ - اسبیات
---
# شمارهٔ  ۷ - اسبیات

<div class="b" id="bn1"><div class="m1"><p>ای میر عالمی ز عطای تو بهره مند</p></div>
<div class="m2"><p>لطفت چرا کند به من خسته ماجرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسپی که از برای من انعام کرده یی</p></div>
<div class="m2"><p>آن هم به کهکشان فلک می کند چرا</p></div></div>