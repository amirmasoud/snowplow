---
title: >-
    شمارهٔ  ۱۵ - اسبیات
---
# شمارهٔ  ۱۵ - اسبیات

<div class="b" id="bn1"><div class="m1"><p>اسپی دارم کز غمش سوخته ام</p></div>
<div class="m2"><p>با عیب سراپاش نظر دوخته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که به صاحبش روم رد سازم</p></div>
<div class="m2"><p>گفتا که به کل عیب بفروخته ام</p></div></div>