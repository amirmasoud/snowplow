---
title: >-
    شمارهٔ  ۶ - اسبیات
---
# شمارهٔ  ۶ - اسبیات

<div class="b" id="bn1"><div class="m1"><p>صاحب جاها ترا ز من نیست خبر</p></div>
<div class="m2"><p>هرگز نکنی به حال افتاده گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسپی که به بنده وعده کردی نرسید</p></div>
<div class="m2"><p>این وعده تو سراسپگی بود مگر</p></div></div>