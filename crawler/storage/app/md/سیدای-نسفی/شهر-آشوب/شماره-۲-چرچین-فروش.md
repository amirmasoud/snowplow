---
title: >-
    شمارهٔ  ۲ - چرچین فروش
---
# شمارهٔ  ۲ - چرچین فروش

<div class="b" id="bn1"><div class="m1"><p>به کف چقماق چون برگیرد آن چرچین فروش من</p></div>
<div class="m2"><p>رسد چون سنگ آواز خریداران به گوش من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عکس او دکان از بس که چون آئینه روشن شد</p></div>
<div class="m2"><p>ندارد طاقت نظاره او چشم و هوش من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روم در پیش او هر روز پرسم نقد هر جنسی</p></div>
<div class="m2"><p>بود افزونتر از سوداگران جوش و خروش من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به دستش سیدا چون شانه را با صد زبان بینم</p></div>
<div class="m2"><p>در آید در سخن از رشک لب‌های خموش من</p></div></div>