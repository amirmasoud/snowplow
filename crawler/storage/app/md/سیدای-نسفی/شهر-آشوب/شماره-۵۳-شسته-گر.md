---
title: >-
    شمارهٔ  ۵۳ - شسته گر
---
# شمارهٔ  ۵۳ - شسته گر

<div class="b" id="bn1"><div class="m1"><p>شسته گر امرد که باشد زیر دستش بحر و بر</p></div>
<div class="m2"><p>حکم او جاریست چون آب روان در خشک و تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر شستشو شو اگر پا بر لب دریا نهد</p></div>
<div class="m2"><p>کاسه خود را صدف پر سازد از آب گهر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رخت خون آلود خود را بهر شستن تا برم</p></div>
<div class="m2"><p>کرده ام بر دست خود چون گل مهیا مشت زر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدمتش را بر سر خود می کنم همچون کدنگ</p></div>
<div class="m2"><p>تا دگر او را نباشد حاجت پردازگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیدا چون تخته پیش روی او ایستاده ام</p></div>
<div class="m2"><p>از دکان خویشتن هر چند می سازد به در</p></div></div>