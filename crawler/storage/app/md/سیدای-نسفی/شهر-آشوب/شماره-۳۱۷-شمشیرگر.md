---
title: >-
    شمارهٔ  ۳۱۷ - شمشیرگر
---
# شمارهٔ  ۳۱۷ - شمشیرگر

<div class="b" id="bn1"><div class="m1"><p>دلبر شمشیرگر تیغ جفا بر سر براند</p></div>
<div class="m2"><p>گردن کج کرده ام را در پهلو نشاند</p></div></div>