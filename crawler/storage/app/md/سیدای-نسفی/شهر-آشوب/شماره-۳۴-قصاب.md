---
title: >-
    شمارهٔ  ۳۴ - قصاب
---
# شمارهٔ  ۳۴ - قصاب

<div class="b" id="bn1"><div class="m1"><p>زان مه قصاب مردم گوشت سودا می کنند</p></div>
<div class="m2"><p>عشقبازان دنبه او را تماشا می کنند</p></div></div>