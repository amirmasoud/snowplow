---
title: >-
    شمارهٔ  ۳۰۰ - دارباز
---
# شمارهٔ  ۳۰۰ - دارباز

<div class="b" id="bn1"><div class="m1"><p>دارباز امرد چو مه جایش بود بر آسمان</p></div>
<div class="m2"><p>عاشقان را بسته است آن نازنین بی ریسمان</p></div></div>