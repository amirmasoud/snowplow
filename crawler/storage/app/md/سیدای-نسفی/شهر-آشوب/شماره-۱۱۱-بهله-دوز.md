---
title: >-
    شمارهٔ  ۱۱۱ - بهله دوز
---
# شمارهٔ  ۱۱۱ - بهله دوز

<div class="b" id="bn1"><div class="m1"><p>بهله دوز امرد که باشد کیسه او پر ز مشک</p></div>
<div class="m2"><p>بر میانش گر رسد دستم شود چون بهله خشک</p></div></div>