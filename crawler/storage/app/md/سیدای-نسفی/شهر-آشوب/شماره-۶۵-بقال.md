---
title: >-
    شمارهٔ  ۶۵ - بقال
---
# شمارهٔ  ۶۵ - بقال

<div class="b" id="bn1"><div class="m1"><p>شوخ بقال او متاع خود مرا تکلیف ساخت</p></div>
<div class="m2"><p>از دهان پسته اش بوسیدم و مغزم گداخت</p></div></div>