---
title: >-
    شمارهٔ  ۱۱۷ - شلغم فروش
---
# شمارهٔ  ۱۱۷ - شلغم فروش

<div class="b" id="bn1"><div class="m1"><p>دلبر شلغم فروش خویش را دیدم ز دور</p></div>
<div class="m2"><p>گفتمش از پا فتادم گفت ای شل غم مخور</p></div></div>