---
title: >-
    شمارهٔ  ۳۱ - قصاب
---
# شمارهٔ  ۳۱ - قصاب

<div class="b" id="bn1"><div class="m1"><p>دلبر قصاب من آمد دکان خود گشاد</p></div>
<div class="m2"><p>دنبه را از پشت خود بگرفت و پیش من نهاد</p></div></div>