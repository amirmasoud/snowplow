---
title: >-
    شمارهٔ  ۱۰۸ - صابون پز
---
# شمارهٔ  ۱۰۸ - صابون پز

<div class="b" id="bn1"><div class="m1"><p>بامه صابون پز امشب گفتگو انگیختم</p></div>
<div class="m2"><p>رفتم و در دیگ او تیزاب خود را ریختم</p></div></div>