---
title: >-
    شمارهٔ  ۱۴۶ - پاده چی
---
# شمارهٔ  ۱۴۶ - پاده چی

<div class="b" id="bn1"><div class="m1"><p>پاده چی امرد که از رخ دشت را پر لاله کرد</p></div>
<div class="m2"><p>عشقبازان را علف زار خطش گوساله کرد</p></div></div>