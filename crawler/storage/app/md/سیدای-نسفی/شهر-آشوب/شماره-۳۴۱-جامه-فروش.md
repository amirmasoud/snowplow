---
title: >-
    شمارهٔ  ۳۴۱ - جامه فروش
---
# شمارهٔ  ۳۴۱ - جامه فروش

<div class="b" id="bn1"><div class="m1"><p>نگار جامه فروشم خوش است بالایش</p></div>
<div class="m2"><p>کشم چو جامه در آغوش قد رعنایش</p></div></div>