---
title: >-
    شمارهٔ ۳ - و برای او فی النصیحه
---
# شمارهٔ ۳ - و برای او فی النصیحه

<div class="b" id="bn1"><div class="m1"><p>دلم ز خلق جهان و جهان ملال گرفت</p></div>
<div class="m2"><p>شبی بستر خوابم چنین خیال گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که مهر عمر دگر روی در زوال گرفت</p></div>
<div class="m2"><p>گرفتم آنکه کنون مرغ روح بال گرفت</p></div></div>
<div class="b2" id="bn3"><p>به قبر رفتم و منکر ز من سئوال گرفت</p></div>
<div class="b" id="bn4"><div class="m1"><p>که ای بخوان جهان گشته مدتی مهمان</p></div>
<div class="m2"><p>به ما ز بیش و کم این سفر نما تو عیان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مساز نیک و بد خویش را ز ما پنهان</p></div>
<div class="m2"><p>متاع جان و تنت را چه گشت سود و زیان</p></div></div>
<div class="b2" id="bn6"><p>تو را چه دست از این جمع ملک و مال گرفت</p></div>
<div class="b" id="bn7"><div class="m1"><p>هزار سال به باغ جهان وطن کردی</p></div>
<div class="m2"><p>ز جهل این تن خاکی به ناز پروردی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو خواستی سوی شهر و دیار برگردی</p></div>
<div class="m2"><p>برای اهل وطن ارمغان چه آوردی؟</p></div></div>
<div class="b2" id="bn9"><p>تو را چه بهره ز عمر این هزار سال گرفت</p></div>
<div class="b" id="bn10"><div class="m1"><p>عجب ز جام جهان جمله مست و مدهوشیم</p></div>
<div class="m2"><p>بدین عجوز مفتن چه خوش هم آغوشیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تمام عمر پی جمع مال می‌کوشیم</p></div>
<div class="m2"><p>اجل به خنده و ما گرم خواب خرگوشیم</p></div></div>
<div class="b2" id="bn12"><p>عنان‌زاد تو این کهنه پیر زال گرفت</p></div>
<div class="b" id="bn13"><div class="m1"><p>به صبح حشر که شام فنا بسر آید</p></div>
<div class="m2"><p>خدا ز نیک و بد از ما سئوال فرماید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در بهشت و جهنم به خلق بگشاید</p></div>
<div class="m2"><p>در آن مقام چرا دامن کفر باید</p></div></div>
<div class="b2" id="bn15"><p>به پیش روی خود از روی انفعال گرفت</p></div>
<div class="b" id="bn16"><div class="m1"><p>عجوز دهر بسی کشت همچو ما شوهر</p></div>
<div class="m2"><p>اگر نصیحت من می نیابیدت باور</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دمی که از می وصلش دماغ سازی تر</p></div>
<div class="m2"><p>به هوش باش در آن عین مستی و بنگر</p></div></div>
<div class="b2" id="bn18"><p>به خاک کیست که جام می ازسفال گرفت</p></div>
<div class="b" id="bn19"><div class="m1"><p>نشسته‌اند عتید و رقیب در همه کام</p></div>
<div class="m2"><p>ز نامه عمل ما گرفته بر کف دام</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رقم کنند ز نیک و بدو حلال و حرام</p></div>
<div class="m2"><p>تو را خیال رسد بر دو خوردو گشت تمام</p></div></div>
<div class="b2" id="bn21"><p>هر آنکه مال یتیمان به خود حلال گرفت</p></div>
<div class="b" id="bn22"><div class="m1"><p>شنیده‌ام که بسی خسروان با تدبیر</p></div>
<div class="m2"><p>به کام دل چو نشستند بر فراز سریر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برای آنکه شود ملک دیگران تسخیر</p></div>
<div class="m2"><p>کلامشان به زبان بود با ندیم و وزیر</p></div></div>
<div class="b2" id="bn24"><p>اجل رسید و گلوشان در آن محال گرفت</p></div>
<div class="b" id="bn25"><div class="m1"><p>به روز حشر تو را اگر جحیم مسکن شد</p></div>
<div class="m2"><p>بسوزی ار بتنت از حدید جوش شد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چرا زبان تو (صامت) ز بیم الکن شد</p></div>
<div class="m2"><p>ز هول حادثه هر دو کون ایمن شد</p></div></div>
<div class="b2" id="bn27"><p>هر آنکه دامن حب علی و آل گرفت</p></div>