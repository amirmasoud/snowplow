---
title: >-
    شمارهٔ ۸ - موعظه
---
# شمارهٔ ۸ - موعظه

<div class="b" id="bn1"><div class="m1"><p>از قضا روزی مرا شد سوی قبرستان گذار</p></div>
<div class="m2"><p>دیدم اندر خواب حسرت خفتگان بی‌شمار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلشنی اما ز تاراج فنا اندر خزان</p></div>
<div class="m2"><p>گلستانی خوش ولی پژمرده‌ اندر نوبهار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر طرف زیبا رخی شمشاد قد عناب لب</p></div>
<div class="m2"><p>رو به خاک افتاده از تیغ اجل بی‌برگ و بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تازه دامادان شبستان عدم را کرده فروش</p></div>
<div class="m2"><p>در گزار نوعروسان باز چشم انتظار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوعروسان گشته هم آغوش با داماد مرگ</p></div>
<div class="m2"><p>خال بر اعضا ز مور و چنبر گیسو ز مار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک طرف مستان جام نخوت و جهل و غرور</p></div>
<div class="m2"><p>سر برآورده به زیر خاک از خواب خمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاتب قدرت با لواح جبین یک به یک</p></div>
<div class="m2"><p>سر «گل من علیها فان» نموده آشکار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آن گلزار ناکامی شدم سرگرم سیر</p></div>
<div class="m2"><p>کرده بر احوال یک یک باز چشم اعتبار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جمله را خاموش دیدم از سخن گفتن ولیک</p></div>
<div class="m2"><p>شرح حال خود نمودندی از این بیت آشکار</p></div></div>
<div class="b2" id="bn10"><p>السلام ای بعد ما آیندگان رفتنی</p>
<p>بر شما خوش باد این غمخوانه ناماندنی</p></div>
<div class="b" id="bn11"><div class="m1"><p>ما که می‌بینید اکنون خفته در زیر زمین</p></div>
<div class="m2"><p>چشم حسرت بازداریم و در اندوهگین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سالها بودیم ساکن اندرین دیر غرور</p></div>
<div class="m2"><p>همنشین بخت و روی تخت با عشرت قرین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کودن حرص و هوا آورده قایم زیر ران</p></div>
<div class="m2"><p>توسن چهل و هوس بنموده دایم زیر زین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاه اندر صحن بستان با هزاران همزبان</p></div>
<div class="m2"><p>گاه در طرف گلستان با نگاران همنشین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حلقه حلقه شاهدان زرنی کمر اندر یسار</p></div>
<div class="m2"><p>جوقه جوقه گلرخان سیم‌بر اندر یمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنچه اندر دل نمی‌گردید مرگی آنچنان</p></div>
<div class="m2"><p>وانکه در خاطر نمی‌ٍنجد روزی اینچنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سیم زرهای جهان را کرده سقف آستان</p></div>
<div class="m2"><p>دیه‌های پرنیا نرا کرده عطف آستین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جاهل از تیر قضا پیوسته آن اندر کمان</p></div>
<div class="m2"><p>غافل از گرگل اجل همواه این اندر کمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زد شبیخون ناگهان خیل فنا ما را بسر</p></div>
<div class="m2"><p>این سخن را ورد خود کردیم روز واپسین</p></div></div>
<div class="b2" id="bn20"><p>السلام ای بعد ما آیندگان رفتنی</p>
<p>بر شما خوش باد این غمخانه ناماندنی</p></div>
<div class="b" id="bn21"><div class="m1"><p>تا توانید ای عزیزان پیشه از تقوی کنید</p></div>
<div class="m2"><p>اندرین دار فنا فکر ره عقبا کنید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خیر و احسان از برای رفتگان فرضست فرض</p></div>
<div class="m2"><p>از چه نیکی‌های خود را پس دریغ از ما کنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ما ندانستیم اندر دهر قدر عافیت</p></div>
<div class="m2"><p>فکر حال خویش از احوال ما یکجا کنید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر چه ما اندوختیم از سیم و زر بر باد رفت</p></div>
<div class="m2"><p>تخم امیدی شما در مزرع دنیا کنید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زود بفرستید بهر خود چراغی پیش پیش</p></div>
<div class="m2"><p>پیش از آن کاندر شبستان لحد ماموا کنید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طرح یکرنگی بیندازید کاخر مردنست</p></div>
<div class="m2"><p>چند چند از بهر جمع سبم ورر دعوا کنید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سعی بی‌اندازه تا کی در ره اهل و عیال</p></div>
<div class="m2"><p>در طریق حق‌شناسی معرفت پیدا کنید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چار دیوار لحدهم قابل تعبیر هست</p></div>
<div class="m2"><p>تا بکی سقف عمارتهای خود زیبا کنید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>جمله بربندید چون ما بار از این دار فنا</p></div>
<div class="m2"><p>این حکایت را برای دیگران انشا کنید</p></div></div>
<div class="b2" id="bn30"><p>السلام ای بعد ما آیندگان رفتنی</p>
<p>برشما خوش باد این غمخانه ناماندنی</p></div>
<div class="b" id="bn31"><div class="m1"><p>مدتی جمشید اندر دهر صاحب جام بود</p></div>
<div class="m2"><p>عشترتش با ساقیان سر و سیم اندام بود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تاج و تخت و حشمت جمشید چون بر باد شد</p></div>
<div class="m2"><p>صاحب کوس و علم ضحاک بر فرجام بود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس فریدون بود و ایراج بود و سلم و تور بود</p></div>
<div class="m2"><p>بعد نوذر بعد طوس آن شاه نیکونام بود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>از پس اینها منوچهر و پس از او کیقباد</p></div>
<div class="m2"><p>بعد کاوس و سیاوش خسر ایام بود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دولت اسفندیار و بهمن و ملک هما</p></div>
<div class="m2"><p>حشمت افراسیاب و ملک بهرام بود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روزگاری بد عروس سلطنت پرویز را</p></div>
<div class="m2"><p>مدتی هم وحشی دولت بکسری رام بود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سالها نمرود بی‌دین عمرها شداد شوم</p></div>
<div class="m2"><p>بعد فرعون دغا آن زشت بد انجام بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>همچنین از عزل و نصب این سلاطین یک به یک</p></div>
<div class="m2"><p>دور دنیا را گهی آشوب و گه آرام بود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جملگی را روزگار وعده چون آمد بسر</p></div>
<div class="m2"><p>این سخن گفتند از جان تا زبان در کام بود</p></div></div>
<div class="b2" id="bn40"><p>السلام ای بعد ما آیندگان رفتنی</p>
<p>بر شما خوشباد این غمخانه ناماندنی</p></div>
<div class="b" id="bn41"><div class="m1"><p>پا نهاد از بدو عالم چو به دنیا بوالبشر</p></div>
<div class="m2"><p>اولش از ترک اولی گشت عمری دیده‌تر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بعد از آن هجران حوا کرد او را اشکبار</p></div>
<div class="m2"><p>س به درد و غم قرین شد از غم داغ پسر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نوح در طغیان قوم و بحر و کشتی شد دچار</p></div>
<div class="m2"><p>هود و شیث و صالح از عصیان امت در حذر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>حضرت ایوب اندر ابتلا شد مبتلا</p></div>
<div class="m2"><p>حضرت یعقوب اندر هجریوسف نوحه گر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گشت ابراهیم را در نار نمرودی مقام</p></div>
<div class="m2"><p>بود یونس را به زندان دل ماهی مقر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>حضت موسی بن عمران از جفای قبطیان</p></div>
<div class="m2"><p>گاه در مصر غمش جا بود گه نیل خطر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گشت عیسی را تن کاهیده زیب روی دار</p></div>
<div class="m2"><p>بود یحیی را سر ببریده جا در طشت زر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>یک به یک کردند از این دار فنا رو در بقا</p></div>
<div class="m2"><p>سر به سر بستند از این دیر کهن بارسفر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>جمله را نقد نفس افتاد چون اندر شمار</p></div>
<div class="m2"><p>این سخن گفتند و گردیدند از این ره رهسپر</p></div></div>
<div class="b2" id="bn50"><p>السلام ای بعد ما آیندگان رفتنی</p>
<p>برشما خوش باد این غمخانه ناماندنی</p></div>
<div class="b" id="bn51"><div class="m1"><p>تا که احمد هادی دین اولوالباب شد</p></div>
<div class="m2"><p>هتک حرمت کردن شان پیمبر باب شد</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گاه اندر اضطراب از کینه اقوام شد</p></div>
<div class="m2"><p>گاه اندرگیر و دار از زحمت اصحاب شد</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گاه آزردند دندان وی از سنگ ستم</p></div>
<div class="m2"><p>پر ز خون درج دهان آن در نایاب شد</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بعد از آن پهلوی زهرا راز ضرب درشکست</p></div>
<div class="m2"><p>آنکه از ناحق امیر زمره اعراب شد</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>گاه علی راشد گلو چون شیر در قید طناب</p></div>
<div class="m2"><p>گه تن وی غرقه خون در دامن محراب شد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>مجتبی بعد از پدر شد کینه زهر ستم</p></div>
<div class="m2"><p>ارغوانی عارضش همرنگ چون ماهتاب شد</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>وه چه زهری کو شرر اندر دل ز هر ستم</p></div>
<div class="m2"><p>نی همین لخت جگر از وی بخوان ناب شد</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>وه چه زهری کز شرارش سوخت قلب مرتضی</p></div>
<div class="m2"><p>وه چه زهری کز تفش جسم پیمبر آب شد</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>هر یکی گفتند این بیت حزین را در وداع</p></div>
<div class="m2"><p>چون که هنگام فراق و دوری احباب شد</p></div></div>
<div class="b2" id="bn60"><p>السلام ای بعد ما آیندگان رفتنی</p>
<p>بر شما خوش باد این غمخانه ناماندنی</p></div>
<div class="b" id="bn61"><div class="m1"><p>آه واویلا که اولاد پیمبر خوار شد</p></div>
<div class="m2"><p>ظلم وقف دودمان حیدر کرار شد</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>آه واویلا که اولاد پیمبر خوار شد</p></div>
<div class="m2"><p>ظلم وقف دودمان حیدر کرار شد</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>نوجوانان بنی‌هاشم به دشت کربلا</p></div>
<div class="m2"><p>جمله را سر بر سنان از کینه کفار شد</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>نور چشم حضرت زهرا و پیغمبر حسین</p></div>
<div class="m2"><p>در میان قوم کوفی بی‌کس و بی‌یار شد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>یکه و تنها ز بس بر جسمش آمد نوک تیز</p></div>
<div class="m2"><p>پا ز زبن خالی نمود و دست وی از کار شد</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>بر سر خاک سیه جا کرد سبط بوتراب</p></div>
<div class="m2"><p>از پی قتلش روان شمر جفا کردار شد</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>چون به روی سینه‌اش جا کرد ین زشت پلید</p></div>
<div class="m2"><p>شاه دین گوهرفشان از لعل گوهر بار شد</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>زاری آن بی‌گنه ننمود بر قاتل اثر</p></div>
<div class="m2"><p>سر جدا از جسم وی با کام آتشبار شد</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>هیچ میدانی چه می‌فرمود (صامت) زیر تیغ</p></div>
<div class="m2"><p>شاه دین با اهل او چون از جهان بیزار شد</p></div></div>
<div class="b2" id="bn70"><p>السلام ای بعد ما آیندگان رفتی</p>
<p>بر شما خوش باد این غمخانه ناماندنی</p></div>