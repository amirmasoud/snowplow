---
title: >-
    شمارهٔ ۲۰ - نوحه جدید
---
# شمارهٔ ۲۰ - نوحه جدید

<div class="b" id="bn1"><div class="m1"><p>نیست ممکن که شود از دلی شاد فلک</p></div>
<div class="m2"><p>بشنود چون ز غم قاسم داماد فلک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاش می‌رفت پس از نوگل گلزار حسین</p></div>
<div class="m2"><p>خرمن عشرت عالم همه بر باد فلک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از تو فریاد فلک داد و بیداد فلک</p></div>
<div class="m2"><p>زدی آتش به جهان خانه‌ات آباد فلک</p></div></div>
<div class="b2" id="bn4"><p>ز تو فریاد فلک</p></div>
<div class="b" id="bn5"><div class="m1"><p>ساختی حجله دادمادی او را بر پا</p></div>
<div class="m2"><p>تا کنی شاد دل وی به صف کرب و بلا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی او را دل پرحسرت و تا روز جزا</p></div>
<div class="m2"><p>وعده وصل عروسش ز تو افتاد فلک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز تو فریاد فلک داد و بیداد فلک</p></div>
<div class="m2"><p>زدی آتش به جهان خانه‌ات آباد فلک</p></div></div>
<div class="b2" id="bn8"><p>ز تو فریاد فلک</p></div>
<div class="b" id="bn9"><div class="m1"><p>دید چون مادر قاسم که در آن دشت محن</p></div>
<div class="m2"><p>قاسم از بهر شهادت به بدن کرده کفن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت زین گردش وارونه شد ای چرخ کهن</p></div>
<div class="m2"><p>خانه صبر مرا رخنه به بنیاد فلک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز تو فریاد فلک داد و بیداد فلک</p></div>
<div class="m2"><p>زدی آتش به جهان خانه‌ات آباد فلک</p></div></div>
<div class="b2" id="bn12"><p>ز تو فریاد فلک</p></div>
<div class="b" id="bn13"><div class="m1"><p>قاسم افتاد بسر چون هوس میدانش</p></div>
<div class="m2"><p>فاطمه زین سخن افتاد شرر بر جانش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کان عروسی که ز خون گشت حنابندانش</p></div>
<div class="m2"><p>دیگر از عشرت دنیا نکند یاد فلک</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز تو فریاد فلک داد و بیداد فلک</p></div>
<div class="m2"><p>زدم آتش به جهان خانه‌ات آباد فلک</p></div></div>
<div class="b2" id="bn16"><p>ز تو فریاد فلک</p></div>
<div class="b" id="bn17"><div class="m1"><p>گشت تا منزل قاسم بسر حجله خاک</p></div>
<div class="m2"><p>جگر (صامت) افسرده ز غم شد صد چاک</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در دل خاک شود زین غم عظمی چو هلاک</p></div>
<div class="m2"><p>افکند زلزله در عالم ایجاد فلک</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز تو فریاد فلک داد و بیدا فلک</p></div>
<div class="m2"><p>زدی آتش به جهان خانه‌ات آباد فلک</p></div></div>
<div class="b2" id="bn20"><p>ز تو فریاد فلک</p></div>