---
title: >-
    شمارهٔ ۱۷ - و برای او همچنین
---
# شمارهٔ ۱۷ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>چون به صف کرب و با بخت و هب یار شد</p></div>
<div class="m2"><p>آمد و یار پسر احمد مختار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر کار پسر دختر خیرالانام</p></div>
<div class="m2"><p>با پسر سعد لعین بسته به پیکار شد</p></div></div>
<div class="b2" id="bn3"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn4"><div class="m1"><p>گریه کنان مادر زار وهب شیر دل</p></div>
<div class="m2"><p>رو به وهب کرد که ای غیرت سر و چگل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستی اگر طالب برهم ‌زدن آب و گل</p></div>
<div class="m2"><p>خیز که هنگام تجلای رخ یار شد</p></div></div>
<div class="b2" id="bn6"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرببلا</p></div>
<div class="b" id="bn7"><div class="m1"><p>شمع رخ دوست به پروانه شور می‌زند</p></div>
<div class="m2"><p>بر جگر پیر و جوان تیر نظر می‌زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خیز که معشوق ازل حلقه بدر می‌زند</p></div>
<div class="m2"><p>موسم افروختن طلعت دلدار شد</p></div></div>
<div class="b2" id="bn9"><p>چرخ بی‌ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn10"><div class="m1"><p>گر به تمنای حیات ابدی مایلی</p></div>
<div class="m2"><p>در همه حالی ندهد دست چنین محفلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قافله افتاده بره خفته تو در منزلی</p></div>
<div class="m2"><p>سبط رسول عربی قافله سالار شد</p></div></div>
<div class="b2" id="bn12"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn13"><div class="m1"><p>خیز و ره عشق به ارباب هوس تنگ کن</p></div>
<div class="m2"><p>پنجه ز خون در نظر خون خدا رنگ کن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رو به رکاب پسر شیر خدا جنگ کن</p></div>
<div class="m2"><p>چون که حسین بن علی بی‌کس و بی‌بار شد</p></div></div>
<div class="b2" id="bn15"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn16"><div class="m1"><p>رو بفکن بر زبر قصر سعادت کمند</p></div>
<div class="m2"><p>مادر خود را ببر فاطمه کن سر بلند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر فرس همت خود زین سعادت ببند</p></div>
<div class="m2"><p>وقت جداساختن یار ز اغیار شد</p></div></div>
<div class="b2" id="bn18"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn19"><div class="m1"><p>جوهر مردانگی امروز نماید ظهور</p></div>
<div class="m2"><p>زن سرپایی به عروس و به نشاط و سرور</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گر به جنان طالبی و راغب حور و قصور</p></div>
<div class="m2"><p>جنت تو کرب و بلا تحتها الانهار شد</p></div></div>
<div class="b2" id="bn21"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب بو بلا</p></div>
<div class="b" id="bn22"><div class="m1"><p>کرد وهب نزد شه تشنه لبان سر قدم</p></div>
<div class="m2"><p>ساخت طلب رخصت میدان از امام امم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زد به یکی حمله صف لشگر عدوان بهم</p></div>
<div class="m2"><p>تیغ کفش برق تن لشگر کفار شد</p></div></div>
<div class="b2" id="bn24"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زدبر سر کرب و بلا</p></div>
<div class="b" id="bn25"><div class="m1"><p>مورد صفت لشگر کفار به جوش آمدند</p></div>
<div class="m2"><p>پیل دمان را پی کشتن به خروش آمدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>جمله پی قتل سلیمان چون و حوش آمدند</p></div>
<div class="m2"><p>روز به چشم وهب آخر چه شب تار شد</p></div></div>
<div class="b2" id="bn27"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn28"><div class="m1"><p>عاقبت از اوج شهادت چو هما پر نهاد</p></div>
<div class="m2"><p>حنجر خود را زوفا بردم خنجر نهاد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در ره سودای حسین بن علی سر نهاد</p></div>
<div class="m2"><p>بر سروی خسرو بی‌بار و مددکار</p></div></div>
<div class="b2" id="bn30"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn31"><div class="m1"><p>از مدد بخت بلند وهب نوجوان</p></div>
<div class="m2"><p>کرد نظر بر رخ زیبای حسین دادجان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گشت شه تشنه لبان را به زمین چون مکان</p></div>
<div class="m2"><p>شمر روان بر سر آن سرور ابرار شد</p></div></div>
<div class="b2" id="bn33"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>
<div class="b" id="bn34"><div class="m1"><p>تا کند از تن سر مهر افسر او را جدا</p></div>
<div class="m2"><p>جا به سر سینه وی کرد سنگ بی‌حیا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تشنه جدا کرد سر سبط نبی از قفا</p></div>
<div class="m2"><p>(صامت) از این مرحله از چشم گهربار شد</p></div></div>
<div class="b2" id="bn36"><p>چرخ پی ابتلا کوفت به کوس بلا</p>
<p>ابر بلا خیمه زد بر سر کرب و بلا</p></div>