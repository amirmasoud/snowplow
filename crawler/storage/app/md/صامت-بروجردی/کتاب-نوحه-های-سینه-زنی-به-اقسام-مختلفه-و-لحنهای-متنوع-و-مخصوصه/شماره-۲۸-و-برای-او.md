---
title: >-
    شمارهٔ ۲۸ - و برای او
---
# شمارهٔ ۲۸ - و برای او

<div class="b" id="bn1"><div class="m1"><p>ای مسیب ز جهان سوی جنان در سفرم</p></div>
<div class="m2"><p>شده پرخون جگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشین لحظه اندر دم آخر به برم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسکه کاهیده شد از صدمه زنجیر تنم</p></div>
<div class="m2"><p>آب گشته بدنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقمی نیست ز پا تا بسر من دیگرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل افسرده و محزون و جگر خون و غریب</p></div>
<div class="m2"><p>بی‌پرستار و طبیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه معنی است به بالین نه انیسی به سرم</p></div>
<div class="m2"><p>شده پر خون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زد چنان برق اجل شعله مرا بر تن و جان</p></div>
<div class="m2"><p>که به دوران جهان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه دگر اسم بجا باز بود نی اثرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زهر هارون ستمگر جگرم را بگداخت</p></div>
<div class="m2"><p>بدل آتش انداخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساخت بی‌مونس و دور از وطن و دربدرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قاصدی کو که رود از بر من سوی وطن</p></div>
<div class="m2"><p>با غم و درد و محن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به محبان و عزیزان برساند خبرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به رضا گوید آیا نور دو چشمان پدر</p></div>
<div class="m2"><p>بسرم کن تو گذر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که به راه تو بود موسم مردن نظرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای مسیب دم مرگ است ز بیداد رسی</p></div>
<div class="m2"><p>همچو مرغ قفسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یاد آید ز حسین جد به خون غوطه‌ورم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو کنم یاد ز احوال تن بی‌سر او</p></div>
<div class="m2"><p>غرقه خون پیکر او</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>روز چون شام شود تیره بعد نظرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز غم العطش وی بلب شط فرات</p></div>
<div class="m2"><p>شد مرا قطع حیات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز و شب گشته روان خون ز دو چشمان ترم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>وعده دیدن رویت به قیامت افتاد</p></div>
<div class="m2"><p>عاقبت در بغداد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بی تو مدفون شده‌ام مونس شام و سحرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کس نبود ا زدفن و کفن شاه شهید</p></div>
<div class="m2"><p>شد مرا قطع امید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همچو (صامت) به خدنگ غم ماتم سپرم</p></div>
<div class="m2"><p>شده پرخون جگرم کو رضا کو پسرم</p></div></div>