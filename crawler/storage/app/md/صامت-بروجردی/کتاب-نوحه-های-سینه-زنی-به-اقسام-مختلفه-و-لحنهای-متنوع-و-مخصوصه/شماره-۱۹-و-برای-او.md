---
title: >-
    شمارهٔ ۱۹ - و برای او
---
# شمارهٔ ۱۹ - و برای او

<div class="b" id="bn1"><div class="m1"><p>چون آل طاها از جور ایام</p></div>
<div class="m2"><p>گشتند وارد در کشور شام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا سکینه در محضر عام</p></div>
<div class="m2"><p>ای خلق بی‌رحم ای قوم بدنام</p></div></div>
<div class="b2" id="bn3"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn4"><div class="m1"><p>ما عین قبله اصل نمازیم</p></div>
<div class="m2"><p>سلطان یثرب شاه حجازیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حکم قرآن ما سرفرازیم</p></div>
<div class="m2"><p>در کشور دین در ملک اسلام</p></div></div>
<div class="b2" id="bn6"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn7"><div class="m1"><p>ما کاندرین شهر زار و ملولیم</p></div>
<div class="m2"><p>از محیط وحی از بیت الهام</p></div></div>
<div class="b2" id="bn8"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn9"><div class="m1"><p>خوش می‌نوازید نای و نقاره</p></div>
<div class="m2"><p>گردیده بر ما گرم نظاره</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خوش پرده شرم گردید پاره</p></div>
<div class="m2"><p>بر آل یاسین دادید دشنام</p></div></div>
<div class="b2" id="bn11"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn12"><div class="m1"><p>با سینه ریش با قلب پرغم</p></div>
<div class="m2"><p>با سر عریان با چشم پر نم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ما را نمودند سرگرد عالم</p></div>
<div class="m2"><p>از کثرت بغض از روی ابرام</p></div></div>
<div class="b2" id="bn14"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn15"><div class="m1"><p>روزی که قرآن گردید نازل</p></div>
<div class="m2"><p>بر حرمت ما می‌بود شامل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما را خرابه دادید منزل</p></div>
<div class="m2"><p>از جای حرمت از بهر اکرام</p></div></div>
<div class="b2" id="bn17"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn18"><div class="m1"><p>آخر غریبیم ما آل حیدر</p></div>
<div class="m2"><p>در کشور شام ای خلق کافر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تا چند ما را ریزید بر سر</p></div>
<div class="m2"><p>خاکستر و سنگ از پشت هر بام</p></div></div>
<div class="b2" id="bn20"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="b" id="bn21"><div class="m1"><p>در این ولایت ما میهمانیم</p></div>
<div class="m2"><p>چون طایر دور از آشیانیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا کی بر افلاک شیون رسانیم</p></div>
<div class="m2"><p>از ناله صبح از گریه شام</p></div></div>
<div class="b2" id="bn23"><p>نحن سبایا آل محمد</p>
<p>جدی رسول فی کل مشهد</p></div>
<div class="m2"><p>گر دم شب و روز در ناله چون نی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مانند (صامت) شد عمر من طی</p></div>
<div class="b2" id="bn25"><p>در محنت و رنج در حزن و آلام</p></div>
<div class="b" id="bn26"><div class="m1"><p>نحن سبایا آل محمد</p></div>
<div class="m2"><p>جدی رسول فی کل مشهد</p></div></div>