---
title: >-
    شمارهٔ ۶ - در زهد حضرت عیسی
---
# شمارهٔ ۶ - در زهد حضرت عیسی

<div class="b" id="bn1"><div class="m1"><p>یکی روز از سر عبرت به عالم</p></div>
<div class="m2"><p>گذشتی حضرت عیسی بن مریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرستو کی بدید اندر زمانه</p></div>
<div class="m2"><p>که بهر خویش سازد آشیانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب معجز نما چون غنچه بشگفت</p></div>
<div class="m2"><p>همانا با حورایین چنین گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که این بسته زبان همخانه دارد</p></div>
<div class="m2"><p>تعلق سوی آب و دانه دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عکس من که ماوائی ندارم</p></div>
<div class="m2"><p>ز ملک این جهان جایی ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگفتند از بود طبع تو مایل</p></div>
<div class="m2"><p>به تعمیر مقام و جاه و منزل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو تا سر بهمت بر فرازیم</p></div>
<div class="m2"><p>برایت خانه عالی بسازیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که از باغ جنان ممتاز باشد</p></div>
<div class="m2"><p>ز رفعت با فلک دمساز باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عنان گفتگو را می‌کشیدند</p></div>
<div class="m2"><p>ز آنجا تا لب دریا رسیدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگفتا اندر آن دریای پرموج</p></div>
<div class="m2"><p>که موجش می‌رساند بر فلک اوج</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بسازید از فتوت بارگاهی</p></div>
<div class="m2"><p>به وقف طقع محکم بارگاهی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بگفتندش ایا مهر جهان تاب</p></div>
<div class="m2"><p>بنا را هیچکس ننهاده بر آب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ترا گر در حقیقت این خیال است</p></div>
<div class="m2"><p>بعید از عقل و این فکرت محال است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تبسم کرد و با ایشان بفرمود</p></div>
<div class="m2"><p>مرا هم این حکایت بود مقصود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که دنیا همچو این بحر عمیق است</p></div>
<div class="m2"><p>بهر دم عالمی در وی غریق است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنائی را که بنیادش بر آبست</p></div>
<div class="m2"><p>خرابست و خرابست و خرابست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جهان (صامت) چه جای خانه باشد</p></div>
<div class="m2"><p>مگر آن را که بس دیوانه باشد</p></div></div>