---
title: >-
    شمارهٔ ۲ - حمایت در فریب دنیا
---
# شمارهٔ ۲ - حمایت در فریب دنیا

<div class="b" id="bn1"><div class="m1"><p>یکی بیچاره‌ای محنت رسیده</p></div>
<div class="m2"><p>به دوران کور و محروم از دو دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نهان در پرده عصمت زنی داشت</p></div>
<div class="m2"><p>چو شیطان بلکه دزد رهزنی داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به افسون و حیل دایم شب و روز</p></div>
<div class="m2"><p>زدی بر قلب شوهر تبر دلدوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمادم عشوه بنیاد کردی</p></div>
<div class="m2"><p>به این افسانه‌اش دلشاد کردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که صد حیف از چنین حسن یگانه</p></div>
<div class="m2"><p>شدی محروم از دور زمانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دریغا گر تو را چشمی بسر بود</p></div>
<div class="m2"><p>به رخسار و جمال من نظر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر یک ره برویم دیده بودی</p></div>
<div class="m2"><p>گل از گلزار حسنم چیده بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز شوق طلعتم از بسکه نیکوست</p></div>
<div class="m2"><p>نگنجیدی بسان مغز در پوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدیدی گر که سبب غبغبم را</p></div>
<div class="m2"><p>کنار چشمه نوش لبم را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شدی یکباره بیرون از سرت هوش</p></div>
<div class="m2"><p>نمودی چشمه حیوان فراموش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سواد زلف جعد مشک بویم</p></div>
<div class="m2"><p>بیاض طلعت روی نکویم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به چشم حور و غلمان سرمه داده</p></div>
<div class="m2"><p>به رضوان روزن جنت گشاده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون من همخوابه در نیکویی طاق</p></div>
<div class="m2"><p>ندیده است و نبیند چشم آفاق</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بدان مکاره پرحیله و فن</p></div>
<div class="m2"><p>بگفتا مرد کور از قلب روشن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر از نظاره چشمم ناامید است</p></div>
<div class="m2"><p>ولی از عقل این مطلب بعید است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو را با این رخ زیبا که داری</p></div>
<div class="m2"><p>بدین سرو قد رعنا که داری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا با چون منی همراز بودی</p></div>
<div class="m2"><p>به این کوری مرا دمساز بودی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گرفتم کز حقیقت با من کور</p></div>
<div class="m2"><p>تو را رسم وفا می‌بود منظور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگرد ما در این آباد کشور</p></div>
<div class="m2"><p>بسی هستند رندان قلندر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که چون برسر برندی تاج گیرند</p></div>
<div class="m2"><p>ز ماه آسمانی باج گیرند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چه دیدندی جمال دل فریبت</p></div>
<div class="m2"><p>چه گوهر در کف مفس غریبت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به ساعت دست غارت می‌گشودند</p></div>
<div class="m2"><p>تو را از دامن من می‌ربودند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>غرض از این سخن بی‌قیل و قالست</p></div>
<div class="m2"><p>برای عشوه دنیا مثال است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شود اندر جهان کی مرد عاقل</p></div>
<div class="m2"><p>به نیرنگ عجو دهر مایل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اگر زال زمانه باوفا بود</p></div>
<div class="m2"><p>زمانی یار مردان خدا بود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نبودش گر طریقبی‌وفایی</p></div>
<div class="m2"><p>چرا می‌کرد از خوبان جدایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز وصل وی نباشد شاد و مسرور</p></div>
<div class="m2"><p>مگر چشمی که فی الواقع بود کور</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مگو (صامت) برای دیگران پند</p></div>
<div class="m2"><p>برو خود را برون بنما از این پند</p></div></div>