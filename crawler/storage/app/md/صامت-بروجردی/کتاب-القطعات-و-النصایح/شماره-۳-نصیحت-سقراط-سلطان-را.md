---
title: >-
    شمارهٔ ۳ - نصیحت سقراط سلطان را
---
# شمارهٔ ۳ - نصیحت سقراط سلطان را

<div class="b" id="bn1"><div class="m1"><p>یکی از شهریاران زمانه</p></div>
<div class="m2"><p>که اکنون نیست از نام نشانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوی سقراط دانا راه سر کرد</p></div>
<div class="m2"><p>به خوابش دیدچون بروی گذر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدان دانا حکیم آن فتنه‌انگیز</p></div>
<div class="m2"><p>سرپایی بزد کز جای برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه از خواب آن خردمند هشیوار</p></div>
<div class="m2"><p>ز گستاخی سلطان گشت بیدار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدانجاه و جلال و پادشاهی</p></div>
<div class="m2"><p>نظر ننمود از بی‌اعتنایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شه نادان سوی وی کرد پرخاش</p></div>
<div class="m2"><p>که تا کی بیخودی یک دم به خود باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا با این همه محکم اساسی</p></div>
<div class="m2"><p>ندانی کیستم یا می‌شناسی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جوابش داد سقراط خردمند</p></div>
<div class="m2"><p>کزین گفتار باطل لب فرو بند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو را با ین همه کبر و منی من</p></div>
<div class="m2"><p>ندانم غیر حیوانی لگد زن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وگرنه با تمام خود سنائی</p></div>
<div class="m2"><p>چو حیوان لگد افکن چرائی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر انسانی چرا ای مرد گمراه</p></div>
<div class="m2"><p>زنی خوابیده را در خواب ناگاه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز این گفتار همچون مار ارقم</p></div>
<div class="m2"><p>غضب آلوده شد پیچید درهم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدو گفتار ز روی کبر و نخوت</p></div>
<div class="m2"><p>به سلطان کی چنین گوید رعیت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مکش افزون ز حد خویشتن پا</p></div>
<div class="m2"><p>تو بر من بنده من بر تو مولی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تبسمکرد سقراط و به سلطان</p></div>
<div class="m2"><p>چنین فرمود کای سلطان نادان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بخار ما و من از سر بدر کن</p></div>
<div class="m2"><p>ز گفت ناپسند خود حذر کن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که از شیادی این گردون پرشید</p></div>
<div class="m2"><p>به صیادی چو تو کرده بسی صید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همین طبل و همین نقاره و کوس</p></div>
<div class="m2"><p>بود ز اسفندیار و نوذر و طوس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همین تاج و نگین و افسر و تخت</p></div>
<div class="m2"><p>بود بر جا ز ضحاک سیه بخت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همین سنج و همین بوق و علم بود</p></div>
<div class="m2"><p>که از طهمورث و جمشید جم بود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو را هر چیز کزوی افتخار است</p></div>
<div class="m2"><p>ز اشهان زمانه یادگار است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کجا رفتند کز ایشان نشان نیت</p></div>
<div class="m2"><p>چرا نامی از ایشان در میان نیست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بداند هر که دارای شعور است</p></div>
<div class="m2"><p>که سلطانی گدایان را ضرور است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به سامان خردمندان ساده</p></div>
<div class="m2"><p>ز اسب خودستایی شو پیاده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بیا تا خلوتی را برگزینیم</p></div>
<div class="m2"><p>برای گفتگو با هم نشینیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سخن از هر دری با هم برانیم</p></div>
<div class="m2"><p>کمال و نقص همدیگر بدانیم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بلی (صامت) سر خاک سیاهی</p></div>
<div class="m2"><p>بود به از هزاران تخت شاهی</p></div></div>