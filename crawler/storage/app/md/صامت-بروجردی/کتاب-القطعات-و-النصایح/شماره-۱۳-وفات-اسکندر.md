---
title: >-
    شمارهٔ ۱۳ - وفات اسکندر
---
# شمارهٔ ۱۳ - وفات اسکندر

<div class="b" id="bn1"><div class="m1"><p>شنیدستم که اسکندر چه شد وقت</p></div>
<div class="m2"><p>که از دنیا سوی عقبی کشید رخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصیت کرد با یاران همراه</p></div>
<div class="m2"><p>که چون شد جانم از قیدغم آزاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز تاج و تخت جویم چون کناره</p></div>
<div class="m2"><p>شوم بر مرک چوبین سواره</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تابوتم چو جا دادید محزون</p></div>
<div class="m2"><p>نهیدم دست از تابوت بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنم را همچنان اندر عماری</p></div>
<div class="m2"><p>بگردانید اندر هر دیاری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسی چون گشت پیدا تا بر دوی</p></div>
<div class="m2"><p>بسر دست بیرون ماندیم پی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یقین دانید کانجا هست خاکم</p></div>
<div class="m2"><p>در آنجا جا دهید اندر مغاکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس آنگه پشت پا بر بیش و کم زد</p></div>
<div class="m2"><p>ز دنیا جانب عقبی قدم زد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پی فرموده آن شاه دانا</p></div>
<div class="m2"><p>به تابوتش نهادند و از آنجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندیدمانش به صد افغان و شیون</p></div>
<div class="m2"><p>همه شال عزا بسته به گردن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پیرامون تابوت سکندر</p></div>
<div class="m2"><p>نور دیدند گیتی را سراسر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز دانایان اسرار نهفته</p></div>
<div class="m2"><p>نشد این گوهر ناسفته سفته</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به یک شهری رسیدند آخر کار</p></div>
<div class="m2"><p>یکی از نکهت سنجان هشیار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در آن کشور شکست او قفل این گنج</p></div>
<div class="m2"><p>که کرد آن خلق را آسوده از رنج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به گفت اسکندر این حکمی که فرمود</p></div>
<div class="m2"><p>بجز تنبیه خلقش نیست مقصود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>که تا انجام کار خود بدانید</p></div>
<div class="m2"><p>از این دفتر خط خود را بخوانید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>که اسکندر از آن کشور ستانی</p></div>
<div class="m2"><p>وز آن طبل و نفیر کاویانی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن آوازه و لشگر کشیدن</p></div>
<div class="m2"><p>وز آن صف‌های دشمن را دریدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از آن سیم و زر و گنج و خزینه</p></div>
<div class="m2"><p>از آن لعل و گهرهای ثمینه</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو دست او ز دنیا گشت کوتاه</p></div>
<div class="m2"><p>نبرد از سلطنت چیزی به همراه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنین بر خلق عالم کرد حالی</p></div>
<div class="m2"><p>که رفتم از جهان با دست خالی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خوش آن آزاد مردم تهیدست</p></div>
<div class="m2"><p>کز این صهبا نگردیدند سر مست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به سوی اوج رفعت پرگشادند</p></div>
<div class="m2"><p>قدم اندر سر عالم نهادند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به مثل (صامت) از دنیای فانی</p></div>
<div class="m2"><p>شدند عازم به ملک جاودانی</p></div></div>