---
title: >-
    شمارهٔ ۴ - در بی‌وفایی زن و فرزند
---
# شمارهٔ ۴ - در بی‌وفایی زن و فرزند

<div class="b" id="bn1"><div class="m1"><p>شنیدستم که شاپور ذوالاکناف</p></div>
<div class="m2"><p>خصومت داشت یا شاهی از اسلاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشه گردید از شاپور در جنگ</p></div>
<div class="m2"><p>ز بس جنگ خصومت عرصه شد تنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز میدان جدال آمد فراری</p></div>
<div class="m2"><p>به حصن شهر سلطان شد حصاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزد شاپور دور شهر آن شاه</p></div>
<div class="m2"><p>پی بگرفتن آن شهر خرگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرفت از لشکر و سر خیل و سردار</p></div>
<div class="m2"><p>به قدر چهار سال از پیش او کار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی روز از قضا شاپور دلگیر</p></div>
<div class="m2"><p>بگرد شهر گشتی بهر تسخیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شه آن شهر زیبا دختری داشت</p></div>
<div class="m2"><p>به چرخ حسن رخشان اختری داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیاه حسن او دوران گرفته</p></div>
<div class="m2"><p>به خوبی اج از خوبان گرفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیرلشگر شاپور آنجا</p></div>
<div class="m2"><p>ز بام قلعه بود اندر تماشا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نظر باز قضا افکند از دور</p></div>
<div class="m2"><p>نگاه آن پری بر روی شاپور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بشد آن دختر شیرین شمایل</p></div>
<div class="m2"><p>بهطاق ابروی شاپورمایل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پنهانی سوی او داد پیغام</p></div>
<div class="m2"><p>که گر شاپورمی‌بخشد مرا کام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بکوشم درحصول مدعایش</p></div>
<div class="m2"><p>به فتح لعه گردم رهنمایش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل شاپور از آن پیغام شد شاد</p></div>
<div class="m2"><p>نوید وصل بر دختر فرستاد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به تمهیدی که می‌دانست دختر</p></div>
<div class="m2"><p>سوی شهر پدر سر داد لشگر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نخستین کار کان لشگر نمودند</p></div>
<div class="m2"><p>پدر را در برش بی‌سر نمودند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ره بیداد و بر روی رعیت</p></div>
<div class="m2"><p>گشودن از برای قتل و غارت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر آن دادی که باید داد داند</p></div>
<div class="m2"><p>غبار شهر را بر باد دادند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو شد شاپور ز آن جنگ و جدل فرد</p></div>
<div class="m2"><p>همان دختر به عقد خود درآورد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صباحی دید شهبر روی بستر</p></div>
<div class="m2"><p>به خون آلوده سر تا پای دختر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تفحص کرد چون معلوم گردید</p></div>
<div class="m2"><p>یکی برگ گل اندر بسترش دید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که از غلطیدن آن ماه منظر</p></div>
<div class="m2"><p>شده از برگ گل مجروح پیکر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تعجب کرد شاپور و بپرسید</p></div>
<div class="m2"><p>که ای نیکو نهال باغ امید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پدر همچون تو سرو نو رسیده</p></div>
<div class="m2"><p>مگر اندر چه بستان پروریده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>غذایت را به طفلی از چه داده</p></div>
<div class="m2"><p>لطافت از چه در طبعت نهاده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بگفتا زرده تخمی نوا داد</p></div>
<div class="m2"><p>ز مغز بره و مرغم غذا داد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شراب صافیم قوت روان کرد</p></div>
<div class="m2"><p>که جسمم را چه یاقوت روان کرد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>غضب آلوده شد شاپور بروی</p></div>
<div class="m2"><p>بدان بی‌مهر نا رعنا بزد هی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>که ای در بی‌وفایی شهره شهر</p></div>
<div class="m2"><p>بناید از تو ایمن بود در دهر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پدر را کاین همه وصفش شمردی</p></div>
<div class="m2"><p>به چون من دشمنی آخر سپردی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یقین دارم که ای مکار پر فن</p></div>
<div class="m2"><p>از او بهتر نخواهی بود با من</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ببستش برسم توسن دو گیسوی</p></div>
<div class="m2"><p>روان بنمود در هر شهر و هر کوی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یقین دارم که ای مکار پر فن</p></div>
<div class="m2"><p>از او بهتر نخواهی بود با من</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ببستش برسم توسن دو گیسوی</p></div>
<div class="m2"><p>روان بنمود در هر شهر و هر کوی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بلی (صامت) وفای دهر اینست</p></div>
<div class="m2"><p>زن و فرزند را یاری چنین است</p></div></div>