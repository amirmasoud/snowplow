---
title: >-
    شمارهٔ ۱۰ - در مدح شاه اولیاء(ع)
---
# شمارهٔ ۱۰ - در مدح شاه اولیاء(ع)

<div class="b" id="bn1"><div class="m1"><p>روز ایجاد که حق خلقت دنیا می‌کرد</p></div>
<div class="m2"><p>در پس پرده علی بود تماشا می‌کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلکه از آینه کنت نبیا چو نبی</p></div>
<div class="m2"><p>سیر در آب گل آدم و حوامی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود سر منزل آدم بشبستان عدم</p></div>
<div class="m2"><p>که دو تا قدرسا در بر یکتا می‌کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهر پاک وی اندر صدف علم اله</p></div>
<div class="m2"><p>مشق آموختن حکمت اشیا می‌کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خیابان چنان سیر احبا می‌داد</p></div>
<div class="m2"><p>بحر کیفر بسقر منزل اعدا می‌کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یاد می‌داد ره و رسم عیادت به ملک</p></div>
<div class="m2"><p>چون به تمحید خدا درج دهن وا می‌کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاور دین احد بود معین احمد</p></div>
<div class="m2"><p>هر کجا روی به بازوی توانا می‌کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز را روز عزا در بر چشم کافر</p></div>
<div class="m2"><p>تیره و تار به مثل شب یلدا می‌کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ذوالفقار دو دمش از رک شریان عدو</p></div>
<div class="m2"><p>دشت را سر به سر از موج چو دریا می‌کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدرش دیده امید مه گردون داشت</p></div>
<div class="m2"><p>زرخش کسب ضیاء بیضه بیضا می‌کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهر ایتام و ارامل شب و روز و مه و سال</p></div>
<div class="m2"><p>وقف آسایششان رنج سر و پا می‌کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کاش در یاری فرزند غریبش ز نجف</p></div>
<div class="m2"><p>یک زمانی به صف کرببلا جا می‌کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اندر آن دم که سرسینه دلبند رسول</p></div>
<div class="m2"><p>شمر بی‌واهمه می‌آمد و ماوی می‌کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>یا علی ساقی کوثر توو از شمر حسین</p></div>
<div class="m2"><p>قطره آبی بلب تشنه تمنا می‌کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بی‌کسی بین که بنزد پسر سعد پلید</p></div>
<div class="m2"><p>التماس شه دین دختر زهرا می‌کرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شمر حنجر به گلوی شه لب تشن نهاد</p></div>
<div class="m2"><p>زینب غمزده با گریه تماشا می‌کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر یتمی شرر شعله‌اش اندر دامن</p></div>
<div class="m2"><p>روی از خیمه سراسیمه به صحرا می‌کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چادر آن یک ز سر زینب بی‌کس می‌برد</p></div>
<div class="m2"><p>و آن دگر رو به حرم از پی یغما می‌کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کرد خولی چو سر خسرو دین زیب‌ تنور</p></div>
<div class="m2"><p>کاش از دود دل فاطمه پروا می‌کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>برد سیلاب فنا خرمن صبر (صامت)</p></div>
<div class="m2"><p>اندر آن روز که این مرثیه انشا می‌کرد</p></div></div>