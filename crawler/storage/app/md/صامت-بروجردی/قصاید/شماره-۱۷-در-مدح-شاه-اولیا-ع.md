---
title: >-
    شمارهٔ ۱۷ - در مدح شاه اولیاء(ع)
---
# شمارهٔ ۱۷ - در مدح شاه اولیاء(ع)

<div class="b" id="bn1"><div class="m1"><p>در لوح چون قلم به سخن ابتدا نمود</p></div>
<div class="m2"><p>دیباچه را به مدح شه اولیا نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که ساخت صف عدوقاع صفصفا</p></div>
<div class="m2"><p>هر جا که رو بیاوری مصطفی نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر جا نهاد کشف غطا را یقین وی</p></div>
<div class="m2"><p>از بس که در بحار معارف شنا نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ممکن نبود رویت واجب از این سبب</p></div>
<div class="m2"><p>او را خدای آئینه حق نما نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دادش خدا ز علم لدنی بدل فروغ</p></div>
<div class="m2"><p>پس بر تمام گمشدگان رهنما نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زد ضربتی به تارک مرحب که تا سقر</p></div>
<div class="m2"><p>هی تاخت با دو اسبه وهی مرحبا نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چون کلیم رومز جهودان چو شب کند</p></div>
<div class="m2"><p>سبابه را به کندن در چون عصا نمود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر ثبوت معنی الا به ذوالفقار</p></div>
<div class="m2"><p>احیا و اهل شرک به تصویر لا نمود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قسام خلد و نار که پیش از صف شمار</p></div>
<div class="m2"><p>از هم بهشت و دوزخیان را سوا نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پاس شریعت نبوی را نگاه داشت</p></div>
<div class="m2"><p>بعد از نبی باسم بنی اکتفا نمود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ور نه سگی که بود پلید کم از زنی</p></div>
<div class="m2"><p>کو ادعای منصب شیر خدا نمود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هرکس بچار موجه درد و بلا فتاد</p></div>
<div class="m2"><p>بهر نجات خود بعلی التجا نمود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یاللعجب که با همه قدرت نمود صبر</p></div>
<div class="m2"><p>تا شمر این قدر به حسینش جفا نمود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مظلوم و تشنه‌کام و دل افسرده و غریب</p></div>
<div class="m2"><p>با خنجر از جفا سر او را جدا نمود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در پیش چشم زینب محزون دل کباب</p></div>
<div class="m2"><p>از سم اسب جسم حسین توتیا نمود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی‌غسل و بی‌کفن بدن سبط مصطفی</p></div>
<div class="m2"><p>عریان به روی خاک به کرببلا نمود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دود از خیام آل نبی رفت تا سپهر</p></div>
<div class="m2"><p>زان آتشی که شمر ستمگر به پا نمود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیمار را سوار شتر کرد و بی‌جهاز</p></div>
<div class="m2"><p>الحق عجب رعایت زین العبا نمود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رخسار او ز ضربت سیلی کباب کرد</p></div>
<div class="m2"><p>هر جا سکینه زمزمه یا ابا نمود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آن روز شه بدیده زینب جهان سیاه</p></div>
<div class="m2"><p>کاندر خرابه با دل افسرده جا نمود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درمجلس یزید چو بنشست بی‌حجاب</p></div>
<div class="m2"><p>از غصه مرگ خویش طلب از خدا نمود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا بیشتر زند بدل وی شرر یزید</p></div>
<div class="m2"><p>چوب ستم بلعل حسین آشنا نبود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>(صامت) به ماتم شه دین بود نوحه‌گر</p></div>
<div class="m2"><p>تا از جهان مقام بدار بقا نمود</p></div></div>