---
title: >-
    شمارهٔ ۲۲ - مدح شفیعه روز جزا فاطمه زهرا(س)
---
# شمارهٔ ۲۲ - مدح شفیعه روز جزا فاطمه زهرا(س)

<div class="b" id="bn1"><div class="m1"><p>چند ز شهوت زنی به پیکر آذر</p></div>
<div class="m2"><p>سوزی از این آتش مکرر پیکر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هستی روان بگرد حشمت پویان</p></div>
<div class="m2"><p>گیری شبها عروس غفلت در بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه در این وسوسه که باشی سلطان</p></div>
<div class="m2"><p>گاه در این روزها که گیری کشور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داشت اگر زندگی ثبات نبی را</p></div>
<div class="m2"><p>«انک میت» نمی‌سرودی داور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند عزراییل سان به سجده برسیم</p></div>
<div class="m2"><p>چند چو قارون حریص در طلب زر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون بدوی کو خبر ز بحر ندارد</p></div>
<div class="m2"><p>آب حیوه از غدیر جوئی در بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن دونان بهل ز کف که نروید</p></div>
<div class="m2"><p>هرگز از شوره زار لاله عبهر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عصمت پاکی بجو که شاخه عصیان</p></div>
<div class="m2"><p>غیر ندامت نداده و ندهد بر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گَر گُلِ عصمت نچیده و ندانی</p></div>
<div class="m2"><p>رو به سوی گلستان عفت داور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بضعه خیرالوری حبیبه یزدان</p></div>
<div class="m2"><p>دختر بدرالدجی شفیعه محشر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فاطمه نام و زکیه نفس و ملک جاه</p></div>
<div class="m2"><p>عرش مقام و فرشته خوی و ملک فر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شمسه طاق حیا کتیبه عفت</p></div>
<div class="m2"><p>سیده دو سرا بتول مطهر</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ضابطه کاف و نون نتیجه خلقت</p></div>
<div class="m2"><p>واسطه کن فکان زجاجه انور</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حسنه و حوا خصال و مریم سیرت</p></div>
<div class="m2"><p>ساره و هاجر کنیز و آیه منظر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>طیبه باوقار و عصمت کبری</p></div>
<div class="m2"><p>طاهره روزگار و عفت اکبر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عالمه علم حق محدثه دهر</p></div>
<div class="m2"><p>فاکهه اصطفی عزیز پیمبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دخت رسول انام ام ائمه</p></div>
<div class="m2"><p>زوج ولی گرام همسر حیدر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هست چنین دختری چنانش بابا</p></div>
<div class="m2"><p>باید چونان زنی چنانش شوهر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهر بباید به مهر یابد پیوند</p></div>
<div class="m2"><p>ماه بباید به ماه باشد همسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اعلی آن خانواده که اینش خاتون</p></div>
<div class="m2"><p>ارفع آن آسمان که اینش اختر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آباد آن حجله که اینش خاتون</p></div>
<div class="m2"><p>احسن زان مادری که اینش دختر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>روح بود گو چه روح؟ روح مجسم</p></div>
<div class="m2"><p>عقل بود گو چه عقل؟ عقل مصور</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دختر اگر این بود نداشتی ای کاش</p></div>
<div class="m2"><p>دایه امکان به بطن الا دختر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نخل امامت ازو گرفته شکوفه</p></div>
<div class="m2"><p>فرق ولایت از او رسیده به افسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زورق ایمان بوی شناخته ساحل</p></div>
<div class="m2"><p>کشتی عرفان از وی فراشته لنگر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ملک نجابت ز امر اوست منظم</p></div>
<div class="m2"><p>شهر شرافت به فضل اوست مسخر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جاه موبد بعونِ اوست مهیا</p></div>
<div class="m2"><p>عزت سرمد به نصر اوست میسر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آتش و باد آب و خاک عالم و آدم</p></div>
<div class="m2"><p>مَلِک و مَلَک جن و انس کهتر و مهتر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بر درش آنان کنند سجده دمادم</p></div>
<div class="m2"><p>در برش اینان برند هدیه سراسر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا چه بود مصلحت ز امت عاصی</p></div>
<div class="m2"><p>خواری بی‌حد کشید و زحمت بیمر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زد عمر آتش به آن دری که پی فخر</p></div>
<div class="m2"><p>بودی روح‌الامین مدامش چاکر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>آن سگ بی‌آبرو به پهلوی پاکش</p></div>
<div class="m2"><p>زد ز غضب از شکاف در سر خنجر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دخت پیمبر ستاده با تن مجروح</p></div>
<div class="m2"><p>پور قحافه نشسته بر سر منبر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>داد از آن تازیانه کف قنفذ</p></div>
<div class="m2"><p>آه از آن ریسمان گردن حیدر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دست خدا را دو دست بست ز بیداد</p></div>
<div class="m2"><p>پهلوی زهرا شکست و خست ز کیفر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>یعنی این است اجر و مزد رسالت</p></div>
<div class="m2"><p>یعنی آنست شکر حق پیمبر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آتش این فتنه بود کآتش افروخت</p></div>
<div class="m2"><p>در صف کرب و بلا بطارم اخضر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آری اگر این عمَر به باد نمی‌داد</p></div>
<div class="m2"><p>حرمت آن رسول و حیدر صفدر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طعمه شمشیر آن عمَر ننمودی</p></div>
<div class="m2"><p>تازه جوانان ما ز اکبر و اصغر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>گر در آن خانه را نسوخته بودند</p></div>
<div class="m2"><p>بر در آن خیمه کس نمی‌زدی اخگر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غصب فدک گر کس از بتول نکردی</p></div>
<div class="m2"><p>تشنه نگشتی حسین کشته و بی‌سر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر به سرای علی نریخته بودند</p></div>
<div class="m2"><p>از سر زینب کسی نبردی معجر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گر که علی را رسن نبود به گردن</p></div>
<div class="m2"><p>بسته بغل می‌نگشت عابد مضطر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>فاطمه گر ضرب تازیانه نخوردی</p></div>
<div class="m2"><p>لعل حسین کی شد کبود ز خیزر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>(صامت) از این غم فزا عزا بنمودی</p></div>
<div class="m2"><p>قلب محبان کباب تا صف محشر</p></div></div>