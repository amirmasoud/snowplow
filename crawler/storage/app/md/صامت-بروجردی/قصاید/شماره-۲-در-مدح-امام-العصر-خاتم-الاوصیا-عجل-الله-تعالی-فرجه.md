---
title: >-
    شمارهٔ ۲ - در مدح امام العصر خاتم الاوصیا عجل‌الله تعالی فرجه
---
# شمارهٔ ۲ - در مدح امام العصر خاتم الاوصیا عجل‌الله تعالی فرجه

<div class="b" id="bn1"><div class="m1"><p>که خفته‌ اندرین قالب که باشد اندرین ماوی</p></div>
<div class="m2"><p>که گه خواند سوی دینم گهی راند سوی دنیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی پوشد به جسم طیلسان «سولت نفسی»</p></div>
<div class="m2"><p>گهی بخشد شرف بر قدم از تشریف کرمنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گهی روشن کند دل از ید و بیضاء موسایم</p></div>
<div class="m2"><p>گهی سازد چه فرعون ادعای ربکم اعلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهد در کسوت جم گه قلم خود را چو اهریمن</p></div>
<div class="m2"><p>گهی از رب هب لی چون سلیمان برکشد آوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گهی گوید بیاض طلعت نیکان بود نیکو</p></div>
<div class="m2"><p>گهی گوید سوادزلف حورالعین بود زیبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گهی گوید برو تا کعبه مقصود از خشکی</p></div>
<div class="m2"><p>گهی گوید مترس از غرق و رو کن جانب دریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود عمری کزین جمعیت ضدین از هر سو</p></div>
<div class="m2"><p>شدم دنبال قول عمرو و بکر و زیدرا پویا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نه از فتوای این گردید لختی کام جان شیرین</p></div>
<div class="m2"><p>نه از غوغای آن شد ذره مغز خرد پیدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سر تسلیم بنهادم به خاک قبله طاعت</p></div>
<div class="m2"><p>کشیدم از دل سوزان نعیر «رب سلمنا»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خدنک «ربکم ادعونی» آمد بر نشان یعنی</p></div>
<div class="m2"><p>نوید استجابت یافتم از ایزد یکتا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همانا گشتم از الهام ربانی چنین ملهم</p></div>
<div class="m2"><p>که ای سلطان ملک جهل و شاه کشور سودا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چرا در تیه حیرت مانده حیران و سرگردان</p></div>
<div class="m2"><p>نمی‌بینی مگر شمع هدایت درکف موسی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زند کوس جهان شادی بر او رنگ جهانشاهی</p></div>
<div class="m2"><p>که قائم بر وجود وی بود دنیا و مافیها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>امام عصر و ختم اوصیا شاهی که می‌باشد</p></div>
<div class="m2"><p>به دفتر خانه ایجاد نامش اولین طغری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ولی حضرت دادار و مه کعبه و زمزم</p></div>
<div class="m2"><p>سمی احمد مختارشاه یثرب و بطحا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اساس شرع و منهاج طریقت مقتدای دین</p></div>
<div class="m2"><p>سپهر مجد و منشاء حقیقت عائی اشیا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کتاب خلقت کون و مکان را اولین مطلع</p></div>
<div class="m2"><p>سودا انبیا و اولیا آخرین انشا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شه دجال کش ویران کن معموره بدعت</p></div>
<div class="m2"><p>امام بت شکن درهم نورد عزت عزی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نسیم رحمت حق شعله قهر خداوندی</p></div>
<div class="m2"><p>علیم سر مطلق راز دان وحی ما او حی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز نقش کلک نقاش ازل در صفحه هستی</p></div>
<div class="m2"><p>چنین صورت نخواهد یافت تا شام ابد اجرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عباد الله را معبود در ملک عبودیت</p></div>
<div class="m2"><p>نموده قامت موزون دو تا در سجده یکتا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>الا ای شمه طاق هدایت چند در راهت</p></div>
<div class="m2"><p>بماند باز چشم انظار بنده و مولی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گرفت ظلمت خفاش شرق و غرب عالم را</p></div>
<div class="m2"><p>نهان تا کی بزیر ابر باید بیضه بیضا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خلل افتاد در ارکان شرع و پایه ملت</p></div>
<div class="m2"><p>به غیر از اسمی از اسلام نبود در جهان بر جا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>معطل مانده حکم ایزد و امر نبی چندان</p></div>
<div class="m2"><p>که نبود امتیازی در میان مومن و ترسا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نموده مندرس کار نبوت را و افکنده</p></div>
<div class="m2"><p>به دعوای ریاست هر طرف نومفیان غوغا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز بس احکام ناشایسته ز اقلامشان جاری</p></div>
<div class="m2"><p>زمین از خون ناحق سرخ شد چون لاله حمرا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>حسام انتقامت چند ماند در نیام آخر</p></div>
<div class="m2"><p>جهان شد سر به سر ویران از این قضات بی‌پروا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به اولاد علی کردند ظلمی آل مرجانه</p></div>
<div class="m2"><p>که افتاد از زبانها نام خون ناحق یحیی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فتاده گوشوار راست از عرش خدا یعنی</p></div>
<div class="m2"><p>حسن را کرد از زهر بلا خونین جگر اسما</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برای حفظ ملک عاریت آخر معاویه</p></div>
<div class="m2"><p>حسن را کشته و ننمود از روز جزا پروا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز بعد مرگ غیر مجبتی کی گشته مظلومی</p></div>
<div class="m2"><p>تنش سوراخ از پیکان ظلم زمره اعدا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چرا کردند از فن جوار جد خود منعش</p></div>
<div class="m2"><p>مگر در شان وی نازل نشد حکم ذوی القربی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سه روز افتاد اندر کربلا بعد از حسن به یسر</p></div>
<div class="m2"><p>تن صدپاره جدت حسین در دامن صحرا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به یاری تا کشد در وقت جان دادن ز غمخواری</p></div>
<div class="m2"><p>عزیز مصطفی را رو به سوی قبله دست و پا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نه دلسوزی که گوید ابن سعد نامسلمان را</p></div>
<div class="m2"><p>مکن پامال اسباب پیکر پرورده زهرا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پی ننگ عرب این بس که آخر کهنه پیراهن</p></div>
<div class="m2"><p>نمودند از تن سبط رسول هاشمی یغما</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>این ای صاحب عصر و زمان کز معصیت(صامت)</p></div>
<div class="m2"><p>شده آهش جهان افروز اشک دید طوفان را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چه باشد کز نگاه کیمیا آثار خودسازی</p></div>
<div class="m2"><p>بحاس پیکر او را اخلاص از آذر عقبی</p></div></div>