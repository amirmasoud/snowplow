---
title: >-
    شمارهٔ ۲۶ - این قصیده از قصاید استادن المعظم مرحوم المغفور المبرور آقا میرزا عبدالمجید المتخلص بوفائی طاب ثراه تیمناً و تبرکاً ثبت شده
---
# شمارهٔ ۲۶ - این قصیده از قصاید استادن المعظم مرحوم المغفور المبرور آقا میرزا عبدالمجید المتخلص بوفائی طاب ثراه تیمناً و تبرکاً ثبت شده

<div class="b" id="bn1"><div class="m1"><p>پس بدل شبها فروزم شعله از اد وصال</p></div>
<div class="m2"><p>شد شبستان ضمیرم روشن از شمع خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رده فانوس طبعم شد بر پروانه‌ها</p></div>
<div class="m2"><p>فکر بکرم بسکه همچون شمع دارم اشتغال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس به گردون تیر آهم زد شبیخون نی عجیب</p></div>
<div class="m2"><p>چون شهاب از سیر طاهر را بسوزد پر و بال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نی نی از تنگی سینه راه آهم بسته شد</p></div>
<div class="m2"><p>یوسف غم راست زین زندان برون رفتن محال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خاصه اینک کز کلامی با دو صد لاف گزاف</p></div>
<div class="m2"><p>از خریداران یوسف گشته ام چون پیر زال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سرم زد مدح شوق نو گل گلزار دین</p></div>
<div class="m2"><p>ما کنعان ولایت اکبر یوسف جمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرو بستان حسینی آنکه در کون و مکان</p></div>
<div class="m2"><p>در سجودش خم بود قد الف قدان چودال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه اندر صورت و سیرت بود احمد نظیر</p></div>
<div class="m2"><p>آنکه اندر قوت و قدرت بود حیدر مثال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فیض لعل جانفزایش را بود عیسی مریض</p></div>
<div class="m2"><p>ماه روی پرضیائش را کف موسی ضلال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آنکه از وصف کمالش خامه از تحریر لنگ</p></div>
<div class="m2"><p>وانکه در نعمت جمالش خامه از تقریر لال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر جمالی از جمال اوست در حد نصاب</p></div>
<div class="m2"><p>هر کمالی از کمال اوست در حد کمال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن هژیر افکن هنرمندی که در روز دغا</p></div>
<div class="m2"><p>کم بود از پیر زالی در مصافش پور زال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یوسف کنعان اگر ماه جمالش دیده بود</p></div>
<div class="m2"><p>آب می‌شد در چه کنعان ز فرط انفعال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خود یدالله فوق ایدیهم بیان واضحست</p></div>
<div class="m2"><p>قوت بازوی او را در کلام ذوالجلال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دست او دست علی دست علی دست خداست</p></div>
<div class="m2"><p>داستان لحمک لحمی است بر این نکهت دال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر که اسرار «حسین منی»ات خورده به گوش</p></div>
<div class="m2"><p>دارد این رشته حقیقت تا پیمبر اتصال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نسبت جان و تن است او را بد آن سرور بلی</p></div>
<div class="m2"><p>گر بتن زخمی رسد جان را بود رنج و ملال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ورنه از بهر چه اندر ماتم آن نور عین</p></div>
<div class="m2"><p>سرو قد مصطفی شد در جنان خم چون هلال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آه از آن ساعت که آن شهزاده آزاده کرد</p></div>
<div class="m2"><p>در زمین کربلا با کوفیان عزم قتال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در حرم بهر و داغ به یکسان چون رفت گشت</p></div>
<div class="m2"><p>سیر از جان دیدشان چون ازعطش در قیل و قال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دید زنها را به یک سو بسته لب از گفتگو</p></div>
<div class="m2"><p>در تحیر بر زبانشانگشته گم راه مقال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>یک طرف اطفال کوچک سال بهر نان و آب</p></div>
<div class="m2"><p>از نفس افتاده بس کرده عجز و ابتهال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یک طرف از بسکه حیران مانده بر حال حسین</p></div>
<div class="m2"><p>نقش دیوار است گویی زینب آزرده حال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ام لیلی دید چون دارد جوانش عزم جنگ</p></div>
<div class="m2"><p>مات شد ز انسان که از لا و نعم گردید لال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>پس وداع به یکسان بنمود آن سرو روان</p></div>
<div class="m2"><p>رو به میدان کرد و گفت ای فرقه خسران مآل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این شه بی یار و بی لشگر که در این سرزمین</p></div>
<div class="m2"><p>یاور دیگر ندارد غیر یک مشت عیال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عندلیب گلشن دینست کاندر این زمین</p></div>
<div class="m2"><p>خنک ظلم کوفیانش اینچنین بشکسته بال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آنکه آمد علت ایجاد تا کی بر شما</p></div>
<div class="m2"><p>از برای قطره بی کند روی سئوال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از چه رو آب حلال او را حرام آمد حرام</p></div>
<div class="m2"><p>خون او را ریختن پس چون حلال آمد حلال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>پس به بازوی یلی مانند جد خود علی</p></div>
<div class="m2"><p>کربلا را چون احد بنمود از جنگ و جدال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>متقذ بن مره عبدی شکست از تیغ تیز</p></div>
<div class="m2"><p>عاقبت آن شاهباز اوج دین را پر و بال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شبه احمد را ز نو کرد آیت شق‌القمر</p></div>
<div class="m2"><p>ظاهر از پیشانی شمشیر ظلم آن بدفعال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بسکه خون رفتا ز تن مجروح وی یکباره رفت</p></div>
<div class="m2"><p>رشته طاقت ز دست آن جوان خردسال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دیده بست از جان شیرین و در آخر او فکند</p></div>
<div class="m2"><p>دست را در گردن اسب عقاب از ضعف حال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون حسین آمد به سروقت جوان خویشتن</p></div>
<div class="m2"><p>دید پا تا سر به خون جسم شریفش مال مال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر کشیده آه از دل پردرد گفت ای نوجوان</p></div>
<div class="m2"><p>ماندن جان بعد تو در تن بود امر محال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در جواب مادرت لیلا چه گویم در حرم</p></div>
<div class="m2"><p>گر بپرسد از من احوال تو ای نیکو خصال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون دل زار نوایی خوب از سنگ فراق</p></div>
<div class="m2"><p>قلب باب خود شکستی ای مه برج وصال</p></div></div>