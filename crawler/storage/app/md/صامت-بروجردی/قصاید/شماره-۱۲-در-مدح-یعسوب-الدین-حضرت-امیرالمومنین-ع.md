---
title: >-
    شمارهٔ ۱۲ - در مدح یعسوب الدین حضرت امیرالمومنین(ع)
---
# شمارهٔ ۱۲ - در مدح یعسوب الدین حضرت امیرالمومنین(ع)

<div class="b" id="bn1"><div class="m1"><p>شهی که محض وجودش بنای عالم شد</p></div>
<div class="m2"><p>ز حکم اوست که بنیان شرع محکم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به آبیاری تیغش ز خون گمراهان</p></div>
<div class="m2"><p>بهار گلشن شرع رسول خرم شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار بار به کعبه نجف شرف دارد</p></div>
<div class="m2"><p>که خاک تربت پاکش مطاف آدم شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان عرش چه پرسی از او که پنجه او</p></div>
<div class="m2"><p>نخست بانی و بنای عرش اعظم شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز حرم و عزم جنابش بود که در خلقت</p></div>
<div class="m2"><p>چهار عنصر با اختلاف همدم شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظهور نورش اگر ز انبیا موخر شد</p></div>
<div class="m2"><p>پس از نبی به همه انبیا مقدم شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر به مصحف دادار و فیض کرمنا</p></div>
<div class="m2"><p>از اوست زاده آدم اگر مکرم شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قوام ملک سلیمان که بود از خاتم</p></div>
<div class="m2"><p>ازو بپرس که نام که نقش خاتم شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عدم وجود شد از آن زمان که میم عدم</p></div>
<div class="m2"><p>ز لطف دوست بواو وجود او خم شد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ولی دریغ که اندر نماز وقت سجود</p></div>
<div class="m2"><p>شکسته فرق وی از تیغ ابن ملجم شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رخی که بد ز شرف اشرف از کلام الله</p></div>
<div class="m2"><p>ز خون جبهه نورانیش مترجم شد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از این گناه که سر زد ز ناخلف پسری</p></div>
<div class="m2"><p>به ناله آدم و حوا قرین ماتم شد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نبود پس به حسن درد داغ بی‌پدری</p></div>
<div class="m2"><p>چگونه آب روان در گلوی او سم شد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هزار پاره جگر شد اگر حسن از زهر</p></div>
<div class="m2"><p>حسین که آب وی از اشک چشم پرنم شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز سوز العطش کودکان شاه شهید</p></div>
<div class="m2"><p>جناب فاطمه چون موی خویش درهم شد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو ای فرات ندادی چرا به اصغر آَ</p></div>
<div class="m2"><p>از آب خوردن اعدا مگر ز تو کم شد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبود مهر چو محرم به سایه زینب</p></div>
<div class="m2"><p>به دست کوفی و شامی چگونه محرم شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تنی که داشت به دوش نبی مکان آخر</p></div>
<div class="m2"><p>ز سم اسب مترجم چه اسم اعظم شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برای داغ علی اکبرش که در دل بود</p></div>
<div class="m2"><p>سنان و خولی و تیر سه شعبه مرهم شد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بس است (صامت) از این شرح غم که تا صف حشر</p></div>
<div class="m2"><p>فلک به لرزه ملایک بناله همدم شد</p></div></div>