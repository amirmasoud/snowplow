---
title: >-
    شمارهٔ ۲۱ - در بیان رحلت پیغمبر رحمت(ص)
---
# شمارهٔ ۲۱ - در بیان رحلت پیغمبر رحمت(ص)

<div class="b" id="bn1"><div class="m1"><p>روایت است که چون از جهان حبیب خدا</p></div>
<div class="m2"><p>مسافر سفر قرب لیله الاسری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رسید وقت که قلب زمانه بگذارد</p></div>
<div class="m2"><p>از این سراچه اندوه و غم برون تازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بستر مرض افتاده بود با تب و تاب</p></div>
<div class="m2"><p>یکی به باب رسالت نمود دق الباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جناب فاطمه در پشت در نمود کدار</p></div>
<div class="m2"><p>از راه کوفتن در نمود استفسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جواب داد که ای خانواده عصمت</p></div>
<div class="m2"><p>مراست عرض نهانی به شافع امت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پی جواب جوان عرب جناب بتول</p></div>
<div class="m2"><p>به گفت نیست در این حال وقت اذن دخول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برفت و بعد زمانی نمود بار دگر</p></div>
<div class="m2"><p>پی گرفتن اذن دخول حلقه در</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن زمان شه امی لقب بهوش آمد</p></div>
<div class="m2"><p>به سوی فاطمه با ناله در خروش آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که زودتر بشتابید و در فراز کنید</p></div>
<div class="m2"><p>بروی پیک خدا باب حجره باز کنید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که او به همزن مجموعه جماعات است</p></div>
<div class="m2"><p>سفیر مرگ و شکست اساس لذاتست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هیچ کس نسپرده چنین طریق ادب</p></div>
<div class="m2"><p>ز ما سوی به جز از من کرده اذن طلب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نمود قابض ارواح اذن چون حاصل</p></div>
<div class="m2"><p>به پای بوس رسول خدای شد واصل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سلام کرد و دو دست ادب به سینه نهاد</p></div>
<div class="m2"><p>ز روی شاهد مقصود خویش پرده گشاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پیام داد ز حق کی شفیع خلق الله</p></div>
<div class="m2"><p>گرت بسر هوس وصل ماست بسم الله</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گرفت صدر امم مهلتی ز عزرائیل</p></div>
<div class="m2"><p>که تا رسید برش جبرئیل با تعجیل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به جبرئیل بفرمود سید دو سرا</p></div>
<div class="m2"><p>مرا چگونه نهادی در این زمان تنها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گفت بهر ورود تو ای فلک رتبت</p></div>
<div class="m2"><p>بدم مباشر اسباب زینت جنت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت چیست بشارانتت از خدای غفور</p></div>
<div class="m2"><p>بگو به من که شود بلکه دل ز غم مسرور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بگفت بازنشاندم حرارت نارین</p></div>
<div class="m2"><p>صفا و روح فزودم به باغ علین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو حور کرده مزین رخ از شعف غلمان</p></div>
<div class="m2"><p>زده ز شوق صف و دیده در رهت حوران</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز پیشتر بز تو و امتت به روز قیام</p></div>
<div class="m2"><p>بود به سایر امت دخول خلد حرام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بگیر و دار صف حشر و شورش محشر</p></div>
<div class="m2"><p>نخست تاج شفاعت تو را بود بر سر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جواب داد نبی کی امین وحی خدا</p></div>
<div class="m2"><p>گذشت زین همه‌ام عقده ز دل بگشا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بگفت ای به فدای دلت دگر چه غم است</p></div>
<div class="m2"><p>که بعد از این همه قلب تو باز پرالم است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جواب داد که ای پیک حضرت عزت</p></div>
<div class="m2"><p>غم دگر به دلم نیست جز غم امت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نهاد روح‌الامین پس پیام یکتا را</p></div>
<div class="m2"><p>مداد تسلیمه «ربک فترضی» را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفت غم مخواری غمگسار پیر و جوان</p></div>
<div class="m2"><p>که روز حشر خداوند قادر منان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز اهل معصیت اینقدر خواهدت بخشود</p></div>
<div class="m2"><p>که تا رضا شوی و قلب تو شود خوشنود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هزار خاک ندامت به فرق امت تو</p></div>
<div class="m2"><p>چگونه آب نگردند از خجالت تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دو چیز را به امانت گذاشت آن سرور</p></div>
<div class="m2"><p>کتاب و عترت خود را به گفته داور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شکست بعد نبی حرمت کلام الله</p></div>
<div class="m2"><p>چو قلب عترت پاکش به دشت کرب و بلا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روایت اس تکه چون بی‌کس غریب وحید</p></div>
<div class="m2"><p>به خاک ماریه بنمود جا حسین شهید</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>نهاده بود به هم هر دو دده حق بین</p></div>
<div class="m2"><p>که دید سینه مجروح خویشتن سنگین</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گشود چمش و نظر کرد شمر بی‌دین را</p></div>
<div class="m2"><p>بگفت آن. سک بی‌شرم زشت آئین را</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که ای شده ز خدا و رسول بیگانه</p></div>
<div class="m2"><p>مرا شناسی و لب تشنه می‌کشی یا نه</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جوا داد بلی می‌شناسمت ای شاه</p></div>
<div class="m2"><p>توئی حسین و بود جد تو رسول الله</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>علی بود پدر و فاطمه است مادر تو</p></div>
<div class="m2"><p>ندارم از همگی باک و می‌برم سر تو</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگفت حال که در کشتنم تر است شتاب</p></div>
<div class="m2"><p>حرارت جگرم را نشان ز قطره آب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>به طعنه گفت که داری گمان تو ای سرور</p></div>
<div class="m2"><p>که هست باب گرام تو ساقی کوثر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بگو بیاید و بنشاند از جگر تابت</p></div>
<div class="m2"><p>کند ز آب به هنگام مرگ سیرابت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>کشید خنجر از بهر قتل آن امام امم</p></div>
<div class="m2"><p>اساس خرمی کائنات زد بر هم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بس است (صامت) از این ماجری که لال شوی</p></div>
<div class="m2"><p>اگر زیاد پی شرح این مقال شوی</p></div></div>