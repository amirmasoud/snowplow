---
title: >-
    شمارهٔ ۳۳ - جنگ شداد با رب‌العالمین جل ذکره
---
# شمارهٔ ۳۳ - جنگ شداد با رب‌العالمین جل ذکره

<div class="b" id="bn1"><div class="m1"><p>کرد چون شداد از راه عناد</p></div>
<div class="m2"><p>سرکشی بنیاد با رب العباد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز عنایت کردگارمهربان</p></div>
<div class="m2"><p>هرچه او را داد در دنیا امان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مگر از گمرهی آید براه</p></div>
<div class="m2"><p>قلب وی شد دم به دم بدتر سیاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقبت مامور شد از کردگار</p></div>
<div class="m2"><p>حضرت داود بر آن نابکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه خواند افسانه دوزخ برش</p></div>
<div class="m2"><p>بیشتر شد مغز کبر اندر سرش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی داود برآن بد سرشت</p></div>
<div class="m2"><p>کرد توصیف گلستان بهشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاقبت داود را اندر جواب</p></div>
<div class="m2"><p>ساخت غمگین زین جواب ناصواب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت خود سازم بهشتی باصفا</p></div>
<div class="m2"><p>من نمی‌خواهم بهشت کبریا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>داد فرمان بر خطا و روم و چین</p></div>
<div class="m2"><p>بر تمام ربع مسکون زمین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از زر و سیم و جواهر بار بار</p></div>
<div class="m2"><p>استر و اشتر قطار اندر قطار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گرد کردند آنقدر بر روی هم</p></div>
<div class="m2"><p>کز بیان وی شود عاجز قلم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>منتخب کردند خوش آب و هوا</p></div>
<div class="m2"><p>طرفه صحرایی وسیع و باصفا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جمله معماران ز هر شهر و دیار</p></div>
<div class="m2"><p>جمع گردیدند روز و شب بکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا به سیصد سال با آن اهتمام</p></div>
<div class="m2"><p>گشت آن بنیاد نامیمون تمام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کرده وصف وی خدای ذوالنعم</p></div>
<div class="m2"><p>نام او باشد گلستان ارم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون خبر دادن بر آن بی‌ادب</p></div>
<div class="m2"><p>کرد سرداران لشگر را طلب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شد ز دار الملک خود آن نابکار</p></div>
<div class="m2"><p>از پی سیر بهشت خود سوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با جلالت کرد طی راه امید</p></div>
<div class="m2"><p>دوزخی تا بر در جنت رسید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از دو پا یکپا برون کرد از رکاب</p></div>
<div class="m2"><p>تا شود از سیر جنت کامیاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه کرده صید پشه پیل را</p></div>
<div class="m2"><p>کرد حاضر نزدش عزرائیل را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جانب شداد با شکل مهیب</p></div>
<div class="m2"><p>پیک حق زد هی به آواز عجیب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>لرزلرزان گفت بر گو کیستی</p></div>
<div class="m2"><p>خار راه من برای چیستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت عزرائیلم و بسته کمر</p></div>
<div class="m2"><p>بهر قبض و روح تو ای خیره سر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>داشت یکپا بر زمین یکپا بزین</p></div>
<div class="m2"><p>کرد قبض روح آن زشت لعین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای خداوند عزیز ذوانتقام</p></div>
<div class="m2"><p>داد از شداد شوم شهر شام</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبر کردی آنقدر کان بی‌ادب</p></div>
<div class="m2"><p>کشت سبط مصطفی را تشنه‌لب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>راس او را با حریم آنجناب</p></div>
<div class="m2"><p>داد جا در مجلس بزم شراب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با چنان حالت که دارد کبر ننک</p></div>
<div class="m2"><p>از چنین ظلمی به کفار فرنک</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بت‌پرست و گبر و ترسا و یهود</p></div>
<div class="m2"><p>بر سر کرسی به نزد آن عنود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مو پریشان عصمت پروردگار</p></div>
<div class="m2"><p>زینب و کلثوم با حال نزار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر دو اطفال بازو درطاب</p></div>
<div class="m2"><p>ایستاده سر برنه بی‌نقاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>غل به گردن قبله اهل یقین</p></div>
<div class="m2"><p>با تن تب‌دار زین‌العابدین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در بر آنرو سیاه تیره بخت</p></div>
<div class="m2"><p>بی‌عمامه بر سر پا پیش تخت</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گه به زنیب می‌زدی زخم زبان</p></div>
<div class="m2"><p>گاه با کلثوم زار و ناتوان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گاه خندیدی ز عجب آن بت‌پرست</p></div>
<div class="m2"><p>گاه گردیدی ز شرب خمر مست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گه بلب‌های شهید کربلا</p></div>
<div class="m2"><p>می‌نمودی خیزران را آشنا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گاه تا آرد دل زینب بدرد</p></div>
<div class="m2"><p>زین مزخرف کفر خود را تازه کرد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>«لیت اشیا خی به بدر شهدوا</p></div>
<div class="m2"><p>جزع الخزرج من وقع الاسل»</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>«فاهلوا و استهلوا فرحاً</p></div>
<div class="m2"><p>ثم قالوا یا یزید لاتشل»</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آه از آن ساعت که کرد آن زشت دین</p></div>
<div class="m2"><p>حکم بر قتل امام ساجدین</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دید چون زینب به دست قاتلش</p></div>
<div class="m2"><p>بی‌تحمل کنده شد از جا دلش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چشم گریان کرد رو سوی یزید</p></div>
<div class="m2"><p>کی لعین منما امیدم ناامید</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>این علبل بی‌نوای خسته‌جان</p></div>
<div class="m2"><p>یادگاری مانده از یک دودمان</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>قتل وی گر می‌کند قلب تو خوش</p></div>
<div class="m2"><p>پس مرا ای بی‌حیا اول بکش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>