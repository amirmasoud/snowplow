---
title: >-
    شمارهٔ ۶ - سئوال از ملک الموت
---
# شمارهٔ ۶ - سئوال از ملک الموت

<div class="b" id="bn1"><div class="m1"><p>شنیده‌ام که خدای مهیمن معبود</p></div>
<div class="m2"><p>زمانی از ملک الموت این سئوال نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که ای تو قابض روح تمام خلق جهان</p></div>
<div class="m2"><p>ز جن و انس و سیاه و سفید و پیر و جوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن زمان که به این امر گشته ای مامور</p></div>
<div class="m2"><p>گذشته بر تو از این کار بس سنین و شهور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فوق عرش ز هر خانمان بری شیون</p></div>
<div class="m2"><p>شوند بسته فتراک تو چه مردوچه زن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دمی شده است که سوزد دلت به حال کسی</p></div>
<div class="m2"><p>که برکشی ز دل تنگ آشیان نفسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین به درگه پروردگار رب جلیل</p></div>
<div class="m2"><p>نمود عرض به معجز و نیاز عزرائیل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که ای خدا دل من درد و جای پرخون است</p></div>
<div class="m2"><p>که حد گفتن وی از حساب بیرون است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اول به ماتم طفل صغیر خون جگر</p></div>
<div class="m2"><p>که نبودش بدم مرگ مادر و پدری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یتیم چون بکشاکشز دادن جانست</p></div>
<div class="m2"><p>مرا مصیبت بیرون و رحد امکانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دوم کسی که غریبست و از وطن مهجور</p></div>
<div class="m2"><p>ز یار و یاور و اهل دیار باشد دور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نه مونسی که شود رازدار و دلجویش</p></div>
<div class="m2"><p>نه همدمی که دهد دل به مرحمت سویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غریب را نکند هیچ کس گذر بسرش</p></div>
<div class="m2"><p>مگر غریب دگر در وطن برد خبرش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فغان که بازمرا در دل اضطراب افتاد</p></div>
<div class="m2"><p>به یاد شام همان کشور خراب افتاد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قسم به ذات خدا کودک یتیم حسین</p></div>
<div class="m2"><p>همان ستمکش محزون الیم حسین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به غیر درد یتیمی مگر غریب نبود</p></div>
<div class="m2"><p>همان ستمکش محزون و غم نصیب نبود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ستمکش دو جان دختری رقیه بنام</p></div>
<div class="m2"><p>در آن زمان که مکان داشت در خرابه شام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نبود ورد زبانش به غیر نام پدر</p></div>
<div class="m2"><p>غذای روز شبش اشک چشم و لخت جگر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یزید شد چه خبردار از تب و تابش</p></div>
<div class="m2"><p>روانه کرد به تسکین وی سر بابش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سر پدر به طبق نزد وی چو بنهادند</p></div>
<div class="m2"><p>ستم رسیده زنان خود ز دیده بگشادند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چون آن صغیر گرفت از سر طبق سرپوش</p></div>
<div class="m2"><p>سر پدر به طبق دید از سرش شد هوش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به هوش آمد و آن سر گرفت بر سینه</p></div>
<div class="m2"><p>زبان گشود پی شکوه‌های دیرینه</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که ای جناب پدر تاکنون کجا بودی</p></div>
<div class="m2"><p>چرا ز دختر دلبند خود جدا بودی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو بودی آنکه مرا بود جا در آغوشت</p></div>
<div class="m2"><p>چگونه گشت به کلی ز من فراموشت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به همرهت علی اصغر چرا نیامده است</p></div>
<div class="m2"><p>برادرم علی اکبر چرا نیامده است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو زنده باشی و باشد عذار من نیلی</p></div>
<div class="m2"><p>ز دست شمر لعین تا یکی خورم سیلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پدر به شام نباشد مگر دگر خانه</p></div>
<div class="m2"><p>که داده‌اند به ما جا به کنج ویرانه</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به صدقه مرد و زن کوفیان بی‌پروا</p></div>
<div class="m2"><p>به ما دهند همی نان پاره و خرما</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز اهل شام همین نیست ای پدر گله‌ام</p></div>
<div class="m2"><p>نظاره کن به کف پای پر ز آبله‌ام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بروی خار مغیلان بسی دویدم من</p></div>
<div class="m2"><p>جفای کوفی و شامی همی کشیدم من</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان رسید که از درد بی‌مدد کاری</p></div>
<div class="m2"><p>نهان شدم به تعب زیر بته خاری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خاک بستر و خشمت و خرابه‌ام بالین</p></div>
<div class="m2"><p>عجب یتیم نوازی کند یزید لعین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>لب مرار عطش و قحط آب گشته کبود</p></div>
<div class="m2"><p>لب مارک تو از چه باب گشته کبود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گمانم این لب و دندان همچو مروارید</p></div>
<div class="m2"><p>کبود گشته پدرجان ز ضرب چوب یزید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو با پدر کمی از درد دل اشاره نمود</p></div>
<div class="m2"><p>خرابه را ز تف آه پر ستاره نمود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز روی سینه او سر به یک طرف غلطید</p></div>
<div class="m2"><p>به خاک روی یتیمی نهاد و آه کشید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فغان و آه که (صامت) به وقت جان دادن</p></div>
<div class="m2"><p>بدی به گردن آن طفل بی‌گناه رسن</p></div></div>