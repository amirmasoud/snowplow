---
title: >-
    شمارهٔ ۳۵ - ورود سر مطهر به دیر راهب
---
# شمارهٔ ۳۵ - ورود سر مطهر به دیر راهب

<div class="b" id="bn1"><div class="m1"><p>چون سر مهر افسر سبط رسول</p></div>
<div class="m2"><p>ساخت اندر دیر نصرانی نزول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرد در آن شب ز راه احترام</p></div>
<div class="m2"><p>راهب اندر خدمت آن سر قیام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شست شو بنمود او را از وفا</p></div>
<div class="m2"><p>بر سر سجاده خود داد جا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم گریان با دو زانوی ادب</p></div>
<div class="m2"><p>نزد راس خسرو ملک عرب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مناجات آمد و سوز و گداز</p></div>
<div class="m2"><p>در حضور کردگار بی‌نیاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کی ز تو گردنده دوران سپهر</p></div>
<div class="m2"><p>صانع ارض و سماء و ماه و مهر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده از اسرار این سرباز کن</p></div>
<div class="m2"><p>با من افسرده‌اش دمساز کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غصه این سر شده قفل دلم</p></div>
<div class="m2"><p>باز کن این قفل و بگشا مشکلم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بدانم این سر دور از بدن</p></div>
<div class="m2"><p>کیست وز چه گشته ببریده ز تن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت پس کی صد چو عیسی چاکرت</p></div>
<div class="m2"><p>زنده دل از خضر از لب جان پرورت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای نشان کبریای بی‌نشان</p></div>
<div class="m2"><p>از رخت ظاهر ز سیمایت عیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>باز گو ای راس پرخون کیستی</p></div>
<div class="m2"><p>از بدن ببریده بهر چیستی؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نور چشم احمد ختمی مآب</p></div>
<div class="m2"><p>باز با آن حال آمد در جواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت من هابیل درد و ماتمم</p></div>
<div class="m2"><p>نوح طوفان دیده بحر غمم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>من خلیل نار بی‌سامانیم</p></div>
<div class="m2"><p>در منای غم ذبیح ثانیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پیر کنعان دیار کربتم</p></div>
<div class="m2"><p>یوسف زندان چاه غربتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سر بریده حضرت یحیی منم</p></div>
<div class="m2"><p>بر سر دار فنا عیسی منم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ماسوی را من بر تبت سرورم</p></div>
<div class="m2"><p>از شرف ریحانه پیغمبرم(ص)</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست مظلومی چو من در نشاتین</p></div>
<div class="m2"><p>هست نامم شاه مظلومان حسین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من که اندر این بلا و کربتم</p></div>
<div class="m2"><p>زاده پیغمبر این امتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>من ز تیغ ظلم بی‌سر گشته‌ام</p></div>
<div class="m2"><p>تشنه بی‌سر زیر خنجر گشته‌ام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دیده‌ام اندر زمین کربلا</p></div>
<div class="m2"><p>بزم عیش اکبر خود را عزا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قامتم خم گشته از بار محن</p></div>
<div class="m2"><p>از غم بی‌دستی عباس من</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قاسم داماد بی‌سر دیده‌ام</p></div>
<div class="m2"><p>تیر در حقلوم اصغر دیده‌ام</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشته از بی‌باکی قوم ضلال</p></div>
<div class="m2"><p>زیر سم اسب جسمم پایمال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در کف دشمن ز دور چرخ پیر</p></div>
<div class="m2"><p>شد حریم من اسیر و دستگیر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من براه کبریا سر داده‌ام</p></div>
<div class="m2"><p>جان و سر در راه داور داده‌ام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گه سرم را نیزه گردد نخل طور</p></div>
<div class="m2"><p>گاه سازد جای در کنج تنور</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه کنند این کوفیان تیره بخت</p></div>
<div class="m2"><p>راس من چون میوه آویز درخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گه ز بام و در جدا از نام و ننگ</p></div>
<div class="m2"><p>بر سر من می‌زنند از قهر سنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اهل بیتم چون اسیران تنار</p></div>
<div class="m2"><p>هر زمان باشند در شهر و دیار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون تو ای راهب مرا یار آمدی</p></div>
<div class="m2"><p>بر من غمدیده غمخوار آمدی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ساز از کیش نصاری احتراز</p></div>
<div class="m2"><p>درحقیقت رو کن از راه مجاز</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گر شفاعت باشدت از ما امید</p></div>
<div class="m2"><p>شو مسلمان تا که گردی روسفید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صات) این هنگامه را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کرد چون زهرای اطهر از جهان</p></div>
<div class="m2"><p>رو به سوی عشرت‌آباد جنان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در مدینه تیره شد چون شد تمام</p></div>
<div class="m2"><p>پیش چشم ام ایمن روزگار</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>وسعت ملک مدینه در نظر</p></div>
<div class="m2"><p>شد بوی از چشم سوزن تنگ‌تر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از هجوم درد و اندوه و محن</p></div>
<div class="m2"><p>کرد عزم شهر مکه از وطن</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در یکی از منزل ز تاب آفتاب</p></div>
<div class="m2"><p>تشنگی برد از کف او صبر و تاب</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>با تضرع در بر یزدان پاک</p></div>
<div class="m2"><p>سود پیشانی در آن صحرا به خاک</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گفت کای معمار نه طاق سپهر</p></div>
<div class="m2"><p>روشنی بخش سپهر از ماه و مهر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تشنگی افنده بر جام اخگرم</p></div>
<div class="m2"><p>من کنیز دختر پیغمبر (ص)</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رحم بر من با دل بی‌تاب کن</p></div>
<div class="m2"><p>اندر این صحرا مرا سیراب کن</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گفت راوی ناگهان از آسمان</p></div>
<div class="m2"><p>دلوی آمد بر زمین با ریسمان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ام ایمن برگرفت آن دلو آب</p></div>
<div class="m2"><p>خورد و شد ایمن ز تاب التهاب</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کرد شکر کردگار لم یزال</p></div>
<div class="m2"><p>زنده بود از آن زمان تا هفت سال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>اندر این مدت ز فیض دادگر</p></div>
<div class="m2"><p>می‌ ندید از تشنگی اسم و اثر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بار دیگر آمدم زین ابتلا</p></div>
<div class="m2"><p>یادم از لب تشنگان کربلا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>غنچه‌های گلشن باغ بتول</p></div>
<div class="m2"><p>میوه‌های نخل بستان رسول</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر یکی افتاده از تاب عطش</p></div>
<div class="m2"><p>چون گل پژمرده و بنموده غش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شیرخوار ناز پستان امید</p></div>
<div class="m2"><p>اصغر ششماهه شاه شهید</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>از عطش چون شیر پستان رباب</p></div>
<div class="m2"><p>از غزال چشم او رم کرده خواب</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>قره العین رسول محترم</p></div>
<div class="m2"><p>برد او را سوی میدان از حرم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>پیش چشم مردم دنیاپرست</p></div>
<div class="m2"><p>کرد چون قرآن علم بر روی دست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گفت کی ببرحم قول دل سیاه</p></div>
<div class="m2"><p>چیست جرم این صغیر بی‌گناه</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>کز شرار تشنگی پر می‌،ند</p></div>
<div class="m2"><p>چنک بر پستان مادر می‌زند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>گر گمان دارید من از بهر خویش</p></div>
<div class="m2"><p>آب می‌خواهم به احوال پریش</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>خود بگیرید از من این افسرده را</p></div>
<div class="m2"><p>این نهال نورس پژمرده را</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چاره‌یی بر قلب بی‌تابش کنید</p></div>
<div class="m2"><p>در بر خود برده سیرابش کنید</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در جواب زاده ختمی مآب</p></div>
<div class="m2"><p>کوفیان بستند لب از شیخ و شاب</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>از میان آن گروه ده دله</p></div>
<div class="m2"><p>بر کمان بنهاده پیکان حرمله</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>زد به حلقوم شریف آن صغیر</p></div>
<div class="m2"><p>جای آب آن نامسلمان نوک تیر</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>همچو به سمل بر سر دست پدر</p></div>
<div class="m2"><p>زد به خون خویش اصغر بال و پر</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>دست و پای بسته از دام جهان</p></div>
<div class="m2"><p>مرغ روحش بال زد سوی جنان</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگام را</p></div></div>