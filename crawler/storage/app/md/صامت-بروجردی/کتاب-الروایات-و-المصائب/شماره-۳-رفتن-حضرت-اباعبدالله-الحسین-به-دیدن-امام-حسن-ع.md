---
title: >-
    شمارهٔ ۳ - رفتن حضرت اباعبدالله الحسین به دیدن امام حسن(ع)
---
# شمارهٔ ۳ - رفتن حضرت اباعبدالله الحسین به دیدن امام حسن(ع)

<div class="b" id="bn1"><div class="m1"><p>روایت است که یک روز نور چشم نبی</p></div>
<div class="m2"><p>حسین سلاله نسل محمد عربی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای دیدن فخر انام و شاه ز من</p></div>
<div class="m2"><p>چراغ دیده آن عبا امام حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>علم نمود قد خود چو شاخه شمشاد</p></div>
<div class="m2"><p>چو سرو سوی سرای حسن به راه افتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسید چون بدر حجره برادر خویش</p></div>
<div class="m2"><p>شنید صوت دلارای ماه انور خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که کرده ملک و ملک را ز لحن خود حیران</p></div>
<div class="m2"><p>حسن ز صوت حسن در تلاوت قرآن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای روح فزای همان رفیع جناب</p></div>
<div class="m2"><p>نموده آب روان را به جای خود در خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ستاده تافته پرها به هم تمام طیور</p></div>
<div class="m2"><p>برون نموده سر از غرفه‌های جنت حور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به داد خسرو لب تشنه تکیه بر دیوار</p></div>
<div class="m2"><p>ز دیده کرد روان گریه همچو ابر بهار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد چو موسی عمران ز پای وی تا فرق</p></div>
<div class="m2"><p>به مثل وادی سینا به آب حیرت غرق</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حسن ز غنچه شاداب در شکر ریزی</p></div>
<div class="m2"><p>حسین ز دیده غمناک در گهر ریزی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حسن فشانده در از بحر سینه مواج</p></div>
<div class="m2"><p>حسین مستمع صوت عشق در معراج</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکی ز جمله خدام مجتبی ز وداد</p></div>
<div class="m2"><p>گذار وی ببر شاه تشنه لب افتاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خبر به نزد حسن برد کی رفیع جناب</p></div>
<div class="m2"><p>حسین برادر گریان خویش را دریاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که مدتی ست به بیرون حجره گریانست</p></div>
<div class="m2"><p>به قرص ماه رخ خود ستاره افشانست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حسن ز حجره به سوی حسین شتاب نمود</p></div>
<div class="m2"><p>تفحص الم قلب آن جناب نمود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سئوال کرد که ای از محن شرشته گلت</p></div>
<div class="m2"><p>چه محنتست برادر که کرده رو به دلت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جواب داد که ای شمع جمع انجمنم</p></div>
<div class="m2"><p>شه ممالک اندوه ابتلا حسنم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از آن زمان که رسید از لب درافشانت</p></div>
<div class="m2"><p>به گوش من ز در حجره صوت قرآنت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بلند گشت ز جانم فغان غم فرسود</p></div>
<div class="m2"><p>که از چه عاقبت این لب شود ز زهر کبود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چگونه این رخ رنگین ز لاله حمرا</p></div>
<div class="m2"><p>شود به رنگ ز مرد ز کینه اعدا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرا زحدت سم ریزد از گلو ببرت</p></div>
<div class="m2"><p>به طشت یکصدوهفتاد پاره جگرت</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از این سخن حسن از دل فغان و ناله کشید</p></div>
<div class="m2"><p>به گفت کای لب عطشان به نزد آب شهید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بلای من چوبلاهای غم فروز تو نیست</p></div>
<div class="m2"><p>ز ابتلای جهان روز کس چو رو زتو نیست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به کربلا پی قتل تو سی هزار نفر</p></div>
<div class="m2"><p>کشند سنگ و نی و چوب و ناوک خنجر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به سوی عرش رسانند آه جان کاهت</p></div>
<div class="m2"><p>کشند سنگ‌دلان قربه الی اللهت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر آنچه یاد نمائی تو از خدا و رسول</p></div>
<div class="m2"><p>کسی نمی‌کند آن روز حجت تو قبول</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کسی به حالت بی‌یاریت نظر نکند</p></div>
<div class="m2"><p>گلوی خشک تو ز آب فرات تر نکند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بنای عمر ترا رخنه افکند به اساس</p></div>
<div class="m2"><p>غم عروسی قاسم شهادت عباس</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ترا به کرببلا می‌نماید از جان سیر</p></div>
<div class="m2"><p>فراق اکبر و اندوه اصغر بی‌شیر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو زیر خاک به عزت کنی نهان تن من</p></div>
<div class="m2"><p>تن تو بر سر خاک اوفتد بدون کفن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر به پرده نهان است عزت منزار</p></div>
<div class="m2"><p>سر برهنه رود خواهر تو در بازار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا به دامن تو وقت مرگ باشد سر</p></div>
<div class="m2"><p>شود نهان سر تو در تنور خاکستر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز سوز زهر گر افتد به طشت من ببرم</p></div>
<div class="m2"><p>ز بی‌کسی صد و هفتاد پاره جگرم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>زند یزید لعین پیش چشم زینب تو</p></div>
<div class="m2"><p>میان طشت طلا چوب ظلم بر لب تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بپوش (صامت) از این شرح جانگداز نظر</p></div>
<div class="m2"><p>که نیست مستمعان را توان و تاب دگر</p></div></div>