---
title: >-
    شمارهٔ ۱۲ - در وصف غلام سیاه حضرت سیدالساجدین
---
# شمارهٔ ۱۲ - در وصف غلام سیاه حضرت سیدالساجدین

<div class="b" id="bn1"><div class="m1"><p>بد سیاهی در سپاه کربلا</p></div>
<div class="m2"><p>خواجه زیبنده زین العبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دیار حق پرستی منزلش</p></div>
<div class="m2"><p>بلکه حق در جلوه آب و گلش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خضر سرگردان آب جوی او</p></div>
<div class="m2"><p>راه اسکندر رخ دلجوی او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساخته رب اللیالی و الدهور</p></div>
<div class="m2"><p>نور رویش نور فوق کل نور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشک را بنموده رویش درختن</p></div>
<div class="m2"><p>صورتی از معنی حب الوطن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شام یلدا را ز مویش نصرتی</p></div>
<div class="m2"><p>لیله الاسری ز مویش آتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از سیاهی پای تا سر یک ورق</p></div>
<div class="m2"><p>یک ورق از دفتر توفیق حق</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیکرش در محضر پاک و دود</p></div>
<div class="m2"><p>در شهادت مهر ارباب شهود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برده خاک پای او را ارمغان</p></div>
<div class="m2"><p>از برای سرمه حوران جنان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شد سیه یعنی سواد چشم حو</p></div>
<div class="m2"><p>بهر زینب سرمه‌ای دارد ضرور</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این کرامت عش عالم سوز کرد</p></div>
<div class="m2"><p>کو نهان در شب رخ نوروز کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا ز زخم چشم بد یابد امان</p></div>
<div class="m2"><p>نور را بنمود در ظلمت نهان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کرد از علیین چو در دنیا گذر</p></div>
<div class="m2"><p>تیر: شد رنگ وی از رنج سفر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جاعل نور و ظلم‌های نقاب</p></div>
<div class="m2"><p>کرده ظلمت را حجاب آفتاب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بشکند تا قیمت شب‌های قدر</p></div>
<div class="m2"><p>در سیاهی منخسف رویش چو بدر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شد مرکب تا به حکم سرنوشت</p></div>
<div class="m2"><p>برد خلقی را ز دوزخ در بهشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>پیش پیش از بی‌کسی در جیش شاه</p></div>
<div class="m2"><p>بهر خود پوشیده بود درخت سیاه</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با همه پستی بلند اقبال شد</p></div>
<div class="m2"><p>بهر رخسار شهادت خال شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با چنان روی سیاه و بوی زشت</p></div>
<div class="m2"><p>شد بزرگ روسفیدان بهشت</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از پی تحصیل علم کیمیا</p></div>
<div class="m2"><p>رفت مس را کرد قربان طلا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا نماید سکه دین پایدار</p></div>
<div class="m2"><p>سیم ایمان را چوسم گردید بار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از درنگ دار فانی خسته شد</p></div>
<div class="m2"><p>با حریفان بقا پیوسته شد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کرد با قانون تسلیم و رضا</p></div>
<div class="m2"><p>خون خود را داخل خون خدا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از تمام ماسوا مایوس شد</p></div>
<div class="m2"><p>دست حق را ار پی‌ پابوس شد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یعنی آمد آن غلام باوفا</p></div>
<div class="m2"><p>خدمت مظلوم دشت کربلا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قند تر پیوسته ناز شکر فشاند</p></div>
<div class="m2"><p>لعل و مروارید از گوهر فشاند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جبهه را بر قبله طاعت نهاد</p></div>
<div class="m2"><p>سر به پای شاه دین بهر جهاد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سوی وی از مظهر رب النعم</p></div>
<div class="m2"><p>شد ندا از قاب قوسین خیم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کای بلب کف بر سر افکنده خروش</p></div>
<div class="m2"><p>با حریفان شهادت جرعه نوش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ای زایمان تو محکم پشت دین</p></div>
<div class="m2"><p>رو به نزد رهبر دین عابدین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رشته پیمان وی در پای تست</p></div>
<div class="m2"><p>مالک الملک جهان مولای تست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بهر پاس رسم و آئین ادب</p></div>
<div class="m2"><p>رخصت میدان ز وی بنما طلب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شاه امکان یعنی آن عبد ذلیل</p></div>
<div class="m2"><p>شد به سوی سبط احمد جبرئیل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در حضور مهبط وحی خدا</p></div>
<div class="m2"><p>مقتدای ساجدین زین العبا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چون کلیم اندر مناجات وثنا</p></div>
<div class="m2"><p>شد به قربطور سر کبریا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>عرض حاجت آنچه در دلداشت گفت</p></div>
<div class="m2"><p>آنچه باید بشنود از حق شنفت</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو مرخص از بساط طور شد</p></div>
<div class="m2"><p>جانب فرعونیان مامور شد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اژدهای تیغ آورد از غلاف</p></div>
<div class="m2"><p>بهر قتل ساحران اندر مصاف</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>حکم شد از معنی ایمان و دین</p></div>
<div class="m2"><p>خواجه کونین زین العابدین</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کاتش غم را ز سر تا پازدند</p></div>
<div class="m2"><p>دامن آن خیمه را بالا زدند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا ببینند در منای کربلا</p></div>
<div class="m2"><p>بذل جان آن غلام باوفا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برق تیغ وی بدان قوم یهود</p></div>
<div class="m2"><p>زد شرر چون صرصر عاد و ثمود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همچو رو به شد به سوراخ هوام</p></div>
<div class="m2"><p>ار نهیبش جان شیرین در کنام</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>داس تیغش چون کند تنها درو</p></div>
<div class="m2"><p>قابض الارواح را شد پیشرو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کوفیان بر مالک نار و سقر</p></div>
<div class="m2"><p>تنک کردند عرصه از این المفر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شامیان را نزهت دارالقرار</p></div>
<div class="m2"><p>شد به دوزخ آشکار اندر فرار</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>عاقبت از پشت زین بر روی خاک</p></div>
<div class="m2"><p>کرد جانا جسم چون گل چاکچاد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شد نسیم رحمت حق یاورش</p></div>
<div class="m2"><p>یعنی آمد شاه خوبان برسرش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>روی آن زیبا غلام با وفا</p></div>
<div class="m2"><p>شد به وجه الله آخر آشما</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>یعنی اندر وقت مردن از وداد</p></div>
<div class="m2"><p>رو به رویش زاده زهرا نهاد</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>