---
title: >-
    شمارهٔ ۳۷ - در بیان شهادت جناب حر(ع)
---
# شمارهٔ ۳۷ - در بیان شهادت جناب حر(ع)

<div class="b" id="bn1"><div class="m1"><p>روز عاشورا چو حر نامدار</p></div>
<div class="m2"><p>دید شد شور قیامت آشکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خلیل‌الله در کوی منا</p></div>
<div class="m2"><p>گشته شاه دین مهیای فدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوفیان کافر دنیارست</p></div>
<div class="m2"><p>شسته از دین پیمبر جمله دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لشگر شیطان گرفته در میان</p></div>
<div class="m2"><p>نقطه توحید را پرگار سان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد از رخسار حر پرواز رنگ</p></div>
<div class="m2"><p>گشت عقل و جهل او سرگرم جنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل گفتا این عزیز مصطفی است</p></div>
<div class="m2"><p>جهل گفتا دو دور اشقیا است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل گفتا با شهیدان یار باش</p></div>
<div class="m2"><p>جهل گفت از عمر برخوردار باش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل گفتا در رضای حق بکوش</p></div>
<div class="m2"><p>جهل گفتا چشم از نسیه بپوش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل گفت این تشنه لب مهمان تست</p></div>
<div class="m2"><p>جهل گفتا موسم جولان تست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل گفت ای حر پشیمان می‌شوی</p></div>
<div class="m2"><p>جهل گفتا صاحب نان می‌شوی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عقل گفت ای حر حسین بی‌یاور است</p></div>
<div class="m2"><p>جهل گفتا جاه و منصب بهتر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عقل گفتا با حسین بگذر ز جنگ</p></div>
<div class="m2"><p>جهل گفتا چون کنی با نام و ننگ</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عقل دامانش سوی جنت کشید</p></div>
<div class="m2"><p>جهل گفتا می‌دهد خلعت یزید</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آخر از امداد عقل پربها</p></div>
<div class="m2"><p>حر شد از گرداب تاریکی رها</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با سری از خجلت افکنده به زیر</p></div>
<div class="m2"><p>شد بر سبط رسول بی‌نظیر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کرد بر فرزند پیغمبر سلام</p></div>
<div class="m2"><p>گفت کای دارای گردون احتشام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای بدرالملک ایمان شهریار</p></div>
<div class="m2"><p>من ندانستم شود این گونه کار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون نمی‌دانستم از ره چاه را</p></div>
<div class="m2"><p>روز اول بر تو بستم راه را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>او فکندم از نخستین ای جناب</p></div>
<div class="m2"><p>در دل اهل حریمت اضطراب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>توبه کردم بنده‌ات را بنده‌ام</p></div>
<div class="m2"><p>تا قیامت از رخت شرمنده‌ام</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بگذران شاها سرم را از سپهر</p></div>
<div class="m2"><p>بر مس قلبم بزن اکسیر مهر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ساز صافی از صفا آئینه‌ام</p></div>
<div class="m2"><p>دست محرومی منه بر سینه‌ام</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>توبه‌ام را کن قبول و از وداد</p></div>
<div class="m2"><p>بر من عاصی بده اذن جهاد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مظهر لطف خداوند و دود</p></div>
<div class="m2"><p>از عذار حر غبار غم زدود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت ای شورید آزرده جان</p></div>
<div class="m2"><p>این زمان هستی تو بر ما میهمان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مهمان را کس نسازد بی‌دریغ</p></div>
<div class="m2"><p>در چنین روزی میان تیر و تیغ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زد شرر فرمان شه بر جان حر</p></div>
<div class="m2"><p>دیده را از اشک چون در کرد پر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یعنی ای پرورده خیر الانام</p></div>
<div class="m2"><p>گر بود از بهر مهمان احترام</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از چه پس نبود ترحم سوی تو</p></div>
<div class="m2"><p>هر کسی تیغی کشد بر روی تو</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اهل‌بیتت را لب آب روان</p></div>
<div class="m2"><p>می‌رود سوز عطش بر آسمان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چشم خود پوشیده این قوم جهول</p></div>
<div class="m2"><p>از حدیث «اکرم الضیف» رسول</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حرمله اندر کمان بنهاد تیر</p></div>
<div class="m2"><p>بهر حلق اصغر ناخورده شیر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کرد منقذ یتیر تیغ آبدار</p></div>
<div class="m2"><p>بهر فرق اکبر نسرین عذار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ایستاده شمر بهر کینه‌ات</p></div>
<div class="m2"><p>تا زند چکمه به روی سینه‌ات</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>