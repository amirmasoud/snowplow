---
title: >-
    شمارهٔ ۴۰ - وقایع بعد از قتل
---
# شمارهٔ ۴۰ - وقایع بعد از قتل

<div class="b" id="bn1"><div class="m1"><p>دوش کردم خالی از اغیار و یار</p></div>
<div class="m2"><p>گوشه‌ای را در تفکر اختیار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد سفر وهم را چابک عنان</p></div>
<div class="m2"><p>ساخت اندر وادی عبرت مکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرصه‌ای دیدم وسیع و هولناک</p></div>
<div class="m2"><p>رهروان بسیار اندر وی هلاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو من افتاده در هر گوشه‌ای</p></div>
<div class="m2"><p>نیمره وامانده پی توشه‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش حسرت به جان افروختم</p></div>
<div class="m2"><p>عودسان در مجمر غم سوختم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کانیزه وحشت‌زده چون طی شود</p></div>
<div class="m2"><p>ره شناسی خضر را هم کی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همرهان رخش جدایی تاختند</p></div>
<div class="m2"><p>بیرق دوری ز من افراختند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من که بی‌برک و نوایم چون کنم</p></div>
<div class="m2"><p>بی‌رفیق و آشنایم چون کنم؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمان فکر محالی داشتم</p></div>
<div class="m2"><p>در دل غافل خیالی داشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عاقبت گردید بعد از چند و چون</p></div>
<div class="m2"><p>در رهم روشن ضمیری رهنمون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفت ای وامانده از بار گناه</p></div>
<div class="m2"><p>لطف حق باشد گنه را عذر خواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر ز عصیان اشکباری توبه کن</p></div>
<div class="m2"><p>در معاصی بی‌قراری توبه کن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>توبه‌دل را صافی و روشن کند</p></div>
<div class="m2"><p>گلخن تن وادی ایمن کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توبه وحشی را دلیل راه شد</p></div>
<div class="m2"><p>تا ز «مرجون لامرالله» شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پاسخ او را جوابی ساختم</p></div>
<div class="m2"><p>پیشکش جان در برش انداختم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کی در این وادی دلیل راه من</p></div>
<div class="m2"><p>در شب تاریک رویت ماه من</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حال کز الطاف حی کردگار</p></div>
<div class="m2"><p>این چنین کردی مرا امیدوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صرف کردم در گناه اوقات را</p></div>
<div class="m2"><p>کو تلافی غفلت مافات را</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون که ناچار است در پایان کار</p></div>
<div class="m2"><p>سوی محشر خلق عالم را گذار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اندر آن آشفته حالی چون کنم</p></div>
<div class="m2"><p>آن زمان با دست خالی چون کنم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پس مرا کن در هدایت رهبری</p></div>
<div class="m2"><p>کز دری شاید برون آرم سری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تحفه‌ای خواهم به خاطرخواه حق</p></div>
<div class="m2"><p>بلکه افتد قابل درگاه حق</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گفت و جاری کرد اشک از هر دو عین</p></div>
<div class="m2"><p>گریه کن برشاه مظلومان حسین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>می‌کند زینب روایت اینچنین</p></div>
<div class="m2"><p>بعد قتل سبط خیرالمرسلین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آتش کین شامیان افروختند</p></div>
<div class="m2"><p>خیمه سبط نبی را سوختند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اهل‌بیت آن شه بی‌غمگسار</p></div>
<div class="m2"><p>هر یکی کردند به رستمی فرار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>من ز بهر جمع ایشان تاختم</p></div>
<div class="m2"><p>زین طرف بر آن طرف پرداختم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جمع کردم جمله را از هر کنار</p></div>
<div class="m2"><p>یک به یک بنمودم ایشان را شمار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیدم از آن کودکان نبود دو تن</p></div>
<div class="m2"><p>خواهرم کلثوم را با صد محن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خواندم و گفتم که ای چون من غریب</p></div>
<div class="m2"><p>بی‌کس و بی‌یاور و حسرت نصیب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از وصایای حسینم یاد کن</p></div>
<div class="m2"><p>اندر این صحرا مرا امداد کن</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گوئیا این کودکان بردند راه</p></div>
<div class="m2"><p>با دل بریان به سوی قتلگاه</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خیز سوی قتلگاه آریم رو</p></div>
<div class="m2"><p>تا کنیم این کودکان را جستجو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چشم گریان هر دو سر کردیم راه</p></div>
<div class="m2"><p>می‌نبد ز ایشان نشان در قتلگاه</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گفتم ای خواهر چنین دارم گمان</p></div>
<div class="m2"><p>تشنه بودند آن طفل ناتوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کرده‌اند از تشنگی قطع حیات</p></div>
<div class="m2"><p>رفته‌اند ایشان سوی شط فرات</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سوی شبط کردیم رو با صد شتاب</p></div>
<div class="m2"><p>می‌نبودند آن دو طفل دل کباب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>پای را از سر دگر نشناختیم</p></div>
<div class="m2"><p>هر دو بی‌کس در بیابان تاختیم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>می‌نمودم من به حال مستمند</p></div>
<div class="m2"><p>اندر آن صحرا صدای خود بلند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کای یتیمان برادر چون شدید؟</p></div>
<div class="m2"><p>ای دو طفل نازپرور چون شدید؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا به گودالی رسیدم از قضا</p></div>
<div class="m2"><p>هر دو را دیدم که دور از اقربا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>فارغ از غم گوشه‌ای بگزیده‌اند</p></div>
<div class="m2"><p>دستها در گردن و خوابیده‌اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چشمشان در کاسه سرکرده جا</p></div>
<div class="m2"><p>هر دو را یاقوت لب چون کهربا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بر سر خود خاک حسرت ریخته</p></div>
<div class="m2"><p>اشگشان با خاک حسرت بیخته</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سوختن لحنی به حال زارشان</p></div>
<div class="m2"><p>دست بردم تا کنم بیدارشان</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با احل دیدم که پیمان داده‌اند</p></div>
<div class="m2"><p>هر دو از تاب عطش جان داده‌اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>در حرم افشا چو این آوازه شد</p></div>
<div class="m2"><p>زین مصیبت داغ ایشان تازه شد</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>رشته بی‌طاقتی بگسیخته</p></div>
<div class="m2"><p>مو پریشان خاک بر سر ریخته</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اهل شام از این خبر گریان شدند</p></div>
<div class="m2"><p>سنگ دلها زین ستم بریان شدند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>سوی ابن سعد بنهادند روی</p></div>
<div class="m2"><p>کای پلید رو سیاه زشت خو</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ای لعین این قوم را تقصیر چیست</p></div>
<div class="m2"><p>این همه بی‌باکی و تذویر چیست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>حال کای ظالم حسین را کشته‌ای</p></div>
<div class="m2"><p>پیکرش در خاک و خون آغشته‌ای</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>رخصتی ده تا رسانیم این زمان</p></div>
<div class="m2"><p>قطره آبی برلب لب تشنگان</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اذن بگرفتند و با روی سیاه</p></div>
<div class="m2"><p>آب آوردند سوی خیمه‌گاه</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>با خبر گشتند چون اهل حرم</p></div>
<div class="m2"><p>پای تا سر سوختند از این ستم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>یعنی ای بی‌رحم قوم ناقبول</p></div>
<div class="m2"><p>تشنه‌لب کشتید فرزند بتول</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>درد ما را مرگ می‌باشد علاج</p></div>
<div class="m2"><p>آب دیگر نیست ما را احتیاج</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>