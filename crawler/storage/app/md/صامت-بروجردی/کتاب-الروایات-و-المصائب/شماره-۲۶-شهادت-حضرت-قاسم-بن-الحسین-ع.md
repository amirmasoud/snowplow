---
title: >-
    شمارهٔ ۲۶ - شهادت حضرت قاسم بن الحسین(ع)
---
# شمارهٔ ۲۶ - شهادت حضرت قاسم بن الحسین(ع)

<div class="b" id="bn1"><div class="m1"><p>گفت راوی چون به دشت کربلا</p></div>
<div class="m2"><p>شد به میدان قاسم نو کدخدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر طرف کاورد رو با تیغ تیز</p></div>
<div class="m2"><p>بر عدو دادی نشان رستخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو شیری کو رهد از سلسله</p></div>
<div class="m2"><p>کرد کاخ کفر را پر زلزله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناگهان عمر بن سعد بد سیر</p></div>
<div class="m2"><p>بست از دنیا و از عقبی نظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سوی آن شهزاده بی‌کس شتافت</p></div>
<div class="m2"><p>فرقش از شمشیر تا ابرو شکافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از قفا زد شیبه بر پشتش سنان</p></div>
<div class="m2"><p>شد گذار از سینه آن نوجوان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسل ناپاک سعید نا سعید</p></div>
<div class="m2"><p>قلب محزون وی آز خنجر درید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از غضب انداخت یحیی بن وهب</p></div>
<div class="m2"><p>نیزه بر پهلوی آن عالی نسب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر زمان با خویش کردندی خطاب</p></div>
<div class="m2"><p>خارجی بچه بود قتلش ثواب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر آن افسرده دل پر ز خون</p></div>
<div class="m2"><p>بر زمین شد از سر زین واژگون</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کرد با حال حزین آن مستمند</p></div>
<div class="m2"><p>صوت «یا عماه ادرکنی» بلند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از صدای مستغاث آن وحید</p></div>
<div class="m2"><p>شاه مظلومان به بالینش رسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دید قاسم را چو در خون بسملش</p></div>
<div class="m2"><p>حمله‌ور گردید سوی قاتلش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از دم شمشر کرد آن با وفا</p></div>
<div class="m2"><p>قاتلش را دست از مرفق جدا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از برای یاری آن دین تباه</p></div>
<div class="m2"><p>جیش کوفی حلقه زد بر دور شاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>روی نعش قاسم والاتبار</p></div>
<div class="m2"><p>جنگ شد مغلوبه اندر گیر و دار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عاقبت گردید زان جنگ و جدال</p></div>
<div class="m2"><p>پیکر مجروح قاسم پایمال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صبح عمر کوته وی شام شد</p></div>
<div class="m2"><p>از جهان آن نوجوان ناکام شد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>