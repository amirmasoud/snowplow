---
title: >-
    شمارهٔ ۱۸ - نامه نوشتن فاطمه صغرا به پدر
---
# شمارهٔ ۱۸ - نامه نوشتن فاطمه صغرا به پدر

<div class="b" id="bn1"><div class="m1"><p>روایت است که چون از وطن نمود سفر</p></div>
<div class="m2"><p>به سوی کرببلا آن امام تشنه جگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علیله دخترکی در مدینه فاطمه نام</p></div>
<div class="m2"><p>به جای ماند از آن شهریار عرش مقام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه با تن تبدار و اشک و ناله و آه</p></div>
<div class="m2"><p>به راه کرببلا مانده بود چشم به راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به فکر اینکه رسد از پدر به او خبری</p></div>
<div class="m2"><p>مدام داشت مهیا اساس نوحه‌گری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیر آه جهانسوز اهل راز نداشت</p></div>
<div class="m2"><p>به چاره دل افسرده دلنواز نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته بود شب و روز با هجوم بلا</p></div>
<div class="m2"><p>در انتظار پدر چشم سوی کرببلا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشته بود یکی نامه آن علیله زار</p></div>
<div class="m2"><p>برای خسرو لب تشنه با تن تب‌دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عریضه ورقش پرده دل غمناک</p></div>
<div class="m2"><p>مدادش از اثر خون دیده نمناک</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کتابتی کلماتش همه شرر انگیز</p></div>
<div class="m2"><p>عبارتش همه پرحسرت و قیامت خیز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اراده داشت که آن نامه را بهانه کند</p></div>
<div class="m2"><p>به کوفه نزد پدر قاصدی روانه کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حکایت دل پر خون خویش سرتاسر</p></div>
<div class="m2"><p>نوشته بود در آن نامه از برای پدر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گرفت یک عرب نامه را از آن دلخون</p></div>
<div class="m2"><p>به عزم کرببلا گشت از مدینه برون</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نمود طی ره مقصود روز و شب ز وفا</p></div>
<div class="m2"><p>به دشت ماریه آمد به ظهر عاشورا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ساعتی که ز بیداد خلق کوفه و شام</p></div>
<div class="m2"><p>شهید گشته محبان شاه تشنه تمام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عزیز فاطمه لب‌تشنه و غریب و وحید</p></div>
<div class="m2"><p>ستاده یکه و تنها به نزد جیش یزید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عرب دو دست ادب را به سینه نزد امام</p></div>
<div class="m2"><p>نهاد و کرد بدان شاه کم سپاه سلام</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شه شهید دم عیسوی ز هم بگشود</p></div>
<div class="m2"><p>به لطف خاص جواب سلام او فرمود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به گریه گفت که ای قاصد خجسته پیام</p></div>
<div class="m2"><p>تو کیستی که نمودی بدین غریب سلام</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز چهره تو هویدا بود بوجه حسن</p></div>
<div class="m2"><p>حدیث محنت و اماندگان اهل وطن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نمود عرض که ای مظهر صفات خدا</p></div>
<div class="m2"><p>مراست نامه‌ای از نزد دختر صغرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرفت از عرب آن شاه بی‌سپاه و حشم</p></div>
<div class="m2"><p>کتاب و دل پرخون روانه شد به حرم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به دور خویش زنان را تمام جمع نمود</p></div>
<div class="m2"><p>ز روی نامه صغری ز مهر مهر گشود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نوشته بود در آن نامه کای جناب پدر</p></div>
<div class="m2"><p>نموده‌ای ز چه از این علیله قطع نظر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نما مرا ز وفا سربلند نزد کسان</p></div>
<div class="m2"><p>سلام من بعموها و عمه‌ها برسان</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگو به حضرت عباس کای عموی رشید</p></div>
<div class="m2"><p>ز دوری تو ز دنیا بریده‌ام امید</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فدایی سر و جان تو باد جان و سرم</p></div>
<div class="m2"><p>عمو به کرببلا کن حمایت پدرم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر به کرببلا گشته قاسم داماد</p></div>
<div class="m2"><p>عروس را بده از جای من مبارک باد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زند همیشه مرا مرغ روح در تن پر</p></div>
<div class="m2"><p>ز حسرت گل روی برادرم اکبر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بگو به اکبر یوسف جمال مه سیما</p></div>
<div class="m2"><p>بیا مرا ز مدینه ببر به کرب و بلا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مرا به سر هوس دیدن سکینه بود</p></div>
<div class="m2"><p>ز زندگی دل من سیر در مدینه بود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کنم به دامن او جای تا بدون تعب</p></div>
<div class="m2"><p>بیا مرا برسان پیش عمه‌ام زینب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از این علیله هجران کشیده بیمار</p></div>
<div class="m2"><p>رسان سلام به نزدیک عابد تب دار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شها (بصامت) حسرت نصیب کن نظری</p></div>
<div class="m2"><p>که در عزای تو دارد همیشه نوحه گری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گناهکارم و غیر از تو عذرخواهی نیست</p></div>
<div class="m2"><p>مرا به روز قیامت دگر پناهی نیست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به حق اکبر در خون طپیده بی‌سر</p></div>
<div class="m2"><p>مکن ز جانب این روسیاه قطع نظر</p></div></div>