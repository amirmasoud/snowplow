---
title: >-
    شمارهٔ ۷ - آمدن بشیر از جانب یوسف به خدمت یعقوب
---
# شمارهٔ ۷ - آمدن بشیر از جانب یوسف به خدمت یعقوب

<div class="b" id="bn1"><div class="m1"><p>روایتی شده از راویان بسی مطلوب</p></div>
<div class="m2"><p>برای علت دوری یوسف از یعقوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که چون ز مصلحت کردکار بی‌همتا</p></div>
<div class="m2"><p>نمود مادر یوسف و وداع دار فنا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این مقدمه یعقوب شد بسی دلگیر</p></div>
<div class="m2"><p>یکی کنیز خرید ا زبرای دادن شیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شیر دادن یوسف گذشت چون ایام</p></div>
<div class="m2"><p>کنیز داشت یکی کودک و بشیرش نام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز در رسید یکی روز پیر کنعانی</p></div>
<div class="m2"><p>برای دیدن یوسف ز لطف پنهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفته بود در آغوش خود کنیز بشیر</p></div>
<div class="m2"><p>نموده است تغافل به یوسفش از شیر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته بود در آغوش خود کنیز بشیر</p></div>
<div class="m2"><p>نموده است تغافل به یوسفش از شیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الم به سینه یعقوب بس شرر افروخت</p></div>
<div class="m2"><p>گرفت از بر مادر بشیر را بفروخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کنیز گشت از این حال مضطرب احوال</p></div>
<div class="m2"><p>نمود روی تضرع به قادر متعال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>که یا رب از من و حال دلم گواهی تو</p></div>
<div class="m2"><p>به بی‌کسان دل افسرده داد خواهی تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ببین فکند جدایی چنان پیمبر تو</p></div>
<div class="m2"><p>میان مادر و فرزند در برابر تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو دید زاری آن زن مهیمن علام</p></div>
<div class="m2"><p>بدان ضعیفه همانا شد این چنین الهام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ببین چگونه تلاقی از این عمل سازم</p></div>
<div class="m2"><p>میان باب و پسر هم جدایی اندازم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چنانکه تا ز بشیرت به تو خبر نرسد</p></div>
<div class="m2"><p>خبر ز یوسف گمگشته بر پدر نرسد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غرض که گشت چهل سال یوسف از کنعان</p></div>
<div class="m2"><p>جدا ز نزد پدر بهر آن زن گریان</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چنان ز وصل پسر گشت این در نومید</p></div>
<div class="m2"><p>که هر دو دیده وی شد ز انتظار سفید</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مشیت ازلی این چنین گرفت قرار</p></div>
<div class="m2"><p>که آب لطف به آتش فشاند دیگر بار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ندا رسید به یوسف ز خالق دوالمن</p></div>
<div class="m2"><p>که نزد باب گرامی فرست پیراهن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که از فراق تو آن پیر نا صبور شده</p></div>
<div class="m2"><p>سفید گشته دو چشمش چو از تو دور شده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همان بشیر به فرمان کردگار جهان</p></div>
<div class="m2"><p>گرفت پیرهن و کرد روی سوی کنعان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>رسید بر دروازه دید پیر زنی</p></div>
<div class="m2"><p>نه پیرزن که دو مشت استخوان به یک کفنی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نشان خانه یعقوب را از او پرسید</p></div>
<div class="m2"><p>کنیز بوی محبت از آن نشانه شنید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سئوال کرد چه خواهی ز خانه یعقوب</p></div>
<div class="m2"><p>جواب داد که دارم ز یوسفش مکتوب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از این جواب دل پیر زن به سینه طپید</p></div>
<div class="m2"><p>به گریه گفت که یا رب چه شد نشان امید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به وعده‌ای که نمودی عجب وفا کردی</p></div>
<div class="m2"><p>مرا ز محنت فرزند خود رها کردی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بشیر یافت که آن پیر زن چه می‌گوید</p></div>
<div class="m2"><p>کدام راه از این اضطراب می‌پوید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بگفت غم مخور ای زن که من بشیر توام</p></div>
<div class="m2"><p>زمان وعده بسر رفت و دستگیر توام</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بشیر را ز محبت کشید در آغوش</p></div>
<div class="m2"><p>ز هوش رفت و ز فریاد و ناله شد خاموش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بشیر در بر یعقوب برد پیراهن</p></div>
<div class="m2"><p>نمود دیده ز دیدار پیرهن روشن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یکی بشیر دگر در جهان خبر دارم</p></div>
<div class="m2"><p>که از رسالت او شعله بر جگر دارم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همان بشیر که آورد در مدینه خبر</p></div>
<div class="m2"><p>ز حال اهل و عیال حسین تشنه جگر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روایت است که آمد بشیر چون از راه</p></div>
<div class="m2"><p>پیاده شد به سر تربت رسول الله</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به گریه گفت که یا مصطفی سلام علیک</p></div>
<div class="m2"><p>به رتبه ختم همه انبیا سلام علیک</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خجل ز روی تو هستم اگر به خدمت تو</p></div>
<div class="m2"><p>خبر دهم ز حسین و حریم و عترت تو</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ولی رسول چو از اهل بیت اطهارم</p></div>
<div class="m2"><p>به عرض واقعه در خدمت تو ناچارم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به دشت کرببلا از جفای ابن زیاد</p></div>
<div class="m2"><p>به نزد آب حسین تو تشنه لب جا نداد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تمام اهل و عیالش اسیر و خوار شدند</p></div>
<div class="m2"><p>سر برهنه به پشت شیر سوار شدند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سری که داشت به دوش مبارکتو مکان</p></div>
<div class="m2"><p>گهی به خاک تنور و گهی به نوک سنان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سپاه شامی و کوفی سوار و بر مرکب</p></div>
<div class="m2"><p>پیاده عباد بیمار با تن پر تب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز جای عترت زارت خبر دهم یا نه</p></div>
<div class="m2"><p>گهی به گوشه زندان گهی به ویرانه</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به ناسزا دل زینب یکی کباب نمود</p></div>
<div class="m2"><p>یکی به راس حین خارجی خطاب نمود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>برای بردن بزم یزید بی‌پروا</p></div>
<div class="m2"><p>به یک طناب ببستند شصت و شش زن را</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نشسته بود نصاری به روی کرسی زر</p></div>
<div class="m2"><p>ستاده بر سر پا عابدین بی‌یاور</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ظهیر مسخره در بزم شرب نزد یزید</p></div>
<div class="m2"><p>ز دختران عزیزت کنیز می‌طلبید</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>بریده باد زبانم یزید خانه خراب</p></div>
<div class="m2"><p>به نزد راس حسین تو ریخت درد شراب</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شکسته باد دهانم که آن جهود عنود</p></div>
<div class="m2"><p>لب حسین ز چوب جفا نمود کبود</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بزرگوار دایا به حق پیغمبر(ص)</p></div>
<div class="m2"><p>ز جرم (صامت) و عصیان شیعیان بگذار</p></div></div>