---
title: >-
    شمارهٔ ۳۹ - قصه گرگ با رسول خدا(ص)
---
# شمارهٔ ۳۹ - قصه گرگ با رسول خدا(ص)

<div class="b" id="bn1"><div class="m1"><p>کرد رحلت چون به حکم دادگر</p></div>
<div class="m2"><p>مادر نیک اختر خیرالبشر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی پرستار و دل افکار و الیم</p></div>
<div class="m2"><p>بود آن در گردان قیمت یتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جستجو کردند در حی عرب</p></div>
<div class="m2"><p>دایه از بهر رسول الله طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرد در آخر ز نیکو گوهری</p></div>
<div class="m2"><p>کوکب بخت حلیمه یاوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن همای اوج افلاک جلال</p></div>
<div class="m2"><p>بر سرش باز شرف بگشود بال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن گرامی گوهر بحر صفا</p></div>
<div class="m2"><p>چند گاهی کرد چون نشو و نما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با پسرهای حلیمه از وداد</p></div>
<div class="m2"><p>بر سر وی خواهش صحرا فتاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزها با گوسفندان آن جناب</p></div>
<div class="m2"><p>می‌نمودی جانب صحرا شتاب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تایکی روز از قضای ذوالمنن</p></div>
<div class="m2"><p>ماند اندر خانه آن فخر ز من</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کرد همراه برادر خواندگان</p></div>
<div class="m2"><p>گوسفندان را سوی صحرا روان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همره گله چون آن سرور نبود</p></div>
<div class="m2"><p>گرگی آمد گوسفندی را ربود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بازگردیدند وقت شامگاه</p></div>
<div class="m2"><p>رو به سوی خانه چون با اشک و آه</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بر احمد امین ذوالجلال</p></div>
<div class="m2"><p>لب گشودند از برای عرض حال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روز دیگر سرور کل امم</p></div>
<div class="m2"><p>ساخت رنجه جانب صحرا قدم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>جان عالم در بر جان آفرین</p></div>
<div class="m2"><p>جبهه عالم نهاد او بر زمین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>قبله حاجات ارباب وفا</p></div>
<div class="m2"><p>بود در حال تضرع با خدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کز دعای آن رسول ارجمند</p></div>
<div class="m2"><p>گرگ پیدا گشت با آن گوسفند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>عرض کرد ای خسرو دین و ملل</p></div>
<div class="m2"><p>زد به نادانی ز من سر این عمل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چون ربودم روز پیش این میش را</p></div>
<div class="m2"><p>تا نمایم سد جوع خویش را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هاتفی در گوش من داد این ندا</p></div>
<div class="m2"><p>کای تجاوز کرده از راه خدا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هست از خنم رسل این گوسفند</p></div>
<div class="m2"><p>بر تو می‌باشد حرام ای مستمند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در بر خود همچو جان پروردمش</p></div>
<div class="m2"><p>سر قدم کرده برت آوردمش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یادم آمد باز اندر این مقام</p></div>
<div class="m2"><p>گرگ‌های بی‌حیای شهر شام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آن زمان کز یوسف آل عبا</p></div>
<div class="m2"><p>سر جدا می‌کرد شمر بی‌حیا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زینب غم‌پرور بی‌صبر و تاب</p></div>
<div class="m2"><p>بود در نظاره با قلب کباب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>لحظه‌ای در نزد شمر بد سیر</p></div>
<div class="m2"><p>کرد دامن را ز اشک دیده تر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پس به نزد ابن سعد بی‌حیا</p></div>
<div class="m2"><p>با تضرع کرد روی التجا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون ز شمر و ابن سعد روسیاه</p></div>
<div class="m2"><p>گشت محروم آن زمان با اشک و آه</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کرد رو بر شامیان و کوفیان</p></div>
<div class="m2"><p>دختر زهرا به چشم خون فشان</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کز شما ای لشکر بیرون ز دین</p></div>
<div class="m2"><p>یک مسلمان نیست در این سرزمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در جواب آن همای اوج غم</p></div>
<div class="m2"><p>لب فرو بستند از لا و نعم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا ز تن ببرید پیش چشم وی</p></div>
<div class="m2"><p>شمر راس شاه دین را زد به نی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر سر نی راس شاه تشنه لب</p></div>
<div class="m2"><p>در تکلم شد به پیش بی‌ادب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کی لعین اینسان که از تیغ جفا</p></div>
<div class="m2"><p>ساختی از پیکر من سر جدا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>می نه بینی خیر از جان و جهان</p></div>
<div class="m2"><p>لحم تو گردد جدا از استخوان</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>از غضب بنهاد آن برگشته دین</p></div>
<div class="m2"><p>آن سر ببریده را اندر زمین</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تازیانه برگرفت و از عتاب</p></div>
<div class="m2"><p>زد به راس نور چشم بوتراب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>