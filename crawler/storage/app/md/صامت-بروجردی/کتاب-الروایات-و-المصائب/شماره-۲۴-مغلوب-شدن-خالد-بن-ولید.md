---
title: >-
    شمارهٔ ۲۴ - مغلوب شدن خالد بن ولید
---
# شمارهٔ ۲۴ - مغلوب شدن خالد بن ولید

<div class="b" id="bn1"><div class="m1"><p>روایتست که بوبکر دون چو از دغلی</p></div>
<div class="m2"><p>نمود غصب خلافت پس از نبی ز علی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای تقویت کار آن پلید شریر</p></div>
<div class="m2"><p>نمود زاده خطاب این چنین تقریر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که تا علی به جهان زنده است نزد عوام</p></div>
<div class="m2"><p>به ما خلافت ناحق نباید استحکام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نباید آن که علی را گذاشت مامونش</p></div>
<div class="m2"><p>به اجتماع بباید که ریختن خونش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قرار داد ابوبکر زشت حیلت ساز</p></div>
<div class="m2"><p>که وقت صبح بدادم چو من سلام نماز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنند شیر خدا را به وقت سجده شهید</p></div>
<div class="m2"><p>زند به گردن او تیغ خالد بن ولید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وقت صبح که اندر نماز شد مشغول</p></div>
<div class="m2"><p>ز فعل خویش پشیمان شد آن مظلوم جهول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا که گشت ابوبکر بی‌وفا خائف</p></div>
<div class="m2"><p>که گر علی شود از کارهای او واقف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود که بازوی سرپنجه یداللهی</p></div>
<div class="m2"><p>کند خراب جهان را ز ماه تا ماهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نداده بود سلام نماز آن غدار</p></div>
<div class="m2"><p>بسوی خالد بی‌آبرو نمود اخیار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>که زینهار مشو خالدا به خود مغرور</p></div>
<div class="m2"><p>مکن اراده به امری که کردمت مامور</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس از نماز علی کرد رو به ابن ولید</p></div>
<div class="m2"><p>زوی حقیقت این امر و نهی را پرسید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جواب داد که مامور گشته بودم من</p></div>
<div class="m2"><p>جدا کنم سر مهر افسر تو را از تن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نگشته بود ابوبکر گر مرا ناهی</p></div>
<div class="m2"><p>نمی‌نمودی از کشتن تو کوتاهی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد از روان علی زین سخن بلند خروش</p></div>
<div class="m2"><p>حمیت اسداللهی آمد اندر جوش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زهم گشود دو انگشت خویش شیر خدا</p></div>
<div class="m2"><p>به پشت گردن خالد نهاد او ز قفا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چنان گشود گلوگاه آن سک بی‌دین</p></div>
<div class="m2"><p>که همچو سکه منقوش گشت نقش زمین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شکوه حیدری آن سان به دان مخنت کرد</p></div>
<div class="m2"><p>که جامه را به تن نحسن خود را ملوث کرد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو سایه در قدم آن شه سپهر جناب</p></div>
<div class="m2"><p>به التماس فتاددن هر یک از اصحاب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همه به داده وسودند دیده بر قدمش</p></div>
<div class="m2"><p>به حق تربت پاک رسول حق قسمش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>برای حرمت قبر رسول رب مجید</p></div>
<div class="m2"><p>گذشت از سر تقصیر خالد بن ولید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو کس به قتل علی در نماز شد عازم</p></div>
<div class="m2"><p>یکی‌ست خالد و آن دیگریست بن ملجم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نگشت خالد اگر از مراد خود دلشاد</p></div>
<div class="m2"><p>رسید نسل مرادی ز فعل خود به مراد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>برای سجده قد شیر حق چو خم گردید</p></div>
<div class="m2"><p>به قتل شه قد آن بی‌حیا علم گردید</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به قلب پاک نبی شعله بی‌دریغ افکند</p></div>
<div class="m2"><p>به فرق عم و دامادوی چو تیغ افکند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>علی ز ضربت شمشیر وی ز دست افتاد</p></div>
<div class="m2"><p>بر کن اول ارکان دین شکست افتاد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>برفت اوج فلک داد و شیون اصحاب</p></div>
<div class="m2"><p>گرفت موج شط خون به دامن محراب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ساکنان سما زین صدا گزند آمد</p></div>
<div class="m2"><p>ندای «قد قتل المرتضی» بلند آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سر برهنه دویدند با غم و شیون</p></div>
<div class="m2"><p>پی تفحص حال پدر حسین و حسن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو بر سر پدر خویش رهسپار شدند</p></div>
<div class="m2"><p>به درد بی‌پدری هر دو تن دچار شدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سرشک چشم حسین گشت غیرت عمان</p></div>
<div class="m2"><p>حسن گرفت سر باب خویش بر دامان</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به سوی خانه چو بردند نعش میر عرب</p></div>
<div class="m2"><p>کشید معجر بی‌طاقتی ز سر زینب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دو دیده را پی تسکین بی‌کسان واکرد</p></div>
<div class="m2"><p>ز گریه زینب و کلثوم را تسلی کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گشود طایر روح امام جن و بشر</p></div>
<div class="m2"><p>ز ملک جسم سوی شاخسار طوبی پر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دل حسین و حسن گشت از الم پر خون</p></div>
<div class="m2"><p>به احترام نمودند باب خود مدفون</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>فدای آن تن بی‌سر، که بود برنج و تعب</p></div>
<div class="m2"><p>به خاک کرببلا بی‌کفن سه روز و سه شب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کسی نبود که گیرد برای اوماتم</p></div>
<div class="m2"><p>و یا به روی جراحات وحی نهد مرهم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه مادری که کشد در عزای او معجر</p></div>
<div class="m2"><p>نه خواهری که تواند زند به سینه و سر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>نه همدمی که نماید فغان به ماتم او</p></div>
<div class="m2"><p>دمی نهد سر او را مهر بر زانو</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نبود بر سر نعشش معین و یار و حبیب</p></div>
<div class="m2"><p>که تابلند کند «الصلوه‌مات غریب»</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به جای دفن و کفن شد تنش ز سم ستور</p></div>
<div class="m2"><p>به خاک ماریه در زیر خاک و خون مستور</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بس است (صامت) از این بیشتر شتاب مکن</p></div>
<div class="m2"><p>از این قضیه دل خلق را کباب مکن</p></div></div>