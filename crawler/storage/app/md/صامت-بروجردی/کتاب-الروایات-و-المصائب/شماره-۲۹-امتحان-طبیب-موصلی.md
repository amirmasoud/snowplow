---
title: >-
    شمارهٔ ۲۹ - امتحان طبیب موصلی
---
# شمارهٔ ۲۹ - امتحان طبیب موصلی

<div class="b" id="bn1"><div class="m1"><p>گفت راوی در تمام عالمین</p></div>
<div class="m2"><p>چون خلافت شد مسلم بر حسین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود در موصل کی دانا طبیب</p></div>
<div class="m2"><p>با یزید بن معاویه حبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نی همین در خدمتش کردی قیام</p></div>
<div class="m2"><p>بلکه می‌‌دانست آن سگ را امام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت با وی مومنی از شیعیان</p></div>
<div class="m2"><p>بگذر از این راه باطل ای فلان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر سعادت خواهی اندر عالمین</p></div>
<div class="m2"><p>قبله خود کن تو شاه دین حسین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آنکه اندر راه حی لایزال</p></div>
<div class="m2"><p>ساخته وقف یتیمان جان و مال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر حقیقت را به سر داری هوس</p></div>
<div class="m2"><p>در حسین این رتبه را می‌جوی و بس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرد اندر دل طبیب نکته دان</p></div>
<div class="m2"><p>زین سخن با خویش قصد امتحان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از قضا بیوه زنی همسایه داشت</p></div>
<div class="m2"><p>از وجودش بر سر خود سایه داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ناگهان آن بیوه زن بیمار شد</p></div>
<div class="m2"><p>حال طفلش بی‌پدر افکار شد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد روانه کودک دور از شکیب</p></div>
<div class="m2"><p>حال مادر گفت نزد آن طبیب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفت ای کودک اگر جویی علاج</p></div>
<div class="m2"><p>یک جگر از اسب باشد احتیاج</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گفت کودک ای طبیب پرهنر</p></div>
<div class="m2"><p>من ندارم اسب تا آرم جگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گفت با وی آن طبیب موصلی</p></div>
<div class="m2"><p>رو به درگاه حسین بن علی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد شتابان کودک افسرده حال</p></div>
<div class="m2"><p>خدمت آن معدن جود و نوال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زاده زهرا گرفت اندر برش</p></div>
<div class="m2"><p>دست دلجویی کشید اندر سرش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اشک چشم را ز رحمت پاک کرد</p></div>
<div class="m2"><p>جستجوی حال آن غمناک کرد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون تسلی داد اشک و آه او</p></div>
<div class="m2"><p>کشت اسبی را به خاطرخواه او</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>داد پس لخت جگر بر دست او</p></div>
<div class="m2"><p>طفل در نزد طبیب آورد رو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کرد از رنک فرس از وی سئوال</p></div>
<div class="m2"><p>پنج نوبت آن طبیب بی‌مهال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت رنگ اسب اینسان خوب نیست</p></div>
<div class="m2"><p>بهر درد مادرت مطلوب نیست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نج نوبت شاه گردون اقتدار</p></div>
<div class="m2"><p>پنج اسب از خویش کشت آن پنج بار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رحم کردن بر یتیمان را حسین</p></div>
<div class="m2"><p>داشت اندر ذمت خد فرض عین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دید چون این جودو احسان و اطیب</p></div>
<div class="m2"><p>شد محب آن شهنشاه غریب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای حمیت پیشگان و شیعیان</p></div>
<div class="m2"><p>گر بود انصاف در خلق جهان</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دور از رحم است ای اهل کمال</p></div>
<div class="m2"><p>اینچنین سلطان با جود و خصال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شمر روی دخترش نیلی کند</p></div>
<div class="m2"><p>نیلگون رخسارش از سیلی کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جمله را پای پیاده روز و شب</p></div>
<div class="m2"><p>در بیابان‌ها دواند از غضب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عترت آن شاه بی‌مثل و نظیر</p></div>
<div class="m2"><p>برد اندر کوفه چون شمر شریر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>داد اندر گوشه زندان مکان</p></div>
<div class="m2"><p>آن حریم سرور کون و مکان</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سر برهنه دل گرسنه جان کباب</p></div>
<div class="m2"><p>پوست افکنده بدنشان آفتاب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جای دست مرحمت‌های پدر</p></div>
<div class="m2"><p>سنگ و چوب کوفی رشامی بسر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بر سر آن بی‌کسان در روزگار</p></div>
<div class="m2"><p>جز کنیزان کس نمی‌کردی گذار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مردم کوفی شب اندر خانه‌ها</p></div>
<div class="m2"><p>کودکان چون گنج در ویرانه‌ها</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روز و شب از چشم ایشان رفته خواب</p></div>
<div class="m2"><p>شب ز سرما روزها از و آفتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بلکه دادندی تصدق کوفیان</p></div>
<div class="m2"><p>بر یتیمان حسین خرما و نان</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سویشان کلثوم چون کردی نظر</p></div>
<div class="m2"><p>سوختی او را از این محنت جگر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>می‌گرفت آن نان ز دست کودکان</p></div>
<div class="m2"><p>با غصب می‌گفت با آن ناکسان</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>کای گروه سست عهد بی‌وفا</p></div>
<div class="m2"><p>از خدا شرمی ز پیغمبر حیا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>کوفیان مارا تصدق کی رواست</p></div>
<div class="m2"><p>کی خدا خوشنود و پیغمبر رضاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ما که در این شهر خوار و مضطریم</p></div>
<div class="m2"><p>آل یاسین عترت پیغمبریم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>روزگاری خانمانی داشتیم</p></div>
<div class="m2"><p>از بزرگی ما نشانی داشتیم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>