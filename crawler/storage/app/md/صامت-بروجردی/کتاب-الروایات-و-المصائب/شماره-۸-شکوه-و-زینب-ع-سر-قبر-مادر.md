---
title: >-
    شمارهٔ ۸ - شکوه و زینب (ع) سر قبر مادر
---
# شمارهٔ ۸ - شکوه و زینب (ع) سر قبر مادر

<div class="b" id="bn1"><div class="m1"><p>روایت است که چون عترت رسول‌الله</p></div>
<div class="m2"><p>سوی مدینه رسیدند با خروش ز راه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمیده زینب بی‌غمگسار گشت روان</p></div>
<div class="m2"><p>میان روضه مادر به ناله و افغان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلام کرد به حسرت فکند سر در پیش</p></div>
<div class="m2"><p>زبان حال به مادر بگفت با دل ریش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که ای ستمکش ایام چشم تو روشن</p></div>
<div class="m2"><p>که زینبت ز سفر آمده است سوی وطن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری بر آر ز خاک و به پرس احوالم</p></div>
<div class="m2"><p>نظاره کن که چسان گشته است اقبالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز من بپرس که زینب چه شد برادر تو</p></div>
<div class="m2"><p>ز داغ کیست که گشته سیاه معجر تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز کربلا تو چرا بی‌برادر آمده‌ای</p></div>
<div class="m2"><p>چنین شکسته دل و خاک بر سر آمده‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>برم ز کوفه و یا کربلا به نزد تو نام</p></div>
<div class="m2"><p>و یا ز شام و یزید لعین بدفرجام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کربلا ز ستم سوختند خانه ما</p></div>
<div class="m2"><p>بباد داد فلک خاک آشیانه ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا به گوشه زندان همین نه ماوا داد</p></div>
<div class="m2"><p>به کوفه حکم به قتلم نمود ابن زیاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>میان کوفه ندیدی چسان ز آتش دل</p></div>
<div class="m2"><p>زدم ز غصه سر خد به چوبه محمل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>که شد ز خون سرم روی و موی من رنگین</p></div>
<div class="m2"><p>روانه شد ز سرم همچو سیل خون به زمین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز کوفه تا به سوی شام در برابر من</p></div>
<div class="m2"><p>به پیش محمل من بد سر برادر من</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شدم چو وارد اشم خراب ای مادر</p></div>
<div class="m2"><p>خرابه منزل ما بود و خاک ره بستر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی که مونس و غمخوار و همدم ما بود</p></div>
<div class="m2"><p>مدام سنگ و نی و چوب سخت اعدا بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تمام کوچه و بازرا شام آئین بست</p></div>
<div class="m2"><p>یزید دون به سر تخت زرنگار نشست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به بزم عام طلب کرد آن لعین غیور</p></div>
<div class="m2"><p>سر برهنه من و اهل بیت را به حضور</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چنان نمود جفای یزید مدهوشم</p></div>
<div class="m2"><p>که شد ز شمر و صفت کربلا فراموششم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به پیش دیده من آن ستمگر کونین</p></div>
<div class="m2"><p>بزد به چوب ستم بر لب و دهان حسین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گذشته زین همه یک سرخ مو به بزم یزید</p></div>
<div class="m2"><p>ز خاندان نبوت کنیز می‌طلبید</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بس است (صامت) افسرده زین عزا بگذر</p></div>
<div class="m2"><p>که از سرکش دو چشمت سیاه شد دفتر</p></div></div>