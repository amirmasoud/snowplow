---
title: >-
    شمارهٔ ۱۱ - خواب دیدن ناصرالدین شاه قاجار در نجف اشرف
---
# شمارهٔ ۱۱ - خواب دیدن ناصرالدین شاه قاجار در نجف اشرف

<div class="b" id="bn1"><div class="m1"><p>این شنیدستم که شاه جم حشم</p></div>
<div class="m2"><p>ناصرالدین شاه کیسخرو خدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گشت چون الطاف یزدان یاورش</p></div>
<div class="m2"><p>حلقه زد پیک سعادت بر درش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یعنی از ایران روان شد از وفا</p></div>
<div class="m2"><p>موکب میمون وی در کربلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر قدم بنمود با شوق و شعف</p></div>
<div class="m2"><p>بهر پابوس شهنشاه نجف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت چون شد ساکن آن سرزمین</p></div>
<div class="m2"><p>«ادخلوها بسلام آمنین»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>منزل آن شاه گردوه احتشام</p></div>
<div class="m2"><p>در نجف گردید در وادی‌السلام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لشکر آن خرو انجم سپاه</p></div>
<div class="m2"><p>بر فلک زد قبه‌های بارگاه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر آن وادی نمودند انتخاب</p></div>
<div class="m2"><p>گوشه‌ای را بهر اصطبل دواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کرد چون اردوی شاه از صدر و ذیل</p></div>
<div class="m2"><p>خیمه بر پا جوقه جوقه خیل خیل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیده حق بین وی شد کامیاب</p></div>
<div class="m2"><p>از زیارتگاه قبر بوتراب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شامگاهان شاه اورنک عجم</p></div>
<div class="m2"><p>بر سریر استراحت زد قدم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شد چو هنگام سحر ناگه ز خواب</p></div>
<div class="m2"><p>جست سلطان ناصرالدین از شتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لرزلرزان لعل لب را باز کرد</p></div>
<div class="m2"><p>محرمان راز را آواز کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>داد فرمان همراهان راه را</p></div>
<div class="m2"><p>بر کنند آن خیمه و خرگاه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رفت و در سمت دگر منزل نمود</p></div>
<div class="m2"><p>بهر راحت منزلی حاصل نمود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گشت خرم چون دل شه از ملال</p></div>
<div class="m2"><p>حاضران کردند از وی این سئوال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کای گدای در گهت خاقان چین</p></div>
<div class="m2"><p>کیقبا از ملک قدرت خوشه‌چین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای شهنشاه عجم اندر عرب</p></div>
<div class="m2"><p>چیست خوف و اضطرابت را سبب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از چه رو ای خسرو خسرو اساس</p></div>
<div class="m2"><p>ساختی از وادی ایمن هراس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای همای اوج عزت از بهشت</p></div>
<div class="m2"><p>ساختی منزل چرا اندر کنشت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گفت شد در خواب بر من جلوه‌گر</p></div>
<div class="m2"><p>مظهر ذات خدای دادگر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حبل ایمان عروه الوثقای دین</p></div>
<div class="m2"><p>مصدر ایمان امیرالمومنین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با غضب فرمود کز وادی السلام</p></div>
<div class="m2"><p>کرده‌ای بهر چه ترک احترام</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>غافلی از احترام این زمین</p></div>
<div class="m2"><p>کاین زمین باشد قبور مومنین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ساختی او را برای شیخ و شاب</p></div>
<div class="m2"><p>آخور انعام و اصطبل دواب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اندر اینجا بر کلیم آمد ندا</p></div>
<div class="m2"><p>از برای خلع نعلین از خدا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بی‌تامل ترک استکبار کن</p></div>
<div class="m2"><p>زود از این ارض مقدس بار کن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر چنین می‌باشد ای اهل یقین</p></div>
<div class="m2"><p>احترام خاک قبر مومنین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>پس چرا بر باد دادند اهل شام</p></div>
<div class="m2"><p>حرمت نعش امام تشنه کام</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نور چشم مصطفی از صدر زین</p></div>
<div class="m2"><p>جسم صد چاکش فتاد اندر زمین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برد غارت دشمن بدگوهرش</p></div>
<div class="m2"><p>هر لباسی داشت از پانا سرش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کار را بر جسم وی کردند تنک</p></div>
<div class="m2"><p>هریک از ضرب عصا و چوب و سنگ</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا شود آگاه از سر دلش</p></div>
<div class="m2"><p>زد سنان در پهلوی وی قاتلش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سجده گاه رحمه للعالمین</p></div>
<div class="m2"><p>رنگ شد با سنگ از خون جبین</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شد چنان پیکان به نافش کارگر</p></div>
<div class="m2"><p>کز کمرگاهش برون بنمود سر</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>روسیاهی دشمنی با حق نمود</p></div>
<div class="m2"><p>چون قمر با تیغ فرقش شق نمود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گشت افزون چو بلای پیکرش</p></div>
<div class="m2"><p>شمر دون آمد به خنجر برسرش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ساخت از نی راس آن بی‌اقربا</p></div>
<div class="m2"><p>از قفا با یازده ضربت جدا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا که گردد میزبان راس وی</p></div>
<div class="m2"><p>خولی بی‌دین سرش را زد به نی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر سر وی کار را کردند سخت</p></div>
<div class="m2"><p>گه به مطبخ بود گاهی بر درخت</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پس تلافی کرد اندر شهر شام</p></div>
<div class="m2"><p>زاده هند از برای احترام</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ساخت در بزم شراب آن بی‌حیا</p></div>
<div class="m2"><p>راس او را زینت طشت طلا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>عرش را چون فلک بی‌لنگر نمود</p></div>
<div class="m2"><p>بسکه هتک حرمت آن سر نمود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا زند آتش به قلب زینبش</p></div>
<div class="m2"><p>از غضب زد خیزران را بر لبش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>