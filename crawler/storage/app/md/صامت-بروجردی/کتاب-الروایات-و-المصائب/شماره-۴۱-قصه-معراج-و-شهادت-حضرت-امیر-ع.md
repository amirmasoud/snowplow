---
title: >-
    شمارهٔ ۴۱ - قصه معراج و شهادت حضرت امیر(ع)
---
# شمارهٔ ۴۱ - قصه معراج و شهادت حضرت امیر(ع)

<div class="b" id="bn1"><div class="m1"><p>در شب معراج هادی سبل</p></div>
<div class="m2"><p>خواجه امکان محمد(ص) عقل کل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید اندر آسمان پنجمین</p></div>
<div class="m2"><p>شکل زیبای امیرالمومنین(ع)</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت یا جبریل تصویر چیست</p></div>
<div class="m2"><p>این گرامی صورت زیبای کیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرض کرد ای باعث کون و مکان</p></div>
<div class="m2"><p>آرزو کردند اهل آسمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کنند آئینه دل منجلی</p></div>
<div class="m2"><p>از نگاه ماه رخسار علی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزد ایزد با تمنای تمام</p></div>
<div class="m2"><p>بر زمین سودند روی احترام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کای پدید آرَنده بالا و پست</p></div>
<div class="m2"><p>از تو در هستی هویدا هر چه هست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد بنی‌آدم ز لطف کردگار</p></div>
<div class="m2"><p>در زمین از وصل حیدر کامکار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با علی هستند دایم روبرو</p></div>
<div class="m2"><p>شادکام از لذت دیدار او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از پی درک حضور بوتراب</p></div>
<div class="m2"><p>«ربنا با لیتناکنا تراب»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کام ما را هم روا کن ای خدا</p></div>
<div class="m2"><p>از پی درک حضور مرتضی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پس اجابت کرد خلاق ملک</p></div>
<div class="m2"><p>دعوت خیل ملک را در فلک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>یعنی از انواع قدس خویشتن</p></div>
<div class="m2"><p>صورتی آراست حی ذوالمنن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برد صنعتها در آن صورت به کار</p></div>
<div class="m2"><p>آفرین بر صورت صورت نگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شد دگر خیل ملایک و امطاف</p></div>
<div class="m2"><p>صورت حیدر به هنگام طواف</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>یافت تسکین قلب سکان سما</p></div>
<div class="m2"><p>سال‌ها از صورت شیر خدا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا به سجده این ملجم زد ز کین</p></div>
<div class="m2"><p>تیغ بر فرق امام المتقین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در زمین چون شیر حق را سر شکافت</p></div>
<div class="m2"><p>در فلک هم تارک حیدر شکافت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زین سبب شد کز خروش و ولو برای او</p></div>
<div class="m2"><p>شد زمین و آسمان در ولو برای او</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جبرئیل از عرش در داد این ندا:</p></div>
<div class="m2"><p>«هدمت والله ارکان الهدی»</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آه از آن ساعت که بی‌تاب و توان</p></div>
<div class="m2"><p>شد علی در خانه از مسجد روان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آه کلثوم جگر خون سرکشید</p></div>
<div class="m2"><p>زینب از سر در حرم معجر کشید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خون‌چکان شد مجتبی از هر دو عین</p></div>
<div class="m2"><p>شد به دامان اشگ گلگون حسین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دیده بگشود آن امام ممتحن</p></div>
<div class="m2"><p>بهر تسکین حرم از مرد و زن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با حسن گفت ای مرا نور بصر</p></div>
<div class="m2"><p>آن زمان کن گریه ای جان پدر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کز شرار ز هر سوزد پیکرت</p></div>
<div class="m2"><p>او فتد لخت جگر اندر برت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یا حسین گفت ای شه گلگون قبا</p></div>
<div class="m2"><p>گریه کن اندر زمین کربلا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آن زمان کز قوم کوفی هر طرف</p></div>
<div class="m2"><p>می‌زند بی‌حد پی قتل تو صف</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اینقدر خواهی نمودن العطش</p></div>
<div class="m2"><p>تا ز بی‌آبی کنی هر لحظه غش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>طاقتت از مرگ قاسم کم شود</p></div>
<div class="m2"><p>از غم عباس پشتت خم شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر گلوی اصغر، آن طفل صغیر</p></div>
<div class="m2"><p>جای شیر آید ز میدان نوک تیر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>می‌برد از چشم حق بین تو نور</p></div>
<div class="m2"><p>داغ اکبر تا صف یوم النشور</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>عاقبت گردد سرت ای ارجمند</p></div>
<div class="m2"><p>برسنان کوفیان یک نی بلند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عارضت چون آفتاب انوری</p></div>
<div class="m2"><p>در تنور آخر شود خاکستری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>پس به زینب گفکای بی‌صبر و تاب</p></div>
<div class="m2"><p>گریه کن در رفتن شام خراب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرهی کن روزی که در بازار شام</p></div>
<div class="m2"><p>می‌برندت پش چشم خاص و عام</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>داغ من کرد از جهالت ناامید</p></div>
<div class="m2"><p>پس چه خواهی کرد در بزم یزید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن زمان دندان بنه اندر جگر</p></div>
<div class="m2"><p>کز جفا بینی یزید بد سیر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>می‌زند از قهر چوب خیرزان</p></div>
<div class="m2"><p>بر لب شاهنشه لب تشنه‌گان</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نیست یارای نوشتن خامه را</p></div>
<div class="m2"><p>مختصر کن (صامت) این هنگامه را</p></div></div>