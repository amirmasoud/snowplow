---
title: >-
    شمارهٔ ۱ - مصائب و غیره
---
# شمارهٔ ۱ - مصائب و غیره

<div class="b" id="bn1"><div class="m1"><p>زینب چون دید خسرو دین مانده بی‌معین</p></div>
<div class="m2"><p>رفت و گرفت دست دو طفلان نازنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آورد آن دو تا گل گلزار خویش را</p></div>
<div class="m2"><p>با چشم اشکبار به نزد امام دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا که خواهم ای شه خوبان ز جان کنم</p></div>
<div class="m2"><p>این هدیه را نثار قدومت در این زمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای حشمت الله از ره احسان نما قبول</p></div>
<div class="m2"><p>ران ملخ ز مور دل افسرده غمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این عون و آن محمد خواهم کنم ز جان</p></div>
<div class="m2"><p>آن را فدای اکبر و قربان اصغر این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرمود شه که این دو مرا نور دیده‌اند</p></div>
<div class="m2"><p>سازم چسان روان بدم تیغ مشرکین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرگ برادر و غم یاران مرا بس است</p></div>
<div class="m2"><p>منما فزون داغ من زار بیش از این</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر نیاز زینب و عون و محمدش</p></div>
<div class="m2"><p>سودند جبهه بر در آن قبله یقین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کردند بس نیاز که شه دادن اذن جنگ</p></div>
<div class="m2"><p>بر آل دو طفل غمزده نورس حزین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوسید آن دو کودک و بوئیدشان ز مهر</p></div>
<div class="m2"><p>آن را چو شاخ نرگس و این را چو یاسمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پس زینب ستمزده پوشید شان کفن</p></div>
<div class="m2"><p>زد شانه به سنبل گیسوی عنبرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ و سپر ببست و روان کرد همچو ماه</p></div>
<div class="m2"><p>شد ز آسمان دیده سرشگش به آستین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تیر و کمان فکند به مرکب نشاندشان</p></div>
<div class="m2"><p>گفتی که مهر و ماه عیان شد ز برج زین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رفتند سوی رزم و برآن دست و تیغشان</p></div>
<div class="m2"><p>برخاست از قضا و قدر صورت آفرین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن همچو رعد غلغله در شش جهت فکند</p></div>
<div class="m2"><p>وین زد چو برق شعله به قلب سپاه کین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن چون شرار از نار عدو را ز پا فکند</p></div>
<div class="m2"><p>وین دست و سر چوب برگ خزان ریخت بر زمین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>و آن به اسنان ز جسم عدو جوی خون گشود</p></div>
<div class="m2"><p>بست این ره فرار ز هر سو به مشرکین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>کرد آن صدای الحذر ازکوفیان بلند</p></div>
<div class="m2"><p>این الامان رساند به گردون هفتمین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن فوج فوج را به سقر دادشان مقر</p></div>
<div class="m2"><p>این فرقه فرقه را به درک کردشان مکین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گفتا یکی ز حمزه مگر دارد این نشان</p></div>
<div class="m2"><p>گفت آن دگر به جعفر طیار ماند این</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آخر ز پیش جنگ دو شیران گریختند</p></div>
<div class="m2"><p>روباه وار حمله نمودند از کمین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیر اجل ز ابر بلا ریخت چون مطر</p></div>
<div class="m2"><p>بر جسم ناز پرورشان گشت دلنشین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن می‌فکند نیزه و پیکانش از یسار</p></div>
<div class="m2"><p>آن می‌زدی به خنجر برانش از یمین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آخر همان دو پیکر پاک شریف شد</p></div>
<div class="m2"><p>از نیزه پاره پاره ز جور مخالفین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گشتند آن دو طفل و فکندند از الم</p></div>
<div class="m2"><p>آتش به قلب زینب غمدیده حزین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سرداد شاه تشنه در این ماتم و کشید</p></div>
<div class="m2"><p>از دیده سیل اشک و ز دل آه آتشین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>(حاجب) ز داغ این دو برادر سرشک ریخت</p></div>
<div class="m2"><p>شاید شوند شافع او یوم واپسین</p></div></div>