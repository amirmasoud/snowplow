---
title: >-
    شمارهٔ ۴ - ورود اسرا در شام خراب
---
# شمارهٔ ۴ - ورود اسرا در شام خراب

<div class="b" id="bn1"><div class="m1"><p>چو کرد قافله به یکسان به شام ورود</p></div>
<div class="m2"><p>نمود صبح قیامت به چشم خلق قیام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آنچه بر سر آل علی گذشت ز جور</p></div>
<div class="m2"><p>نمود تازه دگر باره عهد خود ایام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی به عیش که آل علی قتیل شدند</p></div>
<div class="m2"><p>یکی بنوش که ما را زمانه گشت به کام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لباس نو همه بر تن ز اطلس دیبا</p></div>
<div class="m2"><p>به کف خضاب چو کف الخضیب بسته تما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز یک طرف سر بی‌چادر حریم رسول</p></div>
<div class="m2"><p>ز جانبی به تماشاهم از خواص و عوام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز قید سلسله مجروح گردن بیمار</p></div>
<div class="m2"><p>شده ز جور رسن خسته بازوی ایتام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یکی به چوب ستم می‌زدی به فرق زنان</p></div>
<div class="m2"><p>یکی ز آتش نی ریختی بهر دور بام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چسان گذشت به عابد که دید راس پدر</p></div>
<div class="m2"><p>نشان سنگ جفا شد به شام غم انجام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه کس که بر سر کلثوم افکند معجر</p></div>
<div class="m2"><p>نه دادرس که به آن کودکان کند اطعام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جهان گرفت چنان تنگ بر حریم رسول</p></div>
<div class="m2"><p>که گوئیا شده راحت بدان گروه حرام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به صدهزار تعب اهل بیت بی‌کس را</p></div>
<div class="m2"><p>به وقت شام بدادند در خرابه مقام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بی‌وفایی دنیا همین بس است که رفت</p></div>
<div class="m2"><p>به بزم کفر سر شاه کشور اسلام</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نشسته بود به کرسی زر یهود و مجوس</p></div>
<div class="m2"><p>ستاده بود بپا عابدین امام انام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چه گویم آه که کفر یزید شوم چه کرد</p></div>
<div class="m2"><p>بچوب خیزر و لعل لب شریف امام</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بس است شرح غم شام سر مکن (حاجب)</p></div>
<div class="m2"><p>که نی به جسم توان ماند و نه بدل آرام</p></div></div>