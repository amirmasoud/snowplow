---
title: >-
    شمارهٔ ۱۲ - از اشعار مشهور صامت که جدیداً بدست آمده است(در رانده شدن ابلیس از درگاه حضرت احدیت و نزول جبرئیل در گودال قتلگاه)
---
# شمارهٔ ۱۲ - از اشعار مشهور صامت که جدیداً بدست آمده است(در رانده شدن ابلیس از درگاه حضرت احدیت و نزول جبرئیل در گودال قتلگاه)

<div class="b" id="bn1"><div class="m1"><p>شنیدستم این قصه معتبر</p></div>
<div class="m2"><p>من از راویان صحیح الخبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که از دست قدرت چه روز نخست</p></div>
<div class="m2"><p>گل بوالبشر را به تصویر جست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تلبیس ابلیس منظور شد</p></div>
<div class="m2"><p>ملایک پی سجده مامور شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تمامی بدین امر اقرار کرد</p></div>
<div class="m2"><p>جز ابلیس کار سجده انکار کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خود گفت من ز آتشم او ز خاک</p></div>
<div class="m2"><p>گر از سجده‌اش رو بپیچم چه باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تکبر به سویش چو آورد روی</p></div>
<div class="m2"><p>بشد طوقی از لعنتش در گلوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بر نار نازید او نار شد</p></div>
<div class="m2"><p>به نیران سوزان سزاوار شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس از توسن کبر آمد فرود</p></div>
<div class="m2"><p>بگفتا که ای پاک حی و دود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو میرانیم از درت شرمسار</p></div>
<div class="m2"><p>چه شد مزد طاعاتم ای کردگار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ندا آمد از داور دادخواه</p></div>
<div class="m2"><p>که ای روسیاه آنچه خواهی بخواه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چنین گفت شیطان که ای کردگار</p></div>
<div class="m2"><p>سه مطلب کن از بهر من بخواه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین گفت شیطان که ای کردگار</p></div>
<div class="m2"><p>سه مطلب کن از بهر من اختیار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نخست آنکه پاداش این بندگی</p></div>
<div class="m2"><p>به جاوید خواهم ز تو زندگی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دوم آنکه راهم دهی رایگان</p></div>
<div class="m2"><p>تو در عضو عضو همه بندگان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سوم مطلبم ای خدای غفور</p></div>
<div class="m2"><p>بماند به وقتی که باشد ضرور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندا آمد از حضرت کبریا</p></div>
<div class="m2"><p>که کردیم ما حاجتت را روا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از آن روز رو کرد آن بدگمان</p></div>
<div class="m2"><p>به عالم به گمراهی بندگان</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سوم مطلبش شد عیان برملا</p></div>
<div class="m2"><p>همان ظهور عاشورا در کربلا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که فرزند زهرا چو از پشت زین</p></div>
<div class="m2"><p>نگون گشت بی‌کس به روی زمین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>غریبانه بر خاک سر بر نهاد</p></div>
<div class="m2"><p>مهیا ز بهر شهادت ستاد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آن لحظه شیطان نمود این خیال</p></div>
<div class="m2"><p>که گر کشته شد این شه بی‌همال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شفاعت در این کار حاصل شود</p></div>
<div class="m2"><p>همه سعی من هیچ و باطل شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رهند عاصیان از عذاب و گناه</p></div>
<div class="m2"><p>همین دم بمانم به روی سیاه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کنم حیله خویش در کار او</p></div>
<div class="m2"><p>که شاید ترش سازد از صبر رو</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بگفت ای خداوند گار مبین</p></div>
<div class="m2"><p>بود موسم مطلب سومین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سوم مطلبم این بود بی‌حجاب</p></div>
<div class="m2"><p>که آید به اول فلک آفتاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نماید تمام حرارت عیان</p></div>
<div class="m2"><p>بتابد به جسم حسین آن زمان</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو این آرزو کرد آمد خطاب</p></div>
<div class="m2"><p>به نوعی که می‌خواست شد آفتاب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چنان تافت بر پیکر شاهدین</p></div>
<div class="m2"><p>که شد دود بر آسمان و زمین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>عطش گشت غالب چنان بر امام</p></div>
<div class="m2"><p>که کام و زبان شه تشنه کام</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز خشکی شدی چون به هم آشنا</p></div>
<div class="m2"><p>تو گفتی که از چوب خیزد صدا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز هر زخم وی خون درآمد به جوش</p></div>
<div class="m2"><p>بر آمد ز خیل ملایک خروش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در آن دم ز درگاه رب جلیل</p></div>
<div class="m2"><p>به سوی زمین شد روان جبرئیل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>پر خویش را کرد پس سایبان</p></div>
<div class="m2"><p>بدان جسم پر زخم تیر و سنان</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شه تشنه لب دیده را کرد باز</p></div>
<div class="m2"><p>به جبریل فرمود با صد نیاز</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که ای جبرئیل این پرت بر سرم</p></div>
<div class="m2"><p>حجابی است بین من و دلبرم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مرا روی دل جز به محبوب نیست</p></div>
<div class="m2"><p>در این دم به سر سایه مطلوب نیست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر هست منظورت احسان من</p></div>
<div class="m2"><p>برو سایه کن بر جوانان من</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>برو سایه کن بر علی اکبرم</p></div>
<div class="m2"><p>به طفل صغیرم علی اصغرم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر مطلبت هست امداد من</p></div>
<div class="m2"><p>برو سایه افکن به داماد من</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>که او را دو آتش نموده کباب</p></div>
<div class="m2"><p>یکی داغ حسرت یکی آفتاب</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گذر بر سر خسرو ناس کن</p></div>
<div class="m2"><p>دمی سایه بر زخم عباس کن</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>برو جانب خیمه ای دل غمین</p></div>
<div class="m2"><p>فکن سایه اندر سر عابدین</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>که از سوز تب العطش می‌کند</p></div>
<div class="m2"><p>به بستر فتاده است و غش می‌کند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>چو گردد دم دیگر ای جبرئیل</p></div>
<div class="m2"><p>عیالم در این دشت خوار و ذلیل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نه چادر به سر نی لباسی به تن</p></div>
<div class="m2"><p>تمامی بر همه سر عریان بدن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>به هر جا که گردند ایشان مقیم</p></div>
<div class="m2"><p>فکن سایه بر کودکان یتیم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>خصوصاً به ویرانه شهر شام</p></div>
<div class="m2"><p>گذر کن در آنجای بی‌سقف و بام</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>محبت به اطفال داریوش کن</p></div>
<div class="m2"><p>دمی سایه‌شان از پر خویش کن</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>اگر شعر (صامت) تو را شد قبول</p></div>
<div class="m2"><p>ببر در جنان عرضه کن بر رسول</p></div></div>