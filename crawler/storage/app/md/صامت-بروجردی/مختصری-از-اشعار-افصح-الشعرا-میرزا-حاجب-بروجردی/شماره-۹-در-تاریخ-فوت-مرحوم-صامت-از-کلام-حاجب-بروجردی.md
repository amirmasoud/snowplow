---
title: >-
    شمارهٔ ۹ - در تاریخ فوت مرحوم صامت از کلام حاجب بروجردی
---
# شمارهٔ ۹ - در تاریخ فوت مرحوم صامت از کلام حاجب بروجردی

<div class="b" id="bn1"><div class="m1"><p>چون محمدباقر از حکم خداوند حکیم</p></div>
<div class="m2"><p>بر سرش افتاد شوق قرب خلاق رحیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید نبود این جهان را جز غم و رنج و تعب</p></div>
<div class="m2"><p>زحمتش بار گران و محنتش دردی الیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیده بست از فطرت پست لئیم روزگار</p></div>
<div class="m2"><p>چشم بگشود از پی الطاف و نعمای کریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ندای ارجعی بشنید از پیک اله</p></div>
<div class="m2"><p>رفت در گلزار جهت مرغ روحش چون نسیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کمال رغبت و فیض حضور و اوج قرب</p></div>
<div class="m2"><p>از حضیض تن بشد بر شاخه طوبی مقیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کرد مصف عمر در مدح نبی و آل او</p></div>
<div class="m2"><p>بس در ناسفته سفت از نظم و طبع مستقیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آسمان چون بی‌عدیلش دید در فضل و کمال</p></div>
<div class="m2"><p>کرد پنهان پیکرش در خاک چون در یتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از محرم شانزده بگذشت در یوم خمیس</p></div>
<div class="m2"><p>رفت مهمان شد بخوان جو در زاق قدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهر تاریخ وفاتش خامه (حاجب) نوشت</p></div>
<div class="m2"><p>داد یزدان جای صامت سوی جنات نعیم</p></div></div>