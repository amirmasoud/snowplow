---
title: >-
    شمارهٔ ۵ - همچنین در مصیبت
---
# شمارهٔ ۵ - همچنین در مصیبت

<div class="b" id="bn1"><div class="m1"><p>چو شاه تشنه جگر راه کارزار گرفت</p></div>
<div class="m2"><p>پی قتال به به کف تیغ آبدار گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز وحشت عدم و انقلاب خود عالم</p></div>
<div class="m2"><p>خط امان زدم تیغ پر شرار گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میان معرکه آمد به یک جهان هیبت</p></div>
<div class="m2"><p>صف جدال به صد فخر و اقتدار گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خطاب کرد که ای قوم از چه رو باید</p></div>
<div class="m2"><p>به عترت نبی اینگونه سخت کار گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آن حسین مگر نیستم که روح الامین</p></div>
<div class="m2"><p>به دهر خدمت من بهر افتخار گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آن حسین مگر نیستم که ختم رسل</p></div>
<div class="m2"><p>گهم به دوش گه آغوش گه کنار گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو از حجاز به سوی عراقم آوردید</p></div>
<div class="m2"><p>نفاق را ز چه باید به خود شعار گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر آنچه موعظه فرمود می‌نکرد اثر</p></div>
<div class="m2"><p>زبان پند رها کرد و ذوالفقار گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز نعره‌ای که کشید از جگر جهان لرزید</p></div>
<div class="m2"><p>ز گاو ارض و زشیر فلک قرار گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اشاره‌ای که به تیغ دو سر نمود بسر</p></div>
<div class="m2"><p>تن از یمین بیفکند و سر از یسار گرفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو ضرب دست خدا دید خصم در پیکار</p></div>
<div class="m2"><p>دو اسبه رو به جهنم ره فرار گرفت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ولی چه سود نیز زد به قطره خونی</p></div>
<div class="m2"><p>که تیر حرمله ز آن طفل شیرخواره گرفت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صفوف کفر درید و مظفر و منصور</p></div>
<div class="m2"><p>شط فرات به شمشیر شعله بار گرفت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>میان شط شد و بر آب آنچنان نگریست</p></div>
<div class="m2"><p>شطی میان شط از چشم اشکبار گرفت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به یاد تشنه لبان آه سوزناک کشید</p></div>
<div class="m2"><p>که آهش از دل آب روان شرار گرفت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو بود در نظرش کام خشک اطفالش</p></div>
<div class="m2"><p>نخورد آب و دل از شط به اختیار گرفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به عهده وعده روز الست کر وفا</p></div>
<div class="m2"><p>ز شوق داد سرو وصل کردگار گرفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نخورد آب اگر سبط مصطفی (جانب)</p></div>
<div class="m2"><p>هزار ط ز غمش چشم روزگار گرفت</p></div></div>