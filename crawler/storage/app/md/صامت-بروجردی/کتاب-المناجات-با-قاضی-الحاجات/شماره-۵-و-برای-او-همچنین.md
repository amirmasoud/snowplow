---
title: >-
    شمارهٔ ۵ - و برای او همچنین
---
# شمارهٔ ۵ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>شد وقت آنکه درد نهان را دوا کنیم</p></div>
<div class="m2"><p>روی نیازخویش به سوی خدا کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خفتگان بستر راحت سحر رسید</p></div>
<div class="m2"><p>خیزید تا گذر به خدا از رجا کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیزید تا به وعده «ادعونی استجب»</p></div>
<div class="m2"><p>تکلیف خود به درگه داور ادا کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیگانگی بس است ز درگاه کردگار</p></div>
<div class="m2"><p>خود را دمی به خالق خود آشنا کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا صبح عمر ما ننموده است رو به شام</p></div>
<div class="m2"><p>کاری برای خجلت روز جزا کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشکی ز چشم خویش بریزیم و اندر او</p></div>
<div class="m2"><p>از بهر شست و شوی گناهان شنا کنیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر نجات آتش دوزخ به صد امید</p></div>
<div class="m2"><p>دست دعا بلند سوی کبریا کنیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کاسهای دیده نگردیده پر ز خاک</p></div>
<div class="m2"><p>چشمی به حال بی‌کسی خویش را کنیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به گذشتگان و اسیران زیر خاک</p></div>
<div class="m2"><p>نزد خدای عالم و آدم دعا کنیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از یاد رفته وعده روز الست</p></div>
<div class="m2"><p>یک شب وفا به وعده قالوا بلی کنیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شیطان نهاده بند اطاعت به پای ما</p></div>
<div class="m2"><p>بهر رها نمودن خود دست و پا کنیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا از پی شفاعت ما چاره جو شود</p></div>
<div class="m2"><p>روی نیاز را به سوی مصطفی کنیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از قلب سوزناک سوی کشور نجف</p></div>
<div class="m2"><p>با گریه التجا بشه اولیاء کنیم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گاهی سوی علی و ولی متلجی شویم</p></div>
<div class="m2"><p>گه روی استغاثه به خیرالنساء کنیم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دستی به دامن حسن مجتبی زنیم</p></div>
<div class="m2"><p>روئی به آستان شه کربلا کنیم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>این پنج تن مقرب درگاه داورند</p></div>
<div class="m2"><p>هر پنج را شفیع پی مدعا کنیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا رب اگر ز لطف نبخشی گناه ما</p></div>
<div class="m2"><p>در حیرتم که جز در تورو کجا کنیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>یا رب حساب معصیت ما ز حد گذشت</p></div>
<div class="m2"><p>راهی نما که چاره جرم و خطا کنیم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا رب دری گشا ز هدایت بر وی ما</p></div>
<div class="m2"><p>شاید دیگر ز کثرت عصیان حیا کنیم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یا رب همیشه کار تو فضل و کرامتست</p></div>
<div class="m2"><p>بر ما مبین تو جرم و خطائی که ما کنیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>یا رب به خدمت تو چه گوئیم در جواب</p></div>
<div class="m2"><p>به هر حساب چون به صف حشر جا کنیم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اعضای ما تمام بود شاهد گناه</p></div>
<div class="m2"><p>دیگر گذشته آنکه ز عصیان ابا کنیم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نزد تو ای مهیمن یکتا تمام عمر</p></div>
<div class="m2"><p>یک دم نشد که پشت عبادت دو تا کنیم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ما را ببخش ورنه به آل‌عبا تو را</p></div>
<div class="m2"><p>چندان قسم دهیم که از خود رضا کنیم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>(صامت) ز آب توبه گناهان خود بشوی</p></div>
<div class="m2"><p>تا خویش را ز آتش و دوزخ رها کنیم</p></div></div>