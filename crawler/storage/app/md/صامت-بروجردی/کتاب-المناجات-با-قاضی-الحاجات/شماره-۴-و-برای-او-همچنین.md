---
title: >-
    شمارهٔ ۴ - و برای او همچنین
---
# شمارهٔ ۴ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>یا رب اگر ز کرده ما پرده واکنی</p></div>
<div class="m2"><p>ما را به خجلت ابدی مبتلا کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابلیس وار جامه طغیان ببر کنید</p></div>
<div class="m2"><p>گر یک نفس به خویش کسی را رها کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس به جان خویش جفا بیشتر کند</p></div>
<div class="m2"><p>به روی تو بیشتر ز ترحم وفا کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی دهی به مردم بیگانه صد هزار</p></div>
<div class="m2"><p>کز صدهزار یک نفری آشنا کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فعل خویش عارف و عامی کنند شرم</p></div>
<div class="m2"><p>روزی که دستگاه عدالت بپا کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کسرا مجالچون و چرا در بر تو نیست</p></div>
<div class="m2"><p>با بندگان هر آنچه نمایی بجا کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر گبر رو کند بدرت بهر التجا</p></div>
<div class="m2"><p>هر دم ز مهر حاجت او را روا کنی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کس بهر لباس که با عجز و التماس</p></div>
<div class="m2"><p>در حضرت تو رو کند او را رضا کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چندین هزار مجرم و عاصی و روسیاه</p></div>
<div class="m2"><p>در یک نفس ز آتش دوزخ رها کنی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیر از تو نیست راه پناهی برای خلق</p></div>
<div class="m2"><p>باید تو روی لطف به احوال ما کنی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز تو طبیب نیست امید است هر دو کون</p></div>
<div class="m2"><p>این دردهای بی‌حد ما را دوا کنی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یا رب تو قبله‌گاه امیدی و چون کنم</p></div>
<div class="m2"><p>گر دست ما ز دامن لطف جدا کنی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>باشد امید ما بدرت آن که جمله را</p></div>
<div class="m2"><p>از دوستان درگه آل عبا کنی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اندر شمار ما همه را در صف شمار</p></div>
<div class="m2"><p>از شیعیان پادشه لافتی کنی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در روز رستخیز قیامت شفیع ما</p></div>
<div class="m2"><p>سر خیل کائنات و شه انبیاء کنی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شایسته محبت لطف تو گرنه‌ایم</p></div>
<div class="m2"><p>رحمی به ما به خاطر شیر خدا کنی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یا رب همین بس است تمنای ما که تو</p></div>
<div class="m2"><p>هنگام مرگ مدفن ما کربلا کنی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در آفتاب گرم قیامت مقام ما</p></div>
<div class="m2"><p>زیر لوای احمد صاحب لوا کنی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از لطف بی‌حساب تو یا رب بعید نیست</p></div>
<div class="m2"><p>گر رحمتی (به صامت) بی‌دست و پا کنی</p></div></div>