---
title: >-
    شمارهٔ ۳ - و برای او همچنین
---
# شمارهٔ ۳ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>یا رب مرا به چنگ بلا مبتلا مکن</p></div>
<div class="m2"><p>دست مرا ز دامن لطفت جدا مکن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حد گذشته گرچه گناه و خطای ما</p></div>
<div class="m2"><p>چشم از گنه بپوش و نظر بر خطا مکن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افعال ما به وفق و رضا تو گر که نیست</p></div>
<div class="m2"><p>بر ما ز لطف سد طریق رضا مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما در خورعذاب و تو شایسته کرم</p></div>
<div class="m2"><p>غیر از کرم سلوک به احوال ما مکن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه گناه پرده ما را دریده است</p></div>
<div class="m2"><p>از کار ما به رحمتخود پرده وامکن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ستاریست شیوه تو چون به هر دو کون</p></div>
<div class="m2"><p>رسوا مرا ز جرم به روز جزا مکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر معصیت که باعث حبس دعای ماست</p></div>
<div class="m2"><p>نادیده بین ز بخشش ورد دع مکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امیدوار لطف و عطای تو بوده‌ایم</p></div>
<div class="m2"><p>قطع امیدواری ما ای خدا مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما را به غیر جرم و خطا نیست پیشه‌ای</p></div>
<div class="m2"><p>ما را رها به خویش از این ماجرا مکن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذات تو از عبادت خلق است بی‌نیاز</p></div>
<div class="m2"><p>ای بی‌نیاز شیوه خود را رها مکن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>«ادعونی استجب لکم» اندر کلام تو است</p></div>
<div class="m2"><p>از جرم ما ز وعده قرآن ابا مکن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در هر دو کون دست گنهکار ما جدا</p></div>
<div class="m2"><p>از دامن مودت آل عبا مکن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آفتاب گرم قیامت مرا برون</p></div>
<div class="m2"><p>از سایه لوای شه لافتی مکن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از خدمت ائمه اثنی عشر بحشر</p></div>
<div class="m2"><p>ما را جدا به حق شه کربلا مکن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تعجیل کن برای ظهور امام عصر</p></div>
<div class="m2"><p>زین بیش طول غیبت آن مقتدا مکن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اسلام منهدم شد زین بیشتر دگر</p></div>
<div class="m2"><p>منع ظهور مهدی صاحب لوا مکن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>(صامت) بود سک در سلطان اولیا</p></div>
<div class="m2"><p>او را ز جای خویش دیگر جابجا مکن</p></div></div>