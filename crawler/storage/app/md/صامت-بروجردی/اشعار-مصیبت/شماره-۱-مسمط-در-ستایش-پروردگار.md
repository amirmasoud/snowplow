---
title: >-
    شمارهٔ ۱ - مسمط در ستایش پروردگار
---
# شمارهٔ ۱ - مسمط در ستایش پروردگار

<div class="b" id="bn1"><div class="m1"><p>اول ایجاد چون خدای تعالی</p></div>
<div class="m2"><p>کرد پدید از قلم چو صورت اشیاء</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت قلم بهر وصف ایزد یکتا</p></div>
<div class="m2"><p>ای ز صفات تو، ذات پاک تو پیدا</p></div></div>
<div class="b2" id="bn3"><p>در دل هر ذره قدرت تو هویدا</p></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی از چاره دست وی شده کوته</p></div>
<div class="m2"><p>سوی تو آورده روی درگه و بیگه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله تو را بنده گر گدا و اگر شه</p></div>
<div class="m2"><p>علم تو چون قدر توز عیب منزه</p></div></div>
<div class="b2" id="bn6"><p>قدر تو چون علم تو ز نقص مبرا</p></div>
<div class="b" id="bn7"><div class="m1"><p>نیست کسی را به کنه معرفت پی</p></div>
<div class="m2"><p>مرغ نفس روز و شب به گفتن یاحی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کرده به تن آشیان قرب تو را طی</p></div>
<div class="m2"><p>جلوه حسن تو گر نتافته بروی</p></div></div>
<div class="b2" id="bn9"><p>روح به زندان گرفته بهر چه ماوا</p></div>
<div class="b" id="bn10"><div class="m1"><p>مخزن عقل هر آنکه باشد ز گهر پر</p></div>
<div class="m2"><p>کرد به صنع خایی تو تفکر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در عجب ابلیس شد ز عجب و تکبر</p></div>
<div class="m2"><p>در شب معراج گفت بهر تحیر</p></div></div>
<div class="b2" id="bn12"><p>آدم خاکی کجا و عالم بالا</p></div>
<div class="b" id="bn13"><div class="m1"><p>هر که به راه محبت تو قدم زد</p></div>
<div class="m2"><p>دولت جاوید جست و عزت سرمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کار مجاز از حقیقت تو موید</p></div>
<div class="m2"><p>عشق اگر از تو نیست بهر چه نبود</p></div></div>
<div class="b2" id="bn15"><p>هیکل مجنون جدا ز هیبت لیلا</p></div>
<div class="b" id="bn16"><div class="m1"><p>هر که شد اندر حریم قرب تو محرم</p></div>
<div class="m2"><p>شاهی او شد به کائنات مسلم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نسل بنی‌آدم از تو گشت مکرم</p></div>
<div class="m2"><p>گرنه تو را بنگرد به قالب آدم</p></div></div>
<div class="b2" id="bn18"><p>سجده به آدم کند ملائکه؟ حاشا!</p></div>
<div class="b" id="bn19"><div class="m1"><p>دانش کونین در صفات تو قاصر</p></div>
<div class="m2"><p>نیست کسی را به جز تو یاور و ناصر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بود ازل را وجود تست معاصر</p></div>
<div class="m2"><p>قصد عبودیت چهار عناصر</p></div></div>
<div class="b2" id="bn21"><p>خاصه معبودی و تو قادر و دانا</p></div>
<div class="b" id="bn22"><div class="m1"><p>قهر تو اهل غرور را شده ناکب</p></div>
<div class="m2"><p>مهر به جنگ سپهر ز امر تو راکب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سیر فلک را مشیت تو مراکب</p></div>
<div class="m2"><p>جلوه شمع شهود هفت کواکب</p></div></div>
<div class="b2" id="bn24"><p>شاهد یکتایی تو شاهد یکتا</p></div>
<div class="b" id="bn25"><div class="m1"><p>نیست به قصد جلالت تو رسیدن</p></div>
<div class="m2"><p>حضرت جبریل را مجال پریدن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>راه تو رفتن خوش است و روی تو دیدن</p></div>
<div class="m2"><p>در شجر از جلوه تو گاه بریدن</p></div></div>
<div class="b2" id="bn27"><p>اره خجل شد ز طاقت زکریا</p></div>
<div class="b" id="bn28"><div class="m1"><p>طوطی شیرین سخن شکر شکن از تو</p></div>
<div class="m2"><p>بلبل شیدا به گل کند سخن از تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بویسمن از تو عطر یاسمن از تو</p></div>
<div class="m2"><p>صانع صنعت گری که در چمن از تو</p></div></div>
<div class="b2" id="bn30"><p>سوسن اسود شگفت و لاله حمرا</p></div>
<div class="b" id="bn31"><div class="m1"><p>طره سنبل ز تاب جعد تو پرچین</p></div>
<div class="m2"><p>سوی تو نرگس گشاده دیده حق بین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روی شقایق ز جام شوق تو رنگین</p></div>
<div class="m2"><p>معنی توحید تست لفظ ریاحین</p></div></div>
<div class="b2" id="bn33"><p>کز خط ریحان سبز می‌شود افشا</p></div>
<div class="b" id="bn34"><div class="m1"><p>گلشن ایجاد را ز حکم تو رونق</p></div>
<div class="m2"><p>لاله به سیر چمن ز وصل تو ملحق</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مست مدام از شراب لعل مروق</p></div>
<div class="m2"><p>مصدر اسرار تست ذکر انا الحق</p></div></div>
<div class="b2" id="bn36"><p>کز لب منصور غنچه می‌شود انشا</p></div>
<div class="b" id="bn37"><div class="m1"><p>نرگس شهلا به طرف باغ چو زنبق</p></div>
<div class="m2"><p>برده چو سرو سهی ز حسن تو رونق</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هست ثنای تو در شکوفه مفلق</p></div>
<div class="m2"><p>از پی تعظیم تست بید معلق</p></div></div>
<div class="b2" id="bn39"><p>خم شده در باغ ایستاده به یک پا</p></div>
<div class="b" id="bn40"><div class="m1"><p>آنچه که مرعی بود به کشور امکان</p></div>
<div class="m2"><p>وانچه نهان است از تصور اعیان</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>جمله در اوصاف ذات تست ثناخوان</p></div>
<div class="m2"><p>از غم سودای تست گشته پریشان</p></div></div>
<div class="b2" id="bn42"><p>سنقل آشتفه همچون زلف چلیپا</p></div>
<div class="b" id="bn43"><div class="m1"><p>قلب معارف به داغ مهر تو محزن</p></div>
<div class="m2"><p>امن تحلای تست وادی ایمن</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>طالب دیدار تست شیخ و بر همین</p></div>
<div class="m2"><p>دیده جمال تو جلوه‌گر که به گلشن</p></div></div>
<div class="b2" id="bn45"><p>دیدهحیرت شده است نرگس شهلا</p></div>
<div class="b" id="bn46"><div class="m1"><p>خاتم مهر تو مهر کرده لب گل</p></div>
<div class="m2"><p>غنچه نموده به صنعت تو تامل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>غرق به سیلاب شبنم است قرمفل</p></div>
<div class="m2"><p>حسن تو را می‌کند اشاره به بلبل</p></div></div>
<div class="b2" id="bn48"><p>گل به شکر خنده و شکوه به ایما</p></div>
<div class="b" id="bn49"><div class="m1"><p>دیر و حرم در پناه لطف تو آمن</p></div>
<div class="m2"><p>ارض و سما را ز حضرت تو میا من</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>صانع کونین و خدای مهیمن</p></div>
<div class="m2"><p>عین ستایش تویی ز کعبه مومن</p></div></div>
<div class="b2" id="bn51"><p>محض پرستش تویی ز معبد ترسا</p></div>
<div class="b" id="bn52"><div class="m1"><p>فیض تو جان را مدد اگر نرساند</p></div>
<div class="m2"><p>تن به تمنای وصل روح بماند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درک صفای تو مشت خاک چه داند</p></div>
<div class="m2"><p>قول تو را نطق عقل کل نتواند</p></div></div>
<div class="b2" id="bn54"><p>با همه حکمت بلا نعم ولا</p></div>
<div class="b" id="bn55"><div class="m1"><p>دولت لطف تو بهتر از همه دولت</p></div>
<div class="m2"><p>فضل تو اسباب فیض دولت و ملت</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>وا اسفا نزد حضرتت ز خجالت</p></div>
<div class="m2"><p>غرق گناهیم در سراچه غفلت</p></div></div>
<div class="b2" id="bn57"><p>بی‌خبر از خود چو باده‌خوار ز صهبا</p></div>
<div class="b" id="bn58"><div class="m1"><p>هر چه بود عیب و نقص از همه پاکی</p></div>
<div class="m2"><p>عین بقا عاری از فنا و هلاکی</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>در بر تو ماسوی کم از کف خاکی</p></div>
<div class="m2"><p>با تو محاکم کی از محاکمه باکی</p></div></div>
<div class="b2" id="bn60"><p>با تو محاسب خود از حساب چه پروا</p></div>
<div class="b" id="bn61"><div class="m1"><p>این منم آن مستمند عاصی حیران</p></div>
<div class="m2"><p>صدرنشین سر بر غفلت عصیان</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>منحرف از راه و رسم مذهب و ایمان</p></div>
<div class="m2"><p>دل به تو مشغول گشته نفس به شیطان</p></div></div>
<div class="b2" id="bn63"><p>نق عمل از میانه رفته به یغما</p></div>
<div class="b" id="bn64"><div class="m1"><p>مهر جان ذوق بندگی ز دلم برد</p></div>
<div class="m2"><p>شیشه عقلم به سنگ جهل هوا خورد</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>گشته ز صاف حیات قسمت ما درد</p></div>
<div class="m2"><p>گر پسری زشت و کور از پدری مرد</p></div></div>
<div class="b2" id="bn66"><p>گفت چو بادام بود چشم تو زیبا</p></div>
<div class="b" id="bn67"><div class="m1"><p>قامت جان خم به زیر بار غم توست</p></div>
<div class="m2"><p>منتظر لطف‌های دم به دم توست</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>بی‌پر و بی‌بال صیدی از کرم توست</p></div>
<div class="m2"><p>چون دیه با عاقله است از کرم توست</p></div></div>
<div class="b2" id="bn69"><p>دادن کالا به شخص گمشده کالا</p></div>
<div class="b" id="bn70"><div class="m1"><p>ای غم روی تو مونس شب و روزم</p></div>
<div class="m2"><p>نور لقای تو شمع بزم فروزم</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>قهر تو سرمیه رضا است هنوزم</p></div>
<div class="m2"><p>چون تو پسندی که من به حشر بسوزم</p></div></div>
<div class="b2" id="bn72"><p>مدعیان هر طرف کنند تماشا</p></div>
<div class="b" id="bn73"><div class="m1"><p>جنت و حور وقصور و کوثر و غلمان</p></div>
<div class="m2"><p>نار حجیم و شرار دووزخ سوزان</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>در بر من هست با رضای تو یکسان</p></div>
<div class="m2"><p>چون تو رضایی که من به دوزخ سوزان</p></div></div>
<div class="b2" id="bn75"><p>سوزم هر دم به زیر سایه طوبی</p></div>
<div class="b" id="bn76"><div class="m1"><p>لایق هر کس هر آنچه دیده وادادی</p></div>
<div class="m2"><p>بر رخ هر کس دری ز لطف گشادی</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>اول و آخر به جز تو نیست مرادی</p></div>
<div class="m2"><p>گر به تمنا نمی‌رسیم و تو شادی</p></div></div>
<div class="b2" id="bn78"><p>عین تمنای ما است ترک تمنا</p></div>
<div class="b" id="bn79"><div class="m1"><p>نیست بخوان کرم به جز تو کریمی</p></div>
<div class="m2"><p>صاحب احسان خاص و لطف عمیمی</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>مبدء اشیاء معید عرش عظیمی</p></div>
<div class="m2"><p>رازق و رحمانی و روف و رحیمی</p></div></div>
<div class="b2" id="bn81"><p>خالق سبحانی و حکیمی و دانا</p></div>
<div class="b" id="bn82"><div class="m1"><p>عین کمالات در وجود تو کامل</p></div>
<div class="m2"><p>رسم خدایی بود به اسم تو شامل</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>بر همه کس کوه کوه فیض تو نازل</p></div>
<div class="m2"><p>حی و سمیع و بصیر و عالم و عادل</p></div></div>
<div class="b2" id="bn84"><p>قادر و قیوم و فرد و وتر و توانا</p></div>
<div class="b" id="bn85"><div class="m1"><p>(صامت) اگر بر در تو روی گذارد</p></div>
<div class="m2"><p>دست دعایی زروی صدق برآرد</p></div></div>
<div class="b" id="bn86"><div class="m1"><p>روز قیمت ز دیده اشک ببارد</p></div>
<div class="m2"><p>جوهری از سیئات باک ندارد</p></div></div>
<div class="b2" id="bn87"><p>گرچه فناده ز بار معصیبت از پا</p></div>