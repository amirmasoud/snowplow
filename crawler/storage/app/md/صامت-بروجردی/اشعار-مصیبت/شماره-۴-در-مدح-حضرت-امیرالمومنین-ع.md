---
title: >-
    شمارهٔ ۴ - در مدح حضرت امیرالمومنین(ع)
---
# شمارهٔ ۴ - در مدح حضرت امیرالمومنین(ع)

<div class="b" id="bn1"><div class="m1"><p>مه شعبان گذشت و گشت عیان</p></div>
<div class="m2"><p>پیک ماه مبارک رمضان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای غزل خوان من غزل بر خوان</p></div>
<div class="m2"><p>غزلی تازه و بما مستان</p></div></div>
<div class="b2" id="bn3"><p>شو بر غم حسود باده گسار</p></div>
<div class="b" id="bn4"><div class="m1"><p>کو چنان عمر و کوچنان اقبال</p></div>
<div class="m2"><p>که دگر باره در مه شوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز غم روزگار فارغ بال</p></div>
<div class="m2"><p>به نشینیم خسرم و خوشحال</p></div></div>
<div class="b2" id="bn6"><p>صوم خود راز می‌کنیم افطار</p></div>
<div class="b" id="bn7"><div class="m1"><p>دو سه روزی به روزه مانده که باز</p></div>
<div class="m2"><p>خم شود قامتم ز بار نماز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حالیا از پی کلوخ انداز</p></div>
<div class="m2"><p>ساغر می به گردش آواز باز</p></div></div>
<div class="b2" id="bn9"><p>تاز کار افکنی مرا یک بار</p></div>
<div class="b" id="bn10"><div class="m1"><p>آن چنان مست کن مرا از می</p></div>
<div class="m2"><p>که شود صوم من به مستی طی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می به ساغر بریر پی در پی</p></div>
<div class="m2"><p>با دف و عود و به ربط و بانی</p></div></div>
<div class="b2" id="bn12"><p>بابم وزیر چنگ و موسیقار</p></div>
<div class="b" id="bn13"><div class="m1"><p>نه می‌دخت رز بود غرضم</p></div>
<div class="m2"><p>که برد جوهر و نهد عرضم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سستی آرد به درک ما فرضم</p></div>
<div class="m2"><p>کاهد از صحت و ده مرضم</p></div></div>
<div class="b2" id="bn15"><p>جای اقبال آورد ادبار</p></div>
<div class="b" id="bn16"><div class="m1"><p>خواهم از آنمیی که کرده خدا</p></div>
<div class="m2"><p>وصف او را به لیله الاسری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عارف و عامی از طریق وفا</p></div>
<div class="m2"><p>کرد تفسیر او خدا به خدا</p></div></div>
<div class="b2" id="bn18"><p>به می حب حیدر کرار</p></div>
<div class="b" id="bn19"><div class="m1"><p>علت غائی جهان وجود</p></div>
<div class="m2"><p>مایه اعتبار بود و نبود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر وجودی ز جود او موجود</p></div>
<div class="m2"><p>بنده پاک حضرت معبود</p></div></div>
<div class="b2" id="bn21"><p>وصی خاص احمد مختار</p></div>
<div class="b" id="bn22"><div class="m1"><p>چمن آرای گلشن وهاب</p></div>
<div class="m2"><p>زینت افزای منبر و محراب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شرف خاک و باد و آتش و آب</p></div>
<div class="m2"><p>باعث رتبه اولوا الالباب</p></div></div>
<div class="b2" id="bn24"><p>مردم دیده اولواالابصار</p></div>
<div class="b" id="bn25"><div class="m1"><p>موج دریای قدرت احدی</p></div>
<div class="m2"><p>ثمر نخل هیئت صمدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نمت خوان نعمت ابدی</p></div>
<div class="m2"><p>تحفه زاکیات لم یلدی</p></div></div>
<div class="b2" id="bn27"><p>باد دایم به آنجناب نثار</p></div>
<div class="b" id="bn28"><div class="m1"><p>ای ولی خدا خدایی کن</p></div>
<div class="m2"><p>یعنی از غیب خود نمایی کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در جهان کار کبریایی کن</p></div>
<div class="m2"><p>از محبان گره‌گشایی کن</p></div></div>
<div class="b2" id="bn30"><p>روبهان جمله گشته شیر شکار</p></div>
<div class="b" id="bn31"><div class="m1"><p>کربلا بر حسینت ای سرور</p></div>
<div class="m2"><p>تنگ شد آن قدر رجور قدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که لب خشک با دو دیده تر</p></div>
<div class="m2"><p>شد ز شمشیر شر دون بی‌سر</p></div></div>
<div class="b2" id="bn33"><p>دادرس بهر وی نبد دیار</p></div>
<div class="b" id="bn34"><div class="m1"><p>هرچه گفت ای ستمگران رحمی</p></div>
<div class="m2"><p>می‌دهم بهر آب جان رحمی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کس چو من نیست در جهان رحمی</p></div>
<div class="m2"><p>که به دشمن برد امان رحمی</p></div></div>
<div class="b2" id="bn36"><p>سنگ خون گرید از چنین گفتار</p></div>
<div class="b" id="bn37"><div class="m1"><p>لیک بر شمر دون نکرد اثری</p></div>
<div class="m2"><p>گرچه آهش بسوخت هر جگری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یا علی گر تو داشتی خبری</p></div>
<div class="m2"><p>همچو (صامت) مدام نوحه‌گری</p></div></div>
<div class="b2" id="bn39"><p>بود کار تو تا به روز شمار</p></div>