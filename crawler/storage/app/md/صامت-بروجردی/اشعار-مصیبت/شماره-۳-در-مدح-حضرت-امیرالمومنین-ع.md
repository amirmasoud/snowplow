---
title: >-
    شمارهٔ ۳ - در مدح حضرت امیرالمومنین(ع)
---
# شمارهٔ ۳ - در مدح حضرت امیرالمومنین(ع)

<div class="b" id="bn1"><div class="m1"><p>ساقی ز جای خیز فصل بهار شد</p></div>
<div class="m2"><p>چون طلعت نگار عالم نگار شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می ده که جیش دی اندر فرار شد</p></div>
<div class="m2"><p>بر تخت سلطنت گل استوار شد</p></div></div>
<div class="b2" id="bn3"><p>گلشن طرب فزا چون روی یار شد</p>
<p>جیبش فرح نمود تسخیر هفت خط</p></div>
<div class="b" id="bn4"><div class="m1"><p>یاقوت جان من یاقوت جان بیار</p></div>
<div class="m2"><p>لعل روان من لعل روان بیار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آرام جان من آرام جان بیار</p></div>
<div class="m2"><p>یعنی شراب ناب چون ارغوان بیار</p></div></div>
<div class="b2" id="bn6"><p>جان جهان من جان جهان بیار</p>
<p>خیز و پیاله را پر کن ز خون شط</p></div>
<div class="b" id="bn7"><div class="m1"><p>جوهرفروش عقل بنگر روان به رخش</p></div>
<div class="m2"><p>شد در هوا مسیر از بهر بذل و بخش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سطح زمین تمام مانا بود بدخش</p></div>
<div class="m2"><p>لعل گهر ز چند بنمود بخش بخش</p></div></div>
<div class="b2" id="bn9"><p>شد پیکر سمین در انتظار بخش</p>
<p>منما به جان دوست ما را ز دیده خط</p></div>
<div class="b" id="bn10"><div class="m1"><p>بشنو ز گلستان فریاد بلبلی</p></div>
<div class="m2"><p>بنگر به بوستان در ناله صلصلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر یک ز هر طرف افکنده غلغلی</p></div>
<div class="m2"><p>چند از الم پریش چون زلف سنبلی</p></div></div>
<div class="b2" id="bn12"><p>جا کن به طرف باغ در پای نوگلی</p>
<p>خو کن به یک دلی از سر بنه غبط</p></div>
<div class="b" id="bn13"><div class="m1"><p>هامون و باغ چون قصر خورنق است</p></div>
<div class="m2"><p>همچون بهشت گشت با فرو رونق است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>منصور غنچه را ذکر اناالحق است</p></div>
<div class="m2"><p>بردار شاخسار ز آن رو معلق است</p></div></div>
<div class="b2" id="bn15"><p>ما را به فضل گل عهدی موثق است</p>
<p>گیریم خامه باز سازیم خامه خط</p></div>
<div class="b" id="bn16"><div class="m1"><p>پس ابتدا کنیم در مدح شیر حق</p></div>
<div class="m2"><p>شاهی که شد سبب بر خلق ما خلق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دارد به جز نبی بر ماسوا سبق</p></div>
<div class="m2"><p>جوید عطارد از بهر ثناش رق</p></div></div>
<div class="b2" id="bn18"><p>طوبی شود قلم ارض و سما ورق</p>
<p>نتوان ز وصف او بنوشت نصف خط</p></div>
<div class="b" id="bn19"><div class="m1"><p>شاه ملک خدم ماه فلک جناب</p></div>
<div class="m2"><p>مسند نشین شرح مفتاح کل باب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر کن جن و انس بر جمله شیخ و شاب</p></div>
<div class="m2"><p>هم مرجع الانام هم مالک الرقاب</p></div></div>
<div class="b2" id="bn21"><p>زینب ده تراب یعنی ابوتراب</p>
<p>هم ایه نشاط هم باعث نشط</p></div>
<div class="b" id="bn22"><div class="m1"><p>نه اطلس سپهر عطف سرادقش</p></div>
<div class="m2"><p>قتال مارقین سوزان سقاسقش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>محروبه مطبخی است از قدر خافقش</p></div>
<div class="m2"><p>صدق و صفا نهان اندر تصادفش</p></div></div>
<div class="b2" id="bn24"><p>نبود روا که خواند مخلوق خالقش</p>
<p>خلاق و خلق را گنجیده در وسط</p></div>
<div class="b" id="bn25"><div class="m1"><p>فرزین عزم را روزی که زین کند</p></div>
<div class="m2"><p>در عرصه نبرد رو بهر کین کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کل جهات مات از کفر و دین کند</p></div>
<div class="m2"><p>بر بیرق و سوار پرچین جبین کند</p></div></div>
<div class="b2" id="bn27"><p>بر شاه و بر وزیر رو از کمین کند</p>
<p>دوران بدست اوست چون مهره وسط</p></div>
<div class="b" id="bn28"><div class="m1"><p>ای حصین دین حصین از دست تیغ تو</p></div>
<div class="m2"><p>حلال مشکلات نطق بلیغ تو</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>فیاض بحر و کان کف فریغ تو</p></div>
<div class="m2"><p>طفلی است عقل کل نزد نشیغ تو</p></div></div>
<div class="b2" id="bn30"><p>چون روح در مشام عطر نشیغ تو</p>
<p>بوی تو جانفزاست چون باده در فرط</p></div>
<div class="b" id="bn31"><div class="m1"><p>ای دست ذوالجلال ای نور لایزال</p></div>
<div class="m2"><p>در حیرتم چرا با این همه جلال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ماندی نو در نجف آسوده بی‌ملال</p></div>
<div class="m2"><p>تا شد به کربلا از لشگر ضلال</p></div></div>
<div class="b2" id="bn33"><p>بی‌سر حسین تو با محنت و کلال</p>
<p>اعدادی او تمام در عشرت و نشط</p></div>
<div class="b" id="bn34"><div class="m1"><p>یک تن نبرد جان ز آن دشت هولناک</p></div>
<div class="m2"><p>گویی که ابن سعد از حق نداشت باک</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کرد عترت تو را از تیغ کین هلاک</p></div>
<div class="m2"><p>تنها نشد حسین غلطان به خون و خاک</p></div></div>
<div class="b2" id="bn36"><p>هر گوشه گل رخی گردید چاک چاک</p>
<p>هر جا سمنبری افتاد سبز خط</p></div>
<div class="b" id="bn37"><div class="m1"><p>شاها جهان چنین کی بی‌حساب بود</p></div>
<div class="m2"><p>بر عترت رسول کی ظلم باب بود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شط فرات اگر غلطان ز آب بود</p></div>
<div class="m2"><p>در دیده سحین موج سراب بود</p></div></div>
<div class="b2" id="bn39"><p>سیراب وحش و طیرو دل او کباب بود</p>
<p>عطشان شهید گشت آخر به نزد شط</p></div>
<div class="b" id="bn40"><div class="m1"><p>رو به قتل شیر شاهان دلیر شد</p></div>
<div class="m2"><p>بی سر حینیت از شمر شریر شد</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ظلمی به زینت از چرخ پیر شد</p></div>
<div class="m2"><p>کز جان و از جان یکباره سیر شد</p></div></div>
<div class="b2" id="bn42"><p>در دست شامیان زار و اسیر شد</p>
<p>اندوه وی گذشت ز اندازه شط</p></div>
<div class="b" id="bn43"><div class="m1"><p>ای دهر این چنین رسم وفا نبود</p></div>
<div class="m2"><p>ای آسمان سم اینسان روا نبود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>این ظلمبر حسین بالله بجا نبود</p></div>
<div class="m2"><p>از روی مصطفی چونت حیا نبود</p></div></div>
<div class="b2" id="bn45"><p>این انقتام اگر روز جزا نبود</p>
<p>(صامت) چه می‌گذشت بر ما از این سخط</p></div>