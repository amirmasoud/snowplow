---
title: >-
    شمارهٔ ۱۰ - در توحید و مدح ائمه طاهرین علیهم السلام
---
# شمارهٔ ۱۰ - در توحید و مدح ائمه طاهرین علیهم السلام

<div class="b" id="bn1"><div class="m1"><p>افراخت علم پادشه گل به چمن باز</p></div>
<div class="m2"><p>فردوس صفت گشت همه تل و دمن باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل به چمن شور در افکنده چون من باز</p></div>
<div class="m2"><p>باد سحری آمده در ملک ختن باز</p></div></div>
<div class="b2" id="bn3"><p>وز مشک ختن کرده جوان دیر کهن باز</p>
<p>گلزار و گلستان همه شد احمر و ارقط</p></div>
<div class="b" id="bn4"><div class="m1"><p>برخیز ز جا ساقیکا چیست تعلل</p></div>
<div class="m2"><p>بردار نوا مطربکا چند تحمل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می ده به قدح ساقیکا چیست تسلسل</p></div>
<div class="m2"><p>هی ساز نماز مطربکا نغمه چو بلبل</p></div></div>
<div class="b2" id="bn6"><p>بی عیش و طرق حیف بود وقت گل و مل</p>
<p>کو ساقی و کو ساغر و کو مطرب و بربط</p></div>
<div class="b" id="bn7"><div class="m1"><p>از قعهده سر کرده غزل کبک به کهسار</p></div>
<div class="m2"><p>وز باغ رسد ناله قمری به غم سار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اندر شکرستان شده طوطی شکر خار</p></div>
<div class="m2"><p>چون بلبل شوریده خوش آهنگ به گلزار</p></div></div>
<div class="b2" id="bn9"><p>سارنج چو ععق بودش شور و نواکار</p>
<p>از سره و سرخاب شنو طنطنه بط</p></div>
<div class="b" id="bn10"><div class="m1"><p>در فیض کف ابر ببین بخش بی‌حد</p></div>
<div class="m2"><p>کز جوهری قدرت خود قادر سرمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پر کرد مر این حقه اغبر ز زبرجد</p></div>
<div class="m2"><p>یاقوت و در و لعل و گهر زمرد و بسند</p></div></div>
<div class="b2" id="bn12"><p>حق باد نگهدار جهان از نظر بد</p>
<p>از مند مطرا شد و از بس که منشط</p></div>
<div class="b" id="bn13"><div class="m1"><p>سوسن چو من اندر چمن حمد الهی</p></div>
<div class="m2"><p>گویاست به شکر نعم نامتناهی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن شاه که بخشد به جهان افسر شاهی</p></div>
<div class="m2"><p>هر ذره به جود و کرمش داد گواهی</p></div></div>
<div class="b2" id="bn15"><p>چتر شبهی افراشته از ماه به ماهی</p>
<p>خلاق سفید و سیه و پیر و محطط</p></div>
<div class="b" id="bn16"><div class="m1"><p>اکنون زنم از نعت رسول عربی دم</p></div>
<div class="m2"><p>گویا شوم از مدحث پیغمبر اکرم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مقصود ز ایجاد همه عالم و آدم</p></div>
<div class="m2"><p>بدر ز من و صدر رسولان مکرم</p></div></div>
<div class="b2" id="bn18"><p>با واو وجودش شده چون میم عدم خم</p>
<p>شد عالم ایجاد یکی قطره از آن شط</p></div>
<div class="b" id="bn19"><div class="m1"><p>بر نعت نبی مدح علی باز کنم وصل</p></div>
<div class="m2"><p>از بعد پیمبر جو وصی است بلافصل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از رحمت دادار دو شاخند ز یک اصل</p></div>
<div class="m2"><p>از مدحت او چار کتابست به یک فصل</p></div></div>
<div class="b2" id="bn21"><p>اسلام ز صمصامش پاینده شد و حصل</p>
<p>نی ممکن و نی واجب بل آمده اوسط</p></div>
<div class="b" id="bn22"><div class="m1"><p>پس حضرت زهرا گل گلزار نبوت</p></div>
<div class="m2"><p>آن کوست به بیدای جهان لاله رحمت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آن سرو چمان چمن عفت و عصمت</p></div>
<div class="m2"><p>خورشید حیا شمع شبستان امامت</p></div></div>
<div class="b2" id="bn24"><p>گر او نبود شافعه روز قیامت</p>
<p>پس خامه تقدیر به جرم که کشد خط</p></div>
<div class="b" id="bn25"><div class="m1"><p>زان بعد حسن عامر معموره تعلیم</p></div>
<div class="m2"><p>سبط نبی و سر نبی معدن تحلیم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اول سخن حرمت و دیباچه تحریم</p></div>
<div class="m2"><p>مجموعه والایی و مقصوره تعظیم</p></div></div>
<div class="b2" id="bn27"><p>پا تا بسر اندر ره حق آمده تسلیم</p>
<p>در صبر از آن روز ز خدا یافته سر خط</p></div>
<div class="b" id="bn28"><div class="m1"><p>از بعد حسن هست حسین سید اخیار</p></div>
<div class="m2"><p>آرام دل فاطمه و احمد مختار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شمع شهداء نور هدی حامل اسرار</p></div>
<div class="m2"><p>مقصود ز ایجاد بهشت و غرض نار</p></div></div>
<div class="b2" id="bn30"><p>قندیل ملک را همه در بارگهش بار</p>
<p>جبریل امین را در او منزل و مهبط</p></div>
<div class="b" id="bn31"><div class="m1"><p>پس سید سجاد که از فرط تهجد</p></div>
<div class="m2"><p>شد ختم بر او نامه توفیق و تعبد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در زهد بود سرور اقلیم تزهد</p></div>
<div class="m2"><p>قائم به قیام و به قعود و به تشهد</p></div></div>
<div class="b2" id="bn33"><p>با نفس همه عمر به خصمی و تجاهد</p>
<p>بر دیده شیطان ز عبادت زده مشرط</p></div>
<div class="b" id="bn34"><div class="m1"><p>پس مخزن علم نبوی حضرت باقر</p></div>
<div class="m2"><p>از لوث معاصی چو پدر طیب و طاهر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر شرع نبی از دل و جان حافظ و ناصر</p></div>
<div class="m2"><p>دانای علوم و حکم باطن و ظاهر</p></div></div>
<div class="b2" id="bn36"><p>از تیغ زبان بر همه کس قالب و قاهر</p>
<p>وز سابقه بر هر چه سلیطه است مسلط</p></div>
<div class="b" id="bn37"><div class="m1"><p>س جعفر صادق لقب آل محمد</p></div>
<div class="m2"><p>شمس فلک قدر و گل گلشن احمد</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چون والد خود ماجدود چون جد خود امجد</p></div>
<div class="m2"><p>در مرتبه نور نظر ابیض و اسود</p></div></div>
<div class="b2" id="bn39"><p>پنهان به طواف در او شاهی سرمد</p>
<p>مفتاح یقین داعی دین ماهی مسقط</p></div>
<div class="b" id="bn40"><div class="m1"><p>زان بعد بود موسی کاظم که کظیم است</p></div>
<div class="m2"><p>همروی کلام الله و همنام کلیم است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در انفس و آفاق قوام است و قدیمست</p></div>
<div class="m2"><p>احیا چومسیحا ز دمش عظم رمیم است</p></div></div>
<div class="b2" id="bn42"><p>حادث بود اما بنظر مثل قدیم است</p>
<p>نی نی قدم اینجا غلط است و حدث اغلط</p></div>
<div class="b" id="bn43"><div class="m1"><p>اکنون بسرایم سخن از قبله هفتم</p></div>
<div class="m2"><p>هرچند که بیرون بود از فکر و توهم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در وادی عقلش شده عقل عقلا کم</p></div>
<div class="m2"><p>آری چه فزاید اثر قطره بقلزم</p></div></div>
<div class="b2" id="bn45"><p>از مدح رضا بست خرد لب ز تکلم</p>
<p>ختم سخن از مدح جنابش شده احوط</p></div>
<div class="b" id="bn46"><div class="m1"><p>اکنون بتقی باز کنم دست تضرع</p></div>
<div class="m2"><p>ظاهر کنم از دوستش رسم تشیع</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دارای سخامندی و کالای تمتع</p></div>
<div class="m2"><p>ملک و ملک از هیبتش ایمن ز تزعرع</p></div></div>
<div class="b2" id="bn48"><p>از کف جوادش شده بنیاد تبرع</p>
<p>در محکه‌اش پیر خرد طفل مقمط</p></div>
<div class="b" id="bn49"><div class="m1"><p>زان بعد نقی شمه ایوان نقابت</p></div>
<div class="m2"><p>آن در گرانمایه دریای سخاوت</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در حل عقود است چو احمد به کذاوت</p></div>
<div class="m2"><p>در جهد جهود است چو حیدر به قضاوت</p></div></div>
<div class="b2" id="bn51"><p>مهرش مطلب از دل پر بغض و قساوت</p>
<p>او را چه کند مبغض مجهول مخبط</p></div>
<div class="b" id="bn52"><div class="m1"><p>دیگر حسن عسگری آن شاه فلک جاه</p></div>
<div class="m2"><p>کش عسگر نصرب بود از ماهی تا ماه</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>قطب فلک حشمت منصور من الله</p></div>
<div class="m2"><p>جبریل امینش شرف حاجب درگاه</p></div></div>
<div class="b2" id="bn54"><p>در ملک عبودیت و در کون و ملکان شاه</p>
<p>بر کسوت وی همت وی آمده مخیط</p></div>
<div class="b" id="bn55"><div class="m1"><p>پس حضرت حجه خلف صدق پیمبر</p></div>
<div class="m2"><p>دلبند علی فاطمه عصمت حسنین فر</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>سجاد سخا جان و دل باقر و جعفر</p></div>
<div class="m2"><p>چون موسی و مانند رضا سید و سرور</p></div></div>
<div class="b2" id="bn57"><p>همشان نقی و تقی و همسر عسکر</p>
<p>(صامت) به امامت بنما ختم مسمط</p></div>