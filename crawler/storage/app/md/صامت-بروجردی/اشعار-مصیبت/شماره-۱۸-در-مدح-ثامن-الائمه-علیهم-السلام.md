---
title: >-
    شمارهٔ ۱۸ - در مدح ثامن الائمه علیهم‌السلام
---
# شمارهٔ ۱۸ - در مدح ثامن الائمه علیهم‌السلام

<div class="b" id="bn1"><div class="m1"><p>ماهرخا ابتدای فصل بهار است</p></div>
<div class="m2"><p>وقت گل و استماع صورت هزار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که گل سرخ پیش روی تو خار است</p></div>
<div class="m2"><p>صفحه گیتی تمام نقش و نگار است</p></div></div>
<div class="b2" id="bn3"><p>از بس کز باغ و راغ سبزه دمیده</p></div>
<div class="b" id="bn4"><div class="m1"><p>تا عنبی می‌دهید پیاله پیاله</p></div>
<div class="m2"><p>باده شبنم ببین به ساغر لاله</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روزی مستان شده به باغ حواله</p></div>
<div class="m2"><p>سنبل بویا پریش کرده کلاله</p></div></div>
<div class="b2" id="bn6"><p>شاخه گل زیر بار غنچه خمیده</p></div>
<div class="b" id="bn7"><div class="m1"><p>کرده ز کلک بدیع مبدع اشیا</p></div>
<div class="m2"><p>نقش عجبی بکار دیبه صحرا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کارگه چین شده است دشت سراپا</p></div>
<div class="m2"><p>باز در اطراف کوه نرگس شهلا</p></div></div>
<div class="b2" id="bn9"><p>سرمه به چشم سیاه خویش کشیده</p></div>
<div class="b" id="bn10"><div class="m1"><p>تاج تبارک نهاد مرغ سلیمان</p></div>
<div class="m2"><p>کبک چو در اج در ترانه و افغان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قمری افسرده در چین چو هزاران</p></div>
<div class="m2"><p>بر سر سرو سهی به نغمه والحان</p></div></div>
<div class="b2" id="bn12"><p>شاخ به شاخ از سر نشاط پریده</p></div>
<div class="b" id="bn13"><div class="m1"><p>اصلحک الله ای نگار پری روی</p></div>
<div class="m2"><p>ای صنم مشکمو بساخت مشگو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شو به سیاحت به سیر باغ و لب جو</p></div>
<div class="m2"><p>تا من و تو در مدیح ضامن آهو</p></div></div>
<div class="b2" id="bn15"><p>ذکر مسمط کنیم و فکر قصیده</p></div>
<div class="b" id="bn16"><div class="m1"><p>قبله هفتم امام هشتم شیعه</p></div>
<div class="m2"><p>ماه کواکب سپاه مهر طلیعه</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مظهر حق اصل دین معین شریعه</p></div>
<div class="m2"><p>آنکه ز حکمش بود برسم ودیعه</p></div></div>
<div class="b2" id="bn18"><p>روح در اجسام خلق و نور به دیده</p></div>
<div class="b" id="bn19"><div class="m1"><p>خسرو بطحا خدیو خطه امکان</p></div>
<div class="m2"><p>شاه مدینه پناه ملک خراسان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آنکه به جای کلام ایزد منان</p></div>
<div class="m2"><p>همره او بوده هر چه موسی عمران</p></div></div>
<div class="b2" id="bn21"><p>در جبل طور گفته یا که شنیده</p></div>
<div class="b" id="bn22"><div class="m1"><p>ای که به ادریس در مدارس تجرید</p></div>
<div class="m2"><p>شخص تو آموخته مباحث توحید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کنز خفی را هدایت تو مقالید</p></div>
<div class="m2"><p>خوشه توحید جبرئیل به تقلید</p></div></div>
<div class="b2" id="bn24"><p>در ازل از خرمن جلال تو چیده</p></div>
<div class="b" id="bn25"><div class="m1"><p>زاده موسی بن جعفر ای علوی جاه</p></div>
<div class="m2"><p>ای که شب و روز زائر تو به درگاه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ساخته درک مقام سیر الی الله</p></div>
<div class="m2"><p>سهل بود در زیارت تو اگر راه</p></div></div>
<div class="b2" id="bn27"><p>با مژه سازند طی راه به عیده</p></div>
<div class="b" id="bn28"><div class="m1"><p>ای که ملق ز کبریا به رضایی</p></div>
<div class="m2"><p>مامن اشرف ضامن غربائی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>حجت الاسلام و کعبه فقرایی</p></div>
<div class="m2"><p>چون به صفت مظهر جمال خدایی</p></div></div>
<div class="b2" id="bn30"><p>عقل بکنه جلال تو نرسیده</p></div>
<div class="b" id="bn31"><div class="m1"><p>نور رخت شعله داده شمع هدا را</p></div>
<div class="m2"><p>علم تو ظاهر نموده دین خدا را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>خضر ز جوی تو برده آب بقا را</p></div>
<div class="m2"><p>پنجه قدرت قبای صبر و صفا را</p></div></div>
<div class="b2" id="bn33"><p>به رقد زیبا و قامت تو بریده</p></div>
<div class="b" id="bn34"><div class="m1"><p>سیر مقام تو را به سرعت بسیار</p></div>
<div class="m2"><p>گر که شده جبرنئیل و هم طلبکار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>راه نبرده به سوی بارگه داد</p></div>
<div class="m2"><p>بال و پرویر فتد ز کار به یک باز</p></div></div>
<div class="b2" id="bn36"><p>صد ره اگر بر فراز سدره دویده</p></div>
<div class="b" id="bn37"><div class="m1"><p>صبح به روی تو کرده کسب شفق را</p></div>
<div class="m2"><p>آیت عظمی رخ تو رب فلق را</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>لیل و نها از تو جسته نظم نسق را</p></div>
<div class="m2"><p>فر خدا معنی هویت حق را</p></div></div>
<div class="b2" id="bn39"><p>چشم دل از روی حق نمای تو دیده</p></div>
<div class="b" id="bn40"><div class="m1"><p>خاک خراسان ز مقدم تو بهشت است</p></div>
<div class="m2"><p>بلکه بر روضه‌ات بهشت کنشت است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>در بر چشم کسی که پاک سرشت است</p></div>
<div class="m2"><p>بهر رواقت بهشت نسبت زشت است</p></div></div>
<div class="b2" id="bn42"><p>آن که شنیده چه سود آنکه ندیده</p></div>
<div class="b" id="bn43"><div class="m1"><p>ای شرف و اشرف نتایج آدم</p></div>
<div class="m2"><p>شادی احباب در عزای تو ماتم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>ماه صفر را غم تو کرده محرم</p></div>
<div class="m2"><p>بهر تو ای نور چشم آدم و عالم</p></div></div>
<div class="b2" id="bn45"><p>خون ز فلک جای اشک دیده چکیده</p></div>
<div class="b" id="bn46"><div class="m1"><p>آنکه ز شهر مدینه در به درت کرد</p></div>
<div class="m2"><p>سوی خراسان مصمم سفرت کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از شرر زهر پر ز خون جگرت کرد</p></div>
<div class="m2"><p>بستر تو خاک و خشت جای سرت کرد</p></div></div>
<div class="b2" id="bn48"><p>پرده شرم و حیای خویش دریده</p></div>
<div class="b" id="bn49"><div class="m1"><p>چون اثر زهر در تن تو عیان شد</p></div>
<div class="m2"><p>جسم عزیزت ز درد سیر ز جان شد</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خاک عزایت بهفرق کون و مکان شد</p></div>
<div class="m2"><p>نور الهی خموش گشت و عیان شد</p></div></div>
<div class="b2" id="bn51"><p>قدر تو با این صفات و ذات حمیده</p></div>
<div class="b" id="bn52"><div class="m1"><p>خواهر زارت بسر نبود به دوران</p></div>
<div class="m2"><p>در دم مرگ تو ای غریب خاک خراسان</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا نگرد حال تو به دیده گریان</p></div>
<div class="m2"><p>زین طرف و آن طرف تنت شده غلطان</p></div></div>
<div class="b2" id="bn54"><p>لحظه به لحظه چو شخص مارگزیده</p></div>
<div class="b" id="bn55"><div class="m1"><p>باز غم و غصه کرد روز مرا شب</p></div>
<div class="m2"><p>سوخت دل من به حالت دل زینب</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نداشت چه حالی به قتلگاه که یارب</p></div>
<div class="m2"><p>دید ز نعل سمند و از سم مرکب</p></div></div>
<div class="b2" id="bn57"><p>جسم حسین را به خون و خاک طپیده</p></div>
<div class="b" id="bn58"><div class="m1"><p>صورت اطفال ناز پرور غمگین</p></div>
<div class="m2"><p>روی یتیمان آل عترت یاسین</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نیلی از دست شمر کافر بی‌دین</p></div>
<div class="m2"><p>هر یکی از یک طرف به دیده خونین</p></div></div>
<div class="b2" id="bn60"><p>رو به بیابان به پای خار خزیده</p></div>
<div class="b" id="bn61"><div class="m1"><p>عصمت حق عترت رسول گرامی</p></div>
<div class="m2"><p>گشته ذلیل و اسیر و کوفی و شامی</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>گوئی آل رسول را به تمامی</p></div>
<div class="m2"><p>بهر کنیزی از برای غلامی</p></div></div>
<div class="b2" id="bn63"><p>نسل معاویه پلید خریده</p></div>
<div class="b" id="bn64"><div class="m1"><p>عارض زینب که بود مهر نقابش</p></div>
<div class="m2"><p>موی پریشان به چهره گشت حجابش</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>غصه نامحرمان و بزم شرابش</p></div>
<div class="m2"><p>زد به جهان آنچنان به قلب کبابش</p></div></div>
<div class="b2" id="bn66"><p>شعله که مرگ خود از خدا طبیده</p></div>
<div class="b" id="bn67"><div class="m1"><p>ای شه طوسی که ساخته ره دورت</p></div>
<div class="m2"><p>دور مرا از حضور بزم سرورت</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>ای تو سلیمان و ماسوا همه مورت</p></div>
<div class="m2"><p>گوی به خدام آستان حضورت</p></div></div>
<div class="b2" id="bn69"><p>گفته (صامت) کننده ثبت جریده</p></div>