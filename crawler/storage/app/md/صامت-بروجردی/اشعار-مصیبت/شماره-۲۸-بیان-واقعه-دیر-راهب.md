---
title: >-
    شمارهٔ ۲۸ - بیان واقعه دیر راهب
---
# شمارهٔ ۲۸ - بیان واقعه دیر راهب

<div class="b" id="bn1"><div class="m1"><p>چون حریم خسرو بطحا ز بیداد زمانه</p></div>
<div class="m2"><p>سوی شام از کربلا بهر اسیری شد روانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جملگی چون طایر پر بسته بی‌آشیانه</p></div>
<div class="m2"><p>در یکی منزل مکان کردند هنگام شبانه</p></div></div>
<div class="b2" id="bn3"><p>بر در دیر نصاری به افغان و اضطرابی</p></div>
<div class="b" id="bn4"><div class="m1"><p>راهبی می‌بود در آن دیر اندر کیش عیسی</p></div>
<div class="m2"><p>طالب طور تجلی سالها مانند موسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جا پی گنج حقیقت کرده در کنج کلیسا</p></div>
<div class="m2"><p>جذبه نور حسینی شد دلیل مرد ترسا</p></div></div>
<div class="b2" id="bn6"><p>دید شد بر پا به دور دیر شور و انقلابی</p></div>
<div class="b" id="bn7"><div class="m1"><p>لشگر خونخوار جراری بدد از حصر بیرون</p></div>
<div class="m2"><p>نیزه‌ها بر دست زیب نیزه‌ها سرهای پرخون</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر سری از نور چهر آتش زده بر ماه گردون</p></div>
<div class="m2"><p>چند زن با دختر منتظم چون در مکنون</p></div></div>
<div class="b2" id="bn9"><p>در پی هر نیزه با دست بسته در طنابی</p></div>
<div class="b" id="bn10"><div class="m1"><p>رفت راهب را از این هنگامه هوش از سر ز تن تاب</p></div>
<div class="m2"><p>گفت یا رب این به بیداریست بینم یا که در خواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صبح محشر گشته ظاهر در جهان گویا از این باب</p></div>
<div class="m2"><p>آفتاب است از زمین یک نی بلند از امر وهاب</p></div></div>
<div class="b2" id="bn12"><p>ورنه هرگز بر سر نی کس ندیده آفتابی</p></div>
<div class="b" id="bn13"><div class="m1"><p>این زنان موپریشان غریب تیره کوکب</p></div>
<div class="m2"><p>کیستند و از برای چیست روز جمله چون شب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از چه رو دارند ذکر واحسینا جمله بر لب</p></div>
<div class="m2"><p>هادی من شوبجاه و قرب روح الله یا رب</p></div></div>
<div class="b2" id="bn15"><p>برگشا از بهر من از سر این اسرار بابی</p></div>
<div class="b" id="bn16"><div class="m1"><p>پس از بام دیر نصرانی به قلب پرتلاطم</p></div>
<div class="m2"><p>بر زمین گردید نال چون مسیح ار چرخ چارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گفت ای قوم شده از راه و رسم مردمی گم</p></div>
<div class="m2"><p>این چه آشوبست این سیر کیست ای بی‌رحم مردم</p></div></div>
<div class="b2" id="bn18"><p>کس ندیده گوش نشنیده چنین ظلم و عذابی</p></div>
<div class="b" id="bn19"><div class="m1"><p>گفت با او ظالمی زان ناکسان زشت ابتر</p></div>
<div class="m2"><p>هست این سر از حسین بن علی سبط پیمبر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر امیر شام یاغی گشت و شد لب تشنه بی‌سر</p></div>
<div class="m2"><p>این اسیراناهل بیت او بود از بهر کیفر</p></div></div>
<div class="b2" id="bn21"><p>سوی شام آورده‌ایم از کوفه با چنگ و ربابی</p></div>
<div class="b" id="bn22"><div class="m1"><p>ریخت نصرانی به دامن گوهر از دریای دیده</p></div>
<div class="m2"><p>گفت ای قوم ز کف دین داده و دنیا خریده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کز طمع پیوسته با شیطان و از یزدان بریده</p></div>
<div class="m2"><p>چند بدر زر ز میراث پدر بر من رسده</p></div></div>
<div class="b2" id="bn24"><p>می‌دهم این زر که سردار شما سازد ثوابی</p></div>
<div class="b" id="bn25"><div class="m1"><p>این سر ببریده را امشب نهد اندر بر من</p></div>
<div class="m2"><p>در زمان کوچ تسلیمش کنم بر وجه احسن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مرغ روح شمر زد از وعده زربال بر تن</p></div>
<div class="m2"><p>داد سر زر را گرفت از راهب پاکیزه دامن</p></div></div>
<div class="b2" id="bn27"><p>دیده گریان برد سوی دیر سر را باشتابی</p></div>
<div class="b" id="bn28"><div class="m1"><p>هاتفی در گوش وی داد این ندای روح افزا</p></div>
<div class="m2"><p>کای مسیحا ساختی از خود رضا روح مسیحا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سودها از بهر این سودا نصیبت شد ز یکتا</p></div>
<div class="m2"><p>راهب پاکیزه سیرت راس نور چشم زهرا</p></div></div>
<div class="b2" id="bn30"><p>شست و جا در معبد خود داد با مشک و گلابی</p></div>
<div class="b" id="bn31"><div class="m1"><p>رفت اندر گوشه‌ای آن مرد نصرانی نهان شد</p></div>
<div class="m2"><p>دید بعد از لحظه‌ای هنگامه کبری عیان شد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از خروش واحسینا لرزه بر کون و مکان شد</p></div>
<div class="m2"><p>با ندای طرقوا سوی زمین از آسمان شد</p></div></div>
<div class="b2" id="bn33"><p>شش زن معجر سیه در ناله یا قلب کبابی</p></div>
<div class="b" id="bn34"><div class="m1"><p>ساره و مریم، صفورا، آسیه، حوا و هاجر</p></div>
<div class="m2"><p>حلقه ماتم زدند از گریه در اطراف آن سر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عرش و فرش افتاد از نور در تزلزل بار دیگر</p></div>
<div class="m2"><p>از فلک آمد خدیجه بر سر آن راس انوار</p></div></div>
<div class="b2" id="bn36"><p>شد زمین از اشک وی چون بر سر دریا حبابی</p></div>
<div class="b" id="bn37"><div class="m1"><p>ناگهان آمدند ابر گوش آن راهب دوباره</p></div>
<div class="m2"><p>می‌رسد زهرای اطهر چشم بر بند از نظاره</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چشم حق بین را به هم بنهاد راهب زان اشاره</p></div>
<div class="m2"><p>لیک می‌آمد به گوش وی از آن دارالزیاره</p></div></div>
<div class="b2" id="bn39"><p>ناله زار و حزینی از دل پرپیچ و تابی</p></div>
<div class="b" id="bn40"><div class="m1"><p>با فغان می‌گفت ای شاهنشه بی‌سر حسینم</p></div>
<div class="m2"><p>از قفا ببرید سر سلطان بی‌لشکر حسینم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>زیب پیکر زینب آغوش پیغمبر حسینم</p></div>
<div class="m2"><p>کشته بی‌یار غمخوار و الم‌پرورم حسینم</p></div></div>
<div class="b2" id="bn42"><p>از چه‌ای مظلوم با مادر نمی‌گویی جوابی</p></div>
<div class="b" id="bn43"><div class="m1"><p>ای غریب کشته بی‌غسل و کفن کو پیکر تو</p></div>
<div class="m2"><p>کو علمدار و سپه کو اکبر و کو اصغر تو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کو ستمکش زینب آواره غم‌پرور تو</p></div>
<div class="m2"><p>محنت دوران چه آورده است ای سر بر سر تو</p></div></div>
<div class="b2" id="bn45"><p>گه به مطبخ گه بنی گه دیرو گه بزم شرابی</p></div>
<div class="b" id="bn46"><div class="m1"><p>رفت نصرانی ز هوش از ناله جان‌سوز و زهرا</p></div>
<div class="m2"><p>چون به هوش آمد کسیر ازان زنان نادید برجا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نزد آن سر گفت و در عین ادب استاد برپا</p></div>
<div class="m2"><p>ایهاالراس المبارک ای عزیز فرد یکتا</p></div></div>
<div class="b2" id="bn48"><p>تو کدامین سرفرازی سرور عالی جنابی</p></div>
<div class="b" id="bn49"><div class="m1"><p>گفت ای راهب من مظلوم سبط مصطفایم</p></div>
<div class="m2"><p>مادرم زهرای اطهر خود حسین سر جدایم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>در منای نینوا قربانی راه خدایم</p></div>
<div class="m2"><p>تشنه لب سر داده اندر راه حق در کربلایم</p></div></div>
<div class="b2" id="bn51"><p>نیست ای راهب غم و درد مرا حد و حسابی</p></div>
<div class="b" id="bn52"><div class="m1"><p>بر دل راهب دگر طاقت نماند از گفتگویش</p></div>
<div class="m2"><p>زد بسر دست عزا بنهاد روی خود برویش</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>کرد روی خویشتن را سرخ از خون گلویش</p></div>
<div class="m2"><p>از ادب زد بوسه بر پمرده لبهای نکویش</p></div></div>
<div class="b2" id="bn54"><p>با تضرع نزد آن سر کرد عجز و اضطرابی</p></div>
<div class="b" id="bn55"><div class="m1"><p>گفت شاها بر ندارم دست امیدت ز دامن</p></div>
<div class="m2"><p>تا نگویی در قیامت شافع تو می‌شوم من</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گفت بیرون کن دگر زنا راهب ز گردن</p></div>
<div class="m2"><p>شو مسلمان تا شفیع تو شوم در پیش ذوالمن</p></div></div>
<div class="b2" id="bn57"><p>همچو (صامت) روز محر از وصالم کامیابی</p></div>