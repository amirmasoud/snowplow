---
title: >-
    شمارهٔ  ۳ - نوحه دیگر
---
# شمارهٔ  ۳ - نوحه دیگر

<div class="b" id="bn1"><div class="m1"><p>آه که صد پاره جگر شد حسن</p></div>
<div class="m2"><p>دارد ز زن داد از بیداد زن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهر معاویه کافر ز سر</p></div>
<div class="m2"><p>کرد جهان را همه بیت الحزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیر خدا پادشه لو کشف</p></div>
<div class="m2"><p>جانب یثرب بشتاب از نجف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمده با فوج ملک صف به صف</p></div>
<div class="m2"><p>ورد زبان کرده همه با اسف</p></div></div>
<div class="b2" id="bn5"><p>آه که صدپاره جگر شد حسن</p></div>
<div class="b" id="bn6"><div class="m1"><p>گمشده از عرش برین گوشوار</p></div>
<div class="m2"><p>غم شده با احمد مختار یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جانب جبریل امین گوش دار</p></div>
<div class="m2"><p>گرید و گوید ز الم زار زار</p></div></div>
<div class="b2" id="bn8"><p>آه که صد پاره جگر شد حسن</p></div>
<div class="b" id="bn9"><div class="m1"><p>بوالبشر از خجلت خیرالبشر</p></div>
<div class="m2"><p>بر سر زانو بنهاده است سر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوح از این داغ شده نوحه گر</p></div>
<div class="m2"><p>گرید و گوید بدو چشمان تر</p></div></div>
<div class="b2" id="bn11"><p>آه که صد پاره جگر شد حسن</p></div>