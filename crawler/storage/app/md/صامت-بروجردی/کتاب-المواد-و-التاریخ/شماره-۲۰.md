---
title: >-
    شمارهٔ  ۲۰
---
# شمارهٔ  ۲۰

<div class="b" id="bn1"><div class="m1"><p>تا میل دل از مهر جهان کم نشود</p></div>
<div class="m2"><p>اسباب سعادتی فراهم نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او در پی ناکامی و ما طالب کام</p></div>
<div class="m2"><p>سودای دو کج حساب با هم نشود</p></div></div>