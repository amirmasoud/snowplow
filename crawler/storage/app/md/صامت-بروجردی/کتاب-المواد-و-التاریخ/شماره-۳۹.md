---
title: >-
    شمارهٔ  ۳۹
---
# شمارهٔ  ۳۹

<div class="b" id="bn1"><div class="m1"><p>یا رب اگر از اهل گناهم چه کنم</p></div>
<div class="m2"><p>یا عاصیم و گمشده راهم چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاشا نتوان نمود خود می‌دانم</p></div>
<div class="m2"><p>مردود و لئیم و روسیاهم چه کنم</p></div></div>