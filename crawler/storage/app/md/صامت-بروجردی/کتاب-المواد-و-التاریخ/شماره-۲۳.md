---
title: >-
    شمارهٔ  ۲۳
---
# شمارهٔ  ۲۳

<div class="b" id="bn1"><div class="m1"><p>دنیا به کسی خط امانی نسپرد</p></div>
<div class="m2"><p>هرکس که بزدا عاقبت باید مرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سنگ که نام نامی وی اجلست</p></div>
<div class="m2"><p>بر شیشه عمر همه کس خواهد خورد</p></div></div>