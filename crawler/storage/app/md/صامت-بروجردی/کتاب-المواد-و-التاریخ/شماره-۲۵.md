---
title: >-
    شمارهٔ  ۲۵
---
# شمارهٔ  ۲۵

<div class="b" id="bn1"><div class="m1"><p>آن قوم کز الفت جهان خرسندند</p></div>
<div class="m2"><p>بر دوستیش چگونه دل می‌بندند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنان که بر آدمی سر افراز شدند</p></div>
<div class="m2"><p>دندان محبت جهان را کندند</p></div></div>