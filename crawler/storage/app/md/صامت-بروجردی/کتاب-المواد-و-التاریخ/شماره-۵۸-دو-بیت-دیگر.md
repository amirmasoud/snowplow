---
title: >-
    شمارهٔ  ۵۸ - دو بیت دیگر
---
# شمارهٔ  ۵۸ - دو بیت دیگر

<div class="b" id="bn1"><div class="m1"><p>تا بود در دریده اشک امشب مجال خواب نیست</p></div>
<div class="m2"><p>خواب آید آن زمان در دیده کورا آب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم من شبهاست روشن از خیال روی تو</p></div>
<div class="m2"><p>خانه درویش را شمعی به از مهتاب نیست</p></div></div>