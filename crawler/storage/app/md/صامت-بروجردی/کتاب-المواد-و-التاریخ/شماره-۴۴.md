---
title: >-
    شمارهٔ  ۴۴
---
# شمارهٔ  ۴۴

<div class="b" id="bn1"><div class="m1"><p>جانا به من ارجو کنی افزون کن</p></div>
<div class="m2"><p>زین بیش مرا به عشق خود مفتون کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکرانه اینکه چون منی در دامت</p></div>
<div class="m2"><p>مرغان دیگر را ز قفس بیرون کن</p></div></div>