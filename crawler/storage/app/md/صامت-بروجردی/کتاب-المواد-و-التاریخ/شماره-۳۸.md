---
title: >-
    شمارهٔ  ۳۸
---
# شمارهٔ  ۳۸

<div class="b" id="bn1"><div class="m1"><p>یا رب خجل از نعمت و احسان توام</p></div>
<div class="m2"><p>من عاصی و مسحق غفران توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گلّه که باشد به سگی محتاج است</p></div>
<div class="m2"><p>من هم سگ گلّهٔ محبان توام</p></div></div>