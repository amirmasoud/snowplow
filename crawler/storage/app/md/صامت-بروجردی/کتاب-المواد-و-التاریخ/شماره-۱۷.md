---
title: >-
    شمارهٔ  ۱۷
---
# شمارهٔ  ۱۷

<div class="b" id="bn1"><div class="m1"><p>کس نیست که از زمانه خرسند شود</p></div>
<div class="m2"><p>جز اینکه به دام غصه دربند شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آدم که به غم دچار شد دانستم</p></div>
<div class="m2"><p>میراث پدر نصیب فرزند شود</p></div></div>