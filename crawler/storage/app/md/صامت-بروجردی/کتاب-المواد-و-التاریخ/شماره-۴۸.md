---
title: >-
    شمارهٔ  ۴۸
---
# شمارهٔ  ۴۸

<div class="b" id="bn1"><div class="m1"><p>خواهی به تمام سروران سر باشی</p></div>
<div class="m2"><p>آسوده ز گیر و دار محشر باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باید ز صفا و صدق و اخلاص و ادب</p></div>
<div class="m2"><p>خاک قدم آل پیمبر باشی</p></div></div>