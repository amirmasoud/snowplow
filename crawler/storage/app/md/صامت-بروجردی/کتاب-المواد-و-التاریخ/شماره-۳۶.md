---
title: >-
    شمارهٔ  ۳۶
---
# شمارهٔ  ۳۶

<div class="b" id="bn1"><div class="m1"><p>نه غزه به طاعت و نه ننگ و نامم</p></div>
<div class="m2"><p>خالی بود از می حقیقت جامم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسباب امیدواری من این است</p></div>
<div class="m2"><p>کامروز سواد لشگر اسلامم</p></div></div>