---
title: >-
    شمارهٔ  ۵۴ - خیز کاین جای تو نیست
---
# شمارهٔ  ۵۴ - خیز کاین جای تو نیست

<div class="b" id="bn1"><div class="m1"><p>نیست دردی که ز هر گوشه مهیای تو نیست</p></div>
<div class="m2"><p>خیز کاین جای تو نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمیر نیلی کند که از ظلم رخ دختر تو</p></div>
<div class="m2"><p>کودک مضطر تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر این سوخته دل دخت دل‌آرای تو نیست</p></div>
<div class="m2"><p>خیز کاین جای تو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنت امروز چنین سرمه صفت می‌نگرم</p></div>
<div class="m2"><p>خاک عالم به سرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خبر خواهرت از امشب و فردایت و نیست</p></div>
<div class="m2"><p>خیز کاین جای تو نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جز از چشم من و چشمه زخم بدنت</p></div>
<div class="m2"><p>جان به قربان تنت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون فشان چشم کسی بهر تماشای تو نیست</p></div>
<div class="m2"><p>خیز کاین جای تو نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غیر زنجیر که در بستن ما بسته کمر</p></div>
<div class="m2"><p>ای شه تشنه جگر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هیچکس نیست که دربند سروپای تو نیست</p></div>
<div class="m2"><p>خیز کاین جای تو نیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خسروا (صامت) محزون ز عزایت شب و روز</p></div>
<div class="m2"><p>گویند از ناله سوز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کارزوئی به دلم غیرتمنای تو نیست</p></div>
<div class="m2"><p>خیز کاین جای تو نیست</p></div></div>