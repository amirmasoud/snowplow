---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>تبسم لبت از لعل آبدارتر است</p></div>
<div class="m2"><p>ز فصل گل رخ خوب تو خوش بهارتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سمند تازی نازت به قلب های خراب</p></div>
<div class="m2"><p>ز رخش رستمی ای شوخ راهوارتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فساد زلف تو در جنت رخت همه جا</p></div>
<div class="m2"><p>ز مار و فتنه ابلیس آشکارتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پیش عقرب زلفت خوشم ولی چه کنم</p></div>
<div class="m2"><p>که چشم شوخ تو هر روز پایدارتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دشمنان نکنی آنچه می‌کنی با دوست</p></div>
<div class="m2"><p>چه شد که دوست ز دشمن بر تو خوارتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به سیر لاله مخوان (صامتا) ببستانم</p></div>
<div class="m2"><p>که چهره جگر از لاله داغدارتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیاه بختم و کس را خبر ز حالم نیست</p></div>
<div class="m2"><p>مگر کسی که ز من تیره روزگارتر است</p></div></div>