---
title: >-
    شمارهٔ ۵۹
---
# شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>ما از دو کون پای به دامن کشیده‌ایم</p></div>
<div class="m2"><p>در سایه محبت یاری خزیده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن بلبلیم ما که چو از بیضه درشدیم</p></div>
<div class="m2"><p>بر شاخسار زلف نکویان پریده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای باغبان برای گلی در بما مبند</p></div>
<div class="m2"><p>آخر نه ما به گلشن تو نو رسیده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مائیم در ازل که پیام الست را</p></div>
<div class="m2"><p>با گوش خویش از لب جانان شنیده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهد دگر حدیث زانهار و سلسبیل</p></div>
<div class="m2"><p>با ما بگو که طعم محبت چشیده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر چشم شیخ وسوسه آمد به روزگار</p></div>
<div class="m2"><p>نقشی که ما در آئینه جام دیده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی بط شراب بیاور که خسته‌ایم</p></div>
<div class="m2"><p>ما از عدم به ساحت امکان دویده‌ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشتر به روز مرگ چه باشد به ما کفن</p></div>
<div class="m2"><p>زان پیرهن که در شب هجران دریده‌ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش در خطای عشق غزالانه (صامتا)</p></div>
<div class="m2"><p>از دام کید زاهد و عابد رمیده‌ایم</p></div></div>