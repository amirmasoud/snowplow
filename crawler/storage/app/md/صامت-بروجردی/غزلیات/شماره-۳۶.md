---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>دلم بهانه رویت زیاد می‌گیرد</p></div>
<div class="m2"><p>به شوق وصل تو فال زیاد می‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخت هر آن چه ز عاشق‌کشی نمی‌داند</p></div>
<div class="m2"><p>ز چشم شوخ سیاه تو یاد می‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز روی تجربه مغروری از جهان غلطست</p></div>
<div class="m2"><p>که دهر هر چه به هر کس که داد می‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شاه کشور حسنی ولی عدالت کن</p></div>
<div class="m2"><p>که شاه مملکت از عدل و داد می‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به غیر رخ منما پیش دیده (صامت)</p></div>
<div class="m2"><p>که شعله غمش اندر نهاد می‌گیرد</p></div></div>