---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>خوبان اگر که منع نگاهی به ما کنند</p></div>
<div class="m2"><p>ما شکر می‌کنیم اگر اکتفا کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منت کشیم و ناز کشیم و ستم کشیم</p></div>
<div class="m2"><p>حاشا کنند و جور کنند و جفا کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهل است انتظار کشیدم تمام عمر</p></div>
<div class="m2"><p>کز صد هزار وعده یکی را وفا کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بالله که بهر کشتن ما عین خونبهاست</p></div>
<div class="m2"><p>خونی که غمزه‌های تواش زیر پا کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغی که ریخت بال و پرش در ته نفس</p></div>
<div class="m2"><p>کشتن نکوتر است گر او را رها کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد همچو روز حشر به جایی نمی‌رسد</p></div>
<div class="m2"><p>طورمار شکوه شب هجران چووا کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(صامت) من آن نیم که کشم پاز کوی دوست</p></div>
<div class="m2"><p>ور فی المثل که بند ز بندم جدا کنند</p></div></div>