---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>دگر دل بهر بدنامی ره تدبیر می‌گیرد</p></div>
<div class="m2"><p>جنون من خبر از ناله زنجیر می‌گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جهان آرزو را چون منی ناکام می‌یابد</p></div>
<div class="m2"><p>که بیخ بختم آب از شعله شمشیر می‌گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثر هم بهر آهم ادعای سرکشی دارد</p></div>
<div class="m2"><p>گمان کرده است نفرین ضعیفان دربرمی‌گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب کج بخت افتاده است آسایش که هر ساعت</p></div>
<div class="m2"><p>سر راهی ز دوری بر فراز شیر می‌گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراب عشق آبادی نمی‌فهمید بلی عاشق</p></div>
<div class="m2"><p>چسان بر دوش بار منت تعمیر می‌گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو روز عمر را ضایع مکن گویا نمی‌دانی</p></div>
<div class="m2"><p>که این دولت اجل هم از جوان هم پیر می‌گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سراپا می‌شود اندام (صامت) شعله آتش</p></div>
<div class="m2"><p>ز درد دل قلم چون از پی تحریر می‌گیرد</p></div></div>