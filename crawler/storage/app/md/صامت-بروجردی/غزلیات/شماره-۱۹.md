---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>ز خوب و زشت جهان یار ما به ما کافی است</p></div>
<div class="m2"><p>اگر وفا نکن با کسی جفا کافی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بی‌نشانیم ای روزگار خنده مکن</p></div>
<div class="m2"><p>که بهر سرزنشت نامی از هما کافی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا به دام تعلق فزون زبون منمای</p></div>
<div class="m2"><p>که خشت زیر سر و خاک زیر پا کافی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذشتم از سر و سامان کدخدایی دهر</p></div>
<div class="m2"><p>که کد به کار نیاید همان خدا کافی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به صم یک الهی افسر تو افساری است</p></div>
<div class="m2"><p>حمار نفس مرا بند از هوا کافی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر ز ناز لئیمان مرا کشی چه غم است</p></div>
<div class="m2"><p>که بی‌نیازیم از بهر خونبها کافی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سودی دیار فنا رهسپار شد (صامت)</p></div>
<div class="m2"><p>برادران نظر همت شما کافی است</p></div></div>