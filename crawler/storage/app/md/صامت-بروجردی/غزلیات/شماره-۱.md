---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آتش عشق کنون سوخت دیگر پیکر ما</p></div>
<div class="m2"><p>بعد از این تا چه کند باد به خاکستر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوس آزادی ما سر و صفت گشت بلند</p></div>
<div class="m2"><p>سوخت بابرق محبت همه بال و پر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌شود کشتی تن زود غریق یم اشک</p></div>
<div class="m2"><p>نشود سستی اندام اگر لنگرها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه نقشی نبود نقش کف پای نگار</p></div>
<div class="m2"><p>بر وای خاک تو خود راهنما همسر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موسم خرج معین شود و وقت حساب</p></div>
<div class="m2"><p>حالیا فاش بود قلبی سیم و زر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فلک از گردش وارونه مترسان ما را</p></div>
<div class="m2"><p>زآنکه از دفتر تو فرد شده اختر ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>(صامت) آسوده نشین در کف همت دوست</p></div>
<div class="m2"><p>که نبسته است کمر هیچ کس از کیفر ما</p></div></div>