---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>در سر کوی وفا عشاق را منزل یکی‌ست</p></div>
<div class="m2"><p>گر کم و بیشت تخم معرفت حاصل یکی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کن گذر در قتلگاه عشق او بنگر به خون</p></div>
<div class="m2"><p>هر قدم بس کشته‌ها افتاده و قاتل یکی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله گوناگون گر از دل می‌رسد نبود عجب</p></div>
<div class="m2"><p>هر سو موش ز تیری نالد و بسمل یکی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوی آن دلداده را کوغریب عشق اوست</p></div>
<div class="m2"><p>ره به پیش و پس مبر کاین بحر را ساحل یکی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جستجوی کعبه و بتخانه را مقصد تویی</p></div>
<div class="m2"><p>ساکنان مسجد و میخانه را مشکل یکی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که بینی نخل آهی کرده بر کیوان بلند</p></div>
<div class="m2"><p>بر سر کویت نپنداری که پا در گل یکی‌ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چند گویی بس نما افسانه عاشق نیستی</p></div>
<div class="m2"><p>در تمنای تو (صامت) را زبان با دل یکی‌ست</p></div></div>