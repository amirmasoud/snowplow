---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>ای خوش آن روز که دل به هر غمت جایی داشت</p></div>
<div class="m2"><p>سر سودازده با مهر تو سودایی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه سوز دلم از درد فراقت غم نیست</p></div>
<div class="m2"><p>کاشکی شام غمت و عده فردایی داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت از دامن مقصود مکش دست ای کاش</p></div>
<div class="m2"><p>دل شوریده من تاب و توانای داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خانه بر دوش کسی یاد ندارد چون من</p></div>
<div class="m2"><p>باز مجنون به جهان گوشه صحرایی داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا رفتم اگر کعبه و گر بتکده بود</p></div>
<div class="m2"><p>دیدم از زلف تو یک سلسله برپایی داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب در قفس سینه دلم ناله کند</p></div>
<div class="m2"><p>آه اگر رخنه از بهر تماشایی داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کور خوانده است مراز اهد مغرور ای کاش</p></div>
<div class="m2"><p>سود خود دیدی اگر دیده بینایی داشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر قدم در ره عشقت که نهادم دیدم</p></div>
<div class="m2"><p>خسته و مانده چه من آبله برپایی داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت دست تقاضای قضا برهم زد</p></div>
<div class="m2"><p>هر کجا دید کسی عیش مهیایی داشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>(صامتا) هر که من و عیش مرا دید به خویش</p></div>
<div class="m2"><p>گفت ای کسی که جا بر لب دریایی داشت</p></div></div>