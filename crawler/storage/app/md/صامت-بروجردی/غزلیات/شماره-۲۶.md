---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>جز مهر تو ای مه سروکارم به کسی نیست</p></div>
<div class="m2"><p>جز خاک سر کوی تو بر سر هوسی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد لال جرس در ره عشق تو چو داند</p></div>
<div class="m2"><p>خوشتر ز فغان دل پر خون جرسی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی تو کنی یاد اسیران که چو بینی</p></div>
<div class="m2"><p>از ما به جز از مشت پری در قفسی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی که به بالین تو آیم دم مردن</p></div>
<div class="m2"><p>افسانه اگر نیست مرا جز نفسی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر رشته کار دو جهان رفته ز دستم</p></div>
<div class="m2"><p>زانرو که به زلفین توام دسترسی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنداشتم آن دانه خال است به دام است</p></div>
<div class="m2"><p>اکنون شدم آگه که ره پیش و پسی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بینید غرورش که پس از کشتن(صامت)</p></div>
<div class="m2"><p>می‌گفت منم قاتل و کاری به کسی نیست</p></div></div>