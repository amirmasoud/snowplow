---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>همیشه افسر فرماندهی بر سر نمی‌ماند</p></div>
<div class="m2"><p>اگر ماند دمی ماندم دم دیگرْ نمی‌ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شکر سلطنت منما عدول از عدل در عالم</p></div>
<div class="m2"><p>که این ملک و اساس و کشور و لشکر نمی‌ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دست دار و گیر خلق بهر منصوب و مکنت</p></div>
<div class="m2"><p>جهان آسوده یک ساعت ز شور و شر نمی‌ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز فتواهای ناحق عنقریسست اینکه در عالم</p></div>
<div class="m2"><p>که اسم و رسمی از آئین پیغمبر نمی‌ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ملک و مال این ویرانسرای عاریت بگذر</p></div>
<div class="m2"><p>ز نام نیک چیزی در جهان بهتر نمی‌ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به بذل و بخش خود منمای پروا از تهیدستی</p></div>
<div class="m2"><p>سخاوت پیشه در آفاق هرگز در نمی‌ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشم زین منزلت (صامت) که در عالم به جای من</p></div>
<div class="m2"><p>اساس و فرش و نقد و جنس و سیم و زر نمی‌ماند</p></div></div>