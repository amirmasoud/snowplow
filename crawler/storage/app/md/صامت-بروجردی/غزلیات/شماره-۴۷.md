---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>کسی که در صف مستان به احتراز نشیند</p></div>
<div class="m2"><p>چه قابل است که در بزم اهل راز نشیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بپای خیز و در این شهر غارت دل و دین کن</p></div>
<div class="m2"><p>تو را که گفت نشین تا که فتنه باز نشیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعادت ابدی چون نوشته بر پیر تیرت</p></div>
<div class="m2"><p>بهر دلی که نشیند بگو به ناز نشیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همای عشق چون آگاه بود ز سلطنت فقر</p></div>
<div class="m2"><p>همیشه بر سر رندان پاکباز نشیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>محبت است که باید چو روح از تن محمود</p></div>
<div class="m2"><p>برون شود بسر طره ایاز نشیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر طرف نکند جلوه‌گر جمال تو از چیست</p></div>
<div class="m2"><p>گهی بدیر و گهی بر در حجاز نشیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر مگوی ز زلفش که دام جمله دلهاست</p></div>
<div class="m2"><p>کزین مقدمه (صامت) سخن دراز نشیند</p></div></div>