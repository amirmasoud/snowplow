---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>تا سرو کار بدان طره پر خم دارم</p></div>
<div class="m2"><p>از پریشانی ایام چرا غم دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب هجران و تب فرقت و گلهای فراق</p></div>
<div class="m2"><p>شکر صد شکر که هر عیش فراهم دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لخت دل خون جگر قسمت امروز منست</p></div>
<div class="m2"><p>روز نا آمده را بهر چه ماتم دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌نیاز است چنان دیده‌ام از دولت فقر</p></div>
<div class="m2"><p>که جهان را به یکی مور مسلم دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من که هرگز ندهم ملک قناعت از دست</p></div>
<div class="m2"><p>از غنیمت ز سلیمان چه مگر کم دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم امید من از خواجه خویش است ارنه</p></div>
<div class="m2"><p>عار در بندگی از سلطنت جم دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زخمی غمزه خونریز نگارم (صامت)</p></div>
<div class="m2"><p>به جز او کی ز کسی دیده مرهم دارم</p></div></div>