---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>آن مشک که در چین به صدا اعزاز خرندش</p></div>
<div class="m2"><p>در چین سر زلف تو با ناز خرندش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندیشه چه داری ز خطاکردن تیرت</p></div>
<div class="m2"><p>گر بگذر از دیده بدل باز خرندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسرار غم عشق تو نایاب متاعی است</p></div>
<div class="m2"><p>کو را نتوان مردم غمساز خرندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌قدرتری از دل عاشق نبود لیک</p></div>
<div class="m2"><p>از بهر نگهداشتن راز خرندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پس خاصیت اشک شب هجرد گر چیست</p></div>
<div class="m2"><p>از زهد و ریا گرنه که ممتاز خرندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راضی مشو افشا شود آوازه حسنت</p></div>
<div class="m2"><p>خوار است متاعی که به آواز خرندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بندگی ار خاک شود هیکل (صامت)</p></div>
<div class="m2"><p>مشکمل که از این طالع ناساز خرندش</p></div></div>