---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>ای دل از این ناله گر تاثیر می‌خواهی ندارد</p></div>
<div class="m2"><p>با دمی زان لعتب کشمیر می‌خواهی ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرنوشت ما شده روز ازل در نامرادی</p></div>
<div class="m2"><p>گر تو از حکم قضا تغییر می‌خواهی ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار چون رفت از برت ای جان به رفتن شو مهیا</p></div>
<div class="m2"><p>بعد از این از عمر گر تخیر میخواهی ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا زخم گیشو و چشم و رخش قطع نظر کن</p></div>
<div class="m2"><p>یا که راحت گر که از زنجیر می‌خواهی ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشته ابروی چالاکش برد فیض شهادت</p></div>
<div class="m2"><p>درک این لذت گر از شمشیر می‌خواهی ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سینه را بنما هدف در نزد این ابر و کمانها</p></div>
<div class="m2"><p>گر تو از کیش و فاجز تیر می‌خواهی ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیده باید بست از اول تا دل خود را نبازی</p></div>
<div class="m2"><p>چون نبستی (صامتا) تدبیر می‌خواهی ندارد</p></div></div>