---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>ز بسکه در غم روی تو انتظار کشیدم</p></div>
<div class="m2"><p>قلم به صفحه عشاق روزگار کشیدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم ز صافی طینت چنان به پرتو عشقت</p></div>
<div class="m2"><p>که مهره را به سلوک از دهان مار کشیدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بیم خواهش بی‌جا که از وصال تو می‌کرد</p></div>
<div class="m2"><p>پی مواخذه منصور دل بدار کشیدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شدم تسلی حام می و محبت دیگر</p></div>
<div class="m2"><p>که مهره را به سلوک از دهان مار کشیدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بیم خواهش بی جا که از وصال تو می‌کرد</p></div>
<div class="m2"><p>پی مواخذه منصور دل بدار کشیدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زیر پر ننمودم سری برون همه عمر</p></div>
<div class="m2"><p>نه زحمت دی و نه منت بهار کشیدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شدم تسلی جامی و محبت دیگر</p></div>
<div class="m2"><p>نه شور باده نه درد سر خمار کشیدم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مران مراد گرای باغبان ز ساحت گلشن</p></div>
<div class="m2"><p>که من کلام حقیقت زنیش خار کشیدم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن زمان که شدم (صامتا) مصاحب عزلت</p></div>
<div class="m2"><p>عروس لذت کونین در کنار کشیدم</p></div></div>