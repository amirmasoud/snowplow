---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>دلم دائم ز هجرت خویش را بیمار می‌خواهد</p></div>
<div class="m2"><p>ز تیغ بی‌دریغت سینه را افکار می‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نمی‌خواهم که داغ عارضت از آب و تاب افتد</p></div>
<div class="m2"><p>بلی بلبل همیشه رونق گلزار می‌خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه تاثیری بود بی‌اشک در آه سحر گاهی</p></div>
<div class="m2"><p>که لشکر موسم جنگ و هنر سردار می‌خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کز بهر کفر و دین به ما ایراد می‌گیرد</p></div>
<div class="m2"><p>بگو این گفتگوهاآدم بیکار می‌خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بس از دوستان رنجیده قلب زودرنج من</p></div>
<div class="m2"><p>که دیگر راه و رسم یاری از اغیار می‌خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به محض ادعا کی حق شناسی می‌شود ثابت</p></div>
<div class="m2"><p>هر آن کس را که گفتاری بود کردار می‌خواهد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر (صامت) وصال یار خود را آرزو داری</p></div>
<div class="m2"><p>بود ممکن ولیکن زحمت بسیار می‌خواهد</p></div></div>