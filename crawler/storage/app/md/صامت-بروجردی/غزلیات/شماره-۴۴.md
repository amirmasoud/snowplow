---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>چینیان در چین گر از زلف تو چینی داشتند</p></div>
<div class="m2"><p>بر جبین در چین ز بوی مشک چینی داشتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مگس شکر فروشانرا محالست ایمنی</p></div>
<div class="m2"><p>زانکه صاحب خرمنان هم خوشه‌چینی داشتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواری احباب خود بنگر که در روز فراق</p></div>
<div class="m2"><p>ترک ایشان گفت هر جا همنشینی داشتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست و پای عاشق بیچاره بستن تازگی است</p></div>
<div class="m2"><p>روز اول راه در هر سرزمینی داشتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دورنگی بود مانع در وصالت عاشقان</p></div>
<div class="m2"><p>دوختند از غیر گر چشم دو بینی داشتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشور دلها چنین تنها مسخر می‌کنند</p></div>
<div class="m2"><p>تا چه می‌کردند خوبان گر معینی داشتند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می‌رود از دست مردم دین و غافل می‌چرند</p></div>
<div class="m2"><p>باز دین‌داران سابق درد دینی داشتند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عاقبت بینی اگر اندر میان خلق بود</p></div>
<div class="m2"><p>بود وقف چشم تر گر آستینی داشتند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صلح و جنگی (صامتا) از یار ما معلوم نیست</p></div>
<div class="m2"><p>خوبرویان روز اول مهر و کینی داشتند</p></div></div>