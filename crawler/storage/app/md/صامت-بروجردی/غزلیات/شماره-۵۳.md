---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>هر چه خواهی بر من ای دنیا ز تلخی تنگ گیر</p></div>
<div class="m2"><p>هی به قصد شیشه دلهای نازک سنگ گیر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا توانی یا لئیمان گرم کن طرح و فاق</p></div>
<div class="m2"><p>تا توانی یکدلی را بر ضعیفان تنگ گیر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نه از مهرت شوم خوشدل نه از قهرت ملول</p></div>
<div class="m2"><p>دیگران را رو بدام خود بر یور رنگ گیر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من سپر انداختم روز نخستین پیش تو</p></div>
<div class="m2"><p>بهر قتل تنگ چشمان رویراق جنگ گیر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>(صامت) اردنیا تو را سازد ز قید خود خلاص</p></div>
<div class="m2"><p>تو ز بهر پیشکش جان را به روی چنگ گیر</p></div></div>