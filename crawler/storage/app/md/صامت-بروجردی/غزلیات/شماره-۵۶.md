---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>اگر از بی‌وفایی‌های تو حرفی به لب دارم</p></div>
<div class="m2"><p>مشو آزرده دل جانا که هذیا ن‌ست تب دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو زخمم از دو ابر بهر کشتن و عده فرمودی</p></div>
<div class="m2"><p>به دوی حقی که من ای کج‌حساب از تو طلب دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا دیوانگی اندر محبت لازم است ورنه</p></div>
<div class="m2"><p>به هنگام ضرورت فخر از حسن ادب دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ایام فراغت هم نخواهم ذلت دشمن</p></div>
<div class="m2"><p>که چشم طول عمر اندر شب هجران ز شب دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکم عمری شدم قانع به مانند حباب امام</p></div>
<div class="m2"><p>بود خوشحالیم از اینکهاز دریا نسب دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آن دیگم که از خامی بجوشم دائماً (صامت)</p></div>
<div class="m2"><p>ولی از مهر جانان مهر خاموشی به لب دارم</p></div></div>