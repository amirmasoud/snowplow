---
title: >-
    شمارهٔ ۱۵ - همچنین
---
# شمارهٔ ۱۵ - همچنین

<div class="b" id="bn1"><div class="m1"><p>ای غمگین تو را با شادی دوران چکار</p></div>
<div class="m2"><p>صید شمشیر اجل را با لب خندان چکار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشت خاکی را به کبر و کنینه و طغیان چکار</p></div>
<div class="m2"><p>واله و آشفته را کفر و با ایمان چکار</p></div></div>
<div class="b2" id="bn3"><p>مردم دیوانه را با شحنه و سلطان چکار</p></div>
<div class="b" id="bn4"><div class="m1"><p>کار را از بهر دنیا کرده بر خویش تنگ</p></div>
<div class="m2"><p>می‌زنی بر شیشه قلب ضعیفان چند سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌بری تا کی به خون مردمان از ظلم چنگ</p></div>
<div class="m2"><p>هر که در بحر عمیق افتاده در کام نهنگ</p></div></div>
<div class="b2" id="bn6"><p>دیگرش با کار نوح و صدمه طوفان چه کار</p></div>
<div class="b" id="bn7"><div class="m1"><p>از سر پی افسر خود دور کن تاج غرور</p></div>
<div class="m2"><p>تا نگردی پایمال خلق محشر همچو مور</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا توانی با خدا شو آشنا ز ابلیس دور</p></div>
<div class="m2"><p>زاهدان را چشم بر فردوس و حور است و قصور</p></div></div>
<div class="b2" id="bn9"><p>عاشقان را با بهشت و کوثر و غلمان چکار</p></div>
<div class="b" id="bn10"><div class="m1"><p>نیمه شب از بستر غفلت گهی بردار سر</p></div>
<div class="m2"><p>عذرخواهی کن ز جرم خود به نزد دادگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا شود اندر دلت نور حقیقت جلوه‌گر</p></div>
<div class="m2"><p>بلبلان گر راز دل گویند با گل در سحر</p></div></div>
<div class="b2" id="bn12"><p>بوم را با ارغوان و نرگس خندان چکار</p></div>
<div class="b" id="bn13"><div class="m1"><p>ای در آغوش عروس جهل روز شب بخوانب</p></div>
<div class="m2"><p>یا مبر مال کسان را یا نما فکر جواب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>می‌کمنی تا کی ز ظلم خود دل مردم کباب</p></div>
<div class="m2"><p>هر که در فکر قیامت باشد و یوم حساب</p></div></div>
<div class="b2" id="bn15"><p>کار او با اهل ظلم و دفتر دیوان چکار</p></div>
<div class="b" id="bn16"><div class="m1"><p>ای خوشا آنان که پیش لطمه چوگان عشق</p></div>
<div class="m2"><p>پای افشاندند خویش مردانه در میدان عشق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان چون (صامت) باختند اندر سر پیمان عشق</p></div>
<div class="m2"><p>سعدیا تا چند گویی درد بی‌درمان عشق</p></div></div>
<div class="b2" id="bn18"><p>کشتگان دوست را با درد و با درمان چکار</p></div>