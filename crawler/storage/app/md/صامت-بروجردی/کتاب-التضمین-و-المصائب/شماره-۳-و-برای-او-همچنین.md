---
title: >-
    شمارهٔ ۳ - و برای او همچنین
---
# شمارهٔ ۳ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>شیعیان بار دگر نخل عزا می‌بندند</p></div>
<div class="m2"><p>باز بار سفر کرب و بلا می‌بندند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا مگر حجله قاسم به مبلا می‌بندند</p></div>
<div class="m2"><p>باز پیرایه گلشن به حنا می‌بندند</p></div></div>
<div class="b2" id="bn3"><p>بوی گلهای چمن را به صبا می‌بندند</p></div>
<div class="b" id="bn4"><div class="m1"><p>گفت قاسم اگرم لشگر غم چیره شود</p></div>
<div class="m2"><p>تیر صیاد پی صید حرم چیره شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نترسم که به من خیل ستم چیره شود</p></div>
<div class="m2"><p>هر کجا چتر دو طاووس به هم چیره شود</p></div></div>
<div class="b2" id="bn6"><p>نخل قتل دل پر داغ مرا می‌بندند</p></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا جان ره جانان ز وفا بسپارد</p></div>
<div class="m2"><p>پای همت بسر کون و مکن بگذارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گور را حجله دامادی خون پندارد</p></div>
<div class="m2"><p>دلم از خون شدن خویش نشاطی دارد</p></div></div>
<div class="b2" id="bn9"><p>همچو طفان که شب عید حنا می‌بندند</p></div>
<div class="b" id="bn10"><div class="m1"><p>ای عمویی که تو را هست خدم خیل ملک</p></div>
<div class="m2"><p>تشنه آب شهادت شده‌ام همچو ملک</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لطف بنما و مکن نام من از دفتر حک</p></div>
<div class="m2"><p>تویی آن آیت رحمت که ملایک به فلک</p></div></div>
<div class="b2" id="bn12"><p>حرز نام تو به بازوی دعا می‌بندند</p></div>
<div class="b" id="bn13"><div class="m1"><p>نوعروسا بنگر پیک اجل بر کف جام</p></div>
<div class="m2"><p>دارد و می‌دهدم از بر جانان پیغام</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>توهم آماده تاراج شو و رفتن شام</p></div>
<div class="m2"><p>درد هجر است سزای دل و جانم که مدام</p></div></div>
<div class="b2" id="bn15"><p>تهمت رجم بر آن شوخ بلا می‌بندند</p></div>
<div class="b" id="bn16"><div class="m1"><p>عاشقی را که شود دیده دل محو صفات</p></div>
<div class="m2"><p>آرزویی به دلش نیست به جز دیدن ذات</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>(صامتا) از دل عشاق محو صبر و ثبات</p></div>
<div class="m2"><p>تو خیالان همه خوش طبع و ظریفند نجات</p></div></div>
<div class="b2" id="bn18"><p>لیک کی چون تو سخن را به ادا می‌بندند</p></div>