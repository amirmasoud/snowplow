---
title: >-
    شمارهٔ ۱۲ - و برای او همچنین
---
# شمارهٔ ۱۲ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>یا رب نظری کن به من و چشم پر آبم</p></div>
<div class="m2"><p>کز بیم مکافات تو اندر تب و تابم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما کرمت برده ز دل خوف عذابم</p></div>
<div class="m2"><p>چندان بسر کوی خرابات خرابم</p></div></div>
<div class="b2" id="bn3"><p>کاسوده ز اندیشه فردای حسابم</p></div>
<div class="b" id="bn4"><div class="m1"><p>تا دام عطای تو بود بر سر راهم</p></div>
<div class="m2"><p>گر بنده فرمانم و گر روی سیاهم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نبود جانب اعمال نگاهم</p></div>
<div class="m2"><p>گر کار تو فضلست چه پرواز گناهم</p></div></div>
<div class="b2" id="bn6"><p>ور شغل تو عدل است چه حال به ثوابم</p></div>
<div class="b" id="bn7"><div class="m1"><p>چون من به بدن جامه تذویر نپوشم</p></div>
<div class="m2"><p>آزار دلی ندهم و زهدی نفروشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>الا به ره دوستی دوست نکوشم</p></div>
<div class="m2"><p>افسانه دوزخ همه باد است به گوشم</p></div></div>
<div class="b2" id="bn9"><p>من ز آتش هجران تو در عین عذابم</p></div>
<div class="b" id="bn10"><div class="m1"><p>هرچند مرا فقر به سر حد کمال است</p></div>
<div class="m2"><p>دستم به کسی باز کی از بهر سئوالست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خاطر ز پی وصل تو سرگرم خیالست</p></div>
<div class="m2"><p>آه سحر و اشک شبم شاهد حالست</p></div></div>
<div class="b2" id="bn12"><p>کز یاد رخ و زلف تو در آتش و آیم</p></div>
<div class="b" id="bn13"><div class="m1"><p>نایافتم از بی‌خبری راحت جان را</p></div>
<div class="m2"><p>کندم ز بدن پیرهن شک و گمان را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>انداختم از سر هوس کون و مکان را</p></div>
<div class="m2"><p>نخجیر نمودم همه شیران جهان را</p></div></div>
<div class="b2" id="bn15"><p>تا آهوی چشمت سگ خود کرد خطابم</p></div>
<div class="b" id="bn16"><div class="m1"><p>پنداشتم اول که زبون کرد مرا عشق</p></div>
<div class="m2"><p>چون یافتم از خویش برون کرد مرا عشق</p></div></div>
<div class="b2" id="bn17"><p>تا برد مرا سلسله موی تو تابم</p></div>
<div class="b" id="bn18"><div class="m1"><p>روزی که دلم جلوه خوبان جهان دید</p></div>
<div class="m2"><p>ز آن جلوه عیان پرتو آن روی نهان دید</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن را که نظر در طلبش بود همان دید</p></div>
<div class="m2"><p>گفتم که به شب چشمه خورشید توان دید</p></div></div>
<div class="b2" id="bn20"><p>گفت ار بگشایند شبی بند نقابم</p></div>
<div class="b" id="bn21"><div class="m1"><p>ای پیش رو مردم آزاد فروغی</p></div>
<div class="m2"><p>بنیاد محبت ز تو آباد فروغی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جسته ز تو (صامت) ره ارشاد فروغی</p></div>
<div class="m2"><p>از تنگی دل هر چه زدم داد فروغی</p></div></div>
<div class="b2" id="bn23"><p>یک بار نداد آن مه بی‌باک جوابم</p></div>