---
title: >-
    شمارهٔ ۱۷ - و برای او
---
# شمارهٔ ۱۷ - و برای او

<div class="b" id="bn1"><div class="m1"><p>ای که نموده ترک سر بهره کلاه سروری</p></div>
<div class="m2"><p>می‌ترسد به سروری هیچ سری به سر سری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیهده هیچکس نشد شحنه شهر مهتری</p></div>
<div class="m2"><p>با ملک ار ترا بسر هست هوای همسری</p></div></div>
<div class="b2" id="bn3"><p>ترک نماز جان و دل شیوه نفس پروری</p></div>
<div class="b" id="bn4"><div class="m1"><p>جغد صفت نشسته‌ای خوش به خرابه بدن</p></div>
<div class="m2"><p>بسته‌ای آشیانه را سخت به شاخسار تن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ره تست منتظر دیده مردم وطن</p></div>
<div class="m2"><p>خیز وز شهر اغنیا خیمه به ملک فقر زن</p></div></div>
<div class="b2" id="bn6"><p>تا بسپهر برکشی ماهیچه توانگری</p></div>
<div class="b" id="bn7"><div class="m1"><p>ره تبری به منزلی تا کنی سفر ز خود</p></div>
<div class="m2"><p>می‌طلبی ز گمرهی از دگری خبر ز خود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواهی اگر که خویش را جوی تو راهبر ز خود</p></div>
<div class="m2"><p>ساغر بزم بیخودی در کش و در گذر ز خود</p></div></div>
<div class="b2" id="bn9"><p>تا کندت به آسمان ماه دو هفته ساغری</p></div>
<div class="b" id="bn10"><div class="m1"><p>طی طریق بندگی نیست به لشکر و سپه</p></div>
<div class="m2"><p>در سرلشگر و سپه عمر چه می‌کنی تبه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جانب همرهان خود از چه نمی‌کنی نگه</p></div>
<div class="m2"><p>منزل یار را بود وادی نفس نیمره</p></div></div>
<div class="b2" id="bn12"><p>کی برسی بیار خودگر که ز خویش نگذری</p></div>
<div class="b" id="bn13"><div class="m1"><p>نوبت خسروی زند چرخ به آشیان تو</p></div>
<div class="m2"><p>از فلک و ملک بسی آمده پاسبان تو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بنده نقش می‌شود کو نکشد کمان تو</p></div>
<div class="m2"><p>با همه کبر و سرکشی هست ز چاکران تو</p></div></div>
<div class="b2" id="bn15"><p>آن که تو بسته‌ای کمر بر در او به چاکری</p></div>
<div class="b" id="bn16"><div class="m1"><p>باغ و بهشت و عجب مفت ز دست هشته‌ای</p></div>
<div class="m2"><p>از سر خانمانتاز بی‌خبری گذشته‌ای</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گرد حریم قرب حق یک نفسی نگشته‌ای</p></div>
<div class="m2"><p>ای که ز پست فطرتی مرکب دیو گشته‌ای</p></div></div>
<div class="b2" id="bn18"><p>کوش که با فلک زنی طنطعنه برابری</p></div>
<div class="b" id="bn19"><div class="m1"><p>گرد حلاوت جهان هرگز مگرد چون مگس</p></div>
<div class="m2"><p>محتسب حساب خود باش و بدرد خود برس</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کوس رحیل کاروان بشنو و ناله جرس</p></div>
<div class="m2"><p>توشه راه خویش کن تا نگرفته راه پس</p></div></div>
<div class="b2" id="bn21"><p>عاریه‌های خویش را از تو سپهر چنبری</p></div>
<div class="b" id="bn22"><div class="m1"><p>دوری نمی‌کنی چرا ساغر بیهشی ز لب؟</p></div>
<div class="m2"><p>طالب راحتی اگر منزل عافیت طلب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نماد معاد خویش کن دانه اشک نیمه شب</p></div>
<div class="m2"><p>قافله وقت صبحدم رفت و تو مانده عقب</p></div></div>
<div class="b2" id="bn24"><p>بر سر راه منتظر راهزنان لشگری</p></div>
<div class="b" id="bn25"><div class="m1"><p>لوح ضمیر خویش کن صاف ز نقش مهر کین</p></div>
<div class="m2"><p>پند طبیب گوش کن پس به شفای او ببین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به بودت ز درد دان شور شراب درد دین</p></div>
<div class="m2"><p>تن به بر هیست بس سمین گرک فناش در کمین</p></div></div>
<div class="b2" id="bn27"><p>از پی قوت خصم خود این به را چه پروری</p></div>
<div class="b" id="bn28"><div class="m1"><p>تیر تغافل امل تا به کمان تو بود</p></div>
<div class="m2"><p>طعم طعام خود سری تا به دهان تو بود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چون بره‌ای تو فی‌المثل گرگ شبان تو بود</p></div>
<div class="m2"><p>نفس هواپرست تو دشمن جان تو بود</p></div></div>
<div class="b2" id="bn30"><p>بیهده ظن دشمنی بر دگران چرا بری</p></div>
<div class="b" id="bn31"><div class="m1"><p>عمر عزیز تو تلف در سر روزگار شد</p></div>
<div class="m2"><p>دست امید کوته و پای طلب فکار تو</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>قافله رفت (صامتا) خیز که وقت بار شد</p></div>
<div class="m2"><p>هر که بدیدم او حدی رهرو کوی یار شد</p></div></div>
<div class="b2" id="bn33"><p>از همه مانده‌ای بجا خود مگر از که کمتری</p></div>