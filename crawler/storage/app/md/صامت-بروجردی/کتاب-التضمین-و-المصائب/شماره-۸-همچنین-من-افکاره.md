---
title: >-
    شمارهٔ ۸ - همچنین من افکاره
---
# شمارهٔ ۸ - همچنین من افکاره

<div class="b" id="bn1"><div class="m1"><p>زینب به حسین گفت که ای تاج سر ما</p></div>
<div class="m2"><p>ای قافله سالار من و همسفر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده بخوابی چه خوش از رهگذر ما</p></div>
<div class="m2"><p>نگذاشت که بر روی تو افتد نظر ما</p></div></div>
<div class="b2" id="bn3"><p>دیدی که چها کرد به ما چشم تر ما</p></div>
<div class="b" id="bn4"><div class="m1"><p>طاقت کم و غم بیش زمان کوته که تواند</p></div>
<div class="m2"><p>تا درد گرفتاری ما بر تو رساند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کو مرگ که از قید حیاتم برهاند</p></div>
<div class="m2"><p>احوال دل سوخته دل سوخته داند</p></div></div>
<div class="b2" id="bn6"><p>از شمع بپرسید ز سوز جگر ما</p></div>
<div class="b" id="bn7"><div class="m1"><p>ای کعبه اسلام کشیدیم سوی دیر</p></div>
<div class="m2"><p>رخت از سوی کوی تو خداوند کند خیر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با شمر ستمکار جفا جوی سبکسیر</p></div>
<div class="m2"><p>گو این همه شادی مکن از رفتن ما غیر</p></div></div>
<div class="b2" id="bn9"><p>گاهی نبود بیش ز کویش سفر ما</p></div>
<div class="b" id="bn10"><div class="m1"><p>داغ تو بر آورد ز کانون دلم دود</p></div>
<div class="m2"><p>دودی که بسوزد ز تفش آتش نمرود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زین محنت بسیار که دارم همه موجود</p></div>
<div class="m2"><p>غیرم به فسون کرد جدا از تو چه می‌بود</p></div></div>
<div class="b2" id="bn12"><p>گر داشت اثر تیر دعای سحر ما</p></div>
<div class="b" id="bn13"><div class="m1"><p>تا دهر چه خواهد ز من سوخته کوکب</p></div>
<div class="m2"><p>کز داغ غمت روز مرا ساخته چون شب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وز سنگ عدو جان ز تعب آمده بر لب</p></div>
<div class="m2"><p>تا از پی آزار که گرد آمده امشب</p></div></div>
<div class="b2" id="bn15"><p>جمعند رقیبان بسر رهگذر ما</p></div>
<div class="b" id="bn16"><div class="m1"><p>آمد به سر نعش تو خواهر پی دیدن</p></div>
<div class="m2"><p>شد غنچه دیدار تو را موسم چیدن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ده گوش به دردم بود ارمیل شنیدم</p></div>
<div class="m2"><p>شادم که ز کویت نتوانم به پریدن</p></div></div>
<div class="b2" id="bn18"><p>بشکته شد از سنگ ستم بال و پر ما</p></div>
<div class="b" id="bn19"><div class="m1"><p>تا تاب عطش لاله سیراب تو پژمرد</p></div>
<div class="m2"><p>شمع رخ رنگین تو از صرصر کین برد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر شیشه بی‌تابی من سنگ جفا خورد</p></div>
<div class="m2"><p>زین بانک جرس راه به جایی نتوان برد</p></div></div>
<div class="b2" id="bn21"><p>کو خضر رهی تا که شود راهبر ما</p></div>
<div class="b" id="bn22"><div class="m1"><p>از بس بشه تشنه جگر راز نهان گفت</p></div>
<div class="m2"><p>راز غم پنهان به برادر به عیان گفت</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>(صامت) به امانم آمده هر دم به فغان گفت</p></div>
<div class="m2"><p>امشب همه مجمر سخن از سوختگان گفت</p></div></div>