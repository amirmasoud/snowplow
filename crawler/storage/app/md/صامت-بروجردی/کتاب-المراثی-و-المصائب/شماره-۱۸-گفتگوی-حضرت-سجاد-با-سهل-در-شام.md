---
title: >-
    شمارهٔ ۱۸ - گفتگوی حضرت سجاد با سهل در شام
---
# شمارهٔ ۱۸ - گفتگوی حضرت سجاد با سهل در شام

<div class="b" id="bn1"><div class="m1"><p>این سرانی که بینی با رخ همچون قمرند</p></div>
<div class="m2"><p>صدف علم نبی را همه یکتا گهرند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه گردیده گرفتار ز هر بی‌پدری</p></div>
<div class="m2"><p>اسد بیشه دین شیر خدا را پسرند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه بنهاده اگر سر به سر حکم قضا</p></div>
<div class="m2"><p>هر یکی باد شه امر قضا و قدرند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خشک گشته لبشان گر ز تف بی‌آبی</p></div>
<div class="m2"><p>منبع کوثر و آب همه بحر و برند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهنمایند بهر گمده در هر دو جهان</p></div>
<div class="m2"><p>گرچه بی‌یار و معین بر سر هر رهگذرند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کودکانی که به رخ کرده روان سیل سرشک</p></div>
<div class="m2"><p>اصل ایمان شجر باغ نبی را ثمرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه را گرد یتیمی بنگر بر رخسار</p></div>
<div class="m2"><p>هر طرف روی نمایند به فکر پدرند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این زنانی که ندارند به سرها معجر</p></div>
<div class="m2"><p>محرم خاص نبی عصمت حقرا ثمرند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رفته تاراج ستم زینب و اسبا همه</p></div>
<div class="m2"><p>به سر عهد و وفا در بدر و خون جگرند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شامیان خنده نمایند به اولاد رسول</p></div>
<div class="m2"><p>ز دل فاطمه و گریه او بی‌خبرند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>(صامتا) بهر چه یک مرد ز اسلام نگفت</p></div>
<div class="m2"><p>کاین اسیران حرم خسرو و جن و بشرند</p></div></div>