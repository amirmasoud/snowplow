---
title: >-
    شمارهٔ ۱۹ - و برای او همچنین
---
# شمارهٔ ۱۹ - و برای او همچنین

<div class="b" id="bn1"><div class="m1"><p>دگر ندیده ستم کش جهان مقابل زینب</p></div>
<div class="m2"><p>سرشته شد به غم و درد گوئیا گل زینب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جز اسیری و طعن سنان و طعنه دشمن</p></div>
<div class="m2"><p>ز زندگانی دنیا نگشت حاصل زینب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بعد قتلبرادر میان کوفی و شامی</p></div>
<div class="m2"><p>بهر دم از غم و دردی شکسته شد دل زینب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنم حکایت سیلی شمر و روی سکینه</p></div>
<div class="m2"><p>و یا ز بستن بازوی در سلاسل زینب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میان لشگر اعدا ندات چادر و معجر</p></div>
<div class="m2"><p>به سوی سربمه چهره گشت حایل زینب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به راه شام نمودند نصب لشگر کوفه</p></div>
<div class="m2"><p>سر برادر او را به پیش محمل زینب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر نبود مقامی برای عترت طه</p></div>
<div class="m2"><p>که در خرابه بی‌سقف گشت منزل زینب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان که زد ز ستم خیزران یزید سیه‌روی</p></div>
<div class="m2"><p>به راس تشنه‌لب کربلا مقابل زینب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سزد که فخر کند (صامت) حزین بدو عالم</p></div>
<div class="m2"><p>که شد به عین گدایی غلام مقبل زینب</p></div></div>