---
title: >-
    شمارهٔ ۸ - وقایع روز یازدهم عاشورا
---
# شمارهٔ ۸ - وقایع روز یازدهم عاشورا

<div class="b" id="bn1"><div class="m1"><p>از نفاق فلک و گردش دوران امشب</p></div>
<div class="m2"><p>تن صدچاک حسین مانده به میدان امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز فلک رخ منما زهره که زهرای حزین</p></div>
<div class="m2"><p>شده از داغ حسین موی پریشان امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه بودی به یتیمان پدر مرده پناه</p></div>
<div class="m2"><p>مانده اطفال وی اندر کف عدوان امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوفه را بزم طرب گرم تماشا دارد</p></div>
<div class="m2"><p>که چراغان شده از آه یتیمان امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاسبانی به تن سبط پیمبر نبود</p></div>
<div class="m2"><p>مانده در دشت بلا پیکر عریان امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی غلط گفتم باشد به سر بالینش</p></div>
<div class="m2"><p>ساربان و به کفش خنجر بران امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتگویی بودش گر به برادر زینب</p></div>
<div class="m2"><p>گو که در مطیع خولی شده مهمان امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آه و صد آه که دادن مکان زینب را</p></div>
<div class="m2"><p>بایتیمان حسین گوشه زندان امشب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غیر خون جگر و لخت دل و اشک بصر</p></div>
<div class="m2"><p>آب و نانی نبود از بهریتیمان امشب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عجبی نیست اگر از ستم ابن زیاد</p></div>
<div class="m2"><p>ملک ایجاد شود یکسره ویران امشب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کلک (صامت) اگر اینگونه دهد شرح عزا</p></div>
<div class="m2"><p>خلق باید که بشویند دل از جان امشب</p></div></div>