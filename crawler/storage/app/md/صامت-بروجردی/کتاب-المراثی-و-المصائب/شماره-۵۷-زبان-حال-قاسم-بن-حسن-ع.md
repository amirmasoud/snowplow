---
title: >-
    شمارهٔ ۵۷ - زبان حال قاسم بن حسن(ع)
---
# شمارهٔ ۵۷ - زبان حال قاسم بن حسن(ع)

<div class="b" id="bn1"><div class="m1"><p>عمو به حالت من چشم مرحمت واکن</p></div>
<div class="m2"><p>بیا و قاسم دلخسته را تماشا کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفته تنگ به حالم سپاه سنگین دل</p></div>
<div class="m2"><p>نظر به قاسم و سپر هجوم اعدا کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به جز تو هیچ کس اندر غم یتیمان نیست</p></div>
<div class="m2"><p>بیا دمی به سرم از ره وفا جا کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رضا مشو که به حسرت روم به حجله خاک</p></div>
<div class="m2"><p>اساس عشرت داماد خود مهیا کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمو به جان پدر کن به حال من پدری</p></div>
<div class="m2"><p>برای من زوفا بزم عیش برپا کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو به خیمه عموجان برای خاطر من</p></div>
<div class="m2"><p>عروس بی‌کس افسرده را تسلی کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شده است پیکر قاسم هزار پاره ز تیغ</p></div>
<div class="m2"><p>بیا جراحت جسم مرا مداوا کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سم اسب نگردیده تا تنم پامال</p></div>
<div class="m2"><p>مرا خلاص ز اعدادی بی‌سروپا کن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو شد عروسی قاسم عزا دگر (صامت)</p></div>
<div class="m2"><p>ز دست دور فلک مرگ خود تمنا کن</p></div></div>