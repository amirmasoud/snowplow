---
title: >-
    شمارهٔ ۶ - در شکایت از سپهر و گریز به مصیبت
---
# شمارهٔ ۶ - در شکایت از سپهر و گریز به مصیبت

<div class="b" id="bn1"><div class="m1"><p>حسد چرخ نگر رونق دین بر شکند</p></div>
<div class="m2"><p>جبهه انور زیبای پیمبر شکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تیغ خون ریز دهد در کف بن ملجم دون</p></div>
<div class="m2"><p>نزد محراب دعا تارک حیدر شکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسر تخت خلافت بنشاند بوبکر</p></div>
<div class="m2"><p>پهلوی فاطمه را از لگد و درشکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهر در کام حسن ریزد و تار و ز جزا</p></div>
<div class="m2"><p>ز غمش غلغله بر طارم اخضر فکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشی آل نبی غرق کند دریم خون</p></div>
<div class="m2"><p>لنگررونق ایجاد ز صرصر شکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست عباس علی را کند از تیغ جدا</p></div>
<div class="m2"><p>در جنان پشت علی ساقی کوثر شکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>داغ اکبر بنهد بر دلا لیلای حزین</p></div>
<div class="m2"><p>تیر پران به گلوی علی اصغر فکند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پا کند بزم عروسی ز برای قاسم</p></div>
<div class="m2"><p>پس از آن قلب عروس از غم شوهر شکند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون حسین را سر زین به زمین اندازد</p></div>
<div class="m2"><p>سینه‌اش از لگد شمر ستمگر شکند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فکند در صف میدان تن نازک بدنان</p></div>
<div class="m2"><p>وانگه از سم ستوران همه یکسر شکند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>محشر آنست که (صامت) به سحرگاه جزا</p></div>
<div class="m2"><p>ز غم شاه شهیدان صف محشر شکند</p></div></div>