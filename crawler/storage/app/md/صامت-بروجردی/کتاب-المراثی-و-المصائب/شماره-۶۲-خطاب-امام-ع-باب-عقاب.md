---
title: >-
    شمارهٔ ۶۲ - خطاب امام(ع) باب عقاب
---
# شمارهٔ ۶۲ - خطاب امام(ع) باب عقاب

<div class="b" id="bn1"><div class="m1"><p>کجاست راکبت ای مرکب نکوسیما</p></div>
<div class="m2"><p>علی اکبر من کرده در کجا ماوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوان نو خط و فرزند نو رسم چو نشد</p></div>
<div class="m2"><p>کجا به خاک مکان کرد و غرقه در خو نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برون نیاوری از انتظار جان مرا</p></div>
<div class="m2"><p>نمی‌دهی خبر اکبر جوان مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا چرا ز علی اکبرم جدا کردی</p></div>
<div class="m2"><p>جوان نو سفرم را چرا نیاوردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گمان نداشتم آنقدر بی‌وفا باشی</p></div>
<div class="m2"><p>که بی‌سبب ز علی‌اکبرم جدا باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای چیست که زین تو واژگون گشته</p></div>
<div class="m2"><p>ز پای تا بسرت از چه غرقه خون گشته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز شرم آب اگر اکبرم نیامده است</p></div>
<div class="m2"><p>به برج خیمه ه انورم نیامده است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگو سکینه‌ام ای نوجوان ز آب گذشت</p></div>
<div class="m2"><p>دگر ز خواهش آب از دل کباب گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا علاج دل دردمند لیلا کن</p></div>
<div class="m2"><p>ز گریه مادر افسرده را تسلی کن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک ز کشتن اکبر فزوده داغم را</p></div>
<div class="m2"><p>نموده کور اگر آسمان چراغم را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا رسان ز برای خدا به بالینش</p></div>
<div class="m2"><p>که وقت مرگ ببندم دو چشم حق‌بینش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بهسوی خیمه رسانم قد رسایش را</p></div>
<div class="m2"><p>کشم ز مهر سوی قبله دست و پایش را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>رسید وقت ز فاق یگانه فرزندم</p></div>
<div class="m2"><p>به خیمه حجله شادی برای او بندم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نموده است ز خون گلو خضابش را</p></div>
<div class="m2"><p>به حجله رفته ببوسد دو دست بابش را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز بس که واقعه کربلا غم‌انگیز است</p></div>
<div class="m2"><p>همیشه دیده (صامت) ز غصه خونریز است</p></div></div>