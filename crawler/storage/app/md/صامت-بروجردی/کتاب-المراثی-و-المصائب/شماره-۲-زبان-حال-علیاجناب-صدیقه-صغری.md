---
title: >-
    شمارهٔ ۲ - زبان حال علیاجناب صدیقه صغری
---
# شمارهٔ ۲ - زبان حال علیاجناب صدیقه صغری

<div class="b" id="bn1"><div class="m1"><p>زینب چو دید بر سر نی راس شاه را</p></div>
<div class="m2"><p>بر نه فلک نمود روان پیک آه را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاک و خون به نوک سنان دید منخسف</p></div>
<div class="m2"><p>آن رخ که کرده بود خجل مهر و ماه را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتا به ناله‌ای که نمودی به عهد مهد</p></div>
<div class="m2"><p>روشن ز یری خویشتن عرش اله را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جای تو بود بر سر دوش نبی چرا</p></div>
<div class="m2"><p>کردی سر سنان سنان جایگاه را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شرط وفا نبود که تنها گذاشتی</p></div>
<div class="m2"><p>در دست اهل ظلم من بی‌پناه را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن ظلم‌ها که کرد پشیمان نمی‌کند</p></div>
<div class="m2"><p>ابن زیاد سنگدل دین تباه را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سجاد غل به گردن و مسرور ابن‌سعد</p></div>
<div class="m2"><p>بین تا کجا رسانده فلک اشتباه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون بر سر تو دسترسم نیست می‌کنم</p></div>
<div class="m2"><p>از بهر چاره بر سر خود خاک راه را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن یک به کعب می زندم دیگری به سنک</p></div>
<div class="m2"><p>آخر گناه چیست من بی‌گناه را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از بعد خویش بی‌کسی من نظاره کن</p></div>
<div class="m2"><p>یک تن چسان شکستم من یک سپاه را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر باد داده خرمن صبرم جفای شمر</p></div>
<div class="m2"><p>آری چسان تحمل کوهست کاه را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه طاقتی که بر سر نی بنگرم سرت</p></div>
<div class="m2"><p>نی صبر کز رخ تو بپوشم نگاه را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از گریه گر سفید شود چشم من چه سود</p></div>
<div class="m2"><p>نتوان نمود چاره بخت سیاه را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>محنت ز بس کشیدم و دیدم که برده است</p></div>
<div class="m2"><p>از یاد من مقدمه عز و جاه را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از من گذشت ای سر پر خون مکن دریغ</p></div>
<div class="m2"><p>ز اطفال خویش مرحمت گاهگاه را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>(صامت) ز بس نموده محن جا به ملک تن</p></div>
<div class="m2"><p>ترسم اجل بهم زند این دستگاه را</p></div></div>