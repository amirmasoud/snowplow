---
title: >-
    شمارهٔ ۳۳ - زبان حال صدیقه صغرا
---
# شمارهٔ ۳۳ - زبان حال صدیقه صغرا

<div class="b" id="bn1"><div class="m1"><p>برادرا دلم رفتنت قرار ندارد</p></div>
<div class="m2"><p>مرو که خواهر تو ناب انتظار ندارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به درد بی‌کسی‌ام مبتلا مکن به فدایت</p></div>
<div class="m2"><p>که خواهر تو کسی را در این دیدار ندارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو منع می‌کنی از گریه‌ام ولی نتوانم</p></div>
<div class="m2"><p>دل شکسته‌ام از گریه اختیار ندارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز وعده برگشتنت قرار نگیرد</p></div>
<div class="m2"><p>چرا که گردش ایام اعتبار ندارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به خیمه منتظر تو نشسته عابد بیمار</p></div>
<div class="m2"><p>میان بستر تب غیر گریه کار ندارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرفتم آن که پس از تو رضا شود به اسیری</p></div>
<div class="m2"><p>توان اینکه به اشتر شود سوار ندارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سکینه را بنشان در کنار خویش زمانی</p></div>
<div class="m2"><p>که تاب دوری باب بزرگوار ندارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بده تسلی لیلی برای خاطر اکبر</p></div>
<div class="m2"><p>که دادغر و غریب است و غمگسار ندارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مبر به جانب میدان علی اصغر خود را</p></div>
<div class="m2"><p>که طفل طاقت پیکان آب دار ندارد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر به شام بود یا به کوفه یا مدینه</p></div>
<div class="m2"><p>به هیچ وجه دل پر ز خون قرار ندارد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بکن سفارش طفلت رقیه بر پر سعد</p></div>
<div class="m2"><p>که تاب سیلی شمر ستم شعار ندارد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا اسیر یزید کردی و رفتی</p></div>
<div class="m2"><p>مرو که داد رسمی زینب فکار ندارد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شها شفاعت (صامت) نما بروز قیامت</p></div>
<div class="m2"><p>که جز تو چشم به ابناء روزگار ندارد</p></div></div>