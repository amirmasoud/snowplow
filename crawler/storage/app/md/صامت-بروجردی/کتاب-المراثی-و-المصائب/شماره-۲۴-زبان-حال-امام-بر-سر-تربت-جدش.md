---
title: >-
    شمارهٔ ۲۴ - زبان حال امام بر سر تربت جدش
---
# شمارهٔ ۲۴ - زبان حال امام بر سر تربت جدش

<div class="b" id="bn1"><div class="m1"><p>سری بهر تماشا از لحد بر گیر یا جدا</p></div>
<div class="m2"><p>که از کویت روم با ناله شبگیر یا جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طلب کردند قوم کوفیان ما را به مهمانی</p></div>
<div class="m2"><p>ولی دایم فلک دارد سر تدبیر یا جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدین زودی نمی‌رفتم ز کویت لیک ترسیدم</p></div>
<div class="m2"><p>که در امر الهی اوفتد تاخیر یا جدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جفاهایی که دارد زاده مرجانه در خاطر</p></div>
<div class="m2"><p>نموده پیش پیش اندر دلم تاثیر یا جدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یقین دارم که اندر این سفر بایست گردیدن</p></div>
<div class="m2"><p>علی اکبر من طعمه شمشیر با جدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یقین دانم ز قحط آب طفل شیر خوار من</p></div>
<div class="m2"><p>علی اصغر شود سیراب ز آب تیر یا جدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یقین دارم که از قطع دو دست حضرت عباس</p></div>
<div class="m2"><p>ز جان خود کنندم قوم کوفی سیر یا جدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یقین دارم که از قطع دو دست حضرت عباس</p></div>
<div class="m2"><p>ز جان خود کنندم قوم کوفی سیر یا جدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یقین دارم که باید زینب و کلثوم را بستن</p></div>
<div class="m2"><p>پس از من آن دو تن را در غل و زنجیر یا جدا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر گر برسنان خواهدشدن یا گوشه مطبخ</p></div>
<div class="m2"><p>به هر صورت نمی‌پیچم سر از تقصیر یا جدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به بزم شرب خواهد زد یزید بی‌حیا بر لب</p></div>
<div class="m2"><p>مرا چوب جفا بی‌جرم و بی‌تقصیر یا جدا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به محشر کن شفاعت از سلک مداح خود (صامت)</p></div>
<div class="m2"><p>که سرافکنده از شرم گنه بر زیر یا جد</p></div></div>