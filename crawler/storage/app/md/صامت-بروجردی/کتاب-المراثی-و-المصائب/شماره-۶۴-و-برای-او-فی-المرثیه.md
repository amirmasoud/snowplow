---
title: >-
    شمارهٔ ۶۴ - و برای او فی المرثیه
---
# شمارهٔ ۶۴ - و برای او فی المرثیه

<div class="b" id="bn1"><div class="m1"><p>فلک امان ز تو و بی‌حساب کردن تو</p></div>
<div class="m2"><p>ستم به عترت ختمی مآب کردن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن عمارت و آبادی‌ات به کشور شام</p></div>
<div class="m2"><p>وز آن مدینه و بطحا خراب‌کردن تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا روم؟ ببرم در جهان بنزد کهداد</p></div>
<div class="m2"><p>ز ظلم بر پسر بوتراب کردن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به قتلگه سر نعش حسین زینب گفت</p></div>
<div class="m2"><p>فدای جانب میدان شتاب کردن تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمود خواهرت اسب شهادتت رازین</p></div>
<div class="m2"><p>فدای حالت پا در رکاب کردن تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل شکسته زینب همیشه باید سوخت</p></div>
<div class="m2"><p>به استغاثه ز قلب کباب کردن تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فدای گردن کج ماندن و تن تنها</p></div>
<div class="m2"><p>به اهل کوفه سئوال و جواب کردن تو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حیرتم که چرا زنده ماندم و دیدم</p></div>
<div class="m2"><p>به زیر خنجر شمر اضطراب کردن تو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قسم به جان تو کز خاطرم نخواهد رفت</p></div>
<div class="m2"><p>ز شمر خواهش یک قطره آب کردن تو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برادرا شب دامادی علی اکبر</p></div>
<div class="m2"><p>فدای شادی و از خون خضاب کردن تو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بزیر تیغ فدای نظاره حسرت</p></div>
<div class="m2"><p>به سوی خواهر بی‌صبر و تاب‌کردن تو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>فدای پیکر در آفتاب مانده تو</p></div>
<div class="m2"><p>دگر بخواب به قربان خواب کردن تو</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به ماتم شه لب تشنه گریه کن (صامت)</p></div>
<div class="m2"><p>که بلکه شرم کنند از عذاب کردن تو</p></div></div>