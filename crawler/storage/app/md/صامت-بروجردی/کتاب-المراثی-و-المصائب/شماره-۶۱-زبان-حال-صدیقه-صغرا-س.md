---
title: >-
    شمارهٔ ۶۱ - زبان حال صدیقه صغرا(س)
---
# شمارهٔ ۶۱ - زبان حال صدیقه صغرا(س)

<div class="b" id="bn1"><div class="m1"><p>ای برادر تو پناه من گریان بودی</p></div>
<div class="m2"><p>در صف ماریه غمخوار یتیمان بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دانم از رفتن تو جان ز تنم خواهد رفت</p></div>
<div class="m2"><p>زآنکه اندر بدنم تو به جهان جان بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر غمم بود ز دیدار تو از دل می‌رفت</p></div>
<div class="m2"><p>دردی ار داشتم از لطف تو درمان بودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمدم تا به مدینه به سوی کرب و بلا</p></div>
<div class="m2"><p>همه جا یار من زار پریشان بودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون علی اکبر و عباس ز دستم رفتند</p></div>
<div class="m2"><p>مایه صبر من بی‌سر و سامان بودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد جد و پدر مادرم ای تشنه جگر</p></div>
<div class="m2"><p>مونس خواهر دلخسته و نالان بودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آخر از تشنه لبی سیر ز جان گردیدی</p></div>
<div class="m2"><p>با وجودی که تو در ماریه مهمان بودی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمر را در دل به یتیمان تو کی خواهد سوخت</p></div>
<div class="m2"><p>آن بودی که پرستار یتیمان بودی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاتل تو به لب تشنه تو رحم نکرد</p></div>
<div class="m2"><p>آخر ای سبط پیمبر تو مسلمان بودی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بود جای تو در آغوش نبی بر سر خاک</p></div>
<div class="m2"><p>نی چنین بی‌سر و صدا پاره و عریان بودی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>(صامتا) شکر خداوند که در مدت عمر</p></div>
<div class="m2"><p>روز و شب نوحه‌گر شاه شهیدان بودی</p></div></div>