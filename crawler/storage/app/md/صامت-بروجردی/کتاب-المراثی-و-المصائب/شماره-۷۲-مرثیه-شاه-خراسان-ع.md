---
title: >-
    شمارهٔ ۷۲ - مرثیه شاه خراسان(ع)
---
# شمارهٔ ۷۲ - مرثیه شاه خراسان(ع)

<div class="b" id="bn1"><div class="m1"><p>چو شاه طوس در ملک خراسان</p></div>
<div class="m2"><p>ز سوز زهر شد حال پریشان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داد پر اضطراب و چشم گریان</p></div>
<div class="m2"><p>زبان حال می‌فرمود نالان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>الهییا الهی من غریبم</p></div>
<div class="m2"><p>به غربت بی‌پرستار و طبیبم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو هستی با خبر ای حی بی‌چون</p></div>
<div class="m2"><p>ز احوال من و این قلب پر خون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>الهی الامان از جور مامون</p></div>
<div class="m2"><p>دم مردن به غربت چون کنم چون</p></div></div>
<div class="b2" id="bn6"><p>الهی یا الهی من غریبم</p>
<p>به غربت بی‌پرستار و طبیبم</p></div>
<div class="b" id="bn7"><div class="m1"><p>دلم بهر وطن در اضطرابست</p></div>
<div class="m2"><p>ز زهر جانگزا قلب کبابست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به بالینم اجل اندر شتابست</p></div>
<div class="m2"><p>به غربت کوکب بختم به خوابست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الهی یا الهی من غریبم</p></div>
<div class="m2"><p>به غربت بی‌پرستار و طبیبم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا کرد از وطن مامون خونخوار</p></div>
<div class="m2"><p>به شهر طوس بی‌یار و هوادار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به وقت مردن از بیداد اشرار</p></div>
<div class="m2"><p>مرا نبود انیس و مونس ویار</p></div></div>
<div class="b2" id="bn12"><p>الهی یا الهی من غریبم</p>
<p>به غربت بی‌پرستار و طبیبم</p></div>
<div class="b" id="bn13"><div class="m1"><p>چه کردم من مگر غیر از هدایت</p></div>
<div class="m2"><p>که شد زخم درونم بی‌نهایت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ندارم چون کسی بهر حمایت</p></div>
<div class="m2"><p>ز مامون می‌کنم با تو شکایت</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>الهی یا الهی من غریبم</p></div>
<div class="m2"><p>به غربت بی‌پرستار و طبیبم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدایا جز تو من یاری ندارم</p></div>
<div class="m2"><p>در این کشور مددکاری ندارم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به غیر از چشم خونباری ندارم</p></div>
<div class="m2"><p>غم بسیار و غمخواری ندارم</p></div></div>
<div class="b2" id="bn18"><p>الهی یا الهی من غریبم</p>
<p>به غرب بی‌پرستار و طبیبم</p></div>
<div class="b" id="bn19"><div class="m1"><p>بخشت بی‌کسی باشد سر من</p></div>
<div class="m2"><p>به خاک طوس مانده پیکر من</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نباشد وقت مردن بر سر من</p></div>
<div class="m2"><p>تقی نور دو چشمان تر من</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>الهی یا الهی من غریبم</p></div>
<div class="m2"><p>به غربت بی‌پرستار و طبیبم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ندارم قاصدی تا از خراسان</p></div>
<div class="m2"><p>فرستم در وطن با چشم گریان</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بگوید با تقی که ای مونس جان</p></div>
<div class="m2"><p>سر قبرم بیا با آه و افغان</p></div></div>
<div class="b2" id="bn24"><p>الهی یا الهی من غریبم</p>
<p>به غربت بی‌پرستار و طبیبم</p></div>
<div class="b" id="bn25"><div class="m1"><p>صبا سوی مدینه رو ز یاری</p></div>
<div class="m2"><p>به معصومه بگو با آه و زاری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که بنما در عزابم اشکباری</p></div>
<div class="m2"><p>خبر از حالم ای خواهر نداری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الهی یا الهی من غریبم</p></div>
<div class="m2"><p>به غربت بی‌پرستار و طبیبم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کسی نبود کند برپا عزابم</p></div>
<div class="m2"><p>زند بر سینه و سر از برایم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ببندد در غریبی چشمهایم</p></div>
<div class="m2"><p>سوی قبله کشد از مهر پایم</p></div></div>
<div class="b2" id="bn30"><p>الهی یا الهی من غریبم</p>
<p>به غربت بی‌پرستار و طبیبم</p></div>
<div class="b" id="bn31"><div class="m1"><p>دریغ از راه دور و عمر کوتاه</p></div>
<div class="m2"><p>به بالینم اجل آمد به ناگاه</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو (صامت) چشم گریان با غم و آه</p></div>
<div class="m2"><p>برای دوستانم مانده در راه</p></div></div>
<div class="b2" id="bn33"><p>الهی یا الهی من غریبم</p>
<p>به غربت بی‌پرستار و طبیبم</p></div>