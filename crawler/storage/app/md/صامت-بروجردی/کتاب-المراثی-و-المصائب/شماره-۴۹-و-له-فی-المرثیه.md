---
title: >-
    شمارهٔ ۴۹ - و له فی المرثیه
---
# شمارهٔ ۴۹ - و له فی المرثیه

<div class="b" id="bn1"><div class="m1"><p>گر شاه دین هوای شفاعت بسر نداشت</p></div>
<div class="m2"><p>از روزگار این همه خون جگر نداشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زخمهای کاری و از داغهای دل</p></div>
<div class="m2"><p>کاری به جز رضای خدا در نظر نداشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمن ز صدهزار فزون بود و آنجناب</p></div>
<div class="m2"><p>غیر از خدا پناه و معینی دگر نداشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌خواست تا به بال شهادت پرد بخلد</p></div>
<div class="m2"><p>بیچاره تیر بر تن خود بال و پر نداشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ارض و سما ز محنت او باخبر شدند</p></div>
<div class="m2"><p>وینطرفه‌تر که خویشتن از خود خبر نداشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در زیر تیغ داتش مناجات با خدا</p></div>
<div class="m2"><p>آری دگر به غیر خدا راه بر نداشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس که داشت یاد خداوند در نظر</p></div>
<div class="m2"><p>دیگر غم برادر و فکر پسر نداشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد سنگ خاره آب ز سوز گلوی او</p></div>
<div class="m2"><p>و آهش به قلب شمر ستمگر اثر نداشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زینب که از مدینه روان شد به کربلا</p></div>
<div class="m2"><p>گویا خبر ز آمدن این سفر نداشت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همره نبرده بود حسین گر سکینه را</p></div>
<div class="m2"><p>در قتلگاه بر سر خود نوحه‌گر نداشت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیلا اگر به کرببلا بود یا به شام</p></div>
<div class="m2"><p>جز رود رود اکبر والا گهر نداشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>(صامت) ز محنت شه لب تشنه روز و شب</p></div>
<div class="m2"><p>یک دم نشد که دل ز خدنگی سپر نداشت</p></div></div>