---
title: >-
    شمارهٔ ۲۷ - زبان حال حضرت زینب(ع)
---
# شمارهٔ ۲۷ - زبان حال حضرت زینب(ع)

<div class="b" id="bn1"><div class="m1"><p>ای بی‌کفن فدای تو جسم اطهرت</p></div>
<div class="m2"><p>زینب شو فدایی ببریده حنجرت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای کاش خواهر تو نمی‌دید اینچنین</p></div>
<div class="m2"><p>بر نوک نی سر تو و صدپاره پیکرت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آوردی ام به کرب و بلا بی‌کس و غریب</p></div>
<div class="m2"><p>خوش می‌کنی غریب نوازی ز خواهرت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بردار سر ز خهاک و بپرس از من فکار</p></div>
<div class="m2"><p>کای زینب ستمزده، کو کهنه معجرت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمر این قدر نداشت مروت که بعد قتل</p></div>
<div class="m2"><p>بیرون نمود پیرهن کهنه از برت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کافی نبود زخم تنت از سم ستور</p></div>
<div class="m2"><p>کرد ابن سعد سرمه صفت جسم اطهرت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چندان امان نمی‌دههدم شمر بی‌پدر</p></div>
<div class="m2"><p>تا قاصدی روانه کنم نزد مادرت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوید که یا بتول سوی کربلا بیا</p></div>
<div class="m2"><p>غلطان به خون ببین بدن نازپرورت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از بهر گوشواره دریدند کوفیان</p></div>
<div class="m2"><p>گوش عروس فاطمه زار دخترت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رفتم دگر، ولی بود این داغ بر دلم</p></div>
<div class="m2"><p>کز بعد من چه آید از این قوم بر سرت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>(صامت) شدی چه نوحه‌گر ماتم حسین</p></div>
<div class="m2"><p>دیگر بود چه واهمه از روز محشرت؟</p></div></div>