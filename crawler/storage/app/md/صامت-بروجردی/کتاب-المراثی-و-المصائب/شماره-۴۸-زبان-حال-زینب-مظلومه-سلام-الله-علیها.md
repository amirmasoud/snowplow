---
title: >-
    شمارهٔ ۴۸ - زبان حال زینب مظلومه سلام الله علیها
---
# شمارهٔ ۴۸ - زبان حال زینب مظلومه سلام الله علیها

<div class="b" id="bn1"><div class="m1"><p>شوم فدای تن بی‌کفن به روی ترابت</p></div>
<div class="m2"><p>به خواب جان برادر که گشته موسوم خوابت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پی وداع تو با اشک و آه آمده زینب</p></div>
<div class="m2"><p>ز من بپرس که خواهر که برده است نقابت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر کفن ننهادم به پیکر تو و رفتم</p></div>
<div class="m2"><p>مرا ببخش که شرمنده‌ام ز روی جنابت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاطرم نرود تا به روز حشر برادر</p></div>
<div class="m2"><p>ز درد تشنه لبی پیچ و تاب قلب کبابت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو ابن سعد نمی‌داد بر تو قطره آبی</p></div>
<div class="m2"><p>دگر برای چه آن بی‌حیا نداد جوابت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشید شمر به حلق تو دشنه با لب تشنه</p></div>
<div class="m2"><p>نداد قطره آبی چرا ز راه ثوابت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرفته وعده مهمانی از تو خولی و ترسم</p></div>
<div class="m2"><p>ز کنج مطبخ آن میزان خانه خرابت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به حیرتم که رسم بعد از این کجا به وصالت</p></div>
<div class="m2"><p>به شهر شام بود وعَده یا بیزم شرابت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دگر ز معصیت خود مترس(صامت) محزون</p></div>
<div class="m2"><p>که هست شاه شهیدان شفیع روز حسابت</p></div></div>