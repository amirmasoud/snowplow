---
title: >-
    شمارهٔ ۲۳ - رسیدن خبر شهدای کربلا به مدینه
---
# شمارهٔ ۲۳ - رسیدن خبر شهدای کربلا به مدینه

<div class="b" id="bn1"><div class="m1"><p>رسید نامه فتح عبید زشت بد اختر</p></div>
<div class="m2"><p>ز شهر کوفه ویرانه به مدینه اطهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برای خواندن آن نامه روسیاه خطیبی</p></div>
<div class="m2"><p>بسان سکه خارج نمود جای به منبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوشته بود که الیوم دور دور یزید است</p></div>
<div class="m2"><p>که رو به کوتهی آورد روز آل پیمبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوشته بود که با خلق تشنه بر لب دریا</p></div>
<div class="m2"><p>بریده شد ز تن شهریار تشنه لبان سر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوشته بود که از منبع آب تا به قیامت</p></div>
<div class="m2"><p>به جان عترت حیدر فکندم از عطش آذر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوشته بود که اندر شب عروسی قاسم</p></div>
<div class="m2"><p>حنای عیش ز خون بست نوعروس مکدر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشته بود که لیلی ز بی‌کسی شده مجنون</p></div>
<div class="m2"><p>به خون طپیده ز شمشیر کین چو قامت اکبر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوشته بود که عباس بهر قطره آبی</p></div>
<div class="m2"><p>ز دست خصم شد او را جدا دو دست ز پیکر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نوشته بود به اصغر کسی نکرد ترحم</p></div>
<div class="m2"><p>نشان تیر ستم بیگناه ساخت چو اصغر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نوشته بود که چو نشد حسین سوار به مرکب</p></div>
<div class="m2"><p>رکابداری او را نمود زینب مضطر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نوشته بود که کردند منع آب روان را</p></div>
<div class="m2"><p>ز اهل بیت رسول و حریم ساقی کوثر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نوشته بود تنی را که بود دوش نبی جا</p></div>
<div class="m2"><p>به خاک راه فکندند همچو مهر منور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نوشته بود که پرورده رسول خدا را</p></div>
<div class="m2"><p>شکست سینه‌اش از ضرب چکمه شمر ستمگر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نوشته بود که مرهم به زخ او بنهادیم</p></div>
<div class="m2"><p>ز بعد قتل ز سم ستورچابک و رهور</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نوشته بود که کردیم غارت از حرم او</p></div>
<div class="m2"><p>ز گوش‌های زنان گوشوار با زر و زیور</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نوشته بود که در پیش چشم کوفی و شامی</p></div>
<div class="m2"><p>به فرق عترت طه نماند چادر و معجر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوشته بود که گردید میزبان شه دین</p></div>
<div class="m2"><p>به شهر کوفه میان تنور خولی ابتر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نوشته بود که دادند نان برسم تصدق</p></div>
<div class="m2"><p>به کودکان غریب حسین بی‌کس و یاور</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نوشته بود که بستند از برای اسیری</p></div>
<div class="m2"><p>حریم ختم رسل را به یک طناب سراسر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نوشته بود سر سوران یثرب و بطحا</p></div>
<div class="m2"><p>به نوک نیزه سوی شام رفت با رخ انور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نوشته بود که لعب لب حسین علی را</p></div>
<div class="m2"><p>کبود کرد یزید لعین ز ضربت خیزر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کنون که لال نگردی ز شرح این غم عظمی</p></div>
<div class="m2"><p>بسوز (صامت) محزون بساز تا صف محشر</p></div></div>