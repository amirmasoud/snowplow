---
title: >-
    شمارهٔ ۲۲ - به شاهد لغت خبزدو، بمعنی جعل
---
# شمارهٔ ۲۲ - به شاهد لغت خبزدو، بمعنی جعل

<div class="b" id="bn1"><div class="m1"><p>آن روی و ریش پر گه و پر بلغم و خدو</p></div>
<div class="m2"><p>همچون خبزدوئی که شود زیر پای پخچ</p></div></div>