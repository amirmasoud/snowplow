---
title: >-
    شمارهٔ ۲۳ - به شاهد لغت دند، بمعنی ابله و بی باک و خود کامه
---
# شمارهٔ ۲۳ - به شاهد لغت دند، بمعنی ابله و بی باک و خود کامه

<div class="b" id="bn1"><div class="m1"><p>اندرین شهر بسی ناکس برخاسته اند</p></div>
<div class="m2"><p>همه خر طبع و همه احمق و بی دانش و دند</p></div></div>