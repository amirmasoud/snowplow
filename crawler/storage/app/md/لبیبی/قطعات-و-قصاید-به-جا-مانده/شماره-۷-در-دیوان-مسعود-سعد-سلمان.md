---
title: >-
    شمارهٔ ۷ - در دیوان مسعود سعد سلمان
---
# شمارهٔ ۷ - در دیوان مسعود سعد سلمان

<div class="n" id="bn1"><p>مسعود سعد سلمان شاعر نامی در قصیدتی به مطلع:</p></div>
<div class="b" id="bn2"><div class="m1"><p>بنظم و نثر کسی را گر افتخار سزاست</p></div>
<div class="m2"><p>مرا سزاست که امروز نظم و نثر مراست</p></div></div>
<div class="n" id="bn3"><p>به استاد لبیبی و مصراعی از شعر وی تضمین کند و گوید:</p></div>
<div class="b" id="bn4"><div class="m1"><p>در این قصیده که گفتم من اقتفا کردم</p></div>
<div class="m2"><p>باوستاد لبیبی که سیدالشعراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر آن طریق بنا کردم این قصیده که گفت:</p></div>
<div class="m2"><p>«سخن که نظم کنند آن درست باید و راست »</p></div></div>
<div class="n" id="bn6"><p>این متن کامل‌تر این قصیده است که «امید سروری» در «سفینهٔ ترمذ» گردآمدهٔ «محمد بن یغمور» احتمالاً مربوط به قرن هشتم یا بعد از آن یافت است. به نظر می‌رسد قصیده خطاب به «عنصری» سروده شده باشد.</p></div>
<div class="n" id="bn7"><p>قصیده‌ای که مسعود سعد سلمان ذکر او کرد این استاد لبیبی است -رحمة الله علیهما- و آن استاد لبیبی در ایام سامانیان و محمدی سیدالشعر بوده است، خاصه در عصر سلطان محمود -نورالله مرقده</p></div>
<div class="b" id="bn8"><div class="m1"><p>سخن که نظم دهند آن درست باید و راست</p></div>
<div class="m2"><p>طریق نظم درست اندر این زمانه جزاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سخن که من بنگارم به نظم اگر دگری</p></div>
<div class="m2"><p>به نثر خوب گزارد چنان گزارد راست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حسن خالی دارم ز لفظ ناقص پاک</p></div>
<div class="m2"><p>درست و راست ز بایسته نه فزون نه به کاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرا سخن به بلندی سماست و معنی‌ها</p></div>
<div class="m2"><p>از او درفشان گویی که آفتاب سماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به صنعت و به معانی و نازکی و خوشی</p></div>
<div class="m2"><p>یکی قصیدهٔ من ... شعراست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>وگر گواهی خواهد یکی بر این دعوی</p></div>
<div class="m2"><p>همین قصیده بدین بدین گفت من بسنده گواست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مرا چه باید گفت این سخن که نیک افتاد</p></div>
<div class="m2"><p>چو آفتاب درفشان ز آسمان پیداست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به صنعت است روان شعر چو جان در تن</p></div>
<div class="m2"><p>بلی و آن دگر کس به سان باد رواست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ایا گروهی کاین شعرها همی‌خوانیت</p></div>
<div class="m2"><p>به حلق و حنجره گویی که زیر باد دو تاست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مرا به سوی شما آب نیست و مرتبه نیست</p></div>
<div class="m2"><p>سوی شما همه جاه و بزرگی آن کس راست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که شعرهاش چو تعویذهای کالبدی‌ست</p></div>
<div class="m2"><p>درست است نماینده نه درست و نه راست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به شعرهای لبیبی شما نگاه کنیت</p></div>
<div class="m2"><p>که شعرهای لبیبی چه بابت عقلاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همیشه رغبت اهل هنر به شعر من است</p></div>
<div class="m2"><p>به سوی اوست شما را همیشه میل و رواست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دسته‌های ریاحین کی التفات کند</p></div>
<div class="m2"><p>ستور سرزده جایی که دسته‌های گیاست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>مرا بگوی که یک شعر نیک بایسته</p></div>
<div class="m2"><p>کزو مثل زد شاید ز گفته‌هاش کجاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>نه هر ه نظمی دارد ز گفته‌ها نیک است</p></div>
<div class="m2"><p>نه هر چه رنگش باشد ز جامه‌ها دیباست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ز مشک و زلف وزآن کاربسته معنی‌ها</p></div>
<div class="m2"><p>چه خوشی و چه شگفتی وزآن چه خواهد خاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به نظم و نثر سخن را نهایتی باید</p></div>
<div class="m2"><p>کزو مثل زد شاید که زین چه گفت و چه خواست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر این طریق بگویش که یک دو بیت بگوی</p></div>
<div class="m2"><p>بر این قیاس که من گفته‌ام گرش یاراست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صفات مشک مگوی و ز زلف یاد مکن</p></div>
<div class="m2"><p>اگر توانی دانم که این قصیده تو راست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جز آن قصیده که از روزگار برنایی</p></div>
<div class="m2"><p>که کار پیر نه چون کار مردم برناست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وگر به خواسته آراسته نشده تن تو</p></div>
<div class="m2"><p>رواست کایزد جان مرا به علم آراست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بدان که بی‌خردی را درم فزون باشد</p></div>
<div class="m2"><p>به فضل کی آخر برابر داناست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به هیچ حال ابوجهل چون محمد نیست</p></div>
<div class="m2"><p>وگر چه هر دو به نسبت ز آدم و حواست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا ز دانش رنج تن است و راحت جان</p></div>
<div class="m2"><p>شناخته مثل است این که خار با خرماست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>مرا بی‌درمی ویحکا چه طعنه زتی</p></div>
<div class="m2"><p>بدان قدر که بسنده‌ست حال من به نواست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به هیچ وقتی آزار تو نجستم من</p></div>
<div class="m2"><p>تویی که سوی منت سال و ماه قصد جفاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به طبع دشمن آنی که دانشی دارد</p></div>
<div class="m2"><p>شگفت نیست که ظلمت میشه ضد ضیاست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به شعرت چه عطای بزرگ داد ملک</p></div>
<div class="m2"><p>هنر نه از توست آن پادشا بزرگ عطاست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به سیم خواستن و یافتن چه فخر کنی</p></div>
<div class="m2"><p>تفاخر آن کاو را مکارم است و سخاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو هر چه یافته‌ای من ندانم آن دانم</p></div>
<div class="m2"><p>که نظم و نثر تو یکسر معلل است و خطاست</p></div></div>