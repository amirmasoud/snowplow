---
title: >-
    غزل شمارهٔ ۱
---
# غزل شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای پریشان کرده عمدا، زلف عنبربیز را</p></div>
<div class="m2"><p>بر دل من دشنه داده غمزهٔ خونریز را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد فروزان آتش سودایت اندر جان و دل</p></div>
<div class="m2"><p>درفکن در جام بی رنگ، آب رنگ آمیز را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می پیاپی، بی محابا ده، میندیش از حریف</p></div>
<div class="m2"><p>یاد می‌دار این دو بیت گفته دست آویز را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر حریفی از دمادم سر بپیچاند رواست</p></div>
<div class="m2"><p>بر کف من نه، که پور زال به شبدیز را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان من می را و قالب خاک را و دل تو را</p></div>
<div class="m2"><p>وین سر طناز پر وسواس تیغ تیز را</p></div></div>