---
title: >-
    رباعی شمارهٔ ۷۰
---
# رباعی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>هفتاد و دو فرقه در رهت می‌پویند</p></div>
<div class="m2"><p>هر یک سخنان مختلف می‌جویند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سررشتهٔ حق به دست یک طایفه است</p></div>
<div class="m2"><p>باقی به خوش آمد سخنی می‌گویند</p></div></div>