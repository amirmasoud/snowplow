---
title: >-
    رباعی شمارهٔ ۸۳
---
# رباعی شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>هر گه که دلم با غمت انباز شود </p></div>
<div class="m2"><p>صد در ز طرب بر رخ من باز شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به زان نبود که جان فدای تو کنم </p></div>
<div class="m2"><p>تیهو چو فدای باز شود باز شود</p></div></div>