---
title: >-
    رباعی شمارهٔ ۱۸۲
---
# رباعی شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>ای آن که خلاصهٔ چهار ارکانی</p></div>
<div class="m2"><p>بشنو سخنی ز عالم روحانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیوی و ددی و ملکی، انسانی</p></div>
<div class="m2"><p>با توست هر آنچه می نمایی، آنی</p></div></div>