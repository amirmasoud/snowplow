---
title: >-
    رباعی شمارهٔ ۱۱۸
---
# رباعی شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>از نه پدر و چهار مادر زادم</p></div>
<div class="m2"><p>از هفت و دو و سه، مستمند و شادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پنج اصلم و در خانهٔ شش بنیادم</p></div>
<div class="m2"><p>من در کف این گروه چون افتادم؟</p></div></div>