---
title: >-
    رباعی شمارهٔ ۱۷۹
---
# رباعی شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>ای دل، ز شراب جهل، مستی تا کی؟</p></div>
<div class="m2"><p>وی نیست شونده، لاف هستی تا کی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای غرقهٔ بحر غفلت، ار ابر نه‌ای</p></div>
<div class="m2"><p>تر دامنی و هوا پرستی تا کی؟</p></div></div>