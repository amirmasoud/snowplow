---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>درویش کسی بود که نامش نبود</p></div>
<div class="m2"><p>گامی که نهد مراد و کامش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آتش فقر اگر بسوزد شب و روز </p></div>
<div class="m2"><p>هرگز طمع پخته و خامش نبود</p></div></div>