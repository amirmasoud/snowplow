---
title: >-
    رباعی شمارهٔ ۳۶
---
# رباعی شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>در هر برزن که بنگرم آشوبی ست</p></div>
<div class="m2"><p>آشوب شکنجه ای و زخم چوبی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا پاک کنند گیتی از یک دیکر</p></div>
<div class="m2"><p>هر ریش که هست، بر زنخ جارویی ست</p></div></div>