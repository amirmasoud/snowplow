---
title: >-
    رباعی شمارهٔ ۱۲۶
---
# رباعی شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>از روی تو شاد شد دل غمگینم</p></div>
<div class="m2"><p>من چون رخ تو به دیگری بگزینم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تو نگرم، صورت خود می یابم</p></div>
<div class="m2"><p>در خود نگرم، همه تو را می بینم</p></div></div>