---
title: >-
    رباعی شمارهٔ ۱۷۱
---
# رباعی شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>یا رب ز قضا بر حذرم می‌داری</p></div>
<div class="m2"><p>وز حادثه ها بی خبرم می‌داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند ز من بیش بدی می‌بینی</p></div>
<div class="m2"><p>هر دم ز کرم نکوترم می‌داری</p></div></div>