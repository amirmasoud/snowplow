---
title: >-
    رباعی شمارهٔ ۱۲۵
---
# رباعی شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>من با تو نظر از سر هستی نکنم</p></div>
<div class="m2"><p>اندیشه ز بالا و ز پستی نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌بینم و می‌پرستم از روی یقین</p></div>
<div class="m2"><p>خود بینی و خویشتن پرستی نکنم</p></div></div>