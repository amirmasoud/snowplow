---
title: >-
    رباعی شمارهٔ ۱۸۷
---
# رباعی شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>تا خاص خدای را تو از جان نشوی</p></div>
<div class="m2"><p>بر مرکب عشق مرد میدان نشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیران جهان پیش تو روبه باشند</p></div>
<div class="m2"><p>گر تو سگ نفس را به فرمان نشوی</p></div></div>