---
title: >-
    رباعی شمارهٔ ۱۰۰
---
# رباعی شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p> بیرون ز چهار عنصر و پنج حواس</p></div>
<div class="m2"><p>از شش جهت و هفت خط و هشت اساس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سری است نهفته در میان خانهٔ جان</p></div>
<div class="m2"><p>کان را نتوان یافت به تقلید و قیاس</p></div></div>