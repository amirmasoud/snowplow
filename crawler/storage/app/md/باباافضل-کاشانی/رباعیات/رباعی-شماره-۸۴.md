---
title: >-
    رباعی شمارهٔ ۸۴
---
# رباعی شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>تا دل ز علایق جهان حُرّ نشود</p></div>
<div class="m2"><p>هرگز صدف وجود پُر دُر نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پر می نشود کاسهٔ سرها از عقل</p></div>
<div class="m2"><p>هر کاسه که سر نگون بود، پر نشود</p></div></div>