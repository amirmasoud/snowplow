---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>گر ملک تو مصر و شام و چین خواهد بود</p></div>
<div class="m2"><p>و آفاق تو را زیر نگین خواهد بود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش که عاقبت نصیب من و تو </p></div>
<div class="m2"><p>ده گز کفن و دو گز زمین خواهد بود</p></div></div>