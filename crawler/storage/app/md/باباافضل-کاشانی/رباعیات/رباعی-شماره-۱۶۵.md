---
title: >-
    رباعی شمارهٔ ۱۶۵
---
# رباعی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>تا کی پی اسباب و تنعم گردی؟</p></div>
<div class="m2"><p>تا چند دَرِ سرای مردم گردی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دایرهٔ وجود تو دایره ای ست</p></div>
<div class="m2"><p>زین دایره گر برون روی گم گردی</p></div></div>