---
title: >-
    رباعی شمارهٔ ۱۵۲
---
# رباعی شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>گر مغز همه بینی و گر پوست همه</p></div>
<div class="m2"><p>هان تا نکنی کج نظری، کوست همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو دیده نداری که درو در نگری</p></div>
<div class="m2"><p>ورنه ز سرت تا به قدم اوست همه</p></div></div>