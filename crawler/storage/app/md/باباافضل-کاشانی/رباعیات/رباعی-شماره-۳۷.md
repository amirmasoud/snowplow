---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>چندین غم مال و حسرت دنیا چیست</p></div>
<div class="m2"><p>هرگز دیدی کسی که جاوید بزیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یک نفسی که در تنت عاریتی ست</p></div>
<div class="m2"><p>با عاریتی، عاریتی، باید زیست</p></div></div>