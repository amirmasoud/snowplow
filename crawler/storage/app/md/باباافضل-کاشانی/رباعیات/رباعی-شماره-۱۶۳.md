---
title: >-
    رباعی شمارهٔ ۱۶۳
---
# رباعی شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>مردی باید، بلند همت مردی</p></div>
<div class="m2"><p>زین تجربه دیده ای، خرد پروردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو را به تصرف اندرین عالم خاک</p></div>
<div class="m2"><p>بر دامن همت ننشیند گردی </p></div></div>