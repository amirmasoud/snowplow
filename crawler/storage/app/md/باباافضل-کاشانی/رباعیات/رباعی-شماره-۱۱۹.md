---
title: >-
    رباعی شمارهٔ ۱۱۹
---
# رباعی شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p> من مهر تو در میان جان ننهادم</p></div>
<div class="m2"><p>تا مهر تو بر سر زبان ننهادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دل ز همه جهان کرانه نگرفت</p></div>
<div class="m2"><p>با او سخن تو در میان ننهادم </p></div></div>