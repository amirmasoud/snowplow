---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای از همه آزرده، بی آزار گذر</p></div>
<div class="m2"><p>وای مست فریب بوده، هشیار گذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آرامگه نهنگ مرگ است دهنت</p></div>
<div class="m2"><p>بر خوابگه نهنگ، بیدار گذر</p></div></div>