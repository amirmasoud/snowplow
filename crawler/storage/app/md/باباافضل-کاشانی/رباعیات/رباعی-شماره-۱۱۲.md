---
title: >-
    رباعی شمارهٔ ۱۱۲
---
# رباعی شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>گر فضل کنی ندارم از عالم باک</p></div>
<div class="m2"><p>ور قهر کنی، شوم به یک بار هلاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی صد بار گویم ای صانع پاک</p></div>
<div class="m2"><p>مشتی خاکم، چه آید از مشتی خاک</p></div></div>