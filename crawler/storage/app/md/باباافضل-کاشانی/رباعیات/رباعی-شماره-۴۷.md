---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>هستی تو سزای این و صد چندین رنج</p></div>
<div class="m2"><p>تا با تو که گفت کاین همه بر خود سنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خوردن و خواستن بر آسا و بباش</p></div>
<div class="m2"><p>و آرام گزین که خفته ای بر سر گنج</p></div></div>