---
title: >-
    رباعی شمارهٔ ۱۴
---
# رباعی شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>با یار بگفتم به زبانی که مراست</p></div>
<div class="m2"><p>کز آرزوی روی تو جانم برخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا: قدمی ز آرزو زآن سو نه</p></div>
<div class="m2"><p>کاین کار به آرزو نمی آمد راست</p></div></div>