---
title: >-
    رباعی شمارهٔ ۴۰
---
# رباعی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>عشق تو ز هر بی خبری خالی نیست</p></div>
<div class="m2"><p>درد تو ز هر بی بصری خالی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که در خلق جهان می نگرم</p></div>
<div class="m2"><p>سودای تو از هیچ سری خالی نیست</p></div></div>