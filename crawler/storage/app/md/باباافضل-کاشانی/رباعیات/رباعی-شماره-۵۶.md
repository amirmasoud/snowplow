---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>چندان برو این ره که دویی برخیزد</p></div>
<div class="m2"><p>گر هست دویی، به رهروی برخیزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو او نشوی، ولی اگر جهد کنی</p></div>
<div class="m2"><p>جایی برسی کز تو، تویی برخیزد</p></div></div>