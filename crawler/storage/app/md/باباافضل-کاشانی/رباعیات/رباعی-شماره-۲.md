---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>اندوه تو دلشاد کند هر جان را </p></div>
<div class="m2"><p>کفر تو دهد تازگی ای ایمان را </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل راحت وصل تو مبیناد دمی</p></div>
<div class="m2"><p>با درد تو گر طلب کند درمان را</p></div></div>