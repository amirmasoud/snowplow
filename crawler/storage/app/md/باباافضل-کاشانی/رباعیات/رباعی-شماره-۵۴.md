---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>در عشق تو جان بوالهوس می میرد</p></div>
<div class="m2"><p>چون شعله ز انبوهی خس می میرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روزی که دلم به طره بستی، گفتم</p></div>
<div class="m2"><p>کاین مرغ آخر در این قفس می میرد</p></div></div>