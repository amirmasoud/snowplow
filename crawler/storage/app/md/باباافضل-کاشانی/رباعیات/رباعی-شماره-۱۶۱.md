---
title: >-
    رباعی شمارهٔ ۱۶۱
---
# رباعی شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>دعوی به سر زبان خود وابستی</p></div>
<div class="m2"><p>در خانه هزار بت، یکی نشکستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی به یک قول شهادت رستم</p></div>
<div class="m2"><p>فردات کند خمار، کامشب مستی</p></div></div>