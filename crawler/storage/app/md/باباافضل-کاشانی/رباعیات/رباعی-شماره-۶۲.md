---
title: >-
    رباعی شمارهٔ ۶۲
---
# رباعی شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>مردان رهت واقف اسرار تواند</p></div>
<div class="m2"><p>باقی همه سرگشتهٔ پرگار تواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفتاد و دو ملت همه در کار تواند</p></div>
<div class="m2"><p>تو با همه و همه طلبکار تو اند</p></div></div>