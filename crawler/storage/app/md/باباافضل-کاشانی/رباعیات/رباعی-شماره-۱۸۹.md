---
title: >-
    رباعی شمارهٔ ۱۸۹
---
# رباعی شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>تا دیدهٔ دل ز دیده‌ها نگشایی</p></div>
<div class="m2"><p>هرگز ندهند دیده‌ها بینایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>امروز از این شراب جامی در کش</p></div>
<div class="m2"><p>مسکین تو که در امید پس فردایی</p></div></div>