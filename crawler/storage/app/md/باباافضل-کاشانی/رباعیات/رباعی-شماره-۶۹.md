---
title: >-
    رباعی شمارهٔ ۶۹
---
# رباعی شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>برخیز که عاشقان به شب راز کنند</p></div>
<div class="m2"><p>گرد در و بام دوست پرواز کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جا که دری بود به شب در بندند</p></div>
<div class="m2"><p>الا در عاشقان که شب باز کنند</p></div></div>