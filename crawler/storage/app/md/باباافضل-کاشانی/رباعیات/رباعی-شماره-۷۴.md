---
title: >-
    رباعی شمارهٔ ۷۴
---
# رباعی شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>تا روی زمین و آسمان خواهد بود</p></div>
<div class="m2"><p>حیوان و نبات، هر دوان خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چرخ و سراسر اختران سیر کنند</p></div>
<div class="m2"><p>نقد تو خلاصهٔ جهان خواهد بود</p></div></div>