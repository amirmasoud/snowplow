---
title: >-
    رباعی شمارهٔ ۱۶۸
---
# رباعی شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>در آینهٔ جمال حق کن نظری</p></div>
<div class="m2"><p>تا جان و دلت بیابد از حق خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که دل و جانت منور گردد</p></div>
<div class="m2"><p>باید که به کویش گذری سحری</p></div></div>