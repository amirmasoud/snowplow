---
title: >-
    رباعی شمارهٔ ۴۶
---
# رباعی شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بر خود چه نهی رنج در این جای سِپَنج</p></div>
<div class="m2"><p>چون پای یقین نهاده ای بر سر گنج</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنشین به تأنی و بر آسا از رنج</p></div>
<div class="m2"><p>و آن گنج به معیار خرد بر خود سنج</p></div></div>