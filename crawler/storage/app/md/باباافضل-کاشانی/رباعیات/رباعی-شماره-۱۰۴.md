---
title: >-
    رباعی شمارهٔ ۱۰۴
---
# رباعی شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>هان ای دل بد زهره ز شمشیر مترس</p></div>
<div class="m2"><p>بفشار قدم، ز حملهٔ شیر مترس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ساحت این زمانهٔ عاریتی</p></div>
<div class="m2"><p>ز اقبال مشو شاد و ز ادبیر مترس</p></div></div>