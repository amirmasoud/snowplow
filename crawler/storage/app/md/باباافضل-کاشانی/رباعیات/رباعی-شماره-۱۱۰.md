---
title: >-
    رباعی شمارهٔ ۱۱۰
---
# رباعی شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p> زین تابش آفتاب و تاریکی میغ</p></div>
<div class="m2"><p>زین بیهده زندگانی مرگ آمیغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خویشتن آی تا نباشی باری</p></div>
<div class="m2"><p>نه بوده به افسوس و نه رفته به دریغ</p></div></div>