---
title: >-
    رباعی شمارهٔ ۶۴
---
# رباعی شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>در مصطبهٔ عمر ز بدنامی چند؟</p></div>
<div class="m2"><p>عاجز شده از سرزنش عامی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو قوت پایی که مرا گیرد دست</p></div>
<div class="m2"><p>تا پیش اجل باز روم گامی چند</p></div></div>