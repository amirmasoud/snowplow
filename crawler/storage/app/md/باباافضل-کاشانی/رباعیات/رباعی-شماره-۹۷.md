---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>تاریک شد از هجر دل افروزم، روز</p></div>
<div class="m2"><p>شب نیز شد از آه جهان سوزم، روز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شد روشنی از روز و سیاهی ز شبم</p></div>
<div class="m2"><p>اکنون نه شبم شب است، نه روزم روز</p></div></div>