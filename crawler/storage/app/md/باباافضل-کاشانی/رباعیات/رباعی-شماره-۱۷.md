---
title: >-
    رباعی شمارهٔ ۱۷
---
# رباعی شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>هر نقش که بر تختهٔ هستی پیداست</p></div>
<div class="m2"><p>آن صورت آن کس است کان نقش آراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریای کهن چو بر زند موجی نو</p></div>
<div class="m2"><p>موجش خوانند و در حقیقت دریاست</p></div></div>