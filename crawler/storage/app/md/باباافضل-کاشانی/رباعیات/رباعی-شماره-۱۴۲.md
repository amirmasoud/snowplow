---
title: >-
    رباعی شمارهٔ ۱۴۲
---
# رباعی شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>دل مغز حقیقت است و تن پوست، ببین</p></div>
<div class="m2"><p>در کسوت روح، صورت دوست ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چیز که آن نشان هستی دارد </p></div>
<div class="m2"><p>یا سایهٔ نور اوست، یا اوست، ببین</p></div></div>