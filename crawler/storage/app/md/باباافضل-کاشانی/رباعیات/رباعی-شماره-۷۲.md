---
title: >-
    رباعی شمارهٔ ۷۲
---
# رباعی شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>ای ذات تو در دو کون مقصود وجود</p></div>
<div class="m2"><p>نام تو محمد و مقامت محمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بر لب دریای شفاعت بستم</p></div>
<div class="m2"><p>زان روی روان می کنم از دیده دو رود</p></div></div>