---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>تا حا و دو میم و دال نامت کردند</p></div>
<div class="m2"><p>عرش و فلک و کعبه مقامت کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اکنون که به رهبری تمامت کردند</p></div>
<div class="m2"><p>سرتاسر آفاق به نامت کردند</p></div></div>