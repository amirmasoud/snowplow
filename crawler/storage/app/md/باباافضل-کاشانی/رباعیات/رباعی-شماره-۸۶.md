---
title: >-
    رباعی شمارهٔ ۸۶
---
# رباعی شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>رو دیده بدوز تا دلت دیده شود</p></div>
<div class="m2"><p>زان دیده جهان دگرت دیده شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو ز سر پسند خود برخیزی</p></div>
<div class="m2"><p>احوال تو سر به سر پسندیده شود</p></div></div>