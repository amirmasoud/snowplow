---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>گر با توام از تو جان دهم آدم را </p></div>
<div class="m2"><p>از نور تو روشنی دهم عالم را </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون بی تو شوم قوت آنم نبود</p></div>
<div class="m2"><p>کز سینه به کام دل برآرم دم را </p></div></div>