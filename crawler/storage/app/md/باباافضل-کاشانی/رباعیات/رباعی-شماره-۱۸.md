---
title: >-
    رباعی شمارهٔ ۱۸
---
# رباعی شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>در کار جهان، بیع و شری بر هیچ است</p></div>
<div class="m2"><p>نقشی است خوش آدمی، ولی بر هیچ است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار بر این چهار دیوار وجود</p></div>
<div class="m2"><p>فارغ ننشینی، که بنی بر هیچ است</p></div></div>