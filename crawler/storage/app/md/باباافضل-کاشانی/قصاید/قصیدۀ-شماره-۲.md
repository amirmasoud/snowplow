---
title: >-
    قصیدۀ شمارهٔ ۲
---
# قصیدۀ شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گشوده گردد بر تو در حقیقت باز</p></div>
<div class="m2"><p>کناره گیر به یکبار از این جهان مجاز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که در جهان مجاز آن کسی بود پر سود</p></div>
<div class="m2"><p>که بی زیان به سرانجام خود رسد ز آغاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو کار آن سری‌ات خود نکو طرازیده‌ست</p></div>
<div class="m2"><p>تو این سری به تمنای خویشتن مطراز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که این، جهان فنای است و آن، جهان بقا</p></div>
<div class="m2"><p>فنا بد است و بقا نیک، پس به نیکی یاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مال و جاه، فراغت سعادتی است بزرگ</p></div>
<div class="m2"><p>به زر و زور شده غره، محنتی است دراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عجب تر آنکه چرا از سعادت است گریز</p></div>
<div class="m2"><p>به محنت از چه بود خلق را همشه نیاز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو کوشش تو به رنجی است برده، بیش مکوش</p></div>
<div class="m2"><p>چو نازش تو به عمری است رفته، بیش مناز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عروس عقل شود در حجاب جاویدان</p></div>
<div class="m2"><p>چو گشت همت پستِ نیاز و بستهٔ آز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپاس و منتِ جاوید حق تعالی را</p></div>
<div class="m2"><p>که داد جان مرا سوی راه خویش جواز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خشم و آز مرا داد امان به صد اکرام</p></div>
<div class="m2"><p>ز حرص و کینه به خود خوانَدَم به صد اعزاز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ضمیر پاک مرا در ره یقین و خرد</p></div>
<div class="m2"><p>هزار مشعله‌دار است در نشیب و فراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به رنگ و تُنبُل و جادو چه حاجتم، چو نهاد</p></div>
<div class="m2"><p>خدای عز وجل در یقین من اعجاز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کجا به سحر و فسون همتم فرود آید</p></div>
<div class="m2"><p>کجا بود که شکار ملخ کند شهباز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر آن کسی که مرا کرد نسبتی به دروغ</p></div>
<div class="m2"><p>گذشتم از وی ار مفسد است اگر غماز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که قول و فعل چنین خلق، من هزاران بار</p></div>
<div class="m2"><p>اگر چه دیدم و بینم کنم فرامش باز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو ای ستودهٔ ایام، پشت ملت و دین</p></div>
<div class="m2"><p>جمال دولت و دین، مفخر زمانه، ایاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز روی معدلت و راستی و لطف و کرم</p></div>
<div class="m2"><p>خلاص بنده بجوی و به کار وی پرداز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که نیست بنده سزای موکل و زنجیر</p></div>
<div class="m2"><p>مباد کز چو تویی ماند او به گرم و گداز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نه بنده هست سزاوار این گزند و بلا</p></div>
<div class="m2"><p>نه این غریب که با من در این غم است انباز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ندارم از تو من این غم، نعم که هست مرا</p></div>
<div class="m2"><p>توقع از کرمت صد هزار نعمت و ناز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گمان مبر که همه خواهش از پی خویش است</p></div>
<div class="m2"><p>که بنده نیست به آسیب در، چنین بدساز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ولی ز اندُه یک خانه طفل، کز غمشان</p></div>
<div class="m2"><p>به گوش جان من آید ز ناله شان آواز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو مرغ خسته، دل همگنان، ز محنت من</p></div>
<div class="m2"><p>به سینه در، ز تپیدن همی کند پرواز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مباد که افکند اندوه سوز و ناله‌اشان</p></div>
<div class="m2"><p>جهان مملکت آرمیده در تک و تاز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز توست نام تو بر نامهٔ کرم عنوان</p></div>
<div class="m2"><p>ز توست عدل تو بر جامهٔ زمانه طراز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کمند توست به روز مصاف پنجهٔ شیر</p></div>
<div class="m2"><p>سنان توست به هنگام حمله یشگ گراز</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو ای گزیده نژاد از سپهر حادثه زای</p></div>
<div class="m2"><p>تو ای ربوده گهر در جهان شعبده باز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر چه کار من و کار مدح توست دراز</p></div>
<div class="m2"><p>چو از شنیده نشاید مجاز هم ایجاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بزی تو صافی و خالص ز هر بدی چون زر</p></div>
<div class="m2"><p>فتاده دشمن جاهت همیشه در دم گاز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سر حسود تو بی مغز، خشک چون گشنیز</p></div>
<div class="m2"><p>تن عدوت به صد پرده در نهان چو پیاز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>رفیع جاه تو را جن و انس کرده سجود</p></div>
<div class="m2"><p>بلند قدر تو را ماه و مهر برده نماز</p></div></div>