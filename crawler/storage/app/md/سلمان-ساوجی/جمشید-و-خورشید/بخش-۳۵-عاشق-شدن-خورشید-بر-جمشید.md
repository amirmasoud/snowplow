---
title: >-
    بخش ۳۵ - عاشق شدن خورشید بر جمشید
---
# بخش ۳۵ - عاشق شدن خورشید بر جمشید

<div class="b" id="bn1"><div class="m1"><p>گلی دید از هوا پیراهنش چاک </p></div>
<div class="m2"><p>مهی از آسمان افتاده در خاک </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز پا افتاده قدی همبر سرو </p></div>
<div class="m2"><p>پریده طوطی هوش از سر سرو </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرق بر عارض گلگون نشسته </p></div>
<div class="m2"><p>هزاران عقد در بر گل گسسته </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو نیلوفر گل صد برگ در آب </p></div>
<div class="m2"><p>شده بادام چشمش در شکر خواب </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته دامن لعلش زمرد </p></div>
<div class="m2"><p>دری ناسفته در وی لعل و بسد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خورشید را پا رفت در گل </p></div>
<div class="m2"><p>بر او چون ذره عاشق شد به صد دل </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به حیلت خفته می‌زد راه بیدار </p></div>
<div class="m2"><p>به صنعت برد مستی رخت هشیار </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک چون سایه بیهوش اوفتاده </p></div>
<div class="m2"><p>فراز سایه خورشید ایستاده </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سهی سرو از دو نرگس ژاله انگیخت </p></div>
<div class="m2"><p>گلابی چند بر برگ سمن ریخت </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبا با چین زلفش بود دمساز </p></div>
<div class="m2"><p>دماغ خفته بویی برد از آن راز </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به فندق مالش ترکان چین داد </p></div>
<div class="m2"><p>دو هندو را ز سیمین بند بگشاد </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو زلف خویشتن بر خویش پیچید </p></div>
<div class="m2"><p>چو اشک خود دمی بر خاک غلتید </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرش چون گرم شد از تاب خورشید </p></div>
<div class="m2"><p>ز خواب خوش بر آمد شاه جمشید </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز خواب خوش چو مژگان را بمالید </p></div>
<div class="m2"><p>به بیداری جمال ماه خود دید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بر آورد از دل شوریده آهی </p></div>
<div class="m2"><p>چو ماهی شد تپان از بهر ماهی </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پری رخ بازگشت از پیش جمشید </p></div>
<div class="m2"><p>خرامان شد به برج خویش خورشید </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بدو مهراب گفت آهسته، ای شاه </p></div>
<div class="m2"><p>چه برخیزد به جز رسوائی از راه؟ </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز آب دیده کاری برنخیزد </p></div>
<div class="m2"><p>ز روی دل غباری بر نخیزد </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نباشد بی سرشک و ناله سودا </p></div>
<div class="m2"><p>ولی هر چیز را وقتی است پیدا </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز بارانی که تابستان ببارد </p></div>
<div class="m2"><p>به غیر از بار دل باری نیارد </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نداری تاب انوار تجلی </p></div>
<div class="m2"><p>مکن بسیار دیدارش تمنی </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تحمل باید و صبر اندرین کار </p></div>
<div class="m2"><p>تحمل کن دمی، خود را نگه دار» </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک برخاست چون باد از گلستان </p></div>
<div class="m2"><p>سوی خرگاه رفت افتان و خیزان </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دو درج لعل با خود داشت جمشید </p></div>
<div class="m2"><p>فرستاد آن دو درج از بهر خورشد </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مه نو برج درج لعل بگشود، </p></div>
<div class="m2"><p>هزاران زهره در یک برج بنمود </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به زیر لعل دری سفت سر بست </p></div>
<div class="m2"><p>گهر بنمود و درج لعل بشکست </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>که هست این گوهر از آتش نه از خاک </p></div>
<div class="m2"><p>هزارش آفرین بر گوهر پاک!» </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سمن رخسار خورشید گل اندام </p></div>
<div class="m2"><p>کنیزی داشت،« گلبرگ طری نام </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اشارت کرد گلبرگ طری را </p></div>
<div class="m2"><p>که رو بیرون بگو آن جوهری را:</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه لعل است این بدین زیب و بها، چیست؟ </p></div>
<div class="m2"><p>بگو تا این گهر ها را بها چیست؟» </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ملک در بهر حیرت بود مدهوش </p></div>
<div class="m2"><p>برون کرده حدیث گوهر از گوش </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نمی‌دانست گفتار سمن رخ </p></div>
<div class="m2"><p>زبان بگشاد مهرابش به پاسخ </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که شاها، این گهرهای نثاری است </p></div>
<div class="m2"><p>نه زیبای قبول شهریاری است </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز هر جنسی گهر با خویش دارم </p></div>
<div class="m2"><p>اگر فرمان دهی فردا بیارم» </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زمین بوسید خسرو گفت:« شاها، </p></div>
<div class="m2"><p>به برج نیکویی تابنده ماها، </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نثار و هدیه را رسم اعادت </p></div>
<div class="m2"><p>به شهر ما نباشد رسم و عادت </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>نه من گردون دونم کان گهر کان </p></div>
<div class="m2"><p>برون آرد، برد بازش بدان کان </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>من خاکی به خاک خوار مانم </p></div>
<div class="m2"><p>ز هر جنسی که دارم بر فشانم» </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>سمن رخ پیش گلرخ برد پاسخ </p></div>
<div class="m2"><p>چو گل بشکفت و گفتا با سمن رخ: </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>« چنین بازارگان هرگز ندیدم </p></div>
<div class="m2"><p>بدین همت جوان هرگز ندیدم </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>غریب است این که ناکامی غریبی </p></div>
<div class="m2"><p>ز ما نایافته هرگز نصیبی </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گهرهای چنین بر ما بپاشد </p></div>
<div class="m2"><p>چنین شخص از گهر خالی نباشد </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>همانا گوهرش پلک است در اصل</p></div>
<div class="m2"><p>هزاران آفرینش باد بر اصل»‌</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>« کتایون» نام، آن مه دایه‌ای اشت </p></div>
<div class="m2"><p>که از هر دانشی پیرایه‌ای داشت </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>فرستادش به رسم عذر خواهان </p></div>
<div class="m2"><p>بپوشیدش به خلعت‌های شاهان </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از آن پس نافه‌های چین طلب کرد </p></div>
<div class="m2"><p>حریر و دیبه رنگین طلب کرد </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سر بار متاع چین گشادند </p></div>
<div class="m2"><p>ز دیبا جامه‌ها بر هم نهادند </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شد از عرض حریر و مشک عارض </p></div>
<div class="m2"><p>زمین با عارض خوبان معارض </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به هر سو طبله عنبر نهادند </p></div>
<div class="m2"><p>نسیم گلستان بر باد دادند </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ملک یاقوت اشک از دیده می‌راند </p></div>
<div class="m2"><p>نهان در زیر لب این شعر می‌خواند: </p></div></div>