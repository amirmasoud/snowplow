---
title: >-
    بخش ۹۵ - غزل
---
# بخش ۹۵ - غزل

<div class="b" id="bn1"><div class="m1"><p>شادی آمد از درون امشب که هان جان می‌رسد</p></div>
<div class="m2"><p>جان به استقبال شد بیرون که جانان می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار چون گیسو کشان در پای یار آمد ز در</p></div>
<div class="m2"><p>مژده ای دل کان شب سودا به پایان می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بخند ای دل که اینک صبح خندان می‌دمد</p></div>
<div class="m2"><p>خوش برقص ای ذره کاینک مهر رخشان می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پریشان و سرو جان داده بر باد</p></div>
<div class="m2"><p>چو زلف آمد ملک بر پایش افتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل خندان به زیر پر گرفتش</p></div>
<div class="m2"><p>گشاد آغوش و خوش در بر گرفتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشستند آن دو نازک یار باهم</p></div>
<div class="m2"><p>بر آن گلزار روح افزا چو شبنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بپرسیدند هر دو یکدگر را</p></div>
<div class="m2"><p>ببوسیدند بادام و شکر را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشا آن هر دو معشوق موافق</p></div>
<div class="m2"><p>که بنشینند با هم چون دو عاشق</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به مژگان گفته با هم هر دو صد راز</p></div>
<div class="m2"><p>به ابرو کرده باهم هر دو صد ناز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک را گفت: «ای روی تو روزم</p></div>
<div class="m2"><p>به شام آورده روز دلفروزم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مده بر عکس خورشید ای گل اندام</p></div>
<div class="m2"><p>سپاه حسن چون مه عرض بر شام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ فرخ چرا می تابی از روم</p></div>
<div class="m2"><p>به عزم بام صبحم را مکن شوم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ندانم تا کی ای عمر گرامی</p></div>
<div class="m2"><p>چنین تو در سفر فرسوده مانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو مه روز و شب ای زرین شمایل</p></div>
<div class="m2"><p>چه تن می کاهی از قطع منازل؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مه و خور گرچه در بر داری از من</p></div>
<div class="m2"><p>ندیدی هیچ برخورداری از من</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو چون زلف ار نبودی فتنه بر روم</p></div>
<div class="m2"><p>چرا گشتی چنین سرگشته در روم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز حلوایم به جز دودی ندیدی</p></div>
<div class="m2"><p>زیانها کردی و سودی ندیدی»</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بگفت این و سرشک از دیده افشاند</p></div>
<div class="m2"><p>روان این مطلع موزون فرو خواند:</p></div></div>