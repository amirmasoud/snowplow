---
title: >-
    بخش ۸ - دعای دولت امیر شیخ اویس
---
# بخش ۸ - دعای دولت امیر شیخ اویس

<div class="b" id="bn1"><div class="m1"><p>درودی بی‌شمار از رب ارباب</p></div>
<div class="m2"><p>بر احمد باد و بر آل و بر اصحاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پناه خسروان و شهریاران</p></div>
<div class="m2"><p>سر و تاج سران و تاجداران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اساس خطه دین باد دایم</p></div>
<div class="m2"><p>به عون و عدل شاهنشاه قایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سکندر رایت جمشید شوکت</p></div>
<div class="m2"><p>فریدون زینت و پرویز طلعت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیط عالم شاهی گرفته</p></div>
<div class="m2"><p>ز اوج ماه تا ماهی گرفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جبینش مظهر آیات شاهی</p></div>
<div class="m2"><p>ضمیرش مهبط نور الهی</p></div></div>