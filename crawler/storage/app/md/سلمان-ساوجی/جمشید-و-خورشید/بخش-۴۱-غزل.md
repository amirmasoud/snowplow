---
title: >-
    بخش ۴۱ - غزل
---
# بخش ۴۱ - غزل

<div class="b" id="bn1"><div class="m1"><p>زهی دو نرگس چشمت در ارغنون خفته</p></div>
<div class="m2"><p>دو ترک مست تو با تیر و با کمان خفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کلاله ات ز کنار تو ساخته بالین</p></div>
<div class="m2"><p>ز برگ گل زده خرگاه و در میان خفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فتاده بر سمن عارضت دو خال سیاه</p></div>
<div class="m2"><p>دو زنگی اند بر اطراف بوستان خفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه ز آن دو خانه مشکین نمی کنی؟ که تراست</p></div>
<div class="m2"><p>هزار مورچه بر گرد گلستان خفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشیده بر چمنی سایه بانی از ابرو</p></div>
<div class="m2"><p>دو ترک مست تو در زیر سایه بان خفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تن چون سیم تو گنجی است شایگاه و آنگه</p></div>
<div class="m2"><p>دو مار بر سر آن گنج شایگان خفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیال چشم خوشت را که فتنه ای است به خواب </p></div>
<div class="m2"><p>به حال خود بگذارش هم آنچنان خفته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلا برو شکری ز آن دهان بدزد و بیار</p></div>
<div class="m2"><p>چنان کزان نشد آگاه ناگهان خفته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز چشم و غمزه که هستند پاسبانانش</p></div>
<div class="m2"><p>دلا مترس که هستند این و آن خفته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صنم حیران در آن گلبرگ و شمشاد </p></div>
<div class="m2"><p>به زیر لب در این نظم می داد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چشم مخمور تو تا در خواب مستی خفته است</p></div>
<div class="m2"><p>از خمار چشم مستت عالمی آشفته است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دل چو در محراب ابرو چشم مستش دید گفت</p></div>
<div class="m2"><p>کافر سرمست در محراب بین چون خفته است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سنبلت را بس پریشان حال می بینم، مگر</p></div>
<div class="m2"><p>باد صبح از حال ما با او حدیثی گفته است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دیده باریک بینم در شب تاریک هجر</p></div>
<div class="m2"><p>بسکه بر یاد لبت درهای عمان سفته است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خاک راهت خواستم رفتن به مژگان عقل گفت</p></div>
<div class="m2"><p>نیست حاجت کان صبا صدره به مژگان رفته است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>عاقبت هم سر بجایی برکند این خون دل</p></div>
<div class="m2"><p>کز غم سودای تو دل در درون بنهفه است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو اخگر کرد خورشید این عمل را</p></div>
<div class="m2"><p>مهی دیگر فرو خواند این غزل را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بیا ،ساقی،بیا جامی در انداز</p></div>
<div class="m2"><p>حجاب ما ز پیش ما بر انداز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برو، ماها ، به کوی او فرو شو</p></div>
<div class="m2"><p>بیا ای شمع و در پایش سر انداز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هوا چون ساغر آب روی ما ریخت</p></div>
<div class="m2"><p>ز لعلت آتشی در ساغر انداز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چو خفتی خیز و رخت خواب بردار</p></div>
<div class="m2"><p>ز خلوتخانه ما بر در انداز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو گل گر صحبتم می خواهی از جان</p></div>
<div class="m2"><p>به شب در زیر پهلو بستر انداز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وگر چون زلف میل روم داری</p></div>
<div class="m2"><p>به ترسائی چلیپا بر سر انداز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همان دم چنگ را بنواخت ناهید</p></div>
<div class="m2"><p>ادا کرد این غزل در وصف خورشید:</p></div></div>