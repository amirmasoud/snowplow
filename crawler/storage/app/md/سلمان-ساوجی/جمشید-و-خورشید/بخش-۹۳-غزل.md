---
title: >-
    بخش ۹۳ - غزل
---
# بخش ۹۳ - غزل

<div class="b" id="bn1"><div class="m1"><p>بود ز غم صد گره بر گل و بر بارگل</p></div>
<div class="m2"><p>باد به یکدم گشاد صد گره از کار گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرف چمن را چو کرد چشم شکوفه سپید</p></div>
<div class="m2"><p>باز منور شدش دیده به دیدار گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله زر آتشی است ناسره اش در میان</p></div>
<div class="m2"><p>لاجرم آن قیمتش نیست به بازار گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوس قزح در هوا تا سر پرگار زد</p></div>
<div class="m2"><p>دایره لعل گشت نقطه پرگار گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چمنی کان صنم جلوه دهد حسن را</p></div>
<div class="m2"><p>خار عجب گر کشد بار دگر بار گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کف به لب آورده باز جام پر از لعل می</p></div>
<div class="m2"><p>می به کف آور ببین روی پریوار گل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملک با لشکری افزون ز باران</p></div>
<div class="m2"><p>فرود آمد در آن خرم بهاران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>میان سبزه چون گل جای کردند</p></div>
<div class="m2"><p>ملک را بارگه بر پای کردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به یاد روی گل ساغر گرفتند</p></div>
<div class="m2"><p>چو نرگس دور جام از سر گرفتند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک یک هفته با قیصر طرب کرد</p></div>
<div class="m2"><p>بر آن گل ارغوانی باده می خورد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک نالید یک شب پیش مهراب</p></div>
<div class="m2"><p>که کار از دست رفته ای دوست دریاب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین از عمر تا کی دور باشم؟</p></div>
<div class="m2"><p>ز جان خویشتن مهجور باشم؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برای من بسی زحمت کشیدی</p></div>
<div class="m2"><p>ز دست من بسی تلخی چشیدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برای من بکن یک کار دیگر</p></div>
<div class="m2"><p>بیار آن ماه را یکبار دیگر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرشک از دیدگان بارید بر زر</p></div>
<div class="m2"><p>فرو خواند این غزل با اشک برتر</p></div></div>