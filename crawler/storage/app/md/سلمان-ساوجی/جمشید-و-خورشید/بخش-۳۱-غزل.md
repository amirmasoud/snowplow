---
title: >-
    بخش ۳۱ -  غزل
---
# بخش ۳۱ -  غزل

<div class="b" id="bn1"><div class="m1"><p>فریاد همی دارم و فریاد رسی نیست</p></div>
<div class="m2"><p>پندار درین گنبد فیروزه کسی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باد خبر بر، بر آن یار همی دم</p></div>
<div class="m2"><p>کز بهر خبر جز تو مرا همنفسی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هستی من جز نفسی باز نمانده‌ست</p></div>
<div class="m2"><p>هر چند که این نیز بر آنم که بسی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را هوس آن دست که در پای تو میریم</p></div>
<div class="m2"><p>دارد مگسی در شکرستان تو پرواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردا که مرا قوت پر مگسی نیست</p></div>
<div class="m2"><p>خواهیم گذشت از سر آن قلزم نیلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آخر قدم همت ما کم ز خسی نیست</p></div>
<div class="m2"><p>ای طوطی جان زرین قفس سبز برون ای</p></div></div>