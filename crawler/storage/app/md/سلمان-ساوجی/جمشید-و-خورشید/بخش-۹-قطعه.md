---
title: >-
    بخش ۹ - قطعه
---
# بخش ۹ - قطعه

<div class="b" id="bn1"><div class="m1"><p>داور دنیا، معز الدین حق، سلطان اویس</p></div>
<div class="m2"><p>آفتاب عدل پرور سایه پروردگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن شهنشاهی که رای او اگر خواهد، دهد</p></div>
<div class="m2"><p>چون اقالیم زمین اقلیم گردون را قرار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنامیزد چو آفریدون و هوشنگ</p></div>
<div class="m2"><p>ز سر تا پا همه هوشست و فرهنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طراز طرز شاهی می‌طرازد</p></div>
<div class="m2"><p>سر دیهیم و افسر می‌فزاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز مار رمح او پیچان دلیران</p></div>
<div class="m2"><p>ز مور تیغ او دلخسته شیران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هلال فتح نعل ادهم اوست</p></div>
<div class="m2"><p>شب و روز سعادت پرچم اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز یاجوج ستم گشته است آزاد</p></div>
<div class="m2"><p>که تیغش در میان سدیست پولاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ظفر در آب تیغش غوطه خورده</p></div>
<div class="m2"><p>سر بدخواه آب تیغ برده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به جای زر ز آهن دارد افسر</p></div>
<div class="m2"><p>ز پولادش بود خفتان چو گوهر</p></div></div>