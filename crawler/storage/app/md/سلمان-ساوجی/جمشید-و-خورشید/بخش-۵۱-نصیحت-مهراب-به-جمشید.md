---
title: >-
    بخش ۵۱ - نصیحت مهراب به جمشید
---
# بخش ۵۱ - نصیحت مهراب به جمشید

<div class="b" id="bn1"><div class="m1"><p>معنبر زلف را چون داد شب تاب</p></div>
<div class="m2"><p>عروس روز سر برداشت از خواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مه رویی که شب می خورده باشد</p></div>
<div class="m2"><p>همه شب خواب خوش ناکرده باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو گل رویی که بردارد زبالین</p></div>
<div class="m2"><p>رخ لعل و سر و چشم خمارین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سپهر آورده تشت و آفتابه</p></div>
<div class="m2"><p>خضاب شب فرو شست از دو آبه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشسته با قدح خورشید سرمست</p></div>
<div class="m2"><p>مهی در دست و خورشیدش پا بست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آمد گرم خورشیدی ز افلاک</p></div>
<div class="m2"><p>به پیشش جرعه وار افتاد در خاک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبوحی عیش خوش تا چاشت کردند</p></div>
<div class="m2"><p>ز زرین خان گردون چاشت خوردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز مستی تکیه می زد بر شکر ماه</p></div>
<div class="m2"><p>ملک را خواب نوشین برد ناگاه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد از مجلس شکر جمشید را برد</p></div>
<div class="m2"><p>شکر خواب آمد و خورشید را برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانی خفت و باز از جای برخاست</p></div>
<div class="m2"><p>به نای نوش مجلس را بیاراست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هوای عشرت و میل طرب کرد</p></div>
<div class="m2"><p>همان یاران دوشین را طلب کرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جم از بازی دوشین در ملالت</p></div>
<div class="m2"><p>همی دادند یارانش خجالت</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همان مهراب می کردش نصیحت</p></div>
<div class="m2"><p>که: «لایق نیست ،شاها، این فضیحت</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ترا با حلقه زلفش چه کارست؟</p></div>
<div class="m2"><p>سر زلفش حقیقت دم مارست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کسی را کاین نصور در سر آید</p></div>
<div class="m2"><p>مرآن دیوانه را زنجیر باید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تو چون با دخت قیصر دست یازی</p></div>
<div class="m2"><p>کنی مرگت به دست خویش بازی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو خواهی بر فراز نردبان رفت</p></div>
<div class="m2"><p>ز یک یک پایه بر بالا توان رفت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بستان نیز تا وقت رسیدن</p></div>
<div class="m2"><p>نباشد، میوه را نتوان چیدن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به بوی سفره گل باش خرسند</p></div>
<div class="m2"><p>به گردش گرد بی اذن خداوند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چو شهد خود خوری می دان حلالش</p></div>
<div class="m2"><p>ولی تا موم نستانی ممالش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ستم کردی که لعنت بر ستم بادا</p></div>
<div class="m2"><p>کرم کرد او، که رحمت بر کرم بادا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر جم هدهدی آمد ز بلقیس</p></div>
<div class="m2"><p>که خورشیدت مایل سوی بر جیس</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز نو دارد نشاط اتصالی</p></div>
<div class="m2"><p>زهی خوش صحبتی فرخ وصالی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ملک را بود در رفتن حجیبی</p></div>
<div class="m2"><p>نبودش هم به نارفتن شکیبی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو سروی از بر مهراب برخاست</p></div>
<div class="m2"><p>از آن مجلس سوی خورشید شد راست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو نرگس سرگران از شرمساری</p></div>
<div class="m2"><p>در آمد پیش گلبرگ بهاری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>سمن بویش به نرمی باز پرسید</p></div>
<div class="m2"><p>ز روی لطف در رویش بخندید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به ساقی گفت: «جام می بگردان</p></div>
<div class="m2"><p>که بنیادی ندارد دور گردان</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دمی باهم به کام دل برآریم</p></div>
<div class="m2"><p>جهان را تا گذارد، خوش گذاریم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همین کز تیره شب بگذشت پاسی</p></div>
<div class="m2"><p>به یاد جم شکر لب خورد کاسی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برون شد از چمن خورشید مهوش</p></div>
<div class="m2"><p>نجوم انجم را کرد شب خوش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز مستی چون صبا افتان وم خیزان</p></div>
<div class="m2"><p>همی گردید گرد آن گلستان</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گهی با گل به بویش روح پرورد</p></div>
<div class="m2"><p>گهی با لاله عیشی تازه می کرد</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گهی بر روی نسرین بوسه دادی</p></div>
<div class="m2"><p>گهی در پای سروش سر نهادی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>محب گر نقش بر دیوار بیند</p></div>
<div class="m2"><p>در او نقش جمال یار بیند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>نسیم خوش نفس را گفت: «برخیز،</p></div>
<div class="m2"><p>روان گل راز خواب خوش برانگیز</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو هست اسباب عیش امشب مهیا</p></div>
<div class="m2"><p>نمی دانم چه باشد حال فردا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بگو کای صبح رویت عید احباب</p></div>
<div class="m2"><p>بیا کامشب شب قدرست دریاب</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تن گرم و دم سوزنده داریم</p></div>
<div class="m2"><p>بیا تا هر دو یک شب زنده داریم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>روا باشد که من شبهای تاری</p></div>
<div class="m2"><p>کنم چون بلبلان فریاد و زاری؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دو شمعیم از هوا موقوف یک دم</p></div>
<div class="m2"><p>بیا تا هر دو می سوزیم با هم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>کشی چادر شبی چون غنچه بر سر </p></div>
<div class="m2"><p>گذاری بلبلان را رنجه بر در</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>رها کن، چیست چندان خواب بر خواب</p></div>
<div class="m2"><p>چه خواهی دید غیر از خواب در خواب؟</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>اگر خواهی جمال فرخ بخت</p></div>
<div class="m2"><p>به بیداری توان دیدن رخ بخت</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>سبک می بایدت زیت خواب برخاست</p></div>
<div class="m2"><p>که خوابی بس گران اندر پی ماست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نسیم آمد به خیل چین گذر کرد</p></div>
<div class="m2"><p>مه چین را بنزد قیصر آورد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همی آمد ملک تازان و نازان</p></div>
<div class="m2"><p>به ذوق این شعر بر بربط نوازان:</p></div></div>