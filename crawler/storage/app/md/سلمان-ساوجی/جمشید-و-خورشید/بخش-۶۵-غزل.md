---
title: >-
    بخش ۶۵ - غزل
---
# بخش ۶۵ - غزل

<div class="b" id="bn1"><div class="m1"><p>دردا که رفت دلبر و دردم دوا نکرد</p></div>
<div class="m2"><p>صد وعده بیش داد و یکی را وفا نکرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردم هزار قصه حاجت به نزد یار</p></div>
<div class="m2"><p>القصه شد روان و حاجت روا نکرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن تیر غمزه بر تن من موی کرد راست</p></div>
<div class="m2"><p>آن ترک مو شکاف به مویی خطا نکرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر خاک کوی دوست که مالید روی چو من</p></div>
<div class="m2"><p>کان خاک در رخش اثر کیمیا نکرد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به آهی بر دلی صد راه می زد</p></div>
<div class="m2"><p>به راهی هر دلی صد آه می زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شکر بر نی نوایی زد حصاری</p></div>
<div class="m2"><p>به کف شهناز کردش دستیاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حدیث گرمش از نی آتش افروخت</p></div>
<div class="m2"><p>دمی خوش در گرفت و خشک و تر سوخت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درون بر کوه بر بط ساز و نی زن</p></div>
<div class="m2"><p>شده خلق انجمن در کوی و بر زن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن شکل و شمایل خیره ماندند</p></div>
<div class="m2"><p>بر آن صورت حزین جان را فشاندند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکر گوهر بتار چنگ می سفت</p></div>
<div class="m2"><p>چو چنگش کژ نشست و راست می گفت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>شد از آوازشان در پرده ناهید</p></div>
<div class="m2"><p>رسید آوازه ایشان به خورشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غمی بود از فراق آشنایی</p></div>
<div class="m2"><p>طلب می کرد مسکین غم نوایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پرده خادمی بیرون فرستاد</p></div>
<div class="m2"><p>به خلوتگاه خویش آوازشان داد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دو بزم افروز ساز چنگ کردند</p></div>
<div class="m2"><p>بدان فرخ مقام آهنگ کردند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صنم شهناز را چون دید بنواخت</p></div>
<div class="m2"><p>شکر خورشید را چون دید بگداخت</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به خون دیده لوح چهره بنگاشت</p></div>
<div class="m2"><p>ز خود می شد برون خود را نگهداشت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مهی دید از ضعیفی چون هلالی</p></div>
<div class="m2"><p>تراشیده قدی همچون خلالی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نهالی بود قدش خم گرفته</p></div>
<div class="m2"><p>گل اطراف خدش نم گرفته</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نشستند و نوایی ساز کردند</p></div>
<div class="m2"><p>از اول این غزل آغاز کردند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سروا، چه شد که دور شدی از کنار ما؟</p></div>
<div class="m2"><p>بازا که خوش نمی گذرد روزگار ما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خاک وجود ما چو فراقت به باد داد </p></div>
<div class="m2"><p>باد آورد به کوی تو زین پس غبار ما</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وصل تو بود آب همه کارها، دریغ</p></div>
<div class="m2"><p>آن آب رفت و باز نیامد به کار ما</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بودیم تازه و خوش و خندان چو برگ گل</p></div>
<div class="m2"><p>نمام بود عشرت و نهاد خوار ما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پژمرده غنچه دل پر خون ز مهرگان</p></div>
<div class="m2"><p>بنمای رخ به تازگی که تویی نو بهار ما</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو چشمه حیاتی ، حاشا که بر دلت</p></div>
<div class="m2"><p>خاشاک ریزه ای بود از رهگذر ما</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از یار از دیار جدا مانده ایم و هیچ </p></div>
<div class="m2"><p>نه نزد یار ماست خبر نزد دیار ما</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو خورشید آن دو گل رخسار را دید</p></div>
<div class="m2"><p>بر آمد سرخ و خوش چون گل بخندید</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز شادی ارغوان بر زعفران داشت</p></div>
<div class="m2"><p>ولی چون غنچه راز دل نهان داشت</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لب شکر نوازش کرد نی را</p></div>
<div class="m2"><p>شکر لب نیز خوش بنواخت وی را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر آن صوت شکر شهناز زد چنگ</p></div>
<div class="m2"><p>عقاب عشق در شهناز زد چنگ</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز سوز عشق چنگ آمد به ناله</p></div>
<div class="m2"><p>شکر خواند این غزل را بر غزاله:</p></div></div>