---
title: >-
    بخش ۴۸ - باز گفتن خورشید از احوال جمشید به کتایون
---
# بخش ۴۸ - باز گفتن خورشید از احوال جمشید به کتایون

<div class="b" id="bn1"><div class="m1"><p>گل زرد افق را دور بی باک</p></div>
<div class="m2"><p>چو زین گلزار سبز افکنده بر خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برآمد تیره ابری ژاله بارید</p></div>
<div class="m2"><p>به کوهستان مغرب لاله بارید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پری رخ زهره بود و لا ابالی</p></div>
<div class="m2"><p>ملک را مست دید و خانه خالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کتایون را به نزد خویش بنشاند</p></div>
<div class="m2"><p>حدیث جم به گوش او فرو خواند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شب تاریک روشن کرد خورشید</p></div>
<div class="m2"><p>یکایک بر کتایون حال جمشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کتایون گفت: «ای من خاک پایت،</p></div>
<div class="m2"><p>شنیدم هرچه گفتی، چیست رایت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین شک نیست کاین بازارگان مرد</p></div>
<div class="m2"><p>جوانی خوبروی است و جوانمرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به شهر خویش گفتی شهریار ست</p></div>
<div class="m2"><p>به گوهر نیز گفتی تاجدار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من اول روز دانستم که این مرد</p></div>
<div class="m2"><p>نهان در سینه دارد گنجی از درد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بدانستم که او بیمار عشق است</p></div>
<div class="m2"><p>زر افشانی و زاری کار عشق است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کسی اندر جهان نشنید باری</p></div>
<div class="m2"><p>که شخصی بیغرض کرده ست کاری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از آن خورشید زر بر خاک ریزد</p></div>
<div class="m2"><p>که از خاک بدخشان لعل خیزد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از آن دهقان درخت خار کارد،</p></div>
<div class="m2"><p>که گلبرگ طری خارش برآرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن ابر آبرو ریزد به دریا</p></div>
<div class="m2"><p>که آب او شود لولوی لالا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به امیدی دهد زاهد می از دست</p></div>
<div class="m2"><p>که در فردوس ازین بهتر میی هست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ندانم چون برآید نقش این کار</p></div>
<div class="m2"><p>تو قیصرزاده ای ،او بار سالار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر او گوهر از تو بیش دارد</p></div>
<div class="m2"><p>ولیکن گوهری درویش دارد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اگر خواهی که گردد با تو او جفت</p></div>
<div class="m2"><p>ترا باید ضرورت با پدر گفت</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کجا قیصر فرود آرد بدان سر </p></div>
<div class="m2"><p>که بازاری بود داماد قیصر؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ورت در سر هوای عشقبازیست</p></div>
<div class="m2"><p>تو پنداری که کار عشق بازی است؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بباید ترک ننگ و نام کردن </p></div>
<div class="m2"><p>صباح عمر بر خود شام کردن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سری و سروری از سر نهادن </p></div>
<div class="m2"><p>چو زلف خویش سر بر باد دادن</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تو دخت قیصری، ای جان مادر،</p></div>
<div class="m2"><p>مکن در دختری خود را بداختر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو گل بودی همیشه پاک دامن </p></div>
<div class="m2"><p>هوایت کرد خواهد چاک دامن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو درج گوهری سر ناگشوده</p></div>
<div class="m2"><p>در و دری ثمین کس نابسوده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که دارند از پی تاج کیانش</p></div>
<div class="m2"><p>میفکن در کف بازاریانش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چو بشنید این سخن شمع جهانتاب</p></div>
<div class="m2"><p>برآشفت و بدو گفت از سر تاب :</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مرا برخاست دود از سر چو مجمر</p></div>
<div class="m2"><p>تو دامن بر سر دودم مگستر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تو از سوز منی ای دایه غافل</p></div>
<div class="m2"><p>ترا دامن همی سوزد مرا دل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هوای دل مرا بیمار کرده ست</p></div>
<div class="m2"><p>هوای دل چنین بسیار کرده ست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برو دیگر مگو بازاری است او</p></div>
<div class="m2"><p>که از سودای من با زاری است او</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو بازرگان ملک جمشید باشد</p></div>
<div class="m2"><p>سزد گر مشتری خورشید باشد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>که خاقان زاده است او من زقیصر</p></div>
<div class="m2"><p>گر از من نیست مهتر نیست کهتر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرا گر دوست داری یار من باش</p></div>
<div class="m2"><p>مکن کاری دگر در کار من باش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>اشارت کرد گلبرگ طری را</p></div>
<div class="m2"><p>که در حلقه در آرد مشتری را</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>درآمد جم چو سرو رفته از دست</p></div>
<div class="m2"><p>زمین بوسید و دور از شاه بنشست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>به یکباره شد آن مه محو جمشید</p></div>
<div class="m2"><p>چه مه در وقت پیوستن به خورشید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>میان باغ حوضی بود مرمر</p></div>
<div class="m2"><p>که می برد آبروی حوض کوثر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در آب روشنش تابنده مهتاب</p></div>
<div class="m2"><p>ز ماهی تا به مه پیدا در آن آب</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بدستان مطربان استناده بر پای</p></div>
<div class="m2"><p>یکی ناهید و دیگر بلبل آوای</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نشاط انگیز شهناز دلاویز</p></div>
<div class="m2"><p>شکر با ارغنون ساز و شکر ریز</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ترا سرسبز باد ای سرو آزاد</p></div>
<div class="m2"><p>چو گل دایم رخت سرخ و دلت شاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تو گوئی سخت چون پولاد چینم </p></div>
<div class="m2"><p>که غم بگداخت جان آهنینم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گهی رفتم در آب و گه در آتش</p></div>
<div class="m2"><p>چو آیینه ز شوق روی مهوش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دل از فولاد کردم روی از روی</p></div>
<div class="m2"><p>نشینم با تو اکنون روی در روی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ببستم بر تو خود را چون میان من</p></div>
<div class="m2"><p>زهی لطف ار بدان در می دهی تن</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بدان امید گشتم خاک پایت</p></div>
<div class="m2"><p>که باشد بر سرم همواره جایت</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از آن از دیده گوهر می فشانم</p></div>
<div class="m2"><p>که همچون اشک بر چشمت نشانم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>اگر بر هم زنی چون زلف کارم</p></div>
<div class="m2"><p>سر از پای تو هرگز بر ندارم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به شب چون شمع می سوزم برایت</p></div>
<div class="m2"><p>همی میرم به روز اندر هوایت</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو زلفت تا سر من هست بر دوش</p></div>
<div class="m2"><p>ز سودای تو دارم حلقه در گوش</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو قمری هست تا سر بر تن من</p></div>
<div class="m2"><p>بود طوق تو اندر گردن من</p></div></div>