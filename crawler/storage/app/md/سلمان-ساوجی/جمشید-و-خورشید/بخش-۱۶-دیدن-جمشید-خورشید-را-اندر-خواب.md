---
title: >-
    بخش ۱۶ - دیدن جمشید، خورشید را اندر خواب 
---
# بخش ۱۶ - دیدن جمشید، خورشید را اندر خواب 

<div class="b" id="bn1"><div class="m1"><p>چو روی خود بهشتی دید در خواب </p></div>
<div class="m2"><p>روان هر سوی چو کوثر چشمه آب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنار جوی ریحان بر دمیده </p></div>
<div class="m2"><p>میان باغ طوبی سر کشیده </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فراز شاخ مرغان خوش آواز </p></div>
<div class="m2"><p>همی کردند با هم سر دل باز </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شبنم تاج گل چون تاج پرویز </p></div>
<div class="m2"><p>بر آن آویزه نور دلاویز </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه خاکش عبیر و زعفران بود </p></div>
<div class="m2"><p>همه فرشش حریر و پرنیان بود </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبا می‌کرد بر گل جان فشانی </p></div>
<div class="m2"><p>به گل می‌داد هر دم زندگانی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میان باغ قصری دید عالی </p></div>
<div class="m2"><p>چو برج ماه خورشیدیش والی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک می‌گفت با خود که این چه جایست </p></div>
<div class="m2"><p>که جوزا صورت و حورا نمایست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آن آمد که فردوس برین است </p></div>
<div class="m2"><p>قصور خلد و جای حور عین است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>درین بود او که ناگه بی حجابی </p></div>
<div class="m2"><p>ز بام قصر سر زد آفتابی </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خورشیدش عذار ارغوانی </p></div>
<div class="m2"><p>درخشان از نقاب آسمانی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بتی رعنا و کش، ماه مقنع </p></div>
<div class="m2"><p>چو مه بر جبهه اکلیلش مرصع </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فروغ عارض او عکس خورشید </p></div>
<div class="m2"><p>نگین خاتمش را مهر جمشید </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز سنبل بر سمن مرغول بسته </p></div>
<div class="m2"><p>ز موغولش بنفشه دسته دسته </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>لب لعلش درخشان در نگین داشت </p></div>
<div class="m2"><p>به پیشانی خم ابروی چین داشت </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز زلفش سنبل اندر تاب می‌شد </p></div>
<div class="m2"><p>ز شرم عارضش گل آب می‌شد </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگر در دل خیالش بسته گشتی </p></div>
<div class="m2"><p>ز تاب دل عذارش خسته گشتی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>قضا شهزاده را ناگه خبر کرد </p></div>
<div class="m2"><p>در آن زلف و قد و بالا نظر کرد </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صباح زندگانی شد بر او شام </p></div>
<div class="m2"><p>که آمد آفتابش بر لب بام </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قضای آسمانی چون بر آید </p></div>
<div class="m2"><p>اگر بندی در از بامت در آید </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>کمند عنبر از بالای آن قصر </p></div>
<div class="m2"><p>فرو هشته ز سر تا پای آن قصر </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دل سودایی او بی سر و پا </p></div>
<div class="m2"><p>به مشکین نردبان بر شد به بالا </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل جمشید را ناگه پری برد </p></div>
<div class="m2"><p>به دستانش ز دست انگشتری برد </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>چو بیدل شد ملک، فریاد در بست </p></div>
<div class="m2"><p>بجست از خواب و خواب از چشم او جست </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی زد دست بر سر سنگ بربر </p></div>
<div class="m2"><p>که نه دل داشت اندر بر نه دلبر </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همی نالید و در اشک می‌سفت </p></div>
<div class="m2"><p>به زاری این غزل با خویش می‌گفت </p></div></div>