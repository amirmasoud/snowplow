---
title: >-
    بخش ۵۲ - غزل
---
# بخش ۵۲ - غزل

<div class="b" id="bn1"><div class="m1"><p>شوق می ام نیمه شب بر در خمار برد</p></div>
<div class="m2"><p>بوی گلم صبح دم بر صف گلزار برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله چنگ مغان آمد و گوشم گرفت</p></div>
<div class="m2"><p>بیخودم از صومعه بر در خمار برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با همه مستی مرا پیر مغان بار داد</p></div>
<div class="m2"><p>هرچه ز هستی من یافت به یکبار برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی ام از یک جهت ساقر و پیمانه داد</p></div>
<div class="m2"><p>مطربم از یک طرف خرقه و دستار برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو گلم مدتی عشق در آتش نهاد</p></div>
<div class="m2"><p>عاقبت آب مرا بر سر بازار برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار چو با عقل بود عشق مجالی نداشت</p></div>
<div class="m2"><p>عشق درآمد ز در عقل من از کار برد</p></div></div>