---
title: >-
    بخش ۷۴ -  قطعه
---
# بخش ۷۴ -  قطعه

<div class="b" id="bn1"><div class="m1"><p>شب دوشین بت نوشین لب من</p></div>
<div class="m2"><p>چو می کرد از برم عزم جدایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان تاریکی اش در برگرفتم</p></div>
<div class="m2"><p>چه گفتم؟ گفتمش کای روشنایی،</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو آخر داشتی با آشنایان</p></div>
<div class="m2"><p>سر بیگانگی و بیوفایی ،</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان آشنایان روز اول</p></div>
<div class="m2"><p>چه بودی گر نبودی آشنایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک بوسید پای یار مهوش</p></div>
<div class="m2"><p>سبک از آب زد نقشی بر اتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برفت آن عمر تیز آهنگ از پیش</p></div>
<div class="m2"><p>به صوت نرم خواند این قطعه با خویش:</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به وقت صبح کآن خورشید بد مهر</p></div>
<div class="m2"><p>روانه گشت و می شد در عماری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقاب عنبرین از لاله برداشت</p></div>
<div class="m2"><p>ز سنبل برگ سوسن کرد عاری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نرگس کرد سوی من اشارت</p></div>
<div class="m2"><p>که چون تو بیش از این فرصت نداری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تمتع من شمیم عرار نجد</p></div>
<div class="m2"><p>فما بعد العشیه من عراری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چمان شد بر لب آب آن سهی سرو</p></div>
<div class="m2"><p>به جای آب ، یوسف رفت در دلو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دگر بار آن مقنع ماه دلکش</p></div>
<div class="m2"><p>فتاد از چرخ گردان در کشاکش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز چاه مصر شد تا چاه کنعان</p></div>
<div class="m2"><p>چنین باشد مدار چرخ گردان</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو خورشید بلند عالم آرا</p></div>
<div class="m2"><p>توجه کرد از آن پستس به بالا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>صباحی گشت تاری روز جمشید</p></div>
<div class="m2"><p>که رفتش بر سر دیوار خورشید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پریشان از جفای گردش دهر</p></div>
<div class="m2"><p>ز پای قلعه سر بنهاد در شهر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز هر جنسی متاعی کرد پیدا</p></div>
<div class="m2"><p>ز لعل و گوهر و دیبای زیبا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به مهراب جهان گردیده بسپرد</p></div>
<div class="m2"><p>که: «پیش افسر این می بایدت برد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به افسر گو که این دیبا و گوهر</p></div>
<div class="m2"><p>ز چین بهرم فرستادست مادر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اگر چه نیست حضرت را سزاوار</p></div>
<div class="m2"><p>در آن درگه به شوخی کردم این کار»</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر افسر شد آن صورتگر چین</p></div>
<div class="m2"><p>ز هر جنسی حدیثی داشت رنگین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخن در درج گوهر درج می کرد</p></div>
<div class="m2"><p>حکایت را به گوهر خرج می کرد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به هر دیبا حدیثی نغز می گفت</p></div>
<div class="m2"><p>به تحسین در زه در گوش می سفت</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هزارش قطعه بود از لعل و گوهر</p></div>
<div class="m2"><p>نهاد آن یک به یک در پیش افسر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز هر جنسی برای افسر آورد</p></div>
<div class="m2"><p>برش هر روز نقدی دیگر آورد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کنیزان را زر و پیرایه بخشید</p></div>
<div class="m2"><p>به لالایان لؤلؤ مایه بخشید</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>شدی مهراب گه گه نزد بانو</p></div>
<div class="m2"><p>سخنها راندی از هر نوع با او</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دمی گفتی صفات حسن جمشید</p></div>
<div class="m2"><p>رسانیدی سخن را تا به خورشید</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گه از قیصر گه از فغفور گفتی</p></div>
<div class="m2"><p>گه از نزدیک و گه از دور گفتی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنان با مهر مهراب اندر آمیخت</p></div>
<div class="m2"><p>که طوق شوق او در گردن آویخت</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شبی در خوشی ترین وقتی و حالی</p></div>
<div class="m2"><p>به افسر گفت: «من دارم سوالی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ز خورشی آن مه تابان چه دیدی</p></div>
<div class="m2"><p>کزو یکبارگی دوری گزیدی؟</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بود فرزند مقبل دیده را نور</p></div>
<div class="m2"><p>نشاید کرد نور از چشم خود دور</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چنان شمعی تو در کنجی نشانی</p></div>
<div class="m2"><p>کجا یابی فروغ شادمانی ؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چنان شمعی کسی بی نور دارد؟</p></div>
<div class="m2"><p>چنان روحی کس از خود دور دارد؟</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خورشید تو باشد در چه غم</p></div>
<div class="m2"><p>به دیدار که خواهی دید عالم؟</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>پاسخ افسر ، به مهراب بازرگان</p></div>
<div class="m2"><p>چو بشنید آن فسون افسر ز مهراب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز شبنم داد برگ لاله را آب</p></div>
<div class="m2"><p>به پاسخ گفت: «ای جان برادر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مرا هست از فراقش جان پر آذر</p></div>
<div class="m2"><p>ولیکن چون کنم کان سرو مهوش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>چو دوران است ناهموار و سرکش</p></div>
<div class="m2"><p>چو ابر اندر دلش غیر از هوا نیست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ولی یک ذره در رویش حیا نیست</p></div>
<div class="m2"><p>به می پیوسته آب روی ریزد</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چو نرگس مست خفته، مست خیزد</p></div>
<div class="m2"><p>بنامیزد سهی سرویست آزاد</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هوای دل سرش را داده بر باد</p></div>
<div class="m2"><p>نگاری دلکش است از دست رفته</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شکاری سرکش است از شست رفته</p></div>
<div class="m2"><p>چو گل در غنچه باید دختر بکر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در دل بسته بر اندیشه و فکر</p></div>
<div class="m2"><p>کند پنهان رخ از خورشید و از ماه</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>نباشد باد را در پرده اش راه</p></div>
<div class="m2"><p>اگر در گوشش آید بانگ بلبل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>برآشوبد دلش از پرده چون گل</p></div>
<div class="m2"><p>اگر با بکر گردد باد دمساز</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برو چون گل بدرد پرده راز</p></div>
<div class="m2"><p>از آن پس سر به رسوائی کشد کار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نهد راز دلش بر روی بازار</p></div>
<div class="m2"><p>نماند در جوانی رنگ و بویش</p></div></div>