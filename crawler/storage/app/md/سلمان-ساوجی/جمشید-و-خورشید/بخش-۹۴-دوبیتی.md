---
title: >-
    بخش ۹۴ - دوبیتی
---
# بخش ۹۴ - دوبیتی

<div class="b" id="bn1"><div class="m1"><p>آیا کراست زهره؟ آیا کراست یارا؟</p></div>
<div class="m2"><p>کر من برد به یارم این یک سخن که: «یارا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان ما ندارد بی طلعت تو نوری</p></div>
<div class="m2"><p>ای سرو ناز بازآ ، بستان ما بیارا»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان دلخسته هجرانم امشب</p></div>
<div class="m2"><p>که مشتاق وداع جانم امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به شب مهراب رفت از پیش جمشید</p></div>
<div class="m2"><p>شب مهتاب شد جویای خورشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سواری دید بر شبرنگی از دور</p></div>
<div class="m2"><p>چو در تاریکی شب شعله نور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو طاووسی نشسته بر پر زاغ</p></div>
<div class="m2"><p>چو بادی کاورد گلبرگی از باغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی آمد بر آن تازنده دلدل</p></div>
<div class="m2"><p>چو بر باد بهاری خرمن گل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مهرابش در آن شب دید بشناخت</p></div>
<div class="m2"><p>که خورشید است سر در پایش انداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زاری گفت «ای شمع دل افروز</p></div>
<div class="m2"><p>شبت فرخنده باد و روز نوروز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیا ای تازه گلبرگ بهاری</p></div>
<div class="m2"><p>بگو عزم کدامین باغ داری؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز جان نازکتری ای سرو آزاد</p></div>
<div class="m2"><p>به تنها می روی جانت فدا باد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سبک گردان عنان و زود بشتاب</p></div>
<div class="m2"><p>رکابت را گران کن، وقت دریاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مگر جمشید را سازی وداعی</p></div>
<div class="m2"><p>مهی دارد هوای اجتماعی»</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به شب می راند مرکب گرم خورشید</p></div>
<div class="m2"><p>بیامد تا به لشکرگاه جمشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در آن گلزار عمر افزای مهتاب</p></div>
<div class="m2"><p>ملک با یاوران بر گوشه آب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نشسته صوت بلبل گوش می کرد</p></div>
<div class="m2"><p>به یاد یار جامی نوش می کرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کجا بر سنبلی بادی گذشتی</p></div>
<div class="m2"><p>ملک شوریده و آشفته گشتی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گمان بردی که مشکین زلف یارست</p></div>
<div class="m2"><p>که از باد بهاری بیقرار است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو سرو نازنین جنبید از جای</p></div>
<div class="m2"><p>ملک از جای جستی بی سر و پای</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>چنان پنداشتی کامد نگارش</p></div>
<div class="m2"><p>گرفتی خوش در آغوش و کنارش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دوان آمد به پیش شاه مهراب</p></div>
<div class="m2"><p>که شاها ، هان شب قدرست، دریاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به استقبالت آمد بخت پیروز</p></div>
<div class="m2"><p>شب قدر تو خواهد گشت نوروز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو شد خورشید با آن مه مقابل</p></div>
<div class="m2"><p>ملک را برزد این مطلع سر از دل</p></div></div>