---
title: >-
    بخش ۹۰ - خواستگاری شادیشاه از دختر قیصر
---
# بخش ۹۰ - خواستگاری شادیشاه از دختر قیصر

<div class="b" id="bn1"><div class="m1"><p>چو رای هند رخ بر تافت قیصر</p></div>
<div class="m2"><p>نمود از ملک چین رخشنده افسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقاضای عروسی کرد داماد</p></div>
<div class="m2"><p>بر قیصر به خواهش کس فرستاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شه رومی به ابرو چین درآورد</p></div>
<div class="m2"><p>تأمل کرد و آنگه سر برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که: «شادیشاه نور دیده ماست</p></div>
<div class="m2"><p>ولیکن هست از او ما را سه درخواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخستین از پی کابین دختر</p></div>
<div class="m2"><p>دهد یک نیمه ملک شاه و بربر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوم باید که پوید سوی افرنج</p></div>
<div class="m2"><p>برسم باج از آن بوم آورد گنج</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوم شرط آنکه سوی کشور شام</p></div>
<div class="m2"><p>نسازد عزم و اینجا گیرد آرام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مبادا کو شود زین شرط مأیوس</p></div>
<div class="m2"><p>مراد ما از این نام است و ناموس»</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسولان چون شنیدند این حکایت</p></div>
<div class="m2"><p>به شادی باز گفتند این روایت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک را گشت روشن ز آن فسانه</p></div>
<div class="m2"><p>که می گیرد بر او قیصر بهانه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به پاسخ گفت: «این کاریست دشوار</p></div>
<div class="m2"><p>نشاید بی پدر کردن چنین کار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر فرمان بود من باز گردم</p></div>
<div class="m2"><p>ازین در با پدر همراز گردم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به فرمان پدر یکسال دیگر</p></div>
<div class="m2"><p>بیایم بر خط فرمان نهم سر»</p></div></div>