---
title: >-
    بخش ۷ - غزل
---
# بخش ۷ - غزل

<div class="b" id="bn1"><div class="m1"><p>ای ممکن دست قدرت بر بساط لامکان</p></div>
<div class="m2"><p>منتهای سدره اول پایه‌ات از نردبان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده همچون آستین غنچه و جیب چمن</p></div>
<div class="m2"><p>مجمر خلقت معطر دامن آخر زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تکیه‌گاهت قبه عرشست و مرقد زیر خاک</p></div>
<div class="m2"><p>بر مثال آفتابست این و روشنتر از آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب اندر چهارم چرخ می‌تابد ولی</p></div>
<div class="m2"><p>خلق می‌بینند کاندر خاک می‌گردد نهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه بر بالای گردونی و گه در زیر چرخ</p></div>
<div class="m2"><p>آفتاب عالم افروزی و ابرت سایه‌بان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع جمع انبیا چشم و چراغ امتی</p></div>
<div class="m2"><p>ز آن زبانت مظهر آیات نورست و دخان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خاک مسکین از لباس سایه‌ات محروم ماند</p></div>
<div class="m2"><p>خاک باری چیست تا تو سایه اندازی بر آن؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جای نعلین نبی بر طور در صف نعال</p></div>
<div class="m2"><p>بود چون کار نبوت بد بدست دیگران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باز شد تاج سر عرش و چنین باشد چنین</p></div>
<div class="m2"><p>لاجرم وقتی که پای خواجه باشد در میان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کعبه صورت اگر برخیزد از ناف زمین</p></div>
<div class="m2"><p>بعد از این گردد زمین بر پای همچون آسمان</p></div></div>