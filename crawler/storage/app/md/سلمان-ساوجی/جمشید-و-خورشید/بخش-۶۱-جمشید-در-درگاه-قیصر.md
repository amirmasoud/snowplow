---
title: >-
    بخش ۶۱ - جمشید در درگاه قیصر
---
# بخش ۶۱ - جمشید در درگاه قیصر

<div class="b" id="bn1"><div class="m1"><p>ملک با تاج زر کرد عزم درگاه</p></div>
<div class="m2"><p>چو صبح صادق آمد در سحرگاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روان بر کوه خنگ کوه پیکر</p></div>
<div class="m2"><p>بر اطرافش غلامان کمر زر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تتاری ترک بر یک سوی تارک</p></div>
<div class="m2"><p>حمایل در برش چینی بلارک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو گل در بر قبای لعل زرکش</p></div>
<div class="m2"><p>دو مشکین سنبلش بر گل مشوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به زیر قصر افسر داشت جمشید</p></div>
<div class="m2"><p>گذر چون ماه زیر قصر خورشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن بالای قصر افسر بدیدش</p></div>
<div class="m2"><p>ز راه دید مرغ دل پریدش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بالا سرو بالایی فرستاد</p></div>
<div class="m2"><p>که داند باز راز سرو آزاد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بالا سرو بالا راز پرسید</p></div>
<div class="m2"><p>بدان بالا خرامان باز گردید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بدو گفت: «این جوان بازارگانست</p></div>
<div class="m2"><p>شهنشه را ز جمع چاکرانست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ملک جمشید چون آمد به درگاه</p></div>
<div class="m2"><p>به نزد حاجب بار آمد از راه</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>امیر بار را گفت: «ای خداوند</p></div>
<div class="m2"><p>مرا از چین هوای شاه بر کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به عزم آن ز چین برخاست چاکر</p></div>
<div class="m2"><p>که چون میرم بود خاکم درین در</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بدان نیت سفر کردم من از چین</p></div>
<div class="m2"><p>که سازم آستان شاه بالین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>کنون خواهم که پیش شاه باشم</p></div>
<div class="m2"><p>مقیم خاک این درگاه باشم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به دولت باز بر بسته است این کار</p></div>
<div class="m2"><p>قبول افتم گرم دولت شود یار </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همانگونه حاجبش در بارگه برد</p></div>
<div class="m2"><p>گرفته دست جم را پیش شه برد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک جمشید را قیصر بپرسید</p></div>
<div class="m2"><p>بدان در منصب عالیش بخشید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بدو گفت: « ای غریب کشور ما</p></div>
<div class="m2"><p>چرا دوری گزینی از بر ما؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>زمین بوسید و بر شاه آفرین کرد</p></div>
<div class="m2"><p>دعای شاه را باجان قرین کرد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>که: «گر دوری گزیدم دار معذور</p></div>
<div class="m2"><p>که بودم دور ازین درگاه رنجور</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ملک زآن روز چون اقبال دایم</p></div>
<div class="m2"><p>بدی در حضرت قیصر ملازم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به شب چندان ستادی شاه بر پای</p></div>
<div class="m2"><p>که بنشستی چراغ عالم آرای</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وز آن پس آمدی بر درگه شاه</p></div>
<div class="m2"><p>که بودی در شبستان شمع را راه</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دمی خوش بی حضور جم نمی زد</p></div>
<div class="m2"><p>چه جم هم بی حضورش دم نمی زد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو بادش در گلستان بود همدم</p></div>
<div class="m2"><p>چو شمعش در شبستان بود محرم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو یکچندی ندیم خلوتش گشت</p></div>
<div class="m2"><p>پس از سالی وزیر حضرتش گشت</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>جهان زیر نگین شاه جم بود</p></div>
<div class="m2"><p>روان حکمش چو قرطاس و قلم بود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>پدر قیصر بدش مادر بد افسر</p></div>
<div class="m2"><p>ولیکن بود ازو مادر در آذر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خیالش هرزمان در سر همی تاخت</p></div>
<div class="m2"><p>نهان در پرده با جم عشوه می باخت</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شبی نالید خسرو پیش مهراب </p></div>
<div class="m2"><p>که: «کار از دست رفت ای دوست دریاب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز یار خویش تا کی دور باشم؟</p></div>
<div class="m2"><p>چنین دلخسته و رنجور باشم؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>ملک را گفت مهراب ای جهاندار</p></div>
<div class="m2"><p>بسی اندیشه کردم من درین کار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کنون این کار ما گر می گشاید</p></div>
<div class="m2"><p>ز شهناز و ز شکر می گشاید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شکر را عود باید برگرفتن</p></div>
<div class="m2"><p>سحرگاهی پی شهناز رفتن</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر آهنگ حصار برج خورشید</p></div>
<div class="m2"><p>شدن با چنگ و بر بط همچو ناهید</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر آن در پرده ای خوش ساز کردن</p></div>
<div class="m2"><p>نوایی در حصار آغاز کردن</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>صواب آمد ملک را رای مهراب</p></div>
<div class="m2"><p>ره بیرون شدن می دید از آن باب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شکر را گفت: «وقت یاری آمد</p></div>
<div class="m2"><p>ترا هنگام شیرین کاری آمد</p></div></div>