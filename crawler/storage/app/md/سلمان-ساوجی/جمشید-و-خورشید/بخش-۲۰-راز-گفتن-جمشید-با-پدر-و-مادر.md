---
title: >-
    بخش ۲۰ -  راز گفتن جمشید با پدر و مادر
---
# بخش ۲۰ -  راز گفتن جمشید با پدر و مادر

<div class="b" id="bn1"><div class="m1"><p>در آخر غنچه این راز بشکفت</p></div>
<div class="m2"><p>حدیث خواب یک یک با پدر گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پدر گفت: «این پسر شوریده حالست</p></div>
<div class="m2"><p>حدیثش یکسر از خواب و خیالست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همی ترسم که او دیوانه گردد </p></div>
<div class="m2"><p>به یکبار از خرد بیگانه گردد»</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به مادر گفت: «تیمار پسر کن</p></div>
<div class="m2"><p>علاج جان بیمار پسر کن»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همایون هر زمان می‌داد پندش</p></div>
<div class="m2"><p>نبود آن پند مادر سودمندش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلش را هر دم آتش تیزتر بود</p></div>
<div class="m2"><p>خیالش در نظر خونریز تر بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن ایام بد بازرگانی</p></div>
<div class="m2"><p>جهان گردیده ای و بسیار دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بسان پسته خندان روی و شیرین</p></div>
<div class="m2"><p>زبان چرب و سخن پر مغز و رنگین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسی همچون صبا پیموده عالم</p></div>
<div class="m2"><p>چو گل لعل و زر آورده فراهم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی از شاه رفتی سوی سقسین</p></div>
<div class="m2"><p>گهی در روم بودی گاه در چین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به هر شهری ز هر ملکی گذر داشت</p></div>
<div class="m2"><p>ز احوال هر اقلیمی خبر داشت</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنان در نقش بندی بود استاد</p></div>
<div class="m2"><p>که می‌زد نقش چین بر آب چون باد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>پری را نقش بر آینه می‌بست</p></div>
<div class="m2"><p>پری از آینه فکرش نمی‌رست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز رسمش نقش مانی گشته بی‌رنگ</p></div>
<div class="m2"><p>ز دستش پای در گل نقش ارژنگ</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کجا سروی سمن عارض بدیدی</p></div>
<div class="m2"><p>ز سر تا پای شکلش بر کشیدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه اشکال بت رویان عالم</p></div>
<div class="m2"><p>به صورت داشت همچون نقش خانم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ملک جمشید چون از کار درماند</p></div>
<div class="m2"><p>شبی او را به خلوت پیش خود خواند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نشاندش پیش و از وی هر زمانی</p></div>
<div class="m2"><p>همی جست از پری رویان نشانی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کز آن خوبان که دیدی یا شنیدی</p></div>
<div class="m2"><p>کدامین را به خوبی برگزیدی؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کدامین مه به چشمت خوش برآمد</p></div>
<div class="m2"><p>کدام آب حیایتت خوشتر آمد؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به پاسخ دادنش نقاش برخاست</p></div>
<div class="m2"><p>سخن در صورت رنگین بیاراست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>که: «شاها حسن خوبان بی‌کنار است</p></div>
<div class="m2"><p>در و دیوار عالم پر نگار است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ولی در هر یکی رنگی و بویی است</p></div>
<div class="m2"><p>کمال حسن هر شاهد به رویی است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رطب را لذت شکر اگر نیست</p></div>
<div class="m2"><p>در آن ذوقیست کان هم در شکر نیست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ازین خوبان که من دیدم به هر بوم</p></div>
<div class="m2"><p>ندیدم مثل دخت قیصر روم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مه از شرم رخ او در نقاب است</p></div>
<div class="m2"><p>میان ماه رویان آفتاب است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو گویی طینش از آب و گل نیست</p></div>
<div class="m2"><p>ز سر تا پا به غیر از جان و دل نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>به میدانست با مه در محاذات</p></div>
<div class="m2"><p>به اسب و زخ شهان را می‌کند مات</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به حسن و خوبیش حسن ملک نیست</p></div>
<div class="m2"><p>چنان مه در کبودی فلک نیست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز مویش رومیان ز نار بستند</p></div>
<div class="m2"><p>ز مهر رویش آتش می‌پرستند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نه او کس برون پرده دیده</p></div>
<div class="m2"><p>نه اندر پرده آوازش شنیده</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>که یارد نام شوهر پیش او گفت؟</p></div>
<div class="m2"><p>که زیر طاق گردون نیستش جفت</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ازین خور طلعتی ناهید رامش</p></div>
<div class="m2"><p>از این مه پیکری، خورشید نامش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو گیرد جام می در دست خورشید</p></div>
<div class="m2"><p>ببوسد خاک ره چون جرعه ناهید</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>سفر می‌کردم اندر هر دیاری</p></div>
<div class="m2"><p>ز چین افتاد بر رومم گذاری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در آن اقلیم بازاری نهادم</p></div>
<div class="m2"><p>سر بار بدخش و چین گشادم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز هر سو مشتری بر من بجوشید</p></div>
<div class="m2"><p>چنان کاوازه‌ام خورشید بشنید</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>فرستاد و ز من دیبای چین خواست</p></div>
<div class="m2"><p>چو لعل خود بدخشانی نگین خواست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>متاعی چند با خود برگرفتم</p></div>
<div class="m2"><p>به سوی منزل آن ماه رفتم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>دری همچون جبین خوش بوستانی</p></div>
<div class="m2"><p>به هر جانب یکی حاجب ستاده</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>مرا بردند در خوش بوستانی</p></div>
<div class="m2"><p>در او قصری به شکل گلستانی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>ز برج آسمان تابنده ماهی</p></div>
<div class="m2"><p>چو انجم گردش از خوبان سیپاهی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چو چشم من بدان مه منظر افتاد</p></div>
<div class="m2"><p>دل مسکین ز دست من در افتاد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همان دم خواست افتادن دل از پای</p></div>
<div class="m2"><p>به حیلت خویشتن را داشتم بر جای</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کلید قفل یاقوتی ز در ساخت</p></div>
<div class="m2"><p>دل تنگم بدان یاقوت بنواخت</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز منظر ناگهان در من نظر کرد</p></div>
<div class="m2"><p>دل و جان مرا زیر و گذر کرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>متاع خویشتن پیشش نهادم</p></div>
<div class="m2"><p>دل و دین هردو در شکرانه دادم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نگینی چند از آن لب قرض کردم</p></div>
<div class="m2"><p>به پیشش این نگین‌ها عرض کردم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ز زلفش نافه‌های چین گشادم</p></div>
<div class="m2"><p>به دامن بردم و پیشش نهادم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پسندید آن گوهرها را سراسر</p></div>
<div class="m2"><p>به نرمی گفت: «ای پاکیزه گوهر</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ندارد این گوهرهای تو مانند</p></div>
<div class="m2"><p>بهایش چیست؟» گفتم: «ای خداوند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>قماش من نه حد خدمت تست</p></div>
<div class="m2"><p>بهای آن قبول حضرت تست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به خون مشک چون رخسار شویم؟</p></div>
<div class="m2"><p>ز تو چون خون بهای لعل جویم؟</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بهای لعل باید کرد ارزان</p></div>
<div class="m2"><p>چو باشد مشتری خورشید تابان»</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>بدانم یک سخن چندان عطا داد</p></div>
<div class="m2"><p>که لعل و مشک صد خونبها داد</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کنون من صورتش با خویش دارم</p></div>
<div class="m2"><p>اگر فرمان دهی پیش تو آرم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>بدان صورت درونش میل فرمود</p></div>
<div class="m2"><p>بشد مهراب و پیش آورد و بگشود</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ملک جمشید نقش یار خود یافت</p></div>
<div class="m2"><p>نگارین صورت دلدار خود یافت</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نظر چون بر جمال صورت انداخت</p></div>
<div class="m2"><p>همان دم صورت نادیده بشناخت</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>روان در پای آن صورتگر افتاد</p></div>
<div class="m2"><p>بسی بر دست و پایش بوسه‌ها داد</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>کزین سان صورت زیبا که آراست؟</p></div>
<div class="m2"><p>چنان کاری خود از دست که برخاست؟</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>تو خضر چشمه حیوان مایی</p></div>
<div class="m2"><p>چراغ کلبه احزان مایی</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>فراوان گوهر و پیرایه دادش</p></div>
<div class="m2"><p>ز هر چیزی بسی سرمایه دادش</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>چو افسر گوهرش بر فرق کردند</p></div>
<div class="m2"><p>سرا پایش به گوهر غرق کردند</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>نهاد آن صورت دلبند در پیش</p></div>
<div class="m2"><p>به زاری این غزل می‌خواند با خویش:</p></div></div>