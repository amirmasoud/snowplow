---
title: >-
    بخش ۴۳ - از خمار باز آمدن جمشید
---
# بخش ۴۳ - از خمار باز آمدن جمشید

<div class="b" id="bn1"><div class="m1"><p>ملک در خواب صوت چنگ بشنید</p></div>
<div class="m2"><p>چو باد صبحدم بر خود بخندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خمار آلود سر، برخاست از خواب</p></div>
<div class="m2"><p>شراب و آب و مطرب دید مهتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مه بیدار شد خورشید برجست</p></div>
<div class="m2"><p>خرامان شد به برج خویش بنشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا می داد بویی از بهارش</p></div>
<div class="m2"><p>سمن را بود رنگی از نگارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهی خورشید رویش دید بی جان</p></div>
<div class="m2"><p>روان این مطلعش سر بر زد از جان:</p></div></div>