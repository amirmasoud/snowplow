---
title: >-
    بخش ۷۵ -  پاسخ مهراب به افسر
---
# بخش ۷۵ -  پاسخ مهراب به افسر

<div class="b" id="bn1"><div class="m1"><p>بدو مهراب گفت ای افسر روم</p></div>
<div class="m2"><p>به تو آباد باد این کشور روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنون در زیر این پیروزه چادر</p></div>
<div class="m2"><p>کسی را نیست چون خورشید دختر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی دانم به تنهایی نسازد</p></div>
<div class="m2"><p>که تنهایی خدا را می برازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جنس خویش گیرد هرکسی جفت</p></div>
<div class="m2"><p>خدایست آنکه بی یار است و بی جفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین نه پرده پیروزه پیکر</p></div>
<div class="m2"><p>زن از خورشید عذرا نیست برتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر ماهی شبانروزی به خلوت</p></div>
<div class="m2"><p>کند در خانه ای با ماه صحبت</p></div></div>