---
title: >-
    بخش ۵۰ -  رباعی
---
# بخش ۵۰ -  رباعی

<div class="b" id="bn1"><div class="m1"><p>ماییم کله چو لاله بر خاک زده</p></div>
<div class="m2"><p>صد نعره چو ابر از دل غمناک زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مهر چو صبح پیرهن چاک زده</p></div>
<div class="m2"><p>آنگه علم مهر بر افلاک زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شکر گفتار گفتا: «ای سمن بوی،</p></div>
<div class="m2"><p>چرا در بسته ای بر من به یک موی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم چون شانه بود از غم به صد شاخ</p></div>
<div class="m2"><p>از آن دستت زدم بر موی گستاخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دل گفتم سیاهی حلقه در گوش</p></div>
<div class="m2"><p>چرا با او نشیند دوش با دوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل من داشت در زلف تو منزل </p></div>
<div class="m2"><p>ز دستت می زدم دست بر دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از آن من دست هندوئی گرفتم</p></div>
<div class="m2"><p>که او را بر پریروئی گرفتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنور گرم چون بیند فقیری</p></div>
<div class="m2"><p>دلش خواهد که بر بندد فطیری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کژی کردم بسی آشوب دیدم</p></div>
<div class="m2"><p>به جرم آن پریشانی کشیدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطا کردم به جرمم دست بر بند</p></div>
<div class="m2"><p>وگر خواهی جدا کن دستم از بند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو هندو چیره گشت از دست رفتم</p></div>
<div class="m2"><p>زدم دست و بدین جرمش گرفتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نگردد پایه رکن حرم پست</p></div>
<div class="m2"><p>اگر در حلقه اش مستی زند دست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صنم چون دیده جم را جامه ها چاک</p></div>
<div class="m2"><p>چو گل کرد از هوا صد جا قبا چاک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سحرگه جامه جم را صبا برد</p></div>
<div class="m2"><p>قبای گل نسیم جانفزا برد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>برون کردش حریری جامه از جم</p></div>
<div class="m2"><p>به دیبایش بپوشانید شبنم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سماع ارغنون از سر گرفتند</p></div>
<div class="m2"><p>شراب ارغوانی برگرفتند</p></div></div>