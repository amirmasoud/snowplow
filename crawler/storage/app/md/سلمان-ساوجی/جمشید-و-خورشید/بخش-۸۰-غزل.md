---
title: >-
    بخش ۸۰ - غزل
---
# بخش ۸۰ - غزل

<div class="b" id="bn1"><div class="m1"><p>بی گل رویت ندارد رونقی بستان ما</p></div>
<div class="m2"><p>بی حضورت هیچ نوری نیست در ایوان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به سامان سر کویش رسی، ای باد صبح،</p></div>
<div class="m2"><p>عرضه داری شرح حال بی سر و سامان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شرح سودایش که دل با جان مرکب کرده است</p></div>
<div class="m2"><p>بر نمی آید به نوک کلک سرگردان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل ما خار غم بشکست غم در دل بماند</p></div>
<div class="m2"><p>چیست یاران چاره غمهای بی پایان ما؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوستان گویند دل را صبر فرمایید، صبر</p></div>
<div class="m2"><p>چون کنم ای دوست چون دل نیست در فرمان ما؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در فراقش چیست یارب زندگانی را سبب</p></div>
<div class="m2"><p>سخت رویی فلک، یا سستی پیمان ما؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو افسر زخمه جمشید بشنید</p></div>
<div class="m2"><p>دمیده سبزه گرد سوسنش دید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به پای مور فرش گل سپرده</p></div>
<div class="m2"><p>به موران مهر جمشیدی سترده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو خط این تازه شعر روح پرورد</p></div>
<div class="m2"><p>ز طبع نازک او سر بر آورد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خطت هر روز رسمی نو در آرد</p></div>
<div class="m2"><p>به خون من براتی دیگر آرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کشد لشکر ز چین زلف بر روم</p></div>
<div class="m2"><p>سپاه شب به گرد مه در آرد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز هندستان زلفت طوطی آمد</p></div>
<div class="m2"><p>که در منقار تنگ شکر آرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به شوخی سر بر آوردست مگذار</p></div>
<div class="m2"><p>خطت را گر بدینسان سر در آرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو سودای خیال خال و زلفت</p></div>
<div class="m2"><p>جهان را بر من خاکی سر آرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تن پر حسرت ما خاک گردد</p></div>
<div class="m2"><p>ز خاکم باد گرد عنبر آرد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نباتی کز سویدایم بروید</p></div>
<div class="m2"><p>ز حسرت حبه السودا بر آرد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بشنید این سخنهای دلاویز</p></div>
<div class="m2"><p>ملک را شد لب شیرین شکر ریز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زبان بگشاد و در بر افسر افشاند</p></div>
<div class="m2"><p>به وصف افسر این مطلع فرو خواند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای آفتاب جرعه رخشنده جام تو</p></div>
<div class="m2"><p>مه ساقی مدامی دور مدام تو</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای در سواد شام دو زلفت هزار چین</p></div>
<div class="m2"><p>تا حد نیمروز کشیدست شام تو</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>خورشید پادشاه سریر سپهر باد</p></div>
<div class="m2"><p>فرمانبر غلام تو، ای من غلام تو</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا بر زرست نام تو هرجا که خسرویست</p></div>
<div class="m2"><p>بر سر نهاده افسری از زر به نام تو</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به سر مستی ملک را گفت افسر</p></div>
<div class="m2"><p>چه می خواهی؟ بخواه از سیم، از زر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو فرزندی مرا از من مکن شرم</p></div>
<div class="m2"><p>تو خورشیدی مرا با من براگرم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>فدایت می کنم چندانکه خواهی</p></div>
<div class="m2"><p>ز تخت و گنج و ملک و پادشاهی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ملک بنهاد سر در پای افسر</p></div>
<div class="m2"><p>بدو گفت ای سر من جای افسر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به اقبال تو ما را هیچ کم نیست</p></div>
<div class="m2"><p>به رویت خاطر شادم دژم نیست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ولی خواهم که بهر جاندرازی</p></div>
<div class="m2"><p>کنی بیچارگان را چاره سازی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اسیران را ز غم گردانی آزاد</p></div>
<div class="m2"><p>دل غمگین غمگینان کنی شاد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به زندانت مرا جانی است محبوس</p></div>
<div class="m2"><p>مگردانم ز جان خویش مأیوس</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دلم را داشتن در بند تا چند ؟</p></div>
<div class="m2"><p>برون آور دل من از چه بند»</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>جهان بانو نهاد انگشت بر چشم</p></div>
<div class="m2"><p>بدو گفت: «ای بجای نور در چشم،</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دل و جان در تن از بهر تو دارم</p></div>
<div class="m2"><p>به جان و دل همه کارت بر آرم»</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>به نازش در کنار آورد افسر</p></div>
<div class="m2"><p>نهادش بوسه ها بر چشم و بر سر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به دل می گفت دانی این چه بوس است</p></div>
<div class="m2"><p>کنار مادر زیبا عروس است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ستون سیم کردش حلقه در گوش</p></div>
<div class="m2"><p>فکند این در ز نظمش در بن گوش</p></div></div>