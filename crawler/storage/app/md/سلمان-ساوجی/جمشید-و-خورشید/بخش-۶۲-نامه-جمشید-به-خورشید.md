---
title: >-
    بخش ۶۲ -  نامه جمشید به خورشید
---
# بخش ۶۲ -  نامه جمشید به خورشید

<div class="b" id="bn1"><div class="m1"><p>شب تاری به روز آورد جمشید</p></div>
<div class="m2"><p>به شب بنوشت مکتوبی به خورشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطول رقعه ای ببریده در شب</p></div>
<div class="m2"><p>چو زاغ شب به دنبالش مرکب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که در هندوستان سنگین وطن داشت</p></div>
<div class="m2"><p>پریدن در هوای ملک چین داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز هندستان به سوی چینش آورد</p></div>
<div class="m2"><p>بر اطراف ختن شکر فشان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درونش داشت سوزان قصه ای راز</p></div>
<div class="m2"><p>به نوک خامه کرد این نامه آغاز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نام دادبخش دادخواهان</p></div>
<div class="m2"><p>گنه بخشنده صاحب گناهان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلاص انگیز مظلومان محبوس</p></div>
<div class="m2"><p>علاج آمیز رنجوران مأیوس</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ازو باد آفرین بر شاع خوبان</p></div>
<div class="m2"><p>چراغ دلبران و ماه خوبان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مه برج صفا صبح صباحت</p></div>
<div class="m2"><p>گل باغ وفا، عین ملاحت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طراز کسوت چین و طرازی</p></div>
<div class="m2"><p>نگین تاج و فرق سرفرازی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چراغ ناظر و خورشید آفاق </p></div>
<div class="m2"><p>فراغ خاطر و امید مشتاق</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عزیزی ناگه افتادی به زاری</p></div>
<div class="m2"><p>ز جاه یوسفی در چاه خواری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سرشک گرم رو را می دواند</p></div>
<div class="m2"><p>به صدق دل دعایت می رساند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>که ای نازک نگار ناز پرورد</p></div>
<div class="m2"><p>چو گل نه گرم گیتی دیده نه سرد</p></div></div>