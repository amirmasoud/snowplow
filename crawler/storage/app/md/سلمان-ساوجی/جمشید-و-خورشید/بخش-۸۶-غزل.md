---
title: >-
    بخش ۸۶ - غزل
---
# بخش ۸۶ - غزل

<div class="b" id="bn1"><div class="m1"><p>باد صبا به گرد سمندش نمی رسد</p></div>
<div class="m2"><p>سرو سهی به قد بلندش نمی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مه شکسته طرف کلاه است ازین سبب</p></div>
<div class="m2"><p>از چشم آفتاب گزندش نمی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد سمند او به فلک نمی رسد ولی</p></div>
<div class="m2"><p>خنگ فلک به گرد سمندش نمی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پایم به بند زلف گرفتار کرده است</p></div>
<div class="m2"><p>دردا که دست بنده به بندش نمی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر گردی که می انگیخت جمشید</p></div>
<div class="m2"><p>بر آوردی غبار از جان خورشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هر گامی که اسبش بر گرفتی</p></div>
<div class="m2"><p>ز اشک آن خاک در گوهر گرفتی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صنم گلگون سرشک از دیده می راند</p></div>
<div class="m2"><p>ملک شبدیز با گلگونه می راند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملک گوی ار همه کس پیش می برد</p></div>
<div class="m2"><p>به هر صنعت که بود از پیش می برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غریو اهل روم و شام برخاست</p></div>
<div class="m2"><p>ملک چوگان فکند و نیزه را خواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آمد خوش به طرد و عکس کردن</p></div>
<div class="m2"><p>به طرد بدسگال و عکس دشمن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سماک رامح از بالای افلاک</p></div>
<div class="m2"><p>ز غیرت نیزه را انداخت بر خاک</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هزاران حلقه همچون زلف جانان</p></div>
<div class="m2"><p>ز چوگان کرد در میدان بر افشان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز پشت باد پا چون باد در تک</p></div>
<div class="m2"><p>به رمح ان حلقه ها بر بود یک یک</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بر او شاهنشه از جان آفرین کرد</p></div>
<div class="m2"><p>ثنای قدرت جان آفرین کرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به پیروزی ز میدان باز گشتند</p></div>
<div class="m2"><p>همان با نای و نی دمساز گشتند</p></div></div>