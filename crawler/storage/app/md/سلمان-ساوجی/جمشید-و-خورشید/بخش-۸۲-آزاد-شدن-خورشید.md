---
title: >-
    بخش ۸۲ - آزاد شدن خورشید
---
# بخش ۸۲ - آزاد شدن خورشید

<div class="b" id="bn1"><div class="m1"><p>چو صبح از کوه بنمود افسر زر</p></div>
<div class="m2"><p>ز کوه آمد برون خورشید خاور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس افسر بر سمند عزم بنشست</p></div>
<div class="m2"><p>به ناز آورد باز رفته از دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز شهرستان به سوی دژ روان شد</p></div>
<div class="m2"><p>ز شهر تن به شهرستان جان شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو در دژ شد به نزد آن شکر لب</p></div>
<div class="m2"><p>مهی را یافت همچون ماه یک شب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دری در صدف تنها نشسته</p></div>
<div class="m2"><p>ز هر یک غمزه عقدی در گسسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو جسم ناتوانش چم بیمار</p></div>
<div class="m2"><p>چو شیشه چشم هایش رفته در غار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو عکس طلعت خورشید را دید</p></div>
<div class="m2"><p>سرشک لاله گون از دیده بارید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرشک افشان گرفت اندر کنارش</p></div>
<div class="m2"><p>که بنشاند به اشک از دل غبارش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو مادر حال دختر را تبه دید</p></div>
<div class="m2"><p>چو چشم خود جهان یکسر سیه دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پوزش گفت: «ای ترک خطایی</p></div>
<div class="m2"><p>خطا کردم، خطا کردم خطایی»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به زاری گفت: « ای سرو گل اندام</p></div>
<div class="m2"><p>فدای چشم مخمور تو بادام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی بر شکر و گل بوسه ها داد</p></div>
<div class="m2"><p>شکر پاسخ برو افسانه بگشاد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به تندی گفت: «ای بد مهر مادر،</p></div>
<div class="m2"><p>مرا بهر چه افکندی در آذر؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو من نه رستم و سامم به هر حال</p></div>
<div class="m2"><p>چرام افکنده ای در کوه چون زال؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگو تا زین جگر گوشه چه دیدی</p></div>
<div class="m2"><p>که او را بیگناه از خود بریدی؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرا رسوای خاص و عام کردی</p></div>
<div class="m2"><p>میان انجمن بد نام کردی»</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بگفت این قصه و بسیار بگریست</p></div>
<div class="m2"><p>وز آن زاریش مادر زار بگریست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>برون آوردش از غمخانه تنگ</p></div>
<div class="m2"><p>چو لعل از سنگ و همچون شکر از تنگ</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>همان دم چتر شاهی باز کردند</p></div>
<div class="m2"><p>عماری را به دیبا ساز کردند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گل آمد در عماری سوی بستان</p></div>
<div class="m2"><p>مه هودج نشین اندر شبسنان</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>پری رخسار خوبان دلاویز</p></div>
<div class="m2"><p>بهار افروز و گلبرگ شکر ریز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نسیم جانفزای و ارغنون ساز</p></div>
<div class="m2"><p>سمن بوی و نگارین روی و شهناز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هزار و سیصد و هفتاد دختر</p></div>
<div class="m2"><p>همه خورشید روی و فرخ اختر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>که پیش آن صنم در کار بودند</p></div>
<div class="m2"><p>بدان درگاه خدمتکار بودند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همی روی طرب را باز کردند</p></div>
<div class="m2"><p>همان آیین پیشین ساز کردند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کبوتر گر بود صد سال در بند</p></div>
<div class="m2"><p>رود روزی سوی برج خداوند</p></div></div>