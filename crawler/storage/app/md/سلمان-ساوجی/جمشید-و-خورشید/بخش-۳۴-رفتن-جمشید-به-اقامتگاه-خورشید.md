---
title: >-
    بخش ۳۴ - رفتن جمشید به اقامتگاه خورشید 
---
# بخش ۳۴ - رفتن جمشید به اقامتگاه خورشید 

<div class="b" id="bn1"><div class="m1"><p>در آن خرگه بت موزون شمایل </p></div>
<div class="m2"><p>چو معنی لطیف و بکر در دل </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرستاری« پری رخسار» نامش، </p></div>
<div class="m2"><p>پری و آدمی از جان غلامش </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خرگه بانگ زد کای بار سالار </p></div>
<div class="m2"><p>چه بار آورده‌ای؟ بگشای و پیش آر </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن پرداز چین گفت ای خداوند </p></div>
<div class="m2"><p>ندارم هیچ کاری من بدین بار </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>که دارد بار مهر بار سالار </p></div>
<div class="m2"><p>طلب کردند میر کاروان را </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر و سالار خیل عاشقان را </p></div>
<div class="m2"><p>ملک چون ذره با جانی پر امید </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جا جست و روان شد سوی خورشید </p></div>
<div class="m2"><p>دو درج لعل کان در کان نباشد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دو عقد در که در عمان نباشد </p></div>
<div class="m2"><p>به رسم هدیه با خود برگرفت آن </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو باد آمد بدان خرم گلستان </p></div>
<div class="m2"><p>چمان در باغ چون سرو سهی شد </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نزد ماه برج خرگهی شد </p></div>
<div class="m2"><p>دلش با خویش می‌گفت این چه حالست </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>همان خوابست گویی با خیالست </p></div>
<div class="m2"><p>به بیداری کنون می‌بینم آن خواب </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مگر بیدار شد وقت گران خواب </p></div>
<div class="m2"><p>مه خورشید چهر اعنی که جمشید </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو چشم انداخت بر خرگاه خورشید </p></div>
<div class="m2"><p>نماندش تاب و چون مه جامه زد چاک </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو نور آفتاب افتاد بر خاک </p></div>
<div class="m2"><p>از آن خمخانه‌اش یک جرعه سر جوش </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بدادند و برون رفت از سرش هوش </p></div>
<div class="m2"><p>گل نمناک را آبی تمام است </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دل غمناک را تابی تمام است </p></div>
<div class="m2"><p>سران انجمن بر پای جستند </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یکایک چون نبات از هم گسستند </p></div>
<div class="m2"><p>بر آن مه چون ثریا جمع گشتند </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه پروانه آن شمع گشتند </p></div>
<div class="m2"><p>برش عنبر بر آتش می‌نشاندند </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گلابش بر گل تر می‌فشاندند </p></div>
<div class="m2"><p>همه نسرین بران و مشک مویان </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شدند از بهر جم گریان و مویان </p></div>
<div class="m2"><p>خبر کردند ماه انجمن را </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گل آن باغ و سرو آن چمن را </p></div>
<div class="m2"><p>برون آمد چو گل سر مست و رعنا </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به یک پیراهن از خرگاه مینا </p></div>
<div class="m2"><p>چو سرو از باد و قد از باده مایل </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مهش در قلب عقرب کرده منزل </p></div>
<div class="m2"><p>ز رنگ عارضش روی هوا لعل </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خم زلفش در آتش کرده صد نعل </p></div>
<div class="m2"><p>خرامان در پی خورشید رویان </p></div></div>