---
title: >-
    بخش ۶۳ - غزل
---
# بخش ۶۳ - غزل

<div class="b" id="bn1"><div class="m1"><p>تو ای جان من ای بیمار چونی؟</p></div>
<div class="m2"><p>درین بیماری و تیمار چونی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلی بودی نبودت هیچ خاری</p></div>
<div class="m2"><p>کنون در چنگ چندین خار چونی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا همواره بستر بود گلبرگ</p></div>
<div class="m2"><p>گلا، از جای ناهموار چونی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا باری خیال تست مونس</p></div>
<div class="m2"><p>ندانم با که می داری تو مجلس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبا با من همه روزست دمساز </p></div>
<div class="m2"><p>ترا آخر بگو تا کیست همراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نشسته در ره بادم به بویت</p></div>
<div class="m2"><p>که باد آرد مگر گردی ز کویت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چون شمعی نشسته در شبستان</p></div>
<div class="m2"><p>در آهن پای و در سر دشمن جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من از شوق جمال یار مهوش</p></div>
<div class="m2"><p>زنم پروانه سان خود را بر آتش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گه از حسرت زنم من سنگ بر دل</p></div>
<div class="m2"><p>که دارد یار من در سنگ منزل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگر آهم تواند کرد کاری</p></div>
<div class="m2"><p>کند در خلوتت یک شب گذاری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مرادی نیست در عالم جز اینم</p></div>
<div class="m2"><p>که روی نازنینت باز بینم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سر زلف دل آشوبت بگیرم</p></div>
<div class="m2"><p>به سودای تو در پای تو میرم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>مرا جانی است مرکب رانده در گل</p></div>
<div class="m2"><p>از آن ترسم که ناگه در پی دل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>رود جان و تنم در گل بماند</p></div>
<div class="m2"><p>مرا شوق رخت در دل بمان </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو در دل نقش زلف یار گردد</p></div>
<div class="m2"><p>دلم ز آزار جان بیمار گردد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به چشمم در غم آن نرگس شنگ</p></div>
<div class="m2"><p>جهان گاهی سیه باشد گهی تنگ </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خبر ده تا دوای کار من چیست؟</p></div>
<div class="m2"><p>طبیب درد بی درمان من کیست؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غم پنهان خود را با که گویم؟</p></div>
<div class="m2"><p>علاج درد دل را از که جویم؟</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو آمد نامه خسرو به پایان</p></div>
<div class="m2"><p>به خون دیده اش بنوشت عنوان</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روان از دیده خون دل چو خامه</p></div>
<div class="m2"><p>بدان هر دو صنم بنوشت نامه</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>که این غم نامه را هیچ ار توانید</p></div>
<div class="m2"><p>بدان ماه پری پیکر رسانید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو عود و چنگ را آهنگ سازید</p></div>
<div class="m2"><p>ز قولم این غزل بر چنگ سازید</p></div></div>