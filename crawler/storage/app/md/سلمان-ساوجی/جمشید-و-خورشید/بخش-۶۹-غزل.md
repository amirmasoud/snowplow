---
title: >-
    بخش ۶۹ - غزل
---
# بخش ۶۹ - غزل

<div class="b" id="bn1"><div class="m1"><p>ای باد صبحگاهی بادا فدات جانم</p></div>
<div class="m2"><p>در گوش آن صنم گو این نکته از زبانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای آرزوی جانم در آرزوی آنم</p></div>
<div class="m2"><p>کز هجر یک حکایت در گوش وصل خوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی که با تو بودم بد بخت همنشینم </p></div>
<div class="m2"><p>امروز کت به سالی روی چو مه نبینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی چگونه باشم در محنت حبیبم</p></div>
<div class="m2"><p>زآن پس که دیده باشی در دولتی چنانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با دل به درد گفتم کان خوشدلی کجا شد؟</p></div>
<div class="m2"><p>آخر مرا نگویی؟ دل گفت من ندانم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهم که از جمالت حظی تمام یابم</p></div>
<div class="m2"><p>وز ساغر وصالت ذوقی رسد به جانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آری گرت بیابم روزی به کام یابم</p></div>
<div class="m2"><p>ورنه چنان که دانی در درد دل بمانم</p></div></div>