---
title: >-
    بخش ۵۵ -  دربند افتادن خورشید به دستور افسر، مادرش
---
# بخش ۵۵ -  دربند افتادن خورشید به دستور افسر، مادرش

<div class="b" id="bn1"><div class="m1"><p>به نوشانوش رفت آن شب به پایان</p></div>
<div class="m2"><p>سحر چون شد لب آفاق خندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دگر عیش و طرب را تازه کردند</p></div>
<div class="m2"><p>ز می بر روی عشرت غازه کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دو مه گه آشکار و گه نهانی</p></div>
<div class="m2"><p>دو مه خوردند با هم دوستگانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز بوسی نجست از دلستان هیچ</p></div>
<div class="m2"><p>کناری بود دیگر در میان هیچ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همی خوردند جام از شام تا بام</p></div>
<div class="m2"><p>که ناگه تشتشان افتاد از بام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسانیدند غمازان کشور</p></div>
<div class="m2"><p>ازین رمزی به نزدیکان آن در</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که خورشید دلارا ناگهانی</p></div>
<div class="m2"><p>به صد دل گشته عاشق بر جوانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه روز و شبش جام است بر کف</p></div>
<div class="m2"><p>هزارش بار زد ناهید بر دف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زن قیصر که بد خورشید را مام</p></div>
<div class="m2"><p>بلند اختر زنی بود افسرش نام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو شد مشهور در شهر این حکایت</p></div>
<div class="m2"><p>به افسر باز گفتند این روایت</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز غیرت سر و قدش گشت چون بید</p></div>
<div class="m2"><p>همان دم رفت سوی کاخ خورشید</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صنم در گلشنی چون گل خزیده</p></div>
<div class="m2"><p>ز غیر دوست دامن در کشیده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به کنج خلوتی دو دوست با دوست</p></div>
<div class="m2"><p>نشسته چون دو مغز اندر یکی پوست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>موافق چون دو گوهر در یکی درج</p></div>
<div class="m2"><p>معانق چون دو کوکب در یکی برج</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>درون پرده گل بلبل به آواز</p></div>
<div class="m2"><p>نوازان نغمه ای بر صورت شهناز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بهار افروز و شکر با شکر ریز</p></div>
<div class="m2"><p>به چنگ آورده الحان دلاویز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به گرد آن دیار روح پرور</p></div>
<div class="m2"><p>نمی گردید جز ساقی و ساغر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بر آمد ابر و بارانی فرو کرد</p></div>
<div class="m2"><p>در آمد سیل و طوفانی در آورد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نسیم آمد عنان از دست داده</p></div>
<div class="m2"><p>چو باد صبحدم دم برفتاده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صنم را گفت : «اینک افسر آمد</p></div>
<div class="m2"><p>چه می نالی که افسر بر سر برآمد؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ترا افسر بدین حال ار ببیند</p></div>
<div class="m2"><p>سرت دور از تو باد افسر نبیند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>صنم را بود بیم جان جمشید</p></div>
<div class="m2"><p>همی لرزید بر جمشید چون بید</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ملک را گفت: «آمد مادر من</p></div>
<div class="m2"><p>نمی دانم چه آید بر سر من!</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ندیدی هیچ ازین بستان تو باری</p></div>
<div class="m2"><p>همان بهتر که باشی بر کناری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چو گنجی باش پنهان در خرابی</p></div>
<div class="m2"><p>چو نیلوفر فرو بر سر در آبی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>میان سرو همچون جان نهان شد</p></div>
<div class="m2"><p>سراپا سرو پنداری روان شد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ز شاخ سرو نجمی یافت شاهی</p></div>
<div class="m2"><p>درخت سرو بار آورد ماهی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ملک جمشید جان انداخت در سرو</p></div>
<div class="m2"><p>همانی آشیانی ساخت بر سر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چو خلوتخانه خالی شد ز جمشید</p></div>
<div class="m2"><p>به ماهی منکسف شد چشم خورشید</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خروش چاوشان از در بر آمد </p></div>
<div class="m2"><p>سر خوبان روم از در در آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به سر بر می شد آتش چون چراغش</p></div>
<div class="m2"><p>همی آمد برون دود از دماغش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گره بر رخ زده چون زلف مشکین</p></div>
<div class="m2"><p>چو ابرو داد عرض لشکر چین</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پری رخسار حالی مادرش دید</p></div>
<div class="m2"><p>به استقبال شد، دستش ببوسید</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نظر بر روی دختر کرد مادر</p></div>
<div class="m2"><p>چو زلف خویش می دیدش بر آذر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مرکب کرد حنظل با طبر زد</p></div>
<div class="m2"><p>به خورشید شکر لب بانگ بر زد</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>که: «ای رعنا چو گل تا چند و تا کی </p></div>
<div class="m2"><p>کشی از جام زرین لاله گون می؟ </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو نرکس تا به کی ساغر پرستی</p></div>
<div class="m2"><p>قدح در دست و سر در خواب مستی؟</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تو تا باشی نخواهد شد چو لاله</p></div>
<div class="m2"><p>سرت خالی ز سودای پیاله</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بسی جان خراب از می شد آباد</p></div>
<div class="m2"><p>بس آبادا که دادش باده بر باد</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>میی با رنگ صافی چون لب یار</p></div>
<div class="m2"><p>حیات افزاید و روح آورد بار</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز مستی گران چون چشم دلبر</p></div>
<div class="m2"><p>چه آید غیر بیماریت بر سر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>به چشم خویش می بینم که هستی</p></div>
<div class="m2"><p>که باشد در سرت سودای مستی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بسی چوب از قفای مطربان زد</p></div>
<div class="m2"><p>نی اندر ناخن شیرین لبان زد</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو ابرو روی حاجب را سیه کرد</p></div>
<div class="m2"><p>چو زلفش سلسله در گردن آورد</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به کوهی در حصاری داشت افسر</p></div>
<div class="m2"><p>که با گردون گردان بود همبر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کشان خورشید را با خویشتن برد</p></div>
<div class="m2"><p>به لالائی دو سه شبرنگ بسپرد</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>شکر لب را در آن بتخانه تنگ</p></div>
<div class="m2"><p>نهان بنشاند چون یاقوت در سنگ</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ندادندی برش جز باد را بار</p></div>
<div class="m2"><p>نبودی آفتاب را سایه را بار</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چمن پرورد گلبرگ بهاری</p></div>
<div class="m2"><p>چو گل در غنچه شد ناگه حصاری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>حصاری بود عالی سور بر سور</p></div>
<div class="m2"><p>پری پیکر عزا می داشت در سور</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در آن سور آن گلی سوری به ماتم</p></div>
<div class="m2"><p>چو صبح از دیده می افشاند شبنم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بدان آتش که هجرانش بر افروخت</p></div>
<div class="m2"><p>جدا شد چون عسل از موم می سوخت</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>نمی آسود روز و شب نمی خفت</p></div>
<div class="m2"><p>شب و روز این سخن را باد می گفت</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>دل من باری از تیمار خون است</p></div>
<div class="m2"><p>ندادم حال آن بیمار چون است</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>از آن جانب ملک چون حال خورشید</p></div>
<div class="m2"><p>بدید از جان خود برداشت امید</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>به دندان می گزید انگشت چون باز</p></div>
<div class="m2"><p>کبوتر وار کرد از سرو پرواز</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>فرود آمد به برج ماه رخسار</p></div>
<div class="m2"><p>همی گردید گرد برج دیار</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>همی گردید و خون از دیده می راند</p></div>
<div class="m2"><p>به زاری بر دیار این قطعه می خواند</p></div></div>