---
title: >-
    بخش ۲۳ -  اجازه سفر خواستن جمشید
---
# بخش ۲۳ -  اجازه سفر خواستن جمشید

<div class="b" id="bn1"><div class="m1"><p>ملک جمشید کرد آن راز مشهور</p></div>
<div class="m2"><p>فرستاد از در و درگاه فغفور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیمی را طلب فرمود و بنشاند</p></div>
<div class="m2"><p>حکایت‌های شب یک یک فرو خواند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عزم روی دستوری طلب کرد</p></div>
<div class="m2"><p>مثال حکم فغفوری طلب کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو شاه این قصه را بشنید در جمع</p></div>
<div class="m2"><p>برای روشنایی سوخت چون شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبالب شد ز خشم و قصه بنهفت</p></div>
<div class="m2"><p>به زیر لب سخن پرداز را گفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«برو از من بپرس آن نازنین را،</p></div>
<div class="m2"><p>پدید آرنده تاج و نگین را،</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگویش این خیال از سر بدر کن</p></div>
<div class="m2"><p>به تارک ترک تاج و تخت زر کن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چرا چون نافه ببریدی ز مسکن</p></div>
<div class="m2"><p>چرا چون لعل برکندی ز معدن؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عزیز من مکن پند مرا خوار</p></div>
<div class="m2"><p>جوانی، خاطر پیران نگه‌دار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به پیران سر مکن از من جدایی</p></div>
<div class="m2"><p>مده بر باد ملک و پادشاهی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نمی‌دانم پدر با تو چه بد کرد</p></div>
<div class="m2"><p>که خواهی کشتنش در حسرت و درد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مر از دست من ای شاهبازم</p></div>
<div class="m2"><p>که چون رغتی نخواهی یافت بازم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به گیتی چون تو فرزندی ندارم</p></div>
<div class="m2"><p>دلارایی و دلبندی ندارم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پدر دوران عمر خویش رانده‌ست</p></div>
<div class="m2"><p>مرا غیر از تو عمری نمانده‌ست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تو نیز اکنون بخواهی رفتن ای عمر</p></div>
<div class="m2"><p>نمی‌دانم چه خواهی گفتن ای عمر»</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رسول آمد حکایت با ملک گفت</p></div>
<div class="m2"><p>ملک چون روزگار خود برآشفت</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به سوی مادر آمد رفته در خشم</p></div>
<div class="m2"><p>روان بر برگ گل بارانش از چشم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو نور چشم خود را دید گریان</p></div>
<div class="m2"><p>همایون گشت چون زلفش پریشان</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>روان برخاست چشمش را ببوسید</p></div>
<div class="m2"><p>پس ار بوسیدنش احوال پرسید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>پسر بنشست و با او راز می‌گفت</p></div>
<div class="m2"><p>حدیث رفته یکی یک باز می‌گفت</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به دارای دو گیتی خورد سوگند</p></div>
<div class="m2"><p>که گر منعم کند گیتی خداوند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>به خنجر سینه خود را کنم چاک</p></div>
<div class="m2"><p>به جای تخت سازم بستر از خاک</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چو مادر قصه دلبند بشنید</p></div>
<div class="m2"><p>ز جان نازنین او بترسید</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بسی پند و بسی امید دادش</p></div>
<div class="m2"><p>بدان امیدها می‌کرد شادش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ملک را گشت معلوم آن روایت</p></div>
<div class="m2"><p>که با او در نمی‌گیرد حکایت</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>فرستاد و شبی مهراب را خواند</p></div>
<div class="m2"><p>بسی با او ز هر نوعی سخن راند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ملک را گفت مهراب: «ای خداوند</p></div>
<div class="m2"><p>اگر خواهی بقای جان فرزند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>بباید ساختن تدبیر راهش</p></div>
<div class="m2"><p>که دارد ایزد از هر بد نگاهش </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روان می‌بایدش کردن هم امروز</p></div>
<div class="m2"><p>مگر گردد به بخت شاه فیروز»</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نهاد آنگه ملک سازه ره آغاز</p></div>
<div class="m2"><p>به یک مه کرد ساز رفتنش ساز</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>غلامان سمن رخسار، سیصد</p></div>
<div class="m2"><p>کنیزان پری دیدار، بی‌جد</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بسی شد هودج و کوس و علم راست</p></div>
<div class="m2"><p>هیونان را به هودج‌ها بیاراست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ناشانده نازکان را در عماری</p></div>
<div class="m2"><p>چو اندر غنچه گل‌های بهاری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز نزدیکان دوراندیش بخرد</p></div>
<div class="m2"><p>روان کرد اندران موکب تنی صد</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بسی جنگ آوران رزم دیده</p></div>
<div class="m2"><p>جفای نیزه و خنجر کشیده</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بسی مردم ز هر جنسی فرستاد</p></div>
<div class="m2"><p>بسی پند و بسی اندرزشان داد</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>روان شد کاروانی فوج بر فوج</p></div>
<div class="m2"><p>تو پنداری که زد دریای چین موج</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درایش ناله بر گردون کشیده</p></div>
<div class="m2"><p>درنگ او به هندوستان رسیده</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جلاجل را روان بر مرحبا بود</p></div>
<div class="m2"><p>همه کوه و در آواز درا بود</p></div></div>