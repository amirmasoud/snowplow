---
title: >-
    بخش ۱۰۱ - رباعی
---
# بخش ۱۰۱ - رباعی

<div class="b" id="bn1"><div class="m1"><p>ای آینه کرده در رخت روی امید</p></div>
<div class="m2"><p>بر چشمم ازین خط سیه روی ، سپید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به ز آن نبود که دیده دوزند آنجا</p></div>
<div class="m2"><p>کآیینه براری کند با خورشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مشاطه زدش در زلف شانه</p></div>
<div class="m2"><p>نسیم این بیت را زد بر ترانه:</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس گره و پیچ که زلف تو نمود،</p></div>
<div class="m2"><p>آمد شدن شانه در آن مشکل بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در حل دقایق ارچه ره می پیمود،</p></div>
<div class="m2"><p>از مشکل زلف شانه مویی نگشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو نیل خط کشیدندش به آواز</p></div>
<div class="m2"><p>بخواند این بیت را بر شاه شهناز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی که فلک حسن تو را نیل کشید</p></div>
<div class="m2"><p>چشم بد روزگار را میل کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو بر ابرو کمانش وسمه بنهاد</p></div>
<div class="m2"><p>مغنی بر کمانچه ساز می داد:</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روی تو که آتشی در آفاق نهاد،</p></div>
<div class="m2"><p>بس داغ که بر سینه عشاق نهاد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشاطه چو چشم و طاق ابروی تو دید</p></div>
<div class="m2"><p>از هوش برفت و وسمه بر طاق نهاد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو آمد غمزه اش با میل در ناز</p></div>
<div class="m2"><p>فرو خواند این رباعی ارغنون ساز:</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو میل ز جیب سرمه دان سر بر کرد</p></div>
<div class="m2"><p>نظاره چشم سیه دلبر کرد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خود را خجل و سرزده در گوشه کشید</p></div>
<div class="m2"><p>از دست بتم خاک سیه بر سر کرد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو شد در چشم شوخش سرمه پیدا</p></div>
<div class="m2"><p>بهار افروز خواند این نظم غرا:</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای خاک در تو سرمه دیده ما</p></div>
<div class="m2"><p>خور از هوس خاک رهت چشم سیاه</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با خاک رهت که سرمه آرد در چشم</p></div>
<div class="m2"><p>جز میل که باد بر سرش خاک سیاه؟</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چو بر برگ سمن خندید غازه</p></div>
<div class="m2"><p>سمن رخ زد بر آب این شعر تازه</p></div></div>