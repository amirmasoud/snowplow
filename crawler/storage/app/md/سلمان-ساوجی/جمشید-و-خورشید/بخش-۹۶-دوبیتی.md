---
title: >-
    بخش ۹۶ - دوبیتی
---
# بخش ۹۶ - دوبیتی

<div class="b" id="bn1"><div class="m1"><p>از دیده دلم روز وداعش نگران شد</p></div>
<div class="m2"><p>با قافله اشک در افتاد و روان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جان کم از او گیر برو با غم او باش</p></div>
<div class="m2"><p>دل رفت و همه روزه در آن می نتوان شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جوابش داد جم کای مایه ناز</p></div>
<div class="m2"><p>طراز خوبی و پیرایه ناز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تن و جان کرده ام وقف هوایت</p></div>
<div class="m2"><p>سرم بادا فدای خاک پایت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر من گرنه سودای تو ورزد،</p></div>
<div class="m2"><p>سر وارون سودایی چه ارزد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز شمعت شعله ای در هر که گیرد</p></div>
<div class="m2"><p>چراغ روشنش هرگز نمیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جان و تن که بنیادیست بس سست</p></div>
<div class="m2"><p>مراد من تویی و صحبت تست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تنم خاک است و جانم باد پر درد</p></div>
<div class="m2"><p>چه برخیزد ز خاک و باد جز گرد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به اقبالت نمی اندیشم از کس</p></div>
<div class="m2"><p>مرا از هجر تست اندیشه و بس</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرا باغمزه این دل می خراشد</p></div>
<div class="m2"><p>چه باک از زخم تیغ و تیر باشد؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو خواهم طاق ابروی تو دیدن</p></div>
<div class="m2"><p>چرا باید کمان باری کشیدن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز بهر آن زنم بر تیغ جان را</p></div>
<div class="m2"><p>ز عشق آن شوم قربان کمان را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>درین ره از هوا سر می زنم من</p></div>
<div class="m2"><p>اگر سر می نهم خونم به گردن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فک با عاشقان دایم به کین است</p></div>
<div class="m2"><p>چه شاید کرد قسمت اینچنین است؟</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فلک تا تیغ خود خواهد کشیدن</p></div>
<div class="m2"><p>عزیزان را زهم خواهد بریدن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک می گفت و آب از دیده می راند</p></div>
<div class="m2"><p>بر او این قطعه موزون فرو خواند</p></div></div>