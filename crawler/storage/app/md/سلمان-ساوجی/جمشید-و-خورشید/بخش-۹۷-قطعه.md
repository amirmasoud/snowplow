---
title: >-
    بخش ۹۷ - قطعه
---
# بخش ۹۷ - قطعه

<div class="b" id="bn1"><div class="m1"><p>در آن روز وداع آن ماه خوبان</p></div>
<div class="m2"><p>لبم بر لب نهاد و گفت نرمک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز روی حسرت او با من همی گفت:</p></div>
<div class="m2"><p>هذا فراق بینی و بینک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه شب با دوتن افسر در آن دشت</p></div>
<div class="m2"><p>تماشا را بدان مهتاب می گشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طوافی گرد آب و سبزه می کرد</p></div>
<div class="m2"><p>ز ناگه ره بدان منزل در آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک منزل دو مه را دید با هم</p></div>
<div class="m2"><p>نشسته هر دو چون بلقیس با جم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوای چنگ و بانگ رود بشنود</p></div>
<div class="m2"><p>بدان فرخ مقام آهنگ فرمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن مهتاب خرم بود خورشید</p></div>
<div class="m2"><p>نشسته چون گلی در سایه بید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو مادر را بدید از دور بشناخت</p></div>
<div class="m2"><p>صنم خود را به بیدستان درانداخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به دستان چون فلک نقشی عیان کرد</p></div>
<div class="m2"><p>به بیدستان چو گل خود را نهان کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانه قاطع عیش است و شادی</p></div>
<div class="m2"><p>نمی خواهد به غیر از نامرادی</p></div></div>