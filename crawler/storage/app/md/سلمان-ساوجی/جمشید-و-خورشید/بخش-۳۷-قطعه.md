---
title: >-
    بخش ۳۷ - قطعه
---
# بخش ۳۷ - قطعه

<div class="b" id="bn1"><div class="m1"><p>از سرگرمی جوابش داد شمع</p></div>
<div class="m2"><p>گفت: « تا کی سرزنش کردن مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقم خواندی، بلی، من عاشقم</p></div>
<div class="m2"><p>اشک سرخ و روی زردم بس گوا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آنچه گفتی، سر فرازی می کنم</p></div>
<div class="m2"><p>سر فرازی هست بر عاشق روا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرفرازی من از عشقست و بس</p></div>
<div class="m2"><p>در هوایش سر فرایم دایماً</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه می گویی که بنشین و بمیر </p></div>
<div class="m2"><p>یا سر و خود گیر و یک چندی به پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا سرم برجاست نتوانم نشست </p></div>
<div class="m2"><p>من نخواهم مردن الا در هوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به کی گیرم سر خود زانکه هست</p></div>
<div class="m2"><p>از سر من بر سر من این بلا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کار عشق و عاشقی سربازی است</p></div>
<div class="m2"><p>گر سر این ماجرا داری، بیا!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در پی من شو که نتوان یافتن</p></div>
<div class="m2"><p>رهروان را بهتر از من پیشوا</p></div></div>