---
title: >-
    بخش ۵۹ - رفتن مهراب در پی جمشید
---
# بخش ۵۹ - رفتن مهراب در پی جمشید

<div class="b" id="bn1"><div class="m1"><p>همی گردید مهراب از پی جم</p></div>
<div class="m2"><p>بسان جم کزو گم گشته خاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غلامان گرد کوه و دشت پویان</p></div>
<div class="m2"><p>همی گشتند یکسر شاه جویان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس از یکماه دیدندش در آن کوه </p></div>
<div class="m2"><p>چو ماه نو شده باریک از اندوه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز حسرت چشمهایش رفته در غار</p></div>
<div class="m2"><p>سرشک از چشمها ریزان چو کهسار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو آن سرو سهی را دید مهراب</p></div>
<div class="m2"><p>به زیر پای او افتاد و چون آب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو اشک آمد رخ و چشمش بپوشید</p></div>
<div class="m2"><p>ز درد دل بسی در خاک غلطید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آتش نیک پای آورد در چنگ</p></div>
<div class="m2"><p>شکر در تنگ و گوهر یافت در سنگ</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو لعل از تاج شاهی اوفتاده</p></div>
<div class="m2"><p>میان سنگ خارا دل نهاده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به زاری گفت: «ای شمع شب افروز</p></div>
<div class="m2"><p>نمی دانم که افکندت بدین روز؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>الا ای نافه مشکین دلبند</p></div>
<div class="m2"><p>بدین صحرا کدام آهوت افکند؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به چین اول ترا ای مشک اذفر</p></div>
<div class="m2"><p>به خوناب جگر پرورد مادر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هوا زد بر دماغت بوی سودا</p></div>
<div class="m2"><p>فتاد از اندرون رازت به صحرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بوی دوست از مادر بریدی</p></div>
<div class="m2"><p>رها کردی وطن، غربت گزیدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گهی در بحر گردی یا نهنگان</p></div>
<div class="m2"><p>گهی در کوه باشی با پلنگان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به شب نالنده چون مرغ شب آویز</p></div>
<div class="m2"><p>به روز آشفته چون باد سحر خیز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چو گل بر باد رفتی در جوانی</p></div>
<div class="m2"><p>چو می کردی به تلخی زندگانی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>سفر کردی به سودای تجارت</p></div>
<div class="m2"><p>بسی دیدی ازین سودا خسارت</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز سر بیرون کن این سودای فاسد</p></div>
<div class="m2"><p>که بازاریست سست و جنس کاسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مکن زاری که از زاری و شیون</p></div>
<div class="m2"><p>نیفزاید به جز شادی دشمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ملک یکدم برآن گفتار بگریست</p></div>
<div class="m2"><p>زمانی در فراق یار بگریست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نگار خویش را در خورد خود دید</p></div>
<div class="m2"><p>نگارین آب چشم از دیده بارید</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بدان امید کان زیبا نگارش</p></div>
<div class="m2"><p>چو اشک از دیده آرد در کنارش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جوابش داد و گفت: «ای یار همدرد</p></div>
<div class="m2"><p>مشو گرم و مکوب این آهن سرد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دم گرمت مرا این آتش افروخت</p></div>
<div class="m2"><p>به چربی زبان قندیل دل سوخت</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>مرا منع تو افزون می کند شوق</p></div>
<div class="m2"><p>وزین تلخی زیادم می شود ذوق</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل عاشق ملامت بر نتابد</p></div>
<div class="m2"><p>رخ از تیر ملامت بر نتابد</p></div></div>