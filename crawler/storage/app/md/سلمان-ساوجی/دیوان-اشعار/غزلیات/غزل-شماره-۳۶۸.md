---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>نه در کوی تو می‌یابم مجالی </p></div>
<div class="m2"><p>نه می‌بینم وصالت هر به سالی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجالی کی بود بر خاک آن کوی؟</p></div>
<div class="m2"><p>که باد صبح را نبود مجالی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز مهر روی چون ماه تمامت </p></div>
<div class="m2"><p>تنم گشت از ضعیفی، چون هلالی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال خواب دارد، دیده من </p></div>
<div class="m2"><p>مگر کز وصل او، بیند خیالی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو گر برگشتی از پیمان دل من </p></div>
<div class="m2"><p>نگردد هرگز از حالی به حالی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگویم بیش ازین، با تو غم دل </p></div>
<div class="m2"><p>مبادا کز منت گیرد ملالی!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا کز دوری روی تو سلمان </p></div>
<div class="m2"><p>تنش از ناله شد مانند نالی </p></div></div>