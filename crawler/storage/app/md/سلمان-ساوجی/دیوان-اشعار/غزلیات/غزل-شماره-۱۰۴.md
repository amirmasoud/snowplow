---
title: >-
    غزل شمارهٔ ۱۰۴
---
# غزل شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>بر سر کوی غمش، بی سروپا باید رفت </p></div>
<div class="m2"><p>گاه با خویش و گه از خویش جدا، باید رفت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به مقصود از این جا که تویی، یک قدم است </p></div>
<div class="m2"><p>قدمی از پی مقصود، فرا باید رفت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رهبری جو، که درین بادیه هر سوی رهی است </p></div>
<div class="m2"><p>مرد سرگشته چه داند که کجا باید رفت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نگویی سفر صوب حجازست صواب </p></div>
<div class="m2"><p>وقت باشد که تو را راه خطا، باید رفت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان را چو هوای حرم کعبه بود </p></div>
<div class="m2"><p>بر سر خار مغیلان به صفا، باید رفت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا غبار سر کویت نشوم، ننشینم </p></div>
<div class="m2"><p>وگرم خود همه بر باد هوا، باید رفت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنک آن دم، که به بوی سر زلف تو مرا </p></div>
<div class="m2"><p>به فدای قدم باد صبا، باید رفت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرض از کعبه و بتخانه تویی سلمان را </p></div>
<div class="m2"><p>چه کنم خانه پی خانه خدا باید رفت </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقد گنجینه آن خانه، چو در سینه ماست </p></div>
<div class="m2"><p>به گدایی به در خانه، چرا باید رفت </p></div></div>