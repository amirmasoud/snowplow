---
title: >-
    غزل شمارهٔ ۶
---
# غزل شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>زان پیش کاتصال بود خاک و آب، را</p></div>
<div class="m2"><p>عشق تو خانه ساخته بود، این خراب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهر رخت ز آب و گل ما شد آشکار</p></div>
<div class="m2"><p>پنهان به گل چگونه کنند آفتاب را؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا کفر و دین شود، همه یک روی و یک جهت</p></div>
<div class="m2"><p>بردار یک ره، از طرف رخ حجاب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس رخت چو مانع دیدار می‌شود</p></div>
<div class="m2"><p>بهر خدا چه می‌کند آن رخ نقاب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر ما کشید خط خطا مدعی و ما</p></div>
<div class="m2"><p>خط در کشیده‌ایم، خطا و صواب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فردا که نامه عملم را کنند عرض </p></div>
<div class="m2"><p>روشن کنم به روی تو یک یک حساب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک شب خیال تو دیدم ما بخواب</p></div>
<div class="m2"><p>زان چشم، دگر به چشم ندیدم خواب را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی‌وصل تو دو کون، سرابی است پیش ما</p></div>
<div class="m2"><p>در پیش ما چه آب بود خود سراب را؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلمان به خاک کوی تو، تا چشم باز کرد</p></div>
<div class="m2"><p>یکبارگی ز دیده، بینداخت، آب را</p></div></div>