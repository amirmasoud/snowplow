---
title: >-
    غزل شمارهٔ ۲۴۷
---
# غزل شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>کارها دارد دل من با لب جانان هنوز</p></div>
<div class="m2"><p>دور حسنش راست اکنون اول دوران هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهار حسنش از صد گل یکی نشکفته است</p></div>
<div class="m2"><p>گرد گلزارش کنون بر می‌دهد ریحان هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی از چوگان زلف دوست تابی دیده‌ام</p></div>
<div class="m2"><p>لاجرم چون گوی می‌گردیم سرگردان هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر سر بازار عالم راز من در عشق تو</p></div>
<div class="m2"><p>آشکار شد ولی من می‌کنم پنهان هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچنان سودای زلفت می‌دهد تشویش دل</p></div>
<div class="m2"><p>همچنان خطت تصرف می‌کند در جان هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورده‌ام از دست عشقت سال‌ها خون جگر</p></div>
<div class="m2"><p>از نفس می‌آیدم چون نافه بوی جان هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رهروان عشق در بیدای سودایت به سر</p></div>
<div class="m2"><p>سال‌ها رفتند و پیدا نیستش پایان هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بهای یک سر مویت دو عالم می‌دهم</p></div>
<div class="m2"><p>گر بدین قیمت به دست آید، بود ارزان هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نرگس رعنا، شبی در خواب چشمت دیده است</p></div>
<div class="m2"><p>بر نمی‌دارد سر از شرم تو از بستان هنوز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر سر کوی خودم دیروز نرمک با رقیب</p></div>
<div class="m2"><p>گفت یعنی زنده است این سخت جان سلمان هنوز؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دل ز دست دوست می‌نالد که از عشقش جهان</p></div>
<div class="m2"><p>تنگ شد بر من کجایی ای دل نادان هنوز</p></div></div>