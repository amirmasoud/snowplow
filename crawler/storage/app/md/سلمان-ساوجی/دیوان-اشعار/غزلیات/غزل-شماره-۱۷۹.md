---
title: >-
    غزل شمارهٔ ۱۷۹
---
# غزل شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>سلام حال بیماران رسانیدن صبا داند</p></div>
<div class="m2"><p>ولی او نیز بیمارست و می‌ترسم که نتواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبا شوریده سودای زلف اوست می‌ترسم</p></div>
<div class="m2"><p>که گستاخی کند ناگه بران در، حلقه جنباند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوس دارم که درپیچم میانه نامه‌اش خود را</p></div>
<div class="m2"><p>چه می‌پیچم درین سودا مرا چون او نمی‌خواند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر صدباره گرداند به سر چون خامه کاتب را</p></div>
<div class="m2"><p>محال است این که تا باشد سر از خطش بپیچاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن در شرح هجرانش، چه رانم کاندر میدان؟</p></div>
<div class="m2"><p>قلم کو می‌رود چون آب، بر جا خشک می‌ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به مشتاقان خود وقتی که لطفش نامه فرماید</p></div>
<div class="m2"><p>چه باشد نام درویشی اگر در نامه گنجاند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهاده چشم بر راه است سلمان تا کجا بادی</p></div>
<div class="m2"><p>ز راهش خیزد از گرد رهش بر دیده بنشاند؟</p></div></div>