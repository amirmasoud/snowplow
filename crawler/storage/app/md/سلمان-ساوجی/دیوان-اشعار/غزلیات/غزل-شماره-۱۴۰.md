---
title: >-
    غزل شمارهٔ ۱۴۰
---
# غزل شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>ناتوان چشم توام گرچه به زنهار آورد</p></div>
<div class="m2"><p>ناتوان دردسری بر سر بیمار آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مخمور تو را یک نظر از گوشه خویش</p></div>
<div class="m2"><p>مست و سودا زده‌ام بر در خمار آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل را بوی سر زلف تو از کار ببرد</p></div>
<div class="m2"><p>عشق را شور می لعل تو در کار آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفت صورت روی تو به چین می‌کردند</p></div>
<div class="m2"><p>صورت چین ز حسد روی به دیوار آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منکر باده پرستان لب لعلت چو بدید</p></div>
<div class="m2"><p>هم به کفر خود و ایمان من اقرار آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خار سودای تو در دل به هوای گل وصل</p></div>
<div class="m2"><p>بنشاندیم و همه خون جگر بار آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با رخ و زلف تو گفتم که به روز آرم شب</p></div>
<div class="m2"><p>عاقبت هجر تو روزم به شب تار آورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوییا دود کدامین دل آشفته مرا</p></div>
<div class="m2"><p>به کمند سر زلف تو گرفتار آورد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخ ز دیدار تو یک ذره نتابد سلمان</p></div>
<div class="m2"><p>که مرا مهر تو چون ذره پدیدار آورد</p></div></div>