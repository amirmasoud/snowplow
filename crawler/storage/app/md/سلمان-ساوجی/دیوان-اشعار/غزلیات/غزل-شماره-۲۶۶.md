---
title: >-
    غزل شمارهٔ ۲۶۶
---
# غزل شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>به مهر روی تو خواهم رسید، ذره مثال</p></div>
<div class="m2"><p>نمی‌رسد به زمین پایم از نشاط وصال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مه دوهفته درین یک دو روز خواهم دید</p></div>
<div class="m2"><p>که کس نبیند از آن ماه در هزاران سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سواد زلف توام خواهد آمدن در چشم</p></div>
<div class="m2"><p>که بوی عنبر تو می‌دهد نسیم شمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک پای عزیزت که تشنه است لبم</p></div>
<div class="m2"><p>به خاک پای عزیزت چو تشنگان به زلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه دم زنم چو رسم با تو آن دمم باشد</p></div>
<div class="m2"><p>مجال آنکه کنم بر تو عرض صورت حال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم به پیش تو می‌خواست جان فرستادن</p></div>
<div class="m2"><p>ولی کبوتر جان را نبود قوت بال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشیده‌ام تب هجرت، بسی و در شب هجر</p></div>
<div class="m2"><p>نبود بر سر سلمان کسی به غیر حال</p></div></div>