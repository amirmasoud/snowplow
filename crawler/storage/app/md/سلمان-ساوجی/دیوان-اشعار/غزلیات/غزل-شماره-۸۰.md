---
title: >-
    غزل شمارهٔ ۸۰
---
# غزل شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>یار ما را یار بسیارست تا او یار کیست </p></div>
<div class="m2"><p>دل بسی دارد ندانم، زان میان، دلدار کیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک پایش را تصور می‌کند در چشم خویش </p></div>
<div class="m2"><p>هر کسی تا کحل چشم دولت بیدار، کیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میدهم جان و ستانم عشوه، این داد و ستد </p></div>
<div class="m2"><p>جز که در بازار سودای تو، در بازار کیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم مردن به پیشش گفت رویش کار خود </p></div>
<div class="m2"><p>کین نه کار توست ای جان و جهان پس کار کیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان من چون چشم او بیدار شد، گیرم که هست </p></div>
<div class="m2"><p>جان من بیمار چشمش، چشم او بیمار کیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کاشکی دیدی، گل رخسار خود در آینه </p></div>
<div class="m2"><p>تا بدانستی که در پای دل من، خار کیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل ز سلمان برد و خونش خورد و می‌گوید کنون </p></div>
<div class="m2"><p>کار عالم بین، که چون کار من بیکار کیست </p></div></div>