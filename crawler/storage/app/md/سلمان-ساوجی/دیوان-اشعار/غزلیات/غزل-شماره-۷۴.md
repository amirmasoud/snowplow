---
title: >-
    غزل شمارهٔ ۷۴
---
# غزل شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>دل ز جا برخاست ما را، وصل او بر جا نشست </p></div>
<div class="m2"><p>تا بپنداری که عشقش، در دل تنها نشست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاست غوغایی ز قدش، در میان عاشقان </p></div>
<div class="m2"><p>در میان ما نخواهد، هرگز این غوغا نشست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از نخل وجود من، خلالی باز ماند </p></div>
<div class="m2"><p>تا سرم باشد، نخواهم، همچو نخل، از پا نشست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی شد تا دلم، در بند مشک زلف اوست </p></div>
<div class="m2"><p>چون تواند بیش ازین، مسکین درین سودا نشست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من به وصلش کی رسم، جایی که باد صبحدم </p></div>
<div class="m2"><p>تا به درگاهش رسد از ضعف تن، ده جا نشست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهر دیدار جمالش، دل به راه دیده رفت </p></div>
<div class="m2"><p>از پی دردانه و بیچاره در دریا، نشست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جز غمت، کاری نخواهد، بر ضمیر ما گذشت </p></div>
<div class="m2"><p>جز رخت، نقشی نخواهد در خیال ما نشست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که را با شاهدی صحبت به خلوت داد دست </p></div>
<div class="m2"><p>بی‌گمان با حوریی در « جنته الماوی» نشست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>زینهار امروز سلمان با می و حوری نشین </p></div>
<div class="m2"><p>چند خواهی بر امید وعده فردا نشست </p></div></div>