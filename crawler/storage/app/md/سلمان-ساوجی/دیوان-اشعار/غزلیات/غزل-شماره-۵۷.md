---
title: >-
    غزل شمارهٔ ۵۷
---
# غزل شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>فراق روی تو از شرح و بسط، بیرون است </p></div>
<div class="m2"><p>زما مپرس، که حال درون دل، چون است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون نوشته‌ام، این نامه را که خواهی خواند</p></div>
<div class="m2"><p>اگر چه دود درونم، نشسته در خون است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکرد آتش شوق درون قلم ظاهر </p></div>
<div class="m2"><p>مگر ز شوق قلم دود رفته بیرون است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌کنم سخن اشتیاق، کان تقدیر </p></div>
<div class="m2"><p>ز طرف حرف و زحد عبارت، افزون است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و قصه حالم بخوان، که بر رخ من </p></div>
<div class="m2"><p>نوشته دیده، به خطی، چو در مکنون است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیال روی تو دارم، مقام در چشمم </p></div>
<div class="m2"><p>سرشک چشمم، از آن رو مقیم گلگون است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل مقید سلمان، اسیر آن لیلی است </p></div>
<div class="m2"><p>که در سلاسل زلفش، هزار مجنون است </p></div></div>