---
title: >-
    غزل شمارهٔ ۲
---
# غزل شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>امشب من و تو هر دو، مستیم، ز می اما</p></div>
<div class="m2"><p>تو مست می حسنی، من، مست می سودا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از صحبت من با تو، برخاست بسی فتنه</p></div>
<div class="m2"><p>دیوانه چو بنشیند، با مست بود غوغا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن جان که به غم دادم، از بوی تو شد حاصل </p></div>
<div class="m2"><p>وان عمر که گم کردم، در کوی تو شد پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل! به ره دیده، کردی سفر از پیشم</p></div>
<div class="m2"><p>رفتی و که می‌داند، حال سفر دریا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>انداخت قوت دل را، بشکست به یکباره</p></div>
<div class="m2"><p>چون نشکند آخر نی، افتاد از آن بالا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند زنم حلقه؟ در خانه به غیر از تو</p></div>
<div class="m2"><p>چون نیست کسی دیگر، برخیز و درم بگشا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بوی تو من مستم، ساقی مدهم ساغر</p></div>
<div class="m2"><p>بگذار که می‌ترسم، از درد سر فردا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در رهگذر مسجد، از مصطبه بگذشتم</p></div>
<div class="m2"><p>رندی به کفم برزد، دامن، که مرو ز اینجا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نقدی که تو می‌خواهی، در کوی مسلمانی</p></div>
<div class="m2"><p>من یافته‌ام سلمان؟ در میکده ترسا</p></div></div>