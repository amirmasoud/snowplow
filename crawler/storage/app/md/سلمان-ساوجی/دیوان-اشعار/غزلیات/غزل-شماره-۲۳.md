---
title: >-
    غزل شمارهٔ ۲۳
---
# غزل شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>چشمم از پرتو خورشید رخت، گیرد آب</p></div>
<div class="m2"><p>رویت از آتش اندیشه دل یابد تاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مست تو که بر هر طرفی، می‌افتد</p></div>
<div class="m2"><p>بر من افتاد، زمستی و مرا کرد خراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با خیال تو مرا، خواب نیاید در چشم</p></div>
<div class="m2"><p>کو خیالت که طلب می‌کندش، دیده در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر از دیده تو را رغبت خواب است، مگر</p></div>
<div class="m2"><p>آب او ریزی وزین بخت، کنی خواهش آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به تمنای لب لعل تو گردد، بر کف</p></div>
<div class="m2"><p>آتشین جان رسانیده به لب، جام شراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ترا شمع صفت، با همه کس رویی هست</p></div>
<div class="m2"><p>من که پروانه‌ام ای شمع! ز من روی متاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نه از آب و گلی، بلکه همه جان و دلی</p></div>
<div class="m2"><p>که گر از ماء و ترابی، پس ازین ما و تراب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیگران را هوس جنت اگر می‌باشد</p></div>
<div class="m2"><p>روضه جنت سلمان در توست، از همه باب</p></div></div>