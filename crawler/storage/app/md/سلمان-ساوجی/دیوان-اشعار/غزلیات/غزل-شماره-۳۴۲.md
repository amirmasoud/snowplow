---
title: >-
    غزل شمارهٔ ۳۴۲
---
# غزل شمارهٔ ۳۴۲

<div class="b" id="bn1"><div class="m1"><p>مسکین دل من گم شد و کردم طلب وی </p></div>
<div class="m2"><p>بردم به کمانخانه ابروی تواش پی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خامند کسانی که به داغت نرسیدند </p></div>
<div class="m2"><p>من سوخته آن که به من کی رسد او کی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی به سفال کهنم جام جم آور </p></div>
<div class="m2"><p>مطلوب سکندر بد هم در قدح کی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد بار می لعل تو جانم به لب آورد </p></div>
<div class="m2"><p>ای دوست به کامم برسان یکدم از آن می </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب بزن آن ساز جگر سوز دمادم </p></div>
<div class="m2"><p>ساقی بده آن جام دلفروز پیاپی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شرح فراق تو سخن را چه دهم بسط؟</p></div>
<div class="m2"><p>شرط ادب آن است که این نامه کنم طی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی رویت اگر دیده به خورشید کنم باز </p></div>
<div class="m2"><p>صد بار کند چشم من از شرم رخت خوی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی بویت اگر برگذرد باد بهاری </p></div>
<div class="m2"><p>حقا که بود بر دل من سردتر از دی </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلمان ره سودای تو می‌رفت غمت گفت</p></div>
<div class="m2"><p>کین راه به پای چو تویی نیست بروهی </p></div></div>