---
title: >-
    غزل شمارهٔ ۱۴۱
---
# غزل شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>لطف جانبخش تو جانم ز عدم باز آورد</p></div>
<div class="m2"><p>دل آزرده ما را به کرم باز آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاک آن پیک مبارک دم صاحب قدمم</p></div>
<div class="m2"><p>که دلم هم به دم و هم به قدم باز آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سیاهی که شبان خط و خالت با من</p></div>
<div class="m2"><p>کرد انصاف که لطفت بقلم باز آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌کنم خون جگر نوش به شادی لبت</p></div>
<div class="m2"><p>که به یک جرعه مرا از همه غم، باز آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدتی گردش این دایره ما را از هم</p></div>
<div class="m2"><p>همچو پرگار جدا کرد و به هم باز آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواستم رفت به حسرت ز جهان، باز مرا</p></div>
<div class="m2"><p>کشش موی تو از کوی عدم باز آورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خط به خون خواست نوشتن، به تو سلمان ننوشت</p></div>
<div class="m2"><p>تا نگویی که فلان عشوده و دم باز آورد</p></div></div>