---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>درون، ز غیر بپرداز و ساز، خلوت دوست </p></div>
<div class="m2"><p>که اوست، مغز حقیقت، برون از همه پوست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دویی میان تو و دوست هم ز توست، ار نی </p></div>
<div class="m2"><p>به اتفاق دو عالم یکی است، با آن دوست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را نظر همگی بر خود است و آن هیچ است </p></div>
<div class="m2"><p>تو هیچ شو همه، وانگه بدان، که خود همه اوست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برای دیدن رویش مگرد، گرد جهان </p></div>
<div class="m2"><p>که او نشسته، چو آیینه، با تو رو باروست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشو، به نقش و نگار جمال او، قانع </p></div>
<div class="m2"><p>که حسن طلعت آن گل، چو غنچه تو بر توست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به پیش دوست مبر، جز متاع دل، چیزی </p></div>
<div class="m2"><p>اگر چه سنگ دلست، آن صنم، ولی دلجوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر چه آب حیات لبش روانبخش است </p></div>
<div class="m2"><p>هزار، چون خضرش، تشنه مرده بر لب جوست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر به تربت سلمان رسی، ببوی، گلش</p></div>
<div class="m2"><p>که این گل، از اثر صحبت گل خوشبوست </p></div></div>