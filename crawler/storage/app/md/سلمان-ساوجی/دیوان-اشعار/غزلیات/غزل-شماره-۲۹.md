---
title: >-
    غزل شمارهٔ ۲۹
---
# غزل شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>جان نیاید در نشاط، الا که بر بوی حبیب</p></div>
<div class="m2"><p>تا گل رنگین نبالد، خوش ننالد عندلیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عود خشکم؛ آتش جانسوز می‌باید، مرا</p></div>
<div class="m2"><p>تا ز طیب جان، دماغ حاضران گردد، ز طیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولت بوسیدن پایش ندارد، هر کسی</p></div>
<div class="m2"><p>این سعادت نیست، الا در سر زلف حبیب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم دار آخر دمی، با ما، که بادا گوش دار</p></div>
<div class="m2"><p>ایزد از چشم بدانت، اول از چشم رقیب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز و بر ما عرضه کن ایمان، از آن عارض که باز</p></div>
<div class="m2"><p>در میان آورد زلفت، رسم ز ناز و صلیب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌تو جان، در تن بجایی بس غریب افتاده است</p></div>
<div class="m2"><p>جن من دانی به تنها چون بود حال غریب؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دست بیماران گرفتن، بر طبیبان واجب است</p></div>
<div class="m2"><p>من ز پا افتاده‌ام، دستم نمی‌گیرد طبیب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتمش هرگز نشد کامیم، حاصل، زان دهن</p></div>
<div class="m2"><p>از وصالت نیست گویی، هیچ سلمان را نصیب</p></div></div>