---
title: >-
    غزل شمارهٔ ۱۵۵
---
# غزل شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>ما را به جز خیالت، فکری دگر نباشد</p></div>
<div class="m2"><p>در هیچ سر خیالی، زین خوبتر نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی شبروان کویت آرند ره به سویت</p></div>
<div class="m2"><p>عکسی ز شمع رویت، تا راهبر نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما با خیال رویت، منزل در آب دیده</p></div>
<div class="m2"><p>کردیم تا کسی را، بر ما گذر نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرگز بدین طراوت، سرو و چمن نروید</p></div>
<div class="m2"><p>هرگز بدین حلاوت، قند و شکر نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوی عشق باشد، جان را خطر اگر چه</p></div>
<div class="m2"><p>جایی که عشق باشد، جان را خطر نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر با تو بر سر و زر، دارد کسی نزاعی</p></div>
<div class="m2"><p>من ترک سر بگویم، تا دردسر نباشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دانم که آه ما را، باشد بسی اثرها</p></div>
<div class="m2"><p>لیکن چه سود وقتی، کز ما اثر نباشد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خلوتی که عاشق، بیند جمال جانان</p></div>
<div class="m2"><p>باید که در میانه، غیر از نظر نباشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشمت به غمزه هر دم، خون هزار عاشق</p></div>
<div class="m2"><p>ریزد چنانکه قطعاً کس را خبر نباشد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از چشم خود ندارد، سلمان طمع که چشمش</p></div>
<div class="m2"><p>آبی زند بر آتش، کان بی‌جگر نباشد</p></div></div>