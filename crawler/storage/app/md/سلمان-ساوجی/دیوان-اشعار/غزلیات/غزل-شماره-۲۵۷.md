---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>چون تحمل می‌کند تن صحبت پیراهنش</p></div>
<div class="m2"><p>چون کند افتاده است آن این زمان در گردنش؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دست در گردن که یار کرد با او یا که یافت</p></div>
<div class="m2"><p>جز ره پیراهن دولت زهی پیراهنش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوختم در آتشش چون عود و زانم بیم نیست</p></div>
<div class="m2"><p>بیم آن دارم که دود من بگیرد دامنش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قوت صبرم چو کوهی بود از آن کاهی نماند</p></div>
<div class="m2"><p>بس که عشقش می‌دهد بر باد جو جو خرمنش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر دم از شوق تو عارف می‌دهد جانی چو جام</p></div>
<div class="m2"><p>باز ساقی می‌کند روشن روانی در تنش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حاجی ار در کوی او یابد مقامی از حرم</p></div>
<div class="m2"><p>روی بر تابد بگردد بعد از آن پیراهنش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جست دل راهی کزان ره پیش باز آید نهان</p></div>
<div class="m2"><p>بر دو چشم انگشت را بنمود راهی روشنش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من غبار راه یارم یار چون آب حیات</p></div>
<div class="m2"><p>شکر ایزد را که بر خاطر نمی‌آید منش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یار می‌جویی رفیق توست و اینک می‌رود</p></div>
<div class="m2"><p>خیز همچون گرد سلمان دست در گردن زنش</p></div></div>