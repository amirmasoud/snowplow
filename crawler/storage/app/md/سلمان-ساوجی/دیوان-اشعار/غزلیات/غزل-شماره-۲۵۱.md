---
title: >-
    غزل شمارهٔ ۲۵۱
---
# غزل شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>ما از در او دور و چنین بر در و بامش</p></div>
<div class="m2"><p>باد سحری می‌گذرد، باد حرامش!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر گل روی از کله‌اش دام نهادی</p></div>
<div class="m2"><p>مرغان ز هوا روی نهادند به دامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای مرغ ز دام سر زلفش خبرت نیست</p></div>
<div class="m2"><p>گستاخ از آن می‌گذری، بر سر مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی تو بهشت است که شهدست لبانش</p></div>
<div class="m2"><p>لعل تو عقیق است که مشک است ختامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن روی چه رویی است که با آن همه شوکت</p></div>
<div class="m2"><p>شد شاه ریاحین به همه روی غلامش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وقت است که سلطان سراپرده انجم</p></div>
<div class="m2"><p>در مملکت حسن زند سکه بنامش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وصف مه روی تو و مهر دل سلمان</p></div>
<div class="m2"><p>از بس که بگفتیم، نگفتیم تمامش</p></div></div>