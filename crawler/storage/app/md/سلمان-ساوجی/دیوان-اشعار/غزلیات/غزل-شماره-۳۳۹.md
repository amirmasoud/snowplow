---
title: >-
    غزل شمارهٔ ۳۳۹
---
# غزل شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>باز بیمار خودم ساختی و خوش کردی </p></div>
<div class="m2"><p>خون من ریختی و جان مرا پروردی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شرط کردی که دل سوختگان را نبرم </p></div>
<div class="m2"><p>دل من بردی و آن قاعده باز آوردی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیز و چون گرد زنش دست به دامن نه چنان </p></div>
<div class="m2"><p>کاستین بر تو فشاند تو ازو برگردی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز صبا نیست بریدی که برد نامه به دوست </p></div>
<div class="m2"><p>خنکا باد صبا گر نکند دم سردی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌روی گردئ صفت در عقب او سلمان </p></div>
<div class="m2"><p>به ازان نیست که اندر عقب او گردی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر هجران چش اگر عارف صاحب ذوقی </p></div>
<div class="m2"><p>ترک درمان کن اگر عارف صاحب ذوقی </p></div></div>