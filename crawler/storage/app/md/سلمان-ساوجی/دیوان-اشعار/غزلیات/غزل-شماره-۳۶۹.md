---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>جز باد همدمی نه که با او زنم دمی </p></div>
<div class="m2"><p>جز باده مونسی نه که از دل برد غمی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز دیده کو به خون رخ ما سرخ می‌کند </p></div>
<div class="m2"><p>در کار ما نکرد کس از مردمی، دمی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوردم هزار زخم ز هر کس به هیچ یک </p></div>
<div class="m2"><p>رحمی نکرد بر من مسکین به هر مرهمی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریای عشق در دل ما جوش می‌زند </p></div>
<div class="m2"><p>ز آنجا سحاب دیده ما می‌کشد نمی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرمست عشق را ز دو عالم فراغت است </p></div>
<div class="m2"><p>زیرا که دارد او به سر خویش عالمی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان پیش روی بر در او داشتم که داشت </p></div>
<div class="m2"><p>روی زمین غباری و پشت فلک خمی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلمان مگوی را ز دل الا به خود که نیست </p></div>
<div class="m2"><p>در زیر پرده فلک امروز مرحمی </p></div></div>