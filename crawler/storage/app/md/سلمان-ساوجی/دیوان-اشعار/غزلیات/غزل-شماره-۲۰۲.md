---
title: >-
    غزل شمارهٔ ۲۰۲
---
# غزل شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>آن پری کیست که از عالم جان روی نمود؟</p></div>
<div class="m2"><p>وین چه حوری است که بر ما در فردوس گشود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به پروانه غم شمع من از من بستند</p></div>
<div class="m2"><p>می به پیمانه جان لعل تو بر من پیمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه آواز رباب است مخالف با شرع</p></div>
<div class="m2"><p>راستی او ره تحقیق به عشاق نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گل تیره ما گشت نهان خورشیدی</p></div>
<div class="m2"><p>روی خورشید به گل چون بتوانم اندود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما چو عودیم بر آتش، مکش از پا دامن</p></div>
<div class="m2"><p>کز وفا دود برآید چه زیانت زان دود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمر ما کم شد و عشق تو فزون پنداری</p></div>
<div class="m2"><p>کانچه کم گشت زعمرم همه در عشق فزود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچنان نازکی ای گل که اگر با تو نسیم</p></div>
<div class="m2"><p>دم زند، روی تو چون لاله شود خون آلود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده ما به خیال لب عنابی تو</p></div>
<div class="m2"><p>بس که از جام زجاجی عنبی می‌ پالود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بنشستیم پس پرده تقوی، عمری</p></div>
<div class="m2"><p>ناگهان باد هوا آمد و آن پرده ربود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سود سلمان همه این است که سر بر در تو</p></div>
<div class="m2"><p>سود و سرمایه خود را چه زیان کرد و چه سود</p></div></div>