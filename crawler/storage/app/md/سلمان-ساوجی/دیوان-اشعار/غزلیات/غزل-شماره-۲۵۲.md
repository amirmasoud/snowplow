---
title: >-
    غزل شمارهٔ ۲۵۲
---
# غزل شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>در خرابات مغان مست و بهم بر زده دوش</p></div>
<div class="m2"><p>می‌کشیدند مرا چون سر زلف تو به دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدم از باده نوشین و لب نوش لبان</p></div>
<div class="m2"><p>بزم رندان خرابات پر از «نوشانوش»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه حال پریشان من امشب زغمت</p></div>
<div class="m2"><p>به درازی چو سر زلف تو بگذشت ز دوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاقلا پند من بیدل بیهوش مده</p></div>
<div class="m2"><p>می به من ده که ندارم سر عقل و دل و هوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات مغان دلق مرقع نخرند</p></div>
<div class="m2"><p>برو ای خواجه برو دلق مرقع بفروش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامه زرق و لباسات در این ره عیب است</p></div>
<div class="m2"><p>آشکارا چه کنی خرقه قبا ساز و بپوش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چو شمعت بکشد یار از و روی متاب</p></div>
<div class="m2"><p>ور چو چنگت بزند دوست ز دستش مخروش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش شوق رخت جرعه صفت سلمان را</p></div>
<div class="m2"><p>آبرو ریخته بر خاک در باده فروش</p></div></div>