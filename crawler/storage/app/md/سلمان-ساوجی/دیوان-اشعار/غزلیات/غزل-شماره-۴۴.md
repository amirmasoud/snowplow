---
title: >-
    غزل شمارهٔ ۴۴
---
# غزل شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>از کوی مغان، نیم شبی، ناله نی، خاست</p></div>
<div class="m2"><p>زاهد به خرابات مغان آمد و می‌، خواست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما پیرو آن راهروانیم، که ما را</p></div>
<div class="m2"><p>چون نی بنماید، به انگشت، ره راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من کعبه و بتخانه نمی‌دانم و دانم</p></div>
<div class="m2"><p>کانجا که تویی، کعبه ارباب دل، آنجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آنکه به فردا دهی امروز، مرا بیم!</p></div>
<div class="m2"><p>رو، بیم کسی کن که امیدیش به فرداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواهیم که بر دیده ما، بگذرد آن سرو</p></div>
<div class="m2"><p>تا خلق بدانند که او، بر طرف ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشست غمت در دل من تنگ و ندانم</p></div>
<div class="m2"><p>با ما چنین تنگ نشینی، ز کجا خواست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بسیار مشو غره، بدین حسن دلاویز</p></div>
<div class="m2"><p>کین حسن دلاویز تو را حسن من آراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جمعیت حسنی، که سر زلف تو دارد</p></div>
<div class="m2"><p>از جانب دلهای پراکنده شیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از عقد سر زلف و رقوم خط مشکین</p></div>
<div class="m2"><p>حاصل غم عشق، آمد و باقی همه سوداست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق تو ز سلمان، دل و جان و خرد و هوش</p></div>
<div class="m2"><p>بربود کنون، مانده و مسکین‌ تن و تنهاست</p></div></div>