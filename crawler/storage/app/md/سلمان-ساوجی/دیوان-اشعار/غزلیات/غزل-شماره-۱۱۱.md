---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>آن پری چهره که ما را نگران می‌دارد </p></div>
<div class="m2"><p>چشم با ما و نظر، با دگران می‌دارد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیر لب می‌دهم وعده، که کامت بدهم </p></div>
<div class="m2"><p>غالب آن است که ما را به زبان، می‌دارد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش گفتم که غمت، جان مرا داد به باد </p></div>
<div class="m2"><p>گفت ای ساده، هنوزت غم جان می‌دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رایگان، چون سر و زر در قدمش، می‌بازم </p></div>
<div class="m2"><p>سر چرا بر من شوریده، گران می‌دارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اغی گل از حال دل بلبل بیچاره بپرس </p></div>
<div class="m2"><p>تا این همه فریاد و فغان می‌دارد؟ </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به دیدار تو فرسوده‌ای، آسوده شود </p></div>
<div class="m2"><p>مایه حسن رخت را چه زیان، می‌دارد؟ </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خبرت نیست که در باغ جمالت، همه شب </p></div>
<div class="m2"><p>چشم من آب گل و سرو روان می‌دارد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رفته بود از سر قلاشی و رندی، سلمان </p></div>
<div class="m2"><p>چشم سرمست تو‌اش، باز بر آن می‌دارد </p></div></div>