---
title: >-
    غزل شمارهٔ ۲۶۱
---
# غزل شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>می‌کند غارت صبر و دل و دین سودایش</p></div>
<div class="m2"><p>آنکه او هیچ ندارد، چه غم از یغمایش؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دل و جان من دلشده بودی بر جای</p></div>
<div class="m2"><p>کردمی در دل و جان جای چو بودی رایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رقم هستی من عاقبت از لوح وجود</p></div>
<div class="m2"><p>برود لیک بماند اثر سودایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لایق ضرب محبت نبود هر قلبی</p></div>
<div class="m2"><p>که ز اخلاص حکایت نکند سیمایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواب ما را ز خیالش بنمود اسبابی</p></div>
<div class="m2"><p>بعد از آن روز ندیدیم بخواب آسایش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دست در دامن او می‌زنم و می‌کشمش</p></div>
<div class="m2"><p>تا بر غم سر من سر ننهد در پایش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عجب آن است که در بزم ریاحین گل را</p></div>
<div class="m2"><p>زیر شمشاد نشانند و تو بر بالایش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در پی باد صبا چند رود سرگردان</p></div>
<div class="m2"><p>دل به بوی شکن طره عنبر سایش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>که خبر یابد از آمد شدن پیک نسیم</p></div>
<div class="m2"><p>که ز بوی سر زلف تو کند رسوایش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غم عشق تو چه خوش می‌خورد اولی خونم</p></div>
<div class="m2"><p>که به پالوده‌ام از دیده خون پالایش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که امروز به خلوت نفسی با تو نشست</p></div>
<div class="m2"><p>غالبا رغبت جنت نبود فردایش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در شب تیره زلفت دل سلمان گم شد</p></div>
<div class="m2"><p>شمعی از چهره بر افروز و رهی بنمایش</p></div></div>