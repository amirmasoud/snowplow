---
title: >-
    غزل شمارهٔ ۲۷۹
---
# غزل شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>بی دوست من از باغ ارم یاد نیارم </p></div>
<div class="m2"><p>ور جنت فردوس بود، دوست ندارم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دست رقیبان نروم، ور برود سر </p></div>
<div class="m2"><p>من خاک در دوست به دشمن نگذارم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پرورده به خون جگرش بودم و چون اشک </p></div>
<div class="m2"><p>از دیده من رفت و نیامد به کنارم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن دم که دهم جان و به خاکم بسپارند </p></div>
<div class="m2"><p>من خاک درش را به دل و جان نسپارم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر خاک درش میرم و چون خاک شوم من </p></div>
<div class="m2"><p>زان در نتوانند برانگیخت غبارم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نامه چو نامت نبود نامه نخواهم </p></div>
<div class="m2"><p>و آن دم که به یادت نزنم دم نشمارم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کو دولت آنم که شبی با تو نشینم؟</p></div>
<div class="m2"><p>کو فرصت آنم که دمی با تو برآرم؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در نامه همه شرح فراق تو، نویسم </p></div>
<div class="m2"><p>بر دیده همه نقش خیال تو نگارم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشمان سیاه تو به اول نظرم مست </p></div>
<div class="m2"><p>کردند و بکشتند در آخر به خمارم </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یارب چه دلست آن دل سنگین که نشد نرم؟</p></div>
<div class="m2"><p>از « یارب‌» دلسوز من و ناله زارم </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گویند که سلمان سر و جان در قدمش باز </p></div>
<div class="m2"><p>گر کار به سر می‌رودم بر سر کارم </p></div></div>