---
title: >-
    غزل شمارهٔ ۱۳۱
---
# غزل شمارهٔ ۱۳۱

<div class="b" id="bn1"><div class="m1"><p>نه قاصدی که پیامی، به نزد یار برد</p></div>
<div class="m2"><p>نه محرمی، که سلامی بدان دیار، برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو باد راهروی صبح خیز می‌خواهم</p></div>
<div class="m2"><p>که ناله سحر به گوش یار برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا اگر چه رسول من است بیمار است</p></div>
<div class="m2"><p>بدین بهانه مبادا که روزگار برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتاده‌ایم به شهری غریب و یاری نیست</p></div>
<div class="m2"><p>که قصه‌ای ز فقیری به شهریار برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من آن نیم که توانم بدان دیار شدن</p></div>
<div class="m2"><p>صبا مگر ز سر خاک من غبار برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو اختیار منی از جهانیان و جهان</p></div>
<div class="m2"><p>در آن هوس که ز دست من اختیار برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلام ساقی لعل توام که چاره من</p></div>
<div class="m2"><p>به جرعه می‌نوشین خوشگوار برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیار ساقی از آن می که می‌پرستان را</p></div>
<div class="m2"><p>دمی به کار بدارد، دمی ز کار برد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می میار که درد سر و خمار آرد</p></div>
<div class="m2"><p>از آن می آرد که هوش آرد و خمار برد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هزار بار دلم هست و در میان دل نیست</p></div>
<div class="m2"><p>در این میان دل سلمان کدام بار برد؟</p></div></div>