---
title: >-
    غزل شمارهٔ ۳۴۰
---
# غزل شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>دلا من قدر وصل او ندانستم تو می‌دانی </p></div>
<div class="m2"><p>کنون دانستم و سودی نمی‌دارد پشیمانی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب وصل تو شد روزی و قدرش من ندانستم </p></div>
<div class="m2"><p>به دشواری توان دانست قدر آسانی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به بایدی نا گه از رویت فتادم دور چون مویت </p></div>
<div class="m2"><p>به سر می‌آورم دور از تو عمری در پریشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آب دیده هر ساعت نویسم نامه‌ای لیکن </p></div>
<div class="m2"><p>تو حال ما نمی‌پرسی و نقش ما نمی‌خوانی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حدیث کار و بار دل چه گویم بارها گفت:</p></div>
<div class="m2"><p>که بد حال است و تو حال دل من نیک می‌دانی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر خود را نمی‌دانم سزای خاک درگاهت </p></div>
<div class="m2"><p>ولیکن کرده‌ام حاصل من این منصب به پیشانی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الا ای بخت کی باشد که باز آن سرور رعنا را </p></div>
<div class="m2"><p>بدست آری بناز اندر کنار ماش بنشانی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا چون نیست امکان تصرف در سر کویش </p></div>
<div class="m2"><p>نگر تا حلقه اقبال ناممکن نجنبانی!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو زلف او مرا جانی است سودایی ز من بستان </p></div>
<div class="m2"><p>به شرط آنکه چون پیشش رسی در پایش افشانی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برو در یک نفس بازا که یک دم ماند سلمان را </p></div>
<div class="m2"><p>نخواهی یافتن بازش دمی گر دیرتر مانی </p></div></div>