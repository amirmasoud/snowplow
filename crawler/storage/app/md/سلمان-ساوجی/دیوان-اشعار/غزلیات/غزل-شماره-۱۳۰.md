---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>باد هوای کویت، گرد از جهان برآرد </p></div>
<div class="m2"><p>آب جمال رویت، ز آتش، فغان برآرد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبی بر آتشم زن، زان پیشتر، که ناگه </p></div>
<div class="m2"><p>خاک مرا هوایت، باد از میان، برآرد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر هر زمین که افتد، از قامت تو سایه </p></div>
<div class="m2"><p>تا دامن قیامت، آن خاک جان برآرد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مثلث فلک نبیند، با صد هزار دیده </p></div>
<div class="m2"><p>چند آنچه دیده‌ها را، گرد جهان برآرد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلمان سری و جانی، یک دم اشارتی کن </p></div>
<div class="m2"><p>تا آن سبک ببازد، تا این روان برآرد</p></div></div>