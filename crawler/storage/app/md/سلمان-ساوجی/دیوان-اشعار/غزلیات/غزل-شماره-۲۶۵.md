---
title: >-
    غزل شمارهٔ ۲۶۵
---
# غزل شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>به غیر صورت او هر چه آیدم در دل</p></div>
<div class="m2"><p>به جان دوست که باشد تصور باطل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کوی دوست که خاکش به آب دیده گل است</p></div>
<div class="m2"><p>که برگذشت که پایش فرو نرفت به گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قتیل تیغ تو خواهیم گشت تا در حشر</p></div>
<div class="m2"><p>بدین بهانه بگیریم دامن قاتل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی رویم به راهی که نیستش پایان</p></div>
<div class="m2"><p>فتاده‌ایم به بحری که نیستش ساحل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرت ارادت پیوند دوست می‌باشد</p></div>
<div class="m2"><p>برو نخست ز دنیا و آخرت بگسل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجز دهان توام هیچ آروزیی نیست</p></div>
<div class="m2"><p>ولی چه سود که هیچم نمی‌شود حاصل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسود گفت که سلمان چه می‌روی پی یار</p></div>
<div class="m2"><p>نمی‌روم پی دلدار می‌روم پی دل</p></div></div>