---
title: >-
    غزل شمارهٔ ۲۶۰
---
# غزل شمارهٔ ۲۶۰

<div class="b" id="bn1"><div class="m1"><p>نداشت این دل شوریده تاب سودایش</p></div>
<div class="m2"><p>سرم برفت و نرفت از سرم تمنایش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به نرد درد چو وامق نبود مرد حریف</p></div>
<div class="m2"><p>هزار دست پیاپی ببرد عذرایش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی نتافت از و سر چو زلفش از بن گوش</p></div>
<div class="m2"><p>سیاه روی درآمد فتاد و در پایش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غمش ز جای خودم برد و خود چه جای من است</p></div>
<div class="m2"><p>که گر به کوه رسد، برکند دل از جایش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رخ مرا که برو سیم اشک می‌آید</p></div>
<div class="m2"><p>بیان عشق عیان می‌شود ز سیمایش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نهفته داشت دلم راز عشق چون غنچه</p></div>
<div class="m2"><p>هوای دوست دمش داد و کرد رسوایش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل مرا که امروز رنجه داشت چه غم</p></div>
<div class="m2"><p>دلم خوش است که خواهد نواخت فردایش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه امید به آلا و رحمتش دارد</p></div>
<div class="m2"><p>وجود من که ز سر تا بپاست آلایش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گناهکار و فرومانده‌ام ببخش مرا</p></div>
<div class="m2"><p>که هست بر من بیچاره جای بخشایش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سواد هستی سلمان ز روی لوح وجود</p></div>
<div class="m2"><p>رود ولیک بماند نشان سودایش</p></div></div>