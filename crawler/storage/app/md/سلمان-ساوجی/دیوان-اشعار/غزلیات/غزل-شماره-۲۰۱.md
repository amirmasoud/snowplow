---
title: >-
    غزل شمارهٔ ۲۰۱
---
# غزل شمارهٔ ۲۰۱

<div class="b" id="bn1"><div class="m1"><p>اگرم بر سر آتش بنشانی چون عود</p></div>
<div class="m2"><p>نیست ممکن که برآید ز من سوخته دود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سرم هرچه رود خاک رهم گو: می‌رو</p></div>
<div class="m2"><p>نیستم باد که از کوی تو برخیزم زود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم از باغ تو چون غنچه به بویی خوشدل</p></div>
<div class="m2"><p>منم از کوی تو چون باد، به گردی خشنود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوقم افزون شد و آرام کم و صبر نماند</p></div>
<div class="m2"><p>در فراق تو ولی عهد همانست که بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌شراب عنبی را که به موی مژه‌ام</p></div>
<div class="m2"><p>دیده بر یاد تو از جام زجاجی پالود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خنده‌ای زد دهنت، تنگ شکر پیدا کرد</p></div>
<div class="m2"><p>هر یکی گوهر پاکیزه خود باز نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عمر من کم شد و عشق تو فزون پنداری</p></div>
<div class="m2"><p>کانچه از عمر کم آمد، همه در عشق فزود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دیده از غیر تو تا خلوت دل خالی کرد</p></div>
<div class="m2"><p>جز به روی تو مرا، هیچ دردل نگشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>وه که چون غنچه چه مشکین نفسی ای سلمان؟</p></div>
<div class="m2"><p>نیست مشکین دمت الا زدم خون آلود</p></div></div>