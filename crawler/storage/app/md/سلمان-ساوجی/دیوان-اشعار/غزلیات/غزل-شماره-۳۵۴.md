---
title: >-
    غزل شمارهٔ ۳۵۴
---
# غزل شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>به نیازی که با خدا داری </p></div>
<div class="m2"><p>که دلم بیش ازین نیازاری </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من نیاز آرم ار تو ناز آری </p></div>
<div class="m2"><p>من نیاز آرم ار تو ناز آری </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل من برده‌ای ز دست مده </p></div>
<div class="m2"><p>چه شود گر دلی به دست آری </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای ز زاری عاشقان بیزار </p></div>
<div class="m2"><p>عاشقان چون کنند بی‌زاری </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زارم از بی زری و می‌ترسم </p></div>
<div class="m2"><p>که کشد بی زری به بیزاری </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چاره کار من زرست چو نیست </p></div>
<div class="m2"><p>زاریی می‌کنم به ناچاری </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخت خود را به خواب می‌بینم </p></div>
<div class="m2"><p>کاشکی دیدمی به بیداری </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من افتاده بر توانم خواست </p></div>
<div class="m2"><p>از سر جان اگر کنی یاری </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما نیاریم کرد در تو نظر </p></div>
<div class="m2"><p>نظری کن به ما اگر یاری </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بوی زلفت اگر مدد ندهد </p></div>
<div class="m2"><p>برنخیزد صبا ز بیماری </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بار دل بس نبود سلمان را </p></div>
<div class="m2"><p>عشق در می‌خورد به سر، باری </p></div></div>