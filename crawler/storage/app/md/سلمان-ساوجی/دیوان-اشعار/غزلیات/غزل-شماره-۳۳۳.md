---
title: >-
    غزل شمارهٔ ۳۳۳
---
# غزل شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>دورم از جانان و مسکین آنکه شد مهجور ازو</p></div>
<div class="m2"><p>چون تنی باشد که جانش رفته باشد دور ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذره حالم نمی‌گردد ز حال ذره‌ای</p></div>
<div class="m2"><p>کافتاب عالم آرا بازگیرد نور ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گو نسیم صبح از خاک درش بویی دهد</p></div>
<div class="m2"><p>بو که بستانم دمی داد دل رنجور ازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی به جوی چشم من بازآید آن آب حیات</p></div>
<div class="m2"><p>تا خراب آباد جان من شود معمور ازو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خضر زان چشمه نوشین نشانی باز ده</p></div>
<div class="m2"><p>کاروزی شربتی دارد دل محرور ازو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مستش راوق افشان کرد چشمم را بپرس</p></div>
<div class="m2"><p>تا چه می‌خواهد مدام آن نرگس مخمور ازو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل چو رازش گفت با جان من نبودم در میان</p></div>
<div class="m2"><p>در درون او بود و بس شد راز او مشهور ازو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه باداباد خواهم راز دل با باد گفت</p></div>
<div class="m2"><p>همدم است القصه نتوان داشتن مستور ازو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر بیاض دیده سلمان می‌کند نقش سواد</p></div>
<div class="m2"><p>کان جو بگشاید ببارد لولو منثور ازو</p></div></div>