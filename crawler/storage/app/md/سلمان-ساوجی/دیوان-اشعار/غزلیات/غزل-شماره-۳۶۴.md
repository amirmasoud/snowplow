---
title: >-
    غزل شمارهٔ ۳۶۴
---
# غزل شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>صنما مرده آنم که تو جانم باشی </p></div>
<div class="m2"><p>می‌دهم جان که مگر جان جهانم باشی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز عمر من مسکین به شب آمد تا تو </p></div>
<div class="m2"><p>روشنایی دل و شمع روانم باشی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بار گردون و غم هر دو جهان در دل من </p></div>
<div class="m2"><p>نه گران باشد اگر تو نگرانم باشی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به سودای تو‌ام عمر زیان است چه غم </p></div>
<div class="m2"><p>سودم این بس که تو خرم به زیانم باشی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو سراپا همه آنی و همه آن تواند </p></div>
<div class="m2"><p>غرض من همگی آنکه تو آنم باشی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من نهان درد دلی دارم و آن دل بر توست </p></div>
<div class="m2"><p>ظاهرا با خبر از درد نهانم باشی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان برون کرده‌ام از دل همگی داده به تو </p></div>
<div class="m2"><p>جای دل تا تو بجای دل و جانم باشی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون در اندیشه روم گرد درونی گردی </p></div>
<div class="m2"><p>چو در آیم به سخن ورد زبانم باشی </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در معانی صفات تو چه گوید سلمان </p></div>
<div class="m2"><p>هر چه گویم تو منزه ز بیانم باشی </p></div></div>