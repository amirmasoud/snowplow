---
title: >-
    غزل شمارهٔ ۲۸۶
---
# غزل شمارهٔ ۲۸۶

<div class="b" id="bn1"><div class="m1"><p>عزم آن دارم که با پیمانه پیمانی کنم </p></div>
<div class="m2"><p>وین سبوی زرق را بر سنگ قلاشی کنم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من خراب مسجد و افتاده سجاده‌ام </p></div>
<div class="m2"><p>می‌روم باشد که خود را در خرابات افکنم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی دوران هر آن خون کز گلوی شیشه ریخت </p></div>
<div class="m2"><p>گر بجویی باز یابی خون او در گردنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدا با من مپیما قصه پیمان که من </p></div>
<div class="m2"><p>از پی پیمانه‌ای صد عهد و پیمان بشکنم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر به دوزخ بگذرم کوی مغان باشد رهم </p></div>
<div class="m2"><p>ور به جنت در شوم میخانه باشد مسکنم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر نوای ناله مستانه‌ام هر آفتاب </p></div>
<div class="m2"><p>زهره همچون ذره رقصد در هوای روزنم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رشته جانم بسوزاند دمادم عشق و تاب </p></div>
<div class="m2"><p>من چراغم گوییا عشق آتش و من روغنم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زنده می‌گردم به می بی‌منت آب حیات </p></div>
<div class="m2"><p>خود چرا باید کشیدن ننگ هر تر دامنم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من پس از صد عصر کاندر زیر گل باشم چو می </p></div>
<div class="m2"><p>گردد از یاد قدح خندان روان روشنم </p></div></div>