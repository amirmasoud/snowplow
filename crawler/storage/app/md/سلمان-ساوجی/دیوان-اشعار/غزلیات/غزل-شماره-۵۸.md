---
title: >-
    غزل شمارهٔ ۵۸
---
# غزل شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>شب است و بادیه و دل، فتاده از راه است </p></div>
<div class="m2"><p>ز چپ و راست، مخالف، ز پیش و پس، چاه است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مقم تهلکه است این ولی منم، فارغ</p></div>
<div class="m2"><p>ز کار دل، که به دلخواه یار دلخواه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرا سری است که دارم، بر آستانه تو</p></div>
<div class="m2"><p>نهاده‌ایم به پیش تو هرچه در راه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وصل قد تو دارم بسی امید و لیک</p></div>
<div class="m2"><p>قبای عمر به قد امید، کوتاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عکس طالب منصب، شویم خاک درت</p></div>
<div class="m2"><p>از این رفیع ترا آخر چه منصب و راه است؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که آورد به تو احوال دیده و دل من؟</p></div>
<div class="m2"><p>که پیک دیده، سرشک و رسول دل، آه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منور است به مهر تو، سینه عشاق</p></div>
<div class="m2"><p>بلی زجانب مهر است، هرچه در ماه است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پس از فراق تو گر بنده، زنده خواهد ماند</p></div>
<div class="m2"><p>بحق وصل تو کان زیستن، به اکراه است</p></div></div>