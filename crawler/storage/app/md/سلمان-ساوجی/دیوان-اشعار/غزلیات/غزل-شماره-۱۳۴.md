---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>ز کویش نسیم صبا بوی برد</p></div>
<div class="m2"><p>به بویش دلم پی بدان کوی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از چنبر زلف او چون جهد؟</p></div>
<div class="m2"><p>که باد سحر جان به یک سوی ‌برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال کنارش بسی داشتند</p></div>
<div class="m2"><p>ز هی پیرهن کز میان گوی برد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پشتی رویش قوی گشت زلف</p></div>
<div class="m2"><p>دل عالمی را از آن روی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سهی سرو من تاز چشمم برفت</p></div>
<div class="m2"><p>به یکبارگی آبم از جوی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>که راز پریشان ما فاش کرد؟</p></div>
<div class="m2"><p>که چون زلف او باد هر سوی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مگر زلف او گفت در گوش او</p></div>
<div class="m2"><p>صبا در گذر بود از آن بوی برد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلی داشت سلمان، شد آن نیز گم</p></div>
<div class="m2"><p>چرا گم شد آن لعل دلجوی برد؟</p></div></div>