---
title: >-
    غزل شمارهٔ ۳۲۴
---
# غزل شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>قدم خمیده گشت، ز بار بلاست این </p></div>
<div class="m2"><p>اشکم روان شدست، ز عین عناست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خویش ره نداد دلم هیچ صورتی </p></div>
<div class="m2"><p>غیر خیال دوست که گفت آشناست این؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمریست تا نشسته‌ام ای دوست بر درت!</p></div>
<div class="m2"><p>نگذشت بر دلت که برین در چراست این؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌گفت: کام جان تو از لب روا کنم</p></div>
<div class="m2"><p>این خود نکرد جان به لب آمد رواست این </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذشت دوش بر من و انگشت می‌نهاد </p></div>
<div class="m2"><p>بر دیده گفتمش: صنما بر کجاست این؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تهدید می‌نمود ولی گفت: چشم من </p></div>
<div class="m2"><p>دل می‌برد ز مردم والحق جفاست این </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او می‌کند جفا و من انگشت می‌نهم </p></div>
<div class="m2"><p>بر حرف عین خویش که عین خطاست این </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عهدی است تا نمی‌شنوم بویت از صبا </p></div>
<div class="m2"><p>از توست یا ز سستی باد صباست این</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می‌زد غم تو حلقه و در بسته بود دل </p></div>
<div class="m2"><p>جان گفت در مبند که دلدار ماست این </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر در رهش نهادم و گفتم: قبول کن! </p></div>
<div class="m2"><p>گفتا: چه می‌کنم که محل بلاست این؟</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پرسیده‌ای که ناله سلمانت از چه خواست؟</p></div>
<div class="m2"><p>آیینه را بخواه و ببین کز چه خاست این؟</p></div></div>