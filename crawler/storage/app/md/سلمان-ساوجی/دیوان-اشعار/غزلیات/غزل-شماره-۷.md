---
title: >-
    غزل شمارهٔ ۷
---
# غزل شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>نظری نیست، به حال منت ای ماه، چرا؟</p></div>
<div class="m2"><p>سایه برداشت ز من مهر تو ناگاه چرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن است این که مرا، آینه عمر، تویی</p></div>
<div class="m2"><p>در تو آهم نکند، هیچ اثر، آه چرا؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر منم دور ز روی تو، دل من با توست</p></div>
<div class="m2"><p>نیستی هیچ، ز حال دلم آگاه چرا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برگرفتی ز سر من، همگی سایه مهر</p></div>
<div class="m2"><p>سرو نورسته من، «انبتک الله» چرا؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل در آن چاه زنخ مرد و به مویی کارش</p></div>
<div class="m2"><p>بر نمی‌آوری، ای یوسف از آن چاه چرا؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیک‌خواه توام و روی تو، دلخواه من است</p></div>
<div class="m2"><p>می‌رود عمر عزیزم، نه به دلخواه چرا؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پادشاه منی و من، ز گدایان توام</p></div>
<div class="m2"><p>از گدایان، خبری نیستت ای ماه چرا؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در ازل، خواند به خود حضرت تو سلمان را</p></div>
<div class="m2"><p>«حاش لله» که بود، رانده درگاه چرا؟</p></div></div>