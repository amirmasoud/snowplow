---
title: >-
    غزل شمارهٔ ۳۵۹
---
# غزل شمارهٔ ۳۵۹

<div class="b" id="bn1"><div class="m1"><p>ترک من می‌آیی و دلها به یغما می‌بری </p></div>
<div class="m2"><p>روی پنهان می‌کنی، دل آشکارا می‌بری </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دی دل من برده‌ای، امروز دین اکنون مرا </p></div>
<div class="m2"><p>نیم جانی مانده است، آن نیز فردا می‌بری </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه گفتی: بود بالایش مرا ای دل منت </p></div>
<div class="m2"><p>منکرم زیرا که خود را بس به بالا می‌بری </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کفر زلفت را به دین من می‌خرم زیرا به دین </p></div>
<div class="m2"><p>سر فرو می‌آورد، لیکن تو در پا می‌بری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نمی‌دانم کزین دل بردنت مقصود چیست؟</p></div>
<div class="m2"><p>بارها گفتی: نخواهم برد، اما می‌بری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویی یک زمان آرام گیر و صبر کن </p></div>
<div class="m2"><p>چون کنم کارام و صبر و طاقت از ما می‌بری </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من چو وامق باختم در نرد سودایت روان </p></div>
<div class="m2"><p>زین روان بازی چه سودم چون تو عذرا می‌بری </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ عاقل در سر کویت به پای خود نرفت </p></div>
<div class="m2"><p>زلف می‌آری به صد زنجیر و آنجا می‌بری </p></div></div>