---
title: >-
    غزل شمارهٔ ۲۳۳
---
# غزل شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>یار می‌آید و در دیده چنان می‌آید </p></div>
<div class="m2"><p>که پری پیکری از عالم جان می‌آید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر سودای تو گنجی است نهان در دل من </p></div>
<div class="m2"><p>به زیان می‌رود آن چون به زبان می‌آید </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من گرفتم که ز عشق تو حکایت نکنم </p></div>
<div class="m2"><p>چه کنم کز در و دیوار فغان می‌آید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جمالت که اگر بی تو نظر بر خورشید </p></div>
<div class="m2"><p>می‌کنم در نظرم تیغ و سنان می‌آید </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به حیاتت که اگر می‌خورم از دست تو زهر </p></div>
<div class="m2"><p>خوشتر از آب حیاتم به دهان می‌آید </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا تویی در دل من کی دگری می‌گنجد؟</p></div>
<div class="m2"><p>یا کجا در نظرم هر دو جهان می‌آید؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرهم لطف خوش آید همه کس را لیکن </p></div>
<div class="m2"><p>زخم تیغ تو مرا خوشتر از آن می‌آید </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دلم صحبت آن کس که ندارد ذوقی </p></div>
<div class="m2"><p>گر همه جان عزیز است، گران می‌آید </p></div></div>