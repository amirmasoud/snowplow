---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>ز صبا سنبل او دوش به هم بر می‌شد </p></div>
<div class="m2"><p>وز نسیمش همه آفاق معطر می‌شد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سواد شکن زلف به هم بر شده‌اش</p></div>
<div class="m2"><p>دیدم احوال جهانی که به هم بر می‌شد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز دل و دیده نمی‌رفت خیالت که مرا </p></div>
<div class="m2"><p>با دل و دیده خیال تو برابر می‌شد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دهن از یاد تو چون غنچه معطر می‌گشت </p></div>
<div class="m2"><p>سینه از مهر تو چون صبح منور می‌شد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهم از سینه، چو عیسی، به فلک بر می‌رفت</p></div>
<div class="m2"><p>اشکم از دیده، چو قارون به زمین برمی‌شد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنشستم که فراقت به قلم شرح دهم </p></div>
<div class="m2"><p>شرح می دادم و طومار به خون تر می‌شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به گلم پای فرو رفته، چندانکه زغم </p></div>
<div class="m2"><p>می‌زدم دست به سر پای فروتر می‌شد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روز اول که سر زلف تو را سلمان دید</p></div>
<div class="m2"><p>دید ‌کش جان و دل و دیده در آن سر می‌شد</p></div></div>