---
title: >-
    غزل شمارهٔ ۳۷۸
---
# غزل شمارهٔ ۳۷۸

<div class="b" id="bn1"><div class="m1"><p>می‌آیی و دمی دو سه در کار می‌کنی </p></div>
<div class="m2"><p>ما را به دام خویشتن گرفتار می‌کنی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دین می‌خری به عشوه و دل می‌بری ز دست </p></div>
<div class="m2"><p>آری تو زین معامله بسیار می‌کنی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم هزار بی سر و پا را چو زلف خویش </p></div>
<div class="m2"><p>برمی‌کشی و باز نگونسار می‌کنی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم دلی خراب به غایت ضعیف و تو </p></div>
<div class="m2"><p>هر جه غمی است بر دل من بار می‌کنی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خواب، آن دو چشم گران خواب را ممال </p></div>
<div class="m2"><p>زنهار فتنه‌ای را به چه بیدار می‌کنی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حلقه‌های زلف خود آتش فروختی </p></div>
<div class="m2"><p>وین از برای گرمی بازار می‌کنی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زان خط که گرد دایره روی می‌کشی </p></div>
<div class="m2"><p>روز سفید ما چو شب تار می‌کنی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>من پرده بر سرایر عشق تو می‌کشم </p></div>
<div class="m2"><p>لیکن تو هتک پرده اسرار می‌کنی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلمان چو آفتاب به کویش بر آ چرا </p></div>
<div class="m2"><p>چون سایه سجده پس دیوار می‌کنی </p></div></div>