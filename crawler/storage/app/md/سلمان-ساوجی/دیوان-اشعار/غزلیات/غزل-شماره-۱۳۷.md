---
title: >-
    غزل شمارهٔ ۱۳۷
---
# غزل شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>آخرت روزی ز سلمان یاد می‌بایست کرد</p></div>
<div class="m2"><p>خاطر غمگین او را شاد می‌بایست کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهدها کردی که آخر هیچ بنیادی نداشت</p></div>
<div class="m2"><p>روز اول کار بر بنیاد می‌بایست کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد من یک روز می‌بایست دادن بعد از آن</p></div>
<div class="m2"><p>هرچه می‌شایست از بیداد، می‌بایست کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اشک من از مردم چشمم بزاد آخر تو را</p></div>
<div class="m2"><p>رحمتی بر اشک مردم زاد می‌بایست کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل ای دل گفتمت: گر وصل یارت آرزوست</p></div>
<div class="m2"><p>جان فدا کن، هر چه بادا باد می‌بایست کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صحبتش چون آینه، گر روبرو می‌خواستی</p></div>
<div class="m2"><p>پشت بر زر روی بر پولاد می‌بایست کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر تو شاهی جهان در روز و شب می‌خواستی</p></div>
<div class="m2"><p>بندگی حضرت دلشاد می‌بایست کرد</p></div></div>