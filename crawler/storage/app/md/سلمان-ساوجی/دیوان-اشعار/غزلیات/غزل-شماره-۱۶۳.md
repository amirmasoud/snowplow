---
title: >-
    غزل شمارهٔ ۱۶۳
---
# غزل شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>شبهای فراقت را، آخر سحری باشد </p></div>
<div class="m2"><p>وین ناله شبها را، روزی اثری باشد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده اگر آبی خواهیم به صد گریه </p></div>
<div class="m2"><p>آبی ندهد ما را، کان بیجگری باشد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما بی‌خبریم از دل، ای باد گذاری کن </p></div>
<div class="m2"><p>بر خاک درش باشد کانجا خبری باشد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که کرا زیبد، چون زلف تو سودایت </p></div>
<div class="m2"><p>آن را که به هر مویی، چون دوش سری باشد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تنها نه منم عاشق، کز خاک سر کویت </p></div>
<div class="m2"><p>هر گرد که بر خیزد، صاحب نظری باشد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من خاکت از آن گشتم امروز که بعد از من </p></div>
<div class="m2"><p>هر ذره‌ای از خاکم، کحل بصری باشد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشتاق حرم را گو: شو محرم میخانه </p></div>
<div class="m2"><p>باشد که ازین خانه، در کعبه دری باشد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون زلف به بالایت، سلمان سر و جان ریزد </p></div>
<div class="m2"><p>گر یک سر مو جان را، پیشت خطری باشد </p></div></div>