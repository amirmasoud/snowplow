---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>چه نویسم که دل از درد فراقت چه کشید؟</p></div>
<div class="m2"><p>یا ز نادیدنت این دیده غم دیده چه دید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به امیدی که رسد در تو دل خام طمع</p></div>
<div class="m2"><p>سالها دیگ هوس پخت و به آخر نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه این دل دیوانه درازست و مپرس:</p></div>
<div class="m2"><p>که در آن سلسله زلف پریشان چه کشید؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قصه راز تو مردیم و نگفتیم به کس</p></div>
<div class="m2"><p>بشنو این قصه که هرگز به جهان کس نشنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق صورت توست آینه و این صورت</p></div>
<div class="m2"><p>هست در چهره آیینه چو خورشید پدید </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر زلف تو مرا توبه ناموس شکست</p></div>
<div class="m2"><p>چشم مست تو مرا پرده سالوس درید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جرعه در دور تو رسمی است که نتوان انداخت</p></div>
<div class="m2"><p>خرقه در عهد تو عیبی است که نتوان پوشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دشمنان گر همه کردند زبان چون شمشیر</p></div>
<div class="m2"><p>نیست ممکن که مرا از تو توانند برید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواست تا شرح فراق تو نویسد سلمان</p></div>
<div class="m2"><p>حال دل در قلم آمد ز قلم خون بچکید</p></div></div>