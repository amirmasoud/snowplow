---
title: >-
    غزل شمارهٔ ۴۷
---
# غزل شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>از بار فراق تو مرا، کار خراب است</p></div>
<div class="m2"><p>دریاب، که کار من از این بار، خراب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرسید، که حال دل بیمار تو چون است؟</p></div>
<div class="m2"><p>چون است مپرسید، که بیمار خراب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی چشم تو با حال من افتد که شب و روز؟</p></div>
<div class="m2"><p>او خفته و مست است و مرا کار خراب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هشیار سری، کز می سودای تو مست، است</p></div>
<div class="m2"><p>آباد دلی، کز غم دلدار خراب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من مستم و فارغ ز غم محتسب امروز</p></div>
<div class="m2"><p>کو نیز چو من، بر سر بازار، خراب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تنها نه منم، مست، ز خمخانه عشقت</p></div>
<div class="m2"><p>کز جرعه جامش، در و دیوار خراب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلمان ز می جام الست، است چنین مست</p></div>
<div class="m2"><p>تا ظن نبری کز خم خمار، خراب است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد چه دهی پند مرا؟ جامی ازین می</p></div>
<div class="m2"><p>درکش که دماغ تو ز پندار خراب است</p></div></div>