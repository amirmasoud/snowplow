---
title: >-
    غزل شمارهٔ ۱۱۸
---
# غزل شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>من امروز، از میی مستم، که در ساغر نمی‌گنجد </p></div>
<div class="m2"><p>چنان شادم، که از شادی، دلم در بر نمی‌گنجد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز سودایت برون کردم، کلاه خواجگی، از سر </p></div>
<div class="m2"><p>به سودایت که این افسر، مرا در سر، نمی‌گنجد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بران بودم که بنویسم، مطول، قصه شوقت </p></div>
<div class="m2"><p>چه بنویسم، که در طومار و در دفتر، نمی‌گنجد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عشق چنبر زلفت، چه باک، از چنبر چرخم </p></div>
<div class="m2"><p>سرم تا دارد این سودا، در آن چنبر، نمی‌گنجد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه شب، دوست می‌گردد، به گرد گوشه دلها </p></div>
<div class="m2"><p>که جز تو در دل تنگم، کسی دیگر، نمی‌گنجد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیثی زان دهن گفتم، رقیبم گفت: زیر لب </p></div>
<div class="m2"><p>برو سلمان، که هیچ اینجا، حکایت در نمی‌گنجد </p></div></div>