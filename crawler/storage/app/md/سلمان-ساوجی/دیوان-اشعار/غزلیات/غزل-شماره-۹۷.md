---
title: >-
    غزل شمارهٔ ۹۷
---
# غزل شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>تیر خدنگ غمزه‌ات، از جان ما گذشت </p></div>
<div class="m2"><p>بر ما ز غمزه تو چه گویم، چه‌ها گذشت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت صباح، بر سر شمع، از ممر باد </p></div>
<div class="m2"><p>نگذشت، آن چه بر سر ما از صبا گذشت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حیرتم، که باد به زلف تو، چون رسید </p></div>
<div class="m2"><p>فی الجمله چون رسید از آنجا چرا گذشت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر ما ز آب دیده شب، دوش تا به روز </p></div>
<div class="m2"><p>باران محتن آمد و سیل بلا گذشت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یارب چه رفت، بر سر ما دوش، کان صنم </p></div>
<div class="m2"><p>بیگانه وش، درآمد و بر آشنا گذشت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چندان گریستیم، که من بعد اگر کسی </p></div>
<div class="m2"><p>آید به سوی ما نتواند ز ما گذشت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلمان دوای درد دل، از کس طلب مکن </p></div>
<div class="m2"><p>با درد خود بساز، که کار از دوا گذشت </p></div></div>