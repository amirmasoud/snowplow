---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>بیمار غمت را به جز از صبر دوا نیست </p></div>
<div class="m2"><p>صبرست دوای من و دردا که مرا نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هیچ طرف راه ندارم که ز زلفت </p></div>
<div class="m2"><p>بر هیچ طرف نیست که دامی ز بلا نیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق است میان دل و جان من و بی‌عشق </p></div>
<div class="m2"><p>حقا که میان دل و جان هیچ صفا نیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد دهدم توبه ز روی تو زهی روی </p></div>
<div class="m2"><p>هیچش ز خدا شرم و ز روی تو حیا نیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهری و وفایی که تو را نیست مرا هست </p></div>
<div class="m2"><p>صبری و قراری که تو را هست مرا نیست </p></div></div>