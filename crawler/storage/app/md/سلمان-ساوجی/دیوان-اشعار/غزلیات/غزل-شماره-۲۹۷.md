---
title: >-
    غزل شمارهٔ ۲۹۷
---
# غزل شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>ما روی دل به خانه خمار کرده‌ایم </p></div>
<div class="m2"><p>محراب جان ز ابروی دلدار کرده‌ایم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهر یک پیاله دردی، هزار بار</p></div>
<div class="m2"><p>خود را گرو به خانه خمار کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر بوی جرعه‌ای که ز جامش به ما رسد </p></div>
<div class="m2"><p>خود را چو خاک بر در او خوار کرده‌ایم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرمست رفته‌ایم و به بازارو جرعه‌وار </p></div>
<div class="m2"><p>جانها نثار بر سر بازار کرده‌ایم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قندیل را شکسته و پیمانه ساخته </p></div>
<div class="m2"><p>تسبیح را گسسته و زنار کرده‌ایم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهاد تکیه بر عمل خویش کرده‌اند </p></div>
<div class="m2"><p>ما اعتماد بر کرم یار کرده‌ایم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صوفی مکن مجادله با ما، که پیش ازین </p></div>
<div class="m2"><p>ما نیز ازین معامله بسیار کرده‌ایم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>امروز با تو نیست سر و کار ما که ما </p></div>
<div class="m2"><p>عمر عزیز بر سر این کار کرده‌ایم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>افکنده‌ایم بار سر از دوش در رهت </p></div>
<div class="m2"><p>خود را بدین طریق سبکبار کرده‌ایم </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای مدعی برندی سلمان چه می‌کنی؟</p></div>
<div class="m2"><p>دعوی که ما به جرم خود اقرار کرده‌ایم </p></div></div>