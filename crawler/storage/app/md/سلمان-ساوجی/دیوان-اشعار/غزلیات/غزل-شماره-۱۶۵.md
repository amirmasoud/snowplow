---
title: >-
    غزل شمارهٔ ۱۶۵
---
# غزل شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>مرا هوای تو از سر بدر نخواهد شد </p></div>
<div class="m2"><p>شمایل تو ز پیش نظر نخواهد شد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر سرم برود گو برو مراد از سر </p></div>
<div class="m2"><p>هوای توست مرا آن ز سر نخواهد شد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم به کوی تو رفت و مقیم شد آنجا </p></div>
<div class="m2"><p>وزان مقام به جایی دگر نخواهد شد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرم برفت به سودای وصل، می‌دانم </p></div>
<div class="m2"><p>که این معامله با او به سر نخواهد شد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان ز چشم تو در خواب مستیم که مرا </p></div>
<div class="m2"><p>ز خواب خوش به قیامت خبر نخواهد شد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به نوک غمزه چون نیشتر بخواهی ریخت </p></div>
<div class="m2"><p>هزار خون که سر نیش‌تر نخواهد شد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خدنگ غمزه‌ات از جان اگر چه می‌گذرد </p></div>
<div class="m2"><p>ولیکن از دل سلمان بدر نخواهد شد </p></div></div>