---
title: >-
    غزل شمارهٔ ۱۲۲
---
# غزل شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>به حضرت تو، که یارد، که قصه‌ای ز من آرد؟</p></div>
<div class="m2"><p>به غیر باد و برآنم که باد، نیز نیارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نسیم نماید، کسالتی به رسالت</p></div>
<div class="m2"><p>سلام من که رساند، پیام من که گزارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نسیمی از سر زلف تو می‌خرم به دو عالم</p></div>
<div class="m2"><p>وگر چه خود همه عالم، نسیم زلف تو دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال روی تو در چشم ما و ما، متحیر</p></div>
<div class="m2"><p>در آن قلم که چنین صورتی بر آب، نگارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لبم چو یاد کند، ذوق خاکبوس درت را</p></div>
<div class="m2"><p>ز شوق مردم چشم من، آب در دهن آرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرم وصال تو بگذاشت پیش از این دو سه روزی</p></div>
<div class="m2"><p>مرا فراق تو دائم که پیش ازین، نگذارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بروز وصل خودم وعده داده بودی، سلمان</p></div>
<div class="m2"><p>درین هوس، همه شبهای تیره روز شمارد</p></div></div>