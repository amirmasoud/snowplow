---
title: >-
    غزل شمارهٔ ۲۰۴
---
# غزل شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>دوشم آن گلچهره در آغوش بود</p></div>
<div class="m2"><p>حبذا وقتی که ما را دوش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب به لب، رخسار بر رخسار بد</p></div>
<div class="m2"><p>رو به رو، آغوش بر آغوش بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه آن جز باده بد، مکروه گشت</p></div>
<div class="m2"><p>آنچه غیر از دوست بد، فرموش بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از می لعل لبش تا صبحدم</p></div>
<div class="m2"><p>بانگ «هایاهای و نوشانوش» بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از نشاط جرعه پیمان ما</p></div>
<div class="m2"><p>عقل و جان سرمست و دل مدهوش بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خروش ما فلک بد در خروش</p></div>
<div class="m2"><p>تا خروس صبحدم خاموش بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهره و خورشید را از رشک ما</p></div>
<div class="m2"><p>بر فلم خون جگر بر جوش بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح ناگه از سر ما برگرفت</p></div>
<div class="m2"><p>پرده پب را که آن سرپوش بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عزم رفتن کرد حالی دلبرم</p></div>
<div class="m2"><p>آن هم از بد گفتن بد گوش بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ریخت سلمان در پیش، از دیدگان</p></div>
<div class="m2"><p>گوهری کز لطف او، در گوش بود</p></div></div>