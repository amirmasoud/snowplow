---
title: >-
    غزل شمارهٔ ۲۵۶
---
# غزل شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>آنکه از جان دوست‌تر می‌دارمش</p></div>
<div class="m2"><p>او مرا بگذاشت، من نگذارمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بدو دادم ز من رنجید و رفت</p></div>
<div class="m2"><p>می‌دهم جان تا مگر باز آرمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه در خون دل من میرود</p></div>
<div class="m2"><p>من چو چشم خویشتن می‌دارمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قالبی بی‌روح دارم می‌برم</p></div>
<div class="m2"><p>تا به خاک کوی او بسپارمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌دهم جان روز و شب در کار دوست</p></div>
<div class="m2"><p>گو مران از پیش اگر در کارمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی در پای تو می‌مالم مرنج</p></div>
<div class="m2"><p>گر به روی سخت می‌آزارمش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه رویش داد بر بادم چو زلف</p></div>
<div class="m2"><p>همچنان جانب نگه می‌دارمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ رحمی نیست بر بیمار خویش</p></div>
<div class="m2"><p>آن طبیبی را که من بیمارمش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گرچه او یار منست من یار او</p></div>
<div class="m2"><p>من نمی‌یارم که گویم یارمش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با دل خود گفتم او را چیستی؟</p></div>
<div class="m2"><p>گفت سلمان او گل و من خارمش</p></div></div>