---
title: >-
    غزل شمارهٔ ۱۰۳
---
# غزل شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>دل، در برم گرفت و پی یار من برفت </p></div>
<div class="m2"><p>لب بوسه داد و جان و روان از بدن برفت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دید دل، که قافله اشک می‌رود </p></div>
<div class="m2"><p>با کاروان روان شد و از چشم من برفت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل شنید ناله من، در فراق یار </p></div>
<div class="m2"><p>مستانه، نعره‌ای زد و از خویشتن برفت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن کس که باز ماند ز جانان برای جان </p></div>
<div class="m2"><p>یوسف گذاشت، در طلب پیرهن برفت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سرو ناز، تا ز چمن سایه برگرفت </p></div>
<div class="m2"><p>بنشست آتش گل و آب سمن برفت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زلف جمع کرد، پراکنده لشگری </p></div>
<div class="m2"><p>آمد، به قصد خونم و در آمدن برفت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشکست، قلب لشکر دلها و درپیش </p></div>
<div class="m2"><p>لشکر برفت و آن بت لشکر شکن برفت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناگفتنی است، راز دهانش ولی، چه سود </p></div>
<div class="m2"><p>خوردن، دریغ بر سخنی کز دهن برفت </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بازا، که عمر جز نفسی نیست و آن نفس </p></div>
<div class="m2"><p>یکبارگی، درآمدن و در شدن برفت </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلمان ز شوق او اگرت جان بشد چه شد </p></div>
<div class="m2"><p>سودای او نرفت ز جان و ز تن برفت </p></div></div>