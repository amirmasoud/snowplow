---
title: >-
    غزل شمارهٔ ۲۴۰
---
# غزل شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>زین پیش داشت یار غم کار و بار یار</p></div>
<div class="m2"><p>آخر فرو گذاشت به یکبار کار یار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عمری گذشت تا سخنم را به هیچ وجه</p></div>
<div class="m2"><p>در خود نداد ره، دهن تنگ بار یار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندانکه می‌روم ز پی یار جز غبار </p></div>
<div class="m2"><p>چیزی نمی‌رسد به من از رهگذار یار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افتاده‌ام به بحری وانگه کدام بحر؟</p></div>
<div class="m2"><p>بحری که نیست ساحل آن جز کنار یار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بار جهان کجا و دل تنگم از کجا؟</p></div>
<div class="m2"><p>جایی است دل که نیست در و غیر بار یار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نگرفته است دامن من هیچ آب و خاک</p></div>
<div class="m2"><p>الا که آب دیده و خاک دیار یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار ار به اختیار تو شد نیک، ور نشد</p></div>
<div class="m2"><p>واجب بود متابعت اختیار یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون غنچه‌ام اگر چه بسی خار در دل است</p></div>
<div class="m2"><p>من دل خوشم به بوی نسیم بهار یار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبل گذاشت شاخ سمن، میل خار کرد</p></div>
<div class="m2"><p>یعنی که خوشتر از گل اغیار خار یار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سلمان! تو چند دعوی یار کنی که خود</p></div>
<div class="m2"><p>پیداست بر محک محبت عیار یار؟</p></div></div>