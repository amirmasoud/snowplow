---
title: >-
    غزل شمارهٔ ۳۴۸
---
# غزل شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>در خیل تو گشتیم، بسی از همه بابی </p></div>
<div class="m2"><p>کردیم سوال و نشنیدیم جوابی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوردیم بسی خون و ندیدیم کسی را </p></div>
<div class="m2"><p>جز دیده که ما را مددی کرد به آبی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نگذرم از خاک درت خاک من اینجاست </p></div>
<div class="m2"><p>ای عمر تو بگذر اگرت هست شتابی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در شرح فراقت چه نویسم که نگنجد </p></div>
<div class="m2"><p>شرح غم هجران تو در هیچ کتابی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خواب خیال تو هوس دارم و کو خواب </p></div>
<div class="m2"><p>ای بخت شبی بخش بدین یکدمه خوابی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان خواست که در لطف به شکل تو بر آید </p></div>
<div class="m2"><p>هم رنگی طاوس هوس کرد غرابی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دی مدعیی دعوت من که سلمان </p></div>
<div class="m2"><p>تا کی ز خرابات چه آید ز خرابی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آمد به سرم عشق که مشنو سخن او </p></div>
<div class="m2"><p>تو روی به ما کرده‌ای او روی به آبی </p></div></div>