---
title: >-
    غزل شمارهٔ ۳۸
---
# غزل شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>من خراباتیم و باده پرست</p></div>
<div class="m2"><p>در خرابات مغان، عاشق و مست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوش، بر زمزمه قول بلی </p></div>
<div class="m2"><p>هوش، غارت زده جام الست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کشندم چون سبو، دوش به دوش </p></div>
<div class="m2"><p>می‌دهندم چو قدح، دست به دست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدی آن توبه سنگین مرا؟</p></div>
<div class="m2"><p>که به یک شیشه می چون بشکست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رندی و عاشقی و قلاشی</p></div>
<div class="m2"><p>هیچ شک نیست که در ما همه هست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما همان خاک در مصطبه‌ایم </p></div>
<div class="m2"><p>معنی و صورت ما عالی و پست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن زمان نیز که گردیم غبار </p></div>
<div class="m2"><p>بر در میکده خواهیم نشست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همه ذرات جهان می‌بینیم </p></div>
<div class="m2"><p>به هوایت شده خورشید پرست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بود در بند تعلق، سلمان </p></div>
<div class="m2"><p>به کمند تو در افتاد و برست </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ذره‌ای بود و به خورشید رسید </p></div>
<div class="m2"><p>قطره‌ای بود و به دریا پیوست </p></div></div>