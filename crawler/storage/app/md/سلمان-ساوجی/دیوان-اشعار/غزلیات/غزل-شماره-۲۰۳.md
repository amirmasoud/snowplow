---
title: >-
    غزل شمارهٔ ۲۰۳
---
# غزل شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>آنجا که عشق آمد کجا پند و خرد را جا بود؟</p></div>
<div class="m2"><p>در معرض خورشید، کی نور سها پیدا بود؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندیست کار بیدلان، تقوی شعار زاهدان</p></div>
<div class="m2"><p>آری دلا هر کسوتی، بر قامتی زیبا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکس که آرد در نظر، روی چنان و همچنان</p></div>
<div class="m2"><p>عقلش بود بر جا عجب گر عقل او بر جا بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من در شب سودای او، دل خوش به فردا می‌کنم</p></div>
<div class="m2"><p>لیکن شب سودای او ترسم که بی فردا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه سخن راندم بلند، از وصف قدش قاصرم</p></div>
<div class="m2"><p>هر چیز کاید در نظر، قدش از آن بالا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که بالای خوشت، اما بلایی می‌دهد</p></div>
<div class="m2"><p>گفتی: بلی در راه ما، این باشد و آنها بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>او ریخت خون چشم من، دامن گرفت از خون مرا</p></div>
<div class="m2"><p>او می‌کند بر ما ستم، لیکن گناه از ما بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تابی ز شمع روی او، گر در تو گیرد مدعی!</p></div>
<div class="m2"><p>آنگه بدانی کزچه رو پروانه نا پروا بود؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در آب می‌جستم تو را دل گفت: کای سلمان بیا!</p></div>
<div class="m2"><p>در بحر عشقش غوص کن، کان در درین دریا بود</p></div></div>