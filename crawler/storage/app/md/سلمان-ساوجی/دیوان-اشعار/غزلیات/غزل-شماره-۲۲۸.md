---
title: >-
    غزل شمارهٔ ۲۲۸
---
# غزل شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>چون خاک شوم وز گل من خار برآید </p></div>
<div class="m2"><p>زان خار ببوی تو همه گل ببر آید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از عمر بسی رفت و ندانم که چه باقی است </p></div>
<div class="m2"><p>وین نیز به هر نوع که باشد به سر آید </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که ز خاک سر کوی تو کنم یاد </p></div>
<div class="m2"><p>زان خاک همه خون دل و دیده برآید </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر خاک سر کوی تو چون مشک ببویند </p></div>
<div class="m2"><p>زان خاک معطر همه بوی جگر آید </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیوسته جمال تو بود در نظر من </p></div>
<div class="m2"><p>خود غیر جمال تو مرا در نظر آید </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار من سودا زده عشق است و ز سلمان </p></div>
<div class="m2"><p>جز عشق مپندار که کاری دگر آید </p></div></div>