---
title: >-
    غزل شمارهٔ ۲۶۷
---
# غزل شمارهٔ ۲۶۷

<div class="b" id="bn1"><div class="m1"><p>ای جان نازنین من ای آرزوی دل</p></div>
<div class="m2"><p>میل من است سوی تو میل تو سوی دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آرزوی روی تو دل جان همی دهد</p></div>
<div class="m2"><p>وا حسرتا! اگر ندهی آرزوی دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون غنچه بسته‌ام سر دل را به صد گره</p></div>
<div class="m2"><p>تا بوی راز عشق تو آید ز بوی دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان را به یاد تو به صبا می‌دهم که او</p></div>
<div class="m2"><p>می‌آورد ز سنبل زلف تو بوی دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دیده دید روی تو را روی دل ندید</p></div>
<div class="m2"><p>با روی دوست خود نتوان دید روی دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگر به دیده دل ندهم من کز آب چشم</p></div>
<div class="m2"><p>هر بار خود درست نیاید سبوی دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلمان اگر ز اهل دلی نام دل مبر</p></div>
<div class="m2"><p>جان دادن است کار تو بی‌گفتگوی دل</p></div></div>