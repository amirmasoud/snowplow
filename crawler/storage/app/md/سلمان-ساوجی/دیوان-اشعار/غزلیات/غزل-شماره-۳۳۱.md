---
title: >-
    غزل شمارهٔ ۳۳۱
---
# غزل شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>ای سر سودای من رفته در سودای تو </p></div>
<div class="m2"><p>باد سر تا پای من برخی ز سر تا پای تو </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر من رفت در سودای عشقت گو: برو </p></div>
<div class="m2"><p>بر سرم پاینده بادا سایه بالای تو </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای سروت در میان جویبار چشم ماست </p></div>
<div class="m2"><p>گرچه ماییم از میان جان و دل جویای تو </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نبینم مردم چشم جهان بین را رواست </p></div>
<div class="m2"><p>خود کسی را کی توانم دید من بر جای تو </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو لافی می‌زند یعنی که بالای توام </p></div>
<div class="m2"><p>سرو بی‌برگی است باری تا تو بود بالای تو </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ترکت ترکتاز و حاجبش پیشانی است </p></div>
<div class="m2"><p>چون در آید کس به چشم تنگ ترک آسای تو </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رای من جز بندگی سرو آزاد تو نیست </p></div>
<div class="m2"><p>بس بلند افتاد سلمان راستی رارای تو </p></div></div>