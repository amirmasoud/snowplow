---
title: >-
    غزل شمارهٔ ۲۰۷
---
# غزل شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>دی دیده از خیال رخش بازمانده بود</p></div>
<div class="m2"><p>گلگون اشک در طبلش گرم رانده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>افتاده بود دل به خم چین زلف او</p></div>
<div class="m2"><p>شب بود و ره دراز هم آنجا بمانده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل رفته بود و ما پی دل تا بکوی دوست</p></div>
<div class="m2"><p>بردیم از آنکه او همه ره خون فشانده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل دیده خواست تا ببرد، خون گرفته بود</p></div>
<div class="m2"><p>جان خواست خواستم بدهم، غم ستانده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌خواستم که عمر عزیزت کنم نثار</p></div>
<div class="m2"><p>نقدی عزیز بود ولیکن نمانده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خطا شده ز خال سیاه مبارکش</p></div>
<div class="m2"><p>کش نیش لب طره سلمان نشانده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خالش به جای خویش گرفتم، نشسته بود</p></div>
<div class="m2"><p>بیگانه خط نامه سیه را که خوانده بود</p></div></div>