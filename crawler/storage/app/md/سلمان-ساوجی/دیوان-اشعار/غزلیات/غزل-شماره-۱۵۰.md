---
title: >-
    غزل شمارهٔ ۱۵۰
---
# غزل شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>دلی که شیفته یار دلربا باشد</p></div>
<div class="m2"><p>همیشه زار و پریشان و مبتلا باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلی عجب نبود گر بود پریشان حال</p></div>
<div class="m2"><p>گدا که در طلب وصل پادشا باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهانه تو رقیب است و نیست این مسموع</p></div>
<div class="m2"><p>رقیب را چه محل گر تو را رضا باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جفای دشمن و جور رقیب و طعنه خلق</p></div>
<div class="m2"><p>خوش است بر دل اگر دوست را وفا باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو را گذری بر من غریب افتد</p></div>
<div class="m2"><p>و یا تو را نظری بر من گدا باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از آن طرف نپذیرد کمال او نقصان</p></div>
<div class="m2"><p>وزین طرف شرف روزگار ما باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فگار گشت به خون جگر دل سلمان</p></div>
<div class="m2"><p>بترس از آنکه بد و نیک را جزا باشد</p></div></div>