---
title: >-
    غزل شمارهٔ ۲۷۳
---
# غزل شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>بر زلف تو من بار دگر توبه شکستم </p></div>
<div class="m2"><p>بس عهد که چون زلف تو بشکستم و بستم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب که زد کار جهانی همه بر هم </p></div>
<div class="m2"><p>چشم تو و عذرش همه این است که مستم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نامه چو من شرح فراق تو نویسم </p></div>
<div class="m2"><p>خون گرید و فریاد کند خامه ز دستم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید بلندی تو و من پست چو سایه </p></div>
<div class="m2"><p>آنجا که تو باشی نتوان گفت که هستم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم تو به دل گفت که مست منی ای دل </p></div>
<div class="m2"><p>دل گفت: بلی مست تو از روز الستم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنجی است روان جام می و توبه طلسمش </p></div>
<div class="m2"><p>برداشتم آن گنج و طلسمش بشکستم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سوختن و مردن من شمع شب افروز </p></div>
<div class="m2"><p>خندید بسی امشب و من می‌نگریستم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روزش به سر آمد سحری گفت که سلمان </p></div>
<div class="m2"><p>برخیز که من نیز به روز تو نشستم </p></div></div>