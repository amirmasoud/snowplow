---
title: >-
    غزل شمارهٔ ۲۸۷
---
# غزل شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>کمترین صید سر زلف کمند تو منم </p></div>
<div class="m2"><p>چون تو ای دوست به هیچم نگرفتی چه کنم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در درونم به جز از دوست دگر چیزی نیست </p></div>
<div class="m2"><p>یوسفم دوست من آلوده به خون پیرهنم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درگذشت از سر من آب ولی گر دهدم </p></div>
<div class="m2"><p>آشنایی مددی دستی و پایی بزنم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان چه دارد که نثار ره جانان سازم؟</p></div>
<div class="m2"><p>یا که سر چیست که در پای عزیزش فکنم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خیال تو نگردد دگری در نظرم </p></div>
<div class="m2"><p>جز حدیث تو نیاید سخنی در دهنم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور سودای من و تلخی عیشم بگذار </p></div>
<div class="m2"><p>بنگر ای خسرو خوبان که چه شیرین سخنم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوت کندن سنگ ارچه چو فرهادم نیست </p></div>
<div class="m2"><p>سنگ جانم روم القصه و جانی بکنم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ساقیا باده، که من بر سر پیمان توام </p></div>
<div class="m2"><p>در من این نیست که پیمانه و پیمان شکنم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطربا راه برون شد بنما، سلمان را </p></div>
<div class="m2"><p>به در دوست که من گمشده در خویشتنم </p></div></div>