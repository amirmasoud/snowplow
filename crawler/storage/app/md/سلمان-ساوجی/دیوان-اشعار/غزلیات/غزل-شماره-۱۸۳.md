---
title: >-
    غزل شمارهٔ ۱۸۳
---
# غزل شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>زلف و رخسار تو را شام و سحر چون خواند؟</p></div>
<div class="m2"><p>هر که یک حرف سیاهی ز سپیدی داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌کنم ترک هوای سر زلف تو و باز</p></div>
<div class="m2"><p>باد می‌آید و این سلسله می‌جنباند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اشک من آنچه ز زار دل من می‌گوید</p></div>
<div class="m2"><p>راست می‌گوید و از دیده سخن می‌راند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به او دادم و او کرد به جانم بیداد</p></div>
<div class="m2"><p>هیچکس نیست که داد من از او بستاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب چشمم ننشاند آتش و من می‌دانم</p></div>
<div class="m2"><p>کاتش من به جز از خاک درش ننشاند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه گوید ز لبش جان، همه شیرین گوید</p></div>
<div class="m2"><p>و آنچه داند ز رخش دل، همه نیکو داند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماند سلمان ز درت دور و چنان می‌شنود:</p></div>
<div class="m2"><p>که مراد تو چنین است و بدین می‌ماند</p></div></div>