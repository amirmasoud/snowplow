---
title: >-
    غزل شمارهٔ ۲۲۴
---
# غزل شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>مرا از آینهٔ سخت روی سخت آید</p></div>
<div class="m2"><p>که در برابر روی تو روی بنماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو شانه دست به دندان اگر برم شاید</p></div>
<div class="m2"><p>که شانه در سر زلف تو دست می‌ساید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لطیفه‌ایست دهان تو تا که دریابد</p></div>
<div class="m2"><p>دقیقه‌ایست میان تو تا که بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عروس گل ز جمال تو چون خجل نشود</p></div>
<div class="m2"><p>سپیده دم که به گلگونه رخ بیاراید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر مراز سعادت به دولت عشقت</p></div>
<div class="m2"><p>جز آستان درت هیچ در نمی‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عروس خاطر سلمان که با لبت پیوند</p></div>
<div class="m2"><p>کند هر آیینه زین گونه گوهری زاید</p></div></div>