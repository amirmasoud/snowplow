---
title: >-
    غزل شمارهٔ ۳۷۴
---
# غزل شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>هر که از روی تواضع بنهد پیشانی </p></div>
<div class="m2"><p>پیش روی تو زهی روی و زهی پیشانی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه خواهند تو را، تا تو کرا می‌خواهی؟</p></div>
<div class="m2"><p>همه خوانند تو را، تا تو کرا می‌خوانی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان غمت یاد نیاید که منم در غم تو </p></div>
<div class="m2"><p>زان عزیزست مرا جان که تو هم در جانی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر مگردان ز من آخر که همه عمر عزیز </p></div>
<div class="m2"><p>خود به پایان نتوان برد به سرگردانی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفت در حلقه زلف تو به مویی صد دل </p></div>
<div class="m2"><p>دل به خود رفت از آنست بدین ارزانی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا نوبت آنست که از دست خودم </p></div>
<div class="m2"><p>بدهی جامی و از دست خودم بستانی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت: درد دل خود می‌طلبم چون طلبم؟</p></div>
<div class="m2"><p>که دلم با تو و من بیخودم از حیرانی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد پایان سخن را تو سواری سلمان </p></div>
<div class="m2"><p>آفرین بر سخنت باد، که خوش می‌رانی</p></div></div>