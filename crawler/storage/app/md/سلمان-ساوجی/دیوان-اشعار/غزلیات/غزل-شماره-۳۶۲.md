---
title: >-
    غزل شمارهٔ ۳۶۲
---
# غزل شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>رفتی از دست من ای یار و نه آن شهبازی </p></div>
<div class="m2"><p>که بدست آورمت، باز به بازی بازی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تو چون آب من ای سرو روان می‌باشم </p></div>
<div class="m2"><p>چه شود سایه اگر بر سر من اندازی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه آنی همه حسنی همه لطفی همه ناز </p></div>
<div class="m2"><p>به چنان حسن و لطافت رسدت گر نازی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و جان دادم و سر نیز فدا می‌کنمت </p></div>
<div class="m2"><p>چون کنم چون تو بدین هیچ نمی‌پردازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای کار تو می‌سازم اگر خواهی ساخت </p></div>
<div class="m2"><p>ز انتظارم به چه می‌سوزی و کی‌ می‌یازی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوخت چون عود مرا عشق و بران می‌پوشم </p></div>
<div class="m2"><p>دامن از دود درونم نکند غمازی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرده گل ز هوا می‌درد و کی ماند </p></div>
<div class="m2"><p>غنچه مستور که با باد کند همرازی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>درم خالص قلبم نکند میل خلاص </p></div>
<div class="m2"><p>گر تو در بوته غم دم به دمش بگدازی </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده بردار ز رخ تا پس ازین بر سلمان </p></div>
<div class="m2"><p>زاهد پرده نشین را نرسد طنازی </p></div></div>