---
title: >-
    غزل شمارهٔ ۴۱
---
# غزل شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>گر بدین شیوه کند، چشم تو مردم را مست </p></div>
<div class="m2"><p>نتوان گفت، که در دور تو، هشیاری هست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوردم از دست تو جامی، که جهان جرعه اوست </p></div>
<div class="m2"><p>هرکه زین دست خورد می، برود زود از دست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دارم از بهر دوای غم دل، می، برکف </p></div>
<div class="m2"><p>این دوایی است، که بی وصل تو دارم در دست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌زند، حلقه زلف تو در غارت جان </p></div>
<div class="m2"><p>نتوان، با سر زلف تو، به جانی در بست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می، به هشیار ده ای ساقی مجلس، که مرا </p></div>
<div class="m2"><p>نشئه‌ای هست هنوز، از می باقی الست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من که صد سلسله از دست غمت، می‌گسلم </p></div>
<div class="m2"><p>یک سر مو نتوانم، ز دو زلف تو گسست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که پیوست به وصلت، ز همه باز برید </p></div>
<div class="m2"><p>وانکه شد صید کمندت، ز همه قید برست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان صوفی نشد، از دود کدورت، صافی</p></div>
<div class="m2"><p>نا نشد در بن خمخانه، چو دردی بنشست </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>با سر زلف تو سودای من، امروزی نیست </p></div>
<div class="m2"><p>ما نبودیم که این سلسله در هم پیوست </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جست، سلمان ز جهان بهر میان تو کنار</p></div>
<div class="m2"><p>راستی آنکه ازین ورطه، به یک موی بجست </p></div></div>