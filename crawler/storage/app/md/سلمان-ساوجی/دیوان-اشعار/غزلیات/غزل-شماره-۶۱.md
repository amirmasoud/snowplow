---
title: >-
    غزل شمارهٔ ۶۱
---
# غزل شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>تا در سرم، ز زلف تو، سودا فتاده است</p></div>
<div class="m2"><p>کارم ز دست رفته و در پا، فتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌اتفاق صحبت و بی‌اختیار هجر</p></div>
<div class="m2"><p>مشکل حکایتی است که ما را فتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون شمع، می‌گدازم و روشن نمی‌شود</p></div>
<div class="m2"><p>کین خود، چه آتشی است که در ما فتاده است؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر افتدت هوس، که بپرسی، دل مرا</p></div>
<div class="m2"><p>در زلف خود بجو، که هم آنجا فتاده است</p></div></div>