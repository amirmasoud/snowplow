---
title: >-
    غزل شمارهٔ ۲۶۴
---
# غزل شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>ای به دیدار توام، دیده گریان مشتاق!</p></div>
<div class="m2"><p>ز اشتیاق لب لعلت، به لبم جان مشتاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به سوز تو چو پروانه به آتش مایل</p></div>
<div class="m2"><p>جان به درد تو چو بیمار به درمان مشتاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان محبوس تن من به تمنای رخت</p></div>
<div class="m2"><p>عندلیبی است مقفس به گلستان مشتاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بود سبزه پژمرده به باران مشتاق</p></div>
<div class="m2"><p>بیش از آنم من مهجور، به جانان مشتاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خسروا بنده به بوسیدن خاک در تو</p></div>
<div class="m2"><p>چون سکندر به لب چشمه حیوان مشتاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هوای دل ما، حسن رخ خوبان است</p></div>
<div class="m2"><p>چون به انفاس صبا، لاله و ریحان مشتاق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تشنه بادیه چون است به زمزم مایل</p></div>
<div class="m2"><p>بیش از آنست به دیدار تو سلمان مشتاق</p></div></div>