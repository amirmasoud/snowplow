---
title: >-
    غزل شمارهٔ ۲۷۱
---
# غزل شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>من هر چه دیده‌ام ز دل و دیده دیده‌ام </p></div>
<div class="m2"><p>گاهی ز دل بود گله، گاهی ز دیده‌ام </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من هر چه دیده‌ام ز دل و دیده‌ام کنون </p></div>
<div class="m2"><p>از دل ندیده‌ام همه از دیده دیده‌ام </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه دهن دریده مرا فاش کرد راز </p></div>
<div class="m2"><p>او را گناه نیست، منش برکشیده‌ام </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول کسی که ریخته است آب روی من </p></div>
<div class="m2"><p>اشک است کش به خون جگر پروریده‌ام </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری بدان امید که روزی رسم به کام </p></div>
<div class="m2"><p>سودای خام پخته‌ام و نا رسیده‌ام </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا مهر ماه چهره تو در دلم نشست </p></div>
<div class="m2"><p>از مهر و ماه مهر بکلی بریده‌ام </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشقت به جان خریدم و قصدم به جان کند </p></div>
<div class="m2"><p>بر جان خویش دشمن جان را گزیده‌ام </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بازا که در غم تو به بازار عاشقان </p></div>
<div class="m2"><p>جان را بداده و غم عشقت خریده‌ام </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شیدا صفت شراب غمت خورده‌ام بسی </p></div>
<div class="m2"><p>لیکن ز باغ وصل تو یک گل نچیده‌ام </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گویند بوی زلف تو جان تازه می‌کند </p></div>
<div class="m2"><p>سلمان قبول کن که من از جان شنیده‌ام </p></div></div>