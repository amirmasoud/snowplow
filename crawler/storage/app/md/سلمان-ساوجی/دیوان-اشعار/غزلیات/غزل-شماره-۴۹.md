---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>تا به هوای تو دل، از سر جان، برنخاست</p></div>
<div class="m2"><p>از دل بی‌طاقتم، بار گران ، برنخاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق تو تا جان و دل، خواست، که یغما کند</p></div>
<div class="m2"><p>تا جگرم خون نکرد، از سر آن، برنخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دل نازک تو را، بود غباری، ز من</p></div>
<div class="m2"><p>تا نشدم خاک ره، آن زمیان، برنخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو نخوانم تورا، کز لب جوی بهشت</p></div>
<div class="m2"><p>چون قد زیبای تو، سرو روان برنخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف پریشان تو، باد به هم برزند</p></div>
<div class="m2"><p>کز دل سودا زده، آه و فغان بر نخاست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیش به تیغ ستم، خون غریبان، مریز</p></div>
<div class="m2"><p>ظلم مکن در جهان، امن و امان برنخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پرتو مهر تو تا، بر دل سلمان، بتافت</p></div>
<div class="m2"><p>ذره صفت از هوا، رقص کنان، برنخاست</p></div></div>