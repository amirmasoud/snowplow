---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>دل می‌خرد حبیب و مرا این متاع نیست </p></div>
<div class="m2"><p>گر طالب سرست برین سر، نزاع نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کاری است عشق مشکل و حالی است بس غریب </p></div>
<div class="m2"><p>کس را به هیچ حال بران، اطلاع نیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دنیا خرند اهل مروت به هیچ وجه </p></div>
<div class="m2"><p>ارباب عشق را هوس این متاع نیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عاشقی دلا ز ملامت مشو ملول </p></div>
<div class="m2"><p>کاحوال خستگاه هوا جز صراع نیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سر ز استماع الست است مستیی </p></div>
<div class="m2"><p>ما را که احتیاج شراب و سماع نیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون زلف اگر به تیغ سرم قطع می‌کنی </p></div>
<div class="m2"><p>ما را به مویی، از تو، سر انقطاع نیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ آتشی به حرقت فرقت نمی‌رسد </p></div>
<div class="m2"><p>وان نیز دیده‌ام به سوز و وداع نیست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سلمان امید مهر از آن ماهرو مدار </p></div>
<div class="m2"><p>زیرا میان این مه و مهر اجتماع نیست </p></div></div>