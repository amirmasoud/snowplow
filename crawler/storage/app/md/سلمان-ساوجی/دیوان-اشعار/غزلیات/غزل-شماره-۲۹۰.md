---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>تو می‌روی و من خسته باز می‌مانم </p></div>
<div class="m2"><p>چگونه بی تو بمانم، عجب همی مانم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو باد پای عزیمت، چو باد می‌رانی </p></div>
<div class="m2"><p>من آب دیده گلگون چو آب می‌رانم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو آفتاب منیزی که می‌روی ز سرم </p></div>
<div class="m2"><p>فتاده بر سر ره من به سایه می‌مانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکسته بسته زلف توام روا داری </p></div>
<div class="m2"><p>فرو گذاشتن آخر چنین پریشانم؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدست لطف عنان را کشیده‌دار که من </p></div>
<div class="m2"><p>ز پای بوس رکاب تو باز می‌مانم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه پای عزم و نه جای نشست در منزل </p></div>
<div class="m2"><p>بمانده‌ام ره بیرون شدن نمی‌دانم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریغ روز جوانی که می‌رود عمرم </p></div>
<div class="m2"><p>فسوس عمر گرامی که می‌رود جانم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو آن نه‌ای که کنی گاگاه سلمان را </p></div>
<div class="m2"><p>به نامه یاد و من این نانوشته می‌خوانم </p></div></div>