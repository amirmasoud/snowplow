---
title: >-
    غزل شمارهٔ ۳۳۰
---
# غزل شمارهٔ ۳۳۰

<div class="b" id="bn1"><div class="m1"><p>باز می‌افکند آن زلف کمند افکن او</p></div>
<div class="m2"><p>کار آشفته ما را همه در گردن او </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مکش ای باد صبا دامن گل را که نهاد </p></div>
<div class="m2"><p>کار خود بلبل سودا زده بر دامن او </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آتش عارض او از دل ماهر دودی </p></div>
<div class="m2"><p>که برآورد بر آمد همه پیرامن او </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اینکه مویی شده‌ام در غم آن موی میان </p></div>
<div class="m2"><p>کاج ( کاش ) مویی شدمی همچو میان بر تن او </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه کنم حال درون عرض که حال دل من </p></div>
<div class="m2"><p>می‌نماید رخ چون آینه روشن او </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آهن سرد چه کوبم؟ که دم آتشیم </p></div>
<div class="m2"><p>نکند هیچ اثر در دل چون آهن او </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز بر هم زده‌ای زلف به هم برزده‌ای </p></div>
<div class="m2"><p>که رباید دل مسکین من و مسکن او </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رحم کن بر دل سلمان که به تنگ آمده‌اند </p></div>
<div class="m2"><p>مردم از شیوه چشم تو و از شیون او </p></div></div>