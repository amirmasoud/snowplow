---
title: >-
    غزل شمارهٔ ۱۷۵
---
# غزل شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>غوغای عشق دوشم، ناگاه بر سر آمد </p></div>
<div class="m2"><p>هم دل به غم فرو شد، هم جان به هم برآمد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر روی اهل عالم، بودیم بسته محکم </p></div>
<div class="m2"><p>درهای دل ندانم، عشق از کجا درآمد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از زلف او کشیده راهیست در دل من </p></div>
<div class="m2"><p>وز دل دریست تا جان، عشقش از آن درآمد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یار آشناست اما نشناخت هر کس او را </p></div>
<div class="m2"><p>زیرا که هر زمانی، بر شکل دیگر آمد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردانه رو به کویش ای دل که رفت دیده </p></div>
<div class="m2"><p>در خون خود چو پیشش، با دامن تر آمد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درویش بر درش رو، کانکس که بر در او </p></div>
<div class="m2"><p>درویش رفت ازین جا، آنجا توانگر آمد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل با سر دو زلفش، زین پیش داشت کاری </p></div>
<div class="m2"><p>بگذشته بود از آن سر، امروز با سر آمد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ماجرای اشکم، مطرب ترانه سازد </p></div>
<div class="m2"><p>بس قطره‌های خونین، کز چشم ساغر آمد </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس که مرد، روزی دربند زلف و عشقت </p></div>
<div class="m2"><p>از خاک او نسیمی کامد، معنبر آمد </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیمار توست سلمان، وانگه خوش آن مریضی </p></div>
<div class="m2"><p>کز آستانت او را، بالین و بستر آمد </p></div></div>