---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>صفت خرابی دل، به حدیث کی درآید؟</p></div>
<div class="m2"><p>سخن درون عاشق، به زبان کجا برآید؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو قلم بدست گیرم که حکایتت نویسم </p></div>
<div class="m2"><p>سخنم رسد به پایان و قلم به سر درآید </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر من فدای زلفت، که ز خاک کشتگانش </p></div>
<div class="m2"><p>همه گرد مشک خیزد، همه بوی عنبر آید </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به تصور خیالت، نرود به خواب چشمم </p></div>
<div class="m2"><p>که به چشم من خیال تو ز خواب خوشتر آید </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به قلندری ملامت، چه کنی من گدارا؟</p></div>
<div class="m2"><p>که سکندر ار بکوی تو رسد قلندر آید </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگرم به لب رسد جان، به خدا که نیست ممکن</p></div>
<div class="m2"><p>که به جز خیال رویت، دگریم بر سر آید </p></div></div>