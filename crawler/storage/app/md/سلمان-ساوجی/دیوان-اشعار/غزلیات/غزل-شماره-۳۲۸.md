---
title: >-
    غزل شمارهٔ ۳۲۸
---
# غزل شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>با آنکه آبم برده‌ای، یکباره دست از ما مشو</p></div>
<div class="m2"><p>باشد که یکبار دگر، باز آید آب ما به جو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی به بوی عنبرین زنجیر زلف سر کشت؟</p></div>
<div class="m2"><p>آشفته پویم در به در دیوانه گردم کو به کو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من مست ورندو عاشقم، وز زهد و تقوی فارغم</p></div>
<div class="m2"><p>بد گوی را در حق من، گوهر چه می‌خواهی بگو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای در خم چوگان تو، گوی دل صاحبدلان</p></div>
<div class="m2"><p>دل گوی می‌گردد ترا میلی اگر داری بگو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از موی فرقت تا میان، فرقی نباشد در میان</p></div>
<div class="m2"><p>باریک بینی هردو را، چون باز بینی مو به مو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با سرو کردم نسبتت، گفتی که ای کوته نظر</p></div>
<div class="m2"><p>گر راست می‌گویی چو من، رو در چمن سروی بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شانه شکسته بسته از زلف حکایت می‌کند</p></div>
<div class="m2"><p>آیینه را بردار تا روشن بگوید روبرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شمع زبان آور شبی از سر گرفت افسانه‌ام</p></div>
<div class="m2"><p>دودش بر سر رفت از آن اشکش ازو آمد فرو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سلمان حریف یار شد وز غیر او بیزار شد</p></div>
<div class="m2"><p>یکدم رها کن مدعی، او را به ما ما را به او</p></div></div>