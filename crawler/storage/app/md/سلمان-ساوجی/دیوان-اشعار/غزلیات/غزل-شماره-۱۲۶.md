---
title: >-
    غزل شمارهٔ ۱۲۶
---
# غزل شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>جان زندگی از چشمه پرنوش تو دارد </p></div>
<div class="m2"><p>دل، بستگی از سنبل خاموش تو دارد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دانه و دام دل ما، حلقه کویت </p></div>
<div class="m2"><p>باز آی که دل، منتظر گوش تو دارد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوشت، همه قصد طرف خاطر ما بود </p></div>
<div class="m2"><p>امشب سر زلفت، طرف دوش تو دارد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رنگی که سمن یابد، از اقدام تو یابد </p></div>
<div class="m2"><p>بویی که صبا دارد، از آغوش تو دارد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شرح پراکندگی ماست، وگرنه </p></div>
<div class="m2"><p>زلف این همه سر، بهر چه در دوش تو دارد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از نیش، نیندیشد و از زهر، نترسد </p></div>
<div class="m2"><p>هر کس که هوای لب چون نوش تو دارد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این جوشش خون جگر و غلغل سلمان </p></div>
<div class="m2"><p>زان است که دیگ هوسش، جوش تو دارد </p></div></div>