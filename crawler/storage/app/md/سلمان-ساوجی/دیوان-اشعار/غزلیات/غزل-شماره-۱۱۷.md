---
title: >-
    غزل شمارهٔ ۱۱۷
---
# غزل شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>نه تنها، بر سر کوی تو ما را، کار، می‌افتد </p></div>
<div class="m2"><p>که هر روی در آن منزل، ازین، صد بار می‌افتد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بویت باد شبگیری، چنان مست است، در بستان </p></div>
<div class="m2"><p>که چون زلفت ز مستی، بر گل و گلزار، می‌افتد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خون مردم چشمم، شماتت کم کن، ای دشمن </p></div>
<div class="m2"><p>چه شاید کرد، مردم را ازین، بسیار می‌افتد </p></div></div>