---
title: >-
    غزل شمارهٔ ۳۷۰
---
# غزل شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>سوز تو کجا گیرد، در خرمن هر خامی؟</p></div>
<div class="m2"><p>مرغ تو فرو ناید، ای دوست به هر بامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرد ره سودایت، صاحب قدمی باید </p></div>
<div class="m2"><p>کان بادیه را نتوان، پیمود به هر گامی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بد نام ابد کردم، خود را و نمی‌دانم </p></div>
<div class="m2"><p>درنامه اهل دل، نیکوتر ازین نامی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عشق تو زاهد را دم گرم نخواهد شد </p></div>
<div class="m2"><p>زیرا که بدان آتش هرگز نرسد خامی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیوانه دلی دارم، کارام نمی‌گیرد </p></div>
<div class="m2"><p>جز بر در خماری، یا پیش دلارامی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از تو نظری سلمان، می‌دارد و می‌شاید </p></div>
<div class="m2"><p>درویشی اگر خواهد، از پادشه انعامی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب را به سخن بگشا، زیرا که ندارد دل </p></div>
<div class="m2"><p>غیر از دهنت کامی، و آنگاه چه خوش کامی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آغاز غمت کردم، تا چون بود انجامش </p></div>
<div class="m2"><p>این نیست از آن کاری، کان را بود انجامی </p></div></div>