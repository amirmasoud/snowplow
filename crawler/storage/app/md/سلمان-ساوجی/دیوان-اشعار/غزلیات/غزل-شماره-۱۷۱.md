---
title: >-
    غزل شمارهٔ ۱۷۱
---
# غزل شمارهٔ ۱۷۱

<div class="b" id="bn1"><div class="m1"><p>نظری کن که دل از جور فراقت خون شد </p></div>
<div class="m2"><p>نیست دل را به جز از دیده ره بیرون شد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناتوان بود دل خسته ندانم چون رفت؟</p></div>
<div class="m2"><p>حال آن خسته بدانید که آخر چون شد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا شدم دور ز خورشید جمالت، چو هلال </p></div>
<div class="m2"><p>اثر مهر توام روز به روز افزون شد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوای گل رخسار تو ای گلبن حسن </p></div>
<div class="m2"><p>ای بسا رخ که درین باغ به خون گلگون شد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غنچه را پیش دهان تو صبا خندان یافت </p></div>
<div class="m2"><p>آنچنان بر دهنش زد که دهن پر خون شد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت حسن تو زد عکس تجلی بر دل </p></div>
<div class="m2"><p>نقش خود در آیینه بر او مفتون شد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار برعکس فتاد آیینه و لیلی را </p></div>
<div class="m2"><p>آیینه لیلی و لیلی همگی مجنون شد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش ازین صورت گل با تو تعلق سلمان </p></div>
<div class="m2"><p>بیش ازین داشت، تصور نکنی اکنون شد </p></div></div>