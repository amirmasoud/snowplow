---
title: >-
    غزل شمارهٔ ۲۲۱
---
# غزل شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>ما رقمی می‌کشیم، تا به چه خواهد کشید</p></div>
<div class="m2"><p>ما قدمی می‌زنیم، تا به چه خواهد رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قبله و مذهب بسی است، یار یکی بیش نیست</p></div>
<div class="m2"><p>هر که دویی در میان دید یکی را دو دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفر سر زلف توست، قبله آتش پرست</p></div>
<div class="m2"><p>دید رخت کاتشی است، آتش از آن رو گزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ز جهان بگذرم، وز تو نخواهم گذشت</p></div>
<div class="m2"><p>ور تو به تیغم زنی، از تو نخواهم برید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در همه بحری دهند، جان به امید کنار</p></div>
<div class="m2"><p>لیک درین بحر ما، نیست کناری پدید</p></div></div>