---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>شب فراق، چو زلفت اگر چه تاریک است</p></div>
<div class="m2"><p>امیدوارم از آن رو، که صبح، نزدیک است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خفتگان، خبری می‌دهد، خروش خروس</p></div>
<div class="m2"><p>ز هاتف دگرست، آن خطاب نزدیک است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا، سلاسل دیوانگان عشق تو را</p></div>
<div class="m2"><p>به بوی زلف تو هر صبح، داده تحریک است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپرس حال من از چشم خود، که این معنی</p></div>
<div class="m2"><p>حکایتی است که معلوم ترک و تاجیک، است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز کفر زلف تو، دل ره نمی‌برد بیرون</p></div>
<div class="m2"><p>که راه پر خم و پیچ و محله تاریک است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمی‌رسد به خیال تو، آب دیده من</p></div>
<div class="m2"><p>که دیده، سخت ضعیف است و راه، باریک است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو مالکی به همه روی، در ممالک حسن</p></div>
<div class="m2"><p>مرا بپرس، که سلمانت از ممالیک است</p></div></div>