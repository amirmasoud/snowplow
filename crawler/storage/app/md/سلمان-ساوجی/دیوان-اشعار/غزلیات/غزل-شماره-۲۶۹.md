---
title: >-
    غزل شمارهٔ ۲۶۹
---
# غزل شمارهٔ ۲۶۹

<div class="b" id="bn1"><div class="m1"><p>ای بهم برزده زلف تو سراسر کارم </p></div>
<div class="m2"><p>من چو موی توام آشفته، فرو نگذارم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرده‌ام نرم به فرمان تو گردن چون شمع </p></div>
<div class="m2"><p>چه کنم من که به فرمان تو سر در نارم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه در راه تو چون خاک رهم رفته به باد </p></div>
<div class="m2"><p>تو مپندار کزین راه غباری دارم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظری کن به من آخر که چو چشم خوش تو </p></div>
<div class="m2"><p>مدتی شد که به هم برزده‌ای بنیادم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشفقی بر سر من نیست که بر آتش من </p></div>
<div class="m2"><p>زند آبی به جز از دیده مردم دارم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نیست جز صبح مرا یک متنفس همدم </p></div>
<div class="m2"><p>کز سر مهر کند یک نفسی در کارم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شعله آتش من سوخت جهانی و هنوز </p></div>
<div class="m2"><p>دم من می‌دهی و می‌نهی ای گل خارم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خام طبعان طبع تو به مدارید زمن </p></div>
<div class="m2"><p>زان که من سوخته، خام خم خمارم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هست سودای ورع در سر سلمان لیکن </p></div>
<div class="m2"><p>حلقه زلف بتان می‌شکند بازارم </p></div></div>