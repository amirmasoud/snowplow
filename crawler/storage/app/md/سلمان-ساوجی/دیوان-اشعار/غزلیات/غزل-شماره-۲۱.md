---
title: >-
    غزل شمارهٔ ۲۱
---
# غزل شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>نوبهار و عشق و مستی، خاصه در عهد شباب</p></div>
<div class="m2"><p>می‌کند، بنیاد مستوری مستوران، خراب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه مستور صاحبدل، نمی‌بینی که چون</p></div>
<div class="m2"><p>بشنود، بوی بهار، از پیش بردارد نقاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوی عشرت در بهار، از لاله می‌آید که اوست</p></div>
<div class="m2"><p>در دلش، سودای عشق و در سرش جام شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دور باد، از نرگس صاحب نظر چشم بدان</p></div>
<div class="m2"><p>کو چو چشمت، بر نمی‌دارد سر از مستی و خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدعی منعم مکن، در عاشقی، زیرا که نیست</p></div>
<div class="m2"><p>عقل را با پیچ و تاب زلف خوبان، هیچ تاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم نرگس دل به یغما برد و جان، گر می برد</p></div>
<div class="m2"><p>ترک سرمست معربد را، که می‌گوید جواب؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای بهار روی جانان! گل برون آمد ز مهد</p></div>
<div class="m2"><p>تا به کی باشد گل رخسار از ما، در حجاب؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسخه حسن رخت را عرض کن بر جویبار</p></div>
<div class="m2"><p>تا ورق‌های گل نسرین، فرو شوید به آب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بلبلان اوصاف گل گویند و ما وصف رخت</p></div>
<div class="m2"><p>ما دعای پادشاه کامران کامیاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایه لطف الهی، دندی سلطان که هست</p></div>
<div class="m2"><p>آسمان سلطنت را رای و رویش آفتاب</p></div></div>