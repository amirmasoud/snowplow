---
title: >-
    غزل شمارهٔ ۲۸
---
# غزل شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>از لب لعل توام، کار به کام است، امشب</p></div>
<div class="m2"><p>دولتم بنده و اقبالم، غلام است، امشب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسمان گو بنشان، مشعله ماه تمام</p></div>
<div class="m2"><p>که زمین را مه روی تو، تمام است، امشب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده در دین من امروز، حلال است، حلال</p></div>
<div class="m2"><p>خواب، در چشم من ای بخت، حرام است، امشب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو ای قافله صبح! مزن دم کانجا</p></div>
<div class="m2"><p>آفتابی است که در پرده شام است، امشب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع بین، سوخته آتش و او مرده شمع</p></div>
<div class="m2"><p>گوییا عاشق ازین هردو، کدام است امشب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اثر عکس لب توست، درون می‌ ناب</p></div>
<div class="m2"><p>که صفایی عجب، اندر دل جام است، امشب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من هوای حرم کعبه ندارم، که مرا</p></div>
<div class="m2"><p>عرفات سر کوی تو مقام است، امشب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حاشدت را که چو عودست بر آتش، سلمان</p></div>
<div class="m2"><p>گو همی سوز، که سودای تو خام است امشب</p></div></div>