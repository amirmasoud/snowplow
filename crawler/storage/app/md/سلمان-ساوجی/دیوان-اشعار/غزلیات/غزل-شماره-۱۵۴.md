---
title: >-
    غزل شمارهٔ ۱۵۴
---
# غزل شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>صنمی اگر جفایی کند آن جفا نباشد</p></div>
<div class="m2"><p>ز صنم جفا چه جویی که درو وفا نباشد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حبیب خود شنیدم که به نزد ما جمادی</p></div>
<div class="m2"><p>به از آن وجود باشد که درو هوا نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو به حسرت گلت گل، شوم از گلم گیاهی</p></div>
<div class="m2"><p>ندمد که بوی مهر تو در آن گیا نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خمار سر گرانم، قدحی بیار ساقی</p></div>
<div class="m2"><p>که از آن مصدعی را به ازین دوا نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نسیم می، چنان کن ملکان کاتبان را</p></div>
<div class="m2"><p>که به هیچشان شعور از بد و نیک ما نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به شکستگان شنیدم که همی کنی نگاهی</p></div>
<div class="m2"><p>به من شکسته آخر نظرت چرا نباشد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ملکیم گفت: سلمان به دعای شب وصالش</p></div>
<div class="m2"><p>بطلب که حاجت الا به دعا روا نباشد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل خسته نیست با من که ز دل کنم دعایش</p></div>
<div class="m2"><p>چه کنم دعا که بی‌دل اثر دعا نباشد</p></div></div>