---
title: >-
    غزل شمارهٔ ۳۷۹
---
# غزل شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>گفتم: خیال وصلت گفتا: بخواب بینی </p></div>
<div class="m2"><p>گفتم: مثال قدت گفتا: در آب بینی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم: به خواب دیدن زلفت چگونه باشد؟</p></div>
<div class="m2"><p>گفتا: که خویشتن را در پیچ و تاب بینی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم: رخ تو بینم گفتا: زهی تصور </p></div>
<div class="m2"><p>گفتم: به خواب جانا گفتا: به خواب بینی! </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم: که روی خوبت بنمای تا ببینم </p></div>
<div class="m2"><p>گفتا: که در دل شب چون آفتاب بینی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم: خراب گشتم در دور چشم مستت </p></div>
<div class="m2"><p>گفتا: که هر چه بینی مست و خراب بینی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم: لب تو دیدن صد جان بهاست او را </p></div>
<div class="m2"><p>گفتا: مبصری تو، در لعل ناب بینی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم: که روز سلمان شب شد ز تار مویت </p></div>
<div class="m2"><p>گفتا: نگر به رویم تا آفتاب بینی </p></div></div>