---
title: >-
    غزل شمارهٔ ۱۵۲
---
# غزل شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>ما را که شور لعلش، در سر مدام باشد</p></div>
<div class="m2"><p>سودای باده پختن، سودای خام باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جام باده حاصل، یک ساعت است مستی</p></div>
<div class="m2"><p>وز شکر لب او، سکری مدام باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با قد تو صنوبر، در چشم ما نیاید</p></div>
<div class="m2"><p>او کیست تا قدت را، قایم مقام باشد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان خواست لعلت از من، گر می‌برد حلالش</p></div>
<div class="m2"><p>جان تا لب تو خواهد، بر من حرام باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی به ناتمامان، می ده تمام و از ما</p></div>
<div class="m2"><p>بگذر که پختگان را، بویی تمام باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با این همه غم دل، گر می‌کنی قبولم</p></div>
<div class="m2"><p>اقبال هندوی من، شادی غلام باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای صد هزار طالب، جویای درد عشقت!</p></div>
<div class="m2"><p>مخصوص این سعادت، تا خود کدام باشد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در سلک بندگانت گر نیست نام ما را</p></div>
<div class="m2"><p>در نامه گدایان، باشد که نام باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبح ازل نشستم، بر آستان عشقت</p></div>
<div class="m2"><p>زین در قیام سلمان، شام قیام باشد</p></div></div>