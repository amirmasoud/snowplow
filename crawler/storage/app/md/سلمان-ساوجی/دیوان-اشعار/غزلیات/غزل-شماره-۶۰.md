---
title: >-
    غزل شمارهٔ ۶۰
---
# غزل شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>امشب، چراغ مجلس ما، در گرفته است</p></div>
<div class="m2"><p>در تاب رفته و سخن، از سر گرفته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانه چون مجال برون شد ز کوی دوست</p></div>
<div class="m2"><p>یابد بدین طریق، که او در گرفته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ظاهر نمی‌شود، اثر صبح گوییا</p></div>
<div class="m2"><p>دود دلم، دریچه خاور، گرفته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانی که چیست، مایه آن لعل آتشین؟</p></div>
<div class="m2"><p>کامروز، باز، در قدح زر، گرفته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون حرام ماست که ساقی، به روزگار</p></div>
<div class="m2"><p>در گردن صراحی و ساغر گرفته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صبح از نسیم زلف تو، یکدم دمیده است</p></div>
<div class="m2"><p>عالم همه شمامه عنبر، گرفته است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باد صبا به بوی تو در باغ، رفته است</p></div>
<div class="m2"><p>بس خردها که بر گل احمر، گرفته است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آتش که اندرونی اصحاب خلوت است</p></div>
<div class="m2"><p>شمعش نگر، که چون به زبان در گرفته است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل با خیال قد تو، بر رست در ازل</p></div>
<div class="m2"><p>زان روی راست، شکل صنوبر گرفته است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شکل صنوبری که دلش، نام کرده‌اند</p></div>
<div class="m2"><p>سلمان به یاد قد تو، در بر گرفته است</p></div></div>