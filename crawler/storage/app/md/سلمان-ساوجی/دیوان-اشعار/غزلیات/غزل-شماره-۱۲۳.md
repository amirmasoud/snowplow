---
title: >-
    غزل شمارهٔ ۱۲۳
---
# غزل شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>مسپار دل، به هر کس، که رخ چو ماه دارد </p></div>
<div class="m2"><p>به کسی سپار دل را، که دلت نگاه دارد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر چشم یار شد دل، که ز دیده، داد، خواهد </p></div>
<div class="m2"><p>عجب ار سیه دلان را، غم داد خواه دارد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو مرا مگوی واعظ، که مریز، آب دیده </p></div>
<div class="m2"><p>بگذار تا بریزم، که بسی گناه، دارد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر خرابی من، ز کسی، توان شنیدن </p></div>
<div class="m2"><p>که دلی خراب و حالی، ز غمش تباه دارد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من بی‌نوا بر گل، ره دم زدن، ندارم </p></div>
<div class="m2"><p>حسدست بر هزارم، که هزار راه ندارد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو به حسن پادشاهی، دل عاشقت رعیت </p></div>
<div class="m2"><p>خنکا رعیتی کو، چو تو پادشاه دارد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عذار و شاهد و خط، بستد رخت دل از من </p></div>
<div class="m2"><p>چه دهم جواب آن کس، که خط و گواه دارد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان دل جهانی، همه وقف خویش کردن </p></div>
<div class="m2"><p>به همین قدر که لعل تو خطی سیاه دارد </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به طریق لطف می‌کن، نظری به حال سلمان </p></div>
<div class="m2"><p>که همین قدر توقع، به تو گاه گاه دارد </p></div></div>