---
title: >-
    غزل شمارهٔ ۳۳۲
---
# غزل شمارهٔ ۳۳۲

<div class="b" id="bn1"><div class="m1"><p>داشتم روزی دلی بر من بسی بیداد ازو</p></div>
<div class="m2"><p>رفت و جز خون جگر کاری دگر نگشاد ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله و فریاد من رفت از زمین تا آسمان</p></div>
<div class="m2"><p>ناله از دل می‌کند فریاد ازو فریاد ازو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در پی دل چند گردم کاب رویم ریخت دل</p></div>
<div class="m2"><p>دست خواهم شست ازین پس هرچه باداباد ازو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می‌نشاند باد سرد دل چراغ عمر من</p></div>
<div class="m2"><p>حاصل عمرم نگر چون می‌رود بر باد ازو</p></div></div>