---
title: >-
    غزل شمارهٔ ۸۷
---
# غزل شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>می‌کشم دردی که درمانیش، نیست </p></div>
<div class="m2"><p>می‌روم راهی که پایانیش نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که در خم خانه عشق تو بار </p></div>
<div class="m2"><p>یافت برگ هیچ بستانیش نیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بندگان دارد بسی سلطان غم </p></div>
<div class="m2"><p>لیک چون من بند فرمانیش نیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که جان در ره جانانی نباخت </p></div>
<div class="m2"><p>یا ز دل دورست یا جانیش نیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود دل مجموع، در عالم که دید </p></div>
<div class="m2"><p>کز عقب آه پریشانیش نیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم ترکت کو سیه دل کافری است </p></div>
<div class="m2"><p>هیچ رحمی، بر مسلمانیش نیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم آن انسان که عاشق نیست هست </p></div>
<div class="m2"><p>راست چون عینی که انسانیش نیست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که چون سلمان به زلف کافرت </p></div>
<div class="m2"><p>نیستش اقرار، ایمانیش نیست </p></div></div>