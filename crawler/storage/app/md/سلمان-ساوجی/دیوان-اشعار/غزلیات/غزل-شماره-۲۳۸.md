---
title: >-
    غزل شمارهٔ ۲۳۸
---
# غزل شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>زحمت ما می‌دهی، زاهد تو را با ما چه کار</p></div>
<div class="m2"><p>عقل و دین و زهد را با عاشق شیدا چه کار؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌خورد صوفی غم فردا و ما می می‌خوریم</p></div>
<div class="m2"><p>مرد امروزیم، ما را با غم فردا چه کار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جای عیاران سرباز است کوی عاشقی</p></div>
<div class="m2"><p>ای سلامتجوی برو بنشین، تو را با ما چه کار؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راز لعل شاهدان بر زاهدان پوشیده است</p></div>
<div class="m2"><p>متقی را در میان مجلس صهبا چه کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما ز سودای دو چشم آهویی سر گشته‌ایم</p></div>
<div class="m2"><p>ورنه این سرگشته را در کوه و در صحرا چه کار؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل برای گوهری از راه چشمم رفته است</p></div>
<div class="m2"><p>هر که را گوهر نیاید، در دل دریا چه کار؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دین و دنیا هر دو باید باخت در بازار عشق</p></div>
<div class="m2"><p>مردم کم مایه را خود با چنین سودا چه کار؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما شراب و شاهد و کوی مغان دانیم و بس</p></div>
<div class="m2"><p>با صلاح توبه و حج و حرم ما را چه کار؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا نپنداری که سلمان را نظر بر شاهدست</p></div>
<div class="m2"><p>مست جام عشق را با شاهد رعنا چه کار؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عشق اگر زیبا بود، معشوقه گو زیبا مباش</p></div>
<div class="m2"><p>عشق را با صورت زیبا و نا زیبا چه کار؟</p></div></div>