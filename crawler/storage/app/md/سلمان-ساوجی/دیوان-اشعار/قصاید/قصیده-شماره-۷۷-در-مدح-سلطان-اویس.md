---
title: >-
    قصیدهٔ شمارهٔ ۷۷ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۷۷ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>منت خدای را که به تعید ذو المنن </p></div>
<div class="m2"><p>رونق گرفت شرع به پیرایهٔ سنن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلقی است متفق همه بر سنت اویس </p></div>
<div class="m2"><p>ملکی است مجتمع همه بر سیرت حسن </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سوری است ملک را که موسون است تا ابد </p></div>
<div class="m2"><p>از منجنیق ماتم واز رخنه ی محن </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه چهارده شبه در غره ی شباب </p></div>
<div class="m2"><p>همچون هلال گشت به خورشید مقترن </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سدر چار بالش بلقیس تکیه داد </p></div>
<div class="m2"><p>جمشید روزگار علی رقم اهرمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فرخنده باد تا ابد این سور واین زفاف </p></div>
<div class="m2"><p>بر خسروی زمانه و شه زاده ی زمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمشید عهد شیخ حسن آفتاب جا </p></div>
<div class="m2"><p>دارای ملک پرو رو نویین صف شکن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آ نکه از نهیب خنجرش اندام آفتاب </p></div>
<div class="m2"><p>پیوسته می جهد چو دل برق در یمن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از تاب زلف پرچم او عارض ظفر </p></div>
<div class="m2"><p>تا بنده چون جمال یقین از حجاب ظن </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>افکنده بحر را غضبش لرزه بر وجود </p></div>
<div class="m2"><p>آورده آب را کرمش آب در دهن </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آید زجام معد لتش بره شیر گیر </p></div>
<div class="m2"><p>گردد به یمن تربیتش پشه پیلتن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای خسروی که گر به مثل سایبان زند </p></div>
<div class="m2"><p>نو شیروان عدل تو بر ساحت چمن </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فراش باد زهره ندارد که بعد از این </p></div>
<div class="m2"><p>گردد به گرد پرده سرای گل وسمن </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شاخ در خت باز ستاند به عون تو </p></div>
<div class="m2"><p>رهزنان باد خزان برگ خویشتن </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاید اگر بنات فلک چون بنین عهد </p></div>
<div class="m2"><p>یابد در زمان تو جمعیت ترند </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از چمبر مطابعتت هر که سر به تاف </p></div>
<div class="m2"><p>حبل الورید گشت به گردن درش رسن </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حکم قضا مثال قدر قدرت تو را </p></div>
<div class="m2"><p>در کائنات حکم روان است بر بدن </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جاه تو کشوری است که در باغ حشمتش </p></div>
<div class="m2"><p>باشد بنفشه زار فلک سبزه ی دمن </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لفظ تو گوهری است که در رشته یخرد </p></div>
<div class="m2"><p>دارد هزار دانه در ثمین ثمن </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که سرکه از نهیب خمار تو شد گران </p></div>
<div class="m2"><p>دورش در اولین قدح آورد در ددن </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هم بره را به عهد تو شیر است مستشار </p></div>
<div class="m2"><p>هم قاز را به دور تو باز است موعتمن </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تا بر سریر ملک نزد تکیه ی حکم عدل تو </p></div>
<div class="m2"><p>هم خوابه نیام نشد خنجر فتن </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای رای روشن تو به روزی هزار بار </p></div>
<div class="m2"><p>بر دختران غیب قبا کرد پیرهن </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تو نور عین عدلی اگر عدل راست عین </p></div>
<div class="m2"><p>تو جان جسم شرعی اگر شرع راست تن </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همچون کشف حسود تو را پوست شد حصار </p></div>
<div class="m2"><p>چون کرم پیله ی خصم تو را جامع شد کفن </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گیرم که دشمنت به صلابت شود چو کوه </p></div>
<div class="m2"><p>سهل است هست صرصر قهر تو کوه کن </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ور چون ستاره از عدد خیل بی شمار </p></div>
<div class="m2"><p>لافی زند به غیبت خورشید تیغ زن </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چندان بود سیاهی احشام شام را </p></div>
<div class="m2"><p>کز خاوران کند یزک صبح تاختن </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با حمله یشمال چه تاب آورد چراغ </p></div>
<div class="m2"><p>با دولت همای چه پهلو زند زغن </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>هست اعتبار او همه از عدت سپاه </p></div>
<div class="m2"><p>هست اعتماد تو همه بر لطف ذو المنن </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>برهان دولتت همه شمشیر قاطع است </p></div>
<div class="m2"><p>وان مخالفت همه تزویرو مکر وفن </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چشم سعادت تو چو خورشید روشن است </p></div>
<div class="m2"><p>دائم به نور طلعت این ماه انجمن </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>دارای عهد شیخ اویس آنکه ذات اوست </p></div>
<div class="m2"><p>پیرایه ی بزرگی وسر مایه ی فطن </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آن روز از لطافت او گشته منفعل </p></div>
<div class="m2"><p>وان عقل بر شمایل آن گشته مفطنن </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جز در هوای خلق خوشش نافع دم نزد </p></div>
<div class="m2"><p>زان دم که نافع مشک بریدند در ختن </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شاها من آن کسم که به مدح تو کرده ام </p></div>
<div class="m2"><p>گوش جهانیان صدف لو لو عدن </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>من عن دلیب آن چمنم که از هوای او </p></div>
<div class="m2"><p>دارند رنگ وبو گل نسرین ونسترن </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اکنون که دور گل سپری شد ومن پناه </p></div>
<div class="m2"><p>آورده ام به سایه ی شمشاد ونارون </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ای نوبهار عدل مرا بی نوا ممان </p></div>
<div class="m2"><p>وی دور روزگار مرا بال وپر مکن </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ده سال رفت تا به هوای تو کرده ام </p></div>
<div class="m2"><p>ترک دیارو مسکن وماوای خویشتن </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ببریده ام چو نافع چینی زاهل خویش </p></div>
<div class="m2"><p>بر کنده ام چو لعل بد اخشی دل از وطن </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مگذار ضایع ام که بسی در به مدح تو </p></div>
<div class="m2"><p>در گوش روزگار بخواهم گذاشتن </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کامروز می کنند برای دعوام نام </p></div>
<div class="m2"><p>شاهان روزگار توسل به شعر من</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>رخساره یعروس بزرگی نیافت زیب </p></div>
<div class="m2"><p>الا به خردکاری مشاطه ی سخن </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>حسن کلام انوریست آنک می کند </p></div>
<div class="m2"><p>تا این زمان حکایت احسان بو الحسن </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>باقی به قول شاعر طوسی است در جهان </p></div>
<div class="m2"><p>نا موس وشیر مردی کاوس وتهمتن </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>افتاده بود بلبل تبع من از نوا </p></div>
<div class="m2"><p>بازش بهار مدح تو آورد در سخن </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا در حدیقه یفلک سبز آبگون </p></div>
<div class="m2"><p>روید به صبح وشام گل زرد ونسترن </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گلزار دولت تو که دارد نسیم خلد </p></div>
<div class="m2"><p>باد تا ابد از صرصر محن </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>وین تازه میوه ی شجر عزو جاه را </p></div>
<div class="m2"><p>از گردش زمان مرساد آفت شجن </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دائم ثنای جاه شما ذکر شیخ وشاب </p></div>
<div class="m2"><p>دائم دعای جان شما ورد مرد وزن </p></div></div>