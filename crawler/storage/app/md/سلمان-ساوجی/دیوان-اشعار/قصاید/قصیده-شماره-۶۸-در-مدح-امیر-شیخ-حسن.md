---
title: >-
    قصیدهٔ شمارهٔ ۶۸ - در مدح امیر شیخ حسن
---
# قصیدهٔ شمارهٔ ۶۸ - در مدح امیر شیخ حسن

<div class="b" id="bn1"><div class="m1"><p>اگویی خیال قد تو ای گلستان چشم! </p></div>
<div class="m2"><p>سرو است راست رسته بر آب روان چشم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نو بهار حسن تو بر چشم من گذ شت </p></div>
<div class="m2"><p>شد پر گل وشکوفه مرا بوستان چشم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمم سپر بر آب فکند ست تا تراست </p></div>
<div class="m2"><p>گیسو کمند عارض از ابرو کمان چشم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم ودلم فکنده بدین روز ومی کشم </p></div>
<div class="m2"><p>گاهی خسارت دل وگاهی زیان چشم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم فضول خانه ی دل را خراب کرد </p></div>
<div class="m2"><p>یا رب سیاه باد مرا ، خان ومان چشم !</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی به مهر روی تو ریزند چون شهاب </p></div>
<div class="m2"><p>سیارگان اشک من از آسمان چشم ؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چشمم از جمال تو خط نظر نیافت </p></div>
<div class="m2"><p>خون است در میان دل ودر میان چشم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد گنج شایگان کنم اندر هر آستین </p></div>
<div class="m2"><p>بهر نثارش از گهر رایگان چشم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پالوده ی سرشک وکباب جگر نهم </p></div>
<div class="m2"><p>پیش خیال روی تو بر گرد خوان چشم </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با آنکه آب در جگرم نیست هر شبی </p></div>
<div class="m2"><p>باشد عیار روی توام میهمان چشم </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنشاندش ز مردمی انسان عین من </p></div>
<div class="m2"><p>چون سرو تازه بر لب آب روان چشم </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وانگه زراوق عینی پیش آورم </p></div>
<div class="m2"><p>قرابه یزجاجی راوق فشان چشم </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشمم چو گلستان همه پر خار محنت است </p></div>
<div class="m2"><p>شبنم نشسته به طرف گلستان چشم </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در گوشه ها نشسته فرو برده سر بر آب </p></div>
<div class="m2"><p>ازترکتاز غمزه ی تو مردمان چشم </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چشمم خیال ابروی شوخ تو بست وهست </p></div>
<div class="m2"><p>پیوسته این خیال کج اندر کمان چشم </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از بس که من خیال تو تحریر می کنم </p></div>
<div class="m2"><p>بشکسته خامه ی مژه ام در بیان چشم </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکش خیال لعل تو در چشم خانه ساخت </p></div>
<div class="m2"><p>گوهر به آستین کشد از آستان چشم </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گلگون اشک بس که براند بهر طرف </p></div>
<div class="m2"><p>آن کس که او کشیده ندارد عنان چشم </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در انتظار مقدم خیل خیال تو </p></div>
<div class="m2"><p>روز وشب است بر سر ره دیده بان چشم </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ننشاند همچو قد ورخت هیچ سر وگل </p></div>
<div class="m2"><p>اندر حدیقه حدقه یباغبان چشم </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در چشم تو کی آیم ازین سان که غمزه هاست </p></div>
<div class="m2"><p>صف بر کشیده اند کران تا کران چشم </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هندوی چشم من سفر بحر می کند </p></div>
<div class="m2"><p>آراسته است از آن به لالی وکان چشم </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گویی سحاب خاطر دریا وکان لطف </p></div>
<div class="m2"><p>سر مایه داده است به دریا وکان چشم </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آنکو عروس باصره بی رای و حسن او </p></div>
<div class="m2"><p>بنمود چهره در تتق پرنیان چشم </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>شیرین بود ز شکر شکرش دهان گوش </p></div>
<div class="m2"><p>روشن به نور طلعت رویش روان چشم </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بی حسن روی صائب او جلوه گر نشد </p></div>
<div class="m2"><p>طاوس نور در چمن بوستان چشم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>آلا که در لقای هوای مبارکش </p></div>
<div class="m2"><p>مرغ نظر نمی پرد از آشیان چشم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر ابر همتش فکند سایه بر وجود </p></div>
<div class="m2"><p>گوهر چکد به جای نم از ناودان چشم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>چشم و چراغ اهل و جودی و از وجود </p></div>
<div class="m2"><p>ذات شریفت آمده بر سر بسان چشم </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اوج جلالت تو نبیند سپهر اگر </p></div>
<div class="m2"><p>با صد هزار دیده کند امتحان چشم </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از چشم حاسدان گل بخت تو ایمن است </p></div>
<div class="m2"><p>کو را ز خار غصه مبادا امان چشم </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از کحل موکب تو جلاگر نیافتی </p></div>
<div class="m2"><p>تاریک بودی آینه روشنان چشم </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>آن را که کحل دیده نه از خاک پای توست </p></div>
<div class="m2"><p>آب سیه برآیدش از دودمان چشم </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خصم مزور تو که روی بهیش نیست </p></div>
<div class="m2"><p>بر روی چون بهی فکند ناردان چشم </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>با زیب خاک پایت اگر چشم یاد کند </p></div>
<div class="m2"><p>از سرمه باد خاک سیه در دهان چشم </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز ادراک اوج قدر تو شد چشم نا توان </p></div>
<div class="m2"><p>پیداست که تا چه قدر بود آخر توان چشم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شاها بدان خدایی که فراش قدرتش </p></div>
<div class="m2"><p>بنهاد شمع باصره در شمعدان چشم </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>برآفتاب روی نگاران خرگهی </p></div>
<div class="m2"><p>زابروی چون هلال کشد سایبان چشم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بر مسطر د ماغ که مشکات دانش است </p></div>
<div class="m2"><p>بنشانده است هندو کی پاسبان چشم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>مهر وسپهر وروز وشب ومردم ونبات </p></div>
<div class="m2"><p>ابداع کرده حکمتش اندر جهان چشم </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از شرم آسمان فکند چشم بر زمین </p></div>
<div class="m2"><p>ار بیند این منا ظره اند ر میان چشم </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چشمم به مدح خاک درت کرد ترزبان </p></div>
<div class="m2"><p>اینک هنوز می چکد آب از دهان چشم </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>تا هست گرد عارض سیمین مدار خط </p></div>
<div class="m2"><p>تا هست زیر سایه ی ابرو مکان چشم </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>تا چشم بد خزان بهار سعادت است </p></div>
<div class="m2"><p>بادا بهار جاه تو دور از خزان چشم !</p></div></div>