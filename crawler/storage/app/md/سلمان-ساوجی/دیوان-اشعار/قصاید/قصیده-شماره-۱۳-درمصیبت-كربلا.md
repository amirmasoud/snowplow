---
title: >-
    قصیدهٔ شمارهٔ ۱۳ - درمصیبت كربلا
---
# قصیدهٔ شمارهٔ ۱۳ - درمصیبت كربلا

<div class="b" id="bn1"><div class="m1"><p>خاک، خون آغشته لب تشنگان کربلاست</p></div>
<div class="m2"><p>آخر ای چشم بلابین! جوی خون بارت کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز به چشم و چهره مسپر خاک این ره، کان همه</p></div>
<div class="m2"><p>نرگس چشم و گل رخسار آل مصطفاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل بی‌صبر من آرام گیر اینجا دمی</p></div>
<div class="m2"><p>کاندر اینجا منزل آرام جان مرتضاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سواد خوابگاه قره‌العین علی است</p></div>
<div class="m2"><p>وین حریم بارگاه کعبه عز و علاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روضه پاک حسین است این که مشک زلف حور</p></div>
<div class="m2"><p>خویشتن را بسته بر جاروب این جنت سراست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع عالم تاب عیسی را درین دیر کهن</p></div>
<div class="m2"><p>هر صباح از پرتو قندیل زرینش ضیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاب چشم زایران روضه‌اش «طوبی لهم»</p></div>
<div class="m2"><p>شاخ طوبی را به جنت قوت نشو و نماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مهبط انوار عزت، مظهر اسرار لطف</p></div>
<div class="m2"><p>منزل آیات رحمت، مشهد آل عباست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای که زوار ملایک را جنابت مقصد است</p></div>
<div class="m2"><p>وی که مجموع خلایق را ضمیرت پیشواست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعل شبرنگ تو گوش عرشیان را گوشوار</p></div>
<div class="m2"><p>گرد نعلین تو چشم روشنان را توتیاست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صفحه تیغ زبانت عاری از عیب خلاف</p></div>
<div class="m2"><p>روی مرات ضمیر صافی از رنگ ریاست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ناری از نور جبینت، شمع تابان صباح</p></div>
<div class="m2"><p>تاری از لطف سیاهت، خط مشکین مساست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نا سزایی کاتش قهر تو در وی شعله زد</p></div>
<div class="m2"><p>تا قیامت هیمه دوزخ شد و اینش سزاست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بهره جز آتش چه دارد هر که سر برد به تیغ؟</p></div>
<div class="m2"><p>خاصه شمعی را که او چشم و چراغ انبیاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هر سگی کز روبهی با شیر یزدان پنجه زد</p></div>
<div class="m2"><p>گر خود او آهوی تاتارست، در اصلش خطاست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تا نهان شد آفتاب طلعتت در زیر خاک</p></div>
<div class="m2"><p>هر سحر پیراهن شب در بر گیتی قباست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در حق باب شما آمد «علی بابها»</p></div>
<div class="m2"><p>هر کجا فضلی درین باب است، در باب شماست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا صبا از سر خاک عنبرینت برد بوی</p></div>
<div class="m2"><p>عاشق او شد به صد دل زین سبب نامش صباست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هر کس از باطل به جایی التجایی می‌کند</p></div>
<div class="m2"><p>زان میان ما را جناب آل حیدر ملتجاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کوری چشم مخالف، من حسینی مذهبم</p></div>
<div class="m2"><p>راه حق این است و نتوانم نهفتن راه راست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای چو دریا خشک لب، لب تشنگان رحمتیم</p></div>
<div class="m2"><p>آب رویی ده به ما کاب همه عالم‌تر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>خواهشت آب است و ما می‌آوریم اینک به چشم</p></div>
<div class="m2"><p>خاکسار آنکس که با دریا به آبش ماجراست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر لب رود علی، تا آب دلجوی فرات</p></div>
<div class="m2"><p>بسته شد زان روز باز افتاده آب از چشمهاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جوهر آب فرات از خون پاکان گشت لعل</p></div>
<div class="m2"><p>این زمان آن آب خونین همچنان در چشم ماست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سنگها بر سینه کوبان، جامها در نیل عرق</p></div>
<div class="m2"><p>می‌رود نالان فرات، آری ازین غم در عزاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آب کف بر روی ازین غم می‌زند، لیکن چه سود؟</p></div>
<div class="m2"><p>کف زدن بر سر کنون کاندر کفش باد هواست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>یا امام المتقین! ما مفلسان طاعتیم</p></div>
<div class="m2"><p>یک قبولت صد چو ما را تا ابد برگ و نواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یا شفیع المذنبین! در خشکسال رحمتیم</p></div>
<div class="m2"><p>زابر احسان تو ما را چشم باران عطاست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>یا امیر المومنین! عام است خوان رحمتت</p></div>
<div class="m2"><p>مستحق بی‌نوا را بر درت گوش صلاست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>یا امام المسلمین! از ما عنایت وا مگیر</p></div>
<div class="m2"><p>خود تو می‌دانی که سلمان بنده آل عباست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نسبت من با شما اکنون درین ابیات نیست</p></div>
<div class="m2"><p>مصطفی فرمود سلمان هم زاهل بیت ماست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>روضه‌ات را من هوادارم بجان قندیل‌وار</p></div>
<div class="m2"><p>آتشین دل در برم دایم معلق زین هواست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>خدمتی لایق نمی‌آید ز من بهر نثار</p></div>
<div class="m2"><p>خرده‌ای آورده‌ام وان در منظوم ثناست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هرکسی را دست بر چیزی، و ما را بر دعا</p></div>
<div class="m2"><p>رد مکن چون دست این درویش مسکین بر دعاست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>یا ابا عبدالله! از لطف تو حاجات همه</p></div>
<div class="m2"><p>چون روا شد حاجت ما گر برآید هم رواست</p></div></div>