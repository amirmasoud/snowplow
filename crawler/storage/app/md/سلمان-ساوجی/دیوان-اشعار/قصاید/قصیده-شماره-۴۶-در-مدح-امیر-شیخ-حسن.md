---
title: >-
    قصیدهٔ شمارهٔ ۴۶ - در مدح امیر شیخ حسن
---
# قصیدهٔ شمارهٔ ۴۶ - در مدح امیر شیخ حسن

<div class="b" id="bn1"><div class="m1"><p>دجله عمری است، تر وتازه که خوش می گذرد </p></div>
<div class="m2"><p>ساقیا می گذر عمر به عطلت مگذار!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند پیچیم چو زلفین تو در دور قمر ؟ </p></div>
<div class="m2"><p>چند باشیم چو چشمان تو در عین خمار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار آن است تو را کار ، ورت صد کار است</p></div>
<div class="m2"><p>بر لب دجله رو ودست بشوی از همه کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمتر از خارنه ای ، دامن گل بویی گیر </p></div>
<div class="m2"><p>کمتر از سرونه ای ، تازه نگاری به کف آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام خورشید از آن پیش که بردارد صبح </p></div>
<div class="m2"><p>جام جمشیدیصبها به صبوحی بردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام بر کف نه ودر باده نگر تا زصفا </p></div>
<div class="m2"><p>حور در پرده روحت بنماید دیدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می گلگون که کند پرتو عکسش به صبوح </p></div>
<div class="m2"><p>صبح را همچو شفق گونه به گلگونه نگار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بخت یار است وفلک تابع وایام به کام</p></div>
<div class="m2"><p>فتنه در خواب وجهان ایمن ودولت بیدار </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دور مستی است در این دور نزیبد که بود </p></div>
<div class="m2"><p>بجز از حزم خداوند جهان کس هشیار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نقطه دایره پادشهی ، شیخ حسن </p></div>
<div class="m2"><p>شاه خورشید محل ، خسرو جمشید آثار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنکه بر شاهسوار فلک ار بانگ زند </p></div>
<div class="m2"><p>که بدار ای فلک او را نبود باز مدار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>کف او مقسم ارزاق وضیع است وشریف </p></div>
<div class="m2"><p>در او کعبه آمال صغار است وکبار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بار ها با گهر افشانی دستش زحیا </p></div>
<div class="m2"><p>ابر آب دهن انداخته در روی بحار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قرص خورشید اگر در خور خوانش بودی </p></div>
<div class="m2"><p>عیسی مایده آراش بدی خوان سالار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ای که از نزهت ایوان تو بابی است بهشت</p></div>
<div class="m2"><p>وی که از روضه ی اخلاق تو فصلی است بهار !</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فلک آثار سم اسب تو در روز مصاف </p></div>
<div class="m2"><p>همه بر دیده خورشید نویسد به غبار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زحل از قدر تو آموخت بزرگی وشرف</p></div>
<div class="m2"><p>این چنین ها کند آری اثر حسن جوار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شرح رای تو دهد شمع فلک در اصباح </p></div>
<div class="m2"><p>دم زخلق تو زند باد صبا در اسحار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بیلکت چون بنهد چشم بر ابروی کمان</p></div>
<div class="m2"><p>زه به گوش ظفر آید زدهان سوفار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز بزم تو درم به همه قدر از سبکی </p></div>
<div class="m2"><p>در نیار به جوی هیچکس او را به شمار </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گرزند نا میه در دامن انصاف تو چنگ </p></div>
<div class="m2"><p>بر کند لطف تو از پای گل ونسرین خار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>باز اگر پای به دست تو مشرف نکند </p></div>
<div class="m2"><p>پای خود را ندهد بوسه به روزی صد بار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هر که بیرون نهد از دایره حکم تو پای</p></div>
<div class="m2"><p>بس که سر گشته رود گرد جهان چون پرگار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خسروا لشگر منصورت اگر رجعت کرد </p></div>
<div class="m2"><p>نیست بر دامن جاه تو ازین هیچ غبار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عقل داند که در ادوار فلک بی رجعت </p></div>
<div class="m2"><p>استقامت نپذیرند نجوم سیار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این یقین است که در عرصه ملک شطرنج </p></div>
<div class="m2"><p>برتر از شاه یکی نیست به تمکین و وقار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دیده باشی که چو رخ برطرف شاه نهد</p></div>
<div class="m2"><p>بیدقی بی هنری کم خطری بی مقدار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وقت باشد که نظر بر سبب مصلحتی </p></div>
<div class="m2"><p>نزد شاپش ویک سو شود از راه گذار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه ارزان عزم بود پایه بیدق را قدر</p></div>
<div class="m2"><p> نه از این حزم بود منصف شاهی را عار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آخر دست بر آرد اثر دولت شاه</p></div>
<div class="m2"><p>زنهادش به سم اسب وپی پیل دمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>پادشاها منم آن مدح سرایی که نیافت </p></div>
<div class="m2"><p>مثل من باغ سخن طوطی شکر گفتار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بلبلی نیست که در معرضم آید امروز </p></div>
<div class="m2"><p>من تنها وز مرغان خوش آواز هزار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا جهان را بود از گردش ایام نظام </p></div>
<div class="m2"><p>تا زمین را بود از جنبش افلاک قرار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>باد در سایه اقبال تو شهزاده اویس</p></div>
<div class="m2"><p>دایم از عمر وجوانی وجهان بر خوردار!</p></div></div>