---
title: >-
    قصیدهٔ شمارهٔ ۷۸ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۷۸ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>این وصلت مبارک وین مجلس همایون </p></div>
<div class="m2"><p>بر پادشاه عالم فرخنده باد ومیمون </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهی که باز چترش هر گه که پر گشاید </p></div>
<div class="m2"><p>طاوس چرخش آید در سهیه یهمهیون </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فر مانروای عالم مقصود نسل آدم </p></div>
<div class="m2"><p>جمشید هفت کشور دارای ربع مسکون </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان اویس شاهی کز سیر مر کب او </p></div>
<div class="m2"><p>بر روی چرخ وانجم دامن فشانده هامون </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از موکبش فلک را اطباق دیده کوحلی </p></div>
<div class="m2"><p>وز مدحتش ملک را اوراق طبع مشحون </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مجلسی که طبعش عزم نشاط کرده </p></div>
<div class="m2"><p>بر دست ساقیانش گردیده جام گردون </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون جام دور بزمش وقت صبوح خندد </p></div>
<div class="m2"><p>خورشید را بر آید از شرم روی گلگون </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با صوت رود سازش چون برکشد نباشد </p></div>
<div class="m2"><p>در چشم های میزان اشکال زهره موزون </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تن در نداد قطعا قدرش بدان چه دوران </p></div>
<div class="m2"><p>از روزوشب به قدش اطلس برید واکسون </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آن که از درون صافی پیشش کمر نبندد </p></div>
<div class="m2"><p>چون کوه چشمهایش آرد زمانه بیرون </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ای داوری که داری زآفات آسمانی </p></div>
<div class="m2"><p>چون ملک آسمانی اطراف ملک محصون </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>احوال مهرا رای تو کرد روشن </p></div>
<div class="m2"><p>اعمال ملک ودین را عدل تو بسته قانون </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با نسبت جمالت گیتی چو چاه یوسف </p></div>
<div class="m2"><p>با نسبت کمالت گردون چو حوت ذو النون </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جز بحر عین ذاتت با نون نشد مغارن </p></div>
<div class="m2"><p>کافی که از حدودش سی منزل است تا نون </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در اهتمام کمتر لالای درگه توست </p></div>
<div class="m2"><p>بر قصر لاجه وردی چندین هزار خاتون </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می خواست نعل اسبت گردون نداشت وجهی </p></div>
<div class="m2"><p>تاج مرصع از سر برداشت کرد مرهون </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خط مسلسل تو بر نهاد لیلی </p></div>
<div class="m2"><p>عقل از سلاسل آن سودایی است ومجنون </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر کس که در نیارد سر با تو چون صراحی </p></div>
<div class="m2"><p>همچون پیاله اش دل مادام باد پر خون </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گردون علو رطبت از در گه تو دارد </p></div>
<div class="m2"><p>فی الجمله بنده یتوست گر عالیست گردون </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو وارثی کیان را چون در قرون ماضی </p></div>
<div class="m2"><p>داراب را سکندر جمشید را فریدون </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر شام تا چو یوسف در چاه مغرب افتد </p></div>
<div class="m2"><p>خورشید وشب بر آید همراه گنج قارون </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بادا نثار عهدت هر گنج دولتی کان </p></div>
<div class="m2"><p>تقدیر داشت آن را از کنج غیب مد فون </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روزو شبت ملازم سورو سرور عشرت </p></div>
<div class="m2"><p>روز سرور سورت تا شام حشر مغرون </p></div></div>