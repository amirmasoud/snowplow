---
title: >-
    قصیدهٔ شمارهٔ ۳۷ - در مدح غیاث الدین محمد
---
# قصیدهٔ شمارهٔ ۳۷ - در مدح غیاث الدین محمد

<div class="b" id="bn1"><div class="m1"><p>آن دم که باد صبح به زلفت گذر کند</p></div>
<div class="m2"><p>مشک ختن به خون جگر چهره تر کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آگه نه‌ای که سنبل تو مشک را</p></div>
<div class="m2"><p>هر دم ز روی رشک چه خون در جگر کند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یاد تو سوختگان اجل را شفا دهد</p></div>
<div class="m2"><p>بوی تو خفتگان عدم را خبر کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هردم که از صفای جمال تو دم زنم</p></div>
<div class="m2"><p>صبحم سر از دریچه انفاس برکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگه که مهر روی تو در خاطر آورم</p></div>
<div class="m2"><p>خورشید سر ز روزن اندیشه در کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارم شکسته بسته چو زلفت دلی که او</p></div>
<div class="m2"><p>هر دم هوای صحبت رویی چو خور کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کار من از تو راست به زر می‌شود چو زر</p></div>
<div class="m2"><p>آری چو زر بود همه کاری چو زر کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوشه نهاد سر به کمرگاه تو مگر</p></div>
<div class="m2"><p>آمد که با تو دست هوس در کمر کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سرگشته هندویت، چه سوداست بر سرش؟</p></div>
<div class="m2"><p>آن که به این خیال کج از سر بدر کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل خواست تا حکایت زلف تو مو به مو</p></div>
<div class="m2"><p>معلوم رای آصف جمشید فر کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیکن چنین حدیث پراکنده چون کسی</p></div>
<div class="m2"><p>دربندگی خواجه نیکو سیر کند؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خورشید آسمان وزارت که آسمان</p></div>
<div class="m2"><p>خاک درش به سرمه کحل بصر کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اعظم غیاث دولت و دین آنکه روزگار</p></div>
<div class="m2"><p>نامش وزیر مملکت بحر و بر کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تا رایت مظفر سلطان خاوری</p></div>
<div class="m2"><p>هر شام عزم مملکت باختر کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بادا ز قدر رایت چنانکه سر</p></div>
<div class="m2"><p>هر روز فتح عرصه ملکی دگر کند</p></div></div>