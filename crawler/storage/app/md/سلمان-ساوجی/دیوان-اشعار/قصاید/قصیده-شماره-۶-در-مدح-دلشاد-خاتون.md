---
title: >-
    قصیدهٔ شمارهٔ ۶ - در مدح دلشاد خاتون
---
# قصیدهٔ شمارهٔ ۶ - در مدح دلشاد خاتون

<div class="b" id="bn1"><div class="m1"><p>ای عید رخت کعبه دل اهل صفا را</p></div>
<div class="m2"><p>هر لحظه صفایی دگر از روی تو ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کعبه حسنی و سر زلف تو حرم روح قدس را</p></div>
<div class="m2"><p>در موقف کون تو مفام اهل صفا را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لبیک زنان بر عرفات سر کویت</p></div>
<div class="m2"><p>صد قافله جان منتظر آواز درآرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی زمزم آتش وش لعلت</p></div>
<div class="m2"><p>جان هر نفسی بر لب خشک آمده ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید طواف حرم وصل تو افکند</p></div>
<div class="m2"><p>در وادی غم طایفه بی‌سر و پارا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو در خم محراب دو ابروی تو کردم</p></div>
<div class="m2"><p>گفتم: مگر آنجا اثری هست دعا را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سایه محراب نظر کرد دلم دید</p></div>
<div class="m2"><p>ترکان خطایی نسب حور لقا را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فریاد برآورد: که ای قوم که ره داد</p></div>
<div class="m2"><p>سرمست به محراب حرم ترک خطا را!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشمت به کرشمه نظری کرد که تن زن</p></div>
<div class="m2"><p>بر مست همان به که نگیرد خطا را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زایر، حرم کعبه گزید از پی فردوس</p></div>
<div class="m2"><p>ما کوی تو آن کعبه فردوس نما را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حاجی به طواف حرم کعبه، ملازم</p></div>
<div class="m2"><p>ما طوف کنان بارگاه کعبه بنا را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دلشاد شه، آن سایه یزدان که زرایش</p></div>
<div class="m2"><p>خورشید فلک رفعت خورشید لقا را</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سلطان قضا رای قدر قدر، که چون او</p></div>
<div class="m2"><p>سلطان قدر قدر نبوداست قضا را</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در عهد اسکندر عدلش نبود بیم</p></div>
<div class="m2"><p>از رخنه یاجوج اجل سد بقا را</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با مهر سلیمان قبولش نبود راه</p></div>
<div class="m2"><p>در دایره خطه دل دیو هوا را</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از عفت او می‌دهد آن بوی که دیگر</p></div>
<div class="m2"><p>در پرده گل ره نبود باد صبا را</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مهر نظر تربیت او بدماند</p></div>
<div class="m2"><p>در ماه دی از شور زمین، مهر گیا را</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای از شرف سجده درگاه تو حاصل!</p></div>
<div class="m2"><p>این تاج مرصع فلک سبز لقا را!</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر آینه تیغ تو گوهر بنماید</p></div>
<div class="m2"><p>رخساره به خون لعل کند کاه ربا را</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ور صبح ضمیرت تتق از چهره گشاید</p></div>
<div class="m2"><p>از روی جهان برفکند زلف سیا را</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در پرده‌سرای تو کشد زهره به گردن</p></div>
<div class="m2"><p>چنگ طرب مطربه پرده‌سرا را</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنجا که سحاب کرمت سایه بگسترد</p></div>
<div class="m2"><p>بر باد دهد ابر سیه روی گدا را</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر قیمت خاک کف پای تو کند عقل</p></div>
<div class="m2"><p>از گوهر خود نقد کند وجه بها را</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر جا که دلی جسته خلاص از مرض جهل</p></div>
<div class="m2"><p>بنمود اشارات تو قانون شفا را</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>چون مهر شود چشم و چراغ همه عالم</p></div>
<div class="m2"><p>گر شمع ضمیر تو دهد نور سها را</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا شعر مرا زیور مدح تو شعارست</p></div>
<div class="m2"><p>بر چرخ سخن شعری شعرم شعرا را</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>منثور شود گوهر منظوم ثریا</p></div>
<div class="m2"><p>در مدح تو چون نظم دهم ورد ثنا را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا از نفس باد صبا هر سر سالی</p></div>
<div class="m2"><p>دوران کهن تازه کند عهد صبا را</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر شام و سحر عکس گل و نسترن از باغ</p></div>
<div class="m2"><p>سرخاب و سفید آب کند روی هوا را</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بلبل از سر سوز دهد ساز غزل را</p></div>
<div class="m2"><p>قمری به سر سرو کند راست نوا را</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بادا چمن جاه شما خرم و سرسبز!</p></div>
<div class="m2"><p>زان سان که بران رشک برد صحن سما را</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا عید چو نوروز بود غره شادی</p></div>
<div class="m2"><p>هر روز زنو، عید دگر باد شما را</p></div></div>