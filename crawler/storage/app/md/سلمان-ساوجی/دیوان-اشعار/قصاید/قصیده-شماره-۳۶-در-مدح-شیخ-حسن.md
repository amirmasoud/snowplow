---
title: >-
    قصیدهٔ شمارهٔ ۳۶ - در مدح شیخ حسن
---
# قصیدهٔ شمارهٔ ۳۶ - در مدح شیخ حسن

<div class="b" id="bn1"><div class="m1"><p>دل را هوای چشم تو بیمار می‌کند</p></div>
<div class="m2"><p>جان را امید وصل تو تیمار می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرار طره تو دلم برد عارضت</p></div>
<div class="m2"><p>رو وانهاده پشتی طرار می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بندگی قد تو شد کار سرو راست</p></div>
<div class="m2"><p>آزادی از تو دارد و هموار می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خال تو پیش چشم تو زعنبر بخور کرد</p></div>
<div class="m2"><p>وین بهره قوت دل بیمار می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هشیار باش ای دل غافل که چشم یار</p></div>
<div class="m2"><p>مست است و قصد مردم هشیار می‌کند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدار او به خواب خیال است دیده را</p></div>
<div class="m2"><p>کاری است اینکه دولت بیدار می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دربست با دلم دهن تنگ او به هیچ</p></div>
<div class="m2"><p>او این چنین مضایقه بسیار می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>افتاده دل ز کار به یکبارگی که یار</p></div>
<div class="m2"><p>هرجا غمی است بر دل من بار می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرغ شکسته بال دل من که روز و شب</p></div>
<div class="m2"><p>پرواز در هوای رخ یار می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تشویش از آن دو دام دلاویز می‌برد</p></div>
<div class="m2"><p>اندیشه زان دو ترک کماندار می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مست است و بی‌خبر مگر از دور عدل شاه</p></div>
<div class="m2"><p>چشم سیه دلش که دل آزار می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>دارای عهد، شیخ حسن، آنکه خدمتش</p></div>
<div class="m2"><p>چرخ دوتا به چاروبه ناچار می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>شاهی که در هلاک اعادی به روز رزم</p></div>
<div class="m2"><p>احیای رسم حیدر کرار می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>روشن شد اینکه از غضب اوست کافتاب</p></div>
<div class="m2"><p>خوناب لعل در دل احجار می‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پوشیده نیست کز کرم اوست کاسمان</p></div>
<div class="m2"><p>دیبای سبز در بر اشجار می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از شرم رای روشن او هر شب آفتاب</p></div>
<div class="m2"><p>چون سایه سجده پس دیوار می‌کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خسروی که کوکبه رای روشنت</p></div>
<div class="m2"><p>رایات آفتاب نگونسار می‌کند!</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>از طبیب خلق نافه گشای تو شمه‌ای است</p></div>
<div class="m2"><p>باد آن روایتی که ز گلزار می‌کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از فیض دست بحر یسار تو قطره‌ایست</p></div>
<div class="m2"><p>ابر آن ترشحی که به اقطار می‌کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در قطع و فصل دشمن بد اصل بدگهر</p></div>
<div class="m2"><p>تیغ تو پاکی گهر اظهار می‌کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو ملتفت مشو به عدو ز آنکه خود فلک</p></div>
<div class="m2"><p>تدبیر دفع فتنه اشرار می‌کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کانکس که کرد در حق دارا بدی هنوز</p></div>
<div class="m2"><p>نقاش نقش او همه بردار می‌کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر مرتفع شوند نجوم فلک چه باک؟</p></div>
<div class="m2"><p>رای تو حکم ثابت و سیار می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پیر ار بود وعده تدبیر چون نکرد</p></div>
<div class="m2"><p>امید داشتم که مگر پاره می‌کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زامسال نیز قرب سه مه رفت و بند گیش</p></div>
<div class="m2"><p>با من همان حکایت پیرار می‌کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در حسب حال تذکره نظم کرده‌ام</p></div>
<div class="m2"><p>نظمی که کسر لول شهوار می‌کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کاری ز پیش می‌رود از لطف شاهیش</p></div>
<div class="m2"><p>این نظم را پیش تو در کار می‌کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا هر بهار خامه نقاش روزگار</p></div>
<div class="m2"><p>بر خار نقش صورت فرخار می‌کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سرسبز باد گلبن جاه تو تا زرشک</p></div>
<div class="m2"><p>در چشم دشمنان مژه چون خار می‌کند!</p></div></div>