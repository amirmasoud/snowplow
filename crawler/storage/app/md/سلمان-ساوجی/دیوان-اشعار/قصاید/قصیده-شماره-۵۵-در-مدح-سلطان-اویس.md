---
title: >-
    قصیدهٔ شمارهٔ ۵۵ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۵۵ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>مبشران سعادت برین بلند رواق </p></div>
<div class="m2"><p>همی کنند ندا در ممالک آفاق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که سال هفتصد و پنجاه و هفت رجب</p></div>
<div class="m2"><p>به اتفاق خلایق بیاری خلاق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشست خسرو ریو زمین به استحقاق </p></div>
<div class="m2"><p>فراز تخت سلاطین به دار ملک عراق </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدایگان سلاطین عهد شیخ اویس </p></div>
<div class="m2"><p>پنا هو پشت جهان خسرو علی الا طلاق </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شهنشهی که برای نثار مجلس اوست </p></div>
<div class="m2"><p>پر از جواهر انجم سپهر را اطباق </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشام روح ودماغ خرد زباغ بهشت </p></div>
<div class="m2"><p>به جز روایح خلقش نکرده استنشاق </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>-زبان ناطقه از منهیان عالم غیب </p></div>
<div class="m2"><p>به جز نتایج طبعش نکرده استنطاق </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فکنده قصه یوسف جمال او در چاه </p></div>
<div class="m2"><p>نهاده نامه کسری ، زمان او بر طاق </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر نه ترک فلک پیش او کمر بند </p></div>
<div class="m2"><p>فلک به جای کله بر سرش نهد بغطاق </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کسی به دولت عدلش نمی کند جز عود </p></div>
<div class="m2"><p>ز دست راهزنان ، نا له در مقام عراق </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چه گوشمال که از دست او کشیده کمان ؟ </p></div>
<div class="m2"><p>چه سر زنش که ز انصاف او نیافت چماق ؟</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>زهی شهنشه انجم تو را کمینه غلام ! </p></div>
<div class="m2"><p>زهی مبارز پنچم تو را کهینه وشاق !</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به بندگی جناب تو خسروان مشعوف </p></div>
<div class="m2"><p>بپای بوس رکاب تو سر وران مشتاق </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز گوشه های سیر تو بخت جسته وطن </p></div>
<div class="m2"><p>به خانه های کمانت ظفر گر فته وثاق </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فروغ تیغ به چشم تو لمعه ای ساغر </p></div>
<div class="m2"><p>نوای کوس به گوش تو ناله عشاق </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کمان هیبتت افکنده سهم در اطراف </p></div>
<div class="m2"><p>کمند طاعتت آورده دست در اعناق </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به بحر نسبت طبع تو می کنو همه وقت </p></div>
<div class="m2"><p>اگر چه در صف بحر می کنم اغراق </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>صحیفه ای است وجود مبارک تو درو </p></div>
<div class="m2"><p>همه مکارم ذات و محاسن اخلاق </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علو قدر تو را آفتاب اگر نگردد </p></div>
<div class="m2"><p>چو سایه باز فتد در رواق چرخ به طاق </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>صبا ز دفتر خلق تو یک ورق می خواند </p></div>
<div class="m2"><p>چمن مجلد گل را به باد داد اوراق </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شمال صیت تو را شد براق برق عنان </p></div>
<div class="m2"><p>هلال زین براق تو گشت وبدر جناق </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز هیبت تو دل دشمنان بروز نبرد </p></div>
<div class="m2"><p>چنان بود که دل عاشقان به روز فراق </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خدایگانا ! ز امروز تا به روز حساب </p></div>
<div class="m2"><p>به توست عالمیان را حوالت ارزاق !</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تراست مملکت سلطنت به استقلال </p></div>
<div class="m2"><p>تراست سلطنت مملکت ، به استحقاق </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جهانیان همه زنهاریان عدل تواند </p></div>
<div class="m2"><p>امیدوار به فضل ومراحم واشفاق </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به چشم راستی آنکس که ننگرد در تو </p></div>
<div class="m2"><p>چو نرگسش بدر آورد ز پلکها ، احداق </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به آب تیغ نشان آتش شرارت خصم </p></div>
<div class="m2"><p>از آنکه می زندش دیگ سینه جوش نفاق </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یقین به موضوع تریاک داده باشی زهر </p></div>
<div class="m2"><p>به جای زهر عدو را اگر دهی تریاق </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اگر چه با تو نه آبای آسمان خوردند </p></div>
<div class="m2"><p>به چادر مادر عنصر هزار پی سه طلاق </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به سد عدل حصین کن حصار دولت خویش </p></div>
<div class="m2"><p>مباش غافل از این چرخ ازرق زراق </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هنوذ با تو کنون می خورد فلک سوگند </p></div>
<div class="m2"><p>هنوز با تو کنون می کند جهان میثاق </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به پایه ای برسی از شرف که چون سدره </p></div>
<div class="m2"><p>درخت قدر تو بر ساق عرش ساید ساق </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شها ! به شکر طوطی گر این حدیث از من </p></div>
<div class="m2"><p>کند سماع شکر خوش نباشدش به مذاق </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مرادلی وزبانی است پر صفا وصفات </p></div>
<div class="m2"><p>مرا سری ودرو نیست بر وفا ووفاق </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>عروس خاطر من نیست زان قبیل که او </p></div>
<div class="m2"><p>به جز قبول جنابت کند قبول صداق </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همیشه تا ملک شرق با مداد پگاه </p></div>
<div class="m2"><p>بر آید وکند آفاق روشن از اشراق </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خجسته باد تو را تاج وتخت سلطانی ! </p></div>
<div class="m2"><p>به بندگیت سلاطین عهد بسته نطاق !</p></div></div>