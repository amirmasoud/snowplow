---
title: >-
    قصیدهٔ شمارهٔ ۱۵ - در مدح دلشاد خاتون
---
# قصیدهٔ شمارهٔ ۱۵ - در مدح دلشاد خاتون

<div class="b" id="bn1"><div class="m1"><p>مصور ازل از روح، صورتی می‌خواست</p></div>
<div class="m2"><p>مثال قد تو را برکشید و آمدراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنفشه سنبل زلفت به خواب دید شبی</p></div>
<div class="m2"><p>علی الصباح پریشان و سرگران برخاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه خیال سر زلف بار می‌بندم</p></div>
<div class="m2"><p>شب دراز و برانم که سر به سر سوداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیال سرو بلندت در آب می‌جویم</p></div>
<div class="m2"><p>زهی لطیف خیالی که در تصور ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ناز اگر بخرامد درخت قامت تو</p></div>
<div class="m2"><p>ز جای خود برود سرو، اگرچه پابرجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان حسن تو خوش عالمی است ز آنکه درو</p></div>
<div class="m2"><p>شمال بر طرف آفتاب، غالیه ساست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تراست بی‌سخن اندر دهان نهان گوهر</p></div>
<div class="m2"><p>نشان گوهر پاک تو در سخن پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیا به حلقه دیوانگان عشق و ببین</p></div>
<div class="m2"><p>کز آن سلاسل مشکین چه فتنه‌ها برپاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فتادگان سر کوی دوست بسیارند</p></div>
<div class="m2"><p>ولیکن از سر کویت چو من فتاده نخاست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو نیم مرده چراغی است آتشین، جانم</p></div>
<div class="m2"><p>که در هوای تو بر رهگذر باد صباست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر آن نظری که نه در روی توست، عین خطاست</p></div>
<div class="m2"><p>هر آن نفس که نه بر یاد توست، باد هواست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رخ تو چشمه مهرست و گرد چشمه مهر</p></div>
<div class="m2"><p>دمیده سبزه خطت، مثال مهر گیاست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>فتاده خال تو بر آفتاب می‌بینم</p></div>
<div class="m2"><p>مگر که سایه چتر رفیع ظل خداست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خدایگان سلاطین بحر و بر، دلشاد</p></div>
<div class="m2"><p>که آسمان بزرگی و آفتاب عطاست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دلش به چشم یقین از دریچه امروز </p></div>
<div class="m2"><p>همه مشاهد احوال عالم فرداست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز شادی کف دستش مدام در مجلس</p></div>
<div class="m2"><p>امل به قهقه خندان، چو ساغر صهباست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قصور عقل ز درک کمال رفعت او</p></div>
<div class="m2"><p>مثال چشمه خورشید، و چشم نابیناست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به بوی آنکه دماغ ملوک تازه کند</p></div>
<div class="m2"><p>غبار اشهب او، گشته عنبر ساراست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بدان امید که در سلک خادمانش کشند</p></div>
<div class="m2"><p>کمینه حلقه به گوش تو، لولو لالاست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز تاب پرتو انوار روی روشن او</p></div>
<div class="m2"><p>پناه جسته نظیرش به سایه عنقاست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ایا ستاره سپاهی که برج عصمت را</p></div>
<div class="m2"><p>فروغ قبه مهد تو غره غراست!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تو عین لطفی و دریا، غدیر مستعمل</p></div>
<div class="m2"><p>تو نور محضی و گردون، غبار مستعلاست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رفیع قدر تو چرخی همه ثبات و قرار</p></div>
<div class="m2"><p>شریف ذات تو بدری، همه دوام و بقاست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زمانه را ز تو خطی که جسم را ز حیات</p></div>
<div class="m2"><p>وجود را به تو راهی که چشم را به ضیاست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به کوشش آمده بر سر حسام تو در رزم</p></div>
<div class="m2"><p>به بخشش آمده برتر کف تو از دریاست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بیاض تیغ تو آیینه جمال و ظفر</p></div>
<div class="m2"><p>زبان کلک تو دندانه کلید رجاست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کف به بسط، بسیط جهان گرفت و تو را</p></div>
<div class="m2"><p>کف آیتی است که آن بر کفایت تو گواست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تمکن تو سراپرده در مقامی زد</p></div>
<div class="m2"><p>که زهره با همه سازش، کنیز پرده‌سراست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلت نوشته بر اقطار ابر را ادرار</p></div>
<div class="m2"><p>کف تو رانده در آفاق بحر را اجراست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ز روی و رای تو خورشید، با هزار فروغ</p></div>
<div class="m2"><p>ز زبم عیش تو ناهید، با هزار نواست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به عهد عدل تو اسم خلاف بر بیداست</p></div>
<div class="m2"><p>ازین مخالفتش افتاده لرزه بر اعضاست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به مرده‌ای که رسد مژده عنایت تو</p></div>
<div class="m2"><p>چو غنچه در کفنش آرزوی نشو و نماست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سرای جاه تو دار الشفا پنداری</p></div>
<div class="m2"><p>به خاک پای تو کان خون‌بهای مشک خطاست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ز باغ تیغ زمرد لباس خون ریزت</p></div>
<div class="m2"><p>علامت یرقان بر جبین کاهرباست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ز چین ابروی خوبت به چشم خسرو چین</p></div>
<div class="m2"><p>فضای عرصه چین تنگ‌تر ز چین قباست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هلال نعل ستاره ستام گردون سیر</p></div>
<div class="m2"><p>جهان نورد و زمان سرعت و زمین پیماست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بلند پایه چو همت، فراخ رو چو طمع</p></div>
<div class="m2"><p>گران رکاب چو حلم و سبک عنان چو ذکاست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شب سعادت ارباب دولت است مگر</p></div>
<div class="m2"><p>که روشنی سحر در مبادیش پیداست؟</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز روز و شب بگذشتی اگر نه آن بودی</p></div>
<div class="m2"><p>که روز روشنش از روی و تیره شب زقفاست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ز اشتیاق سمش رفته نعل در آتش</p></div>
<div class="m2"><p>شکال از آرزوی دست بوس او برپاست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>به سعی و قوت سیرش، رسیده خاک زمین</p></div>
<div class="m2"><p>هزار پی ز حضیض سمک بر اوج سماست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>شدن به جانب بالا سحاب را ماند</p></div>
<div class="m2"><p>ولی عرق نکند آن و این غریق حیاست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>جوان چو دولت سلطان روان چو فرمانش</p></div>
<div class="m2"><p>جهنده همچو اعادی رسیده همچو قضاست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>شها، حسود ترا گر نمی‌تواند دید</p></div>
<div class="m2"><p>تو زشادی که سبب کوربختی اعداست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مدار باک زکید عدو که در همه وقت</p></div>
<div class="m2"><p>مدار دور فلک بر مدار رای شماست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>اگر چه دشمن آتش نهاده سوخته دل</p></div>
<div class="m2"><p>ز تاب تیغ تو در سنگ خاره ساخته جاست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کنون ببین که ز تاپیر نعل شبرنگت</p></div>
<div class="m2"><p>بسان لمعه آتش، بجسته از خاراست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>برآب زد سر جهل دشمنت نقشی</p></div>
<div class="m2"><p>گهی کز آتش شمشیر توامان می‌خواست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بسان مردمک چشم خود چون بدید، صورت بست</p></div>
<div class="m2"><p>که خود هرآینه اینجای بهترین ملجاست</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>زبان چرب تو اینک برآورد زمانه و نبود</p></div>
<div class="m2"><p>یکی چنانکه در آینه تصور ماست</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>عدوی خیبریت گر به قلعه جست پناه</p></div>
<div class="m2"><p>شکوه حیدریت منجنیق قلعه گشاست</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فلک جناب شها، با جناب عالی شاه</p></div>
<div class="m2"><p>مرا ز گردش گردون دون شکایت‌هاست</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>سوار گرم رو آفتاب پنداری</p></div>
<div class="m2"><p>کشیده تیغ زر از بهر مردم داناست</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>جهان اگرچه سراپای رنگ و بوست همه</p></div>
<div class="m2"><p>ولی نه رنگ مروت درو، نه بوی وفاست</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تو خوی و رسم سپهر و ستاره از من پرس</p></div>
<div class="m2"><p>نه در سپهر محابا، نه در ستاره حیاست</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>نه آخر از ستم، طبع دهر بی‌مهرست؟</p></div>
<div class="m2"><p>نه آخر از سبب، چرخس سرکش رعناست</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>که بی‌اردات و اختیار قرب دو ماه</p></div>
<div class="m2"><p>کمینه بنده شاه از رکاب شاه جداست</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>تنم بکاست از این غم چو شمع و نیست عجب</p></div>
<div class="m2"><p>که سینه همدم سوز است و دیده جفت بکاست</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ز خدمت ار چه جدا بوده‌ام و لیک مرا</p></div>
<div class="m2"><p>همیشه در عقب شاه لشگری ز دعاست</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>قوافل دعوت از زبان من همه وقت</p></div>
<div class="m2"><p>رفیق کوکبه صبح و کاروان صباست</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>منم که نیست مرا در سخات هیچ سخت</p></div>
<div class="m2"><p>تویی که در سخن امروز خاتم الشعراست</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ز روی آینه زرنگار روشن روز</p></div>
<div class="m2"><p>همیشه تا نفس پاک صبح زنگ زداست</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ز گرد خاطر و زنگ کدورت ایمن باد</p></div>
<div class="m2"><p>درون پاک تو کایینه خدای نماست</p></div></div>