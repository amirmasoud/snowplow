---
title: >-
    قصیدهٔ شمارهٔ ۳۵ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۳۵ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>صبح ظفر از مشرق امید بر آمد</p></div>
<div class="m2"><p>اصحاب غرض را تب سودا ببر آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غنچه پیکان و زباد دم شمشیر</p></div>
<div class="m2"><p>بشکفت گل فتح و نسیم ظفر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آینه تیغ شهنشاه دگر بار</p></div>
<div class="m2"><p>رخسار دل‌آرای ظفر جلوه‌گر آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی‌درد سر نیزه و آمد شد پیکان</p></div>
<div class="m2"><p>آن فتح که مفتاح امان بود برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سلطان فلک با کفن و تیغ به زنهار</p></div>
<div class="m2"><p>زیر علم خسرو جمشید فر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خورشید کرم، شیخ اویس آنکه ثریا</p></div>
<div class="m2"><p>در کوکبه همت او بی‌سپر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جمشید جهانگیر که خاک کف پایش</p></div>
<div class="m2"><p>تاج سر گردون مرصع کمر آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن قلزم زخار که عمان گهربخش</p></div>
<div class="m2"><p>با موج کف او ز شمار شمر آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تیغ و قلمش رابطه خوف و رجا گشت</p></div>
<div class="m2"><p>لطف و غضبش واسطه نفع و ضر آمد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک رو زعطایش نه که یک ساعت خرجش</p></div>
<div class="m2"><p>محصول تر و خشک همه بحر و بر آمد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر سرکه به خاک در او گشت مشرف</p></div>
<div class="m2"><p>همچون فلک از دور ازل تاجور آمد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ای شیر شکاری که به عونت چو غزاله</p></div>
<div class="m2"><p>آهو بره در چشم و دل شیر نر آمد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون خط نگارین بتان بر گل رخسار</p></div>
<div class="m2"><p>طغرای تو آرایش دور قمر آمد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابر سر شمشیر تو هرجا که ببارد</p></div>
<div class="m2"><p>از خاک زمین خنجر بران به بر آمد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنجا که نسیم دم لطف تو اثر کرد</p></div>
<div class="m2"><p>بر شاخ شجر، زهره به جای زهر آمد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از سیر سپاهت خم چوگان فلک را</p></div>
<div class="m2"><p>گه گوی زمین زیر و گهی بر زبر آمد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکس که چو نرگس نتوانست تو را دید</p></div>
<div class="m2"><p>از عین حسد، دیده شوخش به در آمد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چون نقره دلت با همه کس صافی و پاک است</p></div>
<div class="m2"><p>کار تو درست از پی آن همچو زر آمد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>هرکس که به عهد تو بر او اسم خلاف است</p></div>
<div class="m2"><p>چون بید سراپاش، سزای تبر آمد</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>اوصاف کمالات تو از شرح فزون است</p></div>
<div class="m2"><p>وصف تو نه به اندازه فکر بشر آمد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن را که جگر گرم شد از آتش کینت</p></div>
<div class="m2"><p>هم چشمه شمشیر تواش آبخور آمد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرز تو چه سودا به سر خصم درافتاد</p></div>
<div class="m2"><p>رمحت به دلش راست چو اندیشه در آمد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ تو که از زخم زبان مغز سران برد</p></div>
<div class="m2"><p>هرجا که دمی زد دم او کارگر آمد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بر دوش بلای سیه آمد سر خصمت</p></div>
<div class="m2"><p>وز هر سر مویش بلایی به سر آمد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>دو لشکر جرار که از کینه یکایک</p></div>
<div class="m2"><p>چون کوه سراپا همه تیغ و کمر آمد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>این پیش تو بر خاک ره افتاد چو سایه</p></div>
<div class="m2"><p>وآن ز آتش تیغ تو جهان، چون شرر آمد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>فی الجمله، یکی جست و برون شد ز میانه</p></div>
<div class="m2"><p>والقصه، یکی از در زنهار در آمد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شاها! منم آن طوطی گویا که به شکرت</p></div>
<div class="m2"><p>از گفته من کام جهان پر شکر آمد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زان روی که دارم دم مشکین، من مسکین</p></div>
<div class="m2"><p>چون نافه نصیبم همه خون جگر آمد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باشد به هنر بیشی قدر همه کس، لیک</p></div>
<div class="m2"><p>کم قدری من بنده به قدر هنر آمد</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>قسمت چو به تقدیر قضا رفت، رضا ده</p></div>
<div class="m2"><p>سلمان چه توان کرد نصیب این قدر آمد؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا هست محل بد و نیک و غم و شادی</p></div>
<div class="m2"><p>زین خانه شش سو که به اول دو در آمد</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چون رکن حرم قبله شاهان جهان باد</p></div>
<div class="m2"><p>درگاه تو کز جاه جهانی دگر آمد</p></div></div>