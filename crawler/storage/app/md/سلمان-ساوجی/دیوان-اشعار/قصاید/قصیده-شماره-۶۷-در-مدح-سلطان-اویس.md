---
title: >-
    قصیدهٔ شمارهٔ ۶۷ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۶۷ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>خوش نسیمی از چمن بر خاست بر خیز ای ندیم ! </p></div>
<div class="m2"><p>خوش بر آور در هوای باغ یک دم چون نسیم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صبحدم بوی عرار نجد می بخشد شمال </p></div>
<div class="m2"><p>جان بپرور بو که بتوان یافت در شام این شمیم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می جهد نبض صبا خوش خوش به حد اعتدال </p></div>
<div class="m2"><p>تا طبایع را مزاج مختلف شد مستقیم </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سنم باید که طرف بوستان سازی مقام </p></div>
<div class="m2"><p>چون قدح باید که گرد دوستان گردی مقیم </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون هوا در جنبش آید دل کجا گیرد قرار </p></div>
<div class="m2"><p>چون قدح در گردش آید عقل کی ماند سلیم </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تفرج خواهی اندر باغ بسم اله داری </p></div>
<div class="m2"><p>هر ورق بین دفتری از صنع رحمان رحیم </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبحدم بشنو که در دیبا چه ی فصل بهار </p></div>
<div class="m2"><p>می دهد بلبل مفصل شرح ابواب نعیم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از نسیمی گشت گل در غنچه پیدا چون مسیح </p></div>
<div class="m2"><p>با درختی در حکایت رفت بلبل چون کلیم </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سنبل از زلف نگار من سوادی یافت کج </p></div>
<div class="m2"><p>نرگس از چشم سیاهش نسخه ای دارد سقیم </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لاله را در سر خیال تاج گردد چون ملوک </p></div>
<div class="m2"><p>غنچه در دل نقش های خوب بندد چون حکیم </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نر گس از مینا وسیم زر تو گویی جمع کرد </p></div>
<div class="m2"><p>بر ورق های ریاحین شکل جیم وعین ومیم </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر سریر سلطنت گل می دهد هر روز بار </p></div>
<div class="m2"><p>راستی در سلطنت گل شوکتی دارد عظیم </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گنج باد آورد سیم برف بود اندر زمین </p></div>
<div class="m2"><p>چون زر قارون فرو برد این زمان گنج وسیم </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد به یک دم بارور چون دختر عمران زباد </p></div>
<div class="m2"><p>مادر بستان که شش ماهست تا هست او عقیم </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز ابر نوروزی بسی بر شاخ با رمنت است </p></div>
<div class="m2"><p>گر کسی منت برد فی الجمله باری از کریم </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست جایی آن که از لطف هوا پیدا شود </p></div>
<div class="m2"><p>قوت نشو ونما در شخص مدفون رمیم </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ساقی احسان سلطان گوییا بخشیده است </p></div>
<div class="m2"><p>آب را فیض مدام وباد را لطف عمیم </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آفتاب آسمان سلطنت سلطان اویس </p></div>
<div class="m2"><p>کافتابش هم چو ما هست از غلامان قدیم </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آ ن که دارد بوی خلقش باد چون گل در دماغ </p></div>
<div class="m2"><p>و آنکه بندد نقش نامش لعل چون زر در صمیم </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در زمن او جگر خونین ودل سوراخ نیست </p></div>
<div class="m2"><p>در جهان جز نامه ودر هیچ مسکین ویتیم </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گر نسیم لطف او بر آتش دوزخ وزد </p></div>
<div class="m2"><p>شاخ نار آرد همه گلنار بار اندر جحیم </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>استواء خط رای او اگر بیند الف </p></div>
<div class="m2"><p>از خجالت زین سبب در پیش دارد سر چو جیم </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در سر کوه از خیال برق شمشیر فتد </p></div>
<div class="m2"><p>تا کمر گه کوه را از فرق سر سازد دو نیم </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اژدهای رایتت در دامن آخر زمان </p></div>
<div class="m2"><p>فتنه ها را چون کشف سر در گریبان یافته</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از زبانتبز شمشیرت که قاطع حجت است </p></div>
<div class="m2"><p>دعوی عدل تو را ملک تو برهان یافته</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در هوای دست بوس و پای بوست اسمان </p></div>
<div class="m2"><p>ماه را گاهی چو گوی و گه چو چوگان یافته</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>طوطی لفظ شکر خای تو بر خوان سخن </p></div>
<div class="m2"><p>پر طاوس ملایک را مگس را ن حیران</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اندر این مدت که بود از بس غبار عارضه </p></div>
<div class="m2"><p>چرخ چشم روشنان تاریک وحیران یافته</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روزگار اندر مزاج بدور صدر سلطنت </p></div>
<div class="m2"><p>انحرافی ز اختلاف چرخ گردان یافته</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>قرة العین جهان را یک دو روزی چشم بد </p></div>
<div class="m2"><p>ز تکسر ناتوان چون چشم خوبان یافته</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ادل سواد مملکت را بود دور از روی تو </p></div>
<div class="m2"><p>چون سواد طره دلگیر و پریشان یافته</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در دعایت در مساجد شب همه شب تا به روز </p></div>
<div class="m2"><p>مومنان را همچو شمع از سوز گریان یافته </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>صبحدم زان غم که ناگه بر تو بادی بگذارد </p></div>
<div class="m2"><p>آتشین دل در بر خورشید لرزان یافته </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آسمان گردیده چون نرگس به بستان کرده باز </p></div>
<div class="m2"><p>غنچه هایش یک به یک در دیده پیکان یافته </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>منت ایزد را که می بینم به یمن همتت </p></div>
<div class="m2"><p>چشم بیمار جهان داروی درمان یافته </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>موسی عمران علم بر وادی ایمن زده </p></div>
<div class="m2"><p>یوسف دولت خلاص از چاه زندان یافته </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>سالها گیتی نثار مقدم این روز را </p></div>
<div class="m2"><p>دامن دریا و کان پر در ومرجان یافته </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>کرده دستت در کنار سایلان شکرانه را </p></div>
<div class="m2"><p>هر سرشک لعل وکان بر چهره ی کان یافته </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آتش طبعم به فر مدحتت داغ حسد </p></div>
<div class="m2"><p>بر جبین خان خاقانی وخاقان یافته </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در جنابت کز طهارت چون جناب مصطفاست </p></div>
<div class="m2"><p>بنده ی خود را گاه سلمان گاه حسان یافته </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا بد ونیک جهان را پنج حس وشش جهت </p></div>
<div class="m2"><p>از نه آباء وسه روح وچار ارکان یافته </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باد با احباب واعدایت قرین هر نیک وبد </p></div>
<div class="m2"><p>کاسمان زآغاز تا انجام دوران یافته !</p></div></div>