---
title: >-
    قصیدهٔ شمارهٔ ۶۴ - در مدح امیر شیخ حسن
---
# قصیدهٔ شمارهٔ ۶۴ - در مدح امیر شیخ حسن

<div class="b" id="bn1"><div class="m1"><p>ای سر کوی تو را کعبه رسانیده سلام </p></div>
<div class="m2"><p>عاشقان را حرم کعبه کوی تو مقام </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سعی در راه تو حج است و غمت زاد مرا </p></div>
<div class="m2"><p>در ره حج تو این زاد همه عمره تمام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سالکان طرق عشق تو هم کرده فدا </p></div>
<div class="m2"><p>جان درآن بادیه بی دیه خون آشام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طایره سد ره نشین را که حمام حرم است </p></div>
<div class="m2"><p>از هوا دانه خال تو درآورده به دام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حسرت زمزم خاک درت آن مشرب </p></div>
<div class="m2"><p>جان ما را به لب آورده چو جام است مدام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نبات لب تو آب خضر بوده مضر </p></div>
<div class="m2"><p>بی هوای در تو بیت حرم گشته حرام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر در کعبه کوی تو زباران سرشک </p></div>
<div class="m2"><p>ناودانهاست فرود آمده تا شام ز بام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بود سنگ سیه دل غمت از جا ببرد </p></div>
<div class="m2"><p>دل چه باشد که به مهر تو کند صخره قیام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>کعبه روی صفا بخش تو در کعبه روی </p></div>
<div class="m2"><p>آفتابی ست بنامیزد در ظل غمام</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جز به زلف سیت فرق نشاید کردن </p></div>
<div class="m2"><p>که کدام است جمال تو و خورشید کدام</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر کجا گفته جمال تو که عبدی عبدی </p></div>
<div class="m2"><p>زده لبیک لب خواجه سیاره غلام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آفتابی و چنان گرد تو دل ذره صفت </p></div>
<div class="m2"><p>در طواف است که یک ذره ندارد آرام </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زان لب ای عید همایون شکری بخش مرا ! </p></div>
<div class="m2"><p>که به قربان لبان شکرینت با دام </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>حاجیادر پی مقصود قدم فر سودی </p></div>
<div class="m2"><p>خنک آنان که به گام می برسیدند به کام </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چه کنی این همه ره ؟ صدر رهت آخر گفتم </p></div>
<div class="m2"><p>کز تو تا کعبه مقصود دو گام ست دو گام </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دولت حاج نیابد مگر آن کس که به صدق </p></div>
<div class="m2"><p>بندد احرام در کعبه حاجات انام </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>صورت لطف خدا مظهر حاجات ، اویس </p></div>
<div class="m2"><p>ظل حق روی ظفر پشت وپناه اسلام </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>لمعات ظفر از پرچم او می تابد </p></div>
<div class="m2"><p>چون کواکب زسواد شکن زلف ظلام </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رای او آن که دهد پیر خرد را تعلیم </p></div>
<div class="m2"><p>فکر او آن که کند سر قضا را اعلام </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خوانده از چهره یامروز نقوش فردا</p></div>
<div class="m2"><p>دیده از روزن آغاز لقای انجام </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای زاندیشه تیغ تو بداند یشان را </p></div>
<div class="m2"><p>نقطه از صلب گریزان و جنین از ارحام </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عکس رای تو اگر بر رخ ماه افتادی </p></div>
<div class="m2"><p>خواستی مهر به عکس از رخ مه نور به وام </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>شرم رای تو رخ عین کند چون دل نون </p></div>
<div class="m2"><p>زخم تیر تو دل قاف کند چون تن لام </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از می ساغر لطف تو حبابی ناهید </p></div>
<div class="m2"><p>وز دم آتش قهر تو شراری بهرام </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نظر پاک تو در کتم عدم می بیند </p></div>
<div class="m2"><p>آنچه اسکندر وجم دید در آیینه یجام </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دیده از کبک در ایم تو شاهین شاهی </p></div>
<div class="m2"><p>کرده با شیر به دوران تو گوران آرام </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چرخ بر عزم طواف در تو هر روزی </p></div>
<div class="m2"><p>بسته از چادر کافوری صبحست احرام </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کوه را گر تف قهر تو بگیرد نا گه </p></div>
<div class="m2"><p>خون لعلش به طیق عرق آید به مشام </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آب را با سخطت پای بود در زنجیر </p></div>
<div class="m2"><p>کوه را با غضبت لرزه فتد بر اندام </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با کفت ابر حیا داشت زیم خواهش آب </p></div>
<div class="m2"><p>گفت چون ملتمسی می طلبم هم زکرام </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>کمترین نایب دیوان تو در مسند حکم </p></div>
<div class="m2"><p>آسمان را قلم نسخ کشد بر احکام </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در زوایای حریم حرم معد لتت </p></div>
<div class="m2"><p>شده طاوس ملایک به حمایت چو حمام </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>شد به خون عدویت تیغ به حدی تشنه </p></div>
<div class="m2"><p>که زبان از دهان افکنده برونست حسام </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>می گدازد تن خود را زر از آن شوق کجا </p></div>
<div class="m2"><p>لقب شاه کند نقش جبین از پی نام </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>قلمم گر به ثنای تو ز سر ساخت قدم </p></div>
<div class="m2"><p>طبع من ریخت به دامن گهرش در اقدام </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا کند فصل خزان ابر سیه بستان را </p></div>
<div class="m2"><p>یعنی اطفال چمن راست کنون وقت طعام </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>مهرگان باد همایون ومبارک عیدت </p></div>
<div class="m2"><p>ای همایون زرخت عید وشهور وایام ! </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شب اقبال نکوه خواه تو در زیور روز </p></div>
<div class="m2"><p>صبح اعمار بداند یش تو در کسوت شام </p></div></div>