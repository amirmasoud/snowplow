---
title: >-
    قصیدهٔ شمارهٔ ۲۰ - در مدح امیر شیخ حسن 
---
# قصیدهٔ شمارهٔ ۲۰ - در مدح امیر شیخ حسن 

<div class="b" id="bn1"><div class="m1"><p>تا باد خزان رانگ رز رنگرزان است </p></div>
<div class="m2"><p>گویی که چمن کارگه رنگرزان است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر برگ رز اینک به زر آب است نوشته </p></div>
<div class="m2"><p>کانکس که چنین رنگ کند رنگرز آن است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رفت آنکه به زنگار و بقم سبزه و لاله </p></div>
<div class="m2"><p>گفتی که سم گور و لب رنگرزان است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز چو چشم اسد و شاخ غزال است </p></div>
<div class="m2"><p>گر شاخ درخت است و گر رنگرزان است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر برگ رزان قطره باران شده ریزان </p></div>
<div class="m2"><p>اشکی است که بر چهره عشاق روان است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آب شمر آن همه ماهی زراندود </p></div>
<div class="m2"><p>بید از پی آن ریخت که به راه یرقان است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ابر سر خوان فلک دیده پر از برگ </p></div>
<div class="m2"><p>از ذوق فرود آمده آبش به دهان است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یاران سبک روح معطل منشینید </p></div>
<div class="m2"><p>امروز که روز طلب و رطل گران است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ماه رمضان رفت، دگر عذر میارید </p></div>
<div class="m2"><p>خیزید و می‌آرید که عیدست و خزان است </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در غره شوال محرم نبود، می </p></div>
<div class="m2"><p>آن رفت که گویند رجب یا رمضان است </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عمر از پی دنیا مگذارید به سختی </p></div>
<div class="m2"><p>خوش می‌گذرانید که دنیا گذران است </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نای است فرو رفته دم آواز دهیدش </p></div>
<div class="m2"><p>کو گوش به ره دارد و چشمش نگران است </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از دست مغان چنگ از آن رو که زنندش </p></div>
<div class="m2"><p>در بارگه شاه برآورده فغان است </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دارای زمان، شیخ حسن، آنکه به تحقیق </p></div>
<div class="m2"><p>دارای زمین است و خداوند زمان است </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بحری است که در وقت سکون، کوه رکاب است </p></div>
<div class="m2"><p>ابری است که گاه حرکت، برق عنان است </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آن نیست قضا کز سخن او به درآید </p></div>
<div class="m2"><p>هرچیز که او گفت چنین است چنان است </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای شیر شکاری که دل شیر زبیمت </p></div>
<div class="m2"><p>همچون دل آهوی فلک در خفقان است </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جود تو محیطی است که بی غور و کنار است </p></div>
<div class="m2"><p>جاه تو جهانی است که بی حد و کران است </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>قدر تو درختی است که طاووس فلک را </p></div>
<div class="m2"><p>پیوسته بر اغصان جلالش طیران است </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>عدل تو چو رسم ستم اسباب جدل را </p></div>
<div class="m2"><p>برداشته یکبارگی از روی جهان است </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مملکتت آنچه بگویند کسی هست </p></div>
<div class="m2"><p>کز بهر جدل تیز کند تیغ فسان است </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ناداده به عهد تو کسی آب حسامت </p></div>
<div class="m2"><p>انصاف تو مالیده بسی گوش کمان است </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ورنه چه سبب میل کمان است به گوشه </p></div>
<div class="m2"><p>خود را ز چه رو تیغ کشیده ز میان است </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>الا که سنان همچو حسام از گهر بد </p></div>
<div class="m2"><p>در مملکتت طعنه زدن کس نتوان است </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>امروز از ایشان که به مجموع مذاهب </p></div>
<div class="m2"><p>مستوجب حدند و حسام است و سنان است </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر چیز تنی دارد و جانی و روانی</p></div>
<div class="m2"><p>تو جان و تن ملکی و حکم تو روان است </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بخت از هوس صحبت تو خواب ندارد </p></div>
<div class="m2"><p>زان روز و شبش خاک جناب تو مکان است </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گر بخت شود عاشق روی تو عجب نیست </p></div>
<div class="m2"><p>تو وجه حسن داری و بخت تو جوان است </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شاها چو دعا گوت بسی‌اند دعاگو </p></div>
<div class="m2"><p>تا ظن نبری کو ز قبیل دگران است </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در راه هوا، مجمره و شمع دمی گرم </p></div>
<div class="m2"><p>دارند ولی این به دم و آن به زبان است </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جایی که درآید به زبان بلبل طبعم </p></div>
<div class="m2"><p>آنجا شکرین نکته طوطی، هذیان است </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>من ختم سخن می‌کنم اکنون به دعایت </p></div>
<div class="m2"><p>کامین ملایک ز میان دل و جان است </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا هست جهان در کنف امن و امان باد </p></div>
<div class="m2"><p>ذات تو که او واسطه امن و امان است</p></div></div>