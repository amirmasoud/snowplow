---
title: >-
    قصیدهٔ شمارهٔ ۲۷ - در مدح سلطان اویس 
---
# قصیدهٔ شمارهٔ ۲۷ - در مدح سلطان اویس 

<div class="b" id="bn1"><div class="m1"><p> گفت: لبش نکته‌ای، لعل بدخشان شکست</p></div>
<div class="m2"><p> زد دهنش خنده‌ای، پسته خندان شکست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز به چوگان زلف، آمد و میدان بتاخت</p></div>
<div class="m2"><p>گوی دلم را که شد، پاره و چوگان شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کی به رخ او سد، با همه تاب آفتاب</p></div>
<div class="m2"><p>خاصه که او طرف گل، بر مه تابان شکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با خط نسخش که آن انشا یاقوت اوست</p></div>
<div class="m2"><p>خال سیه شد غبار، رونق ریحان شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرد یرون ز آستین دست که خون ریزدم</p></div>
<div class="m2"><p>دیبه چین از حریر، از سر دستان شکست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف جان پای بست، بود به زندان دل</p></div>
<div class="m2"><p>غمزه سرمست او، زد در زندان شکست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برقع او روی بست، آرزوی من نداد</p></div>
<div class="m2"><p>کار بیکبارگی، بر من ازینسان شکست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماهر خان فلک، با تو مقابل شدند</p></div>
<div class="m2"><p>مهر جمالت فکند، بر مه تابان شکست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم تو هر ناوکی، کز خم مشکین کمان</p></div>
<div class="m2"><p>بر دل من زد دروناوک و پیکان شکست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روی تو بس فتنه‌ها، کز پس برقع نمود</p></div>
<div class="m2"><p>چشم تو بس قلب‌ها، کز صف مژگان شکست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گریه خونین من، رشته گوهر گسست</p></div>
<div class="m2"><p>خنده شیرین تو، حقه مرجان شکست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در پی روی تو ماه، ترک خور و خواب کرد</p></div>
<div class="m2"><p>بر سر کوی تو مهر، پای دل د جان شکست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>زانچه تو ترکم کنی، ترک تو نتوان گرفت</p></div>
<div class="m2"><p>زانچه دلم بشکنی، عهد تو نتوان شکست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در دل من بود و هست آرزوی زلف تو</p></div>
<div class="m2"><p>هجر تو آن آرزو، در دل سلمان شکست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آتش روی بتان، آب جمالت نشاند</p></div>
<div class="m2"><p>گردن اعدای دین دولت سلطان شکست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>داور خورشید فر، شاه اویس آنکه او</p></div>
<div class="m2"><p>از شرف و منزلت، پایه کیوان شکست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه کفش در سوال، کام و لب بحر بست</p></div>
<div class="m2"><p>وانکه دلش در نوال، دست و دل کان شکست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آب حسامش به روم، آتش قیصر نشاند</p></div>
<div class="m2"><p>لعب سنانش به چین، لعبت خاقان شکست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نسخه سر دلش، صاحب جوزا نوشت</p></div>
<div class="m2"><p>حمل نوال کفش، کفه میزان شکست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>همت عالی او، کوکبه بر عرصه‌ای</p></div>
<div class="m2"><p>راند که نعل هلال، درسم یکران شکست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روی فلک لشگرش، درگه جنبش نهفت</p></div>
<div class="m2"><p>پشت زمین مرکبش، در صف جولان شکست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پشه به پشتی او، گردن پیلان شکست</p></div>
<div class="m2"><p>صعوه به یاری او، شهپر عقبان شکست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بازوی او گاه بزم، بازوی رستم ببست</p></div>
<div class="m2"><p>پنجه او روز زور، پنجه دستان شکست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تیغ و مه ار یک قدم، جز به مرادش زدند</p></div>
<div class="m2"><p>هم قدم این برید، هم قلم آن شکست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>خوان فلک گر چه هست، رزق جهانی برو</p></div>
<div class="m2"><p>سفره انعام او پایه آن خوان شکست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کاسه و خان فلک، چیست که در مطبخش</p></div>
<div class="m2"><p>روز ضیافت چنین، کاسه فراوان شکست؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خوانی و یک نان گرم بروی نشنید کس</p></div>
<div class="m2"><p>آنکه به عالم کسی، گوشه آن نان شکست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای که کمین چاوشت، درگه با سامیشی</p></div>
<div class="m2"><p>قبه جان خطا، در کله خان شکست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شب به خلافت مگر، زد نفسی ورنه صبح</p></div>
<div class="m2"><p>در دهن شب چرا، آن همه دندان شکست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>مملکتی را که زد، قهر تو شبخون برو</p></div>
<div class="m2"><p>بیضه صبحش فلک، در کف دوران شکست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>معدلت کسرویت، داشت جهان را به پای</p></div>
<div class="m2"><p>ورنه درآورد بود، طاق نه ایوان شکست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>صیت سنانت به بحر، گوش نهنگان بسفت</p></div>
<div class="m2"><p>زخم عمودت به بر، مهره ثعبان شکست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زهره مطرب تو را، ساز مغنی کشید</p></div>
<div class="m2"><p>تیر محرر تو را، کاغذ دیوان شکست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چرخ به دخل جهان، خرج تو را شد ضمان</p></div>
<div class="m2"><p>مال ضمان بر فلک، از ره نقصان شکست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نیست صبا تندرست زانکه به دوران تو</p></div>
<div class="m2"><p>یافت به مویی ازو، زلف پریشان شکست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>طبع تو هر گه که داد، گوهر منظوم نظم</p></div>
<div class="m2"><p>کلک تو در زیر پا، لولوی عمان شکست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عقل چو با آفتاب، رای تو را دید، گفت:</p></div>
<div class="m2"><p>پایه خورشید را سایه یزدان شکست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بخت جوان تو برد، گوی ز پیر فلک</p></div>
<div class="m2"><p>دولت کیخسروی قوت پیران شکست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>فتنه آخر زمان، مایه باست نشاند</p></div>
<div class="m2"><p>لشگر فسق و فساد، حمله طوفان شکست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ماهچه سنجقت بر در سمنان و خوار</p></div>
<div class="m2"><p>لشگر مازندران همچو خراسان شکست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دولت تو کار کرد، لیک به تحقیق من</p></div>
<div class="m2"><p>با تو بگویم که کار، از چه بر ایشان شکست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>نعمت و لطف تو را قدر چو نشناختند</p></div>
<div class="m2"><p>گردن آن طاغیان، علت طغیان شکست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زود بگیرد نمک، دیده آن کس که او</p></div>
<div class="m2"><p>نان و نمک خورد و رفت، نان و نمکدان شکست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بود وجود حسود، صورت عصیان محض</p></div>
<div class="m2"><p>سیلی انصاف تو، گردن عصیان شکست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>پیرویت کرد خصم، مدتی و عاقبت</p></div>
<div class="m2"><p>جانب کفران گرفت، بیعت ایمان شکست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>با تو معارض شود ضد تو، اما کجا</p></div>
<div class="m2"><p>دیو تواند به ریو، مهر سلیمان شکست؟</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>دعوی حساد، کرد حجت تیغ تو قطع</p></div>
<div class="m2"><p>رایت اضداد را، آیت قرآن شکست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>تا که بر آن است شرع کاخر کار جهان</p></div>
<div class="m2"><p>یابد از آسیب حشر، گنبد گردان شکست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>باد مشید چنان قصر جلالت که چرخ</p></div>
<div class="m2"><p>هیچ نیارد بر آن خانه و بنیان شکست</p></div></div>