---
title: >-
    قصیدهٔ شمارهٔ ۷۶ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۷۶ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>نقره ی خنگ صبح را در تاخت سلطان ختن </p></div>
<div class="m2"><p>ساقیا گلگون کمیتت را به میدان در افکن </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خسرو چین می کند بر اشهب زرین ستام </p></div>
<div class="m2"><p>تا به شام اندر عقیب لشکر شتاختن </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست گلگون باده را کامی که بوسد لعل تو </p></div>
<div class="m2"><p>سعی کن تا کام گلگون را بر آری از دهن </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمه ای بر قله ی کوهسار مشرق جوش زد </p></div>
<div class="m2"><p>ای پسر سیراب گردان قله را از حوض دن </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چرخ توسن را که دارد هر سرمه ناخنه </p></div>
<div class="m2"><p>باز می بینم که هستش چشم اختر غمزه </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باد پای عمر سر کش تند وناخوش می رود </p></div>
<div class="m2"><p>دست وپایش را شکالی ساز از مشکین رسند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر رگر دان هان کمیتت را که می گیرد سبق </p></div>
<div class="m2"><p>اشهب مشکین دم خاور در آهوی ختن </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبح شب رنگ سیا وش را سر افسار بتاب </p></div>
<div class="m2"><p>بر گرفت از سر به جایش بست رخش تهمتن </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سبز خنگ آسمان را کش مرصع بود جعل </p></div>
<div class="m2"><p>زین زرین بر نهاد از بحر جمشید زمن </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شهسوار ابلق دور زمان سلطان اویس </p></div>
<div class="m2"><p>آفتاب آسمان ملک ظل ذو المنن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گوشه ی نعل براقش حلقه ی گوش فلک </p></div>
<div class="m2"><p>ابغر سم سمندش سر مه ی چشم پرند </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نه سپهر آورد زیر په سهند همتش </p></div>
<div class="m2"><p>دم نزند از سبز ه در مرغزار پر سمن </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هست از آن برتر براق آسمان اصطبل را </p></div>
<div class="m2"><p>پایگه کوه سر فرود آرد به خضرای دمن </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با محیط دست در پایش جواد او چرا </p></div>
<div class="m2"><p>ابرش ابر آب خور سازد ز دریای عدن </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در صفات مرکب صر صر تک جمشید عهد </p></div>
<div class="m2"><p>می نم تضمین دو بیت از سحر بیت خویشتن </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ملک را امید فتح از چرخ باید قطع کرد </p></div>
<div class="m2"><p>چشم بر گرد سمند شاه باید داشتن </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زان که هیچ از دست وپای ابلق شام وسحر </p></div>
<div class="m2"><p>بر نمی خیزد به غیر از گرد آشوب .فتن </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که سیاس مر کبانت سایش پنجم رواق </p></div>
<div class="m2"><p>وای غلام آستانت خسرو و زرین مجن </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر براق برق را بر سر کن حکمت لجام </p></div>
<div class="m2"><p>هیچ نتواند زجا جستن دگر برق یمن </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>با در دستت زمام آسمان تا آفتاب </p></div>
<div class="m2"><p>هر سحر خواهد عنان از حد مشرق تاختن </p></div></div>