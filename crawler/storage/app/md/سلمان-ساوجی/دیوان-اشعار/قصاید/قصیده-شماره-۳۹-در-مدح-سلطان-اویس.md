---
title: >-
    قصیدهٔ شمارهٔ ۳۹ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۳۹ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>وقت آن آمد که بلبل در چمن گویا شود</p></div>
<div class="m2"><p>بهر گل گوید «خوش آمد» تا دل گل وا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچه غناج و شاخ شوخ رنگ آمیزی گل</p></div>
<div class="m2"><p>این دم طاووس گردد و آن سر ببغا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی گل برچین شود چون درنیارد چین برو</p></div>
<div class="m2"><p>نازک اندامی که چندان خارش اندر پا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شجر مرغ سحر گوید کلیم آسا کلام</p></div>
<div class="m2"><p>چون ید بیضای صبح از جیب شب پیدا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوه جام لاله گیرد ابر لولو گسترد</p></div>
<div class="m2"><p>باغ چون مینو نماید راغ چون مینا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خسرو ملک فلک بهر تماشای بهار</p></div>
<div class="m2"><p>از زمستان خانه‌های زیر بر بالا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوه را کاندر زمستان داشت از قاقم قبا</p></div>
<div class="m2"><p>اطلس گلزیر روی جامه خارا شود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رعد چون دعد از هوا نالد به سودای رباب</p></div>
<div class="m2"><p>باد چون وامق فدای غنچه عذرا شود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر کشد آواز ابر و در چکاند از دهن</p></div>
<div class="m2"><p>گوشه‌های باغ از آن پر لولوی لالا شود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زال گیتی را که بهمن داشت در آهن داشت به بند</p></div>
<div class="m2"><p>خط سبزش بردمد پیرانه سر برنا شود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>روز عیش و عشرت است امروز و محروم آنکه او</p></div>
<div class="m2"><p>عیش امروزی گذارد در پی فردا شود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شکل عین عید پیدا شد ز لوح آسمان</p></div>
<div class="m2"><p>عارفی کوتابه عینی این چنین بینا شود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در بهار آمد صبوحی فرض اگر نه هر صباح</p></div>
<div class="m2"><p>لاله را ساغر چرا پر لاله گون صهبا شود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گل چو درگیرد چراغ از شمع کافوری صبح</p></div>
<div class="m2"><p>بلبل شوریده چون پروانه ناپروا شود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پیکر نرگس دو سر بر هیات میزان بود</p></div>
<div class="m2"><p>گلبن نسرین به شکل گلشن جوزا شود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سوسن آزاد بگشاید زبان را تا چو من</p></div>
<div class="m2"><p>مادح سلطان معز الدین و الدنیا شود</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتاب سلطنت سلطان اویس آنکه از شکوه</p></div>
<div class="m2"><p>حمله‌اش گر کوه بیند پای کوه از جا شود</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه رای خرده دانش گرنماید اهتمام</p></div>
<div class="m2"><p>ذره خرد از بزرگی آسمان آسا شود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر مزاج نخل و نحل از لطف او یابد مدد</p></div>
<div class="m2"><p>نیش او پر نوش گردد خار آن خرما شود</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هرکجا بال همای چتر شاهی باز شد</p></div>
<div class="m2"><p>آشیان باز و شاهین کبک را ماوا شود</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تا سر انگشتش از نی ساخت طوطی نزد عقل</p></div>
<div class="m2"><p>نیست مستعبد که چوب خشک اژدرها شود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر درش جوزا بدان امید می‌بندد کمر</p></div>
<div class="m2"><p>کش عطارد صاحب دیوان استیفا شود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>چون براق عزم جزمش زیر زین آرد ملک</p></div>
<div class="m2"><p>ذاکر تسبیح سبحان الذی اسری شود</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ملک روی رای او چون دید گفت ار کار من</p></div>
<div class="m2"><p>با سر و سامان شود زین روی ملک آرا شود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گفت ابرویم که با فیض کف فیاض او</p></div>
<div class="m2"><p>این همه ادرار و اجرا از چه خرج ما شود</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ای شهنشاهی که گر مهر افکنی بر آفتاب</p></div>
<div class="m2"><p>عاشق دیدار خور خفاش چون حربا شود!</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ابر چندان گرید از رشک کف دستت که اشک</p></div>
<div class="m2"><p>آید از چشمش روان در دامن صحرا شود</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>وصف حکمت گر به گوش صخره صما رسد</p></div>
<div class="m2"><p>ای بسا خارا که در چشم دل خارا شود</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>می‌نماید دشمن ملکت سودای از سپاه</p></div>
<div class="m2"><p>تا دماغ مملکت شوریده زان سودا شود</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زود بهر دفع آن سودا به خون گردنش</p></div>
<div class="m2"><p>روی بیضای حسام خسروی حمرا شود</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>این همه غوغا که خصمت را ز سودا در سرست</p></div>
<div class="m2"><p>آخر این برگشته طالع گشته غوغا شود</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دشمنت خود را به دست خود بدستت می‌دهد</p></div>
<div class="m2"><p>تا مگر دستی بگردد پایه‌اش بالا شود</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>پس عجب مرغی حریص افتاده است این آدمی</p></div>
<div class="m2"><p>کز برای دانه‌ای صدبار در دریا شود</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آخر آن نادان که هرگز دانه‌اش روزی مباد</p></div>
<div class="m2"><p>بسته دام بلا چون مرغک دانا شود</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>چاکری باید فرستادن به دفع آن عدو</p></div>
<div class="m2"><p>چون تو شاهی کی معارض با چنین اعدا شود</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن کند حقا که رستم کرد در مازندران</p></div>
<div class="m2"><p>بر سر گردان ز خیلت گر پری تنها شود</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در ثنای حضرت شاها ز بحر خاطرم</p></div>
<div class="m2"><p>هر گهر کان سر برآرد لولو لالا شود</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>قرنها ملک سخن باید کشیدن انتظار</p></div>
<div class="m2"><p>تا چو من صاحب قرانی دیگرش پیدا شود</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>غره می‌باشد به نظم خویش هرکس تا چو من</p></div>
<div class="m2"><p>شهره عالم به نظم دلکش غرا شود</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شعر من نگرفت عالم جز به عون دولتت</p></div>
<div class="m2"><p>کی چنین فتحی به سعی خاطر تنها شود</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باید اول التفات پادشاهی همچو تو</p></div>
<div class="m2"><p>بعد از آن طبعی چو طبع بنده تا اینها شود</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا نویسد منشی دور فلک منشور عید</p></div>
<div class="m2"><p>بر سر منشور شکل ماه نو طغرا شود</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باد نام عالیت طغرای هر منشور کان</p></div>
<div class="m2"><p>نافذ از دیوان حکم کشور خضرا شود</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مقدم عیدت مبارک، پایه قدرت چنان</p></div>
<div class="m2"><p>کز علو چرخ گردون صد درج اعلا شود!</p></div></div>