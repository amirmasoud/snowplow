---
title: >-
    قصیدهٔ شمارهٔ ۵۸ - در مدح شیخ زاهد برادر سلطان اویس
---
# قصیدهٔ شمارهٔ ۵۸ - در مدح شیخ زاهد برادر سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>ماهی از برج شرف زاده خورشید کمال </p></div>
<div class="m2"><p>زاده الله جمالاً به جهان داد جمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلبن (انبته الله نباتاً حسنا) </p></div>
<div class="m2"><p>بر دمانید سپهر از چمن جاه و جلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز آدینه نه از ماه ربیع الاخر </p></div>
<div class="m2"><p>رفته از عهد عرب هفتصدو پنجاه و سه سال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیخ زاهد شه فرخنده پی آمد به وجود</p></div>
<div class="m2"><p>شد جهان از اثر طالع او فرخ فال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی خواب گهش در ازل آراسته اند </p></div>
<div class="m2"><p>مهد فیروزه افلاک به انواع لال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حضرتش مجد جلال است و ببینی روزی </p></div>
<div class="m2"><p>بسته خود را فلک پیرو برو چون اطفال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در هوای شرف طالعش از گشت فلک </p></div>
<div class="m2"><p>سر کشیدست کنون سنبله بر اوج کمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا کند زهره نثار قدم میمونش </p></div>
<div class="m2"><p>در انجم به ترازو کشد از بیت المال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اژدهای علم عزم ورا بهر عدو </p></div>
<div class="m2"><p>عقرب از پیش دوان نیش اجل در دنبال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مشتری خانه قوسش زره ملکیت داد</p></div>
<div class="m2"><p>و بنوشت ز ایوان قضاتیرمثال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جدی کان خانه عیش و طرب اولاد است </p></div>
<div class="m2"><p>زحل آراست به پیرایه عز و اقبال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا غبار مرض و خوف نشاند زرهش </p></div>
<div class="m2"><p>می کشد چرخ به دلو از یم کوثر سلسال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>برج هوتش که شد آن خانه زوج و شرکاء </p></div>
<div class="m2"><p>چون جمش مملکتی داد بلا شرک و مثال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هشتمین خانه او داشت امیر هفتم </p></div>
<div class="m2"><p>تا در خوف و خطر را ندهد هیچ مجال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نهمین خانه علم است و در و پیر و زحل </p></div>
<div class="m2"><p>همچو طفلان شده ساکن ز پی کسب کمال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>حصه مملکت و سلطنت جوزا شد </p></div>
<div class="m2"><p>وندرو زهره و مریخ و عطارد عمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مهر و برجیس و معالراس به برج سرطان </p></div>
<div class="m2"><p>رفته کان باب نجاح است ومال آمال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اسدش خانه اعداد و به خون اعدا کرده </p></div>
<div class="m2"><p>چون کف خضیب است و مخضب چنگال </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باش تا غنچه این روضه دماند گل بخت </p></div>
<div class="m2"><p>باش تا طایر این بیضه درآرد پر و بال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شود انگشت نمای همه عالم چو هلال</p></div>
<div class="m2"><p>باش تا کنگره افسر گردون سایش </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باش تا باز کند چتر همایونش پر </p></div>
<div class="m2"><p>عالمی بینی در سایه اوفارغ بال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از پی تهنیت آیند ملایک چو ملوک </p></div>
<div class="m2"><p>به در خسرو اعظم ز سر استقبال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>داور دور زمان شیخ حسن آنکه به تیغ </p></div>
<div class="m2"><p>فتنه را می کند از روی زمین استیصال </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در خوی از غیرت فیض کرمش روی سحاب </p></div>
<div class="m2"><p>در گل از طیره قدمش آب زلال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای زبحر کرمت چشمه خورشید سراب </p></div>
<div class="m2"><p>وی زتاب غضبت آتش مریخ زگال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اثر کوثر شمشیر تو در روز اجل </p></div>
<div class="m2"><p>صدمه نعل سم اسب تو درگاه جلال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>خون کند نقطه امطار در ارحام صدف </p></div>
<div class="m2"><p>بشکند مهره احجار در اصلاب جبال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>گرد خیل تو چون از روی زمین برخیزد </p></div>
<div class="m2"><p>آسمانش کند از مرکز خویش استقبال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اثر عدل تو دان اینک بر اطراف افق </p></div>
<div class="m2"><p>در دم گرگ رود آهوی زرین تمثال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در مقامی که نهد خنگ فلک سیر تو نعل </p></div>
<div class="m2"><p>ماه نو جای ندارد به جز از صف نعال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>خسروا داد کن و شکر به شکرانه آنک </p></div>
<div class="m2"><p>همه چیزی به تو داده است خدای متعال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فسحت مملکت وکامرواییو خدم </p></div>
<div class="m2"><p>رونق سلطنت و جاه و جوانی و جمال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در مقامی که نهد خنگ فلک سیر تو نعل </p></div>
<div class="m2"><p>ماه نو جای ندارد به جز از صف نعال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>خسروا داد کن و شکر به شکرانه آنک </p></div>
<div class="m2"><p>همه چیزی به تو داده است خدای متعال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فسحت مملکت وکامرواییو خدم </p></div>
<div class="m2"><p>رونق سلطنت و جاه و جوانی و جمال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وین دو نوباوه عزو شرف و جاه که هست </p></div>
<div class="m2"><p>عالمی شان ز جلال آمده در تحت ظلال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اینت اسکندر گیتی زره استعداد </p></div>
<div class="m2"><p>وانت کیخسرو ثانی ز سر استقلال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ثالث این عیسی فرخ قدم میمون فر </p></div>
<div class="m2"><p>کامد از رابطه ثانیه در مهد جلال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>پادشاهی است مطیع تو که هستند</p></div>
<div class="m2"><p>امروزپادشاهان جهانش همه ممنون نوال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>شاه دلشاد جوانبخت که در روی زمین </p></div>
<div class="m2"><p>با همه دیده ندیدش فلک پیر مثال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>آنکه رضوان به سرو دیده کشد روی بهشت </p></div>
<div class="m2"><p>خاک پایش ز پی سرمه ارباب حجال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خاتم مملکت جم نشدی ضایع اگر </p></div>
<div class="m2"><p>بودی آراسته بلقیس بدین خوی و خصال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ای به توشیح ثنای تو مرشح اوراق </p></div>
<div class="m2"><p>وی به تزیین دعای تو مزین اقوال</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پایه قدر تو بر فرق زحل زرین تاج </p></div>
<div class="m2"><p>سایه چتر تو بر روی ظفر مشکین خال</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>نیل گردون شده بر چهره اقبال تو لام </p></div>
<div class="m2"><p>لام اقبال تو بر عین سعادت شده دال</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>میکشد ذیل کرم عفو تو بر روی گناه </p></div>
<div class="m2"><p>می برد گوی سبق جود تو از پیش سوال</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بی هوایت خرد از الفت سرگشت ملول </p></div>
<div class="m2"><p>بی رضایت بدن از صحت جان یافت ملال</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر دماغ چمن ازخوی تو بویی یابد </p></div>
<div class="m2"><p>بر دل غنچه گل سرد شود باد شمال</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در زمان گوهر تیغ تو آزار حریر </p></div>
<div class="m2"><p>سوزن تیز نیارد که درآرد به خیال</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>با عطای کف تو بخشش آل برمک </p></div>
<div class="m2"><p>مثل لجه دریا بود و لمعه آل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>نور رای تو اگر نامیه را مایه دهد </p></div>
<div class="m2"><p>به جز از عقد ثریا ندهد بار نهال</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>سرورا مدت شش سال تمام است که من </p></div>
<div class="m2"><p>هستم از حلقه به گوشان درت چون اقبال </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>به هواداری درگاه فلک قدر شما </p></div>
<div class="m2"><p>کرده ا ترک دیار و وطن و مال و منال</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>بعد ازآن کز صدف مدح شما خاطر من </p></div>
<div class="m2"><p>گرد اطراف جهان را زگهر مالا مال</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>قرب سی سال به نیکو سخنی در عالم </p></div>
<div class="m2"><p>شده مشهور شدم جاهل و بدگو امسال</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>هنر آمد شرف مردم و از طالع بد </p></div>
<div class="m2"><p>هنر من همه شد عیب و شرف گشت وبال</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>من چه بر بسته ام از لولو لالای سخن </p></div>
<div class="m2"><p>کاش چون لاله زبان سخنم بودی لال </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بسته نظم دلاویز شدم همچو صدف </p></div>
<div class="m2"><p>خسته نافه مشکین خودم همچو غزال </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>نبود هجو به جز کار خسیسی طامع </p></div>
<div class="m2"><p>نبود هزل به جز کار سفیهی هزال</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>من که امروز کمال سخنم تا حدی است </p></div>
<div class="m2"><p>که عطارد کند از خاطر من استکمال</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به چنین شغل کنم قصد زهی قصد و غرض </p></div>
<div class="m2"><p>به چنین فکر کنم میل زهی فکر محال</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>خود به یکبارگی از پای درآورد مرا </p></div>
<div class="m2"><p>غم درویشی و بیماری و تیمار عیال</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>سفره وارم فلک افکند و من حلقه به گوش </p></div>
<div class="m2"><p>می کنم خدمت شاه از بن دندان چو خلال</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>سالها رفت که من می کنم این ناله و کس </p></div>
<div class="m2"><p>نرسانید به من هیچ نوایی ز منال</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>تا برآید به چمن ناله زار از صلصل </p></div>
<div class="m2"><p>تا که باشد به جهان طینت خلق از صلصال</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>تا ابد طینت ذات تو مبیناد خلل </p></div>
<div class="m2"><p>جاودان سایه جاهت مپذیرد زوال</p></div></div>