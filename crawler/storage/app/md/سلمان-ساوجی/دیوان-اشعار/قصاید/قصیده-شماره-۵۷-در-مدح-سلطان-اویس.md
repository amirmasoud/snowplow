---
title: >-
    قصیدهٔ شمارهٔ ۵۷ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۵۷ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>عید من آنکه هست خم ابرویش هلال </p></div>
<div class="m2"><p>بر عین عید ابروی چون نون اوست دال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیدی که قدر اوست فزون از هزار ماه </p></div>
<div class="m2"><p>ماهی که مثل او نبود در هزار سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش می خرامد ز بن گوش می کشد </p></div>
<div class="m2"><p>هر دم به دوش غالیه زلف او شمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا خود خیال ابروی اوبست ماه نو</p></div>
<div class="m2"><p>کج می نمود در نظر مردم این خیال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هندوی اوست هر سرمه از آن جهان </p></div>
<div class="m2"><p>می گویدش (مبارک)و می خواندش هلال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طالع شوای خجسته مه نو که عالمی است </p></div>
<div class="m2"><p>بی عید طلعت تو همه روزه در ملال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لعلت به خنده می شکند حقه عقیق </p></div>
<div class="m2"><p>چشمم به گریه می گسلد رشته لال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با چشم مست گو که میدان چو می بریز </p></div>
<div class="m2"><p>خون مرا مگو که حرامست یا حلال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو گان زلفت آنکه به میدان دلبری </p></div>
<div class="m2"><p>سر جز به گوی ماه درآرد بود محال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کم می کنم حدیث دهان تو چون می کنم </p></div>
<div class="m2"><p>کانجا سخن نمی رود از تنگی مجال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رویت گل دو روی به یک روی چون ندید </p></div>
<div class="m2"><p>صد بار زرد و سرخ برآمد زانفعال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>با توست گوییا نظر آفتاب ملک </p></div>
<div class="m2"><p>کامد چو ماه عید مبارک رخت به فال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خورشید صبح سنجق و ماه زحل محل </p></div>
<div class="m2"><p>دارای چرخ کوکبه مشتری خصال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سلطان معزدین خدا پادشه اویس </p></div>
<div class="m2"><p>سلطان بی عدیل و شهنشاه بی مثال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>شاهی که ظل مرکز چتر جلال اوست </p></div>
<div class="m2"><p>دوران هفت دایره را نقطه کمال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شاهی که زیر شهپر شاهین دولتش </p></div>
<div class="m2"><p>خوش خفته است کبک دری با فراغ بال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای گشته مالکان همه ملوک ملک تو </p></div>
<div class="m2"><p>وی مال کان زکف دست تو پایمال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تقدیر داده تا ابدت بخت (لاینام)</p></div>
<div class="m2"><p>ایزد سپرده در ازلت ملک (لایزال)</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مهرست و ماه رای زرینتوراغلام </p></div>
<div class="m2"><p>کان است و بحر طبع جواد تو را عیال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>آفاق راست بحر کفت منشا کرم </p></div>
<div class="m2"><p>افلاک راست خاک درت مسند جلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>امر تو مرکبان زمین را کند روان </p></div>
<div class="m2"><p>نهی تو بختیان فلک را نهد عقال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن خلق خلق توست که ده تو ز غیرتش </p></div>
<div class="m2"><p>خون بسته است در جگر نافه غزال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>وان لطف لطف توست که در عین سلسبیل </p></div>
<div class="m2"><p>بر روی کف می زند از طره اش زلال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وان قهر قهر توست که از باد هیبتش </p></div>
<div class="m2"><p>آب نبات زهر شود در عروق بال </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>وان گرز گرز توست که بدخواه را کند </p></div>
<div class="m2"><p>پیدا میان دو کتفش فرق در جدال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بر کوه جامد ار گذرد با هیبتت </p></div>
<div class="m2"><p>گردند چون سحاب روان در هوا جبال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مریخ را بدل شمرد زهره بعد از ین </p></div>
<div class="m2"><p>با ماه رایت تو اگر یابد اتصال </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>مه خواست تا به سم سمندت رسد مگر </p></div>
<div class="m2"><p>خود را بر او ببندد اگر دارد احتمال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آنجا که خنگ ماه منیر تو سم نهد </p></div>
<div class="m2"><p>ماه نو افتاده بود در صف نعا ل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>ظل ظلیل چتر تو و خوی پرچمت </p></div>
<div class="m2"><p>رخسار نو عروس ظفر راست زلف و خال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر التجا کند به تو خورشید خاوری </p></div>
<div class="m2"><p>دیگر به نیم روز نبیند کسش زوال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چرخ دوال باز اگر سر کشی کند </p></div>
<div class="m2"><p>امرت کشد به جرم زجرم اسد دوال </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بد خواه را چه زهره که گردد معارضت ؟ </p></div>
<div class="m2"><p>با شیر خود چه پنجه تواند زدن شغال </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دست سوال پیش تو سایل چه آورد </p></div>
<div class="m2"><p>چون هست پیش دست عطا تو بر سوال </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جود تو منع کرد ترا زو از آن شدست </p></div>
<div class="m2"><p>میزان درست مغربی مهر را زوال </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شا ها بدان خدای که از خوان نعمتش </p></div>
<div class="m2"><p>دنیاست یک نواله وعقبی است یک نوال !</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کا مروز در جمیع ممالک منم که نیست </p></div>
<div class="m2"><p>جز فکر مدحت تو مرا هیچ اشتغال </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از صبح تا به شام دعای تو می کنم </p></div>
<div class="m2"><p>بی آنکه باشدم طمع جا ه وحرص مال </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ورنه به دولتت چو دگر بندگان تو </p></div>
<div class="m2"><p>من بنده نیز داشتمی منصب و منال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>بر غیر حضرت تو حرام است شعر من </p></div>
<div class="m2"><p>کان سحر مطلق است بهر مذهبی حلال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تا در طباع آتش و آب است اختلاف </p></div>
<div class="m2"><p>تا در مزاج باد بهاریست اعتدال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>بادا حدود ملک تو ایمن ز اختلاف </p></div>
<div class="m2"><p>بادا مزاج امر تو خالی ز اختلال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>فرخنده باد بر تو شب قدر و روز عید </p></div>
<div class="m2"><p>پشت و پناه و قدر جلال تو ذوالجلال</p></div></div>