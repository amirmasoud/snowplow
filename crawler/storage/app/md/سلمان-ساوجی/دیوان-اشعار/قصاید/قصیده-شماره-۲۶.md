---
title: >-
    قصیدهٔ شمارهٔ ۲۶
---
# قصیدهٔ شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>از تکسر، اگرش طره به هم بر شده است </p></div>
<div class="m2"><p>عارضش باری ازین عارضه خوشتر شده است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشتش آینه گردی و کنون روشن شد </p></div>
<div class="m2"><p>که به آه دل عشاق منور شده است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لبت شربت قند ار چه رسیدست به کام </p></div>
<div class="m2"><p>شکر از شرم دهانت به عرق تر شده است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طبیب از دهن یار به عطار بگوی </p></div>
<div class="m2"><p>برمکش قند گران را که مکرر شده است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شربتی ساز مفرح دل بیمار مرا </p></div>
<div class="m2"><p>زان دو یاقوت که پرورده به شکر شده است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌دهد لعل توام ساده جوابی لیکن </p></div>
<div class="m2"><p>چشم بیمار تو مایل به مزور شده است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صبح برخاست به بوی تو صبا پنداری </p></div>
<div class="m2"><p>که ز بیماری دوشینه سبکتر شده است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر کجا کرده گذر بر سر زلفت بادی </p></div>
<div class="m2"><p>روز من چون شب تاریک مکدر شده است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر سر من برود عشقت از این سر نرود </p></div>
<div class="m2"><p>زانکه سرمایه عشق تو درین سر شده است </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چشم بیمار تو از دیده من کرد هوس </p></div>
<div class="m2"><p>ناردانی که بدین گونه مزعفر شده است </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا دگر کی به لب جام لبت باز خورد </p></div>
<div class="m2"><p>ای سبا خون که ز غم در دل ساغر شده است </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بعد ازین غم مخور ای دل که غم امروز همه </p></div>
<div class="m2"><p>روزی دشمن دارای مظفر شده است </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سایه لطف خدا شاه، اویس، آنکه به حق </p></div>
<div class="m2"><p>پادشاهان جهان را سر و افسر شده است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه در منصب شاهی، شرف و مرتبتش </p></div>
<div class="m2"><p>ناسخ سلطنت طغرل و سنجر شده است </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کلک او نقش قدر را سر پرگار آمد </p></div>
<div class="m2"><p>رای او کلک قضا را خط مستر شده است </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فکر تیغش اگر آورده اسد در خاطر </p></div>
<div class="m2"><p>اسد از تیزی آن فکر دو پیکر شده است </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا خورد در ظلمات دل خصم آب حیات </p></div>
<div class="m2"><p>تیغ بزش چو خضر یار سکندر شده است </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای جهان گیر جهان بخش که از حکم ازل </p></div>
<div class="m2"><p>سلطنت تا به ابد بر تو مقرر شده است </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مار رمحت به سنان، مهره شکاف آمده است </p></div>
<div class="m2"><p>شیر را یات تو در معرکه صفدر شده است </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مژه بر دیده بدخواه تو پیکان گشته </p></div>
<div class="m2"><p>آب در حنجره خصم تو خنجر شده است </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>روشن است آنکه تو خورشیدی از آن روی جهان </p></div>
<div class="m2"><p>شرق تا غرب به تیغ تو مسخر شده است </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گرگ با عدل تو همراز شبان آمده است </p></div>
<div class="m2"><p>باز با داد تو انباز کبوتر شده است </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>کرد گردون به دلت نسبت دریای عدن </p></div>
<div class="m2"><p>لاجرم زاده طبعش همه گوهر شده است </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نجم در قبضه شمشیر تو کوکب گشته </p></div>
<div class="m2"><p>چرخ بر قبه خرگاه تو چنبر شده است </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عقل را پیروی رای تو می‌باید کرد </p></div>
<div class="m2"><p>در دماغ خرد این فکر مصور شده است </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>طاعت فکر تو در خود ننهاست فلک </p></div>
<div class="m2"><p>در نهاد فلک این وضع مخمر شده است </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ذره از عون تو با مهر مقابل گشته</p></div>
<div class="m2"><p>زر به دوران تو با سنگ برابر شده است </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که از نام تو بر لوح جبین کرد نشان </p></div>
<div class="m2"><p>کار و بارش بدرستی همه با زر شده است </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>وانکه از سایه اقبال تو برتافته روی </p></div>
<div class="m2"><p>شده سرگشته تراز ذره و در خور شده است </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>خسروا از سبب عارضه یک شبه‌ات </p></div>
<div class="m2"><p>چه خرابی که درین خانه ششدر شده است </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>یارب آن شب چه شبی بود که گفتی سحرش </p></div>
<div class="m2"><p>میخ چشم مه و قفل در خاور شده است؟</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بس که از سوز دعای ملک و ناله ملک </p></div>
<div class="m2"><p>اشک انجم به کنار فلک اندر شده است </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گنبد سبز فلک گنبد گل را ماند </p></div>
<div class="m2"><p>بس که از مجمر انفاس معطر شده است </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دست در دامن آهم زده این جان عزیز </p></div>
<div class="m2"><p>با دعایت ز لب من به فلک بر شده است </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>صبح بهر تو دعای خواند و دمید </p></div>
<div class="m2"><p>با دعای سحر این فتح میسر شده است </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>جان ملکی و سر مملکتی، ملک بدین </p></div>
<div class="m2"><p>در گمان بود کنونش همه باور شده است </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>شکر این موهبت و نعمت این صحت را </p></div>
<div class="m2"><p>با زبان قلم و تیغ سخنور شده است </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا دل نار و رخ شهره آبی به شهور </p></div>
<div class="m2"><p>خاکی و آتشی از آب و ز آذر شده است </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خاک و آب تو ز آفات جهان باد مصون </p></div>
<div class="m2"><p>کاب در حلق بد اندیش تو آذر شده است </p></div></div>