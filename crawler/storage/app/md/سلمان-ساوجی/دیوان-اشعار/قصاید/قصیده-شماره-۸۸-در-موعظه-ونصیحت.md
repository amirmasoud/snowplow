---
title: >-
    قصیدهٔ شمارهٔ ۸۸ - در موعظه ونصیحت
---
# قصیدهٔ شمارهٔ ۸۸ - در موعظه ونصیحت

<div class="b" id="bn1"><div class="m1"><p>زحبس نفس خلاص ای عزیز اگر یابی </p></div>
<div class="m2"><p>سریر سلطنتت مصر جان مقر یابی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از این خرابه کنگر مقام اگر ببری </p></div>
<div class="m2"><p>فراز کنگره یعرش مستقر یابی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر به چشم تامل به خاک در نگری </p></div>
<div class="m2"><p>به زیر پای خود اندر هزار سر یابی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمال قدر وشرف می کنی طلب چون ما </p></div>
<div class="m2"><p>منازلی که تو می جویی از سفر یابی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز خود سفر کن اگر نعمت ابد طلبی </p></div>
<div class="m2"><p>که در چنین سفر آن سفره ی ما حضر یابی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو مرغ بی پری از بال نیست خبری </p></div>
<div class="m2"><p>به بال کن طیران تازه بال پر یابی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به زیر تیغ چو کوهی نشسته تا باشد </p></div>
<div class="m2"><p>که سنگ پاره ای از لعل بر کمر یابی </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بدان قدر که بیابی زرزق راضی شو </p></div>
<div class="m2"><p>چو بیش و کم همه در قبضه ی قدر یابی </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل است کعبه ی عرفان کعبه ی دل را </p></div>
<div class="m2"><p>دراز صفات توسعی بکن که در یابی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به بوی دوست سحر خیز شو چو باد صبا </p></div>
<div class="m2"><p>که بوی دوست زمشکین دم سحر یابی </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو مشکو عود عزیزی نفس وطیب نفس </p></div>
<div class="m2"><p>بسوز سینه و خونا به ی جگر یابی </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ندیم مجلس کروبیان قدس شوی </p></div>
<div class="m2"><p>زشر نفس خلاصی بجوی اگر یابی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خلوت حرم دوست آن زمان برسی </p></div>
<div class="m2"><p>کزین ده و دو درونه تتق گذر یابی </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دل شکسته چو یاقوت شاد کن وانگه </p></div>
<div class="m2"><p>به عهده یمن از آتش اگر ضرر یابی </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر نه بر دل کوه است خاری از درون </p></div>
<div class="m2"><p>فسرده خون زچه در سینه یحجر یابی </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زغصه بر جگر بحر نیز داغی هست </p></div>
<div class="m2"><p>وگرنه از چه لبش خشک وچشم تریابی </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز چشمت ار سبل ریب عیب بر خیزد </p></div>
<div class="m2"><p>سرایر حجب غیب در نظر یابی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خواص خاص زعامی مجو که ممکن نیست </p></div>
<div class="m2"><p>که آنچه در دل بحر است در شمر یابی </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>برای مصلحتی پادشاه گردون را </p></div>
<div class="m2"><p>گهی به خاوران و گاهی به باختر یابی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>سپهر با عظمت را که بسته اند کمر </p></div>
<div class="m2"><p>برای خدمت اولاد بو البشر یابی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو در مزراغ دنیا چو تخم بد کاری </p></div>
<div class="m2"><p>در آخرت هم ازین جنس بارو بر یابی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دو تویی فقرا جامه ایست کز عظمت </p></div>
<div class="m2"><p>هزار میخی افلاکش استر یابی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ندارد ان شرف و اعتبار دینی درون </p></div>
<div class="m2"><p>که خویش را تو بدان چیز معتبر یابی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ببخش مال و نترس از کمی که هر چه دهی</p></div>
<div class="m2"><p>جزای آن به یک ده ز داد گر یابی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تو همچو منبع ماهی به عینه چندانی </p></div>
<div class="m2"><p>که بیشتر بدهی فیض بیشتر یابی </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو غنچه خانه پر از برگ ودایمی دلتنگ </p></div>
<div class="m2"><p>که کی ز باد هوا خرده ای ز زر یابی </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مقرر است نصیب ار هزار سعی کنی </p></div>
<div class="m2"><p>هر آنچه هست مقدر همان قدر یابی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو نرگست همگی چشم بر زر و سیم است </p></div>
<div class="m2"><p>نظر به زر نکنی هیچ اگر بصر یابی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مکن ملامت دنیا که سست بنیاد است </p></div>
<div class="m2"><p>کزین سرای دو در خلد هشت در یابی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>جلیس او شوی آنگه که چشم و گوشی را </p></div>
<div class="m2"><p>کز آن جمال و مقال حبیب در یابی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو گاو و چشم ز دیدار عیب سازی کور </p></div>
<div class="m2"><p>چو پیل گوش ز گفتار خلق کر یابی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گذر به لاله ستان کن چو باد تا در خاک </p></div>
<div class="m2"><p>غریق خون همه سرهای تا جور یابی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>اگر به نسخه تشریح چشم در نگری </p></div>
<div class="m2"><p>شروح صنع درین جلد مختصر یابی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>گذشت عمر عزیزت به هرزه تا امروز </p></div>
<div class="m2"><p>دلا به کوش که باقی عمر دریابی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تو مردمی ز همه مردمی امید مدار </p></div>
<div class="m2"><p>که این کرم ز نفوس ملک سیر یابی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مباش در دم نحلی که در دمش نوش است </p></div>
<div class="m2"><p>که در دم و دم او نوش نیشتر یابی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ببین که با همه حسن اللقا چه کوتاهست </p></div>
<div class="m2"><p>بقای صبح دوم را که پرده در یابی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ز آه سرد حذر کن که کوه را چون کاه </p></div>
<div class="m2"><p>زباد سینه درویش بر حذر یابی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از مروت نیست پیشش بحر را خواندن سخی </p></div>
<div class="m2"><p>وز سبکباری ست گفتن کوه را نزد ش حلیم </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای عیون اختران از خاک در گاهت کحیل ! </p></div>
<div class="m2"><p>وی جبین آسمان از داغ فر مانت وسیم </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هم به جنب همتت گردون خسیس ومه گد ا </p></div>
<div class="m2"><p>هم به خیل حشمتت دریا بخیل وکان لئیم </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سفره ی افلا ک را رای تو بخشد قرص چاشت </p></div>
<div class="m2"><p>ابلق ایام را جودت دهد وجه فضیم </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>می کند ثابت به برهان های قاطع تیغ تو </p></div>
<div class="m2"><p>کوشهاب ثاقب است وخصم شیطان رجیم </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>در میان روز وشب گر تیغ تو سدی کشد </p></div>
<div class="m2"><p>خیل شب زان پس نیارد سر بر آوردن زبیم </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کعبه درگاه توست اندر مقامی کاسمان </p></div>
<div class="m2"><p>بسته احرام عبادت گرددش گرد حریم </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خویشتن را دشمنت بر تیغ دولت می زند </p></div>
<div class="m2"><p>لاجرم پروانه سان می سوزد از تاب الیم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از در اصحاب دولت می توان گشت آدمی </p></div>
<div class="m2"><p>یافت از اقبال ایشان پایه ی انسان رقیم </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ای عدو در زیر شیر رایت او شد که هیچ </p></div>
<div class="m2"><p>در نمی گیرد سگی وروبهی با این غنیم!</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>با قضا حیلت چه آرزد زانکه در روز اجل </p></div>
<div class="m2"><p>عاجز است از دفع دشمن سوزن چو موی سیم </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خصم بالین سلامت را کجا بیند به خواب </p></div>
<div class="m2"><p>زا نکه آن سر کش زیادت می کشد پا از گلیم </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>پادشا ها در بهار دولتت من بی نوا </p></div>
<div class="m2"><p>هستم آن بلبل که چون عنقاست مثل من عدیم </p></div></div>
<div class="b" id="bn52"><div class="m1"><p>گر چه بیمار است طبعم قوتی دارد سخن </p></div>
<div class="m2"><p>ورچه باریک است معنی دارم الفاضی جسیم </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر به دست دیگری آرم سخن عیبم مکن </p></div>
<div class="m2"><p>زان سبب کز دست خویشم در عذابی بس الیم </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا ندید گل بود هر سال بلبل در بهار </p></div>
<div class="m2"><p>در بهار کامرانی دولتت بادا ندیم !</p></div></div>