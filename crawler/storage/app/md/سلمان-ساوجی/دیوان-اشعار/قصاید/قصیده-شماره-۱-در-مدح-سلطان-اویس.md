---
title: >-
    قصیدهٔ شمارهٔ ۱ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۱ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>ای غبار موکبت چشم فلک را توتیا</p></div>
<div class="m2"><p>خیر مقدم، مرحبا «اهلاً و سهلاً» مرحبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رایت رایت، به پیروزی چو چتر آفتاب</p></div>
<div class="m2"><p>سایه بر ربع ربیع انداخت از «بیت الشتا»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز چتر سایه‌ بر نسرین چرخ انداخته</p></div>
<div class="m2"><p>فرخ و میمون شده، فی ظله بال هما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابت در رکاب، و مشتری در کوکبه</p></div>
<div class="m2"><p>آسمان زیر علم، ماه علم خورشید سا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با غبار نعل شبذیر تو می‌ارزد کنون</p></div>
<div class="m2"><p>خاک آذربایجان، مشگ ختن را خون بها</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شهر تبریز از قدوم موکب سلطان اویس</p></div>
<div class="m2"><p>چون مقام مکه از پیغمبر آمد با صفا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این بشارت در چمن هر دم که می‌آرد نسیم</p></div>
<div class="m2"><p>می‌نهد اشجار سرها بر زمین، شکرانه را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌نهد بر خوان دولتخانه گل صد گونه برگ</p></div>
<div class="m2"><p>می‌زند بر روی مهر آن رود بلبل صد نوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای ز فیض خاطرات آب سخن کوثر ذهاب!</p></div>
<div class="m2"><p>وی ز ابر همتت باغ امل طوبی نما!</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سایه لطف خدایی، تا جهان پاینده است</p></div>
<div class="m2"><p>بر جهان پاینده باد این سایه لطف خدا!</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ملک لطفت راست آن نعمت که در ایران زمین</p></div>
<div class="m2"><p>عطف ذیل عاطفت می‌گستراند بر خطا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>وصف لطفت در چمن می‌کرد ابر نوبهار</p></div>
<div class="m2"><p>سوسن و گل را عرق بر چهره افتاد از حیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در افق مهر از نهیبت روی تابد، ور به کین</p></div>
<div class="m2"><p>بازگردانی افق را نیز ننماید قفا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دور رای استوارت کافتابش نقطه‌ایست</p></div>
<div class="m2"><p>در کشید از استقامت، خط به خط استوا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>غنچه‌ای بودی به نسبت بر درخت همتت</p></div>
<div class="m2"><p>گنبد نیلوفری گرداشتی رنگ و نما</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رایت عزم شریفت دولتی بی‌انقلاب</p></div>
<div class="m2"><p>سده قدر رفیعت سدره بی‌منتها</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در نهاد آب شمشیرت قضای مبرم است</p></div>
<div class="m2"><p>بر سر شوم عدویت خواهد آمد این قضا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در شب هیجا سپاه فتح را تیغت دلیل</p></div>
<div class="m2"><p>در ره تدبیر، پیر عقل را کلکت عصا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آفتاب از عکس شمشیر تو می‌گیرد فروغ</p></div>
<div class="m2"><p>آسمان از بار احسان تو می‌گردد دو تا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در جهانداری، دو آیت داری از تیغ و قلم</p></div>
<div class="m2"><p>کاسمان خواند همی آن را صبا، این را مسا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گردی از کهل سپاهت بر فلک رفت، آفتاب</p></div>
<div class="m2"><p>کردش استقبال و گفت: ای روشنایی مرحبا!</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ابر اگر آموزد از طبع تو رسم مردمی</p></div>
<div class="m2"><p>در زمین دیگر نرویاند به جز مردم گیا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>پیش چترت آن مقدم بر سمات اندر سمو</p></div>
<div class="m2"><p>جبهه و اکلیل را بر ارض می‌ساید، سما</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اطلسی بر قد قدرت در ازل می‌دوختند</p></div>
<div class="m2"><p>وصله‌ای افتاد از آن اطلس، فلک را شد قبا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صد ره ار با صخره صما کند امرت خطاب</p></div>
<div class="m2"><p>جز «سمعنا و اطعنا» نشنود سمع از صدا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر کجا تیغت همی گرید، همی خندد اجل</p></div>
<div class="m2"><p>هر کجا کلکت همی نالد، همی نالد سخا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تا شبانگاه امل می‌گردد ایمن از زوال</p></div>
<div class="m2"><p>گر به چترت می‌کند چون سایه خورشید التجا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>طبع گیتی راست شد در عهد تو ز انسان که باز</p></div>
<div class="m2"><p>نشنود صوت مخالف هیچکس زین چار تا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کاهی از ملکت نیارد برد خصمت، گرچه گشت</p></div>
<div class="m2"><p>از نهیب تیغ مینایین، چو رنگ کهربا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دشمنت بیمار و شمشیرت طبیب حاذق است</p></div>
<div class="m2"><p>بر سرش می‌آید و می‌سازدش در دم دوا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هر که رو بر در گهت بنماد کارش شد چو زر</p></div>
<div class="m2"><p>خاک درگاهت مگر دارد خواص کیمیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هرکه چون دل در درون دارد هوای حضرتت</p></div>
<div class="m2"><p>در یسارست او همه وقتی و دارد صدر جا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هست مستغنی، بحمد الله، ز اعوان درگهت</p></div>
<div class="m2"><p>گر به درگاهت نیاید شوربختی، گو: میا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تیره باد آن روز و سال و مه که دارد بر سپهر</p></div>
<div class="m2"><p>چشمه خورشید چشم روشنایی از سها</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خویش را بیگانه می‌دارد ز مدحت طبع من</p></div>
<div class="m2"><p>زآنکه دریای زاخر نیست جای آشنا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چون ز تقدیر بیانت عاجز آمد طبع من </p></div>
<div class="m2"><p>این غزل سر زد درون دل، در اثنای ثنا </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در فراقت گرچه بگذشت آب چشم از سر مرا </p></div>
<div class="m2"><p>بر زبان هرگز نراندم سرگذشت و ماجرا </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شمع وارم، روزگار از جان شیرین دور کرد </p></div>
<div class="m2"><p>باز دارد آنگه به دست دشمنم سر رشته را</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا مگر وصل تو یکدم وصله کارم شود </p></div>
<div class="m2"><p>در فراقت پیرهن را ساختم در بر قبا </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>من به بویت کرده‌ام با باد خو در همرهی </p></div>
<div class="m2"><p>لاجرم بی باد یک دم بر نمی‌آید مرا </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هست دایی بی‌دوا در جان من از عشق تو </p></div>
<div class="m2"><p>بود و خواهد بود بر جان من این غم دائما </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در میان چشم و دل گردی است دور از روی تو </p></div>
<div class="m2"><p>خیز و بنشین در میان هر دو، بشنو ماجرا </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خاصه این ساعت که دلها را صفایی حاصل است </p></div>
<div class="m2"><p>از غبار موکب جمشید افریدون لقا </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>آن جهانگیری، جهانداری، جهان بخشی که هست </p></div>
<div class="m2"><p>تیغ و کلک او جهان را مایه خوف و رجا </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>دولتت چون آفتاب و نور و کوه و سایه‌اند </p></div>
<div class="m2"><p>آفتاب از نور و کوه از سایه چون گردد جدا </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>پادشاها هشت مه نزدیک شد تا کرده‌است </p></div>
<div class="m2"><p>دور از آن حضرت، بلای درد پایم مبتلا </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>درد پای ماست همچون ما، به غایت پایدار</p></div>
<div class="m2"><p>در ثبات و پایداری درد آرد پای ما </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>نی که پایم پای بر جا تر ز درد آمد که درد </p></div>
<div class="m2"><p>هر زمان می‌جنبد و پایم نمی‌جنبد ز جا </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>شرح این درد مفاصل را مفصل چون کنم؟</p></div>
<div class="m2"><p>کی شود ممکن به شرح این قیام آنگه مرا </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ضعف پایم کرد چون نرگس چنان کز عین ضعف</p></div>
<div class="m2"><p>سرنگون بر پای می‌خیزم به یاری عصا </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>درد پایم کرد منع از خاک بوس درگهت </p></div>
<div class="m2"><p>خاک بر سر می‌کنم هر ساعتی از درد پا </p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اندرین مدت که بود از درد غم صباح من عشا </p></div>
<div class="m2"><p>گفته‌ام حقا دعایت، در صباح و در مسا </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>مرکبی از روشنی نگذشت بر من تا که من </p></div>
<div class="m2"><p>همره ایشان نکردم کاروانی از دعا </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>تا چو باد نوبهاری مژده گل می‌دهد </p></div>
<div class="m2"><p>لاله می‌اندازد از شادی کله را بر هوا </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هم هوا گردد چو چشم عاشقان گوهر فشان </p></div>
<div class="m2"><p>هم زمین باشد چو صحن آسمان انجم نما </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گل گشاید سفره پر برگ بهر عندلیب </p></div>
<div class="m2"><p>صبح خیزان را زند بر سفره گلبانگ صلا </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تاج نرگس را بیاراید به زر هر شب، سحاب </p></div>
<div class="m2"><p>آتش گل را بر افروزد به دم هر دم صبا </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>روضه عمرت که هست آن ملکت باغ بهار </p></div>
<div class="m2"><p>باد چون دارالبقا آسوده از باد فنا </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>عالم فرسوده از جور سپهر آسوده باد </p></div>
<div class="m2"><p>جاودان در سایه این رایت گیتی گشا </p></div></div>
<div class="b" id="bn60"><div class="m1"><p>باد ماه روزه‌ات میمون و هر ساعت زنو </p></div>
<div class="m2"><p>ابتدای دولتی کان را نباشد انتها </p></div></div>