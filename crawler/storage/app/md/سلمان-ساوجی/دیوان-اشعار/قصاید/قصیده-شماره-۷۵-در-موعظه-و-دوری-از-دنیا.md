---
title: >-
    قصیدهٔ شمارهٔ ۷۵ - در موعظه و دوری از دنیا 
---
# قصیدهٔ شمارهٔ ۷۵ - در موعظه و دوری از دنیا 

<div class="b" id="bn1"><div class="m1"><p>ای دل آخر یک قدم بیرون خرام از خویشتن </p></div>
<div class="m2"><p>آشنا شو با روان بیگانه شو از خویشتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی ننماید هلال مطلع عین الیقین </p></div>
<div class="m2"><p>تا هوای ملک جان تاریک دارد گرد ظن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عین انسانیتی خواهی که ظاهر گردت ؟ </p></div>
<div class="m2"><p>چهره پنهان داد چون انسان عین از خویشتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدمی را آن زمان آرایش دین بر کنند </p></div>
<div class="m2"><p>کا دمش زآلایش طین پا گرداندبدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون زنی پیر از دنیا کهنه چرخی در کنار </p></div>
<div class="m2"><p>گر جوانمردی چه گردی چرخ پیرزن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لاف ردی می زنی با چرخ گردانت چه کار ؟ </p></div>
<div class="m2"><p>رشته پیوند بگسل چرخ را بر هم شکن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیر زین داری براق آخر چه خسبی در گلیم </p></div>
<div class="m2"><p>زیر ران داری نجیب ؟ آخر چه پایی در عطن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دار دنیا را به دین دزدان دین ده چون مسیح </p></div>
<div class="m2"><p>راه دار ملک جان گیر از خراب آباد تن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خیمه جان بر جهانی زن که در صحرای او </p></div>
<div class="m2"><p>لاله زار گلشن خضر است خضرای دمن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در مقام صدق جان باید که باشد درنعیم </p></div>
<div class="m2"><p>جسم خواهی در تنعم باش خواهی در حذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ذات یوسف را به مصر اندر کجا دارد زیان </p></div>
<div class="m2"><p>زان که در کنعان به خون آلوده باشد پیرهن </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به کی در باد خواهی دادن این عمر عزیز؟ </p></div>
<div class="m2"><p>در هوای رنگ و بوی ارغوان یا یا سمن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بس کن این آتش زبانی بس که در پایان چو شمع </p></div>
<div class="m2"><p>خواهدت بر باد دادن سر زبانت بی سخن </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر زبانی کز میان او رسد جان را زیان </p></div>
<div class="m2"><p>شمع وار آن به که سوزد یا بمیرد درلگن </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آبروی هر دو عالم آن زمان حاصل کنی </p></div>
<div class="m2"><p>کز سر اخلاص گردی خاک پای بو الحسن</p></div></div>