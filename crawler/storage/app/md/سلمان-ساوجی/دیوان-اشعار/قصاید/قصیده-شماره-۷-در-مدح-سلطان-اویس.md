---
title: >-
    قصیدهٔ شمارهٔ ۷ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۷ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>زکان سلطنت لعلی سزای تاج شد، پیدا</p></div>
<div class="m2"><p>که لولو با همه لطف از بن گوشش شود، لالا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مهی گشت از افق طالع که پیش طالع سعدش</p></div>
<div class="m2"><p>کمر چون توامان بسته است، خورشید جهان‌آرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قضا تا مهد اطفال فلک را می‌دهد جنبش</p></div>
<div class="m2"><p>نخوابانید ازین ماهی، درین گهواره مینا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قبای اطلس گردون، به قد قدرش اربودی</p></div>
<div class="m2"><p>بریدندی قماط او، ازین نه شفه والا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همایون مقدم این ماه میون فال فرخ پی</p></div>
<div class="m2"><p>مبارک باد! بر سلطان، معز الدین و الدنیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان سلطنت، سلطان اویس این شاه کو دارد</p></div>
<div class="m2"><p>جهان در سایه فرخ همای چتر گردون سا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهنشاهی که در تشریح اعضای بداندیشان</p></div>
<div class="m2"><p>به شرح گوهر پاکش زبان تیغ شد گویا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سحاب همت او گرفکندی بر جهان سایه</p></div>
<div class="m2"><p>زمین را بودی از خورشید و گردون نیز، استغنا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در معراج فکرت رو به منهاج کمال آرد</p></div>
<div class="m2"><p>ملایک دردمند آواز «سبحان الذی اسری»</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز مهرش صبح دم می‌زد دم مرا شد صدق او روشن</p></div>
<div class="m2"><p>که صدق اندرونی را توان دانست از سیما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو در هیجا کمان گیرد چو در مسند قدح خواهد</p></div>
<div class="m2"><p>تو گویی مشتری در قوس و خورشیدست در جوزا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ضمیر پیش بین او روان چون آب می‌خواند</p></div>
<div class="m2"><p>ز لوح چهره امروز نقش صورت فردا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چنان احکام شرعی بر طریق عقل می‌داند</p></div>
<div class="m2"><p>که اندر سر نمی‌آید کمیت خوشرو صهبا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>برای او بود پیوسته میل اختران آری</p></div>
<div class="m2"><p>به سوی کل چو در باشد همیشه جنبش اجزا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زدست دست طبع او شب و روز است، متواری</p></div>
<div class="m2"><p>گهر در قلعه پولاد و زر در خانه خارا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زرای دین پناه او حربا گر خبر یابد</p></div>
<div class="m2"><p>نسازد قبله از خورشید رخشان بعد ازین حربا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>دعای دولتش باشد، جهان راورد پنج ارکان</p></div>
<div class="m2"><p>ثنای حضرتش باشد، فلک را حرز هفت اعضا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دو سلطانند در ملک مروت دست و طبع او</p></div>
<div class="m2"><p>که داد آن ابر را ادرار و راند این بحر را اجرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به عهدش داد گل بر باد مستوری خود زان رو</p></div>
<div class="m2"><p>کشندش بر سر سرباز و ریزندش آبرو، رسوا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا شاهی که تیغ تیز آهن روی رویین تن</p></div>
<div class="m2"><p>نیارد کرد از امر تو سرمویی گذر قطعا!</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تو عین لطفی و دریای اعظم آب مستعمل </p></div>
<div class="m2"><p>تو نور محضی و گردون گردون دود مستعلا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سواد سایه چتر تو نور دیده دولت</p></div>
<div class="m2"><p>غبار نعل شبذیر تو نیل چهره حورا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>جلالت از گریبان سپهر آورد سر بیرون</p></div>
<div class="m2"><p>مانت دامن آخر زمان را می کشد در پا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گذشته روز و شب آب حسامت از سر دشمن</p></div>
<div class="m2"><p>نشسته سال و مه سهم خدنگ بر دل اعدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بساط مجلس عدلت، جهان را ملجا و مرجع</p></div>
<div class="m2"><p>بسیط عالم قدرت، ملک را مولد و منشا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو خیزد شعله تیغت، نشیند آب بر آتش</p></div>
<div class="m2"><p>چو خندد ساغر بزمت، بگرید آب بر دریا</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کجا خیل بداندیشان چو مور و مار شد جوشان</p></div>
<div class="m2"><p>سنانت، آن ید بیضا، نمود از چوب اژدرها</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خرابی می‌شود، ورنه به عون عدل دیندارت</p></div>
<div class="m2"><p>شریعت چار مادر را جدا کردی ز هفت آبا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>الا تا قطه نیسان، که از صلب سحاب افتند</p></div>
<div class="m2"><p>کند در یتیمش در صدف دریای گوهرزا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به یمن همت ذات شریفت، منتظم بادا!</p></div>
<div class="m2"><p>عقود رشته پیوند نسل آدم و حوا</p></div></div>