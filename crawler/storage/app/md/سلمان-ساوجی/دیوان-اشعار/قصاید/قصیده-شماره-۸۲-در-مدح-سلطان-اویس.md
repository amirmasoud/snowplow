---
title: >-
    قصیدهٔ شمارهٔ ۸۲ - در مدح سلطان اویس
---
# قصیدهٔ شمارهٔ ۸۲ - در مدح سلطان اویس

<div class="b" id="bn1"><div class="m1"><p>طراوتی است جهان را به فر فروردین </p></div>
<div class="m2"><p>که هر زمان خجل است اسمان ز روی زمین </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لطف خاک صبا گشت بر هوا غالب </p></div>
<div class="m2"><p>چنان که می چکدش از حیا عرق ز جبین </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک ز قوس و قزح بر هوا کشیده کمان </p></div>
<div class="m2"><p>هوا ز برق جهان بر جهان گشاده کمین </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریر سبز چمن شد شکوفه را بستر </p></div>
<div class="m2"><p>کنار برگ سمن شد بنفشه بالین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا عذاب خوش آید که می زند بر رود </p></div>
<div class="m2"><p>ترانه های دل آویز و صوت های حزین </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درخت میوه را چون شاخ ثور برگ نداشت </p></div>
<div class="m2"><p>چو برج ثور بر اورد زهره و پروین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چمن به است ز چرخ برین به سهیه بید </p></div>
<div class="m2"><p>خلاف نیست بر ان چرخ پیر است برین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مثل نرگس رعنا بعینه گویی </p></div>
<div class="m2"><p>که در چمن به تماشای لاله و نسرین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گذشته اند سحر گه مخدرات بهشت </p></div>
<div class="m2"><p>بمانده است در و باز چشم حور العین </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نهاده لاله کله کج به شیوه خسرو </p></div>
<div class="m2"><p>گشاده غنچه دهن خوش به خنده شیرین </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>رسیده خسرو انجم به خانه بهرام </p></div>
<div class="m2"><p>زدند خیمه گل بر منار چوبین </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به وصف عارض گل بلبل سخن گو را </p></div>
<div class="m2"><p>معانی کلماتی است نازک و رنگین </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سمن چو نظم ثریا و ژاله چون شعری است </p></div>
<div class="m2"><p>که کرده اند در ان نظم دلگشا تضمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چمان چو من به چمن با چمانه چم بر جوی </p></div>
<div class="m2"><p>اگر معاینه جویی بهشت و ماء معین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو باد صبح به بوی گل و سمن بر خیز </p></div>
<div class="m2"><p>بیا چو شبنم و خوش بر کنار سبزه نشین </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نگر به لاله و نرگس کلاه زر در سر </p></div>
<div class="m2"><p>چنین روند لطیفان به باغ روز چنین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز داغ طاعت تو سبز خنگ گردون را </p></div>
<div class="m2"><p>ز راه مرتبه بر سر سبق گرفت سرین </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در ان زمین که ببارد کفن به جای نبات </p></div>
<div class="m2"><p>بر آورند سر از خاک گنجهای دو فینم </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز هی زلوح ضمیر تو عقل علم آموز </p></div>
<div class="m2"><p>زهی زفیض نوال تو ابر گوهر چین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز عین نعل براق مواکبت دل قاف </p></div>
<div class="m2"><p>هزار بار شده رخنه رخنه چون سر سیم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چنان به عهد تو میزان عدل شد طیار </p></div>
<div class="m2"><p>که میل سوی کبوتر نمی کند شاهین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از ان گذشت که در روزگار احسانت </p></div>
<div class="m2"><p>برای رزق کسی خون خورد به غیر چنین </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به طالع تو مشرف شده است شاه فلک </p></div>
<div class="m2"><p>به طلعت تو منور شده است تاج و نگین </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ظفر به بند کمند تو معتصم شد و گفت </p></div>
<div class="m2"><p>که فتح را به ازین نیست هیچ حبل متین </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به اب تیغ تو میرود به روز کین خود </p></div>
<div class="m2"><p>بود عدوی توزین پس چو اتش بر زین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر سپهر در اید به سایه علمت </p></div>
<div class="m2"><p>بنات پرده نشین فلک شوند بنین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نهد ز ضعف شکم و بر زمین براق فلک </p></div>
<div class="m2"><p>اگر شمار تو بر پشت او ببند زین</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر ز روضه خلدت غزال بوی برد </p></div>
<div class="m2"><p>سراز چه روی فرود اورد به سنبل چین</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زبان سوسن ازاده در حدیث اید </p></div>
<div class="m2"><p>اگر کند به ثنای تو این سخن تلقین</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر چه طبع روان من است گوهر بخش </p></div>
<div class="m2"><p>ور چه شعر متین من است سحر مبین</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طرب سرای خیال من است پرده غیب </p></div>
<div class="m2"><p>خزینه دار ضمیر من است روح امین</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>مرا تصور مدحت چنان بود که بود </p></div>
<div class="m2"><p>شکسته پر مگسی را هوای الیین </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>سخن دراز کشیدم کنون زمان دعاست </p></div>
<div class="m2"><p>که جبرئیل امین راست بر زبان آمین </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>همیشه تا متولد شود اناث وذکور </p></div>
<div class="m2"><p>همیشه تا مترادف بود شهور وسنین </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هزار سال جلالی بقای عمر تو باد </p></div>
<div class="m2"><p>شهور آن همه اردیبهشت وفروردین </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ملوک ملک وملک داعی ومطیع ورهی </p></div>
<div class="m2"><p>خدای عزو جل حافظ ونصیرو معین </p></div></div>