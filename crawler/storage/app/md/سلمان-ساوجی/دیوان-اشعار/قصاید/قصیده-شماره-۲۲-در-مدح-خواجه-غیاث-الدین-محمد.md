---
title: >-
    قصیدهٔ شمارهٔ ۲۲ - در مدح خواجه غیاث الدین محمد 
---
# قصیدهٔ شمارهٔ ۲۲ - در مدح خواجه غیاث الدین محمد 

<div class="b" id="bn1"><div class="m1"><p>تا ز مشک ختنت، دایره بر نسترن است </p></div>
<div class="m2"><p>سبزهٔ خط تو آرایش برگ سمن است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل مشک و سمن گرد برآورد، زرشک </p></div>
<div class="m2"><p>گرد مشک تو که برگرد گل و نسترن است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زره جعد تو را حلقه مشکین گره است </p></div>
<div class="m2"><p>رسن زلف تو را، چنبر عنبر شکن است </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخت شوریده من خفته‌تر از غمزه توست </p></div>
<div class="m2"><p>زلف آشفته تو بسته تراز کار من است </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خال و خط و دهنت چشمه خضر و ظلمات </p></div>
<div class="m2"><p>رخ و زلف و زنخت یوسف و چاه و رسن است </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یوسف عهد خودی، نه نه چه یوسف که تو را </p></div>
<div class="m2"><p>یوسفی گمشده در هر شکن پیرهن است </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سنبل زلف سرانداز تو عنبر زده است</p></div>
<div class="m2"><p>نرگس ترک کماندار تو ناوک فکن است </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حلقه گوش تو، یا رب، چه صفایی دارد </p></div>
<div class="m2"><p>کز صفا حلقه بگوشش شده در عدن است </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل فدای سر زلف تو که هر تاتارش </p></div>
<div class="m2"><p>خون بهای جگر نافه مشک ختن است </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جان نثار لب لعل تو که از غیرت او </p></div>
<div class="m2"><p>داغ غم بر دل خونین عقیق یمن است </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در غم شهدلبان شکرین تو مرا </p></div>
<div class="m2"><p>تن بیمار گدازان چو شکر در لبن است </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا دلم در شکن زلف تو آرام گرفت </p></div>
<div class="m2"><p>دیده من شده در خون دل خویشتن است </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر زلفت به قدم چهره مه می‌سپرد </p></div>
<div class="m2"><p>گوییا نعل سم اسب وزیر زمن است </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن فلک قدر ملک مهر کواکب موکب </p></div>
<div class="m2"><p>که زحل حزم و زحل عزم و عطارد فطن است </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آفتاب فلک جاه، غیاث الحق و دین </p></div>
<div class="m2"><p>که محمد و صفت و نام محمد سنن است </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ناصر شرع نبی، نایب عدل عمرست </p></div>
<div class="m2"><p>وارث علم علی، صاحب خلق حسن است </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آنکه بر مسند ایوان سخا پادشه است </p></div>
<div class="m2"><p>وانکه در عرصه میدان سخن، تهمتن است </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آنکه اندر نظرش، صورت دنیا و فلک </p></div>
<div class="m2"><p>راست چون پیرزنی در پس چرخ کهن است </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ای که بر خاک درت مهر فلک را حسد است </p></div>
<div class="m2"><p>وی که در درج دلت روح ملک را سکن است </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>خرد از سحر حلال سخنت مدهوش است </p></div>
<div class="m2"><p>دل و جان بر خط و خال و قلمت مفتتن است </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مقامی که صریر قلمت در نغم است </p></div>
<div class="m2"><p>در زمانی که زبان سخنت در سخن است </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>تیغ هر چند که آهن دل و پولاد رگ است </p></div>
<div class="m2"><p>شمع با آنکه زبان آور و آتش دهن است </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تیغ را دست هنر مانده به زیر کمر است </p></div>
<div class="m2"><p>شمع را تیغ زبان سوخته اندر لگن است </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>لطفت آن در ثمین است که در رشته عقل </p></div>
<div class="m2"><p>مایه و سود جهانش همه در ثمن است </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>به صفت، رای تو نور است و فلک چون جسم است </p></div>
<div class="m2"><p>به مثل، عدل تو جان است و جهان همچو تن است </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چهره عقل تو فارغ ز غبار ستم است </p></div>
<div class="m2"><p>عرصه ملک تو ایمن ز سپاه فتن است </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>روبه از تقویت شوکت تو شیردل است </p></div>
<div class="m2"><p>پشه از تربیت همت تو پیل تو است </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سلک دور قمر از واسطه کلک و کفت </p></div>
<div class="m2"><p>لله الحمد، که با رونق نظم پرن است </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دیده حاسد تو تیر بلا را هدف است </p></div>
<div class="m2"><p>سینه دشمن تو تیغ فنا را محن است </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>سایه از هر که همای کرمت باز گرفت </p></div>
<div class="m2"><p>کاسه چشم و سرش مطعم زاغ و زغن است </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بر زوایای ضمایر نظرت مطلع است </p></div>
<div class="m2"><p>در سراپای سرایر قلمت موتمن است </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دشمن ار سرکشیی کرد چو شمع از تو چه غم </p></div>
<div class="m2"><p>زانکه آن سرکشی‌اش موجب گردن زدن است </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>فلک از ایودچی درگه عالی تو گشت </p></div>
<div class="m2"><p>هر شبی بر فلک از انجم از آن انجمن است </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>صاحبا بحر مدیح تو نه بحریست کزان </p></div>
<div class="m2"><p>کشتی طبع رهی را ره بیرون شدن است </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>مدح جاه تو نه از روی و ریا می‌گویم </p></div>
<div class="m2"><p>که مرا مدح تو در جان چو روان در بدن است </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیت من گرنه به مدح تو بود باد خراب</p></div>
<div class="m2"><p>بیت کان نبود بیت تو بیت الحزن است </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حق علیم است که در حب محمد امروز </p></div>
<div class="m2"><p>صدق سلمان نه کم از صدق اویس قرن است </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از جبینم همه آثار سعادت تابد </p></div>
<div class="m2"><p>از چه رو، زانکه به خاک در تو مقترن است </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا سپیدی رخ برف و سیاهی سحاب </p></div>
<div class="m2"><p>در چمن موجب سرسبزی سروچمن است </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>باد، آزاد ز باد ستم و جور زمان </p></div>
<div class="m2"><p>سر و جاه تو که سر سبزتر از نارون است </p></div></div>