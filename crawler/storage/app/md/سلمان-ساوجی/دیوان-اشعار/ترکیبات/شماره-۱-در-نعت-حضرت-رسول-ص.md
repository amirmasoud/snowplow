---
title: >-
    شمارهٔ ۱ - در نعت حضرت رسول (ص)
---
# شمارهٔ ۱ - در نعت حضرت رسول (ص)

<div class="b" id="bn1"><div class="m1"><p>ای ذروه لامکان مکانت </p></div>
<div class="m2"><p>معراج ملایک آستانت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطانی و عرش تکیه‌گاهت </p></div>
<div class="m2"><p>خورشیدی و ابر سایه‌بانت </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طاقی است فلک ز بارگاهت</p></div>
<div class="m2"><p>مرغی است ملک ز آشیانت </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوثر عرقی است از جبینت </p></div>
<div class="m2"><p>طوبی ورقی ز بوستانت </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فرزند نخست فطرتی تو </p></div>
<div class="m2"><p>طفلی است طفیل آسمانت </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند که پرورید تقدیر </p></div>
<div class="m2"><p>در آخر دامن الزمانت </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن قرطه مه که چارده شب </p></div>
<div class="m2"><p>خور دوخت شکافته بنانت </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو خانه شرع را چراغی </p></div>
<div class="m2"><p>عالم همه روشن از زبانت </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو گنج دو عالمی از آن رو </p></div>
<div class="m2"><p>کردند به خاک در نهانت </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از توست صلات در حق ما </p></div>
<div class="m2"><p>و ز ما صلوات بر روانت </p></div></div>
<div class="b2" id="bn11"><p>یا قوم علی النبی صلوا </p>
<p>توبوا و تضرعوا و ذلوا </p></div>
<div class="b" id="bn12"><div class="m1"><p>بابای شفیق هر دو عالم </p></div>
<div class="m2"><p>فرزند خلف ترین آدم </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>او خاتم انبیاست زان سنگ </p></div>
<div class="m2"><p>بر سینه ببست همچو خاتم </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای پیرو و تو کلیم عمران </p></div>
<div class="m2"><p>وی پیشروت مسیح مریم </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ذیل محمدی زد این دست </p></div>
<div class="m2"><p>در دولت احمدی زد آن دم </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>زان شد دم او چنین مبارک </p></div>
<div class="m2"><p>زان شد کف آن چنان مکرم </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از عیسی مریمی موخر </p></div>
<div class="m2"><p>بر عالم و آدمی مقدم </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سلطان دو عالمی و هستت </p></div>
<div class="m2"><p>ملک ازل و ابد مسلم </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باغی است فضای کبریایت </p></div>
<div class="m2"><p>بیرون ز ریاض سبز طارم </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>از هر ورقش چو طاق خضرا </p></div>
<div class="m2"><p>آویخته صد هزار شبنم </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقلی تو بلی ولی مصور </p></div>
<div class="m2"><p>روحی تو بلی ولی مجسم </p></div></div>
<div class="b2" id="bn22"><p>ای نام تو بر زمین محمد </p>
<p>خوانند بر آسمانت احمد </p></div>
<div class="b" id="bn23"><div class="m1"><p>تو بحری و هر دو کون خاشاک </p></div>
<div class="m2"><p>خاشاک و درون بحر حاشاک </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زد معجزه‌ات شب ولادت </p></div>
<div class="m2"><p>بر طاق سرای کسر وی چاک </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>رفت آتش کفر پارس بر باد </p></div>
<div class="m2"><p>شد آب سیاه ساوه در خاک </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>در دیده همتت نیاید </p></div>
<div class="m2"><p>دریای جهان به نیم خاشاک </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو بحر حقیقی و از آنرو </p></div>
<div class="m2"><p>داری لب خشک و چشم نمناک </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با سیر براق تو چو صخره </p></div>
<div class="m2"><p>سنگی شده پای برق چالاک </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از طبع تو زاده است دریا </p></div>
<div class="m2"><p>وز نسبت توست گوهرش پاک </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>این دلق هزار میخ نه تو </p></div>
<div class="m2"><p>پوشیده به خانقاهت افلاک </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>مردود تو شد نبیره رز </p></div>
<div class="m2"><p>زین است سرشک دیده تاک </p></div></div>
<div class="b2" id="bn32"><p>قطب شش و هفت و سیصد اخیار </p>
<p>گردون دو شش مه ده و چار </p></div>
<div class="b" id="bn33"><div class="m1"><p>ای سدره ستون بارگاهت </p></div>
<div class="m2"><p>کونین غبار خاک راهت </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کردی نه و هفت و چار را ترک </p></div>
<div class="m2"><p>آن روز که فقر شد هلاکت </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نه چرخ هزار دانه گردان </p></div>
<div class="m2"><p>در حلقه ذکر خانقاهت </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>مهر و فلک است از برایت </p></div>
<div class="m2"><p>ملک و ملک است در پناهت </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در چشم محققان خیالیست </p></div>
<div class="m2"><p>نقش دو جهان ز کارگاهت </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>از منزلت سپهر نازل </p></div>
<div class="m2"><p>و ز مسکنت است اوج جاهت </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ترکان سفید روی بلغار </p></div>
<div class="m2"><p>هندوی دو نرگس سیاهت </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ذی اجنحه لشکر جنایت </p></div>
<div class="m2"><p>قلب فقرا بود سپاهت </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ما مجرم و عاصییم و داریم </p></div>
<div class="m2"><p>امید یه لطف عذر خواهت </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>با آنکه هزار کوه کاه است </p></div>
<div class="m2"><p>با صر صر قهر کوه کاهت </p></div></div>
<div class="b2" id="bn43"><p>سلطان رسل سراج ملت </p>
<p>هادی سبل شفیع امت </p></div>
<div class="b" id="bn44"><div class="m1"><p>چیزی تو شنیده‌ای و دیده </p></div>
<div class="m2"><p>نا دیده کسی و نا شنیده </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تا حشر کسی که مثل او نیست </p></div>
<div class="m2"><p>مثل تو کسی نیافریده </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در عین سفیدی و سیاهی </p></div>
<div class="m2"><p>ذات تو خرد چو نور دیده </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>قهر تو حجاب عنکبوتی </p></div>
<div class="m2"><p>بر دیده دشمنان تنیده </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گیتی که نیافت سایه‌ات را </p></div>
<div class="m2"><p>در سایه توست پروریده </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>روزی که شرار شرک اشراک </p></div>
<div class="m2"><p>هر دم ز سر سنان جهیده </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ز آنجا که ز کیش مارمیتت </p></div>
<div class="m2"><p>مرغان چهار پر، پریده </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>هر دم مدد سپاه نصرت </p></div>
<div class="m2"><p>از ینصرک امدات رسیده </p></div></div>
<div class="b" id="bn52"><div class="m1"><p>آن از کرم تو دیده حیه </p></div>
<div class="m2"><p>کانگشت ز حیرتت گزیده </p></div></div>
<div class="b2" id="bn53"><p>با آنکه کنیز کانت حورند </p>
<p>از بندگی تو در قصورند </p></div>
<div class="b" id="bn54"><div class="m1"><p>با آنکه تو راست سد ره منزل </p></div>
<div class="m2"><p>با قدر تو منزلی است نازل </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>عالم همه حق توست و هر چیز </p></div>
<div class="m2"><p>کان حق تو نیست هست باطل </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>آنجا که براق عزم رانده </p></div>
<div class="m2"><p>افتاده خر مسیح در گل </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>دین تو به قوت نبوت </p></div>
<div class="m2"><p>ذات تو به معجز دلایل </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بر کنده ز جای کفر خیبر </p></div>
<div class="m2"><p>افکنده به چاه سحر بابل </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>آن بحر حقیقی که آن را </p></div>
<div class="m2"><p>نه غور پدید شد نه ساحل </p></div></div>
<div class="b" id="bn60"><div class="m1"><p>در ملک تو صد چو مصر جامع </p></div>
<div class="m2"><p>در کوی تو صد چو نیل سایل </p></div></div>
<div class="b" id="bn61"><div class="m1"><p>در ملک قلوب مشرکان رمح </p></div>
<div class="m2"><p>از کد یمین توست عامل </p></div></div>
<div class="b" id="bn62"><div class="m1"><p>ماهی است رخت که نیستش نقص </p></div>
<div class="m2"><p>سروی است قدت که نیستش ظل </p></div></div>
<div class="b2" id="bn63"><p>ای بر خردت هزار توجیح </p>
<p>در دست تو سنگ کرده تسبیح </p></div>
<div class="b" id="bn64"><div class="m1"><p>ای خوانده حبیب خود خدایت </p></div>
<div class="m2"><p>ملک و ملک و فلک برایت </p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اول علمی کز آفرینش </p></div>
<div class="m2"><p>افراشت نبود جز ولایت </p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ای هفت فلک به رسم در خواست </p></div>
<div class="m2"><p>حلقه شده بر در سرایت </p></div></div>
<div class="b" id="bn67"><div class="m1"><p>تو دیده فطرتی از آن شد </p></div>
<div class="m2"><p>در پرده عنکبوت جایت </p></div></div>
<div class="b" id="bn68"><div class="m1"><p>تو نافه مشکی آفریده </p></div>
<div class="m2"><p>بی آهوی و بی خطا خدایت </p></div></div>
<div class="b" id="bn69"><div class="m1"><p>آراسته سد ره از وجودت </p></div>
<div class="m2"><p>برخاسته صخره از هوایت </p></div></div>
<div class="b" id="bn70"><div class="m1"><p>شد قرص جوت خورش اگر چه </p></div>
<div class="m2"><p>قرص مه و خورشید شده برایت </p></div></div>
<div class="b" id="bn71"><div class="m1"><p>ما را چه مجال نطق باشد </p></div>
<div class="m2"><p>جایی که خدا کند ثنایت </p></div></div>
<div class="b" id="bn72"><div class="m1"><p>با آنکه عطاردست محروم </p></div>
<div class="m2"><p>از خط بنان بحر زایت </p></div></div>
<div class="b" id="bn73"><div class="m1"><p>یک خوشه فلک به توشه دادش </p></div>
<div class="m2"><p>و آن نیز ز خرمن عطایت </p></div></div>
<div class="b" id="bn74"><div class="m1"><p>سکان سراد قات عزت </p></div>
<div class="m2"><p>محتاج شفاعت و دعایت </p></div></div>
<div class="b2" id="bn75"><p>هندوی تو چون بلال کیوان </p>
<p>سلمانت غلام پارسی خوان </p></div>
<div class="b" id="bn76"><div class="m1"><p>ادریس که بر سما رسیده </p></div>
<div class="m2"><p>از رهگذر شما رسیده </p></div></div>
<div class="b" id="bn77"><div class="m1"><p>در شارع معجزات عیسی </p></div>
<div class="m2"><p>جان داده و در تو نارسیده </p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از ناف زمین نسیم مشکت </p></div>
<div class="m2"><p>برخاسته تا خطا رسیده </p></div></div>
<div class="b" id="bn79"><div class="m1"><p>مرغی که نرفت از آشیانت </p></div>
<div class="m2"><p>پیداست که تا کجا رسیده </p></div></div>
<div class="b" id="bn80"><div class="m1"><p>از تذکره رسالت توست </p></div>
<div class="m2"><p>یک رقعه به انبیا رسیده </p></div></div>
<div class="b" id="bn81"><div class="m1"><p>و ز مملکت ولایت توست </p></div>
<div class="m2"><p>یک بقعه به اولیا رسیده </p></div></div>
<div class="b" id="bn82"><div class="m1"><p>بر خلق شده حطام دنیا </p></div>
<div class="m2"><p>مقسوم و به تو بلا رسیده </p></div></div>
<div class="b" id="bn83"><div class="m1"><p>در منزل قرب تو ملایک </p></div>
<div class="m2"><p>از شاهره دعا رسیده </p></div></div>
<div class="b" id="bn84"><div class="m1"><p>جسته ملکت مقام ادنی </p></div>
<div class="m2"><p>از سدره گذشته تا رسیده </p></div></div>
<div class="b2" id="bn85"><p>رخسار تو و مه ده و چار </p>
<p>سیبی است دو نیم کرده پندار </p></div>
<div class="b" id="bn86"><div class="m1"><p>رضوان جنان سرای دارت </p></div>
<div class="m2"><p>جبریل امین امیر بارت </p></div></div>
<div class="b" id="bn87"><div class="m1"><p>کرده سر آسمان متوج </p></div>
<div class="m2"><p>یمن قدم بزرگوارت </p></div></div>
<div class="b" id="bn88"><div class="m1"><p>ای پنج ستون خانه شرع </p></div>
<div class="m2"><p>قایم به وجود چار یارت </p></div></div>
<div class="b" id="bn89"><div class="m1"><p>اول به وجود ثانی اثنین </p></div>
<div class="m2"><p>صدیق که بود یار غارت </p></div></div>
<div class="b" id="bn90"><div class="m1"><p>ثانی عمر است آنکه زد خشت </p></div>
<div class="m2"><p>و افراخت بنای استوارت </p></div></div>
<div class="b" id="bn91"><div class="m1"><p>عثمان که از حیای ابرش </p></div>
<div class="m2"><p>شد تازه و سبز کشتزارت </p></div></div>
<div class="b" id="bn92"><div class="m1"><p>باقی است علی ولی عهدت </p></div>
<div class="m2"><p>او بود وصی حق گزارت </p></div></div>
<div class="b" id="bn93"><div class="m1"><p>داری دو گهر که گوش عرش است </p></div>
<div class="m2"><p>آراسته زان دو گوشوارت </p></div></div>
<div class="b" id="bn94"><div class="m1"><p>این گل عرقی است از تو مانده </p></div>
<div class="m2"><p>بر روی زمین به یادگارت </p></div></div>
<div class="b2" id="bn95"><p>سردار رسل امام کونین </p>
<p>سلطان سریر قاب قوسین </p></div>
<div class="b" id="bn96"><div class="m1"><p>عمری بزدیم دست و پایی </p></div>
<div class="m2"><p>در بحر هوای آشنایی </p></div></div>
<div class="b" id="bn97"><div class="m1"><p>چون بر درش آمدیم امروز </p></div>
<div class="m2"><p>داریم امید مرحبایی </p></div></div>
<div class="b" id="bn98"><div class="m1"><p>ای گل چه شود که از تو یابد </p></div>
<div class="m2"><p>این بلبل بینوا نوایی </p></div></div>
<div class="b" id="bn99"><div class="m1"><p>در سفره رحمت تو گردد </p></div>
<div class="m2"><p>خرم به نواله گدایی </p></div></div>
<div class="b" id="bn100"><div class="m1"><p>از کوی نجات نا امیدی </p></div>
<div class="m2"><p>از راه فتاده مبتلایی </p></div></div>
<div class="b" id="bn101"><div class="m1"><p>بیمار و هوا رسیدگانیم </p></div>
<div class="m2"><p>بخش از شفتین مان شفایی </p></div></div>
<div class="b" id="bn102"><div class="m1"><p>درمانده شدیم و هیچ کس نیست </p></div>
<div class="m2"><p>غیر از تو رجا و ملتجایی</p></div></div>
<div class="b" id="bn103"><div class="m1"><p>آورده‌ام این ثنا و دارم </p></div>
<div class="m2"><p>در خواه ز حضرتت دعایی </p></div></div>
<div class="b" id="bn104"><div class="m1"><p>ما بر سفریم و بهر زادی </p></div>
<div class="m2"><p>خواهیم ز درگهت عطایی </p></div></div>
<div class="b2" id="bn105"><p>هر چند که ما گناهکاریم </p>
<p>امید شفاعت تو داریم</p></div>