---
title: >-
    شمارهٔ ۱۰ - خدنگ مصایب
---
# شمارهٔ ۱۰ - خدنگ مصایب

<div class="b" id="bn1"><div class="m1"><p>ای صبحدم چه شد که گریبان دریده‌ای </p></div>
<div class="m2"><p>وی شب چه حالتی است که گیسو بریده‌ای </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دیده زمانه روان است جوی خون </p></div>
<div class="m2"><p>ای دیده زمانه بگو تا چه دیده‌ای </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای اشک گرم رو خبری بازده ز دل </p></div>
<div class="m2"><p>تا چسیست حال او که بدین رو دیده‌ای </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای آفتاب لرزه فتادست بر دلت </p></div>
<div class="m2"><p>آخر چه دیده‌ای که چنین دل رمیده‌ای </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آسمان تو جامه کبود از چه کرده‌ای </p></div>
<div class="m2"><p>آری مگر تو نیز مصیبت رسیده‌ای </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای پرچم از برای چه سرباز کرده‌ای </p></div>
<div class="m2"><p>آری مگر تو نیز مصیبت رسیده‌ای </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرغان باغ ناله و فریاد می‌کنند </p></div>
<div class="m2"><p>ای باغبان چه موجب فریاد دیده‌ای </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گل جامه پاره می‌کند آخر بپرس ازو </p></div>
<div class="m2"><p>کز باد صبحدم چه حکایت شنیده‌ای</p></div></div>
<div class="b2" id="bn9"><p>نی نی سخن مپرس که جای ملالت است </p>
<p>دانم ملالت است و ندانم چه حالت است </p></div>
<div class="b" id="bn10"><div class="m1"><p>دیدی چه کرد چرخ ستمکار و اخترش </p></div>
<div class="m2"><p>نامش مبر چه چرخ مه چرخ و مه اخترش </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر خاک ریخت آن گل دولت که باغ ملک </p></div>
<div class="m2"><p>با صد هزار ناز بپرورد در برش </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>افشانده خاک بر سر خورشید انورست </p></div>
<div class="m2"><p>گردون که خاک بر سر خورشید انورش </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن شد که بود در قدح روزگار نوش </p></div>
<div class="m2"><p>زهر هلاهل است کنون قند عسکرش </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>شد خار و خاره بستر آن سرو نازنین </p></div>
<div class="m2"><p>کازار می‌رسید ز دیبای ششترش </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگریست تخت بر مملکت شاه تاج بخش </p></div>
<div class="m2"><p>کاورد تخت افسر شاهی به گوهرش </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خط عذار بر ورق حسن او تمام </p></div>
<div class="m2"><p>ننوشته ریخت دست اجل خاک بر سرش </p></div></div>
<div class="b2" id="bn17"><p>مگذر به باغ ازین پس بگذر ز لاله زار </p>
<p>زیرا که باغ بر دل باغ است و لاله زار </p></div>
<div class="b" id="bn18"><div class="m1"><p>شد سرد و تیز بر دل و بر چشم روزگار </p></div>
<div class="m2"><p>هم آب روی دجله و هم باد نوبهار </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دردیده می نیاید از این آب جز سرشک </p></div>
<div class="m2"><p>بر دل نمی‌نشیند از این باد جز غبار </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در کوه سنگ دل نگر از چشمه‌های او </p></div>
<div class="m2"><p>آب روان، روان شده دردروی مرغزار </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مسکین بنفشه بر سر زانو نهاده سر </p></div>
<div class="m2"><p>با جامه کبود پریشان و سوگوار </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>افکندی ای سپهر سواری که مثل او </p></div>
<div class="m2"><p>شیری به روزگار و هژبری به روزگار </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای شوخ دیده بر سر خاکش به خون دل </p></div>
<div class="m2"><p>چندانکه آب در جگرت هست اشک بار </p></div></div>
<div class="b2" id="bn24"><p>رسم امارت از سر عالم بر اوفتاد </p>
<p>تاج سعادت از سر گردون در اوفتاد </p></div>
<div class="b" id="bn25"><div class="m1"><p>گردون به دود حادثه عالم سیاه کرد </p></div>
<div class="m2"><p>ایام خاک بر سر خورشید و ماه کرد </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>صبح این خبر به نوحه ز مرغ سحر شنید </p></div>
<div class="m2"><p>از تاب سینه زد نفسی سرد و آه کرد </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>پوشید آفتاب پلاس سیاه شب </p></div>
<div class="m2"><p>از کهکشان و سنبله ترتیب کاه کرد </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باد اجل چراغ امل را فرو نشاند </p></div>
<div class="m2"><p>وز دود آن چراغ جهانی سیاه کرد </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ای چرخ بی حیا به چه چشم و کدام روی</p></div>
<div class="m2"><p>خواهی به روی خسرو ایران نگاه کرد </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بایست یاد کردنت آن لطف و سعیها </p></div>
<div class="m2"><p>کاندر مدار کار تو دلشاد شاه کرد </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ای چرخ چار بالش خورشید بهر کیست؟</p></div>
<div class="m2"><p>عیسی چو رفت صدر جنان تکیه‌گاه کرد </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چندان گریست مردم ازین غم که چون حباب </p></div>
<div class="m2"><p>اختر به آب دیده مردم شناه کرد </p></div></div>
<div class="b2" id="bn33"><p>کان مصر مملکت که تو دیدی خراب شد </p>
<p>وان نیل مکرمت که شنیدی سراب شد </p></div>
<div class="b" id="bn34"><div class="m1"><p>کو خسروی که بود جهان در امان او </p></div>
<div class="m2"><p>پیوسته بود جان جهانی به جان او </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کو صفدری که روز دغا خصم شوم پی </p></div>
<div class="m2"><p>می‌جست همچو تیر ز دست و کمان او </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کو آن عنان گرای که کوه گران رکاب </p></div>
<div class="m2"><p>جستی کران ز صدمه گرز گران او </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>آن نامور کجاست که دارد بر آسمان </p></div>
<div class="m2"><p>روی قمر هنوز نشان سنان او </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گویی چگونه کرد دل نازنین شاه </p></div>
<div class="m2"><p>ناگاه تحمل خبر ناگهان او </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>چرخا پیاده رو به درگاه میر </p></div>
<div class="m2"><p>کافتاب شهسوار جهان پهلوان او </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ای مرغ نوحه‌گر شو وای ابر به خون گری </p></div>
<div class="m2"><p>بر قامت چو نارون ناروان او </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>دزد وفات گنج حیاتش چگونه برد </p></div>
<div class="m2"><p>کو بخت هوشیار که بد پاسبان او </p></div></div>
<div class="b2" id="bn42"><p>جان داد در موافقت یار نازنین </p>
<p>یار عزیز شرط محبت بود همین </p></div>
<div class="b" id="bn43"><div class="m1"><p>ای دل جهان محل ثبات و قرار نیست </p></div>
<div class="m2"><p>دست از جهان بدار که او پایدار نیست </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زنهار زینهار مخواه از اجل که او </p></div>
<div class="m2"><p>کس را درین سرا چه به جان زینهار نیست </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>مستظهری به مرتبه و اختیار خویش </p></div>
<div class="m2"><p>هیچت ز رفتن دگران اعتبار نیست </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>دنیا چو شاهدی است کناری گزین از او </p></div>
<div class="m2"><p>کز شاهدان خلاصه به جز از کنار نیست </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>صبر و تحمل است و رضا چاره با قضا </p></div>
<div class="m2"><p>تدبیر این قضیه برون زین سه چار نست </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>در حیز وجود همانا نیامدست </p></div>
<div class="m2"><p>آن سیه کز خدنگ مصایب فگار نیست </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بنشین بر آستان رضا چون به هیچ باب </p></div>
<div class="m2"><p>ما رادرون پرده تقدیر بار نیست </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ما بندگان و اوست خداوندگار ما </p></div>
<div class="m2"><p>با کار او مرا و تو را هیچ کار نیست </p></div></div>
<div class="b2" id="bn51"><p>جان در بدن ودیعه پروردگار ماست </p>
<p>می‌خواهد از تو باز ودیعت چه ماجراست </p></div>
<div class="b" id="bn52"><div class="m1"><p>سرو ار فتاد ظل چمن مستدام باد </p></div>
<div class="m2"><p>در گر شکست بحر عدن با نظام باد </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر کوکب منیر فرو شد ز آسمان </p></div>
<div class="m2"><p>خورشید آسمان سعادت مدام باد </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خورشید عمر شه ایلکان گر زوال یافت </p></div>
<div class="m2"><p>ظل امیر شیخ حسن بردوام باد </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>تا روزگار منزل اندوه سختی است </p></div>
<div class="m2"><p>دلشاد و شاه جم عظمت شاد کام باد </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>چونانکه اقبوقا ایلکان راست یادگار </p></div>
<div class="m2"><p>سلطان اویس ولی و قایم مقام باد </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا روز حشر بر سر واماندگان او </p></div>
<div class="m2"><p>ظل ظلیل جاه شما مستدام باد </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>آن سرو قد که گشت تابوت تخته بند </p></div>
<div class="m2"><p>قدرش درخت روضه دارالسلام باد </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>روزی هزار بار ز انفاس قدسیان </p></div>
<div class="m2"><p>بر تربتش نثار درود و سلام باد </p></div></div>