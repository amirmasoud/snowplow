---
title: >-
    رباعی شمارهٔ ۴۳
---
# رباعی شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>با آنکه دو چشم شوخ او عربده جوست </p></div>
<div class="m2"><p>در شوخی و دلبری خم ابروی اوست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بالای تو چشم است که می‌یارد گفت </p></div>
<div class="m2"><p>با دوست که بالای دو چشمت ابروست </p></div></div>