---
title: >-
    رباعی شمارهٔ ۵۴
---
# رباعی شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>سیمین ز نخت که جان از آن بنماید </p></div>
<div class="m2"><p>سییبی است که دانه از میان بنماید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خنده بار دانه ماند لب تو </p></div>
<div class="m2"><p>کز دانه لعلش استخوان بنماید </p></div></div>