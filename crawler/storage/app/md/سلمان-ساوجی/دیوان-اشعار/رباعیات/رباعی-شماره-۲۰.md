---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>چون حال دل من ز غمت گشت تباه </p></div>
<div class="m2"><p>آویخت در آن زلف دل آشوب سیاه </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز آنسان که در آتش سقر اهل گناه </p></div>
<div class="m2"><p>آرند به مار و کژدم از عجز پناه </p></div></div>