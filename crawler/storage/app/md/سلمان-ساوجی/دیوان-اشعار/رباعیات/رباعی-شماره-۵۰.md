---
title: >-
    رباعی شمارهٔ ۵۰
---
# رباعی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>گل بین که ز عندلیب بگریخته است </p></div>
<div class="m2"><p>با دامن با قلی در آمیخته است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذشته ز سبحان سخنی چون بلبل </p></div>
<div class="m2"><p>وز دامن باقلی در آویخته است </p></div></div>