---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>در وصل نماند بیش ازین تدبیرم </p></div>
<div class="m2"><p>پیشم بنشین دمی که پیشت میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون اشک ز چشم من جدا خواهی شد </p></div>
<div class="m2"><p>آخرکم آنکه در کنارت گیرم </p></div></div>