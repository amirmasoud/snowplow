---
title: >-
    رباعی شمارهٔ ۹۳
---
# رباعی شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>توفیق نمی‌شود به زاری حاصل </p></div>
<div class="m2"><p>وز عمر عزیز است چه خواری حاصل </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون باد ز گردیدن بیهوده چه چیز </p></div>
<div class="m2"><p>کردیم به غیر جانسپاری حاصل؟</p></div></div>