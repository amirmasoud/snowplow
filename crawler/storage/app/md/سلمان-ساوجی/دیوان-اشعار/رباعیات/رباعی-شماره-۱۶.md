---
title: >-
    رباعی شمارهٔ ۱۶
---
# رباعی شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>ای سایه سنبلت سمن پرورده، </p></div>
<div class="m2"><p>یاقوت تو را در عدن پرورده، </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون لب خود مدام جان می‌پرور </p></div>
<div class="m2"><p>ز آن راح که روحی است بدن پرورده </p></div></div>