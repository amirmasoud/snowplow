---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>درد آمد و گرد من ز هر سو بنشست </p></div>
<div class="m2"><p>گه بر سرو چشم و گاه بر رو بنشست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دولت کار او به پایان برسید </p></div>
<div class="m2"><p>آمد به ادب به هر دو زانو بنشست </p></div></div>