---
title: >-
    رباعی شمارهٔ ۴۰
---
# رباعی شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>تا ناله بلبلم به گوش آمده است </p></div>
<div class="m2"><p>دل با سر عیش نای و نوش آمده است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رگ از تن خشک تاک برخاسته است </p></div>
<div class="m2"><p>خون در تن جام می‌ بجوش آمده است </p></div></div>