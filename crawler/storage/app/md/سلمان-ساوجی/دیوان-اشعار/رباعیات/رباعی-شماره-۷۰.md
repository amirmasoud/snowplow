---
title: >-
    رباعی شمارهٔ ۷۰
---
# رباعی شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>از شمع جمال تو دلم تاب کشد </p></div>
<div class="m2"><p>از جام لبت خرد می ناب کشد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این مردمک دیده تر دامن من </p></div>
<div class="m2"><p>تا چند ز چاه ز نخت آب کشد؟</p></div></div>