---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>آمد سحری ندا ز میخانه ما </p></div>
<div class="m2"><p>کای رند خراباتی دیوانه ما </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برخیز که پر کنیم پیمانه ز می </p></div>
<div class="m2"><p>زآن پیش که پر کنند پیمانه ما </p></div></div>