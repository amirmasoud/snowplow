---
title: >-
    رباعی شمارهٔ ۱۰۱
---
# رباعی شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>تا کی چو گل از هوا مشوش باشیم؟</p></div>
<div class="m2"><p>چند از پی آبرو در آتش باشیم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون جان عزیز ما به دست قدر است </p></div>
<div class="m2"><p>تن را به قضا دهیم و دلخوش باشیم </p></div></div>