---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>آتش ز دهان شمع دیشب می‌جست </p></div>
<div class="m2"><p>ناگاه سپیده دم زبانش بشکست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر رشته به پایان شد و تابش بنماند </p></div>
<div class="m2"><p>روزش به شب آمد و بروزم بنشست </p></div></div>