---
title: >-
    رباعی شمارهٔ ۶۱
---
# رباعی شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>دی سرو به باغ سرفرازی می‌کرد </p></div>
<div class="m2"><p>سوسن به چمن زبان درازی می‌کرد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غنچه نسیم صبحدم می‌پیچید </p></div>
<div class="m2"><p>با بید و چنار دست بازی می‌کرد </p></div></div>