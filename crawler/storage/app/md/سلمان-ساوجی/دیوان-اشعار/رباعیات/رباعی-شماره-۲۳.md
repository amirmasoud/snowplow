---
title: >-
    رباعی شمارهٔ ۲۳
---
# رباعی شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ای دوست کجائی و کجائی که نئی؟</p></div>
<div class="m2"><p>آخر تو کرائی و کرائی که نئی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانگی تو با من افتاد ار نه</p></div>
<div class="m2"><p>تو یار کدام آشنایی که نئی؟</p></div></div>