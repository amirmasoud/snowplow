---
title: >-
    رباعی شمارهٔ ۷۵
---
# رباعی شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>بر زلف تو چون باد وزیدن گیرد </p></div>
<div class="m2"><p>از هر طرفی مشک وزیدن گیرد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در لبت اندیشه باریک کنم </p></div>
<div class="m2"><p>خون از رگ اندیشه چکیدن گیرد </p></div></div>