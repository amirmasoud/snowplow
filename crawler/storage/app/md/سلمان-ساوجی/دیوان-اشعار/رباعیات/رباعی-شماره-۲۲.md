---
title: >-
    رباعی شمارهٔ ۲۲
---
# رباعی شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>ای بس که شکست و باز بستم توبه </p></div>
<div class="m2"><p>فریاد همی کند ز دستم توبه </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیروز به توبه‌ای شکستم ساغر </p></div>
<div class="m2"><p>امروز به ساغری شکستم توبه </p></div></div>