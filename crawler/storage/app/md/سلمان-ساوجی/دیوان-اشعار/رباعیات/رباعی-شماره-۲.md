---
title: >-
    رباعی شمارهٔ ۲
---
# رباعی شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه تو طالب خدایی به خود آ </p></div>
<div class="m2"><p>از خود بطلب کز تو جدا نیست خدا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول به خود آ چون به خود آیی به خدا </p></div>
<div class="m2"><p>کاقرار نمایی به خدایی به خدا </p></div></div>