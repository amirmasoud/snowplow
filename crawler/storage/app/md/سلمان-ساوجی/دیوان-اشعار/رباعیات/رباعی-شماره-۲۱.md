---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>با منعم خود برون منه پای ز راه </p></div>
<div class="m2"><p>عصیان ولی نعم گناهست گناه </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در منعم خود که او دواتست، چو کلک </p></div>
<div class="m2"><p>بگشاد زبان او سیه گشت سیاه </p></div></div>