---
title: >-
    رباعی شمارهٔ ۲۸
---
# رباعی شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دی دیده به دل گفت که چرا پر خونی؟</p></div>
<div class="m2"><p>ز آن سلسله زلف چرا مجنونی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من دیده‌ام از برای او پر خونم </p></div>
<div class="m2"><p>آخر تو ندیده‌ای، چرا پر خونی؟</p></div></div>