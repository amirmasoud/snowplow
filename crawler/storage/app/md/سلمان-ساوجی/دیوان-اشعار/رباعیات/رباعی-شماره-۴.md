---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>جز نقش تو در نظر نیامد ما را</p></div>
<div class="m2"><p>جز کوی تو رهگذر نیامد ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواب ار چه خوش آید همه را در عهدش</p></div>
<div class="m2"><p>حقا که به چشم در نیامد ما را</p></div></div>