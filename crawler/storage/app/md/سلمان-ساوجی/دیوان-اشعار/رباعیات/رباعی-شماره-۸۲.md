---
title: >-
    رباعی شمارهٔ ۸۲
---
# رباعی شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>ای خواجه فلان الدین که ریشت ... باد </p></div>
<div class="m2"><p>ریشت نفسی نیست ز دندان آزاد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر ریش تو یک گوزگره خواهم زد </p></div>
<div class="m2"><p>زآنان که به دندان نتوانیش گشاد </p></div></div>