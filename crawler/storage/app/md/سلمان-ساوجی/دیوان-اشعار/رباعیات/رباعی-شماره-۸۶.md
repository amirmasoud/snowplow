---
title: >-
    رباعی شمارهٔ ۸۶
---
# رباعی شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>دل خواستم از زلف سمن پوش تو دوش </p></div>
<div class="m2"><p>گفتا که چه دل؟ دل که؟ دل چیست؟ خموش </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زلف تو اگر چه حال ما می‌ماند </p></div>
<div class="m2"><p>لیکن طرف دوش تو می‌دارد گوش </p></div></div>