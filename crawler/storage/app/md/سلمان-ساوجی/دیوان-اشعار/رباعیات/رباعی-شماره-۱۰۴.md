---
title: >-
    رباعی شمارهٔ ۱۰۴
---
# رباعی شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>در مجلس تو ز گل پراکنده‌ترم </p></div>
<div class="m2"><p>وز نرگس مخمور، سرافکنده‌ ترم </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از غنچه گل اگر چه دل زنده ترم </p></div>
<div class="m2"><p>از غنچه به خون جگر آکنده ترم </p></div></div>