---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>قسمم همه درد است و دوا چیزی نیست </p></div>
<div class="m2"><p>در سینه به جز رنج و عنا چیزی نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد است گرفته سر و دستم در دست </p></div>
<div class="m2"><p>دردا که به جز درد مرا چیزی نیست</p></div></div>