---
title: >-
    رباعی شمارهٔ ۵
---
# رباعی شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>از عهد و وفا هیچ خبر نیست تو را </p></div>
<div class="m2"><p>جز وعده و دم هیچ دگر نیست تو را </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سازند کمر به دست عشاق به ناز </p></div>
<div class="m2"><p>چون است کز این دست کمر نیست تو را </p></div></div>