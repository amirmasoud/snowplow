---
title: >-
    رباعی شمارهٔ ۴۵
---
# رباعی شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>آورد بهم تیر و کمان را در دست </p></div>
<div class="m2"><p>تیر آمد و در خانه خویشش بنشست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد به سر تیر کمان خانه فرو </p></div>
<div class="m2"><p>انصاف که نیک از آن میان بیرون جست </p></div></div>