---
title: >-
    رباعی شمارهٔ ۵۷
---
# رباعی شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>در وصف لبت نطق زبان بسته بود </p></div>
<div class="m2"><p>پیش دهنت پسته زبان بسته بود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابروی تو آن سیاه پیشانی دار </p></div>
<div class="m2"><p>پیوسته به قصد سرمیان بسته بود </p></div></div>