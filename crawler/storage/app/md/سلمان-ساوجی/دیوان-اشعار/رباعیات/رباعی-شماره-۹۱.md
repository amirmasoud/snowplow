---
title: >-
    رباعی شمارهٔ ۹۱
---
# رباعی شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>امسال مکرر است وقت گل و مل </p></div>
<div class="m2"><p>وز غم سر و برگ ندارد بلبل </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با آن همه شوکت ز پریشانی وقت </p></div>
<div class="m2"><p>بی تیغ و سپر برون نمی‌آید گل </p></div></div>