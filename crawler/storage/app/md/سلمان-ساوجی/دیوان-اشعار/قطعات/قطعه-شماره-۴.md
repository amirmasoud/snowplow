---
title: >-
    قطعه شمارهٔ ۴
---
# قطعه شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>یک حدیثم یادگارست از پدر </p></div>
<div class="m2"><p>کای پسر چون حاجتی افتد تو را </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همت از صاحب دلی کن التماس </p></div>
<div class="m2"><p>پس به صاحب دولتی بر التجا </p></div></div>