---
title: >-
    قطعه شمارهٔ ۱۴۱
---
# قطعه شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>جهان مجد و معالی رشید دولت و دین </p></div>
<div class="m2"><p>زهی به جاه و جمال تو چشم جان روشن </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به فیض ابر کفت بحر و بر چنان پر شد </p></div>
<div class="m2"><p>که بحر خشک لب آمد چو ابر تر دامن </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلک جنابا چون رای و تیغ هر دو ثور است </p></div>
<div class="m2"><p>به جنب رای تو گو آفتاب تیغ مزن </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تویی که در چمن فضل هر که سر بر زد </p></div>
<div class="m2"><p>زبان شود همه تن در ثنات چون سوسن </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ولیک ایزد داند که هر کجا هستم </p></div>
<div class="m2"><p>بجز جناب ثنای تو نیستم مسکن </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو تو کریم ندیدم که می در آویزد </p></div>
<div class="m2"><p>وسایل تو به سایل چو غازیان به رسن </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هنوز گردنم از بار منتت پست است </p></div>
<div class="m2"><p>وگرنه هم سوی شکرت بر آرمی گردن </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جهان اگر پر ارزن کنند مرغی را </p></div>
<div class="m2"><p>دهند قوت به هر سال دانه ارزن </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جهان تهی شود از ارزن و تهی نشود </p></div>
<div class="m2"><p>دلم ز دانه شکرت به قوت مرغ سخن </p></div></div>