---
title: >-
    قطعه شمارهٔ ۳۴
---
# قطعه شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>ایا سحاب نوالی که ابر دریا دل </p></div>
<div class="m2"><p>به های های ز دست تو بارها بگریست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر کجا که کنی روی فتح پیشرو است </p></div>
<div class="m2"><p>که پشت فتح به روی مبارک تو قویست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدا یگانا من بنده قدیم توام </p></div>
<div class="m2"><p>به حال بنده ازین بیش بایدت نگریست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که در ورق بخشش تو ثابت نیست </p></div>
<div class="m2"><p>سه چار سال پیاپی به غیر سلمان کیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ذل فاقد و از طعن مردمان هر دم </p></div>
<div class="m2"><p>مرا بدار به نوعی که خوش توانم زیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وظیفه‌ای که ازین پیش داشتم آن نیز </p></div>
<div class="m2"><p>نمی‌دهند ازین پس وظیفه من چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زیاده باد هزاران عطیه کبری </p></div>
<div class="m2"><p>شمار عمر تو و هر عطیه‌ای صد و بیست </p></div></div>