---
title: >-
    قطعه شمارهٔ ۸۶
---
# قطعه شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>خدایگانا چو شد اشارتت که رهی </p></div>
<div class="m2"><p>به ملک فارس به تحصیل وجه زر برود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گمان بنده نبود آنکه بعد چندین سال </p></div>
<div class="m2"><p>ز درگهت به چنین کار مختصر برود </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولی به حکم قضا بر رضا چه چاره بود </p></div>
<div class="m2"><p>چو هست حکم قضا گو بدین قدر برود </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خاک پای عزیزت که گر به آب سیاه </p></div>
<div class="m2"><p>اشارت تو بود چون قلم به سر برود </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگرچه رفتن او هر چه دیرتر بکشید </p></div>
<div class="m2"><p>کنون چو می‌رود آن به که زودتر برود </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساز کار من امروز زانکه می‌ترسم </p></div>
<div class="m2"><p>که گر دو روز بمانم یکی دگر برود </p></div></div>