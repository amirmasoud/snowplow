---
title: >-
    قطعه شمارهٔ ۴۷
---
# قطعه شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>خدایگان وزیران ملک آصف عهد </p></div>
<div class="m2"><p>زهی نهاده نهاد تو عدل را بنیاد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غبار ادهم کلک تو عنبر اشهب </p></div>
<div class="m2"><p>غلام سنبل خلق تو سوسن آزاد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز صنع تربیت رای بنده پرور توست </p></div>
<div class="m2"><p>خرد که پیر فلک را نزاده دارد یاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر از شمامه خلقت صبا اثر یابد </p></div>
<div class="m2"><p>شود بنفشه محزون چو گل از آن هم شاد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان لاله ازان شد به عنبر آلوده </p></div>
<div class="m2"><p>که او حکایت خلق تو می‌کند با باد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدایگانا احوال من ز دور فلک </p></div>
<div class="m2"><p>به صورتی است که احوال دشمنان تو باد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الاغکی دو سه زین پیش داشت بنده تو </p></div>
<div class="m2"><p>به وجه قرض یکایک به قرض خواهان داد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنون تصور آن می‌کند که بر تابد </p></div>
<div class="m2"><p>به سوی ساوه عنان عزیمت از بغداد </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پیاده رخ به ره آورده ماتم از حیرت </p></div>
<div class="m2"><p>تو شهسواری و اسبی به مات باید داد </p></div></div>