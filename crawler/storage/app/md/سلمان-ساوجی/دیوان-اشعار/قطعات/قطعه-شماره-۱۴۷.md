---
title: >-
    قطعه شمارهٔ ۱۴۷
---
# قطعه شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>صاحب سلطان نشان دستور اعظم شمس الدین </p></div>
<div class="m2"><p>ای جلال رفعتت را اوج گردون پایگاه </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ز سدر آستان حضرت تو برگذشت </p></div>
<div class="m2"><p>شاید ار سجده برندش هر زمان خورشید و ماه </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشمه خورشید را خون گردد از بهر تو دل </p></div>
<div class="m2"><p>چون کند اندر ضمیر عالم آرایت نگاه </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردهی رخصت که بوسد آستانت را فلک </p></div>
<div class="m2"><p>هر زمان از غایت شادی بر اندازد کلاه </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون به دار الملک حکمت پادشاهی می‌سزد </p></div>
<div class="m2"><p>گر بود قدر تو را از چرخ اطلس بارگاه </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدتی شد تا نکردی در خلا و در ملا </p></div>
<div class="m2"><p>یاد من کارم ازان شوریده شد حالم تباه </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود نمی‌دانی که از من روی برتابد طرب </p></div>
<div class="m2"><p>گر نباشد سد اقبالت مرا پشت و پناه </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خلاف راستی کردند نقلی نیست غم </p></div>
<div class="m2"><p>هست بر تصدیق قول من ضمیر تو گواه </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حق همی داند که از من بد نیاید در وجود </p></div>
<div class="m2"><p>ور در آمد خود کجا شد خلعت ثم اجتباه </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ور همه خود راست است آن بیت یاد‌آور که گفت: </p></div>
<div class="m2"><p>از بزرگان عفو باشد وز فرو دستان گناه </p></div></div>