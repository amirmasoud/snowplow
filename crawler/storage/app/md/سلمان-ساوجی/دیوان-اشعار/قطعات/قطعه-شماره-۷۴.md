---
title: >-
    قطعه شمارهٔ ۷۴
---
# قطعه شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>یا رب این قوم چه دم سرد و چه افسرده </p></div>
<div class="m2"><p>که به دم سردی و افسردگی از دی بترند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیر قوم همه شان خواجه علاالدین است </p></div>
<div class="m2"><p>که ورا اهل خرد لاشه لاشی شمرند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کسی در سرو شکلش نگرد قی بکند </p></div>
<div class="m2"><p>نوکرانش همه از گرسنگی قی بخورند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به تقدیر و به به تحریر چو تیر فلک است </p></div>
<div class="m2"><p>به ازین نیست کزین مملکتش پی ببرند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سبلتش را به کششهای پیاپی بکنند </p></div>
<div class="m2"><p>دبه‌اش را به لگدهای دمادم بدرند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش می‌گفت حریفی که فلانی امروز </p></div>
<div class="m2"><p>خواجه فرمود که در ملک دگر می نخورند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به سر خواجه که من دست فرا می نبرم </p></div>
<div class="m2"><p>تا سر خواجه از اینجا به فرو می نبرند </p></div></div>