---
title: >-
    قطعه شمارهٔ ۱۰
---
# قطعه شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>به سال هفتصد و هفتاد و پنج گشت خراب </p></div>
<div class="m2"><p>به آب شهر معظم که خاک بر سر آب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریغ روضه بغداد، آن بهشت آباد </p></div>
<div class="m2"><p>که کرده است خرابش جهان خانه خراب </p></div></div>