---
title: >-
    قطعه شمارهٔ ۴۶
---
# قطعه شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای یاد حضرتت چو قدح مایه نشاط </p></div>
<div class="m2"><p>طبع جهان به خرمی دولت تو شاد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن لفظ وعده‌ای که پی آن محقرست </p></div>
<div class="m2"><p>حاشا که از ضمیر منیرت رود زیاد </p></div></div>