---
title: >-
    قطعه شمارهٔ ۱۵۶
---
# قطعه شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>ز دور دایره این محیط پرگاری</p></div>
<div class="m2"><p>نصیب من همه سرگشتگی است پنداری </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نشسته‌ام به کناری چو چنگ سر در پیش </p></div>
<div class="m2"><p>فتاده در پس زانو و می‌کنم زاری </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آتشم چو زر از دوستان قلب دو رو </p></div>
<div class="m2"><p>ز بی زری همه از من نموده بیزاری </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بی زری است اگر چون چراغ بی روغن </p></div>
<div class="m2"><p>زمان زمان نفسی می‌زنم به دشواری </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برای تلخی عیش حسود و شادی دوست </p></div>
<div class="m2"><p>کنم به خون چو قدح رنگ چهره گلناری </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عزیز مصر وجودم، نیم اسیر کسی </p></div>
<div class="m2"><p>درین دیار از اخوان چرا کشم خواری </p></div></div>