---
title: >-
    قطعه شمارهٔ ۲۸
---
# قطعه شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>دیدمش دوش رخ تراشیده </p></div>
<div class="m2"><p>گفتم ای جان و دل که روی تو خست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت مشاطه بهر چشم بدان </p></div>
<div class="m2"><p>خالی از وسمه بر رخم می‌بست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عرضم زانکه سخت نازک بود </p></div>
<div class="m2"><p>تاب وسمه نداشت خون برجست </p></div></div>