---
title: >-
    قطعه شمارهٔ ۴۵
---
# قطعه شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>ای وجودت سبب راحت و آسایش خلق </p></div>
<div class="m2"><p>به وجودت ابدا زحمت و رنجی مرساد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ره راستی اندر چمن دین سروی </p></div>
<div class="m2"><p>خاطرت باد چو سرو از همه بندی آزاد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از هستی ما عارضه‌ات گرد انگیخت </p></div>
<div class="m2"><p>نور چشم هنری هیچ غبارت مرصاد </p></div></div>