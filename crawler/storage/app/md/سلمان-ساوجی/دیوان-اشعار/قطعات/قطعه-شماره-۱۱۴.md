---
title: >-
    قطعه شمارهٔ ۱۱۴
---
# قطعه شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>ای وزیری که ملک جاه تو راست</p></div>
<div class="m2"><p>از سماوات و ارض افزون عرض</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زمانه شکایتی دارم</p></div>
<div class="m2"><p>بر ضمیر تو کرد خواهی عرض</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون روا باشد ای خلاصه عمر</p></div>
<div class="m2"><p>کی سزا باشد ای خلیفه ارض</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که در ایام دولت تو کسی</p></div>
<div class="m2"><p>که دعای تو باشد او را فرض</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نخورد هیچ چیز الا غم</p></div>
<div class="m2"><p>نکند هیچ کار الا قرض</p></div></div>