---
title: >-
    قطعه شمارهٔ ۷۸
---
# قطعه شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>خسروا یاد می‌کنم هر دم </p></div>
<div class="m2"><p>به سر تخت خسروی سوگند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که به همراهی مواکب شاه </p></div>
<div class="m2"><p>هست چون دیده‌ام دل اندر بند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم زخمی رسید ناگاهم </p></div>
<div class="m2"><p>درد چشمم ز راه باز افکند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستیم تا کنم به دیده و دل </p></div>
<div class="m2"><p>خدمتت منع کرد بخت نژند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل به کلی ز خویش برکندم </p></div>
<div class="m2"><p>دیده را بر نمی‌توانم کند </p></div></div>