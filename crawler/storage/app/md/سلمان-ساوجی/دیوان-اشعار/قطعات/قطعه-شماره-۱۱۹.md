---
title: >-
    قطعه شمارهٔ ۱۱۹
---
# قطعه شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>اکمل دولت و دین ای شرف منصب تو </p></div>
<div class="m2"><p>در کمال شرف و قدر ازان سوی کمال </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهره را از حسد مجلس لطفت هر شب </p></div>
<div class="m2"><p>بوده از خون شفق جام افق مالامال </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تن بدخواه تو دیدم شده غربال به تیر </p></div>
<div class="m2"><p>گرچه خون نیز ندیدم به جز آن یک غربال </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در جهان شبه نظیر تو که ممکن نبود </p></div>
<div class="m2"><p>همچو آسایش اهل نظر و فضل محال </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو را شمه‌ای از حال دل من بشنو </p></div>
<div class="m2"><p>از سر لطف و کرم نی ز سر رنج و ملال </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا بدانی که به جرم هنر و فضل مراست </p></div>
<div class="m2"><p>دل ز مویه شده چون موی و تن از ناله چونال </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بود عمری که مرا در طلب فضل گذشت </p></div>
<div class="m2"><p>خوشتر از دور صبی تازه‌تر از عهد وصال </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوش می‌دارم وصیت کرمت می‌شنوم </p></div>
<div class="m2"><p>کین سخن بشنود از توشه خورشید نوال </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>التماس از در الطاف تو تا کی نکنم </p></div>
<div class="m2"><p>چون بود رنج همه گنج شود مالامال </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نعمت و محنت ایام چو باقی نبود </p></div>
<div class="m2"><p>عمر فانی چه کنم در طلب نعمت و مال </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تا جهان باشد باد از اثر طالع سعد </p></div>
<div class="m2"><p>بر جهان طلعت میمون تو فرخنده به فال </p></div></div>