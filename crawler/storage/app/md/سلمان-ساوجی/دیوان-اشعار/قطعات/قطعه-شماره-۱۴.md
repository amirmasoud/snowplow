---
title: >-
    قطعه شمارهٔ ۱۴
---
# قطعه شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای عذار تو از آفتاب تا بی یافت </p></div>
<div class="m2"><p>گمان مبر که عذارت در آفتاب بسوخت </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولی چو در رهت افتاد آفتاب به مهر </p></div>
<div class="m2"><p>جمال روی تو را دل بر آفتاب بسوخت </p></div></div>