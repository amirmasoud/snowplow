---
title: >-
    قطعه شمارهٔ ۸
---
# قطعه شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>بر آستان رفیع خدایگان جهان </p></div>
<div class="m2"><p>سپهر کوه و قار آفتاب ابر عطا </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستاره لشکر خورشید رای گردون قدر </p></div>
<div class="m2"><p>سکندر آیت جمشید ملک دارا را </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خدایگان سلاطین امیر شیخ حسن </p></div>
<div class="m2"><p>که باد کام و مرادش همه روان و روا </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کمینه بنده داعی دولتش سلمان </p></div>
<div class="m2"><p>پش از وظیفه ارسال بندگی و دعا </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رسم تذکره در باب حال خویش دو فصل </p></div>
<div class="m2"><p>به عز عرض ضمیر منیر غیب نما </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی که مدت ده سال می‌رود تا من </p></div>
<div class="m2"><p>درین جناب زبان برگشاده‌ام به ثنا </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوافل دعوات از دل و زبان من اند </p></div>
<div class="m2"><p>رفیق کوکبه صبح و کاروان مسا </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز فاضل صدقات تو بود در دیوان </p></div>
<div class="m2"><p>بنام بنده ازین پیش مباغی مجرا </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سه سال شد که از آن کرده‌اند بعضی کم </p></div>
<div class="m2"><p>وزان کمی شده افزون شماتت اعدا </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پس از ملازمت ده دوازده ساله </p></div>
<div class="m2"><p>پس از رسالت پنجه قصیده غرا </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>معایش دگران از فواضل کرمت </p></div>
<div class="m2"><p>زیاده گشت و مراکم چرا شده است اجرا </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا ز مرحمت خسروانه‌ات اکنون </p></div>
<div class="m2"><p>اشارتی است توقع ز جانب و ز را </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>که از مواجب من آنچه قطع فرمودند </p></div>
<div class="m2"><p>کشد اضافت مرسوم بنده و قطعا </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دگر تغیر و تبدیل ره بدان ندهند </p></div>
<div class="m2"><p>به هیچ وجه و سبب نایبامن استیفا </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>که تا به دولت شاه از سر فراغ درون </p></div>
<div class="m2"><p>نقود عمر کنم صرف در دعای شما </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دوم چو دخل رهی کم شد و زیادت خرج </p></div>
<div class="m2"><p>به خرج بنده نمی‌کرد وجه دخل وفا </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>قروض شد متراکم ازین سبب بر من </p></div>
<div class="m2"><p>زمانه شد متطاول ازین جهت بر ما </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهد خاک پای تو کز فرط ازدحام عیال </p></div>
<div class="m2"><p>به حال خویشتنم نیست یک زمان پروا </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر چنانچه مرا کارکی بفرمایید</p></div>
<div class="m2"><p>که وجه قرض توان کرد ازان قضیه ادا </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قضای قرض کنم و ز بلا شوم ایمن </p></div>
<div class="m2"><p>که باد جان و تنت ایمن از قضا و بلا </p></div></div>