---
title: >-
    قطعه شمارهٔ ۱۱
---
# قطعه شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>شنودم که می‌گفت بشوده به شیخ </p></div>
<div class="m2"><p>که احوال حاجی است در اضطراب </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه من دوش خوابی عجب دیده‌ام </p></div>
<div class="m2"><p>که سیلی در آمد ز کوه زراب </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمارات حاجی و پالانهاش </p></div>
<div class="m2"><p>همی برد و می‌کرد یکسر خراب </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یکی از خبیثان شهر این سخن </p></div>
<div class="m2"><p>به جایی رسانید و دادش جواب </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمایند هر شب خران را بخواب </p></div>
<div class="m2"><p>که پالان گران را ببردست آب </p></div></div>