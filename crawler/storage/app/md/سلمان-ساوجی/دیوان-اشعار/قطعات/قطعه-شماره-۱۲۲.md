---
title: >-
    قطعه شمارهٔ ۱۲۲
---
# قطعه شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>صاحب عادل کمال الدین حسن </p></div>
<div class="m2"><p>ای تو را مه چاکر و کیوان غلام </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو گردون گوهر خاص تو پاک </p></div>
<div class="m2"><p>همچو باران فیض انعام تو عام </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جهان مکرمت هستی حسن </p></div>
<div class="m2"><p>هم به خلق و هم به جود و هم به نام </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سعادت چون ظفر میمون لقا </p></div>
<div class="m2"><p>وز معالی چون فلک عالی مقام </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خواجه بهر این دعاگوی فقیر </p></div>
<div class="m2"><p>کرده انعامی بر ابنای کرام </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌برم نرد سعادت گر کند </p></div>
<div class="m2"><p>کعبتین لطف او، او را تمام </p></div></div>