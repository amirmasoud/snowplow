---
title: >-
    قطعه شمارهٔ ۸۴
---
# قطعه شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>نفس من اگر چه جانبخش است </p></div>
<div class="m2"><p>جگرم غرق خون چو مشک بود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه دریا به ابر آب دهد </p></div>
<div class="m2"><p>لب دریا همیشه خشک بود </p></div></div>