---
title: >-
    قطعه شمارهٔ ۴۰
---
# قطعه شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>مشیر ملک و صلاح زمانه عزالدین </p></div>
<div class="m2"><p>که هر چه هست به جز خدمت تو نیست صلاح </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرآنچه بر دل خصمت گذشته کج بوده </p></div>
<div class="m2"><p>مگر به روز نبرد تو در سهام و رماح </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر زمین که گذر کرده باد آبادی </p></div>
<div class="m2"><p>نسیم معدلتت جسته از مهب ریاح </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزگوارا یک شمه بشنو از حالم </p></div>
<div class="m2"><p>که چیست بر دلم از گردش صباح و رواح </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جماعتی چه جماعت سه چار بی سر و بن </p></div>
<div class="m2"><p>همه به خصمی من بر کشیده قلب و جناح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر آن امید نشسته که خون من ریزند </p></div>
<div class="m2"><p>که هر چه بود به جز خونشان نبود مباح </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز هنر همه جرمم دعای دولت توست </p></div>
<div class="m2"><p>که عقد منتظمش کرد روزگار و شاح </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به دولت تو بر آرم دمارشان از سر </p></div>
<div class="m2"><p>مرا زبان چو خنجر کفایت است صلاح </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو دیرمان به جهان و جهانیان که تو را </p></div>
<div class="m2"><p>بدین به خلق فرستاد رازق فتاح </p></div></div>