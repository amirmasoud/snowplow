---
title: >-
    قطعه شمارهٔ ۲۴
---
# قطعه شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>پادشاها ز عمر خویش مرا </p></div>
<div class="m2"><p>بی حضور شما چه فایده است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دعا گو به غیبت و حضور </p></div>
<div class="m2"><p>شاه را جز دعا چه فایده است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچنان چون ز حضرتت دورم </p></div>
<div class="m2"><p>بودن اینجا مرا چه فایده است </p></div></div>