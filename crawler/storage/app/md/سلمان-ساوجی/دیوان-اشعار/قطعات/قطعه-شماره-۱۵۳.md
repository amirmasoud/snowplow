---
title: >-
    قطعه شمارهٔ ۱۵۳
---
# قطعه شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>در ره بغداد کز هر جانبی </p></div>
<div class="m2"><p>ناله افتاده باری آمدی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داشتم اسبی که از رفتار او </p></div>
<div class="m2"><p>بر دلم هر دم غباری آمدی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندکی زر نیز بود اما نبود </p></div>
<div class="m2"><p>آن قدر کاندر شماری آمدی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زر نماند و مرد ریگ اسبم بماند </p></div>
<div class="m2"><p>هم نماندی گربه کاری آمدی </p></div></div>