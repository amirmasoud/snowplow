---
title: >-
    قطعه شمارهٔ ۹۷
---
# قطعه شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>ایا شمع جمع و چراغ ملوک</p></div>
<div class="m2"><p>چو پروانه تا چند تابم دهید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شما عین لطفید و دریای جود</p></div>
<div class="m2"><p>چرا وعده چون سرابم دهید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناهی نکردم خطایی نرفت</p></div>
<div class="m2"><p>چه موجب که چندین عذابم دهید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا کز تب محرق انتظار</p></div>
<div class="m2"><p>جگر سوخت یک شربت آبم دهید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به یک شربت تربیت قانعم</p></div>
<div class="m2"><p>وگر نیست شربت جوابم دهید</p></div></div>