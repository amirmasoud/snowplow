---
title: >-
    قطعه شمارهٔ ۱۵۹
---
# قطعه شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>چو بر طلول دیار حبیب بگذشتم </p></div>
<div class="m2"><p>که کرده بود خرابش جهان ز بی باکی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجاوران دیار خراب را دیدم </p></div>
<div class="m2"><p>در آن خرابه خراب و شکسته و باکی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خاک رهگذار حبیب می‌گفتم </p></div>
<div class="m2"><p>که ای غلام تو آب حیات در پاکی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کجا شدت گل این باغ و شمع این مجلس </p></div>
<div class="m2"><p>کجا شد آن طرب و عیش و آن طربناکی </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسی ازین کلمات و حدیث رفت و نبود </p></div>
<div class="m2"><p>در آن منازل خاکی به جز صدا حاکی </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زمان زمان به دل و چشم خویش می‌گفتم </p></div>
<div class="m2"><p>ایا منازل سلمان این سلماکی </p></div></div>