---
title: >-
    قطعه شمارهٔ ۱۰۲
---
# قطعه شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>خسرو اخاک درگه تو مرا</p></div>
<div class="m2"><p>از غبار ذر ور نیکوتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیک در حالتی چنین که منم</p></div>
<div class="m2"><p>غیبتم از حضور نیکوتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال چشمم بدست دور از تو</p></div>
<div class="m2"><p>چشم بد از تو دور نیکوتر</p></div></div>