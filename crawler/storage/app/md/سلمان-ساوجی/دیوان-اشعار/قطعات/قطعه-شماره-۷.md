---
title: >-
    قطعه شمارهٔ ۷
---
# قطعه شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>هوس مملکت چرا نبود </p></div>
<div class="m2"><p>بعد ازین هر گدا و تونی را </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که به جای خلیفه در بغداد </p></div>
<div class="m2"><p>بنشاندند کازرونی را </p></div></div>