---
title: >-
    قطعه شمارهٔ ۱۵۱
---
# قطعه شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>بر بتان، حسن و جوانی مفروش </p></div>
<div class="m2"><p>ای جوان گرچه به غایت خوبی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی زرت کار میسر نشود </p></div>
<div class="m2"><p>گر تو خود یوسف بن یعقوبی </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه بی زر چه زنی بر در دوست </p></div>
<div class="m2"><p>آهن سرد چرا می‌کوبی </p></div></div>