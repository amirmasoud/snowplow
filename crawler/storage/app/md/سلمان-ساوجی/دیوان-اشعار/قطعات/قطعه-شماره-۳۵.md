---
title: >-
    قطعه شمارهٔ ۳۵
---
# قطعه شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>پادشاها مهد عالی می‌رود سوی شکار </p></div>
<div class="m2"><p>لیکن اسباب شدن ما را مهیا هیچ نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیمه و اسب است و زین جامه اسباب سفر </p></div>
<div class="m2"><p>جز دو اسب لاغری با بنده زینها هیچ نیست </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوکرانی نیز نیکو دارم اما هیچ یک </p></div>
<div class="m2"><p>بر سرش دستار بر تن جبه در پا هیچ نیست </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لاجرم از گفت و گوی نوکران در خانه‌ام </p></div>
<div class="m2"><p>جز حدیث سرد و تشنیع و تقاضا هیچ نیست </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر و بالا چون مگوید مردکی کش روز و شب </p></div>
<div class="m2"><p>جز زمین و آسمان در زیر و بالا هیچ نیست </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هم عفا الله قرض خواهم کودم فردای من </p></div>
<div class="m2"><p>می‌خورد با آنکه می‌داند که فردا هیچ نیست </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجه مرسومی که سلطانم معین کرده بود </p></div>
<div class="m2"><p>جو به جو مستغرق است و حالا هیچ نیست </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن محقر چون دهان شاهدان آوازه‌ای </p></div>
<div class="m2"><p>داشت اما چون نظر کردیم پیدا هیچ نیست </p></div></div>