---
title: >-
    قطعه شمارهٔ ۱۲۴
---
# قطعه شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>شاها از میان جان و دل بیگاه و گاه </p></div>
<div class="m2"><p>من دعایت با دعای قدسیان پیوسته‌ام </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وجود ابر احسانت که بر من فایض است </p></div>
<div class="m2"><p>راستی از منت دور فلک وارسته‌ام </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خداوندی که رنگ و بوی بزمت چون بدید </p></div>
<div class="m2"><p>گفت گل بر خود چه می‌خندی که اینجا دسته‌ام </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد چشمی ناگهانم خاست و اندر خانه‌ای </p></div>
<div class="m2"><p>تنگ و تاری همچو چشم خویشتن بنشسته‌ام </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کرده‌ام عادت به چشم و سر به درگاه آمدن </p></div>
<div class="m2"><p>زان نمی‌آیم که چشمم بسته و من خسته‌ام </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم‌های بنده از نادیدنت دیوانه‌ام </p></div>
<div class="m2"><p>هر دو را زان روی چون دیوانگان بر بسته‌ام </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دولتت بادا ابد پیوند و خود باشد چنین </p></div>
<div class="m2"><p>بارها عقل این سخن در گوش گفت آهسته‌ام </p></div></div>