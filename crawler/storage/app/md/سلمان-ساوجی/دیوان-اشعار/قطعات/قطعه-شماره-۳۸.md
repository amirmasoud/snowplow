---
title: >-
    قطعه شمارهٔ ۳۸
---
# قطعه شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>خواجه از قول باز می‌گردد </p></div>
<div class="m2"><p>خواجه را شرط و قول و پیمان نیست </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلپذیر است قول او لیکن </p></div>
<div class="m2"><p>لیکنش بازگشت چندان نیست </p></div></div>