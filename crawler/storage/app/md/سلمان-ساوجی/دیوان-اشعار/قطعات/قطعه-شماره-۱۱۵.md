---
title: >-
    قطعه شمارهٔ ۱۱۵
---
# قطعه شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>ماه گردون سلطنت ناگاه </p></div>
<div class="m2"><p>شد نهان در حجاب میغ دریغ </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین تحسر بماند در دندان </p></div>
<div class="m2"><p>لب و دست نگین و تیغ دریغ </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ابد بر زوال شاه اویس </p></div>
<div class="m2"><p>ملک و دین می‌زنند دریغ دریغ </p></div></div>