---
title: >-
    قطعه شمارهٔ ۴۴
---
# قطعه شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>کنار حرص الا پر کجا توانی کرد </p></div>
<div class="m2"><p>تو از طمع که سه حرف میان تهی افتاد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزیز من در درویشی و قناعت زن </p></div>
<div class="m2"><p>که خواری از طمع و عزت از قناعت زاد </p></div></div>