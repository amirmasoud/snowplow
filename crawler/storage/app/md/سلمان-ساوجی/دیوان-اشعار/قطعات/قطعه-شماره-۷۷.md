---
title: >-
    قطعه شمارهٔ ۷۷
---
# قطعه شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>ای خداوندی که پیش طبع فیاضت سحاب </p></div>
<div class="m2"><p>بی‌حیا شخصی بود گر دعوی رادی کند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرکب عزمت به هر میدان که برخیزد ز جا </p></div>
<div class="m2"><p>گوی خاکی در رکابش جنبش بادی کند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همتت گر ملک باقی را نماید التفات </p></div>
<div class="m2"><p>هر گیاهی بعد ازین در باغ شمشادی کند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور کنی هندوی کیوان را به دربانی قبول </p></div>
<div class="m2"><p>مقبل جاوید گردد زین فرج شادی کند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با حریف رای تو گردون همی ریزد به طرح </p></div>
<div class="m2"><p>مهره سیمین انجم و آن ز استادی کند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاحبا داد سخن من داده‌ام در روزگار </p></div>
<div class="m2"><p>آسمان بر من نمی‌شاید که بیدادی کند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از برای بنده بنیادی نهادی سخت نیک </p></div>
<div class="m2"><p>زودتر ترسم که گردون سست بنیادی کند </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گردن من بنده گر آزاد گردانی زدین </p></div>
<div class="m2"><p>تا بود از بندگیت بنده آزادی کند </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جاودان پاینده بادی تا به یمن دولتت </p></div>
<div class="m2"><p>این خراب آباد دنیا جنت آبادی کند </p></div></div>