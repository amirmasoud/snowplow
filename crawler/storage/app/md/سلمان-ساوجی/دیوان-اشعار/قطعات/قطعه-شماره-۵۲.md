---
title: >-
    قطعه شمارهٔ ۵۲
---
# قطعه شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>ای شهنشاهی که این چرخ مقوس روز رزم </p></div>
<div class="m2"><p>طایران فتح را از پر تیرت بال کرد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طینت پاک تو را از جوهر عقل آفرید </p></div>
<div class="m2"><p>آن خداوندی که شخص آدم از صلصال کرد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد خیلت را ظفر در چشم دولت سرمه کرد </p></div>
<div class="m2"><p>ظل چترت را فلک بر روی دولت خال کرد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دست تو ابواب آمال خلایق کرد باز </p></div>
<div class="m2"><p>دامن خواهندگان از مال مالامال کرد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همتت راضی نشد ورنه ز گرد موکبت </p></div>
<div class="m2"><p>خواست رضوان حوریان را یاره و خلخال کرد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تیزیی می‌کرد خنجر بهر خون دشمنت </p></div>
<div class="m2"><p>آمد اندر سر بخونش بس که استعجال کرد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زر غلام حلقه در گوش غلامان تو شد </p></div>
<div class="m2"><p>زان جهان نامش گهی دینار و گه مثقال کرد </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فیض دستت دید دریا زیر لب با ابر گفت </p></div>
<div class="m2"><p>گر نوالی می‌کنی باید بدین منوال کرد </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر که در مدح تو چون سوسن نشد رطب اللسان </p></div>
<div class="m2"><p>لاله سان گردون زبانش را سیاه و لال کرد </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پادشاها گر چه گستاخی است لیکن واجب است </p></div>
<div class="m2"><p>عرض حال خود مرا پیشت علی الا جمال کرد </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر من از دین است باری بس قوی و من ضعیف </p></div>
<div class="m2"><p>چون توانم احتمال این چنین اثقال کرد </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>طوبی بار آور طبع مرا طوبی له </p></div>
<div class="m2"><p>تند باد صر صر غم خواهد استیصال کرد </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بر ادا قرض سلمان وعده‌ها دادند لیک </p></div>
<div class="m2"><p>دولت توفیق شاه آن وعده‌ها پامال کرد </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بود مقصودش که در دست تو گردد ساخته </p></div>
<div class="m2"><p>در ادا قرض من دوران ازان اهمال کرد </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کعبه آمال چون درگاه گردون قدرت است </p></div>
<div class="m2"><p>خواستم زین پیش عزم کعبه آمال کرد </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرغ جان ناتوانم بال پریدن نداشت </p></div>
<div class="m2"><p>در هوای عزم درگاه تو پر از بال کرد </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>داشتم عزم سفر چون ماه بهر اجتماع </p></div>
<div class="m2"><p>ناگه از یک ماهه ره خورشیدم استقبال کرد </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>التفاتی گر کند رای قدر فرمان تو </p></div>
<div class="m2"><p>می‌تواند بنده را تدبیر جبر حال کرد </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>حاجت من بنده می‌داند که خواهم شد روا </p></div>
<div class="m2"><p>چون دعاگو روی دل در قبله اقبال کرد </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>روز حشرت از حساب سال و ماه و عمر باد </p></div>
<div class="m2"><p>کز پی عمرت فلک تدبیر ماه و سال کرد </p></div></div>