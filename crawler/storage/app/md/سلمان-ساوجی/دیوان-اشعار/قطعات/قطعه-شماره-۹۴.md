---
title: >-
    قطعه شمارهٔ ۹۴
---
# قطعه شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>شبی زبان فصاحت ز منهیان خرد</p></div>
<div class="m2"><p>سوال کرد غرض آنکه مدتی است مدید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که بر خلاف طباع زمانه می‌بینم</p></div>
<div class="m2"><p>که حصن ملک حصین است و سد عدل سدید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زوال ظلمت ظلم است از ستاره بدیع</p></div>
<div class="m2"><p>کمال دولت فضل است از زمانه بعید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه خامه‌ای است که کوتاه می‌کند هر دم</p></div>
<div class="m2"><p>زبان تیغ که بودی دراز در تهدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه عادلی است که ز عدل او ممالک را</p></div>
<div class="m2"><p>تنعمی است موفی سعادتی است سعید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه صاحبی است که اصحاب دین و دولت را</p></div>
<div class="m2"><p>ز تیغ خامه او حاصل است وعده وعید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جواب داد خرد کافتاب دولت و دین</p></div>
<div class="m2"><p>که ماه رایت او خلق راست چون مه عید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هلال غره دولت جمال طلعت ملک</p></div>
<div class="m2"><p>غیاث دین محمد محمدبن رشید</p></div></div>