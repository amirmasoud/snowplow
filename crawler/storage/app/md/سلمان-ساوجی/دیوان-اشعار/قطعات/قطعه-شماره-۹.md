---
title: >-
    قطعه شمارهٔ ۹
---
# قطعه شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>آصف ثانی رشیدالحق والدین آنکه هست </p></div>
<div class="m2"><p>آسمان عکسی ز روی عالم آرای شما </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صاحبا از ماجرای حال خود من شمه‌ای </p></div>
<div class="m2"><p>عرض خواهم داشت بر رای اعلای شما </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان سبب بالای گردون خم شد اندر قدر صدر</p></div>
<div class="m2"><p>کو به عکس راستی بنشست بالای شما </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کجا عزم تو پای مردی آرد در رکاب </p></div>
<div class="m2"><p>جز رکاب آنجا که دارد در جهان پای شما </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن تویی کز ابتدا در باب ارباب هنر </p></div>
<div class="m2"><p>تربیت بودست و بخشش رسم آبای شما </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وان منم کز گوهر نظمم مزین کرده‌است </p></div>
<div class="m2"><p>گردن و گوش جهان را مدح بابای شما </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با وجود آنکه استعداد و استحقاق من </p></div>
<div class="m2"><p>روشن است امروز بر آیینه رای شما </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از برای خرده‌ای زر جستن آزار من </p></div>
<div class="m2"><p>بس عجب می‌دارم از طبع گهرزای شما </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حاصل دی و پریرم همچنان تا چار ماه </p></div>
<div class="m2"><p>صرف شد بر وعده امروز و فردای شما </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سخت بی برگم بساز امروز کارم را که هست </p></div>
<div class="m2"><p>بیش ازین ما را سر برگ تقاضای شما </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آنچه انصاف است آمد شد خجل گشتم خجل </p></div>
<div class="m2"><p>من ز خاک آستان آسمان سای شما </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از قدمهای خود اکنون من خجالت می‌کشم </p></div>
<div class="m2"><p>هم برین صورت کزین پیش از کرمهای شما </p></div></div>