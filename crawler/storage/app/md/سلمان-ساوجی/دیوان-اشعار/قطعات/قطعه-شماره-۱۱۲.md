---
title: >-
    قطعه شمارهٔ ۱۱۲
---
# قطعه شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>قوی و بزرگ و سرافراز و سرخ رو ناگه </p></div>
<div class="m2"><p>به آرزوی تو برخاستم ز مسکن خویش </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو در جناب تو آمد شدم دراز کشید </p></div>
<div class="m2"><p>برفت آب و هوس کم شد و ندامت پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روا مدار کنون باز پس روم ز درت </p></div>
<div class="m2"><p>به خود فرو شده گریان و سر فکنده به پیش </p></div></div>