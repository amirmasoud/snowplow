---
title: >-
    قطعه شمارهٔ ۱۴۶
---
# قطعه شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>چشم من جای تو بود ای نور چشم </p></div>
<div class="m2"><p>رفتی و ماند از تو خالی جای تو </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم خود را اگر نمی‌بینم رواست </p></div>
<div class="m2"><p>چون نبینم بی تو من ماوای تو </p></div></div>