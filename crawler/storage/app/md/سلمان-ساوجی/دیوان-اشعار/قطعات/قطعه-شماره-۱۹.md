---
title: >-
    قطعه شمارهٔ ۱۹
---
# قطعه شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>شهی که شنقر عنقا شکار او را ماه </p></div>
<div class="m2"><p>کلاه گوشه خورشید زنگ زرین است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو باز سر علمش راهمای گردون دید </p></div>
<div class="m2"><p>به چرخ گفت نظر باز کن که شاهین است </p></div></div>