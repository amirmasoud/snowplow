---
title: >-
    قطعه شمارهٔ ۹۳
---
# قطعه شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>دیگر آن است که محبوب جهان سقری شاه</p></div>
<div class="m2"><p>آمد از بندگی شاه که می‌فرماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو بگو بنده دیرینه ما سلمان را</p></div>
<div class="m2"><p>که بخواه از کرمم هر چه تو را می‌باید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنده بر حسب اشارت طلبی کردم و شاه</p></div>
<div class="m2"><p>داشت مبذول چنان کز کرم شاه آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وعده دین است و ز دین من اگر ز انچه کند</p></div>
<div class="m2"><p>ذمت همت خود شاه بری می‌شاید</p></div></div>