---
title: >-
    قطعه شمارهٔ ۲۱
---
# قطعه شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>الا ای آفتاب مشرق فضل </p></div>
<div class="m2"><p>که تاب مهرت اندر جان ماه است </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنان تا تافتی بر جانب شام </p></div>
<div class="m2"><p>جهان در چشم من چون شب سیاه است </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به امید قدومت انجمن را </p></div>
<div class="m2"><p>همه شب دیده چون انجم به راه است </p></div></div>