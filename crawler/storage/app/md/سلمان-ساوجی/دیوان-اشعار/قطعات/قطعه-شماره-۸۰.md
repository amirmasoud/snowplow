---
title: >-
    قطعه شمارهٔ ۸۰
---
# قطعه شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>شاها وزرایی که امینان امیرند </p></div>
<div class="m2"><p>بی وجه مرا در پی خود چند دوانند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بودند بران عزم که مرسوم رهی را </p></div>
<div class="m2"><p>امسال نرانند و کنون نیز برآنند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کیسه که من بر کرمت دوخته بودم </p></div>
<div class="m2"><p>یک یک بدر دیدند و شب و روز درآنند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه خلق پسندد نه خدا کانچه خداوند </p></div>
<div class="m2"><p>بخشد به دعاگو دگران باز ستانند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون قاعده رسم مرا شاه نهادست </p></div>
<div class="m2"><p>برداشتن رسم تو ایشان نتوانند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موقوف رسانیدن پروانه عالی است </p></div>
<div class="m2"><p>وجه من و یک جو نرسد تا نرسانند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ماننده آبی است که در راه بماند </p></div>
<div class="m2"><p>مرسوم دعاگوی بفرما که برانند </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در دولت شاهی ابدالدهر بمانی </p></div>
<div class="m2"><p>تا دولت و شاهی ابدالدهر بمانند </p></div></div>