---
title: >-
    قطعه شمارهٔ ۷۱
---
# قطعه شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>عقل را گفتم که عمری پیش ازین چوپانیان </p></div>
<div class="m2"><p>گردن از گردون گردان از چه می‌افراشتند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این زمان آخر چرا زین سان جدا از خان مان </p></div>
<div class="m2"><p>پشت بر کردند و روی از دشمنان برداشتند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفت ای غافل تو از صورتگران روزگار </p></div>
<div class="m2"><p>نیستی آگه کزین صورت بسی انگاشتند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش ازین چون گله در صحرای گیتی مردمان </p></div>
<div class="m2"><p>خویشتن را گرگ یکدیگر همی پنداشتند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نبود این گله را از حفظ چوپانی گریز </p></div>
<div class="m2"><p>میر چوپان را به چوپانی برو بگماشتند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میر چوپان را به چوپانی گریز </p></div>
<div class="m2"><p>گرگ و میش آن داوری را از میان برداشتند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا به عهد دولتت شاهنشه ایران رسد </p></div>
<div class="m2"><p>گله را ار خواستند ار نه بدو بگذاشتند </p></div></div>