---
title: >-
    قطعه شمارهٔ ۴۱
---
# قطعه شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>صدر عالی، کمال دولت و دین </p></div>
<div class="m2"><p>ای به تو کشور کرم، آباد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سخا و مروت و احسان </p></div>
<div class="m2"><p>مثل تو مادر زمانه نزاد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او دست در رکاب تو زد </p></div>
<div class="m2"><p>پای بر فرق فرقدان بنهاد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بزرگان روزگار تویی </p></div>
<div class="m2"><p>خوب خلق و اصیل و نیک نهاد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک قرابه شراب نیکم بخش </p></div>
<div class="m2"><p>که تو را کردگار بد مد هاد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده بیتی همی کند تضمین </p></div>
<div class="m2"><p>که از آن خوبتر ندارد یاد </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بخت نیکت به منتهای امید </p></div>
<div class="m2"><p>برساناد و چشم بد مرساد </p></div></div>