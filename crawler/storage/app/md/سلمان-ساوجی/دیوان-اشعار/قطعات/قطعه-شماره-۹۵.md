---
title: >-
    قطعه شمارهٔ ۹۵
---
# قطعه شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>ایا سحاب نوالی که از صریر قلم</p></div>
<div class="m2"><p>به گوش صامعه جز مدحت شما نرسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بوی خلق خوشت بح کاروان نسیم</p></div>
<div class="m2"><p>سحرگه از طرف تبت و خطا نرسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نماند در همه آفاق ذره‌ای که درو</p></div>
<div class="m2"><p>ز آفتاب دلت پرتو عطا نرسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به گرد گرد سمندت براق جمشیدی</p></div>
<div class="m2"><p>اگر چه بود جهان گرد و باد پا نرسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر ز اطلس سبز فلک قبایی دوخت</p></div>
<div class="m2"><p>ولی به قد بزرگی تو فرا نرسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خدا یگانا در غیبت آنچه فرمودی</p></div>
<div class="m2"><p>به ما رسید هماندم ولی به ما نرسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چرا قضیه اسبی که میر با بنده</p></div>
<div class="m2"><p>روانه کرد فرومانده و هیچ جا نرسید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سمند سرکش تند تو از طریق وفا</p></div>
<div class="m2"><p>به من پیاده بی‌دست و به چار پا نرسید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دعای من چون به تو می‌رسد دگر سهل است</p></div>
<div class="m2"><p>اگر من ز تو زنجیری رسید یا نرسید</p></div></div>