---
title: >-
    قطعه شمارهٔ ۹۲
---
# قطعه شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ای وزیری که دلت همت اگر در بندد </p></div>
<div class="m2"><p>گره عقد ز ابروی فلک بگشاید </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدم همت تو تارک کیوان سپرد </p></div>
<div class="m2"><p>چنبر طاعت تو گردن گردون ساید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زمان قلمت زهره ندارد بهرام</p></div>
<div class="m2"><p>که زبان و لب شمشیر به خون آلاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه با عقل در ایام تو کردند رجوع</p></div>
<div class="m2"><p>گفت تا خواجه درین باب چه می‌فرماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش ماه از در خورشید چراغی طلبید</p></div>
<div class="m2"><p>گفت پروانه دستوری او می‌باید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صاحبا رای جهانگیر تو را معلوم است</p></div>
<div class="m2"><p>که جهان هر نفسی حادثه‌ای می‌زاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو به لطفی و صفا پاکتر از آب روان</p></div>
<div class="m2"><p>چه عجب باشد اگر پای تو در سنگ آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>که گرفته شود و گاه جهان گیرد شمس</p></div>
<div class="m2"><p>گه زند تیغ و گهی روی زمین آراید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر از کسر شدت قتح زیادت چه عجب</p></div>
<div class="m2"><p>مستی غمزه خوبان ز خمار افزاید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>موکب عزم همایون تو لاینصرف است</p></div>
<div class="m2"><p>فتح در موضع کسرش اگر آید شاید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بر جهان سایه انصاف تو باقی بادا</p></div>
<div class="m2"><p>تا جهان در کنف عدل تو می‌آساید</p></div></div>