---
title: >-
    قطعه شمارهٔ ۶۶
---
# قطعه شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>ایا ستاره سپاهی که آب شمشیرت </p></div>
<div class="m2"><p>غبار فتنه و ظلم از هوای ملک بنشاند </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک به نام تو تا خطبه داد در عالم </p></div>
<div class="m2"><p>زمانه جز تو کسی را به پادشاهید نخواند </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غبار دامن قدر تو بود چرخ کبود </p></div>
<div class="m2"><p>گهی که همت تو دامن از جهان افشاند </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به جاه خواست که ماند به تو مخالف تو </p></div>
<div class="m2"><p>بسی بداد درین اشتیاق جان و نماند </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شها کمیت من آمد رکاب سان در پا </p></div>
<div class="m2"><p>عنان عزم به کلی ز دست من بستاند </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به زور می‌کشمش چون کمان از آنکه برو </p></div>
<div class="m2"><p>جز استخوان و پی و پوست هیچ چیز نماند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به مرکبیم مدد ده ازان که نیست مرا </p></div>
<div class="m2"><p>بدست لاغری جز قلم که بتوان راند </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وگرنه درستی خواهد از کسالت و ضعف </p></div>
<div class="m2"><p>میان گرد بماند ستور و مرد نماند </p></div></div>