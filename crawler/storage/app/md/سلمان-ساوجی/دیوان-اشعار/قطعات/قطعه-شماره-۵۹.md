---
title: >-
    قطعه شمارهٔ ۵۹
---
# قطعه شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>فراز تخت معانی چو کوس فضل زنم </p></div>
<div class="m2"><p>سبق ز جمله اقران خود مرا باشد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنایت و کرمت گر شود پذیر فتار </p></div>
<div class="m2"><p>سپهر در صدد بندگان ما باشد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برای یک دو سه بی دست و پای گاه سخن </p></div>
<div class="m2"><p>مرا چه رنجه کنی این سخن روا باشد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که از مکاره عنف توام عنا آمد </p></div>
<div class="m2"><p>هم از مکارم لطف توام شفا باشد </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دراز نمی‌کنم قصه کوته اولیتر </p></div>
<div class="m2"><p>بقای عمر تو خواهم که دائما باشد </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دعای زنده دلانت رفیق باد و جلیس </p></div>
<div class="m2"><p>همیشه تا که فلک قبله دعا باشد </p></div></div>