---
title: >-
    قطعه شمارهٔ ۶۱
---
# قطعه شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>شاها مرا به اسبی موعود کرده بودی </p></div>
<div class="m2"><p>در قول پادشاهان قیلی مگر نباشد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسبی سیاه پیرم دادند و من برآنم </p></div>
<div class="m2"><p>کاندر جهان سیاهی زان پیرتر نباشد </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن اسب باز دادم تا دیگری ستانم </p></div>
<div class="m2"><p>بر صورتی که کس را زان سر خبر نباشد </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسب سیاه تا رفت رنگی دگر نیامد </p></div>
<div class="m2"><p>آری پس از سیاهی رنگی دگر نباشد </p></div></div>