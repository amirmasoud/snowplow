---
title: >-
    قطعه شمارهٔ ۱۲
---
# قطعه شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>جهان جود و مروت سپهر فضل و کرم </p></div>
<div class="m2"><p>که خاک پای تو را چاکرست آب حیات </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز حزم و عزم تو آن لاف می‌زنیم دایم </p></div>
<div class="m2"><p>که چون فلک به مسیری و چون زمین به ثبات </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر زمین که گذشتی ز ابر احسانت </p></div>
<div class="m2"><p>برآمدست سخا و کرم به جای نبات </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بزرگوارا از طلعت همایونت </p></div>
<div class="m2"><p>بر ارغنون نشاطم بلند گشت اصوات </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز قول طایف بغداد و مصر می‌خواهم </p></div>
<div class="m2"><p>از آنکه دست تو چون دجله است و نیل و فرات </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو می‌زند دل من لاف یکتایی </p></div>
<div class="m2"><p>سزد گرم به تو باشد توقع سوغات </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرس همی ران در عرصه امید به کام </p></div>
<div class="m2"><p>که گشت در عری عرصه دشمنت شهمات </p></div></div>