---
title: >-
    قطعه شمارهٔ ۵۷
---
# قطعه شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>من که باشم که شوی رنجه به پرسیدن من </p></div>
<div class="m2"><p>این چنین لطف و کرم هم ز شما برخیزد </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاهی تو هم عذر تو خواهه ورنه </p></div>
<div class="m2"><p>چه ز دست من درویش گدا برخیزد </p></div></div>