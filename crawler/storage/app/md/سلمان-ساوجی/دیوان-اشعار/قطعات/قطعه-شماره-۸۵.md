---
title: >-
    قطعه شمارهٔ ۸۵
---
# قطعه شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>هلال غره دولت وجیه دولت و دین </p></div>
<div class="m2"><p>که با ضمیر تو خورشید راضیا نبود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هلال خواندمت زانکه زاده شمسی </p></div>
<div class="m2"><p>وگرنه نام قمر، شمس را سزا نبود </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عهد خلقت اگر نافه دم زند از مشک </p></div>
<div class="m2"><p>حقیقت است که بی آهو و خطا نبود </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه ابر با تو اگر لاف زد مرنج که ابر </p></div>
<div class="m2"><p>گدای یم بود و در گدا حیا نبود </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر چانچه دهی صلح آب و آتش را </p></div>
<div class="m2"><p>میانشان پس ازین جنگ و ماجرا نبود </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبانت از سخن عدل و جور خالی نیست </p></div>
<div class="m2"><p>بلی ز تیغ مهند گهر جدا نبود </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر عنایت تو پشت آسمان گردد </p></div>
<div class="m2"><p>به هیچ رویش ازین پشت او دو تا نبود </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دران مکان که نهد دست مسندت فراش </p></div>
<div class="m2"><p>عجب گرش سر فرقد به زیر پا نبود </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اساس دولتی از بهرت ابتدا امروز </p></div>
<div class="m2"><p>کند زمانه که قطعاش انتها نبود </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در آن مصاف که از خون کشته گل خیزد </p></div>
<div class="m2"><p>به غیر بنده تو فتح را عصا نبود </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در آن مقام که بر ملک کار ملک افتد </p></div>
<div class="m2"><p>گره به جز سر کلکت گره گشا نبود </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز سنبله به عطارد دگر جوی نرسد </p></div>
<div class="m2"><p>اگر عنایت رای تو را رضا نبود </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به خاصیت نبود ربود برگی کاه </p></div>
<div class="m2"><p>اگر حمایت تو یار کهربا نبود </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چو با عنایتت افتاد کار غله سبب </p></div>
<div class="m2"><p>تو را چو نیست عنایت نصیب ما نبود </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>حقوق خدمت ما بر شما چو معلوم است </p></div>
<div class="m2"><p>جهانیان را پوشیده بر شما نبود </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به باب وجد تو مشهور گشت خلق و کرم </p></div>
<div class="m2"><p>بدین سخن سخنی هیچ خصم را نبود </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کرم که از همه با بی تو راست موروثی </p></div>
<div class="m2"><p>چو هست باد گران با منت چرا نبود؟</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>محقری که کریمی خصوص با چو منی </p></div>
<div class="m2"><p>کند روانه تو باطل کنی روا نبود </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>طمع بود شعرا را ز اسخیا لیکن </p></div>
<div class="m2"><p>توقع از شعرا رسم اسخیا نبود </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مده در اول دن دردیم که دن را درد </p></div>
<div class="m2"><p>بود همیشه ولیکن در ابتدا نبود </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در آرزوی ثنای منند پادشهان </p></div>
<div class="m2"><p>چرا جناب شما رغبت ثنا نبود </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عنایتی است مرادم ز تو دگر سهل است </p></div>
<div class="m2"><p>اگر بود زر من در میانه یا نبود </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سخن دراز کشیدم زمان، زمان دعاست </p></div>
<div class="m2"><p>که هر چه بهر تو گویم به از دعا نبود </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه باد تو را دولت و بقا باقی </p></div>
<div class="m2"><p>که هیچ چیز به از دولت و بقا نبود </p></div></div>