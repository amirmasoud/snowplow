---
title: >-
    بخش ۵ - آغاز داستان 
---
# بخش ۵ - آغاز داستان 

<div class="b" id="bn1"><div class="m1"><p>شنیدم که شاهی به ایران زمین </p></div>
<div class="m2"><p>سزاوار دیهیم و تاج و نگین </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر افشان چو خورشید در گاه بزم </p></div>
<div class="m2"><p>سر افشان چو شمشیر درگاه رزم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آب کفش بحر گریان شده </p></div>
<div class="m2"><p>ز تاب تفش ببر بریان شده </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر با فلک در کمر دست کین </p></div>
<div class="m2"><p>زدی آسمان را زدی بر زمین </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به رمح از فلک عقده را می‌گشود </p></div>
<div class="m2"><p>ز چوگان او گوی مه می‌ربود </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو دستش کمان را بیاراستی </p></div>
<div class="m2"><p>ز هازه ز هر گوشه برخاستی </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو بر گوش مرکب نهادی قدم </p></div>
<div class="m2"><p>زدی خامه را پای کردی قلم </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهی زور دست شهنشاه زه </p></div>
<div class="m2"><p>که بست از سر دست بر چرخ زه </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه رادی و مردی و بخردی </p></div>
<div class="m2"><p>ز سر تا به پا فره ایزدی </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قدش در لطافت که جانی است پاک </p></div>
<div class="m2"><p>فرو برده آب روان را به خاک </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر مانی آن روی دیدی یقین </p></div>
<div class="m2"><p>به هم برزدی صورت نقش چین </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خرامان قدش با رخ ماهتاب </p></div>
<div class="m2"><p>چو سروی که بار آورد آفتاب </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو خورشید ماهیش منظور بود </p></div>
<div class="m2"><p>ز سر تا قدم پایه نور بود </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>فرشته نهادی، پری پیکری</p></div>
<div class="m2"><p>لطیفی، ظریفی، هنر پروری </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ز سر تا به پا و ز پا تا به سر </p></div>
<div class="m2"><p>همه جان و دل بود و هوش و هنر </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دو گنجش نهان در دو کنج دهن </p></div>
<div class="m2"><p>نبودش در آن کنج گنج سخن </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ز شور لب لعل شیرین وی </p></div>
<div class="m2"><p>به تلخی همی داد جان جام می </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>به هر گوشه نرگسش دلربا </p></div>
<div class="m2"><p>در آن گوشه‌ها جاودان کرده جا </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جوانی به قد راست، چون نیشکر </p></div>
<div class="m2"><p>تراشیده اندام و بسته کمر </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لبانش سراسر ز قند و نبات </p></div>
<div class="m2"><p>دهانش لبالب ز آب حیات </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از او پر هنر‌تر جوانی نبود </p></div>
<div class="m2"><p>به حسن رخش دلستانی نبود </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز معشوق عاشق به خوبی بسی </p></div>
<div class="m2"><p>فرون بود و دانست این هر کسی </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خرد وزنشان کرد با یکدگر </p></div>
<div class="m2"><p>به شیرینی این بود از آن چرب‌تر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>در آیینه می‌دید رخسار خویش </p></div>
<div class="m2"><p>که او بود صد ره به از یار خویش </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ولی عشق را با چنین‌ها چه کار؟</p></div>
<div class="m2"><p>هوی پادشاهی‌ است بس کامگار </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گهی خیمه را بر سرابی زند </p></div>
<div class="m2"><p>گهی بر کند، بر سر آبی زند </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گهش راه روم است و گه ز نگبار </p></div>
<div class="m2"><p>گهش جای هند است و گه قندهار </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شهنشاه را مونس و یار بود </p></div>
<div class="m2"><p>شب و روز دلجوی و دلدار بود </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>مه و سالشان چون مه و آفتاب </p></div>
<div class="m2"><p>نظر بود با همه به روز شباب </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>کشیدی گه و بیگه از جام کی </p></div>
<div class="m2"><p>به شادی روی دلارام می </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>چو چشم و لب خویشتن کامیاب </p></div>
<div class="m2"><p>گهی در شکار و گهی در شراب </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو ابروی خود گاه در بوستان </p></div>
<div class="m2"><p>کشیدند بر گلستان سایه‌بان </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چو خورشید تابان به فصل بهار </p></div>
<div class="m2"><p>مبارک شده هر دو بر روزگار </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>« چو شیر و شکر با هم آمیخته » </p></div>
<div class="m2"><p>چو جان و خرد در هم آمیخته </p></div></div>