---
title: >-
    بخش ۳ - در ستایش پیامبر 
---
# بخش ۳ - در ستایش پیامبر 

<div class="b" id="bn1"><div class="m1"><p>رسولی که پا بر عرش سود </p></div>
<div class="m2"><p>ز پایش سر عرش را تاج بود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلند آفتاب مبارک نظر </p></div>
<div class="m2"><p>که او راست هر دو عالم اثر </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسول کریم و متاع امین </p></div>
<div class="m2"><p>امام الوری، قدوه العالمین </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی جبرئیلش بود میر بار </p></div>
<div class="m2"><p>گهی عنکبوتش بود پرده‌دار </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امام شش و هفت و سی بار ده </p></div>
<div class="m2"><p>سپهر و دو مه و چارده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد از نافه مشگ عبد مناف </p></div>
<div class="m2"><p>معطر حرم کان زمین راست ناف </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از اینجا براقش توجه نبود </p></div>
<div class="m2"><p>به جائی که آنجایگه جای نبود </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یک پی بساط فلک در نوشت </p></div>
<div class="m2"><p>چو تیر از کمان فلک در گذشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چنین رفت تا سدره المنتها </p></div>
<div class="m2"><p>به ملکی گذر کرد بی منتها </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کجا دایه رحمتش داد شیر </p></div>
<div class="m2"><p>مسیحا شد آنجاش طفلی به شیر </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر معجز یوسف از ماهی است </p></div>
<div class="m2"><p>تو خورشیدی و معجزت ماهی است </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهاده قدم بر سر آسمان </p></div>
<div class="m2"><p>نینداخته سایه بر خاکدان </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز یونس به احمد همان است راه </p></div>
<div class="m2"><p>که از قعر ماهی است تا اوج ماه </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>همه عقل و روح است و روحی لدیه </p></div>
<div class="m2"><p>ایا معشر الناس، صلوا علیه</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پس از شکر دادار، نعمت نبی است </p></div>
<div class="m2"><p>وز آن پس عائی که فرض است چیست؟</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دعای شهنشاه دیهیم و گاه </p></div>
<div class="m2"><p>پدر بر پدر خسرو و پادشاه </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>فشاننده گنج دریا به بزم </p></div>
<div class="m2"><p>دراننده قلب خارا به رزم </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فرازنده پایه سروری </p></div>
<div class="m2"><p>فروزنده ماه نیک اختری </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سپهر از کمر بستگان درش </p></div>
<div class="m2"><p>ظفر یک سپاهی است از لشکرش </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>کجا لشکر عزم او سیر کرد </p></div>
<div class="m2"><p>رود چرخ گردنده آنجا به گرد </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بر آفاق گسترده ظل همای </p></div>
<div class="m2"><p>در آن سایه آسوده خلق خدای </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز یک سوی ظلم است و یکسو امان </p></div>
<div class="m2"><p>چه سدی است شمشیر او در میان </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ز شیر درفشش درفشان ظفر </p></div>
<div class="m2"><p>چو از خانه شیر تابنده خور </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نیبند شبیهش بصر جز به خواب </p></div>
<div class="m2"><p>نیابد نظیرش نظر جز در آب </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر از کوه پرسی که در بحر و بر </p></div>
<div class="m2"><p>که زیبد که بندند پیشش کمر؟</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به لفظ صدا پاسخ آید ز کوه </p></div>
<div class="m2"><p>که سلطان اویس آسمان شکوه </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>الا ای جهاندار پیروز بخت </p></div>
<div class="m2"><p>سزاوار شاهی و زیبای تخت </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سر فرقدان پایه تخت تست </p></div>
<div class="m2"><p>بلند آسمان سایه بخت توست </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نگینی است خورشید بر افسرت </p></div>
<div class="m2"><p>حبابی است ناهید در ساغرت </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زمین و زمانه به کام تواند </p></div>
<div class="m2"><p>همه پادشاهان غلام تواند </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شب مملکت را مه و اختری </p></div>
<div class="m2"><p>تن سلطنت را سر و افسری </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زهی در تن مملکت جاودان </p></div>
<div class="m2"><p>وجود تو چون جان و حکمت روان </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>کسی را که کین تواش داد تاب </p></div>
<div class="m2"><p>ندادش جز از چشمه تیغ آب </p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر حمله بر کوه خارا کنی </p></div>
<div class="m2"><p>چو خاشاکش از جای خود بر کنی </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>به عهد تو خونریز شد بی دریغ </p></div>
<div class="m2"><p>چنین واجب الحد از آنست تیغ </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قلم کرد تزویر در عهد شاه </p></div>
<div class="m2"><p>بریدش زبان، کرد درویش سیاه </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خدایت همه هر چه بایست داد </p></div>
<div class="m2"><p>جوانمردی و دانش و دین و داد </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>ترا داد رسم است و بخشش طریق </p></div>
<div class="m2"><p>همین کن که توفیق بادت رفیق </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>مراد از جهان نام نیک است و بس </p></div>
<div class="m2"><p>بجز نام نیکو نماند به کس </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>جهان راست حاصل همه چیز لیک </p></div>
<div class="m2"><p>چه با خود توان برد جز نام نیک؟</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بخوان قصه خسروان جوان </p></div>
<div class="m2"><p>ز هوشنگ و جم تا به چنگیز خان </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>که گر عکس شمشیرشان آفتاب </p></div>
<div class="m2"><p>بدیدی، اسد را شدی زهره آب </p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز چندین زر و افسر و تخت و گنج </p></div>
<div class="m2"><p>که کردند حاصل به سختی و رنج </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>بجز نام نیکو ازین انجمن </p></div>
<div class="m2"><p>ببین تا چه بردند با خویشتن؟</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>شنیدم که می‌گفت بهرام گور </p></div>
<div class="m2"><p>پدر را کز او شد جهان پر ز شور </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>که:« آه ضعیفان به گردون رسید </p></div>
<div class="m2"><p>سرشک یتیمان به جیحون رسید </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>از آن ترسم ای شهریار جوان </p></div>
<div class="m2"><p>که اشک ستمدیدگان ناگهان </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>فراهم شود، ملک گردد خراب </p></div>
<div class="m2"><p>برد جاه ما را به یکباره آب‌» </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چو بشنید، در دیده آورد آب </p></div>
<div class="m2"><p>بپیچید مردانه دادش جواب </p></div></div>
<div class="b" id="bn50"><div class="m1"><p>که ایزد تو را بخشش و داد داد </p></div>
<div class="m2"><p>به من در ازل جور و بیداد داد </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تو را آن، نصیب من این آمدست </p></div>
<div class="m2"><p>چه تدبیر؟ قسمت چنین آمدست </p></div></div>
<div class="b" id="bn52"><div class="m1"><p>مرا جز که بی معدلت نیست رای </p></div>
<div class="m2"><p>ولی غیر از این است حکم خدای </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>درختی است عدل ملک بارور </p></div>
<div class="m2"><p>که بیخش دوام است و دولت ثمر </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اساس بقا عدل ثابت کند </p></div>
<div class="m2"><p>درخت سعدات ستم برکند </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نبی ملک را گفت دین تواُم است </p></div>
<div class="m2"><p>حقیقت بدان کان بدین قائم است </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>قیامت که آنجاست قاضی خدای </p></div>
<div class="m2"><p>برابر نشینند شاه و گدای </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>اگر عدل باشد گوای ملک </p></div>
<div class="m2"><p>شود عرش ثابت برای ملک </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بود ساعتی عدل دارای دین </p></div>
<div class="m2"><p>ز هفتاد ساله عبادت گزین </p></div></div>
<div class="b" id="bn59"><div class="m1"><p>صبوح سعدات صباح تو باد </p></div>
<div class="m2"><p>جنود ملائک جناح تو باد </p></div></div>
<div class="b" id="bn60"><div class="m1"><p>کسی را که با تست سر در غرور </p></div>
<div class="m2"><p>کلاه از سر و سر ز تن باد دور </p></div></div>