---
title: >-
    بخش ۸ - پائیز
---
# بخش ۸ - پائیز

<div class="b" id="bn1"><div class="m1"><p>به وقتی که باد خزان خاستی </p></div>
<div class="m2"><p>رزان را به زیور بیاراستی </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای مخالف زدی باغ را </p></div>
<div class="m2"><p>شدی زرد و بیمار شاخ از هوا </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خزان بر رزان دامن افشاندی </p></div>
<div class="m2"><p>چراغ گل و لاله بنشاندی </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زمانی شدی بید بن تیغ بار </p></div>
<div class="m2"><p>دمی باد می‌برد دست چنار </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سوز فراق سمن یافت داغ </p></div>
<div class="m2"><p>از آن جامه زرد پوشید باغ </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبینی که خور پشت چون برکند، </p></div>
<div class="m2"><p>زمین جامه رزد در بر کند </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اوان جوانی و فصل بهار </p></div>
<div class="m2"><p>همه رنگ و بوی است و نقش و نگار </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خزان است ایام پیری و مرگ </p></div>
<div class="m2"><p>شود روی زرد و برد باد برگ </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار ار نبودی خزان کی شدی؟</p></div>
<div class="m2"><p>چنین زرد روی رزان کی شدی؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>رخ زرد به را گرفتی غبار </p></div>
<div class="m2"><p>به خون سرخ می‌کرد دندان انار </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ز بی برگی از بس که بر سر چنار </p></div>
<div class="m2"><p>زدی دست دستش فتادی ز کار </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بسی آب نالید و بر خود گریست </p></div>
<div class="m2"><p>که زنجیر بر گردن من ز چیست؟</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بسم نیست این کاندرین روز چند </p></div>
<div class="m2"><p>هوا کرد خواهد مرا تخته بند؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بساط رزان بود در زر نهان </p></div>
<div class="m2"><p>چو بزم جهانبخش گیتی ستان </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>به فصلی چنان شاه پیروز بخت </p></div>
<div class="m2"><p>سر آب جستی و پای درخت </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>می‌ زرد زرین چو برگ رزان </p></div>
<div class="m2"><p>کشیدندی اندر هوای خزان </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نسیم خزانی چو برخاستی </p></div>
<div class="m2"><p>همه بزم مستان بیاراستی </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ملک در خزان داشتی نوبهار </p></div>
<div class="m2"><p>درختش برومند و باغش به بار </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گزیدی لب یار را بی حجیب </p></div>
<div class="m2"><p>گرفتی ز نخدان سیمین چو سیب </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به حسن ار چه سیب از میان برد گو، </p></div>
<div class="m2"><p>ز نخدان زد او با ز نخدان او </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگر چه زند خنده شیرین انار </p></div>
<div class="m2"><p>به خود خندد او با لب لعل یار </p></div></div>