---
title: >-
    بخش ۱۰ - شب
---
# بخش ۱۰ - شب

<div class="b" id="bn1"><div class="m1"><p>شبی همچو روز قیامت دراز </p></div>
<div class="m2"><p>پریشان چو موی بتان طراز </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوا نقطه‌ای بود گفتی سیاه </p></div>
<div class="m2"><p>ز تاریکیش چرخ گم کرده راه </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه روشنان فلک گشته جمع </p></div>
<div class="m2"><p>شده طالب روشنایی چو شمع </p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گفتی که گردون نهان کرد مهر </p></div>
<div class="m2"><p>و یا ایزد از وی ببرید مهر </p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تهی گشته پستان گردون ز شیر </p></div>
<div class="m2"><p>بر اندوه درهای مشرق به قیر </p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سیه گشته چشم جهان سر به سر </p></div>
<div class="m2"><p>در او کس ندید از سپیدی اثر </p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهان گشته مرغان سبز آشیان </p></div>
<div class="m2"><p>سیاهی ز زاغ سیه طیلسان </p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو گفتی که راه هوا بسته‌اند </p></div>
<div class="m2"><p>همه بال در بال پیوسته‌اند </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ملک گفت تا مجلس آراستند </p></div>
<div class="m2"><p>ز ساقی گلچهره می‌خواستند </p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بیاراست بزمی چو باغ بهشت </p></div>
<div class="m2"><p>به رخسار خوبان حوری سرشت </p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به یک جای صد نازنین مست مل </p></div>
<div class="m2"><p>فراهم نشسته چو در غنچه گل </p></div></div>
<div class="b" id="bn12"><div class="m1"><p>می‌افکنده بر روی ساقی شعاع </p></div>
<div class="m2"><p>شده ماه و خورشید را اجتماع </p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بر حسن می حسن ساقی فزود </p></div>
<div class="m2"><p>همه خانه نور علی نور بود </p></div></div>
<div class="b" id="bn14"><div class="m1"><p>صراحی به گردن درش خون دن </p></div>
<div class="m2"><p>ز خونش قدح را لبالب دهن </p></div></div>
<div class="b" id="bn15"><div class="m1"><p>چو بنمود رامشگر از پرده راز </p></div>
<div class="m2"><p>همه برگ عیش از نوا کرد ساز </p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دلی پرده از غم نمی‌داشتی </p></div>
<div class="m2"><p>مغنی زدی پرده برداشتی </p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نوای دف و نی به هم گشت راست </p></div>
<div class="m2"><p>ز عشاق مشتاق فریاد خاست </p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو بلبل نمی‌گشت مطرب خموش </p></div>
<div class="m2"><p>به او داده گلچهرگان گوش هوش </p></div></div>
<div class="b" id="bn19"><div class="m1"><p>می اندر سر شاهدان تاخته </p></div>
<div class="m2"><p>ز اندیشه‌ها دل بپرداخته </p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز باد جوانی سر افشان شده </p></div>
<div class="m2"><p>به بستان همه پایکوبان شده </p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشسته به عشرت چو خورشید شه </p></div>
<div class="m2"><p>برابر ستاده مه چارده </p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در آن مجلس آن هر دو مه را نظر </p></div>
<div class="m2"><p>چو خورشید و مه بود با یکدیگر </p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به هر می که کردی شهنشاه نوش </p></div>
<div class="m2"><p>شهنشاه را گفتی آن ماه نوش </p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ملک ساغری با پری روی خورد </p></div>
<div class="m2"><p>چو جرعه پری رخ زمین بوس کرد </p></div></div>
<div class="b" id="bn25"><div class="m1"><p>سهی سرو خورشید را سجده برد </p></div>
<div class="m2"><p>به گلبرگ روی زمین را سترد </p></div></div>
<div class="b" id="bn26"><div class="m1"><p>که شاها درونت چو گل شاد باد! </p></div>
<div class="m2"><p>دل از بار چون سروت آزاد باد! </p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو تابنده مهری، زوالت مباد </p></div>
<div class="m2"><p>تو رخشند ماهی، وبالت مباد! </p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چراغ من از دولتت در گرفت </p></div>
<div class="m2"><p>مرا لطفت از خاک ره بر گرفت </p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سعادت مرا سایه بر سر فکند </p></div>
<div class="m2"><p>شد از خاک پایت سر من بلند </p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چو لطف تو در چاهم افتاده دید </p></div>
<div class="m2"><p>شدم دستگیر و مرا بر کشید </p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شها از جهان سایه‌ات کم مباد! </p></div>
<div class="m2"><p>جهان بی رضای تو یک دم مباد! </p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تویی آن دلفروز و شمع جهان </p></div>
<div class="m2"><p>که گیرد ز نورت چراغ آسمان </p></div></div>
<div class="b" id="bn33"><div class="m1"><p>منم همچو پروانه شیدای تو </p></div>
<div class="m2"><p>سر مردنم هست در پای تو</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>امیدم ز لطف خداوندگار </p></div>
<div class="m2"><p>فزون زین نمی‌باشد ای شهریار </p></div></div>
<div class="b" id="bn35"><div class="m1"><p>که چون خاک سازند بستر مرا </p></div>
<div class="m2"><p>تو باشی در آن حال بر سر مرا </p></div></div>
<div class="b" id="bn36"><div class="m1"><p>چو خسرو سخن‌های شیرین شنید </p></div>
<div class="m2"><p>ز شیرینی‌اش لب به دندان گزید </p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ز ناز دو چشمش ملک مست بود </p></div>
<div class="m2"><p>ز سودای او رفته از دست بود </p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بدو گفت ای سرو دلجوی من </p></div>
<div class="m2"><p>گل مهربان وفا خوی من </p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همه روز‌ه‌ام یار و مونس توئی </p></div>
<div class="m2"><p>شب تیره‌ام شمع مجلس توئی </p></div></div>
<div class="b" id="bn40"><div class="m1"><p>توای آنکه گوئیز سر تا به پای </p></div>
<div class="m2"><p>به دلخواه من آفریدن خدای </p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پری یا ملک، یا بنی آدمی </p></div>
<div class="m2"><p>چو انسان عینی، همه مردمی </p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تو عمری، از آن نیست هیچت وفا </p></div>
<div class="m2"><p>چو صبحی، که پیوسته بادت بقا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سعادت رفیق جوانیت باد </p></div>
<div class="m2"><p>فزون از همه زندگانیت باد </p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نکوئی ز حسن نکوئی تو را </p></div>
<div class="m2"><p>چه می‌باید ای دوست غیر از وفا </p></div></div>
<div class="b" id="bn45"><div class="m1"><p>به بازی سخن تلخ می‌گفت شاه </p></div>
<div class="m2"><p>چو آتش برافروخت زین طیره ماه </p></div></div>
<div class="b" id="bn46"><div class="m1"><p>رخ شمع مجلس پر از تاب شد </p></div>
<div class="m2"><p>در آن تاب چشمش پر ازآب شد </p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گهر ریخت از جزع و در از عقیق </p></div>
<div class="m2"><p>به آواز گفت ای سروشت رفیق </p></div></div>
<div class="b" id="bn48"><div class="m1"><p>منم بنده شاه تا زنده‌ام </p></div>
<div class="m2"><p>به سر در رکاب تو تا زنده‌ام </p></div></div>
<div class="b" id="bn49"><div class="m1"><p>چنین بی‌وفا از چه خوانی مرا؟</p></div>
<div class="m2"><p>بجور از در خود، چه رانی مرا؟</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>ترا کار، شاهی، مرا بندگی است </p></div>
<div class="m2"><p>درین راه رسمم سرافکندگی است </p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو در زندگانی جفا می‌برم </p></div>
<div class="m2"><p>من این زندگانی کجا می‌برم؟</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>چو من بی‌وفایم همان به که من </p></div>
<div class="m2"><p>نیایم ازین پس درین انجمن </p></div></div>
<div class="b" id="bn53"><div class="m1"><p>بگفت این و برخاست از پیش شاه </p></div>
<div class="m2"><p>ز مجلس بتابید رخشنده ماه </p></div></div>
<div class="b" id="bn54"><div class="m1"><p>چو آزاد سروی پر از باد سر </p></div>
<div class="m2"><p>روان گشت و از مجلس آمد بدر </p></div></div>
<div class="b" id="bn55"><div class="m1"><p>روان رفت و آورد پا در رکاب </p></div>
<div class="m2"><p>دلی پر ز تاب و سری پر عتاب </p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تکاور برانگیخت مانند باد </p></div>
<div class="m2"><p>سراندر بیابان و صحرا نهاد </p></div></div>
<div class="b" id="bn57"><div class="m1"><p>گهش سایه می‌ماند باز از رکاب </p></div>
<div class="m2"><p>گهی در پیش قطره می‌زد سحاب </p></div></div>
<div class="b" id="bn58"><div class="m1"><p>ز خاک زمین داشت گردی هوا </p></div>
<div class="m2"><p>که بر دامنش می‌نشینی چرا؟</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>از آن رو که بر تخت او پشت کرد </p></div>
<div class="m2"><p>چه بنشست در وجه او غیر گرد </p></div></div>
<div class="b" id="bn60"><div class="m1"><p>جهان را همه ساله آئین و خوست </p></div>
<div class="m2"><p>جدائی فکندن میان دو دوست </p></div></div>
<div class="b" id="bn61"><div class="m1"><p>همانا حسد برد بر حالشان </p></div>
<div class="m2"><p>زمانه تبه کرد احوالشان </p></div></div>
<div class="b" id="bn62"><div class="m1"><p>رخ عشقشان گرچه بس خوب بود </p></div>
<div class="m2"><p>از آرایش هجر محجوب بود </p></div></div>
<div class="b" id="bn63"><div class="m1"><p>از آن تا بدانند قدر وصال </p></div>
<div class="m2"><p>به هجران فلک دادشان گوشمال </p></div></div>
<div class="b" id="bn64"><div class="m1"><p>کسی تا به هجران نشد پایمال </p></div>
<div class="m2"><p>ندانست قدر زمان وصال </p></div></div>
<div class="b" id="bn65"><div class="m1"><p>وصال آورد رخنه در کار عشق </p></div>
<div class="m2"><p>جدائی کند گرم بازار عشق </p></div></div>
<div class="b" id="bn66"><div class="m1"><p>ازین سوی شبگیر چون شاه چین </p></div>
<div class="m2"><p>در آورد خنگ فلک را به زین </p></div></div>
<div class="b" id="bn67"><div class="m1"><p>در آمد از آن خواب نوشین ملک </p></div>
<div class="m2"><p>پریشان ز غوغای دوشین ملک </p></div></div>
<div class="b" id="bn68"><div class="m1"><p>دلش بود در بند سودای یار </p></div>
<div class="m2"><p>وز آن مستی دوش در سر خمار </p></div></div>
<div class="b" id="bn69"><div class="m1"><p>ندانست کز دست بازش برفت </p></div>
<div class="m2"><p>دل آزرده شد دلنوازش برفت </p></div></div>
<div class="b" id="bn70"><div class="m1"><p>یکی گفت کان روشنائی چشم </p></div>
<div class="m2"><p>شب تیره شد در سیاهی به چشم </p></div></div>
<div class="b" id="bn71"><div class="m1"><p>شهنشاه پیچید در خویشتن </p></div>
<div class="m2"><p>ولی راز نگشود بر انجمن </p></div></div>
<div class="b" id="bn72"><div class="m1"><p>دل از بزم یکبارگی بر رفت </p></div>
<div class="m2"><p>به ترک می و جام و ساغر گرفت </p></div></div>
<div class="b" id="bn73"><div class="m1"><p>می از دست ساقی نمی‌کرد نوش </p></div>
<div class="m2"><p>به گفتار مطرب نمی‌داد گوش </p></div></div>
<div class="b" id="bn74"><div class="m1"><p>نمی‌داد در پیش خود راه نی </p></div>
<div class="m2"><p>همی ریخت بر خاک ره خون می </p></div></div>
<div class="b" id="bn75"><div class="m1"><p>گهی سنگ زد بر سبوی شراب </p></div>
<div class="m2"><p>گه از کاسه بر بست دست رباب </p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گذشت از گل و باغ و صحرا همه </p></div>
<div class="m2"><p>که با یار خوش باشد آنها همه </p></div></div>
<div class="b" id="bn77"><div class="m1"><p>نه پروای باز و نه رای شکار </p></div>
<div class="m2"><p>که بازش نمی‌آمد آنجا به کار </p></div></div>
<div class="b" id="bn78"><div class="m1"><p>ندیدی به غیر از خیال رخش </p></div>
<div class="m2"><p>نجستی به جز طلعت فرخش </p></div></div>
<div class="b" id="bn79"><div class="m1"><p>ملک چون جدا ماند از یار خویش </p></div>
<div class="m2"><p>خیال نگارینش آمد به پیش </p></div></div>
<div class="b" id="bn80"><div class="m1"><p>خیالی نمودش سحرگاه دوست </p></div>
<div class="m2"><p>شد از جای و برجست و پنداشت اوست </p></div></div>
<div class="b" id="bn81"><div class="m1"><p>گهی دست کردی چو زلفش دراز </p></div>
<div class="m2"><p>که چون گیسویش در برآرد به ناز </p></div></div>
<div class="b" id="bn82"><div class="m1"><p>به غیر از خیال رخ دلبرش</p></div>
<div class="m2"><p>نیامد شب تیره کس بر سرش </p></div></div>
<div class="b" id="bn83"><div class="m1"><p>چو آغوش بهر کنارش گشود </p></div>
<div class="m2"><p>نظر کردش اندر میان هیچ بود </p></div></div>
<div class="b" id="bn84"><div class="m1"><p>به خورشید گفتی بر آن رخ متاب </p></div>
<div class="m2"><p>مبادا که آزرده گردد ز تاب </p></div></div>
<div class="b" id="bn85"><div class="m1"><p>به باد صبا لابه کردی سحر </p></div>
<div class="m2"><p>که آهسته بر راه او می‌گذر </p></div></div>
<div class="b" id="bn86"><div class="m1"><p>مبادا که چشمش که خوش خفته است </p></div>
<div class="m2"><p>همان زلف مشکین که آشفته است</p></div></div>
<div class="b" id="bn87"><div class="m1"><p>به آواز پایت در آید ز خواب </p></div>
<div class="m2"><p>رود از حدیث تو ناگه به تاب </p></div></div>
<div class="b" id="bn88"><div class="m1"><p>دلم را ز خاک درش باز جو </p></div>
<div class="m2"><p>وگر یابی آنجاش آهسته گو </p></div></div>
<div class="b" id="bn89"><div class="m1"><p>که من دورم ای دل ز جانان تو </p></div>
<div class="m2"><p>تو با جان خوشی، ای خوشا جان تو </p></div></div>
<div class="b" id="bn90"><div class="m1"><p>تو نزدیکی ای دل بر آن دل گسل </p></div>
<div class="m2"><p>مرا چاره‌ای کن که دورم ز دل </p></div></div>
<div class="b" id="bn91"><div class="m1"><p>شب تیره‌اش دیده دمساز بود </p></div>
<div class="m2"><p>خروش و فغانش هم آواز بود </p></div></div>
<div class="b" id="bn92"><div class="m1"><p>ز سودای دل نامه‌ای زد رقم </p></div>
<div class="m2"><p>سیاهی ز دل ساخت مژگان قلم </p></div></div>