---
title: >-
    بخش ۱ - فراقنامه 
---
# بخش ۱ - فراقنامه 

<div class="b" id="bn1"><div class="m1"><p>به نام خدایی که با تیره خاک</p></div>
<div class="m2"><p>بر آمیخت این جوهر جان پاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو با یکدگر کردشان آشنا</p></div>
<div class="m2"><p>دگر بارشان کرد از هم جدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که دانست کان آشنائی چه بود؟</p></div>
<div class="m2"><p>پس از آشنائی جدائی چه بود؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درین پرده کس را ندادند بار</p></div>
<div class="m2"><p>نمی‌داند این راز جز کردگار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بوئی که در نافه افزون کند</p></div>
<div class="m2"><p>بسی آهوان را جگر خون کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدف تا کند دانه در پدید</p></div>
<div class="m2"><p>بسی شور و تلخش بباید چشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر افراخت نه پرده لاجورد</p></div>
<div class="m2"><p>ده و دو مقام اندر و راست کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بهر پرده و هر مقامی که ساخت</p></div>
<div class="m2"><p>یکی را زد و دیگری را نواخت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شکر را زنی خانه‌ای بر فراخت</p></div>
<div class="m2"><p>گره کاری و بند گیریش ساخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مگس خواست حلوائی از خوان او</p></div>
<div class="m2"><p>عسل آیتی گشت در شان او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>خداوند هفت آسمان و زمین</p></div>
<div class="m2"><p>زمین گستر و آسمان آفرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>ز خورشید مه را جدائی دهد</p></div>
<div class="m2"><p>شب و روزشان روشنائی دهد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نپرسی چرا اختر و آسمان</p></div>
<div class="m2"><p>شب و روز گردند گرد جهان؟</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مپندار کین بی سبب می‌کنند</p></div>
<div class="m2"><p>خداوند خود را طلب می‌کنند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نمی‌گنجد او در تمنای تو</p></div>
<div class="m2"><p>تو او را بجو کوست جویای تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گل ما بنا کرده قدرتش</p></div>
<div class="m2"><p>دل ما سرا پرده عزتش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به نورش دو چشم جهان ناظر است</p></div>
<div class="m2"><p>از آن نور مردم شده ظاهر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خداوند چار و دو و سه یکی است</p></div>
<div class="m2"><p>بماند شش و چار و نه اندکی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>به مسمار هفت اخترش دوخته</p></div>
<div class="m2"><p>فلک حلقه‌ای بر درش دوخته</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به حکمت رسانیده است آن بدین</p></div>
<div class="m2"><p>روان ز آسمان و تن از زمین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>فزون از زمین است تا آسمان</p></div>
<div class="m2"><p>تفاوت مر این هر دو را در میان</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>دگر بارشان کرد از هم جدا</p></div>
<div class="m2"><p>دو بیگانه با هم شدند آشنا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>که آن از گل تیره است این ز نور</p></div>
<div class="m2"><p>ز تن تا به جان نسبتی هست دور</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به زاری و حسرت جدا می‌شوند</p></div>
<div class="m2"><p>چو با یکدگر آشنا می‌شوند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نمی‌بودشان کاشکی اتصال</p></div>
<div class="m2"><p>چون جان را و تن را چنین بود حال</p></div></div>