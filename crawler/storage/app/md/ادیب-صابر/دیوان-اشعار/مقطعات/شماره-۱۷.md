---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>ز روزگار مرا خار هست و خرما نیست</p></div>
<div class="m2"><p>مثل خطاست که گویند خار با خرماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک نزد فلک کمترم از خورشید</p></div>
<div class="m2"><p>نصیب او همه زر و نصیب من گرماست</p></div></div>