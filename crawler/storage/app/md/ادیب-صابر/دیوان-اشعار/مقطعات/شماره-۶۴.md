---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>نه طالعی که امانم دهد ز خشم خدا</p></div>
<div class="m2"><p>نه نعمتی که بدو خلق را کنم خشنود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شده ست معصیت و مفلسی بضاعت من</p></div>
<div class="m2"><p>بدین بضاعت ناقص چه سود خواهد بود</p></div></div>