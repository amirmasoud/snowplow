---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>فرو بارید طوفان بر سر من</p></div>
<div class="m2"><p>چو از پیری مرا مجروح شد روح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بماندم از قدم تا فرق در غرق</p></div>
<div class="m2"><p>کزین طوفان نه کشتی ماند نه نوح</p></div></div>