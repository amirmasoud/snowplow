---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>با هر آن دوست که گویم غم خویش</p></div>
<div class="m2"><p>غم او از غم من بیشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کز او مرهم دل می طلبم</p></div>
<div class="m2"><p>دل او از دل من ریش تر است</p></div></div>