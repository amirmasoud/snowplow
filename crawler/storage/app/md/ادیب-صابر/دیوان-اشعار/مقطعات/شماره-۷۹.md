---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>زاهل جود و سخاوت زمانه خای ماند</p></div>
<div class="m2"><p>چه جرم مفلسی خویش بر زمانه نهم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمانه ای که خود از مفلسی همی نرهد</p></div>
<div class="m2"><p>دراین زمانه من از مفلسی چگونه رهم</p></div></div>