---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>زنفس او به لطافت همی رسند نفوس</p></div>
<div class="m2"><p>ز عقل او متحیر همی شوند عقول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گاه عزم دلیر و به گاه حزم حذور</p></div>
<div class="m2"><p>گه غضب متانی، به گاه عفو عجول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدار علم و عمل بر لطافتش مقصور</p></div>
<div class="m2"><p>صلاح دولت و دین بر اشارتش موکول</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی مناقب اسلاف تو کمال خطب</p></div>
<div class="m2"><p>زهی محاسن اوصاف تو جمال فصول</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سپاس و شکر تو برگردن زمان و زمین</p></div>
<div class="m2"><p>نثار مدح تو در خاطر کبار و فحول</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه همت تو شناسد به بذل مال ملال</p></div>
<div class="m2"><p>نه حشمت تو نماید ز راه عدل عدول</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به لطف یک شرر است از ماثر تو، اثیر</p></div>
<div class="m2"><p>به طبع یک اثر است از شمایل تو شمول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز وصف ذات تو قاصر بود بنان و بیان</p></div>
<div class="m2"><p>ز زخم کلک تو عاجز بود فصال و فصول</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شرف ز علم تو یابد همی قلیل و کثیر</p></div>
<div class="m2"><p>شرف ز علم تو گیرد همی فروع و اصول</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمین غم ندهد جز به دشمن تو نزل</p></div>
<div class="m2"><p>قضای بد نکند جز به حاسد تو نزول</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو چرخ بذل و عطایی و اخترت منصف</p></div>
<div class="m2"><p>تو بحر فضل و سخایی و گوهرت مبذول</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چنین عطا که تو بخشی ز چرخ ناممکن</p></div>
<div class="m2"><p>چنین سخا که تو ورزی ز بحر نامعطول</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو بی عطای تو باشد سخا بود مختل</p></div>
<div class="m2"><p>چو بی ثنای تو ماند سخن شود معلول</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ستایش تو چرا زاید از جبلت من</p></div>
<div class="m2"><p>اگر نه در دل من شد هوای تو مجبول</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نوازش چو منی نیست کار هر معطی</p></div>
<div class="m2"><p>ستایش چو تویی نیست کار هر مجهول</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چگونه وصف کمال و فضایل تو کنند</p></div>
<div class="m2"><p>جماعتی که ندانند فاضل از مفضول</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بقای ذکر بود لایق خداوندان</p></div>
<div class="m2"><p>چو ذکر نیک نماند چه عرض ماند و طول</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ز کاخ و باغ بدیع و زمال و ملک عزیز</p></div>
<div class="m2"><p>چو روزگار برآمد چه حاصل و محصول</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو ختم عمر به تن راه یافت، ره یابد</p></div>
<div class="m2"><p>بدین فنا و زوال و بدان رسوم و طلول</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یکی به حال بزرگان پیشتر بنگر</p></div>
<div class="m2"><p>ز پادشاه و وزیر و زقابل و مقبول</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>همی به شعر شناسد هر آنکه بشناسد</p></div>
<div class="m2"><p>دخولشان ز خروج و خروجشان ز دخول</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز بهر ذکر همی گویم این چنین اشعار</p></div>
<div class="m2"><p>چو ذکر ماند نخواهد، چه قایل و چه مقول</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>به شعر بد نتوان ذکر نیک حاصل کرد</p></div>
<div class="m2"><p>ابی کعب عزیز است نی ابی سلول</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>همیشه تا که ز نصرت جدا بود خذلان</p></div>
<div class="m2"><p>همیشه باش تو منصور و حاسدت مخذول</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همیشه تا نبود عز چو ذل و نیک چو بد</p></div>
<div class="m2"><p>عزیز باش و بد اندیش تو ذلیل و ذلول</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ز روز عید تو را باد عیش ها حاصل</p></div>
<div class="m2"><p>به ماه روزه تو را باد خیرها مقبول</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مه مراد تو را ناسپرده پای محاق</p></div>
<div class="m2"><p>گل بقای تو را ناببرده دست ذبول</p></div></div>