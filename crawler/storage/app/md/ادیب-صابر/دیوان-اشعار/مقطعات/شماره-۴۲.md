---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>قرب یک ماه شد که در شب روز</p></div>
<div class="m2"><p>چشم من ماه و آفتاب ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر آن خانه ام که در همه عمر</p></div>
<div class="m2"><p>هیچ جغدی چنان خراب ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز آتش دل کباب شد جگرم</p></div>
<div class="m2"><p>ز آتش دل کسی کباب ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا در این خانه ام ز بیداری</p></div>
<div class="m2"><p>دیده من خیال خواب ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس حدیث مرا جواب نداد</p></div>
<div class="m2"><p>کس خلاص مرا صواب ندید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ مومن چنین عتاب نیافت</p></div>
<div class="m2"><p>هیچ کافر چنین عذاب ندید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچنان می خورم طعام و شراب</p></div>
<div class="m2"><p>که کسی خواب دید و آب ندید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ مصلح چنین طعام نخورد</p></div>
<div class="m2"><p>هیچ مفسد چنین شراب ندید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بی خطا بر من این خطاب چراست</p></div>
<div class="m2"><p>بی خطا کس چنین خطاب ندید</p></div></div>