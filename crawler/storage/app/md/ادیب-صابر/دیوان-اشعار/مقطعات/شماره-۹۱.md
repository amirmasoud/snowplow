---
title: >-
    شمارهٔ ۹۱
---
# شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>دل من مهر آن گزید که او</p></div>
<div class="m2"><p>بسته دارد میان به کینه من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من زدشمن چگونه پرهیزم</p></div>
<div class="m2"><p>دشمن من میان سینه من</p></div></div>