---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>اهل عطا کسی است که فضلی بود در او</p></div>
<div class="m2"><p>نبود هر آن عطا که بدین کس دهی خطا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناکس بود کسی که در او هیچ فضل نیست</p></div>
<div class="m2"><p>ناکس بود کسی که به ناکس دهد عطا</p></div></div>