---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>بودم از روز جوانی هر نفس در لذتی</p></div>
<div class="m2"><p>زان چنین در حسرت روز جوانی مانده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لذتی از زندگانی نیست در پیری مرا</p></div>
<div class="m2"><p>زانکه در بیم زوال زندگانی مانده ام</p></div></div>