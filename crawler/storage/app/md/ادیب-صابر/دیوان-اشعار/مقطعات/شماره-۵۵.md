---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>به هنر فخر کن مکن به گهر</p></div>
<div class="m2"><p>نه همه فخر از آب و گل باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنده ای کو به مرده فخر کند</p></div>
<div class="m2"><p>نه همانا که زنده دل باشد</p></div></div>