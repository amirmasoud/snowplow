---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>قوتم با نام برنایی برفت</p></div>
<div class="m2"><p>بار ضعف از وام پیری می کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیستم یک لحظه بی رنج خمار</p></div>
<div class="m2"><p>تا شراب از جام پیری می کشم</p></div></div>