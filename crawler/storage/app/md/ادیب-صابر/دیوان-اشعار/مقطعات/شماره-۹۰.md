---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای دو چشم اجل به تو نگران</p></div>
<div class="m2"><p>چند خندی زگریه دگران</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لقب تو چه سود صدر اجل</p></div>
<div class="m2"><p>چون اجل هست سوی تو نگران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اجل از تو کران نخواهد کرد</p></div>
<div class="m2"><p>گر بگیری جهان کران به کران</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند نازی که معتبر شده ام</p></div>
<div class="m2"><p>بنخواهند مرد معتبران</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پی دفع مرگ و حفظ حیات</p></div>
<div class="m2"><p>حیله ها ساختند حلیه گران</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به هنر قصد مرگ دفع نشد</p></div>
<div class="m2"><p>تا بمردند همچو بی هنران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بینم از بهر مال عاریتی</p></div>
<div class="m2"><p>پدران اوفتاده در پسران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بی خطر نعمتی بود که رسد</p></div>
<div class="m2"><p>پسران را ز مردن پدران</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه بر وی نشست نام فنا</p></div>
<div class="m2"><p>بی خطر گشت نزد با خطران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مال و ملکی که بر گذر باشد</p></div>
<div class="m2"><p>نکند عاقل اعتماد بر آن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر همی ملک بی گذر طلبی</p></div>
<div class="m2"><p>دل منه بر زمانه گذران</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>از پی این جهان بی سر و بن</p></div>
<div class="m2"><p>چون همی سر فدا کنند سران</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آخر از کارها خبر یابند</p></div>
<div class="m2"><p>روزی این غافلان بی خبران</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت مردن ضعیف دل گردند</p></div>
<div class="m2"><p>این قوی گردنان بی جگران</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کار و کردار ما همی شمرند</p></div>
<div class="m2"><p>این رقیبان نیک و بد شمران</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>همه غمها سبک شود بر دل</p></div>
<div class="m2"><p>گر ترازو بود به حشر گران</p></div></div>