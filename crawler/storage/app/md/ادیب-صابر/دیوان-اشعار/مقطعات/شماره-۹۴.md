---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>شوم به بدرقه لا اله الا الله</p></div>
<div class="m2"><p>ز راه آخرت از خوف خاتمت ایمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسند عفو گناه مرا به نزد خدای</p></div>
<div class="m2"><p>رسول و دوستی اهل بیت او ضامن</p></div></div>