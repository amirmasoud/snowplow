---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>اگر پیری مرا در خانه بنشاند</p></div>
<div class="m2"><p>بسا رنجا کز آن آسودم اکنون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبینم هر کرا طبعم نخواهد</p></div>
<div class="m2"><p>چو نیکو بنگری بر سودم اکنون</p></div></div>