---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>چرا تفاخر جویی بر این و آن به لقب</p></div>
<div class="m2"><p>چرا تکبر برزی بر این و آن به خطاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر از خطاب و لقب هیچ کس بزرگ شود</p></div>
<div class="m2"><p>ربود گوی بزرگی جریده القاب</p></div></div>