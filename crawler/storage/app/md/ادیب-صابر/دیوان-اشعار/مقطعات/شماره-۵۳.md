---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>به ماتم نشستی به مرگ زنت</p></div>
<div class="m2"><p>از این پس به مرگ تو ماتم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنت مرد چون تو نمیری همی</p></div>
<div class="m2"><p>چه مردی بود کز زنی کم بود</p></div></div>