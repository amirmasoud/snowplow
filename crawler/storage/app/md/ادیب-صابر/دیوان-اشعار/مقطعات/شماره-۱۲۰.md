---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>به مشغولی روزگار اندرون</p></div>
<div class="m2"><p>همی چشم دارم فراغ دلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به شب نور خورشید جویم همی</p></div>
<div class="m2"><p>شنیدی چو من هیچ بی حاصلی</p></div></div>