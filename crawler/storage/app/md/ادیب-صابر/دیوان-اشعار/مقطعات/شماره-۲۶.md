---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هیچ نعمت چو زندگانی نیست</p></div>
<div class="m2"><p>به خوشی نزد هر که جا نور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم آن کسی که زندگانی من</p></div>
<div class="m2"><p>بی تو از روز مرگ تلخ تر است</p></div></div>