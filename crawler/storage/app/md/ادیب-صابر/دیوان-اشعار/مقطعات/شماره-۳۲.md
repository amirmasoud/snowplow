---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>اگر چه هست جان اندر تن ما</p></div>
<div class="m2"><p>نمی دانند دانایان که جان چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو کس بر آسمان از ما نبوده ست</p></div>
<div class="m2"><p>چه داند کز بر هفت آسمان چیست</p></div></div>