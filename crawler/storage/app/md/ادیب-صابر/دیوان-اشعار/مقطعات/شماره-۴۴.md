---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>عالم که خوردنش همه غم باشد از جهان</p></div>
<div class="m2"><p>بهتر ز جاهلی که نعیم جهان خورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه غذای جغد شود سینه تذرو</p></div>
<div class="m2"><p>به زو همای اگرچه که او استخوان خورد</p></div></div>