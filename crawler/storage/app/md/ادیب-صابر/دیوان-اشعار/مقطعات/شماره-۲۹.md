---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>خبرت خفته می دهد دربان</p></div>
<div class="m2"><p>گر نخفتی خلاف گفتن چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زاصحاب فیلی اندر فعل</p></div>
<div class="m2"><p>خواب اصحاب کهف گفتن چیست</p></div></div>