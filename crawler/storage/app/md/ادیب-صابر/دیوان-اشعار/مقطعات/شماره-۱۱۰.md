---
title: >-
    شمارهٔ ۱۱۰
---
# شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>پیری آمد جوانی از من شد</p></div>
<div class="m2"><p>سال بیشی گرفت و مال کمی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بترین وقت مرمراست که نیست</p></div>
<div class="m2"><p>وقت پیری بتر ز بی درمی</p></div></div>