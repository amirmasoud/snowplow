---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>ای دل مشو از حال که از حال نگردد</p></div>
<div class="m2"><p>در گردش احوال زمانه دل مردان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حالی که همی از فلک گردان زاید</p></div>
<div class="m2"><p>ساکن نبود بل چو فلک باشد گردان</p></div></div>