---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>ز دشمنان کهن دوستان نوسازی</p></div>
<div class="m2"><p>به دست دیو بود عقل را گرو کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز مرده زنده شدن ممکن است و ممکن نیست</p></div>
<div class="m2"><p>ز دشمنان کهن دوستان نو کردن</p></div></div>