---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>خواجه را با همه زفتی هوس مدح خود است</p></div>
<div class="m2"><p>بر لب خوک چه جای هوس بوسه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این حماقت چه عجب باشد از آن ریش بزرگ</p></div>
<div class="m2"><p>هر که را ریش بزرگ است خرد کوسه بود</p></div></div>