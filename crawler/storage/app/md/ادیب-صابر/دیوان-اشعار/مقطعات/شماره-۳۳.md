---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>به وفات تو مال تو ببرند</p></div>
<div class="m2"><p>وارثان تو از ذکور و اناث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو به منت بده که بی منت</p></div>
<div class="m2"><p>برد خواهند وارثان میراث</p></div></div>