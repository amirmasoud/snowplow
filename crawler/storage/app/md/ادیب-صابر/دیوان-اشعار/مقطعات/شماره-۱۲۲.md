---
title: >-
    شمارهٔ ۱۲۲
---
# شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>ای شهاب دین به خدمت چند کرت آمدم</p></div>
<div class="m2"><p>هر که را گفتم که ممکن هست دیدن گفت نی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواستم تا از جمال بی همال و روی تو</p></div>
<div class="m2"><p>با سعادت جفت گردم بازگشتم جفت نی</p></div></div>