---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>بنده در مستی اگر گفت فضول</p></div>
<div class="m2"><p>جرم او را به تفضل بگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه را نیست به هشیاری عقل</p></div>
<div class="m2"><p>زو به مستی طمع عقل مدار</p></div></div>