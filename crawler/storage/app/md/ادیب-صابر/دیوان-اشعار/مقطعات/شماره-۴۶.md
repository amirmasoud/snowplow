---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>چون همه روی زمانه سوی جفا بود</p></div>
<div class="m2"><p>محتشمان عادت زمانه گرفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خرد از کارها میانه گزیند</p></div>
<div class="m2"><p>چون ز همه فضلها کرانه گرفتند</p></div></div>