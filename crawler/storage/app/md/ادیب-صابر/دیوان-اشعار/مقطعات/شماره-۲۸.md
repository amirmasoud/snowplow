---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>تو را که فضل و هنر هست و بخت و دولت نیست</p></div>
<div class="m2"><p>درست شد که گنه مر زمانه نه تو راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه حسن ادب داری و جمال هنر</p></div>
<div class="m2"><p>چه فایده که دو چشم زمانه نابیناست</p></div></div>