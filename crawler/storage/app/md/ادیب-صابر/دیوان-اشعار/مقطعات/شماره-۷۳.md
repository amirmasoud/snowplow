---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>ایا به محمدت و بر و مکرمت معروف</p></div>
<div class="m2"><p>خط علوم و ادب را شمایل تو حروف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشید ملک، ادیب عمید، زین الدین</p></div>
<div class="m2"><p>چو دین به هر صفتی کز ثنا رود موصوف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>محل کلک تو را رتبت زمین و زمان</p></div>
<div class="m2"><p>بنان و نطق تو را قوت رماح و سیوف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدین محل که تویی کم ز رتبت تو بود</p></div>
<div class="m2"><p>اگر دوات تو را زلف حور باشد صوف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنیده ای که چه اعجوبه ساختند از من</p></div>
<div class="m2"><p>ستاره گاه گاه مسیر و زمانه وقت صروف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در صنوف معانی مرا نبود نظیر</p></div>
<div class="m2"><p>چه در رسید مرا از بلا، صنوف صنوف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به من رسد هم جور از زمانه پنداری</p></div>
<div class="m2"><p>که قصد او همه از بهر من بود موقوف</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همیشه رنج و عنا در صفات حال من است</p></div>
<div class="m2"><p>چنانکه در صفت ایزدی رحیم و رئوف</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اگر اسیر حوادث شدم شگفت مدار</p></div>
<div class="m2"><p>به مهر و ماه رسد نکبت خسوف و کسوف</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خوف بی درمی چون رهم دراین ایام</p></div>
<div class="m2"><p>که حال فضل تباه است و راه جود مخوف</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بخوان دعای مرا پس بخر ثنای مرا</p></div>
<div class="m2"><p>که نام محتشمان را ثنا کند معروف</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پناه من ز صروف زمانه مجلس توست</p></div>
<div class="m2"><p>همیشه باد مکاره ز مجلست مصروف</p></div></div>