---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>روشن شود دو دیده چو بینم خطاب تو</p></div>
<div class="m2"><p>من در خطاب و خط تو زان دارم اعتقاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا از سواد خط توام نور یافت چشم</p></div>
<div class="m2"><p>باور شد آن حدیث که النور فی السواد</p></div></div>