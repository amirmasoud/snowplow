---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>مدار بسته در خویش و تنگ بار مباش</p></div>
<div class="m2"><p>که این دو عیب بزرگ از بزرگواری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آن گشاده کفی شرط نیست در بستن</p></div>
<div class="m2"><p>بر آن فراخ دلی جای تنگ باری نیست</p></div></div>