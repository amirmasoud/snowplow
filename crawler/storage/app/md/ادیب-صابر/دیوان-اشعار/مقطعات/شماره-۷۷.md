---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>گرم حاجت آمد به تعریف تو</p></div>
<div class="m2"><p>تو را هست فخر و مرا نیست ننگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبینی که مر زر پاکیزه را</p></div>
<div class="m2"><p>همی حاجت آید به تعریف سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبینی که مر زر پاکیزه را</p></div>
<div class="m2"><p>همی حاجت آید به تعریف سنگ</p></div></div>