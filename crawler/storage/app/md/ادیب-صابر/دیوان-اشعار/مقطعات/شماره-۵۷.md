---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>بیار ای ساقی خورشید چهره</p></div>
<div class="m2"><p>میی کو صفوت از خورشید دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه خورشیدی کزو چون خورد مفلس</p></div>
<div class="m2"><p>تو گویی نعمت جمشید دارد</p></div></div>