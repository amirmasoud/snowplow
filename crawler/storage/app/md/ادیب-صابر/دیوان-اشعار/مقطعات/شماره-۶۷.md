---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>اگر به زمیری است پیری، نخواهم</p></div>
<div class="m2"><p>که هرگز بت من مرا میر خواند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم تازه گشتی چو خواندی جوانم</p></div>
<div class="m2"><p>کنون چون شود چون مرا پیر خواند</p></div></div>