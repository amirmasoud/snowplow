---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>تا زبرج حوت آهنگ حمل کرد آفتاب</p></div>
<div class="m2"><p>در حمل در هر نباتی صد عمل کرد آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دو شاخی بر کمر بستند چون جوزاکمر</p></div>
<div class="m2"><p>تا سریر شاهی از برج حمل کرد آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میان زاغ و بلبل مشکلی افتاده بود</p></div>
<div class="m2"><p>در حمل هر مشکلی کافتاد حل کرد آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روضه فردوس گشت از ماه تا ماهی جهان</p></div>
<div class="m2"><p>چون زماهی بر فلک منزل بدل کرد آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از رخ نسرین و روی لاله و دیدار گل</p></div>
<div class="m2"><p>سبزه را پر ماه و مریخ و زحل کرد آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روضه فردوس گشت از ماه تا ماهی جهان</p></div>
<div class="m2"><p>باغ را در زینت طینت مثل کرد آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وین همه طینت که اندر زینت بستان نهاد</p></div>
<div class="m2"><p>از برای نزهت صدر اجل کرد آفتاب</p></div></div>
<div class="b2" id="bn8"><p>ساحت صحرا ز زینت همچو نقش مانوی است</p>
<p>هر کجا چشمت برافتد صورت و نقش نوی است</p></div>
<div class="b" id="bn9"><div class="m1"><p>ابر فروردین ز فردوس برین آید همی</p></div>
<div class="m2"><p>زانکه با ماء معین و حور عین آید همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر زمین را پیش از این از آسمان رشک آمدی</p></div>
<div class="m2"><p>آسمان را زین سپس رشک از زمین آید همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از سماع قمریان قاری خجل گردد همی</p></div>
<div class="m2"><p>وز گلوی بلبلان صوت حزین آید همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رعد از آن چون مالک اشتر بغرد کز رخش</p></div>
<div class="m2"><p>شعله تیغ امیرالمومنین آید همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از نسیم گل به تن مشک ختن خیزد همی</p></div>
<div class="m2"><p>وز ضمیر گل به دل در ثمین آید همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باده خوردن باد بر روی ریاحین، دین ما</p></div>
<div class="m2"><p>کز ریاحین بوی بزم مجددین آید همی</p></div></div>
<div class="b2" id="bn15"><p>آنکه هنگام خطاب وکنیت و نام و نسب</p>
<p>عمده الاسلام ابوالقاسم علی الموسوی است</p></div>
<div class="b" id="bn16"><div class="m1"><p>آن خداوندی که عالی شد بدو نام شرف</p></div>
<div class="m2"><p>از طرایف مدح او توزد همی نام طرف</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا نیابی بر او ضایع بود رنج طمع</p></div>
<div class="m2"><p>تانگویی نام او مشکل بود نام شرف</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خدمت درگاه او توقیع انعام نعیم</p></div>
<div class="m2"><p>فکرت بدخواه او تاریخ ایام اسف</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گرچه بی اسلاف او اسلام را رونق نبود</p></div>
<div class="m2"><p>تازه در ایام او گشته است اسلام سلف</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>قطره باران زلفظ او لطافت یافته است</p></div>
<div class="m2"><p>زان همی لولو شود کافتاد در کام صدف</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عقل مست علم گشت از بس که در بزم هنر</p></div>
<div class="m2"><p>ساقی لفظش بدو می داد در جام نتف</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شکر چون مرغان به دام ذکر او بسته بماند</p></div>
<div class="m2"><p>تا بدید انعام او را دانه دام لطف</p></div></div>
<div class="b2" id="bn23"><p>اوست آن عالی نسب کز عدل او و علم او</p>
<p>شغل دولت مستقیم و کار ملت مستوی است</p></div>
<div class="b" id="bn24"><div class="m1"><p>کهترش را در زمانه مهتری کردن سزد</p></div>
<div class="m2"><p>آسمان را پیش قدرش چاکری کردن سزد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>همتش را سر ز چرخ هفتمین برتر شده است</p></div>
<div class="m2"><p>بر سران روزگار او را سری کردن سزد</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>افتخار آل حیدر نیست در عالم جز او</p></div>
<div class="m2"><p>کلک او را کار تیغ حیدری کردن سزد</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عدل او با چرخ بی انصاف جوید داوری</p></div>
<div class="m2"><p>هر کجا انصاف باشد، داوری کردن سزد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سیرت خوبش دل سلطان و لشکر صید کرد</p></div>
<div class="m2"><p>هر کجا خوبی بباشد دلبری کردن سزد</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>لشکرش شد پر طمع تا لشکر جودش بدید</p></div>
<div class="m2"><p>در چنان لشکر طمع را لشکری کردن سزد</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>برتر از اقبال او اختر ندانم بر فلک</p></div>
<div class="m2"><p>بر فلک اقبال او را برتری کردن سزد</p></div></div>
<div class="b2" id="bn31"><p>شاه سادات است و گیسو بر سر او تاج او</p>
<p>تاج پر گوهر چه باشد تاج تاج گیسوی است</p></div>
<div class="b" id="bn32"><div class="m1"><p>نیست از قدر و خطر در هفت کشور هم کفوش</p></div>
<div class="m2"><p>زین همی نازد ولیش و زان همی سوزد عدوش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر عدو خواهد که در راه خلافش دم زند</p></div>
<div class="m2"><p>نم نماند در دهانش دم بگیرد در گلوش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اوج علیین نخوانم همت عالیش را</p></div>
<div class="m2"><p>اوج علیین یکی جزو است از اجزای علوش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر غلو با کارها در شرع جدش راست نیست</p></div>
<div class="m2"><p>وقت بذل مال و نعمت چون بود چندان غلوش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>همچو نور از ماه و ماه از اختران تابنده شد</p></div>
<div class="m2"><p>سروری از راه و رسمش مهتری از خلق و خوش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گرچه باقی نیست قدر و رتبتش را در جهان</p></div>
<div class="m2"><p>از جهان جز ذکر باقی نیست چیزی آرزوش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آسمان باصد هزاران چشم بینا بر زمین</p></div>
<div class="m2"><p>گرچه بسیاری عجب بیند، نبیند هم کفوش</p></div></div>
<div class="b2" id="bn39"><p>ای خداوندی که در دست تو آن کلک ضعیف</p>
<p>حجت دولت مبین و قوت ملت قوی است</p></div>
<div class="b" id="bn40"><div class="m1"><p>نیست کس در نیک نامی هم نفس مانند تو</p></div>
<div class="m2"><p>در معالی و معانی نیست کس مانند تو</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>هیچ نشگفت ار نماند هیچکس فریاد خواه</p></div>
<div class="m2"><p>تا بود در عهد ما فریاد رس مانندتو</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>از بزرگان گرچه خالی نیست دور روزگار</p></div>
<div class="m2"><p>هم تویی در روزگار خویش و بس مانند تو</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>سیم و زر با خاک و خس نزدیک جود تو یکی است</p></div>
<div class="m2"><p>کس نبخشد در جهان این خاک و خس مانند تو</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>از بزرگی کسب کردن بی هوس هرگز نماند</p></div>
<div class="m2"><p>کیست در عالم که باشد زین هوس مانند تو</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>در شب ظلم از دل عادل عسس داری همی</p></div>
<div class="m2"><p>روز من شب باد اگر باشد عسس مانند تو</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>یک نفس داریم و از عدل تو در وی صد دعا</p></div>
<div class="m2"><p>ای ندیده نفس ناطق هم نفس مانند تو</p></div></div>
<div class="b2" id="bn47"><p>در مدیح تو طریق جادوی خواهم سپرد</p>
<p>فعل نیک و صنعت نغز از حساب جادوی است</p></div>
<div class="b" id="bn48"><div class="m1"><p>گرچه صدر عالمی در علم صد عالم تویی</p></div>
<div class="m2"><p>در بزرگی افتخار نسبت آدم تویی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر در این عالم به از عالم یکی عالم بود</p></div>
<div class="m2"><p>اندر این عالم به از عالم یکی عالم تویی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>خواستم تا علم و عالم را دعا گویم یکی</p></div>
<div class="m2"><p>آن دعا هم در تو گفتم زانکه هر دو هم تویی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خاتم پیغمبران اندر جهان جد تو بود</p></div>
<div class="m2"><p>از بزرگی چون نگین جم در آن خاتم تویی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خواهم از ایزد بقای نوح و عمر جم تو را</p></div>
<div class="m2"><p>زانکه در خورد بقای نوح و جام جم تویی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>باد عزت بی زوال و باد خرم خاطرت</p></div>
<div class="m2"><p>کاهل عز بی زوال و خاطر خرم تویی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>روی شادی بین به چشم دل که از ابنای دهر</p></div>
<div class="m2"><p>آنکه او هرگز نخواهد دید روی غم تویی</p></div></div>
<div class="b2" id="bn55"><p>خسروانی جام خواه و خسروی ران کام دل</p>
<p>جام جام خسروانی کام کام خسروی است</p></div>