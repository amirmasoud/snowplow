---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>آزر و مانی که صورتهای دلبر کرده اند</p></div>
<div class="m2"><p>نی رخ چون ماه و نی زلف چو عنبر کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عنبرین گیسوی و مه دیدار آن دلبر مرا</p></div>
<div class="m2"><p>بی نیاز از صورت مانی و آزر کرده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نرگسش چشم است و سروش قد و خوبان نام او</p></div>
<div class="m2"><p>ماه نرگس چشم و سرو ماه منظر کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصف آن رخشنده عارض نعت آن تابنده روی</p></div>
<div class="m2"><p>فهم و فکرت را به رتبت روم و ششتر کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اختیار دل ربودن بر لب شیرین اوست</p></div>
<div class="m2"><p>گویی آن لب را به دل بردن مخیر کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو زنجیر و زره کار مرا در هم زده</p></div>
<div class="m2"><p>حلقه و زنجیر آن زلف زره ور کرده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم سرین فربه او هم میان لاغرش</p></div>
<div class="m2"><p>عشق و صبرم را به تن فربی و لاغر کرده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر دل و جان و تن من جور و بیداد و ستم</p></div>
<div class="m2"><p>پیچ و تاب و چین آن زلف ستمگر کرده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رسم غارت نیست اندر لشکر سلطان، چرا</p></div>
<div class="m2"><p>زلف و لفظش غارت خرخیز و عسگر کرده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاه شاهان سنجر آن کز بیم دست و خنجرش</p></div>
<div class="m2"><p>خطبه هر منبری بر نام سنجر کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از حروف دست و خنجر بیش باشد در جمل</p></div>
<div class="m2"><p>فتحهایی کان مبارک دست و خنجر کرده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>پادشاه هفت کشور گشت و هفت اختر مدار</p></div>
<div class="m2"><p>بر مراد پادشاه هفت کشور کرده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در ازل لوج و قلم وقت قرار کارها</p></div>
<div class="m2"><p>تا ابد ملک جهان بر وی مقرر کرده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هیبت او را فنای عمر خاقان داده اند</p></div>
<div class="m2"><p>دولت او را زوال ملک قیصر کرده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از برای نسخت فتحش کرام الکاتبین</p></div>
<div class="m2"><p>از شب و روز زمانه نقش دفتر کرده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چون دعای رستگاری چون ثنای کردگار</p></div>
<div class="m2"><p>نامه های فتح او را هر دو از بر کرده اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لطف او و حلم او و عفو او و خشم او</p></div>
<div class="m2"><p>از مزاج باد و خاک و آب و آذر کرده اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>دست و تیغش در هلاک بت پرست و قمع کفر</p></div>
<div class="m2"><p>اقتداگویی به دست و تیغ حیدر کرده اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیغ حیدر فتح خیبر کرد و دست و تیغ او</p></div>
<div class="m2"><p>صد هزاران فتح بیش از فتح خیبر کرده اند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>جرعه ای از جام او و قطره ای از بحر اوست</p></div>
<div class="m2"><p>آنچ افریدون و دارا و سکندر کرده اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>تاج داران را مسخر کامرانان را ذلیل</p></div>
<div class="m2"><p>او به ذات خود کند ایشان به لشکر کرده اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پیش از این شاهان ز بهر تخت و افسر در مصاف</p></div>
<div class="m2"><p>سرکشان را از سر شمشیر بی سر کرده اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دولت و اقبال سلطان بازو و شمشیر او</p></div>
<div class="m2"><p>صد ملک را در جهان با تاج و افسر کرده اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شرع پیغمبر به ملک او همی نازد بدانک</p></div>
<div class="m2"><p>ملک او را قوت شرع پیمبر کرده اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>اینک اهل شرع تا باقی بماند ملک او</p></div>
<div class="m2"><p>وهم ها در بسته اند و دست ها بر کرده اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اوست آن سلطان که خیر و شر و نحس و سعد را</p></div>
<div class="m2"><p>اختران در خشم و خشنودیش مضمر کرده اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر همه شاهان مظفر شد که تقدیر و قضا</p></div>
<div class="m2"><p>نام او را در ازل شاه مظفر کرده اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چتر و تاجش چون ببیند دیده را صورت شود</p></div>
<div class="m2"><p>کاسمآن دیگر و خورشید دیگر کرده اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خانه خورشید برج شیر باشد بر فلک</p></div>
<div class="m2"><p>وین سخن را همگنان نادیده باور کرده اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از سر منجوق شه تابد همی خورشید فتح</p></div>
<div class="m2"><p>زین قبل میدان او را شیر پیکر کرده اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>صورت ملک است و ملت زانکه نقاشان صنع</p></div>
<div class="m2"><p>ملک و ملت را به ترکیبش مصور کرده اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>از میان دین و دنیا داوری برخاسته است</p></div>
<div class="m2"><p>تا مراو را در میان هر دو داور کرده اند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در پناه دولت او در امان عدل او</p></div>
<div class="m2"><p>آهوان در بیشه با شیران چراخور کرده اند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عدل و انصافش که گردانند گرد شرق و غرب</p></div>
<div class="m2"><p>حنظل و زهر جهان را نوش و شکر کرده اند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>دولتش چون حکم ایزد نصرتش چون دورچرخ</p></div>
<div class="m2"><p>اهل مشرق را و مغرب را مسخر کرده اند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ملک او را حجت دعوی به معنی داده اند</p></div>
<div class="m2"><p>نام او را حاجت دینار و منبر کرده اند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>خسروان کش نایبانند از پی تعظیم او</p></div>
<div class="m2"><p>نام او را نایب الله اکبر کرده اند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گر سخای خسروان را پیش از این اهل سخن</p></div>
<div class="m2"><p>در صفت با ابر و با دریا برابر کرده اند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>در سخن نام سخا دست و دل شاه جهان</p></div>
<div class="m2"><p>در جهان بر ابر و بر دریا مزور کرده اند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>از پی تقدیر عمر و از پی تقریر کار</p></div>
<div class="m2"><p>چون دبیران قضا اول قلم تر کرده اند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ملک او را ابتدا بنیاد عالم گفته اند</p></div>
<div class="m2"><p>عمر او را انتها تا روز محشر کرده اند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>خنجر پر گوهر و پیکان زرینش به رزم</p></div>
<div class="m2"><p>صد هزاران چشم را پر زر و گوهر کرده اند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کوشش و رزمش ز جان گر خصم را مفلس کند</p></div>
<div class="m2"><p>مفلسان را بخشش و بزمش توانگر کرده اند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>گر فلک فریاد خصمش نشنود معذور هست</p></div>
<div class="m2"><p>کاسه و کوس شهنشه گوش او کر کرده اند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>گر هلاک عادیان از باد صرصر گشته بود</p></div>
<div class="m2"><p>لشکر او بر معادی فعل صرصر کرده اند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گر عدو از بیمشان در آتش سوزان شده ست</p></div>
<div class="m2"><p>خویشتن در آتش سوزان سمندر کرده اند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>چون کند آهنگ اعدا خلق پندارد مگر</p></div>
<div class="m2"><p>باز و شاهین قصد دراج و کبوتر کرده اند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>از نمایش گرچه روز رزم بحر اخضرند</p></div>
<div class="m2"><p>ای بسا کز خون خصمان بحر احمر کرده‌اند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>بر زمین آنجا که رزم آرد ز عکس موج خون</p></div>
<div class="m2"><p>از ثریا تا ثری گویی معصفر کرده اند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>روز بزمش گویی از بس چهره و قد بتان</p></div>
<div class="m2"><p>از زمین تا آسمان کشمیر و کشمر کرده اند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>شاه خورشیدست و بزمش چرخ و اندر بزم او</p></div>
<div class="m2"><p>گویی از مریخ می وز زهره ساغر کرده اند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>خسروا شاهنشها صورتگران صنع حق</p></div>
<div class="m2"><p>ملک و ملت را به ترکیبت مصور کرده اند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر سپاه تو به خواری قصد زی قیصر نکرد</p></div>
<div class="m2"><p>هیبت و هول سپاهت قصد قیصر کرده اند</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>لشکر از فتح و ظفر داری و شاهان را ذلیل</p></div>
<div class="m2"><p>روز کوشش لشکر تو زین دو لشکر کرده اند</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>باده و بزم تو را وقت صفات و گاه نعت</p></div>
<div class="m2"><p>عاقلان با خلد و با کوثر برابر کرده اند</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>تا تو را در ملک باقی عمر جاویدان بود</p></div>
<div class="m2"><p>ساقیان در جام زرین آب کوثر کرده اند</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا فلک را زیور اصلی ز اختر داده اند</p></div>
<div class="m2"><p>تا عرض را نسبت کلی به جوهر کرده اند</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>جوهر تاجش چو اختر ز آسمان تابنده باد</p></div>
<div class="m2"><p>کاسمان ملک را پر زیب و زیور کرده اند</p></div></div>