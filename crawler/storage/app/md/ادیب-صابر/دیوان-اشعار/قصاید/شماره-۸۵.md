---
title: >-
    شمارهٔ ۸۵
---
# شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>صحن چمن که خرم و زیبا شود همی</p></div>
<div class="m2"><p>چون درج در و رزمه دیبا شود همی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیباتر است عشرت و خرم تر است عیش</p></div>
<div class="m2"><p>تا باغ و سبزه خرم و زیبا شود همی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باغ از در تنعم و نزهت شود همی</p></div>
<div class="m2"><p>راغ از در نشاط و تماشا شود همی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برنا و پیر قصد گل و مل همی کنند</p></div>
<div class="m2"><p>زین دهر پیر گشته که برنا شود همی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بهر زنده کردن گلهای نوبهار</p></div>
<div class="m2"><p>باد صبا دعای مسیحا شود همی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کهربا که باد خزانی به باغ داد</p></div>
<div class="m2"><p>آن کهربا زمرد و مینا شود همی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نرگس نشان تاج سکندر همی دهد</p></div>
<div class="m2"><p>تا بوستان چو مسند دارا شود همی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل یوسف است و گل چو زلیخا جوان شده</p></div>
<div class="m2"><p>یوسف اسیر عشق زلیخا شود همی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رعنا بود هر آنکه دل عاشقان برد</p></div>
<div class="m2"><p>گل دل زما ببرد که رعنا شود همی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا شد شکفته همچو ثریا سر سمن</p></div>
<div class="m2"><p>بوی خوش از ثری به ثریا شود همی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>زلف بنفشه گرچه دو تا شد چو پشت من</p></div>
<div class="m2"><p>او را دلم یگانه و یکتا شود همی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راز دلم ز سبزه به صحرا براوفتد</p></div>
<div class="m2"><p>از دلبری که سبزه و صحرا شود همی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طرف چمن طرایف باغ بهشت یافت</p></div>
<div class="m2"><p>تاگل به حسن صورت حورا شود همی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باغی که زاغ ناخوش از او آشیانه ساخت</p></div>
<div class="m2"><p>ماوای عندلیب خوش اوا شود همی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ابر از هوا چو دیده وامق شد از سرشک</p></div>
<div class="m2"><p>تا لاله همچو عارض عذرا شود همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دارد فروغ شعله آتش میان دود</p></div>
<div class="m2"><p>برق از میان ابر که پیدا شود همی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ماند به سایلان خداوند مجددین</p></div>
<div class="m2"><p>آن ساعتی که ابر به دریا شود همی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سبط رسول سید مشرق که ذات او</p></div>
<div class="m2"><p>فهرست فخر آدم و حوا شود همی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدر زمانه تاج معالی علی که لفظ</p></div>
<div class="m2"><p>اندر ثناش لولو لالا شود همی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بی طبع و خاطر از طرب مدح او سخن</p></div>
<div class="m2"><p>موزون و معنوی و مقفا شود همی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مخدوم آل حیدر و زهرا که خدمتش</p></div>
<div class="m2"><p>تاریخ آل حیدر و زهرا شود همی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>جدش سوار دلدل شهباست، وز هنر</p></div>
<div class="m2"><p>مثل سوار دلدل شهبا شود همی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای کعبه شرف که طواف زمان هرا</p></div>
<div class="m2"><p>گرد در تو مکه و بطحا شود همی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>مبخل ز وصف جود تو معطی شود همی</p></div>
<div class="m2"><p>نادان ز نعت علم تو دانا شود همی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ناز مخالفان ز تو گر رنج شد رواست</p></div>
<div class="m2"><p>خار موافقان ز تو خرما شود همی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دریای بی کرانی و دریای بی کران</p></div>
<div class="m2"><p>روز عطا زجود تو رسوا شود همی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عنقاست ناپدید و ز عدل تو نام ظلم</p></div>
<div class="m2"><p>از عرصه زمانه چو عنقا شود همی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>این عالم کهن شده هرسال در بهار</p></div>
<div class="m2"><p>از بهر نزهت تو مطراشود همی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا تو نشاط باده کنی در هوای خوش</p></div>
<div class="m2"><p>تیره هوا زباده مصفا شود همی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا بر جمال لاله به ساغر خوری شراب</p></div>
<div class="m2"><p>لاله به شکل ساغر صهبا شود همی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امروز کن طرب که مهیاست عیش و عمر</p></div>
<div class="m2"><p>باده ز جام عمر مهیا شود همی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فردای نارسیده چو امروز عمر توست</p></div>
<div class="m2"><p>بس عمرها که در سر فردا شود همی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا تن به عمر مایل و راغب بود همی</p></div>
<div class="m2"><p>تا دل زعشق واله و شیدا شود همی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عمرت همیشه باد که اسباب عمر ما</p></div>
<div class="m2"><p>از عمر و دولت تو مهنا شود همی</p></div></div>