---
title: >-
    شمارهٔ ۴۸
---
# شمارهٔ ۴۸

<div class="b" id="bn1"><div class="m1"><p>روزه رفت و رسید عید فراز</p></div>
<div class="m2"><p>عود پیش آر و کار عید بساز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رمضان را پدید گشت انجام</p></div>
<div class="m2"><p>خیز تا خرمی کنیم آغاز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزه از تاختن فرود آسود</p></div>
<div class="m2"><p>ساقیا با شراب و جام بتاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش محتسب فرو مرده است</p></div>
<div class="m2"><p>ای مغنی بلند کن آواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از جهان چمنگ روزه کوته شد</p></div>
<div class="m2"><p>چنگ برگیر و رود را بنواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>علم عید برفراشته اند</p></div>
<div class="m2"><p>علم شادی وطرب بفراز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بازگشت از نمازگه مردم</p></div>
<div class="m2"><p>خیز تا پیش می بریم نماز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوبت روزه دراز گذشت</p></div>
<div class="m2"><p>پس از این ما و زلفکان دراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر لباس طرب طراز کنیم</p></div>
<div class="m2"><p>از سر زلف نیکوان طراز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گرمه روزه بازداشت ز می</p></div>
<div class="m2"><p>مه شوالمان ندارد باز</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جبر یک ماه تا به یازده ماه</p></div>
<div class="m2"><p>ما و رود و می و نشاط و گراز</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر زما این گنه بود چه کنیم</p></div>
<div class="m2"><p>در توبه نکرده اند فراز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گنهان را امید عفو بود</p></div>
<div class="m2"><p>چون نگویی خدای را انباز</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آدمی زاده بی گنه نبود</p></div>
<div class="m2"><p>ایمنی نیست کبک را از باز</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر مرا بر صراط باید رفت</p></div>
<div class="m2"><p>مدح صدر اجل بس است جواز</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>شرف ساده عمده اسلام</p></div>
<div class="m2"><p>مجد دین داروی امید و نیاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آفتاب علو علی که به قدر</p></div>
<div class="m2"><p>همه با آفتاب گوید راز</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>گوی برده لطافتش ز عراق</p></div>
<div class="m2"><p>دل ربوده فصاحتش ز حجاز</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>نظم او گشته معدن اعجاب</p></div>
<div class="m2"><p>سخن اوست مایه اعجاز</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ذکر او با زمانه در گردش</p></div>
<div class="m2"><p>رای او با ستاره در پرواز</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نشود مردم ذلیل عزیز</p></div>
<div class="m2"><p>تا نیابد ز صدر او اعزاز</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چرخ را اقتدا به همت اوست</p></div>
<div class="m2"><p>رمه را اقتدا بود به نهاز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>هیچ سر خرد نهفته نماند</p></div>
<div class="m2"><p>تا همی کلک او بود غماز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سبز گشت از سخاش کشت امید</p></div>
<div class="m2"><p>سیر گشت از عطاش معده آز</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ای همه خلق را ز گشت فلک</p></div>
<div class="m2"><p>مجلس صدر تو مفر و مفاز</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>به سخا با تو برنیاید ابر</p></div>
<div class="m2"><p>چون مرکب کجا بود مجتاز؟</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زشت را کی بود ملاحت خوب</p></div>
<div class="m2"><p>زاغ را کی بود جلادت باز</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>تا ستوده است در سخا تعجیل</p></div>
<div class="m2"><p>تا گزیده است در سخن ایجاز</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>عمر بین عیش کن سعادت یاب</p></div>
<div class="m2"><p>شاد زی خصم کش عدو پرداز</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تو قرین نشاط و عیش به عید</p></div>
<div class="m2"><p>حاسد تو قرین گرم و گداز</p></div></div>