---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>دولت سلطان ما فرمان یزدان آمده ست</p></div>
<div class="m2"><p>هر چه سلطان خواست زین دولت همه آن آمده ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر زمان یزدانش عز نو دهد در مملکت</p></div>
<div class="m2"><p>تا سر تیغش معز دین یزدان آمده ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سلاطین جهان هرگز نیامد در وجود</p></div>
<div class="m2"><p>هیچ سلطانی بدین دولت که سلطان آمده ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاه شاهان پادشا سنجر که سهم خنجرش</p></div>
<div class="m2"><p>تاج و تخت و دین و دنیا را نگهبان آمده ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک شاهی تا مسلم شد به اسم و رسم او</p></div>
<div class="m2"><p>بر همه شاهان رسوم ملک تاوان آمده ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزگارش چون فلک منقاد و فرمانبر شده ست</p></div>
<div class="m2"><p>مغربش چون مشرق اندر عهد و پیمان آمده ست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز آسمان هر چش مرادست آن همی یابد بدانک</p></div>
<div class="m2"><p>آسمانش چون زمین در زیر فرمان آمده ست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تخت او گشته است از قدر آسمان آسمان</p></div>
<div class="m2"><p>همچنان کان تاج او کیوان کیوان آمده ست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب پادشاهان است و در تاج او</p></div>
<div class="m2"><p>آسمان ملک را خورشید تابان آمده ست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتابش خوانده ام زیرا که در میدان رزم</p></div>
<div class="m2"><p>باره دریا گذارش چرخ جولان آمده ست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چونش در جولان ببیند دیده پندارد مگر</p></div>
<div class="m2"><p>کافتاب از آسمان در صحن میدان آمده ست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تیغ گوهر بار او سبزه است و گوهرها در او</p></div>
<div class="m2"><p>راست پنداری مگر بر سبزه مگر برسبزه باران آمده ست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نیست در بالای رمحش هیچ نقصانی چرا</p></div>
<div class="m2"><p>عمر بدخواهان از او در حد نقصان آمده ست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>چون فراق قد جانان جان رباید روز رزم</p></div>
<div class="m2"><p>زانکه در قامت قرین قد جانان آمده ست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>میزبان نصرت و فتح و ظفر پیکان اوست</p></div>
<div class="m2"><p>میزبان سیری گرفت از بس که مهمان آمده ست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خوش نیابی یک دل اندر کشور بدخواه شاه</p></div>
<div class="m2"><p>خوش نباشد دل کرا در دیده پیکان آمده ست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عمر نوحش بود خواهد زانکه از شمشیر او</p></div>
<div class="m2"><p>در عذاب عاصیان ملک طوفان آمده ست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>تا بکوبد گردن طغیان فرعونان ملک</p></div>
<div class="m2"><p>لشکر او چون عصای پورعمران آمده ست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لشکر او چون سمندر گر به آتش در شود</p></div>
<div class="m2"><p>همچنان آید که خضر از آب حیوان آمده ست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گرچه از ملک سلیمان ملک او افزون تر است</p></div>
<div class="m2"><p>هیبت او بر سر دیوان سلیمان آمده ست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>هر کش اندر رزم بیند نیزه خطی به دست</p></div>
<div class="m2"><p>گوید اندر رزمگه موسی و ثعبان آمده ست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هرگز از صرصر نیامد پیش از این بر قوم عاد</p></div>
<div class="m2"><p>کز نهیب تیغ او بر اهل طغیان آمده ست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مایه خذلان ایزد علت عصیان اوست</p></div>
<div class="m2"><p>وای آن سر کاندرو سودای عصیان آمده ست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا نپنداری که بی خذلان بود عصیان او</p></div>
<div class="m2"><p>کانکه عصیان داشت در سر جفت خذلان آمده ست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر عزیز مصر خواهد تا بیابد عز عمر</p></div>
<div class="m2"><p>پیش خدمت پیش از آن آید که خاقان آمده ست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاد باد این پادشا کاندر امان عدل او</p></div>
<div class="m2"><p>پادشاه چین و ماچین در خراسان آمده ست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هیچ خسرو را چنین فتحی نیامد بعد از آنک</p></div>
<div class="m2"><p>رایت کیخسرو از توران به ایران آمده ست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خان اگر در خانه شکر نعمت سلطان نکرد</p></div>
<div class="m2"><p>لاجرم بی خان و مان و تخت و ایوان آمده ست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>قیصر رومی کمر بندد به پیش تخت شاه</p></div>
<div class="m2"><p>ورنه بر قیصر همان آید که بر خان آمده ست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نام او توقیع فتح و فر و پیروزی شده ست</p></div>
<div class="m2"><p>ملک او تاریخ عدل و امن و ایمان آمده ست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جز به فتح کشوری نامه نکردست افتتاح</p></div>
<div class="m2"><p>هیچ روزی چون دبیر شه به دیوان آمده ست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با فتوح بندگانش بی خطر گشت آن خبر</p></div>
<div class="m2"><p>کز فتوج آل ساسان و آل سامان آمده ست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>این چنین تمکین و نصرت این چنین فتح و ظفر</p></div>
<div class="m2"><p>هیچ خسرو راکی اندر حد امکان آمده ست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تا به هر وقتی که یاد عمرو حکمت کرده اند</p></div>
<div class="m2"><p>در زبان خلق نام نوح و لقمان آمده ست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در جهان داریش صد چون نوح و لقمان باد عمر</p></div>
<div class="m2"><p>زان که در عمرش صلاح هر مسلمان آمده ست</p></div></div>