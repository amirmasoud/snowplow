---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بسته است رنگ روی مرا بر میان خویش</p></div>
<div class="m2"><p>کرده سرشک چشم مرا در دهان خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بر میان ستم کند از بستن کمر</p></div>
<div class="m2"><p>بر من همان کند که کند بر میان خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بس که هست یاد لبش بر زبان من</p></div>
<div class="m2"><p>یابم حلاوت لب او در زبان خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارد ز پرنیان تن و کرده تن مرا</p></div>
<div class="m2"><p>چون تار پرنیان زغم پرنیان خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تیر مژه کشیده به ابروی چون کمان</p></div>
<div class="m2"><p>بر من کمین گشاده به تیرو کمان خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ذره رحم در دل نامهربانش نیست</p></div>
<div class="m2"><p>شرمش نیاید از دل نامهربان خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدم زیان خویش چو دادم دلی بدو</p></div>
<div class="m2"><p>تا مر مراگلی دهد از گلستان خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اصل زیان هر کسی از دشمنان بود</p></div>
<div class="m2"><p>اصل زیان من همه از دوستان خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک بوسه باید از دو لب لعل او مرا</p></div>
<div class="m2"><p>تا صد هزار سود کنم برزیان خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تا دست یافت بر دل من دلستان من</p></div>
<div class="m2"><p>تنها نشسته ام ز دل و دلستان خویش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با من چرا به بوسه بخیلی همی کند</p></div>
<div class="m2"><p>چون من بر او بخیل نباشم به جان خویش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جادوست کارغوان مرا کرد زعفران</p></div>
<div class="m2"><p>در آرزوی چهره چون ارغوان خویش</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جادو منم که گر به جمالش نگه کنم</p></div>
<div class="m2"><p>در ساعت ارغوان کنم از زعفران خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دورم ز روز وصلش و هرگز ندیده ام</p></div>
<div class="m2"><p>دوری میان روز فراق و میان خویش</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از آرزوی سی و دولولوش هر شبی</p></div>
<div class="m2"><p>دریا کنم دو دیده لولو فشان خویش</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>لولو ز کس دریغ ندارد دو چشم من</p></div>
<div class="m2"><p>همچون دو دست صدر اجل سوزیان خویش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن مجد دین و عمده اسلام و مسلیمن</p></div>
<div class="m2"><p>کاسلام از او شده ست مکین در مکان خویش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خورشید خاندان نبوت علی که هست</p></div>
<div class="m2"><p>در علم چون علی شرف خاندان خویش</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>صدری که جود و مجد بنازد به ذات او</p></div>
<div class="m2"><p>روز و شبان چنانکه شعیب از شبان خویش</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا قهرمان گنج سخا دست او شده ست</p></div>
<div class="m2"><p>قهرست گنج را همه از قهرمان خویش</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>از بس که بر برات عطاها نشان کند</p></div>
<div class="m2"><p>گرد جهان نشانه شده ست از نشان خویش</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ای در زمانه بی قلم و لوح ساخته</p></div>
<div class="m2"><p>اسرار لوح کلک تو را ترجمان خویش</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مهدی بود که ظلم برد عدل گسترد</p></div>
<div class="m2"><p>مهدی تویی بدین صفت اندر زمان خویش</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گر داستان دست تو در جود بشنود</p></div>
<div class="m2"><p>طی کرده گیر حاتم طی داستان خویش</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر هست نزد تو سخن راست را قبول</p></div>
<div class="m2"><p>اینک همی شنو سخن مدح خوان خویش</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چون مشتری ضمان جهانی به فال سعد</p></div>
<div class="m2"><p>زان داردت خدای همی درضمان خویش</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بر لفظ و مدحت تو همی آفرین کنند</p></div>
<div class="m2"><p>لولو ز بحر خویش جواهر ز کان خویش</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>دریا کرانه دارد و دریای فضل تو</p></div>
<div class="m2"><p>ننموده هیچ وقت کسی را کران خویش</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>با جود آفتابی و آنگه چو آفتاب</p></div>
<div class="m2"><p>آورده مرکبی چو فلک زیر ران خویش</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر باره گران چو رکابت گران شود</p></div>
<div class="m2"><p>ماهی از او به ماه رساند فغان خویش</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>بار رعیت از تو سبک شد چراکنی</p></div>
<div class="m2"><p>بار زمین گران ز رکیب گران خویش</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با آنکه چرخ بوسه دهد بر رکاب تو</p></div>
<div class="m2"><p>هرگز ز راه عدل نتابی عنان خویش</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هرگز ندیده اند قرین تو بی قرین</p></div>
<div class="m2"><p>در قرنها کواک چرخ از قران خویش</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>بر زر و سیم نام عزیزی نهاده اند</p></div>
<div class="m2"><p>چون خوار کرده ای ز عطا هر دوان خویش</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از سیم و زر همیشه چو نرگس دهد نشان</p></div>
<div class="m2"><p>آن را که همت تو نشاند به خوان خویش</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر روز اگر جلال و جمالت فزون تر است</p></div>
<div class="m2"><p>من دیده ام دقیقه این در گمان خویش</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>دارنده جهان به جمال و جلال تو</p></div>
<div class="m2"><p>زینت همی تمام کند در جهان خویش</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>آن کس که در ستایش ممدوح خویش گفت</p></div>
<div class="m2"><p>ای کرده چرخ تیغ تو را پاسبان خویش</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز آسیب چرخ اگر برهیدی روان او</p></div>
<div class="m2"><p>کردی به نام تو همه شعر روان خویش</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ور فرخی به عهد تو بودی ز لفظ عذب</p></div>
<div class="m2"><p>بر نظم مدحت تو فشاندی روان خویش</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>از سیستان به بست نکردی بسیج راه</p></div>
<div class="m2"><p>سوی تو آمدی همه از سیستان خویش</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>گر نیستم به طبع دقیقی و فرخی</p></div>
<div class="m2"><p>هستم کنون مقدمه کاروان خویش</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بر صدر تو به لفظ دقیقی کنم نثار</p></div>
<div class="m2"><p>از قدر تو فروتر و بیش از توان خویش</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>پنهان نهند گنج و من اینک نهاده ام</p></div>
<div class="m2"><p>گنجی به نام تو زثنا در نهان خویش</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر گه که آرزوی ثنای تو گیردم</p></div>
<div class="m2"><p>پنهانش را پدید کنم در بنان خویش</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>بینم ثنای شکر تو واجب که دیده ام</p></div>
<div class="m2"><p>مغز عطا و بر تو در استخوان خویش</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>خشنودم از زمانه که مدحتگر توام</p></div>
<div class="m2"><p>چونانکه مجلس تو ز بخت جوان خویش</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرچه در این دیار غریبم ز جود تو</p></div>
<div class="m2"><p>با خان و مان خویشم و با آب و نان خویش</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>زان جمله نیستم که از این پیش گفته اند</p></div>
<div class="m2"><p>ای من غریب و ممتحن از خان و مان خویش</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>تا در زمانه جشن بهار و خزان بود</p></div>
<div class="m2"><p>خرم گذار جشن بهار و خزان خویش</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>بادا امان جاه تو ایمن ز روزگار</p></div>
<div class="m2"><p>و ایزد نگاه دار تو اندر امان خویش</p></div></div>