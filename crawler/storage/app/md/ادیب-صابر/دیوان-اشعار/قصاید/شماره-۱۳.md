---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>ای خجل با روی و زلفت روز و شب</p></div>
<div class="m2"><p>مانده ام با روی و زلفت در عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویت از روز است یا روز از رخت</p></div>
<div class="m2"><p>شب ز زلف توست یا زلفت ز شب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرده ای از روی روزی مختصر</p></div>
<div class="m2"><p>یا سر زلفت شبی شد منتخب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز را ازلاله پوشیدی لباس</p></div>
<div class="m2"><p>تا شبت را عنبرین کردی سلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای سرینت آفت تل سمن</p></div>
<div class="m2"><p>ای میانت حسرت تار قصب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مانده ام با دیده یاقوت بار</p></div>
<div class="m2"><p>تا تو را دیدستم از یاقوت لب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای دل افتاده در سودای عشق</p></div>
<div class="m2"><p>خسته خاری و دور از تو رطب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر طرب را طالبی مطلوب خویش</p></div>
<div class="m2"><p>نزد زین الدین ابوطالب طلب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آن جمال ساده و نور شرف</p></div>
<div class="m2"><p>آسمان فضل و خورشید نسب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جاه را قدر رفیع او اساس</p></div>
<div class="m2"><p>جود را طبع کریم او سبب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تازه با کردار او روی هنر</p></div>
<div class="m2"><p>روشن از دیدار او چشم ادب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نام نیک اوست تشریف خطاب</p></div>
<div class="m2"><p>عرض پاک اوست تاریخ لقب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>حضرتش هم مرتجی هم ملتجا</p></div>
<div class="m2"><p>حرمتش هم منتسب هم مکتسب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مدحت او چون شراب آرد نشاط</p></div>
<div class="m2"><p>خدمت او چون سماع آرد طرب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>با شراب ذل حسود او حریف</p></div>
<div class="m2"><p>وز مراد دل عدوی او عزب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هست جودش اضطرار موج بحر</p></div>
<div class="m2"><p>هست جدش اختیار صنع رب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در خصال و خلق او لفظ عجم</p></div>
<div class="m2"><p>در بنان و کلک او جود عرب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن را از بذل او خواری و ذل</p></div>
<div class="m2"><p>جود را با مال او شور و شغب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>فعل بذل و بر او در حرص و آز</p></div>
<div class="m2"><p>همچنان چون فعل آتش در حطب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در حساب مکرمت تاثیر او</p></div>
<div class="m2"><p>همچو تاثیر فضایل در حسب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ای دعای نیکخواهت مستجاب</p></div>
<div class="m2"><p>ای هلاک بدسگالت مستحب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>موکب ماه مبارک در رسید</p></div>
<div class="m2"><p>بار بر بستند شعبان و رجب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آتش روزه زبانه برکشید</p></div>
<div class="m2"><p>تا هزیمت گشت از او آب عنب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>باده خواران را عدیل آمد عنا</p></div>
<div class="m2"><p>رود سازان را نصیب آمد نصب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آن کنیم اکنون که یزدان را رضاست</p></div>
<div class="m2"><p>تا بیفزاییم شیطان را غضب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا بود در بوستان سرو و سمن</p></div>
<div class="m2"><p>تا بود بر آسمان راس و ذنب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نیکخواهت باد با سور و سرور</p></div>
<div class="m2"><p>بدسگالت باد در تیمار و تب</p></div></div>