---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>رخت به باغ ارم ماند ای بدیع صنم</p></div>
<div class="m2"><p>ز خط بنفشه دمیده به گرد باغ ارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخی که هست به گردش کمند لاله و گل</p></div>
<div class="m2"><p>به هیچ حال ز باغ ارم نباشد کم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به باغ اگر سمن و نرگس و بنفشه بود</p></div>
<div class="m2"><p>زروی و چشم و خطت با همند هر سه به هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخت ز دیده من دیر دیر دور مدار</p></div>
<div class="m2"><p>که باغ تازه نماند چو دیر یابد نم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم که خسته عشق است مرهمش رخ توست</p></div>
<div class="m2"><p>که دید خسته که او را بود ز مه مرهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زلف دیبه رخساره را رقم زده ای</p></div>
<div class="m2"><p>که زد ز غالیه بر طرف آفتاب رقم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلم شکار تو گشت ای نگار آهو چشم</p></div>
<div class="m2"><p>تو از شکار من ایمن چو آهوان حرم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به زلف روی بپوشی چو پیش من گذری</p></div>
<div class="m2"><p>مگر جمال تو را نیست چشم من محرم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز تاب آتش اگر نرم گردد آهن سخت</p></div>
<div class="m2"><p>دل تو زین نفس گرم نرم گردد هم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز بس که زلف تو بر هم زند گره بر هم</p></div>
<div class="m2"><p>چو زلف توست همه کار من خم اندر خم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگرچه زاده حوری نه زاده حوا</p></div>
<div class="m2"><p>وصال توست چو افسون زاده مریم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مرا به عشق علم کرده ای و من مانده</p></div>
<div class="m2"><p>ز بیم هجر تو لرزان چو روز باد علم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به چهره باغ خلیلی به غمزه چوب کلیم</p></div>
<div class="m2"><p>به لب دعای مسیحی به زلف خاتم جم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از آن چهار جفاو ستم ندید کسی</p></div>
<div class="m2"><p>از این چهار تو تا کی مرا جفا و ستم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اگر چه رنجه ام از عشق تو به تنگی دل</p></div>
<div class="m2"><p>ز تنگی دهنت هم به رنجه باشد دم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فراخی از پس تنگی بود وز این معنی است</p></div>
<div class="m2"><p>که چشم تنگ تو بر من فراخ دارد غم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگرچه بر دل تنگم الم رسید ز عشق</p></div>
<div class="m2"><p>به مدح سید شرقم امان رسد ز الم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>امیر ساده رضی الملوک مجدالدین</p></div>
<div class="m2"><p>که آفتاب جلال است و آسمان همم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>امیر سید عالم علی بن جعفر</p></div>
<div class="m2"><p>که مجتبای خلیفه است و مقتدای امم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ز اوج همت او طیره گنبد اعلی</p></div>
<div class="m2"><p>ز نور نسبت او تیره نیر اعظم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>لقای او غرض نعمت زمان و زمین</p></div>
<div class="m2"><p>بقای او سبب حرمت عبید و خدم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از اوست فایده جود و مجد مستوفا</p></div>
<div class="m2"><p>بدوست قاعده علم و فضل مستحکم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>رهی است خدمت او کش منافع است دلیل</p></div>
<div class="m2"><p>شهی است منت او کش مکارم است (حشم)</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>رسید نور جلالش به دیده اعمی</p></div>
<div class="m2"><p>همی رسد خبر حشمتش به گوش اصم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز بهر مجلس انسش که باده نوشیده است</p></div>
<div class="m2"><p>ستاره مشعله دار است و آسمان طارم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همیشه هست به جودش تکاثر ارزاق</p></div>
<div class="m2"><p>چنانکه هست به جدش تفاخر آدم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگرچه نسبت پاکش زخاتم الرسل است</p></div>
<div class="m2"><p>در اوست قدر رسولی که معجزش خاتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>شکوه او که به عرق از پیامبر عربی است</p></div>
<div class="m2"><p>پیمبری است پدید آمده میان عجم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>کند سیاست خشمش صحیح را معلول</p></div>
<div class="m2"><p>کند سلاست لفظش فصیح را ابکم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شود ز همت او گر شود ستاره خجل</p></div>
<div class="m2"><p>خورد به نعمت او گر خورد زمانه قسم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سلام اوست دلیل ره سلامت و امن</p></div>
<div class="m2"><p>کلام اوست کلید در علوم و حکم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمانه ای است که فضلش تنی نماند به رنج</p></div>
<div class="m2"><p>ستاره ای که ز عدلش دلی نماند دژم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ز قدر او امرای همه عجم عاجز</p></div>
<div class="m2"><p>ز مدح او فصحای همه عرب مفحم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>ثنا و خدمت او حاجب امید و امل</p></div>
<div class="m2"><p>حدیث حرمت (او) چون ره حدوث و قدم؟</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>شده است نامه فضل و شرف بدو مکتوب</p></div>
<div class="m2"><p>شده است جامه علم و هنر بدو معلم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>ز بهر خسرو عالم که جاودانه زیاد</p></div>
<div class="m2"><p>همی تهی کند از فتنه عرصه عالم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>جماعتی که از ایشان به رنج بودی خلق</p></div>
<div class="m2"><p>ز بهر قصد ستم کرده خویشتن رستم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چو گرگ و ساخته از کاروان مانده گله</p></div>
<div class="m2"><p>چو شیر و داشته از سنگهای خاره اجم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>طریقشان همه چون کیش کافران مظلم</p></div>
<div class="m2"><p>حصارشان همه چون دین مومنان محکم</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه خرقه ای ز صلاحی فرو گرفته به پشت</p></div>
<div class="m2"><p>نه لقمه ای زحلالی فرو شده به شکم</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>نه هیچ بوده بر الفاظشان کلام نجات</p></div>
<div class="m2"><p>نه هیچ بوده در اسلامشان ثبات قدم</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>یکی مکابره گیرد به روز خانه خال</p></div>
<div class="m2"><p>یکی معاینه دزدد به شب عمامه عم</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ز رنجشان برهانید خلق عالم را</p></div>
<div class="m2"><p>به رنجهای فراوان و گنجهای خدم</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>زهی ز مدح تو عاجز شده بیان سخن</p></div>
<div class="m2"><p>زهی ز شکر تو قاصر شده زبان قلم</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>میان بخل و سخا جود کامل تو حجاب</p></div>
<div class="m2"><p>میان عیب و هنر علم شامل تو حکم</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تنی نماند ز انعام تو اسیر اسف</p></div>
<div class="m2"><p>دلی نگشت در ایام تو ندیم ندم</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>سوال سایل علم و سوال سایل مال</p></div>
<div class="m2"><p>ز فضل و بذل تو یابد همی جواب نعم</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>به نام تو نتوان بود و بود نتوانند</p></div>
<div class="m2"><p>نظیر تو به رسوم و عدیل تو به شیم</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>نه هست هیچ بنا را متانت کعبه</p></div>
<div class="m2"><p>نه هست هیچ چهی را مثابت زمزم</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>به مرتبت چو سر شاخ کی بود تن شاخ</p></div>
<div class="m2"><p>به منزلت چو لب یار کی بود لب یم</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>فضایل و کرمت نیست در جهان مشکل</p></div>
<div class="m2"><p>مناقب و هنرت نیست بر خرد مبهم</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>نه مشکل است سوی خلق هیبت شمشیر</p></div>
<div class="m2"><p>نه مبهم است بر خلق قوت ضیغم</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تو مشکی و جگر سوخته است حاسد تو</p></div>
<div class="m2"><p>به مشک ماند لیکن در او نباشد شم</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>اگرچه هر دو به عالم درند ظلمت و نور</p></div>
<div class="m2"><p>نه اندکی است تفاوت میان نور و ظلم</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>رصد که راست نهادی میان اهل نجوم</p></div>
<div class="m2"><p>وجود یافت حسابی که داشت بیم عدم</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>همه صواب کنی آنچه می کنی و بود</p></div>
<div class="m2"><p>خطا جراحت جان و صواب مرهم هم</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>صوابکار بود هر که دوست دارد مدح</p></div>
<div class="m2"><p>صوابکار همی باش و رستی از همه غم</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چو عزمهای صوابت فتوح عمر تواند</p></div>
<div class="m2"><p>منم به جمع فتوحت محمد اعشم</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>به نظم مدح تو مشغول گشته ام همه سال</p></div>
<div class="m2"><p>که نظم مدح تو شغلی است پیش من معظم</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>چو بی مدیح تو ماند سقیم گردد مدح</p></div>
<div class="m2"><p>جلال مدح تو او را شفا دهد ز سقم</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>رسید عید عرب وز تو دید در یک شخص</p></div>
<div class="m2"><p>لطافت عجم و همت عرب شده ضم</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>فرو کشید کنون بر سرو غنم رقمی</p></div>
<div class="m2"><p>که جرم خاک شود زان رقم به رنگ بقم</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>غنیمتی است غنم را که کشته تو شود</p></div>
<div class="m2"><p>به دست خویش غنیمت رسان به جان غنم</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>تو کشته زنده کنی زنده را چگونه کشی</p></div>
<div class="m2"><p>کدام نوش کند در جهان صناعت سم</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>همیشه تا سبب خرمی بود باده</p></div>
<div class="m2"><p>به باده باد دل و طبع و خاطرت خرم</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>حریف دست کریمت همه جمال قدح</p></div>
<div class="m2"><p>ندیم طبع لطیفت همه وصال صنم</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>مباد بزم تو خالی زناله و زاری</p></div>
<div class="m2"><p>یکی ز زاری زیر و یکی ز ناله بم</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>روانت خرم و چشمت ز شمس دین روشن</p></div>
<div class="m2"><p>ز حلق و چشم بد اندیش تو روان شده دم</p></div></div>