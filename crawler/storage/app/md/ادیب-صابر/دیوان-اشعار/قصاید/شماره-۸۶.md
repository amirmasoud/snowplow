---
title: >-
    شمارهٔ ۸۶
---
# شمارهٔ ۸۶

<div class="b" id="bn1"><div class="m1"><p>ای زلف یار من زرهی یا زره گری</p></div>
<div class="m2"><p>یا پیش تیره غمزه دلبر زره وری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرگز زره ز ره نبرد هیچ خلق را</p></div>
<div class="m2"><p>گر تو زره گری به زره چون زره بری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشنیده ام که هیچ زره زهره پرورد</p></div>
<div class="m2"><p>بر روی آن صنم زره زهره پروری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هاروت خوانمت من و داوود گویمت</p></div>
<div class="m2"><p>تا دیدمت که زهره پرست و زره گری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داوود نیستی به زره رغبت تو چیست</p></div>
<div class="m2"><p>هاروت نیستی غم زهره چرا خوری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را به راه حسن دلیل محبتی</p></div>
<div class="m2"><p>جان را ز باغ عشق نسیم معنبری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر گل نهاده توده شمشاد و سنبلی</p></div>
<div class="m2"><p>بر مه فتاده سایه چوگان و چنبری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خرمی چو سای طوبی و سدره ای</p></div>
<div class="m2"><p>و اندر جوار چشمه حیوان و کوثری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشق چرا کشی که نه سلمی و ویسه ای</p></div>
<div class="m2"><p>بی ره چرا کنی که نه مانی و آزری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گاهی چو شب حجاب کنی پیش آفتاب</p></div>
<div class="m2"><p>گاهی چو ابر پرده شوی پیش مشتری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گاه از رخانش صاحب یاقوت و دیبهی</p></div>
<div class="m2"><p>گاه از لبانش صاحب مرجان و شکری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون کاروان کفری و منزلگهت بهشت</p></div>
<div class="m2"><p>چون زنگیان مستی و فرش تو عبقری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون جور تیره رنگی و دلهای بری به جبر</p></div>
<div class="m2"><p>گویی که در ربودن دلها مخیری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در ظلمتی و چشمه حیوان کنی طلب</p></div>
<div class="m2"><p>زلفی تو یا شبی خضری یا سکندری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رنگ شب و شباب و شبه چون ربوده ای</p></div>
<div class="m2"><p>یا از شباب و از شبه و شب مصوری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در چفتگی معالجه قد چفته ای</p></div>
<div class="m2"><p>در تیرگی مشاطه روز منوری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مسکین دلم کبوتر مضراب عشق توست</p></div>
<div class="m2"><p>تا تو به شکل و صورت طوق کبوتری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بالین و بستر تو زنسرین و سوسن است</p></div>
<div class="m2"><p>وز چین و تاب زینت بالین و بستری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در تاب توست زینت روی مه منیر</p></div>
<div class="m2"><p>در چین توست زیور و برگ گل طری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باغی مگر که معدن نسرین و سوسنی</p></div>
<div class="m2"><p>چرخی مگر که جایگه ماه و اختری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>منزلگه تو با کف موسی برابر است</p></div>
<div class="m2"><p>گر تو به گونه با دل فرعون برابری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عنبر همی گزینی و چنبر همی کنی</p></div>
<div class="m2"><p>برگل همی نشینی و از دل همی چری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای دلبری که این صفت تاب زلف توست</p></div>
<div class="m2"><p>در نیکویی نگاری و در دلبری پری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سیمین نشد صنوبر و زرین کمر نبست</p></div>
<div class="m2"><p>(زرین کمر تو بندی و سیمین صنوبری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>با روی تو به لاله و ماهم نیاز نیست)</p></div>
<div class="m2"><p>زانم چنین که ماه رخ و لاله منظری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>دل ریش ماندهرکه نیابد وصال تو</p></div>
<div class="m2"><p>درویش ماند هر که نیابد توانگرری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گویم به زلف تو چو وصال تو یافتم</p></div>
<div class="m2"><p>ای شب چه ساحری که به جز روز نسپری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>من در غم تو شیوه گرفتم ستم کشی</p></div>
<div class="m2"><p>تا تو به طبع پیشه گرفتی ستمگری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>خواهی که بشمری غم و اندیشه مرا</p></div>
<div class="m2"><p>خواهم که حلقه و گره خویش بشمری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر قول فیلسوف نه ای چون مسلسلی</p></div>
<div class="m2"><p>ور خلق صدر مشرق نه ای چون معطری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر صدر روزگار علی بن جعفر است</p></div>
<div class="m2"><p>در بوی خوش چو بوی علی بن جعفری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فخر شرف قوام امامت رئیس شرق</p></div>
<div class="m2"><p>در شرق و غرب گشته مسلم به مهتری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از نسبت پیمبر و اندر صفای عرق</p></div>
<div class="m2"><p>همچو پیمبر از صفت ناسزا بری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>او را پیمبری و جز او را مشعبدی است</p></div>
<div class="m2"><p>هرگز مشعبدی نبود چون پیمبری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>فرزند مصطفی و نهاده نجوم چرخ</p></div>
<div class="m2"><p>برطالع سعادت او مهر مادری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>قدرش برادر فلک و یافته به قدر</p></div>
<div class="m2"><p>از خسرو زمانه خطاب برادری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>ای حیدر نسب که به ذاتت نسب کند</p></div>
<div class="m2"><p>اخلاق مصطفایی و افعال حیدری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>درصدر نیکنامی و در صف پردلی</p></div>
<div class="m2"><p>چون مصطفی کریم و چو حیدر دلاوری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از روضه رسالت آن دسته گلی</p></div>
<div class="m2"><p>وز دوحه خلافت آن شاخ بروری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در مسند سیادت و در محفل هنر</p></div>
<div class="m2"><p>گویی درست حیدر کرار دیگری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خیبر علی گرفت و گرفتند دشمنانت</p></div>
<div class="m2"><p>خواری ز عز تو چو جهودان خیبری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اعدای دولت تو اگر عمرو و عنترند</p></div>
<div class="m2"><p>حیدر دلی و قاهر هر عمرو و عنتری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>کلکت چو ذوالفقار خداوند قنبر است</p></div>
<div class="m2"><p>زیرا جمال آل خداوند قنبری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>مرغی مخبرست و ز منقار او رسید</p></div>
<div class="m2"><p>ما را خبر ز سیرت طایی و جعفری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>روشن کند(سخا) و سرش مار پیکری است</p></div>
<div class="m2"><p>فربه دهد عطا و تنش جفت لاغری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ای صدر روزگار که بر روی روزگار</p></div>
<div class="m2"><p>فری و زینتی و جمالی و زیوری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>پاکی و بردباری و لطف و صفای تو</p></div>
<div class="m2"><p>بادی و خاکی آمد و آبی و آذری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گر جود را رهی است به دل پیک آن رهی</p></div>
<div class="m2"><p>ور بخل را دری است به کف قفل آن دری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>در عفو و خشم تو ره آسایش است و رنج</p></div>
<div class="m2"><p>در مهر و کین تو در ایمان و کافری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>گر عقل قبله ای است تو بر وی مقدمی</p></div>
<div class="m2"><p>ور فضل کعبه ای است تو در وی مجاوری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>در جاه و مرتبت زبر هفت کوکبی</p></div>
<div class="m2"><p>در جود و مکرمت سمر هفت کشوری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>اسلام را به مرتبت فتح مکه ای</p></div>
<div class="m2"><p>انصاف را به منزلت روز محشری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر شرق و غرب ملک شهنشاه سنجر است</p></div>
<div class="m2"><p>زین ملک اختیار شهنشاه سنجری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>ور مملکت به خنجر بران کند نسب</p></div>
<div class="m2"><p>در ضبط ملک ضربت برنده خنجری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>هر چند نیست لشکر سلطان عدد پذیر</p></div>
<div class="m2"><p>تو میزبان و معطی سلطان و لشکری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>شاهان دلیل نصرت شاه مظفرند</p></div>
<div class="m2"><p>تا تو دلیل نصرت شاه مظفری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>سیاره در اشارت سلطان اعظم است</p></div>
<div class="m2"><p>تا تو مشیر مجلس سلطان صفدری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>شاهان همی زبارگه قصر او برند</p></div>
<div class="m2"><p>منشور شهریاری و خانی و قیصری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>گر شرع در رعایت آن تاج و افسر است</p></div>
<div class="m2"><p>تو راعی و مصالح آن تخت و افسری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>و اینک تو را کرامت و تشریف او گذشت</p></div>
<div class="m2"><p>از تخت اردشیری و از تاج نوذری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>وان طوق و مرکب و کمر و خلعت و لوا</p></div>
<div class="m2"><p>منشور مهتری شد و توقیع سروری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>وین زر و در و جوهر و زینت به عمر خویش</p></div>
<div class="m2"><p>هرگز ندیده اند نه قارون نه سامری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>تشریف تو مصدر تشریف ها بود</p></div>
<div class="m2"><p>زیرا که بر صدور زمانه مصدری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>ایشان کواکبند و تو خورشید روشنی</p></div>
<div class="m2"><p>ایشان معادنند و تو یاقوت احمری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>چون بحر یافتند نجویند جوی را</p></div>
<div class="m2"><p>جویند خلق عالم و تو بحر اخضری</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>آن خسروی که سایه او سعد اکبر است</p></div>
<div class="m2"><p>در سایه سعادت او سعد اکبری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>امروز دشمن تو به هفتم زمین فروست</p></div>
<div class="m2"><p>وز رغم دشمنانت به هفتم فلک بری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>هر چند در جهان قلم رتبت تو راست</p></div>
<div class="m2"><p>بر مهتران مقدم و بر سروران سری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>از عدل سرنتابی و جز صدق نشنوی</p></div>
<div class="m2"><p>از حلم برنگردی و جز علم ننگری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>از سال نو بهاری و از روزگار عید</p></div>
<div class="m2"><p>از طبع اعتدالی و از بحر گوهری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>بر مشرق معالی و بر عالم علوم</p></div>
<div class="m2"><p>مهر منوری و سپهر مدوری</p></div></div>
<div class="b" id="bn72"><div class="m1"><p>نه طالب عطا چو مطلوب یافته است</p></div>
<div class="m2"><p>نه بایع ثنا چو تو دیده است مشتری</p></div></div>
<div class="b" id="bn73"><div class="m1"><p>از عرض تو چو عالم علوی به مرتبت</p></div>
<div class="m2"><p>این عالم است تا تو بدین عالم اندری</p></div></div>
<div class="b" id="bn74"><div class="m1"><p>وانکه بقای او متعلق به عرض توست</p></div>
<div class="m2"><p>از بهر آنکه او عرض است و تو جوهری</p></div></div>
<div class="b" id="bn75"><div class="m1"><p>هستی چو ابر و بحر عطا بخش و تازه روی</p></div>
<div class="m2"><p>زین ملک گستریدی و زان نام گستری</p></div></div>
<div class="b" id="bn76"><div class="m1"><p>گر بحتری ز نعمت معتز عزیز گشت</p></div>
<div class="m2"><p>آن داده ای به بنده که معتز به بحتری</p></div></div>
<div class="b" id="bn77"><div class="m1"><p>ور عنصری ز مدحت محمود نام یافت</p></div>
<div class="m2"><p>آن یافتم ز تو که ز محمود عنصری</p></div></div>
<div class="b" id="bn78"><div class="m1"><p>از شهرهات شعر فرستند شاعران</p></div>
<div class="m2"><p>زیرا مدایح شعرا را تو در خوری</p></div></div>
<div class="b" id="bn79"><div class="m1"><p>و اینک ادیب از سر اخلاص و اعتقاد</p></div>
<div class="m2"><p>با آنکه نیست صنعت او شعر و شاعری</p></div></div>
<div class="b" id="bn80"><div class="m1"><p>این در زکعبه سفته فرستاد در ثنات</p></div>
<div class="m2"><p>دری نه هر دری و ثنایی نه هر سری</p></div></div>
<div class="b" id="bn81"><div class="m1"><p>از روز و رزق عالیمان گر گریز نیست</p></div>
<div class="m2"><p>تو روز نور بخشی و رزق مقدری</p></div></div>
<div class="b" id="bn82"><div class="m1"><p>تا زلف عنبری بود و چشم نرگسی</p></div>
<div class="m2"><p>با چشم نرگسی زی و با زلف عنبری</p></div></div>
<div class="b" id="bn83"><div class="m1"><p>گردونت پیشکار و میان بسته پیش تو</p></div>
<div class="m2"><p>دولت به بندگی و زمانه به چاکری</p></div></div>