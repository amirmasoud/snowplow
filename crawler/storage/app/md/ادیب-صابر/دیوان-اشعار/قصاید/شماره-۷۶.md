---
title: >-
    شمارهٔ ۷۶
---
# شمارهٔ ۷۶

<div class="b" id="bn1"><div class="m1"><p>ای تو را مملکت حسن شده زیر نگین</p></div>
<div class="m2"><p>نیست چون صورت شیرین تو صورت در چین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست در زیر نگین مملکت عشق مرا</p></div>
<div class="m2"><p>تا تو را مملکت حسن بود زیر نگین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرقه فتنه شدستم ز لب و چهره تو</p></div>
<div class="m2"><p>که دل و دیده من فتنه برانند و بر این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصف رخسار و لب تو به شکر کردم و ماه</p></div>
<div class="m2"><p>ماه روشن شد از این شادی و شکر شیرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نه باغی و نه گردون زچه معنی است بگوی</p></div>
<div class="m2"><p>با تو از هر دو نشان و اثر ای ماه زمین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قامتت سرو و رخت لاله و چشمت نرگس</p></div>
<div class="m2"><p>عارضت زهره و چهره مه و دندان پروین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب نوشین تو کوثر شد و کوی تو بهشت</p></div>
<div class="m2"><p>سایه زلف تو طوبی شد و تو حور العین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر جمال تو همی فتنه شود حسن و جمال</p></div>
<div class="m2"><p>همچو دین بر خرد و رای اجل زین الدین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نور چشم شرف و فخر معالی که شده است</p></div>
<div class="m2"><p>شخصش از نور مرکب دلش از علم عجین</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>طالب محمدت و منت ابوطالب کوست</p></div>
<div class="m2"><p>به سخا بحر محیط و به سخن در ثمین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی نظیری که نیابیش به همت مانند</p></div>
<div class="m2"><p>بی قرینی که نبینیش به انعام قرین</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حزم صافیش چو دیدار نجوم است به سیر</p></div>
<div class="m2"><p>عزم میمونش چو ترکیب سپهر است متین</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای گرفته ز یسارت همه احرار یسار</p></div>
<div class="m2"><p>ای یمین تو به رزق همه آفاق ضمین</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آمد از جود یمین تو یسارت به فغان</p></div>
<div class="m2"><p>که همی گرد بر آرد ز یسار تو یمین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زایر و زر ز سخای تو خطیراست و حقیر</p></div>
<div class="m2"><p>سایل و مال ز جود تو عزیز است و مهین</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>تن مداح تو را هست ز دولت بستر</p></div>
<div class="m2"><p>سر بدخواه تو را هست ز محنت بالین</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>هر عروسی که بزاید ز ضمیر شعرا</p></div>
<div class="m2"><p>همه جز در مدیح تو نخواهد کابین</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آفرین از پی نام تو نهاده ست خدای</p></div>
<div class="m2"><p>همچنان کز جهت نام حسودت نفرین</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>لفظ را وصف بدیع تو کند سحر حلال</p></div>
<div class="m2"><p>سنگ را لفظ ثنای تو دهد ماء معین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تو گزین همه ساداتی و نزد تو رسید</p></div>
<div class="m2"><p>اینک آن ماه که از سال جز او نیست گزین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مصلحان را ز رسیدنش سرور است سرور</p></div>
<div class="m2"><p>مفسدان جمله از اینکار حزینند حزین</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اندر این مه همه جز سورت خیرات مخوان</p></div>
<div class="m2"><p>وندر این مه همه جز صورت طاعات مبین</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر خرامی همه در موکب تهلیل خرام</p></div>
<div class="m2"><p>ور نشینی همه در محفل تسبیح نشین</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>زاهدان بر زدن فسق کشیدند کمان</p></div>
<div class="m2"><p>عابدان بر سپه دیو گشادند کمین</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ضعفا را به چنین وقت معین باش زجود</p></div>
<div class="m2"><p>تا بود جاه تو را ایزد دارنده معین</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا همی زینت گیتی ز مکین است و مکان</p></div>
<div class="m2"><p>تا همی زیور عالم ز شهور است و سنین</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>بنده و خاشع عمر تو سنین باد و شهور</p></div>
<div class="m2"><p>چاکر و خاضع امر تو مکان باد و مکین</p></div></div>