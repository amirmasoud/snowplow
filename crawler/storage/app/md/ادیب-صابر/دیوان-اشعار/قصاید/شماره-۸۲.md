---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>ای زرخسار تو در دی تازه گلزار آمده</p></div>
<div class="m2"><p>با بهار تو بهار نو پدیدار آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بهار چهره تو وزگل رخسار تو</p></div>
<div class="m2"><p>دل چو بلبل وقت گل در ناله زار آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق من در خوردن آن گلزار و رخسار آمده است</p></div>
<div class="m2"><p>این جمال عالم آن گلزار و رخسار آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طره تو غمزه تو چهره تو زلف تو</p></div>
<div class="m2"><p>هر یکی در دلبری از بهر یک کار آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زلف دلداری گزیده چهره جان پرور شده</p></div>
<div class="m2"><p>غمزه غمازی گرفته طره طرار آمده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی بازار جلالی، وز رخ و بالای تو</p></div>
<div class="m2"><p>سرو و مه را کاسدی در روز بازار آمده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی و بالای تو را بر رغم ماه و قهر سرو</p></div>
<div class="m2"><p>دیده دلالی گرفته دل خریدار آمده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ماه را در بی نوایی سرو را در کاسدی</p></div>
<div class="m2"><p>از تو نومیدی رسیده وز من آزار آمده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رخت دل سوی تو دارم کز رخ و بالای توست</p></div>
<div class="m2"><p>بر درخت دلبری هم برگ و هم بار آمده</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>من چرا بی بارم از زلف و رخت کز دود و نور</p></div>
<div class="m2"><p>مشک و مه را ببینم از زلف و رخت بار آمده</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کژدم دلبند جرار است مشکین زلف تو</p></div>
<div class="m2"><p>لشکر عشق تو زان جراره جرار آمده</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صورت زیبا و مشکین زلف پرآشوب تو</p></div>
<div class="m2"><p>آفت بزاز گشته رشک عطار آمده</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>روی تو چون رای زین الدین ابوطالب شده است</p></div>
<div class="m2"><p>هر زبان از بهر فضل او به گفتار آمده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آن جمال ساده و فخر معالی کز علوست</p></div>
<div class="m2"><p>همت عالیش را از آسمان عار آمده</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در شهوار است لفظش وز ثنای لفظ او</p></div>
<div class="m2"><p>لفظ هر شاعر به قدر در شهوار آمده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پشت هر زایر ز تشریف تو دیبا یافته</p></div>
<div class="m2"><p>کیسه هر سایلی را از تو دینار آمده</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>لفظم از گفتار مدحت گوهر افشانی گرفت</p></div>
<div class="m2"><p>ای همه گفتار تو در خورد کردار آمده</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>مشتری فری و بر مال تو از توفیق تو</p></div>
<div class="m2"><p>همت عالیت چون کیوان ستمکار آمده</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>شادمان گردد به فر دولت و تیمار تو</p></div>
<div class="m2"><p>هرکه را شادی ز دل رفته است و تیمار آمده</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نیستی خورشید و رایت چشمه خورشیدوار</p></div>
<div class="m2"><p>والی افلاک گشته عالی آثار آمده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیستی گردون و در انواع رفعت پیش تو</p></div>
<div class="m2"><p>جرم گردون چون زمین بی قدر و مقدار آمده</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وارث زهرا و کراری و در میراث تو</p></div>
<div class="m2"><p>فضل زهرا اوفتاده، علم کرار آمده</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تاج ساداتی به نسبت، فخر احراری به فضل</p></div>
<div class="m2"><p>خاک پایت سرمه سادات و احرار آمده</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ای ز لفظ فضل تو همچون ز لفظ جد تو</p></div>
<div class="m2"><p>شرع جود و مجد را آثار و اخبار آمده</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روزه آمد ناصر و تقوی و خیر و زهد و بر</p></div>
<div class="m2"><p>هست در پیش تو پیش از روزه هر چار آمده</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تا از این شادی چو گل بشکفت روی مصلحان</p></div>
<div class="m2"><p>مفسدان را بینم اندر دیده ها خار آمده</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>عمر تو بسیار خواهم تا همی بینند خلق</p></div>
<div class="m2"><p>روزه را و عید را نزد تو بسیار آمده</p></div></div>