---
title: >-
    شمارهٔ ۸۷
---
# شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>اگر به صورت روی تو آفتابستی</p></div>
<div class="m2"><p>همه بنای شب از روی او خرابستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز کبر و عجب زمانی نتابدی بر خلق</p></div>
<div class="m2"><p>گر از جمال تو جزوی در آفتابستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور آفتاب خبر داردی زصورت تو</p></div>
<div class="m2"><p>ز شرم روی تو پیوسته در نقابستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همیشه عشرت من چون لب تو خوش بودی</p></div>
<div class="m2"><p>اگر سوال مرا از لبت جوابستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهان تنگ تو گویی زلعلی لب تو</p></div>
<div class="m2"><p>ز ناردان صدف لولو خوشابستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خون دیده نگشتی رخم چو پرتذرو</p></div>
<div class="m2"><p>اگر نه زلف تو چون چنگل عقابستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کم از ثواب من استی نعیم هشت بهشت</p></div>
<div class="m2"><p>اگر به دوستی تو مرا ثوابستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بریده کی شدی از چشم من زیارت خواب</p></div>
<div class="m2"><p>اگر نه غمزه آن چشم نیم خوابستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر از بریدن تو جان نبردی ز برم</p></div>
<div class="m2"><p>بریدن از تو خطا نیستی صوابستی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به من نگه نکنی از جفاست یا ز عتاب</p></div>
<div class="m2"><p>عتاب به ز جفا کاشکی عتابستی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بهشت خواندمی این نوبهار خرم را</p></div>
<div class="m2"><p>اگر بنای بهشت از گل و گلابستی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>رسید بلبل و گشت از جهان غریب غراب</p></div>
<div class="m2"><p>(چه باشد اینکه فراق تو چون غرابستی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>اگر نه زلف تو بوسیدیی صبا و سحر</p></div>
<div class="m2"><p>نسیم او نه همانا ز مشک نابستی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>اگر بنفشه به زلف تو اقتداد کندی)</p></div>
<div class="m2"><p>هزار گونه در او پیچ و چین و تابستی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>زمانه را ز زمین تازه گشت عهدبهار</p></div>
<div class="m2"><p>خوشتستی ار همه در عهد این شبابستی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>چمن کتاب طرایف شده ست و پنداری</p></div>
<div class="m2"><p>همه طراوت عالم در آن کتابستی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اگرچه در دل او عشق نیست پنداری</p></div>
<div class="m2"><p>رخش به خون دل عاشقان خضابستی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>(رباب وار بنالند بلبلان گویی</p></div>
<div class="m2"><p>همیشه در گلوی بلبلان ربابستی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>ز ابر بر سر که خیمه هاست وز باران</p></div>
<div class="m2"><p>گمان بری که بران خیمه ها طنابستی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>به سوی عارض گل ماند دیده نرگس</p></div>
<div class="m2"><p>چنانکه گویی این دعد و آن ربابستی)</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سحاب هر نفسی درفشان کند گویی</p></div>
<div class="m2"><p>کف کریم خداوند در سحابستی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رئیس شرق که حلم و لطافت و کرمش</p></div>
<div class="m2"><p>اگر عیان شودی خاک و باد و آبستی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>امیر سید عالم علی که حشمت او</p></div>
<div class="m2"><p>اگر عیان نشدی عدل در حجابستی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گذشت همتش از هفت چرخ پنداری</p></div>
<div class="m2"><p>که مرکبش ز دعاهای مستجابستی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>برادرست خطابش ز پادشاه جهان</p></div>
<div class="m2"><p>خطاستی اگر او را نه این خطابستی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>همه جهان نشدندی اسیر منت او</p></div>
<div class="m2"><p>اگر نه منت او مالک الرقابستی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر نه حرمت آن دست و آن عنانستی</p></div>
<div class="m2"><p>و گرنه هیبت آن پای و آن رکابستی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چو عز او اثر ظلم برفزونستی</p></div>
<div class="m2"><p>چو کیمیا نظر عدل تنگ یابستی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زهی سپهر سخاوت که گر سخاوت تو</p></div>
<div class="m2"><p>نباشدی همه آب طمع سرابستی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر از شراب عطا هیچ خلق مست شدی</p></div>
<div class="m2"><p>طمع ز دست تو سرمست این شرابستی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>ز بس که وقت سخا زر دهی بدان ماند</p></div>
<div class="m2"><p>که زر به چشم تو زر نیستی ترابستی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زمین از نرستی زخشکسال نیاز</p></div>
<div class="m2"><p>اگر نه بذل عطای تو فتح بابستی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ثبات حلم تو گر نیستی در این عالم</p></div>
<div class="m2"><p>ز بیم خشم تو گیتی در اضطرابستی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو را سپهر و جهان خواندمی به رتبت و قدر</p></div>
<div class="m2"><p>اگر نه عادت این هر دو انقلابستی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>گر از عطات ذخیره نهادمی اکنون</p></div>
<div class="m2"><p>مرا خزینه ای و مال مرا نصابستی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بسوختی فلک آبگون گر آتش را</p></div>
<div class="m2"><p>چو هیبت تو و خشم تو نور و تابستی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>برون شدی ز قرار زمین و مرکز خاک</p></div>
<div class="m2"><p>اگر چو مرکب تو باد را شتابستی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>چه مرکبی که چنان عاشق است بر حرکت</p></div>
<div class="m2"><p>کش از قرار و سکون گوییا عذابستی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>زمین چنان سپرد زیر پی که پنداری</p></div>
<div class="m2"><p>زمین صحیفه گردون و او شهابستی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>همیشه تا که به گریه چنان نماید ابر</p></div>
<div class="m2"><p>که گوییا مژه عاشقی مصابستی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>بقای تو چو عطای تو باد ور بودی</p></div>
<div class="m2"><p>بقای تو چو عطای تو بی حسابستی</p></div></div>