---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>زهی زقد و رخت سرو و لاله را خجلی</p></div>
<div class="m2"><p>به سرو عقل ربایی به لاله دل گسلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به سرو برگذری سرو را بود خواری</p></div>
<div class="m2"><p>به لاله در نگری لاله را بود خجلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به باغ اگر نرسد سرو، سرو را عوضی</p></div>
<div class="m2"><p>به راغ اگر ندمد لاله، لاله را بدلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به لب عسل نبود لاله گرچه لعل بود</p></div>
<div class="m2"><p>اگر تو لاله لعلی چرا به لب عسلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیم گل ندهد سرو و رستنش ز گل است</p></div>
<div class="m2"><p>تو رسته از دل و جانی و با نسیم گلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبان لاله تورا گوید ای به قامت سرو</p></div>
<div class="m2"><p>مگر سرشته زآب گلی نه زآب و گلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه لاله و سرو آمدی که لاله و سرو</p></div>
<div class="m2"><p>ز قد و روی تو خواهند هر زمان بحلی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به یاد لاله و سرو توام دهند سرور</p></div>
<div class="m2"><p>نوای باربدی و نبید قطربلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو در غزل صفت سرو و لاله خواهم گفت</p></div>
<div class="m2"><p>غزل به نام تو گویم که اصل آن غزلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سیه نبود دلت تا رخت چو لاله نشد</p></div>
<div class="m2"><p>مگر ز لاله بیاموختی سیاه دلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>دو لاله داری و یک سرو و ساعتی صد بار</p></div>
<div class="m2"><p>بدان دو لاله و یک سرو جان و دل بخلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نهال و تخم تو از باغ شمس دین بوده است</p></div>
<div class="m2"><p>چنین لطیف و چنین دلربا از این قبلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نهال روضه عمران علی بن جعفر</p></div>
<div class="m2"><p>سرشرف شرف الساده جعفر بن علی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جلال موسویان آنکه هست حافظ او</p></div>
<div class="m2"><p>سلامت ابدی و سعادت ازلی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>پناه علم و معالی و در معالی و علم</p></div>
<div class="m2"><p>چو رای خویش وفی و چو طبع خویش ملی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بقای دولت او آیت فنای عدو</p></div>
<div class="m2"><p>لقای فرخ او غایت بقای ولی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهی بزرگ و یگانه که قبله هنری</p></div>
<div class="m2"><p>زهی کریم زمانه که کعبه املی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اجل عالمی و دوست را و دشمن را</p></div>
<div class="m2"><p>گه رضا و غضب هم حیات و هم اجلی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر عمل ز کریمی و عدل و فضل بود</p></div>
<div class="m2"><p>تو صدر و بدر همه عاملان این عملی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نه آسمان و زمینی و گاه حرمت و حلم</p></div>
<div class="m2"><p>چو آسمان رفیع و زمین محتملی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>زراه لطف و معانی چو رمز در سختی</p></div>
<div class="m2"><p>زروی فضل و فواید چو شمس در حملی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>اگر چه مشتری از طلعت تو گردد سعد</p></div>
<div class="m2"><p>چو وقت رفعت و قدر و محل بود زحلی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ستاره را به مثابت سپهر کیوانی</p></div>
<div class="m2"><p>زمانه را به لطافت هوای معتدلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>به روز بذل و عطا مکرمی چو ابر جواد</p></div>
<div class="m2"><p>به وقت علم و بیان روشنی چو نص جلی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز نور علم چو اوصاف علم با شرفی</p></div>
<div class="m2"><p>ز عز عقل چو انواع عقل بی خللی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>چو مصطفا به همه فخر و فضل موصوفی</p></div>
<div class="m2"><p>چو مرتضا به همه علم و جود متصلی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>اگر به حلم زمین به قدر گردونی</p></div>
<div class="m2"><p>وگر به عرض مصونی به مال مبتذلی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نجوم علم و ادب را رفیع تر فلکی</p></div>
<div class="m2"><p>زمین فخر و شرف را شریف تر نزلی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به روزگار فراست ملسم از غلطی</p></div>
<div class="m2"><p>به روز بار سیاست منزه از زللی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>چنانکه نامه زاهد زوحشت سیهی</p></div>
<div class="m2"><p>چنانکه جامه مومن زآفت عسلی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>زمانه با فضلا در جدل بود همه سال</p></div>
<div class="m2"><p>به نصرت فضلا با زمانه در جدلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به نزد همت تو نارواست رد سوال</p></div>
<div class="m2"><p>چنانکه رویت ایزد به نزد معتزلی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>از آنکه حیله یکی از خصال روباه است</p></div>
<div class="m2"><p>گه شکار و سیاست چو شیر بی حیلی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>سخن ز مدح تو رانم که از مدایح من</p></div>
<div class="m2"><p>دهان و گوش سخن پرحلاوت است و حلی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زبان اهل زمان گر خلل گرفت و علل</p></div>
<div class="m2"><p>تو سد آن خللی و طبیب آن عللی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>سزد که خاتم جم کم بود به قدر و محل</p></div>
<div class="m2"><p>زخاتم تو که فرزند خاتم الرسلی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چو هست حافظ عمرت خدای عزوجل</p></div>
<div class="m2"><p>زدور چرخ و صروف زمانه بی وجلی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>زگفته جبلی گر چنین قصیده ستی</p></div>
<div class="m2"><p>زجان ثنا کنمی بر جبلت جبلی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>همیشه تا زچگل ماه سرو قد خیزد</p></div>
<div class="m2"><p>بزی و ساقی بزم تو شاهد چگلی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>قرین و حافظ عمرت سعادت ابدی</p></div>
<div class="m2"><p>معین و ناصر عزت قضای لم یزلی</p></div></div>