---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>صورتگران چه حیله و تدبیر کرده اند</p></div>
<div class="m2"><p>تا شبه روی و موی تو تصویر کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر چو روی و موی تو دلبر نیامده ست</p></div>
<div class="m2"><p>آن حال را چه حیله و تدبیر کرده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بالای و چهره تو به خوبی و دلبری</p></div>
<div class="m2"><p>از شهر بلخ کشمر و کشمیر کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حور و پری که هر دو به خوبی مسلم اند</p></div>
<div class="m2"><p>یک سوره از جمال تو تفسیر کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از زلف دلبر تو کاریگران صنع</p></div>
<div class="m2"><p>بر مه ز مشک حلقه و زنجیر کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مه را که اختران فلک خوب خوانده اند</p></div>
<div class="m2"><p>آن بر جمال روی تو تزویر کرده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا گرد روزت از شبه شب کشیده اند</p></div>
<div class="m2"><p>شبها و روزها شب و شبگیر کرده اند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوبان که خوانده اند تو را میر نیکوان</p></div>
<div class="m2"><p>حقا که در خطاب تو تقصیر کرده اند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گویی چهار طبع جهان صورت تو را</p></div>
<div class="m2"><p>از حسن و سیرت و صفت میر کرده اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تابنده شمس دین که بدو دین و شرع را</p></div>
<div class="m2"><p>ارباب دین کرامت و توفیر کرده اند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صدر اجل محمد طاهر که لفظ حمد</p></div>
<div class="m2"><p>از لطف لفظ اوست که توقیر کرده اند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>آن صدر روزگار که احرار روزگار</p></div>
<div class="m2"><p>بر جان ثناش را همه تحریر کرده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چون همتش به اختر و گردون برآمده است</p></div>
<div class="m2"><p>گردون و اختران همه تکبیر کرده اند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نه رازق است جودش و ارباب رزق را</p></div>
<div class="m2"><p>توفیق جود اوست که تیسیر کرده اند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تقدیر نیک او همه بی بد نوشته اند</p></div>
<div class="m2"><p>آنجا که نیک و بد همه تقدیر کرده اند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای آنکه مادحان عمل ننگ و نام را</p></div>
<div class="m2"><p>بر عامل خصال تو تقریر کرده اند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>کیوان بدان بلند محل شد که اندر او</p></div>
<div class="m2"><p>قدر و محل و رای تو تاثیر کرده اند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اوصاف همت تو سپهر و ستاره را</p></div>
<div class="m2"><p>جفت صفات حسرت و تشویر کرده اند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گویم ز رغبت دل و رایت به ذکر و شکر</p></div>
<div class="m2"><p>شیران نشاط آهو و نخجیر کرده اند؟</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گویی نصیب نفس تو کردند خیر محض</p></div>
<div class="m2"><p>آنجا که نفس خیره و شریر کرده اند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در مدحت تو خیر همه عالم است و خلق</p></div>
<div class="m2"><p>آهنگ مدحت تو نه برخیر کرده اند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>از دولت جوان تو سیارگان سعد</p></div>
<div class="m2"><p>بنیاد قوت فلک پیر کرده اند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بی بحر و بی صدف دل و طبع و ضمیر من</p></div>
<div class="m2"><p>از مدحت تو در بها گیر کرده اند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>خوابی که اهل فضل و ادب نیک دیده اند</p></div>
<div class="m2"><p>آن است کز رسوم تو تعبیر کرده اند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گر در جهان ز صنعت اکسیر زر کنند</p></div>
<div class="m2"><p>مدح و ثنات صنعت اکسیر کرده اند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>پوشیده کن به خلعت خوشیم که مر مرا</p></div>
<div class="m2"><p>چرخ و جهان برهنه تر از سیر کرده اند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>این اختران وگرچه به تقدیم حق ترم</p></div>
<div class="m2"><p>وقت حقوق من همه تاخیر کرده اند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>با من چنان روند که گویی به سوی مورد</p></div>
<div class="m2"><p>آهنگ آب دادن انجیر کرده اند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>روزه رسید و پیش کمان هلال او</p></div>
<div class="m2"><p>جان عدوت را ز اجل تیر کرده اند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>بر تو خجسته باد و گر چند روزهاش</p></div>
<div class="m2"><p>تن را به روزه زارتر از زیر کرده اند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا شاعران صفات رخ و زلف دلبران</p></div>
<div class="m2"><p>اغلب به مشک و قیر و می و شیر کرده اند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>با زلف قیرگون زی و خوش زی که چرخ و دهر</p></div>
<div class="m2"><p>روز مخالفان تو را قیر کرده اند</p></div></div>