---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>ای در حسد چشم تو هاروت به بابل</p></div>
<div class="m2"><p>من در هوس زهره و هاروت تو بیدل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با چهره تو سایه بود تابش زهره</p></div>
<div class="m2"><p>وز غمزه تو مایه برد جادوی بابل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماهی و منت ساخته منزل ز دل و جان</p></div>
<div class="m2"><p>مه را صنما چاره نباشد ز منازل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوسته دل و جان مرا سوخته داری</p></div>
<div class="m2"><p>کم سوز که نیکو نبود سوخته منزل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریادم از آن روز که در جان و دل من</p></div>
<div class="m2"><p>افتاد زآواز رحیل تو زلازل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تو رفته و از رفتن تو مانده نشانی</p></div>
<div class="m2"><p>من مانده و از ماندن من مانده دلایل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خون دلم آمیخته با ریگ بیابان</p></div>
<div class="m2"><p>رنگ رخت آویخته در خاک مراحل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنجا شده از رنگ رخت خاک پر از گل</p></div>
<div class="m2"><p>واینجا شده از خون دلم ریگ پر از گل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقلم شده بی عید زتیمار تو قربان</p></div>
<div class="m2"><p>صبرم شده بی تیغ زهجران تو بسمل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هم عیش من از مهر تو چون فرقت تو تلخ</p></div>
<div class="m2"><p>هم هوش من از هجر تو چون وصل تو زایل</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بی سلسله زلف تو اکنون دل و دانش</p></div>
<div class="m2"><p>بر من نتوان بست به زنجیر و سلاسل</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>حاضر نشود دل چو جمال تو نه حاضر</p></div>
<div class="m2"><p>حاصل نبود جان چو وصال تو نه حاصل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دارم دل و جان مایل دیدار تو لیکن</p></div>
<div class="m2"><p>هرگز نبود رای تو را میل به مایل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از جان گسلم گر دل تو بگسلد از من</p></div>
<div class="m2"><p>جانا نظر دل ز من دلشده مگسل</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آسیمه شد از فرقت تو در تن من جان</p></div>
<div class="m2"><p>چون ظلم ز عدل ملک عالم عادل</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اتسز شه غازی که حسام و قلم او</p></div>
<div class="m2"><p>این رنج عدو آمد و آن راحت سایل</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>شاهی که قوی گشت بدو قاعده حق</p></div>
<div class="m2"><p>حقی که فرو مرد بدو قوت باطل</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای شاه تویی آنکه به توفیق و با تایید</p></div>
<div class="m2"><p>دولت ز تو عالی شد و ملت به تو مقبل</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>دریافت به تایید تو دولت همه مقصود</p></div>
<div class="m2"><p>حل کرد به توفیق تو ملت همه مشکل</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>شد رای تو پیرایه اجرام سماوی</p></div>
<div class="m2"><p>شد لفظ تو سرمایه دیوان رسایل</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>دلهای افاضل به فواضل همه بردی</p></div>
<div class="m2"><p>دلهای افاضل که برد جز به فواضل</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در عهد تو گر زنده شود حاتم و صاحب</p></div>
<div class="m2"><p>این پیش تو جاهل بود آن نزد تو مدخل</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>قاضی است سر تیغ تو در حکم ممالک</p></div>
<div class="m2"><p>مفتی است سر کلک تو در کشف مسایل</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>وقتی که کند همت تو قصد به بالا</p></div>
<div class="m2"><p>روزی که کند هیبت تو تیغ حمایل</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از دست درافتند مقیمان سماوی</p></div>
<div class="m2"><p>وز پای درآیند سواران مقاتل</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن را سزی ای شاه که بینند بزرگان</p></div>
<div class="m2"><p>اطراف جهان را به جمالت متجمل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در دولت سلطان سلاطین شده عالم</p></div>
<div class="m2"><p>از عاطفت عدل تو پر شحنه و عامل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>آسوده نشسته به جلال تو اجلا</p></div>
<div class="m2"><p>و آرام گرفته به منال تو اماثل</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از چین طرف آورده به دیوان تو فغفور</p></div>
<div class="m2"><p>وز روم کمر بسته به فرمان تو هرقل</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آمیخته صحبت تو صاحب بغداد</p></div>
<div class="m2"><p>آموخته خطبه تو خاطب موصل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>دیوار سراپرده و ماه علم تو</p></div>
<div class="m2"><p>با ماه برابر شده با چرخ مقابل</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بر چرخ تو را منزل و می گویدت اقبال</p></div>
<div class="m2"><p>بیرون مشو از منزل یک ساعتک انزل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر زنده شوند از روش و رسم تو گیرند</p></div>
<div class="m2"><p>گردان جهان دیده و شاهان اوایل</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>در عدل طریق و عمل و عادل و سیرت</p></div>
<div class="m2"><p>در ملک رسوم و ره و آیین و شمایل</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از عفو تو آید لطف و رافت و رحمت</p></div>
<div class="m2"><p>وز عدل تو خیزد شرف عاجل و آجل</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>آن باره که بادی است زسندانش قوایم</p></div>
<div class="m2"><p>و آن اسب که ابری است ز خاراش مفاصل</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بینند چو از هر دو ز من سایه پذیرند؟</p></div>
<div class="m2"><p>پرویز در این سایه و شبدیز در آن ظل</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>نه صف هنر دید و نه میدان ملاقات</p></div>
<div class="m2"><p>چون کلک تو و تیغ تو یک قایل و فاعل</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>آن را کشد آن تیغ که فتوی دهدش عقل</p></div>
<div class="m2"><p>جز تیغ تو نشنید کسی آهن عاقل</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>آباد بر آن تیغ که بی دیده و دانش</p></div>
<div class="m2"><p>می بیند و بی راه محق داند و باطل</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر نصرت از او خواسته بودی به همه حال</p></div>
<div class="m2"><p>از منتصر آن فتنه ندیدی متوکل</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>چون رای تو تابنده و چون لفظ تو پر در</p></div>
<div class="m2"><p>چون سهم تو گیرنده و چون خشم تو قاتل</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>چون کلک تو دین پرور و یک لحظه نباشد</p></div>
<div class="m2"><p>از مصلحت ملک تو چون کلک تو غافل</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>کلکی که بداند همه راز دل بدخواه</p></div>
<div class="m2"><p>چون تیغ تو نابوده در او خارج و داخل</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از خاصیت دست تو چون دست تو معطی</p></div>
<div class="m2"><p>وز فایده لفظ تو چون لفظ تو مفضل</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>در شرع چون رسم تو نهد قاعده خوب</p></div>
<div class="m2"><p>در ملک چو تیغ تو نهد نصرت کامل</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گر علم تو او را حکم عدل نسازد</p></div>
<div class="m2"><p>پیدا نشود مرتبت عالم و جاهل</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>شاها به وصول همه اغراض و مقاصد</p></div>
<div class="m2"><p>جز خدمت و جز مدحت تو نیست وسایل</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>موجود شوند ار دل و رای تو بخواهند</p></div>
<div class="m2"><p>معدوم شده دولت و اقبال افاضل</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>پیداست مقامات تو در ملت و در ملک</p></div>
<div class="m2"><p>پنهان نبود در شب تاریک مشاعل</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>خوکرد طمع بر نظر عاطفت تو</p></div>
<div class="m2"><p>خو کرده بود باز به آواز جلاجل</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>شاها به فتوح تو جهان حامله گشته است</p></div>
<div class="m2"><p>جز بار نهادن نبود حاصل حامل</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زان داد مرا عمر جهان خلعت پیری</p></div>
<div class="m2"><p>زیرا به ثناهای تو بودم متوسل</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>هر چند که هستم به سخن طوطی و بلبل</p></div>
<div class="m2"><p>سنجاب جوانیم بدل شد به حواصل</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>با این همه آن صاحب نظمم که نیابند</p></div>
<div class="m2"><p>دریای مرا اهل سخن معبر و ساحل</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>گر مدح تو را بر عرب عاربه خوانم</p></div>
<div class="m2"><p>الفاظ مرا قبله کنند اهل قبایل</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>تا شعر بود در دو زبان اصل بلاغت</p></div>
<div class="m2"><p>تا فضل بود در دو جهان اصل فضایل</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>بادا ز زبان بهره تو مدحت عالی</p></div>
<div class="m2"><p>بادا ز جهان حصه تو نعمت شامل</p></div></div>