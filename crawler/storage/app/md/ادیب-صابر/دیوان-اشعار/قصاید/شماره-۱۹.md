---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>شمشاد قد و لاله رخ و یاسمین بر است</p></div>
<div class="m2"><p>با سرو و گل به قامت و عارض برابر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایم غلام و چاکر یاقوت و شکرم</p></div>
<div class="m2"><p>کو را لب و حدیث ز یاقوت و شکر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ز خط و زلف تو بر جان من بلاست</p></div>
<div class="m2"><p>گفت آن همه بلای تو از مشک و عنبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون دیدمش ز کبر به خورشید ننگرم</p></div>
<div class="m2"><p>کو خود به چهره چشمه خورشید دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در بر است جای دل هر کسی چرا</p></div>
<div class="m2"><p>جای دلم به حلقه زلف وی اندر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لرزان ترم ز ذره و سوزان ترم ز شمع</p></div>
<div class="m2"><p>تا او چراغ مجلس و خورشید لشکر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با من موافق است به یک چیز و بیش نی</p></div>
<div class="m2"><p>من یاسمین سرشکم و او یاسمین بر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر خانه زو بهشت شود بس شگفت نیست</p></div>
<div class="m2"><p>کز قد و لب برابر طوبی و کوثر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>او را سپرده ام دل و او را سزد از آنک</p></div>
<div class="m2"><p>دلبند و دلفریب و دل آشوب و دلبر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ای سرو ماه چهره و ای ماه سرو قد</p></div>
<div class="m2"><p>باغ و سپهر تو ز دل و جان چاکر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو سرو با خرامش و ماه سخنوری</p></div>
<div class="m2"><p>نه سرو با خرامش و نه مه سخنور است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بر لذت و خوشی جهان بس گذشته ام</p></div>
<div class="m2"><p>جانا به جان تو که وصال تو خوشتر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عشقم ز حسن تو چو سرین تو فربه است</p></div>
<div class="m2"><p>صبرم ز عشق تو چو میان تو لاغر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با تو حدیث آزر و مانی چرا کنند</p></div>
<div class="m2"><p>کز صورت تو صنعت هر دو مزور است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوبی رخ تو را و ملاحت لب تو راست</p></div>
<div class="m2"><p>اینجا چه جای صنعت مانی و آزر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>رویت چو رای تاج معالی است پر فروغ</p></div>
<div class="m2"><p>زلفت چو خوی سید مشرق معطر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تاج سر ملاحت و خوبی جمال توست</p></div>
<div class="m2"><p>تاج سر زمانه علی بن جعفر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بنیاد داد و قاعده عدل مجددین</p></div>
<div class="m2"><p>کو دین پناه و دادگر و عدل گستر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>با حلم مصطفاست که فرزند مصطفاست</p></div>
<div class="m2"><p>با علم حیدرست که از عرق حیدر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زایر چوکشت و بخشش او ابر بهمن است</p></div>
<div class="m2"><p>دشمن چو عاد و کوشش او باد صرصر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>قدر رفیع او بر هفت کوکب است</p></div>
<div class="m2"><p>ذکر شریف او سمر هفت کشور است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>در شخص او تانی عقل است و لطف روح</p></div>
<div class="m2"><p>گویی ز عقل و روح مجرد مصور است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>روز عدوش چون شب تاری سیه شده ست</p></div>
<div class="m2"><p>شبهای دوستانش چو روز منور است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بیش از شمار ذره خورشید شد سخاش</p></div>
<div class="m2"><p>وین زو بدیع نیست که خورشید منظر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>منظر بسی بود که به مخبر تبه شود</p></div>
<div class="m2"><p>او را سزای منظر پاکیزه مخبر است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آل پیمبرند سر افتخار دین</p></div>
<div class="m2"><p>او افتخار جمله آل پیمبر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>صدر زمانه را همت زینت به روی اوست</p></div>
<div class="m2"><p>آری سزد که زینت گردون به اختر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اهل زمانه زر و درم را مسخرند</p></div>
<div class="m2"><p>او باز بذل زر و درم را مسخر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>هر جا که نام مجد و معالی کنند یاد</p></div>
<div class="m2"><p>نام بلند او سر دیوان و دفتر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>آزاد و بنده بندگی او گرفته اند</p></div>
<div class="m2"><p>وین زان گرفته اند که او بنده پرور است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از بس که وصف نامه و الفاظ او کنند</p></div>
<div class="m2"><p>طبع ثنا گرش صدف در و گوهر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>در و شکر شود چو به کلکش رسد سخن</p></div>
<div class="m2"><p>کلک است یا بضاعت عمان و عسگر است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ای صدر روزگار و خداوند نامدار</p></div>
<div class="m2"><p>آنی که کردگار تو را پشت و یاور است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دشمن کم است و دوست فزون و جهان به کام</p></div>
<div class="m2"><p>وقت سماع و عشرت و ساقی و ساغر است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بنگر در آن قدح که شراب مروق است</p></div>
<div class="m2"><p>گویی شراب نیست گلاب مقطر است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گر لاله نیست شاید ورگل بشد رواست</p></div>
<div class="m2"><p>وقت بنفشه تر و بوینده عبهر است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>بر روی این دو گل می سوری همی ستان</p></div>
<div class="m2"><p>روی تو گل بس است که همواره احمر است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>تا آب را همیشه بر آتش بود ظفر</p></div>
<div class="m2"><p>اقبال تو همیشه بر اعداد مظفر است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>تا نام جوهر و عرض است اندر این جهان</p></div>
<div class="m2"><p>نام جلال و جاه تو باقی چو جوهر است</p></div></div>