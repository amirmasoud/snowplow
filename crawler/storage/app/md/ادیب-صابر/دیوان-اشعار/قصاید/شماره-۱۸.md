---
title: >-
    شمارهٔ ۱۸
---
# شمارهٔ ۱۸

<div class="b" id="bn1"><div class="m1"><p>خوشا وقتا که وقت نوبهار است</p></div>
<div class="m2"><p>مساعد روز و میمون روزگار است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زمین چون لعبت شمشاد زلف است</p></div>
<div class="m2"><p>جهان چون کودک عنبر عذار است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کجا پایت برآید گلستان است</p></div>
<div class="m2"><p>کجا چشمت برافتد لاله زار است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میان باغ پر مشک و عبیر است</p></div>
<div class="m2"><p>کران راغ چون نقش و نگار است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هوا چون چشم عاشق درفشان است</p></div>
<div class="m2"><p>صبا چون زلف دلبر مشکبار است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بساطی بافت فروردین زمین را</p></div>
<div class="m2"><p>کش از مینا و بسد پود و تار است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قرار اکنون به صحن بوستان دار</p></div>
<div class="m2"><p>که صحن بوستان دار القرار است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کنار باغ پر درست و گوهر</p></div>
<div class="m2"><p>کنار او مگر دریا کنار است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگرید ابر نوروزی همی زار</p></div>
<div class="m2"><p>که شاخ زرد گل بیمار و زار است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زمانی عندلیب از وی جدا نیست</p></div>
<div class="m2"><p>مگر نزدیک او تیماردار است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر بلبل شده ست از عشق گل مست</p></div>
<div class="m2"><p>چرا این چشم نرگس پرخمار است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گیای کیمیا گشته است نرگس</p></div>
<div class="m2"><p>که طبعش مایه زر عیار است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در این فصلی که مرده زنده گردد</p></div>
<div class="m2"><p>چرا شاخ بنفشه سوگوار است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>مگر گل را عروسی کرد نوروز</p></div>
<div class="m2"><p>که ابرش هر زمان گوهر نثار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بهار است این ندانم یا بهشت است</p></div>
<div class="m2"><p>بهشت است این ندانم یا بهار است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نسیم نسترن بفزود جانم</p></div>
<div class="m2"><p>مگر در وی نسیم زلف یار است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>درخت ارغوان گر نیست آتش</p></div>
<div class="m2"><p>چرا شاخش همیشه پر شرار است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همانا یاسمین مست شبانه است</p></div>
<div class="m2"><p>که چون مستان نوان و بی قرار است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چرا لاله همی ننشیند از پای</p></div>
<div class="m2"><p>مگر مر باده را در انتظار است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نشاط باده باید کرد بر گل</p></div>
<div class="m2"><p>که بازار نشاط باده خوار است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بیار ای ساقی آن آب چو آتش</p></div>
<div class="m2"><p>که جان را جان و غم را غمگسار است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو زلف یار بویش دلفریب است</p></div>
<div class="m2"><p>چو وصل دوست طعمش خوشگوار است</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>صفات او چو انعام خداوند</p></div>
<div class="m2"><p>برون از حد و افزون از شمار است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جمال العتره مجدالدین که دین را</p></div>
<div class="m2"><p>ز قصد دشمنان دین حصار است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ابوالقاسم علی تاج المعالی</p></div>
<div class="m2"><p>که چرخ فضل و خورشید تبار است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خداوندی که اندر علم و در حلم</p></div>
<div class="m2"><p>ز حیدر وز پیامبر یادگار است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نسیم مهر او سازنده نور است</p></div>
<div class="m2"><p>سموم کین او سوزنده نار است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ز محنت دشمنان را گوشمال است</p></div>
<div class="m2"><p>ز نعمت دوستان را حق گزار است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دلیل عفو و خشمش سعد و نحس است</p></div>
<div class="m2"><p>نشان رفق و باسش تخت و دار است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به هر معنی که بندیشی تمام است</p></div>
<div class="m2"><p>به هر میدان که پیش آید سوار است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تن انصاف را در عالم عدل</p></div>
<div class="m2"><p>حواس پنج و ارکان چهار است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>هر آنچ از خاک سازد طبع خورشید</p></div>
<div class="m2"><p>به چشم جود او چون خاک خوار است</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>وز آنچ اندر صدف خیزد ز باران</p></div>
<div class="m2"><p>به نظم و نثرش اندر صد هزار است</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>وز آن گوهر که کانش ناف آهوست</p></div>
<div class="m2"><p>نسیم خلق او را ننگ و عار است</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>جماد و ناطق ار مدحش سرایند</p></div>
<div class="m2"><p>هنوز آن بر سبیل اختصار است</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>خطاب فضل والفاظ بزرگی</p></div>
<div class="m2"><p>جز او بر هر که باشد مستعار است</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>اساس جاه و بنیاد جلالش</p></div>
<div class="m2"><p>چو ترکیب فلکها استوار است</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>به شب روی سگالشهای اعدا</p></div>
<div class="m2"><p>کلام اللیل یمحوه النهار است</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>ز فضلش نقص بدخواهان بیفزود</p></div>
<div class="m2"><p>که فضل گل دلیل نقص خار است</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ندارد زر دریغ از معدن شکر</p></div>
<div class="m2"><p>که شکرش فربه از زر نزار است</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>اگر دریاش خوانم بس عجب نیست</p></div>
<div class="m2"><p>که هر لفظیش در شاهوار است</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>وگر گردونش گویم جای آن هست</p></div>
<div class="m2"><p>که گرد عالم فضلش مدار است</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خداوندا تویی کز قول و فعلت</p></div>
<div class="m2"><p>بزرگان جهان را اعتبار است</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>نه از دولت به جز ذکرت ذخیره ست</p></div>
<div class="m2"><p>نه از نعمت به جز شکرت شکار است</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>تو را ای سید آل پیمبر</p></div>
<div class="m2"><p>به جد و جود بر خلق افتخار است</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>ز جدت نا امیدان را امید است</p></div>
<div class="m2"><p>ز جودت بی یساران را یسار است</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>الا تا در جهان بادست و خاک است</p></div>
<div class="m2"><p>یکی پنهان و دیگر آشکار است</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حسود جاه تو با باد سرد است</p></div>
<div class="m2"><p>عدو دولت تو خاکسار است</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>(مدیح تو چنان گفتم من ایدون</p></div>
<div class="m2"><p>سده جشن ملوک نامدار است)</p></div></div>