---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>نوبهار بدیع بی همتا</p></div>
<div class="m2"><p>همتی بذل کرد بر صحرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به تاثیر بذل و همت او</p></div>
<div class="m2"><p>گشت صحرا بدیع و بی همتا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا گشت همتی مبذول</p></div>
<div class="m2"><p>بی گمان نعمتی شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باد چون زایران به بستان تاخت</p></div>
<div class="m2"><p>آنگه از بوی خوش گرفت نوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه حاجت به اهل بردارد</p></div>
<div class="m2"><p>زود بیند مراد خویش روا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت عشق عاشقان بفزود</p></div>
<div class="m2"><p>نغمه بلبل بدیع نوا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ابر بر باغ عاشق است ولیک</p></div>
<div class="m2"><p>هست معشوق او قرین جفا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاین بگرید چو دیده وامق</p></div>
<div class="m2"><p>وان بخندد چو چهره عذرا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر وفا داشتی نخندیدی</p></div>
<div class="m2"><p>هیچ معشوق را نماند وفا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دهن لاله را سرشک سحر</p></div>
<div class="m2"><p>کرد پر تازه لولو لالا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر نوا بلبل نوآیین یافت</p></div>
<div class="m2"><p>لولو اندر دهان لاله چرا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راست گویی که از کمان نرود</p></div>
<div class="m2"><p>تیر حکم زمانه جز به خطا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>کارها گر به راستی بودی</p></div>
<div class="m2"><p>راست بودی بنفشه را بالا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قامت پیر اگر دو تا باشد</p></div>
<div class="m2"><p>راست بر رفته قامت برنا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عمر سو از بنفشه بیشتر است</p></div>
<div class="m2"><p>از چه شد قامت بنفشه دو تا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نرگس آن حال کی پسندیدی</p></div>
<div class="m2"><p>گر نبودیش دیده نابینا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>آن گل سرخ بر کران چمن</p></div>
<div class="m2"><p>زرد گل را همی کند رسوا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>که من ار لعل گشته ام بی می</p></div>
<div class="m2"><p>زرد چون مانده ای تو بی صفرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>یا بداندیش خواجه ای که همی</p></div>
<div class="m2"><p>زرد رویی نگردد از تو جدا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>من چو رخسار نیکخواهانش</p></div>
<div class="m2"><p>هر زمان لعل تر کنم سیما</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جایگاه امان امین الملک</p></div>
<div class="m2"><p>والی رای و همت والا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شرف الحضره آنکه حضرت اوست</p></div>
<div class="m2"><p>کعبه حاجت همه فضلا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>سبب عمر عدل و فضل عمر</p></div>
<div class="m2"><p>چون عمر عامل خلا به ملا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>آسمانی که آسمان برین</p></div>
<div class="m2"><p>جوید از قدر او همیشه علا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>آفتابی که آفتاب فلک</p></div>
<div class="m2"><p>خواهد از رای او همیشه ضیا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>آن بود با علو این چو زمین</p></div>
<div class="m2"><p>وین بود با ضیای آن چو سها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رادی از طبع او قوی گردد</p></div>
<div class="m2"><p>همچو دعوی مدعی به گوا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زفتی از دست او ضعیف شود</p></div>
<div class="m2"><p>همچو طاعات بندگان ز ریا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از حساب عطاش درماند</p></div>
<div class="m2"><p>آنکه احصا کند حساب حصا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گرچه سصید چو میرک سیناست</p></div>
<div class="m2"><p>اوست مقلوب میرک سینا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>جود عفرا و طبع او عروه است</p></div>
<div class="m2"><p>بس به غایت رسید عشق و هوا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>گر به جانش طمع کنی گوید</p></div>
<div class="m2"><p>هان هلا باژگونه کن عفرا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>لیکن ایزد نیافرید دلی</p></div>
<div class="m2"><p>کاین طمع دارد اندر او ماوا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>آسمان وسعود وی شده است</p></div>
<div class="m2"><p>فتنه بر وی چو سعد بر اسما</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا چو باران براو همی بارد</p></div>
<div class="m2"><p>هر زمان نو سعادتی ز سما</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>برج جوزا جواز او دارد</p></div>
<div class="m2"><p>اوج خورشید از آن بود جوزا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>فضل او بی کرانه چون دریاست</p></div>
<div class="m2"><p>لفظ او گوهر بلند بها</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>سایل از لفظ او گهر یابد</p></div>
<div class="m2"><p>نه بدیع است گوهر از دریا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>هر کجا رفق او پدید آید</p></div>
<div class="m2"><p>بدماند ز سنگ خاره گیا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر کجا باس او نماید روی</p></div>
<div class="m2"><p>موم گردد ز بیم او خارا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>خلق او را صفت همی گفتم</p></div>
<div class="m2"><p>خاک بوسید عنبر سارا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>همتش را ثنا همی گفتم</p></div>
<div class="m2"><p>سر فروبرد گنبد خضرا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خدمت بزم او کند شب و روز</p></div>
<div class="m2"><p>طرب انگیز از آن بود صهبا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>عنبر خلق او برد به دماغ</p></div>
<div class="m2"><p>زان سبب خوش بود نسیم صبا</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>از جهان حصه مخالف اوست</p></div>
<div class="m2"><p>رنج بی ناز و خار بی خرما</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>تا بود بهره موافق او</p></div>
<div class="m2"><p>شب بی روز و صبح بی فردا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>ای به هر خوبی از فلک در خور</p></div>
<div class="m2"><p>ای به هر نیکی از زمانه سزا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>که تواند سزا و در خور تو</p></div>
<div class="m2"><p>گفتن از بندگان دعا و ثنا</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا بقا و فناست در گیتی</p></div>
<div class="m2"><p>از بقای تو دور باد فنا</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کمترین نعمت ولیت نشاط</p></div>
<div class="m2"><p>بهترین راحت عدوت بلا</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>دیده دولت تو نادیده</p></div>
<div class="m2"><p>هیچ روی شماتت اعدا</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بر سر نامه سعادت تو</p></div>
<div class="m2"><p>زده توقیع جاودانه بقا</p></div></div>