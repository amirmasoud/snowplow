---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>ای زلف دلبر من دلبند و دلگسلی</p></div>
<div class="m2"><p>گه در پناه مهی گه در جوار گلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در پناه مهی چون چرخ بد چه کنی</p></div>
<div class="m2"><p>ور در جوار گلی چون خار دل چه خلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر گل همی گذری بر مه همی سپری</p></div>
<div class="m2"><p>دل را همی گسلی وز دل نمی گسلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از اصل لاله نه ای بر لاله معتکفی</p></div>
<div class="m2"><p>از جنس زهره نه ای با زهره متصلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دودی بر آتش رخ، لرزان بدان سببی</p></div>
<div class="m2"><p>درعی ز مشک سیه، پر حلقه زان قبلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسایش نظری، آرایش قمری</p></div>
<div class="m2"><p>پیرایه شکری، همسایه عسلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه بریده سری بی نقص و بی گنهی</p></div>
<div class="m2"><p>ور چه شکسته تنی بی عیب و بی خللی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر نام توست غزل، در کام توست طرب</p></div>
<div class="m2"><p>هم حجت طربی هم حاجت غزلی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون وقت فضل بود، مقصود جان و تنی</p></div>
<div class="m2"><p>چون روز عشق بود، معشوق چشم و دلی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>همراه جان و دلی وز جان و دل عوضی</p></div>
<div class="m2"><p>هم رنگ مشک و شبی، وز مشک و شب بدلی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>کردی تو قصد دلم وز بی دلی خجلم</p></div>
<div class="m2"><p>گر قصد جان نکنی از من به دل بحلی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>مهر است بر تو مرا گرچه ز روی جفا</p></div>
<div class="m2"><p>چون کین صدر اجل، یاریگر اجلی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن مجد دین رسول، آن فخر موسویان</p></div>
<div class="m2"><p>آن در سخا و سخن، چون جد خویش ملی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>دریای علم علی خورشید آل نبی</p></div>
<div class="m2"><p>حلمش چو حلم نبی، علمش چو علم علی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آن ناصر ملکان کو راست چون ملکان</p></div>
<div class="m2"><p>مالش ز بهر عدو، بالش ز بهر ولی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در بذل جود و عطا در کسب مدح و ثنا</p></div>
<div class="m2"><p>قولی بود همه کس، او قولی و عملی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از بر شامل او ابر است با دژمی</p></div>
<div class="m2"><p>وز بحر کامل او بحر است با خجلی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای کعبه فضلا ای قبله امرا</p></div>
<div class="m2"><p>هم قبله طمعی هم کعبه املی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جاه و جلال تو را درفخر دست قوی</p></div>
<div class="m2"><p>قدر و کمال تو را در شرع نص جلی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>یک نقطه از نسبت بوطالب قرشی</p></div>
<div class="m2"><p>یک نکته از ادبیت بوالاسود دوئلی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>سادات را ملکی اسلام را فلکی</p></div>
<div class="m2"><p>هم در سمو ملکی هم در علو زحلی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>عرض تو را نبود بس حاجتی به ثنا</p></div>
<div class="m2"><p>خورشید راست هیم خود حلیه بی حللی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>افضال و لطف تو را اجماع قدر تو را</p></div>
<div class="m2"><p>یکسان به قول و عمل جبری و معتزلی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>شایسته وقت سخا، چن علم منتفعی</p></div>
<div class="m2"><p>بایسته گاه سخن، چون طبع معتدلی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>در مرتبت فلکی در منقبت خردی</p></div>
<div class="m2"><p>در محمدت سمری در مکرمت مثلی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>گردون ستم نکند تا مانع ستمی</p></div>
<div class="m2"><p>گیتی حیل نکند تا دافع حیلی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در روز رنج و غضب مریخ در اسدی</p></div>
<div class="m2"><p>در وقت بخش و عطا خورشید در حملی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>چون عزم عفو کنی بینای بی غلطی</p></div>
<div class="m2"><p>چون رای جود زنی دانای بی زللی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ناصر شدی به لقب حافظ شدی به صفت</p></div>
<div class="m2"><p>هم حافظ هنری هم ناصر مللی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>در بحر مدح توام، ایمن ز بیم بلا</p></div>
<div class="m2"><p>انی الغریق فما خوفی من البلل</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>تا دولت ازلی ایمن بود زفنا</p></div>
<div class="m2"><p>ایمن بمان زفنا در دولت ازلی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>کردم نثار سخن بر گوش و گردن او</p></div>
<div class="m2"><p>ظاهر کمال خرد پیدا جمال و حلی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گفتم به وزن عرب لفظی به مدح عجم</p></div>
<div class="m2"><p>جز مدح من نکند کش بشنود جبلی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>مستفعلن فعلن مستفعلن فعلن</p></div>
<div class="m2"><p>اعلی الممالک ما یبنی علی الاسل</p></div></div>