---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>کرا نیست دل در کف دلبری</p></div>
<div class="m2"><p>نیابد به کام دل از دل، ری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر از دل به کام دل آن کس برد</p></div>
<div class="m2"><p>که دایم بود در برش دلبری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ولیکن چه درمان که اندر جهان</p></div>
<div class="m2"><p>نماند همی دلبری در بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگه کن بدان باغ دلبر که بود</p></div>
<div class="m2"><p>گشاده در او هر دلی را دری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هر طرف او خرمن لاله ای</p></div>
<div class="m2"><p>به هر گام او توده عنبری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از او هر درختی یکی خسروی</p></div>
<div class="m2"><p>سر هر یکی را بدیع افسری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به پیمان هر افسری کشوری</p></div>
<div class="m2"><p>به فرمان هر خسروی لشکری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز بی مهری لشکر مهرگان</p></div>
<div class="m2"><p>نبینی کنون افسری بر سری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بهار ار زمرد همی از درخت</p></div>
<div class="m2"><p>درآویخت چون دلبری زیوری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>خزان زان زمرد همی زر کند</p></div>
<div class="m2"><p>زهی من غلام چنین زرگری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>به دیدار این طرفه صنعت رواست</p></div>
<div class="m2"><p>که بینا شود چشم هر عبهری</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هم اکنون خزان بینی از شرم سر</p></div>
<div class="m2"><p>درآرد به کافورگون چادری</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>به باغ اندر از میوه چندین بتان</p></div>
<div class="m2"><p>ندانم که آراست بی آزری</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>درخت آنگهی کاسمان گونه بود</p></div>
<div class="m2"><p>ندیدم چو اختر بر او پیکری</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کنون کآسمان رنگ از او بازخواست</p></div>
<div class="m2"><p>پدید آمد از هر سویش اختری</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>به گوهر بماند همی سیب سرخ</p></div>
<div class="m2"><p>شنیدی چنین کم بها گوهری</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گر آبی به اختر بماند رواست</p></div>
<div class="m2"><p>که او مادری بود و این دختری</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چرا نار ماننده اخگر است</p></div>
<div class="m2"><p>که ناید چنین سودمند اخگری</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چو انگور مر باده را مادر است</p></div>
<div class="m2"><p>روان را به راحت بهین رهبری</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فدا دارد از بهر فرزند جان</p></div>
<div class="m2"><p>چنین مهربان کم بود مادری</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>به فرزند او جان بپرور که نیست</p></div>
<div class="m2"><p>چنو در جهان هیچ جان پروری</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>نه چون می طرب گستری دید کس</p></div>
<div class="m2"><p>نه چون خواجه هرگز درم گستری</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>عمید و عماد همه مملکت</p></div>
<div class="m2"><p>مهین حق گزاری بهین مهتری</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>عمر کاندر احکام عدل آمده است</p></div>
<div class="m2"><p>هر انگشت از دست او عمری</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نه بی شکر او بر زبان نکته ای</p></div>
<div class="m2"><p>نه بی مدح او در جهان دفتری</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نه چون حکم او عدل را حاکمی است</p></div>
<div class="m2"><p>نه جز کلک او ملک را داوری</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه اقرار حریش را منکری است</p></div>
<div class="m2"><p>نه معروف رادیش را منکری</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نه جان را به بایستگی دیگری است</p></div>
<div class="m2"><p>نه او را به شایستگی دیگری</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نه محکم تر از حزم او جوشنی است</p></div>
<div class="m2"><p>نه بران تر از کلک او خنجری</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نه در غیب او عیب را مظهری است</p></div>
<div class="m2"><p>نه از علم او غیب را مضمری</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>به مهر ار اشارت کند بر زمین</p></div>
<div class="m2"><p>پدید آید اندر زمان کوثری</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به خشم ار نهد چشم زی آسمان</p></div>
<div class="m2"><p>ثریا برابر شود با ثری</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>به جوهر عرض قایم آمد وز اوست</p></div>
<div class="m2"><p>قیام مهمات هر جوهری</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>کرا در سر از مهر او مغز نیست</p></div>
<div class="m2"><p>به گردن در از غم بود چنبری</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>کجا ذوالفقاری کند کلک او</p></div>
<div class="m2"><p>نبینی تنی با سر عنتری</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>کجا قوت دست اقبال اوست</p></div>
<div class="m2"><p>به بازی نسنجد در خیبری</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>کرا عنتر و خیبر آید به دست</p></div>
<div class="m2"><p>بباید دل و زهره حیدری</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هنر گر بگردد به گرد جهان</p></div>
<div class="m2"><p>نیاید به از کلک او درخوری</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بود در صف عاد بدخواه او</p></div>
<div class="m2"><p>ازو هر صریری یکی صرصری</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>نه تابنده از طاعت او سری است</p></div>
<div class="m2"><p>نه پاینده با زخم او مغفری</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>چو ابر ار به گوهر نه آبستن است</p></div>
<div class="m2"><p>چه دارد خروشیدن تندری</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>سر شرع و علم مسلمانی اوست</p></div>
<div class="m2"><p>ولیکن سرش چون دل کافری</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>خرد اعور دوربین خواندش</p></div>
<div class="m2"><p>چنین دوربین دیده ای اعوری</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>خداوند اگر پیش خدمت نیم</p></div>
<div class="m2"><p>همی گیرم از رنج دل کیفری</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همی گردم اینک خرد کرده گم</p></div>
<div class="m2"><p>چو گردی در این بی نوا کردری</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گهی جامه چون خرمن لاله ای</p></div>
<div class="m2"><p>گهی دیده چون حوض نیلوفری</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>نه چشم مرا صورت لعبتی</p></div>
<div class="m2"><p>نه گوش مرا نغمت مزمری</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>زترمذ به راون چنان آمدم</p></div>
<div class="m2"><p>چو با گوهری سوی بدگوری</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به آخر چو بلعام باطل شدم</p></div>
<div class="m2"><p>وز آغاز بودم چو پیغمبری</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>هر آن کاندر این ره بدیدی مرا</p></div>
<div class="m2"><p>بر اسبی نشسته بدیدی خری</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>چو کشتی مرا مرکبی زیر ران</p></div>
<div class="m2"><p>زپای و رکاب منش لنگری</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>رسیدیم و این شهر با شهره دید</p></div>
<div class="m2"><p>که دیدنش در دیده زد نشتری</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>در او با بنا گشته هر بی بنی</p></div>
<div class="m2"><p>براو چون علی گشته هر قنبری</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>نه در قوم او قیمت مردمی</p></div>
<div class="m2"><p>نه در باغ او قامت عرعری</p></div></div>
<div class="b" id="bn55"><div class="m1"><p>نه جز سرد و بی تاب طبع و دلی</p></div>
<div class="m2"><p>نه جز خشک و بی آب جوی و جری</p></div></div>
<div class="b" id="bn56"><div class="m1"><p>کنون اندر این شهر بی بر منم</p></div>
<div class="m2"><p>دوم بالشی و سیوم بستری</p></div></div>
<div class="b" id="bn57"><div class="m1"><p>نه مشک مرا یافته نافه ای</p></div>
<div class="m2"><p>نه عود مرا ساخته مجمری</p></div></div>
<div class="b" id="bn58"><div class="m1"><p>چه غم ها خورد دل که ماند جدا</p></div>
<div class="m2"><p>چنین خاطبی از چنان منبری</p></div></div>
<div class="b" id="bn59"><div class="m1"><p>ایا نقش کلک تو بر روی درج</p></div>
<div class="m2"><p>چو بر سوسنی رسته سیسنبری</p></div></div>
<div class="b" id="bn60"><div class="m1"><p>مرا روز هم رنگ سیسنبر سات</p></div>
<div class="m2"><p>مرا دیده هم گونه معبری</p></div></div>
<div class="b" id="bn61"><div class="m1"><p>به هر ساعتی باد ترمذ مرا</p></div>
<div class="m2"><p>بسوزد دل و جان به گرم آذری</p></div></div>
<div class="b" id="bn62"><div class="m1"><p>به اسبی نجستی رضای رهی</p></div>
<div class="m2"><p>بیندیش از بهر من استری</p></div></div>
<div class="b" id="bn63"><div class="m1"><p>ولیکن شرنگی که حاصل بود</p></div>
<div class="m2"><p>سوی من به از وعده شکری</p></div></div>
<div class="b" id="bn64"><div class="m1"><p>به استر بیرزد چو من بنده ای</p></div>
<div class="m2"><p>به اسب ار نیرزد چو من چاکری</p></div></div>
<div class="b" id="bn65"><div class="m1"><p>اگر پیش تو بودمی بستمی</p></div>
<div class="m2"><p>ز خدمتگری بر میان میرزی</p></div></div>
<div class="b" id="bn66"><div class="m1"><p>الا تا هوا و آتش و خاک و آب</p></div>
<div class="m2"><p>بود مایه جان هر جان وری</p></div></div>
<div class="b" id="bn67"><div class="m1"><p>از آن می که جان را زیادت کند</p></div>
<div class="m2"><p>همه ساله بر دست تو ساغری</p></div></div>
<div class="b" id="bn68"><div class="m1"><p>شرابی که خورشید را منظر است</p></div>
<div class="m2"><p>همی خور به دیدار مه منظری</p></div></div>
<div class="b" id="bn69"><div class="m1"><p>نه هست از تو امید را چاره ای</p></div>
<div class="m2"><p>نه خورشید را چاره از خاوری</p></div></div>
<div class="b" id="bn70"><div class="m1"><p>همی تا ستایش بود در جهان</p></div>
<div class="m2"><p>ستایش بر از هر ستایشگری</p></div></div>
<div class="b" id="bn71"><div class="m1"><p>زدفتر چو این خواندی آن را بخوان</p></div>
<div class="m2"><p>«چنین خواندم امروز در دفتری»</p></div></div>