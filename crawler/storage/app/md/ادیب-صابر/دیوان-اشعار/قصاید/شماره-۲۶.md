---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>خوبی به روی خوب تو اقرار می‌کند</p></div>
<div class="m2"><p>عقل از نهیب عشق تو زنهار می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل را دل چو سنگ تو آزار می‌دهد</p></div>
<div class="m2"><p>دم را دهان تنگ تو افگار می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوشتر ز جان و عمری و از خواب خوش مرا</p></div>
<div class="m2"><p>آن چشم نیم‌خواب تو بیدار می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید دلبرانی و رویت به دلبری</p></div>
<div class="m2"><p>با خویشتن دو زلف تو را یار می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون جان بی‌گناهی و سودای عشق تو</p></div>
<div class="m2"><p>جان مرا همیشه گنه‌کار می‌کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بس که در دلم ز تو طوفان محنت است</p></div>
<div class="m2"><p>کشتی بر آب دیده من کار می‌کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وز بس که یاد آن لب و رخسار می‌کنم</p></div>
<div class="m2"><p>عشقم اسیر آن لب و رخسار می‌کند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آسان همی‌نمود دلم را طریق صبر</p></div>
<div class="m2"><p>او را طریق عشق تو دشوار می‌کند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدار تو که مَهْ صفتِ حُسن از او گرفت</p></div>
<div class="m2"><p>دل را به دام فتنه گرفتار می‌کند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بر دل بلا و فتنه ز دیدار می‌رسد</p></div>
<div class="m2"><p>عدلی از آن خصومت دیدار می‌کند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اشک مرا به رنگ عقیق گداخته</p></div>
<div class="m2"><p>تیمار آن عقیق شکربار می‌کند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جانم بلای عشق تو بسیار می‌کشد</p></div>
<div class="m2"><p>عقلم حدیث حسن تو بسیار می‌کند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جعد تو آن هوای خراسان به بوی مشک</p></div>
<div class="m2"><p>به از هوای تبت و تاتار می‌کند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>زلف تو صید کردن مقصود خویش را</p></div>
<div class="m2"><p>کار کمند خسرو دیندار می‌کند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عادل علاء دولت و دنیا و دین که عدل</p></div>
<div class="m2"><p>پیش دلش به بندگی اقرار می‌کند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>دارای روزگار که بدخواه ملک را</p></div>
<div class="m2"><p>از چوب تخت دشمن خود دار می‌کند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اتسز که روز معرکه رمح از دو دست او</p></div>
<div class="m2"><p>کار هزار لشکر جرّار می‌کند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هرچ آن به تیغ قهر ستاند ز دشمنان</p></div>
<div class="m2"><p>آثار جود او همه ایثار می‌کند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>که پیکرست مرکب رهوار پادشاه</p></div>
<div class="m2"><p>که را رکیب اوست که رهوار می‌کند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>نی نی چو شهریار سپهر است و آفتاب</p></div>
<div class="m2"><p>اسبش مسیر کوکب سیّار می‌کند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>باد سبک رواست و گه رزم خاک را</p></div>
<div class="m2"><p>دایم ز باد حمله گرانبار می‌کند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر نقطه‌ای بگردد چون یافت امتحان</p></div>
<div class="m2"><p>پرگاروار گردش پرگار می‌کند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ایزد جزای کافر و مومن در این جهان</p></div>
<div class="m2"><p>از جود و تیغ شاه پدیدار می‌کند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از جود او مثوبت مومن چو می‌دهد</p></div>
<div class="m2"><p>از تیغ او عقوبت کفار می‌کند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا زین چهار طبع چنو شهریار خاست</p></div>
<div class="m2"><p>هفتم سپهر خدمت این چار می‌کند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شاها تویی که رایت اعدات را خدای</p></div>
<div class="m2"><p>در پیش رایت تو نگونسار می‌کند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>علمت نشان حیدر کرّار می‌دهد</p></div>
<div class="m2"><p>تیغت فتوح حیدر کرّار می‌کند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>نیلوفرست تیغ تو و روز کارزار</p></div>
<div class="m2"><p>گل‌های دشمنان تو را خار می‌کند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>از خون بدسگال تو بر خاک رزمگاه</p></div>
<div class="m2"><p>گلزار می‌دماند و گلزار می‌کند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نارکفید می‌کند از مغز دشمنان</p></div>
<div class="m2"><p>وز روی دوستان تو گلنار می‌کند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در گنج ناصحان تو دینار می‌نهد</p></div>
<div class="m2"><p>وز روی حاسدان تو دینار می‌کند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چون التجا به ایزد جبّار می‌کنی</p></div>
<div class="m2"><p>ترتیب ملکت ایزد جبّار می‌کند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>در طلعت تو فر محمد همی‌نهد</p></div>
<div class="m2"><p>وز لشکرت مهاجر و انصار می‌کند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>دیوار ازآن کنند شها گرد خانه‌ها</p></div>
<div class="m2"><p>تا دشمن تو روی به دیوار می‌کند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>خون می‌فشاند از مژه و روز رزم تو</p></div>
<div class="m2"><p>جان را فدای خنجر خونخوار می‌کند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>هر دل که در خلاف تو بیمار می‌شود</p></div>
<div class="m2"><p>تیرت علاج داروی بیمار می‌کند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>گاهی به جان و عمرش و گاهی ز ملک و مال</p></div>
<div class="m2"><p>آزار می‌رساند و بیزار می‌کند</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>شاها بهار تازه صورتگر آمده است</p></div>
<div class="m2"><p>بر خار خشک صورت فرخار می‌کند</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>بی‌رزمه زیب رزمه بزّاز می‌دهد</p></div>
<div class="m2"><p>بی‌طبله کار طبله عطّار می‌کند</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>هر سر که مهرگان به دل خاک در نهاد</p></div>
<div class="m2"><p>نوروز کشف آن همه اسرار می‌کند</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ابر سحرگهی چو کف تو به روز بزم</p></div>
<div class="m2"><p>بر گل نثار لؤلؤ شهوار می‌کند</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>آن نقش‌های طرفه نگه کن که بی‌قلم</p></div>
<div class="m2"><p>نقاش صنع بر سر کهسار می‌کند</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>هر لحظه‌ای نگاری و هر ساعتی گلی</p></div>
<div class="m2"><p>دیدار می‌نماید و بازار می‌کند</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>روی نگار دمدمه عشق می‌دهد</p></div>
<div class="m2"><p>مرغ بهار زمزمهٔ زار می‌کند</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>هر صلصلی ترانهٔ عشّاق می‌کشد</p></div>
<div class="m2"><p>هر بلبلی روایت اشعار می‌کند</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>گویی بهار تازه خریدار یافته است</p></div>
<div class="m2"><p>رخسار عرضه پیش خریدار می‌کند</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>گویی چمن ز ناله مرغ و نسیم گل</p></div>
<div class="m2"><p>با رودکی حکایت عیّار می‌کند</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>بر شاخ گل ز قمری نالنده، عندلیب</p></div>
<div class="m2"><p>گویی سبق گرفت که تکرار می‌کند</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>می خور شها که ردش ایام تیزرو</p></div>
<div class="m2"><p>بر حسب آرزوی تو رفتار می‌کند</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>از بوی باده مست کن این چرخ را از آنک</p></div>
<div class="m2"><p>پیوسته قصد مردم هشیار می‌کند</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>تا نور شمس مایه انوار می‌دهد</p></div>
<div class="m2"><p>تا جرم چرخ گردش هموار می‌کند</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>بادت همیشه گردش چرخ از موافقان</p></div>
<div class="m2"><p>تا بر مخالفان تو پیکار می‌کند</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>گر نیستی ز داد تو عالم شدی خراب</p></div>
<div class="m2"><p>با این ستم که چرخ ستمکار می‌کند</p></div></div>