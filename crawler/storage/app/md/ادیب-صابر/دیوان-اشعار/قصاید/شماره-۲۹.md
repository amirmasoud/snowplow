---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>سبزه ها چون نقش دیبا دلبر و زیبا شدند</p></div>
<div class="m2"><p>ابر دیباباف شد تا سبزه ها دیبا شدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره باران به اشک دلبران ماننده شد</p></div>
<div class="m2"><p>راغها چون روی دلداران از آن زیبا شدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقان را عاشقی گر واله و شیدا کند</p></div>
<div class="m2"><p>بلبلان از عشق گلها واله و شیدا شدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گل اندر باغها چون روی معشوقان شکفت</p></div>
<div class="m2"><p>رازهای عاشقان از باغ و دل پیدا شدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بهاران از دل گل تاگل رعنا دمید</p></div>
<div class="m2"><p>دلبران از روی چون گل همچو گل رعنا شدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از صبای مشکبار و از نسیم نافه بوی</p></div>
<div class="m2"><p>نافه ها کاسد شدند و مشک ها رسوا شدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی دریاها اگر ماوای گوهرها بود</p></div>
<div class="m2"><p>شهرها از ابر گوهربار چون دریا شدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره ها کز دیده های ابر بیرون آمدند</p></div>
<div class="m2"><p>بی صدف بر روی سبزه لولو لالا شدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا بنفشه چون خط خوبان یغمایی دمید</p></div>
<div class="m2"><p>عاشقان را صبر و دل بادیدنش یغما شدند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ابر نوروز از گرستن دیده وامق شده ست</p></div>
<div class="m2"><p>تا گل و لاله به رنگ عارض عذرا شدند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>با چنین نور و نوا کاین باغ و صحرا یافتند</p></div>
<div class="m2"><p>جان و دل جویای باغ و عاشق صحرا شدند</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا به بالای حمل رفت آفتاب از پشت حوت</p></div>
<div class="m2"><p>شاخ و برگ هر نبات از دشت بر بالا شدند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طبع را سودای باغ و بوستان مستی دهد</p></div>
<div class="m2"><p>قمری و بلبل همانا مست از این سودا شدند</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ابر اگر ساقی نشد باران اگر صهبا نگشت</p></div>
<div class="m2"><p>از چه معنی لاله ها چون ساغر صهبا شدند</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از پی پیوستن نسل گل و فصل بهار</p></div>
<div class="m2"><p>راست گویی ابر و باران آدم و حوا شدند</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>وز برای دیدن بزم و تماشاگاه شاه</p></div>
<div class="m2"><p>صحن باغ و صورت گل جنت و حورا شدند</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر نشاط دیدن بزم جهان آرای او</p></div>
<div class="m2"><p>دیده های نرگسان در بوستان بینا شدند</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بوستان ها همچو تاج خسروان پر در و زر</p></div>
<div class="m2"><p>از برای بزمگاه خسرو والا شدند</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>بر زمین و بر زمان آثار عدل شه رسید</p></div>
<div class="m2"><p>زان پس از پیری جوان و تازه و برنا شدند</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داور عادل علاء دین و دولت کز علو</p></div>
<div class="m2"><p>قدر و رای او دو تاج گنبد اعلا شدند</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>آن خداوندی که از انواع اقبال و قبول</p></div>
<div class="m2"><p>بندگانش برتر از اسکندر و دارا شدند</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر زمین ها را ز حلم او ثبات آمد پدید</p></div>
<div class="m2"><p>آسمانها از نهیب خشم او دروا شدند</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا به پرواز اندر آمد باز باز عدل او</p></div>
<div class="m2"><p>ظلم و ظالم در جهان پنهان تر از عنقا شدند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>از عطای همتش بی نعمتان منعم شدند</p></div>
<div class="m2"><p>وز رسوم دولتش بی دانشان دانا شدند</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>هر زمان از جود او بر گنج او غوغا رسد</p></div>
<div class="m2"><p>مفلسان بی رنج تن با گنج از این غوغا شدند</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خسروا در علم و حکمت عالم تنها شدی</p></div>
<div class="m2"><p>عالمان از دل غلام عالم تنها شدند</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دوستان را تا به اقبال تو شبها روز شد</p></div>
<div class="m2"><p>روزهای دشمنان تو شب یلدا شدند</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کعبه امن و امانی لاجرم در مرتبت</p></div>
<div class="m2"><p>بارگاه و مجلس تو مکه و بطحا شدند</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>تا تو خورشید ملوکی بندگان درگهت</p></div>
<div class="m2"><p>بر مثابت برتر از خورشید در جوزا شدشند</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>از خداوندان که آرم در جهان همتا تو را</p></div>
<div class="m2"><p>کز جلالت رفعت و قدر تو بی همتا شدند</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از غلو کردن خرد ترسان بود در وصف تو</p></div>
<div class="m2"><p>امت عیسی غلو کردند از آن ترسا شدند</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زانکه هر امروز اقبال تو از دی بهترست</p></div>
<div class="m2"><p>حاسدان از بیم امروز تو بی فردا شدند</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تا شکوه عدل و انصاف تو بر آفاق تافت</p></div>
<div class="m2"><p>سنگها گوهر شدند و خارها خرما شدند</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هشت چرخ و هفت کوکب چار طبع و پنج حس</p></div>
<div class="m2"><p>خدمتت را بنده مطواع دل یکتا شدند</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>تا ضمیر ما مدیحت گفت گویی شعر و سحر</p></div>
<div class="m2"><p>چاکر طبع و ضمیر و سحر شعر ما شدند</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در جهان تا خرمی جویی ز بزم خویش جوی</p></div>
<div class="m2"><p>خرمیها هر کجا بزم تو بود آنجا شدند</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تا تو باشی خسروا یک لحظه بی اعدا مباش</p></div>
<div class="m2"><p>کز ثریا تا ثری اقبال را اعدا شدند</p></div></div>