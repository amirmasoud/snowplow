---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>چند بارم بر فراق دلبران از دیده آب</p></div>
<div class="m2"><p>چند باشم آتش تیمار خوبان را کباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تاسرشکم بیشتر شد صبر من کمتر شدست</p></div>
<div class="m2"><p>راست پنداری مگر من صبر می بارم نه آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طبع و دستم با دو چیز اندر جهان الفت گرفت</p></div>
<div class="m2"><p>طبع با تیمار عشق و دست با جام شراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی آرد جوانی خرما طبع جوان</p></div>
<div class="m2"><p>بی غمی خیزد زمستی حبذامست خراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش چشمم روز تا شب پیش دل شب تا به روز</p></div>
<div class="m2"><p>داستان سعد و اسما قصه دعد و رباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بافلان دلبر چه گفت و بافلان بیدل چه کرد</p></div>
<div class="m2"><p>آن چه کرد این را سوال و این چه داد آن را جواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مونس عاشق چه باشد جز حدیث نیکوان</p></div>
<div class="m2"><p>چشم نیلوفر چه جوید جز فروغ آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باز دل در دلبری بستم که بندد هر شبی</p></div>
<div class="m2"><p>تا به هنگام سحر خوابم به چشم نیم خواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مهر او یکسر بلا و من طلبکار بلا</p></div>
<div class="m2"><p>عشق او یکسر عذاب و من خریدار عذاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>حال من در هجر او شد همچو زلفش تیره فام</p></div>
<div class="m2"><p>صبر من در عشق او چون وصل او شد تنگ یاب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>او و من هر دو به هر وقتی همی جوییم و هست</p></div>
<div class="m2"><p>جستن او بر خطا و جستن من بر صواب</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>او همی جوید به وقت بوسه بخشیدن درنگ</p></div>
<div class="m2"><p>من همی جویم به مدح مجلس عالی شتاب</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>صدر اهل بیت مجد دین ابوالقاسم علی</p></div>
<div class="m2"><p>ناقد لفظ و معانی صاحب کلک و کتاب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>آنکه گردون نزد قدرش چون بر گردون زمین</p></div>
<div class="m2"><p>آنکه دریا نزد جودش چون بر دریا سراب</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>آنکه مثل او نیابی هیچ کس در هیچ فن</p></div>
<div class="m2"><p>وانکه جنس او نبینی هیچکس در هیچ باب</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بنده دست و زبانش هم سخاو هم سخن</p></div>
<div class="m2"><p>بسته مهر و سپاسش هم قلوب و هم رقاب</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نسبت فضل از دل رخشان او گیرد خرد</p></div>
<div class="m2"><p>نسخت جود از کف احسان او خواهد سحاب</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جود بی توقیر او چون دیده ای باشد فراز</p></div>
<div class="m2"><p>عقل بی تدبیر او چون خانه ای باشد خراب</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>رای او و روی او و لفظ او و طبع او</p></div>
<div class="m2"><p>فضل محض و نور صرف و عقل پاک و جود ناب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ای خداوندی که از تو منقبت گیرد لقب</p></div>
<div class="m2"><p>وی سرافرازی از تو منزلت یابد خطاب</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>با مناقب هم نشینی با فضایل هم نشان</p></div>
<div class="m2"><p>با معالی هم عنانی با معانی هم رکاب</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سیرت تو در لطیفی چون هوای نوبهار</p></div>
<div class="m2"><p>همت تو در بلندی چون دعای مستجاب</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یک نسیم از روضه عفو تو در گیتی ارم</p></div>
<div class="m2"><p>یک شرر از آتش خشم تو برگردون شهاب</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>منت تو چون سخای کامل تو بی قیاس</p></div>
<div class="m2"><p>مدحت تو چون عطای شامل تو بی حساب</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بحر اگر چه جود ورزد کی بود چون دست تو</p></div>
<div class="m2"><p>باز اگر چه صید گیرد کی برآید با عقاب</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ابر اگر چه در فشاند کی بود چون لفظ تو</p></div>
<div class="m2"><p>رنگ پیری کی بپوشد زال گربندد خضاب</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>ای بزرگی کز تراب درگه میمون تو</p></div>
<div class="m2"><p>توتیای چشم خود سازند آل بوتراب</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>زردی از رخسار خصمت نگسلد صحبت همی</p></div>
<div class="m2"><p>همچنان چون سرخی از گلنار و سبزی از سداب</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>راست گویی اصل سیماب از دل بدخواه توست</p></div>
<div class="m2"><p>کز نهیب تو نباشد ساعتی بی اضطراب</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>نسبت کردار نیکو سوی فعل و رسم توست</p></div>
<div class="m2"><p>زان دهد ایزد همی کردار نیکو را ثواب</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>شعر من زیبا چنان آید همی بر نام تو</p></div>
<div class="m2"><p>چون نشاط اندر شراب و چون شراب اندر شباب</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا نباشد نام تو، نیکو نیاید شعر من</p></div>
<div class="m2"><p>تا نباشد آتش از گل کی توان کردن گلاب</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>یک جهان دوشیزگان دارم نهفته در ضمیر</p></div>
<div class="m2"><p>جز به عشق نام تو بیرون نیایند از حجاب</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>نیست احوالم نکو هر چند اشعارم نکوست</p></div>
<div class="m2"><p>روی نیکو هست لیکن نیست در خوردش نقاب</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>از جهان است این گناه از روزگار است این خلل</p></div>
<div class="m2"><p>از ستاره ست این جفا با آسمان است این عتاب</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>تا همی رخسار دلبندان بود پر زیب و حسن</p></div>
<div class="m2"><p>تا همی زلفین معشوقان بود پر پیچ و تاب</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>هر نشاطی کان تو را رغبت همی باشد بکن</p></div>
<div class="m2"><p>هر مرادی کان تو را در دل همی گردد بیاب</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>گرچه احوال جهان با انقلاب آمیخته است</p></div>
<div class="m2"><p>دور باد از دامن جاه تو دست انقلاب</p></div></div>