---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>بهار لاله رخساری نگار سرو بالایی</p></div>
<div class="m2"><p>گل و شمشاد زلفینی مه و خورشید سیمایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگار و مه تو را خوانم، بهار و گل تو راگویم</p></div>
<div class="m2"><p>که اینها عالم آرایند و آنها را تو آرایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به شب با ماه بازم عشق ازیرا ماه رخساری</p></div>
<div class="m2"><p>به روز از سرو باشم شاد ازیرا سرو بالایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر آنی که حاصل گشت یعقوب و زلیخا را</p></div>
<div class="m2"><p>زرویش فر برنایی زبویش نور بینایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه یوسف صورتی جاناکه چون بنمایی آن صورت</p></div>
<div class="m2"><p>گزیند عقل یعقوبی و گیرد جان زلیخایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زبیم چشم بد بر تو بخوانم سورت یوسف</p></div>
<div class="m2"><p>چو تو با صورت یوسف، مرا رخساره بنمایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو را جانا که جانانی و دلبندی و دلداری</p></div>
<div class="m2"><p>غلامانند جان و دل غلامان را چه فرمایی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه فرمان آمد از عشقت که تقصیری پدید آمد</p></div>
<div class="m2"><p>ز جان و دل در آن فرمان به مقدار توانایی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه دیبا و نه دیناری، ولیکن دل ربودن را</p></div>
<div class="m2"><p>چو دلبر شکل دیناری چو زیبا نقش دیبایی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو با عشق تو پیوستم شکیبی داشتم در دل</p></div>
<div class="m2"><p>که شرط عاشقان باشد به عشق اندر شکیبایی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جمال تو شکیبایی به زیبایی برید از من</p></div>
<div class="m2"><p>جمال است آنکه بر باید شکیبایی به زیبایی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر در عهد برنایی، گل وصل تو بوییدم</p></div>
<div class="m2"><p>چرا در ترک عهد من چو گل گشتی به رعنایی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چو برنایی برفت از من، ز عهد من برون رفتی</p></div>
<div class="m2"><p>دریغا عهد برنایی دریغا عهد برنایی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گذشت آن عهد و ان مدت رمید آن روز و آن ساعت</p></div>
<div class="m2"><p>که بودی طبع و عقلم را به پنهانی و پیدایی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>رخ جانان غذای جان لب ساغر قرین لب</p></div>
<div class="m2"><p>کف موسی رخ ساقی دم عیسی دم نایی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>کنون کز روز برنایی و از روی تو تنهایم</p></div>
<div class="m2"><p>اسیر دور گردونم ز جور این دو تنهایی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به برنایی و وصل تو تمنی می کند جانم</p></div>
<div class="m2"><p>چو بی چشمان به بینایی چو نادانان به دانایی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>غلام آن دلم کو را غلام عشق گرداند</p></div>
<div class="m2"><p>به غارت سرو تاتاری به یغما ماه یغمایی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اگر عاقل به از نادان و گر دانا به از شیدا</p></div>
<div class="m2"><p>شدم با عقل و دانایی غلام عشق و شیدایی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>رباینده است عشق تو ستاننده است زلف تو</p></div>
<div class="m2"><p>از این جز صبر نستانی و زان جز عقل نربایی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>درازی را سر زلفت به سرو قامتت ماند</p></div>
<div class="m2"><p>چو سرو باغ مجددین چرا او را نپیرایی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کریم خلق صدر شرق ابوالقاسم علی کایزد</p></div>
<div class="m2"><p>به قدر و جود او داده است گردونی و دریایی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خداوندی که مولایند رایش را و ذاتش را</p></div>
<div class="m2"><p>خردمندی و دانایی خداوندی و مولایی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>سخا را دل قوی گردد چو از دستش سخن رانی</p></div>
<div class="m2"><p>سخن را قدر بفزاید چو در مدحش بیفزایی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز لفظ او زیادت شد سخن را صاحب رازی</p></div>
<div class="m2"><p>ز بذل او پدید آمد سخا را حاتم طایی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خداوندا تویی آن کز بزرگی و خداوندی</p></div>
<div class="m2"><p>چو خورشیدی زبی مثلی چو گردونی به والایی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>زمین میدان جاه توست اگر چه آسمان قدری</p></div>
<div class="m2"><p>زحل دربان قصر توست اگر چه مشتری رایی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>اگر چه نسبت از پیغمبر آخر زمان داری</p></div>
<div class="m2"><p>کند انصاف و اقبالت کلیمی و مسیحایی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>گر آدم را به فرزندیت فخر آمد روا باشد</p></div>
<div class="m2"><p>که فخر آل و اولاد بهین فرزند حوایی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>زنور علم چون حیدر جهان تیره چون شب را</p></div>
<div class="m2"><p>چو زهره روشنی دادی که صدر آل زهرایی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>گر از نسبت شرف زاید در این بی مثل و مانندی</p></div>
<div class="m2"><p>ور از رتبت ثنا آید در آن بی جنس و همتایی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>چو دانش را قلم رانی همه فرهنگ و آدابی</p></div>
<div class="m2"><p>چو بخشش را نعم گویی همه آلا و نعمایی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>زهمت چون سخا ورزی به حکمت چون سخن گویی</p></div>
<div class="m2"><p>نیاز از خلق برداری و زنگ از عقل بزدایی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اگر گردون طریق ظلم بگشاید، تو در بندی</p></div>
<div class="m2"><p>و گر گیتی در انصاف بر بندد، تو بگشایی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>نزیبد جز تو را رفعت، که رفعت را تو می زیبی</p></div>
<div class="m2"><p>نشاید جز تو را رتبت که رتبت را تو می شایی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>به خدمت مهر جویان را ره اقبال جاویدی</p></div>
<div class="m2"><p>به نعمت مدخ گویان را در عیش مهیایی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>چنان بینی به چشم دل همی اسرار اعدا را</p></div>
<div class="m2"><p>که هر دانا چنان داند که صاحب سر اعدایی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>جهان نور از تو می گیرد مگر انباز خورشیدی</p></div>
<div class="m2"><p>همیشه برتری داری مگر هم راز جوزایی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>جهان را از تو حاصل شد هم آسایش هم آرایش</p></div>
<div class="m2"><p>به فضل آرایش دهری به بذل آسایش مایی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>اگر جان پرورد فردوس و دل خرم کند حورا</p></div>
<div class="m2"><p>همی نازند باغ و گل به فردوسی و حورایی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>پر از خوبان و حوران شد جهان یک چند جان پرور</p></div>
<div class="m2"><p>بدین خوبان نوروزی بدین حوران صحرایی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اگر خلقت نفرماید که فرماید به فروردین</p></div>
<div class="m2"><p>هوا را عنبر افشانی صبا را مشک پیمایی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>بهاری ابر کز دریا برآید قطره پالاید</p></div>
<div class="m2"><p>تویی آن ابر دریا دل که بر ما بدره پالایی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>چو در باغ آمدی گل را زبان رعد می گوید</p></div>
<div class="m2"><p>بدین شادی طراوت گیر و خوش بشکف چه می پایی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کزین پس با سرشک ابر و بذل بر مجددین</p></div>
<div class="m2"><p>زرنج خار نندیشی زباد دی نفرسایی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>خداوندا به جان و دیده گر حاجت بود تن را</p></div>
<div class="m2"><p>تو آن شخصی که عالم را چو جان و دیده می‌بایی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>اگر چه سخندانی مرا آمد ید بیضا</p></div>
<div class="m2"><p>خرد نامم زسودای مدیحت کرد سودایی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>ذخیره هر دو عالم شد مدیح تو مرا زیرا</p></div>
<div class="m2"><p>که هم اقبال امروزی و هم امید فردایی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به هر وقتم که یادآری مدایح را مهیایم</p></div>
<div class="m2"><p>به هر وقتت که مدح آرم ایادی را مهیایی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>چو حق رخساره بنماید مگر باطل شود باطل</p></div>
<div class="m2"><p>در افتادند مداحان از این مدحت به رسوایی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>همیشه تا دل عاقل به علم و عدل بگراید</p></div>
<div class="m2"><p>بزی تا همچنین دایم به علم و عدل بگرایی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>به دست حرمت باقی همه پای عدو بندی</p></div>
<div class="m2"><p>به پای همت عالی همه فرق فلک سایی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>زباز دولت عالی و شاهین مراد دل</p></div>
<div class="m2"><p>نصیب ناصح مصلح همایی باد و عنقایی</p></div></div>