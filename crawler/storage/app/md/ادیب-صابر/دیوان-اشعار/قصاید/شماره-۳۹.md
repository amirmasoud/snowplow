---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>اگر ندیده ای از مشک پیش لاله سپر</p></div>
<div class="m2"><p>همی نگر به سوی آن دو زلف لاله سپر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رخش همی به دی از لاله نوبهار کند</p></div>
<div class="m2"><p>اگر حذر کند از چشم بد رواست حذر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ندید کس که ز هیچ آتشی بنفشه دمید</p></div>
<div class="m2"><p>از آتش رخ او چون دمد بنفشه تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر شگفت بود لاله شکفته به دی</p></div>
<div class="m2"><p>بنفشه ای که ز آتش دمد شگفتی تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطش بنفشه و از شرم آن بنفشه همی</p></div>
<div class="m2"><p>بنفشه چمن باغ بر نیارد سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدان بنفشه فزاید جمال باغ و بهار</p></div>
<div class="m2"><p>بدین بنفشه فزاید جمال شمس و قمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر آن بنفشه همیدون ز خاک روید و آب</p></div>
<div class="m2"><p>بنفشه خط او را ز گل بود بستر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر این بنفشه نگویی بنفشه را چه محل</p></div>
<div class="m2"><p>بر این بنفشه نگویی بنفشه را چه خطر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از آن دو لاله که بشکفت بر دو عارض او</p></div>
<div class="m2"><p>جمال را خطر افزود و حسن را زیور</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>وز این بنفشه که بر عارض و رخش بدمید</p></div>
<div class="m2"><p>از آتش دل من بر فلک رسید شرر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اگر تو را هوس لاله و بنفشه کند</p></div>
<div class="m2"><p>به خط و عارض آن دلبر نگار نگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>و گر سعادت دل خواهی و سلامت جان</p></div>
<div class="m2"><p>به مدح صدر اجل دل فروز و جان پرور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سپهر همت و خورشید مجد مجدالدین</p></div>
<div class="m2"><p>خجسته تاج معالی علی بن جعفر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سر شرف شرف الساده عمده اسلام</p></div>
<div class="m2"><p>جهان عترت و اقبال آل پیغمبر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کریم عادت محمود فعل خوب خصال</p></div>
<div class="m2"><p>حمید خلق عطا گستر بزرگ نظر</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بلند نسبت پاکیزه عرق نیکو نام</p></div>
<div class="m2"><p>رهی نواز بهی منظر نکو مخبر</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>مفسر است همیشه ز سیرتش صورت</p></div>
<div class="m2"><p>مخبر است همیشه به مخبرش منظر</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نه بحر و بحر عطا و نه ابر و ابر نوال</p></div>
<div class="m2"><p>نه چرخ و چرخ علو و نه کوه و کوه جگر</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>علی علوم و علی کوشش و علی بخشش</p></div>
<div class="m2"><p>نبی خصال و نبی سیرت و نبی گوهر</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>زهی به مدحت صدرت فلک گشاده زبان</p></div>
<div class="m2"><p>زهی به خدمت قدرت سپهر بسته کمر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز بهر دیدن روی تو و ستایش تو</p></div>
<div class="m2"><p>شریف گشته زبان و عزیز گشته بصر</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>وز آن قبل که تویی اختر سپهر شرف</p></div>
<div class="m2"><p>بلند گشت سپهر و منیر گشت اختر</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>اگر نه از پی نشر محامدت بودی</p></div>
<div class="m2"><p>ز فخر مدح تو بر آسمان شدی دفتر</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>اگر نه فضل و هنر نسبت از دل تو کند</p></div>
<div class="m2"><p>در این جهان که تقرب کند به فضل و هنر</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز امن لعل تو لعلی گرفت گونه گل</p></div>
<div class="m2"><p>ز بیم جود تو زردی گرفت گونه زر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بود در آتش خشم تو ذره ای دوزخ</p></div>
<div class="m2"><p>بود ز آب رضای تو قطره ای کوثر</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نه جود را غرضی حاصل است بی کف تو</p></div>
<div class="m2"><p>نه در جهان عرضی ممکن است بی جوهر</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هم از جهانی و بیش است قدر تو ز جهان</p></div>
<div class="m2"><p>زکان به است وگرچه زکان بود گوهر</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ز روزگاری و بی شک ز روزگار بهی</p></div>
<div class="m2"><p>ز ابر بارد و بی شک به است از ابر مطر</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>همیشه معدن آزادگی دل و کف توست</p></div>
<div class="m2"><p>چنانکه معدن آهن در آتش است و حجر</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>اگر چه فخر به حیدر کند سخاوت و علم</p></div>
<div class="m2"><p>تویی به علم و سخاوت تفاخر حیدر</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>فضایل از تو خطر گیرد و شمایل قدر</p></div>
<div class="m2"><p>مناقب از تو شرف یابد و معالی فر</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ضمیر ما نشناسد محل حرمت تو</p></div>
<div class="m2"><p>هر آینه نشانسد صدف محل درر</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو نام نیک همی گستری عطا و سخن</p></div>
<div class="m2"><p>زهی کریم عطاپرور سخن گستر</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>هزار بار کم از قدر و رتبت تو بود</p></div>
<div class="m2"><p>اگر ستاره ببوسد تو را ستانه در</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>وگر سپهر شود بنده تو را بنده</p></div>
<div class="m2"><p>و گر زمانه بود چاکر تو را چاکر</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>تو نیک محضی و در جز تو نیک باشد و بد</p></div>
<div class="m2"><p>تو خیر صرفی و در جز تو خیر باشد و شر</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>اگر مکارم اخلاق تو سخن گوید</p></div>
<div class="m2"><p>کمینه لفظی از او مشک باشد و عنبر</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>وگر بزرگی و قدر تو منقسم گردد</p></div>
<div class="m2"><p>کمینه قسمی از او جاه باشد و مفخر</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>ثنا کنیم تو را و تو بهتری ز ثنا</p></div>
<div class="m2"><p>هر آینه شرف سر فزونتر از افسر</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>ز حسن رسم تویک (شمه) است باد بهار</p></div>
<div class="m2"><p>ز عطر خلق تو یک نایب است باد سحر</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>مرا که هست زبانم بر آفرین تو وقف</p></div>
<div class="m2"><p>همی زبان مرا آفرین کند حنجر</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>اگر چه صدر تو را بندگان فراوانند</p></div>
<div class="m2"><p>به من بود همه ذکر تو زنده تا محشر</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>همیشه تا اثرست از سپهر و گردش او</p></div>
<div class="m2"><p>ز عمر و عز تو در دولت تو باد اثر</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>همیشه زیر و زبر باد کار دشمن تو</p></div>
<div class="m2"><p>چنانکه هست فلک زیر و همت تو زبر</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>همیشه تا به جهان گاه نفع و گه ضرر است</p></div>
<div class="m2"><p>نصیب تو همه نفع و نصیب خصم ضرر</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>همیشه تا به سوی برتری کشد آتش</p></div>
<div class="m2"><p>تو آتشی و عدو تو باد خاکستر</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>همیشه تا زمی از آسمان پذیرد فعل</p></div>
<div class="m2"><p>تو ‌آفتابی و صدر تو آسمان پیکر</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>به کام و نام و مراد تمام در گیتی</p></div>
<div class="m2"><p>هزار سال بزی زان پس از جهان بگذر</p></div></div>