---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>گر دل و دلبر مرا دایم به فرمان باشدی</p></div>
<div class="m2"><p>درد عشقم را از او صد گونه درمان باشدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بند صبر و درد عشق من نگشتی سست و سخت</p></div>
<div class="m2"><p>گرنه دلبر سخت جور و سست پیمان باشدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دلم در بند آن زلف پریشان نیستی</p></div>
<div class="m2"><p>حال من چون زلف دلبر کی پریشان باشدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از فلک سرگشته جور و جفا کی باشمی</p></div>
<div class="m2"><p>گر دل او از جفا کردن پشیمان باشدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد جور از دلبران امید انصافی بود</p></div>
<div class="m2"><p>کاشکی جور فلک چون جور ایشان باشدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جور گردون جان رباید، جور جانان دل برد</p></div>
<div class="m2"><p>جور گردون کاشکی چون جور جانان باشدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیستی از عشق جانان و لب دلبر دریغ</p></div>
<div class="m2"><p>گر مرا در سینه و تن صد دل و جان باشدی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بر در او دارمی از عشق دیدارش طواف</p></div>
<div class="m2"><p>گرنه از باران چشمم بیم طوفان باشدی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب آسمان رخسار او را ماندی</p></div>
<div class="m2"><p>گر چو روی او به روز و شب درفشان باشدی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر به روی حسن گیری واجبستی کافتاب</p></div>
<div class="m2"><p>بر سپهر از شرم آن رخسار پنهان باشدی</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قامتش را ماندی سرو سهی در راستی</p></div>
<div class="m2"><p>سرو راگر دیده و دل باغ و بستان باشدی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سرو اگر گفتی که من چون قد دلبر دل برم</p></div>
<div class="m2"><p>آنچه گفتی سر به سر بر سرو تاوان باشدی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سال و مه جولان نبودی عشق را گرد دلم</p></div>
<div class="m2"><p>گرنه زلفینش به گرد ماه جولان باشدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوس او اصل حیات جاودانی نیستی</p></div>
<div class="m2"><p>گر لب او را نه لطف آب حیوان باشدی</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ماه رویان روی او را ماه کردندی خطاب</p></div>
<div class="m2"><p>گرنه مه را جای بر گردون گردان باشدی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیستی خالی دو دستم یک زمان از زلف او</p></div>
<div class="m2"><p>گرنه جادو زلف او پر زرق و دستان باشدی</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر تن و جان و دل من ظاهرستی ذل عشق</p></div>
<div class="m2"><p>گرنه عز خدمت صدر خراسان باشدی</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>سید سادات شمس دین ابوجعفر که دین</p></div>
<div class="m2"><p>گرنه فر اوستی بی فر و سامان باشدی</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن خداوندی که (گر) گردون ستمگر نیستی</p></div>
<div class="m2"><p>قدر این و رفعت آن هر دو یکسان باشدی</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>مشتری را گر سعادت نیستی از طلعتش</p></div>
<div class="m2"><p>در مسیر مشتری تاثیر کیوان باشدی</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در محامد هست مانند محد، کاشکی</p></div>
<div class="m2"><p>طبع ما در مدح او چون مدح حسان باشدی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر کمال مهتری در صورت تنهاستی</p></div>
<div class="m2"><p>در میان کهتر و مهتر چه نقصان باشدی</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ور کسی بی عدل و بذل و فضل مهتر گرددی</p></div>
<div class="m2"><p>مهتری کردن به غایت سهل و آسان باشدی</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بی نبوت هر محمد چون محمد گرددی</p></div>
<div class="m2"><p>بی جلالت هر سلیمان چون سلیمان باشدی</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>بی هدایت هر خسی دانا و داهی آمدی</p></div>
<div class="m2"><p>بی ولایت هر کسی سالار و سلطان باشدی</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>نظم نغز و نثر نیکو را فضیلت نیستی</p></div>
<div class="m2"><p>گر نه کلک او سوار هر دو میدان باشدی</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نوبهار خرمستی چار فصل روزگار</p></div>
<div class="m2"><p>گر چو دست و بخشش تو ابر و باران باشدی</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>ای خداوندی که گر قدر تو دانستی فلک</p></div>
<div class="m2"><p>جرم کیوان مر تو را فراش ایوان باشدی</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>ور محل مدح و اوصاف تو دانندی نجوم</p></div>
<div class="m2"><p>مدحتت را از فلکها درج و دیوان باشدی</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اصل و فرع شرع و ایمان نیستی در روزگار</p></div>
<div class="m2"><p>گر نه جدت رهنمای شرع و ایمان باشدی</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نیست ممکن چون تو بودن آن که را فضل تو نیست</p></div>
<div class="m2"><p>نیستی انصاف اگر دانا چو نادان باشدی</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نامه فخر و شرف نام تو را عنوان شده ست</p></div>
<div class="m2"><p>کاشکی هر نامه را زین نام عنوان باشدی</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>گر به استحقاق قدرت مدحتی گفتی خرد</p></div>
<div class="m2"><p>سر به سر ابیات او آیات قرآن باشدی</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>عاجزستی نفس ناطق در بیان مدح تو</p></div>
<div class="m2"><p>گر نه او را قوت از الهام یزدان باشدی</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>معده آز و امل را سال و مه سیرستی</p></div>
<div class="m2"><p>گر همه بر خوان انعام تو مهمان باشدی</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>در زمانه جز به نام تو نگویندی مدیح</p></div>
<div class="m2"><p>گرنه حاجتهای مداحان فراوان باشدی</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>عاقلان در شهرهای بد نسازندی مقام</p></div>
<div class="m2"><p>گرنه مهر اقربا و حب اوطان باشدی</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هر زبانی بر زبان من ثناخوان نیستی</p></div>
<div class="m2"><p>گر زبان من نه بر صدرت ثناخوان باشدی</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>گر مرا مدح چو آبت مونس جان نامدی</p></div>
<div class="m2"><p>زآتش انده دلم پیوسته بریان باشدی</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>در زمین شرق اگر معمار عدلت نیستی</p></div>
<div class="m2"><p>صحن او چون خانه خصم تو ویران باشدی</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>گر زانسان بعد جدت چون تو موجود آمدی</p></div>
<div class="m2"><p>هر فضیلت کان ملک دارد در انسان باشدی</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>در دل اسلامیان ثابت نبودی مهر تو</p></div>
<div class="m2"><p>گرنه در مهرت نجات هر مسلمان باشدی</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>ساعتی از ذکر تو خالی نبودی هیچ دل</p></div>
<div class="m2"><p>گرنه دل را آفت وسواس شیطان باشدی</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>با جمال روضه رضوان شد از فر تو بلخ</p></div>
<div class="m2"><p>کاشکی هر روضه را فر تو رضوان باشدی</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>ذوف من در مدح تو از طبع خرما خوشترست</p></div>
<div class="m2"><p>خوش نیستی گر همه خرما به کرمان باشدی</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>کی شدی مجموع انواع فضایل وصف تو</p></div>
<div class="m2"><p>گر بر این دعوی نه از فضل تو برهان باشدی</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کی رسیدی در سخن طبع مرا دعوی نظم</p></div>
<div class="m2"><p>گرنه در تفصیل او تفصیل الوان باشدی</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>حاجت از گردون مرا اقبال و عمر و عز توست</p></div>
<div class="m2"><p>هر چه من خواهم به حاجت کاشکی آن باشدی</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>گر به فرمان منستی دور چرخ و حکم دهر</p></div>
<div class="m2"><p>دهر و چرخت جاودان در حکم و فرمان باشدی</p></div></div>
<div class="b" id="bn50"><div class="m1"><p>کمترین خدمتگر امر تو گردون گرددی</p></div>
<div class="m2"><p>کمترین فرمانبر حکم تو کیهان باشدی</p></div></div>
<div class="b" id="bn51"><div class="m1"><p>ماه شعبان رفت و می گویند اصحاب قدح</p></div>
<div class="m2"><p>کاشکی شوال در پهلوی شعبان باشدی</p></div></div>
<div class="b" id="bn52"><div class="m1"><p>فاسقان از فر روزه زهد سلمان یافتند</p></div>
<div class="m2"><p>هر مسلمان کاشکی با زهد سلمان باشدی</p></div></div>
<div class="b" id="bn53"><div class="m1"><p>تا اگر صحن چمن را آفت دی نیستی</p></div>
<div class="m2"><p>گل بر او پیوسته همچون برق خندان باشدی</p></div></div>
<div class="b" id="bn54"><div class="m1"><p>خنده گل بادت از شادی و بدخواهت زغم</p></div>
<div class="m2"><p>گر نمردی چشم او چون ابر گریان باشدی</p></div></div>