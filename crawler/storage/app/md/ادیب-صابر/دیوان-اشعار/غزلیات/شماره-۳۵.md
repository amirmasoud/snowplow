---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>دل به عشق روی دلبر شاد کن</p></div>
<div class="m2"><p>وز رخ و زلفش گل و شمشاد کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل بیش اندیش را بر طاق نه</p></div>
<div class="m2"><p>نفس شادی دوست را دل شاد کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه بنای بی غمی بر پای دار</p></div>
<div class="m2"><p>گه سرای خرمی آباد کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاکران عشق را اجرای بده</p></div>
<div class="m2"><p>بندگان حرص را آزاد کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جهان از ظلم ما انصاف خواه</p></div>
<div class="m2"><p>در فلک بیداد ما را داد کن</p></div></div>