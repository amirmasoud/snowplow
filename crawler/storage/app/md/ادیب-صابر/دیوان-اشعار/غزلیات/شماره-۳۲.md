---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>پیک دو زلف دلبری ای باد صبحدم</p></div>
<div class="m2"><p>زان با نسیم عنبری ای باد صبحدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر مشک تاب داده دلبر گذشته ای</p></div>
<div class="m2"><p>زان دلربای و دلبری ای باد صبحدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر حلقه معطر مشکین گذشته ای</p></div>
<div class="m2"><p>چون مشک از آن معطری ای باد صبحدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در تن لطافت تو مرا جان نو نهاد</p></div>
<div class="m2"><p>گویی که جان دیگری ای باد صبحدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرورده نسیم سر زلف دلبری</p></div>
<div class="m2"><p>زین روی روح پروری ای باد صبحدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش گشت با نسیم توام عمر و عاشقی</p></div>
<div class="m2"><p>کز عمر و عشق خوشتری ای باد صبحدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عهدست با منت که سلامم بری به دوست</p></div>
<div class="m2"><p>هان تا ز عهد نگذری ای باد صبحدم</p></div></div>