---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>بر سپهر نیکویی رویش چو مه خرمن زده است</p></div>
<div class="m2"><p>آتش عشق آن مه خرمن زده در من زده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام من در عشق او گشته است خرمن سوخته</p></div>
<div class="m2"><p>تا سر زلفش ز عنبر گرد مه خرمن زده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کوته است از دامن عقل و صبوری دست من</p></div>
<div class="m2"><p>تا مرا سودای آن مه دست در دامن زده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق شورانگیز او زد راه دین و دل مرا</p></div>
<div class="m2"><p>گرچه او را دوست خواندم زخم چون دشمن زده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش تیر عشق او از صبر جوشن ساختم</p></div>
<div class="m2"><p>روز صبرم تیره شد تا تیر بر جوشن زده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده ام روشن به رویش بود و اکنون باد سرد</p></div>
<div class="m2"><p>خاک نومیدی مرا در دیده روشن زده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرچه هر دم زان دل بی رحم او آهی زنم</p></div>
<div class="m2"><p>رحم ناید در دلش گویی دل از آهن زده است</p></div></div>