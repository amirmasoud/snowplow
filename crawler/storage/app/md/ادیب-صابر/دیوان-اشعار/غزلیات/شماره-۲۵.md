---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ای دو لب تو ز شهد خوشتر</p></div>
<div class="m2"><p>زندان توام ز مهد خوشتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدخو مشو و تبه مکن عهد</p></div>
<div class="m2"><p>خوی خوش و حسن عهد خوشتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر وصل تو را به جهد یابم</p></div>
<div class="m2"><p>کاری نبود ز جهد خوشتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور شهد خوش است گو همی باش</p></div>
<div class="m2"><p>بوس تو مرا ز شهد خوشتر</p></div></div>