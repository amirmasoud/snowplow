---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ساقی کمی قرین قدح کن شراب را</p></div>
<div class="m2"><p>مطرب یکی به زخمه ادب کن رباب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام از قیاس آب و می از جنس آتش است</p></div>
<div class="m2"><p>ساقی نثار مرکب آتش کن آب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بفکن مرا به باده ای و مست و خراب کن</p></div>
<div class="m2"><p>یکسو فکن حدیث جهان خراب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عهد شباب دارم و جام شراب هست</p></div>
<div class="m2"><p>عهد شباب زیبد جام شراب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اندیشه چون سوال بود باده چون جواب</p></div>
<div class="m2"><p>پیش از سوال ساخته دار این جواب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بهر آن که عمر همی بگذرد چو خواب</p></div>
<div class="m2"><p>معزول کردم از عمل دیده، خواب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عمر خوش نبود مگر با شراب و عشق</p></div>
<div class="m2"><p>دل عشق را سپردم و تن مر شراب را</p></div></div>