---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>هر گه که گل لعل بخندد به چمن بر</p></div>
<div class="m2"><p>جز جام می لعل نشاید به دهن بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من جامه گر از جور غم عشق دریدم</p></div>
<div class="m2"><p>گل جامه ز جور که دریده است به تن بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد کند هر که به بیداد در افتد</p></div>
<div class="m2"><p>فریاد ز رعد آمد و بیداد به من بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماند به سرشک من و رخساره معشوق</p></div>
<div class="m2"><p>هر قطره که شبگیر درافتد به سمن بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لاله همه دشت عقیق یمنی گشت</p></div>
<div class="m2"><p>تاراج کی آمد ز خراسان به یمن بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وز ژاله زمین معدن در عدنی شد</p></div>
<div class="m2"><p>تا باد گذر کرد به دریای عدن بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بس که همی مشک فشانند درختان</p></div>
<div class="m2"><p>افسوس کند شاخ درختان به ختن بر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدر همه سادات علی تاج معالی</p></div>
<div class="m2"><p>در مدحت او فتنه معانی به سخن بر</p></div></div>