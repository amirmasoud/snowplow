---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>شب شنیدستی ز روز آویخته</p></div>
<div class="m2"><p>ماه دیدستی ز مشک انگیخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله پیش روی نرگس صف زده</p></div>
<div class="m2"><p>گرد مرجان گرد عنبر بیخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این همه بر روی زیبا روی اوست</p></div>
<div class="m2"><p>عاشقان را رنگ عشق آمیخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای زماه آویخته عنبر مراست</p></div>
<div class="m2"><p>دل بدان آویخته آویخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش جنگ و جفا بر من مریز</p></div>
<div class="m2"><p>تا نگردد آبرویم ریخته</p></div></div>