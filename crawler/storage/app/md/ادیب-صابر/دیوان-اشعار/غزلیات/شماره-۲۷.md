---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>آورد به ما بلبل عاشق خبر گل</p></div>
<div class="m2"><p>جز بلبل گل دوست که داند خطر گل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند که در گل همه خوبی و خوشی هست</p></div>
<div class="m2"><p>بی مل نه همانا که خوش آید خبر گل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان قطره شبیگر که بر شاخ گل افتاد</p></div>
<div class="m2"><p>چون تاج شهان گشت ز در تاج سر گل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بس که صبا بر سر گلزار گذر کرد</p></div>
<div class="m2"><p>پر مشک و عقیق است همه رهگذر گل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زیر و زبر گل همه لعل است و زبرجد</p></div>
<div class="m2"><p>دل زیر و زبر گشت ز زیر و زبر گل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل یک اثر است از قدح مل به لطافت</p></div>
<div class="m2"><p>زیبا نبود بی اثر مل اثر گل</p></div></div>