---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>ای دل غبار غم ببرد باد صبحدم</p></div>
<div class="m2"><p>بر زلف دوست گر گذرد باد صبحدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی باد صبحدم نزنم دم که هر شبی</p></div>
<div class="m2"><p>پیغام من به دوست برد باد صبحدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عطر و بخور بوی به زلفش سپرده اند</p></div>
<div class="m2"><p>بر زلف او از آن سپرد باد صبحدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زلف او شده است معطر چو زلف او</p></div>
<div class="m2"><p>از بس گره که می شمرد باد صبحدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان زلف گشت مونس دلهای عاشقان</p></div>
<div class="m2"><p>آری صلاح خود نگرد باد صبحدم</p></div></div>