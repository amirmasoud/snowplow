---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>عید خوبان را چو روی خویشتن آراسته است</p></div>
<div class="m2"><p>راست پنداری ز رویش عید عیدی خواسته است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جمال عید، عالم را بیاراید رواست</p></div>
<div class="m2"><p>عید را باری جمال روی او آراسته است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک راه از بوی زلفش پر نسیم عنبر است</p></div>
<div class="m2"><p>چشم خلق از نور رویش بر مه ناکاسته است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فتنه ای از حسن او در تعبیه ره یافته است</p></div>
<div class="m2"><p>نوحه ای از عشق او از عیدگه برخاسته است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرو و باغ و باغبان از قامت او طیره اند</p></div>
<div class="m2"><p>گویی او را باغبان مجد دین پیراسته است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سید مشرق که از بخشیده و انعام اوست</p></div>
<div class="m2"><p>هر چه اندر مشرق و مغرب نعیم و خواسته است</p></div></div>