---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>همه مقصود ما شد راست امروز</p></div>
<div class="m2"><p>که آن مقصود دل با ماست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آراید بهار نو جهان را</p></div>
<div class="m2"><p>جهان را روی او آراست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم دی وصل او حاجت همی خواست</p></div>
<div class="m2"><p>رواگشت آنچه دی می خواست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو فر روی او امروز اینجاست</p></div>
<div class="m2"><p>جمال نو بهار اینجاست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زپیش او چو او بنشست با ما</p></div>
<div class="m2"><p>غم نادیدنش برخاست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر آن شادی که از عالم نهان بود</p></div>
<div class="m2"><p>از آن بر روز ما پیداست امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پری می گفت از او نیکوترم من</p></div>
<div class="m2"><p>پدید آمد دروغ از راست امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر اقبال مرا فرداست وعده</p></div>
<div class="m2"><p>مرا با وصل او فرداست امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چو ما درسایه اقبال شاهیم</p></div>
<div class="m2"><p>همه اقبالها ما راست امروز</p></div></div>