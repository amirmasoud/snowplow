---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>سپهر نیکویی را مهر و ماهی</p></div>
<div class="m2"><p>جهان بدخوبی را سال و ماهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنین در نیکویی تا کی فزایی</p></div>
<div class="m2"><p>چرا از بدخویی لختی نکاهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه بی وصل تو روزم را سپیدی است</p></div>
<div class="m2"><p>نه بی هجرت گلیمم را سیاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو لب داری که بردند از حلاوت</p></div>
<div class="m2"><p>به یک بوسه ز حال من تباهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو را جویم که سرو با قبایی</p></div>
<div class="m2"><p>تو را خواهم که ماه با کلاهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو خواهان توام دیگر چه جویی</p></div>
<div class="m2"><p>چو جویان توام دیگر چه خواهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همی نام ملاحت بر تو زیبد</p></div>
<div class="m2"><p>چو بر خوارزمشه خوارزمشاهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>علاءالدین شه فرخنده اتسز</p></div>
<div class="m2"><p>که نام اوست از مه تا به ماهی</p></div></div>