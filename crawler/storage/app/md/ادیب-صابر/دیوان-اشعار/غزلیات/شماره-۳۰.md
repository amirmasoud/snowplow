---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>زان دو لب چون عقیق یارم</p></div>
<div class="m2"><p>از دیده همی عقیق بارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارست مرا عقیق باری</p></div>
<div class="m2"><p>تا عشق عقیق اوست کارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردست سرشک من عقیقین</p></div>
<div class="m2"><p>عشق لب چون عقیق یارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا عشق عقیق او گزیدم</p></div>
<div class="m2"><p>چون کار عقیق شد کنارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چند ز دیده با عقیقم</p></div>
<div class="m2"><p>همتای عقیق او ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر من به یمن عقیق جویم</p></div>
<div class="m2"><p>همچون لب او به کف نیارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا رغبت دل به عشق باشد</p></div>
<div class="m2"><p>در عشق عقیق آن نگارم</p></div></div>