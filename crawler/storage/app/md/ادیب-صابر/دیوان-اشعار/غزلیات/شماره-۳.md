---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>ساقی بده آن شراب گلگون را</p></div>
<div class="m2"><p>کز گونه خجل کند طبر خون را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که رخ تو رنگ گل گیرد</p></div>
<div class="m2"><p>از کف منه آن شراب گلگون را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناخوش نتوان گذاشت بی باده</p></div>
<div class="m2"><p>وقت خوش و ساعت همایون را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن باده عقیق ناب را ماند</p></div>
<div class="m2"><p>چونانکه پیاله، در مکنون را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک قطره از او فدای هامون کن</p></div>
<div class="m2"><p>تا لاله ستان کنیم هامون را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک جرعه از او بریز در جیحون</p></div>
<div class="m2"><p>تا گونه گل دهیم جیحون را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>افسون غمند باده و مستی</p></div>
<div class="m2"><p>بر لشکر غم گمار، افسون را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کاین صرف کند صروف گیتی را</p></div>
<div class="m2"><p>وآن دفع کند بلای گردون را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده سبب است عیش مردم را</p></div>
<div class="m2"><p>لیلی غرض است عشق مجنون را</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قانون قرار عشرت آمد می</p></div>
<div class="m2"><p>ضایع مکن این قرار و قانون را</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر طالب مال و گنج افزونی</p></div>
<div class="m2"><p>آراسته باش رنج افزون را</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی مال چه بد رسید موسی را</p></div>
<div class="m2"><p>وز گنج چه نفع بود قارون را</p></div></div>