---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>با من دلت آشنا نمی گردد</p></div>
<div class="m2"><p>وز تو دل من جدا نمی گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند وفای من رها کردی</p></div>
<div class="m2"><p>از دست تو دل رها نمی گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بهر سه بوسه زان دو لب هرگز</p></div>
<div class="m2"><p>یک حاجت من روا نمی گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی که دو چشم من نمی گرید</p></div>
<div class="m2"><p>درشهر یک آسیا نمی گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر عشق تو پادشاست دل لیکن</p></div>
<div class="m2"><p>بر وصل تو پادشا نمی گردد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر تیر که غمزه تو اندازد</p></div>
<div class="m2"><p>بر دل زند و خطا نمی گردد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا تو به مراد ما نمی باشی</p></div>
<div class="m2"><p>گردون به مراد ما نمی گردد</p></div></div>