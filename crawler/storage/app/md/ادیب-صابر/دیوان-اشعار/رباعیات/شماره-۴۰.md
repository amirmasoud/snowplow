---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>افتادن دندان تو ای بدر منیر</p></div>
<div class="m2"><p>داده ست دو گلبرگ تو را رنگ زریر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندین چه کشی زبهر دندان تشویر</p></div>
<div class="m2"><p>از یک صدف ای نگار یک در کم گیر</p></div></div>