---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>روی تو ز خورشید همی دارد ننگ</p></div>
<div class="m2"><p>خورشید نخوانمت که گردی دلتنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید ز رخسار تو می گیرد نور</p></div>
<div class="m2"><p>یاقوت ز خورشید بدان گیرد رنگ</p></div></div>