---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>جانا لب و غمزه تو نوش و نیش است</p></div>
<div class="m2"><p>زان روح به راحت است و زین دل ریش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان غمزه و لب که دلبریشان کیش است</p></div>
<div class="m2"><p>هر چند که رنج هست راحت بیش است</p></div></div>