---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هجر تو وباست هر کجا بر گذرد</p></div>
<div class="m2"><p>زو پرده عمر و زندگانی بدرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمم به لبت همیشه زان می نگرد</p></div>
<div class="m2"><p>گویند که یاقوت وبا را ببرد</p></div></div>