---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>در تو نگرم که هر که در تو نگرد</p></div>
<div class="m2"><p>گر دل نبرد رنگ غم از دل ببرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می با تو خورم که هر که می با تو خورد</p></div>
<div class="m2"><p>از راه ملامت به سلامت گذرد</p></div></div>