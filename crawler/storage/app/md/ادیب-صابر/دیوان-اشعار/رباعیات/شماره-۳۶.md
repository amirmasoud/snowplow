---
title: >-
    شمارهٔ ۳۶
---
# شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>آن بت که همی وعده مجازی دارد</p></div>
<div class="m2"><p>بردن دل و جان من به بازی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب های مرا دراز کرده ست زعشق</p></div>
<div class="m2"><p>آری شب عاشقان درازی دارد</p></div></div>