---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>چون گردش آسمان نکوخواه من است</p></div>
<div class="m2"><p>دیدم رخ آن که بر زمین ماه من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وصلش که به راه عشق همراه من است</p></div>
<div class="m2"><p>تاثیر دعاهای سحرگاه من است</p></div></div>