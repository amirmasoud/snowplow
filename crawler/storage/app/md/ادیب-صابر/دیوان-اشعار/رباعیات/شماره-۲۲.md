---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>چون عشق تو عقل را گریبان بگرفت</p></div>
<div class="m2"><p>جادو ز تو انگشت به دندان بگرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای تو چون ملک دل و جان بگرفت</p></div>
<div class="m2"><p>چندانکه دلش خواست دو چندان بگرفت</p></div></div>