---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>ای بی تو نخفته من شبی خواب دوست</p></div>
<div class="m2"><p>بی خوابی چشم من ز خوش خوابی توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چشم من از عشق تو بی خوابی رست</p></div>
<div class="m2"><p>تا آب دو دیده خوابم از دیده بشست</p></div></div>