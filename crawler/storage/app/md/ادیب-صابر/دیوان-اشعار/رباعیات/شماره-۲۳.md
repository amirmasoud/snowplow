---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>چون نیست در این زمانه سودی ز خرد</p></div>
<div class="m2"><p>جز بی خرد از زمانه بر می نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای دوست بیار آنچه خرد را ببرد</p></div>
<div class="m2"><p>باشد که زمانه سوی ما به نگرد</p></div></div>