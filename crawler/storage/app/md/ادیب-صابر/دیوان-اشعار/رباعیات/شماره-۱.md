---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>با باغ گل و باده گلگون ما را</p></div>
<div class="m2"><p>چون دید حسد نمود گردون ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرماش ز باغ کرد بیرون ما را</p></div>
<div class="m2"><p>سردابه گزید باید اکنون ما را</p></div></div>