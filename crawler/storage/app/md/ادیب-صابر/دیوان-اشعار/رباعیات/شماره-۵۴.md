---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>ای تعبیه حسن تو جوق از پس جوق</p></div>
<div class="m2"><p>وز شهد و شکر برده لبت لذت و شوق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دایم به سر کوی تو گردم پر شوق</p></div>
<div class="m2"><p>در گردن خود چو قمری از زلف تو طوق</p></div></div>