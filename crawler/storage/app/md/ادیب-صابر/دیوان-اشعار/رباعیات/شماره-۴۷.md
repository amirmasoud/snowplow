---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>از دیدن خلق، دیده بی دوست بدوز</p></div>
<div class="m2"><p>وز صحبت بی دلی ز دل کینه بتوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماننده ابر و شمع، از این پس شب و روز</p></div>
<div class="m2"><p>بی دیده همی گری و بی دل می سوز</p></div></div>