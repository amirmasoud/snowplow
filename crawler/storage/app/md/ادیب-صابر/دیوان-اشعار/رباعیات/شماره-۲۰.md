---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>چون با دل تو نیست وفا در یک پوست</p></div>
<div class="m2"><p>در چشم تو یک رنگ بود دشمن و دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بس بس که شکایت تو ناکرده به است</p></div>
<div class="m2"><p>رو رو که حکایت تو ناگفته نکوست</p></div></div>