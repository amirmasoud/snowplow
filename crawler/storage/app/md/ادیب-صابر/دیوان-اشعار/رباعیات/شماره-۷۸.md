---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>از بس که کنی ده دلی و ده رایی</p></div>
<div class="m2"><p>ده بند ببندی و یکی نگشایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر دل یکتای من ای بینایی</p></div>
<div class="m2"><p>صد گونه غم است از آن دل ده تایی</p></div></div>