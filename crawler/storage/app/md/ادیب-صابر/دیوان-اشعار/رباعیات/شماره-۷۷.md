---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>هستم ز جفای دوست در هر بابی</p></div>
<div class="m2"><p>آسیمه سری، تر مژه ای بی خوابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نیستمی ز عشق در هر بابی</p></div>
<div class="m2"><p>دریا کنمی ز دیده هر محرابی</p></div></div>