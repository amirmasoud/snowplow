---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>دل تنگم از آنک هر چه خواهم آن نیست</p></div>
<div class="m2"><p>دریای دل تنگ مرا پایان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون شدن از تنگ دلی آسان نیست</p></div>
<div class="m2"><p>درمانش ز صبر است و مرا درمان نیست</p></div></div>