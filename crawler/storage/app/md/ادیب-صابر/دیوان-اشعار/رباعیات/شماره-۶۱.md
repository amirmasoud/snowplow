---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>دارم سر آن کز خط تو سر نکشم</p></div>
<div class="m2"><p>چه کنم که جفای چون تو دلبر نکشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تو به جفا دست همی درنکشم</p></div>
<div class="m2"><p>وز چاه زنخدان تو دل برنکشم</p></div></div>