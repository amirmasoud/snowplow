---
title: >-
    شمارهٔ ۴۹
---
# شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>رای طربم نیست ز رای سفرش</p></div>
<div class="m2"><p>خوابم سفری شد از بلای سفرش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب به که نالم از عنای سفرش</p></div>
<div class="m2"><p>یارب چه جفا کنم به جای سفرش</p></div></div>