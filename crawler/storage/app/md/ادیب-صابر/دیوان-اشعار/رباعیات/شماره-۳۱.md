---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>آن بت که به رخسار بهار آراید</p></div>
<div class="m2"><p>چون رعد همی نالم و رحمش ناید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون برق به خنده تا لبی بگشاید</p></div>
<div class="m2"><p>چون ابر مرا گریستن فرماید</p></div></div>