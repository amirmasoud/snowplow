---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>روی تو به چشم آتش بی دود نمود</p></div>
<div class="m2"><p>دل گفت که بی دود کدام آتش بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خط تو برون دمید چون ز آتش دود</p></div>
<div class="m2"><p>دودی که از او آتش عشقم بفزود</p></div></div>