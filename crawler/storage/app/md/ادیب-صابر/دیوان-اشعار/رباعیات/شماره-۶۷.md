---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>تا آتش عشق تو به دل ره دادم</p></div>
<div class="m2"><p>چون ابر ز آب دیده با فریادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دست فراق تا اسیر افتادم</p></div>
<div class="m2"><p>بیچاره تر از خاک به دست بادم</p></div></div>