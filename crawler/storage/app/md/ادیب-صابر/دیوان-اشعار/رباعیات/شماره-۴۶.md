---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>مرغی که چو ماهیش به آب است نیاز</p></div>
<div class="m2"><p>از نسبت بط نی چو بط سینه فراز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آب همی رود همه روز دراز</p></div>
<div class="m2"><p>چون توده خاک دید بر گردد باز</p></div></div>