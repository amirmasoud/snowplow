---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>هر چند بود مردم دانا درویش</p></div>
<div class="m2"><p>آخر بود از توانگر نادان بیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن را نبود جاه چو مالش شد بیش</p></div>
<div class="m2"><p>وین شاد بود همیشه از دانش خویش</p></div></div>