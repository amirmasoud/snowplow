---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>پر نور شود دیده چو در می نگرد</p></div>
<div class="m2"><p>تا می نخوری دل ز طرب برنخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویی که می از دل ببرد هوش و خرد</p></div>
<div class="m2"><p>برخیز و می آر چون نیابد چه برد</p></div></div>