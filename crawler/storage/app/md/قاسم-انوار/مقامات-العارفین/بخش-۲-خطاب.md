---
title: >-
    بخش ۲ - خطاب
---
# بخش ۲ - خطاب

<div class="b" id="bn1"><div class="m1"><p>بشنو،ای طالب ره توفیق</p></div>
<div class="m2"><p>در طریق خدا علی التحقیق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد مقامست پیش اهل دید</p></div>
<div class="m2"><p>اولش یقظه،آخرش توحید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه زین بیشتر توان گفتن</p></div>
<div class="m2"><p>در معنی به صد بیان سفتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیک این صد بود اصول همه</p></div>
<div class="m2"><p>سالکان را بود وصول همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست این صد مقام بر ده قسم</p></div>
<div class="m2"><p>هریک از هم تمیز کرده باسم</p></div></div>