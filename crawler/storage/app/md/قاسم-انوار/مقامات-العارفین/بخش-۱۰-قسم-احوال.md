---
title: >-
    بخش ۱۰ - قسم احوال
---
# بخش ۱۰ - قسم احوال

<div class="b" id="bn1"><div class="m1"><p>قسم احوال بعد ازین باشد</p></div>
<div class="m2"><p>هر که دانست مرد دین باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول آن محبتست، بدان</p></div>
<div class="m2"><p>پس ازان غیرتست و شوق ز جان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس قلق،پس عطش بود،ای دل</p></div>
<div class="m2"><p>بعداز آن وجد دید شد منزل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هیمانست و برق و ذوق تمام</p></div>
<div class="m2"><p>ختم شد این درود کرد و سلام</p></div></div>