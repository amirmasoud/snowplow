---
title: >-
    بخش ۸ - قسم وصول
---
# بخش ۸ - قسم وصول

<div class="b" id="bn1"><div class="m1"><p>بعد ازان شد مغازلات وصول</p></div>
<div class="m2"><p>که بود جملگی نشان قبول</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصد و عزم و ارادت نیکوست</p></div>
<div class="m2"><p>پس ادب، پس یقین و انس بدوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذکر و فکر و غنا،مقام مراد</p></div>
<div class="m2"><p>شد تمام،این همه صفات تو باد</p></div></div>