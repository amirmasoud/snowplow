---
title: >-
    بخش ۶ - قسم معاملات
---
# بخش ۶ - قسم معاملات

<div class="b" id="bn1"><div class="m1"><p>پس کنم در معاملات شروع</p></div>
<div class="m2"><p>باتو گویم همه اصول و فروع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول آن رعایتست بدان</p></div>
<div class="m2"><p>پس ترا در مراقبه است مکان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد ازان حسرت آمد و اخلاص</p></div>
<div class="m2"><p>که طریق سلامتست و خلاص</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس بتهذیب و استقامت رو</p></div>
<div class="m2"><p>بعد ازان بر در توکل شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بعد ازاین هست منزل تفویض</p></div>
<div class="m2"><p>پس ثقة باشد،ای رفیق مفیض</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پس ازین سر بر آری از تسلیم</p></div>
<div class="m2"><p>تا بیاسایی از عذاب الیم</p></div></div>