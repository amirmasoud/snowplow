---
title: >-
    بخش ۱۱ - قسم ولایات
---
# بخش ۱۱ - قسم ولایات

<div class="b" id="bn1"><div class="m1"><p>بعد ازین قسمت ولایاتست</p></div>
<div class="m2"><p>داند آن کس که در مقاماتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لحظه و وقت،پس صفا و سرور</p></div>
<div class="m2"><p>سر و نفسست و غربت از خود دور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرق و هیبت،تمکن و آنگاه</p></div>
<div class="m2"><p>بعد ازین بر حقایق آمد راه</p></div></div>