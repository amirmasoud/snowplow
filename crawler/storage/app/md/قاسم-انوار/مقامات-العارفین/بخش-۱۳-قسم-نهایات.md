---
title: >-
    بخش ۱۳ - قسم نهایات
---
# بخش ۱۳ - قسم نهایات

<div class="b" id="bn1"><div class="m1"><p>معرفت،پس بقای جان باشد</p></div>
<div class="m2"><p>پس فنا ملک جاودان باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس بتحقیق میشود مشهور</p></div>
<div class="m2"><p>پس بتلبیس می شود مستور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پس وجودست و بعد ازان تجرید</p></div>
<div class="m2"><p>هست تفرید و جمع،پس توحید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاسمی یار آنکه دین دارد</p></div>
<div class="m2"><p>هرکه دین داشت محض این دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صلوات خدای بر احمد</p></div>
<div class="m2"><p>بر روان صحابه امجد</p></div></div>