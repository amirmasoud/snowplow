---
title: >-
    بخش ۳ - عدد مقامات بطریق اجمال
---
# بخش ۳ - عدد مقامات بطریق اجمال

<div class="b" id="bn1"><div class="m1"><p>از بدایات گیر تا ابواب</p></div>
<div class="m2"><p>بعد ازان تا معاملات و صواب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بعد اخلاق دان تو قسم اصول</p></div>
<div class="m2"><p>بعد ازان ادویه،مباش ملول</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قسم احوال پس ولایاتست</p></div>
<div class="m2"><p>تا نگویی که شطح و طاماتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس حقایق بود،یقین می دان</p></div>
<div class="m2"><p>پس نهایات،ای عزیز زمان</p></div></div>