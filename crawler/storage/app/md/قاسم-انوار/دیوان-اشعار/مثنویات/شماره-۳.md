---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>گاه با خود نشسته ام ز بدی</p></div>
<div class="m2"><p>گاه برخاسته ز فکر خودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من درویش زار بی دل و یار</p></div>
<div class="m2"><p>مدتی در هوای آن دلدار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوده ام گه ز خاست،گه ز نشست</p></div>
<div class="m2"><p>ماه در سی و ماهی اندر شست</p></div></div>