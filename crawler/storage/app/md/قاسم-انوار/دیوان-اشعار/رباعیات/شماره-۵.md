---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>عاشق که سمندر نبود خر کوفست</p></div>
<div class="m2"><p>صوفی که قلندر نبود موقوفست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندی که نه پارسا بود نامردست</p></div>
<div class="m2"><p>زاهد که نه شاهدیش باشد بوفست</p></div></div>