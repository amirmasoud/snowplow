---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>ای ساعد لطف شاه از روح تو باز</p></div>
<div class="m2"><p>محبوب خدا،طایر عالی پرواز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با پیل چه نسبتت؟که شاهان جهان</p></div>
<div class="m2"><p>بر خاک درت پیاده بر نطع نیاز</p></div></div>