---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای دل،غم عشق ذو فنونت سازد</p></div>
<div class="m2"><p>وز هرچه گمان بری فزونت سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در واقعه هجر زبونت سازد</p></div>
<div class="m2"><p>آخرغم آن نگار چونت سازد؟</p></div></div>