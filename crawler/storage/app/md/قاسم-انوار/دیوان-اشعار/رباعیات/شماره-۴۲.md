---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>گر شاه زمانه ای وگر دستوری</p></div>
<div class="m2"><p>گر باز جهان شکار،اگر عصفوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر مست طریقتی و گر مستوری</p></div>
<div class="m2"><p>تا راه بحق نبرده ای مغروری</p></div></div>