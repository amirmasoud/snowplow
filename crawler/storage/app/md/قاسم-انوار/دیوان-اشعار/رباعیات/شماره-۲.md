---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>گر جانم گویم، عاشق پیشین شماست</p></div>
<div class="m2"><p>ور دل گویم، بنده مسکین شماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلق دو جهان طفیل تمکین شماست</p></div>
<div class="m2"><p>گر کافر و مؤمنست، بر دین شماست</p></div></div>