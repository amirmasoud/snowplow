---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>معشوقه بهر صفت که آید بظهور</p></div>
<div class="m2"><p>از ظلمت محض،یا خود از خالص نور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق بهمان صفت موصوف گردد</p></div>
<div class="m2"><p>بر دین ملوکست رعیت مامور</p></div></div>