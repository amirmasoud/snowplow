---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>هر چندکه در زمانه یک محرم نیست</p></div>
<div class="m2"><p>بنیاد اساس دوستی محکم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مادر همه حال باغمش دلشادیم</p></div>
<div class="m2"><p>چون غم بسلامتست،دیگر غم نیست</p></div></div>