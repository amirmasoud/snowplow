---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>ای رفته بپای خود بجایی که مپرس</p></div>
<div class="m2"><p>وز دست خودی تو در بلایی که مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مس وجود خود دمی بیرون آی</p></div>
<div class="m2"><p>تا راه بری بکیمیایی که مپرس</p></div></div>