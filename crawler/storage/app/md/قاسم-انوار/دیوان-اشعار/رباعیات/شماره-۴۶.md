---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>ای سرو ریاض آشنایی که تویی</p></div>
<div class="m2"><p>وی شمع طراز روشنایی که تویی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که غلط نیفتدت رندی کن</p></div>
<div class="m2"><p>وامانده کوی پارسایی که تویی</p></div></div>