---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>بر دیده چون سحاب من رحمت کن</p></div>
<div class="m2"><p>بر سیل سرشک ناب من رحمت کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر جان و دل خراب من رحمت کن</p></div>
<div class="m2"><p>بر زاری و اضطراب من رحمت کن</p></div></div>