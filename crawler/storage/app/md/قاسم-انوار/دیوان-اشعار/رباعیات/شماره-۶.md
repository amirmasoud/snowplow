---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>گفتم:بهزار دل ترا دارم دوست</p></div>
<div class="m2"><p>در خنده شد از ناز که:این شیوه نکوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم:صنما،راه وصال از که بکیست؟</p></div>
<div class="m2"><p>فرمود که: ای دوست، هم از دوست بدوست</p></div></div>