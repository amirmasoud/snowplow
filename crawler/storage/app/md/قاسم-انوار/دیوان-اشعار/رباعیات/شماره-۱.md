---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>من بنده روی توام، ای باده پرست</p></div>
<div class="m2"><p>وز نرگس مخمور تو جانم شده مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون پرتو دیدار تو ظاهر گردد</p></div>
<div class="m2"><p>مارا بسر کوی تو یک هی هی هست</p></div></div>