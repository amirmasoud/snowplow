---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>مستدعیم از حضرت سلطان قدم</p></div>
<div class="m2"><p>یک جرعه شراب را که سرتابقدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستم کند آن چنان که آسوده شوم</p></div>
<div class="m2"><p>از قاعده وجود و از رسم عدم</p></div></div>