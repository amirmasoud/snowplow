---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ای دلبر دلدار، طلب گار توایم</p></div>
<div class="m2"><p>ای منبع انوار، طلب گار توایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای سالک اطوار،طلب گار توایم</p></div>
<div class="m2"><p>ای واقف اسرار، طلب گار توایم</p></div></div>