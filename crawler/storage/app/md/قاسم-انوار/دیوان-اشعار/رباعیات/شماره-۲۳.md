---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>ای جان جهان،جان جهان،دلبرگیل</p></div>
<div class="m2"><p>می دل همه روج داروتی دیمی میل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیلاب سرشک قاسم از ابر غمت</p></div>
<div class="m2"><p>اندی بشو،که بردگیلان را سیل</p></div></div>