---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>در دل هوس روی نگاری دارم</p></div>
<div class="m2"><p>در سر ز می عشق خماری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا زلف و رخ ترا بدیدم شب وروز</p></div>
<div class="m2"><p>آشفته دلی و روزگاری دارم</p></div></div>