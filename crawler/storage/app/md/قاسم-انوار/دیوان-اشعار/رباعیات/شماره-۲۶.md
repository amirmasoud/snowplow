---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>من بنده شیوه های شیرین توام</p></div>
<div class="m2"><p>آشفته طره های مشکین توام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتی که :بگو:تاچه کسی در ره ما؟</p></div>
<div class="m2"><p>مسکین تو،مسکین تو،مسکین توام</p></div></div>