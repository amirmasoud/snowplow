---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>مقصود ما ز ملک جهان وصل یار ماست</p></div>
<div class="m2"><p>این کار اگر برآید، پس کار کار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما در میان نار محبت بسوختیم</p></div>
<div class="m2"><p>بعد از فنا مراد دل اندر کنار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر بلبلی بگلشن ما راه کی برد؟</p></div>
<div class="m2"><p>آن مرغ زار ماست که از مرغزار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شادی اگر بما نرسد یار حاکمست</p></div>
<div class="m2"><p>با غم بسر بریم، که او یار غار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ، برو، ز حرفک و چربک بدار دست</p></div>
<div class="m2"><p>راه تو مظلم آمد و نور تو نار ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد، ز شرم شیوه ما آب گشته ای</p></div>
<div class="m2"><p>باری بدان که شرم تو هم شرمسار ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پر شود یمین و یسار جهان ز غم</p></div>
<div class="m2"><p>ما را چه غم ز غم؟ که غمت غمگسار ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>منصور گفت که بر سر دار از صفای عشق :</p></div>
<div class="m2"><p>این دار دار نیست که دارالعیار ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باغ ارم، که مثل وی اندر جهان نبود</p></div>
<div class="m2"><p>بی پرتو جمال تو دار البوار ماست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم که: کیست احمد؟ گفتا که: شاه جان</p></div>
<div class="m2"><p>گفتم: بلیس؟ گفت که: او پرده دار ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گفتم که عقل؟ گفت که: قاضی کن فکان</p></div>
<div class="m2"><p>گفتم که عشق؟ گفت که: میر شکار ماست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گفتم که: کیست قاسمی اندر طریق؟ گفت:</p></div>
<div class="m2"><p>بی اختیار ماست، ولی اختیار ماست</p></div></div>