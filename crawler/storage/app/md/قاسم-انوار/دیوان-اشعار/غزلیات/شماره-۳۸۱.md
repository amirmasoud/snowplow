---
title: >-
    شمارهٔ ۳۸۱
---
# شمارهٔ ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>هر که هشیار درین دیر مغانش مگذار</p></div>
<div class="m2"><p>سر تسلیم ندارد، سرش از تن بردار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من همان لحظه بدریای یقین تو رسم</p></div>
<div class="m2"><p>که دلم ابر کرم گردد و چشمم در بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی، از روز ازل بنده مسکین توایم</p></div>
<div class="m2"><p>دفع مخموری ما جام رها کن، خم آر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کسی را ز شرابات خدا بخش رسید</p></div>
<div class="m2"><p>زاهد آمد که: مرا بخش ولیکن خروار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که منصور شد، او جام «انالحق » برداشت</p></div>
<div class="m2"><p>چون تو منصور شدی جام «انالحق » بردار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر ز مستان حقی، در ره تحقیق و یقین</p></div>
<div class="m2"><p>باده می نوش ولی کاسه مستان مشمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، در دو جهان بر خور از آن یار نکو</p></div>
<div class="m2"><p>تا نهم نام تو در هر دو جهان برخوردار</p></div></div>