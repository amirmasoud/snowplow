---
title: >-
    شمارهٔ ۳۱۸
---
# شمارهٔ ۳۱۸

<div class="b" id="bn1"><div class="m1"><p>عشقش بخاک بردم و گفتم که: یا ودود</p></div>
<div class="m2"><p>«ارحم لنا» که غیر تو کس نیست در وجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاقت نداشت نور خرد پیش نار عشق</p></div>
<div class="m2"><p>خود را ز راه تجربه بسیار آزمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف تو جعد شد،همه سرسبز و تازه ایم</p></div>
<div class="m2"><p>خیری ز شب در آمد و در روز در فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر زانکه یار پرده عزت برافکند</p></div>
<div class="m2"><p>جان و روان بباز،چه فکر زیان و سود؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جانها همه گدایی و دریوزه می کنند</p></div>
<div class="m2"><p>زان جان سرفراز،که محوست در شهود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک ساغری ز خم بلا نوش کرده ایم</p></div>
<div class="m2"><p>سودای یار جبه و دستار مار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای جان نازنین، بهوای تو زنده ایم</p></div>
<div class="m2"><p>قاسم بشوق روی تو میخواند این سرود</p></div></div>