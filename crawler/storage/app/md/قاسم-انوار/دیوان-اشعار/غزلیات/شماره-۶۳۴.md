---
title: >-
    شمارهٔ ۶۳۴
---
# شمارهٔ ۶۳۴

<div class="b" id="bn1"><div class="m1"><p>زمانی یار شو، گر یار باشی</p></div>
<div class="m2"><p>اگر با ما نباشی با که باشی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم را از تو دوری نیست ممکن</p></div>
<div class="m2"><p>که جان را خواجه ای و خواجه تاشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو مردان با معاد خویش رفتند</p></div>
<div class="m2"><p>تو سر پوشیده در بند معاشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خبر از وحدت جانان نداری</p></div>
<div class="m2"><p>که هر دم خاطر نومی خراشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو جامت میدهد دلدار می نوش</p></div>
<div class="m2"><p>که کفرست اندرین حالت تحاشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو مردان سر خود پوشیده می دار</p></div>
<div class="m2"><p>مگو از قصه «لا حق و لا شی »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بهر حالت که هستی، قاسمی، شکر</p></div>
<div class="m2"><p>که تا در راه او مشرک نباشی</p></div></div>