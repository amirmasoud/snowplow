---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>ساقی بیار باده و بنواز عود را</p></div>
<div class="m2"><p>یک دم بلند کن نغمات سرود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی بتشنگان حیات ابد رسان</p></div>
<div class="m2"><p>هی بر زنید زاهد خشک حسود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیطان حسود و دشمن و رحمان امین جان</p></div>
<div class="m2"><p>از بهر آن حسود مرنجان ودود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنگست راکعی و کمانچه است ساجدی</p></div>
<div class="m2"><p>بهر که میکنند رکوع و سجود را؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر وحدت خدا همه ذرات شاهدند</p></div>
<div class="m2"><p>هجران نصیب منکر کور کبود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مصطفی گریز، که دریای رحمتست</p></div>
<div class="m2"><p>بگذار باد سبلت عاد و ثمود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر گه سرود عشق تو گویند عاشقان</p></div>
<div class="m2"><p>قاسم روان کند ز دو دیده دورود را</p></div></div>