---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>یحبهم و یحبونه چه اقرارست؟</p></div>
<div class="m2"><p>بزیر پرده مگر خویش را خریدارست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو عاشقند و دو معشوق در مکین و مکان</p></div>
<div class="m2"><p>ولی تصور اغیار محض پندارست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار جان گرامی بیک کرشمه خرند</p></div>
<div class="m2"><p>میان عاشق و معشوق این چه بازارست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزا جان و دل و دین برای یک غمزه</p></div>
<div class="m2"><p>بداد عاشق مسکین، که بس خریدارست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدام چون بسر تست شاه را سوگند</p></div>
<div class="m2"><p>چنین شریف سری را چه جای دستارست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خطاست این که: فلانی چنین روایت کرد</p></div>
<div class="m2"><p>بیا بدیده بینا، که وقت دیدارست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرید جمله ذرات کاینات شود</p></div>
<div class="m2"><p>دلی که جلوه خورشید را طلب کارست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بجان دوست، کزین راست تر حدیثی نیست</p></div>
<div class="m2"><p>که هرکه عشق نورزید نقش دیوارست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>برون ز حد و صفت قاسمی جمال تو دید</p></div>
<div class="m2"><p>جمال روی ترا جلوهای بسیارست</p></div></div>