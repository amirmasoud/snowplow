---
title: >-
    شمارهٔ ۳۰۰
---
# شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>چند در مسجد و در صومعه غارت کردند</p></div>
<div class="m2"><p>سبب این بود که می خانه عمارت کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده گران شد و ذرات جهان مست شدند</p></div>
<div class="m2"><p>من ندانم که بساقی چه اشارت کردند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد و صافی همه خوردن و بچرخ افتادند</p></div>
<div class="m2"><p>صورت حال به «احببت » عبارت کردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر وصل تو جستند و کسی باز نیافت</p></div>
<div class="m2"><p>گر چه عمری بجهان روبتجارت کردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از می صافی رخشنده انارتها شد</p></div>
<div class="m2"><p>کاسه ای چند درین بزم انارت کردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر گناهی که ز ماخسته دلان آمده بود</p></div>
<div class="m2"><p>جاودان از کرم یار کفارت کردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه گنه کردم،ای دل،چه خطا افتادست؟</p></div>
<div class="m2"><p>که از آن مستی و می رو بجهارت کردند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خرابات مغان زنده دلان چالاک</p></div>
<div class="m2"><p>هرکه هشیار ندیدند زیارت کردند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی عاشق بیچاره بسودای تو ماند</p></div>
<div class="m2"><p>عاقلان میل بزرگی و صدارت کردند</p></div></div>