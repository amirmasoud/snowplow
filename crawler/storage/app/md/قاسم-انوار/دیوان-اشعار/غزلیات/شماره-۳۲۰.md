---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>فتنه در خواب قیامت خفته بود</p></div>
<div class="m2"><p>چشم بیدار تو خوابش در ربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا چو گل از پرده بیرون آمدی</p></div>
<div class="m2"><p>در گلستان عام شد بانگ سرود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر سر بازار جان مست آمدی</p></div>
<div class="m2"><p>مست حیرت ماند جانها در شهود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب رحمت ریختی در جام ما</p></div>
<div class="m2"><p>تابهر جانب برآمد بانگ رود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتاب عالم آرا جلوه کرد</p></div>
<div class="m2"><p>منبسط شد در جهان ظل ودود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور و غوغا عام شد در کاینات</p></div>
<div class="m2"><p>تا نقاب از چهره معنی گشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خطایی رفت بازآ از کرم</p></div>
<div class="m2"><p>قاسمی باز آمدست از هرچه بود</p></div></div>