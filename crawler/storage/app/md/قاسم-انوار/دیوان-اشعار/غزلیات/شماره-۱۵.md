---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>طلوع پرتو حسنست در جهان، اما</p></div>
<div class="m2"><p>خلاف مذهب و دین چیست؟ معنی اسما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بجان تو که هزاران هزار فرسنگست</p></div>
<div class="m2"><p>ز شهر عالم صورت بملک «اوادنا»</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جهان پرست ازین آفتاب عالمتاب</p></div>
<div class="m2"><p>کجاست طلعت خورشید و چشم نابینا؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق مستی عشق تو کف زنان گویم:</p></div>
<div class="m2"><p>«تنن تنن، تنن تن، تلا تلا تللا»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خطا ز فعل خدا نیست، راه جهل مرو</p></div>
<div class="m2"><p>ز چین ابروی خود اوفتاده ای بخطا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>«یحبهم » ز خدا آمد، ای فقیه، مرنج</p></div>
<div class="m2"><p>خلاف مذهب حق نیست، عشق بازی ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیمن دولت وصل تو قاسمی وارست</p></div>
<div class="m2"><p>ز فکر سایه طوبی و جنت الماوا</p></div></div>