---
title: >-
    شمارهٔ ۵۸۵
---
# شمارهٔ ۵۸۵

<div class="b" id="bn1"><div class="m1"><p>ای کوس کبریای تو در لامکان زده</p></div>
<div class="m2"><p>وی آتش هوای تو در ملک جان زده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقت بغیرت آمده و قهرمان شده</p></div>
<div class="m2"><p>آتش میان خرمن صاحبدلان زده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حیران شد از لوامع اشراق آن جمال</p></div>
<div class="m2"><p>عقلی که در صفات تو لاف بیان زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رویت ز لمعه پیشرو کاروان شده</p></div>
<div class="m2"><p>چشمت بغمزه ای ره صد کاروان زده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک نعره زد ز شوق دلم، تیر غمزه خورد</p></div>
<div class="m2"><p>زان پس هزار نعره بامید آن زده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر روز درد و سوز دلم را زیاده کن</p></div>
<div class="m2"><p>تا در طریق عشق نباشم زیان زده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برخاسته ز فکر جهان جان قاسمی</p></div>
<div class="m2"><p>تا از شراب شوق تو رطل گران زده</p></div></div>