---
title: >-
    شمارهٔ ۵۹۵
---
# شمارهٔ ۵۹۵

<div class="b" id="bn1"><div class="m1"><p>می کشد آن حبیب فرزانه</p></div>
<div class="m2"><p>چشم را سرمه، زلف را شانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می رود در فضای ملک وجود</p></div>
<div class="m2"><p>«اینما کان » و «حیث ما کانه »</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست و طناز و سرفراز و ملیح</p></div>
<div class="m2"><p>هر کرا دید داد پیمانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه از جام اوست مستی جان</p></div>
<div class="m2"><p>چیست این نعرهای مستانه؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدان را صوامع و تسبیح</p></div>
<div class="m2"><p>عاشقان را شراب و می خانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بهوای تو دایم این دل مست</p></div>
<div class="m2"><p>گاه شمعست و گاه پروانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوت هرکس بقدر همت اوست</p></div>
<div class="m2"><p>طفل را شیر و مرغ را دانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سخن از دوست گو، ز غیر مگوی</p></div>
<div class="m2"><p>بگذر از قصهای افسانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر نقاب از جمال بردارد</p></div>
<div class="m2"><p>قاسمی جان دهد بشکرانه</p></div></div>