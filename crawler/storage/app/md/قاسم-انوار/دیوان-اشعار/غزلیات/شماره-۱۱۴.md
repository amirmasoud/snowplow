---
title: >-
    شمارهٔ ۱۱۴
---
# شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>ملامت را بمان، چه جای بیمست؟</p></div>
<div class="m2"><p>که روح القدس جانت را ندیمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگرچه راه دشوارست آخر</p></div>
<div class="m2"><p>که امداد از مددهای کریمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو عاشق باش و طور عاشقی ورز</p></div>
<div class="m2"><p>که عاشق بر صراط مستقیمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غنیمی کز روانت روضه سازد</p></div>
<div class="m2"><p>بچشم سر ببین: عشق آن غنیمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی، کز عاشقی بی رنگ و بویست</p></div>
<div class="m2"><p>مخوانش دل، که شیطان رجیمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم را غیر درگاه تو جا نیست</p></div>
<div class="m2"><p>مدام این دل برین درگه مقیمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا، قاسم، ز هستی توبه می کن</p></div>
<div class="m2"><p>که سلطان تو تواب الرحیمست</p></div></div>