---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>رخسار تو چون آینه صورت و معنیست</p></div>
<div class="m2"><p>در پرتو دیدار تو انوار تجلیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خاک کف پای تو هر بو که شنیدیم</p></div>
<div class="m2"><p>لطفیست که در خاصیت باد صبا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بوی تو شد جان و دلم زنده جاوید</p></div>
<div class="m2"><p>با نکهت طیب تو چه جای دم عیسیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون صورت و معنی تو در حد کمالست</p></div>
<div class="m2"><p>جان و دل ما عاشق آن صورت و معنیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جمله احوال پرستیدن صورت</p></div>
<div class="m2"><p>از نشائه ما نیست ولی نشائه ما نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک جذبه ز حق آمد و دل برد بغارت</p></div>
<div class="m2"><p>مجنون چه کند؟ کین کشش از جانب لیلیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم دل و دین داد بامید وصالت</p></div>
<div class="m2"><p>در مذهب عشاق همین توبه و تقویست</p></div></div>