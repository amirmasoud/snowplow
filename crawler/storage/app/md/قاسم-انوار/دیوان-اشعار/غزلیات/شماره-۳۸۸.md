---
title: >-
    شمارهٔ ۳۸۸
---
# شمارهٔ ۳۸۸

<div class="b" id="bn1"><div class="m1"><p>ماییم و حضرت تو و صد سوز و صد نیاز</p></div>
<div class="m2"><p>ای عشق چاره ساز جگرسوز جان گداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو در غنای مطلق و ما در فنای محض</p></div>
<div class="m2"><p>جانها در آرزوی تو، ای عشق چاره ساز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که: سر ببازم بر آستان تو</p></div>
<div class="m2"><p>گفتا که: هر چه بازی، می باز و کج مباز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن یار ظاهرست و در اعیان مقررست</p></div>
<div class="m2"><p>در کسوت حقیقت و در صورت مجاز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ترس و بیم باش، که عشقست بت شکن</p></div>
<div class="m2"><p>امیدوار باش، که وصلست دلنواز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قومی ز شوق روی تو در لذت مدام</p></div>
<div class="m2"><p>جمعی بجست و جوی تو در روزه و نماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوتاه کرده ایم حکایت ز هر چه بود</p></div>
<div class="m2"><p>اما بسان زلف تو گشت این سخن دراز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با رنج گفت: رنج ندارم بهیچ روی</p></div>
<div class="m2"><p>گفتند: سبز باشی و خوشبوی و سرفراز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر کس نیازمند کسی شد بصورتی</p></div>
<div class="m2"><p>قاسم نیاز برد بدرگاه بی نیاز</p></div></div>