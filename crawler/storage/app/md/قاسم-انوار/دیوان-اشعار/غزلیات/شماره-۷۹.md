---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>در خاکدان دهر دلی شادمان کجاست؟</p></div>
<div class="m2"><p>یک دل که ایمنست ز غم در جهان کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گیر و دار فتنه دوران بسوختیم</p></div>
<div class="m2"><p>داری خبر بگوی که: دارالامان کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صومعه چو جرعه ای از درد درد نیست</p></div>
<div class="m2"><p>راهم نشان دهید که: دیر مغان کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ارغنون ز درد خماریم در نفیر</p></div>
<div class="m2"><p>ساقی بیار جام، می ارغوان کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن دارد آن نگار که آنست هرچه هست</p></div>
<div class="m2"><p>آنرا طلب کنند حریفان که آن کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پنهان شدست یار چو گنج نهان ز ما</p></div>
<div class="m2"><p>هرچند ظاهرست که گنج نهان کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نو بهار عمر چو سر سبزیی نماند</p></div>
<div class="m2"><p>ای باغبان، بگوی که: باد خزان کجاست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>با آنکه یار در عمه اعیان عیان شدست</p></div>
<div class="m2"><p>مخفیست در ظهور، که عین عیان کجاست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم بر آستانه عشق تو بار یافت</p></div>
<div class="m2"><p>دانست سجده گاه سر عاشقان کجاست؟</p></div></div>