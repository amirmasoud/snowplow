---
title: >-
    شمارهٔ ۱۸۹
---
# شمارهٔ ۱۸۹

<div class="b" id="bn1"><div class="m1"><p>پیر ما جامیست، اما در خور این جام نیست</p></div>
<div class="m2"><p>باده صافی نوشد، اما رند درد آشام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شرابات خدا مستند ذرات دو کون</p></div>
<div class="m2"><p>لیک هر جان در جهان در خورد این انعام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش مستان طریقت این حکایت روشنست :</p></div>
<div class="m2"><p>درد نوشان خاص درگاهند و این می عام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باز ناز آغاز کرد آن یار و جان می پروریم</p></div>
<div class="m2"><p>لطف دیگر آنکه: این آغاز را انجام نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دایما در وصل آن جان و جهان مستغرقیم</p></div>
<div class="m2"><p>در چنین وصلی، که گفتم، حاجت پیغام نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفرین بر ساقی ما باد و بر مستی او</p></div>
<div class="m2"><p>گرچه جامی می کشد، بد مست و نافرجام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، در پیش این کوران مگو اسرار فاش</p></div>
<div class="m2"><p>هرکجا فهمی نباشد جای استفهام نیست</p></div></div>