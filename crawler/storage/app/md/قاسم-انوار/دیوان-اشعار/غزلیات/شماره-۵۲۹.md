---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>مقررست و معین بحجت و برهان</p></div>
<div class="m2"><p>که: غیر دوست کسی نیست در مکین و مکان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار بار بگفتم، هزار بار هزار</p></div>
<div class="m2"><p>که: قدر خود بشناس، ای خلاصه دوران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخست منزلت «الله » گوی و خوش می باش</p></div>
<div class="m2"><p>بگو به منزل ثانی که: «علم القرآن »</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجان دوست، که یک قصه را مسلم دار</p></div>
<div class="m2"><p>محمدست امین و خداست دار امان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدان ز مشرب عرفان حیات مگو جان یابی</p></div>
<div class="m2"><p>که عین آب حیاتست مشرب عرفان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دگر ز عقل حکایت مگو در این مجلس</p></div>
<div class="m2"><p>هزار عقل بیک جو بمجلس مستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا بگوش دل عاشقانه خوش بشنو:</p></div>
<div class="m2"><p>که شوق و شورش عشقست در زمین و زمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بقدر عشق بود جاه، هر کجا باشد</p></div>
<div class="m2"><p>چه جای حشمت جمشید و ملکت خاقان؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بآشکار و نهان قاسمی گوید</p></div>
<div class="m2"><p>که: عشق دوست بورزید آشکار و نهان</p></div></div>