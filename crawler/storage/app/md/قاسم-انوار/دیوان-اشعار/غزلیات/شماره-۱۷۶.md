---
title: >-
    شمارهٔ ۱۷۶
---
# شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>حلقه بر در مزن، که بارت نیست</p></div>
<div class="m2"><p>خانه کمتر طلب، که جارت نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتمت ناروا مدار چنان</p></div>
<div class="m2"><p>هوس دار و این دیارت نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه با یاد خویشتن باشی</p></div>
<div class="m2"><p>فکر خود کن، که فکر یارت نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نان خورش یاد یار می باشد</p></div>
<div class="m2"><p>گر همه شام و گر نهارت نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش بادت نصیحت مستان</p></div>
<div class="m2"><p>باده می نوش، چون خمارت نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آدمی چون ترا بدست آرد</p></div>
<div class="m2"><p>اشتر مستی و مهارت نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی شد مقیم خاک درت</p></div>
<div class="m2"><p>بعد از این حاجت تجارت نیست</p></div></div>