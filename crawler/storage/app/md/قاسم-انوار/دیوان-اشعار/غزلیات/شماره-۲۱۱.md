---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>دیدمش دوش که سرمست و خرامان می‌رفت</p></div>
<div class="m2"><p>جام بر کف، طرف مجلس مستان می‌رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده در دست و غزل خوان و عجب عربده‌جوی</p></div>
<div class="m2"><p>از نهان خانه واجب سوی امکان می‌رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن از روی دل‌افروز به مردم می‌گفت</p></div>
<div class="m2"><p>قصه‌ای از شکن زلف پریشان می‌رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس نداند صفت لطف خرامیدن او</p></div>
<div class="m2"><p>آب حیوان که به سرچشمه انسان می‌رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن چنان پادشهی نزد گدایان درش</p></div>
<div class="m2"><p>من نگویم به چه تمکین و چه سامان می‌رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون من آن شیوه رفتار و ملاحت دیدم</p></div>
<div class="m2"><p>اشک خونین ز دل و دیده به دامان می‌رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم از پای در افتاد چو دید آن شه را</p></div>
<div class="m2"><p>کز سراپرده کان جانب اعیان می‌رفت</p></div></div>