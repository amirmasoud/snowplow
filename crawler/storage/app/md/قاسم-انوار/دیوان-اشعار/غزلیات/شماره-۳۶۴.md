---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>بیا، بیا، که غریبیم و عاشقیم و نزار</p></div>
<div class="m2"><p>بیا، بیا، که نداریم بی تو صبر و قرار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا، که بی تو ز افراط آرزومندی</p></div>
<div class="m2"><p>مرا دلیست درو صد هزار شعله نار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بداغ عشق تو دل را هزار عز و شرف</p></div>
<div class="m2"><p>ز جلوهای تو جان را هزار استظهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بفرض آنکه جهان از قصور پاک شود</p></div>
<div class="m2"><p>محال صرف بود فیض غافر و غفار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مباش غره بگلگونه بهار، ای دل</p></div>
<div class="m2"><p>که در خزان نتوان یافتن گلی بر بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طریق عقل مقلد، فغان دارا گیر</p></div>
<div class="m2"><p>حدیث عشق مشعبد، هزار دار و مدار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همیشه خاطر قاسم بورد و ذکر شماست</p></div>
<div class="m2"><p>برین حدیث گواهست عالم الاسرار</p></div></div>