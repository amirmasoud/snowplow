---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>تا که از جور زمان بر جگرم ریش رسد؟</p></div>
<div class="m2"><p>حق بفریاد دل خسته درویش رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ز بیگانه نترسم،که درین راه مرا</p></div>
<div class="m2"><p>هر بلایی که رسد از قبل خویش رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یارب،این عشق بلاییست،ندانم چه بلاست؟</p></div>
<div class="m2"><p>هر چه پرهیز کنم تیر بلا پیش رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل،که درحال بلا ثابت و راسخ باشد</p></div>
<div class="m2"><p>چونکه معنیش تمام آمد دعویش رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون نمیرم؟که درین آتش غم میسوزم</p></div>
<div class="m2"><p>تیر هجران تو بر جان غم اندیش رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل و جان را بتو دادیم،هم از روز ازل</p></div>
<div class="m2"><p>راضیم از تو،اگر مرهم،اگر نیش رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتشی بود که در خرمن جانها افتاد</p></div>
<div class="m2"><p>وقت آنست که با قاسم دلریش رسد</p></div></div>