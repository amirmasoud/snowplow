---
title: >-
    شمارهٔ ۶۰۸
---
# شمارهٔ ۶۰۸

<div class="b" id="bn1"><div class="m1"><p>السلام علیک، یا سندی</p></div>
<div class="m2"><p>«انتموا سیدی و مستندی »</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تو دل عاشقست و حیرانست</p></div>
<div class="m2"><p>«قد تحیرت فیک، خذ بیدی »</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ازل در تو مست و حیرانند</p></div>
<div class="m2"><p>جمله جانها، که شاهد ابدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه دل خرده دان و زیرک بود</p></div>
<div class="m2"><p>گنگ شد پیش صدمت صمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه ذرات شاهدند که تو</p></div>
<div class="m2"><p>شاهد جان و واهب خردی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هله! ای منکر طریقت عشق</p></div>
<div class="m2"><p>عاشقان زبده اند و تو زبدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منکر راه شیر مردانی</p></div>
<div class="m2"><p>سگ به از تو، اگر درین عددی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت حق: «صد عن سبیل الله »</p></div>
<div class="m2"><p>تو ازان زمره ای، ازان صددی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی در فنای محض رسید</p></div>
<div class="m2"><p>از تجلی حضرت احدی</p></div></div>