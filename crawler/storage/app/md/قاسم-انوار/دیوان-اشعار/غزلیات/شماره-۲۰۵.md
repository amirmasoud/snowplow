---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>دردم ز اشتیاق تو ز اندازه در گذشت</p></div>
<div class="m2"><p>از پا در اوفتادم و آبم ز سر گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دل، که با وفای تو رفت از جهان برون</p></div>
<div class="m2"><p>جان بخش و مشکبو چو نسیم سحر گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر طور عشق روی تو هر کس که بار یافت</p></div>
<div class="m2"><p>موسی صفت ز عرصه طور بشر گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوی عاشقی، که دو عالم طفیل اوست</p></div>
<div class="m2"><p>آن کس قدم نهاد که از فکر سر گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لذت حیات جهان بهره ور نشد</p></div>
<div class="m2"><p>هر دل که از حقیقت خود بیخبر گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب، چه شکرها که ندارند عاشقان؟</p></div>
<div class="m2"><p>از لطف یار ما، که ز شیر و شکر گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک آستان تو جان را نثار کرد</p></div>
<div class="m2"><p>قاسم بحضرت تو ازین مختصر گذشت</p></div></div>