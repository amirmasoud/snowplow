---
title: >-
    شمارهٔ ۴۳۶
---
# شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>بدوستی، که ترا نیک دوست می دارم</p></div>
<div class="m2"><p>بجان دوست، که از غیر دوست بیزارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی چو من نبود در جهان کون و فساد</p></div>
<div class="m2"><p>اگر حجاب دویی را ز پیش بر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گناه بنده عظیم و عذاب دوست الیم</p></div>
<div class="m2"><p>ولی برحمت و فضلش امیدها دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بپیش تیغ تو هر دم هزار جان بدهم</p></div>
<div class="m2"><p>بجان دوست، که با تیغ تو سری دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگفتمش: بکرم رفع کن حجابم، گفت :</p></div>
<div class="m2"><p>تویی حجاب، ترا از میانه بردارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار بحر کشیدم، هنوز یک قطره</p></div>
<div class="m2"><p>اگر بدست من آید بجان خریدارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قاسمی خبر این جهان چه می پرسی؟</p></div>
<div class="m2"><p>به دوستی، اگر از خویشتن خبر دارم</p></div></div>