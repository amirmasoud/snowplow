---
title: >-
    شمارهٔ ۵۰۳
---
# شمارهٔ ۵۰۳

<div class="b" id="bn1"><div class="m1"><p>با هیچ رسید این صفت باده فروشان</p></div>
<div class="m2"><p>دایم ز سر سود شود مایه گریزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کش ز چه باشد؟ مگر از باغ بهشتست؟</p></div>
<div class="m2"><p>خنبش چه مقامست؟ بگو :روضه رضوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندر ته خم چیست، بگو: دردی دردست</p></div>
<div class="m2"><p>خشت سر خم چیست؟ بگو: لعل بدخشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکس که خورد باده کند عربده آغاز</p></div>
<div class="m2"><p>و آن کس که نخوردست طلب کار و خروشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در باده فروشی در می حاصل ما نیست</p></div>
<div class="m2"><p>جز نعره و فریاد و خروشیدن مستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سود و زیان غلغل مستانه تمامست</p></div>
<div class="m2"><p>خوش نعره مستانه! خوشا حالت رندان!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خنبش بلقب گفتم و سرپوش بدان خم</p></div>
<div class="m2"><p>نی خم سفالینه، بگو لجه عمان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای جان، همه هشیاری تو غایت بعدست</p></div>
<div class="m2"><p>تا مست نگردی نشود کار تو آسان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هان! تا ننهی پای درین راه بغفلت</p></div>
<div class="m2"><p>چون غرقه بخونند درین کوچه دلیران</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>هر روز ز روی دگرم روی نماید</p></div>
<div class="m2"><p>پس شانه زند زلف که شانست درین شان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قاسم، هم مردان خدا مست خموشند</p></div>
<div class="m2"><p>هان! تا نکنی غلغله در بزم خموشان</p></div></div>