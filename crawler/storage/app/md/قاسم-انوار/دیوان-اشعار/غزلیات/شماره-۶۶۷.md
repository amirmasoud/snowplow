---
title: >-
    شمارهٔ ۶۶۷
---
# شمارهٔ ۶۶۷

<div class="b" id="bn1"><div class="m1"><p>هله! ای عشق کهن سال، که هر روز نوی</p></div>
<div class="m2"><p>بنده فرمان تو هرجا که ضعیفست و قوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قیمت عشق ندانست دل و غافل ماند</p></div>
<div class="m2"><p>قیمت در شب افروز نداند قروی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصف آن یار ندانی، که ز دانش دوری</p></div>
<div class="m2"><p>قدر جانان نشناسی، که بجان در گروی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره ار یک دل و یک رنگ شوی مقبولی</p></div>
<div class="m2"><p>راه وحدت نتوان رفت بوصف بغوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باغبانا، بجهان تخم نکو باید کاشت</p></div>
<div class="m2"><p>هرچه کشتی، بیقین، باز همان را دروی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکرا لطف خدا شامل احوال شود</p></div>
<div class="m2"><p>ره بمقصود برد زود بوصف نبوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی،قصه جانان بصفت ناید راست</p></div>
<div class="m2"><p>ره تحقیق میسر نشود تا نروی</p></div></div>