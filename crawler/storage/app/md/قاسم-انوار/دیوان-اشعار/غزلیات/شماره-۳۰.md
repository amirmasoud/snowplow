---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>وقت آن شد که می ناب دهی مستان را</p></div>
<div class="m2"><p>خاصه من بیدل شوریده سرگردان را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدحی چند روان کن، که جگرها تشنه است</p></div>
<div class="m2"><p>تا ز خود دور کنم این سر و این سامان را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شیشه خالی و حریفان همه مخمورانند</p></div>
<div class="m2"><p>مگر از ساقی جان واطلبم تاوان را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میخانه ببستند، بده جامی چند</p></div>
<div class="m2"><p>تا به هم درشکنم این در و این دربان را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>«کل یوم هو فی شان» صفت سلطانیست</p></div>
<div class="m2"><p>گر شوی واقف اسرار بدانی شان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان من کشته آن غمزه مستانه تست</p></div>
<div class="m2"><p>چه محل باشد در حضرت جان جانان را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، زاهد ما در دو گناه افتادست</p></div>
<div class="m2"><p>می ننوشید و بسی طعنه زند مستان را</p></div></div>