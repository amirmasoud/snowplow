---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای آسیا، ای آسیا، سرگشته‌ای چون ما چرا؟</p></div>
<div class="m2"><p>از ما مپوشان راز خود، با ما بیان کن ماجرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چرخ خود مستانه‌ای در دور خود فرزانه‌ای</p></div>
<div class="m2"><p>از ما چه‌ها داری خبر؟ کز ما برقصی دایما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از کان جدا ماندی، زان در نفیری، ای رجا</p></div>
<div class="m2"><p>با ما بگو، ای بوالوفا، از قصه‌های ما مضا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>داری سلوکی بس عجب در حالت وجد و طرب</p></div>
<div class="m2"><p>در یک نفس طی می‌کنی از مبتدا تا منتها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از آب دارد جان ما در قصه‌ها چون آسیا</p></div>
<div class="m2"><p>ای عقل دوراندیش ما، آخر کجا رفتی؟ بیا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گم شو ولیکن گم مکن اسرار در عشق لَدُن</p></div>
<div class="m2"><p>هم تو نوی هم تو کهن هم کوه و هم بانگ و صدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم جان تویی، هم تو جهان، هم جوی و هم آب روان</p></div>
<div class="m2"><p>هم آسیابانی بدان هم گندمی، هم آسیا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای عشق، بس جان دوستی، هم دشمنی هم دوستی</p></div>
<div class="m2"><p>در مغزی و در پوستی، فی‌الجمله با شاه و گدا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ای محرم اسرار تو، ای جعفر طیار تو</p></div>
<div class="m2"><p>ای قاسم انوار تو، ای مس جان را کیمیا</p></div></div>