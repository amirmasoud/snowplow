---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>طریق عشق سپردن طریق بوالعجبیست</p></div>
<div class="m2"><p>نشان عشق نجستن نشان بی طلبیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو که: عشق حرامست در طریقت شرع</p></div>
<div class="m2"><p>که مست باده عشق اند اگر ولی و نبیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شراب ما همه از خم لامکان آمد</p></div>
<div class="m2"><p>چه جای کاسه چینی و شیشه حلبیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز موج عشق برقصیم و فاش می گوییم :</p></div>
<div class="m2"><p>خوشست شورش مستان، اگرچه بی ادبیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا بمجلس رندان و حال ما بنگر</p></div>
<div class="m2"><p>که جام ما ز می کوثر است، نه عنبیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو که: معنی قرآن حبیب از که گرفت؟</p></div>
<div class="m2"><p>زبان او عجم آمد، روان او عربیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طراوت دل و جان جلوهای مجنونست</p></div>
<div class="m2"><p>نشان بی طلبی ها نشان بی طربیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو طالب چلبی شو، که مقصد اقصیست</p></div>
<div class="m2"><p>که فیض روح مقدس ز حضرت چلبیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ببین که: قاسم بیدل ز دست رفت تمام</p></div>
<div class="m2"><p>بدان که ساقی جانها نبی مطلبیست</p></div></div>