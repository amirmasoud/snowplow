---
title: >-
    شمارهٔ ۲۱۹
---
# شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>من و معشوق و جام ناب صباح</p></div>
<div class="m2"><p>بگشا بر من این در،ای فتاح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در در بسته از کرم بگشا</p></div>
<div class="m2"><p>در در بسته را تویی مفتاح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و کشتی و راه دریا بار</p></div>
<div class="m2"><p>خطری نیست، لاح فی الملاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خطری نیست، ازچه می ترسید؟</p></div>
<div class="m2"><p>لیس فی البحر غیرناتمساح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدحی دیگرم تصدیق کن</p></div>
<div class="m2"><p>کلما زدت،زدت فی الارواح</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار مستست و باده می نوشد</p></div>
<div class="m2"><p>در چنین دم صلاح نیست صلاح</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در چنین حالتی بفتوی عشق</p></div>
<div class="m2"><p>عیش جانهامباح گشت، مباح</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش مستان گرفت نیست، که ما</p></div>
<div class="m2"><p>مست عشقیم در صباح ورواح</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مستی مازحد گذشت، که دوست</p></div>
<div class="m2"><p>جام در دست و می کند الحاح</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بهر دام دل شکسته دلان</p></div>
<div class="m2"><p>ساختند از ملاح صد ملواح</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان هر کس سجنجلست، اما</p></div>
<div class="m2"><p>جان قاسم سجنجل الارواح</p></div></div>