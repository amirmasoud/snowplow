---
title: >-
    شمارهٔ ۴۳۸
---
# شمارهٔ ۴۳۸

<div class="b" id="bn1"><div class="m1"><p>گر بنالم من از این درد که در دل دارم</p></div>
<div class="m2"><p>بس عجب نبود اگر رحم کند دلدارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کهنه گنجیست درین کنج نهانی پنهان</p></div>
<div class="m2"><p>ترک سر گویم و آن گنج نهان بردارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قسمتی کان ز ازل رفت چه شاید کردن؟</p></div>
<div class="m2"><p>من بر آن قسمتم، ار زاهد، اگر خمارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش: رو بنما، گفت که: هی! حد تو نیست</p></div>
<div class="m2"><p>خجل از گفته خویشم، پس سر می خارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک گلگون مرا رحم کن، ای جان و جهان</p></div>
<div class="m2"><p>که بسودای تو از دیده فرو می بارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت کشته شمشیر غمت خواهم شد</p></div>
<div class="m2"><p>من که از واقعه عشق تو بر خور دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ کس غیر تو در جان و دل قاسم نیست</p></div>
<div class="m2"><p>حالم اینست، اگر مستم، اگر هوشیارم</p></div></div>