---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>ای چشم تو در شوخی سرفتنه دوران‌ها</p></div>
<div class="m2"><p>خط خوش و رخسارت رشک گل و ریحان‌ها</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نرگس مخمورت وز زلف پریشانت</p></div>
<div class="m2"><p>سرمست صفا دل‌ها، آغشته غم جان‌ها</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که: نکو دانم وصف دهنت، گفتا:</p></div>
<div class="m2"><p>در قصه جان ماندی، با دعوی عرفان‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مسجد و میخانه هرجا که روم بینم</p></div>
<div class="m2"><p>از درد تو زاری‌ها وز شوق تو افغان‌ها</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتی: همه تیر خود بر جان تو اندازم</p></div>
<div class="m2"><p>ای عهدشکن، باری، کو آن همه پیمان‌ها؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غایت مشتاقی باشد دل و جانم را</p></div>
<div class="m2"><p>با جور تو راحت‌ها، با درد تو درمان‌ها</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شوق تو ز جان من گر می‌طلبی شاید</p></div>
<div class="m2"><p>چون گنج طلب کردن رسمست رویران‌ها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفتی: دل قاسم را از جور بسوزانم</p></div>
<div class="m2"><p>دل غرق خجالت شد از کثرت احسان‌ها</p></div></div>