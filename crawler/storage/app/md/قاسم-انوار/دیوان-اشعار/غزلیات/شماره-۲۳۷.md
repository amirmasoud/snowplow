---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>ز ذوق عالم عرفان کجا خبر دارد</p></div>
<div class="m2"><p>کسی که همت درون،فکرمختصردارد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی بوصف نکو راه یابد اندر دل</p></div>
<div class="m2"><p>اگر بحسن و لطافت رخ قمر دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگو بواعظ ما: دین خود نگه می دار</p></div>
<div class="m2"><p>بشرط آنکه دلت زین متاع اگر دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهیچ حال بجز دوست سر فرو نارد</p></div>
<div class="m2"><p>دلی که از صفت عاشقی خبر دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگوزحسن ولطافت بپیش خواجه خطیب</p></div>
<div class="m2"><p>که غیر عالم تو عالمی دگر دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کمر نبنددهرگز بچست و چالاکی</p></div>
<div class="m2"><p>کسی که باغم او دست در کمر دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بحسن دلبر ما کیست در جهان،قاسم؟</p></div>
<div class="m2"><p>هزار شیوه شیرین چون شکر دارد</p></div></div>