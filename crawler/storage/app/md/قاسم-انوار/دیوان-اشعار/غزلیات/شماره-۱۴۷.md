---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>در سویدای دلم سودای اوست</p></div>
<div class="m2"><p>در دل و جانم تمناهای اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیر اعظم، که شمع عالمست</p></div>
<div class="m2"><p>پرتوی از چهره زیبای اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من نمی دانم ز حال دل که چیست؟</p></div>
<div class="m2"><p>این قدر دانم که دل مولای اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مقلد با طریقت ره نبرد</p></div>
<div class="m2"><p>در حقیقت خار ما خرمای اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که فانی شد ز طبع آب و خاک</p></div>
<div class="m2"><p>این قبای عشق بر بالای اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بوی جان می آید از باد و صبا</p></div>
<div class="m2"><p>نکهتی از عنبر سارای اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی چون واقف اسرار شد</p></div>
<div class="m2"><p>خاک کویش جنت الماوای اوست</p></div></div>