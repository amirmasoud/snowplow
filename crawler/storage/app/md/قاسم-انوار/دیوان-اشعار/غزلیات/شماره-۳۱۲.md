---
title: >-
    شمارهٔ ۳۱۲
---
# شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>رنگ رز خواست که خمی کند از کور و کبود</p></div>
<div class="m2"><p>رنگ او راست نشد،حیرت و وحشت افزود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خم اگر تیره شود،صوفی ما طیره مشو</p></div>
<div class="m2"><p>که درین کاسه همانست که از خم پالود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان شرابی که ازو زنده شود جان و جهان</p></div>
<div class="m2"><p>ساقی جان و جهان بر دل ما می پیمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جمله دلها همه مستند چو دیدند این حال</p></div>
<div class="m2"><p>که صراحی بسجود آمد و جانها بشهود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باده از خم الهی خور و چندان می خور</p></div>
<div class="m2"><p>که ترا باز رهاند همه از ننگ وجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخرابات جهان واله و سرگردانیم</p></div>
<div class="m2"><p>کس نداند که چه حالست و چه افتاد و چه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی را ز شرابات الهی در ده</p></div>
<div class="m2"><p>ساقی،امروز علی رغم جحودان حسود</p></div></div>