---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>از باده گلگون قدری هست بگویید</p></div>
<div class="m2"><p>در شهر چو زیبا قمری هست بگویید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را خبری نیست، که مستان خرابیم</p></div>
<div class="m2"><p>گر زانکه شما را خبری هست بگویید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دیر مغان، نیم شبان، در شب تاریک</p></div>
<div class="m2"><p>جز پیر مغان راهبری هست؟ بگویید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای زاهد مغرور، مکن منع من از عشق</p></div>
<div class="m2"><p>گر تیر بلا را سپری هست بگویید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشقست که بابای جهانست بتحقیق</p></div>
<div class="m2"><p>جز عشق کسی را پدری هست؟بگویید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق تو دلها همه چون آتش گرمند</p></div>
<div class="m2"><p>دل گرم تر از من دگری هست؟ بگویید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آن دور رخ و زلف تو غارتگر دلهاست</p></div>
<div class="m2"><p>در دور و تسلسل نظری هست؟ بگویید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در باغ لطافت، که چراغ همه دلهاست</p></div>
<div class="m2"><p>شیرین ثمری، بر شجری هست؟ بگویید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مصر جهان دل نگرانیم چو یعقوب</p></div>
<div class="m2"><p>گر یوسف زرین کمری هست بگویید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>در بادیه هجر بماندم شب تاریک</p></div>
<div class="m2"><p>گر تیره شبم را سحری هست بگویید</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در کشتن قاسم، که دلش مست و خرابست</p></div>
<div class="m2"><p>بالا شجری، دل حجری هست؟ بگویید</p></div></div>