---
title: >-
    شمارهٔ ۵۶
---
# شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>مرآت اوست جمله ذرات کاینات</p></div>
<div class="m2"><p>«یا عاشقین، قوموا، حیوا علی الصلات »</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی عشق او بادب رو، که گفته اند :</p></div>
<div class="m2"><p>در طور عاشقی حسناتست سیئات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای آرزوی جان، چه کنم من دوای دل؟</p></div>
<div class="m2"><p>بی تو نه خواب دارم و نه صبر و نه ثبات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان مست روی تست، که آن روی دلفروز</p></div>
<div class="m2"><p>صبحیست از سعادت و روزیست از نجات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زار و نزار تست، دلم هر کجا که هست</p></div>
<div class="m2"><p>جان دوستدار تست، اگر موت اگر حیات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسم الله، ار بخون دلم مایلی بگو</p></div>
<div class="m2"><p>روزی مبارک آمد و شب لیلة البرات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتند: قاسمی ز هوای تو جان نبرد</p></div>
<div class="m2"><p>در خنده رفت یار و چنین گفت «مات مات »</p></div></div>