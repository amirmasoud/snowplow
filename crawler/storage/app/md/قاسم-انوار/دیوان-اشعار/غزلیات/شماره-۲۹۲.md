---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>آخر،ای شوخ جهان،عشوه گری با ما چند؟</p></div>
<div class="m2"><p>ما بسودای تو مردیم، خدا را مپسند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در غمت خسته دلان غرقه خونند مدام</p></div>
<div class="m2"><p>نفسی برسرشان آ و ببین در چه دمند؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن دل از وسوسه هر دو جهان آزادست</p></div>
<div class="m2"><p>که بزنجیر سر زلف تو افتاده ببند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عار داریم ز شاهی بهوای تو،ببین</p></div>
<div class="m2"><p>که گدایان سر کوی تو چون محتشمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی چون ماه تو خواهیم،زهی طالع سعد!</p></div>
<div class="m2"><p>قد چون سرو تو جوییم،زهی بخت بلند!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر اسیران سر کوی غمت چون گذری</p></div>
<div class="m2"><p>نظری کن ز سر زلف،که اهل نظرند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم:از خویش بریدم،بتوپیوند شدم</p></div>
<div class="m2"><p>گفت :احسنت و زهی قاسم نیکو پیوند!</p></div></div>