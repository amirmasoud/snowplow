---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>در صومعه و دیر مغان هیچ سری نیست</p></div>
<div class="m2"><p>کز آتش عشق تو در آن سر شرری نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذرات جهان آینه سر الهند</p></div>
<div class="m2"><p>در کوچه ما عاشق صاحب نظری نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلس زهاد خبر جستم از آن یار</p></div>
<div class="m2"><p>گفتند: خبر اینست که: ما را خبری نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در وادی تاریک جهان مرد بزاری</p></div>
<div class="m2"><p>آن را که دلیلش رخ همچون قمری نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جایی نتوان یافت، که از عکس جمالش</p></div>
<div class="m2"><p>بالا شجری، دل حجری، لب شکری نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسرار خدا فاش مکن، تا که نگویند:</p></div>
<div class="m2"><p>در روی زمین هیچ کس از وی بتری نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند که: این راه درازست و خطرناک</p></div>
<div class="m2"><p>گر راست روی راه خدا را،خطری نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر بار درین کوچه طلب کرد مقلد</p></div>
<div class="m2"><p>بارش کن از آن بار، که کمتر ز خری نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در دست دوای دل بیچاره قاسم</p></div>
<div class="m2"><p>جز درد درین راه دگر چاره بری نیست</p></div></div>