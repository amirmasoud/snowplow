---
title: >-
    شمارهٔ ۵۲۷
---
# شمارهٔ ۵۲۷

<div class="b" id="bn1"><div class="m1"><p>گفت حق: «کل من علیها فان »</p></div>
<div class="m2"><p>بفنا راضیم برغبت جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل «کان » ز «شان » شود روشن</p></div>
<div class="m2"><p>مشکل «شان » ز جان الله خوان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست شوقم ز عشق شورانگیز</p></div>
<div class="m2"><p>چو برقصست ازو زمین و زمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفت طیفور: «اعظم الشانی »</p></div>
<div class="m2"><p>که چنینست شأن سرمستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله ذرات کون می گویند</p></div>
<div class="m2"><p>داستان ترا بصد دستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بود دل، عیان توان دیدن</p></div>
<div class="m2"><p>نور حق را ز ظلمت حدثان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد و روزه و نماز و بهشت</p></div>
<div class="m2"><p>ما و معشوق و عشق جان در جان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ره بتوحید چون توانی برد؟</p></div>
<div class="m2"><p>عین او را ندیده در اعیان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>پرده بردار، تا شود فی الحال</p></div>
<div class="m2"><p>در چنین عید قاسمی قربان</p></div></div>