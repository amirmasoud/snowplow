---
title: >-
    شمارهٔ ۳۷۶
---
# شمارهٔ ۳۷۶

<div class="b" id="bn1"><div class="m1"><p>لاف عرفان می زند آن زاهد لاغر شکار</p></div>
<div class="m2"><p>نغمه ققنوس را با جقبق عقعق چه کار؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حس نداری، گر ندانی بوی دوزخ از بهشت</p></div>
<div class="m2"><p>خوار باشی، گر گل و نسرین نمی دانی ز خار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفی ما در طلب چون گوی می گردد بسر</p></div>
<div class="m2"><p>مهره گل را نمی داند ز در شاهوار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوفی ما خواست تا با گنج مخفی پی برد</p></div>
<div class="m2"><p>در حقیقت گنج مخفی را نمی داند ز مار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته بودی: در بروی ما ببندد آن رقیب</p></div>
<div class="m2"><p>آخر ای جان و جهان، ما را درین درها مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بارها رفتم بدرگاه تو، کس بارم نداد</p></div>
<div class="m2"><p>یافت جان خسته ام در حضرتت این بار بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ناصحا با ما سخن از عقل سرگردان مگوی</p></div>
<div class="m2"><p>عاشقی فخرست و ما از عاقلی داریم عار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جانت اندر خواب غفلت مرد و غافل مانده ای</p></div>
<div class="m2"><p>ساعتی برخیز و رسم ماتم جان را بدار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی را جام ده، ساقی، که وقت فرصتست</p></div>
<div class="m2"><p>عاشقان را باده فرما، عاقلان را انتظار</p></div></div>