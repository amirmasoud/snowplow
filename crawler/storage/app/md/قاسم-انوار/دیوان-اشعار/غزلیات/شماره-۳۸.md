---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>«اسمعوا منی، یا اولی الالباب »:</p></div>
<div class="m2"><p>همه لبند و دوست لب لباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه در وقت آشکار و نهان</p></div>
<div class="m2"><p>این حکایت کنند چنگ و رباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه بشنید در میان آرد</p></div>
<div class="m2"><p>بهمان وصف عود در مضراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز تو کس نیست از ظهور و بطون</p></div>
<div class="m2"><p>بهمین شد تمام فصل خطاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما بمحبوب راز می گوییم</p></div>
<div class="m2"><p>«اغلق الباب، ایهاالبواب »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخنی می رود ز شوق درون</p></div>
<div class="m2"><p>این سخن را بذوق جان دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن از روی و زلف او گویم</p></div>
<div class="m2"><p>قصه روشن و شب مهتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر زمانی ندا رسد از غیب:</p></div>
<div class="m2"><p>دل مبندید در رباط خراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی روی بر زمین دارد</p></div>
<div class="m2"><p>رو ازین خسته فقیر متاب</p></div></div>