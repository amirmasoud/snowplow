---
title: >-
    شمارهٔ ۵۸۳
---
# شمارهٔ ۵۸۳

<div class="b" id="bn1"><div class="m1"><p>ای کمالت نعت عزت در جهان انداخته</p></div>
<div class="m2"><p>جان ز شوق تو کله بر آسمان انداخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک سخن گفته زر از خویشتن با بلبلان</p></div>
<div class="m2"><p>شور و غوغا در زمین و در زمان انداخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان از عشق رویت عقل در شور آمده</p></div>
<div class="m2"><p>جان و دل را در محیط بی کران انداخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ارغوان را گفته: وصل ما بیابی، غم مخور</p></div>
<div class="m2"><p>زین حکایت صد عرق بر ارغوان انداخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قعر دریای کمال از ناگهان موجی زده</p></div>
<div class="m2"><p>مستعین را در مقام مستعان انداخته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی نشانست آن حبیب، اما برای بازیافت</p></div>
<div class="m2"><p>صد حدیث با نشان در بی نشان انداخته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یک کرشمه کرده با خود از برای خویشتن</p></div>
<div class="m2"><p>قاسمی را در بلای جاودان انداخته</p></div></div>