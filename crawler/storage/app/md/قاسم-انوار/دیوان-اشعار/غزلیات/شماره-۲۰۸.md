---
title: >-
    شمارهٔ ۲۰۸
---
# شمارهٔ ۲۰۸

<div class="b" id="bn1"><div class="m1"><p>ذکر جمیل یار جهان را فرو گرفت</p></div>
<div class="m2"><p>عالم گرفت، لیک بوجه نکو گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان نکته ای شنیداز آن حسن بر کمال</p></div>
<div class="m2"><p>سوزی زدل برآمد و شوری درو گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فارغ شد از سلامت و راه فنا گزید</p></div>
<div class="m2"><p>هر دل که با ملامت عشق تو خو گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روشن شد از لوامع اشراق آن جمال</p></div>
<div class="m2"><p>آن پرتوی که نیر خورشید ازو گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خواند گل ز وصف جمال تو آیتی</p></div>
<div class="m2"><p>عشقت چه نکتها که برو رو برو گرفت؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اوصاف یار عشق نخست از خرد شنید</p></div>
<div class="m2"><p>اول ازو شنید و بآخر برو گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اندر میان این همه رندان باده نوش</p></div>
<div class="m2"><p>جم بود خال آدم و جام جم او گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دانی میان زاهد و عارف چه فرق بود؟</p></div>
<div class="m2"><p>این راه اعتدال گزید، آن علو گرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم میان خاک در شاهوار یافت</p></div>
<div class="m2"><p>چون باز یافت باز ره جست و جو گرفت</p></div></div>