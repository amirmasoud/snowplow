---
title: >-
    شمارهٔ ۶۰۵
---
# شمارهٔ ۶۰۵

<div class="b" id="bn1"><div class="m1"><p>با یاد خدا باش به هر جای که هستی</p></div>
<div class="m2"><p>بی یار نگویم به تو هشیار، که مستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صومعه رفتی به صفا، وقت تو خوش باد!</p></div>
<div class="m2"><p>زنهار! که در صومعه خود را نپرستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر چه فتادت که درین راه خطرناک</p></div>
<div class="m2"><p>جامی نچشیدی و دو صد جام شکستی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دوست، بگو راست که احوال تو چونست؟</p></div>
<div class="m2"><p>در مکتب شادی ز چه رو در عبسستی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باری چه رسیدت، که درین غایت مقصود</p></div>
<div class="m2"><p>عشاق رسیدند و تو در بار نشستی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغان همه بر ذِروه کُهسار رسیدند</p></div>
<div class="m2"><p>احوال تو چون شد که میان قفسستی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای محتسب، آخر دل ما بیش میازار</p></div>
<div class="m2"><p>تو محتسب راه نه، میر عسستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در بادیه هجر بماندم شب تاریک</p></div>
<div class="m2"><p>فریاد رس ای دوست، که فریاد رسستی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم همه در حال تو حیران شده، تا کی</p></div>
<div class="m2"><p>در نعره و آشوب به سان جرسستی؟</p></div></div>