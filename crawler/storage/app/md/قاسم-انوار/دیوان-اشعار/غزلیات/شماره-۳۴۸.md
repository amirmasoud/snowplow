---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>خانه ما روضه شد،چون مقدم رضوان رسید</p></div>
<div class="m2"><p>دیده روشن شد،چو بوی یوسف کنعان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصه عشق زلیخا را کجا پنهان کنم؟</p></div>
<div class="m2"><p>کین حکایت از سواد مصر تا صنعان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش ازین در شهر جانها رفته بودی لایزال</p></div>
<div class="m2"><p>شهر ایمن گشت،اکنون سنجق سلطان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا، تا ذکر هشیاران نگویی بعد ازین</p></div>
<div class="m2"><p>وقت هشیاران برفت و نوبت مستان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا،ما را پیاپی ده قدح،کز فضل یار</p></div>
<div class="m2"><p>حالت هجران گذشت و نوبت احسان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چند گویی!واعظ،آخر از خدا شرمی بدار</p></div>
<div class="m2"><p>نوبت جان در گذشت و نوبت جانان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی،تا چند می نالی ز درد دل؟بگو:</p></div>
<div class="m2"><p>دردها بگذشت،اکنون نوبت درمان رسید</p></div></div>