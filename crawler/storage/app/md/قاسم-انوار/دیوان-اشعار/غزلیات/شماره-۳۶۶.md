---
title: >-
    شمارهٔ ۳۶۶
---
# شمارهٔ ۳۶۶

<div class="b" id="bn1"><div class="m1"><p>در داستان عشق تفصی نمود یار</p></div>
<div class="m2"><p>تکرار عشق کرد هزاران هزار بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دستار و خرقها گرو جام باده شد</p></div>
<div class="m2"><p>تا لمعه جمال تو دیدند آشکار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بردار بی مدار جهان اعتماد نیست</p></div>
<div class="m2"><p>حسن وفا مجوی ازین دار بی مدار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از وفات من چو بخاکم گذر کنی</p></div>
<div class="m2"><p>از خاک تربتم شنوی نالهای زار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین سان که حسن روی تو در جلوه آمدست</p></div>
<div class="m2"><p>گر از جهان خروش برآید عجب مدار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شهر می خرامی و جانها بر آتشست</p></div>
<div class="m2"><p>زان زلف تابدار و زان چشم پرخمار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، اگر تو طالب راهی و عاشقی</p></div>
<div class="m2"><p>چون جان طلب کنند، بده جان و سر مخار</p></div></div>