---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>نگین سلیمان بدیوان که داد؟</p></div>
<div class="m2"><p>سریر سلاطین بدربان که داد؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفات کمال خداوند را</p></div>
<div class="m2"><p>بدست مرنج و مرنجان که داد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرت رنگ وبویی از آن یار هست</p></div>
<div class="m2"><p>بگو:رنگ لعل بدخشان که داد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همی ترسم این جام را بشکند</p></div>
<div class="m2"><p>که جام سلیمان بموران که داد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شیر راهی حقیقت بدان</p></div>
<div class="m2"><p>که این زوروشیروپلنگان که داد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حقیقت گر از بحر مایی مگو</p></div>
<div class="m2"><p>که:درها بدریای عمان که داد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگو:بوی وصلی که جان پرود</p></div>
<div class="m2"><p>پس از فرقت پیر کنعان که داد؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر داد حق دیده ای باز گو:</p></div>
<div class="m2"><p>فلک را همه در و مرجان که داد؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه حسن و لطفی،که در آدمیست</p></div>
<div class="m2"><p>بگو راست، قاسم، بانسان که داد؟</p></div></div>