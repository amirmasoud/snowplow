---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>دیده ام تا بر رخ آن گل عذار افتاده است</p></div>
<div class="m2"><p>اشک سرخم بر رخ زرد آشکار افتاده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کسی را اختیاری هست در عالم، مرا</p></div>
<div class="m2"><p>عشق او بر هر دو عالم اختیار افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست و حیران و خرابم از کمال حسن یار</p></div>
<div class="m2"><p>تا دو چشم نرگسینش در خمار افتاده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتمش: عمر عزیز، آخر خرابم از غمت</p></div>
<div class="m2"><p>ز آتش عشق توام جان در شرار افتاده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کمال کبریا محبوب سویم ننگریست</p></div>
<div class="m2"><p>گفت: ما را چون تو هر جا صد هزار افتاده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم : آخر جز پریشانی ندارم هیچ کار</p></div>
<div class="m2"><p>تا مرا با زلف مشکین تو کار افتاده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت: قاسم، بر سر خاک مذلت پیش من</p></div>
<div class="m2"><p>چون تو بسیاری پریشان روزگار افتاده است</p></div></div>