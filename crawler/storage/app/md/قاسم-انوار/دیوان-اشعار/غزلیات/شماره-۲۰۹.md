---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>هرگز هوای وصل تو از جان ما نرفت</p></div>
<div class="m2"><p>سودای سلطنت ز سر این گدا نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک شب نشد که از غم عشقت ز چشم و دل</p></div>
<div class="m2"><p>سیلابها نیامد و فریادها نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلبی که نقد دولت دردترا نجست</p></div>
<div class="m2"><p>مس پاره ایست کز طلب کیمیانرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتی:سگ منست فلان، محترم شدم</p></div>
<div class="m2"><p>هرگز چنین مبالغه در مدح ما نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق نشد دلی که نیامد اسیر غم</p></div>
<div class="m2"><p>صادق نبود هر که بتیغ بلا نرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی که دل شکسته نیامد بکوی تو</p></div>
<div class="m2"><p>با تحفه ای ز درد،که با صد دوا نرفت؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ارزان خرید درد تو قاسم بجان و دل</p></div>
<div class="m2"><p>با مشتری مبالغه ای در بها نرفت</p></div></div>