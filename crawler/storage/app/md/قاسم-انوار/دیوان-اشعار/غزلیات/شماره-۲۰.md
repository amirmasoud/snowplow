---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>گریبان می‌درم هردم که: دامان درمکش از ما</p></div>
<div class="m2"><p>که ما مشتاق دیداریم و رند و عاشق و شیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم مست میگونت بگو: ای ترک یغمایی</p></div>
<div class="m2"><p>که: آخر چیست مقصودت ز چندین غارت دل‌ها؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان خورشید رخسارت سواد زلف یک سو کن</p></div>
<div class="m2"><p>که مشتاقان به روز آیند از تاریکی شب‌ها</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنان در عشق یک رویم که گر تیغم رود بر سر</p></div>
<div class="m2"><p>به روز امتحان باشم چو شمع استاده پابرجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سر تا پا همه جانم، که غرق عشق جانانم</p></div>
<div class="m2"><p>همه عشقست ارکانم، ز سر تا پا، ز سر تا پا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز زلف دل پریشانست گفتم؛ گفت: عاشق را</p></div>
<div class="m2"><p>شراب لعل می‌باید علاج علت سودا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز قاسم عشق می‌بارد، ز واعظ زهد و رعنایی</p></div>
<div class="m2"><p>که «بعدالمشرقین» آمد میان شیخ و مولانا</p></div></div>