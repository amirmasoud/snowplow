---
title: >-
    شمارهٔ ۵۰۵
---
# شمارهٔ ۵۰۵

<div class="b" id="bn1"><div class="m1"><p>بشنو ز عشق رمزی، حیران مباش، حیران</p></div>
<div class="m2"><p>یک جام به ز صد جم در بزم می پرستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که صاف نوشد، در راه زهد کوشد</p></div>
<div class="m2"><p>لیکن خبر ندارد از ذوق درد نوشان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای جان جان جانم، در حال من نظر کن</p></div>
<div class="m2"><p>تا دل بناله آید، تا جان شود خروشان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون با تو باشد این دل جان را غمی نباشد</p></div>
<div class="m2"><p>در عرصه قیامت، روز صراط و میزان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه عشق جانان، حیران مباش، حیران</p></div>
<div class="m2"><p>صحوست ضد حیرت، کفرست ضد ایمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کافر بوقت مردن روی آورد بدان رو</p></div>
<div class="m2"><p>چون روی نیک بیند، از بد شود پشیمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواهی سماع مستان خوش گردد، ای دل و جان</p></div>
<div class="m2"><p>یا در میان چرخ آ، یا آستین برافشان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل پرده دارد اما، دارد بتو تولی</p></div>
<div class="m2"><p>این پردها بسوزد از آه دردمندان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آشفته گشت قاسم آن دم که گشت پیدا</p></div>
<div class="m2"><p>بر چهره مشعشع آن زلفها پریشان</p></div></div>