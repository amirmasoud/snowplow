---
title: >-
    شمارهٔ ۴۴۷
---
# شمارهٔ ۴۴۷

<div class="b" id="bn1"><div class="m1"><p>چشم گریان و دل زار و نزاری دارم</p></div>
<div class="m2"><p>در نهان خانه دل نقش نگاری دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زر نابم، که ببازار جهان آمده ام</p></div>
<div class="m2"><p>محکی کو؟ که ببیند که عیاری دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من از آن شهر کلانم، نه از آن ده که تویی</p></div>
<div class="m2"><p>با همه خلق جهان دار و مداری دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو چه دانی که من این جا بچه کار آمده ام؟</p></div>
<div class="m2"><p>که بصحرای بشر عزم شکاری دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش آهنگ خرانی و بدان مفتخری</p></div>
<div class="m2"><p>علم الله، که از فخر تو عاری دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچو بلبل که بنالد بهوای گل مست</p></div>
<div class="m2"><p>با خیالش همه شب ناله زاری دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی نیست ازین شهره ملامت بگذار</p></div>
<div class="m2"><p>من ز شهر دگرم، رو بدیاری دارم</p></div></div>