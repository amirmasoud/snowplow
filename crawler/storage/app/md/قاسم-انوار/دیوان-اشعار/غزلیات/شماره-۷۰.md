---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>رنگ رز رنگ خمی کرد و نیامد خم راست</p></div>
<div class="m2"><p>گنه رنگ رزست این و تو گویی: خم راست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ رز کافر اماره آواره بود</p></div>
<div class="m2"><p>قول و فعلش همه در راه خدا روی و ریاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رنگ عقل و دل و جان گیر، اگر رنگ رزی</p></div>
<div class="m2"><p>رنگ اماره بدکاره همه زنگ خطاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو در رنگ رزی عاقل و دانا باشی</p></div>
<div class="m2"><p>جنس با جنس درآمیز، که نورست و صفاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ جان رنگ لطیفست درآمیز درو</p></div>
<div class="m2"><p>که همه نور هدایت ز جبینش پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس اماره ما مایه کفرست یقین</p></div>
<div class="m2"><p>چونکه برخاست، همه کفر و ضلالت برخاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این همه گفت و شنیدیم ولی هست یقین</p></div>
<div class="m2"><p>کار با زاهد خود کام نمی آید راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان بهجران تو آخر چه المها که کشید؟</p></div>
<div class="m2"><p>دل بدرد تو درآمیخت که آن عین دواست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دل و جان را بتو دادیم و فراغت گشتیم</p></div>
<div class="m2"><p>قاسمی، در ره تحقیق فنا عین بقاست</p></div></div>