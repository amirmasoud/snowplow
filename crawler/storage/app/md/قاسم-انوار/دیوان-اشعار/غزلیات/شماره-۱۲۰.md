---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ای ساقی جان بخش، که در جام تو جانست</p></div>
<div class="m2"><p>پر کن قدح باده، که دل در خفقانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن سرو روان رفت بهر جای که دل داشت</p></div>
<div class="m2"><p>جان و دل ما در پی آن سرو روانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن شاه دل افروز، که سرمایه حسنست</p></div>
<div class="m2"><p>هرجا که روان شد دل و جانها نگرانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلها همه گلشن شد و جانها همه روشن</p></div>
<div class="m2"><p>آن ماه دل افروز مگر شمع جهانست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل خواست که با عشق برآید بتجلد</p></div>
<div class="m2"><p>هرچند که کوشید، ولیکن نتوانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام دل ما را بشکست آن مه روشن</p></div>
<div class="m2"><p>گر جام شکسته است، ولی دوست همانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی، بده آن جام وز شفقت نظری کن</p></div>
<div class="m2"><p>قاسم گذرانست و جهان در گذرانست</p></div></div>