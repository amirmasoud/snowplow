---
title: >-
    شمارهٔ ۵۸۸
---
# شمارهٔ ۵۸۸

<div class="b" id="bn1"><div class="m1"><p>گرم از طالع فرخ رخ جانان شود دیده</p></div>
<div class="m2"><p>ز عکس رنگ آن رخسار عین جان شود دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بوقت دیدن رویش نبیند دیده ام خود را</p></div>
<div class="m2"><p>عجب گر هیچ عاشق را بدان امکان شود دیده!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدور چشم مخمورش جهان مستند و من مستم</p></div>
<div class="m2"><p>ندانم هیچ هشیاری درین دوران شود دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو زلف و روی او بیند مشتاقان شیدایی</p></div>
<div class="m2"><p>از آن آشفته گردد دل، درین حیران شود دیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وگر گوید که: بنمایم جمال عالم آرا را</p></div>
<div class="m2"><p>در آن امید سر تا پای مشتاقان شود دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر از عارض برافشاند سواد عنبرین گیسو</p></div>
<div class="m2"><p>بزیر کفر زلفش لمعه ایمان شود دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برای عید وصلش، قاسمی، قربان شوی،آری</p></div>
<div class="m2"><p>که داند تا چه عیدی اندرین قربان شود دیده</p></div></div>