---
title: >-
    شمارهٔ ۶۶۲
---
# شمارهٔ ۶۶۲

<div class="b" id="bn1"><div class="m1"><p>هله ای دوست، چه گویم؟ که تو محبوب جهانی</p></div>
<div class="m2"><p>همه سعدی و سعادت، همه لطفی، همه جانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتو چشمم شده روشن، بتو کویم شده گلشن</p></div>
<div class="m2"><p>همه فتحی و فتوحی، همه امنی امانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چه وصفت کنم، ای جان؟ که تو از وصف برونی</p></div>
<div class="m2"><p>تو بصیر همه بینی و خبیر همه دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدرش را نتوان یافت، که مقدور ضعیفی</p></div>
<div class="m2"><p>قدمش را نشناسی، که اسیر حدثانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز جمال تو مطرا، همه اعیان، همه اکوان</p></div>
<div class="m2"><p>ز جبین تو هویدا اثر «سبع مثانی »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخدا، خاک درت را بدو عالم نفروشم</p></div>
<div class="m2"><p>اگرم پیش بخوانی وگر از پیش برانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل قاسم ز شراب تو خرابست، چه گویم؟</p></div>
<div class="m2"><p>هم از آن جودت باده، هم ازان لطف اوانی</p></div></div>