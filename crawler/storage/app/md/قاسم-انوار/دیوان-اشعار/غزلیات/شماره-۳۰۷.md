---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>ای عشق توام در دو جهان مقصد و مقصود</p></div>
<div class="m2"><p>در طور عدم گشتن من وصل تو موجود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنمود بعشاق جهان سکه مهرت</p></div>
<div class="m2"><p>سیماب سرشک مژه بر روی زر اندود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سازم همه در مجلس غمهای تو چون چنگ</p></div>
<div class="m2"><p>سوزم همه بر آتش سودای تو چون عود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اقفال زر اندود ز ابواب سعادت</p></div>
<div class="m2"><p>کس جز بمفاتیح هدایات تو نگشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر نظری کن بدل غمزده یکبار</p></div>
<div class="m2"><p>کاندر غم هجران تو یک باره بفرسود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نی نی،چو ترا بر دل من هجر مرا دست</p></div>
<div class="m2"><p>با هجر تو دلشادم و با درد تو خشنود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون شد بتقاضای تو راضی دل قاسم</p></div>
<div class="m2"><p>سودش همه خسران شد و خسران همگی سود</p></div></div>