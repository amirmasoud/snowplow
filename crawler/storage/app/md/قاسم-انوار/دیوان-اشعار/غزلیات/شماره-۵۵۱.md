---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>ای یار، ندانم که چه رسمست و چه آیین؟</p></div>
<div class="m2"><p>گه ساقی جانهایی و گه محتسب دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عکسی بدل انداز از آن روی دل افروز</p></div>
<div class="m2"><p>روی تو چو ماهست و دلم آینه چین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>المنة الله که رسیدیم و چشیدیم</p></div>
<div class="m2"><p>از فاتحه فتح شما لذت آمین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی تو نروم جانب جنت بتکلف</p></div>
<div class="m2"><p>با صحبت تو فارغم از باغ و ریاحین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای باد، مکن از سر زلفش سخن آغاز</p></div>
<div class="m2"><p>زنهار! نجنبانی زنجیر مجانین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمت بگشایند، شوی واقف اسرار</p></div>
<div class="m2"><p>تا عین خدا بینی در عین خدا بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم دل و دین باخت بیاد تو و جان هم</p></div>
<div class="m2"><p>زین بیش چه باشد صفت عاشق مسکین؟</p></div></div>