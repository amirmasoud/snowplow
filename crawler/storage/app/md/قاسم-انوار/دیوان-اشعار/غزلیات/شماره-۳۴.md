---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>از حد گذشت قصه درد نهان ما</p></div>
<div class="m2"><p>ترسم که ناله فاش کند راز جان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی رسید ناله که از آسمان گذشت</p></div>
<div class="m2"><p>با او بهیچ جا نرسید این فغان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما گم شدیم در طلب حی لایموت</p></div>
<div class="m2"><p>از سالکان ره ندهد کس نشان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نادیده کرد هر نفس از لطف عیب پوش</p></div>
<div class="m2"><p>چندین جفا که دید ز ما دلستان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نی همدمی خوشست، که تا روز رستخیز</p></div>
<div class="m2"><p>با دوستان حدیث کند داستان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آتش تو منتظر آب رحمتیم</p></div>
<div class="m2"><p>ساقی، بیار جام می ارغوان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیحکمتی غریب وحدیثی عجیب نیست</p></div>
<div class="m2"><p>شادی یک زمان و غم جاودان ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>همت نگر، که از عالم فراغتند</p></div>
<div class="m2"><p>دردی کشان کوچه دیر مغان ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیار فکر کرد و ندانست شمه ای</p></div>
<div class="m2"><p>در لطف آن دهان خرد خرده دان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفتم که: قاسمی چه کسست؟ ای مراد جان</p></div>
<div class="m2"><p>گفتا که: رند زنده دل کس مدان ما</p></div></div>