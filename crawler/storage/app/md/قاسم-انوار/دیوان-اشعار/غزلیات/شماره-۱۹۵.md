---
title: >-
    شمارهٔ ۱۹۵
---
# شمارهٔ ۱۹۵

<div class="b" id="bn1"><div class="m1"><p>هرکه را نفی فراوان شد و اثباتی نیست</p></div>
<div class="m2"><p>گرچه بیناست، ولی صاحب مرآتی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای در راه بعزت نه و تحقیق بدان :</p></div>
<div class="m2"><p>قدمی نیست درین راه که آفاتی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سعی سودی نکند، جهد بجائی نرسد</p></div>
<div class="m2"><p>اگر از جانب محبوب مراعاتی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سید ملک وجودست بنی نوع بشر</p></div>
<div class="m2"><p>همچو انسان بجهان سید و ساداتی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موسیا، طور معانی بحقیقت عشقست</p></div>
<div class="m2"><p>که در آن طور ترا حاجت میقاتی نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ شب نیست که از درد تو مشتاقان را</p></div>
<div class="m2"><p>بر سر کوی غمت هی هی و هیهاتی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غرق دریای حیاتم، بخدا خوش حالم</p></div>
<div class="m2"><p>که مرا صورت تسبیح و عباداتی نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه مخمور شدم ساقی جان جامم داد</p></div>
<div class="m2"><p>خالی از شیوه تنبیه و کراماتی نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی، خرقه و تسبیح ندارد سودی</p></div>
<div class="m2"><p>گر ترا در دل و جان سوز و مناجاتی نیست</p></div></div>