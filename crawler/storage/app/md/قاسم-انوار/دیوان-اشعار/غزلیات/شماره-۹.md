---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>جگر پردرد و دل پرخون و جان سرمست و ناپروا</p></div>
<div class="m2"><p>شبم تاریک و مرکب لنگ و در سر مایه سودا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوای خود نمی دانم، درین اندیشه حیرانم</p></div>
<div class="m2"><p>بیا، ای ساقی باقی، بیار آن باده حمرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمعم پیش رویت من، گرم سر وا کنی از تن</p></div>
<div class="m2"><p>شوم پیش رخت روشن، چو شمع استاده پا برجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر هشیار و مستوری ز سر این سخن دوری</p></div>
<div class="m2"><p>میان رهروان کوری ز سر قرب «اوادنا»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین دریای بی پایان اگر گوهر بدست آری</p></div>
<div class="m2"><p>نگه دارش میان جان، مگو با هیچ کس عمدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا، ای یار روحانی،بگو اسمای انسانی</p></div>
<div class="m2"><p>چو میدانم که می دانی طریق علم «الاسما»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن از شرع و سنت گو به هشیاران صورت بین</p></div>
<div class="m2"><p>حدیث از عشق سرمد ران بمجنونان ناپروا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرا گویی: نشانی گو بما از عالم معنی</p></div>
<div class="m2"><p>خبر از بی خبر پرسی، نشان از بی نشانی ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>الا، ای عشق سلطان وش، که اجمالی و تفصیلی</p></div>
<div class="m2"><p>تویی حکمت، تویی قدرت، تویی زیباتر از زیبا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محمد را بمهمان بر، کنار خوان احسان بر</p></div>
<div class="m2"><p>شراب از جام سبحان بر، که «سبحان الذی اسرا»</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تو بنما روی میمون را، برافشان زلف میگون را</p></div>
<div class="m2"><p>که می یابم ز بوی او نسیم جنت الماوا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اگر از اسم قهاری تجلی میکند باری</p></div>
<div class="m2"><p>ببین، گر مرد اقراری، نشان طامة الکبرا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ز اول ذات را بشناس، پس اوصاف اللهی</p></div>
<div class="m2"><p>که این اوصاف اللهی فذلک باشد از منها</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>پس آنگه عالم آثار و افعالست پیوسته</p></div>
<div class="m2"><p>زهی حکمت، زهی قدرت، تعالی ربنا الاعلا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>عجب در حسن یکتایی، عجب موزون و زیبایی</p></div>
<div class="m2"><p>عجب شاه دلارایی، زهی یکتای بی همتا!</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز خوشید جمال او بهر وصفی که میگویم</p></div>
<div class="m2"><p>همه ذرات میگویند: «شهدنا» بعد «آمنا»</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بوحدانیت ذاتش گواهی میدهد هر دم</p></div>
<div class="m2"><p>اگر خوشید، اگر ذره، اگر اعلی، اگر دانا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>بهر سویی که گردیدم ترا دانستم و دیدم</p></div>
<div class="m2"><p>زهی محسن، زهی احسان، زهی ماه جهان آرا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان مستان عشق تو، زهی دستان عشق تو</p></div>
<div class="m2"><p>همه حیران عشق تو، اگر والی، اگر والا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بباید رفتن و خفتن، حدیث عشق بنهفتن</p></div>
<div class="m2"><p>کجا شاید سخن گفتن ز اوصافی که لاتحصا؟</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>بپیشت خسرو خاقان شود با خاک ره یکسان</p></div>
<div class="m2"><p>اگر یک گوهر رخشان بدست آری ازین دریا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بیا، ای جان خوش سودا، ببین نور تجلی را</p></div>
<div class="m2"><p>خطاب مستطابی را بگو: «لبیک ما اوحا»</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>مگو «ارنی » بترس از منع «لن » در عالم معنی</p></div>
<div class="m2"><p>که غرق بحر حیرت شد درین وادی دل موسا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دعا خوانان اورادی فراوانند در عالم</p></div>
<div class="m2"><p>ولی سری دگر باشد دعا را با دم علیا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گران جانان «لا» مردند در ظلمات تاریکی</p></div>
<div class="m2"><p>بسر بردند این ره را سبک روحان باستثنا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>ولی بشنو ز من پندی،که بیرون آیی از بندی</p></div>
<div class="m2"><p>گر از معنی خبر داری ممان در «لا» و در «الا»</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>تو در ظلمات تن ماندی، از آن خشنود و خوشحالی</p></div>
<div class="m2"><p>اگر دین و دلی داری نگویی سوز ماتم را</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>عجب وابسته جسمی، مسما نیستی، اسمی</p></div>
<div class="m2"><p>چو اهل عادت و رسمی چه گویم با تو، ای دانا؟</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>بصورت آدم و حوا بغایت روشنست، اما</p></div>
<div class="m2"><p>بمعنی عقل و نفس کل مدان جز آدم و حوا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگر هشیار و بیداری، ببین در قدرت باری</p></div>
<div class="m2"><p>هزاران آدم وحوا زنا پیدا شده پیدا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>الا، ای احمد مرسل، چراغ مسجد و منبر</p></div>
<div class="m2"><p>تویی سید، تویی سرور، تویی مقصود از استقصا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>شریعت از تو روشن شد، طریقت ها مبرهن شد</p></div>
<div class="m2"><p>حقیقت ها معین شد، زهی یاسین، زهی طاها!</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>تویی مؤمن، تویی ایمان، تویی سرچشمه حیوان</p></div>
<div class="m2"><p>تویی سلطان جاویدان، تویی مقصد، تویی اقصا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>تو داری مقصد اقصی، تو داری قرب «اوادنی »</p></div>
<div class="m2"><p>همه دردند و تو صافی، همه صافند و تو اصفا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>زهر کامل که پیش آید کمالات تو بیش آید</p></div>
<div class="m2"><p>مثال کاملان با تو مثال پشه با عنقا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بیا، قاسم، چه میگویی، چه میپوئی، چه میجویی؟</p></div>
<div class="m2"><p>اگر امروز با اویی مگو افسانه فردا</p></div></div>