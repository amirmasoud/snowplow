---
title: >-
    شمارهٔ ۴۶۲
---
# شمارهٔ ۴۶۲

<div class="b" id="bn1"><div class="m1"><p>ما در جهان کون برای تو آمدیم</p></div>
<div class="m2"><p>بهر تو آمدیم و برای تو آمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تنگنای خاک بماندیم عمرها</p></div>
<div class="m2"><p>در تنگنای غم بفضای تو آمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون کرکسان نشیمن ماخاک توده بود</p></div>
<div class="m2"><p>در ملک جان بفرهمای تو آمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما باز وحدتیم وز کهسار حضرتیم</p></div>
<div class="m2"><p>اکنون بدست شه بصدای تو آمدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوست لم یزل بدلم میرسد ندا :</p></div>
<div class="m2"><p>ما لایزال درد و دوای تو آمدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را همین بسست که در ملکت وجود</p></div>
<div class="m2"><p>فخر دو عالمیم و گدای تو آمدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عهدی که داشتیم ز روز ازل بدوست</p></div>
<div class="m2"><p>در صحن کن فکان بوفای تو آمدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای یار نازنین، که تو گشتی فدای ما</p></div>
<div class="m2"><p>ما نیز در دو کون فدای تو آمدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در حال زار ما نظری کن، که نادریم</p></div>
<div class="m2"><p>عین تو آمدیم و سوای تو آمدیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سر ولای تست نگهدار جان ما</p></div>
<div class="m2"><p>ما در جهان بسر ولای تو آمدیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>از ملک لایزال بریدیم، قاسمی</p></div>
<div class="m2"><p>در ملک لم یزل بهوای تو آمدیم</p></div></div>