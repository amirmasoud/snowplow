---
title: >-
    شمارهٔ ۳۳۸
---
# شمارهٔ ۳۳۸

<div class="b" id="bn1"><div class="m1"><p>صبح ازل ز مشرق انوار بردمید</p></div>
<div class="m2"><p>از نور روی یار به ما لمعه‌ای رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ایام هجر یار ز اندازه درگذشت</p></div>
<div class="m2"><p>صبحی ز نو برآمد و روزی ز نو دمید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جایگه که نور رخ یار جلوه کرد</p></div>
<div class="m2"><p>آنجا مرید راه جنیدست و بایزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل بیا و قصه هجرانیان مگو</p></div>
<div class="m2"><p>همراه عشق شو، که مرا دست و هم مرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرجا که جرعه‌نوش خدا باده‌ای خورد</p></div>
<div class="m2"><p>از کاینات بانگ برآید که: «بر مزید»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل در حجاب پرده پندار مانده بود</p></div>
<div class="m2"><p>عشقت رسید و پرده پندار ما درید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم به آرزوی تو رفت از جهان برون</p></div>
<div class="m2"><p>واحسرتا که یک گل از این بوستان نچید!</p></div></div>