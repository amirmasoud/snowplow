---
title: >-
    شمارهٔ ۴۲۲
---
# شمارهٔ ۴۲۲

<div class="b" id="bn1"><div class="m1"><p>بهیچ یار و دیاری اگر چه دل ننهادم</p></div>
<div class="m2"><p>ولیک عاقبة الامر دل بمهر تو دادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دری ز وصل گشادی، بروی من نظری کن</p></div>
<div class="m2"><p>بیمن دولت وصل ببین که در چه گشادم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیار جام مصفا، مگو حکایت فردا</p></div>
<div class="m2"><p>نفس قبول کن از ما، دم این دمست، دم این دم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جور روی مگردان، که در طریقه رندان</p></div>
<div class="m2"><p>حدیث عشق و سلامت نبوده است مسلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رموز عشق بیان کن بپیش ما، که نگویند</p></div>
<div class="m2"><p>مجردان طریقت حدیث عالم و آدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طریق عشق و مودت ره فناست، فنا شو</p></div>
<div class="m2"><p>سخن تمام شد این جا، «اذا اصبت فالزم »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز جذب خاطر قاسم رقیب بهره ندارد</p></div>
<div class="m2"><p>طریق صید نداند سگی که نیست معلم</p></div></div>