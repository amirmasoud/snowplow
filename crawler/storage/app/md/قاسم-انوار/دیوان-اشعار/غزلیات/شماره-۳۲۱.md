---
title: >-
    شمارهٔ ۳۲۱
---
# شمارهٔ ۳۲۱

<div class="b" id="bn1"><div class="m1"><p>فرو ریختی باز در جام جود</p></div>
<div class="m2"><p>بعمدا شرابی که هوشم ربود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین جام تا جرعه ای خورده ام</p></div>
<div class="m2"><p>سرم در سجودست و جان در شهود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین جام دیدم بعین الیقین</p></div>
<div class="m2"><p>نمودست غیر تو، یعنی نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه غیر و کجا غیر و کو نقش غیر؟</p></div>
<div class="m2"><p>«سوی الله والله مافی الوجود»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم سوخت در عشق و من ساختم</p></div>
<div class="m2"><p>درین سوختن ساختن داشت سود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببین سوز و سازش که چون ساختست</p></div>
<div class="m2"><p>تنم را چو چنگ و دلم را چو عود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشادست قاسم زبان را به لاف</p></div>
<div class="m2"><p>چو ساقی سر خم وحدت گشود</p></div></div>