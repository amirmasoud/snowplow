---
title: >-
    شمارهٔ ۴۹۶
---
# شمارهٔ ۴۹۶

<div class="b" id="bn1"><div class="m1"><p>من بجانان زنده ام، گر باز دانی این سخن</p></div>
<div class="m2"><p>عاشقی باشی، یقین، از عاشقان ذوالمنن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون شراب ناب عرفان نوش کردی: جم شدی</p></div>
<div class="m2"><p>در طریقت محو باش و از حقیقت دم مزن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چونکه تو خود را شناسی، از در انصاف باش</p></div>
<div class="m2"><p>گر بگویندت: سر مویی نداری، مو مکن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوست گوید: با توام من، چون نمی بینی مرا؟</p></div>
<div class="m2"><p>گویم: ای جان و جهان از پرده های ما و من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جمله ذرات جهان را رو بدان روی نکوست</p></div>
<div class="m2"><p>بنده آن روی زیبا هم حسن، هم بوالحسن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله در تسبیح و در تقدیس مست حیرتند</p></div>
<div class="m2"><p>صد هزاران لاله سیراب از صحن چمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان عارف در شهود حضرت حق الیقین</p></div>
<div class="m2"><p>جان عاقل در میان عقده تخمین و ظن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سر توحید ازل بشنو ز «حی لایموت »</p></div>
<div class="m2"><p>مدعی گر عاقلی جان پرور، این جا جان مکن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی از وصل جانان دولت جاوید یافت</p></div>
<div class="m2"><p>چون میسر گشت جان را خلوت اندر انجمن</p></div></div>