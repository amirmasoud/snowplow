---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>ساقی مرا ز باده ناب مغانه داد</p></div>
<div class="m2"><p>دردی درد داد ولی در میانه داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد صباح کژمژ و خرم همی رود</p></div>
<div class="m2"><p>ساقی مگر که رطل گران شبانه داد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در کوی عشق یار،که آن جای جای نیست</p></div>
<div class="m2"><p>مرغ دل مرا بکرم آشیانه داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان را خبر نبود ز نام و نشان عشق</p></div>
<div class="m2"><p>این عشق دل فروز تو جان را نشانه داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بس خوشدلند اهل زمین و زمان مدام</p></div>
<div class="m2"><p>زان باده ای که عشق تو اندر زمانه داد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی کار و کارخانه بد این دل میان دهر</p></div>
<div class="m2"><p>سلطان عشق از کرم این کارخانه داد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم ز درد دوست از آن مست و شاد شد</p></div>
<div class="m2"><p>کین موهبت به زمره کروبیان نداد</p></div></div>