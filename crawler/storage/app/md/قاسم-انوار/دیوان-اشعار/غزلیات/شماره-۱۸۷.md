---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>شکی نماند که جز دوست در جهان کس نیست</p></div>
<div class="m2"><p>معینست که پیدا و در نهان کس نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار بار گواهی دهند ملک و ملک</p></div>
<div class="m2"><p>که غیر دوست درین عرض کن فکان کس نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بغیر دلبر ما، کآفتاب اعیانست</p></div>
<div class="m2"><p>دگر بهر دو جهان مخفی و عیان کس نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز راه خدا اندکی خبر داری</p></div>
<div class="m2"><p>معینست که جز دوست در میان کس نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدیدهای عیان دیده است دیده دل</p></div>
<div class="m2"><p>که غیر دوست درین ملک جاودان کس نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مسافران طریقت، که راه حق رفتند</p></div>
<div class="m2"><p>نشان دهند که جز ذات بی نشان کس نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جهانیان همه دانسته اند و قاسم هم</p></div>
<div class="m2"><p>که غیر یار گرامی درین جهان کس نیست</p></div></div>