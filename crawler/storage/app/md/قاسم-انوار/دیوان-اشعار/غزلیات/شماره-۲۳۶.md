---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>دلم از جور تو بسیار شکایت دارد</p></div>
<div class="m2"><p>وقت آن شد که شکایت بحکایت آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی بود که اندر هوست جان می داد</p></div>
<div class="m2"><p>وقت آن شد که بدیدار تو جان بسپارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آرزوی تو، که صد جان گرامی ارزد</p></div>
<div class="m2"><p>در زمین دل من تخم وفا می کارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما همه منتظرانیم، ولی گه گاهی</p></div>
<div class="m2"><p>باد می آید و ما را خبری می آرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکجادرهمه عالم صفت لطفی هست</p></div>
<div class="m2"><p>چونکه نیکونگری روی به انسان دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوشه دامن این زاهد ما تر نشود</p></div>
<div class="m2"><p>آسمان گر همه باران هدایت بارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی در ره جانان سر و جان باخته است</p></div>
<div class="m2"><p>غیرآن زاهد بیچاره که سر می خارد</p></div></div>