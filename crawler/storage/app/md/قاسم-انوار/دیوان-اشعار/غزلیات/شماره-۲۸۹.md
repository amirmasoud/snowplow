---
title: >-
    شمارهٔ ۲۸۹
---
# شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>دلدار من از خانه ببازار برآمد</p></div>
<div class="m2"><p>ناگه بسر کوچه خمار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلبانگ «تعالی » بشنیدند روانها</p></div>
<div class="m2"><p>صد نعره ز تسبیح و ز زنار برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دعوی بچه تیره فرو رفت بخواری</p></div>
<div class="m2"><p>معنی ز ته که گل فخار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم همه روشن شد از آن نور بیکبار</p></div>
<div class="m2"><p>گفتند که: آن دلبر عیار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتیم: چو خورشید جمال تو عیانست</p></div>
<div class="m2"><p>«صدق » ز دل مؤمن وکفار برآمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم که: تویی، غیر تو کس نیست بعالم</p></div>
<div class="m2"><p>این نعره هم از طبله عطار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خورشید رسیدست ز ایمن بدل من</p></div>
<div class="m2"><p>تا از دل قاسم دم اقرار برآمد</p></div></div>