---
title: >-
    شمارهٔ ۴۴۱
---
# شمارهٔ ۴۴۱

<div class="b" id="bn1"><div class="m1"><p>باده می نوشم و سودای تو در سر دارم</p></div>
<div class="m2"><p>آیت مصحف سودای تو از بردارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زرعم اینست که کشتم بهمه عمر عزیز</p></div>
<div class="m2"><p>من ندانم که ازین کشته چه بر بردارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل و جانم بچه کار آید امروز؟ که من</p></div>
<div class="m2"><p>دل و جان شیفته زلف معنبر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم سرم در سر کار تو رود آخر کار</p></div>
<div class="m2"><p>با خود این قاعده دیریست مقرر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحم کن بر دل عشاق ز الطاف کریم</p></div>
<div class="m2"><p>خاصه من خسته، که معشوق ستمگر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق و بیماری و درویشی و محنت بردن</p></div>
<div class="m2"><p>از غم عشق تو این جمله میسر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی را نظری کن، که دل از دست برفت</p></div>
<div class="m2"><p>دل من آتش غم، سینه چو مجمر دارم</p></div></div>