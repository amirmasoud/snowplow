---
title: >-
    شمارهٔ ۳۳۱
---
# شمارهٔ ۳۳۱

<div class="b" id="bn1"><div class="m1"><p>گر با تو دمی محرم اسرار توان بود</p></div>
<div class="m2"><p>بر ملک و ملک فایض انوار توان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با ابروی تو محرم محراب توان شد</p></div>
<div class="m2"><p>با چشم خوشت ساکن خمار توان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با روی تو برمذهب اسلام توان زیست</p></div>
<div class="m2"><p>با زلف تو در حلقه کفار توان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با شحنه عشقت می توحید توان خورد</p></div>
<div class="m2"><p>با محتسب حکم تو هشیار توان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بر سر بیمار خود آیی بعیادت</p></div>
<div class="m2"><p>صد سال بامید تو بیمار توان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک آه، که از جان بهوای تو بر آید</p></div>
<div class="m2"><p>حقا که بکونین خریدار توان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با حفظ تو در دوزخ سوزان بتوان زیست</p></div>
<div class="m2"><p>با یاری تو رافع اغیار توان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن بار که از شدت او کوه ابا کرد</p></div>
<div class="m2"><p>با قوت تو حامل آن بار توان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در بادیه محنت هجران شب تاریک</p></div>
<div class="m2"><p>با نور رخت قافله سالار توان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>با لمعه تو نیر خورشید توان گشت</p></div>
<div class="m2"><p>با قطره تو قلزم زخار توان بود</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر بر سر بازار جهان جلوه گر آیی</p></div>
<div class="m2"><p>قلاش صفت بر سر بازار توان بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر وعده دیدار تو در صومعه باشد</p></div>
<div class="m2"><p>تا روز ابد در پس دیوار توان بود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با حکمت تو لذت اسرار توان یافت</p></div>
<div class="m2"><p>با جذبه تو سالک اطوار توان بود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با معرفت حسن تو معروف توان گشت</p></div>
<div class="m2"><p>با نقد غمت مالک دینار توان بود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مشکین نفس از شوق تو شد قاسمی، آری</p></div>
<div class="m2"><p>با طیب موالات تو عطار توان بود</p></div></div>