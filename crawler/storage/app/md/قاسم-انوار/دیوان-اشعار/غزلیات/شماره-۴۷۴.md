---
title: >-
    شمارهٔ ۴۷۴
---
# شمارهٔ ۴۷۴

<div class="b" id="bn1"><div class="m1"><p>ماییم که چون باده گل رنگ بجوشیم</p></div>
<div class="m2"><p>گه باده بنوشیم و گهی باده فروشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صحبت عقل این دل بیچاره ملولست</p></div>
<div class="m2"><p>بنشین نفسی، تا قدحی باده بنوشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خرقه ما آلت عقل آمد و تزویر</p></div>
<div class="m2"><p>آلودگی خرقه بزنار بپوشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از حلقه ما دور مشو، ای دل و دینم</p></div>
<div class="m2"><p>ما حلقه بگوشان ترا حلقه بگوشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با عشق روانیم و دوانیم درین راه</p></div>
<div class="m2"><p>از روز ازل تا به ابد دوش بدوشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گویند که: این راه ندارد سر و پایان</p></div>
<div class="m2"><p>هرچند که ما بی سر و پاییم بکوشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، بنگر حالت رندان خرابات</p></div>
<div class="m2"><p>در مجلس مستان همه گویای خموشیم</p></div></div>