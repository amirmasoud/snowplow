---
title: >-
    شمارهٔ ۵۳۵
---
# شمارهٔ ۵۳۵

<div class="b" id="bn1"><div class="m1"><p>«حصل ما فی الصدور» لذت جان یافتن</p></div>
<div class="m2"><p>«بعثر ما فی القبور» گنج نهان یافتن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یافت عطای خداست، یافت سبیل هداست</p></div>
<div class="m2"><p>یافت طریق فناست، گر بتوان یافتن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دولت جاوید چیست؟ غایت امید چیست؟</p></div>
<div class="m2"><p>درد ترا هر زمان در دل و جان یافتن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلسه تو «قم » شود، بحر چو قلزم شود</p></div>
<div class="m2"><p>نور جمال ازل وقت عیان یافتن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار تو نیکو کند، یار بتو خو کند</p></div>
<div class="m2"><p>جمله صفات کمال در همگان یافتن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لذت جام ازل در همه جانها رسید</p></div>
<div class="m2"><p>لیک کجا هرکسی رطل گران یافتن؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم هجران زده لذت دیدار یافت</p></div>
<div class="m2"><p>همچو مه عید را در رمضان یافتن</p></div></div>