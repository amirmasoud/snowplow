---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>پیش از بنای مدرسه و دیر سومنات</p></div>
<div class="m2"><p>ما با تو بوده ایم در اطوار کاینات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اندر میان حکایت پیغام در گذشت</p></div>
<div class="m2"><p>چون با منی همیشه چه حاجت بمرسلات؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازما خلاف دوست نیاید،که با حبیب</p></div>
<div class="m2"><p>همراه بوده ایم در انواع واردات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنهار،ذکر غیر دگر برزبان مران</p></div>
<div class="m2"><p>صاحبدلان بغیر نکردندالتفات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هشیار شرط نیست که باشی،که در جهان</p></div>
<div class="m2"><p>هر ذره از ذراری کونند ساقیات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد،مکن مبالغه با ما و این بدان</p></div>
<div class="m2"><p>بر جنس طیبین حلالست طیبات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم،خموش باش و عنان سخن بکش</p></div>
<div class="m2"><p>تا پیر عشق با تو نگوید ز باقیات</p></div></div>