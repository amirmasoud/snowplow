---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>زهی شوق و زهی شوق، زهی عشق و تمنا!</p></div>
<div class="m2"><p>زهی عشق جهانسوز، زهی حسن و تولا!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی لطف و کرامت، زهی خوش قد و قامت</p></div>
<div class="m2"><p>زهی روز قیامت، زهی نور تجلا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زهی یار و زهی یار، و زهی مونس احرار</p></div>
<div class="m2"><p>زهی معدن اسرار، زهی منصب اعلا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زهی نور و زهی نور، زهی رایت منصور</p></div>
<div class="m2"><p>زهی آیت مشهور، تقدس و تعالا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زهی طالع مسعود، زهی حامد و محمود</p></div>
<div class="m2"><p>زهی واجد و موجود، زهی حضرت والا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهی ذات معلا، زهی نور مزکی</p></div>
<div class="m2"><p>زهی روی مصفی، زهی مولی و مولا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زهی فتنه برانگیز، زهی شهد شکرریز</p></div>
<div class="m2"><p>زهی روی دلاویز، زهی زلف سمن سا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سوز تو کبابیم، سر از سوز نتابیم</p></div>
<div class="m2"><p>همه مست خرابیم، از آن جام مصفا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گهی فتنه جانی، گهی شور جهانی</p></div>
<div class="m2"><p>گهی امن و امانی، گهی کان خطرها</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گهی قاضی شهری، گهی شحنه قهری</p></div>
<div class="m2"><p>گهی چشمه زهری، گهی موجی و دریا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تویی کاشف اسرار، تویی قاسم انوار</p></div>
<div class="m2"><p>تویی سالک اطوار، تویی اسم و مسما</p></div></div>