---
title: >-
    شمارهٔ ۴۴۹
---
# شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>خیالست این که: من بی یار باشم</p></div>
<div class="m2"><p>محالست این که: بی دلدار باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نباشم یک زمان از یار خالی</p></div>
<div class="m2"><p>اگر در جنت، ار در نار باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی کان دم جمال یار بینم</p></div>
<div class="m2"><p>ز عمر خویش برخوردار باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم قبله ای جز روی آن یار</p></div>
<div class="m2"><p>اگر در کعبه و خمار باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو فیضی نیست جان را در دو عالم</p></div>
<div class="m2"><p>چه قید که گل فخار باشم؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من آن دم از جهان آزاد گردم</p></div>
<div class="m2"><p>که صید واحد قهار باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز گستاخی که قاسم کرد در عشق</p></div>
<div class="m2"><p>ز شب تا روز در زنهار باشم</p></div></div>