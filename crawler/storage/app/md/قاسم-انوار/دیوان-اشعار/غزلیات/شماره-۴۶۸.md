---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>هزاران بحر در دردانه دیدیم</p></div>
<div class="m2"><p>درخت کون را در دانه دیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سحرگاهی بدان حضرت رسیدیم</p></div>
<div class="m2"><p>بر آن در حاجب و دربان ندیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حجابات جهان درهم شکستیم</p></div>
<div class="m2"><p>همه تقلید را افسانه دیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظهور آفتاب طلعت دوست</p></div>
<div class="m2"><p>میان کعبه و بت خانه دیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو می خانه مقام شور و مستیست</p></div>
<div class="m2"><p>سریر سلطنت می خانه دیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذر کردیم بر کوی ملامت</p></div>
<div class="m2"><p>همه عاشق، همه فرزانه دیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قاسم در جهان جان نظر کرد</p></div>
<div class="m2"><p>یکی شمع و همه پروانه دیدیم</p></div></div>