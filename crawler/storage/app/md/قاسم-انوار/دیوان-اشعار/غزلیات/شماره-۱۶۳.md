---
title: >-
    شمارهٔ ۱۶۳
---
# شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>رخسار تو چون آینه صورت و معنیست</p></div>
<div class="m2"><p>در صبح جبینت همه انوار تجلیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم جذبه او بود که دل مست لقا شد</p></div>
<div class="m2"><p>مجنون چه کند؟ کین کشش از جانب لیلیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیوسته ز سودای تو مستیم و خرابیم</p></div>
<div class="m2"><p>ما را بتو صد گونه تولی وتمنیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرجام که از دست تو آید همه نوشست</p></div>
<div class="m2"><p>این دور نه جورست، که در دور تو اولیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جلوه دیدار تو عشاق تو دیدند</p></div>
<div class="m2"><p>از هر طرفی بانگ تقدس و تعالیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما رو بتو داریم، که مرآت شریفی</p></div>
<div class="m2"><p>در روی تو خود قاعده روی و ریا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دل، که چو قاسم بجمالت نشود شاد</p></div>
<div class="m2"><p>در قاعده نشائه او صورت دعویست</p></div></div>