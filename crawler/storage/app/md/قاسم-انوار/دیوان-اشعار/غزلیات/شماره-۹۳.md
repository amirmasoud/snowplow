---
title: >-
    شمارهٔ ۹۳
---
# شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>بازم نمکی بر جگر ریش رسیدست</p></div>
<div class="m2"><p>صد گونه بلا بر من درویش رسیدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من ناله ز بیگانه ندارم، که دلم را</p></div>
<div class="m2"><p>هر غم که رسیدست هم از خویش رسیدست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درد تو بهرکس نرسیدست ولیکن</p></div>
<div class="m2"><p>المنة لله که مرا بیش رسیدست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیشم همه عشقست و نیاید بصفت راست</p></div>
<div class="m2"><p>تیری که مرا بر دل از آن کیش رسیدست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش دو جهان را همگی کرد فراموش</p></div>
<div class="m2"><p>تا بر دل من لذت آن نیش رسیدست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عشق جهان سوز، کجایی؟ که دلم را</p></div>
<div class="m2"><p>صد واقعه از عقل بد اندیش رسیدست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر ناله کند قاسم بیدل، مکنش عیب</p></div>
<div class="m2"><p>پیداست از آن ناله که دردیش رسیدست</p></div></div>