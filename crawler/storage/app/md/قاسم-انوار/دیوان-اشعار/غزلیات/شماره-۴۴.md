---
title: >-
    شمارهٔ ۴۴
---
# شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>سخنی می رود بوجه صواب</p></div>
<div class="m2"><p>همه قشرند و دوست لب لباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دوست در پرده می نماید روی</p></div>
<div class="m2"><p>دل ما چاک می زند جلباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما و دلدار خوش نشسته بهم</p></div>
<div class="m2"><p>«اغلق الباب، ایها البواب »</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خدا رحمتیست پنهانی</p></div>
<div class="m2"><p>دل بیدار و دیده بی خواب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه آید از آن حبیب قلوب</p></div>
<div class="m2"><p>جمله وحیست، یا ورای حجاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شادی و دیدن دلدار</p></div>
<div class="m2"><p>بگشای، ای مفتح الابواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، این مقلدان کورند</p></div>
<div class="m2"><p>ره نبینند در خطاب و صواب</p></div></div>