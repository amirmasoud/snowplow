---
title: >-
    شمارهٔ ۳۲۴
---
# شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>این عشق و مودت اثر لطف خدا بود</p></div>
<div class="m2"><p>وین جمله عنایت نه باندازه ما بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جوری، که ز تو بر دل غمدیده ما رفت</p></div>
<div class="m2"><p>بر شکل بلا بود ولی عین عطا بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از روز ازل عاشق و شوریده و مستیم</p></div>
<div class="m2"><p>این نیز هم از سابقه لط ف شما بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در حال «اناالحق » زد و شد بر سر آن دار</p></div>
<div class="m2"><p>منصور که سر حلقه مستان خدا بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر یک دو سه گامی بدویدند و برفتند</p></div>
<div class="m2"><p>گر مست خدا بود و گر مرد هوا بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر قصه، که بینید درین راه خطرناک</p></div>
<div class="m2"><p>زنهار مپرسید که: چونست و چرا بود؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دایم دل قاسم بکرمهای تو شاد است</p></div>
<div class="m2"><p>شان تو همیشه کرم و صدق و صفا بود</p></div></div>