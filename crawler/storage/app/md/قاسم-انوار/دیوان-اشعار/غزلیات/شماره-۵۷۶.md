---
title: >-
    شمارهٔ ۵۷۶
---
# شمارهٔ ۵۷۶

<div class="b" id="bn1"><div class="m1"><p>«عز من قائل » چه گفت اله؟</p></div>
<div class="m2"><p>«قوله: لا اله الا الله »</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفت: در کون کاینا ما کان</p></div>
<div class="m2"><p>همه بر وحدت منند گواه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«لا» چه باشد؟ نهنگ بحر محیط</p></div>
<div class="m2"><p>چیست «الا»؟ جمال عزت و جاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>«لا» و «الا» چو جمع شد با هم</p></div>
<div class="m2"><p>شد عیان سر مولی و مولاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هله! ای عشق، جرعه ای دیگر</p></div>
<div class="m2"><p>که جهان را بتست پشت و پناه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه مستان تو، عقول و نفوس</p></div>
<div class="m2"><p>همه حیران تو، سپید و سیاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی را بلطف خود بنواز</p></div>
<div class="m2"><p>«اعتمادی علیک، یا مثواه »!</p></div></div>