---
title: >-
    شمارهٔ ۳۸۹
---
# شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>با زلف و رخت مست مدامیم شب و روز</p></div>
<div class="m2"><p>ای ماه وفا پیشه و ای شاه دل افروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی شاهد و شمعیم درین وادی ایمن</p></div>
<div class="m2"><p>شاهد، بنما چهره و آن شمع بر افروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را ز ازل جام می عشق تو دادند</p></div>
<div class="m2"><p>از باده پارینه بدان مستی امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما خرقه ناموس بصد پاره دریدیم</p></div>
<div class="m2"><p>زاهد، تو برو خرقه تزویر و ریا دوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امروز که مهمان منست آن دل و دلدار</p></div>
<div class="m2"><p>ای چنگ، دمی ساز کن، ای عود، همی سوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>امید چنانست دلم را بخداوند</p></div>
<div class="m2"><p>در پیش رخت چاک زنم خرقه پیروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>المنة لله که زمستان بسر آمد</p></div>
<div class="m2"><p>هنگام بهار آمد و شد نکهت نوروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زاهد دهدم توبه ز روی تو، چه گویم؟</p></div>
<div class="m2"><p>از قول بداندیش و حکایات بدآموز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشقت بدل عاشق آشفته قاسم</p></div>
<div class="m2"><p>از بخت بلند آمد و از طالع فیروز</p></div></div>