---
title: >-
    شمارهٔ ۵۲۰
---
# شمارهٔ ۵۲۰

<div class="b" id="bn1"><div class="m1"><p>عشق و معشوق و عاشق حیران</p></div>
<div class="m2"><p>هر سه یکیست در طریق عنان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سه یکیست در طریقت عشق</p></div>
<div class="m2"><p>عشق و معشوق و عاشقی همه دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم بینا کجاست؟ تا بیند</p></div>
<div class="m2"><p>عین آن یار در همه اعیان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک سخن را قبول کن از من</p></div>
<div class="m2"><p>هوشیاری، مرو بر مستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر گذاری کنی بدان مجلس</p></div>
<div class="m2"><p>همه جا روح و راحت و ریحان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنه قایم شدست در عالم</p></div>
<div class="m2"><p>بنشین، دوست، فتنه را بنشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توسن قاسمی عجب تندست</p></div>
<div class="m2"><p>لاجرم در کشیده ایم عنان</p></div></div>