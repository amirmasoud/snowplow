---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>ای نور دل و دیده وای زبده اعیان</p></div>
<div class="m2"><p>باری گذری کن بسر چشمه حیوان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او آب حیاتست، ازو چاره نباشد</p></div>
<div class="m2"><p>او قبله جانست، ازو روی مگردان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میل تو بمستیست، زهی لذت مستی!</p></div>
<div class="m2"><p>چشمان تو مستند، خوشا حالت مستان!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا روی تو دیدم ز سر شوق و مودت</p></div>
<div class="m2"><p>تا عرش رسانید دلم قهقه جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق تو خمارست و لبت ساقی جانها</p></div>
<div class="m2"><p>زلفت شب قدرست و رخت شمع شبستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عقل مگویید حکایت ز فراغت</p></div>
<div class="m2"><p>از عشق مپرسید حدیث سر و سامان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشغولی هر دل بهوایی و ولایی</p></div>
<div class="m2"><p>در حسن جهانگیر تو قاسم شده حیران</p></div></div>