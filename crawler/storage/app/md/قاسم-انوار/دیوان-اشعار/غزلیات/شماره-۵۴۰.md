---
title: >-
    شمارهٔ ۵۴۰
---
# شمارهٔ ۵۴۰

<div class="b" id="bn1"><div class="m1"><p>نیم مستان ترا سرگشته چون پرگار کن</p></div>
<div class="m2"><p>آخر ای جان و جهان، جامی دگر در کار کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فتنه در خواب قیامت خفته است، ای جان، دگر</p></div>
<div class="m2"><p>زلف مشکین برفشان و فتنه را بیدار کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوشیاران را ز جام شوق خود سرمست ساز</p></div>
<div class="m2"><p>هر کرا بد مست بینی ساعتی هشیار کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل جز وی رهزن راهست، ازو غافل مشو</p></div>
<div class="m2"><p>یا بترک عقل گیر و یا بترک یار کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هان و هان! تا هستی خود را نبینی در میان</p></div>
<div class="m2"><p>گر ببینی، خود گناهی، از خود استغفار کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خدا خوانی مکن اسرار عرفان فاش فاش</p></div>
<div class="m2"><p>ور خدا دانی، بیا، اسرار حق اظهار کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، جای مدارا نیست با کوران راه</p></div>
<div class="m2"><p>گر ببینی منکر حق را تو هم انکار کن</p></div></div>