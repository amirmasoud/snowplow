---
title: >-
    شمارهٔ ۶۲۰
---
# شمارهٔ ۶۲۰

<div class="b" id="bn1"><div class="m1"><p>ای دوست، بگو باری تا: عزم کجا داری؟</p></div>
<div class="m2"><p>سرمست و خرامانی انگیز بلا داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هردم بدگر صورت ظاهر شوی، ای دولت</p></div>
<div class="m2"><p>گه راه صفا گیری، گه تیغ جفا داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه رای کنی، گه رو، گه های کنی، گه هو</p></div>
<div class="m2"><p>این مسئله را برگو: در هوی چه ها داری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای صاحب بحر و بر، وی طالب نیک اختر</p></div>
<div class="m2"><p>بگذار سر و افسر، هان! گر سر ما داری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرجا من و ما باشد، از جنس فنا باشد</p></div>
<div class="m2"><p>فانی نشود هرگز، این عشق و هواداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای مایه مستوری، ای چهره مهجوری</p></div>
<div class="m2"><p>خوش قابل و مقبولی، گر قبله خدا داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر لحظه کند القا، با ابر دل دریا :</p></div>
<div class="m2"><p>از ما شده ای پیدا، هم روی بما داری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بگذار حکایت ها، مجنون شو و ناپروا</p></div>
<div class="m2"><p>کار تو شود زیبا، گر رو بفنا داری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همراه تو شد جانان، هرجا که روی، ای جان</p></div>
<div class="m2"><p>گر قصد سمک داری، گر رو بسما داری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>بگذار تصرفها، تا چند تکلفها</p></div>
<div class="m2"><p>بگذار ره سودا، گر رو بصفا داری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من قاسم حیرانم، بس بی سر و سامانم</p></div>
<div class="m2"><p>در آتش هجرانم آخر تو روا داری؟</p></div></div>