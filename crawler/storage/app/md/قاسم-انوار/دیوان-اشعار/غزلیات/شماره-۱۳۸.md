---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>بوی جان میآید از باد صبا، این بو چه بوست؟</p></div>
<div class="m2"><p>مشک را این حد نباشد، نکهت گیسوی اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست بو؟ واقف شدن از سر محبوب ازل</p></div>
<div class="m2"><p>آنکه چون آیینه با ذرات عالم روبروست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جمله عالم بما پیداست، ما آیینه ایم</p></div>
<div class="m2"><p>گر نباشد آینه، شاهد چه داند کونکوست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده تا با جان ما واصل نگردد مست نیست</p></div>
<div class="m2"><p>باده را مستی ز جان ما، نه از جام و سبوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حد این سر نیست او را سجده کردن، لاجرم</p></div>
<div class="m2"><p>سر بپیش افکنده ام، بیچاره من، از شرم دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من ز غمهای کهن هرگز ننالم، چون ترا</p></div>
<div class="m2"><p>دولت تشریف غم ساعت بساعت، نوبنوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان بپیش دوست دادن دولتی باشد عظیم</p></div>
<div class="m2"><p>قاسمی را در دو عالم خود همین یک آرزوست</p></div></div>