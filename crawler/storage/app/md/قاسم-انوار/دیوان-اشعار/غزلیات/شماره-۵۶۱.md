---
title: >-
    شمارهٔ ۵۶۱
---
# شمارهٔ ۵۶۱

<div class="b" id="bn1"><div class="m1"><p>باده کهنه گیر و شیشه نو</p></div>
<div class="m2"><p>دلق و تسبیح کن بباده گرو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ندانی تو قدر شاهد و می</p></div>
<div class="m2"><p>سر خود گیر، ازین دیار برو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خیال حبیب رهبر نیست</p></div>
<div class="m2"><p>بخیالات خویش غره مشو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق اگر نیست همره تو،چه سود</p></div>
<div class="m2"><p>گنج قارون و ملک کیخسرو؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقانیم و کشته معشوق</p></div>
<div class="m2"><p>همه عالم بپیش ما بد و جو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه را کشته ای همان دروی</p></div>
<div class="m2"><p>نوبت حاصلست و وقت درو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی،نوبت وصال رسید</p></div>
<div class="m2"><p>بگریز از فراق و دو، دو، دو</p></div></div>