---
title: >-
    شمارهٔ ۱۹۳
---
# شمارهٔ ۱۹۳

<div class="b" id="bn1"><div class="m1"><p>بپیش مردم نادیده این سخن شینیست</p></div>
<div class="m2"><p>که غیر دلبر ما در جهان دگر شی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال باطل از آنست در دماغ فقیه</p></div>
<div class="m2"><p>که در مزاج دلش بوی نشأئه می نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار مجنون در حی عشق نعره زنان</p></div>
<div class="m2"><p>که هرکه کشته لیلی ما نشد حی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدور حسن رخش جمله جهان مستند</p></div>
<div class="m2"><p>ولی چو ما قدح هیچ کس پیاپی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو دیده باز گشا، تاجمال جان بینی</p></div>
<div class="m2"><p>مگو که: کیست وصالش؟ ولی بگو: کی نیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جهان پرست ازین آفتاب عالم تاب</p></div>
<div class="m2"><p>بجز وجود تو دیگر درین میان فی نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز زهد لاف نزد جان قاسمی هرگز</p></div>
<div class="m2"><p>که مرد ره نزند لاف آنچه در وی نیست</p></div></div>