---
title: >-
    شمارهٔ ۴۶۶
---
# شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>در دور رخت یک دل هشیار ندیدیم</p></div>
<div class="m2"><p>جز روی خوشت مشرق انوار ندیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بردیم ببازار جهان گرامی</p></div>
<div class="m2"><p>غیر از غم عشق تو خریدار ندیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مطلوب کسی نیست بغیر از تو درین راه</p></div>
<div class="m2"><p>وین طرفه که غیر تو طلب کار ندیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ظلمت و از نور گذشتیم بیک بار</p></div>
<div class="m2"><p>غیر از تو کسی عالم اسرار ندیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بردار تو منصور عجب گفت که: هیهات؟</p></div>
<div class="m2"><p>دیار بغیر از تو درین دار ندیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صومعه و دیر مغان هیچ کسی را</p></div>
<div class="m2"><p>بی یاد تو در خرقه و زنار ندیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود کشته ای قاسم را، خود تعزیه داری</p></div>
<div class="m2"><p>ای یار، بعیاری تو یار ندیدیم</p></div></div>