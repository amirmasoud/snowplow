---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>منم و عشق سرکش عیار</p></div>
<div class="m2"><p>«ثانی اثنین اذهما فی الغار»</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق چبود؟ بگو: بلای عظیم</p></div>
<div class="m2"><p>عقل چبود؟ بگو که: دارا دار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اول و آخر زمین و زمان</p></div>
<div class="m2"><p>که جهان را بتست استظهار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو اگر حاضری، مشو غافل</p></div>
<div class="m2"><p>جام گلرنگ باده را بکف آر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ما آر جام سرمستان</p></div>
<div class="m2"><p>تا ببازیم عالمی را بقمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا دیر شد که مخموریم</p></div>
<div class="m2"><p>بهر دفع خمار باده خم آر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی را عیار معلومست</p></div>
<div class="m2"><p>قاسم و شیشه تمام عیار</p></div></div>