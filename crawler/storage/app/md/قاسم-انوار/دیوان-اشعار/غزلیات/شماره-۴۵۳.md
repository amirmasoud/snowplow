---
title: >-
    شمارهٔ ۴۵۳
---
# شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>من بیچاره سودا زده سرگردانم</p></div>
<div class="m2"><p>که باوصاف خداوند سخن چون رانم؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من و توحید تو؟هیهات! دلم می لرزد</p></div>
<div class="m2"><p>این قدر بس که حدیثت بزبان می رانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردگارا، ملکا، پادشها، دیانا</p></div>
<div class="m2"><p>چونکه بی چونی، من چون ترا چون دانم؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظری کن ز سر لطف، که عمریست که من</p></div>
<div class="m2"><p>در بیابان تمنای تو سرگردانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با هر جودی و قیوم وجودی بیقین</p></div>
<div class="m2"><p>«حسبناالله کفی » قاعده ایمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همجی کرد سئوالی که: بگو حق بکجاست؟</p></div>
<div class="m2"><p>گفتم: آخر همه جا، در همه جا می دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من بسامان صفات تو کجا ره یابم؟</p></div>
<div class="m2"><p>عاجزم، خسته دلم، بی سر و بی سامانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر قبولم کنی از لطف و کرم یک نفسی</p></div>
<div class="m2"><p>همه اقبال جهان را بجوی نستانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه جا، از همه رو، روی تو در جلوه گریست</p></div>
<div class="m2"><p>مصحف روی ترا از همه رو می خوانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چند روزیست که قاسم ز تو ماندست جدا</p></div>
<div class="m2"><p>بس عجب مانده ام، ای دوست، عجب می مانم</p></div></div>