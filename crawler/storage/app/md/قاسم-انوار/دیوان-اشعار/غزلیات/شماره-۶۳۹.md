---
title: >-
    شمارهٔ ۶۳۹
---
# شمارهٔ ۶۳۹

<div class="b" id="bn1"><div class="m1"><p>زنهار! درین کوی بغفلت نخرامی</p></div>
<div class="m2"><p>جویان خدا باش، اگر مرد تمامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون ز ره راست طریقی بخدا نیست</p></div>
<div class="m2"><p>گر پیر هری باشی، اگر احمد جامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تندست و جگرسوز و جهان تاز بحدی</p></div>
<div class="m2"><p>کس را نبود زهره که گوید که: چه نامی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایمان همه تسلیم و همه صلح و صلاحست</p></div>
<div class="m2"><p>با ما بچه جنگی؟ هله! ای پیر نظامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجا که نظامست همه کار بکامست</p></div>
<div class="m2"><p>من با تو چه گویم؟ که نه خاصی و نه عامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرآن ز خدا آمد و سنت ز پیمبر</p></div>
<div class="m2"><p>گفتند سلف قصه این نامی نامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ار اهل دلی باز بپرسی که: درین راه</p></div>
<div class="m2"><p>کس را خبری هست از آن یار گرامی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مقصود ز اسلام و ز تسلیم همین بود</p></div>
<div class="m2"><p>باقی همه الفاظ و اشارات و اسامی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم، ز جهان معرفت دوست مرادست</p></div>
<div class="m2"><p>گر حق نشناسی، چه عظامی، چه کرامی؟</p></div></div>