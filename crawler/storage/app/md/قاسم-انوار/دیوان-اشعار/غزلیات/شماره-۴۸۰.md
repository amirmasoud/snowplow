---
title: >-
    شمارهٔ ۴۸۰
---
# شمارهٔ ۴۸۰

<div class="b" id="bn1"><div class="m1"><p>تو جان و دل مایی، من وصف تو چون گویم؟</p></div>
<div class="m2"><p>بی چون و چرا گویم: در وصف تو چون گویم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه در طلب عشقت می افتم و می خیزم</p></div>
<div class="m2"><p>گه از صفت حسنت می گریم و می مویم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چند که بحرم من، نه جوی و نه نهرم من</p></div>
<div class="m2"><p>من آ ب حیات، ای جان، از جام تو می جویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بویی ز سر زلفت آورد صبا ناگه</p></div>
<div class="m2"><p>آشفته آن بویم، بر بوی تو می پویم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرگشته و سرگردان، در کوی تو، ای جانان</p></div>
<div class="m2"><p>آشفته آن زلفم، دیوانه آن رویم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناصح، چه دهی پندم؟ نگشود از آن بندم</p></div>
<div class="m2"><p>تو مست هوای خود، من مست می اویم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم ز تو حیران شد، در حلقه مستان شد</p></div>
<div class="m2"><p>از دولت درد تو رخساره بخون شویم</p></div></div>