---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>بسودای تو خوش حالیم و دلشاد</p></div>
<div class="m2"><p>بدردت آرزومندیم و معتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو عالم رابقایی نیست، خوش باش</p></div>
<div class="m2"><p>بیا، می خور، که بربادست بنیاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدامم وقت خوش دارد بجامی</p></div>
<div class="m2"><p>که ساقی را مدامش وقت خوش باد!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندارد لذتی از زندگانی</p></div>
<div class="m2"><p>دلی، کز فکر عالم نیست آزاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحسن ارشاد می فرمایدم عشق</p></div>
<div class="m2"><p>ازین خوشتر چه باشد حسن ارشاد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زدست خوبرویان داد خواهم</p></div>
<div class="m2"><p>الهی،داد ازین سنگین دلان، داد!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر افتاد قاسم در ره عشق</p></div>
<div class="m2"><p>ملامت تا به کی؟ آخر چه افتاد؟</p></div></div>