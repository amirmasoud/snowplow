---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>مرا پیوند او پیوند جانست</p></div>
<div class="m2"><p>دریغست آن جمال از ما نهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگوییم جان ما تنها چه باشد؟</p></div>
<div class="m2"><p>نه تنها جان، که او خود جان جانانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز امید وصال یار مستان</p></div>
<div class="m2"><p>همه ره کاروان در کاروانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیا، گر عاشقی، تا باز بینی</p></div>
<div class="m2"><p>دلم را، کآسمان لامکانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عجایب دولتی دارم، که دایم</p></div>
<div class="m2"><p>دلم با دوست سر بر آستانت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم را برد و جان می خواهد از من</p></div>
<div class="m2"><p>یقینست این که سری در میانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرا تنها مبین در راه توحید</p></div>
<div class="m2"><p>دل قاسم جهان اندر جهانست</p></div></div>