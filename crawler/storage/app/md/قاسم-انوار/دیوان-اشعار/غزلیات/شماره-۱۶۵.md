---
title: >-
    شمارهٔ ۱۶۵
---
# شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>چون روی تو ز مصحف تنزیه آیتیست</p></div>
<div class="m2"><p>هرجا که آیتیست در آنجا درایتیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از من قبول کن سخن خوش، باعتقاد :</p></div>
<div class="m2"><p>هرجا درایتیست هم آنجا هدایتیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آخر نگشت عشق و بپایان رسید عمر</p></div>
<div class="m2"><p>اول بدایتیست، بآخر نهایتیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرجا که می رود سخنی در بیان عشق</p></div>
<div class="m2"><p>مقصود حسن تست، دگرها حکایتیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین بیشتر جفا مکن، ای عمر نازنین</p></div>
<div class="m2"><p>آخر جفا و جور ترا حد و غایتیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همراه عشق باش، که این عشق چاره ساز</p></div>
<div class="m2"><p>اول هدایتیست، بآخر عنایتیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خانه جای عقل بود، یا مقام عشق</p></div>
<div class="m2"><p>مامور عشق باش، که جان را حمایتیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>این عشق چاره ساز در اطوار کاینات</p></div>
<div class="m2"><p>منصور رایتی و از نور رایتیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم، بهر کجا که زند عشق او علم</p></div>
<div class="m2"><p>در ظل او گریز، که مشهور رایتیست</p></div></div>