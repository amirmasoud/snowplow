---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>دل ما بصد جان طلب کار اوست</p></div>
<div class="m2"><p>ولی در حقیقت طلب کار اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهی روی روشن، که در رویها</p></div>
<div class="m2"><p>ظهورات خوبی ز انوار اوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیک جو فروشند صد جان بشهر</p></div>
<div class="m2"><p>چو گویند: بازار بازار اوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو شادان ز عشقی وزو عشق شاد</p></div>
<div class="m2"><p>تو غم خوار اویی و غم خوار اوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو بلبل صفت مانده ای زار گل</p></div>
<div class="m2"><p>ولی بی خبر زانکه گل زار اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برقصند ازین حال ذرات کون</p></div>
<div class="m2"><p>که هر ذره مرآت دیدار اوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشاید که آزار جوید کسی</p></div>
<div class="m2"><p>دل قاسمی را، که دلدار اوست</p></div></div>