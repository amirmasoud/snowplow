---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>بسوخت آتش عشق تو زهد و تقوی را</p></div>
<div class="m2"><p>بباد داد ورقهای درس و فتوی را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز عاصفات خدا بحر قهر موجی زد</p></div>
<div class="m2"><p>نهنگ عشق فرو برد طور موسی را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرامتست نظر بر مهوسی که ندید</p></div>
<div class="m2"><p>میان جلوه صورت جمال معنی را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بغیر دیده مجنون کسی نیاورد تاب</p></div>
<div class="m2"><p>اشعه لمعات جمال لیلی را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسیمی از سر زلفت وزید در عالم</p></div>
<div class="m2"><p>هوای خلد برین داد داردنیی را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سواد زلف ز رخسار بر فشان و بگو:</p></div>
<div class="m2"><p>«بماهتاب چه حاجت شب تجلی را؟»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلای عشق بدعوی قدم کمان کردست</p></div>
<div class="m2"><p>که داشتم بهمه عمر این تمنی را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر چه زار و نزارم، ولی بدولت عشق</p></div>
<div class="m2"><p>کسی چو من نکشید این کمان دعوی را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیش قاسمی از زهد رندی اولی تر</p></div>
<div class="m2"><p>به کاهلی نتوان کرد ترک اولی را</p></div></div>