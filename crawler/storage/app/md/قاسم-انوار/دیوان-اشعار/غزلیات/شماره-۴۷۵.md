---
title: >-
    شمارهٔ ۴۷۵
---
# شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>با روی تو ز سبزه و گلزار فارغیم</p></div>
<div class="m2"><p>با چشم تو ز خانه خمار فارغیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی بیار، ساقی و گردان کن از کرم</p></div>
<div class="m2"><p>کز جور دور گنبد دوار فارغیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را همین بسست که اندر طریق عشق</p></div>
<div class="m2"><p>بر یار عاشقیم و ز اغیار فارغیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان من، اسیر مشو در طریق غم</p></div>
<div class="m2"><p>رقصی بکن که از غم و غمخوار فارغیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما درد دوست را بدو عالم خریده ایم</p></div>
<div class="m2"><p>زانکار هر دو عالم و اقرار فارغیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حق ما اگرچه بدی گوید آن رقیب</p></div>
<div class="m2"><p>اقرار می کنیم و ز انکار فارغیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با قاسمی بهودج اسرار می رویم</p></div>
<div class="m2"><p>در عشق او ز درس و ز تکرار فارغیم</p></div></div>