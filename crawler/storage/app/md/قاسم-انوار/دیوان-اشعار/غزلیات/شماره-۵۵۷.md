---
title: >-
    شمارهٔ ۵۵۷
---
# شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>ما داغ آرزوی تو داریم بر جبین</p></div>
<div class="m2"><p>ما در هوای عشق تو داریم عقل و دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که هست بنده عشقیم، لایزال</p></div>
<div class="m2"><p>دل را نگاه دار، تو ای شاه راستین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را پناه بخش باین عشق چاره ساز</p></div>
<div class="m2"><p>یا رب بحق حرمت مردان راهبین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی عشق نیست جمله ذرات کاینات</p></div>
<div class="m2"><p>هرجا که هست شیوه عشقست در کمین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ظل عشق باش، بهرجا که می روی</p></div>
<div class="m2"><p>از عشق وا ممان، که ضلالی بود مبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما بوده ایم اهل مناجات را امان</p></div>
<div class="m2"><p>ما بوده ایم اهل خرابات را امین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرجا که بوده ایم همه فاش دیده ایم</p></div>
<div class="m2"><p>عکس جمال روی تو در لعبتان چین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنجا که آفتاب جمال تو شعله زد</p></div>
<div class="m2"><p>ما را بپیش روی تو روییست بر زمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر ز روی لطف نظر کن بحال ما</p></div>
<div class="m2"><p>قاسم ز خرمن تو گداییست خوشه چین</p></div></div>