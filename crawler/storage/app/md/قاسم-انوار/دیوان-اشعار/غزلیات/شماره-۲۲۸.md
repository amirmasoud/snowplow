---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>هزار شکر که سلطان عشق جان را داد</p></div>
<div class="m2"><p>هزار مجد و معالی، هزار حسن و رشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به پیش وصل تو از هجر دادها کردم</p></div>
<div class="m2"><p>هزار شکر که سلطان وصل دادم داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای وصل تو جانبخش و دلنواز آمد</p></div>
<div class="m2"><p>که باشد آنکه نباشد به مهر رویت شاد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هزار سال من این ره به سر بپیمودم</p></div>
<div class="m2"><p>که تا رسید مرا سر بر آستان مراد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحسن ولطف وامانی دهر غره مشو</p></div>
<div class="m2"><p>که خانه ایست منقش، ولیک بی بنیاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مورز وصف تانی وکاوکاوی کن</p></div>
<div class="m2"><p>که گنجهاست درین عرصه خراب آباد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بداد قاسم بیچاره جان شیرین را</p></div>
<div class="m2"><p>بآرزوی وصال تو، هر چه باداباد!</p></div></div>