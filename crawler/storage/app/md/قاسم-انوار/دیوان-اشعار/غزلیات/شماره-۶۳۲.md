---
title: >-
    شمارهٔ ۶۳۲
---
# شمارهٔ ۶۳۲

<div class="b" id="bn1"><div class="m1"><p>من عشقم و عشق من چه پرسی؟</p></div>
<div class="m2"><p>جانم همگی، ز تن چه پرسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر تا پای محو یارم</p></div>
<div class="m2"><p>اینست سخن، سخن چه پرسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از پرتو آفتاب حسنش</p></div>
<div class="m2"><p>کارم همه شد حسن، چه پرسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروای مدیح دوست هم نیست</p></div>
<div class="m2"><p>از دشمن طعنه زن چه پرسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از غمزه یار فتنه برخاست</p></div>
<div class="m2"><p>زان غمزه پر فتن چه پرسی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذرات وجود مست عشقند</p></div>
<div class="m2"><p>از باده ذوالمنن چه پرسی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، که فنا شدست، از وی</p></div>
<div class="m2"><p>افسانه ما و من چه پرسی؟</p></div></div>