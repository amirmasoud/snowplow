---
title: >-
    شمارهٔ ۳۶۲
---
# شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>اول ثبوت عرش، پس آنگه جلوس یار</p></div>
<div class="m2"><p>این نکته را بدان و مثل را بیاد دار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن دم که عرش و فرش نبود و خدای بود</p></div>
<div class="m2"><p>آن دم مقر عز بکجا بود اختیار؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این رمز چیست؟ حاصل این قصه بازگو</p></div>
<div class="m2"><p>این را هم تو ندانی و مثل تو صد هزار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حق بر عروش جمله ذرات مستویست</p></div>
<div class="m2"><p>این نکته را ببین تو، ولی سر نگاه دار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل عرش اعظمست خدا را، باتفاق</p></div>
<div class="m2"><p>آنجاست دار سلطنت، آنجاست یار غار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا چند ناله می کنی از سوز و درد دل؟</p></div>
<div class="m2"><p>خواهی ز درد دل برهی، دل بدو سپار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا چند در موافقت نفس راهزن؟</p></div>
<div class="m2"><p>خواهی که جان ز غم ببری، دست از او مدار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در انتظار وعده فردا بسوختی</p></div>
<div class="m2"><p>نقدست وصل یار، چه حاجت به انتظار؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آخر بصد زبان مقر آمد بعجز خویش</p></div>
<div class="m2"><p>قاسم، ز شکرهای ایادی بی شمار</p></div></div>