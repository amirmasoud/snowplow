---
title: >-
    شمارهٔ ۳۸۳
---
# شمارهٔ ۳۸۳

<div class="b" id="bn1"><div class="m1"><p>پیش ما قصه شوقست و شهودست و حضور</p></div>
<div class="m2"><p>در نهان خانه وحدت همه نورست و سرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر سر راه تولا همه شادی و طرب</p></div>
<div class="m2"><p>در بیابان تمنا همه حسبان و غرور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادمانم که بکوی تو گذر خواهم کرد</p></div>
<div class="m2"><p>ترسم از عشق که گوید که: ازین درگه دور!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بس عجب مانده ام، ای جان و جهان، در صفتت</p></div>
<div class="m2"><p>که تو کان نمکی وز تو جهانی در شور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه عجب باشد اگر صید تو گردد دلها؟</p></div>
<div class="m2"><p>عشق چون باز نجیب آمد و جانها عصفور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش «انالحق » گو و بر دار سلامت بر شو</p></div>
<div class="m2"><p>چون کشیدی تو ازین جام شراب منصور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم از جام تو مستست و خراب افتادست</p></div>
<div class="m2"><p>که بهش باز نیاید بگه نغمه صور</p></div></div>