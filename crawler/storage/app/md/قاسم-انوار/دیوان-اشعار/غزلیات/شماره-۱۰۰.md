---
title: >-
    شمارهٔ ۱۰۰
---
# شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>من اگر توبه شکستم کرمش موفورست</p></div>
<div class="m2"><p>پیش دریای کرم توبه من محصورست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جرم بخشیدن و الطاف نمودن کرمست</p></div>
<div class="m2"><p>چه توان گفت؟ که این واعظ ما مغرورست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یا از یار جدا نیست، چه شاید گفتن؟</p></div>
<div class="m2"><p>زاهد شهر ازین قصه بغایت دورست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکه او بانگ «اناالحق » زدم یار شنید</p></div>
<div class="m2"><p>شاه عالم شد و در هر دو جهان منصورست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بشمشیر غمت کشته شوم باکی نیست</p></div>
<div class="m2"><p>هرکه شد کشته شمشیر غمت مغفورست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی را همه آشفته و حیران بینم</p></div>
<div class="m2"><p>همه در کار تو، گر مخلص، اگر مزدورست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، سجده اخلاص کن اندر بر یار</p></div>
<div class="m2"><p>هیچ شک نیست که طاعات چنین مبرورست</p></div></div>