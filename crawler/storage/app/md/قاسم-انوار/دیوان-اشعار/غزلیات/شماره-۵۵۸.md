---
title: >-
    شمارهٔ ۵۵۸
---
# شمارهٔ ۵۵۸

<div class="b" id="bn1"><div class="m1"><p>منت خدای را، که در اطوار ما و طین</p></div>
<div class="m2"><p>در قید مال و جاه نشد جان نازنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر نفس بصدق و صفا ذکر جان و دل</p></div>
<div class="m2"><p>«ایاک نعبد» ست و «ایاک نستعین »</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا که بود حضرت حقست بود ماست</p></div>
<div class="m2"><p>هرجا که مستعان بود آنجاست مستعین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را بیک کرشمه رهاندی ز فکر ما</p></div>
<div class="m2"><p>ای عشق چاره ساز، چه گویم؟ صد آفرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بانگی زدم بکوی قلندر که «ماالفنا»؟</p></div>
<div class="m2"><p>گفتا؟ اگر نه خویشتنی، خویشتن مبین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اکوان بر آستان جلال تو سر نهند</p></div>
<div class="m2"><p>آن دم که برفشانی از حالت آستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا در میان رقص فنا جان فشان شویم</p></div>
<div class="m2"><p>آن زلف را برافشان، ای شاه راستین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گویند: نیست غیر خدا، گفت عالمی :</p></div>
<div class="m2"><p>معنی رب جداست ز معنی عالمین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم، بلی ولیک همان فیض فضل اوست</p></div>
<div class="m2"><p>کاظهار عالمین شد و اقرار متقین</p></div></div>