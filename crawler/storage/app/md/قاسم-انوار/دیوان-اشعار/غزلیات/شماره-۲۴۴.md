---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>هر دلی در دو جهان چشم و چراغی دارد</p></div>
<div class="m2"><p>دل ما در دو جهان بی تو فراغی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ جانیست که بوی تو بدانجا نرسد</p></div>
<div class="m2"><p>بشنود نکهت آن هر که دماغی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این همه قصه «احییت لکی اعراف » چیست؟</p></div>
<div class="m2"><p>دوست از خلوت جان میل به باغی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده ای دارد در خم صفا آن دلبر</p></div>
<div class="m2"><p>هرکه را دید از آن باده ایاغی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما شناسیم که: آن حال خیالست و محال</p></div>
<div class="m2"><p>صوفی از صومعه گر بانگ کلاغی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن حق به همه کس نتوان گفت،اما</p></div>
<div class="m2"><p>عارف آنست که او حسن بلاغی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برساند دل و جان رابه مقامات وصال</p></div>
<div class="m2"><p>هرکه از عشق درین راه چراغی دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خدا زود به مقصود رساند جان را</p></div>
<div class="m2"><p>دل، که از شیوه شوق تو الاغی دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاقبت از نظر لطف به مرهم برسد</p></div>
<div class="m2"><p>دل قاسم،که ز سودای تو داغی دارد</p></div></div>