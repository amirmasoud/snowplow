---
title: >-
    شمارهٔ ۶۷۲
---
# شمارهٔ ۶۷۲

<div class="b" id="bn1"><div class="m1"><p>بسیار طالبی، که مگر ذو فنون شوی</p></div>
<div class="m2"><p>همراه عشق شو، که جنون در جنون شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کوی عشق یار، که دارالامان ماست</p></div>
<div class="m2"><p>با سر اگر درآمده ای سرنگون شوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی لطف یار ما بوصالش مجال نیست</p></div>
<div class="m2"><p>گر کوه آتش آمدی، ار بحر خون شوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو مرغ نارسیده و ناآزموده ای</p></div>
<div class="m2"><p>وقت آمد، ای عزیز، که دست آزمون شوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیر مغان، که رهبر راه حقیقتی</p></div>
<div class="m2"><p>ما را مگر بوصل خدا رهنمون شوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بایدت بوصل دلارام در رسی</p></div>
<div class="m2"><p>شاید که همچو کوه احد بیستون شوی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، سخن ز غیر نگویی و نشنوی</p></div>
<div class="m2"><p>همراز عشق باش که نورالعیون شوی</p></div></div>