---
title: >-
    شمارهٔ ۶۷۷
---
# شمارهٔ ۶۷۷

<div class="b" id="bn1"><div class="m1"><p>درمانده ام از غم جدایی</p></div>
<div class="m2"><p>ای عشق گره گشا کجایی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیگانه مشو ز آشنایان</p></div>
<div class="m2"><p>پیش آی، که نیک آشنایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل غرقه بحر تست، جاوید</p></div>
<div class="m2"><p>ای گوهر فرد دلربایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر لحظه درود می فرستم</p></div>
<div class="m2"><p>آن دم که سرود می سرایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در موت و حیات چاره سازی</p></div>
<div class="m2"><p>در کعبه و دیر رهنمایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هر دو جهان بجود فردی</p></div>
<div class="m2"><p>در ملک وجود پادشایی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم ز سر وجود برخاست</p></div>
<div class="m2"><p>از جود تو می کند گدایی</p></div></div>