---
title: >-
    شمارهٔ ۵۵۳
---
# شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>پیر مغان کجاست؟ که آن مرد دوربین</p></div>
<div class="m2"><p>چون فکر در دل آمد و چون شیر در کمین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرجا که هست پیر مغان، با هزار جان</p></div>
<div class="m2"><p>ما را همیشه روی نیازست بر زمین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصفش چگونه گویم و شرحش چسان دهم؟</p></div>
<div class="m2"><p>آنرا که آفتاب عیانست بر جبین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچا که بینمش همه جانم جهان شود</p></div>
<div class="m2"><p>زان مهر ذره پرور و زان ماه نازنین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با روی او حکایت «ایاک نعبد» است</p></div>
<div class="m2"><p>با خوی او شفاعت «ایاک نستعین »</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم بشیوه های ملاحت گرفته است</p></div>
<div class="m2"><p>آن ماه دل فروز من، آن شاه راستین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیر مغانه گفت و غلط گفت، قاسمی</p></div>
<div class="m2"><p>پیر مغان مگوی، که نوریست راست بین</p></div></div>