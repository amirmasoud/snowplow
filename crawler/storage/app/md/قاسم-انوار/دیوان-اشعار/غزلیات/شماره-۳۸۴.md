---
title: >-
    شمارهٔ ۳۸۴
---
# شمارهٔ ۳۸۴

<div class="b" id="bn1"><div class="m1"><p>در کهن دیر زمان جمله فریبست و غرور</p></div>
<div class="m2"><p>وقت آن شد که زنم خیمه بصحرای سرور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صفت شیوه «احببت » شنید این دل مست</p></div>
<div class="m2"><p>علم عشق برافراخت بصحرای ظهور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چنان مست خرابم بخرابات امروز</p></div>
<div class="m2"><p>که بهش باز نیایم بگه نغمه صور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صفت نور ترا دید ورای انوار</p></div>
<div class="m2"><p>ورد جان و دل ما گشت که: «یا نورالنور»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل، از هستی خود یک قدمی بیرون نه</p></div>
<div class="m2"><p>تا شود در نفسی جرم و گناهت مغفور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حالت هستی تو خانه دل کرد خراب</p></div>
<div class="m2"><p>هان و هان! تا نشنوی باز بهستی مغرور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، از جنت و فردوس مگو، کان شه را</p></div>
<div class="m2"><p>جنتی هست، که آنجا نه قصور است و نه حور</p></div></div>