---
title: >-
    شمارهٔ ۶۰۴
---
# شمارهٔ ۶۰۴

<div class="b" id="bn1"><div class="m1"><p>دل ز من برداشت یار مهربان، واحسرتی</p></div>
<div class="m2"><p>دشمنی کردند با من دوستان، واحسرتی!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهد من بشکست، جانم سوخت، مهر از من برید</p></div>
<div class="m2"><p>با من این کرد او بقول دشمنان، واحسرتی!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آستین بر من فشاند آن دلبر پیمان شکن</p></div>
<div class="m2"><p>رخت ما را مینهد بر آستان، واحسرتی!</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد آن دارم که: چون دلبر کند از ما کنار</p></div>
<div class="m2"><p>با که خواهد کرد دست اندر میان؟، واحسرتی!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راست میگویم: چو با من شیوه کج می رود</p></div>
<div class="m2"><p>راستش ناید بحق راستان، واحسرتی!</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشمنی کردند با من دوستان، از بخت بد</p></div>
<div class="m2"><p>این حکایت در جهان شد داستان، واحسرتی!</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی را آه سرد از دل برآمد، کان نگار</p></div>
<div class="m2"><p>میل دل دارد بمهر دیگران، واحسرتی!</p></div></div>