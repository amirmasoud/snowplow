---
title: >-
    شمارهٔ ۵۹۴
---
# شمارهٔ ۵۹۴

<div class="b" id="bn1"><div class="m1"><p>مرآت دل شکستی، ای گنج جاودانه</p></div>
<div class="m2"><p>جان را وحید کردی آخر بهر بهانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب بود قوت ما، روزست قدرت ما</p></div>
<div class="m2"><p>ما مست جام عشقیم، از باده شبانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رحمی کن،ای طبیبم، ای دلبر حبیبم</p></div>
<div class="m2"><p>بر روی زعفرانی، بر اشک دانه دانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم: نشان زلفت با ما که گوید آخر؟</p></div>
<div class="m2"><p>هرجا مدققی بد، کردند ذکر شانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عاشقی و مردی، در راه عشق فردی</p></div>
<div class="m2"><p>کو آه دردمندان؟ کو سوز عاشقانه؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر قاب قوسین آخر چه فهم کردی؟</p></div>
<div class="m2"><p>با مصطفی خدا را سریست در میانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر سر عشق جویی، قاسم، ز دل طلب کن</p></div>
<div class="m2"><p>گنجیست بی نهایت، بحریست بی کرانه</p></div></div>