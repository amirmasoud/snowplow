---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>عاشقان را چو صلا جانب می خانه زدند</p></div>
<div class="m2"><p>آتشی بود که اندر دل دیوانه زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در تمنای تو عشاق ز پای افتاده</p></div>
<div class="m2"><p>مست گشتند و ز مستی کف مستانه زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عکس ساقی چو درین باده صافی افتاد</p></div>
<div class="m2"><p>عاشقان در هوست ساغر و پیمانه زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم آشفته شد،ای دوست، دگر باره،چه بود؟</p></div>
<div class="m2"><p>زلف میگون تراباز مگر شانه زدند؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر سخن کز صفت شمع جمالت گفتند</p></div>
<div class="m2"><p>آتشی بود که در باطن پروانه زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرمشان نامد از آن یار،که در عین غرور</p></div>
<div class="m2"><p>طعنهایی که بر آن عاشق فرزانه زدند؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی،بنده آن راه روانم که ز شوق</p></div>
<div class="m2"><p>قدم صدق درین بادیه مستانه زدند</p></div></div>