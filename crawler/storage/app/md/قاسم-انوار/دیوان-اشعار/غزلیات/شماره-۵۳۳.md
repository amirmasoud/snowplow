---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>جگر پردرد و دل پر خونم، ای جان</p></div>
<div class="m2"><p>بآب دیده گلگونم، ای جان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندارم طاقت ایان فرقت</p></div>
<div class="m2"><p>چه گویم من که بی تو چونم، ای جان؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه سازم؟ چاره دردم چه باشد؟</p></div>
<div class="m2"><p>بران زلف و رخت مفتونم، ای جان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ندانم داروی دردم چه سازم؟</p></div>
<div class="m2"><p>که در هجران تو مغبونم، ای جان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهر حالی نمی دانم شب از روز</p></div>
<div class="m2"><p>چنان واله، چنان مجنونم، ای جان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همیشه قامت من چون الف بود</p></div>
<div class="m2"><p>ز سودایت کنون چون نونم، ای جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر قاسم نبیند روی آن یار</p></div>
<div class="m2"><p>بجان تو که بس محزونم، ای جان</p></div></div>