---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>از تو بمقصود ره دور نیست</p></div>
<div class="m2"><p>گر دل و جان غافل و مغرور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از همه ذرات جهان ظاهرست</p></div>
<div class="m2"><p>یار، اگر دیده دل کور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام من از خم قدیم خداست</p></div>
<div class="m2"><p>باده ما باده انگور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق مناجات نیابی بدل</p></div>
<div class="m2"><p>موسی جان چون بسر طور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لاف «اناالحق » مزن ای مدعی</p></div>
<div class="m2"><p>نشأئه تو نشأئه منصور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مفتی ما فهم نکرد این سخن</p></div>
<div class="m2"><p>لقمه باز از پی عصفور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تارخ چون ماه تو شد در نقاب</p></div>
<div class="m2"><p>هیچ دلی نیست که مهجور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچ دمی نیست که از شوق تو</p></div>
<div class="m2"><p>در دل و جان عربده و شور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی از درد تو دارد نصیب</p></div>
<div class="m2"><p>بی رخ زیبای تو مسرور نیست</p></div></div>