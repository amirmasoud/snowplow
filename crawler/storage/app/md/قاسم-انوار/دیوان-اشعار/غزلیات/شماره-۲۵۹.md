---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>ارنی و لن ترانی راز و نیاز باشد</p></div>
<div class="m2"><p>نزدیک مرد عارف این هر دو باز باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر رهروی، بدانی،ای معدن امانی</p></div>
<div class="m2"><p>بیرون ازین دو منزل دریای راز باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ذرها ببینی انوار حسن جانان</p></div>
<div class="m2"><p>گر دیده بصیرت فی الجمله باز باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر حکمت شریعت در جان بود و دیعت</p></div>
<div class="m2"><p>در عالم حقیقت با برگ و ساز باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرمایه حقیقت عشقست در طریقت</p></div>
<div class="m2"><p>بی عشق هرچه بینی،امرمجاز باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی اگر ببینم دیدار دلنوازش</p></div>
<div class="m2"><p>آن روز را چه گویم؟عمر دراز باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم نیازمندی دارد بر آستانت</p></div>
<div class="m2"><p>سرمایه فقیران سوز و گداز باشد</p></div></div>