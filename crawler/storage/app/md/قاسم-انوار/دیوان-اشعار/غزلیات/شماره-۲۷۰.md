---
title: >-
    شمارهٔ ۲۷۰
---
# شمارهٔ ۲۷۰

<div class="b" id="bn1"><div class="m1"><p>از دولت وصال تو کارم بکام شد</p></div>
<div class="m2"><p>بختم بلند گشت و سعادت غلام شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جلوه های حسن تو جانم حیات یافت</p></div>
<div class="m2"><p>با چشمهای مست تو عیشم مدام شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی:سلام ذوق سلامت بدل رسید</p></div>
<div class="m2"><p>این خانه از سلام تو دارالسلام شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل را حلال گشت ز عشق تو دم زدن</p></div>
<div class="m2"><p>زان دم که یاد غیر تو بردن حرام شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عمرها صفای تو باشد قرین حال</p></div>
<div class="m2"><p>دل راکه دار کعبه وصلت مقام شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ازمن برند لمعه نور آفتاب و ماه</p></div>
<div class="m2"><p>تاسایه تو بر سر من مستدام شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون دید زلف و روی ترا قاسمی بهم</p></div>
<div class="m2"><p>در طور کفر و دین همه کارش تمام شد</p></div></div>