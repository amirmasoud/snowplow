---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>امروز بهرحال به از دی و پریرست</p></div>
<div class="m2"><p>عالم همه پر عنبر سارا و عبیرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفاق عبیر بیز شد، آخر چه ظهورست؟</p></div>
<div class="m2"><p>یا نور تجلی که ز سلطان نصیرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ار جمله ذرات جهان مخفی و پیداست</p></div>
<div class="m2"><p>ار جمله صفت شاهد بی مثل و نظیرست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زنهار! دل از غیر نگه دار، که آن یار</p></div>
<div class="m2"><p>در هر نفسی واقف اسرار ضمیرست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان و دل آفاق بیک جلوه ببردی</p></div>
<div class="m2"><p>خوش حالت صیدی که بدام تو اسیرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای طالب دیدار، برو دیده بدست آر</p></div>
<div class="m2"><p>چون واقف اسرار شوی خیر کثیرست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، چو خطا رفت، بیا سجده سهو آر</p></div>
<div class="m2"><p>کان لطف و کرم توبه ده و عذر پذیرست</p></div></div>