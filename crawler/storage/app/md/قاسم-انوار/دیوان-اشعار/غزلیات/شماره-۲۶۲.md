---
title: >-
    شمارهٔ ۲۶۲
---
# شمارهٔ ۲۶۲

<div class="b" id="bn1"><div class="m1"><p>تا دل آشفته آن زلف پریشان باشد</p></div>
<div class="m2"><p>دل شوریده من واله و حیران باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی جان را بتوان دیدن و خرم گشتن</p></div>
<div class="m2"><p>گر دلت آینه نیر عرفان باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر توحید توان گفت به هشیاران؟ نی</p></div>
<div class="m2"><p>بتوان گفت اگر مجلس مستان باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دورست ز معنی به حقیقت دیوست</p></div>
<div class="m2"><p>گر بصورت مثلا یوسف کنعان باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما بسودای تو خواری جهانی بکشیم</p></div>
<div class="m2"><p>حاجیان را چه غم از خار مغیلان باشد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که از کوی تو بگریزد و جنت طلبد</p></div>
<div class="m2"><p>غبن فاحش بود،از غبن پشیمان باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر دل خسته قاسم ز کرم رحمت کن</p></div>
<div class="m2"><p>کین متاعیست که در ملک تو ارزان باشد</p></div></div>