---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>چو عکس مشرق صبح ازل هویدا شد</p></div>
<div class="m2"><p>جمال دوست ز ذرات کون پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه خم شراب ازل مصفا بود</p></div>
<div class="m2"><p>ولی بجان و دل ما رسید،اصفا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خزانه رحمت بقفل حکمت بود</p></div>
<div class="m2"><p>زمان دولت ما رسید،دروا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز در آینه جان ما نکرد ظهور</p></div>
<div class="m2"><p>جمال عشق، که هم اسم و هم مسما شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جهان ز پرتو روی حبیب روشن گشت</p></div>
<div class="m2"><p>بجان دوست،که آن روشنی هم از ما شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث دوست ببازار کاینات رسید</p></div>
<div class="m2"><p>قیامتی که نهان بود، آشکارا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار جان مقدس فدای شاه عرب</p></div>
<div class="m2"><p>که عیش قاسمی از عشق او مهنا شد</p></div></div>