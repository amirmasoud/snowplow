---
title: >-
    شمارهٔ ۲۳۵
---
# شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>ابر سودای تو آن لحظه که توفان بارد</p></div>
<div class="m2"><p>دل دیوانه ما جان بجوی نشمارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تخم سودای تو در بهر یقین افشاندم</p></div>
<div class="m2"><p>دل شناسد که: ازین بحر چه بر می دارد؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد از شیوه تقلید درین مزرع عمر</p></div>
<div class="m2"><p>من ندانم چه درودست و چها می کارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظ از مستی عشاق ندارد خبری</p></div>
<div class="m2"><p>در چنین معصره ای غوره چه می افشارد؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد می آید و از کوی تو دارد خبری</p></div>
<div class="m2"><p>دل و جانها همه خون، تا چه خبر می آرد؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما را بصفا وصل تو جان می بخشد</p></div>
<div class="m2"><p>جان مارا بجفا هجر تو می آزارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، هر که دراین کوچه در آمد سر باخت</p></div>
<div class="m2"><p>غیر آن زاهد ترسیده که سر می خارد</p></div></div>