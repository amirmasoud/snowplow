---
title: >-
    شمارهٔ ۵۳۰
---
# شمارهٔ ۵۳۰

<div class="b" id="bn1"><div class="m1"><p>میان باطن جانی و جان تویی، ای جان</p></div>
<div class="m2"><p>همه تویی، بهمه حال، آشکار و نهان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که دل بخرابات می کشد چه کنم؟</p></div>
<div class="m2"><p>حدیث توبه و تقوی، بیان امن و امان؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فلان، که معدن فضلست و مقتدای بشر</p></div>
<div class="m2"><p>چو ذوق عشق ندارد، نگویمش انسان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که گفت پیر جعل را که: قاید راهست؟</p></div>
<div class="m2"><p>نه قایدست ولی هست رهزن پنهان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو دیده باز گشا، تا جمال جان بینی</p></div>
<div class="m2"><p>که نیست خالی ازو هیچ ذره از اعیان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بجذبه دو جهان طی کند بیک ساعت</p></div>
<div class="m2"><p>بطور عشق رسیدست سیر موسی جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه حکمتست درین قصه؟ کس نمی داند</p></div>
<div class="m2"><p>تو در میان حجابی و عالمی نگران</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نقاب را بگشا و فغان مستان بین</p></div>
<div class="m2"><p>خوشست وقت تجلی خروش سرمستان</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگو که: قاسم بیچاره کلب کوچه ماست</p></div>
<div class="m2"><p>چه کم شود که ز جودت گدا شود سلطان؟</p></div></div>