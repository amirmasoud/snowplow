---
title: >-
    شمارهٔ ۳۹۲
---
# شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>از لب لعل توام کار بکامست امروز</p></div>
<div class="m2"><p>فلکم بنده و خورشید غلامست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که قانون شفای دل خود میطلبد</p></div>
<div class="m2"><p>از اشارات منش کار بکامست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خسرو مصر جهان شاهد یوسف روییست</p></div>
<div class="m2"><p>که مهش بنده و خورشید بدامست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجلس عشق نهاده است و می اندر داده</p></div>
<div class="m2"><p>سخن عقل درین بزم حرامست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش ازین حالت دل مستی و هشیاری بود</p></div>
<div class="m2"><p>از می ساقی جان مست مدامست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار چون شد متکلم، تو رها کن کلمات</p></div>
<div class="m2"><p>خام ریشی و حکایات تو خامست امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، فاش مکن قصه اسرار ازل</p></div>
<div class="m2"><p>سر نگه دار، که غوغای عوامست امروز</p></div></div>