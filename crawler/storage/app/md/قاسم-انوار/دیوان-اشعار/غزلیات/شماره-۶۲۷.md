---
title: >-
    شمارهٔ ۶۲۷
---
# شمارهٔ ۶۲۷

<div class="b" id="bn1"><div class="m1"><p>نه مست جام خدایی، نه مرد مستوری</p></div>
<div class="m2"><p>نه مخبری و نه مستخبری، عجب کوری!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین خرابه غفلت برون خرام، ای دل</p></div>
<div class="m2"><p>یقین بدان به حقیقت که بیت معموری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غدیر گفت به دریا: نه عاشق و مستی</p></div>
<div class="m2"><p>بگو: همیشه چرا در فغان و در شوری؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو طالب در مکنون و آن گهر با توست</p></div>
<div class="m2"><p>چو در میان وصالی چراست مهجوری؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شراب و شاهد و شمع اندرون خانه توست</p></div>
<div class="m2"><p>ازان چه بهره بری با کری و با کوری؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگو به صوفی رسمی: مرقص و نعره مزن</p></div>
<div class="m2"><p>که مست جام هوایی، نه جام منصوری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار نفخه صورست در جهان هر دم</p></div>
<div class="m2"><p>اگر نه مرده‌دلی پس چرا درین گوری؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طریق رسم رها کن، بدان که: ممکن نیست</p></div>
<div class="m2"><p>که شاهباز توان شد به بال عصفوری</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیا، ساقی، از آن می که راحت جانست</p></div>
<div class="m2"><p>به جان رسید روانم ز رنج مخموری</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>اگر به چشم حقیقت جمال خود بینی</p></div>
<div class="m2"><p>سرت بلند، که هم ناظری و منظوری</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بگو ز قاسمی این یک سخن به واعظ شهر</p></div>
<div class="m2"><p>که: راه حق نتوان شد به وصف مغروری</p></div></div>