---
title: >-
    شمارهٔ ۳۱۱
---
# شمارهٔ ۳۱۱

<div class="b" id="bn1"><div class="m1"><p>دل آینه صورت و معنیست عجب بود</p></div>
<div class="m2"><p>کان شاهدما روی درین آینه ننمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه نه، چو صفا نبود هرگز ننماید</p></div>
<div class="m2"><p>در آینه جان صفت شاهد و مشهود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در راه تو عشاق سر از پای ندانند</p></div>
<div class="m2"><p>ای دولت عشاق،زهی مقصد و مقصود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حاجی ز ره کعبه پشیمان شد و برگشت</p></div>
<div class="m2"><p>چون باده نپیمود ره بادیه پیمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>واعظ،بس از این قصه،که هرگز نستانند</p></div>
<div class="m2"><p>در دار عیار دل ما قلب زراندود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگر سخن ازشمع وزپروانه مگویید</p></div>
<div class="m2"><p>با زاهد ما، چون نه ایازست و نه محمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راه غمت قاسم بیچاره شب و روز</p></div>
<div class="m2"><p>اندر طلبت دربدر و کوی بکو بود</p></div></div>