---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>دین هر کس بقدر صدق و صفاست</p></div>
<div class="m2"><p>دیدن عاشقان طریق فناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چند پرسی: لباب عرفان چیست؟</p></div>
<div class="m2"><p>آنچه با فهم تو نیاید راست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سخن سر این معما را</p></div>
<div class="m2"><p>تو ندانسته ای، مگو که خطاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواستم، جام داد و عذر بگفت</p></div>
<div class="m2"><p>عذر این را کجا توانم خواست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ریاض ابد بفیض ازل</p></div>
<div class="m2"><p>جان ما را هزار نشو و نماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قبله گم کرده ام، رو بنما</p></div>
<div class="m2"><p>که جمال تو قبله دلهاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، آسمان الا الله</p></div>
<div class="m2"><p>گرچه بالا بود ولی بی لاست</p></div></div>