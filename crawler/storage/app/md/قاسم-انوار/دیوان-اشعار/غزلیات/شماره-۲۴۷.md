---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>در ولای تو دلم حسن وفایی دارد</p></div>
<div class="m2"><p>روی زیبای تو هر لحظه صفایی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق مستست،ندانم که چه خواهد کردن؟</p></div>
<div class="m2"><p>غالبا نیت انگیز بلایی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بیچاره من بر سر کوی تو رسید</p></div>
<div class="m2"><p>من چه گویم که چه خوش آب و هوایی دارد؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سحرگه که وزد باد صبا زان سر کو</p></div>
<div class="m2"><p>بوستان دل من نشو و نمایی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخت ترسانم ازین هجر ولی شادانم</p></div>
<div class="m2"><p>کز وصال تو دلم برگ و نوایی دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق مستست بخم خانه و می می نوشد</p></div>
<div class="m2"><p>جام بر کف، همه را بانگ و صلایی دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست پرسید ز اصحاب که: قاسم چونست؟</p></div>
<div class="m2"><p>با چنین پایه غم بی و سر پایی دارد</p></div></div>