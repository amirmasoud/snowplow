---
title: >-
    شمارهٔ ۴۳۲
---
# شمارهٔ ۴۳۲

<div class="b" id="bn1"><div class="m1"><p>من از سودای جانان نیم مستم</p></div>
<div class="m2"><p>بده، ساقی، می گلگون بدستم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا جام آر، از آن خم دل افروز</p></div>
<div class="m2"><p>که من این شیشها درهم شکستم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خطایی ناید از من، یا رب، آمین</p></div>
<div class="m2"><p>در آن عهدی که من با دوست بستم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر حالی دلم با اوست دایم</p></div>
<div class="m2"><p>اگر خود مؤمنم گر بت پرستم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر جویای آن یاری بتحقیق</p></div>
<div class="m2"><p>بجه از جو، که من از جوی جستم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو چشمش فتنه ای انگیخت ناگاه</p></div>
<div class="m2"><p>هنوز اندر میان قرقشستم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زناگه آتشی افروخت جانان</p></div>
<div class="m2"><p>چو قاسم در میان مجمرستم</p></div></div>