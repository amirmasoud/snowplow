---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>همه دردست درین واقعه، پس درمان چیست؟</p></div>
<div class="m2"><p>چاره کار من بیدل سرگردان چیست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل و جان ملک حبیبست و بلامال محب</p></div>
<div class="m2"><p>شرع عشاق چنینست، مرا تاوان چیست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرنه چشم خوش تو باده فروشست، ای جان</p></div>
<div class="m2"><p>بر درت شب همه شب مشغله مستان چیست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسمان نیز بصد دیده ترا می طلبد</p></div>
<div class="m2"><p>ورنه اندر کفش این مشغله تابان چیست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی تو روضه رضوان اگرم جای دهند</p></div>
<div class="m2"><p>گویم: ای دوست، چه دزدیده ام؟ این زندان چیست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که کوی تو ندیدست نداند هرگز</p></div>
<div class="m2"><p>که بر زنده دلان جنت جاویدان چیست؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل قاسم بطلب تا که یقینت گردد</p></div>
<div class="m2"><p>مشرق صبح ازل، مملکت عرفان چیست؟</p></div></div>