---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>بسیار سعی کردم و بسیار اجتهاد</p></div>
<div class="m2"><p>عشقست هر چه هست،دگر هرچه هست باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک ذره بوی عشق بهر جا که باد برد</p></div>
<div class="m2"><p>مؤمن ز دین برآمد و صوفی زاعتقاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چندین هزار نور نبوت، که آمدند</p></div>
<div class="m2"><p>کمتر در آمدند ز خلقان درین رشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک لمعه نور عشق اگر جلوه گر شدی</p></div>
<div class="m2"><p>ذرات کون «اشهد» گفتی بصد وداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای جان و دل،بجان نظری کن ز روی لطف</p></div>
<div class="m2"><p>بی تو نه خواب دارم و نه صبر ونه سداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای عشق دل فروز، که جان را حمایتی</p></div>
<div class="m2"><p>از جورتست این همه فریادودادداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، طریق عشق چنینست جاودان</p></div>
<div class="m2"><p>از دلبران جفا و ز دلدار انقیاد</p></div></div>