---
title: >-
    شمارهٔ ۳۶۷
---
# شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>درد سری میدهد زحمت رنج خمار</p></div>
<div class="m2"><p>زهد برون کن ز سر، ساقی جان را در آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان جهانرا طلب، ملک عیانرا طلب</p></div>
<div class="m2"><p>جان جهان باقیست، ملک جهان مستعار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون همه جان و دلت لایق آن حضرتست</p></div>
<div class="m2"><p>دل بر الله بر، دیده بر آن راه دار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در صفت هر کسی رفت حکایت بسی</p></div>
<div class="m2"><p>هیچ نپرسی سخن از صفت یار غار؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند روی غافلی، بر سر آب و گلی؟</p></div>
<div class="m2"><p>سر ز رقیبان بر، سر ز حبیبان بر آر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کس در گوشه ای، باشد با توشه ای</p></div>
<div class="m2"><p>ما و فراق حبیب، خسته دل و سوگوار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دل و هر حالتی، دارد از آن راحتی</p></div>
<div class="m2"><p>قاسمی و گوشه ای، درد دل بی قرار</p></div></div>