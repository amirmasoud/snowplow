---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>چون صبح سعادت ز جبین تو هویداست</p></div>
<div class="m2"><p>ما را بتو صد گونه تولا و تمناست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو ساقی جانهایی و جانها بتو شادند</p></div>
<div class="m2"><p>در ده قدح باده، که هنگام تولاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلس مستان خدا وقت سماعست</p></div>
<div class="m2"><p>چون عشق مزید آمد و چون حسن هویداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر قصه که در وصف جمالست و جلالست</p></div>
<div class="m2"><p>تا فکر زیادت نکنی، نسبت سوداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنجای که جسمست بکلی همه اسمست</p></div>
<div class="m2"><p>آن جای که جانیست نه اسم و نه مسماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با عشق خدا باش، که در صورت و معنی</p></div>
<div class="m2"><p>چون کار تو عشقست همه کار مهیاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، چه کنی؟ گر نکنی روی بدین بحر</p></div>
<div class="m2"><p>کین کثرت امواج هم از لجه دریاست</p></div></div>