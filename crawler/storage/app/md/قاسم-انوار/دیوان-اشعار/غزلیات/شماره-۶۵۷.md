---
title: >-
    شمارهٔ ۶۵۷
---
# شمارهٔ ۶۵۷

<div class="b" id="bn1"><div class="m1"><p>جراحت دل من تازه کرد دلبر جانی</p></div>
<div class="m2"><p>که هر جدید درو لذتیست وین تو ندانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پس از مجاورت چاه دید یوسف کنعان</p></div>
<div class="m2"><p>بمصر عالم صورت تجلیات معانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا ز ذوق ملامت خبر کجاست؟ که دایم</p></div>
<div class="m2"><p>رهین نعمت و عزت، قرین امن و امانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سعادتی که تو داری بوصف راست نیاید</p></div>
<div class="m2"><p>حیات و صحت و عرفان، جمال و جان و جوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بکوش تا بشناسی کمال نعمت منعم</p></div>
<div class="m2"><p>رموز دفتر شکرش چنان بخوان که بدانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو یوسف جان را ز حبس تن بدر آری</p></div>
<div class="m2"><p>باتفاق عزیزان، عزیز هر دو جهانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خموش، قاسم، ازین پس بپوش حال درون را</p></div>
<div class="m2"><p>ترا چه شد که همه آه و درد و سوز و فغانی؟</p></div></div>