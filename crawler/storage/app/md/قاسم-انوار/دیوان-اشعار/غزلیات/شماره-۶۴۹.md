---
title: >-
    شمارهٔ ۶۴۹
---
# شمارهٔ ۶۴۹

<div class="b" id="bn1"><div class="m1"><p>بتو جان کجا برد پی؟ که تو شاه بی نشانی</p></div>
<div class="m2"><p>ز تو دل کجا گریزد؟که تو معدن امانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهمین خوشست جانم که سگ در تو باشم</p></div>
<div class="m2"><p>چه کنم؟ چه چاره سازم؟ اگرم ز در برانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بثنای تو زبانم نرسید و گنگ گشتم</p></div>
<div class="m2"><p>پس ازین مگر بگویم بزبان بی زبانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بچه نسبتت کند جان؟ که شدست در تو حیران</p></div>
<div class="m2"><p>بتو هیچ کس نماند، تو بهیچ کس نمانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشا رگ سبل را، بنما بما ازل را</p></div>
<div class="m2"><p>بعدم فرست اجل را، که حیات جاودانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شب وصلت حبیبان چه محل این رقیبان؟</p></div>
<div class="m2"><p>بمیان جنت، ای جان، چه غمست از زیانی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قدح شراب در ده، که بروزگار پیری</p></div>
<div class="m2"><p>هوسست قاسمی را دو سه هفته ای جوانی</p></div></div>