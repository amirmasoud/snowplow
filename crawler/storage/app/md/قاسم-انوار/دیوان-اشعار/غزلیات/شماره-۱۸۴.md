---
title: >-
    شمارهٔ ۱۸۴
---
# شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>عاشقان در جمع با یارند و این بس دور نیست</p></div>
<div class="m2"><p>پیش دوران طریقت این سخن مشهور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رمز مستان معانی را نداند عقل دون</p></div>
<div class="m2"><p>صیدبازان حقیقت در خور عصفور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق مستست و بتیغ تیز میگوید سخن</p></div>
<div class="m2"><p>پیش مستان حقایق این سخن مستور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو صید دفتر بخوانی از حدیث عاشقان</p></div>
<div class="m2"><p>عشق و نام نیک هرگز مثبت و مسطور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من ز اسرار خدا هرگز کجا گویم سخن؟</p></div>
<div class="m2"><p>یوسفم در قید زندان، موسیم بر طور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی را همتی باید بغایت بس بلند</p></div>
<div class="m2"><p>عشقبازی در خور آن زاهد مغرور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، سر خدا با جان سرگردان مگو</p></div>
<div class="m2"><p>کین سخن ها در خور کیخسرو و فغفور نیست</p></div></div>