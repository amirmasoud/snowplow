---
title: >-
    شمارهٔ ۶۴۰
---
# شمارهٔ ۶۴۰

<div class="b" id="bn1"><div class="m1"><p>گر شمس منیر آمدی، ار بدر تمامی</p></div>
<div class="m2"><p>یک جرعه تصدق طلب از ساقی جامی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بیشه شیران همه شیران سرافراز</p></div>
<div class="m2"><p>زنهار! درین کوی بغفلت نخرامی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان بنده شاهیست، که آن شاه دل افروز</p></div>
<div class="m2"><p>خورشید جهان را نپسندد بغلامی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محبوب خدا، شیر دغا، احمد صادق</p></div>
<div class="m2"><p>هم روی تو فرخنده و هم نام تو نامی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتوان بزبان وصف تو گفتن بتمامی</p></div>
<div class="m2"><p>ای جان جهان، صدر امینی و امامی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک بار نقاب از طرف چهره برانداز</p></div>
<div class="m2"><p>تا عاشق روی تو شود عارف و عامی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم ز غمت بیدل و بیچاره و مستست</p></div>
<div class="m2"><p>نتوان صفت لطف تو گفتن به تمامی</p></div></div>