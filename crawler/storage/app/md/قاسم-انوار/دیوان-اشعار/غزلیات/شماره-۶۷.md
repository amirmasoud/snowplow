---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>خورشید منور ز جمال تو هویداست</p></div>
<div class="m2"><p>در مشرب عذب تو چه گویم که چه سرهاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارف نکند منع من از عشق تو، آری</p></div>
<div class="m2"><p>مجنون چه کند؟ کین کشش از جانب لیلاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمریست بسر می برم اندر سر کویت</p></div>
<div class="m2"><p>در سایه زلف تو، که آن مایه سوداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناصح ز سیاهی دل خویش ندانست</p></div>
<div class="m2"><p>کان زلف سیه پوش تو غارتگر دلهاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل، همه کس طالب یارند، فاما</p></div>
<div class="m2"><p>تا بخت کرا جوید و دولت ز کجا خاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از بوی می عشق تو شد مست جهانی</p></div>
<div class="m2"><p>زاهد بخرابات مغان آمد و می خواست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند که: آن یار ز قاسم نکند یاد</p></div>
<div class="m2"><p>این نیز هم از طالع شوریده شیداست</p></div></div>