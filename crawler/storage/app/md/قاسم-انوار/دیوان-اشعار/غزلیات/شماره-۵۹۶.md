---
title: >-
    شمارهٔ ۵۹۶
---
# شمارهٔ ۵۹۶

<div class="b" id="bn1"><div class="m1"><p>آیینه تیره شد، ز چه تیره است آینه؟</p></div>
<div class="m2"><p>چون رو به روی دوست ندارد هرآینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرآت دل به مصقله ذکر پاک کن</p></div>
<div class="m2"><p>تا روی دوست را بنماید معاینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>«جَفُّ القَلَم بِما هو کاین» تمام شد</p></div>
<div class="m2"><p>تفصیل یافت صورت اجمال کاینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوشینه شب که اول شب بود و عید بود</p></div>
<div class="m2"><p>آن غازی من آمد «یاریم سراینه»</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم سلام گرم و زدم بوسه بر رکاب</p></div>
<div class="m2"><p>«هیچ التفات قلمادی اول شه گداینه»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکس ز کوه «کون» صدایی شنیده‌اند</p></div>
<div class="m2"><p>آواز یار غار شنیدم «صداینه»</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوشدل شدم ز مغلطهٔ آن مرادِ دل</p></div>
<div class="m2"><p>جانم نجات یافت ز هجران باینه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می‌خواستم به کوی تو آیم به پای‌بوس</p></div>
<div class="m2"><p>ره نیک دور بود و مرا زور پای نه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گفتم که: قاسمی به جمال تو یافت راه</p></div>
<div class="m2"><p>در خنده رفت یار گرامی که:«های نه»</p></div></div>