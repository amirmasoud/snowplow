---
title: >-
    شمارهٔ ۴۱۱
---
# شمارهٔ ۴۱۱

<div class="b" id="bn1"><div class="m1"><p>ای زلف و رخت میگون، ای دوست سلام علیک</p></div>
<div class="m2"><p>وی شیوه تو موزون، ای دوست سلام علیک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کارم همه موزون شد، روی دل از آن سون شد</p></div>
<div class="m2"><p>بر روی تو مفتون شد، ای دوست سلام علیک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریا همه هامون شد، دلها همگی خون شد</p></div>
<div class="m2"><p>جان جانب بیچون شد، ای دوست سلام علیک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل شاه فریدون شد، از کوه بهامون شد</p></div>
<div class="m2"><p>در صفوت ذوالنون شد، ای دوست سلام علیک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساعات چو میمون شد، جان جانب بیچون شد</p></div>
<div class="m2"><p>با باده گلگون شد، ای دوست سلام علیک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون طبع تو موزون شد، راه تو از آن سون شد</p></div>
<div class="m2"><p>ساعات تو میمون شد، ای دوست سلام علیک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم ز چه موزون شد؟ حال دل او چون شد؟</p></div>
<div class="m2"><p>در عشق تو مجنون شد، ای دوست سلام علیک</p></div></div>