---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>چون حسن دلاویز تو در جلوه گری بود</p></div>
<div class="m2"><p>کار دل بیچاره من پرده دری بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دور رخت یک دل هشیار ندیدیم</p></div>
<div class="m2"><p>این شیوه ز خاصیت دور قمری بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای جان جهان، نسبت یاد تو بجانم</p></div>
<div class="m2"><p>چون باد سحر بر رخ گل برگ طری بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و دل و دین برد زمن عشق تو،هیهات!</p></div>
<div class="m2"><p>در غارت عشق تو چنین حمله بری بود!</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جا که نظر کرد دلم روی ترا دید</p></div>
<div class="m2"><p>این نیز هم از غایت صاحب نظری بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درمان وصال تو فنا آمد و دیگر</p></div>
<div class="m2"><p>هر چاره که کردیم همه حیله گری بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتند که:قاسم همه از زهد زند لاف</p></div>
<div class="m2"><p>بیچاره خود از تهمت این قصه بری بود</p></div></div>