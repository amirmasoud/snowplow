---
title: >-
    شمارهٔ ۲۰۴
---
# شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>در شرح آن جمال بیان‌ها ز حد گذشت</p></div>
<div class="m2"><p>در حسن یار حیرت جان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نقطه دهان تو، کان سر نازکست</p></div>
<div class="m2"><p>کس را نشد یقین و گمان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نادیده یار را، به تصور حکایتی</p></div>
<div class="m2"><p>افتاد در زبان و زیان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عین حسن دلبر بی‌نام و بی‌نشان</p></div>
<div class="m2"><p>یک جلوه کرد، نام و نشان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زین بیش بی‌نقاب مرو در میان شهر</p></div>
<div class="m2"><p>ای دوست، الحذر، که فغان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای یار جان، که بر سر بازار عاشقی</p></div>
<div class="m2"><p>شاد آمدی و شادی جان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از فکر بر خیال تو ناایمنست شهر</p></div>
<div class="m2"><p>در ملک لایزال امان‌ها ز حد گذشت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وقتست تا قرین شود آن یار، قاسمی</p></div>
<div class="m2"><p>کز شدت فراق و قران‌ها ز حد گذشت</p></div></div>