---
title: >-
    شمارهٔ ۴۱۷
---
# شمارهٔ ۴۱۷

<div class="b" id="bn1"><div class="m1"><p>خاطرم آشفته و جان در ملال</p></div>
<div class="m2"><p>رو بنما، ای مه فرخنده فال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی تو عجب مضطربم روز و شب!</p></div>
<div class="m2"><p>مرغ دلم چند زند پر و بال؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بلبل شوریده دل، افغان مکن</p></div>
<div class="m2"><p>موسم هجران شد و آمد وصال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وصل بفریاد دل من رسید</p></div>
<div class="m2"><p>یافتم از هجر بسی گوشمال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل پس پرده ز همه فارغست</p></div>
<div class="m2"><p>بلبل، ازین حال دمی خوش بنال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل آشفته، شغب را بمان</p></div>
<div class="m2"><p>نوبت حالست، مکن قیل و قال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>واعظ ما قصه و افسانه گفت</p></div>
<div class="m2"><p>خواجه سمینست، نشد در جدال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواجه عزیزست، ولیکن نکرد</p></div>
<div class="m2"><p>از طرف تن سوی جان انتقال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی، از عین عیان قصه کن</p></div>
<div class="m2"><p>تا بکی اندیشه خواب و خیال؟</p></div></div>