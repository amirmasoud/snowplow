---
title: >-
    شمارهٔ ۱۲۳
---
# شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>در دیده صاحب نظران کشف عیانست :</p></div>
<div class="m2"><p>کان ماه دل افروز پس پرده نهانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زانکه بغفلت روی این ره، نکنی سود</p></div>
<div class="m2"><p>هر سود که بی دوست کنی عین زیانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرجا نگرم روی تو بینم بهمه حال</p></div>
<div class="m2"><p>گر خانه کعبه است و گر دیر مغانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد، هله! امروز تو در ملک امانی</p></div>
<div class="m2"><p>در کوچه ما نعره و آشوب و فغانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنیست در آن چهره زیبای تو مادام</p></div>
<div class="m2"><p>هرجای که آنست، دلم عاشق آنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دی رفت بهاری، همه پر لاله سیراب</p></div>
<div class="m2"><p>دهقان چه کند چاره؟ که امروز خزانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گویند بقاسم که: ازین عشق حذر کن</p></div>
<div class="m2"><p>بیچاره حذر کرد، ولیکن نتوانست</p></div></div>