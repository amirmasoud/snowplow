---
title: >-
    شمارهٔ ۴۹۷
---
# شمارهٔ ۴۹۷

<div class="b" id="bn1"><div class="m1"><p>یارم ز در در آمو، وشتن کنید وشتن</p></div>
<div class="m2"><p>این خانه را بوشتن گلشن کنید، گلشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارم ز در درآمد، با حسن و زیب و با فر</p></div>
<div class="m2"><p>گفتم وفا نداری، خندید و گفت:«سن سن »</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای پادشاه جانها وی راحت روانها</p></div>
<div class="m2"><p>اول «سنی سیورم » آخر «سنی سیورمن »</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خانقاه صورت، در گوشه های معنی</p></div>
<div class="m2"><p>هم بوده ایم با تو در دیرهای ارمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من مست عشق یارم، مشتاق آن نگارم</p></div>
<div class="m2"><p>از من مپرس، باری، اوصاف عشق ذوالمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل حیات خواهی؟ روی نجات خواهی؟</p></div>
<div class="m2"><p>این عشق ایزدی را دیدن کنید، دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم، خیال بازی، در حالت نیازی</p></div>
<div class="m2"><p>یک دم قدم برون نه زین خانه ملون</p></div></div>