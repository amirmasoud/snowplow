---
title: >-
    شمارهٔ ۴۱۸
---
# شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>ما گنج قدیمیم درین دیر کهن سال</p></div>
<div class="m2"><p>ما را چه بود گر بشناسی بهمه حال؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خواجه سر سال شد و نوبت مستیست</p></div>
<div class="m2"><p>مستان خرابیم، نه امسال چو هر سال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معشوق چو جانست و ندانم که چه جانست؟</p></div>
<div class="m2"><p>هر جا که رود میرودش عشق بدنبال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنجا که سراپرده اجلال تو باشد</p></div>
<div class="m2"><p>جانها همه مستند، اگر رستم، اگر زال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از روی دل افروز تو جان را نتوان برد</p></div>
<div class="m2"><p>وان زلف سیه رنگ تو دالست برین، دال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مدرسه و صومعه گردیدم و دیدم:</p></div>
<div class="m2"><p>آنجا همه قال آمد و اینجا همه احوال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قوال چه خوش گفت که: جز دوست کسی نیست</p></div>
<div class="m2"><p>قاسم بسماع آمد از گفته قوال</p></div></div>