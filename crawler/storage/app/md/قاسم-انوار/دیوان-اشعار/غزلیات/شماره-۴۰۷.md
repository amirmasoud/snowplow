---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>متمادی شدست یوم فراق</p></div>
<div class="m2"><p>«کیف احوال؟ ایها العشاق »</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد ما را مگر دوایی نیست؟</p></div>
<div class="m2"><p>که تو بس فارغی و ما مشتاق!</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ریشم ز دوست مرهم یافت</p></div>
<div class="m2"><p>مدعی ریش می کند ز نفاق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان در وصال مستغرق</p></div>
<div class="m2"><p>بهوس نی، ولی باستحقاق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لذت عشق را نمی دانی</p></div>
<div class="m2"><p>که نداری بهیچ گونه مذاق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز، چون شب گذشت و روز آمد</p></div>
<div class="m2"><p>نور توحید می کند اشراق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، سر عشق می طلبی؟</p></div>
<div class="m2"><p>در دل خود طلب، نه در اوراق</p></div></div>