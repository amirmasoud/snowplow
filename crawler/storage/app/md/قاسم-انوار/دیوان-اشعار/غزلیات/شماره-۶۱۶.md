---
title: >-
    شمارهٔ ۶۱۶
---
# شمارهٔ ۶۱۶

<div class="b" id="bn1"><div class="m1"><p>ای آفتاب روی ترا ماه مشتری</p></div>
<div class="m2"><p>جانش مباد، هرکه کند از تو دل بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای جام اگر ز باده نداری تو چاشنی</p></div>
<div class="m2"><p>من بر کفت نگیرم، اگر کاسه زری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این راه عشق شیوه انسست و هیبتست</p></div>
<div class="m2"><p>مأمور عشق گردی، اگر خود غضنفری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای پادشاه عالم جان، دل نگاه دار</p></div>
<div class="m2"><p>دل کشور تو گشت و تو سلطان کشوری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راهیست بی نهایت و شیران در آن کمین</p></div>
<div class="m2"><p>از سر سخن مگو، مگر این ره بسر بری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اندر پناه عشق تو ایمن شدست دل</p></div>
<div class="m2"><p>ای عشق، چاره ساز، که سد سکندری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاجز شد از ثنای تو قاسم بصد زبان</p></div>
<div class="m2"><p>کز هرچه برتر آمد ازان نیز برتری</p></div></div>