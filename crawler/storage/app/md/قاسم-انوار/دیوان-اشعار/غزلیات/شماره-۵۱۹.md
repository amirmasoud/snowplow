---
title: >-
    شمارهٔ ۵۱۹
---
# شمارهٔ ۵۱۹

<div class="b" id="bn1"><div class="m1"><p>سرگشته ایم و حیران در کوی مهرورزان</p></div>
<div class="m2"><p>زان زلفهای میگون، زان چشمهای فتان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان شیوه و ملاحت، زان حسن و زان صباحت</p></div>
<div class="m2"><p>واله شدیم، واله، حیران شدیم، حیران</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کس بروی و راهی، رفتند پیش شاهی</p></div>
<div class="m2"><p>عاشق بکل فنا شد، در عشق روی جانان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر رشته مان شد از دست، حیران شدیم و سرمست</p></div>
<div class="m2"><p>باشد بدست آید، سررشته مان بدستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مذهب حقیقت صد بار بهتر ارزد</p></div>
<div class="m2"><p>این خاک می پرستان از خون خودپرستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن خواجه معظم، خوشحال و شاد و خرم</p></div>
<div class="m2"><p>لیکن خبر ندارد از حال درد نوشان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از عشق چون نترسم؟ کین عشق اژدها وش</p></div>
<div class="m2"><p>قعریست پر ز آتش، بحریست پر ز توفان</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ناله همچو نالم ریزید پر و بالم</p></div>
<div class="m2"><p>در حالت محالم، مسکین دل غریبان!</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هرکس بعشق یاری آشفته است باری</p></div>
<div class="m2"><p>آشفته است قاسم زان طره پریشان</p></div></div>