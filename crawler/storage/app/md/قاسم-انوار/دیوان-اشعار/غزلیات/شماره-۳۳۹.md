---
title: >-
    شمارهٔ ۳۳۹
---
# شمارهٔ ۳۳۹

<div class="b" id="bn1"><div class="m1"><p>منم و چشم رمد دیده و نور خورشید</p></div>
<div class="m2"><p>بتور روشن،بهمه حال، مرا چشم امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی زیبای تو فرخنده و رخشان دیدم</p></div>
<div class="m2"><p>بی نصیبست ازین عین عیان چشم سفید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ما خاک ره درد کشان خواهد بود</p></div>
<div class="m2"><p>واعظ، افسانه مفرما، که نمانی جاوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدح باده بدست آر،اگر دست دهد</p></div>
<div class="m2"><p>خوشتر از تخت فریدون و زتاج جمشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بکی در هوس قصر معلا بودن؟</p></div>
<div class="m2"><p>قصر جان را تو بدست آرو مجو قصر مشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راه را اهل طریقت بمشقت رفتند</p></div>
<div class="m2"><p>تو فراغت بنشسته بمیان گل و بید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گشت هجران تو بر خاطر قاسم ممدود</p></div>
<div class="m2"><p>این چنین قصه ممدود بعالم که شنید؟</p></div></div>