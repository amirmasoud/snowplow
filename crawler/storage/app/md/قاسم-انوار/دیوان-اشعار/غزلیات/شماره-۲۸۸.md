---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>چون ماه نو از مشرق انوار برآمد</p></div>
<div class="m2"><p>فریاد ز اسلام وز کفار برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنت سخنی گفت بگلذار و ریاحین</p></div>
<div class="m2"><p>ریحان بخجالت شد و گل زار بر آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق تو چو افتاد بسر حلقه مستان</p></div>
<div class="m2"><p>از حلقه مستان همه انوار برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوقت گذری کرد بکاشانه رندان</p></div>
<div class="m2"><p>«صدق » ز دل مست و ز هشیار برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادند جهانی و چه گویم که چه شادند؟</p></div>
<div class="m2"><p>زین مشغله کز که گل فخار بر آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین پیش دو عالم همه ز اغیار تهی بود</p></div>
<div class="m2"><p>چون کرد ظهور این همه اظهار برآمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتند که:قاسم طلب وصل تو دارد</p></div>
<div class="m2"><p>در حال و زمان لمعه دیدار برآمد</p></div></div>