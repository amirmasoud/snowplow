---
title: >-
    شمارهٔ ۶۳۵
---
# شمارهٔ ۶۳۵

<div class="b" id="bn1"><div class="m1"><p>چه غمست آخر از غم؟ چو تو در میانه باشی</p></div>
<div class="m2"><p>غم جانانه باشد چو تو در میان نباشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می فیض فضل جانان نرسد بکامت، ای جان</p></div>
<div class="m2"><p>اگر از میان گریزی وگر از کرانه باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر از غرور مستی، نرسی بملک هستی</p></div>
<div class="m2"><p>تو کجا حریف آن رطل می مغانه باشی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن از سر صفا گو، ز صفات یار ما گو</p></div>
<div class="m2"><p>چه شدت؟ چو بودت آخر؟ که هم فسانه باشی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه ذل و پستی تو، ز چه بد؟ ز هستی تو</p></div>
<div class="m2"><p>چو ز خویش فرد گشتی ز جهان یگانه باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفسی نکو نظر کن، تو ز خویشتن سفر کن</p></div>
<div class="m2"><p>که تو هم خزانه داری و تو هم خزانه باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بمیان دشت و صحرا، بکنار جوی دیدم</p></div>
<div class="m2"><p>چو بشهر باز جویم بمیان خانه باشی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز قبول خلق مستی ز هوای خود پرستی</p></div>
<div class="m2"><p>اگر این چنین بمانی صنم زمانه باشی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هله! قاسمی، که مرغان همه ظلمتند و عدوان</p></div>
<div class="m2"><p>بچنین زبان همان به که بر آشیانه باشی</p></div></div>