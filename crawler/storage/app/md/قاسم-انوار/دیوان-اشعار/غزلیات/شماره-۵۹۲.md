---
title: >-
    شمارهٔ ۵۹۲
---
# شمارهٔ ۵۹۲

<div class="b" id="bn1"><div class="m1"><p>بیا، ای ماه کنعانی، بیا ای شاه فرزانه</p></div>
<div class="m2"><p>نمی دانم چه می گویم؟ که عقلم گشت دیوانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عجب حیران و سرمستم، بگیر، ای جان و دل، دستم</p></div>
<div class="m2"><p>که از مستی و حیرانی نمی دانم ره خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بکوی عاشقی پستم، جنون افتاده در دستم</p></div>
<div class="m2"><p>ز سودای تو سرمستم، چه جای جام و پیمانه؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر در کعبه و دیری، رهین رؤیت غیری</p></div>
<div class="m2"><p>همه ذکر تو افسون شد، همه فکر تو افسانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و دیده روشن کن، بیا و خانه گلشن کن</p></div>
<div class="m2"><p>تو شمع مجلس جانی و جانها جمله پروانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درآ در وادی حیرت، برای هیبت و قربت</p></div>
<div class="m2"><p>رها کن شیوه غفلت، چو می دانی که می دانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>امید قاسم مسکین بجانانست پیوسته</p></div>
<div class="m2"><p>که آن دلدار موری را سر مویی نرنجانه</p></div></div>