---
title: >-
    شمارهٔ ۶۳۰
---
# شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>تو همچو عقل شریفی و همچو روح عزیزی</p></div>
<div class="m2"><p>«فداک عقلی و روحی » ندانمت که چه چیزی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا هوای تو از عقل و جان ربود، چه گویم؟</p></div>
<div class="m2"><p>بجانب تو گریزم بهر طرف که گریزی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تویی مقاصد عالم، یقین بدان و«فألزم »</p></div>
<div class="m2"><p>تمیز راه عیان شد، اگر ز اهل تمیزی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برغم خویش تو مستی، برو که دوری ازین در</p></div>
<div class="m2"><p>نه مست جام خدایی ولیک مست قمیزی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا بمجلس مستان، بروی عشق نظر کن</p></div>
<div class="m2"><p>هزار جان متحیر، چه جای عقل غریزی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جام شوق و محبت خبر نداری و مستی</p></div>
<div class="m2"><p>نه مست باده شوقی، که مست جوز و مویزی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هوای عشق تو دارم، بهر طرف که رخ آرم</p></div>
<div class="m2"><p>ولی بگفت نیاید، که نیک تندی و تیزی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز ذره های من آید هوای مهر و محبت</p></div>
<div class="m2"><p>اگر تو خاک مرا صد هزار بار ببیزی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز خاک کوی تو قاسم بجانبی نگریزد</p></div>
<div class="m2"><p>هزار بار اگر خون من بخاک بریزی</p></div></div>