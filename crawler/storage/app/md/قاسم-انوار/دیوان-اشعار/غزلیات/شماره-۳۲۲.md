---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>کسی که شیوه حکمت گرفت گوی ربود</p></div>
<div class="m2"><p>به حکمتست حکایت،نه کار سعی و جهود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برسم مردم عاقل زبان نگه می دار</p></div>
<div class="m2"><p>که غافلان حسودند و منکران جحود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز پیر دهقان بشنو،که نیک می گوید:</p></div>
<div class="m2"><p>کسی که تخم نکو کاشت تخم بد ندرود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانیان به جهان آب خضر می جویند</p></div>
<div class="m2"><p>و لیک قسمت آن کس شود که روزی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا که بودن و نابود هر دو یکسانست</p></div>
<div class="m2"><p>چه حاصلست ز افسانهای کور و کبود؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز چه روی لطف دلم را بخود پناهی ده</p></div>
<div class="m2"><p>بجاه و حرمت رندان عاقبت محمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار جان و دل قاسمی فدای تو باد</p></div>
<div class="m2"><p>که آفتاب یقینی و شاهد و مشهود</p></div></div>