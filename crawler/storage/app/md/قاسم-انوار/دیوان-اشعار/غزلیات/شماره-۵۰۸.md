---
title: >-
    شمارهٔ ۵۰۸
---
# شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>بیا، که عشق برافراخت سنجق سلطان</p></div>
<div class="m2"><p>بیا، که شهر شد ایمن ز حیله شیطان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار شهر بگردیدم از فلک بفلک</p></div>
<div class="m2"><p>بغیر حضرت السان نیافتم همه دان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار عاشق صادق بدند احمد را</p></div>
<div class="m2"><p>ولیک کمتر افتد چو بوذر و سلمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر بتوبه و تقوی طریقتی بروی</p></div>
<div class="m2"><p>وگر نه راه نیابی بکوچه سلطان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم چو گم شود از کوی یار واطلبید</p></div>
<div class="m2"><p>دگر مجوی دلم را ز روضه رضوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز عزت و عظمت خود بهیچ پروا نیست</p></div>
<div class="m2"><p>ترا، چنانکه تویی، بر جمال خود نگران</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو قاسمی ز غمت خوشدل و سبکبارست</p></div>
<div class="m2"><p>خدای رحم کند بر دل سبک باران</p></div></div>