---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>با ما سخن از خرقه و سجاده مگویید</p></div>
<div class="m2"><p>از ما بجز از شیوه مستانه مجویید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کعبه بهر چار جهت رو بهم آرند</p></div>
<div class="m2"><p>در دایره وحدت حق روی برویید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خاصه عشقید بجز عشق مدانید</p></div>
<div class="m2"><p>گر عاشق یارید بجز یار مگویید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک منزل دیگر زلب جوی بدریاست</p></div>
<div class="m2"><p>در لجه دریا طرف جوی مجویید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر عاشق یارید درین کو بخرامید</p></div>
<div class="m2"><p>گر گلبن عشقید درین روضه برویید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کس مهراسید که در حفظ خدایید</p></div>
<div class="m2"><p>خود را بشناسید، که زیبا و نکویید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیرون ز شما نیست، اگر صورت و معنیست</p></div>
<div class="m2"><p>زنهار که از جانب بیهوده مپویید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قاسم، ره تقلید خیالست و محالست</p></div>
<div class="m2"><p>در باغ جهان گلبن تقلید مبویید</p></div></div>