---
title: >-
    شمارهٔ ۴۷۳
---
# شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>ما عاشق و رند و پاکبازیم</p></div>
<div class="m2"><p>در قبله عشق در نمازیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سوز بمانده ایم چون عود</p></div>
<div class="m2"><p>در چنگ غمیم، تا چه سازیم؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ذره ای از وجود باقیست</p></div>
<div class="m2"><p>در بوته عشق می گدازیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما را سگ کوی خویشتن خواند</p></div>
<div class="m2"><p>شاید که بدین شرف بنازیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچند حبیب ناز دارد</p></div>
<div class="m2"><p>ما معتکف در نیازیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رندیم و قمارباز، اما</p></div>
<div class="m2"><p>در ششدر عشق کژ نبازیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جان چو ارغنون قاسم</p></div>
<div class="m2"><p>صد پرده راز می نوازیم</p></div></div>