---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>دل ز داروخانه دردت دوا دارد امید</p></div>
<div class="m2"><p>چشم جان از خاک پایت توتیا دارد امید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهدان از دولت درد نو غافل مانده اند</p></div>
<div class="m2"><p>این سعادت را ز عشقت جان ما دارد امید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب درد و جفاهای تو میخواهد دلم</p></div>
<div class="m2"><p>راستی را دولت بی منتها دارد امید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خسته تیغ غمت را کی بود مرهم طمع؟</p></div>
<div class="m2"><p>دردمند عشق تو درمان چرا دارد امید؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بارها در خون نشست این دل ز تیر غمزه ات</p></div>
<div class="m2"><p>بازش اندر خون نشان، گر خون بها دارد امید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان گدایی می کند درد از تو وین نبود عجب</p></div>
<div class="m2"><p>گر گدایی رحمتی از پادشا دارد امید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفرین بر همت قاسم، که از ملک دو کون</p></div>
<div class="m2"><p>منصب خاک سر کوی ترا دارد امید</p></div></div>