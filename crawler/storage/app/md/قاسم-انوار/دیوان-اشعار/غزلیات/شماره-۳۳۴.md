---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>گاهی درون پرده عزت نهان شود</p></div>
<div class="m2"><p>گاهی هزار پرده بدرد،عیان شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گاهی درون پرده جهانی بهم زند</p></div>
<div class="m2"><p>گاهی برون پرده جهان در جهان شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه در طریق عزت امان زمین بود</p></div>
<div class="m2"><p>گاهی درون پرده امام زمان شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گاهی امین مدرسه و خانقه بود</p></div>
<div class="m2"><p>گاهی امیر کوچه دردی کشان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی غمش برای دلم ارغنون زند</p></div>
<div class="m2"><p>گاهی بحسن و لطف گل ارغوان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او بی نشان و جمله عالم نشان اوست</p></div>
<div class="m2"><p>گه در نشان نماید و گه بی نشان شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پرسدم که قاسم مسکین ما کجاست؟</p></div>
<div class="m2"><p>رویم ازین شرف چو مه آسمان شود</p></div></div>