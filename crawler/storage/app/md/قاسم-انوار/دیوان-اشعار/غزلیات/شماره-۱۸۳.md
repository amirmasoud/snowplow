---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>بی یاد دوست در دل مستان سرور نیست</p></div>
<div class="m2"><p>بی روی او بکعبه و بت خانه نور نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند قدس ذات ز اشیا منزهست</p></div>
<div class="m2"><p>در هیچ ذره نیست که حق را ظهور نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>واعظ ز من برآ و مگو قصه منبری</p></div>
<div class="m2"><p>بگذر ازین مقام، که جای حضور نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آفتاب حسن جهانگیر جلوه کرد</p></div>
<div class="m2"><p>این جلوه را ببیند هرکس که کور نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان را حیات داد، دل و دیده را جلا</p></div>
<div class="m2"><p>این عشق چاره ساز کم از نفخ صور نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد بزهد و توبه و تقوی مزینست</p></div>
<div class="m2"><p>چون نیست نیست، نشائه او بی غرور نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در راه آشنایی و اسرار معرفت</p></div>
<div class="m2"><p>جانی که غیربین بود، آن جان غیور نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در عاشقی گریز، که دارالامان هموست</p></div>
<div class="m2"><p>کانجا همه هدایت حقست و زور نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم، بهشت حضرت حق را بجان طلب</p></div>
<div class="m2"><p>کان جلوه گاه حور و مقام قصور نیست</p></div></div>