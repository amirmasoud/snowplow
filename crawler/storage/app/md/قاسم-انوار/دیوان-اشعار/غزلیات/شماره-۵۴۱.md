---
title: >-
    شمارهٔ ۵۴۱
---
# شمارهٔ ۵۴۱

<div class="b" id="bn1"><div class="m1"><p>بیتی همی سرایم و زاریست کار من</p></div>
<div class="m2"><p>تا یک نظر کند بمن آن یار غار من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منصوروار بر سر دار ملامتم</p></div>
<div class="m2"><p>وین دار گشت قصه دار العیار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آرزوی روی خودم بیش ازین مدار</p></div>
<div class="m2"><p>کز حد گذشت واقعه انتظار من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان غم رسیده، ز دلدار شرم دار</p></div>
<div class="m2"><p>چون اختیار ازوست مگو اختیار من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل تو جنتست و دو عالم طفیل اوست</p></div>
<div class="m2"><p>هجر تو دوزخ آمد و دارالبوار من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای جان و دل، حکایت هجران ز حد گذشت</p></div>
<div class="m2"><p>افزون شدست درد دل بی قرار من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر جان قاسمی نظری کن روا مدار</p></div>
<div class="m2"><p>کین عنکبوت چشم شود پرده دار من</p></div></div>