---
title: >-
    شمارهٔ ۴۸۷
---
# شمارهٔ ۴۸۷

<div class="b" id="bn1"><div class="m1"><p>ما عشق یار را بدو عالم نمیدهیم</p></div>
<div class="m2"><p>جامی ز دست دوست بصد جم نمیدهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما عاشقان روی حبیبیم و عاقبت</p></div>
<div class="m2"><p>دار الجمال را بجهنم نمیدهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن گوشه را که دیر مغانست و ما درو</p></div>
<div class="m2"><p>رکنی از آن بگنبد اعظم نمیدهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما آیتی بمکتب عشق تو خوانده ایم</p></div>
<div class="m2"><p>معنی آن بمحکم و مبرم نمیدهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خرقه پوش پیر مغانیم در طریق</p></div>
<div class="m2"><p>این کسوه را بشیخ معمم نمیدهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما تشنگان بحر محیطیم، قاسمی</p></div>
<div class="m2"><p>در بحر عشق آب بآدم نمیدهیم</p></div></div>