---
title: >-
    شمارهٔ ۶۳۳
---
# شمارهٔ ۶۳۳

<div class="b" id="bn1"><div class="m1"><p>دلا، ز باده و خُم‌خانهٔ که می‌پرسی؟</p></div>
<div class="m2"><p>ز چشم و غمزهٔ مستانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار خانه برانداخت عشق عالم‌سوز</p></div>
<div class="m2"><p>درین خرابه تو از خانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گذشت نوبت کیخسرو و فریدون شد</p></div>
<div class="m2"><p>درین دیار تو افسانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو جان جمله جهانی و شاه موجودات</p></div>
<div class="m2"><p>به جان تو که ز جانانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هزار بحر پر از موج درناب خوشاب</p></div>
<div class="m2"><p>گدای تست، ز دردانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تراست رُتبَتِ «سبحانی اعظم الشانی»</p></div>
<div class="m2"><p>ز جعد گیسو و از شانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گذشت قصر جلالت ز سقف عرش مجید</p></div>
<div class="m2"><p>زهی کمال! ز کاشانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تو شمع جانی و جان جهان چو پروانه</p></div>
<div class="m2"><p>درین میانه ز پروانهٔ که می‌پرسی؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به پیش قاسم بی‌دل، که بحر جرعه اوست</p></div>
<div class="m2"><p>ز جام باده و پیمانهٔ که می‌پرسی؟</p></div></div>