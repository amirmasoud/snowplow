---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>نی چو بنالید، بگفتا: نیم</p></div>
<div class="m2"><p>باده بجوش آمد و گفتا: میم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق و وفا گفت که: من ثابتم</p></div>
<div class="m2"><p>فقر و فنا گفت که: من لاشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبت شادیست، گه عشرتست</p></div>
<div class="m2"><p>باده بنوشیم بپهنای یم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز کف ساقی جان می خوری</p></div>
<div class="m2"><p>بر سر افلاک بر آری علم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من نشناسم ملک الموت چیست؟</p></div>
<div class="m2"><p>جان بسوی حضرت جانان دهم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رو ننماید بتو از هیچ روی</p></div>
<div class="m2"><p>تا نکنی در طلبش سر قدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من نتوانم که گریزم ز عشق</p></div>
<div class="m2"><p>بر سر قاسم قلم این زد رقم</p></div></div>