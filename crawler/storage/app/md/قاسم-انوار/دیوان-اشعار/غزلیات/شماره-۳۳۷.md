---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>دل ما باده طلب کرد و شرابش برسید</p></div>
<div class="m2"><p>برسد چون برسد سابقه حبل ورید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زان زمانی که ترا دیدم و دانستم باز</p></div>
<div class="m2"><p>دل و جانم بهوای تو ز اغیار رمید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ما ساکن درگاه تو خواهد بودن</p></div>
<div class="m2"><p>عزتش دار،که بیچاره سعیدست و شهید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کرمی باشد،اگر زنده جاوید کنی</p></div>
<div class="m2"><p>دل عشاق که در عشق فریدند و وحید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقانت همه بر خاک نهادند جبین</p></div>
<div class="m2"><p>چون ز پیشانی تو صبح سعادت بدمید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دل ما بهوایت طلبی، هم دل ماست</p></div>
<div class="m2"><p>دل ما کز همه عالم بهوایت ببرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسمی، قصه هجران نتوان گفت به کس</p></div>
<div class="m2"><p>کین چنین قصه بعالم نتوان گفت و شنید</p></div></div>