---
title: >-
    شمارهٔ ۸۰
---
# شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>دلدار یار ماست، غمش غمگسار ماست</p></div>
<div class="m2"><p>در غار وحدتیم و همو یار غار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما شیر شرزه ایم درین عرصه وجود</p></div>
<div class="m2"><p>گرگی که اندرین گله بینی ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ما دگر بچشم حقارت نظر مکن</p></div>
<div class="m2"><p>ما انتظار دوست، جهان انتظار شکار ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آتش فراق بیک بار سوختیم</p></div>
<div class="m2"><p>شمع رخت کجاست؟ که شب زنده دار ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم می خوریم و هیچ شکایت نمی کنیم</p></div>
<div class="m2"><p>ما را چه غم ز غم؟ که غمت غمگسار ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی گلشن وصال تو سرسبز نیستیم</p></div>
<div class="m2"><p>بنمای آن جمال، که باغ و بهار ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که: کیست قاسمی؟ ای آرزوی جان</p></div>
<div class="m2"><p>گفتند: عاشقیست که زار و نزار ماست</p></div></div>