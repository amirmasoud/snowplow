---
title: >-
    شمارهٔ ۱۲۵
---
# شمارهٔ ۱۲۵

<div class="b" id="bn1"><div class="m1"><p>دلم از زلف تو آشفته و سرگردانست</p></div>
<div class="m2"><p>جان بدیدار تو شادست ولی حیرانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق دریای محیطست، بتحقیق بدان</p></div>
<div class="m2"><p>جدول اوست، اگر قلزم، اگر عمانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با من از دوزخ و فردوس مگویید سخن</p></div>
<div class="m2"><p>هرکجا اوست، مرا جنت جاویدانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل از دوست مباشید و بغفلت مروید</p></div>
<div class="m2"><p>در نهان خانه وحدت قمری پنهانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش مستان طریقت سخنی می گویید</p></div>
<div class="m2"><p>هرکه او منکر عشقست یقین شیطانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقست این دل شوریده من، درمان چیست؟</p></div>
<div class="m2"><p>بس عجب نبود اگر بی سر و بی سامانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم ار جامه درید از غم او باکی نیست</p></div>
<div class="m2"><p>نعره و جامه دریدن صفت مستانست</p></div></div>