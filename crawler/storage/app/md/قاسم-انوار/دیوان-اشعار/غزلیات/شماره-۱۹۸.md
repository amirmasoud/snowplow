---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>ز بحر عشق تو هر قطره ای چو دریاییست</p></div>
<div class="m2"><p>بکوی وصل تو هر پشه ای چو عنقاییست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار دیده کنم وام، اگر توانم کرد</p></div>
<div class="m2"><p>که در جمال تو هر دیده را تماشاییست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل مرا بهوای تو ذوق سربازیست</p></div>
<div class="m2"><p>مقررست که در هر دلی تمناییست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهیچ رو نبرم ره بکوی آزادی</p></div>
<div class="m2"><p>مرا که هر سر مویی اسیر سوداییست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگر بگوشه چشمی نظر بمستان کرد</p></div>
<div class="m2"><p>میان شهر بهر گوشه شور و غوغاییست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن بلند شد، اکنون بلند می گویم</p></div>
<div class="m2"><p>که: خاطرم بهوای بلند بالاییست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلند بالا یعنی رفیع قدر جلیل</p></div>
<div class="m2"><p>چنین شناسد هرجا که عقل داناییست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو لفظ اسم شنیدی پی مسما شو</p></div>
<div class="m2"><p>که قول مردم شوریده دل معماییست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بگو بقاسم: در کوی عشق جا کردی</p></div>
<div class="m2"><p>نگاه دار ادب را، که بس عجب جاییست</p></div></div>