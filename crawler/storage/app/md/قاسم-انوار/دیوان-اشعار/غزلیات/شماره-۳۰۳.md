---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>هر گه که یار شیوه ناز ابتدی کند</p></div>
<div class="m2"><p>عاشق کسی بودکه دل وجان فدی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ شد از جهان، بحیات ابد رسید</p></div>
<div class="m2"><p>هر جان معتقد که بعشق اقتدی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از معنی آمدن سوی صورت بدان که چیست</p></div>
<div class="m2"><p>معشوق «الست » گوید و عاشق بلی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غافل مشو، که مایه ظلمات غافلیست</p></div>
<div class="m2"><p>با یاد دوست باش،که جان را جلی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با یاد دوست از بد دشمن فراغتیم</p></div>
<div class="m2"><p>با خود کند کسی که بعالم بدی کند؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیدار شو ز قده غفلت، که عشق یار</p></div>
<div class="m2"><p>بعد از وفات قبر ترا مرقدی کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حسنیست بی نهایت و لطفیست بی دریغ</p></div>
<div class="m2"><p>هنگام فرصتست که قاسم کدی کند</p></div></div>