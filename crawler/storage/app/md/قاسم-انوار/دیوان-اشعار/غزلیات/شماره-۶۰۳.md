---
title: >-
    شمارهٔ ۶۰۳
---
# شمارهٔ ۶۰۳

<div class="b" id="bn1"><div class="m1"><p>به جان تو، که خمارم به غایت، ای چلبی</p></div>
<div class="m2"><p>بریز باده حمرا به شیشه حلبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو مست جام «اناالحق » شوم، چنانکه مپرس</p></div>
<div class="m2"><p>هم از تو در تو گریزم ز شرم بی ادبی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه حسن سبب را بجان خریدارم</p></div>
<div class="m2"><p>بدان سبب که تو، ای جان، مسبب سببی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر بعشق شوی آشنا عیان بینی</p></div>
<div class="m2"><p>هزار شیوه شیرین، هزار بوالعجبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو حبیب عجم را که: غافل از عربیست</p></div>
<div class="m2"><p>زبان او عجم آمد، روان او عربی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار درد درون داشتم، چو رو بنمود</p></div>
<div class="m2"><p>«فزال لی تعبی » و «و زاد لی طربی »</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نسب حقیقت عشقست، اگر خبر داری</p></div>
<div class="m2"><p>مگو بمجلس مستان فسانه نسبی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو آفتاب برقصیم و مست و خوش حالیم</p></div>
<div class="m2"><p>برو، تو ناصح، ازین جا، که عقده ذنبی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نیاز قاسم بیچاره از کرم بپذیر</p></div>
<div class="m2"><p>«حبیبی، انت رجایی و انت منقلبی »</p></div></div>