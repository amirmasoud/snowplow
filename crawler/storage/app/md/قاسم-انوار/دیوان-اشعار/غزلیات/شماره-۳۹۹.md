---
title: >-
    شمارهٔ ۳۹۹
---
# شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>جان هوادار تو شد، فاش مکن اسرارش</p></div>
<div class="m2"><p>دل به سودای تو افتاد، گرامی دارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق یاغی شد و با ما سر غارت دارد</p></div>
<div class="m2"><p>وصل را گو که: عنایت کن و وامگذارش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبتدی را بکرم جرعه تصدق فرما</p></div>
<div class="m2"><p>منتهی را مده آن جرعه ولی خم آرش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه در شیوه عرفان حق خود را بشناخت</p></div>
<div class="m2"><p>گر همه نیر چرخست، منه مقدارش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل من خسته زلفین کمان ابروییست</p></div>
<div class="m2"><p>در چنین حال مگر هم تو کنی تیمارش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یا رب، این مرغ اجل طرفه عجایب مرغیست</p></div>
<div class="m2"><p>خورد خون همه و سرخ نشد منقارش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قاسم از جان حقیقت خبری باز نیافت</p></div>
<div class="m2"><p>هرکه را نیست بدل داعیه دیدارش</p></div></div>