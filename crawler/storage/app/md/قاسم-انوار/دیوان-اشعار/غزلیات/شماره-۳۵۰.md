---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>موسی بکوه طور بنور عیان رسید</p></div>
<div class="m2"><p>توفیق وصل یار عنان در عنان رسید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادند اهل عالم و هنگام شادیست</p></div>
<div class="m2"><p>کاندر زمانه مهدی آخر زمان رسید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آسوده ایم و خاطر ما شاد و خرمست</p></div>
<div class="m2"><p>چون فیض فضل یار جهان در جهان رسید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر خداست آدم و ابلیس کور بود</p></div>
<div class="m2"><p>هر سر که سر بدید بگنج نهان رسید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سری که کاینات بجان طالب ویند</p></div>
<div class="m2"><p>منت خدا را که بما رایگان رسید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما ناگهان بکوی خرابات سر زدیم</p></div>
<div class="m2"><p>چون جذب یار بر دل ما ناگهان رسید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشنید هر که گوش و دلی داشت، قاسمی</p></div>
<div class="m2"><p>گلبانگ وصل او، که بکون و مکان رسید</p></div></div>