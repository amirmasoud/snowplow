---
title: >-
    شمارهٔ ۵۵۴
---
# شمارهٔ ۵۵۴

<div class="b" id="bn1"><div class="m1"><p>جعل را چند ازین تحسین و تمکین؟</p></div>
<div class="m2"><p>جعل اماره راهست و بد دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جعل را گفتم: از سرگین گذر کن</p></div>
<div class="m2"><p>بساتینست در صحن بساتین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جعل گفتا که: چون سرگین ببویم</p></div>
<div class="m2"><p>مرا خوشتر ز تشمیم ریاحین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چنانم بوی سرگین تازه دارد</p></div>
<div class="m2"><p>که شبنم در سحر بر برگ نسرین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جعل خود راست می گوید، چه گویم؟</p></div>
<div class="m2"><p>که خود اصلش ز سرگینست و چامین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جعل در اصل خودضالست اما</p></div>
<div class="m2"><p>ندارد آدمی این رسم و آیین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جعل گر آدمی بودی نبودی</p></div>
<div class="m2"><p>ز طبع خویشتن در سجن سجین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جعل در اصل ذاتش کور بهتر</p></div>
<div class="m2"><p>که کوری بهتر است از چشم کژبین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ریز، ای ساقی جان، بهر قاسم</p></div>
<div class="m2"><p>شراب ارغوان در جام زرین</p></div></div>