---
title: >-
    شمارهٔ ۴۶۳
---
# شمارهٔ ۴۶۳

<div class="b" id="bn1"><div class="m1"><p>این عنایت ازلی بود که ره پرسیدیم</p></div>
<div class="m2"><p>وین هدایت ابدی گشت که رویت دیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو بلبل ز غم روی تو گریان بودیم</p></div>
<div class="m2"><p>چون گل روی تو دیدیم چو گل خندیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهوایی که نشانی ز تو یابیم مگر</p></div>
<div class="m2"><p>همچو پرگار بسر گرد جهان گردیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز تمنای تو حظی ز جهان نگرفتیم</p></div>
<div class="m2"><p>غیر سودای تو سودی بجهان نگزیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدد عشق تو خواهیم بهرحال که هست</p></div>
<div class="m2"><p>عمرها رفت که ما در پی این تاییدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو گویی: بتمنای من از دین برگرد</p></div>
<div class="m2"><p>دین ببازیم، چرا در غم این تقلیدیم؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پیش رفتیم بامید وصالی که نشد</p></div>
<div class="m2"><p>باز گشتیم، بخجلت پس سرخاریدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عید دیدار تو یک روز نصیب جان شد</p></div>
<div class="m2"><p>عمرها رفت که ما منتظر آن عیدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی، نیست حجابی، دل خود را بازآر</p></div>
<div class="m2"><p>خود حجابیم درین راه، ز خود ترسیدیم</p></div></div>