---
title: >-
    شمارهٔ ۳۱۴
---
# شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>زلفت شب قدرست،زهی سایه ممدود!</p></div>
<div class="m2"><p>رویت مه بدرست،زهی طالع مسعود!</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بادیه محنت هجران،شب تاریک</p></div>
<div class="m2"><p>بی نور رخت جان نبرد راه بمقصود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در باده و زاویه جز دوست ندیدیم</p></div>
<div class="m2"><p>این راه بدانستم و این بادیه پیمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از مسکن جانهاگل صد برگ بر آمد</p></div>
<div class="m2"><p>تا سنبل سیراب تو بربرگ سمن سود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از حسن هویدا شود این عشق جهان سوز</p></div>
<div class="m2"><p>این جا بشناسی صفت شاهد و مشهود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک غمزه ز تو دادن و صد جان و دل از ما</p></div>
<div class="m2"><p>بردند باقبال تو سودازدگان سود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حیران تو امروز نشد قاسم مسکین</p></div>
<div class="m2"><p>تا هست چنین باشد و تا بود چنین بود</p></div></div>