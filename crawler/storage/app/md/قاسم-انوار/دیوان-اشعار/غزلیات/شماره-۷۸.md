---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>جان گنه کار است و مجرم، رحمت جانان کجاست؟</p></div>
<div class="m2"><p>قصه طغیان ز حد شد، سوره غفران کجاست؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>محو گرداند گناه عالمی را در دمی</p></div>
<div class="m2"><p>یا رب آن موج کرم و آن بحر بی پایان کجاست؟</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قصه فرعونیان از حد گذشت،ای پیر عقل</p></div>
<div class="m2"><p>طالب جان را خبر کن،موسی عمران کجاست؟</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ظلمت بو جهل بگرفتست عالم سربسر</p></div>
<div class="m2"><p>درد بو دردا کجا شد؟ صفوت سلمان کجاست؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالمی اخوان شیطانند، با هم متفق</p></div>
<div class="m2"><p>آخر، ای دانا، نشان نشائه انسان کجاست؟</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از عطش جانها بلب آمد در این دریای ژرف</p></div>
<div class="m2"><p>ساقی باقی شناسد چشمه حیوان کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق سر مستست و میگوید بآواز بلند:</p></div>
<div class="m2"><p>ما بجانان واصلیم، آن عقل سرگردان کجاست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طاعت بی درد را هرگز نباشد چاشنی</p></div>
<div class="m2"><p>ناله مستان سرگردان بی سامان کجاست؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسمی از دیو مردم نفرتی دارد عظیم</p></div>
<div class="m2"><p>صولت غولان ز حد شد، صدمت سلطان کجاست؟</p></div></div>