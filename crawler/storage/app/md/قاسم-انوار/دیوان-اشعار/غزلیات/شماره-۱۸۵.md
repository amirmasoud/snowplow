---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>دل را ز جان گزیر وز جانان گزیر نیست</p></div>
<div class="m2"><p>غیر از هوای دوست نصیر و ظهیر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفی، که لاف نور کرامات میزند</p></div>
<div class="m2"><p>تا مست نور یار نشد مستنیر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسرار دوست را نشناسد بهیچ حال</p></div>
<div class="m2"><p>جانی که همچو آینه روشن ضمیر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واعظ، برو حکایت تقلید را بمان</p></div>
<div class="m2"><p>افسانه پیش اهل دلان دلپذیر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشمی که روی دوست نبیند بهیچ حال</p></div>
<div class="m2"><p>او مظهر تجلی اسم بصیر نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز بجذب خاطر تو میل ما نشد</p></div>
<div class="m2"><p>رو، رو، که باز ساعد شه موش گیر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان نصرت از تو خواهد و حیران تست عقل</p></div>
<div class="m2"><p>دل را بجز ولای تو نعم النصیر نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک دم بکوی ما بگذشتی و سالهاست</p></div>
<div class="m2"><p>در هیچ گوشه نیست که بوی عبیر نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قاسم بر آستان جلالت نهاده سر</p></div>
<div class="m2"><p>جز خاک آستان تو جان را مصیر نیست</p></div></div>