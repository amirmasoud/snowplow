---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>در همه بابی سخن را داد داد</p></div>
<div class="m2"><p>حجة الاسلام غزالی راد</p></div></div>