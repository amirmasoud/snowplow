---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>قاسمم، قاسم انوار، که اسرار ازل</p></div>
<div class="m2"><p>نیست پوشیده ز من، خلق چه دانند مرا؟</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه دانم، بخدا و همه دانم او را</p></div>
<div class="m2"><p>همه دانند که اندر همه دانند مرا</p></div></div>