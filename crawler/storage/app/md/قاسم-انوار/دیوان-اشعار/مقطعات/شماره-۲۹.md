---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>ای برادر، گر ره صورت روی</p></div>
<div class="m2"><p>تا قیامت بوی معنی نشنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان جاویدان اگر خواهی، بخوان</p></div>
<div class="m2"><p>مثنوی معنوی مولوی</p></div></div>