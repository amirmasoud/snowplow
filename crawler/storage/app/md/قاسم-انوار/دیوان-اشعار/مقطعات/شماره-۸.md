---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>سید رهروان دین طیفور</p></div>
<div class="m2"><p>آنکه در عمر خویشتن بدفرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شریعت رسید، راهی یافت</p></div>
<div class="m2"><p>در حقیقت رسید ره گم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راه کم گشت و راهرو هم گم</p></div>
<div class="m2"><p>گم کند راه خویش اینجا مرد</p></div></div>