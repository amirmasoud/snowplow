---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>شاه رستم را درودی میفرستم با سلام</p></div>
<div class="m2"><p>زانکه کس رامثل او فرزند فرزندی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میر رستم، شه محمد، شاه روحانی صفت</p></div>
<div class="m2"><p>رفت ازین ویرانه و تن برد تا دار خلود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دید جنت با هزاران فر و زیب آراسته</p></div>
<div class="m2"><p>میل ازین عالم برید و اندر آن عالم فزود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یا الهی، جان پاکش را بجنت شاد دار</p></div>
<div class="m2"><p>یا غیاث المستغیثین، در خلود و در شهود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی پنهان کرد و پنهان شد ز ما آن نور چشم</p></div>
<div class="m2"><p>سال اندر هشتصد و سی و سه بد کین رو نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با وجود جاه و حرمت علم می جست ازاله</p></div>
<div class="m2"><p>آشنای یار گشت و نور عرفانش ربود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکجا ذکر تو می‌گویند در افواه خلق</p></div>
<div class="m2"><p>قاسم خسته روان می‌راند از دیده دو رود</p></div></div>