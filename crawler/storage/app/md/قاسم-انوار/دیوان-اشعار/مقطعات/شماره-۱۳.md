---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>یا رب، بحق لطفت کو جان عاشقان را</p></div>
<div class="m2"><p>بهر ظهور اسما پیدا کند مظاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جود بی دریغت بخشای بندگان را</p></div>
<div class="m2"><p>یا عصمتی در اول، یا توبه ای در آخر</p></div></div>