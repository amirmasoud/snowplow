---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>گر ببینی عارفی یا طالبی</p></div>
<div class="m2"><p>هر دو از روی حقیقت متفق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این یکی را حمد گو آن را ثنا</p></div>
<div class="m2"><p>زان که این مست حق است آن مستحق</p></div></div>