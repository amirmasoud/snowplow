---
title: >-
    شمارهٔ ۵
---
# شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>گر ترا میل عالم جانست</p></div>
<div class="m2"><p>زاده ترک سین ساسانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از شهامت کمان بی زه را</p></div>
<div class="m2"><p>دو الف کن، که کار آسانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بعد از آنت چو ماند نقطه روح</p></div>
<div class="m2"><p>باز ماندن نه کار مردانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقطه را صفر ساز و ثانی شو</p></div>
<div class="m2"><p>تا بدانی که جمله سبحانست</p></div></div>