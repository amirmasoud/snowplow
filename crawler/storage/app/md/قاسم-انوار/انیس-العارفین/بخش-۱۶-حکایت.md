---
title: >-
    بخش ۱۶ - حکایت
---
# بخش ۱۶ - حکایت

<div class="b" id="bn1"><div class="m1"><p>عارفی خوش گفت با مردی بخیل:</p></div>
<div class="m2"><p>ای بدست ناجوانمردی ذلیل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بخیلی چون زنان بی زهره ای</p></div>
<div class="m2"><p>دایم از وصل خدا بی بهره ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل او در بذل جانست، ای علیل</p></div>
<div class="m2"><p>دور ازین دولت بود مرد بخیل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون ز دستت بر نیاید نان دهی</p></div>
<div class="m2"><p>پای بر سر همچو مردان کی نهی؟</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دل، از هستی بجان جویای او</p></div>
<div class="m2"><p>«لن تنالوا البر حتی تنفقوا»</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روزی از بخلت نمی گردد زیاد</p></div>
<div class="m2"><p>جان مکن در بخل چندینی زیاد</p></div></div>