---
title: >-
    بخش ۲ - فی نظم انیس العارفین
---
# بخش ۲ - فی نظم انیس العارفین

<div class="b" id="bn1"><div class="m1"><p>یا مغیث المذنبین معطی السؤال</p></div>
<div class="m2"><p>یاانیس العارفین،یا ذوالجلال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای ز عشقت هر دلی را مشکلی</p></div>
<div class="m2"><p>وی ز شوقت در جنون هر عاقلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در تمنای تو دل سودازده</p></div>
<div class="m2"><p>شور عشقت آتش اندر ما زده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جهان عقل و جان حیران تو</p></div>
<div class="m2"><p>گوی دلها در خم چوگان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرغ دل در دام عشقت پای بند</p></div>
<div class="m2"><p>هرکه سودای تو دارد سر بلند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شور عشقت شعله در عالم زده</p></div>
<div class="m2"><p>بی تو در هر گوشه صد ماتم زده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل دانا در رهت بی خویشتن</p></div>
<div class="m2"><p>بحر عشقت در دل ما موج زن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پادشاهان پیش در گاهت گدا</p></div>
<div class="m2"><p>از تو بی برگان عالم را نوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چشم شهبازخرد عشقت بدوخت</p></div>
<div class="m2"><p>در هوایت مرغ جان را پر بسوخت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مانده حیران رهت مردان مرد</p></div>
<div class="m2"><p>اشک عنابی روان بر روی زرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جان مشتاقان بدردت شادمان</p></div>
<div class="m2"><p>بندگان خاصت آزاد جهان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>راستی را،با تویک دم داغ و درد</p></div>
<div class="m2"><p>قاسمی را خوشتر از صد باغ ورد</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نزد آن کس،کین سخن را محرمست</p></div>
<div class="m2"><p>نوش نیش آمد،جراحت مرهمست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ای زبانها در ثنایت مانده لال</p></div>
<div class="m2"><p>در هوایت مرغ وهم افگنده بال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ثنایت قاسمی حیران شده</p></div>
<div class="m2"><p>دیده نی پایان و سرگردان شده</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ای غم عشق تو با جان سازگار</p></div>
<div class="m2"><p>از کرمهای تو دل امیدوار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>ای خداوند جهاندار کریم</p></div>
<div class="m2"><p>لایزال لم یزل، حی قدیم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نیست جز لطف توکس فریادرس</p></div>
<div class="m2"><p>یا اله العالمین، فریاد رس</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>پادشاها، بندگان خسته ایم</p></div>
<div class="m2"><p>جمله در بند هوی پا بسته ایم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>در بیابان طلب حیران شده</p></div>
<div class="m2"><p>غرقه دریای بی پایان شده</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>نیست بی فضل تو جان را قوتی</p></div>
<div class="m2"><p>یا غیاث المستغیثین،رحمتی</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>قاسم سرگشته سر گردان تست</p></div>
<div class="m2"><p>گر بدست،ارنیک،باری آن تست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای خداوند کریم کار ساز</p></div>
<div class="m2"><p>از کرمهای تو کارم را بساز</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جرعه ای آخر،که ازعقلی فکور</p></div>
<div class="m2"><p>تشنه ماندم در بیابان غرور</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جذبه ای،تا یک زمان طیران کنم</p></div>
<div class="m2"><p>در هوای لامکان جولان کنم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>خانه دل را بلطف آباد کن</p></div>
<div class="m2"><p>جانم از بند جهان آزاد کن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مرغ روحم را بوصلت راه ده</p></div>
<div class="m2"><p>دیده بینا، دل آگاه ده</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>جانم از خلق جهان بیگانه کن</p></div>
<div class="m2"><p>یاد خودرا بادلم هم خانه کن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نفس کرکس راز بازی بازدار</p></div>
<div class="m2"><p>در هوایت مرغ جان را باز دار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>با خودم نزدیک کن وز خلق دور</p></div>
<div class="m2"><p>ذل و جرمم عفو گردان، یا غفور</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>از محبت جانم اندر شور دار</p></div>
<div class="m2"><p>رازم از خلق جهان مستور دار</p></div></div>