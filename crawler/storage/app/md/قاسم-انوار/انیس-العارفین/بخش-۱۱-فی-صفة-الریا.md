---
title: >-
    بخش ۱۱ - فی صفة الریا
---
# بخش ۱۱ - فی صفة الریا

<div class="b" id="bn1"><div class="m1"><p>هر کرا قصد حریم کبریاست</p></div>
<div class="m2"><p>دشمنش در راه دین کبر و ریاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گربه باشد هرکه دارد این صفت</p></div>
<div class="m2"><p>سگ به ازوی،پیش اهل معرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حب دنیا مظهر وصف ریاست</p></div>
<div class="m2"><p>خودریایی کیست؟شخص خودنماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ضد اخلاصست و شرک اصغر ست</p></div>
<div class="m2"><p>این حدیث حضرت پیغمبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راستی،شخص ریایی مرد نیست</p></div>
<div class="m2"><p>در طریق دین دلش را درد نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صد فغان از دست آن درویش درون</p></div>
<div class="m2"><p>کز درونش این صفت آید برون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از قبول خلق،چند،ای بی خرد؟</p></div>
<div class="m2"><p>کان قبولت نیست الا بیخ رد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لازم از اثبات آید نفی دوست</p></div>
<div class="m2"><p>در طریقی نفی خود اثبات اوست</p></div></div>