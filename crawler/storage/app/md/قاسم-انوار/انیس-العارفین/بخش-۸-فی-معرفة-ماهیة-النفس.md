---
title: >-
    بخش ۸ - فی معرفة، ماهیة النفس
---
# بخش ۸ - فی معرفة، ماهیة النفس

<div class="b" id="bn1"><div class="m1"><p>مرحبا!ای سایل شیرین سؤال</p></div>
<div class="m2"><p>در بیان نفس خود بشنو مقال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صانعی،کو انس و جان راآفرید</p></div>
<div class="m2"><p>عقل ونفس وقلب وجان راپرورید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دادانسان را کمال از چار چیز</p></div>
<div class="m2"><p>قادر بیچون بتقدیر عزیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلغم و صفرا و سودا بعد ازان</p></div>
<div class="m2"><p>خون که باشد در همه اعضا روان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زان سپس آرد ز عین لطف وجود</p></div>
<div class="m2"><p>زین چهار ارکان بخاری در وجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کسی از عین حکمت داندش</p></div>
<div class="m2"><p>بی شکی روح طبیعی خواندش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در وجود آرد بخاری زین بخار</p></div>
<div class="m2"><p>روح حیوانیش گوید هوشیار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بعد ازان از روح حیوانی دگر</p></div>
<div class="m2"><p>زو بخاری صاف تر آید بدر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بس لطیف و روشن و زیبا بود</p></div>
<div class="m2"><p>روح قدسی را درو مأوا بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>روح انسانیش گویند،ای پسر</p></div>
<div class="m2"><p>قابل انوار گردد سر بسر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>منزل روح القدس گردد تمام</p></div>
<div class="m2"><p>عارفان زان پس کنندش نفس نام</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>روح قدسی قوتش بخشد،بدان</p></div>
<div class="m2"><p>کار فرمای حواس آید،بدان</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چونکه تقوی ورزد و زهد و صلاح</p></div>
<div class="m2"><p>مطمئنه باشد اندر اصطلاح</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر ز تقوی در فجوری عاق شد</p></div>
<div class="m2"><p>اسم اماره برو اطلاق شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>ور میان هر دو ساکن شد دمی</p></div>
<div class="m2"><p>عارفان لوامه خوانندش همی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اصل تقوی و فجور ازچیست باز؟</p></div>
<div class="m2"><p>ای برادر،لطف و قهر بی نیاز</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>خلق را سر رشته آنجا شد ز دست</p></div>
<div class="m2"><p>هر که آمد در شریعت، رست، رست</p></div></div>