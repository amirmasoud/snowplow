---
title: >-
    بخش ۲۱ - حکایت در معرفت قلب
---
# بخش ۲۱ - حکایت در معرفت قلب

<div class="b" id="bn1"><div class="m1"><p>شیخ عالم، آفتاب اولیا</p></div>
<div class="m2"><p>پیشوای دین، صفی الاصفیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنکه ازوی گشت مشهور اردویل</p></div>
<div class="m2"><p>وز جمالش شد پر از نور اردویل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلنواز طالبان جان گداز</p></div>
<div class="m2"><p>واقف اسرار و شه بیت نیاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زابتدای حال میک ردی سفر</p></div>
<div class="m2"><p>در طلب پرسان پیر راهبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون بشهر شهره شیراز شد</p></div>
<div class="m2"><p>شیخ سعدی شیخ را دمساز شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شیخ را پرسید مرد خرده دان:</p></div>
<div class="m2"><p>کای منور از جمالت جسم و جان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بیابان طلب مقصود چیست؟</p></div>
<div class="m2"><p>وین همه درد دل ممدود چیست؟</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از کمال همت خود شاهباز</p></div>
<div class="m2"><p>قصه ای با شیخ سعدی گفت باز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>چون شنید آن قصه سرگردان بماند</p></div>
<div class="m2"><p>وز کمال همتش حیران بماند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شیخ راگفت:ای ز معنی بهره مند</p></div>
<div class="m2"><p>وز کمال همت خود سر بلند</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن مقامی را که فرمودی نشان</p></div>
<div class="m2"><p>مرغ سعدی را نبودست آشیان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دلم شد زین سخن دردی مقیم</p></div>
<div class="m2"><p>عاجزم ازسر این معنی عظیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لیک ن ار گویی،من از دیوان خویش</p></div>
<div class="m2"><p>نکته ای چندت دهم از کان خویش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در جوابش گفت شیخ ازعین درد:</p></div>
<div class="m2"><p>جان مااز غیر جانانست فرد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دردل از دیوان حق دارم بسی</p></div>
<div class="m2"><p>نیستم پروای دیوان کسی</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ما بدرد او تولا کرده ایم</p></div>
<div class="m2"><p>وز جهان و جان تبرا کرده ایم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جان بدرد دلبری دیوانه شد</p></div>
<div class="m2"><p>وزخیال غیر او بیگانه شد</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>شیخ سعدی زین سخن بگریست زار</p></div>
<div class="m2"><p>شیخ را گفت :ای بزرگ نامدار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گوی دولت را بچوگان طلب</p></div>
<div class="m2"><p>برده ای در حال میدان طرب</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>داری از حق ملکت بی منتها</p></div>
<div class="m2"><p>یرلغش «الله یهدی من یشا»</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شیرمردان از هوای آب و خاک</p></div>
<div class="m2"><p>خانه دل را چنین کردند پاک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>کرده اند از صدق دل مردان کار</p></div>
<div class="m2"><p>درد او بر هر دو عالم اختیار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>دل،که دایم روز و شب در کار اوست</p></div>
<div class="m2"><p>لاجرم مستغرق دیدار اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>دردلت گر درد جانانست وبس</p></div>
<div class="m2"><p>خود نگه دارش،که جان آنست وبس</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ذره ای اندوه محبوب،ای پسر</p></div>
<div class="m2"><p>خوشتر از ملک دو عالم سر بسر</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>هر کرا یک ذره در دل درد اوست</p></div>
<div class="m2"><p>تا قیامت سر فراز از ورد اوست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>گر ترا بانفس و شیطان کار نیست</p></div>
<div class="m2"><p>در دیار دل جزو دیار نیست</p></div></div>