---
title: >-
    بخش ۳۱ - حکایت مرد مریض
---
# بخش ۳۱ - حکایت مرد مریض

<div class="b" id="bn1"><div class="m1"><p>ابلهی را علت درد شکم</p></div>
<div class="m2"><p>کرد عاجز هفته ای، یا بیش و کم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت نزدیک طبیب خرده دان</p></div>
<div class="m2"><p>علت خود عرضه کرد اندر زمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون سؤالش از غذا کرد آن عزیز</p></div>
<div class="m2"><p>گفت: جغرات و چغندر با مویز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این سخن بشنید ازو داننده مرد</p></div>
<div class="m2"><p>بر سر و ریشش زمانی خنده کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفت: چشمت را سبل کرده است کور</p></div>
<div class="m2"><p>ای ز تو دانش به صد فرسنگ دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چنین غافل نمی شاید غنود</p></div>
<div class="m2"><p>بایدت رفتن بر کحال زود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا سبل گرداند از چشم تو کم</p></div>
<div class="m2"><p>وارهی از علت درد شکم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت:می گویی جواب بی محل</p></div>
<div class="m2"><p>درد اشکم را چه نسبت با سبل؟</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>من چو از درد شکم پرسم سؤال</p></div>
<div class="m2"><p>از سبل گویی جوابم،چیست حال؟</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفت:اگر کورت نمی بودی بصر</p></div>
<div class="m2"><p>زانچه می دارد زیان کردی حذر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قصه کم تر گو، بر کحال رو</p></div>
<div class="m2"><p>هیچ تاخیری مکن، درحال رو</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چشم تو کورست و تو آواره ای</p></div>
<div class="m2"><p>سخت محرمی و بس بیچاره ای</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>می کنی اثبات خویش و نفی یار</p></div>
<div class="m2"><p>نفی خود کن،تا شود یار آشکار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تو چنین گویی که:بر شیطان دون</p></div>
<div class="m2"><p>غالبم در حیله و مکر و فسون</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نیستی غالب ولی پندار تو</p></div>
<div class="m2"><p>می دهد بر باد کار و بار تو</p></div></div>