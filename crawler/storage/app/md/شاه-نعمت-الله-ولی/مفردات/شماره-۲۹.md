---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>آمده بود یار بازاری</p></div>
<div class="m2"><p>رفت از این جا سزد که باز آری</p></div></div>