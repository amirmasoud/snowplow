---
title: >-
    شمارهٔ ۲۱۶
---
# شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>ما و ساقی نشسته مست خراب</p></div>
<div class="m2"><p>خیز اگر عاشقی بیا دریاب</p></div></div>