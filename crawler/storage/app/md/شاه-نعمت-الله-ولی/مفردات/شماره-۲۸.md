---
title: >-
    شمارهٔ ۲۸
---
# شمارهٔ ۲۸

<div class="b" id="bn1"><div class="m1"><p>آفتابی ز غیب پیدا شد</p></div>
<div class="m2"><p>نور او در همه هویدا شد</p></div></div>