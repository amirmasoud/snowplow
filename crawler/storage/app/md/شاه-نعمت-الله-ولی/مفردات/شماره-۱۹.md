---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>این ریاضت چو بوتهٔ عشق گداز</p></div>
<div class="m2"><p>زر قلب نیاز خوش بگداز</p></div></div>