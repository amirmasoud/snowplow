---
title: >-
    شمارهٔ ۷۳
---
# شمارهٔ ۷۳

<div class="b" id="bn1"><div class="m1"><p>تا توانی دلی به دست آور</p></div>
<div class="m2"><p>این چنین حاصلی به دست آور</p></div></div>