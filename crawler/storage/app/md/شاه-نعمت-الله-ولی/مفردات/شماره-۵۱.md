---
title: >-
    شمارهٔ ۵۱
---
# شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>به سایه روی منه رو به آفتاب آور</p></div>
<div class="m2"><p>به آفتاب نشین و ز نور او بر خور</p></div></div>