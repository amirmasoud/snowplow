---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>گر تو فانی شوی ز جود وجود</p></div>
<div class="m2"><p>آن یکی هست و بود و خواهد بود</p></div></div>