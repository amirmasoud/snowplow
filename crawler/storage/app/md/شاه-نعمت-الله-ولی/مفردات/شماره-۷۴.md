---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>تافته خوش آفتابی بر همه</p></div>
<div class="m2"><p>گر ببیند ور نبیند بر همه </p></div></div>