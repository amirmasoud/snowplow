---
title: >-
    شمارهٔ ۶۶
---
# شمارهٔ ۶۶

<div class="b" id="bn1"><div class="m1"><p>پادشاهی گر همی خواهی از او</p></div>
<div class="m2"><p>بندگی کن بندگی کن بندگی</p></div></div>