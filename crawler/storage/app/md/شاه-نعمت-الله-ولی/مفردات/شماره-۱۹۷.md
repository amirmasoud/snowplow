---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>گر تو را عزم هست تا دربند</p></div>
<div class="m2"><p>رو به شروان نه و میان دربند</p></div></div>