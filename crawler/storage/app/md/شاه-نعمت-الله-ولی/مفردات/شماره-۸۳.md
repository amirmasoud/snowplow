---
title: >-
    شمارهٔ ۸۳
---
# شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>جسم و جان است همچو آب و حباب</p></div>
<div class="m2"><p>نظری کن به عین ما دریاب</p></div></div>