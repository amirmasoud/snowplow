---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>نشان زمرهٔ جنت چهار است</p></div>
<div class="m2"><p>به قول بهترین هر دو عالم</p></div></div>