---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>در حقیقت بنده و سیّد یکی است</p></div>
<div class="m2"><p>گر تو را شک هست ما را بی شکی است</p></div></div>