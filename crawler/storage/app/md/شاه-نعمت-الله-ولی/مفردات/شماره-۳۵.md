---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>باز گردد به برزخ جامع</p></div>
<div class="m2"><p>نیک دریاب این سخن سامع</p></div></div>