---
title: >-
    شمارهٔ ۱۲۸
---
# شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>دل حاضر دار با خدایت</p></div>
<div class="m2"><p>تا فیض بیابی از عنایت</p></div></div>