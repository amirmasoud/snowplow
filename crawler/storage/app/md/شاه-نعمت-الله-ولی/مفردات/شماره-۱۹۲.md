---
title: >-
    شمارهٔ ۱۹۲
---
# شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>که غیر از انبیا و اولیا کس</p></div>
<div class="m2"><p>نداند سر این علم از مه و که</p></div></div>