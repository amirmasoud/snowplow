---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>بر یمین و یسار و ارض و سما</p></div>
<div class="m2"><p>جز خدا نیست یک زمان به خدا</p></div></div>