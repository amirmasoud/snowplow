---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>بی مظاهر ظهور مظهر نیست</p></div>
<div class="m2"><p>گر چه در عقل هست ظاهر نیست</p></div></div>