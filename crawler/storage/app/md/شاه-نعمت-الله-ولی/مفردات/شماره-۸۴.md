---
title: >-
    شمارهٔ ۸۴
---
# شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>جسم و جان خوشی همی یابد</p></div>
<div class="m2"><p>تا چنین آدمی بیاراید</p></div></div>