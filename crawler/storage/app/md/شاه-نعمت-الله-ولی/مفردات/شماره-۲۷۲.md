---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>هر کس که نهد تاج سر ما بر سر</p></div>
<div class="m2"><p>فارغ شود از درد سر هر دو سرا</p></div></div>