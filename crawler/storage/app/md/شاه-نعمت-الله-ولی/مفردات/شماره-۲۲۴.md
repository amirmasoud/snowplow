---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>مخلصانه به صدق بی ‌اکراه</p></div>
<div class="m2"><p>خوش بگو لا اله الا اللّه</p></div></div>