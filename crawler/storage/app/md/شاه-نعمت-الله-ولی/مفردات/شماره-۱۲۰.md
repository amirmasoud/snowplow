---
title: >-
    شمارهٔ ۱۲۰
---
# شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>در فنا رفت و در بقا آسود</p></div>
<div class="m2"><p>گر چه گفتند بود هیچ نبود</p></div></div>