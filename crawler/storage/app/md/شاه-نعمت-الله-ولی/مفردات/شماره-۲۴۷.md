---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>نقطه ای که الف نقش بست </p></div>
<div class="m2"><p>بر در محجوبهٔ احمد نشست</p></div></div>