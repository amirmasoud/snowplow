---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>خواجه‌ای دیدم که می آید ز کیچ</p></div>
<div class="m2"><p>گرچه کیچی بود با ما بود کیچ</p></div></div>