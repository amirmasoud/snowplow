---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>در فضای وجود و اوج شهود</p></div>
<div class="m2"><p>شاهبازم همی کند پرواز</p></div></div>