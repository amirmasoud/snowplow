---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>نور خود در نار موسی را نمود</p></div>
<div class="m2"><p>در همه اشیا چنین ما را نمود</p></div></div>