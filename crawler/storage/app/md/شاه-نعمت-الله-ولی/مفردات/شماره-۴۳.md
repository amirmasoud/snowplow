---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>بگسسته کسی ز هر دو عالم بی ‌شک</p></div>
<div class="m2"><p>چون ابروی یار خویش پیوسته خوش است</p></div></div>