---
title: >-
    شمارهٔ ۹۶
---
# شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>حسن خلق و خلق می ‌دانم ز حق</p></div>
<div class="m2"><p>عارفان دانند سر خلق خلق</p></div></div>