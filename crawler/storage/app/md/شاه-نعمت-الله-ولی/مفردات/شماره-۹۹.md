---
title: >-
    شمارهٔ ۹۹
---
# شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>خری بر اسب و عیسی شد پیاده</p></div>
<div class="m2"><p>خر و خر کره شیخ و شیخ زاده</p></div></div>