---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>همه عالم تن است و او جان است</p></div>
<div class="m2"><p>جام گیتی نمای سلطان است</p></div></div>