---
title: >-
    شمارهٔ ۲۱۰
---
# شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>لاجرم تا ز عشق آگاهم</p></div>
<div class="m2"><p>عین معشوق نعمت‌اللّهم</p></div></div>