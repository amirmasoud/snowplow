---
title: >-
    شمارهٔ ۱۵۶
---
# شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>سیّد ما بندهٔ جانی اوست</p></div>
<div class="m2"><p>پیش او سلطان غلام است ای پسر</p></div></div>