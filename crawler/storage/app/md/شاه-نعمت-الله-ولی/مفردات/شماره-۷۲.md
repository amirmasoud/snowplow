---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>تا تو خود را تمام نشناسی</p></div>
<div class="m2"><p>خواجه را از غلام نشناسی</p></div></div>