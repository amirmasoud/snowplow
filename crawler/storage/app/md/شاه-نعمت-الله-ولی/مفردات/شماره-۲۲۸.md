---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>مشکل ما جمله حلوا کرده‌اند</p></div>
<div class="m2"><p>صحن ما را پر ز حلوا کرده‌اند</p></div></div>