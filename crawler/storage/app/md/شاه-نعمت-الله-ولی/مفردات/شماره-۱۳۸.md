---
title: >-
    شمارهٔ ۱۳۸
---
# شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>رب و مربوب خویش می ‌جوید</p></div>
<div class="m2"><p>نعمت‌اللّه سخن چنین گوید</p></div></div>