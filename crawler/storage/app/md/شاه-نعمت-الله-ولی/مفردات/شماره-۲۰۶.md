---
title: >-
    شمارهٔ ۲۰۶
---
# شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>گرد بر گرد عاشقان می ‌گرد</p></div>
<div class="m2"><p>گر ما دایماً روان می گرد</p></div></div>