---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>آفتاب خوشی است تابنده</p></div>
<div class="m2"><p>نفس او مرده و دلش زنده</p></div></div>