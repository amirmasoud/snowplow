---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>نشان جملهٔ مردم همین است</p></div>
<div class="m2"><p>که بگزینند جنت بر جهنم</p></div></div>