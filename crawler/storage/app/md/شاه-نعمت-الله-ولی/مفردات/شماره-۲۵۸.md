---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>هر چه باشند ما همان باشیم</p></div>
<div class="m2"><p>هر چه پاشند ما همان پاشیم</p></div></div>