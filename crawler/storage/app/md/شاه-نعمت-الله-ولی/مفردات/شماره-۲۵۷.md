---
title: >-
    شمارهٔ ۲۵۷
---
# شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>هر چند رئیس ما گزیر است</p></div>
<div class="m2"><p>اما چکنم که ناگزیر است</p></div></div>