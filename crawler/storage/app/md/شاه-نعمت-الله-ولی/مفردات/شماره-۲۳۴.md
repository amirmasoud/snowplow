---
title: >-
    شمارهٔ ۲۳۴
---
# شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>موج و بحر و حباب ای دانا</p></div>
<div class="m2"><p>گر طلب می‌کنی بجو از ما</p></div></div>