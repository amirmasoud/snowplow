---
title: >-
    شمارهٔ ۲۳۶
---
# شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>می دار به دست خود ترازو</p></div>
<div class="m2"><p>تا ره نزند کسی تو را زو</p></div></div>