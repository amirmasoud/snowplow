---
title: >-
    شمارهٔ ۱۰۴
---
# شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>خوش لب ماست یک زمان بنشین</p></div>
<div class="m2"><p>وز لب من گل دو سر بر چین</p></div></div>