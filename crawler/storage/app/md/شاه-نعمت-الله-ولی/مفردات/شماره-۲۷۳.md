---
title: >-
    شمارهٔ ۲۷۳
---
# شمارهٔ ۲۷۳

<div class="b" id="bn1"><div class="m1"><p>هر یار که ثابت نبود در یاری</p></div>
<div class="m2"><p>شاید که ورا به یاریش نشماری</p></div></div>