---
title: >-
    شمارهٔ ۲۷۴
---
# شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>هرچه داری به عشق او درباز</p></div>
<div class="m2"><p>تا کند او به روی تو در باز</p></div></div>