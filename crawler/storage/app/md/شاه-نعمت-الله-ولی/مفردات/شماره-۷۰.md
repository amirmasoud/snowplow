---
title: >-
    شمارهٔ ۷۰
---
# شمارهٔ ۷۰

<div class="b" id="bn1"><div class="m1"><p>پیر رندانم بیا ای نوجوان</p></div>
<div class="m2"><p>یاد گیر از من که آن ورزیده‌ام</p></div></div>