---
title: >-
    شمارهٔ ۲۰۵
---
# شمارهٔ ۲۰۵

<div class="b" id="bn1"><div class="m1"><p>گرد اندوه من نمی ‌گردم</p></div>
<div class="m2"><p>بر من اندوه گرد می ‌گردد</p></div></div>