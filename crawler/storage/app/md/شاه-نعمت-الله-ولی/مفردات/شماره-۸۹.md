---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>چون دلم کار خاک کم کردی</p></div>
<div class="m2"><p>ننشسته به دامنم گردی</p></div></div>