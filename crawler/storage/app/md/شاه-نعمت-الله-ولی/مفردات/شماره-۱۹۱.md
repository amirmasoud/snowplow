---
title: >-
    شمارهٔ ۱۹۱
---
# شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>کردم از وی سؤال و گفت جواب</p></div>
<div class="m2"><p>خوش جوابی لطیف بود چو آب</p></div></div>