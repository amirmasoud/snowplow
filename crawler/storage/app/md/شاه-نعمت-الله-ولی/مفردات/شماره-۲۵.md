---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>این خرقهٔ چار وصله بگذار</p></div>
<div class="m2"><p>وان خلعت پادشاه بردار</p></div></div>