---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>در چنین خانه گر بیاری یار</p></div>
<div class="m2"><p>طلب و طالبی و هم مطلوب</p></div></div>