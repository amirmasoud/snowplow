---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>پاک شو تا قبول او گردی</p></div>
<div class="m2"><p>چون شدی پاک خوش نکو گردی</p></div></div>