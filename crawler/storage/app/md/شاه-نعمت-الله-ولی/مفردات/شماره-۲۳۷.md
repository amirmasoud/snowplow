---
title: >-
    شمارهٔ ۲۳۷
---
# شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>نام نیک است یادگار بشر</p></div>
<div class="m2"><p>نام نیکت به خیر به که به شر</p></div></div>