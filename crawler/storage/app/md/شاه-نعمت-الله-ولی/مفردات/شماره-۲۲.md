---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>این همه رنگهای پر نیرنگ</p></div>
<div class="m2"><p>خم وحدت همه کند یکرنگ</p></div></div>