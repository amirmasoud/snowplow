---
title: >-
    شمارهٔ ۷۹
---
# شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>جام بی می بگو چه باشد هیچ</p></div>
<div class="m2"><p>رند بی وی بگو چه باشد هیچ</p></div></div>