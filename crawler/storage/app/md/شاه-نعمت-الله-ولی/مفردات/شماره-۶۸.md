---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>پاک باش و پاک باز و پاک نوش</p></div>
<div class="m2"><p>ساغر پاکی بگیر و پاک توش</p></div></div>