---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>سعادت همچو ماهی خوش بر آمد</p></div>
<div class="m2"><p>درخت دولت ما در بر آمد</p></div></div>