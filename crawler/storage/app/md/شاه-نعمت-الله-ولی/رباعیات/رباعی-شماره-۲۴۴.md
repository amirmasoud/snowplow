---
title: >-
    رباعی شمارهٔ ۲۴۴
---
# رباعی شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>زان باده نخورده ام که هشیار شوم</p></div>
<div class="m2"><p>آن مست نیم که باز بیدار شوم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جام تجلّی بلای تو بَسَم</p></div>
<div class="m2"><p>تا از عدم و وجود بیدار شوم</p></div></div>