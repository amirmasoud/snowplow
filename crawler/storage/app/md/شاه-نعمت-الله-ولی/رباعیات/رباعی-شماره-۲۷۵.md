---
title: >-
    رباعی شمارهٔ ۲۷۵
---
# رباعی شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>گر شه خواهی محرم آن شاه بجو</p></div>
<div class="m2"><p>در راه در آ و یار همراه بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بنده و سیّدی به هم می‌ طلبی</p></div>
<div class="m2"><p>برخیز و بیا و نعمت‌اللّه بجو</p></div></div>