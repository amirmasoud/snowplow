---
title: >-
    رباعی شمارهٔ ۴
---
# رباعی شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دریاب تو این قول حکیمانهٔ ما</p></div>
<div class="m2"><p>آنگه بخرام سوی میخانهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین پس من و رندی و خرابات مغان</p></div>
<div class="m2"><p>رنداند شنو گفتهٔ مستانهٔ ما</p></div></div>