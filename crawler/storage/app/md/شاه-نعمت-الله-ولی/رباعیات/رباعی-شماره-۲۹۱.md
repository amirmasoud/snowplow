---
title: >-
    رباعی شمارهٔ ۲۹۱
---
# رباعی شمارهٔ ۲۹۱

<div class="b" id="bn1"><div class="m1"><p>مستم ز خرابات ولی از می نه</p></div>
<div class="m2"><p>نقلم همه نقل است و حریفم شی نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای عالمیان نشان ولیکن پی نه</p></div>
<div class="m2"><p>عالم همه در من است و من در وی نه</p></div></div>