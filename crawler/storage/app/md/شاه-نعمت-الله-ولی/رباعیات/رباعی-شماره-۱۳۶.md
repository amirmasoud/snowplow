---
title: >-
    رباعی شمارهٔ ۱۳۶
---
# رباعی شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>آن یار فقیر این و آنش نبود</p></div>
<div class="m2"><p>سرمایهٔ سود و هم زیانش نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در کتم عدم مست و خراب افتاده</p></div>
<div class="m2"><p>او را خبر از نام و نشانش نبود</p></div></div>