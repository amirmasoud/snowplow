---
title: >-
    رباعی شمارهٔ ۲۰
---
# رباعی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>در دیدهٔ ما نقش خیالش پیداست</p></div>
<div class="m2"><p>نوریست که روشنائی دیدهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هر چه نظر کند خدا را بیند</p></div>
<div class="m2"><p>روشن تر از این دیده دگر دیده کراست</p></div></div>