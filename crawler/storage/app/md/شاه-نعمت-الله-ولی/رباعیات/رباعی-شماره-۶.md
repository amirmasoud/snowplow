---
title: >-
    رباعی شمارهٔ ۶
---
# رباعی شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>بنواخت مرا لطف الهی به خدا</p></div>
<div class="m2"><p>هر درد که بود از کرم کرد دوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشریف خلافت او به سیّد بخشید</p></div>
<div class="m2"><p>او را بشناس و یک زمانی به خود آ</p></div></div>