---
title: >-
    رباعی شمارهٔ ۹۰
---
# رباعی شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>این نقش خیال عالمش می ‌خوانند</p></div>
<div class="m2"><p>جانی دارد که آدمش می‌ خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روحی است که روح اولش می‌ گویند</p></div>
<div class="m2"><p>چون اوست تمام خاتمش می‌ خوانند</p></div></div>