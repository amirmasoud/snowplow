---
title: >-
    رباعی شمارهٔ ۸
---
# رباعی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>دادند جهانی دل و هم دست به ما</p></div>
<div class="m2"><p>برخاست ز غیر هر که بنشست به ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما بحر محیطیم و محبان چو حباب</p></div>
<div class="m2"><p>پیوسته بود کسی که پیوسته به ما</p></div></div>