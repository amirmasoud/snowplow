---
title: >-
    رباعی شمارهٔ ۶۴
---
# رباعی شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>بی درد طریق حیدری نتوان یافت</p></div>
<div class="m2"><p>بی کفر ره قلندری نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی رنج فنا گنج بقا نتوان دید</p></div>
<div class="m2"><p>در حضرت ما به سر ، سری نتوان یافت</p></div></div>