---
title: >-
    رباعی شمارهٔ ۲۱
---
# رباعی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>دارنده چو ترکیب چنین خوب آراست</p></div>
<div class="m2"><p>باز از چه سبب فکندش اندر کم و کاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر خوب نیامد این صور عیب کراست</p></div>
<div class="m2"><p>ور خوب آمد شکستش بهر چراست</p></div></div>