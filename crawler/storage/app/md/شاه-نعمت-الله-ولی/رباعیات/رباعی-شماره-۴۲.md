---
title: >-
    رباعی شمارهٔ ۴۲
---
# رباعی شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>مخموری و میکده نجوئی حیف است</p></div>
<div class="m2"><p>با ما سخن از ذوق نگوئی حیف است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میخانهٔ عاشقان سبیل است به ما</p></div>
<div class="m2"><p>تو در طلب جام و سبوئی حیفست</p></div></div>