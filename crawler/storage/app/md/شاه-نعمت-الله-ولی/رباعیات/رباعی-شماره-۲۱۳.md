---
title: >-
    رباعی شمارهٔ ۲۱۳
---
# رباعی شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>انگشت زنان بر در جانان رفتیم</p></div>
<div class="m2"><p>پیدا بودیم باز پنهان رفتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که برفت نعمت الله ز جهان</p></div>
<div class="m2"><p>رفتیم ولی به نور ایمان رفتیم</p></div></div>