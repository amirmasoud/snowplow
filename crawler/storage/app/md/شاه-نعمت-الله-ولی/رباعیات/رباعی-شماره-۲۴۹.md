---
title: >-
    رباعی شمارهٔ ۲۴۹
---
# رباعی شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>تا آتش عشق او برافروخته‌ ایم</p></div>
<div class="m2"><p>عود دل خود بر آتشش سوخته‌ ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل سوخته‌ ایم و کار آتشبازی</p></div>
<div class="m2"><p>آموخته ‌ایم و نیک آموخته‌ ایم</p></div></div>