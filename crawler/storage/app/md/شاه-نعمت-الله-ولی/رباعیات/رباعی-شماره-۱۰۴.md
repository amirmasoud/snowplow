---
title: >-
    رباعی شمارهٔ ۱۰۴
---
# رباعی شمارهٔ ۱۰۴

<div class="b" id="bn1"><div class="m1"><p>آبست که در شیشه شرابش خوانند</p></div>
<div class="m2"><p>با گل چو قرین شود گلابش خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قید و گل و مُل چو مجرد گردد</p></div>
<div class="m2"><p>اهل بصر و بصیرت آبش خوانند</p></div></div>