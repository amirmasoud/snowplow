---
title: >-
    رباعی شمارهٔ ۱۶۴
---
# رباعی شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>ما توبه به جام می شکستیم دگر</p></div>
<div class="m2"><p>با ساقی خویش عهد بستیم دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندانه حریف نعمت الله خودیم</p></div>
<div class="m2"><p>در کوی خرابات نشستیم دگر</p></div></div>