---
title: >-
    رباعی شمارهٔ ۱۲۸
---
# رباعی شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>بلبل سخن از زبان گل می گوید</p></div>
<div class="m2"><p>مستست و حدیث گل و مل می گوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دریاب رموز نعمت الله ولی</p></div>
<div class="m2"><p>جزویست ولی سخن ز کل می گوید</p></div></div>