---
title: >-
    رباعی شمارهٔ ۲۹۵
---
# رباعی شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>یاری دارم یگانهٔ سرمستی</p></div>
<div class="m2"><p>هستی که جز او نیست به عالم هستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند بگیر دست او را در دست</p></div>
<div class="m2"><p>دستش گیرم اگر بیابم دستی</p></div></div>