---
title: >-
    رباعی شمارهٔ ۱۷۹
---
# رباعی شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>بردار نقاب و می نگر آن رویش</p></div>
<div class="m2"><p>دانی که نقاب چیست یعنی مویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موئی ز سر زلف نگارم به کف آر</p></div>
<div class="m2"><p>آنگه بنشین و خوش خوشی می بویش</p></div></div>