---
title: >-
    رباعی شمارهٔ ۱
---
# رباعی شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای آنکه طلب کار خدایی به خود آ</p></div>
<div class="m2"><p>از خود بطلب کز تو خدا نیست جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول به خود آ چون به خود آیی به خدا</p></div>
<div class="m2"><p>اقرار بیاری به خدایی خدا</p></div></div>