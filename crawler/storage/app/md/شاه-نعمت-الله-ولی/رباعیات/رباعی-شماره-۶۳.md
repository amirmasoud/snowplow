---
title: >-
    رباعی شمارهٔ ۶۳
---
# رباعی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و عقل رخت بربست و برفت</p></div>
<div class="m2"><p>آن عهد که بسته بود بشکست و برفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دید که پادشه درآمد سرمست</p></div>
<div class="m2"><p>بیچاره غلام زود برجست و برفت</p></div></div>