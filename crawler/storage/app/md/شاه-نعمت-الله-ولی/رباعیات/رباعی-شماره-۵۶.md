---
title: >-
    رباعی شمارهٔ ۵۶
---
# رباعی شمارهٔ ۵۶

<div class="b" id="bn1"><div class="m1"><p>دریاب و بیا که نازکانه سخنی است</p></div>
<div class="m2"><p>دانستن این سخن برای چو منی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صورت و معنیش نظر کن به تمام</p></div>
<div class="m2"><p>تا دریابی که یوسف و پیرهنی است</p></div></div>