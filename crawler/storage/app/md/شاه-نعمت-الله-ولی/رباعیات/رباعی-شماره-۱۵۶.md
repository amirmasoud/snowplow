---
title: >-
    رباعی شمارهٔ ۱۵۶
---
# رباعی شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>تا داروی دردم سبب درمان شد</p></div>
<div class="m2"><p>پستیم بلندی شد و کفر ایمان شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل و تن هر سه حجابم بودند</p></div>
<div class="m2"><p>تن دل شد و دل جان شد و جان جانان شد</p></div></div>