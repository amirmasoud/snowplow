---
title: >-
    رباعی شمارهٔ ۵۱
---
# رباعی شمارهٔ ۵۱

<div class="b" id="bn1"><div class="m1"><p>نقشی به خیال بسته کاین علم منست</p></div>
<div class="m2"><p>وان لذت او در این زبان و دهن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل ار چه بسی رفت در این راه ولی</p></div>
<div class="m2"><p>یوسف نشناخت عارف پیر من است</p></div></div>