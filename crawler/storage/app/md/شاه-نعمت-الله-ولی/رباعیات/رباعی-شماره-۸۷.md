---
title: >-
    رباعی شمارهٔ ۸۷
---
# رباعی شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>هر چند که ظالمان همه جمع شوند</p></div>
<div class="m2"><p>امید که یک ز یکدگر بر نخورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سال اسد و ماه اسد شیر خدا</p></div>
<div class="m2"><p>از بیشه برون آید و گرگان بدرند</p></div></div>