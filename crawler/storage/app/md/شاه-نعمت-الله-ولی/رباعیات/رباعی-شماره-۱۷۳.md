---
title: >-
    رباعی شمارهٔ ۱۷۳
---
# رباعی شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>حکمی از او محال باشد پرهیز</p></div>
<div class="m2"><p>فرموده و امر کرده از وی مگریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کو به میان امر حکمش عاجز</p></div>
<div class="m2"><p>درماند و دلفکار ، کجدار و مریز</p></div></div>