---
title: >-
    رباعی شمارهٔ ۱۶۱
---
# رباعی شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>سازنده اگرچه ساز نیکو سازد</p></div>
<div class="m2"><p>اما بی ساز ساز چون بنوازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من آینه ام که می نمایم او را</p></div>
<div class="m2"><p>او خالق من که او مرا می سازد</p></div></div>