---
title: >-
    رباعی شمارهٔ ۲۱۷
---
# رباعی شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>جوهر آبست و گوهرش دُر یتیم</p></div>
<div class="m2"><p>دریاب بیان ما که سریست عظیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج است و حباب نزد ما هر دو یکی</p></div>
<div class="m2"><p>بگذر ز دوئی یکی مسازش به دو نیم</p></div></div>