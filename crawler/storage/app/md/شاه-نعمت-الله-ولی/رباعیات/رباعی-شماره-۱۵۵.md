---
title: >-
    رباعی شمارهٔ ۱۵۵
---
# رباعی شمارهٔ ۱۵۵

<div class="b" id="bn1"><div class="m1"><p>از جود وجود عشق لا شی ، شی شد</p></div>
<div class="m2"><p>وز آب حیات جمله جانها حی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند وفات یافت سید حاشا</p></div>
<div class="m2"><p>باقی به بقای اوست فانی کی شد</p></div></div>