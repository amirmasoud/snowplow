---
title: >-
    رباعی شمارهٔ ۲۹۹
---
# رباعی شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>اللّه یکی صفات او بسیاری</p></div>
<div class="m2"><p>وز هر صفتی به عاشقی بازاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاری که به هر صفت ورا باشد یار</p></div>
<div class="m2"><p>یاری باشد چو سید ما باری</p></div></div>