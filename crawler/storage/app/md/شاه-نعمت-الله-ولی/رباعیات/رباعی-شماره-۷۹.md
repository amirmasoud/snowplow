---
title: >-
    رباعی شمارهٔ ۷۹
---
# رباعی شمارهٔ ۷۹

<div class="b" id="bn1"><div class="m1"><p>رندی که ز هر دو کون یکتا گردد</p></div>
<div class="m2"><p>در کتم عدم واله و شیدا گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر در قدم ساقی سرمست نهد</p></div>
<div class="m2"><p>بی زحمت پا به گرد مأوا گردد</p></div></div>