---
title: >-
    رباعی شمارهٔ ۲۴۳
---
# رباعی شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>این درد همیشه من دوا می ‌بینم</p></div>
<div class="m2"><p>در قهر و جفا لطف و وفا می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صحن زمین به زیر نه سقف فلک</p></div>
<div class="m2"><p>در هر چه نظر کنم خدا می‌ بینم</p></div></div>