---
title: >-
    رباعی شمارهٔ ۲۲۴
---
# رباعی شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>ما جمله حروف عالیاتیم مدام</p></div>
<div class="m2"><p>پنهان ز همه به غیب ذاتیم مدام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چند کتاب عالمی بنوشتیم</p></div>
<div class="m2"><p>پوشیده ز لوح کایناتیم مدام</p></div></div>