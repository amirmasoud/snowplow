---
title: >-
    رباعی شمارهٔ ۱۴۶
---
# رباعی شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>محبوب جمال خود به آدم بخشید</p></div>
<div class="m2"><p>سرّ حرمش به یار محرم بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر نقد که در خزانهٔ عالم بود</p></div>
<div class="m2"><p>سلطان به کرم به جود عالم بخشید</p></div></div>