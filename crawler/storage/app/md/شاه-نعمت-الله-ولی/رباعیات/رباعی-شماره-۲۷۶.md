---
title: >-
    رباعی شمارهٔ ۲۷۶
---
# رباعی شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>اسم و صفت و مظاهر و مظهر کو</p></div>
<div class="m2"><p>خود نام و نشان و باطن و ظاهر کو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوقه و عشق و عاشق آنجا نبود</p></div>
<div class="m2"><p>منظور کجا نظر کجا ناظر کو</p></div></div>