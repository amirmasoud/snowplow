---
title: >-
    رباعی شمارهٔ ۲۶۶
---
# رباعی شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>در سایهٔ او تو آفتابش می ‌بین</p></div>
<div class="m2"><p>تمثال جمال او در آبش می‌ بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز نقش خیال او نبینی در خواب</p></div>
<div class="m2"><p>گر می خواهی برو به خوابش می ‌بین</p></div></div>