---
title: >-
    رباعی شمارهٔ ۲۳۱
---
# رباعی شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>بر خاک درش مست و خراب افتادم</p></div>
<div class="m2"><p>همسایهٔ او در آفتاب افتادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم که منم که نور او می‌نگرم</p></div>
<div class="m2"><p>کشتی بشکست و من در آب افتادم</p></div></div>