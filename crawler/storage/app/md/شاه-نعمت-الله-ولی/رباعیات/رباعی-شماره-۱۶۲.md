---
title: >-
    رباعی شمارهٔ ۱۶۲
---
# رباعی شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>بگذر ز تجمل و تکبر بگذار</p></div>
<div class="m2"><p>رو کهنه بپوش و با قناعت به سر آر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی که بود تجمل ذاتی او</p></div>
<div class="m2"><p>زین نوع تجمل به چه کار آید یار</p></div></div>