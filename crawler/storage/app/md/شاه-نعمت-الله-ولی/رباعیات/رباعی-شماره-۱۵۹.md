---
title: >-
    رباعی شمارهٔ ۱۵۹
---
# رباعی شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>در ملک اگر شاه عراقی باشد</p></div>
<div class="m2"><p>شک نیست که مال شاه باقی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر می خواهی که رندکان جمع شوند</p></div>
<div class="m2"><p>باید که یکی همیشه ساقی باشد</p></div></div>