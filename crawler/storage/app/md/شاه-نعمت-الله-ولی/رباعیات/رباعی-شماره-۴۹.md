---
title: >-
    رباعی شمارهٔ ۴۹
---
# رباعی شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>واصل به خودم عین وصالم اینست</p></div>
<div class="m2"><p>بر حال خودم همیشه حالم این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آینهٔ ذات مثالی دارم</p></div>
<div class="m2"><p>تمثال مثال بی مثالم این است</p></div></div>