---
title: >-
    رباعی شمارهٔ ۲۳۵
---
# رباعی شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>هر آینه ای که آید اندر نظرم</p></div>
<div class="m2"><p>تمثال جمال روی او می ‌نگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جام جهان نما نگاهی کردم</p></div>
<div class="m2"><p>مجموع کمال در یکی می‌ شمرم</p></div></div>