---
title: >-
    رباعی شمارهٔ ۳۵
---
# رباعی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>این هشت حرف نام آن شاه منست</p></div>
<div class="m2"><p>آن شاه که او مظهر الله منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجموع دویست و سی و یک بشمارش</p></div>
<div class="m2"><p>تا دریابی که نام دلخواه منست</p></div></div>