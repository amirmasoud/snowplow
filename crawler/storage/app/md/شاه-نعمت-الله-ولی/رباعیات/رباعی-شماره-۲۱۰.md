---
title: >-
    رباعی شمارهٔ ۲۱۰
---
# رباعی شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>ما سوخته ایم و بارها سوخته ایم</p></div>
<div class="m2"><p>وین خرقهٔ پاره بارها دوخته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شعله که ز آتش ز سر شوق جهد</p></div>
<div class="m2"><p>در ما گیرد از آنکه ما سوخته ایم</p></div></div>