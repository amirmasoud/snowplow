---
title: >-
    رباعی شمارهٔ ۱۸۰
---
# رباعی شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>کو دل که بداند نفسی اسرارش</p></div>
<div class="m2"><p>کو گوش که بشنود ز من گفتارش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق جمال می نماید شب و روز</p></div>
<div class="m2"><p>کو دیده که تا برخورد از دیدارش</p></div></div>