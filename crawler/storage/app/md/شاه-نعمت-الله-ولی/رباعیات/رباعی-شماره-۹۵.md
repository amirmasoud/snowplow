---
title: >-
    رباعی شمارهٔ ۹۵
---
# رباعی شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>وجدان تو با وجود چندان نبود</p></div>
<div class="m2"><p>وین غنچهٔ وجدان تو خندان نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن نقش خیالی که تو بینی در خواب</p></div>
<div class="m2"><p>جز خواب و خیال نقشبندان نبود</p></div></div>