---
title: >-
    رباعی شمارهٔ ۳۱۲
---
# رباعی شمارهٔ ۳۱۲

<div class="b" id="bn1"><div class="m1"><p>ای آنکه طلبکار جهان جانی</p></div>
<div class="m2"><p>جانی و دلی و بلکه خود جانانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطلوب توئی طلب توئی طالب تو</p></div>
<div class="m2"><p>دریاب که بی تو هر چه جویی آنی</p></div></div>