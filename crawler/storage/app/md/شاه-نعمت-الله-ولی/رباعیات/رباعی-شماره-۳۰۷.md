---
title: >-
    رباعی شمارهٔ ۳۰۷
---
# رباعی شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>گر مست شراب اذکروا اللّه شوی</p></div>
<div class="m2"><p>گویا به دم انطقنا اللّه شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز نعمت حق را تو به حق بشناسی</p></div>
<div class="m2"><p>حقا که تو نیز نعمت اللّه شوی</p></div></div>