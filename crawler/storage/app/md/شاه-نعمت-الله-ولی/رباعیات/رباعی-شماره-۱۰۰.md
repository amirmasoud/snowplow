---
title: >-
    رباعی شمارهٔ ۱۰۰
---
# رباعی شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>دلدار مرا کشت حیاتم بخشید</p></div>
<div class="m2"><p>وز زحمت این جهان نجاتم بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرمای خبیصی چو ز دستم بربود</p></div>
<div class="m2"><p>اما به عوض شاخ نباتم بخشید</p></div></div>