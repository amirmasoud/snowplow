---
title: >-
    رباعی شمارهٔ ۳۳
---
# رباعی شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>دیدم رندی که سید رندانست</p></div>
<div class="m2"><p>از هر دو جهان گذشته و رند آن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گنج بقاست گر چه در کنج فناست</p></div>
<div class="m2"><p>پیداست به ما و از جهان پنهانست</p></div></div>