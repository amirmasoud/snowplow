---
title: >-
    رباعی شمارهٔ ۳۲
---
# رباعی شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>توحید تو پیش ما همه شرک و دوئی است</p></div>
<div class="m2"><p>اثبات یگانگی همه عین دوئیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از وحدت و اتحاد بگذر که احد</p></div>
<div class="m2"><p>ایمن ز منی باشد و فارغ ز دوئیست</p></div></div>