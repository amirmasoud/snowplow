---
title: >-
    رباعی شمارهٔ ۲۸۷
---
# رباعی شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>موجود به واجب الوجودیم همه</p></div>
<div class="m2"><p>هستیم ولی هیچ نبودیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از جود وجود عشق موجود شدیم</p></div>
<div class="m2"><p>بی جود وجود بی وجودیم همه</p></div></div>