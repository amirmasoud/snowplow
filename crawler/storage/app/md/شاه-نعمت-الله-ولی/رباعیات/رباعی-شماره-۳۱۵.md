---
title: >-
    رباعی شمارهٔ ۳۱۵
---
# رباعی شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>تا جامع اسرار الهی نشوی</p></div>
<div class="m2"><p>شایستهٔ تخت پادشاهی نشوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا غرقهٔ دریا نشوی همچون ما</p></div>
<div class="m2"><p>دانندهٔ حال ما کماهی نشوی</p></div></div>