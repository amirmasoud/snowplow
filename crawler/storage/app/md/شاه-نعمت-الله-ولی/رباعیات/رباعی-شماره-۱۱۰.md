---
title: >-
    رباعی شمارهٔ ۱۱۰
---
# رباعی شمارهٔ ۱۱۰

<div class="b" id="bn1"><div class="m1"><p>خاک در میخانه مگر بیخته اند</p></div>
<div class="m2"><p>کاین گرد و غبار را بر انگیخته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا ماه رخان خطهٔ ماهانند</p></div>
<div class="m2"><p>کز زلف عبیر در جهان ریخته اند</p></div></div>