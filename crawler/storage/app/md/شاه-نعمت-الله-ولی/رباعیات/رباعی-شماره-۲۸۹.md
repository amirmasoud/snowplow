---
title: >-
    رباعی شمارهٔ ۲۸۹
---
# رباعی شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>ما را می کهنه باید و دیرینه</p></div>
<div class="m2"><p>از روز ازل تا به ابد سیری نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خم از عدم و صراحی از جود وجود</p></div>
<div class="m2"><p>او تلخ نه و شور نه و شیرین نه</p></div></div>