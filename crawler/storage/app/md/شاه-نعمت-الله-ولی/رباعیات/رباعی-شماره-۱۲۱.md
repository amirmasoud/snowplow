---
title: >-
    رباعی شمارهٔ ۱۲۱
---
# رباعی شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>تا با تو توئی بود دوئی خواهد بود</p></div>
<div class="m2"><p>ای یار دوئی هم ز توئی خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون تو ز توئی وز دوئی وارستی</p></div>
<div class="m2"><p>در ملک یکی کجا دوئی خواهد بود</p></div></div>