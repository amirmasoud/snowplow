---
title: >-
    رباعی شمارهٔ ۲۲۱
---
# رباعی شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>ما عادت خود بهانه جوئی نکنیم</p></div>
<div class="m2"><p>جز راست روی و نیک خوئی نکنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنها که به جای ما بدی ها کردند</p></div>
<div class="m2"><p>گر دست دهد به جز نکوئی نکنیم</p></div></div>