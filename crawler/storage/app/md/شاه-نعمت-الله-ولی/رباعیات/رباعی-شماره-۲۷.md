---
title: >-
    رباعی شمارهٔ ۲۷
---
# رباعی شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>مائیم چنین تشنه و دریا با ماست</p></div>
<div class="m2"><p>اندر همه قطره محیطی پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آمد و بنشست به تخت دل ما</p></div>
<div class="m2"><p>چون او بنشست عقل از آنجا برخاست</p></div></div>