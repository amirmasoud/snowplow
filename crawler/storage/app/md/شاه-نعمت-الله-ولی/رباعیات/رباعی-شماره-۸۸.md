---
title: >-
    رباعی شمارهٔ ۸۸
---
# رباعی شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>بی اسم کسی درک مسما نکند</p></div>
<div class="m2"><p>نام ار نبود تمیز اشیا نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل از چه مصفا و مزکی باشد</p></div>
<div class="m2"><p>ادراک اله جز به اسما نکند</p></div></div>