---
title: >-
    رباعی شمارهٔ ۲۳۸
---
# رباعی شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>گفتم چه کنم که می گو چه کنم</p></div>
<div class="m2"><p>گفتم جویم گفت که می جو چه کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتا می رو چنانکه من می کارم</p></div>
<div class="m2"><p>گفتم آری اگر نَرویم چه کنم</p></div></div>