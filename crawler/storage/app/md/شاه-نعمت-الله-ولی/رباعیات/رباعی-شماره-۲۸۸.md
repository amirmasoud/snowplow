---
title: >-
    رباعی شمارهٔ ۲۸۸
---
# رباعی شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>در هر دو جهان غیر یکی باشد نه</p></div>
<div class="m2"><p>در بودن آن یکی شکی باشد نه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر زان که یکی در دگری می نگرد</p></div>
<div class="m2"><p>در مذهب عشق نیککی باشد نه</p></div></div>