---
title: >-
    رباعی شمارهٔ ۹۷
---
# رباعی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>چون یوسف باد در چمن می ‌آید</p></div>
<div class="m2"><p>بوئی ز زلیخا به یمن می‌ آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یعقوب دلم نعره زنان می‌گوید</p></div>
<div class="m2"><p>فریاد که بوی پیرهن می‌ آید</p></div></div>