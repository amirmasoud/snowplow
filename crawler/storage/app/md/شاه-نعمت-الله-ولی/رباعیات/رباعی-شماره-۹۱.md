---
title: >-
    رباعی شمارهٔ ۹۱
---
# رباعی شمارهٔ ۹۱

<div class="b" id="bn1"><div class="m1"><p>گر دیدهٔ دیگری خیالش بیند</p></div>
<div class="m2"><p>در دیدهٔ ما نور جمالش بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آینه ای که چشم ما می ‌نگرد</p></div>
<div class="m2"><p>تمثال جمال بی مثالش بیند</p></div></div>