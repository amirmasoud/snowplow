---
title: >-
    رباعی شمارهٔ ۲۲۰
---
# رباعی شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>ما محرم راز حضرت سلطانیم</p></div>
<div class="m2"><p>احوال درون و هم برون می دانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منشی قضا هرچه نویسد مجمل</p></div>
<div class="m2"><p>بر لوح قدر مفصلش می خوانیم</p></div></div>