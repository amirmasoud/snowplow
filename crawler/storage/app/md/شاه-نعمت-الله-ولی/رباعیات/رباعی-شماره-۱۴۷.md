---
title: >-
    رباعی شمارهٔ ۱۴۷
---
# رباعی شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>بودش به کمال خویش بودم بخشید</p></div>
<div class="m2"><p>لطفش به کرم شهد شهودم بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او طالب من که ظاهرش گردانم</p></div>
<div class="m2"><p>من طالب او که تا وجودم بخشید</p></div></div>