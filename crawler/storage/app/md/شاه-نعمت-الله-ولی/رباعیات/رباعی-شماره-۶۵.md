---
title: >-
    رباعی شمارهٔ ۶۵
---
# رباعی شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>خوش آینه ایست مظهر و ذات و صفات</p></div>
<div class="m2"><p>در وی غیری کجا نماید هیهات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر ساغر می که ساقیم می بخشد</p></div>
<div class="m2"><p>جامیست جهان نما پر از آب حیات</p></div></div>