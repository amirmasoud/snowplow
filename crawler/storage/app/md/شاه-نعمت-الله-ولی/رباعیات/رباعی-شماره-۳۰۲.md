---
title: >-
    رباعی شمارهٔ ۳۰۲
---
# رباعی شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>از فقر به عالم معانی برسی</p></div>
<div class="m2"><p>از ترک به ذوق آن جهان برسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ترک وجود ما سوی اللّه کنی</p></div>
<div class="m2"><p>چون خضر به آب زندگانی برسی</p></div></div>