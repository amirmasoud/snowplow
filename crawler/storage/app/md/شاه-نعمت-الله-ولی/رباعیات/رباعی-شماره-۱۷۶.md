---
title: >-
    رباعی شمارهٔ ۱۷۶
---
# رباعی شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>ما عاشق و رندیم ز طامات مپرس</p></div>
<div class="m2"><p>از ما به جز از حال خرابات مپرس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از زاهد هشیار کرامات طلب</p></div>
<div class="m2"><p>مستیم ز ما کشف و کرامات مپرس</p></div></div>