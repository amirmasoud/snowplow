---
title: >-
    رباعی شمارهٔ ۲۹۳
---
# رباعی شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>گر معرفت نامتناهی یابی</p></div>
<div class="m2"><p>درعین همه نور الهی یابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیرون ز تو نیست خویشتن را بشناس</p></div>
<div class="m2"><p>از خود بطلب هر آن چه خواهی یابی</p></div></div>