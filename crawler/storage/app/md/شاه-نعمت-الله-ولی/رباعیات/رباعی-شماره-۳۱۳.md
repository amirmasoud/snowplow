---
title: >-
    رباعی شمارهٔ ۳۱۳
---
# رباعی شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>وقتی که توکلت فراموش کنی</p></div>
<div class="m2"><p>با دلبر من دست در آغوش کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور زان که سبو کشی و منت داری</p></div>
<div class="m2"><p>جامی ز می توکلم نوش کنی</p></div></div>