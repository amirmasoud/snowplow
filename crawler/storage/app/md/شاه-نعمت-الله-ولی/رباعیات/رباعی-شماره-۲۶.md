---
title: >-
    رباعی شمارهٔ ۲۶
---
# رباعی شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در مذهب ما محب و محبوب یکیست</p></div>
<div class="m2"><p>رغبت چه بود راغب و مرغوب یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویند مرا که عین او را بطلب</p></div>
<div class="m2"><p>چه جای طلب طالب و مطلوب یکیست</p></div></div>