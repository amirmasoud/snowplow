---
title: >-
    رباعی شمارهٔ ۳۰۵
---
# رباعی شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>گر عالم سر لی مع اللّه شوی</p></div>
<div class="m2"><p>دانندهٔ راز بنده و شاه شوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر صورت و معنی جهان دریابی</p></div>
<div class="m2"><p>واقف ز رموز نعمت اللّه شوی</p></div></div>