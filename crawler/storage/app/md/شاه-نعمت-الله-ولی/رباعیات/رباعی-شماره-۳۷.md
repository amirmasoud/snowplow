---
title: >-
    رباعی شمارهٔ ۳۷
---
# رباعی شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>میخانه تمام وقف یاران منست</p></div>
<div class="m2"><p>هر رند که هست جان جانان منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد دل بیقرار درمان من است</p></div>
<div class="m2"><p>وین دُردی درد دائما آن منست</p></div></div>