---
title: >-
    رباعی شمارهٔ ۱۸۳
---
# رباعی شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>گفتم که دلم گفت که ویران کنمش</p></div>
<div class="m2"><p>گفتم عقلم گفت که دیوان کنمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفتم جانم گفت که در حضرت من</p></div>
<div class="m2"><p>جانی چه بود تا سخن از جان کنمش</p></div></div>