---
title: >-
    رباعی شمارهٔ ۱۵۰
---
# رباعی شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>هر آینهٔ که در نظر می گذرد</p></div>
<div class="m2"><p>تمثال جمال او نظر می نگرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تمثال خیالیست و لیکن ذاتش</p></div>
<div class="m2"><p>در آینه تمثال به ما می نگرد</p></div></div>