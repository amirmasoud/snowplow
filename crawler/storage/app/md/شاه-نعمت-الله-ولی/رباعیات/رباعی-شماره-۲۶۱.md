---
title: >-
    رباعی شمارهٔ ۲۶۱
---
# رباعی شمارهٔ ۲۶۱

<div class="b" id="bn1"><div class="m1"><p>مائیم ز خود وجود پرداختگان</p></div>
<div class="m2"><p>و آتش به وجود خود در انداختگان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش رخ چون شمع تو شبهای دراز</p></div>
<div class="m2"><p>پروانه صفت وجود خود باختگان</p></div></div>