---
title: >-
    رباعی شمارهٔ ۶۷
---
# رباعی شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>صحبت با غیر اگر چه از بهر خداست</p></div>
<div class="m2"><p>چون غیر بود در آن میان عین خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگذر تو ز غیر و باش همصحبت او</p></div>
<div class="m2"><p>ور صحبت غیر بایدت عین خطاست</p></div></div>