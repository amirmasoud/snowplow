---
title: >-
    رباعی شمارهٔ ۷
---
# رباعی شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>از آتش عشق صنم دلکش ما</p></div>
<div class="m2"><p>افتاده مدام آتشی در کش ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پروانهٔ پرسوخته ما را داند</p></div>
<div class="m2"><p>تو پخته نه ای چه دانی این آتش ما</p></div></div>