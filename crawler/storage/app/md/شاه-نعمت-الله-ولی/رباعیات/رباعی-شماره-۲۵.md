---
title: >-
    رباعی شمارهٔ ۲۵
---
# رباعی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>میخانهٔ عشق او سرای دل ماست</p></div>
<div class="m2"><p>وان دُردی درد دل دوای دل ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم به تمام و جمله اسمای اله</p></div>
<div class="m2"><p>پیدا شده است و از برای دل ماست</p></div></div>