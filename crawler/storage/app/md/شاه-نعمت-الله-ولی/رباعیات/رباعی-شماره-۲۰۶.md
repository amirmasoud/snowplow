---
title: >-
    رباعی شمارهٔ ۲۰۶
---
# رباعی شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>شهبازم و شاهباز بشناخته ام</p></div>
<div class="m2"><p>در عالم عاشقی سر انداخته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی چو شناختی بگویم با تو</p></div>
<div class="m2"><p>بشناخته ام چنان که بشناخته ام</p></div></div>