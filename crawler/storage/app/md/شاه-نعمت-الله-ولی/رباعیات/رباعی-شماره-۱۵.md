---
title: >-
    رباعی شمارهٔ ۱۵
---
# رباعی شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>چشمت همه نرگسست و نرگس همه خواب</p></div>
<div class="m2"><p>لعلت همه آتشست و آتش همه آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رویت همه لاله است و نرگس همه رنگ</p></div>
<div class="m2"><p>زلفت همه سنبلست و سنبل همه ناب</p></div></div>