---
title: >-
    رباعی شمارهٔ ۵۰
---
# رباعی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>در دیدهٔ ما هر دو جهان آینه است</p></div>
<div class="m2"><p>جانان چو نماینده و جان آینه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عینیست که باطنا نماینده بود</p></div>
<div class="m2"><p>هر چند که ظاهرا نهان آینه است</p></div></div>