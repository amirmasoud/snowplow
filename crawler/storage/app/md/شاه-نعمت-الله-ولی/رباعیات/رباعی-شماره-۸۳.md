---
title: >-
    رباعی شمارهٔ ۸۳
---
# رباعی شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>دل دوش دم از لطف الهی می زد</p></div>
<div class="m2"><p>در ملک قدم خیمهٔ شاهی میزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌ زحمت آب و گل دل زنده دلم</p></div>
<div class="m2"><p>مستانه دم از نامتناهی میزد</p></div></div>