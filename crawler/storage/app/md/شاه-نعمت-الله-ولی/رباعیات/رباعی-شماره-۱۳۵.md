---
title: >-
    رباعی شمارهٔ ۱۳۵
---
# رباعی شمارهٔ ۱۳۵

<div class="b" id="bn1"><div class="m1"><p>تقوی که در او اسم الهی نبود</p></div>
<div class="m2"><p>یا مقتبس خبر از شاهی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تقوی چنان از خللی خالی نیست</p></div>
<div class="m2"><p>شاید که کسی به آن مباهی نبود</p></div></div>