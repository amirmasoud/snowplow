---
title: >-
    رباعی شمارهٔ ۴۷
---
# رباعی شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>آئینهٔ حضرت الهی دل تو است</p></div>
<div class="m2"><p>گنجینه و گنج پادشاهی دل تو است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بحر محیط است و در او دُر یتیم</p></div>
<div class="m2"><p>در صدفی چنین که خواهی دل توست</p></div></div>