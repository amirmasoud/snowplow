---
title: >-
    رباعی شمارهٔ ۱۱۸
---
# رباعی شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>هر دل که به ذوق سرمدی خواهد بود</p></div>
<div class="m2"><p>در دایرهٔ محمدی خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یار که مذهب حسینی دارد</p></div>
<div class="m2"><p>او طالب سرّ احمدی خواهد بود</p></div></div>