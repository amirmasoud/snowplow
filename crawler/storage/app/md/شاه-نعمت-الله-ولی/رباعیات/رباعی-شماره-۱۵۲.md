---
title: >-
    رباعی شمارهٔ ۱۵۲
---
# رباعی شمارهٔ ۱۵۲

<div class="b" id="bn1"><div class="m1"><p>در مجلس ما که ترک می نتوان کرد</p></div>
<div class="m2"><p>با عقل بیان عشق وی نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون اوست حقیقت وجود همه چیز</p></div>
<div class="m2"><p>ادراک وجود هیچ شی نتوان کرد</p></div></div>