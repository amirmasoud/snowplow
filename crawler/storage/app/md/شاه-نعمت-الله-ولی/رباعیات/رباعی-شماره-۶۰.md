---
title: >-
    رباعی شمارهٔ ۶۰
---
# رباعی شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>ذاتی که به نزد ما نه فرد است و نه جفت</p></div>
<div class="m2"><p>دُریست که اندرین سخن نتوان سفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه جای من و تو که شناسیم او را</p></div>
<div class="m2"><p>معلوم خود و عالم خود نتوان گفت</p></div></div>