---
title: >-
    رباعی شمارهٔ ۲۳۲
---
# رباعی شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>با شمع رخش دمی چو دمساز شدم</p></div>
<div class="m2"><p>پروانهٔ مستمند جانباز شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن روز که این قفس بباید پرداخت</p></div>
<div class="m2"><p>چون شهبازی به دست شه باز شدم</p></div></div>