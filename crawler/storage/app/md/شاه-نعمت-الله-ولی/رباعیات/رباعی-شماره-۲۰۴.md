---
title: >-
    رباعی شمارهٔ ۲۰۴
---
# رباعی شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>من در ره عشق جان و دل باخته ام</p></div>
<div class="m2"><p>سر بر سر کوی دوست انداخته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خود را به خود و خدای خود را به خدا</p></div>
<div class="m2"><p>بشناخته ام چنان که بشناخته ام</p></div></div>