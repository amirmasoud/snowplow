---
title: >-
    رباعی شمارهٔ ۳۸
---
# رباعی شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>درد تو ندیم دل شیدای منست</p></div>
<div class="m2"><p>ورد تو نهان و آشکارای منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر سر زلف تو که جانم به فداش</p></div>
<div class="m2"><p>کفرش خوانند نور ایمان منست</p></div></div>