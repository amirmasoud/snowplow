---
title: >-
    غزل شمارهٔ ۱۲۸۷
---
# غزل شمارهٔ ۱۲۸۷

<div class="b" id="bn1"><div class="m1"><p>در چشم پر آب ما نظر کن</p></div>
<div class="m2"><p>هر سو برو و ز ما خبر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سودای میان تهی چه داری</p></div>
<div class="m2"><p>رندانه بیا ز سر به در کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک کف پای عاشقان شو</p></div>
<div class="m2"><p>خود را به کمال معتبر کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر می خواهی بهشت جاوید</p></div>
<div class="m2"><p>مستانه به بزم ما گذر کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هستی بگذار عارفانه</p></div>
<div class="m2"><p>در عالم نیستی سفر کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامی ز حباب پر کن از آب</p></div>
<div class="m2"><p>با ما تو حدیث بحر و بر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنگر تو جمال نعمت الله</p></div>
<div class="m2"><p>در جام جهان نما نظر کن</p></div></div>