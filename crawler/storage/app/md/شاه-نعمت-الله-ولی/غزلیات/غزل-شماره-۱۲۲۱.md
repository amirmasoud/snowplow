---
title: >-
    غزل شمارهٔ ۱۲۲۱
---
# غزل شمارهٔ ۱۲۲۱

<div class="b" id="bn1"><div class="m1"><p>هر دمی نقش خیالی می نگارد نور چشم</p></div>
<div class="m2"><p>هر نفس شکلی دگر از نو برآرد نور چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این چنین خوناب دل کز چشم ما گشته روان</p></div>
<div class="m2"><p>چشم ما بی آبروئی کی گذارد نور چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون خیال اوست هر نقشی که آید در نظر</p></div>
<div class="m2"><p>لاجرم بر پردهٔ دیده نگارد نور چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم مستش دل ز عیاران عالم می برد</p></div>
<div class="m2"><p>مردم گوشه نشین را خود چه آرد نور چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت الله نور چشم مردم بینا بود</p></div>
<div class="m2"><p>این چنین نوری به مردم می سپارد نور چشم</p></div></div>