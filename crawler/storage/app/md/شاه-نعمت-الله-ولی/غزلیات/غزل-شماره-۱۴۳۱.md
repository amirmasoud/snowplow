---
title: >-
    غزل شمارهٔ ۱۴۳۱
---
# غزل شمارهٔ ۱۴۳۱

<div class="b" id="bn1"><div class="m1"><p>جز یکی نیست بیائید که گوئیم همه</p></div>
<div class="m2"><p>همه از عین یکی باز بجوئیم همه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که گوئی که چنان گفت و چنین می گوید</p></div>
<div class="m2"><p>وقت آن است که در آب بشوئیم همه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما همه آب حیاتیم و همه بحر محیط</p></div>
<div class="m2"><p>گرچه مانند حبابیم بر اوئیم همه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوی آن زلف ز هر تارهٔ مو می شنویم</p></div>
<div class="m2"><p>لاجرم زلف بتان جمله ببوئیم همه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل دیوانه شود چون شنود قصهٔ عشق</p></div>
<div class="m2"><p>دور نبود که بگوئیم که دوئیم همه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آبروی همهٔ قطره چو ما می بینیم</p></div>
<div class="m2"><p>شاید ار ما همهٔ قطره بپوئیم همه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله چو یکی باشد آن یک همه اوست</p></div>
<div class="m2"><p>آن یکی را سزد ار زان که بگوئیم همه</p></div></div>