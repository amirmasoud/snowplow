---
title: >-
    غزل شمارهٔ ۱۶۱
---
# غزل شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>عشق او آب حیات و آن حیات جان ماست</p></div>
<div class="m2"><p>این چنین سرچشمه ای در جان جاویدان ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج عشق او که در عالم نمی گنجد همه</p></div>
<div class="m2"><p>از دل ما جو که جایش در دل ویران ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان ما با غیر اگر باری حکایت کرده است</p></div>
<div class="m2"><p>تا قیامت نادم است انصاف او بر جان ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نزد ما موج و حباب و قطره و دریا یکیست</p></div>
<div class="m2"><p>گر نظر بر آب داری این همه از کان ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که بینی دست او را بوسه ده از ما بپرس</p></div>
<div class="m2"><p>زانکه او از روی معنی صورت جانان ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سماع عاشقان آن ماه چرخی می زند</p></div>
<div class="m2"><p>خوش بود دور قمر دریاب کاین دوران ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که هست از نعمةالله خوش نصیبی یافته</p></div>
<div class="m2"><p>نعمت الله با همه نعمت که دارد آن ماست</p></div></div>