---
title: >-
    غزل شمارهٔ ۷۸۹
---
# غزل شمارهٔ ۷۸۹

<div class="b" id="bn1"><div class="m1"><p>خاکساران که گو به پاکردند</p></div>
<div class="m2"><p>کی توانند گرد ما گردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقانی که عشق می بازند</p></div>
<div class="m2"><p>پیش معشوق جان فدا کردند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می خمخانهٔ حدوث و قدم</p></div>
<div class="m2"><p>باده نوشان به جرعه ای خوردند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُرد دردش به دست رندان ده</p></div>
<div class="m2"><p>نه به آن زاهدان که بی دردند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر صدند ار هزار اهل کمال</p></div>
<div class="m2"><p>عاشقانه به عشق او فردند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زندگانی که کشتهٔ عشقند</p></div>
<div class="m2"><p>نزد مردان مرد ما مردند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کرم حضرت خدا و رسول</p></div>
<div class="m2"><p>نعمت الله به ذوق پروردند</p></div></div>