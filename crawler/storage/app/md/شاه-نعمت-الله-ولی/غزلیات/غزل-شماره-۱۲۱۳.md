---
title: >-
    غزل شمارهٔ ۱۲۱۳
---
# غزل شمارهٔ ۱۲۱۳

<div class="b" id="bn1"><div class="m1"><p>نور چشمست او به او بینیم</p></div>
<div class="m2"><p>لاجرم جمله را نکو بینیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چو احول نه ایم ای بینا</p></div>
<div class="m2"><p>کی چو احول یکی به دو بینیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه گر هزار می نگریم</p></div>
<div class="m2"><p>خود و محبوب روبرو بینیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجمع زلف او پریشان شد</p></div>
<div class="m2"><p>حال مجموع مو به مو بینیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی به ماه می یابیم</p></div>
<div class="m2"><p>بلکه او را به نور او بینیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج بحریم و سو به سو گردیم</p></div>
<div class="m2"><p>آب در دیده سو به سو بینیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همه عالم چو نعمت الله است</p></div>
<div class="m2"><p>غیر او را بگو که چو بینیم</p></div></div>