---
title: >-
    غزل شمارهٔ ۹۱۱
---
# غزل شمارهٔ ۹۱۱

<div class="b" id="bn1"><div class="m1"><p>عشق بازی روان از جان برخیز</p></div>
<div class="m2"><p>عاشقانه ز جان روان برخیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدمی نه به خانهٔ خمّار</p></div>
<div class="m2"><p>منشین در خمار هان برخیز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر سودای عشق اگر داری</p></div>
<div class="m2"><p>از سر سود و از زیان برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خیز مستانه بر فشان دستی</p></div>
<div class="m2"><p>در سماعی چنین چنان برخیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو حجاب توئی چنین منشین</p></div>
<div class="m2"><p>کرمی کن از این میان برخیز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات عشق رندانه</p></div>
<div class="m2"><p>بنشین و ازین جهان برخیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت اله در سماع آمد</p></div>
<div class="m2"><p>وقت وقتست یک زمان برخیز</p></div></div>