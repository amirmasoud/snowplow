---
title: >-
    غزل شمارهٔ ۸۷۳
---
# غزل شمارهٔ ۸۷۳

<div class="b" id="bn1"><div class="m1"><p>به کام ماست می و جام و جسم و جان هر چار</p></div>
<div class="m2"><p>چه خوش بود که بود یار آن چنان هر چار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حباب و قطره و دریا و موج را دریاب</p></div>
<div class="m2"><p>به عین ما نظری کن یکی است آن هر چار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چهار حرف بگیر و خوشی بگو الله</p></div>
<div class="m2"><p>یگانه باش و یکی را روان بخوان هر چار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حریف سرخوش و ساقی مست و جام شراب</p></div>
<div class="m2"><p>امید هست که باشند جاودان هر چار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهار طبع مخالف موافقت کردند</p></div>
<div class="m2"><p>ببین مخالفت این مخالفان هر چار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یکی است اول و آخر چو ظاهر و باطن</p></div>
<div class="m2"><p>چهار اسم مسمی یکی بدان هر چار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چهار یار رسولند دوستان خدا</p></div>
<div class="m2"><p>به دوستی یکی دوست دارشان هر چار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چهار مرتبه سید تنزلی فرمود</p></div>
<div class="m2"><p>ترقئی کن و می جو ز عاشقان هر چار</p></div></div>