---
title: >-
    غزل شمارهٔ ۱۳۹۶
---
# غزل شمارهٔ ۱۳۹۶

<div class="b" id="bn1"><div class="m1"><p>همچو ما کیست در جهان تشنه</p></div>
<div class="m2"><p>بحر جودیم و همچنان تشنه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عین آب حیات چشمهٔ ماست</p></div>
<div class="m2"><p>چشمه در چشم ما به جان تشنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می رود آب چشم ما هر سو</p></div>
<div class="m2"><p>ما به هر سو شده روان تشنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش کناری پرآب و دیدهٔ ماست</p></div>
<div class="m2"><p>ما فتاده در این میان تشنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه عالم گرفته آب زلال</p></div>
<div class="m2"><p>حیف باشد که تشنگان تشنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب دریا و تشنه مستسقی</p></div>
<div class="m2"><p>می خورد آب ناتوان تشنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن سید است آب حیات</p></div>
<div class="m2"><p>خضر وقت امان به آن تشنه</p></div></div>