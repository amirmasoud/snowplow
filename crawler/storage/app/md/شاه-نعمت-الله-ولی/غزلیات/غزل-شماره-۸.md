---
title: >-
    غزل شمارهٔ ۸
---
# غزل شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>خار بی کنگر چه کار آید مرا</p></div>
<div class="m2"><p>راه بی رهبر چه کار آید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نباشد مرتضی با من رفیق</p></div>
<div class="m2"><p>خدمت قنبر چه کار آید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیسی مریم همی جویم به جان</p></div>
<div class="m2"><p>بندگی خر چه کار آید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه سر باشد فدای پای او</p></div>
<div class="m2"><p>دردسر بر سر چه کار آید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوشتر از مشک است بوی یار من</p></div>
<div class="m2"><p>مشک یا عنبر چه کار آید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خم مِی دارم مدام از حضرتش</p></div>
<div class="m2"><p>جام یا ساغر چه کار آید مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندگی سیدم چون پیشوا است</p></div>
<div class="m2"><p>خدمت سنجر چه کار آید مرا</p></div></div>