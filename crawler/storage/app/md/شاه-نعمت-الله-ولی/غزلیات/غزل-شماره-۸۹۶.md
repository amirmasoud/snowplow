---
title: >-
    غزل شمارهٔ ۸۹۶
---
# غزل شمارهٔ ۸۹۶

<div class="b" id="bn1"><div class="m1"><p>رند مستیم و عشق شورانگیز</p></div>
<div class="m2"><p>عقل مخور گو ز ما پرهیز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساقیا خم می بیار آن دم</p></div>
<div class="m2"><p>خم می بر سر حریفان ریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر در می فروش خوش بنشین</p></div>
<div class="m2"><p>از سر کاینات هم برخیز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جاودان گر حیات می‌جوئی</p></div>
<div class="m2"><p>جان و جانان به همدگر آمیز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر حلیمی تو بردباری کن</p></div>
<div class="m2"><p>آب دیده به خاک ایشان ریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر خاک عاشقان چو رسی</p></div>
<div class="m2"><p>قصر شیرین بساز و هم شبدیز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو فرهاد میل خسرو کن</p></div>
<div class="m2"><p>گو مترس از صلابت پرویز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق شیرین گرش بود فرهاد</p></div>
<div class="m2"><p>عشق سرمست و خنجر سر تیز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عقل مخمور و إرهٔ عمری</p></div>
<div class="m2"><p>به ازین نیست هیچ دست آویز</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دامن سیدم به دست آور</p></div>
<div class="m2"><p>به ازین نیست هیچ دست‌ آویز</p></div></div>