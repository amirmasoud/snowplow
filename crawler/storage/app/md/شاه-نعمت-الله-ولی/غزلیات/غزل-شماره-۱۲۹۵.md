---
title: >-
    غزل شمارهٔ ۱۲۹۵
---
# غزل شمارهٔ ۱۲۹۵

<div class="b" id="bn1"><div class="m1"><p>عاشقانه بشنو و خوش پند ما را گوش کن</p></div>
<div class="m2"><p>در خرابات فنا جام بلا را نوش کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرخوشانه پای کوبان از در خلوت در آ</p></div>
<div class="m2"><p>دست دل با دلبر سرمست در آغوش کن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق سرمستی اگر داری در آ در میکده</p></div>
<div class="m2"><p>آتشی درخوردن و چون خم می خوش جوش کن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدی گر گویدت از باده نوشی توبه کن</p></div>
<div class="m2"><p>جرعه ای در کام جانش ریز گو خاموش کن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاه عشق خوش در غارت ملک دلست</p></div>
<div class="m2"><p>گر تو را عشقست جان ، دل فدای اوش کن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مطربا قولی بگو عشاق را خوشوقت ساز</p></div>
<div class="m2"><p>ساقیا جامی بیار و عالمی مدهوش کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله این سخن از ذوق می گوید به تو</p></div>
<div class="m2"><p>ذوق اگر داری بیا و عاشقانه گوش کن</p></div></div>