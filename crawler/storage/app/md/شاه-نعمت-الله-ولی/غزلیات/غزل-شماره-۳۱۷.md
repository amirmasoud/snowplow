---
title: >-
    غزل شمارهٔ ۳۱۷
---
# غزل شمارهٔ ۳۱۷

<div class="b" id="bn1"><div class="m1"><p>عشق جانان حیات جان من است</p></div>
<div class="m2"><p>خوش حیاتی چنین از آن منست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان دل زنده ام از آن ویست </p></div>
<div class="m2"><p>عشق او جان جاودان منست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر فروشم غمش بهر دو جهان</p></div>
<div class="m2"><p>نزد اهل نظر زیان منست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من امین و امانت سلطان</p></div>
<div class="m2"><p>هست محفوظ و در امان منست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خمخانهٔ حدوث و قدم</p></div>
<div class="m2"><p>همه از بهر عاشقان منست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن معانی که عارفان جویند</p></div>
<div class="m2"><p>گر بدانند در بیان منست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چنین گفته های مستانه</p></div>
<div class="m2"><p>سخن اوست وز زبان منست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تا بود جان به جان ، محب ویم</p></div>
<div class="m2"><p>چون کنم ترک جان که جان منست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حکم سید که یرلغ آل است</p></div>
<div class="m2"><p>آن به نام من و نشان منست</p></div></div>