---
title: >-
    غزل شمارهٔ ۵۲
---
# غزل شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>پادشاه و پادشاهی ما و درویشی ما</p></div>
<div class="m2"><p>عاقلان و آشنائی ما و بی خویشی ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در میان عشقبازان ما کمیم از هر یکی</p></div>
<div class="m2"><p>از کمی ماست در عالم همه بیشی ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواجه گر دارد غنا آرد غنائی بر غنا</p></div>
<div class="m2"><p>از غنای خواجه خوشتر فقر درویشی ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بنده دلریش سلطانیم و مرهم وصل اوست</p></div>
<div class="m2"><p>عاقبت رحمی کند سلطان به دلریشی ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت سید که دیدی آخرش خوانی رواست</p></div>
<div class="m2"><p>معنی او را نگر دریاب این پیشی ما</p></div></div>