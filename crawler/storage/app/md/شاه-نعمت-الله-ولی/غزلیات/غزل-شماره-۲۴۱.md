---
title: >-
    غزل شمارهٔ ۲۴۱
---
# غزل شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>در دل ما عشقش از جان خوشتر است</p></div>
<div class="m2"><p>جان چه باشد عشق جانان خوشتر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق او گنجی و دل ویرانه ای</p></div>
<div class="m2"><p>گنج او در کنج ویران خوشتر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود یک جام می شادی ما</p></div>
<div class="m2"><p>بلکه می خوردن فراوان خوشتر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب چشم ما بهر سو می رود</p></div>
<div class="m2"><p>عین ما از بحر عمان خوشتر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز دل با غیر پیدا کی کنم</p></div>
<div class="m2"><p>سر او در سینه پنهان خوشتر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت بلبل در گلستان خوش بود</p></div>
<div class="m2"><p>مجلس ما از گلستان خوشتر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله گر تو را باشد خوش است</p></div>
<div class="m2"><p>ور نباشد مفلسی زان خوشتر است</p></div></div>