---
title: >-
    غزل شمارهٔ ۱۱۴۸
---
# غزل شمارهٔ ۱۱۴۸

<div class="b" id="bn1"><div class="m1"><p>مستانه ملک صورت و معنی به هم زدیم</p></div>
<div class="m2"><p>رندانه در قدم قدمی از عدم زدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را مسلم است دم از نیستی زدن</p></div>
<div class="m2"><p>کز هستی وجود رقم بر عدم زدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه وار کاغذ تن را بسوختیم</p></div>
<div class="m2"><p>وز شمع عشق آتشی اندر قلم زدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتیم انا الحق و علم عالمی شدیم</p></div>
<div class="m2"><p>منصور وار بر سر داری علم زدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما عارفان سرخوش دلشاد عاشقیم</p></div>
<div class="m2"><p>مستیم و لاابالی و غم را به هم زدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با جام می مدام حریفانه همدمیم</p></div>
<div class="m2"><p>مستانه زان مدام ز میخانه دم زدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دیده روی ساقی و بر دست جام می</p></div>
<div class="m2"><p>شادی روی سید خود جام جم زدیم</p></div></div>