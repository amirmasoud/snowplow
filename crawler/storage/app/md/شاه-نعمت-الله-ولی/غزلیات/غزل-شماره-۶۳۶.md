---
title: >-
    غزل شمارهٔ ۶۳۶
---
# غزل شمارهٔ ۶۳۶

<div class="b" id="bn1"><div class="m1"><p>عقل از اینجا بی خبر او ره به بالا کی برد</p></div>
<div class="m2"><p>مرغ وهم ار پر بسوزد ره به مأوا کی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل مخمور است و میخانه نمی داند کجاست</p></div>
<div class="m2"><p>این چنین شخصی به میخانه شما را کی برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس عشقست و سلطان ساقی و رندان حریف</p></div>
<div class="m2"><p>هر گدای بی سر و پا را به آنجا کی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از لب شیرین یوسف هر که یابد بوسه ای</p></div>
<div class="m2"><p>کی برد شکر به مصر و نام حلوا کی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم مزن از معرفت با ما در این بحر محیط</p></div>
<div class="m2"><p>مرد عاقل آب دریا سوی دریا کی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رستم دستان زبردستی کند با این و آن</p></div>
<div class="m2"><p>گر به دست ما فتد او دست از ما کی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله هر چه می یابد مسمای ویست</p></div>
<div class="m2"><p>با چنین کشف خوشی او اسم اسماء کی برد</p></div></div>