---
title: >-
    غزل شمارهٔ ۷۳۵
---
# غزل شمارهٔ ۷۳۵

<div class="b" id="bn1"><div class="m1"><p>ورد صاحبنظران فاتحهٔ روی تو باد</p></div>
<div class="m2"><p>قل هوالله احد حرز دو ابروی تو باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جاء نصرالله ای شاه چو بنمودی روی</p></div>
<div class="m2"><p>آیة الکرسی تعویذ دو گیسوی تو باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>والضحی روی تو آمد سر زلفت و اللیل</p></div>
<div class="m2"><p>آفرین بر سر زلف تو و ابروی تو باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک و الشمس که بر جملهٔ افلاک شه است</p></div>
<div class="m2"><p>آیت کنت تو را بازد و هندوی تو باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فتح و یاسین و تبارک طرف آخر حشر</p></div>
<div class="m2"><p>این چهار آیه حق بندد و بازوی تو باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ان یکاد از نفس روح امین در شب و روز</p></div>
<div class="m2"><p>دافع چشم بدان از رخ نیکوی تو باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله به دعا خوانده ز آناء اللیل</p></div>
<div class="m2"><p>که دلش بستهٔ گیسو و رخش سوی تو باد</p></div></div>