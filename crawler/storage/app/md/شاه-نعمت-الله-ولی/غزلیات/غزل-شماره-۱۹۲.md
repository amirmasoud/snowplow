---
title: >-
    غزل شمارهٔ ۱۹۲
---
# غزل شمارهٔ ۱۹۲

<div class="b" id="bn1"><div class="m1"><p>چشم عالم روشن از نور خداست</p></div>
<div class="m2"><p>هر که این را دید نور چشم ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل آن کس که او گنجیده است</p></div>
<div class="m2"><p>همچو او صاحبدلی دیگر کراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال ما داند درین دریا به ذوق</p></div>
<div class="m2"><p>یار بحر وی که با ما آشناست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُرد درد او اگر یابی بنوش</p></div>
<div class="m2"><p>زانکه دُرد درد او ما را دواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذرهٔ خورشید این و آن همه</p></div>
<div class="m2"><p>در نظر آئینه گیتی نماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق ار در عشق او کشته شود</p></div>
<div class="m2"><p>حضرت معشوق او را خونبهاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله رند سرمستی خوشست</p></div>
<div class="m2"><p>پادشاهست او نپنداری گداست</p></div></div>