---
title: >-
    غزل شمارهٔ ۲۵۲
---
# غزل شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>صورت و معنی به همدیگر خوش است</p></div>
<div class="m2"><p>آن چنان می در چنین ساغر خوشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس عشقست و ما مست و خراب</p></div>
<div class="m2"><p>ما چنین هستیم و ساقی سرخوشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او با ما درین دریا نشست</p></div>
<div class="m2"><p>از سرش تا پاشنه در زر خوشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان به جانان دل بدلبر داده ایم</p></div>
<div class="m2"><p>در دل ما عشق آن دلبر خوشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر در یتیم از ما بجو</p></div>
<div class="m2"><p>گر به دست آری چنین گوهر خوشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عود دل در مجمر سینه بسوخت</p></div>
<div class="m2"><p>بوی خوش ما را درین مجمر خوشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله دارد از سید نشان</p></div>
<div class="m2"><p>این نشان آل پیغمبر خوشست</p></div></div>