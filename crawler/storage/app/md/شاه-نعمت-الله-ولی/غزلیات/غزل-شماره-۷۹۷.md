---
title: >-
    غزل شمارهٔ ۷۹۷
---
# غزل شمارهٔ ۷۹۷

<div class="b" id="bn1"><div class="m1"><p>عاشقان درش از درد دوا یافته اند</p></div>
<div class="m2"><p>خستگان غمش از رنج شفا یافته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده نوشان سراپردهٔ میخانهٔ دل</p></div>
<div class="m2"><p>جرعهٔ دُردی دردش چو دوا یافته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مبتلایان بلایش ز بلا نگریزند</p></div>
<div class="m2"><p>گرچه از قامت و بالاش بلا یافته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نم چشم و غم دل قوت روان ساز ای جان</p></div>
<div class="m2"><p>که کسان قوت از این آب و هوا یافته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفان بی سر و پا بر سر دارش رفتند</p></div>
<div class="m2"><p>لاجرم اجر فنا دار بقا یافته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن کسانی که چو ما غرقهٔ دریا شده اند</p></div>
<div class="m2"><p>گوهر حاصل ما در دل ما یافته اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو سید ز خود آثار خدا یافته اند</p></div>
<div class="m2"><p>خود شناسان که مقیم حرم مقصودند</p></div></div>