---
title: >-
    غزل شمارهٔ ۱۳۳۶
---
# غزل شمارهٔ ۱۳۳۶

<div class="b" id="bn1"><div class="m1"><p>بقا در عشق اگر خواهی فنا شو</p></div>
<div class="m2"><p>حیات از وصل اگر جوئی چو ما شو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشو خودبین و خود را نیک دریاب</p></div>
<div class="m2"><p>بدان خود را و دانای خدا شو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اناالحق زن چو منصور از سر عشق</p></div>
<div class="m2"><p>بر آ بر دار و در دارالبقا شو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدف دریاب و گوهر را طلب کن</p></div>
<div class="m2"><p>در آ در بحر و با ما آشنا شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوی گلشن جانان گذر کن</p></div>
<div class="m2"><p>بسان بلبل جان خوش نوا شو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فابقوا بالبقاء قرب ربی</p></div>
<div class="m2"><p>فافنوا از وجود خود فنا شو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سید بندهٔ این شاه می باش</p></div>
<div class="m2"><p>به باطن خواجه و ظاهر گدا شو</p></div></div>