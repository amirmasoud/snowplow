---
title: >-
    غزل شمارهٔ ۱۴۷۶
---
# غزل شمارهٔ ۱۴۷۶

<div class="b" id="bn1"><div class="m1"><p>حرف جام شراب اگر دانی</p></div>
<div class="m2"><p>نسخهٔ جسم و روح برخوانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورتا ساغری و معنی می</p></div>
<div class="m2"><p>ظاهراً این و باطناً آنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق و معشوق و عاشق خویشی</p></div>
<div class="m2"><p>دل و دلدار و جان و جانانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سر زلف او پریشان شو</p></div>
<div class="m2"><p>جمع می باش از پریشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در نظر نور دیدهٔ خلقی</p></div>
<div class="m2"><p>گرچه از نور دیده پنهانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه خواهی ز خود طلب می کن</p></div>
<div class="m2"><p>که توئی هر چه خواهی ار دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی روی نعمت الله نوش</p></div>
<div class="m2"><p>می وحدت ز جام سبحانی</p></div></div>