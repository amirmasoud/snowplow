---
title: >-
    غزل شمارهٔ ۴
---
# غزل شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>ما به عین تو دیده ایم تو را</p></div>
<div class="m2"><p>وز همه برگزیده ایم تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقانه یگانه در شب و روز</p></div>
<div class="m2"><p>در کش خود کشیده ایم تو را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور چشمی و در نظر داریم</p></div>
<div class="m2"><p>ما به عین تو دیده ایم تو را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به وجود آفریده ای ما را</p></div>
<div class="m2"><p>به ظهور آورده ایم تو را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت الله را فروخته ایم </p></div>
<div class="m2"><p>به بهایش خریده ایم تو را</p></div></div>