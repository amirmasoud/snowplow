---
title: >-
    غزل شمارهٔ ۱۲۰۲
---
# غزل شمارهٔ ۱۲۰۲

<div class="b" id="bn1"><div class="m1"><p>به سر خواجه که ما مستانیم</p></div>
<div class="m2"><p>غیر می هر چه دهی نستانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>داستان همه عالم مائیم</p></div>
<div class="m2"><p>دست ما گیر کز آن دستانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات مغان مست وخراب</p></div>
<div class="m2"><p>ساقی مجلس سر مستانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل و دلدار خودیم و می و جام</p></div>
<div class="m2"><p>جان و جانانه و این وآنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مطرب خوش نفس عشاقیم</p></div>
<div class="m2"><p>عاشقانه غزلی می خوانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حالت ما دگر و ما دگریم</p></div>
<div class="m2"><p>خدمتش زاهد و ما رندانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله نهاده خوانی</p></div>
<div class="m2"><p>قدمی نه که همه مهمانیم</p></div></div>