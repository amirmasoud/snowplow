---
title: >-
    غزل شمارهٔ ۱۷
---
# غزل شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>رفت آن جانان ما از دست ما</p></div>
<div class="m2"><p>از دریغا دلبر سر مست ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>او برفت و پای او نگشوده‌ایم</p></div>
<div class="m2"><p>تا ابد زلفش بود پا‌بست ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما همه جا نیکی او گفته‌ایم</p></div>
<div class="m2"><p>او نخواهد آنچنان اشکست ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاره‌ای غیر رضا و صبر نیست</p></div>
<div class="m2"><p>این زمان چون تیر رفت از شست ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خیال او است جان ما مدام</p></div>
<div class="m2"><p>دل روان خواهد به او پیوست ما</p></div></div>