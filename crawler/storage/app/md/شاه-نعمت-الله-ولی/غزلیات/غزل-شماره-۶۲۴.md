---
title: >-
    غزل شمارهٔ ۶۲۴
---
# غزل شمارهٔ ۶۲۴

<div class="b" id="bn1"><div class="m1"><p>هوای درد بی درمان که دارد</p></div>
<div class="m2"><p>سر سودای بی سامان که دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفیق راه بی پایان که جوید</p></div>
<div class="m2"><p>خیال مجلس جانان که دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه کس طالب آنند و ما هم</p></div>
<div class="m2"><p>ازین بگذر ببین تا آن که دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو کفر زلف او دین و دلم برد</p></div>
<div class="m2"><p>نظر بر خاطر ایمان که دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا مهمان جان است او شب و روز</p></div>
<div class="m2"><p>چنین شاهی بگو مهمان که دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قدح گردید اکنون نوبت ماست</p></div>
<div class="m2"><p>درین دوران چنین دوران که دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به عشقش چون مجال خود ندارم</p></div>
<div class="m2"><p>بگو پروای خان و مان که دارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو من از جان و دل کردم تبرا</p></div>
<div class="m2"><p>غم از دشوار و از آسان که دارد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوس دارم که جان خود ببازم</p></div>
<div class="m2"><p>ولی سید نظر بر بان که دارد</p></div></div>