---
title: >-
    غزل شمارهٔ ۱۷۶
---
# غزل شمارهٔ ۱۷۶

<div class="b" id="bn1"><div class="m1"><p>هرکجا پیریست طفل پیر ماست</p></div>
<div class="m2"><p>این چنین پیری در این عالم کراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جملهٔ ارواح جزئیات او است</p></div>
<div class="m2"><p>بلکه او در کل عالم پادشاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در صفات و ذات او دیدم عیان</p></div>
<div class="m2"><p>حضرت او مظهر لطف خداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقطهٔ بابل الف بل خود الف</p></div>
<div class="m2"><p>روح اعظم سید هر دو سراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای که می پرسی که این اوصاف کیست</p></div>
<div class="m2"><p>شمه ای از خلق و خوی مصطفی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عین او بحر است و ما امواج او</p></div>
<div class="m2"><p>تا نپنداری که او از ما جداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>من شدم فانی ز خود باقی بُود</p></div>
<div class="m2"><p>بر سر دار فنا دار بقاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کی بیابد لذت از جان عزیز</p></div>
<div class="m2"><p>هر کرا با او به جانش پادشاست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله او به عالم می دهد</p></div>
<div class="m2"><p>نعمت الله نعمت بی منتهاست</p></div></div>