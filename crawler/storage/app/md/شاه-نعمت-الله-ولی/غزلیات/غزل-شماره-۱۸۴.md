---
title: >-
    غزل شمارهٔ ۱۸۴
---
# غزل شمارهٔ ۱۸۴

<div class="b" id="bn1"><div class="m1"><p>نعمت الله امام رندان است</p></div>
<div class="m2"><p>نور چشم تمام رندان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز از دولت چنان شاهی</p></div>
<div class="m2"><p>همه عالم به کام رندان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور رندی و وقت میخواریست</p></div>
<div class="m2"><p>روزگار نظام رندان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قول مستانه ای که میشنوی</p></div>
<div class="m2"><p>دو سه حرف از کلام رندان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سلامی که سنت است به ما</p></div>
<div class="m2"><p>در حقیقت کلام رندان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن شرابی که روحت افزاید</p></div>
<div class="m2"><p>جرعه ای می ز جام رندان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاه ما حکم انّما دارد</p></div>
<div class="m2"><p>آن نشانش به نام رندان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به خرابات رو خوشی بنشین</p></div>
<div class="m2"><p>این نصیحت به نام رندان است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزم عشقست و عاشقان سرمست</p></div>
<div class="m2"><p>سید ما غلام رندان است</p></div></div>