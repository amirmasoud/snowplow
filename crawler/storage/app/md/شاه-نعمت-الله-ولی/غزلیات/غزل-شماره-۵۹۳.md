---
title: >-
    غزل شمارهٔ ۵۹۳
---
# غزل شمارهٔ ۵۹۳

<div class="b" id="bn1"><div class="m1"><p>غیر او کی به یاد ما ماند</p></div>
<div class="m2"><p>دیگری یار ما کجا ماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دُرد دردش بیا و ما را ده</p></div>
<div class="m2"><p>که مرا خوشتر از دوا ماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نبودیم و حضرت او بود</p></div>
<div class="m2"><p>چون نمانیم ما خدا ماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست بیگانه از خدا چیزی</p></div>
<div class="m2"><p>هر چه ماند به آشنا ماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این عجب بین که حضرت سلطان</p></div>
<div class="m2"><p>در نظرگه گهی گدا ماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که مه روی خویش را بیند</p></div>
<div class="m2"><p>خوبی او کجا به ما ماند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم عشق است و سیدم سرمست</p></div>
<div class="m2"><p>بنده مخمور خود چرا ماند</p></div></div>