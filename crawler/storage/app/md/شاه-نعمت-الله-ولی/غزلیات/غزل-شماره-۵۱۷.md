---
title: >-
    غزل شمارهٔ ۵۱۷
---
# غزل شمارهٔ ۵۱۷

<div class="b" id="bn1"><div class="m1"><p>ترک چشم مست او دلها به غارت می برد</p></div>
<div class="m2"><p>جان فدای او که جان ما به غارت می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک دل بگرفت و نقد و نسیه را هر کس که دید</p></div>
<div class="m2"><p>ترکتازی می کند آنها به غارت می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشقیم و ما به عشق او اسیر افتاده ایم</p></div>
<div class="m2"><p>بنده فرمانیم اگر ما را به غارت می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دل ما می برد شکرانه اش بر جان ماست</p></div>
<div class="m2"><p>جان رها کردیم دل را تا به غارت می برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر بازار اگر شخصی دکانی می نهد</p></div>
<div class="m2"><p>دکه ویران می کند کالا به غارت می برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فتنهٔ دور قمر بنگر که چون پیدا شده</p></div>
<div class="m2"><p>آمده تنها و تنها را به غارت می برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله هر چه دارد در نهان و آشکار</p></div>
<div class="m2"><p>یا به حکمت می ستاند یا به غارت می برد</p></div></div>