---
title: >-
    غزل شمارهٔ ۱۴۷۴
---
# غزل شمارهٔ ۱۴۷۴

<div class="b" id="bn1"><div class="m1"><p>تنها نه منم عاشق تو بلکه جهانی</p></div>
<div class="m2"><p>گر جان طلبی هان بسپارند روانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر سو که نظر می کنم ای نور دو چشمم</p></div>
<div class="m2"><p>بینم چو خودی بر سر کویت نگرانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر نام من ای یار بر آید به زبانت</p></div>
<div class="m2"><p>در هر دو جهان یابم از آن نام و نشانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که به پیری رسی ای جان ز جوانی</p></div>
<div class="m2"><p>زنهار مکن قصد دل هیچ جوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این علم معانیست که کردیم بیانش</p></div>
<div class="m2"><p>خود خوشتر از این قول که کرده است بیانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما نقش خیال تو نگاریم به دیده</p></div>
<div class="m2"><p>بی نقش خیال تو نباشیم زمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آینهٔ دیدهٔ سید همه بینند</p></div>
<div class="m2"><p>آن نور که دیدیم در این دیده عیانی</p></div></div>