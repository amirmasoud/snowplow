---
title: >-
    غزل شمارهٔ ۲۷۷
---
# غزل شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>شهر دل در ولایت عشق است</p></div>
<div class="m2"><p>ملک و جان در حمایت عشق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده بینا به نور معرفت است</p></div>
<div class="m2"><p>این عیان از عنایت عشق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنچه عقلم نهایتش می گفت</p></div>
<div class="m2"><p>دیده ام آن بدایت عشق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>لیس فی الدار غیره دیار</p></div>
<div class="m2"><p>این حدیث از روایت عشق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه گوئی ز عشق گو که مرا</p></div>
<div class="m2"><p>سخن خوش حکایت عشق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نالهٔ زار بلبلان شب و روز</p></div>
<div class="m2"><p>در گلستان سرایت عشق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را چنین حیران</p></div>
<div class="m2"><p>گرد حسن کفایت عشق است</p></div></div>