---
title: >-
    غزل شمارهٔ ۴۰۰
---
# غزل شمارهٔ ۴۰۰

<div class="b" id="bn1"><div class="m1"><p>روحها در روح اعظم فانی است</p></div>
<div class="m2"><p>در حقیقت خدمتش هم فانی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه آدم باقی است از وجه حق</p></div>
<div class="m2"><p>هم بوجهی نیز آدم فانی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام جم فانی است نبود این عجب</p></div>
<div class="m2"><p>این عجب بنگر که جم هم فانی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه گوئی فوت شد شادی ما</p></div>
<div class="m2"><p>غم مخور زیرا که هم غم فانی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گردمی با جام می همدم شوی</p></div>
<div class="m2"><p>دمبدم در غیر آن دم فانی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قطره و موج و حباب و جام می</p></div>
<div class="m2"><p>نزد ما این جمله دریم فانی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شبنمی بودیم ما چون آفتاب</p></div>
<div class="m2"><p>خوش طلوعی کرد شبنم فانی است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هرچه باشد غیر او فانی بود</p></div>
<div class="m2"><p>اوست باقی سوز و ماتم فانی است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر بوجهی اسم اعظم اسم اوست</p></div>
<div class="m2"><p>در مسما اسم اعظم فانی است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دیگری را کی بود خود دار و گیر</p></div>
<div class="m2"><p>اندر آن میدان که رستم فانی است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ما همه خود فانی و او باقی است</p></div>
<div class="m2"><p>بشنو از سید که عالم فانی است</p></div></div>