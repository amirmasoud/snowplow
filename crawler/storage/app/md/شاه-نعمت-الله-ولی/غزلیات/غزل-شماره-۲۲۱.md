---
title: >-
    غزل شمارهٔ ۲۲۱
---
# غزل شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>آمد ز درم نگار سرمست</p></div>
<div class="m2"><p>رندانه و جام باده بر دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صد فتنه ز هر کنار برخاست</p></div>
<div class="m2"><p>او مست در این میانه بنشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب را بنهاد بر لب ما</p></div>
<div class="m2"><p>موئی به دونیم راست بشکست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق آمد و زنده کرد ما را</p></div>
<div class="m2"><p>پیوسته بود به ما چو پیوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بود و نبود باز رستیم</p></div>
<div class="m2"><p>آسوده ز نیست فارغ از هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل در سر زلف یار بستیم</p></div>
<div class="m2"><p>محکم جائی شدیم پابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از مستی ذوق نعمت الله</p></div>
<div class="m2"><p>خلق دو جهان شدند سرمست</p></div></div>