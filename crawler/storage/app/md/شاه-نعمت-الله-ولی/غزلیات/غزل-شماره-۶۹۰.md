---
title: >-
    غزل شمارهٔ ۶۹۰
---
# غزل شمارهٔ ۶۹۰

<div class="b" id="bn1"><div class="m1"><p>عقل هر دم که در سرود آید</p></div>
<div class="m2"><p>به دم سرو باده پیماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن عقل پیش عشق مگو</p></div>
<div class="m2"><p>کان سخن خود به کار می ناید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را خود گشایشی دگرست</p></div>
<div class="m2"><p>هیچ کاری ز عقل نگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام گیتی نمای را به کف آر</p></div>
<div class="m2"><p>که به تو روی خویش بنماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی مدام در دور است</p></div>
<div class="m2"><p>به یکی جا دمی نمی پاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق هر لحظه مجلسی سازد</p></div>
<div class="m2"><p>هر زمان بزم نو بیاراید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفسی باش همدم سید</p></div>
<div class="m2"><p>گر تو را همدم خوشی باید</p></div></div>