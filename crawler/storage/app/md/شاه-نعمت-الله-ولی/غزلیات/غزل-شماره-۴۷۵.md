---
title: >-
    غزل شمارهٔ ۴۷۵
---
# غزل شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>در رحمت خدا به ما بگشود</p></div>
<div class="m2"><p>این چنین در خدا به ما بگشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گنجینهٔ حدوث و قدم</p></div>
<div class="m2"><p>به گدایان بینوا بگشود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد گنجینه را به ما بنمود</p></div>
<div class="m2"><p>چشم ما را به عین ما بگشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در به بیگانگان اگر در بست</p></div>
<div class="m2"><p>همه درها به آشنا بگشود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر در صومعه ببست چه شد</p></div>
<div class="m2"><p>در میخانه حالیا بگشود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برقع کاینات را برداشت</p></div>
<div class="m2"><p>این معمای ما به ما بگشود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مشکلاتی که بود حلوا کرد</p></div>
<div class="m2"><p>چشم ما را به آن لقا بگشود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جان ما بود بستهٔ عالم</p></div>
<div class="m2"><p>کرمی کرد و بنده را بگشود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این عنایت نگر که سید ما</p></div>
<div class="m2"><p>در به این بندهٔ گدا بگشود</p></div></div>