---
title: >-
    غزل شمارهٔ ۲۳۷
---
# غزل شمارهٔ ۲۳۷

<div class="b" id="bn1"><div class="m1"><p>ای عاشقان ای عاشقان ما را بیانی دیگر است</p></div>
<div class="m2"><p>ای عارفان ای عارفان ما را نشانی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای بلبلان ای بلبلان ما را نوا خوشتر بود</p></div>
<div class="m2"><p>زیرا که این گلزار ما از بوستانی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای خسروشیرین سخن ای یوسف گل پیرهن</p></div>
<div class="m2"><p>ای طوطی شکرشکن ما را زبانی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاری که اندرکار دل جان داد در بازار دل</p></div>
<div class="m2"><p>همچون دل صاحبدلان زنده به جانی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشیدجمشید فلک بر آسمان چارم است</p></div>
<div class="m2"><p>مهر منیر عاشقان بر آسمانی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا عین عشقش دیده ام مهرش به جان بگزیده ام</p></div>
<div class="m2"><p>در آشکارا و نهان ما را عیانی دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اقلیم دل شد ملک جان شهر تن آید این جهان</p></div>
<div class="m2"><p>کون و مکان عاشقان در لامکانی دیگر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>رند و در میخانه ها صوفی و کنج صومعه</p></div>
<div class="m2"><p>مارا سریر سلطنت برآستانی دیگر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدمرا جانان بود هم دردو هم درمان بود</p></div>
<div class="m2"><p>جانم فدای جان او کو از جهانی دیگر است</p></div></div>