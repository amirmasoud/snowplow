---
title: >-
    غزل شمارهٔ ۱۵۳۲
---
# غزل شمارهٔ ۱۵۳۲

<div class="b" id="bn1"><div class="m1"><p>متناهی شود به تو همه شی</p></div>
<div class="m2"><p>تو شوی منتهی به حضرت وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غایت ذوق ما کجا یابد</p></div>
<div class="m2"><p>به جز از ما و همچو ما هی هی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد و زهد و آرزوی نماز</p></div>
<div class="m2"><p>ما و ساقی و ساغر پر می</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتهٔ عشق و زندهٔ ابد است</p></div>
<div class="m2"><p>کی بمیرد کسی که زو شد حی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابست و عالمی سایه</p></div>
<div class="m2"><p>هر کجا او رود رود در پی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نو او را به نور او دیدیم</p></div>
<div class="m2"><p>نه به یک چیز بلکه در همه شی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر سید ز نعمت الله جو</p></div>
<div class="m2"><p>دم نائی طلب کنش از نی</p></div></div>