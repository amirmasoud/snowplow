---
title: >-
    غزل شمارهٔ ۱۳۹۸
---
# غزل شمارهٔ ۱۳۹۸

<div class="b" id="bn1"><div class="m1"><p>ساقی بده آن می شبانه</p></div>
<div class="m2"><p>مستم کن ازین شرابخانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو تو رموز عشقبازان</p></div>
<div class="m2"><p>کان است نشان و این نشانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داریم بقای مطلق از حق</p></div>
<div class="m2"><p>باقی همه کارها بهانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار دل ماست عشقبازی</p></div>
<div class="m2"><p>از دولت عشق جاودانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پروانهٔ جان ما روان سوخت</p></div>
<div class="m2"><p>چون آتش عشق جاودانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر میل کنار یار داری</p></div>
<div class="m2"><p>جانست بیار در میانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هستی خود چو نیست گشتی</p></div>
<div class="m2"><p>در هر دو جهان توئی یگانه</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دامی است وجود آدم ای یار</p></div>
<div class="m2"><p>مائیم شکار و روح دانه</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مطرب بنواز قول سید</p></div>
<div class="m2"><p>وز نغمهٔ ساز عاشقانه</p></div></div>