---
title: >-
    غزل شمارهٔ ۸۱۳
---
# غزل شمارهٔ ۸۱۳

<div class="b" id="bn1"><div class="m1"><p>قطره قطره جمع کن دریا نگر</p></div>
<div class="m2"><p>آب را می نوش و ذوق ما نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نه ای احول یکی را دو مبین</p></div>
<div class="m2"><p>سر به سر یکتای بی همتا نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه گر صد نماید ور هزار</p></div>
<div class="m2"><p>در صفای هر یکی او را نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه بینی مظهر اسمای اوست</p></div>
<div class="m2"><p>مظهر ما در همه اشیانگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی می نگردد ذرهٔ</p></div>
<div class="m2"><p>یک نظر در روی مه سیما نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر تو می پرسی که جای او کجاست</p></div>
<div class="m2"><p>جای آن بی جای ما هر جا نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را به نور او ببین</p></div>
<div class="m2"><p>چشم بگشا دیدهٔ بینا نگر</p></div></div>