---
title: >-
    غزل شمارهٔ ۱۳۰۸
---
# غزل شمارهٔ ۱۳۰۸

<div class="b" id="bn1"><div class="m1"><p>ای منور دیدهٔ مردم به نور روی تو</p></div>
<div class="m2"><p>عالمی آشفته چون باد صبا از بوی تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل می خواهد که گردد گرد کوی تو ولی</p></div>
<div class="m2"><p>گرد اگر گردد نگردد هیچ گرد کوی تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه می بینم بود در چشم من آئینه ای</p></div>
<div class="m2"><p>می نماید در نظر نقش خیال روی تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به کعبه می روم یا می روم در میکده</p></div>
<div class="m2"><p>واقفی بر حال من باشم به جستجوی تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما در این دریا به هر سوئی که کشتی می رود</p></div>
<div class="m2"><p>می رویم و رفتن ما نیست الا سوی تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قیمت یک موی تو دنیی و عقبی کی دهد</p></div>
<div class="m2"><p>کی ستانم کی دهد یک تارئی از موی تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد مخمور باشد روز و شب در گفتگو</p></div>
<div class="m2"><p>سید سرمست ما دائم به گفتگوی تو</p></div></div>