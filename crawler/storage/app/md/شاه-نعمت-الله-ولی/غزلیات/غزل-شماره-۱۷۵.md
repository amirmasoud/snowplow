---
title: >-
    غزل شمارهٔ ۱۷۵
---
# غزل شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>عشق او سلطان ملک جان ماست</p></div>
<div class="m2"><p>اینچنین ملک و چنین سلطان کراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پادشاه هفت اقلیم ای عزیز</p></div>
<div class="m2"><p>نزد این سلطان درویشان گداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با وجود او کرا باشد وجود</p></div>
<div class="m2"><p>ور تو گوئی هست آن عین خطاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رند سر مستیم و با ساقی حریف</p></div>
<div class="m2"><p>همچو ما رندی در این عالم نخاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دُرد درد عشق او نوشیده ایم</p></div>
<div class="m2"><p>دُرد درد عشق او ما را دواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجلس عشقشت و ما سرمست او</p></div>
<div class="m2"><p>شاهد میخانه در فرمان ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله در همه عالم یکیست</p></div>
<div class="m2"><p>لاجرم او سید هر دو سراست</p></div></div>