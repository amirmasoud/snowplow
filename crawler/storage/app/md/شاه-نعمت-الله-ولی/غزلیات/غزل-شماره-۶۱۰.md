---
title: >-
    غزل شمارهٔ ۶۱۰
---
# غزل شمارهٔ ۶۱۰

<div class="b" id="bn1"><div class="m1"><p>توحید و موحد و موحد</p></div>
<div class="m2"><p>این جمله طلب کنش را حمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک فاعل و فعل او یکی هم</p></div>
<div class="m2"><p>گه نیک نماید و گهی بد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خمخانه و جام و ساقی ما</p></div>
<div class="m2"><p>می جوی ولی ز مجلس خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که عقل ذوفنون است</p></div>
<div class="m2"><p>اما بر عاشقان چه سنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر دو جهان یکیست موجود</p></div>
<div class="m2"><p>هر لحظه به صورتی مجدد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک حرف و معانی فراوان</p></div>
<div class="m2"><p>یک نقطه و اعتبار بی حد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دریاب به ذوق قول سید</p></div>
<div class="m2"><p>ای سائل کامل سرآمد</p></div></div>