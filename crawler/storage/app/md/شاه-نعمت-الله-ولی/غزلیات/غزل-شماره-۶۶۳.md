---
title: >-
    غزل شمارهٔ ۶۶۳
---
# غزل شمارهٔ ۶۶۳

<div class="b" id="bn1"><div class="m1"><p>جان مجنون فدای لیلی بود</p></div>
<div class="m2"><p>در دل او هوای لیلی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاطر دل شکستهٔ مجنون</p></div>
<div class="m2"><p>مبتلای بلای لیلی بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق لیلی نبود بی مجنون</p></div>
<div class="m2"><p>بود مجنون برای لیلی بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق و رند و مست و لایعقل</p></div>
<div class="m2"><p>روز و شب در قفای لیلی بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر خیالی که نقش می بستی</p></div>
<div class="m2"><p>نظرش بر لقای لیلی بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راحت جان خستهٔ مجنون</p></div>
<div class="m2"><p>از جفا و وفای لیلی بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جان سید فدای مجنون باد</p></div>
<div class="m2"><p>زانکه مجنون فدای لیلی بود</p></div></div>