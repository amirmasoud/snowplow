---
title: >-
    غزل شمارهٔ ۲۲۹
---
# غزل شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>نور او در جمله اشیاء ظاهر است</p></div>
<div class="m2"><p>ظاهرش بنگر که بر ما ظاهر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشنست آئینهٔ عالم تمام</p></div>
<div class="m2"><p>در همه اسما مسما ظاهر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور روی اوست ما را در نظر</p></div>
<div class="m2"><p>نور آن منظور زیبا ظاهر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باطنت از چشم نابینا ولی</p></div>
<div class="m2"><p>ظاهرا بر چشم بینا ظاهر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خیال دی و فردا مانده ای</p></div>
<div class="m2"><p>از همه فرد آنکه فردا ظاهر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما ز دریائیم و دریا عین ما</p></div>
<div class="m2"><p>عین ما در عین دریا ظاهر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمة الله ظاهر و باطن بود</p></div>
<div class="m2"><p>باطنش پنهان و پیدا ظاهر است</p></div></div>