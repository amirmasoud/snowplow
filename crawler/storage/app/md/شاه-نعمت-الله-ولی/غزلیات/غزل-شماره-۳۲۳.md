---
title: >-
    غزل شمارهٔ ۳۲۳
---
# غزل شمارهٔ ۳۲۳

<div class="b" id="bn1"><div class="m1"><p>در نظر آنکه نور چشم من است</p></div>
<div class="m2"><p>یوسف نازنین و پیرهن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم تن است و او جان است</p></div>
<div class="m2"><p>روشنست آفتاب و مه بدن است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم مستی نموده کاین عین است</p></div>
<div class="m2"><p>سر میمی گشوده کاین دهن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون یکی در یکی یکی باشد</p></div>
<div class="m2"><p>گر بگویم هزار یک سخن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر از نیست ور تو گوئی هست</p></div>
<div class="m2"><p>همه نقش خیال مرد و زن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما تخت گاه سلطان است</p></div>
<div class="m2"><p>عشق او پادشاه انجمن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمة الله بود ز آل حسین</p></div>
<div class="m2"><p>در همه جا چو بوالحسن حسن است</p></div></div>