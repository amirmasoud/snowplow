---
title: >-
    غزل شمارهٔ ۱۷۷
---
# غزل شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>عاشق رندی که او همدرد ماست</p></div>
<div class="m2"><p>جام دُرد درد او ما را دواست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که او از خویش بیگانه بود</p></div>
<div class="m2"><p>گو بیا اینجا که با ما آشناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی مستیم و جام می به دست</p></div>
<div class="m2"><p>می پرست رند سرمستی کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج بحر ماست دریای محیط</p></div>
<div class="m2"><p>حوض کوثر جرعه ای از جام ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نالهٔ نی بشنو ای جان عزیز</p></div>
<div class="m2"><p>بینوایان را نوائی بی نواست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات فنا دارم مقام</p></div>
<div class="m2"><p>خوش مقامی این سر دار بقاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقان در عشق گر کشته شوند</p></div>
<div class="m2"><p>نعمت الله کشتگان را خونبهاست</p></div></div>