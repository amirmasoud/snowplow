---
title: >-
    غزل شمارهٔ ۸۳۸
---
# غزل شمارهٔ ۸۳۸

<div class="b" id="bn1"><div class="m1"><p>عشق او ما را به کام است ای پسر</p></div>
<div class="m2"><p>دل که باشد جان کدام است ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقی در عشق اگر جان را نداد</p></div>
<div class="m2"><p>نزد کامل ناتمام است ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس عشق است و ما مست و خراب</p></div>
<div class="m2"><p>عمر ما بی او حرام است ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش حبابی پر کن از آب حیات</p></div>
<div class="m2"><p>کو شراب ما و جام است ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همدم جامیم و با ساقی حریف</p></div>
<div class="m2"><p>عقل را اینجا چه نام است ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قرض بگذار و خوشی آسوده شو</p></div>
<div class="m2"><p>هر چه داری جمله وامست ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ جانی عبدالله ما</p></div>
<div class="m2"><p>حضرت عبدالسلام است ای پسر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سید ما بندهٔ جانی اوست</p></div>
<div class="m2"><p>پیش او سلطان غلام است ای پسر</p></div></div>