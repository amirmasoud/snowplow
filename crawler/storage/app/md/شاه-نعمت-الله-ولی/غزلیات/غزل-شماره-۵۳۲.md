---
title: >-
    غزل شمارهٔ ۵۳۲
---
# غزل شمارهٔ ۵۳۲

<div class="b" id="bn1"><div class="m1"><p>چشم ما نقش خیال او بر آتش می کشد</p></div>
<div class="m2"><p>نور دیده پیش مردم بی حسابش می کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زآفتاب حسن او ذرات عالم روشن است</p></div>
<div class="m2"><p>لاجرم ذرات عالم آفتابش می کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر زاهد به جنت گر کشد گو خوش بود</p></div>
<div class="m2"><p>جان ما جانانه مست خرابش می کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم ما در خواب اگر بیند خیال روی او</p></div>
<div class="m2"><p>خویشتن را پیشکش حالی به خوابش می کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همدم جام مئیم و محرم ساقی مدام</p></div>
<div class="m2"><p>همت عالی ما جام شرابش می کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هوایش آب چشم ما به هر سو رو نهاد</p></div>
<div class="m2"><p>دیدهٔ تر دامنش دامن در آبش می کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله درکش خود گر کشد یار خوشی</p></div>
<div class="m2"><p>گو برو با او که در راه صوابش می کشد</p></div></div>