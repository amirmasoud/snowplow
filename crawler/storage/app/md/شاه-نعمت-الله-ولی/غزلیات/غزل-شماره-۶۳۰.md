---
title: >-
    غزل شمارهٔ ۶۳۰
---
# غزل شمارهٔ ۶۳۰

<div class="b" id="bn1"><div class="m1"><p>هر که او عاشق است جان دارد</p></div>
<div class="m2"><p>جان فدایش کنم که آن دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقان نور چشم خوانندش</p></div>
<div class="m2"><p>عاشق ار عشق عاشقان دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نشانی ز بی نشان داریم</p></div>
<div class="m2"><p>خوش نشانی که آن نشان دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می و جام است جسم و جان با هم</p></div>
<div class="m2"><p>هر چه بینی همین همان دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که با ما نشست در دریا</p></div>
<div class="m2"><p>خبر از بحر بیکران دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواجه علم بدیع می خواند</p></div>
<div class="m2"><p>آن معانی ازین بیان دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می مست خوشی اگر جوئی</p></div>
<div class="m2"><p>نعمت الله بجو که آن دارد</p></div></div>