---
title: >-
    غزل شمارهٔ ۱۳۸۳
---
# غزل شمارهٔ ۱۳۸۳

<div class="b" id="bn1"><div class="m1"><p>عشق او خوش آتشی افروخته</p></div>
<div class="m2"><p>غیرت او غیر او را سوخته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقبازی کار آتش بازی است</p></div>
<div class="m2"><p>او چنین کاری به ما آموخته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج او در کنج دل ما یافتیم</p></div>
<div class="m2"><p>دل فراوان نقد از او اندوخته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور ما روشنتر است از آفتاب</p></div>
<div class="m2"><p>گوئیا از نار عشق افروخته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سید ما تا جمالش دیده است</p></div>
<div class="m2"><p>دیده را از این و آن بردوخته</p></div></div>