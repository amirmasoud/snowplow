---
title: >-
    غزل شمارهٔ ۱۸۸
---
# غزل شمارهٔ ۱۸۸

<div class="b" id="bn1"><div class="m1"><p>عقل گرچه رئیس این دل ماست</p></div>
<div class="m2"><p>عشق شاه است و این رئیس گداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بر تخت دل نشسته به ذوق</p></div>
<div class="m2"><p>این چنین پادشاه و تخت کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم و جان هرچه هست آن ویست</p></div>
<div class="m2"><p>ملک الملک و مالک دو سراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر و موج و حباب و جو آبند</p></div>
<div class="m2"><p>لاجرم هر چه باشد آن از ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر کوی او کسی بنشست</p></div>
<div class="m2"><p>که چو ما از سر همه برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابست و ماه خوانندش</p></div>
<div class="m2"><p>نور چشمست و در نظر پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق بالاش در بلام انداخت</p></div>
<div class="m2"><p>خوش بلائی بود کزان بالاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که سودای زلف او دارد</p></div>
<div class="m2"><p>سر او هم چو دیگ پر سوداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله برای اهل دلان</p></div>
<div class="m2"><p>مجلس عاشقانه ای آراست</p></div></div>