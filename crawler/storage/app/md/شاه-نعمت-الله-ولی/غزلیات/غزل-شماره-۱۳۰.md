---
title: >-
    غزل شمارهٔ ۱۳۰
---
# غزل شمارهٔ ۱۳۰

<div class="b" id="bn1"><div class="m1"><p>در محبت جان اگر بازی خوش است</p></div>
<div class="m2"><p>گر کنی بازی چنین بازی خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار کرمانی اگر چه خوش بود</p></div>
<div class="m2"><p>دلبر سر‌مست شیرازی خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رند سرمستیم و با ساقی حریف</p></div>
<div class="m2"><p>با حریف خویش دمسازی خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چند گردی تو به خود گرد جهان</p></div>
<div class="m2"><p>یک دمی با خویش‌ پردازی خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساز ما ار ذوق خوشتر می‌ دهد</p></div>
<div class="m2"><p>ساز ما را گر تو بنوازی خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق چون سلطان به تخت دل نشست</p></div>
<div class="m2"><p>خانه را با عشق پردازی خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیم قلب تو ندارد رونقی</p></div>
<div class="m2"><p>سیم قلب خویش بگدازی خوش است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در طریق عاشقی چون عاشقان</p></div>
<div class="m2"><p>هر چه داری جمله دربازی خوش است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یک دمی با سید رندان بساز</p></div>
<div class="m2"><p>تا بدانی ذوق دمسازی خوش است</p></div></div>