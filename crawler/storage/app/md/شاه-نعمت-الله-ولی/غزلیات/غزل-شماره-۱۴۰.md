---
title: >-
    غزل شمارهٔ ۱۴۰
---
# غزل شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>دیده تا نور جمالش دیده است</p></div>
<div class="m2"><p>در نظر ما را چو نور دیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم مردم روشن است از نور او</p></div>
<div class="m2"><p>خوش بود چشمی که او را دیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ما مست و جا م می به دست</p></div>
<div class="m2"><p>گرد رندان یک به یک گردیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل سرمست می‌ نالد به ذوق</p></div>
<div class="m2"><p>تا گلی از گلستانش چیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق و معشوق عشق است ای عزیز</p></div>
<div class="m2"><p>هر که سر از غیر او پیچیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در نظر مائیم بحر بیکران</p></div>
<div class="m2"><p>ما به ما این دیدهٔ ما دیده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتهٔ مستانهٔ سید شنو</p></div>
<div class="m2"><p>این چنین قولی کسی نشنیده است</p></div></div>