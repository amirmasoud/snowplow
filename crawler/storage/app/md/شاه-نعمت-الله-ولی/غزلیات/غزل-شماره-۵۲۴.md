---
title: >-
    غزل شمارهٔ ۵۲۴
---
# غزل شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>هرکه باشد بندهٔ او درجهان سلطان شود</p></div>
<div class="m2"><p>خوش بود جانی که مقبول چنان جانان شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی او در دیدهٔ ما آفتاب روشن است</p></div>
<div class="m2"><p>این چنین نوری کجا از چشم ما پنهان شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه آید در نظر نقش خیال او بود</p></div>
<div class="m2"><p>لاجرم در حسن خوبان عقل ما حیران شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز دریائیم و با ما هر که بنشیند دمی</p></div>
<div class="m2"><p>گر چه باشد قطره ای در بحر ما عمان شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل حل است و حل مشکلات عالمست</p></div>
<div class="m2"><p>حل این مشکل تو را در مجلس رندان شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گنج معنی هر که می خواهد بیاید همچو ما</p></div>
<div class="m2"><p>عارفانه ساکن کنج دل ویران شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله حاصل عمر حیاتست ای عزیز</p></div>
<div class="m2"><p>خوش بود گر حاصل عمر عزیزت آن شود</p></div></div>