---
title: >-
    غزل شمارهٔ ۸۴۱
---
# غزل شمارهٔ ۸۴۱

<div class="b" id="bn1"><div class="m1"><p>نام آن لعل شکر بار مبر</p></div>
<div class="m2"><p>وز لبش قند به خروار مبر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جمالش سخن از ماه مگو</p></div>
<div class="m2"><p>زینت ماه به یک بار مبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرمه در نرگس مخمور مکش</p></div>
<div class="m2"><p>دردسر بر سر بیمار مبر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنبلت بر ورق گل مفشان</p></div>
<div class="m2"><p>رونق کلبهٔ عطار مبر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزد ما جز خبر باده میار</p></div>
<div class="m2"><p>نام ما جز بر خمار مبر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتشی در من دلسوز مزن</p></div>
<div class="m2"><p>سر یاران بر اغیار مبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قیمت گوهر سید مشکن</p></div>
<div class="m2"><p>سخنش بر سر بازار مبر</p></div></div>