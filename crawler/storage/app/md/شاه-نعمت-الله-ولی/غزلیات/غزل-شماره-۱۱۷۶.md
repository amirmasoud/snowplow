---
title: >-
    غزل شمارهٔ ۱۱۷۶
---
# غزل شمارهٔ ۱۱۷۶

<div class="b" id="bn1"><div class="m1"><p>تا به نور روی خوب او جمالش دیده ایم</p></div>
<div class="m2"><p>همچو دیده گرد عالم سر به سر گردیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بهشت جاودان گشتیم با یاران بسی</p></div>
<div class="m2"><p>عارفانه میوه ها از هر درختی چیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه آمد در نظر آورد از آن حضرت خبر</p></div>
<div class="m2"><p>لاجرم از یک به یک نیکو خبر پرسیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات مغان مستیم و با رندان حریف</p></div>
<div class="m2"><p>جام می شادی روی عاشقان نوشیده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به تخت نیستی خوش در عدم بنشسته ایم</p></div>
<div class="m2"><p>فرش هستی سر به سر بر همدگر پیچیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیگران از خود سخن گفتند ما گوئیم از او</p></div>
<div class="m2"><p>این چنین قول خوشی از دیگران نشنیده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله در همه آئینهٔ روشن نمود</p></div>
<div class="m2"><p>آن چنان نور خوشی روشن به نورش دیده ایم</p></div></div>