---
title: >-
    غزل شمارهٔ ۵۶۸
---
# غزل شمارهٔ ۵۶۸

<div class="b" id="bn1"><div class="m1"><p>ناز با یار غار خوش باشد</p></div>
<div class="m2"><p>آن میان در کنار خوش باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش رویش خیال می بندم</p></div>
<div class="m2"><p>در نظر آن نگار خوش باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق او آفتاب تابان است</p></div>
<div class="m2"><p>مهر او بی غبار خوش باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور او را به نور او بنگر</p></div>
<div class="m2"><p>آن نهان آشکار خوش باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیس فی الدار غیره دیار</p></div>
<div class="m2"><p>در چنین دار یار خوش باشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در همه چون جمال او پیداست</p></div>
<div class="m2"><p>گر یکی ور هزار خوش باشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل مست و صحبت سید</p></div>
<div class="m2"><p>بابت گلعذار خوش باشد</p></div></div>