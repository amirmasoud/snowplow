---
title: >-
    غزل شمارهٔ ۷۱۷
---
# غزل شمارهٔ ۷۱۷

<div class="b" id="bn1"><div class="m1"><p>رخت ما را به سراپردهٔ میخانه برید</p></div>
<div class="m2"><p>آلت مجلس ما جمله به ساقی سپرید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چو غنچه به هوا جامهٔ خود جا کردیم</p></div>
<div class="m2"><p>بعد از این خرقهٔ ما را به ملامت ندرید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عیب ما را مکنید ار شده ایم عاشق او</p></div>
<div class="m2"><p>نور چشمست ببینید که صاحب نظرید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر ز ما از سر مستی سخنی گوش کنید</p></div>
<div class="m2"><p>از سر لطف و کرم از سر آن درگذرید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا نقش خیالی که ببیند دیده</p></div>
<div class="m2"><p>معنی خوب در آن صورت زیبا نگرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میل میخانه ندارید ندانیم چرا</p></div>
<div class="m2"><p>مگر از ذوق می و مستی ما بی خبرید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید رندان خرابات شوید</p></div>
<div class="m2"><p>که به نزدیک سلاطین جهان معتبرید</p></div></div>