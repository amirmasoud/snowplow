---
title: >-
    غزل شمارهٔ ۱۰۳۷
---
# غزل شمارهٔ ۱۰۳۷

<div class="b" id="bn1"><div class="m1"><p>به هرحالی که پیش آید خیالی نقش می بندم</p></div>
<div class="m2"><p>از آن رو چون گل خندان به رویش باز می خندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سرمستان به میخانه دگرباره درافتادم</p></div>
<div class="m2"><p>حجاب رند رندانه ز پیش خود بر افکندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گسستم از همه عالم به اصل خویش پیوستم</p></div>
<div class="m2"><p>به اصل خود چو پیوندی بدانی اصل پیوندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مکن دعوت مرا شاها به شیراز و به اصفاهان</p></div>
<div class="m2"><p>که دارم با هری میلی و جویای سمرقندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه انسیم نه جنیم نه عرشیم نه فرشیم</p></div>
<div class="m2"><p>نه از بلغار و نه از چین مگر از شهر ارکندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو غیر او نمی یابم به غیری دل کجا بندم</p></div>
<div class="m2"><p>گهی بر تخت مالگدار و گه در کوه الوندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراباتست و رندان مست و سید ساقی مجلس</p></div>
<div class="m2"><p>حریف نعمت اللهم نه من در بند دربندم</p></div></div>