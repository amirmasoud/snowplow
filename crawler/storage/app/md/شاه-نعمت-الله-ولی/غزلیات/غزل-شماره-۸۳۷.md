---
title: >-
    غزل شمارهٔ ۸۳۷
---
# غزل شمارهٔ ۸۳۷

<div class="b" id="bn1"><div class="m1"><p>مه نقاب آفتاب است ای پسر</p></div>
<div class="m2"><p>آفتاب مه نقال است ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب چنین باشد ولی چون روز شد</p></div>
<div class="m2"><p>روشن است و آفتابست ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می نماید عالمی در چشم ما</p></div>
<div class="m2"><p>چون حبابی پر ز آبست ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی ما کرد میخانه سبیل</p></div>
<div class="m2"><p>لطف ساقی بی حسابست ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>میر مستانیم و با ساقی حریف</p></div>
<div class="m2"><p>این سعادت زان جنابست ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بخواهی هفت هیکل نزد ما</p></div>
<div class="m2"><p>حرفی از ام الکتاب است ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله در خرابات مغان</p></div>
<div class="m2"><p>عاشق و مست و خرابست ای پسر</p></div></div>