---
title: >-
    غزل شمارهٔ ۶۹۶
---
# غزل شمارهٔ ۶۹۶

<div class="b" id="bn1"><div class="m1"><p>گر در طلب اوئی ناگه به برت آید</p></div>
<div class="m2"><p>ور گرد درش گردی او در به تو بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر آینهٔ روشن اندر نظری آری</p></div>
<div class="m2"><p>تمثال جمال او در آینه بنماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن به که تو عمر خود در عشق کنی صرفش</p></div>
<div class="m2"><p>چون عمر عزیز تو پیوسته نمی پاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای عقل تو مخموری ، ما عاشق سرمستیم</p></div>
<div class="m2"><p>در مجلس سرمستان وعظ تو نمی یابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر چه نظر کردم چون اوست که می بینم</p></div>
<div class="m2"><p>اقرار به او دارم انکار نمی شاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا نور جمال او در دیدهٔ ما بنمود</p></div>
<div class="m2"><p>نوری به جز آن نورش در چشم نمی آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتار خوش سید هر کس که بخواند خوش</p></div>
<div class="m2"><p>آن بزم ملوکانه مستانه بیاراید</p></div></div>