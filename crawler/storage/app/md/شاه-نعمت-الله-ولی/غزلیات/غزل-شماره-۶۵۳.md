---
title: >-
    غزل شمارهٔ ۶۵۳
---
# غزل شمارهٔ ۶۵۳

<div class="b" id="bn1"><div class="m1"><p>این که گوئی نعمت الله جان سپرد</p></div>
<div class="m2"><p>جان سپرد و جان با ایمان سپرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان به جانان دل به دلبر داد و رفت</p></div>
<div class="m2"><p>جان از این خوشتر دگر نتوان سپرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوای گلستان عشق او</p></div>
<div class="m2"><p>جان چو غنچه با لب خندان سپرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بندگی کرد او به صدق دل تمام</p></div>
<div class="m2"><p>ظاهر و باطن به آن سلطان سپرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود میخانه سبیل خدمتش</p></div>
<div class="m2"><p>رفت و آن منصب به این و آن سپرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان امانت بود با وی مدتی</p></div>
<div class="m2"><p>خوش امینانه به آن جانان سپرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگری گر جان به دشواری بداد</p></div>
<div class="m2"><p>سید سرمست ما آسان سپرد</p></div></div>