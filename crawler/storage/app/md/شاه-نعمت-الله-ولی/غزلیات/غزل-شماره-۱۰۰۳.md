---
title: >-
    غزل شمارهٔ ۱۰۰۳
---
# غزل شمارهٔ ۱۰۰۳

<div class="b" id="bn1"><div class="m1"><p>اگر ذوق خوشی خواهی حریفی کن دمی بادل</p></div>
<div class="m2"><p>و گر جانانه می جوئی فدا کن جان خود با دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو چون پروانه ای عقل و ما چون شمع و عشق آتش</p></div>
<div class="m2"><p>تو را دامن همی سوزد به عشق او و ما را دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بحر است و جان گوهر تنم کشتی و من ملاح</p></div>
<div class="m2"><p>زهی گوهر زهی کشتی زهی ملاح دریا دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خراباتست و رندان مست و ساقی جام می بر دست</p></div>
<div class="m2"><p>بهای جرعهٔ صدجان چه قدرش هست اینجا دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به امیدی که در غربت به کام دل رسم روزی</p></div>
<div class="m2"><p>غریبی می کشم دائم ندارد میل مأوا دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه وصل او باشد نباشد جان ما را ذوق</p></div>
<div class="m2"><p>و گر نه عشق او بودی نبودی هیچ با ما دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف نعمت اللهم که میر می پرستانست</p></div>
<div class="m2"><p>چه خوش رندی که از ذوقش شود سرمست جان دل</p></div></div>