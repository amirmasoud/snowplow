---
title: >-
    غزل شمارهٔ ۵۸۱
---
# غزل شمارهٔ ۵۸۱

<div class="b" id="bn1"><div class="m1"><p>چه خوش چشمی که نور او به نور روی او بیند</p></div>
<div class="m2"><p>چو نور روی او باشد همه چیزی نکو بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی کو را به خود بیند کجا من عارفش خوانم</p></div>
<div class="m2"><p>من آن کس عارفش خوانم که نور او به او بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود این رشتهٔ یک تو ولی احول دو تو یابد</p></div>
<div class="m2"><p>چو گم کردست سر رشته از آن یک تو دو تو بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی کو مست شد از می چه داند جام و پیمانه</p></div>
<div class="m2"><p>مگر رندی بود سرخوش که می نوشد سبو بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر آئینهٔ روشن محبی در نظر آرد</p></div>
<div class="m2"><p>خود و محبوب در یک جا نشسته روبرو بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نبیند چشم دریا بین به غیر از عین ما دیگر</p></div>
<div class="m2"><p>اگر سرچمه ای یابد و گر در آب جو بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیالی گر پزد شخصی که سید غیر او دیده</p></div>
<div class="m2"><p>بگو چون نیست غیر او نگوئی غیر چو بیند</p></div></div>