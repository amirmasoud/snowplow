---
title: >-
    غزل شمارهٔ ۷۲۱
---
# غزل شمارهٔ ۷۲۱

<div class="b" id="bn1"><div class="m1"><p>آفتاب چرخ معنی بایزید</p></div>
<div class="m2"><p>سایهٔ خورشید اعلی بایزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>واقف اسرار سبحانی به حق</p></div>
<div class="m2"><p>کاشف انوار معنی بایزید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر دریای عرفان از یقین</p></div>
<div class="m2"><p>عارف و معروف یعنی بایزید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه جان روشن نشد بی بوالحسن</p></div>
<div class="m2"><p>کار دل پیدا نشد بی بایزید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقطهٔ وحدت درآمد در الف</p></div>
<div class="m2"><p>در ظهور حرف شد بی بایزید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت فردوس جان بسطام عشق</p></div>
<div class="m2"><p>میوهٔ معنی طوبی بایزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید از صاحبدلانی لاجرم</p></div>
<div class="m2"><p>کرده با جانت تجلی بایزید</p></div></div>