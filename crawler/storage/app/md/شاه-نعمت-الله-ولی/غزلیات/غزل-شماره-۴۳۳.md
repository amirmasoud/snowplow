---
title: >-
    غزل شمارهٔ ۴۳۳
---
# غزل شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>عقل مشوش دماغ از سر ما رفت رفت</p></div>
<div class="m2"><p>عشق درآمد ز در عقل ز جا رفت رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقش خیالی نگاشت هیچ حقیقت نداشت</p></div>
<div class="m2"><p>بود هوا در سرش هم به هوا رفت رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمر به باد هوا داد در این گفتگو</p></div>
<div class="m2"><p>میل صوابی نکرد راه خطا رفت رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق مستی رسید عربده آغاز کرد</p></div>
<div class="m2"><p>عاقل مخمور از آن از بر ما رفت رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه به دریا فتاد نام و نشانش مجو</p></div>
<div class="m2"><p>بشنو و دیگر مگو خواجه چرا رفت رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام حبابی پر آب گر شکند صورتش</p></div>
<div class="m2"><p>معنی او آب بود آب کجا رفت رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید هر دو سرا آمده بود از خدا</p></div>
<div class="m2"><p>باز به حکم خدا نزد خدا رفت رفت</p></div></div>