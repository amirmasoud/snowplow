---
title: >-
    غزل شمارهٔ ۱۰۵
---
# غزل شمارهٔ ۱۰۵

<div class="b" id="bn1"><div class="m1"><p>دیده ام مهر منیر مه نقاب</p></div>
<div class="m2"><p>ذره ای از نور رویش آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی از می پر ز می داریم ما</p></div>
<div class="m2"><p>نوش کن جام شرابی از شراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما در این دریا به هر سو می رویم</p></div>
<div class="m2"><p>ساغری داریم پر آب از حباب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج و دریا و حباب و قطره هم</p></div>
<div class="m2"><p>چار اسم و یک حقیقت عین آب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ما روشن به نور روی اوست</p></div>
<div class="m2"><p>لاجرم بینیم رویش بی حجاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دمی نقش خیالی می کشد</p></div>
<div class="m2"><p>گه به بیداری بُود گاهی به خواب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله یافتم از لطف او</p></div>
<div class="m2"><p>بی خطا والله اعلم بالصواب</p></div></div>