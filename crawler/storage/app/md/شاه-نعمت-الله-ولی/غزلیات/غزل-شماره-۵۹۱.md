---
title: >-
    غزل شمارهٔ ۵۹۱
---
# غزل شمارهٔ ۵۹۱

<div class="b" id="bn1"><div class="m1"><p>دل عاشق نظر به جان نکند</p></div>
<div class="m2"><p>خاطرش میل با جنان نکند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای که گوئی که ترک رندی کن</p></div>
<div class="m2"><p>رند سرمست آنچنان نکند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دنیی و آخرت مده که دلم</p></div>
<div class="m2"><p>التفاتی به این و آن نکند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رند مستیم نام ما که برد</p></div>
<div class="m2"><p>بینشان را کسی نشان نکند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرعهٔ می به جان خرید دلم</p></div>
<div class="m2"><p>کرد سودائی و زیان نکند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق و رند مست او باشیم</p></div>
<div class="m2"><p>عاشق انکار عاشقان نکند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله حریف و می در جام</p></div>
<div class="m2"><p>هیچکس توبه این زمان نکند</p></div></div>