---
title: >-
    غزل شمارهٔ ۷۳۲
---
# غزل شمارهٔ ۷۳۲

<div class="b" id="bn1"><div class="m1"><p>هر که او در عشق جانان جان نداد</p></div>
<div class="m2"><p>بوسهٔ خوش بر لب جانان نداد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جود او بخشید عالم را وجود</p></div>
<div class="m2"><p>آشکارا داد او پنهان نداد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام می در دست و ساقی در نظر</p></div>
<div class="m2"><p>فکر این و آن به آن رندان نداد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون که مخموری بود دردسری</p></div>
<div class="m2"><p>دردسر ساقی به سرمستان نداد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لایق هر کس عطا او می دهد</p></div>
<div class="m2"><p>ذوق سرمستان به میخواران نداد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس گران و هم سبک سر بود عقل</p></div>
<div class="m2"><p>جان به عشق او از آن آسان نداد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را به ما داد از کرم</p></div>
<div class="m2"><p>این چنین دادی به هر سلطان نداد</p></div></div>