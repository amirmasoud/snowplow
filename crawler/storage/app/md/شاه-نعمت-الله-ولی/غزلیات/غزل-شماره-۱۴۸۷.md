---
title: >-
    غزل شمارهٔ ۱۴۸۷
---
# غزل شمارهٔ ۱۴۸۷

<div class="b" id="bn1"><div class="m1"><p>بی درد دلی دوا نیابی</p></div>
<div class="m2"><p>بی رنج تنی شفا نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عین فنا بقا توان یافت</p></div>
<div class="m2"><p>ناگشته فنا بقا نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا ترک خودی خود نگوئی</p></div>
<div class="m2"><p>چون ما به خدا خدا نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق شو و عقل را رها کن</p></div>
<div class="m2"><p>کز عقل دنی وفا نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانه مشو که در خرابات</p></div>
<div class="m2"><p>رندی چو من آشنا نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز بر در بارگاه وحدت</p></div>
<div class="m2"><p>ای یار مجو مرا نیابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی خوشی چو نعمت الله</p></div>
<div class="m2"><p>در میکده حالیا نیایی</p></div></div>