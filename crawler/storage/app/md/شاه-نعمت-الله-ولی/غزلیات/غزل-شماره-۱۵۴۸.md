---
title: >-
    غزل شمارهٔ ۱۵۴۸
---
# غزل شمارهٔ ۱۵۴۸

<div class="b" id="bn1"><div class="m1"><p>گفتهٔ عشاق می خوانم بلی</p></div>
<div class="m2"><p>عشقبازی نیک می دانم بلی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام آئینهٔ گیتی نما</p></div>
<div class="m2"><p>بر جمال خویش حیرانم بلی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسته ام زُنار کفر زلف او</p></div>
<div class="m2"><p>لاجرم نیکو مسلمانم بلی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردمندم دردمندم دردمند</p></div>
<div class="m2"><p>دُردی درد است درمانم بلی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه به این و گه به آن خوانی مرا</p></div>
<div class="m2"><p>هرچه می خوانی بخوان آنم بلی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر هر دو جهان برخواستم</p></div>
<div class="m2"><p>همنشین جان و جانانم بلی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درخرابات مغان مست و خراب</p></div>
<div class="m2"><p>سیدم مجموع رندانم بلی</p></div></div>