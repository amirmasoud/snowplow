---
title: >-
    غزل شمارهٔ ۷۳۶
---
# غزل شمارهٔ ۷۳۶

<div class="b" id="bn1"><div class="m1"><p>عشق او با جان و دل پیوسته باد</p></div>
<div class="m2"><p>دولت عشقش مرا پیوسته باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل اگر منعم کند از عشق او</p></div>
<div class="m2"><p>خاطرش چون خاطر من خسته باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدم من باد جام می مدام</p></div>
<div class="m2"><p>با لب ساقی لبم پیوسته باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلوت عشقست و رندان در حضور</p></div>
<div class="m2"><p>در به غیر عاشقان بر بسته باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقی سرمست بشکست توبه ام</p></div>
<div class="m2"><p>پشت توبه دائما بشکسته باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ جان من ز دام عقل رست</p></div>
<div class="m2"><p>هر که در دام است یا رب رسته باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابات مغان بنشسته ام</p></div>
<div class="m2"><p>سیدم دائم چنین بنشسته باد</p></div></div>