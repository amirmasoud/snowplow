---
title: >-
    غزل شمارهٔ ۶۵۹
---
# غزل شمارهٔ ۶۵۹

<div class="b" id="bn1"><div class="m1"><p>یک دم بی می نمی توان بود</p></div>
<div class="m2"><p>بی می خود حی نمی توان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی عشق دمی نمی توان زیست</p></div>
<div class="m2"><p>بی ساغر می نمی توان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما سایه و عشق یار خورشید</p></div>
<div class="m2"><p>بی بودن وی نمی توان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جام شراب و عشق لیلی</p></div>
<div class="m2"><p>مجنون در حی نمی توان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستیم و خراب و لاابالی</p></div>
<div class="m2"><p>بی نالهٔ نی نمی توان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا کی غم این و آن توان خورد</p></div>
<div class="m2"><p>در ماندهٔ کی نمی توان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بی بود وجود نعمت الله</p></div>
<div class="m2"><p>والله که شی نمی توان بود</p></div></div>