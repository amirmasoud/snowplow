---
title: >-
    غزل شمارهٔ ۹۹۵
---
# غزل شمارهٔ ۹۹۵

<div class="b" id="bn1"><div class="m1"><p>آفتابی می پرستم لایزال</p></div>
<div class="m2"><p>مهر من هرگز نمی گیرد زوال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده در آئینهٔ گیتی نما</p></div>
<div class="m2"><p>دیده تمثال جمال بی مثال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه ذره می نماید آفتاب</p></div>
<div class="m2"><p>ماه نور او نماید بر کمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک نفس با ما درین دریا درآ</p></div>
<div class="m2"><p>نو شکن گر تشنه ای آب زلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نماید حسن او هر آینه</p></div>
<div class="m2"><p>او جمیل و دوست می دارد جمال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشم مستش چشم بندی می کند</p></div>
<div class="m2"><p>می برد از چشم ما خواب و خیال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رند سرمستیم و با سید حریف</p></div>
<div class="m2"><p>عاشق و معشوق دائم در وصال</p></div></div>