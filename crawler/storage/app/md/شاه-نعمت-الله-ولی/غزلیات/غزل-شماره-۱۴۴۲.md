---
title: >-
    غزل شمارهٔ ۱۴۴۲
---
# غزل شمارهٔ ۱۴۴۲

<div class="b" id="bn1"><div class="m1"><p>سروری خواهی بیا و سر بنه</p></div>
<div class="m2"><p>سر نهادی پا از آن خوشتر بنه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش پیشانی مده دستار را</p></div>
<div class="m2"><p>مفردی دستار را بستر بنه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که گوئی جام می نوشیده ایم</p></div>
<div class="m2"><p>خم بگیر ای یار ما ساغر بنه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا کی از دفتر سخن گوئی به ما</p></div>
<div class="m2"><p>لوح محفوظش بخوان دفتر بنه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفانه نفی غیر او بکن</p></div>
<div class="m2"><p>رو قدم در راه پیغمبر بنه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نداری ذوق سرمستی ما</p></div>
<div class="m2"><p>رخت بند و بار خود بر خر بنه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر به پای سید مستان فکن</p></div>
<div class="m2"><p>این کلاه سلطنت از سر بنه</p></div></div>