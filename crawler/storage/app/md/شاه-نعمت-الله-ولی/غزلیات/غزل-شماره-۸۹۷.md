---
title: >-
    غزل شمارهٔ ۸۹۷
---
# غزل شمارهٔ ۸۹۷

<div class="b" id="bn1"><div class="m1"><p>من سودازده با عشق درافتادم باز</p></div>
<div class="m2"><p>دل به دست سر زلف صنمی دادم باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آستان در او قبلهٔ حاجات من است</p></div>
<div class="m2"><p>روی خود بر در آن میکده بنهادم باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کار رندان جهان بسته نماند دیگر</p></div>
<div class="m2"><p>چون من مست در میکده بگشادم باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خورم جام غم انجام به شادی ساقی</p></div>
<div class="m2"><p>غم ندارم ز کس و عاشق و دلشادم باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هست بنیاد من از عاشقی و میخواری</p></div>
<div class="m2"><p>رفته ام بر سر آن قصه و بنیادم باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نکنم عیب اگر توبه شکستم دیگر</p></div>
<div class="m2"><p>یافتم آب حیاتی و در افتادم باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ بندگی سید سرمستانم</p></div>
<div class="m2"><p>از چنین بندگئی بندهٔ آزادم باز</p></div></div>