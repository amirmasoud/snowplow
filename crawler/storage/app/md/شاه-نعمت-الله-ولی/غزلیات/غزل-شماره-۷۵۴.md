---
title: >-
    غزل شمارهٔ ۷۵۴
---
# غزل شمارهٔ ۷۵۴

<div class="b" id="bn1"><div class="m1"><p>واحد به صفات کثرت آمد</p></div>
<div class="m2"><p>کثرت بالذات وحدت آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیلاب محبتش روان شد</p></div>
<div class="m2"><p>عالم همه غرق رحمت آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جود وجود داد ما را</p></div>
<div class="m2"><p>منعم همه عین نعمت آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما کشتهٔ او و خونبها او</p></div>
<div class="m2"><p>قیمت چو به قدر همت آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>معشوق حریف و عشق ساقی</p></div>
<div class="m2"><p>زان مجلس ما چو جنت آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل آینه ، عشق آفتابی</p></div>
<div class="m2"><p>این آینه ماه طلعت آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید به ظهور بنده ای شد</p></div>
<div class="m2"><p>سلطان چو گدا به خدمت آمد</p></div></div>