---
title: >-
    غزل شمارهٔ ۹۰۵
---
# غزل شمارهٔ ۹۰۵

<div class="b" id="bn1"><div class="m1"><p>که را روئی چنین زیباست امروز</p></div>
<div class="m2"><p>که را لعلی روان افزاست امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بالای تو سروی در چمن نیست</p></div>
<div class="m2"><p>ز من بشنو حدیث راست امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نمی دانم چه خواهد کرد چشمت</p></div>
<div class="m2"><p>که از دستی دگر برخاست امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه روی است آن به نام ایزد که در وی</p></div>
<div class="m2"><p>نشان لطف حق پیداست امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مرا گفتار نغز دلپذیر است</p></div>
<div class="m2"><p>تو را روی جهان آراست امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نمودی روی و فردا بود وعده</p></div>
<div class="m2"><p>چه حال است این مگر فرداست امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز دست نرگس مخمور مستت</p></div>
<div class="m2"><p>جهان پر فتنه و غوغاست امروز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سودای جمالت عارف شهر</p></div>
<div class="m2"><p>چو من دیوانه و شیداست امروز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غنیمت دان حضور نعمت الله</p></div>
<div class="m2"><p>که دشمن را شب یلداست امروز</p></div></div>