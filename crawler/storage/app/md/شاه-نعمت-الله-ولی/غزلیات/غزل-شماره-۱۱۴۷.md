---
title: >-
    غزل شمارهٔ ۱۱۴۷
---
# غزل شمارهٔ ۱۱۴۷

<div class="b" id="bn1"><div class="m1"><p>ما ز می شوق او عاشق و مست آمدیم</p></div>
<div class="m2"><p>بر سر کوی مغان باده پرست آمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیشتر از این ظهور ، خورده شراب طهور</p></div>
<div class="m2"><p>ساقی ما گشته حور زان همه مست آمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون که بیامد چو جان ، دوست درآن لامکان</p></div>
<div class="m2"><p>گفت به ما این زمان بهر نشست آمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دل ما خوش شده چون که رسید این خبر</p></div>
<div class="m2"><p>چند روی در به در جام به دست آمدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون که درون دلم گشت نهان دلبرم</p></div>
<div class="m2"><p>گفت به ما این زمان دست به دست آمدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساغر و ساقی ما جمله توئی والسلام</p></div>
<div class="m2"><p>عشق نگوید تمام جمله ز هست آمدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دوست درین یک چله کرد چنین غلغله</p></div>
<div class="m2"><p>جمله در آن سلسله عشق پرست آمدیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر سحری آن نگار برد مرا نزد یار</p></div>
<div class="m2"><p>کرد مرا بی قرار نیست ز هست آمدیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سید دریا شکاف شست فکنده به بحر</p></div>
<div class="m2"><p>در طلب عشق او جمله به شست آمدیم</p></div></div>