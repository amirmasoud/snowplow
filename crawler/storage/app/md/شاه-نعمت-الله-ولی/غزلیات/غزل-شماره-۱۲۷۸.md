---
title: >-
    غزل شمارهٔ ۱۲۷۸
---
# غزل شمارهٔ ۱۲۷۸

<div class="b" id="bn1"><div class="m1"><p>جام گیتی نما ز ما بستان</p></div>
<div class="m2"><p>ساغر پر ز ما بیا بستان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دُردی درد دل دوا باشد</p></div>
<div class="m2"><p>دردمندی خوشی دوا بستان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر بلائی دهد خدا دریاب</p></div>
<div class="m2"><p>بخشش حضرت خدا بستان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون رسیدی در این سرابستان</p></div>
<div class="m2"><p>هم مرادی از این سرا بستان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر آب چشم ما بنشین</p></div>
<div class="m2"><p>آبروئی ز چشم ما بستان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به بستان گذر کنی نفسی</p></div>
<div class="m2"><p>همچو بلبل ز گل نوا بستان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله مجو ز بیگانه</p></div>
<div class="m2"><p>هر چه خواهی ز آشنا بستان</p></div></div>