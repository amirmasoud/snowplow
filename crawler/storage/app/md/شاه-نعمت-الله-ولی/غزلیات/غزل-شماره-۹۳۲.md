---
title: >-
    غزل شمارهٔ ۹۳۲
---
# غزل شمارهٔ ۹۳۲

<div class="b" id="bn1"><div class="m1"><p>زر بپاش و خواجهٔ زر پاش باش</p></div>
<div class="m2"><p>سر بنه بر پاش و خاکپاش باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زهد بگذار و به میخانه خرام</p></div>
<div class="m2"><p>در خرابات مغان قلاش باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذتی از عمر اگر خواهی برو</p></div>
<div class="m2"><p>همنشین زندگی اوباش باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز امروزت غنیمت می شمر</p></div>
<div class="m2"><p>دی گذشت آسوده از فرداش باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بیابی سید هر دو سرا</p></div>
<div class="m2"><p>ناظر آن دیدهٔ بیناش باش</p></div></div>