---
title: >-
    غزل شمارهٔ ۱۲۸۰
---
# غزل شمارهٔ ۱۲۸۰

<div class="b" id="bn1"><div class="m1"><p>وقت سرمستی است مخموری بمان</p></div>
<div class="m2"><p>نیک نزدیکی مرو دوری بمان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشنائی ترک بیگانه بگو</p></div>
<div class="m2"><p>در وصالی هجر و مهجوری بمان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرهٔ علم و عمل چندین مباش</p></div>
<div class="m2"><p>بگذر از هستی و مغروری بمان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صحبت رندان غنیمت می شمر </p></div>
<div class="m2"><p>قصهٔ رضوان مگو حوری بمان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور چشم عالمی پیدا شده</p></div>
<div class="m2"><p>روشنش می بین و مستوری بمان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیرت ار داری ز غیرش در گذر</p></div>
<div class="m2"><p>غیر او ناریست یا نوری بمان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از انا بگذر به حق می گو که حق</p></div>
<div class="m2"><p>نعمت الله باش منصوری بمان</p></div></div>