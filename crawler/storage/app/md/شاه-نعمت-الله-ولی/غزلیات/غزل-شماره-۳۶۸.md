---
title: >-
    غزل شمارهٔ ۳۶۸
---
# غزل شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>عشق را خود قرار پیدا نیست</p></div>
<div class="m2"><p>دو نفس حضرتش به یک جا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو دریا مدام در موج است</p></div>
<div class="m2"><p>این چنین بحر هیچ دریا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عین عشقیم لاجرم شب و روز</p></div>
<div class="m2"><p>صبر و آرام در دل ما نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور چشم است و در نظر پیداست</p></div>
<div class="m2"><p>دیده ای کان ندید بینا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیقراری عشق شورانگیز</p></div>
<div class="m2"><p>در غم هست و نیست گویا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق را هم ز عشق باید جست</p></div>
<div class="m2"><p>خبر از حال او جز او را نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذوق سید ز نعمت الله جو</p></div>
<div class="m2"><p>وصف او حد گفتن ما نیست</p></div></div>