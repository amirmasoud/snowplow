---
title: >-
    غزل شمارهٔ ۹۲۵
---
# غزل شمارهٔ ۹۲۵

<div class="b" id="bn1"><div class="m1"><p>جان به جانان سپار و خوش می باش</p></div>
<div class="m2"><p>دل به دلبر گذار و خوش می باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن یکی در هزار خوش می بین</p></div>
<div class="m2"><p>یک به یک می شمار و خوش می باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه با عاشقی و سرمستی</p></div>
<div class="m2"><p>فارغی از خمار و خوش می باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات عشق رندانه</p></div>
<div class="m2"><p>با می خوشگوار خوش می باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنظر می نگار نقش و نگار</p></div>
<div class="m2"><p>با خیال نگار خوش می باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقانه در آ به مجلس ما</p></div>
<div class="m2"><p>دمی با ما برآر خوش می باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام می نوش شادی سید</p></div>
<div class="m2"><p>از کسی غم مدار خوش می باش</p></div></div>