---
title: >-
    غزل شمارهٔ ۹۲۷
---
# غزل شمارهٔ ۹۲۷

<div class="b" id="bn1"><div class="m1"><p>دُرد دردش بنوش خوش می باش</p></div>
<div class="m2"><p>کسوت او بپوش خوش می باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خرابات رو خوشی بنشین</p></div>
<div class="m2"><p>همدم میفروش خوش می باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ار می دهد تو را جامی</p></div>
<div class="m2"><p>بستان و بنوش خوش می باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو خم شراب مستانه</p></div>
<div class="m2"><p>گرم شو خوش بجوش خوش می باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه میخانه گر دهد ساقی</p></div>
<div class="m2"><p>عاشقانه بنوش و خوش می باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نوش کن جام می که نوشت باد</p></div>
<div class="m2"><p>تا نیائی به هوش و خوش می باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سخن از ذوق نعمت الله گو</p></div>
<div class="m2"><p>ور نگوئی خموش و خوش می باش</p></div></div>