---
title: >-
    غزل شمارهٔ ۶۶۸
---
# غزل شمارهٔ ۶۶۸

<div class="b" id="bn1"><div class="m1"><p>صبحدم آفتاب رو بنمود</p></div>
<div class="m2"><p>زهره و مشتری چه خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه تاریک بود روشن شد</p></div>
<div class="m2"><p>نور چشمی به ما عطا فرمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی درآمد از در ما</p></div>
<div class="m2"><p>در دولت به روی ما بگشود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام گیتی نما به ما بخشید</p></div>
<div class="m2"><p>در چنین آن چنان به ما بنمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتش عشق عود جانم سوخت</p></div>
<div class="m2"><p>عود آتش شد و نماندش دود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دامن خود بگیر ای عارف</p></div>
<div class="m2"><p>تا بیابی ز خویشتن مقصود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم عشق است و سیدم سرمست</p></div>
<div class="m2"><p>هرکه آمد به مجلسش آسود</p></div></div>