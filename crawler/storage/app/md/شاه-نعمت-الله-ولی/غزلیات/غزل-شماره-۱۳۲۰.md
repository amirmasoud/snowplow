---
title: >-
    غزل شمارهٔ ۱۳۲۰
---
# غزل شمارهٔ ۱۳۲۰

<div class="b" id="bn1"><div class="m1"><p>در خرابات مغان ما را بجو</p></div>
<div class="m2"><p>رند سرمستی خوشی آنجا بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو قطره چند گردی در هوا</p></div>
<div class="m2"><p>خوش روان شو سوی ما دریا بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دو عالم را به این و آن گذار</p></div>
<div class="m2"><p>حضرت یکتای بی همتا بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش در آ در بحر بی پایان ما</p></div>
<div class="m2"><p>تشنهٔ آب حیات از ما بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا کنجیست گنجی درویست</p></div>
<div class="m2"><p>گنج او در جملهٔ اشیا بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرد جو گردی برای آبرو</p></div>
<div class="m2"><p>حاصل از دریا و جو ما را بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جو که تا یابی مراد</p></div>
<div class="m2"><p>شارح اسما طلب اسما بجو</p></div></div>