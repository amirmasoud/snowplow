---
title: >-
    غزل شمارهٔ ۱۱۰۷
---
# غزل شمارهٔ ۱۱۰۷

<div class="b" id="bn1"><div class="m1"><p>بُود ممکن که من بی جان بمانم</p></div>
<div class="m2"><p>محال است اینکه بی جانان بمانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا ساقی حریف و عشق یار است</p></div>
<div class="m2"><p>نمی خواهم که از یاران بمانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوای درد دل درد است و دارم</p></div>
<div class="m2"><p>مباد آن دم که بی درمان بمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عزیز مصر عشقم ای برادر</p></div>
<div class="m2"><p>چو یوسف چند در زندان بمانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو او پیدا شود پنهان شوم من</p></div>
<div class="m2"><p>وگر پیدا شود پنهان بمانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر نه او مرا بخشد وجودی</p></div>
<div class="m2"><p>همیشه در عدم حیران بمانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نه عشق او باشد دلیلم</p></div>
<div class="m2"><p>شوم گمراه و سرگردان بمانم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگر جانم نماند غم ندارم</p></div>
<div class="m2"><p>به جانان زنده جاویدان بمانم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نمی دانم ز غیرت غیرت ای دوست</p></div>
<div class="m2"><p>کدامست غیر تو تا آن بمانم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوم پیدا اگر پنهان شوی تو</p></div>
<div class="m2"><p>وگر پیدا شوی پنهان بمانم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چو سید بی سر و سامان بمانم</p></div>
<div class="m2"><p>اگر زلف پریشان برفشانی</p></div></div>