---
title: >-
    غزل شمارهٔ ۴۹۲
---
# غزل شمارهٔ ۴۹۲

<div class="b" id="bn1"><div class="m1"><p>حاصلم از دین و دنیا او بود</p></div>
<div class="m2"><p>این چنین خوش حاصلی نیکو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دو آئینه یکی چون رو نمود</p></div>
<div class="m2"><p>دو نماید آن یکی نی دو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صوفیانه جامه را شوئیم پاک</p></div>
<div class="m2"><p>کار ما پیوسته شست وشو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام می در دوره می گردد مدام</p></div>
<div class="m2"><p>خوش بود آن دَم که همدم او بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آینه گر چه دو رو باشد ولی</p></div>
<div class="m2"><p>در دو رویش روی او یک رو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک سر موئی نمی یابی از او</p></div>
<div class="m2"><p>تا حجاب تو سر یک مو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما از عرب پیدا شده</p></div>
<div class="m2"><p>شاه ترکستان برش هندو بود</p></div></div>