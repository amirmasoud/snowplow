---
title: >-
    غزل شمارهٔ ۳۹۶
---
# غزل شمارهٔ ۳۹۶

<div class="b" id="bn1"><div class="m1"><p>هر کجا جامی است بی می هست نیست</p></div>
<div class="m2"><p>هرچه مست آن هست بی وی هست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک جمال و صد هزاران آینه</p></div>
<div class="m2"><p>در دو عالم غیر یک شی هست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ناله نی بشنو ای جان عزیز </p></div>
<div class="m2"><p>ناله ای چون نالهٔ نی هست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتهٔ عشق است زنده جاودان</p></div>
<div class="m2"><p>زنده ای مانند این حی هست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رند سرمست ایمن است از هست و نیست</p></div>
<div class="m2"><p>جام می را نوش تا کی هست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این همه رفتند در راه خدا</p></div>
<div class="m2"><p>در چنین ره نقش یک پی هست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست همچون نعمت الله ساقئی</p></div>
<div class="m2"><p>همدمی چون ساغر می هست نیست</p></div></div>