---
title: >-
    غزل شمارهٔ ۹۱۷
---
# غزل شمارهٔ ۹۱۷

<div class="b" id="bn1"><div class="m1"><p>خوش دری بر روی ما بگشاد باز</p></div>
<div class="m2"><p>آفتابی در قمر بنمود باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام و پیمانه به ما بخشید او</p></div>
<div class="m2"><p>می به پیمانه به ما پیمود باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخزن اسرار را در باز کرد</p></div>
<div class="m2"><p>گنجها ایثار ما فرمود باز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب حسن او چون رو نمود</p></div>
<div class="m2"><p>مه ز نور روی او افزود باز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیر آمد خود بر ما زود رفت</p></div>
<div class="m2"><p>گفتمش جانا مرو نشنود باز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل شهبازیست خوش پرواز کرد</p></div>
<div class="m2"><p>در هوای عاشقی فرسود باز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را به ما انعام کرد</p></div>
<div class="m2"><p>عالمی از نعمتش آسود باز</p></div></div>