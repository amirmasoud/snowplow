---
title: >-
    غزل شمارهٔ ۳۵۴
---
# غزل شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>در دو عالم خدا یکیست یکیست</p></div>
<div class="m2"><p>مالک دو سرا یکیست یکیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در کبریای حضرت او</p></div>
<div class="m2"><p>پادشاه و گدا یکیست یکیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینه در جهان فراوان است</p></div>
<div class="m2"><p>جام گیتی نما یکیست یکیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دو مگو و دوئی به جا مگذار</p></div>
<div class="m2"><p>تو یگانه بیا یکیست یکیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج و بحر و حباب بسیارند</p></div>
<div class="m2"><p>آن همه نزد ما یکیست یکیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردمندیم و درد می نوشیم</p></div>
<div class="m2"><p>دُرد و درد و دوا یکیست یکیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله یکیست در عالم</p></div>
<div class="m2"><p>سخن آشنا یکیست یکیست</p></div></div>