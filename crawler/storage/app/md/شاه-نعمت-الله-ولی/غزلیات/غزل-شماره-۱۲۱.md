---
title: >-
    غزل شمارهٔ ۱۲۱
---
# غزل شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>در دیار تو غریبیم و هوادار غریب</p></div>
<div class="m2"><p>خوش بود گر بنوازی صنما یار غریب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مخزن جملهٔ اسرار خداوند ، دل است</p></div>
<div class="m2"><p>دل به من ده که بگویم به تو اسرار غریب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر غریبی برت آید به کرم بنوازش</p></div>
<div class="m2"><p>سخت کاریست غریبی ، مکن انکار غریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما دعاگوی غریبان جهانیم همه</p></div>
<div class="m2"><p>در همه حال خدا باد نگهدار غریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردمندیم و به امید دوا آمده ایم</p></div>
<div class="m2"><p>تو طبیبی و دوا کن دل بیمار غریب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار غربت چه اگر کار غریبی است ولی</p></div>
<div class="m2"><p>خوش شود گر تو بسازی به کرم کار غریب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ماست سرجمله غریبان جهان</p></div>
<div class="m2"><p>که به سر وقت غریب آمده سردار غریب</p></div></div>