---
title: >-
    غزل شمارهٔ ۱۲۰۹
---
# غزل شمارهٔ ۱۲۰۹

<div class="b" id="bn1"><div class="m1"><p>لذت رند مست ما دانیم</p></div>
<div class="m2"><p>عادی می پرست ما دانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به میخانه رفت خوش بنشست</p></div>
<div class="m2"><p>نیک جائی نشست دانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد گنجینهٔ حدوث و قدم</p></div>
<div class="m2"><p>در وجود آنچه هست ما دانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام می را مدام می نوشیم</p></div>
<div class="m2"><p>توبهٔ ما شکست ما دانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رند مستیم و دامن ساقی</p></div>
<div class="m2"><p>خوش گرفته به دست ما دانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ما تا ابد به عهد خود است</p></div>
<div class="m2"><p>از ازل عهد بست ما دانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو چه دانی که ذوق سید چیست</p></div>
<div class="m2"><p>ذوق این میر مست ما دانیم</p></div></div>