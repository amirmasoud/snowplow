---
title: >-
    غزل شمارهٔ ۷۲۹
---
# غزل شمارهٔ ۷۲۹

<div class="b" id="bn1"><div class="m1"><p>هرکه در دریای بی پایان فتاد</p></div>
<div class="m2"><p>همچو ما در بحر بی پایان فتاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق جانان آتشی خوش برفروخت</p></div>
<div class="m2"><p>شعله ای در جان مشتاقان فتاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رند مستی سر به پای خم نهاد</p></div>
<div class="m2"><p>غلغلی در مجلس رندان فتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه جان بفروخت درد دل خرید</p></div>
<div class="m2"><p>نیک سودا کرد و خوش ارزان فتاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار ما را کار با اغیار نیست</p></div>
<div class="m2"><p>کار او ای یار با یاران فتاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر کویش کسی کو دور شد</p></div>
<div class="m2"><p>بی سر و پا سخت سرگردان فتاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جان به جانان داد و رفت</p></div>
<div class="m2"><p>خوش بود جانی که با جانان فتاد</p></div></div>