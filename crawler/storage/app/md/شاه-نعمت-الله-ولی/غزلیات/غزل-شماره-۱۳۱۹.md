---
title: >-
    غزل شمارهٔ ۱۳۱۹
---
# غزل شمارهٔ ۱۳۱۹

<div class="b" id="bn1"><div class="m1"><p>نقد گنج کنج دل از ما بجو</p></div>
<div class="m2"><p>آبرو جوئی در این دریا بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک دمی با ما به میخانه خرام</p></div>
<div class="m2"><p>ذوق سرمستان ما آنجا بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دنیی و عقبی به این و آن گذار</p></div>
<div class="m2"><p>حضرت یکتای بی همتا بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رند سرمستی اگر جوئی بیا</p></div>
<div class="m2"><p>در خرابات مغان ما را بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در همه آئینه ها او را طلب</p></div>
<div class="m2"><p>یک مسما از همه اسما بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شرح اسماء الهی خوش بخوان</p></div>
<div class="m2"><p>معنیش در دفتر اشیا بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور او در چشم ما پنهان شده</p></div>
<div class="m2"><p>آنچنان پنهان چنین پیدا بجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما مقیم خلوت دل گشته ایم</p></div>
<div class="m2"><p>جای ما در جنت المأوا بجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سید ما نور چشم عالم است</p></div>
<div class="m2"><p>نور او از جملهٔ اشیا بجو</p></div></div>