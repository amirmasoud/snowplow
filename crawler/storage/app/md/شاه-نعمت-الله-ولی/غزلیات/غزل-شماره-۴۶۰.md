---
title: >-
    غزل شمارهٔ ۴۶۰
---
# غزل شمارهٔ ۴۶۰

<div class="b" id="bn1"><div class="m1"><p>عشق را مسجد و میخانه یکی است</p></div>
<div class="m2"><p>عشق را عاقل و دیوانه یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق جانان خود و جان خود است</p></div>
<div class="m2"><p>عشق را دلبر و جانانه یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را آتش دلسوزی هست </p></div>
<div class="m2"><p>نزد او خرمن و یک دانه یکی است</p></div></div>