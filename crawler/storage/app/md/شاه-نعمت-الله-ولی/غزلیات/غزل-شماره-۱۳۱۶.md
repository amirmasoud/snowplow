---
title: >-
    غزل شمارهٔ ۱۳۱۶
---
# غزل شمارهٔ ۱۳۱۶

<div class="b" id="bn1"><div class="m1"><p>خوش در آ در بحر ما ما را بجو</p></div>
<div class="m2"><p>خانهٔ اصلی است این ما را بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما ز دریائیم و دریا عین ما</p></div>
<div class="m2"><p>عین ما جوئی به عین ما بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما از نور رویش روشنست</p></div>
<div class="m2"><p>نور او در دیدهٔ بینا بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه گر صد ببینی ور هزار</p></div>
<div class="m2"><p>در همه آئینه ها او را بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در وجود خویشتن سیری بکن</p></div>
<div class="m2"><p>حضرت یکتای بی همتا بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان رندانه رو</p></div>
<div class="m2"><p>ساقی سرمست مستان را بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جستجوی عاشقانه خوش بود</p></div>
<div class="m2"><p>نعمت الله در همه اشیا بجو</p></div></div>