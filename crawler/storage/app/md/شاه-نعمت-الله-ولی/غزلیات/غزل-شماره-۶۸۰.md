---
title: >-
    غزل شمارهٔ ۶۸۰
---
# غزل شمارهٔ ۶۸۰

<div class="b" id="bn1"><div class="m1"><p>لطف ساقی بسی کرم فرمود</p></div>
<div class="m2"><p>در میخانه را به ما بگشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام گیتی نما به ما بخشید</p></div>
<div class="m2"><p>می میخانه را به ما پیمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد گنجینهٔ حدوث و قدم</p></div>
<div class="m2"><p>جمع کرده همه به ما بنمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ازل تا ابد عنایت او</p></div>
<div class="m2"><p>هست با بندگان و خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هو هو لا اله الا هو</p></div>
<div class="m2"><p>لیس فی الدار غیره موجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش غیری خیال اگر بندی</p></div>
<div class="m2"><p>آن خیالت محال خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صد است ار هزار جمله یکی</p></div>
<div class="m2"><p>جز یکی نیست بنده را مقصود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وحده لاشریک له گفتم</p></div>
<div class="m2"><p>غیر او نیست شاهد و مشهود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بزم ما مجلسی است شاهانه</p></div>
<div class="m2"><p>سید ما ایاز و او محمود</p></div></div>