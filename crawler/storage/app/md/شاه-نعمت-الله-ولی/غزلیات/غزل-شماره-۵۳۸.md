---
title: >-
    غزل شمارهٔ ۵۳۸
---
# غزل شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>نعمت الله خدا به ما بخشید</p></div>
<div class="m2"><p>خوش نوائی به بینوا بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنج اسما به ما عطا فرمود</p></div>
<div class="m2"><p>پادشاهی به این گدا بخشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلعتی خوش مرصع از کرمش</p></div>
<div class="m2"><p>رحمتی کرد و آن به ما بخشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرچه خواهد چنین چنان بخشد</p></div>
<div class="m2"><p>کس نگوید که او چرا بخشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم نبوت به انبیا او داد</p></div>
<div class="m2"><p>هم ولایت به اولیا بخشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل اگر برد جان کرامت کرد</p></div>
<div class="m2"><p>درد اگر داد هم دوا بخشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدی ساخت بندهٔ خود را</p></div>
<div class="m2"><p>منصب عالی ای مرا بخشید</p></div></div>