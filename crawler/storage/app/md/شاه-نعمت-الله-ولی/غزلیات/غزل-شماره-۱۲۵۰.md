---
title: >-
    غزل شمارهٔ ۱۲۵۰
---
# غزل شمارهٔ ۱۲۵۰

<div class="b" id="bn1"><div class="m1"><p>در جام جهان نما جهان بین</p></div>
<div class="m2"><p>در آینه عین ما روان بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جامی به کف آر عارفانه</p></div>
<div class="m2"><p>معشوقهٔ جمله عاشقان بین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دیدهٔ ما نشین زمانی</p></div>
<div class="m2"><p>نور بصر محققان بین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از دیدهٔ مردم ار نهانست</p></div>
<div class="m2"><p>پیداست به چشم ما عیان بین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوئی فردا ببینم او را</p></div>
<div class="m2"><p>فردا امروز و این زمان بین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذر ز نشان و نام هستی</p></div>
<div class="m2"><p>در عالم نیستی نشان بین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی روان نعمت الله</p></div>
<div class="m2"><p>می نوش و حیات جاودان بین</p></div></div>