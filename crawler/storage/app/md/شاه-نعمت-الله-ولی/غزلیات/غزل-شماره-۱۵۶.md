---
title: >-
    غزل شمارهٔ ۱۵۶
---
# غزل شمارهٔ ۱۵۶

<div class="b" id="bn1"><div class="m1"><p>روح اعظم روان سید ماست</p></div>
<div class="m2"><p>لوح محفوظ آن سید ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر معانی که عارفان دانند</p></div>
<div class="m2"><p>دو سه حرف از بیان سید ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی مثال و مثال هر فردی</p></div>
<div class="m2"><p>یرلغی از نشان سید ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان جزوی فنا شود اما</p></div>
<div class="m2"><p>جان جاوید جان سید ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل اول به نزد اهل دلان</p></div>
<div class="m2"><p>عاشق عاشقان سید ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر یکی را از او بود اسمی</p></div>
<div class="m2"><p>اسم اعظم از آن سید ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله که میر مستانست</p></div>
<div class="m2"><p>بندهٔ بندگان سید ماست</p></div></div>