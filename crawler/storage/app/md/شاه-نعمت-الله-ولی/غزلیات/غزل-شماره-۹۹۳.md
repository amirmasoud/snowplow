---
title: >-
    غزل شمارهٔ ۹۹۳
---
# غزل شمارهٔ ۹۹۳

<div class="b" id="bn1"><div class="m1"><p>شاها کرمی کن و مکن جنگ</p></div>
<div class="m2"><p>زنهار مکن به جنگ آهنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر جنگ کنی ملازمانت</p></div>
<div class="m2"><p>اشکسته شوند و سخت دلتنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشنو سخنی ز نعمت‌اللّه</p></div>
<div class="m2"><p>صلحی کن و باز گرد از جنگ</p></div></div>