---
title: >-
    غزل شمارهٔ ۹۸
---
# غزل شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>آب ما می رود بجو دریاب</p></div>
<div class="m2"><p>عین ما را بجو نکو دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام بستان و باده را می نوش</p></div>
<div class="m2"><p>خم می می نگر سبو دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وا مکن دیده را ز اهل نظر</p></div>
<div class="m2"><p>او به او بین و او به او دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن پشت و رو بسی گفتند</p></div>
<div class="m2"><p>این سخن نیز پشت و رو دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در سر زلف او پریشان شو</p></div>
<div class="m2"><p>جمع می باش مو به مو دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک زمانی به چشم ما بنگر</p></div>
<div class="m2"><p>آب این چشمه سو به سو دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام گیتی نما به دست آور</p></div>
<div class="m2"><p>نعمت الله را نکو دریاب</p></div></div>