---
title: >-
    غزل شمارهٔ ۵۸۰
---
# غزل شمارهٔ ۵۸۰

<div class="b" id="bn1"><div class="m1"><p>چشم ما عین ما به ما بیند</p></div>
<div class="m2"><p>هم به نور خدا ، خدا بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدهٔ ما ندیده غیری را</p></div>
<div class="m2"><p>غیر چون نیست او که را بیند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که خودبین بود نبیند او</p></div>
<div class="m2"><p>زانکه خودبین همه خطا بیند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که با ما نشست در دریا</p></div>
<div class="m2"><p>عین ما آشنای ما بیند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفی کو جمال او را دید</p></div>
<div class="m2"><p>دیده باشد به او چو وا بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دردمندی که درد می نوشد</p></div>
<div class="m2"><p>هم از آن درد دل دوا بیند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به خرابات رندی ار آید</p></div>
<div class="m2"><p>سید مست دو سرا بیند</p></div></div>