---
title: >-
    غزل شمارهٔ ۱۱۱۰
---
# غزل شمارهٔ ۱۱۱۰

<div class="b" id="bn1"><div class="m1"><p>من ترک می و صحبت رندان نتوانم</p></div>
<div class="m2"><p>یک لحظه جدائی ز حریفان نتوانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی ساغر و بی شاهد و بی می نتوان بود</p></div>
<div class="m2"><p>بی دلبر و بی مجلس جانان نتوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرگز ندهم جام می ازدست زمانی</p></div>
<div class="m2"><p>جان است رها کردن آسان نتوانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوئی که بکن توبه ازین باده پرستی</p></div>
<div class="m2"><p>زنهار مگو خواجه که من آن نتوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سریست مرا در سر و با کس نتوان گفت</p></div>
<div class="m2"><p>دردیست مرا در دل و درمان نتوانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در کوی خرابات مغان مست و خرابم</p></div>
<div class="m2"><p>بودن نفسی بی می و مستان نتوانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دیدهٔ من نقش خیال رخ سید</p></div>
<div class="m2"><p>نوریست که پیدا شده پنهان نتوانم</p></div></div>