---
title: >-
    غزل شمارهٔ ۸۳
---
# غزل شمارهٔ ۸۳

<div class="b" id="bn1"><div class="m1"><p>در دل ما نقد گنج ما طلب</p></div>
<div class="m2"><p>گوهر ار جوئی از این دریا طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک زمان در بحر ما با ما نشین</p></div>
<div class="m2"><p>عین ما را هم به عین ما طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را جائی معین هست نیست</p></div>
<div class="m2"><p>جای آن بی جای ما هر جا طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور او در جمله اشیا می نگر</p></div>
<div class="m2"><p>یک مسمی از همه اسما طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دنیی و عقبی به این و آن گذار</p></div>
<div class="m2"><p>نصرت یکتای بی همتا طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>طالب و مطلوب را با هم ببین</p></div>
<div class="m2"><p>این نظر از دیدهٔ بینا طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را اگر جوئی بیا</p></div>
<div class="m2"><p>ما به دست آور ز ما ، ما را طلب</p></div></div>