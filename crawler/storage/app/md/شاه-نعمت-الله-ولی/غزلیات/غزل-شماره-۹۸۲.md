---
title: >-
    غزل شمارهٔ ۹۸۲
---
# غزل شمارهٔ ۹۸۲

<div class="b" id="bn1"><div class="m1"><p>در آینهٔ وجود مطلق</p></div>
<div class="m2"><p>خود بینم و خودنمایم الحق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مائیم حباب و آب دریا</p></div>
<div class="m2"><p>هم جام شراب و بحر و زورق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او معشوقست و عاشق ما</p></div>
<div class="m2"><p>از عشق شدیم هر دو مشتق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستیم و خراب در خرابات</p></div>
<div class="m2"><p>ایمن ز مقیدیم و مطلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک جرعه ز دُرد درد ساقی</p></div>
<div class="m2"><p>بهتر ز هزار جام رادق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما بلبل سرخوشیم و گلشن</p></div>
<div class="m2"><p>از نالهٔ ما گرفت رونق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر قول که گفت نعمت الله</p></div>
<div class="m2"><p>گفتند جهانیان که صدق</p></div></div>