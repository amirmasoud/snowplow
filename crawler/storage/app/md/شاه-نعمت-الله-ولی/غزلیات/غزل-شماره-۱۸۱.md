---
title: >-
    غزل شمارهٔ ۱۸۱
---
# غزل شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>درد با همدرد اگر گوئی رواست</p></div>
<div class="m2"><p>درد با بی درد خود گفتن خطاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمندانیم و دُردی می خوریم</p></div>
<div class="m2"><p>دردمندی همچو ما دیگر کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُرد دردش نوش کن گر عاشقی</p></div>
<div class="m2"><p>زانکه دُرد درد او ما را دواست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نظر داریم بحر بیکران</p></div>
<div class="m2"><p>آبروی ما همه از عین ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق در دور است و ما همراه او</p></div>
<div class="m2"><p>سیر ما بی ابتدا و انتهاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمله موجودیم از جود وجود</p></div>
<div class="m2"><p>هرچه بود و هست نور کبریاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هیچ شیئی بی نعمت الله هست نیست</p></div>
<div class="m2"><p>هرچه هست و بود و باشد از خداست</p></div></div>