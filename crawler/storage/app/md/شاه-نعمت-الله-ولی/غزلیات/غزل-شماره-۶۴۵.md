---
title: >-
    غزل شمارهٔ ۶۴۵
---
# غزل شمارهٔ ۶۴۵

<div class="b" id="bn1"><div class="m1"><p>با من بینوا چه خواهی کرد</p></div>
<div class="m2"><p>حاجتم جز روا چه خواهی کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان غمدیده را چه خواهی داد</p></div>
<div class="m2"><p>درد دل جز دوا چه خواهی کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما نکردیم جز گنه چیزی</p></div>
<div class="m2"><p>تو به ما جز عطا چه خواهی کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تو ما را به جرم ما گیری</p></div>
<div class="m2"><p>کرم و لطف را چه خواهی کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این دل ریش مستمندان را</p></div>
<div class="m2"><p>عاقبت جز شفا چه خواهی کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان آمدند بر خوانت</p></div>
<div class="m2"><p>طعمه شان جز لقا چه خواهی کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ریختی خون نعمت الله را</p></div>
<div class="m2"><p>ننگ خون گدا چه خواهی کرد</p></div></div>