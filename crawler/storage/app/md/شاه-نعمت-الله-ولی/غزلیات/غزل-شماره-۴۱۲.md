---
title: >-
    غزل شمارهٔ ۴۱۲
---
# غزل شمارهٔ ۴۱۲

<div class="b" id="bn1"><div class="m1"><p>آتشی ظاهر شد و پیدا و پنهانم بسوخت</p></div>
<div class="m2"><p>شمع عشقش در گرفت و رشتهٔ جانم بسوخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دم گرمم به عالم آتشی خوش در فتاد</p></div>
<div class="m2"><p>هرچه بود ازخشک و تر هم این و هم آنم بسوخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق جانان آتش است و جان من پروانه ای</p></div>
<div class="m2"><p>منتش بر جان من کز عشق او جانم بسوخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عود دل را سوختم در مجمر سینه خوشی</p></div>
<div class="m2"><p>از تف آن دامن و کوی گریبانم بسوخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بود گنج معرفت در کنج ویران دلم</p></div>
<div class="m2"><p>آتشی افتاد و گنج و کنج ویرانم بسوخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز آه دل سوزم که آتش می نهد در این و آن</p></div>
<div class="m2"><p>جسم و جان بر باد رفت و کفر و ایمانم بسوخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفته های نعمت الله می نوشتم در کتاب</p></div>
<div class="m2"><p>در ورق آتش فتاد و دست و دیوانم بسوخت</p></div></div>