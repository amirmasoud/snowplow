---
title: >-
    غزل شمارهٔ ۳۰۶
---
# غزل شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>میخانه سرای عاشقان است</p></div>
<div class="m2"><p>خود خلوت خاص عاشقانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم بدن است و عشق جانان</p></div>
<div class="m2"><p>جان است که در بدن روانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقست که عاشق است و معشوق</p></div>
<div class="m2"><p>در مذهب عاشقان چنان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با صورت و معنئی که او راست</p></div>
<div class="m2"><p>چه جای معانی و بیان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام است و شراب و رند و ساقی</p></div>
<div class="m2"><p>در مجلس ما همین همان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دیدهٔ مست ما نظر کن</p></div>
<div class="m2"><p>نوری که به چشم ما عیان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این گوهر نظم نعمت الله</p></div>
<div class="m2"><p>از بحر محیط بیکران است</p></div></div>