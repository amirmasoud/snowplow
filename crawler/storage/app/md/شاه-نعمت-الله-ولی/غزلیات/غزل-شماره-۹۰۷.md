---
title: >-
    غزل شمارهٔ ۹۰۷
---
# غزل شمارهٔ ۹۰۷

<div class="b" id="bn1"><div class="m1"><p>به کام دل رسیدم باز امروز</p></div>
<div class="m2"><p>جمال یار دیدم باز امروز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحمدالله که از هجران رمیدم</p></div>
<div class="m2"><p>به وصل او رسیدم باز امروز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسی دیروز گفتم ای خداوند</p></div>
<div class="m2"><p>جواب خود شنیدم باز امروز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خمخانهٔ معنی و صورت</p></div>
<div class="m2"><p>به جامی در کشیدم باز امروز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به ساقی خویش را بفروختم دوش</p></div>
<div class="m2"><p>بهایش می خریدم باز امروز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ندای ارجعی آمد به گوشم</p></div>
<div class="m2"><p>به سوی شه پریدم باز امروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گلی از گلستان نعمت الله</p></div>
<div class="m2"><p>به دست ذوق چیدم باز امروز</p></div></div>