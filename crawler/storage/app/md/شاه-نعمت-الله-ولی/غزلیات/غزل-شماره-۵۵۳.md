---
title: >-
    غزل شمارهٔ ۵۵۳
---
# غزل شمارهٔ ۵۵۳

<div class="b" id="bn1"><div class="m1"><p>عاقبت سید ما سوی مغان خواهد شد</p></div>
<div class="m2"><p>به سراپردهٔ میخانه روان خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بگویند که فرما و بیا مستانه</p></div>
<div class="m2"><p>زند انگشت خوشی رقص کنان خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی است که از مشرق جان می تابد</p></div>
<div class="m2"><p>گرچه از دیدهٔ ما باز نهان خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عالم چو بود آینهٔ حضرت او</p></div>
<div class="m2"><p>در همه آینه بر خود نگران خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عین ما آب حیاتست و حبابش خوانند</p></div>
<div class="m2"><p>زود بینند که بی نام و نشان خواهد شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام می آمد و آورد پیام ساقی</p></div>
<div class="m2"><p>که دمی همدم ما شو که چنان خواهد شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صحبت سید سرمست غنیمت می دان</p></div>
<div class="m2"><p>که در این یک دو سه روزی ز جهان خواهد شد</p></div></div>