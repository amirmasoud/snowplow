---
title: >-
    غزل شمارهٔ ۳۶۷
---
# غزل شمارهٔ ۳۶۷

<div class="b" id="bn1"><div class="m1"><p>هرچه امروز حاصل ما نیست</p></div>
<div class="m2"><p>طلب آن مکن که فردا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر در اینجا ندیده ای او را</p></div>
<div class="m2"><p>رؤیت او تو را در اینجا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حق به حق بین که ما چنین دیدیم</p></div>
<div class="m2"><p>دیده ای کان ندید بینا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وانکه حق را به خویشتن بیند</p></div>
<div class="m2"><p>دیده اش بر کمال گویا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که گوید که حق به خود بیند</p></div>
<div class="m2"><p>این سعادت ورا مهیا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه آبند قطره و دریا</p></div>
<div class="m2"><p>قطره در وصف همچو دریا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله نور دیده بود</p></div>
<div class="m2"><p>چشم هر کو ندید بینا نیست</p></div></div>