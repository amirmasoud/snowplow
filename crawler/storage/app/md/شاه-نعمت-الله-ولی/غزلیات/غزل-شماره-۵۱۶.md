---
title: >-
    غزل شمارهٔ ۵۱۶
---
# غزل شمارهٔ ۵۱۶

<div class="b" id="bn1"><div class="m1"><p>ترک چشم مست او دلها بغارت می برد</p></div>
<div class="m2"><p>ملک دل بگرفت و جان ما به غارت می برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانمان ما به غارت برد و یک موئی نماند</p></div>
<div class="m2"><p>هرچه با ما دید سر تا پا به غارت می برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دور شو ای عقل از اینجا رخت خود را هم ببر</p></div>
<div class="m2"><p>زانکه رخت هر که دید اینجا به غارت می برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کیش او چون غارتست ترکش نگوید ترک مست</p></div>
<div class="m2"><p>جان کند قربان و قربان را به غارت می برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه دید از نقد و جنس و زیر و بالا پاک کرد</p></div>
<div class="m2"><p>این بلا هم زیر و هم بالا به غارت می برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان ما بادش فدا کو جان و هم جانان ماست</p></div>
<div class="m2"><p>هر چه خواهد گو ببر هل تا به غارت می برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما صد بخارا را به غارت برده است</p></div>
<div class="m2"><p>بوعلی چبود که او سینا به غارت می برد</p></div></div>