---
title: >-
    غزل شمارهٔ ۱۴۶۲
---
# غزل شمارهٔ ۱۴۶۲

<div class="b" id="bn1"><div class="m1"><p>بیا بر چشم ما بنشین که خوش آب روان بینی</p></div>
<div class="m2"><p>دمی از خود بیاسائی سر آبی چنان بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آ در گوشهٔ دیده کناری گیر از مردم</p></div>
<div class="m2"><p>که بر دست و کنار آنجا کنارش در میان بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال عارضش جوئی در آب چشم ما می جو</p></div>
<div class="m2"><p>که نور دیدهٔ مردم درین آب روان بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بحر ما خوشی چون ما در آ با ما دمی بنشین</p></div>
<div class="m2"><p>که ما را عین ما هم چون محیطی بی کران بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان و نام خود بگذار بی نام و نشان می رو</p></div>
<div class="m2"><p>چو بی نام و نشان گشتی به نام او نشان بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریف بزم رندان شو که عمر جاودان یابی</p></div>
<div class="m2"><p>به میخانه در آ با ما که میر عاشقان بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز سید جام می بستان و جام و می به هم می بین</p></div>
<div class="m2"><p>بیابی لذتی چون ما اگر این بینی آن بینی</p></div></div>