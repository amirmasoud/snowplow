---
title: >-
    غزل شمارهٔ ۱۴۳۸
---
# غزل شمارهٔ ۱۴۳۸

<div class="b" id="bn1"><div class="m1"><p>راهیم و رهنمائیم هم رهرویم و همراه</p></div>
<div class="m2"><p>هم سیدیم و بنده هم چاکریم و هم شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام می لطیف است این جسم و جان که داریم</p></div>
<div class="m2"><p>در باطن آفتابیم در ظاهریم چون ماه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاهی چنین که بینی بر تخت چون سلیمان</p></div>
<div class="m2"><p>گاهی چنانکه دانی چون یوسفیم در چاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رندیم و لاابالی سرمست در خرابات</p></div>
<div class="m2"><p>با ساقئی حریفیم دایم به گاه و بیگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در راه بی کرانه ما می رویم دایم</p></div>
<div class="m2"><p>گر عزم راه داری ما با تو ایم همراه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای بنده بندگی کن تا پادشاه گردی</p></div>
<div class="m2"><p>زیرا که پادشاهند این بندگان درگاه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>توقیع آل دارد حکم ولایت ما</p></div>
<div class="m2"><p>باشد نشان آن حکم بر نام نعمت الله</p></div></div>