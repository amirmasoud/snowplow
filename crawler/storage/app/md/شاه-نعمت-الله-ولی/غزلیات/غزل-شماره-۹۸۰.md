---
title: >-
    غزل شمارهٔ ۹۸۰
---
# غزل شمارهٔ ۹۸۰

<div class="b" id="bn1"><div class="m1"><p>تن به جان زنده است و جان از عشق</p></div>
<div class="m2"><p>در بدن روح ما روان از عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق داند که ذوق عاشق چیست</p></div>
<div class="m2"><p>باز جو ذوق عاشقان از عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه در کاینات موجود است</p></div>
<div class="m2"><p>جود عشق است و باشد آن از عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقان عشق را به جان جویند</p></div>
<div class="m2"><p>عاقلانند غافلان از عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت الله که میر مستان است</p></div>
<div class="m2"><p>می دهد بنده را نشان از عشق</p></div></div>