---
title: >-
    غزل شمارهٔ ۹۷۹
---
# غزل شمارهٔ ۹۷۹

<div class="b" id="bn1"><div class="m1"><p>عاشقان غرقند در دریای عشق</p></div>
<div class="m2"><p>اوفتاده مست در غوغای عشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن معشوق بگرفته به دست</p></div>
<div class="m2"><p>سر نهاده دائما در پای عشق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق و معشوق و عشق آمد یکی</p></div>
<div class="m2"><p>در سر ما نیست جز سودای عشق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور چشم عاشقان عشق وی است</p></div>
<div class="m2"><p>عقل کی داریم ما بر جای عشق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک عالم را به سلطانی گرفت</p></div>
<div class="m2"><p>حضرت یکتای بی همتای عشق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار ما از عاشقی بالا شده</p></div>
<div class="m2"><p>این بلا می جو تو از بالای عشق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق در جان است و در دل درد او</p></div>
<div class="m2"><p>نعمت الله واله و شیدای عشق</p></div></div>