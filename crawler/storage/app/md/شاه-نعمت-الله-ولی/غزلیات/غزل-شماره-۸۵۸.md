---
title: >-
    غزل شمارهٔ ۸۵۸
---
# غزل شمارهٔ ۸۵۸

<div class="b" id="bn1"><div class="m1"><p>آمد خیال غیر چو خوابیم در نظر</p></div>
<div class="m2"><p>بنمود کاینات سرابیم در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردند جلوه صورت و معنی به یکدیگر</p></div>
<div class="m2"><p>چون شاهدان حور نقابیم در نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون رند و لاابالی و سرمست و عاشقیم</p></div>
<div class="m2"><p>عالم نموده جام شرابیم در نظر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمم به نور دیدن رویش منور است</p></div>
<div class="m2"><p>شکرت که نیست هیچ حجابیم در نظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز نخورده ایم می دوستی غیر</p></div>
<div class="m2"><p>گرچه مدام مست و خرابیم در نظر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دم که تشنه بودم و آبم نبود بود</p></div>
<div class="m2"><p>بحر محیط قطرهٔ آبیم در نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر لوح دل نوشته ام اسرار سیدم</p></div>
<div class="m2"><p>باشد مدام همچو کتابیم در نظر</p></div></div>