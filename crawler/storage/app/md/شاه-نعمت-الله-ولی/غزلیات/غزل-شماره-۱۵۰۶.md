---
title: >-
    غزل شمارهٔ ۱۵۰۶
---
# غزل شمارهٔ ۱۵۰۶

<div class="b" id="bn1"><div class="m1"><p>جام ساقی پر مئی آری</p></div>
<div class="m2"><p>همدم نائی و نئی آری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر تو گوئی میَم مئی آری</p></div>
<div class="m2"><p>ور تو گوئی نیَم نیی آری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این عجب بین که جامع همه شی</p></div>
<div class="m2"><p>با همه شئی لاشئی آری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>که به رند اقتدا کند صوفی</p></div>
<div class="m2"><p>در پی پیر نیک پی آری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتهٔ او اگر شوی عبدی</p></div>
<div class="m2"><p>همچو من سید حیی آری</p></div></div>