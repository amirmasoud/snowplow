---
title: >-
    غزل شمارهٔ ۷۷۵
---
# غزل شمارهٔ ۷۷۵

<div class="b" id="bn1"><div class="m1"><p>گرد میخانهٔ دل به جان گردید</p></div>
<div class="m2"><p>همچو رندان به جان روان گردید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه مخمور بود مستی شد</p></div>
<div class="m2"><p>این چنین بود آن چنان گردید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد کنج خراب گشت بسی</p></div>
<div class="m2"><p>گنج پنهان بر او عیان گردید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نشانی ز بی نشان یابد</p></div>
<div class="m2"><p>نام را ماند و بی نشان گردید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف معشوق ما کرم فرمود</p></div>
<div class="m2"><p>مونس جان عاشقان گردید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسم علم بدیع را خواندیم</p></div>
<div class="m2"><p>آن معانی به ما عیان گردید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مقامی که نعمت الله است</p></div>
<div class="m2"><p>گرد آن در کجا توان گردید</p></div></div>