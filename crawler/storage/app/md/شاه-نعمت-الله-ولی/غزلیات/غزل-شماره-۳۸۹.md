---
title: >-
    غزل شمارهٔ ۳۸۹
---
# غزل شمارهٔ ۳۸۹

<div class="b" id="bn1"><div class="m1"><p>در حقیقت عشق را خود نام نیست</p></div>
<div class="m2"><p>می که می نوشد چو آنجا جام نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کی بیابد نیک نامی در جهان</p></div>
<div class="m2"><p>هر که او در عاشقی بدنام نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مرغ دل سیمرغ قاف معرفت</p></div>
<div class="m2"><p>جز سر زلف بتانش دام نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سوختگان دانند و ایشان گفته اند</p></div>
<div class="m2"><p>پخته داند کاین سخن با خام نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صبحدم می گفت سرمستی به من</p></div>
<div class="m2"><p>بامداد عاشقان را شام نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان مستان بسی است</p></div>
<div class="m2"><p>همچو من مستی در این ایام نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جام می بخشد مدام</p></div>
<div class="m2"><p>خوشتر از انعام او انعام نیست</p></div></div>