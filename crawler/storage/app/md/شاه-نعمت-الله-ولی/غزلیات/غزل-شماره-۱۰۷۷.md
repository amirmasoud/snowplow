---
title: >-
    غزل شمارهٔ ۱۰۷۷
---
# غزل شمارهٔ ۱۰۷۷

<div class="b" id="bn1"><div class="m1"><p>مدتی در به در به جان گشتم</p></div>
<div class="m2"><p>گرد میخانهٔ جهان گشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میر میخانه خدمتش کردم</p></div>
<div class="m2"><p>هم به فرمان او روان گشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات عشق رندانه</p></div>
<div class="m2"><p>ساقی بزم عاشقان گشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نام من شد نشانهٔ عالم</p></div>
<div class="m2"><p>گرچه بی نام و بی نشان گشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون محب حباب او بودم</p></div>
<div class="m2"><p>نیک محبوب این و آن گشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان به جانان خویش بسپردم</p></div>
<div class="m2"><p>زندهٔ ملک جاودان گشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>موج بودم ولی شدم دریا</p></div>
<div class="m2"><p>این چنین بودم آن چنان گشتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عقل سرمایه بود شد بر باد</p></div>
<div class="m2"><p>فارغ از سود و از زیان گشتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گنج در کنج دل طلب کردم</p></div>
<div class="m2"><p>واقف از گنج بیکران گشتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پادشه خوش مرا کنار گرفت</p></div>
<div class="m2"><p>چون کمر گرد آن میان گشتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>بنده ام بندگی او کردم</p></div>
<div class="m2"><p>سید جمله سیدان گشتم</p></div></div>