---
title: >-
    غزل شمارهٔ ۱۸۶
---
# غزل شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>نور او روشنی دیدهٔ ماست</p></div>
<div class="m2"><p>نظری کن به چشم ما پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی او را به نور او بینند</p></div>
<div class="m2"><p>چشم بیننده ای که او بیناست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحده لاشریک له گفتم</p></div>
<div class="m2"><p>آنکه عالم به نور خود آراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر دل را کرانه نیست پدید</p></div>
<div class="m2"><p>جان ما غرقهٔ چنین دریاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق آمد به جای ما بنشست</p></div>
<div class="m2"><p>مائی ما چه از میان برخاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه گفتند و هرچه می گویند</p></div>
<div class="m2"><p>حضرت وحدتش از آن یکتاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله که میر مستانست</p></div>
<div class="m2"><p>عاشق روی جملهٔ اشیاست</p></div></div>