---
title: >-
    غزل شمارهٔ ۱۲۴۲
---
# غزل شمارهٔ ۱۲۴۲

<div class="b" id="bn1"><div class="m1"><p>عشق در آن و این توان دیدن</p></div>
<div class="m2"><p>بر یسار و یمین توان دیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن چنان آفتاب روشن رای</p></div>
<div class="m2"><p>در رخ شمس دین توان دیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه اگر چه بر آسمان باشد</p></div>
<div class="m2"><p>نور او در زمین توان دیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقانه اگر طلبکاری</p></div>
<div class="m2"><p>آن چنان این چنین توان دیدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر امین خدا چو من باشی</p></div>
<div class="m2"><p>جبرئیل امین توان دیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با سلیمان اگر حریف شوی</p></div>
<div class="m2"><p>خاتمش با نگین توان دیدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را اگر یابی</p></div>
<div class="m2"><p>دلبر نازنین توان دیدن</p></div></div>