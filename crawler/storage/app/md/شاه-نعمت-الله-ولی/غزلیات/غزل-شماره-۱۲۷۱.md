---
title: >-
    غزل شمارهٔ ۱۲۷۱
---
# غزل شمارهٔ ۱۲۷۱

<div class="b" id="bn1"><div class="m1"><p>اگر ذوق صفا داری طلب کن خدمت رندان</p></div>
<div class="m2"><p>و گر خواهی حضوری خوش در آ در خلوت رندان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را از خدمت زاهد به عمری کار نگشاید</p></div>
<div class="m2"><p>هزاران کار بگشاید دمی از خدمت رندان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طلب کن رند سرمستی که تا ذوق خوشی یابی</p></div>
<div class="m2"><p>دمی با جام همدم شو که یابی لذت رندان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خراباتست و مارمست و ساقی جام می بر دست</p></div>
<div class="m2"><p>چه خوشحالی که من دارم مدام از صحبت رندان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مگو در بزم سرمستان حدیث دنیی وعقبی</p></div>
<div class="m2"><p>به آنها کی فرود آید زمام همت رندان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعیم نعمت رندی مجو از جنت رندان</p></div>
<div class="m2"><p>بیا از نعمت الله جو نعیم نعمت رندان</p></div></div>