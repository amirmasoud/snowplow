---
title: >-
    غزل شمارهٔ ۱۵۷
---
# غزل شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>عشق جانان در میان جان ماست</p></div>
<div class="m2"><p>گنج معنی در دل ویران ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به درد دل گرفتار آمدیم</p></div>
<div class="m2"><p>وین عجب کاین درد دل درمان ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کسی را کفر و ایمانی بود</p></div>
<div class="m2"><p>زلف رویش کفر و هم ایمان ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهدی باری به شأن عقل تو است</p></div>
<div class="m2"><p>عشق بازی آیتی در شأن ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به عشق او به میدان آمدیم</p></div>
<div class="m2"><p>گوی عالم در خم چوگان ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از شراب ناب بی غش سرخوشیم</p></div>
<div class="m2"><p>مستی ما از می جانان ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سماع عارفان در کنج دل</p></div>
<div class="m2"><p>زهره قوال و قمر رقصان ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سید خلوت سرای وحدتیم</p></div>
<div class="m2"><p>نعمت الله از دل و جان آن ماست</p></div></div>