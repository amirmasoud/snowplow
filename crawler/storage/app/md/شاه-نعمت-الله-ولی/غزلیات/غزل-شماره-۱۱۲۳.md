---
title: >-
    غزل شمارهٔ ۱۱۲۳
---
# غزل شمارهٔ ۱۱۲۳

<div class="b" id="bn1"><div class="m1"><p>توبه از می کجا کنم نکنم</p></div>
<div class="m2"><p>ترک رندی چرا کنم نکنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نکنم توبه از می و رندی</p></div>
<div class="m2"><p>بنده هرگز خطا کنم نکنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزم عشق است و عاشقان سرمست</p></div>
<div class="m2"><p>جای دیگر هوا کنم نکنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دامن ساقی و لب ساغر</p></div>
<div class="m2"><p>تا قیامت رها کنم نکنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز به دُردی درد دل جانا</p></div>
<div class="m2"><p>درد خود را دوا کنم نکنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتهٔ تیغ عشق مطلوبم</p></div>
<div class="m2"><p>طلب خونبها کنم نکنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عشق سید که راحت جان است</p></div>
<div class="m2"><p>از دل خود جدا کنم نکنم</p></div></div>