---
title: >-
    غزل شمارهٔ ۶۴۷
---
# غزل شمارهٔ ۶۴۷

<div class="b" id="bn1"><div class="m1"><p>کردگار از کرم عیانم کرد</p></div>
<div class="m2"><p>واقف از حال این و آنم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چو بی نام و بی نشان بودم</p></div>
<div class="m2"><p>بی نشانی مرا نشانم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به تجلی ظاهر و باطن</p></div>
<div class="m2"><p>گاه پیدا و گه نهانم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل آمد به جای جان بنشست</p></div>
<div class="m2"><p>رحمتی خوش به جای جانم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خمخانه را به من بخشید</p></div>
<div class="m2"><p>ساقی مست عاشقانم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا شوم رهبر همه رندان</p></div>
<div class="m2"><p>رهنمودم به رهروانم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شرح علم بدیع او خواندم</p></div>
<div class="m2"><p>این معانی از آن بیانم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون ز هستی خود فنا گشتم</p></div>
<div class="m2"><p>باقی ملک جاودانم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله به من عطا فرمود</p></div>
<div class="m2"><p>رازق زرق بندگانم کرد</p></div></div>