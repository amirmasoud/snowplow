---
title: >-
    غزل شمارهٔ ۱۳۹۷
---
# غزل شمارهٔ ۱۳۹۷

<div class="b" id="bn1"><div class="m1"><p>تا خیال روی خوبش دیده ام در آینه</p></div>
<div class="m2"><p>روز و شب دارم ز عشقش در برابر آینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی او آئینهٔ گیتی نمای جان ماست</p></div>
<div class="m2"><p>جان ما آئینه ای جانانه بنگر آینه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورتی در آینه بنموده تمثالش عیان</p></div>
<div class="m2"><p>شد ز عکس نور آن معنی مصور آینه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بود آئینه روشن روی بنماید تو را</p></div>
<div class="m2"><p>ور نه رویش کی نماید در مکدر آینه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق او شمعست و جانم آینه وین رمز ما</p></div>
<div class="m2"><p>عشقبازان را بود روشن منور آینه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من دلی دارم چو آئینه منیر و با صفا</p></div>
<div class="m2"><p>آفتاب مهر رویش تافته بر آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برنداری آینه از پیش رویت یک زمان</p></div>
<div class="m2"><p>همچو سید گر ببینی روی خود در آینه</p></div></div>