---
title: >-
    غزل شمارهٔ ۹۰۳
---
# غزل شمارهٔ ۹۰۳

<div class="b" id="bn1"><div class="m1"><p>برو ای میر من به مال مناز</p></div>
<div class="m2"><p>بیش از این سیم و زر به هم مگداز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی آزار خلق می جوئی</p></div>
<div class="m2"><p>مکن آزار ور نیابی باز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ور خماری و درد سر داری</p></div>
<div class="m2"><p>با من مست کی شوی دمساز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخنم ساقی است روح افزا</p></div>
<div class="m2"><p>نفسم مطربیست خوش آواز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک من عالمی است بی پایان</p></div>
<div class="m2"><p>و آن تو از ختاست تا شیراز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>من به سلطان خویش می نازم</p></div>
<div class="m2"><p>تو به تاج و سریر خود می ناز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله پیر رندان است</p></div>
<div class="m2"><p>گر مریدی به پیر خود پرداز</p></div></div>