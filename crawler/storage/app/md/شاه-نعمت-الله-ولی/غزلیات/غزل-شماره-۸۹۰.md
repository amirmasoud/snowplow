---
title: >-
    غزل شمارهٔ ۸۹۰
---
# غزل شمارهٔ ۸۹۰

<div class="b" id="bn1"><div class="m1"><p>به هر طرف که نظر می کنم توئی منظور</p></div>
<div class="m2"><p>که دیده است چنین فاش این چنین مستور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز لطف تو نظری یافتم شدی ناظر</p></div>
<div class="m2"><p>چه جای من که توئی ناظر و توئی منظور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو نیست در دو جهان جز یکی کراست وصال</p></div>
<div class="m2"><p>عجب بود که یکی از یکی بود مستور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نور طلعت او روشن است دیدهٔ من</p></div>
<div class="m2"><p>ببین که در همه عالم جز او که دارد نور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز ذوق گفته ام این شعر بشنو از سر ذوق</p></div>
<div class="m2"><p>کسی که ذوق ندارد ز بزم ما گو دور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مقام اهل دلانست صحبت جانم</p></div>
<div class="m2"><p>چه جای روضهٔ رضوان چه قدر حور و قصور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف سیدم و ساقی خراباتم</p></div>
<div class="m2"><p>مدام عاشق مستم نه عاقل مخمور</p></div></div>