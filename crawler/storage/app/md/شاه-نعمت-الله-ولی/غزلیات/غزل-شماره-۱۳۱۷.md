---
title: >-
    غزل شمارهٔ ۱۳۱۷
---
# غزل شمارهٔ ۱۳۱۷

<div class="b" id="bn1"><div class="m1"><p>خوش درآ در بحر ما ما را بجو</p></div>
<div class="m2"><p>آبرو جوئی درین دریا بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره و موج و حباب و بحر و جو</p></div>
<div class="m2"><p>هر چه می خواهی بیا ازما بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قاب قوسین از میانه طرح کن</p></div>
<div class="m2"><p>مخزن اسرار او ادنی بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات فنا افتاده ایم</p></div>
<div class="m2"><p>جای ما جوئی بیا این جا بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بلا چون کار ما بالا گرفت</p></div>
<div class="m2"><p>منصب عالی از آن بالا بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر او نقش خیالی بیش نیست</p></div>
<div class="m2"><p>بگذر از نقش خیال او را بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما را ز یاسین می طلب</p></div>
<div class="m2"><p>صورتش از معنی طه بجو</p></div></div>