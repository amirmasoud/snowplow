---
title: >-
    غزل شمارهٔ ۴۷۰
---
# غزل شمارهٔ ۴۷۰

<div class="b" id="bn1"><div class="m1"><p>بیا ای یار و بر اغیار می‌خند</p></div>
<div class="m2"><p>بنوش این جام و با خمار می‌خند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی ایمان گزید و دیگری کفر</p></div>
<div class="m2"><p>تو مؤمن باش و با کفار می‌خند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یکی با تو نعم گوید یکی لا</p></div>
<div class="m2"><p>تو با اقرار و با انکار می‌خند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو دنیا نیست مأوای حکومت</p></div>
<div class="m2"><p>دلا بر ریش دنیادار می‌خند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زبان بربند و خامش باش در عشق</p></div>
<div class="m2"><p>مشو بی ‌زار و بر آزاد می‌خند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیا چون نعمت‌الله ناظر حق</p></div>
<div class="m2"><p>ببین دیدار و بر دیدار می‌خند</p></div></div>