---
title: >-
    غزل شمارهٔ ۶۸۷
---
# غزل شمارهٔ ۶۸۷

<div class="b" id="bn1"><div class="m1"><p>در مرتبه ای ساجد در مرتبه ای مسجود</p></div>
<div class="m2"><p>در مرتبه ای عابد در مرتبه ای معبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مرتبه ای عبد است در مرتبه ای رب است</p></div>
<div class="m2"><p>در مرتبه ای حامد در مرتبه ای محمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مرتبه ای فانی در مرتبه ای باقی</p></div>
<div class="m2"><p>در مرتبه ای معدوم در مرتبه ای موجود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مرتبه ای طالب در مرتبه ای مطلوب</p></div>
<div class="m2"><p>در مرتبه ای قاصد در مرتبه ای مقصود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مرتبه ای آدم در مرتبه ای خاتم</p></div>
<div class="m2"><p>در مرتبه ای عیسی در مرتبه ای داود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مرتبه ای موسی در مرتبه ای فرعون</p></div>
<div class="m2"><p>در مرتبه ای عیسی در مرتبه ای داود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مرتبه ای بی حد در مرتبه ای بی عد</p></div>
<div class="m2"><p>در مرتبه ای محدود در مرتبه ای معدود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در مرتبه ای ظاهر در مرتبه ای باطن</p></div>
<div class="m2"><p>در مرتبه ای غایب در مرتبه ای مشهود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در مرتبه ای سید در مرتبه ای بنده</p></div>
<div class="m2"><p>در مرتبه ای واجد در مرتبه ای موجود</p></div></div>