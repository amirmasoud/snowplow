---
title: >-
    غزل شمارهٔ ۳۴۵
---
# غزل شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>چشم ما روشن به نور روی اوست</p></div>
<div class="m2"><p>هرچه بیند دوست را بیند به دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق و معشوق ما هر دو یکی است</p></div>
<div class="m2"><p>تا نپنداری که این رشته دوتوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جرعهٔ جام می ما هر که خورد</p></div>
<div class="m2"><p>چون محبان دائما در جستجوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق سرمست است و فارغ از همه</p></div>
<div class="m2"><p>عقل مخمور است و هم در گفتگوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسته ام نقش خیالش در نظر</p></div>
<div class="m2"><p>هرچه دیده می شود چشمم بر اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرقه می شویم به جام می مدام</p></div>
<div class="m2"><p>مدتی شد تا مرا این شست و شوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بیند نعمت الله با همه</p></div>
<div class="m2"><p>بد نبیند هرچه می بیند نکوست</p></div></div>