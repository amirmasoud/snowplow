---
title: >-
    غزل شمارهٔ ۲
---
# غزل شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>نعمت الله است دائم با خدا</p></div>
<div class="m2"><p>نعمت از الله کی باشد جدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل و دیده ندیدم جز یکی</p></div>
<div class="m2"><p>گر چه گردیدم بسی در دو سرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میل ساحل کی کند بحری چو شد</p></div>
<div class="m2"><p>غرقه در دریای بی پایان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما نوا از بینوائی یافتیم</p></div>
<div class="m2"><p>گر نوا جوئی بجو از بینوا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از خدا بیگانه ای دیدیم نه</p></div>
<div class="m2"><p>هر که باشد هست با او آشنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سروری خواهی برآ بر دار عشق</p></div>
<div class="m2"><p>کز سر دار فنا یابی بقا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید سرمست اگر جوئی حریف</p></div>
<div class="m2"><p>خیز مستانه به میخانه درآ</p></div></div>