---
title: >-
    غزل شمارهٔ ۱۱۷
---
# غزل شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>آفتابی ز ماه بسته نقاب</p></div>
<div class="m2"><p>می نماید به چشم ما دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نظری کن در آینه بنگر</p></div>
<div class="m2"><p>ور نداری تو آینه دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش غیری خیال اگر بندی</p></div>
<div class="m2"><p>آن خیالی بود ولی در خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت و معنی همه داند</p></div>
<div class="m2"><p>هر که او باشد از اولوالالباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لیک در هرچه روی بنماید</p></div>
<div class="m2"><p>هم مسبب ببین و هم اسباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب است ماه خوانندش</p></div>
<div class="m2"><p>نور مهر است گفته ام مهتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله مربی نیکوست</p></div>
<div class="m2"><p>تربیت یافته وی از ارباب</p></div></div>