---
title: >-
    غزل شمارهٔ ۳۸۱
---
# غزل شمارهٔ ۳۸۱

<div class="b" id="bn1"><div class="m1"><p>دل ندارد هر که او را درد نیست</p></div>
<div class="m2"><p>وانکه خود دردی ندارد مرد نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نزد بی دردان مگو زینهار درد</p></div>
<div class="m2"><p>دشمنست آن دوست کو همدرد نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با لب و رخسار و چشم مست یار</p></div>
<div class="m2"><p>حاجت نقل و شراب و درد نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوای آفتاب روی او</p></div>
<div class="m2"><p>در به در گشتیم از وی گرد نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد بی درمان ما را از یقین</p></div>
<div class="m2"><p>غیر سید دیگری در خورد نیست</p></div></div>