---
title: >-
    غزل شمارهٔ ۸۵۲
---
# غزل شمارهٔ ۸۵۲

<div class="b" id="bn1"><div class="m1"><p>راه را گم کرده ای جان پدر</p></div>
<div class="m2"><p>خویش را گم کن که ره یابی دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقبازی گر کنی با من نشین</p></div>
<div class="m2"><p>جان بباز و دل بده سر هم به سر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذوق اگر داری ببینی نور او</p></div>
<div class="m2"><p>خوش به چشم ما در آ او را نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آینه گر صد نماید ور هزار</p></div>
<div class="m2"><p>می نماید آفتابی در نظر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک وجود است و صفاتش بی شمار</p></div>
<div class="m2"><p>آن یکی در هر یکی خوش می شمر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق و معشوق و عشقی در وجود</p></div>
<div class="m2"><p>از وجود خود اگر یابی خبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مست نعمت الله را ببین</p></div>
<div class="m2"><p>نور او دارد همیشه در بصر</p></div></div>