---
title: >-
    غزل شمارهٔ ۸۱۹
---
# غزل شمارهٔ ۸۱۹

<div class="b" id="bn1"><div class="m1"><p>یک نظر در چشم مست ما نگر</p></div>
<div class="m2"><p>عین ما در عین این دریا نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>میل ما داری به میخانه خرام</p></div>
<div class="m2"><p>مجلس رندان ما آنجا نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت و معنی عالم را ببین</p></div>
<div class="m2"><p>یک مسمی در همه اسما نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم نابینا نبیند روی او</p></div>
<div class="m2"><p>نور او در دیدهٔ بینا نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در همه آئینه گر داری نظر</p></div>
<div class="m2"><p>حضرت یکتای بی همتا نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رمز گنج کنت کنزاً را بدان</p></div>
<div class="m2"><p>نقد گنجش را بجو اشیا نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ظاهر و باطن ببین ای نور چشم</p></div>
<div class="m2"><p>نعمت الله در همه پیدا نگر</p></div></div>