---
title: >-
    غزل شمارهٔ ۱۵۱۶
---
# غزل شمارهٔ ۱۵۱۶

<div class="b" id="bn1"><div class="m1"><p>جان و جانان توئی چه پنداری</p></div>
<div class="m2"><p>باش یکتا دوئی چه پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از حدوث قدم چه می گوئی</p></div>
<div class="m2"><p>کهنه و نو ، نوی چه پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتمت عاشقانه می ، می نوش</p></div>
<div class="m2"><p>قول ما نشنوی چه پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه میخانه را غلط کردی</p></div>
<div class="m2"><p>به خطا می روی چه پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما چنین مست و تو چنان مخمور</p></div>
<div class="m2"><p>تو چو ما کی شوی چه پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار در خانه و تو سرگشته</p></div>
<div class="m2"><p>در به در می روی چه پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می و جامی و سید و بنده</p></div>
<div class="m2"><p>نعمت الله توئی چه پنداری</p></div></div>