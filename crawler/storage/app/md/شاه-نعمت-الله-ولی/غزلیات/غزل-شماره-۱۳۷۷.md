---
title: >-
    غزل شمارهٔ ۱۳۷۷
---
# غزل شمارهٔ ۱۳۷۷

<div class="b" id="bn1"><div class="m1"><p>عارفانه بیا و خوش می گو</p></div>
<div class="m2"><p>وحده لا اله الا هو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذکر مستانه می کنم شب و روز</p></div>
<div class="m2"><p>تو ز من بشنوی و من از او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه عشق است و ما در او غرقیم</p></div>
<div class="m2"><p>عین ما را به عین ما می جو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باش با عاشقان او یک روی</p></div>
<div class="m2"><p>خوش بگو لا اله الا هو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در دو آئینه رو نمود یکی</p></div>
<div class="m2"><p>آن یکی باشد و نماید دو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر او نیست در وجود ای دوست</p></div>
<div class="m2"><p>ور تو گوئی که هست غیری کو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این چنین گفته های مستانه</p></div>
<div class="m2"><p>بشنو از من که گفته ام نیکو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خرقهٔ پاک اگر هوس داری</p></div>
<div class="m2"><p>جامهٔ خود تو از خودی می شو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله یکی است در عالم</p></div>
<div class="m2"><p>فارغ است از خیال عقل دو رو</p></div></div>