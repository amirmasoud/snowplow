---
title: >-
    غزل شمارهٔ ۲۷۴
---
# غزل شمارهٔ ۲۷۴

<div class="b" id="bn1"><div class="m1"><p>شاه ما در همه جهان طاق است</p></div>
<div class="m2"><p>بس کریم و لطیف اخلاق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به او نیک نیک مشتاقیم</p></div>
<div class="m2"><p>او به ما نیز نیک مشتاق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که او دوستدار یاران است</p></div>
<div class="m2"><p>یاری یار یار مصداق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن عاقلان دگر باشد</p></div>
<div class="m2"><p>قول ما گفته های عشاق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام با زهر را چه می نوشی</p></div>
<div class="m2"><p>می عشقش بجو که تریاق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سهل باشد هزار جان در عشق</p></div>
<div class="m2"><p>نفسی در فراق او شاق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله که میر مستان است</p></div>
<div class="m2"><p>سید عاشقان آفاق است</p></div></div>