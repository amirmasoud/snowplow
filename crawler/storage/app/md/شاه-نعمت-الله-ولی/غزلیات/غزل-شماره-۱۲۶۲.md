---
title: >-
    غزل شمارهٔ ۱۲۶۲
---
# غزل شمارهٔ ۱۲۶۲

<div class="b" id="bn1"><div class="m1"><p>بر در می فروش خوش بنشین</p></div>
<div class="m2"><p>جام می را بنوش خوش بنشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پرده را ز خویشتن مدران</p></div>
<div class="m2"><p>سِر خود را بپوش خوش بنشین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این نصیحت نکوست یادش دار</p></div>
<div class="m2"><p>حلقه ای کن به گوش خوش بنشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد اگر هست خوش خوشی می جوش</p></div>
<div class="m2"><p>ور تو صافی مجوش خوش بنشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از سر کاینات خوش برخیز</p></div>
<div class="m2"><p>تا بیائی به هوش خوش بنشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سمرقند اگر نیابی یار</p></div>
<div class="m2"><p>خوش برو تا بلوش خوش بنشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابات نعمت الله را</p></div>
<div class="m2"><p>گر بیابی به گوش خوش بنشین</p></div></div>