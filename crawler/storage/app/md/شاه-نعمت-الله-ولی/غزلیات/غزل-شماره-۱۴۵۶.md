---
title: >-
    غزل شمارهٔ ۱۴۵۶
---
# غزل شمارهٔ ۱۴۵۶

<div class="b" id="bn1"><div class="m1"><p>می حلالت باد اگر در بزم رندان خورده ای</p></div>
<div class="m2"><p>نوش جانت باد اگر باد باده نوشان خورده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قوت جان و قوت دل دُردی درد است ای عزیز</p></div>
<div class="m2"><p>قوت و قوت خوشی داری اگر آن خورده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات فنا جام بقا را نوش کن</p></div>
<div class="m2"><p>تا توان گفتن که می با می پرستان خورده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل سرمست من جانم فدا بادت که باز</p></div>
<div class="m2"><p>می ز جام جان و نقل از بزم جانان خورده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت فردوس اعلی نیست قدرش پیش تو</p></div>
<div class="m2"><p>گوئیا نزل خوشی از خوان سلطان خورده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم مخور گر خورده ای از عشق او جام شراب</p></div>
<div class="m2"><p>کان می پاک حلال است و به فرمان خورده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا حریف نعمت اللهی که این سان سرخوشی</p></div>
<div class="m2"><p>یا ز خم خسروانی می فراوان خورده ای</p></div></div>