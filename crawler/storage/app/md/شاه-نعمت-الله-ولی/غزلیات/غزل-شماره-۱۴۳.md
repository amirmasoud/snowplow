---
title: >-
    غزل شمارهٔ ۱۴۳
---
# غزل شمارهٔ ۱۴۳

<div class="b" id="bn1"><div class="m1"><p>از جور و جفای بی وفا دوست</p></div>
<div class="m2"><p>چون شد دل خستهٔ بلا دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مائیم غلام و یار مولا</p></div>
<div class="m2"><p>مائیم گدا و پادشا دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیگانه ز هر دو کون گشتیم</p></div>
<div class="m2"><p>دردا که نگشت آشنا دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بند بلا چو بسته پائیم</p></div>
<div class="m2"><p>دیگر چه کند به جای ما دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوست وفا طلب نمودیم</p></div>
<div class="m2"><p>هر چه نکند وفا به ما دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دردسر طبیب رستیم</p></div>
<div class="m2"><p>هم درد من است و هم دوا دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید نکند ز عشق توبه</p></div>
<div class="m2"><p>گر جور کند و گر جفا دوست</p></div></div>