---
title: >-
    غزل شمارهٔ ۹۳۹
---
# غزل شمارهٔ ۹۳۹

<div class="b" id="bn1"><div class="m1"><p>به گوش و هوش من آمدند ای ساقی دوش</p></div>
<div class="m2"><p>که جام جم بستان و می حلال بنوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا که مجلس عشقست و عاشقان سرمست</p></div>
<div class="m2"><p>مدام همدم جامند و خم می در جوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشوده برقع صورت ز روی معنی باز</p></div>
<div class="m2"><p>هزار جان شده حیران و عقلها مدهوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به عشق ساقی رندان که جان من به فداش</p></div>
<div class="m2"><p>سبوی مجلس رندان خوش کشم بر دوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به مشت گل نتوان آفتاب را اندود</p></div>
<div class="m2"><p>بگو به عاشق مستی که عشق را می پوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به گندمی اگر آدم بهشت را بفروخت</p></div>
<div class="m2"><p>تو باز خر به جوی و به نیم جو بفروش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شنو که سید سرمست وعظ می گوید</p></div>
<div class="m2"><p>بگو خطیب مخوان خطبه یک زمان خاموش</p></div></div>