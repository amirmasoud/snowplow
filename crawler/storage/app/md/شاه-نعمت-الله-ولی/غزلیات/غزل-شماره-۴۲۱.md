---
title: >-
    غزل شمارهٔ ۴۲۱
---
# غزل شمارهٔ ۴۲۱

<div class="b" id="bn1"><div class="m1"><p>جان به خلوت سرای جانان رفت</p></div>
<div class="m2"><p>دل سرمست سوی مستان رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی به ماه رو بنمود</p></div>
<div class="m2"><p>گشت پیدا و باز پنهان رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدتی زاهدی همی کردم</p></div>
<div class="m2"><p>توبه بشکستم این زمان آن رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عمر باقی که هست دریابش</p></div>
<div class="m2"><p>در پی عمر رفته نتوان رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرکه جمعیتی ز خویش نیافت</p></div>
<div class="m2"><p>ماند بیگانه و پریشان رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز حیران ز خاک برخیزد</p></div>
<div class="m2"><p>از جهان هر کسی که حیران رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله رفیق سید شد</p></div>
<div class="m2"><p>یار ما رفت گوئیا جان رفت</p></div></div>