---
title: >-
    غزل شمارهٔ ۱۰۵۰
---
# غزل شمارهٔ ۱۰۵۰

<div class="b" id="bn1"><div class="m1"><p>جام گیتی نماست در نظرم</p></div>
<div class="m2"><p>همه عالم به نور او نگرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساغر می مدام می نوشم</p></div>
<div class="m2"><p>شادی عاشقان و غم نخورم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا رند سرخوشی بینی </p></div>
<div class="m2"><p>قدمش بوسه ده بجو خبرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام می می نمایدم روشن</p></div>
<div class="m2"><p>روی ساقی مدام در نظرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یافتم ملک و صورت معنی</p></div>
<div class="m2"><p>لاجرم پادشاه بحر و برم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دو جهان می کنم فدای یکی</p></div>
<div class="m2"><p>چه کنم این رسیده از پدرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید خراباتم</p></div>
<div class="m2"><p>پیش سلطان عشق معتبرم</p></div></div>