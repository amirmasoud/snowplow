---
title: >-
    غزل شمارهٔ ۱۹۰
---
# غزل شمارهٔ ۱۹۰

<div class="b" id="bn1"><div class="m1"><p>پادشاهی چه بندگی خداست</p></div>
<div class="m2"><p>بندگی کن که پادشاه گداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از هوا بگذر و خدا را جو</p></div>
<div class="m2"><p>هرچه غیر ازویست باد هواست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر درش هر که خلوتی دارد</p></div>
<div class="m2"><p>فارغ از خانقاه هر دو سراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُرد دردش دوای درد دلست</p></div>
<div class="m2"><p>درد دل خوشتر از هزار دواست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابست و ماه خوانندش</p></div>
<div class="m2"><p>نظری کن که نور دیدهٔ ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات ساقی سر مست</p></div>
<div class="m2"><p>سید ما و خادم فُقراست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگران در پناه علم و عمل</p></div>
<div class="m2"><p>نعمت الله در پناه خداست</p></div></div>