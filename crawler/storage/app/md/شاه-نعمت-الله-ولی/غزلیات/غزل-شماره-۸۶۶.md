---
title: >-
    غزل شمارهٔ ۸۶۶
---
# غزل شمارهٔ ۸۶۶

<div class="b" id="bn1"><div class="m1"><p>برو و دلبری به دست آور</p></div>
<div class="m2"><p>به سوی عاشقان مست آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بزم عشق است عاشقانه برو</p></div>
<div class="m2"><p>ساغری از می الست آور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق و مست و رند و او باشیم</p></div>
<div class="m2"><p>شاهد مست می پرست آور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ دام فنا چه خواهی کرد</p></div>
<div class="m2"><p>شاهباز بقا به دست آور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت خلق را به جا بگذار</p></div>
<div class="m2"><p>نعمت الله را به دست آور</p></div></div>