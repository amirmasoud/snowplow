---
title: >-
    غزل شمارهٔ ۱۲۱۶
---
# غزل شمارهٔ ۱۲۱۶

<div class="b" id="bn1"><div class="m1"><p>ما از شرابخانهٔ جانانه می رسیم</p></div>
<div class="m2"><p>مستان حضرتیم و ز میخانه می رسیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از ما نشان ذوق خرابات جو که ما</p></div>
<div class="m2"><p>مستیم و لاابالی و رندانه می رسیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای عقل دور باش که رندیم و باده نوش</p></div>
<div class="m2"><p>از بزم عشق و مجلس جانانه می رسیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پروانه وار ز آتش عشقش بسوختیم</p></div>
<div class="m2"><p>شمعی گرفته ایم و به پروانه می رسیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تاجی ز ذوق بر سر و در بر قبای عشق</p></div>
<div class="m2"><p>بسته کمر به عزت و شاهانه می رسیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمست می رسیم ز میخانهٔ قدیم</p></div>
<div class="m2"><p>مخمور نیستیم که مستانه می رسیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از بندگی سید خود می رسیم باز</p></div>
<div class="m2"><p>از ملک غیب ، ببین که چه مردانه می رسیم</p></div></div>