---
title: >-
    غزل شمارهٔ ۱۰۵۴
---
# غزل شمارهٔ ۱۰۵۴

<div class="b" id="bn1"><div class="m1"><p>خوش خیالی را به خوبی دیده ام</p></div>
<div class="m2"><p>حضرت عالیجنابی دیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام آئینهٔ گیتی نما</p></div>
<div class="m2"><p>آفتابی مه نقابی دیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هفت دریا در نظر آورده ام</p></div>
<div class="m2"><p>از محیطش یک حبابی دیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ام روشن به نور روی اوست</p></div>
<div class="m2"><p>آن چنان نوری در آبی دیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر او دیگر نیاید در نظر</p></div>
<div class="m2"><p>هر چه دیدم بی حجابی دیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت و معنی عالم یافتم</p></div>
<div class="m2"><p>جسم و جان ، جام و شرابی دیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابات مغان گشتم بسی</p></div>
<div class="m2"><p>سید مست خرابی دیده ام</p></div></div>