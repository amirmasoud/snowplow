---
title: >-
    غزل شمارهٔ ۷۸۲
---
# غزل شمارهٔ ۷۸۲

<div class="b" id="bn1"><div class="m1"><p>خلق دنیا مقلد قالند</p></div>
<div class="m2"><p>اهل عقبی مقید حالند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خوشا وقت ما و آن یاران</p></div>
<div class="m2"><p>که منزه ز قال و از حالند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگران گوشمال مال خورند</p></div>
<div class="m2"><p>عاشقان گوشمال را مالند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارفان مجرد مفرد</p></div>
<div class="m2"><p>چون الف فرد و دال ابدالند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقان بلبلان معشوقند</p></div>
<div class="m2"><p>در گلستان عشق از آن نالند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالکانی که پیر توحیدند</p></div>
<div class="m2"><p>فارغ از ماه و هفته و سالند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روح محضند همچو سید ما</p></div>
<div class="m2"><p>ظن مبر کاهل دل ز صلصالند</p></div></div>