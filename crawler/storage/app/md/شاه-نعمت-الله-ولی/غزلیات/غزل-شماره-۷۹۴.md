---
title: >-
    غزل شمارهٔ ۷۹۴
---
# غزل شمارهٔ ۷۹۴

<div class="b" id="bn1"><div class="m1"><p>غرّهٔ ماه مبارک بین که غرّا کرده‌اند</p></div>
<div class="m2"><p>طرّهٔ زلف بتم از نو مطرا کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاق ابرویش نگر شکل هلالی بسته‌اند</p></div>
<div class="m2"><p>آفتابی در خیال ماه پیدا کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور چشم مردم است از دیده مردم نهان</p></div>
<div class="m2"><p>زان سبب انگشت نمای پیر و برنا کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش می‌بندد خیالش هرچه آید در نظر</p></div>
<div class="m2"><p>این نظر بنگر که با این چشم بینا کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام می در دور می‌بینم که می‌گردد مدام</p></div>
<div class="m2"><p>جاودان بزمی چنین ما را مهیا کرده‌اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صورت موجی که در دریای معنیٰ دیده‌اند</p></div>
<div class="m2"><p>عارفان تشبیه آن بر صورت ما کرده‌اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از برای نعمت الله مجلسی آراستند</p></div>
<div class="m2"><p>آنگهی آن را برای خود هویدا کرده‌اند</p></div></div>