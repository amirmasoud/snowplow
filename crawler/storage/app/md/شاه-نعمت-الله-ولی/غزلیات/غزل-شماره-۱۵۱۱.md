---
title: >-
    غزل شمارهٔ ۱۵۱۱
---
# غزل شمارهٔ ۱۵۱۱

<div class="b" id="bn1"><div class="m1"><p>سخن یار بشنو از یاری</p></div>
<div class="m2"><p>تخم نیکی بکار اگر کاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بد مکن ای عزیز نیک اندیش</p></div>
<div class="m2"><p>تا نیابی جزای خود خواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حضرت حق کجا بود راضی</p></div>
<div class="m2"><p>که دل بنده اش بیازاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیگران بار تو کشند به دوش</p></div>
<div class="m2"><p>گر کشی بار حضرت باری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر ببینی جمال او باری</p></div>
<div class="m2"><p>نقش عالم خیال پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام می را بگیر و خوش می نوش</p></div>
<div class="m2"><p>گر هوائی به ذوق ما داری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید و بنده را به هم بینی</p></div>
<div class="m2"><p>نعمت الله اگر به دست آری</p></div></div>