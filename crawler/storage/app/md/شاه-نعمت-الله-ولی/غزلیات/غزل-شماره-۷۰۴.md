---
title: >-
    غزل شمارهٔ ۷۰۴
---
# غزل شمارهٔ ۷۰۴

<div class="b" id="bn1"><div class="m1"><p>گهی عکس رخش جان می‌نماید</p></div>
<div class="m2"><p>گهی زلفش پریشان می‌نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو سنبل می‌کند بر گل مشوش</p></div>
<div class="m2"><p>سواد کفرش ایمان می‌نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه زخم است اینکه مرهم ساز جانست</p></div>
<div class="m2"><p>چه درد است اینکه درمان می‌نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه جام است اینکه می‌ریزد از او می</p></div>
<div class="m2"><p>چه جان است اینکه جانان می‌نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی دارم چو آیینه ز عشقش</p></div>
<div class="m2"><p>همه آیینه این آن می‌نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمال عشق بین و حسن معنی</p></div>
<div class="m2"><p>که چون در صورت جان می‌نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظر کن چشم سید تا ببینی</p></div>
<div class="m2"><p>که پیدا سر پنهان می‌نماید</p></div></div>