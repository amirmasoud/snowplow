---
title: >-
    غزل شمارهٔ ۷۷۲
---
# غزل شمارهٔ ۷۷۲

<div class="b" id="bn1"><div class="m1"><p>در جهنم خراب می گردد</p></div>
<div class="m2"><p>دیده ها پر ز آب می گردد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن همه تخت و ملک را بگذاشت</p></div>
<div class="m2"><p>این زمان در سراب می گردد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو سر گشته ای به گرما در</p></div>
<div class="m2"><p>روز و شب در عذاب می گردد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخت مخمور ماند میر قمر</p></div>
<div class="m2"><p>همچنان بی شراب می گردد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رند مستی که یار سید ماست</p></div>
<div class="m2"><p>نیک مست خراب می گردد</p></div></div>