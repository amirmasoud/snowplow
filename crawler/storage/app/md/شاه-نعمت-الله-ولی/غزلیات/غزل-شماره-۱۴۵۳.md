---
title: >-
    غزل شمارهٔ ۱۴۵۳
---
# غزل شمارهٔ ۱۴۵۳

<div class="b" id="bn1"><div class="m1"><p>در آ در بحر ما با ما که عین ما به ما بینی</p></div>
<div class="m2"><p>به چشم ما نظر می‌کن که تا نور خدا بینی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیا و نوش کن جامی ز دُرد درد عشق او</p></div>
<div class="m2"><p>حریف دردمندان شو که درد دل دوا بینی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مگر آئینه گم کردی که بی‌آئینه می‌گردی</p></div>
<div class="m2"><p>به بینی روی خود روشن اگر آئینه را بینی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خود بینی نخواهی دید آن ذوقی که ما داریم</p></div>
<div class="m2"><p>خدابین شو که غیر او چو بینی هوا بینی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال غیر اگر داری خیالی بس محال است آن</p></div>
<div class="m2"><p>اگر تو غیر او جوئی ندانم تا کجا بینی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر فانی شوی از خود توئی باقی جاویدان</p></div>
<div class="m2"><p>سر دار فنا بنشین که تا دار بقا بینی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غلام سید ما شو که چون بنده شوی خواجه</p></div>
<div class="m2"><p>به نور نعمت اللّه بین که تا نور خدا بینی</p></div></div>