---
title: >-
    غزل شمارهٔ ۱۴۷۱
---
# غزل شمارهٔ ۱۴۷۱

<div class="b" id="bn1"><div class="m1"><p>در هوای دنیای دون دنی</p></div>
<div class="m2"><p>روز و شب جانی به غصه می کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی خبر از یوسف مصری چرا</p></div>
<div class="m2"><p>در خیال مژده پیراهنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ریسمان حرص دنیائی مدام</p></div>
<div class="m2"><p>گرد خود چون عنکبوتی می تنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر تموز خان میری عاقبت</p></div>
<div class="m2"><p>موم گردی فی المثل گر آهنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش نشینی بر سر تاج شهان</p></div>
<div class="m2"><p>گر به خاک راه خود را افکنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حی قیومی و فارغ از هلاک</p></div>
<div class="m2"><p>در خرابات فنا گر ساکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که را بگذار و جام می بنوش</p></div>
<div class="m2"><p>نعمت الله جو اگر یار منی</p></div></div>