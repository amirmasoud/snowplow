---
title: >-
    غزل شمارهٔ ۱۰۸۰
---
# غزل شمارهٔ ۱۰۸۰

<div class="b" id="bn1"><div class="m1"><p>مست می ملامتم نیست سر سلامتم</p></div>
<div class="m2"><p>نیست سر سلامتم مست می ملامتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل نصیحتم دهد عشق غرامتم کند</p></div>
<div class="m2"><p>فارغ از آن نصیحتم ، بندهٔ این غرامتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هست ندیم بزم من ساقی مست عشق او</p></div>
<div class="m2"><p>باده خورم به شادیش نیست غم ندامتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بادهٔ صاف عاشقان دُردی درد او بود</p></div>
<div class="m2"><p>هست دوای من همین تا که شود قیامتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چهرهٔ زرد و اشک من هست گواه حال من</p></div>
<div class="m2"><p>گر تو ندانی حال من نیک ببین علامتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خرقهٔ زهد بر تنم خوش ننماید ای فقیه</p></div>
<div class="m2"><p>جامهٔ عاشقی بود راست به قد و قامتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ حضرت شهم همدم نعمت اللهم</p></div>
<div class="m2"><p>در دو جهان کجا بود خوشتر از این کرامتم</p></div></div>