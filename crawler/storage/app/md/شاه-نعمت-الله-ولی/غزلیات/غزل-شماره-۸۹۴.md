---
title: >-
    غزل شمارهٔ ۸۹۴
---
# غزل شمارهٔ ۸۹۴

<div class="b" id="bn1"><div class="m1"><p>منظور یکی ، یکی است ناظر</p></div>
<div class="m2"><p>مظهر به مظاهر است ظاهر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام است و شراب هر دو یک آب</p></div>
<div class="m2"><p>نوریست به نور خویش ساتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستیم وخراب جام بر دست</p></div>
<div class="m2"><p>داریم حضور و اوست حاضر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صد جان در عشق اگر ببازیم</p></div>
<div class="m2"><p>باشیم ز بندگیش قاصر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با باطن پاک عشق بازیم</p></div>
<div class="m2"><p>با ظاهر نازنین ظاهر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شد بر همه کائنات ناصر</p></div>
<div class="m2"><p>منصور چو رفت بر سر دار</p></div></div>