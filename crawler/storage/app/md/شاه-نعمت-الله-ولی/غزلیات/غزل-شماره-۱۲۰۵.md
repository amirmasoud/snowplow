---
title: >-
    غزل شمارهٔ ۱۲۰۵
---
# غزل شمارهٔ ۱۲۰۵

<div class="b" id="bn1"><div class="m1"><p>ظاهراً جسم و باطناً جانیم</p></div>
<div class="m2"><p>آخراً این و اولاً آنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن غیر او مگو با ما</p></div>
<div class="m2"><p>زان که ما غیر او نمی دانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وحده لاشریک له گوئیم</p></div>
<div class="m2"><p>مومن و صادق و مسلمانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسم اعظم که جامع اسماست</p></div>
<div class="m2"><p>حافظانه به ذوق می خوانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق و معشوق و عاشق خویشیم</p></div>
<div class="m2"><p>دل و دلدار و جان و جانانیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کنج دل گنجخانهٔ عشق است</p></div>
<div class="m2"><p>نقد این گنج و کُنج ویرانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید خراباتیم</p></div>
<div class="m2"><p>ساقی مست بزم رندانیم</p></div></div>