---
title: >-
    غزل شمارهٔ ۹۹۸
---
# غزل شمارهٔ ۹۹۸

<div class="b" id="bn1"><div class="m1"><p>خواجه مخمور باز ماند به مال</p></div>
<div class="m2"><p>رند سرمست و جام مالامال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواجه درویش شد چو مال نماند</p></div>
<div class="m2"><p>عرض و مالش برفت و ماند وبال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه مالش نماند او باقیست</p></div>
<div class="m2"><p>گو برو از برای مال و منال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حالیا خوش به ذوق می گردد</p></div>
<div class="m2"><p>حال ما با محول الاحوال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش غیری خیال اگر بندی</p></div>
<div class="m2"><p>نزد ما باشد آن خیال محال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام گیتی نما چو می نگرم</p></div>
<div class="m2"><p>می نماید جمال او به کمال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقیم سید است و من سرمست</p></div>
<div class="m2"><p>باده درجام همچو آب زلال</p></div></div>