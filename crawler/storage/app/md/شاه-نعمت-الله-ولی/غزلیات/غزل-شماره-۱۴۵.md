---
title: >-
    غزل شمارهٔ ۱۴۵
---
# غزل شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>قطره‌ای کو به بحر ما پیوست</p></div>
<div class="m2"><p>عین دریا بود به ما پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زندهٔ جاودان بُود به خدا</p></div>
<div class="m2"><p>روح پاکی که با خدا پیوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نکند میل خویش و بیگانه</p></div>
<div class="m2"><p>آشنا چون به آشنا پیوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دو عالم به جز یکی نبود</p></div>
<div class="m2"><p>آن یکی با یکی کجا پیوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نتواند برید پیوندش</p></div>
<div class="m2"><p>آنکه با اصل خویش واپیوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دو عالم ولی والا شد</p></div>
<div class="m2"><p>هرکه با شاه اولیا پیوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم عشق است و عاشقان مستند</p></div>
<div class="m2"><p>ذوق داری به ما بیا پیوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>لطف ساقی نگر که جام شراب</p></div>
<div class="m2"><p>می‌دهد او به دست ما پیوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت‌الله گنج سلطانی</p></div>
<div class="m2"><p>می‌کند صرف هر گدا پیوست</p></div></div>