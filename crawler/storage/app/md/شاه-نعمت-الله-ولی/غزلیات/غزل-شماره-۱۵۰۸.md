---
title: >-
    غزل شمارهٔ ۱۵۰۸
---
# غزل شمارهٔ ۱۵۰۸

<div class="b" id="bn1"><div class="m1"><p>یاریست یار یاران یاری چگونه یاری</p></div>
<div class="m2"><p>یاری که می توان گفت داریم یار غاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یاری اگر ز یاری باری رسید بر وی</p></div>
<div class="m2"><p>ما را نبود هرگز از یار خویش باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش خیال رویش بر دیده می نگاریم</p></div>
<div class="m2"><p>در چشم ما نظر کن روشن ببین نگاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز عاشقی و رندی کار دگر نداریم</p></div>
<div class="m2"><p>مستانه در خرابات مائیم و خواندگاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عین ما نظر کرد خلوتسرای خود دید</p></div>
<div class="m2"><p>بر جای خویش بنشست بگرفته خوش کناری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می نوش ساغر می می بوس دست ساقی</p></div>
<div class="m2"><p>باشد که بگذرانی رندانه روزگاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام جهان نمائی بستان ز نعمت الله</p></div>
<div class="m2"><p>تا رو به تو نماید خورشید بی غباری</p></div></div>