---
title: >-
    غزل شمارهٔ ۴۸۹
---
# غزل شمارهٔ ۴۸۹

<div class="b" id="bn1"><div class="m1"><p>جان بی جانان تن بی جان بود</p></div>
<div class="m2"><p>خوش بود جانی که با جانان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمندان را دوا درد دل است</p></div>
<div class="m2"><p>این چنین دردی مرا درمان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق را خود با سر و سامان چه کار</p></div>
<div class="m2"><p>کار عاشق بی سر و سامان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که او پابستهٔ زلف بتی است</p></div>
<div class="m2"><p>همچو مو پیوسته سرگردان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی کز عشق او کشته شود</p></div>
<div class="m2"><p>او نمیرد زنده جاویدان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق او گنجی و دل پروانه ای</p></div>
<div class="m2"><p>جای گنجش در دل ویران بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید و بنده اگر خواهی بیا</p></div>
<div class="m2"><p>نعمت الله جو که این و آن بود</p></div></div>