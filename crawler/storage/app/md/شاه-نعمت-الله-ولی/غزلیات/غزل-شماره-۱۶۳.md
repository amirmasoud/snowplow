---
title: >-
    غزل شمارهٔ ۱۶۳
---
# غزل شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>دل ما گنج و گنجخانهٔ ماست</p></div>
<div class="m2"><p>گوشهٔ جان ما خزانهٔ ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نغمهٔ بلبلان گلشن عشق</p></div>
<div class="m2"><p>صفت صوت خوش ترانهٔ ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات عشق شب تا روز</p></div>
<div class="m2"><p>نالهٔ زار عاشقانهٔ ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اندر این دامگاه عرصهٔ دل</p></div>
<div class="m2"><p>مهر شهباز عشق دانهٔ ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نشان است راه جان لیکن</p></div>
<div class="m2"><p>دل ما پیرو نشانهٔ ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر زمان خود زمانه دگر است</p></div>
<div class="m2"><p>این زمان بی گمان زمانهٔ ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دم به دم می رسد ندا کای یار</p></div>
<div class="m2"><p>نعمت الله ما یگانهٔ ماست</p></div></div>