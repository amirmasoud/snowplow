---
title: >-
    غزل شمارهٔ ۸۶۳
---
# غزل شمارهٔ ۸۶۳

<div class="b" id="bn1"><div class="m1"><p>جام گیتی نما به دست آور</p></div>
<div class="m2"><p>معنی انما به دست آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشنو و از مراد خود بگذر</p></div>
<div class="m2"><p>رو رضای خدا به دست آور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آستین بر همه جهان افشان</p></div>
<div class="m2"><p>دامن کبریا به دست آور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُرد دردش بنوش مردانه</p></div>
<div class="m2"><p>این چنین خوش دوا به دست آور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آبروئی بجو در این دریا</p></div>
<div class="m2"><p>عین ما را به ما به دست آور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زر و سیم فنا چه می جوئی</p></div>
<div class="m2"><p>نقد گنج بقا به دست آور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت این و آن به جا بگذار</p></div>
<div class="m2"><p>نعمت الله را به دست آور</p></div></div>