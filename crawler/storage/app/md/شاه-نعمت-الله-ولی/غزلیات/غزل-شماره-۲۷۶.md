---
title: >-
    غزل شمارهٔ ۲۷۶
---
# غزل شمارهٔ ۲۷۶

<div class="b" id="bn1"><div class="m1"><p>عقل از ما کنار کرد و برفت</p></div>
<div class="m2"><p>گو برو زانکه در میان عشق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق بخشد حیات جاویدان</p></div>
<div class="m2"><p>حاصل عمر جاودان عشق است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم از نور عشق شد روشن</p></div>
<div class="m2"><p>نظری کن که این و آن عشق است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل عاقل به عقل مشغول است</p></div>
<div class="m2"><p>مونس جان عاشقان عشق است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش بهشتی است مجلس سید</p></div>
<div class="m2"><p>در چنین جنتی چنان عشق است</p></div></div>