---
title: >-
    غزل شمارهٔ ۶۲
---
# غزل شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>هر که آمد بر سر دار فنا</p></div>
<div class="m2"><p>یابد از دار فنا دار بقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خدمت منصور از آن سردار شد</p></div>
<div class="m2"><p>ذوق سرداری اگر داری بیا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قل هو الله احد می خوان مدام</p></div>
<div class="m2"><p>چون موحد در خلا و در ملا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما درین دریا خوشی افتاده ایم</p></div>
<div class="m2"><p>ما ز دریائیم و دریا عین ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردمندی را که باشد درد دل</p></div>
<div class="m2"><p>دُرد درد دل بود او را دوا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر در خلوتسرای می فروش</p></div>
<div class="m2"><p>ساکنیم و فارغ از هر دو سرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدیم و بندهٔ سلطان خود</p></div>
<div class="m2"><p>ما جَمیم و جام ما گیتی نما</p></div></div>