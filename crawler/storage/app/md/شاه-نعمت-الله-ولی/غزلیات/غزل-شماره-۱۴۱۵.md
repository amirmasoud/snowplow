---
title: >-
    غزل شمارهٔ ۱۴۱۵
---
# غزل شمارهٔ ۱۴۱۵

<div class="b" id="bn1"><div class="m1"><p>ما نقش خیال تو کشیدیم به دیده</p></div>
<div class="m2"><p>خوش نقش خیالیست درین دیده بدیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نوریست که در دیدهٔ ما روی نموده</p></div>
<div class="m2"><p>نقشیست که بر پردهٔ این دیده کشیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دایم دل ما بر در جانانه مقیم است</p></div>
<div class="m2"><p>گر جان طلبد هان بسپاریم به دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این گفتهٔ مستانهٔ ما از سر ذوق است</p></div>
<div class="m2"><p>خود خوشتر ازین قول که گفته که شنیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی عیب بود هرچه به ما می رسد از غیب</p></div>
<div class="m2"><p>عیبش مکن ای دوست که از غیب رسیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش خلق عظیمی که همه خلق برانند</p></div>
<div class="m2"><p>صد رحمت حق باد بر اخلاق حمیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در بندگی سید رندان خرابات</p></div>
<div class="m2"><p>این بنده غلامیست که آن خواجه خریده</p></div></div>