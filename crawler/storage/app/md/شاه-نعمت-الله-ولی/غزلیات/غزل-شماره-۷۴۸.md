---
title: >-
    غزل شمارهٔ ۷۴۸
---
# غزل شمارهٔ ۷۴۸

<div class="b" id="bn1"><div class="m1"><p>سیدم روح اعظمش خوانند</p></div>
<div class="m2"><p>آب ارواح و آدمش خوانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روح اعظم به اعتبار بدن</p></div>
<div class="m2"><p>جام گویند و هم جمش خوانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت اسم جامع است از آن</p></div>
<div class="m2"><p>معنی جمله عالمش خوانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همدم او اگر دمی باشی</p></div>
<div class="m2"><p>حاصل عمر آن دمش خوانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم او راحت دل و جان است</p></div>
<div class="m2"><p>حیف باشد اگر غمش خوانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفان جز کلام حضرت او</p></div>
<div class="m2"><p>قصه این و آن کمش خوانند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را اگر یابند</p></div>
<div class="m2"><p>صورت اسم اعظمش خوانند</p></div></div>