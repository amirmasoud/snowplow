---
title: >-
    غزل شمارهٔ ۱۰۸
---
# غزل شمارهٔ ۱۰۸

<div class="b" id="bn1"><div class="m1"><p>با تو گویم که چیست جام و شراب</p></div>
<div class="m2"><p>به مَثل نزد ما چو آب و حباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش بیا سوی ما در این دریا</p></div>
<div class="m2"><p>عین ما را به عین ما دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج و دریا یکیست تا دانی</p></div>
<div class="m2"><p>نظری کن به چشم ما در آب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت و معنئی که می نگرم</p></div>
<div class="m2"><p>سبب است و مُسببُ الاسباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که گوید که غیر او دیدم</p></div>
<div class="m2"><p>دیده نقش خیال او در خواب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب است و ماه گویندش</p></div>
<div class="m2"><p>نور مهر است و نام او مهتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله خدا به من بخشید</p></div>
<div class="m2"><p>یافتم خوش عطائی از وهّاب</p></div></div>