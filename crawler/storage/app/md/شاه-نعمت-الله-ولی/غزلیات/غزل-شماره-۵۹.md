---
title: >-
    غزل شمارهٔ ۵۹
---
# غزل شمارهٔ ۵۹

<div class="b" id="bn1"><div class="m1"><p>همه عالم تو را و او ما را</p></div>
<div class="m2"><p>طلب او کن و بجو ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر زلفش به دست ما افتاد</p></div>
<div class="m2"><p>می نمایند مو به مو ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غرق بحریم تا نپنداری</p></div>
<div class="m2"><p>تشنه جویای آب جو ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما خراباتیان سر مستیم</p></div>
<div class="m2"><p>جام می آن تو سبو ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت الله رند سرمست است</p></div>
<div class="m2"><p>می کشد باز سو به سو ما را</p></div></div>