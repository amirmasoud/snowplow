---
title: >-
    غزل شمارهٔ ۱۴۲۴
---
# غزل شمارهٔ ۱۴۲۴

<div class="b" id="bn1"><div class="m1"><p>جنت المأوای ما خلوتسرای میکده</p></div>
<div class="m2"><p>جان سرمست خراباتی فدای میکده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوای میکده بر باد خواهم داد دل</p></div>
<div class="m2"><p>هر که را جانی است باشد در هوای میکده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدم میر خراباتیم و با رندان حریف</p></div>
<div class="m2"><p>پادشاه عالمیم اما گدای میکده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق و مستم برو ای عاقل خلوت نشین</p></div>
<div class="m2"><p>صومعه هرگز ندارم من به جای میکده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاف درمان است ما را دُرد درد عشق او</p></div>
<div class="m2"><p>هر کرا دردیست باشد در هوای میکده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سر بازار سودا مایه و سود دکان</p></div>
<div class="m2"><p>هرچه حاصل کرده ام دادم برای میکده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نالهٔ دلسوز سید مطرب عشاق ماست</p></div>
<div class="m2"><p>می نوازد ساز جانها از نوای میکده</p></div></div>