---
title: >-
    غزل شمارهٔ ۴۲۶
---
# غزل شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>نعمت الله جان به جانان داد و رفت</p></div>
<div class="m2"><p>بر در میخانه مست افتاد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابش از قمر بسته نقاب</p></div>
<div class="m2"><p>آن نقاب از روی خود بگشاد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بود استادی به شاگردان بسی</p></div>
<div class="m2"><p>کرد شاگردان همه استاد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات مغان مست و خراب</p></div>
<div class="m2"><p>سر به پای خم می بنهاد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>او خلیفه بود در بغداد تن</p></div>
<div class="m2"><p>رخت را بربست از بغداد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفانه در جهان صد سال بود</p></div>
<div class="m2"><p>نی چو غافل داد جان بر باد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما بود ظاهر شد نهان</p></div>
<div class="m2"><p>بندگان را جمله کرد آزاد و رفت</p></div></div>