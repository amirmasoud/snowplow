---
title: >-
    غزل شمارهٔ ۱۷۳
---
# غزل شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>ساقی سرمست رندان میر بی همتای ماست</p></div>
<div class="m2"><p>گوشهٔ میخانهٔ او جنت المأوای ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما درین دریای بی پایان خوشی افتاده ایم</p></div>
<div class="m2"><p>آبروی عالمی ای یار از دریای ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما روشن به نور روی او باشد مدام</p></div>
<div class="m2"><p>این چنین نور خوشی در دیدهٔ بینای ماست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات مغان مستیم و با رندان حریف</p></div>
<div class="m2"><p>ذوق اگرداری بیا آنجا که آنجا جای ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتهٔ ما مرده ای گر بشنود زنده شود</p></div>
<div class="m2"><p>گوئیا آب حیات از نطق جان افزای ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم از بالای تو جانا بلائی می کشم</p></div>
<div class="m2"><p>گفت خوش باشد بلای تو که از بالای ماست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سر ما عشق زلفش دیگ سودا می پزد</p></div>
<div class="m2"><p>مایهٔ سودای خلقی سرخوش از سودای ماست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسم اعظم در همه عالم ظهور نور او است</p></div>
<div class="m2"><p>جامع ذات و صفاتش این دل دانای ماست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از دل و جان بنده ای از بندگان حضرتیم</p></div>
<div class="m2"><p>نعمت الله در دو عالم سید یکتای ماست</p></div></div>