---
title: >-
    غزل شمارهٔ ۱۴۰۰
---
# غزل شمارهٔ ۱۴۰۰

<div class="b" id="bn1"><div class="m1"><p>درآمد ترک سرمستی که غارت می کند خانه</p></div>
<div class="m2"><p>چنان مستست کز مستی نداند خویش بیگانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراباتست و من سرمست و ساقی جام می بر دست</p></div>
<div class="m2"><p>بهشت جاودان ما بود این کنج میخانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز عشقش آتشی افروخت جان عاشقان را سوخت</p></div>
<div class="m2"><p>وجود ما و عشق او مثال شمع و پروانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برو ای عقل سرگردان که من مستم تو مخموری</p></div>
<div class="m2"><p>سخن از غیر میگوئی مرا با غیر پروانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درین بزم ملوکانه نشسته جان و جانانه</p></div>
<div class="m2"><p>نشسته جان و جانانه در این بزم ملوکانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر جانست حیرانست و گر دل والهٔ عشقست</p></div>
<div class="m2"><p>اگر علمست نادانست و گر عقلست دیوانه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بیا ای مطرب عشاق و ساز عاشقان بنواز</p></div>
<div class="m2"><p>حریف نعمت الله شو بخوان این قول مستانه</p></div></div>