---
title: >-
    غزل شمارهٔ ۱۰۹۸
---
# غزل شمارهٔ ۱۰۹۸

<div class="b" id="bn1"><div class="m1"><p>ما اگر شاه اگر گدا باشیم</p></div>
<div class="m2"><p>در همه حال با خدا باشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله اسما به ذوق می خوانیم</p></div>
<div class="m2"><p>از مسما کجا جدا باشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج بحریم و عین ما آبست</p></div>
<div class="m2"><p>ما درین بحر آشنا باشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دردمندیم و درد می نوشیم</p></div>
<div class="m2"><p>دائما همدم دوا باشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر او دیگری نمی دانیم</p></div>
<div class="m2"><p>عاشق غیر او کجا باشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات رند و سرمستیم</p></div>
<div class="m2"><p>این چنین بوده ایم تا باشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما چو باشیم بندهٔ سید</p></div>
<div class="m2"><p>بندهٔ دیگری چرا باشیم</p></div></div>