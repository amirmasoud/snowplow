---
title: >-
    غزل شمارهٔ ۵۲۶
---
# غزل شمارهٔ ۵۲۶

<div class="b" id="bn1"><div class="m1"><p>غرق دریاییم و ما را موج دریا می‌کشد</p></div>
<div class="m2"><p>آبرو می‌بخشد و ما را به مأوا می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق هرجایی است ما هم در پی او می‌رویم</p></div>
<div class="m2"><p>او به هرجا می‌رود ما را به هرجا می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ازل بالانشین بودیم گویا تا ابد</p></div>
<div class="m2"><p>جذبهٔ او می‌کشد ما را به بالا می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر گیتی‌نما پر می به رندان می‌دهد</p></div>
<div class="m2"><p>خاطر مستانهٔ رندان ما را می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سر زلفش در افتادیم و سودایی شدیم</p></div>
<div class="m2"><p>دل به دست زلف او دادیم و دریا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک پایش توتیای دیدهٔ بینای ماست</p></div>
<div class="m2"><p>از برای روشنی در چشم بینا می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کش خود می‌کشد ما را به صد تعظیم و ناز</p></div>
<div class="m2"><p>این کشاکش خوش بود چون سید ما می‌کشد</p></div></div>