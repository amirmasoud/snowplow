---
title: >-
    غزل شمارهٔ ۱۲۴۸
---
# غزل شمارهٔ ۱۲۴۸

<div class="b" id="bn1"><div class="m1"><p>نور او در دیدهٔ بینا ببین</p></div>
<div class="m2"><p>آن یکی در هر یکی پیدا ببین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبی از جام حبابی نوش کن</p></div>
<div class="m2"><p>عین ما را هم به عین ما ببین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که می گوئی که آنجا بینمش</p></div>
<div class="m2"><p>دیده را بگشا بیا اینجا ببین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر لب دریاچه می گردی مدام</p></div>
<div class="m2"><p>غرقهٔ دریا شو و دریا ببین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آینه گر صد ببینی ور هزار</p></div>
<div class="m2"><p>در همه یکتای بی همتا ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرم سودای زلف او فتاد</p></div>
<div class="m2"><p>حال این سودائی شیدا ببین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را اگر خواهی بیا</p></div>
<div class="m2"><p>در خرابات مغان ما را ببین</p></div></div>