---
title: >-
    غزل شمارهٔ ۱۴۹۶
---
# غزل شمارهٔ ۱۴۹۶

<div class="b" id="bn1"><div class="m1"><p>زر به باران ده که تا جان را بری</p></div>
<div class="m2"><p>ور زرت باشد بشو از جان بری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطنت خواهی سر و زر را بباز</p></div>
<div class="m2"><p>سلطنت خود نیست کار سرسری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر از یاساق و راه شرع گیر</p></div>
<div class="m2"><p>گر به ایمان تابع پیغمبری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای همت بر سر دنیا بکوب</p></div>
<div class="m2"><p>تا بر آری دست و پای سروری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نو عروسانند فکر بکر من</p></div>
<div class="m2"><p>خوشترند از لعبتان بربری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر بیابی حبه ای از قند ما</p></div>
<div class="m2"><p>گنج قارون را به یک جو نشمری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو سید تخم نیکی را بکار</p></div>
<div class="m2"><p>گر همی خواهی که از خود برخوری</p></div></div>