---
title: >-
    غزل شمارهٔ ۸۷۱
---
# غزل شمارهٔ ۸۷۱

<div class="b" id="bn1"><div class="m1"><p>گرفته عشق او دستم دگر بار</p></div>
<div class="m2"><p>ز دست عقل وارستم دگر بار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به صد دستان گرفتم دست ساقی</p></div>
<div class="m2"><p>بزن دستی که زان رستم دگر بار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به عشق چشم مست می فروشش</p></div>
<div class="m2"><p>به حمدلله که سرمستم دگر بار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ببستم بر میان زنار زلفش</p></div>
<div class="m2"><p>چو زلفش توبه بشکستم دگر بار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دانستم که غیر او دگر نیست</p></div>
<div class="m2"><p>ز غیرت غیر نپرستم دگر بار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا گر هست هستی هستی اوست</p></div>
<div class="m2"><p>ز خود فانی به او هستم دگر بار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روان برخواستم از یار و اغیار</p></div>
<div class="m2"><p>خوشی با یار بنشستم دگر بار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به سرمستی لبش را بوسه دادم</p></div>
<div class="m2"><p>لب خود را از آن خستم دگر بار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به کنج صومعه در بند بودم</p></div>
<div class="m2"><p>شکستم بند را جستم دگر بار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز خود بگسستم و پیوست گشتم</p></div>
<div class="m2"><p>از آن گویم که پیوستم دگر بار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حریف سید سرمست اویم</p></div>
<div class="m2"><p>ز جام عشق او مستم دگر بار</p></div></div>