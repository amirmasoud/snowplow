---
title: >-
    غزل شمارهٔ ۱۳۲۳
---
# غزل شمارهٔ ۱۳۲۳

<div class="b" id="bn1"><div class="m1"><p>آبرو جوئی بیا از ما بجو</p></div>
<div class="m2"><p>دل به دریا ده بیا دریا بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو جهان بگذار تا یکتا شوی</p></div>
<div class="m2"><p>آنگهی یکتای بی همتا بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رند مستی گر همی خواهی بیا</p></div>
<div class="m2"><p>در خرابات مغان ما را بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده بگشا نور چشم ما نگر</p></div>
<div class="m2"><p>عین ما در دیدهٔ بینا بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به دست زلف او دادیم دل</p></div>
<div class="m2"><p>در سر ما مایهٔ سودا بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عدم ما را حضوری بس خوش است</p></div>
<div class="m2"><p>گر حضوری بایدت آنجا بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه می بینی از او دارد نصیب</p></div>
<div class="m2"><p>نعمت الله از همه اشیا بجو</p></div></div>