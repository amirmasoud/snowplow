---
title: >-
    غزل شمارهٔ ۱۱۰۴
---
# غزل شمارهٔ ۱۱۰۴

<div class="b" id="bn1"><div class="m1"><p>من به جان دوستدار رندانم</p></div>
<div class="m2"><p>عاشق روی باده نوشانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به جز از عاشقی و می خواری</p></div>
<div class="m2"><p>هیچ کار دگر نمی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوبتی توبه کردم از باده</p></div>
<div class="m2"><p>مدتی شد کز آن پشیمانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شعر مستانه ای همی گویم</p></div>
<div class="m2"><p>غزلی عاشقانه می خوانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دُرد دردش مدام می نوشم</p></div>
<div class="m2"><p>یار و همدرد دردمندانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندهٔ حضرت خداوندم</p></div>
<div class="m2"><p>پادشاه هزار سلطانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید مجلس خراباتم</p></div>
<div class="m2"><p>ساقی بزم می پرستانم</p></div></div>