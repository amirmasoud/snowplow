---
title: >-
    غزل شمارهٔ ۴۲۵
---
# غزل شمارهٔ ۴۲۵

<div class="b" id="bn1"><div class="m1"><p>نعمت الله جان به جانان داد و رفت</p></div>
<div class="m2"><p>بر در میخانه مست افتاد و رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سید ما بندهٔ خاص خداست</p></div>
<div class="m2"><p>گوییا شد از جهان آزاد و رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قرب صد سالی غم هجران کشید</p></div>
<div class="m2"><p>عاقبت از وصل شد دلشاد و رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا نپنداری که او معدوم گشت</p></div>
<div class="m2"><p>یا بداد او عمر خود بر باد و رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برقعه ای از جسم و جان بربسته بود</p></div>
<div class="m2"><p>بند برقع را ز رو بگشاد و رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان مست و خراب</p></div>
<div class="m2"><p>سر به پای خم می بنهاد و رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون ندای ارجعی از حق شنود</p></div>
<div class="m2"><p>زنده دل از عشق او جان داد و رفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>کل شیئی هالک الا وجهه</p></div>
<div class="m2"><p>خواند بر دنیای بی بنیاد و رفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله دوستان یادش کنید</p></div>
<div class="m2"><p>تا نگوئی رفت او از یاد و رفت</p></div></div>