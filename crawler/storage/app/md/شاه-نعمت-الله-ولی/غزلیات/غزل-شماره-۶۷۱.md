---
title: >-
    غزل شمارهٔ ۶۷۱
---
# غزل شمارهٔ ۶۷۱

<div class="b" id="bn1"><div class="m1"><p>جیب شب آفتاب چون بگشود</p></div>
<div class="m2"><p>از گریبان روز رو بنمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب امکان خیال بود نماند</p></div>
<div class="m2"><p>هست روز و وجود خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر او نیست ور تو گوئی هست</p></div>
<div class="m2"><p>او به خود دیگران به او موجود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل چون شب برفت و روز آمد</p></div>
<div class="m2"><p>خاطر ما از این و آن آسود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک حقیقت که آدمی خوانند</p></div>
<div class="m2"><p>گه ایاز او به نام و گه محمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی را به رقص آورده</p></div>
<div class="m2"><p>قول مستانه ای که او فرمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله گرد نقطهٔ دل</p></div>
<div class="m2"><p>همچو پرگار دایره پیمود</p></div></div>