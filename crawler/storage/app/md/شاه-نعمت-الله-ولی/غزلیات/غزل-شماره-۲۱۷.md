---
title: >-
    غزل شمارهٔ ۲۱۷
---
# غزل شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>دامن دلبر اگر آری به دست</p></div>
<div class="m2"><p>نیک باشد ور نیاری آن به دست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خراباتی و رند و عاشقیم</p></div>
<div class="m2"><p>چشم مستش توبهٔ ما را شکست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما بسته خیالش در نظر</p></div>
<div class="m2"><p>نور دیده خوش به جا دارد نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاهبازی رفته بود از دست ما</p></div>
<div class="m2"><p>باز آمد شاهباز ما به دست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حق پرست کاملی دانی که کیست</p></div>
<div class="m2"><p>آنکه او از خودپرستی باز رَست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقلان در نیست و هست افتاده اند</p></div>
<div class="m2"><p>عشقبازان فارغند از نیست و هست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابات مغان دیگر مجو</p></div>
<div class="m2"><p>همچو سید نعمت الله رند و مست</p></div></div>