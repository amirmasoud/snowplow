---
title: >-
    غزل شمارهٔ ۵۲۵
---
# غزل شمارهٔ ۵۲۵

<div class="b" id="bn1"><div class="m1"><p>خاطر ما سوی دریا می‌کشد</p></div>
<div class="m2"><p>گوییا ما را به مأوا می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج دریاییم و دریا عین ما</p></div>
<div class="m2"><p>می‌برد ما را به هرجا می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جذبهٔ او می‌کشد ما را به خود</p></div>
<div class="m2"><p>خوش بود چون حق تعالی می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کشاکش عالمی آورده است</p></div>
<div class="m2"><p>نی من سرگشته تنها می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می‌کشد نقش خیالی دم به دم</p></div>
<div class="m2"><p>هم خطی بر لوح اشیا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما بلای عشق او خوش می‌کشیم</p></div>
<div class="m2"><p>کار ما در عشق بالا می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا نماید نعمت الله را به ما</p></div>
<div class="m2"><p>اینچنین نعمت بر ما می‌کشد</p></div></div>