---
title: >-
    غزل شمارهٔ ۴۷۶
---
# غزل شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>دولتی خوش خدا به ما بخشید</p></div>
<div class="m2"><p>جام گیتی نما به ما بخشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کرم پادشاه ما بنگر</p></div>
<div class="m2"><p>پادشاهی به این گدا بخشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج اسما به ما عطا فرمود</p></div>
<div class="m2"><p>گر به اصحاب دو سرا بخشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما از او غیر از او نمی‌جستیم</p></div>
<div class="m2"><p>آشنا یافت خویش را بخشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دُرد دردش به ذوق نوشیدیم</p></div>
<div class="m2"><p>لاجرم این چنین دوا بخشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون که سید شفیع خود کردیم</p></div>
<div class="m2"><p>نعمت‌الله را به ما بخشید</p></div></div>