---
title: >-
    غزل شمارهٔ ۱۵۰
---
# غزل شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>تا مرا عین عشق مفهوم است</p></div>
<div class="m2"><p>سر علمم به عشق معلوم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا رموز وجود شد مفهوم</p></div>
<div class="m2"><p>هر وجودی که هست مفهومست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خادم خلوت دلم آری</p></div>
<div class="m2"><p>بنگر آن خادمی که مخدومست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شمع روشن ضمیر مجلس ماست</p></div>
<div class="m2"><p>دل پروانه ای که چون موم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز سرمست شد دل مخمور</p></div>
<div class="m2"><p>لیکن از خمر غیر معصوم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قسمتم عشق بود روز ازل</p></div>
<div class="m2"><p>آری خوش قسمتی که مقسومست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون که شد سید از خودی فانی</p></div>
<div class="m2"><p>نزد عشاق حی قیوم است</p></div></div>