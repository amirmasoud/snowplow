---
title: >-
    غزل شمارهٔ ۸۷۹
---
# غزل شمارهٔ ۸۷۹

<div class="b" id="bn1"><div class="m1"><p>مو نمی گنجد میان ما و یار</p></div>
<div class="m2"><p>عشق در جانست و جانان در کنار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رند و قلاشیم ای زاهد برو</p></div>
<div class="m2"><p>لا ابالی ایم ساقی می بیار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق و مستیم و با رندان حریف</p></div>
<div class="m2"><p>عاقل هشیار را با ما چه کار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق عاشق تا به کی جوئی ز عقل</p></div>
<div class="m2"><p>روی گل را چند می خاری به خار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود چه داند عقل ذوق عاشقی</p></div>
<div class="m2"><p>خود که باشد او و چون او صدهزار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرم سودا و جام می به دست</p></div>
<div class="m2"><p>بر یمینم عشق و ساقی بر یسار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد دل دارم اگر نالم بسوز</p></div>
<div class="m2"><p>ناله ام بشنو ولی معذور دار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در هزار آئینه بنماید یکی</p></div>
<div class="m2"><p>آن یکی در هر یکی خوش می شمار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در خرابات مغان دیگر مجو</p></div>
<div class="m2"><p>همچو سید دردمند و درد خوار</p></div></div>