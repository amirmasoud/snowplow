---
title: >-
    غزل شمارهٔ ۶۹۷
---
# غزل شمارهٔ ۶۹۷

<div class="b" id="bn1"><div class="m1"><p>چشمت به تو نور خوش نماید</p></div>
<div class="m2"><p>گوش تو در سخن گشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلشن ما زبان بلبل</p></div>
<div class="m2"><p>هر لحظه تو را همی سراید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست تو بیان کند یدالله</p></div>
<div class="m2"><p>گر زانکه یدش به دستت آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پائی که به قدرتش بپایست</p></div>
<div class="m2"><p>بی قدرت او به پا نپاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی جود وجود سید ما</p></div>
<div class="m2"><p>خود بود وجود ما نشاید</p></div></div>