---
title: >-
    غزل شمارهٔ ۴۴
---
# غزل شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>خوش چشمهٔ آبی است روان در نظر ما</p></div>
<div class="m2"><p>سیراب شده خاک در از رهگذر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما آب حیاتیم روانیم به هر سو</p></div>
<div class="m2"><p>سرسبزی باغ خضر است در نظر ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>میخانهٔ ما قبلهٔ حاجات جهانست</p></div>
<div class="m2"><p>شاید که جهانی به سر آید به در ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوریست که در دیدهٔ مردم شده پنهان</p></div>
<div class="m2"><p>روشن بتوان دید ولی در نظر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستیم و نداریم خبر از همه عالم</p></div>
<div class="m2"><p>اینست خبر هر که بپرسد خبر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در آینهٔ دیدهٔ سید نظری کن</p></div>
<div class="m2"><p>تا باز نماید به تو روشن بصر ما</p></div></div>