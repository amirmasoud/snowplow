---
title: >-
    غزل شمارهٔ ۳۸۷
---
# غزل شمارهٔ ۳۸۷

<div class="b" id="bn1"><div class="m1"><p>او با تو ، تو را از او خبر نیست</p></div>
<div class="m2"><p>جز عین یکی ، یکی دگر نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقشی که خیال غیر دارد</p></div>
<div class="m2"><p>صاحبنظرش بر آن نظر نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون صورت دوست معنی ماست</p></div>
<div class="m2"><p>بس معتبر است و مختصر نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در بحر گهر بود ولیکن</p></div>
<div class="m2"><p>چون دُر یتیم ما گهر نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در کوچهٔ ما بیا و بنشین</p></div>
<div class="m2"><p>زان کوچه مرو که ره به در نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما خرقهٔ خویش پاک شستیم</p></div>
<div class="m2"><p>از هستی ما بر او اثر نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیر البشر است سید ما</p></div>
<div class="m2"><p>گویند بشر ولی بشر نیست</p></div></div>