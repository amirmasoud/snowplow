---
title: >-
    غزل شمارهٔ ۴۵۳
---
# غزل شمارهٔ ۴۵۳

<div class="b" id="bn1"><div class="m1"><p>هرگز نبود عاشقی و راه سلامت</p></div>
<div class="m2"><p>رندان نگریزند ز مستان به ملامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو میر خراباتی و من مست خرابم</p></div>
<div class="m2"><p>رندانه درین هفته بیابیم به سلامت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر در قدمت بازم و پای تو ببوسم</p></div>
<div class="m2"><p>دست من و دامان تو تا روز قیامت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خاک درت هر که نشنید بتوان یافت</p></div>
<div class="m2"><p>در صدر خرابات به صد عجز و کرامت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر دل نفسی نقش خیال دیگری دید</p></div>
<div class="m2"><p>جان پیشکشت می کنم اینک به غرامت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خال نهی دانه و از زلف کشی دام</p></div>
<div class="m2"><p>مرغ دل خلقی همه افتاده به دامت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می نوش کن ای سید رندان خرابات</p></div>
<div class="m2"><p>شادی حریفان که جهان باد به کامت</p></div></div>