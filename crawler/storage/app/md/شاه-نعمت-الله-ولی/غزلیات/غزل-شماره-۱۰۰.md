---
title: >-
    غزل شمارهٔ ۱۰۰
---
# غزل شمارهٔ ۱۰۰

<div class="b" id="bn1"><div class="m1"><p>چون بر آمد از دل جام آفتاب </p></div>
<div class="m2"><p>نزد ما هر دو یکی شد برف و آب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجمع البحرین جامست و شراب</p></div>
<div class="m2"><p>این شراب و جام آبست و حباب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام می بردست می گردم به ذوق</p></div>
<div class="m2"><p>در خرابات مغان مست و خراب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس نبیند از هزاران زهد و علم</p></div>
<div class="m2"><p>آنچه من دیدم ز یک جام شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لوح محفوظ است ما را در نظر</p></div>
<div class="m2"><p>خود که دارد این چنین ام الکتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اصل گُل آب است و فرع آب گل</p></div>
<div class="m2"><p>اصل و فرعش دوست دارم چون گلاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نیم هشیار بگذر از سرم</p></div>
<div class="m2"><p>چون ندارم عقل بگذار احتساب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>غرق دریائی و تشنه ای عجب</p></div>
<div class="m2"><p>بر سر آبی و پنداری سراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باده می نوشم مدام از جام عشق</p></div>
<div class="m2"><p>در حضور سید خود بی حساب</p></div></div>