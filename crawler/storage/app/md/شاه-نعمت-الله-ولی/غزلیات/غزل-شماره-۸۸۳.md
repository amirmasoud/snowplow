---
title: >-
    غزل شمارهٔ ۸۸۳
---
# غزل شمارهٔ ۸۸۳

<div class="b" id="bn1"><div class="m1"><p>صبحدم شد آفتابی آشکار</p></div>
<div class="m2"><p>عالمی در رقص آمد ذره وار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر او نقش خیالی بیش نیست</p></div>
<div class="m2"><p>عقل گو نقش خیالی می نگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کناری گیری از خود در میان</p></div>
<div class="m2"><p>یار خود بینی گرفته در کنار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق بازی کار بیکاران بود</p></div>
<div class="m2"><p>عاقلش با کار بی کاران چه کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب رو می نوش از جام حباب</p></div>
<div class="m2"><p>آن یکی در هر یکی خوش می شمار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>صدهزار آئینه پیش خود بنه</p></div>
<div class="m2"><p>معنیش یک بین به صورت صد هزار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله ما و سید آفتاب</p></div>
<div class="m2"><p>شمس با ماه است و ماهش پرده دار</p></div></div>