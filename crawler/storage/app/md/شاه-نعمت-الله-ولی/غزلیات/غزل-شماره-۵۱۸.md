---
title: >-
    غزل شمارهٔ ۵۱۸
---
# غزل شمارهٔ ۵۱۸

<div class="b" id="bn1"><div class="m1"><p>خوش بود گر این دوئی یکتا شود</p></div>
<div class="m2"><p>آفتاب حسن او پیدا شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غیر نور او نیاید در نظر</p></div>
<div class="m2"><p>چشم ما از نور او بینا شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آ چشم ما به هر سو شد روان</p></div>
<div class="m2"><p>ِآید آن روزی که آن دریا شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بحر می گوید به آواز بلند</p></div>
<div class="m2"><p>آنکه او از ماست با مأوا شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عارفی کَز هر دو عالم بگذرد</p></div>
<div class="m2"><p>بر در یکتای بی همتا شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان رندی که شد</p></div>
<div class="m2"><p>عاقبت سر دفتر غوغا شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که بوسد آن لب شیرین او</p></div>
<div class="m2"><p>همچو سید لاجرم گویا شود</p></div></div>