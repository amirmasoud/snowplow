---
title: >-
    غزل شمارهٔ ۱۵۳۱
---
# غزل شمارهٔ ۱۵۳۱

<div class="b" id="bn1"><div class="m1"><p>مجلس عشق است و ما سرمست می</p></div>
<div class="m2"><p>یار با ساقی و ما مهمان وی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز با میر خراباتم حریف</p></div>
<div class="m2"><p>خلوتی خالی و جز ما هیچ شی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشتهٔ عشقم از آنم زنده دل</p></div>
<div class="m2"><p>مردهٔ دردم از آنم گشته حی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بیابی عاشقی گو الصلا</p></div>
<div class="m2"><p>ور ببینی عاقلی گو دو رهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق ما را رو به میخانه نمود</p></div>
<div class="m2"><p>جان فدای این دلیل نیک پی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی سرمست و خماری کریم</p></div>
<div class="m2"><p>تو چنین مخمور باشی تا به کی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما را نگر کز عشق او</p></div>
<div class="m2"><p>نامهٔ هستی به مستی کرده طی</p></div></div>