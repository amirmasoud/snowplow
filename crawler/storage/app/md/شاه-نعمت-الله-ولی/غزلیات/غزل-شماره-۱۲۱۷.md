---
title: >-
    غزل شمارهٔ ۱۲۱۷
---
# غزل شمارهٔ ۱۲۱۷

<div class="b" id="bn1"><div class="m1"><p>ما گدایان حضرت شاهیم</p></div>
<div class="m2"><p>پرده داران خاص اللهیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده نوشان مجلس عشقیم</p></div>
<div class="m2"><p>ره نشینان خاک این راهیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه از خود خبر نمی داریم</p></div>
<div class="m2"><p>به خدا کز خدای آگاهیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ضمیر منیر دل مهریم</p></div>
<div class="m2"><p>بر سپهر وجود جان ماهیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاه در مصر تن عزیز خودیم</p></div>
<div class="m2"><p>که چو یوسف فتاده درچاهیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کام دل در کنار جان داریم</p></div>
<div class="m2"><p>ایمن از آرزوی دلخواهیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ ذاکران توحیدیم</p></div>
<div class="m2"><p>سید ملک نعمت اللهیم</p></div></div>