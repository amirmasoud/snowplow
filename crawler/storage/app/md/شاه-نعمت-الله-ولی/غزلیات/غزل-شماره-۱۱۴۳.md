---
title: >-
    غزل شمارهٔ ۱۱۴۳
---
# غزل شمارهٔ ۱۱۴۳

<div class="b" id="bn1"><div class="m1"><p>رخت بربستیم و دل برداشتیم</p></div>
<div class="m2"><p>آمده نا آمده پنداشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خیالی می نماید کائنات</p></div>
<div class="m2"><p>بود و نابودش یکی انگاشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در زمین بوستان دوستان</p></div>
<div class="m2"><p>سالها تخم محبت کاشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی بستیم نقشی در خیال</p></div>
<div class="m2"><p>بر سواد دیده اش بنگاشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقبت دیدیم جز نقشی نبود</p></div>
<div class="m2"><p>از خیال آن نقش را بگذاشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات فنا ساکن شدیم</p></div>
<div class="m2"><p>عاشقانه چاه جاه انباشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا خلیل الله آمد در کنار</p></div>
<div class="m2"><p>نعمت الله از میان برداشتیم</p></div></div>