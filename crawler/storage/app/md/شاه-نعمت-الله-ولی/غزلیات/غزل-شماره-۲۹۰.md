---
title: >-
    غزل شمارهٔ ۲۹۰
---
# غزل شمارهٔ ۲۹۰

<div class="b" id="bn1"><div class="m1"><p>درد ار داری دوا همان است</p></div>
<div class="m2"><p>درد ار نوشی شفا همان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با جام می ار دمی برآری</p></div>
<div class="m2"><p>دانی که حیات ما همان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عمریست که مبتلای دردیم</p></div>
<div class="m2"><p>خود راحت مبتلا همان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فانی از خود فنا همین است</p></div>
<div class="m2"><p>باقی به خدا بقا همان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آینه همه نظر کن</p></div>
<div class="m2"><p>می بین همه را لقا همان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما جام جهان نمای عشقیم</p></div>
<div class="m2"><p>این جام جهان نما همان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر صورت سیدم دگر شد</p></div>
<div class="m2"><p>اما به خدا خدا همان است</p></div></div>