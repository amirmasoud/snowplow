---
title: >-
    غزل شمارهٔ ۷۴۳
---
# غزل شمارهٔ ۷۴۳

<div class="b" id="bn1"><div class="m1"><p>هر در که به روی ما گشایند</p></div>
<div class="m2"><p>حسن دیگری به ما نمایند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر دم به پیالهٔ شرابی</p></div>
<div class="m2"><p>ذوق دگرم همی فزایند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در میکده دلبران عیار</p></div>
<div class="m2"><p>صد دل به کرشمه ای ربایند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رندان مستند و لاابالی</p></div>
<div class="m2"><p>مستانه سرود می سرایند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیدیم جمال ماهرویان</p></div>
<div class="m2"><p>آئینهٔ حضرت خدایند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بینند همه که ما چه دیدیم</p></div>
<div class="m2"><p>گر پرده ز روی بر گشایند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزمی سازند هر زمانی</p></div>
<div class="m2"><p>تا سید و بنده خوش برآیند</p></div></div>