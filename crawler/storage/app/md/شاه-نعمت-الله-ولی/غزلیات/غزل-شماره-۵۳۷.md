---
title: >-
    غزل شمارهٔ ۵۳۷
---
# غزل شمارهٔ ۵۳۷

<div class="b" id="bn1"><div class="m1"><p>هرچه بخشد خدا به ما بخشد</p></div>
<div class="m2"><p>پادشاهی به هر گدا بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر رحمت به ما روان سازد</p></div>
<div class="m2"><p>آبروئی به عین ما بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُردی درد عشق او مینوش</p></div>
<div class="m2"><p>تا به لطفش تو را دوا بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می به بیگانه کی دهد ساقی</p></div>
<div class="m2"><p>ساغر می به آشنا بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات اگر فنا گردی</p></div>
<div class="m2"><p>از حیاتش تو را بقا بخشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بندگی کن که حضرت سلطان</p></div>
<div class="m2"><p>هرچه خواهی از او تو را بخشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بینوایان نوا از او یابند</p></div>
<div class="m2"><p>نعمت الله به بینوا بخشد</p></div></div>