---
title: >-
    غزل شمارهٔ ۵۰۶
---
# غزل شمارهٔ ۵۰۶

<div class="b" id="bn1"><div class="m1"><p>چشم بینائی که بر او اوفتد</p></div>
<div class="m2"><p>سر نهد بر پاش و بر رو اوفتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که بر خاک درش افتد چو ما</p></div>
<div class="m2"><p>مسکن او جای نیکو اوفتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابست او و عالم سایه بان</p></div>
<div class="m2"><p>نور او بر ما و بر تو اوفتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل به دریا داده ایم و می رویم</p></div>
<div class="m2"><p>آخر این کار تا چو اوفتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رنگ و بوی اوست رنگ و بوی ما</p></div>
<div class="m2"><p>گر سخن با رنگ و با بو اوفتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سر کوی خرابات مغان</p></div>
<div class="m2"><p>گر رسد مستی به یلهو اوفتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله ساقی سرمست ماست</p></div>
<div class="m2"><p>برنخیزد هر که با او اوفتد</p></div></div>