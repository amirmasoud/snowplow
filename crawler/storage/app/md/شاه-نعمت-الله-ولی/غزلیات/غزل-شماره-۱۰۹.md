---
title: >-
    غزل شمارهٔ ۱۰۹
---
# غزل شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>چیست عالم سایه و آن آفتاب</p></div>
<div class="m2"><p>تن بود چون سایه و جان آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور عالم شمس دینش خوانده اند</p></div>
<div class="m2"><p>سِر این دریاب و می خوان آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از برای نزل و بزم عاشقان</p></div>
<div class="m2"><p>جام زرین است بر خوان آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب حسن او عالم گرفت</p></div>
<div class="m2"><p>تا قیامت باد تابان آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور روی نعمت الله دیده ام</p></div>
<div class="m2"><p>می نماید در نظر آن آفتاب</p></div></div>