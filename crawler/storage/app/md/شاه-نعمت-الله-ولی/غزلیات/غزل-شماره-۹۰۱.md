---
title: >-
    غزل شمارهٔ ۹۰۱
---
# غزل شمارهٔ ۹۰۱

<div class="b" id="bn1"><div class="m1"><p>شاهبازی درآمد از در باز</p></div>
<div class="m2"><p>خیز و در پای او تو سر در باز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو ای عقل چون درآمد عشق</p></div>
<div class="m2"><p>خانهٔ خویشتن به او پرداز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به میخانه می کشد دیگر</p></div>
<div class="m2"><p>مرغ جان می کند روان پرواز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام جم خوش بود به ما همدم</p></div>
<div class="m2"><p>نی و نائی به همدگر دمساز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساز و سازنده هر دو می باید</p></div>
<div class="m2"><p>ورنه بی ساز کی نوازد ساز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست رازی میان دیده و دل</p></div>
<div class="m2"><p>می کند فاش غمزهٔ غماز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سیدم دل ببرد از همه کس</p></div>
<div class="m2"><p>لیک دل را گذاشت در شیراز</p></div></div>