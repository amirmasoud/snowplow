---
title: >-
    غزل شمارهٔ ۷۵۹
---
# غزل شمارهٔ ۷۵۹

<div class="b" id="bn1"><div class="m1"><p>دولت وصل تو به ما کی رسد</p></div>
<div class="m2"><p>منصب شاهی به گدا کی رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نخورد دُردی دردت به ذوق</p></div>
<div class="m2"><p>صوفی صافی به صفا کی رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که به خود راه خدا می رود</p></div>
<div class="m2"><p>با خودی خود به خدا کی رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>راه بیابان فنا چون نرفت</p></div>
<div class="m2"><p>در حرم دار بقا کی رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام حبابیم پر آب حیات</p></div>
<div class="m2"><p>جز لب ما بر لب ما کی رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساکن میخانه چو خوش ایمنست</p></div>
<div class="m2"><p>خانهٔ امنی است بلا کی رسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما حاکم و ما بنده ایم </p></div>
<div class="m2"><p>هر چه کند چون و چرا کی رسد</p></div></div>