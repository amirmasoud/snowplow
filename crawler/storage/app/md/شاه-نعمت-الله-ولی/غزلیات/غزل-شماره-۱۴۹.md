---
title: >-
    غزل شمارهٔ ۱۴۹
---
# غزل شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>ای عاشقان ای عاشقان معشوق با ما همدم است</p></div>
<div class="m2"><p>با ما حریفی می کند یاری که با ما محرم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست شراب عشق از ذوق خوشی دارد مدام</p></div>
<div class="m2"><p>یک جرعه ای ازجام او خوشتر ز صد جام جم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما در خرابات مغان رندانه خوش می می خوریم</p></div>
<div class="m2"><p>شادی مست عاشقی کز جمله عالم بی غم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم دلی چون آینه دلدار دارد در نظر</p></div>
<div class="m2"><p>در آینه پیدا شده حسنی که اسم اعظم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نور دو چشم عالم است نقش خیال روی او</p></div>
<div class="m2"><p>نقش خیال روی او نور دو چشم عالمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مجلس سلطان ما نقل و شراب بی حد است</p></div>
<div class="m2"><p>دُردی درد او که آن در بزم این سلطان کمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر یک دمی همدم شوی با سید سرمست ما</p></div>
<div class="m2"><p>در جام می بنمایدت ساقی که با ما همدمست</p></div></div>