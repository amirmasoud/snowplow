---
title: >-
    غزل شمارهٔ ۱۳۵۲
---
# غزل شمارهٔ ۱۳۵۲

<div class="b" id="bn1"><div class="m1"><p>بستیم کمر به خدمت او</p></div>
<div class="m2"><p>رفتیم روان به حضرت او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیزی که تو را به او رساند</p></div>
<div class="m2"><p>آن نیست به جز محبت او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالم چو وجود یافت از وی</p></div>
<div class="m2"><p>مرحوم بود به رحمت او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منعم چو به نعمت خدائی</p></div>
<div class="m2"><p>منعم باشی به نعمت او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر بندهٔ صادقی که بینی</p></div>
<div class="m2"><p>جان داده برای خدمت او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او داده به ما هر آنچه داریم</p></div>
<div class="m2"><p>داریم هزار منت او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مائیم و حضور نعمت الله</p></div>
<div class="m2"><p>خوشوقت به یمن همت او</p></div></div>