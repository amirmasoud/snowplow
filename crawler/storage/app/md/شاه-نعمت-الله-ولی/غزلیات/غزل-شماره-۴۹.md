---
title: >-
    غزل شمارهٔ ۴۹
---
# غزل شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>بندهٔ ساقی ما شو تا شوی سلطان ما</p></div>
<div class="m2"><p>جان فدا کن تا شوی جانان ما ای جان ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم صورت بین ببند و دیدهٔ معنی گشا</p></div>
<div class="m2"><p>تا ببینی بر سریر ملک دل سلطان ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر گدای عشق باشی پادشاه عالمی</p></div>
<div class="m2"><p>حکم تو گردد روان گر می بری فرمان ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نم چشم و غم دل نُقل و باده می خوریم</p></div>
<div class="m2"><p>الصلا گر عاشقی نزلی بخور از خوان ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حال ما پیدا شود بر ساکنان صومعه</p></div>
<div class="m2"><p>گر جمال خود نماید شاهد پنهان ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همدم جامیم ساقی را حریف سرخوشیم</p></div>
<div class="m2"><p>ذوق اگر داری طلب کن خدمت رندان ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مجلس عشقست و سید عاشق و معشوق او</p></div>
<div class="m2"><p>این چنین بزمی بیابی گر شوی مهمان ما</p></div></div>