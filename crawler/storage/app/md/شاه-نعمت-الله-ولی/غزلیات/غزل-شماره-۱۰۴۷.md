---
title: >-
    غزل شمارهٔ ۱۰۴۷
---
# غزل شمارهٔ ۱۰۴۷

<div class="b" id="bn1"><div class="m1"><p>خوش حیاتی که پیش او میرم</p></div>
<div class="m2"><p>چون بمیرم به کیش او میرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق او شمع و من چو پروانه</p></div>
<div class="m2"><p>گرچه سوزد که در برش گیرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر زند ور نوازدم چون نی</p></div>
<div class="m2"><p>بجز از ناله نیست تدبیرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش دیدم خیال او درخواب</p></div>
<div class="m2"><p>لطفش امروز کرده تعبیرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سروری بر همه توانم کرد</p></div>
<div class="m2"><p>من چو در پای میر خود میرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون توانم که عذر او خواهم</p></div>
<div class="m2"><p>که سراپا تمام تقصیرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچه گویم ز خود نمی گویم</p></div>
<div class="m2"><p>نعمت الله کرده تقدیرم</p></div></div>