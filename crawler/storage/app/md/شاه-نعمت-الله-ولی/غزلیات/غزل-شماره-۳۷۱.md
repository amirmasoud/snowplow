---
title: >-
    غزل شمارهٔ ۳۷۱
---
# غزل شمارهٔ ۳۷۱

<div class="b" id="bn1"><div class="m1"><p>موجود حقیقی به جز از ذات خدا نیست</p></div>
<div class="m2"><p>مائیم صفات و صفت از ذات جدا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز عین یکی در دو جهان نیست حقیقت</p></div>
<div class="m2"><p>گر هست تو را در نظرت غیر مرا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق است مرا چاره و این چاره مرا هست</p></div>
<div class="m2"><p>درد است دوای تو و این درد تو را نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرجا که تو انگشت نهی عین حقست آن</p></div>
<div class="m2"><p>زین نیست معین که کجا هست و کجا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون است بقای همه و باقی مطلق</p></div>
<div class="m2"><p>چیزی که بود قابل تغییر و فنا نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آن دم که دمیدند دم آدم خاکی</p></div>
<div class="m2"><p>بود آن دم ما زان همه دم جز دم ما نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سرمست شراب ازل و جام الستیم</p></div>
<div class="m2"><p>در مجلس ما ساقی ما غیر خدا نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما ماهی دریای محیطیم کماهی</p></div>
<div class="m2"><p>ماهیت ما را تو نگر تا که که را نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سید چو همه طالب و مطلوب نمایند</p></div>
<div class="m2"><p>عاشق نتوان گفت که معشوق نما نیست</p></div></div>