---
title: >-
    غزل شمارهٔ ۸۴۹
---
# غزل شمارهٔ ۸۴۹

<div class="b" id="bn1"><div class="m1"><p>یک حقیقت هست ما را در نظر</p></div>
<div class="m2"><p>این حقیقت در حقایق می نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم حقیقت هم حقایق آن توئی</p></div>
<div class="m2"><p>با خود آ گر زانکه هستی با خبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اصل و فرع عالمی ای نور چشم</p></div>
<div class="m2"><p>حق طلب فرما و از خود درگذر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون یکی اندر یکی باشد یکی</p></div>
<div class="m2"><p>آن یکی در عین اعیان می نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زر یکی و تنگهٔ زر بی شمار</p></div>
<div class="m2"><p>یک حقیقت صورتش بی حد و مر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتابی تافته بر آینه</p></div>
<div class="m2"><p>گشته پیدا فتنهٔ دور قمر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر از مخموری ای جان عزیز</p></div>
<div class="m2"><p>نعمت الله جوی وان گه باده خور</p></div></div>