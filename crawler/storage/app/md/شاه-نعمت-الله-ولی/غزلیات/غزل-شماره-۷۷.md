---
title: >-
    غزل شمارهٔ ۷۷
---
# غزل شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>موج است و حباب و آب و دریا</p></div>
<div class="m2"><p>هر چار یکی بود بر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم آب و حباب و آب دریا</p></div>
<div class="m2"><p>دریا داند حقیقت ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنگر به یقین که جز یکی نیست</p></div>
<div class="m2"><p>هم قطره و جود سیل و دریا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می دانکه حجاب ما هم از ماست</p></div>
<div class="m2"><p>ما را نبُود حجاب جز ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیگانه شوی ز هر دو عالم</p></div>
<div class="m2"><p>گر زانکه تو را بود سر ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا رسته نگردی از من و ما</p></div>
<div class="m2"><p>سید نشوی تو واصل ما</p></div></div>