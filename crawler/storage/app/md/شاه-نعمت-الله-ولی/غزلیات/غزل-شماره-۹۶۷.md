---
title: >-
    غزل شمارهٔ ۹۶۷
---
# غزل شمارهٔ ۹۶۷

<div class="b" id="bn1"><div class="m1"><p>آن یکی از هر یکی می جویمش</p></div>
<div class="m2"><p>دو نمی گویم یکی می گویمش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده گر نقش خیال غیر دید</p></div>
<div class="m2"><p>پاکبازانه روان می شویمش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شد معطر عالمی از بوی او</p></div>
<div class="m2"><p>این چنین بوی خوشی می بویمش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک حقیقت در دو عالم رو نمود</p></div>
<div class="m2"><p>در دو عالم آن یکی می گویمش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیدم تخم محبت کاشته</p></div>
<div class="m2"><p>از محبت من چنین می رویمش</p></div></div>