---
title: >-
    غزل شمارهٔ ۱۰۹۴
---
# غزل شمارهٔ ۱۰۹۴

<div class="b" id="bn1"><div class="m1"><p>دُرد دردش به ذوق می نوشم</p></div>
<div class="m2"><p>خلعت از جود عشق می پوشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غم عشقش خریده ام به جهان</p></div>
<div class="m2"><p>به همه کائنات نفروشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاج عشق ویست بر سرمن</p></div>
<div class="m2"><p>حلقهٔ بندگیش در گوشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتشی هست در دلم که مدام</p></div>
<div class="m2"><p>همچو خم شراب می جوشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مستم و چون سبوی میخواران</p></div>
<div class="m2"><p>عاشقان می کشند بر دوشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقانه به باده نوشیدن</p></div>
<div class="m2"><p>تا که جان در تن است می کوشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله یادگار من است</p></div>
<div class="m2"><p>نکند هیچکس فراموشم</p></div></div>