---
title: >-
    غزل شمارهٔ ۱۴۳۰
---
# غزل شمارهٔ ۱۴۳۰

<div class="b" id="bn1"><div class="m1"><p>دیده صبح از تو منور شده</p></div>
<div class="m2"><p>طرهٔ شام از تو معنبر شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باد صبا بوی تو را یافته</p></div>
<div class="m2"><p>عالم از آن بوی معطر شده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نظر اهل نظر کائنات</p></div>
<div class="m2"><p>نقش خیالیست مصور شده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت و معنی چو مه و آفتاب</p></div>
<div class="m2"><p>هر دو به هم نیک برابر شده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گشته روان چشمهٔ آب حیات</p></div>
<div class="m2"><p>رهگذر ما همه خوشتر شده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عین مسما بود اسمش از آن</p></div>
<div class="m2"><p>آمده و اول دفتر شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتهٔ نوباوهٔ سید شنو</p></div>
<div class="m2"><p>نه سخن آنکه مکرر شده</p></div></div>