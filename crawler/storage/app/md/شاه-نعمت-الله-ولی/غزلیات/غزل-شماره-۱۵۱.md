---
title: >-
    غزل شمارهٔ ۱۵۱
---
# غزل شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>لطف اگر بر ما گمارد حاکم است</p></div>
<div class="m2"><p>ور دمار از ما برآرد حاکم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشنه ایم و رحمتی خواهیم از او</p></div>
<div class="m2"><p>گر ببارد ور نبارد حاکم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر شمارد بنده را از بندگان</p></div>
<div class="m2"><p>حاکمست ار نه شمارد حاکمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر کشد نقش خیالی حاکم است</p></div>
<div class="m2"><p>ور نگاری می نگاری حاکمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کشد صد جان فدای حضرتش</p></div>
<div class="m2"><p>ور به خاکم می سپارد حاکمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی گل را حکم او خارد به خار</p></div>
<div class="m2"><p>گر نخارد ور بخارد حاکمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما گنه کاریم و سید پادشاه</p></div>
<div class="m2"><p>گر بگیرد ور گذارد حاکمست</p></div></div>