---
title: >-
    غزل شمارهٔ ۱۱۳۰
---
# غزل شمارهٔ ۱۱۳۰

<div class="b" id="bn1"><div class="m1"><p>خسته‌حالیم و ز زلف تو شفا می‌طلبیم</p></div>
<div class="m2"><p>دردمندیم و ز وصل تو دوا می‌طلبیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکسی را ز تو گر هست به نوعی طلبی</p></div>
<div class="m2"><p>ما به هر وجه که هست از تو تو را می‌طلبیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از خدا نعمت جنت طلبد زاهد و ما</p></div>
<div class="m2"><p>به خدا گر ز خدا غیر خدا می‌طلبیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه ما می‌طلبیمش همه دانند ولیک</p></div>
<div class="m2"><p>نیست ما را که بگوییم کرا می‌طلبیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکل اینست که سعی طلب ما هرگز</p></div>
<div class="m2"><p>نرسیده است بدان جای که ما می‌طلبیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کیمیایی که مس قلب از او زر گردد</p></div>
<div class="m2"><p>به یقین از نظر پاک شما می‌طلبیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر بقا می‌طلبی باش فنا چون سید</p></div>
<div class="m2"><p>ما ز خود ناشده فانی چه بقا می‌طلبیم</p></div></div>