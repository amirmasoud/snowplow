---
title: >-
    غزل شمارهٔ ۸۹
---
# غزل شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>خوش حضوریست بزم ما دریاب</p></div>
<div class="m2"><p>هرچه می بایدت بیا دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می جام فنا چه می نوشی</p></div>
<div class="m2"><p>ذوق خمخانهٔ بقا دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات دُرد دردش نوش</p></div>
<div class="m2"><p>زان شفاخانه این دوا دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره و موج و بحر و جو آبند</p></div>
<div class="m2"><p>عین ما را به عین ما دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رند مستی اگر طلب داری</p></div>
<div class="m2"><p>بر سر کوی او مرا دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت الله را به دست آور</p></div>
<div class="m2"><p>مظهر حضرت خدا دریاب</p></div></div>