---
title: >-
    غزل شمارهٔ ۱۴۶۶
---
# غزل شمارهٔ ۱۴۶۶

<div class="b" id="bn1"><div class="m1"><p>غیر حق باطلست تا دانی</p></div>
<div class="m2"><p>عقل از این غافل است تا دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج بحریم و عین ما آبست</p></div>
<div class="m2"><p>عالمش ساحلست تا دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که عالم نشد به علم رسول</p></div>
<div class="m2"><p>به خدا جاهلست تادانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه دانست این سخن به تمام</p></div>
<div class="m2"><p>بندهٔ کاملست تا دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر تجلی که بر دلت آید</p></div>
<div class="m2"><p>از خدا نازل است تا دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر که غیر از خداست ای درویش</p></div>
<div class="m2"><p>همه بی حاصل است تا دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشتهٔ عشق و زنده ام جاوید</p></div>
<div class="m2"><p>سیدم قاتل است تا دانی</p></div></div>