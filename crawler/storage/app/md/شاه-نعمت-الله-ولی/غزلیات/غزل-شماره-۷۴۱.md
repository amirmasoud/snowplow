---
title: >-
    غزل شمارهٔ ۷۴۱
---
# غزل شمارهٔ ۷۴۱

<div class="b" id="bn1"><div class="m1"><p>ترک سرمستم دگر باره کلاه کج نهاد</p></div>
<div class="m2"><p>ملک دل بگرفت و خان و مان همه بر باد داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش سلطان داد بتوان خواستن از دیگران</p></div>
<div class="m2"><p>چون که زو بیداد باشد از که خواهم خواست داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل سرگردان ز پا افتاد و عشقش در ربود</p></div>
<div class="m2"><p>همچو مخموری به دست ترک سرمستی فتاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در چمن سرو سهی تا دید آن بالای او</p></div>
<div class="m2"><p>سر به پای او فکند و پیش او بر پاستاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش در میخانه را بر روی ما بگشاده اند</p></div>
<div class="m2"><p>بس گشایش ها که ما را رو نموده زین گشاد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات مغان رندی که نام ما شنود</p></div>
<div class="m2"><p>سرخوشانه پای کوبان رو به سوی ما نهاد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کسی گوید که سید توبه کرد از عاشقی</p></div>
<div class="m2"><p>حاش لله این نخواهم کرد و این هرگز مباد</p></div></div>