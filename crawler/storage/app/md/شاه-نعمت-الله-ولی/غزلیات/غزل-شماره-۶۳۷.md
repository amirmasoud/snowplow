---
title: >-
    غزل شمارهٔ ۶۳۷
---
# غزل شمارهٔ ۶۳۷

<div class="b" id="bn1"><div class="m1"><p>گر ز چین سنبل زلفت صبا بوئی برد</p></div>
<div class="m2"><p>نافهٔ مشک ختن گیرد به هر سوئی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل به دست باد خواهم داد هر چه باد باد</p></div>
<div class="m2"><p>لیکن آن بادی که از خاک درت بوئی برد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک آن بادم که ما را در هوای عشق تو</p></div>
<div class="m2"><p>ذره ذره گرد گرداند به هر کوئی برد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نه کفر زلف تو بر روی ایمان چیره شد</p></div>
<div class="m2"><p>از چه رو رومی جمالی جور هندوئی برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در ختن با زلف تو گر دم زند مشک ختا</p></div>
<div class="m2"><p>چین زلفت آبروی او به یک موئی برد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ببردی از برم جان می بری خوش می کنی</p></div>
<div class="m2"><p>ای خوشا وقت دل و جانی که خوشخوئی برد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ار باری برد در عشق تو بار تو است</p></div>
<div class="m2"><p>زانکه خوش باشد که یاری بار مهروئی برد</p></div></div>