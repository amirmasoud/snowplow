---
title: >-
    غزل شمارهٔ ۱۵۳۵
---
# غزل شمارهٔ ۱۵۳۵

<div class="b" id="bn1"><div class="m1"><p>گر آینه عین او نبودی</p></div>
<div class="m2"><p>آن روی به ما که می نمودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشاد در سرا به عالم</p></div>
<div class="m2"><p>گر در بستی که می گشودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او می بخشد وجود ور نه</p></div>
<div class="m2"><p>بودی ز من و ز تو نبودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی خندهٔ گل نوای بلبل</p></div>
<div class="m2"><p>در گلشن او که می شنودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نقش خیال او ندیدی</p></div>
<div class="m2"><p>این دیدهٔ ما کجا غنودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این گفته اگر نه گفتهٔ اوست</p></div>
<div class="m2"><p>از آینه زنگ کی زدودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیدم سید که درخرابات</p></div>
<div class="m2"><p>مستانه سرود می سرودی</p></div></div>