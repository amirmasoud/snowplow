---
title: >-
    غزل شمارهٔ ۸۹۱
---
# غزل شمارهٔ ۸۹۱

<div class="b" id="bn1"><div class="m1"><p>در مرتبه ای سرمست در مرتبه ای مخمور</p></div>
<div class="m2"><p>در مرتبه ای واصل در مرتبه ای مهجور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مرتبه ای عاشق درمرتبه ای معشوق</p></div>
<div class="m2"><p>در مرتبه ای ناظر در مرتبه ای منظور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مرتبه ای سلطان در مرتبه ای درویش</p></div>
<div class="m2"><p>در مرتبه ای شاه است در مرتبه ای دُستور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مرتبه ای کرمان در مرتبه ای شیراز</p></div>
<div class="m2"><p>در مرتبه ای پیدا در مرتبه ای مستور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در مرتبه ای خالق در مرتبه ای مخلوق</p></div>
<div class="m2"><p>در مرتبه ای قادر در مرتبه ای مقدور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مرتبه ای غایب در مرتبه ای حاضر</p></div>
<div class="m2"><p>در مرتبه ای پنهان در مرتبه ای مشهور</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مرتبه ای سید در مرتبه ای بنده</p></div>
<div class="m2"><p>در مرتبه ای ناصر در مرتبه ای منصور</p></div></div>