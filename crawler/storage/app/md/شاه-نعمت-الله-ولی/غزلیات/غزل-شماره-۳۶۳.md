---
title: >-
    غزل شمارهٔ ۳۶۳
---
# غزل شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>تن میرد و روح پاک باقی است</p></div>
<div class="m2"><p>خواه حیدریست و خواه نراقی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تن زنده به جان و جان به جانان</p></div>
<div class="m2"><p>گه مغربی است گه عراقی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش جام مرصعیست پر می</p></div>
<div class="m2"><p>مائیم حریف و عشق ساقی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی بنمود رو به صورت</p></div>
<div class="m2"><p>این صورت و معنی نفاقی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جاوید بود حیات سید</p></div>
<div class="m2"><p>باقی به بقای حی باقی است</p></div></div>