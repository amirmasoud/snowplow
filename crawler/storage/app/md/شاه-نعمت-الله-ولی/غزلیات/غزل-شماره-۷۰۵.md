---
title: >-
    غزل شمارهٔ ۷۰۵
---
# غزل شمارهٔ ۷۰۵

<div class="b" id="bn1"><div class="m1"><p>خیال غیر خوابی می نماید</p></div>
<div class="m2"><p>همه عالم سرابی می نماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به چشم نقش بندان خیالش</p></div>
<div class="m2"><p>جهان نقشی بر آبی می نماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در این خمخانه هر رندی که یابی</p></div>
<div class="m2"><p>به ما جام شرابی می نماید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر صورت که می بینی به معنی</p></div>
<div class="m2"><p>نگاری بی حجابی می نماید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بده جامی به هر رندی که باشد</p></div>
<div class="m2"><p>که خیر است و ثوابی می نماید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ضمیر روشن هر ذره ما را</p></div>
<div class="m2"><p>ز نورش آفتابی می نماید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجود نعمت الله درخرابات</p></div>
<div class="m2"><p>چو گیتی در خرابی می نماید</p></div></div>