---
title: >-
    غزل شمارهٔ ۱۵۶۱
---
# غزل شمارهٔ ۱۵۶۱

<div class="b" id="bn1"><div class="m1"><p>از برای خدا بیا ساقی </p></div>
<div class="m2"><p>بده آن جام جانفزا ساقی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشق و رند و مست و اوباشیم</p></div>
<div class="m2"><p>نظری کن به حال ما ساقی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نفسی بی شراب نتوان بود</p></div>
<div class="m2"><p>پرکن آن جام می بیا ساقی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد ما را به جرعهٔ دردی</p></div>
<div class="m2"><p>خوش بود گر کنی دوا ساقی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزم عشقست و عاشقان سرمست</p></div>
<div class="m2"><p>عقل بیگانه آشنا ساقی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بهشتیم و باده می نوشیم</p></div>
<div class="m2"><p>می تجلی بود خدا ساقی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله حریف و می در جام</p></div>
<div class="m2"><p>خوش حضوری است خاصه با ساقی</p></div></div>