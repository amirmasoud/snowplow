---
title: >-
    غزل شمارهٔ ۱۰۰۲
---
# غزل شمارهٔ ۱۰۰۲

<div class="b" id="bn1"><div class="m1"><p>جام گیتی نماست یعنی دل</p></div>
<div class="m2"><p>مظهر کبریاست یعنی دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دردمند است و درد می نوشد</p></div>
<div class="m2"><p>دُرد دردش دواست یعنی دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل نظر گاه حضرت عشق است</p></div>
<div class="m2"><p>مثل او خود کجاست یعنی دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خلوت دل سرای سلطان است</p></div>
<div class="m2"><p>فارغ از دو سراست یعنی دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج و گنجینهٔ طلسم نگر</p></div>
<div class="m2"><p>جامع انتهاست یعنی دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ولایت ولی کامل اوست</p></div>
<div class="m2"><p>روز و شب با خداست یعنی دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله به ذوق می گوید</p></div>
<div class="m2"><p>جان و جانان ماست یعنی دل</p></div></div>