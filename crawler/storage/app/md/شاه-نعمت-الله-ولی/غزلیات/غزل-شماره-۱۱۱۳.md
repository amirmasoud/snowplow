---
title: >-
    غزل شمارهٔ ۱۱۱۳
---
# غزل شمارهٔ ۱۱۱۳

<div class="b" id="bn1"><div class="m1"><p>دولت وصل یار می‌بینم</p></div>
<div class="m2"><p>کام دل در کنار می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه روشن به نور او نگرم</p></div>
<div class="m2"><p>گر یکی ور هزار می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه از چشم مردمست نهان</p></div>
<div class="m2"><p>روشن و آشکار می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر خیالی که نقش می‌بندم</p></div>
<div class="m2"><p>نور روی نگار می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خانهٔ دل که رُفته‌ام از غیر</p></div>
<div class="m2"><p>خلوت یار غار می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این عجایب که دید یا که شنید</p></div>
<div class="m2"><p>که یکی بی‌شمار می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را چو می‌نگری</p></div>
<div class="m2"><p>از نبی یادگار می‌بینم</p></div></div>