---
title: >-
    غزل شمارهٔ ۴۹۴
---
# غزل شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>چشم ما روشن به نور او بود</p></div>
<div class="m2"><p>این چنین چشمی خوش و نیکو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه با او نشسته روبرو</p></div>
<div class="m2"><p>روشنی آئینه را زان رو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر تو می گوئی که این رشته دو تو است</p></div>
<div class="m2"><p>تو غلط گفتی که آن یک تو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره و دریا به نزد ما یکی است</p></div>
<div class="m2"><p>دو نماید در نظر نی دو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او را یافت آن را یافته</p></div>
<div class="m2"><p>همچو ما دایم به جست و جو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جود او بخشید عالم را وجود</p></div>
<div class="m2"><p>بی وجود او وجودی چو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله مظهر اسمای اوست</p></div>
<div class="m2"><p>اسم او ذات و صفات او بود</p></div></div>