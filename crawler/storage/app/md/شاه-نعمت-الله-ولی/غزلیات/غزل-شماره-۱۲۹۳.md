---
title: >-
    غزل شمارهٔ ۱۲۹۳
---
# غزل شمارهٔ ۱۲۹۳

<div class="b" id="bn1"><div class="m1"><p>دردمندیم و از دوا ایمن</p></div>
<div class="m2"><p>بینوائیم وز نوا ایمن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خرابات خلوتی داریم</p></div>
<div class="m2"><p>خوش نشسته در این سرا ایمن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به خدا هر که باشد او باقی</p></div>
<div class="m2"><p>همچو ما گردد از فنا ایمن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که خواهی و هر که بینی بود</p></div>
<div class="m2"><p>یار ما باشد و ز ما ایمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدمی نه در آ به میخانه</p></div>
<div class="m2"><p>تا که گردی چو اولیا ایمن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باش ایمن ز خوف بیگانه</p></div>
<div class="m2"><p>بنشین پیش آشنا ایمن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید خراباتی</p></div>
<div class="m2"><p>رند مستیم و از شما ایمن</p></div></div>