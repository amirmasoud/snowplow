---
title: >-
    غزل شمارهٔ ۲۹۸
---
# غزل شمارهٔ ۲۹۸

<div class="b" id="bn1"><div class="m1"><p>همه عالم تن است و او جان است</p></div>
<div class="m2"><p>شاه تبریز و میر او جان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کنج دل شد به گنج او معمور</p></div>
<div class="m2"><p>ورنه بی گنج کنج ویرانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل کل در جمال حضرت او</p></div>
<div class="m2"><p>همچو من واله است و حیران است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف او مو به مو پریشان شد</p></div>
<div class="m2"><p>حال جمعی از آن پریشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام گیتی نمای دیدهٔ من</p></div>
<div class="m2"><p>روشن از نور روی جانان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه بینی به دیدهٔ معنی</p></div>
<div class="m2"><p>نظری کن که عین این آن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم عشقست و عاشقان سرمست</p></div>
<div class="m2"><p>نعمت الله میر مستان است</p></div></div>