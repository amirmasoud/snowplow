---
title: >-
    غزل شمارهٔ ۱۰۸۵
---
# غزل شمارهٔ ۱۰۸۵

<div class="b" id="bn1"><div class="m1"><p>آفتابست و سایه بان عالم</p></div>
<div class="m2"><p>به مثل او چنین چنان عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام گیتی نماست می بینش</p></div>
<div class="m2"><p>که نماید همین همان عالم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غیر او دیگری نخواهد دید</p></div>
<div class="m2"><p>هر که بینا شود در آن عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این میان و کنار کی بودی</p></div>
<div class="m2"><p>گر نبودی درین میان عالم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت اوست نور دیدهٔ ما</p></div>
<div class="m2"><p>این معانی کند بیان عالم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همه عالم نشان او دارد</p></div>
<div class="m2"><p>بی نشان او بود نشان عالم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر زمان عالمی کند پیدا</p></div>
<div class="m2"><p>می برد آورد روان عالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالم عشق را نهایت نیست</p></div>
<div class="m2"><p>هست این بحر بیکران عالم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله چون می و جام است</p></div>
<div class="m2"><p>جام و می را بدان بدان عالم</p></div></div>