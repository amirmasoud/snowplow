---
title: >-
    غزل شمارهٔ ۱۴۲۰
---
# غزل شمارهٔ ۱۴۲۰

<div class="b" id="bn1"><div class="m1"><p>در شهادت شاهدی از غیب بی عیب آمده</p></div>
<div class="m2"><p>این چنین شادی خوش بی عیب از غیب آمده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در گلستان غنچهٔ گل در هوای روی او</p></div>
<div class="m2"><p>پیرهن بدریده و بی دامن و جَیب آمده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن معانی بدیع او بدیع دیگر است</p></div>
<div class="m2"><p>زان که بر وی این کلام الله بی رَیب آمده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نو عروس فکر بکرم شاهدی بس دلکش است</p></div>
<div class="m2"><p>در مشاهد شاهدی می خواهد از غیب آمده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جوانی نعمت الله با سواد و معرفت</p></div>
<div class="m2"><p>این زمان باز آمده پروانه با شیب آمده</p></div></div>