---
title: >-
    غزل شمارهٔ ۲۱۴
---
# غزل شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>هرچه او می دهد همه داده است</p></div>
<div class="m2"><p>دادهٔ او مگو که بیداد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای خوشا وقت عاشقی که مدام</p></div>
<div class="m2"><p>بر در میفروش افتاده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزم عشقست و عاشقان سرمست</p></div>
<div class="m2"><p>کس چنین بزم خوب ننهادست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غم عشقش خجسته باد که دل</p></div>
<div class="m2"><p>به غم عشق دایما شاد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل در بزم عشق دانی چیست</p></div>
<div class="m2"><p>چون چراغی نهاده بر باد است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه او شد غلام سید ما</p></div>
<div class="m2"><p>بنده مقبلست و آزاد است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه کنم نعمت همه عالم</p></div>
<div class="m2"><p>نعمت الله خدا مرا داده است</p></div></div>