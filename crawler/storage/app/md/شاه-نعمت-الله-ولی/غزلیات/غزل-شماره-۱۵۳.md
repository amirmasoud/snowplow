---
title: >-
    غزل شمارهٔ ۱۵۳
---
# غزل شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>در کوی خرابات کسی را که مقام است</p></div>
<div class="m2"><p>در دنیی و در آخرتش جاه تمام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما توبه شکستیم در این قول درستیم</p></div>
<div class="m2"><p>با ساغر می عهد که بستیم مدام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زان مجلس ما بزم ملوکانهٔ عشق است</p></div>
<div class="m2"><p>ساقی قدیم است و شرابی به قوام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نوش که در مذهب ما پاک و حلال است</p></div>
<div class="m2"><p>کاین می نه شرابست که گویند حرامست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنجینهٔ ما مخزن اسرار الهی است</p></div>
<div class="m2"><p>هر گنج درین کنج که یابی به نظام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دور بگردید و نمائید به یاران</p></div>
<div class="m2"><p>رندی که بود چون من سرمست کدامست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشنو سخن سید رندان خرابات</p></div>
<div class="m2"><p>کامروز درین دور خداوند کلام است</p></div></div>