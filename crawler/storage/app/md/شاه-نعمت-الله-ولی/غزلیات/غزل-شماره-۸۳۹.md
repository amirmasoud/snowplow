---
title: >-
    غزل شمارهٔ ۸۳۹
---
# غزل شمارهٔ ۸۳۹

<div class="b" id="bn1"><div class="m1"><p>مال قلبش کن لام است ای پسر</p></div>
<div class="m2"><p>قلب آدم نیز دام است ای پسر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دام را بگذار تا فارغ شوی</p></div>
<div class="m2"><p>هر چه ما داریم دام است ای پسر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر فدا کن در طریق عاشقی</p></div>
<div class="m2"><p>جان که باشد دل کدام است ای پسر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام ما باشد حبابی پر ز آب</p></div>
<div class="m2"><p>بادهٔ ما عین جام است ای پسر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقلی گر عالم عالم بود</p></div>
<div class="m2"><p>نزد عاشق ناتمام است ای پسر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر یکی را یک دو روزی دور اوست</p></div>
<div class="m2"><p>دور ما اما مدام است ای پسر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله در خرابات مغان</p></div>
<div class="m2"><p>رهنمای خاص و عام است ای پسر</p></div></div>