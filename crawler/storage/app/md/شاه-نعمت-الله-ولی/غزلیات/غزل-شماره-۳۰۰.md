---
title: >-
    غزل شمارهٔ ۳۰۰
---
# غزل شمارهٔ ۳۰۰

<div class="b" id="bn1"><div class="m1"><p>بندگی کن که کار نیک آن است</p></div>
<div class="m2"><p>این چنین کار کار نیکان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل ما دلبری که می بیند</p></div>
<div class="m2"><p>جان به او می دهد که جانان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی به مه شده پیدا</p></div>
<div class="m2"><p>گرچه او هم به ماه پنهان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>موج و بحر و حباب و قطرهٔ آب</p></div>
<div class="m2"><p>نزد ما هر چهار یکسان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنج دل گنجخانهٔ عشق است</p></div>
<div class="m2"><p>خانه بی گنج کُنج ویران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدان را مجال کی باشد</p></div>
<div class="m2"><p>در مقامی که جای رندان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید خرابات است</p></div>
<div class="m2"><p>نعمت الله که میر مستان است</p></div></div>