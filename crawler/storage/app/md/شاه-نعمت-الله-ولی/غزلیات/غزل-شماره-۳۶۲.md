---
title: >-
    غزل شمارهٔ ۳۶۲
---
# غزل شمارهٔ ۳۶۲

<div class="b" id="bn1"><div class="m1"><p>هر شاهدی که بینم با او مرا هوائیست</p></div>
<div class="m2"><p>آئینه ایست روشن جام جهان نمائیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلوتسرای دیده از نور اوست روشن</p></div>
<div class="m2"><p>بر چشم ما قدم نه بنشین که خوش سرائیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گوشهٔ خرابات رندی اگر ببینی</p></div>
<div class="m2"><p>بیگانه اش ندانی او یار آشنائیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درویش کنج عزلت او را به دار عزت</p></div>
<div class="m2"><p>صورت گدا نماید معنیش پادشائیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما دردمند عشقیم دُردی درد نوشیم</p></div>
<div class="m2"><p>خوشتر ز صاف درمان عشاق را دوائیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقش خیال غیری بر دیده گر نگاری</p></div>
<div class="m2"><p>نقاش خطهٔ چین گوید که این خطائیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی عنایتی کرد خمخانه ای به ما داد</p></div>
<div class="m2"><p>ز انعام نعمت الله ما را چنین عطائیست</p></div></div>