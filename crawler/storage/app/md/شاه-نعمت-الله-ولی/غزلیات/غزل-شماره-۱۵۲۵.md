---
title: >-
    غزل شمارهٔ ۱۵۲۵
---
# غزل شمارهٔ ۱۵۲۵

<div class="b" id="bn1"><div class="m1"><p>از دوئی بگذر که تا یابی یکی</p></div>
<div class="m2"><p>در وجود آن یکی نبود شکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقد گنج کنت کنزا را طلب</p></div>
<div class="m2"><p>چون گدایان چند جوئی پولکی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد هزار آئینه گر بنمایدت</p></div>
<div class="m2"><p>آن یکی را می نگر در هر یکی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل خود را دید از خود بی خبر</p></div>
<div class="m2"><p>خودنمائی می کند خود بینکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شعر ما گر عارفی باشد خوشی</p></div>
<div class="m2"><p>ذوق اگر داری بکن تحسینکی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زر یکی و تنگهٔ زر بیشمار</p></div>
<div class="m2"><p>آن یکی را می شمارش نیککی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیک نبود منکر آل عبا</p></div>
<div class="m2"><p>ور بود نبود بهه جز بد دینکی</p></div></div>