---
title: >-
    غزل شمارهٔ ۳۸۲
---
# غزل شمارهٔ ۳۸۲

<div class="b" id="bn1"><div class="m1"><p>جان ندارد هر که جانانیش نیست</p></div>
<div class="m2"><p>گرچه تن دارد ولی جانیش نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد گوشه نشین در عشق او</p></div>
<div class="m2"><p>هست از زاهد ولی آنیش نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کفر زلفش گر ندارد دیگری</p></div>
<div class="m2"><p>کی بود مومن که ایمانیش نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی سر و سامان شدم در عاشقی</p></div>
<div class="m2"><p>ای خوش آن رندی که سامانیش نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساغر می گر چه دارد جرعه ای</p></div>
<div class="m2"><p>همچو خم ، ذوق فراوانیش نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دلی کز عشق او شد دردمند</p></div>
<div class="m2"><p>غیر دُرد درد درمانیش نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید سرمست مهمان من است</p></div>
<div class="m2"><p>هیچکس چون بنده مهمانیش نیست</p></div></div>