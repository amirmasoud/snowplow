---
title: >-
    غزل شمارهٔ ۱۴۵۴
---
# غزل شمارهٔ ۱۴۵۴

<div class="b" id="bn1"><div class="m1"><p>وه چه حسن است اینکه پیدا کرده ای</p></div>
<div class="m2"><p>شکل جان را آشکارا کرده ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت و معنی پدید آورده ای</p></div>
<div class="m2"><p>تا جمال خود هویدا کرده ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غنچه ای از گلستان بنموده ای</p></div>
<div class="m2"><p>بلبلان را مست و شیدا کرده ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ترک چشم مست را می داده ای</p></div>
<div class="m2"><p>عقل هر هشیار یغما کرده ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهری را در صدف بنهاده ای</p></div>
<div class="m2"><p>چشم ما را عین دریا کرده ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جود هر عاشق وجود تو است باز</p></div>
<div class="m2"><p>نام خود معشوق یکتا کرده ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باز سید را به خود بنموده ای</p></div>
<div class="m2"><p>وز کلام خویش گویا کرده ای</p></div></div>