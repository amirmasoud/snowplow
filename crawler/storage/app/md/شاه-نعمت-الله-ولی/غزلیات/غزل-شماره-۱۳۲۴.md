---
title: >-
    غزل شمارهٔ ۱۳۲۴
---
# غزل شمارهٔ ۱۳۲۴

<div class="b" id="bn1"><div class="m1"><p>ای دل گشایشی ز در عاشقان بجو</p></div>
<div class="m2"><p>آسایشی ز صحبت صاحبدلان بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در یوزه ای ز همت مردان حق بکن</p></div>
<div class="m2"><p>بخشایشی ز خدمت این دوستان بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پروانه ای ز آتش عشقش بسوز دل</p></div>
<div class="m2"><p>آن لحظه آروزی دل و کام جان بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خود در آ به خلوت جانانه رو خرام</p></div>
<div class="m2"><p>چون بی نشان شدی ز خود آن دم نشان بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر طالب حقیقتی مطلوب نزد تو است</p></div>
<div class="m2"><p>دریاب و آرزوی دل طالبان بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذرات کاینات ز خورشید روی او</p></div>
<div class="m2"><p>روشن شدند ذره به ذره عیان بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ازین میان و کنارش طلب مکن</p></div>
<div class="m2"><p>پرُتر شو از کنار و برون از میان بجو</p></div></div>