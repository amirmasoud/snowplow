---
title: >-
    غزل شمارهٔ ۱۰۳۹
---
# غزل شمارهٔ ۱۰۳۹

<div class="b" id="bn1"><div class="m1"><p>توبه از زهد و زاهدی کردم</p></div>
<div class="m2"><p>در خرابات مست می گردم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خمخانهٔ حدوث و قدم</p></div>
<div class="m2"><p>شادی روی عاشقان گردم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاطر کس ز من ملول نشد</p></div>
<div class="m2"><p>ننشسته به دامنی گردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دُردی درد دل همی نوشم</p></div>
<div class="m2"><p>دردمندانه همدم دردم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زن دنیا و آخرت چه کنم</p></div>
<div class="m2"><p>رند و مست و مجرد و فردم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق و صادقم گواهانم</p></div>
<div class="m2"><p>اشک سرخست و چهرهٔ زردم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید خراباتم</p></div>
<div class="m2"><p>هر چه فرمود بنده آن کردم</p></div></div>