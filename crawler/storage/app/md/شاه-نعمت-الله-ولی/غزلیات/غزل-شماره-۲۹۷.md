---
title: >-
    غزل شمارهٔ ۲۹۷
---
# غزل شمارهٔ ۲۹۷

<div class="b" id="bn1"><div class="m1"><p>هر که حلقه به گوش مردانست</p></div>
<div class="m2"><p>نزد مردان مرد مرد آنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقانی به جان و دل دایم</p></div>
<div class="m2"><p>در طریقت رفیق یارانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرچه بینم به عشق حضرت او</p></div>
<div class="m2"><p>جان فدایش کنم که جانانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سنبل زلف یار داد به باد</p></div>
<div class="m2"><p>کار جمعی از آن پریشانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو جان در کنار خود گیرم</p></div>
<div class="m2"><p>گرچه او پادشاه کرمانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چنین پادشه که می شنوی</p></div>
<div class="m2"><p>در همه کائنات سلطانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله که رند سرمست است</p></div>
<div class="m2"><p>بندهٔ خاص شاه مردانست</p></div></div>