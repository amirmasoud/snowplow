---
title: >-
    غزل شمارهٔ ۱۰۸۱
---
# غزل شمارهٔ ۱۰۸۱

<div class="b" id="bn1"><div class="m1"><p>من رند خراباتم ایمن ز کراماتم</p></div>
<div class="m2"><p>در گوشهٔ میخانه دائم به مناجاتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر حلقهٔ رندانم ساقی حریفانم</p></div>
<div class="m2"><p>نه زاهد و درویشم ، سلطان خراباتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من آینهٔ اویم ، در آینه او جویم</p></div>
<div class="m2"><p>از ذوق سخن گویم آسوده ز طاماتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهی که صفات او در ذات یکی بینی</p></div>
<div class="m2"><p>مجموع صفاتش بین در آینهٔ ذاتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من سید عشاقم بگزیدهٔ آفاقم</p></div>
<div class="m2"><p>در هر دو جهان طاقم اینست کراماتم</p></div></div>