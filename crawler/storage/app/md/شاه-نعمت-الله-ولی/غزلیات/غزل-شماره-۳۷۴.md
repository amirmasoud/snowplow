---
title: >-
    غزل شمارهٔ ۳۷۴
---
# غزل شمارهٔ ۳۷۴

<div class="b" id="bn1"><div class="m1"><p>شک به عدم نیست که او هیچ نیست</p></div>
<div class="m2"><p>شک به وجود است و هم او هیچ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست گمانم که جز او هیچ نیست</p></div>
<div class="m2"><p>هست یقینم که جز او هیچ نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی هو با تو بگویم که چیست</p></div>
<div class="m2"><p>اوست دگر این من و تو هیچ نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک سخنی بشنو و یکرنگ باش</p></div>
<div class="m2"><p>قول یکی گفتن و دو هیچ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و منی را بگذار ای عزیز</p></div>
<div class="m2"><p>کز من و ما یک سر مو هیچ نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر خدا هیچ بود هیچ هیچ</p></div>
<div class="m2"><p>هیچ نه ای هیچ مجو هیچ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوش کن و باش خموش و برو</p></div>
<div class="m2"><p>هیچ مگو گفت و مگو هیچ نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خم می آور چه کنم جام را </p></div>
<div class="m2"><p>مست و خرابیم و سبو هیچ نیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عاشق سید شو و معشوق او</p></div>
<div class="m2"><p>باش بکی رو که دورو هیچ نیست</p></div></div>