---
title: >-
    غزل شمارهٔ ۱۲۰۳
---
# غزل شمارهٔ ۱۲۰۳

<div class="b" id="bn1"><div class="m1"><p>ما مرشد عشاق خرابات جهانیم</p></div>
<div class="m2"><p>ساقی سراپردهٔ میخانهٔ جانیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از همدانی ولیکن همه دان نه</p></div>
<div class="m2"><p>از ما شنو ای دوست که سر همه دانیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو عالم یک حرفی ما عالم عالم</p></div>
<div class="m2"><p>تو میر صدی باشی و ما شاه جهانیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کس به جمال و رخ خوبی نگرانند</p></div>
<div class="m2"><p>در آینهٔ خویش به خود ما نگرانیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از ما به همه عمر یکی مور نرنجید</p></div>
<div class="m2"><p>تا بود بر این بوده و تا هست برآنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر یار که بینیم که او قابل عشقست</p></div>
<div class="m2"><p>حسنی بنمائیم و دلش را بستانیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رندان سراپردهٔ ما عاشق و مستند</p></div>
<div class="m2"><p>ما سید رندان سراپرده از آنیم</p></div></div>