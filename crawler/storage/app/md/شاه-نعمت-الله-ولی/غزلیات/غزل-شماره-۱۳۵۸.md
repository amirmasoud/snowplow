---
title: >-
    غزل شمارهٔ ۱۳۵۸
---
# غزل شمارهٔ ۱۳۵۸

<div class="b" id="bn1"><div class="m1"><p>جان عاشق نجوید الا هو</p></div>
<div class="m2"><p>دل عاشق نپوید الا هو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>غنچهٔ شاخ گلشن لاهوت</p></div>
<div class="m2"><p>هیچ بلبل نبوید الا هو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منی ما به آب رحمت خویش</p></div>
<div class="m2"><p>هیچ راحِم نشوید الا هو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من کیم تا زبان من گوید</p></div>
<div class="m2"><p>سخن از من نگوید الا هو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مست عاشق نخواهد الا دوست</p></div>
<div class="m2"><p>نعمت الله نگوید الا هو</p></div></div>