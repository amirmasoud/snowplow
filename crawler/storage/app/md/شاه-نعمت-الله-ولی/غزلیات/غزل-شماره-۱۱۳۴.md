---
title: >-
    غزل شمارهٔ ۱۱۳۴
---
# غزل شمارهٔ ۱۱۳۴

<div class="b" id="bn1"><div class="m1"><p>درد دل بردیم و درمان یافتیم</p></div>
<div class="m2"><p>نوش وصل از نیش هجران یافتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندگی کردیم و سلطان را بسی</p></div>
<div class="m2"><p>سلطنت از قرب سلطان یافتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از بر ما مدتی دل رفته بود</p></div>
<div class="m2"><p>در سر زلف پریشان یافتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بیفکندیم و سردار آمدیم</p></div>
<div class="m2"><p>جان فدا کردیم و جانان یافتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنچه می جویند و می گویند آن</p></div>
<div class="m2"><p>می طلب از ما که ما آن یافتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سالها در کنج دل ساکن شدیم</p></div>
<div class="m2"><p>گنج او در کنج ویران یافتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را به دست آورده ایم</p></div>
<div class="m2"><p>لاجرم نعمت فراوان یافتیم</p></div></div>