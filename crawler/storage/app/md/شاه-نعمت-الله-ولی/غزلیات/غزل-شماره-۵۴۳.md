---
title: >-
    غزل شمارهٔ ۵۴۳
---
# غزل شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>از احد احمد آشکارا شد</p></div>
<div class="m2"><p>هم به احمد احد هویدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شهادت احد کمر بربست</p></div>
<div class="m2"><p>میم احمد ز غیب پیدا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن یکی در عدد ظهوری کرد</p></div>
<div class="m2"><p>صد عدد از یکی مهیا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قطره و بحر و جو همه آبند</p></div>
<div class="m2"><p>ما نگوئیم قطره دریا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج بحریم و عین ما آب است</p></div>
<div class="m2"><p>نتوان گفت ما که از ما شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آفتاب وجود رو بنمود</p></div>
<div class="m2"><p>ذرهٔ کاینات در وا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آمد و شد حقیقتاً خود نیست</p></div>
<div class="m2"><p>به مجاز است کآمد و یا شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خم می خوش خوشی به جوش آمد</p></div>
<div class="m2"><p>راز سر بسته آشکارا شد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله پرده را برداشت</p></div>
<div class="m2"><p>مشکلاتی که بود حل واشد</p></div></div>