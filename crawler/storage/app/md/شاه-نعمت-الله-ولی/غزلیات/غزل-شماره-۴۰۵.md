---
title: >-
    غزل شمارهٔ ۴۰۵
---
# غزل شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>در نظر عالم چو جامی پر می است</p></div>
<div class="m2"><p>جام من بی خدمت ساقی کی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما روشن شده از نور او</p></div>
<div class="m2"><p>هرچه ما را در نظر آید وی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عالمی از جود او دارد وجود</p></div>
<div class="m2"><p>بی وجودش ما سوی الله لاشی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صوت نائی می رسد ما را به گوش</p></div>
<div class="m2"><p>دیگران گویند آواز وی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش کن آب حیات معرفت</p></div>
<div class="m2"><p>تا بدانی زنده دل از وی حی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام را بگذار و خم می بجو</p></div>
<div class="m2"><p>همت عالی بر آن خم می است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آفتابست او و سید سایه اش</p></div>
<div class="m2"><p>هر کجا او می رود او در پی است</p></div></div>