---
title: >-
    غزل شمارهٔ ۱۴۱۶
---
# غزل شمارهٔ ۱۴۱۶

<div class="b" id="bn1"><div class="m1"><p>خیالش نقش می بندد بهه دیده</p></div>
<div class="m2"><p>چنان نقش و چنین دیده که دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو چشمم روشن است از نور رویش</p></div>
<div class="m2"><p>به مردم می نمایم آن به دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیال عارضش در دیدهٔ ما</p></div>
<div class="m2"><p>بود نقشی بر آبی خوش کشیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صبا در گلستان می خواند شعرم</p></div>
<div class="m2"><p>شنیده غنچه و جامه دریده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درآمد از درم ساقی سرمست</p></div>
<div class="m2"><p>چنان شاهی مرا مهمان رسیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم آئینه گیتی نمائی است</p></div>
<div class="m2"><p>به لطف خود لطیفش آفریده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فتاده آتشی در نی دگر بار</p></div>
<div class="m2"><p>مگر از سیدم حرفی شنیده</p></div></div>