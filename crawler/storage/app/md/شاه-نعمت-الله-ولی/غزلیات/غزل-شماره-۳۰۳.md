---
title: >-
    غزل شمارهٔ ۳۰۳
---
# غزل شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>میر میخانهٔ ما سید سرمستان است</p></div>
<div class="m2"><p>رنداگر می طلبی ساقی سرمستان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چشم است و به نورش همه را می بینم</p></div>
<div class="m2"><p>آفتابی است که در دور قمر تابان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما روشنی از نور جمالش دارد</p></div>
<div class="m2"><p>تو مپندار که او از نظرم پنهان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر فروشند به صد جان نفسی صحبت او</p></div>
<div class="m2"><p>بخر ای جان عزیزم که نگو ارزان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج اگر می طلبی در دل ما می جویش</p></div>
<div class="m2"><p>زانکه گنجینهٔ او کنج دل ویران است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دُردی درد به من ده که خوشی می نوشم</p></div>
<div class="m2"><p>من دوا را چه کنم درد دلم درمان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رند مستی به تو گر روی نماید روزی</p></div>
<div class="m2"><p>نعمت الله طلب از وی که مرا جانان است</p></div></div>