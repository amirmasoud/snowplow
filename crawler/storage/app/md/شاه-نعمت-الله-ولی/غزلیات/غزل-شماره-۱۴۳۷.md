---
title: >-
    غزل شمارهٔ ۱۴۳۷
---
# غزل شمارهٔ ۱۴۳۷

<div class="b" id="bn1"><div class="m1"><p>هر بنده که سوی شه برد راه</p></div>
<div class="m2"><p>هم شاد بود به دولت شاه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما شاه درون پرده دیدیم</p></div>
<div class="m2"><p>دیگر نرویم سوی خرگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای شاه تو قرص آفتابی</p></div>
<div class="m2"><p>ما خاک محقریم در راه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو جان طلبی و ما نخواهیم</p></div>
<div class="m2"><p>هستیم در این سخن به اکراه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما زان تو ایم هر چه داریم</p></div>
<div class="m2"><p>العبد و ماله لمولاه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هست از نظر تو ناظر حق</p></div>
<div class="m2"><p>سلطان دو کون نعمت الله</p></div></div>