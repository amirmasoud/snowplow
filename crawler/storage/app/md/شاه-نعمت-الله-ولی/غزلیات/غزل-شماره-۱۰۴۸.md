---
title: >-
    غزل شمارهٔ ۱۰۴۸
---
# غزل شمارهٔ ۱۰۴۸

<div class="b" id="bn1"><div class="m1"><p>هر کجا حسن خوشی می نگرم</p></div>
<div class="m2"><p>جان به عشق تو به او می سپرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگرانم به جمال خوبان</p></div>
<div class="m2"><p>چه کنم حسن تو را می نگرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دم به دم کلک خیالت به کرم</p></div>
<div class="m2"><p>صورتی نقش کند در نظرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خورم جام می عشق مدام</p></div>
<div class="m2"><p>غم بیهودهٔ عالم نخورم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هوای در میخانهٔ تو</p></div>
<div class="m2"><p>از سر هر دو جهان در گذرم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا ز اسرار می و دیر مغان</p></div>
<div class="m2"><p>خبری یافته ام بی خبرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بندهٔ سید سرمستانم</p></div>
<div class="m2"><p>پیش رندان جهان معتبرم</p></div></div>