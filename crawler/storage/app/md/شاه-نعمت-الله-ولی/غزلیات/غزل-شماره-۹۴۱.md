---
title: >-
    غزل شمارهٔ ۹۴۱
---
# غزل شمارهٔ ۹۴۱

<div class="b" id="bn1"><div class="m1"><p>در خرابات تا سحرگه دوش</p></div>
<div class="m2"><p>می کشیدم سبوی می بر دوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادی روی ساقی سرمست</p></div>
<div class="m2"><p>دوش تا روز بود نوشانوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بزم عشق است خرقه را بر کن</p></div>
<div class="m2"><p>جامهٔ عاشقانه ای درپوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در ره عاشقی و می خواری</p></div>
<div class="m2"><p>عاشقانه به جان و دل می کوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما خراباتیان سرمستیم</p></div>
<div class="m2"><p>چون خم می فروش خوش در جوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گل تبسم کنان و می در جام</p></div>
<div class="m2"><p>بلبل مست کی شود خاموش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله حریف و ساقی او</p></div>
<div class="m2"><p>جام در دور و عاشقان مدهوش</p></div></div>