---
title: >-
    غزل شمارهٔ ۱۱۳۵
---
# غزل شمارهٔ ۱۱۳۵

<div class="b" id="bn1"><div class="m1"><p>درد دل بردیم و درمان یافتیم</p></div>
<div class="m2"><p>سوز جان دیدیم و جانان یافتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما تا مبتلای عشق شد</p></div>
<div class="m2"><p>از بلایش راحت جان یافتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلبر خود در دل خود دیده ایم</p></div>
<div class="m2"><p>گنج او در کنج ویران یافتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی بودیم با ساقی حریف</p></div>
<div class="m2"><p>عاشقانه می فراوان یافتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یوسف مصری که صد مصرش بهاست</p></div>
<div class="m2"><p>ناگهان در ملک کنعان یافتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت الله در خرابات مغان</p></div>
<div class="m2"><p>میر سرمستان و رندان یافتیم</p></div></div>