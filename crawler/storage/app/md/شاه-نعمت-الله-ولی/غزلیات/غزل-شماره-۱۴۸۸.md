---
title: >-
    غزل شمارهٔ ۱۴۸۸
---
# غزل شمارهٔ ۱۴۸۸

<div class="b" id="bn1"><div class="m1"><p>چو یارم دلبر دیگر نیابی</p></div>
<div class="m2"><p>چنان دلبر درین کشور نیابی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو روی خوب او مؤمن نبینی</p></div>
<div class="m2"><p>چو کفر زلف او کافر نیابی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حریف سرخوشی ساقی رندی</p></div>
<div class="m2"><p>چو چشم مست آن دلبر نیابی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیابی ذوق از یک جرعهٔ می</p></div>
<div class="m2"><p>که از صد ساغر کوثر نیابی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا و خرقه بفروش و به می ده</p></div>
<div class="m2"><p>که سودائی ازین خوشتر نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به درد دل بیا درمان طلب کن</p></div>
<div class="m2"><p>ز من شکرانه بستان گر نیابی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>غنیمت دان حضور نعمت الله</p></div>
<div class="m2"><p>که عمری این چنین دیگر نیابی</p></div></div>