---
title: >-
    غزل شمارهٔ ۱۰۸۹
---
# غزل شمارهٔ ۱۰۸۹

<div class="b" id="bn1"><div class="m1"><p>با سر زلف بتی باز در افتاد دلم</p></div>
<div class="m2"><p>لاجرم چون سر زلفش به سر افتاد دلم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجمع اهل دلان زلف پریشان ویست</p></div>
<div class="m2"><p>مکنم عیب درین جمع گر افتاد دلم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه کنم مجلس عشقست و حریفان سرمست</p></div>
<div class="m2"><p>خاطرم یافت چنین بزم و در افتاد دلم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دوش دلدار کرم کرد دلم را بنواخت</p></div>
<div class="m2"><p>باز امروز در آن رهگذر افتاد دلم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ناظر اویم و منظور من اندر نظر است</p></div>
<div class="m2"><p>نور چشمست که روشن نظر افتاد دلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پردهٔ دل که حجاب دل و دلدارم بود</p></div>
<div class="m2"><p>خوش بر افتاد از آن رو که بر افتاد دلم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما خبری گفت ز حال دل خویش</p></div>
<div class="m2"><p>زان خبر مست شد و بی خبر افتاد دلم</p></div></div>