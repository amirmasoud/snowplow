---
title: >-
    غزل شمارهٔ ۱۵۰۳
---
# غزل شمارهٔ ۱۵۰۳

<div class="b" id="bn1"><div class="m1"><p>گر چه میری در این جهان میری</p></div>
<div class="m2"><p>چون رسد وقت ناگهان میری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب سر چشمهٔ حیات بنوش</p></div>
<div class="m2"><p>تا نمانند این و آن میری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش کناری بگیر ازین عالم</p></div>
<div class="m2"><p>پیش از آن دم که در میان میری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زندهٔ جاودان توانی بود</p></div>
<div class="m2"><p>گر تو در پای عاشقان میری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که مُرد او دگر نخواهد مُرد</p></div>
<div class="m2"><p>ور نمیری چو دیگران میری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنده دل باش و جان به جانان ده</p></div>
<div class="m2"><p>گرنخواهی که جاودان میری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله به ذوق جان بسپرد</p></div>
<div class="m2"><p>تو چنان رو که همچنان میری</p></div></div>