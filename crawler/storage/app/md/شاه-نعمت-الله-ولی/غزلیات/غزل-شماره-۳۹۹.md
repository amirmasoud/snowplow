---
title: >-
    غزل شمارهٔ ۳۹۹
---
# غزل شمارهٔ ۳۹۹

<div class="b" id="bn1"><div class="m1"><p>زاهدان را ذوق رندان هست نیست</p></div>
<div class="m2"><p>رند را میلی بر ایشان هست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در دل ما مهر دلبر هست نیست</p></div>
<div class="m2"><p>جان ما جز عشق جانان هست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یوسف گل پیرهن آمد به باغ</p></div>
<div class="m2"><p>این چنین گل در گلستانی هست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر که دارد هرچه دارد آن اوست</p></div>
<div class="m2"><p>هرچه هست و بود و بی آن هست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج او در کنج ویران نیست هست</p></div>
<div class="m2"><p>خازن آن غیر سلطان هست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد نوش دردمند عشق او</p></div>
<div class="m2"><p>خاطرش با صاف درمان هست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو سید رند سرمست خوشی</p></div>
<div class="m2"><p>در میان می پرستان هست نیست</p></div></div>