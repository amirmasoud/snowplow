---
title: >-
    غزل شمارهٔ ۴۴۶
---
# غزل شمارهٔ ۴۴۶

<div class="b" id="bn1"><div class="m1"><p>معنی او نمود در صورت</p></div>
<div class="m2"><p>نه به یک صورتی به هر صورت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما تا جمال معنی دید</p></div>
<div class="m2"><p>معنئی بیند و دگر صورت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذره ذره چو نور می بینم</p></div>
<div class="m2"><p>آفتابی بود قمر صورت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده می نوش و جام را دریاب</p></div>
<div class="m2"><p>معنئی بین و مینگر صورت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر چه بینیم صورت عشق است</p></div>
<div class="m2"><p>لاجرم عاشقیم بر صورت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون که معنی ماست صورت او</p></div>
<div class="m2"><p>نور چشمست و در نظر صورت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام گیتی نماست سید ما</p></div>
<div class="m2"><p>نعمت الله نموده در صورت</p></div></div>