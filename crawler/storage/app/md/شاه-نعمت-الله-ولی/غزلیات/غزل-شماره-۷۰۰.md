---
title: >-
    غزل شمارهٔ ۷۰۰
---
# غزل شمارهٔ ۷۰۰

<div class="b" id="bn1"><div class="m1"><p>خوش درددلی دارم درمان به چه کار آید</p></div>
<div class="m2"><p>با کفر سر زلفش ایمان به چه کار آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل زنده بود جانم چون کشتهٔ عشق اوست</p></div>
<div class="m2"><p>بی خدمت آن جانان این جان به چه کار آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل از سر مخموری سامان طلبد از ما</p></div>
<div class="m2"><p>ما عاشق سرمستیم سامان به چه کار آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق آمد و ملک دل بگرفت به سلطانی</p></div>
<div class="m2"><p>جز حضرت این سلطان به چه کار آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خلوت میخانه بزمی است ملوکانه</p></div>
<div class="m2"><p>روضه چو بود اینجا رضوان به چه کار آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماهان ز خدا خواهم با صحبت مه رویان</p></div>
<div class="m2"><p>بی صحبت مه رویان ماهان به چه کار آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با سید سرمستان کرمان چو بهشتی بود</p></div>
<div class="m2"><p>بی نور حضور او کرمان به چه کار آید</p></div></div>