---
title: >-
    غزل شمارهٔ ۱۴۰۸
---
# غزل شمارهٔ ۱۴۰۸

<div class="b" id="bn1"><div class="m1"><p>می نگارد نگار بر دیده</p></div>
<div class="m2"><p>می نماید چو نور در دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور روئی که چشم سر بیند</p></div>
<div class="m2"><p>دیدهٔ ما به چشم سر دیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر که بیند به عین ما ، ما را</p></div>
<div class="m2"><p>صدف و بحر و هم گهر دیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام می هر که دیده رندانه</p></div>
<div class="m2"><p>هست سیاح بحر و بر دیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیده هر ذره ای که می بیند</p></div>
<div class="m2"><p>آفتابیست در قمر دیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده دیده به نور او او را</p></div>
<div class="m2"><p>این نظر دیده ز آن نظر دیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه او نور نعمت الله دید</p></div>
<div class="m2"><p>جان و جانان به همدگر دیده</p></div></div>