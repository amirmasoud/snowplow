---
title: >-
    غزل شمارهٔ ۸۴۲
---
# غزل شمارهٔ ۸۴۲

<div class="b" id="bn1"><div class="m1"><p>بیا با یوسف کنعان به سر بر </p></div>
<div class="m2"><p>چو ما با او در این زندان به سر بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دلبر دل سپار و جان به جانان</p></div>
<div class="m2"><p>خوشی در خدمت جانان به سر بر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گردی گرد اغیاران شب و روز</p></div>
<div class="m2"><p>به جز یاران و با یاران به سر بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برابر دار تا سردار گردی</p></div>
<div class="m2"><p>به سرداری به سرداران به سر بر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به سوی ما بیا و آب و جو</p></div>
<div class="m2"><p>درین دریای بی پایان به سر بر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی با زاهد مخمور بنشین</p></div>
<div class="m2"><p>بیا با میر سرمستان به سر بر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خرابات است و ساقی نعمت الله</p></div>
<div class="m2"><p>توهم با سید رندان به سر بر</p></div></div>