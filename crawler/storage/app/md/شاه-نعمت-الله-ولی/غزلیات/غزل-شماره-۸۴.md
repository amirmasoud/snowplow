---
title: >-
    غزل شمارهٔ ۸۴
---
# غزل شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>نقد گنج کنت کنزا را طلب</p></div>
<div class="m2"><p>گوهر دُر یتیم از ما طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عاشقانه خم می را نوش کن</p></div>
<div class="m2"><p>جرعه ای بود بیا دریا طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از دوئی بگذر که تا یابی یکی</p></div>
<div class="m2"><p>از همه یکتای بی همتا طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارفانه دولت خود را بگیر</p></div>
<div class="m2"><p>آنچه گم کردی همه آنجا طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم عالم روشنست از نور او</p></div>
<div class="m2"><p>نور او در دیدهٔ بینا طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت الله است و عالم سر به سر</p></div>
<div class="m2"><p>نعمتی خوش از همه اشیا طلب</p></div></div>