---
title: >-
    غزل شمارهٔ ۷۳۴
---
# غزل شمارهٔ ۷۳۴

<div class="b" id="bn1"><div class="m1"><p>می محبت او نوش کن که نوشت باد</p></div>
<div class="m2"><p>بیا و خدمت او نوش کن که نوشت باد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب پاک هلال است و ساقی سرمست</p></div>
<div class="m2"><p>زلال نعمت او نوش کن که نوشت باد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همیشه رحمت او آبرو دهد ما را</p></div>
<div class="m2"><p>ز آب رحمت او نوش کن که نوشت باد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو جای جام و صراحی بیا به میخانه</p></div>
<div class="m2"><p>به قدر همت او نوش کن که نوشت باد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که قسمت ما کرده اند جام شراب</p></div>
<div class="m2"><p>خوشست قسمت او نوش کن که نوشت باد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسید ساقی کوثر حیات می بخشد</p></div>
<div class="m2"><p>ز دست حضرت او نوش کن که نوشت باد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شراب سید ما جرعه ای به صد جان است</p></div>
<div class="m2"><p>به یاد قیمت او نوش کن که نوشت باد</p></div></div>