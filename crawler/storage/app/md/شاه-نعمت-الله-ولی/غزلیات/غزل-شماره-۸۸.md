---
title: >-
    غزل شمارهٔ ۸۸
---
# غزل شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>همت از درویش صاحب دل طلب</p></div>
<div class="m2"><p>خدمت درویش کن حاصل طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد هجران از دل درویش جو</p></div>
<div class="m2"><p>راحت ار می جویی از واصل طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوهر ار خواهی درآ در بحر ما</p></div>
<div class="m2"><p>ور نمی خواهی برو ساحل طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حضرت جانانه را از جان بجو</p></div>
<div class="m2"><p>خدمت دلدار خود در دل طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشکلت حل وا شود گر طالبی</p></div>
<div class="m2"><p>هم ز طالب حل این مشکل طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در ره عشقش قدم مردانه نه</p></div>
<div class="m2"><p>رهبری صاحبدلی کامل طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قابل کامل اگر آری به دست</p></div>
<div class="m2"><p>نعمت الله را از آن قابل طلب</p></div></div>