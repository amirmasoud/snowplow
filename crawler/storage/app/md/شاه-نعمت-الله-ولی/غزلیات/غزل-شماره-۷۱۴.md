---
title: >-
    غزل شمارهٔ ۷۱۴
---
# غزل شمارهٔ ۷۱۴

<div class="b" id="bn1"><div class="m1"><p>عین او در عین اعیان شد پدید</p></div>
<div class="m2"><p>آن چنان پنهان چنین پیدا که دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابست او و عالم سایه بان</p></div>
<div class="m2"><p>چتر شاهی بر سر عالم کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامی از می پر ز می بستان بنوش</p></div>
<div class="m2"><p>این سخن از ما به جان باید شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هوای یوسف گل پیرهن</p></div>
<div class="m2"><p>همچو غنچه جامه را باید درید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف او آئینهٔ گیتی نما</p></div>
<div class="m2"><p>از برای حضرت خود آفرید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما حباب و عین ما آب حیات</p></div>
<div class="m2"><p>نوش کن جامی بگو هل من مزید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما از جمال پر کمال</p></div>
<div class="m2"><p>می نماید هر زمان حسنی پدید</p></div></div>