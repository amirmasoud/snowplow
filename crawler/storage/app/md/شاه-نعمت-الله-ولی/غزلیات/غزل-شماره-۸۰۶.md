---
title: >-
    غزل شمارهٔ ۸۰۶
---
# غزل شمارهٔ ۸۰۶

<div class="b" id="bn1"><div class="m1"><p>گر یار غار خواهی مائیم یار سید</p></div>
<div class="m2"><p>ور ذوق دوست جوئی ما دوستدار سید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آینه که بینی جام جهان نمائیست</p></div>
<div class="m2"><p>چون نور می نماید روی نگار سید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سید در انتظار است تا کی رسد اشارت</p></div>
<div class="m2"><p>گر چه بود جهانی در انتظار سید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صیاد عقل اول عالم بود شکارش</p></div>
<div class="m2"><p>سیمرغ قاف وحدت باشد شکار سید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاحبدلان کامل در عشق جان سپردند</p></div>
<div class="m2"><p>بر خاک ره فتاده در رهگذار سید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرجا که رند مستی است در گوشهٔ خرابات</p></div>
<div class="m2"><p>باشد چو دردمندان او درد خوار سید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفتم که می رساند ما را به حضرت او</p></div>
<div class="m2"><p>حق گفت نعمت الله این است کار سید</p></div></div>