---
title: >-
    غزل شمارهٔ ۴۱
---
# غزل شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>در خرابات فنا ملک بقا داریم ما</p></div>
<div class="m2"><p>خوش بقای جاودانی زین فنا داریم ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشته عشقیم و جان در کار جانان کرده ایم</p></div>
<div class="m2"><p>این حیات لایزالی خونبها داریم ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خم می درجوش و ما سرمست و ساقی در نظر</p></div>
<div class="m2"><p>غم ز مخموران این دوران کجا داریم ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام دُرد درد او شادی رندان می خوریم</p></div>
<div class="m2"><p>درد می دانیم و دایم این دوا داریم ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دیگران گر ملک و مال و تخت شاهی یافتند</p></div>
<div class="m2"><p>سهل باشد نزد ما زیرا خدا داریم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقد گنج عشق او در کنج دل ما دیده ایم</p></div>
<div class="m2"><p>این چنین گنجی طلب میکن ز ما داریم ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در طریق عاشقی عمریست تا ره می رویم</p></div>
<div class="m2"><p>رهبری چون نعمت الله رهنما داریم ما</p></div></div>