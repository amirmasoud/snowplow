---
title: >-
    غزل شمارهٔ ۱۰۵۶
---
# غزل شمارهٔ ۱۰۵۶

<div class="b" id="bn1"><div class="m1"><p>تا جمالش در تجلی دیده ام</p></div>
<div class="m2"><p>صورتش را عین معنی دیده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ام روشن به نور روی اوست</p></div>
<div class="m2"><p>لاجرم بیناست یعنی دیده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مست ومجنون ، روز و شب سرگشته ام</p></div>
<div class="m2"><p>تا به لیلی حسن لیلی دیده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذات من آئینه ، او آئینه دار</p></div>
<div class="m2"><p>هر دو را در یک تجلی دیده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غیر معشوقم نیاید در نظر</p></div>
<div class="m2"><p>عاشقان را گر چه خیلی دیده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا محیط دیده بر زد موج عشق</p></div>
<div class="m2"><p>هفت دریا را چو سیلی دیده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله یافتم در هر وجود</p></div>
<div class="m2"><p>با همه عشقی و میلی دیده ام</p></div></div>