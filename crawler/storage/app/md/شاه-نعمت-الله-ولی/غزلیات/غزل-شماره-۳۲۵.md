---
title: >-
    غزل شمارهٔ ۳۲۵
---
# غزل شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>چشم ما از نور رویش روشنست</p></div>
<div class="m2"><p>مهر و مه چون یوسف و پیراهنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور اول روح اعظم خوانمش</p></div>
<div class="m2"><p>بلکه او جان است و عالم چون تنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس او بزم سرمستان بود</p></div>
<div class="m2"><p>جرعه ای از جام او شیر افکنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق می گوید سخنها ورنه عقل</p></div>
<div class="m2"><p>در بیان آن معانی الکنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی گریزد عاشق از خار جفا</p></div>
<div class="m2"><p>کاو چو بلبل در هوای گلشنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خود کجا آید به چشم ما بهشت</p></div>
<div class="m2"><p>بر در میخانه ما را مسکن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را بسی جستم به جان</p></div>
<div class="m2"><p>چون بدیدم نعمت الله با من است</p></div></div>