---
title: >-
    غزل شمارهٔ ۹۸۴
---
# غزل شمارهٔ ۹۸۴

<div class="b" id="bn1"><div class="m1"><p>در محیطی فکنده ام زورق</p></div>
<div class="m2"><p>که دو عالم در اوست مستغرق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نتوان زورق از محیط شناخت</p></div>
<div class="m2"><p>یا وجود محیط از زورق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور خوشید در سپهر یکی است</p></div>
<div class="m2"><p>شد مرتب میان صبح و شفق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هو هو لا اله الا هو</p></div>
<div class="m2"><p>نیک دریاب سر این مغلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خود پرستی و ما و من گوئی</p></div>
<div class="m2"><p>راه گم کرده ای ایا احمق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدهٔ ما ندید غیری را</p></div>
<div class="m2"><p>تا گشودیم دیده را بر حق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جام می بخشد</p></div>
<div class="m2"><p>تا بنوشید راوق مطلق</p></div></div>