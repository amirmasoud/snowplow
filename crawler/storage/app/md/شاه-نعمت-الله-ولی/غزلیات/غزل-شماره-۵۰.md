---
title: >-
    غزل شمارهٔ ۵۰
---
# غزل شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>جامیست چه جم نما دل ما</p></div>
<div class="m2"><p>بنموده خدا به ما دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمع دل ماست نور عالم</p></div>
<div class="m2"><p>افروخت به خود خدا دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشقش بحریست بیکرانه</p></div>
<div class="m2"><p>خوش بحری و آشنا دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان عشقست و دل غلامش</p></div>
<div class="m2"><p>او پادشه و گدا دل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درد دل ما دوای جان است</p></div>
<div class="m2"><p>به زین چه کند دوا دل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عهدی بستیم و جاودان است</p></div>
<div class="m2"><p>پیوند نگار با دل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خلوت خاص سید ما</p></div>
<div class="m2"><p>او خانه خدا ، سرا دل ما</p></div></div>