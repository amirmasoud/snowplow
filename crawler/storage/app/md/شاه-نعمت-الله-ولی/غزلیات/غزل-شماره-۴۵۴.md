---
title: >-
    غزل شمارهٔ ۴۵۴
---
# غزل شمارهٔ ۴۵۴

<div class="b" id="bn1"><div class="m1"><p>مستیم و خرابیم و گرفتار خرابات</p></div>
<div class="m2"><p>سرگشته در آن کوچه چو پرگار خرابات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس پی کاری و حریفی و ندیمی</p></div>
<div class="m2"><p>ما را نبود کار به جز کار خرابات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر حلقهٔ رندان سراپردهٔ عشقیم</p></div>
<div class="m2"><p>هم صحبت ما خدمت خمار خرابات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از عقل مجو صورت میخانهٔ معنی</p></div>
<div class="m2"><p>از ما طلب ای یار تو اسرار خرابات</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در زمزمهٔ مطرب عشاق کلامم</p></div>
<div class="m2"><p>حیران شده ات بلبل گلزار خرابات</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غیرت آن شاهد سرمست یگانه</p></div>
<div class="m2"><p>دیّار نمی گنجد در دار خرابات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ایام به کام است و حریفان به مرادند</p></div>
<div class="m2"><p>از بندگی سید سردار خرابات</p></div></div>