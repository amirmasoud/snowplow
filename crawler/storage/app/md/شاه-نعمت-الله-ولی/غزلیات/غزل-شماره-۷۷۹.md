---
title: >-
    غزل شمارهٔ ۷۷۹
---
# غزل شمارهٔ ۷۷۹

<div class="b" id="bn1"><div class="m1"><p>به علی رغم عدو باز زدم جامی چند</p></div>
<div class="m2"><p>توبه بشکستم و وارستم از این خامی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم و رندی و خاصان سراپردهٔ عشق</p></div>
<div class="m2"><p>فارغ از سرزنش عام کالانعامی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرصت از دست مده زلف نگاری به کف آر</p></div>
<div class="m2"><p>می خور و وقت غنیمت شمر ایامی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنج میخانه مرا خلوت خاص است مدام</p></div>
<div class="m2"><p>زاهد و گوشهٔ محراب و دو سه عامی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوبهار است و گل اروجه میت نیست بیا</p></div>
<div class="m2"><p>برو از پیر خرابات بکن وامی چند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مغان از لب جام و لب یار ای ساقی</p></div>
<div class="m2"><p>به مراد دل خود یافته ام کامی چند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ار راه روی ، جز ره میخانه مرو</p></div>
<div class="m2"><p>بشنو از من که در این راه زدم گامی چند</p></div></div>