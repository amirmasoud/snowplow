---
title: >-
    غزل شمارهٔ ۶۰۲
---
# غزل شمارهٔ ۶۰۲

<div class="b" id="bn1"><div class="m1"><p>در دل به جز از خدا نگنجد</p></div>
<div class="m2"><p>چون او گنجد هوا نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل خلوت خاص حضرت اوست</p></div>
<div class="m2"><p>بیگانه و آشنا نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مائیم و نگار خوش کناری</p></div>
<div class="m2"><p>مویی به میان ما نگنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سلطان عشقست و عقل درویش</p></div>
<div class="m2"><p>در مجلس شه گدا نگنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردی دارم دوا ندارد</p></div>
<div class="m2"><p>با درد چنین دوا نگنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون نیست به جز یکی که گوید</p></div>
<div class="m2"><p>درد خود گنجد و یا نگنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوش خم میست نعمت الله</p></div>
<div class="m2"><p>در جام جهان نما نگنجد</p></div></div>