---
title: >-
    غزل شمارهٔ ۱۱۳۷
---
# غزل شمارهٔ ۱۱۳۷

<div class="b" id="bn1"><div class="m1"><p>بی‌نشانی را نشانش یافتیم</p></div>
<div class="m2"><p>گنج پنهانی عیانش یافتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت و معنی عالم دیده ایم</p></div>
<div class="m2"><p>این معانی را بیانش یافتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه عقل از دیدنش محروم ماند</p></div>
<div class="m2"><p>عاشقانه ناگهانش یافتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده ایم آئینهٔ گیتی نما</p></div>
<div class="m2"><p>آشکارا و نهانش یافتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلبر سرمست در کوی مغان</p></div>
<div class="m2"><p>در میان عاشقانش یافتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه آید در نظر ای نور چشم</p></div>
<div class="m2"><p>جسم او دیدیم و جانش یافتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مظهر ذات و صفات کبریا</p></div>
<div class="m2"><p>سید آخر زمانش یافتیم</p></div></div>