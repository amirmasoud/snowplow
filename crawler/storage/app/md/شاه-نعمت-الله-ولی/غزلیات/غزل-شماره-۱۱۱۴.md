---
title: >-
    غزل شمارهٔ ۱۱۱۴
---
# غزل شمارهٔ ۱۱۱۴

<div class="b" id="bn1"><div class="m1"><p>به عشق چشم بیمارت دلم بیمار می‌بینم</p></div>
<div class="m2"><p>ولی از نوش سیراب لبت تیمار می‌بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همیشه چشم سرمست تو را مخمور می‌یابم</p></div>
<div class="m2"><p>ولی در عین سرمستی خوش و هشیار می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لب لعلت چو می‌بوسم حدیثی بازمی‌گویم</p></div>
<div class="m2"><p>از آن طوطی نطق خود شکرگفتار می‌بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهال سرو بالای تو را بر دیده بنشانم</p></div>
<div class="m2"><p>چه نخلست اینکه چشم خویش برخوردار می‌بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عالم هرکجا حسن رخ خوبی که می‌باشد</p></div>
<div class="m2"><p>خیال عکس خورشید جمال یار می‌بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببین بی‌روی جانانه چه باشد حال جان و دل</p></div>
<div class="m2"><p>چو بی‌گل خاطر بلبل چنین افکار می‌بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو سید صوفی صافی که باشد ساکن خلوت</p></div>
<div class="m2"><p>ز عشقت بر سر بازار شسته زار می‌بینم</p></div></div>