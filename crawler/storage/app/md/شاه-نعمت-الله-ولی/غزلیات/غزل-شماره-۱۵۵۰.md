---
title: >-
    غزل شمارهٔ ۱۵۵۰
---
# غزل شمارهٔ ۱۵۵۰

<div class="b" id="bn1"><div class="m1"><p>ترک مستم می پرستم یللی</p></div>
<div class="m2"><p>ساغر باده به دستم یللی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عهد با ساقی ببستم تننا</p></div>
<div class="m2"><p>توبه را دیگر شکستم یللی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مدتی بوده اسیر بند عقل</p></div>
<div class="m2"><p>از چنین بندی بجستم یللی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست گشتم از خود و هر دو جهان</p></div>
<div class="m2"><p>از وجود عشق هستم یللی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دردسر می داد مخموری مرا</p></div>
<div class="m2"><p>باده خوردم باز رستم یللی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد هشیار را با من چه کار</p></div>
<div class="m2"><p>سید رندان مستم یللی</p></div></div>