---
title: >-
    غزل شمارهٔ ۱۴۶۹
---
# غزل شمارهٔ ۱۴۶۹

<div class="b" id="bn1"><div class="m1"><p>در وجود او یکی است تا دانی</p></div>
<div class="m2"><p>آن یکی بی شکی است تا دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز یکی نیست پادشاه وجود</p></div>
<div class="m2"><p>گر چه شکرلکی است تا دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر سپاهی ز لشکر سلطان</p></div>
<div class="m2"><p>شاه و خانی یکی است تا دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بیابی هزار موج حباب</p></div>
<div class="m2"><p>عین ایشان یکی است تا دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عقل در بارگاه حضرت عشق</p></div>
<div class="m2"><p>به مثل دلقکی است تا دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با محیطی که ما در آن غرقیم</p></div>
<div class="m2"><p>هفت بحر اندکی است تا دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که داند که ما چه می گوئیم</p></div>
<div class="m2"><p>یارکی زیرکی است تا دانی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نعمت الله که میرمستان است</p></div>
<div class="m2"><p>ساقی نیککی است تا دانی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>میر میران به نزد سید ما</p></div>
<div class="m2"><p>میرک خُردکی است تا دانی</p></div></div>