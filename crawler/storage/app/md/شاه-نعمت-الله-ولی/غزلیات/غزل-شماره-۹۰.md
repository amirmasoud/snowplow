---
title: >-
    غزل شمارهٔ ۹۰
---
# غزل شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>در محیط عشق ما گوهر طلب </p></div>
<div class="m2"><p>هفت دریا را نجو دیگر طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عود دل در مجمر سینه بسوز</p></div>
<div class="m2"><p>آنچنان عودی در این مجمر طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وصل آن محبوب بی همتای ما</p></div>
<div class="m2"><p>گر طلب داری از این خوشتر طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان باقی یابی از جانان خود</p></div>
<div class="m2"><p>گر فنا گردی چو یاران در طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این سر تو چون کلاه آن سراست</p></div>
<div class="m2"><p>سر بنه در پای او آن سر طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان چو جوئی حضرت جانان بجو</p></div>
<div class="m2"><p>دل رها کن خدمت دلبر طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کجا جام میئی یابی بنوش</p></div>
<div class="m2"><p>نعمت الله را در آن ساغر طلب</p></div></div>