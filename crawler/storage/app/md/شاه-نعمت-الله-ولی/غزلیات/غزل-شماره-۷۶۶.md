---
title: >-
    غزل شمارهٔ ۷۶۶
---
# غزل شمارهٔ ۷۶۶

<div class="b" id="bn1"><div class="m1"><p>نوریست که آن نور به آن نور توان دید</p></div>
<div class="m2"><p>هر دیده که آن دید یقین دان که چنان دید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام می عشق است که در دور روان است</p></div>
<div class="m2"><p>در دور قمر هر که نظر کرد روان دید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در آینه بنمود جمال و چه جمالی</p></div>
<div class="m2"><p>خود را چه به خود دید ، به خود خود نگران دید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشمی که نظر از نظر اهل نظر یافت</p></div>
<div class="m2"><p>در هر چه نظر کرد همین دید و همان دید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی نام و نشان شو که نشان نقش خیالیست</p></div>
<div class="m2"><p>این نیست نشانی که تو گوئی به نشان دید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوئی که مرا هست تمنای وصالش</p></div>
<div class="m2"><p>نقشی و خیالی است که درخواب توان دید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوریست که سید به همه خلق نماید</p></div>
<div class="m2"><p>یاری که نظر کرد به هر دیده عیان دید</p></div></div>