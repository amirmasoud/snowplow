---
title: >-
    غزل شمارهٔ ۷۸۶
---
# غزل شمارهٔ ۷۸۶

<div class="b" id="bn1"><div class="m1"><p>آفتابی را به مه بنموده اند</p></div>
<div class="m2"><p>خم می در ساغری پیموده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این عجب بنگر که پنهان گشته اند</p></div>
<div class="m2"><p>آفتابی را به گل اندوده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجلس مستانه ای بنهاده اند</p></div>
<div class="m2"><p>بر همه رندان دری بگشوده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باده نوشان در خرابات فنا</p></div>
<div class="m2"><p>فارغ از عالم خوش و آسوده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خیالش می نماید رو به خواب</p></div>
<div class="m2"><p>بی خیالش یک دمی نغنوده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشق و معشوق ما با همدگر</p></div>
<div class="m2"><p>هر کجا بودند با هم بوده اند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در ولایت حاکمی اولیا</p></div>
<div class="m2"><p>نعمت الله را عطا فرموده اند</p></div></div>