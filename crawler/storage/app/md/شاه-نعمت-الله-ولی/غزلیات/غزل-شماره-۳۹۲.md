---
title: >-
    غزل شمارهٔ ۳۹۲
---
# غزل شمارهٔ ۳۹۲

<div class="b" id="bn1"><div class="m1"><p>موج دریائیم و هر دو غیر آبی هست نیست</p></div>
<div class="m2"><p>در میان ما و او جز ناحجابی هست نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خرابات مغان هستند سر مستان ولی</p></div>
<div class="m2"><p>همچو من رند خوشی مست خرابی هست نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما شراب ذوق از آن لعل لبش نوشیده ایم</p></div>
<div class="m2"><p>خوبتر زین جام و خوشتر زان شرابی هست نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نیست هستی غیر آن سلطان بی همتای ما</p></div>
<div class="m2"><p>ورکسی گوید که هست آن در حسابی هست نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آفتاب روی او ذرات عالم روشن است</p></div>
<div class="m2"><p>درنظر پیداست غیر از آفتابی هست نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل اگر در خواب می بیند خیال دیگری</p></div>
<div class="m2"><p>اعتباری بر خیالی یا به خوابی هست نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله این سخن از ذوق می گوید مدام</p></div>
<div class="m2"><p>این چنین مستانه قولی در کتابی هست نیست</p></div></div>