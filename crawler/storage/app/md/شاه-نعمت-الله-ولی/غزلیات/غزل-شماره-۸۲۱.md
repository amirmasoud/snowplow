---
title: >-
    غزل شمارهٔ ۸۲۱
---
# غزل شمارهٔ ۸۲۱

<div class="b" id="bn1"><div class="m1"><p>نظری کن در آن جمال نگر</p></div>
<div class="m2"><p>حسن او بین و در کمال نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام گیتی نما به دست آور</p></div>
<div class="m2"><p>نور تمثال بی مثال نگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغر می بنوش رندانه</p></div>
<div class="m2"><p>آب سرچشمهٔ زلال نگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عالمند از او به خیال</p></div>
<div class="m2"><p>غیر او نیست این خیال نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق دارم که وصل او یابم</p></div>
<div class="m2"><p>طلب و طالب محال نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خرابات میر مستانیم</p></div>
<div class="m2"><p>حکم ما و نشان آل نگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را اگر یابی</p></div>
<div class="m2"><p>اثر ذوق او و حال نگر</p></div></div>