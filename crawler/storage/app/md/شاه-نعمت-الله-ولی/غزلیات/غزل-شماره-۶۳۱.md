---
title: >-
    غزل شمارهٔ ۶۳۱
---
# غزل شمارهٔ ۶۳۱

<div class="b" id="bn1"><div class="m1"><p>پادشاهی گدای او دارد</p></div>
<div class="m2"><p>سلطنت بی نوای او دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا خسرویست در عالم</p></div>
<div class="m2"><p>جان شیرین برای او دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور دیده ز چشمش اندازم</p></div>
<div class="m2"><p>دیگری گر به جای او دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی شد که این دل مستم</p></div>
<div class="m2"><p>عاشقانه هوای او دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان فدای بلای بالایش</p></div>
<div class="m2"><p>که دل من بلای او دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق مست است و جام می بر دست</p></div>
<div class="m2"><p>عقل مسکین چه پای او دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله با چنین نعمت</p></div>
<div class="m2"><p>چشم جان بر عطای او دارد</p></div></div>