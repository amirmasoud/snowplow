---
title: >-
    غزل شمارهٔ ۴۶
---
# غزل شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>مرا گفت یاری که ای یار ما</p></div>
<div class="m2"><p>اگر یار مائی بکش بار ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برو مایه و سود دکان بمان</p></div>
<div class="m2"><p>گرت هست سودای بازار ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا قول مستانهٔ ما شنو</p></div>
<div class="m2"><p>بخوان از سر ذوق گفتار ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نداریم ما کار با کار کس</p></div>
<div class="m2"><p>ندارد کسی کار با کار ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بندی تو نقش خیالی به خواب</p></div>
<div class="m2"><p>نظر کن درین چشم بیدار ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر رند مست و حریف خوشی</p></div>
<div class="m2"><p>بیابی مرادی ز خمار ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سزاوار ما نیست هر بنده ای </p></div>
<div class="m2"><p>بود سید ما سزاوار ما</p></div></div>