---
title: >-
    غزل شمارهٔ ۱۳۸
---
# غزل شمارهٔ ۱۳۸

<div class="b" id="bn1"><div class="m1"><p>مقام عاشقان در ملک جان است</p></div>
<div class="m2"><p>مکان عارفان در لامکان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرای می‌ فروشان حقیقی</p></div>
<div class="m2"><p>به خلوت خانهٔ اقلیم جان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو درد دل نمی دانی دوایش</p></div>
<div class="m2"><p>دوای درد دل سوز روان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشان و نام را بگذار و می ‌رو</p></div>
<div class="m2"><p>که راه کوی عشقش بی‌ نشان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نهان است از همه عالم ولیکن</p></div>
<div class="m2"><p>ز پیدائی عیان اندر عیان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیانی می‌کنم از صورت دوست</p></div>
<div class="m2"><p>در این معنی معانی را بیان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به دین سیدم چون نعمت‌الله</p></div>
<div class="m2"><p>برآنم من که دلدارم بر آن است</p></div></div>