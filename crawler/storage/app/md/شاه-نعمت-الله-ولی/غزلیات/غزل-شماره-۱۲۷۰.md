---
title: >-
    غزل شمارهٔ ۱۲۷۰
---
# غزل شمارهٔ ۱۲۷۰

<div class="b" id="bn1"><div class="m1"><p>مست بودی مست رفتی از جهان </p></div>
<div class="m2"><p>مست باشی مست خیزی جاودان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست خیزد هر که او سرمست رفت</p></div>
<div class="m2"><p>ور رود مخمور باشد همچنان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه ورزی دان که می ارزی همان</p></div>
<div class="m2"><p>قیمتت باشد به قدر این و آن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من نشان از بی نشانی یافتم</p></div>
<div class="m2"><p>بی نشان شو تا بیابی این نشان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا میان او گرفتم در کنار</p></div>
<div class="m2"><p>نیست غیری در کنار و در میان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیز دستی برفشان پائی بکوب</p></div>
<div class="m2"><p>سر فدا کن در سماع عارفان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله گر همی خواهی بجو</p></div>
<div class="m2"><p>همچو گنجی در دل صاحبدلان</p></div></div>