---
title: >-
    غزل شمارهٔ ۱۵۳۳
---
# غزل شمارهٔ ۱۵۳۳

<div class="b" id="bn1"><div class="m1"><p>توئی جانا که عین هر وجودی</p></div>
<div class="m2"><p>به خوبی دل ز خود هم خود ربودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبود این بود و بودی عین وحدت</p></div>
<div class="m2"><p>نمودی کثرت از وحدت که بودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جان صورت و معنی عیان شد</p></div>
<div class="m2"><p>چو بند برقع پنهان گشودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به چشم خود بدیدی حسن خود را</p></div>
<div class="m2"><p>جمال خود در آئینه نمودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو تو با شمع خود رازی بگفتی</p></div>
<div class="m2"><p>چه گویم آنچه خود گفتی شنودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جود او وجود جمله موجود</p></div>
<div class="m2"><p>عجب تو خود وجود عین جودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وجود هر دو عالم نزد سید</p></div>
<div class="m2"><p>نباشد جز وجود فی وجودی</p></div></div>