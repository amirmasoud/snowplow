---
title: >-
    غزل شمارهٔ ۱۲۸
---
# غزل شمارهٔ ۱۲۸

<div class="b" id="bn1"><div class="m1"><p>آئینهٔ ذات عین ذات است</p></div>
<div class="m2"><p>ذات است که مجمع صفات است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌ جود وجود حضرت او</p></div>
<div class="m2"><p>عالم به تمام فانیات است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می نوش مدام دُردی درد</p></div>
<div class="m2"><p>کاین دُردی درد دل دوات است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میخانهٔ ما است در خرابات</p></div>
<div class="m2"><p>و این خانه ورای شش جهات است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیراب شدند خلق عالم</p></div>
<div class="m2"><p>آری همه چیز ذو‌حیات است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کشته شوی به تیغ عشقش</p></div>
<div class="m2"><p>آن حی قدیم خونبهات است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید به حضور نعمت‌الله</p></div>
<div class="m2"><p>دایم به وضو و در صلات است</p></div></div>