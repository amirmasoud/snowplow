---
title: >-
    غزل شمارهٔ ۱۱۹۰
---
# غزل شمارهٔ ۱۱۹۰

<div class="b" id="bn1"><div class="m1"><p>ماییم کز جهان همه دل برگرفته‌ایم</p></div>
<div class="m2"><p>جان داده‌ایم و دامن دلبر گرفته‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست و خراب و عاشق و رندیم و باده‌نوش</p></div>
<div class="m2"><p>آب حیات از لب ساغر گرفته‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون مذهب قلندر رندی و عاشقی است</p></div>
<div class="m2"><p>رندانه ما طریق قلندر گرفته‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صدبار خوانده‌ایم کلام خدا تمام</p></div>
<div class="m2"><p>امروز فاتحه دگر از سر گرفته‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق آتشی گرفته و در جان ما زده</p></div>
<div class="m2"><p>ما شمع‌وار از آتش او در گرفته‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر لب گرفته‌ایم لب جام می مدام</p></div>
<div class="m2"><p>دامان ساقی و لب کوثر گرفته‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یاران ندیم مجلس ما نعمت الله است</p></div>
<div class="m2"><p>بنگر که ما حریف چه درخور گرفته‌ایم</p></div></div>