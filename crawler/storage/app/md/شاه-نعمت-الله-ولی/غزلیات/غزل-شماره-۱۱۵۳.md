---
title: >-
    غزل شمارهٔ ۱۱۵۳
---
# غزل شمارهٔ ۱۱۵۳

<div class="b" id="bn1"><div class="m1"><p>هر آن نقشی که بر دیده کشیدیم</p></div>
<div class="m2"><p>به جز نور جمال او ندیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرد نقطه چون پرگار گشتیم</p></div>
<div class="m2"><p>به آخر هم بدان اول رسیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو قطره غرق بحر عشق گشتیم</p></div>
<div class="m2"><p>محیطی را به یک دم در کشیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خراباتست و ما مست و خرابیم</p></div>
<div class="m2"><p>ز هر خم مئی جامی چشیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به عالم نعمت الله را نمودیم</p></div>
<div class="m2"><p>از آن دم روح در مَردم دمیدیم</p></div></div>