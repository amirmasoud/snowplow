---
title: >-
    غزل شمارهٔ ۱۱۴۵
---
# غزل شمارهٔ ۱۱۴۵

<div class="b" id="bn1"><div class="m1"><p>نور او عین این و آن دیدیم</p></div>
<div class="m2"><p>در همه آینه نهان دیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه بینیم ما به او بینیم</p></div>
<div class="m2"><p>تو چنین بین که ما چنان دیدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقطه در دور دایره بنمود</p></div>
<div class="m2"><p>خوش محیطی درین میان دیدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب جمال ظاهر گشت</p></div>
<div class="m2"><p>نور چشم محققان دیدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر حبابی که دید دیدهٔ ما</p></div>
<div class="m2"><p>عین او بحر بیکران دیدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیده او داد و نور او بخشید</p></div>
<div class="m2"><p>نور رویش به او روان دیدیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام گیتی نماست سید ما</p></div>
<div class="m2"><p>ما در آن نور انس و جان دیدیم</p></div></div>