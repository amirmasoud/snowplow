---
title: >-
    غزل شمارهٔ ۴۹۱
---
# غزل شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>خوش بود دردی که درمان او بود</p></div>
<div class="m2"><p>خرم آن جانی که جانان او بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کفر زلفش رونق ایمان ماست</p></div>
<div class="m2"><p>کفر کی باشد که ایمان او بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد عالم روز و شب گردیده ام</p></div>
<div class="m2"><p>دیده ام پیدا و پنهان او بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی نشانی آیتی در شأن اوست</p></div>
<div class="m2"><p>شأن او نام و نشان او بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موج دریائیم و دریا عین ماست</p></div>
<div class="m2"><p>هر چه ما داریم آن او بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عین او در عین ما چون شد عیان</p></div>
<div class="m2"><p>در همه عالم عیان او بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عارفانه گفتهٔ سید بخوان</p></div>
<div class="m2"><p>کاین معانی از بیان او بود</p></div></div>