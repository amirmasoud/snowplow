---
title: >-
    غزل شمارهٔ ۱۱۱۵
---
# غزل شمارهٔ ۱۱۱۵

<div class="b" id="bn1"><div class="m1"><p>نقش عالم خیال می بینم</p></div>
<div class="m2"><p>در خیال آن جمال می بینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم چو مظهر عشقند</p></div>
<div class="m2"><p>همه را بر کمال می بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغر باده ای که می نوشم</p></div>
<div class="m2"><p>عین آب زلال می بینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور چشمست و در نظر دارم</p></div>
<div class="m2"><p>از سر ذوق و حال می بینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آینه پیش دیده می دارم</p></div>
<div class="m2"><p>حسن او بی مثال می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک رندی و عاشقی کردن</p></div>
<div class="m2"><p>از دل خود محال می بینم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را چو می بینم</p></div>
<div class="m2"><p>صورت ذوالجلال می بینم</p></div></div>