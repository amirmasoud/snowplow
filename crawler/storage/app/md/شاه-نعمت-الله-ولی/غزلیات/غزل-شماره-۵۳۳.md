---
title: >-
    غزل شمارهٔ ۵۳۳
---
# غزل شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>مدام جام می او حیات می بخشد</p></div>
<div class="m2"><p>همیشه همت او کاینات می بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کمال بخشش ساقی نگر که رندان را</p></div>
<div class="m2"><p>شراب و جام ز ذات و صفات می بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلت به دردی دردش دوا کن و خوش باش</p></div>
<div class="m2"><p>که خسته ای و دم او شفات می بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه قدر خرقه که زنار بر میان داریم</p></div>
<div class="m2"><p>به جای کعبه به ما سومنات می بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیا که زنده دلان کشتگان معشوقند</p></div>
<div class="m2"><p>اگر تو کشتهٔ اوئی به مات می بخشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل یگانه من عاشقانه در دو سرا</p></div>
<div class="m2"><p>برای یک جهتی شش جهات می بخشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هزار رحمت حق بر روان سید ما</p></div>
<div class="m2"><p>که روح او دل ما را حیات می بخشد</p></div></div>