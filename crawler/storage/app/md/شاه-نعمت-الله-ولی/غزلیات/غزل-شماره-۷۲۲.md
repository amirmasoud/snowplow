---
title: >-
    غزل شمارهٔ ۷۲۲
---
# غزل شمارهٔ ۷۲۲

<div class="b" id="bn1"><div class="m1"><p>ترک می و میخانه به یک بار مگوئید</p></div>
<div class="m2"><p>با من سخن از زاهد زنار مگوئید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با عشاق سرمست مگوئید ز توبه</p></div>
<div class="m2"><p>ور زانکه بگوئید دگر بار مگوئید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رازی است میان من و ساقی خرابات</p></div>
<div class="m2"><p>از یار مپوشید و به اغیار مگوئید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با لعل لب او سخن از غنچه مپرسید</p></div>
<div class="m2"><p>با گلشن رویش سخن از خار مگوئید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از لعبت ترسا بچه اسلام مجوئید</p></div>
<div class="m2"><p>با زلف بتم قصهٔ زُنار مگوئید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سری که شنیدید امینید و امانت</p></div>
<div class="m2"><p>دارید نگه بر سر بازار مگوئید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از گفتهٔ سید غزلی خوش بنویسید</p></div>
<div class="m2"><p>اما سخنش جز بر خمار مگوئید</p></div></div>