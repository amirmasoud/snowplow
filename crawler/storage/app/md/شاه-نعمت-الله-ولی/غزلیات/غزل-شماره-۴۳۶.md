---
title: >-
    غزل شمارهٔ ۴۳۶
---
# غزل شمارهٔ ۴۳۶

<div class="b" id="bn1"><div class="m1"><p>نور چشم عالمی بر دیدهٔ ما جا گرفت</p></div>
<div class="m2"><p>این چنین نور خوشی در جای خود مأوا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخته می خواست تا آتش زند در جان او</p></div>
<div class="m2"><p>از میان سوختگان خویشتن ما را گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل مخمور است و ما مست و خراب افتاده ایم</p></div>
<div class="m2"><p>در چنین وقتی نباشد عقل را بر ما گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک دل بگرفت عشقش غارت جان می کند</p></div>
<div class="m2"><p>ترک سرمستی درآمد این ولایتها گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبتلائیم و بلا را مرحبائی می زنیم</p></div>
<div class="m2"><p>زانکه از بالای او این کار ما بالا گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به دست زلف او دادم دل سودا زده</p></div>
<div class="m2"><p>چون سر زلفش وجودم مو به مو سودا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در سرابستان میخانه حضوری دیگر است</p></div>
<div class="m2"><p>لاجرم سید حضوری یافت آنجا جاگرفت</p></div></div>