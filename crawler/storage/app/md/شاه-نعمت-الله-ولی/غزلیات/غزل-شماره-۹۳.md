---
title: >-
    غزل شمارهٔ ۹۳
---
# غزل شمارهٔ ۹۳

<div class="b" id="bn1"><div class="m1"><p>ساغر پر شراب را دریاب</p></div>
<div class="m2"><p>آب نوش و حباب را دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست نقش خیال جمله حجاب</p></div>
<div class="m2"><p>بی حجابست حجاب را دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب است و ماه خوانندش</p></div>
<div class="m2"><p>ماه بین آفتاب را دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه عالم سراب او سر آب</p></div>
<div class="m2"><p>سر آب و سراب را دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل صاحبدلان به دست آور</p></div>
<div class="m2"><p>جمع ام الکتاب را دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار خیر است عشق و میخواری</p></div>
<div class="m2"><p>کار خیر و ثواب را دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابات نعمت الله آی</p></div>
<div class="m2"><p>رند مست و خراب را دریاب</p></div></div>