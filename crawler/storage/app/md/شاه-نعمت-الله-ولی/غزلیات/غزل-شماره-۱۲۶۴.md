---
title: >-
    غزل شمارهٔ ۱۲۶۴
---
# غزل شمارهٔ ۱۲۶۴

<div class="b" id="bn1"><div class="m1"><p>چیست عالم سایه بان شمس دین</p></div>
<div class="m2"><p>این و آن باشد از آن شمس دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمس دین را دوست می دارم بجان</p></div>
<div class="m2"><p>می خورم سوگند جان شمس دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارفانه با تو می گویم سخن</p></div>
<div class="m2"><p>این معانی از بیان شمس دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور دین از شمس دین روشن شده</p></div>
<div class="m2"><p>دادمت اینک نشان شمس دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مجلس عشقست و ما مست و خراب</p></div>
<div class="m2"><p>باده نوشان عاشقان شمس دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر به بیت الله عزیمت می کنی</p></div>
<div class="m2"><p>راهرو با رهروان شمس دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله سید جانان بود</p></div>
<div class="m2"><p>گر چه هست از بندگان شمس دین</p></div></div>