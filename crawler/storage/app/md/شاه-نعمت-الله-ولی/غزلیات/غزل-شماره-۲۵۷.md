---
title: >-
    غزل شمارهٔ ۲۵۷
---
# غزل شمارهٔ ۲۵۷

<div class="b" id="bn1"><div class="m1"><p>جان ما با ما در این دریا نشست</p></div>
<div class="m2"><p>یار دریادل خوشی با ما نشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر هر دو جهان برخاست دل</p></div>
<div class="m2"><p>بر در یکتای بی همتا نشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات مغان ما را چو یافت</p></div>
<div class="m2"><p>مجلسی خوش دید و خوش آنجا نشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سر دار فنا دار بقاست</p></div>
<div class="m2"><p>بر سر دار آمد و از پا نشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما و ساقی خوش به هم بنشسته ایم</p></div>
<div class="m2"><p>خوش بود با مردم دانا نشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهد مخمور زیر افتاد و شد</p></div>
<div class="m2"><p>عاشق مست آمد و بالا نشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما نور چشم مردم است</p></div>
<div class="m2"><p>لاجرم بر دیدهٔ بینا نشست</p></div></div>