---
title: >-
    غزل شمارهٔ ۱۴۸۰
---
# غزل شمارهٔ ۱۴۸۰

<div class="b" id="bn1"><div class="m1"><p>زهی عقل و زهی دانش که تو خود را نمی دانی</p></div>
<div class="m2"><p>دمی باخود نپردازی کتاب خود نمی دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو تو نشناختی خود را چگونه عارف اوئی</p></div>
<div class="m2"><p>خدای خود نمی دانی بگو تا چون مسلمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خیالی نقش می بندی که کار بت پرستانست</p></div>
<div class="m2"><p>رهاکن این خیال خود که یابی زان پشیمانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر زلفش به دست آری بیابی مجمع دلها</p></div>
<div class="m2"><p>بسی جمعیتی یابی از آن زلف پریشانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر از میخانهٔ باقی می جام فنا نوشی</p></div>
<div class="m2"><p>حیات جاودان یابی و گردی ایمن از فانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حریف نعمت الله شو که تا جانت بیاساید</p></div>
<div class="m2"><p>که دارد در همه عالم چنین همصحبت جانی</p></div></div>