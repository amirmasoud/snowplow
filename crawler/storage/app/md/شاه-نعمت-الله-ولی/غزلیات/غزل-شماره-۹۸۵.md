---
title: >-
    غزل شمارهٔ ۹۸۵
---
# غزل شمارهٔ ۹۸۵

<div class="b" id="bn1"><div class="m1"><p>عشق است زیاده بر همه خلق</p></div>
<div class="m2"><p>عشقست فتاده بر همه خلق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق آمد و طرح نو بینداخت</p></div>
<div class="m2"><p>بنیاد نهاد بر همه خلق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی در آن سرای باقی</p></div>
<div class="m2"><p>از لطف گشاده بر همه خلق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خورشید جمال او عیان شد</p></div>
<div class="m2"><p>زان نور فتاده بر همه خلق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشود ز روی لطف و احسان</p></div>
<div class="m2"><p>جودش در داد بر همه خلق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق آمد و جام باده آورد</p></div>
<div class="m2"><p>جاویدان باد بر همه خلق</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقبول قبول نعمت الله</p></div>
<div class="m2"><p>شد خرم و شاد بر همه خلق</p></div></div>