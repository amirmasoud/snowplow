---
title: >-
    غزل شمارهٔ ۶۲۵
---
# غزل شمارهٔ ۶۲۵

<div class="b" id="bn1"><div class="m1"><p>پردهٔ دیدهٔ من نقش خیالت دارد</p></div>
<div class="m2"><p>دل شوریدهٔ من شوق وصالت دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا ماه رخی در نظرم می آید</p></div>
<div class="m2"><p>نیک می بینم و حسنی ز جمالت دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بینوائی که گدای سر کوی تو بود</p></div>
<div class="m2"><p>بر سلاطین جهان جاه و جلالت دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان فدا کردم و سر در قدمت افکندم</p></div>
<div class="m2"><p>از چنین بندگی ای بنده خجالت دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساقیا ساغر می ده که لبم بی لب جام</p></div>
<div class="m2"><p>به سر جملهٔ مستان که سلامت دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برو ای عقل که من مستم و تو مخموری</p></div>
<div class="m2"><p>توچه دانی که دل از عشق چه حالت دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله سخنش آب حیاتی است روان</p></div>
<div class="m2"><p>روح بخشد چه نصیبی ز زلالت دارد</p></div></div>