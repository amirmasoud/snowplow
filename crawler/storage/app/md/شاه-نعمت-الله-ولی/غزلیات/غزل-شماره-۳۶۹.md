---
title: >-
    غزل شمارهٔ ۳۶۹
---
# غزل شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>هر دل که به عشق مبتلا نیست</p></div>
<div class="m2"><p>هستش مشمر که گوئیا نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا دُردی درد نوش کردیم</p></div>
<div class="m2"><p>دل را به از این دگر دوا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رندیم و مدام جام رندان</p></div>
<div class="m2"><p>از ساقی و جام می جدا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستیم و خراب در خرابات</p></div>
<div class="m2"><p>ما را جائی دگر هوا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بحر محیط عشق غرقیم</p></div>
<div class="m2"><p>جز ما خبرش ز حال ما نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نقش که در خیال آید</p></div>
<div class="m2"><p>نیکش بنگر که بی خدا نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستیم و حریف نعمت الله</p></div>
<div class="m2"><p>حیف است که ذوق او تو را نیست</p></div></div>