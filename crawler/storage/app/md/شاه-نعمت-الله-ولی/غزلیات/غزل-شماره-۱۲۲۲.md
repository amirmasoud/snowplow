---
title: >-
    غزل شمارهٔ ۱۲۲۲
---
# غزل شمارهٔ ۱۲۲۲

<div class="b" id="bn1"><div class="m1"><p>هر زمان حسنی به هر دم می نماید نور چشم</p></div>
<div class="m2"><p>هر دمی بر ما دری دیگر گشاید نور چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما خیال عارضش بر آب دیده بسته ایم</p></div>
<div class="m2"><p>لاجرم لحظه به لحظه می فزاید نور چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دوش می گفتم خیالش را که از چشمم مرو</p></div>
<div class="m2"><p>ترک مردم هم به کلی می نشاید نور چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نباشد عشق او در جان نگیرد جان قرار</p></div>
<div class="m2"><p>ور نبیند نور روی او نیابد نور چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توتیائی چشم ما از خاک راهش ساخته</p></div>
<div class="m2"><p>تا غبار دیدهٔ ما را زداید نور چشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر سواد دیده هر نقشی که می بندد خیال</p></div>
<div class="m2"><p>در نظر نقش خیال او نماید نور چشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نور چشم نعمت الله گر شود روشن از او</p></div>
<div class="m2"><p>پیش مردم در همه جا بر سر آید نور چشم</p></div></div>