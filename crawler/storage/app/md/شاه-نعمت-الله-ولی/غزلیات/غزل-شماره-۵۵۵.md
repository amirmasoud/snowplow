---
title: >-
    غزل شمارهٔ ۵۵۵
---
# غزل شمارهٔ ۵۵۵

<div class="b" id="bn1"><div class="m1"><p>عشق او با جان ما پیوسته شد</p></div>
<div class="m2"><p>زنده آمد دل از آن پیوسته شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب چشم ما به گلشن رو نهاد</p></div>
<div class="m2"><p>غنچه گشت و خوش خوشی گلدسته شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق سرمست است و می گوید سرود</p></div>
<div class="m2"><p>عقل مخمور است از آن دل خسته شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغ دل در دام زلف او فتاد</p></div>
<div class="m2"><p>سر نهاد و مو به مو پابسته شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به او پیوست جان من تمام</p></div>
<div class="m2"><p>از همه کون و مکان خوش رسته شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دل من غیر او را راه نیست</p></div>
<div class="m2"><p>خانهٔ خالی ورا در بسته شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله عاشقانه جان بداد</p></div>
<div class="m2"><p>رند سرمست از جهان وارسته شد</p></div></div>