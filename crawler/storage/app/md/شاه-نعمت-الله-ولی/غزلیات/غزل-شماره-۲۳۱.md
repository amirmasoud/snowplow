---
title: >-
    غزل شمارهٔ ۲۳۱
---
# غزل شمارهٔ ۲۳۱

<div class="b" id="bn1"><div class="m1"><p>بحر بی پایان ما را آبروئی دیگر است</p></div>
<div class="m2"><p>چشمهٔ آب حیات ما ز جوئی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ و بوی این و آن نقش خیالی بیش نیست</p></div>
<div class="m2"><p>یار رندی شو که او را رنگ و بوئی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از می خمخانهٔ ما عالمی سرمست شد</p></div>
<div class="m2"><p>نوش کن جامی که این می از سبوئی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روی او بینم اگر آئینه بینم صدهزار</p></div>
<div class="m2"><p>روی او در هر یکی گوئی که روئی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاقلان راگفتگوی و عاشقان را های و هو</p></div>
<div class="m2"><p>گفتگو بگذار ما را های و هوئی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پردهٔ دیده به آب چشم خود ما شسته ایم</p></div>
<div class="m2"><p>پاک بازانیم و ما را شست و شوئی دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دیگران از طوع سید زلفها بربسته اند</p></div>
<div class="m2"><p>نعمة الله راز خون عشق طوعی دیگر است</p></div></div>