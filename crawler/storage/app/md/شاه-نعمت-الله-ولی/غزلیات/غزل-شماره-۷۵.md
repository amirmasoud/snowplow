---
title: >-
    غزل شمارهٔ ۷۵
---
# غزل شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>به سر خواجه کلان که مرا</p></div>
<div class="m2"><p>نبُود میل با کلاه شما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دنیی و آخرت نمی طلبم</p></div>
<div class="m2"><p>این و آن از کجا و ما ز کجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال امروز را غنیمت دان</p></div>
<div class="m2"><p>دی گذشت و نیامده فردا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوش کن گفته های مستانه</p></div>
<div class="m2"><p>چه کنی قول بو علی سینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات مست می گردم</p></div>
<div class="m2"><p>گر حریف منی بیا اینجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر زلف نگار در دستم</p></div>
<div class="m2"><p>با خیالش همی برم سودا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله چو آینهٔ روشن</p></div>
<div class="m2"><p>می نماید به ما خدا به خدا</p></div></div>