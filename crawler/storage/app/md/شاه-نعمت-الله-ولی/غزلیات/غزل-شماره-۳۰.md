---
title: >-
    غزل شمارهٔ ۳۰
---
# غزل شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در آمد ساقی و آورد جام می برای ما</p></div>
<div class="m2"><p>منور کرد نور او سرای که سرای ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه می های میخانه به ما انعام فرمودند</p></div>
<div class="m2"><p>کرم بنگر که الطافش چه ها کرده به جای ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراباتست و ما سرمست و ساقی جام می بر دست</p></div>
<div class="m2"><p>حیات جاودان یابی از آن آب و هوای ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در میخانه بگشادند و داد عاشقان دادند</p></div>
<div class="m2"><p>به حمدالله اجابت شد دعای کدخدای ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>حریف دردمندانیم و دُرد درد مینوشیم</p></div>
<div class="m2"><p>به ماده دُردی دردش که آن باشد دوای ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه خوش ذوقیست ذوق ما که عالم ذوق از او یابند</p></div>
<div class="m2"><p>نوای عالمی بخشد نوای بی نوای ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گدای نعمت اللهیم سلطان همه عالم</p></div>
<div class="m2"><p>بیا و پادشاهی کن ز انعام گدای ما</p></div></div>