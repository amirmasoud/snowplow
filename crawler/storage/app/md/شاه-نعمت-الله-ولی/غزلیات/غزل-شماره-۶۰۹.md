---
title: >-
    غزل شمارهٔ ۶۰۹
---
# غزل شمارهٔ ۶۰۹

<div class="b" id="bn1"><div class="m1"><p>خراباتست و خم در جوش و ساقی مست و ما بی خود</p></div>
<div class="m2"><p>سر از دستار نشناسیم و می از جام و نیک از بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حضور باده نوشان است و رندان جمله سرمستند</p></div>
<div class="m2"><p>نمی بینم کسی مخمور اگر یک بینم ور صد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر شمعی ز دلگرمی بپیچد از هوایش سر</p></div>
<div class="m2"><p>روان از آتش غیرت کشیدش تیغ بر سر زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آب و خاک میخانه مرا ایجاد فرمودند</p></div>
<div class="m2"><p>زهی جام و زهی باده زهی موجد زهی موجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در آن سرحد که جان بازند ما آنجا وطن داریم</p></div>
<div class="m2"><p>که دارد عشق همراهی که می آید بدان سرحد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گذر فرما به خاک ما زیارت کن دمی ما را</p></div>
<div class="m2"><p>که نور روح ما روشن توان دیدن در آن مرقد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صراط مستقیم من طریق نعمت الله است</p></div>
<div class="m2"><p>به عمر خود نمی گردم سر موئی ز راه خود</p></div></div>