---
title: >-
    غزل شمارهٔ ۹۲۹
---
# غزل شمارهٔ ۹۲۹

<div class="b" id="bn1"><div class="m1"><p>گر فسرده نیستی گر ما نه باش</p></div>
<div class="m2"><p>عاقلی ور عاشقی دیوانه باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشنائی گر کنی با عاشقان</p></div>
<div class="m2"><p>عاشقانه از خود بیگانه باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق بحر بیکران است ای پسر</p></div>
<div class="m2"><p>گر به دریا می روی مردانه باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد مغرور و کُنج صومعه</p></div>
<div class="m2"><p>تو مقیم گوشهٔ میخانه باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق دریا صورت تو چون صدف</p></div>
<div class="m2"><p>معنیش چون طالب دردانه باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع عشقش صورتی در ما فکند</p></div>
<div class="m2"><p>ذوق اگر داری بیا پروانه باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن رها کن جان به جانانه می سپار</p></div>
<div class="m2"><p>نعمت الله را بجو جانانه باش</p></div></div>