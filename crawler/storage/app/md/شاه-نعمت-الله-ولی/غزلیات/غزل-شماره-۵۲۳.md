---
title: >-
    غزل شمارهٔ ۵۲۳
---
# غزل شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>عین دریاییم و ما را موج دریا می‌کشد</p></div>
<div class="m2"><p>وین دل دریا دل ما سوی مأوا می‌کشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکل ما چون که حلوای لبش حل می‌کند</p></div>
<div class="m2"><p>دور نبود خاطر ما گر به حلوا می‌کشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست ما و دامن او آب چشم و خاک راه</p></div>
<div class="m2"><p>گرچه سرو قامت او دامن از ما می‌کشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جذبهٔ او می‌کشد ما را به میخانه مدام</p></div>
<div class="m2"><p>ما از آن خوش می‌رویم آنجا که ما را می‌کشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک سر مویی سخن از زلف او گفتم ولی</p></div>
<div class="m2"><p>شد پریشان خاطرم هم سر به سودا می‌کشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می‌کشد نقش خیال وی نماید در نظر</p></div>
<div class="m2"><p>هرکه بیند همچو ما بیند که زیبا می‌کشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را مدام از وی عطایی می‌رسد</p></div>
<div class="m2"><p>کار سید لاجرم هر لحظه بالا می‌کشد</p></div></div>