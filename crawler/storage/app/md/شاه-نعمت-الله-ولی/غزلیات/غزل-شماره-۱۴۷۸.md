---
title: >-
    غزل شمارهٔ ۱۴۷۸
---
# غزل شمارهٔ ۱۴۷۸

<div class="b" id="bn1"><div class="m1"><p>مرنجان جان باقی را برای این تن فانی</p></div>
<div class="m2"><p>دریغ از آن چنان جانی که بهر تن برنجانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به دشواری مخور خونی مشو ممنون هر دونی</p></div>
<div class="m2"><p>قناعت کن ز کسب خود بخور نانی به آسانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای دیو نفسانی مسخر کن سلیمانی</p></div>
<div class="m2"><p>چرا عاجز شدی آخر به دست دیو نفسانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شراب عشق او در کش که تا چون ما شوی سرخوش</p></div>
<div class="m2"><p>اگر فرمان نخواهی برد مخمورم تو می دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بزن شمشیر مردانه بگیر اقلیم شاهانه</p></div>
<div class="m2"><p>بیا بر تخت دل بنشین که در عالم تو سلطانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر دنیی اگر عقبی طلبکار همان ارزی</p></div>
<div class="m2"><p>هر آن چیزی که می ورزی حقیقت دان که خود آنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف نعمت الله شو که ذوق با خوشی یابی</p></div>
<div class="m2"><p>چرا مخمور می گردی مگر غافل ز یارانی</p></div></div>