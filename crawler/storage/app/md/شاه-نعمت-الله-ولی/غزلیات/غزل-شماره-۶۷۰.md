---
title: >-
    غزل شمارهٔ ۶۷۰
---
# غزل شمارهٔ ۶۷۰

<div class="b" id="bn1"><div class="m1"><p>هر کجا صاحبجمالی رو نمود</p></div>
<div class="m2"><p>روی او دیدم چو برقع برگشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیدمش در آینه عین العیان</p></div>
<div class="m2"><p>آینه او بود دوری می نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتاب خاطرم تا روشنست</p></div>
<div class="m2"><p>ذرهٔ بی مهر او هرگز نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه موجودست از جود ویست</p></div>
<div class="m2"><p>خود کجا موجود باشد بی وجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساجد و مسجود نزد ما یکیست</p></div>
<div class="m2"><p>سجده می کن تا ببینی در سجود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دوش رفتم درخرابات مغان</p></div>
<div class="m2"><p>ساقی سرمست دیدم یار بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکته های عارفانه سیدم</p></div>
<div class="m2"><p>خود به خود می گفت و از خود می شنود</p></div></div>