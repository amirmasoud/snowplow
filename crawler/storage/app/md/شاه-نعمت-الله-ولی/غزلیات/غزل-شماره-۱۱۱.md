---
title: >-
    غزل شمارهٔ ۱۱۱
---
# غزل شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>این طرفه بین که حضرت او با همه حجاب</p></div>
<div class="m2"><p>روشن تر است نور وی از نور آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موج و حباب و قطره و دریا به چشم ما</p></div>
<div class="m2"><p>عارف چو بنگرد بنماید به عین آب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیدار شو ز خواب به بیداریش ببین</p></div>
<div class="m2"><p>نقش خیال او نتوان دیدنش به خواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دستش به دست آور و دامان او بگیر</p></div>
<div class="m2"><p>جامی از او طلب کن و بستان ازو شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادی روی ساقی ما جام می بنوش</p></div>
<div class="m2"><p>تا همچو ما شوی ابدالمست و هم خراب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بگذار نور و ظلمت و بگذر ز روز و شب</p></div>
<div class="m2"><p>جانان ما طلب که بُود جان و تن حجاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>الهام سید است که گوید به بندگان</p></div>
<div class="m2"><p>ورنه چنین سخن نتوان گفت در کتاب</p></div></div>