---
title: >-
    غزل شمارهٔ ۳۱۳
---
# غزل شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>یاری که ز ملک آشنائی است</p></div>
<div class="m2"><p>داند که قماش ما کجائی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهد بر مست اگر کند میل</p></div>
<div class="m2"><p>آن میل به نزد ما هوائی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سلطانی این جهان فانی</p></div>
<div class="m2"><p>با همت عارفان گدائی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق ز بلا اگر گریزد</p></div>
<div class="m2"><p>در مذهب عشق بی وفائی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مائیم و نوای بی نوائی</p></div>
<div class="m2"><p>ما را چو نوا ز بی نوائی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتیم که غرق بحر عشقیم</p></div>
<div class="m2"><p>این مائی ما ز خودنمائی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مستیم و حریف نعمة الله</p></div>
<div class="m2"><p>این نیز عنایت خدائی است</p></div></div>