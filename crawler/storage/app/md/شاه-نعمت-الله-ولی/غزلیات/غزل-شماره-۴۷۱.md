---
title: >-
    غزل شمارهٔ ۴۷۱
---
# غزل شمارهٔ ۴۷۱

<div class="b" id="bn1"><div class="m1"><p>عاشقانی که در جهان باشند</p></div>
<div class="m2"><p>همچو جان در بدن روان باشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می و جامند همچو آب و حباب</p></div>
<div class="m2"><p>موج و دریا همین همان باشند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش کناری گرفته‌اند زَ اغیار</p></div>
<div class="m2"><p>گر چه با یار در میان باشند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از همه پادشه نشان دارند</p></div>
<div class="m2"><p>بی‌نشانی از آن نشان باشند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلق و حق را به ذوق دریابند</p></div>
<div class="m2"><p>واقف از سر این و آن باشند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت‌الله را به دست آور</p></div>
<div class="m2"><p>تا بدانی که آنچنان باشند</p></div></div>