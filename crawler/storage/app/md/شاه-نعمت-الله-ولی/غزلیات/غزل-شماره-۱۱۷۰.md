---
title: >-
    غزل شمارهٔ ۱۱۷۰
---
# غزل شمارهٔ ۱۱۷۰

<div class="b" id="bn1"><div class="m1"><p>این عنایت بین که ما دربارهٔ جان کرده‌ایم</p></div>
<div class="m2"><p>جان سرمست خوشی ایثار جانان کرده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنده‌ایم و بنده‌فرمانیم و فرمان می‌بریم</p></div>
<div class="m2"><p>هرچه ما کردیم در عالم به فرمان کرده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حضرتش سلطان و ما از جان غلام خدمتش</p></div>
<div class="m2"><p>مخلصانه تخت دل تسلیم سلطان کرده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات مغان بزم خوشی بنهاده‌ایم</p></div>
<div class="m2"><p>خان و مان زاهدی را نیک ویران کرده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام دُرد درد دل چون صاف درمان خورده‌ایم</p></div>
<div class="m2"><p>دردمندان را به دُرد درد درمان کرده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش در میخانهٔ مستانه‌ای بگشوده‌ایم</p></div>
<div class="m2"><p>نعمت الله را سبیل راه رندان کرده‌ایم</p></div></div>