---
title: >-
    غزل شمارهٔ ۱۳۷
---
# غزل شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>هر قطره‌ای از این بحر دریای بیکران است</p></div>
<div class="m2"><p>در چشم ما نظر کن بنگر که عین آن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر آینه که بینی تمثال او نماید</p></div>
<div class="m2"><p>آئینه این چنین بود تمثال آن چنان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زنده ‌دلان عالم دارند حیاتی از وی</p></div>
<div class="m2"><p>عالم تن است و او جان ، جان در بدن روان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما دیده‌ای که دیدیم روشن به نور او بود</p></div>
<div class="m2"><p>بنگر که نور رویش بر چشم ما عیان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گوشهٔ خرابات بزم خوشی است ما را</p></div>
<div class="m2"><p>بزمی چگونه بزمی فردوس جاودان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>معنی صورت او در این و آن نماید</p></div>
<div class="m2"><p>دریاب کان معانی برتر از این بیان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>منشور نعمت‌الله بگرفت جمله عالم</p></div>
<div class="m2"><p>توقیع آل سید بر حکم او نشان است</p></div></div>