---
title: >-
    غزل شمارهٔ ۱۱۵۶
---
# غزل شمارهٔ ۱۱۵۶

<div class="b" id="bn1"><div class="m1"><p>هرچه داریم از خدا داریم</p></div>
<div class="m2"><p>از خدایست هرچه ما داریم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرنه از حضرت خداوند است</p></div>
<div class="m2"><p>آنچه داریم از کجا داریم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج بحریم و عین ما آب است</p></div>
<div class="m2"><p>موج از بحر چون جدا داریم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساغر درد و درد می نوشیم</p></div>
<div class="m2"><p>بی تکلف نگر دوا داریم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نعمت الله عطای بار خداست</p></div>
<div class="m2"><p>خوش عطائی که از خدا داریم</p></div></div>