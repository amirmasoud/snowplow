---
title: >-
    غزل شمارهٔ ۱۲۸۱
---
# غزل شمارهٔ ۱۲۸۱

<div class="b" id="bn1"><div class="m1"><p>گر خدا خواهی جدا از خود مدان</p></div>
<div class="m2"><p>از خدا می دان خدا از خود مدان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر همه عالم به درویشی دهی</p></div>
<div class="m2"><p>لطف می فرما عطا از خود مدان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فاعل مختار در عالم یکی است</p></div>
<div class="m2"><p>در حقیقت فعل ما از خود مدان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما به او محتاج و او از ما غنی</p></div>
<div class="m2"><p>تو فقیری این غنا از خود مدان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از فنا و از بقا بگذر خوشی</p></div>
<div class="m2"><p>این فنا و این بقا از خود مدان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد او بخشد دوا هم او دهد</p></div>
<div class="m2"><p>عارفا درد و دوا از خود مدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در همه حالی که باشی ای عزیز</p></div>
<div class="m2"><p>نعمت الله را جدا از خود مدان</p></div></div>