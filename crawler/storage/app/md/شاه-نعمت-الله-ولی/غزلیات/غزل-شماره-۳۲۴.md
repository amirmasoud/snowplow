---
title: >-
    غزل شمارهٔ ۳۲۴
---
# غزل شمارهٔ ۳۲۴

<div class="b" id="bn1"><div class="m1"><p>نعمت الله جان و عالم چون تن است</p></div>
<div class="m2"><p>این چنین جان و تنی آن من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مصر دل دارم عزیز حضرتم</p></div>
<div class="m2"><p>جسم و جانم یوسف و پیراهنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورتم جام است و معنی می مدام</p></div>
<div class="m2"><p>عشق ساقی کار من می خوردن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حال ما از عقل می پرسی مپرس</p></div>
<div class="m2"><p>در بیان ذوق ما او الکن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رندم و در میکده دارم مقام</p></div>
<div class="m2"><p>جنت المأوی مدامم مسکن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع جمع عاشقان سر خوشم</p></div>
<div class="m2"><p>حال من بر اهل مجلس روشن است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام در دور است و سید در نظر</p></div>
<div class="m2"><p>خوش حضوری وقت جان پروردن است</p></div></div>