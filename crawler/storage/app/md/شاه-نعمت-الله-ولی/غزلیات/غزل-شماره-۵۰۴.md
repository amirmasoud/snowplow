---
title: >-
    غزل شمارهٔ ۵۰۴
---
# غزل شمارهٔ ۵۰۴

<div class="b" id="bn1"><div class="m1"><p>فعل عالم ظل فعل الله بود</p></div>
<div class="m2"><p>این کسی داند که او آگه بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مظهر افعال او باشد همه</p></div>
<div class="m2"><p>خود گدائی گیر و خواهی شه بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور می یابد قمر از آفتاب</p></div>
<div class="m2"><p>گر چه ظاهر نور ، نور مه بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرد دانا سر نپیچد زین سخن</p></div>
<div class="m2"><p>غیر نادانی که او گمره بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی شود مایل به سلطانی مصر</p></div>
<div class="m2"><p>هر که او با یوسفی در چه بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاک پایش توتیای چشم ماست</p></div>
<div class="m2"><p>رند سرمستی کز آن درگه بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر چه بینی نعمت الله بود</p></div>
<div class="m2"><p>نعمت الله در همه عالم یکی است</p></div></div>