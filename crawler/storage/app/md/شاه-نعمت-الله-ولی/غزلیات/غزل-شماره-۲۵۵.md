---
title: >-
    غزل شمارهٔ ۲۵۵
---
# غزل شمارهٔ ۲۵۵

<div class="b" id="bn1"><div class="m1"><p>جان ما با صحبت جانی خوش است</p></div>
<div class="m2"><p>صحبتم با آنکه می دانی خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک ماهان است و ما چون آفتاب</p></div>
<div class="m2"><p>مهر ما با ماه ماهانی خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهی می کنم از عشق او</p></div>
<div class="m2"><p>آری آری ذوق سلطانی خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از سر ذوق است این گفتار ما</p></div>
<div class="m2"><p>گر بدانی این سخن دانی خوش است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سید ما در همه عالم یکیست</p></div>
<div class="m2"><p>جامع مجموع اگر خوانی خوش است</p></div></div>