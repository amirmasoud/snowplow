---
title: >-
    غزل شمارهٔ ۱۲۳۲
---
# غزل شمارهٔ ۱۲۳۲

<div class="b" id="bn1"><div class="m1"><p>جانم فدای جان تو ای جان و ای جانان من</p></div>
<div class="m2"><p>کفر منست آن زلف تو هم روی تو ایمان من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آمد هوای زلف تو ایمان من خندان شده</p></div>
<div class="m2"><p>هر بلبلی برده گلی از گلشن و بستان من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من در میان با تو خوشم تو در کنار من خوشی</p></div>
<div class="m2"><p>موئی نگنجد در میان ، من آن تو تو آن من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رندان بزم خاص من هستند با ساقی حریف</p></div>
<div class="m2"><p>خمخانه در جوش آمده از مستی مستان من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صاحبنظر دانی که کیست یاری که باشد اهل دل</p></div>
<div class="m2"><p>گنج محبت یافته کنج دل ویران من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از دولت سلطان خود من در ولایت حاکمم</p></div>
<div class="m2"><p>هر کس کجا دستان کند با رستم دستان من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو سیدی من بنده ام تو خواجه ای و من غلام</p></div>
<div class="m2"><p>دعوی عشقت گر کنم سید بود برهان من</p></div></div>