---
title: >-
    غزل شمارهٔ ۴۲۸
---
# غزل شمارهٔ ۴۲۸

<div class="b" id="bn1"><div class="m1"><p>گرد و خاک ما روان بر باد رفت</p></div>
<div class="m2"><p>بنده زین گرد و غبار آزاد رفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما هرگز غم دنیا نخورد</p></div>
<div class="m2"><p>لاجرم او از جهان دلشاد رفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاشق سرمست آمد سوی ما</p></div>
<div class="m2"><p>عاقل مخمور بی بنیاد رفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یوسف مصری خوشی با مصر شد</p></div>
<div class="m2"><p>یار بغدادی سوی بغداد رفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یاد می کردم بهشت جاودان</p></div>
<div class="m2"><p>روی او دیدم بهشت از یاد رفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داد بخشد هر چه او بخشد به ما</p></div>
<div class="m2"><p>تا نپنداری به ما بیداد رفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دمی بی سید خود بوده ام </p></div>
<div class="m2"><p>حسرتی داریم کان بر باد رفت</p></div></div>