---
title: >-
    غزل شمارهٔ ۹۲
---
# غزل شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>رو فنا شو بیا بقا دریاب</p></div>
<div class="m2"><p>خوش بقائی از این فنا دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدمی نه درآ در این دریا</p></div>
<div class="m2"><p>عین ما را به عین ما دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُردی درد دل تو خوش می نوش</p></div>
<div class="m2"><p>دردمندانه آن دوا دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام گیتی نما به دست آور</p></div>
<div class="m2"><p>مظهر حضرت خدا دریاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاه و گدا نشسته به هم</p></div>
<div class="m2"><p>ذوق آن شاه و این گدا دریاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در میخانه را غنیمت دان</p></div>
<div class="m2"><p>دولت ملک دو سرا دریاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید رند مست اگر جوئی</p></div>
<div class="m2"><p>در خرابات بنده را دریاب</p></div></div>