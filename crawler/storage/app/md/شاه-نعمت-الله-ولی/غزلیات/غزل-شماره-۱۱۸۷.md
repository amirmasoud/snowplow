---
title: >-
    غزل شمارهٔ ۱۱۸۷
---
# غزل شمارهٔ ۱۱۸۷

<div class="b" id="bn1"><div class="m1"><p>تا خیال روی او بر دیده نقشی بسته ایم</p></div>
<div class="m2"><p>با خیالش روز و شب در گوشه ای بنشسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چشمست او از آن در دیده اش بنشانده ایم</p></div>
<div class="m2"><p>تا نبینندش در خلوتسرا بربسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدم جامیم و با ساقی نشسته روبرو</p></div>
<div class="m2"><p>عهد با او بسته ایم و عهد او نشکسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خرابات مغان با عاشقان همصحبتیم</p></div>
<div class="m2"><p>رند سرمستیم از دنیی و عقبی رسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق ما و نعمت الله جاودان باهم بود</p></div>
<div class="m2"><p>از ازل پیوسته ایم و تا ابد بگسسته ایم</p></div></div>