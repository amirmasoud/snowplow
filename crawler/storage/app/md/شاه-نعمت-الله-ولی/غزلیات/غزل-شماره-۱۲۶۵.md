---
title: >-
    غزل شمارهٔ ۱۲۶۵
---
# غزل شمارهٔ ۱۲۶۵

<div class="b" id="bn1"><div class="m1"><p>دیگران جانند و جانان شمس دین</p></div>
<div class="m2"><p>این و آن چون بنده ، سلطان شمس دین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفت هیکل آیتی در شأن اوست</p></div>
<div class="m2"><p>خوش بخوان قرآن و می دان شمس دین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل بود گنجینهٔ گنج اله</p></div>
<div class="m2"><p>نقد گنج کنج ویران شمس دین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بدر دین از شمس دین روشن شده</p></div>
<div class="m2"><p>نور بخش ماه تابان شمس دین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش خراباتی و مستان در حضور</p></div>
<div class="m2"><p>ساقی سرمست رندان شمس دین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چار یارانند امام انس و جان</p></div>
<div class="m2"><p>رهنمای چار یاران شمس دین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علم ما علم بدیعی دیگر است</p></div>
<div class="m2"><p>از معانی و بیان شمس دین</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چشم عالم روشن است از نور او</p></div>
<div class="m2"><p>دیده ام روشن به نور شمس دین</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شمس دین از نعمت الله می طلب</p></div>
<div class="m2"><p>زان که او دارد نشان شمس دین</p></div></div>