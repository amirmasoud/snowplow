---
title: >-
    غزل شمارهٔ ۱۳۱۲
---
# غزل شمارهٔ ۱۳۱۲

<div class="b" id="bn1"><div class="m1"><p>جان فدا کن وصل جانان را بجو</p></div>
<div class="m2"><p>دُرد دردش نوش درمان را بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق زلفش سر به سودا می کشد</p></div>
<div class="m2"><p>مجمع زلف پریشان را بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذر از صورت چو ما معنی طلب</p></div>
<div class="m2"><p>کفر را بگذار و ایمان را بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج او در کنج دل گر یافتی</p></div>
<div class="m2"><p>گنج را بگذار و سلطان را بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ذوق از مخمور نتوان یافتن</p></div>
<div class="m2"><p>ذوق خواهی خیز و مستان را بجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گوهر این بحر ما گر بایدت</p></div>
<div class="m2"><p>همچو غواصان تو عمان را بجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همت عالی نخواهد غیر آن</p></div>
<div class="m2"><p>گر تو عالی همتی آن را بجو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خرابات مغان ما را طلب</p></div>
<div class="m2"><p>می بنوش و راحت جان را بجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله جو که تا یابی امان</p></div>
<div class="m2"><p>ساقی سرمست رندان را بجو</p></div></div>