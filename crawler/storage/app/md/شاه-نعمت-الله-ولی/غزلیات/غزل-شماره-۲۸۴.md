---
title: >-
    غزل شمارهٔ ۲۸۴
---
# غزل شمارهٔ ۲۸۴

<div class="b" id="bn1"><div class="m1"><p>رند سرمست فارغ البال است</p></div>
<div class="m2"><p>بی غم از قال و ایمن از حال است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی که موجود ثانیش خوانند</p></div>
<div class="m2"><p>بر الف نزد عارفان دال است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر فدا کن چه قدر زر باشد</p></div>
<div class="m2"><p>خرقه چو بود که مال پامال است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواجه گر راه میکده گم کرد</p></div>
<div class="m2"><p>مرد هادی نگر که او ضال است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرچه بر عقل مشکلست ای یار</p></div>
<div class="m2"><p>حلَش از عشق جو گر اشکال است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق مشاطه ایست تا دانی</p></div>
<div class="m2"><p>بلکه صاحب تمیز و دلال است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل کل در بیان سید ما</p></div>
<div class="m2"><p>دم فرو بسته گوئیا لال است</p></div></div>