---
title: >-
    غزل شمارهٔ ۳۴۱
---
# غزل شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>جانم خیال شد به خیال خیال دوست</p></div>
<div class="m2"><p>دل بیقرار گشت به عشق وصال دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس به آرزوی جمالست در جهان</p></div>
<div class="m2"><p>مائیم و آرزوی خیال جمال دوست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر منیر چیست شعاعی ز روی یار</p></div>
<div class="m2"><p>یا کیست ماه نو چو غلامی هلال دوست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا زنگ غیر ز آئینهٔ دل زدوده ام</p></div>
<div class="m2"><p>در آینه ندیده ام الوصال دوست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مردم ندیده اند و گر سرو راستین</p></div>
<div class="m2"><p>بر جویبار دیدهٔ ما چون هلال دوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما را کمال نیست به خود ای عزیز ما</p></div>
<div class="m2"><p>داریم ما کمال ولی از کمال دوست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید تو بار جان منه اندر وثاق دل</p></div>
<div class="m2"><p>کاین خانه جای رخت بود یا محال دوست</p></div></div>