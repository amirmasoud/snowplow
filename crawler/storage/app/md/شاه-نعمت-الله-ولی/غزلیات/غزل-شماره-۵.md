---
title: >-
    غزل شمارهٔ ۵
---
# غزل شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>رند مستی جو دمی با او برآ</p></div>
<div class="m2"><p>از در میخانهٔ ما خوش درآ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس ما را غنیمت می شمر</p></div>
<div class="m2"><p>زانکه اینجا خوشتر از هر دو سرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام می بستان و مستانه بنوش</p></div>
<div class="m2"><p>قول ما می گو سرودی می سرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش خراباتی و خم می سبیل</p></div>
<div class="m2"><p>ما چنین مست و تو مخموری چرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آب چشم ما روان بر روی ما است</p></div>
<div class="m2"><p>باز می گویند با هم ماجرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه من امشب برآمد خوش خوشی</p></div>
<div class="m2"><p>تو بیا تا روز امشب خوش برآ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت دنیی و عقبی آن تو</p></div>
<div class="m2"><p>نعمت الله از همه عالم مرا</p></div></div>