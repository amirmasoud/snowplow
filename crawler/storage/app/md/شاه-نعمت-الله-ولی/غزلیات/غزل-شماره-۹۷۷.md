---
title: >-
    غزل شمارهٔ ۹۷۷
---
# غزل شمارهٔ ۹۷۷

<div class="b" id="bn1"><div class="m1"><p>وقت آن آمد که ما را باز بنوازی به لطف</p></div>
<div class="m2"><p>یک زمانی از کرم با ما بپردازی به لطف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حال ما گرچه خرابست ، از کرم معمور ساز</p></div>
<div class="m2"><p>خوش بود گر ساز ما را باز بنوازی به لطف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر چه بر خاک درم انداختی ای نور چشم</p></div>
<div class="m2"><p>چشم آن دارم که از چشمم نیندازی به لطف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب عالمی و عالمی در سایه ات</p></div>
<div class="m2"><p>لطف فرمائی و کار عالمی سازی به لطف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشقبازی می کنی با ما ولی پنهان ز ما</p></div>
<div class="m2"><p>این لطیفه گر که با ما عشق می بازی به لطف</p></div></div>