---
title: >-
    غزل شمارهٔ ۲۳۲
---
# غزل شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>سر درین راه عشق دردسر است</p></div>
<div class="m2"><p>بگذر از سر که کار معتبر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر موئی حجاب اگر باقی است</p></div>
<div class="m2"><p>بتراشش چه جای ریش و سرست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر بنه زیر پا و دستش گیر</p></div>
<div class="m2"><p>گر تو را میل تاج یا کمر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نفسی صحبتش غنیمت دان</p></div>
<div class="m2"><p>زانکه عمر عزیز در گذر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاهدان دیگرند و ما دیگر</p></div>
<div class="m2"><p>حالت ما و ذوق ما دگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقی کو ز ما خبر دارد</p></div>
<div class="m2"><p>از خود و کاینات بی خبر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نظری کن ببین به دیدهٔ ما</p></div>
<div class="m2"><p>نعمت الله چو نور در نظر است</p></div></div>