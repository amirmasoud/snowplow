---
title: >-
    غزل شمارهٔ ۲۳۵
---
# غزل شمارهٔ ۲۳۵

<div class="b" id="bn1"><div class="m1"><p>چشم مستش میفروشی دیگر است</p></div>
<div class="m2"><p>نوش لعلش باده نوشی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش عشقش دل ما را بسوخت</p></div>
<div class="m2"><p>داغ او بر دل فروشی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نالهٔ دلسوز ما بشنو دمی</p></div>
<div class="m2"><p>کاین دم ما را خروشی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق و مستیم و لایعقل ولی</p></div>
<div class="m2"><p>جان ما را فهم و هوشی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش ما و او به هم دوشی زدیم</p></div>
<div class="m2"><p>امشبم امید دوشی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرکه او تجرید گردد پیش او</p></div>
<div class="m2"><p>در طریقت خرقه پوشی دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خم می در جوش و ما مست وخراب</p></div>
<div class="m2"><p>سیدم در ذوق و جوشی دیگر است</p></div></div>