---
title: >-
    غزل شمارهٔ ۴۲
---
# غزل شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>جان چو عودست و دل چو مجمر ما</p></div>
<div class="m2"><p>آتش نور عشق دلبر ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب سپهر و جان جهان</p></div>
<div class="m2"><p>پرتوی دان ز رای انور ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نهر آب حیات و عین زلال </p></div>
<div class="m2"><p>قطره ای دان ز حوض کوثر ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهر تیغ مهر روشنزای</p></div>
<div class="m2"><p>ذره ای باشد آن ز خنجر ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنکه سلطان خلوت جانست</p></div>
<div class="m2"><p>بنده وار ایستاده بر در ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عرصهٔ کاینات و ما فیها</p></div>
<div class="m2"><p>خطه ای دان ز ملک و کشور ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دامن او و دست ما پس از این</p></div>
<div class="m2"><p>چون که آمد به خود فرو سر ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما نه مائیم با همه اوئیم</p></div>
<div class="m2"><p>اوئی او شده برابر ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سیدی از میانه چون برخواست</p></div>
<div class="m2"><p>خواجه و بنده شد یکی بر ما</p></div></div>