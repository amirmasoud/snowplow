---
title: >-
    غزل شمارهٔ ۴۷۳
---
# غزل شمارهٔ ۴۷۳

<div class="b" id="bn1"><div class="m1"><p>چشم ما روشن به نور او بود</p></div>
<div class="m2"><p>این چنین چشم خوشی نیکو بود </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روبروی خویش بنشیند چو ماه</p></div>
<div class="m2"><p>آئینه گر ساده و یک‌رو بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به دریا رفت و ماه در پیش</p></div>
<div class="m2"><p>حال دریا عاقبت تا چو شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق سرمست او می‌نوشد مدام</p></div>
<div class="m2"><p>عقل مخمور و بگفت و گو بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که باشد بندهٔ سلطان ما</p></div>
<div class="m2"><p>بر در او پادشه انجو بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از ازل یاری که دارد دولتی</p></div>
<div class="m2"><p>تا ابد دایم به جست و جو بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت‌الله میر سرمستان ما است</p></div>
<div class="m2"><p>میر میران نزد او میرو بود</p></div></div>