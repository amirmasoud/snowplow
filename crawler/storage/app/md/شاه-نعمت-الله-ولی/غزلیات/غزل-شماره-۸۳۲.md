---
title: >-
    غزل شمارهٔ ۸۳۲
---
# غزل شمارهٔ ۸۳۲

<div class="b" id="bn1"><div class="m1"><p>عقل غیر از عقال نیست دگر</p></div>
<div class="m2"><p>غایتش جز محال نیست دگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مدتی بحث او شنید ستم</p></div>
<div class="m2"><p>بجز از قیل و قال نیست دگر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مالک لم یزل خداوند است</p></div>
<div class="m2"><p>غیر او لایزال نیست دگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نوش کن جام می که خوشتر ازین</p></div>
<div class="m2"><p>هیچ آب زلال نیست دگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز خیال جمال حضرت او</p></div>
<div class="m2"><p>در خیالم جمال نیست دگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش کمالی که عاشقان دارند</p></div>
<div class="m2"><p>غیر از این خود کمال نیست دگر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله رسید تا جائی</p></div>
<div class="m2"><p>که سخن را مجال نیست دگر</p></div></div>