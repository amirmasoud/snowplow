---
title: >-
    غزل شمارهٔ ۵۵۰
---
# غزل شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>بلبل جان چو ساکن تن شد</p></div>
<div class="m2"><p>مجلس کاینات گلشن شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتاب وجوب رو بنمود</p></div>
<div class="m2"><p>شب امکان چون روز روشن شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گنج اسما نثار ما فرمود</p></div>
<div class="m2"><p>نقد هر یک از آن معین شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بود پیدا ولی نهان از ما</p></div>
<div class="m2"><p>آمد اینجا به ما مبین شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عین اول ظهور چون فرمود</p></div>
<div class="m2"><p>واضح و لائح و مبرهن شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام گیتی نما چو صیقل یافت</p></div>
<div class="m2"><p>حسن آمد به حسن و احسن شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جمال را بنمود</p></div>
<div class="m2"><p>نور او نور دیدهٔ من شد</p></div></div>