---
title: >-
    غزل شمارهٔ ۹۵۱
---
# غزل شمارهٔ ۹۵۱

<div class="b" id="bn1"><div class="m1"><p>چه خوش جمعیتی داریم از زلف پریشانش</p></div>
<div class="m2"><p>بود دلشاد جان ما که دلدار است جانانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیاور دُردی دردش که آن صاف دوای ماست</p></div>
<div class="m2"><p>کسی کو درد دل دارد همان درد است درمانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم گنجینهٔ عشقست و خوش گنجی در او پنهان</p></div>
<div class="m2"><p>چنین گنجی اگر جوئی بود درکنج ویرانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من ازذوق این سخن گفتم تو هم بشنو به ذوق از من</p></div>
<div class="m2"><p>بیا و قول مستانه روان مستانه می خوانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراباتست و ما سرمست و ساقی جام می بر دست</p></div>
<div class="m2"><p>سر ما و آستان او ، دست ما و دامانش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر تو آبرو جوئی بیا با من دمی بنشین</p></div>
<div class="m2"><p>که دریائیست بحر ما که پیدا نیست پایانش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>حریف نعمت الله شو که تا جانت بیاساید</p></div>
<div class="m2"><p>بنوش این ساغر می را به شادی روی یارانش</p></div></div>