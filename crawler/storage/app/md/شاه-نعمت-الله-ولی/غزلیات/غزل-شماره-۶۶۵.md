---
title: >-
    غزل شمارهٔ ۶۶۵
---
# غزل شمارهٔ ۶۶۵

<div class="b" id="bn1"><div class="m1"><p>این سعادت بین که ما را رو نمود</p></div>
<div class="m2"><p>حضرت بی چون نگویم چو نمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشن است آئینهٔ گیتی نما</p></div>
<div class="m2"><p>حسن روی او به ما نیکو نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دو آئینه یکی پیدا شده</p></div>
<div class="m2"><p>بیشکی باشد یکی و دو نمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی نیمشب بر ما بتافت</p></div>
<div class="m2"><p>نور او در چشم ما مه رو نمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه به ترکستان به ما بنمود ترک</p></div>
<div class="m2"><p>گه به هندستان به ما هندو نمود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در محیط بیکران افتاده ایم</p></div>
<div class="m2"><p>عین ما بر عین ما هر سود نمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما نظر از سید خود دیده ایم</p></div>
<div class="m2"><p>هم به نور دیدهٔ او او نمود</p></div></div>