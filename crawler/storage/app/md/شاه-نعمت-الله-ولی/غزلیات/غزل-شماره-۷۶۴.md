---
title: >-
    غزل شمارهٔ ۷۶۴
---
# غزل شمارهٔ ۷۶۴

<div class="b" id="bn1"><div class="m1"><p>جام می گر به دست ما برسد</p></div>
<div class="m2"><p>پادشاهی به این گدا برسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب جام شراب اگر بوسم</p></div>
<div class="m2"><p>خوش نوائی به بینوا برسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُردی درد دل اگر نوشم</p></div>
<div class="m2"><p>درد ما را از آن دوا برسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر جفا و وفا رسد ما را</p></div>
<div class="m2"><p>خوش بود هر چه از خدا برسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که فانی شود از این خانه</p></div>
<div class="m2"><p>به سراپردهٔ بقا برسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بحر عشق است و ما در او غرقیم</p></div>
<div class="m2"><p>هر که آید به آشنا برسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را به دست آرد</p></div>
<div class="m2"><p>هر غریبی که او به ما برسد</p></div></div>