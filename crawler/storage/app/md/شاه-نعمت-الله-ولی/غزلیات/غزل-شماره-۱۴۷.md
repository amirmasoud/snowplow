---
title: >-
    غزل شمارهٔ ۱۴۷
---
# غزل شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>نقش خیال اوست که گویند عالم است</p></div>
<div class="m2"><p>این صورتست و معنی آن اسم اعظم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسمی که هست جامع اسما به نزد ما</p></div>
<div class="m2"><p>آن اسم اعظمست و بر اسما مقدمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام جهان نماست پر از می بیابگیر</p></div>
<div class="m2"><p>شادی ما بنوش که جام می جم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سردار عاشقان به سر دار پا نهاد</p></div>
<div class="m2"><p>دعوی که می کند بر یاران مسلم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خمخانه ایست پر می و ساقی ما کریم</p></div>
<div class="m2"><p>رندان کم اند خواجه نگوئی که می کم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زخم عشق گرچه دلم ریش شد ولی</p></div>
<div class="m2"><p>ناله نمی کنم که چنان ریش مرهم است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با جام می دمی چو بر آریم خوش بود</p></div>
<div class="m2"><p>خاصه دمی که سید سرمست همدمست</p></div></div>