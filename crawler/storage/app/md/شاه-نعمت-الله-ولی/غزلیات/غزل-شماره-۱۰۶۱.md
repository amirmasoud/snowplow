---
title: >-
    غزل شمارهٔ ۱۰۶۱
---
# غزل شمارهٔ ۱۰۶۱

<div class="b" id="bn1"><div class="m1"><p>پادشاهی می کنم تا بنده ام</p></div>
<div class="m2"><p>روز و شب در بندگی پاینده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روشنم از آفتاب عشق او</p></div>
<div class="m2"><p>همچو ماهی بر همه تابنده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در هوای گلشن وصل نگار</p></div>
<div class="m2"><p>بر لب غنچه خوشی در خنده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا مگر بادی به خاکی بگذرد</p></div>
<div class="m2"><p>خویشتن بر خاک ره افکنده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان فدای عشق جانان کرده ام</p></div>
<div class="m2"><p>تا قیامت زین کرم شرمنده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا همه رندان من مستان شوند</p></div>
<div class="m2"><p>در خرابات مغان و امانده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساقی رندان بزم وحدتم</p></div>
<div class="m2"><p>سید سرمست خود را بنده ام</p></div></div>