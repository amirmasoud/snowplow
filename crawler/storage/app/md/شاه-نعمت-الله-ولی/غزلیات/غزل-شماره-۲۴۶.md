---
title: >-
    غزل شمارهٔ ۲۴۶
---
# غزل شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>سرم سرگشتهٔ سودای عشق است</p></div>
<div class="m2"><p>دلم آشفتهٔ غوغای عشق است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان دیده که بتوان دید او را</p></div>
<div class="m2"><p>دو چشم روشن بینای عشقست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حقیقت سرمهٔ چشم خردمند</p></div>
<div class="m2"><p>غبار گرد خاک پای عشقست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز عبرت غیر او از دل به در کن</p></div>
<div class="m2"><p>که غیر دل دگر نه جای عشقست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به شمع عشق جان و دل بسوزان</p></div>
<div class="m2"><p>چو پروانه گرت پروای عشق است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو از دی و از فردا و فردا</p></div>
<div class="m2"><p>که امروز وعدهٔ فردای عشق است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تن تنها در آ سید به خلوت</p></div>
<div class="m2"><p>که در خلوت تن تنهای عشقست</p></div></div>