---
title: >-
    غزل شمارهٔ ۴۰۸
---
# غزل شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>مطرب عشق ساز ما بنواخت</p></div>
<div class="m2"><p>به نوا جان بینوا بنواخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خرابات ساقی سرمست</p></div>
<div class="m2"><p>درد ما را به صد دوا بنواخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه بنواخت جان عالم را</p></div>
<div class="m2"><p>پادشاه است و این گدا بنواخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می نوازد به لطف عالم را</p></div>
<div class="m2"><p>دل این خسته بارها بنواخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبتلای بلای او بودم</p></div>
<div class="m2"><p>چاره ای کرد و مبتلا بنواخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهد غیر در سرای وجود</p></div>
<div class="m2"><p>به نهان خاطر مرا بنواخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شهرتی یافت در جهان که به عشق</p></div>
<div class="m2"><p>نعمت الله را خدا بنواخت</p></div></div>