---
title: >-
    غزل شمارهٔ ۲۹۹
---
# غزل شمارهٔ ۲۹۹

<div class="b" id="bn1"><div class="m1"><p>هرکه چون ما حریف مستان است</p></div>
<div class="m2"><p>در خرابات رند مستان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چشمست هرچه می بینم</p></div>
<div class="m2"><p>دل و دلدار و جان و جانان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی است برقعی بسته</p></div>
<div class="m2"><p>روشنش بین که ماه تابان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همه آئینهٔ جمال ویند</p></div>
<div class="m2"><p>نظری کن که عین اعیان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج اسماست در همه عالم</p></div>
<div class="m2"><p>گنج و گنجینهٔ فراوان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>موج و دریا دو رسم و دو اسمند</p></div>
<div class="m2"><p>نزد ما هر دو آب یکسان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قطره ای از محیط سید ماست</p></div>
<div class="m2"><p>به مثل گر چه بحر عمان است</p></div></div>