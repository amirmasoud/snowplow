---
title: >-
    غزل شمارهٔ ۵۷۷
---
# غزل شمارهٔ ۵۷۷

<div class="b" id="bn1"><div class="m1"><p>ما مریدیم و پیر ما مرشد</p></div>
<div class="m2"><p>ره روانیم و رهنما مرشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رو نوائی ز یار مرشد جو</p></div>
<div class="m2"><p>که دهد بی ریا نوا مرشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نبری ره به خانهٔ اصلی</p></div>
<div class="m2"><p>گر نیابی در این سرا مرشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روز و شب از خدای خود می جو</p></div>
<div class="m2"><p>کاملی تا بود تو را مرشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بحر ما را کرانه پیدا نیست</p></div>
<div class="m2"><p>غرق آبیم و عین ما مرشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دُرد دردش بنوش و خوش می باش</p></div>
<div class="m2"><p>که کند درد تو دوا مرشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که ارشاد نعمت الله یافت</p></div>
<div class="m2"><p>دائما خواهد از خدا مرشد</p></div></div>