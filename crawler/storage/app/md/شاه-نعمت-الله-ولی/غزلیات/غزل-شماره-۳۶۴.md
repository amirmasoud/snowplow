---
title: >-
    غزل شمارهٔ ۳۶۴
---
# غزل شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>دل جام جهان نمای شاهیست</p></div>
<div class="m2"><p>آئینهٔ حضرت الهی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقدیست دفینه در دل و دل</p></div>
<div class="m2"><p>گنجینهٔ گنج پادشاهی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روز و شب ماست زلف و رویش</p></div>
<div class="m2"><p>چه جای سفیدی و سیاهی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقشی که خیال غیر بندد</p></div>
<div class="m2"><p>در مذهب ما همه مناهی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بحر و محیط جان عالم</p></div>
<div class="m2"><p>در بحر محیط همچو ماهی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل دادن و جان نهاده بر سر</p></div>
<div class="m2"><p>در حضرت عشق عذرخواهیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پایه وجود نعمت الله</p></div>
<div class="m2"><p>پروردهٔ نعمت الهی است</p></div></div>