---
title: >-
    غزل شمارهٔ ۱۵۳۸
---
# غزل شمارهٔ ۱۵۳۸

<div class="b" id="bn1"><div class="m1"><p>ای ترک نیم مست به یغما خوش آمدی</p></div>
<div class="m2"><p>وی همچو جان نهفته پیدا خوش آمدی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>الا و مرحبا مگر از غیب می رسی</p></div>
<div class="m2"><p>ای شاهد شهادت رعنا خوش آمدی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خالی است خلوت دل ما از برای تو</p></div>
<div class="m2"><p>ور نه قدم به خلوت و فرما خوش آمدی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیشب خیال روی تو در خواب دیده ایم</p></div>
<div class="m2"><p>ای نور چشم در نظر ما خوش آمدی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلال عاشقان به سر چهارسوی عشق</p></div>
<div class="m2"><p>گلبانگ می زند که به سودا خوش آمدی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سرمست می رسی ز خرابات عاشقان</p></div>
<div class="m2"><p>دل برده ای به غارت جانها خوش آمدی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ای پادشاه صورت و معنی گدای تو</p></div>
<div class="m2"><p>وی سید مجرد یکتا خوش آمدی</p></div></div>