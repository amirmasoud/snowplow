---
title: >-
    غزل شمارهٔ ۴۶۱
---
# غزل شمارهٔ ۴۶۱

<div class="b" id="bn1"><div class="m1"><p>عشق آمد و عقل کرد غارت</p></div>
<div class="m2"><p>ای دل تو به جان بر این بشارت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ترک عجمیست عشق دانی</p></div>
<div class="m2"><p>ور ترک غریب نیست غارت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم به عبارتی در آرم</p></div>
<div class="m2"><p>وصف رخ او به استعارت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون آتش عشق او برافروخت </p></div>
<div class="m2"><p>هم عقل بسوخت هم عبارت</p></div></div>