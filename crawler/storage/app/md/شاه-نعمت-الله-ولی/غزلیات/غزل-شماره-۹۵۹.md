---
title: >-
    غزل شمارهٔ ۹۵۹
---
# غزل شمارهٔ ۹۵۹

<div class="b" id="bn1"><div class="m1"><p>ساقیم می رفت و رندان در پیش</p></div>
<div class="m2"><p>جام می بر دست و مستان در پیش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عزم کردم تا خرابات مغان</p></div>
<div class="m2"><p>عاشقان و می پرستان در پیش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نعره مستانه می زد دم به دم</p></div>
<div class="m2"><p>های و هوی باده نوشان در پیش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر به مستی عربده کردی دمی</p></div>
<div class="m2"><p>لطف فرمودی فراوان در پیش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون روان شد از برم عمر عزیز</p></div>
<div class="m2"><p>دل روان شد از بدن جان در پیش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در هوای بزم او نی در خروش</p></div>
<div class="m2"><p>چنگ با زلف پریشان در پیش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دُرد دردش نوش کن ای جان من</p></div>
<div class="m2"><p>تا بیابی صاف درمان در پیش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خضر رفته از پی ساقی ما</p></div>
<div class="m2"><p>نوش کرده آب حیوان در پیش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوش خرامان می رود مست و خراب</p></div>
<div class="m2"><p>نعمةالله و حریفان در پیش</p></div></div>