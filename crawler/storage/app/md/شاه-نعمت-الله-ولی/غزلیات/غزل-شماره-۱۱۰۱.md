---
title: >-
    غزل شمارهٔ ۱۱۰۱
---
# غزل شمارهٔ ۱۱۰۱

<div class="b" id="bn1"><div class="m1"><p>ما حلقه به گوش می فروشیم</p></div>
<div class="m2"><p>ما مست و خراب و باده نوشیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز اسرار الست در سماعیم</p></div>
<div class="m2"><p>وز جام بلاش در خروشیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم به هوای آتش دل</p></div>
<div class="m2"><p>چون بحر به خویشتن بجوشیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یک جرعه ز دُرد درد عشقش</p></div>
<div class="m2"><p>والله اگر به جان فروشیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می نوش تو پند و باده می نوش</p></div>
<div class="m2"><p>ز آن ساغر و خم که ما سبوشیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر دُرد دهد به ما و گر صاف</p></div>
<div class="m2"><p>شادی روان او بنوشیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید چو نگار ساقی ماست</p></div>
<div class="m2"><p>شاید که به می خوری بکوشیم</p></div></div>