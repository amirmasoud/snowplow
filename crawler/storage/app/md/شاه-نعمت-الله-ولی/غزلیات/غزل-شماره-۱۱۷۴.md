---
title: >-
    غزل شمارهٔ ۱۱۷۴
---
# غزل شمارهٔ ۱۱۷۴

<div class="b" id="bn1"><div class="m1"><p>عشق او در بحر و در بر دیده ایم</p></div>
<div class="m2"><p>نور او در خشک و در تر دیده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما روشن به نور او بود</p></div>
<div class="m2"><p>روی او چون ماه انور دیده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه هر دم می نماید صورتی</p></div>
<div class="m2"><p>معنی اینها مکرر دیده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در همه آئینه دیدیم آن یکی</p></div>
<div class="m2"><p>دیده ایم و بار دیگر دیده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گدائی را که می بینیم ما</p></div>
<div class="m2"><p>پادشاه تاج بر سر دیده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خبر از غیر می پرسی مپرس</p></div>
<div class="m2"><p>زانکه ما خود غیر کمتر دیده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما نور چشم ما بود</p></div>
<div class="m2"><p>نور آن پاکیزه منظر دیده ایم</p></div></div>