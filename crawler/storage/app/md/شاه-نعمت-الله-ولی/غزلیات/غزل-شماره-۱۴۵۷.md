---
title: >-
    غزل شمارهٔ ۱۴۵۷
---
# غزل شمارهٔ ۱۴۵۷

<div class="b" id="bn1"><div class="m1"><p>عشق تو گنجی و دل ویرانه ای</p></div>
<div class="m2"><p>مهر تو شمعی و جان پروانه ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل دوراندیش و ما در عشق تو</p></div>
<div class="m2"><p>نیست الا بیدلی دیوانه ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آشنای عشقت آن کس شد که او</p></div>
<div class="m2"><p>همچو ما گشت از خرد بیگانه ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کار ما از جام ساغر درگذشت</p></div>
<div class="m2"><p>ساقیا پر کن بده پیمانه ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی و صافی و کنج صومعه</p></div>
<div class="m2"><p>ما و یار و گوشهٔ میخانه ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غرقهٔ خوناب دل شد چشم ما</p></div>
<div class="m2"><p>در نظر داریم از آن دردانه ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشقی را سیدی باید چو من</p></div>
<div class="m2"><p>پاکبازی عارفی فرزانه ای</p></div></div>