---
title: >-
    غزل شمارهٔ ۱۱۰۶
---
# غزل شمارهٔ ۱۱۰۶

<div class="b" id="bn1"><div class="m1"><p>حضرتی غیر او نمی دانم</p></div>
<div class="m2"><p>گر تو دانی بگو نمی دانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر که گوید که غیر او باشد</p></div>
<div class="m2"><p>مشنو از وی بگو نمی دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عین او را به عین او جویم</p></div>
<div class="m2"><p>به از این جستجو نمی دانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خمخانه پاک می نوشم</p></div>
<div class="m2"><p>کوزه ای یا سبو نمی دانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برو ای عقل و گفتگو بگذار</p></div>
<div class="m2"><p>مستم و گفتگو نمی دانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هو هو لا اله الا هو</p></div>
<div class="m2"><p>من چه گویم جز او نمی دانم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید عاشقان یک رویم</p></div>
<div class="m2"><p>عاقلانه دو رو نمی دانم</p></div></div>