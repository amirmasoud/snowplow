---
title: >-
    غزل شمارهٔ ۱۴۶۷
---
# غزل شمارهٔ ۱۴۶۷

<div class="b" id="bn1"><div class="m1"><p>همه تقدیر اوست تا دانی</p></div>
<div class="m2"><p>همه زان رو نکوست تا دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسم و جان را به همدگر می بین</p></div>
<div class="m2"><p>بنگر آن مغز و پوست تا دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتهٔ عاشقان به جان بشنو</p></div>
<div class="m2"><p>غیر این گفتگوست تا دانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب باشد یکی و ظرف بسی</p></div>
<div class="m2"><p>گر چه مشک و سبوست تا دانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با تو گر ماجرا همی دادم</p></div>
<div class="m2"><p>غرضم شستشوست تا دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام گیتی نماست در نظرم</p></div>
<div class="m2"><p>جسم و جان روبروست تا دانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعت الله که نور دیدهٔ ماست</p></div>
<div class="m2"><p>مظهر لطف اوست تا دانی</p></div></div>