---
title: >-
    غزل شمارهٔ ۵۵۶
---
# غزل شمارهٔ ۵۵۶

<div class="b" id="bn1"><div class="m1"><p>بحر عشقش را کران پیدا نشد</p></div>
<div class="m2"><p>واصل دریای او جز ما نشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سرابستان مستان ره نبرد</p></div>
<div class="m2"><p>هر که چون ما سو به سو جویا نشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ ما تا نظر از وی نیافت</p></div>
<div class="m2"><p>چشم نابینای ما بینا نشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان ما تا مبتلای او نگشت</p></div>
<div class="m2"><p>کار دل در عاشقی والا نشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرفرازی در میان ما نیافت</p></div>
<div class="m2"><p>هر که را سر در سر سودا نشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در حریم عشق عاشق پی نبرد</p></div>
<div class="m2"><p>در ره معشوق تا پویا نشد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر پریشان کو نشد از جمع ما</p></div>
<div class="m2"><p>دولت پنهانیش پیدا نشد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که آمد سوی ما سرمست رفت</p></div>
<div class="m2"><p>هیچکس تشنه از این دریا نشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تا حدیث عشقبازی گفته اند</p></div>
<div class="m2"><p>همچو سید دیگری گویا نشد</p></div></div>