---
title: >-
    غزل شمارهٔ ۴۳۵
---
# غزل شمارهٔ ۴۳۵

<div class="b" id="bn1"><div class="m1"><p>تا که سودای خیالش در سویدا جا گرفت</p></div>
<div class="m2"><p>چون سر زلفش وجودم مو به مو سودا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در هوایش چون بنفشه ما ز پا افتاده ایم</p></div>
<div class="m2"><p>نرگسش عین عنایت از سر ما وا گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم ما بر پردهٔ دیده خیالش نقش بست</p></div>
<div class="m2"><p>خوش نگاری لاجرم در دیدهٔ ما جا گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روضهٔ رضوان نجوید میل جنت کی کند</p></div>
<div class="m2"><p>هر که در میخانهٔ ما همچو ما مأوا گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به جاروب مژه خاک درش را رفته ایم</p></div>
<div class="m2"><p>گرد و خاک آن در او ، دامَن ما را گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب چشم ما به هر سو رو نهاده می رود</p></div>
<div class="m2"><p>لاجرم از آب چشم ما جهان دریا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید ما گر جفائی می کند ما بنده ایم</p></div>
<div class="m2"><p>بندگان را کی رسد بر شاه بی همتا گرفت</p></div></div>