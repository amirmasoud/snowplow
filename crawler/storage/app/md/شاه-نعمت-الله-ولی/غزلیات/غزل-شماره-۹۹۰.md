---
title: >-
    غزل شمارهٔ ۹۹۰
---
# غزل شمارهٔ ۹۹۰

<div class="b" id="bn1"><div class="m1"><p>گر مشکگ را شکی باشد به یک</p></div>
<div class="m2"><p>کی موحد در یکی افتد به شک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوق بحر ما ز دریا دل طلب</p></div>
<div class="m2"><p>یا در آور بحر و می جو از سمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک سبو بر آب و یک کوزه پر آب</p></div>
<div class="m2"><p>آن یکی بسیار دارد این کمک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در نمکساز خوشی افتاده ایم</p></div>
<div class="m2"><p>هر که چون ما اوفتد گردد نمک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همدم جام می ار باشی دمی</p></div>
<div class="m2"><p>حاصل عمر عزیز است آن دمک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دُرد درد دل بود درمان ما</p></div>
<div class="m2"><p>زخم تیغ عشق بر دل مرهمک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم عشاقست و سید در نظر</p></div>
<div class="m2"><p>مست و دل شادیم و فارغ از غمک</p></div></div>