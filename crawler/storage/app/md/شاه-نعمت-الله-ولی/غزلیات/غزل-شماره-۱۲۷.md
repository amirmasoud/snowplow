---
title: >-
    غزل شمارهٔ ۱۲۷
---
# غزل شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>جامی ز حباب پر ز آب است</p></div>
<div class="m2"><p>آب است که صورتاً حباب است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در ظاهر و باطنش نظر کن</p></div>
<div class="m2"><p>دریاب حجاب آب ، آب است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن جام جهان نمای اول</p></div>
<div class="m2"><p>یک عین و صفات بی‌حساب است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی جود وجود چیست عالم</p></div>
<div class="m2"><p>گوئی سر آب نه ، سراب است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ماهی که تو را به شب نماید</p></div>
<div class="m2"><p>خورشید بُود که در نقاب است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقشی که خیال غیر بندد</p></div>
<div class="m2"><p>بگذار که آن خیال خواب است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر پرسندت که چیست توحید</p></div>
<div class="m2"><p>خاموشی تو ، تو را جواب است</p></div></div>