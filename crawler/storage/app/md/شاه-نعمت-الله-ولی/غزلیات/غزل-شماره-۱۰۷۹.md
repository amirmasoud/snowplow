---
title: >-
    غزل شمارهٔ ۱۰۷۹
---
# غزل شمارهٔ ۱۰۷۹

<div class="b" id="bn1"><div class="m1"><p>آتش عشقش خوشی افروختم</p></div>
<div class="m2"><p>نام و ننگ و نیک و بد را سوختم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوختم پروانهٔ جان و دلم</p></div>
<div class="m2"><p>شمع جمع عاشقان افروختم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خرقهٔ ناموس بدریدم دگر</p></div>
<div class="m2"><p>جامهٔ رندانه ای بردوختم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوهری بخریدم از صراف عشق</p></div>
<div class="m2"><p>نقد و نسیه در بها بفروختم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عالم عشقم چو من عالم کجاست</p></div>
<div class="m2"><p>عالمی را علم عشق آموختم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت الله حاصل عمر من است</p></div>
<div class="m2"><p>حاصل عمر خوشی اندوختم</p></div></div>