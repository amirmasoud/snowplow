---
title: >-
    غزل شمارهٔ ۱۴۵۰
---
# غزل شمارهٔ ۱۴۵۰

<div class="b" id="bn1"><div class="m1"><p>دولتت را که هست پاینده</p></div>
<div class="m2"><p>باد فرخنده سال آینده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سایهٔ دولت تو بر عالم</p></div>
<div class="m2"><p>باد چون آفتاب تابنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر در حضرتت ملازم وار </p></div>
<div class="m2"><p>جملهٔ خلق شاه تابنده</p></div></div>