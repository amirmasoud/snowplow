---
title: >-
    غزل شمارهٔ ۱۳۰۰
---
# غزل شمارهٔ ۱۳۰۰

<div class="b" id="bn1"><div class="m1"><p>من عین تو و تو عین وین عینین</p></div>
<div class="m2"><p>یک عین بود ظهور او در کونین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر گه که دو جام پر کنند از یک می</p></div>
<div class="m2"><p>این هر دو یکی باشد و آن یک اثنین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامی ز شراب خانه دارد رطلی</p></div>
<div class="m2"><p>جامی دگر از می مصفای متین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند که آب را نباشد لونی</p></div>
<div class="m2"><p>چون در دو قدح کنی نماید لونین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در شمس و قمر نگر که روشن بینی</p></div>
<div class="m2"><p>یک نور که رو نموده اندر عنین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر سلطنت صورت و معنی یابی</p></div>
<div class="m2"><p>شاهی گردی چو حضرت ذوالقرنین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زاهد به هوای جنتین و سید</p></div>
<div class="m2"><p>باشد بیدات جنتینش سجنین</p></div></div>