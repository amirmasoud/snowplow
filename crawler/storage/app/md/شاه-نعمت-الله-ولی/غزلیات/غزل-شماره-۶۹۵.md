---
title: >-
    غزل شمارهٔ ۶۹۵
---
# غزل شمارهٔ ۶۹۵

<div class="b" id="bn1"><div class="m1"><p>عقل چندان که خود بیاراید</p></div>
<div class="m2"><p>در نظر هیچ خوب ننماید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خاکساری است آبرویش نیست</p></div>
<div class="m2"><p>با دم سرد باده پیماید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بستهٔ او مشو که حیف بود</p></div>
<div class="m2"><p>کار عاشق ز عقل نگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشتهٔ عشق شو چو زنده دلان</p></div>
<div class="m2"><p>گر تو را عمر جاودان باید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که با عاشقی شود همدم</p></div>
<div class="m2"><p>از دم او دمی بیاساید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به عدم عالمی رود ز وجود</p></div>
<div class="m2"><p>به وجود جدید باز آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جان به جانان داد</p></div>
<div class="m2"><p>خوش بود گر قبول فرماید</p></div></div>