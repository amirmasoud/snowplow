---
title: >-
    غزل شمارهٔ ۲۱۰
---
# غزل شمارهٔ ۲۱۰

<div class="b" id="bn1"><div class="m1"><p>بیا ای شاه ترکستان که هندوستان غلام تست</p></div>
<div class="m2"><p>جهان صورت و معنی همه دیدم به کام تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به باطن آفتابی تو به ظاهر ماه خوانندت</p></div>
<div class="m2"><p>شده دور قمر روشن هم از بدر تمام تست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگرحوری اگر رضوان تو را بیند همی گویند</p></div>
<div class="m2"><p>سلام الله سلام الله سلام ما پیام تست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خدا عالم تو را بخشید ای سلطان انس و جان</p></div>
<div class="m2"><p>بهشت جاودان داری همه عالم زمام تست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان ساقی رندان که مستان ذوق می داند</p></div>
<div class="m2"><p>توئی آب حیات ما و جام جم ز جام تست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر چه ما و هم یاران سخن گوئیم مستانه</p></div>
<div class="m2"><p>ولی خوشتر ازین و آن کلام بانظام تست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تو خورشیدی و ما سایه منور گشته از نورت</p></div>
<div class="m2"><p>پناه نعمت اللهی همه در اهتمام تست</p></div></div>