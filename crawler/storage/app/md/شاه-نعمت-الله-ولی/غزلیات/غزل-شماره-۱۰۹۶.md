---
title: >-
    غزل شمارهٔ ۱۰۹۶
---
# غزل شمارهٔ ۱۰۹۶

<div class="b" id="bn1"><div class="m1"><p>منم که عاشق دیدار یار خود باشم</p></div>
<div class="m2"><p>منم که والهٔ زلف نگار خود باشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم که سیدم و بندهٔ خداوندم</p></div>
<div class="m2"><p>منم که دانه و دام شکار خود باشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم چو پرده و جانم امیر پرده نشین</p></div>
<div class="m2"><p>منم که میر خود و پرده دار خود باشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هر کنار که باشم از این میان به یقین</p></div>
<div class="m2"><p>چو نیک بنگرم اندر کنار خود باشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به گرد کوه و بیابان دگر نخواهم گشت</p></div>
<div class="m2"><p>به کنج دل روم و یار غار خود باشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چرا جفا کشم از هر کسی درین غربت</p></div>
<div class="m2"><p>به شهر خود روم و شهریار خود باشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به غیر عشق مرا نیست کاری و باری</p></div>
<div class="m2"><p>از آن مدام پی کار و بار خود باشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آنکه عاشق و معشوق نعمةاللهم</p></div>
<div class="m2"><p>به گرد کار خود و کردگار خود باشم</p></div></div>