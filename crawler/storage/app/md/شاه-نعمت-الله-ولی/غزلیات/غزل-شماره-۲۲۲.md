---
title: >-
    غزل شمارهٔ ۲۲۲
---
# غزل شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>منم آن رند عاشق سرمست</p></div>
<div class="m2"><p>که می عشق می خورم پیوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خرابات عشق مست و خراب</p></div>
<div class="m2"><p>دست در دست شاهد سرمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در دلم عشق و در سرم سود است</p></div>
<div class="m2"><p>در نظر یار و جام می بر دست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی مست و رند لایعقل</p></div>
<div class="m2"><p>به یکی جرعه عقل ما برده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشقانه حریف خمّاریم</p></div>
<div class="m2"><p>فارغ از نیست ایمنیم از هست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از سر هر دو کون خوش برخاست</p></div>
<div class="m2"><p>هر که یک لحظه نزد ما بنشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>میر مستان مجلس عشقیم</p></div>
<div class="m2"><p>سید عاشقان باده پرست</p></div></div>