---
title: >-
    غزل شمارهٔ ۳۱۴
---
# غزل شمارهٔ ۳۱۴

<div class="b" id="bn1"><div class="m1"><p>جامی ز می پر از می در بزم ما روان است</p></div>
<div class="m2"><p>هرگز که دیده باشد جامی که آنچنان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم بود چو جامی باده در او تجلی</p></div>
<div class="m2"><p>این جام و باده با هم مانند جسم و جان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از نور روی ساقی شد بزم ما منور</p></div>
<div class="m2"><p>و آن نور چشم مردم از دیده ها نهان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در عمر خود کناری خالی ندیدم از وی</p></div>
<div class="m2"><p>لطفش نگر که دایم با جمله در میان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جائیکه اسم باشد بی شک بود مسمی</p></div>
<div class="m2"><p>هر جا که منظری هست اسمی به نام آن است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آئینه ای که بینی روئی به تو نماید</p></div>
<div class="m2"><p>جام مئی که نوشی ساقی در آن میان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام و شراب و ساقی ، معشوق و عشق و عاشق</p></div>
<div class="m2"><p>هر سه یکیست اینجا این قول عاشقان است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیلاب رحمت او سیراب کرد ما را</p></div>
<div class="m2"><p>هر قطره ای از این بحر دریای بیکران است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دیدیم نعمت الله سرمست در خرابات</p></div>
<div class="m2"><p>میخانه در گشاده سر حلقهٔ مغان است</p></div></div>