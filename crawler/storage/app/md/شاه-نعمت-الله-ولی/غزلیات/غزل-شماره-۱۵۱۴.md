---
title: >-
    غزل شمارهٔ ۱۵۱۴
---
# غزل شمارهٔ ۱۵۱۴

<div class="b" id="bn1"><div class="m1"><p>در خرابات مجو همچو من میخواری</p></div>
<div class="m2"><p>که به عمری نتوان یافت چنین خماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار سودازدگان عاشقی و میخواریست</p></div>
<div class="m2"><p>هر کسی در پی کاری و سر بازاری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ما بود امینی و امانت عشقش</p></div>
<div class="m2"><p>آن امانت به امینی بسپارند آری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق او صدره اگرمی کشدم در روزی</p></div>
<div class="m2"><p>خونبها می دهمش از لب خود هر باری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کفر او رونق ایمان مسلمانان است</p></div>
<div class="m2"><p>بسته ام از سر زلفش به میان زُناری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غم من می خورد آن یار که جانم به فداش</p></div>
<div class="m2"><p>شادمانم ز غم یار چنین غمخواری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در همه مجلس رندان جهان گردیدم</p></div>
<div class="m2"><p>نیست چون سید سرمست دگر سرداری</p></div></div>