---
title: >-
    غزل شمارهٔ ۹۶۵
---
# غزل شمارهٔ ۹۶۵

<div class="b" id="bn1"><div class="m1"><p>بیا ای صوفی صافی می جام صفا درکش</p></div>
<div class="m2"><p>بیاور دُردی دردش به امید دوا درکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حریف بزم رندان شو چرا مخمور می گردی</p></div>
<div class="m2"><p>ز دست ساقی باقی می جام بقا درکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر کوی بلای او مقام مبتلایان است</p></div>
<div class="m2"><p>اگر تو از بلا ترسی عنان از کربلا درکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاک پاک سرمستی اگر گردی به دست آری</p></div>
<div class="m2"><p>روان در دیدهٔ جانت بسان توتیا درکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراباتست و می درجام و او معشوق می خواران</p></div>
<div class="m2"><p>اگر تو عاشق اوئی به عشق او بیا درکش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر در بزم جانبازان زمانی فرصتی یابی</p></div>
<div class="m2"><p>اجازت خواه مستانه بیا و خوش مرا درکش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی الله را وداعی کن مرید نعمت الله شو</p></div>
<div class="m2"><p>قدم در ملک باقی نه رقم گرد فنا درکش</p></div></div>