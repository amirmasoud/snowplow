---
title: >-
    غزل شمارهٔ ۱۹۹
---
# غزل شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>چشم مردم دیدهٔ ما نور رویش دیده است</p></div>
<div class="m2"><p>لاجرم در دیدهٔ ما همچو نور دیده است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر ذوق است این گفتار ما بشنو ز ما</p></div>
<div class="m2"><p>زانکه قول این چنین هرگز کسی نشنیده است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خیال آنکه نقش روی او بیند به چشم</p></div>
<div class="m2"><p>دیدهٔ اهل نظر گرد جهان گردیده است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تُرک چشم مست او دلها به غارت می برد</p></div>
<div class="m2"><p>زلف طرّارش به هر موئی دلی دزدیده است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عشق سرمست است و با رندان حریفی می کند</p></div>
<div class="m2"><p>عقل مخمور است و از زندان ما رنجیده است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کرم ساقی ما مِی می دهد ما را مدام</p></div>
<div class="m2"><p>بر سر ما آب رحمت گوئیا باریده است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکسی از لطف سلطانی نوائی یافتند</p></div>
<div class="m2"><p>حضرت او نعمت الله را به ما بخشیده است</p></div></div>