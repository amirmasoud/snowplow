---
title: >-
    غزل شمارهٔ ۸۶۴
---
# غزل شمارهٔ ۸۶۴

<div class="b" id="bn1"><div class="m1"><p>بشنو حضرتش به دست آور</p></div>
<div class="m2"><p>منصب خدمتش به دست آور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر خود را به پای او انداز</p></div>
<div class="m2"><p>دامن دولتش به دست آور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل ما راست همت عالی</p></div>
<div class="m2"><p>دل بجو همتش به دست آور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام گیتی نمای را بطلب</p></div>
<div class="m2"><p>مظهر رحمتش به دست آور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن حضوری که روحت افزاید</p></div>
<div class="m2"><p>در چنان حضرتش به دست آور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت الله را طلب می کن</p></div>
<div class="m2"><p>منعم و نعمتش به دست آور</p></div></div>