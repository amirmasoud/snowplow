---
title: >-
    غزل شمارهٔ ۱۳۴
---
# غزل شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>میخانه سرای عاشقان است</p></div>
<div class="m2"><p>رندی که چو ماست عاشق آن است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بستان و بنوش شادی ما</p></div>
<div class="m2"><p>جامی که به از شراب جان است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از ما نکند کناره معشوق</p></div>
<div class="m2"><p>با عاشق خویش در میان است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این دیده به نور اوست روشن</p></div>
<div class="m2"><p>آن نور به عین ما عیان است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتم عشقش نشان ندارد</p></div>
<div class="m2"><p>این نیز نشان بی نشان است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالم همه زنده دل به عشق ‌اند</p></div>
<div class="m2"><p>روحی است که در بدن روان است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ما را می جو ز نعمت‌الله</p></div>
<div class="m2"><p>کو غرقهٔ بحر بی‌ کران است</p></div></div>