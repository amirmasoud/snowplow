---
title: >-
    غزل شمارهٔ ۲۰۶
---
# غزل شمارهٔ ۲۰۶

<div class="b" id="bn1"><div class="m1"><p>آئینهٔ ذات عین ذاتست</p></div>
<div class="m2"><p>دانست که مجمع صفاتست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی جود وجود حضرت او</p></div>
<div class="m2"><p>عالم به تمام فانیاتست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می نوش مدام دُردی درد</p></div>
<div class="m2"><p>کین دُردی درد دل دواتست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میخانهٔ ماست در خرابات</p></div>
<div class="m2"><p>وین خانه ورای شش جهاتست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سیراب شدند اهل عالم</p></div>
<div class="m2"><p>آری همه چیز ذوحیاتست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر کشته شوی به تیغ عشقش</p></div>
<div class="m2"><p>آن حی قدیم خونبهاتست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید به حضور نعمة الله</p></div>
<div class="m2"><p>دایم به طهارت و صلاتست</p></div></div>