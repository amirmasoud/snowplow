---
title: >-
    غزل شمارهٔ ۹۸۹
---
# غزل شمارهٔ ۹۸۹

<div class="b" id="bn1"><div class="m1"><p>ای نهان کرده در آن تنگ شکربار نمک</p></div>
<div class="m2"><p>بسته ای پستهٔ خندان و در آن بار نمک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شوری از عشق تو در چار سوی جان افتاد</p></div>
<div class="m2"><p>به از این کس نبرد بر سر بازار نمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما ز شورابهٔ دیده نمکی آوردیم</p></div>
<div class="m2"><p>پیش همچو تو عزیزی نبود خار نمک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از نمکدان دهانت سخنی می گویم</p></div>
<div class="m2"><p>می کشم خوان کرم می کنم ایثار نمک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سخن من نمکین است برت می آرم</p></div>
<div class="m2"><p>می برم زیره به کرمان به نمکسار نمک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می خرامی و نمک از تو فرو می ریزد</p></div>
<div class="m2"><p>قدمی نه که خرم از تو به خروار نمک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نمکی ریخته ای بر دل ریش سید</p></div>
<div class="m2"><p>گرچه دل سوزدش اما کشد آزار نمک</p></div></div>