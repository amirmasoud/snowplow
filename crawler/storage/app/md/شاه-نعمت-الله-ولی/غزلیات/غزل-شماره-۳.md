---
title: >-
    غزل شمارهٔ ۳
---
# غزل شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>عارفی کو بُود ز آل عبا</p></div>
<div class="m2"><p>خواه گو خرقه پوش و خواه قبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان معنی طلب نه صورت تن</p></div>
<div class="m2"><p>تن بی جان چه می کند دانا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده می نوش و جام را می بین</p></div>
<div class="m2"><p>تا تن و جان تو بود زیبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه حق ظاهر است کی بیند</p></div>
<div class="m2"><p>دیدهٔ دردمند نابینا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>احمق است آنکه ما و حق گوید</p></div>
<div class="m2"><p>مرد عاشق نگوید این حاشا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یک وجود است و صد هزار صفت</p></div>
<div class="m2"><p>به وجود است این دوئی یکتا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می وحدت ز جام کثرت نوش</p></div>
<div class="m2"><p>نیک دریاب این سخن جانا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ما و کعبه حکایتی است غریب</p></div>
<div class="m2"><p>رند سرمست و جنت المأوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر در دیر تکیه گاه من است</p></div>
<div class="m2"><p>گر مرا طالبی بیا آنجا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>قطره و بحر و موج و جو آبند</p></div>
<div class="m2"><p>هر چه خواهی بجو ولی از ما</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نعمت الله را به دست آور</p></div>
<div class="m2"><p>با خدا باش با خدا خدا</p></div></div>