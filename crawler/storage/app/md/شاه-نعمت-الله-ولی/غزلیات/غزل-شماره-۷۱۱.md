---
title: >-
    غزل شمارهٔ ۷۱۱
---
# غزل شمارهٔ ۷۱۱

<div class="b" id="bn1"><div class="m1"><p>عاشق آن است که معشوق به جان می جوید</p></div>
<div class="m2"><p>می رود بی سروپا گرد جهان می جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مجنون همه جا لیلی خود می طلبد</p></div>
<div class="m2"><p>همه لیلی طلبد وز همگان می جوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می کند دلبر سرمست مرا دلجوئی</p></div>
<div class="m2"><p>بی تکلف دل من نیز چنان می جوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عارف از اول و آخر چو خبر می جوید</p></div>
<div class="m2"><p>ظاهر و باطن و پیدا و نهان می جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کسی آنچه طلب می کند ار داند باز</p></div>
<div class="m2"><p>دامن خویش به دست آرد و آن می جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>رسته از نام و نشان ، نام و نشان جوید نه</p></div>
<div class="m2"><p>رسته از نام و نشان ، نام و نشان می جوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله ز خدا از سر اخلاص مدام</p></div>
<div class="m2"><p>صحبت ساقی سرمست مغان می جوید</p></div></div>