---
title: >-
    غزل شمارهٔ ۶۸۲
---
# غزل شمارهٔ ۶۸۲

<div class="b" id="bn1"><div class="m1"><p>هستی ما همه بود به وجود</p></div>
<div class="m2"><p>نفسی بی وجود نتوان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنماید یکی به نقش و خیال</p></div>
<div class="m2"><p>در دو آئینه آن یکی دو نمود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جسم و جان ، جام و می ، دل و دلدار</p></div>
<div class="m2"><p>هر چه دارد همه به ما بنمود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو پرگار بود دل پر کار</p></div>
<div class="m2"><p>نقطه نقطه محیط را بنمود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اول و آخرش به هم پیوست</p></div>
<div class="m2"><p>ظاهر و باطنش ز هم آسود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لیس فی الدار غیره دیار</p></div>
<div class="m2"><p>هر موحد که بود این فرمود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله که میر مستان است</p></div>
<div class="m2"><p>در میخانه بر جهان بگشود</p></div></div>