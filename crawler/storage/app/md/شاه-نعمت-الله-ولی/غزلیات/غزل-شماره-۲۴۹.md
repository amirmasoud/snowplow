---
title: >-
    غزل شمارهٔ ۲۴۹
---
# غزل شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>عشق جانان در میان جان خوشست</p></div>
<div class="m2"><p>راز دلدار از جهان پنهان خوش است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>درد بی درمان او درمان ما</p></div>
<div class="m2"><p>در دلم این درد بی درمان خوش است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال سودائی زلف یار من</p></div>
<div class="m2"><p>همچو زلفش می برد سامان خوش است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق و گنجی و دل ویرانه ای</p></div>
<div class="m2"><p>آن چنان گنجی در این ویران خوشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جرعهٔ دُردی درد عشق او</p></div>
<div class="m2"><p>جان ما را دادهٔ جان آن خوش است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حال دل با عشق دلبر خوش بود</p></div>
<div class="m2"><p>جان ما پیوسته با جانان خوش است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله مست و جام می به دست</p></div>
<div class="m2"><p>جاودان در بزم سرمستان خوش است</p></div></div>