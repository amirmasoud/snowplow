---
title: >-
    غزل شمارهٔ ۱۵۴۲
---
# غزل شمارهٔ ۱۵۴۲

<div class="b" id="bn1"><div class="m1"><p>ای از جمال رویت نقش جهان خیالی</p></div>
<div class="m2"><p>وی ز آفتاب رویت هر ذره ای هلالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این مظهر مطهر روشن شد از جمالت</p></div>
<div class="m2"><p>در آینه نمودی تمثال بی مثالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از چشم پر خمارت هر گوشه نیم مستی</p></div>
<div class="m2"><p>وز لعل شکرینت در هر طرف زلالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم هوا که گردم خاک در سرایت</p></div>
<div class="m2"><p>این دولت ار بیابم ما را بود کمالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صوفی و کنج خلوت رند و شرابخانه</p></div>
<div class="m2"><p>هر یک به جستجوئی باشند و ما به حالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در خلوت سرایت جان خواست تا درآید</p></div>
<div class="m2"><p>گفتم مرو مبادا یابد ز تو ملالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید خیال رویت پیوسته بسته با دل</p></div>
<div class="m2"><p>ای جان من که دارد خوشتر ازین خیالی</p></div></div>