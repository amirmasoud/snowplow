---
title: >-
    غزل شمارهٔ ۱۰۵۱
---
# غزل شمارهٔ ۱۰۵۱

<div class="b" id="bn1"><div class="m1"><p>خبر از دل اگر پرسی منم کز دل خبر دارم</p></div>
<div class="m2"><p>به چشم من ببین رویش که دائم در نظر دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>منم صوفی ملک دل که باشد شکر او وردم</p></div>
<div class="m2"><p>منم عطار شهر جان که در دکان شکر دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مروا ی عاشق صادق که من معشوق جانانم</p></div>
<div class="m2"><p>بیا ای بلبل شیدا که من گلهای تر دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>منم آن شمع مومین دل که می سوزم به عشق او</p></div>
<div class="m2"><p>ضمیر روشنم بنگر که چون در جان شرر دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو از می گشته ای مخمور و من سرمست ساقیم</p></div>
<div class="m2"><p>تو را چیز دگر دادند و من چیز دگر دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز هر خاکی که می بینی در او کان زری باشد</p></div>
<div class="m2"><p>ز من جو نقد این معنی که در دریا گهر دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر عزم سفر داری بیا تا رهبرت باشم</p></div>
<div class="m2"><p>که تا گوئی در این عالم چو سید راهبر دارم</p></div></div>