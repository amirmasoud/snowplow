---
title: >-
    غزل شمارهٔ ۸۵
---
# غزل شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>دردمندانه بیا ما را طلب</p></div>
<div class="m2"><p>درد دل جانا ز بودردا طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چنین دریای بی پایان درآ</p></div>
<div class="m2"><p>عین ما را هم ز عین ما طلب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طالب و مطلوب را با هم نگر</p></div>
<div class="m2"><p>جای آن بی جای ما هر جا طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم ما روشن به نور روی اوست</p></div>
<div class="m2"><p>نور او در دیدهٔ بینا طلب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا کنجیست گنجی در ویست</p></div>
<div class="m2"><p>گنج اسما در همه اشیا طلب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفانه دامن هر یک بگیر</p></div>
<div class="m2"><p>حضرت یکتای بی همتا طلب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرابات مغان مستانه رو</p></div>
<div class="m2"><p>نعمت الله را در آنجا وا طلب</p></div></div>