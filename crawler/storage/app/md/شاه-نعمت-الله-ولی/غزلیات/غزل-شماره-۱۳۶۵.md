---
title: >-
    غزل شمارهٔ ۱۳۶۵
---
# غزل شمارهٔ ۱۳۶۵

<div class="b" id="bn1"><div class="m1"><p>بود ما پیدا شده از بود او</p></div>
<div class="m2"><p>لاجرم داریم ما بودی نکو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل می گوید مگو اسرار عشق</p></div>
<div class="m2"><p>عشق می گوید سخن مستانه گو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا میانش در کنار آورده ایم</p></div>
<div class="m2"><p>مو نمی گنجد میان ما و او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدهٔ ما هر یکی بیند یکی</p></div>
<div class="m2"><p>چشم احول گر یکی بیند به دو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غرق دریائیم و گویا تشنه ایم</p></div>
<div class="m2"><p>آب می جوئیم ما در بحر و جو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خوش درین دریای بی پایان در آ</p></div>
<div class="m2"><p>تا ببینی عین ما را سو به سو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آینه داریم دایم در نظر</p></div>
<div class="m2"><p>سید و بنده نشسته روبرو</p></div></div>