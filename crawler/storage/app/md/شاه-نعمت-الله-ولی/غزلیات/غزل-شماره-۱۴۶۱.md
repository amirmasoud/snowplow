---
title: >-
    غزل شمارهٔ ۱۴۶۱
---
# غزل شمارهٔ ۱۴۶۱

<div class="b" id="bn1"><div class="m1"><p>ای که می گوئی که هستم از منی</p></div>
<div class="m2"><p>از منی بگذر که این دم با منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیش کاید آدمی اندر وجود</p></div>
<div class="m2"><p>معنیش جان بود و در صورت منی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از منی بگذر چو مردان خدا</p></div>
<div class="m2"><p>کز منی پیدا شود مرد و زنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سروری یابی چو سرداران عشق</p></div>
<div class="m2"><p>گر به پای عاشقان سرافکنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان تو چون یوسف و تن پیرهن</p></div>
<div class="m2"><p>یوسف مصری نه این پیراهنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز هر دل روزنی با حق بود</p></div>
<div class="m2"><p>خاطر موری سزد گر نشکنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله جو که تا یابی مراد</p></div>
<div class="m2"><p>بگذر از دنیا که دونست و دنی</p></div></div>