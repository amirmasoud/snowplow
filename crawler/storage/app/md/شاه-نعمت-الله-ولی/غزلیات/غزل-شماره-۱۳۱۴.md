---
title: >-
    غزل شمارهٔ ۱۳۱۴
---
# غزل شمارهٔ ۱۳۱۴

<div class="b" id="bn1"><div class="m1"><p>خوش در آ در بحر ما ما را بجو</p></div>
<div class="m2"><p>چو چه می جوئی بیا دریا بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در وجود خویشتن سیری بکن</p></div>
<div class="m2"><p>حضرت یکتای بی همتا بجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه می بینی به نور او نگر</p></div>
<div class="m2"><p>نور او در دیدهٔ بینا بجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاب قوسین از میانه طرح کن</p></div>
<div class="m2"><p>منصب عالی او ادنی بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درخرابات مغان رندانه رو</p></div>
<div class="m2"><p>سید سرمست ما آنجا بجو</p></div></div>