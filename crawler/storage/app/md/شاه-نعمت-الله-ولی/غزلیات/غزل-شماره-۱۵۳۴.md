---
title: >-
    غزل شمارهٔ ۱۵۳۴
---
# غزل شمارهٔ ۱۵۳۴

<div class="b" id="bn1"><div class="m1"><p>اگر نه درد او بودی دوای دل که فرمودی</p></div>
<div class="m2"><p>و گر نه عشق او بودی طبیب ما که می بودی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیالش نقش می بندم به هر حالیکه پیش آید</p></div>
<div class="m2"><p>نیابم خالی از جودش وجود هیچ موجودی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیا و نوش کن جامی ز دُرد درد عشق او</p></div>
<div class="m2"><p>که غیر از دُرد درد او ندیدم هیچ بهبودی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خراباتست و ما سرمست و ساقی جام می بر دست</p></div>
<div class="m2"><p>مده تو پند مستان را ندارد پند تو سودی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر نه جام می بودی که از ساقی خبر دادی</p></div>
<div class="m2"><p>و گر نه آینه بودی به ما او را که بنمودی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنه بر آتش عشقم که تا بوی خوشی یابی</p></div>
<div class="m2"><p>بسوزانم کزین خوشتر نیایی در جهان عودی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طلسم گنج سلطانی معمائیست پر معنی</p></div>
<div class="m2"><p>اگر نه سیدم بودی معما را که بگشودی</p></div></div>