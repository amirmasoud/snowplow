---
title: >-
    غزل شمارهٔ ۹۵۶
---
# غزل شمارهٔ ۹۵۶

<div class="b" id="bn1"><div class="m1"><p>چیست عالم سایه بان حضرتش</p></div>
<div class="m2"><p>کیست آدم پاسبان حضرتش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر چه بود و هست و خواهد بود هم</p></div>
<div class="m2"><p>هست و بود و باشد از آن حضرتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابش نوربخش عالم است</p></div>
<div class="m2"><p>دادمت روشن نشان حضرتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مجلس عشق است و ما مست و خراب</p></div>
<div class="m2"><p>باده نوشان عاشقان حضرتش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل به من ده تا روان گویم ز جان</p></div>
<div class="m2"><p>این معانی از بیان حضرتش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتهٔ عشقم از آنم زنده دل</p></div>
<div class="m2"><p>حی جاویدم به جان حضرتش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید مست است و جام می به دست</p></div>
<div class="m2"><p>رند و سرخوش بندگان حضرتش</p></div></div>