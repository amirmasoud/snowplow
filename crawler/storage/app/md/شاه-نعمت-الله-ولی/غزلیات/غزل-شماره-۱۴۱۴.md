---
title: >-
    غزل شمارهٔ ۱۴۱۴
---
# غزل شمارهٔ ۱۴۱۴

<div class="b" id="bn1"><div class="m1"><p>خوش نقش خیالیست که بستیم به دیده</p></div>
<div class="m2"><p>خوشتر به ازین نقش که بستیم که دیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در نقش سراپردهٔ این دیده نظر کن</p></div>
<div class="m2"><p>کان نقش نگاریست که در دیده بدیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم که لبت بوسه دهم گفت ببوسش</p></div>
<div class="m2"><p>شیرین تر ازین قول که گفته که شنیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در کوی خرابات مغان مست و خرابیم</p></div>
<div class="m2"><p>از دردسر زاهد مخمور رمیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با ساقی سرمست حریفیم دگر بار</p></div>
<div class="m2"><p>یک جام شرابی به دو صد جم بخریده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیشب ز در خلوت ما شاه درآمد</p></div>
<div class="m2"><p>مهمان عزیزیست که از غیب رسیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خلق حسن و خوی حسینیست که او راست</p></div>
<div class="m2"><p>چون سید ما کیست به اوصاف حمیده</p></div></div>