---
title: >-
    غزل شمارهٔ ۲۳۴
---
# غزل شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>عشق او در جان هوائی دیگر است</p></div>
<div class="m2"><p>درد دل ما را دوائی دیگر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کشتهٔ عشقیم و زنده جاودان</p></div>
<div class="m2"><p>جان ما را خونبهائی دیگر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوت ما گوشهٔ میخانه است</p></div>
<div class="m2"><p>جای ما خلوتسرائی دیگر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ما ز ما فانی شده باقی به او</p></div>
<div class="m2"><p>این فنائی و بقائی دیگر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بینوایان را نوا دادیم از او</p></div>
<div class="m2"><p>بینوایان را نوائی دیگر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام پاکی پر ز می بستان بنوش</p></div>
<div class="m2"><p>جام ما گیتی نمائی دیگر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله تا گدای کوی او است</p></div>
<div class="m2"><p>نزد شاهان پادشاهی دیگر است</p></div></div>