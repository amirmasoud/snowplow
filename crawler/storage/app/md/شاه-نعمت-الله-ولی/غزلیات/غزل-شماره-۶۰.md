---
title: >-
    غزل شمارهٔ ۶۰
---
# غزل شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>یار من بی یار کی ماند مرا</p></div>
<div class="m2"><p>خسته و بیمار کی ماند مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر چه بیمارم ولی دارم امید</p></div>
<div class="m2"><p>کو چنین بیمار کی ماند مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادمانم گرچه غمها می خورم</p></div>
<div class="m2"><p>غمخورم غمخوار کی ماند مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من چنین مخمور و او مست و خراب</p></div>
<div class="m2"><p>بر در خمّار کی ماند مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کار بیکاریست کار عاشقان</p></div>
<div class="m2"><p>عشق او بیکار کی ماند مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر پر از سودا و هم کیسه تهی</p></div>
<div class="m2"><p>بر سر بازار کی ماند مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر نباشد صدق من صدیق وار</p></div>
<div class="m2"><p>سیدم در غار کی ماند مرا</p></div></div>