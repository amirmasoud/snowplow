---
title: >-
    غزل شمارهٔ ۱۴۸۱
---
# غزل شمارهٔ ۱۴۸۱

<div class="b" id="bn1"><div class="m1"><p>ای درد تو درمان من جان منی تو یا تنی</p></div>
<div class="m2"><p>من خود که باشم من تو ام می از من و تو خود منی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کل وجود جودک من جودک موجودنا</p></div>
<div class="m2"><p>با من مگو ترکی دگر تا کی منی و سن سنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خلوتسرای چشم ما خوش گوشهٔ آب روان</p></div>
<div class="m2"><p>بر چشم ما بنشین دمی ای چشم ما را روشنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم سر توئی هم سر توئی هم مصر پر شکر توئی</p></div>
<div class="m2"><p>هم یوسف دلبر توئی هم شخص و هم پیراهنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان مغز بادام است و تن همچون شجر ای جان من</p></div>
<div class="m2"><p>تو در میان جان و تن ای جان دل چون روغنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چه گدای حضرتم سلطان ملک همتم</p></div>
<div class="m2"><p>ور چه فقیر خدمتم هستم ز عشق تو غنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید به جستجوی تو گردد به هر در روز و شب</p></div>
<div class="m2"><p>او در برون جویای تو ، تو خود درون مخزنی</p></div></div>