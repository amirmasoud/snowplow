---
title: >-
    غزل شمارهٔ ۷۹۲
---
# غزل شمارهٔ ۷۹۲

<div class="b" id="bn1"><div class="m1"><p>گنج پنهانی که پیدا کرده اند</p></div>
<div class="m2"><p>از برای بخشش ما کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم ما را نور خود بخشیده اند</p></div>
<div class="m2"><p>بر جمال خویش بینا کرده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جزو و کل را جام وحدت داده اند</p></div>
<div class="m2"><p>بر همه خود را هویدا کرده اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل ز دست عالمی بربوده اند</p></div>
<div class="m2"><p>عاشقانه ملک یغما کرده اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>لطف معنی را بصورت داده اند</p></div>
<div class="m2"><p>این دوئی را باز یکتا کرده اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا عیان گردد چو سید عارفی</p></div>
<div class="m2"><p>آنچه پنهان بود پیدا کرده اند</p></div></div>