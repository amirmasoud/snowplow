---
title: >-
    غزل شمارهٔ ۵۱۵
---
# غزل شمارهٔ ۵۱۵

<div class="b" id="bn1"><div class="m1"><p>عقل مخمور است و مستان را به قاضی می برد</p></div>
<div class="m2"><p>سخت بی شرمست از آن رو پردهٔ ما می درد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رند و سرمست مناجاتیم و با ساقی حریف</p></div>
<div class="m2"><p>فارغ است از ریش قاضی هر که او می می خورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که گوئی دل به دلبر می فروشد جان من</p></div>
<div class="m2"><p>نقد تو گر قلب باشد سیم قلبی کی خرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می بیارد رند مست و سرکه آرد زاهدی</p></div>
<div class="m2"><p>هر چه تو آری بری و هر چه او آرد برد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر هزار آئینه باشد در همه بینم یکی</p></div>
<div class="m2"><p>عارف است آن کس که این یک در هزاران بنگرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سرابستان او غیری نمی یابد مجال</p></div>
<div class="m2"><p>گر کسی مرغی شود بر گرد قصرش کی پرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درهوای نعمت الله غنچهٔ سیراب گل</p></div>
<div class="m2"><p>درگلستان همچو مستان جامه بر خود می درد</p></div></div>