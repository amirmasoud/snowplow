---
title: >-
    غزل شمارهٔ ۱۰۳۳
---
# غزل شمارهٔ ۱۰۳۳

<div class="b" id="bn1"><div class="m1"><p>عاقلی بودم به عشق یار دیوانه شدم</p></div>
<div class="m2"><p>آشنائی یافتم ازخویش بیگانه شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رشتهٔ شمع وجودم آتش عشقش بسوخت</p></div>
<div class="m2"><p>عارفانه با خبر از ذوق پروانه شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آمدم رندانه در کوی خرابات مغان</p></div>
<div class="m2"><p>جام می را نوش کردم باز مستانه شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدتی با زاهدان در زاویه بودم مقیم</p></div>
<div class="m2"><p>چون ندیدم حاصلی دیگر به میخانه شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>راز جانانه اگر جوئی بجو از جان من</p></div>
<div class="m2"><p>زان که جان کردم فدا همراز جانانه شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خم می را سر گشودم جام می دارم به دست</p></div>
<div class="m2"><p>توبه را بشکستم و در بند پیمانه شدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چشم مست نعمت الله در نظر دارم مدام</p></div>
<div class="m2"><p>عیب من کم کن اگر سرمست و دیوانه شدم</p></div></div>