---
title: >-
    غزل شمارهٔ ۲۸۹
---
# غزل شمارهٔ ۲۸۹

<div class="b" id="bn1"><div class="m1"><p>گر جفا می کند وفا آنست</p></div>
<div class="m2"><p>ور فنا می دهد بقا آنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور چشم است و در نظر داریم</p></div>
<div class="m2"><p>نظری کن ببین بیا آنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُرد دردش بنوش و خوش می باش</p></div>
<div class="m2"><p>دردمندی تو را دوا آنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قدمی تو در آ درین دریا</p></div>
<div class="m2"><p>طلبش کن که آشنا آنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که غیری ز شاه ما جوید</p></div>
<div class="m2"><p>نزد یاران ما گدا آنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به خرابات هر که فانی شد</p></div>
<div class="m2"><p>رند سرمست بینوا آنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر که گردد غلام سید ما</p></div>
<div class="m2"><p>سید ملک دو سرا آنست</p></div></div>