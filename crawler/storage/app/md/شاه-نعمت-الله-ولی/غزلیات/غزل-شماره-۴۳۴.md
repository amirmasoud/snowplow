---
title: >-
    غزل شمارهٔ ۴۳۴
---
# غزل شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>تا که سودای خیالش در سُویدا جا گرفت</p></div>
<div class="m2"><p>چون سر زلفش وجودم مو به مو سودا گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از بلای عشق آن بالا نمی نالیم ما</p></div>
<div class="m2"><p>مبتلائیم از بلا این کار ما بالا گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موج دریا می رسد ما را به دریا می کشد</p></div>
<div class="m2"><p>اختیاری نیست ما را کی بود بر ما گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق مستیم اگر گفتیم اناالحق دور نیست</p></div>
<div class="m2"><p>مرد عاقل کی گنه بر عاشق شیدا گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات مغان خوش گوشه ای بگرفته ایم</p></div>
<div class="m2"><p>گر بقا خواهی همین جا بایدت مأوا گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آب چشم ما به هر سو رو نهاده می رود</p></div>
<div class="m2"><p>لاجرم آب وجود ما همه دریا گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر کسی دستی زده بر دامن صاحبدلی</p></div>
<div class="m2"><p>نعمت الله دامن یکتای بی همتا گرفت</p></div></div>