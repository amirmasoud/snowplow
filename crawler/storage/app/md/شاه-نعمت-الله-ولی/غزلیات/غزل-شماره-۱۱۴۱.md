---
title: >-
    غزل شمارهٔ ۱۱۴۱
---
# غزل شمارهٔ ۱۱۴۱

<div class="b" id="bn1"><div class="m1"><p>مستیم و خراب و می پرستیم</p></div>
<div class="m2"><p>پنهان چه کنیم مست مستیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی مستی و رند و عاشق</p></div>
<div class="m2"><p>آری مستیم و رند هستیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برخواسته از سریر هستی</p></div>
<div class="m2"><p>بر مسند نیستی نشستیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستیم و مدام همدم جام</p></div>
<div class="m2"><p>صد شکر که توبه را شکستیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا جان باشد شراب نوشیم</p></div>
<div class="m2"><p>کردیم این شرط و عهد بستیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بند خیال دی و فردا</p></div>
<div class="m2"><p>بودیم امروز باز رستیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شادی روان نعمت الله</p></div>
<div class="m2"><p>می می نوشیم و می پرستیم</p></div></div>