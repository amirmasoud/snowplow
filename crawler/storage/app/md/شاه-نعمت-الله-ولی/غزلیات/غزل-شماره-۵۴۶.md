---
title: >-
    غزل شمارهٔ ۵۴۶
---
# غزل شمارهٔ ۵۴۶

<div class="b" id="bn1"><div class="m1"><p>نیمشب ماه ما هویدا شد</p></div>
<div class="m2"><p>گوئیا آفتاب پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان ما گرد بحر می گردید</p></div>
<div class="m2"><p>خود در افتاد و غرق دریا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور رویش به چشم ما بنمود</p></div>
<div class="m2"><p>دیدهٔ ما تمام بینا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد و تخت دل روان بگرفت</p></div>
<div class="m2"><p>پادشاه ممالک ما شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عین اول خوشی تجلی کرد</p></div>
<div class="m2"><p>در مرایا ظهور اسما شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جام می را به همه گر آمیخت</p></div>
<div class="m2"><p>بزم مستانه ای مهیا شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ساز ما را به لطف خود بنواخت</p></div>
<div class="m2"><p>نعمت الله به ذوق گویا شد</p></div></div>