---
title: >-
    غزل شمارهٔ ۱۲۲۹
---
# غزل شمارهٔ ۱۲۲۹

<div class="b" id="bn1"><div class="m1"><p>ای نفس شوخ چشم مرو در قفای نان</p></div>
<div class="m2"><p>جانت مده به باد هوا در هوای نان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بگشاده‌ای چو کاسه دهان در خیال آش</p></div>
<div class="m2"><p>مانند سفره حلقه به گوشی برای نان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بهر دو نان مرو بر ِ دونان و شرم‌دار</p></div>
<div class="m2"><p>حیف است کآبروی فروشی بهای نان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آدم برای دانهٔ گندم بهشت هشت</p></div>
<div class="m2"><p>تو بازخر به نان جو ای مبتلای نان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر هشت خلد و شش جهت و پنج حس تو را</p></div>
<div class="m2"><p>گردد مطیع اگر بدهی یک دو تای نان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل را شراب ده که همین است دوای دل</p></div>
<div class="m2"><p>نان پیش سگ بمان که همان است سزای نان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از خوان نعمت‌اللّه اگر خورده‌ای طعام</p></div>
<div class="m2"><p>چه قدر آش نزد تو باشد چه جای نان</p></div></div>