---
title: >-
    غزل شمارهٔ ۱۳۳۰
---
# غزل شمارهٔ ۱۳۳۰

<div class="b" id="bn1"><div class="m1"><p>ذوق سرمستان ز مخموران مجو</p></div>
<div class="m2"><p>حال مستی جز که از مستان مجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خرابات مغان رندانه رو</p></div>
<div class="m2"><p>مجلسی جز مجلس رندان مجو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش درآ در بحر بی پایان ما</p></div>
<div class="m2"><p>غیر ما در بحر بی پایان مجو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جان و دل ایثار جانان کن چو ما</p></div>
<div class="m2"><p>جز وصال حضرت جانان مجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گنج او در کنج دل می جو مدام</p></div>
<div class="m2"><p>غیر گنجش در دل ویران مجو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از خدا دائم خدا را می طلب</p></div>
<div class="m2"><p>گر محبی جنت و حوران مجو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر دار فنا با ما نشین</p></div>
<div class="m2"><p>مثل سید میر سرمستان مجو</p></div></div>