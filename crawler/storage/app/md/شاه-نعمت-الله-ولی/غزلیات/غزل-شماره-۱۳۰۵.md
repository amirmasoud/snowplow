---
title: >-
    غزل شمارهٔ ۱۳۰۵
---
# غزل شمارهٔ ۱۳۰۵

<div class="b" id="bn1"><div class="m1"><p>شاهان جهان باشند از جان چو گدای تو</p></div>
<div class="m2"><p>محبوب تر از جانی صد جان به فدای تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رندان ز تو می جویند زهاد ز تو حلوا</p></div>
<div class="m2"><p>هر کس به هوای خود مائیم و هوای تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل خلوت خاص تست ، بنشین تو به جای خود</p></div>
<div class="m2"><p>والله که نخواهم داشت غیر تو به جای تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر دست مرا گیری من دامن تو گیرم</p></div>
<div class="m2"><p>پائی ز تو گر یابم آیم به سرای تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند که این و آن باشند برای ما</p></div>
<div class="m2"><p>نی نی که غلط کردند هستند برای تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جز نقش خیال تو در چشم نمی آید</p></div>
<div class="m2"><p>هر نور که می یابم بینم به لقای تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دار فنا سید از عشق تو گر جان داد</p></div>
<div class="m2"><p>جانش ز خدا جوید پیوسته بقای تو</p></div></div>