---
title: >-
    غزل شمارهٔ ۸۴۵
---
# غزل شمارهٔ ۸۴۵

<div class="b" id="bn1"><div class="m1"><p>نعمت الله است عالم سر به سر</p></div>
<div class="m2"><p>نعمت الله در همه عالم نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آفتابی رو نموده مه لقا</p></div>
<div class="m2"><p>گشته پیدا فتنهٔ دور قمر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون یکی اندر یکی باشد یکی</p></div>
<div class="m2"><p>آن یکی در هر یکی خوش می شمر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ذوق سرمستان اگر داری بیا</p></div>
<div class="m2"><p>از سر دنیی و عقبی در گذر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جان کدام است تا بیان جان کنم</p></div>
<div class="m2"><p>سر چه باشد در سخن گویم ز سر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرچه او از جود او دارد وجود</p></div>
<div class="m2"><p>معتبر باشد نباشد مختصر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر خبر پرسی ز سرمستان ما</p></div>
<div class="m2"><p>نعمت الله جو که او دارد خبر</p></div></div>