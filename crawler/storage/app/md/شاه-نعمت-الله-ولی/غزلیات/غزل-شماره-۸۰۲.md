---
title: >-
    غزل شمارهٔ ۸۰۲
---
# غزل شمارهٔ ۸۰۲

<div class="b" id="bn1"><div class="m1"><p>سلطان که بود گدای سید</p></div>
<div class="m2"><p>عالم چو بود فدای سید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما جام جهان نمای اوئیم</p></div>
<div class="m2"><p>او جام جهان نمای سید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داریم هوا و خوش هوائی</p></div>
<div class="m2"><p>آنگه چو هوا هوای سید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جائی که بقای اوست جاوید</p></div>
<div class="m2"><p>باقی بود از بقای سید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نغمهٔ قول کن بر آمد</p></div>
<div class="m2"><p>بگرفت جهان صدای سید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سید چو برای ماست دائم</p></div>
<div class="m2"><p>مائیم از آن برای سید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون نیست به غیر سید ما</p></div>
<div class="m2"><p>غیری نبود به جای سید</p></div></div>