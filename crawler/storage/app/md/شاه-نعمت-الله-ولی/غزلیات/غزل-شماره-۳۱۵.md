---
title: >-
    غزل شمارهٔ ۳۱۵
---
# غزل شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>عشق جانان حیات جان من است</p></div>
<div class="m2"><p>حاصل عمر جاودان من است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معنی چار حرف و هفت هیکل</p></div>
<div class="m2"><p>جمع و تفصیل آن بیان من است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقد گنجینهٔ حدوث و قدم</p></div>
<div class="m2"><p>گوهر بحر بیکران من است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عین آب حیات دانی چیست</p></div>
<div class="m2"><p>آب سرچشمهٔ روان من است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خرابات پیر میخانه</p></div>
<div class="m2"><p>طالب رند نوجوان من است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نام بگذار و از نشان بگذر</p></div>
<div class="m2"><p>بی نشان شو که آن نشان من است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت اوست هر چه موجود است</p></div>
<div class="m2"><p>نعمة الله من از آن من است</p></div></div>