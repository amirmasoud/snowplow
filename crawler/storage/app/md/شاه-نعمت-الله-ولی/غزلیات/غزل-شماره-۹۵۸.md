---
title: >-
    غزل شمارهٔ ۹۵۸
---
# غزل شمارهٔ ۹۵۸

<div class="b" id="bn1"><div class="m1"><p>در خواب خوش نماید نقش خیال رویش</p></div>
<div class="m2"><p>نور نظر فزاید نقش خیال رویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از نور طلعت او دیده شود منور</p></div>
<div class="m2"><p>در چشم من چو آید نقش خیال رویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقش خیال رویش بر دیده می نگارم</p></div>
<div class="m2"><p>جائی دگر نشاید نقش خیال رویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دایم ز نو خیالش بر دیده می کشم نقش</p></div>
<div class="m2"><p>پیوسته خود نپاید نقش خیال رویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر لحظه ای خیالی بر دیده نقش بندم</p></div>
<div class="m2"><p>هر دم دلی رباید نقش خیال رویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هرگز خیال غیری در چشم ما نیاید</p></div>
<div class="m2"><p>چون پرده برگشاید نقش خیال رویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در عین نعمت الله بنگر به چشم معنی</p></div>
<div class="m2"><p>چون نور می نماید نقش خیال رویش</p></div></div>