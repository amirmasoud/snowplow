---
title: >-
    غزل شمارهٔ ۱۲۴۰
---
# غزل شمارهٔ ۱۲۴۰

<div class="b" id="bn1"><div class="m1"><p>ای به روی تو دیده ها روشن</p></div>
<div class="m2"><p>ای به نور تو جان ما روشن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به کمالت زبانها گویا</p></div>
<div class="m2"><p>به جمال تو چشمها روشن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نور چشم منی از آن شب و روز</p></div>
<div class="m2"><p>من به تو دیده ام تو را روشن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردم دیده تا به خود بیناست</p></div>
<div class="m2"><p>در همه دیده ام خدا روشن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر تو آفتاب جان و دل است</p></div>
<div class="m2"><p>من چو ذره در آن هوا روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشق تو شمع خلوت جان است</p></div>
<div class="m2"><p>دل پروانه زان ضیا روشن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صورت روی خوب سید ماست</p></div>
<div class="m2"><p>نور معنی والضحی روشن</p></div></div>