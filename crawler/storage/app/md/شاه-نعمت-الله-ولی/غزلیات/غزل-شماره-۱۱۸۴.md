---
title: >-
    غزل شمارهٔ ۱۱۸۴
---
# غزل شمارهٔ ۱۱۸۴

<div class="b" id="bn1"><div class="m1"><p>دردمندیم و به امید دوا آمده‌ایم</p></div>
<div class="m2"><p>مستمندیم و طلبکار شفا آمده‌ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از در لطف تو نومید نگردیم که ما</p></div>
<div class="m2"><p>بی‌نوایان به تمنای نوا آمده‌ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما گداییم و تو سلطان جهان کرمی</p></div>
<div class="m2"><p>نظری کن که به امید شما آمده‌ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل فدا کرده و جان داده و سر بر کف دست</p></div>
<div class="m2"><p>تا نگویی که به تزویر و ریا آمده‌ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این چنین عاشق و سرمست که بینی ما را</p></div>
<div class="m2"><p>نیست حاجت که بگویی ز کجا آمده‌ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما اگر زاهد سجاده‌نشینیم نه رند</p></div>
<div class="m2"><p>بر سر کوی خرابات چرا آمده‌ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید بزم خرابات جهان جانیم</p></div>
<div class="m2"><p>بندگانیم به درگاه خدا آمده‌ایم</p></div></div>