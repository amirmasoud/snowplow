---
title: >-
    غزل شمارهٔ ۲۰۴
---
# غزل شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>موجیم و حباب هر دو آبست</p></div>
<div class="m2"><p>آبست که صورت حبابست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آن کس که خیال غیر بندد</p></div>
<div class="m2"><p>نقش غلطست و خود به خوابست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>موجست و حباب هر دو یک آب</p></div>
<div class="m2"><p>آبست که آب را حجابست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مهتاب چو رو به تو نماید</p></div>
<div class="m2"><p>روشن بنگر که آفتابست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بسته نقاب می برد دل</p></div>
<div class="m2"><p>این طرفه که عین آن نقابست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلسوخت در آتش محبت</p></div>
<div class="m2"><p>گر میل کنی جگر کبابست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اسرار ضمیر نعمت الله</p></div>
<div class="m2"><p>احسان که کند که بی حسابست</p></div></div>