---
title: >-
    غزل شمارهٔ ۱۸۷
---
# غزل شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>موج بحریم و عین ما دریاست</p></div>
<div class="m2"><p>بحر می داند آنکه او از ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام و می ساقیم به هم آمیخت</p></div>
<div class="m2"><p>مجلس عاشقانه ای آراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صورت و معنئی به هم پیوست</p></div>
<div class="m2"><p>عالمی از میانه خوش برخاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن ما زر است و مروارید</p></div>
<div class="m2"><p>هر که در گوش می کند زیباست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم ما نور او به او بیند</p></div>
<div class="m2"><p>دیدهٔ ما به نور او بیناست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در جهان آن اوست این عجبست</p></div>
<div class="m2"><p>که خداوند از این و آن یکتاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام گیتی نما به دست آور</p></div>
<div class="m2"><p>که درو نعمت اللهم پیداست</p></div></div>