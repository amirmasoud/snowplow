---
title: >-
    غزل شمارهٔ ۴۴۹
---
# غزل شمارهٔ ۴۴۹

<div class="b" id="bn1"><div class="m1"><p>هر که بد بازی کند بد باز گردد عاقبت</p></div>
<div class="m2"><p>ور کسی نیکو نشد بد باز گردد عاقبت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرچه بی ساز است ساز مطرب عشاق ما</p></div>
<div class="m2"><p>گر نوازد ساز ما با ساز گردد عاقبت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدم جامیم و با ساقی حریفی می کنیم</p></div>
<div class="m2"><p>خوش بود گر همدمی دمساز گردد عاقبت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشقی گر پیش معشوقی نیازی می کند</p></div>
<div class="m2"><p>آن نیاز عاشقان با ناز گردد عاقبت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر که او در سایهٔ فَر هُما مأوا گرفت</p></div>
<div class="m2"><p>گرچه گنجشکی بود شهباز گردد عاقبت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عقل مخمور است دردسر به رندان می دهد</p></div>
<div class="m2"><p>بی غمی داند که او غمساز گردد عاقبت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سید از بنده تمیزی گر کند صاحبدلی</p></div>
<div class="m2"><p>در میان عاشقان ممتاز گردد عاقبت</p></div></div>