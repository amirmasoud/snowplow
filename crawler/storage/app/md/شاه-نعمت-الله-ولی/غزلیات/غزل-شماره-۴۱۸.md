---
title: >-
    غزل شمارهٔ ۴۱۸
---
# غزل شمارهٔ ۴۱۸

<div class="b" id="bn1"><div class="m1"><p>بلبل چو هوای گلستان یافت</p></div>
<div class="m2"><p>هر کام که بود در زمان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صومعه دل نیافت ذوقی</p></div>
<div class="m2"><p>ذوقی ز حضور عاشقان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بی جام شراب عشق ساقی</p></div>
<div class="m2"><p>نتوان کامی در این جهان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر زنده دلی که کشتهٔ اوست</p></div>
<div class="m2"><p>چون خضر حیات جاودان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دردی دردنوش کردیم</p></div>
<div class="m2"><p>دل از همه دردها امان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمری است که می خورم می عشق</p></div>
<div class="m2"><p>هر چیز که یافت دل از آن یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در کنج دل شکستهٔ من</p></div>
<div class="m2"><p>گنجی است که جان من عیان یافت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زهد از بر ما کناره ای کرد</p></div>
<div class="m2"><p>تا ساغر و باده در میان یافت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مستیم و حریف نعمت الله</p></div>
<div class="m2"><p>بزمی به از این کجا توان یافت</p></div></div>