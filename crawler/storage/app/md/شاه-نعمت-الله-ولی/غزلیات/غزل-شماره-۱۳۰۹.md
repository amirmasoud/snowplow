---
title: >-
    غزل شمارهٔ ۱۳۰۹
---
# غزل شمارهٔ ۱۳۰۹

<div class="b" id="bn1"><div class="m1"><p>ز سودای سر زلفت پریشانم به جان تو</p></div>
<div class="m2"><p>محبان تو بسیارند از ایشانم به جان تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر لطفت کند رحمت مرا از خاک بردارد</p></div>
<div class="m2"><p>نثار و پیشکش جان را بر افشانم به جان تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر حالی که می باشم نباشم بی خیال تو</p></div>
<div class="m2"><p>وگر بی تو دمی بودم پشیمانم به جان تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم خلوتسرای تست غیری در نمی گنجد</p></div>
<div class="m2"><p>کجا گنجد چو غیر تو نمی دانم به جان تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کفر زلف تو ایمان من آوردم به جان و دل</p></div>
<div class="m2"><p>سر موئی نمی گردم مسلمانم به جان تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر بلبل ثنای گل دو روزی در چمن گوید</p></div>
<div class="m2"><p>منم مداح تو کز جان ثنا خوانم به جان تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر رند خوشی جوئی به میخانه گذاری کن</p></div>
<div class="m2"><p>حریف نعمت الله شو که من آنم به جان تو</p></div></div>