---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>در دو عالم جز یکی موجود نیست</p></div>
<div class="m2"><p>ور تو گوئی هست آن مقصود نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با خیال دیگری گر سرخوشی</p></div>
<div class="m2"><p>خوش خوشی جام شرابی می کشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر خیالی را که می بینی به خواب</p></div>
<div class="m2"><p>نقش او باشد چو بُرداری نقاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اصل جوهر دان و گوهر فرع او</p></div>
<div class="m2"><p>اصل و فرع ما بُود در وی نکو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت و معنی عالم گفتمش</p></div>
<div class="m2"><p>دُر توحید است نیکو سُفتمش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در صدف آبی است بر بسته نقاب</p></div>
<div class="m2"><p>می نماید در نظر دُر خوشاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هستی ما سایهٔ هستی اوست</p></div>
<div class="m2"><p>مستی ما عین سرمستی اوست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قطره و دریا به نزد ما یکی است</p></div>
<div class="m2"><p>بشنو از ما قطره و دریا یکیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>این دوئی پیدا شده ازما و تو</p></div>
<div class="m2"><p>شرک باشد گر یکی خوانی به دو</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از کتاب ذات و آیات صفات</p></div>
<div class="m2"><p>نسخهٔ خوش خوانده ای در کائنات</p></div></div>