---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>جامع مجموع اسما آدم است</p></div>
<div class="m2"><p>لاجرم او روح جمله عالم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل اول درهٔ بیضا بود</p></div>
<div class="m2"><p>صورت و معنی ز جد ما بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آدمی معنی است عقل کل به نام</p></div>
<div class="m2"><p>جملهٔ عالم از او یابد نظام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حضرت مبدا چو او را آفرید</p></div>
<div class="m2"><p>مبدا مجموع عالم شد پدید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم اجمالیست او را از قضا</p></div>
<div class="m2"><p>لاجرم لوح قضا خوانیم ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نفس کلیه از او حاصل شده</p></div>
<div class="m2"><p>این و آن با یکدگر واصل شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مرد و زن یعنی نفوس و هم عقول</p></div>
<div class="m2"><p>فرع ایشانند این هر دو اصول</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نفس کل یاقوتهٔ حمرا بود</p></div>
<div class="m2"><p>این کسی داند که او از ما بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بعد از این هر دو طبیعت گفته اند</p></div>
<div class="m2"><p>در این معنی بحکمت سفته اند</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علم تفصیلی ز لوح دل بخوان</p></div>
<div class="m2"><p>جامع علم قدر باشد چنان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن گهی باشد هیولا یاد دار</p></div>
<div class="m2"><p>صورتی خوش بر هیولائی نگار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر دو با هم جسم کلّی خوانده اند</p></div>
<div class="m2"><p>خوش حکیمانه سخنها رانده اند</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>عرش اعظم تخت الرّحمن بگو</p></div>
<div class="m2"><p>الرحیم از کرسی اعلا بجو</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سقف جنت عرش کرسی زمین</p></div>
<div class="m2"><p>خوش جنانی باشد ار یابی چنین</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بندگی سید هر دو سرا</p></div>
<div class="m2"><p>این چنین فرمود ما را از خدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>هفت افلاکند نیکو یاد دار</p></div>
<div class="m2"><p>کوکب هر یک به هر یک می شمار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>چون زحل چون مشتری مریخ هم</p></div>
<div class="m2"><p>آفتاب و زهره همچون جام جم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>با عطارد ماه خوش سیما بود</p></div>
<div class="m2"><p>نیست پنهان این سخن پیدا بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چار ارکان مخالف بعد ازاین</p></div>
<div class="m2"><p>معدنست و پس نبات ای نازنین</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>باز حیوان آنگهی جن ای پسر</p></div>
<div class="m2"><p>نیک ترتیبی است نیکو می نگر</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در زمین و آسمان باشد ملک</p></div>
<div class="m2"><p>روز و شب خیرات می باشد ملک</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آخر ایشان همه انسان بود</p></div>
<div class="m2"><p>گر چه انسان اول ایشان بود</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>معنیش اول ، به صورت آخر است</p></div>
<div class="m2"><p>روح باطن ، جسم پاکش ظاهر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>جامع مجموع اسما او بود</p></div>
<div class="m2"><p>جمله می دان کاین جعل نیکو بود</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>روشنست و دیده ام در آینه</p></div>
<div class="m2"><p>می نماید نور او هر آینه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>از وجودش یافته عالم نظام</p></div>
<div class="m2"><p>بلکه جان عالم است او والسلام</p></div></div>