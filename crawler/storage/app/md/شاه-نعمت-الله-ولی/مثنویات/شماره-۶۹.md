---
title: >-
    شمارهٔ ۶۹
---
# شمارهٔ ۶۹

<div class="b" id="bn1"><div class="m1"><p>ده چیز نبی حق به امت</p></div>
<div class="m2"><p>فرمود علامت قیامت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول دود از جهان برآید</p></div>
<div class="m2"><p>دنیا پس از آن بسی نپاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنگه دجال کور ناخوش</p></div>
<div class="m2"><p>پیدا گردد چو آب و آتش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دابه پس از آن پدید آید</p></div>
<div class="m2"><p>اما بسیار هم نپاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خورشید عیان شود ز مغرب</p></div>
<div class="m2"><p>آنگه روان رود ز مغرب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مغرب مشرق نماید آن روز</p></div>
<div class="m2"><p>از پرتو شمع عالم افروز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پنجم عیسی فرود آید</p></div>
<div class="m2"><p>بر ما در رحمتی گشاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنگه باشد ظهور یأجوج</p></div>
<div class="m2"><p>با لشکر بی ‌شمار مأجوج </p></div></div>
<div class="b" id="bn9"><div class="m1"><p>یکسال سه بار مه بگیرد</p></div>
<div class="m2"><p>بسیار شه و گدا بمیرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آخر ز یمن بر آید آتش</p></div>
<div class="m2"><p>سوزد تر و خشک مردمان خوش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>این است علامت قیامت</p></div>
<div class="m2"><p>فرمود رسول حق به امت</p></div></div>