---
title: >-
    شمارهٔ ۶۸
---
# شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>چار نور آمد مرا در پیش راه</p></div>
<div class="m2"><p>نور سرخ و زرد و اسفید و سیاه </p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون من از هر چار برتر بر شدم</p></div>
<div class="m2"><p>در دو عالم عین یک دلبر شدم</p></div></div>