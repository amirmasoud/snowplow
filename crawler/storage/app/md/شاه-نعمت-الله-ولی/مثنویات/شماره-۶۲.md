---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>چشم اهل مراقبت باید</p></div>
<div class="m2"><p>که نظر را به غیر نگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه صدهزار اگر شمرد</p></div>
<div class="m2"><p>در همه آینه یکی نگرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواه تنها و خواه با تنها</p></div>
<div class="m2"><p>چون بود با خدا بود همه جا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشهٔ چشم سوی او دارد</p></div>
<div class="m2"><p>نقش او در خیال بنگارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در گلستان اگر گلی چیند</p></div>
<div class="m2"><p>شیشهٔ پر گلاب را بیند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر خرد را فروشد آن عاقل</p></div>
<div class="m2"><p>نشود از خدای خود غافل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سایه و آفتاب بر من و تو</p></div>
<div class="m2"><p>خط موهوم می نماید دو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خط موهوم اگر براندازی</p></div>
<div class="m2"><p>خانه از غیر او به پردازی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه جا آفتاب تابان است</p></div>
<div class="m2"><p>نظری کن ببین که این آن است</p></div></div>