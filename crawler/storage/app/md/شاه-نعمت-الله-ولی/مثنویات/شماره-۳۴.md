---
title: >-
    شمارهٔ ۳۴
---
# شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>گرم باش و آتشی خوش برفروز</p></div>
<div class="m2"><p>خرقه و سجاده و هستی بسوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صورت و معنی به این و آن گذار</p></div>
<div class="m2"><p>دنیی و عقبی به جسم و جان گذار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جام می بگذار و ساقی را طلب</p></div>
<div class="m2"><p>تا چو رندان مستی ای یابی عجب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بعد از آن مستی چو ما هشیار شو</p></div>
<div class="m2"><p>عارفانه بر سر بازار شو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ببینی آن یکی اندر یکی</p></div>
<div class="m2"><p>خود یکی باشی و باشی نیککی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر کجا کنجیست گنجی در وی است</p></div>
<div class="m2"><p>کنج دل بی گنج عشق وی کی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر صدف در بحر ما دُر خوشاب</p></div>
<div class="m2"><p>باشد آن حاصل ولی از عین آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گوهر ار جوئی درین دریا بجو</p></div>
<div class="m2"><p>جوهر دُر یتیم از ما بجو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عین او در عین اعیان رو نمود</p></div>
<div class="m2"><p>چون نظر فرمود غیر او نمود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک حقیقت صد هزارش اعتبار</p></div>
<div class="m2"><p>آن یکی باشد یکی نی صد هزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>قطره و موج و حباب و جو نگر</p></div>
<div class="m2"><p>عین این دریای ما نیکو نگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>درصد آئینه یکی چون رو نمود</p></div>
<div class="m2"><p>صد نمود اما به جز یک رو نبود</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>جامی از می پر ز می داریم ما</p></div>
<div class="m2"><p>جرعه ای با غیر نگذاریم ما</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>در خرابات مغان رندان تمام</p></div>
<div class="m2"><p>می خورند شادی سید والسلام</p></div></div>