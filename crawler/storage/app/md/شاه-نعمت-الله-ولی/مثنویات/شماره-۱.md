---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>خوش بگو ای یار بسم الله بگو</p></div>
<div class="m2"><p>هرچه می جوئی ز بسم الله بجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اسم جامع جامع اسما بود</p></div>
<div class="m2"><p>صورت این اسم عین ما بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مقام جمع روشن شد چو شمع</p></div>
<div class="m2"><p>آنچه مخفی بود اندر جمع جمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جلمهٔ اسما به اعیان رو نمود</p></div>
<div class="m2"><p>صد هزار اسما مسمی یک وجود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر کجا اسمی است عینی آن اوست</p></div>
<div class="m2"><p>هر کرا عینیست اسمی جان اوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مجمع مجموع انسان آدمست</p></div>
<div class="m2"><p>لاجرم او قطب جمله عالمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکسی کو مظهر الله شد</p></div>
<div class="m2"><p>ز آفتاب حضرتش چون ماه شد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جسم و روح و عین و اسم و این چهار</p></div>
<div class="m2"><p>ظل یک ذاتند نیکو یاد دار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت الله مظهر او دانمش</p></div>
<div class="m2"><p>صورت اسم الهی خوانمش</p></div></div>