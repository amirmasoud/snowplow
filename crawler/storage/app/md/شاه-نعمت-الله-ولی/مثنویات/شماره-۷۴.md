---
title: >-
    شمارهٔ ۷۴
---
# شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>همه محکوم حکم او باشند</p></div>
<div class="m2"><p>سر و زر را به پای او پاشند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عقل و عاقل رعیت اویند</p></div>
<div class="m2"><p>از دل و جان دعای او گویند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهر او بر دلی که شارق شد</p></div>
<div class="m2"><p>هر که در خانه بود عاشق شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غیرتش غیر چون براندازد</p></div>
<div class="m2"><p>خانه از غیر خود بپردازد</p></div></div>