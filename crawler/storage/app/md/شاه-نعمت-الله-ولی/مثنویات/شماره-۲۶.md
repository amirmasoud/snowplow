---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>نقطه ای در دایره بنمود میم</p></div>
<div class="m2"><p>میم این معنی طلب فرما ز جیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لازم جیمست میم ای یار من</p></div>
<div class="m2"><p>کی بود بی میم جیم ای یار من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارفان دانند راز عارفان</p></div>
<div class="m2"><p>عارفانه گفتهٔ عارف بخوان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنبش سایه بود از آفتاب</p></div>
<div class="m2"><p>با تو گفتم سر عالم بی حجاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از وجودش سایه می یابد وجود</p></div>
<div class="m2"><p>ور نه بی او سایه را بودی نبود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>وحدت از ذاتست و کثرت از صفات</p></div>
<div class="m2"><p>وحدت و کثرت بجو از کاینات</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر دو می خوانی بخوانش صادقی</p></div>
<div class="m2"><p>ور یکی گوئی بگو گر عاشقی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حق تعالی بر همه شیئی شهید</p></div>
<div class="m2"><p>جان من شهد شهادت زو چشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آیت غیب و شهادت را بخوان</p></div>
<div class="m2"><p>وحدت و کثرت از آن هر دو بدان</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غیب باطن دان شهادت ظاهرش</p></div>
<div class="m2"><p>آن یکی اول بگیر این آخرش</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>حالت و ماضی و مستقبل بدان</p></div>
<div class="m2"><p>حد فاصل حال باشد در میان</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر نبودی حال بودی بی شکی</p></div>
<div class="m2"><p>ماضی و مستقبل ای عاقل یکی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>از خط موهوم آن یک دو نمود</p></div>
<div class="m2"><p>دو نمود اما حقیقت دو نبود</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خط موهوم ار نبودی در میان</p></div>
<div class="m2"><p>کی نمودی یک حقیقت در جهان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>خوانم از لوح ابد راز ازل</p></div>
<div class="m2"><p>می نوازم تا ابد ساز ازل</p></div></div>