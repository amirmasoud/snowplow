---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>علم ما در علم او عین وی است</p></div>
<div class="m2"><p>علم عالم بی وجودش لاشی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می دهد ما را وجود از جود خویش</p></div>
<div class="m2"><p>می دهیم او را ظهور از بود خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبروی جام می از وی بود</p></div>
<div class="m2"><p>گر چه وی را هم ظهور از می بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام در دور است و ساقی در نظر</p></div>
<div class="m2"><p>جام می بستان و ساقی می نگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یک زمان بر دیدهٔ بینا نشین</p></div>
<div class="m2"><p>شاهد معنی به هر صورت ببین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عالمی از نور او روشن شده</p></div>
<div class="m2"><p>یوسفی پنهان به پیراهن شده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در محیط علم اعیان چون حباب</p></div>
<div class="m2"><p>نقش بسته صورت اسما بر آب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عین ما بر ما اگر پیدا بود</p></div>
<div class="m2"><p>هرچه ما بینیم عین ما بود</p></div></div>