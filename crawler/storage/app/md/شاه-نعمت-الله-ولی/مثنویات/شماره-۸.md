---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ساقی مستیم و جام می به دست</p></div>
<div class="m2"><p>می خورند از جام ما رندان مست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک میخانه سبیل ما بود</p></div>
<div class="m2"><p>آید اینجا هر که او ما را بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر کجا رندیست ما را محرم است</p></div>
<div class="m2"><p>هرکجا جامیست با ما همدم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صورت او مظهر معنی ماست</p></div>
<div class="m2"><p>این و آن دو شاهد دعوی ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم وحدانیست علم عارفان</p></div>
<div class="m2"><p>علم اگر خوانی چنین علمی بخوان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قول ما صدیق تصدیقش کند</p></div>
<div class="m2"><p>او محقق نیست تحقیقش کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>تا ننوشی می ندانی ذوق می</p></div>
<div class="m2"><p>تا نگردی وی نیابی حال وی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مستم و خوردم شراب بی حساب</p></div>
<div class="m2"><p>هرکه بیند گویدم خورده شراب</p></div></div>