---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>حمد آن حامدی که محمود است</p></div>
<div class="m2"><p>بخشش اوست هر چه موجود است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فرض عین است حمد حضرت او</p></div>
<div class="m2"><p>بر همه خلق خاصه بر من و تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حمد او از کلام او گویم</p></div>
<div class="m2"><p>لاجرم حمد او نکو گویم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شکر شکر او چه شیرین است</p></div>
<div class="m2"><p>شکر گویم که شکّرم این است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مدح صنعت چو مدح صانع اوست</p></div>
<div class="m2"><p>مدح جمله بگو که این نیکوست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چه مخلوق حضرت اویند</p></div>
<div class="m2"><p>همه تسبیح حضرتش گویند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد هزاران درود در هر دم</p></div>
<div class="m2"><p>بر روان خلاصهٔ عالم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آنکه عالم طفیل او باشد</p></div>
<div class="m2"><p>روح قدسی ز خیل او باشد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عارف سرّ عین عالم اوست</p></div>
<div class="m2"><p>واقف راز اسم اعظم اوست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقل اول وزیر آن شاه است</p></div>
<div class="m2"><p>باطنا شمس و ظاهرا ماه است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>در الف نقطه ایست بنهفته</p></div>
<div class="m2"><p>اول و آخر الف نقطه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نقطه در الف نموده جمال</p></div>
<div class="m2"><p>الفی در حروف بسته خیال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی الف بی و بی الف بی تی</p></div>
<div class="m2"><p>الفی بی نقط بود نی تی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>قطب عالم چو نقطه پرگار است</p></div>
<div class="m2"><p>دایره گرد نقطه در کار است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مظهر اسم اعظمش خوانم</p></div>
<div class="m2"><p>بلکه خود اسم اعظمش دانم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>اول او دلایل است به حق</p></div>
<div class="m2"><p>واقف است از مقید و مطلق</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عارفانی که علم ما دانند</p></div>
<div class="m2"><p>صفت و ذات و اسم اعظمش دانم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>اسم الله اصل اسم ویست</p></div>
<div class="m2"><p>آن یکی گنج و این طلسم وی است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>کل شیئی له کمرآت</p></div>
<div class="m2"><p>وجه کلها مساوات</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لیس بینی و بینه بین</p></div>
<div class="m2"><p>هو فی العین لاتقل عین</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عین وحدت ظهور چون فرمود</p></div>
<div class="m2"><p>بحر در قطره رو به ما بنمود</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>گر هزار است ور هزار هزار</p></div>
<div class="m2"><p>اول او یکی بود به شمار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>آینه صدهزار می بینم</p></div>
<div class="m2"><p>در همه روی یار می بینم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>بلکه یک آینه بود این جا</p></div>
<div class="m2"><p>صور مختلف در او پیدا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>کون کونی یکون من کونه</p></div>
<div class="m2"><p>عین عینی بعینه عینه</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>یک شراب است و جام رنگارنگ</p></div>
<div class="m2"><p>رنگ بی رنگ می دهد نیرنگ</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>رنگ می رنگ جام می باشد</p></div>
<div class="m2"><p>وین عجب بین که جام می باشد</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر کجا ساغریست می دارد</p></div>
<div class="m2"><p>جان سرمست ذوق وی دارد</p></div></div>