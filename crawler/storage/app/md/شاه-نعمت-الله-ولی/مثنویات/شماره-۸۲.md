---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>جنّت ذاتند اعیان گوش کن</p></div>
<div class="m2"><p>در چنین جنّت شرابی نوش کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالم ارواح جنات صفات</p></div>
<div class="m2"><p>جمله می ‌یابند ازین جنّت حیات</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک باشد جنّت خاص ملک</p></div>
<div class="m2"><p>بینم اینجا حضرت خاص ملک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جنّت افعال این جنّت بود</p></div>
<div class="m2"><p>جنّت زیبای پر حکمت بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جنّت او عین روح و جسم ماست</p></div>
<div class="m2"><p>ساتر اوئیم و جنّت اسم ماست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جنّت او با تو چون کردم بیان</p></div>
<div class="m2"><p>جنّت تو با تو گویم هم بدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سر بیت اللّه اگر دانی توئی</p></div>
<div class="m2"><p>جنّت حضرت که می خوانی توئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جنّت زاهد بود در آن سرا</p></div>
<div class="m2"><p>بوستانی بس نزه پر میو‌ه‌ها</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نعمت بسیار و حوران بی شمار</p></div>
<div class="m2"><p>هر چه خواهد نفس باشد صد هزار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنّت اعمال می خوانند این</p></div>
<div class="m2"><p>نیکوئی کن تا جزا یابی چنین</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>عارفان را جنّتی دیگر بود </p></div>
<div class="m2"><p>جنّت ایشان ازین خوشتر بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گر به خلق حق تخلق یافتی</p></div>
<div class="m2"><p>با چنین جنّت تعلق یافتی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>متصف شو با صفات حضرتش</p></div>
<div class="m2"><p>تا بیابی جنّتی از رحمتش</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>جنّت ذاتست اعلای جنان</p></div>
<div class="m2"><p>جنّت صاحبدلان است آن چنان</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در ظهور ذات این جنّت بود</p></div>
<div class="m2"><p>در چنین جنّت چنان حضرت بود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با تو گفتم جنّت هر دو سرا</p></div>
<div class="m2"><p>در بهشت جاودان ما درآ</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>حال جنّت نیک دریابی تمام</p></div>
<div class="m2"><p>گر تو از اهل بهشتی والسلام</p></div></div>