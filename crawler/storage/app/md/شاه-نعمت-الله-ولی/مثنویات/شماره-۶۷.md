---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>در هر آن پیرهن که خواهی مرد</p></div>
<div class="m2"><p>خواه کرباس گیر و خواهی برد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم در آن پیرهن شوی محشور</p></div>
<div class="m2"><p>در ما صبیح دیده‌ ام مسطور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنکه گوید که پیرهن این است</p></div>
<div class="m2"><p>گو بگو ظاهر سخن این است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور بگوید که پیرهن بدن است</p></div>
<div class="m2"><p>یوسفی در درون پیرهن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ممکن است این و آن ولی بر ما</p></div>
<div class="m2"><p>پیرهن از صفت بود جان را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامهٔ جان چنان که یافته‌ ای</p></div>
<div class="m2"><p>هم تو پوشی همان که بافته‌ ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنچه رشتی و بافتی جانا</p></div>
<div class="m2"><p>خود بپوشی پلاس یا دیبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر پلاس است جامه‌ ات آن دم</p></div>
<div class="m2"><p>هیچ سودی ندارت ماتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ور حریر است و جامهٔ شاهی</p></div>
<div class="m2"><p>خوش بپوشش که خوشتر از ماهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>پیرهن چون برون کنی از تن</p></div>
<div class="m2"><p>هنر و عیب تن شود روشن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آشکارا شود چنانکه بود</p></div>
<div class="m2"><p>بنماید به تو همان که بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>جامه از علم وز عمل می‌ دوز</p></div>
<div class="m2"><p>جامه دوزی بیا ز ما آموز</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>خلعت خاص پوش سلطانی</p></div>
<div class="m2"><p>حیف باشد که برهنه مانی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>خرقه دوزم ز وصلهٔ اخلاق</p></div>
<div class="m2"><p>بهر یاران خود علی الاطلاق</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>هرکه را پیرهن چنین باشد</p></div>
<div class="m2"><p>یوسف او در آستین باشد</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر چه بسیار جامه بخشیدیم</p></div>
<div class="m2"><p>به از این جامه‌ ای نپوشیدیم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بستان یادگار ما درپوش</p></div>
<div class="m2"><p>تاج بر سر نه و علم بر دوش</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>جامهٔ آخرت چنین باشد</p></div>
<div class="m2"><p>آخر این سخن همین باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گفت پیغمبر خدا که خدا</p></div>
<div class="m2"><p>این چنین گفت از کرم با ما</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر که داند که من که سلطانم</p></div>
<div class="m2"><p>گر به بخشم گناه بتوانم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>عفو فرمایمش گناه تمام</p></div>
<div class="m2"><p>هیچ باکم نه از خواص و عوام</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>سخنی با موحد است ای یار</p></div>
<div class="m2"><p>هر که شرک آورد رود در ناز</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ما نداریم شرک و می‌داند</p></div>
<div class="m2"><p>گر به بخشد گناه بتواند</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>پای تا سر همه گنه کاریم</p></div>
<div class="m2"><p>لیکن امید عفو می ‌داریم</p></div></div>