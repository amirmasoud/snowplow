---
title: >-
    شمارهٔ ۳۰
---
# شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>در خرابات مغان رندانه رو</p></div>
<div class="m2"><p>خم می را نوش کن مستانه رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خرابات مغان رندی بجو</p></div>
<div class="m2"><p>حال سرمستی ما با او بگو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دردمندی جوی و درمان را طلب</p></div>
<div class="m2"><p>کفر را بگذار و ایمان را طلب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خوش درین دریای بی پایان در آ</p></div>
<div class="m2"><p>تا ببینی آبروی ما به ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با حباب و آب اگر داری نظر</p></div>
<div class="m2"><p>یک دمی در عین این دریا نگر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این چنین دریای وحدت را بجو</p></div>
<div class="m2"><p>گرد هستی را ز خود نیکو بشو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرکه را خواهی به نور او نگر</p></div>
<div class="m2"><p>بد مبین ای یار من نیکو نگر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در خرابات ار بیابی رند مست </p></div>
<div class="m2"><p>به که با مخمور باشی همنشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عشق او شمع است تو پروانه باش</p></div>
<div class="m2"><p>در طریق عاشقی مردانه باش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ساقی ار بخشد تو را پیمانه ای</p></div>
<div class="m2"><p>نوش کن می جو دگر خمخانه ای</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گر تو داری همت عالی تمام</p></div>
<div class="m2"><p>هرچه می خواهی بیابی والسلام</p></div></div>