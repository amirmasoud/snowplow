---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>صورت ما پرده دار او بود</p></div>
<div class="m2"><p>معنی ما حاجت نیکو بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینهٔ ما مخزن اسرار او</p></div>
<div class="m2"><p>دیدهٔ ما منظر انوار او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه ما داریم ملک او بود</p></div>
<div class="m2"><p>مالک و ملکش همه نیکو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ملک او مائیم و ملک ماست اوست</p></div>
<div class="m2"><p>گر ملک جوئی درین ملکش بجو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک ما از ملک او اعظم بود</p></div>
<div class="m2"><p>نه بدین معنی که بیش و کم بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ملک او اعیان و پنهان ملک ما</p></div>
<div class="m2"><p>اسم جامع جمع اسماء خدا</p></div></div>