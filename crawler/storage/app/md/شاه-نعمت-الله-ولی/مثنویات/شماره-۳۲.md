---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>آفتابی در قمر پیدا شده</p></div>
<div class="m2"><p>فتنهٔ دور قمر در وا شده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست عالم صورت اسمای او</p></div>
<div class="m2"><p>صورت و معنی به هم باشد نکو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسم او ذات و صفات او بود</p></div>
<div class="m2"><p>نام او یک نزد ما آن دو بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی اسم و مسمی باز جو</p></div>
<div class="m2"><p>عارفی را گر بیابی راز گو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آفتابی رو نموده مه لقا</p></div>
<div class="m2"><p>بنگر این آئینهٔ گیتی نما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ذره ای بی نور او بینیم ، نی</p></div>
<div class="m2"><p>یک نفس با غیر بنشینیم ، نی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علم ذوقست ای برادر گوش کن</p></div>
<div class="m2"><p>جام می شادی رندان نوش کن</p></div></div>