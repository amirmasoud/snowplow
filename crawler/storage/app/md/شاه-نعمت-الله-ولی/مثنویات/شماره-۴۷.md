---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>همه عالم حجاب و عین حجاب</p></div>
<div class="m2"><p>غیر او نیست این سخن دریاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دفتر کاینات می خوانم</p></div>
<div class="m2"><p>معنیش حرف حرف می دانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شانه را گر هزار دندانه است</p></div>
<div class="m2"><p>یک حقیقت هویت آن است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بگویم هزار یک سخنست</p></div>
<div class="m2"><p>یوسفی را هزار پیرهن است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظلمت و نور هر دو یک ذاتند</p></div>
<div class="m2"><p>گرچه اندر ظهور آیاتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ور ظهور است این منی و توئی</p></div>
<div class="m2"><p>به مسما یکی به اسم دوئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آنکه انسان کاملش نام است</p></div>
<div class="m2"><p>نزد رندان چو باده و جامست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوش کن جام می که نوشت باد</p></div>
<div class="m2"><p>خم می دائما به جوشت باد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ساغر می مدام می نوشم</p></div>
<div class="m2"><p>خلعت از جود عشق می پوشم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ما خراباتیان سرمستیم</p></div>
<div class="m2"><p>در خرابات عشق پابستیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>می و جامیم و جان و جانانه</p></div>
<div class="m2"><p>شاه و دُستور و کنج ویرانه</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شیخ مرشد جنید بغدادی</p></div>
<div class="m2"><p>مصر معنی و مشق دلشادی</p></div></div>