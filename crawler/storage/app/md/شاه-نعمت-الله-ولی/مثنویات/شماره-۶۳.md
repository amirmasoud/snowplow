---
title: >-
    شمارهٔ ۶۳
---
# شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>گنج اسم اعظم از ذات و صفات</p></div>
<div class="m2"><p>آشکارا کرده اندر کائنات</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کجا کنجی است گنجی در وی است</p></div>
<div class="m2"><p>کنج هر ویرانه بی گنجی کی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>معنی او گنج و صورت چون طلسم</p></div>
<div class="m2"><p>در چنین گنجی بود آن گنج اسم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جام می باشد حبابی پر ز آب</p></div>
<div class="m2"><p>نوش کن جامی که دریابی شراب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نسخهٔ اسما بجو یک یک بخوان</p></div>
<div class="m2"><p>وحدت اسم و مسما را بدان</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی من و تو ، من تو ام تو هم منی</p></div>
<div class="m2"><p>ور تو من گوئی و تو باشد منی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در مراتب آن یکی باشد هزار</p></div>
<div class="m2"><p>در هزاران آن یکی را می شمار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن یکی در هر یکی پیدا شده</p></div>
<div class="m2"><p>قطره قطره آمده دریا شده</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسم اعظم گنج و اسما چون طلسم</p></div>
<div class="m2"><p>نعمت الله را بجو دریاب اسم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آفتابی را ببین در ذره ای</p></div>
<div class="m2"><p>عین دریا را نگر در قطره ای</p></div></div>