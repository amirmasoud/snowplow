---
title: >-
    شمارهٔ ۵۸
---
# شمارهٔ ۵۸

<div class="b" id="bn1"><div class="m1"><p>تا نگیری دامن رهبر به دست</p></div>
<div class="m2"><p>کی ز گمراهی توانی بازرست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره بیابان است و تو گمره کجا</p></div>
<div class="m2"><p>ره توانی برد ای مرد خدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیدهٔ تو بسته و راهی دراز</p></div>
<div class="m2"><p>بی دلیلی چون روی راه حجاز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رهروی کن در طریق نیستی</p></div>
<div class="m2"><p>شاید اندر هیچ منزل نایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رهنمائی جو قدم در راه نه</p></div>
<div class="m2"><p>گر روی در راه با همراه به</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کار بی مرشد کجا گردد تمام</p></div>
<div class="m2"><p>مرشدی باید مکمل والسلام</p></div></div>