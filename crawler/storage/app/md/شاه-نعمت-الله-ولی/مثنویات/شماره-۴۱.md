---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>آن یکی کوزه ای ز یخ برداشت</p></div>
<div class="m2"><p>کرد پر آب یک زمان بگذاشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون هوا ز آفتاب گرمی یافت</p></div>
<div class="m2"><p>گرمیش بر وجود کوزه بتافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب شد برف و کوزه شد با آب</p></div>
<div class="m2"><p>اسم و رسم از میانه شد دریاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اول ما چو آخر ما شد</p></div>
<div class="m2"><p>قطره دریاست چون به دریا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قطر و بحر و موج و جو آبند</p></div>
<div class="m2"><p>عین ما را به عین ما یابند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نقد گنجینه ای قدم مائیم</p></div>
<div class="m2"><p>گرچه موجیم عین دریائیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آب در هر قدح که جا گیرد</p></div>
<div class="m2"><p>در زمان رنگ آن انا گیرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر نه آبست اصل گوهر چیست</p></div>
<div class="m2"><p>جوهر گوهر منور چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همه عالم چو گوهری دریاب</p></div>
<div class="m2"><p>عین او بین و جوهری دریاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چیست عالم به نزد درویشان</p></div>
<div class="m2"><p>پرده دار حقیقت ایشان</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آن حقیقت که اول همه اوست</p></div>
<div class="m2"><p>صورتش عالمست و معنی دوست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گنج و گنجینه و طلسم نگر</p></div>
<div class="m2"><p>صفت و ذات بین و اسم نگر</p></div></div>