---
title: >-
    شمارهٔ ۳۸
---
# شمارهٔ ۳۸

<div class="b" id="bn1"><div class="m1"><p>وجودی در همه عالم عیان است</p></div>
<div class="m2"><p>ولی از دیدهٔ مردم نهان است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به هر آئینه حسنی می نماند</p></div>
<div class="m2"><p>ز هر برجی به شکلی نو برآید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نقد گنج او در کنج عالم</p></div>
<div class="m2"><p>طلب این کنج و این گنجینه فافهم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حقیقت در دو عالم جز یکی نیست</p></div>
<div class="m2"><p>یکی هست و در آن مأوا شکی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال ار نقش می بندد به خوابی</p></div>
<div class="m2"><p>جز او تعبیر خوابی خود نیابی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز می جامیست پر می بر کف ما</p></div>
<div class="m2"><p>حبابی می نماید عین دریا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>که دارد این چنین ذوقی که ما راست</p></div>
<div class="m2"><p>که ذوق ما همه عالم بیاراست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>معانی بیان نعمت الله</p></div>
<div class="m2"><p>بپرس از آفتاب و حضرت ماه</p></div></div>