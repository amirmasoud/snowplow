---
title: >-
    شمارهٔ ۵۲
---
# شمارهٔ ۵۲

<div class="b" id="bn1"><div class="m1"><p>صفت و ذات بین و اسم نگر</p></div>
<div class="m2"><p>گنج و گنجینه و طلسم نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در چنین بحر بیکرانه در آ</p></div>
<div class="m2"><p>نظری کن به عین ما در ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جامی گیتی نما به دست آور</p></div>
<div class="m2"><p>مظهر حضرت خدا بنگر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقطه اصل گر چه ما دانی</p></div>
<div class="m2"><p>هفت هیکل به ذوق برخوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آینه صد هزار می شمرد</p></div>
<div class="m2"><p>در همه آینه یکی نگرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواه تنها و خواه ناتنها</p></div>
<div class="m2"><p>گر بود با خدا بود همه جا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوشهٔ چشم سوی او دارد</p></div>
<div class="m2"><p>نقش او در خیال بنگارد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در گلستان اگر گلی چیند</p></div>
<div class="m2"><p>شیشهٔ پر گلاب را بیند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر خرد را فروشد آن عاقل</p></div>
<div class="m2"><p>نشود از خدای خود غافل</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جزو و کل را باعتبار سپار</p></div>
<div class="m2"><p>کاعتباریست جزو و کل ای یار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جز خدا را احد نمی گوئیم</p></div>
<div class="m2"><p>از احد جز خدا نمی جوئیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دو آئینه رو نمود آن یک</p></div>
<div class="m2"><p>دو نماید یکی بود بی شک</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>غرق آبند عالمی چو حباب</p></div>
<div class="m2"><p>ظاهرش ساغر است و باطن آب</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>سایهٔ او به ما چو پیدا شد</p></div>
<div class="m2"><p>از من و تو دوئی هویدا شد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اصل و فرعی به همدگر پیوست</p></div>
<div class="m2"><p>هست پیوند ما به او پیوست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>سخن عارفان از او باشد</p></div>
<div class="m2"><p>لاجرم قولشان نکو باشد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>او به او دیده می شود ای دوست</p></div>
<div class="m2"><p>نظری گر کنی چنین نیکوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نور رویش به چشم ما بنمود</p></div>
<div class="m2"><p>چون بدیدیم نور او ، او بود</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>احدی آمده کمر بسته</p></div>
<div class="m2"><p>میم احمد به تخت بنشسته</p></div></div>