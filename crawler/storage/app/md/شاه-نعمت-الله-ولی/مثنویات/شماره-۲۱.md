---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>هر یک از اسمای حق در علم او</p></div>
<div class="m2"><p>صورتی دارد که باشد عین تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نور هر عینی که می بیند بصر</p></div>
<div class="m2"><p>وجه خاصی می نماید در نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جود او بخشید اسما را وجود</p></div>
<div class="m2"><p>ورنه اسما را به خود بودی نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چه موجود است مرحوم خداست</p></div>
<div class="m2"><p>گر چه اسمای وی و اعیان ماست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کثرت اسمای او اندر عدم</p></div>
<div class="m2"><p>از صفاتش نقش می بندد قلم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون صفت از ذات او دارد وجود</p></div>
<div class="m2"><p>رحمت ذاتش غضب را داده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>راحم و مرحوم از آن می خوانمش</p></div>
<div class="m2"><p>اسم او ذات و صفت می دانمش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نسخهٔ اعیان اگر خوانی تمام</p></div>
<div class="m2"><p>شرح اسما را بدانی والسلام</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>جملهٔ عالم تن است و عشق و جان</p></div>
<div class="m2"><p>اسم ظاهر این و باطن اسم آن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>یک مسمی دان و اسما صدهزار</p></div>
<div class="m2"><p>یک وجود و صد هزارش اعتبار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>صورتش جام است و معنی می بود</p></div>
<div class="m2"><p>گرچه هر دو نزد ما یک شی بود</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در دو میدان یک یکی و دو یکی</p></div>
<div class="m2"><p>نیک دریابش که گفتم نیککی</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>بی وجود او همه عالم عدم</p></div>
<div class="m2"><p>بر وجود او همه عالم عَلَم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>عالم از بسط وجود عام اوست</p></div>
<div class="m2"><p>هرچه می بینی ز جود عام اوست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>اوئی او ذاتی و ماتی ما</p></div>
<div class="m2"><p>عارضی باشد فنا شو زین فنا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مائی عالم نقاب عالم است</p></div>
<div class="m2"><p>بلکه عالم خود حجاب عالم است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>جاودان این حجاب ای جان من</p></div>
<div class="m2"><p>ای خلیل الله ِ من برهان من</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>حال عالم با تو می گویم تمام</p></div>
<div class="m2"><p>تا بدانی حال عالم والسلام</p></div></div>