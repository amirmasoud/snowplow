---
title: >-
    شمارهٔ ۴۲
---
# شمارهٔ ۴۲

<div class="b" id="bn1"><div class="m1"><p>عدد از واحد آشکارا شد</p></div>
<div class="m2"><p>واحدی در عدد هویدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کثرت و وحدتست در هر باب</p></div>
<div class="m2"><p>مجملا و مفصلا دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کثرتش چون حباب دان دایم</p></div>
<div class="m2"><p>وحدتش بحر و این به آن قایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وحدت و کثرت اعتباری دان</p></div>
<div class="m2"><p>نسخهٔ عقل را چنین می خوان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش عالم خیال می بینم</p></div>
<div class="m2"><p>در خیال آن جمال می بینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او لطیف است و در همه ساری</p></div>
<div class="m2"><p>آب رحمت بجوی او جاری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه حلول است حلّ و حال منست</p></div>
<div class="m2"><p>سخنی از من و کمال منست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هر که در معرفت سخن راند</p></div>
<div class="m2"><p>وصف خود می کند اگر داند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تو منی ، من تو ام دوئی بگذار</p></div>
<div class="m2"><p>من نماندم تو هم دوئی بگذار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انت ما انت و انا ما هو</p></div>
<div class="m2"><p>هو هو لا اله الا هو</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لیس فی الدار غیره باقی</p></div>
<div class="m2"><p>غیره عندنا کر اغراقی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>هر چه داریم جمله جود وی است</p></div>
<div class="m2"><p>جود او نزد ما وجود وی است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ور تو گوئی که غیر او باشد</p></div>
<div class="m2"><p>بد نباشد همه نکو باشد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>تن بود سایبان و جان خورشید</p></div>
<div class="m2"><p>آن یکی چتر دان و آن جمشید</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سایه و شخص می نماید دو</p></div>
<div class="m2"><p>در حقیقت یکی است بی من و تو</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>مرغ سان سوزم و دو جانم پر</p></div>
<div class="m2"><p>سیدم پر ز سوز و سوزم پر</p></div></div>