---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>تو منی من تو ام ، توئی بگذار</p></div>
<div class="m2"><p>بشنو از من تو هم دوئی بگذار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چیست نقش خیال ما و توئی</p></div>
<div class="m2"><p>همچو خوابیست این خیال دوئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابست و عالمش سایه</p></div>
<div class="m2"><p>سایه روشن به نور همسایه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عین اول یکیست تا دانی</p></div>
<div class="m2"><p>عین اول سزد اگر خوانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جام گیتی نماش می خوانند</p></div>
<div class="m2"><p>اصل مجموع عالمش دانند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاشقان از شراب او مستند</p></div>
<div class="m2"><p>همه عالم به نور او هستند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باطنش آفتاب ظاهر ماه</p></div>
<div class="m2"><p>ما محبیم و او حبیب الله</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آبروئی ز عین دریا جو</p></div>
<div class="m2"><p>سر درّ یتیم از ما جو</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نظری کن که نور دیدهٔ ماست</p></div>
<div class="m2"><p>آنه عالم به نور خود آراست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گنج و گنجینه و طلسم نگر</p></div>
<div class="m2"><p>صفت و ذات بین و اسم نگر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مظهر اسم اعظمش خوانم</p></div>
<div class="m2"><p>بلکه خود اسم اعظمش دانم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اسم اعظم طلب کن از کامل</p></div>
<div class="m2"><p>زان که کامل بود بدان فاضل</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سید عالمست و ما بنده</p></div>
<div class="m2"><p>بنده در خدمت است پاینده</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نظری به حال ما فرمود</p></div>
<div class="m2"><p>گنج اسما به ما عطا فرمود</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در گنجینهٔ قدم بگشود</p></div>
<div class="m2"><p>نقد آن گنج را به ما بنمود</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آفتابست و ماه خوانندش</p></div>
<div class="m2"><p>پادشاه و سپاه خوانندش</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اول انبیاء و آخر اوست</p></div>
<div class="m2"><p>باطن اولیا و ظاهر اوست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>همه عالم طفیل او باشد</p></div>
<div class="m2"><p>روح قدسی ز خیل او باشد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>باد بر آل او درود و سلام</p></div>
<div class="m2"><p>بر همه تابعان او به تمام</p></div></div>