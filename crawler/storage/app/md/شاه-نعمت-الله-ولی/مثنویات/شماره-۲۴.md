---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>چیست انسان دیدهٔ بینا بود</p></div>
<div class="m2"><p>جامع مجموعهٔ اسماء بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجمع الطاف اسرار اله</p></div>
<div class="m2"><p>آن ایاز بندگی پادشاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مخزن اسرار سبحانیست او</p></div>
<div class="m2"><p>مطلع انوار ربانیست او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح و جسم و عین و اسم این هر چهار</p></div>
<div class="m2"><p>می نماید او به مردم آشکار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کون جامع نزد ما انسان بود</p></div>
<div class="m2"><p>ور نباشد این چنین حیوان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جامع انسان کامل را بخوان</p></div>
<div class="m2"><p>معنی مجموع قرآن را بدان</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نقش می بندد جمال ذوالجلال</p></div>
<div class="m2"><p>در خیال و صورت او بر کمال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اسم اعظم کارساز ذات اوست</p></div>
<div class="m2"><p>عقل کل یک نقطه از آیات اوست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر چه باشد از حدوث و از قدم</p></div>
<div class="m2"><p>جمع دارد در وجود و در عدم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>لیس فی الامکان ابدع منهم</p></div>
<div class="m2"><p>هکذا قلنا واسمع منهم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسم اعظم می نماید صورتش</p></div>
<div class="m2"><p>این معما می گشاید صورتش</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>صورتش آئینهٔ گیتی نماست</p></div>
<div class="m2"><p>معنی او پرده دار کبریاست</p></div></div>