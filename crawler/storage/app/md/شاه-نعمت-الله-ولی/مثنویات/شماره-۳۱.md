---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ابتدا کردم به نام آن یکی</p></div>
<div class="m2"><p>در وجود آن یکی نبود شکی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک وجود است و صفاتش بی شمار</p></div>
<div class="m2"><p>آن یکی در هر یکی خوش می شمار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم احول گر دو بیند تو مبین</p></div>
<div class="m2"><p>تو یکی می بین چو احول دو مبین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر هزار آئینه دیدم ور یکی</p></div>
<div class="m2"><p>آن یکی را دیده ام در هر یکی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>علم او آئینهٔ ذات وی است</p></div>
<div class="m2"><p>آئینه خود غیر ذات او کی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>او تجلی کرده خوش در آینه</p></div>
<div class="m2"><p>می نماید آن یکی هر آینه</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی او بنگر به نور روی او</p></div>
<div class="m2"><p>تا چو آئینه نماید روبرو</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نوش کن جام حبابی پر ز آب</p></div>
<div class="m2"><p>تا خبر یابی ز جام و از شراب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ما درین دریا به هر سو می رویم</p></div>
<div class="m2"><p>آبرو داریم و نیکو می رویم</p></div></div>