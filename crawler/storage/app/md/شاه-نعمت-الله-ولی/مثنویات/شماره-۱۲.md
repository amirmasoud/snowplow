---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>عین ما مانَد حبابی پر ز آب</p></div>
<div class="m2"><p>گر چه خالی می نماید این حباب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر تو می خوانم ازین بیتی هزار</p></div>
<div class="m2"><p>یاد می گیرش ز من این یادگار</p></div></div>