---
title: >-
    شمارهٔ ۶۱
---
# شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>اولا توحید کلی آن اوست</p></div>
<div class="m2"><p>کل کلیّات در فرمان اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنگهی ابلاغ جامع یافته</p></div>
<div class="m2"><p>در همه مصنوع صانع یافته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کون جامع مظهر ذات صفات</p></div>
<div class="m2"><p>سایهٔ حق آفتاب کائنات</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وجهی از امکان و وجهی از وجوب</p></div>
<div class="m2"><p>در شهادت آمد ازغیب الغیوب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صورت و معنی به هم آراسته</p></div>
<div class="m2"><p>ظاهر و باطن به هم پیراسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمع کرده خلق و با خود همدگر</p></div>
<div class="m2"><p>همچو نوری می نماید در نظر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هفت دریا قطره ای از جام او</p></div>
<div class="m2"><p>روح قدسی رند دُرد آشام او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چیست عالم بی وجود او عدم</p></div>
<div class="m2"><p>می دهد جودش وجودی دم به دم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بندهٔ اوئیم و او سلطان ما</p></div>
<div class="m2"><p>جسم و جان مائیم و او جانان ما</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>سرور مجموع رندان میر ماست</p></div>
<div class="m2"><p>این چنین ساقی مستی پیر ماست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>آفتابست او ولی نامش قمر</p></div>
<div class="m2"><p>آفتابی در قمر خوش می نگر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>نور او در چشم ما ظاهر شده</p></div>
<div class="m2"><p>آمده منظور ما ناظر شده</p></div></div>