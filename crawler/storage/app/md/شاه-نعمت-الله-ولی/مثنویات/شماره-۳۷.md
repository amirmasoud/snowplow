---
title: >-
    شمارهٔ ۳۷
---
# شمارهٔ ۳۷

<div class="b" id="bn1"><div class="m1"><p>بیا با ما درین دریا به سر بر</p></div>
<div class="m2"><p>از اینجا دامنی خوش پر گهر بر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز ما بشنو حبابی پر کن از آب</p></div>
<div class="m2"><p>حباب از آب و در وی آب دریاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به معنی آب در صورت حباب است</p></div>
<div class="m2"><p>ببین در این و آن کان هر دو آبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دمی در آفتاب و سایه بنگر</p></div>
<div class="m2"><p>در آن هم سایه را همسایه بنگر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه دریائی که ما غرقیم در وی</p></div>
<div class="m2"><p>چه خوش جامی که ما داریم پر می</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درین دریا به عین ما نظر کن</p></div>
<div class="m2"><p>صدف بشکن تماشای گهر کن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر نورست اگر ظلمت که او راست</p></div>
<div class="m2"><p>به راه کج مرو بشنو ز ما راست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وجودی جز وجود او نبینی</p></div>
<div class="m2"><p>اگر آئی به چشم ما نشینی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>به نور او جمال او توان دید</p></div>
<div class="m2"><p>چنین می بین که سید آنچنان دید</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نشان بی نشانی عارفان است</p></div>
<div class="m2"><p>اگرچه بی نشانی هم نشان است</p></div></div>