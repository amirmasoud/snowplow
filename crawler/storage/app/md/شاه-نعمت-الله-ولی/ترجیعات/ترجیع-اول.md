---
title: >-
    ترجیع اول
---
# ترجیع اول

<div class="b" id="bn1"><div class="m1"><p>تا لوای حیدری بر طارم خضرا زدند</p></div>
<div class="m2"><p>کوس غرّش بر فراز عالم اعلا زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا که در خلوت سرای لی مع الله شد مقیم</p></div>
<div class="m2"><p>ساکنان درگهش زان دم ز او ادنی زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جود او مفتاح موجودات کردند آنگهی</p></div>
<div class="m2"><p>قفل حیرت بر زبان نطق هر گویا زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرفرازان در هوای خاک پایش همچو ما</p></div>
<div class="m2"><p>از سر همت قدم بر تارک دنیا زدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پادشاهان از برای حشمت شاهنشهی</p></div>
<div class="m2"><p>سکهٔ دولت به نامش بر سر زرها زدند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عارفان تا نکته ای خواندند از اسرار او</p></div>
<div class="m2"><p>طعنها بر گفته های بوعلی سینا زدند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لَمعه ای از آفتاب ذوالفقارش شد پدید</p></div>
<div class="m2"><p>عارفان تمثال نورش بر ید بیضا زدند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>حکم فرمانش بنام انّما کرده نشان</p></div>
<div class="m2"><p>ابلغ توقیع آل آلش از طه زدند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مقصد و مقصود عالم اوست و ابن عم او</p></div>
<div class="m2"><p>این ندا روز ازل در گوش جان ما زدند</p></div></div>
<div class="b2" id="bn10"><p> نفس خیر المرسلین است آن ولی کردگار</p>
<p> لافتی الا علی لاسیف الا ذوالفقار</p></div>
<div class="b" id="bn11"><div class="m1"><p>نور چشم عالمش خوانم علی مرتضی</p></div>
<div class="m2"><p>محرم راز رسول و ابن عم مصطفی</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>گوهر دریای عرفان بحر و علم کان وجود</p></div>
<div class="m2"><p>رهنمون رهروان و پیشوای اتقیا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>هادئی کز نسل او مهدی هویدا می شود</p></div>
<div class="m2"><p>شاید ار گویند او را اهل حق نور هدی</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از ولای او ولایت یافته هر کو ولیست</p></div>
<div class="m2"><p>رو موالی شو که این است اعتقاد اولیا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>دوستدار خاندان باش و محب اهلبیت</p></div>
<div class="m2"><p>تابع دین محمد باش و از بهر خدا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>نیست مؤمن هر که دارد با علی یک مو خلاف</p></div>
<div class="m2"><p>یار مؤمن شو چو ما و تابع آل عبا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از محبت آفتابی بر دل ما تافته</p></div>
<div class="m2"><p>می نماید نور او آئینهٔ گیتی نما</p></div></div>
<div class="b2" id="bn18"><p> نفس خیر المرسلین است آن ولی کردگار</p>
<p> لافتی الا علی لاسیف الا ذوالفقار</p></div>
<div class="b" id="bn19"><div class="m1"><p>مسند ملک ولایت درحقیقت آن اوست</p></div>
<div class="m2"><p>در حریم عصمتش روح القدس دربان اوست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هر کسی از گنج سلطانی عطائی یافته</p></div>
<div class="m2"><p>نقد گنج کنت کنزأ نزد سید آن اوست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>حق تعالی وصف او فرمود در قرآن تمام</p></div>
<div class="m2"><p>هفت هیکل هر که خواند آیتی در شأن اوست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>حاکم او در ولایت اولیا او را مرید</p></div>
<div class="m2"><p>شاه عالم خوانمش هر کو علی سلطان اوست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>یافته حکم ولایت از خدا و مصطفی</p></div>
<div class="m2"><p>هر چه هست از جزء و کل پیوسته در فرمان اوست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روح اعظم جان عالم عقل کل از جان و دل</p></div>
<div class="m2"><p>در امامت این امام انس و جان جانان اوست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرچه عالم از عطای نعمت الله منعمند</p></div>
<div class="m2"><p>نعمت الله نعمت شایسته از احسان اوست</p></div></div>
<div class="b2" id="bn26"><p> نفس خیر المرسلین است آن ولی کردگار</p>
<p> لافتی الا علی لاسیف الا ذوالفقار</p></div>