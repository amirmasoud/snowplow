---
title: >-
    دوبیتی شمارهٔ ۲۱۹
---
# دوبیتی شمارهٔ ۲۱۹

<div class="b" id="bn1"><div class="m1"><p>ذکر حق می گوی و در خلوت نشین</p></div>
<div class="m2"><p>باش فارغ از چنان و از چنین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حاصل عمر ای عزیزان یک دم است</p></div>
<div class="m2"><p>دم به دم در یک دمی با ما نشین</p></div></div>