---
title: >-
    دوبیتی شمارهٔ ۶۳
---
# دوبیتی شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>یوسف گل پیرهن سلطان ماست</p></div>
<div class="m2"><p>این چنین خوش گلستانی آن ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاجرم هر بلبلی کآید به باغ</p></div>
<div class="m2"><p>او همی نالد که او جانان ماست</p></div></div>