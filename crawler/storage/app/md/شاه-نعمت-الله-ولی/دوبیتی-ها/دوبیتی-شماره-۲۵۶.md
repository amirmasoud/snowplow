---
title: >-
    دوبیتی شمارهٔ ۲۵۶
---
# دوبیتی شمارهٔ ۲۵۶

<div class="b" id="bn1"><div class="m1"><p>گر یکی را دو بار بشماری</p></div>
<div class="m2"><p>آن یکی را دو یک نگهداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دو یکی باشد و یکی دو عجب</p></div>
<div class="m2"><p>یاد دارش ز یار از یاری</p></div></div>