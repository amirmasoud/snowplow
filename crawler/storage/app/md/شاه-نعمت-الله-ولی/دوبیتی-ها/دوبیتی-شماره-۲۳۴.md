---
title: >-
    دوبیتی شمارهٔ ۲۳۴
---
# دوبیتی شمارهٔ ۲۳۴

<div class="b" id="bn1"><div class="m1"><p>تا تونشوی یگانهٔ او</p></div>
<div class="m2"><p>هرگز نشود یگانه آن دو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باشی تو یگانهٔ دو عالم</p></div>
<div class="m2"><p>آن دم که اثر نماند از تو</p></div></div>