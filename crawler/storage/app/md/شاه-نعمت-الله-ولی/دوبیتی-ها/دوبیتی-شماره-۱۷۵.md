---
title: >-
    دوبیتی شمارهٔ ۱۷۵
---
# دوبیتی شمارهٔ ۱۷۵

<div class="b" id="bn1"><div class="m1"><p>عارفانه اول و آخر نگر</p></div>
<div class="m2"><p>هر چه بینی باطن و ظاهر نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این و آن با همدگر نیکو ببین</p></div>
<div class="m2"><p>عین و اعیان مظهر و مظهر نگر</p></div></div>