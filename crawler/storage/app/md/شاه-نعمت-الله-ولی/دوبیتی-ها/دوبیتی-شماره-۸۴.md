---
title: >-
    دوبیتی شمارهٔ ۸۴
---
# دوبیتی شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>همه نیکند و هیچ خود بد نیست</p></div>
<div class="m2"><p>آنکه نیکو نباشد او خود نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز یکی نیست در همه عالم</p></div>
<div class="m2"><p>صد مگو ای عزیز من صد نیست</p></div></div>