---
title: >-
    دوبیتی شمارهٔ ۷۸
---
# دوبیتی شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>به همه صورتی مصور اوست</p></div>
<div class="m2"><p>به همه نورها منور اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندهٔ حضرت خداوند است</p></div>
<div class="m2"><p>پادشاه تمام کشور اوست</p></div></div>