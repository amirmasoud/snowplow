---
title: >-
    دوبیتی شمارهٔ ۲۲۸
---
# دوبیتی شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>خوش بگو اللّه و اسم ذات بین</p></div>
<div class="m2"><p>جمله اشیا مصحف و آیات بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در زمین و آسمان می کن نظر</p></div>
<div class="m2"><p>نور او در دیدهٔ ذرات بین</p></div></div>