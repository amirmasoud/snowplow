---
title: >-
    دوبیتی شمارهٔ ۲۹
---
# دوبیتی شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>دل تو خلوت محبت اوست</p></div>
<div class="m2"><p>جانت آئینه دار طلعت اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه پاک دار و دل خالی</p></div>
<div class="m2"><p>که نظرگاه خاص حضرت اوست</p></div></div>