---
title: >-
    دوبیتی شمارهٔ ۵۳
---
# دوبیتی شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>عین ما این سخن چو با ما گفت</p></div>
<div class="m2"><p>قطره را جمع کرد و دریا گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن از عقل ما نمی گوئیم</p></div>
<div class="m2"><p>سخن از عقل پورسینا گفت</p></div></div>