---
title: >-
    دوبیتی شمارهٔ ۴۴
---
# دوبیتی شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>جنت نفس دوزخ جان است</p></div>
<div class="m2"><p>ترک دوزخ بگو بهشت آنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبش آتش نماید ، آتش آب</p></div>
<div class="m2"><p>دوزخش در بهشت پنهان است</p></div></div>