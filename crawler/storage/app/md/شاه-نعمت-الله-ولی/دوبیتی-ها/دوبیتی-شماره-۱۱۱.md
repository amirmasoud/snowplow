---
title: >-
    دوبیتی شمارهٔ ۱۱۱
---
# دوبیتی شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>نور او را به نور او بیند</p></div>
<div class="m2"><p>هر چه بیند همه نکو بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم از او گوید و از او شنود</p></div>
<div class="m2"><p>نه چو احول یکی به دو بیند</p></div></div>