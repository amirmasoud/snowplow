---
title: >-
    دوبیتی شمارهٔ ۲۰
---
# دوبیتی شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>درّهٔ بیضا ز بحر ما طلب</p></div>
<div class="m2"><p>آنچنان گوهر ازین دریا طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عین ما جویی به عین ما بجو</p></div>
<div class="m2"><p>طالب و مطلوب را از ما طلب</p></div></div>