---
title: >-
    دوبیتی شمارهٔ ۲۳
---
# دوبیتی شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>هر بلائی که باشد از محبوب</p></div>
<div class="m2"><p>آن بلا خود مرا بود مطلوب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بلا صبر کن که تا باشی</p></div>
<div class="m2"><p>مبتلای بلاش چون ایوب</p></div></div>