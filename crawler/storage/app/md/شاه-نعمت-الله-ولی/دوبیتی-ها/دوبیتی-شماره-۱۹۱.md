---
title: >-
    دوبیتی شمارهٔ ۱۹۱
---
# دوبیتی شمارهٔ ۱۹۱

<div class="b" id="bn1"><div class="m1"><p>گر بیابی کمال اهل کمال</p></div>
<div class="m2"><p>همچنان باش طالب متعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به گرد نقطه چون پرگار گشتیم</p></div>
<div class="m2"><p>تا ابد می طلب کمال کمال</p></div></div>