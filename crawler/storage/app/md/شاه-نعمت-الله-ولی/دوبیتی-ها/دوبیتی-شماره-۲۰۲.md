---
title: >-
    دوبیتی شمارهٔ ۲۰۲
---
# دوبیتی شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>بسی نقشی که بر دیده کشیدیم</p></div>
<div class="m2"><p>به جز نور جمال او ندیدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون کمالات را نهایت نیست</p></div>
<div class="m2"><p>به آخر هم بدان اول رسیدیم</p></div></div>