---
title: >-
    دوبیتی شمارهٔ ۱۲
---
# دوبیتی شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>به نور غیب روشن شد دل ما</p></div>
<div class="m2"><p>منوّر شد به نورش منزل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تجلّی کرد بر ما حضرت او</p></div>
<div class="m2"><p>چه خوش لطفی که آمد حاصل ما</p></div></div>