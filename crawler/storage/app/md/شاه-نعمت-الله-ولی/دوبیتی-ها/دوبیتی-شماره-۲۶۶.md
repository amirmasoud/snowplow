---
title: >-
    دوبیتی شمارهٔ ۲۶۶
---
# دوبیتی شمارهٔ ۲۶۶

<div class="b" id="bn1"><div class="m1"><p>نگاری مست ولا یعقل چو ماهی</p></div>
<div class="m2"><p>در آمد از در خلوت به گاهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سیه چشم و سیه زلف و سیه خال</p></div>
<div class="m2"><p>سیه گز بود پوشیده سیاهی</p></div></div>