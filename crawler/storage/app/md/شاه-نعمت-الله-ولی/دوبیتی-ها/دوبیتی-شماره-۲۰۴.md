---
title: >-
    دوبیتی شمارهٔ ۲۰۴
---
# دوبیتی شمارهٔ ۲۰۴

<div class="b" id="bn1"><div class="m1"><p>رو به خاک راه او بنهاده ام</p></div>
<div class="m2"><p>خاک آن راهم به راه افتاده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بگوید جان بده بدهم روان</p></div>
<div class="m2"><p>بندهٔ فرمان منتظر استاده ام</p></div></div>