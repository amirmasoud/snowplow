---
title: >-
    دوبیتی شمارهٔ ۱۵۱
---
# دوبیتی شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>یک هویت اول و آخر بود</p></div>
<div class="m2"><p>آن حقیقت باطن و ظاهر بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهر و باطن یکی گوید مدام</p></div>
<div class="m2"><p>در هویت هر که او ناظر بود</p></div></div>