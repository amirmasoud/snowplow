---
title: >-
    دوبیتی شمارهٔ ۸
---
# دوبیتی شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>دعانی من الکرمان ثم دعانیا</p></div>
<div class="m2"><p>فان هواها مولع بهوا نیا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ولا تذکرونی ماءِ ماهان نه</p></div>
<div class="m2"><p>بماهان بی فی الجسم ماکان هانیا</p></div></div>