---
title: >-
    دوبیتی شمارهٔ ۲۵۱
---
# دوبیتی شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>رایت اللّه فی عینی بعینه</p></div>
<div class="m2"><p>و عینی عینه فانظر بعینه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حبیبی عند غیر غیر عینی</p></div>
<div class="m2"><p>و عندی عینه من حیث عینه</p></div></div>