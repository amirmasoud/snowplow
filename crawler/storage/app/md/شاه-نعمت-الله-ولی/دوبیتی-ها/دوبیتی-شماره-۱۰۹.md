---
title: >-
    دوبیتی شمارهٔ ۱۰۹
---
# دوبیتی شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>صبری کنیم تا ستم او چه می‌ کند</p></div>
<div class="m2"><p>با این دل شکسته غم او چه می ‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر کس علاج درد دلی می‌کنند و ما</p></div>
<div class="m2"><p>دم در کشیده تا ستم او چه می ‌کند</p></div></div>