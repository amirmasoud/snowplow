---
title: >-
    دوبیتی شمارهٔ ۲۳۹
---
# دوبیتی شمارهٔ ۲۳۹

<div class="b" id="bn1"><div class="m1"><p>نعمت الله به عشق حضرت شاه</p></div>
<div class="m2"><p>خوش به ماهان نشسته همچون ماه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عارفانه به صدق می گوید</p></div>
<div class="m2"><p>دائما لا اله الا الله</p></div></div>