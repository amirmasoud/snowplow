---
title: >-
    دوبیتی شمارهٔ ۷۴
---
# دوبیتی شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>نزد ما خلت خلیل این است</p></div>
<div class="m2"><p>بخشش حضرت جمیل این است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حق تعالی خلیل خواند او را</p></div>
<div class="m2"><p>تو خلیلش بگو دلیل این است</p></div></div>