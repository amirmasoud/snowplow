---
title: >-
    دوبیتی شمارهٔ ۱۹۸
---
# دوبیتی شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>بگذر ز وجود و از عدم هم</p></div>
<div class="m2"><p>بگذار حدوث را قدم هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جمله هویت است دریاب</p></div>
<div class="m2"><p>اسم و صفت است و جام جم هم</p></div></div>