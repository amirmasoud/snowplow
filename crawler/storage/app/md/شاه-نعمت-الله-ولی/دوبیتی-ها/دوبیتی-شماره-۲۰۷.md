---
title: >-
    دوبیتی شمارهٔ ۲۰۷
---
# دوبیتی شمارهٔ ۲۰۷

<div class="b" id="bn1"><div class="m1"><p>بگذر ز وجود وز عدم هم</p></div>
<div class="m2"><p>بگذر ز حدوث وز قدم هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این جمله هویت است دریاب</p></div>
<div class="m2"><p>اسم و صفتست جام و جم هم</p></div></div>