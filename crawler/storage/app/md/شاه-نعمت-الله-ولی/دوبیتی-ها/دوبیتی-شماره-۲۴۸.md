---
title: >-
    دوبیتی شمارهٔ ۲۴۸
---
# دوبیتی شمارهٔ ۲۴۸

<div class="b" id="bn1"><div class="m1"><p>مظهر اسم اعظم است آن شاه</p></div>
<div class="m2"><p>به حقیقت یکی است عبداللّه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نعمت‌اللّه به صدق می‌گوید</p></div>
<div class="m2"><p>وحده لا اله الا اللّه</p></div></div>