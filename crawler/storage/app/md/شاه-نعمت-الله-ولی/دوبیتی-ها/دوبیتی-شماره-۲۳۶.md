---
title: >-
    دوبیتی شمارهٔ ۲۳۶
---
# دوبیتی شمارهٔ ۲۳۶

<div class="b" id="bn1"><div class="m1"><p>مصصفا فرمود بقوا او تقوا</p></div>
<div class="m2"><p>باش یک رنگ از دو رنگی فاتقوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جان و دل را دوست می‌ داری ولی</p></div>
<div class="m2"><p>لن تنالوا البر حتی تنفقوا</p></div></div>