---
title: >-
    دوبیتی شمارهٔ ۶۸
---
# دوبیتی شمارهٔ ۶۸

<div class="b" id="bn1"><div class="m1"><p>ای آنکه جزو لایتحزی دهان تست</p></div>
<div class="m2"><p>طولی که هیچ عرض ندارد میان تست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کردی به نطق نقطهٔ موهوم را دو نیم</p></div>
<div class="m2"><p>پس مبطل کلام حکیمان بیان تست</p></div></div>