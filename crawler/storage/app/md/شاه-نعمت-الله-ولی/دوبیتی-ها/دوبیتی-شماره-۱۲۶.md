---
title: >-
    دوبیتی شمارهٔ ۱۲۶
---
# دوبیتی شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>هر چه در غیب و در شهادت بود</p></div>
<div class="m2"><p>همه ایثار بندگان فرمود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسن اسما و هم جمال و صفات</p></div>
<div class="m2"><p>در چنین آینه به ما بنمود</p></div></div>