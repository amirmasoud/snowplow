---
title: >-
    دوبیتی شمارهٔ ۲۵
---
# دوبیتی شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>درهٔ بیضا ز بحر ما طلب</p></div>
<div class="m2"><p>آن چنان گوهر از این دریا طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عین ما جوئی به عین ما بجو</p></div>
<div class="m2"><p>طالب و مطلوب را از ما طلب</p></div></div>