---
title: >-
    دوبیتی شمارهٔ ۲۱۲
---
# دوبیتی شمارهٔ ۲۱۲

<div class="b" id="bn1"><div class="m1"><p>ای صبا گر روی به ترکستان</p></div>
<div class="m2"><p>دوستان را سلام ما برسان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به جان پیش آن عزیزانیم</p></div>
<div class="m2"><p>گرچه تن ساکنست در کرمان</p></div></div>