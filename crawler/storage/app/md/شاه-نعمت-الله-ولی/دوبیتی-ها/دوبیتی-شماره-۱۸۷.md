---
title: >-
    دوبیتی شمارهٔ ۱۸۷
---
# دوبیتی شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>عمل و علم هست کار خواص</p></div>
<div class="m2"><p>خوش بود نیز در عمل اخلاص</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ور نباشد چنین که ما گفتیم</p></div>
<div class="m2"><p>نتوان یافتم به علم خلاص</p></div></div>