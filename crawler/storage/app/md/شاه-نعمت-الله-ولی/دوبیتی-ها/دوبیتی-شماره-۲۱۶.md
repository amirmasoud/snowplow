---
title: >-
    دوبیتی شمارهٔ ۲۱۶
---
# دوبیتی شمارهٔ ۲۱۶

<div class="b" id="bn1"><div class="m1"><p>بگذر از خوف و رجا با ما نشین</p></div>
<div class="m2"><p>عاشقانه خوش درین دریا نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قصهٔ ماضی و مستقبل مگو</p></div>
<div class="m2"><p>حالیا با ما به حال ما نشین</p></div></div>