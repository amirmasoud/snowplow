---
title: >-
    دوبیتی شمارهٔ ۱۳۶
---
# دوبیتی شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>عاقلان گرچه بسی دُر سفته‌اند</p></div>
<div class="m2"><p>در همه بابی سخن‌ها گفته‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در سراشان همچنان خاشاک هست</p></div>
<div class="m2"><p>تا نپنداری که خانه رفته‌اند</p></div></div>