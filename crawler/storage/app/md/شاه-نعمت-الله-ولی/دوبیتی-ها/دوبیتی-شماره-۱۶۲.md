---
title: >-
    دوبیتی شمارهٔ ۱۶۲
---
# دوبیتی شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>آتش غیرتش برافروزد</p></div>
<div class="m2"><p>غیر خود را به یک نفس سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیس فی الدار غیره دیار</p></div>
<div class="m2"><p>این سخن را به ما بیاموزد</p></div></div>