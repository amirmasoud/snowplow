---
title: >-
    دوبیتی شمارهٔ ۱۲۰
---
# دوبیتی شمارهٔ ۱۲۰

<div class="b" id="bn1"><div class="m1"><p>ناظر و منظور آنجا کی بود</p></div>
<div class="m2"><p>بود و هم نابود آنجا کی بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هفت دریا غرقه اند در بحر او</p></div>
<div class="m2"><p>بلکه اسم و رسم و دریا کی بود</p></div></div>