---
title: >-
    دوبیتی شمارهٔ ۹۷
---
# دوبیتی شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>مطلوب خود است و طالب خود</p></div>
<div class="m2"><p>چه جای خیال نیک یا بد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>موجود غرض بگو کدام است</p></div>
<div class="m2"><p>غیری او را چگونه یابد</p></div></div>