---
title: >-
    دوبیتی شمارهٔ ۱۲۲
---
# دوبیتی شمارهٔ ۱۲۲

<div class="b" id="bn1"><div class="m1"><p>جمله آئینه یک حدید بود</p></div>
<div class="m2"><p>خواه عتیق است و خواه جدید است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آینه روشن است نزدیک آی</p></div>
<div class="m2"><p>کور ازین رمز ما بعید بود</p></div></div>