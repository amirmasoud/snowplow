---
title: >-
    دوبیتی شمارهٔ ۲۲۵
---
# دوبیتی شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>فقر بگزین و غنا ایثار کن</p></div>
<div class="m2"><p>اختیار خود فدای یار کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صوفیانه گر بیابی این خصال</p></div>
<div class="m2"><p>رو به صوفیخانه ای و کار کن</p></div></div>