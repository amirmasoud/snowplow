---
title: >-
    دوبیتی شمارهٔ ۱۷۸
---
# دوبیتی شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>در همه آئینه ای اسما نگر</p></div>
<div class="m2"><p>بلکه با اسما مسما می نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خو ش بیا با ما درین دریا در آ</p></div>
<div class="m2"><p>بحر را می بین و در دریا نگر</p></div></div>