---
title: >-
    دوبیتی شمارهٔ ۲۲۹
---
# دوبیتی شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>خوش بگو اللّه و اسم ذات بین</p></div>
<div class="m2"><p>معنیش در صورت و آیات بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمله مرآتند ها و هو و هی</p></div>
<div class="m2"><p>یک حقیقت در دو سه مرآت بین</p></div></div>