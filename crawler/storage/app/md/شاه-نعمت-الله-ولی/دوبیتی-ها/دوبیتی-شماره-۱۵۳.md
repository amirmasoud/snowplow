---
title: >-
    دوبیتی شمارهٔ ۱۵۳
---
# دوبیتی شمارهٔ ۱۵۳

<div class="b" id="bn1"><div class="m1"><p>بر علم قدر عظیم بود</p></div>
<div class="m2"><p>خوش بزرگی که او علیم بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حکم حاکم به قدر استعداد</p></div>
<div class="m2"><p>بود ار حاکم حکیم بود</p></div></div>