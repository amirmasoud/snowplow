---
title: >-
    دوبیتی شمارهٔ ۲۴۴
---
# دوبیتی شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>رهر و میر ما خلیل اللّه</p></div>
<div class="m2"><p>در همه راه و با همه همراه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جمع کن رهروان و خوش می گو</p></div>
<div class="m2"><p>وحده لا اله الا اللّه</p></div></div>