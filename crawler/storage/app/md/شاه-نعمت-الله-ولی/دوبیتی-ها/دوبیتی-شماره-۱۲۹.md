---
title: >-
    دوبیتی شمارهٔ ۱۲۹
---
# دوبیتی شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>در هزاران یکی چو بنماید</p></div>
<div class="m2"><p>در هزاران یکی پدید آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در همه آینه یکی بینی</p></div>
<div class="m2"><p>پرده از چشم تو چو بگشاید</p></div></div>