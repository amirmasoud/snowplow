---
title: >-
    دوبیتی شمارهٔ ۲۱
---
# دوبیتی شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>این بهشت از آشنای او طلب</p></div>
<div class="m2"><p>جنّت المأوا برای او طلب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زاهدانه گر همی جوئی بهشت</p></div>
<div class="m2"><p>بشنو از بهر رضای او طلب</p></div></div>