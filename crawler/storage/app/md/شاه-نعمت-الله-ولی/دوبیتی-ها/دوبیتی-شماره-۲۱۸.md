---
title: >-
    دوبیتی شمارهٔ ۲۱۸
---
# دوبیتی شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>خوش بگو الله و اسم ذات بین</p></div>
<div class="m2"><p>معنیش در صورت و آیات بین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جلمه مرآتند ها و هوی ما</p></div>
<div class="m2"><p>یک حقیقت در دو صد مرآت بین</p></div></div>