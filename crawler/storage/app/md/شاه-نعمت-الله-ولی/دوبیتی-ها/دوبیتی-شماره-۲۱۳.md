---
title: >-
    دوبیتی شمارهٔ ۲۱۳
---
# دوبیتی شمارهٔ ۲۱۳

<div class="b" id="bn1"><div class="m1"><p>از این عالم بدان عالم سفر کن</p></div>
<div class="m2"><p>از آن عالم به بالاتر نظر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو جسم و جان رها کردی و رفتی</p></div>
<div class="m2"><p>به نور او به عین او نظر کن</p></div></div>