---
title: >-
    دوبیتی شمارهٔ ۳
---
# دوبیتی شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>از صفات خود اگر یابی فنا</p></div>
<div class="m2"><p>حضرت باقی تو را بخشد بقا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جز صفات او نیابی در نظر</p></div>
<div class="m2"><p>گر ببینی نور چشم ما به ما</p></div></div>