---
title: >-
    دوبیتی شمارهٔ ۲۲۶
---
# دوبیتی شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>در صورت و معنیش نظر کن</p></div>
<div class="m2"><p>می بین همه و مرا خبر کن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خواهی که رسی به نعمت‌اللّه</p></div>
<div class="m2"><p>بر درگه سیّدم گذر کن</p></div></div>