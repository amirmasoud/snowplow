---
title: >-
    دوبیتی شمارهٔ ۵۰
---
# دوبیتی شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>حکم عدل نام آن شاه است</p></div>
<div class="m2"><p>باطناً شمس و ظاهراً ماه است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رند مست است زاهد هشیار</p></div>
<div class="m2"><p>بندهٔ بندگان درگاه است</p></div></div>