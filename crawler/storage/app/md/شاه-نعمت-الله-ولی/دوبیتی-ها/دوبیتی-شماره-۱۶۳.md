---
title: >-
    دوبیتی شمارهٔ ۱۶۳
---
# دوبیتی شمارهٔ ۱۶۳

<div class="b" id="bn1"><div class="m1"><p>در همه آئینهٔ اسما نگر</p></div>
<div class="m2"><p>بلکه با اسما مسمی را نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش بیا با ما درین دریا در آ</p></div>
<div class="m2"><p>بحر را می بین و در دریا نگر</p></div></div>