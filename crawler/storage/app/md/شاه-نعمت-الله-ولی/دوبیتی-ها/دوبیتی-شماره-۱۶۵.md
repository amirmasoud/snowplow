---
title: >-
    دوبیتی شمارهٔ ۱۶۵
---
# دوبیتی شمارهٔ ۱۶۵

<div class="b" id="bn1"><div class="m1"><p>عارفانه اول و آخر نگر</p></div>
<div class="m2"><p>هر چه بینی باطن و ظاهر نگر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این و آن با همدگر نیکو ببین</p></div>
<div class="m2"><p>از کرم هر بی خبر را کن خبر</p></div></div>