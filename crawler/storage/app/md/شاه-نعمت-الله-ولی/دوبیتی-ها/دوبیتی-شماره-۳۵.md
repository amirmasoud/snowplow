---
title: >-
    دوبیتی شمارهٔ ۳۵
---
# دوبیتی شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>همه عالم جمال حضرت اوست</p></div>
<div class="m2"><p>او جمیل و جمال دارد دوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم محب خود است و هم محبوب</p></div>
<div class="m2"><p>عشق و معشوق و عاشق نیکوست</p></div></div>