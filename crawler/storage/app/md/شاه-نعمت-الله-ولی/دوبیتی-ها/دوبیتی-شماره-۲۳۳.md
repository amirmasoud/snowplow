---
title: >-
    دوبیتی شمارهٔ ۲۳۳
---
# دوبیتی شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>دنیی دون دنی از دون مجو</p></div>
<div class="m2"><p>چون رها کن غیر آن بی چون مجو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشق عاقل را چو مجنون می‌ کند</p></div>
<div class="m2"><p>عاقلی از خدمت مجنون مجو</p></div></div>