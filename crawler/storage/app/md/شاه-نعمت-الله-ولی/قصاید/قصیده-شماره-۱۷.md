---
title: >-
    قصیدهٔ شمارهٔ ۱۷
---
# قصیدهٔ شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>داد جاروبی به دستم آن نگار</p></div>
<div class="m2"><p>گفت کز دریا برانگیزان غبار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب آتش گشت و جاروبم بسوخت</p></div>
<div class="m2"><p>گفت کز آتش تو جاروبی برآر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل جاروبت نگار آن پیر کار</p></div>
<div class="m2"><p>باطنت دریا و هستی چون غبار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آتش عشقش چو سوزد عقل را</p></div>
<div class="m2"><p>باز جاروبی ز عشق آید به کار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کردم از حیرت سجودی پیش او</p></div>
<div class="m2"><p>گفت بی ساجد سجودی خوش بیار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه بی ساجد سجودی چون بود</p></div>
<div class="m2"><p>گفت بی چون باشد و بیچاره یار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل لای نافیه می دان همی</p></div>
<div class="m2"><p>عشق اثبات حق است ای یار یار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سجده بی ساجد ندانی چون بود</p></div>
<div class="m2"><p>یعنی بی هستی ساجد سجده آر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گردنک را پیش کردم گفتمش</p></div>
<div class="m2"><p>ساجدی را سر ببر با ذوالفقار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>تیغ تا او بیش زد سر پیش شد</p></div>
<div class="m2"><p>تا برست از گردنم سر صد هزار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گردنم یعنی سر هستی بود</p></div>
<div class="m2"><p>تیغ تیز عشق باشد ذوالفقار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چون سر هستی ببرید از بدن</p></div>
<div class="m2"><p>معرفت شد آشکارا صد هزار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ای مزاجت سرد کو طاس دلت</p></div>
<div class="m2"><p>تا در این گرمابه تو گیری قرار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بگذر از گلخن تو در گرمابه رو</p></div>
<div class="m2"><p>جامه بر کن بنگر آن نقش و نگار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گر فسرده نیستی برخیز گرم</p></div>
<div class="m2"><p>ترک صورت کن بمعنی کن گذار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>طاس دل بر کن ز تن حمام تن</p></div>
<div class="m2"><p>سوی باغ جان خرام ای با وقار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا ببینی نقشهای دل ربا</p></div>
<div class="m2"><p>تا ببینی رنگهای لاله زار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>خاک و آب از عکس او رنگین شده</p></div>
<div class="m2"><p>جان بتازیده به ترک و زنگبار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>از حُجُب بیرون خرامد بی حجاب</p></div>
<div class="m2"><p>رونق گلزار و جان لاله زار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>لاله زار و نقشهای بی حساب</p></div>
<div class="m2"><p>از تجلی باشد ای صاحب وقار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چیست شرق و غرب اندر لامکان</p></div>
<div class="m2"><p>گلخن تاریک و حمامی نگار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شش جهت حمام و روزن لامکان</p></div>
<div class="m2"><p>بر سر روزن جمال شهریار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>خلوت دل لامکانست از یقین</p></div>
<div class="m2"><p>روزنش جانست و جانان شهریار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>گلخن تاریک نفس شوم تست</p></div>
<div class="m2"><p>چیست حمام این تن ناپایدار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>من چراغ هر سرم همچون فتیل</p></div>
<div class="m2"><p>جمله را اندر گرفته از شرار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>شمعها بر می شد از سرهای من</p></div>
<div class="m2"><p>شرق و مغرب را گرفته از قطار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>چون گذر کردی از این و آن به عشق</p></div>
<div class="m2"><p>جامه درپوش از صفاتش ذات وار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>باز چون همرنگ و بوی او شدی</p></div>
<div class="m2"><p>یار خود بینی نگار هر نگار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>شب گذشت و قصه ام کوته نشد</p></div>
<div class="m2"><p>ای شب و روز از حدیثش ذات وار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شاه شمس الدین تبریزی مرا</p></div>
<div class="m2"><p>مست می دارد ز جام بی خمار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>سید ملک وجودم لاجرم</p></div>
<div class="m2"><p>آنچه پنهان بود کردم آشکار</p></div></div>