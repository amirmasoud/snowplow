---
title: >-
    قصیدهٔ شمارهٔ ۳
---
# قصیدهٔ شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>از نور روی اوست که عالم منور است</p></div>
<div class="m2"><p>حسنی چنین لطیف چه حاجت به زیور است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان چار بالش و شش طاق و نه رواق</p></div>
<div class="m2"><p>بر درگه رفیع جلالش چو چاکر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زوج بتول باب امامین مرتضی</p></div>
<div class="m2"><p>سردار اولیا و وصی پیمبر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مسند نشین مجلس ملک ملائکه</p></div>
<div class="m2"><p>در آرزوی مرتبه و جای قنبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر ماه ، ماه نو به جهان مژده می‌دهد</p></div>
<div class="m2"><p>یعنی فلک ز حلقه به گوشان حیدر است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اسکندر است بنده او از میان جان</p></div>
<div class="m2"><p>چوبک زن درش بمثل صد چو قیصر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گیسو گشاد و گشت معطر دماغ روح</p></div>
<div class="m2"><p>رو را نمود و عالم از آن رو مصور است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>جودش وجود داد به عالم از آن سبب</p></div>
<div class="m2"><p>عالم به یمن جود و جودش منور است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خورشید لَمعه ایست ز نور ولایتش</p></div>
<div class="m2"><p>صد چشمه حیات و دو صد حوض کوثر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نزدیک ما خلیفهٔ بر حق امام ماست</p></div>
<div class="m2"><p>مجموع آسمان و زمینش مسخر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>مداح اهل بیت به نزدیک شرع و عقل</p></div>
<div class="m2"><p>دنیا و آخرت همه او را میسر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>لعنت به دشمنان علی گر کنی رواست</p></div>
<div class="m2"><p>می کن مگو که این سخنت بس مکرر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گوئی که خارجی بود از دین مصطفی</p></div>
<div class="m2"><p>خارج مگو که خارجی ، شوم کافر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>هر مؤمنی که لاف ولای علی زند</p></div>
<div class="m2"><p>توقیع آن جناب به نامش مقرر است</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>یا دست جود او چه بود کان مختصر</p></div>
<div class="m2"><p>با همتش محیط سرابی محقر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>او را بشر مخوان تو که سر خداست او</p></div>
<div class="m2"><p>او دیگر است و حالت او نیز دیگر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>طبع لطیف ماست که بحریست بیکران</p></div>
<div class="m2"><p>هر حرف از این سخن صدفی پر ز گوهر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>هر بیت از این قصیده که گفتم به عشق دل</p></div>
<div class="m2"><p>می خوان که هر یکی ز یکی خوب و خوشتر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>سید که دوستدار رسولست و آل او</p></div>
<div class="m2"><p>بر دشمنان دین محمد مظفر است</p></div></div>