---
title: >-
    قصیدهٔ شمارهٔ ۵
---
# قصیدهٔ شمارهٔ ۵

<div class="b" id="bn1"><div class="m1"><p>گر نه آب است اصل گوهر چیست</p></div>
<div class="m2"><p>جوهر گوهر منور چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم چو گوهری دریاب</p></div>
<div class="m2"><p>با تو گفتم بدان که گوهر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نقطه در دور دایره بنمود</p></div>
<div class="m2"><p>گرنه آب است این مدور چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خط فاصل میان ظلمت و نور</p></div>
<div class="m2"><p>جز وجود مضاف دیگر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نه می ساغر است و ساغر می</p></div>
<div class="m2"><p>در حقیقت بگو که ساغر چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نزد ما موج و بحر هر دو یکی است</p></div>
<div class="m2"><p>به جز از آب عین مظهر چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جام گیتی نماست یعنی دل</p></div>
<div class="m2"><p>به کف آور ببین که دلبر چیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عالمی از وجود موجودند</p></div>
<div class="m2"><p>کس نگوید وجود خود بر چیست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر یکی را هزار بشماری</p></div>
<div class="m2"><p>آن همه جز یکی مکرر چیست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گر بدانی حقیقت انسان</p></div>
<div class="m2"><p>باز یابی که صدر مصدر چیست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>نقش عالم خیال اوست ببین</p></div>
<div class="m2"><p>ورنه معنی این مصور چیست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>به مَثل گر نمود حق جوئی</p></div>
<div class="m2"><p>حلقهٔ سیم و خاتم زر چیست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>لوح محفوظ را روان می خوان</p></div>
<div class="m2"><p>تا بدانی که اصل دفتر چیست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گرنه آب و حیات معرفت است</p></div>
<div class="m2"><p>عین کوثر بگو که کوثر چیست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بزم عشقست و عاشقان سرمست</p></div>
<div class="m2"><p>به از این جنت ای برادر چیست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>گر نگوئی که مصطفی حقست</p></div>
<div class="m2"><p>بازوی ذوالفقار و حیدر چیست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>نعمت الله مظهر عشق است</p></div>
<div class="m2"><p>منکر او به غیر کافر چیست</p></div></div>