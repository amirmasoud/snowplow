---
title: >-
    قصیدهٔ شمارهٔ ۲۴
---
# قصیدهٔ شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>سالها در سفر به سر گشتیم</p></div>
<div class="m2"><p>عاشقانه به بحر و برگشتیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا ببینیم نور دیدهٔ خود</p></div>
<div class="m2"><p>پای تا سر همه نظر گشتیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرد بر گرد نقطهٔ وحدت</p></div>
<div class="m2"><p>همچو پرگار پی سپر گشتیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق و مست و لاابالی وار</p></div>
<div class="m2"><p>در پی دوست در به در گشتیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهر و باطن جهان دیدیم</p></div>
<div class="m2"><p>معنی خاص هر صُور گشتیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی خبر طالب همی بودیم</p></div>
<div class="m2"><p>تا که از خویش باخبر گشتیم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یار ما بود عین ما به یقین</p></div>
<div class="m2"><p>ما بدین معرفت سمر گشتیم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>او شکر بود و جان من چون گل</p></div>
<div class="m2"><p>ما به هم همچو گلشکر گشتیم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آفتاب جمال او دیدیم</p></div>
<div class="m2"><p>باز تابنده چون قمر گشتیم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>کُشتگان بلای غم بودیم</p></div>
<div class="m2"><p>زنده و شادمان دگر گشتیم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>پا نهادیم بر سر کونین</p></div>
<div class="m2"><p>در همه حال معتبر گشتیم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>غرقه اندر محیط عشق شدیم</p></div>
<div class="m2"><p>واصل مخزن گهر گشتیم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>نعمت الله را عیان دیدیم</p></div>
<div class="m2"><p>عین توحید را بصر گشتیم</p></div></div>