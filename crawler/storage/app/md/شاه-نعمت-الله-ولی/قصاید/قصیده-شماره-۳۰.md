---
title: >-
    قصیدهٔ شمارهٔ ۳۰
---
# قصیدهٔ شمارهٔ ۳۰

<div class="b" id="bn1"><div class="m1"><p>حبیبی سیدی یا ذالمعالی</p></div>
<div class="m2"><p>سوالله عند شمسی کالظلالی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیالی نقش بسته عالمش نام</p></div>
<div class="m2"><p>نمودی در خیالی آن جمالی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>و عینی ناظر من کل وجه</p></div>
<div class="m2"><p>و قلبی حاضر فی کل حالی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می صافست و خوش جامی مصفی</p></div>
<div class="m2"><p>فخذ منی القدح و اشرب زلالی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رایت الله فی مرآت کونی</p></div>
<div class="m2"><p>بعین الله هذا من کمالی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>و شمس الروح نور من ظهوری</p></div>
<div class="m2"><p>و بدر الکون عندی کالهلالی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سوی الله چیست ای صوفی صافی</p></div>
<div class="m2"><p>خیال فی خیال فی خیالی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وجودی جز وجود حق مطلق</p></div>
<div class="m2"><p>ظلال فی ظلال فی ظلالی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>غلام و بندگی سید ما</p></div>
<div class="m2"><p>کمال فی کمال فی کمالی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو سید نعمت الله رند و مستی</p></div>
<div class="m2"><p>محال فی محال فی محالی</p></div></div>