---
title: >-
    قصیدهٔ شمارهٔ ۲۰
---
# قصیدهٔ شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>دُرد دردش خورده ام تا صاف درمان یافتم</p></div>
<div class="m2"><p>دل ز جان برداشتم تا وصل جانان یافتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کار جمعی شد پریشان در هوای زلف او</p></div>
<div class="m2"><p>گرچه من جمعیت از زلف پریشان یافتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عارفانه آمدم از غیب و در غیبُ الغیوب</p></div>
<div class="m2"><p>جمع و تفصیل وجود خویشتن زان یافتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روح اعظم عقل او در درهٔ بیضا بود</p></div>
<div class="m2"><p>آدم معنی و هم لوح قضا زان یافتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مبدأ از غیر سبب مُبدع به قدرت آفرید</p></div>
<div class="m2"><p>جملهٔ ام الکتاب از لوحش آسان یافتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد از آن در مکتبُ الباعث از لوح قدر</p></div>
<div class="m2"><p>جمع فرقان خواندم و تفصیل قرآن یافتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عقل کل و نفس کلیه به هم آمیختند</p></div>
<div class="m2"><p>آدم و حوا و ذریات ایشان یافتم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>طبع من چون با طبیعت بعد ایشان میل کرد</p></div>
<div class="m2"><p>کارساز این و آن در مجلس جان یافتم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اسم ِ الباطن طبیعت را نگه دارد مدام</p></div>
<div class="m2"><p>لاجرم در جمله عالم یار یاران یافتم</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>برق منشور هیولا نقش بستم در خیال</p></div>
<div class="m2"><p>آن محل در صورت زیبای خوبان یافتم</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>اسم ِ الاخر در او مستور و او مستور از او</p></div>
<div class="m2"><p>یافتم عنقا ولی از خلق پنهان یافتم</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>عنبر و کافور با هم ساخته جسم خوشی</p></div>
<div class="m2"><p>اسم الظاهر در او با چار ارکان یافتم</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>آن حکیم این جسم را شکلی مدور داده است</p></div>
<div class="m2"><p>هر کجا شکلی بود شکلش به اینسان یافتم</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>باز دیدم حقه ای مانند گوئی زرنگار</p></div>
<div class="m2"><p>روز و شب در گرد همچون چرخ گردان یافتم</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نقطه و پرگار دیدم در سماع عارفان</p></div>
<div class="m2"><p>در میان استاده شیخ و خرقه رقصان یافتم</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>بی ستاره یک فلک دیدم که اطلس خوانده ام</p></div>
<div class="m2"><p>حاکمش اسم محیط است و بفرمان یافتم</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>یک فلک دیدم مرصع در نشیب او بر او</p></div>
<div class="m2"><p>یکهزار و بیست و دو کوکب درخشان یافتم</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>المحیط این عرش را بر فرق اشیاء داشته</p></div>
<div class="m2"><p>هر چه هست از جزو و کل در تحت اوزان یافتم</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مقتدر بر وی نشسته آن منازل یافته</p></div>
<div class="m2"><p>هم به مغرب هم به مشرق او خرامان یافتم</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>هفت بابا چار مادر با سه فرزند عزیز</p></div>
<div class="m2"><p>در کنار دایگان شادان و خندان یافتم</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>چرخ کیوان مسکن خاص خلیل الله بود</p></div>
<div class="m2"><p>رب تجلی کرده نور او به کیوان یافتم</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>بر جبین مشتری بنوشته اسم العلیم</p></div>
<div class="m2"><p>در سرا بستان او موسی بن عمران یافتم</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر فراز مسند بهرام هارون دیده ام</p></div>
<div class="m2"><p>اسم القاهر بخواندم قهر خاقان یافتم</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هست ادریس نبی بر چرخ چارم معتکف</p></div>
<div class="m2"><p>از جمال آستانش نور سبحان یافتم</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>یوسف مصری به دست زهره افتاده خوشی</p></div>
<div class="m2"><p>از مصور صورتی در ملک کنعان یافتم</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اسم المحصی ز دیوان عطارد خوانده ام</p></div>
<div class="m2"><p>عیسی مریم در آنجا میر دیوان یافتم</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>نور عالم دیده ام در آسمان این جهان</p></div>
<div class="m2"><p>روشن از اسم مبین چون ماه تابان یافتم</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>الشکور از کرسی حق خوانده ام بی اشتباه</p></div>
<div class="m2"><p>ارض جنت دیدم و انعام و احسان یافتم</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>اسم القابض ز آتش جوی و محیی از هوا</p></div>
<div class="m2"><p>تا بیابی همچو من زیرا کز ایشان یافتم</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>حی بجو از آب و باز آ زخاک اسمُ الممیت</p></div>
<div class="m2"><p>شش جهات این سرا از چار ارکان یافتم</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>در معادن خوش تجلی کرده اسم العزیز</p></div>
<div class="m2"><p>عزت هر خواجه ای از آن عزیزان یافتم</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اسم الرّزاق اگر خواهی طلب کن از نبات</p></div>
<div class="m2"><p>المذل در شأن مسکینان حیوان یافتم</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>جنیان را یافتم نازک ز اسم اللطیف</p></div>
<div class="m2"><p>بشنو از من این لطیفه کز لطیفان یافتم</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>القوی داده ملایک را وجود از جود خود</p></div>
<div class="m2"><p>از حضور این کریمان روح و ریحان یافتم</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>روشنست آئینهٔ گیتی نما در چشم ما</p></div>
<div class="m2"><p>اسم جامع صورت آن عین انسان یافتم</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>گرد عالم گشتم و کردم تفرج سر بسر</p></div>
<div class="m2"><p>رنج اگر بردم بسی گنج فراوان یافتم</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>از نبی و از ولی تا جان من دل زنده شد</p></div>
<div class="m2"><p>مَحرم آن حضرتم اسرار سلطان یافتم</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>باز از غربت به شهر خویشتن گشتم روان</p></div>
<div class="m2"><p>شهر خود را دیدم و نه این و نه آن یافتم</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>یادگار نعمت الله است نیکو یاددار</p></div>
<div class="m2"><p>زانکه من این مرتبه نیکو ز نیکان یافتم</p></div></div>