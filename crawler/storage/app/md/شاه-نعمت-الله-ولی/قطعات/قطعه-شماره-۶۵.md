---
title: >-
    قطعهٔ شمارهٔ ۶۵
---
# قطعهٔ شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>ما چو حلوایی و حلوا یار ماست</p></div>
<div class="m2"><p>صحن ما را پر ز حلوا کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مشکلات عالمی حل وا شده</p></div>
<div class="m2"><p>مشکل ما را چو حلوا کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای که گوئی ذره گردد آفتاب</p></div>
<div class="m2"><p>قطرهٔ ما بین که دریا کرده‌اند</p></div></div>