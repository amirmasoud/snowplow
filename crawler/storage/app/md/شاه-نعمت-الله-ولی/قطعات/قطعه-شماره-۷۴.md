---
title: >-
    قطعهٔ شمارهٔ ۷۴
---
# قطعهٔ شمارهٔ ۷۴

<div class="b" id="bn1"><div class="m1"><p>در حقیقت یکی عدد نبود</p></div>
<div class="m2"><p>گر شماری یکی هزار هزار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باطنش را نگر که جمله یکی است </p></div>
<div class="m2"><p>گر چه در ظاهر است این تکرار</p></div></div>