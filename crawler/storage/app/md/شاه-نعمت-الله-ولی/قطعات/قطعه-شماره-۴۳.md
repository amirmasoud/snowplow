---
title: >-
    قطعهٔ شمارهٔ ۴۳
---
# قطعهٔ شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>روی غیری ندیده دیدهٔ ما</p></div>
<div class="m2"><p>غیر چون نیست دیده چون بیند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لیس فی الدار غیره دیار</p></div>
<div class="m2"><p>چشم ما نور او به او بیند</p></div></div>