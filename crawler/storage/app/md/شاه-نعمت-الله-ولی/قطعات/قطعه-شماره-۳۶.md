---
title: >-
    قطعهٔ شمارهٔ ۳۶
---
# قطعهٔ شمارهٔ ۳۶

<div class="b" id="bn1"><div class="m1"><p>موئی به میان ما نگنجد</p></div>
<div class="m2"><p>سلطان چه بود گدا نگنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گوئی که بلای عشق آمد</p></div>
<div class="m2"><p>خوش باش که آن بلا نگنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دُردی کش کوی می فروشم</p></div>
<div class="m2"><p>درمان چه بود دوا نگنجد</p></div></div>