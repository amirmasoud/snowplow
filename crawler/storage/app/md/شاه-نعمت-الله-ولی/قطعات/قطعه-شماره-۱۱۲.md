---
title: >-
    قطعهٔ شمارهٔ ۱۱۲
---
# قطعهٔ شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>نیک و بد را به لطف خود بنواز</p></div>
<div class="m2"><p>آنگهی خوش بزی و خوش می رو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این نصیحت قبول اگر نکنی</p></div>
<div class="m2"><p>بگذر از این فقیر و خوش می رو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست ریش دنیی دون زن</p></div>
<div class="m2"><p>دم خر را بگیر و خوش می رو</p></div></div>