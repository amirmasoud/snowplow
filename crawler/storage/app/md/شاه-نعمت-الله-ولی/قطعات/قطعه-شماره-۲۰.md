---
title: >-
    قطعهٔ شمارهٔ ۲۰
---
# قطعهٔ شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>انس با محبوب اگر گیرد محبّ</p></div>
<div class="m2"><p>گر چه باشد یک نفس مطلوب اوست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دمی با یار خود همدم شود</p></div>
<div class="m2"><p>حاصل او زان نفس محبوب اوست</p></div></div>