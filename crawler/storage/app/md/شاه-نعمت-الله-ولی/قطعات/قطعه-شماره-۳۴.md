---
title: >-
    قطعهٔ شمارهٔ ۳۴
---
# قطعهٔ شمارهٔ ۳۴

<div class="b" id="bn1"><div class="m1"><p>هر چه بینی نعمت الله بود</p></div>
<div class="m2"><p>به از این خود حکایتی نبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ذوق ما را چو غایتی نبود</p></div>
<div class="m2"><p>بحر ما را نهایتی نبود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>که شنیده ولی سرمستی</p></div>
<div class="m2"><p>همچو او در ولایتی نبود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتهٔ عارفان به جان بشنو</p></div>
<div class="m2"><p>به از این خود حکایتی نبود</p></div></div>