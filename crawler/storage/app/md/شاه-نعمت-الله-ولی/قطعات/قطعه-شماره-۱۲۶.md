---
title: >-
    قطعهٔ شمارهٔ ۱۲۶
---
# قطعهٔ شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>گر زانکه ز اهل اعتباری</p></div>
<div class="m2"><p>بگذر ز رموز اعتباری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گیرم که حباب را بیابی</p></div>
<div class="m2"><p>جز آب بگو دگر چه داری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مستانه بیا و باده می نوش</p></div>
<div class="m2"><p>ای یار عزیز در خماری</p></div></div>