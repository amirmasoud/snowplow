---
title: >-
    قطعهٔ شمارهٔ ۸۵
---
# قطعهٔ شمارهٔ ۸۵

<div class="b" id="bn1"><div class="m1"><p>هر کجا محدثی بود بی ‌شک</p></div>
<div class="m2"><p>افتقارش بود به محدث خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک وجود است مظهر عالم </p></div>
<div class="m2"><p>مظهرش صد هزار باشد بیش</p></div></div>