---
title: >-
    قطعهٔ شمارهٔ ۸۸
---
# قطعهٔ شمارهٔ ۸۸

<div class="b" id="bn1"><div class="m1"><p>علماء رسوم می بینم</p></div>
<div class="m2"><p>همه را علم هست و نیست عمل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روز و شب عمر خویش صرف کنند</p></div>
<div class="m2"><p>در پی قال و قیل و بحث و جدل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همه تجهیل هم کنند تمام</p></div>
<div class="m2"><p>بله تکفیر یکدگر به مثل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عامیان عالمان چنان بینند</p></div>
<div class="m2"><p>لاجرم کار دین بود به خلل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمل و علم جمع کن با هم</p></div>
<div class="m2"><p>که چنین گفته اند اهل دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترک این لقمهٔ حرام بگو</p></div>
<div class="m2"><p>تا نیابی ملال را بی دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نعمت الله را به دست آور</p></div>
<div class="m2"><p>تا شوی پاک از جمیع علل</p></div></div>