---
title: >-
    قطعهٔ شمارهٔ ۱
---
# قطعهٔ شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>قرب صدسال عمر من بگذشت</p></div>
<div class="m2"><p>قصد موری نکرده ام به خدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نان خود خورده ام ز کسب حلال</p></div>
<div class="m2"><p>مال غیری نخورده ام به خدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات عشق رندانه</p></div>
<div class="m2"><p>روزگاری سپرده ام به خدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به خدا زنده ام به حق رسول</p></div>
<div class="m2"><p>گرچه از خویش مرده ام به خدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>موی هستی به تیغ سرمستی</p></div>
<div class="m2"><p>از سر خود سترده ام به خدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا عزیز خدا و خلق شدم</p></div>
<div class="m2"><p>ذاکرانه شمرده ام به خدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نفس خود به یاد سید خویش</p></div>
<div class="m2"><p>عزت کس نبرده ام به خدا</p></div></div>