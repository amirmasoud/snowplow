---
title: >-
    قطعهٔ شمارهٔ ۶۳
---
# قطعهٔ شمارهٔ ۶۳

<div class="b" id="bn1"><div class="m1"><p>بلبل گلستان معشوقم</p></div>
<div class="m2"><p>من ازین گلستان نخواهم شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر به ظاهر نهان شوم ز نظر </p></div>
<div class="m2"><p>از دل دوستان نخواهم شد</p></div></div>