---
title: >-
    قطعهٔ شمارهٔ ۱۰۳
---
# قطعهٔ شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>بر لب دریا چه می گردی نشین</p></div>
<div class="m2"><p>همچو ما با ما در این دریا نشین</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مجلس عشق است و ما مست خراب</p></div>
<div class="m2"><p>سر قدم ساز و بیا از پا نشین</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در خرابات مغان افتاده‌ایم</p></div>
<div class="m2"><p>عشق اگر داری بیا با ما نشین</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرد هر در می روی دیگر مرو</p></div>
<div class="m2"><p>بر در یکتای بی‌همتا نشین</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیز و بنشین زیردست عارفان</p></div>
<div class="m2"><p>آنگهی بر منصب بالا نشین</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دیدهٔ روشن اگر خواهی چو نور</p></div>
<div class="m2"><p>در نظر با مردم بینا نشین</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیمه از خانه به صحرا می‌زنیم</p></div>
<div class="m2"><p>وقت نوروز است و ما صحرانشین</p></div></div>