---
title: >-
    قطعهٔ شمارهٔ ۹۶
---
# قطعهٔ شمارهٔ ۹۶

<div class="b" id="bn1"><div class="m1"><p>شنیدم ساقی سرمست می گفت</p></div>
<div class="m2"><p>یکی را جام بخشم دیگری خم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر جام می آری پر بری می</p></div>
<div class="m2"><p>وگر انبان بیاری پر ز گندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگفتم این تفاوت از چه افتاد</p></div>
<div class="m2"><p>بگفتا این ز استعداد مردم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صراط مستقیم است اینکه گفتم</p></div>
<div class="m2"><p>طریق نعمت الله را مکن گم</p></div></div>