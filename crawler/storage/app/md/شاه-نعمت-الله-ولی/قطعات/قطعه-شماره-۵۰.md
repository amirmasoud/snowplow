---
title: >-
    قطعهٔ شمارهٔ ۵۰
---
# قطعهٔ شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>طرهٔ شب را مطرّا کرده اند</p></div>
<div class="m2"><p>نور روی روز پیدا کرده اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش در میخانه را بگشاده اند</p></div>
<div class="m2"><p>ساغری پر می به رندان داده اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در نظر نقش خیالی بسته اند</p></div>
<div class="m2"><p>با خیال خویش خوش پیوسته اند</p></div></div>