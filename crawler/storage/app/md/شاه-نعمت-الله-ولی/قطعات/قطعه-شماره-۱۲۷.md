---
title: >-
    قطعهٔ شمارهٔ ۱۲۷
---
# قطعهٔ شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>گر به خانه روی و در بندی</p></div>
<div class="m2"><p>به حقیقت بدان که دربندی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ملک شروان چه می کنی عارف</p></div>
<div class="m2"><p>بطلب پادشاه دربندی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همدانی طلب همی کردم</p></div>
<div class="m2"><p>یافتم آن عزیز الوندی</p></div></div>