---
title: >-
    قطعهٔ شمارهٔ ۱۰۱
---
# قطعهٔ شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>خطی کو را نه حسن است و نه ترتیب</p></div>
<div class="m2"><p>نه در اعراب او فتح است و نه ضم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه تفصیل او اجمال تحقیق</p></div>
<div class="m2"><p>همه توحید او تحقیق اعظم </p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی بر‌خواند این خط معما</p></div>
<div class="m2"><p>که در عالم نه خود بیند نه عالم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه آغارش شود مانع نه انجام</p></div>
<div class="m2"><p>نه ابلیسش حجاب آید نه آدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نه از کفرش بود اندیشه نه از دین</p></div>
<div class="m2"><p>نه اندیشه ز فردوس و جهنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>برای آفرینش باشدش سیر</p></div>
<div class="m2"><p>نه نامحرم بود با او نه محرم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مبرّا باشد از هر بود و نابود</p></div>
<div class="m2"><p>مجرد باشد از هر کم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو سید را مسلم نیست این درد</p></div>
<div class="m2"><p>ندانم تا که را باشد مسلم</p></div></div>