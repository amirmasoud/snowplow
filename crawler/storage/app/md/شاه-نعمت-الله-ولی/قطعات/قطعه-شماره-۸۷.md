---
title: >-
    قطعهٔ شمارهٔ ۸۷
---
# قطعهٔ شمارهٔ ۸۷

<div class="b" id="bn1"><div class="m1"><p>سوی اللّه چیست ای صوفی صافی</p></div>
<div class="m2"><p>نتوان یافت بی‌ وجود کمال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هست عالم همه خیال وجود </p></div>
<div class="m2"><p>وز تجلی اوست بود خیال</p></div></div>