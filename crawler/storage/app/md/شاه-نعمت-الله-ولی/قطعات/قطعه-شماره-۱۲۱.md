---
title: >-
    قطعهٔ شمارهٔ ۱۲۱
---
# قطعهٔ شمارهٔ ۱۲۱

<div class="b" id="bn1"><div class="m1"><p>به کرامات صوفیی در جنگ</p></div>
<div class="m2"><p>دستبردی نمود مردانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا کرامات بود یا که نبود</p></div>
<div class="m2"><p>به مثل چون خر است و ویرانه</p></div></div>