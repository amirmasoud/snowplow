---
title: >-
    قطعهٔ شمارهٔ ۸۴
---
# قطعهٔ شمارهٔ ۸۴

<div class="b" id="bn1"><div class="m1"><p>همه عالم چو سایه سجده کنان</p></div>
<div class="m2"><p>اوفتاده به خاک درگاهش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه منقاد امر او باشند</p></div>
<div class="m2"><p>هر که باشد گدا و هم شاهش</p></div></div>