---
title: >-
    قطعهٔ شمارهٔ ۶۴
---
# قطعهٔ شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>ساقی باید که می ببخشد</p></div>
<div class="m2"><p>رندی باید که می بنوشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تشریف شریف می دهد شاه </p></div>
<div class="m2"><p>عبدی باید که آن بپوشد</p></div></div>