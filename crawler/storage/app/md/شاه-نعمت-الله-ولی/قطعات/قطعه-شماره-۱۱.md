---
title: >-
    قطعهٔ شمارهٔ ۱۱
---
# قطعهٔ شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>ای جان پدر به حال ما رحمی کن</p></div>
<div class="m2"><p>زیرا بی تو تمتعی از جان نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسیار فراق تو کشیدم اما</p></div>
<div class="m2"><p>زین بیش مرا تحمل هجران نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ملک و ملکوت تخت سلطانی ماست</p></div>
<div class="m2"><p>مخصوص به شهر یزد یا کرمان نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بگذر ز خرابهٔ جهان جان پدر</p></div>
<div class="m2"><p>آن گیر که این جهان همه ویران نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>برخیز و بیا که دنیی و عقبی هم</p></div>
<div class="m2"><p>با همت دوست قیمتش چندان نیست</p></div></div>