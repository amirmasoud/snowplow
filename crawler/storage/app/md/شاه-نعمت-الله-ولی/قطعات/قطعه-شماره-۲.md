---
title: >-
    قطعهٔ شمارهٔ ۲
---
# قطعهٔ شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>چون مرا درخواب کردی روز و شب</p></div>
<div class="m2"><p>روز و شب درخواب می بینم تو را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روی تو ماهست و چشم من پر آب</p></div>
<div class="m2"><p>روز و شب در آب می بینم تو را</p></div></div>