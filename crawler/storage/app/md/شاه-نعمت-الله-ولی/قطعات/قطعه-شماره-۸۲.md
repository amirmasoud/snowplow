---
title: >-
    قطعهٔ شمارهٔ ۸۲
---
# قطعهٔ شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>بنمود جمال او به خوابم</p></div>
<div class="m2"><p>گفتم باشد مگر جمالش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیدار شدم ز خواب مستی</p></div>
<div class="m2"><p>نه نقش بماند نه خیالش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه من ماندم نه غیر او هم</p></div>
<div class="m2"><p>او ماند و کمال پر کمالش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ما اثری نماند با ما</p></div>
<div class="m2"><p>با او نبود کسی مجالش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دریاب به ذوق نعمت الله</p></div>
<div class="m2"><p>این دولت و مال لایزالش</p></div></div>