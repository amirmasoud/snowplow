---
title: >-
    قطعهٔ شمارهٔ ۱۲۳
---
# قطعهٔ شمارهٔ ۱۲۳

<div class="b" id="bn1"><div class="m1"><p>لشکر پادشه بسی باشد</p></div>
<div class="m2"><p>شاه جانیبکی است تا دانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اختلاف صور فراوان است</p></div>
<div class="m2"><p>ور نه معنی یکی است تا دانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر کسی را شکی بود به خدا </p></div>
<div class="m2"><p>سیدم بی‌شک است تا دانی</p></div></div>