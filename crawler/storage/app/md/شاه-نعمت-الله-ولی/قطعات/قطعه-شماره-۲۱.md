---
title: >-
    قطعهٔ شمارهٔ ۲۱
---
# قطعهٔ شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>در راه خدا پای برهنه گو برو</p></div>
<div class="m2"><p>آن یار که همچو بشر حافی اهل است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر سر به ره است پا برهنه غم نیست </p></div>
<div class="m2"><p>ور نیست به ره سر برهنه سهل است</p></div></div>