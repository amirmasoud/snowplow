---
title: >-
    قطعهٔ شمارهٔ ۸۰
---
# قطعهٔ شمارهٔ ۸۰

<div class="b" id="bn1"><div class="m1"><p>بار او می کش و خوشی می رو</p></div>
<div class="m2"><p>ناز او می کش و خوشی می ناز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همه عالم به زیر بال آری</p></div>
<div class="m2"><p>مرغ همت اگر کند پرواز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می ما مستی دگر دارد</p></div>
<div class="m2"><p>خوش بود گر به ما شوی دمساز</p></div></div>