---
title: >-
    قطعهٔ شمارهٔ ۴۹
---
# قطعهٔ شمارهٔ ۴۹

<div class="b" id="bn1"><div class="m1"><p>پیش ازین گر مرا حجابی بود</p></div>
<div class="m2"><p>شکر گویم که آن حجاب نماند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود گنجی درین خرابهٔ تن</p></div>
<div class="m2"><p>گنج باقیست گر خراب نماند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آفتابی ز چشم پنهان شد</p></div>
<div class="m2"><p>تا نگوئی که آفتاب نماند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میکده باقی است و خم پر می</p></div>
<div class="m2"><p>جام بشکست نه شراب نماند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی حسابم نواخت لطف خدا</p></div>
<div class="m2"><p>هیچ باقی درین حساب نماند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نعمت الله به خواب رفت دمی</p></div>
<div class="m2"><p>باز بیدار شد چه خواب نماند</p></div></div>