---
title: >-
    قطعهٔ شمارهٔ ۷
---
# قطعهٔ شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>دوش تا روز ما به هم بودیم</p></div>
<div class="m2"><p>لذتی یافتم که چه توان گفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بندگی خدای خود کردم</p></div>
<div class="m2"><p>حرمتی یافتم که چه توان گفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دست و پایش خوشی ببوسیدم</p></div>
<div class="m2"><p>حضرتی یافتم که چه توان گفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رحمتی کرد بر من مسکین</p></div>
<div class="m2"><p>رحمتی یافتم که چه توان گفت</p></div></div>