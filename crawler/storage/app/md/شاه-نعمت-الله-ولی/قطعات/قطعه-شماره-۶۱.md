---
title: >-
    قطعهٔ شمارهٔ ۶۱
---
# قطعهٔ شمارهٔ ۶۱

<div class="b" id="bn1"><div class="m1"><p>هر که او نقص دیگری گوید</p></div>
<div class="m2"><p>شک ندارم که نقص او باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نقص مردم مگو که نیکو نیست</p></div>
<div class="m2"><p>نقص آدم کجا نکو باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر سراپای او فرو باشی</p></div>
<div class="m2"><p>لطف او بر سرت فرو باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ور محب لقای او باشی</p></div>
<div class="m2"><p>او محب لقای تو باشد</p></div></div>