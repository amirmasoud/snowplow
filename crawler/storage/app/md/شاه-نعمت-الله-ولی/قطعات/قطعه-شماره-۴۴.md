---
title: >-
    قطعهٔ شمارهٔ ۴۴
---
# قطعهٔ شمارهٔ ۴۴

<div class="b" id="bn1"><div class="m1"><p>پیوسته شکسته باش چون ما</p></div>
<div class="m2"><p>کو کار شکستگان برآرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مائیم و دل شکسته چون یار</p></div>
<div class="m2"><p>پیوسته شکسته دوست دارد</p></div></div>