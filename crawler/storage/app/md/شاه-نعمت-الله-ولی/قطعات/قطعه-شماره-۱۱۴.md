---
title: >-
    قطعهٔ شمارهٔ ۱۱۴
---
# قطعهٔ شمارهٔ ۱۱۴

<div class="b" id="bn1"><div class="m1"><p>منم که همت من جز خدا نمی جوید</p></div>
<div class="m2"><p>خوش است همت عالی که باد پاینده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هزار مطرب عشاق را نوا سازم</p></div>
<div class="m2"><p>چو ساز ما بنوازد به لطف سازنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به هر طرف که نظر می کنم به دیدهٔ خود</p></div>
<div class="m2"><p>هزار آینه بینم یکی نماینده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو راست گوشهٔ عقل و مراست خلوت عشق</p></div>
<div class="m2"><p>توراست خطهٔ دارا مراست دارنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غلام سیدم و پادشاه هر دوجهان</p></div>
<div class="m2"><p>عجب مدار که سلطان مرا بود بنده</p></div></div>