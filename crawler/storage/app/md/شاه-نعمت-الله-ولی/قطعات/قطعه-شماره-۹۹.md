---
title: >-
    قطعهٔ شمارهٔ ۹۹
---
# قطعهٔ شمارهٔ ۹۹

<div class="b" id="bn1"><div class="m1"><p>نعمت‌اللّهم وز آل رسول</p></div>
<div class="m2"><p>حد کس نیست دانش حدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسبت شعر و شاعری بر من</p></div>
<div class="m2"><p>همچو ابجد بود بر جدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می خورم جام می ز کد یمین</p></div>
<div class="m2"><p>خوش حلال است حاصل کدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو بحر محیط در جوشم</p></div>
<div class="m2"><p>گاه در جزر و گاه در مدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاکر شکر نعمت اللّهم</p></div>
<div class="m2"><p>تانفس باقی است در شدّم</p></div></div>