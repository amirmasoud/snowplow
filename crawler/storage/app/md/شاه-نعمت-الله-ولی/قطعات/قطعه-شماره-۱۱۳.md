---
title: >-
    قطعهٔ شمارهٔ ۱۱۳
---
# قطعهٔ شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>منم که همت من جز خدا نمی جوید</p></div>
<div class="m2"><p>خوشست همت عالی که باد پاینده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا به سایهٔ طوبی چه التفات بود</p></div>
<div class="m2"><p>که هست سایهٔ من آفتاب تابنده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو راست دنیی وعقبی مراست حضرت او</p></div>
<div class="m2"><p>تو راست خطهٔ دارا مراست دارنده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به نور طلعت او روشنست دیدهٔ ما</p></div>
<div class="m2"><p>چه جای روشنی آفتاب تابنده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به روی او در میخانه را گشادم باز</p></div>
<div class="m2"><p>ببین تو مرحمت حضرت گشاینده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز روی خود به کرم ساز بینوا بنواخت</p></div>
<div class="m2"><p>بیا و گوش کن آواز آن نوازنده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر یکی به هزار آینه نماید رو</p></div>
<div class="m2"><p>هزار رو بنماید یکی نماینده</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مرو که شاه جهانی مرا غلام بود</p></div>
<div class="m2"><p>از آنکه سید خود را به جان شدم بنده</p></div></div>