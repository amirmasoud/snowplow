---
title: >-
    قطعهٔ شمارهٔ ۹۷
---
# قطعهٔ شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>پیدا شده در عالم آن نور جمال او</p></div>
<div class="m2"><p>آن نور جمال او پیدا شده در عالم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پیدا شده در آدم ذات و صفتش با هم</p></div>
<div class="m2"><p>ذات و صفتش با هم پیدا شده در آدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک جرعه ز جام جم خوشتر بود از صد جان</p></div>
<div class="m2"><p>خوشتر بود از صد جان یک جرعه ز جام جم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از هر دو جهان بی غم مائیم به عشق او</p></div>
<div class="m2"><p>مائیم به عشق او از هر دو جهان بی غم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را نبود ماتم گر دل برد و ور جان</p></div>
<div class="m2"><p>گر دل برود ور جان ما را نبود ماتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با جام میم همدم در گوشهٔ میخانه</p></div>
<div class="m2"><p>در گوشهٔ میخانه با جام میم همدم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذر تو ز پیش و کم فانی شو و باقی شو</p></div>
<div class="m2"><p>فانی شو و باقی شو بگذر تو ز پیش و کم</p></div></div>