---
title: >-
    قطعهٔ شمارهٔ ۱۱۸
---
# قطعهٔ شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>در روزه و در زکوة و در حج</p></div>
<div class="m2"><p>اسرار بسی بود نهفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اما سری که در نماز است</p></div>
<div class="m2"><p>سرّی است که با تو کس نگفته</p></div></div>