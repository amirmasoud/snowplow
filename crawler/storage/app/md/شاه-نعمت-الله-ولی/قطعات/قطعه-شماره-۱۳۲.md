---
title: >-
    قطعهٔ شمارهٔ ۱۳۲
---
# قطعهٔ شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>گیرم که حباب را بیابی</p></div>
<div class="m2"><p>جز آب بگو دگر چه داری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مستانه بیا و باده می نوش</p></div>
<div class="m2"><p>ای یار عزیز در خماری</p></div></div>