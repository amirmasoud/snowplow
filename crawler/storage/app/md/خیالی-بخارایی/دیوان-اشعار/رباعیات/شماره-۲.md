---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای دوست کسی که عشق در سر دارد</p></div>
<div class="m2"><p>دایم دل غمدیده منوّر دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسودهٔ هر دو عالم آمد به یقین</p></div>
<div class="m2"><p>در لنگر عشق هرکه لنگر دارد</p></div></div>