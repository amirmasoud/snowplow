---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>در مطبخ دنیا تو همه دود خوری</p></div>
<div class="m2"><p>تا کی تو غمان بود و نابود خوری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از مایه نخواهی که جوی کم گردد</p></div>
<div class="m2"><p>مایه که خورد چون تو همه سود خوری</p></div></div>