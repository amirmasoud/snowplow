---
title: >-
    شمارهٔ ۱
---
# شمارهٔ ۱

<div class="b" id="bn1"><div class="m1"><p>ای حرم عزّتت ملکت بی منتها</p></div>
<div class="m2"><p>نقش دو عالم زده بر علم کبریا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سپهت رومی صبح ملمّع لباس</p></div>
<div class="m2"><p>وز حشمت زنگی شام مرصّع وطا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آینهٔ صنع تو مهر به دست صباح</p></div>
<div class="m2"><p>مشعلهٔ مهر تو ماه به فرق مسا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عالم قدر تو را مجمره سوز آفتاب</p></div>
<div class="m2"><p>گلشن لطف تو را مروحه گردان صبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نامزد نام تو فاختهٔ طوقدار</p></div>
<div class="m2"><p>عاشق گلبانگ تو بلبل دستان سرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آمده زامر تو آب جانب بستان به سر</p></div>
<div class="m2"><p>رفته به حکم تو سرو سوی چمن راست پا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از طرف مرتبه سدّهٔ صنعت رفیع</p></div>
<div class="m2"><p>وز جهت منزلت سدرهٔ تو منتها</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قهر تو بهرام را کرده حوالت به تیغ</p></div>
<div class="m2"><p>لطف تو ناهید را داده نوید نوا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نامهٔ تذکیر تو طوق وحوش و طیور</p></div>
<div class="m2"><p>سکّهٔ تسبیح تو بر دل سنگ و گیا</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شوق تو دارد مراد بحر به گاه خروش</p></div>
<div class="m2"><p>حمد تو دارد غرض کوه به وقت صدا</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>ناطق و ناهق به صدق علوی و سفلی به ذوق</p></div>
<div class="m2"><p>جمله به تسبیح تو قایل ربّ العلا</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بارکشان تو را همدم جان نا صبور</p></div>
<div class="m2"><p>عذرخواهان تو را ورد زبان ربّنا</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>با روش لطف تو عذر سقیمان قبول</p></div>
<div class="m2"><p>با صفت قهر تو سعی مطیعان هبا</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>از نظرت یافتند مرتبهٔ چشم و گوش</p></div>
<div class="m2"><p>نرگس زرّین کلاه، وَرد زمرّد قبا</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>سرو سرافراز را در چمن عزّتت</p></div>
<div class="m2"><p>پای ز حیرت به گل مانده و سر در هوا</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>صبح بگه خیز گشت؟ حاجب درگاه تو</p></div>
<div class="m2"><p>رایت صدقش ز پیش خنجر قهر از قفا</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>تا کشش تو نشد رهبر اشیا، نکرد</p></div>
<div class="m2"><p>در پر کاهی اثر جاذبهٔ کهربا</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نافه چرا دم به دم خون جگر می خورد</p></div>
<div class="m2"><p>در طلبت گر نزد دم به طریق خطا</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>گر به قوی طالعان خشم کنی و عتاب</p></div>
<div class="m2"><p>ور به فرمایگان لطف کنی و عطا</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تارک گردون شود همچو زمین پی سپر</p></div>
<div class="m2"><p>تا به ثریّا رسد پایهٔ تحت الثرا</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>گه به گدایان دهی منصب شاهنشهی</p></div>
<div class="m2"><p>گه به سیاست کنی تاجوران را گدا</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>پادشه مطلقی هرچه بخواهی بکن</p></div>
<div class="m2"><p>نیست کسی را به تو زهرهٔ چون و چرا</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>تا چو قلم سر نهاد بر خط امر تو عقل</p></div>
<div class="m2"><p>عشق قلم درکشید بر صحف ماسوا</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ثابت جاوید گشت بر در الاّ هوَ</p></div>
<div class="m2"><p>چون ز پی نفی غیر، بست میان حرف لا</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>تا دهد از نیش نحل حکمت تو نوش جان</p></div>
<div class="m2"><p>کرده به قانون در او تعبیه ذوق شفا</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کرده ز آب خضر شربت ماءالحیات</p></div>
<div class="m2"><p>داده به چوب کلیم خاصیت اژدها</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از تو عصای کلیم منصب ثعبان گرفت</p></div>
<div class="m2"><p>ورنه چو ثعبان هزار هست در این ره عصا</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>کی فلک از آفتاب تاج شرف یافتی</p></div>
<div class="m2"><p>گر ز پی خدمتت پشت نکردی دوتا</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آینه گردان توست خسرو چارم سریر</p></div>
<div class="m2"><p>بندهٔ فرمان توست خواجهٔ هر دو سرا</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>احمد محمود نام امیّ صادق کلام</p></div>
<div class="m2"><p>منشق ماهِ تمام قطب امم مصطفا</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>طایر عرش آشیان واسطهٔ کن فکان</p></div>
<div class="m2"><p>خسرو اقلیم جان پادشه انبیا</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>اکمل ارباب فخر واقف اسرار فقر</p></div>
<div class="m2"><p>محرم آیات حق هادی دین هدا</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قامت او سروناز در چمن فاَستَقِم</p></div>
<div class="m2"><p>چهرهٔ او آفتاب بر فلک والضحّا</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>شعشعهٔ رأی او مشعلهٔ آفتاب</p></div>
<div class="m2"><p>شاهد تعدیل او راستی استوا</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>بر کف دستش حجر یافته تشریف نطق</p></div>
<div class="m2"><p>وز سر صدقش غزال گفته درود و ثنا</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>منزل او در سفر ذُروهٔ معراج قدس</p></div>
<div class="m2"><p>محفل او شام وصل خلوت خاص خدا</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در شب قرب از حرم تا به حریم وصال</p></div>
<div class="m2"><p>رفته به پای براق برق صفت جابه جا</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>بر ورق جان هنوز نقش محبّت نبود</p></div>
<div class="m2"><p>کاو ز ره دلبری بود حبیب خدا</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خطّهٔ افضال را پیشتر از جمله شاه</p></div>
<div class="m2"><p>زمرهٔ اسلام را بعد نبی پیشوا</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>یارِ سپهدار دین خواجهٔ قنبر، علی</p></div>
<div class="m2"><p>شحنهٔ ملک نجف ساقی روز جزا</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>باغ جنان رخش از طرف دلبری</p></div>
<div class="m2"><p>سرو روان قدش از چمن لافتا</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>باب حسین و حسن ابن ابی طالب آنک</p></div>
<div class="m2"><p>مقتل اولاد اوست بادیهٔ کربلا</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>باد به مدح علی رُفته ز مرآت عمر</p></div>
<div class="m2"><p>گرد هوا و هوس، زنگ نفاق و ریا</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>یارب از آنجا که هست در حق اهل گنه</p></div>
<div class="m2"><p>رحمت تو بی قیاس لطف تو بی انتها</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>کز سر جهل آنچه کرد بنده خیالی تو</p></div>
<div class="m2"><p>تو به کرم درگذر چون به تو کرد التجا</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>از طرف تو همه مرحمت است و کرم</p></div>
<div class="m2"><p>وز جهت ما همه زلّت و جرم و خطا</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>بهر شرف کرده ایم نسبت مدحت به خویش</p></div>
<div class="m2"><p>ورنه کجا مدح تو فکرت انسان کجا</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>گرچه به جز آب چشم نیست مرا عذرخواه</p></div>
<div class="m2"><p>هست امیدی که تو رد نکنی عذر ما</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>تا که قبول اوفتد دعوت من، کرده ام</p></div>
<div class="m2"><p>عرض دعا در سخن ختم سخن بر دعا</p></div></div>