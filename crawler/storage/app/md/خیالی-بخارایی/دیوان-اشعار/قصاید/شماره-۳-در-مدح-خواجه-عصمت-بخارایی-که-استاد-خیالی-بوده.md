---
title: >-
    شمارهٔ ۳ - در مدح خواجه عصمت بخارایی که استاد خیالی بوده
---
# شمارهٔ ۳ - در مدح خواجه عصمت بخارایی که استاد خیالی بوده

<div class="b" id="bn1"><div class="m1"><p>در این سراچهٔ فانی که منزل خطر است</p></div>
<div class="m2"><p>به عیش کوش که دوران عمر در گذر است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر آر پیش ز مستی سر از شراب غرور</p></div>
<div class="m2"><p>چرا که حاصل کار خمار دردسر است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اگر چه جرعهٔ جام جهان فرح بخش است</p></div>
<div class="m2"><p>منوش و نیک حذر کن که نیش بر اثر است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که نیست به بوی شرابِ شوق مدام</p></div>
<div class="m2"><p>ز خویش بی خبر، از کار خویش بی خبر است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز سالکان طریقت در این رباط کهن</p></div>
<div class="m2"><p>که چار رکن و نُه ایوان و شش ره و دو در است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به چشم سر ز بد و نیک هر که را دیدم</p></div>
<div class="m2"><p>مقیم ناشده در سر عزیمت سفر است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>به باغ دهر سبب بیوفایی عمر است</p></div>
<div class="m2"><p>که لاله غرقهٔ خون است و مرغ نوحه گر است</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>اگرچه هیچ ندارم به غیر گوهر اشک</p></div>
<div class="m2"><p>به روی دوست که آن نیز جمله در نظر است</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>مرا چو لاله ز گلزار دهر رنگی نیست</p></div>
<div class="m2"><p>جز اینکه ز آتش اندیشه داغ بر جگر است</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>چو اعتبار ندارد متاع دنیی دون</p></div>
<div class="m2"><p>به قول عقل و همه قول عشق معتبر است</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>من و ملازمت آستان مخدومی</p></div>
<div class="m2"><p>که سجده گاه عقول است و قبلهٔ هنر است</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سپهر فضل و هنر آنکه از ره معنی</p></div>
<div class="m2"><p>فرشته پی ست اگرچه به صورت بشر است</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>ستوده یی که به اوصاف او از این مطلع</p></div>
<div class="m2"><p>چو کام نی دهن اهل عقل پر شکر است</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>«بیا که عید رسید و چو عمر برگذر است</p></div>
<div class="m2"><p>ز چهره پرده برافکن که عشق پرده در است»</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>مرا لقای تو خوشتر هزار بار ز عید</p></div>
<div class="m2"><p>که عید دیگر و ذوق لقای تو دگر است</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز شوق خندهٔ یاقوت گوهر افشانت</p></div>
<div class="m2"><p>مرا چو جیب صدف دیده مخزن گهر است</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>به پیش شرح جمالت فسانهٔ یوسف</p></div>
<div class="m2"><p>قسم به لطف دهانت که نیک مختصر است</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چه جای قصّهٔ یوسف که در جهان حسنت</p></div>
<div class="m2"><p>بسان مردمی خواجه در جهان سمر است</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>جهان لطف و کرم خواجه عصمت الله آنک</p></div>
<div class="m2"><p>دو کون بر سر خوان عطاش ماحضر است</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>ایا کلیم کلامی که حسن منطق تو</p></div>
<div class="m2"><p>عقول را سوی طور کمال راهبر است</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>اگرچه تو ز مقیمان منزل خویشی</p></div>
<div class="m2"><p>ولیک شعر تو از سالکان بحر و بر است</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>ز صیت حسن کلامت خبر کسی را نیست</p></div>
<div class="m2"><p>که همچو نرگس و گل چشم و گوش کور و کراست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>بر آسمان فضایل دل تو آن بدر است</p></div>
<div class="m2"><p>که پرتوی ز طلوعش طلیعهٔ سحر است</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>درون خلوت معنی ضمیر تو شمعی ست</p></div>
<div class="m2"><p>چنانکه قصر فلک را چراغ شب قمر است</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ز شمع رای تو پروانه یی ست روشن دل</p></div>
<div class="m2"><p>چراغ روح که خورشید عالم صوَر است</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>اگر نه راندهٔ درگاه توست خسرو مهر</p></div>
<div class="m2"><p>چو سایلان ز چه رو کو به کو و دربدر است</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>به سان غمزهٔ خونریز یار پیوسته</p></div>
<div class="m2"><p>عدوی جاه تو در عین فتنه و خطر است</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>همای همّت تو طایریست عالیقدر</p></div>
<div class="m2"><p>کش آسمان مدوّر چو بیضه زیر پر است</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>سخنورا چو تویی آنکه اهل دانش را</p></div>
<div class="m2"><p>ز کیمیای قبول تو خاک همچو زر است</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>اگرچه نظم خیالی متاع بی قدریست</p></div>
<div class="m2"><p>قبول کن که ز لطفت مرادم اینقدر است</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>هماره تا که ز طوبی ست زینت فردوس</p></div>
<div class="m2"><p>چنانکه باغ جهان را طراوت از شجر است</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>به باغ دهر سرافراز باد پیوسته</p></div>
<div class="m2"><p>نهال عمر تو کش لطف و مردمی ثمر است</p></div></div>