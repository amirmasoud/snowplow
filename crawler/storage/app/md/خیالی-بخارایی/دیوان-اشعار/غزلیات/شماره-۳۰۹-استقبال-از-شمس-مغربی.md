---
title: >-
    شمارهٔ ۳۰۹ - استقبال از شمس مغربی
---
# شمارهٔ ۳۰۹ - استقبال از شمس مغربی

<div class="b" id="bn1"><div class="m1"><p>من که از دیدهٔ معنی به رخت می‌نگرم</p></div>
<div class="m2"><p>چشم دارم که نرانی چو سرشک از نظرم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه کز حسرت مهر رخ تو می ترسم</p></div>
<div class="m2"><p>که بسوزد عَلَم صبح ز آه سحرم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اثری بیش نماند از من خاکی دریاب</p></div>
<div class="m2"><p>ورنه بسیار بجویی و نیابی اثرم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل از بادهٔ لعلش مطلب کام که من</p></div>
<div class="m2"><p>تا از این می خبری یافته ام بی خبرم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا نگویند دگر کرد خیالی از یار</p></div>
<div class="m2"><p>نیست جز فکر خیال تو خیالی دگرم</p></div></div>