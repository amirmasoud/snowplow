---
title: >-
    شمارهٔ ۳۹
---
# شمارهٔ ۳۹

<div class="b" id="bn1"><div class="m1"><p>به اهل درد غمت هرچه می کند غم نیست</p></div>
<div class="m2"><p>چرا که هیچ دلی بی غم تو خرّم نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از آن به کعبهٔ وصل تو ره ندارد جان</p></div>
<div class="m2"><p>که غیر در حرم خاص دوست محرم نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اساس عهد و وفا با تو محکم است مرا</p></div>
<div class="m2"><p>ولی چه سود که بنیاد عمر محکم نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دلم ز بادهٔ شوقت فتاده مست و خراب</p></div>
<div class="m2"><p>به عالمی ست که هیچش خبر ز عالم نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجاست غالیه مویی که چون بنفشه ز شرم</p></div>
<div class="m2"><p>به دور زلف تو آشفته حال و در هم نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو لاله داغ منه بر دل خیالی بیش</p></div>
<div class="m2"><p>کز این متاع ز سوز غم تواش کم نیست</p></div></div>