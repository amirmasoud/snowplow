---
title: >-
    شمارهٔ ۳۳۴ - استقبال از خواجوی کرمانی
---
# شمارهٔ ۳۳۴ - استقبال از خواجوی کرمانی

<div class="b" id="bn1"><div class="m1"><p>ای تیرِ غمت را دل عشّاق نشانه</p></div>
<div class="m2"><p>خلقی به تو مشغول و تو غایب ز میانه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه معتکف دیرم و گه ساکن مسجد</p></div>
<div class="m2"><p>یعنی که تو را می‌طلبم خانه به خانه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکس به زبانی سخن عشق تو راند</p></div>
<div class="m2"><p>عاشق به سرود غم و مطرب به ترانه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مقصود من از کعبه و بتخانه تویی تو</p></div>
<div class="m2"><p>مقصود تویی کعبه و بتخانه بهانه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>افسون دل افسانهٔ عشق است دگر نه</p></div>
<div class="m2"><p>باقی به جمالت که فسون است و فسانه</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تقصیر خیالی به امید کرم تست</p></div>
<div class="m2"><p>باری چو گنه را به ازاین نیست بهانه</p></div></div>