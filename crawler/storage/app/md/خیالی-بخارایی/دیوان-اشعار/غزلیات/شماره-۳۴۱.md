---
title: >-
    شمارهٔ ۳۴۱
---
# شمارهٔ ۳۴۱

<div class="b" id="bn1"><div class="m1"><p>ای اشک مرا از سر کویش خبری گوی</p></div>
<div class="m2"><p>ز آنروی که بسیار دویدی تو در این کوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرچند که گل از طرف حُسن به برگ است</p></div>
<div class="m2"><p>او نیز کم است از رخ خوب تو به صد روی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نیست ز دشنام رقیبان چو نهانی</p></div>
<div class="m2"><p>بسیار نظرهاست سگت را به دعاگوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گویم صفت نکهت زلف تو و لیکن</p></div>
<div class="m2"><p>ترسم که از این قصّه برد باد صبا بوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر غارت دلها کند آن طرّه خیالی</p></div>
<div class="m2"><p>با او بدر آویز از اینها سر یک موی</p></div></div>