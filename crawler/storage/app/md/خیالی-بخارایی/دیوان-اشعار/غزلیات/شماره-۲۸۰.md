---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>ما را به درِ دوست گشاد از هوس خویش</p></div>
<div class="m2"><p>وقتی ست که دارد سگ آن کوی کس خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا کی برِ آن روی بلافی گل رعنا؟</p></div>
<div class="m2"><p>بگذار دو رویی و نگر پیش و پس خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نیست که نزدیک بلاییم ولیکن</p></div>
<div class="m2"><p>فریاد که دوریم ز فریاد رس خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افسرده حریف سخن سوختگان نیست</p></div>
<div class="m2"><p>زنهار تو ضایع مکن ای نی نفس خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باز آن دهن تنگ چه پوشی ز خیالی</p></div>
<div class="m2"><p>پنهان چه کنی تنگ شکر از مگس خویش</p></div></div>