---
title: >-
    شمارهٔ ۱۸۱
---
# شمارهٔ ۱۸۱

<div class="b" id="bn1"><div class="m1"><p>دوش می گفتم که ماه این دلفروزی از که دید</p></div>
<div class="m2"><p>جانب رویش اشارت کرد شمع و لب گزید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در صفات گوهر سیراب دندان صدف</p></div>
<div class="m2"><p>چون دهن بگشاد از گفتار او دُر می چکید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کاکل و زلفش سیه کاری عجب از سر نهد</p></div>
<div class="m2"><p>در سیاست گرچه این را بست و آن را سر برید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه درد انگیز را از آتش دل فاش کرد</p></div>
<div class="m2"><p>لاجرم زاین گرم نرمی باید او را برکشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا که عمر رفته بر من باز گردد ساعتی</p></div>
<div class="m2"><p>دل ز پی فریاد می کرد و سرشکم می دوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون ز مژگانش خیالی ناوکی کرد التماس</p></div>
<div class="m2"><p>غمزهٔ او هردمی فرمود گفت از من رسید</p></div></div>