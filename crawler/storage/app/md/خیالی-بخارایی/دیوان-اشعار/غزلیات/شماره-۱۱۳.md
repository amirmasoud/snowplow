---
title: >-
    شمارهٔ ۱۱۳
---
# شمارهٔ ۱۱۳

<div class="b" id="bn1"><div class="m1"><p>باد از هوای کوی تو پیغام می‌دهد</p></div>
<div class="m2"><p>جان را به بوی وصل تو آرام می‌دهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یارب چه دولت است که هر شب سگت مرا</p></div>
<div class="m2"><p>بعد از دعای جان تو دشنام می‌دهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بوست عود لیک به دور خطت خطاست</p></div>
<div class="m2"><p>هرکس که دل به نکهت آن خام می‌دهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن نیست جم که دور به زیر نگین اوست</p></div>
<div class="m2"><p>جم ساقیی‌ست کاو به کسی جام می‌دهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تحفهٔ غمت به خیالی رسد ز شوق</p></div>
<div class="m2"><p>اوّل به مژده حاصل ایّام می‌دهد</p></div></div>