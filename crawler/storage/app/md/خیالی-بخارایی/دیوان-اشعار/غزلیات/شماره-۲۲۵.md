---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>گهی به پای تو جانم سرِ نیاز کشید</p></div>
<div class="m2"><p>که دست از هوس کار غیر باز کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم ز شیوهٔ چشمت ندید مردمی ئی</p></div>
<div class="m2"><p>به غیر از این که نیازی نمود و ناز کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>متاع قلب مرا زآن نفس رواجی نیست</p></div>
<div class="m2"><p>که سوز عشق تو در بوتهٔ گداز کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>غرض زیارت سر منزل حقیقت بود</p></div>
<div class="m2"><p>ریاضتی که دلم در ره مجاز کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموش باش خیالیّ و قصّه کوته کن</p></div>
<div class="m2"><p>که از فسانهٔ زلفش سخن دراز کشید</p></div></div>