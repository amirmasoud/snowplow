---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>آزاد بنده‌ای که قبول دلی شود</p></div>
<div class="m2"><p>خرّم دلی که خاک ره مقبلی شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناچار هرکه در خط فرمان کاملی ست</p></div>
<div class="m2"><p>روزی به یمن همّت او کاملی شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از جان سرشته‌اند تو را ورنه مشکل است</p></div>
<div class="m2"><p>کاین شکل دل‌فریب ز آب و گلی شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کس نیست جز نسیم که بگشاید ای دریغ</p></div>
<div class="m2"><p>دل را به فکر زلف تو گر مشکلی شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کشتی عمر غرقهٔ بحر غم است و نیست</p></div>
<div class="m2"><p>زاین ورطه اش امید که بر ساحلی شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوزد درون چو مجمر و ترسم که عاقبت</p></div>
<div class="m2"><p>راز دلم فسانهٔ هر محفلی شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>باشد خیالیا که متاع حدیث تو</p></div>
<div class="m2"><p>روزی قبول خاطر صاحبدلی شود</p></div></div>