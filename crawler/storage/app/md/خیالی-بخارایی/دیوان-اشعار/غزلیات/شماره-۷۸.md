---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>گرچه ماه نو به شوخی بی نظیر عالم است</p></div>
<div class="m2"><p>لیک در خوبی ز ابروی تو بسیاری کم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرنه دزد نقد قلب ماست زلف شب روَت</p></div>
<div class="m2"><p>از چه معنی اینچنین آشفته حال و درهم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گوشهٔ خاطر بپرداز ای دل از سودای جان</p></div>
<div class="m2"><p>در حریم خاص جانان غیر چون نامحرم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه کز سودای چشمت حاصل عمر عزیز</p></div>
<div class="m2"><p>می‌رود در عین خونخواری و آن هم یک دم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای دریغا نیست بنیاد بقا را محکمی</p></div>
<div class="m2"><p>ورنه با جانان بنای عهد ما بس محکم است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خیالی کار عالم چون به کین جان ماست</p></div>
<div class="m2"><p>جان اگر خواهی مباش ایمن که کار عالم است</p></div></div>