---
title: >-
    شمارهٔ ۹۷
---
# شمارهٔ ۹۷

<div class="b" id="bn1"><div class="m1"><p>آن دم ایاز خاص به مقصود می‌رسد</p></div>
<div class="m2"><p>کز بندگی به خدمت محمود می‌رسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناموس چون محاب ده کوی وحدت است</p></div>
<div class="m2"><p>زاین عقبه هرکه می‌گذرد زود می‌رسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دریاب خویش را اگرت عزم کوی اوست</p></div>
<div class="m2"><p>کآنجا کسی که بگذرد از سود می‌رسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر سالکی که پا ز سر صدق می‌نهد</p></div>
<div class="m2"><p>اوّل قدم به منزل مقصود می‌رسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو پیش آه گرم رو ای پیک تیزرو</p></div>
<div class="m2"><p>کو نیز در قفای تو چون دود می‌رسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر شام غم رسید خیالی صبور باش</p></div>
<div class="m2"><p>کز غیب آنچه روزی ما بود می‌رسد</p></div></div>