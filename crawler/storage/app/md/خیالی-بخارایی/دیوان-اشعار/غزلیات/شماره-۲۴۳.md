---
title: >-
    شمارهٔ ۲۴۳
---
# شمارهٔ ۲۴۳

<div class="b" id="bn1"><div class="m1"><p>نکردم جز به زلف یار پیوند</p></div>
<div class="m2"><p>که نتوان کرد خود را بی رسن بند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چه شرین کرد طوطی کز سر شوق</p></div>
<div class="m2"><p>لبت را دید و بگذشت از سرقند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سگت را بی وفا گفتم عفاالله</p></div>
<div class="m2"><p>گناه از بنده و عفو از خداوند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرو چون سیل اشک از دیده هر دم</p></div>
<div class="m2"><p>که خون کردی دلم را ای جگر بند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل غمگین به بویی از تو خوشنود</p></div>
<div class="m2"><p>خیالی با خیالی از تو خرسند</p></div></div>