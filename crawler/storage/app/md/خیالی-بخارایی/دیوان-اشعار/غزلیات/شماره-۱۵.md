---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>هر خبر کز سرکشی گوید صبا</p></div>
<div class="m2"><p>سرو قدّت می رباید از هوا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرو تا شد بندهٔ نخل قدت</p></div>
<div class="m2"><p>می برآید گرد باغ آزاد پا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زد بنفشه با خطِ سبز تو لاف</p></div>
<div class="m2"><p>زان زبانش را کشیدند از قفا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دی به دشنامی سگِ کوی توام</p></div>
<div class="m2"><p>در چه کاری گفت گفتم در دعا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر درِ دل دوش در می زد یکی</p></div>
<div class="m2"><p>کیست پرسیدم غمت گفت آشنا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سایل اشک خیالی را ز روی</p></div>
<div class="m2"><p>چند رانی نیست آخر روی ما</p></div></div>