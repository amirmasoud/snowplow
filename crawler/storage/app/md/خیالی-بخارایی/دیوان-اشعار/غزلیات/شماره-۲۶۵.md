---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>ما را ز روزگار غمِ روزگار بس</p></div>
<div class="m2"><p>جور و جفایِ دلبر زیبا عذار بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشم از جهان و خلق جهان، سخت بسته ایم</p></div>
<div class="m2"><p>زیرا برای ما کَرَمِ کردگار بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از هرچه خوب روی و گلندام و سروقد</p></div>
<div class="m2"><p>ما را، اشارتی ز دو ابروی یار بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از ناله های سوخته جانان روزگار</p></div>
<div class="m2"><p>در باغ دهر نغمهٔ مرغِ هزار بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر جویبار عمر خیالی چو بنگری</p></div>
<div class="m2"><p>اشک روانِ دیدهٔ شب زنده دار بس</p></div></div>