---
title: >-
    شمارهٔ ۳۴۰
---
# شمارهٔ ۳۴۰

<div class="b" id="bn1"><div class="m1"><p>اگرچه مشک را باشد به هر سویی خریداری</p></div>
<div class="m2"><p>ولی در حلقهٔ زلفت ندارد روز بازاری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به رویت صورت چین تا نزد لاف دل افروزی</p></div>
<div class="m2"><p>نمی خواهم که بینم نقش او بر هیچ دیواری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شدم خاک و در آن کو می برد بادم بحمدالله</p></div>
<div class="m2"><p>که در کویت بدین تدبیر باری یافتم باری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مه نو را چو با چشم خریداری شبی دیدم</p></div>
<div class="m2"><p>به چشمم کم نمود از ابروی شوخ تو بسیاری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلا گر حاجتی داری به نزد سرو قدّی بر</p></div>
<div class="m2"><p>چو حاجت می بری باری به نزدیک قدم داری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیالی چند می‌پیچی ز حکمش گردن طاعت</p></div>
<div class="m2"><p>سری در کار تیغش کن اگر داری سر کاری</p></div></div>