---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>گر قدح با لب میگون تو لافی دارد</p></div>
<div class="m2"><p>زو نرنجی که به غایت دل صافی دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سینه از زخم فراق تو چنان شد نی را</p></div>
<div class="m2"><p>که به هر جا که نهی دست شکافی دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم فتّان تو پیوسته ز ابرو و مژه</p></div>
<div class="m2"><p>صف کشیده ست و به عشّاق مصافی دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محرم کوی تو محروم ز دیدار چراست</p></div>
<div class="m2"><p>چون به گرد حرمِ کعبه طوافی دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر کسی پیش خیالی کند اظهار سخن</p></div>
<div class="m2"><p>هیچ کس را سخنی نیست که لافی دارد</p></div></div>