---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>تا به معنی اهل صورت دم ز آب و گل زدند</p></div>
<div class="m2"><p>جان گدازان سکّهٔ محنت به نام دل زدند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در مقام غم چو بزم امتحان آراست عشق</p></div>
<div class="m2"><p>خویش را ارباب دل بر شربت قاتل زدند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای بسا کشتی که بشکستند سیّاحان راه</p></div>
<div class="m2"><p>تا قدم زاین ورطهٔ خون خوار بر ساحل زدند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفرین بر راه بینانی که شبهای رحیل</p></div>
<div class="m2"><p>ناغنوده کوس رحلت زاین کهن منزل زدند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اوّل از حرف جنون نام خیالی نقش شد</p></div>
<div class="m2"><p>چون رقم بر نامهٔ رندان لایعقل زدند</p></div></div>