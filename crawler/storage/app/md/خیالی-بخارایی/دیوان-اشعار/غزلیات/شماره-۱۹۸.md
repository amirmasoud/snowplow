---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>طوطیِ عقلم که دعویّ تکلّم می‌کند</p></div>
<div class="m2"><p>چون دهانت نقش می‌بندد سخن گم می‌کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فریب غمزه دانستم که عین مردمی‌ست</p></div>
<div class="m2"><p>چشم مستت آنچه از شوخی به مردم می‌کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر ندارد از گُل روی تو رنگی از چه رو</p></div>
<div class="m2"><p>غنچه از شادی به زیر لب تبسّم می‌کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقیا زآن رو ز دوران شاکرم کاو عاقبت</p></div>
<div class="m2"><p>خاک هستی مرا خشت سر خُم می‌کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌رخت آگه ز فریاد خیالی بلبل است</p></div>
<div class="m2"><p>کاو ز شوق رویِ گل با خود ترنّم می‌کند</p></div></div>