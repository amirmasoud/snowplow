---
title: >-
    شمارهٔ ۷۵
---
# شمارهٔ ۷۵

<div class="b" id="bn1"><div class="m1"><p>گرچه تو حقیری و گناه تو عظیم است</p></div>
<div class="m2"><p>نومید نباشی که خداوند کریم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو عذر به پیش آر که بر عذر گنه درّ</p></div>
<div class="m2"><p>چون گوش بگیرد همه گویند یتیم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از محدث تقصیر چه غم اهل گنه را</p></div>
<div class="m2"><p>چون لطف تو عام است و عطای تو قدیم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیم است و امید از تو در این ره همه کس را</p></div>
<div class="m2"><p>لیکن چو امید از کرم توست چه بیم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر رحم کند یار عجب نیست خیالی</p></div>
<div class="m2"><p>آری نشنیدی که کریم است و رحیم است</p></div></div>