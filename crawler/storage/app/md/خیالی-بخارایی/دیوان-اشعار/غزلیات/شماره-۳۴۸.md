---
title: >-
    شمارهٔ ۳۴۸
---
# شمارهٔ ۳۴۸

<div class="b" id="bn1"><div class="m1"><p>ای دل سر تسلیم بنه بر کف پایی</p></div>
<div class="m2"><p>کز راه تکبر نرسد کار به جایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکس که به می صاف نارد قدح دل</p></div>
<div class="m2"><p>گر صوفی وقت است در او نیست صفایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون دفتر گل باد پراکنده به هر باد</p></div>
<div class="m2"><p>هر دل که در او از طرفی نیست هوایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستانِ می شوق تو را غیر قدح نیست</p></div>
<div class="m2"><p>در دور حریفی که زند گرد برایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را چو سکندر هوس چشمهٔ حیوان</p></div>
<div class="m2"><p>زآن است که دارد به لبت نسبت مایی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای سرو خیالی چو هوادار قدیم است</p></div>
<div class="m2"><p>گه گه به رهش بهر خدا طال بقایی</p></div></div>