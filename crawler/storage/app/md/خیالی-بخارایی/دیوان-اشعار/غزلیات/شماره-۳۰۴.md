---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>ما ز تقصیر عبادت چون پشیمان آمدیم</p></div>
<div class="m2"><p>گوش بگرفته به درویشی به سلطان آمدیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو مورانِ حقیر از غایت تقصیر خویش</p></div>
<div class="m2"><p>سر به پیش انداخته پیش سلیمان آمدیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا مگر بویی بریم آخر ز درگاه قبول</p></div>
<div class="m2"><p>ره همه ره چون صبا افتان و خیزان آمدیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مدّتی چون ذره سر گردان شدیم و عاقبت</p></div>
<div class="m2"><p>پای کوبان جانب خورشید تابان آمدیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرچه کمتر برده ایم از روی صورت ره به دوست</p></div>
<div class="m2"><p>نیست جز غم از ره معنی فراوان آمدیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو بشارت ده خیالی را به اسم بندگی</p></div>
<div class="m2"><p>خاصه این ساعت که ما خاص از پیِ آن آمدیم</p></div></div>