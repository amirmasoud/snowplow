---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>ماه رخسارِ تو دید و عاشقی بنیاد کرد</p></div>
<div class="m2"><p>گل نسیمت از صبا بشنید و دل برباد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نخلِ قدّ دلکشت را بنده چون بسیار شد</p></div>
<div class="m2"><p>از برای جان درازی سرو را آزاد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردمیهای رقیبت را فرامُش چون کنم</p></div>
<div class="m2"><p>کاو سگ کوی تو را چون دید ما را یاد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز ابرو علم سحر آموزد آخر غمزه ات</p></div>
<div class="m2"><p>مدّتی در عین شوخی خدمت استاد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر خیالی در غم عشقت بمیرد باک نیست</p></div>
<div class="m2"><p>چون به تکبیری بخواهی روح او را شاد کرد</p></div></div>