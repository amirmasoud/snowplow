---
title: >-
    شمارهٔ ۷
---
# شمارهٔ ۷

<div class="b" id="bn1"><div class="m1"><p>چون نی اگر چه عمری خوش می نواخت ما را</p></div>
<div class="m2"><p>دیگر نمی شناسد آن ناشناخت ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صرّاف عشق در ما قلبی اگر نمی دید</p></div>
<div class="m2"><p>در بوتهٔ جدایی کی می گداخت ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل مساز ما را بی او به صبر راضی</p></div>
<div class="m2"><p>زیرا که این مفرّح هرگز نساخت ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل در طریق وحدت از نیستی نزد دم</p></div>
<div class="m2"><p>در راه عشقبازان تا در نباخت ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با سوز او خیالی چون عود ساز و خوش باش</p></div>
<div class="m2"><p>کآخر چو چنگ روزی خواهد نواخت ما را</p></div></div>