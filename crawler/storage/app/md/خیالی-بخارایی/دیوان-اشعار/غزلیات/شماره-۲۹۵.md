---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>سرشک جانب خاک در تو بست احرام</p></div>
<div class="m2"><p>که دیده کرد روانش به آبروی تمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلامت است در این راه قاصدی که درست</p></div>
<div class="m2"><p>بدان جناب رساند ازاین شکسته سلام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برفت ناله کنان دل ز کعبه سوی درت</p></div>
<div class="m2"><p>که در محل ترنم خویش است سیر مقام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر ز حسرت جام لبت چو ساغر می</p></div>
<div class="m2"><p>بغیر خون جگر طعمه ای خوریم حرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خموش باش خیالی که درس نظم آن را</p></div>
<div class="m2"><p>بجای شرح معانی بس است حسن کلام</p></div></div>