---
title: >-
    شمارهٔ ۲۶۳
---
# شمارهٔ ۲۶۳

<div class="b" id="bn1"><div class="m1"><p>گر به رویت کنند نسبت حور</p></div>
<div class="m2"><p>جان من نسبتی ست دورادور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سرِ کویِ تو تا ندید، نشد</p></div>
<div class="m2"><p>باغ فردوس معترف به قصور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آن چنان شد ز رشک روی تو ماه</p></div>
<div class="m2"><p>که نمانده ست در رخ او نور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکسی چون به دور نرگس تو</p></div>
<div class="m2"><p>مست جام غم است نامخمور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به سودای ابرویت آن به</p></div>
<div class="m2"><p>که نگیریم گوشه‌ای و حضور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خیالی ز ننگ میری به</p></div>
<div class="m2"><p>که به نام نکو شوی مشهور</p></div></div>