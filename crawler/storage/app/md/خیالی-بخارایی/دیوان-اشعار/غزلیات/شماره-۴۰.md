---
title: >-
    شمارهٔ ۴۰
---
# شمارهٔ ۴۰

<div class="b" id="bn1"><div class="m1"><p>به قتل خسته دلان غمزهٔ تو قانع نیست</p></div>
<div class="m2"><p>وگرنه از طرف بنده هیچ مانع نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبت چو دعویِ خون کرد غمزه تیغ کشید</p></div>
<div class="m2"><p>حدیث منطقیان بی دلیل قاطع نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز خدمت سگ کوی تو راحتی دیدم</p></div>
<div class="m2"><p>به مردمی که کریم اند رنج ضایع نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به سعی ما سحری دولتی طلوع نکرد</p></div>
<div class="m2"><p>چه سود کوشش بیچارگان چو طالع نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اسیر دام سر زلف توست مرغ دلم</p></div>
<div class="m2"><p>قسم به طایر گردون که غیر واقع نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز جام دور خیالی میِ غرور منوش</p></div>
<div class="m2"><p>که شربتی ست فرح بخش لیک نافع نیست</p></div></div>