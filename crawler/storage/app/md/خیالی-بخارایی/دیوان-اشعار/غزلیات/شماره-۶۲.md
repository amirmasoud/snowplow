---
title: >-
    شمارهٔ ۶۲
---
# شمارهٔ ۶۲

<div class="b" id="bn1"><div class="m1"><p>رنجور عشق را سر ناز طبیب نیست</p></div>
<div class="m2"><p>یعنی طبیب خسته دلان جز حبیب نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تنها نه ما وظیفهٔ انعام می خوریم</p></div>
<div class="m2"><p>از خوان رحمت تو کسی بی نصیب نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر یاد می کند ز غریب دیار خویش</p></div>
<div class="m2"><p>هیچ از کمال مرحمت او غریب نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گوشی که شد ز ولولهٔ چنگ و نی گران</p></div>
<div class="m2"><p>شایستهٔ نصیحت و پند ادیب نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگشا دری به روی خیالی ز باغ وصل</p></div>
<div class="m2"><p>مرغی که صید تست کم از عندلیب نیست</p></div></div>