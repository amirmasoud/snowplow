---
title: >-
    شمارهٔ ۱۶۶
---
# شمارهٔ ۱۶۶

<div class="b" id="bn1"><div class="m1"><p>خیز که پیر مغان میکده را درگشاد</p></div>
<div class="m2"><p>نوبت مستی رسید باده بده برگشاد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دولت جم بایدت سر مکش از خطّ جام</p></div>
<div class="m2"><p>چون خم تجرید را ساقی جان سرگشاد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر درِ طاعت ببست یار به رویم چه باک</p></div>
<div class="m2"><p>چون ز ره مرحمت صد درِ دیگر گشاد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در طرف نیستی تا نشدم گم نیافت</p></div>
<div class="m2"><p>بستگی کارم از پیر قلندر گشاد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم خیالی ز اشک مخزن یاقوت شد</p></div>
<div class="m2"><p>تا به تبسّم لبت حقّهٔ گوهر گشاد</p></div></div>