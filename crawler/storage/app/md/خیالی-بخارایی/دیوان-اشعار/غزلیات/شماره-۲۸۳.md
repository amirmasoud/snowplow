---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>هر نقد دل کآن غمزهٔ پر حیله می آرد به کف</p></div>
<div class="m2"><p>بازش ز عین سرخوشی چشم تو می سازد تلف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر غمزه ات خون می کند چشم تو یاری می دهد</p></div>
<div class="m2"><p>ور عارضت دل می برد زلف تو می گیرد طرف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر خدمتی آید ز ما بر وجه منت ز آنکه هست</p></div>
<div class="m2"><p>از خدمت خاک درت خورشید را چندین شرف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هردم به دندان تو دُر لاف لطافت می زند</p></div>
<div class="m2"><p>هرچند کآن بی باک را دندان همی خاید صدف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی رقیب از خرّمی طعنه زند بر حال من</p></div>
<div class="m2"><p>این ژاژ خایی را بگو تا بس کند آن بد علف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گرچه خیالی سر به سر اشک تو دُر شد زآن چه سود</p></div>
<div class="m2"><p>چون آبت از رو می برد باز آن یتیم ناخلف</p></div></div>