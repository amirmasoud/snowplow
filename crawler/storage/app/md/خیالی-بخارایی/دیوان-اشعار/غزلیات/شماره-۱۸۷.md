---
title: >-
    شمارهٔ ۱۸۷
---
# شمارهٔ ۱۸۷

<div class="b" id="bn1"><div class="m1"><p>سرشک تا به کی از چشم آن و این افتد</p></div>
<div class="m2"><p>مباد کز نظر خلق بر همین افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگو به مردم بیگانه راز خود ای اشک</p></div>
<div class="m2"><p>چنان مکن که حدیث تو بر زمین افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش است خلد برین و دلم نمی خواهد</p></div>
<div class="m2"><p>که بی جمال تو هرگز نظر بر این افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی که قدّ تو بیند نه بیند ابرویت</p></div>
<div class="m2"><p>چگونه کج نگرد هر که راست بین افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو دل فتاد خیالی به دست خوش دارش</p></div>
<div class="m2"><p>که باز دیر بیاید که این چنین افتد</p></div></div>