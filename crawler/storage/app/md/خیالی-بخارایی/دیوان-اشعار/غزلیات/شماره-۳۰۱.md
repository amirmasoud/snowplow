---
title: >-
    شمارهٔ ۳۰۱
---
# شمارهٔ ۳۰۱

<div class="b" id="bn1"><div class="m1"><p>مابه وجهی صفت روی تو با مه کردیم</p></div>
<div class="m2"><p>که بر او عاقبت این نکته موجّه کردیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سر ز خجلت نه برآورد دگر یوسف مصر</p></div>
<div class="m2"><p>به حدیث تواش از بس که فرا چه کردیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به بالای تو بستیم دل ای سروِ بلند</p></div>
<div class="m2"><p>به هوای تو که دست از همه کوته کردیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از پریشانیِ زلف تو دلم ز آن جمع است</p></div>
<div class="m2"><p>که تو را عاقبت از حال خود آگه کردیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا توان بی خطری بار به منزل بردن</p></div>
<div class="m2"><p>توشهٔ ره ز توکّلت علی الله کردیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خیالی ز ره و رسم محبّت بیرون</p></div>
<div class="m2"><p>هرچه کردیم به جان تو که بی ره کردیم</p></div></div>