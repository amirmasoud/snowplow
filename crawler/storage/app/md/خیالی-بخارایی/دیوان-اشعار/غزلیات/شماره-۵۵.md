---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>دلا طریقهٔ عشّاق خود پرستی نیست</p></div>
<div class="m2"><p>چرا که شیوهٔ مردان راه هستی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو خاک پست شو ار آبروی می طلبی</p></div>
<div class="m2"><p>که میل آب روان جز به سوی پستی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراب بادهٔ شوقیم و عین بی خبری ست</p></div>
<div class="m2"><p>از این شراب کسی را که ذوق مستی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به آب دیده ز دل نقش غیر پاک بشوی</p></div>
<div class="m2"><p>که قبله گاه نظر جای بت پرستی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کجاست نقش دهانت کز او خیالی را</p></div>
<div class="m2"><p>به دست مایده یی غیر تنگدستی نیست</p></div></div>