---
title: >-
    شمارهٔ ۲۴۶
---
# شمارهٔ ۲۴۶

<div class="b" id="bn1"><div class="m1"><p>هر خطایی که سزاوار عتابی باشد</p></div>
<div class="m2"><p>عفو فرما که تو را نیز صوابی باشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر از گریهٔ غم آن بَرَد چشم مرا</p></div>
<div class="m2"><p>هیچ غم نیست گرش پیش تو آبی باشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پردهٔ ما بدرد فکر جنون تا که تو را</p></div>
<div class="m2"><p>بر مه از سلسلهٔ مشگ نقابی باشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر نباشد خبر از محنت دوران چه عجب</p></div>
<div class="m2"><p>سر خوشی را که به کف جام شرابی باشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خیالی به خیالی شده ای قانع و آن</p></div>
<div class="m2"><p>هم به شرطی ست که در چشم تو خوابی باشد</p></div></div>