---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>گر ندیدی کز سرای دیده‌ام خون می‌چکد</p></div>
<div class="m2"><p>ساعتی بنشین در او تا بنگری چون می‌چکد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لاله می‌روید به یاد روی لیلی تا به حشر</p></div>
<div class="m2"><p>از زمین، هرجا که آب چشم مجنون می‌چکد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌کشد هردم به قصد خون مردم تیغ تاز</p></div>
<div class="m2"><p>ترک چشمت کز سر شمشیر او خون می‌چکد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبنمی باشد که از برگ گل افتد بر زمین</p></div>
<div class="m2"><p>قطره‌های خون کز آن رخسار گلگون می‌چکد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دم به دم از روچه می‌رانی خیالی اشک را</p></div>
<div class="m2"><p>او بر آب از خانهٔ مردم چو بیرون می‌چکد</p></div></div>