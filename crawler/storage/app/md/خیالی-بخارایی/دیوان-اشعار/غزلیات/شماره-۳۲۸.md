---
title: >-
    شمارهٔ ۳۲۸
---
# شمارهٔ ۳۲۸

<div class="b" id="bn1"><div class="m1"><p>صوفی ما جام باشد باده تا باشد در او</p></div>
<div class="m2"><p>هرکه دارد باطن صافی صفا باشد در او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ابر چون باران ببیند سیل مژگان مرا</p></div>
<div class="m2"><p>از خجالت آب گردد گر حیا باشد در او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفته ای رنگی ست روزی از گل رویم تو را</p></div>
<div class="m2"><p>وعدهٔ خوب است اگر بوی وفا باشد در او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاق ابروی تو بر روی نکو بی وجه نیست</p></div>
<div class="m2"><p>هر کجا قبله ست محراب دعا باشد در او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر دهد بر باد روزی ای خیالی همچو سرو</p></div>
<div class="m2"><p>هر سری کز جانب سروی هوا باشد در او</p></div></div>