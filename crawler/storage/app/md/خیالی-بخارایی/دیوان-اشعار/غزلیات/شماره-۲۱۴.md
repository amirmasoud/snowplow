---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>گر تیغ زند یار نخواهیم حذر کرد</p></div>
<div class="m2"><p>کز دوست به تیغی نتوان قطع نظر کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر تیر بلایی که رسید از طرف یار</p></div>
<div class="m2"><p>جان پیش ستاد و همه را سینه سپر کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر یارِ مرا باز هوای دگری نیست</p></div>
<div class="m2"><p>بی جرم چرا از من بیچاره دگر کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گشت مقیم حرم دل غم عشقت</p></div>
<div class="m2"><p>جان از وطن خویش روان عزم سفر کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عمری شد و هرگز نشنیدم که خیالی</p></div>
<div class="m2"><p>بی غصّه شبی را به خیال تو سحر کرد</p></div></div>