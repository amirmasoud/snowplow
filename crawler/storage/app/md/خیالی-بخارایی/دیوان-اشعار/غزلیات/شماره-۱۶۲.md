---
title: >-
    شمارهٔ ۱۶۲
---
# شمارهٔ ۱۶۲

<div class="b" id="bn1"><div class="m1"><p>خدا بتان جفا کیش را وفا بخشد</p></div>
<div class="m2"><p>ندامت از ستم و توبه از جفا بخشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو را ز حُسن و ملاحت هر آنچه باید هست</p></div>
<div class="m2"><p>ولی طریقهٔ مهر و وفا خدا بخشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کرامتی به از این نیست زاهدا که کریم</p></div>
<div class="m2"><p>مرا نیاز و تو را توبه از ریا بخشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز جور و کین تو باشد که ای رقیب مرا</p></div>
<div class="m2"><p>خدای صبر دهد یا تو را حیا بخشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>امید و بیم خیالی از این دو بیرون نیست</p></div>
<div class="m2"><p>که عشق او کُشدش از فراق یا بخشد</p></div></div>