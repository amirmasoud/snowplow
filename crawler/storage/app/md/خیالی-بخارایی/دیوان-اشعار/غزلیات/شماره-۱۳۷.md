---
title: >-
    شمارهٔ ۱۳۷
---
# شمارهٔ ۱۳۷

<div class="b" id="bn1"><div class="m1"><p>تا خرد خیمه سوی عالم جسمانی زد</p></div>
<div class="m2"><p>عشق در کشور جان رایت سلطانی زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طرّهٔ زلف بتان حلقهٔ رسوایی شد</p></div>
<div class="m2"><p>کافر چشم بتان راه مسلمانی زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار چون پردهٔ ناموس فرو هشت ز رخ</p></div>
<div class="m2"><p>عقل سرگشته قدم در ره حیرانی زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باشد از طرف رخ دوست کسی را دل جمع</p></div>
<div class="m2"><p>که چو زلف سیهش دم ز پریشانی زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا تو در راه طلب پا ننهی بر سر خویش</p></div>
<div class="m2"><p>قدم راست در این بادیه نتوانی زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ساقیا دست بشوی از می و بنگر که سبو</p></div>
<div class="m2"><p>بر سر از شرمِ گنه دست پشیمانی زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرنه آئین خیالی صفت نادانی ست</p></div>
<div class="m2"><p>پیش اصحاب چرا لاف سخندانی زد</p></div></div>