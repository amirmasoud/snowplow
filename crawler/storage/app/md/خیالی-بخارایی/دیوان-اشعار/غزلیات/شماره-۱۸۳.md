---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>روی تو طعنه بر گُل سیراب می‌زند</p></div>
<div class="m2"><p>لعل تو خنده بر شکر ناب می‌زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنبل شکسته خاطر از آن است در چمن</p></div>
<div class="m2"><p>کز رشک سبزهٔ خطِ تو تاب می‌زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش وقت نرگس تو که در عین سرخوشی</p></div>
<div class="m2"><p>پیوسته در میانهٔ گل خواب می‌زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در آرزوی خیل خیال تو هر شبی</p></div>
<div class="m2"><p>باران اشک خانهٔ چشم آب می‌زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>روی از درش متاب خیالی به هیچ باب</p></div>
<div class="m2"><p>چون مرد عشق لاف از این باب می‌زند</p></div></div>