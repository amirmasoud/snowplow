---
title: >-
    شمارهٔ ۲۱۸
---
# شمارهٔ ۲۱۸

<div class="b" id="bn1"><div class="m1"><p>گر ز بالای تو هر ساعت بلا باید کشید</p></div>
<div class="m2"><p>به ز ناز سرو کز باد هوا باید کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با وجود قامتت سوسن ز رعنائی و لطف</p></div>
<div class="m2"><p>گر کند دعوی زبانش از قفا باید کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما اگر مشگ ختا گفتیم زلفت را به سهو</p></div>
<div class="m2"><p>تو کریمی عفو بر فکر خطا باید کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بپرسند از پریشان حالی ما روز حشر</p></div>
<div class="m2"><p>سر چو زلفت از خجالت زیر پا باید کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خیال قدّ و رخسارش خیالی در دل است</p></div>
<div class="m2"><p>منّت شمشاد و ناز گل چرا باید کشید</p></div></div>