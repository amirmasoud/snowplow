---
title: >-
    شمارهٔ ۲۹۳
---
# شمارهٔ ۲۹۳

<div class="b" id="bn1"><div class="m1"><p>دل گم شد و جز بر دهنش نیست گمانم</p></div>
<div class="m2"><p>جویید به او گرد نبوَد هیچ ندانم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای اشک چو آن رویِ نکو روزیِ مانیست</p></div>
<div class="m2"><p>بی وجه تو را در طلبش چند دوانم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با شمع چو گفتم غمِ دل گفت که من نیز</p></div>
<div class="m2"><p>از دست دل سوختهٔ خویش بجانم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی ماه رخت آه من از چرخ گذر کرد</p></div>
<div class="m2"><p>تا دورم از آن روی چنین می گذرانم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آن که چو کوهم کمر شوق تو بسته</p></div>
<div class="m2"><p>در چشم تو بی سنگ و به روی تو گرانم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتم برسانم به تو پیغام خیالی</p></div>
<div class="m2"><p>حیف است که این گویم و جایی نرسانم</p></div></div>