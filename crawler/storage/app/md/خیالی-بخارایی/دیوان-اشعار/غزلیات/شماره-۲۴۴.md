---
title: >-
    شمارهٔ ۲۴۴
---
# شمارهٔ ۲۴۴

<div class="b" id="bn1"><div class="m1"><p>واقف از جام می لعل تو مدهوشانند</p></div>
<div class="m2"><p>در خور بادهٔ لعل تو قدح نوشانند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آخر ای نامه سفید از صف رندان بدر آی</p></div>
<div class="m2"><p>که در این خانهٔ تاریک سیه پوشانند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چون قدح گرد برای صف عشاق بزن</p></div>
<div class="m2"><p>تا ببینی که در این حلقه چه مدهوشانند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون بر آریم سر از شرم گنه گرنه به حشر</p></div>
<div class="m2"><p>دامن مغفرتی بر سرمان پوشانند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دهن قال فروبند خیالی و خموش</p></div>
<div class="m2"><p>راز دار سخن عشق چو خاموشانند</p></div></div>