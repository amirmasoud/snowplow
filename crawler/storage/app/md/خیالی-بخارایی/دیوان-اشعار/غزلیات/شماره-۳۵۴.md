---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>تا در قدم اهل دلی خاک نگردی</p></div>
<div class="m2"><p>از تیرگی عجب و ریا پاک نگردی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید صفت تا که مجّرد نه برآیی</p></div>
<div class="m2"><p>سلطان سراپردهٔ افلاک نگردی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار از پی بهبود چو کاری به تو فرمود</p></div>
<div class="m2"><p>شرط ادب این است که چالاک نگردی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل غم عشق است نصیب تو در این راه</p></div>
<div class="m2"><p>و آن نیز به شرطی ست که غمناک نگردی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ظاهر نشود سرخی روی تو خیالی</p></div>
<div class="m2"><p>تا همچو گل از غصّه کفن چاک نگردی</p></div></div>