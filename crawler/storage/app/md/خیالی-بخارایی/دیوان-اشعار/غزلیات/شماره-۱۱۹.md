---
title: >-
    شمارهٔ ۱۱۹
---
# شمارهٔ ۱۱۹

<div class="b" id="bn1"><div class="m1"><p>باز بیرون شدی و نوبت حیرانی شد</p></div>
<div class="m2"><p>زلف برهم زدی و وقت پریشانی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدمی نه سوی کنج دلم از گنج مراد</p></div>
<div class="m2"><p>که ز دوریّ تو نزدیک به ویرانی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به پیش لب جان پرور تو چشمهٔ خضر</p></div>
<div class="m2"><p>چه خطا گفت که منسوب به حیوانی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل از وسوسهٔ نفس حذر کن که مرا</p></div>
<div class="m2"><p>به جمالش هوس صحبت روحانی شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چند بر خاک نهی پیش بتان پیشانی</p></div>
<div class="m2"><p>عذر پیش آر که هنگام پشیمانی شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خیالی چو لبش بوسه به جانی بفروخت</p></div>
<div class="m2"><p>غم افلاس مخور بیش که ارزانی شد</p></div></div>