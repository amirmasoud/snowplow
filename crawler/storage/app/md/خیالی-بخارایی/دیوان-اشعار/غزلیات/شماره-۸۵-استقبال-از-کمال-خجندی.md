---
title: >-
    شمارهٔ ۸۵ - استقبال از کمال خجندی
---
# شمارهٔ ۸۵ - استقبال از کمال خجندی

<div class="b" id="bn1"><div class="m1"><p>نرگس خیال چشم تو در خواب ناز یافت</p></div>
<div class="m2"><p>سرو از هوای قدّ تو عمر دراز یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نی را که رفته بود دل سوخته ز دست</p></div>
<div class="m2"><p>چون از وصال تو خبری یافت باز یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خاک ره نیاز شو ای دل که چشم من</p></div>
<div class="m2"><p>هر آب رو که یافت ز اشک نیاز یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با سوز دل بساز و دم از نور زن که شمع</p></div>
<div class="m2"><p>این منزلت که یافت ز سوز و گداز یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای نی رهی نما به خیالی ز کوی دوست</p></div>
<div class="m2"><p>کاو در طریقِ عشق تو را چشم باز یافت</p></div></div>