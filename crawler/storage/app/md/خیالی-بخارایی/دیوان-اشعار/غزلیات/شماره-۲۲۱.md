---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>گر نه با من سر زلفت به جفا پیدا شد</p></div>
<div class="m2"><p>در سرم این همه سودا زکجا پیدا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا نهان شد ز نظر صورت روی تو مرا</p></div>
<div class="m2"><p>بر رخ از دیده چه گویم که چه ها پیدا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آبم از روی ببرد اشک و نمی دانم چیست</p></div>
<div class="m2"><p>غرض او، که بدین وجه به ما پیدا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با وجود خط و خال تو دل سوخته را</p></div>
<div class="m2"><p>هوس مشگ ز سودای خطا پیدا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر نشد ماه نو از ابروی شوخ تو خجل</p></div>
<div class="m2"><p>به چه معنی ز نظر خم زد و ناپیدا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عاقبت تا چه شود حال خیالی به رقیب</p></div>
<div class="m2"><p>این چنین کآن سگ بدخوبه گدا پیدا شد</p></div></div>