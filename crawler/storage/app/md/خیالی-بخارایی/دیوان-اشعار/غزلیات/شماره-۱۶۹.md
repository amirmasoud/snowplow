---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>در چمن سبزهٔ سیراب به هرجا که رسید</p></div>
<div class="m2"><p>ماند بربوی خط سبز تو چندان که دمید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آب دوش از هوس عارض و قدّت همه شب</p></div>
<div class="m2"><p>در قدمهای گل و سرو به سر می غلتید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دی گل و لاله همی چید کسی در بستان</p></div>
<div class="m2"><p>چون رخت دید از آنها همه دامن درچید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش صاحب نظران در صفت عارض تو</p></div>
<div class="m2"><p>اشک هر نکته که گفت از سخنش آب چکید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما به خاک در تو راه نبردیم ولیک</p></div>
<div class="m2"><p>اشک هر فکر که می کرد در این باب دوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفته ام غیرت مه روی تو را نادیده</p></div>
<div class="m2"><p>تا نگویند خیالی رخ آن مه را دید</p></div></div>