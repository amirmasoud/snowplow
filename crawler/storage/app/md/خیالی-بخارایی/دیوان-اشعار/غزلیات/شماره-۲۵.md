---
title: >-
    شمارهٔ ۲۵
---
# شمارهٔ ۲۵

<div class="b" id="bn1"><div class="m1"><p>ار شیخ صومعه ست وگر رندِ دیرِ توست</p></div>
<div class="m2"><p>وِرد زبان پیر و جوان ذکر خیرِ توست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا غیرت جمال تو در پرده رخ نمود</p></div>
<div class="m2"><p>بر دوختیم چشم دل از هرچه غیرِ توست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اسرار جلوه گاه جمال از کلیم پرس</p></div>
<div class="m2"><p>یعنی نوای طور زدن طور طیرِ توست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای دل طواف کعبهٔ کویش مده ز دست</p></div>
<div class="m2"><p>کز دیر باز آن سرِ کو جای سیرِ توست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گه گه بگیر دست خیالی به ساغری</p></div>
<div class="m2"><p>کاو نیز دیر شد که ز رندان دیرِ توست</p></div></div>