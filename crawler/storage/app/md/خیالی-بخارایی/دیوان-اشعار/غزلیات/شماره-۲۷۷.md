---
title: >-
    شمارهٔ ۲۷۷
---
# شمارهٔ ۲۷۷

<div class="b" id="bn1"><div class="m1"><p>دل چو گشت از جام معنی جرعه نوش</p></div>
<div class="m2"><p>دلقِ صورت کرد رهن می فروش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عیب خرقه گفتن از نامردمی ست</p></div>
<div class="m2"><p>دولت رندی که باشد عیب پوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باز از جوش می و غوغای چنگ</p></div>
<div class="m2"><p>سر خوشان را تازه شد جوش و خروش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناصحا چند از نصیحت بعد از این</p></div>
<div class="m2"><p>دم ز وصف گلرخان زن یا خموش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر به سر پند خیالی گوهر است</p></div>
<div class="m2"><p>لیک آن بدخو نمی‌گیرد به گوش</p></div></div>