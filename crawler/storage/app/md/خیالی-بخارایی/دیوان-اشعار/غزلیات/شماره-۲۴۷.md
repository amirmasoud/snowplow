---
title: >-
    شمارهٔ ۲۴۷
---
# شمارهٔ ۲۴۷

<div class="b" id="bn1"><div class="m1"><p>هردم از جانب او تیغ بلا می‌آید</p></div>
<div class="m2"><p>چه بلاهاست کز او بر سرِ ما می‌آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست خالی ز هوایِ تو غم‌آبادِ دلم</p></div>
<div class="m2"><p>خانه‌ای را که تو سازی به هوا می‌آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صدف گوهر وصل تو نه تنها دل ماست</p></div>
<div class="m2"><p>فیض باران عطایت همه جا می‌آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یارب ارباب حقیقت چه سخن‌ها رانند</p></div>
<div class="m2"><p>کز زبان همه پیغام خدا می‌آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوش کن نغمهٔ بلبل ز من ای گل نفسی</p></div>
<div class="m2"><p>تا ببینی که چه گلبانگ دعا می‌آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زاهدا همچو خیالی دم یکرنگی زن</p></div>
<div class="m2"><p>کز گِل طاعت تو بوی ریا می‌آید</p></div></div>