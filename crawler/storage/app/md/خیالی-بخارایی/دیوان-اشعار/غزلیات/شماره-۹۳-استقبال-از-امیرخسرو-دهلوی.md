---
title: >-
    شمارهٔ ۹۳ - استقبال از امیرخسرو دهلوی
---
# شمارهٔ ۹۳ - استقبال از امیرخسرو دهلوی

<div class="b" id="bn1"><div class="m1"><p>یار جز در پیِ آزار دل ریش نرفت</p></div>
<div class="m2"><p>چه جفاها که از او بر منِ درویش نرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای ننهاده به راه غم او سر بنهاد</p></div>
<div class="m2"><p>اشک با آنکه در این ره به سر خویش نرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل می گفت مرو در پی دلدار ولی</p></div>
<div class="m2"><p>دل نکو کرد که بر قول بداندیش نرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آمد و رفت بسی راست بر این در لیکن</p></div>
<div class="m2"><p>هرکه اندک خبری یافت از او بیش نرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا رود پیش سگش ذکر خیالی روزی</p></div>
<div class="m2"><p>سعی بسیار نمودیم ولی پیش نرفت</p></div></div>