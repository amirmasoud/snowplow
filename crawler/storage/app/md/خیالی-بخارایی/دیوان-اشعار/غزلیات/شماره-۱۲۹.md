---
title: >-
    شمارهٔ ۱۲۹
---
# شمارهٔ ۱۲۹

<div class="b" id="bn1"><div class="m1"><p>تا به رحمت خوان قسمت را مزیّن کرده‌اند</p></div>
<div class="m2"><p>درخور هر فرقه مرسومی معیّن کرده‌اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نام نیک و نقد هستی را به زاهد داده‌اند</p></div>
<div class="m2"><p>نیستیّ و عشق را در گردن من کرده‌اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با تو دعویِّ نظر بازی کسانی را رسد</p></div>
<div class="m2"><p>کز غبار خاک کویت دیده روشن کرده‌اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر گران ز آن است چشم مست یار و جام می</p></div>
<div class="m2"><p>کاندر این ره هر یکی خونی به گردن کرده‌اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خیالی دامن جان چاک زن کارباب دل</p></div>
<div class="m2"><p>سرخ‌رویی‌ها چو گُل از چاک دامن کرده‌اند</p></div></div>