---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>دل در آن زلف شد و روی دل افروز ندید</p></div>
<div class="m2"><p>وه که هیچ از سر زلف تو کسی روز ندید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت دل گرم نشد تا ز غم عشق نسوخت</p></div>
<div class="m2"><p>عود خوشبوی نشد تا الم سوز ندید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خویش را بر صف عشاق زدن کام دل است</p></div>
<div class="m2"><p>عقل را عشق در این معرکه فیروز ندید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده استاد تر از چشم تو در دور قمر</p></div>
<div class="m2"><p>هندوی عربده جوی ستم آموز ندید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خیالی طرف غمزهٔ شوخ تو گرفت</p></div>
<div class="m2"><p>چه جفاها که از آن ناوک دلدوز ندید</p></div></div>