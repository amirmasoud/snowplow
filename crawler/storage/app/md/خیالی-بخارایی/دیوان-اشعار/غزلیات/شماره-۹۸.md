---
title: >-
    شمارهٔ ۹۸
---
# شمارهٔ ۹۸

<div class="b" id="bn1"><div class="m1"><p>آنها که ز آیینهٔ دل زنگ زدودند</p></div>
<div class="m2"><p>خود را به تو هر نوع که بودند نمودند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اهل نظر از آینهٔ وحدت از آن پیش</p></div>
<div class="m2"><p>حیران تو بودند که موجود نبودند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا چشم دل از غیر تماشای تو عشّاق</p></div>
<div class="m2"><p>بستند، نقاب از رخِ مقصود گشودند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شک نیست که سودازدگان تا به ارادت</p></div>
<div class="m2"><p>سودند سری بر قدمت در سر سودند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا دل بربایند نمودند رخ خوب</p></div>
<div class="m2"><p>خوبان به طریقی که نمودند ربودند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>راهی به عدم جوی خیالی ز دهانش</p></div>
<div class="m2"><p>بر رغم کسانی که گرفتار وجودند</p></div></div>