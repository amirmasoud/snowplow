---
title: >-
    شمارهٔ ۱۴۷
---
# شمارهٔ ۱۴۷

<div class="b" id="bn1"><div class="m1"><p>تا کافر چشمت ز مژه عزم سپه کرد</p></div>
<div class="m2"><p>بر خون دلم غمزهٔ تو چشم سیه کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این دل که کمین داشت بدآن چشم تو عمری</p></div>
<div class="m2"><p>خم زد به فن ابروی تو تا چشم نگه کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مسکین دلم ار خطّ تو را مشگ ختا گفت</p></div>
<div class="m2"><p>دیوانه وشی بود خطا گفت و تبه کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>این نکته نشد روشنم از ماه که آخر</p></div>
<div class="m2"><p>چندین که به رخسار تو زد لاف چه مه کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از راه وفایت به جفا روی نتابم</p></div>
<div class="m2"><p>زاین مرتبه چون عشق توام روی به ره کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآن گونه تو را قصد خیالی ست که گویی</p></div>
<div class="m2"><p>اظهار هواداریِ تو کرد گنه کرد</p></div></div>