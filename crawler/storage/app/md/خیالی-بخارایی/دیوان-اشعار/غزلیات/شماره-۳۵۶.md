---
title: >-
    شمارهٔ ۳۵۶
---
# شمارهٔ ۳۵۶

<div class="b" id="bn1"><div class="m1"><p>تا کی ای شوخ به هر بی‌خبری می‌سازی</p></div>
<div class="m2"><p>خاک کوی تو منم گر گذری می‌سازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>این هم از آتش سودای تو داغ دگر است</p></div>
<div class="m2"><p>که مرا سوختی و با دگری می‌سازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روشن است این که مرا شمع‌صفت می‌سوزی</p></div>
<div class="m2"><p>تو به هرکس که قضا را قدری می‌سازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زلف از چهره برافکندی و معلومم شد</p></div>
<div class="m2"><p>که شب تیره‌دلان را سحری می‌سازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفته‌ای بنده خیالی هم از اهل نظر است</p></div>
<div class="m2"><p>بندهٔ مخلص خود را نظری می‌سازی</p></div></div>