---
title: >-
    شمارهٔ ۳۵۷
---
# شمارهٔ ۳۵۷

<div class="b" id="bn1"><div class="m1"><p>تا همچو غنچه خندان از خود به در نیایی</p></div>
<div class="m2"><p>گر گل شوی کسی را هم در نظر نیایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ره به خود ندانی تدبیر بیخودی کن</p></div>
<div class="m2"><p>بی خویش تا نگردی با خویش برنیایی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل به کوی وحدت چون غیر می نگنجد</p></div>
<div class="m2"><p>تا ترک خود نگویی با خوددگر نیایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هرکس که بی ارادت آید به کوی جانان</p></div>
<div class="m2"><p>پوشند در به رویش یعنی که در نیایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون یار خامه وارت می خواند ای خیالی</p></div>
<div class="m2"><p>شرط ادب نباشد گر تو به سر نیایی</p></div></div>