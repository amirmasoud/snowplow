---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>تا دو چشم سیهت غارتِ جان کرد مرا</p></div>
<div class="m2"><p>غم پنهان تو رسوای جهان کرد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بارها عشق تو می گفت که رسوا کُنمت</p></div>
<div class="m2"><p>هرچه می گفت غم عشق همان کرد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این نشان بس ز وفا ترک کماندار تو را</p></div>
<div class="m2"><p>که چو تیری به کف آورد نشان کرد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم ای اشک مرو هر طرفی گفت برو</p></div>
<div class="m2"><p>کآنکه پرورد بدین گونه روان کرد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شادکام از روش نظم خیالی ز آنم</p></div>
<div class="m2"><p>که خیال سخنش ورد زبان کرد مرا</p></div></div>