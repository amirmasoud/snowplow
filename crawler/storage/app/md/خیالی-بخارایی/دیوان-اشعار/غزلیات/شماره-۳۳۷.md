---
title: >-
    شمارهٔ ۳۳۷
---
# شمارهٔ ۳۳۷

<div class="b" id="bn1"><div class="m1"><p>نامهٔ طاعت و عصیان چه سفید و چه سیاه</p></div>
<div class="m2"><p>سرنوشت ازل این بود کسی را چه گناه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر نباشد نظر لطف بود کاه چو کوه</p></div>
<div class="m2"><p>ور بود جذبهٔ توفیق شود کوه چو کاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تاجداران جهان پیرو فرمان تواند</p></div>
<div class="m2"><p>زآنکه این قوم سپاهند و تویی میر سپاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا تو بر راه ارادت ننهی روی نیاز</p></div>
<div class="m2"><p>نشوی از طرف اهل صفا روی به راه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در طواف حرم وصل خیالی بشتاب</p></div>
<div class="m2"><p>که دراز است ره بادیه عمرت کوتاه</p></div></div>