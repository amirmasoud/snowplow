---
title: >-
    شمارهٔ ۳۳۳
---
# شمارهٔ ۳۳۳

<div class="b" id="bn1"><div class="m1"><p>من آن نیَم که گذارم ز دست دامن تو</p></div>
<div class="m2"><p>تو گر ز طوق وفا سرکشی به گردن تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گذار تا به کف آریم دامن زلفت</p></div>
<div class="m2"><p>وگرنه روز جزا دست ما و دامن تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو شمعِ صبح دم از نور زن که بس باشد</p></div>
<div class="m2"><p>صفای چهره دلیل ضمیر روشن تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبی که مجلس خلوت صفا دهی، مه را</p></div>
<div class="m2"><p>نمی رسد که سر اندر زند به روزن تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مقام قرب همین بس بود خیالی را</p></div>
<div class="m2"><p>که خانهٔ دل مسکین اوست مسکن تو</p></div></div>