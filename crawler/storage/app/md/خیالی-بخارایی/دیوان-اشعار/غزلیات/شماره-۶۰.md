---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>دل وصل تو می خواهد و دلخواست همین است</p></div>
<div class="m2"><p>چیزی که مرا از تو تمنّاست همین است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه گه گذرد سرو قدت بر گذر چشم</p></div>
<div class="m2"><p>میلی که قدت را طرف ماست همین است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما از دو جهان چشم به رخسار تو داریم</p></div>
<div class="m2"><p>کآن قبله که منظور نظرهاست همین است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که قدت سروِ روان است تو از ناز</p></div>
<div class="m2"><p>سر می کشی امّا سخن راست همین است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیین وفا از تو خیالی نه کنون خواست</p></div>
<div class="m2"><p>عمری ست که ما را ز تو درخواست همین است</p></div></div>