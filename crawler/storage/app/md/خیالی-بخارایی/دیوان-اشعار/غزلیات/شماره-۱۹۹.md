---
title: >-
    شمارهٔ ۱۹۹
---
# شمارهٔ ۱۹۹

<div class="b" id="bn1"><div class="m1"><p>عاقبت حسرتِ لعل تو دلم را خون کرد</p></div>
<div class="m2"><p>داغ سودای مرا فکر خطت افزون کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده را از قبل اشک هر آن راز که بود</p></div>
<div class="m2"><p>به خیالت همه را دوش ز دل بیرون کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حلقه یی از شکن سلسلهٔ موی تو بود</p></div>
<div class="m2"><p>زلف لیلی که به هر شیوه دلی مجنون کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دل بشد در طلب وصل و نشد معلومم</p></div>
<div class="m2"><p>که چسان رفت در این راه و به آخر چون کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنگ در پردهٔ عشّاق زد و راست نشد</p></div>
<div class="m2"><p>عود سازی که در این دایره بی قانون کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هیچ کس خرده بر اشعار خیالی نگرفت</p></div>
<div class="m2"><p>تا به وصف قد تو طبع خرد موزون کرد</p></div></div>