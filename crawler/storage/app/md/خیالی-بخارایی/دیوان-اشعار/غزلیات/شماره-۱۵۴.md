---
title: >-
    شمارهٔ ۱۵۴
---
# شمارهٔ ۱۵۴

<div class="b" id="bn1"><div class="m1"><p>چشمت آزار ما چه می‌خواهد</p></div>
<div class="m2"><p>از دل مبتلا چه می‌خواهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من چو وصل تو خواستم به دعا</p></div>
<div class="m2"><p>شیخ شهر از دعا چه می‌خواهد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بوسه‌ای زکات حُسن مرا</p></div>
<div class="m2"><p>بده از تو گدا چه می‌خواهد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون غمت هرچه خواست کرد به جان</p></div>
<div class="m2"><p>دگر از جان ما چه می‌خواهد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار خواهد همیشه خاطر من</p></div>
<div class="m2"><p>خاطر یار تا چه می‌خواهد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای خیالی مخواه چاره ز غیر</p></div>
<div class="m2"><p>صبر کن تا خدا چه می‌خواهد</p></div></div>