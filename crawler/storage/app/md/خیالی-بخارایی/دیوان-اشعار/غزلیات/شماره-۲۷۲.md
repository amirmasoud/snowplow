---
title: >-
    شمارهٔ ۲۷۲
---
# شمارهٔ ۲۷۲

<div class="b" id="bn1"><div class="m1"><p>چه کرد سرو به قدّت که بر کشیدندش</p></div>
<div class="m2"><p>چه گفت شمع به رویت که سر بریدندش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیده دوش چه دیدند سایلان سرشک</p></div>
<div class="m2"><p>که یک به یک همه بر روی می دویدندش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنفشه سبزهٔ خطّ تو را پریشان گفت</p></div>
<div class="m2"><p>بدین گناه زبان از قفا کشیدندش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ تو برد دلم را و مردمان دیدند</p></div>
<div class="m2"><p>به مردمی که در این حال خوب دیدندش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرفته بود دو چشم تو بر خیالی راه</p></div>
<div class="m2"><p>که گیسوان تو نیز از قفا رسیدندش</p></div></div>