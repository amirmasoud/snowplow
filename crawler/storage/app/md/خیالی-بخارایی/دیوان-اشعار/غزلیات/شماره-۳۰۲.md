---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>ما در خیال زلف تو شبگیر می کنیم</p></div>
<div class="m2"><p>دیوانه ییم و پای به زنجیر می کنیم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو از کمال لطف بیارا قصور ما</p></div>
<div class="m2"><p>هرچند ما به کار تو تقصیر می کنیم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با می حقوق صحبت ما این زمانه نیست</p></div>
<div class="m2"><p>عمری ست تا که خدمت این پیر می کنیم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ره برده ایم جانب ملک قلندری</p></div>
<div class="m2"><p>تا پیرویّ شحنهٔ تقدیر می کنیم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر یار چاره یی نکند درد عشق را</p></div>
<div class="m2"><p>با محنت فراق چه تدبیر می کنیم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اقرار می کنیم خیالی به جرم خویش</p></div>
<div class="m2"><p>نه دعوی صلاح و نه تزویر می کنیم</p></div></div>