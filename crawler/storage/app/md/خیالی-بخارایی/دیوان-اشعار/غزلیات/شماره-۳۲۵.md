---
title: >-
    شمارهٔ ۳۲۵
---
# شمارهٔ ۳۲۵

<div class="b" id="bn1"><div class="m1"><p>تا قدح هردم چرا بوسد لب میگون او</p></div>
<div class="m2"><p>زاین حسد عمری ست تا من تشنه‌ام بر خون او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطربا چون عود سر تا پای خود در چنگ غم</p></div>
<div class="m2"><p>تا نمی سوزد نمی داند کسی قانون او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینک اینک عاشقانِ مست تو، لیلی کجاست</p></div>
<div class="m2"><p>تا ز سر دیوانگی آموختی مجنون او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>افعیِ زلفت که در عاشق‌کشی افسانه‌ای‌ست</p></div>
<div class="m2"><p>آمدی در دست اگر دانستمی افسون او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خیالی کاش از این دلسوز تر بودی غمت</p></div>
<div class="m2"><p>تا زمانی شادمان بودی دل محزون او</p></div></div>