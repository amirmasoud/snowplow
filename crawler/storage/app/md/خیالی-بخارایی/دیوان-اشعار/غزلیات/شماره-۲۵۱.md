---
title: >-
    شمارهٔ ۲۵۱
---
# شمارهٔ ۲۵۱

<div class="b" id="bn1"><div class="m1"><p>هرکجا خطّ تو عرض نافهٔ چینی کند</p></div>
<div class="m2"><p>مشگ از چین آید و پیش تو مسکینی کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چوب ها باید زدن بر سر نبات مصر را</p></div>
<div class="m2"><p>بعد از این گر با لبت دعویّ شیرینی کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا لبت را دید جان من ز غم بر لب رسید</p></div>
<div class="m2"><p>این بوَد انجام کارِ آنکه خودبینی کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با مسلمانان دو چشمت آنچه کرد از دوستی</p></div>
<div class="m2"><p>کافرم با هیچ کس گر دشمن دینی کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با خیال لعل شیرینت خیالی هرکجا</p></div>
<div class="m2"><p>لب گشاید طوطی معنی شکر چینی کند</p></div></div>