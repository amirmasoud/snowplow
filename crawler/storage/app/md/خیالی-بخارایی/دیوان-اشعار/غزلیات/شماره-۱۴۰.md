---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>تا دلم شیوهٔ آن زلف دوتا می‌داند</p></div>
<div class="m2"><p>صفت نافهٔ چین فکر خطا می‌داند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با من آن غمزهٔ پُرفتنه چه‌ها کرد و هنوز</p></div>
<div class="m2"><p>در فن خویش چه گویم که چه‌ها می‌داند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف مشکینِ تو با آن که پریشان‌حال است</p></div>
<div class="m2"><p>مو به مو حال پریشانی ما می‌داند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حالیا سوختهٔ آتش هجریم و هنوز</p></div>
<div class="m2"><p>چه شود عاقبت کار خدا می‌داند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بازم از غصه درون خون شد و زین بیرون نیست</p></div>
<div class="m2"><p>که نمی‌داند از آن لعل تو یا می‌داند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بادپیمای خیالی به هوای قد توست</p></div>
<div class="m2"><p>تو گر آگه نیی ای سرو صبا می‌داند</p></div></div>