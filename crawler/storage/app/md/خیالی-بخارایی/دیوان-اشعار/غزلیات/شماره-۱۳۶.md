---
title: >-
    شمارهٔ ۱۳۶
---
# شمارهٔ ۱۳۶

<div class="b" id="bn1"><div class="m1"><p>تا جفایی نکشد دل به وفایی نرسد</p></div>
<div class="m2"><p>ورنه بی درد در این ره به دوایی نرسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نالهٔ هر که چو بلبل نه به سودای گلی ست</p></div>
<div class="m2"><p>عاقبت زین چمنش برگ و نوایی نرسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل ار سعی تو این است که من می بینم</p></div>
<div class="m2"><p>جای آن است که کار تو به جایی نرسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پای بوس تو طمع داشت دلم، عقلم گفت</p></div>
<div class="m2"><p>رو که این پایه به هر بی سروپایی نرسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بیش مخرام که از چشم بدان می ترسم</p></div>
<div class="m2"><p>تا به بالای بلند تو بلایی نرسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر وصالت به خیالی نرسد نیست عجب</p></div>
<div class="m2"><p>هیچگه منصب شاهی به گدایی نرسد</p></div></div>