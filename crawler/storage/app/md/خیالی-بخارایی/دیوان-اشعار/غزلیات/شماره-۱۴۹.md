---
title: >-
    شمارهٔ ۱۴۹
---
# شمارهٔ ۱۴۹

<div class="b" id="bn1"><div class="m1"><p>تا گلشن از طراوت روی تو یاد داد</p></div>
<div class="m2"><p>سرو از هوای قامت تو سر به باد داد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلتنگ بود غنچه به صد رو چو من ولی</p></div>
<div class="m2"><p>پایش صبا گرفت و خدایش گشاد داد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با گل نداد حسن رخت نقشبند صنع</p></div>
<div class="m2"><p>پیرایه یی ست حسن که با هر که داد داد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اسباب نامرادیِ جاوید بود و غم</p></div>
<div class="m2"><p>عشق تو تحفه یی که بدین نامراد داد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با اهل درد عشق تو تقسیم شوق کرد</p></div>
<div class="m2"><p>چیزی زِ یادِ تو به خیالی زیاد داد</p></div></div>