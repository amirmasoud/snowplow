---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>با رخ خوبت که وَرد بوستان خرّمی ست</p></div>
<div class="m2"><p>حور اگر دعویّ رعنایی کند ناآدمی ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بخت بد بنگر که می پوشد ز من راز تو دل</p></div>
<div class="m2"><p>در میان ما و او با آنکه چندین محرمی ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل نشاید بست بر عهد بتان بی وفا</p></div>
<div class="m2"><p>کاین بنا را از ازل بنیاد برنا محکمی ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا جداییم از رخ چون روز و زلف چون شبت</p></div>
<div class="m2"><p>روز با دردم قرار و شب به ناله همدمی ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بریزد چشم تو خون خیالی باک نیست</p></div>
<div class="m2"><p>هرچه با مردم کند آن شوخ عین مردمی ست</p></div></div>