---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>مسافران که در این ره به کاروان رفتند</p></div>
<div class="m2"><p>عجب مدار که از فتنه در امان رفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا چو جان و جهان فانی اند اهل نظر</p></div>
<div class="m2"><p>به ترک جان بگرفتند و از جهان رفتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از این منازل فانی به عزم شهر بقا</p></div>
<div class="m2"><p>قدم به راه نهادند و هم عنان رفتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از آن ز غصّه دلِ غنچه تو به تو خون است</p></div>
<div class="m2"><p>که بلبلان خوش الحان ز بوستان رفتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیالیا چو رهِ رفتنی ست خیره مباش</p></div>
<div class="m2"><p>تو نیز سازِ سفر کن که همرهان رفتند</p></div></div>