---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>آن معلم که لبت را روش جان آموخت</p></div>
<div class="m2"><p>هرچه آموخت به زلف تو پریشان آموخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ظاهراً بر ورق گل به خط سبز خرد</p></div>
<div class="m2"><p>آیت حسن نه از یار که قرآن آموخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داد از آن شوخ که در مکتب خوبی ز ادب</p></div>
<div class="m2"><p>رحمتِ اندک و بیدادِ فراوان آموخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شیوهٔ فتنه ز خوبان جفا پیشه بپرس</p></div>
<div class="m2"><p>که ادیب ازل این حرف به ایشان آموخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آیت دلبری و شیوهٔ جان بخشی را</p></div>
<div class="m2"><p>جز به تعلیم خط و لعل تو نتوان آموخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز در خون خیالی شدی و می دانم</p></div>
<div class="m2"><p>کاین همه فتنه ترا نرگس فتّان آموخت</p></div></div>