---
title: >-
    شمارهٔ ۷۲ - استقبال از همام تبریزی
---
# شمارهٔ ۷۲ - استقبال از همام تبریزی

<div class="b" id="bn1"><div class="m1"><p>که می‌داند میِ شوق از چه جام است</p></div>
<div class="m2"><p>به جز چشمت که او مست مدام است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شراب ار با تو نو شد دل حلال است</p></div>
<div class="m2"><p>وگرنه این صفت بر وی حرام است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلا بگذر ز خود کاندر ره عشق</p></div>
<div class="m2"><p>نخستین گفته ترک ننگ و نام است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بجز سودای ابروی تو دیگر</p></div>
<div class="m2"><p>همه کارِ مه نو ناتمام است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سر زلف تو را مرغی که داند</p></div>
<div class="m2"><p>کدام است و به در ماند که دام است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیالی گر چو شمعی ز آتش دل</p></div>
<div class="m2"><p>نسوزی خویشتن را کار خام است</p></div></div>