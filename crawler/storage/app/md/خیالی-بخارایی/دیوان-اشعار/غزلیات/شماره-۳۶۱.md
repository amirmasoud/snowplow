---
title: >-
    شمارهٔ ۳۶۱
---
# شمارهٔ ۳۶۱

<div class="b" id="bn1"><div class="m1"><p>خیز ای مست و سلامی به رخ ساقی گوی</p></div>
<div class="m2"><p>باقیِ باده به پیش آر و هوالباقی گوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مطربا مجلس شوق است و حریفان جمعند</p></div>
<div class="m2"><p>ماجرای غم و افسانهٔ مشتاقی گوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزه اش گر سخن از فتنه نگفت، ای ابرو</p></div>
<div class="m2"><p>تو که در شیوهٔ خوبی به جهان طاقی گوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای طبیب دل رندان چو غمِ رنج خمار</p></div>
<div class="m2"><p>درد نوشان به تو گفتند تو با ساقی گوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سرکشی کار بتان است خیالی در عشق</p></div>
<div class="m2"><p>گر تو رندی سخن از رندی و عشاقی گوی</p></div></div>