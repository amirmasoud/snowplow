---
title: >-
    شمارهٔ ۱۹۶
---
# شمارهٔ ۱۹۶

<div class="b" id="bn1"><div class="m1"><p>صبا به تحفه نسیمی که دلگشای آرد</p></div>
<div class="m2"><p>شمامه‌ای ست که ز آن جعد عطرسای آرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر نه در پس زانوی محنت آرد سر</p></div>
<div class="m2"><p>چگونه پیش برد دل به هرچه رای آرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل از لب تو به شکر است و آنچنان مجنون</p></div>
<div class="m2"><p>که چشم داشت که حقّ نمک به جای آرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>وداع کز سر کوی تو درد سر بردیم</p></div>
<div class="m2"><p>رسیم باز به خدمت اگر خدای آرد</p></div></div>