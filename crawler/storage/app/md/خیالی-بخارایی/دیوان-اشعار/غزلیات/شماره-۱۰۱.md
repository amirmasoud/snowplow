---
title: >-
    شمارهٔ ۱۰۱
---
# شمارهٔ ۱۰۱

<div class="b" id="bn1"><div class="m1"><p>از مخزن دل دیده هر آن دُر که بر آورد</p></div>
<div class="m2"><p>چون مردمیی داشت روان در نظر آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>المنّة لله که صبا گرچه دلم برد</p></div>
<div class="m2"><p>بر بوی توام آمد و از جان خبر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کس نیست که آرد ز توام شربت دردی</p></div>
<div class="m2"><p>جز غصّه که خون دل و داغ جگر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نابرده هنوز از دل من بار فراقت</p></div>
<div class="m2"><p>بار دگر آمد غم و بار دگر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر بوی تو هرجا که شدم رایحهٔ مشگ</p></div>
<div class="m2"><p>پی برد من شیفته را درد سر آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از حال پریشان خیالی خبری برد</p></div>
<div class="m2"><p>ز آن طرّه پیامی که نسیم سحر آورد</p></div></div>