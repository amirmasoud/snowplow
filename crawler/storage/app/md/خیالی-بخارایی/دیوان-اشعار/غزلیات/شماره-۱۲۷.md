---
title: >-
    شمارهٔ ۱۲۷
---
# شمارهٔ ۱۲۷

<div class="b" id="bn1"><div class="m1"><p>تا بر بیاض رویت خطّ سیه برآمد</p></div>
<div class="m2"><p>از نامهٔ محبّان نام گنه برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو طرّهٔ را مبُر سر اکنون که رخ نمودی</p></div>
<div class="m2"><p>فکر از درازی شب نبوَد چو مه برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زلف سیاهکارت بی جرم تا که را سوخت</p></div>
<div class="m2"><p>کز خان و مانش آخر دود سیه برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سر بر ره تو دارد پیوسته درّ اشکم</p></div>
<div class="m2"><p>ای دولت یتیمی کاو سر به ره برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر ناوکی که چشمت زد بر دل خیالی</p></div>
<div class="m2"><p>کاری فتاد یعنی بر کارگه برآمد</p></div></div>