---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>به جهان لطیف طبعی که ز خود ملال دارد</p></div>
<div class="m2"><p>ز غم رخش چه گویم که دلم چه حال دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدحی که جان زارم نه به یاد او بنوشد</p></div>
<div class="m2"><p>غم او حرام بادم دل اگر حلال دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به چمن که نسخه بُرد از دهن و رخش ندانم</p></div>
<div class="m2"><p>که درون غنچه خون است و گل انفعال دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنهی چو آید از سر بنهم بر آستانش</p></div>
<div class="m2"><p>به امید آنکه روزی دو سه پایمال دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه عجب اگر برم پی به حدایق میانش</p></div>
<div class="m2"><p>به معانی خیالی که همین خیال دارد</p></div></div>