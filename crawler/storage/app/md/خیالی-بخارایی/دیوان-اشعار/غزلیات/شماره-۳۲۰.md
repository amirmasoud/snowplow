---
title: >-
    شمارهٔ ۳۲۰
---
# شمارهٔ ۳۲۰

<div class="b" id="bn1"><div class="m1"><p>گر شود بر خاک کویت فرصت سر باختن</p></div>
<div class="m2"><p>خویش را خواهد سرشک اوّل به پیش انداختن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته ای چو نی ز غم کو هر شبی مهمان تست</p></div>
<div class="m2"><p>می خورم غم تا دمی با او توان پرداختن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در طریق شب نشینان عاشقی دانی که چیست</p></div>
<div class="m2"><p>ساختن چون شمع با سوز دل و پرداختن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در مقام خانهٔ رندیست کوی عاشقی</p></div>
<div class="m2"><p>پردلی ست ار می توانی خویش را در باختن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر چو جام می خیالی سرخ رویی بایدت</p></div>
<div class="m2"><p>با حریفان تو دل خود صاف باید ساختن</p></div></div>