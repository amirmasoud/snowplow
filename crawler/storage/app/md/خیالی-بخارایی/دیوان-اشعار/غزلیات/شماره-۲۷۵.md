---
title: >-
    شمارهٔ ۲۷۵
---
# شمارهٔ ۲۷۵

<div class="b" id="bn1"><div class="m1"><p>دلا به دست هوس تار زلف یار مکش</p></div>
<div class="m2"><p>در او مپیچ و بلا را به اختیار مکش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>که فتنه یی ست به هر تاب طرّهٔ او را</p></div>
<div class="m2"><p>چو تاب فتنه نداری تو زینهار مکش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سرکشیِ چو به قدّش نمی رسی ای</p></div>
<div class="m2"><p>به جای خویش نشین و به هرزه بار مکش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیار باده که جامت مدام می گوید</p></div>
<div class="m2"><p>که ساغری کش و دردسر خمار مکش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیال وصل خیالی بنه تمام از سر</p></div>
<div class="m2"><p>نمی رسد به تو این پایه انتظار مکش</p></div></div>