---
title: >-
    شمارهٔ ۲۸۲
---
# شمارهٔ ۲۸۲

<div class="b" id="bn1"><div class="m1"><p>یار اگر بد کند ای دل نتوان گفت بدش</p></div>
<div class="m2"><p>که به آن روی نکو هرچه کند می رسدش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا به اول نکنی فکر روانی ای سرو</p></div>
<div class="m2"><p>دعویِ خوبی تو راست نیاید به قدش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هرکه با تو به ازل لاف ز درویشی زد</p></div>
<div class="m2"><p>نبود میل به سلطانی ملک ابدش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می رود در پیِ زلف تو دل شیفته ام</p></div>
<div class="m2"><p>تا سرِ رشتهٔ تقدیر کجا می کشدش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>باد هر لحظه به عمدا نکشیدی زلفت</p></div>
<div class="m2"><p>گر سر زلف تو گستاخ نکردی به خودش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ابروی شوخ تو در بردن دل نیست</p></div>
<div class="m2"><p>گرچه پیوسته خیالیّ تو دل می‌دهدش</p></div></div>