---
title: >-
    شمارهٔ ۳۴۷
---
# شمارهٔ ۳۴۷

<div class="b" id="bn1"><div class="m1"><p>ای دل به طریقی سوی زلفش اگر افتی</p></div>
<div class="m2"><p>پرهیز از آن حلقه، مبادا که درافتی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زنهار چو من در قدمش سر نهم ای اشک</p></div>
<div class="m2"><p>تو نیز روان آیی و در پا به سر افتی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ارباب نظر را ز تماشای رخ دوست</p></div>
<div class="m2"><p>مانع تویی ای پرده خدایا که برافتی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بعد من و رهگذر کوی تو تا حشر</p></div>
<div class="m2"><p>باشد که به دام من از این رهگذر افتی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>زاین بیش منه پای به روی من و اندیش</p></div>
<div class="m2"><p>زآن دم که به ناگه چو سرشک از نظر افتی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زآن باده که جامش لب یار است خیالی</p></div>
<div class="m2"><p>ترسم که چو یابی خبری بیخبر افتی</p></div></div>