---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>مرا مکش که تو را خاک رهگذار شدم</p></div>
<div class="m2"><p>پی رضای تو رفتم گناهکار شدم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به اختیار غلامیّ حضرتت کردم</p></div>
<div class="m2"><p>که بر ولایت دل صاحب اختیار شدم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز بیم جرم پراکنده حال بودم لیک</p></div>
<div class="m2"><p>چو فیض لطف تو دیدم امیدوار شدم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به بازی سر زلفت دل پریشانم</p></div>
<div class="m2"><p>قرار بست و من از غصه بیقرار شدم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذار تا ز هوای تو دم زنم نفسی</p></div>
<div class="m2"><p>که از تصوّر خاک درت غبار شدم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرا به عهد جوانان ز عشق حاصل کار</p></div>
<div class="m2"><p>همین بس است خیالی که پیر کار شدم</p></div></div>