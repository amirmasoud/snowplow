---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>سرو تا بندهٔ بالای تو شد آزاد است</p></div>
<div class="m2"><p>هر نفس کآن نه به یادِ تو برآید باد است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لطف فرمای و بده دادِ اسیران امروز</p></div>
<div class="m2"><p>که تو را لطف خدا منصب شاهی داد است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غمزه چشم ستم آموز تو را شاگرد است</p></div>
<div class="m2"><p>در فن فتنه، ولی در فن خود استاد است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناله و آهِ مرا مرتبه بالاست ولی</p></div>
<div class="m2"><p>زین میان سیل سرشک است که پیش افتاد است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش یاد کن از عهد فرامش شدگان</p></div>
<div class="m2"><p>گفت خوش باش خیالی که مرا این یاد است</p></div></div>