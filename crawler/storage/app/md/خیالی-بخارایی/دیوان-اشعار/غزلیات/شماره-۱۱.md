---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>گر ز می رنگ نبودی گل سیرابش را</p></div>
<div class="m2"><p>شیوه مستی نشدی نرگس پر خوابش را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما چنین غرقه به خون از پیِ آنیم ز اشک</p></div>
<div class="m2"><p>که در اوّل نگرفتیم سرِ آبش را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه گرم از سبب آنکه مرا بی سببی</p></div>
<div class="m2"><p>سوخت، یارب تو نسازی دگر اسبابش را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>طاق ابرو بنما گوشه نشین را نفسی</p></div>
<div class="m2"><p>تا که درهم شکند گوشهٔ محرابش را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبا گر ز خیالی دلِ جمعت هوس است</p></div>
<div class="m2"><p>بر گُل آشفته مکن سنبل سیرابش را</p></div></div>