---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>دلم از زلف تو پا بستهٔ سودا آمد</p></div>
<div class="m2"><p>بی دُر وصل توام اشک به دریا آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گفته بودی که بپرهیز ز تیر نظرم</p></div>
<div class="m2"><p>چون نرفتیم پیِ گفت تو برما آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب را از نظر انداخت روان مردم چشم</p></div>
<div class="m2"><p>سوی او مژدهٔ خاک قدمت تا آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کلک نقّاش قدر چون صور حُسن کشید</p></div>
<div class="m2"><p>ز آن میان نقش دهان تو چه گویا آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خیالی گله از شیوهٔ آن چشم مکن</p></div>
<div class="m2"><p>این بلاها همه بر ما چو ز بالا آمد</p></div></div>