---
title: >-
    شمارهٔ ۱۱۰ - استقبال از کمال خجندی
---
# شمارهٔ ۱۱۰ - استقبال از کمال خجندی

<div class="b" id="bn1"><div class="m1"><p>ای لبت کام دل بی‌سروسامانی چند</p></div>
<div class="m2"><p>کاکلت حلقهٔ سودای پریشانی چند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوکب سعدی و منظور سبک روحانی</p></div>
<div class="m2"><p>قدر وصل تو چه دانند گران جانی چند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لذّت شربت دیدار نکو می داند</p></div>
<div class="m2"><p>هرکه گشته ست اسیر غم هجرانی چند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مردمی می کند آن غمزه و در عین بلاست</p></div>
<div class="m2"><p>کافر چشم تو برقصد مسلمانی چند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا خیالی به خیال تو سخن‌پرداز است</p></div>
<div class="m2"><p>می‌برد شعر تَرش آب سخندانی چند</p></div></div>