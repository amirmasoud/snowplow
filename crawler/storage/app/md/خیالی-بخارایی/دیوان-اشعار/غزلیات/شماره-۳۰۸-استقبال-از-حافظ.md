---
title: >-
    شمارهٔ ۳۰۸ - استقبال از حافظ
---
# شمارهٔ ۳۰۸ - استقبال از حافظ

<div class="b" id="bn1"><div class="m1"><p>من که با لعل تو فارغ ز می رنگینم</p></div>
<div class="m2"><p>خون دل می‌خورم و درخور صد چندینم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دورم از دولت دیدار تو و نزدیک است</p></div>
<div class="m2"><p>که ببینم رخ مقصود و چنین می‌بینم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زآن چو نافه خوشم از همدمی خون جگر</p></div>
<div class="m2"><p>که نسیمی‌ست ز زلفت نفس مشکینم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش از آن دم که گریبان درم از غصّه چو گل</p></div>
<div class="m2"><p>دامن آن به که ز خود غنچه صفت برچینم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گرم پرسیِّ توام سوخت چه باشد ای دوست</p></div>
<div class="m2"><p>که زمانی ببری دردسر از بالینم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر نباشد سبب اشعار خیالی که برد</p></div>
<div class="m2"><p>پیش آن خسرو خوبان سخن شیرینم</p></div></div>