---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>باز بالا بنمودی و بلا خواهد شد</p></div>
<div class="m2"><p>چشم بگشادی و مفتاح جفا خواهد شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هیچ کس نیست کز آن طرّه گشاید شکنی</p></div>
<div class="m2"><p>این گشاد از قدم باد صبا خواهد شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حالیا شیفته حالیم به سودای خطت</p></div>
<div class="m2"><p>بعد از این حال که داند که چه ها خواهد شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آب چشمی که محبّان همه این خاک شدند</p></div>
<div class="m2"><p>ز آن که تا چشم زنی نوبت ما خواهد شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش می گفت خیالی که کجا شد دل من</p></div>
<div class="m2"><p>دهنت گفت همین جاست کجا خواهد شد</p></div></div>