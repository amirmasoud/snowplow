---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>مرا که بر سر کویت سگ وفا دارم</p></div>
<div class="m2"><p>ز در مران که در این باب کارها دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بدان هوس که به سر وقت من رسی روزی</p></div>
<div class="m2"><p>ز پا فتاده ام و دست بر دعا دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو را که در غم هجران نبوده ای چه خبر</p></div>
<div class="m2"><p>که بر دل از غم هجران تو چه ها دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو باد خوشدل از آنم که هرکجا هستم</p></div>
<div class="m2"><p>تو را که سرو روان منی هوا دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر شوم چو خیالی ز خویش بیگانه</p></div>
<div class="m2"><p>روا بوَد چو خیال تو آشنا دارم</p></div></div>