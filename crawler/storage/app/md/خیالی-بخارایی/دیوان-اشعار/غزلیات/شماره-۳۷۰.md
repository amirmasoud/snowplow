---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>ما نداریم به غیر از جگرِ افگاری</p></div>
<div class="m2"><p>کو طبیبی که شود چاره گرِ بیماری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بار هجر تو گران است مرا بر دل ریش</p></div>
<div class="m2"><p>که بیابیم شبی بر در خدمت باری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل به سودای تو در باخته ام لیک چه سود</p></div>
<div class="m2"><p>که نکردی درم قلب مرا بازاری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا قدم در حرم کعبهٔ تجرید زدیم</p></div>
<div class="m2"><p>سخن دوست شنیدیم ز هر دیواری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در چنین حال که افتاد خیالی از پای</p></div>
<div class="m2"><p>مگر از دست قبول تو برآید کاری</p></div></div>