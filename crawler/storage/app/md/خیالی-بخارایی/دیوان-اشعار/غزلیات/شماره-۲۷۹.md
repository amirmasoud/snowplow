---
title: >-
    شمارهٔ ۲۷۹
---
# شمارهٔ ۲۷۹

<div class="b" id="bn1"><div class="m1"><p>گر ترحّم نکند طرّهٔ بی آرامش</p></div>
<div class="m2"><p>مرغ دل جان نتواند که برد از دامش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هرکه خواهد که به کامی رسد از نخل بتان</p></div>
<div class="m2"><p>صبر باید به جفایی که رسد تا کامش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل آغاز به کاری که کنی روز نخست</p></div>
<div class="m2"><p>سعی آن کن که ندامت نبری انجامش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با تو هرکس که دمی خوش گذراند، دیگر</p></div>
<div class="m2"><p>چه غم از محنت دهر و ستم ایّامش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا به نام تو خیالی قدحی درنکشد</p></div>
<div class="m2"><p>نرود در همه آفاق به رندی نامش</p></div></div>