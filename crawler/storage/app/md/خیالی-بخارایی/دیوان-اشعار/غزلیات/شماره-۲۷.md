---
title: >-
    شمارهٔ ۲۷
---
# شمارهٔ ۲۷

<div class="b" id="bn1"><div class="m1"><p>از زروه‌های باغِ خطش یاسمن یکی‌ست</p></div>
<div class="m2"><p>وز پرده‌های سبز قدش ناروَن یکی‌ست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یوسف‌رخان اگرچه هزارند هر طرف</p></div>
<div class="m2"><p>در ملک حسن یوسف گل پیرهن یکی‌ست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای دل مرو ز عشوهٔ شیرین‌لبان ز راه</p></div>
<div class="m2"><p>کز کشتگان کمترشان کوهکن یکی‌ست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>واقف بود ز نالهٔ جان‌سوز من سگت</p></div>
<div class="m2"><p>کاو هم بر آستان تو هرشب چو من یکی‌ست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا زآفتاب روی تو عالم نیافت نور</p></div>
<div class="m2"><p>روشن نشد که صاحب وجه حسن یکی‌ست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهی دهی نجات خیالی به هر طریق</p></div>
<div class="m2"><p>عشق است رهنمای بگفتم سخن یکی‌ست</p></div></div>