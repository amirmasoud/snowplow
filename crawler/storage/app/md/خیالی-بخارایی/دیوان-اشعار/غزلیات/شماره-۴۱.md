---
title: >-
    شمارهٔ ۴۱
---
# شمارهٔ ۴۱

<div class="b" id="bn1"><div class="m1"><p>بیا که بی خبران را خبر ز روی نکو نیست</p></div>
<div class="m2"><p>وگرنه چیست که عکسی ز نور طلعت او نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلا بسوز که بی سوز دل اگر به حقیقت</p></div>
<div class="m2"><p>شمامهٔ نفس مجمر است غالیه بو نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>طریق موی شکافی چه سود بی خبران را</p></div>
<div class="m2"><p>ز سرّ آن دهنش چون وقوف یک سر مو نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گذار دست سبو ساقیا و پای خمی گیر</p></div>
<div class="m2"><p>چرا که چارهٔ کار غمش به دست سبو نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر تو محرم رازی خموش باش خیالی</p></div>
<div class="m2"><p>که گویِ عشق به چوگان مرد بیهده گو نیست</p></div></div>