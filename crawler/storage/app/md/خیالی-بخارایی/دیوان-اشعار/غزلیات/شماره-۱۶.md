---
title: >-
    شمارهٔ ۱۶
---
# شمارهٔ ۱۶

<div class="b" id="bn1"><div class="m1"><p>عشق می‌گفت از کَرَم‌های حبیب</p></div>
<div class="m2"><p>غم نصیب تست گفتم یا نصیب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به داغ سینه‌سوز خود خوشیم</p></div>
<div class="m2"><p>تو مبر دردِ سرِ خویش ای طبیب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>غم نباشد هیچ از این دوری اگر</p></div>
<div class="m2"><p>وعدهٔ وصل تو باشد عنقریب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر بپرسی از غریبان دور نیست</p></div>
<div class="m2"><p>نبود از اصل کرم این‌ها غریب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با رخت دارد خیالی الفتی</p></div>
<div class="m2"><p>قدر گل نیکو شناسد عندلیب</p></div></div>