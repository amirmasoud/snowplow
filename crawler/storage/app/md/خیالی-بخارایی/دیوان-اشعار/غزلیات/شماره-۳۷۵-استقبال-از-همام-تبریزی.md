---
title: >-
    شمارهٔ ۳۷۵ - استقبال از همام تبریزی
---
# شمارهٔ ۳۷۵ - استقبال از همام تبریزی

<div class="b" id="bn1"><div class="m1"><p>منم و بادیهٔ عشق و دل آگاهی</p></div>
<div class="m2"><p>کس به جایی نرسد جز به چنین همراهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیش در خرمنم آتش مزن ای ماه و بترس</p></div>
<div class="m2"><p>که برآید ز منِ سوخته خرمن آهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سال‌ها شد که به سودای همین بیمارم</p></div>
<div class="m2"><p>که شوی رنجه به پرسیدن من گه گاهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای مه نو به جمالت که بدین خرسندم</p></div>
<div class="m2"><p>که ببینم به شبی روی تو در هر ماهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر بخواهد لب تو دل ز خیالی و تو نیز</p></div>
<div class="m2"><p>بر همین باش که به زاین نبود دلخواهی</p></div></div>