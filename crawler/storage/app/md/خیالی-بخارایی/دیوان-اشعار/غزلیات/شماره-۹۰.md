---
title: >-
    شمارهٔ ۹۰
---
# شمارهٔ ۹۰

<div class="b" id="bn1"><div class="m1"><p>هر دل که به عشق مبتلا نیست</p></div>
<div class="m2"><p>واقف ز شکیب حال ما نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از فتنهٔ عشق سر کشیدن</p></div>
<div class="m2"><p>در مذهب عاشقان روا نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رسم و ره یار بی وفایی</p></div>
<div class="m2"><p>ز آن است که عمر را وفا نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماییم و نیاز اگرچه آن نیز</p></div>
<div class="m2"><p>در حدّ قبول هست یا نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>این است خیالیا که باری</p></div>
<div class="m2"><p>در طاعت بی دلان ریا نیست</p></div></div>