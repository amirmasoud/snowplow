---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>مرا از دل خبر جز بی دلی نیست</p></div>
<div class="m2"><p>ز جان حاصل به جز بی حاصلی نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو غنچه تنگدل زآنم همه عمر</p></div>
<div class="m2"><p>که باغ دهر جای خوشدلی نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به قول بی دلان در مذهب عشق</p></div>
<div class="m2"><p>طریقی خوشتر از لایعقلی نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به زنجیر سر زلف تو دل را</p></div>
<div class="m2"><p>زدن لاف جنون از عاقلی نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خیالی را به اقبال غلامی</p></div>
<div class="m2"><p>نشانی به ز داغ مقبلی نیست</p></div></div>