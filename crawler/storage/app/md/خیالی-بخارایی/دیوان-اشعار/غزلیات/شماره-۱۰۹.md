---
title: >-
    شمارهٔ ۱۰۹
---
# شمارهٔ ۱۰۹

<div class="b" id="bn1"><div class="m1"><p>ای دل از باطن آن فرقه که صاحب قدمند</p></div>
<div class="m2"><p>همّتی خواه که این طایفه اهل کرمند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آبروی ابد از اشک ندامت بطلب</p></div>
<div class="m2"><p>که شهانند کسانی که ندیم ندمند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با غمت یاری جان و دلم امروزی نیست</p></div>
<div class="m2"><p>به تمنّای تو عمری ست که ایشان به همند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به هوای دهن تنگ تو اصحاب وجود</p></div>
<div class="m2"><p>سالکانند که سر گشتهٔ راه عدمند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای خیالی چه غم از رنج بیابان فراق</p></div>
<div class="m2"><p>محرمان درِ او را که مقیم حرمند</p></div></div>