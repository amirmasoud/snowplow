---
title: >-
    شمارهٔ ۳۱۵
---
# شمارهٔ ۳۱۵

<div class="b" id="bn1"><div class="m1"><p>تا دست دهد روی چو خورشید تو دیدن</p></div>
<div class="m2"><p>بر ماست دعا گفتن و از صبح دمیدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گل گوش همه بر سخن حسن تو دارد</p></div>
<div class="m2"><p>بد نیست ز خوبان سخن خوب شنیدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتی که مکش زلف مرا کآن سر فتنه ست</p></div>
<div class="m2"><p>هر فتنه که آید ز تو خواهیم کشیدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آن روی به سر می رود اندر طلبت اشک</p></div>
<div class="m2"><p>کش آبله شد پای ز بسیار دویدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای اشک چو در دیده وطن کرد خیالش</p></div>
<div class="m2"><p>می باید از این مرحله بر آب چکیدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زنهار به جایی که رخ اوست خیالی</p></div>
<div class="m2"><p>در ماه نبینی که نکو نیست دو دیدن</p></div></div>