---
title: >-
    شمارهٔ ۳۶۵
---
# شمارهٔ ۳۶۵

<div class="b" id="bn1"><div class="m1"><p>شد باز به دیدار سحر چشم جهانی</p></div>
<div class="m2"><p>بیدار نشد بخت عجب خواب گرانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را خبر از محنت و اندوه زمانه</p></div>
<div class="m2"><p>وقت است که بی یاد تو باشیم زمانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شک نیست که طفل ره ارباب طریق است</p></div>
<div class="m2"><p>پیری که نگفته است ارادت به جوانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا دیده چه نامحرمیی دید ز اشکم</p></div>
<div class="m2"><p>کز خانهٔ مردم به درش کرد روانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کس را نبود مایهٔ قدر تو خیالی</p></div>
<div class="m2"><p>گر یار بگوید که سگ ماست فلانی</p></div></div>