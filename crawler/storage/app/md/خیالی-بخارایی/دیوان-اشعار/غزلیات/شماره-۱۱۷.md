---
title: >-
    شمارهٔ ۱۱۷
---
# شمارهٔ ۱۱۷

<div class="b" id="bn1"><div class="m1"><p>باز این دل خود کام به فرمان کسی شد</p></div>
<div class="m2"><p>شهباز جهانگرد اسیر قفسی شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از سر هوس روی نکو کم شده بودم</p></div>
<div class="m2"><p>ناگاه رخت دیدم و باز هوسی شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با ناله خوشم چون نی از این وجه که باری</p></div>
<div class="m2"><p>دیر است منِ سوخته را هم نفسی شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آرامگه خال سیه شد لب لعلت</p></div>
<div class="m2"><p>آری شکری بود به کام مگسی شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گویند کز این پیش سگی بود خیالی</p></div>
<div class="m2"><p>سگ بود ولی در قدم یار کسی شد</p></div></div>