---
title: >-
    شمارهٔ ۷۷
---
# شمارهٔ ۷۷

<div class="b" id="bn1"><div class="m1"><p>گرچه طریق وفا قدیم است</p></div>
<div class="m2"><p>علم نداری تو حق علیم است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با تو دل ما یکی ست لیکن</p></div>
<div class="m2"><p>آنهم به تیغ جفا دو نیم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر به ادب درّ گوش نگیرد</p></div>
<div class="m2"><p>پیش حدیث تو نایتیم است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنکه ز زلف تو گاه گاهی</p></div>
<div class="m2"><p>جان به نسیمی دهد چو نسیم است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر تو ز راه کرم نبخشی</p></div>
<div class="m2"><p>کام خیالی، خدای کریم است</p></div></div>