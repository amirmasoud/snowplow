---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>از چشم ما چو می طلبد لعل او گهر</p></div>
<div class="m2"><p>نامردمی بوَد که نیاریم در نظر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون دُر خبر ز رستهٔ دندان یار گفت</p></div>
<div class="m2"><p>معلوم می شود که یتیمی ست باخبر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی چو روز عمر تو را تابدید شمع</p></div>
<div class="m2"><p>هر شب ز رشک می رودش آتشی به سر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم فدای چشم تو رخسار زرد من</p></div>
<div class="m2"><p>خندید و گفت چند خری فتنه را به زر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گو بر فروز شمع مرادی که از خطت</p></div>
<div class="m2"><p>روزی به پیش آمده از شب سیاهتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر در رهت ز دیده خیالی بریخت آب</p></div>
<div class="m2"><p>سهل است، گو بیا و از این ماجرا گذر</p></div></div>