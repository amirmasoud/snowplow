---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>افسوس که ره بینان یک یک ز نظر رفتند</p></div>
<div class="m2"><p>وز راه سبکباری با هم به سفر رفتند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پای از سر و جان بر کف در راه رضا بودند</p></div>
<div class="m2"><p>از یار چو فرمان شد مجموع به سر رفتند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همراه طلب کایشان از دولت همراهی</p></div>
<div class="m2"><p>در بادیهٔ حیرت ایمن ز خطر رفتند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بودند به صد عشرت در قصر جهان عمری</p></div>
<div class="m2"><p>و آخر دل پر حسرت زاین خانه به در رفتند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر اهل نظر کاری جز عجز نشد معلوم</p></div>
<div class="m2"><p>در کارگه عزّت هرچند که در رفتند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از راه جهانداری برتاب خیالی روی</p></div>
<div class="m2"><p>ز آن روی که همراهان از راه دگر رفتند</p></div></div>