---
title: >-
    شمارهٔ ۲۹۴
---
# شمارهٔ ۲۹۴

<div class="b" id="bn1"><div class="m1"><p>زهی تیره از زلف تو روز، شام</p></div>
<div class="m2"><p>به روی تو دعویّ مه ناتمام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو نسبت ندارد به زلف تو مشگ</p></div>
<div class="m2"><p>چرا می پزد عود سودای خام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کنون ما و کویت که نبوَد عجب</p></div>
<div class="m2"><p>در بارِ خاصان و غوغای عام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>حیاتی که بی او رود، گر کسی</p></div>
<div class="m2"><p>بگوید حلال است بادش حرام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>توای مه یکی ز آنچه دارد رخش</p></div>
<div class="m2"><p>نداریّ و لاف است بالای بام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خیالی چو بگذشت از نام و ننگ</p></div>
<div class="m2"><p>به رندیّ و مستی برآورد نام</p></div></div>