---
title: >-
    شمارهٔ ۱۶۱
---
# شمارهٔ ۱۶۱

<div class="b" id="bn1"><div class="m1"><p>چو نام مستیِ نرگس به بزم باغ برآمد</p></div>
<div class="m2"><p>ز خاک لالهٔ رعنا به کف ایاغ برآمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندید نرگس صاحب نظر ز روی لطافت</p></div>
<div class="m2"><p>نظیر روی تو چندان که گرد باغ برآمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کمند شب رو زلف تو پر دل است از آن رو</p></div>
<div class="m2"><p>به دزدیِ دل عشّاق با چراغ برآمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بس که سوخت ز رشک نسیم سنبل زلفت</p></div>
<div class="m2"><p>بنفشه را به چمن دود از دماغ برآمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به هرکجا که ز سوز درون خویش خیالی</p></div>
<div class="m2"><p>دهن گشاد چو شمعش ز دل فُراغ برآمد</p></div></div>