---
title: >-
    شمارهٔ ۱۰۷
---
# شمارهٔ ۱۰۷

<div class="b" id="bn1"><div class="m1"><p>اوّل استادی که عشق و حسن را تقسیم کرد</p></div>
<div class="m2"><p>عاشقان را صبر و خوبان را جفا تعلیم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طوبی قدّ تو را از راست بینان هر که دید</p></div>
<div class="m2"><p>در سرافرازی بر او قدّ تو را تقدیم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز مه رویت منجم هیچ مقصودی نداشت</p></div>
<div class="m2"><p>ز این همه نقش دل افروزی که بر تقویم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آخرالامر از ره عزّت به جایی می‌رسد</p></div>
<div class="m2"><p>هرکه خواری را ز راه مردمی تعظیم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گوهر جان در تن خاکی خیالی را ز دوست</p></div>
<div class="m2"><p>چون امانت بود آخر هم بدو تسلیم کرد</p></div></div>