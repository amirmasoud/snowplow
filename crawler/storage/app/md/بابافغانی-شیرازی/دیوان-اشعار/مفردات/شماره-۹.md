---
title: >-
    شمارهٔ ۹
---
# شمارهٔ ۹

<div class="b" id="bn1"><div class="m1"><p>هر نفست با کسی شوخی و بی باکیست</p></div>
<div class="m2"><p>جان مرا سوختی این چه هوسناکیست</p></div></div>