---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>دارم دل گرم و دم تقریر ندارم</p></div>
<div class="m2"><p>دریاب که می سوزم و تدبیر ندارم</p></div></div>