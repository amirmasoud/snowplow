---
title: >-
    شمارهٔ  ۱۴ - در منقبت مولای متقیان امیرالمؤمنین علی بن ابیطالب علیه السلام
---
# شمارهٔ  ۱۴ - در منقبت مولای متقیان امیرالمؤمنین علی بن ابیطالب علیه السلام

<div class="b" id="bn1"><div class="m1"><p>ای آمده در گلشن جان نخل تو واحد</p></div>
<div class="m2"><p>اثبات دویی بر الف قد تو زاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنی که پی روشنی کار دو عالم</p></div>
<div class="m2"><p>شد نور تو از مشرق و مغرب متصاعد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روی تو بود در نظر بنده ی مؤمن</p></div>
<div class="m2"><p>چون جلوه ی معبود در آیینه ی عابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارند محبان تو چون عقد لآلی</p></div>
<div class="m2"><p>از شایبه ی گرد ریا، پاک عقاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر گوهر مقصود که در پرده نهان بود</p></div>
<div class="m2"><p>بر لوح ضمیر تو یکایک شده وارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایی که قلم نام تو بر لوح نویسد</p></div>
<div class="m2"><p>آنجا چه نماید رقم کلک عطارد؟</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سلطان سراپرده ی عزت که ز عصمت</p></div>
<div class="m2"><p>از هر چه بود غیر خدا آمده زاید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خورشیدی و در مطلع انوار امامت</p></div>
<div class="m2"><p>آثار بود عصمت ذات تو شواهد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تسبیح تو آزاد کند در صف طاعت</p></div>
<div class="m2"><p>از دام هوی مرغ دل راکع و ساجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>از نور تو شد مشرق انوار سعادت</p></div>
<div class="m2"><p>در صبح ازل گوشه ی محراب مساجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>سیر تو بود در چمن عالم علوی</p></div>
<div class="m2"><p>شرح شب معراج بدین واقعه شاهد</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>بی نور ولایت نبود شمع نبوت</p></div>
<div class="m2"><p>هم قول رسولست درین نکته مؤید</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>در آینه ی نور خدا نقش دویی نیست</p></div>
<div class="m2"><p>هیهات که شد دیده ی احول متردد</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>گر پرتو خورشید به صد آینه تابد</p></div>
<div class="m2"><p>یک عین بود در نظر دید موحد</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>در دیده ی غیر تو خیال تو نگنجد</p></div>
<div class="m2"><p>یعنی که برونست ازین پرده زواید</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>در یک نظر از ذره بخورشید برد راه</p></div>
<div class="m2"><p>آنرا که شود جذبه مهر تو مساعد</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>عیسی نفسان بر سر خوان انا املح</p></div>
<div class="m2"><p>از چاشنی نطق تو گیرند فواید</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>در نیت کاری که رضای تو نباشد</p></div>
<div class="m2"><p>گر عقد نمازست بود نیت فاسد</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در گردن جان حجله نشینان سخن را</p></div>
<div class="m2"><p>از سلسله ی گوهر وصف تو قلاید</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>تا چند بود پرتو خورشید ولایت</p></div>
<div class="m2"><p>در پرده نهان از حسد دیده ی حاسد</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>شد وقت که خورشید عنایت بدرخشد</p></div>
<div class="m2"><p>از اوج یقین کوری این جمع مقلد</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آنروز بود اول نوروز هدایت</p></div>
<div class="m2"><p>کاین باغ کهن را شود امر تو مجدد</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>با سوز دل و دیده ی خونبار، فغانی</p></div>
<div class="m2"><p>شد در طلب گوهر وصف تو مجاهد</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>تا اهل صفا در طلب گوهر بینش</p></div>
<div class="m2"><p>آرند بجا در حرمت شرط قواعد</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>گرد قدمت سرمه ی ارباب یقین باد</p></div>
<div class="m2"><p>کاین گوهر مقصود بود اصل مقاصد</p></div></div>