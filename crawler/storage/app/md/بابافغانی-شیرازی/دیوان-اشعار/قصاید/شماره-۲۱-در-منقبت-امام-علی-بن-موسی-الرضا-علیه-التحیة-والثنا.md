---
title: >-
    شمارهٔ  ۲۱ - در منقبت امام علی بن موسی الرضا علیه التحیة والثناء
---
# شمارهٔ  ۲۱ - در منقبت امام علی بن موسی الرضا علیه التحیة والثناء

<div class="b" id="bn1"><div class="m1"><p>ای شعله ی چراغ در خانه ات هلال</p></div>
<div class="m2"><p>سیاره ات شراره ی شمع صف نعال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان علی موسی کاظم امین وحی</p></div>
<div class="m2"><p>ای مهچه ی لوای تو خورشید بیزوال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آنجا که سایه ی شجر کبریای تست</p></div>
<div class="m2"><p>بر گیست ماه عید که افتاده از نهال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر کوکبی که از افق طالع تو تافت</p></div>
<div class="m2"><p>بر شرق و غرب حکم کند تا هزار سال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آنی که بسته اند برای شکار ملک</p></div>
<div class="m2"><p>بر طبل باز تربیتت خسروان دوال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در دامن تو هر که زند دست اعتصام</p></div>
<div class="m2"><p>لطف تو ملتفت شودش تا دم سؤال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رگ بر تن ضعیف عدو از نهیب تو</p></div>
<div class="m2"><p>پیچیده ز انفعال چو در جوف خامه نال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>روشن شود ز پرتو نور یقین جهان</p></div>
<div class="m2"><p>از ذکر، چون چراغ دلت گیرد اشتعال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>رمحت ز خون مردمک دیده ی عدو</p></div>
<div class="m2"><p>بر روی فتح و چهره ی نصرت نهاده خال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>انفاس مشکبار تو گر بگذرد بچین</p></div>
<div class="m2"><p>از شرم نامه را فگند بر زمین غزال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون خط استوا خرد شرع پرورت</p></div>
<div class="m2"><p>بیرون نمی نهد قدم از حد اعتدال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>تا نامزد بلهو و لعب گشت ماه عید</p></div>
<div class="m2"><p>در حضرت تو سرزده خورشید بی زوال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>طرخان تست غره ی عید این که از شفق</p></div>
<div class="m2"><p>پیچیده بر نشان همایون پرند آل</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>با شغل آن جهان نفسی از خیال علم</p></div>
<div class="m2"><p>غافل نمی شود دل پاکت زهی خیال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>تا روز حشر، حلقه ی زنجیر عدل تو</p></div>
<div class="m2"><p>با رشته ی شهور و سنین دارد اتصال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>با اتصال سلسله ی عهد دولتت</p></div>
<div class="m2"><p>پیوند روزگار کجا یابد انفصال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>در بوستان ز تربیت لطف شاملت</p></div>
<div class="m2"><p>چیند عرق ز چهره ی گلبرگ تر شمال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آندم که آتش غضبت مشتعل بود</p></div>
<div class="m2"><p>بر کوه اگر رسد اثر آن شود ز گال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>تیر دعای صبحدمت آورد فرود</p></div>
<div class="m2"><p>از پیشگاه قلعه ی تقدیر کوتوال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>گلبانگ طایر لب بام تو خلق را</p></div>
<div class="m2"><p>خواند به راه راست چو قد قامت بلال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>داری جبلتی که بهمت روان کنی</p></div>
<div class="m2"><p>از پیش راه خلق چو ریگ روان جبال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>آن قطب ساکنی که بمعنی عیان شوی</p></div>
<div class="m2"><p>از شرق تا بغرب در آیینه ی خیال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گر کار خود قضا برضایت گذاشتی</p></div>
<div class="m2"><p>نگذاشتی که رخنه کند در دلی ملال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>قول تو در امور بود راست همچو فعل</p></div>
<div class="m2"><p>دورست قول مخبر صادق ز احتمال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>جز هیأتت که سایه ی نورانی حقست</p></div>
<div class="m2"><p>دیگر هر آنچه هست محالست در خیال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>مردم تمام در پی مالند و ذات تو</p></div>
<div class="m2"><p>پیوسته در ملاحظه ی حالت مآل</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>از حق امانتی که به شاه نجف رسید</p></div>
<div class="m2"><p>نسلا بنسل کرده بذات تو انتقال</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>خواهد سعادتت ز خدا هر کجا دلیست</p></div>
<div class="m2"><p>این حرف بر سعادت اهل دلست دال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>در قسمت ازل نمک سفره ی تو شد</p></div>
<div class="m2"><p>مصروف رزق آدم و ذریت و عیال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>گر یکنظر بسبعه ی سیاره افگنی</p></div>
<div class="m2"><p>تا بامداد حشر نیفتد درو وبال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>آمد ثنای ذات تو در اول ورق</p></div>
<div class="m2"><p>از دفتر سخن چو فغانی گشود فال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>تا بر فراز سده ی نه پایه ی سپهر</p></div>
<div class="m2"><p>بنماید از نقاب شفق ماه نو جمال</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>هر بامداد عید که صف مستعد شود</p></div>
<div class="m2"><p>بادا بنای خطبه بنام علی و آل</p></div></div>