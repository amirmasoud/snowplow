---
title: >-
    شمارهٔ  ۲۵ - در مدح شاه تاج الدین حسن
---
# شمارهٔ  ۲۵ - در مدح شاه تاج الدین حسن

<div class="b" id="bn1"><div class="m1"><p>گل شکفت و غنچه ها را باز شد مهر از دهن</p></div>
<div class="m2"><p>گلبن از لب تشنگان باغ می گوید سخن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز وقت آمد که رو پوشیدگان روزگار</p></div>
<div class="m2"><p>هر یکی دیدار بنمایند بر وجه حسن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تازه گردد نرگس مخمور از دست و ترنج</p></div>
<div class="m2"><p>لاله را چون دامن یوسف بدرد پیرهن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چاک پیراهن گشاید غنچه و چین قبا</p></div>
<div class="m2"><p>داغهای دل نماید لاله ی خونین کفن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر خاک شهید عشق گردد ده زبان</p></div>
<div class="m2"><p>غنچه ی سوسن که چون شمعیست نیلی در لگن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ناربن در جان مرغان هوا آتش زند</p></div>
<div class="m2"><p>آستین بر طوطی گردون فشاند نارون</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هوا هر دانه ی شبنم که افتد بر زمین</p></div>
<div class="m2"><p>باد صبح از گل برون آرد برنگ یاسمن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بانگ روح افزای مرغ و نکهت دمساز گل</p></div>
<div class="m2"><p>عشقبازان را ببستان خواند از بیت الحزن</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خوبی و لطف هوا بنگر که در اردیبهشت</p></div>
<div class="m2"><p>صاف می سازد دماغ طبع را دردی دن</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>صبحدم خورشید از نظاره شد سیاره بار</p></div>
<div class="m2"><p>گرمی بازار نسرین بین و جوش نسترن</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>گلبن از گلهای رنگین عود سوز و عطر ساز</p></div>
<div class="m2"><p>بوستان مجموعه پرداز ورقهای سمن</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>شاخسار از نکهت گل غیرت عطار چین</p></div>
<div class="m2"><p>جویبار از عقد شبنم رشک غواص عدن</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>دانه ها چون خوشه ی پروین ز جوهر عقد بند</p></div>
<div class="m2"><p>کشتزار از باد همچون روی دریا موجزن</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ارغوان از باد می شوید به آب گل دهان</p></div>
<div class="m2"><p>در هوای دستبوس سرفراز انجمن</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کاشف سر حقیقت وارث علم نبی</p></div>
<div class="m2"><p>افتخار آل یس شاه تاج الدین حسن</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>آنکه بی شهد ولای او دهان طفلرا</p></div>
<div class="m2"><p>نیست آن یارا که آلاید لبان را از لبن</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>از پدر تا عقل اول زاده ی زهد و ورع</p></div>
<div class="m2"><p>همچنین تا ذات واجب تابع شرع و سنن</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>نوک کلکش رهروانرا مقسم زاد سفر</p></div>
<div class="m2"><p>دست جودش ساکنانرا شامل ساز وطن</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>در ادای شکر انعامش نواها بسته اند</p></div>
<div class="m2"><p>طوطیان در شکرستان بلبلان اندر چمن</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>راستی یکروزه خرج خانقاه خیر اوست</p></div>
<div class="m2"><p>آنچه در کان بدخشانست و صحرای یمن</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>جذبه یی دارد که گر کفار را خواند بدین</p></div>
<div class="m2"><p>بشکند بتخانه و محراب سازد برهمن</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر که خواند یک ورق از دفتر اخلاص او</p></div>
<div class="m2"><p>دفتر دل پاک گرداند ز حرف ما و من</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>ای خیالت همچو نور علم در مشکوة دل</p></div>
<div class="m2"><p>وی ضمیرت چون فروغ عقل در مصباح تن</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>ذات بی مثل تو کز دریای عرفان گوهریست</p></div>
<div class="m2"><p>ایزدش پیوسته دارد در پناه خویشتن</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>از صفا چون کعبه بر روی زمین دارد شرف</p></div>
<div class="m2"><p>مجلس وعظت ز تسبیح دعای مرد و زن</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>التفات ذات محمود تو با این بینوا</p></div>
<div class="m2"><p>دارد آن نسبت که احمد داشت باویس قرن</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>دعوی منصور اگر بودی بدور عدل تو</p></div>
<div class="m2"><p>کی قرین آب و آتش میشد و دارو رسن</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>هر که از روی ارادت برد از دست تو داد</p></div>
<div class="m2"><p>در ثبات هستیش مشکل اگر افتد شکن</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>دین پناها نخل مدحم در خور قدر تو نیست</p></div>
<div class="m2"><p>گرد گلزارت دعایی می کند این خار کن</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>تا کنند از آب زر پر دفتر گل عشر و خمس</p></div>
<div class="m2"><p>در کتاب لاله تا باشد خط از مشک ختن</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>نامه ی عمرت بعنوان بقا پیوسته باد</p></div>
<div class="m2"><p>نقطه ی حرفش مصون بادا ز آسیب فتن</p></div></div>