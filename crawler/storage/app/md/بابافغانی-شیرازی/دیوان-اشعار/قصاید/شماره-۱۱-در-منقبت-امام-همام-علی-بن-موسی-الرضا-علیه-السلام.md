---
title: >-
    شمارهٔ  ۱۱ - در منقبت امام همام علی بن موسی الرضا علیه السلام
---
# شمارهٔ  ۱۱ - در منقبت امام همام علی بن موسی الرضا علیه السلام

<div class="b" id="bn1"><div class="m1"><p>چمن شکفت و جهان پر ز سوسن و سمنست</p></div>
<div class="m2"><p>بصد هزار زبان روزگار در سخنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز خاک سوخته ی داغ آتشین رویان</p></div>
<div class="m2"><p>دمید لاله و سوزش هنوز در کفنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نه روز آنست که در خانه مست بتوان بود</p></div>
<div class="m2"><p>برون خرام ز مجلس که نوبت چمنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسید نافه گشا باد صبحدم، گویا</p></div>
<div class="m2"><p>روان به دامن صحرا روایح ختنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خراش غنچه ی رعنا و خار خار نسیم</p></div>
<div class="m2"><p>نشان دست زلیخا و چاک پیرهنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز وصف گوهر لعل تو در حریم چمن</p></div>
<div class="m2"><p>دهان غنچه ی سیراب پر در عدنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو لاله بی گل روی تو جامه چاک زدن</p></div>
<div class="m2"><p>بتر ز واقعه ی بیستون و کوهکنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در آن چمن که شود قامت تو دست افشان</p></div>
<div class="m2"><p>چه جای جلوه ی شمشاد و رقص نارونست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هوای کوی تو دارد صبا ز گشت چمن</p></div>
<div class="m2"><p>چو آن غریب که میلش به جانب وطنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز جان گذشتم و دیدم جمال کعبه ی جان</p></div>
<div class="m2"><p>درین ره آنکه ز هستی گذشت جان منست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تبارک الله ازین روضه ی بهشت آیین</p></div>
<div class="m2"><p>که یک غبار درش آبروی نه چمنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چه جای گلشن عالم که هفت باغ جنان</p></div>
<div class="m2"><p>طفیل روضه ی سلطان دین ابوالحسنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>علی موسی کاظم امین گلشن وحی</p></div>
<div class="m2"><p>که طوف بارگهش از فرایض سننست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>ز یمن سایه ی عنقای قاف قدرت او</p></div>
<div class="m2"><p>همای ناطقه ناظر به کشور بدنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بگرد روضه ی او گر نعیم هشت بهشت</p></div>
<div class="m2"><p>شود نثار یکایک بجای خویشتنست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>فرو گرفت جهان را چراغ همت او</p></div>
<div class="m2"><p>چو آفتاب که خنجر گزار و تیغ زنست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گلی که از چمن کبریای او سر زد</p></div>
<div class="m2"><p>شکفته باد که چشم و چراغ انجمنست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>چو پرچم علمش باد صبح جلوه دهد</p></div>
<div class="m2"><p>چه جای دم زدن یاسمین و نسترنست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>چراغ دولت او لاله ی ابد پیوند</p></div>
<div class="m2"><p>نهال همت او شمع آسمان لگنست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فغان ز مکر تو ای ناصبی بگو آخر</p></div>
<div class="m2"><p>که این چه دشمنی و لاف دوستی زدنست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>ز جام ساقی کوثر کجا شود سیراب</p></div>
<div class="m2"><p>ترا که کاسه ی سر بر هوای درد دنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>درین چمن که ز آسیب برگ ریز خزان</p></div>
<div class="m2"><p>هوا مبدل و بلبل فگار و ممتحنست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>فسانه ی زن جادو و سر پرده ی شیر</p></div>
<div class="m2"><p>حکایتیست که ورد زبان مرد و زنست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>کسی که دانه ی انگور دام حیلت ساخت</p></div>
<div class="m2"><p>چو خوشه از گنه آن بگردنش رسنست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>زهی چراغ دلت شمع هفت پرده ی دل</p></div>
<div class="m2"><p>خیال نحل قدت زیب چهار باغ تنست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>قبای سبز تو فارغ ز چاک دامن و جیب</p></div>
<div class="m2"><p>نگین لعل تو ایمن ز دست اهرمنست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>کند ولای تو رنگ موالیان چو عقیق</p></div>
<div class="m2"><p>طلوع مهر تو همچون سهیل در یمنست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>سپهر را سر رمح تو اختر شرفست</p></div>
<div class="m2"><p>زمانه را دم تیغ تو مانع فتنست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>به سجده ی تو رود سر که در بدن زنده ست</p></div>
<div class="m2"><p>بمدح ذات تو گویا زبان که در دهنست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>به آب دیده فغانی چو مدحت تو نوشت</p></div>
<div class="m2"><p>سواد کاغذ شعرش بنفشه و سمنست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>همیشه تا به مصاف سپاه غنچه و گل</p></div>
<div class="m2"><p>نسیم پرده درو باد صبح صف شکنست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>حسود جاه تو در پرده ی خجالت باد</p></div>
<div class="m2"><p>چو عنکبوت که بر عیب خویش پرده تنست</p></div></div>