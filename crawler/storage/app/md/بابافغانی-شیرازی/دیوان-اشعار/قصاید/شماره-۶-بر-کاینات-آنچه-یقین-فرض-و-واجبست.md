---
title: >-
    شمارهٔ  ۶ - بر کاینات آنچه یقین فرض و واجبست
---
# شمارهٔ  ۶ - بر کاینات آنچه یقین فرض و واجبست

<div class="b" id="bn1"><div class="m1"><p>بر کاینات آنچه یقین فرض و واجبست</p></div>
<div class="m2"><p>مهر و محبت اسدالله غالبست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>انسان ندانمش که نداند بهین قوم</p></div>
<div class="m2"><p>آنرا که هل اتی علی الانسان مناقبست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فرقست از آنکه زاده ی دین آمد از ازل</p></div>
<div class="m2"><p>با آن نو اعتقاد که ده روزه طالبست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با آنکه آفتاب بحکمش کند عمل</p></div>
<div class="m2"><p>صدق دروغ کج نظران صبح کاذبست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدر علی ز صاحب معراج بازپرس</p></div>
<div class="m2"><p>تا روشنت شود که در اعلی مراتبست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر افضلیتست اتم افاضلست</p></div>
<div class="m2"><p>ور اقربیتست اقر اقاربست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خواند از وفا حبیب خدایش حبیب خویش</p></div>
<div class="m2"><p>اخلاص تا کجاست که مطلوب طالبست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دست بریده کرد درست این غریب نیست</p></div>
<div class="m2"><p>بخشید سر بخصم خود این از غرایبست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نبود عجب گر از همه شانی کند ظهور</p></div>
<div class="m2"><p>ذات علی که مظهر کل عجایبست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>علمش خبر دهد که سعیدست یا شقی</p></div>
<div class="m2"><p>از هر چه در میانه ی صلب و ترایبست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>یک گام صوریش ز مدینه ست تا تبوک</p></div>
<div class="m2"><p>یک سیر معنیش به ثریا ز یثربست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>در بارگاه شاه نجف هر صباح و شام</p></div>
<div class="m2"><p>پروانه افتاب و مه بدر حاجبست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>امرش بر امتان نبی دین و لازمست</p></div>
<div class="m2"><p>مهرش به بندگان خدا فرض و واجبست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نیک اختری که یافت فروغ چراغ او</p></div>
<div class="m2"><p>نزد خدا و خلق سعید العواقبست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>نسبت بطاق روضه سرای امیر نحل</p></div>
<div class="m2"><p>این پرده های سبز چو بیت عناکبست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>خدام شاه را ز تف آتش جحیم</p></div>
<div class="m2"><p>پروانه ی نجات و برات مواجبست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>اصحاب صفه را عوض جیفه ی فنا</p></div>
<div class="m2"><p>هر بامداد عمر ابد نزل راتبست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>آن خس که خار خار دل اهل بیت خواست</p></div>
<div class="m2"><p>رگ در تنش بقصد چو نیش عقاربست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>آن بد گمان که با اسدالله کینه باخت</p></div>
<div class="m2"><p>گو دیده باز کن که به خواب ارانبست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>دیگر مکن مناظره با غاصب فدک</p></div>
<div class="m2"><p>او را همین بسست که گویند غاصبست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>در حیف نخل باغ جگر گوشه ی رسول</p></div>
<div class="m2"><p>از جویبار دیده روان دمع ساکبست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>شایسته ی مصیبت و رنجست ناصبی</p></div>
<div class="m2"><p>کو دشمن امام زحب مناصبست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>زخم زبان که هست بدل نقش فی الحجر</p></div>
<div class="m2"><p>بر قلب رو سیاه خوارج مناسبست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نور علی ز ارض نجف می رسد بعرش</p></div>
<div class="m2"><p>باشد چراغ دل اگر از دیده غایبست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>صوت نهان و معنی مقصود در حضور</p></div>
<div class="m2"><p>جان واله مشاهده و تن مراقبست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>تعداد رشحه ی قلم فیض بخش او</p></div>
<div class="m2"><p>یبرون ز جذر و مد رقوم محاسبست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در صورت ار نهانست بمعنی بود عیان</p></div>
<div class="m2"><p>خورشید را چه نقص که گویند غاربست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>یک پرتو از فروغ رخش مهر لامعست</p></div>
<div class="m2"><p>یک شعله از چراغ دلش نجم قاقبست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نادعلی چو ورد زبان ساخت متقی</p></div>
<div class="m2"><p>آسوده از بلا و مصون از نوایبست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>دانش و بال و زهد ریا، بی قبول او</p></div>
<div class="m2"><p>گر شیخ خانقاه و گر پیر راهبست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>عین علیست آینه ی اعتقاد مرد</p></div>
<div class="m2"><p>روی کسی سفید که پاک از معایبست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>زاندم که خواست تافت بر اهل شک آفتاب</p></div>
<div class="m2"><p>دامن بخون دل زده در چاه معزبست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>ساغر ز دست ساقی کوثر کشد مدام</p></div>
<div class="m2"><p>آن کز زلال چشمه ی تحقیق شاربست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>اشیا به آستین یدالله داده دست</p></div>
<div class="m2"><p>چون اختیار بنده که در دست صاحبست</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ای شسته از مطالب ارباب جیفه دست</p></div>
<div class="m2"><p>دامان شاه گیر که ذیل مآربست</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>بر خود مساز مذهب هفتاد و دو دراز</p></div>
<div class="m2"><p>یک رنگ آل باش که اصل مذاهبست</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>در مدح حیدر آنچه خدا و رسول گفت</p></div>
<div class="m2"><p>راجع به ذات مهدی صاحب مواهبست</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>هم نشأه ی بنی و ولی صاحب الزمان</p></div>
<div class="m2"><p>شاهی که فتح و نصرتش از این دو جا نبست</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>خلقش عظیم و طبع کریم و دلش رحیم</p></div>
<div class="m2"><p>این موهیت تمام ز توفیق واهبست</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>با شرع و دین ز جنبش اولیست توأمان</p></div>
<div class="m2"><p>با عقل کل ز غیب هویت مصاحبست</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>تسبیح کرده ز اختر و دفتر ز ماه و مهر</p></div>
<div class="m2"><p>تا روز همدم شب مشکین ذوایبست</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>اینست بندگی که بود خاصه بهر حق</p></div>
<div class="m2"><p>نه طاعتی که بهر وصول کواعبست</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>زهدش شفیع قهقهه ی صبح ضاحکست</p></div>
<div class="m2"><p>علمش مزیل شعبده ی چرخ لاعبست</p></div></div>
<div class="b" id="bn44"><div class="m1"><p>دشمن گداز و دوست نوازست روز رزم</p></div>
<div class="m2"><p>در میمنه ست جاذب و بر قلب حاربست</p></div></div>
<div class="b" id="bn45"><div class="m1"><p>آنجا که عرض لشکر نصرت شعار اوست</p></div>
<div class="m2"><p>اجرام سبعه گرد نعال مراکبست</p></div></div>
<div class="b" id="bn46"><div class="m1"><p>شاها بقادری که وضیع و شریف را</p></div>
<div class="m2"><p>ازوی امید لطف نجات از مصایبست</p></div></div>
<div class="b" id="bn47"><div class="m1"><p>کاین بنده تا بشارع هستی مجال یافت</p></div>
<div class="m2"><p>همراه این جناب و پی این مواکبست</p></div></div>
<div class="b" id="bn48"><div class="m1"><p>واثق بعفوتست فغانی که از خطا</p></div>
<div class="m2"><p>عنوان نامه ی عملش عبد مذنبست</p></div></div>
<div class="b" id="bn49"><div class="m1"><p>ظل علی و آل علی مستدام باد</p></div>
<div class="m2"><p>اینست مطلبی که اهم مطالبست</p></div></div>