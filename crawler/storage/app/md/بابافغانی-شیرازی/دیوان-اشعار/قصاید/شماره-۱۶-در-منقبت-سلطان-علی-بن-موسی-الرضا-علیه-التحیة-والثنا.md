---
title: >-
    شمارهٔ  ۱۶ - در منقبت سلطان علی بن موسی الرضا علیه التحیة والثناء
---
# شمارهٔ  ۱۶ - در منقبت سلطان علی بن موسی الرضا علیه التحیة والثناء

<div class="b" id="bn1"><div class="m1"><p>ای کعبه را ز وقفه ی عید تو افتخار</p></div>
<div class="m2"><p>قربانی تو هستی ابنای روزگار</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در عیدگه ز شوق رخت چشم اهل دید</p></div>
<div class="m2"><p>بازست همچو دیده ی قربانی فگار</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا گرد مقدم تو دهد مروه را صفا</p></div>
<div class="m2"><p>از کعبه مانده حلقه بدر چشم انتظار</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر نثار محمل گردون شکوه تست</p></div>
<div class="m2"><p>زمزم که چون ستاره کند قطره ها قطار</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پاک از گنه شد آنکه نثار تو کرد جان</p></div>
<div class="m2"><p>ای جان پاک در حرم حرمتت نثار</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دم به روزگار تو عیدیست خلق را</p></div>
<div class="m2"><p>فرخنده روز وصل تو ای عید روزگار</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گل گل شکفته آنکه هواخواه کعبه بود</p></div>
<div class="m2"><p>دارد برای طوف حریم تو خار خار</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دارد طواف روضه ی مشهد ثواب حج</p></div>
<div class="m2"><p>نزدیک گشته از تو ره خلق این دیار</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ان کعبه راست خار مغیلان بجای گل</p></div>
<div class="m2"><p>وین روضه راست لاله و ریحان بجای خار</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>آوازه ی جمال تو هر کس که بشنود</p></div>
<div class="m2"><p>تا ننگرد بدیده نگیرد دلش قرار</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>طاوس روضه در حرمت جان فدا کند</p></div>
<div class="m2"><p>در جلوه گر بخاک درت افگند گذار</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>سلطان بارگاه امامت ابوالحسن</p></div>
<div class="m2"><p>ای مهر و مه ز گرد رهت یکدو ذره وار</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>چشم و چراغ دوره اثنا عشر تویی</p></div>
<div class="m2"><p>ای قبله ی قبایل و ای کعبه ی تبار</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>وقت دعا سفینه ی نوح آورد روان</p></div>
<div class="m2"><p>انفاس روح بخش تو از ورطه بر کنار</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>گیرد فضای ملک دو عالم پیک نفس</p></div>
<div class="m2"><p>چون بر براق برق شود همت سوار</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>از خاک آستان تو دارند آبرو</p></div>
<div class="m2"><p>پیران مو سفید و جوانان گلعذار</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>بر چار جوی هشت چمن سایه ی افگند</p></div>
<div class="m2"><p>قدت که طوبییست ز فردوس هشت و چار</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>ای راز مخفی دو جهان از فروغ دل</p></div>
<div class="m2"><p>بر آفتاب رای تو چون روز آشکار</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>اهل نظر ز عین صفا توتیا کنند</p></div>
<div class="m2"><p>در کعبه گر ز دامن پاکت رسد غبار</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بر آسمان قدر کند کار آفتاب</p></div>
<div class="m2"><p>فانوس بارگاه تو در پرده ی وقار</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرغ حریم سدره چو پروانه صبح و شام</p></div>
<div class="m2"><p>پرواز کرد گرد سر شمع این مزار</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>هر ذره بی که خاست بمهر تو از زمین</p></div>
<div class="m2"><p>پهلو بر آفتاب زد از عین افتخار</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>گاهی که التفات بکار جهان کنی</p></div>
<div class="m2"><p>دیگر سپهر را نرسد دخل هیچ کار</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>روز ازل که فاعل مختار تا ابد</p></div>
<div class="m2"><p>بر دست اعتبار تو می داد اختیار</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>ذات بزرگوار تو از همت بلند</p></div>
<div class="m2"><p>فرمود بر مطالعه ی علم اختصار</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>کار جهان چو نامزد دولت تو شد</p></div>
<div class="m2"><p>گردون بوفق امر کمر بست بنده وار</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>هم قدر علم دارد و هم دولت عمل</p></div>
<div class="m2"><p>شخصت که در دو کون خدا ساخت بختیار</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>از لاف خصم روشنی مهر کم نشد</p></div>
<div class="m2"><p>بیشست از ملایمت مهره، زهر مار</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>آن خس که ساخت دانه ی انگور دام ره</p></div>
<div class="m2"><p>شد بر مثال برگ خزان خوار و شرمسار</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>باشد نشان بغض وی از زردی رخش</p></div>
<div class="m2"><p>آری دلیل روشن نارست برگ نار</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حالا ز جام جهل بود مست خارجی</p></div>
<div class="m2"><p>فریاد ازان نفس که رسد نوبت خمار</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>دارد فغانی از طلب گرد مقدمت</p></div>
<div class="m2"><p>بر رهگذار باد صبا چشم انتظار</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>چندانکه میدمد گل و نوروز می شود</p></div>
<div class="m2"><p>چندانکه عید می رسد و می رسد بهار</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چون صبح نوبهار به صد رو شکفته باد</p></div>
<div class="m2"><p>گلزار آلت از اثر لطف کردگار</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>در باغ دهر ظل رفیع تو مستدام</p></div>
<div class="m2"><p>کاین نخل نو ز گلشن آلست یادگار</p></div></div>