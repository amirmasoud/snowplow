---
title: >-
    شمارهٔ  ۲۰ - در منقبت مولای متقیان و ائمه ی اطهار علیهم السلام
---
# شمارهٔ  ۲۰ - در منقبت مولای متقیان و ائمه ی اطهار علیهم السلام

<div class="b" id="bn1"><div class="m1"><p>ای رخ فرخنده ات خورشید ایوان جمال</p></div>
<div class="m2"><p>قامت نورانیت شمع شبستان خیال</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هدهد فرخنده فال طرف بامت جبرئیل</p></div>
<div class="m2"><p>بلبل دستانسرای باغ اسلامت بلال</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گاه اعجاز کلام از لفظ گوهر بار خویش</p></div>
<div class="m2"><p>داده یی صدره فصیحان عرب را گوشمال</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نطق انفاس روانبخش تو در لفظ حدیث</p></div>
<div class="m2"><p>از صفا چون گوهر رخشنده در آب زلال</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد مسلسل گوهر ارواح در بحر قدم</p></div>
<div class="m2"><p>شاهباز عرش پرواز تو چون افشاند بال</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خاستی از جوهر خاک قدومت ذره یی</p></div>
<div class="m2"><p>کلک صورتگر نهادی بر رخ خورشید خال</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کی ابد آباد گشتی گر نبودی در ازل</p></div>
<div class="m2"><p>آفرینش را به خاک آستانت اتصال</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بود در لوح ازل آدم مجرد چون الف</p></div>
<div class="m2"><p>منضم از نام محمد گشت باوی میم و دال</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>اینکه می جست از خدا طوفان به آب دیده نوح</p></div>
<div class="m2"><p>خواست تا بنشاند از پیش رهت گر درحال</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>نور بیچون بود مرآت دلت زانرو نشد</p></div>
<div class="m2"><p>جز تو کس را در درون خلوت جانان مجال</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>لن ترانی شد جواب موسی عمران ز طور</p></div>
<div class="m2"><p>بر تو خود ظاهر شد انوار مقدس بی سؤال</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>یکنظر نور تجلی دید و بیخود شد کلیم</p></div>
<div class="m2"><p>روز و شب داری تو آن را در نظر بی انفعال</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>تا سر خوان نبوت را ولینعمت شوی</p></div>
<div class="m2"><p>شد خلیل از انتظار مقدمت همچون خلال</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>بوی خلقت گر نبودی شامل حال رسل</p></div>
<div class="m2"><p>کی سلیمانرا به فرمان آمدی باد شمال</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>از حجر مرغ مرصع شد به فرمانت عیان</p></div>
<div class="m2"><p>جان نثار مقدمت ای طایر فرخنده فال</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>پرتو مهر ازل کز حسن یوسف جلوه داشت</p></div>
<div class="m2"><p>از مه روی تو ظاهر گشت بر وجه کمال</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>گلشن جان را سبب نخل دلارای تو شد</p></div>
<div class="m2"><p>بردمد آری هزاران شاخ گل از یک نهال</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>فقرت از تسکین مسکینان امت بود و بس</p></div>
<div class="m2"><p>ورنه کی باشد نبوت را زیان از ملک و مال</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>مه اسیر دام مهر تست ز آنرو می کشد</p></div>
<div class="m2"><p>هر سر ماهش فلک در طوق سیمین هلال</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>فیض عامت گر نبودی زاد راه آخرت</p></div>
<div class="m2"><p>آدم خاکی چه کردی چاره ی مشتی عیال</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مرکب عزم ترا صانع ز فضل خویش داد</p></div>
<div class="m2"><p>تن ز جوهر سرز در وز رشته های حور یال</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>رحمت عام تو با شاه و گدا باشد یکی</p></div>
<div class="m2"><p>آب صافی را چه غم از کاسه ی زر یا سفال</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>در سجود افتند خلق عالمی بی اختیار</p></div>
<div class="m2"><p>شعله ی شمع رخت هر جا که یابد اشتعال</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>هر خیال بد که در دل داشتند اهل نفاق</p></div>
<div class="m2"><p>جمله را احسان عوض کردی زهی حسن خصال</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>نور ابن عم تو نبود جدا از نور تو</p></div>
<div class="m2"><p>در میان یکدلان رسم دویی باشد محال</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>سر نزد زین هفت پرده بر مثال پنج فرق</p></div>
<div class="m2"><p>پنج گوهر در بها و قدر، بی شبه و مثال</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>در قبای سبز، یکتا سرو آزاد حسن</p></div>
<div class="m2"><p>شمع سبزی بود روشن در سرابستان آل</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>در لباس ارغوانی، نخل گلرنگ حسین</p></div>
<div class="m2"><p>راست چون شاخ گلی در بوستان اعتدال</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>زینت و زیب ریاض شرع زین العابدین</p></div>
<div class="m2"><p>آن بهار بی خزان آن آفتاب بی زوال</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>شمع محراب امامت باقر، آن کز علم و دین</p></div>
<div class="m2"><p>از سر سجاده ی طاعت نرفتی ماه و سال</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>حفظ جعفر گر شود پیوند ترکیب زمان</p></div>
<div class="m2"><p>تا ابد سر رشته ی هستی نیابد انفصال</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>بحر عرفان موسی کاظم که از عین ورع</p></div>
<div class="m2"><p>گوهر افشان بود چشمش دایم از فکر مآل</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>قبله ی هشتم غریب طوس کز بیداد و جور</p></div>
<div class="m2"><p>شربت زهر مخالف خورد بی تغییر حال</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>هر پسر کو از دل و جان پرورد مهر تقی</p></div>
<div class="m2"><p>همچو شیر مادرش نان پدر بادا حلال</p></div></div>
<div class="b" id="bn35"><div class="m1"><p>ماه ایوان ولایت شاه روشندل نقی</p></div>
<div class="m2"><p>آنکه مهرش در دل هر ذره دارد اتصال</p></div></div>
<div class="b" id="bn36"><div class="m1"><p>شهسوار لشکر دین عسکری آن کز شکوه</p></div>
<div class="m2"><p>زیر نعل توسن او توتیا گردد جبال</p></div></div>
<div class="b" id="bn37"><div class="m1"><p>حضرت ختم ولایت مهدی صاحب زمان</p></div>
<div class="m2"><p>آنکه زو شد صدر خاور رشک ایوان جلال</p></div></div>
<div class="b" id="bn38"><div class="m1"><p>یا حبیب الله بحق مهر این روشندلان</p></div>
<div class="m2"><p>کز دعا روز جزا خلقی رهانند از وبال</p></div></div>
<div class="b" id="bn39"><div class="m1"><p>از کمال و رحمت و احسان، من درمانده را</p></div>
<div class="m2"><p>دستگیری کن که هستم غرقه ی بحر ضلال</p></div></div>
<div class="b" id="bn40"><div class="m1"><p>سر به زانو مانده ام عمری به فکر نعت تو</p></div>
<div class="m2"><p>قامت خم گشته ام اینک بدین معنیست دال</p></div></div>
<div class="b" id="bn41"><div class="m1"><p>یک رقم از بحر اوصافت نیارد در قلم</p></div>
<div class="m2"><p>گر فغانی تا ابد نظم سخن بندد خیال</p></div></div>
<div class="b" id="bn42"><div class="m1"><p>تا زنند از غایت همت ببام قصر دین</p></div>
<div class="m2"><p>پنج نوبت اهل دین بر کوس استغنا دوال</p></div></div>
<div class="b" id="bn43"><div class="m1"><p>گوش جان دوستانت باد بر نعت و درود</p></div>
<div class="m2"><p>جسم بدخواه و مخالف از فغان و ناله نال</p></div></div>