---
title: >-
    شمارهٔ  ۸ - در منقبت امام همام علی بن موسی الرضا علیه التحیة والثناء
---
# شمارهٔ  ۸ - در منقبت امام همام علی بن موسی الرضا علیه التحیة والثناء

<div class="b" id="bn1"><div class="m1"><p>خطی که یک رقمش آبروی نه چمنست</p></div>
<div class="m2"><p>نشان خاتم سلطان دین ابوالحسنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>علی موسی جعفر که مهر دولت او</p></div>
<div class="m2"><p>ستاره ی شرف و آفتاب انجمنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نقش خاتم او گر هزار جوهر جان</p></div>
<div class="m2"><p>شود نثار یکایک بجای خویشتنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شرح میمنت خاتم همایونش</p></div>
<div class="m2"><p>همای ناطقه را مهر عجز بر دهنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بمهر اوست که پروانه ی حیات ابد</p></div>
<div class="m2"><p>ز شهر روح مقرر بکشور بدنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حدیث گوهر سیراب لعل خاتم او</p></div>
<div class="m2"><p>چو شهد در دهن طوطی شکر شکنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آن صحیفه که طغرای او کنند رقم</p></div>
<div class="m2"><p>چه جای لاله ی نعمان و برگ نسترنست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سواد خاتم فیروزه ی سعادت او</p></div>
<div class="m2"><p>سپهر عربده جو را مزیل مکر و فنست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>طلسم خاتم حفظش چو حرز گنج العرش</p></div>
<div class="m2"><p>بگرد دایره ی کون مانع فتنست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>عقیق خاتم توقیع حکم آل علی</p></div>
<div class="m2"><p>بچشم اهل نظر چون سهیل در یمنست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>تبارک الله ازان هیأت خجسته مثال</p></div>
<div class="m2"><p>که هیکل دل و آرام جان و حرز تنست</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>چو نقش جام جم از جلوه ی سواد و بیاض</p></div>
<div class="m2"><p>نشان معرفت سر و صورت علنست</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>گلیست جلوه گر از بوستان دولت و دین</p></div>
<div class="m2"><p>که نو شکفته بروی بنفشه و سمنست</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>نشانه ی ید و بیضاست کز بیاض شرف</p></div>
<div class="m2"><p>چو ماه بدر در آفاق روشنی فگنست</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>فروغ شمسه ی مهر و ظلال او دارد</p></div>
<div class="m2"><p>لوای حمد که بر کاینات پرده تنست</p></div></div>
<div class="b" id="bn16"><div class="m1"><p>ز مهر ماه جمال تو ماه کنعان را</p></div>
<div class="m2"><p>تراوش مژه بهر طراز پیرهنست</p></div></div>
<div class="b" id="bn17"><div class="m1"><p>زهی امام که تعظیم حکم خدامت</p></div>
<div class="m2"><p>موالیان ترا از فرایض و سننست</p></div></div>
<div class="b" id="bn18"><div class="m1"><p>زمردیست نگینت که در سواد امان</p></div>
<div class="m2"><p>نهان ز دیده ی افعی و چشم اهرمنست</p></div></div>
<div class="b" id="bn19"><div class="m1"><p>عقیق خاتم طغرا نویس امر ترا</p></div>
<div class="m2"><p>سهیل صورت مهر ولایت یمنست</p></div></div>
<div class="b" id="bn20"><div class="m1"><p>بروی برگ ریاحین رقوم خاتم تو</p></div>
<div class="m2"><p>نشان نازکی ارغوان و نسترنست</p></div></div>
<div class="b" id="bn21"><div class="m1"><p>مسیر خامه ی مشکین مثال حکم ترا</p></div>
<div class="m2"><p>بنفشه مهر جواز قوافل ختنست</p></div></div>
<div class="b" id="bn22"><div class="m1"><p>چو داغ لاله، شهیدان راه عشق ترا</p></div>
<div class="m2"><p>نشان مهر و وفا بر حواشی کفنست</p></div></div>
<div class="b" id="bn23"><div class="m1"><p>برای مهر عقیق سخنورت ما را</p></div>
<div class="m2"><p>سفینه از رقم خون دیده موج زنست</p></div></div>
<div class="b" id="bn24"><div class="m1"><p>نگین مهر سلیمان چه قید راه شود</p></div>
<div class="m2"><p>ترا که مهر نبوت چراغ انجمنست</p></div></div>
<div class="b" id="bn25"><div class="m1"><p>عدو که با شکرت زهر داشت زیر نگین</p></div>
<div class="m2"><p>چو دور حلقه ی خاتم بگردنش رسنست</p></div></div>
<div class="b" id="bn26"><div class="m1"><p>بدور نقش نگین خجسته فرجامت</p></div>
<div class="m2"><p>که حرف روشن او شمع آسمان لگنست</p></div></div>
<div class="b" id="bn27"><div class="m1"><p>مثال نظم فغانی که یافت مهر قبول</p></div>
<div class="m2"><p>سواد خامه ی او مهر خاتم سخنست</p></div></div>
<div class="b" id="bn28"><div class="m1"><p>برین صحیفه ی فیروزه تا ز خامه ی صنع</p></div>
<div class="m2"><p>نشان دایره ی مهر نقطه پرنست</p></div></div>
<div class="b" id="bn29"><div class="m1"><p>نشان مهر تو بر کاینات باد روان</p></div>
<div class="m2"><p>چو آفتاب که طغرای حکم ذوالمننست</p></div></div>
<div class="b" id="bn30"><div class="m1"><p>فروغ مهر رخت باد همدم شاهی</p></div>
<div class="m2"><p>که نقش خاتم او نور دیده ی زمنست</p></div></div>
<div class="b" id="bn31"><div class="m1"><p>امین خاتم اقبال شاه اسماعیل</p></div>
<div class="m2"><p>که فرق تا قدمش لطف و سیرت حسنست</p></div></div>
<div class="b" id="bn32"><div class="m1"><p>نگین خاتم آن شاه واجب التعظیم</p></div>
<div class="m2"><p>نشان دولت و پروانه ی حیات منست</p></div></div>
<div class="b" id="bn33"><div class="m1"><p>بنام حیدر و آلست مهر خاتم او</p></div>
<div class="m2"><p>چو خاتمی که مرصع بگوهر عدنست</p></div></div>
<div class="b" id="bn34"><div class="m1"><p>چو حرف خاتم زر باد بر لب مه و مهر</p></div>
<div class="m2"><p>دعای شاه که ورد زبان مرد و زنست</p></div></div>