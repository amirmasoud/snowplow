---
title: >-
    شمارهٔ ۵۰۲
---
# شمارهٔ ۵۰۲

<div class="b" id="bn1"><div class="m1"><p>چو در فسانه لبت شهد بر شکر بسته</p></div>
<div class="m2"><p>هزار نکته ی شیرین بیکدگر بسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فغان که هندوی خالت بجلوه ی موزون</p></div>
<div class="m2"><p>بخون مردمک دیده ام کمر بسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بدور خط مگس خال زان لب شیرین</p></div>
<div class="m2"><p>نخاست زانکه دل از مهر بر شکر بسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخار هر مژه ام غنچه هاست بسته گره</p></div>
<div class="m2"><p>که قطره قطره ز خونابه ی جگر بسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوق گوهر لعل و قطره های سرشک</p></div>
<div class="m2"><p>دو رسته در صدف دیده ام گهر بسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز اشتیاق تو بر غیر بسته ام در دل</p></div>
<div class="m2"><p>بیا که شهر دلم ملک تست در بسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نهال قد تو در جلوه نازنین نخلیست</p></div>
<div class="m2"><p>که روزگار ز آشوب و فتنه بربسته</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ز سر غنچه ی لعلش دلا به آه سحر</p></div>
<div class="m2"><p>مجو گشاد که آن نکته ییست سربسته</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فروغ مهر جمال تو بر من حیران</p></div>
<div class="m2"><p>بهر طرف که نگه می کنم گذر بسته</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>ز حسرت تو فغانی بشاهراه خیال</p></div>
<div class="m2"><p>نهاده دیده و بر صورتت نظر بسته</p></div></div>