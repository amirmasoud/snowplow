---
title: >-
    شمارهٔ ۵۲۱
---
# شمارهٔ ۵۲۱

<div class="b" id="bn1"><div class="m1"><p>باز ای فلک نتیجهٔ انجم نموده‌ای</p></div>
<div class="m2"><p>دندان کین به اهل تنعم نموده‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید من چو ذره جهانیست در پِیَت</p></div>
<div class="m2"><p>از بس که روی گرم به مردم نموده‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو رخ نموده‌ای که دهم جان به یک نظر</p></div>
<div class="m2"><p>من زنده می‌شوم که ترحم نموده‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آن انجمن کجاست که چون ابر نوبهار</p></div>
<div class="m2"><p>من گریه کرده و تو تبسم نموده‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق چگونه تاب زبان تو آورد</p></div>
<div class="m2"><p>زین شیوه‌ها که وقت تکلم نموده‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>همچون فغانی از تو نگردم اگرچه تو</p></div>
<div class="m2"><p>هردم ره دگر به من گم نموده‌ای</p></div></div>