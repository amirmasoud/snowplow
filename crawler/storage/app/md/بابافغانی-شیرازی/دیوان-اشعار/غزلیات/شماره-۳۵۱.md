---
title: >-
    شمارهٔ ۳۵۱
---
# شمارهٔ ۳۵۱

<div class="b" id="bn1"><div class="m1"><p>سراسر شیوهٔ نازست سرو ناز پروردش</p></div>
<div class="m2"><p>ولی در جلوهٔ جولان نمی‌یابد کسی گردش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خیال جوهر فرد دهانش جان مشتاقان</p></div>
<div class="m2"><p>ز هستی فرد سازد جان فدای جوهر فردش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرفتاری که حیران جمال اوست روز و شب</p></div>
<div class="m2"><p>نبیند راحت از خواب و نباشد لذت از خوردش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کسی را سجده ی محراب ابرویش قبول افتد</p></div>
<div class="m2"><p>که اشک سرخ پیدا باشد از رخساره ی زردش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بقدر حال خود هر کس بدین در تحفه یی دارد</p></div>
<div class="m2"><p>من مسکین ندارم هیچ غیر از تحفه ی دردش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باظهار محبت هر که خود را مرد ره داند</p></div>
<div class="m2"><p>گر از تیغ ملامت رو بگرداند مخوان مردش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغانی با گل و گلزار عالم داشت دلگرمی</p></div>
<div class="m2"><p>هوای گلرخی از هستی خود ساخت دلسردش</p></div></div>