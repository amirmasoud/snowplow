---
title: >-
    شمارهٔ ۱۸۲
---
# شمارهٔ ۱۸۲

<div class="b" id="bn1"><div class="m1"><p>تو آن گلی که مه آسمان جبین تو بوسد</p></div>
<div class="m2"><p>ملک ز سد ره فرود آید و زمین تو بوسد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چنان لطیف مزاجی که جای بوسه بماند</p></div>
<div class="m2"><p>اگر نسیم صبا برگ یاسمین تو بوسد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رود نشانه ی دندان حسرت از لب عاشق</p></div>
<div class="m2"><p>دمی که می دهی و دست نازنین تو بوسد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخوبی آنکه سر از جیب آفتاب برآرد</p></div>
<div class="m2"><p>هنوز دل نپسندد که آستین تو بوسد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کسی که مهر خموشی بلعل نوش لبان زد</p></div>
<div class="m2"><p>در آرزوست که بگذاری و نگین تو بوسد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بمکتب تو ملازم بود فرشته ی رحمت</p></div>
<div class="m2"><p>که رشحه ی قلم سحر آفرین تو بوسد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نخورد عاشق لب تشنه می ز جام مرصع</p></div>
<div class="m2"><p>ازین هوس که مگر لعل آتشین تو بوسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ببین که تا بچه غایت رسید شوق فغانی</p></div>
<div class="m2"><p>که در خیال، دهان چو انگبین تو بوسد</p></div></div>