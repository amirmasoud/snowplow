---
title: >-
    شمارهٔ ۲۵۲
---
# شمارهٔ ۲۵۲

<div class="b" id="bn1"><div class="m1"><p>چکند دل که بدوران غمت خون نخورد</p></div>
<div class="m2"><p>می دهد خون جگر سوخته اش چون نخورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می خورد خون دلم غنچه ی لعل تو چنان</p></div>
<div class="m2"><p>که بدان میل کسی باده ی گلگون نخورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تشنه ی باده ی لعلت ز کف خضر و مسیح</p></div>
<div class="m2"><p>دم آبی به صد افسانه و افسون نخورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می برد مستی می عشوه ی چشمت ز سرم</p></div>
<div class="m2"><p>ورنه در دور تو کس می زمن افزون نخورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتشی می رسد از منزل لیلی بشتاب</p></div>
<div class="m2"><p>چاره یی نیست که بر خرمن مجنون نخورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میجهد شعله ی آهی ز دلت برق صفت</p></div>
<div class="m2"><p>دم نگهدار فغانی که بگردون نخورد</p></div></div>