---
title: >-
    شمارهٔ ۳۰۵
---
# شمارهٔ ۳۰۵

<div class="b" id="bn1"><div class="m1"><p>چون بدلسوزی من یار زبان تیز کند</p></div>
<div class="m2"><p>بسخن پسته ی خندان شکر آمیز کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر دهان تلخی فرهاد بدآمد، شیرین</p></div>
<div class="m2"><p>خنده بر انجمن عشرت پرویز کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عقل را جادوی بابل کند از غایت شوق</p></div>
<div class="m2"><p>عشق هر نکته که از لعل تو انگیز کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانه ی مرغ بلا خواره همان سنگ بلاست</p></div>
<div class="m2"><p>آسمان گر چمن دهر گهر ریز کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه چه دیده ست فغانی ز پی کسب نظر</p></div>
<div class="m2"><p>گر بفردوس رود رغبت تبریز کند</p></div></div>