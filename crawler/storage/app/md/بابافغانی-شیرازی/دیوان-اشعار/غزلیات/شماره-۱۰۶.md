---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>رویم شکفته از سخن تلخ مردمست</p></div>
<div class="m2"><p>زهرست در دهان و لبم در تبسمست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بیطاقتم چنانکه ندارم مجال صبر</p></div>
<div class="m2"><p>رحمی، به دل درآی که جای ترحمست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سیاره ی زبون چکند فتنه مهر تست</p></div>
<div class="m2"><p>در کار من گره نه از افلاک و انجمست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم حلاوت سخن پند گو ولی</p></div>
<div class="m2"><p>آفت زبان ساقی شیرین تکلمست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خون می چکد ز اطلس سیما بی سپهر</p></div>
<div class="m2"><p>بس رنگ بوالعجب که درین نیلگون حمست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>با هر که تاختی سر و جان باخت در رهت</p></div>
<div class="m2"><p>رخش ترا چه خون که نه در کاسه ی سمست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از هیچ رو نبرد فغانی رهی بدوست</p></div>
<div class="m2"><p>خضر رهش شوید که در کار خود گمست</p></div></div>