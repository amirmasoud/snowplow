---
title: >-
    شمارهٔ ۵۲۹
---
# شمارهٔ ۵۲۹

<div class="b" id="bn1"><div class="m1"><p>ای غنچهٔ تو در سخن از سر معنوی</p></div>
<div class="m2"><p>نخلت کرشمه‌بار ز انفاس عیسوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شیرین‌خرام من گذری کن به بوستان</p></div>
<div class="m2"><p>تا گل به مقدمت فگند تاج خسروی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل‌های نوشکفته به وصف تو در چمن</p></div>
<div class="m2"><p>هر یک سفینه‌ای‌ست ز درهای معنوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نقش جمالت از قلم صنع آیتی‌ست</p></div>
<div class="m2"><p>کاین شیوه‌ای‌ست در رقم کلک مانوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وصل تو گر به ترک علایق میسرست</p></div>
<div class="m2"><p>قطع نظر ز حاصل اسباب دنیوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمی و صد کرشمه، سری و هزار ناز</p></div>
<div class="m2"><p>ای فتنهٔ زمانه چه مستانه می‌روی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نوشد بلای عشق فغانی ز نو گلی</p></div>
<div class="m2"><p>پیرانه‌سر نهاده غمش روی درنوی</p></div></div>