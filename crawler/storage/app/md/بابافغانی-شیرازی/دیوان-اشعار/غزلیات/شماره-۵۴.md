---
title: >-
    شمارهٔ ۵۴
---
# شمارهٔ ۵۴

<div class="b" id="bn1"><div class="m1"><p>در مستان زدم تا حال هشیاران شود پیدا</p></div>
<div class="m2"><p>نهفتم قدر خود تا قیمت یاران شود پیدا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فلک ای کاش بردارد ز روی کارها پرده</p></div>
<div class="m2"><p>که نقد زاهدان و جنس میخواران شود پیدا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ز سیل فتنه چون در ورطه افتد زورق هستی</p></div>
<div class="m2"><p>در آن طوفان سرانجام سبکباران شود پیدا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوای ذره پروردن ندارد آفتاب من</p></div>
<div class="m2"><p>که استعداد هر یک زین هواداران شود پیدا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اگر معشوق نگشاید گره از گوشهٔ ابرو</p></div>
<div class="m2"><p>هزاران عقده در کار گرفتاران شود پیدا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به دور چشم مستت باده می‌نوشند و می‌ترسم</p></div>
<div class="m2"><p>که ناگه فتنه‌ای در بزم میخواران شود پیدا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شراب لعل در جامست و من در سجده سهوست این</p></div>
<div class="m2"><p>گذارم گر عذار لاله‌رخساران شود پیدا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغانی باده پنهان خور که حق از غایت رحمت</p></div>
<div class="m2"><p>نمی‌خواهد که کردار گنه‌کاران شود پیدا</p></div></div>