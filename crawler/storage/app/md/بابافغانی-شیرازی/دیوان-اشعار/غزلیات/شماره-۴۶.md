---
title: >-
    شمارهٔ ۴۶
---
# شمارهٔ ۴۶

<div class="b" id="bn1"><div class="m1"><p>بر دل فزود خال تو داغی دگر مرا</p></div>
<div class="m2"><p>افروخت از رخ تو چراغی دگر مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر جام می که در نظرم میدهی بغیر</p></div>
<div class="m2"><p>داغیست تازه بر سر داغی دگر مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایندم که بی رقیب روی گیرمت عنان</p></div>
<div class="m2"><p>زین خوبتر کجاست فراغی دگر مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر روز بهر دفع غم از خانه همدمی</p></div>
<div class="m2"><p>بیرون برد بگلشن و باغی دگر مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اما بجز نوید وصالت عجب که کس</p></div>
<div class="m2"><p>از ره برد بلابه و لاغی دگر مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>داغم از آن گلست فغانی درین چمن</p></div>
<div class="m2"><p>کی دل کشد بلاله و راغی دگر مرا</p></div></div>