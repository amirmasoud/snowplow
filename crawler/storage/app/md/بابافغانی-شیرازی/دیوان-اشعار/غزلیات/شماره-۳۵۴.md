---
title: >-
    شمارهٔ ۳۵۴
---
# شمارهٔ ۳۵۴

<div class="b" id="bn1"><div class="m1"><p>با کسان در صلح و با خود دایما در جنگ باش</p></div>
<div class="m2"><p>هیچکار از بیغمی نگشایدت دلتنگ باش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>طاعت و عشرت نگردد جمع با هم ای عزیز</p></div>
<div class="m2"><p>گر مرید پیر راهی یکدل و یکرنگ باش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پادشاهی مانع فقر و نقیض عشق نیست</p></div>
<div class="m2"><p>همت از دلهای آگه خواه و بر اورنگ باش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خضر اگر همره بود از دوری منزل چه باک</p></div>
<div class="m2"><p>وادی مقصود گو هر گام صد فرسنگ باش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون ندانستی که در اصل از کدام آب و گلی</p></div>
<div class="m2"><p>خواه لعل آتشین خواهی سفال و سنگ باش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پیر صحبت گفت بشنو هر که دارد قول راست</p></div>
<div class="m2"><p>گر نوای نی نباشد گو صدای چنگ باش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آه گرمت مجلس عشاق می آرد بجوش</p></div>
<div class="m2"><p>نیک می‌نالی فغانی بر همین آهنگ باش</p></div></div>