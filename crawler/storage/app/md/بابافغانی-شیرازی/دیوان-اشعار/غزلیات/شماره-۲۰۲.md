---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>نوبهار آمد که بوی گل جهان را خوش کند</p></div>
<div class="m2"><p>جرعه نوشان را شقایق نعل در آتش کند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خرم آن شاهد که نوشد جرعه ی بیغش بناز</p></div>
<div class="m2"><p>عاشق دلخسته از نظاره ی او غش کند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لاله خون ریزان، گل آتشبار و سوسن ده زبان</p></div>
<div class="m2"><p>مرغ سرگردان ازینها با که خاطر خوش کند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم و دل گردد زمین و آسمان، چون ماه من</p></div>
<div class="m2"><p>جلوه بر تخت روان و ناز بر ابرش کند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آهوان را چشم و مرغان را نظر مانده به راه</p></div>
<div class="m2"><p>تا کی این ترک شکاری دست در ترکش کند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمه یی طاقت نیارد گر بود صبح و شفق</p></div>
<div class="m2"><p>آنچه بر دل جام صاف و ساقی مهوش کند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبل طبع فغانی در گلستان نظر</p></div>
<div class="m2"><p>بهر تسخیر گلی این نغمهٔ دلکش کند</p></div></div>