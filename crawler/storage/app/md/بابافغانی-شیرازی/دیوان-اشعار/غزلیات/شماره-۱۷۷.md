---
title: >-
    شمارهٔ ۱۷۷
---
# شمارهٔ ۱۷۷

<div class="b" id="bn1"><div class="m1"><p>بازم بسینه عشق و جنون جوش می زند</p></div>
<div class="m2"><p>وز خون گرم دل بدرون جوش می زند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آسوده بودم آه که از یک نگاه گرم</p></div>
<div class="m2"><p>خونی که مرده بود کنون جوش می زند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر تا قدم گداختم از داغ عاشقی</p></div>
<div class="m2"><p>خونابه بنگرید که چون جوش می زند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جانم به لب رسید و هنوز از خیال خام</p></div>
<div class="m2"><p>در سینه آرزوی فزون جوش می زند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مور شکسته بال بشهد تو چون رسد</p></div>
<div class="m2"><p>کز طامعان درون و برون جوش می زند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زین کافری که کرد فلک با شهید عشق</p></div>
<div class="m2"><p>خون در نهاد خاک زبون جوش می زند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در دیده از هوای تو ای شاخ ارغوان</p></div>
<div class="m2"><p>هر دم هزار قطره ی خون جوش می زند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نتوان نگاه کرد بدان روی آتشین</p></div>
<div class="m2"><p>از بسکه خال غالیه گون جوش می زند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>هر دم ز خامی تو فغانی در آتشی</p></div>
<div class="m2"><p>بهر سواد سحر و فسون جوش می زند</p></div></div>