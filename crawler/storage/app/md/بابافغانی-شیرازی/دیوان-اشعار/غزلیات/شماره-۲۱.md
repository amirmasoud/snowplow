---
title: >-
    شمارهٔ ۲۱
---
# شمارهٔ ۲۱

<div class="b" id="bn1"><div class="m1"><p>عشقت مدام خون جگر میدهد مرا</p></div>
<div class="m2"><p>دردی نرفته درد دگر میدهد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>صدره بجستجوی تو کردم زخود سفر</p></div>
<div class="m2"><p>غافل همان نشان بسفر میدهد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>داری جواب تلخ و من از غایت امید</p></div>
<div class="m2"><p>خوش میکنم دهان که شکر میدهد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دل نشانده وعده ی وصلت نهال صبر</p></div>
<div class="m2"><p>این نخل تازه تا چه ثمر میدهد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با آفتاب همنفسم لیک آتشست</p></div>
<div class="m2"><p>آبی که از پیاله ی زر میدهد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پروا نمیکنی و بهر کس که دل دهم</p></div>
<div class="m2"><p>چون بیندم بداغ تو سر میدهد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این آه سوزناک فغانی زمان زمان</p></div>
<div class="m2"><p>از روزگار رفته خبر میدهد مرا</p></div></div>