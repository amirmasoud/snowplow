---
title: >-
    شمارهٔ ۳۰۳
---
# شمارهٔ ۳۰۳

<div class="b" id="bn1"><div class="m1"><p>نوروز علم برزد و گل در چمن آمد</p></div>
<div class="m2"><p>خورشید سفر کرده ی من در وطن آمد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرغی که ز هجران گلی داشت ملالی</p></div>
<div class="m2"><p>در باغ بنظاره سرو چمن آمد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل باز رسید از سفر و سرو ز گلگشت</p></div>
<div class="m2"><p>پیمانه بیارید که پیمان شکن آمد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یعقوب جوان شد ز صبا من شدم آتش</p></div>
<div class="m2"><p>آن بوی دگر بود کزان پیرهن آمد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همراه صبا بوی مسیحا نفسی بود</p></div>
<div class="m2"><p>زان بوی دل مرده ی من با سخن آمد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در عشق دمی زندگی آرد دو جهان غم</p></div>
<div class="m2"><p>آفت نه همان بود که بر کوهکن آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آشفته چنان نیستم از غم که بدانم</p></div>
<div class="m2"><p>کز شاخ چه گل سر زد و چون نسترن آمد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سرمست رسید از ره و خوبان بنظاره</p></div>
<div class="m2"><p>گشتند سراسیمه که هان پیلتن آمد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خاموش نشد از سخن عشق فغانی</p></div>
<div class="m2"><p>هرچند که سنگ ستمش بر دهن آمد</p></div></div>