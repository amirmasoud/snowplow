---
title: >-
    شمارهٔ ۳۶۳
---
# شمارهٔ ۳۶۳

<div class="b" id="bn1"><div class="m1"><p>میگدازم دیده تا یکروزه موجودم چو شمع</p></div>
<div class="m2"><p>می رسد بر اوج گردون هر نفس دودم چو شمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دیده ی اختر شمارم شهرت نیسان گرفت</p></div>
<div class="m2"><p>بسکه بر هر نوک مژگان گوهر آمودم چو شمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گرچه نقد هستیم در آتش عشق تو سوخت</p></div>
<div class="m2"><p>اندکی هم در صفای فطرت افزودم چو شمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون سپند از گرد مجلس دور کردم چشم بد</p></div>
<div class="m2"><p>خوابگاهت را بدود دل نیالودم چو شمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داشتم داغ ترا در سینه چون مجمر نهان</p></div>
<div class="m2"><p>راز پنهان را نقاب از چهره نگشودم چو شمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اشک گوهر زایم از خوناب دل شد لعل سان</p></div>
<div class="m2"><p>بسکه هر دم آستین بر چشم تر سودم چو شمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از دم گرم فغانی دود آهم در گرفت</p></div>
<div class="m2"><p>گرچه در راه محبت باد پیمودم چو شمع</p></div></div>