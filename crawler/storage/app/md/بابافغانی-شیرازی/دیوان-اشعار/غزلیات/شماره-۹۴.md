---
title: >-
    شمارهٔ ۹۴
---
# شمارهٔ ۹۴

<div class="b" id="bn1"><div class="m1"><p>دیوانه ی ترا هوس گشت باغ نیست</p></div>
<div class="m2"><p>در گلشنم مخوان که مرا این دماغ نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همکاسه چون شود بحریفان درد نوش</p></div>
<div class="m2"><p>آنرا که غیر پاره ی دل در ایاغ نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می سوزم و رقیب همان خنده می زند</p></div>
<div class="m2"><p>آتش هزار بار بر آن دل که داغ نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر من چگونه سایه ی مهر افگند همای</p></div>
<div class="m2"><p>کاین استخوان سوخته در خورد زاغ نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من عاشقم مراست پریشانی همه</p></div>
<div class="m2"><p>معشوق را چه شد که حضور و فراغ نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روشن ترست مردن شبهای من ز روز</p></div>
<div class="m2"><p>با آنکه در خرابه ی تارم چراغ نیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق چه کسب فیض کند زین سیه دلان</p></div>
<div class="m2"><p>هنگامه ییست این که درو غیر لاغ نیست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زین انجمن فغانی دیوانه چون رود</p></div>
<div class="m2"><p>یک لاله چون برنگ تو در هیچ باغ نیست</p></div></div>