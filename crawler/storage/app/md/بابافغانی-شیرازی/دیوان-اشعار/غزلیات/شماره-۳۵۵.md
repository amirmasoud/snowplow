---
title: >-
    شمارهٔ ۳۵۵
---
# شمارهٔ ۳۵۵

<div class="b" id="bn1"><div class="m1"><p>دل از عیش جهان کندیم و ذوق بادهٔ نابش</p></div>
<div class="m2"><p>نمی‌ارزد به ظلم شحنهٔ شب گشت مهتابش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلی کز روشنی هر ذره‌اش صد شب‌چراغ ارزد</p></div>
<div class="m2"><p>چرا بهر شراب تلخ اندازم به غرقابش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه شکر بخت خود گویم چو دیدم بر قرار اینجا</p></div>
<div class="m2"><p>فروغ بزم عشرت با فراغ کنج محرابش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه عیش از مستی یک ساعت شب، تیره‌روزان را</p></div>
<div class="m2"><p>که آتش از غم فردا بود در جامهٔ خوابش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلی باید چو کوهی دیده‌ای باید چو دریایی</p></div>
<div class="m2"><p>که با خورشیدرویی چون نشینی آوری تابش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به جام زر توان خوردن شراب لعل با خوبان</p></div>
<div class="m2"><p>چه سازد عاشق بی‌خان و مان چون نیست اسبابش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مپنداری که با مغزست نقل مجلس گردون</p></div>
<div class="m2"><p>هزار افسون و نیرنگست در بادام و عنابش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>مشو سرگرم اگر بخشد سپهرت خلعت خورشید</p></div>
<div class="m2"><p>که تیزی سنان دارد سر هر موی سنجابش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فغانی چون دلت سیری ندارد از می و ساقی</p></div>
<div class="m2"><p>به اصلاحش چه می‌کوشی بیفگن تا برد آبش</p></div></div>