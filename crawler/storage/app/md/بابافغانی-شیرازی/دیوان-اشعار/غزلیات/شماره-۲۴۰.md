---
title: >-
    شمارهٔ ۲۴۰
---
# شمارهٔ ۲۴۰

<div class="b" id="bn1"><div class="m1"><p>چه تندی است که سویت نگاه نتوان کرد</p></div>
<div class="m2"><p>نهفته روی نکویت نگاه نتوان کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ازین شراب که در کار عاشقان کردی</p></div>
<div class="m2"><p>دگر بجام و سبویت نگاه نتوان کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بشیوه های دگر زنده می کنی ما را</p></div>
<div class="m2"><p>بجور و تندی خویت نگاه نتوان کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه سود زین همه آب حیات وه که هنوز</p></div>
<div class="m2"><p>بسبزه ی لب جویت نگاه نتوان کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز بسکه دود برآوردی از دلم چو سپند</p></div>
<div class="m2"><p>بخال غالیه بویت نگاه نتوان کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین شراب کجا خوردی ای بهشتی رو</p></div>
<div class="m2"><p>که سیر بر گل رویت نگاه نتوان کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سگت فغانی دیوانه را کشید بخون</p></div>
<div class="m2"><p>فغان که بر سگ کویت نگاه نتوان کرد</p></div></div>