---
title: >-
    شمارهٔ ۳۰۴
---
# شمارهٔ ۳۰۴

<div class="b" id="bn1"><div class="m1"><p>گر تلخ شدی سوز تو از سینه کجا شد</p></div>
<div class="m2"><p>شیرینی درد از دل بی کینه کجا شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب یار و سحر دشمن جان این چه وفاییست</p></div>
<div class="m2"><p>خاصیت نقل و می دوشینه کجا شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از لخت کباب دل ما زود شدی سیر</p></div>
<div class="m2"><p>حق نمک صحبت دیرینه کجا شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق نشود تیره بیک آه ز مجنون</p></div>
<div class="m2"><p>نور و خرد طبع چو آیینه کجا شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مهر در گنجینه ی دل بود وفایت</p></div>
<div class="m2"><p>آن مهر وفا از در گنجینه کجا شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر چند بود سوختنی دلق فغانی</p></div>
<div class="m2"><p>آخر ادب خرقه ی پشمینه کجا شد</p></div></div>