---
title: >-
    شمارهٔ ۱۹
---
# شمارهٔ ۱۹

<div class="b" id="bn1"><div class="m1"><p>وای که تلخ شد دوا، بر دل پرگزند ما</p></div>
<div class="m2"><p>مرگ بود نه زندگی، داروی سودمند ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دو لبت نصیب ما، ناز و عتاب میشود</p></div>
<div class="m2"><p>وه که شراب تلخ شد، از تو گلاب و قند ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عاقبت مراد ما چون همه نامرادیست</p></div>
<div class="m2"><p>چیست بیکدو جام می اینهمه زهرخند ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشرت یکزمان ما محنت جاودانه شد</p></div>
<div class="m2"><p>بین که چه کار میکند طالع ارجمند ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر سر دار شعله زد آتش دل، همین بود</p></div>
<div class="m2"><p>پیش بلندهمتان مرتبه ی بلند ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غمزه ساقی ارچنین کار کند در استخوان</p></div>
<div class="m2"><p>عشق و جنون برآورد دود زبند بند ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست فغانی آنکه دست از تو رها کند دگر</p></div>
<div class="m2"><p>باش که صید اینچنین کم جهد از کمند ما</p></div></div>