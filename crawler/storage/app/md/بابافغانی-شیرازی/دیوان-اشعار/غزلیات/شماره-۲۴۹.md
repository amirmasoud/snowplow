---
title: >-
    شمارهٔ ۲۴۹
---
# شمارهٔ ۲۴۹

<div class="b" id="bn1"><div class="m1"><p>لعلت ازمی خنده بر برگ گل سیراب زد</p></div>
<div class="m2"><p>شمع رویت شعله بر خورشید عالمتاب زد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دید در محراب نقش طاق ابرویت امام</p></div>
<div class="m2"><p>شد دلش بیتاب و سر در گوشه ی محراب زد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دل که سوی غمزه ی مژگان خونریزت شتافت</p></div>
<div class="m2"><p>خویش را از بیخودی بر خنجر قصاب زد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیش خورشید رخت گل رفته بود از حال خود</p></div>
<div class="m2"><p>بر رخش ابر بهاران از ترحم آب زد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیوه ی چشم سیاهت فتنه ی ایام شد</p></div>
<div class="m2"><p>عشوه لعل چو قندت خنده بر عناب زد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر گل سیراب زد آب لطافت عارضت</p></div>
<div class="m2"><p>از حیا روی تو آتش در شراب ناب زد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بنده ی آن شاه خوبانم که در مصر جمال</p></div>
<div class="m2"><p>سکه ی خوبی برای رونق احباب زد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>هیچگه خونابه از چشم فغانی کم نشد</p></div>
<div class="m2"><p>بسکه از لعلت نمک بر دیده ی بیخواب زد</p></div></div>