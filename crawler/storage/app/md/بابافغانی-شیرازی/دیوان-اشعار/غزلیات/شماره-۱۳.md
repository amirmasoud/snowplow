---
title: >-
    شمارهٔ ۱۳
---
# شمارهٔ ۱۳

<div class="b" id="bn1"><div class="m1"><p>بهر گلشن که بینم مبتلایی رو نهم آنجا</p></div>
<div class="m2"><p>ز داغش آتشی افروزم و پهلو نهم آنجا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو بینم دردمندی بر سر ره بی‌خود افتاده</p></div>
<div class="m2"><p>به خاک افتم سر او بر سر زانو نهم آنجا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روم تا شهر بابل از جفای این سیه‌چشمان</p></div>
<div class="m2"><p>غم دل در میان با مردم جادو نهم آنجا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بهر منزل که بینم صحبت گرم تو با یاران</p></div>
<div class="m2"><p>هزاران داغ حسرت بر دل بدخو نهم آنجا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو بوی آشنایی از سگ کویت نمی‌یابم</p></div>
<div class="m2"><p>به صحرا افتم و سر در پی آهو نهم آنجا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو در گلشن برم مست و خرامانت به گل چیدن</p></div>
<div class="m2"><p>چه منتها که بر سرو و گل خود رو نهم آنجا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشینم چون فغانی روز جولان بر سر راهت</p></div>
<div class="m2"><p>که هرجا پای بردارد سمندت رو نهم آنجا</p></div></div>