---
title: >-
    شمارهٔ ۵۷۳
---
# شمارهٔ ۵۷۳

<div class="b" id="bn1"><div class="m1"><p>تو و حسن و کامرنی من و عشق و نامرادی</p></div>
<div class="m2"><p>که بروی خویش بستم در خرمی و شادی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ره و رسم نامرادی ز دل شکسته یی جو</p></div>
<div class="m2"><p>که قدم بهستی خود زده در هزار وادی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه بود سیاهی شب چو تویی چراغ منزل</p></div>
<div class="m2"><p>چه غم از درازی ره چو تویی دلیل و هادی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگذاشت برق عشقت اثری ز هستی ما</p></div>
<div class="m2"><p>چه حریف خانه سوزی که بوقت ما فتادی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو نساخت هیچکاری بمراد دل فغانی</p></div>
<div class="m2"><p>برهت نهاده مسکین سر عجز و نامرادی</p></div></div>