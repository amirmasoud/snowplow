---
title: >-
    شمارهٔ ۷۲
---
# شمارهٔ ۷۲

<div class="b" id="bn1"><div class="m1"><p>آنکه از لوح جفا نوک قلم باز گرفت</p></div>
<div class="m2"><p>زین دعا گوی چرا لطف و کرم باز گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مژده ی مهر و وفا می دهم یار مدام</p></div>
<div class="m2"><p>خود ندیدم که دمی جور و ستم باز گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>حال آن خسته چه باشد که طبیبش بعلاج</p></div>
<div class="m2"><p>خواست صدره بنهد پیش، قدم باز گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من همانروز ببستم نظر از آب حیات</p></div>
<div class="m2"><p>که فلک درد می از ساغر جم باز گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در بیابان مکافات یکی ده بدرود</p></div>
<div class="m2"><p>هر که یک دانه ز مرغان حرم باز گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می رسد گل که کند صد طبق لعل نثار</p></div>
<div class="m2"><p>گر بهار از قدم سبزه درم باز گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>قلم شوق فغانی ورقت کرد سیاه</p></div>
<div class="m2"><p>چند روزی که ازین صفحه قلم باز گرفت</p></div></div>