---
title: >-
    شمارهٔ ۱۵۸
---
# شمارهٔ ۱۵۸

<div class="b" id="bn1"><div class="m1"><p>مرغ دلم به حلقهٔ مویی نهاده رخ</p></div>
<div class="m2"><p>در باغ وصل بر گل رویی نهاده رخ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مست وصال چون نشود آنکه هر نفس</p></div>
<div class="m2"><p>بیخود بجیب غالیه بویی نهاده رخ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افگنده ام عنان دل از دست هر طرف</p></div>
<div class="m2"><p>در خون من دواسبه عدویی نهاده رخ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در گلشن خیال من از تند باد غم</p></div>
<div class="m2"><p>هر برگ لاله بر لب جویی نهاده رخ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دیده ام بجستن آن دانه ی گهر</p></div>
<div class="m2"><p>هر قطره ی سرشک بسویی نهاده رخ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در بزم عشق هر نفس از گرمی فراق</p></div>
<div class="m2"><p>لب تشنه یی به پای سبویی نهاده رخ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر صبح تا بشام فغانی بحاجتی</p></div>
<div class="m2"><p>گریان به قبلهٔ سر کویی نهاده رخ</p></div></div>