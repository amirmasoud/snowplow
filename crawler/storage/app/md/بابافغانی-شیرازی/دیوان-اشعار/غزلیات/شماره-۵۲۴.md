---
title: >-
    شمارهٔ ۵۲۴
---
# شمارهٔ ۵۲۴

<div class="b" id="bn1"><div class="m1"><p>ساقی چه سر گران به من زار گشته‌ای</p></div>
<div class="m2"><p>پیمانه‌ای بنوش که هشیار گشته‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در بحر خواب بودی و طوفان گرفته بود</p></div>
<div class="m2"><p>اکنون قیامتست که بیدار گشته‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قدر گلاب می‌شکند عطر دامنت</p></div>
<div class="m2"><p>معلوم می‌شود که به گلزار گشته‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ای جان رفتنی چه شتابست، یک زمان</p></div>
<div class="m2"><p>خوش باش چون به خسته‌دلان یار گشته‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من کز دو کون گشته‌ام آزاد سال‌هاست</p></div>
<div class="m2"><p>هستم غلام اگر تو خریدار گشته‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>پرهیز می‌کنند دلا از تو دوستان</p></div>
<div class="m2"><p>آخر چه دشمنی که چنین خوار گشته‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اخلاص این شکسته ندانسته‌ای هنوز</p></div>
<div class="m2"><p>عمری اگرچه در دل افگار گشته‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ای در مقام جنگ زده راه آشتی</p></div>
<div class="m2"><p>صنعت مکن که درد دل یار گشته‌ای</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر آستان عشق فغانی قرار گیر</p></div>
<div class="m2"><p>بنشین به یک مقام که بسیار گشته‌ای</p></div></div>