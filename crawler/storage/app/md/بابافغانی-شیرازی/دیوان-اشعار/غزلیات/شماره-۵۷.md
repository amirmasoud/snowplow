---
title: >-
    شمارهٔ ۵۷
---
# شمارهٔ ۵۷

<div class="b" id="bn1"><div class="m1"><p>دهی حیات ابد این دم از تو نیست عجب</p></div>
<div class="m2"><p>به یک کرشمه کشی این هم از تو نیست عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز من که سوخته ام عیش و خرمی عجبست</p></div>
<div class="m2"><p>تو شادزی که دل خرم از تو نیست عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هزار بار نمک بر جراحتم زده یی</p></div>
<div class="m2"><p>یکی اگر بنهی مرهم از تو نیست عجب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دگر ز خون شهیدان عشق طوفانست</p></div>
<div class="m2"><p>چنین هزار درین عالم از تو نیست عجب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنین که در خم زنار می کشی دل ما</p></div>
<div class="m2"><p>بکفر اگر بشود محکم از تو نیست عجب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مدام مست شراب غروری ای خواجه</p></div>
<div class="m2"><p>اگر ز دست دهی خاتم از تو نیست عجب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر آر ناله فغانی و خون ببار از چشم</p></div>
<div class="m2"><p>تو خانه سوخته یی ماتم از تو نیست عجب</p></div></div>