---
title: >-
    شمارهٔ ۱۳۲
---
# شمارهٔ ۱۳۲

<div class="b" id="bn1"><div class="m1"><p>بیگناهم خشم و نازت با من ای خود کام چیست</p></div>
<div class="m2"><p>یک طمع ناکرده زان لب این همه دشنام چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگزیده آن لب شیرین چه داند هر کسی</p></div>
<div class="m2"><p>کز تو بر جان من رسوای خون آشام چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار پیش دیده و دل همچنان در اضطراب</p></div>
<div class="m2"><p>سوختم این آتشم بر جان بی آرام چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی سخن گردد دل دشمن بحال من کباب</p></div>
<div class="m2"><p>گر برد بویی کزان خونخواره ام در جام چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داغ داغم کردی ایدل در تمنای وصال</p></div>
<div class="m2"><p>آتشم در جان زدی باز این خیال خام چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بیشتر عمرم از آن بدخو بناکامی گذشت</p></div>
<div class="m2"><p>بهر اندک روزگاری دیگر این ابرام چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شاخ گل در بر می آرد فغانی ز آب چشم</p></div>
<div class="m2"><p>عیش مردم تلخ شد این گریهٔ هر شام چیست</p></div></div>