---
title: >-
    شمارهٔ ۳۲۲
---
# شمارهٔ ۳۲۲

<div class="b" id="bn1"><div class="m1"><p>هلاک جانم از آن خط دلکشست هنوز</p></div>
<div class="m2"><p>اگر چه سبزه ی سیراب شد خوشست هنوز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فدای آن گل رویم که دستزد نشدست</p></div>
<div class="m2"><p>خراب آن می لعلم که بیغشست هنوز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگرد آینه اش خط سبز دایره ییست</p></div>
<div class="m2"><p>ولی ز آه دل ما مشوشست هنوز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز شوق آن لب میگون و خط زنگاری</p></div>
<div class="m2"><p>بخون سفینه ی دلها منقشست هنوز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نمیرود ز دلم لعل یار و خنده ی جام</p></div>
<div class="m2"><p>کجاست باده که نعلم در آتشست هنوز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گسسته رشته ی جانم هزار بار ز ناز</p></div>
<div class="m2"><p>بنیم بوسه دلم در کشاکشست هنوز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلا پی نی تیرش ز گوش پنبه برآر</p></div>
<div class="m2"><p>که آنچه می شنوی بانگ ترکشست هنوز</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغان گوشه نشینان ز گوش ابر گذشت</p></div>
<div class="m2"><p>سوار من چو مه نو بر ابر شست هنوز</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>سپید ساخت فغانی ز غصه موی سیاه</p></div>
<div class="m2"><p>دلش اسیر جوانان مهوشست هنوز</p></div></div>