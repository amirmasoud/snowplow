---
title: >-
    شمارهٔ ۴۵
---
# شمارهٔ ۴۵

<div class="b" id="bn1"><div class="m1"><p>آه کامشب دیده‌ام خوابی که می‌سوزد مرا</p></div>
<div class="m2"><p>خورده‌ام جایی می نابی که می‌سوزد مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>می‌تپد در خون دل بی‌صبر و یادم می‌دهد</p></div>
<div class="m2"><p>هردم از گلگشت مهتابی که می‌سوزد مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صحبت گرمی که دارد سر گرانم همچو شمع</p></div>
<div class="m2"><p>دیده‌ام زان ترک آدابی که می‌سوزد مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آه از آن جادو که چون می‌آورد لب در فسون</p></div>
<div class="m2"><p>نکته‌ای می‌گوید از بابی که می‌سوزد مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تشنه بودم بر لب آب و نخوردم جرعه‌ای</p></div>
<div class="m2"><p>دارم اکنون در جگر تابی که می‌سوزد مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از کجا برخاستی امروز سرو من که باز</p></div>
<div class="m2"><p>دارد آن روی چو گل آبی که می‌سوزد مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در نماز عاشقی شب‌ها فغانی تا به روز</p></div>
<div class="m2"><p>حالتی دارد به محرابی که می‌سوزد مرا</p></div></div>