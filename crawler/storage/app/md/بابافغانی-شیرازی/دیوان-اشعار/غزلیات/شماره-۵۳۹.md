---
title: >-
    شمارهٔ ۵۳۹
---
# شمارهٔ ۵۳۹

<div class="b" id="bn1"><div class="m1"><p>شب چون روم ز منزل آن ماه خرگهی</p></div>
<div class="m2"><p>از دیده سیل اشک نهد رو بهمرهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چندانکه رفتم از پی گرد سمند او</p></div>
<div class="m2"><p>روزی نشد که پر شود این دیده ی تهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر دم هزار قافله ی جان ببوی او</p></div>
<div class="m2"><p>آیند و بگذرند چو باد سحر گهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شرح درازی شب هجران نگویمت</p></div>
<div class="m2"><p>روز وصال اگر ننهند رو بکوتهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهی بعشق، ناصح عشاق می شدم</p></div>
<div class="m2"><p>دریافتم که بی خبری بود و ابلهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آسوده یی که مانع دل می شود بعشق</p></div>
<div class="m2"><p>از دل خبر ندارد و از عشق آگهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر خاک می نهم چو فغانی رخ نیاز</p></div>
<div class="m2"><p>هر جا که بر زمین قدم ناز می نهی</p></div></div>