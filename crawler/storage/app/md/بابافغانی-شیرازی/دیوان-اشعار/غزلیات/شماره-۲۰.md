---
title: >-
    شمارهٔ ۲۰
---
# شمارهٔ ۲۰

<div class="b" id="bn1"><div class="m1"><p>زبسکه داشتی ای گل همیشه خوار مرا</p></div>
<div class="m2"><p>نماند پیش کسان هیچ اعتبار مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسی امید بدل داشتم چو روی تو دید</p></div>
<div class="m2"><p>زدست رفت و نیامد بهیچ کار مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عجب اگر نروم از میان که مجنون دوش</p></div>
<div class="m2"><p>بخواب آمد و بگرفت در کنار مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هنوز سوزدم از داغ آرزوی تو دل</p></div>
<div class="m2"><p>گهی که لاله دمد از سر مزار مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوای خود زکه جویم که تا تو برگشتی</p></div>
<div class="m2"><p>شدست دشمن جان آنکه بود یار مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه من زسنگ جفای تو دل شکسته شدم</p></div>
<div class="m2"><p>که در فراق، چنین ساخت روزگار مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بشهر و کوی فغانی کسم نمیباید</p></div>
<div class="m2"><p>که نیست بی مه خود هیچ جا قرار مرا</p></div></div>