---
title: >-
    شمارهٔ ۳۴۵
---
# شمارهٔ ۳۴۵

<div class="b" id="bn1"><div class="m1"><p>می‌رسد عشق و دل افسرده می‌آرد به جوش</p></div>
<div class="m2"><p>آه ازین آتش که خون مرده می‌آرد به جوش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما هلاک غمزهٔ آن شوخ و او گرم شکار</p></div>
<div class="m2"><p>باز خون صید پیکان خورده می‌آرد به جوش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می‌رود مستانه می‌گوید بسوز و دم مزن</p></div>
<div class="m2"><p>این سخن‌ها عاشق آزرده می‌آرد به جوش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تنگدل ماییم ورنه غنچهٔ او را چه باک</p></div>
<div class="m2"><p>زان که جان‌های به لب آورده می‌آرد به جوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رفته بودم در عدم از یک اشارت باز خواند</p></div>
<div class="m2"><p>آن مسیحا صد چنین دل مرده می‌آرد به جوش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتشی هست اینکه می‌ریزد فغانی اشک گرم</p></div>
<div class="m2"><p>وز جگر این قطرهٔ نشمرده می‌آرد به جوش</p></div></div>