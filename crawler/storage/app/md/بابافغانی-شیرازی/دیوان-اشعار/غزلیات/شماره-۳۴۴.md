---
title: >-
    شمارهٔ ۳۴۴
---
# شمارهٔ ۳۴۴

<div class="b" id="bn1"><div class="m1"><p>از پی دل مرو و عاشق بی باک مباش</p></div>
<div class="m2"><p>ما غم عشق تو داریم تو غمناک مباش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست چالاکتر از قد تو در گلشن حسن</p></div>
<div class="m2"><p>از هوس مایل هر قامت چالاک مباش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای تو خود مرهم ریش دل خونین جگران</p></div>
<div class="m2"><p>در خیال دل ریش و جگر چاک مباش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خاک شد بر سر راه تو بسی جان عزیز</p></div>
<div class="m2"><p>دامن افشان چو روی غافل ازین خاک مباش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما چو آیینه دل از غیر تو پرداخته ایم</p></div>
<div class="m2"><p>یکنفس غافل ازین آینه ی پاک مباش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>باز از ادراک فغانی چو رود جانب عشق</p></div>
<div class="m2"><p>ناصح او مشو و منکر ادراک مباش</p></div></div>