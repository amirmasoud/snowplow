---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>صد شعله ی آه از دل هر گوشه نشین خاست</p></div>
<div class="m2"><p>آه این چه بلا بود که از خانه ی زین خاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آشفته و کاکل بسر دوش فگنده</p></div>
<div class="m2"><p>گویا که همین دم ز پریخانه ی چین خاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دشمن چه فسون خواند که آن شمع دل افروز</p></div>
<div class="m2"><p>بنشست چو شاخ گل خندان و حزین خاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر چند کزین صف شکنان گوشه گرفتم</p></div>
<div class="m2"><p>از جای دگر سخت کمانی ز کمین خاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در خاتم فیروزه ی شیرین چه حلاوت</p></div>
<div class="m2"><p>چون زهر جداییش هم از زیر نگین خاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر زد ز دلم صد بته ی صبر بیندیش</p></div>
<div class="m2"><p>زین تلخ گیاهی که ازین شوره زمین خاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر بار که شمشیر ترا دید فغانی</p></div>
<div class="m2"><p>آنگونه برآشفت که مویش ز جبین خاست</p></div></div>