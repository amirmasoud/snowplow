---
title: >-
    شمارهٔ ۱۰۲
---
# شمارهٔ ۱۰۲

<div class="b" id="bn1"><div class="m1"><p>شمع من میل منت امروز چون هر روز نیست</p></div>
<div class="m2"><p>وان نگاه گرم و شکر خندهٔ جان‌سوز نیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بی‌سخن آن شکل مخمورانه خواهد کشتنم</p></div>
<div class="m2"><p>حاجت گفتار تلخ و غمزهٔ دلدوز نیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک بیک اسباب حسنت آتش انگیزست لیک</p></div>
<div class="m2"><p>هیچ دل‌سوزان‌تر از لب‌های سحرآموز نیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاب دیگر دارد آن عارض که سوزد خلق را</p></div>
<div class="m2"><p>ورنه هیچ آتش بدین صورت جهان افروز نیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا بکشتن بر نیاید کام از پیش توام</p></div>
<div class="m2"><p>وه که این بخت زبونم هیچ جا فیروز نیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آه گرمم گر دهد وی کباب دل چه سود</p></div>
<div class="m2"><p>بوی عشقست این فغانی نکهت نوروز نیست</p></div></div>