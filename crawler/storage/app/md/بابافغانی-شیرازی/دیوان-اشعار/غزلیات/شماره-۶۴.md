---
title: >-
    شمارهٔ ۶۴
---
# شمارهٔ ۶۴

<div class="b" id="bn1"><div class="m1"><p>گل گل رخت ز دیده ی نمناک من شکفت</p></div>
<div class="m2"><p>گلزار حسنت از نظر پاک من شکفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خون می چکد ز داغ دل لاله در چمن</p></div>
<div class="m2"><p>گویا همین دم از جگر چاک من شکفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر گل که نقشبند جمال تو نقش بست</p></div>
<div class="m2"><p>در جویبار دیده ی نمناک من شکفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر روزگار کشته ی هجر تو خون گریست</p></div>
<div class="m2"><p>هر لاله یی که صبحدم از خاک من شکفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رویش که نوگلیست فغانی ز باغ حسن</p></div>
<div class="m2"><p>بهر جلای دیده ی ادراک من شکفت</p></div></div>