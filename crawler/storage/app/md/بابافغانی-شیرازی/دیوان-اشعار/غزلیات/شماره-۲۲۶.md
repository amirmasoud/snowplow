---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>کنون که باد خزان فرش لعل فام کشید</p></div>
<div class="m2"><p>خوش آنکه در صف مستان نشست و جام کشید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم که جام نگون داشت سالها چو حباب</p></div>
<div class="m2"><p>ببین که موج شرابش چسان بدام کشید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خزان در آمدن آن سوار حاضر بود</p></div>
<div class="m2"><p>که در رهش ورق زر باحترام کشید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فلک بداد مرادم چنانکه دل می خواست</p></div>
<div class="m2"><p>ولی ز هر سر مویم صد انتقام کشید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شدم اسیر شکار افگنی که صد باره</p></div>
<div class="m2"><p>سنان بدیده ی شیران تیز گام کشید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار جرعه ی فیضست در قرابه ی عشق</p></div>
<div class="m2"><p>خوش آن حریف که این باده را تمام کشید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چگونه لذت ذوق وصال دریابد</p></div>
<div class="m2"><p>ز یار هر که نه بعد از فراق کام کشید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن فتاده که هر چند یار سرکش بود</p></div>
<div class="m2"><p>بگرمی نفسش بر کنار بام کشید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بسیل داد فغانی روان سفینه ی عشق</p></div>
<div class="m2"><p>نه نام ننگ شنید و نه ننگ نام کشید</p></div></div>