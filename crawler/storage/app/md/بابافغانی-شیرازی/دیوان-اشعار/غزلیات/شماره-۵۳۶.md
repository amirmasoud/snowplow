---
title: >-
    شمارهٔ ۵۳۶
---
# شمارهٔ ۵۳۶

<div class="b" id="bn1"><div class="m1"><p>دارد نسیم گل دم جانبخش عیسوی</p></div>
<div class="m2"><p>تا بوی گل ز گلشن مقصود بشنوی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آیینه ی جمال تو در چشم اهل دید</p></div>
<div class="m2"><p>دارد هزار جلوه ی صوری و معنوی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ما را چو در سخن لب لعل تو جان دهد</p></div>
<div class="m2"><p>دیگر چه احتیاج بانفاس عیسوی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد چو قرب کعبه ی وصل تو در نیافت</p></div>
<div class="m2"><p>بیچاره شد بزاویه ی هجر منزوی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پرتو کدام و نور کدام ای خداشناس</p></div>
<div class="m2"><p>تا کی دو دل ز تفرقه ی نور و پرتوی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ای دل گدایی در میخانه کار تست</p></div>
<div class="m2"><p>ما را چه کار با طرب و عیش خسروی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر جا که هست دیده ز روی تو روشنست</p></div>
<div class="m2"><p>ای روشنی دیده چرا دور می روی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>تخم امل بباغ جهان کشته یی ولی</p></div>
<div class="m2"><p>جز بار دل فغانی از این کشته ندروی</p></div></div>