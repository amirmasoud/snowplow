---
title: >-
    شمارهٔ ۴۸۱
---
# شمارهٔ ۴۸۱

<div class="b" id="bn1"><div class="m1"><p>فصل خزان گذشت و رخ زرد من همان</p></div>
<div class="m2"><p>بلبل ز ناله ماند و دم سرد من همان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رنگ از رخ چمن شد و برگ درخت ریخت</p></div>
<div class="m2"><p>وین داغ کهنه بر دل پر درد من همان</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشتم غبار و رفتم ازین خاکدان برون</p></div>
<div class="m2"><p>باشد براه او اثر گرد من همان</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نتوان بصد چراغ دلی در زمانه یافت</p></div>
<div class="m2"><p>بر اوج دلبری مه شبگرد من همان</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نقش مراد خویش فغانی نیافتم</p></div>
<div class="m2"><p>ماندست با فلک بعقب نردمن همان</p></div></div>