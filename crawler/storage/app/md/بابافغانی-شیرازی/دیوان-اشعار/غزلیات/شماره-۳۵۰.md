---
title: >-
    شمارهٔ ۳۵۰
---
# شمارهٔ ۳۵۰

<div class="b" id="bn1"><div class="m1"><p>فردا که هر غنیم نماید غنیم خویش</p></div>
<div class="m2"><p>دست منست و دامن یار قدیم خویش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یا رب بمذهب که بود سوختن روا</p></div>
<div class="m2"><p>آنرا که پرورند بناز و نعیم خویش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر پی برد غنی که چه سودست در کرم</p></div>
<div class="m2"><p>ریزد چو آب در قدم خلق سیم خویش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>یاری کجاست تا بخرابات رو نهیم</p></div>
<div class="m2"><p>کز دست داده ایم ره مستقیم خویش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نازکترست از آنکه توان داد ازو نشان</p></div>
<div class="m2"><p>آن گل که تازه ساخت جهان از نسیم خویش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما در عرق زرنگ خوش و بوی دلکشش</p></div>
<div class="m2"><p>او بی نیاز چون گل و می از شمیم خویش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عاشق نه آنکسست که معشوق دلنواز</p></div>
<div class="m2"><p>سازد برود و باده مدامش ندیم خویش</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نام از کرم ثبات پذیرد نه از درم</p></div>
<div class="m2"><p>این نکته گفت حاتم طی با ندیم خویش</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>دانستنیست سر محبت نه گفتنی</p></div>
<div class="m2"><p>بگذار فهم نکته بطبع سلیم خویش</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>محرم نشد فغانی درویش کان غیور</p></div>
<div class="m2"><p>میر اندیش بتیر ز گرد حریم خویش</p></div></div>