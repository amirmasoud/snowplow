---
title: >-
    شمارهٔ ۲۹۵
---
# شمارهٔ ۲۹۵

<div class="b" id="bn1"><div class="m1"><p>وقتست ای حریف که می در سبو کنند</p></div>
<div class="m2"><p>دردیکشان بمنزل مقصود رو کنند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما جوی شیر و قصر زبرجد گذاشتیم</p></div>
<div class="m2"><p>ساقی بگو که میکده را رفت و رو کنند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می ده که وضع میکده بی مصلحت نشد</p></div>
<div class="m2"><p>کاری که می کنند حکیمان نکو کنند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>امروز داد مرشد ما رخصت شراب</p></div>
<div class="m2"><p>اما به این قرار که کم گفتگو کنند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذار کار توبه ی صوفی بساقیان</p></div>
<div class="m2"><p>تا اندک اندکی بگلویش فرو کنند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مشکل حکایتیست که هر ذره عین اوست</p></div>
<div class="m2"><p>اما نمی توان که اشارت به او کنند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خوبان ز آب دیده ی ما غافلند حیف</p></div>
<div class="m2"><p>زین یوسفان که جامه بخون شستشو کنند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>قسمت نگر که کشته ی شمشیر عشق یافت</p></div>
<div class="m2"><p>مرگی که زندگان بدعا آرزو کنند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آلوده ی شراب فغانی به خاک رفت</p></div>
<div class="m2"><p>آه ار ملایکش کفن تازه بو کنند</p></div></div>