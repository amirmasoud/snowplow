---
title: >-
    شمارهٔ ۴۲۶
---
# شمارهٔ ۴۲۶

<div class="b" id="bn1"><div class="m1"><p>ز شوق آنکه خواند نامه‌ام را آنچنان شادم</p></div>
<div class="m2"><p>که در وقت نوشتن می‌رود نام خود از یادم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به خون دل نوشتم نامه و سویش روان کردم</p></div>
<div class="m2"><p>بخواند یا نه باری من نیاز خود فرستادم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم بی‌اختیار از بخت جوید هردم آزادی</p></div>
<div class="m2"><p>مگر از بنده یادآور جایی سرو آزادم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز یبماری چنان گشتم که گر عمرم امان بخشد</p></div>
<div class="m2"><p>به راهی افگنم خود را که سویت آورد بادم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به جان بندم میان شمع و از سر سوختن گیرم</p></div>
<div class="m2"><p>به شکر آنکه در بزمت قبول خدمت افتادم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنای صبر اگر محکم نباشد، در دل ویران</p></div>
<div class="m2"><p>برد دور از سر کوی تو آب دیده بنیادم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دل من نامه در دست گر بگشایمش روزی</p></div>
<div class="m2"><p>شود روشن فغانی موجب افغان و فریادم</p></div></div>