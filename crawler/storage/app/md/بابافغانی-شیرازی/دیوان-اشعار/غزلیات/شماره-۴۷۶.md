---
title: >-
    شمارهٔ ۴۷۶
---
# شمارهٔ ۴۷۶

<div class="b" id="bn1"><div class="m1"><p>اگر یاد آرمش یکدم که از دل غم برد بیرون</p></div>
<div class="m2"><p>غمی آید که بازم بیخود از عالم برد بیرون</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود از مردنم دشوارتر دلسوزی همدم</p></div>
<div class="m2"><p>چه باشد گر ز بالین من این ماتم برد بیرون</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خراش سینه افزون می کند ناز طبیبانم</p></div>
<div class="m2"><p>خوشا بیهوشیی کز دل غم مرهم برد بیرون</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هم از نظاره اش آخر ز بیدادی شوم کشته</p></div>
<div class="m2"><p>کسی جان از بلای عشقبازی کم برد بیرون</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان بی صبرم از رویش که گر تیغم زند بر سر</p></div>
<div class="m2"><p>نخواهم کز بر من یکدمش محرم برد بیرون</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چنین قهری که دارد بر فغانی آن جفا پیشه</p></div>
<div class="m2"><p>عجب کز مجلسش یک ره دل خرم برد بیرون</p></div></div>