---
title: >-
    شمارهٔ ۴۳۴
---
# شمارهٔ ۴۳۴

<div class="b" id="bn1"><div class="m1"><p>ساقی خرابم از طرب دوش چون کنم</p></div>
<div class="m2"><p>از دستت این شراب دگر نوش چون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لب می گزی که زود چرا مست می شوی</p></div>
<div class="m2"><p>ساغر تو می دهی، من مدهوش چون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویند جامه می دری و آه می کشی</p></div>
<div class="m2"><p>با این سهی قدان قباپوش چون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دانم که هست از تو مرادم خیال خام</p></div>
<div class="m2"><p>این آرزو نایستد از جوش چون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل گوید این فسانه مرا اختیار نیست</p></div>
<div class="m2"><p>خود را ز گفتگوی تو خاموش چون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دشنام می دهی که مجو وصل و صبر کن</p></div>
<div class="m2"><p>تلخست ترک من سخنت، گوش چون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روز از غمت زیاد برم محنت خمار</p></div>
<div class="m2"><p>این بزم چون بهشت، فراموش چون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صدره سرم بخاک عدم دادی و هنوز</p></div>
<div class="m2"><p>سوزم، که با تو دست در آغوش چون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>تاب دلم نماند فغانی و آن حریف</p></div>
<div class="m2"><p>کاکل نمی کند ز سر دوش چون کنم</p></div></div>