---
title: >-
    شمارهٔ ۳۱۰
---
# شمارهٔ ۳۱۰

<div class="b" id="bn1"><div class="m1"><p>ای عارضت به بوسه ز لب دلنوازتر</p></div>
<div class="m2"><p>آبت ز آتش همه کس جانگدازتر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شمعیست قامت تو که در جلوه ی جمال</p></div>
<div class="m2"><p>هست از تمام کج کلهان سرفرازتر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از تکبر تو که بیگانه تر شوی</p></div>
<div class="m2"><p>هرچند دارمت من مسکین بنازتر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیداد کن که حسن اگر اینست هر زمان</p></div>
<div class="m2"><p>دستت بود بعاشق مسکین درازتر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دوری تو دیده ی شب زنده دار من</p></div>
<div class="m2"><p>دارد شبی ز روز قیامت درازتر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کردی نگاه و اهل نظر را نواختی</p></div>
<div class="m2"><p>معشوق کس ندیده ز تو چشم بازتر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نشکفته است در چمن حسن و دلبری</p></div>
<div class="m2"><p>شاخ گلی ز دلبر ما چشم بازتر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دل چون نهم بعشوه ی خوبان که این گروه</p></div>
<div class="m2"><p>هستند هر یک از دگری عشوه سازتر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ناز ترا کشید فغانی بصد نیاز</p></div>
<div class="m2"><p>هرچند ساخت عشق تواش بی نیازتر</p></div></div>