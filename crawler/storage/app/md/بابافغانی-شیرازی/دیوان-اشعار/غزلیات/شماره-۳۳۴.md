---
title: >-
    شمارهٔ ۳۳۴
---
# شمارهٔ ۳۳۴

<div class="b" id="bn1"><div class="m1"><p>آتشم در جان و در دل حسرت جامست و بس</p></div>
<div class="m2"><p>حاصل عمرم همین اندیشهٔ خامست و بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جام یاقوت و شراب لعل خاصان را رسد</p></div>
<div class="m2"><p>بی‌نوایان را نظر بر رحمت عامست و بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صد سخن در ضمن هر یک نکتهٔ شیرین اوست</p></div>
<div class="m2"><p>اضطراب دل نه از شادی پیغماست و بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشئهٔ خاصی‌ست در هر برگ این عشرت‌سرا</p></div>
<div class="m2"><p>غیر پندارد که مستی در می و جامست و بس</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پی به مقصد برکه نبود بی‌مسمیٰ هیچ اسم</p></div>
<div class="m2"><p>اینکه می‌گویند عَنقایی همین نامست و بس</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از زبان راست قولی، نکته‌ای کردم سؤال</p></div>
<div class="m2"><p>گفت دم درکش که خاموشی سرانجامست و بس</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درد می‌باید فغانی نه همین درس و دعا</p></div>
<div class="m2"><p>ورد عاشق آه صبح و گریه شامست و بس</p></div></div>