---
title: >-
    شمارهٔ ۲۴۱
---
# شمارهٔ ۲۴۱

<div class="b" id="bn1"><div class="m1"><p>میخواره ی مرا لب خندان نگه کنید</p></div>
<div class="m2"><p>زان شکل آنچه می کشدم آن نگه کنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناگه سیاستی بنماید غیور من</p></div>
<div class="m2"><p>گفتم هزار بار که پنهان نگه کنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ای گلرخان به صورت آن ترک بنگرید</p></div>
<div class="m2"><p>چشم سیاه و زلف پریشان نگه کنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بیباک من رسید دگر مست و سرگران</p></div>
<div class="m2"><p>طرف کلاه و چاک گریبان نگه کنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا چند منع ما ز خرابی و بیخودی</p></div>
<div class="m2"><p>یکبار آن کرشمه و جولان نگه کنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر دیده نیست آگه از آن صورت غریب</p></div>
<div class="m2"><p>خوبی او ازین دل ویران نگه کنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در یکزمان وصل چه درد از دلم رود</p></div>
<div class="m2"><p>عمری بلا و محنت هجران نگه کنید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داغی که در دلست فغانی خسته را</p></div>
<div class="m2"><p>زین آه گرم و ناله ی سوزان نگه کنید</p></div></div>