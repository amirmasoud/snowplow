---
title: >-
    شمارهٔ ۴۲۰
---
# شمارهٔ ۴۲۰

<div class="b" id="bn1"><div class="m1"><p>ما نقد جان بگوشه ی میخانه برده ایم</p></div>
<div class="m2"><p>دل را بچشم و غمزه ی ساقی سپرده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون در حریم میکده مستان نوا کنند</p></div>
<div class="m2"><p>ما هم برآوریم صدایی نمرده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در اشک ما مبین بحقارت که این شراب</p></div>
<div class="m2"><p>از پرده های دیده ی روشن فشرده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیکان آبدار ز تیر و کمان چرخ</p></div>
<div class="m2"><p>دلخواه تر ز قطره ی باران شمرده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از بسکه خورده ایم فرو آتش نهان</p></div>
<div class="m2"><p>مردم گمان برند که آبی فسرده ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ما آن درخت بادیه خیزیم ای صبا</p></div>
<div class="m2"><p>کز تندباد حادثه صد زخم خورده ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صدره باشک گرم فغانی و برق آه</p></div>
<div class="m2"><p>نقش خودی ز صفحه ی خاطر سترده ایم</p></div></div>