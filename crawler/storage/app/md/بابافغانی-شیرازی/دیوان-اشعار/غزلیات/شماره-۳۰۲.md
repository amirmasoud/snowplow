---
title: >-
    شمارهٔ ۳۰۲
---
# شمارهٔ ۳۰۲

<div class="b" id="bn1"><div class="m1"><p>طبع تو بدخوی بود نرم و حلیم از چه شد</p></div>
<div class="m2"><p>آنکه سر فتنه داشت یار و ندیم از چه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>رفت ز دامان تو گرد حیا میل میل</p></div>
<div class="m2"><p>سرمه ی اقبال ما بخش نسیم از چه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گشت هوای توام همدم عهد ازل</p></div>
<div class="m2"><p>با من خاکی ملک یار قدیم از چه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گنج تمنای تو تاب نیارد ملک</p></div>
<div class="m2"><p>در دل ویران ما وه که مقیم از چه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ساخت اسیر توام نغمه ی قانون بزم</p></div>
<div class="m2"><p>دام ره اهل دل پرده ی سیم از چه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوختم از آه گرم شعله چو افزود عشق</p></div>
<div class="m2"><p>نور چراغ دلم نار جحیم از چه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گرنه صباح ازل تیغ سیاست زدی</p></div>
<div class="m2"><p>سیب مه و آفتاب از تو دو نیم از چه شد</p></div></div>