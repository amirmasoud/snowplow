---
title: >-
    شمارهٔ ۳۲۶
---
# شمارهٔ ۳۲۶

<div class="b" id="bn1"><div class="m1"><p>ما گرفتاریم بر ما ناوک بیداد ریز</p></div>
<div class="m2"><p>سوسن و گل در کنار مردم آزاد ریز</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قطره ی خونابه ام در آتش گلخن فگن</p></div>
<div class="m2"><p>پاره ی خاکسترم در رهگذار باد ریز</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خار خشک ما سزاوار سموم آتشست</p></div>
<div class="m2"><p>آسمان گو آب رحمت بر گل و شمشاد ریز</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ایکه با شیرین لبالب میزنی جام مراد</p></div>
<div class="m2"><p>جرعه یی گر می توانی بر گل فرهاد ریز</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>استخوانم ریخت وز نو مینهم بنیاد عشق</p></div>
<div class="m2"><p>از پر خود ای هما گردی برین بنیاد ریز</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خواهد از بسیاری غم بردنم خواب عدم</p></div>
<div class="m2"><p>جرعه یی از ساغر خود بر من ناشاد ریز</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>برگذرگاه فغانی خار هم باشد دریغ</p></div>
<div class="m2"><p>ای صبا نسرین و گل بر منزل آباد ریز</p></div></div>