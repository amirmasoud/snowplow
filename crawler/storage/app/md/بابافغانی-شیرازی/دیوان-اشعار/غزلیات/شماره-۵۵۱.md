---
title: >-
    شمارهٔ ۵۵۱
---
# شمارهٔ ۵۵۱

<div class="b" id="bn1"><div class="m1"><p>چه شد کز صحبت یاران چنین رنجیده می‌آیی</p></div>
<div class="m2"><p>ز گلزاری که می‌رفتی گلی ناچیده می‌آیی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گلت از غیرت آه کدامین تشنه می‌جوشد</p></div>
<div class="m2"><p>که در آب و عرق زین گونه تر گردیده می‌آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کسی باید که بیند یک نظر شکل پر آشوبت</p></div>
<div class="m2"><p>چنان شاهانه چون تاج و کمر بخشیده می‌آیی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نمی‌گویم که رحمی بر فغان و گریهٔ من کن</p></div>
<div class="m2"><p>تو کز ناز و جفا بر دیگران خندیده می‌آیی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه افسونت چنین دیوانه‌وَش دارد نمی‌دانم</p></div>
<div class="m2"><p>که هرجا می‌روی یک دم نیارامیده می‌آیی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>به راهت هر قدم چشم و دلی در خاک و خون مانده</p></div>
<div class="m2"><p>تو بی‌باکانه دامن از زمین درچیده می‌آیی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>جگر سوزد کجا گفت فغانی بشنوی چون تو</p></div>
<div class="m2"><p>نوای بلبل و آواز نی نشنیده می‌آیی</p></div></div>