---
title: >-
    شمارهٔ ۲۶۴
---
# شمارهٔ ۲۶۴

<div class="b" id="bn1"><div class="m1"><p>دلم روانشد و جان هم ره سفر گیرد</p></div>
<div class="m2"><p>که از مسافر ره دور من خبر گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کف غبارم و جایی رسم بدولت عشق</p></div>
<div class="m2"><p>گرم نسیم عنایت ز خاک برگیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تو نازنینی و ما دردمند درد آشام</p></div>
<div class="m2"><p>میان ما و تو صحبت چگونه در گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز تاب شمع رخت آتشیست در دل من</p></div>
<div class="m2"><p>که گر نفس نکشم شعله در جگر گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بپایبوس تو آن کس رسد که چون خورشید</p></div>
<div class="m2"><p>اگر خرام کنی مقدمت بزر گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>لبت بوعده ی شیرین و خنده ی نمکین</p></div>
<div class="m2"><p>هزار نکته ی باریک بر شکر گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشد گلاب فغانی روان ز شیشه ی دل</p></div>
<div class="m2"><p>گرت ز ناله ی عشاق دردسر گرد</p></div></div>