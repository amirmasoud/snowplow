---
title: >-
    شمارهٔ ۳۷۹
---
# شمارهٔ ۳۷۹

<div class="b" id="bn1"><div class="m1"><p>زبان در ذکر و در دل نقش زلف یار می بندم</p></div>
<div class="m2"><p>مسلمانی اگر اینست من زنار می بندم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بتنگ از من در و دیوار من از بهر دیداری</p></div>
<div class="m2"><p>چو نقش خامه خود را بر در و دیوار می بندم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمادم شکر و بادام او در عشوه با مردم</p></div>
<div class="m2"><p>من از غیرت نمک بر دیده ی خونبار می بندم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا غمهای دیگر می کشد در عشق مهرویان</p></div>
<div class="m2"><p>ز تاب درد تهمت بر دل افگار می بندم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به دست آرد گلی از گلشن امید خود هر کس</p></div>
<div class="m2"><p>مرا خون در جگر شد بسکه در دل خار می بندم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بکام دشمنان برخاستم از مجلس رندان</p></div>
<div class="m2"><p>خیال دوستی با مردم هشیار می بندم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نگاهی می کنم همچون فغانی از سر حسرت</p></div>
<div class="m2"><p>ز پیکان رخنهای دیده ی بیدار می بندم</p></div></div>