---
title: >-
    شمارهٔ ۲۰۳
---
# شمارهٔ ۲۰۳

<div class="b" id="bn1"><div class="m1"><p>دلم آه سحر چون با دعا دمساز گردانید</p></div>
<div class="m2"><p>ز غربت آفتاب من عنانرا باز گردانید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هوای دلکش صحرا و آب دیده ی عاشق</p></div>
<div class="m2"><p>نهال نازکش خوشتر ز سرو ناز گردانید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کدام ابرو کمانت یار و همدم شد درین رفتن</p></div>
<div class="m2"><p>که چشم عشوه سازت را شکار انداز گردانید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>فدای بازی اسبت دل ممتاز درویشان</p></div>
<div class="m2"><p>که بس شاهانه ات از همرهان ممتاز گردانید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ربود از نرگست باد خزانی رنگ دلداری</p></div>
<div class="m2"><p>غرورت غمزه ی مستانه را غماز گردانید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هوای زلف مشک آمیز و چشم سرمه سای تو</p></div>
<div class="m2"><p>چو تار عنکبوتم زار و بی آواز گردانید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مقدس آتشی کان از نهاد شمع سر برزد</p></div>
<div class="m2"><p>ز روی تربیت پروانه را جانباز گردانید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صبا آورد گرد دامن پیراهن یوسف</p></div>
<div class="m2"><p>در بیت الحزن را پرده ی صد راز گردانید</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همینت بس فغانی در بلاد پارسی گویان</p></div>
<div class="m2"><p>که عشقت عندلیب گلشن شیراز گردانید</p></div></div>