---
title: >-
    شمارهٔ ۴۹۱
---
# شمارهٔ ۴۹۱

<div class="b" id="bn1"><div class="m1"><p>زهی شمع فلک در خرگه از تو</p></div>
<div class="m2"><p>همه جادو زبانان در چه از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر اینست لب ای چشمه ی نوش</p></div>
<div class="m2"><p>شود خضر و مسیحا گمره از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گدایان را ز خوان نعمت خویش</p></div>
<div class="m2"><p>تو روزی می دهی شیی الله از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه بازیها که کردی با حریفان</p></div>
<div class="m2"><p>تو از من غافل و من آگه از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فروزان مهر رخسار تو از من</p></div>
<div class="m2"><p>رخ اقبال من همچون مه از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سخن دانسته می گویی فغانی</p></div>
<div class="m2"><p>زبان نکته گیران کوته از تو</p></div></div>