---
title: >-
    شمارهٔ ۴۶۶
---
# شمارهٔ ۴۶۶

<div class="b" id="bn1"><div class="m1"><p>منم و دو چشم روشن برخ تو باز کردن</p></div>
<div class="m2"><p>ز نعیم هر دو عالم در دل فراز کردن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>قدمی بهستی خود زدنست، قصه کوته</p></div>
<div class="m2"><p>بخیال کعبه تا کی ره خود دراز کردن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چو تو صبح و شام خوانی بحریم وصل ما را</p></div>
<div class="m2"><p>چه ضرورتست ازین در سفر حجاز کردن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تو گلی و من زبویت چو نسیم صبحگاهی</p></div>
<div class="m2"><p>بچه رو توانم ای گل ز تو احتراز کردن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قدمی به دیده ام نه که بود نشان دولت</p></div>
<div class="m2"><p>بسر نیازمندان گذری بناز کردن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه عنایتست یا رب ز پی هزار غمزه</p></div>
<div class="m2"><p>گرهی ز طاق ابرو بکرشمه باز کردن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو زر از خیال لعلت منم و دلی پر آتش</p></div>
<div class="m2"><p>نفسی بزرد رویی زدن و گداز کردن</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بنعیم هر دو عالم نکند بدل فغانی</p></div>
<div class="m2"><p>نظری بنازنینی ز سر نیاز کردن</p></div></div>