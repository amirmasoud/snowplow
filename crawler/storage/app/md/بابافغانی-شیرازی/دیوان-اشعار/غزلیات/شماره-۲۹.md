---
title: >-
    شمارهٔ ۲۹
---
# شمارهٔ ۲۹

<div class="b" id="bn1"><div class="m1"><p>در طاعت و عشرت بقرارست دل ما</p></div>
<div class="m2"><p>هر جا که رود همره یارست دل ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما آینه ی حسن تو آشفته نخواهیم</p></div>
<div class="m2"><p>برخیزد اگر زانکه غبارست دل ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزی هدف تیر بلایی شود این دل</p></div>
<div class="m2"><p>ویرانه مگردان که حصارست دل ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر پاره ی این قلب سیه جوهر فردیست</p></div>
<div class="m2"><p>بگذار و مسوزان که بکارست دل ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در جستن این طعمه همایان نگرانند</p></div>
<div class="m2"><p>بربند که تعویذ شکارست دل ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر حرف دل ما منه انگشت ملامت</p></div>
<div class="m2"><p>ای مدعی اندیش که خارست دل ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دارد نظر همت بسیار عزیزان</p></div>
<div class="m2"><p>هر چند که در دست تو خوارست دل ما</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>باد از شرف لذت دیدار تو محروم</p></div>
<div class="m2"><p>گر در غم آغوش و کنارست دل ما</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از غلغله ی سینه ی پرجوش فغانی</p></div>
<div class="m2"><p>آسوده زگلبانگ هزارست دل ما</p></div></div>