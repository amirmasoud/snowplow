---
title: >-
    شمارهٔ ۲۷۱
---
# شمارهٔ ۲۷۱

<div class="b" id="bn1"><div class="m1"><p>دوش آن پری ز دام رقیبان رمیده بود</p></div>
<div class="m2"><p>صید کمند ما شده آیا چه دیده بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جویبار دیده ی عشاق جلوه داشت</p></div>
<div class="m2"><p>سروی که سر ز چشمه ی حیوان کشیده بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر برگ گل دمیده فسون سبزه ی خطش</p></div>
<div class="m2"><p>خوش سبزه یی کز آب لطافت دمیده بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رندانه با گدای خود آن پادشاه حسن</p></div>
<div class="m2"><p>بزم وصال بر در میخانه چیده بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می گفت هر سخن که گره بود در دلم</p></div>
<div class="m2"><p>گویا که از زبان من آنها شنیده بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آشوب دیده و دل و آسیب عقل و دین</p></div>
<div class="m2"><p>آن قامت کشیده و زلف خمیده بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر هر اشاره که شرح و بیان نداشت</p></div>
<div class="m2"><p>تا دیده را به هم زده بودم رسیده بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن لاله یی که چید فغانی ز باغ وصل</p></div>
<div class="m2"><p>تأثیر آتش جگر و آب دیده بود</p></div></div>