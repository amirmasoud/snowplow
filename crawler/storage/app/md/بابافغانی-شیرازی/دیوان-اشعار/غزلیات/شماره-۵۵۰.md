---
title: >-
    شمارهٔ ۵۵۰
---
# شمارهٔ ۵۵۰

<div class="b" id="bn1"><div class="m1"><p>بتو حال خود چه گویم که تو خود شنیده باشی</p></div>
<div class="m2"><p>غم دل عیان نسازم که بدان رسیده باشی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چکند کسی که عمری بغزال نیم خوابت</p></div>
<div class="m2"><p>چو نر فگنده باشد ز برش رمیده باشی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>برهت فتاده بیخود چه خوش آنکه بیگمانی</p></div>
<div class="m2"><p>بسرم رسیده ناگاه و عنان کشیده باشی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه فراغ بیند آندل که تو جلوه گاه سازی</p></div>
<div class="m2"><p>چه حجاب ماند آن را که تو نور دیده باشی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>غم ناامیدی من مگر آن نفس بدانی</p></div>
<div class="m2"><p>که برون روی ز باغی و گلی نچیده باشی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخط بنفشه فامش نظر آن زمان کن ایدل</p></div>
<div class="m2"><p>که دعای صبحگاهی برخش دمیده باشی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بوصال سرو قدش نرسی مگر زمانی</p></div>
<div class="m2"><p>که درین چمن فغانی چو الف جریده باشی</p></div></div>