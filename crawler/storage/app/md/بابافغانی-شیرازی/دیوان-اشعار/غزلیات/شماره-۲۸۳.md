---
title: >-
    شمارهٔ ۲۸۳
---
# شمارهٔ ۲۸۳

<div class="b" id="bn1"><div class="m1"><p>از کعبه عزم دیر برون از طریق بود</p></div>
<div class="m2"><p>آیا چه چاره چون دل گمره رفیق بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچون فرشته از در میخانه بازگشت</p></div>
<div class="m2"><p>عقلم که دیرساله رفیق شفیق بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اندیشه ی مفرح یاقوت داشت دل</p></div>
<div class="m2"><p>غافل که نشأه در می همچون عقیق بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رمزی که از زبان صراحی شنید جام</p></div>
<div class="m2"><p>کنهش کسی نیافت که مقصد عمیق بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آخر باب و دانه ی میخانه صید شد</p></div>
<div class="m2"><p>مرغ دلم که طایر بیت العتیق بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حرفی شنیدم از لب جانبخش ساقیی</p></div>
<div class="m2"><p>از جا شدم که نکته بغایت دقیق بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هم در میان گریه فغانی فرود رفت</p></div>
<div class="m2"><p>بیرون نشد ز بزم تو مسکین غریق بود</p></div></div>