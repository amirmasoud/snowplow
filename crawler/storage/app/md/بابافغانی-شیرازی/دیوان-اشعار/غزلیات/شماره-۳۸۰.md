---
title: >-
    شمارهٔ ۳۸۰
---
# شمارهٔ ۳۸۰

<div class="b" id="bn1"><div class="m1"><p>شود در گلشنم دل چاک و در مجلس جگر خون هم</p></div>
<div class="m2"><p>فغان از اختر بد حال و از بخت دگرگون هم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نبودم من که می زد عشق در آب و گلم آتش</p></div>
<div class="m2"><p>وگر باور نداری در همان کارست اکنون هم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیازی باید و سوزی که رحم آرد دل افروزی</p></div>
<div class="m2"><p>وگر اینها نباشد در نگیرد سحر و افسون هم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>باندک عشوه جان می داد مجنون من چرا باشم</p></div>
<div class="m2"><p>که چندین شیوه دارد نوخط من طبع موزون هم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلم می سوزد و کام دلم صورت نمی بندد</p></div>
<div class="m2"><p>درونم داغ شد صد بار ازین معنی و بیرون هم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>حلالش باد این عشرت که روز از روز افزونست</p></div>
<div class="m2"><p>صفای نرگس مخمور و آب لعل میگون هم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغانی عشق بیدردسر و بی غصه ممکن نیست</p></div>
<div class="m2"><p>همین فریاد می زد سالها فرهاد و مجنون هم</p></div></div>