---
title: >-
    شمارهٔ ۶۵
---
# شمارهٔ ۶۵

<div class="b" id="bn1"><div class="m1"><p>دوش جان زندگی از چشمه ی حیوان تو داشت</p></div>
<div class="m2"><p>دیده آب دگر از چاه زنخدان تو داشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل بسی چاشنی ازچشمه ی نوش تو گرفت</p></div>
<div class="m2"><p>دیده چندین نمک از پسته ی خندان تو داشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>روزگار دل دیوانه برآشفت که دوش</p></div>
<div class="m2"><p>کار با سلسله ی زلف پریشان تو داشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق می خواست که رسوا کند ای خرقه ی تر</p></div>
<div class="m2"><p>دست بر من زد و در آتش سوزان تو داشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ملک دل خرم و آراسته بی شرکت غیر</p></div>
<div class="m2"><p>شد به قربان خیال تو که فرمان تو داشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از گل عشق فراهم نشود غنچه ی دل</p></div>
<div class="m2"><p>وین گشادیست که از چاک گریبان تو داشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بلبلی صبح فغانی غزلی خواند غریب</p></div>
<div class="m2"><p>گریه آورد مگر نسخه ی دیوان تو داشت</p></div></div>