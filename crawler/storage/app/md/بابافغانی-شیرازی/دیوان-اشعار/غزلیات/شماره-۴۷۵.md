---
title: >-
    شمارهٔ ۴۷۵
---
# شمارهٔ ۴۷۵

<div class="b" id="bn1"><div class="m1"><p>گردم بهوا رفت چه گلگون فرسست این</p></div>
<div class="m2"><p>خون می کند و می رود آیا چه کسست این</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر دیده ی ما منتظران رخش مکن گرم</p></div>
<div class="m2"><p>آهسته رو ای ترک نه رود ارسست این</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر صبح فروزان تری از آه اسیران</p></div>
<div class="m2"><p>برخور که هنوز از دل ما یکنفسست این</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نالان دل من نغمه ی داود نداند</p></div>
<div class="m2"><p>آزاد کنیدش که نه مرغ قفسست این</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر جام مراد دگران چشم چه داری</p></div>
<div class="m2"><p>همت طلب ایدل همه را دسترسست این</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر مرغ درین باغ گلی دید و بهاری</p></div>
<div class="m2"><p>ماییم و خزان کز دگران باز پسست این</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در خرمن خود بهر گلی بر زدی آتش</p></div>
<div class="m2"><p>می سوز چه تدبیر فغانی هوسست این</p></div></div>