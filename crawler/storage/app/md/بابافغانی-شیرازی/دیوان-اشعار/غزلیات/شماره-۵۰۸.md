---
title: >-
    شمارهٔ ۵۰۸
---
# شمارهٔ ۵۰۸

<div class="b" id="bn1"><div class="m1"><p>عمریست کز سر ما میل مراد رفته</p></div>
<div class="m2"><p>شادی و کامرانی ما را زیاد رفته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از برق نا امیدی آتش بجان فتاده</p></div>
<div class="m2"><p>وز آه نا مرادی هستی بباد رفته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در غنچه ی دل ما رنگ بهی نمانده</p></div>
<div class="m2"><p>وز کار بسته ی ما بوی گشاد رفته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقست و صد ملامت گفتن چه سود ما را</p></div>
<div class="m2"><p>کاین بر صلاح مانده وان بر فساد رفته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عاشقی و مستی گشتم چنانکه از من</p></div>
<div class="m2"><p>بر آسمان فرشته بی اعتقاد رفته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز و شب از غم دل این چشم خونفشانرا</p></div>
<div class="m2"><p>اشک از بیاض ریزان نور از سواد رفته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردون اگر نبخشد کام دلت فغانی</p></div>
<div class="m2"><p>غمگین مشو که از وی این اعتماد رفته</p></div></div>