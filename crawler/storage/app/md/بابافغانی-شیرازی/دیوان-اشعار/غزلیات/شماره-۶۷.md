---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>باز آن رخ شکفته عرقناک بهر چیست</p></div>
<div class="m2"><p>وان زلف تاب داده بپیچاک بهر چیست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مگذار زنده هر که نخواهی، ترا چه غم</p></div>
<div class="m2"><p>چشم سیاه و غمزه ی بیباک بهر چیست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مردم ز رشک غیر زبانم چه می دهی</p></div>
<div class="m2"><p>زهرم چو کارگر شده تریاک بهر چیست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخ بر فروز تا همه جانها شود سپند</p></div>
<div class="m2"><p>چون گل شکفت منت خاشاک بهر چیست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داری هنوز دوش و کنار فرشته جای</p></div>
<div class="m2"><p>همدوشیت بمردم ناپاک بهر چیست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گشتم خراب و هیچ ندانم که سال و ماه</p></div>
<div class="m2"><p>خاصیت عناصر و افلاک بهر چیست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خود را بکش که نیست فغانی مراد دل</p></div>
<div class="m2"><p>بنگر که چند همچو تو در خاک بهر چیست</p></div></div>