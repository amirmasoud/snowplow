---
title: >-
    شمارهٔ ۱۸۰
---
# شمارهٔ ۱۸۰

<div class="b" id="bn1"><div class="m1"><p>تا رخت را سبزه در گلبرگ تر پنهان بود</p></div>
<div class="m2"><p>از تماشا سیر نتوان شد مگر پنهان بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز وقت آمد که هر کس با حریف سرو قد</p></div>
<div class="m2"><p>در میان لاله و گل تا کمر پنهان بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خوش بود با لاله رویان باده در لبهای جو</p></div>
<div class="m2"><p>خاصه آن ساعت که خورشید از نظر پنهان بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده را حالا ز جام باده آبی می دهم</p></div>
<div class="m2"><p>گرچه داغ بیشمارم در جگر پنهان بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بلبل شیدا نمی داند که در این دامگاه</p></div>
<div class="m2"><p>زیر هر برگ گلی صد نیشتر پنهان بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عیش من تلخست ورنه عالم از شهدست پر</p></div>
<div class="m2"><p>بلکه در هر گوشه صد تنگ شکر پنهان بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در گل و نسیرین نیابی بلکه در خورشید و ماه</p></div>
<div class="m2"><p>آنچه در هر ذره ی این خاک در پنهان بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زود بگذر زین نگارستان که زیر هر دری</p></div>
<div class="m2"><p>صد هزاران نازکی در یکدگر پنهان بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>باغ فردوسست آن یا طرفه جای دلکشست</p></div>
<div class="m2"><p>زانکه صد شاخ گل اندر هر شجر پنهان بود</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>زار می سوزد فغانی گرچه پیدا نیست داغ</p></div>
<div class="m2"><p>برق آه دردمندان را اثر پنهان بود</p></div></div>