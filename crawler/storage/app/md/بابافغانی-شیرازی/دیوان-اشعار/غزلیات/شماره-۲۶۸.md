---
title: >-
    شمارهٔ ۲۶۸
---
# شمارهٔ ۲۶۸

<div class="b" id="bn1"><div class="m1"><p>مرا هر روز بیتو صد غم جانسوز پیش آید</p></div>
<div class="m2"><p>الهی دشمن جان ترا این روز پیش آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم مشکین غزالی برد و میگردم من بیدل</p></div>
<div class="m2"><p>بود کز جانبی آن صید دست آموز پیش آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان دلتنگم از نادیدن آن گل که بی رویش</p></div>
<div class="m2"><p>نگردم شاد اگر صد عید و صد نوروز پیش آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبش در خواب می دیدم که میزد آتشی در دل</p></div>
<div class="m2"><p>بسوزم پیش او خود را اگر امروز پیش آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خوش آن شبها که سوزم تا سحر در کنج تنهایی</p></div>
<div class="m2"><p>چو بیرون آیم آن شمع جهان افروز پیش آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چو وقت آید که از لعل لبش فیروزه یی یابم</p></div>
<div class="m2"><p>بلاهای عجب از بخت نافیروز پیش آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بطاق ابرویش دارد فغانی دیده ی حیران</p></div>
<div class="m2"><p>که از هر گوشه تیر غمزه ی دلدوز پیش آید</p></div></div>