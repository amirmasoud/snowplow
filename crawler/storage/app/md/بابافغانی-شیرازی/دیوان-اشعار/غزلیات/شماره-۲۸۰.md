---
title: >-
    شمارهٔ ۲۸۰
---
# شمارهٔ ۲۸۰

<div class="b" id="bn1"><div class="m1"><p>گر آن خورشید روزی بر سر من سایه اندازد</p></div>
<div class="m2"><p>رقیبش همچو ابری آید و روزم سیه سازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گرفتارم بدست نازنینی کز هوای خود</p></div>
<div class="m2"><p>مرا چون زارتر بیند بخوبی بیشتر نازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنان خوبی که گر آیی میان مجلس خوبان</p></div>
<div class="m2"><p>زهر جانب پریرویی بر خسارت نظر بازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیب از محرمی گر شمع بالینت شود شب‌ها</p></div>
<div class="m2"><p>گمارم آه گرم خود برو چندانکه بگدازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بغیر از خاک پایش ای فغانی گر کشی سرمه</p></div>
<div class="m2"><p>سرشک از دیده بیرون آید و رویت سیه سازد</p></div></div>