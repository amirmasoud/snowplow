---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>ما را نه میل باغ و نه پروای بلبلست</p></div>
<div class="m2"><p>فریاد ما ز جلوه ی آن روی چون گلست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گویا ندارد از قدو زلف تو آگهی</p></div>
<div class="m2"><p>مرغ چمن که شیفته ی سرو و سنبلست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر دست فتنه سلسله ی هستیم گسست</p></div>
<div class="m2"><p>سر رشته ی حیات من آن جعد کاکلست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماییم و ذکر حلقه ی زنجیر زلف دوست</p></div>
<div class="m2"><p>عشاق را چه کار بدور تسلسلست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هر جلوه ی تو موجب صد گونه حیرتست</p></div>
<div class="m2"><p>در هر کرشمه ی تو هزاران تأملست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روی تو کرد عرض تجمل ز خط و خال</p></div>
<div class="m2"><p>فرخنده آن جمال که اینش تجملست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی تو دید و سوخت فغانی متاع صبر</p></div>
<div class="m2"><p>منعش نمی کنم چکند بی تحملست</p></div></div>