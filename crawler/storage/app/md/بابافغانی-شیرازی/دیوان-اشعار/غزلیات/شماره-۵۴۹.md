---
title: >-
    شمارهٔ ۵۴۹
---
# شمارهٔ ۵۴۹

<div class="b" id="bn1"><div class="m1"><p>از او قاصد بخشم آمد به من یارست پنداری</p></div>
<div class="m2"><p>ز مرگم می دهد پیغام غمخوارست پنداری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نگاهی می کنم از دور و خرسندم بجان دادن</p></div>
<div class="m2"><p>مراد از عاشقی این مردن زارست پنداری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشم از دوستان جوری که داغ دشمنم سهلست</p></div>
<div class="m2"><p>بلای من همین بیداد اغیارست پنداری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چه باک از سوختن آنجا که باشد آتشین رویی</p></div>
<div class="m2"><p>هلاک خویش بر پروانه دشوارست پنداری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان از جلوه ی شاخ گلی افتاده در خونم</p></div>
<div class="m2"><p>که در پایم هزاران نشتر خارست پنداری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شود خون هزاران آب تا برگ گلی روید</p></div>
<div class="m2"><p>چه دل بندم باین خونابه گلزارست پنداری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رود در عاشقی هردم سر آشفته‌ای دیگر</p></div>
<div class="m2"><p>شود بسیار از این‌ها فتنه بیدارست پنداری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه درد از آه مظلومان فغانی مست غفلت را</p></div>
<div class="m2"><p>خبر از خود ندارد خواجه هشیارست پنداری</p></div></div>