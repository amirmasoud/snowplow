---
title: >-
    شمارهٔ ۵۳۸
---
# شمارهٔ ۵۳۸

<div class="b" id="bn1"><div class="m1"><p>گر بگویم به تو ای مه که چه زیبندهٔ نازی</p></div>
<div class="m2"><p>رخ بر افروزی و از عشوه و نازم بگدازی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر بدانی که چه خوبست خطت بر ورق گل</p></div>
<div class="m2"><p>یکنفس آینه از پیش نظر دور نسازی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کشته و مرده ی آنم که برعنایی و شوخی</p></div>
<div class="m2"><p>نرگس از سرمه سیه سازی و سنبل بطرازی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتابی و منت ذره ی خورشید پرستم</p></div>
<div class="m2"><p>آه اگر بر سرم آیی ز پی بنده نوازی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کی از آینه ی همنفسان زنگ زدودن</p></div>
<div class="m2"><p>ما در آب و عرق از رشک و تو در خنده ی نازی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز کوتاه حیاتم سیه از هجر وز امید</p></div>
<div class="m2"><p>چشم دارم که شب وصل نهد رو به درازی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کشته افتاده فغانی ز کمین ساختن تو</p></div>
<div class="m2"><p>صید در خون جگر غرق و تو مشغول به بازی</p></div></div>