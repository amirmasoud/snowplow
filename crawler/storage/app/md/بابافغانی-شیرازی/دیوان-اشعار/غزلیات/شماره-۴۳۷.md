---
title: >-
    شمارهٔ ۴۳۷
---
# شمارهٔ ۴۳۷

<div class="b" id="bn1"><div class="m1"><p>ما باده را بنغمه ی ناهید خورده ایم</p></div>
<div class="m2"><p>آب از کنار چشمه ی خورشید خورده ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاهانه مجلسی طلب و ساقیی که ما</p></div>
<div class="m2"><p>می در شرابخانه ی جمشید خورده ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در مجلسی حبیب ز دست مسیح و خضر</p></div>
<div class="m2"><p>آب بقا و نعمت جاوید خورده ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستیم ازان شراب که با محرمان بباغ</p></div>
<div class="m2"><p>در سایه ی درخت گل و بید خورده ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل بسته ایم همچو فغانی بلطف دوست</p></div>
<div class="m2"><p>از شاخ عمر میوه ی امید خورده ایم</p></div></div>