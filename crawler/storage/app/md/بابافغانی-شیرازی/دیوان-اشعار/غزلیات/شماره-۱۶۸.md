---
title: >-
    شمارهٔ ۱۶۸
---
# شمارهٔ ۱۶۸

<div class="b" id="bn1"><div class="m1"><p>چو باشم سر به زانو مانده شب در فکر یار خود</p></div>
<div class="m2"><p>رود چشمم به خواب و ماه بینم در کنار خود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>به بزم شمع خودخواهم که سوزم همچو پروانه</p></div>
<div class="m2"><p>که غیرت می‌برم از سایهٔ شخص نزار خود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به راه انتظارش تا به کی از اشک نومیدی</p></div>
<div class="m2"><p>به خون غلتیده بینم دیدهٔ شب‌زنده‌دار خود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز آه سینه‌سوزم چون چراغ لاله درگیرد</p></div>
<div class="m2"><p>خس و خاری که شب در دشت غم سازم حصار خود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغانی چون به خاطر بگذراند روز وصل او</p></div>
<div class="m2"><p>نهد صد داغ حسرت بر دل امیدوار خود</p></div></div>