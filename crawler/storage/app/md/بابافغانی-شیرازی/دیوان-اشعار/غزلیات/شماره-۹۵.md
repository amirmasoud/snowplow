---
title: >-
    شمارهٔ ۹۵
---
# شمارهٔ ۹۵

<div class="b" id="bn1"><div class="m1"><p>شبانه می زده یی ماه من چنین پیداست</p></div>
<div class="m2"><p>نشان باده ات از لعل آتشین پیداست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همین بکینه ی ما تیر در کمان داری</p></div>
<div class="m2"><p>در ابرویت ز سیاست هنوز چین پیداست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آتش دل گرم که دست داشته یی</p></div>
<div class="m2"><p>که داغ تازه ات از چاک آستین پیداست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بطرف باغ گذر کرده یی بگل چیدن</p></div>
<div class="m2"><p>ز چاک پیرهنت برگ یاسمین پیداست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدین و دل چه تفاخر کدام دین و چه دل</p></div>
<div class="m2"><p>مرا که در غم عشقت نه دل نه دین پیداست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نه آدمی که ملک نیز در سجود آرد</p></div>
<div class="m2"><p>سعادتی که ترا ایمه از جبین پیداست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراب آن کمر نازکم که چون مه نو</p></div>
<div class="m2"><p>به شیوه های بلند از میان زین پیداست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>به نکته های غریبم اسیر خواهی کرد</p></div>
<div class="m2"><p>چنین از آن دو لب سحر آفرین پیداست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>لب بوعده ی شیرین کشد فغانی را</p></div>
<div class="m2"><p>هلاک مور گرفتار از انگبین پیداست</p></div></div>