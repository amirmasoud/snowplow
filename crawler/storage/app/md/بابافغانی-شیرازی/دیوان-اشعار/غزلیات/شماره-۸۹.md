---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>گشود چاک گریبان که یاسمین اینست</p></div>
<div class="m2"><p>نمود ساعد و گفتا در آستین اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>من از حلاوت خطش کنایتی گفتم</p></div>
<div class="m2"><p>لبش بخنده در آمد که انگبین اینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگاه بر شکرش کردم از سر حسرت</p></div>
<div class="m2"><p>بغمزه کرد اشارت که در کمین اینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سخن ز صورت چین می گذشت در مجلس</p></div>
<div class="m2"><p>کشید زلف ز عارض که نقش چین اینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نشان حال خرابات جستم از رندی</p></div>
<div class="m2"><p>نهاد کاسه ی دردی که بر زمین اینست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر محبت اسلام داری ای زاهد</p></div>
<div class="m2"><p>در آبکوچه ی عرفان که راه دین اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>رحیم ساخت فغانی دل چو سنگ بتان</p></div>
<div class="m2"><p>سرایت نفس و آه آتشین اینست</p></div></div>