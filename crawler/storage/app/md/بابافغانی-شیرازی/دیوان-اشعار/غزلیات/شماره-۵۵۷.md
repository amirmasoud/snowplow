---
title: >-
    شمارهٔ ۵۵۷
---
# شمارهٔ ۵۵۷

<div class="b" id="bn1"><div class="m1"><p>ای بکرشمه هر زمان گلبن باغ دیگری</p></div>
<div class="m2"><p>من شده کوه درد و تو لاله ی راغ دیگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوز تو در دل حزین چون نگرم بنیکوان</p></div>
<div class="m2"><p>بر دل خویش چون نهم بیهده داغ دیگری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یار به دیگری روان من ز پیش بسر دوان</p></div>
<div class="m2"><p>چند توان چنین شدن ره بچراغ دیگری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من بخیال آن پری گم شده ام ز خویشتن</p></div>
<div class="m2"><p>وای که او برغم من کرده سراغ دیگری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو فغانیم بود کاسه ی دیده پر ز خون</p></div>
<div class="m2"><p>تا شده عکس ساقیم نقش ایاغ دیگری</p></div></div>