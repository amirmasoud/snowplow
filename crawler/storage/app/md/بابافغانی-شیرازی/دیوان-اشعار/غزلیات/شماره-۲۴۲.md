---
title: >-
    شمارهٔ ۲۴۲
---
# شمارهٔ ۲۴۲

<div class="b" id="bn1"><div class="m1"><p>غباری کان گل از دامن به وقت رفتن افشاند</p></div>
<div class="m2"><p>بمیرم تا صبا همچون عبیرش بر من افشاند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کسی همچون صبا در گلشن کوی تو ره یابد</p></div>
<div class="m2"><p>که یکباره ز گرد هستی خود دامن افشاند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آن رو شعله ی شوق توام افزون شود هر دم</p></div>
<div class="m2"><p>که چشم خون فشان بر آتش من روغن افشاند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پس از من بلبلی پیدا شود در پای هر گلبن</p></div>
<div class="m2"><p>صبا خاکسترم را چون بطرف گلشن افشاند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغانی می رود افتان و خیزان بر سر راهی</p></div>
<div class="m2"><p>که جان خود به پای رخش آن صید افگن افشاند</p></div></div>