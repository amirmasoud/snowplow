---
title: >-
    شمارهٔ ۲۵۴
---
# شمارهٔ ۲۵۴

<div class="b" id="bn1"><div class="m1"><p>چون از می صبوحی رنگ رخش برآید</p></div>
<div class="m2"><p>بهر نظاره ی او خورشید بر در آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آنکه سر بزانو باشم در انتظارش</p></div>
<div class="m2"><p>ناگه چو سر بر آرم آن ماه بر سر آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افسون پندگویان دیوانه ساخت ما را</p></div>
<div class="m2"><p>با آن پری بگویید تا در برابر آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آسوده یی کزین در بیرون رود سلامت</p></div>
<div class="m2"><p>دارد سر ملامت گر بار دیگر آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن نور دیده دارد جا در دل فغانی</p></div>
<div class="m2"><p>در دل خوشست لیکن در دیده خوشتر آید</p></div></div>