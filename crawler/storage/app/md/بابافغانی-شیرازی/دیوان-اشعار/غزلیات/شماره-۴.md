---
title: >-
    شمارهٔ ۴
---
# شمارهٔ ۴

<div class="b" id="bn1"><div class="m1"><p>به ترانهٔ ندیمان نتوان ربود ما را</p></div>
<div class="m2"><p>چو بود غم تو در دل ز طرب چه سود ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنما رخ و هماندان که نماند کس به عالم</p></div>
<div class="m2"><p>چه کسیم ما که باشد عدم و وجود ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>به نوید آب حیوان دل مرده بازمانَد</p></div>
<div class="m2"><p>تو ز عمر و حسن برخور که هوس غنود ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مشکن عیار عاشق به قیاس فهم دشمن</p></div>
<div class="m2"><p>بدو نیک ما چه داند که نیازمود ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به نظارهٔ تو دود از دل عاشقان برآمد</p></div>
<div class="m2"><p>چو سپند سوخت اکنون چه غم از حسود ما را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سر فتنه داشت امشب خود ما رقیب و رندی</p></div>
<div class="m2"><p>به شراب و ساقی کس طمعی نبود ما را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چو نوای نی فغانی دم جان گداز دارد</p></div>
<div class="m2"><p>که در آتش محبت فگند چو عود ما را</p></div></div>