---
title: >-
    شمارهٔ ۸۲
---
# شمارهٔ ۸۲

<div class="b" id="bn1"><div class="m1"><p>وقت گلم تمام به آه و فغان گذشت</p></div>
<div class="m2"><p>چون بگذرد خزان که بهارم چنان گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین انجمن چه دید که بیرون نمی رود</p></div>
<div class="m2"><p>دیوانه یی که از سر کون و مکان گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سهلست اگر کنند ز جامی مضایقه</p></div>
<div class="m2"><p>با دل شکسته یی که تواند ز جان گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر باد بودی ار نشدی صرف گلرخان</p></div>
<div class="m2"><p>این عمر بی بدل که چو آب روان گذشت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فکر کفن کنید که آن ترک تندخو</p></div>
<div class="m2"><p>تیغی چنان رساند که از استخوان گذشت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گو برفروز چهره و بازار گرم کن</p></div>
<div class="m2"><p>اکنون که عاشق از سر سود و زیان گذشت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فرهاد کار کرد فغانی که از وفا</p></div>
<div class="m2"><p>رسمی چنان نهاد که نتوان ازان گذشت</p></div></div>