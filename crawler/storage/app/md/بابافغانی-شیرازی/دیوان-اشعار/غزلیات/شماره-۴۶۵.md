---
title: >-
    شمارهٔ ۴۶۵
---
# شمارهٔ ۴۶۵

<div class="b" id="bn1"><div class="m1"><p>بمن هر کس که روزی یار شد دامن کشید از من</p></div>
<div class="m2"><p>که جز درد و بلای عاشقی چیزی ندید از من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بود بر هر سر ره دردمندی واقف حالم</p></div>
<div class="m2"><p>که در عشق و جنون بر هر دلی دردی رسید از من</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نگردد رام اگر چون سگ دهم جان در وفاداری</p></div>
<div class="m2"><p>سیه چشمی که همچون آهوی وحشی رمید از من</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برغم من کشد بر دیگران شمشیر و می ترسم</p></div>
<div class="m2"><p>که در روز جزا خواهند خون صد شهید از من</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بخون دل نهالی در کنار خویش پروردم</p></div>
<div class="m2"><p>چو وقت آمد که از وی گل بچینم سرکشید از من</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بخواری مردم و یکره نگفت آن غنچه ی خندان</p></div>
<div class="m2"><p>که برد این بینوا صد حسرت و برگی نچید از من</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز بس خواری که امشب در رهش با خویشتن کردم</p></div>
<div class="m2"><p>به من هرکس که روزی داشت یاری دل برید از من</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ملولم چون فغانی دور از او از طعن بدگویان</p></div>
<div class="m2"><p>اجل کوتا کند کوتاه این گفت و شنید از من</p></div></div>