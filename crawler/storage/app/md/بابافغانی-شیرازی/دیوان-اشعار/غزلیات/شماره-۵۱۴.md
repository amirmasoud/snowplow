---
title: >-
    شمارهٔ ۵۱۴
---
# شمارهٔ ۵۱۴

<div class="b" id="bn1"><div class="m1"><p>بازم ز جفایی دل افگار شکسته</p></div>
<div class="m2"><p>بیداد گلی در جگرم خار شکسته</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آه از دل آن مست که می خورده باغیار</p></div>
<div class="m2"><p>ساغر بسر یار وفا دار شکسته</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دیگر چه ملامت بود از جنگ رقیبان</p></div>
<div class="m2"><p>ما را که سرو دست در اینکار شکسته</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رسوایی و تر دامنی از خلق چه پوشیم</p></div>
<div class="m2"><p>پیمانه ی ما بر سر بازار شکسته</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون برگ گل و لاله روان گشت ببستان</p></div>
<div class="m2"><p>جام طرب ما که بگلزار شکسته</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گلشن عیشم نظر انداز بعبرت</p></div>
<div class="m2"><p>تا سوخته بینی در و دیوار شکسته</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>این مستی از اندازه برونست فغانی</p></div>
<div class="m2"><p>امروز خمار تو مگر یار شکسته</p></div></div>