---
title: >-
    شمارهٔ ۸۱
---
# شمارهٔ ۸۱

<div class="b" id="bn1"><div class="m1"><p>تویی مراد دو عالم خرد همین دانست</p></div>
<div class="m2"><p>کسی که دید خدا در میان چنین دانست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خطا نگر که بیکدم هزار شیشه ی دل</p></div>
<div class="m2"><p>شکست زاهد و خود را درست درین دانست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر آنکه دست به دست گره گشایی داد</p></div>
<div class="m2"><p>کلید گنج سعادت در آستین دانست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>به پایه ی شرف آن رند حق شناس رسید</p></div>
<div class="m2"><p>که ریگ بادیه را لعل آتشین دانست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان کرشمه ی ساقی ربود عاشق را</p></div>
<div class="m2"><p>که درکشید می تلخ و انگبین دانست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز برق حادثه آتش بخرمنش نرسید</p></div>
<div class="m2"><p>غنی که قدر گدایان خوشه چین دانست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سزد که مهر سلیمان باهرمن بخشد</p></div>
<div class="m2"><p>هر آنکه نیک و بد کار از نگین دانست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چه خاک در نظر همتش چه آب حیات</p></div>
<div class="m2"><p>کسی که شیوه ی رندان ره نشین داشت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>قبول داشت فغانی که مقبلش خواندی</p></div>
<div class="m2"><p>تو طعنه کردی و آن ساده آفرین دانست</p></div></div>