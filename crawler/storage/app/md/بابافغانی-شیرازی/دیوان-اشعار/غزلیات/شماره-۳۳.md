---
title: >-
    شمارهٔ ۳۳
---
# شمارهٔ ۳۳

<div class="b" id="bn1"><div class="m1"><p>آنکه بتیزی زبان نرم کند ادیب را</p></div>
<div class="m2"><p>نیست گناه اگر کشد عاشق بی نصیب را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناله ی مرغ بوستان گریه کی آرد اینقدر</p></div>
<div class="m2"><p>منکه بهانه ساختم نغمه ی عندلیب را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آب حیات کی شود روزی ناکسی چومن</p></div>
<div class="m2"><p>من بهلاک خود خوشم غصه مده رقیب را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشق چو پنجه زد بجان تیغ رسد باستخوان</p></div>
<div class="m2"><p>هست کشنده درد من نیست گنه طبیب را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کی دل یوسف حزین یار شود بمصریان</p></div>
<div class="m2"><p>بلکه وفای دیگران بند بود غریب را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بعد نماز چون بود وعده بطرف بوستان</p></div>
<div class="m2"><p>دل چه تحمل آورد زمزمه ی خطیب را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بزم وصال گرم شد خیز فغانی از میان</p></div>
<div class="m2"><p>دانه ی دل سپند کن جلوه گه حبیب را</p></div></div>