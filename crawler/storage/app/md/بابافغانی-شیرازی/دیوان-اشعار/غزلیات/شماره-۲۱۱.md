---
title: >-
    شمارهٔ ۲۱۱
---
# شمارهٔ ۲۱۱

<div class="b" id="bn1"><div class="m1"><p>آمد بهار و دل بمی و جام تازه شد</p></div>
<div class="m2"><p>مهرم بساقیان گلندام تازه شد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر شاخ گل ز کج کلهی می دهد نشان</p></div>
<div class="m2"><p>خوبان رفته را به جهان نام تازه شد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>آه از فریب دهر کزین عشوه بس نکرد</p></div>
<div class="m2"><p>تا خلق را همان طمع خام تازه شد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از خاک کشتگان وفا خاست بوی گل</p></div>
<div class="m2"><p>داغی که بود بر دل از ایام تازه شد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دل کنذده بودم از می و ساقی چو گل رسید</p></div>
<div class="m2"><p>جان رمیده را هوس جام تازه شد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مرغ هوا به خانه خرابی من گریست</p></div>
<div class="m2"><p>چندانکه سبزه ام بلب بام تازه شد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>می نوش و گل بریز فغانی که عاقبت</p></div>
<div class="m2"><p>باغ هنر ز چشمه ی انعام تازه شد</p></div></div>