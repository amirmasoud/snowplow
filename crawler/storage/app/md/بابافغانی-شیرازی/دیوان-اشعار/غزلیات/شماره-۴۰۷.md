---
title: >-
    شمارهٔ ۴۰۷
---
# شمارهٔ ۴۰۷

<div class="b" id="bn1"><div class="m1"><p>چنین که پیش نظر صورت نکوی تو دارم</p></div>
<div class="m2"><p>بهر طرف که کنم سجده روبروی تو دارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز دیدن دگرانم چه سود چون من حیران</p></div>
<div class="m2"><p>نظر بصورت ایشان و دل بسوی تو دارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبا ز دست تو برگ گلی که سوی من آرد</p></div>
<div class="m2"><p>درون پیرهنش مدتی ببوی تو دارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نشسته اند حریفان ببزم عشق و من از غم</p></div>
<div class="m2"><p>گرفته ساغر و با خویش گفتگوی تو دارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>درون سوخته و آه گرم و چهره ی شمعی</p></div>
<div class="m2"><p>نشانه هاست که از داغ آرزوی تو دارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گرد هستی موهوم شسته آینه ی دل</p></div>
<div class="m2"><p>چو آب دیده ی خود رو بخاک کوی تو دارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>شب شراب اگر نیست ره ببزم تو این بس</p></div>
<div class="m2"><p>که گوش دل چو فغانی به های و هوی و دارم</p></div></div>