---
title: >-
    شمارهٔ ۱۵۰
---
# شمارهٔ ۱۵۰

<div class="b" id="bn1"><div class="m1"><p>بازم چراغ دل بمی ناب روشنست</p></div>
<div class="m2"><p>چشمم ز جلوه ی گل سیراب روشنست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون صبح اگر ستاره فشانی کنم رواست</p></div>
<div class="m2"><p>کز دیدن تو دیده ی بیخواب روشنست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>امشب که در خرابه ی درویش آمدی</p></div>
<div class="m2"><p>بیرون مرو که خانه ز مهتاب روشنست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بخشد صفای چشمه ی خورشید دیده را</p></div>
<div class="m2"><p>آیینه ی رخ تو که چون آب روشنست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع مراد من ز تماشای ابرویت</p></div>
<div class="m2"><p>همچون چراغ گوشه ی محراب روشنست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>خالی مباد ساغر دور از می وصال</p></div>
<div class="m2"><p>کز این چراغ مجلس احباب روشنست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>سربازی فغانی شیدا به تیغ عشق</p></div>
<div class="m2"><p>جوهر صفت ز خنجر قصاب روشنست</p></div></div>