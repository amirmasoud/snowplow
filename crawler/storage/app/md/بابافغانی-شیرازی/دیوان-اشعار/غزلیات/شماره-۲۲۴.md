---
title: >-
    شمارهٔ ۲۲۴
---
# شمارهٔ ۲۲۴

<div class="b" id="bn1"><div class="m1"><p>سحر فغان من آنمه ز طرف بام شنید</p></div>
<div class="m2"><p>شکایتی که ازو داشتم تمام شنید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زیان دشمنی و سود دوستی گفتم</p></div>
<div class="m2"><p>عیان نگشت که خود رای من کدام شنید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دگر هوای گلستان نکرد مرغ چمن</p></div>
<div class="m2"><p>چو حال خسته دلان اسیر دام شنید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیام وصل ز معشوق عین مرحمتست</p></div>
<div class="m2"><p>خجسته وقت اسیری که این پیام شنید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بنام و ننگ مقید مشو که زاهد شهر</p></div>
<div class="m2"><p>هزار طعن ز هر کس برای نام شنید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سلیم گو بجواب شکسته پردازد</p></div>
<div class="m2"><p>بشکر آنکه بهرجا که شد سلام شنید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دگر ز عشق جوانان مست توبه نکرد</p></div>
<div class="m2"><p>بنکته یی که فغانی ز پیر جام شنید</p></div></div>