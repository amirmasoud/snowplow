---
title: >-
    شمارهٔ ۲۴۵
---
# شمارهٔ ۲۴۵

<div class="b" id="bn1"><div class="m1"><p>معلم چون به تعلیم خط از دستش قلم گیرد</p></div>
<div class="m2"><p>خط او بیند و تعلیم از آن مشگین رقم گیرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ستم گویند هر کس از معلم یاد می گیرد</p></div>
<div class="m2"><p>معلم آید و زان شوخ تعلیم ستم گیرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چنین افسانه ی خوش که دل گفت از دهان او</p></div>
<div class="m2"><p>خضر گر بشنود از غیرتش خواب عدم گیرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کشم سر در گریبان هر سحر بی آن گل خندان</p></div>
<div class="m2"><p>مبادا آه سردم در چراغ صبحدم گیرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین سوزی که دارد پیر کنعان در غم یوسف</p></div>
<div class="m2"><p>سزد کز گوشه بیت الحزن آتش علم گیرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر من سوختم بادا چراغ حسن او روشن</p></div>
<div class="m2"><p>قضا پروانه‌ای از مطلع انوار کم گیرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغانی در حریم کویت آمد با دل سوزان</p></div>
<div class="m2"><p>چه سگ باشد که بی‌داغ تو خود را محترم گیرد</p></div></div>