---
title: >-
    شمارهٔ ۲۵۹
---
# شمارهٔ ۲۵۹

<div class="b" id="bn1"><div class="m1"><p>جز جور و جفا پیشه ی محبوب نباشد</p></div>
<div class="m2"><p>خوبی که جفایی نکند خوب نباشد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جایی نرسد نکهت پیراهن یوسف</p></div>
<div class="m2"><p>گر خود کشش از جانب یعقوب نباشد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>یک شمه نجات از الم عشق نیابد</p></div>
<div class="m2"><p>آن را که بدل صبر صد ایوب نباشد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گردیده و دل پاک نگه داشته باشی</p></div>
<div class="m2"><p>هیچ از نظر پاک تو محجوب نباشد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هرگز دل پر خون نشود طالب درمان</p></div>
<div class="m2"><p>ما را که بجز درد تو مطلوب نباشد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر جذبه ی عشقت نشود یار فغانی</p></div>
<div class="m2"><p>در راه طلب سالک مجذوب نباشد</p></div></div>