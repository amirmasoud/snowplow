---
title: >-
    شمارهٔ ۳۶۹
---
# شمارهٔ ۳۶۹

<div class="b" id="bn1"><div class="m1"><p>تا کی روم ز کوی تو غمگین و دردناک</p></div>
<div class="m2"><p>در دیده آب گشته و بر رخ نشسته خاک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از خون غنچه ی دل احباب کن حذر</p></div>
<div class="m2"><p>ای دامنت چو برگ گل نوشکفته پاک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پیش نسیم بسکه گریبان گشاده یی</p></div>
<div class="m2"><p>دارم دلی ز دست تو چون غنچه چاک چاک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پیوند ما چو با سر زلف تو محکمست</p></div>
<div class="m2"><p>سر رشته ی حیاتم اگر بگسلد چه باک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>صید حرم که ساخت خدا قتل او حرام</p></div>
<div class="m2"><p>می خواهد از خدا که بتیغت شود هلاک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در تنگنای هجر فغانی گشاد دل</p></div>
<div class="m2"><p>از ناله ی حزین طلب و آه دردناک</p></div></div>