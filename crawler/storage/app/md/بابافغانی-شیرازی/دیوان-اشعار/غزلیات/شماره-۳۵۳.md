---
title: >-
    شمارهٔ ۳۵۳
---
# شمارهٔ ۳۵۳

<div class="b" id="bn1"><div class="m1"><p>نتوانم که بینم از دورش</p></div>
<div class="m2"><p>آه از شرم چشم مخمورش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیست در شهر کس که عاشق نیست</p></div>
<div class="m2"><p>چه بلا گشت حسن مشهورش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>این چراغ از کدام انجمنست</p></div>
<div class="m2"><p>که جهانی بسوخت از نورش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رفت آن ماه نیم مست برون</p></div>
<div class="m2"><p>چه شبی روز کرد مخمورش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چه بود حالت نظر بازی</p></div>
<div class="m2"><p>که چنین آفتیست منظورش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چکند عطر پیرهن، عاشق</p></div>
<div class="m2"><p>فکر کن زعفران و کافورش</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چه دلست این دل فغانی وای</p></div>
<div class="m2"><p>که نه پیداست ماتم و سورش</p></div></div>