---
title: >-
    شمارهٔ ۱۷۳
---
# شمارهٔ ۱۷۳

<div class="b" id="bn1"><div class="m1"><p>خزان آمد گریبان را برندی چاک خواهم کرد</p></div>
<div class="m2"><p>بمن می ده که پر افشانیی چون تاک خواهم کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ورق را تازه گردانید بستان، می بگردانید</p></div>
<div class="m2"><p>که چندین معنی رنگین دگر ادراک خواهم کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فروزان گشت روی برگ و خون رز بجوش آمد</p></div>
<div class="m2"><p>سر از می گرم در این بزم آتشناک خواهم کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گر آن خورشید رویم این خزان همکاسه خواهد شد</p></div>
<div class="m2"><p>میی همچون شفق در شیشه ی افلاک خواهم کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چمن از برگ رنگین گشت چون بتخانه آزر</p></div>
<div class="m2"><p>زمستی سجده یی در هر خس و خاشاک خواهم کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بچین ابروی ساقی که تا دارم میی باقی</p></div>
<div class="m2"><p>نظر در چشم مست و غمزه بیباک خواهم کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>درین میدان که چون برگ خزان مرغ از هوا افتد</p></div>
<div class="m2"><p>سری دارم فدای حلقه ی فتراک خواهم کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چو عکس خط ساقی در شراب افتاد دانستم</p></div>
<div class="m2"><p>که حرف عافیت از صفحه ی دل پاک خواهم کرد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بروی تازه رویان در خزانی باده خواهم خورد</p></div>
<div class="m2"><p>حریف سفله را در کاسه ی سر خاک خواهم کرد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>فلک پیوسته می گوید که نقد انجم افلاک</p></div>
<div class="m2"><p>نثار میر عادل قاسم پرناک خواهم کرد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فغانی بوسه ی ساقیست تریاک شراب تلخ</p></div>
<div class="m2"><p>دهان تلخ را شیرین بدین تریاک خواهم کرد</p></div></div>