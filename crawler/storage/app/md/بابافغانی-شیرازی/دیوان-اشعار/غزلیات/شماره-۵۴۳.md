---
title: >-
    شمارهٔ ۵۴۳
---
# شمارهٔ ۵۴۳

<div class="b" id="bn1"><div class="m1"><p>تا نباشد دولتی روی تو چون بیند کسی</p></div>
<div class="m2"><p>چشمهٔ حیوان کجا بی‌رهنمون بیند کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گو بگیرد بر سرم چون کاسهٔ مجنون شکن</p></div>
<div class="m2"><p>گر به دستم بی‌تو جام لاله‌گون بیند کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در گمان افتد که آیا کوهکن چون زنده شد</p></div>
<div class="m2"><p>گر چنین آشفته‌ام در بیستون بیند کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کی توانم دیدن آن آیینه در دست رقیب</p></div>
<div class="m2"><p>دیدهٔ خود را به دست غیر چون بیند کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دور از آن گل رفتم از پای درخت ارغوان</p></div>
<div class="m2"><p>چند خود را در میان خاک و خون بیند کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دور نبود از جفاهای تو و طعن رقیب</p></div>
<div class="m2"><p>گر فغانی را به زنجیر جنون بیند کسی</p></div></div>