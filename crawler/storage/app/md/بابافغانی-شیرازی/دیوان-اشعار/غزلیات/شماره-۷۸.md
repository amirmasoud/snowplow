---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>چشمت ز حال ما چو نظر باز می گرفت</p></div>
<div class="m2"><p>این شیوه کاشکی هم از آغاز می گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل از بهانه ی تو زبون شد، چنین بود</p></div>
<div class="m2"><p>چون یا نکته دان سخن ساز می گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فریاد کس نمی شنوی ور نه آه من</p></div>
<div class="m2"><p>می شد چنان بلند که آواز می گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عشقت نهان نماند و ملامت شدم دریغ</p></div>
<div class="m2"><p>آن صبر کو که پرده به صد راز می گرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بسیار پشت دست گزید از خیال خام</p></div>
<div class="m2"><p>آن کو ندیده سبب ترا گاز می گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون آبگینه در دل عاشق شکست خورد</p></div>
<div class="m2"><p>جامی که با تو خانه برانداز می گرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نه تلخ باده یی تو فغانی که آن حریف</p></div>
<div class="m2"><p>شکر ز دست غیر به صد ناز می گرفت</p></div></div>