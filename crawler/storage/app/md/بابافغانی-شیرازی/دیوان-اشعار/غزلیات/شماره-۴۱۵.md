---
title: >-
    شمارهٔ ۴۱۵
---
# شمارهٔ ۴۱۵

<div class="b" id="bn1"><div class="m1"><p>مبین، که تاب نگاه تو آفتاب ندارم</p></div>
<div class="m2"><p>بناز خنده ی پنهان مزن که تاب ندارم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هنوز در خفقانم ز گریه های شبانه</p></div>
<div class="m2"><p>زبان بگز که وجود چنین شراب ندارم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بماند دانش من در جواب یک سخن تو</p></div>
<div class="m2"><p>دگر زهر چه سخن می کنی جواب ندارم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قلم بحرف ملامت کشیده ام من مجنون</p></div>
<div class="m2"><p>چنانکه گر بزنند آتشم عذاب ندارم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ببزم لاله رخان گشته چون سپند بر آتش</p></div>
<div class="m2"><p>چه جای باغ که پروای مشک ناب ندارم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز گلخنم مبر ای دل که داشتی بچراغم</p></div>
<div class="m2"><p>برو که من هوس گشت ماهتاب ندارم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>زمان زمان ز خیالت در آتشم همه ی شب</p></div>
<div class="m2"><p>چه خوابهای پریشان کز اضطراب ندارم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زیاد روی تو هر شب نداشتم خبر از خود</p></div>
<div class="m2"><p>تویی برابرم امشب که هیچ خواب ندارم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گذشت گریه ام از حد چنین مسوز فغانی</p></div>
<div class="m2"><p>که آب در جگر از دود این کباب ندارم</p></div></div>