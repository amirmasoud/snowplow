---
title: >-
    شمارهٔ ۱۳۴
---
# شمارهٔ ۱۳۴

<div class="b" id="bn1"><div class="m1"><p>آه ازین ناز و دلبری که تراست</p></div>
<div class="m2"><p>وین جفا و ستمگری که تراست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شاید ار آدمی پرستد بت</p></div>
<div class="m2"><p>زین همه ظلم و کافری که تراست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ایدل آشفتگی دراز کشید</p></div>
<div class="m2"><p>رشته در دست آن پری که تراست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تشنه لب جان دهی بخاک ایدل</p></div>
<div class="m2"><p>زین خیال سکندری که تراست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی سر و پا کند فغانی را</p></div>
<div class="m2"><p>این تراش قلندری که تراست</p></div></div>