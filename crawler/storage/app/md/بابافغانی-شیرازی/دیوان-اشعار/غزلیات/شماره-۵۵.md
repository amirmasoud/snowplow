---
title: >-
    شمارهٔ ۵۵
---
# شمارهٔ ۵۵

<div class="b" id="bn1"><div class="m1"><p>نشد جز درد و داغ عشق حاصل در سفر ما را</p></div>
<div class="m2"><p>که از هر شهر و یاری ماند داغی در جگر ما را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر چه می رویم از دست شوخی زین دیار اما</p></div>
<div class="m2"><p>همین حالت دهد رو باز در شهر دگر ما را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همان بهتر که یاران با کسی دیگر نپیوندیم</p></div>
<div class="m2"><p>که دوران می کند آخر جدا از یکدگر ما را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نه ما داریم این سرگشتگی از گردش گردون</p></div>
<div class="m2"><p>بتان دارند زینسان کوبکو و در بدر ما را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فغانی اشک ریزان از سر کوی بتان مگذر</p></div>
<div class="m2"><p>که بس بی‌آبرویی می‌رسد زین رهگذر ما را</p></div></div>