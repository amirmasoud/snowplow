---
title: >-
    شمارهٔ ۴۳۳
---
# شمارهٔ ۴۳۳

<div class="b" id="bn1"><div class="m1"><p>ما سر به آب خنجر قصاب شسته ایم</p></div>
<div class="m2"><p>دست از مراد خویش بصد آب شسته ایم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>پهلو نهاده ایم بشمشیر آبدار</p></div>
<div class="m2"><p>وز دل غبار بستر سنجاب شسته ایم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>انگشت خاکرا بلب تشنه سوده ایم</p></div>
<div class="m2"><p>دست و دهان ز نقل و می ناب شسته ایم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شبها برای خاک در پاکدامنی</p></div>
<div class="m2"><p>تن را به آب دیده ی بیخواب شسته ایم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتار بیخودانه ی ما گریه آورد</p></div>
<div class="m2"><p>دفتر به آب دیده ازین باب شسته ایم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشتی شکسته وار پریشان بهر کنار</p></div>
<div class="m2"><p>دست تهی ز جمله ی اسباب شسته ایم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خونین قبای خویش در آتش فگنده ایم</p></div>
<div class="m2"><p>کتان خویش در شب مهتاب شسته ایم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ترسم که آفتی رسد این کهنه دلق را</p></div>
<div class="m2"><p>کز بهر سجده بردن محراب شسته ایم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از یاد برده ایم فغانی غم جهان</p></div>
<div class="m2"><p>زنگار دل به صحبت احباب شسته ایم</p></div></div>