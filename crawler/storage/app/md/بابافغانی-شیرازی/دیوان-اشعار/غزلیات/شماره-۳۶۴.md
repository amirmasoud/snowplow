---
title: >-
    شمارهٔ ۳۶۴
---
# شمارهٔ ۳۶۴

<div class="b" id="bn1"><div class="m1"><p>مرا که تیره شد از کثرت گناه چراغ</p></div>
<div class="m2"><p>چه سود آنکه درآرم بپیشگاه چراغ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خراب کوی مغانم که نیمشب چو روم</p></div>
<div class="m2"><p>مهی ز هر طرف آرد به پیش راه چراغ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درآ بمیکده و اعتقاد روشن کن</p></div>
<div class="m2"><p>که می برند از آنجا بخانقاه چراغ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چرا چو گلخنیان دل بخاک تیره نهی</p></div>
<div class="m2"><p>ترا که خانه سپهرت و مهر و ماه چراغ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شنیده ام که ز همت به آفتاب رسید</p></div>
<div class="m2"><p>بسوز این دل و برکن ز برق آه چراغ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصدق دل چو درآیی بوادی ایمن</p></div>
<div class="m2"><p>یقین که سرزند از هر بن گیاه چراغ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فروغ کوکب طالع کنون شود پیدا</p></div>
<div class="m2"><p>که برفروخت فغانی ببزم شاه چراغ</p></div></div>