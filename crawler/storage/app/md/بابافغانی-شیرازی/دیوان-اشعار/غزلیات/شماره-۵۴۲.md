---
title: >-
    شمارهٔ ۵۴۲
---
# شمارهٔ ۵۴۲

<div class="b" id="bn1"><div class="m1"><p>تویی که هیچ گرفتی گل و شراب کسی</p></div>
<div class="m2"><p>مدام خنده زدی بر دل کباب کسی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو کز دریچه ی خورشید سر بدر کردی</p></div>
<div class="m2"><p>کجا پسند کنی خانه ی خراب کسی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا که خانه پر از در شبچراغ بود</p></div>
<div class="m2"><p>چه احتیاج بگلگشت ماهتاب کسی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همین ز چشمه ی خورشید خود بر آمده یی</p></div>
<div class="m2"><p>قدم نکرده تر از ناز از گلاب کسی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز آب و آینه هم روی خویش پوشیدی</p></div>
<div class="m2"><p>ز شرم چشم نکردی بر آفتاب کسی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مگو که شب ز خیالم چه خواب می بینی</p></div>
<div class="m2"><p>مپرس ازین که پریشان مباد خواب کسی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اثر نماند ز گردم فغانی آنهم رفت</p></div>
<div class="m2"><p>که می شدم بصد آشوب در رکاب کسی</p></div></div>