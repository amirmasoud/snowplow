---
title: >-
    شمارهٔ ۲۵۸
---
# شمارهٔ ۲۵۸

<div class="b" id="bn1"><div class="m1"><p>ما را گلی از باغ تو چیدن نگذارند</p></div>
<div class="m2"><p>چیدن چه خیالیست که دیدن نگذارند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهر سخنی از لبت ای غنچه ی خندان</p></div>
<div class="m2"><p>چون گل همه گوشیم شنیدن نگذارند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر جا که شود آینه ی روی تو پیدا</p></div>
<div class="m2"><p>آهی ز سر درد کشیدن نگذارند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بی چاشنی درد و غم از ساغر مقصود</p></div>
<div class="m2"><p>یک جرعه بدلخواه چشیدن نگذارند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را ز نمکدان تو ای کان ملاحت</p></div>
<div class="m2"><p>غیر از جگر پاره گزیدن نگذارند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>این طرفه که رندان خرابات مغان را</p></div>
<div class="m2"><p>پیراهن ناموس دریدن نگذارند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هرچند کشد سرزنش خار فغانی</p></div>
<div class="m2"><p>او را گلی از باغ تو چیدن نگذارند</p></div></div>