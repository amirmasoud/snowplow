---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>غریب کوی تو بی ناله ی حزین ننشست</p></div>
<div class="m2"><p>نداشت صحبت و با هیچ همنشین ننشست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نه مرغ بر سر من مور نیز خانه گرفت</p></div>
<div class="m2"><p>کسی به راه بت خویش بیش ازین ننشست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه غم ز دامن آلوده ی منست ترا</p></div>
<div class="m2"><p>که گرد غیر به دامان و آستین ننشست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز خاک کشته ی زهر فراق سبزه دمید</p></div>
<div class="m2"><p>هنوز یکسر مویت بر انگبین ننشست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گل مراد ز روی تو شمع مجلس چید</p></div>
<div class="m2"><p>که تا نخاست ازو شعله بر زمین ننشست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار زهره جبین رام تازیانه ی تست</p></div>
<div class="m2"><p>بدین ظهور بلند اختری بزین ننشست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خراب آن دو لب لعل یار خویشتنم</p></div>
<div class="m2"><p>که هرگز او ز حیا با بتان چین ننشست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خوش آن حریف که هر چند درد درد کشید</p></div>
<div class="m2"><p>گره به گوشه ی ابرو نزد غمین ننشست</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>حسود بر دل تنگم هزار داغ نهاد</p></div>
<div class="m2"><p>که یکرهش عرق از شرم بر جبین ننشست</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به دامن تو چه زیباست قطره های شراب</p></div>
<div class="m2"><p>به برگ لاله و گل شبنم این چنین ننشست</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>برای صبح وصالت فغانی مهجور</p></div>
<div class="m2"><p>شبی نرفت که تا روز در کمین ننشست</p></div></div>