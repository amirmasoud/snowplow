---
title: >-
    شمارهٔ ۵۲۳
---
# شمارهٔ ۵۲۳

<div class="b" id="bn1"><div class="m1"><p>من کیستم شکسته‌دل هیچکاره‌ای</p></div>
<div class="m2"><p>سرگرم جلوه‌ای و خراب نظاره‌ای</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>زین آتشی که عشق تو افروخت در دلم</p></div>
<div class="m2"><p>فریاد اگر به خرمنت افتد شراره‌ای</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در چنگ آفتم چو دهد شوق مو کشان</p></div>
<div class="m2"><p>بر من هزار رشتهٔ تدبیر تاره‌ای</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر پاره‌ای ز دل به جگر گوشه‌ای دهم</p></div>
<div class="m2"><p>فارغ مگر شوم ز غم خویش پاره‌ای</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با من رقیب ساده درافتاد بی‌جهت</p></div>
<div class="m2"><p>چون آبگینه‌ای که درافتد به خاره‌ای</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌آفتاب روی تو هر شام تا سحر</p></div>
<div class="m2"><p>داغی‌ست تازه بر دلم از هر ستاره‌ای</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فردا که دوست خوان کرم در میان نهد</p></div>
<div class="m2"><p>گیرد به قدر حوصله هرکس کناره‌ای</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بیچارگی‌ست کار فغانی و در غمش</p></div>
<div class="m2"><p>هرکس کند برای دل خویش چاره‌ای</p></div></div>