---
title: >-
    شمارهٔ ۲۸۷
---
# شمارهٔ ۲۸۷

<div class="b" id="bn1"><div class="m1"><p>خطش چو بنام من از خامه برون آید</p></div>
<div class="m2"><p>بس نکته ی دلسوزی کز نامه برون آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آنروز که در مکتب دیدم سبقش گفتم</p></div>
<div class="m2"><p>کاین طفل گرانمایه علامه برون آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ننوشته سلام ایدل بهر چه کشی خود را</p></div>
<div class="m2"><p>بگذار که حرفی چند از خامه برون آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دندان بجگر دارم باشد که ازین مجلس</p></div>
<div class="m2"><p>بخش من کم روزی یک شامه برون آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای آنکه نظر داری عمری بزلال خضر</p></div>
<div class="m2"><p>میباش که سرو من از خانه برون آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دانم که دهد تسکین یک روز فغانی را</p></div>
<div class="m2"><p>هرچند که آن بدخو خود کامه برون آید</p></div></div>