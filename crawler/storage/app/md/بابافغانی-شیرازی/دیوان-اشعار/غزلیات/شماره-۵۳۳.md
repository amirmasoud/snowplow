---
title: >-
    شمارهٔ ۵۳۳
---
# شمارهٔ ۵۳۳

<div class="b" id="bn1"><div class="m1"><p>تا کی ای غنچه دهن گوش بهر پند کنی</p></div>
<div class="m2"><p>سخنی گو که زبان همه را بند کنی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وقت آن شد که در آیی ز ره مهر و وفا</p></div>
<div class="m2"><p>تا بکی جور نمایی و جفا چند کنی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم دارم که کشی جام و مرا جرعه دهی</p></div>
<div class="m2"><p>ساغر عیش مرا پر شکر و قند کنی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هوس کشتن من کن که بود غایت لطف</p></div>
<div class="m2"><p>که بدین شیوه مرا خرم و خرسند کنی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای صبا گر بگشایی گرهی زان خم زلف</p></div>
<div class="m2"><p>رشته ی جان بسر او بچه پیوند کنی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده ی پیر مغان باش که در مجلس انس</p></div>
<div class="m2"><p>عیش جاوید ز الطاف خداوند کنی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لذت عمر همینست فغانی که مدام</p></div>
<div class="m2"><p>وصف جان بخشی آن لعل شکر خند کنی</p></div></div>