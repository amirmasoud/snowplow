---
title: >-
    شمارهٔ ۶۰
---
# شمارهٔ ۶۰

<div class="b" id="bn1"><div class="m1"><p>منم ای شمع دل رفته و جان آمده بر لب</p></div>
<div class="m2"><p>شده بر آتش شوق تو چو پروانه مقرب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شب وصلت که دران پرده کند عقل گرانی</p></div>
<div class="m2"><p>من و افسانهٔ لعلت که فسونی‌ست مجرب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من و خورشید جمالت چکنم ماه وشانرا</p></div>
<div class="m2"><p>که بانوار تجلی نرسد پرتو کوکب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نرود از نظرم نقش خط و خال تو هرگز</p></div>
<div class="m2"><p>که سواد نظر من شده زین هر دو مرکب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نبود عشوه گریهای تو در فهم معلم</p></div>
<div class="m2"><p>که کسی این همه منصوبه نیاموخت بمکتب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصد امید فگندم به سر راه تو خود را</p></div>
<div class="m2"><p>چکنم گر نگذاری که ببوسم سم مرکب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر امروز دگر جرعه ی وصلم نرسانی</p></div>
<div class="m2"><p>نرسانم من مخمور در این واقعه تا شب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>می عشق تو حرامست بر آن سفله که هرگز</p></div>
<div class="m2"><p>نکشد ساغر دردی و کند دعوی مشرب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صفت گرمی عشقت من سودا زده دانم</p></div>
<div class="m2"><p>که کسی چون من سودا زده نگداخت درین تب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>به نیاز شب و آه سحری یار نگردی</p></div>
<div class="m2"><p>چه کند با تو فغانی جگر سوخته یا رب</p></div></div>