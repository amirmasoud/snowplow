---
title: >-
    شمارهٔ ۱۱
---
# شمارهٔ ۱۱

<div class="b" id="bn1"><div class="m1"><p>دلگیرم از بزم طرب غمخانه‌ای باید مرا</p></div>
<div class="m2"><p>من عاشق دیوانه‌ام ویرانه‌ای باید مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دولت عشق و جنون آزادم از قید خرد</p></div>
<div class="m2"><p>اکنون برای همدمی دیوانه‌ای باید مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خواهم که افروزم شبی شمع طرب در کنج غم</p></div>
<div class="m2"><p>لیکن ز دیوان قضا پروانه‌ای باید مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شاید گزینم حالتی در خواب شیرین اجل</p></div>
<div class="m2"><p>از نرگش عاشق‌کشی افسانه‌ای باید مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بی‌صحبت شیرین‌لبی تلخ است بر من زندگی</p></div>
<div class="m2"><p>از جان به تنگ آمد دلم جانانه‌ای باید مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بی‌آن چراغ و چشم دل شب‌ها مقیم گلخنم</p></div>
<div class="m2"><p>شمعی ندارم کز طرب کاشانه‌ای باید مرا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچون فغانی آمدم از کعبه در دیر مغان</p></div>
<div class="m2"><p>پیمان شکستم ساقیا پیمانه‌ای باید مرا</p></div></div>