---
title: >-
    شمارهٔ ۱۵
---
# شمارهٔ ۱۵

<div class="b" id="bn1"><div class="m1"><p>شد باز دیده بر رخ نیکوی او مرا</p></div>
<div class="m2"><p>گلها شکفت در چمن کوی او مرا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ای باغبان برو که خدا داد در ازل</p></div>
<div class="m2"><p>سرو سهی ترا، قد دلجوی او مرا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شادم که هر دم از دم دیگر فزونترست</p></div>
<div class="m2"><p>دیوانگی زسلسله ی موی او مرا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رخصت نمیدهد بتماشای ماه نو</p></div>
<div class="m2"><p>میل نظاره ی خم ابروی او مرا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منهم یکی زگوشه نشینانم ای رفیق</p></div>
<div class="m2"><p>سرگشته کرده نرگس جادوی او مرا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از منت صبا چو فغانی درین چمن</p></div>
<div class="m2"><p>آزاد ساخت نکهت گیسوی او مرا</p></div></div>