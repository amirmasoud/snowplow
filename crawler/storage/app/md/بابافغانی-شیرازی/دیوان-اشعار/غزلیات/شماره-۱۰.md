---
title: >-
    شمارهٔ ۱۰
---
# شمارهٔ ۱۰

<div class="b" id="bn1"><div class="m1"><p>شکسته شد دل و شادست جان خستهٔ ما</p></div>
<div class="m2"><p>که یار نیست جدا از دل شکستهٔ ما</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو روز حشر برآریم سر ز خواب اجل</p></div>
<div class="m2"><p>به روی دوست شود باز چشم بستهٔ ما</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نشست آتش دل چهره برفروز ای شمع</p></div>
<div class="m2"><p>بود که شعله کشد آتش نشستهٔ ما</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رمید خواب خوش از چشم ما کجاست خیال</p></div>
<div class="m2"><p>که آرمیده شود چشم خواب جستهٔ ما</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گذشت کوکبهٔ صبح وصل و منتظریم</p></div>
<div class="m2"><p>که باز جلوه کند طالع خجستهٔ ما</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هزار دستهٔ گل بسته شد به خون جگر</p></div>
<div class="m2"><p>نظر نکرد به گل‌های دسته دستهٔ ما</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز خاک و خون فغانی هزار لاله دمید</p></div>
<div class="m2"><p>همین بود ز رخت باغ تازه رستهٔ ما</p></div></div>