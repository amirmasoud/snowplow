---
title: >-
    شمارهٔ ۳۱۳
---
# شمارهٔ ۳۱۳

<div class="b" id="bn1"><div class="m1"><p>خط گرد خال آن لب میگون زیاده تر</p></div>
<div class="m2"><p>دردم زیاد بود شد اکنون زیاده تر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنت زیاده باد که هر روز می کنی</p></div>
<div class="m2"><p>خوبی زیاده شیوه ی موزون زیاده تر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کردی چنان عتاب که در سینه کار کرد</p></div>
<div class="m2"><p>حسن عبارت از لب میگون زیاده تر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عاشق چه غم خورد که عنان را ز دست داد</p></div>
<div class="m2"><p>سوزد دل قبیله ز مجنون زیاده تر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از مجلس تو کشته برندم که ساخت دل</p></div>
<div class="m2"><p>شور درون خانه ز بیرون زیاده تر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظرفم مبین حقیر که گر ساقیم تویی</p></div>
<div class="m2"><p>خواهد شد این سفال ز جیحون زیاده تر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خیزد هزار ذره ز هر گام توسنت</p></div>
<div class="m2"><p>هر ذره یی ز ملک فریدون زیاده تر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>یک روز صرف مجلس میخواره ی منست</p></div>
<div class="m2"><p>هستی هر حریف ز قارون زیاده تر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>عمرم وبال گشت فغانی که دیده است</p></div>
<div class="m2"><p>آب حیات را الم از خون زیاده تر</p></div></div>