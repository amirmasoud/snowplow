---
title: >-
    شمارهٔ ۲۲
---
# شمارهٔ ۲۲

<div class="b" id="bn1"><div class="m1"><p>نظر بغیر نباشد اسیر بند ترا</p></div>
<div class="m2"><p>بناز کس نکشد دل نیازمند ترا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکر لبان همه دارند بر کلام تو گوش</p></div>
<div class="m2"><p>چه لطف داد خدا لعل نوشخند ترا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مهی که از کف یوسف عنان حسن ربود</p></div>
<div class="m2"><p>هزار بوسه دهد جلوه ی سمند ترا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نگاه بر کمر لعل و تاج زر نکنی</p></div>
<div class="m2"><p>چه احتیاج بود همت بلند ترا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کنند دام رهم عاقلان کلاله ی حور</p></div>
<div class="m2"><p>زهی جنون که گذارم خم کمند ترا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ترا رسد که لب از شیر شسته می نوشی</p></div>
<div class="m2"><p>کسی بهانه نیارد گرفت قند ترا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پری باینهمه افسونگری نیارد تاب</p></div>
<div class="m2"><p>که روز بزم بر آتش نهد سپند ترا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>بوعده صبر نکردیم و تلخکام شدیم</p></div>
<div class="m2"><p>بکش بناز که نشنیده ایم پند ترا</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>صبا زمجلس گرم تو داستانی گفت</p></div>
<div class="m2"><p>که تن گداخت فغانی دردمند ترا</p></div></div>