---
title: >-
    شمارهٔ ۴۳
---
# شمارهٔ ۴۳

<div class="b" id="bn1"><div class="m1"><p>بسوز ای شمع خوبان عاشق دیوانهٔ خود را</p></div>
<div class="m2"><p>مشرف کن به تشریف بقا پروانهٔ خود را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تو شمع بزم اغیاری و من در آتش غیرت</p></div>
<div class="m2"><p>ز برق آه روشن می‌کنم کاشانهٔ خود را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر من در خمارست از می لعل لبت ای گل</p></div>
<div class="m2"><p>به هر خاری می‌فشان جرعهٔ پیمانهٔ خود را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مزن سنگ ملامت زاهدا بر ساغر رندان</p></div>
<div class="m2"><p>اگر خواهی سلامت سبحهٔ صد دانهٔ خود را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چنان از بادهٔ بزم وصالت بی‌خبر گشتم</p></div>
<div class="m2"><p>که از مستی ندانم باز راه خانهٔ خود را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز کنج عافیت تا در میان مردم افتادم</p></div>
<div class="m2"><p>فراوان یاد کردم گوشهٔ ویرانهٔ خود را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیازست و محبت شیوهٔ رندان میخواره</p></div>
<div class="m2"><p>غنیمت دان فغانی شیوهٔ رندانهٔ خود را</p></div></div>