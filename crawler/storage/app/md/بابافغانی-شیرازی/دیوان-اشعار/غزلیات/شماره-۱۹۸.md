---
title: >-
    شمارهٔ ۱۹۸
---
# شمارهٔ ۱۹۸

<div class="b" id="bn1"><div class="m1"><p>صبا برگ گلی سوی من مجنون نیندازد</p></div>
<div class="m2"><p>که از خار دگر در رهگذارم خون نیندازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نیفتم هیچگه در بزم شمع خود چو پروانه</p></div>
<div class="m2"><p>که کس دستم نگیرد وز درم بیرون نیندازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>فسون خوان در پی تسکین سوز و من بفکر آن</p></div>
<div class="m2"><p>که آهم آتشی در دفتر افسون نیندازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>توانم خواند آسان نامه ی او گر برغم من</p></div>
<div class="m2"><p>رقیبش در نوشتن حرفی از مضمون نیندازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی در بزم آنمه زنده دارم بر مراد دل</p></div>
<div class="m2"><p>اگر ساقی دوران در میم افیون نیندازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغانی دل منه بر مهر گردون کاین ستم پیشه</p></div>
<div class="m2"><p>نیفرازد سری تا آخرش در خون نیندازد</p></div></div>