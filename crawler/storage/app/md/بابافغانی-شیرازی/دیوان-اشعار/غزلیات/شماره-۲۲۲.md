---
title: >-
    شمارهٔ ۲۲۲
---
# شمارهٔ ۲۲۲

<div class="b" id="bn1"><div class="m1"><p>کیم من تا کس از مرکب برای من فرود آید</p></div>
<div class="m2"><p>مرا تشریف بس گردی که از دامن فرود آید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فدای حلقه ی فتراک آن صیاد دلبندم</p></div>
<div class="m2"><p>که بهر صید پیکان خورده از توسن فرود آید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ازان روی عرقناکت رسد از چشم و دل آبی</p></div>
<div class="m2"><p>مثال شبنم صبحی که در گلشن فرود آید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>برافروز از چراغ جام بهر مهوشان منزل</p></div>
<div class="m2"><p>که خورشید از برای باده ی روشن فرود آید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>سمند ناز را بر آتش من گرم کن زانرو</p></div>
<div class="m2"><p>که شاه وقت گاهی بر در گلخن فرود آید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنور جان برافروزم سرای دیده را لیکن</p></div>
<div class="m2"><p>دل سلطان من مشکل درین مسکن فرود آید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چراغ تیره سوز من چه بنماید در آن مجلس</p></div>
<div class="m2"><p>که روزش آفتاب و شب مه از روزن فرود آید</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغانی جز بصاحبدل مخوان درس نظر بازی</p></div>
<div class="m2"><p>چنین معنی کجا در طبع هر کودن فرود آید</p></div></div>