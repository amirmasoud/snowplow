---
title: >-
    شمارهٔ ۳۶۸
---
# شمارهٔ ۳۶۸

<div class="b" id="bn1"><div class="m1"><p>یارم اگر بمهر کشد یا بکین چه باک</p></div>
<div class="m2"><p>من کشته ی ملامت و دردم ازین چه باک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در خنده اش هزار گشادست زیر لب</p></div>
<div class="m2"><p>از ناز اگر زند گرهی بر جبین چه باک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بر دو کون دست فشاندم برای او</p></div>
<div class="m2"><p>او گر بمن ز قهر فشاند آستین چه باک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرغی که دارد از چمن آسمان نصیب</p></div>
<div class="m2"><p>گر دانه یی نیافت ز کشت زمین چه باک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گیرم که اهرمن برد انگشتری ملک</p></div>
<div class="m2"><p>چون نام دیگریست نشان نگین چه باک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جایی که صد همای نیابند استخوان</p></div>
<div class="m2"><p>مور حقیر اگر نبرد انگبین چه باک</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دشمن ز آه گرم فغانی حذر نکرد</p></div>
<div class="m2"><p>آتش پرست را ز دم آتشین چه باک</p></div></div>