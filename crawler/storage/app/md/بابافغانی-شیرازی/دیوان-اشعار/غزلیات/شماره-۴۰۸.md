---
title: >-
    شمارهٔ ۴۰۸
---
# شمارهٔ ۴۰۸

<div class="b" id="bn1"><div class="m1"><p>دیده را فرش حریم حرمت ساخته ام</p></div>
<div class="m2"><p>مردم دیده طفیل قدمت ساخته ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اینکه از وصل توام غنچه ی امید شکفت</p></div>
<div class="m2"><p>گل آنست که با داغ غمت ساخته ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا خطت بر ورق لاله زد از مشک رقم</p></div>
<div class="m2"><p>جان فدای خط مشکین رقمت ساخته ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تو جمعیتم این بس که دل مجنون را</p></div>
<div class="m2"><p>بسته ی سلسله ی خم بخمت ساخته ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ای زبان قلم از وصف رخت شعله ی نور</p></div>
<div class="m2"><p>دیده روشن ز سواد قلمت ساخته ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چون فغانی زده بر هستی موهوم قدم</p></div>
<div class="m2"><p>خویش را کشته ی تیغ ستمت ساخته ام</p></div></div>