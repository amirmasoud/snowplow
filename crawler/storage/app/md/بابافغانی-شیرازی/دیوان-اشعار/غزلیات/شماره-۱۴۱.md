---
title: >-
    شمارهٔ ۱۴۱
---
# شمارهٔ ۱۴۱

<div class="b" id="bn1"><div class="m1"><p>با که گویم اینکه در بیخوابیم شب چون گذشت</p></div>
<div class="m2"><p>صحبتم تلخ از جفای آن شکر لب چون گذشت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون توان گفتن که از دل گرمی جانم چه شد</p></div>
<div class="m2"><p>بر تن فرسوده ام آزار آن تب چون گذشت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من بهر تلخابه یی تا شب رساندم این خمار</p></div>
<div class="m2"><p>روز تا بر آن دل نازک بمکتب چون گذشت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از زنخدانش من بیهوش خود بودم خراب</p></div>
<div class="m2"><p>در خیالم آرزوی سیم غبغب چون گذشت</p></div></div>