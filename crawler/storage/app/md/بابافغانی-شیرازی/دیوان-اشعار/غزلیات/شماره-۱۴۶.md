---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>یار باید که غم یار خورد یار کجاست</p></div>
<div class="m2"><p>غم دل هست فراوان دل غمخوار کجاست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ماه من روشنی دیده ی بیدار منست</p></div>
<div class="m2"><p>یا رب آن روشنی دیده ی بیدار کجاست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دلم افگار شد از داغ و بمرهم نرسید</p></div>
<div class="m2"><p>سوختم مرهم داغ دل افگار کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زخم خاریست مرا در دل از آن غنچه ی گل</p></div>
<div class="m2"><p>خون روانست و عیان نیست که آن خار کجاست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نرگس از چشم تو مردم کشی آموخت ولی</p></div>
<div class="m2"><p>چشم او را مژه و غمزه ی خونخوار کجاست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زهر چشم و سخن تلخ ز اندازه گذشت</p></div>
<div class="m2"><p>آن شکر خنده و شیرینی گفتار کجاست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نیست در حلقه ی مستان تو بیگانه کسی</p></div>
<div class="m2"><p>همه یارند درین دایره اغیار کجاست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>شد گرفتار فغانی بکمند غم عشق</p></div>
<div class="m2"><p>کس نپرسید که آن صید گرفتار کجاست</p></div></div>