---
title: >-
    شمارهٔ ۲۶۵
---
# شمارهٔ ۲۶۵

<div class="b" id="bn1"><div class="m1"><p>چو رو از جانب صید آن شکارانداز می‌تابد</p></div>
<div class="m2"><p>عنان می‌افگند بر من ز ناز و باز می‌تابد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سخن در پرده می‌گویی ولی گویا بود حسنش</p></div>
<div class="m2"><p>فروغ روی خوب از جوهر آواز می‌تابد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عفی الله برق پیکانت چه شمع دل‌فروز است آن</p></div>
<div class="m2"><p>که از شست تو ای ترک شکارانداز می‌تابد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب سوزی‌ست از شمع رخت در جان پروانه</p></div>
<div class="m2"><p>که از هر شهپرش صد شعله در پرواز می‌تابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز چنگ قامت عاشق چه گلبانگ طرب خیزد</p></div>
<div class="m2"><p>که چرخ واژگون ابریشم این ساز می‌تابد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ببین حال فغانی ای که بر آیینهٔ پاکت</p></div>
<div class="m2"><p>رخ انجام کار هرکس از آغاز می‌تابد</p></div></div>