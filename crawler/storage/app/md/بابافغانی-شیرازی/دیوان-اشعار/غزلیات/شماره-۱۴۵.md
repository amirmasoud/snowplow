---
title: >-
    شمارهٔ ۱۴۵
---
# شمارهٔ ۱۴۵

<div class="b" id="bn1"><div class="m1"><p>چشمم نظری در رخ آن دل گسل انداخت</p></div>
<div class="m2"><p>درهم شد و تیرم بدل منفعل انداخت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جنگ من و معشوق چو جنگ دل و دیده ست</p></div>
<div class="m2"><p>کو حمله بدل زد دل پر خون بگل انداخت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در جامه نمی گنجم ازین شوق که آن شمع</p></div>
<div class="m2"><p>دستم بگریبان زد و آتش بدل انداخت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>می خواست که سر رشته فرو ریزدم از هم</p></div>
<div class="m2"><p>آتش شد و سوزم بدل مضمحل انداخت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یکبار نپرسید بغلتیدن چشمی</p></div>
<div class="m2"><p>ما را که ز مژگان زدن متصل انداخت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بهله ی بلغار که در دست نگاریست</p></div>
<div class="m2"><p>دستیست که سرپنجه ی ترک چگل انداخت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در آب و عرق از غضب یار فغانی</p></div>
<div class="m2"><p>دل را چو گل نم زده خوار و خجل انداخت</p></div></div>