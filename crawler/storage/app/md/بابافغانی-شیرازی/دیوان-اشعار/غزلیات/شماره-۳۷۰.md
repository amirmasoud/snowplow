---
title: >-
    شمارهٔ ۳۷۰
---
# شمارهٔ ۳۷۰

<div class="b" id="bn1"><div class="m1"><p>دارم ز پسته ی تو بدل آتشین نمک</p></div>
<div class="m2"><p>بستان که کس ندیده کبابی بدین نمک</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دامن کشان و دست فشان می کنی خرام</p></div>
<div class="m2"><p>می گیرد از غبار تو روی زمین نمک</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گویا کسی که مرهم داغ دلم نهد</p></div>
<div class="m2"><p>در دست تیغ دارد و در آستین نمک</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>شوری که من ز عشق تو دارم نداشت کس</p></div>
<div class="m2"><p>زیرا که کس نداشت جوانی بدین نمک</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گاهم بزهر چشم جگر می کنی کباب</p></div>
<div class="m2"><p>گاهم به دیده می زنی از خشم و کین نمک</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در گریه ی فراق، فغانی ز بخت شور</p></div>
<div class="m2"><p>زد بر سواد دیده ی مردم نشین نمک</p></div></div>