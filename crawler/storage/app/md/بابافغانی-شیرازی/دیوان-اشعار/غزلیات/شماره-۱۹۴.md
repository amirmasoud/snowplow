---
title: >-
    شمارهٔ ۱۹۴
---
# شمارهٔ ۱۹۴

<div class="b" id="bn1"><div class="m1"><p>رسید آن شمع و از هر جانبی پروانه می‌جوید</p></div>
<div class="m2"><p>پریشان کرده کاکل عاشق دیوانه می‌جوید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ز بدخویی و مستی خون کند در کاسه‌ام اکنون</p></div>
<div class="m2"><p>که پیمان‌بسته با میگون‌لبی پیمانه می‌جوید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>رود تنها و نگذارد که باشم همره و دانم</p></div>
<div class="m2"><p>که همتای خود آن گوهر کدامین دانه می‌جوید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چگویم کان بهشتی از هوای گل‌رخی چون خود</p></div>
<div class="m2"><p>چو آتش گشته در کوی ملامت خانه می‌جوید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نگردد آشنا با کس و گر هم آشنا گردد</p></div>
<div class="m2"><p>حریفی همچو خود کافر دل و بیگانه می‌جوید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کجا آرام گیرد روز و شب در دیده خواب آرد</p></div>
<div class="m2"><p>کسی کان چشم مست و غمزهٔ مستانه می‌جوید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>نکرده گوش بر گفت کسان اکنون که عاشق شد</p></div>
<div class="m2"><p>پی خواب از فغانی هر شبی افسانه می‌جوید</p></div></div>