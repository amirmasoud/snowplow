---
title: >-
    شمارهٔ ۴۹۴
---
# شمارهٔ ۴۹۴

<div class="b" id="bn1"><div class="m1"><p>داری برقیبان سریاری عجب از تو</p></div>
<div class="m2"><p>بر گریه ی ما رحم نداری عجب از تو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما را به یک چشم زدن کار توان ساخت</p></div>
<div class="m2"><p>پیش نظر خود مگذاری عجب از تو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دانی که غنیمان چه غیورند بخونم</p></div>
<div class="m2"><p>دل برطرف من نگماری عجب از تو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از تربت من مهر گیا رست و تو بدخو</p></div>
<div class="m2"><p>یک ذره بدل رحم نداری عجب از تو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>می خوردن فاشت همه را داد بطوفان</p></div>
<div class="m2"><p>تو شیفته ی خواب و خماری عجب از تو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>افگنده عنان و شده بیباک بیکبار</p></div>
<div class="m2"><p>آموخته با خون شکاری عجب از تو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون کشت تو شد خشک فغانی مخور اندوه</p></div>
<div class="m2"><p>گریان چه هوا خواه بهاری عجب از تو</p></div></div>