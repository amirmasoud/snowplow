---
title: >-
    شمارهٔ ۱۵۱
---
# شمارهٔ ۱۵۱

<div class="b" id="bn1"><div class="m1"><p>من بنده ی حسنی که نشانش نتوان یافت</p></div>
<div class="m2"><p>پنهان نتوان دید و عیانش نتوان یافت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گنجی که ازان کون و مکانست بفریاد</p></div>
<div class="m2"><p>فریاد که در کون و مکانش نتوان یافت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>افتاده چو دولت بکنار من درویش</p></div>
<div class="m2"><p>آن نقد که در هیچ میانش نتوان یافت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عنقای خیالش که شکار نظر ماست</p></div>
<div class="m2"><p>صیدیست که بی بند زبانش نتوان یافت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نزدیکتر از لب بدهانست درین باغ</p></div>
<div class="m2"><p>آن سیب سخنگو که نشانش نتوان یافت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بلبل ز زر چهره ی گل در غلط افتاد</p></div>
<div class="m2"><p>پنداشت که در برگ خزانش نتوان یافت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون عاقبت درد کشان دید فغانی</p></div>
<div class="m2"><p>دیریست که در دیر مغانش نتوان یافت</p></div></div>