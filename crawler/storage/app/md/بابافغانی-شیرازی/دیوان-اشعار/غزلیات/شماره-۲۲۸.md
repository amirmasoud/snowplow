---
title: >-
    شمارهٔ ۲۲۸
---
# شمارهٔ ۲۲۸

<div class="b" id="bn1"><div class="m1"><p>دود از دل من باده ی گلرنگ برآورد</p></div>
<div class="m2"><p>زین خرقه ی تر آینه ام زنگ برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هر بار نمی برد چنین مطربم از دست</p></div>
<div class="m2"><p>این بار ندانم که چه آهنگ برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عشق آمد و در چاه فراموشیم افگند</p></div>
<div class="m2"><p>آنگاه سر او بگل و سنگ برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گفتم که به یک نغمه درم جامه ی ناموس</p></div>
<div class="m2"><p>من گفتم و مطرب بنوا چنگ برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شد دیده سپید و گل مقصود نچیدیم</p></div>
<div class="m2"><p>نخل غرض ما همه این رنگ برآورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بس تخم امل در هوس نام فشاندیم</p></div>
<div class="m2"><p>نامش نشنیدیم ولی ننگ برآورد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>صد کوه بلا زیر و زبر کرد فغانی</p></div>
<div class="m2"><p>هر گاه که آهی ز دل تنگ برآورد</p></div></div>