---
title: >-
    شمارهٔ ۲۳۸
---
# شمارهٔ ۲۳۸

<div class="b" id="bn1"><div class="m1"><p>معاذالله گرت با همدمان رغبت زیاد افتد</p></div>
<div class="m2"><p>من بیتاب را از غصه آتش در نهاد افتد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دلم خواهد که ساید دیده بر اندام سیمینت</p></div>
<div class="m2"><p>چه می گویم که از چشم جهان بینم سواد افتد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بخو وصلت روا می داشتم بر دیگران هجران</p></div>
<div class="m2"><p>چه دانستم که فالم جمله بر عکس مراد افتد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>رقیبان حال من باور نمی دارند اگر سوزم</p></div>
<div class="m2"><p>الهی آتشی در مردم بد اعتقاد افتد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شبی در کلبه ی تاریک عاشق در نمی آیی</p></div>
<div class="m2"><p>چرا با دوستان کس این چنین بد اعتماد افتد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>مبر حاجت بغیر ایدل که در دست کسی نبود</p></div>
<div class="m2"><p>اگر ناگه خدا خواهد که در کارت گشاد افتد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغانی زین نظر بازی سیه شد نامه ات تا کی</p></div>
<div class="m2"><p>خیالت با خط نو خیز و خال فتنه زاد افتد</p></div></div>