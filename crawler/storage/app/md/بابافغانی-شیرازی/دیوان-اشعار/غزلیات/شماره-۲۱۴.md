---
title: >-
    شمارهٔ ۲۱۴
---
# شمارهٔ ۲۱۴

<div class="b" id="bn1"><div class="m1"><p>با چون منی چرا می چون ارغوان خورند</p></div>
<div class="m2"><p>بگذار تا به کوی تو خونم سگان خورند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مغرور ناز و غمزه ی خویشی ترا چه غم</p></div>
<div class="m2"><p>بیچاره آنگروه که بر دل سنان خورند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونابه ی دلم ز تو ای گل نه اندکست</p></div>
<div class="m2"><p>دردیکشان عشق تو رطل گران خورند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیوانگان عشق ترا خواب و خور حرام</p></div>
<div class="m2"><p>آنانکه عاشقند چرا آب و نان خورند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شیران مرغزار تو ای مشگبو غزال</p></div>
<div class="m2"><p>بخشند صید را و دل خون چکان خورند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تاب زبان خلق نداری شکر مخواه</p></div>
<div class="m2"><p>دانی که عافیت طلبان استخوان خورند</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>خونم حلال گر نکشی پیش دشمنم</p></div>
<div class="m2"><p>این باده را ز دیده ی مردم نهان خورند</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گر کوه غم رسد ز تو دل بد نمی کنم</p></div>
<div class="m2"><p>یاران مهربان غم یاران بجان خورند</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>می خور فغانی از کف خوبان که جور نیست</p></div>
<div class="m2"><p>جامی که دوستان برخ دوستان خورند</p></div></div>