---
title: >-
    شمارهٔ ۴۸۴
---
# شمارهٔ ۴۸۴

<div class="b" id="bn1"><div class="m1"><p>چشم من از نظارهٔ آن زلف مشک‌بو</p></div>
<div class="m2"><p>چون نافهٔ تری‌ست که خون می‌چکد از او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یک قطره خون سوختهٔ خال گلرخی‌ست</p></div>
<div class="m2"><p>هر غنچهٔ بنفشه که بینم به طرف جو</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خونابه‌ای که می‌کشم از تیغ عشق تو</p></div>
<div class="m2"><p>چون آب زندگی به گلو می‌رود فرو</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خواهم چو گل سفینهٔ دل را ورق ورق</p></div>
<div class="m2"><p>هر یک ورق به دست نگاری فرشته‌خو</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تو شاه بیت دفتر حسنی و معنیت</p></div>
<div class="m2"><p>خلق جمیل و خوی خوش و صورت نکو</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر نازکی که بود نهان در نقاب حسن</p></div>
<div class="m2"><p>خالت نمود از شکن زلف مو به مو</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>لب بسته‌ای فغانی و احباب مستمع</p></div>
<div class="m2"><p>طوطی تویی درین شکرستان سخن بگو</p></div></div>