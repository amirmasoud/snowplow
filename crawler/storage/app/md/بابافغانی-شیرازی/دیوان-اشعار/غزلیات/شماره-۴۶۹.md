---
title: >-
    شمارهٔ ۴۶۹
---
# شمارهٔ ۴۶۹

<div class="b" id="bn1"><div class="m1"><p>شود صد سوز پنهان هر دم از داغ دلم روشن</p></div>
<div class="m2"><p>که داغ بود آیینهٔ گیتی‌نمای من</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>روم در دشت و چون مجنون نهم سر در بیابان‌ها</p></div>
<div class="m2"><p>اگر نه غیرت عشق تو هردم گیردم دامن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هوای آن گلم سوی گلستان می‌کشد ورنه</p></div>
<div class="m2"><p>من دیوانه را یکسان نماید گلشن و گلخن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نهالی کز سرشک آتشینم پرورش یابد</p></div>
<div class="m2"><p>برآرد آخر آتش چون درخت وادی ایمن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چراغ و شمع او در بزم عیش یار روشن شد</p></div>
<div class="m2"><p>من تنها نشین را خانه از مهتاب شد روشن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغانی از کجا و جرعهٔ وصلش همینش بس</p></div>
<div class="m2"><p>که در هجرش خورد خونابه تا جانش بود در تن</p></div></div>