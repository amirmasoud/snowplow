---
title: >-
    شمارهٔ ۲۸۸
---
# شمارهٔ ۲۸۸

<div class="b" id="bn1"><div class="m1"><p>شراب خورد و شبیخون بعاشقان آورد</p></div>
<div class="m2"><p>چه آفتست که احباب را بجان آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شدم بوعده ی او زار آه ازان بد مست</p></div>
<div class="m2"><p>که یکدو بوسه کرم کرد و بر زبان آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه گیرم آن کمر بسته را بدعوی خون</p></div>
<div class="m2"><p>که فتنه کاکل آشفته در میان آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز بند بند تنم این زمان براید دود</p></div>
<div class="m2"><p>که عشق خانه کنم پی باستخوان آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوید رحمت جاوید ازان بهشتی دارد</p></div>
<div class="m2"><p>فرشته یی که بمن مژده ی امان آورد</p></div></div>