---
title: >-
    شمارهٔ ۴۴۵
---
# شمارهٔ ۴۴۵

<div class="b" id="bn1"><div class="m1"><p>امیدم این نبود کزین در خجل روم</p></div>
<div class="m2"><p>با داغ دل در آیم و با درد دل روم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عشقم سبک عیار بر آورد پیش دوست</p></div>
<div class="m2"><p>دیگر در آتش که من منفعل روم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بگذار تا بخاک درش میرم ای رفیق</p></div>
<div class="m2"><p>من از کجا و باغ کجا، زیر گل روم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مستم چنانکه در دهن تیغ آبدار</p></div>
<div class="m2"><p>با جان پر ارادت و خون بحل روم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>عاشق نیم فغانی اگر بهر روی خوب</p></div>
<div class="m2"><p>تبریز دیده جانب چین و چگل روم</p></div></div>