---
title: >-
    شمارهٔ ۴۳۹
---
# شمارهٔ ۴۳۹

<div class="b" id="bn1"><div class="m1"><p>شبی که در نظر آن طره ی خمیده کشم</p></div>
<div class="m2"><p>هزار بار بجان بوسم و بدیده کشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا که غنچه ی وصل از دعای صبح شکفت</p></div>
<div class="m2"><p>چگونه منت گلهای نو دمیده کشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نیافت خاطرم آرام تا ز بوی گلی</p></div>
<div class="m2"><p>درین چمن نفسی چند آرمیده کشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا که میچکد از دیده خون دل بچه رو</p></div>
<div class="m2"><p>ز دست ساقی گلرخ می چکیده کشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نچیده از چمن وصل غیر خار و هنوز</p></div>
<div class="m2"><p>هزار درد دل از هر گل نچیده کشم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جریده می روم این راهرا و نزدیکست</p></div>
<div class="m2"><p>که خط نسخ بمضمون این جریده کشم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بجز دهان تو کز هیچ، آفریده خدا</p></div>
<div class="m2"><p>عجب که سرزنش از هیچ آفریده کشم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>دلم رمیده فغانی کجاست همنفسی</p></div>
<div class="m2"><p>که ناله یی بمراد دل رمیده کشم</p></div></div>