---
title: >-
    شمارهٔ ۳۹۷
---
# شمارهٔ ۳۹۷

<div class="b" id="bn1"><div class="m1"><p>بس بینوا ز ساقی خود دور مانده ام</p></div>
<div class="m2"><p>از سر شراب رفته و مخمور مانده ام</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم آب رفته از دل و هم تاب از نظر</p></div>
<div class="m2"><p>دور از چراغ میکده بی نور مانده ام</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نخل مسیح و باغ خلیل و زلال خضر</p></div>
<div class="m2"><p>یکیک ز دست داده و مهجور مانده ام</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در هیچ خرقه نیست ز من ناسزاتری</p></div>
<div class="m2"><p>این هم عنایتیست که مستور مانده ام</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>خلقی به تنگ از من و من از حیات خویش</p></div>
<div class="m2"><p>شرمنده در میانه ی جمهور مانده ام</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چشمم بروی شاهد و دل مایل فنا</p></div>
<div class="m2"><p>موقوف یک اشاره ی منظور مانده ام</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سو تفرجیست درین بزم چون بهشت</p></div>
<div class="m2"><p>تنها نه در مشاهده ی حور مانده ام</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>صد مرده زنده کرد فغانی طبیب شهر</p></div>
<div class="m2"><p>من از دعای کیست که رنجور مانده ام</p></div></div>