---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>گلی که از نفسش مشک ناب بگدازد</p></div>
<div class="m2"><p>چرا لب شکرین از شراب بگدازد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش آن بدن که ز می در قبا چو گل روید</p></div>
<div class="m2"><p>نه آنکه همچو شکر در گلاب بگدازد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر آن سرم که چراغی ز روغنم ماند</p></div>
<div class="m2"><p>دمی که این تن بیخورد و خواب بگدازد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گهی ز غم جگر پاره ام کباب شود</p></div>
<div class="m2"><p>دمی ز غصه دلم چون کباب بگدازد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدلفروزی شمع جمال او نرسد</p></div>
<div class="m2"><p>هزار سال اگر آفتاب بگدازد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغانی از طلب کیمیا نیاساید</p></div>
<div class="m2"><p>مگر دمی که درین اضطراب بگدازد</p></div></div>