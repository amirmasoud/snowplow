---
title: >-
    شمارهٔ ۳۰۶
---
# شمارهٔ ۳۰۶

<div class="b" id="bn1"><div class="m1"><p>هر دل که گرم از آتش پنهان من شود</p></div>
<div class="m2"><p>گر کافر فرنگ بود بت شکن شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل که بگسلم گره ی غم ز تیر آه</p></div>
<div class="m2"><p>تبخاله یی زند سر و مهر دهن شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>مجنون کجا و همدمی بلبلان باغ</p></div>
<div class="m2"><p>دیوانه به که طعمه ی زاغ و زغن شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>با عشق هر که زاده شد از مادر و پدر</p></div>
<div class="m2"><p>در شهر و کو ملامتی مرد و زن شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در هر گل زمین که نشینی شود بهشت</p></div>
<div class="m2"><p>هر جا که در خرام درآیی چمن شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>هر بخیه از قبای کبود تو روز صید</p></div>
<div class="m2"><p>دام هزار یوسف گل پیرهن شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گردون بساط لعل کشید از سر سران</p></div>
<div class="m2"><p>تا خاک مقدم فرس پیلتن شود</p></div></div>