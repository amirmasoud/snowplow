---
title: >-
    شمارهٔ ۵۶۴
---
# شمارهٔ ۵۶۴

<div class="b" id="bn1"><div class="m1"><p>دلا بی نقد جان راه سر کویش نپیمایی</p></div>
<div class="m2"><p>که نتوان رفت راه کعبه تا نبود توانایی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مرا جان بر لب و گفتی که می آیم دم دیگر</p></div>
<div class="m2"><p>چو خواهی آمدن باری چرا ایندم نمی آیی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>دمی گفتی نیاسودم ز سودای پریرویان</p></div>
<div class="m2"><p>به داغ و درد اگر قانع شوی ای دل بیاسایی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظر از روی او بر گل نکردی آفرین بادا</p></div>
<div class="m2"><p>که داری اینقدر در کار خود ای دیده بینایی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به بازار غم او نقد هستی رازدم آتش</p></div>
<div class="m2"><p>رسید آن شوخ و گفتا ای فغانی گرم سودایی</p></div></div>