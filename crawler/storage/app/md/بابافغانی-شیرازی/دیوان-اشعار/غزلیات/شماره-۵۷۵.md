---
title: >-
    شمارهٔ ۵۷۵
---
# شمارهٔ ۵۷۵

<div class="b" id="bn1"><div class="m1"><p>مرا در دیده جان آن پری رخسار بایستی</p></div>
<div class="m2"><p>خرام او دمی در چشم من صد بار بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خلد بی روی او از هر گلی در دیده ام خاری</p></div>
<div class="m2"><p>اگر خاریست باری زان گل رخسار بایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بسرو و سوسن خود باغبان بسیار می نازد</p></div>
<div class="m2"><p>ترا گاهی گذاری جانب گلزار بایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دریغست آتش عشق تو در دلهای آسوده</p></div>
<div class="m2"><p>تمام این شعله در جان من افگار بایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من دلخسته را گل بر سر بالین چکار آید</p></div>
<div class="m2"><p>بحالم یکنظر زان نرگس بیمار بایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ز خوی نازک او اشک و آه من گره تا کی</p></div>
<div class="m2"><p>دلم آتش فشان و دیده گوهر بار بایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>علاج درد بیماران چو می‌پرسید لعل او</p></div>
<div class="m2"><p>فغانی را در آن دم قوت گفتار بایستی</p></div></div>