---
title: >-
    شمارهٔ ۲۵۰
---
# شمارهٔ ۲۵۰

<div class="b" id="bn1"><div class="m1"><p>عید شد هر کس مه نو را مبارکباد کرد</p></div>
<div class="m2"><p>هر گرفتاری بطاق ابرویی دل شاد کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گریه ی مستان ز سوز و ناله ی چنگ صبوح</p></div>
<div class="m2"><p>زاهد خلوت نشین را رخنه در اوراد کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شام عید از جان خود بی او ملالی داشتم</p></div>
<div class="m2"><p>آمد آن سرو وز قید هستیم آزاد کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>گرچه کشتن عادت مردم نباشد روز عید</p></div>
<div class="m2"><p>جان فدای چشم او کاین شیوه را بنیاد کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>رحمتی بود آنکه آمد بر سرم جلوه کنان</p></div>
<div class="m2"><p>این که رفت و همعنان شد با بدان بیداد کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بنده ی آن سرو آزادم که در گلگشت عید</p></div>
<div class="m2"><p>دردمندان را به تشریف عیادت شاد کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر سر موی فغانی ناله یی دارد ز شوق</p></div>
<div class="m2"><p>گرچه نتواند ز ضعف آن ناتوان فریاد کرد</p></div></div>