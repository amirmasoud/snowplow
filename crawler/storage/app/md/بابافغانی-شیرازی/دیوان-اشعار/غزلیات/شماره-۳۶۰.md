---
title: >-
    شمارهٔ ۳۶۰
---
# شمارهٔ ۳۶۰

<div class="b" id="bn1"><div class="m1"><p>ترک یاری کردی از وصل تو یاران را چه حظ</p></div>
<div class="m2"><p>دشمن احباب گشتی دوستداران را چه حظ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون ندارد وعده ی وصل تو امید وفا</p></div>
<div class="m2"><p>غیر داغ انتظار امیدواران را چه حظ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چشم من کز گریه نابیناست چون بیند رخت</p></div>
<div class="m2"><p>از تماشای چمن ابر بهاران را چه حظ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>درد بیدرمان خوبان چون نمیگیرد قرار</p></div>
<div class="m2"><p>دردمندان را چه حاصل بیقراران را چه حظ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آن سوار از خاک ما تا کی برانگیزد غبار</p></div>
<div class="m2"><p>از غبار انگیختن یا رب سواران را چه حظ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>می دهد خاک رهش خاصیت آب حیات</p></div>
<div class="m2"><p>ورنه زین گرد مذلت خاکساران را چه حظ</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>یا رب از قصد فغانی چیست مقصود بتان</p></div>
<div class="m2"><p>از هلاک عندلیبان گلعذاران را چه حظ</p></div></div>