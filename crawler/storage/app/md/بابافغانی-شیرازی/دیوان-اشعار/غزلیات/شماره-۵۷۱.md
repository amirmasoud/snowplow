---
title: >-
    شمارهٔ ۵۷۱
---
# شمارهٔ ۵۷۱

<div class="b" id="bn1"><div class="m1"><p>سرم در راه آن سرو خرامان خاک بایستی</p></div>
<div class="m2"><p>بر او آمد شد آن قامت چالاک بایستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در آن دم کز هوای او گرفتم شاخ گل دربر</p></div>
<div class="m2"><p>دلم چون غنچه گر بشکفت باری چاک بایستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیاد آن دهان پیوسته می بوسم لب ساغر</p></div>
<div class="m2"><p>فروغی در میم زان لعل آتشناک بایستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جهانی بسته ی فتراک خود کردی بیک جولان</p></div>
<div class="m2"><p>سر آشفته من هم در آن فتراک بایستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چو از خون ریختن باکی ندارد غمزه ی شوخت</p></div>
<div class="m2"><p>بجانم ناوکی زان غمزه بی باک بایستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بدور حسن او منعم کنند از عشق بیدردان</p></div>
<div class="m2"><p>دریغا پند گویان مرا ادراک بایستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز باران عنایت کشت امید اسیران را</p></div>
<div class="m2"><p>برغم بخت من مشتی خس و خاشاک بایستی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>فغانی خانه ی دل بهر او چون ساختی خالی</p></div>
<div class="m2"><p>دل پاک تو خلوتخانهٔ آن پاک بایستی</p></div></div>