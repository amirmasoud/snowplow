---
title: >-
    شمارهٔ ۲۹۲
---
# شمارهٔ ۲۹۲

<div class="b" id="bn1"><div class="m1"><p>دلم که همره آن مه چو ابر چست شود</p></div>
<div class="m2"><p>گذار تا برود آن قدر که سست شود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ندیده دامن پاک تو مهر و ماه هنوز</p></div>
<div class="m2"><p>درست باد کتانی که خانه شست شود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلم دریغ مدار از سفینه ی عاشق</p></div>
<div class="m2"><p>که این سفینه باصلاح تو درست شود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نبات تازه ی تو می برد ز دیده گلاب</p></div>
<div class="m2"><p>تبارک الله ازان موسمی که رست شود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>من اولت چو بدیدم بغم نهادم دل</p></div>
<div class="m2"><p>که حکم خیر و شر هر کس از نخست شود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دلم بخدمت نخل قدت برآمد زار</p></div>
<div class="m2"><p>که نازکست نهالی که تازه رست شود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دلیر نیست فغانی هنوز در ره عشق</p></div>
<div class="m2"><p>مگر به خدمت اصحاب درد چست شود</p></div></div>