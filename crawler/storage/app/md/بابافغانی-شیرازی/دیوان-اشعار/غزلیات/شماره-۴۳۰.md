---
title: >-
    شمارهٔ ۴۳۰
---
# شمارهٔ ۴۳۰

<div class="b" id="bn1"><div class="m1"><p>هر زمان با خود خیال آن رخ گلگون کنم</p></div>
<div class="m2"><p>آرزوی دیدن رویت بدل افزون کنم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون خیال صورت خوب تو آرم در نظر</p></div>
<div class="m2"><p>از تحیر آفرین بر خامه ی بیچون کنم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سر ز طاعت بر ندارم گر بعمر خود دمی</p></div>
<div class="m2"><p>سجده یی در سایه آن قامت موزون کنم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>من که دارم صد نوا از ناله ی شبگیر خود</p></div>
<div class="m2"><p>گوش کی بر ارغنون بزم افلاطون کنم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گر مرا صد غم بود در دل، چو بینم روی تو</p></div>
<div class="m2"><p>برکشم آهی و غمها را ز دل بیرون کنم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در سخن هر دم چو شمعم سوز دل روشن نشد</p></div>
<div class="m2"><p>در نمیگیرد چراغم تا بکی افسون کنم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کاش پرسد غنچهٔ لعل تو از من نکته‌ای</p></div>
<div class="m2"><p>تا به تقریب سخن شرح دل پرخون کنم</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خواب شد بر دیدهٔ شب‌زنده‌دار من حرام</p></div>
<div class="m2"><p>بس که شب‌ها ناله از بی‌مهری گردون کنم</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بیت احزان فغانی کی شود بزم طرب</p></div>
<div class="m2"><p>چند در هر گوشه فریاد از دل محزون کنم</p></div></div>