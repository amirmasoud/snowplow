---
title: >-
    شمارهٔ ۴۹۰
---
# شمارهٔ ۴۹۰

<div class="b" id="bn1"><div class="m1"><p>خط مشگین چیست گرد عارض گلگون او</p></div>
<div class="m2"><p>شاه بیت دفتر حسن و وفا مضمون او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سبزه ی تر چون بگرد آن لب میگون دمید</p></div>
<div class="m2"><p>گو مشو غافل دل دیوانه از افسون او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در حضور غنچه گو بلبل زبان را بسته دار</p></div>
<div class="m2"><p>چون به آه و ناله بگشاید دل محزون او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>سرو با آن ناز و رعنایی قد و سرکشی</p></div>
<div class="m2"><p>کی تواند شد برابر با قد موزون او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کمترم از ذره در مهرش ولی چون آفتاب</p></div>
<div class="m2"><p>در نظر دارم خیال حسن روز افزون او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>درد من از ناله ی عشاق می یابد شفا</p></div>
<div class="m2"><p>گو برو مطرب که بی آهنگ شد قانون او</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>طرفه مأواییست بزم عشرت لیلی ولی</p></div>
<div class="m2"><p>شاد از او یکدم نگردد خاطر مجنون او</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>زان دو لب صد آرزو یابی فغانی را گره</p></div>
<div class="m2"><p>گر بتیغ غمزه بشکافی دل پر خون او</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>فارغم از باغ و ناز سوسن آزاد او</p></div>
<div class="m2"><p>وز فریب باغبان و جلوه ی شمشاد او</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>دل نخواهد سایه ی سرو و لب آب روان</p></div>
<div class="m2"><p>کم مبادا از سر ما خنجر بیداد او</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>هر که را تشریف رسوایی دهد سلطان عشق</p></div>
<div class="m2"><p>هر دم آید صد بلا بهر مبارک باد او</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>خانه ی امید در هر جا که طرح افگند دل</p></div>
<div class="m2"><p>آخر از اشک ندامت کنده شد بنیاد او</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>سر خوش از جام طرب شیرین بخلوتگاه ناز</p></div>
<div class="m2"><p>غم ندارد گر بتلخی جان دهد فرهاد او</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>به ز زلفت نیست ما را مرشد روشن ضمیر</p></div>
<div class="m2"><p>باد در گوش دل ما حلقه ی ارشاد او</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>بلبل بستان عشق آمد فغانی زان دو رخ</p></div>
<div class="m2"><p>کم مباد از گلشن دل ناله و فریاد او</p></div></div>