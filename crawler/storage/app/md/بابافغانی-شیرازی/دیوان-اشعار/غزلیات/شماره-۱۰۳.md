---
title: >-
    شمارهٔ ۱۰۳
---
# شمارهٔ ۱۰۳

<div class="b" id="bn1"><div class="m1"><p>دوا خواهم ز تو ادراکم اینست</p></div>
<div class="m2"><p>هلاک آن لبم تریاکم اینست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکی بند قبا بگشا ای گل</p></div>
<div class="m2"><p>دوای سینه ی صد چاکم اینست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ترا در بر کشم یا کشته گردم</p></div>
<div class="m2"><p>تمنای دل بیباکم اینست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بروز آرم شبی با چون تو ماهی</p></div>
<div class="m2"><p>مراد از انجم و افلاکم اینست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همه حرف تو روید بر زبانم</p></div>
<div class="m2"><p>چه گویم چون در آب و خاکم اینست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بسوزان جان من هر جا که باشی</p></div>
<div class="m2"><p>بگو من آتشم خاشاکم اینست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>اگر زهرم چشانی ای دل افروز</p></div>
<div class="m2"><p>مراد از لعل تو تریاکم اینست</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گهی سوزد دلت بهر فغانی</p></div>
<div class="m2"><p>نشان آه آتشناکم اینست</p></div></div>