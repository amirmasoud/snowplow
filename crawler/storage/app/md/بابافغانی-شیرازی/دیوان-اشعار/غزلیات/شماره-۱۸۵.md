---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>مدام از کشت امیدم خس و خاشاک می‌روید</p></div>
<div class="m2"><p>عجب گر بر مراد من گلی از خاک می‌روید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چو من بی‌بهره‌ام از عشرت دنیا چه سودم زان</p></div>
<div class="m2"><p>که بر طرف چمن گل می‌دمد یا تاک می‌روید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>پریشانم ز سعد و نحسن گردون آه ازین گل‌ها</p></div>
<div class="m2"><p>که نونو بهر من از گلشن افلاک می‌روید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مرا از هر گل نو در جگر خاری‌ست پنداری</p></div>
<div class="m2"><p>ز خاک بخت من آن هم به صد امساک می‌روید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>منم در عالم و این دانه‌های اشک بی‌قیمت</p></div>
<div class="m2"><p>که از دل‌های ریش و سینه‌های چاک می‌روید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دمی باقی‌ست دامن برمچین از آب و خاک من</p></div>
<div class="m2"><p>هنوز اندک گیاهی زین گل نمناک می‌روید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>فغانی پاک شو تا مهر گردد کینهٔ دشمن</p></div>
<div class="m2"><p>که داروی محبت از زمین پاک می‌روید</p></div></div>