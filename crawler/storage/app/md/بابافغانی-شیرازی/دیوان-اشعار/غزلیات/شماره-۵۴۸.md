---
title: >-
    شمارهٔ ۵۴۸
---
# شمارهٔ ۵۴۸

<div class="b" id="bn1"><div class="m1"><p>ای رقیب آندم که بر کف تیغ بیدادش دهی</p></div>
<div class="m2"><p>از من سرگشته بهر امتحان یادش دهی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شکل شیرین را نکو آراستی آه ای قضا</p></div>
<div class="m2"><p>گر بدین صورت خرامی سوی فرهادش دهی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر زمان از خیل خوبان فتنه یی سازی سوار</p></div>
<div class="m2"><p>وز جفا سر در پی دلهای ناشادش دهی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون قدش در جلوه کی باشد اگر زاب حیات</p></div>
<div class="m2"><p>صورتی سازی و زیب سر و آزادش دهی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>اشک من کز مقدم او دور ماند ای باغبان</p></div>
<div class="m2"><p>سر بپای ارغوان و سرو و شمشادش دهی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جمع کردم غنچه ی دل را ولی ترسم که باز</p></div>
<div class="m2"><p>دامن افشان بگذری ای سرو و بر بادش دهی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بر سر کوی ملامت خانه می سازد دلم</p></div>
<div class="m2"><p>وای اگر سنگ جفایی بهر بنیادش دهی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>داد می خواهد دل آزرده ای سلطان حسن</p></div>
<div class="m2"><p>وه چه باشد کز سر لطف و کرم دادش دهی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>گر در آیی در خیال زاهد خلوت نشین</p></div>
<div class="m2"><p>رخنه در دینش کنی تشویش اورادش دهی</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>مرشد عشق ای فغانی چون شدی کاش از کرم</p></div>
<div class="m2"><p>دستگیر او شوی یک نکته ارشادش دهی</p></div></div>