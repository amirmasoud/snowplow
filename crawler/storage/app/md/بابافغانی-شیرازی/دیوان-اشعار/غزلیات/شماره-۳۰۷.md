---
title: >-
    شمارهٔ ۳۰۷
---
# شمارهٔ ۳۰۷

<div class="b" id="bn1"><div class="m1"><p>مه خورشدروی من دمی یک جا نمی‌گنجد</p></div>
<div class="m2"><p>چنان گرمست بر دل‌ها که در دل‌ها نمی‌گنجد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>نسیم دامنش گلزار گیتی برنمی‌آرد</p></div>
<div class="m2"><p>غبار موکبش در عرصهٔ غبرا نمی‌گنجد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شهیدی کز سر کویش غبارآلود بیرون شد</p></div>
<div class="m2"><p>ز شوقش تا ابد در جنت‌المأوا نمی‌گنجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عجب گر بر سلام کس فرود آرد سر ابرو</p></div>
<div class="m2"><p>چو برطرف کلاهش عز و استغنا نمی‌گنجد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ازین می خوردن پنهان و پیدا آتشی دارم</p></div>
<div class="m2"><p>که در پنهان ندارد جا و در پیدا نمی‌گنجد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>اگر فردای دیگر مستی جام و صبوح اینست</p></div>
<div class="m2"><p>جزای این گنه در مجلس فردا نمی‌گنجد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ز عشق کافری پیرانه سر در بزم میخواران</p></div>
<div class="m2"><p>به دینم رفت بیدادی که در دنیا نمی‌گنجد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>نخواهد در سر و کار بلای عشق و مستی شد</p></div>
<div class="m2"><p>وجود پربلای من که در یک جا نمی‌گنجد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>ز بیداد غیوری آنچه از کافردلی دیدم</p></div>
<div class="m2"><p>چه جای کعبه در بتخانهٔ ترسا نمی‌گنجد</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>جنون عشق و از جانان خیال بوسه و آغوش</p></div>
<div class="m2"><p>مگو این‌ها که این‌ها در خیال ما نمی‌گنجد</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>فغانی را دهان آرزو شیرین نخواهد شد</p></div>
<div class="m2"><p>پر از زهرست جام او در آن حلوا نمی‌گنجد</p></div></div>