---
title: >-
    شمارهٔ ۴۰۵
---
# شمارهٔ ۴۰۵

<div class="b" id="bn1"><div class="m1"><p>رفتم ز کوی تو، چو مقامی نداشتم</p></div>
<div class="m2"><p>دل برگرفتم از تو، چو کامی نداشتم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یکباره از وفای تو برداشتم امید</p></div>
<div class="m2"><p>چون از تو التفات تمامی نداشتم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر دل کدام روز که از همدمان تو</p></div>
<div class="m2"><p>دردی ز ناخوشی پیامی نداشتم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>روزی بکوی تو نگذشتم که در کمین</p></div>
<div class="m2"><p>آه کسی ز گوشه ی بامی نداشتم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>فریاد ازان زمان که رسیدی تو سرگران</p></div>
<div class="m2"><p>وز بیخودی مجال سلامی نداشتم</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عمرم گذشت در غم و آخر بکام دل</p></div>
<div class="m2"><p>در گوشه یی بپیش تو جامی نداشتم</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روزی نشد که همچو فغانی ز جور بخت</p></div>
<div class="m2"><p>فریاد صبح و گریه ی شامی نداشتم</p></div></div>