---
title: >-
    شمارهٔ ۲۲۹
---
# شمارهٔ ۲۲۹

<div class="b" id="bn1"><div class="m1"><p>به این جادو و شانم تا سر پیوند خواهد بود</p></div>
<div class="m2"><p>به زنجیر محبت گردنم در بند خواهد بود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اگر صد خوب پیش آمد ترا یاد آرم و سوزم</p></div>
<div class="m2"><p>بلا آندل که با وصل تو حاجتمند خواهد بود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین مجلس بچیزی هر کسی دندان فرو برده</p></div>
<div class="m2"><p>امید ما به آن لبهای شکر خند خواهد بود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اگر تلخی رسد در صحبت احباب شیرین باش</p></div>
<div class="m2"><p>مکن ابرو ترش تا کی گلاب و قند خواهد بود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هنوزم دل تپید گر خوشتر از جان در برم آیی</p></div>
<div class="m2"><p>کجا از مژده ی قاصد دلم خرسند خواهد بود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>نسیم پیرهن گر روضه سازد بیت احزان را</p></div>
<div class="m2"><p>همان خون در دل پیر از غم فرزند خواهد بود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>عروس دهر هر ده روز عهدی بسته با یاری</p></div>
<div class="m2"><p>مپنداری که تا آخر به یک سوگند خواهد بود</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>وفای عمر اگر اینست سهلست آب حیوان هم</p></div>
<div class="m2"><p>بخواهد خاک شد این خسته هم تا چند خواهد بود</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>نه مرد عشق خوبانی فغانی زین هوس بازآ</p></div>
<div class="m2"><p>ملامت بشنوی گفتم ز یاران پند خواهد بود</p></div></div>