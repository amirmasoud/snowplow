---
title: >-
    شمارهٔ ۲۹۶
---
# شمارهٔ ۲۹۶

<div class="b" id="bn1"><div class="m1"><p>خنجر کشید و عربده با اهل حال کرد</p></div>
<div class="m2"><p>آن ترک مست بین که چه با خود خیال کرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>حسنش یکی هزار شد و آمد از سفر</p></div>
<div class="m2"><p>خوش آن هوا که پرورش این نهال کرد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر شیوه یی ز صورت او معنییست خاص</p></div>
<div class="m2"><p>غافل همین ملاحظه ی خط و خال کرد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ناصح برو که انس نگیرد به هیچ کس</p></div>
<div class="m2"><p>دیوانه یی که همدمی آن غزال کرد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یا رب چه شد که از سر ما سایه برگرفت</p></div>
<div class="m2"><p>سروی که کارها همه بر اعتدال کرد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایزد ترا ز بهر دل خلق آفرید</p></div>
<div class="m2"><p>وانگه چنین سرآمد و صاحب جمال کرد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بگذار خون غیر جوانان بروزگار</p></div>
<div class="m2"><p>کاین شحنه چند خون چنین پایمال کرد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>خونم چو آب خورد لبت وه چه خط نوشت</p></div>
<div class="m2"><p>آنکس که بر تو خون فغانی حلال کرد</p></div></div>