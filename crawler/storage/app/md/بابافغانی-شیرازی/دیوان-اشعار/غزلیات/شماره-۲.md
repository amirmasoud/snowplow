---
title: >-
    شمارهٔ ۲
---
# شمارهٔ ۲

<div class="b" id="bn1"><div class="m1"><p>ای از لب تو خطبه کلام قدیم را</p></div>
<div class="m2"><p>باعث، رسوم شرع تو امید و بیم را</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>اول عظیم داشته شأن ترا خدای</p></div>
<div class="m2"><p>وانگاه برفراشته عرش عظیم را</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چرخ اثیر تا شرف از گوهرت نیافت</p></div>
<div class="m2"><p>درهم نریخت اینهمه در یتیم را</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر شاهراه عقل نهادی چراغ شرع</p></div>
<div class="m2"><p>تا خلق پی برند ره مستقیم را</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>قول تو هر کجا که دلیل آورد فقیه</p></div>
<div class="m2"><p>دیگر مجال بحث نماند حکیم را</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دارد چنان دمی که بمعجز فرود برد</p></div>
<div class="m2"><p>شمشیر خطبه ی تو عصای کلیم را</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>روی تو در سلامت خلقست وین سخن</p></div>
<div class="m2"><p>روشن بود چون آینه طبع سلیم را</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>آن دم که فخر داشت بدان سالها مسیح</p></div>
<div class="m2"><p>در گلشن تو گشت کرامت نسیم را</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>بر حرف زلف و خال، فغانی قلم کشید</p></div>
<div class="m2"><p>وز دفتر تو خواند الف لام میم را</p></div></div>