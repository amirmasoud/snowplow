---
title: >-
    شمارهٔ ۳۵۲
---
# شمارهٔ ۳۵۲

<div class="b" id="bn1"><div class="m1"><p>چنان تیزست در خون ریختن مژگان خونریزش</p></div>
<div class="m2"><p>که خون دل چکد از دیده ها چون بنگرم تیزش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>لبش از عشوهٔ شیرین دهد کام دلم روزی</p></div>
<div class="m2"><p>ولی در غمزه بیدادست چشم فتنه انگیزش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>درین باغ کهن چون سبزه ی نو خیزد از خاکم</p></div>
<div class="m2"><p>هنوزم در نظر باشد خیال خط نوخیزش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مگر آگه شد از سوز دل من شمع در گریه</p></div>
<div class="m2"><p>که بس دلسوز می آید سرشک آتش آمیزش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز شوق لعل میگونت بخون خود بود تشنه</p></div>
<div class="m2"><p>دل بیمار من کز آب حیوانست پرهیزش</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>فغانی می رود افتان و خیزان در عنان او</p></div>
<div class="m2"><p>که آویزد دل پر خون بفتراک دلاویزش</p></div></div>