---
title: >-
    شمارهٔ ۴۶۸
---
# شمارهٔ ۴۶۸

<div class="b" id="bn1"><div class="m1"><p>لاله عطر آمیز و گل مشکین نفس خواهد شدن</p></div>
<div class="m2"><p>بلابلانرا دیدن بستان هوس خواهد شدن</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ناز افزون کن که بی منت طفیل راه تست</p></div>
<div class="m2"><p>آنقدر وجهی که ما را دسترس خواهد شدن</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اینهمه ناز و سرافرازی که دارد نخل تو</p></div>
<div class="m2"><p>میوه اش کی روزی دندان کس خواهد شدن</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بر نمی دارم دل از لعل لبت گر خون شود</p></div>
<div class="m2"><p>این چنین شوقی نپنداری که بس خواهد شدن</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>با من ناکس نشستی سوختم، این بردهد</p></div>
<div class="m2"><p>هر کجا آتش حریف خار و خس خواهد شدن</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گر چنین میخواره و اوباش خواهی زیستن</p></div>
<div class="m2"><p>عاشقان را خانه تاراج عسس خواهد شدن</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از پی آب حیات آمد فغانی سوی تو</p></div>
<div class="m2"><p>همچنان لب تشنه آیا باز پس خواهد شدن</p></div></div>