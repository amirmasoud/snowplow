---
title: >-
    شمارهٔ ۶
---
# شمارهٔ ۶

<div class="b" id="bn1"><div class="m1"><p>روزیکه فلک بکشت ما داس نهد</p></div>
<div class="m2"><p>نامرد چو مرده تن بکرباس نهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کو شیر دلی که زیر شمشیر فنا</p></div>
<div class="m2"><p>دندان بجگر جگر بالماس نهد</p></div></div>