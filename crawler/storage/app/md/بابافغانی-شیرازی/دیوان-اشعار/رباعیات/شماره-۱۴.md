---
title: >-
    شمارهٔ ۱۴
---
# شمارهٔ ۱۴

<div class="b" id="bn1"><div class="m1"><p>افسوس که آتشم ببیهوده فسرد</p></div>
<div class="m2"><p>وین جام لبالبم رسیدست بدرد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کوشم که کنم دگر چراغی روشن</p></div>
<div class="m2"><p>ترسم که چو برفروزمش باید مرد</p></div></div>