---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>می نوش که شد چمن زر افشان ز خزان</p></div>
<div class="m2"><p>رخ چون گل آتشین کن از آب رزان</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>کز خاک بسی سرو و قد لاله عذار</p></div>
<div class="m2"><p>آیند روان روند چون باد وزان</p></div></div>