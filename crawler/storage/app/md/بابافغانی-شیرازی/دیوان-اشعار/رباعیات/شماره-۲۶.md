---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>در میکده ها حکم جنونم کردند</p></div>
<div class="m2"><p>در صومعه رفتم و برونم کردند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>هم گوشه ی میخانه که توبه شکنان</p></div>
<div class="m2"><p>مژگان سیه سرخ بخونم کردند</p></div></div>