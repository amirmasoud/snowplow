---
title: >-
    شمارهٔ ۱۲
---
# شمارهٔ ۱۲

<div class="b" id="bn1"><div class="m1"><p>منت که رسیدیم بکام دل ازو</p></div>
<div class="m2"><p>حل گشت بما جهان جهان مشکل ازو</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بنگر که چه سهو کرده باشیم همه</p></div>
<div class="m2"><p>معشوق چنین بما و ما غافل ازو</p></div></div>