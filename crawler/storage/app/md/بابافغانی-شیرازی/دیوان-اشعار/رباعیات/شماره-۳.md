---
title: >-
    شمارهٔ ۳
---
# شمارهٔ ۳

<div class="b" id="bn1"><div class="m1"><p>تا هستی ما فنای مطلق نشود</p></div>
<div class="m2"><p>جان را صفت بقا محقق نشود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر سر دار سر نبازد منصور</p></div>
<div class="m2"><p>بیخود متکلم انا الحق نشود</p></div></div>