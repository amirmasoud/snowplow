---
title: >-
    شمارهٔ ۱۲۶
---
# شمارهٔ ۱۲۶

<div class="b" id="bn1"><div class="m1"><p>ای دل دیوانه از اندوه جانان غم مخور</p></div>
<div class="m2"><p>وصل خواهی دید زود از درد هجران غم مخور</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خوش به سودای دو چشم آهوی سرگشته‌ای</p></div>
<div class="m2"><p>با صبا میگرد در کوه و بیابان غم مخور</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ماه روی یار میخواهی چو بلبل بیقرار</p></div>
<div class="m2"><p>نعره زن مستانه در صحن گلستان غم مخور</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چشم چون خواهی بروی ماه تابان برگشای</p></div>
<div class="m2"><p>همچو ابراز گریه خونبار گریان غم مخور</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو مور لنگ بر جانم چو خواهی شد سوار</p></div>
<div class="m2"><p>از سپاه و لشکر نوح و سلیمان غم مخور</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهیا در حلقه زلف مه و خورشید او</p></div>
<div class="m2"><p>تا به حال خود رسی از ضرب چوگان غم مخور</p></div></div>