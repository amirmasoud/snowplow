---
title: >-
    شمارهٔ ۱۲۴
---
# شمارهٔ ۱۲۴

<div class="b" id="bn1"><div class="m1"><p>از بدو نیک و نیک و بد بگذر</p></div>
<div class="m2"><p>بکن از عقل و نفس خویش حذر</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مردم چشم و دیده دل شو</p></div>
<div class="m2"><p>تا به بینی نگار را به نظر</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شو مسافر به عالم جبروت</p></div>
<div class="m2"><p>ملکوت است ملک بحر و بر</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>جز لب خشک و چشم خون افشان</p></div>
<div class="m2"><p>ما نداریم هیچ زاد سفر</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>در عطش سوختیم و باکی نیست</p></div>
<div class="m2"><p>لب او هست ساقی کوثر</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع جان شد بتی و او شاهد</p></div>
<div class="m2"><p>حبذا شمع و شاهد و دلبر</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>وه چو حسن است اینکه در دو جهان</p></div>
<div class="m2"><p>همه را هست عشق او در سر</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ناید از رفته های آن عالم</p></div>
<div class="m2"><p>بر ما کس بغیر پیغمبر</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>شد بدان عالم و درون آمد</p></div>
<div class="m2"><p>همه دیدند مؤمن و کافر</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>غرض این بود آمدن اینجا</p></div>
<div class="m2"><p>که شود خلق را بحق رهبر</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>جبرئیل امین بدو نرسید</p></div>
<div class="m2"><p>که از او درگذشت بالاتر</p></div></div>
<div class="b" id="bn12"><div class="m1"><p>اوست محبوب حضرت عزت</p></div>
<div class="m2"><p>بهمه انبیا بحق سرور</p></div></div>
<div class="b" id="bn13"><div class="m1"><p>همه طفلان مکتب اویند</p></div>
<div class="m2"><p>از صغار و کبار و خیر و ز شر</p></div></div>
<div class="b" id="bn14"><div class="m1"><p>او چو گنج وصال حق را یافت</p></div>
<div class="m2"><p>میل او کی بود به سیم و به زر</p></div></div>
<div class="b" id="bn15"><div class="m1"><p>کوهیا عیب هیچ کس نکنی</p></div>
<div class="m2"><p>تا قبولت کنند اهل نظر</p></div></div>