---
title: >-
    شمارهٔ ۵۳
---
# شمارهٔ ۵۳

<div class="b" id="bn1"><div class="m1"><p>آن مه ترک چون گل خنده زنان دی بدمست</p></div>
<div class="m2"><p>قدح باده چو لعل لب خونخوار بدست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از دل سوخته پیشش چو کباب آوردیم</p></div>
<div class="m2"><p>کام او سوخت لبش گفت کبابی گرم است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ای جان جهان سوختم از هجر تو من</p></div>
<div class="m2"><p>گفت بی ما منشین با توام از روز الست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تاحدیث از لب آن ساقی جان بشنیدم</p></div>
<div class="m2"><p>روح من مست شد و شیشه دلدار شکست</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید ساقی که شکستم قدح از شوق لبش</p></div>
<div class="m2"><p>گفت دیوانه شدی عاشق و معشوق پرست</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>قصد کردم که بگیرم شکن طره او</p></div>
<div class="m2"><p>هم بزنجیر سر زلف مرا در هم بست</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید کوهی که بزنجیر وفا در بند است</p></div>
<div class="m2"><p>در خم جعد سیه رفت بخلوت بنشست</p></div></div>