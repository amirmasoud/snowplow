---
title: >-
    شمارهٔ ۱۸۶
---
# شمارهٔ ۱۸۶

<div class="b" id="bn1"><div class="m1"><p>حرف اسرار ازل بر دل خود خوانا چشم</p></div>
<div class="m2"><p>که خموش است مرا هر دو لب گویا چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از همه خلق جهان بر در دیری دیدیم</p></div>
<div class="m2"><p>داشت بر عاشق خود او پسر ترسا چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب معراج خداوند محمد راگفت</p></div>
<div class="m2"><p>منکر هر طرف و دور مدار از ما چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیده عقل بدیدار خدا چون نرسد</p></div>
<div class="m2"><p>باز کردیم بعین و صفت و اسما چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چشم او با دل کوهی بسر صدق بگفت</p></div>
<div class="m2"><p>نگشائی بجز از دیده ما هرجا چشم</p></div></div>