---
title: >-
    شمارهٔ ۲۴
---
# شمارهٔ ۲۴

<div class="b" id="bn1"><div class="m1"><p>خوانده ام از عنده ام الکتاب</p></div>
<div class="m2"><p>آیه طوبی لهم حسن المآب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باز گشتم بسوی آن حضرت</p></div>
<div class="m2"><p>چون شنودم ز حق الیه متاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>لمن الملک گفت حسن و رخش</p></div>
<div class="m2"><p>کرد از خود سئوال و داد جواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ماه و خورشید خاک آن کوبند</p></div>
<div class="m2"><p>شد بر آن در اقل ما فی الباب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به کلام فصیح حضرت حق</p></div>
<div class="m2"><p>میکند با حبیب خویش خطاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>تا به بخشد مرا وصال ابد</p></div>
<div class="m2"><p>کرم و لطف اوست بی پایاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مطلب گفت غیر ما از ما</p></div>
<div class="m2"><p>چه عطا به ز دیدن وهاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>عشق در جان ما جمال نمود</p></div>
<div class="m2"><p>چون بدرگاه دل شدم بواب</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>همچو خورشید صبحگاهی بود</p></div>
<div class="m2"><p>آن مه بدر کرد رفع حجاب</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>شاهد غیب گوش دل ما لید</p></div>
<div class="m2"><p>گفت بی ماجری شدی درخواب</p></div></div>
<div class="b" id="bn11"><div class="m1"><p>چون رسیدی به آفتاب قدیم</p></div>
<div class="m2"><p>برگذر کوهیا ز آب تراب</p></div></div>