---
title: >-
    شمارهٔ ۲۳۰
---
# شمارهٔ ۲۳۰

<div class="b" id="bn1"><div class="m1"><p>هست گردانید ما را از جهان نیستی</p></div>
<div class="m2"><p>کرد منزل مرغ جان در آشیان نیستی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خانه تن را که قصر پادشاه روح شد</p></div>
<div class="m2"><p>خاک راهی یافتم در آستان نیستی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>اعتبارات یقین در نیستی مطلق است</p></div>
<div class="m2"><p>هستی واجب در آید در نهان نیستی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چون مرادت لا بود از گفتن لاریب فیه</p></div>
<div class="m2"><p>گل شکفت از شاخ لا در بوستان نیستی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>داده از یکدانه ارزن گفت نحن الزارعون</p></div>
<div class="m2"><p>نعمت هستی او پرکرده جان نیستی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش هستی چو غیر خویشتن را پاک سوخت</p></div>
<div class="m2"><p>نه فلک شد بر هوا همچون دخان نیستی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوهیا گر چه الف شد مبدأ هستی ذات</p></div>
<div class="m2"><p>در معاد خلق لام است ابروان نیستی</p></div></div>