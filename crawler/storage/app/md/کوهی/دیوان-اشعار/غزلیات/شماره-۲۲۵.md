---
title: >-
    شمارهٔ ۲۲۵
---
# شمارهٔ ۲۲۵

<div class="b" id="bn1"><div class="m1"><p>باده را نشأیست روحانی</p></div>
<div class="m2"><p>جرعه ای نوش کن که تادانی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>باده و شمع و شاهد و مجلس</p></div>
<div class="m2"><p>هست اسرار سر ربانی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>نوش کن جرعه ای بیخود شو</p></div>
<div class="m2"><p>تا نه خیزد به پیش حیرانی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ساقی مست حضرت عزت</p></div>
<div class="m2"><p>میدهد باده های سبحانی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شمع و نقل و شراب و شاهد داد</p></div>
<div class="m2"><p>هست این جمله را اگر دانی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شمع خود را بسوخت در مجلس</p></div>
<div class="m2"><p>خواند پروانه را به مهمانی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت کوهی که عینها مائیم</p></div>
<div class="m2"><p>دیدم او را بشکل انسانی</p></div></div>