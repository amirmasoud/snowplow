---
title: >-
    شمارهٔ ۱۴۶
---
# شمارهٔ ۱۴۶

<div class="b" id="bn1"><div class="m1"><p>دلم در بند زلف تست ای دلبر مرنجانش</p></div>
<div class="m2"><p>بخوان وصل خود بنشان پس ایمه همچو مهمانش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بمهمانی دل ما را نداری جز جگر خواری</p></div>
<div class="m2"><p>چو پروانه از او کردی به شمع چهره بریانش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بیک حالت نه می بینم دل صد پاره را هر دم</p></div>
<div class="m2"><p>چو زلف و خال خودداری کنی جمع پریشانش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>چو خورشید از گریبان همه ذرات سر برزد</p></div>
<div class="m2"><p>بغیر از او که می گردد بگرداگرد دامانش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بدزدی زلف او دلرا سحر بگرفت و درهم بست</p></div>
<div class="m2"><p>چو کوهی با صبا شد دوش در صحن گلستانش</p></div></div>