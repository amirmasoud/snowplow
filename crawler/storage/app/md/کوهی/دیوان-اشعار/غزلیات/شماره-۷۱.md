---
title: >-
    شمارهٔ ۷۱
---
# شمارهٔ ۷۱

<div class="b" id="bn1"><div class="m1"><p>برداشتیم از کف ساقی روح راح</p></div>
<div class="m2"><p>درجام آفتاب می لعل هر صباح</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شادیم وخرمیم ز صبح ازل مدام</p></div>
<div class="m2"><p>چونکرده ایم دیده بروی تو افتتاح</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساقی ز روی ما و منی همچو آفتاب</p></div>
<div class="m2"><p>میگوید از کرم دو جهانرا که الصلاح</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>از بطن آفتاب بزدایم ما همه</p></div>
<div class="m2"><p>خورشید را چو ماه در آورد در نکاح</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوهی بروح قدس شدی جمله جاودان</p></div>
<div class="m2"><p>از فعل شوم خویش اگر یافتی فلاح</p></div></div>