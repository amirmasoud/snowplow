---
title: >-
    شمارهٔ ۱۱۲
---
# شمارهٔ ۱۱۲

<div class="b" id="bn1"><div class="m1"><p>عارفان میخانه را فردوس اعلی گفته اند</p></div>
<div class="m2"><p>اهل معنی داند این کز روی معنی گفته اند</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون سقیهم ربهم فرمود ایزد در کلام</p></div>
<div class="m2"><p>حسن ساقی گفته اند و وجه باقی گفته اند</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>شب نشینان محبت در مناجات خدا</p></div>
<div class="m2"><p>روح را موسی و دل طور تجلی گفته اند</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>پاکبازان مجرد بهر دیدار خدا</p></div>
<div class="m2"><p>قطع دنیا کرده اند و ترک عقبی گفته اند</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جز فنای محض هر کودم زند در کوی دوست</p></div>
<div class="m2"><p>خورده بینانش همه پندار و دعوی گفته اند</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دم مزن در آینه کوهی چو می بینی عیان</p></div>
<div class="m2"><p>آنچه موجودات در سفلی و علوی گفته اند</p></div></div>