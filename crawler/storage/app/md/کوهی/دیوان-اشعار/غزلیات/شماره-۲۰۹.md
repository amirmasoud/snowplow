---
title: >-
    شمارهٔ ۲۰۹
---
# شمارهٔ ۲۰۹

<div class="b" id="bn1"><div class="m1"><p>دلم از درد تو فریاد برآورد که آه</p></div>
<div class="m2"><p>شده از حال دلم جمله ی ذرات گواه</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا سگ کوی تو بر دیده ما پای نهد</p></div>
<div class="m2"><p>خاک گشتیم و فتادیم از این رو در راه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گفتم ای جان جهان جز تو ندارم در دل</p></div>
<div class="m2"><p>گفت مائیم چوجان در دلت الله الله</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا ز خورشید رخش دیده ما روشن شد</p></div>
<div class="m2"><p>روی او بود بهر ذره چو کردیم نگاه</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر در غیر خدا کوهی دیوانه نرفت</p></div>
<div class="m2"><p>دارد از حضرت سلطان جهان شی الله</p></div></div>