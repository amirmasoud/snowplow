---
title: >-
    شمارهٔ ۲۲۰
---
# شمارهٔ ۲۲۰

<div class="b" id="bn1"><div class="m1"><p>آشکارا و نهان ماتوئی</p></div>
<div class="m2"><p>جان جان و جسم جان ما توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از قدم تا فرق می بینم تو را</p></div>
<div class="m2"><p>چشم بینا و زبان ما توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو طفلان در کنارت بیقرار</p></div>
<div class="m2"><p>شیر ما در آب و نان ما توئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بلبل روحم همی گوید بلند</p></div>
<div class="m2"><p>باغ و سرو گلعذار ما توئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کل یوم هو فی شان آیتی است</p></div>
<div class="m2"><p>با تو مشغولیم شان ما توئی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جان ببوسی با تو سودا کرده ایم</p></div>
<div class="m2"><p>نقد بازار دکان ما توئی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>هر دو عالم هست خاک راه تو</p></div>
<div class="m2"><p>هم زمین و آسمان ما توئی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از عطاهای تو شد کوهی غنی</p></div>
<div class="m2"><p>آفتاب و بحر و کان ماتوئی</p></div></div>