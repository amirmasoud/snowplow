---
title: >-
    شمارهٔ ۱۶۴
---
# شمارهٔ ۱۶۴

<div class="b" id="bn1"><div class="m1"><p>حیدر آسا جان کافر کیش در روز مصاف</p></div>
<div class="m2"><p>ذوالفقار روح را ایدل برآور از غلاف</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>همچو کرم پیله بر خود می تنی از حرص و آز</p></div>
<div class="m2"><p>عنکبوتی نیستی در خانه دنیا مباف</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده صافی بنوش ای ساقیان صاف بین</p></div>
<div class="m2"><p>تا شود آئینه دل از کدورت صاف صاف</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آفتاب روی ساقی بین که جام می بکف</p></div>
<div class="m2"><p>همچو خورشید است گرد بزم مستان در طواف</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه چه لطف است اینکه خاص و عام را ساقی روح</p></div>
<div class="m2"><p>تا ننوشد از کف او می نمیدارد معاف</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ایدل دیوانه تا یابی ز وصل او خبر</p></div>
<div class="m2"><p>سینه را از درد جانان شرحه شرحه کن شکاف</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوهیا طاقت نداری تا به بینی آنجمال</p></div>
<div class="m2"><p>در پس دیوار تا کی می‌زنی لاف و گزاف</p></div></div>