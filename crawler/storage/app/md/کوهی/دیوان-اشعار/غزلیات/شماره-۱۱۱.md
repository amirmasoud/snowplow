---
title: >-
    شمارهٔ ۱۱۱
---
# شمارهٔ ۱۱۱

<div class="b" id="bn1"><div class="m1"><p>شمع روی تو دلمرا چو بجان میسوزد</p></div>
<div class="m2"><p>آفتاب از دم آتش نفسان می سوزد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بحر از کریه ما در بصدف کرد آورد</p></div>
<div class="m2"><p>لعل از یاد لبت در دل کان می سوزد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>کام دل هیچکس از لعل تو هرگز نگرفت</p></div>
<div class="m2"><p>نام آن لب همه را کام و زبان می سوزد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عکس خورشید رخش در دل دریا افتاد</p></div>
<div class="m2"><p>از حرارت جگر آب روان می سوزد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>پیش رخسار تو ای شمع سراپرده جان</p></div>
<div class="m2"><p>همچو پروانه بیکدم دو جهان می سوزد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>آتش روی تو تنها نه دل گل را سوخت</p></div>
<div class="m2"><p>جان بلبل ز غمت نعره زنان می سوزد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر چه رخسار تو در سنگ چو آتش جا کرد</p></div>
<div class="m2"><p>تا نگویند که او چون دیگران می سوزد</p></div></div>