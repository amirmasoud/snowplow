---
title: >-
    شمارهٔ ۱۴۰
---
# شمارهٔ ۱۴۰

<div class="b" id="bn1"><div class="m1"><p>ما نگردیم ز سودای پریرویان بس</p></div>
<div class="m2"><p>نکند دل ز تمنای رخ جانان بس</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بلبل روح مرا در چمن باغ جنان</p></div>
<div class="m2"><p>روی او نسترن و خط خوش ریحان بس</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>عندلیب سحری گریه کنان میگوید</p></div>
<div class="m2"><p>که دلم را بسحرگاه گل خندان بس</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا به بیند همه اسماء و صفات خود را</p></div>
<div class="m2"><p>پیش دیدار خداوند دل انسان بس</p></div></div>