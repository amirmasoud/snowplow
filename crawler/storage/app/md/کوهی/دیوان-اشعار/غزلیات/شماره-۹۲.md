---
title: >-
    شمارهٔ ۹۲
---
# شمارهٔ ۹۲

<div class="b" id="bn1"><div class="m1"><p>سرو گل چهره اگر بند قبا بگشاید</p></div>
<div class="m2"><p>دل پرخون مرا نشود و نما بگشاید</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>یار اگر کاکل مشکین بگشاید بسحر</p></div>
<div class="m2"><p>مشک از نافه آهوی ختا بگشاید</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>صبح صادق بدمد هر طرف از شش جهتم</p></div>
<div class="m2"><p>گر شبی زلف تو را باد صبا بگشاید</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دارم امید به الطاف خداوند که باز</p></div>
<div class="m2"><p>دوست بر روی دلم چشم رضا بگشاید</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از کمان خانه ابروی بت ترک چکل</p></div>
<div class="m2"><p>کار درویش من بی سر و پا بگشاید</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در مقصود که بر زاهد و عابد بستند</p></div>
<div class="m2"><p>مگر از آه دل خسته ما بگشاید</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از ازل تا به ابد روزه کوهی نگشاد</p></div>
<div class="m2"><p>روزه داری است که از خوان شما بگشاید</p></div></div>