---
title: >-
    شمارهٔ ۲۰۲
---
# شمارهٔ ۲۰۲

<div class="b" id="bn1"><div class="m1"><p>دیده ام در دل و جان روی تو را دزدیده</p></div>
<div class="m2"><p>کرده ام طوف سرکوی تو را دزدیده</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جگرم خون شد و دزدیده و دل زین حسرت</p></div>
<div class="m2"><p>تا صبا دید شبی موی تو را دزدیده</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>منم آن دزد که شب تا به سحر می گردم</p></div>
<div class="m2"><p>هر دم از باد صبا بوی تو را دزدیده</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>میگدازد همه شب روز از این بیم چو شمع</p></div>
<div class="m2"><p>که ز رخ خال چو هندوی تو را دزدیده</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>به چمن سرو سهی را به سحر گه دیدم</p></div>
<div class="m2"><p>سایه ی قامت دلجوی تو را دزدیده</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ماه و خورشید بدزدی برد از روی تو نور</p></div>
<div class="m2"><p>بر فلک نیز ملک خوی تو را دزدیده</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گفت کوهی به شب تار به آواز بلند</p></div>
<div class="m2"><p>ذره ی حلقه گیسوی تو را دزدیده</p></div></div>