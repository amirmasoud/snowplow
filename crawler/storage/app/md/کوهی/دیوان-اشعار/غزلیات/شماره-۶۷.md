---
title: >-
    شمارهٔ ۶۷
---
# شمارهٔ ۶۷

<div class="b" id="bn1"><div class="m1"><p>درد جان داریم درمان الغیاث</p></div>
<div class="m2"><p>داد خواهانیم سلطان الغیاث</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>از تطاول های زلف سرکشت</p></div>
<div class="m2"><p>صبح وصل و شام هجران الغیاث</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>راند ما را همچو سگ از در بدر</p></div>
<div class="m2"><p>پیش شاه از جور سلطان الغیاث</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>همچو مور لنگ از جور سپاه</p></div>
<div class="m2"><p>گفت دل پیش سلیمان الغیاث</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دوش میگفتی که دادت میدهم</p></div>
<div class="m2"><p>تا نگردی زو پشیمان الغیاث</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>دل ز حلم نفس شوم بدخصال</p></div>
<div class="m2"><p>گفت نزد جان جانان الغیاث</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>ذره ها چون سوخت اندر آفتاب</p></div>
<div class="m2"><p>ماه گفت ای مهر تابان الغیاث</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>پیش زلف و رویت اندر روز و شب</p></div>
<div class="m2"><p>گفت دایم کفر و ایمان الغیاث</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>آدمی بار امانت بر گرفت</p></div>
<div class="m2"><p>با خدازان گفت انسان الغیاث</p></div></div>