---
title: >-
    شمارهٔ ۲۲۶
---
# شمارهٔ ۲۲۶

<div class="b" id="bn1"><div class="m1"><p>گفت رحمان با حمد عربی</p></div>
<div class="m2"><p>سبقت رحمتی علی غضبی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ساخت کارش مسبب الاسباب</p></div>
<div class="m2"><p>دل او ساخت پیشه بی سببی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>باده روح را بجان مینوش</p></div>
<div class="m2"><p>دل قدح دان چو شیشه حلبی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نور پاکت ز نور احمد دان</p></div>
<div class="m2"><p>نفس کافر ز فعل بو لهبی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون جگر شد کباب ز آتش عشق</p></div>
<div class="m2"><p>خون دل خور چو باده عنبی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهیا در صفات و ذات قدیم</p></div>
<div class="m2"><p>بوده‌ای پیشتر ز ام و ابی</p></div></div>