---
title: >-
    شمارهٔ ۳۱
---
# شمارهٔ ۳۱

<div class="b" id="bn1"><div class="m1"><p>ذات و صفات در نظر عارفان یکی است</p></div>
<div class="m2"><p>گر روشن است چشم دلت جسم و جان یکی است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>معشوق و عشق و عاشق و ذرات کاینات</p></div>
<div class="m2"><p>پنهان و آشکار و مکین و مکان یکی است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گر صد هزار شاهد رعنا نمود روی</p></div>
<div class="m2"><p>بنگر بروی جمله که آن دلستان یکی است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر شی بحمد حضر الله ناطق است</p></div>
<div class="m2"><p>بشنو که جمله را دل وچشم و زبان یکی است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ما را به طفلیت خبری پیر عشق داد</p></div>
<div class="m2"><p>منگر سیه سفید که پیرو جوان یکی است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>گفتند با دواب روان عندلیب را</p></div>
<div class="m2"><p>سرو سهی و باغ و گل و بوستان یکی است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوهی چو شد فنا خبری دارد از بقا</p></div>
<div class="m2"><p>دارد نشان که حضرت او جاودان یکی است</p></div></div>