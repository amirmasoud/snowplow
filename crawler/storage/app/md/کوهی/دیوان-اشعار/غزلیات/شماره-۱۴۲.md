---
title: >-
    شمارهٔ ۱۴۲
---
# شمارهٔ ۱۴۲

<div class="b" id="bn1"><div class="m1"><p>آمد آن دلبر قلندر روش</p></div>
<div class="m2"><p>فارغ از مصحف و عمامه وفش</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سوخت ادراک علم و فتوی را</p></div>
<div class="m2"><p>بمی ارغوان چون آتش</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ساغری پرشراب احمد کرد</p></div>
<div class="m2"><p>لب او گفت بی دهان در کش</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا بدیدم جمال ساقی را</p></div>
<div class="m2"><p>شدم از چشمهای او سرخوش</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دید ساقی که خورده شد جامی</p></div>
<div class="m2"><p>گفت کوهی تو می کنی خوش خوش</p></div></div>