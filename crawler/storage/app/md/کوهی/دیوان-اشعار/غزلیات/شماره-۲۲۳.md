---
title: >-
    شمارهٔ ۲۲۳
---
# شمارهٔ ۲۲۳

<div class="b" id="bn1"><div class="m1"><p>در فنای فقر دیرینم توئی</p></div>
<div class="m2"><p>ملک و تاج و تخت و زرینم توئی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گر ندارم دین و دنیا باک نیست</p></div>
<div class="m2"><p>خالق هم آن و هم اینم توئی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>همچو گل بشکفتم از باد بهار</p></div>
<div class="m2"><p>در چمن چون سرو سیمینم توئی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نون ابروی تو بینم در نظر</p></div>
<div class="m2"><p>روشنی عین چون میمم توئی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>گفتمش بعد از همه یادم کنی</p></div>
<div class="m2"><p>گفت کوهی یار پیشینم توئی</p></div></div>