---
title: >-
    شمارهٔ ۱۵۹
---
# شمارهٔ ۱۵۹

<div class="b" id="bn1"><div class="m1"><p>ما چو داریم بسرو قد دلدار طمع</p></div>
<div class="m2"><p>بلبل از حضرت ما کرد بگلزار طمع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل هر ذره که داریم بصد دلبازی</p></div>
<div class="m2"><p>دارد از طلعت خورشید تو انوار طمع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>من دیوانه بیدل که ندارم زر و سیم</p></div>
<div class="m2"><p>کرده ام از لب جان بخش تو صد بار طمع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>زاهد اندر هوس لعل لب میگونت</p></div>
<div class="m2"><p>کرده از صومعه ها باده خمار طمع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا کند کحل بصر مردمک دیده ما</p></div>
<div class="m2"><p>کرد از خاک رهت چشم گهربار طمع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>یار ماخنده کند با رخ مه تا شب و روز</p></div>
<div class="m2"><p>دارد از عاشق خود دیده خونبار طمع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گر کسی راز کرم های تو چشم طمع است</p></div>
<div class="m2"><p>داشت کوهی ز عطاهای تو صد بار طمع</p></div></div>