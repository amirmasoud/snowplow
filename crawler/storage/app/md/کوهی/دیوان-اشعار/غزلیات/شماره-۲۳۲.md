---
title: >-
    شمارهٔ ۲۳۲
---
# شمارهٔ ۲۳۲

<div class="b" id="bn1"><div class="m1"><p>هر دم بشکل دیگر دید ار مینمائی</p></div>
<div class="m2"><p>روی چو ارغوانرا گلنار مینمائی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه شاهد شکر لب گه بادهای رنگین</p></div>
<div class="m2"><p>گاهی گلاب باشی گه خار مینمائی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گه یار دوست باشی اندر مقام وحدت</p></div>
<div class="m2"><p>گه دشمنی بکثرت خونخوار مینمائی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>اقرار مینمائی یعنی که نیست جز من</p></div>
<div class="m2"><p>چون گویمت که هستی انکار مینمائی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون آفتاب مطلق خود گفته ی اناالحق</p></div>
<div class="m2"><p>هر ذره ی چو منصور بردار مینمائی</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>میخواستم به بینم یکبار رویت ای دوست</p></div>
<div class="m2"><p>هر لمعه ی که دیدم صد بار مینمائی</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>با خویش عشقبازی با دیگری نسازی</p></div>
<div class="m2"><p>از غیر خویش دیدم بیزار مینمائی</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>در جام جمله اشیا سایر توئی چو خورشید</p></div>
<div class="m2"><p>سایر بذات خویشی ستار مینمائی</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>در غار سینه کوهی بنشست و دم فروبست</p></div>
<div class="m2"><p>چون مصطفی حجابی در غار مینمائی</p></div></div>