---
title: >-
    شمارهٔ ۱۶۹
---
# شمارهٔ ۱۶۹

<div class="b" id="bn1"><div class="m1"><p>هستم از علم نظر دانای حق</p></div>
<div class="m2"><p>چون بچشم حق شدم بینای حق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>جسم چون داراست و جان منصور باز</p></div>
<div class="m2"><p>زان اناالحق گفت و شد گویای حق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>هر چه موجودند از بالا و پست</p></div>
<div class="m2"><p>قطره محوند در دریای حق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>معنی کفوا احد دانی که چیست</p></div>
<div class="m2"><p>نیست جز حق هیچکس همتای حق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>هم بگوش جان شنیدم صبحدم</p></div>
<div class="m2"><p>هست کوهی جان انسان جای حق</p></div></div>