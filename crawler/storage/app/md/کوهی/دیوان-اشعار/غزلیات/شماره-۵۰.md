---
title: >-
    شمارهٔ ۵۰
---
# شمارهٔ ۵۰

<div class="b" id="bn1"><div class="m1"><p>جانرا ز عکس خال تو بر دل چو داغها است</p></div>
<div class="m2"><p>در دیده هم ز روی تو بینم چراغها است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چشمت بغمزه گشت مرا بارها ولی</p></div>
<div class="m2"><p>دل زنده شد که خنده لعل تو جان فزا است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از عرش تا بفرش فروغ رخت گرفت</p></div>
<div class="m2"><p>روشن شد اینکه پرتو خورشید از کجاست</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در اصبعین او است دل منقلب بدان</p></div>
<div class="m2"><p>شکرخدا که منزل دلدار جان ما است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بگذشته ایم از بد و از نیک فارغیم</p></div>
<div class="m2"><p>چون هر چه غیر هستی او هست او فنا است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شام زلف او همه سرگشته مانده ایم</p></div>
<div class="m2"><p>ما را بوصل شمع رخت یار رهنما است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوهی دو بوسه جستی و دلداردم نزد</p></div>
<div class="m2"><p>میبوس پای یار که خاموشی از رضا است</p></div></div>