---
title: >-
    شمارهٔ ۷۸
---
# شمارهٔ ۷۸

<div class="b" id="bn1"><div class="m1"><p>تا غمزه شوخ تو بما جنگ برآورد</p></div>
<div class="m2"><p>لعل لبت از خون دلم رنگ برآورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>برگریه و زاری و فغان من درویش</p></div>
<div class="m2"><p>صد ناله زار از دل چون چنگ برآورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>با یاد گل روی تو ای سرو گلندام</p></div>
<div class="m2"><p>با بلبل شوریده دل آهنگ برآورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا غیر تو در خلوت جان راه نیابد</p></div>
<div class="m2"><p>بام و در خود را دل ما تنگ برآورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از دود دل سوخت کوهی بسحرگاه</p></div>
<div class="m2"><p>آئینه رخسار فلک زنگ برآورد</p></div></div>