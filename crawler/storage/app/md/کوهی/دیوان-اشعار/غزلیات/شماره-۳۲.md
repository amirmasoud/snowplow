---
title: >-
    شمارهٔ ۳۲
---
# شمارهٔ ۳۲

<div class="b" id="bn1"><div class="m1"><p>رندی و شب روی به دو عالم چکار ماست</p></div>
<div class="m2"><p>شکرخدا که دلبر عیار یار ماست</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در شام زلف یار چه احیا کنیم شب</p></div>
<div class="m2"><p>گوید بروز وصل که شب زنده دار ماست</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بر ما چو خضر شد صفت و ذات و اسم و فعل</p></div>
<div class="m2"><p>شد امن دل که ذات حقیقت حصار ماست</p></div></div>