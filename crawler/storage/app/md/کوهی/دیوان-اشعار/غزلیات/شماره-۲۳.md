---
title: >-
    شمارهٔ ۲۳
---
# شمارهٔ ۲۳

<div class="b" id="bn1"><div class="m1"><p>از شمع ماه روی تو پر زیور آفتاب</p></div>
<div class="m2"><p>در مشعل فلک بمثل اخگر آفتاب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>خورشید لایزال ز لاشرق چون بتافت</p></div>
<div class="m2"><p>گشتند ذره ها همه مه پیکر آفتاب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از آب و رنگ لعل لب آب دار تو</p></div>
<div class="m2"><p>دارد بجام لعل می انور آفتاب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هر جا قدم نهد صنم مه لقا روان</p></div>
<div class="m2"><p>از خاکپای دوست برآرد سر آفتاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>از پرتو جمال تو ای پرتو اله</p></div>
<div class="m2"><p>ذرات کاینات بسوزد در آفتاب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>عشقت چو در دو کون خروسی بود سفید</p></div>
<div class="m2"><p>چون آسمان و بیضه دراو اصفر آفتاب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>از رشک روی ماه تو ای آفتاب جان</p></div>
<div class="m2"><p>بیرون کشد ز جسم بشر جان در آفتاب</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از آفتاب روی تو کوهی چو ماه شد</p></div>
<div class="m2"><p>بخشد به آفتاب اکرم زیور آفتاب</p></div></div>