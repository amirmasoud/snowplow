---
title: >-
    شمارهٔ ۱۶۷
---
# شمارهٔ ۱۶۷

<div class="b" id="bn1"><div class="m1"><p>بحسن خود شد او دلدار عاشق</p></div>
<div class="m2"><p>که آنرا نیست جز او یار عاشق</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گهی لیلی شدی و گاه مجنون</p></div>
<div class="m2"><p>گهی عذرا شدی و گاه وامق</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>چه اول قل هو الله و احد خواند</p></div>
<div class="m2"><p>بوحدت در نمی گنجد خلایق</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>ز زلف و روی او بشکفت در باغ</p></div>
<div class="m2"><p>گل صد برگ و ریحان و شقایق</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دلیل راه ما شد آفرینش</p></div>
<div class="m2"><p>بخالق راه بردیم از خلایق</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>چه کوهی آفتابی داشت در جان</p></div>
<div class="m2"><p>برآمد از دل او صبح صادق</p></div></div>