---
title: >-
    شمارهٔ ۱۸۳
---
# شمارهٔ ۱۸۳

<div class="b" id="bn1"><div class="m1"><p>بیجان و تن دلم شد با وصل یار واصل</p></div>
<div class="m2"><p>تحصیل یار کردیم علمی بود که حاصل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>گه گه ز روی باطل حق مینماید ای دوست</p></div>
<div class="m2"><p>فرقی نمیتوان کرد ما بین حق و باطل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>او شه بدیده خود بیند جمال خود را</p></div>
<div class="m2"><p>چشمی دیگر نباشد بر روی دوست قابل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود عاشقست و معشوق بر خویش عشق بازد</p></div>
<div class="m2"><p>بر خوان یحبهم را گر بایدت دلایل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>دارد غنای مطلق در غار فقر کوهی</p></div>
<div class="m2"><p>جاوید شد مجرد از جان و از تن و دل</p></div></div>