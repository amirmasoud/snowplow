---
title: >-
    شمارهٔ ۱۱۸
---
# شمارهٔ ۱۱۸

<div class="b" id="bn1"><div class="m1"><p>دل دهان در دهان او دارد</p></div>
<div class="m2"><p>در دهان جان زبان او دارد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>فارغ آمد دلم ز فکر معاش</p></div>
<div class="m2"><p>قوت روح لبان او دارد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جانم اندر نماز پیوسته</p></div>
<div class="m2"><p>سجده بر ابروان او دارد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عقل و ادراک و هوش با کم برد</p></div>
<div class="m2"><p>طره دلستان او دارد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>همچو موسی به باریکی</p></div>
<div class="m2"><p>هر که فکر میان او دارد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>جوهر جان جمله ذرات</p></div>
<div class="m2"><p>لعل شکر فشان او دارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>پای کوهی ز آسمان بگذشت</p></div>
<div class="m2"><p>سر چو بر آستان او دارد</p></div></div>