---
title: >-
    شمارهٔ ۱۷۹
---
# شمارهٔ ۱۷۹

<div class="b" id="bn1"><div class="m1"><p>ای رخت شمع تابخانه دل</p></div>
<div class="m2"><p>خلوت خاص تو میانه دل</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>دل چو در اصبعین تست بچرخ</p></div>
<div class="m2"><p>پس مکن چرخ دل بهانه دل</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>وه که سیمرغ قاف قربت حق</p></div>
<div class="m2"><p>گشته پنهان در آشیانه دل</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>عرش و کرسی و آسمان و زمین</p></div>
<div class="m2"><p>غرقه در بحر بیکرانه دل</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بهمه دل چوبی نشان شده اند</p></div>
<div class="m2"><p>ندهد هیچکس نشانه دل</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>غیر معشوق کس نمی داند</p></div>
<div class="m2"><p>راز پنهان عاشقانه دل</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چنگ و عود و رباب و بربط و نی</p></div>
<div class="m2"><p>پیش مستان بود ترانه دل</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از ازل تا ابد که میگویند</p></div>
<div class="m2"><p>باشد اوصاف یکزمانه دل</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>روح کوهی بدیده جان تو را</p></div>
<div class="m2"><p>در بیانهای عارفانه دل</p></div></div>