---
title: >-
    شمارهٔ ۴۷
---
# شمارهٔ ۴۷

<div class="b" id="bn1"><div class="m1"><p>تا ز رخ آن مه لقا زلفین مشکین برگرفت</p></div>
<div class="m2"><p>نور خورشید رخش هر دو جهان یکسر گرفت</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>آتش تر را در آب خشک ساقی چون بریخت</p></div>
<div class="m2"><p>شعله زد آتش در آب و جمله خشک و تر گرفت</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جز کباب آتشین نقلی نخوردم در شراب</p></div>
<div class="m2"><p>روح من قوت از لب جانبخش آندلبر گرفت</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دید در آئینه روی خویش و آمد در سخن</p></div>
<div class="m2"><p>طوطی روحم که از لعل لبش شکرگرفت</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>تا ابد مست می وصلش بماند بی خمار</p></div>
<div class="m2"><p>در ازل جامی که جام از ساقی کوثر گرفت</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>روز گم شد در دل شب تا سحر گه بی حجاب</p></div>
<div class="m2"><p>هندوی زلفش بشب خورشید را در برگرفت</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>مست بیرون آمد از صحن چمن بگشاد لب</p></div>
<div class="m2"><p>زلف و روی کفر و دین و مؤمن و کافر گرفت</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>ذره ذره آفتاب آمد ز حیرت مه نقاب</p></div>
<div class="m2"><p>هر شبی کو برقع از خورشید درخشان برگرفت</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>خواستم پنهان کنم مهر رخش را در جگر</p></div>
<div class="m2"><p>آفتابی بود لاشرقی که بام و در گرفت</p></div></div>
<div class="b" id="bn10"><div class="m1"><p>گفته کوهی چو بلبل خواند بر سرو سهی</p></div>
<div class="m2"><p>نرگس از مستی آن در بزم گل ساغر گرفت</p></div></div>