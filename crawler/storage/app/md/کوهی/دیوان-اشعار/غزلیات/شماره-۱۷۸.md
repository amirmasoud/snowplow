---
title: >-
    شمارهٔ ۱۷۸
---
# شمارهٔ ۱۷۸

<div class="b" id="bn1"><div class="m1"><p>آتش و آبست و لعل و سیم و زر در جان سنگ</p></div>
<div class="m2"><p>جوهری بشناسد ایدل گوهر پنهان سنگ</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سنگ چون در فطرت خود قابل دیدار بود</p></div>
<div class="m2"><p>نقد جان را بر محک زد این بود بنیان سنگ</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>خانه ی دارد خدا از سنگ بر روی زمین</p></div>
<div class="m2"><p>حاجیان گردند هر عیدی از آن مهمان سنگ</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>قاف و القرآن مراد از کوه مرا دوست را</p></div>
<div class="m2"><p>هست عالم کوهیا چون کاسه ی بر خوان سنگ</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>آتشی دارد دل سنگ از محبت در نهاد</p></div>
<div class="m2"><p>داغ دارد لاله بر جان از دل بریان سنگ</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بر معادن دست یابد زر سرخ آرد بدست</p></div>
<div class="m2"><p>هر که چونکوهی نشیند معتکف درکان سنگ</p></div></div>