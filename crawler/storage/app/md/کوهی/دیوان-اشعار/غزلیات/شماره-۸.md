---
title: >-
    شمارهٔ ۸
---
# شمارهٔ ۸

<div class="b" id="bn1"><div class="m1"><p>ازگلستان جنان آمد صبا</p></div>
<div class="m2"><p>جان هر سر در روان آمد صبا</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بسکه می گوید ز گل گل در چمن</p></div>
<div class="m2"><p>از نفیر بلبلان آمد صبا</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>سرو شد خرم بباغ اندر چمن</p></div>
<div class="m2"><p>چون بصحن بوستان آمد صبا</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>تا گل و بلبل بهم شادی کنند</p></div>
<div class="m2"><p>از برای دوستان آمد صبا</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مشک بار آورد هر شاخ شجر</p></div>
<div class="m2"><p>کز دو زلف گلرخان آمد صبا</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>در شب تاریک پیش زلف یار</p></div>
<div class="m2"><p>رهنمای عاشقان آمد صبا</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>آتش اندر غنچه صد برگ زد</p></div>
<div class="m2"><p>بر سر آب روان آمد صبا</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>از صبا بشنید کوهی بوی یار</p></div>
<div class="m2"><p>چون سحر زان آستان آمد صبا</p></div></div>