---
title: >-
    شمارهٔ ۱۱۵
---
# شمارهٔ ۱۱۵

<div class="b" id="bn1"><div class="m1"><p>در شام صبح صادق دیدم که سر بر آورد</p></div>
<div class="m2"><p>ماه دو هفته روئی چون مهر خاور آورد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>شام سحر ندیدم جز آفتاب رویش</p></div>
<div class="m2"><p>کان ماه روی خود را اندر برابر آورد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در روی ما بخندید دلبر چو صبح صادق</p></div>
<div class="m2"><p>کان خنده لب او صد قند و شکر آورد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>آنمه چو روی بنمود شد هر دو کون روشن</p></div>
<div class="m2"><p>هر ذره از جمالش خورشید دیگر آورد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>شاه دو عالم آمد در کلبه فقیران</p></div>
<div class="m2"><p>شمع و شراب و شاهد با خویشتن آورد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>شاهد جمال او بود می لعل آبدارش</p></div>
<div class="m2"><p>روی چو آتش او شمع معنبر آمد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>در داد ساغری می چون آفتاب روشن</p></div>
<div class="m2"><p>در کام من بمستی لعل لبش برآورد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>چون شیر و شهد و شکر بودیم هر دو آن شب</p></div>
<div class="m2"><p>اما شب طویلش چون صبح محشر آورد</p></div></div>
<div class="b" id="bn9"><div class="m1"><p>از پرتو جمالش کان آفتاب جانهاست</p></div>
<div class="m2"><p>کوهی ز سنگ خارا گه لعل و گه زر آورد</p></div></div>