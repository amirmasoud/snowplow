---
title: >-
    شمارهٔ ۲۶
---
# شمارهٔ ۲۶

<div class="b" id="bn1"><div class="m1"><p>هر صبا از چرخ آمد آفتاب مه نقاب</p></div>
<div class="m2"><p>روی بنماید که هستم نور آن عالیجناب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>با همه ذرات عالم در حدیث آمد خموش</p></div>
<div class="m2"><p>گوید ای اولاد من چون نی تو در آب و تراب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>گل سئوال از بلبل شیدا کند کین ناله چیست</p></div>
<div class="m2"><p>عنچه بگشاید دهن گوید سئوالشرا جواب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در دهان بلبل ای گل صد زبان بگشاده</p></div>
<div class="m2"><p>تا بگوئی وصف حسن خویشتن با شیخ و شاب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>وه که پیش شمع رخسار جمالش تا بروز</p></div>
<div class="m2"><p>همچو پروانه دل سوزان ما می‌شد کباب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهی دیوانه دل شد مست و لایعقل بماند</p></div>
<div class="m2"><p>چون کشید از جام ساقی باده با چنگ و رباب</p></div></div>