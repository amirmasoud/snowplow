---
title: >-
    شمارهٔ ۲۲۱
---
# شمارهٔ ۲۲۱

<div class="b" id="bn1"><div class="m1"><p>در تو حیرانم که چونم ساختی</p></div>
<div class="m2"><p>چار عنصر را بهم پرداختی</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>وز دل و ز دیده ی ما ای حبیب</p></div>
<div class="m2"><p>خویشتن را دیده و بشناختی</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قلب مؤمن گفته عرش من است</p></div>
<div class="m2"><p>آمنی و عرش را بنواختی</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>خود شراب و شاهد و ساقی شدی</p></div>
<div class="m2"><p>زان چو شمعم در میان بگداختی</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>کوهیا روزی که قالب ساختند</p></div>
<div class="m2"><p>سگ شدی واسب را می تاختی</p></div></div>