---
title: >-
    شمارهٔ ۱۹۷
---
# شمارهٔ ۱۹۷

<div class="b" id="bn1"><div class="m1"><p>مرکز عرش است دل خال سیه همتای او</p></div>
<div class="m2"><p>رشته زلف است جان عمر سمن فرسای او</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>عالمی را کشت و دردم زنده کرد آن جانفزا</p></div>
<div class="m2"><p>یحیی الموتی است می بینم در لبهای او</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>می نگنجد در زمین وعرش و کرسی آه آه</p></div>
<div class="m2"><p>جز دل پرخون نمی بینم یاران جای او</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>هست موجودات ظل او واو چون آفتاب</p></div>
<div class="m2"><p>در دل هر ذرهٔ روی قمرفرسای او</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بر لب دل گوش نه تا بشنوی بی واسطه</p></div>
<div class="m2"><p>علم توحید خداوند از لب گویای او</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کوهی دیوانه دل تا دید آن چشم سیاه</p></div>
<div class="m2"><p>همچو آهو می دود پیوسته در صحرای او</p></div></div>