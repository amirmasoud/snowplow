---
title: >-
    شمارهٔ ۱۸۵
---
# شمارهٔ ۱۸۵

<div class="b" id="bn1"><div class="m1"><p>دلبرا جانب ارباب وفا بگشا چشم</p></div>
<div class="m2"><p>که مرا از رخ زیبای تو شد بینا چشم</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>تا بر آریم ز وصل تو در از بحر وجود</p></div>
<div class="m2"><p>دارد از گریه پنهان دل و هم دریا چشم</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>تا به بیند نظر پاک بصد دیده تو را</p></div>
<div class="m2"><p>در تماشای تو گشتیم ز سر تا پا چشم</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>نظری کن که همه بر مه رویت دارند</p></div>
<div class="m2"><p>آدم از پستی خاک و ملک از بالا چشم</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>یار چون مردمک دیده دل شد کوهی</p></div>
<div class="m2"><p>باز کردند بدیدار خدا جانها چشم</p></div></div>