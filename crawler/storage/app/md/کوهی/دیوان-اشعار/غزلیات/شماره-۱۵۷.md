---
title: >-
    شمارهٔ ۱۵۷
---
# شمارهٔ ۱۵۷

<div class="b" id="bn1"><div class="m1"><p>کار دنیا همه زرق است و فریب است صداع</p></div>
<div class="m2"><p>عارفان بر سر اینها نکنند ایچ نزاع</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>مفلسانیم که عالم بجوی نستانیم</p></div>
<div class="m2"><p>نیست ما را بجهان جز غم عشاق متاع</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>زاهد از زهد و ریا دور که رندان صبوح</p></div>
<div class="m2"><p>بوی تزویر شنیدند همه زین اوضاع</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>مو کشانش بخرابات درآریم چو چنگ</p></div>
<div class="m2"><p>هر که ما را ز می لعل تو باشد مناع</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>بوصالت نرسد هرگز و واصل نشود</p></div>
<div class="m2"><p>هر که از جان نکند با غم عشق تو وداع</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>ظالم از درد تو هر دم بعدم نیست شود</p></div>
<div class="m2"><p>می کند غمزه خونخوار تو بازش ابداع</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>همچو کوهی بجهان روشن و فردیم همه</p></div>
<div class="m2"><p>تا گرفتیم زخورشید تو چون ماه شعاع</p></div></div>