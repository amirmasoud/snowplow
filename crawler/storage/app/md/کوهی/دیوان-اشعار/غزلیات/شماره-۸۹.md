---
title: >-
    شمارهٔ ۸۹
---
# شمارهٔ ۸۹

<div class="b" id="bn1"><div class="m1"><p>وحدت چو احد نمود واحد</p></div>
<div class="m2"><p>مشهود چو بودعین شاهد</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>چون لیس کمثله شنیدی</p></div>
<div class="m2"><p>یعنی که نبود ذات زائد</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>ذرات به آفتاب پیدا است</p></div>
<div class="m2"><p>کردیم بیان اسم ماجد</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>محمود چه عاشق ایاز است</p></div>
<div class="m2"><p>معبود ببود خویش عابد</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>چون غیر وجود در عدم نیست</p></div>
<div class="m2"><p>با هستی او است نیستی ضد</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>از غیب هویت او نظر کرد</p></div>
<div class="m2"><p>از غیب شد این شهود وارد</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>چون هست یقین که غیر او نیست</p></div>
<div class="m2"><p>بگذر تو هم از خیال فاسد</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>سیمرغ صفت چو جانکوهی</p></div>
<div class="m2"><p>بر قاف قناعت است قاصد</p></div></div>