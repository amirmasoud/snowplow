---
title: >-
    شمارهٔ ۲۳۳
---
# شمارهٔ ۲۳۳

<div class="b" id="bn1"><div class="m1"><p>ای که منظور و برخود ناظری</p></div>
<div class="m2"><p>روی خود بینی به هرجا بنگری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>ما به غیب آورده ایم ایمان بلی</p></div>
<div class="m2"><p>هم تو در غیبی و هم تو حاضری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>قوت روح جمله اشیا شدی</p></div>
<div class="m2"><p>در سخن گفتن چه نقل شکری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>صید تیر چشم مست او شدی</p></div>
<div class="m2"><p>کوهیا گر چه به غایت لاغری</p></div></div>