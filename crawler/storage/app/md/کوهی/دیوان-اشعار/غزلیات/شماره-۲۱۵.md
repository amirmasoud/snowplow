---
title: >-
    شمارهٔ ۲۱۵
---
# شمارهٔ ۲۱۵

<div class="b" id="bn1"><div class="m1"><p>بر دوخت دلم ز ما سوالله</p></div>
<div class="m2"><p>جان داد مقام لی مع الله</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>سلطان دو کون در دل تست</p></div>
<div class="m2"><p>تن خیمه شناس و دل چو خرگاه</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بنمود درون دیده روشن</p></div>
<div class="m2"><p>در ظلمت و نورگاه و بیگاه</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کوهی به هوای تابش نور</p></div>
<div class="m2"><p>چون خاک فتاده بر سر راه</p></div></div>