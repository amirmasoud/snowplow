---
title: >-
    شمارهٔ ۲۱۷
---
# شمارهٔ ۲۱۷

<div class="b" id="bn1"><div class="m1"><p>دوش از صومعه در میکده رفتم سحری</p></div>
<div class="m2"><p>تا بیابم ز خرابات نشان و خبری</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بر در دیر مغان مغبچگان را دیدم</p></div>
<div class="m2"><p>آن یکی بود چو خورشید و دگر چون قمری</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>از سر صدق و صفا دست در آغوشم کرد</p></div>
<div class="m2"><p>سینه بر سینه من زد ز صفا سیم بری</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>بوسه ها بر لب من داد و قدح پیش آورد</p></div>
<div class="m2"><p>گفت ما را به جز این نیست بعالم هنری</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>نوش کردم قدحی چند از آن جام طهور</p></div>
<div class="m2"><p>دیدم از پرتو دیدار بجان در اثری</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>کشف شد سرازل تا به ابد در یکدم</p></div>
<div class="m2"><p>بر من از عالم اسرار گشادند دری</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>گوش جانرا بگرفت و قدحی دیگر داد</p></div>
<div class="m2"><p>گفت بشناس مرا از خود و از هر بشری</p></div></div>
<div class="b" id="bn8"><div class="m1"><p>گفت کوهی که منم جمع به اسماء و صفات</p></div>
<div class="m2"><p>هر چه بینی به جهان خشک و تری خیر و شری</p></div></div>