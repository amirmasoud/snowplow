---
title: >-
    شمارهٔ ۱۷
---
# شمارهٔ ۱۷

<div class="b" id="bn1"><div class="m1"><p>گر من از عشق جگر خوار بنالم چه عجب</p></div>
<div class="m2"><p>یا ز جور و ستم یار بنالم چه عجب</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بهوای گل رخسار تو ای سرو بلند</p></div>
<div class="m2"><p>گر چه بلبل بچمن زار بنالم چه عجب</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>جگرم خون شد و از دیده برویم افتاد</p></div>
<div class="m2"><p>گر بجان از دل بیمار بنالم چه عجب</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>در خم زلف سیه کار تو چون دربندم</p></div>
<div class="m2"><p>زار چون مرغ شب تار بنالم چه عجب</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>مینوازی چه نی و میکشی از ناز مرا</p></div>
<div class="m2"><p>وه که از پرده ی پندار بنالم چه عجب</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>سوختم ز آتش هجران تو ای ماه اگر</p></div>
<div class="m2"><p>بهر یکدیدن دیدار بنالم چه عجب</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>دید کوهی که خدا گریه و زاری طلبد</p></div>
<div class="m2"><p>گفت گر بر در جبار بنالم چه عجب</p></div></div>