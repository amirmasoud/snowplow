---
title: >-
    شمارهٔ ۳۵
---
# شمارهٔ ۳۵

<div class="b" id="bn1"><div class="m1"><p>همسایه آفتاب ماه است</p></div>
<div class="m2"><p>همسایه آدمی اله است</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>در جان و تن تو آب حیوان</p></div>
<div class="m2"><p>چون مردم دیده در سیاه است</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>در ملک وجود غیر حق نیست</p></div>
<div class="m2"><p>در دعوی ما خدا گواه است</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>دیدم به درون دیده او را</p></div>
<div class="m2"><p>از دیده دیده در نگاه است</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>جسم تو ز خاک و جان ز خورشید</p></div>
<div class="m2"><p>خورشید میان خاک راه است</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>زان سوخت ز آفتاب رویش</p></div>
<div class="m2"><p>در سایه زلف او پناه است</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>کوهی همه شب چو شمع بر ما</p></div>
<div class="m2"><p>در گریه زار و سوز و آه است</p></div></div>