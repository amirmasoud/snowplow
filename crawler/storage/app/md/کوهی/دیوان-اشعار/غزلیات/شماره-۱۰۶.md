---
title: >-
    شمارهٔ ۱۰۶
---
# شمارهٔ ۱۰۶

<div class="b" id="bn1"><div class="m1"><p>بفضل صانع کون فیکون شدم موجود</p></div>
<div class="m2"><p>وجود یافت بیک امر عابد معبود</p></div></div>
<div class="b" id="bn2"><div class="m1"><p>بشکر آنکه خدا شد مصور آدم</p></div>
<div class="m2"><p>سری نهاد ملک پیش آدم او بسجود</p></div></div>
<div class="b" id="bn3"><div class="m1"><p>بطاق ابروی آنماه جلوه ها کردم</p></div>
<div class="m2"><p>که او ز غیب هویت نمود رخ بشهود</p></div></div>
<div class="b" id="bn4"><div class="m1"><p>کنون ز شهد و شکر می شویم شیرین کام</p></div>
<div class="m2"><p>که غیر حضرت او نیست شاهد و مشهود</p></div></div>
<div class="b" id="bn5"><div class="m1"><p>ز تاب آتش رویش بسوخت هر دو جهان</p></div>
<div class="m2"><p>تعینات گذشتند از جهان چون دود</p></div></div>
<div class="b" id="bn6"><div class="m1"><p>بصد زبان همه اقرار نیستی کردند</p></div>
<div class="m2"><p>شنو ز چنگ و رباب و نی ز بربط ورود</p></div></div>
<div class="b" id="bn7"><div class="m1"><p>بدید کوهی دیوانه صبغة الله را</p></div>
<div class="m2"><p>نه ابیض است و نه اسود نه سرخ و زرد و کبود</p></div></div>